@ECHO OFF
REM
REM File:: install.bat
REM Description:: Top-level tools install process.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
REM Author:: Greg Smith <greg@rockstarnorth.com>
REM date:: 5 March 2009
REM updated:: 11 Dec 2009
REM updated:: 15 Apr 2011 - new c# tools installer, stripped back to basics
REM

setlocal enabledelayedexpansion

REM in case we are calling from some other location
pushd "%~dp0"

SET POWERSHELL=powershell.exe
SET RS_PREV_TOOLSROOT=%RS_TOOLSROOT%
SET RS_TOOLSROOT=%CD%
SET RS_CHECKFILE=%RS_TOOLSROOT%\config.xml
SET RS_INSTALLER=%RS_TOOLSROOT%\bin\install\installer\Installer.exe

IF NOT EXIST %RS_CHECKFILE% (

	echo ===============================================================================
	echo ===============================================================================
	echo Configuration not present. Your installation is corrupt in some way or you are not running this batch file from the correct location. Mail Tools at RSGEDI Tools
	echo ===============================================================================
	echo ===============================================================================
	pause
	GOTO end
)

REM Set argument list here, since we cannot compose it in the IF without expansion.
SET ARGLIST=error
IF NOT "%1"=="" (
	SET ARGLIST=%ARGLIST%,%1
)
REM Make sure that the batch script is being run as an admin.
CALL %RS_TOOLSROOT%\script\util\check_admin_privileges.bat
IF ERRORLEVEL 1 (
	IF "%1"=="error" (
		GOTO ERROR_END
	) ELSE (

REM Add First Arg if it exists, so we add the optional /switch arg.
		powershell "saps -FilePath %0 -ArgumentList %ARGLIST% -Verb runas"
		exit /b
	)
)


REM this will get replaced by a command line switch to the installer exe but leaving for now
SET RS_SWITCHBAT=%RS_TOOLSROOT%\script\local\switchenv.bat
IF EXIST %RS_SWITCHBAT% (
	IF "%1" == "/switch" (

		CALL %RS_SWITCHBAT%
		GOTO END
	)
	
REM Handle Switch case when running Win 10 and not having admin privs when calling the script
	IF "%2" == "/switch" (

		CALL %RS_SWITCHBAT%
		GOTO END
	)
)

REM =================================================
REM Check that .Net 4.6 framework is installed...
REM =================================================

SET MSNET_CHECK=0
set MSNET_CMD=reg query "HKLM\Software\Microsoft\NET Framework Setup\NDP\v4\Full" /v Version
FOR /F "tokens=2* delims=	 " %%A IN ('%MSNET_CMD%') DO ( SET MSNET_CHECK=%%B )

IF %MSNET_CHECK% LSS 4.6.01586 (
	ECHO Installing Microsoft .Net 4.6.2...
	%RS_TOOLSROOT%\bin\install\NDP462-KB3151800-x86-x64-AllOS-ENU.exe
)

REM =================================================

REM =================================================
REM Stop RAG and SystrayRfs.
REM =================================================

ECHO Killing SystrayRFS and RAG
taskkill /IM SysTrayRfs.exe >NUL 2>NUL
taskkill /IM rag.exe >NUL 2>NUL
taskkill /IM ragApp.exe >NUL 2>NUL

REM =================================================

REM =================================================
REM Start our Installer
REM =================================================

IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 

	REM =================================================
	REM make sure that vc runtime 2010 x64 is installed
	REM =================================================

	SET MSNET_CHECK=notfound
	SET MSNET_CMD=reg query "HKLM\SOFTWARE\Wow6432Node\Microsoft\VisualStudio\10.0\VC\VCRedist\x64" /v Installed
	FOR /F "tokens=2* delims=	 " %%A IN ('!MSNET_CMD!') DO ( SET MSNET_CHECK=%%B )

	IF !MSNET_CHECK!==notfound (

		%RS_TOOLSROOT%\bin\install\vcredist10_x64.exe
	)

	REM =================================================
	REM make sure that vc runtime 2010 x86 is installed
	REM =================================================

	SET MSNET_CHECK=notfound
	SET MSNET_CMD=reg query "HKLM\SOFTWARE\Wow6432Node\Microsoft\VisualStudio\10.0\VC\VCRedist\x86" /v Installed
	FOR /F "tokens=2* delims=	 " %%A IN ('!MSNET_CMD!') DO ( SET MSNET_CHECK=%%B )

	IF !MSNET_CHECK!==notfound (

		%RS_TOOLSROOT%\bin\install\vcredist10_x86.exe
	)
	
	REM =================================================
	REM make sure that vc runtime 2012 x64 is installed
	REM =================================================

	SET MSNET_CHECK=notfound
	SET MSNET_CMD=reg query "HKLM\SOFTWARE\Microsoft\DevDiv\VC\Servicing\11.0\RuntimeMinimum" /v Install
	FOR /F "tokens=2* delims=	 " %%A IN ('!MSNET_CMD!') DO ( SET MSNET_CHECK=%%B )

	IF !MSNET_CHECK!==notfound (

		%RS_TOOLSROOT%\bin\install\vcredist11_2012_x64.exe
	)
	
	REM =================================================
	REM make sure that vc runtime 2012 x86 is installed
	REM =================================================

	SET MSNET_CHECK=notfound
	SET MSNET_CMD=reg query "HKLM\SOFTWARE\Wow6432Node\Microsoft\DevDiv\VC\Servicing\11.0\RuntimeMinimum" /v Install
	FOR /F "tokens=2* delims=	 " %%A IN ('!MSNET_CMD!') DO ( SET MSNET_CHECK=%%B )

	IF !MSNET_CHECK!==notfound (

		%RS_TOOLSROOT%\bin\install\vcredist11_2012_x86.exe
	)
	
	REM =================================================
	REM make sure that vc runtime 2015 x64 is installed
	REM =================================================

	SET MSNET_CHECK=notfound
	SET MSNET_CMD=reg query "HKLM\SOFTWARE\Microsoft\DevDiv\VC\Servicing\14.0\RuntimeMinimum" /v Install
	FOR /F "tokens=2* delims=	 " %%A IN ('!MSNET_CMD!') DO ( SET MSNET_CHECK=%%B )

	IF !MSNET_CHECK!==notfound (

		%RS_TOOLSROOT%\bin\install\vcredist14_2015_x64.exe
	)
	
	REM =================================================
	REM make sure that vc runtime 2015 x86 is installed
	REM =================================================

	SET MSNET_CHECK=notfound
	SET MSNET_CMD=reg query "HKLM\SOFTWARE\Wow6432Node\Microsoft\DevDiv\VC\Servicing\14.0\RuntimeMinimum" /v Install
	FOR /F "tokens=2* delims=	 " %%A IN ('!MSNET_CMD!') DO ( SET MSNET_CHECK=%%B )

	IF !MSNET_CHECK!==notfound (

		%RS_TOOLSROOT%\bin\install\vcredist14_2015_x86.exe
	)
	
) ELSE (

	REM =================================================
	REM make sure that vc runtime 2010 x86 is installed
	REM =================================================

	SET MSNET_CHECK=notfound
	SET MSNET_CMD=reg query "HKLM\SOFTWARE\Microsoft\VisualStudio\10.0\VC\VCRedist\x86" /v Installed
	FOR /F "tokens=2* delims=	 " %%A IN ('!MSNET_CMD!') DO ( SET MSNET_CHECK=%%B )

	IF !MSNET_CHECK!==notfound (

		%RS_TOOLSROOT%\bin\install\vcredist10_x86.exe
	)
	
	REM =================================================
	REM make sure that vc runtime 2012 x86 is installed
	REM =================================================

	SET MSNET_CHECK=notfound
	SET MSNET_CMD=reg query "HKLM\SOFTWARE\Wow6432Node\Microsoft\DevDiv\VC\Servicing\11.0\RuntimeMinimum" /v Install
	FOR /F "tokens=2* delims=	 " %%A IN ('!MSNET_CMD!') DO ( SET MSNET_CHECK=%%B )

	IF !MSNET_CHECK!==notfound (

		%RS_TOOLSROOT%\bin\install\vcredist11_2012_x86.exe
	)

	REM =================================================
	REM make sure that vc runtime 2015 x86 is installed
	REM =================================================

	SET MSNET_CHECK=notfound
	SET MSNET_CMD=reg query "HKLM\SOFTWARE\Wow6432Node\Microsoft\DevDiv\VC\Servicing\14.0\RuntimeMinimum" /v Install
	FOR /F "tokens=2* delims=	 " %%A IN ('!MSNET_CMD!') DO ( SET MSNET_CHECK=%%B )

	IF !MSNET_CHECK!==notfound (

		%RS_TOOLSROOT%\bin\install\vcredist14_2015_x86.exe
	)
)

REM =================================================
REM Run our installer executable
REM Only one now as we can read both x86 and x64 registry.
REM =================================================

ECHO Running Tools Installer
%RS_INSTALLER% %1 %2 %3 %4 %5

REM Installer already running
IF ERRORLEVEL 536870912 GOTO END
REM Installer canceled?
IF ERRORLEVEL 2 GOTO END
REM Installer error?
IF ERRORLEVEL 1 GOTO ERROR_END

REM Register File Association Extensions
REM Re-generate the REG file first, then import into Windows Registry.
SET TOOLSBIN=%RS_TOOLSROOT%\bin
SET TOOLSLIB=%RS_TOOLSROOT%\lib
SET REGISTER_EXTENSIONS_SCRIPT=%TOOLSLIB%\util\registry\generate_register_extensions.rb
SET REGISTER_EXTENSIONS=%RS_TOOLSROOT%\tmp\register_extensions.reg
ECHO Generating file associations script...
%TOOLSBIN%\ruby\bin\ruby -W0 -rrubygems -I%TOOLSLIB% %REGISTER_EXTENSIONS_SCRIPT%
ECHO Importing file associations script...
REG IMPORT %REGISTER_EXTENSIONS%
ECHO Resetting tray icons...
CALL %TOOLSBIN%\resetTray.bat -f

REM =================================================
REM Initialize Context Menu
REM =================================================

call %RS_TOOLSROOT%\bin\install\unregister_shell_extension.bat
call %RS_TOOLSROOT%\bin\install\register_shell_extension.bat

REM =================================================
REM Install Python
REM =================================================
REM %RS_TOOLSROOT%\bin\python\App\setup\SetupPython.bat 

ECHO Done.
GOTO END

:ERROR_END

echo [101;93m
ECHO Error running Rockstar Games Tools Installer.
ECHO Make sure that you are running the installer from Windows Explorer (not from PV4 directly) and that you have mapped the containing folder to 'x:\gta5\tools_ng'. Refer to the Hub documentation for more information.
ECHO Reach out to your local tools support mailing list once you have checked the items above and the installation still fails. 
ECHO [0m
PAUSE

:END

popd

REM install.bat
