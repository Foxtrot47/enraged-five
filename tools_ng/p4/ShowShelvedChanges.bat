@echo off
setlocal EnableDelayedExpansion

if [%1] == [] echo To Use: %0 ^<cl^> && exit /b 1

set bcomp=
set bcompRelative=Beyond Compare 4\bcomp.com
if exist "%ProgramFiles(x86)%\%bcompRelative%" set bcomp="%ProgramFiles(x86)%\%bcompRelative%"
if exist "%ProgramFiles%\%bcompRelative%" set bcomp="%ProgramFiles%\%bcompRelative%"
if exist "%ProgramW6432%\%bcompRelative%" set bcomp="%ProgramW6432%\%bcompRelative%"

if [%bcomp%] == [] echo Beyond Compare not found && goto :error

:process
if [%1] == [] goto :end

set CL=%1
echo %CL%:

for /f "tokens=1,2" %%i in ('p4 describe -s -S %CL%') DO if [%%i] == [...] (
	for /f "delims=#" %%f in ("%%j") DO set depotFile=%%f
 	p4 print -o "%temp%\_currentCOMP" !depotFile!@=%CL% > nul
	for /f "delims=#" %%f in ("!depotFile!") DO set file=%%f
	for /f "tokens=2,3" %%f in ('p4 -ztag where !file!') DO if [%%f] == [path] set file=%%g
	%bcomp% /qc "%temp%\_currentCOMP" !file! || call :different !file!
)

shift /1
goto :process

:end
echo *** Completed
pause
exit /b 0

:different
rem with BC, 12 and above is "different"
if errorlevel 12 echo %1
exit /b 0

:error
pause
exit /b 1
