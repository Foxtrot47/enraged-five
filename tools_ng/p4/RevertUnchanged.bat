@echo off
setlocal

set bcomp=
set bcompRelative=Beyond Compare 4\bcomp.com
if exist "%ProgramFiles(x86)%\%bcompRelative%" set bcomp="%ProgramFiles(x86)%\%bcompRelative%"
if exist "%ProgramFiles%\%bcompRelative%" set bcomp="%ProgramFiles%\%bcompRelative%"
if exist "%ProgramW6432%\%bcompRelative%" set bcomp="%ProgramW6432%\%bcompRelative%"

if [%bcomp%] == [] echo Beyond Compare not found && goto :error

if [%1] == [] echo. Please pass one or more changeslists && goto :error

:process
if [%1] == [] goto :end
echo p4 revert -a -c %1
rem p4 revert -a -c %1 || goto :error

echo revert pending integrations
for /f "tokens=1 delims=# " %%f in ('p4 revert -a -c %1 ^| findstr /C:"has pending integrations"') DO (
	call :revert %%f
)

echo revert branch
rem for /f "tokens=2 delims=# " %%f in ('p4 describe -s %1 ^| find /I "#1 add"') DO (
for /f "tokens=2 delims=# " %%f in ('p4 describe -s %1 ^| findstr /C:"#1 add" /C:"#1 branch"') DO (
	call :revertNew %%f
)

echo revert deleted
for /f "tokens=2 delims=# " %%f in ('p4 describe -s %1 ^| findstr /C:" delete"') DO (
	call :revertDeleted %%f
)

echo revert move/deleted
for /f "tokens=2 delims=# " %%f in ('p4 describe -s %1 ^| findstr /C:" move/delete"') DO (
	call :revertMoveDeleted %%f
)

shift /1
goto :process

:end
exit /b 0

:revert
for /f "tokens=3" %%w in ('p4 -ztag where %1 ^| find /I "... path"') DO (
	p4 print -o %TEMP%\lgh.txt %1
	%bcomp% /qc %TEMP%\lgh.txt %%w
	if NOT errorlevel 11 p4 revert %1 && p4 sync -f %1
)
exit /b 0

:revertNew
for /f "tokens=2,3" %%h in ('p4 fstat %1 ^| find /I "headrev"') DO (
	if [%%i] == [1] (
		for /f "tokens=3" %%w in ('p4 -ztag where %1 ^| find /I "... path"') DO (
			p4 print -o %TEMP%\lgh.txt %1
			%bcomp% /qc %TEMP%\lgh.txt %%w
			if NOT errorlevel 11 p4 revert %1 && p4 sync -f %1
		)
	)
)
exit /b 0

:revertDeleted
setlocal
set headAction=none
set action=none
for /f "tokens=2,3" %%a in ('p4 fstat %1 ^| findstr /C:"headAction " /C:"action "') DO (
set %%a=%%b
)
if [%headAction%] == [delete] if [%action%] == [delete] p4 revert %1 && p4 sync -f %1
exit /b 0

:revertMoveDeleted
setlocal
set headAction=none
set action=none
set movedFile=none
for /f "tokens=2,3" %%a in ('p4 fstat %1 ^| findstr /C:"headAction " /C:"action " /C:"movedFile "') DO (
set %%a=%%b
)
if not [%movedFile%] == [none] if [%headAction%] == [move/delete] if [%action%] == [move/delete] p4 revert %1 %movedFile% && p4 sync -f %1 %movedFile%
exit /b 0

:error
pause
exit /b 1
