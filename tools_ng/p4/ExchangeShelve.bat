@echo off
setlocal

set name=%~nx1%
set tmp=%TEMP%\%name%
set shelveTmp=%TEMP%\shelved_%name%
if exist "%tmp%" call :run del /F /Q "%tmp%"
if exist "%shelveTmp%" call :run del /F /Q "%shelveTmp%"

set cl=
set depotFile=
for /f "tokens=2,3 delims= " %%i in ('p4 -Ztag fstat "%1"') DO if [%%i] == [change] (set cl=%%j) else (if [%%i] == [depotFile] set depotFile=%%j)
if [%cl%] == [] echo %1 doesn't appear to be checked out && exit /b 1
if [%depotFile%] == [] echo failed to get depot location for %1 && exit /b 1
echo using depot file %depotFile% cl %cl%
call :run copy /Z "%1" "%tmp%" || goto :error
call :run p4 print -o "%shelveTmp%" "%depotFile%@=%cl%" || goto :error
call :run p4 shelve -c %cl% -f %depotFile% || goto :error
call :run copy /Z "%shelveTmp%" "%1" || goto :error
call :run attrib -r "%1" || goto :error
rem pause
echo *** FINISHED
exit /b 0

:error
echo *** FAILED
exit /b 1

:run
echo %*
%* || exit /b 1

exit /b 0