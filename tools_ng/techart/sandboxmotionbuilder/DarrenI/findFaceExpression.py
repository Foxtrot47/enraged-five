import pyfbsdk as mobu
import os
import xml.etree.ElementTree as elementTree
from RS import Config, Perforce
from PySide import QtGui


def findFaceExpr(maxPath):
    character = None
    exprSetName = None
    dlcBranch = None
    
    # Find DLC Pack for the current file
    
    if maxPath is not None:
        character = maxPath.split("\\")[-1].split(".")[0]
        dlcBranch = maxPath.lower().split("\\art")[0]
    
    # Find peds.meta for the DLC pack
    if dlcBranch is None:
        QtGui.QMessageBox.information(
                                    None,
                                   "No Art Path!",
                                    "Couldn't find the art path for the max file. Are you sure this was a valid character max path?",
                                    QtGui.QMessageBox.Ok,
                                )
        return
    
    if "p_franklin" in character.lower():
        exprSetName = "P_M_One"
    
    elif not "dlc" in dlcBranch:
        exprSetName = character
        
    else:
        pedMeta = os.path.join(dlcBranch,"build","dev_ng","common","data","peds.meta")
        
        if not Perforce.DoesFileExist(pedMeta) == True:
            exprSetName = character
            return character, exprSetName, dlcBranch
        
        # Store the current version of the meta file
        pedMetaRevision = Perforce.GetFileState(pedMeta).HaveRevision
        Perforce.Sync(pedMeta, force=True)
        
        # Open peds.meta & find entry for the character name
        if os.path.exists(pedMeta):
            tree = elementTree.parse(pedMeta)
            root = tree.getroot()
            for dataManager in root:
                if dataManager.tag == 'InitDatas':
                    for dataSet in dataManager:
                        for item in dataSet:
                            if item.tag == 'Name':
                                if item.text.lower() == character.lower():
                                    # Find the expression dictionary
                                    exprSetField = dataSet.find('ExpressionDictionaryName')
                                    if exprSetField is not None:
                                        exprSetName = exprSetField.text
        
        # Sync back to the revision of the peds.meta file we had before running
        Perforce.Sync(os.path.join(pedMeta), force=True, revision=pedMetaRevision)
    
    if exprSetName is None:
        return
    else:
        return character, exprSetName, dlcBranch


def findExprPath(character, exprSetName, dlcBranch, devBranch="assets_ng"):
    foundExprList = []
    packOrders = {}
    
    headNames = ["Head_000_U.xml",
                 "Head_000_R.xml"]
    
    # Search patchpacks, mppacks, then assets_ng for the expression dictionary for the character
    # Trawl base game expressions
    for headName in headNames:
        baseExprPath = os.path.join(Config.Project.Path.Root, (devBranch + "\\"))
        baseSearchTerm = os.path.join("anim", "expressions", exprSetName, "Components", headName)
        editedBaseTerm = baseSearchTerm.replace("\\","/")
        
        baseArgList = ["-e", "{0}...{1}".format(baseExprPath, editedBaseTerm)]
        baseFiles = Perforce.Run("files", args=baseArgList)
        
        for record in baseFiles.Records:
            try:
                recordText = str(record["depotFile"])
            except:
                recordText = ""
            foundExprList.append(recordText)
        
        # Trawl dlc packs
        dlcPacks = os.path.join("x:\\gta5_dlc", "mpPacks")
        dlcSearchTerm = os.path.join(devBranch, "anim", "expressions", exprSetName, "Components", headName)
        editedDlcTerm = dlcSearchTerm.replace("\\","/")
        
        dlcFiles = ["-e", "{0}...{1}".format(dlcPacks, editedDlcTerm)]
        dlcFiles = Perforce.Run("files", args=dlcFiles)
        
        for record in dlcFiles.Records:
            try:
                recordText = str(record["depotFile"])
            except:
                recordText = ""
            foundExprList.append(recordText)
    
    # Find the pack order for the found paths
    for exprItem in foundExprList:
        if "gta5_dlc" in exprItem:
            dlcBranch = exprItem.split(devBranch)[0].split("mpPacks")[1]
            dlcBranch = dlcBranch.replace("/","")
            buildXml = os.path.join(dlcPacks, dlcBranch, "build", "dev_ng", "setup2.xml")
            Perforce.Sync(buildXml, force=True)
            if os.path.exists(buildXml):
                tree = elementTree.parse(buildXml)
                root = tree.getroot()
                for dataManager in root:
                    if dataManager.tag == 'order':
                        packOrders[exprItem] = dataManager.get('value')
        else:
            packOrders[exprItem] = 1
    
    # compare the pack order
    exprValue = 0
    usedExpr = None
    for item in packOrders:
        if packOrders[item] > exprValue:
            exprValue = packOrders[item]
            usedExpr = item
    
    if usedExpr is None:
        QtGui.QMessageBox.information(
                                    None,
                                   "No Expression Found!",
                                    "Couldn't find face expressions for the max file. Are you sure this was a character max path?",
                                    QtGui.QMessageBox.Ok,
                                )
        return
    
    QtGui.QMessageBox.information(
                                None,
                                "Expression File for {0}:".format(character),
                                usedExpr,
                                QtGui.QMessageBox.Ok,
                            )


maxFilePopup = mobu.FBFilePopup()
maxFilePopup.Caption = "Choose Character Max file"
maxFilePopup.Path = r"x:\gta5_dlc\mpPacks\mpSecurity\Art\ng\Peds"
maxFilePopup.Filter = '*.max'
maxFilePopup.Style = mobu.FBFilePopupStyle.kFBFilePopupOpen

if maxFilePopup.Execute():
    filePath = maxFilePopup.FullFilename
    character = findFaceExpr(filePath)[0]
    faceExpr = findFaceExpr(filePath)[1]
    dlcBranch = findFaceExpr(filePath)[2]
    if faceExpr is not None:
        findExprPath(character, faceExpr, dlcBranch)
    
    
# Otherwise we'll pop up a message to click away
else:
    QtGui.QMessageBox.information(
                                None,
                               "Warning:",
                                "Selection canceled!",
                                QtGui.QMessageBox.Ok
                            )