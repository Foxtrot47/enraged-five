﻿//Written by Geoffrey Fermin
//Simple bat script to grab latest on active RPF and ZIP files to ensure latest data locally when building

@echo off
cls
:start1
echo Hello!
echo This bat file will grab cutscene ZIPs and RPFs for you.
echo Please type in a number, then hit "Enter". 
echo ------------
echo 1 MP_Apartment
echo 2 MP_Lowrider
echo 3 Grab All
echo ------------
echo Please select a project:
set /p choice=
if '%choice%'=='1' goto :choice1
if '%choice%'=='2' goto :choice6
if '%choice%'=='3' goto :choice11
echo "%choice%" is not a valid option. Please try again.
echo
cls
echo You dun goofed, try again.
goto start1

:choice1
cls
:start2
echo MP_Apartment
echo Please type in a number, then hit "Enter". 
echo ------------
echo 1 Grab PS4
echo 2 Grab XBOXONE
echo 3 Grab PC
echo 4 Grab All
echo ------------
echo Please select a platform:
set /p choice=
if '%choice%'=='1' goto :choice2
if '%choice%'=='2' goto :choice3
if '%choice%'=='3' goto :choice4
if '%choice%'=='4' goto :choice5
echo "%choice%" is not a valid option. Please try again.
echo
cls
echo You dun goofed, try again.
goto start2

:choice2
p4 sync -f //gta5_dlc/mpPacks/mpApartment/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/build/dev_ng/ps4/anim/cutscene/...
pause
goto end

:choice3
p4 sync -f //gta5_dlc/mpPacks/mpApartment/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/build/dev_ng/xboxone/anim/cutscene/...
pause
goto end

:choice4
p4 sync -f //gta5_dlc/mpPacks/mpApartment/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/build/dev_ng/x64/anim/cutscene/...
pause
goto end

:choice5
p4 sync -f //gta5_dlc/mpPacks/mpApartment/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/build/dev_ng/x64/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/build/dev_ng/ps4/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/build/dev_ng/xboxone/anim/cutscene/...
pause
goto end

:choice6
cls
:start3
echo MP_Lowrider
echo Please type in a number, then hit "Enter". 
echo ------------
echo 1 Grab PS4
echo 2 Grab XBOXONE
echo 3 Grab PC
echo 4 Grab All
echo ------------
echo Please select a platform:
set /p choice=
if '%choice%'=='1' goto :choice7
if '%choice%'=='2' goto :choice8
if '%choice%'=='3' goto :choice9
if '%choice%'=='4' goto :choice10
echo "%choice%" is not a valid option. Please try again.
echo
cls
echo You dun goofed, try again.
goto start3

pause
goto end

:choice7
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/ps4/anim/cutscene/...
pause
goto end

:choice8
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/xboxone/anim/cutscene/...
pause
goto end

:choice9
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/x64/anim/cutscene/...
pause
goto end

:choice10
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/x64/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/ps4/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/xboxone/anim/cutscene/...
pause
goto end

:choice11
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/assets_ng/export/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/x64/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/ps4/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/xboxone/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/build/dev_ng/x64/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/build/dev_ng/ps4/anim/cutscene/...
p4 sync -f //gta5_dlc/mpPacks/mpApartment/build/dev_ng/xboxone/anim/cutscene/...
pause

goto end
pause