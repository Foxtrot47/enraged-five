�PNG
from pyfbsdk import *
from pyfbsdk_additions import *
from RS.Utils import Scene
from RS.Utils.Scene import Constraint

def FindAnimationNode( pParent, pName ):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

def plotOptions():
    
    plot_options = FBPlotOptions()
    plot_options.PlotAllTakes = False
    plot_options.PlotOnFrame = True
    plot_options.PlotPeriod = FBTime(0, 0, 0, 1)
    plot_options.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
    plot_options.UseConstantKeyReducer = False
    plot_options.ConstantKeyReducerKeepOneKey = True
    plot_options.PlotTranslationOnRootOnly = False
    
    return plot_options

def RemoveXandYRotationKeys( object ):
    for key in object.Rotation.GetAnimationNode().Nodes[0].FCurve.Keys:
        key.Value = 0.0
    for key in object.Rotation.GetAnimationNode().Nodes[1].FCurve.Keys:
        key.Value = 0.0

def getCharacterHeirarchyList(character):
    characterNodeList = []
    hips = character.GetModel( FBBodyNodeId.kFBHipsNodeId )
    dummy = Scene.GetParent( hips )
    Scene.GetChildren( dummy, characterNodeList, "", True )
    return characterNodeList
    
def clearSelection():
    ''' Clear selection function '''
    # Get selected models
    modelList = FBModelList ()
    FBGetSelectedModels (modelList, None, True)
    
    # Deselect models
    for model in modelList:
        model.Selected = False


currentCharacter = FBApplication().CurrentCharacter
characterNodeList = getCharacterHeirarchyList(currentCharacter)

OH_Facing = None
Skel_Head = None

# Find required nodes for constraint
for node in characterNodeList:
    if node.Name.lower() == 'oh_facingdirection':
        OH_Facing = node
    elif node.Name.lower() == 'skel_head':
        Skel_Head = node

if OH_Facing != None:
    print ("Found OH Facing Node")
    
if Skel_Head != None:
    print ("Found Skel_Head Node")
    
animalAimNull = FBModelNull("Animal_Aim_Null")
animalAimNull.Show = True

animalAimOffsetNull = FBModelNull("Animal_Aim_Offset_Null")
animalAimOffsetNull.Show = True
    
headPositionConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

for i in range(0, headPositionConstraint.ReferenceGroupGetCount()):
        if headPositionConstraint.ReferenceGroupGetName(i) == 'Constrained object (Child)':
            headPositionConstraint.ReferenceAdd(i, animalAimNull )
        elif headPositionConstraint.ReferenceGroupGetName(i) == 'Source (Parent)':
            headPositionConstraint.ReferenceAdd(i, Skel_Head )


headPositionConstraint.Active = True
headPositionConstraint.PropertyList.Find("%s.Offset T" % Skel_Head.LongName).Data = FBVector3d( 5.0, 0.0, 0.0 )



headPositionOffsetXZConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

for i in range(0, headPositionOffsetXZConstraint.ReferenceGroupGetCount()):
        if headPositionOffsetXZConstraint.ReferenceGroupGetName(i) == 'Constrained object (Child)':
            headPositionOffsetXZConstraint.ReferenceAdd(i, animalAimOffsetNull )
        elif headPositionOffsetXZConstraint.ReferenceGroupGetName(i) == 'Source (Parent)':
            headPositionOffsetXZConstraint.ReferenceAdd(i, animalAimNull )

headPositionOffsetXZConstraint.PropertyList.Find("Affect Rotation X").Data = False
headPositionOffsetXZConstraint.PropertyList.Find("Affect Rotation Y").Data = False
headPositionOffsetXZConstraint.PropertyList.Find("Affect Rotation Z").Data = False
headPositionOffsetXZConstraint.PropertyList.Find("Affect Translation Y").Data = False

headPositionOffsetXZConstraint.Active = True



headPositionOffsetYConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

for i in range(0, headPositionOffsetYConstraint.ReferenceGroupGetCount()):
        if headPositionOffsetYConstraint.ReferenceGroupGetName(i) == 'Constrained object (Child)':
            headPositionOffsetYConstraint.ReferenceAdd(i, animalAimOffsetNull )
        elif headPositionOffsetYConstraint.ReferenceGroupGetName(i) == 'Source (Parent)':
            headPositionOffsetYConstraint.ReferenceAdd(i, OH_Facing )

headPositionOffsetYConstraint.PropertyList.Find("Affect Rotation X").Data = False
headPositionOffsetYConstraint.PropertyList.Find("Affect Rotation Y").Data = False
headPositionOffsetYConstraint.PropertyList.Find("Affect Rotation Z").Data = False
headPositionOffsetYConstraint.PropertyList.Find("Affect Translation X").Data = False
headPositionOffsetYConstraint.PropertyList.Find("Affect Translation Z").Data = False

headPositionOffsetYConstraint.Active = True



headAimConstraint = Constraint.CreateConstraint(Constraint.AIM)

for i in range(0, headAimConstraint.ReferenceGroupGetCount()):
        if headAimConstraint.ReferenceGroupGetName(i) == 'Constrained Object':
            headAimConstraint.ReferenceAdd(i, OH_Facing )
        elif headAimConstraint.ReferenceGroupGetName(i) == 'Aim At Object':
            headAimConstraint.ReferenceAdd(i, animalAimOffsetNull )
            
headAimConstraint.Active = True
headAimConstraint.PropertyList.Find("Rotation Offset").Data = FBVector3d( 0.0, -90.0, -90.0 )

#####################################################################################################
# Plot and delete constraint
#####################################################################################################

selected_takes_list = []

# how many takes has user selected?
for iTake in FBSystem().Scene.Takes:
    if ( iTake.Selected == True ):
        selected_takes_list.append(iTake)
        
clearSelection()

# make sure system in on base layer
FBSystem().CurrentTake.SetCurrentLayer(0)    

OH_Facing.Selected = True
    # if user hasn't selected any takes then just use the current one
if len( selected_takes_list ) == 0:
    FBSystem().CurrentTake.PlotTakeOnSelected( plotOptions() )

    # flatten torso contraint by zeroing keys on X and Y axes
    RemoveXandYRotationKeys( OH_Facing )

    
# else plot each selected take
else:
    for take in selected_takes_list:
        FBSystem().CurrentTake = take
        FBSystem().CurrentTake.PlotTakeOnSelected( plotOptions() )
        
        # flatten torso contraint by zeroing keys on X and Y axes
        RemoveXandYRotationKeys( OH_Facing )
        
headAimConstraint.FBDelete()
headPositionOffsetYConstraint.FBDelete()
headPositionConstraint.FBDelete()
headPositionOffsetXZConstraint.FBDelete()
animalAimOffsetNull.FBDelete()
animalAimNull.FBDelete()�PNG
from pyfbsdk import *
from pyfbsdk_additions import *
from RS.Utils import Scene
from RS.Utils.Scene import Constraint

def FindAnimationNode( pParent, pName ):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

def plotOptions():
    
    plot_options = FBPlotOptions()
    plot_options.PlotAllTakes = False
    plot_options.PlotOnFrame = True
    plot_options.PlotPeriod = FBTime(0, 0, 0, 1)
    plot_options.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
    plot_options.UseConstantKeyReducer = False
    plot_options.ConstantKeyReducerKeepOneKey = True
    plot_options.PlotTranslationOnRootOnly = False
    
    return plot_options

def RemoveXandYRotationKeys( object ):
    for key in object.Rotation.GetAnimationNode().Nodes[0].FCurve.Keys:
        key.Value = 0.0
    for key in object.Rotation.GetAnimationNode().Nodes[1].FCurve.Keys:
        key.Value = 0.0

def getCharacterHeirarchyList(character):
    characterNodeList = []
    hips = character.GetModel( FBBodyNodeId.kFBHipsNodeId )
    dummy = Scene.GetParent( hips )
    Scene.GetChildren( dummy, characterNodeList, "", True )
    return characterNodeList
    
def clearSelection():
    ''' Clear selection function '''
    # Get selected models
    modelList = FBModelList ()
    FBGetSelectedModels (modelList, None, True)
    
    # Deselect models
    for model in modelList:
        model.Selected = False


currentCharacter = FBApplication().CurrentCharacter
characterNodeList = getCharacterHeirarchyList(currentCharacter)

OH_Facing = None
Skel_Head = None

# Find required nodes for constraint
for node in characterNodeList:
    if node.Name.lower() == 'oh_facingdirection':
        OH_Facing = node
    elif node.Name.lower() == 'skel_head':
        Skel_Head = node

if OH_Facing != None:
    print ("Found OH Facing Node")
    
if Skel_Head != None:
    print ("Found Skel_Head Node")
    
animalAimNull = FBModelNull("Animal_Aim_Null")
animalAimNull.Show = True

animalAimOffsetNull = FBModelNull("Animal_Aim_Offset_Null")
animalAimOffsetNull.Show = True
    
headPositionConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

for i in range(0, headPositionConstraint.ReferenceGroupGetCount()):
        if headPositionConstraint.ReferenceGroupGetName(i) == 'Constrained object (Child)':
            headPositionConstraint.ReferenceAdd(i, animalAimNull )
        elif headPositionConstraint.ReferenceGroupGetName(i) == 'Source (Parent)':
            headPositionConstraint.ReferenceAdd(i, Skel_Head )


headPositionConstraint.Active = True
headPositionConstraint.PropertyList.Find("%s.Offset T" % Skel_Head.LongName).Data = FBVector3d( 0.0, 5.0, 0.0 )



headPositionOffsetXZConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

for i in range(0, headPositionOffsetXZConstraint.ReferenceGroupGetCount()):
        if headPositionOffsetXZConstraint.ReferenceGroupGetName(i) == 'Constrained object (Child)':
            headPositionOffsetXZConstraint.ReferenceAdd(i, animalAimOffsetNull )
        elif headPositionOffsetXZConstraint.ReferenceGroupGetName(i) == 'Source (Parent)':
            headPositionOffsetXZConstraint.ReferenceAdd(i, animalAimNull )

headPositionOffsetXZConstraint.PropertyList.Find("Affect Rotation X").Data = False
headPositionOffsetXZConstraint.PropertyList.Find("Affect Rotation Y").Data = False
headPositionOffsetXZConstraint.PropertyList.Find("Affect Rotation Z").Data = False
headPositionOffsetXZConstraint.PropertyList.Find("Affect Translation Y").Data = False

headPositionOffsetXZConstraint.Active = True



headPositionOffsetYConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

for i in range(0, headPositionOffsetYConstraint.ReferenceGroupGetCount()):
        if headPositionOffsetYConstraint.ReferenceGroupGetName(i) == 'Constrained object (Child)':
            headPositionOffsetYConstraint.ReferenceAdd(i, animalAimOffsetNull )
        elif headPositionOffsetYConstraint.ReferenceGroupGetName(i) == 'Source (Parent)':
            headPositionOffsetYConstraint.ReferenceAdd(i, OH_Facing )

headPositionOffsetYConstraint.PropertyList.Find("Affect Rotation X").Data = False
headPositionOffsetYConstraint.PropertyList.Find("Affect Rotation Y").Data = False
headPositionOffsetYConstraint.PropertyList.Find("Affect Rotation Z").Data = False
headPositionOffsetYConstraint.PropertyList.Find("Affect Translation X").Data = False
headPositionOffsetYConstraint.PropertyList.Find("Affect Translation Z").Data = False

headPositionOffsetYConstraint.Active = True



headAimConstraint = Constraint.CreateConstraint(Constraint.AIM)

for i in range(0, headAimConstraint.ReferenceGroupGetCount()):
        if headAimConstraint.ReferenceGroupGetName(i) == 'Constrained Object':
            headAimConstraint.ReferenceAdd(i, OH_Facing )
        elif headAimConstraint.ReferenceGroupGetName(i) == 'Aim At Object':
            headAimConstraint.ReferenceAdd(i, animalAimOffsetNull )
            
headAimConstraint.Active = True
headAimConstraint.PropertyList.Find("Rotation Offset").Data = FBVector3d( 0.0, -90.0, -90.0 )

#####################################################################################################
# Plot and delete constraint
#####################################################################################################

selected_takes_list = []

# how many takes has user selected?
for iTake in FBSystem().Scene.Takes:
    if ( iTake.Selected == True ):
        selected_takes_list.append(iTake)
        
clearSelection()

# make sure system in on base layer
FBSystem().CurrentTake.SetCurrentLayer(0)    

OH_Facing.Selected = True
    # if user hasn't selected any takes then just use the current one
if len( selected_takes_list ) == 0:
    FBSystem().CurrentTake.PlotTakeOnSelected( plotOptions() )

    # flatten torso contraint by zeroing keys on X and Y axes
    RemoveXandYRotationKeys( OH_Facing )

    
# else plot each selected take
else:
    for take in selected_takes_list:
        FBSystem().CurrentTake = take
        FBSystem().CurrentTake.PlotTakeOnSelected( plotOptions() )
        
        # flatten torso contraint by zeroing keys on X and Y axes
        RemoveXandYRotationKeys( OH_Facing )
        
headAimConstraint.FBDelete()
headPositionOffsetYConstraint.FBDelete()
headPositionConstraint.FBDelete()
headPositionOffsetXZConstraint.FBDelete()
animalAimOffsetNull.FBDelete()
animalAimNull.FBDelete()from pyfbsdk import *

def HUDDisplay(HUD, event):
    currentTime = FBSystem().LocalTime.GetFrame()
    start = float( FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame() )
    stop = float(FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame() )
    result = float((currentTime - start) / (stop - start))
    actualTime = float(currentTime - start)
    currentSeconds = float(actualTime / 30.00)
    HUDTextElement.Content = ("Phase: %0.3f" % result + " Frame: %0.0f" % actualTime  + " Seconds: %0.3f" % currentSeconds) 

take = FBSystem().CurrentTake 

#Init
Scene = FBSystem().Scene
System = FBSystem()
Application = FBApplication()

#Create HUD with Text Element
HUD = FBHUD("Phase Calculator HUD")
HUDTextElement = FBHUDTextElement("Top Element")
Scene.ConnectSrc(HUD)          # Connect the HUD to the scene
HUDTextElement.Content = "Free memory"
HUDTextElement.X = 0
HUDTextElement.Y = 0
HUDTextElement.Font = "Malgun Gothic"
HUDTextElement.Height = 3
HUDTextElement.Scale = 2.2
HUDTextElement.Justification = FBHUDElementHAlignment.kFBHUDRight
HUDTextElement.HorizontalDock = FBHUDElementHAlignment.kFBHUDRight
HUDTextElement.VerticalDock = FBHUDElementVAlignment.kFBHUDTop
HUD.ConnectSrc(HUDTextElement) # Connect HUDTextElement to the HUD
Scene.Cameras[0].ConnectSrc(HUD) # Connect to Perspective camera

# Register for HUD Display

HUD.OnDisplay.Add(HUDDisplay)�PNG
from pyfbsdk import *
from pyfbsdk_additions import *


def CreateCube(StairZPos, StairYPos, StairHeight, StairWidth, StairDepth, stairOrigin):
    name = 'Stair'
    cube = FBModelCube(name)
    cube.Show = True

    cube.Translation = FBVector3d(0.0, StairYPos, -StairZPos)
    cube.Scaling = FBVector3d(StairWidth, StairHeight, StairDepth)
    cube.Parent = stairOrigin
    return cube


def DeleteBranch(topModel):
    '''
    Deletes the given model and all of its descendants.
    '''
    while len(topModel.Children) > 0:
        DeleteBranch(topModel.Children[-1])

    topModel.FBDelete()


def BtnCallback(control, event):

    existingOrigin = FBFindModelByLabelName("stairOrigin")
    if existingOrigin != None:
        DeleteBranch(existingOrigin)

    stairOrigin = FBModelNull("stairOrigin")
    stairOrigin.Show = True

    stairHeight = t.height.Value / 2
    print stairHeight
    stairWidth = t.width.Value / 2
    print stairWidth
    stairDepth = t.depth.Value / 2
    print stairDepth
    stairCount = t.stair_number.Value
    print stairCount

    StairZPos = stairDepth
    StairYPos = stairHeight
    i = 0


    while i < stairCount:
        CreateCube(StairZPos, StairYPos, stairHeight, stairWidth, stairDepth, stairOrigin)
        StairYPos += (stairHeight * 2)
        StairZPos += (stairDepth * 2)
        i += 1


ui_x_offset = 5
ui_y_offset = 5
ui_x_width = 240
ui_button_height = 30
ui_button_width = 40
ui_column_a = 20
ui_column_b = 110
ui_column_c = 100


t = FBCreateUniqueTool("Led Zeppelins Stairway to Heaven Helper")
t.StartSizeX = ui_x_width + 30
t.StartSizeY = 600

##########################################################################################
# Width Border and Controls
##########################################################################################

ui_y_offset += 10

x = FBAddRegionParam(ui_x_offset, FBAttachType.kFBAttachNone, "")
y = FBAddRegionParam(ui_y_offset, FBAttachType.kFBAttachNone, "")
w = FBAddRegionParam(ui_x_width, FBAttachType.kFBAttachNone, "")
h = FBAddRegionParam(40, FBAttachType.kFBAttachNone, "")
t.AddRegion("width_border", "Width", x, y, w, h)
t.SetBorder("width_border", FBBorderStyle.kFBEmbossBorder, True, True, 2, 0, 90.0, 0)

ui_y_offset += 10

x = FBAddRegionParam(ui_x_offset + 5, FBAttachType.kFBAttachTop, "")
y = FBAddRegionParam(ui_y_offset, FBAttachType.kFBAttachNone, "")
w = FBAddRegionParam(ui_x_width - 10, FBAttachType.kFBAttachNone, "")
h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
t.AddRegion("width_edit", "width_edit", x, y, w, h)

t.width = FBEditNumber()
t.SetControl("width_edit", t.width)
t.width.Visible = True
t.width.ReadOnly = False
t.width.Enabled = True
t.width.Value = 400.0
t.width.Hint = "Width of the stairs"
t.width.Min = 0.0

##########################################################################################
# Height Border and Controls
##########################################################################################

ui_y_offset += 50

x = FBAddRegionParam(ui_x_offset, FBAttachType.kFBAttachNone, "")
y = FBAddRegionParam(ui_y_offset, FBAttachType.kFBAttachNone, "")
w = FBAddRegionParam(ui_x_width, FBAttachType.kFBAttachNone, "")
h = FBAddRegionParam(40, FBAttachType.kFBAttachNone, "")
t.AddRegion("height_border", "Height", x, y, w, h)
t.SetBorder("height_border", FBBorderStyle.kFBEmbossBorder, True, True, 2, 0, 90.0, 0)

ui_y_offset += 10

x = FBAddRegionParam(ui_x_offset + 5, FBAttachType.kFBAttachTop, "")
y = FBAddRegionParam(ui_y_offset, FBAttachType.kFBAttachNone, "")
w = FBAddRegionParam(ui_x_width - 10, FBAttachType.kFBAttachNone, "")
h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
t.AddRegion("height_edit", "height_edit", x, y, w, h)

t.height = FBEditNumber()
t.SetControl("height_edit", t.height)
t.height.Visible = True
t.height.ReadOnly = False
t.height.Enabled = True
t.height.Value = 20.0
t.height.Hint = "Height of the stairs"
t.height.Min = 0.0

##########################################################################################
# Depth Border and Controls
##########################################################################################

ui_y_offset += 50

x = FBAddRegionParam(ui_x_offset, FBAttachType.kFBAttachNone, "")
y = FBAddRegionParam(ui_y_offset, FBAttachType.kFBAttachNone, "")
w = FBAddRegionParam(ui_x_width, FBAttachType.kFBAttachNone, "")
h = FBAddRegionParam(40, FBAttachType.kFBAttachNone, "")
t.AddRegion("depth_border", "Depth", x, y, w, h)
t.SetBorder("depth_border", FBBorderStyle.kFBEmbossBorder, True, True, 2, 0, 90.0, 0)

ui_y_offset += 10

x = FBAddRegionParam(ui_x_offset + 5, FBAttachType.kFBAttachTop, "")
y = FBAddRegionParam(ui_y_offset, FBAttachType.kFBAttachNone, "")
w = FBAddRegionParam(ui_x_width - 10, FBAttachType.kFBAttachNone, "")
h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
t.AddRegion("depth_edit", "depth_edit", x, y, w, h)

t.depth = FBEditNumber()
t.SetControl("depth_edit", t.depth)
t.depth.Visible = True
t.depth.ReadOnly = False
t.depth.Enabled = True
t.depth.Value = 30.0
t.depth.Hint = "Depth of the stairs"
t.depth.Min = 0.0

##########################################################################################
# Stair Number
##########################################################################################

ui_y_offset += 50

x = FBAddRegionParam(ui_x_offset, FBAttachType.kFBAttachNone, "")
y = FBAddRegionParam(ui_y_offset, FBAttachType.kFBAttachNone, "")
w = FBAddRegionParam(ui_x_width, FBAttachType.kFBAttachNone, "")
h = FBAddRegionParam(40, FBAttachType.kFBAttachNone, "")
t.AddRegion("stairs_border", "Stair Amount", x, y, w, h)
t.SetBorder("stairs_border", FBBorderStyle.kFBEmbossBorder, True, True, 2, 0, 90.0, 0)

ui_y_offset += 10

x = FBAddRegionParam(ui_x_offset + 5, FBAttachType.kFBAttachTop, "")
y = FBAddRegionParam(ui_y_offset, FBAttachType.kFBAttachNone, "")
w = FBAddRegionParam(ui_x_width - 10, FBAttachType.kFBAttachNone, "")
h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
t.AddRegion("stairs_edit", "stairs_edit", x, y, w, h)

t.stair_number = FBEditNumber()
t.SetControl("stairs_edit", t.stair_number)
t.stair_number.Visible = True
t.stair_number.ReadOnly = False
t.stair_number.Enabled = True
t.stair_number.Value = 30.0
t.stair_number.Hint = "Depth of the stairs"
t.stair_number.Min = 0.0

##########################################################################################
# Create Stairs Button
##########################################################################################
ui_y_offset += 40


# Pin button
x = FBAddRegionParam(ui_x_offset, FBAttachType.kFBAttachTop, "")
y = FBAddRegionParam(ui_y_offset, FBAttachType.kFBAttachNone, "")
w = FBAddRegionParam(ui_x_width, FBAttachType.kFBAttachNone, "")
h = FBAddRegionParam(ui_button_height, FBAttachType.kFBAttachNone, "")
t.AddRegion("create_stairs_region", "create_stairs_region", x, y, w, h)

t.create_stairs_button = FBButton()

t.SetControl("create_stairs_region", t.create_stairs_button)
t.create_stairs_button.Visible = True
t.create_stairs_button.ReadOnly = False
t.create_stairs_button.Enabled = True
t.create_stairs_button.Hint = ""


t.create_stairs_button.Caption = "Create Stairs"


t.create_stairs_button.State = 0
t.create_stairs_button.Style = FBButtonStyle.kFBPushButton
t.create_stairs_button.Justify = FBTextJustify.kFBTextJustifyCenter
t.create_stairs_button.Look = FBButtonLook.kFBLookNormal
t.create_stairs_button.OnClick.Add(BtnCallback)

t.StartSizeX = 267
t.StartSizeY = ui_y_offset + 75

ShowTool(t)from pyfbsdk import *

DupeTake = FBSystem().CurrentTake.CopyTake

def getNumberOfSelectedTakes():
    number_of_selected_takes = 0
    
    for take_index in range( len( FBSystem().Scene.Takes ) ):
        if FBSystem().Scene.Takes[ take_index ].Selected == True:
            number_of_selected_takes += 1
      
    return number_of_selected_takes

number_of_selected_takes = getNumberOfSelectedTakes()

if number_of_selected_takes > 1:
        for take_index in range( len( FBSystem().Scene.Takes ) ):
            if FBSystem().Scene.Takes[ take_index ].Selected == True:
                # go to take
                FBSystem().CurrentTake = FBSystem().Scene.Takes[ take_index ]
                CurrentTakeName = FBSystem().CurrentTake.Name  
                new = DupeTake(CurrentTakeName + "_TURN_L1")
                new = DupeTake(CurrentTakeName + "_TURN_L2")
                new = DupeTake(CurrentTakeName + "_TURN_R1")
                new = DupeTake(CurrentTakeName + "_TURN_R2")

else:
    CurrentTakeName = FBSystem().CurrentTake.Name  
    new = DupeTake(CurrentTakeName + "_TURN_L1")
    new = DupeTake(CurrentTakeName + "_TURN_L2")
    new = DupeTake(CurrentTakeName + "_TURN_R1")
    new = DupeTake(CurrentTakeName + "_TURN_R2")



�PNG
from pyfbsdk import *
from pyfbsdk_additions import *

region1 = FBLabel()
editStart = FBEditNumber()
buttonStart = FBButton()
region4 = FBLabel()
editStop = FBEditNumber()
buttonStop = FBButton()
buttonTimeline = FBButton()
buttonAction = FBButton()
listAction = FBList()
region9 = FBLabel()
editBlendTime = FBEditNumber()

def PopulateTool(t):
    #populate regions here

    x = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    t.AddRegion("region1","region1", x, y, w, h)

    t.SetControl("region1", region1)
    region1.Visible = True
    region1.ReadOnly = False
    region1.Enabled = True
    region1.Hint = ""
    region1.Caption = "Start"
    region1.Style = FBTextStyle.kFBTextStyleNone
    region1.Justify = FBTextJustify.kFBTextJustifyLeft
    region1.WordWrap = True
    
    x = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(45,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    t.AddRegion("editStart","editStart", x, y, w, h)

    t.SetControl("editStart", editStart)
    editStart.Visible = True
    editStart.ReadOnly = False
    editStart.Enabled = True
    editStart.Hint = ""
    editStart.Value = 0.000000
    editStart.Min = 0.000000
    editStart.Max = 0.000000
    editStart.Precision = 0.000000
    editStart.LargeStep = 5.000000
    editStart.SmallStep = 1.000000
    
    x = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(75,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    t.AddRegion("buttonStart","buttonStart", x, y, w, h)

    t.SetControl("buttonStart", buttonStart)
    buttonStart.Visible = True
    buttonStart.ReadOnly = False
    buttonStart.Enabled = True
    buttonStart.Hint = ""
    buttonStart.Caption = "Assign Start"
    buttonStart.State = 0
    buttonStart.Style = FBButtonStyle.kFBPushButton
    buttonStart.Justify = FBTextJustify.kFBTextJustifyLeft
    buttonStart.Look = FBButtonLook.kFBLookNormal
    buttonStart.OnClick.Add(ButtonStartEvent)
    
    x = FBAddRegionParam(110,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    t.AddRegion("region4","region4", x, y, w, h)

    t.SetControl("region4", region4)
    region4.Visible = True
    region4.ReadOnly = False
    region4.Enabled = True
    region4.Hint = ""
    region4.Caption = "Stop"
    region4.Style = FBTextStyle.kFBTextStyleNone
    region4.Justify = FBTextJustify.kFBTextJustifyLeft
    region4.WordWrap = True
    
    x = FBAddRegionParam(110,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(45,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    t.AddRegion("editStop","editStop", x, y, w, h)

    t.SetControl("editStop", editStop)
    editStop.Visible = True
    editStop.ReadOnly = False
    editStop.Enabled = True
    editStop.Hint = ""
    editStop.Value = 0.000000
    editStop.Min = 0.000000
    editStop.Max = 0.000000
    editStop.Precision = 0.000000
    editStop.LargeStep = 5.000000
    editStop.SmallStep = 1.000000
    
    x = FBAddRegionParam(110,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(75,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    t.AddRegion("buttonStop","buttonStop", x, y, w, h)

    t.SetControl("buttonStop", buttonStop)
    buttonStop.Visible = True
    buttonStop.ReadOnly = False
    buttonStop.Enabled = True
    buttonStop.Hint = ""
    buttonStop.Caption = "Assign Stop"
    buttonStop.State = 0
    buttonStop.Style = FBButtonStyle.kFBPushButton
    buttonStop.Justify = FBTextJustify.kFBTextJustifyLeft
    buttonStop.Look = FBButtonLook.kFBLookNormal
    buttonStop.OnClick.Add(ButtonStopEvent)
    
    x = FBAddRegionParam(205,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(75,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    t.AddRegion("buttonTimeline","buttonTimeline", x, y, w, h)

    t.SetControl("buttonTimeline", buttonTimeline)
    buttonTimeline.Visible = True
    buttonTimeline.ReadOnly = False
    buttonTimeline.Enabled = True
    buttonTimeline.Hint = ""
    buttonTimeline.Caption = "Timeline range"
    buttonTimeline.State = 0
    buttonTimeline.Style = FBButtonStyle.kFBPushButton
    buttonTimeline.Justify = FBTextJustify.kFBTextJustifyLeft
    buttonTimeline.Look = FBButtonLook.kFBLookNormal
    buttonTimeline.OnClick.Add(ButtonTimelineEvent)
    
    x = FBAddRegionParam(45,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(120,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(110,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")
    t.AddRegion("buttonAction","buttonAction", x, y, w, h)

    t.SetControl("buttonAction", buttonAction)
    buttonAction.Visible = True
    buttonAction.ReadOnly = False
    buttonAction.Enabled = True
    buttonAction.Hint = ""
    buttonAction.Caption = "Do it!"
    buttonAction.State = 0
    buttonAction.Style = FBButtonStyle.kFBPushButton
    buttonAction.Justify = FBTextJustify.kFBTextJustifyLeft
    buttonAction.Look = FBButtonLook.kFBLookNormal
    buttonAction.OnClick.Add(ButtonActionEvent)
    
    x = FBAddRegionParam(175,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(120,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(110,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")
    t.AddRegion("listAction","listAction", x, y, w, h)

    t.SetControl("listAction", listAction)
    listAction.Style = FBListStyle.kFBDropDownList
    listAction.Items.append('T')
    listAction.Items.append('R')
    listAction.Items.append('TR')
    listAction.Items.append('S')
    listAction.Items.append('TRS')
    listAction.ItemIndex = 2
    
    x = FBAddRegionParam(205,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    t.AddRegion("region9","region9", x, y, w, h)

    t.SetControl("region9", region9)
    region9.Visible = True
    region9.ReadOnly = False
    region9.Enabled = True
    region9.Hint = ""
    region9.Caption = "Blend Time"
    region9.Style = FBTextStyle.kFBTextStyleNone
    region9.Justify = FBTextJustify.kFBTextJustifyLeft
    region9.WordWrap = True
    
    x = FBAddRegionParam(205,FBAttachType.kFBAttachNone,"")
    y = FBAddRegionParam(45,FBAttachType.kFBAttachNone,"")
    w = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    t.AddRegion("editBlendTime","editBlendTime", x, y, w, h)

    t.SetControl("editBlendTime", editBlendTime)
    editBlendTime.Visible = True
    editBlendTime.ReadOnly = False
    editBlendTime.Enabled = True
    editBlendTime.Hint = ""
    editBlendTime.Value = 3.000000
    editBlendTime.Min = 0.000000
    editBlendTime.Max = 100.000000
    editBlendTime.Precision = 0.000000
    editBlendTime.LargeStep = 1.000000
    editBlendTime.SmallStep = 0.100000
    
def ButtonStartEvent(control, event):
    time = FBSystem().LocalTime
    frame = time.GetFrame()
    editStart.Value = frame
    
def ButtonStopEvent(control, event):
    time = FBSystem().LocalTime
    frame = time.GetFrame()
    editStop.Value = frame        
    
def ButtonTimelineEvent(control, event):
    btime = FBPlayerControl().LoopStart
    etime = FBPlayerControl().LoopStop
    editStart.Value = btime.GetFrame()
    editStop.Value = etime.GetFrame()
    
def IsTimeInside(timespan, time):
    if (time > timespan.GetStart() and time < timespan.GetStop() ): return True
    return False
    
def ProcessFCurve(curve, currTime, startTime, stopTime, blendTime):
    value = curve.Evaluate(currTime)
    count = len(curve.Keys)
    
    startTimeBlend = startTime - blendTime
    stopTimeBlend = stopTime + blendTime
    
    if curve: curve.KeyDeleteByTimeRange(startTimeBlend, stopTimeBlend, True)
    
    startNdx = curve.KeyAdd(startTime, value)
    stopNdx = curve.KeyAdd(stopTime, value)
    
    if startNdx >= 0: 
        curve.Keys[startNdx].TangentMode = FBTangentMode.kFBTangentModeUser
        curve.Keys[startNdx].LeftDerivative = 0.0
        curve.Keys[startNdx].RightDerivative = 0.0

    if stopNdx >= 0: 
        curve.Keys[stopNdx].TangentMode = FBTangentMode.kFBTangentModeUser   
        curve.Keys[stopNdx].LeftDerivative = 0.0
        curve.Keys[stopNdx].RightDerivative = 0.0
    
def ProcessAnimationNode(animNode, currTime, startTime, stopTime, blendTime):
    for node in animNode.Nodes:
        if node.FCurve:
            ProcessFCurve(node.FCurve, currTime, startTime, stopTime, blendTime)
    
def ButtonActionEvent(control, event):
    currTime = FBSystem().LocalTime
    startTime = FBTime(0,0,0, int(editStart.Value) )
    stopTime = FBTime(0,0,0, int(editStop.Value) )
    blendTime = FBTime(0,0,0, int(editBlendTime.Value) )
    
    
    models = FBModelList()
    FBGetSelectedModels(models)
    
    mode = listAction.ItemIndex
    
    for model in models:
        if mode in [0,2,4]:
            animNode = model.Translation.GetAnimationNode()
            if animNode: ProcessAnimationNode(animNode, currTime, startTime, stopTime, blendTime)
        if mode in [1,2,4]:
            animNode = model.Rotation.GetAnimationNode()
            if animNode: ProcessAnimationNode(animNode, currTime, startTime, stopTime, blendTime)
        if mode in [3,4]:
            animNode = model.Scaling.GetAnimationNode()
            if animNode: ProcessAnimationNode(animNode, currTime, startTime, stopTime, blendTime)
    
def CreateTool():
    t = FBCreateUniqueTool("Foot Fix")
    t.StartSizeX = 325
    t.StartSizeY = 200
    PopulateTool(t)
    ShowTool(t)
CreateTool()
�PNG
from pyfbsdk import *
from RS.Utils import Scene


#Create a relation constraint called SaddleConstraint
lConstraintRelation = FBConstraintRelation( 'SaddleConstraint' )

lVectorToNumberBoxMover = lConstraintRelation.CreateFunctionBox( 'Converters', 'Vector to Number' )
lConstraintRelation.SetBoxPosition( lVectorToNumberBoxMover, 400, 30 )
lVectorToNumberBoxSaddle = lConstraintRelation.CreateFunctionBox( 'Converters', 'Vector to Number' )
lConstraintRelation.SetBoxPosition( lVectorToNumberBoxSaddle, 400, 230 )
lNumberToVectorBox = lConstraintRelation.CreateFunctionBox( 'Converters', 'Number to Vector' )
lConstraintRelation.SetBoxPosition( lNumberToVectorBox, 700, 130 )

def ConnectBox(nodeOut, nodeOutPos, nodeIn, nodeInPos):
    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
    mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )
    if mynodeOut and mynodeIn:
        FBConnect(mynodeOut, mynodeIn)

def FindAnimationNode( pParent, pName ):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

#Get Mover

def getCharacterHeirarchyList(character):
    characterNodeList = []
    hips = character.GetModel( FBBodyNodeId.kFBHipsNodeId )
    dummy = Scene.GetParent( hips )
    Scene.GetChildren( dummy, characterNodeList, "", True )
    return characterNodeList
 
currentCharacter = FBApplication().CurrentCharacter
characterNodeList = getCharacterHeirarchyList(currentCharacter)
    
mover = None
saddle = None
for node in characterNodeList:
    if node.Name.lower() == 'mover':
        mover = node
    elif node.Name.lower() == 'skel_saddle':
        saddle = node
        
if mover != None and saddle != None:
    SaddleMoverSender = lConstraintRelation.SetAsSource(mover)
    lConstraintRelation.SetBoxPosition( SaddleMoverSender, 100, 30 )
    SaddleMoverReceiver = lConstraintRelation.ConstrainObject(mover)
    lConstraintRelation.SetBoxPosition( SaddleMoverReceiver, 1000, 30 )
    SaddleRootSender = lConstraintRelation.SetAsSource(saddle)
    lConstraintRelation.SetBoxPosition( SaddleRootSender, 100, 230 )

    # Connect Constraints
    ConnectBox(SaddleRootSender, "Translation", lVectorToNumberBoxSaddle, 'V')
    ConnectBox(SaddleMoverSender, "Translation", lVectorToNumberBoxMover, 'V')
    ConnectBox(lVectorToNumberBoxMover, "Y", lNumberToVectorBox, 'Y')
    ConnectBox(lVectorToNumberBoxSaddle, "X", lNumberToVectorBox, 'X')
    ConnectBox(lVectorToNumberBoxSaddle, "Z", lNumberToVectorBox, 'Z')
    ConnectBox(lNumberToVectorBox, "Result", SaddleMoverReceiver, 'Translation')

    # Finally activate the constraint.
    lConstraintRelation.Active = Truefrom pyfbsdk import *

def GetParent(model):
    parentModel = model.Parent
    if parentModel: 
        parentModel = GetParent(parentModel)
    else: 
        parentModel = model

    
    return parentModel

def MoveKeysToZero(time, node):
    fcurve = node.FCurve
    if fcurve:
        print len(fcurve.Keys)
        for key in fcurve.Keys:
            key.Time -= time
    
    for child in node.Nodes:
        MoveKeysToZero(time, child)


def SelectHierarchy(time, model):
    print model.Name
    MoveKeysToZero(time, model.AnimationNode)
    for child in model.Children:
        SelectHierarchy(time, child)

playerControl = FBPlayerControl()
startTime = playerControl.LoopStart
endTime = playerControl.LoopStop
zeroTime = FBTime(0)
print startTime
print zeroTime
IsZero = startTime != zeroTime
print IsZero
currChar = FBApplication().CurrentCharacter
if currChar and (startTime != zeroTime):
    hipsModel = currChar.GetModel(FBBodyNodeId.kFBHipsNodeId)
    if hipsModel:
        print hipsModel.Name
        parentModel = GetParent(hipsModel)
        if parentModel:
            SelectHierarchy(startTime, parentModel)
            
            
    playerControl.LoopStart = zeroTime
    playerControl.LoopStop = endTime - startTime
�PNG
from pyfbsdk import *
from RS.Utils import Scene


lConstraintRelation = FBConstraintRelation( 'OH_AutoConstraint' )

lVectorToNumberBoxHead = lConstraintRelation.CreateFunctionBox( 'Converters', 'Vector to Number' )
lConstraintRelation.SetBoxPosition( lVectorToNumberBoxHead, 400, 30 )

lVectorToNumberBoxShoulders = lConstraintRelation.CreateFunctionBox( 'Converters', 'Vector to Number' )
lConstraintRelation.SetBoxPosition( lVectorToNumberBoxShoulders, 400, 230 )

lNumberToVectorBoxHead = lConstraintRelation.CreateFunctionBox( 'Converters', 'Number to Vector' )
lConstraintRelation.SetBoxPosition( lNumberToVectorBoxHead, 1000, 30 )

lNumberToVectorBoxShoulders = lConstraintRelation.CreateFunctionBox( 'Converters', 'Number to Vector' )
lConstraintRelation.SetBoxPosition( lNumberToVectorBoxShoulders, 1000, 230 )

lNumberAddBoxHead = lConstraintRelation.CreateFunctionBox( 'Number', 'Add (a + b)' )
lConstraintRelation.SetBoxPosition( lNumberAddBoxHead, 700, 30 )

lNumberAddBoxShoulders = lConstraintRelation.CreateFunctionBox( 'Number', 'Add (a + b)' )
lConstraintRelation.SetBoxPosition( lNumberAddBoxShoulders, 700, 230 )

def ConnectBox(nodeOut, nodeOutPos, nodeIn, nodeInPos):
    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
    mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )
    if mynodeOut and mynodeIn:
        FBConnect(mynodeOut, mynodeIn)
        
def SetBoxInput(nodeIn, nodeInPos):
    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
    mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )
    if mynodeOut and mynodeIn:
        FBConnect(mynodeOut, mynodeIn)

def FindAnimationNode( pParent, pName ):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

def getCharacterHeirarchyList(character):
    characterNodeList = []
    hips = character.GetModel( FBBodyNodeId.kFBHipsNodeId )
    dummy = Scene.GetParent( hips )
    Scene.GetChildren( dummy, characterNodeList, "", True )
    return characterNodeList
 
currentCharacter = FBApplication().CurrentCharacter
characterNodeList = getCharacterHeirarchyList(currentCharacter)

OH_Facing = None
OH_MoverFixup = None
Skel_Head = None
Skel_Shoulders = None

# Find required nodes for constraint
for node in characterNodeList:
    if node.Name.lower() == 'oh_upperfixupdirection':
        OH_MoverFixup = node
    elif node.Name.lower() == 'oh_facingdirection':
        OH_Facing = node
    elif node.Name.lower() == 'skel_head':
        Skel_Head = node
    elif node.Name.lower() == 'skel_spine5':
        Skel_Shoulders = node

# Add nodes to constraint
if OH_MoverFixup != None:
    OH_Facing_Reciever = lConstraintRelation.ConstrainObject(OH_Facing)
    lConstraintRelation.SetBoxPosition( OH_Facing_Reciever, 1300, 30 )
    
if OH_Facing != None:
    OH_MoverFixup_Receiver = lConstraintRelation.ConstrainObject(OH_MoverFixup)
    lConstraintRelation.SetBoxPosition( OH_MoverFixup_Receiver, 1300, 230 )
    
if Skel_Head != None:
    Skel_Head_Sender = lConstraintRelation.SetAsSource(Skel_Head)
    lConstraintRelation.SetBoxPosition( Skel_Head_Sender, 100, 30 )

if Skel_Shoulders != None:
    Skel_Shoulders_Sender = lConstraintRelation.SetAsSource(Skel_Shoulders)
    lConstraintRelation.SetBoxPosition( Skel_Shoulders_Sender, 100, 230 )

# Connect Head Facing Constraints
ConnectBox(Skel_Head_Sender, "Rotation", lVectorToNumberBoxHead, 'V')
ConnectBox(lVectorToNumberBoxHead, "X", lNumberAddBoxHead, 'a')
ConnectBox(lNumberAddBoxHead, "Result", lNumberToVectorBoxHead, 'Y')
ConnectBox(lNumberToVectorBoxHead, "Result", OH_Facing_Reciever, 'Rotation')
# Add Head Offsets
lAddHeadInB = FindAnimationNode( lNumberAddBoxHead.AnimationNodeInGet(), 'b' )
lAddHeadInB.WriteData( [90] )
lVectorHeadInX = FindAnimationNode( lNumberToVectorBoxHead.AnimationNodeInGet(), 'X' )
lVectorHeadInX.WriteData( [-90] )

# Connect Shoulder Facing Constraints
ConnectBox(Skel_Shoulders_Sender, "Rotation", lVectorToNumberBoxShoulders, 'V')
ConnectBox(lVectorToNumberBoxShoulders, "X", lNumberAddBoxShoulders, 'a')
lAddShoulderInB = FindAnimationNode( lNumberAddBoxShoulders.AnimationNodeInGet(), 'b' )
ConnectBox(lNumberToVectorBoxShoulders, "Result", OH_MoverFixup_Receiver, 'Rotation')
# Add Shoulder Upperfixup Offsets
lAddShoulderInB.WriteData( [90] )
ConnectBox(lNumberAddBoxShoulders, "Result", lNumberToVectorBoxShoulders, 'Y')
lVectorShoulderInX = FindAnimationNode( lNumberToVectorBoxShoulders.AnimationNodeInGet(), 'X' )
lVectorShoulderInX.WriteData( [-90] )

# Activate the constraint.
lConstraintRelation.Active = True

###################################
#PLOTTING
###################################

# Set options for Plot process
lOptions = FBPlotOptions()
lOptions.ConstantKeyReducerKeepOneKey = True
lOptions.PlotAllTakes = True
lOptions.PlotOnFrame = True
lOptions.PlotPeriod = FBTime( 0, 0, 0, 1 )
lOptions.PlotTranslationOnRootOnly = True
lOptions.PreciseTimeDiscontinuities = True
lOptions.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
lOptions.UseConstantKeyReducer = False

lCharacter = FBApplication().CurrentCharacter
lCharacter.PlotAnimation (FBCharacterPlotWhere.kFBCharacterPlotOnControlRig,lOptions )

# Deactivate Relation Cosntraint
lConstraintRelation.Active = False
    
OH_MoverFixup.Selected = True
OH_Facing.Selected = True

# Find a given model in the scene.
def facingFilter():
    lFilterFacing = FBFilterManager().CreateFilter( 'Key Reducing' )
    lFilterFacing.PropertyList.Find( 'Precision' ).Data = 4.0
    lFilterFacing.Apply( OH_Facing.Rotation.GetAnimationNode(), True )
    del(lFilterFacing)  
    
def upperFilter():
    lFilterUpper = FBFilterManager().CreateFilter( 'Key Reducing' )
    lFilterUpper.PropertyList.Find( 'Precision' ).Data = 4.0
    lFilterUpper.Apply( OH_MoverFixup.Rotation.GetAnimationNode(), True )
    del(lFilterUpper)  

allTakes = FBSystem().Scene.Takes 


for take in allTakes:
    FBSystem().CurrentTake = take
    upperFilter()
    facingFilter()

# Cleanup.
lConstraints = FBSystem().Scene.Constraints
for aConstraint in lConstraints:
    if aConstraint.Name == 'OH_AutoConstraint':
        aConstraint.FBDelete()

�PNG
�PNG
from pyfbsdk import *
from pyfbsdk_additions import *
from RS.Utils import Scene

def clearSelection():
    ''' Clear selection function '''
    # Get selected models
    modelList = FBModelList ()
    FBGetSelectedModels (modelList, None, True)
    
    # Deselect models
    for model in modelList:
        model.Selected = False
        
def getCharacterHeirarchyList(character):
    characterNodeList = []
    hips = character.GetModel( FBBodyNodeId.kFBHipsNodeId )
    dummy = Scene.GetParent( hips )
    Scene.GetChildren( dummy, characterNodeList, "", True )
    return characterNodeList
    
def getSceneObject( name, characterNodeList ):
    
    for node in characterNodeList:
        if node.Name.lower() == name:
            return node
    
    # Error node not found in scene!
    FBMessageBox( "Error", "Error: '" + name + "' object not found in scene!", "OK" )
    raise Exception( "Error: " + name + " not found in scene!" )
    
currentCharacter = FBApplication().CurrentCharacter
characterNodeList = getCharacterHeirarchyList(currentCharacter)
    
# Find Camera nodes

cameraNode = getSceneObject( 'camerabasepivotoffset', characterNodeList)
cameraStartNode = getSceneObject( 'camerabasepivotoffsetstart', characterNodeList)
cameraEndNode = getSceneObject( 'camerabasepivotoffsetend', characterNodeList)

def ClearAnim( pNode ):
    if pNode.FCurve:
        pNode.FCurve.EditClear()
    else:
        for lNode in pNode.Nodes:
            ClearAnim( lNode )
            del( lNode )
            
def cameraToolStart():
    
    ClearAnim( cameraStartNode.AnimationNode )
    ClearAnim( cameraEndNode.AnimationNode )  
    
    # Setup CameraStartPivot Offset keys
    
    lPlayerControl = FBPlayerControl()
    lPlayerControl.GotoStart()
    
    cameraNodelTranslation = cameraNode.PropertyList.Find( 'Lcl Translation' ).Data
    cameraStartNode.PropertyList.Find( 'Lcl Translation' ).Data = cameraNodelTranslation
    cameraNodelRotation = cameraNode.PropertyList.Find( 'Lcl Rotation' ).Data
    cameraStartNode.PropertyList.Find( 'Lcl Rotation' ).Data = cameraNodelRotation
    
    keyCameraStartNodelTranslation = cameraStartNode.PropertyList.Find( 'Lcl Translation' )
    keyCameraStartNodelRotation = cameraStartNode.PropertyList.Find( 'Lcl Rotation' )
    keyCameraStartNodelTranslation.Key()
    keyCameraStartNodelRotation.Key()
    
    # Setup CameraStartPivot Offset keys
    lPlayerControl = FBPlayerControl()
    lPlayerControl.GotoEnd()
    
    cameraNodelTranslation = cameraNode.PropertyList.Find( 'Lcl Translation' ).Data
    cameraEndNode.PropertyList.Find( 'Lcl Translation' ).Data = cameraNodelTranslation
    cameraNodelRotation = cameraNode.PropertyList.Find( 'Lcl Rotation' ).Data
    cameraEndNode.PropertyList.Find( 'Lcl Rotation' ).Data = cameraNodelRotation
    
    keyCameraEndNodelTranslation = cameraEndNode.PropertyList.Find( 'Lcl Translation' )
    keyCameraEndNodelRotation = cameraEndNode.PropertyList.Find( 'Lcl Rotation' )
    keyCameraEndNodelTranslation.Key()
    keyCameraEndNodelRotation.Key()
    
    # Plot Camera Nodes
    
    cameraEndNode.Selected = True
    cameraStartNode.Selected = True
    
    lOptions = FBPlotOptions()
    lOptions.PlotOnFrame = True
    lOptions.UseConstantKeyReducer = False
    lOptions.ConstantKeyReducerKeepOneKey = True
    lOptions.PlotLockedProperties = False
    lSystem = FBSystem()
    lSystem.CurrentTake.PlotTakeOnSelected( lOptions )
    



selected_takes_list = []

# how many takes has user selected?
for iTake in FBSystem().Scene.Takes:
    if ( iTake.Selected == True ):
        selected_takes_list.append(iTake)
        
if len( selected_takes_list ) == 0:
    cameraToolStart()

    
# else plot each selected take
else:
    for take in selected_takes_list:
        FBSystem().CurrentTake = take
        cameraToolStart()
    
�PNG
from pyfbsdk import *
from RS.Utils import Scene

def FindAnimationNode( pParent, pName ):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

def getCharacterHeirarchyList(character):
    characterNodeList = []
    hips = character.GetModel( FBBodyNodeId.kFBHipsNodeId )
    dummy = Scene.GetParent( hips )
    Scene.GetChildren( dummy, characterNodeList, "", True )
    return characterNodeList

 
currentCharacter = FBApplication().CurrentCharacter
characterNodeList = getCharacterHeirarchyList(currentCharacter)
    
mover = None
moveroffset = None
independentmoverdirection = None
upperfixupdirection = None
facingdirection = None
moveroffset = None
torsodirection = None

for node in characterNodeList:
    if node.Name.lower() == 'mover':
        mover = node
    elif node.Name.lower() == 'oh_moveroffset':
        moveroffset = node
    elif node.Name.lower() == 'oh_independentmoverdirection':
        independentmoverdirection = node
    elif node.Name.lower() == 'oh_upperfixupdirection':
        upperfixupdirection = node
    elif node.Name.lower() == 'oh_torsodirection':
        torsodirection = node
    elif node.Name.lower() == 'oh_facingdirection':
        facingdirection = node
    elif node.Name.lower() == 'oh_moveroffset':
        moveroffset = node

print mover
print moveroffset
print independentmoverdirection
print upperfixupdirection
print facingdirection
print moveroffset
print torsodirection

moverVisibility = mover.Show

if mover != None:
    mover.Show = not moverVisibility

if moveroffset != None:
    moveroffset.Show = not moverVisibility

if independentmoverdirection != None:
    independentmoverdirection.Show = not moverVisibility
    
if upperfixupdirection != None:
    upperfixupdirection.Show = not moverVisibility
    
if facingdirection != None:
    facingdirection.Show = not moverVisibility
    
if moveroffset != None:
    moveroffset.Show = not moverVisibility
    
if torsodirection != None:
    torsodirection.Show = not moverVisibility
�PNG
