import inspect

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Globals as glo
import RS.Utils.Creation as cre
import RS.Tools.MoverSnap as snap
import RS.Tools.UI


class snarkReset( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Reset Shark', size = [ 200, 106 ] )

##############

    def rs_SetSharkToDefaultPose( self, control, event ):
        
        print "A"
        propBoneList = [ #LIST OF THE ACTUAL JOINTS
        #"P_tigershark_S_Dummy",
        #"P_tigershark_S_Mover",
        #"P_tigershark_S_Root",
        "P_tigershark_S_Main",
        "P_tigershark_S_tailEnd",
        "P_tigershark_S_tail5",
        "P_tigershark_S_tail4",
        "P_tigershark_S_tail3",
        "P_tigershark_S_tail2",
        "P_tigershark_S_DorsalFin",
        "P_tigershark_S_tail1",
        "P_tigershark_S_spine1",
        "P_tigershark_S_spine2",
        "P_tigershark_S_spine3",
        "P_tigershark_S_head",
        "P_tigershark_S_jaw",
        "P_tigershark_S_L_finFront",
        "P_tigershark_S_R_finFront",
        "P_tigershark_S_L_finRear",
        "P_tigershark_S_R_finRear",
        "P_tigershark_S_TailFinBottom",
        "P_tigershark_S_TailFinTop"]
        
        propBonePosition = [
        #"P_tigershark_S_Dummy",
        #"P_tigershark_S_Mover",
        #"P_tigershark_S_Root",
        (0,37.23,0),         #"P_tigershark_S_Main",
        (0,37.23,-153.55),   #"P_tigershark_S_tailEnd",
        (0,37.23,-121.79),   #"P_tigershark_S_tail5",
        (0,37.23,-92.61),    #"P_tigershark_S_tail4",
        (0,37.23,-63.07),    #"P_tigershark_S_tail3",
        (0,37.23,-37.6),     #"P_tigershark_S_tail2",
        (0,65.5,-32.59),     #"P_tigershark_S_DorsalFin",
        (0,37.23,-20.18),    #"P_tigershark_S_tail1",
        (0,37.23,-2.37),     #"P_tigershark_S_spine1",
        (0,37.23,7.06),      #"P_tigershark_S_spine2",
        (0,37.23,20.74),     #"P_tigershark_S_spine3",
        (0,37.23,35.51),     #"P_tigershark_S_head",
        (0,28.52,37.05),     #"P_tigershark_S_jaw",
        (24.84,16.13,2.5),   #"P_tigershark_S_L_finFront",
        (-24.68,16.13,2.5),  #"P_tigershark_S_R_finFront",
        (12.42,25.4,-99.26), #"P_tigershark_S_L_finRear",
        (-12.36,25.4,-99.24),#"P_tigershark_S_R_finRear",
        (0,34.82,-155.20),   #"P_tigershark_S_TailFinBottom",
        (0,41.43,-155.20)    #"P_tigershark_S_TailFinTop"    
        ]
        
        print "B"
        
        for i in range(len(propBoneList)):
            lControl = FBFindModelByLabelName(propBoneList[i]+ '_Control')                
            lControl.Translation = FBVector3d(propBonePosition[i])
            lControl.Rotation = FBVector3d(0,0,0)        
            print str(i)
    
        print "C"

    def Create( self, mainLayout ):
        

            
        # Setup layout region.
        x = FBAddRegionParam( 6, FBAttachType.kFBAttachLeft, "" )
        y = FBAddRegionParam( 30, FBAttachType.kFBAttachTop, "" )
        w = FBAddRegionParam( -6, FBAttachType.kFBAttachRight, "" )
        h = FBAddRegionParam( -6, FBAttachType.kFBAttachBottom, "" )
        mainLayout.AddRegion( "main", "main", x, y, w, h )
        boxLayout = FBVBoxLayout()
        mainLayout.SetControl( "main", boxLayout )
        btnEnable = FBButton()
        btnEnable.Caption = "Reset Shark pose"
        btnEnable.Style = FBButtonStyle.kFBPushButton
        btnEnable.OnClick.Add(self.rs_SetSharkToDefaultPose)
        boxLayout.Add( btnEnable, 30 )  
        
        print "D" 
#################### 
            
def Run( show = True ):
    tool = snarkReset()
    
    if show:
        tool.Show()
        
    return tool
    
Run(True)