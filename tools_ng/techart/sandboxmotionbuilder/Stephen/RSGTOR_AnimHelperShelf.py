from pyfbsdk import *
from pyfbsdk_additions import *

import os, sys

import RS.Config

# FILE FUNCTIONS
def copyTakeCallback(control, event):
    FBApplication().ExecuteScript(r"x:\gta5\tools_ng\techart\sandboxmotionbuilder\Stephen\RSGTOR_CopyTake.py")   
    
def plotToSkeletonCallback(control, event):
	FBApplication().ExecuteScript(r"x:\gta5\tools_ng\techart\sandboxmotionbuilder\Stephen\RSGTOR_PlotToSkeleton.py")
        
def plotToRigCallback(control, event):
	FBApplication().ExecuteScript(r"x:\gta5\tools_ng\techart\sandboxmotionbuilder\Stephen\RSGTOR_PlotToControlRig.py")    
	
def greyBackGroundCallback(control, event):
	FBApplication().ExecuteScript(r"x:\gta5\tools_ng\techart\sandboxmotionbuilder\Mike\mj_AllCamerasGrey.py")           

def propBoneVisibilityCallback(control, event):
	FBApplication().ExecuteScript(r"x:\gta5\tools_ng\techart\sandboxmotionbuilder\Mike\mj_TogglePropBonesVisible.py")

def moverVisibilityCallback(control, event):
	FBApplication().ExecuteScript(r"x:\gta5\tools_ng\techart\sandboxmotionbuilder\Mike\mj_ToggleMoverVisible.py")
    
def PopulateLayout(mainLyt):
    tab = FBTabControl()
        
    # Insert tab control
    x = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
    w = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
    h = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
    
    mainLyt.AddRegion("tab", "tab",x,y,w,h)
    mainLyt.SetControl("tab", tab)

    # ONLY TAB

    # This is the container inside tab 1...
    l = FBLayout()                
    x = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
    w = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
    h = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
    l.AddRegion("Helpers","Helpers", x, y, w, h)    
    
    # COPY CURRENT TAKE BUTTON
    copyTake = FBButton()
    copyTake.Style = FBButtonStyle.kFBBitmapButton
    copyTake.SetImageFileNames ("characterpose/copy.png") 
    copyTake.Hint = "Copy Current Take"
    
    x = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"Helpers")
    w = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    l.AddRegion("copyTake","copyTake", x, y, w, h)
    l.SetControl("copyTake",copyTake)
    copyTake.OnClick.Add(copyTakeCallback)
    
    # PLOT SKELETON BUTTON
    plotToSkeleton = FBButton()
    plotToSkeleton.Style = FBButtonStyle.kFBBitmapButton
    plotToSkeleton.SetImageFileNames ("browsing/template_character_actor.png") 
    plotToSkeleton.Hint = "Using Current Character, Bake(Plot) to Skeleton"
    
    x = FBAddRegionParam(40,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"Helpers")
    w = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    l.AddRegion("plotToSkeleton","plotToSkeleton", x, y, w, h)
    l.SetControl("plotToSkeleton",plotToSkeleton)    
    plotToSkeleton.OnClick.Add(plotToSkeletonCallback)
       
    # PLOT TO CONTROL RIG BUTTON
    plotToRig = FBButton()
    plotToRig.Style = FBButtonStyle.kFBBitmapButton
    plotToRig.SetImageFileNames ("browsing/template_character_character.png") 
    plotToRig.Hint = "Using Current Character, Bake(Plot) to Control Rig"
    
    x = FBAddRegionParam(80,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"Helpers")
    w = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    l.AddRegion("plotToRig","plotToRig", x, y, w, h)
    l.SetControl("plotToRig",plotToRig)
    plotToRig.OnClick.Add(plotToRigCallback)     

    # Make All Grey Background
    greyBackGround = FBButton()
    greyBackGround.Style = FBButtonStyle.kFBBitmapButton
    greyBackGround.SetImageFileNames ("{0}\\GreyIcon.jpg".format( RS.Config.Script.Path.ToolImages )) 
    greyBackGround.Hint = "Toggle Viewer Colour Lt. Grey / Dk. Grey"
    
    x = FBAddRegionParam(120,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"Helpers")
    w = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    l.AddRegion("greyBackGround","greyBackGround", x, y, w, h)
    l.SetControl("greyBackGround",greyBackGround)
    greyBackGround.OnClick.Add(greyBackGroundCallback)   

    # Toggle Prop Bones Visibility
    propBoneVisibility = FBButton()
    propBoneVisibility.Style = FBButtonStyle.kFBBitmapButton
    propBoneVisibility.SetImageFileNames ("Physics/joints-l.png") 
    propBoneVisibility.Hint = "Toggle Prop Bones Visibility"
    
    x = FBAddRegionParam(160,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"Helpers")
    w = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    l.AddRegion("propBoneVisibility","propBoneVisibility", x, y, w, h)
    l.SetControl("propBoneVisibility",propBoneVisibility)
    propBoneVisibility.OnClick.Add(propBoneVisibilityCallback)           

    # Toggle Prop Bones Visibility
    moverVisibility = FBButton()
    moverVisibility.Style = FBButtonStyle.kFBBitmapButton
    moverVisibility.SetImageFileNames ("primitives/Cylinder-l.png") 
    moverVisibility.Hint = "Toggle Mover Visibility"
    
    x = FBAddRegionParam(200,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"Helpers")
    w = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(35,FBAttachType.kFBAttachNone,"")
    l.AddRegion("moverVisibility","moverVisibility", x, y, w, h)
    l.SetControl("moverVisibility",moverVisibility)
    moverVisibility.OnClick.Add(moverVisibilityCallback)              
    
    # Last but not least add the layout to the tab
    tab.Add("Helpers",l) # Actually adds the tab you have just layed out to the UI 
       
    tab.SetContent(0) # This sets which tab will be the default one open.    
    tab.TabPanel.TabStyle = 0 # tab style
    
def Run():
    t = FBCreateUniqueTool("Anim Helper Bar")
    t.StartSizeX = 264
    t.StartSizeY = 95 
    PopulateLayout(t)
    ShowTool(t)   
Run()