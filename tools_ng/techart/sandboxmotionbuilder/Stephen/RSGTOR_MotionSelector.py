'''
Script Name     RSGTOR_motionSelector.py
Author          Stephen Sloper, Rockstar Toronto. (Modified by Kristine Middlemiss to be compliant with MotionBuilder 2014)

Description     Loads UI which helps creates new takes from source motion take.

HISTORY         v1.00       First revision.

To Run          Drag Drop file into viewport, click 'Execute'.
'''
#===================================================================================================
# Import all the modules needed, add pyfbsdk_additions path if MoBu doesn't already have in loaded.
additionsPath = r'C:\Program Files\Autodesk\MotionBuilder 2014\bin\config\Python'
import sys
import os

if additionsPath not in sys.path:
    sys.path.append(additionsPath)

from pyfbsdk import *
from pyfbsdk_additions import *

#===================================================================================================

def RSGTOR_MotionSelector_errorChecks(newTakeName, newTakeStart, newTakeEnd):
    if newTakeStart  == '*' or newTakeEnd == '*':
        message = "Value ' * ' is not a valid frame."
        FBMessageBox('GTA5_MotionSelector - Frame Value Error', message, 'OK')
        return False        
        
    if int(newTakeStart) > int(newTakeEnd) :
        message = "Start Value is greater than End Value."
        FBMessageBox('GTA5_MotionSelector - Frame Value Error', message, 'OK')
        return False 
        
    for take in FBSystem().Scene.Takes:
        if take.Name == newTakeName:
            message = "Take name '%s' already exists. Choose a different name." % newTakeName
            FBMessageBox('GTA5_MotionSelector - Take Name Error', message, 'OK')
            return False 


def RSGTOR_MotionSelector_plotTake():
    #create and define plot options before plotting.
    plotOptns = FBPlotOptions()
    plotOptns.PlotAllTakes = False
    plotOptns.PlotPeriod =  FBTime(0,0,0,1)
    plotOptns.PlotOnFrame = True
    
    #create rig to plot to skeleton and control rig
    currentCharacter = FBApplication().CurrentCharacter
    if currentCharacter:
        currentCharacter.PlotAnimation(FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, plotOptns)
        currentCharacter.PlotAnimation(FBCharacterPlotWhere.kFBCharacterPlotOnControlRig, plotOptns)
    else:
        print "No Current Character was Found"

def RSGTOR_MotionSelector_createNewTake(control, event):
    newTakeName = g_motionSeletor_tNameEditLabel.Text
    newTakeStart = g_motionSeletor_startFrameBut.Caption
    newTakeEnd = g_motionSeletor_endFrameBut.Caption
    plotTake = g_motionSeletor_plotBut.State
    currentTake = FBSystem().CurrentTake
    #run error checks.
    lError = RSGTOR_MotionSelector_errorChecks(newTakeName, newTakeStart, newTakeEnd)
    if lError != False:
    
        newTake = FBSystem().CurrentTake.CopyTake(newTakeName)
        timeRange = FBTimeSpan(FBTime(0,0,0,int(newTakeStart)), FBTime(0,0,0,int(newTakeEnd)))
        newTake.LocalTimeSpan = timeRange
        FBSystem().CurrentTake = newTake
        
        if  plotTake == True:
            RSGTOR_MotionSelector_plotTake()
        
        #return take to original
        FBSystem().CurrentTake = currentTake
        
        print ('New take created: "%s". Frames %s to %s.' % (newTakeName, newTakeStart, newTakeEnd))
    
    
def RSGTOR_MotionSelector_setStart(control, event):
    currentFrame = FBSystem().LocalTime.GetTimeString()
    currentFrame = currentFrame.replace('*','')
    g_motionSeletor_startFrameBut.Caption = currentFrame


def RSGTOR_MotionSelector_setEnd(control, event):
    currentFrame = FBSystem().LocalTime.GetTimeString()
    currentFrame = currentFrame.replace('*','')
    g_motionSeletor_endFrameBut.Caption = currentFrame
    

def RSGTOR_MotionSelector_PopulateLayout(ui):
    #create the main adjustable layout --------------------------------------------------
    al = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
    at = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
    ar = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
    ab = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")
    ui.AddRegion("main","main", al, at, ar, ab)
    manualLyt = FBVBoxLayout()
    ui.SetControl("main",manualLyt)
    
    #create the takeName  layout -------------------------------------------------------
    al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
    at = FBAddRegionParam(5,FBAttachType.kFBAttachTop,"")
    ab = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
    takeNameLyt = FBHBoxLayout()
    ui.AddRegion("tName","tName", al, at, ar, ab)
    ui.SetControl("tName",takeNameLyt)
    
    tNameLabel = FBLabel()
    tNameLabel.Caption = 'New take name:'
    takeNameLyt.Add(tNameLabel,370)
    
    #create the take name field ---------------------------------------------------------------
    at = FBAddRegionParam(25,FBAttachType.kFBAttachTop,"")
    takeNameEditLyt = FBHBoxLayout()
    ui.AddRegion("tNameEdit","tNameEdit", al, at, ar, ab)
    ui.SetControl("tNameEdit",takeNameEditLyt)
    
    global g_motionSeletor_tNameEditLabel
    g_motionSeletor_tNameEditLabel = FBEdit()
    g_motionSeletor_tNameEditLabel.Caption = 'New take name:'
    takeNameEditLyt.Add(g_motionSeletor_tNameEditLabel,280)
    
    #create the take settings buttons ---------------------------------------------------------------
    at = FBAddRegionParam(58,FBAttachType.kFBAttachTop,"")
    takeSettingsLyt = FBHBoxLayout()
    ui.AddRegion("tSettings","tSettings", al, at, ar, ab)
    ui.SetControl("tSettings",takeSettingsLyt)
    
    dummyLabel = FBLabel()
    dummyLabel.Caption = 'From:'
    takeSettingsLyt.Add(dummyLabel,28)
    
    global g_motionSeletor_startFrameBut
    g_motionSeletor_startFrameBut = FBButton()
    g_motionSeletor_startFrameBut.Caption = '*'
    g_motionSeletor_startFrameBut.OnClick.Add(RSGTOR_MotionSelector_setStart)
    takeSettingsLyt.Add(g_motionSeletor_startFrameBut,50)
    
    dummyLabel = FBLabel()
    dummyLabel.Caption = '     To:'
    takeSettingsLyt.Add(dummyLabel,30)
    
    global g_motionSeletor_endFrameBut
    g_motionSeletor_endFrameBut = FBButton()
    g_motionSeletor_endFrameBut.Caption = '*'
    g_motionSeletor_endFrameBut.OnClick.Add(RSGTOR_MotionSelector_setEnd)
    takeSettingsLyt.Add(g_motionSeletor_endFrameBut,50)
    
    dummyLabel = FBLabel()
    dummyLabel.Caption = ' '
    takeSettingsLyt.Add(dummyLabel,35)
    
    global g_motionSeletor_plotBut
    g_motionSeletor_plotBut = FBButton()
    g_motionSeletor_plotBut.Style = FBButtonStyle.kFBCheckbox
    g_motionSeletor_plotBut.Caption = 'plot take'
    takeSettingsLyt.Add(g_motionSeletor_plotBut,70)
    
    #create the create take button ---------------------------------------------------------------
    at = FBAddRegionParam(85,FBAttachType.kFBAttachTop,"")
    ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
    ab = FBAddRegionParam(150,FBAttachType.kFBAttachBottom,"")
    createTakeLyt = FBVBoxLayout()
    ui.AddRegion("createTake","createTake", al, at, ar, ab)
    ui.SetControl("createTake",createTakeLyt)
    
    createNewTake = FBButton()
    createNewTake.Caption = '- CREATE TAKE -'
    createNewTake.Look = FBButtonLook.kFBLookColorChange
    createNewTake.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.3, 0.6, 0.6))
    createNewTake.OnClick.Add(RSGTOR_MotionSelector_createNewTake)
    createTakeLyt.Add(createNewTake, 40)

    
def RSGTOR_MotionSelector_createTool():
    ui = FBCreateUniqueTool('RSGTOR_MotionSelector - v1.00')
    ui.StartSizeX = 325
    ui.StartSizeY = 170
    
    RSGTOR_MotionSelector_PopulateLayout(ui)
    ShowTool(ui)
    
RSGTOR_MotionSelector_createTool()