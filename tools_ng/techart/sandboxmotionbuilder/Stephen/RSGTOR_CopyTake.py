from pyfbsdk import *

lSys = FBSystem()
currTakeName = lSys.CurrentTake.Name
if "Copy" in currTakeName:
    if currTakeName.endswith("Copy"):
        lTake = lSys.CurrentTake.CopyTake(currTakeName + " 1")
    else:
        TakeNameSplit = currTakeName.split(" - ")
        VerSplit = TakeNameSplit[1].split(" ")
        verNum = int(VerSplit[1])
        newTakeName = TakeNameSplit[0] + " - Copy " + str(verNum+1)        
        lTake = lSys.CurrentTake.CopyTake(newTakeName)
        
else:
    lTake = lSys.CurrentTake.CopyTake(currTakeName + " - Copy")