'''
Script Name     RSGTOR_QuickParentConstraint.py
Author          Stephen Sloper, Rockstar Toronto.  (Modified by Kristine Middlemiss to be compliant with MotionBuilder 2014)

Description     Quickly sets up a parent constraint to work like Maya. Select object to constrain, then source, run script.

HISTORY         v1.00           First revision.

To Run          Drag Drop file into viewport, click 'Execute'.
'''
#===================================================================================================
# Import all the modules needed.
from pyfbsdk import *

#===================================================================================================

def RSGTOR_QuickParentConstraint_doIt(innitSel, follow, source):
    constraintMan = FBConstraintManager()
    parentConstraintIndexValue = 3
    prntCnst = constraintMan.TypeCreateConstraint(parentConstraintIndexValue)
    prntCnst.Name = ('%s > %s' % (follow.Name, source[0].Name))
    prntCnst.ReferenceAdd (0, follow)
    prntCnst.ReferenceAdd (1, source[0])
    
    prntCnst.Active = True
    prntCnst.Lock = True
    print ('"%s" has been parent constrained to follow "%s"' % (follow.Name, source[0].Name))

def RSGTOR_QuickParentConstraint():
    innitSel = FBModelList()
    FBGetSelectedModels(innitSel)
    
    if len(innitSel) != 2:
        FBMessageBox('RSGTOR_QuickParentConstraint.py', 'Please select only 2 objects.', 'OK')
        return
    
    #use the undo manager to HACK how to find the last two selected objects.
    undoMan = FBUndoManager()
    undoMan.TransactionEnd()    #End the current transaction by MotionBuilder. This Prevents the 'undo hack' getting confused with current and previous selections.
    undoMan.Undo(False)
    
    bufferSel = FBModelList()
    FBGetSelectedModels(bufferSel)
    
    follow = bufferSel[0]
    source = list(innitSel)
    source.remove(follow)
    
    undoMan.Redo()
    
    message = ('"%s" will be constrained to follow "%s".' % (follow.Name, source[0].Name))
    promt = FBMessageBox('RSGTOR_QuickParentConstraint.py', message, 'OK', 'Cancel' )
    OK = 1
    
    if promt is OK:
        RSGTOR_QuickParentConstraint_doIt(innitSel, follow, source)

RSGTOR_QuickParentConstraint()