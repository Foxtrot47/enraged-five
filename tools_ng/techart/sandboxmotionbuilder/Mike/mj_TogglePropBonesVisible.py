##########################################################
# Make hand prop bones visible and larger
##########################################################

from pyfbsdk import *

cl = FBComponentList()
FBFindObjectsByName ("PH_R_Hand", cl, False, True)
cl2 = FBComponentList()
FBFindObjectsByName ("PH_L_Hand", cl2, False, True)
if len(cl) > 0 and len(cl2) > 0:
    ph_r_hand =cl[0]                
    ph_l_hand =cl2[0]  
    
    if ph_r_hand.PropertyList.Find('Look').Data == 0:
        ph_r_hand.PropertyList.Find('Look').Data = 1
        ph_r_hand.PropertyList.Find('Size').Data = 4
    else:
        ph_r_hand.PropertyList.Find('Look').Data = 0
        ph_r_hand.PropertyList.Find('Size').Data = 4    
    
    if ph_l_hand.PropertyList.Find('Look').Data == 0:
        ph_l_hand.PropertyList.Find('Look').Data = 1
        ph_l_hand.PropertyList.Find('Size').Data = 4
    else:
        ph_l_hand.PropertyList.Find('Look').Data = 0
        ph_l_hand.PropertyList.Find('Size').Data = 4    
