##########################################################
# Make hand prop bones visible and larger
##########################################################

from pyfbsdk import *

mover_list = FBComponentList() 
FBFindObjectsByName('mover', mover_list, False, True)

for mover in mover_list:
    if mover.PropertyList.Find('Show').Data == 0:
        mover.PropertyList.Find('Show').Data = 1
    else:
        mover.PropertyList.Find('Show').Data = 0

