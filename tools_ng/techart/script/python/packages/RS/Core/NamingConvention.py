'''
RS.Core.NamingConvention

Author:

	Jason Hayes <jason.hayes@rockstarsandiego.com>
	
Description:

	Module for dealing with naming conventions.
'''

class TextureMapType:
	DiffuseMap	= 1
	SpecularMap = 2
	NormalMap	= 3
	ChannelMap	= 4
	HeightMap	= 5
	
class TextureMapSuffix:
	DiffuseMap	= ''
	SpecularMap = '_s'
	NormalMap	= '_n'
	ChannelMap	= '_cm'
	HeightMap	= '_hm'