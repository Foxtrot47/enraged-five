'''
Module for working with metadata.

This module allows us to deserialize/serialize from metadata files.
The module has support for the majority of 'simple' and 'complex'
types in use by the ParGen system.

Author: Ross George <ross.george@rockstarlondon.com>
'''

import clr
import sys

clr.AddReference("RSG.Base.Configuration")
clr.AddReference("RSG.Metadata")

from RSG.Base.Configuration import ConfigFactory
from RSG.Metadata.Parser import Structure
from RSG.Metadata.Data import TunableFactory
import RSG.Metadata.Model
from System import ArgumentOutOfRangeException 
from System import Boolean 

_config = ConfigFactory.CreateConfig()
_definitionDirectory = _config.Project.DefaultBranch.Metadata + "\\definitions"
_structureDefinitionDictionary = RSG.Metadata.Model.StructureDictionary()
_structureDefinitionDictionary.Load( _config.Project.DefaultBranch, _definitionDirectory )

class MetaUnsupportedTunableTypeError( Exception ):
    """Raised if the an unknown/unsupported tunable type has been encounterd."""
        
class MetaUnrecognisedStructureDefinitionError( Exception ):
    """Raised if the we fail to find a structure definition during file creation."""
    
class MetaEmptyFilePathError( Exception ):
    """Raised if the user tries to save a metadata file without the filepath property set correctly."""
    
def ParseMetaFile( metaFilePath ):
    '''Parses the passed MetaFilePath and returns an initialized class which backs the MetaFile.''' 
    
    #Create a .NET MetaFile using the _structureDefinitionDictionary and a path to an existing MetaFilePath
    targetNETMetaFile = RSG.Metadata.Model.MetaFile( metaFilePath , _structureDefinitionDictionary, _config )
    
    #Check that the MetaFile was loaded correctly
    if targetNETMetaFile == None or targetNETMetaFile.Definition == None:
        raise MetaUnrecognisedStructureDefinitionError('Failed to parse the given MetaFile. Ensure that you are synced to the latest definitions.')
         
    #Create a new MetaFile from the NETMetaFile
    return( MetaFile( targetNETMetaFile ) )

def CreateMetaFile( structureDefinitionName ):
    '''Creates a new MetaFile from the structure definition name.''' 
    
    if _structureDefinitionDictionary.ContainsKey( structureDefinitionName ):
        #Create a .NET MetaFile using the corrolating entry in our _structureDefinitionDictionary
        targetNETMetaFile = RSG.Metadata.Model.MetaFile( _structureDefinitionDictionary.get_Item( structureDefinitionName ) )    
        
        #Create a new MetaStruct from the RootStructTunable
        return( MetaFile( targetNETMetaFile ) )
    else:    
        raise MetaUnrecognisedStructureDefinitionError('[{0}] is not a recognised definition. Ensure that you are synced to the latest definitions and you have the name of the defintion correct'.format(structureDefinitionName) )

class MetaFile( object ):
    '''Represents a MetaFile that can be edited and serialized. Should be created via the CreateMetaFile and ParseMetaFile module functions.'''
    
    def __init__( self, sourceNETMetaFile ):
        self._sourceNETMetaFile = sourceNETMetaFile
        self._filePath = sourceNETMetaFile.Filename
        rootTunable = sourceNETMetaFile.Members.get_Item( sourceNETMetaFile.Definition.DataType )        
        self.Root = MetaCreate( rootTunable )
    
    def __getattr__ ( self, attr ):
        try:
            metaObject = self.__dict__["Root"].__dict__[attr]
            return MetaGet( metaObject )
        except: raise AttributeError( "{0} object does not have the attribute {1}".format( self, attr ) )

    def __setattr__( self, attr, value ):
        try:
            metaObject = self.__dict__["Root"].__dict__[attr]
            MetaSet( metaObject, value )
            return
        except: pass
        self.__dict__[attr] = value
        
    def SetFilePath( self, newFilePath ):
        self._filePath = newFilePath
    
    def Save( self ):
        if self._filePath == "":
            raise MetaEmptyFilePathError()

        self._sourceNETMetaFile.Serialise( self._filePath ) 
    
class MetaBase( object ):
    '''The base type for all Meta member types.'''
    
    def __init__( self, sourceTunable ):
        '''Initialize the Meta member. Must be provided with a sourceTunable. This is the .NET ITunable that this Meta member backs'''
        
        #We set these directly into the dictionary to avoid invoking our __setattr__ logic
        self.__dict__['_sourceTunable'] = sourceTunable
        
    def __setattr__( self, name, value):   
        raise AttributeError( "You cannot directly add additional attributes to this object: {0}".format( self.__class__.__name__ ) )     
        
class MetaProperty( object ):
    
    def __init__( self, targetAttributeName ):
        self._targetAttributeName = targetAttributeName
        
    def __get__(self, obj, type=None):
        return getattr( obj._sourceTunable, self._targetAttributeName )
    
    def __set__(self, obj, value):
        setattr( obj._sourceTunable, self._targetAttributeName, value )
        
    def __delete__(self, obj):
        pass

def MetaSet( metaObject, newValue ):
    if issubclass( type( metaObject ), MetaSimple ):
        try:
            if( type(newValue) == bool):
                newValue = Boolean( newValue )
            metaObject._sourceTunable.Value = newValue
        except TypeError:
            raise TypeError( "Incorrect data type. Expected {0}.".format( metaObject.__class__.__name__ ) )
        
    if issubclass( type( metaObject ), MetaComplex ):
        if type( metaObject ) == MetaEnum:
            if metaObject._sourceTunable.Definition.Values.ContainsKey( newValue ):
                metaObject._sourceTunable.Value = metaObject._sourceTunable.Definition.Values[ newValue ]
            else:
                raise AttributeError( "Bad enum value: Received '{0}' and expected one of the following.".format( newValue ) )  
        else:
            raise AttributeError( "Cannot set because it is a MetaComplex type: {0}:".format( metaObject.__class__.__name__ ) )  
    
def MetaGet( metaObject ):
    
    if type( metaObject ) == MetaEnum:
        return metaObject._sourceTunable.ValueAsString  
    
    if issubclass( type( metaObject), MetaSimple ):
        return metaObject._sourceTunable.Value

    return metaObject 

#Simple types
#---------------------------------
class MetaSimple( MetaBase ): 
    def Set( self, Value ):
        MetaSet(self, Value)
        
class MetaString( MetaSimple ): pass
class MetaInteger( MetaSimple ): pass
class MetaFloat( MetaSimple ): pass
class MetaBitset( MetaSimple ): pass
class MetaBoolean( MetaSimple ): pass

#Complex types
#---------------------------------
class MetaComplex( MetaBase ): pass

class MetaEnum( MetaComplex ): pass

class MetaComposite( MetaComplex ):
    
    def __getattr__( self, name ):
        if hasattr( self.__dict__['_sourceTunable'], name ):
            return getattr( self.__dict__['_sourceTunable'], name ) 
        else:
            raise Exception("No attribute called '{0}' on type: {1}".format(name, self.__class__.__name__))
        
    def __setattr__( self, name, value ):
        if hasattr( self._sourceTunable, name ):
            setattr( self._sourceTunable, name, value ) 
        else:
            raise Exception("No attribute called '{0}' on type: {1}. Cannot add new attributes.".format(name, self.__class__))

class MetaVector2( MetaComposite ): pass
    #X = MetaProperty( "X" )
    #Y = MetaProperty( "Y" )

class MetaVector3( MetaComposite ): pass
    #X = MetaProperty( "X" )
    #Y = MetaProperty( "Y" )
    #Z = MetaProperty( "Z" )

class MetaVector4( MetaComposite ): pass
    #X = MetaProperty( "X" )
    #Y = MetaProperty( "Y" )
    #Z = MetaProperty( "Z" )
    #W = MetaProperty( "W" )

class MetaColor( MetaComposite ): pass
    #R = MetaProperty( "R" )
    #G = MetaProperty( "G" )
    #B = MetaProperty( "B" )

class MetaStruct( MetaComplex ):
    '''The most complex of all the Meta types. This type is a composite of many and any of the other Meta types. Effectively it is abstract and only gets inherited from.'''
    
    def __getattribute__( self, name ):
        metaObject = MetaBase.__getattribute__( self, name )
        return MetaGet( metaObject )
          
    def __setattr__( self, name, value ):
        metaObject = MetaBase.__getattribute__( self, name )
        MetaSet( metaObject, value )
        
    def _addMember( self, name, metaObject ):
        self.__dict__[name] = metaObject    
    
class MetaArray( MetaComplex ):
    '''The Meta type that handles arrays of other Meta types.'''    
    
    def __getitem__( self, index ):
        metaObject = self._getMetaObject( index )
        return MetaGet( metaObject )
        
    def __setitem__( self, index, value ):
        metaObject = self._getMetaObject( index )
        MetaSet( metaObject, value )
        
    def __delitem__( self, index ):
        self._sourceTunable.RemoveAt( index )
        
    def _getMetaObject( self, index ):
        try:
            targetTunable = self._sourceTunable.get_Item( index )
            return( MetaCreate( targetTunable ) )
        
        except ( TypeError, ArgumentOutOfRangeException ):
            raise IndexError()        

    def Remove( self, metaObject ):
        self._sourceTunable.Remove( metaObject._sourceTunable )
        
    def Add( self, metaObject ):
        self._sourceTunable.Add( metaObject._sourceTunable )
        
    def Create( self ):
        '''Creates and returns a new element of the Meta member type.'''
        
        newTunable = TunableFactory.Create( self._sourceTunable, self._sourceTunable.Definition.ElementType )
        
        self._sourceTunable.Add( newTunable )
        return MetaCreate( newTunable )

STRUCT_TUNABLE  = u'RSG.Metadata.Data.StructureTunable'
POINTER_TUNABLE = u'RSG.Metadata.Data.PointerTunable'
ARRAY_TUNABLE   = u'RSG.Metadata.Data.ArrayTunable'
FLOAT_TUNABLE   = u'RSG.Metadata.Data.FloatTunable'
FLOAT16_TUNABLE = u'RSG.Metadata.Data.Float16Tunable'
COLOR32_TUNABLE = u'RSG.Metadata.Data.Color32Tunable'
STRING_TUNABLE  = u'RSG.Metadata.Data.StringTunable'
UINT8_TUNABLE   = u'RSG.Metadata.Data.U8Tunable'
SINT8_TUNABLE   = u'RSG.Metadata.Data.S8Tunable'
UINT32_TUNABLE  = u'RSG.Metadata.Data.U32Tunable'
SINT32_TUNABLE  = u'RSG.Metadata.Data.S32Tunable'
UINT16_TUNABLE  = u'RSG.Metadata.Data.U16Tunable'
SINT16_TUNABLE  = u'RSG.Metadata.Data.S16Tunable'
SHORT_TUNABLE   = u'RSG.Metadata.Data.ShortTunable'
INT_TUNABLE     = u'RSG.Metadata.Data.IntTunable'
BITSET_TUNABLE  = u'RSG.Metadata.Data.BitsetTunable'
BOOL_TUNABLE    = u'RSG.Metadata.Data.BoolTunable'
VECTOR2_TUNABLE = u'RSG.Metadata.Data.Vector2Tunable'
VECTOR3_TUNABLE = u'RSG.Metadata.Data.Vector3Tunable'
VECTOR4_TUNABLE = u'RSG.Metadata.Data.Vector4Tunable'
ENUM_TUNABLE    = u'RSG.Metadata.Data.EnumTunable'
COLOR32_TUNABLE = u'RSG.Metadata.Data.Color32Tunable'

MetaStructClassDictionary = dict()
MetaPointerClassDictionary = dict()

def MetaCreate( sourceTunable ):
    '''This will take a .NET ITunable and return a python Meta member type which wraps it correctly'''
    
    #Get the source tunable type
    sourceTunableType = sourceTunable.GetType().FullName
    
    if sourceTunableType in ( POINTER_TUNABLE ):   
        
        #If the pointer is null we'll create a fresh structure
        if sourceTunable.Value.Name == 'NULL':
            tempNETMetaFile = RSG.Metadata.Model.MetaFile( sourceTunable.Definition.ObjectType );
            sourceTunable.Value = tempNETMetaFile.Members.get_Item( tempNETMetaFile.Definition.DataType )
        
        sourceTunable = sourceTunable.Value        
        sourceTunableType = sourceTunable.GetType().FullName

    if sourceTunableType in ( STRUCT_TUNABLE ):
        
        #Get the type name of the structure
        structureTypeName = sourceTunable.Definition.TypeName
              
        #First check to see if we already have a class created for the .NET Structure 
        metaStructClass = None
        if structureTypeName in MetaStructClassDictionary:
            metaStructClass = MetaStructClassDictionary[ structureTypeName ]
        
        #If not we'll have to creata a new python type
        if metaStructClass == None:
                 
            #Generate a new Type with the TypeName and NewTypeAttributeDictionary
            metaStructClass = type( 'MetaStruct[{0}]'.format( structureTypeName ), ( MetaStruct, ), {} )    
        
            #Add it to our TypeDictionary for future use
            MetaStructClassDictionary[ structureTypeName ] = metaStructClass
        
        #Create an instance of our MetaStructClass
        metaStructInstance = metaStructClass( sourceTunable )
        metaStructInstance.__dict__['Type'] = structureTypeName        
        
        #Now we must go through each of the members of the StructTunable and intialize them
        for subTunable in sourceTunable.Values:
            metaStructInstance._addMember( subTunable.Name, MetaCreate( subTunable ) )
            
        return metaStructInstance
    
    if sourceTunableType in ( ENUM_TUNABLE ) :
        return MetaEnum( sourceTunable )
    
    if sourceTunableType in ( ARRAY_TUNABLE ) :
        return MetaArray( sourceTunable )
    
    if sourceTunableType in ( FLOAT_TUNABLE, FLOAT16_TUNABLE ) :
        return MetaFloat( sourceTunable )
    
    if sourceTunableType in ( UINT8_TUNABLE, SINT8_TUNABLE, 
                              UINT16_TUNABLE, SINT16_TUNABLE, 
                              UINT32_TUNABLE, SINT32_TUNABLE, 
                              INT_TUNABLE, SHORT_TUNABLE ) :
        return MetaInteger( sourceTunable )
    
    if sourceTunableType in ( STRING_TUNABLE ) :
        return MetaString( sourceTunable )    

    if sourceTunableType in ( VECTOR2_TUNABLE ) :
        return MetaVector2( sourceTunable )

    if sourceTunableType in ( VECTOR3_TUNABLE ) :
        return MetaVector3( sourceTunable )

    if sourceTunableType in ( VECTOR4_TUNABLE ) :
        return MetaVector4( sourceTunable )
    
    if sourceTunableType in ( BITSET_TUNABLE ) :
        return MetaBitset( sourceTunable )

    if sourceTunableType in ( BOOL_TUNABLE ) :
        return MetaBoolean( sourceTunable )

    if sourceTunableType in ( COLOR32_TUNABLE ) :
        return MetaColor( sourceTunable )
    
    raise MetaUnsupportedTunableTypeError( 'Tunable is not supported: {0}'.format( sourceTunableType ) )




        