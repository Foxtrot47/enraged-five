'''
RS.Core.Substance

Author:

	Jason Hayes <jason.hayes@rockstarsandiego.com>
	
Description:

	Library for working with Substance.
'''

import os
import subprocess
import _winreg
import xml.etree.cElementTree

import RS.Core.Substance.Path


## Constants ##

BATCH_TOOLS_VERSION = 3
DESIGNER_VERSION	= 4


## Functions ##

def BatchToolsInstalled():
	return os.path.isdir( RS.Core.Substance.Path.GetBatchToolsInstallDir() )


## Classes ##

class OutputResolution:
	Full	= 'Full'
	Half	= 'Half'
	Quarter	= 'Quarter'

class SbsPreset( object ):
	def __init__( self, sbsPresetFilename ):
		self.__sbsPresetFilename = sbsPresetFilename
		self.__package = None
		self.__packageLabel = None
		self.__inputs = {}
		
		self.Load()
		
	
	## Properties ##
	
	@property
	def Package( self ):
		return self.__package
	
	@property
	def PackageLabel( self ):
		return self.__packageLabel
		
	@property
	def Filename( self ):
		return self.__sbsPresetFilename
	
	@property
	def Inputs( self ):
		return self.__inputs
		

	## Methods ##
	
	def Load( self ):
		if os.path.isfile( self.Filename ):
			self.__inputs = {}
			
			doc = xml.etree.cElementTree.parse( self.Filename )
			root = doc.getroot()
			
			if root:
				presetNode = root.find( 'sbspreset' )
				
				self.__package = presetNode.get( 'pkgurl' )
				self.__packageLabel = presetNode.get( 'label' )
				
				presetInputNodes = presetNode.findall( 'presetinput' )
				
				for presetInputNode in presetInputNodes:
					identifier = presetInputNode.get( 'identifier' )
					value = presetInputNode.get( 'value' )
					
					self.__inputs[ identifier ] = value
	
class SbsInfo( object ):
	'''
	Collects information about a Substance file.
	'''
	def __init__( self, sbsFilename ):
		assert BatchToolsInstalled(), 'The Substance Batch Tools {0}.x are not installed!  Cannot continue.'.format( BATCH_TOOLS_VERSION )
		
		self.__sbsFilename = sbsFilename
		
		self.__inputImages = []
		self.__inputParameters = {}
		self.__outputImages = {}
		self.__package = None
		
		self.Query()
		
	
	## Properties ##
	
	@property
	def InputParameters( self ):
		return self.__inputParameters
	
	@property
	def InputImages( self ):
		return self.__inputImages
	
	@property
	def OutputImages( self ):
		return self.__outputImages
	
	@property
	def Package( self ):
		return self.__package
	
	
	## Methods ##
		
	def Query( self ):
		sbsRenderExe = RS.Core.Substance.Path.GetSbsRenderExe()
			
		if sbsRenderExe:
			self.__inputImages = []
			self.__inputParameters = {}
			self.__outputImages = {}
			
			cmd = '{0} info {1}'.format( sbsRenderExe, self.__sbsFilename )
			proc = subprocess.Popen( cmd, stdout = subprocess.PIPE )
			output, err = proc.communicate()
			
			# Parse output for information.
			lines = output.split( '\n' )
			
			for line in lines:
				line = line.rstrip().lstrip()
				
				# Input images.
				if line.startswith( 'INPUT' ) and line.endswith( 'IMAGE' ):
					inputIdentifier = line.split( ' ' )[ 1 ]
					self.__inputImages.append( inputIdentifier )
					
				# Input parameters.
				if line.startswith( 'INPUT' ) and not line.endswith( 'IMAGE' ):
					inputId, inputIdentifier, inputType = line.split( ' ' )
					self.__inputParameters[ inputIdentifier ] = inputType
					
				# Output images.
				if line.startswith( 'OUTPUT' ):
					outputType, outputIdentifier, outputLabel = line.split( ' ' )
					self.__outputImages[ outputIdentifier ] = outputLabel
					
				# Package
				if line.startswith( 'GRAPH-URL' ):
					self.__package = line.split( ' ' )[ -1 ]
		
class SbsWrapper( object ):
	'''
	Simple wrapper class for generating outputs from a .sbs or .sbsar file.
	'''
	def __init__( self, sbsFilename, outputImageType = 'tif' ):
		assert BatchToolsInstalled(), 'The Substance Batch Tools {0}.x are not installed!  Cannot continue.'.format( BATCH_TOOLS_VERSION )
		
		self.__filename = sbsFilename
		self.__outputName = None
		self.__inputImages = {}
		self.__inputParameters = {}
		self.__info = SbsInfo( sbsFilename )
		self.__outputImageType = outputImageType
		self.__preset = None
		
	
	## Properties ##
	
	@property
	def Preset( self ):
		return self.__preset
	
	@property
	def Info( self ):
		return self.__info
	
	@property
	def OutputName( self ):
		return self.__outputName
	
	@property
	def OutputImageType( self ):
		return self.__outputImageType
	
	@property
	def Filename( self ):
		return self.__filename
	
	@property
	def InputImages( self ):
		return self.__inputImages
	
	@property
	def InputParameters( self ):
		return self.__inputParameters
	
	@property
	def OutputImages( self ):
		outputImages = []
		
		for labelName, identifier in self.Info.OutputImages.iteritems():
			outputImages.append( '{0}_{1}.{2}'.format( self.__outputName, identifier, self.OutputImageType ) )
			
		return outputImages
	

	## Methods ##
	
	def ClearInputs( self ):
		self.__inputImages = {}
		self.__inputParameters = {}
	
	def SetPreset( self, presetFilename ):
		if os.path.isfile( presetFilename ):
			self.__preset = SbsPreset( presetFilename )
			
		else:
			print 'The supplied preset filename does not exist!  Cannot set preset.', presetFilename
			
	def SetOutputSize( self, width, height ):
		
		# Determine exponent for both the supplied width and height.  Setting the output size in Substance
		# is done by pow( 2, n ).  So we supply an image size like 1024, 1024.  Then determine it's exponent
		# for each axis.
		wExponent = 1
		hExponent = 1
		
		w = width
		h = height
		
		while w != 2:
			wExponent += 1
			w /= 2
			
		while h != 2:
			hExponent += 1
			h /= 2			

		self.SetInputParameter( '$outputsize', '{0},{1}'.format( wExponent, hExponent ) )
	
	def SetOutputImageType( self, imageType ):
		self.__outputImageType = imageType
	
	def SetFilename( self, filename ):
		self.__filename = filename
	
	def SetOutputName( self, name ):
		self.__outputName = name
		
	def SetInputParameter( self, identifier, value ):
		if identifier in self.Info.InputParameters:
			self.__inputParameters[ identifier ] = value			
			
		else:
			print 'The supplied identifier "{0}" does not exist as an input parameter!'.format( identifier )
	
	def SetInputImage( self, identifier, imageFilename ):
		if os.path.isfile( imageFilename ):
			if identifier in self.Info.InputImages:
				self.__inputImages[ identifier ] = imageFilename
				
			else:
				print 'The supplied input image identifier "{0}" does not exist in the Substance!'.format( identifier )
				
		else:
			print 'The supplied image filename does not exist!', imageFilename
			
	def GetInputImage( self, identifier ):
		if identifier in self.__inputImages:
			return self.__inputImages[ identifier ]
		
		else:
			print 'The supplied input image identifier "{0}" does not exist in the Substance!'.format( identifier )
		
		return None
		
	def Render( self, outputDir, ulog = None ):
		outputs = []
		
		sbsRenderExe = RS.Core.Substance.Path.GetSbsRenderExe()
		
		if sbsRenderExe:
			print 'Rendering outputs using Substance: ', os.path.basename( self.Filename )
			
			cmd = '{0} render '.format( sbsRenderExe )
			cmd += '--inputs "{0}" '.format( self.Filename )
			
			# Set input images.
			for identifier, imageFilename in self.InputImages.iteritems():		
				cmd += '--set-entry {0}@"{1}" '.format( identifier, imageFilename )
				
			# Set input parameters.
			for identifier, value in self.InputParameters.iteritems():
				cmd += '--set-value {0}@{1} '.format( identifier, value )
				
				if ulog:
					ulog.LogMessage( 'Setting input parameter "{0}" to "{1}".'.format( identifier, value ), context = 'Input' )
				
			# Set preset parameters.
			if self.Preset:
				if self.Preset.Package == self.Info.Package:
					if ulog:
						ulog.LogMessage( 'Applying Substance Preset: "{0}".'.format( os.path.basename( self.Preset.Filename ) ), context = 'Preset' )
					
					for identifier, value in self.Preset.Inputs.iteritems():
						if identifier not in self.Info.InputImages:
							if identifier in self.Info.InputParameters:
								
								# Skip setting output size from the preset.
								if identifier == '$outputsize':
									ulog.LogDebug( 'Skipped setting "$outputsize" from preset.', context = 'Preset' )
									continue
								
								cmd += '--set-value {0}@{1} '.format( identifier, value )
								
								if ulog:
									ulog.LogDebug( 'Setting preset value "{0}" to "{1}".'.format( identifier, value ) )
								
							else:
								if ulog:
									ulog.LogWarning( 'Cannot set preset input "{0}" because it does not exist in the Substance!'.format( identifier ) )
							
				else:
					if ulog:
						ulog.LogWarning( 'Cannot apply preset "{0}" because it was saved from the Substance package "{1}".  You must supply a preset saved from the Substance package "{2}".'.format( os.path.basename( self.Preset.Filename ), self.Preset.Package, self.Info.Package ) )
				
			cmd += '--output-path "{0}" '.format( outputDir )
			cmd += '--output-name {0}_{1} '.format( self.OutputName, '{outputNodeName}' )
			cmd += '--output-format {0} '.format( self.OutputImageType )
			
			proc = subprocess.Popen( cmd, stdout = subprocess.PIPE )
			output, err = proc.communicate()
			
			# Parse the output filenames from the Substance output.
			outputLines = output.split( '\n' )
			
			for line in outputLines:
				if line.startswith( '* Output' ):
					line = line.rstrip()
					sep = line.find( ':' )
					path = line[ sep + 2: ]
					path = os.path.abspath( path.lstrip( '"' ).rstrip( '"' ) )
					outputs.append( path )
					
		return outputs
			
			
if __name__ == '__main__':
	sbsar = '{0}\\Bitmap2Material_2.sbsar'.format( RS.Core.Substance.Path.GetProjectArchivesDir() )
	
	sbs = SbsWrapper( sbsar )
	sbs.SetOutputName( 'diffuseMapTest' )
	sbs.SetInputImage( 'Bitmap_Diffuse', 'x:\\temp\\diffuseMap.tif' )
	sbs.SetPreset( 'x:\\rdr3\\art\\techart\\substance\\presets\\test1.sbsprs' )
	outputImages = sbs.Render( 'x:\\temp\\substance_playground' )
	
	print outputImages