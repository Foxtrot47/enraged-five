'''
RS.Core.Substance.Batch

Author: 

	Jason Hayes <jason.hayes@rockstarsandiego.com>

Description:

	Library module for batch processing images through Substance.
'''
import os
import xml.etree.cElementTree

import RS.Core.Substance
import RS.Core.Substance.Path
import RS.Utils.Logging.Universal

from PySide import QtCore
from PIL import Image


# Rockstar Substance batch settings file extension.
FILE_EXT = '.sbsbatch'

# Stores all of the mappings.
__Mappings = {}

# Progress update.
__ProgressCount = 0
__ProgressCurrentCount = 0


## Functions ##

def GetBatchProgress():
	global __ProgressCount
	global __ProgressCurrentCount
	
	return 100 * __ProgressCurrentCount / __ProgressCount

def GetMapping( substanceName ):
	global __Mappings
	
	if substanceName in __Mappings:
		return __Mappings[ substanceName ]
	
def GetAllMappings():
	global __Mappings
	return __Mappings

def LoadMappings():
	batchXml = RS.Core.Substance.Path.GetBatchXml()
	
	if batchXml:
		global __Mappings
		
		doc = xml.etree.cElementTree.parse( batchXml )
		root = doc.getroot()
		
		if root:
			substanceNodes = root.findall( 'SubstanceMapping' )
			
			for substanceNode in substanceNodes:
				mapping = SubstanceMapping()
				
				filename = str( substanceNode.get( 'filename' ) )
				
				if '{projRoot}' in filename:
					mapping.Filename = os.path.abspath( filename.format( projRoot = RS.Config.Project.Path.Root ) )
				
				# Input images
				inputImageNodes = substanceNode.find( 'InputImages' ).findall( 'InputImage' )
				
				for inputImageNode in inputImageNodes:
					identifier = inputImageNode.get( 'identifier' )
					prefix = inputImageNode.get( 'prefix' )
					suffix = inputImageNode.get( 'suffix' )
					required = inputImageNode.get( 'required' )
					
					inputMapping = InputMapping()
					inputMapping.Identifier = identifier
					inputMapping.Prefix = prefix
					inputMapping.Suffix = suffix
					
					if required.lower() == 'false':
						inputMapping.Required = False
						
					else:
						inputMapping.Required = True
					
					mapping.InputImages[ identifier ] = inputMapping
					
				# Options
				mapping.OutputSizeIdentifier = substanceNode.find( 'Options' ).find( 'OutputSizeIdentifier' ).get( 'name' )
				
				presetFilename = substanceNode.find( 'Options' ).find( 'Preset' ).get( 'filename' )
				mapping.PresetFilename = presetFilename if presetFilename != '' else None
					
				__Mappings[ mapping.Name ] = mapping
			
def ProcessTextureList( substanceName, textureList, presetFilename = None, showLog = True, outputDir = None, outputSize = [], outputResolution = RS.Core.Substance.OutputResolution.Full ):
	global __Mappings
	global __ProgressCount
	global __ProgressCurrentCount
	global ProgressUpdateEvent
	
	__ProgressCount = len( textureList )
	__ProgressCurrentCount = 0
	
	ulog = RS.Utils.Logging.Universal.UniversalLog( 'SubstanceBatch' )
	
	if substanceName in __Mappings:
		ulog.LogMessage( 'Starting batch using the Substance "{0}"'.format( substanceName ) )
		
		mapping = __Mappings[ substanceName ]
		sbs = RS.Core.Substance.SbsWrapper( mapping.Filename )
		
		if presetFilename:
			sbs.SetPreset( presetFilename )
		
		for texture in textureList:
			textureBasename = os.path.basename( texture ).split( '.' )[ 0 ]
			
			ulog.LogMessage( 'Processing texture "{0}".'.format( texture ), context = textureBasename )
			                 
			sbs.ClearInputs()
			sbs.SetOutputName( textureBasename )
			
			for identifier, inputMapping in mapping.InputImages.iteritems():
				inputTexture = '{0}\\{prefix}{baseName}{suffix}.tif'.format( os.path.dirname( texture ), prefix = inputMapping.Prefix, baseName = textureBasename, suffix = inputMapping.Suffix )
				
				if os.path.isfile( inputTexture ):
					ulog.LogMessage( 'Setting input texture "{0}" to identifier "{1}".'.format( inputTexture, identifier ), context = 'Input' )
					sbs.SetInputImage( identifier, inputTexture )
					
				elif not inputMapping.Required:
					ulog.LogWarning( 'Could not find the optional input texture for identifier "{0}".'.format( identifier ), context = 'Input' )
					
				elif inputMapping.Required:
					ulog.LogError( 'Could not find the required input texture for identifier "{0}".  Was expecting "{1}".  The rendered result will likely be incorrect!'.format( identifier, inputTexture ), context = 'Input' )
					
			# Set output size.
			if outputSize:
				size = outputSize
				
				if outputResolution == RS.Core.Substance.OutputResolution.Half:
					size[ 0 ] /= 2
					size[ 1 ] /= 2
					
				elif outputResolution == RS.Core.Substance.OutputResolution.Quarter:
					size[ 0 ] /= 4
					size[ 1 ] /= 4
					
				if size[ 0 ] < 1:
					size[ 0 ] = 1
					
				if size[ 1 ] < 1:
					size[ 1 ] = 1
					
				sbs.SetOutputSize( size[ 0 ], size[ 1 ] )
			
			# No output size supplied, so use whatever size the texture is.
			else:
				if mapping.OutputSizeIdentifier:
					imageFilename = sbs.GetInputImage( mapping.OutputSizeIdentifier )
					
					if os.path.isfile( imageFilename ):
						img = Image.open( imageFilename )
						size = [ img.size[ 0 ], img.size[ 1 ] ]
						
						if outputResolution == RS.Core.Substance.OutputResolution.Half:
							size[ 0 ] /= 2
							size[ 1 ] /= 2
											
						elif outputResolution == RS.Core.Substance.OutputResolution.Quarter:
							size[ 0 ] /= 4
							size[ 1 ] /= 4
							
						if size[ 0 ] < 1:
							size[ 0 ] = 1
							
						if size[ 1 ] < 1:
							size[ 1 ] = 1						

						sbs.SetOutputSize( size[ 0 ], size[ 1 ] )
			
			if outputDir == None:
				outputDir = os.path.dirname( texture )
				
			if not os.path.isdir( outputDir ):
				os.makedirs( outputDir )
				
			outputs = sbs.Render( outputDir, ulog = ulog )
			
			for output in outputs:
				ulog.LogMessage( 'Rendered output texture "{0}".'.format( output ), context = 'Output' )
				
			__ProgressCurrentCount += 1
			
			# Emit Qt signal of progress.
			ProgressUpdateEvent.Update( GetBatchProgress() )
				
		ulog.LogMessage( 'Finished.' )
			
	else:
		ulog.LogError( 'Could not process because the supplied Substance name "{0}" does not have a mapping!'.format( substanceName ) )
	
	if showLog:
		ulog.Show( False )


## Classes ##

class ProgressUpdate( QtCore.QObject ):
	Updated = QtCore.Signal( int )
	
	def __init__( self ):
		QtCore.QObject.__init__( self )
		
	def Update( self, val ):
		self.Updated.emit( val )
		
class InputMapping( object ):
	def __init__( self ):
		self.Identifier = None
		self.Prefix = None
		self.Suffix = None
		self.Required = True

class SubstanceMapping( object ):
	def __init__( self ):
		self.Filename = None
		self.InputImages = {}
		self.OutputSizeIdentifier = None
		self.PresetFilename = None
		
	@property
	def Name( self ):
		return os.path.basename( self.Filename )
	
class BatchSettings( object ):
	def __init__( self ):
		self.SubstanceName = None
		self.PresetFilename = None
		self.UseTexturePreset = False
		self.TextureList = []
		self.OutputDirectory = None
		
	
	## Methods ##
	
	def Save( self, filename ):
		settings = open( filename, 'w' )
		settings.write( '{0},{1},{2},{3}\n'.format( self.SubstanceName, self.PresetFilename, self.UseTexturePreset, self.OutputDirectory ) )
		
		for texture in self.TextureList:
			settings.write( '{0}\n'.format( texture ) )
			
		settings.close()
		
	def Load( self, filename ):
		settings = open( filename, 'r' )
		lines = settings.readlines()
		settings.close()
		
		if lines:
			data = lines[ 0 ].rstrip().split( ',' )
			substanceName = data[ 0 ]
			presetFilename = data[ 1 ]
			useTexturePreset = data[ 2 ]

			# Doing it this way to keep pre-existing batch files supported, since I've added this later.
			if len( data ) > 3:
				if data[ 3 ].lower() != 'none':
					self.OutputDirectory = os.path.abspath( data[ 3 ] )
			
			self.SubstanceName = substanceName
			self.PresetFilename = presetFilename if presetFilename.lower() != 'none' else None
			self.UseTexturePreset = True if useTexturePreset.lower() == 'true' else False
			
			for i in range( 1, len( lines ) ):
				line = lines[ i ]
				
				self.TextureList.append( line.rstrip() )
	
# Signal for progress update.
ProgressUpdateEvent = ProgressUpdate()