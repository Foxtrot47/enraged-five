'''
RS.Core.Substance.Bake

Author:

	Jason Hayes <jason.hayes@rockstarsandiego.com>
	
Description:

	Library module for baking textures from meshes using Substance.
'''

import os
import sys
import subprocess

import RS.DCC.Max
import RS.Core.Substance
import RS.Core.Substance.Path
import RS.Core.NamingConvention


## Enums ##

class BakingType:
	HeightMapFromMesh		= 'Height Map from Mesh'
	VertexColorMapFromMesh	= 'Vertex Color Map from Mesh'
	NormalMapFromMesh		= 'Normal Map from Mesh'

class OutputSize:
	Size32		= 0
	Size64		= 1
	Size128		= 2
	Size256		= 3
	Size512		= 4
	Size1024	= 5
	Size2048	= 6
	Size4096	= 7

class UVSet:
	UV0	= 0
	UV1 = 1
	UV2	= 2
	
class NormalOrientation:
	OpenGL	= 0
	DirectX = 1
	
class NormalMapType:
	WorldSpace		= 0
	TangentSpace	= 1
	
	
## Baker Options ##

class OptionsBase( object ):
	def __init__( self ):
		self.OutputSize = OutputSize.Size2048
		self.UVSet = UVSet.UV0		

class HeightFromMeshOptions( OptionsBase ):
	'''
	Options for the "Height from Mesh" Substance baker.
	'''
	def __init__( self ):
		OptionsBase.__init__( self )
		
		self.LowDefinitionMeshFile = None
		self.HighDefinitionMeshFile = None
		self.UseCage = False
		self.CageFile = None
		self.AverageNormals = True
		self.RelativeToBoundingBox = False
		self.MaxFrontalDistance = 1.0
		self.MaxRearDistance = 1.0
		self.IgnoreBackface = False
		self.Antialiasing = 1
		self.AutomaticNormalization = True
		
class VertexColorFromMeshOptions( OptionsBase ):
	'''
	Options for the "Vertex Color Map from Mesh" Substance baker.
	'''
	def __init__( self ):
		OptionsBase.__init__( self )
		
		self.LowDefinitionMeshFile = None
		self.HighDefinitionMeshFile = None
		self.UseCage = False
		self.CageFile = None
		self.AverageNormals = True
		self.RelativeToBoundingBox = False
		self.MaxFrontalDistance = 1.0
		self.MaxRearDistance = 1.0
		self.IgnoreBackface = False
		self.Antialiasing = 1
			
class NormalMapFromMeshOptions( OptionsBase ):
	'''
	Options for the "Normal Map from Mesh" Substance baker.
	'''
	def __init__( self ):
		OptionsBase.__init__( self )

		self.LowDefinitionMeshFile = None
		self.HighDefinitionMeshFile = None
		self.UseCage = False
		self.CageFile = None
		self.AverageNormals = True
		self.RelativeToBoundingBox = False
		self.MaxFrontalDistance = 1.0
		self.MaxRearDistance = 1.0
		self.IgnoreBackface = False
		self.Antialiasing = 1
		self.MapType = NormalMapType.TangentSpace
		self.NormalOrientation = NormalOrientation.DirectX


## Baker Functions ##

def HeightFromMesh( outputFilename, options, useCurrent3dsmaxSelection = False ):
	'''
	Generates a height map.
	
	Arguments:
	
		outputFilename: The image filename to create.
		options: An instance of "HeightFromMeshOptions".
		
	Keyword Arguments:
	
		useCurrent3dsmaxSelection: Whether or not to use the current 3dsmax selection when baking.
	'''
	assert isinstance( options, HeightFromMeshOptions ), 'You must supply an instance of "HeightFromMeshOptions"!'
	
	if not useCurrent3dsmaxSelection:
		if options.LowDefinitionMeshFile == None:
			assert 0, 'You must set a low definition mesh file in the options!'
			
		if options.HighDefinitionMeshFile == None:
			assert 0, 'You must set a high definition mesh file in the options!'
	
	if not os.path.isdir( os.path.dirname( outputFilename ) ):
		os.makedirs( os.path.dirname( outputFilename ) )
		
	cmd = RS.Core.Substance.Path.GetSbsBakerExe()
	
	if cmd:
		if useCurrent3dsmaxSelection:
			result = ExportHighLowMeshFrom3dsmax()
			
			if result:
				lowMesh, highMesh = result
				
				options.LowDefinitionMeshFile = lowMesh
				options.HighDefinitionMeshFile = highMesh
				
			else:
				sys.stderr.write( 'An unknown error occurred while exporting the high and low FBX meshes from 3dsmax!  Cannot continue.' )
				return False
		
		cmd += ' height-from-mesh '
		cmd += '--inputs "{0}" '.format( options.LowDefinitionMeshFile )
		cmd += '--output-name {0}{1} '.format( os.path.basename( outputFilename ).split( '.' )[ 0 ], RS.Core.NamingConvention.TextureMapSuffix.HeightMap )
		cmd += '--output-path "{0}" '.format( os.path.dirname( outputFilename ) )
		cmd += '--output-format {0} '.format( os.path.basename( outputFilename ).split( '.' )[ -1 ] )
		cmd += '--output-size {0} '.format( options.OutputSize )
		cmd += '--uv-set {0} '.format( options.UVSet )
		cmd += '--highdef-mesh "{0}" '.format( options.HighDefinitionMeshFile )
		cmd += '--use-cage {0} '.format( str( options.UseCage ).lower() )
		
		if options.UseCage:
			cmd += '--cage-mesh "{0}" '.format( options.CageFile )
			
		# Hayes 12/19/2013 - Commenting this out for now.  This argument is duplicated with the "Relative to bounding box"
		# argument, which causes the baker to fail.  I've contacted Allegorithmic to fix the baker and provide us a new one.
		#cmd += '--average-normals {0} '.format( str( heightFromMeshOptions.AverageNormals ).lower() )
		
		cmd += '--max-frontal {0} '.format( options.MaxFrontalDistance )
		cmd += '--max-rear {0} '.format( options.MaxRearDistance )
		cmd += '--ignore-backface {0} '.format( str( options.IgnoreBackface ).lower() )
		cmd += '--antialiasing {0} '.format( options.Antialiasing )
		cmd += '--auto-normalize {0} '.format( str( options.AutomaticNormalization ).lower() )
		
		proc = subprocess.Popen( cmd, stdout = subprocess.PIPE )
		output, err = proc.communicate()	
		
	else:
		sys.stderr.write( 'Could not locate sbsbaker.exe! Cannot bake height map.' )
		
def VertexColorFromMesh( outputFilename, options, useCurrent3dsmaxSelection = False ):
	'''
	Generates a vertex color map.
	
	Arguments:
	
		outputFilename: The image filename to create.
		options: An instance of "VertexColorFromMeshOptions".
		
	Keyword Arguments:
	
		useCurrent3dsmaxSelection: Whether or not to use the current 3dsmax selection when baking.
	'''
	assert isinstance( options, VertexColorFromMeshOptions ), 'You must supply an instance of "VertexColorFromMeshOptions"!'
	
	if not useCurrent3dsmaxSelection:
		if options.LowDefinitionMeshFile == None:
			assert 0, 'You must set a low definition mesh file in the options!'
			
		if options.HighDefinitionMeshFile == None:
			assert 0, 'You must set a high definition mesh file in the options!'
	
	if not os.path.isdir( os.path.dirname( outputFilename ) ):
		os.makedirs( os.path.dirname( outputFilename ) )
		
	cmd = RS.Core.Substance.Path.GetSbsBakerExe()
	
	if cmd:
		if useCurrent3dsmaxSelection:
			result = ExportHighLowMeshFrom3dsmax()
			
			if result:
				lowMesh, highMesh = result
				
				options.LowDefinitionMeshFile = lowMesh
				options.HighDefinitionMeshFile = highMesh
				
			else:
				sys.stderr.write( 'An unknown error occurred while exporting the high and low FBX meshes from 3dsmax!  Cannot continue.' )
				return False
		
		cmd += ' color-from-mesh '
		cmd += '--inputs "{0}" '.format( options.LowDefinitionMeshFile )
		cmd += '--output-name {0}{1} '.format( os.path.basename( outputFilename ).split( '.' )[ 0 ], RS.Core.NamingConvention.TextureMapSuffix.ChannelMap )
		cmd += '--output-path "{0}" '.format( os.path.dirname( outputFilename ) )
		cmd += '--output-format {0} '.format( os.path.basename( outputFilename ).split( '.' )[ -1 ] )
		cmd += '--output-size {0} '.format( options.OutputSize )
		cmd += '--uv-set {0} '.format( options.UVSet )
		cmd += '--highdef-mesh "{0}" '.format( options.HighDefinitionMeshFile )
		cmd += '--use-cage {0} '.format( str( options.UseCage ).lower() )
		
		if options.UseCage:
			cmd += '--cage-mesh "{0}" '.format( options.CageFile )
			
		# Hayes 12/19/2013 - Commenting this out for now.  This argument is duplicated with the "Relative to bounding box"
		# argument, which causes the baker to fail.  I've contacted Allegorithmic to fix the baker and provide us a new one.
		#cmd += '--average-normals {0} '.format( str( heightFromMeshOptions.AverageNormals ).lower() )
		
		cmd += '--max-frontal {0} '.format( options.MaxFrontalDistance )
		cmd += '--max-rear {0} '.format( options.MaxRearDistance )
		cmd += '--ignore-backface {0} '.format( str( options.IgnoreBackface ).lower() )
		cmd += '--antialiasing {0} '.format( options.Antialiasing )
		
		proc = subprocess.Popen( cmd, stdout = subprocess.PIPE )
		output, err = proc.communicate()	
		
	else:
		sys.stderr.write( 'Could not locate sbsbaker.exe! Cannot bake height map.' )
		
def NormalFromMesh( outputFilename, options, useCurrent3dsmaxSelection = False ):
	'''
	Generates a normal map.
	
	Arguments:
	
		outputFilename: The image filename to create.
		options: An instance of "NormalMapFromMeshOptions".
		
	Keyword Arguments:
	
		useCurrent3dsmaxSelection: Whether or not to use the current 3dsmax selection when baking.
	'''
	assert isinstance( options, NormalMapFromMeshOptions ), 'You must supply an instance of "NormalMapFromMeshOptions"!'
	
	if not useCurrent3dsmaxSelection:
		if options.LowDefinitionMeshFile == None:
			assert 0, 'You must set a low definition mesh file in the options!'
			
		if options.HighDefinitionMeshFile == None:
			assert 0, 'You must set a high definition mesh file in the options!'
	
	if not os.path.isdir( os.path.dirname( outputFilename ) ):
		os.makedirs( os.path.dirname( outputFilename ) )
		
	cmd = RS.Core.Substance.Path.GetSbsBakerExe()
	
	if cmd:
		if useCurrent3dsmaxSelection:
			result = ExportHighLowMeshFrom3dsmax()
			
			if result:
				lowMesh, highMesh = result
				
				options.LowDefinitionMeshFile = lowMesh
				options.HighDefinitionMeshFile = highMesh
				
			else:
				sys.stderr.write( 'An unknown error occurred while exporting the high and low FBX meshes from 3dsmax!  Cannot continue.' )
				return False
		
		cmd += ' normal-from-mesh '
		cmd += '--inputs "{0}" '.format( options.LowDefinitionMeshFile )
		cmd += '--output-name {0}{1} '.format( os.path.basename( outputFilename ).split( '.' )[ 0 ], RS.Core.NamingConvention.TextureMapSuffix.NormalMap )
		cmd += '--output-path "{0}" '.format( os.path.dirname( outputFilename ) )
		cmd += '--output-format {0} '.format( os.path.basename( outputFilename ).split( '.' )[ -1 ] )
		cmd += '--output-size {0} '.format( options.OutputSize )
		cmd += '--uv-set {0} '.format( options.UVSet )
		cmd += '--highdef-mesh "{0}" '.format( options.HighDefinitionMeshFile )
		cmd += '--use-cage {0} '.format( str( options.UseCage ).lower() )
		
		if options.UseCage:
			cmd += '--cage-mesh "{0}" '.format( options.CageFile )
			
		# Hayes 12/19/2013 - Commenting this out for now.  This argument is duplicated with the "Relative to bounding box"
		# argument, which causes the baker to fail.  I've contacted Allegorithmic to fix the baker and provide us a new one.
		#cmd += '--average-normals {0} '.format( str( heightFromMeshOptions.AverageNormals ).lower() )
		
		cmd += '--max-frontal {0} '.format( options.MaxFrontalDistance )
		cmd += '--max-rear {0} '.format( options.MaxRearDistance )
		cmd += '--ignore-backface {0} '.format( str( options.IgnoreBackface ).lower() )
		cmd += '--antialiasing {0} '.format( options.Antialiasing )
		cmd += '--map-type {0} '.format( options.MapType )
		cmd += '--normal-invert {0} '.format( options.NormalOrientation )
		
		proc = subprocess.Popen( cmd, stdout = subprocess.PIPE )
		output, err = proc.communicate()	
		
	else:
		sys.stderr.write( 'Could not locate sbsbaker.exe! Cannot bake height map.' )
		
def BakeFactory( outputFilename, options, useCurrent3dsmaxSelection = False ):
	if isinstance( options, HeightFromMeshOptions ):
		HeightFromMesh( outputFilename, options, useCurrent3dsmaxSelection = useCurrent3dsmaxSelection )
		
	if isinstance( options, VertexColorFromMeshOptions ):
		VertexColorFromMesh( outputFilename, options, useCurrent3dsmaxSelection = useCurrent3dsmaxSelection )
		
	if isinstance( options, NormalMapFromMeshOptions ):
		NormalFromMesh( outputFilename, options, useCurrent3dsmaxSelection = useCurrent3dsmaxSelection )
		
def ExportHighLowMeshFrom3dsmax():
	'''
	'''
	maxApp = RS.DCC.Max.Application( launch = False )
	
	if maxApp:
		lowFbxFilename = '{0}/low_mesh.fbx'.format( RS.Core.Substance.Path.GetTempDir() )
		highFbxFilename = '{0}/high_mesh.fbx'.format( RS.Core.Substance.Path.GetTempDir() )
		
		# Export current 3dsmax selection to FBX.
		cmd = '''
		(
			local objs = selection as array
			
			if objs.count == 2 then (
			    local lowMesh = objs[ 1 ]
			    local hiMesh = objs[ 2 ]
			    
			    select lowMesh
			    exportFile ( RSMakeSafeSlashes "{lowFbx}" ) #noprompt selectedonly:true
			    
			    select hiMesh
			    exportFile ( RSMakeSafeSlashes "{hiFbx}" ) #noprompt selectedonly:true
		        
		        -- Reset selection.
		        select lowMesh
		        selectmore hiMesh
			    
			    return True
			) else (
		    	messageBox "Please select two meshes!  First is the low mesh, second is the high mesh." title:"Rockstar"
			    return False
			)
		)
		'''.format( lowFbx = lowFbxFilename, hiFbx = highFbxFilename )
		
		success = maxApp.RunCommand( cmd )
		
		if success:
			return ( lowFbxFilename, highFbxFilename )
		
	else:
		sys.stderr.write( '3dsmax is not running!  Please open the application and select two meshes.' )
		
	return None
