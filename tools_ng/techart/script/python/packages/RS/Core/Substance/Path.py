'''
RS.Core.Substance.Path

Author:

	Jason Hayes <jason.hayes@rockstarsandiego.com>

Description:

	Module to get various paths related to Substance, such as installation directories, and exe's.
'''

import os
import _winreg

import RS.Config
import RS.Core.Substance


def GetProjectSubstanceDir():
	return '{0}\\techart\\substance'.format( RS.Config.Project.Path.Art )

def GetProjectArchivesDir():
	return '{0}\\archives'.format( GetProjectSubstanceDir() )

def GetBatchToolsInstallDir():
	path = '{0}\\bin\\substance\\batchTools\\{1}.x'.format( RS.Config.Tool.Path.Root, RS.Core.Substance.BATCH_TOOLS_VERSION )
	return path

def GetDesignerInstallDir():
	reg = _winreg.ConnectRegistry( None, _winreg.HKEY_LOCAL_MACHINE )
	key = _winreg.OpenKey( reg, r'SOFTWARE\Wow6432Node\Allegorithmic\Substance\designer\{0}_x\install'.format( RS.Core.Substance.DESIGNER_VERSION ), 0, _winreg.KEY_ALL_ACCESS )
	
	if key:
		installDir = _winreg.QueryValueEx( key, 'installDir' )[ 0 ]
		return os.path.abspath( installDir )
	
	return None

def GetTempDir():
	
	# Gotta format path this way because of stoopid 3dsmax.
	tempDir = 'x:/{0}/cache/raw/substance'.format( RS.Config.Project.Name )
	
	if not os.path.isdir( tempDir ):
		os.makedirs( tempDir )
		
	return tempDir

def GetResourcesDir():
	'''
	Returns the Designer application resources directory.  Necessary when compiling .sbs files into a single archive (.sbsar).
	'''
	path = '{0}\\bin\\substance\\resources\\{1}.x'.format( RS.Config.Tool.Path.Root, RS.Core.Substance.DESIGNER_VERSION )
	return path

def GetSbsRenderExe():
	installDir = GetBatchToolsInstallDir()
	
	if os.path.isdir( installDir ):
		sbsRenderExe = '{0}\\sbsrender.exe'.format( installDir )
		
		if os.path.isfile( sbsRenderExe ):
			return sbsRenderExe
		
		else:
			print 'sbsrender.exe does not exist!  Was expecting it here:', sbsRenderExe
		
	else:
		print 'Path does not exist!', installDir
		
	return None

def GetSbsCookerExe():
	installDir = GetBatchToolsInstallDir()
	
	if os.path.isdir( installDir ):
		sbsCookerExe = '{0}\\sbscooker.exe'.format( installDir )
		
		if os.path.isfile( sbsCookerExe ):
			return sbsCookerExe
		
		else:
			print 'sbscooker.exe does not exist!  Was expecting it here:', sbsCookerExe
		
	else:
		print 'Path does not exist!', installDir
		
	return None

def GetSbsBakerExe():
	installDir = GetBatchToolsInstallDir()
	
	if os.path.isdir( installDir ):
		sbsBakerExe = '{0}\\sbsbaker.exe'.format( installDir )
		
		if os.path.isfile( sbsBakerExe ):
			return sbsBakerExe
		
		else:
			print 'sbsbaker.exe does not exist!  Was expecting it here:', sbsBakerExe
		
	else:
		print 'Path does not exist!', installDir
		
	return None

def GetSbsMutatorExe():
	installDir = GetBatchToolsInstallDir()
	
	if os.path.isdir( installDir ):
		sbsMutatorExe = '{0}\\sbsmutator.exe'.format( installDir )
		
		if os.path.isfile( sbsMutatorExe ):
			return sbsMutatorExe
		
		else:
			print 'sbsmutator.exe does not exist!  Was expecting it here:', sbsMutatorExe
		
	else:
		print 'Path does not exist!', installDir
		
	return None

def GetBatchXml():
	batchXml = '{0}\\etc\\config\\substance\\SubstanceBatch.xml'.format( RS.Config.Tool.Path.TechArt )
	
	if os.path.isfile( batchXml ):
		return batchXml
	
	else:
		print 'The Substance batch xml file does not exist!  Was expecting it here:', batchXml
		return None