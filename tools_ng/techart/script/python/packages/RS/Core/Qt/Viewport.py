from PySide import QtGui, QtCore


class BaseViewport( QtGui.QGraphicsView ):
    def __init__( self, scene, parent, imageItem, *args, **kwargs ):
        QtGui.QGraphicsView.__init__( self, scene, parent, *args, **kwargs )
        
        self.setRenderHint( QtGui.QPainter.Antialiasing, True )
        self.setViewportUpdateMode( QtGui.QGraphicsView.FullViewportUpdate )
        self.setBackgroundBrush( QtGui.QBrush( QtGui.QColor( 30, 30, 30 ) ) )
        self.setDragMode( QtGui.QGraphicsView.ScrollHandDrag )
        self.setHorizontalScrollBarPolicy( QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff )
        self.setVerticalScrollBarPolicy( QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff )
        self.setFocusPolicy( QtCore.Qt.NoFocus )
        self.setAlignment( QtCore.Qt.AlignCenter | QtCore.Qt.AlignTop )
        
        self.__zoomFactor = 1.1
        self.__middlePressed = False
        self.__imageItem = imageItem
        
    
    ## Methods ##
        
    def ZoomExtents( self ):
        self.fitInView( self.__imageItem, aspectRadioMode = QtCore.Qt.AspectRatioMode.KeepAspectRatio )
        
    def Center( self ):
        r = self.sceneRect()
        x = r.center().x() - ( self.__imageItem.sceneBoundingRect().width() / 2.0 )
        y = r.center().y()
        
        self.__imageItem.setPos( x, y )
        
        
    ## Overrides ##
    
    def resizeEvent( self, event ):
        QtGui.QGraphicsView.resizeEvent( self, event )
        
    def mouseDoubleClickEvent( self, event ):
        self.ZoomExtents()
        
        QtGui.QGraphicsView.mouseDoubleClickEvent( self, event )
        
    def mousePressEvent( self, event ):
        self.__lastMousePos = event.pos()
        
        self.__middlePressed = True if event.button() == QtCore.Qt.MouseButton.MiddleButton else False    

        if self.__middlePressed:
            self.setDragMode( QtGui.QGraphicsView.ScrollHandDrag )
            
        else:
            self.setDragMode( QtGui.QGraphicsView.NoDrag )
            
        QtGui.QGraphicsView.mousePressEvent( self, event )
    
    def mouseMoveEvent( self, event ):
        if self.__middlePressed:
            delta = event.pos() - self.__lastMousePos
            
            hBar = self.horizontalScrollBar()
            vBar = self.verticalScrollBar()
            
            hBar.setValue( hBar.value() - delta.x() )
            vBar.setValue( vBar.value() - delta.y() )
            
            self.__lastMousePos = event.pos()
            
        QtGui.QGraphicsView.mouseMoveEvent( self, event )
            
    def mouseReleaseEvent( self, event ):
        if self.__middlePressed:
            self.setDragMode( QtGui.QGraphicsView.NoDrag )
            
        self.__middlePressed = False
        
        QtGui.QGraphicsView.mouseReleaseEvent( self, event )

    def wheelEvent( self, event ):
        self.setTransformationAnchor( QtGui.QGraphicsView.ViewportAnchor.AnchorUnderMouse )
        
        if event.delta() > 0:
            self.scale( self.__zoomFactor, self.__zoomFactor )
            
        else:
            self.scale( 1.0 / self.__zoomFactor, 1.0 / self.__zoomFactor )