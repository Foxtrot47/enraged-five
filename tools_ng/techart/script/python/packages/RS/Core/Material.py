import os
import xml.etree.cElementTree

import RS.Config

MATERIAL_SURFACES_XML = '{0}\\etc\\config\\material\\materialSurfaces.xml'.format( RS.Config.Tool.Path.TechArt )
MATERIAL_WORKFLOW_XML = '{0}\\etc\\config\\material\\materialWorkflow.xml'.format( RS.Config.Tool.Path.TechArt )

__MaterialSurfaceGroups = {}


## Classes ##

class MaterialSurface( object ):
	def __init__( self ):
		self.Name = None
		self.ChannelRGB = None
		self.Roughness = 0
		
class MaterialSurfaceGroup( object ):
	def __init__( self ):
		self.Name = None
		self.Surfaces = {}
		
	
## Functions ##

def LoadMaterialSurfaces():
	global __MaterialSurfaceGroups
	
	if os.path.isfile( MATERIAL_SURFACES_XML ):
		doc = xml.etree.cElementTree.parse( MATERIAL_SURFACES_XML )
		root = doc.getroot()
		
		if root:
			groupNodes = root.findall( 'Group' )
			
			for groupNode in groupNodes:
				name = groupNode.get( 'name' )
				
				surfaceGroup = MaterialSurfaceGroup()
				surfaceGroup.Name = name
				
				surfaceNodes = groupNode.findall( 'Surface' )
				
				for surfaceNode in surfaceNodes:
					surfaceName = surfaceNode.get( 'name' )
					surfaceChannelRGB = surfaceNode.get( 'channelRGB' )
					surfaceRoughness = surfaceNode.get( 'roughness' )
					
					r, g, b = surfaceChannelRGB.split( ',' )
					
					surface = MaterialSurface()
					surface.Name = surfaceName
					surface.ChannelRGB = ( int( r ), int( g ), int( b ) )
					surface.Roughness = float( surfaceRoughness )
					
					surfaceGroup.Surfaces[ surfaceName ] = surface
					
				__MaterialSurfaceGroups[ name ] = surfaceGroup
	
	else:
		print 'The material surfaces xml could not be found!  Was expecting it here: {0}'.format( MATERIAL_SURFACES_XML )
		
def GetMaterialSurfaces():
	global __MaterialSurfaceGroups
	
	if not __MaterialSurfaceGroups:
		LoadMaterialSurfaces()
		
	return __MaterialSurfaceGroups
		
if __name__ == '__main__':
	LoadMaterialSurfaces()