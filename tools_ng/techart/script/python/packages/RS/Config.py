'''
Module for accessing configuration and settings.

Provides access to the current Project, User, Environment( Motionbuilder ) and installed Tools.

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
Author: Ross George <ross.george@rockstarlondon.com>
'''

import clr
import sys
import os
import inspect

import RS.Core.Metadata

clr.AddReference("RSG.Base.Configuration")
from RSG.Base.Configuration import ConfigFactory

#Create our .NET config object which we will extract all configuration settings from
_Config = ConfigFactory.CreateConfig()

class Branch( object ):
    '''Represents a branch within a project.'''        
    def __init__( self, in_CLRBranch ):

        self._Name = in_CLRBranch.Name      
        self._IsDefault = in_CLRBranch.IsDefault   
        
        class Path:
            '''Nested singleton which contains all paths related to the branch.'''               
            def __init__( self ):
                self._Common = str( in_CLRBranch.Common )
                self._Build = str( in_CLRBranch.Build )
                self._Audio = str( in_CLRBranch.Audio )
                self._Code = str( in_CLRBranch.Code )
                
            @property
            def Common(self):
                '''Common root path.'''
                return self._Common             
            @property
            def Build(self):
                '''Build root path.'''
                return self._Build  
            @property
            def Audio(self):
                '''Audio root path.'''
                return self._Audio  
            @property
            def Code(self):
                '''Code root path.'''
                return self._Code              
        self._Path = Path()
        
    @property
    def Name(self):
        '''Name of the branch.'''
        return self._Name
    @property
    def IsDefault(self):
        '''Indicates if the branch is the default branch.'''
        return self._IsDefault  
    @property
    def Path(self):
        '''Paths related to the branch'''
        return self._Path
    
class Project( object ):
    '''Provides access to project specific settings and configuration.'''
    def __init__( self ):
        
        class Path( object ):
            '''Nested singleton which contains all root level paths for the project.'''        
            def __init__( self ):
                self._Cache = str( _Config.Project.Cache )
                self._Root = str( _Config.Project.Root )
                self._Art = str( _Config.Project.DefaultBranch.Art )
                self._Anim = str( _Config.Project.DefaultBranch.Anim )
                self._Assets = str( _Config.Project.DefaultBranch.Assets )
                self._Processed = str( _Config.Project.DefaultBranch.Processed )
                self._Export = str( _Config.Project.DefaultBranch.Export )
                self._Metadata = str( _Config.Project.DefaultBranch.Metadata )
                
            @property
            def Cache(self):
                '''Project export cache root path.'''
                return self._Cache    
            @property
            def Root(self):
                '''Project root path.'''
                return self._Root
            @property
            def Art(self):
                '''Project art source root path.'''
                return self._Art
            @property
            def Anim(self):
                '''Project anim source root path.'''
                return self._Anim
            @property
            def Assets(self):
                '''Project assets root path.'''
                return self._Assets
            @property
            def Processed(self):
                '''Project processed root path.'''
                return self._Processed
            @property
            def Export(self):
                '''Project export root path.'''
                return self._Export
            @property
            def Metadata(self):
                '''Project metadata root path.'''
                return self._Metadata

        self._Name = _Config.Project.FriendlyName      
        self._Path = Path()

        self._DefaultBranch = None
        self._Branches = list()
        
        for CLRBranch in _Config.Project.Branches:
            CurrentBranch = Branch( CLRBranch.Value )
            self._Branches.append( CurrentBranch )           
            if( CurrentBranch.Name == _Config.Project.DefaultBranch.Name ):
                    self._DefaultBranch = CurrentBranch
                    self._DefaultBranch._IsDefault = True            
                
    @property
    def Name(self):
        '''Name of the project.'''
        return self._Name 
    @property
    def Path(self):
        '''Paths related to the project'''
        return self._Path      
    @property
    def Branches(self):
        '''Branches for the current project'''
        return self._Branches
    @property
    def DefaultBranch(self):
        '''The default branch for the project'''
        return self._DefaultBranch
    
class User( object ):
    '''Provides access to properties and settings related to the current user.'''
    def __init__( self ):
        self._Name = str( _Config.Username )
        self._Domain = str( _Config.Studios.ThisStudio.Domain )
        self._Studio = str( _Config.Studios.ThisStudio.FriendlyName )
        self._Email = str( _Config.EmailAddress )
        self._IsExternal = False
        self._Type = NotImplemented
        self._ExchangeServer = str( _Config.Studios.ThisStudio.ExchangeServer )
        
    @property
    def Name(self):
        '''Name of the user.'''
        return self._Name 
    @property
    def Domain(self):
        '''Domain the user belongs to.'''
        return self._Domain      
    @property
    def Studio(self):
        '''Studio the user belongs to.'''
        return self._Studio
    @property
    def Email(self):
        '''Email address of the user'''
        return self._Email
    @property
    def IsExternal(self):
        '''Indicates whether the user is external (outsourcer) or not'''
        return self._IsExternal
    @property
    def Type(self):
        '''Type of user (e.g. Animator, Artist, Tools Developer)'''
        return self._Type
    @property
    def ExchangeServer( self ):
        '''Users exhange server'''
        return self._ExchangeServer       
    
class Tool( object ):
    '''Provides access to information and settings about the tools currently installed.'''
    def __init__( self ):
        
        class Path( object ):
            '''Nested singleton which contains relevant paths for tools.'''        
            def __init__( self ):
                self._Library = str( _Config.ToolsLib )
                self._Bin = str( _Config.ToolsBin )
                self._Logs = str( _Config.ToolsLogs )
                self._Root = str( _Config.ToolsRoot )
                self._Config = str( _Config.ToolsConfig )
                self._Drive = str( _Config.ToolsDrive )
                self._Temp = str( _Config.ToolsTemp )
                self._TechArt = '{0}\\techart'.format( self._Root )
                
                currentFile = str( os.path.abspath( inspect.getfile( inspect.currentframe() ) ) ).lower()
                
                if 'x:\\wildwest' in currentFile:
                    self._TechArt = 'x:\\wildwest'
                
            @property
            def TechArt( self ):
                '''Tech Art directory root path.'''
                return self._TechArt            
                            
            @property
            def Library(self):
                '''Tools root library path.'''
                return self._Library    
            @property
            def Bin(self):
                '''Tools root bin path.'''
                return self._Bin
            @property
            def Logs(self):
                '''Tools log path.'''
                return self._Logs
            @property
            def Root(self):
                '''Tools root path.'''
                return self._Root
            @property
            def Config(self):
                '''Tools configuration path.'''
                return self._Config
            @property
            def Drive(self):
                '''Tools root drive.'''
                return self._Drive
            @property
            def Temp(self):
                '''Tools temporary path.'''
                return self._Temp
            
        class Executable( object ):
            '''Nested singleton which contains relevant executable paths for tools.'''        
            def __init__( self ):
                self._LogViewer = '{0}\\bin\\UniversalLogViewer\\UniversalLogViewer.exe'.format( _Config.ToolsLib ) 
                
            @property
            def LogViewer(self):
                '''Log viewer .exe path.'''
                return self._LogViewer    

        self._Version = _Config.Version.InstalledVersion.LabelName        
        self._Path = Path() 
        self._Executable = Executable()
    
    @property
    def Version(self):
        '''Currently install of tools version.'''
        return self._Version 
    @property
    def Path(self):
        '''Useful paths related to tools.'''
        return self._Path
    @property
    def Executable(self):
        '''Useful executable paths (mainly from the bin directory).'''
        return self._Executable  
          
        
class Script( object ):
    '''Provides access to information about the python script framework for this session of Motionbuilder.'''
    
    def __init__( self ):        
        
        class Path( object ):
            '''Nested singleton which contains relevant paths for scripts.'''        
            def __init__( self ):

                currentFile = str( os.path.abspath( inspect.getfile( inspect.currentframe() ) ) ).lower()
                
                self._Root = os.path.abspath( '{0}/../..'.format( os.path.dirname( currentFile ) ) )
                self._RockstarRoot = '{0}\\packages\\RS'.format( self._Root )
                           
            @property
            def Root(self):
                '''Script root path.'''
                return self._Root
            
            @property
            def RockstarRoot(self):
                '''Rockstar script root path.'''
                return self._RockstarRoot

            @property
            def ToolImages(self):
                return os.path.abspath( '{0}\\images'.format( self._Root ) )

            @property
            def StandaloneRoot(self):
                return os.path.abspath( '{0}\\standalone'.format( self._Root ) )

            @property
            def PackagesRoot(self):
                return os.path.abspath( '{0}\\packages'.format( self._Root ) )
            
        self._Path = Path()
    
    @property
    def Path(self):
        '''Useful paths related to script.'''
        return self._Path             
    @property
    def DevelopmentMode(self):
        '''Indicates if the session of MotionBuilder is in development mode.'''
        return self._DevelopmentMode 
    @property
    def TargetBuildVersion(self):
        '''The build version of MotionBuilder the scripts are designed to work with.'''
        return self._TargetBuildVersion
    @property
    def TargetBuildTitle(self):
        '''Human friendly name for the TargetBuildVersion.'''
        return self._TargetBuildTitle     
    @property
    def DependencyPathList(self):
        '''Paths to files and directories that MotionBuilder is dependant on.'''
        return self._DependencyPathList    
        
#Overwrite the Project class definition with a single instance of Project
Project = Project( ) 
#Overwrite the User class definition with a single instance of User  
User = User( )
#Overwrite the Tool class definition with a single instance of User  
Tool = Tool( )
#Overwrite the Environment class definition with a single instance of User  
Script = Script( )

