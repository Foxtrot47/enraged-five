import os
import subprocess
import datetime
import win32com.client

import RS.Utils.Process

class MaxscriptException( Exception ):
    pass

class Application( object ):
    '''
    Provides a COM interface for working with 3dsmax.
    '''
    def __init__( self, launch = True ):
        if not self.IsOpen():
            if launch:
                self.Launch()
            
            else:
                raise WindowsError( '3dsmax is not running!' )
        
        self.__app = win32com.client.Dispatch( 'MAX.Application' )
        self.__app._FlagAsMethod( 'ComInterfaceCommand' )
        
        self.__commandQueue = []
        
    
    ## Command Methods ##
    
    def FlushCommandQueue( self ):
        self.__commandQueue = []
        
    def QueueCommand( self, cmd ):
        self.__commandQueue.append( cmd )
        
    def RunCommand( self, cmd ):
        '''
        Executes a Maxscript command.
        '''
        result = str( self.__app.ComInterfaceCommand( cmd ) )
        
        if '[MXSException]' in result:
            result = result[ 15: ]
            raise MaxscriptException( result )
        
        try:
            result = eval( result )
            
        except:
            pass
        
        return result
        
    def RunCommandQueue( self ):
        for cmd in self.__commandQueue:
            self.RunCommand( cmd )
            
        self.FlushCommandQueue()    

    
    ## Convenience Methods ##
    
    def IsOpen( self ):
        '''
        Checks to see if 3dsmax is running.
        '''
        return RS.Utils.Process.IsRunning( '3dsmax.exe' )
        
    def Launch( self ):
        '''
        Launch 3dsmax.
        '''
        
        # TODO (Hayes 8/23/2013): Query max installation path from registry.
        proc = subprocess.Popen( 'c:\\3dsmax2012\\3ds Max 2012\\3dsmax.exe' )
        
        maxAppOpened = False
        timeStarted = datetime.datetime.now()
        
        # Raise an assert if 3dsmax took longer than 5 minutes to open.
        timeout = 5 * 60
        
        while not maxAppOpened:
            try:
                self.__app = win32com.client.Dispatch( 'MAX.Application' )
                self.__app._FlagAsMethod( 'ComInterfaceCommand' )
                
                maxAppOpened = True
                
            except:
                pass
            
            elapsedTime = datetime.datetime.now() - timeStarted
            
            if elapsedTime.seconds >= timeout:
                raise WindowsError( 'The timeout period for determining if 3dsmax is open has expired.' )
        
    def Close( self ):
        '''
        Close 3dsmax.
        '''
        RS.Utils.Process.Kill( '3dsmax.exe' )
        
    def InheritContainer( self, maxFilename ):
        '''
        Inherit a container.
        '''
        if not os.path.isfile( maxFilename ):
            raise IOError( '{0} does not exist!'.format( maxFilename ) )
        
        cmd = 'Containers.CreateInheritedContainer "{0}"'.format( maxFilename )        
        self.RunCommand( cmd )
        
    def ResetFile( self, noPrompt = True ):
        '''
        Reset the currently loaded max file.
        '''
        cmd = 'resetMaxFile'
        
        if noPrompt:
            cmd += ' #noPrompt'
            
        else:
            cmd += '()'
            
        self.RunCommand( cmd )
        
    def OpenFile( self, maxFilename, quiet = False ):
        '''
        Open a max file.
        '''
        if not os.path.isfile( maxFilename ):
            raise IOError( '{0} does not exist!'.format( maxFilename ) )        

        maxFilename = maxFilename.lower()
        
        # Inherit container if a container max file.
        if maxFilename.endswith( '.maxc' ):
            self.InheritContainer( maxFilename )
        
        else:
            self.RunCommand( 'loadMaxFile "{0}" quiet:{1}'.format( maxFilename, quiet ) )
        
    def RunMaxscriptFile( self, mxsFilename ):
        '''
        Runs a Maxscript file.
        '''
        self.RunCommand( 'filein "{0}"'.format( mxsFilename ) )    
