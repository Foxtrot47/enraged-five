'''
RS.DCC.Photoshop

Author:

    Jason Hayes <jason.hayes@rockstarsandiego.com>
    
Description:

    Interface for working with Photoshop.
    
Example:

    import RS.DCC.Photoshop
    
    psApp = RS.DCC.Photoshop.Application()
    
    # Get active document
    doc = psApp.ActiveDocument
    
    # Get open documents
    docs = psApp.GetOpenDocuments()
    
    # Open a file
    psApp.Open( filename )
    
    # Get layers
    layers = doc.GetLayers()
    
    # Get layer groups
    groups = doc.GetLayerGroups()
    
    # Iterate through groups and layers.
    for group in groups:
        subGroups = group.GetLayerGroups()
        subLayers = group.GetLayers()
        
        for subLayer in subLayers:
            print subLayer.Name
        
'''

import os
import win32com.client


## Enums ##

class ElementPlacement:
    Inside          = 0
    AtBeginning     = 1
    AtEnd           = 2
    Before          = 3
    After           = 4

class ResampleMethod:
    NoResampling    = 1
    NearestNeighbor = 2
    Bilinear        = 3
    Bicubic         = 4
    BicubicSharper  = 5
    BicubicSmoother = 6

class TargaBitsPerPixel:
    Targa16 = 16
    Targa24 = 24
    Targa32 = 32

class TiffCompression:
    NoCompression   = 1
    LZW             = 2
    JPEG            = 3
    ZIP             = 4

class NewDocumentMode:
    Gray            = 1
    RGB             = 2
    CMYK            = 3
    Lab             = 4
    Bitmap          = 5

## Functions ##

def SaveOptionsFactory( fileName, 
                        saveAlphaChannel = True, 
                        saveLayers = True, 
                        compressionType = None, 
                        interlaced = False, 
                        RLECompression = True,
                        bitsPerPixel = TargaBitsPerPixel.Targa24 ):
    '''
    Based on the file extension in the supplied filename, return an 
    appropriate Photoshop SaveOptions object for the file type.
    
    Arguments:
    
        filename: Full filename.
        
    Returns:
    
        A Photoshop SaveOptions object.
    '''
    fileExt = os.path.basename( fileName ).split( '.' )[ -1 ].lower()
    options = None
    
    if fileExt == 'tiff' or fileExt == 'tif':
        options = win32com.client.Dispatch( 'Photoshop.TIFFSaveOptions' )
        options.Layers = saveLayers
        options.AlphaChannels = saveAlphaChannel
        
        if compressionType:
            options.ImageCompression = compressionType
            
        else:
            options.ImageCompression = TiffCompression.NoCompression
        
    elif fileExt == 'png':
        options = win32com.client.Dispatch( 'Photoshop.PNGSaveOptions' )
        options.Interlaced = interlaced
        
    elif fileExt == 'tga':
        options = win32com.client.Dispatch( 'Photoshop.TargaSaveOptions' )
        options.AlphaChannels = saveAlphaChannel
        options.Resolution = bitsPerPixel
        options.RLECompression = RLECompression
        
    elif fileExt == 'psd':
        options = win32com.client.Dispatch( 'Photoshop.PhotoshopSaveOptions' )
        options.AlphaChannels = saveAlphaChannel
        options.Layers = saveLayers
        
    return options
        
def GetColor( r, g, b ):
    '''
    Create a Photoshop SolidColor object based on the supplied RGB color.
    
    Arguments:
    
        r: Red
        g: Green
        b: Blue
        
    Returns:
    
        A Photoshop SolidColor object.
    '''
    colorObj = win32com.client.Dispatch( 'Photoshop.SolidColor' )
    colorObj.RGB.Red = r
    colorObj.RGB.Green = g
    colorObj.RGB.Blue = b 
    
    return colorObj


## Classes ##

class Layer( object ):
    '''
    Simple wrapper around a Photoshop Layer object.
    '''
    def __init__( self, obj ):
        self.__obj = obj
        
    
    ## Properties ##
    
    @property
    def Object( self ):
        return self.__obj
    
    @property
    def IsBackgroundLayer( self ):
        return self.__obj.IsBackgroundLayer
    
    @property
    def Name( self ):
        return self.__obj.Name
    
    @Name.setter
    def Name( self, name ):
        self.__obj.Name = name
    
    @property
    def Visible( self ):
        return self.__obj.Visible
    
    @Visible.setter
    def Visible( self, state ):
        self.__obj.Visible = state
        
    @property
    def Opacity( self ):
        return self.__obj.Opacity
        
    @Opacity.setter
    def Opacity( self, val ):
        self.__obj.Opacity = val
        

    ## Methods ##
    
    def Clear( self ):
        self.__obj.Clear()
    
    def Copy( self, merge = False ):
        self.__obj.Copy( merge )
    
    def Link( self, layer ):
        self.__obj.Link( layer.Object )
        
    def Unlink( self ):
        self.__obj.Unlink()
        
    def Move( self, target, placement = ElementPlacement.Before ):
        
        # Placement is only supported for certain types depending on if Layer or LayerSet.
        if not self.IsBackgroundLayer:
            if isinstance( target, Layer ):
                if placement == ElementPlacement.Before or placement == ElementPlacement.After:
                    self.__obj.Move( target.Object, placement )
                    
                else:
                    assert 0, 'Supplied an unsupported placement type for a Layer!  Must be either "Before" or "After".'
            
            elif isinstance( target, LayerGroup ):
                if placement == ElementPlacement.Inside or placement == ElementPlacement.Before:
                    self.__obj.Move( target.Object, placement )
                    
                else:
                    assert 0, 'Supplied an unsupported placement type for a LayerSet!  Must be either "Before" or "Inside".'
        
        
class LayerGroup( object ):
    '''
    Simple wrapper for a Photoshop LayerSet object.
    '''
    def __init__( self, obj ):
        self.__obj = obj
        
    
    ## Properties ##
    
    @property
    def Object( self ):
        return self.__obj    
    
    @property
    def Name( self ):
        return self.__obj.Name
    
    @Name.setter
    def Name( self, name ):
        self.__obj.Name = name
        
    @property
    def Visible( self ):
        return self.__obj.Visible
    
    @Visible.setter
    def Visible( self, state ):
        self.__obj.Visible = state
        
    
    ## Methods ##
    
    def AddLayer( self, name = None, visible = True ):
        obj = self.__obj.ArtLayers.Add()
        
        if name:
            obj.Name = name
            
        obj.Visible = visible
        
        return Layer( obj )
    
    def AddLayerGroup( self, name = None, visible = True ):
        obj = self.__obj.LayerSets.Add()
        
        if name:
            obj.Name = name
            
        obj.Visible = visible
            
        return LayerGroup( obj )
    
    def GetLayerGroups( self ):
        layerGroups = []
        
        for obj in self.__obj.LayerSets:
            layerGroup = LayerGroup( obj )
            layerGroups.append( layerGroup )
            
        return layerGroups
    
    def GetLayers( self ):
        layers = []
        
        for obj in self.__obj.ArtLayers:
            layer = Layer( obj )
            layers.append( layer )
        
        return layers
        

class Document( object ):
    def __init__( self, obj ):
        self.__obj = obj
        
    
    ## Properties ##
    
    @property
    def Object( self ):
        return self.__obj
    
    @property
    def Name( self ):
        return self.__obj.Name
    
    @property
    def FullName( self ):
        return self.__obj.FullName
    
    @property
    def Height( self ):
        return self.__obj.Height
    
    @property
    def Width( self ):
        return self.__obj.Width
    
    @property
    def Path( self ):
        return self.__obj.Path
    
    @property
    def PixelAspectRatio( self ):
        return self.__obj.PixelAspectRatio
    
    @PixelAspectRatio.setter
    def PixelAspectRatio( self, val ):
        self.__obj.PixelAspectRatio = val
        
    @property
    def Saved( self ):
        return self.__obj.Saved
    
    @property
    def Selection( self ):
        return self.__obj.Selection
    
    @property
    def ActiveChannels( self ):
        return self.__obj.ActiveChannels    
        
        
    ## Private Methods ##
    
    def __RecurseFindLayerGroup( self, groupName, rootGroup ):
        groups = rootGroup.GetLayerGroups()
                
        for group in groups:
            if group.Name == groupName:
                return group
            
            else:
                self.__RecurseFindLayerGroup( groupName, group )     
    
    
    def __RecurseFindLayer( self, layerName, rootGroup ):
        groups = rootGroup.GetLayerGroups()
                
        for group in groups:
            layers = group.GetLayers()
            
            for layer in layers:
                if layer.Name == layerName:
                    return layer
                
            result = self.__RecurseFindLayer( layerName, group )
            
            if result:
                return result
    
    ## Methods ##
    
    def AddChannel( self, name ):
        obj = self.__obj.Channels.Add()
        obj.Name = name
    
    def SetChannelsVisibility( self, red = True, green = True, blue = True, alpha = True ):
        for channel in self.__obj.Channels:
            if channel.Name == 'Red':
                channel.Visible = red
                
            if channel.Name == 'Green':
                channel.Visible = green
                
            if channel.Name == 'Blue':
                channel.Visible = blue            
                
    def SetActiveChannels( self, red = True, green = True, blue = True, alpha = True ):
        channels = []
        
        for channel in self.__obj.Channels:
            if channel.Name == 'Red' and red:
                channels.append( channel )
                
            if channel.Name == 'Green' and green:
                channels.append( channel ) 
                
            if channel.Name == 'Blue' and blue:
                channels.append( channel )
                
            if channel.Name == 'Alpha' and alpha:
                channels.append( channel )
                
        self.__obj.ActiveChannels = channels
    
    def CopySelection( self, merge = False ):
        self.__obj.Selection.Copy( merge )
    
    def SelectAll( self ):
        self.__obj.Selection.SelectAll()
        
    def Deselect( self ):
        self.__obj.Selection.Deselect()
        
    def PasteSelection( self, intoSelection = False ):
        obj = self.__obj.Paste( intoSelection )
        return Layer( obj )
    
    def ResizeImage( self, width, height, resolution, resampleMethod = ResampleMethod.Bicubic ):
        self.__obj.ResizeImage( width, height, resolution, resampleMethod )
    
    def RotateCanvas( self, angle ):
        self.__obj.RotateCanvas( angle )
    
    def Duplicate( self, name = None, duplicateMergedLayersOnly = False ):
        if name == None:
            name = self.Name + '_copy'
            
        obj = self.__obj.Duplicate( name, duplicateMergedLayersOnly )
        
        return Document( obj )
        
    def MergeVisibleLayers( self ):
        self.__obj.MergeVisibleLayers()
    
    def Flatten( self ):
        self.__obj.Flatten()
    
    def Close( self ):
        self.__obj.Close()
    
    def Save( self ):
        self.__obj.Save()
    
    def SaveAs( self, filename, saveOptions = None ):
        if saveOptions:
            options = saveOptions
            
        else:
            options = SaveOptionsFactory( filename )
        
        if options:
            self.__obj.SaveAs( filename, options )
            
        else:
            assert 0, 'Supplied file extension does not have supported save options!  You might need to add them.'
    
    def AddLayerGroup( self, name = None, visible = True ):
        obj = self.__obj.LayerSets.Add()
        
        if name:
            obj.Name = name
            
        obj.Visible = visible
        
        return LayerGroup( obj )
    
    def AddLayer( self, name = None, visible = True ):
        obj = self.__obj.ArtLayers.Add()
        
        if name:
            obj.Name = name
            
        obj.Visible = visible
        
        return Layer( obj )
    
    def GetLayerGroups( self ):
        layerGroups = []
        
        for obj in self.__obj.LayerSets:
            layerGroup = LayerGroup( obj )
            layerGroups.append( layerGroup )
            
        return layerGroups
    
    def GetLayersInGroup( self, groupName ):
        layers = []
        
        group = self.GetLayerGroup( groupName )
        
        if group:
            return group.GetLayers()
        
    def FindLayer( self, layerName ):
        rootLayers = self.GetLayers()
        
        for layer in rootLayers:
            if layer.Name == layerName:
                return layer
            
        rootGroups = self.GetLayerGroups()
        
        for group in rootGroups:
            layers = group.GetLayers()
            
            for layer in layers:
                if layer.Name == layerName:
                    return layer
                
            result = self.__RecurseFindLayer( layerName, group )
            
            if result:
                return result
        
    def FindLayerGroup( self, groupName ):
        rootGroups = self.GetLayerGroups()
        
        group = None
        
        for rootGroup in rootGroups:
            if rootGroup.Name == groupName:
                return rootGroup
            
            else:
                group = self.__RecurseFindLayerGroup( groupName, rootGroup )
                
                if group:
                    return group
            
        return group
            
    def GetLayers( self ):
        layers = []
        
        for obj in self.__obj.ArtLayers:
            layer = Layer( obj )
            layers.append( layer )
            
        return layers
    
    def SetActiveLayer( self, layer ):
        self.__obj.ActiveLayer = layer.Object
        
    def GetActiveLayer( self ):
        return Layer( self.__obj.ActiveLayer )
        
    def Fill( self, layer, r, g, b ):
        colorObj = GetColor( r, g, b )
        
        self.SetActiveLayer( layer )
        
        self.__obj.Selection.SelectAll
        self.__obj.Selection.Fill( colorObj )    


class Application( object ):
    '''
    Provides a COM interface for working with Photoshop.
    '''
    def __init__( self, launch = True ):
        self.__app = win32com.client.Dispatch( 'Photoshop.Application' )
        
        
    ## Properties ##
    
    @property
    def App( self ):
        return self.__app.Application
    
    @property
    def ActiveDocument( self ):
        try:
            return Document( self.__app.Application.ActiveDocument )
        
        except:
            return None
        
    @ActiveDocument.setter
    def ActiveDocument( self, doc ):
        self.__app.Application.ActiveDocument = doc.Object
        

    

    ## Methods ##
    
    def CloseAll( self ):
        docs = self.GetOpenDocuments()
        
        for doc in docs:
            doc.Close()
    
    def SetForegroundColor( self, r, g, b ):
        colorObj = GetColor( r, g, b )
        self.__app.Application.ForegroundColor = colorObj
        
    def SetBackgroundColor( self, r, g, b ):
        colorObj = GetColor( r, g, b )
        self.__app.Application.BackgroundColor = colorObj

    def GetOpenDocuments( self ):
        openDocuments = []
        
        for obj in self.__app.Application.Documents:
            openDocuments.append( Document( obj ) )
            
        return openDocuments
    
    def Open( self, filename ):
        obj = self.__app.Open( filename )
        self.__app.Application.ActiveDocument = obj
        
        return Document( obj )
    
    def New( self, width, height, name = None, resolution = 72, mode = NewDocumentMode.RGB ):
        if name == None:
            name = 'Untitled'
            
        obj = self.__app.Application.Documents.Add( width, height, resolution, name, mode )
        return Document( obj )
    
    
if __name__ == '__main__':
    app = Application()
    doc = app.ActiveDocument
    doc.SetActiveChannels( False, False, False, True )
    
    