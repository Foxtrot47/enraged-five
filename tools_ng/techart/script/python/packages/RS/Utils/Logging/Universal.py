'''
RS.Utils.Logging.Universal

Module for interfacing with the Universal Log.
'''
import os
import subprocess
import clr

clr.AddReference( 'RSG.Base' )

from System.IO import FileMode
from RSG.Base.Logging import FlushMode
from RSG.Base.Logging import LogFactory
from RSG.Base.Logging.Universal import UniversalLogFile

import RS.Config


## Functions ##

def Show( ulogFilename, modal = True ):
    '''
    Show a Universal Log file.
    
    Arguments:
    
        ulogFilename: The Universal Log filename to show.
        
    Keyword Arguments:
    
        modal: Whether or not the window is modal.
    '''
    ulogViewerExe = '{0}\\bin\\UniversalLogViewer\\UniversalLogViewer.exe'.format( RS.Config.Tool.Path.Root )
    
    if os.path.isfile( ulogViewerExe ):        
        process = subprocess.Popen( '{0} {1}'.format( ulogViewerExe, ulogFilename ), shell = False )
        
        # Force ULog to be a blocking call, making it modal.
        if modal:
            process.communicate()
        
    else:
        print 'Cannot open the Universal Log Viewer, since it could not be found!  Was expecting it here ({0}).'.format( ulogViewerExe )


## Classes ##

class UniversalLog( object ):
    '''
    Wrapper that represents a Universal Log object.
    
    Arguments:
    
        name: The name of the Universal Log to create.
        
    Keyword Arguments:
    
        appendToFile: Whether or not to append to an existing Universal Log.
        consoleOutput: Output logging messages to the console as they happen.
    '''
    def __init__( self, name, appendToFile = False, consoleOutput = True ):
        self.__name = name
        self.__filename = '{0}\\{1}.ulog'.format( RS.Config.Tool.Path.Logs, name )
        self.__logObj = LogFactory.CreateUniversalLog( self.__filename )
        
        self.__bufferSize = 100
        self.__fileMode = FileMode.CreateNew
        
        if appendToFile:
            self.__fileMode = FileMode.Append
        
        self.__logFile = UniversalLogFile( self.__filename, self.__fileMode, FlushMode.Always, self.__logObj, self.__bufferSize )
        
        # Whether or not to output logging to the console as it happens.
        self.__consoleOutput = consoleOutput
        
    
    ## Properties ##
    
    @property
    def Name( self ):
        return self.__name
    
    @property
    def Filename( self ):
        return self.__logFile.Filename
    
    @property
    def HasErrors( self ):
        return self.__logObj.HasErrors
    
    @property
    def HasWarnings( self ):
        return self.__logObj.HasWarnings
    
    @property
    def ConsoleOutput( self ):
        return self.__consoleOutput
    
    @ConsoleOutput.setter
    def ConsoleOutput( self, state ):
        self.__consoleOutput = state
    
    
    ## Methods ##
    
    def GetErrors( self ):
        '''
        TODO (Hayes 7/18/2013): Need to finish this implementation.  The cutscene update system is calling this.
        '''
        pass
    
    def Show( self, modal = True ):
        Show( self.Filename, modal )
    
    def Flush( self ):
        self.__logFile.Flush()
    
    def LogMessage( self, message, context = '' ):
        self.__logObj.MessageCtx( context, message, [] )
        
        if self.__consoleOutput:
            try:
                print( 'MESSAGE [{0}]: {1}'.format( context, message ) )
                
            except:
                pass
        
    def LogError( self, message, context = '' ):
        self.__logObj.ErrorCtx( context, message, [] )
        
        if self.__consoleOutput:
            try:
                print( 'ERROR [{0}]: {1}'.format( context, message ) )
                
            except:
                pass
        
    def LogWarning( self, message, context = '' ):
        self.__logObj.WarningCtx( context, message, [] )
        
        if self.__consoleOutput:
            try:
                print( 'WARNING [{0}]: {1}'.format( context, message ) )        
                
            except:
                pass
        
    def LogDebug( self, message, context = '' ):
        self.__logObj.DebugCtx( context, message, [] )
        
        if self.__consoleOutput:
            try:
                print( 'DEBUG [{0}]: {1}'.format( context, message ) )        
                
            except:
                pass
        