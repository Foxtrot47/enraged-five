'''
RS.Utils.Bugstar

Module for interfacing with Bugstar.
'''
import webbrowser
import clr

clr.AddReference( 'RSG.Model.Bugstar' )

from RSG.Model.Bugstar import Bug
from RSG.Model.Bugstar import BugCategory
from RSG.Model.Bugstar import BugBuilder
from RSG.Model.Bugstar import BugPriority
from RSG.Model.Bugstar.Organisation import Project
from RSG.Model.Bugstar.Organisation import User
from RSG.Model.Bugstar.Util import Creation


## Functions ##

def _AssertProject( projectObj ):
    assert isinstance( projectObj, Project ), 'Incorrect type: Must supply a RSG.Model.Bugstar.Organisation.Project object!'
    
def GetConnection():
    '''
    Get the current Bugstar connection.
    
    Returns:
        
        RSG.Model.Bugstar.BugstarConnection object.
        
    '''
    return Creation.UserConnection
    
def GetProject():
    '''
    Query the assigned Bugstar project for the current project.
    
    Returns:
        
        RSG.Model.Bugstar.Organisation.Project object.
    '''
    connection = GetConnection()
    
    if connection:
        return Project.GetProjectById( connection, connection.Config.ProjectId )
        
    return None
    
def GetProjects():
    '''
    Collect all of the users projects as a dictionary.
    
    Returns:
        
        Dictionary of RSG.Model.Bugstar.Organisation.Project objects.  The key
        is the name of the project.
    '''
    projectsDict = {}
    
    projects = Project.GetProjects( GetConnection() )
    
    for project in projects:
        projectsDict[ project.Name ] = project
        
    return projectsDict
    
def GetProjectByName( projectName ):
    '''
    Get a Bugstar project.
    
    Arguments:
        
        projectName: The name of the project to get.
        
    Returns:
        
        RSG.Model.Bugstar.Organisation.Project object.
    '''
    projects = GetProjects()
    
    if projectName in projects:
        return projects[ projectName ]
        
    return None

def GetBugById( bugId ):
    '''
    Get a bug by the supplied id.
    
    Arguments:
        
        bugId: The bug id to query.
        
    Returns:
        
        RSG.Model.Bugstar.Bug object, None if the bug could not be found.
    '''
    projects = GetProjects()
    
    for projectName, project in projects.iteritems():
        bug = Bug( project )
        found = bug.GetBugById( project, bugId )
        
        if found:
            return found
            
    return None
    
def GetUser( projectObj, friendlyName ):
    '''
    Get a Bugstar user.
    
    Arguments:
        
        projectObj: The project to query.  Must supply a RSG.Model.Bugstar.Organisation.Project object.
        friendlyName: The friendly name of the user to look up.  For example:
            
            'Jason Hayes'
            
    Returns:
        
        If the user exists in the project, will return a RSG.Model.Bugstar.Organisation.User object.  None if the user doesn't exist in the project.
    '''
    _AssertProject( projectObj )

    for user in projectObj.Users:
        if friendlyName in user.Name:
            return user
            
    return None
 
def OpenBug( bugId ):
    '''
    Opens the supplied bug.
    
    Arguments:
    
        bugId: The bug id to open.
    '''
    webbrowser.open( 'url:bugstar:{0}'.format( bugId ) )
 
def CreateBug( projectObj, 
               summary, 
               description,
               category    = BugCategory.Task, 
               priority    = BugPriority.P3,
               owner       = None,
               qaOwner     = None,
               reviewer    = None,
               tags        = [] ):
    '''
    Creates a bug.
    
    Arguments:
        
        projectObj: The project to add the bug.  Requires a RSG.Model.Bugstar.Organisation.Project object.
        summary: The summary for the bug.
        description: The description for the bug.
        
    Keyword Arguments:
        
        owner: The owner of the bug.  Provide the friendly name.  If not provided, the current user will be used. For example:
            
            'Jason Hayes'
            
        qaOwner: The QA owner of the bug.  Provide the friendly name.  If not provided, the current user will be used.
        reviewer: The reviewer of the bug.  Provide the friendly name.  If not provided, the current user will be used.
        category: The category type of the bug.  Use RSG.Model.Bugstar.BugCategory enums.  For quick reference:
            
            BugCategory.A
            BugCategory.B
            BugCategory.C
            BugCategory.D
            BugCategory.Task
            BugCategory.Todo
            BugCategory.Track
            
        priority: The bug priority.  Use RSG.Model.Bugstar.BugPriority enums.  For quick reference:
            
            BugPriority.High
            BugPriority.P2
            BugPriority.P3
            BugPriority.P4
            BugPriority.Low
            
        tags: List of tags to add to the bug.
        
    Returns:
        
        The created bug, which is a RSG.Model.Bugstar.Bug object.
    '''
    _AssertProject( projectObj )
    
    bugBuilder = BugBuilder( projectObj )
    bugBuilder.Summary = summary
    bugBuilder.Description = description
    bugBuilder.Category = category
    bugBuilder.Priority = priority
    
    # Owner
    if owner:
        owner = GetUser( projectObj, owner )
        
    else:
        owner = projectObj.CurrentUser
    
    if owner:
        bugBuilder.DeveloperId = owner.Id
    
    # QA Owner
    if qaOwner:
        qaOwner = GetUser( projectObj, qaOwner )
        
    else:
        qaOwner = projectObj.CurrentUser
        
    if qaOwner:
        bugBuilder.TesterId = qaOwner.Id
        
    # Reviewer
    if reviewer:
        reviewer = GetUser( projectObj, reviewer )
        
    else:
        reviewer = projectObj.CurrentUser
        
    if reviewer:
        bugBuilder.ReviewerId = reviewer.Id
    
    # Tags
    for tag in tags:
        bugBuilder.Tags.Add( tag )
    
    # Create the bug.
    return bugBuilder.ToBug( GetConnection() )