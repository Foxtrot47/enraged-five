'''
Utilities for working with files.
'''

import os
import fnmatch
import string


def GetImportPathFromFilename( filename ):
    '''
    Get the Python import path from the supplied filename.  The supplied filename
    must live under our Rockstar Python tools package directory.  For example, supplying
    the following path:
    
        x:\\wildwest\\dcc\\motionbuilder2014\\python\\RS\\Utils\\Path.py
        
    Would return the following:
    
        RS.Utils.Path
    
    Arguments:
    
        filename: The filename to extract import path.
        
    Returns:
    
        The Python import path for the filename.
    '''
    if 'motionbuilder2014\\python\\RS\\' not in filename:
        assert 0, 'Please supply a path to a Python module under the Rockstar Python package directory!'
        
    rawImport = filename[ filename.index( 'RS\\' ): ][ :-3 ]
    return rawImport.replace( '\\', '.' )      

def GetBaseNameNoExtension( filename ):
    '''
    Get just the name part of the filename, with no file extension.  For example:
    
        x:/wildwest/dcc/motionbuilder2014/python/menu.config
        
    Would be returned as:
        
        menu
        
    Arguments:
    
        The filename.
        
    Returns:
    
        Just the name part of the filename.
    '''
    return os.path.splitext( os.path.basename( filename ) )[ 0 ]

def GetFileExtension( filename ):
    return os.path.splitext( os.path.basename( filename ) )[ -1 ]

def Walk( pRoot, pRecurse = 0, pPattern = '*', pReturn_folders = 0 ):
    '''
    
    '''
    lResult = []
    
    try:
        lNames = os.listdir( pRoot )
        
    except os.error:
        return lResult
    
    lPattern = pPattern or '*'
    lPat_list = string.splitfields( lPattern , ';' )
    
    for iName in lNames:
        lFullname = os.path.normpath( os.path.join( pRoot, iName ) )
        
        for iPat in lPat_list:
            if fnmatch.fnmatch( iName, iPat ):
                if os.path.isfile( lFullname ) or ( pReturn_folders and os.path.isdir( lFullname ) ):
                    lResult.append( lFullname )
                    
                continue
            
        if pRecurse:
            if os.path.isdir( lFullname ) and not os.path.islink( lFullname ):
                lResult = lResult + Walk( lFullname, pRecurse, pPattern, pReturn_folders ) 
                
    return lResult