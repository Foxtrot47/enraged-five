'''
Description:
    Module to deal with Windows processes.
    
Author:
    Jason Hayes <jason.hayes@rockstarsandiego.com>
'''

import os
import subprocess
import ctypes


def IsRunning( processName ):
    '''
    Check to see if a process is running.
    '''
    s = subprocess.Popen( 'wmic PROCESS get Caption', stdout = subprocess.PIPE )
    processName = processName.lower()        
        
    for line in s.stdout:
        proc = line.split()
        
        if proc:
            proc = proc[ 0 ].lower()
        
            if proc == processName:
                return True
            
    return False

def GetCount( processName ):
    '''
    Get the number of times a process is running.
    '''
    count = 0
    
    s = subprocess.Popen( 'wmic PROCESS get Caption', stdout = subprocess.PIPE )
    processName = processName.lower()        
        
    for line in s.stdout:
        proc = line.split()
        
        if proc:
            proc = proc[ 0 ].lower()
        
            if proc == processName:
                count += 1
        
    return count

def GetId( processName ):
    '''
    Get the process id.
    '''
    s = subprocess.Popen( 'wmic PROCESS get Caption,ProcessId', stdout = subprocess.PIPE )
    processName = processName.lower()        
        
    for line in s.stdout:
        result = line.split()
        
        if result:
            proc = result[ 0 ].lower()
        
            if proc == processName:
                return int( result[ 1 ] )
                  
    return None

def Kill( processName ):
    '''
    Kill a process by name.
    '''
    pid = GetId( processName )
    
    if pid:
        handle = ctypes.windll.kernel32.OpenProcess( 1, False, pid )
        
        if handle:
            ctypes.windll.kernel32.TerminateProcess( handle, -1 )
            ctypes.windll.kernel32.CloseHandle( handle )
        
            return True
        
    return False
    