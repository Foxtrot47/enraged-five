'''
RS.Utils.Color

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
'''
import math

def LinearToSRGB( val ):
	val = float( val )
	
	if val <= 0.0031308:
		srgb = val * 12.92
		
	else:
		srgb = 1.055 * pow( val, 1 / 2.4 ) - 0.055
		
	return int( round( srgb * 255.0, 0 ) )


def SRGBtoLinear( val ):
	val = float( val / 255.0 )
	
	if val <= 0.04045:
		return val / 12.92
	
	else:
		return pow( ( val + 0.055 ) / 1.055, 2.4 )
	
	
def GetSpecularColorFromIOR( ior ):
	'''
	Returns a specular color from the supplied index of refraction.
	'''
	return pow( ior - 1, 2 ) / pow( ior + 1, 2 )


if  __name__ == '__main__':
	a = GetSpecularColorFromIOR( 1.33 )
	print a