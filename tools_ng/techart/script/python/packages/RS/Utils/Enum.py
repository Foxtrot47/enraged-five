'''
RS.Utils.Enum

Author:

	Jason Hayes <jason.hayes@rockstarsandiego.com>
	
Description:

	Utility module for generating enumerations.
	
Example usage:

	# Create integer enums.
	>>> textureType = RS.Utils.Enum.CreateInt( 'Diffuse', 'Specular', 'Normal' )
	>>> textureType.Diffuse
	0
	>>> textureType.Specular
	1
	>>> textureType.Normal
	2
	
	# Create named enums.
	>>> textureSuffix = RS.Utils.Enum.CreateNamed( Diffuse = '_d', Specular = '_s', Normal = '_n' )
	>>> textureSuffix.Diffuse
	'_d'
	>>> textureSuffix.Specular
	'_s'
	>>> textureSuffix.Normal
	'_n'
'''

def CreateInt( *args ):
	enums = dict( zip( args, range( len( args ) ) ) )
	return type( 'Enum', (), enums )

def CreateNamed( **kwargs ):
	return type( 'Enum;', (), kwargs )