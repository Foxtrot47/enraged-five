"""
Description:
    The master function that controls capture exports within Motionbuilder.
    Doesn't actually run much export code, just monitors and starts other scripts.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import platform
import RS.Config

import RS.Core.Automation.FrameCapture.CaptureModel as CaptureModel
import RS.Core.Automation.FrameCapture.CapturePrep as CapturePrep
import RS.Core.Automation.FrameCapture.CaptureExport as CaptureExport


CAPTUREROOT = "N:/RSGNYC/Production/CaptureRenders"
MACHINENAME = str(platform.node())
PROJECTNAME = RS.Config.Project.Name


def ValidateJobXml(jobCount):
    """
    Simple function to detect we only have one job xml, or alert user of an error.
    Still needs QT error window setup, since mobu message boxes are suppressed.
    Arguments:
        jobCount: An int of the number of job xmls found.
    Returns:
        A bool - True if a valid job is found, False otherwise.
    """
    # No Jobs Found
    if jobCount <= 0:
        messageText = ("ERROR: No jobs xmls found in this machine's job folder.\n\n"
                       "Close Motionbuilder and restart the crapExporter.")
        # make QT window to actually show this...
        return False

    # Too Many Jobs Found
    elif jobCount > 1:
        messageText = ("ERROR: Too many jobs xmls found in this machine's job folder.\n\n"
                       "Close Motionbuilder and restart the crapExporter.")
        # make QT window to actually show this...
        return False

    # Job Validated
    else:
        return True


def RunCapture():
    """
    A simple manager for capture exports. Doesn't actually run any 'real' code,
    just creates the capObj, starts CapturePrep/CaptureExport, and detects errors.
    """
    # Search For Job Xmls
    jobFolder = os.path.join(CAPTUREROOT, "ExportQueue", PROJECTNAME, "jobs", MACHINENAME)
    foundXmls = [eachFile for eachFile in os.listdir(jobFolder) if eachFile.lower().endswith(".xml")]

    # Validate Job Xmls - only run if a single job xml file is found
    validJob = ValidateJobXml(len(foundXmls))
    if validJob:

        # Create capObj Instance and Set fbxOpenTime
        jobXmlPath = os.path.join(jobFolder, foundXmls[0])
        capObj = CaptureModel.MobuCaptureModel(jobXmlPath)

        # Run Capture Prep
        CapturePrep.PrepFbxForCapture(capObj)

        # Run Scripted Export
        exportSuccess = CaptureExport.ExportFbxForCapture(capObj)
        if exportSuccess is True:
            capObj.UpdateTag("status", "EXPORT SUCCESS")
        else:
            errorMessage = "EXPORT ERROR: " + exportSuccess
            capObj.UpdateTag("status", errorMessage)

RunCapture()