import os
import sys
import platform
import msvcrt
import pywintypes
import win32file
import win32con

# Import FileTools and XmlTools from the MB folder
drivePath = os.path.splitdrive(os.getcwd())[0] + os.path.sep
mbframeCapPath = os.path.join(drivePath, "wildwest", "dcc", "motionbuilder2014", "python",
                                "RS", "Core", "Automation", "FrameCapture")
sys.path.append(mbframeCapPath)
import FileTools
import XmlTools


# Set Constants
MACHINENAME = str(platform.node())


def ChangeFileCreationTime(filePath, newTime):
    """
    Sets the creation time of a file to the newTime specified.
    Useful for file locking (so we can write out a new copy without altering creation time).
    Arguments:
        filePath: A string of the filePath we want to alter.
        newTime: A float of the desired new creation time.
    """
    wintime = pywintypes.Time(newTime)
    winfile = win32file.CreateFile(filePath, win32con.GENERIC_WRITE,
           win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE | win32con.FILE_SHARE_DELETE,
           None, win32con.OPEN_EXISTING, win32con.FILE_ATTRIBUTE_NORMAL, None)
    win32file.SetFileTime(winfile, wintime, None, None)
    winfile.close()


class QueueManager(object):
    def __init__(self, queuePath):
        self.queuePath = queuePath
        FileTools.CreateParentFolders(self.queuePath)

    def CreateQueueXmlList(self):
        validXmls = []
        for eachFile in os.listdir(self.queuePath):
            if os.path.splitext(eachFile)[1].lower() == ".xml":
                xmlPath = os.path.join(self.queuePath, eachFile)
                try:
                    XmlTools.ReadTagValueFromXmlPath(xmlPath, "priority")
                except IOError:
                    continue
                validXmls.append(xmlPath)
        return validXmls

    def GetXmlPriorityValue(self, xmlPath):
        priority = XmlTools.ReadTagValueFromXmlPath(xmlPath, "priority")
        if priority is None:
            return 1
        else:
            return int(priority)

    def PrioritizeXmlList(self, xmlList):
        xmlDict = {}
        for eachXml in xmlList:
            priority = self.GetXmlPriorityValue(eachXml)
            if priority not in xmlDict.keys():
                xmlDict[priority] = []
            xmlDict[priority].append(eachXml)

        sortedXmlList = []
        for eachPriority in sorted(xmlDict.keys(), key=int, reverse=True):
            priorityXmlList = sorted(xmlDict[eachPriority], key=lambda x: os.path.getctime(x))
            sortedXmlList += priorityXmlList

        return sortedXmlList

    def SearchForJobs(self):

        # Create Xml List and Prioritize
        queueXmlList = self.CreateQueueXmlList()
        queueXmlList = self.PrioritizeXmlList(queueXmlList)

        # No Xmls Found - just wait and loop again
        if len(queueXmlList) == 0:
            return None

        # Loop Through Valid Xmls
        print "\n{} JOB(S) FOUND".format(str(len(queueXmlList)))
        for xmlPath in queueXmlList:

            # Get jobName and Open Xml File
            jobName = os.path.splitext(os.path.split(xmlPath)[1])[0]
            xmlFile = open(xmlPath)

            # Try Reading and Locking the Xml - continue if it fails
            try:
                xmlLines = xmlFile.readlines()
                msvcrt.locking(xmlFile.fileno(), 1, os.path.getsize(xmlPath))
            except IOError:
                xmlFile.close()
                continue

            # Xml Lock Succeeds
            print "PREPARING JOB: {}".format(jobName)
            jobFolderPath = os.path.join(self.queuePath, "jobs", MACHINENAME)
            backupFolderPath = os.path.join(self.queuePath, "backups")
            FileTools.DeletePath(jobFolderPath)
            FileTools.CreateParentFolders(jobFolderPath)

            # Copy and Backup Job Data Folder (CaptureQueue Only)
            if "CaptureQueue" in xmlPath:
                jobDataPath = os.path.join(self.queuePath, jobName)
                FileTools.CopyPath(jobDataPath, jobFolderPath)
                FileTools.CopyPath(jobDataPath, backupFolderPath, moveMode=True)

            # "Copy" Job Xml to Job Folder
            jobXmlPath = os.path.join(jobFolderPath, os.path.split(xmlPath)[1])
            jobFile = open(jobXmlPath, "w")
            jobFile.writelines(xmlLines)
            jobFile.close()

            # Update "Copied" Xml's Creation Time
            creationTime = os.path.getctime(xmlPath)
            ChangeFileCreationTime(jobXmlPath, creationTime)

            # Unlock Job Xml and Move to Backups
            xmlFile.close()
            FileTools.CopyPath(xmlPath, backupFolderPath, moveMode=True)

            # Job Setup Successfully
            return jobXmlPath

        # No Jobs Setup
        return None