"""
Modify a p4v bookmarks xml file.
"""

# standard lib
import os
import stat
import datetime
import shutil
from sys import exit
import xml.etree.ElementTree as et

SHARED_FOLDER_PATH = os.path.join(os.path.dirname(__file__), "SharedBookMarks")
USER_HOME_DIR = os.path.expanduser("~")

def createBookMarkBackUp(userBookmarkFileF):
    """
    Create BackUpBookMarks
    """
    bookMarkBackUpFolder = "x:\\temp\\BookMarkBckUp"
    bckUpDest = os.path.join(
        bookMarkBackUpFolder
        + "bookmarks_"
        + datetime.datetime.today().strftime("%y-%m-%d-%H-%M-%S")
        + ".xml"
    )
    bckUpDestStr = str(bckUpDest)
    bckUpDir = os.path.exists(bookMarkBackUpFolder)
    if not bckUpDir:
        os.makedirs(bookMarkBackUpFolder)
        shutil.copy(userBookmarkFileF, bckUpDestStr)
        print ("Back up folder created")
        print ("User bookmarks.xml copy created ")
    else:
        shutil.copy(userBookmarkFileF, bckUpDestStr)
        print ("User bookmarks.xml copy created")

def selectTeam():
    """
    Prints Menu Selection based off of XML's in SharedBookMarks Directory
    """
    teamList = os.listdir(SHARED_FOLDER_PATH)
    teamQuestion = "\nShared bookmarks by team. \n"
    print (teamQuestion)

    for itemStr, listStr in enumerate(teamList):
        listPrint = listStr.replace(".xml", "")
        teamSel = "{0}) {1}".format(itemStr + 1, listPrint)
        print (teamSel)

    if len(teamList) > 1:
        while True:
            userTeamSelctionInput = ""
            while not userTeamSelctionInput.isdigit():
                userTeamSelctionInput = raw_input("\nPlease select the number of which team you are on:")

            userTeamSelectionNum = int(userTeamSelctionInput) - 1

            if userTeamSelectionNum not in range(len(teamList)):
                print ("This is not a valid selection \n")
                print (" Please select the bookmark you wish to update by entering the number \n")
                for itemStr, listStr in enumerate(teamList):
                    listPrint = listStr.replace(".xml", "")
                    teamSel = "{0}) {1}".format(itemStr + 1, listPrint)
                    print (teamSel)
            elif userTeamSelectionNum in range(len(teamList)):
                p4SharedBooKMarkFilePath = os.path.join(SHARED_FOLDER_PATH, teamList[userTeamSelectionNum])
                userTeam = teamList[userTeamSelectionNum]
                if os.path.exists(p4SharedBooKMarkFilePath):
                    print (teamList[userTeamSelectionNum]) + " selected ( O . O )"
                    return userTeam, p4SharedBooKMarkFilePath
                else:
                    print ("Team does not exist")
    else:
        print ("Error: Missing Shared Book Marks - Please sync to the following directory and run the UpdateBookMarks.bat again. \n "
               "//*Project*/tools/release/techart/script/python/standalone/p4/BookMarkTool/SharedBookMarks/")

def paresConnectionMap():
    """
    Parse ConnectionMap.XML to get client and port list info
    """
    cmNameList = []
    cmP4PortList = []
    connectionMapXmlFile = USER_HOME_DIR + "\\.p4qt\\connectionmap.xml"
    cmTree = et.parse(str(connectionMapXmlFile))
    cmRoot2 = cmTree.getroot()

    for item in cmRoot2.findall("ConnectionMap"):
        cmName = item.find("Name").text
        cmNameList.append(cmName)
        p4port = item.find("P4Port").text
        cmP4PortList.append(p4port)
    return cmNameList, cmP4PortList

def getUserBookMarks(ConnectMapNameList, ConnectionMap_P4_PortList):
    """
    Prints Menu Selection based off of Parse ConnectionMap.XML
    """

    updateBmStr = "Update existing bookmark file"
    noBmStr = "Create new bookmark file"
    getUserBMList = []
    print(" \n Here is a list of all available P4 workspaces your PC. \n")

    for item in range(len(ConnectionMap_P4_PortList)):
        userBookmarkFile = USER_HOME_DIR + "\\.p4qt\\" + ConnectMapNameList[item] + "Clients\\bookmarks.xml"

        userBookmarkFileStr = str(userBookmarkFile)
        getUserBMList.append(userBookmarkFileStr)
        cmNameListStr = "    Client:" + ConnectMapNameList[item]
        portListStr = "    Port:" + ConnectionMap_P4_PortList[item]
        instructionStr1 = " {0}) {1}".format(item + 1, updateBmStr)
        isExist = os.path.exists(userBookmarkFileStr)

        if isExist:
            print (instructionStr1)
            print (cmNameListStr)
            print (portListStr + "\n")
        else:
            print ("User does not have local bookmarks, "
                   "Please verify that your p4 enviroments are setup correctly")
            quit()

    return getUserBMList

def updateUserBookmark(userBookMarkListF, userTeamF):
    """
    Updates user bookmarks based of user options and selections.
    If the user has more then 1 bookmark gives prints menu to select from
    If user only has 1 option will update userbook marks automatically.
    """
    userBookMarkExistCheck = False
    if len(userBookMarkListF) > 0:
        if len(userBookMarkListF) == 1:
            userBookmarkFile = userBookMarkListF[0]
            createBookMarkBackUp(userBookmarkFile)
            isExist = os.path.exists(userBookmarkFile)
            selectBmPath = SHARED_FOLDER_PATH + "/" + userTeamF
            if isExist:
                userBookMarkExistCheck = True
                print (userBookmarkFile) + " selected ( O . O )"
                os.chmod((userBookmarkFile), stat.S_IWRITE)
                # Parse shared and user xml into an element tree object.
                sTree = et.parse(selectBmPath)
                uTree = et.parse(userBookmarkFile)

                # Get root of xml
                sRoot = sTree.getroot()
                uRoot = uTree.getroot()
                # Copy Shared bookmark parent node
                sbmFolderList = sRoot.findall("BookmarkFolder")
                ubmFolderList = uRoot.findall("BookmarkFolder")
                copyBmS = None
                copyBmU = None

                # Find "Shared" tag in SharedBookMark
                for bookmarkTag in sbmFolderList:
                    nameTag = bookmarkTag.find("Name")
                    if nameTag.text.lower() == "shared":
                        print ("Parsed.".format(bookmarkTag.tag))
                        copyBmS = bookmarkTag
                    else:
                        print ("Failed to Parse")

                # Find "Shared" in user Bookmark
                for bm2 in ubmFolderList:
                    name2 = bm2.find("Name")
                    if name2.text.lower() == "shared":
                        print ("Parent node found \(o_o)/: {0}".format(bm2.tag))
                        copyBmU = bm2
                    else:
                        print ("Folder does not have Shared. {0}".format(bm2.tag))

                # Remove and append user bookmarks  if "Shared" tag is found
                if copyBmU is not None:
                    print ("Updating Bookmarks")
                    uRoot.remove(copyBmU)
                    uRoot.append(copyBmS)
                    uTree.write(userBookmarkFile)
                    print ("done ( x.x )")
                else:
                    print ("Adding New Bookmarks")
                    uRoot.append(copyBmS)
                    uTree.write(userBookmarkFile)
            else:
                userBookMarkExistCheck = True
                shutil.copy(userTeamF, str(selectBmPath))
                print("Created New BookMark File")
                exit()

        else:
            while userBookMarkExistCheck is not True:
                userInput = ""
                while not userInput.isdigit():
                    userInput = raw_input(
                        "Please enter the number of the work space of the bookmark file you would like to update:"
                    )
                userInputNum = int(userInput) - 1

                if userInputNum not in range(len(userBookMarkListF)):
                    print ("This is not a valid selection \n")
                    print (" Please select the bookmark you wish to update by entering the number \n")
                elif userInputNum in range(len(userBookMarkListF)):
                    userBookmarkFile = userBookMarkListF[userInputNum]
                    createBookMarkBackUp(userBookmarkFile)
                    isExist = os.path.exists(userBookmarkFile)
                    selectBmPath = SHARED_FOLDER_PATH + "/" + userTeamF
                    if isExist:
                        userBookMarkExistCheck = True
                        print (userBookmarkFile) + " selected ( O . O )"
                        os.chmod((userBookmarkFile), stat.S_IWRITE)
                        # Parse shared and user xml into an element tree object.
                        sTree = et.parse(selectBmPath)
                        uTree = et.parse(userBookmarkFile)

                        # Get root of xml
                        sRoot = sTree.getroot()
                        uRoot = uTree.getroot()
                        # Copy Shared bookmark parent node
                        sbmFolderList = sRoot.findall("BookmarkFolder")
                        ubmFolderList = uRoot.findall("BookmarkFolder")
                        copyBmS = None
                        copyBmU = None

                        # Find "Shared" tag in SharedBookMark
                        for bookmarkTag in sbmFolderList:
                            nameTag = bookmarkTag.find("Name")
                            if nameTag.text.lower() == "shared":
                                print ("Parsed.".format(bookmarkTag.tag))
                                copyBmS = bookmarkTag
                            else:
                                print ("Failed to Parse")

                        # Find "Shared" in user Bookmark
                        for bm2 in ubmFolderList:
                            name2 = bm2.find("Name")
                            if name2.text.lower() == "shared":
                                print ("Parent node found \(o_o)/: {0}".format(bm2.tag))
                                copyBmU = bm2
                            else:
                                print ("Folder does not have Shared. {0}".format(bm2.tag))

                        # Remove and append user bookmarks  if "Shared" tag is found
                        if copyBmU is not None:
                            print ("Updating Bookmarks")
                            uRoot.remove(copyBmU)
                            uRoot.append(copyBmS)
                            uTree.write(userBookmarkFile)
                            print ("done ( x.x )")
                        else:
                            print ("Adding New Bookmarks")
                            uRoot.append(copyBmS)
                            uTree.write(userBookmarkFile)
                    else:
                        userBookMarkExistCheck = True
                        shutil.copy(userTeamF, str(selectBmPath))
                        print("Created New BookMark File")
                        exit()
    else:
        print("why? you do dis?")

def main():
    userTeam, p4SharedBooKMarkFilePathF = selectTeam()
    cmNameList, cmP4PortList = paresConnectionMap()
    userBookMarkList = getUserBookMarks(cmNameList, cmP4PortList)
    updateUserBookmark(userBookMarkList, userTeam)

if __name__ == "__main__":
    main()
