### Generic Standard Commandlines ###
-rag
-rdrdebugcamera

### output and logging ###
-displayNetInfoOnBugCreation
-spewGameplayDebugOnBugCreation


### Multiplayer Specific ###
-sc_noPrologueCheckForMP
-sc_AllowAllMissionsWithTwoPlayers
-mpavailable

###Additional Self Explanatory Helpful Commandlines ###
-invincible
-sc_NoIdleKick

### Misc ###
-debugExpr
-SC_UseRockstarCandidateMissions

### Changes Display settings, please use if you are encountering display errors/crashes upon launching the game ###
-pc:width=1600
-pc:height=900
-raguseownwindow

### Tells the Game to Load a specific Pack on top of the base game. This will need to be edited to reflect the current DLC pack. ###
-extracontent=x:/gta5_dlc/mpPacks/mpTuner/;
#x:/gta5_dlc/patchPacks/patchDay25NG/;

### Grabs Audio from the RUNTIME Audio Folder (must first get latest on the depo path //depot/gta5/audio/DEV_NG/RUNTIME/platfrom/) ###
#-audiofolder=x:\gta5\audio\DEV_NG\RUNTIME\PC

####PS4 CONTROL COMMANDLINES ####
-ps4_control
