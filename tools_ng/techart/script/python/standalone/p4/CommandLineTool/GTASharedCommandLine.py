# standard lib
import os
import datetime
import shutil

userCommandLineFile = "x:\\gta5\\titleupdate\\dev_ng\\commandline.txt"
platformQuestion = "What game platform are you running your build on?\n"
ctrlQuestion = "What game controller are you using\n"
platformList = ["PC", "PS4", "XBOX"]
controlList = ["XBOX","PS4"]

def selectionMenu(itemList):
    '''
    Print menu of user options from list
    '''
    ListNum = 1
    for item in itemList:
        platfromSelectionMenu = "{0}) {1}".format(ListNum, item)
        print(platfromSelectionMenu)
        ListNum = ListNum + 1

def inputSelection(platformListInput,questionInput):
    """
    Receives and returns user selection, uses selection menu to display options
    """
    platformErrorStr = "\n Error, You entered a letter instead of a  number."
    print("\n")
    print(questionInput)
    selectionMenu(platformListInput)

    if len(platformListInput) > 1:
        while True:
            userInput = ""
            while not userInput.isdigit():
                try:
                    userInput = input("\nPlease enter the number corresponding with your selection:")
                    userSelectionNum = int(userInput)
                    userSelectionNum = userSelectionNum - 1

                    if userSelectionNum not in range(len(platformListInput)):
                        print("This is not a valid selection \n")
                        print(questionInput)
                        selectionMenu(platformListInput)
                    elif userSelectionNum in range(len(platformListInput)):
                        userSelection = platformListInput[userSelectionNum]
                        return userSelection
                except ValueError:
                    print(platformErrorStr)
                    print(questionInput)
                    selectionMenu(platformListInput)
                else:
                    break
        else:
            print("how did you get here? Contact Marco")
    else:
        print("Help me I'm scared, I really have no idea how you got here. Contact Marco")

def sharedCommandLinePathCleanUp(platformSelection,controlSelection):
    """
    Verifies user selected sharedcommandline txt file exists.
    """

    sharedCommandLine = "X:\\gta5\\tools_ng\\techart\\script\\python\\standalone\\p4\\CommandLineTool\\SharedCommandLines\\SharedCommandLine" + platformSelection + controlSelection + ".txt"

    sharedCommandLineFileExists = os.path.exists(sharedCommandLine)
    if sharedCommandLineFileExists:
        return(sharedCommandLine)
    else:
        print("file does not exists, how did you get here?")

def createCommandLineBackUp(commandFileInput, userSelectedSharedCommandlineInput,platformSelection,controlSelection):
    """
    Create a back-up directory and back up of GTA V commandlist txt file.
    """

    commandFileInputExist = os.path.exists(commandFileInput)
    uscerCommandLineFolder = os.path.join("x:\\gta5\\titleupdate\\dev_ng\\")
    copiedUserCommandLineFile = os.path.join("x:\\gta5\\titleupdate\\dev_ng\\SharedCommandLine" + platformSelection + controlSelection + ".txt")

    if commandFileInputExist:
        commandLineBckUpFolder = "x:\\temp\\GtaV_CommandLineBckUp"
        bckUpDest = os.path.join(
            commandLineBckUpFolder
            + "\\"
            + "CommandLineBckUp_"
            + datetime.datetime.today().strftime("%y-%m-%d-%H-%M-%S")
            + ".xml"
        )
        bckUpDestStr = str(bckUpDest)
        bckUpDir = os.path.exists(commandLineBckUpFolder)

        if not bckUpDir:
            os.makedirs(commandLineBckUpFolder)
            print("Back up folder created at the following directory")
            print(commandLineBckUpFolder)

            shutil.copy(commandFileInput, bckUpDestStr)
            print("Back up commandlines created")
        else:
            shutil.copy(commandFileInput, bckUpDestStr)
            print("Back up commandlines created at the following directory")
            print(commandLineBckUpFolder)
    else:
        shutil.copy(userSelectedSharedCommandlineInput, uscerCommandLineFolder)
        os.rename(copiedUserCommandLineFile,commandFileInput)
        print("User Command Line File copied")
        exit()

def makeLineForLineListFromFile(commandLineFile):
    """
    Make and return a list created on line for line from commandline txt file.
    """
    linesList = []

    with open(commandLineFile, "r") as f_in:
        lines = [line.rstrip() for line in f_in]
        for item in lines:
            item = item.replace("\\","/")
            if item not in linesList:
                linesList.append(item)
    return linesList

def cleansAppendCommandLines(userCommandLineFilesListAppending, missingCommandLinesListAppending):
    """
    Removes  ### SHARED COMMANDLINES ### from both list and and re-adds in proper location
    Appends missing command lines to user command lines in list
    """
    sharedCommandLineHeader = "### ADDED SHARED COMMANDLINES ###"

    for item in userCommandLineFilesListAppending:
        if item == sharedCommandLineHeader:
            userCommandLineFilesListAppending.remove(item)

    userCommandLineFilesListAppending.append("\n")
    userCommandLineFilesListAppending.append(sharedCommandLineHeader)
    for item in missingCommandLinesListAppending:
        userCommandLineFilesListAppending.append(item)

    return(userCommandLineFilesListAppending)

def makeListOfMissingCommandLines(userCommandLinesList,sharedCommandLinesList):
    """
    Compares list made from text files and returns list of missing lines
    """
    commandLineAppendList = []

    for item in sharedCommandLinesList:
        if item not in userCommandLinesList:
            if item not in commandLineAppendList:
                commandLineAppendList.append(item)
            else:
                print("List has all needed commandlines")

    return (commandLineAppendList)

def formatEmptyLines(listInput):
    """
    Removes empty lines from list following specfic condition
    """
    commandLineReformatList = []

    indicesEmptySpaceList = [index for index, element in enumerate(listInput) if element == "\n"]

    for index in sorted(indicesEmptySpaceList, reverse=True):
        if "###" in listInput[index - 1]:
            del listInput[index]
        elif "\n" == listInput[index - 1]:
            del listInput[index]

    indicesHashList = [index for index, element in enumerate(listInput) if "###" in element]

    for index in sorted(indicesHashList, reverse=True):
        if index > 0:
            if "\n" != listInput[index - 1]:
                listInput.insert(index, "\n")

    return listInput

def reWriteOldCommandLines(updatedUserCommandLineList, userCommandLineFileFile):

    with open(userCommandLineFileFile, "w+") as commandLineFileTextFile:
        commandLineFileTextFile.truncate()
        for item in updatedUserCommandLineList:
            commandLineFileTextFile.writelines(item)
            commandLineFileTextFile.writelines("\n")
    print("Commandline file updated" + "\n" "{0}".format(userCommandLineFile))

def main():

    ### ask user for input ###
    platformSelection = inputSelection(platformList,platformQuestion)
    print("You have selected {0} build".format(platformSelection))
    controlSelection = inputSelection(controlList,ctrlQuestion)
    print("You have selected {0} controller".format(controlSelection))

    ### verify's path of user selected
    userSelectedSharedCommandline = sharedCommandLinePathCleanUp(platformSelection, controlSelection)

    ## creates back of of orignal commandline ##
    createCommandLineBackUp(userCommandLineFile,userSelectedSharedCommandline,platformSelection,controlSelection)

    ## creates a line for line list of the users commandline
    userCommandLineFilesList = makeLineForLineListFromFile(userCommandLineFile)

    ### creates a line for line list of shared commandline
    sharedCommandLineFilesList = makeLineForLineListFromFile(userSelectedSharedCommandline)

    ### compares user commandlines against shared commmandlines and returns a list of missing commandlines ###
    missingCommandLinesList = makeListOfMissingCommandLines(userCommandLineFilesList,sharedCommandLineFilesList)

    ### cleans and appends list of missing lines to end of list ###
    finalUserCommandLines = cleansAppendCommandLines(userCommandLineFilesList, missingCommandLinesList)

    ### removes unneccsary empty lines and adds new lines where needed ###
    finalUserCommandLinesReformated = formatEmptyLines(finalUserCommandLines)

    #### writes new appendedlist to users commandlines ###
    reWriteOldCommandLines(finalUserCommandLinesReformated, userCommandLineFile)

if __name__ == "__main__":
    main()

