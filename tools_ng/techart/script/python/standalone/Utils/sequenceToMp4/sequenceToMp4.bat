call setenv
set infile=%1%
set noext=%infile:~0,-4%
set outfile=%noext%.mp4

%RS_TOOLSPYTHON% %RS_TOOLSROOT%\techart\script\python\standalone\Utils\sequenceToMp4\edit_sequenceToMp4.py -i %infile% -o %outfile%
pause