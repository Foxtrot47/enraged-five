grammar MaxFloatExpression;

/*
abs(x)         - absolute value of x
acos(x)        - arccosine of x
asin(x)        - arcsine of x
atan(x)        - arctangent of x
ceil(x)        - smallest integer greater than or equal to x
cos(x)         - trigonometric cosine of x
cosh(x)        - hyperbolic cosine of x
degToRad(x)    - x converted from degrees to radians
e              - the constant e (2.71828...)
exp(x)         - e raised to the power x
floor(x)       - largest integer less than or equal to x
if(c, t, f)    - conditional: value is t if c is true, else value is f
ln(x)          - natural logarithm (base e) of x
log(x)         - common logarithm (base 10) of x
max(x, y)      - maximum of x and y
min(x, y)      - minimum of x and y
mod(x, y)      - remainder of x divided by y
pi             - the constant pi (3.14159265...)
pow(x, y)      - x raised to the power y
radToDeg(x)    - x converted from radians to degrees
sin(x)         - trigonometric sine of x
sinh(x)        - hyperbolic sine of x
sqrt(x)        - square root of x
tan(x)         - trigonometric tangent of x
tanh(x)        - hyperbolic tangent of x
*/

expression
	:	( expr )+
	;
	
relation
	:	expr '==' expr															# EqualTo
	| 	expr '!=' expr															# NotEqualTo
	|  	expr '<'  expr															# LessThan
	| 	expr '<=' expr															# LessThanEqualTo
	| 	expr '>'  expr															# GreaterThan
	| 	expr '>=' expr															# GreaterThanEqualTo
	;
	
expr
	:	expr op=('*'|'/') expr													# MultiplyDivide
	| 	expr op=('+'|'-') expr													# AddSubtract
	|	'-' NUMBER																# UniaryNumber
	| 	NUMBER																	# Number
	|	'-' IDENT																# UniaryIdent
	| 	IDENT																	# Identifier 
	|	E																		# E
	|	PI																		# Pi	
	|	'abs' 		LPAREN expr RPAREN											# Absolute
	|	'acos' 		LPAREN expr RPAREN											# ArcCosine
	|	'asin' 		LPAREN expr RPAREN											# ArcSine
	|	'atan' 		LPAREN expr RPAREN											# ArcTangent
	|	'ceil' 		LPAREN expr RPAREN 											# Ceiling
	|	'cos' 		LPAREN expr RPAREN											# Cosine
	|	'cosh' 		LPAREN expr RPAREN											# HyperbolicCosine
	|	'degToRad' 	LPAREN expr RPAREN											# DegreesToRadians
	|	'exp'	 	LPAREN expr RPAREN											# Exponent
	|	'floor' 	LPAREN expr RPAREN											# Floor
	|	'ln' 		LPAREN expr RPAREN											# NaturalLogarithm
	|	'log' 		LPAREN expr RPAREN											# CommonLogarithm
	|	'radToDeg' 	LPAREN expr RPAREN											# RadianToDegrees
	|	'sin'	 	LPAREN expr RPAREN											# Sine
	|	'sinh'	 	LPAREN expr RPAREN											# HyperbolicSine
	|	'sqrt'	 	LPAREN expr RPAREN											# SquareRoot
	|	'tan'	 	LPAREN expr RPAREN											# Tangent
	|	'tanh'	 	LPAREN expr RPAREN											# HyperbolicTangent
	|	'max'		LPAREN expr COMMA expr RPAREN								# Maximum	
	|	'min'		LPAREN expr COMMA expr RPAREN								# Minimum
	|	'mod'		LPAREN expr COMMA expr RPAREN								# Modulus
	|	'pow'		LPAREN expr COMMA expr RPAREN								# Power
	|	'if' 		LPAREN relation COMMA expr COMMA expr RPAREN				# IfStatement
	|	'(' expr ')'															# Parentheses
	;

MULT
	: 	'*'
	;

DIV
	: 	'/'
	;

ADD
	: 	'+'
	;
	
SUB
	: 	'-'
	;
	
E
	:	'e'
	;

PI	
	:	'pi'
	;
	
NUMBER
	:	('0'..'9')+ ('.' ('0'..'9'+)*)?
	;
	
IDENT
	:	('a'..'z' | 'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9' | '_' | '#')*
	;

LPAREN
	:	'('
	;

RPAREN
	:	')'
	;

COMMA
	:	','
	;
	
WS
	:	(' ' | '\t' | '\n' | '\r' | '\f')+ -> skip
	;