import java.util.*;
//import java.util.List;
//import java.util.ArrayList; 

public class MotionBuilderPython extends MaxFloatExpressionBaseVisitor<String> {
	
	public static int depth = 1;
	List<String> stringArray = new ArrayList<String>();
	
	// 	expression : ( expr )+ ;
	@Override
    public String visitExpression(MaxFloatExpressionParser.ExpressionContext ctx){
		
		String expression 	= visit(ctx.expr(0));
		
		String lString = "";
		
		lString = lString + "lConstrained = FBFindModelByLabelName(\"MODEL_NAME\")\n";
		lString = lString + "lConstrainedObject = lRelConstraint.ConstrainObject(lConstrained)\n";
		lString = lString + "lConstrainedObject.UseGlobalTransforms = False\n";
				
		lString = lString + "\n";
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			
			lString = lString + "if \"Rotation\" in \"TRANSFORM_NAME\":\n";
			lString = lString + "	lConverterNumToVecBox = lRelConstraint.CreateFunctionBox('Converters', 'Number to Vector')\n";
			lString = lString + "	rs_ConnectBox(lConverterNumToVecBox, 'Result', lConstrainedObject, 'TRANSFORM_NAME')\n";
			lString = lString + "	lRadToDegrees = lRelConstraint.CreateFunctionBox('Converters', 'Rad to Deg')\n";
			lString = lString + "	lRadToDegreesA = rs_FindAnimationNode(lRadToDegrees.AnimationNodeInGet(), 'a')\n";
			lString = lString + "	lRadToDegreesA.WriteData([" + fExpression + "])\n";	
			lString = lString + "	rs_ConnectBox(lRadToDegrees, 'Result', lConverterNumToVecBox, 'AXIS_NAME')\n";
			lString = lString + "	lRelConstraint.SetBoxPosition(lConstrainedObject, " + ((depth - 2) * -400) + " , " + BoxCount * 200 + ")\n";
			lString = lString + "	lRelConstraint.SetBoxPosition(lConverterNumToVecBox, " + ((depth - 1) * -400) + ", " + BoxCount * 200 + ")\n";
			lString = lString + "	lRelConstraint.SetBoxPosition(lRadToDegrees, " + (depth * -400) + ", " + BoxCount * 200 + ")\n";
			lString = lString + "else:\n";
			lString = lString + "	lConverterBoxValue = rs_FindAnimationNode(lConverterNumToVecBox.AnimationNodeInGet(), 'AXIS_NAME')\n";
			lString = lString + "	lConverterBoxValue.WriteData([" + fExpression + "])\n";	
			
		}catch(NumberFormatException nfe){
			
			lString = lString + "lConverterNumToVecBox = lRelConstraint.CreateFunctionBox('Converters', 'Number to Vector')\n";
			lString = lString + "lOffsetObject = lRelConstraint.SetAsSource(lConstrained)\n";
			lString = lString + "lOffsetObject.UseGlobalTransforms = False\n";
			lString = lString + "lAddBoxOffset = lRelConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')\n";
			
			lString = lString + "rs_ConnectBox(lConverterNumToVecBox, 'Result', lAddBoxOffset, 'V1')\n";
			lString = lString + "rs_ConnectBox(lOffsetObject, 'TRANSFORM_NAME', lAddBoxOffset, 'V2')\n";
			
			lString = lString + "rs_ConnectBox(lAddBoxOffset, 'Result', lConstrainedObject, 'TRANSFORM_NAME')\n";
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
	
				lString = lString + "if \"Rotation\" in \"TRANSFORM_NAME\":\n";
				lString = lString + "	lRadToDegrees = lRelConstraint.CreateFunctionBox('Converters', 'Rad to Deg')\n";
				lString = lString + "	rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', lRadToDegrees, 'a')\n";
				lString = lString + "	rs_ConnectBox(lRadToDegrees, 'Result', lConverterNumToVecBox, 'AXIS_NAME')\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lConstrainedObject, " + ((depth - 3) * -400) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lAddBoxOffset, " + ((depth - 2) * -400) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lOffsetObject, " + ((depth - 1) * -400) + " , " + (BoxCount + 1) * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lConverterNumToVecBox, " + ((depth - 1) * -400) + ", " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lRadToDegrees, " + (depth * -400) + ", " + BoxCount * 200 + ")\n";
				lString = lString + "else:\n";
				lString = lString + "	rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', lConverterNumToVecBox, 'AXIS_NAME')\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lConstrainedObject, " + ((depth - 2) * -400) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lAddBoxOffset, " + ((depth - 1) * -400) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lOffsetObject, " + (depth * -400) + " , " + (BoxCount + 1) * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lConverterNumToVecBox, " + (depth * -400) + ", " + BoxCount * 200 + ")\n";

	
			}else{
				
				lString = lString + "if \"Rotation\" in \"TRANSFORM_NAME\":\n";
				lString = lString + "	lRadToDegrees = lRelConstraint.CreateFunctionBox('Converters', 'Rad to Deg')\n";
				lString = lString + "	rs_ConnectBox(" + expression + ", 'Result', lRadToDegrees, 'a')\n";
				lString = lString + "	rs_ConnectBox(lRadToDegrees, 'Result', lConverterNumToVecBox, 'AXIS_NAME')\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lConstrainedObject, " + ((depth - 3) * -400) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lAddBoxOffset, " + ((depth - 2) * -400) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lOffsetObject, " + ((depth - 1) * -400) + " , " + (BoxCount + 1) * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lConverterNumToVecBox, " + ((depth - 1) * -400) + ", " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lRadToDegrees, " + (depth * -400) + ", " + BoxCount * 200 + ")\n";
				lString = lString + "else:\n";
				lString = lString + "	rs_ConnectBox(" + expression + ", 'Result', lConverterNumToVecBox, 'AXIS_NAME')\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lConstrainedObject, " + ((depth - 2) * -400) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lAddBoxOffset, " + ((depth - 1) * -400) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lOffsetObject, " + (depth * -400) + " , " + (BoxCount + 1) * 200 + ")\n";
				lString = lString + "	lRelConstraint.SetBoxPosition(lConverterNumToVecBox, " + (depth * -400) + ", " + BoxCount * 200 + ")\n";
				
			}

		}
		
		stringArray.add(lString);
		
		String lCodeString = "";
		
		for( String oneItem : stringArray ) {
			
			lCodeString = lCodeString + oneItem;
		
		}
		
		return lCodeString; 
		
	}
	
	public static int BoxCount = 0;
	
//-------------------------------------------------------------------------------------------------
	
	public static int EqualNum = 0;
	
	// 	relation
	//	expr '==' expr
	@Override
    public String visitEqualTo(MaxFloatExpressionParser.EqualToContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		EqualNum = EqualNum + 1; 
		
		String lIdenticalBox = "lIdentical" + EqualNum; 
		
		lString = lString + lIdenticalBox + " 	= lRelConstraint.CreateFunctionBox('Number', 'Is Identical (a == b)')\n";
		
		String left 	= visit(ctx.expr(0));  // get value of left subexpression
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(left.trim()).floatValue();
			lString = lString + lIdenticalBox + "A = rs_FindAnimationNode(" + lIdenticalBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lIdenticalBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (left.contains(" ")){
					
				String[] lControlAttribute = left.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lIdenticalBox + ", 'a')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + left + ", 'Result', " + lIdenticalBox + ", 'a')\n";
				
			}

		}
		
        String right 	= visit(ctx.expr(1));  // get value of right subexpression
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(right.trim()).floatValue();
			lString = lString + lIdenticalBox + "B = rs_FindAnimationNode(" + lIdenticalBox + ".AnimationNodeInGet(),'b')\n";
			lString = lString + lIdenticalBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (right.contains(" ")){
					
				String[] lControlAttribute = right.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lIdenticalBox + ", 'b')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + right + ", 'Result', " + lIdenticalBox + ", 'b')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lIdenticalBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
				
		return lIdenticalBox;
		
	}

//-------------------------------------------------------------------------------------------------
	
	public static int NotEqualNum = 0;
	
	//	expr '!=' expr	
	@Override
    public String visitNotEqualTo(MaxFloatExpressionParser.NotEqualToContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		NotEqualNum = NotEqualNum + 1;
		
		String lDifferentBox = "lDifferent" + NotEqualNum; 
		
		lString = lString + lDifferentBox + " 	= lRelConstraint.CreateFunctionBox('Number', 'Is Different (a != b)')\n";
		
		String left 	= visit(ctx.expr(0));  // get value of left subexpression
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(left.trim()).floatValue();
			lString = lString + lDifferentBox + "A = rs_FindAnimationNode(" + lDifferentBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lDifferentBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
		
			if (left.contains(" ")){
					
				String[] lControlAttribute = left.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lDifferentBox + ", 'a')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + left + ", 'Result', " + lDifferentBox + ", 'a')\n";
				
			}
	
		}
		
        String right 	= visit(ctx.expr(1));  // get value of right subexpression
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(right.trim()).floatValue();
			lString = lString + lDifferentBox + "B = rs_FindAnimationNode(" + lDifferentBox + ".AnimationNodeInGet(),'b')\n";
			lString = lString + lDifferentBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
		
			if (right.contains(" ")){
					
				String[] lControlAttribute = right.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lDifferentBox + ", 'b')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + right + ", 'Result', " + lDifferentBox + ", 'b')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lDifferentBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
				
		return lDifferentBox;
		
	}
	
//-------------------------------------------------------------------------------------------------
	
	public static int LessThanNum = 0;
	
	//	expr '<'  expr
	@Override
    public String visitLessThan(MaxFloatExpressionParser.LessThanContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		LessThanNum = LessThanNum + 1;
		
		String lLessBox = "lLess" + LessThanNum; 
		
		lString = lString + lLessBox + " 	= lRelConstraint.CreateFunctionBox('Number', 'Is Less (a < b)')\n";
		
		String left 	= visit(ctx.expr(0));  // get value of left subexpression
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(left.trim()).floatValue();
			lString = lString + lLessBox + "A = rs_FindAnimationNode(" + lLessBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lLessBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (left.contains(" ")){
					
				String[] lControlAttribute = left.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lLessBox + ", 'a')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + left + ", 'Result', " + lLessBox + ", 'a')\n";
				
			}
	
		}
		
        String right 	= visit(ctx.expr(1));  // get value of right subexpression
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(right.trim()).floatValue();
			lString = lString + lLessBox + "B = rs_FindAnimationNode(" + lLessBox + ".AnimationNodeInGet(),'b')\n";
			lString = lString + lLessBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (right.contains(" ")){
					
				String[] lControlAttribute = right.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lLessBox + ", 'b')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + right + ", 'Result', " + lLessBox + ", 'b')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lLessBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
				
		return lLessBox;
		
	}
	
//-------------------------------------------------------------------------------------------------
	
	public static int LessThanEqualNum = 0;
	
	//	expr '<=' expr
	@Override
    public String visitLessThanEqualTo(MaxFloatExpressionParser.LessThanEqualToContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		LessThanEqualNum = LessThanEqualNum + 1;
		
		String lLessEqualBox = "lLessEqual" + LessThanEqualNum; 
		
		lString = lString + lLessEqualBox + " 	= lRelConstraint.CreateFunctionBox('Number', 'Is Less or Equal (a <= b)')\n";
		
		String left 	= visit(ctx.expr(0));  // get value of left subexpression
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(left.trim()).floatValue();
			lString = lString + lLessEqualBox + "A = rs_FindAnimationNode(" + lLessEqualBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lLessEqualBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (left.contains(" ")){
					
				String[] lControlAttribute = left.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lLessEqualBox + ", 'a')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + left + ", 'Result', " + lLessEqualBox + ", 'a')\n";

			}
	
		}
		
        String right 	= visit(ctx.expr(1));  // get value of right subexpression
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(right.trim()).floatValue();
			lString = lString + lLessEqualBox + "B = rs_FindAnimationNode(" + lLessEqualBox + ".AnimationNodeInGet(),'b')\n";
			lString = lString + lLessEqualBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (right.contains(" ")){
					
				String[] lControlAttribute = right.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lLessEqualBox + ", 'b')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + right + ", 'Result', " + lLessEqualBox + ", 'b')\n";
				
			}
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lLessEqualBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
				
		return lLessEqualBox;
		
	}
	
//-------------------------------------------------------------------------------------------------
	
	public static int GreaterThanNum = 0;
	
	//	expr '>'  expr	
	@Override
    public String visitGreaterThan(MaxFloatExpressionParser.GreaterThanContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		GreaterThanNum = GreaterThanNum + 1;
		
		String lGreaterBox = "lGreater" + GreaterThanNum; 
		
		lString = lString + lGreaterBox + " 	= lRelConstraint.CreateFunctionBox('Number', 'Is Greater (a > b)')\n";
		
		String left 	= visit(ctx.expr(0));  // get value of left subexpression
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(left.trim()).floatValue();
			lString = lString + lGreaterBox + "A = rs_FindAnimationNode(" + lGreaterBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lGreaterBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (left.contains(" ")){
					
				String[] lControlAttribute = left.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lGreaterBox + ", 'a')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + left + ", 'Result', " + lGreaterBox + ", 'a')\n";
				
			}
	
		}
		
        String right 	= visit(ctx.expr(1));  // get value of right subexpression
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(right.trim()).floatValue();
			lString = lString + lGreaterBox + "B = rs_FindAnimationNode(" + lGreaterBox + ".AnimationNodeInGet(),'b')\n";
			lString = lString + lGreaterBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (right.contains(" ")){
					
				String[] lControlAttribute = right.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lGreaterBox + ", 'b')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + right + ", 'Result', " + lGreaterBox + ", 'b')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lGreaterBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
				
		return lGreaterBox;

	}
	
//-------------------------------------------------------------------------------------------------
	
	public static int GreaterThanEqualNum = 0;
	
	//	expr '>='  expr	
	@Override
    public String visitGreaterThanEqualTo(MaxFloatExpressionParser.GreaterThanEqualToContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		GreaterThanEqualNum = GreaterThanEqualNum + 1;
		
		String lGreaterEqualBox = "lGreaterEqual" + GreaterThanEqualNum; 
		
		lString = lString + lGreaterEqualBox + " 	= lRelConstraint.CreateFunctionBox('Number', 'Is Greater or Equal (a >= b)')')\n";
		
		String left 	= visit(ctx.expr(0));  // get value of left subexpression
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(left.trim()).floatValue();
			lString = lString + lGreaterEqualBox + "A = rs_FindAnimationNode(" + lGreaterEqualBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lGreaterEqualBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (left.contains(" ")){
					
				String[] lControlAttribute = left.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lGreaterEqualBox + ", 'a')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + left + ", 'Result', " + lGreaterEqualBox + ", 'a')\n";
				
			}
	
		}
		
        String right 	= visit(ctx.expr(1));  // get value of right subexpression
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(right.trim()).floatValue();
			lString = lString + lGreaterEqualBox + "B = rs_FindAnimationNode(" + lGreaterEqualBox + ".AnimationNodeInGet(),'b')\n";
			lString = lString + lGreaterEqualBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (right.contains(" ")){
					
				String[] lControlAttribute = right.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " + lGreaterEqualBox + ", 'b')\n";
	
			}else{
				
				lString = lString + "rs_ConnectBox(" + right + ", 'Result', " + lGreaterEqualBox + ", 'b')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lGreaterEqualBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
				
		return lGreaterEqualBox;

	}
	
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
		
	//	expr
	//	expr ('*'|'/') expr

	public static int MultNum = 0;
	public static int DivNum  = 0;
	
	@Override
    public String visitMultiplyDivide(MaxFloatExpressionParser.MultiplyDivideContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		if ( ctx.op.getType() == MaxFloatExpressionParser.MULT ){
				
				MultNum = MultNum + 1;
				
				String lMultBox = "lMultiplyBox" + MultNum; 
				
				lString = lString + lMultBox + " 	= lRelConstraint.CreateFunctionBox( 'Number', 'Multiply (a x b)' )\n";

				String left 	= visit(ctx.expr(0));  // get value of left subexpression
				
				BoxCount = BoxCount + 1;
				
				try
				{	
				
					float 	fLeft  = Float.valueOf(left.trim()).floatValue();
					lString = lString + lMultBox + "A = rs_FindAnimationNode(" + lMultBox + ".AnimationNodeInGet(),'a')\n";
					lString = lString + lMultBox + "A.WriteData([" + fLeft + "])\n";
				
				}catch(NumberFormatException nfe){
					
					if (left.contains(" ")){
					
						String[] lControlAttribute = left.split(" ", 2);
						
						lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lMultBox + ", 'a')\n";
						
					}else{
			
						lString = lString + "rs_ConnectBox(" + left + ", 'Result', " 	+ lMultBox + ", 'a')\n";
						
					}
				
				}
				
				String right 	= visit(ctx.expr(1));  // get value of right subexpression
				
				try
				{	
					float 	fRight  = Float.valueOf(right.trim()).floatValue();
					lString = lString + lMultBox + "B = rs_FindAnimationNode(" + lMultBox + ".AnimationNodeInGet(),'b')\n";
					lString = lString + lMultBox + "B.WriteData([" + fRight + "])\n";
					
				}catch(NumberFormatException nfe){
					
					if (right.contains(" ")){
					
						String[] lControlAttribute = right.split(" ", 2);
						
						lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lMultBox + ", 'b')\n";
						
					}else{
			
						lString = lString + "rs_ConnectBox(" + right + ", 'Result', " 	+ lMultBox + ", 'b')\n";
						
					}
					
				}
				
				lString = lString + "lRelConstraint.SetBoxPosition(" + lMultBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
				
				depth = depth - 1;
				
				stringArray.add(lString);
				
				return lMultBox;
				
		}else{
				
				DivNum = DivNum + 1;
				
				String lDivBox = "lDivideBox" 	+ DivNum;
				
				lString = lString + lDivBox  + "  	= lRelConstraint.CreateFunctionBox( 'Number', 'Divide (a/b)')\n";
				
				String left 	= visit(ctx.expr(0));  // get value of left subexpression
				
				BoxCount = BoxCount + 1;
				
				try
				{	
					
					float 	fLeft  = Float.valueOf(left.trim()).floatValue();
					lString = lString + lDivBox  + "A = rs_FindAnimationNode(" + lDivBox + ".AnimationNodeInGet(),'a')\n";
					lString = lString + lDivBox  + "A.WriteData([" + fLeft + "])\n";
				
				}catch(NumberFormatException nfe){
					
					if (left.contains(" ")){
					
						String[] lControlAttribute = left.split(" ", 2);
						
						lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lDivBox + ", 'a')\n";
						
					}else{
			
						lString = lString + "rs_ConnectBox(" + left + ", 'Result', " 	+ lDivBox + ", 'a')\n";
						
					}
					
				}
				
				String right 	= visit(ctx.expr(1));  // get value of right subexpression
				
				try
				{	
				
					float 	fRight  = Float.valueOf(right.trim()).floatValue();
					lString = lString + lDivBox  + "B = rs_FindAnimationNode(" + lDivBox  + ".AnimationNodeInGet(),'b')\n";
					lString = lString + lDivBox  + "B.WriteData([" + fRight + "])\n";
				
				}catch(NumberFormatException nfe){
					
					if (right.contains(" ")){
					
						String[] lControlAttribute = right.split(" ", 2);
						
						lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lDivBox + ", 'b')\n";
						
					}else{
			
						lString = lString + "rs_ConnectBox(" + right + ", 'Result', " 	+ lDivBox + ", 'b')\n";
						
					}
				
				}
				
				lString = lString + "lRelConstraint.SetBoxPosition(" + lDivBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
				
				depth = depth - 1;
				
				stringArray.add(lString);
				
				return lDivBox;
		
		}
		
	}
	
//-------------------------------------------------------------------------------------------------
	
	//	expr ('+'|'-') expr	
	
	public static int AddNum = 0;
	public static int SubNum  = 0;
	
	@Override
    public String visitAddSubtract(MaxFloatExpressionParser.AddSubtractContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		if ( ctx.op.getType() == MaxFloatExpressionParser.ADD ){
			
			AddNum = AddNum + 1;
			
			String lAddBox = "lAdd" + AddNum; 
			
			lString = lString + lAddBox + " = lRelConstraint.CreateFunctionBox('Number', 'Add (a + b)')\n";
		
			String left 	= visit(ctx.expr(0));  // get value of left subexpression
			
			BoxCount = BoxCount + 1;
			
			try
			{	
			
				float 	fLeft  = Float.valueOf(left.trim()).floatValue();
				lString = lString + lAddBox + "A = rs_FindAnimationNode(" + lAddBox + ".AnimationNodeInGet(),'a')\n";
				lString = lString + lAddBox + "A.WriteData([" + fLeft + "])\n";
				
			}catch(NumberFormatException nfe){
			
				if (left.contains(" ")){
					
					String[] lControlAttribute = left.split(" ", 2);
					
					lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lAddBox + ", 'a')\n";
					
				}else{
		
					lString = lString + "rs_ConnectBox(" + left + ", 'Result', " 	+ lAddBox + ", 'a')\n";
					
				}
			
			}
			
			String right 	= visit(ctx.expr(1));  // get value of right subexpression
			
			try
			{	
			
				float 	fRight  = Float.valueOf(right.trim()).floatValue();
				lString = lString + lAddBox + "B = rs_FindAnimationNode(" 	+ lAddBox + ".AnimationNodeInGet(),'b')\n";
				lString = lString + lAddBox + "B.WriteData([" + fRight + "])\n";
				
			}catch(NumberFormatException nfe){
			
				if (right.contains(" ")){
					
					String[] lControlAttribute = right.split(" ", 2);
					
					lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lAddBox + ", 'b')\n";
					
				}else{
		
					lString = lString + "rs_ConnectBox(" + right + ", 'Result', " 	+ lAddBox + ", 'b')\n";
					
				}
			
			}
			
			lString = lString + "lRelConstraint.SetBoxPosition(" + lAddBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
			
			depth = depth - 1;
			
			stringArray.add(lString);
			
			return lAddBox;
			
		}else{
			
			SubNum = SubNum + 1;
			
			String lSubBox = "lSub" + SubNum;
			
			lString = lString + lSubBox + " = lRelConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n";
		
			String left 	= visit(ctx.expr(0));  // get value of left subexpression
			
			BoxCount = BoxCount + 1;
			
			try
			{	
			
				float 	fLeft  = Float.valueOf(left.trim()).floatValue();
				lString = lString + lSubBox + "A = rs_FindAnimationNode(" + lSubBox + ".AnimationNodeInGet(),'a')\n";
				lString = lString + lSubBox + "A.WriteData([" + fLeft + "])\n";
				
			}catch(NumberFormatException nfe){
			
				if (left.contains(" ")){
					
					String[] lControlAttribute = left.split(" ", 2);
					
					lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lSubBox + ", 'a')\n";
					
				}else{
		
					lString = lString + "rs_ConnectBox(" + left + ", 'Result', " 	+ lSubBox + ", 'a')\n";
					
				}
			
			}
			
			String right 	= visit(ctx.expr(1));  // get value of right subexpression
			
			try
			{	
			
				float 	fRight  = Float.valueOf(right.trim()).floatValue();
				lString = lString + lSubBox + "B = rs_FindAnimationNode(" + lSubBox + ".AnimationNodeInGet(),'b')\n";
				lString = lString + lSubBox + "B.WriteData([" + fRight + "])\n";
				
			}catch(NumberFormatException nfe){
			
				if (right.contains(" ")){
					
					String[] lControlAttribute = right.split(" ", 2);
					
					lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lSubBox + ", 'b')\n";
					
				}else{
		
					lString = lString + "rs_ConnectBox(" + right + ", 'Result', " 	+ lSubBox + ", 'b')\n";
					
				}
			
			}
			
			lString = lString + "lRelConstraint.SetBoxPosition(" + lSubBox + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
			
			depth = depth - 1;
			
			stringArray.add(lString);
			
			return lSubBox;
		
		}

	}
	
//-------------------------------------------------------------------------------------------------
	
	//	'-'	NUMBER
	@Override
    public String visitUniaryNumber(MaxFloatExpressionParser.UniaryNumberContext ctx){
		return ( "-" + ctx.NUMBER().getText());
	}

//-------------------------------------------------------------------------------------------------
	
	// NUMBER
	@Override
    public String visitNumber(MaxFloatExpressionParser.NumberContext ctx){
		return ctx.NUMBER().getText();
	}

//-------------------------------------------------------------------------------------------------

	//	'-'	IDENT
	@Override
    public String visitUniaryIdent(MaxFloatExpressionParser.UniaryIdentContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		String lIdent = ctx.IDENT().getText();
		
		if (lIdent.contains("#")){
			
			String[] lControlAttribute = lIdent.split("#", 2);
			
			lString = lString + "l" + lControlAttribute[0] + " = FBFindModelByLabelName(\"" + lControlAttribute[0] + "\")\n";
			lString = lString + "for iProp in l" + lControlAttribute[0] + ".PropertyList:\n";    
			lString = lString + "	if iProp.IsUserProperty():\n";
			lString = lString + "		if str(type(iProp)) == \"<class 'pyfbsdk.FBPropertyAnimatableDouble'>\":\n";
            lString = lString + "			iProp.SetAnimated(True)\n";
			lString = lString + "			glo.gScene.Evaluate()\n";
			lString = lString + "l" + lControlAttribute[0] + "Sender = lRelConstraint.SetAsSource(" + "l" + lControlAttribute[0] + ")\n";
			lString = lString + "l" + lControlAttribute[0] + "Sender.UseGlobalTransforms = False\n";
			BoxCount = BoxCount + 1;
			
			if (lControlAttribute[1].contains("Position")){
				
				lString = lString + "lRelConstraint.SetBoxPosition(l" + lControlAttribute[0] + "Sender, " + ((depth * -400) - 1000) + " , " + BoxCount * 200 + ")\n";
				
				// Adding Zero Controller							
				lString = lString + "lZeroController = lRelConstraint.CreateFunctionBox('Vector', 'Subtract (V1 - V2)')\n";
				lString = lString + "rs_ConnectBox(" + "l" + lControlAttribute[0] + "Sender, 'Lcl Translation', lZeroController, 'V1')\n";
				lString = lString + "lZeroVector = l" + lControlAttribute[0] + ".Translation.Data\n";
				lString = lString + "lZeroControllerV2 = rs_FindAnimationNode(lZeroController.AnimationNodeInGet(),'V2')\n";
				lString = lString + "lZeroControllerV2.WriteData([lZeroVector[0], lZeroVector[1], lZeroVector[2]])\n";	
				lString = lString + "lRelConstraint.SetBoxPosition(lZeroController, " + ((depth * -400) - 800) + " , " + BoxCount * 200 + ")\n";
				
				lString = lString + "lConverterBox = lRelConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n";
				lString = lString + "rs_ConnectBox(lZeroController, 'Result', lConverterBox, 'V')\n";
				lString = lString + "lRelConstraint.SetBoxPosition(lConverterBox, " + ((depth * -400) - 400) + " , " + BoxCount * 200 + ")\n";
				
				lString = lString + "lUniaryIdent 	= lRelConstraint.CreateFunctionBox( 'Number', 'Multiply (a x b)' )\n";
				lString = lString + "lUniaryIdentB = rs_FindAnimationNode(lUniaryIdent.AnimationNodeInGet(),'b')\n";
				lString = lString + "lUniaryIdentB.WriteData([-1.0])\n";
				
				if (lControlAttribute[1].contains("X")){
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'X', lUniaryIdent, 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lUniaryIdent, " + depth * -400 + " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lUniaryIdent");
				
				} else if (lControlAttribute[1].contains("Y")) {
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'Y', lUniaryIdent, 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lUniaryIdent, " + depth * -400 + " , " + BoxCount * 200 + ")\n";
				
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lUniaryIdent");
				
				} else {
				
					lString = lString + "rs_ConnectBox(lConverterBox, 'Z', lUniaryIdent, 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lUniaryIdent, " + depth * -400 + " , " + BoxCount * 200 + ")\n";
				
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lConverterBox Z");
				
				}
				
			} else if (lControlAttribute[1].contains("Rotation")) {
				
				lDegreesToRadianNum = lDegreesToRadianNum + 1;
				
				lString = lString + "lRelConstraint.SetBoxPosition(l" + lControlAttribute[0] + "Sender, " + ((depth * -400) - 1800) + " , " + BoxCount * 200 + ")\n";
				
				// Adding Zero Controller				
				lString = lString + "lZeroController = lRelConstraint.CreateFunctionBox('Vector', 'Subtract (V1 - V2)')\n";
				lString = lString + "rs_ConnectBox(" + "l" + lControlAttribute[0] + "Sender, 'Lcl Rotation', lZeroController, 'V1')\n";
				lString = lString + "lZeroVector = l" + lControlAttribute[0] + ".Rotation.Data\n";
				lString = lString + "lZeroControllerV2 = rs_FindAnimationNode(lZeroController.AnimationNodeInGet(),'V2')\n";
				lString = lString + "lZeroControllerV2.WriteData([lZeroVector[0], lZeroVector[1], lZeroVector[2]])\n";	
				lString = lString + "lRelConstraint.SetBoxPosition(lZeroController, " + ((depth * -400) - 1200) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "lConverterBox = lRelConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n";
				lString = lString + "rs_ConnectBox(lZeroController, 'Result', lConverterBox, 'V')\n";
				lString = lString + "lRelConstraint.SetBoxPosition(lConverterBox, " + ((depth * -400) - 800) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "lDegreesToRadian" + lDegreesToRadianNum + " = lRelConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n";
				
				lString = lString + "lUniaryIdent 	= lRelConstraint.CreateFunctionBox( 'Number', 'Multiply (a x b)' )\n";
				lString = lString + "lUniaryIdentB = rs_FindAnimationNode(lUniaryIdent.AnimationNodeInGet(),'b')\n";
				lString = lString + "lUniaryIdentB.WriteData([-1.0])\n";
				
				if (lControlAttribute[1].contains("X")){
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'X', lUniaryIdent, 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lUniaryIdent, " + ((depth * -400) - 400) + " , " + BoxCount * 200 + ")\n";
					
					lString = lString + "rs_ConnectBox(lUniaryIdent, 'Result', lDegreesToRadian" + lDegreesToRadianNum + ", 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lDegreesToRadian" + lDegreesToRadianNum + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lDegreesToRadian" + lDegreesToRadianNum + " Result");
				
				} else if (lControlAttribute[1].contains("Y")) {
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'Y', lUniaryIdent, 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lUniaryIdent, " + ((depth * -400) - 400) + " , " + BoxCount * 200 + ")\n";
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'Result', lDegreesToRadian" + lDegreesToRadianNum + ", 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lDegreesToRadian" + lDegreesToRadianNum + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lDegreesToRadian" + lDegreesToRadianNum + " Result");
				
				} else {
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'Z', lUniaryIdent, 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lUniaryIdent, " + ((depth * -400) - 400) + " , " + BoxCount * 200 + ")\n";
					
					lString = lString + "rs_ConnectBox(lUniaryIdent, 'Result', lDegreesToRadian" + lDegreesToRadianNum + ", 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lDegreesToRadian" + lDegreesToRadianNum + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lDegreesToRadian" + lDegreesToRadianNum + " Result");
				
				}
				
			} else if (lControlAttribute[1].contains("Scale")) {
				
				lString = lString + "lRelConstraint.SetBoxPosition(l" + lControlAttribute[0] + "Sender, " + ((depth * -400) - 800) + " , " + BoxCount * 200 + ")\n";
				
				lString = lString + "lConverterBox = lRelConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n";
				lString = lString + "rs_ConnectBox(" + "l" + lControlAttribute[0] + "Sender, 'Lcl Scaling', lConverterBox, 'V')\n";
				lString = lString + "lRelConstraint.SetBoxPosition(lConverterBox, " + ((depth * -400) - 400) + " , " + BoxCount * 200 + ")\n";
				
				lString = lString + "lUniaryIdent 	= lRelConstraint.CreateFunctionBox( 'Number', 'Multiply (a x b)' )\n";
				lString = lString + "lUniaryIdentB = rs_FindAnimationNode(lUniaryIdent.AnimationNodeInGet(),'b')\n";
				lString = lString + "lUniaryIdentB.WriteData([-1.0])\n";
				
				if (lControlAttribute[1].contains("X")){
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'X', lUniaryIdent, 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lUniaryIdent, " + depth * -400+ " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lUniaryIdent Result");
				
				} else if (lControlAttribute[1].contains("Y")) {
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'Y', lUniaryIdent, 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lUniaryIdent, " + depth * -400+ " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lUniaryIdent Result");
				
				} else {
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'Z', lUniaryIdent, 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lUniaryIdent, " + depth * -400+ " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lUniaryIdent Result");
				
				}
			
			} else {
				
				lString = lString + "lUniaryIdent 	= lRelConstraint.CreateFunctionBox( 'Number', 'Multiply (a x b)' )\n";
				lString = lString + "lUniaryIdentB = rs_FindAnimationNode(lUniaryIdent.AnimationNodeInGet(),'b')\n";
				lString = lString + "lUniaryIdentB.WriteData([-1.0])\n";
				lString = lString + "rs_ConnectBox(l" + lControlAttribute[0] + "Sender, '" + lControlAttribute[1] + "', lUniaryIdent, 'a')\n";
				lString = lString + "lRelConstraint.SetBoxPosition(l" + lControlAttribute[0] + "Sender, " + depth * -400+ " , " + BoxCount * 200 + ")\n";
				
				stringArray.add(lString);
			
				depth = depth - 1;
				
				return ("lUniaryIdent Result");
							
			}
			
			
		}
		
		depth = depth - 1;
		
		return lIdent;
	}

//-------------------------------------------------------------------------------------------------
	
	public static int lDegreesToRadianNum = 0;
	
	// IDENT
	@Override
    public String visitIdentifier(MaxFloatExpressionParser.IdentifierContext ctx){
		
		depth = depth + 1;
		String lString = "";
		
		String lIdent = ctx.IDENT().getText();
		
		if (lIdent.contains("#")){
			
			String[] lControlAttribute = lIdent.split("#", 2);
			
			lString = lString + "l" + lControlAttribute[0] + " = FBFindModelByLabelName(\"" + lControlAttribute[0] + "\")\n";
			lString = lString + "for iProp in l" + lControlAttribute[0] + ".PropertyList:\n";    
			lString = lString + "	if iProp.IsUserProperty():\n";
			lString = lString + "		if str(type(iProp)) == \"<class 'pyfbsdk.FBPropertyAnimatableDouble'>\":\n";
            lString = lString + "			iProp.SetAnimated(True)\n";
			lString = lString + "			glo.gScene.Evaluate()\n";
			lString = lString + "l" + lControlAttribute[0] + "Sender = lRelConstraint.SetAsSource(" + "l" + lControlAttribute[0] + ")\n";
			lString = lString + "l" + lControlAttribute[0] + "Sender.UseGlobalTransforms = False\n";
			BoxCount = BoxCount + 1;
			
			if (lControlAttribute[1].contains("Position")){
				
				lString = lString + "lRelConstraint.SetBoxPosition(l" + lControlAttribute[0] + "Sender, " + ((depth * -400) - 1000) + " , " + BoxCount * 200 + ")\n";
				
				// Adding Zero Controller							
				lString = lString + "lZeroController = lRelConstraint.CreateFunctionBox('Vector', 'Subtract (V1 - V2)')\n";
				lString = lString + "rs_ConnectBox(" + "l" + lControlAttribute[0] + "Sender, 'Lcl Translation', lZeroController, 'V1')\n";
				lString = lString + "lZeroVector = l" + lControlAttribute[0] + ".Translation.Data\n";
				lString = lString + "lZeroControllerV2 = rs_FindAnimationNode(lZeroController.AnimationNodeInGet(),'V2')\n";
				lString = lString + "lZeroControllerV2.WriteData([lZeroVector[0], lZeroVector[1], lZeroVector[2]])\n";	
				lString = lString + "lRelConstraint.SetBoxPosition(lZeroController, " + ((depth * -400) - 400) + " , " + BoxCount * 200 + ")\n";
				
				lString = lString + "lConverterBox = lRelConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n";
				lString = lString + "rs_ConnectBox(lZeroController, 'Result', lConverterBox, 'V')\n";
				lString = lString + "lRelConstraint.SetBoxPosition(lConverterBox, " + depth * -400 + " , " + BoxCount * 200 + ")\n";
				
				if (lControlAttribute[1].contains("X")){
				
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lConverterBox X");
				
				} else if (lControlAttribute[1].contains("Y")) {
				
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lConverterBox Y");
				
				} else {
				
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lConverterBox Z");
				
				}
				
			} else if (lControlAttribute[1].contains("Rotation")) {
				
				lDegreesToRadianNum = lDegreesToRadianNum + 1;
				
				lString = lString + "lRelConstraint.SetBoxPosition(l" + lControlAttribute[0] + "Sender, " + ((depth * -400) - 1400) + " , " + BoxCount * 200 + ")\n";
				
				// Adding Zero Controller				
				lString = lString + "lZeroController = lRelConstraint.CreateFunctionBox('Vector', 'Subtract (V1 - V2)')\n";
				lString = lString + "rs_ConnectBox(" + "l" + lControlAttribute[0] + "Sender, 'Lcl Rotation', lZeroController, 'V1')\n";
				lString = lString + "lZeroVector = l" + lControlAttribute[0] + ".Rotation.Data\n";
				lString = lString + "lZeroControllerV2 = rs_FindAnimationNode(lZeroController.AnimationNodeInGet(),'V2')\n";
				lString = lString + "lZeroControllerV2.WriteData([lZeroVector[0], lZeroVector[1], lZeroVector[2]])\n";	
				lString = lString + "lRelConstraint.SetBoxPosition(lZeroController, " + ((depth * -400) - 800) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "lConverterBox = lRelConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n";
				lString = lString + "rs_ConnectBox(lZeroController, 'Result', lConverterBox, 'V')\n";
				lString = lString + "lRelConstraint.SetBoxPosition(lConverterBox, " + ((depth * -400) - 400) + " , " + BoxCount * 200 + ")\n";
				lString = lString + "lDegreesToRadian" + lDegreesToRadianNum + " = lRelConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n";
				
				
				if (lControlAttribute[1].contains("X")){
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'X', lDegreesToRadian" + lDegreesToRadianNum + ", 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lDegreesToRadian" + lDegreesToRadianNum + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lDegreesToRadian" + lDegreesToRadianNum + " Result");
				
				} else if (lControlAttribute[1].contains("Y")) {
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'Y', lDegreesToRadian" + lDegreesToRadianNum + ", 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lDegreesToRadian" + lDegreesToRadianNum + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lDegreesToRadian" + lDegreesToRadianNum + " Result");
				
				} else {
					
					lString = lString + "rs_ConnectBox(lConverterBox, 'Z', lDegreesToRadian" + lDegreesToRadianNum + ", 'a')\n";
					lString = lString + "lRelConstraint.SetBoxPosition(lDegreesToRadian" + lDegreesToRadianNum + ", " + depth * -400 + " , " + BoxCount * 200 + ")\n";
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lDegreesToRadian" + lDegreesToRadianNum + " Result");
				
				}
				
			} else if (lControlAttribute[1].contains("Scale")) {
				
				lString = lString + "lRelConstraint.SetBoxPosition(l" + lControlAttribute[0] + "Sender, " + ((depth * -400) - 400) + " , " + BoxCount * 200 + ")\n";
				
				lString = lString + "lConverterBox = lRelConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n";
				lString = lString + "rs_ConnectBox(" + "l" + lControlAttribute[0] + "Sender, 'Lcl Scaling', lConverterBox, 'V')\n";
				lString = lString + "lRelConstraint.SetBoxPosition(lConverterBox, " + depth * -400 + " , " + BoxCount * 200 + ")\n";
				
				if (lControlAttribute[1].contains("X")){
				
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lConverterBox X");
				
				} else if (lControlAttribute[1].contains("Y")) {
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lConverterBox Y");
				
				} else {
					
					stringArray.add(lString);
			
					depth = depth - 1;
					
					return ("lConverterBox Z");
				
				}
			
			} else {
				
				lString = lString + "lRelConstraint.SetBoxPosition(l" + lControlAttribute[0] + "Sender, " + depth * -400+ " , " + BoxCount * 200 + ")\n";
				
				stringArray.add(lString);
			
				depth = depth - 1;
				
				return ("l" + lControlAttribute[0] + "Sender " + lControlAttribute[1]);
							
			}
			
			
		}
		
		depth = depth - 1;
		
		return lIdent;
	}

//-------------------------------------------------------------------------------------------------
	
	//	E
	@Override
    public String visitE(MaxFloatExpressionParser.EContext ctx){
		return "2.718281828459045";
	}
	
//-------------------------------------------------------------------------------------------------
	
	// PI
	@Override
    public String visitPi(MaxFloatExpressionParser.PiContext ctx){
		return "3.141592653589793";
	}
		
//-------------------------------------------------------------------------------------------------
	
	public static int AbsNum  = 0;
	
	//	'abs' 		LPAREN expr RPAREN
	@Override
    public String visitAbsolute(MaxFloatExpressionParser.AbsoluteContext ctx){
		
		depth = depth + 1;
		
		AbsNum = AbsNum + 1;
		
		String lAbsBox = "lAbs" + AbsNum; 
		
		String lString = lAbsBox + "= lRelConstraint.CreateFunctionBox('Number', 'Absolute(|a|)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lAbsBox + "A = rs_FindAnimationNode(" + lAbsBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lAbsBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lAbsBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lAbsBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lAbsBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lAbsBox;
		
	}
	
//-------------------------------------------------------------------------------------------------
	
	public static int AcosNum  = 0;
	
	//	'acos' 		LPAREN expr RPAREN		
	@Override
    public String visitArcCosine(MaxFloatExpressionParser.ArcCosineContext ctx){
		
		depth = depth + 1;
		
		AcosNum = AcosNum +  1;
		
		String lAcosBox = "lAcos" + AcosNum; 
		
		String lString = lAcosBox + "= lRelConstraint.CreateFunctionBox('Number', 'arccos(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lAcosBox + "A = rs_FindAnimationNode(" + lAcosBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lAcosBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lAcosBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lAcosBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lAcosBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lAcosBox;
	
	}

//-------------------------------------------------------------------------------------------------
	
	public static int AsinNum  = 0;
	
	//	'asin' 		LPAREN expr RPAREN
	@Override
    public String visitArcSine(MaxFloatExpressionParser.ArcSineContext ctx){
		
		depth = depth + 1;
		
		AsinNum = AsinNum + 1;
		
		String lAsinBox = "lAsin" + depth; 
		
		String lString = lAsinBox + "= lRelConstraint.CreateFunctionBox('Number', 'arcsin(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lAsinBox + "A = rs_FindAnimationNode(" + lAsinBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lAsinBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lAsinBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lAsinBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lAsinBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lAsinBox;
	
	}
	
//-------------------------------------------------------------------------------------------------
	
	public static int AtanNum  = 0;
	
	//	'atan' 		LPAREN expr RPAREN
	@Override
    public String visitArcTangent(MaxFloatExpressionParser.ArcTangentContext ctx){
		
		depth = depth + 1;
		
		AtanNum = AtanNum + 1;
		
		String lAtanBox = "lAtan" + AtanNum; 
		
		String lString = lAtanBox + "= lRelConstraint.CreateFunctionBox('Number', 'arctan(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lAtanBox + "A = rs_FindAnimationNode(" + lAtanBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lAtanBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lAtanBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lAtanBox + ", 'a')\n";
				
			}
				
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lAtanBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lAtanBox;
	
	}
	
//-------------------------------------------------------------------------------------------------

	public static int CeilNum  = 0;

	//	'ceil' 		LPAREN expr RPAREN
	@Override
    public String visitCeiling(MaxFloatExpressionParser.CeilingContext ctx){
		
		depth = depth + 1;
		
		CeilNum = CeilNum + 1;
		
		String lCeilBox = "lCeil" + CeilNum; 
		
		String lString = lCeilBox + "= lRelConstraint.CreateFunctionBox('My Macros', 'ceil(x)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lCeilBox + "A = rs_FindAnimationNode(" + lCeilBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lCeilBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lCeilBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lCeilBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lCeilBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lCeilBox;
	
	}

//-------------------------------------------------------------------------------------------------
	
	public static int CosNum  = 0;
	
	//	'cos' 		LPAREN expr RPAREN
	@Override
    public String visitCosine(MaxFloatExpressionParser.CosineContext ctx){
		
		depth = depth + 1;
		
		CosNum = CosNum + 1;
		
		String lCosBox = "lCos" + CosNum; 
		
		String lString = lCosBox + "= lRelConstraint.CreateFunctionBox('Number', 'Cosine cos(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lCosBox + "A = rs_FindAnimationNode(" + lCosBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lCosBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lCosBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lCosBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lCosBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lCosBox;
	
	}
	
//-------------------------------------------------------------------------------------------------
/*	
	public static int CoshNum  = 0;

	//	'cosh' 		LPAREN expr RPAREN
	@Override
    public String visitHyperbolicCosine(MaxFloatExpressionParser.HyperbolicCosineContext ctx){
		
		depth = depth + 1;
		
		CoshNum = CoshNum + 1;
		
		String lCoshBox = "lCosh" + CoshNum; 
		
		String lString = lCoshBox + "= lRelConstraint.CreateFunctionBox('Number', 'Cosine cos(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lCoshBox + "A = rs_FindAnimationNode(" + lCoshBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lCoshBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lCoshBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lCoshBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lCoshBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lCoshBox;
	
	}
*/
//-------------------------------------------------------------------------------------------------
	
	public static int DegToRadNum  = 0;
	
	//	'degToRad'	LPAREN expr RPAREN
	@Override
    public String visitDegreesToRadians(MaxFloatExpressionParser.DegreesToRadiansContext ctx){
		
		depth = depth + 1;
		
		DegToRadNum = DegToRadNum + 1;
		
		String lDegToRadBox = "lDegToRad" + DegToRadNum; 
		
		String lString = lDegToRadBox + "= lRelConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lDegToRadBox + "A = rs_FindAnimationNode(" + lDegToRadBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lDegToRadBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lDegToRadBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lDegToRadBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lDegToRadBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lDegToRadBox;
	
	}

//-------------------------------------------------------------------------------------------------
	
	public static int ExpNum  = 0;
	
	//	'exp'	 	LPAREN expr RPAREN	
	@Override
    public String visitExponent(MaxFloatExpressionParser.ExponentContext ctx){
		
		depth = depth + 1;
		
		ExpNum = ExpNum + 1; 
		
		String lExpBox = "lExp" + ExpNum; 
		
		String lString = lExpBox + "= lRelConstraint.CreateFunctionBox('Number', 'exp(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1; 
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lExpBox + "A = rs_FindAnimationNode(" + lExpBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lExpBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lExpBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lExpBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lExpBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lExpBox;
	
	}

//-------------------------------------------------------------------------------------------------

	public static int FloorNum  = 0;
	
	//	'floor'	 	LPAREN expr RPAREN	
	@Override
    public String visitFloor(MaxFloatExpressionParser.FloorContext ctx){
		
		depth = depth + 1;
		
		FloorNum = FloorNum + 1;
		
		String lFloorBox = "lFloor" + depth; 
		
		String lString = lFloorBox + "= lRelConstraint.CreateFunctionBox('My Macros', 'floor(x)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lFloorBox + "A = rs_FindAnimationNode(" + lFloorBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lFloorBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lFloorBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lFloorBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lFloorBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lFloorBox;
	
	}

//-------------------------------------------------------------------------------------------------
	
	public static int LnNum  = 0;
	
	//	'ln'	 	LPAREN expr RPAREN
	@Override
    public String visitNaturalLogarithm(MaxFloatExpressionParser.NaturalLogarithmContext ctx){
		
		depth = depth + 1;
		
		LnNum = LnNum + 1;
		
		String lLnBox = "lLn" + LnNum; 
		
		String lString = lLnBox + "= lRelConstraint.CreateFunctionBox('Number', 'ln(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lLnBox + "A = rs_FindAnimationNode(" + lLnBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lLnBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lLnBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lLnBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lLnBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lLnBox;
	
	}
	
//-------------------------------------------------------------------------------------------------
	
	public static int LogNum  = 0;
	
	//	'log' 		LPAREN expr RPAREN	
	@Override
    public String visitCommonLogarithm(MaxFloatExpressionParser.CommonLogarithmContext ctx){
		
		depth = depth + 1;
		
		LogNum = LogNum + 1;
		
		String lLogBox = "lLog" + LogNum; 
		
		String lString = lLogBox + "= lRelConstraint.CreateFunctionBox('Number', 'log(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lLogBox + "A = rs_FindAnimationNode(" + lLogBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lLogBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lLogBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lLogBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lLogBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lLogBox;
	
	}

//-------------------------------------------------------------------------------------------------
	
	public static int RadToDegNum  = 0;
	
	//	'radToDeg' 	LPAREN expr RPAREN
	@Override
    public String visitRadianToDegrees(MaxFloatExpressionParser.RadianToDegreesContext ctx){
		
		depth = depth + 1;
		
		RadToDegNum = RadToDegNum + 1;
		
		String lRadToDegBox = "lRadToDeg" + RadToDegNum; 
		
		String lString = lRadToDegBox + "= lRelConstraint.CreateFunctionBox('Converters', 'Rad To Deg')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lRadToDegBox + "A = rs_FindAnimationNode(" + lRadToDegBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lRadToDegBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lRadToDegBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lRadToDegBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lRadToDegBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lRadToDegBox;
	
	}

//-------------------------------------------------------------------------------------------------
	
	public static int SinNum  = 0;
	
	//	'sin' 		LPAREN expr RPAREN
	@Override
    public String visitSine(MaxFloatExpressionParser.SineContext ctx){
		
		depth = depth + 1;
		
		SinNum = SinNum + 1;
		
		String lSinBox = "lSin" + SinNum; 
		
		String lString = lSinBox + "= lRelConstraint.CreateFunctionBox('Number', 'Sine sin(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lSinBox + "A = rs_FindAnimationNode(" + lSinBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lSinBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lSinBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lSinBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lSinBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lSinBox;
	
	}

//-------------------------------------------------------------------------------------------------
/*	
	public static int SinhNum  = 0;

	//	'sinh'	 	LPAREN expr RPAREN
	@Override
    public String visitHyperbolicSine(MaxFloatExpressionParser.HyperbolicSineContext ctx){
		
		depth = depth + 1;
		
		SinhNum = SinhNum + 1;
		
		String lSinhBox = "lSinh" + SinhNum; 
		
		String lString = lSinhBox + "= lRelConstraint.CreateFunctionBox('Number', 'Sine sin(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lSinhBox + "A = rs_FindAnimationNode(" + lSinhBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lSinhBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lSinhBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lSinhBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lSinhBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lSinhBox;
		
	}
*/	
//-------------------------------------------------------------------------------------------------
	
	public static int SqrtNum  = 0;
	
	//	'sqrt'	 	LPAREN expr RPAREN
	@Override
    public String visitSquareRoot(MaxFloatExpressionParser.SquareRootContext ctx){
		
		depth = depth + 1;
		
		SqrtNum = SqrtNum + 1;
		
		String lSqrtBox = "lSqrt" + SqrtNum; 
		
		String lString = lSqrtBox + "= lRelConstraint.CreateFunctionBox('Number', 'sqrt(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lSqrtBox + "A = rs_FindAnimationNode(" + lSqrtBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lSqrtBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lSqrtBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lSqrtBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lSqrtBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lSqrtBox;
		
	}
	
//-------------------------------------------------------------------------------------------------
	
	public static int TanNum  = 0;
	
	//	'tan'	 	LPAREN expr RPAREN	
	@Override
    public String visitTangent(MaxFloatExpressionParser.TangentContext ctx){
		
		depth = depth + 1;
		
		TanNum = TanNum + 1;
		
		String lTanBox = "lTan" + TanNum; 
		
		String lString = lTanBox + "= lRelConstraint.CreateFunctionBox('Number', 'Tangeant tan(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lTanBox + "A = rs_FindAnimationNode(" + lTanBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lTanBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lTanBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lTanBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lTanBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lTanBox;
		
	}

//-------------------------------------------------------------------------------------------------
/*	
	public static int TanhNum  = 0;

	//	'tanh'	 	LPAREN expr RPAREN	
	@Override
    public String visitHyperbolicTangent(MaxFloatExpressionParser.HyperbolicTangentContext ctx){
		
		depth = depth + 1;
		
		TanhNum = TanhNum + 1;
		
		String lTanhBox = "lTanh" + TanhNum; 
		
		String lString = lTanhBox + "= lRelConstraint.CreateFunctionBox('Number', 'Tangeant tan(a)')\n";
		
		String expression 	= visit(ctx.expr());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expression.trim()).floatValue();
			lString = lString + lTanhBox + "A = rs_FindAnimationNode(" + lTanhBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lTanhBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expression.contains(" ")){
					
				String[] lControlAttribute = expression.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lTanhBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expression + ", 'Result', " 	+ lTanhBox + ", 'a')\n";
				
			}

		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lTanhBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lTanhBox;
	
	}
*/	
//-------------------------------------------------------------------------------------------------
	
	public static int MaxNum  = 0;

	//	'max'		LPAREN expr COMMA expr RPAREN
	@Override
    public String visitMaximum(MaxFloatExpressionParser.MaximumContext ctx){
		
		depth = depth + 1;
		
		MaxNum = MaxNum + 1;
		
		String lMaxBox = "lMax" + MaxNum; 
		
		String lString = lMaxBox + "= lRelConstraint.CreateFunctionBox('My Macros', 'max(x,y)')\n";
		
		String expressionX 	= visit(ctx.expr(0));
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionX.trim()).floatValue();
			
			lString = lString + lMaxBox + "A = rs_FindAnimationNode(" + lMaxBox + ".AnimationNodeInGet(), 'a')\n";
			lString = lString + lMaxBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionX.contains(" ")){
					
				String[] lControlAttribute = expressionX.split(" ", 2);
								
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lMaxBox + ", 'a')\n";
				
			}else{
				
				lString = lString + "rs_ConnectBox(" + expressionX + ", 'Result', " 	+ lMaxBox + ", 'a')\n";
				
			}
	
		}
		
		String expressionY 	= visit(ctx.expr(1));
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionY.trim()).floatValue();
			
			lString = lString + lMaxBox + "B = rs_FindAnimationNode(" + lMaxBox + ".AnimationNodeInGet(), 'b')\n";
			lString = lString + lMaxBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionY.contains(" ")){
					
				String[] lControlAttribute = expressionY.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lMaxBox + ", 'b')\n";
				
			}else{
								
				lString = lString + "rs_ConnectBox(" + expressionY + ", 'Result', " 	+ lMaxBox + ", 'b')\n";
				
			}
			
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lMaxBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lMaxBox;
		
	}

//-------------------------------------------------------------------------------------------------
	
	public static int MinNum  = 0;

	//	'min'		LPAREN expr COMMA expr RPAREN
	@Override
    public String visitMinimum(MaxFloatExpressionParser.MinimumContext ctx){
		
		depth = depth + 1;
		
		MinNum = MinNum + 1; 
		
		String lMinBox = "lMin" + MinNum; 
		
		String lString = lMinBox + "= lRelConstraint.CreateFunctionBox('My Macros', 'min(x,y)')\n";
		
		String expressionX 	= visit(ctx.expr(0));
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionX.trim()).floatValue();

			lString = lString + lMinBox + "A = rs_FindAnimationNode(" + lMinBox + ".AnimationNodeInGet(), 'a')\n";
			lString = lString + lMinBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionX.contains(" ")){
					
				String[] lControlAttribute = expressionX.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lMinBox + ", 'a')\n";
				
			}else{
				
				lString = lString + "rs_ConnectBox(" + expressionX + ", 'Result', " 	+ lMinBox + ", 'a')\n";
				
			}
	
		}
		
		String expressionY 	= visit(ctx.expr(1));
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionY.trim()).floatValue();
						
			lString = lString + lMinBox + "B = rs_FindAnimationNode(" + lMinBox + ".AnimationNodeInGet(), 'b')\n";
			lString = lString + lMinBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionY.contains(" ")){
					
				String[] lControlAttribute = expressionY.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lMinBox + ", 'b')\n";
				
			}else{
				
				lString = lString + "rs_ConnectBox(" + expressionY + ", 'Result', " 	+ lMinBox + ", 'b')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lMinBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lMinBox;
	
	}
	
//-------------------------------------------------------------------------------------------------
	
	public static int ModNum  = 0;
	
	//	'mod'	 	LPAREN expr COMMA expr RPAREN
	@Override
    public String visitModulus(MaxFloatExpressionParser.ModulusContext ctx){
		
		depth = depth + 1;
		
		ModNum = ModNum + 1;
		
		String lModBox = "lMod" + ModNum; 
		
		String lString = lModBox + "= lRelConstraint.CreateFunctionBox('Number', 'Modulo mod(a, b)')')\n";
		
		String expressionX 	= visit(ctx.expr(0));
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionX.trim()).floatValue();
			lString = lString + lModBox + "A = rs_FindAnimationNode(" + lModBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lModBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionX.contains(" ")){
					
				String[] lControlAttribute = expressionX.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lModBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expressionX + ", 'Result', " 	+ lModBox + ", 'a')\n";
				
			}
	
		}
		
		String expressionY 	= visit(ctx.expr(1));
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionY.trim()).floatValue();
			lString = lString + lModBox + "B = rs_FindAnimationNode(" + lModBox + ".AnimationNodeInGet(),'b')\n";
			lString = lString + lModBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionY.contains(" ")){
					
				String[] lControlAttribute = expressionY.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lModBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expressionY + ", 'Result', " 	+ lModBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lModBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lModBox;

	}
	
//-------------------------------------------------------------------------------------------------
/*	
	public static int PowNum  = 0;

	//	'pow'	 	LPAREN expr COMMA expr RPAREN
	@Override
    public String visitPower(MaxFloatExpressionParser.PowerContext ctx){
		
		depth = depth + 1;
		
		PowNum = PowNum + 1;
		
		String lPowBox = "lPow" + PowNum; 
		
		String lString = lPowBox + "= lRelConstraint.CreateFunctionBox('Number', 'Modulo mod(a, b)')')\n";
		
		String expressionX 	= visit(ctx.expr(0));
		
		BoxCount = BoxCount + 1; 
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionX.trim()).floatValue();
			lString = lString + lPowBox + "A = rs_FindAnimationNode(" + lPowBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lPowBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionX.contains(" ")){
					
				String[] lControlAttribute = expressionX.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lPowBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expressionX + ", 'Result', " 	+ lPowBox + ", 'a')\n";
				
			}

		}
		
		String expressionY 	= visit(ctx.expr(1));
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionY.trim()).floatValue();
			lString = lString + lPowBox + "B = rs_FindAnimationNode(" + lPowBox + ".AnimationNodeInGet(),'b')\n";
			lString = lString + lPowBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionY.contains(" ")){
					
				String[] lControlAttribute = expressionY.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lPowBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expressionY + ", 'Result', " 	+ lPowBox + ", 'a')\n";
				
			}
	
		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lPowBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lPowBox;
	
	}
*/	
//-------------------------------------------------------------------------------------------------
	
	public static int IfNum  = 0;
	
	//	'if' 		LPAREN relation COMMA expr COMMA expr RPAREN
	@Override
    public String visitIfStatement(MaxFloatExpressionParser.IfStatementContext ctx){
		
		depth = depth + 1;
		
		IfNum = IfNum + 1;
		
		String lIfBox = "lIf" + IfNum; 
		
		String lString = lIfBox + "= lRelConstraint.CreateFunctionBox('Number', 'IF Cond Then A Else B')\n";
		
		String relation 		= visit(ctx.relation());
		
		BoxCount = BoxCount + 1;
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(relation.trim()).floatValue();
			lString = lString + lIfBox + "Cond = rs_FindAnimationNode(" + lIfBox + ".AnimationNodeInGet(),'Cond')\n";
			lString = lString + lIfBox + "Cond.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (relation.contains(" ")){
					
				String[] lControlAttribute = relation.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lIfBox + ", 'Cond')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + relation + ", 'Result', " 	+ lIfBox + ", 'Cond')\n";
				
			}
	
		}
		
		String expressionTrue 	= visit(ctx.expr(0)); // True
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionTrue.trim()).floatValue();
			lString = lString + lIfBox + "A = rs_FindAnimationNode(" + lIfBox + ".AnimationNodeInGet(),'a')\n";
			lString = lString + lIfBox + "A.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionTrue.contains(" ")){
					
				String[] lControlAttribute = expressionTrue.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lIfBox + ", 'a')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expressionTrue + ", 'Result', " 	+ lIfBox + ", 'a')\n";
				
			}
	
		}
		
		String expressionFalse 	= visit(ctx.expr(1)); // False
		
		try
		{	
			
			float 	fExpression  = Float.valueOf(expressionFalse.trim()).floatValue();
			lString = lString + lIfBox + "B = rs_FindAnimationNode(" + lIfBox + ".AnimationNodeInGet(),'b')\n";
			lString = lString + lIfBox + "B.WriteData([" + fExpression + "])\n";	
				
		}catch(NumberFormatException nfe){
			
			if (expressionFalse.contains(" ")){
					
				String[] lControlAttribute = expressionFalse.split(" ", 2);
				
				lString = lString + "rs_ConnectBox(" + lControlAttribute[0] + ", '" + lControlAttribute[1] + "', " 	+ lIfBox + ", 'b')\n";
				
			}else{
	
				lString = lString + "rs_ConnectBox(" + expressionFalse + ", 'Result', " 	+ lIfBox + ", 'b')\n";
				
			}

		}
		
		lString = lString + "lRelConstraint.SetBoxPosition(" + lIfBox + " , " + depth * -400 + " , " + BoxCount * 200 + ")\n";
		
		depth = depth - 1;
		
		stringArray.add(lString);
		
		return lIfBox;

	}
	
//-------------------------------------------------------------------------------------------------
	
	// 	'(' expr ')'	
	@Override
    public String visitParentheses(MaxFloatExpressionParser.ParenthesesContext ctx){
		
		String expression 	= visit(ctx.expr());
		
		return ( expression );
	
	}

}