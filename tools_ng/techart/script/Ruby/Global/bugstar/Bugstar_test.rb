#

#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			Will read through the tracking docs and extract approved data and bugnumbers
#			WIll then updat ethe meta data file used by the game to display if something has been approved
#			Will also generate a email report sent to producrion
#			Is deiged to run as a automated task checking perforce for checked in file
#


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
#include RSG::Base::Logging
#include RSG::Base::Logging::Universal

require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'
require 'RSG.Metadata'
require 'RSG.Model.Bugstar.dll'

# Core
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::SourceControl::Perforce
include RSG::Metadata
include RSG::Model::Bugstar
include RSG::Model::Bugstar::Organisation


require 'mscorlib'
require 'System.Core'
require 'System.Xml'
require 'win32ole'
require 'rexml/document'
include REXML


include System::Collections::Generic
include System::IO
include System::Diagnostics
include System::Xml
include System::Runtime::InteropServices
include System::Net
include System::Net::Sockets
include System::Net::Mail



require 'pipeline/os/options'
#require 'pipeline/util/email'
include Pipeline

DEBUG = true
#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [ 
    LongOption::new( 'frequency', LongOption::ArgType.Required, 'f', 
        'frequency to check perforce for submits(required).' )
        ]
        
        
#EMAIL = 'mark.harrison-ball@rockstargames.com'
EMAIL = 'AutoApprovedCutsceneUpdater@rockstargames.com'
AUTHOR = ['mark.harrison-ball@rockstargames.com']
SERVER = 'rsgldnexg1.rockstar.t2.corp'



# Simple bug wrapper aroung around shit
class BugstarUtils
    def initialize(log = nil)
      @bugstarConfig = ConfigFactory.CreateBugstarConfig()            
      @bugstar = BugstarConnection::new()    
      @g_log = log 
      @project = nil
      @g_log.Message("tetsing")

      
    end
    
    # login
    def login(username = nil, password = nil)
      @g_log.Message("CUNTS")
      if username != nil and password != nil
        @bugstar.Login( 'mharrison-ball', 'Michele44', @bugstarConfig.UserDomain )
        
        @project = Project.GetProjectById( @bugstar, @bugstarConfig.ProjectId )
        
        if @g_log
          @g_log.Message( "Current Project: #{@project.Name}" )
        
        end
        
        #bug1 = Bug.GetBugById( @project, 883449 )

      end      
    end
    
    # getbug
    
    # create bug
    def createbug(summary = "",description = "", tag = nil, developerId = nil, testerId = nil )
        # Create Bug test.
        if developerId and testerId
          bugbuilder = BugBuilder::new(@project )
          bugbuilder.summary = summary
          bugbuilder.description = description
          bugbuilder.developerId = developerId #user.Id
          bugbuilder.testerId = testerId #user.Id
          #bugbuilder.Tags.Add( 'Test' )

          #Create Bug
          bug = bugbuilder.ToBug( @bugstar )
        end
    
    end
    
    def getbug(bugnumber = nil)
      if bugnumber
        bug1 = Bug.GetBugById( @project, bugnumber )
        
        if bug1 and @g_log
        # Output bug info
          @g_log.Message( "Bug:" ) 
          @g_log.Message( "\tId:          #{bug1.Id}" )
          @g_log.Message( "\tSummary:     #{bug1.Summary}" )
          @g_log.Message( "\tDescription: #{bug1.Description}" )
          @g_log.Message( "\tPriority:    #{bug1.Priority}" )
          @g_log.Message( "\tComments: " )
          bug1.Comments.each do |comment|
              @g_Log.Message( "\t\t#{comment.Text}" )
          end
        
        end
        
        
      end
    end

     

end


=begin
#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    g_Options = OS::Options::new( OPTIONS )
    LogFactory.CreateApplicationConsoleLogTarget( )
        g_Log = LogFactory.ApplicationLog
        
    

    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end
        
        bugstar = BugstarUtils.new(g_Log)
        
        bugstar.login('mharrison-ball', 'Michele44')
        
        bugstar.getbug(883449)

       
        # Perforce initialisation. 
        g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   
        @email = g_Config.EmailAddress	
        @server= g_Config.studios.this_studio.exchange_server # FUCKING MAKES NOT FUCKING DIFFERNCE!@
        @server= 'rsgldnexg1.rockstar.t2.corp'
		
        branch = g_Config.Project.DefaultBranch
        
        bugstarConfig = ConfigFactory.CreateBugstarConfig()            
        bugstar = BugstarConnection::new()

        bugstar.Login( 'mharrison-ball', 'Michele44', bugstarConfig.UserDomain )
        #bugstar.Login( 'mapautoexport', 'rockstat1A', bugstarConfig.ExternalDomain )
        
        # Output the available Bugstar Projects.
        projects = Project.GetProjects( bugstar )
        projects.each do |project|
            g_Log.Message( "Project: #{project.Name}" )
        end
        
        # Get current Project.
        project = Project.GetProjectById( bugstar, bugstarConfig.ProjectId )
        g_Log.Message( "Current Project: #{project.Name}" )
        
        # Get current User.
        user = project.CurrentUser
        g_Log.Message( "Current User:" )
        g_Log.Message( "\tId:        #{user.Id}" )
        g_Log.Message( "\tUsername:  #{user.UserName}" )
        g_Log.Message( "\tJob Title: #{user.JobTitle}" )
        g_Log.Message( "\tEmail:     #{user.Email}" )
        
        # ReviewerId
        # State
        # Priority
        # Mission_id
        # DueDate
        # CcList
        # Tags
        
        # Create Bug test.
        bugbuilder = BugBuilder::new( project )
        bugbuilder.Summary = 'Test bug summary'
        bugbuilder.Description = 'Test bug description'
        bugbuilder.DeveloperId = user.Id
        bugbuilder.TesterId = user.Id
        bugbuilder.Tags.Add( 'Test' )
        puts(bugbuilder.methods)
        #bugbuilder.Attachments.Add( attachment.Id )
        #Create Bug
        #bug = bugbuilder.ToBug( bugstar )
        
        # Display new bug info.
        g_Log.Message( "New Bug:" ) 
        g_Log.Message( "\tId:          #{bug.Id}" )
        g_Log.Message( "\tSummary:     #{bug.Summary}" )
        g_Log.Message( "\tDescription: #{bug.Description}" )
        g_Log.Message( "\tPriority:    #{bug.Priority}" )   
        #g_Log.Message( "\tAttachments: #{bug.AttachmentList.Count()}" ) 
      
        # Get Bug
        
         # Get bug by ID
        bug1 = Bug.GetBugById( project, 883449 )
        # Display bug info.
        g_Log.Message( "Bug:" ) 
        g_Log.Message( "\tId:          #{bug1.Id}" )
        g_Log.Message( "\tSummary:     #{bug1.Summary}" )
        g_Log.Message( "\tDescription: #{bug1.Description}" )
        g_Log.Message( "\tPriority:    #{bug1.Priority}" )
        g_Log.Message( "\tComments: " )
        bug1.Comments.each do |comment|
            g_Log.Message( "\t\t#{comment.Text}" )
        end


        
    rescue => msg  
        puts(msg)
        @g_Log.error("Crash Deteted ")

        
    end


end

=end