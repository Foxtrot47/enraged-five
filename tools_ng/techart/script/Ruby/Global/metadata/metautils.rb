#
# Pargen Parser
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 May 2013 (AP3)
# Purpose:
# 			Used to read a PSC file and will fail most likely :(
#           Added  back up xml parsing


# CORE
require 'RSG.Metadata'
include RSG::Metadata


# Reference our metaData Types
STRUCT_TUNABLE  = RSG::Metadata::Data::StructureTunable
POINTER_TUNABLE = RSG::Metadata::Data::PointerTunable
ARRAY_TUNABLE   = RSG::Metadata::Data::ArrayTunable
FLOAT_TUNABLE   = RSG::Metadata::Data::FloatTunable
FLOAT16_TUNABLE = RSG::Metadata::Data::Float16Tunable
COLOR32_TUNABLE = RSG::Metadata::Data::Color32Tunable
STRING_TUNABLE  = RSG::Metadata::Data::StringTunable
UINT8_TUNABLE   = RSG::Metadata::Data::U8Tunable
SINT8_TUNABLE   = RSG::Metadata::Data::S8Tunable
UINT32_TUNABLE  = RSG::Metadata::Data::U32Tunable
SINT32_TUNABLE  = RSG::Metadata::Data::S32Tunable
UINT16_TUNABLE  = RSG::Metadata::Data::U16Tunable
SINT16_TUNABLE  = RSG::Metadata::Data::S16Tunable
SHORT_TUNABLE   = RSG::Metadata::Data::ShortTunable
INT_TUNABLE     = RSG::Metadata::Data::IntTunable
BITSET_TUNABLE  = RSG::Metadata::Data::BitsetTunable
BOOL_TUNABLE    = RSG::Metadata::Data::BoolTunable
VECTOR2_TUNABLE = RSG::Metadata::Data::Vector2Tunable
VECTOR3_TUNABLE = RSG::Metadata::Data::Vector3Tunable
VECTOR4_TUNABLE = RSG::Metadata::Data::Vector4Tunable
ENUM_TUNABLE    = RSG::Metadata::Data::EnumTunable


# Simple Wrapper around our Metadata Object
class MetaFile
    # Constructors
    #attr_reader :Root
    #attr_writer :Root
    
    attr_reader :Root
    attr_writer :Root
    
    def initialize( sourceNETMetaFile )
      @_sourceNETMetaFile = sourceNETMetaFile
      @_filePath = sourceNETMetaFile.Filename
      rootTunable = sourceNETMetaFile.Members.get_Item( sourceNETMetaFile.Definition.DataType )   
      #@Root = MetaCreate( rootTunable )
    end
    
    # Save Our MetaData
    def save()
      if @_filePath != "" then        
        @_sourceNETMetaFile.Serialise(@_filePath) 
      else
        return nil
      end
    end
    
    # Set File Path
    def SetFilePath( newFilePath )
      @_filePath = newFilePath      
    end
    
end

class MetaData
    
    def initialize( branch = nil)
      definationsPath = "#{branch.metadata}/definitions/"
      structspath = [definationsPath]
      structPathNet = System::Array[System::String].new(structspath)
      @structDict = RSG::Metadata::Model::StructureDictionary.new 
      @structDict.load(branch, structPathNet)
      
      @tunableUtils = RSG::Metadata::Util::Tunable
      @memberUtils = RSG::Metadata::Util::Member

    end
    
    def CreateMetaFile( structureDefinitionName )
      targetNETMetaFile = nil
      if @structDict.ContainsKey( structureDefinitionName ) then
        targetNETMetaFile = RSG::Metadata::Model::MetaFile.new( @structDict.get_Item( structureDefinitionName ) ) 
      end
      return targetNETMetaFile# MetaFile.new( targetNETMetaFile ) 
    end
    
    def ParseMetaFile( metaFilePath )
        #Create a .NET MetaFile using the _structureDefinitionDictionary and a path to an existing MetaFilePath
        targetNETMetaFile = RSG::Metadata::Model::MetaFile.new( metaFilePath , @structDict  )
        return targetNETMetaFile
    end
    
    def FindFirstStructureNamed( metaData, structureDefinitionName )      
      @tunableUtils.FindFirstStuctureNamed(structureDefinitionName, metaData.Members)
    end
    
    def CreateNewStructTunableFor( metaData, structureDefinitionName )
      member  = @memberUtils.FindStructMemberByName(metaData.Definition.Members, structureDefinitionName)
      structTunable = RSG::Metadata::Data::StructureTunable.new(member, nil)
      structTunable
    end
    
    # Recursive, return value from name
    def getName(metaData, nameVal, ref=[] )
      if not metaData.respond_to?(:each)
        if metaData.name == nameVal
          ref.push(metaData.value)
          return ref
        end
      else
          metaData.each do | val |
            getName(val.value, nameVal, ref)
          end
      end
      ref
    end
  
end




