# File:: email.rb
# Description:: Simple .Net Email sending functions.
#
# Author:: Mark Harrison-Ball <mark.harrison-ball@rockstargames.com>
# Date:: 28 May 2013
#

include System::Net::Mail
include System::IO

# Set Img src to Content ID path
def setContentID(contentID)
	# cid:'#{src}'
	return "cid:#{contentID}"	
end

# Generate s content ID
def getContentID(path)
	return System::IO::Path.GetFileName(path).Replace(".", "")
end

# Update HTML text to refernce Image COntetns and Embed our images
# i.e
# <img src='x:/temp/HAO_MCS_1_rotation.png' alt='logo' />
def collectEmbedImages(htmlContent)
	# lets split our content by line and scan each line
	cidlist = Array.new()
	
	newhtmlContent = htmlContent.gsub(/(.+img\ssrc=).(.+).(\salt=.+)/){
		|m|	
		# Generate a COntetn ID
		contID = getContentID($2)
		cidlist << {:Path => $2, :ID => contID}
		# Replace image Path with our CiontentID
		m = "#{$1}#{setContentID(contID)}#{$3}"
		
	}
	return cidlist, newhtmlContent
	
end

# Send our Email
def send_email(from, to_list, subject, message, localhost = "smtp.rockstar.t2.corp")
	begin
		mail = System::Net::Mail::MailMessage.new()
		@FromAddress = System::Net::Mail::MailAddress.new(from)
		mail.From = @FromAddress
		

		if to_list.is_a?(Array) then
			to_list.each do |addss|
				mail.To.Add(addss.to_s)

			end
		else
			mail.To.Add(to_list)
		end
		# Check the html content to see if we have any images we wh to embed
		contentImgList, newhtmlContent = collectEmbedImages(message)	
		
		# Create our Html Message
		htmlView = System::Net::Mail::AlternateView.CreateAlternateViewFromString(newhtmlContent, nil, "text/html")
		
		# Add any embedded images
		if contentImgList then		
			contentImgList.each do |img|
				hmlImage = System::Net::Mail::LinkedResource.new(img[:Path])
				hmlImage.ContentId = img[:ID] # filename ID
				htmlView.LinkedResources.Add(hmlImage)
			end
		end
		
		# Build our Email
		mail.Subject = subject	
		mail.AlternateViews.Add(htmlView)
		
		# Create email Client
		smtp = System::Net::Mail::SmtpClient.new(localhost)
	
	
		#puts 'test email'
		smtp.Send(mail)
	
	rescue => e  
		raise "Exception occured: #{e.message} in #{e.backtrace} "
	end
	
	
	
end
=begin
def send_email(from, to_list, subject, message)

msgstr = <<END_OF_MESSAGE
From: <#{from}>
To: <#{to_list.is_a?(Array) ? to_list.join(', ') : to_list}>
MIME-Version: 1.0
Content-type: text/html
Subject: #{subject}

#{message}
END_OF_MESSAGE


	begin
	Net::SMTP.start(@localhost, 25) do |smtp|
	  smtp.send_message msgstr,
						from,
						to_list
	end
	rescue => e  
		raise "Exception occured: #{e} "
	end
  
end
=end




  



