#
#		GENERIC CLASS
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			Clip Parser using Managed Rage
#

#-----------------------------------------------------------------------------
# Uses
# NOTES
# CLIP type now supports XML or Normal
#
#-----------------------------------------------------------------------------

path = File.expand_path $0
path = File.dirname(path) 

require 'System.Core'
include System

require 'RSG.ManagedRage.dll'
include RSG::ManagedRage::ClipAnimation

DEFAULTCLIP_TYPE = MClip::ClipType.Normal

class Clip

    def initialize( log = nil )
      MClip.Init()
    end
    
    def getProperty(propertyString, clip)
      return parse(propertyString, clip)
    end
    
    def parse(propertyString, clip)
      
      @rageClip = MClip::new(DEFAULTCLIP_TYPE)
      fbx_property = nil
      begin
        if(@rageClip.Load(clip) == true) then
          fbx_property = @rageClip.FindProperty(propertyString)
          @rageClip.Dispose()          
        end
      rescue => e
        @log.Error("Unable to load Clip:#{clip}")
      end
      fbx_property
    end
end




