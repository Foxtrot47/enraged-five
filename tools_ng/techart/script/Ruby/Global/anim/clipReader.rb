#
#		GENERIC CLASS
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			Animation reader
#


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

include System
include System::IO
include System::Diagnostics

path = File.expand_path $0
path = File.dirname(path)
require "#{path}/../../../../../lib/pipeline/util/string"


class ClipReader
	def initialize(config, log)		
		@config = config
		@log = log
		@log.message('Clip Reader init')
		@extractPath = System::IO::Path.GetTempPath()		

	end	
	
	def parse(filename)
		@log.message("Reading Clip #{filename}")
		clipObj = Array.new()
		datatype = -1
		clipBlockList = ClipBlockTag.new() # Yeah I know

		File::open( filename, 'r' ) do |fp|
			fp.each do |line|
				# Skip empty lines
				next if 0 == line.size
				tokens = line.split(' ')
				
				if line.starts_with( 'propertyname' ) then	
					if clipBlockList.name != nil then						
						clipObj << clipBlockList
						clipBlockList = ClipBlockTag.new()
					end
				end

				if tokens[0] == 'propertyname' then
					clipBlockList.name = tokens[1]
				elsif tokens[0] == 'tagstart' then
					clipBlockList.tagstart = tokens[1]
				elsif tokens[0] == 'tagend' then
					clipBlockList.tagend = tokens[1]
				end
				
			end
		end
		#puts animObj[0].include?('cameraTranslation')
		#puts animObj[0].trackname
		
		return clipObj
		
	end
end
private
# Describe a sible block tag Info
class ClipBlockTag
	attr_writer :name
	attr_reader :name
	attr_writer :tagstart
	attr_reader :tagstart
	attr_writer :tagend
	attr_reader :tagend
	
	
	def initialize()
		@name = nil
		@Data = Array.new()
	end
	
	def add(property)
		@Data.push(property)		
	end
end	
	