#
#		GENERIC CLASS
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			Animation dump
#


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'System.Core'
include System
include System::IO
include System::Diagnostics

class ClipDump
	def initialize(config, log)
		@config = config
		@log = log
		@log.message('Clip Dump init')
		@extractPath = System::IO::Path.GetTempPath()		

	end	
	
	def dump(filename)
		commandline = "$(toolsbin)/anim/clipdump.exe"
		cmd_exe = @config.Environment.Subst(commandline)

		args = cmd_exe
		args += " -clip "
		args += filename
		args += " > #{filename}.txt" # Write to file

		@log.message(args)
		
		exit_code = system args

		if exit_code then
			@log.Message("Clip Extracted ok")
		else
			@log.Error("Errors extracting Clip, see log")
		end
	

	end
end