#

#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			Animation compare
#			Compares anim data to see what has chnaged
#


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

include System
include System::IO
include System::Diagnostics



class AnimCompare
	def initialize(config, log)
		puts('Anim Compare init')
		@config = config
		@log = log
	

	end	
	
	def compare(animA, animB)
		animObj = []
		datatype = -1
		animTrack = AnimTrack.new()

		File::open( filename, 'r' ) do |fp|
			fp.each do |line|
				# Skip empty lines
				next if 0 == line.size
				tokens = line.split(' ')
				
				if line.starts_with( 'Track' ) then	
					if animTrack.trackname != nil then
						animObj << animTrack
						animTrack = AnimTrack.new()
					end						
					datatype = tokens[tokens.size-1]
					animTrack.trackname = datatype
					
				end
				if line.starts_with( 'Value' ) then 
					animInfo = []
					(1..tokens.size-1).each { |i|
						animInfo.push(tokens[i])					
					}
					#puts("#{animInfo}")
					animTrack.add(animInfo)
				end
			end
		end
		puts animObj[0].trackname
		puts animObj[0].trackData.Count
	end
end

