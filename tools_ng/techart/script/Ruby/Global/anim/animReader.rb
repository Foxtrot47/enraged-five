#
#		GENERIC CLASS
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			Animation reader
#


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

include System
include System::IO
include System::Diagnostics

# need to sort out this!!!
path = File.expand_path $0
path = File.dirname(path)
require "#{path}/../../../../../lib/pipeline/util/string"


class AnimReader
	def initialize(config, log)
		@config = config
		@log = log
		@log.message('Anim Reader init')
		@extractPath = System::IO::Path.GetTempPath()		

	end	
	
	def parse(filename)
		@log.message("Reading Anim #{filename}")
		animObj = Array.new()
		datatype = -1
		animTrack = AnimTrack.new()
		
		File::open( filename, 'r' ) do |fp|
			fp.each do |line|
				
				# Skip empty lines
				next if 0 == line.size
				tokens = line.split(' ')
				
				if line.starts_with( 'Track' ) then	
					if animTrack.trackname != nil then
						
						animObj << animTrack
						animTrack = AnimTrack.new()
					end						
					datatype = tokens[tokens.size-1]
					animTrack.trackname = datatype
					#puts datatype
					
				end
				if line.starts_with( 'Value' ) then 
					animInfo = []
					
					# Single values					
					if tokens.size == 2 then
						animInfo = (tokens[1].to_f)
					
					# Multiple array
					else
						(1..tokens.size-1).each { |i|
							animInfo.push(tokens[i].to_f)
						}
						
					# Convert Rotation if Quantertion
					if animTrack.trackname == 'cameraRotation' and animInfo.size == 4 then
						
					end
					
					#puts("#{animInfo}")
					end
					#print animInfo
					animTrack.add(animInfo)
				end
			end
		end
		#puts animObj[0].include?('cameraTranslation')
		#puts animObj[0].trackname
		#print animObj[2].trackData
		return animObj
		
	end
end
private
	class AnimTrack
		attr_writer :trackname
		attr_reader :trackname
		attr_writer :trackData
		attr_reader :trackData
		
		def initialize()
			@trackname = nil
			@trackData = Array.new()
		end
		
		def add(track)
			@trackData.push(track)		
		end
	end