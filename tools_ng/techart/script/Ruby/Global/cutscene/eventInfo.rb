#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 15 September 2013 (AP3)
# Purpose:
#			~ Generic object to store cutscene Events


#

path = File.expand_path $0
path = File.dirname(path)

# Base Class
class CutsceneEventArgs
  attr_reader :sName  
  def initialize()   
  end
  
end

class ObjectVariationEventArgs < CutsceneEventArgs
  attr_reader :iObjectId
  attr_reader :iComponent
  attr_reader :iDrawable
  attr_reader :iTexture
  
  def initialize()
  end
end

class SubtitleEventArgs < CutsceneEventArgs
  def initialize()
  end 
end



# pCutsceneEventList
class CutfObjectIdEvent
  attr_reader :fTime
  attr_reader :iEventId
  attr_reader :iEventArgsIndex
  attr_reader :iObjectId  
  
  def initialize( metadataUtils, metaDataObject )
    @fTime = nil
    @iEventId = nil
    @iEventArgsIndex = nil
    @iObjectId = nil
    parse( metadataUtils, metaDataObject )
  end
  
  def parse( metadataUtils, metaDataObject )
    if metaDataObject.class == STRUCT_TUNABLE
      metaDataObject.each do |object|
        #puts object.value.name
        if object.value.name == 'fTime'
          @fTime = object.value.value
        elsif object.value.name == 'iEventId'
          @iEventId = object.value.value
        elsif object.value.name == 'iEventArgsIndex'
          @iEventArgsIndex = object.value.value
        elsif object.value.name == 'iObjectId'
          @iObjectId = object.value.value
        end
      end
    end    
  end
end
