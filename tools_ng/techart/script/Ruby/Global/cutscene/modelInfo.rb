#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 15 September 2013 (AP3)
# Purpose:
#			~ Generic object to store Model Info


#

path = File.expand_path $0
path = File.dirname(path) 

# Model Types
#require "#{path}/../../Global/cutscene/cutObject"

class ModelInfo
  attr_reader :name
  attr_reader :streamingName 
  attr_reader :handle
  attr_reader :type 
  attr_reader :objectID

  def initialize(metadataUtils, metaDataObject, type) 
    @type = type
    @streamingName = ''
    @handle = ''
    @name = ''
    @objectID = nil
    parse( metadataUtils, metaDataObject, type )
  end
  
  def parse(metadataUtils, metaDataObject, type)
  
    if metaDataObject.class == STRUCT_TUNABLE
      metaDataObject.each do |object|
        if object.value.name == 'cName'
          @name = resolveCName(object.value.value)
        elsif object.value.name == 'iObjectId'
          @objectID = object.value.value
        elsif object.value.name == 'StreamingName'
          @streamingName = object.value.value
        elsif object.value.name == 'cHandle'
          @handle = object.value.value
        end
      end
    end    
  end  
  
  # split our string with the ^ and : characters and return the 1st part
  def resolveCName(stringCName)
    resolvedName = stringCName.split(/(\w+)(\W+)/)
    return resolvedName[1]
  end
end

class AudioInfo < ModelInfo
  attr_reader :offset
  
  def initialize(metadataUtils, metaDataObject, type)
    @offset = 0    
    super( metadataUtils, metaDataObject, type)
    init_Audio( metadataUtils, metaDataObject )
  end
  
  def init_Audio(metadataUtils, metaDataObject)
    if metaDataObject.class == STRUCT_TUNABLE
      metaDataObject.each do |object| 
        if object.value.name == 'fOffset'
          @offset = object.value.value
        end
      end
    end
  end
end

class BlockingInfo < ModelInfo
  attr_reader :Height
  
  def initialize(metadataUtils, metaDataObject, type)
    @height = 0    
    super( metadataUtils, metaDataObject, type)
    init_Block( metadataUtils, metaDataObject )
  end
  
  def init_Block(metadataUtils, metaDataObject)
    if metaDataObject.class == STRUCT_TUNABLE
      metaDataObject.each do |object| 
        if object.value.name == 'fHeight'
          @height = object.value.value
        end
      end
    end
  end
end