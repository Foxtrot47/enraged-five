#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 May 2013 (AP3)
# Purpose:
# 			CUtscene Flag Parser


include System
include System::IO


CUTSCENE_FLAGS = [
"CUTSCENE_FADE_IN_GAME_FLAG",
"CUTSCENE_FADE_OUT_GAME_FLAG",       
"CUTSCENE_FADE_IN_FLAG",                 
"CUTSCENE_FADE_OUT_FLAG",                
"CUTSCENE_SHORT_FADE_OUT_FLAG",           
"CUTSCENE_LONG_FADE_OUT_FLAG",           
"CUTSCENE_FADE_BETWEEN_SECTIONS_FLAG", 
# Lights
"CUTSCENE_NO_AMBIENT_LIGHTS_FLAG",        # Set when the ambient lights should be turned off for the duration of the cut scene
"CUTSCENE_NO_VEHICLE_LIGHTS_FLAG",        # Set when the vehicle lights should be turned off for the duration of the cut scene

# Audio
"CUTSCENE_USE_ONE_AUDIO_FLAG",            # Set when planning to concatenate cutscenes and there will be only 1 audio track for the entire scene
"CUTSCENE_MUTE_MUSIC_PLAYER_FLAG",        # Set when game music should be muted for the entire cut scene
"CUTSCENE_LEAK_RADIO_FLAG",               # Set when it is ok for the game music (from the radio) can play through the cut scene

# Misc editor flags
"CUTSCENE_TRANSLATE_BONE_IDS_FLAG",       # Set when the bone ids need to be translated using the spec file during export  
"CUTSCENE_INTERP_CAMERA_FLAG",            # Set when we should interpolate between camera cuts during export

# Sectioning flags
"CUTSCENE_IS_SECTIONED_FLAG",             # Indicates when the data contained in the file (and the animations) have been sectioned
"CUTSCENE_SECTION_BY_CAMERA_CUTS_FLAG",   # Set when we should segment based on the camera cuts
"CUTSCENE_SECTION_BY_DURATION_FLAG",      # Set when we should segment based on a specified duration for each section
"CUTSCENE_SECTION_BY_SPLIT_FLAG",         # Set when we should segment on the camera's SectionSplit attribute keys

"CUTSCENE_USE_PARENT_SCALE_FLAG",         # Set when exported models should factor in their parent node's scale

"CUTSCENE_USE_ONE_SCENE_ORIENTATION_FLAG",# Set when planning to concatenate cutscenes and we should use only the first scene's orientation

"CUTSCENE_ENABLE_DEPTH_OF_FIELD_FLAG",    # Set when the depth of field camera settings are ready for prime time

"CUTSCENE_STREAM_PROCESSED",

"CUTSCENE_USE_STORY_MODE_FLAG",

"CUTSCENE_USE_IN_GAME_DOF_START_FLAG",
"CUTSCENE_USE_IN_GAME_DOF_END_FLAG",
"CUTSCENE_USE_CATCHUP_CAMERA_FLAG",
"CUTSCENE_USE_BLENDOUT_CAMERA_FLAG",
"CUTSCENE_PART_FLAG",
"CUTSCENE_INTERNAL_CONCAT_FLAG",
"CUTSCENE_EXTERNAL_CONCAT_FLAG",
"CUTSCENE_USE_AUDIO_EVENTS_CONCAT_FLAG"
       
]

class CutFlagsObject
	attr_reader :uiFlags
	attr_reader :flagList
	
	
	def initialize()
		@flagList = Array.new()
	end
	
	def isFlagSet(flag)
		uiFlagCopy = @uiFlags #System::UInt32.new(0)
		#puts uiFlagCopy, flag
		#p = uiFlagCopy
		return (uiFlagCopy & (1 << (flag & 31))) != 0
		#puts (p[flag >> 5] & (1 << (flag & 31))) != 0;
	end
	
	def readFlags(flags)
		@flagList = Array.new()
		@uiFlags = System::UInt32.Parse(flags.to_s)
		
		(1..CUTSCENE_FLAGS.count).each { |n|
			if isFlagSet(n) then
				if CUTSCENE_FLAGS[n] != nil
					@flagList << CUTSCENE_FLAGS[n]
				else
					puts("Flag Index does not exist #{n}")
				end
			end
		}
		return @flagList	
	end
	
	def hasFlag(flagstring)
		return @flagList.include?(flagstring)			
	end
	
end