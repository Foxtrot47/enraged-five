#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 15 September 2013 (AP3)
# Purpose:
#			~ Base Object class


#

path = File.expand_path $0
path = File.dirname(path) 

# Basic paramaters Types


class CutObject
  attr_reader :name
  attr_reader :streamingName 
  attr_reader :handle
  attr_reader :type 

  def initialize(metadataUtils, metaDataObject, type) 
    @type = type
    @streamingName = ''
    @handle = ''
    @name = ''
    parse( metadataUtils, metaDataObject, type )
  end
  
  def parse(metadataUtils, metaDataObject, type)
  
    if metaDataObject.class == STRUCT_TUNABLE
      metaDataObject.each do |object|        
        if object.value.name == 'cName'
          @name = object.value.value
        elsif object.value.name == 'StreamingName'
          @streamingName = object.value.value
        elsif object.value.name == 'cHandle'
          @handle = object.value.value
        end
      end
    end
    
          
  end
  
end