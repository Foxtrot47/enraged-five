puts ""
puts "loading simple debug Module"
puts "Commands:"
puts "debugPrint(object)"
puts ""


public
def debugPrint(object)
  _debugPrintInstvar(object)
end

private
  # TODO: Add recursive
  def _debugPrintInstvar(object)
    # Awesome debug Variable info, will print out all the 
    # properties and their values of an object passed in.
    object.instance_variables.map do |var|
      puts "[Debug]: #{ [var, object.instance_variable_get(var)].join(":") }"
    end
  end
