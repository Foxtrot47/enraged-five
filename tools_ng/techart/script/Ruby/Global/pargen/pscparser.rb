#
# Pargen Parser
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 May 2013 (AP3)
# Purpose:
# 			Used to read a PSC file and will fail most likely :(
#           Added  back up xml parsing


# CORE
require 'RSG.Metadata'
include RSG::Metadata

#TODO:
#REmove hardcoded path

# SYSTEM
include System::Net
class Pargen

	def initialize(branch = nil)
	
		definationsPath = 'x:/gta5/assets/metadata/definitions/'
		structspath = [definationsPath]
        structPathNet = System::Array[System::String].new(structspath)
        @structDict = RSG::Metadata::Model::StructureDictionary.new 
        @structDict.load(branch, structPathNet)
		
		@TunableUtils = RSG::Metadata::Util::Tunable
	    @memberUtils = RSG::Metadata::Util::Member
		@metaLoad = nil
	end
	
	def parse(file)
		@metaLoad = RSG::Metadata::Model::MetaFile.new( file, @structDict, true)
	end
	
	def find(entry)
		list = @TunableUtils.FindFirstStuctureNamed(entry, @metaLoad.Members )		
	end
	
	def findfirst(entry)
		list = @TunableUtils.FindFirstStuctureNamed(entry, @metaLoad.Members )
		list[0]
		
	end
	
	def findAll(entry)
		list = @TunableUtils.FindAllStucturesNamed(entry, @metaLoad.Members )
		list
	
	end
	
	def findmembers(entry)
		FindAllStucturesNamed structName Metadata.Members
	
		member = @memberUtils.FindStructMemberByName(@metaLoad.Definition.Members, entry)
		#puts(@memberUtils)
		#structTunable = RSG::Metadata::Data::StructureTunable.new(member, nil)
		
	end

end