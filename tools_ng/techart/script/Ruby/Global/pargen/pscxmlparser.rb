require 'System.Xml'
require 'System.Xml.Linq'
include System::Xml
include System::Xml::Linq
using_clr_extensions System::Linq
using_clr_extensions System::Xml::Linq
using_clr_extensions System::Xml::XPath


	
class Xmlpargen	
	attr_reader :file
	attr_reader :xmldoc
	attr_reader :autoclips


	def initialize()
		puts('XML Pargen started')		
		
	end	
	
	def parse(xmlfile)
		@xmldoc = XmlDocument.new
		@xmldoc = XDocument::Load( xmlfile )

		@autoclips = []
		
		puts('Loaded XML')
		
		# Autoclips
		@xmldoc.Root.XPathSelectElements( 'pCutsceneEventArgsList/Item' ).each {| elm |

			if elm.Attribute( XName::Get( "type" ) ).Value == "rage__cutfCameraCutEventArgs" then
				@autoclips << (elm.element( XName::Get( "cName" ) ).Value)
			end

		}

	end

	
	# This is too specific
	def returnAutoClips()
		return @autoclips
	end

end
