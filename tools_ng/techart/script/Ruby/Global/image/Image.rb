#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			Image utils
#
require 'System.Core'
require 'System.Drawing'

class Image
	# Constructors
	attr_reader :path
	attr_reader :source
	attr_writer :source
	
	# Basic Image
	# Parameters:
    #   Width, Height
	def initialize( x, y, path = 'x:\temp.png' )
		@source = System::Drawing::Bitmap.new( x , y )
		@path = path
		#return image		
	end
	
	
	def save()
		@source.save(@path)
	end
	
	
end