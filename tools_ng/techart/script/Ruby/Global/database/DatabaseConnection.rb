path = File.expand_path $0
path = File.dirname(path)


require 'system.data'


class DatabaseConnection

  #attr_reader :connectionString
  #attr_writer :connectionString

  def initialize(glog = nil)
    @log = glog 
    @source = nil
    @server = nil
    @connection = nil
    
  end
  
  def close()
    closeDB()
  end
  
  def connect(server, source)
    if source
      @source_server = server
      @source_database = source
      connectDB()    
    end
  end
  
  def log(_message)
    if @log then
      @log.message(_message)
    else
      puts _message
    end
  end

  def source_connection_string(server, database)
    "Data Source=#{server};Initial Catalog=#{database};Integrated Security=SSPI;"
  end
  
  def connectDB()
    connection_string = source_connection_string(@source_server, @source_database)
    @connection = System::Data::SqlClient::SqlConnection.new(connection_string)
    
    log("Opening SQL Server database connection...")
    @connection.Open
    log(@connection.ConnectionString)
    

  end
  
  def closeDB()
    if  @connection
      @connection.Close 
      log("Closing SQL Server database connection...")
    end
  
  end
  
  def read_data(query)
    command = System::Data::SqlClient::SqlCommand.new(query, @connection)
    
    #log.message("Running SQL Server Command...#{query}")
   
    

    reader = command.ExecuteReader
    #log.message("SQL command executed.")

    records_read = 0
    begin
      # Get the number of fields in our record from the reader
      fields = []
      for i in 1..(reader.FieldCount)
        fields.push reader.GetName(i-1).to_s
      end

      
      
      while reader.Read do
        record = {}
 
        # get filed name and assign to a new dictionary
        fields.each_with_index do |field, i|
          record[field] = reader[i] unless reader.IsDBNull(i)
        end
        
        #yield record
        records_read += 1
        
      end

    ensure

      if (reader != nil)
        reader.Close
      end

    end

    return record
  
  end
  
  
  def read_data_con(source_server,source_database,query)
    connection_string = source_connection_string(source_server, source_database)
    connection = System::Data::SqlClient::SqlConnection.new(connection_string)
  end
    
    

end

# Example SQL USAGE

# sourceDB = "gradingStats"
# server = "nycw-mhb-sql"
#dBconnection.connect(server, sourceDB)

# Add New Entry
#query = "INSERT INTO grading ([Scene Name])
#          VALUES ('Cheese')"

# UPdate Existing Entry
#query = "UPDATE grading SET [Scene Name] ='Test Scene' 
#          WHERE Frames = 4"


# Process Querry
#dBconnection.read_data(server, sourceDB, query)
          

