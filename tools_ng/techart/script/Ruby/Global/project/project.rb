#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 24 January 2014 (AP3)
# Purpose:
#			Simple function to return a global Project ID for use with a Database
#     Need to Support Branch Support later

def getProjectID(projectName)
  projectID = 1
  if projectName == 'GTA5' 
    projectID = 1
  elsif projectName == 'RDR3' 
    projectID = 2
  elsif projectName == 'TEST' 
    projectID = 3
  end
  return projectID
end