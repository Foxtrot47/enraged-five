#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 May 2013 (AP3)
# Purpose:
# 			Used to generate a html report
# 			contains a list of functions to generate tablles and shit and return shit back


class TemplateStyles
	attr_reader :default
	attr_writer :default
	attr_writer :basic
	attr_reader :basic
	attr_reader :warning
	attr_reader :invert
	attr_reader :header
	attr_reader :bold
	attr_reader :defaultBeige
	attr_reader :invertBeige
	attr_reader :boldBeige

	
	
	def initialize()
		fontsize = 2
		setstyles(fontsize)
					
	end
	
	def setstyles(fontsize = 2)
		@basic = "bgcolor='#A5B4CE'><font color='Black' size='#{fontsize}' face='Arial'>" # basic template
		@invert = "bgcolor='#5F7190'><font color='White' size='#{fontsize}' face='Arial'><b>"
		@header = "bgcolor='#5F7190'><font color='Black' size='3' face='Arial'><b>"
		@warning = "bgcolor='#AC0F06'><font color='White' size='#{fontsize}' face='Arial'><b>"
		@bold = "bgcolor='#A5B4CE'><font color='black' size='#{fontsize}' face='Arial'><b>"
		@default = "bgcolor='#A5B4CE'><font color='Black' size='#{fontsize}' face='Arial'>"		
		@defaultBeige = "bgcolor='#D5CDA5'><font color='Black' size='#{fontsize}' face='Arial'>"
		@boldBeige = "bgcolor='#D5CDA5'><font color='Black' size='#{fontsize}' face='Arial'><b>"
		@invertBeige = "bgcolor='#C6AA1D'><font color='Black' size='#{fontsize}' face='Arial'>"		
	end
	

end

class ReportHtml
	attr_reader :REFHTML
	attr_writer :REFHTML
	
	attr_reader :STYLE
	
	# HTML Tempaltes
	attr_reader :gTemplateH
	attr_reader :gTemplateBSH

	attr_reader :gTemplateW

	attr_reader :gTemplateDHH
	attr_reader :gTemplateDW 
	# EST OverVIew
	attr_reader :gTemplateBH
	attr_reader :gTemplateB

	# Detail Report
	attr_reader :gTemplateSDB
	attr_reader :gTemplateSDB2


	def initialize()
		@REFHTML = ""
		@STYLE = TemplateStyles.new()
	end
	

	
	def genHtmlHeading(sValue, size = 2)
		@REFHTML += "<div><font color='Black' size='#{size}' face='Arial'><b>#{sValue}</span></div>\n"
		#return @REFHTML
	end
	
	def closetable()
		@REFHTML += "</table>\n"		
	end
	
	def setdefault(template)
		@STYLE.default = template
	end
	
	def setfontsize(fontsize)
		@STYLE.setstyles(fontsize)
	end
	
	def gentableCol(inputArray, bClose = true)  
		@REFHTML += "<tr>\n"
		inputArray.each do | dicName |
			defaultTemplate = @STYLE.default
			value = dicName
			if dicName.class == Array then # If we find a tempalte then set the bitch
				defaultTemplate = dicName[1]
				value = dicName[0]
				
			end
		
			@REFHTML += "<td #{defaultTemplate}#{value}</font></td>\n"
		end
		if bClose then
			@REFHTML += "</tr>\n"
		end
		#return @REFHTML
	end
	
	# Parse our htmlbuilder array
	def parseHtmlData(testarry)
		htmlBody = ''
		testarry.each do | entry |
			gentableCol(entry, true)   
		end 
		#@REFHTML += htmlBody
		#return htmlBody
	end
	
	def generateHtlmReport()
		@REFHTML += "<body>\n"
		#@REFHTML += genHtmlHeading('EST Updated Report')  
		genHtmlHeading('EST Updated Report')  		
		@REFHTML += "<table width='800' border=1 cellspacing='0'>\n"
	end
	
	def content()
		return @REFHTML
	end

end

