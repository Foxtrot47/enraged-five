@ECHO OFF
REM 
REM File:: /wildwest/script/Ruby/utils/CutsceneRangeChecker/\test.rb.rb
REM Description:: cutscene Range Updater
REM
REM Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
REM Date:: 20 Februray 2013 (AP3)
REM

CALL setenv.bat > NUL
::CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\wildwest\script\ruby\utils\ReadFBX\readFBX.rb
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% x:\wildwest\script\ruby\utils\ReadFBX\readFBX.rb

PAUSE