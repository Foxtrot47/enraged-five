#

#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			~ Look through p4 submits in Assets folder
#			~ Check Latest and previous version and compare autoclips and EST range length
#			~ We need to check cutxml?
#


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

path = File.expand_path $0
path = File.dirname(path)

require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal

require "#{ENV['RS_TOOLSROOT']}/lib/pipeline/util/string"
require "#{path}/../../Global/debug/debugUtils.rb"
require "#{path}/../../Global/perforce/p4utils"

#Database
require "#{path}/../../Global/database/DatabaseConnection.rb"



class Transform
		attr_reader :X
		attr_reader :Y
		attr_reader :Z
		attr_writer :X
		attr_writer :Y
		attr_writer :Z
		
		def initialize()
			@X = 0
			@Y = 0
			@Z = 0
		end
	end

	
class SceneTranslation
	attr_reader :Name
	attr_reader :Translation
  attr_reader :Rotation
	

	def initialize(scenename = nil)
		@Name = File.basename(scenename).split('.')[0]
		@Translation = Transform.new()
		@Rotation = Transform.new()
		
	end
	
	
end

class ReadFBX
  def initialize(g_Log, config)
      @g_Log = g_Log
      @dBconnection = DatabaseConnection.new(@g_Log)
      @g_Config = config
      @@sourceDB = "CutsceneStats_#{@g_Config.project.name.upcase}"
  end
    
    
  # X:\wildwest\bin\motionbuilder\enviromentCoords\processFBX.py
  def parseFBX( sFilename, p4change )
    path_to_script = "#{@g_Config.project.config.tools_root}/techart/bin/motionbuilder/enviromentCoords"
    
    python = "X:/rdr3/tools/bin/python/Python27/x64/bin"
    cmd = "#{python}/python.exe #{path_to_script}/processFBX.py #{sFilename}"
    
    @result = IO.popen(cmd, "w+")
    
    
    sceneTranslation = SceneTranslation.new(sFilename)
     
    
    @result.each do | line |
      if line.starts_with('Translation')
        tokens = line.split(' ')
        sceneTranslation.Translation.X = tokens[1].to_f
        sceneTranslation.Translation.Y = tokens[2].to_f
        sceneTranslation.Translation.Z = tokens[3].to_f
      elsif line.starts_with('Rotation')
        tokens = line.split(' ')
        sceneTranslation.Rotation.X = tokens[1].to_f
        sceneTranslation.Rotation.Y = tokens[2].to_f
        sceneTranslation.Rotation.Z = tokens[3].to_f
      end
    end
    
    @g_Log.message( sFilename )
    debugPrint( sceneTranslation.Translation ) 
    debugPrint( sceneTranslation.Rotation )
    
    uploadInfo(sceneTranslation, p4change)
    
    return sceneTranslation
    
  end


  def connectDB()
        server = "nycw-mhb-sql"
        @dBconnection.connect(server, @@sourceDB)
      end
    
  def closeDB()
        @dBconnection.close()
  end	

  def uploadInfo( sceneTranslation, p4change )
    @dBconnection = DatabaseConnection.new( @g_Log )
    connectDB() 
    query = "
        Declare @cutID INT
        SET @cutID = (SELECT CutsceneID FROM Cutscene WHERE CutsceneName = '#{sceneTranslation.Name}')
				IF NOT EXISTS(SELECT * FROM EnviromentFBX WHERE FBXName='#{sceneTranslation.Name}')
                    BEGIN
                      INSERT INTO EnviromentFBX (CutsceneID, FBXName, Trans_X, Trans_Y, Trans_Z, [Rotation_Z],  ChangeList, DateModified, [User] )
                      VALUES (@cutID,'#{sceneTranslation.Name}', #{sceneTranslation.Translation.X}, #{sceneTranslation.Translation.Y}, #{sceneTranslation.Translation.Z},#{sceneTranslation.Rotation.Z}, #{p4change.number.to_i}, '#{p4change.time}', '#{p4change.username}')
					END
				ELSE
					BEGIN
						UPDATE EnviromentFBX 
							SET CutsceneID=@cutID,
              Trans_X='#{sceneTranslation.Translation.X}',
							Trans_Y='#{sceneTranslation.Translation.Y}',
							Trans_Z='#{sceneTranslation.Translation.Z}',
              [Rotation_Z]='#{sceneTranslation.Rotation.Z}',
              ChangeList = #{p4change.number.to_i},
              DateModified ='#{p4change.time}',
              [User] ='#{p4change.username}'
						WHERE FBXName='#{sceneTranslation.Name}'
					END
				
	"

    begin
      @dBconnection.read_data(query)
    rescue       
      @g_Log.error( "Scene #{sceneTranslation.Name} could not be uploaded to Database" ) 
      puts query
    end
      
    
  end
end



# THIS WILL REBUILD/POPULATE ALL ENVIORMENT DATA
if ( __FILE__ == $0 ) then

	begin
		LogFactory.Initialize()
		LogFactory.CreateApplicationConsoleLogTarget( )
		  @g_Log = LogFactory.ApplicationLog
		@g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( ) 
    @p4 = P4Utils.new(@g_Log)  

    readFBX = ReadFBX.new(@g_Log, @g_Config )
    
    
    
   
    
		
		dirPath = "#{@g_Config.project.DefaultBranch.Art}/animation/resources/Sets/"
    
    #@lastCL, changes = @p4.parse_p4_review(dirPath, 7314661, '#head')
    #changes.each do | change | 
    #  puts change
    #  validfiles = @p4.findFilesinChange( change, '*.fbx' )
    #  puts change['time']
    
    #end
    
		Dir["#{dirPath}*.FBX"].each do |fbxname|

      p4record = @p4.get_file_info(fbxname, 'headChange')
      p4changelist = @p4.get_changelistDescribe(p4record['headChange'])
      p4changes = @p4.get_changelists_from_changes( p4changelist )
			translation = readFBX.parseFBX(fbxname, p4changes[0])			

		end
		

	end
	
end














  



