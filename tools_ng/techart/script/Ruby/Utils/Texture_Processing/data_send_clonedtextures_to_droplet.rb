#!/usr/bin/env ruby

require 'fileutils.rb'
require 'pipeline/os/file.rb'


puts "\nDroplet script initialised, using"
puts ARGV[0] + " as the droplet" 
puts ARGV[1] + " as the texturelist"
puts "Overwrite previously copied textures = " + ARGV[2] + "\n"

# Define a texture list file and paths

originalpath = "gta5/art/textures"
newpath= "gta5/art/textures_CLONED"

# inputfilelist = 'c:\DIFFUSEtextureslist.txt'
# leveltool = 'X:\gta5\tools\wildwest\script\photoshop\tonemappingADJ_mapDiffuse.exe'
leveltool = ARGV[0] 
inputfilelist = ARGV[1]

inputtexturecount = 0
processedtexturecount = 0

# Parse each line in the input file
::File::open(inputfilelist) do |fp| 
        lines = fp.readlines( ) 
		
        lines.each do |line|  
			inputtexturecount+=1
			
			puts ""
			theoldpath = line.sub("\n", "")
			puts "Original Path: [#{theoldpath}]"
			
			# If the file exists, copy it
			if File.exists?( theoldpath )
			
				#We need to preserve the case sensitivity
				filesystem_pathname = nil 
				directory = File.dirname( theoldpath ) 
				basename = File.basename( theoldpath ) 
				Dir.entries( directory ).each do |filesystem_basename| 
						if( filesystem_basename.casecmp( basename ) == 0 ) then 
								filesystem_pathname = directory + "/" + filesystem_basename 
								break 
						end 
				end 

				#puts "filesystem_pathname: #{filesystem_pathname}"

				thenewpath = filesystem_pathname.gsub(originalpath,newpath)
				puts "New Path     : [#{thenewpath}]"
			
				# Does the file already exist?
				# If so, we might not copy it
				if ((File.exists?( thenewpath )) and (ARGV[2] == "false"))
					puts "! File already exists: [#{thenewpath}]" 
					puts "! Overwrite previously copied textures = " + ARGV[2] + "\n"
					puts "! Will not copy file." 
				
				# We do we want to copy it
				else	
					if ((File.exists?( thenewpath )) and (ARGV[2] == "true"))
						puts "= File already exists: [#{thenewpath}]" 
						puts "= Overwrite previously copied textures = " + ARGV[2] + "\n"
						puts "= Will copy and process file." 
					else
						puts "+ Doesnt exist   : [#{thenewpath}]"
						puts "+ Will copy and process file."  
					end
					
					FileUtils::mkdir_p( Pipeline::OS::Path.get_directory(thenewpath) ) unless ( ::File::directory?( Pipeline::OS::Path::get_directory( thenewpath ) ) )
					Pipeline::OS::FileUtilsEx::copy_file(theoldpath, thenewpath)
		
					# If a file was copied, make sure that it is writeable
					File.chmod(0755, thenewpath )
					# Pass the copied texture to the leveller software
					puts "Sending the file for processing...."
					
					# The photoshop command needs the slashes switched from / to \
					photoshoppath = "\"#{thenewpath.gsub('/','\\')}\""
					#puts "New Path before PS Switch    : #{thenewpath}"
					#puts "New Path after PS Switch    : #{photoshoppath}"
					
					# Push the image to the droplet
					dropletcommand = (leveltool +  " " + photoshoppath)
					#puts dropletcommand
					system(dropletcommand)
					processedtexturecount +=1
				end
			else
				puts " ** Cannot find: #{theoldpath} to copy"
			end	
			
			
        end 
end


puts "\n****************************************************************"
puts "Script complete"
puts "#{inputtexturecount} textures were checked from the input file."
puts "#{processedtexturecount} were cloned and sent to Photoshop."


