#!/usr/bin/env ruby

require 'fileutils.rb'
require 'pipeline/os/file.rb'

originalpath = "gta5\art\textures"
newpath= "gta5\art\textures_cloned"

# Was a texture supplied to the tool?
if ARGV[0] != nil 

	clonedTexture = ARGV[0]
	puts "\nComparison script stared, using"
	puts clonedTexture + " as the inputfilename" 
	
	# Assuming this is a good texture, no error chcking yet
	# This will be a cloned texture, so figure out the original name
	originalTexture = clonedTexture.gsub("_cloned", "")
	puts "Cloned Texture = " + clonedTexture
	puts "Original Texture = " + originalTexture
	
	#Send the texture to photoshop
	photoshoppath = 'c:\Program Files\Adobe\Adobe Photoshop CS5 (64 Bit)\Photoshop.exe'
	dropletcommand = (photoshoppath + " " + '"' + clonedTexture + '"')
	system(dropletcommand)
	dropletcommand = (photoshoppath + " " + '"' + originalTexture + '"')
	system(dropletcommand)

else
	puts "No input texture supplied"
end