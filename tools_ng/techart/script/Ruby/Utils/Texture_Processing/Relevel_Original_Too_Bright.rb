#!/usr/bin/env ruby

require 'fileutils.rb'
require 'pipeline/os/file.rb'

originalpath = "gta5\art\textures"
newpath= "gta5\art\textures_cloned"

# Was a texture supplied to the tool?
if ARGV[0] != nil 

	clonedTexture = ARGV[0]
	puts "\nRelevel script stared, using"
	puts clonedTexture + " as the inputfilename" 
	
	# Assuming this is a good texture, no error chcking yet
	# This will be a cloned texture, so figure out the original name
	originalTexture = clonedTexture.gsub("_cloned", "")
	puts "Cloned Texture = " + clonedTexture
	puts "Original Texture = " + originalTexture
	
	# Copy that texture to the cloned folder
	puts "Copying " + originalTexture + " over " + clonedTexture
	FileUtils::mkdir_p( Pipeline::OS::Path.get_directory(originalTexture) ) unless ( ::File::directory?( Pipeline::OS::Path::get_directory( originalTexture ) ) )
	Pipeline::OS::FileUtilsEx::copy_file(originalTexture, clonedTexture)

	# If a file was copied, make sure that it is writeable
	File.chmod(0755, clonedTexture )
	# Pass the copied texture to the leveller software
	puts "Sending the file for processing...."
		
	
	#Send the texture to photoshop
	leveltool = 'X:\gta5\tools\wildwest\script\photoshop\tonemappingADJ_mapDiffuse_light.exe'
	puts leveltool
	dropletcommand = (leveltool +  " " + '"' + clonedTexture + '"')
	
	puts dropletcommand
	system(dropletcommand)

else
	puts "No input texture supplied"
end