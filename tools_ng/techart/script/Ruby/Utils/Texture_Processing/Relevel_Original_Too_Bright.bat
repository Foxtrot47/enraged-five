REM Batch file to trigger Ruby script
REM Sends a texture to a Ruby script for processing
REM Rick Stirling <rick.stirling@rockstarnorth.com>

CALL setenv.bat

REM call the script with a texture to use
%RS_TOOLSRUBY% %RS_TOOLSROOT%/wildwest/script/ruby/Texture_processing/Relevel_Original_Too_Bright.rb %1


pause