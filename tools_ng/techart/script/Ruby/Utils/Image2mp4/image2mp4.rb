
#!/usr/bin/ruby
path = File.expand_path $0
path = File.dirname(path)

require 'pathname'


$ffprofile = "ld"
$crfsetting = "25"

TOOLS_BIN = "#{ENV['RS_TOOLSROOT']}\\Bin"
FFMPEG_BIN = "#{TOOLS_BIN}\\video\\ffmpeg.exe"

class Image2mp4
  
  def initialize(in_file=nil, out_file=nil)
    #in_file = %03d
    #convert(in_file, 'x:\temp\test.mp4')
  end

# parse the file adn query the format.  
  def convert(in_file, out_file=nil, frames=nil)
    inputfile = in_file
    if in_file.include? '00000'
      inputfile = in_file.gsub('00000','%05d')
    elsif in_file.include? '0000'
      inputfile = in_file.gsub('0000','%04d')
    elsif in_file.include? '000'
      inputfile = in_file.gsub('000','%03d')  
    end
    
    if out_file == nil
      pn = Pathname.new(in_file)
      dir, base = pn.split
      # replace extension
      _filename= File.basename(base, '.*')
      out_file = File.join(dir, "#{_filename}.mp4")

    end
    runffmpeg(inputfile, out_file, frames)
  end

  # TODO: Build a proper arg parser
  # Main ffmpeg conversion command
  def runffmpeg(in_file, out_file, frames)
    # Parse any parameters
    frameRange = ''
    frameRange = "-vframes #{frames}" if frames != nil
   
    ffmpeg_args = ['-r', '30', '-i', in_file, frameRange, '-vf', 'scale=960:520', '-pix_fmt', 'yuv420p', '-ar', '44100', '-f', 'mp4', '-vcodec', 'libx264', '-strict', 'experimental', '-acodec', 'aac', '-vpre', $ffprofile, '-crf', $crfsetting, '-y', out_file].join(' ')
    system ("#{FFMPEG_BIN} #{ffmpeg_args}")
  end
end

