#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 May 2013 (AP3)
# Purpose:
#			CUSTOM CLASS
# 			Used to generate a html report
# 			contains a list of functions to generate tablles and shit and return shit back
#
#			
#			TODO!!!!!
#			Use the fucking template you muppet instead of writing one.. what a doughnut!

path = File.expand_path $0
path = File.dirname(path)
require "#{path}/../../Global/report/ReportHTML.rb"
require "#{path}/../../Global/email/email.rb"
require "#{path}/../../Global/graph/graph.rb"
require "#{path}/../../Global/array/arrayUtils.rb"


=begin
# need to sort out this!!!
require 'X:/gta5\tools\wildwest\script\Ruby\Global\report\ReportHTML.rb'
require 'X:/gta5/tools/wildwest/script/Ruby/Global/email/email.rb'
#require 'X:\gta5\tools\wildwest\script\Ruby\net\graph.rb'
require 'X:/gta5/tools/wildwest/script/Ruby/Global/graph/graph.rb'
require 'X:\gta5\tools\wildwest\script\Ruby\Global\array\arrayUtils.rb'
=end
include System
include System::IO

EMAIL = 'AutoApprovedCutsceneUpdater@rockstargames.com'
AUTHOR = 'mark.harrison-ball@rockstargames.com'

class CutsceneReport

	def initialize(log)
		@arrayUtils = ArrayUtils.new()
		@g_Log = log
		@graph = Graph.new( 804 , 200 )
		
	end
	# debug Write
	def writeReport(inputText)
		fw = System::IO::StreamWriter.new('X:\temp\htmltest.html')		
		fw.WriteLine(inputText)
		fw.Close()
	
	end
	
	def genShotTable(reportHtml, cutdef)
		headerInfo = Array.new()
		headerInfo.push([['Shot Clips',reportHtml.STYLE.invert],['',reportHtml.STYLE.invert],['',reportHtml.STYLE.invert]])
		cutdef.autoclips.each do | autoclip |
			headerInfo.push([autoclip,'',''])				
		end
		reportHtml.parseHtmlData(headerInfo)
		reportHtml.REFHTML += "</table><br>\n"
		
	end
	
	def retGraphImage(name, animInfo, type)
		savePath = "x:/temp/#{name}_#{type}.png"
		@graph.new(savePath)
		translation = nil
		if type == 'translation' then
			translation = animInfo.translation
		elsif type == 'rotation' then
			translation = animInfo.rotation
		elsif type == 'fov' then
			translation = animInfo.fov
		end
		if translation!= nil then
			
			@graph.drawgraph(translation, type)
			@graph.drawblocks(animInfo.blockTags)
			@graph.save()
		end
		return savePath
	
	end
	

	def genReport(cutDefList, writeReport = false)
		 
		reportHtml = ReportHtml.new()
		embimages = []
		
		begin
			REFHTML = "<body>\n"
				# cREATE OUR hEADER
				
			cutDefList.each do | cutpairdef |
				@g_Log.message("Processing #{cutpairdef.path}")
				reportHtml.genHtmlHeading(cutpairdef.name, 3) 
				reportHtml.REFHTML += "<table width='800' border=0 cellspacing='0'>\n<col width='200'>"
				
				headerInfo = Array.new()
				headerInfo.push([['User',reportHtml.STYLE.invert],cutpairdef.cutdefA.user])
				headerInfo.push([['Cutscene',reportHtml.STYLE.invert],[cutpairdef.name,reportHtml.STYLE.bold]])
				headerInfo.push([['Path',reportHtml.STYLE.invert],cutpairdef.path])
				headerInfo.push([['ChangeList',reportHtml.STYLE.invert], [cutpairdef.changelist, reportHtml.STYLE.warning]])
				headerInfo.push([['Time',reportHtml.STYLE.invert],cutpairdef.cutdefA.time])
				headerInfo.push([['EST Change',reportHtml.STYLE.invert],'?'])
				
				reportHtml.parseHtmlData(headerInfo)
				reportHtml.closetable()
			
				
				# Range Details
				reportHtml.genHtmlHeading('EST Change Overview')   
				reportHtml.REFHTML += "<table width='400' align='left' border=0 cellspacing='0'>\n<col width='200'>" 
				
				headerInfo = Array.new()
				headerInfo.push([['',reportHtml.STYLE.invert],['Before',reportHtml.STYLE.invert],['After',reportHtml.STYLE.invert]])
				# reportHtml.parseHtmlData(headerInfo)
			
				#headerInfo = Array.new()
				reportHtml.setdefault(reportHtml.STYLE.defaultBeige)
				
				style = reportHtml.STYLE.defaultBeige
				if cutpairdef.cutdefA.rangeend != cutpairdef.cutdefB.rangeend then
					style = reportHtml.STYLE.warning
				end
				headerInfo.push([['Range start/end',reportHtml.STYLE.boldBeige],["0-#{cutpairdef.cutdefA.rangeend}",style],["0-#{cutpairdef.cutdefB.rangeend}",style]])
				headerInfo.push([['Range (frames)',reportHtml.STYLE.boldBeige],[cutpairdef.cutdefA.duration,style], [cutpairdef.cutdefB.duration,style]])
				style = reportHtml.STYLE.defaultBeige
				if cutpairdef.cutdefA.autoclips.Count != cutpairdef.cutdefB.autoclips.Count then
					style = reportHtml.STYLE.warning
				end
				headerInfo.push([['Total Clips',reportHtml.STYLE.boldBeige],[cutpairdef.cutdefA.autoclips.Count, style], [cutpairdef.cutdefB.autoclips.Count,style]])
				reportHtml.parseHtmlData(headerInfo)
				reportHtml.closetable()
				
				# Cutscene Flags Table
				headerInfo = Array.new()
				reportHtml.REFHTML += "<table width='400'  border=0 cellspacing='0'>\n<col width='200'>" 
				headerInfo.push([['Cutscene Flags',reportHtml.STYLE.invert]])
				reportHtml.parseHtmlData(headerInfo)
				
				headerInfo = Array.new()
				cutpairdef.cutdefA.flags.each do | flag |
					style = reportHtml.STYLE.defaultBeige

					if flag.include? "BLENDOUT" then
						style = reportHtml.STYLE.invert					
					elsif flag.include? "CATCHUP" then
						style = reportHtml.STYLE.invert
					end
					
					headerInfo.push([[flag,style]])
				end
				reportHtml.parseHtmlData(headerInfo)
				reportHtml.closetable()
				
				reportHtml.setdefault(reportHtml.STYLE.basic)
				
				# Animation Changes Table
				headerInfo = Array.new()
				reportHtml.REFHTML += "<table width='800'  border=0 cellspacing='0'>\n<col width='100'>" 
				headerInfo.push([['Camera Changes',reportHtml.STYLE.invert]])	
				reportHtml.setfontsize(1)
#,['Min',reportHtml.STYLE.invert],['Max',reportHtml.STYLE.invert]
				#headerInfo.push([['Camera Changes',reportHtml.STYLE.invert]])
				style = reportHtml.STYLE.basic
				if cutpairdef.animInfo.TranslationChanged() then
					style = reportHtml.STYLE.warning
					
				end
				pth = retGraphImage(cutpairdef.name,cutpairdef.animInfo, "translation" )
				img = "<img src='#{pth}' alt='logo' />"
				headerInfo.push([["TRANSLATION Min: #{@arrayUtils.retMin(cutpairdef.animInfo.translation).to_s} Max: #{@arrayUtils.retMax(cutpairdef.animInfo.translation).to_s} Meters",style]])
				headerInfo.push([[img,reportHtml.STYLE.boldBeige]])
									
				#reportHtml.REFHTML += "<img src='#{pth}' alt='logo' />"	
				
				style = reportHtml.STYLE.basic
				if cutpairdef.animInfo.RotationChanged() then
					style = reportHtml.STYLE.warning					
				end	
				pth = retGraphImage(cutpairdef.name,cutpairdef.animInfo, "rotation" )
				img = "<img src='#{pth}' alt='logo' />"
				#img = "<img src='#{pth}' alt='logo' />"	
				headerInfo.push([["ROTATION Min: #{@arrayUtils.retMin(cutpairdef.animInfo.rotation).to_s} Max: #{@arrayUtils.retMax(cutpairdef.animInfo.rotation).to_s} Degrees",style]])
				headerInfo.push([[img,reportHtml.STYLE.boldBeige]])
				
				#reportHtml.REFHTML += "<img src='#{pth}' alt='logo' />"
				
				style = reportHtml.STYLE.basic
				if cutpairdef.animInfo.FovChanged() then
					style = reportHtml.STYLE.warning					
				end
				pth = retGraphImage(cutpairdef.name,cutpairdef.animInfo, "fov" )
				img = "<img src='#{pth}' alt='logo' />"	
				headerInfo.push([["FOV Min: #{cutpairdef.animInfo.fov.min.to_s} Max: #{cutpairdef.animInfo.fov.max.to_s}",style]])				
				headerInfo.push([[img,reportHtml.STYLE.boldBeige]])
	
				
				
				reportHtml.parseHtmlData(headerInfo)
				reportHtml.closetable()
				reportHtml.setfontsize(2)
				
				# AutoCLip Details
				reportHtml.setdefault(reportHtml.STYLE.basic)			
				reportHtml.genHtmlHeading('EST Track Details') 
				reportHtml.REFHTML += "<table width='400' border=0 cellspacing='0'>\n"
				reportHtml.REFHTML += "<tr><td valign='top'>\n"
				
				# TableHeader
				reportHtml.REFHTML += "<table width='200' border=0 cellspacing='0'>\n"# New Table
				
				# Gen shot table A
				genShotTable(reportHtml, cutpairdef.cutdefA)			
							
				reportHtml.REFHTML += "<td valign='top'>\n"
				reportHtml.REFHTML += "<table width='200' border=0 cellspacing='0'>\n"# New Table  
				
				# Gen shot table B
				genShotTable(reportHtml, cutpairdef.cutdefB)			
				
				# Close nest tables
				reportHtml.REFHTML += "</table></td>\n" #Close our table  
				reportHtml.REFHTML += "</tr></table><br><br>\n" #Close our table 

				# CLose Html Body
				reportHtml.REFHTML += "</body>\n"
			
			end
		rescue => e 
			@g_Log.error(e.message+'/n'+e.backtrace) 
		end
		
		
		if writeReport then
			writeReport(reportHtml.content())
		end
		
		return reportHtml.content()
	
		
	end
	
	

end

