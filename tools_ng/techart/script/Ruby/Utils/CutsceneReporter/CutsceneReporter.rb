#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			~ Look through p4 submits in Assets folder
#			~ Check Latest and previous version and compare autoclips and EST range length
#			~ We need to check cutxml?
#

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!NOTES!!!!!!!!!!!!!!!!!!!!!!!!!!!
# USE
#  REF:X:\gta5\tools\ironlib\util\anim\cutscene\zip_animations.rb
#     require 'RSG.ManagedRage.dll'
#     include RSG::ManagedRage::ClipAnimation
#     rageClip = MClip::new()
#     if(rageClip.Load(clip) == true) then
#


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
#$LOAD_PATH.unshift File.expand_path('../../Global/perforce/', __FILE__)


require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'
require 'RSG.Metadata'

# Core
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::SourceControl::Perforce
include RSG::Metadata


require 'mscorlib'
require 'System.Core'
require 'System.Xml'


include System::Collections::Generic
include System::IO
include System::Diagnostics
include System::Xml
include System::Runtime::InteropServices
include System::Net
include System::Net::Sockets
include System::Net::Mail

path = File.expand_path $0
path = File.dirname(path)
require "#{path}/../../Global/perforce/p4utils"
require "#{path}/../../Global/pargen/pscparser"
require "#{path}/../../Global/pargen/pscxmlparser"
require "#{path}/../../Utils/CutsceneReporter/CutsceneAnimDef.rb"
require "#{path}/../../Utils/CutsceneReporter/CutsceneRangeDef"
require "#{path}/../../Utils/CutsceneReporter/CutsceneReport.rb"
require "#{path}/../../Global/email/email.rb"



require 'pipeline/os/options'
#require 'pipeline/util/email'
include Pipeline

DEBUG = true # Send Emails me Author Only
REPEAT = false  	# Use Custom CL to generate report from
ALL_CHANGES = false 	# Scan for all changes if NOt in repeat mode
#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

# Will Hook up laters
OPTIONS = [ 
    LongOption::new( 'frequency', LongOption::ArgType.Required, 'f', 
        'frequency to check perforce for submits(required).' )
        ]
        
        

EXCEPTION_EMAIL = 'mark.harrison-ball@rockstargames.com'

AUTHOR = 'mark.harrison-ball@rockstargames.com'
			


					
PRODUCTION_EMAIL = ['dermot.bailie@rockstarnorth.com',
					'tina.chen@rockstargames.com',
					'sam.henman@rockstargames.com',					
					'sean.letts@rockstarsandiego.com',					
					'matt.tempest@rockstarlondon.com',
					'geoffrey.fermin@rockstarnorth.com',
					'jenny.arnold@rockstarnorth.com',
					'Matthew.Rochester@rockstarnorth.com',
					'wallace.robinson@rockstartoronto.com',
					'nick.robey@rockstarleeds.com',					
					'tim.webb@rockstarsandiego.com',
					'blake.buck@rockstargames.com',
					'mark.harrison-ball@rockstargames.com'
					]

					
#PRODUCTION_EMAIL = ['mark.harrison-ball@rockstargames.com',
#					'mark.harrison-ball@rockstargames.com']

@INVALIDEMAILS = []



#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------


# Return full filename if matches passed in tail name (i.e CutXML
def findFileinChange( change, file = nil )
	if file != nil
		change.files.each do | filename |			
			file_name = File.basename(filename).downcase
			return filename if (file_name == file.downcase)
		end
		
	end
	nil
end

# Make sure email is valid...
def validateemail(email)
	validEmail = email
	emailTokens = email.split("@")
	if emailTokens[1] == "take2games.com" then
		validEmail = emailTokens[0] + "@rockstargames.com"		
	end
	validEmail
end


def parse_p4_review()
	@USEREMAILS = []
	# List of Cutscene Chnage Inforamtion
	cutDefList = []
	
	# This goes into our loop
	head = '#head'
	if not ALL_CHANGES then
		head = @currentCL # Set to parse a single chnagelist for debugging
	end
	
	@lastCL, changes = @p4.parse_p4_review(@depotpath, @currentCL, head)
	changes.each do | change |
		
		# Check each change, see if there has been a cutxml file submitted
		filename = findFileinChange( change, 'data.cutxml' )
		exportcam = findFileinChange( change, 'ExportCamera.anim' )

		projectPath = nil
		if filename != nil
			projectPath = "x:"+filename.split("//depot")[1]
		end
		
		if projectPath != nil and File.exist?(projectPath) then
			
			# Parse previous Revision
			revIndex = @p4.get_file_info(filename, 'headRev')
			# get Export Camera revision		
			puts "revIndex = #{revIndex}"
			
			# If we have files to compare
			if revIndex.to_i > 1 then
				args = '#'+(revIndex.to_i-1).to_s
				
				headerinfo = P4fileinfo.new()
				headerinfo.user = change.username
				headerinfo.email = validateemail(@p4.get_user_email(change.username))
				headerinfo.time = change.time
				headerinfo.filename = filename
				headerinfo.revision = revIndex.to_i-1
				

				oldObj = nil
				newObj = nil
				
				#Sync to previous revision then parse
				@p4.sync(filename, args)
				
				begin					
					@pscDefObject.parse(projectPath)
					@xmlpscDefObject.parse(projectPath) # xml to read the auto clips, the pso parser fails often :(					
					oldObj =  cutdef.new( headerinfo, @pscDefObject, @xmlpscDefObject )
					
				rescue => msg 
					puts msg					
				end
				
				# Syn to Head then parse
				headerinfo.revision = revIndex.to_i
				
				@p4.sync(filename)
				begin
					@pscDefObject.parse(projectPath)
					@xmlpscDefObject.parse(projectPath) # xml to read the auto clips, the pso parser fails often :(
					newObj = cutdef.new( headerinfo, @pscDefObject, @xmlpscDefObject )
					
				rescue => msg 
					puts msg					
				end
				
				animInfo = nil
				if exportcam != nil then
					# Compare our anim tracks						
					cameraFilename = File.join(File.dirname(projectPath),"ExportCamera")
					@g_Log.message("Comparing Export Camaera Data #{cameraFilename}")					
					animInfo = @cutsceneAnimCompare.compare(cameraFilename)
				end
				
				
				if newObj and oldObj then
					cutDefList << Cutpairdef.new(oldObj, newObj, change.number, animInfo)
				end
			end

		end
	end
	# Collect changed cutscenes
	invalidCuts = validateCuts(cutDefList)
	# Generate our report
	generateReport(invalidCuts)

end


def isValidChange(cutpairdef)
	# Check if Transaltion difference is more than
	if cutpairdef.animInfo != nil
		if cutpairdef.animInfo.TranslationDiff() > 2.0 then 
			return true
		# Check if Rotation difference is more than
		elsif cutpairdef.animInfo.RotationDiff() > 5.0 then 
			return true
		# Check if FOV difference is more than
		elsif cutpairdef.animInfo.FovDiff() > 3.0 then 
			return true
		else
			return false
		end
	else	
		return false
	end
	
end

# Validate our Cuts
def validateCuts(cutDefList)
	invalidCuts = Array.new()
	@INVALIDEMAILS = []
	@g_Log.message('Validating Submits...')
	begin
		cutDefList.each do | cutpairdef |
			@g_Log.message("Checking Submit #{cutpairdef.changelist} by #{cutpairdef.cutdefA.email}")
			if cutpairdef.has_DifferentRanges() or isValidChange(cutpairdef) then #.has_DifferentAnim() then #cutpairdef.has_DifferentRanges() or 
				invalidCuts.push(cutpairdef)
				@g_Log.message("Submit #{cutpairdef.changelist} has changed")
				if not @INVALIDEMAILS.include? cutpairdef.cutdefA.email
					@INVALIDEMAILS << cutpairdef.cutdefA.email
				end
				# Debug Pront

			end
		end
	rescue => e  
		logerror(e)        
    end
	
	invalidCuts
end

# Generate our Report
def generateReport(invalidCuts)	
	# Create Report
	if invalidCuts.count > 0 then
		@g_Log.message('Creating the Report')
		htmlcontent = @cutsceneReport.genReport(invalidCuts, true)
		@g_Log.message('Emailing report')
		tolist = []
		
		tolist << PRODUCTION_EMAIL
		
		tolist << @INVALIDEMAILS
		
		
		tolist = tolist.flatten

		if DEBUG then
			tolist = AUTHOR
		end
		send_email( AUTHOR,tolist,'Cutscene Change Detected [WIP REPORT]', htmlcontent, @server)
					
	end
end

# Log and email reporter
def logerror(e = 'unknown')
	msg = "Exception occured: #{e.message} in #{e.backtrace}"
	@g_Log.error(msg)
	send_email( AUTHOR, EXCEPTION_EMAIL, 'Cutscene Report Tool Crash', msg, @server)
end


#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    g_Options = OS::Options::new( OPTIONS )
    LogFactory.CreateApplicationConsoleLogTarget( )
        @g_Log = LogFactory.ApplicationLog
    
	# DEbug options:
	      

    begin
        if ( g_Options.is_enabled?( 'help' ) )
            puts "#{__FILE__}"
            puts "Usage:"
            puts g_Options.usage()
            exit( 1 )
        end

        g_Frequency = g_Options.get?( 'frequency' ) unless g_Options.get( 'frequency' ).nil?
        
        # Config initialisation. 
        g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   
		@email = g_Config.EmailAddress	
		@server= g_Config.studios.this_studio.exchange_server # NY SEVER BLOCKS THIS SRUFF
		@g_Log.message("Using Server: #{g_Config.studios.this_studio}")
	
        branch = g_Config.Project.DefaultBranch

        # Static
        @depotpath = '//depot/gta5/assets/cuts/...cutxml'
        
        # Reference our p4 UTIL class
		@p4 = P4Utils.new(@g_Log)
		
		# Refernece our PSC parser
		@pscDefObject = Pargen.new(branch)
		@xmlpscDefObject = Xmlpargen.new()
		# Refernce our Anim Compare parser
		@cutsceneAnimCompare = CutsceneAnimCompare.new(g_Config, @g_Log, @p4)
		
		# Reference email Report Tool
		@cutsceneReport = CutsceneReport.new(@g_Log)
		
		# Get current CL number
		@currentCL = @p4.get_current_changelistnumber()

		loop do
			unless not REPEAT
				begin
					parse_p4_review()
					@currentCL = @lastCL
					@g_Log.message("Current CL: #{@currentCL}")
					@g_Log.message('sleeping')
					sleep (100)
					@g_Log.message('checking p4 submits')
				rescue => e 
					logerror(e)
				end
			else
				begin
					@currentCL = 4558792  #4543003 #4549651
					parse_p4_review()
				rescue => e 
					logerror(e)
				end
				break			
			end
		end

        
    rescue => e  
		logerror(e)      
    end


end