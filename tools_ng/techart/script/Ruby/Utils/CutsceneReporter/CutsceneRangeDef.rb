#
#	Cunscene Range Def
#	CUSTOM CLASS
#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			Simple class of hold all the information about a cutscene
#			
#

# CORE
path = File.expand_path $0
path = File.dirname(path)
require "#{path}/../../Global/cutscene/flags.rb"
#require 'X:\gta5\tools\wildwest\script\Ruby\Global\cutscene\flags.rb'

class Shotdef
	attr_reader :shotname
	attr_reader :rangestart
	attr_reader :rangeend
	attr_reader :discardFramePairsList
	
	def initialize( shotname )
		@shotname = shotname
	end
		
end	
	

class Cutdef
	attr_reader :name
	attr_reader :path
	attr_reader :revision
	attr_reader :user
	attr_reader :email
	attr_reader :time
	attr_reader :flags
	attr_reader :rangestart
	attr_reader :rangeend
	attr_reader :autoclips
	attr_reader :duration
	attr_reader :concatDataList
	

	def initialize( header, pargenobject, xmlpargenobject = nil )
		
		dir, base = File.split( header.filename )					
		@name = File.basename( dir )
		@path = header.filename
		@revision = header.revision
		@user = header.user
		@email = header.email
		@time = header.time
		@flags = nil
		# Class instance of the cutscene flag parser
		@cutflagsdef = CutFlagsObject.new()	
		# Get the properties
		setProperties( pargenobject, xmlpargenobject )

	end
	
	def setProperties( pargenobject, xmlpargenobject = nil )	
		@duration = pargenobject.find('fTotalDuration').value.to_i
		@rangestart = pargenobject.find('iRangeStart').value.to_i
		@rangeend = pargenobject.find('iRangeEnd').value.to_i
		
		@autoclips = xmlpargenobject.returnAutoClips()
		
		flag = pargenobject.findfirst('iCutsceneFlags').value
		@flags = @cutflagsdef.readFlags(flag)
		
	end
	
	# Properties
	# Duration
	def duration()
		(@rangeend - @rangestart)
	end
end
	
=begin	
	# Bit of a hack so we can read teh xml file using xml
	def returnAutoClips()
		autoclips = []
		projectPath = "x:"+@path.split("//depot")[1]
		@file = File.new( projectPath )
		@xmldoc	= REXML::Document.new( file )
		s = xmldoc.elements.each("rage__cutfCutsceneFile2/pCutsceneEventArgsList/Item") { |element| 
			if element.attributes["type"] == "rage__cutfCameraCutEventArgs" then
			
			autoclips << element.elements["cName"]
			
			end
		}		
		return autoclips
	end
=end
	



# Used to hold our clip defs for comparing two entreis
class Cutpairdef
	attr_reader :cutdefA # old
	attr_reader :cutdefB # new
	attr_reader :name
	attr_reader :path
	attr_reader :changelist
	attr_reader :animInfo
	
	
	def initialize( inputA = nil, inputB = nil, changelist = 0, animInfo = nil)
		@name = inputA.name
		@path = inputB.path
		@cutdefA = inputA
		@cutdefB = inputB	
		@changelist = changelist
		@animInfo = animInfo
	end
	
	# Check Ranges
	def has_DifferentRanges()
		( @cutdefA.duration() != @cutdefB.duration() )
	end
	
	def has_DifferentClips()
		( @cutdefA.autoclips != @cutdefB.autoclips )
	end
	
	def autoclips()
		@cutdefA.autoclips()		
	end
	
	def has_DifferentAnim()
		if @animInfo == nil then
			return false
		end 
		@animInfo.hasChanged()		
	end
	
	
end



