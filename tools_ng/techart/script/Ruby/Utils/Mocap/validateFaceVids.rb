#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 10 August 2013 (AP3)
# Purpose:
#			~ Compare file list with directory filelist
#			


require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'


# Core
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS



require 'mscorlib'
require 'System.Core'
require 'System.Xml'



include System::Collections::Generic
include System::IO
include System::Diagnostics
#include System::Xml
include System::Runtime::InteropServices
include System::Net
include System::Net::Sockets
include System::Net::Mail

path = File.expand_path $0
path = File.dirname(path)
require "#{path}/../../../../../lib/pipeline/util/string"


#read files
#remove Dups 
#check missing files
#generate report


#
# Read our files

def readFile(path)
  @g_Log.message("Reading File #{path}" )
  contents = Array.new()
  file = File.new(path, "rb")
  while (line = file.gets)
    line = line.gsub(/\r\n?/, "")
    line = line.gsub(/\n?/, "")
    #@g_Log.message("#{line}" )
    contents << line
  end
  file.close
  return contents
end

# Read a list of quicktime files in our directory
def readDirectoryMovs(path)
    templist = []
    filelist = Dir["#{path}*.mov"]
    filelist.each do | path |
      templist.push(File.basename(path))
    end
    @g_Log.message("Found #{templist.count.to_s} mov files in #{path} directory")
    templist
end


def readCVSFile(path)
	@g_Log.message("Reading File #{path}" )
	contents = Array.new()
  
	file = File.new(path, "rb")
	while (line = file.gets)
    if not line.starts_with( 'CaptureDay' ) then
      lines = line.split(',')
      if lines[3] != nil then
        # column 3, fix name by replacing space of underscore
        scriptCharName = lines[3].gsub(' ', '_')
        # join name to our TrialName
        trialNameMov = "#{lines[1]}_#{scriptCharName}.mov"
        contents << trialNameMov
      end
    end

	end
	file.close
	return contents
end

#
# Get a list of files from the directoy to pass
def getfaceCapfiles()
  filelist = []
  filelist << Dir["#{@workingPath}/**/*.facecap1list"]
  filelist << Dir["#{@workingPath}/**/*.facecap2list"]

end

# Get our CSV file, there will only be one csv file so grab any file that has that extension.
def getCSVFiles()
  filelist = []
  filelist << Dir["#{@workingPath}/**/*.csv"]
end


def genMissingReport(uniqueFiles, filename) 
  missingFiles = Array.new()  
  uniqueFiles.each do | entry |
   
    if not @dirFileList.include?(entry)
      missingFiles << entry
    end
  end
  
  @g_Log.message("Found #{missingFiles.size.to_s} missing files")
  @g_Log.message("Generating Report: #{@workingPath+"/CSV_missingFiles.txt"} ")
  # Generate missing files list report
  file = File.new(@workingPath+"/#{filename}.txt", "w")
  missingFiles.each do | entry |
    file.write(entry+"\n")
  end  
  file.close
  
end



# Dupes migth be found in each fillist group
def genDuplicateReport()
  file = File.new(@workingPath+"/FaceCapLists_duplicateFiles.txt", "w")
  duplist = Hash.new
  arr = @totalFiles
  Hash[*(arr.uniq.map{|x| [x,arr.select{|y| y == x}.length]}).flatten].each do | val, count |
    if count > 1 then
      @faceCapList.each do | name, list |        
        if list.include?(val) then
            if duplist[val] == nil then
              newArray = Array.new()
              newArray << name
              duplist[val] = newArray
            else
              duplist[val] << name
            end
        end
      end
    end
  end
  
  @g_Log.message("Generating Report: #{@workingPath+"/FaceCapLists_duplicateFiles.txt"} ")
  
  duplist.each do | name, entry |
    file.write("#{name}\n#{entry}\n\n")
  end

 
end


#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
        @g_Log = LogFactory.ApplicationLog
     
    # Get our local Dir the batch file is workig from
    @workingPath = ARGV[0]

    # Main
    begin
      g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( ) 
      
      @dirFileList = Array.new() # Parse List form text file 
      @faceCapList = Hash.new    # Holds the dictionary of the facelist files as these are taken from two machines
      
      @cvsList = Array.new() # List of facecap files found in Dir
    

      # Read our list of files in the directory
      @dirFileList = readDirectoryMovs("#{@workingPath}/30Fps/")
      @g_Log.message("\n\n")
      
      # ============================================================
      # Read our CSV files
      #=========================================================   
      getCSVFiles().each do | path |
        @cvsList = readCVSFile(path[0])
      end
      
      uniqueFiles = @cvsList.uniq
      
      @g_Log.message("Total Entires found in *.csv file: #{@cvsList.count.to_s}")
      @g_Log.message("Total Unique Entries: #{uniqueFiles.length.to_s}")
      genMissingReport(uniqueFiles,"CSV_missingFiles")
      
      
      
      #=========================================================      
      # Read the facecap1list files
      #=========================================================   
      @g_Log.message("\n\n")
      getfaceCapfiles().each do | path |
        @faceCapList[path[0]] = readFile(path[0])      
      end
      
      totalFiles = 0
      # Get our unique files Master List
      uniqueFaceCapFiles = Array.new()
      @faceCapList.each do |name, val|
        uniqueFaceCapFiles << val
        totalFiles += val.size
      end
      @totalFiles = uniqueFaceCapFiles.flatten!.sort
      uniqueFaceCapFiles = @totalFiles.uniq 
     
      @g_Log.message("Total Entires found in *.facecap list files: #{@totalFiles.count.to_s}")
      @g_Log.message("Total Unique Entries: #{uniqueFaceCapFiles.length.to_s}")
      genMissingReport(uniqueFaceCapFiles,"FaceCapLists_missingFiles")
      genDuplicateReport()
    end
end