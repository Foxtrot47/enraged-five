#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 15 September 2013 (AP3)
# Purpose:
#			~ Process a list of FBX files and extract the cooridantes from the offset position for use with cutscenes



#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------



require 'System.Xml'
require 'System.Xml.Linq'
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'


# Core
include System::Xml
include System::Xml::Linq
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal



path = File.expand_path $0
path = File.dirname(path)


# global Core classes
require "#{path}/../../Global/project/project"
require "#{path}/../../Global/perforce/p4utils"
# logging
require "#{path}/../../Global/logging/logging.rb"
# Environment Coords Monitoring
require "#{path}/../../Utils/ReadFBX/readFBX.rb"



# Check for Environment submits
def processAllSets()  
  fbxSetsPath = "#{$g_Config.project.DefaultBranch.Art}/animation/resources/Sets/"
  Dir.glob("#{fbxSetsPath}/**/*.fbx").each do | fbxfile |  
    record = @p4.get_file_info(fbxfile,'headChange')
    parseChangefromCL( record['headChange'] )	
  end
end


# process a single fbx from a valid CL
def parseChangefromCL( cl )
  change = @p4.get_Change_from_CL( cl )
  if change != nil
    validfiles = @p4.findFilesinChange( change , '*.fbx' ) 
    validfiles.each do | fbxfile |
      @readFBX.parseFBX(fbxfile.replace('//','x:/'), change)
    end
  end
end



#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    #g_Options = OS::Options::new( OPTIONS )
    #Need to enable the initlaize on the TOOLS NG Branch
    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
    g_Log = LogFactory.ApplicationLog
    
    # Config initialisation. 
    $g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   

    begin
    
      @glog = Logger.new( $g_Config , g_Log)    
      @p4 = P4Utils.new(g_Log) 
      @readFBX = ReadFBX.new(@glog, $g_Config)
    
    
      if ARGV.length == 0   
        @glog.message("Running in Directory mode...")
        processAllSets()
      
      elsif ARGV.length > 0
        @glog.message("Running in Automation mode...")
        cl = ARGV[0].to_i
        parseChangefromCL( cl )    
      end 

    rescue => e 
    
      @glog.errorEmail(e)
      raise
        
    end
end



