@ECHO OFF
REM 
REM File:: /wildwest/script/Ruby/ApprovedCutsceneUpdater/CutsceneApprovedUpdater.rb
REM Description:: Run cutscne updater
REM
REM Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
REM Date:: 20 Februray 2013 (AP3)
REM

CALL setenv.bat > NUL
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\wildwest\script\ruby\ApprovedCutsceneUpdater\CutsceneApprovedUpdater.rb

PAUSE