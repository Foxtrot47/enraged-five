#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 15 September 2013 (AP3)
# Purpose:
#			This is just a conatiner to hold additional info about a cutscene so we can expand easily later


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

path = File.expand_path $0
path = File.dirname(path)

class CutsceneDef
  attr_reader :cutInfo
  attr_reader :fileHeader
  attr_reader :project  # Givs us the option to scan multiple projects at eh same time later
  
  
  def initialize(cutInfo, projectID, fileHeader = nil)
    @cutInfo = cutInfo
    @fileHeader = fileHeader
    @project = projectID
  end
  
  
  
end