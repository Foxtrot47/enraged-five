


path = File.expand_path $0
path = File.dirname(path)

require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.SourceControl.Perforce'
require 'x:\gta5\tools_ng\techart\lib\util\RSG.TechArt.GradingDatabase.dll'
include RSG::TechArt::GradingDatabase
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::SourceControl::Perforce


require "#{path}/../../Global/perforce/p4utils"




PROJECTID = 1
@DLC = 0

class DLCPRJ
  PROJECT = 
  {
    'None' => 0,
    'spAssassination' => 1,
    'spManhunt' => 2,
    'spPilotSchool' => 3,
    'spZombies' => 4,
    'dlc_agentTrevor' => 5,
    'dlc_relationship' => 6,
    'mpHeist' => 7
  }

end




def ParseGrading() 

  
  # DLC
  @g_Config.DLCProjects.each do | dlcPrj |
    targetProject = dlcPrj.Value
    syncPaths = [dlcPrj.Value.DefaultBranch.Assets + "/cuts/!!Cutlists/...", dlcPrj.Value.DefaultBranch.Assets + "/anim/grading/..." ]
   
    
    
    @g_Log.message("Syncing to Paths #{syncPaths}")      

    @p4.sync(syncPaths,'')
    

    
    Dir["#{dlcPrj.Value.DefaultBranch.Assets}/cuts/!!Cutlists/*.concatlist"].each do |file|      
      @g_Log.message("Processing #{file}")
      @DLC = DLCPRJ::PROJECT["#{dlcPrj.key}"]
      
      if @DLC == nil then
        next
      else
        @g_Log.message("Found valid concat #{file}")
        begin
          RSG::TechArt::GradingDatabase::GradingDatabase.ReadGradingObject(targetProject, file, PROJECTID, @DLC)
        rescue => e 
          logerror(e)
        end 
      end
    end
  end

  
  @DLC = 0
  # DEFAULT PROJECT
  TargetProject = @g_Config.project
  concatsPath = @g_Config.project.DefaultBranch.Assets + "/cuts/!!Cutlists/" 
  Dir["#{concatsPath}*.concatlist"].each do |file|
    #@g_Log.message("Processing #{file}")
    begin

      #RSG::TechArt::GradingDatabase::GradingDatabase.ReadGradingObject(TargetProject, file, PROJECTID, @DLC)
    rescue => e 
			logerror(e)
		end
  end

  
  
end


def logerror(e = 'unknown')
	msg = "Exception occured: #{e.message} in #{e.backtrace}"
	@g_Log.error(msg)
end



#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    #g_Options = OS::Options::new( OPTIONS )
    #Need to enable the initlaize on the TOOLS NG Branch
    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
        @g_Log = LogFactory.ApplicationLog
    
    # Config initialisation. 
    @g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   
    @p4 = P4Utils.new(@g_Log) 
    ParseGrading()
    
    
end