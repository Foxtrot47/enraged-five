#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 15 October 2013 (AP3)
# Purpose:
#			~ Cutfile Model Reader



path = File.expand_path $0
path = File.dirname(path)

# Model Types
PROP_TYPE = 'cutfPropModelObject'
PED_TYPE = 'cutfPedModelObject'
VEHICLE_TYPE = 'cutfVehicleModelObject'

class ModelInfo2

    # Purpose:  Updates a cutInfo class with just models from an metaData Definition
    #
    # Input:  CutInfo class , MetaDataUtilties, Metadata Definition      
    # Returns: Updated CutInfo
   
    def getModelNames(cutInfo, metadataUtils, metaDataType)  
      puts "processing MOdesl"
      if not cutInfo == nil    
        if metaDataType.class == ARRAY_TUNABLE
          metaDataType.each do | ent |         
            if ent.class == POINTER_TUNABLE
              if ent.value.name == PED_TYPE  
                puts ent.value.name
                cutInfo.characters.push(getNamefromModel(metadataUtils.getName(ent.value, 'cName')))
              elsif ent.value.name == PROP_TYPE
                cutInfo.props.push(getNamefromModel(metadataUtils.getName(ent.value, 'cName')))
              elsif ent.value.name == VEHICLE_TYPE
                cutInfo.vehicles.push(getNamefromModel(metadataUtils.getName(ent.value, 'cName')))
              end
            end
          end
        end
      else
        return nil
      end
      
    end

    # Fix up the Names from the MetaData Entry
    def getNamefromModel(modelName)
      modelName[0].split(':')[0]      
    end

end