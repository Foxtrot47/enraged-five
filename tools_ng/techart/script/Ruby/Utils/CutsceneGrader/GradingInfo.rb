#-----------------------------------------------------------------------------
# Holds the overall Grading Information
#-----------------------------------------------------------------------------
require 'System.Core'
include System
include System::IO
include System::Diagnostics

class ErrorType
end

class GradingInfo
  # Constructors
  attr_reader :name
  attr_writer :name
  attr_reader :length
  attr_writer :length
  attr_reader :duration
  attr_writer :duration
  attr_reader :characters
  attr_writer :characters  
  attr_reader :props
  attr_writer :props  
  attr_reader :vehicles
  attr_writer :vehicles
  
  
  
  # Move error logging into own class
  attr_reader :error
  attr_writer :error
  attr_reader :errorMsg
  attr_writer :errorMsg


	def initialize()
    @name = ''
    @length = 0
    @duration = 0
    @characters = []
    @cameras = []
    # Props
    @props = []    
    @propInteraction = 0
    # Vehicles
    @vehicles = []
    @vehicleInteraction = 0
    # Report any Error
    @error = 0
    @errorMsg = ''
    
    
  end
end

# Base class
class ObjectInfo
    def initialize()
      @name
      @isAnimated
    end
end

# Inheritance our base class
class PropInfo < ObjectInfo
    def initialize()
      @doors
    end
end
