#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 15 September 2013 (AP3)
# Purpose:
#			~ Reads Cutscene Info


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

path = File.expand_path $0
path = File.dirname(path)
require "#{path}/../../Utils/CutsceneGrader/GradingModel.rb"
require "#{path}/../../Utils/CutsceneGrader/GradingInfo.rb"




# Main Class the will read our CutInfo and generate the Stats we need

class GradingReader

  # Search for name in defination
  def initialize(log)
      @log = log
      #@modelDef = ModelInfo.new()
      # Stats Generator
      
      

  end

  def parse(metadataUtils, cutfile, cutname)    
      @log.message("processing tgis #{cutfile}")
      # Instance of our Cutscene Grading Information
      cutInfo = GradingInfo.new()
      
      # Get the name
      cutInfo.name = cutname
      
      puts 'Goat'
  
      begin 
        # Parse our Cutfile
        cutDef = metadataUtils.ParseMetaFile( cutfile )
        
        # Get Duration and conver to frames
        length = metadataUtils.FindFirstStructureNamed( cutDef, "fTotalDuration" ).value.to_f
        cutInfo.duration = length
        cutInfo.length = (length * 30).to_i 
        
        # GEt Model Info
        puts 'getting models'
        puts @modelDef
        _MetaArrayList = metadataUtils.FindFirstStructureNamed( cutDef, "pCutsceneObjects" )
        @modelDef.getModelNames(cutInfo, metadataUtils, _MetaArrayList)
      
      rescue => msg 
					puts msg
          # Log that the cutscene could not be processed
          cutInfo.error = 1
          cutInfo.errorMsg = msg.to_s
			end
      
      cutInfo


  end
end

