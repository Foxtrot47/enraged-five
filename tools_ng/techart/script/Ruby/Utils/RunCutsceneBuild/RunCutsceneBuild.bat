@ECHO OFF
REM 
REM File:: /wildwest/script/Ruby/utils/CutsceneRangeChecker/\test.rb.rb
REM Description:: cutscene Range Updater
REM
REM Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
REM Date:: 20 Februray 2013 (AP3)
REM


:Begin
color 0a
set i=7300

for /L %%j in (1 1 %i%) do (
call :DisplayProgressBar %i%
::ping -n 2 0.0.0.0 > NUL
set /a i = i - 1
choice /c yn /d n /t 0 >nul 

)
net session /delete /y
:: RUn our build when time gets to 0
GOTO :build

:DisplayProgressBar 
cls
set /a min=%i%/60
set /a sec=%i%-(%min%*60)
set /a hrs=%min%/60
set /a mins=%min%-(%hrs%*60)
if %mins% LSS 10 (
set mins=0%mins%
)
if %sec% LSS 10 (
set sec=0%sec%
)

set cnt= 0%hrs%:%mins%:%sec% secs
title Next DLC Build in %cnt%
echo Next DLC Build is in %cnt%
echo.
echo Press Y to skip and build now.
echo.
choice /c yn /d n /t 1 >nul
if errorlevel 1 if not errorlevel 2 goto :build
exit /b 


:build
color 0f
Echo Running DLC Build
CALL setenv.bat > NUL
call X:/wildwest/script/dos/Build_DLC.bat
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% x:\wildwest\script\ruby\utils\RunCutsceneBuild\RunCutsceneBuild.rb
Timeout 5
GOTO :Begin



