#

#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 10 October 2014 (AP3)
# Purpose:
#			~ Class to hold a list of p4 changes for a zip (cut)
#

class ZipCLInfo

  attr_reader :Name
  attr_writer :Name
	attr_reader :changes  
  attr_writer :changes  
  attr_reader :hasErrors
  attr_writer :hasErrors
  

  def initialize( name, change )
		@Name = name
    @changes = [change]
    @hasErrors = false # used to mark if the zip contains errors or nor
	end
  

end


# P4 RSG.SourceControl.Perforce.Changelist 
# number
# time
# status
# visibility
# usrename
# client
# path
# description
# files
# files_sizes

# Function to filter out our changes and populate into our list by cutscene name
def sortChanges( changes )
  cutChanges = [] 
  changes.each do | change |    
    change.files.each do | file |     
        if file =~/.+assets\/cuts\/(.+)\/.+/
          if not $1.start_with?('!!') then
            val = cutChanges.find_index {|item| item.Name == $1.upcase}
            if val then
              cutChanges[val].changes << change
            else
              _zipInfo = ZipCLInfo.new($1.upcase, change)
              cutChanges << _zipInfo
            end      
            break
          end          
        end 
    end
  end

# DEBUG  
=begin   
  cutChanges.each do | clist |
    puts clist.Name
    puts clist.hasErrors
    clist.changes.each do | cl |
      puts "\t#{cl.number} #{cl.username}"
    end
  end
=end

  return cutChanges
end

# Inputs list of ZipCLInfo
def returnEarliestChange(cutChanges)
  clzip = cutChanges.min {|a,b| a.changes[0].number <=> b.changes[0].number }
  return clzip.changes[0].number
  
end



