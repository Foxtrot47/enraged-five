# Email Utils

path = File.expand_path $0
path = File.dirname(path)

require 'fileutils'

@HTML_TEMPLATE = "X:/wildwest/etc/config/notificationTemplates/AssetBuildTemplate.html"
@HTML_CHANGE_TEMPLATE = "X:/wildwest/etc/config/notificationTemplates/AssetBuildChangesTemplate.html"
@HTML_PROJECTHEADER_TEMPLATE = "X:/wildwest/etc/config/notificationTemplates/AssetBuildProjectHeaderTemplate.html"
@HTML_CUTSCENEHEADER_TEMPLATE = "X:/wildwest/etc/config/notificationTemplates/AssetBuildCutsceneHeaderTemplate.html"

SUCCESS_EMAIL_LIST = ['*CutsceneAnimators@rockstarnorth.com',
                      'RSGNYC_Team_Camera@rockstargames.com',
                      'RSGLightingTeam@rockstargames.com']

FAILURE_EMAIL_LIST = ['Mark.Harrison-Ball@Rockstargames.com',
                      'dermot.bailie@rockstarnorth.com',
                      'geoffrey.fermin@rockstarnorth.com', 
                      'tina.chen@Rockstargames.com',
                      'Santi.Hurtado@rockstarnorth.com']
# default CC list	  
CC_EMAIL_LIST = ['Mark.Harrison-Ball@Rockstargames.com',
				  'dermot.bailie@rockstarnorth.com',
				  'tina.chen@Rockstargames.com',
				  'geoffrey.fermin@rockstarnorth.com', 
				  'tina.chen@Rockstargames.com',
				  'Santi.Hurtado@rockstarnorth.com']


def _sendEmail(emailStatus, subject, to_list, cc_list=nil, attachments=nil)
	html_text = File.read( @HTML_TEMPLATE )
	emailStatus.each_pair do |key, value|
		html_text = html_text.gsub(key, value)		
	end
	
	File.open("#{Dir.tmpdir()}/#{Time.now.strftime("%H_%M_%S")}_complete.html" , "w") do |f|     
		f.write(html_text)  
	end
	
	from = 	'Cutscene.Builder@Rockstargames.com'
	to_list = ['Mark.Harrison-Ball@Rockstargames.com'] if DEBUG
	puts 'sending email'
	send_email(from, to_list, subject, html_text, @server, cc_list, attachments  )
end

def sendStartEmail()
	subject = "[Cutscene.Builder] A Cutscene Asset Build has started"
	emailStatus = {
  '#BUILD#' => @buildtype.to_s,
	'#STATUS#' => 'Has Started',  
	'#STATUSCOLOR#' => 'Orange', 
	'#USER#' => 'Cutscene.Builder',
  '#COMPUTER#' => ENV["COMPUTERNAME"],
	'#DATE#' => (Time.now.strftime "%Y-%m-%d"),
	'#BUILDSTARTED#' => (Time.now.strftime "%H:%M:%S"),
	'#BUILDCOMPLETED#' => 'In Progress.....',
	'#BUILDTIME#' => '',
	'#LOGSFOLDER#' => '',
	'#CL#' => '',
  '#CLSYNC#' => '-',
  '#CHANGES#' => '',
	'#ERRORS#' => ''
	}
	_sendEmail(emailStatus, subject, FAILURE_EMAIL_LIST )	
end

def sendCompleteEmail(tStart, cl)
	subject = "[Cutscene.Builder] A Cutscene Asset Build has completed"
	emailStatus = {
  '#BUILD#' => @buildtype.to_s,
	'#STATUS#' => 'has Completed Succesfully',
	'#STATUSCOLOR#' => 'Green', 
	'#USER#' => 'Cutscene.Builder',
  '#COMPUTER#' => ENV["COMPUTERNAME"],
	'#DATE#' => (Time.now.strftime "%Y-%m-%d"),
	'#BUILDSTARTED#' => tStart.strftime("%H:%M:%S"),
	'#BUILDCOMPLETED#' => Time.now.strftime("%H:%M:%S"),
	'#BUILDTIME#' => Time.at(Time.now - tStart ).utc.strftime("%H:%M:%S"),
	'#LOGSFOLDER#' => @newest,
	'#CL#' => cl.to_s,
  '#CLSYNC#' => formatSyncFromCL(),
  '#CHANGES#' => formatChanges(),
	'#ERRORS#' => 'None'
	}
	
	
	
	_sendEmail(emailStatus, subject, ['Mark.Harrison-Ball@Rockstargames.com'] )
	_sendEmail(emailStatus, subject, SUCCESS_EMAIL_LIST, CC_EMAIL_LIST )
end

def sendCompleteNoUpdateEmail(tStart)
	subject = "[Cutscene.Builder] A Cutscene Asset Build has completed"
	emailStatus = {
  '#BUILD#' => @buildtype.to_s,
	'#STATUS#' => 'has Completed Succesfully',
	'#STATUSCOLOR#' => 'Green', 
	'#USER#' => 'Cutscene.Builder',
  '#COMPUTER#' => ENV["COMPUTERNAME"],
	'#DATE#' => (Time.now.strftime "%Y-%m-%d"),
	'#BUILDSTARTED#' => tStart.strftime("%H:%M:%S"),
	'#BUILDCOMPLETED#' => Time.now.strftime("%H:%M:%S"),
	'#BUILDTIME#' => Time.at(Time.now - tStart ).utc.strftime("%H:%M:%S") ,
	'#LOGSFOLDER#' => @newest,
	'#CL#' => 'NOTHING TO SUBMIT (No Data has changed)',
  '#CLSYNC#' => '-',
  '#CHANGES#' => '-',
	'#ERRORS#' => 'None'
	}
	_sendEmail(emailStatus, subject, FAILURE_EMAIL_LIST)
end

def sendFailourEmail(tStart, errorList = '')
	subject = "[Cutscene.Builder] A Cutscene Asset Build has failed!"
	emailStatus = {
  '#BUILD#' => @buildtype.to_s,
	'#STATUS#' => 'has Failed!',
	'#STATUSCOLOR#' => 'Red', 
	'#USER#' => 'Cutscene.Builder',
  '#COMPUTER#' => ENV["COMPUTERNAME"],
	'#DATE#' => (Time.now.strftime "%Y-%m-%d"),
	'#BUILDSTARTED#' => tStart.strftime("%H:%M:%S"),
	'#BUILDCOMPLETED#' => Time.now.strftime("%H:%M:%S"),
	'#BUILDTIME#' => Time.at(Time.now - tStart ).utc.strftime("%H:%M:%S") ,
	'#LOGSFOLDER#' => @newest,
	'#CL#' => 'NOT SUBMITTED!',
  '#CLSYNC#' => formatSyncFromCL(),
  '#CHANGES#' => '-',
	'#ERRORS#' => errorList
	}
  
  error_logs = []
  @errorList.each do | key, val |
    error_logs << key
  end
  
	_sendEmail(emailStatus, subject, FAILURE_EMAIL_LIST, cc_list=nil, error_logs )
end

# format our synced changelists
def formatSyncFromCL()
  formattedString = '<table width="100%" cellpadding="0" border="1" cellspacing="0"; style="font-family: Arial; font-size: 12px; border: 1pt solid black; padding:1px"  >'
  @DLC_Changes.each do | key, zipList |
    if zipList.count > 0 then
      formattedString += "<tr><td style='width: 100px; text-align: right; color: rgb(253, 253, 253); background-color: rgb(102, 102, 102);'>#{key}:</td><td style='color: Blue; background-color: white; padding-left: 10px;'>#{zipList[0].changes[0].number}</td></tr>"
    end
  end
  formattedString += "</table>"
  return formattedString
end

                                             

# format our errors for Mike Wilson so people can more easily understand the errors in teh email.
def formatError(errorList) 
  formattedString ='<table style="font-family: Arial; font-size: 12px;" cellpadding="0" cellspacing="0" >'
  
  errorList.each do | key, val |

    formattedString += "<tr><td style='color: Red; '>[#{val[0].first}]</td><td style='width:20px'></td><td style='color: DarkRed;'>#{val[0].second}</td></tr>"
    #formattedString +=  "<br>%-34s #{val.second}</br>" % "[#{val.first}]" #, val.second])
  end
  formattedString += "</table>"
  return formattedString
end


# format our synced changes to HTML
def formatChanges()
  html_changeTemplate = File.read( @HTML_CHANGE_TEMPLATE ) 
  html_projectHeaderTemplate = File.read( @HTML_PROJECTHEADER_TEMPLATE )  
  html_cutsceneHeaderTemplate = File.read( @HTML_CUTSCENEHEADER_TEMPLATE )
  formattedString = ''
  @DLC_Changes.each do | key, val |
    if val.count > 0 then
      html_Headertext = html_projectHeaderTemplate #copy our template
      html_Headertext = html_Headertext.gsub('#DLCNAME#', key.to_s)
      formattedString += html_Headertext
      val.each do | zip |
      
        html_cut = html_cutsceneHeaderTemplate
        html_cut = html_cut.gsub('#ZIPNAME#', zip.Name)
        
        changeString = ''        
        zip.changes.each do | change |      
          html_text = html_changeTemplate #copy our template
          html_text = html_text.gsub('#CHANGE#', change.number.to_s)	
          html_text = html_text.gsub('#CLTIME#', change.time.to_s)	
          html_text = html_text.gsub('#USER#', change.username.to_s)	
          html_text = html_text.gsub('#CLIENT#', change.client.to_s)	
          html_text = html_text.gsub('#DESC#', change.description.to_s)	
          
          changeString += html_text
        end
        
        html_cut = html_cut.gsub('#CLCHANGES#', changeString )
        formattedString += html_cut
        
        
        
      end
    end
  end
  return formattedString  
end