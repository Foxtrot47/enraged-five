#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 21 November 2014 (AP3)
# Purpose:
#			This is to parse the map scene interiors
#
# $NAME = GLobal Value
# @NAME = Ruby Instance Variables:
# @@NAME = Ruby Class Variables, must be initialized
# NAME = Ruby Constants, Constants defined within a class or module can be accessed from within that class or module, and those defined outside a class or module can be accessed globally

# read interior xml, then read the level containers
# pass in cutscene position and determine if its in the bound, so we need to do a bound check 
# For now we will just the Milo position and work on a bias on the number compared to the scene posiiton

#!/usr/bin/ruby
path = File.expand_path $0
path = File.dirname(path)

require 'System.Xml'
require 'System.Xml.Linq'
require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'


include System::Xml
include System::Xml::Linq
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal



#using_clr_extensions System::Linq
using_clr_extensions System::Xml::Linq
using_clr_extensions System::Xml::XPath

require "#{path}/../../Global/interior/interiorObject.rb"
require "#{path}/../../Global/project/project"
require "#{path}/../../Global/perforce/p4utils"

# logging
require "#{path}/../../Global/logging/logging.rb"

#Database
require "#{path}/../../Global/database/DatabaseConnection.rb"

#INTERIORS_XML_PATH = "#{@g_Config.project.DefaultBranch.Assets}/export/levels/#{@g_Config.project.name}/interiors/"

GTA_MILO = 'gta_milo'
GTA_MILOTRI = 'gta_milotri'
GTA_MLOROOM = 'gtamloroom'
GTA_REFOBJECT = 'rsrefobject'
GTA_CONTAINER = 'container'
POS_BIAS = 7

SERVER = "nycw-mhb-sql"

class Interiors
  attr_reader :interiorList 
  
  def initialize(g_Config)
    @g_Config = g_Config
    @interiorList = []
    @@sourceDB = "CutsceneStats_#{@g_Config.project.name.upcase}"
   
  end
  

  
  # parses teh containers and searches for milo's
  def parseXML2( xmlfile )
  
    puts "Loading: #{xmlfile}"
    
    begin
    
      @xmldoc = XmlDocument.new
      @xmldoc = XDocument::Load( xmlfile )
    
    rescue => e 
      puts e
      puts "skipping"
      return
    end
    
    
    _containerName = ''
    containObj = @xmldoc.Root.XPathSelectElement( 'objects/object' )
    if containObj.Attribute( XName::Get( "class" ) ).Value == GTA_CONTAINER then
      _containerName = containObj.Attribute( XName::Get( "name" ) ).Value.downcase
    end
    
    @xmldoc.Root.XPathSelectElements( 'objects/object/children/object' ).each {| elm |
      if elm.Attribute( XName::Get( "class" ) ).Value == GTA_REFOBJECT then
        objName = elm.Attribute( XName::Get( "name" ) ).Value.downcase
        if objName.include? "milo" then 
          _interioObj = InteriorObject.new()
          _interioObj.mapfilename = xmlfile          
          _interioObj.container = _containerName
          _interioObj.timestamp = File.mtime(xmlfile).strftime("%Y-%m-%d %H:%M:%S")
          
          puts xmlfile
          puts objName         
          puts _interioObj.timestamp
          
          pos = elm.XPathSelectElement( 'transform/object/position' )
          _x = pos.Attribute( XName::Get( "x" ) ).Value.to_i  
          _y = pos.Attribute( XName::Get( "y" ) ).Value.to_i
          _z = pos.Attribute( XName::Get( "z" ) ).Value.to_i
          puts "#{_x}, #{_y}, #{_z}" 
          _interioObj.position = [_x, _y, _z] 
          
          elm.XPathSelectElements( 'paramblocks/paramblock/parameter' ).each {| param |
            
            if param.Attribute( XName::Get( "name" ) ).Value.downcase == "objectname" then
              _interioObj.name = param.Attribute( XName::Get( "value" ) ).Value.to_s  
              _tempName = _interioObj.name.chomp("_int_milo_") 
              _tempName = _tempName.chomp("_int_main_milo_")               
              _tempName = _tempName.chomp("_main_milo_") 
              _tempName = _tempName.chomp("_milo_") 
              
              _interioObj.friendlyname = _tempName
              puts _interioObj.name.to_s
            elsif param.Attribute( XName::Get( "name" ) ).Value.downcase == "filename" then
              _interioObj.filename = param.Attribute( XName::Get( "value" ) ).Value  
              puts _interioObj.filename .to_s
                 
            end
          }
          interiorList << _interioObj
          
          
        end
      
      
      end
    }
    
    
  end
  
  def connectDB()
    @dBconnection.connect(SERVER, @@sourceDB)
  end
      
  def closeDB()
    @dBconnection.close()
  end
    
  def updateDatabase()
	puts "UPDATING DATABASE"
    @dBconnection = DatabaseConnection.new( nil )
    connectDB() 
    
    
    paramaters = {  '@ProjectID' => getProjectID(@g_Config.project.name.upcase)
    }
    result = @dBconnection.read_data('','GetCutscenePositions', paramaters ) 
    

    
    # THis will select all position data from a declared project
    #query = "Select [Cutscene].ShotlistID, [CutsceneName],[Shots].ShotName, [ShotPosition].ShotID,[CutType].IsConcat , [ShotPosition].*
    #FROM [Cutscene]
    #INNER JOIN [ShotParent]
    #ON [Cutscene].ShotlistID=[ShotParent].ShotlistID
    #INNER JOIN [ShotPosition]
    #ON [ShotPosition].ShotID =[ShotParent].ShotID
    #INNER JOIN [Shots]
    #ON [ShotPosition].ShotID =[Shots].ShotID
    #INNER JOIN [CutType]
    #ON [Cutscene].CutsceneTypeID = [CutType].CutsceneTypeID
    #WHERE [ProjectID] = #{project} AND [CutType].IsConcat = 0
    #order by [CutsceneName] asc
    #"
    #result = @dBconnection.read_data(query)
    
    
    
    
    
    
    ## Update our interiors
    interiorList.each do | interior |
		puts "UPDATING #{interior.name} "
    
      query = "DECLARE @interiorID INT
              IF NOT EXISTS(SELECT * FROM Interiors WHERE Name='#{interior.name}')   
              BEGIN
                INSERT INTO Interiors (Name, FriendlyName, [Filename], MapFilename, Container, [User], PosX, PosY, PosZ, [DateModified] )
                VALUES ('#{interior.name}',
                        '#{interior.friendlyname}',
                        '#{interior.filename}',
                        '#{interior.mapfilename}',
                        '#{interior.container}',
                        '#{interior.user}',                               
                         #{interior.position[0]},
                         #{interior.position[1]},
                         #{interior.position[2]},
                        '#{interior.timestamp}'
                        )	
                SET @interiorID = @@IDENTITY
              END
              ELSE
              BEGIN
                UPDATE Interiors                      	
                SET Name = '#{interior.name}',
                    FriendlyName = '#{interior.friendlyname}',
                    MapFilename = '#{interior.mapfilename}',
                    Container = '#{interior.container}',
                    [User] = '#{interior.user}',
                    PosX = #{interior.position[0]},
                    PosY = #{interior.position[1]},
                    PosZ = #{interior.position[2]},
                    [DateModified] = '#{interior.timestamp}'
                    WHERE Filename='#{interior.filename}'
                SET @interiorID = (SELECT [ID] FROM Interiors WHERE Name='#{interior.name}')          
              END 
              -- return ID Value  
              SELECT @interiorID              
              "
      begin
		  id = @dBconnection.read_data(query)[0]['']
		  interior.id =id.to_i
	  rescue => e 
		puts "ERROR: Can not udpate Interior #{interior.name}"
	  end
      

    end
    ##
	puts "Checking Interior locations"
    
    ## Check if our Interiors are at the same locations as our cutscene Shots
    result.each do | test |
      interiorList.each do | interior |
        if test["vOffset_X"].to_i.between?(interior.position[0]-7, interior.position[0]+7) then
          if test["vOffset_Y"].to_i.between?(interior.position[1]-7, interior.position[1]+7) then
            if test["vOffset_Z"].to_i.between?(interior.position[2]-7, interior.position[2]+7) then
                
                query = "UPDATE Shots
                        Set [InteriorID]=#{interior.id}
                        WHERE [ShotID]='#{test["ShotID"]}'"
                
                @dBconnection.read_data(query)
            
            end
          end
        end
      end      
    end
    

    
    closeDB()
  end
  

end


# get our change info from a single passed in CL
def parseChangefromCL( cl )
  #puts @p4.methods
  #f = @p4.get_changelistDescribe( cl )
  change = @p4.get_Change_from_CL( cl )
  if change != nil
    validfiles = @p4.findFilesinChange( change , '*.xml' ) 

    validfiles.each do | xmlfile |
      @mapInteriors.parseXML2( xmlfile.replace('//', 'x:/' )	)	
    end
    
    @mapInteriors.updateDatabase()    
  end
end


if ( __FILE__ == $0 ) then

  LogFactory.Initialize()
  LogFactory.CreateApplicationConsoleLogTarget( )
  g_Log = LogFactory.ApplicationLog
  # Config initialisation. 
  $g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )  
  @p4 = P4Utils.new(g_Log) 
  
  @glog = Logger.new( $g_Config , g_Log) 

	begin

    @mapInteriors = Interiors.new($g_Config)
    
    if ARGV.length == 0   
      @glog.message("Running in Directory mode...")      
      levelXmlPath = "#{$g_Config.project.DefaultBranch.Export}/levels/rdr3/"
      
      Dir.glob("#{levelXmlPath}/**/*.xml").each do | xmlfile |   
        @mapInteriors.parseXML2( xmlfile )		
      end
      
      @mapInteriors.updateDatabase()
      
    elsif ARGV.length > 0
      @glog.message("Running in Automation mode...")
      cl = ARGV[0].to_i
      parseChangefromCL( cl )
    
    end 
	rescue => e 
    @glog.errorEmail(e)
    raise
  end
	
end

