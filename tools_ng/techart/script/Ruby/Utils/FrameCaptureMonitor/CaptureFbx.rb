#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 03 December 2014 (AP3)
# Purpose:
#			~ Monitor any fbx files and add to the database
#     Additional Info
#     fbx size
#     REvision (included)


#-----------------------------------------------------------------------------
# Uses
#------

path = File.expand_path $0
path = File.dirname(path)


require 'pathname'

FBX_ANIMDIR = 'animation/cutscene/!!scenes/'

EXCLUDE_LIST = ['face','test', 'bck', '000', '001','002 ', 'lattice', 'stems', 'story']

# Get a list of files from Perforce
def checkFBXFiles( project )  
  fbxList = []
  
  p4Files = @p4.files( [File.join(project.Value.DefaultBranch.art, FBX_ANIMDIR, "...fbx")], '-e' )
  p4Files.each do | p4file |  
    # pass to an object
    fbxFileInfo = P4FbxFile.new( p4file ) 
    # only include fbx files that are not in our include list
    if not EXCLUDE_LIST.any? { |word| fbxFileInfo.path.include?(word) }
      fbxList << fbxFileInfo  
      #puts "Mission #{fbxFileInfo.mission} : Strand :#{fbxFileInfo.strand}" 
    end    
  end  
  return fbxList
  
end

# Class to hold data about the p4 file
class P4FbxFile
  attr_reader :name
  attr_reader :path
  attr_reader :revision
  attr_reader :time
  attr_reader :strand
  attr_reader :mission

  def initialize( p4file )
    @name = nil
    @path = p4file['depotFile'].downcase
    @revision = p4file['rev']
    @time = p4file['time']
    @strand = nil
    @mission = nil
    parse()    
  end
  
  def parse()
    # get filename without extension
    @name = File.basename( @path , '.fbx').upcase
    
    tokens = @path.split('/')
    
    # get Mission (First path after !!scenes
    index = tokens.index('!!scenes')
    @mission = tokens[index+1]   
        
    # get Strand name. should be the next one, might be a filename which in case strip of the extension
    @strand = File.basename(tokens[index+2], '.*')
    
  end


end