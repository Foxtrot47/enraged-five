#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 30 July 2015 (AP3)
# Purpose:
#			~ Used with the Automation Framework to monitor capture submits and process the data



#-----------------------------------------------------------------------------
# Uses
#-----

require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'

# Core
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::SourceControl::Perforce

path = File.expand_path $0
path = File.dirname(path)

require "#{path}/../../Global/perforce/p4utils"
require "#{path}/../../Global/email/email.rb"
require "#{path}/../../Global/project/project"
require "#{path}/../../Global/logging/logging.rb"
require "#{path}/../../Global/debug/debugUtils.rb"
#Capture Definition Object [CaptureRender]
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureInfoDef.rb"
#Frame Conversion processor
require "#{path}/../../Utils/FrameCaptureMonitor/ProcessFrames.rb"
#Capture Database Functions
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureDatabase.rb"
# Capture notification system
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureNotify.rb"

# Capture Path
FRAMECAPTURE_PATH = "automation/framecapture/"

DEBUG = false


# SYnc our p4 Folder
def syncFolder(folder)  
  # Sync the definiations files
  syncPaths = [folder]  
  # Batch up sync commands  
  @p4.sync(syncPaths,'')
end


# Process our frames if we find a capture
def processFrameCapture(project, p4Change)
  # sync file
  syncFolder(p4Change.path)
  
  #create an object to hold info about our capture
  capture = CaptureRender.new(p4Change, project, @p4)
  capture.projectID = @@ProjectID  
  file_record = @p4.get_file_info(p4Change.files[0], 'headRev')
  capture.revision = file_record['headRev']
  capture.dlcName = project.value.name if project.value.IsDLC == true

  # Build mp4 from Images
  outpath = @@processFrames.convertImageSequence(capture)
  capture.serverPath = outpath  
    
  # Print out Paths for debugging
  debugPrint(capture) if DEBUG   

  # Write File Info out to local file and Database. //TODO remove write info fucntion
  @@captureDB.update( capture, p4Change )
  
  # Email user that its been updated (Use template File)
  emailToUsers(capture, p4Change)
  
end


def processAllCaptures()
  frameCapturePath = "#{@g_Config.project.DefaultBranch.Assets}/#{FRAMECAPTURE_PATH}"
  #Dir.glob("#{frameCapturePath}/**/*.*").each do | xmlfile |   
  #      @mapInteriors.parseXML2( xmlfile )		
  #end

end

# get our change info from a single passed in CL
def parseChangefromCL( cl )
  # Add all our projects
  projectsList =  @g_Config.DLCProjects    
  projectsList.Add( @g_Config.Project.Name, @g_Config.Project)
  projectsList.each do | project |  
  
    change = @p4.get_Change_from_CL( cl )    
    processFrameCapture( project, change )
    
  end
end



#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

   begin    
      

    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
    @g_Log = LogFactory.ApplicationLog
    
    # Config initialisation. 
    @g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   
    
    #Local Logger
    @glog = Logger.new( @g_Config , @g_Log) 
    
    # P4 Helper CLass
    @p4 = P4Utils.new(@g_Log)     

    # Frame Processor
    @@processFrames = ProcessFrames.new()    
    
    # Get our project ID for the project name
    @@ProjectID = getProjectID(@g_Config.project.name)
   
    # Capture Database
    @@captureDB = CaptureDatabase.new( @@ProjectID, @glog )
    
    if ARGV.length == 0   
        @glog.message("Running in Directory mode...")
        processAllCaptures()
      
      elsif ARGV.length > 0
        @glog.message("Running in Automation mode...")
        cl = ARGV[0].to_i
        parseChangefromCL( cl )    
     end 
  



  rescue => e 
    @glog.errorEmail(e)
    raise
  end  
        
end