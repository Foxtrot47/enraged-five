#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 13 July (AP3)
# Purpose:
#			~ Send files for capture based on CL'



#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------



require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'

# Core
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::SourceControl::Perforce

path = File.expand_path $0
path = File.dirname(path)

require "#{path}/../../Global/perforce/p4utils"
require "#{path}/../../Global/email/email.rb"
require "#{path}/../../Global/logging/logging.rb"

#Capture Automation Process
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureProcessor.rb"


EXCLUDE_LIST = ['face','test', 'bck', '000', '001','002 ', 'lattice', 'stems', 'story']


def syncPrerequisites()
  @p4.sync(["#{@g_Config.project.config.tools_root}/techart/standalone/..."],'')
end


def get_p4_changelist( change, fbxList )
    validfiles = @p4.findFilesinChange( change, '*.fbx' )
    validfiles.each do | filename |
      # only include fbx files that are not in our include list
      if not EXCLUDE_LIST.any? { |word| filename.downcase.include?(word) }
        fbxList << filename
      end      
    end
end

def checkFBXChange( cl )
  fbxList = [] 
  change_records = @p4.get_changelistDescribe( cl )
  changes = @p4.get_changelists_from_changes( change_records )  
  changes.each do |change|
    get_p4_changelist( change, fbxList )
  end
  return fbxList 
end

# Used with Automation Framework
def sendFBXcapture( cl )
  syncPrerequisites()
  fbxList = checkFBXChange( cl ) 
  @@captureProcessor.trigger( fbxList )  
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then

   begin    
      

    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
    @g_Log = LogFactory.ApplicationLog
    
    # Config initialisation. 
    @g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   
    
    #Local Logger
    @glog = Logger.new( @g_Config , @g_Log) 
    
    # P4 Helper CLass
    @p4 = P4Utils.new(@g_Log) 
    
    # Capture Processor
    @@captureProcessor = CaptureProcessor.new(@g_Config.project,@glog )
    
    
  
    # pass in a CL and check if there is any FBX files to send for capture
    if ARGV.length > 0 
        sendFBXcapture( ARGV[0] ) 
    
    end

  rescue => e 
    @glog.errorEmail(e)
    raise
  end  
        
end