#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 09 January 2015 (AP3)
# Purpose:
#			Object to hold information about our Captures from the automation Capture framework

#-----------------------------------------------------------------------------
# Uses
#--

path = File.expand_path $0
path = File.dirname(path)

require 'pathname'


class CaptureRender
  
  # Range class, child of CaptureRenderObject
  class Range  
    attr_reader :in
    attr_reader :out
    attr_writer :in
    attr_writer :out
    
    def initialize(  )
      @in = -1
      @out = -1
    end
    
  end

  attr_reader :projectID
	attr_writer :projectID
  attr_reader :dlcName
	attr_writer :dlcName
	attr_reader :name
	attr_writer :name
	attr_reader :revision
	attr_writer :revision
	attr_reader :frames
	attr_writer :frames
	attr_reader :length
	attr_writer :length
	attr_reader :project
	attr_writer :project
	attr_reader :fbxRevision 
	attr_writer :fbxRevision
	attr_reader :localThumb
	attr_writer :localThumb
  attr_reader :localPath
	attr_writer :localPath
  attr_reader :serverPath
	attr_writer :serverPath
  attr_reader :userEmail
  attr_writer :userEmail

  attr_reader :fbxFile
  attr_reader :fbxChange
  attr_reader :fbxDescription
  attr_reader :mission
  attr_reader :strand

  attr_reader :range
	
	
	def initialize( p4Change, project, p4=nil )
    @p4 = p4
    @projectID = 0
    @dlcName = 'rdr3'
		@name = ''
    @strand = ''
    @mission = ''
		@revision = -1
		@frames = -1
		@length = -1
		@fbxRevision = -1
    @fbxFile = ''
    @fbxChange = -1
    @fbxDescription = ''
		@project = ''
		@localPath = ''
    @localThumb = ''
    @serverPath = ''    
    @range = Range.new()
    @userEmail = nil
		parsePath( p4Change, project )
	end
  
    
  
	
	def parsePath( p4Change, project )
    
		_tempfile = p4Change.path.to_s.gsub!("//","X:/")
		@localPath = _tempfile.gsub!("*","")
	
		# get base Path
		path = File.dirname(p4Change.path)		
		# parse our path and get the top node folder
		pn = Pathname.new(path)    
		base, filename = pn.split
		# set filename
		@name = filename	
    @project = project.value.name    
    # frames from change
    @frames = p4Change.files.count
    # length
    @length = @frames / 30.0
    # Parse the p4 description
    parserDesrip( p4Change.description )
   
 
	end	
  
  # Parse the description
  def parserDesrip( description )
    description.split("\n").delete_if(&:empty?).each do | line |
      #puts "line = #{line}"
      if line.include?('#')
        keyValue = line.split('=')
        if keyValue.count == 2
          case keyValue[0].delete(' ')
            when'#SOURCEFILE'
              @fbxFile = keyValue[1].gsub!("\\","/")
            when'#SOURCECHANGELIST'
              @fbxChange = keyValue[1].to_i
            when'#SOURCEREVISION'
              @fbxRevision = keyValue[1].to_i
            when'#RANGE'
              _ranges = keyValue[1].split('-')              
              @range.in = _ranges[0].to_i
              @range.out = _ranges[1].to_i           
          end
        end
        
      end
    end
    
    # if we get a valid description 
    if @fbxFile != ''
      tokens = @fbxFile.split('/')
      # get Mission (First path after !!scenes
      index = tokens.index('!!scenes')

      @mission = tokens[index+1]   
        
      # get Strand name. should be the next one, might be a filename which in case strip of the extension
      @strand = File.basename(tokens[index+2], '.*')
      
      if @fbxChange != -1
      
        recordset = @p4.get_changelistDescribe(@fbxChange)
        user = recordset[0].fields['user']
        @userEmail = @p4.get_user_email(user)
        
        @fbxDescription = recordset[0].fields['desc']
        
		
		puts userEmail
      end
    end
  
    
  end
  
end