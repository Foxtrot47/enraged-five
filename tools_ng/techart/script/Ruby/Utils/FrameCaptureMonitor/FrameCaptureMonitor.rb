#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 03 December 2014 (AP3)
# Purpose:
#			~ Monitor North's Frame Capture updates from Perforce
#     - WIll detect if new frames have been submitted and then create a video from it and copy to folder


#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------



require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'




# Core
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::SourceControl::Perforce



require 'mscorlib'
require 'System.Core'
require 'System.Xml'
require 'pathname'
#require 'json'


include System::Collections::Generic
include System::IO
include System::Diagnostics
include System::Xml
include System::Runtime::InteropServices
include System::Net
include System::Net::Sockets
include System::Net::Mail



path = File.expand_path $0
path = File.dirname(path)

#Global
require "#{path}/../../Global/project/project"
require "#{path}/../../Global/perforce/p4utils"
require "#{path}/../../Global/email/email.rb"
require "#{path}/../../Global/logging/logging.rb"
require "#{path}/../../Global/debug/debugUtils.rb"

#Capture Definition Object
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureInfoDef.rb"
#Frame Conversion processor
require "#{path}/../../Utils/FrameCaptureMonitor/ProcessFrames.rb"
#Capture Database Functions
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureDatabase.rb"
#Capture Automation Process
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureProcessor.rb"
# FBX Parser
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureFbx.rb"
# Capture parser (p4)
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureParser.rb"
# Capture notification system
require "#{path}/../../Utils/FrameCaptureMonitor/CaptureNotify.rb"



# Static Variables
DEBUG = false
REPEAT = true

# time
sleepTime = 60 * 5


# Parse our p4 changes
def parse_p4_review( projectsList ) 

  # Check for any capture requests
  #checkCaptureRequests()

  projectsList.each do | project |
      @glog.message("Checking #{project.Key}")
      
      # Check for any new FBX submits that might need captures
      #checkForNewFBXSubmits(project)
      
      # Check fo rany submitted captures and process them if found
      checkForCaptureSubmits(project)
  
  end
end


# Check perforce for any new captures that have been submitted
def checkForCaptureSubmits(project)
  checkPathForSubmits(project, '.jpg')
end

# Update any new FBX files submited into the database
def checkForNewFBXSubmits(project)
  if project.Key != 'gta5'
    fbxList = checkFBXFiles(project)
    @@captureDB.updateFbx(project.Key, fbxList) if fbxList.count > 0
  end  
end

# Check for any capture requests from the Database and trigger captures
def checkCaptureRequests()
  listScenes = @@captureDB.gerRequiredCaptures()  
  @@captureProcessor.trigger( listScenes )
end


#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    #g_Options = OS::Options::new( OPTIONS )
    #Need to enable the initlaize on the TOOLS NG Branch
    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
        @g_Log = LogFactory.ApplicationLog
    
    # Config initialisation. 
    @g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   
		@email = @g_Config.EmailAddress	
		@server= @g_Config.studios.this_studio.exchange_server 
    
    
    # Add all our projects
    projectsList =  @g_Config.DLCProjects    
    projectsList.Add( @g_Config.Project.Name, @g_Config.Project)
    
    # Get our project ID for the project name
    @@ProjectID = getProjectID(@g_Config.project.name)
    
    # Not used yet
    branch = @g_Config.Project.DefaultBranch
    
    @glog = Logger.new( @g_Config , @g_Log) 
      puts @g_Config.studios.this_studio
    @glog.message("[MONITOR] Using Server: #{@g_Config.studios.this_studio}")
    
    @p4 = P4Utils.new(@g_Log) 
    @@processFrames = ProcessFrames.new()
    @@captureDB = CaptureDatabase.new( @@ProjectID, @glog )
    @@captureProcessor = CaptureProcessor.new(@g_Config.project,@glog )
    # Ruby Global Logger
    
    
    # Get current CL number
    @currentCL = @p4.get_current_changelistnumber()
    @currentCL = 7090447   if DEBUG    #DEBUG TESTING
    
   
    begin
      loop do
        unless not REPEAT
        begin
          @glog.message('[MONITOR] Working in Automation mode')

          # Check for any Capture notifations
          parse_p4_review(projectsList)
          
          
          @currentCL = @lastCL

          @glog.message("[MONITOR] Current CL: #{@currentCL}")
          @glog.message("[MONITOR] Waiting for...#{sleepTime.to_s} seconds.......")
          sleep(sleepTime)
          
        rescue => e 
          @glog.errorEmail(e)
          raise
				end
			else
				begin
				  @glog.message('[MONITOR] Working in Single mode')
				  # Check if we have a grading request
          checkCaptureRequests()
				  parse_p4_review()
          

				  @glog.message('[MONITOR] Complete')
          
				rescue => e 
					@glog.errorEmail(e)
          raise
				end
				break			
			end
		end
        
    end
end



