#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 07 January 2015 (AP3)
# Purpose:
#			~ Utils to notfy users that a captrue has been compelted


#-----------------------------------------------------------------------------
# Uses
#---


path = File.expand_path $0
path = File.dirname(path)

# Capture Report, change to Project please
@HTML_TEMPLATE = "CaptureNorthTemplate.html"

# Capture completion Report
def generateP4Report(capture, change )
  html_text = File.read( File.join(@g_Config.project.config.tools_root,"techart","etc","config","notificationTemplates", @HTML_TEMPLATE)  )
  html_text = html_text.gsub("#NAME#", capture.name.to_s)  
  html_text = html_text.gsub("#PROJECT#", capture.dlcName.to_s) 
  html_text = html_text.gsub("#MISSION#", capture.mission.to_s)  
  html_text = html_text.gsub("#STRAND#", capture.strand.to_s)  
	html_text = html_text.gsub("#USER#", change.username.to_s)  
	html_text = html_text.gsub("#CHANGELIST#", change.number.to_s)
	html_text = html_text.gsub("#DATE#", change.time.to_s)
  html_text = html_text.gsub("#REVISION#", capture.revision.to_s)  
  html_text = html_text.gsub("#FBXFILE#", "...#{capture.fbxFile.split('!!scenes')[1].to_s}")
  html_text = html_text.gsub("#FBXCHANGE#", capture.fbxChange.to_s)
  html_text = html_text.gsub("#FBXREV#", capture.fbxRevision.to_s)
  html_text = html_text.gsub("#FBXDESC#", capture.fbxDescription.to_s)  
  html_text = html_text.gsub("#FRAMES#", capture.frames.to_s)   
  html_text = html_text.gsub("#MP4_LINK#", capture.serverPath.to_s)
  html_text = html_text.gsub("#MP4_PATH#", capture.serverPath.to_s)
  html_text = html_text.gsub("#RANGE#", "#{capture.range.in.to_s} - #{capture.range.out.to_s}")
  html_text = html_text.gsub("#IMAGE#", capture.localThumb )
  return html_text
end

# Send Email to users 
def emailToUsers(capture, p4Change)
  $FROM = 'mark.harrison-ball@rockstargames.com'
  $TO = ['mark.harrison-ball@rockstargames.com','sean.letts@rockstarsandiego.com','Jeffrey.Tamayo@rockstarsandiego.com','blake.buck@rockstargames.com','Wilmar.Luna@rockstargames.com','fraser.tomison@rockstarnorth.com','dermot.bailie@rockstarnorth.com']
  $TO = ['mark.harrison-ball@rockstargames.com'] if DEBUG
  $CC = ['mark.harrison-ball@rockstargames.com'] if DEBUG
  

  # get User for FB submit to notify them
  if capture.userEmail != nil and not DEBUG
    if not capture.userEmail.downcase.include?('technicolor.com')
      $FROM = capture.userEmail 
      $TO << capture.userEmail 
    end
  end

  
  msg = generateP4Report(capture, p4Change) 
  
  @server= @g_Config.studios.this_studio.exchange_server 
  
  begin
		send_email( $FROM, $TO, "FrameCapture Update:  #{capture.name}", msg, @server)
	rescue => e 
    @glog.errorEmail(e)
	end
		
end
