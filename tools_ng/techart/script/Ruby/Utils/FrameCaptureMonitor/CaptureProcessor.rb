#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 07 January 2015 (AP3)
# Purpose:
#			~ This will start a console and send a requested capture to the capture North Capture machine
#     ~ The AutomationRenderConsole.exe is a wrapper around the Tools automation framework


#-----------------------------------------------------------------------------
# Uses
#---

path = File.expand_path $0
path = File.dirname(path)

CAPTURE_MONITOR_PATH = "/techart/standalone/AutomationRenderConsole/AutomationRenderConsole.exe"

class CaptureProcessor
  def initialize( project, glog = nil )
    @@CaptureMoniterCmd = File.join(project.config.tools_root, CAPTURE_MONITOR_PATH)
    # //TODO: Defaulted to hardcoded path to run on RDR machine
    \
    #@@CaptureMoniterCmd = File.join("X:/gta5/tools_ng", CAPTURE_MONITOR_PATH)
    @@glog = glog
  end
  
  def trigger( listScenes )
    # Convert list of scenes to a string and pass to our capture tool   
    if listScenes.count == 0 then
      @@glog.warning("[PROCESSOR]\tNo captures found to process!")
      return
    end
  
    
    captureScenes = listScenes.join(" ")
    
    if File.file?(@@CaptureMoniterCmd)    
      startThread( captureScenes )
    else
      @@glog.error("[PROCESSOR]\tFile #{@@CaptureMoniterCmd} does not exist. Capture Aborted" ) if @@glog     
    end
  end
  
  # //TODO: CHnage to a proper Threaded solution
  def startThread( captureScenes )
    #Thread.new()
    cmd = "#{@@CaptureMoniterCmd} #{captureScenes}"

    @@glog.message("[PROCESSOR]\t#{cmd}" ) if @@glog        
    
    system(cmd)
    
  end
end

# TEST

#capProcessor = CaptureProcessor.new(nil, "x:/gta5/tools_ng")
#capProcessor.trigger(['ARAB_1_INT_P3_T03'])