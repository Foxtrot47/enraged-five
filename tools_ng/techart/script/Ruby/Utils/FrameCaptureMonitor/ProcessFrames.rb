

#!/usr/bin/ruby
path = File.expand_path $0
path = File.dirname(path)

require 'fileutils'
require 'pathname'

# Process our frames

require "#{path}/../../Utils/Image2mp4/Image2mp4"

#DIR_MP4_FOLDER = "N:/RSGNYC/Production/CaptureRenders_North/dlc_agentTrevor"
DIR_MP4_FOLDER = "//nycw-mhb-sql/Captures_North/"

# always our frams start with this
FRAMENAME = 'frame-000.jpg'

# This will take our frames and create mp4's from them
class ProcessFrames

  def initialize()
    $image2mp4 = Image2mp4.new()
  end
  
  def convertToWorkPath(p4filePath)
    _tempfile = p4filePath.gsub!("//","X:/")
    _tempfile = _tempfile.gsub!("*","")
    file = File.join(_tempfile, FRAMENAME)
    return file
  end
  
  def getProjectPath( path )
	#tokens = path.splt('/')
  end
  
  def createThumbnail( src, dst)
    # get count of our image files

    cnt = Dir["#{src}/*"].count { |file| File.file?(file) }
    # choose 1/3 of the image for thumbnail
    
    
    _thumb = Dir.entries(src)[cnt/3]
    #puts _thumb.methods
    FileUtils.cp(File.join(src,_thumb), dst)
    
    # convert image size?
  end
  
  
  
  def convertImageSequence( capture )
    # get folder name as this is our cutscene name
    #inputfile = convertToWorkPath(p4filePath.to_s)
    inputfile = File.join(capture.localPath, FRAMENAME)
  
    # get base Path
    path = File.dirname(inputfile)
     
    # parse our path and get the top node folder
    #pn = Pathname.new(path)
    #base, filename = pn.split
    
    #make our revision file
    outPath = File.join( DIR_MP4_FOLDER, capture.project, capture.name, capture.revision)
    #make a directory (recursive check)
    FileUtils.mkdir_p(outPath) unless File.exists?(outPath)
    # Create out output file
    outputFile = File.join(outPath,  "#{capture.name}.mov")
    
    createThumbnail( capture.localPath, File.join(outPath, "#{capture.name}.jpg") )
    #createThumbnail( path, File.join(DIR_MP4_FOLDER,filename, "#{filename}.jpg") )

    # Convert image sequence to mov
    $image2mp4.convert(inputfile, outputFile, capture.frames)
    return outputFile
  
  end
  

  
end

=begin
        # Example Usage

        p4Path = "//gta5_dlc/spPacks/dlc_agentTrevor/assets_ng/automation/framecapture/AGT_CIA2_INT_P9_T04/*"
        imageprocessor = ProcessFrames.new()
        imageprocessor.convertImageSequence(p4Path)
=end




