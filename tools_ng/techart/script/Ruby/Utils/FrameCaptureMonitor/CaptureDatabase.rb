#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 07 January 2015 (AP3)
# Purpose:
#			~ Upload capture info to Database

#-----------------------------------------------------------------------------
# Uses
#---

path = File.expand_path $0
path = File.dirname(path)

require "#{path}/../../Global/database/DatabaseConnection.rb"

SOURCE_DB = "CaptureRenders"
SERVER = "nycw-mhb-sql"

class CaptureDatabase
  def initialize( projectid, glog = nil )
    @@dBconnection = DatabaseConnection.new(glog)
    @@projectID = projectid
    @@glog = glog
  end
  
  # Update our Database with capture Info
  def update( capture, p4Change )
    @@dBconnection.connect(SERVER, SOURCE_DB)
    
    paramaters = {  '@ProjectID' => @@projectID,
                    '@Name' => capture.name.to_s,
                    '@DLC_Name' => capture.dlcName.to_s,
                    '@Filenname' => capture.localPath.to_s,
                    '@ServerPath' => capture.serverPath.to_s,
                    '@Length' => capture.frames.to_i,
                    '@CaptureRev' => capture.revision.to_i,
                    '@CaptureChangeList' => p4Change.number.to_i,
                    '@CaptureUpdated' => p4Change.time,
                    '@CaptureStatusID' => 0,
                    '@FBXFile' => capture.fbxFile,
                    '@FBXRev' => capture.fbxRevision,
                    '@Mission' => capture.mission,
                    '@Strand' => capture.strand,
                    '@FBXChangeList' => capture.fbxChange
    }
    
    paramaters.each do | param, value | 
      puts "\t#{param} : #{value}" if DEBUG
    end
 
    @@glog.warning( "[DATABASE] Updating Capture Info for: #{capture.name.to_s}" ) if @@glog
    @@dBconnection.read_data('','UpdateCaptureInfo', paramaters ) 
    @@dBconnection.close()
  end
  
  # Update our Database with any new FBX's
  def updateFbx( project, fbxList )
    @@dBconnection.connect(SERVER, SOURCE_DB)   
    
    fbxList.each do | fbxInfo |
         paramaters = {  '@ProjectID' => @@projectID,
                      '@FBXFile' => fbxInfo.path.to_s,
                      '@Name' => fbxInfo.name.to_s,
                      '@FBXRev' => fbxInfo.revision.to_i,
                      '@DLC_Name' => project,
                      '@Mission' => fbxInfo.mission,
                      '@Strand' => fbxInfo.strand
                      
          
      }      
      @@dBconnection.read_data('','AddFbxFileInfo', paramaters )     
    end
    
    @@dBconnection.close()
    
    
    
  end
  
  # Check for requested capture updates
  def gerRequiredCaptures(  )
  
    @@dBconnection.connect(SERVER, SOURCE_DB)
    
    paramaters = {  '@ProjectID' => @@projectID,
                    '@CaptureStatusID' => 1 
    } 

    @@glog.message( "[DATABASE] Checking Capture Status..." ) if @@glog
    
    dataTable = @@dBconnection.read_data('','GetCaptureStatus', paramaters ) 
        
    listScenes = []
    

    dataTable.each do | entry |
      listScenes.push( entry['Name'] )
      @@glog.warning( "[DATABASE] Queuing for Capture: #{entry['Name']}" ) if @@glog
    end
    
    # Set capture Status to In progress
    setCaptureStatus( listScenes, 2 )
      
    @@dBconnection.close() 

    # Return list of scenes to process
    return listScenes
    
  end

private  
  # Set our Capture Status
  def setCaptureStatus( listScenes, status )
  
    listScenes.each do | scene | 
  
      paramaters = {  '@ProjectID' => @@projectID,
                      '@Name' => scene,
                      '@CaptureStatusID' => status                    
      } 
      @@dBconnection.read_data('','SetCaptureStatus', paramaters ) 
      
    end
  end

end

# TEST CODE
#updateDB = CaptureDatabase.new(nil, 1)
#updateDB.gerRequiredCaptures()
