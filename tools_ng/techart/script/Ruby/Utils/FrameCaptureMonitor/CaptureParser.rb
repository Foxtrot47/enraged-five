#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 07 January 2015 (AP3)
# Purpose:
#			~ This will process our p4 file info about the capture


#-----------------------------------------------------------------------------
# Uses
#---

path = File.expand_path $0
path = File.dirname(path)

# Capture Path
FRAMECAPTURE_PATH = "assets_ng/automation/framecapture/"


#[OBSOLETE] now is stored on Database but keeping it enabled for now
# write info out to a txt file. Might make sense to write to an Database later
def writeInfoFile( capture )
  fileInfoPath = File.join( File.dirname(capture.serverPath), 'info.txt' )  
  file = File.new(fileInfoPath, "w")
  file.write("Name: #{capture.name}\n")
  file.write("Filename: #{capture.localPath}\n")
  file.write("Length: #{capture.frames}\n")
  file.write("Status: Complete\n")
  file.close()
end

# SYnc our p4 Folder
def syncFolder(folder)  
  # Sync the definiations files
  syncPaths = [folder]  
  # Batch up sync commands  
  @p4.sync(syncPaths,'')
end


# Check perforce branch for any captures that have come trhough
def checkPathForSubmits(project, fileType, toChangelist='#head')  
  # set the capture path for the passed in project
  capturePath = "#{project.Value.DefaultBranch.project.Root}/#{FRAMECAPTURE_PATH}" 

  lastCL, changes = @p4.parse_p4_review("#{capturePath}..#{fileType}", @currentCL, toChangelist)
  if @lastCL.to_i < lastCL.to_i
    @lastCL = lastCL  
  end
  
  changes.each do | change |   
    processFrameCapture(project, change)
  end
end

# Process our frames if we find a capture
def processFrameCapture(project, p4Change)
  # sync file
  syncFolder(p4Change.path)
  
  #create an object to hold info about our capture
  capture = CaptureRender.new(p4Change)
  capture.projectID = @@ProjectID  
  capture.revision = file_record = @p4.get_file_info(p4Change.files[0], 'headRev')
  capture.dlcName = project.value.name if project.value.IsDLC == true

  
	if DEBUG   
    debugPrint(capture)
	end
  
  # Build mp4 from Images
  outpath = @@processFrames.convertImageSequence(capture)
  capture.serverPath = outpath  
  
  # Write File Info out to local file and Database. //TODO remove write info fucntion
  writeInfoFile( capture )
  @@captureDB.update( capture, p4Change )
  
   # Email user that its been updated (Use template File)
  emailToUsers(capture, p4Change)
  
end


