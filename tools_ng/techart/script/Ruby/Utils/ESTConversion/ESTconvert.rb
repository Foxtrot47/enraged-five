#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 20 Februray 2013 (AP3)
# Purpose:
#			~ Parse EDL FIle and upload Data to CUtscen Database
#


path = File.expand_path $0
path = File.dirname(path)
require "#{path}/../../Global/time/timecode.rb"

#Database
require "#{path}/../../Global/database/DatabaseConnection.rb"



#-----------------------------------------------------------------------------
# Note: MetaData format may change in future
#-----------------------------------------------------------------------------


class AvidFileDef
    attr_reader :File
    attr_reader :HeaderInfo
    
    def initialize(file, headerInfo)
      @File = file
      @HeaderInfo = headerInfo      
    end
    
    
  
end

class AvidUpdate
    def initialize(g_Log)
      @g_Log = g_Log
      @dBconnection = DatabaseConnection.new(@g_Log)
    end
    
    #-----------------------------------------------------------------------------
    # DB Functions
    #-----------------------------------------------------------------------------
    def connectDB()
      sourceDB = "gradingStats"
      server = "nycw-mhb-sql"
      @dBconnection.connect(server, sourceDB)
    end
        
    def closeDB()
      @dBconnection.close()
    end

    
    def parseEDL(edlfile, headerinfo, projectID)

        # EDL PARSER AND TIME CONVERSION
        eDLparser = EDLParser.new( @g_Log )
        eDLObject = eDLparser.readEDL( edlfile )
        
        
        # Upload Data to Database        
        @dBconnection = DatabaseConnection.new( @g_Log )
        connectDB() 
        query = "   DECLARE @ShotID INT
                    DECLARE @UserID INT
                    IF EXISTS(SELECT * FROM Users WHERE UserName='#{headerinfo.user}')   
                      SET @UserID = (SELECT UserID FROM Users WHERE UserName='#{headerinfo.user}') 
                    ELSE
                    BEGIN
                      INSERT INTO Users (UserName)
                      VALUES ('#{headerinfo.user}')
                      SET @UserID = @@IDENTITY
                    END
        
        
                    IF EXISTS(SELECT * FROM AvidEdits WHERE AvidName='#{eDLObject.TITLE}' AND PROJECTID=#{projectID} )
                    UPDATE AvidEdits 
                    SET LastUpdated='#{headerinfo.time}',
                        UserName='#{headerinfo.user}',
                        Changelist='#{headerinfo.changelist}',
                        Revision='#{headerinfo.revision}',
                        UserID=@UserID,
                        @ShotID = EditID 
                    WHERE AvidName='#{eDLObject.TITLE}'
                    
                    ELSE
                    BEGIN
                      INSERT INTO AvidEdits (ProjectID, AvidName, UserName, LastUpdated, Changelist, Revision, UserID )
                      VALUES('#{projectID}',
                            '#{eDLObject.TITLE}',
                             '#{headerinfo.user}',
                             '#{headerinfo.time}',
                             '#{headerinfo.changelist}',
                             '#{headerinfo.revision}',
                             @UserID
                             )
                      SET @ShotID = @@IDENTITY  
                    END
                    
                    
                    SELECT @ShotID
                "
                
                
        cutID = @dBconnection.read_data(query)['']
        
        
        tab = "\t\t"
        query = "DELETE FROM AvidShots
          WHERE EditID='#{cutID}'
        "
        eDLObject.REELLIST.each do |entry|
           query += "           
                INSERT INTO AvidShots (EditID, SourceName, SourceIN, SourceOUT, EditIN, EditOUT, RangeIN, RangeOUT, Duration, MediaType, RangeType)
                Values (#{cutID},
                          '#{entry.Name}',
                          '#{entry.TimeCode.SourceIN.Time}',
                          '#{entry.TimeCode.SourceOUT.Time}',
                          '#{entry.TimeCode.EditIN.Time}',
                          '#{entry.TimeCode.EditOUT.Time}',
                          #{entry.TimeCode.SourceIN.Frames},
                          #{entry.TimeCode.SourceOUT.Frames},
                          #{entry.TimeCode.SourceDuration},
                          '#{entry.MediaType}',
                          '#{entry.RangeType}'
                        )           
           "  
           @g_Log.message("#{entry.Name}#{tab}#{entry.TimeCode.EditIN.Time}#{tab}#{entry.TimeCode.EditOUT.Time}")
        end
        
        @dBconnection.read_data(query)
        closeDB()

        
     
    end

end



#DEBUG test
if ( __FILE__ == NIL ) then
    require 'RSG.Base.dll'
    require 'RSG.Base.Configuration.dll'
    require 'RSG.Base.Windows.dll'
    require 'RSG.SourceControl.Perforce'
    require 'RSG.Metadata'
    include RSG::Base::Configuration
    include RSG::Base::Logging
    include RSG::Base::Logging::Universal
    include RSG::Base::OS
    include RSG::SourceControl::Perforce
    require "#{path}/../../Global/perforce/p4utils"
    require "#{path}/../../Global/debug/debugUtils.rb"
    def fileDepotPath(filename)
      localPath = nil
      if filename != nil
        # FIX FOR GTA in DEPOT PATH :(
        filename = filename.sub('depot/','')
        # Switch to X drive
        localPath = "x:/"+filename.split("//")[1]      
        localPath
      end
    end

    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
        @g_Log = LogFactory.ApplicationLog
    
    # Config initialisation. 
    @g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   
		@email = @g_Config.EmailAddress	
		@server= @g_Config.studios.this_studio.exchange_server # NY SEVER BLOCKS THIS SRUFF
		@g_Log.message("Using Server: #{@g_Config.studios.this_studio}")
    @p4 = P4Utils.new(@g_Log) 
    eDLparser = EDLParser.new( @g_Log )
    
    @currentCL = 4750442 
    @edlPath = @g_Config.project.DefaultBranch.Assets + "/non_final/cutscene/EDL/...edl"
    @lastCL, changes = @p4.parse_p4_review(@edlPath, @currentCL)
    changes.each do |change|
      @g_Log.message( "Changelist: #{change.Number} by #{change.Username}" )
      fileList = @p4.findFilesinChange( change, '*.edl' )
      
      fileList.each do | filename |
        localPath = fileDepotPath(filename)
        puts localPath
        
        eDLObject = eDLparser.readEDL( localPath )
        puts eDLObject
      
      end

    end
end
