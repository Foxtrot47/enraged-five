@ECHO OFF
REM 
REM File:: /wildwest/script/Ruby/utils/cutsceneRender/cutsceneRender.rb
REM Description:: cutscene Range Updater
REM
REM Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
REM Date:: 20 March 2014 (AP3)
REM

CALL setenv.bat > NUL
set CURRENT_DIR = %CD%
echo %CURRENT_DIR%
echo %~dp0
echo %CD%
echo %RS_TOOLSROOT%

CD CURRENT_DIR
CALL p4 sync x:\wildwest\script\ruby\...

echo on
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% x:\wildwest\script\ruby\utils\cutsceneRender\cutsceneRender.rb

PAUSE