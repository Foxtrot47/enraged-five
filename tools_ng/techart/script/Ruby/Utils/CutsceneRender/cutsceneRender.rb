require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.Base.Windows.dll'
require 'RSG.SourceControl.Perforce'
require 'RSG.Metadata'


# Core
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Base::OS
include RSG::SourceControl::Perforce
include RSG::Metadata

require 'pipeline/os/options'
include Pipeline

# This set our path up corrrectly
path = File.expand_path $0
path = File.dirname(path)
$roboRenderFolder = path.split("/")[0] + "/RoboRenders"

# P4 Utils
require "#{path}/../../Global/perforce/p4utils"


DEBUG = true
# Enable to run on a loop else run once
REPEAT = true


# time
sleepTime = 60 #* 5

# Files to look for
FBX = '*.fbx'
@head = '#head'

def setupRoboRenderFolder()
	if not File.exists?($roboRenderFolder)
		Dir.mkdir($roboRenderFolder)
	end
	queFolder = $roboRenderFolder + "/RenderQueue"
	if not File.exists?(queFolder)
		Dir.mkdir(queFolder)
	end
end

def runRender(fbxFilename)
  #@g_Log.message("What do we do now BLAKE with this file, is it valid???: #{fbxFilename}")
  fbxFilename = fbxFilename.upcase
  if not (fbxFilename.include? "/FACE/" or fbxFilename.include? "/PREVIZ/")
	basicName = fbxFilename.split("/")[-1]
	queFolder = $roboRenderFolder + "/RenderQueue"
	textFilePath = queFolder + "/" + basicName[0...-3] + 'txt'
	
	textFile = File.new(textFilePath, "w")
	textFile.puts fbxFilename
	textFile.close
  end
end


# Get teh local file path... Need to make this generic
def fileDepotPath(filename)
  localPath = nil
  if filename != nil
    # FIX FOR GTA in DEPOT PATH :(
    filename = filename.sub('depot/','')
    # Switch to X drive
    localPath = "x:/"+filename.split("//")[1]      
    localPath
  end
end


# review our submitted changes and do stuff
def parse_p4_review(thisFbxPath)

  

  @lastCL, changes = @p4.parse_p4_review(thisFbxPath, @currentCL, @head)
 
  changes.each do | change |    
    # Check each change, see if there has been a cutxml file submitted
    validfiles = @p4.findFilesinChange( change, FBX )
    
    validfiles.each do | filename |
      localPath = fileDepotPath(filename)
      runRender(localPath)
      # 
    end
    
  end
  
end

# USed to log errors
def logerror(e = 'unknown')
	msg = "Exception occured: #{e.message} in #{e.backtrace}"
	@g_Log.error(msg)
	#send_email( AUTHOR, EXCEPTION_EMAIL, 'Cutscene Report Tool Crash', msg, @server)
end

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    #g_Options = OS::Options::new( OPTIONS )
    #Need to enable the initlaize on the TOOLS NG Branch
    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
        @g_Log = LogFactory.ApplicationLog
    
    # Config initialisation. 
    @g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   
		@email = @g_Config.EmailAddress	
		@server= @g_Config.studios.this_studio.exchange_server 
		@g_Log.message("Using Server: #{@g_Config.studios.this_studio}")
    

    
    # Database/Stat Init
    @p4 = P4Utils.new(@g_Log) 
    @ProjectName = @g_Config.project.name.upcase
    
	
    branch = @g_Config.Project.DefaultBranch
   
    
    # Set our global Paths to Monitor
        
     
    
    
	#For now the below variable has been moved into the loop so we can search both projects.
    #@fbxPath = @g_Config.project.DefaultBranch.Art + "/animation/cutscene/!!scenes/...fbx"
	#@fbxPath = "x:/gta5_dlc/mpPacks/mpHeist/art/animation/cutscene/!!scenes/...fbx"


    # Get current CL number
		@currentCL = @p4.get_current_changelistnumber()
    
    # Uncomment this to test from a specific changelist
    @currentCL = 5817376
	#5817376 is from 7/22. 5772702 is 3ish on 7/11. 5745096 is on 7/8 at 3ish.  5584684 is on 6/13 #5537199 is from 6/4 at 3:20.  5509790 is from noon 5/29.
	#5495884 is from 5/27 around 3pm.  5446735 is from 5/14 at 2pm.  5399901 is from 6:30 5/5  . 5379717 is from 4/30 at 3:15 . 5377286 - Very early VDLC checkin
    
	# SETUP ROBORENDER FOLDER FOR QUE FILES
	setupRoboRenderFolder()

    begin
      loop do
        unless not REPEAT
        begin
          @g_Log.message('Working in Automation mode')
          # CHeck the DataBase to see if there is a full grading request
          #queryGradingRequest()
          # Check if any new data has been submitted to p4
          @g_Log.message("Current CL: #{@currentCL}")
          
		  rdrFbxPath = @g_Config.project.DefaultBranch.Art + "/animation/cutscene/!!scenes/...fbx"
		  vdlcMpHeistPath = "x:/gta5_dlc/mpPacks/mpHeist/art/animation/cutscene/!!scenes/...fbx"
		  vdlcSpTrevPath = "X:/gta5_dlc/spPacks/dlc_agentTrevor/art/animation/cutscene/!!scenes...fbx"
          parse_p4_review(rdrFbxPath)
		  parse_p4_review(vdlcMpHeistPath)
		  parse_p4_review(vdlcSpTrevPath)
          @currentCL = @lastCL

					
          @g_Log.message("Waiting for...#{sleepTime.to_s} seconds.......")
          sleep(sleepTime)
          
        rescue => e 
					logerror(e)
				end
			else
				begin
          @g_Log.message('Working in Single mode')
          # Check if we have a grading request
          
          #test()
          @g_Log.message('Complete')
          
				rescue => e 
					logerror(e)
				end
				break			
			end
		end
        
    end
end