@ECHO OFF
REM 
REM File:: /wildwest/script/Ruby/utils/CutsceneReporter/CutsceneReporter.rb
REM Description:: cutscene Range Updater
REM
REM Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
REM Date:: 20 Februray 2013 (AP3)
REM 

CALL setenv.bat > NUL
PUSHD %RS_PROJROOT%
set arg1=%1
echo on

CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%/techart/script/ruby/utils/P4Intergrator/P4Intergrator.rb %arg1%

::PAUSE