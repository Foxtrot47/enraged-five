


path = File.expand_path $0
path = File.dirname(path)

require 'RSG.Base.dll'
require 'RSG.Base.Configuration.dll'
require 'RSG.SourceControl.Perforce'
require 'rexml/document'

include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::SourceControl::Perforce
include REXML


require "#{path}/../../Global/perforce/p4utils"
require "#{path}/../../Global/email/email.rb"

# using NE server as NY is blocked

@server = 'rsgnweexg1.rockstar.t2.corp'
#@server = 'smtp.rockstar.t2.corp'


# time
sleepTime = 60 * 10
# Perforce Monitor PATHS
@p4Path = "X:/wildwest/dcc/motionbuilder2014/python/..."
_HTML_TEMPLATE = "etc/config/notificationTemplates/IntergrationTemplate.html"
_CONFIG = "etc/config/p4Intergrator/Intergration_config.xml"

@to_list = []
REPEAT = true






def intergrate_p4_file(filelist, change)

  puts "intergrating cl:#{change.number}"
  newPendCL = @p4.CreatePendingChangelist("Automated Integration for changelist #{change.number} from user:#{change.username} - #{change.description}")
  # add out files to the new changelist
  filelist.each do | file | 
    result = @p4.copy(['-c', "#{newPendCL.Number}",'-b','Wildwest_MB2014_to_MB2015', '-s',file],"-f")  
	result = @p4.copy(['-c', "#{newPendCL.Number}",'-b','Wildwest_MB2014_to_MB2016', '-s',file],"-f") 
  end
  # Should then check if there are any files in our submission, if not then rever 
  result = @p4.submit(['-c',"#{newPendCL.Number}"])

  generateP4Report(filelist, change, newPendCL)

end

# Parse a single CL for valid files to integrate
def get_p4_changelist( change )
  files = []
  
  validfiles = @p4.findFilesinChange( change, '*.py' )
  validfiles.each do | filename |
    files << filename      
  end
  intergrate_p4_file(files, change)# change.number, change.description, change.username)  
end

# Parse a list of changes to integrate
def parse_p4_review() 
  @lastCL, changes = @p4.parse_p4_review(@p4Path, @currentCL, @head)
  
  # Go through each change and integrate it over
  changes.each do | change | 
    get_p4_changelist( change )
  end
end

# get our change info from a single passed in CL
def parseChangefromCL( cl )
  change = @p4.get_Change_from_CL( cl )
  if change != nil
    get_p4_changelist( change )
  end
end

# Generate our p4 intergration report
def generateP4Report(filelist, change, newPendCL )
	
	# read our HTML Template file into text
    html_text = File.read( @HTML_TEMPLATE )
	html_text = html_text.gsub("#USER#", change.username.to_s)  
	html_text = html_text.gsub("#CHANGELIST#", change.number.to_s)
	html_text = html_text.gsub("#DATE#", change.time.to_s)
	html_text = html_text.gsub("#DESCRIPTION#", change.description.to_s)
	affected_file = ''
	filelist.each do | file | 
		affected_file += "#{file.to_s}<br/>"
	end
	html_text = html_text.gsub("#FILES#", affected_file.to_s)
	
	subject = "Wildwest Motionbuilder Intergrate submit: #{change.number.to_s} "
	from = @p4.get_user_email(change.username)
	@to_list << from
	send_email(from, @to_list , subject, html_text, @server )
end


def logerror(e = 'unknown')
	msg = "Exception occured: #{e.message} in #{e.backtrace}"
	@g_Log.error(msg)
  
	subject = "P4Intergrator has crashed"
	from = 'Notifications@rockstargames.com'
	to_list = ['Mark.Harrison-Ball@Rockstargames.com'] 
	send_email(from, to_list, subject, msg, @server )
  
end

# Read in a list of email address for us to send emails out too
def readEmailConfg()
	@g_Log.message("Reading Config #{@CONFIG}")
	if File.exist?( @CONFIG )
		xmlfile = File.new( @CONFIG ) 
		xmldoc = Document.new(xmlfile)	
		xmldoc.elements.each("P4IntergratorNotificationEmail/emailrecipients/emailrecipient") do | elm |
			@to_list << elm.attributes["email"]		
		end
	else
		@g_Log.error("Unable to find Config #{@CONFIG}")
		@to_list = ['Mark.Harrison-Ball@Rockstargames.com'] 
	end
end



#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    #g_Options = OS::Options::new( OPTIONS )
    #Need to enable the initlaize on the TOOLS NG Branch
    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
        @g_Log = LogFactory.ApplicationLog
    
    # Config initialisation. 
    @g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )   
    @p4 = P4Utils.new(@g_Log) 
     
    TECHART_ROOT = "#{@g_Config.project.config.tools_root}/techart"
    @HTML_TEMPLATE = "#{TECHART_ROOT}/#{_HTML_TEMPLATE}"
    @CONFIG = "#{TECHART_ROOT}/#{_CONFIG}"
    
    readEmailConfg()     
    
    begin    
      if ARGV.length == 0 
        # Automation mode
        @head = '#head' 
        @currentCL = @p4.get_current_changelistnumber()
        loop do
          unless not REPEAT
          begin
            @g_Log.message('Working in Automation mode')

            # Check if any new data has been submitted to p4
            parse_p4_review()
            @currentCL = @lastCL
            
            @g_Log.message("Current CL: #{@currentCL}")
            @g_Log.message("Waiting for...#{sleepTime.to_s} seconds.......")
            sleep(sleepTime)
          end
        end
      end
      # SIngle mode using Arg
      elsif ARGV.length > 0
        cl = ARGV[0].to_i
        parseChangefromCL( cl )
      end 
      
    rescue => e 
      logerror(e)
      raise
    end

     
     
  
  
end