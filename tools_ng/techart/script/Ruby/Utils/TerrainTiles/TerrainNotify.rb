#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 07 January 2015 (AP3)
# Purpose:
#			~ Utils to notfy users that a terrain fil ehas been updated


#-----------------------------------------------------------------------------
# Uses
#---

path = File.expand_path $0
path = File.dirname(path)

# Mission Ownser Doc
@MISSION_DOC = "RDR3_MissionOwners_HB.xlsx"

# affectedTiles Report, change to Project please
@HTML_TEMPLATE = "TilesCutsceneNotifyTemplate.html"
@HTML_HEADER_TEMPLATE = "TilesHeaderTemplate.html"
#@HTML_BODY_TEMPLATE = "TilesAffectedCutscenesTemplate.html"
@HTML_MISSION_OWNER_TEMPLATE = "TilesMissionOwnersTemplate.html"


# affectedTiles completion Report
def generateP4Report(affectedTiles, change )
  html_text = File.read( File.join($g_Config.project.config.tools_root,"techart","etc","config","notificationTemplates", @HTML_TEMPLATE)  )
  
  html_text = html_text.gsub("#PROJECT#", $g_Config.project.name.to_s)  
	html_text = html_text.gsub("#USER#", change.username.to_s)  
	html_text = html_text.gsub("#CHANGELIST#", change.number.to_s)
  html_text = html_text.gsub("#DESCRIPTION#", change.description.to_s)
	html_text = html_text.gsub("#DATE#", change.time.to_s)
  
  html_content = ''
  affectedTiles.each do | tileinfo |
    # TILE HEADER
    html_header = File.read( File.join($g_Config.project.config.tools_root,"techart","etc","config","notificationTemplates", @HTML_HEADER_TEMPLATE)  )
    html_header = html_header.gsub("#TILENAME#", tileinfo.tileName.to_s)  
    
    html_missionTemplate = File.read( File.join($g_Config.project.config.tools_root,"techart","etc","config","notificationTemplates", @HTML_MISSION_OWNER_TEMPLATE)  ) 
    
    html_body = ''; 
    tileinfo.cutsceneList.each do |key, value |
        if key != nil
          # MISSION CUTSCENES LIST          
          html_mlist = html_missionTemplate.gsub("#Mission#", value.mission.to_s)
          html_mlist = html_mlist.gsub("#Owner#", value.animator.to_s)
          # ADD ANIMATOR EMIL TO OUT EMAIL
          $TO << value.animatorEmail if not $TO.include?(value.animatorEmail)
          
          affected_file = ''
          value.cutsceneList.each do | val |
            affected_file += " #{val.to_s}<br/>"
          end          
          html_mlist = html_mlist.gsub("#CUTSCENES#", affected_file)
          
          html_body += html_mlist
        end
    end 
    
    html_header = html_header.gsub("#CUTSCENES#", html_body )
    html_content += html_header    
    
  end

  html_text = html_text.gsub("#FILES#", html_content)  

  return html_text
end

# Send Email to users 
def emailToUsers(affectedTiles, p4Change, submitEmail = nil)
  $FROM = 'mark.harrison-ball@rockstargames.com'  
  $TO = ['sean.letts@rockstarsandiego.com','Jeffrey.Tamayo@rockstarsandiego.com', 'charlene.dunn@rockstarsandiego.com','Wilmar.Luna@rockstargames.com', 'blake.buck@rockstargames.com','mark.harrison-ball@rockstargames.com', 'tina.chen@rockstargames.com','forrest.karbowski@rockstargames.com', 'felipe.busquets@rockstarnorth.com', 'dermot.bailie@rockstarnorth.com','francesca.howard@rockstarnorth.com']
  $TO = ['mark.harrison-ball@rockstargames.com'] if DEBUG
  $CC = ['Wilmar.Luna@rockstargames.com', 'blake.buck@rockstargames.com','mark.harrison-ball@rockstargames.com', 'tina.chen@rockstargames.com','forrest.karbowski@rockstargames.com' ] 
  $CC = ['mark.harrison-ball@rockstargames.com'] if DEBUG
  if submitEmail != nil then
    $FROM  = submitEmail
    $TO << submitEmail if not DEBUG
  end
  

  msg = generateP4Report(affectedTiles, p4Change) 
  
  @server= $g_Config.studios.this_studio.exchange_server 
  
  begin
    @glog.message("Sending email from #{$FROM} to #{$TO}");
		send_email( $FROM, $TO, "Cutscene Terrain update Update:  #{affectedTiles[0].tileName}", msg, @server )
	rescue => e 
    @glog.errorEmail(e)
	end
		
end