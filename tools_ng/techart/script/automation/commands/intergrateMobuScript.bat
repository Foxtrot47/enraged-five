@ECHO OFF
REM 
REM File:: /wildwest/script/Ruby/utils/CutsceneReporter/CutsceneReporter.rb
REM Description:: cutscene Range Updater
REM
REM Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
REM Date:: 20 Februray 2013 (AP3)
REM

CALL setenv.bat > NUL
set CURRENT_DIR = %CD%
echo %CURRENT_DIR%
echo %~dp0
echo %CD%
echo %RS_TOOLSROOT%


set arg1=%1

echo on
title P4Intergrator.rb (%RS_TOOLSROOT%\techart\script\ruby\utils\P4Intergrator\P4Intergrator.rb)
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\techart\script\ruby\utils\P4Intergrator\P4Intergrator.rb %arg1%

::PAUSE