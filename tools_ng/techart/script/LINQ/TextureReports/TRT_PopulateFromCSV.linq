<Query Kind="Statements">
  <Connection>
    <ID>b7871d32-5849-4657-85ed-76a8d335cb7b</ID>
    <Persist>true</Persist>
    <Server>rsgedisql3</Server>
    <Database>TechArt</Database>
  </Connection>
</Query>

// Specify the path to the .csv file.
string csvPath = "x:\\db\\aa_texture.csv";

// Load the data, converting type as necessary.
var csvTextureData = from row in MyExtensions.ReadFrom(csvPath)
		let columns = row.Split(',')
		select new
		{
 			assetClass = columns[0],
 			texturePath = columns[1],
 			textureName = columns[2],
			dimensions = columns[3],
			mips = Convert.ToInt32(columns[4].Substring(5)),
			format_swizzle = columns[5],
			pixelDataHash = columns[6].Substring(4),
			physicalSize_bytes = Convert.ToInt32(columns[8]),
			conversionFlagsStr = columns[9],
			templateTypeStr = columns[10],
			templateTypeStr2 = columns[11],
			minColour = columns[12],
			maxColour = columns[13],
			maxRGBDiff = Convert.ToDouble(columns[14])
		};


// Transpose data for insertion into relevant existing table.
foreach(var row in csvTextureData)
{
	TextureReportsTextures newTexture = new TextureReportsTextures
	{
		TRT_gameID = 1, // GTA5
		TRT_entryDate = DateTime.Now,
		TRT_buildVer = 80, // Update this when submitting data for each new build
		TRT_assetClass = row.assetClass,
		TRT_texturePath = row.texturePath,
		TRT_textureName = row.textureName,
		TRT_textureDimension = row.dimensions,
		TRT_textureMips = row.mips,
		TRT_textureFormat = row.format_swizzle,
		TRT_texturePixelDataHash = row.pixelDataHash,
		TRT_texturePhysicalSize = row.physicalSize_bytes,
		TRT_textureConversionFlagsStr = row.conversionFlagsStr,
		TRT_textureTemplateTypeStr = row.templateTypeStr,
		TRT_textureTypeStr2 = row.templateTypeStr2,
		TRT_textureMinColour = row.minColour,
		TRT_textureMaxColour = row.maxColour,
		TRT_textureMaxRGBDiff = row.maxRGBDiff
	};
	// Queue up insertion of each formatted row for speedier submit.
	TextureReportsTextures.InsertOnSubmit( newTexture );
}
// Submit all of the queued data.
SubmitChanges();