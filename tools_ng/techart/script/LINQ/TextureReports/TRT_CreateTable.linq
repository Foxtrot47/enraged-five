<Query Kind="SQL">
  <Connection>
    <ID>b7871d32-5849-4657-85ed-76a8d335cb7b</ID>
    <Persist>true</Persist>
    <Server>rsgedisql3</Server>
    <Database>TechArt</Database>
  </Connection>
</Query>

-- Delete the table if it already exists (highlight and selectively execute if needed).
-- if object_id('TextureReportsTextures') is not null drop table TextureReportsTextures

-- Create table for selected aa_texture.csv data
CREATE TABLE TextureReportsTextures
(
TRT_textureID int IDENTITY PRIMARY KEY,
TRT_gameID int not null,	-- The relevant game project
TRT_entryDate DATETIME2 not null,	-- Date the record was entered
TRT_buildVer int not null, 			-- Build version the data corresponds to
TRT_assetClass varchar(255),  			-- Column A from aa_texture.csv
TRT_texturePath varchar(1024), 			-- Column B
TRT_textureName varchar(255), 			-- Column C
TRT_textureDimension varchar(255), 		-- Column D
TRT_textureMips int, 					-- Column E
TRT_textureFormat varchar(255), 		-- Column F
TRT_texturePixelDataHash varchar(255), 	-- Column G
TRT_texturePhysicalSize int, 			-- Column I
TRT_textureConversionFlagsStr varchar(255), -- Column J
TRT_textureTemplateTypeStr varchar(255),	-- Column K
TRT_textureTypeStr2 varchar(255), 		-- Column L
TRT_textureMinColour varchar(255), 		-- Column M
TRT_textureMaxColour varchar(255), 		-- Column N
TRT_textureMaxRGBDiff float, 			-- Column O
)