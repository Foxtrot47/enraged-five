<Query Kind="Program">
  <Namespace>System</Namespace>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>System.ComponentModel</Namespace>
  <Namespace>System.Data</Namespace>
  <Namespace>System.Data.SqlClient</Namespace>
  <Namespace>System.IO</Namespace>
  <Namespace>System.Linq</Namespace>
  <Namespace>System.Linq.Expressions</Namespace>
  <Namespace>System.Reflection</Namespace>
</Query>

// Either copy this entire file to C:\Users\<username>\Documents\LINQPad Plugins\ or copy and paste the 
// ReadFrom(string file) method below into the MyExtensions class in your local version & and compile.

void Main()
{
	// Write code to test your extensions here. Press F5 to compile and run.
}

public static class MyExtensions
{
	// Write custom extension methods here. They will be available to all queries.
	
	public static IEnumerable<string> ReadFrom(string file) 
	{
		string line;
		using(var reader = File.OpenText(file))
		{
			while((line = reader.ReadLine()) != null) 
			{
				yield return line;
			}
		}
	}
}