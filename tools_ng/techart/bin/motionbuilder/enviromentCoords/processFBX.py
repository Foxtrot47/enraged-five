import sys

PointOrig = None

""" Overide our common loader as we don;t care about certain options"""
def LoadScene(sdkManager, scene, fileName):
    importer = FbxImporter.Create(sdkManager, "")    
    result = importer.Initialize(fileName, -1, sdkManager.GetIOSettings())
    if not result:
        return False
    
    if importer.IsFBX():
        sdkManager.GetIOSettings().SetBoolProp(EXP_FBX_MATERIAL, False)
        sdkManager.GetIOSettings().SetBoolProp(EXP_FBX_TEXTURE, False)
        sdkManager.GetIOSettings().SetBoolProp(EXP_FBX_EMBEDDED, False)
        sdkManager.GetIOSettings().SetBoolProp(EXP_FBX_SHAPE, False)
        sdkManager.GetIOSettings().SetBoolProp(EXP_FBX_GOBO, False)
        sdkManager.GetIOSettings().SetBoolProp(EXP_FBX_ANIMATION, False)
        sdkManager.GetIOSettings().SetBoolProp(EXP_FBX_GLOBAL_SETTINGS, True)
        sdkManager.GetIOSettings().SetBoolProp(EXP_FBX_CONSTRAINT, False)
        sdkManager.GetIOSettings().SetBoolProp(EXP_FBX_MODEL, True)        
    
    result = importer.Import(scene)
    importer.Destroy()
    return result


    
""""""    
def SearchHierarchy(pScene, findObject = None):
    lRootNode = pScene.GetRootNode()

    for i in range(lRootNode.GetChildCount()):
        result = SearchNodeHierarchy(lRootNode.GetChild(i), 0, findObject)
        print result 

def SearchNodeHierarchy(pNode, pDepth, findObject = None):
    global PointOrig
    
    if pNode.GetName() == findObject:
        PointOrig = pNode
        return pNode

    for i in range(pNode.GetChildCount()):
        SearchNodeHierarchy(pNode.GetChild(i), pDepth + 1, findObject)
        
  
"""
NOt required
"""
def DisplayGeometricTransform(pNode):
    print("    Geometric Transformations")

    # Translation
    lTmpVector = pNode.GetGeometricTranslation(FbxNode.eSourcePivot)
    print("        Translation: %f %f %f" % (lTmpVector[0], lTmpVector[1], lTmpVector[2]))

    # Rotation
    lTmpVector = pNode.GetGeometricRotation(FbxNode.eSourcePivot)
    print("        Rotation:    %f %f %f" % (lTmpVector[0], lTmpVector[1], lTmpVector[2]))

    # Scaling
    lTmpVector = pNode.GetGeometricScaling(FbxNode.eSourcePivot)
    print("        Scaling:     %f %f %f" % (lTmpVector[0], lTmpVector[1], lTmpVector[2]))
 

    
def DisplayNodeTransforms(pNode):
    '''
    Read in the offset properties
    '''
    lTrans = pNode.LclTranslation
    lProperty = FbxPropertyDouble4(lTrans)
    val = lProperty.Get()
    print
    lBuf = "Translation {0} {1} {2}".format(val[0], val[1], val[2])
    print lBuf

    # Get Parent Node as this holds the Rotation
    Node = pNode.GetParent()

    
    lRotat = Node.LclRotation
    lProperty = FbxPropertyDouble4(lRotat)
    val = lProperty.Get()
    print
    lBuf = "Rotation {0}, {1}, {2}".format(val[0], val[1], val[2])
    print lBuf      
    
        


#lScene = "x:/rdr3/art/animation/resources/Sets/BRT_2_INT_P1_T3.FBX"

if __name__ == "__main__":
    try:
        from FbxCommon import *
        
    except ImportError:
        print "Import Error"
        
    #First we will load the scene and
    print "Starting FBXSDK"
    lSdkManager, lScene = InitializeSdkObjects()

    # The example can take a FBX file as an argument.
    if len(sys.argv) > 1:
        print("\n\nFile: %s\n" % sys.argv[1])
        lResult = LoadScene(lSdkManager, lScene, sys.argv[1])
    else :
        lResult = False

        print("\n\nUsage: ImportScene <FBX file name>\n")  
        
    #print("\n\n---------\nHierarchy\n---------\n")
    SearchHierarchy(lScene, findObject = "PointOrig") 
    if PointOrig:
        #DisplayGeometricTransform(PointOrig)
        DisplayNodeTransforms(PointOrig)
    
    
        
    
        