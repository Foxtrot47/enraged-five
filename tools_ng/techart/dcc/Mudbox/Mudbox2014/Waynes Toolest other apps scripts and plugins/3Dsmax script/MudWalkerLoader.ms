--- smartmenu -- v.0.1 -- (c)2005 - andrei kletskov (111) - http://andklv.narod.ru

-- description:
--		this script scans a folder you define (see below) and creates a menu with found scripts
--		place this script in your "....\scripts\startup" folder to update menu each time you run 3dsmax

-- history: 
--		v0.1 (28 january 2005) - initial version
--used in this case to load the mudwalker scripts

(

------------------ defining a folder to scan 
---IMPORTANT!----- modify the wildcard path below to find your files
--								|
--								|
--								v
files = getFiles "C:\MudboxTemp\ReDucto\*.ms"
sort files

------------------ deleting previous menu
if ((menuMan.findMenu "MyScripts") != undefined) then 
(
	menuMan.unRegisterMenu (menuMan.findMenu "MyScripts")
	menuMan.updateMenuBar()
)
if ((menuMan.findMenu "MudWalkerX") != undefined) then 
(
	menuMan.unRegisterMenu (menuMan.findMenu "MudWalkerX")
	menuMan.updateMenuBar()
)


--------------------creating menu
mainMenuBar = menuMan.getMainMenuBar()
subMenu = menuMan.createMenu "MudWalkerX"
	
-------------------- creating run-maroscripts and populating menu
ii = 0
for i in files do 
(
	ii = ii + 1
	str = "macroscript smartmenu" + (ii as string) + " category:\"smartmenu\" buttontext:\"" + (getFilenameFile i) + "\" (execute (openFile \"" + i + "\"))"
	execute str	

	testItem = menuMan.createActionItem ("smartmenu" + (ii as string)) "smartmenu"
	subMenu.addItem testItem -1	
)

-----------------------updating menu
subMenuItem = menuMan.createSubMenuItem "Test Menu" subMenu
subMenuIndex = mainMenuBar.numItems() - 1
mainMenuBar.addItem subMenuItem subMenuIndex
menuMan.updateMenuBar()

)
