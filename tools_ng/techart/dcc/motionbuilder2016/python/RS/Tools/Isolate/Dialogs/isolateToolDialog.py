import os

import pyfbsdk as mobu

from RS.Tools import UI
from RS.Tools.Isolate import Core as isolateCore

from PySide import QtGui, QtCore


class IsolateTool(UI.QtMainWindowBase):
    """
        main Tool UI
    """
    def __init__(self):
        """
            Constructor
        """
        #Data 
        #self.isolateDataModel = QtGui.QStringListModel()
        self.isolateDataModel = QtGui.QStandardItemModel()
        self._toolName = "Isolate Tool"
        self.settingsArray = []

        # Main window settings
        super(IsolateTool, self).__init__(title=self._toolName, dockable=True)
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()

        # Create Widgets 
        banner = UI.BannerWidget(self._toolName)
        mainWidget = QtGui.QWidget()
        self.isolateButton = QtGui.QPushButton("Isolate selection")
        self.isolateDataListWidget = QtGui.QListView() 
        self.restoreButton = QtGui.QPushButton("Restore visibility")
        self.deteleteSettingsButton = QtGui.QPushButton("Delete settings")
        self.isolateFromNullButton = QtGui.QPushButton("Isolate from nullSettings")

        # Edit properties 
        #self.isolateDataListWidget.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

        self.isolateButton.setMinimumHeight(38)
        self.restoreButton.setMinimumHeight(38)
        self.isolateFromNullButton.setMinimumHeight(38)

        self.isolateDataListWidget.setModel(self.isolateDataModel)
        self._populateIsolateDataList()

        # Assign Layouts to WidgetStack
        mainLayout.addWidget(self.isolateButton)
        mainLayout.addWidget(self.isolateDataListWidget)
        mainLayout.addWidget(self.deteleteSettingsButton)
        mainLayout.addWidget(self.createSeparator())
        mainLayout.addWidget(self.restoreButton)
        mainLayout.addWidget(self.createSeparator())
        mainLayout.addWidget(self.isolateFromNullButton)

        mainWidget.setLayout(mainLayout)
        self.setCentralWidget(mainWidget)

        # callbacks
        self.isolateButton.pressed.connect(self._isolateSelection)
        self.restoreButton.pressed.connect(self._restoreSelection)
        self.deteleteSettingsButton.pressed.connect(self._deteleteSettings)
        self.isolateFromNullButton.pressed.connect(self._applyViewSettings)
        self.isolateDataModel.itemChanged.connect(self._editNullSettingName)

    def _editNullSettingName(self, attribute):
        """
            Internal Method

            Args:
                attribute is provided by qt isolateDataModel.itemChanged signal
        """
        itemCount = len(self.settingsArray)
        selectionIndex = self.isolateDataListWidget.selectionModel().currentIndex()
        model = mobu.FBFindModelByLabelName(self.settingsArray[selectionIndex.row()])

        if model is None:
            return
        
        self.settingsArray[selectionIndex.row()] = str(attribute.text())
        model.Name = str(attribute.text())

    def createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _deteleteSettings(self):
        """
            Internal Method

            delete the selected nullSetting in the UI both in scene and UI list.
        """
        self.ressourceSelectionModel = self.isolateDataListWidget.selectionModel()
        isolateDataModelName = self.ressourceSelectionModel.currentIndex().data()

        if isolateDataModelName is None:
            return

        isolateData = mobu.FBFindModelByLabelName(str(isolateDataModelName))
        
        if isolateData is None:
            return None

        isolateData.FBDelete()
        self._populateIsolateDataList()

    def _restoreSelection(self):
        """
            Internal Method

            Restore visibility setting from the selected nullSettings.
        """
        self.ressourceSelectionModel = self.isolateDataListWidget.selectionModel()
        isolateDataModelName = self.ressourceSelectionModel.currentIndex().data()
        if isolateDataModelName is None:
            return

        viewState = isolateCore.ViewState()        
        viewSettings = viewState.read(str(isolateDataModelName))
        if viewSettings is None:
            return 

        viewState.restore(viewSettings)

    def _applyViewSettings(self):
        """
            Internal Method

            Apply visibility setting from the selected nullSettings.
        """
        self.ressourceSelectionModel = self.isolateDataListWidget.selectionModel()
        isolateDataModelName = self.ressourceSelectionModel.currentIndex().data()
        if isolateDataModelName is None:
            return

        viewState = isolateCore.ViewState()        
        viewSettings = viewState.read(str(isolateDataModelName))
        if viewSettings is None:
            return 

        viewState.applySettings(viewSettings)

    def _isolateSelection(self):
        """
            Internal Method

            Save visibility setting from current scene selection and isolate them.
        """
        viewState = isolateCore.ViewState()
        viewState.isolateSelection()
        self._populateIsolateDataList()

    def filterModelType(self, modelType):
        """
            Internal Method

            Find visibility settings FBModels in the current scene.
            
            Args:
                modelType (type)[usually FBModelNull].
            
            returns (list).
        """
        isolateDataList = mobu.FBComponentList ()

        mobu.FBFindObjectsByName('*',
                                 isolateDataList,
                                 False,
                                 True)

        if len(isolateDataList)<1:
            return []

        nullArray = []
        for model in isolateDataList:
            if not isinstance(model, modelType):
                continue

            isolateStateProperty = model.PropertyList.Find('isolateStateBase64')

            if isolateStateProperty is None:
                continue

            nullArray.append(model.LongName)
        
        return nullArray

    def _populateIsolateDataList(self):
        """
            Internal Method

            Synchronize QStandardItemModel with the visibility settings in the current scene.
        """
        nullArray = self.filterModelType(mobu.FBModelNull)
        self.isolateDataModel.clear()

        self.settingsArray = list(nullArray)
        for ressource in nullArray :
            item = QtGui.QStandardItem(ressource)
            self.isolateDataModel.appendRow(item)

        self.ressourceSelectionModel = self.isolateDataListWidget.selectionModel()
        itemCount = len(nullArray)
        selectionIndex = self.isolateDataModel.index(itemCount-1,0) 
        self.ressourceSelectionModel.setCurrentIndex(selectionIndex , QtGui.QItemSelectionModel.Select)
