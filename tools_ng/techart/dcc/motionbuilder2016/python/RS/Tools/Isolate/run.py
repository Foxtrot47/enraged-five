from RS.Tools.Isolate.Dialogs import isolateToolDialog

def Run(show=True):
    tool = isolateToolDialog.IsolateTool()

    if show: 
        tool.show()

    return tool
