import base64
import json
import os
import tempfile
import zlib
import zipfile

import pyfbsdk as mobu


class ViewState(object):
    '''
        This class enables user to isolate part of a scene .
        
        A Json file compressed into a zip archive will be base64 encoded
        into a string attribute.
    '''
    def __init__(self):
        self.activeCamera = None         #(FBCamera)
        self.dataProperty = 'isolateChunk'
        self.json = None
        self.zip = None

    def collectModels(self):
        """
            Grab all available FBModel in your scene.
            
            returns(FBComponentList)
        """
        modelList = mobu.FBComponentList()
        mobu.FBFindObjectsByName('*',
                                 modelList,
                                 False,
                                 True)
        
        return modelList

    def extractVisibility(self, modelList):
        """
            Store visibility settings for provided modelList.

            Args:  
                modelList (FBComponentList): input models we want to process.

            returns(dict)
        """
        if len(modelList)<1:
            return None

        visibilityStates = {}

        for model in modelList:
            inheritsValue = model.PropertyList.Find('Visibility Inheritance').Data
            visibilityValue = model.PropertyList.Find('Visibility').Data
            showValue = model.PropertyList.Find('Show').Data
            selectionValue = model.Selected


            visibilityStates[model.LongName] = {'Visibility Inheritance':inheritsValue,
                                                'Visibility':visibilityValue,
                                                'Show':showValue,
                                                'Selected':selectionValue}

        return visibilityStates

    def expandTempPath(self, inputFilePath):
        """
            Remove tilde from local path in order to have a stable motionbuilder import.
           
            Args:  
                inputFilePath (str): source file path.

            returns (str)
        """
        filePath =  inputFilePath.replace('\\','/')

        if '~' in filePath:
            resultPath = ''
            pathSplit = filePath.split('/')
            startIndex = 0

            for tildeIndex in range(len(pathSplit)):
                if '~' in pathSplit[tildeIndex]:
                    startIndex = tildeIndex
                    break

            #os.path.expanduser doesn't seem to work on 'c:/cedric~.baz/data/temp/tempExportFile.fbx'
            resultPath = os.path.expanduser('~')
            resultPath =  resultPath.replace('\\','/')

            fixedPath = os.path.join(*pathSplit[startIndex+1:])
            resultPath = os.path.join(resultPath, fixedPath)

            filePath = os.path.realpath(resultPath)
        
        filePath = filePath.replace('\\','/')

        return filePath

    def packageData(self):
        """
            Prepare visibillity data and save them in a temporary zipFile.

            returns (str): output zip file path
        """
        modelList = self.collectModels()
        if modelList is None:
            return None

        jsonFolder = os.path.join(tempfile.gettempdir(), "mobuIsolateState")
        jsonFolder = self.expandTempPath(jsonFolder)

        if not os.path.exists(jsonFolder):
            os.makedirs(jsonFolder)
        
        jsonFile = os.path.join(jsonFolder, 'isolateStateData.jsn')
        jsonFile = self.expandTempPath(jsonFile)

        jsonData = self.extractVisibility(modelList)

        with open(jsonFile, 'w') as outputfile:
            json.dump(jsonData, outputfile, sort_keys=True, indent=4)

        compression = zipfile.ZIP_DEFLATED

        zipFile = os.path.join(jsonFolder, 'isolateStateData.zip')
        zipFile = self.expandTempPath(zipFile)

        with zipfile.ZipFile(zipFile, 'w',compression = zipfile.ZIP_DEFLATED) as isolateDataZip:
            isolateDataZip.write(jsonFile, 'isolateStateData.jsn')

        os.remove(jsonFile)

        return zipFile

    def write(self, zipSource):
        """
            encode the provided zip file into a base64 string attribute.
           
            Args:  
                zipSource (str): source zip file path.
        """
        zipData = None

        with open(zipSource, 'rb') as zipFile:
            zipData = zipFile.read()

        isolateState = base64.b64encode(zipData)
        
        sceneBaseName = os.path.basename(mobu.FBApplication().FBXFileName)
        sceneBaseName = os.path.splitext(sceneBaseName)[0]

        isolateData = mobu.FBModelNull('isolateData_{0}_1'.format(sceneBaseName))
        isolateStateProperty = isolateData.PropertyList.Find('isolateStateBase64')

        if isolateStateProperty is None:
            isolateStateProperty = isolateData.PropertyCreate('isolateStateBase64',  
                                                              mobu.FBPropertyType.kFBPT_charptr, 
                                                              'String', 
                                                              False, 
                                                              True,
                                                              None) 
        isolateStateProperty.Data = isolateState

        os.remove(zipSource)

    def read(self, isolateDataModelName):
        """
            encode the provided zip file into a base64 string attribute.
           
            Args:  
                isolateDataModelName (str):nullSettings longName.

            returns (json).
        """
        isolateData = mobu.FBFindModelByLabelName(isolateDataModelName)
        
        if isolateData is None:
            return None

        isolateStateProperty = isolateData.PropertyList.Find('isolateStateBase64')

        if isolateStateProperty is None:
            return None

        isolateState = base64.b64decode(isolateStateProperty.Data)

        zipFolder = os.path.join(tempfile.gettempdir(), "mobuIsolateState")
        zipFolder = self.expandTempPath(zipFolder)

        if not os.path.exists(zipFolder):
            os.makedirs(zipFolder)

        targetZipFile = os.path.join(zipFolder, "readIsolateState.zip")
        
        with open(targetZipFile, 'wb') as outputfile:
            outputfile.write(isolateState)

        #snippet from davidvega
        jsonData = []
        with zipfile.ZipFile(targetZipFile, "r") as zipfileHandle: 
             jsonData = [json.loads(zipfileHandle.read(jsonSource)) 
                                for jsonSource in zipfileHandle.namelist()] 

        os.remove(targetZipFile)

        return jsonData[0]

    def restore(self, jsonData):
        """
            restore visibily settings.
           
            Args:  
                jsonData (dict):dictionary loaded form a json structure.
        """
        if jsonData is None:
            return
        
        for modelName in jsonData.keys():
            model = mobu.FBFindModelByLabelName(str(modelName))

            if model is None:
                continue
            
            for attribute in jsonData[modelName].keys():
                if attribute == 'Selected':
                    continue
                model.PropertyList.Find(str(attribute)).Data = bool(jsonData[modelName][attribute])

    def applySettings(self, jsonData):
        """
            Hide elements based on the provided jsonData.
           
            Args:  
                jsonData (dict):dictionary loaded form a json structure.
        """
        if jsonData is None:
            return
        
        for modelName in jsonData.keys():
            model = mobu.FBFindModelByLabelName(str(modelName))

            if model is None:
                continue

            if jsonData[modelName]['Selected'] == False:
                model.Show = False
            else:
                model.Show = True
                model.Visibility = True

                model.PropertyList.Find('Visibility Inheritance').Data = jsonData[modelName]['Visibility Inheritance']
                model.PropertyList.Find('Visibility').Data = jsonData[modelName]['Visibility']
                model.PropertyList.Find('Show').Data = jsonData[modelName]['Show']

    def isolateSelection(self):
        """
            Hide elements from the current selection.
        """
        selectedModels = mobu.FBModelList()
        mobu.FBGetSelectedModels(selectedModels)

        if len(selectedModels)<1:
            return

        #Store visibilityData
        zipFile = self.packageData()
        self.write(zipFile)

        #now find unselected model to hide them
        unselectedModels = mobu.FBModelList()
        mobu.FBGetSelectedModels(unselectedModels,None,False,False)

        for model in unselectedModels:
            model.Show = False