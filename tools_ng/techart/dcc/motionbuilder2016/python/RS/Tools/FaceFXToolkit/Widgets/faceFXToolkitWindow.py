import pyfbsdk as mobu

from PySide import QtGui, QtCore
from RS.Tools import UI

import RS.Config
import RS.Perforce as p4
import RS.Tools.FaceFXToolkit.Widgets.faceFXUtils as ffx
import RS.Tools.FaceFXToolkit.Widgets.faceFXReport
import RS.Core.Face.ffxPosition
import RS.Core.Face.Lib

import os

lSys = mobu.FBSystem()

class FaceFXToolkitWindow(UI.QtBannerWindowBase):
    """
    Main window for ______ Tool
    """
    MOBU_SYSTEM = mobu.FBSystem()
    
    def __init__(self, parent=None):
        winWidth = 600
        winHeight = 500
        super(FaceFXToolkitWindow, self).__init__(  parent=parent,
                                                    title="FaceFX Toolkit",
                                                    size=[winWidth, winHeight],
                                                    dockable=False,
                                                    dialog=False,
                                                    store=True,
                                                    closeExistingWindow = True)
        self.setupUi()
    
    def _setupMenuBar(self):
        fileMenu = QtGui.QMenu("File")
        helpMenu = QtGui.QMenu("Help")
        
        saveExprMenuItem = QtGui.QAction("Save Ambient Expression File...", self)
        saveExprMenuItem.triggered.connect(self._handleSaveExprMenuItem)
        
        save3LExprMenuItem = QtGui.QAction("Save 3L Expression File...", self)
        save3LExprMenuItem.triggered.connect(self._handleSave3LExprMenuItem)
        
        makeFFXReportItem = QtGui.QAction("Generate FaceFX Report...", self)
        makeFFXReportItem.triggered.connect(self._handleMakeFFXReportItem)
        
        fileMenu.addAction(saveExprMenuItem)
        fileMenu.addAction(save3LExprMenuItem)
        fileMenu.addAction(makeFFXReportItem)
        
        self.menuBar.addMenu(fileMenu)
        self.menuBar.addMenu(helpMenu)
    
    def setupUi(self):
        """
        Setup for main UI.
        """
        # Style Presets
        btnHeight = 30
        stdMargin = 10
        stdSpacing = 5
        bigSpacing = 20
        self.setStyleSheet("QMenuBar {background: #ffdc19;"
                           "font: bold;"
                           "color: black;}"
                           )
        
        # Menu Bar
        self.menuBar = QtGui.QMenuBar()
        self._setupMenuBar()
        
        # Layouts
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(stdMargin, stdMargin, stdMargin, stdMargin)
        mainLayout.setSpacing(stdSpacing)
        namespaceLayout = QtGui.QHBoxLayout()
        exprLayout = QtGui.QHBoxLayout()
        loadAnimsLayout = QtGui.QVBoxLayout()
        convertLayout = QtGui.QVBoxLayout()
        pathMakerLayout = QtGui.QVBoxLayout()
        dlcLayout = QtGui.QHBoxLayout()
        convLayout = QtGui.QHBoxLayout()
        
        # Widgets
        detectBtn = QtGui.QPushButton("Get Selected Namespace")
        detectBtn.pressed.connect(self._handleDetectBtn)
        browseBtn = QtGui.QPushButton("...")
        browseBtn.pressed.connect(self._handleBrowseBtn)
        convertBtn = QtGui.QPushButton("Convert FFX to Ambient on all takes")
        convertBtn.pressed.connect(self._handleConvertBtn)
        convertBtn.setFixedHeight(btnHeight)
        convert3LBtn = QtGui.QPushButton("Convert FFX to 3L on all takes (Franklin)")
        convert3LBtn.pressed.connect(self._handleConvert3LBtn)
        convert3LBtn.setFixedHeight(btnHeight)
        setupFacialBtn = QtGui.QPushButton("Setup Facial File")
        setupFacialBtn.pressed.connect(self._handleSetupFacialBtn)
        loadAnimsBtn = QtGui.QPushButton("Load FaceFX Anims (use 'Rockstar > Animation > Anim2FBX' tool)")
        loadAnimsBtn.pressed.connect(self._handleLoadAnimsBtn)
        loadAnimsBtn.setEnabled(False)
        assignWavesToTakesBtn = QtGui.QPushButton("Assign Waves to Matching Takes")
        assignWavesToTakesBtn.pressed.connect(self._handleAssignWavesToTakesBtn)
        cleanTakesBtn = QtGui.QPushButton("Cleanup Takes")
        cleanTakesBtn.pressed.connect(self._handleCleanTakesBtn)
        toggleCtrlsBtn = QtGui.QPushButton("Toggle FaceFX Ctrl Layout")
        toggleCtrlsBtn.pressed.connect(self._handleToggleCtrlsBtn)
        printPathsBtn = QtGui.QPushButton("Print Paths for FBX Setup")
        printPathsBtn.pressed.connect(self._handlePrintPathsBtn)
        
        self.namespaceEdit = QtGui.QLineEdit()
        self.exprEdit = QtGui.QLineEdit()
        self.dlcEdit = QtGui.QLineEdit()
        self.dlcEdit.setText("mpSecurity")
        self.dlcAudioEdit = QtGui.QLineEdit()
        self.dlcAudioEdit.setText("DLC_SECURITY")
        self.convEdit = QtGui.QLineEdit()
        self.charEdit = QtGui.QLineEdit()
        
        
        
        self.messageBox = QtGui.QTextEdit()
        self.messageBox.setReadOnly(True)
        
        outputLbl = QtGui.QLabel("Output:")
        namespaceLbl = QtGui.QLabel("Namespace:")
        exprLbl = QtGui.QLabel("Expr File:")
        dlcLbl = QtGui.QLabel("Pack Name:")
        dlcAudioLbl = QtGui.QLabel("Audio Pack Name:")
        convLbl = QtGui.QLabel("Conversation Name:")
        charLbl = QtGui.QLabel("Character Name:")
        
        loadAnimsGrp = QtGui.QGroupBox("Scene Setup")
        convertGrp = QtGui.QGroupBox("FFX to Ambient")
        pathMakerGrp = QtGui.QGroupBox("Paths Generation")
        
        
        # Setup Layouts
        namespaceLayout.addWidget(namespaceLbl)
        namespaceLayout.addWidget(self.namespaceEdit)
        namespaceLayout.addWidget(detectBtn)
        
        exprLayout.addWidget(exprLbl)
        exprLayout.addWidget(self.exprEdit)
        exprLayout.addWidget(browseBtn)
        
        loadAnimsLayout.addWidget(setupFacialBtn)
        loadAnimsLayout.addWidget(loadAnimsBtn)
        loadAnimsLayout.addWidget(cleanTakesBtn)
        loadAnimsLayout.addWidget(assignWavesToTakesBtn)
        loadAnimsLayout.addWidget(toggleCtrlsBtn)
        loadAnimsGrp.setLayout(loadAnimsLayout)
        
        
        convertLayout.addLayout(namespaceLayout)
        convertLayout.addLayout(exprLayout)
        convertLayout.addSpacing(stdSpacing)
        convertLayout.addWidget(convertBtn)
        convertLayout.addWidget(convert3LBtn)
        convertGrp.setLayout(convertLayout)
        
        dlcLayout.addWidget(dlcLbl)
        dlcLayout.addWidget(self.dlcEdit)
        dlcLayout.addWidget(dlcAudioLbl)
        dlcLayout.addWidget(self.dlcAudioEdit)
        
        convLayout.addWidget(convLbl)
        convLayout.addWidget(self.convEdit)
        convLayout.addWidget(charLbl)
        convLayout.addWidget(self.charEdit)
        
        pathMakerLayout.addLayout(dlcLayout)
        pathMakerLayout.addLayout(convLayout)
        pathMakerLayout.addWidget(printPathsBtn)
        
        pathMakerGrp.setLayout(pathMakerLayout)
        
        mainLayout.addSpacing(stdSpacing)
        mainLayout.addWidget(loadAnimsGrp)
        mainLayout.addSpacing(stdSpacing)
        mainLayout.addWidget(convertGrp)
        mainLayout.addSpacing(stdSpacing)
        mainLayout.addWidget(pathMakerGrp)
        mainLayout.addSpacing(stdSpacing)
        mainLayout.addWidget(outputLbl)
        mainLayout.addWidget(self.messageBox)
        
        self.Layout.addWidget(self.menuBar)
        self.Layout.addLayout(mainLayout, 0)
        
        # Fill out default values
        self._handleDetectBtn()
        
        
        
        
    def _handleSaveExprMenuItem(self):
        namespace = self.namespaceEdit.text()
        if namespace:
            namespace += ":"
        projRoot = RS.Config.Project.Path.Root
        lFilePopup = mobu.FBFilePopup()
        lFilePopup.Caption = "Select an conversion file..."
        lFilePopup.Style = mobu.FBFilePopupStyle.kFBFilePopupSave
        lFilePopup.Filter = "*.csv"
        lFilePopup.Path = str(projRoot + "\\tools_ng\\techart\\etc\\config\\face\\ffxToAmbient")
        p4.Sync(os.path.normpath(lFilePopup.Path) + "\\...")
        
        # Get the GUI to show.
        lFilePopupGUI = lFilePopup.Execute()
        if lFilePopupGUI:
            exprFile = lFilePopup.FullFilename
            ffx.writeExprToFile(exprFile, namespace)
            lText = "Saved successfully!\n"
            lText += "{0}, {1}".format(namespace, exprFile)
            self.messageBox.setText(lText)
        else:
            lText = "Cancelled..."
            self.messageBox.setText(lText)
            
    
    def _handleSave3LExprMenuItem(self):
        namespace = self.namespaceEdit.text()
        if namespace:
            namespace += ":"
        projRoot = RS.Config.Project.Path.Root
        lFilePopup = mobu.FBFilePopup()
        lFilePopup.Caption = "Select an conversion file..."
        lFilePopup.Style = mobu.FBFilePopupStyle.kFBFilePopupSave
        lFilePopup.Filter = "*.csv"
        lFilePopup.Path = str(projRoot + "\\tools_ng\\techart\\etc\\config\\face\\ffxTo3L")
        p4.Sync(os.path.normpath(lFilePopup.Path) + "\\...")
        
        # Get the GUI to show.
        lFilePopupGUI = lFilePopup.Execute()
        if lFilePopupGUI:
            exprFile = lFilePopup.FullFilename
            ffx.write3LExprToFile(exprFile, namespace)
            lText = "Saved successfully!\n"
            lText += "{0}, {1}".format(namespace, exprFile)
            self.messageBox.setText(lText)
        else:
            lText = "Cancelled..."
            self.messageBox.setText(lText)
    
    def _handleMakeFFXReportItem(self):
        dlcName = self.dlcEdit.text()
        dlcAudioName = self.dlcAudioEdit.text()
        lText = RS.Tools.FaceFXToolkit.Widgets.faceFXReport.generateFFXReport(dlcName, dlcAudioName)
        self.messageBox.setText(lText)
        
        
    def _handleDetectBtn(self):
        ml = mobu.FBModelList()
        mobu.FBGetSelectedModels(ml)
        
        if len(ml) == 1:
            namespace = ml[0].LongName.split(":")[0]
            self.namespaceEdit.setText(namespace)
    
    def _handleSetupFacialBtn(self):
        RS.Core.Face.Lib.SetupFacialFile()
        self.messageBox.setText("Set up facial file successful.")
    
    def _handleLoadAnimsBtn(self):
        self.messageBox.setText("***WIP FEATURE***\n\nBrowse for folder...")
        
    def _handleAssignWavesToTakesBtn(self):
        for wav in lSys.Scene.AudioClips:
            wav.CurrentTake = None
            for take in lSys.Scene.Takes:
                if wav.Name.replace(".WAV","") == take.Name:
                    wav.CurrentTake = take
                    #wav.DstIn = mobu.FBTime(0,0,0,10)
        message = "Waves are now assigned to matching takes."
        message += "\nOffset all audio to start frame 10 (TEMP - still testing.)"
        self.messageBox.setText(message)
        
    def _handleCleanTakesBtn(self):
        deleteList = []
        message = "Cleaning takes..."
        
        # Delete take_001
        for lTake in lSys.Scene.Takes:
            if lTake.Name == "Take 001":
                deleteList.append(lTake)
        
        # Get list of duped takes
        for matchingTake in lSys.Scene.Takes:
            for processedTake in lSys.Scene.Takes:
                if "{0}.PROCESSED".format(matchingTake.Name) == processedTake.Name:
                    deleteList.append(matchingTake)
        
        # Remove the empty takes
        for emptyTake in deleteList:
            message += "Deleting: {0}".format(emptyTake.Name)
            emptyTake.FBDelete()
            
        # Rename the correct takes
        for processedTake in lSys.Scene.Takes:
            if ".PROCESSED" in processedTake.Name:
                processedTake.Name = processedTake.Name.replace(".PROCESSED","")
            else:
                message += "No changes: {0}".format(processedTake.Name)
                
        self.messageBox.setText(message)
        
    def _handleToggleCtrlsBtn(self):
        RS.Core.Face.ffxPosition.run()
        self.messageBox.setText("Toggling FFX Ctrl Layout...")
        
    def _handleBrowseBtn(self):
        projRoot = RS.Config.Project.Path.Root
        lFilePopup = mobu.FBFilePopup()
        lFilePopup.Caption = "Select an conversion file..."
        lFilePopup.Style = mobu.FBFilePopupStyle.kFBFilePopupOpen
        lFilePopup.Filter = "*.csv"
        lFilePopup.Path = str(projRoot + "\\tools_ng\\techart\\etc\\config\\face")
        p4.Sync(os.path.normpath(lFilePopup.Path) + "\\...")
        
        # Get the GUI to show.
        lFilePopupGUI = lFilePopup.Execute()
        if lFilePopupGUI:
            self.exprEdit.setText(lFilePopup.FullFilename)
            
            self.messageBox.setText("Conversion file selected...")
        else:
            self.messageBox.setText("Failed to browse for conversion file...")
        
    def _handleConvertBtn(self):
        namespace = self.namespaceEdit.text()
        if namespace:
            namespace+= ":"
            
        exprFile = self.exprEdit.text()
        ffx.convertFFXtoAmbient(exprFile,namespace)
        lText = "Converting FFX to Ambient on namespace: {0}\n".format(namespace)
        lText += "Using this expression:\n"
        lText += " {0}\n".format(exprFile)
        self.messageBox.setText(lText)
        
    def _handleConvert3LBtn(self):
        namespace = self.namespaceEdit.text()
        if namespace:
            namespace+= ":"
            
        exprFile = self.exprEdit.text()
        ffx.convertFFXto3L(exprFile,namespace)
        lText = "Converting FFX to Ambient on namespace: {0}\n".format(namespace)
        lText += "Using this expression:\n"
        lText += " {0}\n".format(exprFile)
        self.messageBox.setText(lText)
        
    def _handlePrintPathsBtn(self):
        dlcName = self.dlcEdit.text()
        dlcAudioName = self.dlcAudioEdit.text()
        fp = RS.Tools.FaceFXToolkit.Widgets.faceFXReport.ffxPaths(dlcName, dlcAudioName)
        
        lText = "*** This button is not working yet, but here are some useful paths:\n"
        lText += "\n\nCharacter Name: "
        lText += self.charEdit.text()
        lText += "\nConversation Name: "
        lText += self.convEdit.text()
        lText += "\n\nfacefx report spreadsheet:\n"
        lText += fp.csvFolderPath.replace("x:","/").replace("//gta5/", "//depot/gta5/")
        lText += "\n\nD* Sheets:\n"
        lText += fp.dStarSheets.replace("x:","/").replace("//gta5/", "//depot/gta5/")
        lText += "\n\nfacefx anim path:\n"
        lText += fp.expFfxPath.replace("x:","/").replace("//gta5/", "//depot/gta5/")
        lText += "\n\nwav path:\n"
        lText += fp.expWavPath.replace("x:","/").replace("//gta5/", "//depot/gta5/")
        lText += "\n\nexport path:\n"
        lText += fp.expExportPath.replace("x:","/").replace("//gta5/", "//depot/gta5/")
        lText += "\n\nfbx path:\n"
        lText += fp.expFbxPath.replace("x:","/").replace("//gta5/", "//depot/gta5/")
        lText += "\n\n\nList of contexts:\n\n"
        
        self.messageBox.setText(lText)
        