import csv
import os

import xml.etree.ElementTree as xml

import RS.Perforce as p4


class ffxPaths():
    def __init__(self, dlcName, dlcAudioName):
        self.dStarSheets = "x:/gta5_dlc/mpPacks/{0}/assets_ng/dialogue".format(dlcName)
        
        self.expFfxPath = "x:/gta5/audio/dev_ng/ASSETS/LIPSYNCANIMS/{0}/SCRIPTED_SPEECH".format(dlcAudioName)
        self.expWavPath = "x:/gta5/audio/dev_ng/ASSETS/Waves/{0}/SCRIPTED_SPEECH".format(dlcAudioName)
        self.expExportPath = "x:/gta5/art/ng/anim/export_for_audio/{0}/SCRIPTED_SPEECH".format(dlcAudioName)
        self.expFbxPath = "x:/gta5_dlc/mpPacks/{0}/art/ng/anim/source_fbx/FaceFX_Cleanup/SCRIPTED_SPEECH".format(dlcName)
        
        self.csvFolderPath = "x:/gta5/tools_ng/techart/etc/config/face/ffxReport"
        self.csvFilePath = os.path.join(self.csvFolderPath, "{0}.csv".format(dlcName))

def generateFFXReport(dlcName, dlcAudioName):
    
    guidMap = {
        "4d8a062c-9f25-41ce-b9a4-abca748c137b":"SFX",
        "db9705fb-ea64-43df-978d-5d1acbd6ff85":"FIX_FRANKLIN",
        "1b690f35-cb22-42b1-a31f-2d226d637925":"FIX_LAMAR",
        "54a4b327-538f-4fd0-abad-fffa56778090":"FIX_CROWD3",
        "fb3de595-2e13-4c53-9a9f-16e06bfac62b":"FIX_CROWD2",
        "0175df5f-1fcc-4c45-a94c-1e795825e14a":"FIX_CROWD1",
        "18ff04cb-e443-40be-b158-3aa7a169cc5d":"FIX_IMANI",
        "2a60b2fa-9aa0-49a9-a77a-6302185bbaae":"FIX_RICH1",
        "c316baea-31ba-4c68-9d01-eefdbf05610b":"FIX_GOLF2",
        "3917fb3d-0a5c-429e-aaf8-d5d84957ab04":"FIX_GOLF1",
        "47afbaa4-73a1-4a92-9063-2cdc9eaad501":"FIX_VAGOS3",
        "4849f34b-4701-465a-9192-51f6bd8f5279":"FIX_VAGOSL1",
        "9bcf8bd5-cca2-44b6-b2ac-184c5d305d87":"FIX_BALLASL1",
        "aa2bb26f-7be5-468b-bc1d-afbd41dcfda4":"FIX_GUNNS",
        "b2036b41-bb82-4810-b94a-7423fb0d9e63":"FIX_MARTIST1",
        "d4ea153f-6743-4bfc-bca6-68d1ee6d6609":"FIX_FAML1",
        "e33492fb-2de5-41b3-8b2d-b72e18ed100c":"FIX_SECURE1",
        "ea283a9d-3855-4d4d-9ff1-fbebe30a807b":"FIX_PROMOTER",
        "4a7c0a54-b924-4066-bca9-b38a981e40a7":"HS4_PRODUCER",
        "ddaa5a64-1f2a-4342-a4c5-67aa462fa668":"HS4_POOH",
        "e690c36f-3a10-46e1-9f4a-5be8f22a286b":"HS4_BUSINESS",
        "f8dc4b15-789e-4fed-ae17-e89fe104450d":"HS4_PARTYGIRL1",
        "f8dc4b15-789e-4fed-ae17-e89fe104450d":"HS4_PARTYGIRL1",
        "cccc3632-0d10-4f5c-8d03-9e73843f4f97":"MARNIE",
        "d7962999-8ed0-4bd1-89a8-45f3a56e86db":"JIMMYBOSTON",
        
    }
    
    bankNameList = {
        "FXIG":"FIXIG",
        "FXBA":"FXBA",
        "FXCUT":"",
        "FXDL0":"FXDL0",
        "FXDL1":"FXDL1",
        "FXDL2":"FXDL2",
        "FXDL3":"FXDL3",
        "FXDL4":"FXDL4",
        "FXDL5":"FXDL5",
        "FXFA":"FXFA",
        "FXFL":"FXFL",
        "FXFR":"FXFR",
        "FXIM":"FXIM",
        "FXLM":"FXLM",
    }
    fp = ffxPaths(dlcName, dlcAudioName)
    
    p4.Sync(fp.dStarSheets + "/...")
    p4.Sync(fp.csvFolderPath + "/...")
    p4.Sync(fp.expFfxPath + "/...")
    p4.Sync(fp.expWavPath + "/...")
    p4.Sync(fp.expExportPath + "/...")
    
    msg = "Updating FaceFX report.\n\n"
    msg+= "dlc: {0}\n".format(dlcName)
    msg+= "D* Sheets: {0}\n".format(fp.dStarSheets.replace("x:","/"))
    msg+= "fbx files: {0}\n".format(fp.expFbxPath.replace("x:","/"))
    msg+= "exported anims: {0}\n".format(fp.expExportPath.replace("x:","/"))
    msg+= "faceFX anims: {0}\n".format(fp.expFfxPath.replace("x:","/"))
    msg+= "waves: {0}\n".format(fp.expWavPath.replace("x:","/"))
    
    newCL = p4.CreateChangelist(msg)
    p4.Edit(fp.csvFilePath, newCL.Number)
    
    with open(fp.csvFilePath, 'wb') as csvFile:
        csvWriter = csv.writer(csvFile, delimiter=',')
        csvWriter.writerow(["dialogue sheet","description","character","filename","dialogue","root","placeholder","audio",
                                "ffx Anim","exists","wav","exists","export","exists",
                                #"fbx","exists",
                                ])
    
        for lFileName in os.listdir(fp.dStarSheets):
            if lFileName.endswith(".dstar"):
                dstarFilePath = os.path.join(fp.dStarSheets, lFileName)
                
                dstarTree = xml.parse(dstarFilePath)
                dstarRoot = dstarTree.getroot()
                convsTags = dstarRoot.findall("Conversations")
                for convsTag in convsTags:
                    convTags = convsTag.findall("Conversation")
                    for convTag in convTags:
                        linesTags = convTag.findall("Lines")
                        for linesTag in linesTags:
                            lineIndex = 1
                            prevName = ""
                            lineTags = linesTag.findall("Line")
                            for lineTag in lineTags:
                                
                                speaker = lineTag.get("speaker").encode('utf-8').strip()
                                guid = lineTag.get("guid").encode('utf-8').strip()
                                
                                if speaker != "0":
                                    if guid in guidMap:
                                        character = guidMap[guid]
                                    else:
                                        character = guid
                                        
                                    # Conversation items
                                    description = convTag.get("description").encode('utf-8').strip()
                                    root = convTag.get("root").encode('utf-8').strip()
                                    placeholder = convTag.get("placeholder").encode('utf-8').strip()
                                    
                                    # File specific items
                                    filename = lineTag.get("filename").encode('utf-8').strip()
                                    dialogue = lineTag.get("dialogue").encode('utf-8').strip()
                                    audio = lineTag.get("audio").encode('utf-8').strip()
                                    
                                    # Other
                                    recorded = lineTag.get("recorded").encode('utf-8').strip()
                                    sent = lineTag.get("sent").encode('utf-8').strip()
                                    timestamp = lineTag.get("timestamp").encode('utf-8').strip()
                                    
                                    # Expected paths
                                    prefix = filename.split("_")[0]
                                    bankName = bankNameList[prefix]
                                    if filename == prevName:
                                        lineIndex+=1
                                    else:
                                        prevName = filename
                                        lineIndex = 1
                                        
                                    filenameFinal = "{0}_{1}".format(filename, str(lineIndex).zfill(2))
                                    ffxAnim = os.path.join(fp.expFfxPath, bankName, character, "{0}.anim".format(filenameFinal)).replace("\\","/")
                                    wavFile = os.path.join(fp.expWavPath, bankName, character, "{0}.WAV".format(filenameFinal)).replace("\\","/")
                                    exportAnim = os.path.join(fp.expExportPath, bankName, character, "{0}.anim".format(filenameFinal)).replace("\\","/")
                                    
                                    csvWriter.writerow( [lFileName, description, character, filenameFinal, dialogue, root, placeholder,audio,
                                                            ffxAnim, os.path.isfile(ffxAnim),
                                                            wavFile, os.path.isfile(wavFile),
                                                            exportAnim, os.path.isfile(exportAnim),
                                                            ] )
                                                            
    return "exported to:\n{0}\n\nChecked out in CL {1}".format(fp.csvFilePath, newCL.Number)
                                
                                
#generateFFXReport()