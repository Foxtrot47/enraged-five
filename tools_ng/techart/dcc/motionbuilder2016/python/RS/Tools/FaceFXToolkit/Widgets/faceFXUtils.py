import pyfbsdk as mobu
import csv

lSys = mobu.FBSystem()

# Supplies frame numbers for +1 and -1 ffx pose
FFXCtrls = {
    "open_pose":[5,10],
    "W_pose":[20,25],
    "ShCh":[35,40],
    "PBM":[50,55],
    "FV":[65,70],
    "wide_pose":[80,85],
    "IH":[95,100],
    "Brow_Up_L":[110,115],
    "Brow_Up_R":[125,130],
    "Brows_Down":[140,145],
    "Squint":[155,160],
    "tBack_pose":[175,180],
    "tTeeth_pose":[185,190],
    "tRoof_pose":[195,200],
}

# Controllers to consider for pose
AmbientCtrls = [
    "CTRL_Jaw",
    "CTRL_LowerLip",
    "CTRL_UpperLip",
    "CTRL_L_Mouth",
    "CTRL_Mouth",
    "CTRL_R_Mouth",
    "CTRL_LowerLip_Curl",
    "CTRL_UpperLip_Curl",
    "CTRL_L_Cheek",
    "CTRL_R_Cheek",
]

threeLCtrls = [
    "CIRC_stickyLips_A",
    "CIRC_thinThick_G",
    "CIRC_thinThick_H",
    "CIRC_thinThick_F",
    "CIRC_thinThick_B",
    "CIRC_stickyLips_E",
    "CIRC_thinThick_D",
    "CIRC_thinThick_C",
    "CIRC_openCloseUL",
    "CIRC_squintInnerDL",
    "CIRC_squintInnerUL",
    "CIRC_openCloseDL",
    "CIRC_openCloseUR",
    "CIRC_squintInnerDR",
    "CIRC_squintInnerUR",
    "CIRC_openCloseDR",
    "CIRC_blinkR",
    "CIRC_squeezeL",
    "CIRC_squeezeR",
    "CIRC_blinkL",
    "CIRC_mouthSuckDL",
    "CIRC_mouthSuckUL",
    "CIRC_funnelDL",
    "CIRC_funnelUL",
    "CIRC_lipBite",
    "CIRC_suckPuffL",
    "CIRC_suckPuffR",
    "CIRC_dimpleL",
    "CIRC_dimpleR",
    "CIRC_pressL",
    "CIRC_pressR",
    "CIRC_mouthSuckDR",
    "CIRC_mouthSuckUR",
    "CIRC_funnelDR",
    "CIRC_funnelUR",
    "CIRC_oh",
    "CIRC_puckerL",
    "CIRC_puckerR",
    "CIRC_closeOuterL",
    "CIRC_closeOuterR",
    "CIRC_chinRaiseLower",
    "CIRC_chinRaiseUpper",
    "CIRC_chinWrinkle",
    "CIRC_lipsStretchOpenL",
    "CIRC_lipsStretchOpenR",
    "CIRC_lipsNarrowWideL",
    "CIRC_lipsNarrowWideR",
    "CIRC_scream",
    "CIRC_frownL",
    "CIRC_frownR",
    "CIRC_openSmileL",
    "CIRC_openSmileR",
    "CIRC_smileL",
    "CIRC_smileR",
    "CIRC_nasolabialFurrowR",
    "CIRC_nasolabialFurrowL",
    "CIRC_backFwd",
    "CIRC_clench",
    "CIRC_narrowWide",
    "CIRC_rollIn",
    "CIRC_press",
    "cheek_R_CTRL",
    "cheek_L_CTRL",
    "nose_R_CTRL",
    "nose_L_CTRL",
    "eye_C_CTRL",
    "eye_R_CTRL",
    "eye_L_CTRL",
    "innerBrow_R_CTRL",
    "outerBrow_L_CTRL",
    "innerBrow_L_CTRL",
    "outerBrow_R_CTRL",
    "tongueInOut_CTRL",
    "tongueRoll_CTRL",
    "tongueMove_CTRL",
    "jaw_CTRL",
    "mouth_CTRL",
]

class Pose():
    def __init__(self, lList):
        self.TX = lList[0::2]
        self.TY = lList[1::2]
    
    def isNonZero(self):
        isNonZero = False
        for val in self.TX:
            if float(val) != 0:
                isNonZero = True
        for val in self.TY:
            if float(val) != 0:
                isNonZero = True
        return isNonZero
        
    def addPose(self, lPose):
        for i in range(len(self.TX)):
            self.TX[i] = self.TX[i] + lPose.TX[i]
            self.TY[i] = self.TY[i] + lPose.TY[i]
            
    def clampPose(self, lMin, lMax):
        for i in range(len(self.TX)):
            self.TX[i] = max(min(self.TX[i],lMax),lMin)
            self.TY[i] = max(min(self.TY[i],lMax),lMin)
            

def getCtrlFromName(lName, lNamespace):
    lCtrl = mobu.FBFindModelByLabelName("{0}{1}".format(lNamespace, lName))
    if lCtrl:
        return lCtrl

def getAnimNodeFromName(lName, lNamespace):
    lCtrl = getCtrlFromName(lName, lNamespace)
    if lCtrl:
        animNode = lCtrl.Translation.GetAnimationNode()
        if animNode:
            return animNode

def getAmbientPoseAtFrame(lFrame, lNamespace, lCtrlList):
    ambientPose = []
    for ctrlName in lCtrlList:
        for i in [0,1]:
            animNode = getAnimNodeFromName(ctrlName, lNamespace)
            if animNode:
                time = mobu.FBTime(0,0,0,lFrame)
                valueAtFrame = animNode.Nodes[i].FCurve.Evaluate(time)
                ambientPose.append(valueAtFrame)
                
    return ambientPose
                
        
def writeToFile(lDict, lFilePath, lCtrlList):
    
    newCtrlList = []
    for lCtrl in lCtrlList:
        newCtrlList.append(lCtrl + ".X")
        
    i=1
    for lCtrl in lCtrlList:
        newCtrlList.insert(i, lCtrl + ".Y")
        i+=2
        
    with open(lFilePath, 'wb') as csvFile:
        csvWriter = csv.writer(csvFile, delimiter=',')
        csvWriter.writerow(["bone","index"] + newCtrlList)
        for ffxCtrl in lDict:
            for i in [0,1]:
                csvWriter.writerow([ffxCtrl,i] + lDict[ffxCtrl][i])
                
def readFromFile(lFilePath):
    ambientPose = {}
    with open(lFilePath) as csvFile:
        csvReader = csv.reader(csvFile, delimiter=',')
        csvReader.next()
        for row in csvReader:
            newPose = row[2:]
            if row[0] not in ambientPose:
                ambientPose[row[0]] = []
            ambientPose[row[0]].append(newPose)
                
    return ambientPose
        
def getExprFromScene(lNamespace, lFFXCtrls, lCtrlList):
    # Expression is [ [x,y,...x,y] , [x,y,...x,y] ]
    # index 0 is +1 and index 1 is -1 on the FaceFX Controllers
    
    FFXExpr = {}
    
    for ffxCtrl in lFFXCtrls:
        FFXExpr[ffxCtrl] = []
        
        # Get the ambient pose at the +1 expression
        plusFrame = lFFXCtrls[ffxCtrl][0]
        FFXExpr[ffxCtrl].append(getAmbientPoseAtFrame(plusFrame, lNamespace, lCtrlList))
        
        # Get the ambient pose at the -1 expression
        minusFrame = lFFXCtrls[ffxCtrl][1]
        FFXExpr[ffxCtrl].append(getAmbientPoseAtFrame(minusFrame, lNamespace, lCtrlList))
        
    return FFXExpr

def convertFFXToAmbientPose(val, ffxName, expr):
    # Expression is [ [x,y,...x,y] , [x,y,...x,y] ]
    # index 0 is +1 and index 1 is -1 on the FaceFX Controllers
    if val >= 0:
        scalar = expr[ffxName][0]
        newPose = [float(x)*val for x in scalar]
    else:
        scalar = expr[ffxName][1]
        newPose = [float(x)*-1*val for x in scalar]
        
    return newPose

def consolidatePoses(animList):
    for frame in animList:
        numPoses = len(animList[frame][0].TX)*2
        combinedPose = Pose([0 for x in range(numPoses)])
        for pose in animList[frame]:
            combinedPose.addPose(pose)
        combinedPose.clampPose(-1, 1)
        animList[frame] = combinedPose
        
    return animList
        
def clearKeys(lNamespace, lCtrlList):
    for lCtrl in lCtrlList:
        animNode = getAnimNodeFromName(lCtrl, lNamespace)
        if animNode:
            animNode.Nodes[0].FCurve.EditClear()
            animNode.Nodes[1].FCurve.EditClear()

def setKeyXY(lAnimNode, lFrame, val, axis):
    lTime = mobu.FBTime(0,0,0,lFrame)
    lAnimNode.Nodes[axis].FCurve.KeyAdd(lTime, val)


def transferFFXtoAmbient(lNamespace, lExpr, lCtrlList):
    finalAnims = {}
    for ffxCtrlName in lExpr:
        ffxAnimNode = getAnimNodeFromName(ffxCtrlName, lNamespace)
        if ffxAnimNode:
            for lKey in ffxAnimNode.Nodes[1].FCurve.Keys:
                frame = lKey.Time.GetFrame()
                if frame not in finalAnims:
                    finalAnims[frame] = []
                    
    for ffxCtrlName in lExpr:
        ffxAnimNode = getAnimNodeFromName(ffxCtrlName, lNamespace)
        if ffxAnimNode:
            for lFrame in finalAnims:
                lTime = mobu.FBTime(0,0,0,lFrame)
                
                ffxValue = ffxAnimNode.Nodes[1].FCurve.Evaluate(lTime)
                newPoseValues = convertFFXToAmbientPose(ffxValue, ffxCtrlName, lExpr)
                newPose = Pose(newPoseValues)
                
                finalAnims[lFrame].append(newPose)
    
    finalAnims = consolidatePoses(finalAnims)
    
    for i in range(len(lCtrlList)):
        ctrlAnimNode = getAnimNodeFromName(lCtrlList[i], lNamespace)
        for frame in finalAnims:
            setKeyXY(ctrlAnimNode, frame, finalAnims[frame].TX[i], 0)
            setKeyXY(ctrlAnimNode, frame, finalAnims[frame].TY[i], 1)

def convertFFXtoAmbient(exprFile, namespace):
    
    exprFromFile = readFromFile(exprFile)
    for lTake in lSys.Scene.Takes:
        lSys.CurrentTake = lTake
        clearKeys(namespace,AmbientCtrls)
        transferFFXtoAmbient(namespace,exprFromFile,AmbientCtrls)
        clearKeys(namespace,FFXCtrls)

def convertFFXto3L(exprFile, namespace):
    
    exprFromFile = readFromFile(exprFile)
    for lTake in lSys.Scene.Takes:
        lSys.CurrentTake = lTake
        clearKeys(namespace,threeLCtrls)
        transferFFXtoAmbient(namespace,exprFromFile,threeLCtrls)
        clearKeys(namespace,FFXCtrls)

def writeExprToFile(exprFile, namespace):
    exprFromScene = getExprFromScene(namespace, FFXCtrls, AmbientCtrls)
    writeToFile(exprFromScene, exprFile, AmbientCtrls)
    
def write3LExprToFile(exprFile, namespace):
    exprFromScene = getExprFromScene(namespace, FFXCtrls, threeLCtrls)
    writeToFile(exprFromScene, exprFile, threeLCtrls)
    