"""
Class to deal with the reloading of modules
"""
import os
import logging
import sys
import xreloader


class RsReloader():
    """
    The Rockstar Reloader Class: Named after how epicly awesome it is and well it does its job as a
    reloader!
    
    Totally capable of dynamically reloading python modules in the safest way know to man!
    This should work on webserver code as well as platform code
    
    This is a static class, meaning all the methods are usable without instantiating the class
    Usage:
        from RS.Tools.Reloader import reloader
        
        # Reload all R* Modules
        reloader.RsReloader.ReloadRsModules()
        
        # Reload a given R* Module
        from RS.Tools.CameraToolBox import configLoader
        reloader.RsReloader.ReloadModule(configLoader)
    """
    def __init__(self):
        """
        Constructor
        """
        pass
    
    @classmethod
    def _ReloadOrderKeys(cls, keySet):
        """
        Class Method
        
        Internal Method
        
        Create a new list with reordered keys to ensure any reload is done in logical step process.
        
        Example:
            keys = [RS.Tools.Camera, RS.Tools, RS.Tools.Camera.PyQtCore]
            sorted = [RS.Tools, RS.Tools.Camera, RS.Tools.Camera.PyQtCore]
            
        Args:
            keySet (list of strings): The string keys to reorder
            
        Return:
            (list of strings): The ordered string list
        """
        final = []
        derp = {}
        for key in keySet:
            splitNum = len(key.split("."))
            val = derp.get(splitNum, [])
            val.append(key)
            derp[splitNum] = val
            
        for key, value in derp.iteritems():
            value.sort()
            for val in value:
                final.append(val)
            
        return final
        
    @classmethod
    def ReloadRsModules(cls):
        """
        Class Method
        
        Reloads all Rockstar modules that are currently registered. Useful for completely reloading 
        everything during development of tools. Note this does only reload Rockstar modules - it's 
        assumed that external and standard libraries are not going to be edited so there would be no 
        requirement to reload them.
        """
        # Sort keys into logical patterns
        rsKeys = [key for key, value in sys.modules.iteritems() if key.startswith("RS.") 
                                                                            and value is not None]
        rsKeys = cls._ReloadOrderKeys(rsKeys)
        
        for key in rsKeys:
            value = sys.modules[key]
            cls._ReloadModule(value, key)
    
    @classmethod
    def _ReloadModule(cls, module, moduleName):
        """
        Class Method
        
        Internal Method
        
        Method to do the actual reloading, using the xreloader module. Checks to ensure the module
        is reloadable, as well ensure that it has all the hooks once reloaded such as its file.
        
        Args:
            module (module): The module to reload
            moduleName (string): The name of the module to reload
        """
        # check its a module and not a class
        if "__file__" not in dir(module):
            newPath = cls._ResolveModuleImportPathToModule(moduleName)
            if newPath is None:
                logging.debug("Skipping {0}".format(moduleName))
                return
            setattr(module, "__file__", newPath)
            
        doNotRelaod = getattr(module, "__DoNotReload__", False)
        if doNotRelaod:
            logging.debug("{0} Not Reloadable".format(moduleName))
            return
        filePath = module.__file__
        logging.debug("Reloaded {0}".format(moduleName)) 
        try:
            xreloader.xreload(module)
        except:
            logging.critical("Error Loading {0}".format(moduleName))
        try:
            module.__file__ = filePath
        except:
            logging.warning("Warning: Setting file on {0} issue".format(moduleName))
    
    @classmethod
    def ReloadModule(cls, module):
        """
        Class Method
        
        Method to reload a single module rather than all R* modules
        
        Args:
            module (module): The module to reload
        """
        rsKeys = [key for key, value in sys.modules.iteritems() if value == module]
        rsKeys = cls._ReloadOrderKeys(rsKeys)
        
        for key in rsKeys:
            value = sys.modules[key]
            cls._RealoadModule(value, key)
    
    @classmethod
    def _ResolveModuleImportPathToModule(cls, moduleImportPath):
        """
        Class Method
        
        Internal Method
        
        Method to get a file path to a module from the python path, which can be used if a module
        has lost its file location.
        
        Args:
            moduleImportPath (string): The full name of the module to find ("RS.Tools.CameraToolBox")
        
        Return:
            string: The file path, if found, or None
        """
        modFilePath = moduleImportPath.replace(".", os.path.sep)
        for folder in os.getenv("PYTHONPATH").split(os.path.pathsep):
            path = None
            if os.path.exists(os.path.join(folder, modFilePath)):
                path = os.path.join(folder, modFilePath)
            elif os.path.exists(os.path.join(folder, "{0}.py".format(modFilePath))):
                path = os.path.join(folder, modFilePath)
            elif os.path.exists(os.path.join(folder, "{0}.pyc".format(modFilePath))):
                path = os.path.join(folder, modFilePath)
            
            if path is not None:
                return path
