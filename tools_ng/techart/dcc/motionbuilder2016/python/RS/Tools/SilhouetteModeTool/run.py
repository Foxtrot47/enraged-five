from RS.Tools.SilhouetteModeTool import SilhouetteModeTool

def Run(show=True):
    tool = SilhouetteModeTool.SilhouetteToolWidget()

    if show:
        tool.show()
    return tool
