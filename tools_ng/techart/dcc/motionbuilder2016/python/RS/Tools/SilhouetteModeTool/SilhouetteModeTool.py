import os
import re
import pyfbsdk as mobu
from PySide import QtCore, QtGui
from RS.Tools import UI
from RS import Globals
from RS.Utils import Scene, Namespace
from RS.Tools.Isolate import Core as isolateCore
from RS.Core.ReferenceSystem.Types.Set import Set
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Prop import Prop
from RS.Core.ReferenceSystem.Types.Character import Character
from RS.Core.ReferenceSystem.Types.Vehicle import Vehicle
from RS.Core.ReferenceSystem.Types.CharacterAnimal import CharacterAnimal

class SilhouetteToolWidget(UI.QtMainWindowBase):

    def __init__(self, parent=None):
        super(SilhouetteToolWidget, self).__init__(title="Silhouette mode Tool",
                                                   size=[300, 400],
                                                   dockable=False)
        self.textboxValue = None

        # Get assats from Referance Editor
        self.characters = Manager().GetReferenceListByType(Character)
        self.characterAnimalList = Manager().GetReferenceListByType(CharacterAnimal)
        self.vehicleList = Manager().GetReferenceListByType(Vehicle)
        self.props = Manager().GetReferenceListByType(Prop)
        self.setAssets = Manager().GetReferenceListByType(Set) 

        self._setupUi()

    def getCharacterList(self):
        """
            Get Character list without Animals and exportOrigin_Mover from Referance.
        """
        charactersList = []
        for character in self.characters:
            if character not in self.characterAnimalList:
                if character.Name != "exportOrigin_Mover":
                    charactersList.append(character)
        return charactersList

    def _setupUi(self):
        # create layout
        self.mainLayout = QtGui.QGridLayout()
        self.mainWidget = QtGui.QWidget()
        # create widgets

        self.assetsListTreeWidget = QtGui.QTreeWidget()

        # Add headerItem
        self.headerItem = QtGui.QTreeWidgetItem()
        self.head = self.assetsListTreeWidget.headerItem().setText(0, "Assets")

        # Primary List Items
        self.assetTypes = {
            Character: "Characters",
            CharacterAnimal: "Animals",
            Prop: "Props",
            Vehicle: "Vehicles"
        }

        # Referanced Assats
        self.assets = {
            Character: self.getCharacterList(),
            CharacterAnimal: self.characterAnimalList,
            Prop: self.props,
            Vehicle: self.vehicleList
        }

        # SetText on assetsListTreeWidget
        for asset in self.assets.keys():
            self.parent = QtGui.QTreeWidgetItem(self.assetsListTreeWidget)
            self.parent.setText(0, self.assetTypes[asset])
            self.parent.setFlags(self.parent.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable)
            for prop in self.assets[asset]:
                self.child = QtGui.QTreeWidgetItem(self.parent)
                self.child.setFlags(self.child.flags() | QtCore.Qt.ItemIsUserCheckable)

                self.child.setText(0, "{0}:{1}  ({2})".format(prop.Namespace, prop.Name, prop.PropertyTypeName))
                self.child.setCheckState(0, QtCore.Qt.Unchecked)

        self.selectAllPushButton = QtGui.QPushButton("Select All")
        self.mainLayout.addWidget(self.selectAllPushButton, 0, 1, 1, 1)

        self.hideSetPushButton = QtGui.QPushButton("Hide Sets")
        self.mainLayout.addWidget(self.hideSetPushButton, 0, 0, 1, 1)
        
        self.hideOtherAssetsCheckBox = QtGui.QCheckBox("checkBox")
        self.hideOtherAssetsCheckBox.setText("Hide Other Assets")
        self.hideOtherAssetsCheckBox.setChecked(False)
        self.mainLayout.addWidget(self.hideOtherAssetsCheckBox, 1, 0, 1, 1)
        
        self.hideSetsCheckBox = QtGui.QCheckBox("checkBox_2")
        self.hideSetsCheckBox.setText("Don't hide Sets")
        self.hideSetsCheckBox.setChecked(False)
        self.hideSetsCheckBox.setEnabled(False)        
        self.mainLayout.addWidget(self.hideSetsCheckBox, 1, 1, 1, 1)
        self.mainLayout.addWidget(self.assetsListTreeWidget, 2, 0, 1, 2)

        self.silhouetteModePushButton = QtGui.QPushButton("Silhouette Mode")
        self.mainLayout.addWidget(self.silhouetteModePushButton, 3, 0, 1, 1)

        self.normalModePushButton = QtGui.QPushButton("Normal Mode")
        self.mainLayout.addWidget(self.normalModePushButton, 3, 1, 1, 1)

        self.mainWidget.setLayout(self.mainLayout)

        self.setCentralWidget(self.mainWidget)

        self.silhouetteModePushButton.clicked.connect(self.enableSilhouetteMode)
        self.normalModePushButton.clicked.connect(self.restoreVisibility)
        self.selectAllPushButton.clicked.connect(self.selectAllCheckBoxes)
        self.hideOtherAssetsCheckBox.stateChanged.connect(self.stateChange)
        self.hideSetPushButton.clicked.connect(self.toggleSetVisibility)

    def toggleSetVisibility(self):
        """
           Toggle the set Asset visiablity bstate 
        """

        if self.hideSetPushButton.text() == "Hide Sets":
            state = False
            self.hideSetPushButton.setText("Show Sets")
        else:
            state = True
            self.hideSetPushButton.setText("Hide Sets")
        
        self.setVisibililty(state)
        
    def setVisibililty(self, state):
        """
            Control Set Asset Visiablity 
        """
        for setAsset in self.setAssets:
            namespace = Globals.Scene.NamespaceGet(setAsset.Namespace)            
            for setAssetviselblity in self.getModelType(namespace, mobu.FBModel):
                setAssetviselblity.Show = state            

    def stateChange(self):
        self.hideSetsCheckBox.setChecked(False)
        self.hideSetsCheckBox.setEnabled(self.hideOtherAssetsCheckBox.isChecked())
        
    def selectAllCheckBoxes(self):
        """
            Control checkState of all checkboxes
        """

        if self.selectAllPushButton.text() == "Select All":
            state = QtCore.Qt.Checked
            self.selectAllPushButton.setText("UnSelect All")
        else:
            state = QtCore.Qt.Unchecked
            self.selectAllPushButton.setText("Select All")

        for checkbox in self.getCheckBoxItems():
            checkbox.setCheckState(0, state)

    def getCheckBoxItems(self):
        """
            Get all CheckBox items from assetsListTreeWidget
        """
        rootItem = self.assetsListTreeWidget.invisibleRootItem()
        childCount = rootItem.childCount()

        checkedItemsList = []

        for rootIteration in xrange(childCount):
            signal = rootItem.child(rootIteration)
            childrenCount = signal.childCount()

            for childIteration in xrange(childrenCount):
                child = signal.child(childIteration)
                checkedItemsList.append(child)

        return checkedItemsList

    def getCheckedItemsText(self):
        """
            Get all checked items text from assetsListTreeWidget
        """
        checkedItemsListText = []
        for checkbox in self.getCheckBoxItems():
            if checkbox.checkState(0) == QtCore.Qt.Checked:
                checkedItemsListText.append(checkbox.text(0))
        return checkedItemsListText

    def getIsolateNullName(self):
        """
            Create Name for custom property Null
        """
        sceneBaseName = os.path.basename(Globals.Application.FBXFileName)
        sceneBaseName = os.path.splitext(sceneBaseName)[0]

        isolateData = 'isolateData_{0}_1'.format(sceneBaseName)
        return isolateData

    def _deleteIsolateData(self):
        """
            delete if  FBModelNull "isolateData__1" exist
        """

        isolateDataModelName = self.getIsolateNullName()
        isolateData = mobu.FBFindModelByLabelName(str(isolateDataModelName))

        if isolateData is not None:
            isolateData.FBDelete()

    def _isolateSelection(self):
        """
            Save visibility setting from current scene selection and isolate them.
        """
        selectedModels = Globals.Models.Selected
        if len(selectedModels) > 0:
            self._deleteIsolateData()
            viewState = isolateCore.ViewState()
            viewState.isolateSelection()
            Scene.DeSelectAll()
        else:
            QtGui.QMessageBox.information(None, "R* Error", "Select any Contol")

    def restoreVisibility(self):
        """
            Restore visibility setting from the selected nullSettings.
        """
        self.disableSilhouetteMode()
        
        isolateDataModelName = self.getIsolateNullName()
        if isolateDataModelName is None:
            return
        viewState = isolateCore.ViewState()
        viewSettings = viewState.read(str(isolateDataModelName))
        if viewSettings is None:
            return
        viewState.restore(viewSettings)
        self._deleteIsolateData()
        
    def disableSilhouetteMode(self):
        """
            This method Control the background color, remove old SilhouetteShader, Enable original shaders,
            Change CurrentMapping Method to UV and set the normal Property value for Materials.
        """

        # background color change to Default
        for camera in Globals.Cameras:
            camera.BackGroundColor = mobu.FBColor(0.16, 0.16, 0.16)

        # Active Current shaders
        for shader in Globals.Shaders:
            shader.PropertyList.Find("Enable").Data = True

        # Remove Old SilhouetteShaders
        self.removeTempShader()

        # Change background color in to normal
        for texture in Globals.Textures:
            texture.PropertyList.Find("CurrentMappingType").Data = 1

        # Active Meterials property
        for material in Globals.Materials:
            material.PropertyList.Find("EmissiveFactor").Data = 1.0
            material.PropertyList.Find("AmbientFactor").Data = 1.0
            material.PropertyList.Find("DiffuseFactor").Data = 1.0
            material.PropertyList.Find("SpecularFactor").Data = 1.0            
        self.setVisibililty(True)
        
    def removeTempShader(self):
        for silhouetteShaderItem in Globals.Shaders:
            if silhouetteShaderItem.Name == "SilhouetteShader":
                silhouetteShaderItem.FBDelete() 
                                             
    def desableTexturAndMeterials(self, namespace):
        """
            Change CurrentMapping Method to None
            Deactivate Materials property
        """
        for texture in Namespace.GetContentsByType(namespace, objectType=mobu.FBTexture, exactType=False):
            texture.PropertyList.Find("CurrentMappingType").Data = 0

        for material in Namespace.GetContentsByType(namespace, objectType=mobu.FBMaterial, exactType=False):
            material.PropertyList.Find("EmissiveFactor").Data = 0.0
            material.PropertyList.Find("AmbientFactor").Data = 0.0
            material.PropertyList.Find("DiffuseFactor").Data = 0.0

    def getModelType(self, namespace, modelType):
        """
        Get Models with help of curent assets namesapce
        """
        selectedAssetsTypes = []

        for model in Namespace.GetContentsByType(namespace, objectType=modelType, exactType=False): 
                  
            selectedAssetsTypes.append(model)                    
        return selectedAssetsTypes   
        
    def getAssets(self, namespace, modelType):
        """
        Get Models with help of curent assets namesapce without mover Controls ant etc
        """
        
        Assets = self.getModelType(namespace, modelType)
        
        selectedAssets = []
        regex = re.compile('(?i)mover|text|rect|contact|ctrl|frm')

        for model in Assets:       
            if not regex.search(model.Name) and model.Geometry and type(model.Geometry) == mobu.FBMesh and model.Geometry.PolygonCount() > 6:
                selectedAssets.append(model)
                    
        return selectedAssets  
                                
    def enableSilhouetteMode(self):
        """
            Disabled old shader for characters apply disableSilhouetteMode properties add new shader with "UseLuminosity" active 
            and select only Geometry on all assets and apply isolate selection  
        """       
        if not len(self.getCheckedItemsText()) > 0:            
            QtGui.QMessageBox.information(None, "R* Error", "Select any items in the list")
            return
                     
        # Reset to normal
        self.restoreVisibility()

        self.selectedModelsItems = []
        
        # Get Geometry from all assets
        for checkedItem in self.getCheckedItemsText():
            nameSpace = checkedItem.split(":")[0] 

            if checkedItem.endswith("(CharacterCutscene)") or checkedItem.endswith("(CharacterIngame)"):

                namespace = Namespace.GetFBNamespace(str(nameSpace))
                                 
                for characterShader in self.getModelType(namespace, mobu.FBMaterial):
                    characterShader.PropertyList.Find("EmissiveFactor").Data = 0.0
                    characterShader.PropertyList.Find("AmbientFactor").Data = 0.0
                    characterShader.PropertyList.Find("DiffuseFactor").Data = 0.0
                    characterShader.PropertyList.Find("SpecularFactor").Data = 0.0

                for model in self.getAssets(namespace, mobu.FBModel):
                    self.selectedModelsItems.append(model)

            if checkedItem.endswith("(CharacterAnimal)"):
                
                namespace = Namespace.GetFBNamespace(str(nameSpace))
                self.desableTexturAndMeterials(namespace)
                for characterAnimalModel in self.getAssets(namespace, mobu.FBModel): 
                    self.selectedModelsItems.append(characterAnimalModel)                

            if checkedItem.endswith("(Vehicles)"):
                
                namespace = Namespace.GetFBNamespace(str(nameSpace))
                self.desableTexturAndMeterials(namespace)
                for vehiclesModel in self.getModelType(namespace, mobu.FBModel): 
                    self.selectedModelsItems.append(vehiclesModel)               

            if checkedItem.endswith("(Props)"):

                namespace = Namespace.GetFBNamespace(str(nameSpace))
                self.desableTexturAndMeterials(namespace)
                for propModel in self.getModelType(namespace, mobu.FBModel): 
                    self.selectedModelsItems.append(propModel) 
                           
        # deSelecte All Assets
        Scene.DeSelectAll()

        if self.hideOtherAssetsCheckBox.isChecked():
            # Select assets From Assets Geometry  list
            for asset in self.selectedModelsItems:
                asset.Selected = True

            # Hide All other except Assets Geometry
            self._isolateSelection()
            # background color change to White
            for camera in Globals.Cameras:
                camera.BackGroundColor = mobu.FBColor(1.00, 1.00, 1.00)
                camera.ViewShowGrid = False
                
            if self.hideSetsCheckBox.isChecked():
                self.setVisibililty(True)
