from RS.Tools.FileTakeCombiner.Dialogs import fileTakeToolDialog


def Run(show=True):
    tool = fileTakeToolDialog.FileTakeTool()

    if show: 
        tool.show()

    return tool
