import os

import pyfbsdk as mobu

from RS import Globals
from RS.Tools import UI
from RS.Tools.FileTakeCombiner import Core as FileTakeCore

from PySide import QtGui, QtCore


class FileTakeTool(UI.QtMainWindowBase):
    """
        main Tool UI
    """
    def __init__(self):
        """
            Constructor
        """
        #Path list of names
        self.pathList = []

        self._toolName = "File take combiner"

        # Main window settings
        super(FileTakeTool, self).__init__(title=self._toolName, dockable=True)
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()

        # Create Widgets
        banner = UI.BannerWidget(self._toolName)
        self.centralWidget = QtGui.QWidget()
        self.centralWidget.console = QtGui.QTextEdit(self)

        buttonBox = QtGui.QDialogButtonBox(QtCore.Qt.Horizontal)

        self.addFileButton =  QtGui.QPushButton("Append Files")
        self.mergeFileButton =  QtGui.QPushButton("Run")
        self.cancelButton =  QtGui.QPushButton("Cancel")

        # Edit properties
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self.centralWidget.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self.centralWidget.console.setReadOnly(True)

        self.addFileButton.setMinimumWidth(90)
        self.mergeFileButton.setMinimumWidth(68)
        self.cancelButton.setMinimumWidth(78)

        buttonBox.addButton(self.addFileButton, QtGui.QDialogButtonBox.ActionRole)
        buttonBox.addButton(self.mergeFileButton, QtGui.QDialogButtonBox.ActionRole)
        buttonBox.addButton(self.cancelButton, QtGui.QDialogButtonBox.ActionRole)
        buttonBox.setCenterButtons(True)

        # Assign Widgets to layouts
        mainLayout.addWidget(banner)
        mainLayout.addWidget(self.centralWidget.console)
        mainLayout.addWidget(buttonBox)

        self.centralWidget.setLayout(mainLayout)
        self.setCentralWidget(self.centralWidget)

        # callbacks
        self.addFileButton.clicked.connect(self._appendFileToMerge)
        self.cancelButton.clicked.connect(self.close)
        self.mergeFileButton.clicked.connect(self._mergeTakes)

    def _refreshMergeList(self, text):
        """
            Internal Method

            Refresh content of takesComboBox widget
        """
        self.centralWidget.console.append(text)

    def _appendFileToMerge(self):
        """
            Internal Method

            Add a file to the list of files to merge
        """
        filePath, browseWasCancelled = QtGui.QFileDialog.getOpenFileName(self, 'Open Shape File', QtCore.QDir.currentPath() , "*.fbx")
        if not browseWasCancelled:
            return

        self.pathList.append(str(filePath))
        self._refreshMergeList(str(filePath))

        xmlDirectory = os.path.dirname(str(filePath))

        QtCore.QDir.setCurrent(xmlDirectory)

    def _mergeTakes(self):
        '''
            Internal Method

            Merges all the files from the provided list together
        '''
        if len(self.pathList) < 2:
            return

        Globals.Application.FileNew()
        FileTakeCore.MergeTakesFromFiles(self.pathList)
        self.close()