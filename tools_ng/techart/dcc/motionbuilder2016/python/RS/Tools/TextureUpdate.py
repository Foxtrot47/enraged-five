from pyfbsdk import *
import os
import RS.Perforce
import RS.Config

        
def checkChar(charName):
    if charName.lower().startswith("player_zero") :
        charName = "Michael"
    if charName.lower().startswith("player_one") :
        charName = "Franklin"
    if charName.lower().startswith("player_two") :
        charName = "Trevor"
    if charName.lower().startswith("csb_stripper_01") :
        charName = "S_F_Y_Stripper_01"
    if charName.lower().startswith("csb_stripper_02") :
        charName = "S_F_Y_Stripper_02"
    if "^" in charName :
        charName = (charName.split("^"))[0]
    if " " in charName :
        charName = (charName.split(" "))[0]
        
    return charName
        
        
def searchNamespace(ns,videoFile):
    for items in videoFile.Parents:
        if items.ClassName() == 'FBTexture':
            ns = ((items.LongName).split(":"))[0]
            
    if "Player_" in ns :
        CHAR = checkChar(ns)
        path = os.path.join("art\\peds\\Player",CHAR,"Textures\\HighRes")
        return path
        
    elif ns.lower().startswith("csb_stripper") :
        CHAR = checkChar(ns)
        path = os.path.join("art\\peds\\Ped_Models",CHAR,"Textures\\HighRes")
        return path
        
    elif ns.lower().startswith("cs") :
        CHAR = checkChar(ns)
        path = os.path.join("art\\peds\\Story_Characters",CHAR,"Textures\\HighRes")
        return path
    
    elif len(ns) > 5 :
        if (ns[1]+ns[3]+ns[5]) == "___" :
            ambNS = ns
            CHAR = checkChar(ambNS)
            path = os.path.join("art\\peds\\Ped_Models",CHAR,"Textures\\HighRes")
            return path
        
    return False

def updateTextures() :
    
    RS_PROJROOT = RS.Config.Project.Path.Root
    
    swapList = ["head","teef","uppr","lowr","hair","hand","berd","feet"]
    
    lSystem = FBSystem().Scene
    lSyncList = []
    lVideoList = []
    lPathList = []
    
    for each in lSystem.VideoClips:
        
        namespace = (each.LongName).split(":")
        if len(namespace) > 1 :
            
            #search to see if this character is compatible.
            RS_CHARTXTROOT = searchNamespace(namespace[0],each)
            if RS_CHARTXTROOT != False :
                
                for item in swapList :
                
                    if (item + "_diff_") in each.Filename.lower() :
                        parsedFilename1 = each.Filename.split("\\")
                        parsedFilename2 = parsedFilename1[len(parsedFilename1)-1].split("/")
                        theFile = parsedFilename2[len(parsedFilename2)-1]
                        if theFile.lower().startswith(item + "_"):
                            newFilename = os.path.join(RS_PROJROOT, RS_CHARTXTROOT, theFile)
                            #each.Filename = newFilename
                            lVideoList.append(each)
                            lPathList.append(newFilename)
                            lSyncList.append(newFilename)
    
    #P4 Sync
    #print lSyncList
    if len(lSyncList) != 0:
        RS.Perforce.Sync(lSyncList)
    
    if len(lVideoList) == len(lPathList):
        i=0
        for video in lVideoList:
            video.Filename = lPathList[i]
            i+=1
        
    lSystem.Evaluate()

def fixGUITextures() :
    
    RS_PROJROOT = RS.Config.Project.Path.Root

    videoList = []
    textureList = []
    syncList = []
    facialMainGUI = FBComponentList()
    FBFindObjectsByName("faceControls_OFF", facialMainGUI, False, True)
    FBFindObjectsByName("FacialAttrGUI", facialMainGUI, False, True)
    
    
    def idChildren (parentModel):
        # Get model children
        children = parentModel.Children
        
        # Check if any children exist
        if (len (children) != 0):
            # Loop through the children
            for child in children:
                #print "searching for more children"
                # Get any children  
                idChildren (child)
        else:
            for mats in parentModel.Materials :
                # append the filepath to list
                texture = mats.GetTexture()
                if texture != None:
                    if texture.Video != None:
                        textureFilename = texture.Video.Filename
                        if texture in textureList:
                            None
                        else:
                            videoList.append(textureFilename)
                            textureList.append(texture)
        
        # Return status
        return True
    
    for facialObj in facialMainGUI :
        idChildren(facialObj)
    
    if len(videoList) != 0:
        if len(videoList) == len(textureList):
            for each in videoList :
                parsedFilename1 = each.split("\\")
                parsedFilename2 = parsedFilename1[len(parsedFilename1)-1].split("/")
                theFile = parsedFilename2[len(parsedFilename2)-1]
                newFilename = os.path.join(RS_PROJROOT, 'art\\peds\\3LateralSetup', theFile)
                syncList.append(newFilename)
                      
            RS.Perforce.Sync(syncList)
            i=0
            
            for each in textureList :
                each.Video.Filename = syncList[i]
                i+=1

    lSystem = FBSystem().Scene
    lSystem.Evaluate()


