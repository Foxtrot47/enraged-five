from RS.Tools.FirstPersonController.widgets import firstPersonControllerWidget
from RS.Tools.UI import Application
reload(firstPersonControllerWidget)

def Run(show=True):
    Application.CloseToolByTitle(firstPersonControllerWidget.fpsToolTitle)
    tool = firstPersonControllerWidget.ToolWidgetDialog()

    if show: 
        tool.show()  
    return tool
