"""
    This UI will be shared across multiple tool.
    Acts as a integrated Character control picker.

    For the FpsCamera toolbox every operation is based on reference assets
    The namespace is extracted from the UI FBCharacter.
"""

import inspect
import os

from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Globals
from RS import Config, Core 

from RS.Utils.Scene import Take
from RS.Tools import UI
from RS.Utils import Namespace


class CharacterWidget(QtGui.QWidget):
    """
        character selection widget
    """
    def __init__(self, toolBox):
        """
            Constructor
        """
        # Main window settings
        self.toolBox = toolBox
        super(CharacterWidget, self).__init__()

        self.groupLabel = "Input Character"

        self.buttonSize = 18

        self.setupUi()

    def setupUi(self):
        self.mainLayout = QtGui.QVBoxLayout()
        characterLayout = QtGui.QHBoxLayout()

        self.InputGroupBox =  QtGui.QGroupBox(self.groupLabel)
        self.characterComboBox = QtGui.QComboBox(self)

        self.refreshPushButton = QtGui.QPushButton("")
        self.refreshPushButton.setToolTip('Refresh Input Characters drop down list')

        self.refreshPushButton.setMaximumHeight(self.buttonSize)

        self.refreshPushButton.setMaximumWidth(self.buttonSize)

        self.characterComboBox.setMinimumHeight(self.buttonSize)

        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.mainLayout.setSpacing(2)

        characterLayout.addWidget(self.characterComboBox)
        characterLayout.addWidget(self.refreshPushButton)

        characterLayout.setContentsMargins(6, 2, 6, 4)

        self.mainLayout.addWidget(self.InputGroupBox)
        self.InputGroupBox.setLayout(characterLayout)
        self.setLayout(self.mainLayout)

        self._setRefreshIcon()

        self._characterUI()

    def _createSeparator(self):
        targetLine = QtGui.QFrame()

        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _setRefreshIcon(self):
        """
            Build Refresh namespace button
        """
        currentPath = os.path.dirname(inspect.getfile(self.setupUi)).replace('\\', '/')
        currentPath = currentPath.split('RS/Tools/') 

        imagePath =  os.path.join(currentPath[0],
                                  'RS',
                                  'Tools',
                                  'DressPicker',
                                  'Dialogs',
                                  'data',
                                  'UpdateReference.png')


        self.refreshPushButton.setIcon(QtGui.QIcon(QtGui.QPixmap(QtGui.QImage(imagePath))))

    def _characterUI(self):
        self._refreshCharacterComboBox()

        self.characterComboBox.activated.connect(self._changeCurrentCharacter)
        self.refreshPushButton.pressed.connect(self._refreshCharacterComboBox)

    def _changeCurrentCharacter(self):
        """
            Synchronize the current character in the UI with the one in the scene
        """
        inputCharacterName = self.characterComboBox.currentText() 
    
        for character in Core.System.Scene.Characters:
            if character.LongName == inputCharacterName:
                mobu.FBApplication().CurrentCharacter = character
                break

    def _refreshCharacterComboBox(self):
        """
            Refresh content of characterComboBox widget
        """
        characterCount = len(Core.System.Scene.Characters)

        self.characterComboBox.clear()

        for index in xrange(characterCount):
            self.characterComboBox.addItem(Core.System.Scene.Characters[index].LongName)

        if mobu.FBApplication().CurrentCharacter is None:
            return

        activeCharacterName = mobu.FBApplication().CurrentCharacter.LongName

        for index in xrange(characterCount):
            if str(self.characterComboBox.itemText(index)) == str(activeCharacterName):
                self.characterComboBox.setCurrentIndex(index)
                break

    def getCharacterFromWidget(self):
        """
            From a UI character LongName,
            returns a scene FBCharacter ( if a valid one is found)
        """
        if self.characterComboBox.count() < 1 :
            return None

        inputCharacterName = self.characterComboBox.currentText() 
        inputCharacter = None

        for character in Globals.Characters:
            if character.LongName == inputCharacterName:
                inputCharacter =  character
                break

        if inputCharacter is None :
            return None

        return inputCharacter