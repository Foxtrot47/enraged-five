"""
    This file will expose widgets dedicated to filter animation curve on the fpsCamera rig components
"""
import os
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Config

from RS import Globals
from RS.Utils.Scene import Take
from RS.Utils import Namespace


class Filter(object):
    """
        Data structure holding filter Base configuration
    """
    def __init__(self, 
                 type=None, 
                 attribute=None, 
                 value=None):
        self.type = type

        self.attribute = attribute

        self.value = value


class StabilizerFilterOperations(object):
    """
        Data structure holding registered filter settings
    """
    def __init__(self):
        self.keyReducing = Filter(type="Key Reducing", 
                                  attribute='Precision', 
                                  value=15.0)

        smoothTime = mobu.FBTime()
        smoothTime.SetFrame(3,  mobu.FBTimeMode.kFBTimeMode30Frames) 

        self.smooth = Filter(type="Smooth", 
                             attribute='Width', 
                             value=smoothTime)

        self.reinterpolate = Filter(type="Reinterpolate", 
                                    attribute='Resample', 
                                    value=True)

        self.resample = Filter(type="Resample", 
                               attribute='Frame Rate', 
                               value=30.0)

        self.peakRemoval = Filter(type="Peak Removal")

    def setValue(self, inputType, targetValue):
        targetFilter = getattr(self, inputType)
        targetFilter.value = targetValue

        if inputType == 'smooth':
            smoothTime = mobu.FBTime()
            smoothTime.SetFrame(int(targetValue),  mobu.FBTimeMode.kFBTimeMode30Frames) 
            targetFilter.value = smoothTime

    def getValue(self, inputType):
        targetFilter = getattr(self, inputType)

        if inputType == 'smooth':
            return targetFilter.value.GetFrame()
        else:
            return targetFilter.value


class FilterOptionsWidget(QtGui.QWidget):
    """
        Widget mapping an option to a gui:
            some filter operation needs spinner, checkbox or no widget.

        This logic will be delegate here.
    """
    def __init__(self, 
                 inputLabel='', 
                 inputProperty='',
                 buildEmptyControl=False,
                 buildCheckBox=False):
        super(FilterOptionsWidget, self).__init__()

        self.inputLabel = inputLabel

        self.inputProperty = inputProperty
    
        self.buildEmptyControl = buildEmptyControl

        self.buildCheckBox = buildCheckBox

        self.valueWidget = None

        self.setupUi()

    def setupUi(self):
        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)

        executeLayout = QtGui.QHBoxLayout()
        executeWidget = QtGui.QGroupBox('')

        executeLayout.addWidget(QtGui.QLabel('{} Filter Options'.format(self.inputLabel)))
        executeLayout.addStretch(1)

        executeWidget.setMinimumHeight(46)

        executeWidget.setLayout(executeLayout)
        layout.addWidget(executeWidget)
        self.setLayout(layout)

        if self.buildEmptyControl is True:
            return

        if self.buildCheckBox is False:
            executeLayout.addWidget(QtGui.QLabel(self.inputProperty))
            self.valueWidget = QtGui.QSpinBox()
            self.valueWidget.setMinimumWidth(150)
        else:
            self.valueWidget = QtGui.QCheckBox(self.inputProperty)

        executeLayout.addWidget(self.valueWidget)


class FpsFilterWidget(QtGui.QWidget):
    """
        This UI will expose the most common filter used on fpsCamera rig components:

        A dropdown menu will exposes the available option and change the filter widget accordingly
    """
    FILTER_KEYS = {"Smooth":'smooth',
                  "Key Reducing":'keyReducing',
                  "Peak Removal":'peakRemoval',
                  "Reinterpolate":'reinterpolate',
                  "Resample":'resample'}

    WIDGET_KEYS = ('Smooth', 
                   'Key Reducing',
                   'Resample',
                   'Peak Removal',
                   'Reinterpolate')

    RADION_BUTTON_LABELS = (('Smooth All','Smooth Selection'),
                            ('Reduce All','Reduce Selection'),
                            ('Resample All','Resample Selection'),
                            ('Reduce All','Reduce Selection'),
                            ('Reinterpolate All','Reinterpolate Selection'))

    def __init__(self):
        super(FpsFilterWidget, self).__init__()
        self.filterOptions = StabilizerFilterOperations()
        self.currentFilterOptionWidget = None

        self.setupUi()

    def setupUi(self):
        self.currentProps = QtGui.QLineEdit()

        mainLayout = QtGui.QVBoxLayout()

        mainLayout.setContentsMargins(0, 0, 0, 0)
        self.setMinimumWidth(580)

        mainLayout.addWidget(self._buildButtonsControls())

        self.setLayout(mainLayout)

    def _buildFilterOptions(self):
        executeLayout = QtGui.QHBoxLayout()
        executeWidget = QtGui.QGroupBox('')

        valueWidget = QtGui.QDoubleSpinBox()
        valueWidget.setMinimumWidth(150)

        executeLayout.addWidget(QtGui.QLabel('Smooth Filter Options'))
        executeLayout.addStretch(1)
        executeLayout.addWidget(QtGui.QLabel('Width'))
        executeLayout.addWidget(valueWidget)

        executeWidget.setLayout(executeLayout)

        return executeWidget

    def _setFilterWidgetDefaultValue(self):
        for filterWidget in self.optionWidgets:
            defaultValue = self.filterOptions.getValue(self.FILTER_KEYS[filterWidget.inputLabel])
            if filterWidget.valueWidget is None:
                continue

            if filterWidget.buildCheckBox is False:
                filterWidget.valueWidget.setValue(defaultValue)
            else:
                filterWidget.valueWidget.setChecked(defaultValue)

    def _createFilterDropDownControls(self):
        executeLayout = QtGui.QHBoxLayout()

        executeWidget = QtGui.QWidget()
        self.combo = QtGui.QComboBox(self)

        self.combo.addItem("Smooth")
        self.combo.addItem("Key Reducing")
        self.combo.addItem("Peak Removal")
        self.combo.addItem("Reinterpolate")
        self.combo.addItem("Resample")

        self.combo.setMinimumWidth(210)

        executeLayout.setContentsMargins(0, 0, 0, 0)

        executeLayout.addWidget(self.combo)

        executeWidget.setLayout(executeLayout)        
        self.combo.activated[unicode].connect(self._swapFilterOptions)

        return executeWidget

    def _createFilterOptionsWidget(self):
        self.widgetHidden = QtGui.QWidget()
        self.hiddenOptionLayout = QtGui.QVBoxLayout()
        self.hiddenOptionLayout.setContentsMargins(0, 0, 0, 0)
        self.widgetHidden.hide()

        self.filterOptionWidget = QtGui.QWidget()
        self.filterOptionLayout = QtGui.QVBoxLayout()
        self.filterOptionLayout.setContentsMargins(0, 0, 0, 0)
        self.filterOptionWidget.setLayout(self.filterOptionLayout)

        self.optionWidgets = [FilterOptionsWidget(inputLabel='Smooth', 
                                                  inputProperty='Width'),

                              FilterOptionsWidget(inputLabel='Key Reducing', 
                                                  inputProperty='Precision'),

                              FilterOptionsWidget(inputLabel='Resample', 
                                                  inputProperty='Frame Rate'),

                              FilterOptionsWidget(inputLabel='Peak Removal', 
                                                  buildEmptyControl=True),

                              FilterOptionsWidget(inputLabel='Reinterpolate', 
                                                  inputProperty='Resample',
                                                  buildCheckBox=True)]

        self._setFilterWidgetDefaultValue()

        self.currentFilterOptionWidget = self.optionWidgets[0]

        for widget in self.optionWidgets:
            self.filterOptionLayout.addWidget(widget)

    def _buildFilterGroupBox(self):
        applyLayout = QtGui.QVBoxLayout()

        applyWidget = QtGui.QGroupBox()
        self.keyFilterButton = QtGui.QPushButton("Apply Filter")

        self._createFilterOptionsWidget()
        self.dropDownControls = self._createFilterDropDownControls()

        self.keyFilterButton.setMinimumHeight(38)

        applyLayout.addWidget(self.dropDownControls)
        applyLayout.addWidget(self.widgetHidden)
        applyLayout.addWidget(self.filterOptionWidget)
        applyLayout.addWidget(self.keyFilterButton)

        applyWidget.setLayout(applyLayout)
        self._swapFilterOptions()

        return applyWidget

    def _buildButtonsControls(self):
        executeLayout = QtGui.QVBoxLayout()

        executeWidget = QtGui.QWidget()

        executeLayout.setContentsMargins(0, 0, 0, 0)

        executeLayout.addWidget(self._buildFilterGroupBox())

        executeWidget.setLayout(executeLayout)

        return executeWidget

    def _swapFilterOptions(self):
        filterKey = str(self.combo.currentText())

        for widget in self.optionWidgets:
            widget.setParent(self.widgetHidden)

        currentWidgetIndex = self.WIDGET_KEYS.index(filterKey)
        self.optionWidgets[currentWidgetIndex].setParent(self.filterOptionWidget)
        self.filterOptionLayout.addWidget(self.optionWidgets[currentWidgetIndex])

        self.currentFilterOptionWidget = self.optionWidgets[currentWidgetIndex]

    def _createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine
