"""
    This UI will helps animation loading/Saving for fpsCameraRig components.
"""

import inspect
import os
from functools import partial

from PySide import QtGui, QtCore
import pyfbsdk as mobu
import RS

from RS import Core 
from RS import Config 
from RS import Globals
from RS.Utils import Instrument, ContextManagers
from RS.Utils.Scene import Take
from RS.Utils import Namespace
from RS.Tools.FirstPersonController.Widget import fpsCharacterWidget
from RS.Tools.FirstPersonController import Core as fpsCore 
from RS.Tools.MetapedToFbx.UI.Widgets import RsBaseWidgets


class FpsAnimationIoWidget(QtGui.QWidget):
    SWAP_ICONS = ('SaveReference',
                  'Folder')

    def __init__(self,
                 characterWidget):
        self.characterWidget = characterWidget

        self.saveCurrentAnimation = True

        super(FpsAnimationIoWidget, self).__init__() 

        self.setupUi()

    def swapIcon(self):
        swapIndex = 0
        if self.IoModeWidget.radioButtonArray[1].isChecked():
            swapIndex = 1

        iconFolderString = "{0}/ReferenceEditor/".format(Config.Script.Path.ToolImages)
        refreshLibraryIcon = QtGui.QIcon()
        refreshLibraryPixmap = QtGui.QPixmap("{0}{1}.png".format(iconFolderString,
                                                                 self.SWAP_ICONS[swapIndex]))

        refreshLibraryIcon.addPixmap(refreshLibraryPixmap,
                                     QtGui.QIcon.Normal, 
                                     QtGui.QIcon.Off)

        self.animationFileButton.setIcon(refreshLibraryIcon)

        self.saveCurrentAnimation = not bool(swapIndex)

    def terminateToolBoxWindow(self):
        """
            Close the parent tool here when validation fails.
        """
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._terminateWindow()

    def hasValidRig(self):
        """
            Every operation now run a validation test:
                If the rig state fail this test a modal dialog will be display
                to inform what is incorrect and the tool will then be closed.
        """
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return False

        rigUtils = fpsCore.FpsCameraRigValidator(currentCharacter)

        if not rigUtils.hasValidFpsCameraRig():
            print rigUtils.report.errorType, rigUtils.report.message

            QtGui.QMessageBox.warning(self, 
                                      rigUtils.report.errorType, 
                                      rigUtils.report.caption)

            self.disableShelveWidget()

            return False

        return True

    def refreshToolBoxWindow(self):
        """
            Will force the tool to adopt the correct size .
        """
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._refreshWindow()

    def getToolbox(self):
        """
            Using widget->window(). might not return the correct element when
            the tool is docked.

            Just walk up the hierarchy until a coorect dialog type is found.
        """
        toolbox = None

        currentParent = self.parent()

        for toolIndex in range(10):
            if not isinstance(currentParent,
                              RS.Tools.fpsCameraToolbox.FpsCameraToolBox):
                currentParent = currentParent.parent()

                continue

            toolbox = currentParent 

        return toolbox

    def disableShelveWidget(self):
        """
            Optional action triggered when invalid rig is detected.
        """
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._disableUI()
        currentParent._refreshWindow()

    def _browseExportFile(self):
        """
            Animation curves are saved directly into an fbx container.
            in order to prevent user to directly import it we change its extension to fbo
        """
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        currentDirectory = os.path.join(os.path.dirname(__file__), 
                                        '{}_{}'.format(currentCharacter.Name,
                                                       mobu.FBSystem().CurrentTake.Name))

        filePath, cancelState = QtGui.QFileDialog.getSaveFileName(self, 
                                                                  'Save FpsCamera Animation', 
                                                                  currentDirectory , 
                                                                  "*.fbo")

        if not cancelState:
            return

        self.animationExporterField.setText(filePath)

        if not os.path.isdir(os.path.dirname(filePath)):
            return

        animationUtils = fpsCore.FpsAnimationManager(mobu.FBApplication().CurrentCharacter)

        hasAllComponents = animationUtils.collectAnimationNodes()

        if not hasAllComponents:
            errorStream = 'Please Update your character to the latest version' 
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)

            return

        # Plot FPS animation controls to the base layer
        animationUtils.bakeAnimation()
        Globals.System.CurrentTake.SetCurrentLayer(0)
        
        # Must update curve node refs after bake as curves on anim layers above the base layer
        # may be destroyed during the bake (url:bugstar:5259689)
        animationUtils.collectAllCurveNodes()

        if os.path.exists(filePath):
            os.remove(filePath)

        filePath = filePath.replace('.fbo', '')

        animationUtils.exportAnimation(filePath)

        os.rename('{}.fbx'.format(filePath), 
                  '{}.fbo'.format(filePath))


        successStream = '{} was saved properly'.format(filePath)
        QtGui.QMessageBox.information(None,
                                      'Fps Camera animation Status', 
                                      successStream)

        return

    def _browseimportFile(self):
        currentDirectory = os.path.dirname(__file__)

        filePath, cancelState = QtGui.QFileDialog.getOpenFileName(self, 
                                                                  'Open FpsCamera Animation', 
                                                                  currentDirectory , 
                                                                  "*.fbo")

        if not cancelState:
            return

        self.animationExporterField.setText(filePath)

        if not os.path.isdir(os.path.dirname(filePath)):
            return

        animationUtils = fpsCore.FpsAnimationManager(mobu.FBApplication().CurrentCharacter)

        hasAllComponents = animationUtils.importAnimation(filePath)

        if not hasAllComponents:
            errorStream = 'Please Update your character to the latest version' 
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)

            return

        mobu.FBPlayerControl().StepForward()

        Globals.Scene.Evaluate()

        mobu.FBApplication().FlushEventQueue()

        Globals.Scene.EvaluateDeformations()

        Globals.Scene.Evaluate()

    def _invokeFileBrowser(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        if self.saveCurrentAnimation:
            self._browseExportFile()

        else:
            self._browseimportFile()

    def createIoControls(self):
        animationPickerTab = QtGui.QWidget()
        animationPickerLayout = QtGui.QHBoxLayout()

        animationPickerLayout.setContentsMargins(0, 0, 0, 0)
        animationPickerLayout.setSpacing(5)

        self.animationExporterField = QtGui.QLineEdit()
        self.animationFileButton = QtGui.QPushButton("")
        self.swapIcon()

        self.animationExporterField.setReadOnly(True)
        animationPickerLayout.addWidget(self.animationExporterField)
        animationPickerLayout.addWidget(self.animationFileButton)

        animationPickerTab.setLayout(animationPickerLayout)

        return animationPickerTab

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        mainLayout.setContentsMargins(0, 0, 0, 0)

        mainLayout.setSpacing(5)

        self.IoModeWidget = RsBaseWidgets.ExclusiveOptionsWidget(inputLabel='IO Mode',
                                                                 radioButtonLabels=['Save Animation', 
                                                                                    'Load Animation'])

        self.IoModeWidget.radioButtonArray[0].setChecked(True)

        for widget in (self.IoModeWidget,
                       self.createIoControls()):
            mainLayout.addWidget(widget)

        self.setLayout(mainLayout)

        self.IoModeWidget.radioButtonArray[0].toggled.connect(self.swapIcon)

        self.animationFileButton.pressed.connect(self._invokeFileBrowser)

    def _createSeparator(self):
        targetLine = QtGui.QFrame()

        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine
