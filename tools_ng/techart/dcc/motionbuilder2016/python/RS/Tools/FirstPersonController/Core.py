"""
    this module expose all actions related to the first person camera toolkit
"""

from collections import defaultdict
import math
import re

import pyfbsdk as mobu
import unbind

from RS import Globals
from RS.Utils.Scene import Character as characterUtils
from RS.Utils.Scene import Take, Constraint

from RS.Utils import Namespace
from RS import ProjectData

from RS.Utils.Logging import const as loggingConst
from RS.Utils.Scene import Model
from RS.Tools.IkSpline import Utils as ikUtils
from RS.Utils import ContextManagers
from RS.Tools.IkSpline.Widgets import plotProgressWidget
from RS.Utils.Scene import RelationshipConstraintManager as relationConstraintManager
from RS.Core.ReferenceSystem import Manager as assetReferenceManager
from RS.Tools.IkSpline import Utils as ikConstraintUtils

import fbx
import RS.Core.Fbx.fbxObjects as pipelineFbx


_report_MissingComponents = False
# Default offset from mover origin to head height when player character is in bind pose
DEFAULT_VERTICAL_STABILIZATION_OFFSET = 66.0


class FPSRigError(Exception):
    '''
    Base error type used to report errors encountered during FPS camera rig operations.
    '''
    pass


class FPSRigMissingComponentError(FPSRigError):
    '''
    Error raised when a required component of the FPS camera rig cannot be found.
    '''
    pass


class FPSRigMissingPropertyError(FPSRigError):
    '''
    Error raised when a required property of the FPS camera rig cannot be found.
    '''
    pass


def UnselectAll():
    """
        Flush selection in the current scene.
    """
    sceneModelArray = mobu.FBComponentList()
    mobu.FBFindObjectsByName('*' ,
                             sceneModelArray,
                             False,
                             True)

    mobu.FBBeginChangeAllModels()

    for modelObject in sceneModelArray:
        modelObject.Selected = False

    mobu.FBEndChangeAllModels()


class fpsIntermediateRigData(object):
    """
        Data structure used to store information for fps camera preview
    """
    def __init__(self):
        self.fpsNull = None
        self.fpsHelper = None
        self.fpsMatch = None
        self.fpsMatchOffset = None
        self.fpsNullParentConstraint = None
        self.wristEffector  = None
        self.chConstraint  = None
        self.control = None


def purgeOldFpsRigConstraint(chHandNull):
    """
        Will remove invalid constraint from old reference fpscamera rig.
        Args:
            chHandNull (FBModelNull): object to clean up.
    """
    Globals.Scene.Evaluate()

    sourceCount = chHandNull.GetSrcCount()
    constraintList = []
    for index in range(sourceCount):
        driver = chHandNull.GetSrc(index)
        if not isinstance(driver, mobu.FBConstraint) :
            continue
        if not Constraint.IsConstraintType(driver, Constraint.PARENT_CHILD):
            continue

        driver.Active = False
        parentSource = driver.ReferenceGet(1, 0)
        if parentSource is None:
            constraintList.append(driver)

    Globals.Scene.Evaluate()

    for constraint in constraintList:
        constraint.Active = False
        constraint.FBDelete()

    Globals.Scene.Evaluate()


def PrepareFpsTake(inputTake):
    """
        Create a new take to store the FPS hand animation.
        Args:
            inputTake (FBTake):  source take to replicate.

        returns  (FBTake)
    """
    Globals.System.CurrentTake = inputTake
    targetTakeName = '{0}_FirstPersonPreview'.format(inputTake.Name)

    sceneTakes = Take.GetTakeDictionary()

    if targetTakeName in sceneTakes.keys():
        sceneTakes[targetTakeName].FBDelete()

    fpsTake = Globals.System.CurrentTake.CopyTake(targetTakeName)

    Globals.System.CurrentTake = fpsTake

    return fpsTake


def SetPlotOptions():
    """
        returns  (FBPlotOptions).
    """
    #Code from https://github.com/CountZer0/PipelineConstructionSet/blob/master/python/moBu/core/moBuCore.py
    #set plot options for character , must create blank character and set options during blank character's plot
    dummyCharacter = mobu.FBCharacter("dummyChar")
    lOptions = mobu.FBPlotOptions ()
    lOptions.PlotOnFrame = True
    lOptions.PlotAllTakes = False
    lOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
    lOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterNone
    lOptions.UseConstantKeyReducer = False
    lOptions.ConstantKeyReducerKeepOneKey = False
    lOptions.PlotTranslationOnRootOnly = False
    lOptions.PreciseTimeDiscontinuities = False
    dummyCharacter.PlotAnimation(mobu.FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, lOptions)
    dummyCharacter.FBDelete()

    return lOptions


def SetHelperPlotOptions():
    """
        returns  (FBPlotOptions).
    """
    plotOptions = mobu.FBPlotOptions()
    plotOptions.PlotLockedProperties = False
    plotOptions.PlotOnFrame = True
    plotOptions.PlotAllTakes = False
    plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
    plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterNone
    plotOptions.UseConstantKeyReducer = False
    plotOptions.ConstantKeyReducerKeepOneKey = False

    return plotOptions


def bakeAllCharacterParts(inputCharacter):
    """
        Plot animation on hte provided inputCharacter.
        Args:
            inputCharacter (FBCharacter):  source Character to use in this setup.
    """
    #On some shot/animation we need to plot the current character skeleton[ on the currentTake which should be an Fps preview CH Take ].
    #There seems to have a bug with FBCharacter.PlotAnimation
    characterPlotOptions = SetPlotOptions()

    #Lets key on current take as a security measure
    Model.UnselectAll()
    rigElements = characterUtils.GetControlRigElements(inputCharacter)

    for control in rigElements['fkControls']:
        control.Selected = True
    for control in rigElements['ikControls']:
        control.Selected = True

    characterPlotOptions.PlotLockedProperties = False
    Globals.System.CurrentTake.PlotTakeOnSelected(characterPlotOptions)

    characterPlotTarget= mobu.FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton
    inputCharacter.PlotAnimation(characterPlotTarget, characterPlotOptions)
    Globals.Scene.Evaluate()

    characterPlotTarget= mobu.FBCharacterPlotWhere.kFBCharacterPlotOnControlRig
    inputCharacter.PlotAnimation(characterPlotTarget, characterPlotOptions)
    Globals.Scene.Evaluate()


class PreviewHandleDisabled(object):
    """
        Previous version of the plugins was using the fpsPreview camera to attach th ch_*_hand bone to.

        This context was setting up a temporary constraint to disable the animation on its

        animation controller.
    """
    def __init__(self,
                 inputCharacter):
        self.inputCharacter = inputCharacter

        self.fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)

        self.previewHandle = None

        self.previewHandleConstraint = None

        self.disableNull = None

        self.relationConstraint = None

    def __enter__(self):
        if self.fpsCameraConstraintData is None:
            return

        gyroscope = self.fpsCameraConstraintData.constraint

        self.previewHandle = gyroscope.ReferenceGet(GYROSCOPE_IO.index('PreviewHandle'), 0)

        self.disableNull = mobu.FBModelNull('FpsDisableHandle1')

        self.disableNull.Parent = self.previewHandle.Parent

        self.disableNull.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

        self.disableNull.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

        self.disableNull.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)

        relationConstraintName = 'fpsPreviewHandleDisableConstraint1'

        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)

        self.relationConstraint.SetActive(True)

        self.previewHandleConstraint = self.relationConstraint.GetConstraint()

        self.disableNull.PropertyList.Find('Rotation (Lcl)').SetAnimated(True)

        jointSender = self.relationConstraint.AddSenderBox(self.disableNull)

        jointSender.SetGlobalTransforms(False)

        jointReceiver = self.relationConstraint.AddReceiverBox(self.previewHandle)

        jointReceiver.SetGlobalTransforms(False)

        self.relationConstraint.ConnectBoxes(jointSender,
                                             'Lcl Rotation',
                                             jointReceiver,
                                             'Lcl Rotation')

        Globals.Scene.Evaluate()

    def __exit__(self, exceptionType, value, traceback):
        if self.fpsCameraConstraintData is None:
            return

        if self.previewHandle is None:
            return

        if self.previewHandleConstraint is None:
            return

        self.previewHandleConstraint.Active = False

        self.previewHandleConstraint.FBDelete()

        self.disableNull.FBDelete()

        Globals.Scene.Evaluate()


def PrepareIntermediateRig(inputCharacter,
                           activeComponents,
                           chFpsCameraConstraints,
                           handComponents,
                           plotRigAndSkeleton=True):
    """
        Prepare hand rig with intermediate object.
        Args:
            inputCharacter (FBCharacter):  source Character to use in this setup.
            activeComponents(string list): name of controls to clone animation
            handComponents (list): CH bone(s) to constraint and bake keys.[firsPersonControllerWidget.py -> ToolWidgetDialog._validateVisualizeFpsInputs method]
            plotRigAndSkeleton (bool): bake keys on character parts before creating intermediate CH rig

        returns  (list of fpsIntermediateRigData()) .
    """
    #bug [4329371] - the preview camera influence the PH bones
    #in order to neutralize it we can just mute animation
    #on fpsCameraPreview_Handle
    with PreviewHandleDisabled(inputCharacter):
        if plotRigAndSkeleton:
            #Unselect All
            Model.UnselectAll()
            bakeAllCharacterParts(inputCharacter)

        rigComponent = []
        controlMatrix = mobu.FBMatrix()

        fpsMatchArray = []
        fpsHelperArray = []
        fpsHelperConstraintArray = []
        fpsMatchParentConstraintArray = []

        gyroscopeArray = []
        for index,control in enumerate(handComponents):
            gyroscope =  chFpsCameraConstraints[index].constraint
            cameraHelper = gyroscope.ReferenceGet(GYROSCOPE_IO.index('FpsCamera'), 0)
            _fixInvalidFPSCameraScale(cameraHelper)

            gyroscopeArray.append(gyroscope)
            gyroscope.Active = False
            Globals.Scene.Evaluate()
            drivingPropertyOwner = chFpsCameraConstraints[index].drivingPropertyOwner
            drivingPropertyName = chFpsCameraConstraints[index].drivingPropertyName

            chFpsCameraConstraints[index].secondaryDrivingProperty.Data = False
            chFpsCameraConstraints[index].drivingProperty.Data = False

            Globals.Scene.Evaluate()

            fpsNull = mobu.FBModelNull('FPS_{0}'.format(control.Name))
            fpsNull.Visible = True
            fpsNull.Show = True

            fpsHelper = mobu.FBModelNull('{0}_HELPER'.format(fpsNull.Name))
            fpsHelper.Visible = True
            fpsHelper.Show = True

            fpsMatch = mobu.FBModelNull('{0}_CAMERA_MATCH'.format(fpsNull.Name))
            fpsMatch.Visible = True
            fpsMatch.Show = True

            fpsMatchOffset = mobu.FBModelNull('{0}_CAMERA_MATCH_OFFSET'.format(fpsNull.Name))
            fpsMatchOffset.Visible = True
            fpsMatchOffset.Show = True
            fpsMatchOffset.Parent = fpsMatch

            fpsMatchOffset.Rotation = cameraHelper.PostRotation
            #bug [4555892] - plugin change have change offset
            fpsMatchOffset.Rotation = mobu.FBVector3d(0.0, -90.0, -90.0)

            fpsHelper.Parent = fpsNull

            #Align fpsNull onto CH_Hand Null
            control.GetMatrix(controlMatrix)
            fpsNull.SetMatrix(controlMatrix)

            fpsNullParentConstraint = ApplyParentConstraint(fpsNull,control)

            #Grab cameraHelper source parent in this constraint
            fpsMatchParentConstraint = ApplyParentConstraint(fpsMatch,
                                                             cameraHelper)

            wristEffector = extractWristEffector(inputCharacter,
                                                 control.Name)

            #Be sure to Activate IK here
            reachTranslation = wristEffector.PropertyList.Find('IK Reach Translation')
            reachRotation = wristEffector.PropertyList.Find('IK Reach Rotation')

            #Remove any potential animation on the attributes
            reachTranslation.SetAnimated(False)
            reachRotation.SetAnimated(False)

            #Turn on IK on the Wrist Effector
            reachTranslation.Data = 100.0
            reachRotation.Data = 100.0
            Globals.Scene.Evaluate()

            fpsHelper_parentConstraint = ApplyParentConstraint(fpsHelper,
                                                               wristEffector)

            fpsHelperArray.append(fpsHelper)
            fpsHelperArray.append(fpsNull)
            fpsHelperConstraintArray.append(fpsHelper_parentConstraint)
            fpsHelperConstraintArray.append(fpsNullParentConstraint)

            fpsMatchParentConstraintArray.append(fpsMatchParentConstraint)
            fpsMatchArray.append(fpsMatch)

            componentStructure = fpsIntermediateRigData()

            componentStructure.fpsNull = fpsNull
            componentStructure.fpsHelper = fpsHelper
            componentStructure.fpsMatch = fpsMatch
            componentStructure.fpsMatchOffset = fpsMatchOffset
            componentStructure.fpsNullParentConstraint = fpsNullParentConstraint
            componentStructure.wristEffector = wristEffector
            componentStructure.chConstraint = chFpsCameraConstraints[index]
            componentStructure.chControl = control

            rigComponent.append(componentStructure)

        #Unselect All
        Model.UnselectAll()
        for gyroscope in gyroscopeArray :
            gyroscope.Active = True

        Globals.Scene.Evaluate()

        #Bake key on fpsHelper objectlist
        for fpsHelper in fpsHelperArray:
            fpsHelper.Selected = True

        plotOptions = SetHelperPlotOptions()

        Globals.System.CurrentTake.PlotTakeOnSelected(plotOptions)

        for constraintObject in fpsHelperConstraintArray:
            constraintObject.Active = False
            constraintObject.FBDelete()

        Globals.Scene.Evaluate()

        #now we will create a Null following the fps camera in worldSpace
        #Unselect All then bake keys
        Model.UnselectAll()

        for fpsMatch in fpsMatchArray:
            fpsMatch.Selected = True

        Globals.System.CurrentTake.PlotTakeOnSelected(plotOptions)

        for constraintObject in fpsMatchParentConstraintArray:
            constraintObject.Active = False
            constraintObject.FBDelete()

        Globals.Scene.Evaluate()

        return rigComponent


class TempAnimLayerContext(object):
    '''
    Context manager that ensures that there is an anim layer at the top of the layer stack
    of the given type. A temporary layer will be created for the duration of the context
    and then deleted.
    '''
    def __init__(self, layerMode, layerUtils=None):
        self.layerUtils = layerUtils
        if not self.layerUtils:
            self.layerUtils = FpsAnimLayerUtils()
        self.layerMode = layerMode
        self._tempLayer = None
    
    def __enter__(self):
        self._tempLayer = self.layerUtils.createTempAnimLayer(self.layerMode)

        # Select the layer at the top of the stack
        self.layerUtils.selectTopMostAnimationLayer()
        
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._tempLayer is None:
            return
        # Clean up the temp layer if one was created
        try:
            self._tempLayer.FBDelete()
        except unbind.UnboundWrapperError:
            pass


def InitializeChBones(takeInputs):
    # Enable constraint between CH_*_Hand and the camera
    # bug [4587247] - the plugin need to be update to correct the constraining
    # on the skelHead instead of camera

    overrideConstraintArray = []

    for chBone, fpsConstraint in zip(takeInputs.chBones, takeInputs.fpsCameraConstraints):
        fpsConstraint.secondaryDrivingProperty.Data = True
        fpsConstraint.drivingProperty.Data = True

        gyroscope = fpsConstraint.constraint
        cameraHelper = gyroscope.ReferenceGet(GYROSCOPE_IO.index('FpsCamera'), 0)
        _fixInvalidFPSCameraScale(cameraHelper)

        currentConstraint = ApplyParentConstraint(chBone, cameraHelper)

        overrideConstraintArray.append(currentConstraint)
        for attribute in currentConstraint.PropertyList:
            if not attribute.Name.endswith('Offset R'):
                continue

            attribute.Data = mobu.FBVector3d(0.0, -90, -90)

        currentConstraint.Lock = True
    
    with TempAnimLayerContext(mobu.FBLayerMode.kFBLayerModeAdditive) as layerCtx:
        animUtils = ikUtils.JointCloud()
        with ContextManagers.ClearModelSelection():
            animUtils.bakeJointComponents(takeInputs.chBones, translateRotateOnly=True)
    
        Globals.Scene.Evaluate()
    
        # bug [4587247] - make sure the data is saved on top layer then merge on baseAnimation
        layerCtx.layerUtils.mergeAnimationOnBaseLayer(takeInputs.chBones)

    for constraint in overrideConstraintArray:
        constraint.Enable = False
        constraint.FBDelete()

    Globals.Scene.Evaluate()


class BakeMatrixWorlspaceState(object):
    """
        Plotting  using regular method doesnt gives relaible result.

        In order to deliver consistent behaviour we will extract and write the related information for each frameIndex
        in the provided time range.

        This context pass matrix information between object.
    """
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    def __init__(self,
                 inputComponentArray,
                 componentName='CH hand components',
                 translateRotateOnly=False):
        self.assetProgress = None
        
        self.translateRotateOnly = translateRotateOnly

        self.inputComponentArray = inputComponentArray

        self.targetConstraintArray = []

        self.componentName = componentName

    def exposeTakeRange(self, usePlayerControl=True):
        lPlayer = mobu.FBPlayerControl()
        startTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        stopTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        return startTime, stopTime, lPlayer

    def keyCurrentFrame(self,
                        frameIndex,
                        lPlayer):
        targetTime = mobu.FBTime(0, 0, 0, frameIndex, 0)
        lPlayer.Goto(targetTime)

        Globals.Scene.Evaluate()

        for control in self.inputComponentArray:
            bindMatrix = mobu.FBMatrix()
            control.worldTarget.GetMatrix(bindMatrix)
            control.sourceJoint.SetMatrix(bindMatrix)

        Globals.Scene.Evaluate()

        for animationNode in self.animationNodes:
            animationNode.KeyCandidate()
        
        if not self.translateRotateOnly:
            lPlayer.Key()

    def forceAnimationPlot(self):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        self.animationNodes =[]

        reportMessage = '   MESSAGE [Setting keys on {componentName}] \n\t  percent done:{percent}\n\t  Processing frame{itemName}'
        progressRatio = 1.0/float(stopTime-startTime)
        progressIncrement = 0

        self.uiTextColor = ikUtils.const.MessageTextColours.Minor_Update

        self.assetProgress.show()

        for control in self.inputComponentArray:
            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = control.sourceJoint.PropertyList.Find(property)

                if not inputAttribute.IsAnimated():
                    inputAttribute.SetAnimated(True)

                animationNode = inputAttribute.GetAnimationNode()
                self.animationNodes.append(animationNode)
                inputAttribute.SetFocus(True)

            control.sourceJoint.Selected = True

        for timeIndex in range(startTime, stopTime+1):
            progressValue = progressIncrement*progressRatio*100.0

            self.keyCurrentFrame(timeIndex,
                                 lPlayer)

            progressIncrement+=1
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                                            itemName=timeIndex,
                                                            componentName=self.componentName),
                                                            textcolor=self.uiTextColor)

            self.assetProgress._forceDrawOverride()
        self.assetProgress.hide()

    def __enter__(self):
        componentCount = len(self.inputComponentArray)-1
        if componentCount == 0:
            componentCount = 1

        progressRatio = 1.0/float(componentCount)
        progressValue = 0.0

        self.assetProgress = plotProgressWidget.PeltProgressWindowDialog()
        self.assetProgress.show()

        reportMessage = '   MESSAGE [Transferring worldspace->Source joint] \n\t  percent done:{percent}\n\t  Processing {itemName}'

        self.forceAnimationPlot()

        return self

    def __exit__(self, exceptionType, value, traceback):
        self.assetProgress.close()

        mobu.FBSystem().Scene.Evaluate()

        for control in self.inputComponentArray:
            control.sourceJoint.Selected = False

            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = control.sourceJoint.PropertyList.Find(property)
                inputAttribute.SetFocus(False)

        mobu.FBSystem().Scene.Evaluate()


def BakeCHRigOnFpsTake(fpsRigStructure,
                       inputCharacter,
                       deleteChDebugRig=True):
    """
        Bake CH keys and clean up intermediate objects/constraints.

        Args:
            fpsRigStructure (list of fpsIntermediateRigData()):  list produced by PrepareIntermediateRig function.
            inputCharacter (FBCharacter):  source Character to use in this setup.
            deleteChDebugRig (bool): flag used to debug if temporay rig.
    """
    fpsMatchParentConstraintArray = []
    wristEffectorParentConstraintArray = []
    elementsToPlot = []
    Globals.Scene.Evaluate()

    bakeHelperConstraints = []

    #bug [4329371] - disable the preview camera Handle
    with PreviewHandleDisabled(inputCharacter):
        transferAnimationComponents = []

        for fpsRig in fpsRigStructure:
            ClearAuxillaryEffector(inputCharacter,
                                   fpsRig.chControl.Name)

        Globals.Scene.Evaluate()

        for fpsRig in fpsRigStructure:
            fpsRig.wristEffector = extractWristEffector(inputCharacter,
                                                        fpsRig.chControl.Name)

        for fpsRig in fpsRigStructure:
            wristEffector = extractWristEffector(inputCharacter,
                                                 fpsRig.chControl.Name)

            if wristEffector is None:
                wristEffector = fpsRig.wristEffector

            currentConstraint = ApplyParentConstraint(fpsRig.fpsHelper,
                                                      wristEffector)

            bakeHelperConstraints.append(currentConstraint)
            transferAnimationComponents.append(fpsRig.fpsHelper)

        animUtils = ikUtils.JointCloud()

        with ContextManagers.ClearModelSelection():
            animUtils.bakeJointComponents(transferAnimationComponents,
                                          translateRotateOnly=True)

        for wristConstraint in bakeHelperConstraints:
            wristConstraint.Active = False
            wristConstraint.FBDelete()

        Globals.Scene.Evaluate()

        bindData = []
        for fpsRig in fpsRigStructure:
            bindData.append(ikUtils.TransferComponent(fpsRig.wristEffector,
                                                      fpsRig.fpsHelper))

        bakeStatus = False
        with BakeMatrixWorlspaceState(bindData, translateRotateOnly=True):
            bakeStatus = True

        Globals.Scene.Evaluate()

        bindData = []
        for fpsRig in fpsRigStructure:
            bindData.append(ikUtils.TransferComponent(fpsRig.fpsMatchOffset,
                                                      fpsRig.fpsNull))

        bakeStatus = False
        with BakeMatrixWorlspaceState(bindData, translateRotateOnly=True):
            bakeStatus = True

        Globals.Scene.Evaluate()

        bindData = []
        for fpsRig in fpsRigStructure:
            bindData.append(ikUtils.TransferComponent(fpsRig.fpsHelper,
                                                      fpsRig.wristEffector))

        bakeStatus = False
        with BakeMatrixWorlspaceState(bindData, translateRotateOnly=True):
            bakeStatus = True

        #Unselect All
        Model.UnselectAll()
        bakeAllCharacterParts(inputCharacter)
        
        Globals.Scene.Evaluate()

        if deleteChDebugRig:
            for fpsRig in fpsRigStructure:
                #Delete FPS_CH_R_Hand and FPS_R_Helper
                fpsRig.fpsNull.FBDelete()
                fpsRig.fpsHelper.FBDelete()
                fpsRig.fpsMatch.FBDelete()
                fpsRig.fpsMatchOffset.FBDelete()

        for fpsRig in fpsRigStructure:
            fpsRig.chConstraint.secondaryDrivingProperty.Data = False
            fpsRig.chConstraint.drivingProperty.Data = False

            Globals.Scene.Evaluate()

        Globals.Scene.Evaluate()

        for fpsRig in fpsRigStructure:
            wristEffector = fpsRig.wristEffector
            reachTranslation = wristEffector.PropertyList.Find('IK Reach Translation')
            reachRotation = wristEffector.PropertyList.Find('IK Reach Rotation')

            #Turn off IK on the Wrist Effector
            reachTranslation.Data = 0.0
            reachRotation.Data = 0.0


GYROSCOPE_IO = ('DeformedMesh',
                'FpsCamera',
                'FpsCameraPreview',
                'ParentAnchor',
                'OrientTarget1',
                'OrientTarget2',
                'CameraPivot',
                'PreviewHandle',
                'ConeOffset',
                'ConeFollow',
                'ExportPropertyDriver',
                'Left_CH_Helper',
                'Right_CH_Helper',
                'ExportPivot',
                'ScaleRoot',
                'SKEL_Root',
                'MOVER',
                'ExportAttributeTarget',
                'WorldMover',
                'Aim Target',
                'Up Target',
                'Cone Master Zero')


class gyroscopeData(object):
    """
        Data structure used to store information from the custom constraint
        headGyroscope.

        ({project}/tools/release/dcc/current/motionbuilder2016/plugins/x64/headGyroscope.dll)
    """
    def __init__(self):
        self.constraint = None
        self.drivingProperty = None
        self.drivingPropertyName = None
        self.drivingPropertyOwner = None
        self.secondaryDrivingProperty = None


def ExposeGyroscopeConstraintData(handComponent):
    """
        Try to locate the embbed constraint attached to CH_*_Hand bone.
        Args:
            handComponent (FBModel): input CH_*_Hand bone.

        returns gyroscopeData()
    """
    LEGAL_GYROSCOPE_TYPE = ('headGyroscope',
                            'fpsHeadGyroscope')

    headGyroscopes = []
    for constraint in mobu.FBSystem().Scene.Constraints:
        if constraint.ClassName() in LEGAL_GYROSCOPE_TYPE:
            headGyroscopes.append(constraint)

    if len(headGyroscopes) == 0:
        return None

    chProperties = ['Left_CH_Helper', 'Right_CH_Helper']
    chDrivers = ['activate_L_constraint', 'activate_R_constraint']

    for gyroscope in headGyroscopes:
        for chIndex, chProperty in enumerate(chProperties):
            chNode = gyroscope.ReferenceGet(GYROSCOPE_IO.index(chProperty), 0)
            if chNode is None:
                continue

            if chNode == handComponent:
                drivingProperty = gyroscope.PropertyList.Find(chDrivers[chIndex])
                if drivingProperty is None:
                    continue

                outputData = gyroscopeData()
                outputData.constraint = gyroscope
                outputData.drivingPropertyName = chDrivers[chIndex]

                outputData.drivingPropertyOwner = gyroscope
                outputData.secondaryDrivingProperty = drivingProperty
                outputData.drivingProperty = drivingProperty

                fpsRigDriver = gyroscope.ReferenceGet(GYROSCOPE_IO.index('ExportPropertyDriver'), 0)
                if fpsRigDriver is not None:
                    drivingProperty = fpsRigDriver.PropertyList.Find(chDrivers[chIndex])
                    if drivingProperty is None:
                        continue
                    outputData.drivingPropertyOwner = fpsRigDriver
                    outputData.drivingProperty = drivingProperty

                return outputData

    return None


def ExposeIkToMeConstraint(handComponent):
    """
        Try to locate the embbed constraint attached to CH_*_Hand bone.
        Args:
            handComponent (FBModel): input CH_*_Hand bone.

        returns (FBConstraint)
    """
    sourceCount = handComponent.GetSrcCount()
    for index in range(sourceCount):
        driver = handComponent.GetSrc(index)
        if not isinstance(driver, mobu.FBConstraint) :
            continue
        if not Constraint.IsConstraintType(driver, Constraint.PARENT_CHILD):
            continue
        return driver

    return None


def ApplyParentConstraint(drivenObject,
                          sourceObject,
                          useRotationConstraint=False):
    """
        Apply Parent-Child constraint between drivenObject/sourceObject.
        Args:
            drivenObject (FBModel) .
            sourceObject (FBModel) .

        returns (FBConstraint)
    """
    if useRotationConstraint is False:
        parentConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
    else:
        parentConstraint = Constraint.CreateConstraint(Constraint.ROTATION)

    parentConstraint.Name = '{0}_PrntCtrst1'.format(drivenObject.Name)

    parentConstraint.ReferenceAdd(0,drivenObject)
    parentConstraint.ReferenceAdd(1,sourceObject)
    parentConstraint.Active = True

    for attribute in parentConstraint.PropertyList:
        if re.search("Offset (T|R)", attribute.Name):
            attribute.Data = mobu.FBVector3d()

    if useRotationConstraint is True:
        for attribute in parentConstraint.PropertyList:
            if 'rotation' in attribute.Name.lower():
                attribute.Data = mobu.FBVector3d()
                mobu.FBSystem().Scene.Evaluate()

    return parentConstraint


def extractWristEffector(inputCharacter,
                         side):
    """
        Try to locate an ik wrist controller from the inputCharacter .
        Args:
            inputCharacter (FBCharacter):  source Character to use in this setup.
            side (str) : CH bone name we want to find [see ProjectData.data.GetFpsChBones() ]

        returns (FBModelMarker)
    """
    targetEffector = None
    wristEffector = None

    chBones = ProjectData.data.GetFpsChBones()

    if side == chBones[0]:
        targetEffector = mobu.FBEffectorId.kFBRightWristEffectorId
    elif side == chBones[1]:
        targetEffector = mobu.FBEffectorId.kFBLeftWristEffectorId
    else:
        return None

    for enum in mobu.FBEffectorId.values:
        lEffectorId = mobu.FBEffectorId.values[enum]
        lModelIK = inputCharacter.GetEffectorModel(lEffectorId)

        if lEffectorId is not targetEffector:
            continue

        if lModelIK != None:
            wristEffector = lModelIK
            break

    return wristEffector


def ClearAuxillaryEffector(inputCharacter,
                           side):
    targetEffectorId = None
    chBones = ProjectData.data.GetFpsChBones()

    if side == chBones[0]:
        targetEffectorId = mobu.FBEffectorId.kFBRightWristEffectorId

    elif side == chBones[1]:
        targetEffectorId = mobu.FBEffectorId.kFBLeftWristEffectorId

    else:
        return None

    currentControlSet = inputCharacter.GetCurrentControlSet()
    if not currentControlSet:
        return


    ikCount = currentControlSet.GetIKEffectorPivotCount(targetEffectorId)
    if ikCount < 1:
        return

    for index in range(ikCount-1, -1, -1):
        effectorNode = currentControlSet.GetIKEffectorModel(targetEffectorId, 
                                                            index)

        if not effectorNode:
            continue

        effectorNode.FBDelete()


class FpsAnimLayerUtils(object):
    """
        Utility class helping managing plotting on top most layer / then transferring it on baseLayer.
    """
    def __init__(self):
        self.currentTake = None

    def togglePropertyHighlight(self,
                                inputComponentArray,
                                toggleState):
        for control in inputComponentArray:
            control.Selected = toggleState

            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = control.PropertyList.Find(property)
                inputAttribute.SetFocus(toggleState)

        mobu.FBSystem().Scene.Evaluate()
    
    def topLayerMatchesMode(self, layerMode):
        '''
        Checks whether the top layer on the layer stack matches the given layer mode
        Parameters:
            layerMode (mobu.FBLayerMode): The desired mode to check for
        '''
        currentTake = mobu.FBSystem().CurrentTake
        subLayerCount = currentTake.GetLayerCount()
        topLayer = currentTake.GetLayer(subLayerCount - 1)
        if topLayer.LayerMode == layerMode:
            return True
        return False
    
    def createTempAnimLayer(self, layerMode):
        '''
        Creates a new override layer at the top of the stack and returns it
        '''
        currentTake = mobu.FBSystem().CurrentTake
        currentTake.CreateNewLayer()
        newLayerIdx = currentTake.GetLayerCount() - 1
        newLayer = currentTake.GetLayer(newLayerIdx)
        newLayer.LayerMode = layerMode
        newLayer.Name = "FPSCameraToolboxTempPlotLayer"
        return newLayer
    
    def selectTopMostAnimationLayer(self):
        '''
        Selects the animation layer at the top of the layer stack.
        '''
        currentTake = mobu.FBSystem().CurrentTake
        subLayerCount = currentTake.GetLayerCount()
        currentTake.SetCurrentLayer(subLayerCount - 1)

    def mergeAnimationOnBaseLayer(self,
                                  transferAnimationComponents):
        currentTake = mobu.FBSystem().CurrentTake

        self.togglePropertyHighlight(transferAnimationComponents,
                                     True)

        currentTake.MergeLayers(mobu.FBAnimationLayerMergeOptions.kFBAnimLayerMerge_AllLayers_SelectedProperties,
                                False,
                                mobu.FBMergeLayerMode.kFBMergeLayerModeAutomatic)

        self.togglePropertyHighlight(transferAnimationComponents,
                                     False)


class TransferWorlstateBetweenLayer(object):
    """
        Plotting  using regular method doesnt gives relaible result.

        In order to deliver consistent behaviour we will extract and write the related information for each frameIndex
        in the provided time range.

        This context will plot animation on the top layer.
        later on the keys sampled will be trasnferred on another layer/Take

        [mostly useful for finializing fpscamera preview work]
    """
    def __init__(self,
                 inputNode,
                 componentName='Ch components'):
        self.assetProgress = None

        self.componentName = componentName

        self.inputNode = inputNode

        self.translateCurveValues = []

        self.rotateCurveValues = []

        self.forceAnimationSampling()

    def getCurrentFrame(self,
                        frameIndex,
                        lPlayer):
        targetTime = mobu.FBTime(0, 0, 0, frameIndex, 0)
        lPlayer.Goto(targetTime)

        Globals.Scene.Evaluate()

        self.translateCurveValues.append(self.translateAttribute.Data)

        self.rotateCurveValues.append(self.rotateAttribute.Data)

    def setCurrentFrame(self,
                        frameIndex,
                        lPlayer,
                        progressIncrement):
        targetTime = mobu.FBTime(0, 0, 0, frameIndex, 0)
        lPlayer.Goto(targetTime)

        Globals.Scene.Evaluate()

        self.translateAttribute.Data = self.translateCurveValues[progressIncrement]

        self.rotateAttribute.Data = self.rotateCurveValues[progressIncrement]

        self.translateAttribute.GetAnimationNode().KeyCandidate(targetTime)

        self.rotateAttribute.GetAnimationNode().KeyCandidate(targetTime)

    def forceAnimationPlot(self):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        self.translateAttribute = self.inputNode.PropertyList.Find('Translation (Lcl)')

        if not self.translateAttribute.IsAnimated():
            self.translateAttribute.SetAnimated(True)

        self.rotateAttribute = self.inputNode.PropertyList.Find('Rotation (Lcl)')

        if not self.rotateAttribute.IsAnimated():
            self.rotateAttribute.SetAnimated(True)
        
        # Clamp the stop time to ensure there aren't more frames than we have samples for
        numFramesInTake = stopTime - startTime + 1
        numSampledKeys = len(self.translateCurveValues)
        if numFramesInTake > numSampledKeys:
            stopTime = startTime + numSampledKeys
        
        reportMessage = '   MESSAGE [Reading keys  Values from {componentName}] \n\t  percent done:{percent}\n\t  Processing frame{itemName}'
        progressRatio = 1.0/float(stopTime-startTime)
        progressIncrement = 0

        self.uiTextColor = loggingConst.MessageTextColours.Minor_Update

        self.assetProgress = plotProgressWidget.PeltProgressWindowDialog()
        self.assetProgress.show()

        for timeIndex in range(startTime, stopTime+1):
            progressValue = progressIncrement*progressRatio*100.0
            
            if (progressIncrement >= len(self.translateCurveValues)):
                # Current take length is longer than the length of the buffered data
                # so bail here to avoid an index out of range exception (url:bugstar:5414150)
                break
            
            self.setCurrentFrame(timeIndex,
                                 lPlayer,
                                 progressIncrement)

            progressIncrement += 1
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                                            itemName=timeIndex,
                                                            componentName=self.componentName),
                                                            textcolor=self.uiTextColor)
            self.assetProgress._forceDrawOverride()

        self.assetProgress.hide()
        self.assetProgress.close()

    def forceAnimationSampling(self):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        self.translateAttribute = self.inputNode.PropertyList.Find('Translation (Lcl)')

        if not self.translateAttribute.IsAnimated():
            self.translateAttribute.SetAnimated(True)

        self.rotateAttribute = self.inputNode.PropertyList.Find('Rotation (Lcl)')

        if not self.rotateAttribute.IsAnimated():
            self.rotateAttribute.SetAnimated(True)

        reportMessage = '   MESSAGE [Reading keys  Values from {componentName}] \n\t  percent done:{percent}\n\t  Processing frame{itemName}'
        progressRatio = 1.0/float(stopTime-startTime)
        progressIncrement = 0

        self.uiTextColor = loggingConst.MessageTextColours.Minor_Update


        self.assetProgress = plotProgressWidget.PeltProgressWindowDialog()
        self.assetProgress.show()

        for timeIndex in range(startTime, stopTime+1):
            progressValue = progressIncrement*progressRatio*100.0

            self.getCurrentFrame(timeIndex,
                                 lPlayer)

            progressIncrement+=1
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                                            itemName=timeIndex,
                                                            componentName=self.componentName),
                                                            textcolor=self.uiTextColor)
            self.assetProgress._forceDrawOverride()
        self.assetProgress.hide()
        self.assetProgress.close()

        self.assetProgress = None

    def exposeTakeRange(self, usePlayerControl=True):
        lPlayer = mobu.FBPlayerControl()
        startTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        stopTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        return startTime, stopTime, lPlayer

    def injectAnimation(self):
        self.forceAnimationPlot()


def _fixInvalidFPSCameraScale(fpsCam, scaleTolerance=0.0001):
    '''
    Fix to ensure the FPS camera is not scaled to 0 as this can result in invalid matrices being
    set on objects that are constrained to it (url:bugstar:5402381)
    
    Return (bool): True if the fix was applied or False if not
    '''
    if fpsCam is None:
        return False
    scaleValid = True
    scaleProp = fpsCam.PropertyList.Find("Scaling (Lcl)")
    for value in scaleProp.Data:
        if math.isnan(value):
            scaleValid = False
            break
        if abs(value - 1.0) > scaleTolerance:
            scaleValid = False
            break
    if scaleValid:
        return False
    if scaleProp.IsLocked:
        scaleProp.SetLocked(False)
    if scaleProp.IsAnimated:
        scaleProp.SetAnimated(False)
    Globals.Scene.Evaluate()
    scaleProp.Data = mobu.FBVector3d(1.0, 1.0, 1.0)
    scaleProp.SetLocked(True)
    return True


def FinalizeFpsHandAnimation(previewTake,
                             targetTake,
                             TargetComponentList,
                             fpsCameraConstraints):
    """
        Produce the final fps animation keys and clean up intermediate rig.
        Args:
            previewTake (FBTake): source FirstPersonPreview take .
            targetTake (FBTake)
            TargetComponentList (list of FBModel): CH bone to key
            fpsCameraConstraints (list of gyroscopeData): constraints attached
            between fpsCamera and CH bone.

        returns .
    """
    layerUtils = FpsAnimLayerUtils()
    # Enable constraint between CH_*_Hand and the camera
    # bug [4587247] - the plugin need to be update to correct the constraining
    # on the skelHead instead of camera
    overrideConstraintArray = []

    for itemIndex, fpsConstraint in enumerate(fpsCameraConstraints):
        fpsConstraint.secondaryDrivingProperty.Data = True
        fpsConstraint.drivingProperty.Data = True

        gyroscope = fpsConstraint.constraint
        cameraHelper = gyroscope.ReferenceGet(GYROSCOPE_IO.index('FpsCamera'), 0)
        _fixInvalidFPSCameraScale(cameraHelper)

        currentConstraint = ApplyParentConstraint(TargetComponentList[itemIndex],
                                                  cameraHelper)

        overrideConstraintArray.append(currentConstraint)
        for attribute in currentConstraint.PropertyList:
            if not attribute.Name.endswith('Offset R'):
                continue

            attribute.Data = mobu.FBVector3d(0.0, -90, -90)

        currentConstraint.Lock = True

    animUtils = ikUtils.JointCloud()

    transferAnimationComponents = []
    
    #bug [4587247] - make sure the data is save on top layer then merge on baseAnimation
    with TempAnimLayerContext(mobu.FBLayerMode.kFBLayerModeAdditive, layerUtils=layerUtils):
        with ContextManagers.ClearModelSelection():
            animUtils.bakeJointComponents(TargetComponentList)

        Globals.Scene.Evaluate()
        layerUtils.mergeAnimationOnBaseLayer(TargetComponentList)

    #Disable constraint between CH_*_Hand and the camera
    for fpsConstraint in fpsCameraConstraints:
        fpsConstraint.secondaryDrivingProperty.Data = False
        fpsConstraint.drivingProperty.Data = False

    for constraint in overrideConstraintArray:
        constraint.Enable = False
        constraint.FBDelete()

    Globals.Scene.Evaluate()

    curveValueArray = [TransferWorlstateBetweenLayer(node) for node in TargetComponentList]

    Globals.System.CurrentTake = targetTake

    Globals.Scene.Evaluate()
    
    with TempAnimLayerContext(mobu.FBLayerMode.kFBLayerModeOverride, layerUtils=layerUtils):
        for node in curveValueArray:
            node.injectAnimation()
    
        Globals.Scene.Evaluate()
    
        layerUtils.mergeAnimationOnBaseLayer(TargetComponentList)


def ExtractFpsCameraConstraint(inputCharacter):
    """
        Utility function which will try to find fpscamera rig from a provide FBCharacter
    """
    handComponent = ProjectData.data.GetFpsChBones()[0]
    characterNameSpace = Namespace.GetNamespace(inputCharacter)
    if characterNameSpace is None:
        characterNameSpace = ''

    namespacePrefix = '{0}:{1}'.format(characterNameSpace, handComponent)
    if len(characterNameSpace) == 0:
        namespacePrefix = '{}'.format(handComponent)

    handComponent = mobu.FBFindModelByLabelName(namespacePrefix)
    if handComponent is None:
        return None

    chConstraint = ExposeGyroscopeConstraintData(handComponent)
    return chConstraint


def ExposeTakeRange():
    """
        Take range utility
    """
    lPlayer = mobu.FBPlayerControl()
    startTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart()
    stopTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop()

    return startTime, stopTime


def SetTakeRange(startTime, stopTime):
    """
        Take range utility
    """
    lPlayer = mobu.FBPlayerControl()
    targetTimeSpan = mobu.FBTimeSpan(startTime, stopTime)
    mobu.FBSystem().CurrentTake.LocalTimeSpan = targetTimeSpan
    Globals.Scene.Evaluate()


class TakeRangeActive(object):
    """
        Context which can restore the original take range once a task is finished
    """
    def __init__(self,
                 undoStackName='',
                 startRangeFrame=None,
                 endRangeFrame=None):
        self.__name = undoStackName
        self.__startRangeFrame = startRangeFrame
        self.__endRangeFrame = endRangeFrame

        self._startTime = None
        self._stopTime = None

    def __enter__(self):
        self._startTime, self._stopTime = ExposeTakeRange()
        SetTakeRange(self.__startRangeFrame, self.__endRangeFrame)

    def __exit__(self, exceptionType, value, traceback):
        SetTakeRange(self._startTime, self._stopTime)


class TemporaryNulls(object):
    """
        context which will guaranty temporary locators are clean up,
        once a task is finished.
    """
    def __init__(self):
        self._nullArray = []

    def __enter__(self):
        self._nullArray = []

        return self

    def __exit__(self, 
                 exceptionType, 
                 value, 
                 traceback):
        Globals.Scene.Evaluate()

        for null in set(self._nullArray):
            null.FBDelete()

        Globals.Scene.Evaluate()


def PlotAnimation(inputObject):
    """
        Plot Animation on selected properties.
    """
    Globals.Scene.Evaluate()
    Model.UnselectAll()

    plotOptions = SetHelperPlotOptions()
    inputObject.Selected = True

    Globals.System.CurrentTake.PlotTakeOnSelected(plotOptions)
    Globals.Scene.Evaluate()
    inputObject.Selected = False


def PlotProperty(inputObject,
                 playerControl,
                 startTime,
                 endTime,
                 plotCurrentFrame=False,
                 plotTranslation=False):
    """
        Function which will expose alterante way of baking aniamtion
    """

    rotationProperty = inputObject.PropertyList.Find('Rotation (Lcl)')
    rotationProperty.SetLocked(False)

    if not rotationProperty.IsAnimated():
        rotationProperty.SetAnimated(True)

    rotationProperty.SetFocus(True)
    rotationAnimNode = rotationProperty.GetAnimationNode()

    if plotTranslation is True:
        translationProperty = inputObject.PropertyList.Find('Translation (Lcl)')
        translationProperty.SetLocked(False)
        translationProperty.SetAnimated(True)
        translationAnimNode = translationProperty.GetAnimationNode()
        translationProperty.SetFocus(True)

    if plotCurrentFrame is True:
        mobu.FBSystem().Scene.Evaluate()
        sampleValue = rotationProperty.Data

        rotationProperty.Data = mobu.FBVector3d(sampleValue[0],
                                                sampleValue[1],
                                                sampleValue[2])

        rotationAnimNode.KeyCandidate()
        playerControl.Key()

        rotationProperty.SetFocus(False)
        return

    frameRange = (endTime.GetFrame() - startTime.GetFrame()) + 1
    playerControl.Goto(startTime)

    for frameIndex in xrange(frameRange):
        mobu.FBSystem().Scene.Evaluate()
        sampleValue = rotationProperty.Data

        rotationProperty.Data = mobu.FBVector3d(sampleValue[0],
                                                sampleValue[1],
                                                sampleValue[2])

        rotationAnimNode.KeyCandidate()
        if plotTranslation is True:
            sampleValue = translationProperty.Data

            translationProperty.Data = mobu.FBVector3d(sampleValue[0],
                                                       sampleValue[1],
                                                       sampleValue[2])

            translationAnimNode.KeyCandidate()

        playerControl.Key()
        playerControl.StepForward()

    rotationProperty.SetFocus(False)
    if plotTranslation is True:
        translationProperty.SetFocus(False)


def SampleValueAtFrame(inputProperty,
                       playerControl,
                       targetTime):
    """
        Sampling  Utility
    """
    playerControl.Goto(targetTime)
    Globals.Scene.Evaluate()
    sampledValue = inputProperty.Data

    return sampledValue


def SetValueAtFrame(inputProperty,
                    playerControl,
                    targetTime,
                    sampleValue,
                    jumpToFrame=True):
    """
        Sampling  Utility
    """
    inputProperty.SetFocus(True)
    if jumpToFrame is True:
        playerControl.Goto(targetTime)
    Globals.Scene.Evaluate()

    inputProperty.Data = sampleValue
    playerControl.Key()
    inputProperty.SetFocus(False)


def BakeResult(inputProperty,
               playerControl,
               inputStartTime,
               inputEndTime):
    """
        Some camera tools need to bake first / last frame in order to consolidate chain animation export
    """
    startValue = SampleValueAtFrame(inputProperty,
                                    playerControl,
                                    inputStartTime)

    endValue = SampleValueAtFrame(inputProperty,
                                  playerControl,
                                  inputEndTime)

    SetValueAtFrame(inputProperty,
                    playerControl,
                    inputStartTime,
                    startValue)

    SetValueAtFrame(inputProperty,
                    playerControl,
                    inputEndTime,
                    endValue)


def RestoreFovDefaultValue(inputCharacter):
    """
        Simple Utility in order to reset camera FOV to its default state
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    with ContextManagers.Undo(undoStackName='RestoreFovDefaultValue',
                              attributeArray=[coneMaster.PropertyList.Find('DOF_FOV')]):
        coneMaster.PropertyList.Find('DOF_FOV').Data = 51.28


def ToggleRotationLockState(inputCharacter):
    """
        Deprecated function:
            the coneMaster rotation now needs to stay unlocked.
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    rotationProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')
    lockState = not rotationProperty.IsLocked()
    rotationProperty.SetLocked(lockState)


def ToggleCameraCenters(inputCharacter,
                        enableState):
    """
        Utility function requested by animators.
    """
    characterNameSpace = Namespace.GetNamespace(inputCharacter)
    if characterNameSpace is None:
        characterNameSpace = ''

    LEGAL_GYROSCOPE_TYPE = ('headGyroscope',
                            'fpsHeadGyroscope')

    headGyroscope = None
    for constraint in mobu.FBSystem().Scene.Constraints:
        if constraint.ClassName() not in LEGAL_GYROSCOPE_TYPE:
            continue

        if characterNameSpace not in constraint.LongName:
            continue

        headGyroscope = constraint

    if headGyroscope is None:
        return

    cameraGroup = ('FpsCamera',
                   'FpsCameraPreview')

    cameraArray = []

    for group in cameraGroup:
        camera = headGyroscope.ReferenceGet(GYROSCOPE_IO.index(group), 0)
        if camera is None:
            continue

        camera.PropertyList.Find('Center').Data = enableState


def TransferConeMasterAnimationToMoverSpace(inputCharacter,
                                            startTime,
                                            endTime):
    """
        Prepare animation the game exporter object
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner
    mover = gyroscope.ReferenceGet(GYROSCOPE_IO.index('MOVER'), 0)

    Globals.Scene.Evaluate()
    coneMasterNull = mobu.FBModelNull('coneMasterNull1')
    coneMasterNull.Visible = True
    coneMasterNull.Show = True

    coneConstraint = ApplyParentConstraint(coneMasterNull, coneMaster)
    playerControl = mobu.FBPlayerControl()

    extendedStartTime = mobu.FBTime()
    extendedStartTime.SetFrame(startTime.GetFrame()-1,
                               mobu.FBTimeMode.kFBTimeMode30Frames)

    extendedEndTime = mobu.FBTime()
    extendedEndTime.SetFrame(endTime.GetFrame()+1,
                             mobu.FBTimeMode.kFBTimeMode30Frames)

    rotationDominance = coneMaster.PropertyList.Find('RotationDominance')
    BakeResult(rotationDominance,
               playerControl,
               extendedStartTime,
               extendedEndTime)

    with TakeRangeActive(startRangeFrame=extendedStartTime,
                         endRangeFrame=extendedEndTime):
        PlotProperty(coneMasterNull, playerControl, extendedStartTime, extendedEndTime)

        coneConstraint.Active = False
        coneConstraint.FBDelete()
        Globals.Scene.Evaluate()

    with TakeRangeActive(startRangeFrame=startTime,
                         endRangeFrame=endTime):
        rotationDominance.SetFocus(True)
        frameRange = (endTime.GetFrame() - startTime.GetFrame()) + 1
        playerControl.Goto(startTime)

        for frameIndex in xrange(frameRange):
            mobu.FBSystem().Scene.Evaluate()
            SetValueAtFrame(rotationDominance,
                            playerControl,
                            startTime,
                            -1.0,
                            jumpToFrame=False)

            playerControl.StepForward()
        rotationDominance.SetFocus(False)

        transferConstraint = ApplyParentConstraint(coneMaster,
                                                   coneMasterNull,
                                                   useRotationConstraint=True)

        PlotProperty(coneMaster, playerControl, startTime, endTime)

    rotationProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')
    BakeResult(rotationProperty,
               playerControl,
               extendedStartTime,
               extendedEndTime)

    transferConstraint.Active = False
    transferConstraint.FBDelete()
    Globals.Scene.Evaluate()

    coneMasterNull.FBDelete()

    scaleProperty = coneMaster.PropertyList.Find('Scaling (Lcl)')
    scaleProperty.SetLocked(False)
    scaleProperty.Data = mobu.FBVector3d(1,1,1)
    scaleProperty.SetLocked(True)


def BakeCurrentFrame(inputCharacter):
    """
        Function requested to transfer animation from mover to head space
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner
    mover = gyroscope.ReferenceGet(GYROSCOPE_IO.index('MOVER'), 0)

    Globals.Scene.Evaluate()
    coneMasterNull = mobu.FBModelNull('coneMasterNull1')
    coneMasterNull.Visible = True
    coneMasterNull.Show = True

    coneConstraint = ApplyParentConstraint(coneMasterNull, coneMaster)
    playerControl = mobu.FBPlayerControl()

    currentTime = mobu.FBSystem().LocalTime

    extendedStartTime = mobu.FBTime()
    extendedStartTime.SetFrame(currentTime.GetFrame()-1,
                               mobu.FBTimeMode.kFBTimeMode30Frames)

    extendedEndTime = mobu.FBTime()
    extendedEndTime.SetFrame(currentTime.GetFrame()+1,
                             mobu.FBTimeMode.kFBTimeMode30Frames)

    rotationDominance = coneMaster.PropertyList.Find('RotationDominance')
    BakeResult(rotationDominance,
               playerControl,
               extendedStartTime,
               extendedEndTime)

    with TakeRangeActive(startRangeFrame=extendedStartTime,
                         endRangeFrame=extendedEndTime):
        PlotProperty(coneMasterNull,
                     playerControl,
                     extendedStartTime,
                     extendedEndTime,
                     plotCurrentFrame=True)

        coneConstraint.Active = False
        coneConstraint.FBDelete()
        Globals.Scene.Evaluate()

    with TakeRangeActive(startRangeFrame=extendedStartTime,
                         endRangeFrame=extendedEndTime):
        rotationDominance.SetFocus(True)
        playerControl.Goto(currentTime)

        mobu.FBSystem().Scene.Evaluate()
        SetValueAtFrame(rotationDominance,
                        playerControl,
                        currentTime,
                        -1.0,
                        jumpToFrame=False)

        rotationDominance.SetFocus(False)

        transferConstraint = ApplyParentConstraint(coneMaster, coneMasterNull)
        PlotProperty(coneMaster,
                     playerControl,
                     extendedStartTime,
                     extendedEndTime,
                     plotCurrentFrame=True)

    rotationProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')
    BakeResult(rotationProperty,
               playerControl,
               extendedStartTime,
               extendedEndTime)

    transferConstraint.Active = False
    transferConstraint.FBDelete()
    Globals.Scene.Evaluate()

    coneMasterNull.FBDelete()

    scaleProperty = coneMaster.PropertyList.Find('Scaling (Lcl)')
    scaleProperty.SetLocked(False)
    scaleProperty.Data = mobu.FBVector3d(1,1,1)
    scaleProperty.SetLocked(True)


def SelectFpsConstraintComponent(inputCharacter,
                                 groupLabel):
    """
        Select an fpsCameraRig component from a referenceGroup label:
            instead of metadata / sematic traversal we can extract an elementsToPlot
            from a connection to the constraint group.
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    if not coneMaster:
        return

    disableConeMasterRotationProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
    if disableConeMasterRotationProperty is None:
        if _report_MissingComponents:
            print '[SelectFpsConstraintComponent] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    disableProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')

    component = gyroscope.ReferenceGet(GYROSCOPE_IO.index(groupLabel), 0)
    if not component:
        print 'Sorry, cound not find {} reference group'.format(groupLabel)
        return

    attributeArray = [disableProperty]
    sizeProperty = component.PropertyList.Find('Size')
    if sizeProperty:
        attributeArray.append(sizeProperty)

    lookProperty = component.PropertyList.Find('Look')
    if lookProperty:
        attributeArray.append(lookProperty)

    with ContextManagers.Undo(undoStackName='disableSelection',
                              modelList=[component],
                              attributeArray=attributeArray):
        disableProperty.SetLocked(False)
        disableProperty.Data = True
        disableProperty.SetLocked(True)

        component.Selected = True

        component.Show = True

        if component.Name == 'coneMaster':
            return

        sizeProperty = component.PropertyList.Find('Size')
        if sizeProperty:
            sizeProperty.Data = 8.0

        lookProperty = component.PropertyList.Find('Look')
        if lookProperty:
            lookProperty.Data = 1


def SelectFpsConstraintHook(inputCharacter,
                            groupLabel):
    """
        the aim constraint mode use external target / up vector nodes.

        those component exhibits basic space switching functionalities.

        this Function will help selected the component requested.
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        if _report_MissingComponents:
            print '[SelectFpsConstraintHook] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    gyroscope = fpsCameraConstraintData.constraint

    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    if not coneMaster:
        print 'Sorry, Could not find ConeMaster' 

        return

    disableConeMasterRotationProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
    if disableConeMasterRotationProperty is None:
        if _report_MissingComponents:
            print '[SelectFpsConstraintHook] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    disableProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')

    componentProperty = coneMaster.PropertyList.Find(groupLabel)
    if not componentProperty:
        print 'Sorry, Could not find ConeMaster.{}'.format(groupLabel)

        return

    if componentProperty.GetSrcCount() == 0:
        print 'Sorry, Could not find connections from coneMaster.{}'.format(groupLabel)
        return

    component = componentProperty.GetSrc(0)
    if component is None:
        return

    attributeArray = [disableProperty,
                      component.PropertyList.Find('Size')]

    lookProperty = component.PropertyList.Find('Look')
    if lookProperty:
        attributeArray.append(lookProperty)

    with ContextManagers.Undo(undoStackName='SelectFpsConstraintHook',
                              attributeArray=attributeArray,
                              modelList=[component]):
        component.Selected = True

        component.Show = True

        disableProperty.SetLocked(False)
        disableProperty.Data = True
        disableProperty.SetLocked(True)

        component.PropertyList.Find('Size').Data = 8.0
        lookProperty = component.PropertyList.Find('Look')
        if lookProperty:
            lookProperty.Data = 1


def SwitchHookSpace(inputCharacter,
                    inputLabel,
                    index):
    """
        the aim constraint mode use external target / up vector nodes.

        those component exhibits basic space switching functionalities.

        this Function will set the requested space.
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        if _report_MissingComponents:
            print '[SwitchHookSpace] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner
    componentProperty = coneMaster.PropertyList.Find(inputLabel)
    if not componentProperty:
        print 'Sorry, cound not find {} on conemaster'.format(inputLabel)

        return

    sourceCount = componentProperty.GetSrcCount()
    if not sourceCount:
        print 'Sorry, Could not find connections from coneMaster.{}'.format(inputLabel)
        return

    switchConstraint = componentProperty.GetSrc(0)

    weightList = [0.0, 0.0]
    weightList[index] = 100.0
    startTime = mobu.FBPlayerControl().ZoomWindowStart
    writeIndex = 0

    attributeArray = []

    for item in switchConstraint.PropertyList:
        if not item.Name.endswith('.Weight'):
            continue

        if not item.IsAnimated():
            item.SetAnimated(True)

        targetAnimationNode = item.GetAnimationNode()

        if targetAnimationNode.KeyCount > 0:
            targetAnimationNode.FCurve.EditClear()

        attributeArray.append(item)

    with ContextManagers.Undo(undoStackName='SwitchHookSpace',
                              attributeArray=attributeArray):
        for item in attributeArray:
            targetAnimationNode = item.GetAnimationNode()

            targetAnimationNode.KeyAdd(startTime,
                                       weightList[writeIndex])

            item.Data = weightList[writeIndex]

            targetAnimationNode.SetCandidate([weightList[writeIndex]])
            targetAnimationNode.KeyCandidate(startTime)

            writeIndex+=1


def BlendHookSpace(inputCharacter,
                   inputLabel,
                   inputWeight):
    """
        the aim constraint mode use external target / up vector nodes.

        those component exhibits basic space switching functionalities.

        this Function will set the additional parent constraint weight accordingly.
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        if _report_MissingComponents:
            print '[BlendHookSpace] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner
    componentProperty = coneMaster.PropertyList.Find(inputLabel)

    if not componentProperty:
        print 'Sorry, {} doesnt exists on coneMaster'.format(inputLabel)

        return

    sourceCount = componentProperty.GetSrcCount()
    if not sourceCount:
        print 'Sorry, Could not find connections from coneMaster.{}'.format(inputLabel)

        return

    switchConstraint = componentProperty.GetSrc(0)

    weightList = [100.0-inputWeight,
                  inputWeight]

    startTime = mobu.FBPlayerControl().ZoomWindowStart

    writeIndex = 0
    attributeArray = []

    for item in switchConstraint.PropertyList:
        if not item.Name.endswith('.Weight'):
            continue

        if not item.IsAnimated():
            item.SetAnimated(True)

        targetAnimationNode = item.GetAnimationNode()

        if targetAnimationNode.KeyCount > 0:
            targetAnimationNode.FCurve.EditClear()

        attributeArray.append(item)

    with ContextManagers.Undo(undoStackName='BlendHookSpace',
                              attributeArray=attributeArray):
        for item in attributeArray:
            targetAnimationNode = item.GetAnimationNode()

            targetAnimationNode.KeyAdd(startTime,
                                       weightList[writeIndex])

            item.Data = weightList[writeIndex]

            targetAnimationNode.SetCandidate([weightList[writeIndex]])
            targetAnimationNode.KeyCandidate(startTime)

            writeIndex+=1


def BlendRotationDominanceWeight(inputCharacter,
                                 inputWeight):
    #Rotation dominance will be animated track and not a setting
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        if _report_MissingComponents:
            print '[BlendRotationDominanceWeight] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner
    rotationDominanceProperty = coneMaster.PropertyList.Find('RotationDominance')

    if not rotationDominanceProperty:
        print 'Sorry, {} doesnt exists on coneMaster'.format(RotationDominance)

        return

    startTime = mobu.FBPlayerControl().ZoomWindowStart

    if not rotationDominanceProperty.IsAnimated():
        rotationDominanceProperty.SetAnimated(True)

    with ContextManagers.Undo(undoStackName='BlendRotationDominanceWeight',
                              attributeArray=[rotationDominanceProperty]):
        # we need to remap from -1.0 to 1.0
        remapWeight = -1.0 + 2.0*inputWeight*0.01

        targetAnimationNode = rotationDominanceProperty.GetAnimationNode()

        rotationDominanceProperty.Data = remapWeight

        targetAnimationNode.KeyCandidate()


def ToggleTargetRig(inputCharacter,
                    rigState):
    """
        Helper function:
            enable Target mode will be a setting:
        
        It should not be animated on / off onseveral time on the same take"
        Keys will be cleared and sets at the start of the take.
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    if not coneMaster:
        return

    property = coneMaster.PropertyList.Find('UseAimTarget')

    if property is None:
        if _report_MissingComponents:
            print '[ToggleTargetRig] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    disableConeMasterRotationProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
    if disableConeMasterRotationProperty is None:
        if _report_MissingComponents:
            print '[ToggleTargetRig] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    disableProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
    disableProperty.SetLocked(False)
    disableProperty.Data = True

    startTime = mobu.FBPlayerControl().ZoomWindowStart
    if not property.IsAnimated():
        property.SetAnimated(True)

    targetAnimationNode = property.GetAnimationNode()
    if targetAnimationNode.KeyCount > 0:
        targetAnimationNode.FCurve.EditClear()


    with ContextManagers.Undo(undoStackName='ToggleTargetRig',
                              attributeArray=[property,
                                              disableProperty]):
        targetAnimationNode = property.GetAnimationNode()

        property.Data = rigState
        targetAnimationNode.KeyCandidate(startTime)


def ToggleConeOffsetRotation(inputCharacter,
                             rigState):
    """
        Deprecated Functions
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint

    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    if not coneMaster:
        return

    property = coneMaster.PropertyList.Find('DisableConeMasterRotation')
    if property is None:
        if _report_MissingComponents:
            print '[ToggleConeOffsetRotation] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    disableProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
    disableProperty.SetLocked(False)

    with ContextManagers.Undo(undoStackName='ToggleConeOffsetRotation',
                              attributeArray=[disableProperty]):
        disableProperty.Data = True

        property.Data = rigState


def ToggleWorldMoverMode(inputCharacter,
                         rigState):
    """
        use the worldMover object when needed
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint

    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    property = coneMaster.PropertyList.Find('UseWorldMover')
    if property is None:
        print 'Sorry, {} doesnt have the latest rig installed'.format(inputCharacter.LongName)

        return

    startTime = mobu.FBPlayerControl().ZoomWindowStart

    property.SetLocked(False)

    if not property.IsAnimated():
        property.SetAnimated(True)

    targetAnimationNode = property.GetAnimationNode()

    if targetAnimationNode.KeyCount > 0:
        targetAnimationNode.FCurve.EditClear()

    with ContextManagers.Undo(undoStackName='ProcessCameraDofs',
                              attributeArray=[property]):
        property.Data = rigState
        targetAnimationNode.KeyCandidate(startTime)


class AttributePair(object):
    """
        Datastructure used to transfer property value between attributes
    """
    def __init__(self,
                 sourceProperty,
                 targetProperty,
                 reverseValue=1.0,
                 sampleIndex=0):
        self.sourceProperty = sourceProperty

        self.targetProperty = targetProperty

        self.reverseValue = reverseValue

        self.sampleIndex = sampleIndex

        if not self.targetProperty.IsAnimated():
            self.targetProperty.SetAnimated(True)

        self.targetPropertyAnimationNode = self.targetProperty.GetAnimationNode()

    def focus(self):
        self.targetProperty.SetFocus(True)

    def release(self):
        self.targetProperty.SetFocus(False)


def PlotAttributes(playerControl,
                   startTime,
                   endTime,
                   attributeArray):
    """
        alternate mechanism used to bake animation between attributePair array
    """
    for data in attributeArray:
        data.focus()

    frameRange = (endTime.GetFrame() - startTime.GetFrame()) + 1
    playerControl.Goto(startTime)

    for frameIndex in xrange(frameRange):
        mobu.FBSystem().Scene.Evaluate()

        for data in attributeArray:
            sampleValue = data.sourceProperty.Data

            data.targetProperty.Data = float(sampleValue[data.sampleIndex])*data.reverseValue

            data.targetPropertyAnimationNode.KeyCandidate()

        playerControl.Key()
        playerControl.StepForward()

    for data in attributeArray:
        data.release()


def TransferAttributes(playerControl,
                       startTime,
                       endTime,
                       attributeArray):
    """
        Alternate plotting mechanism for the fpsCameraRig components
    """
    for data in attributeArray:
        data.focus()

    frameRange = (endTime.GetFrame() - startTime.GetFrame()) + 1
    playerControl.Goto(startTime)

    for frameIndex in xrange(frameRange):
        mobu.FBSystem().Scene.Evaluate()

        for data in attributeArray:
            sampleValue = data.sourceProperty.Data

            data.targetProperty.Data = sampleValue

            data.targetPropertyAnimationNode.KeyCandidate()

        playerControl.Key()
        playerControl.StepForward()

    for data in attributeArray:
        data.release()


class SliderTarget(object):
    """
        Deprecated class:
            the constraint not use additional offset property in target mode
    """
    def __init__(self,
                 inputNode,
                 inputProperty,
                 storageProperty,
                 saveAnimationState=True):
        self.inputNode = inputNode

        self.inputProperty = inputProperty

        self.storageProperty = storageProperty

        self.saveAnimationState = saveAnimationState

    def synchronize(self):
        if self.inputNode is None:
            return

        if self.inputProperty is None:
            return

        if self.storageProperty is not None:
            self.storageProperty.SetLocked(False)

            self.storageProperty.SetAnimated(True)

            self.inputProperty.SetAnimated(True)

            self.storageProperty.SetLocked(True)
            return

        self.storageProperty =  self.inputNode.PropertyCreate('storage_{0}'.format(self.inputProperty.Name), 
                                                              mobu.FBPropertyType.kFBPT_double, 
                                                              'Number',True, True, None)

        self.storageProperty.SetAnimated(True)

        self.inputProperty.SetAnimated(True)

        if self.saveAnimationState is False:
            return

        self.saveAnimation()

    def saveAnimation(self):
        self.storageProperty.SetLocked(False)

        sourceAnimationNode = self.inputProperty.GetAnimationNode()

        targetAnimationNode = self.storageProperty.GetAnimationNode()

        targetAnimationNode.FCurve.KeyReplaceBy(sourceAnimationNode.FCurve)

    def loadAnimation(self):
        self.storageProperty.SetLocked(False)

        self.storageProperty.SetAnimated(True)

        self.inputProperty.SetAnimated(True)

        targetAnimationNode = self.inputProperty.GetAnimationNode()

        sourceAnimationNode = self.storageProperty.GetAnimationNode()

        targetAnimationNode.FCurve.KeyReplaceBy(sourceAnimationNode.FCurve)


def CollectConeMasterSliderProperties(inputCharacter,
                                      saveAnimationState=True):
    """
        Deprecated function
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return []

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    sliderAttributes = ('Down/Up',
                        'DollyOut/DollyIn',
                        'Left/Right')

    sliderArray = []
    for attribute in sliderAttributes:
        sliderArray.append(SliderTarget(coneMaster,
                                        coneMaster.PropertyList.Find(attribute),
                                        coneMaster.PropertyList.Find('storage_{0}'.format(attribute)),
                                        saveAnimationState=saveAnimationState))

    for slider in sliderArray:
        slider.synchronize()

    return sliderArray


def TransferLookAtAnimationToConeMaster(inputCharacter,
                                        startTime,
                                        endTime,
                                        enabledTargetRig=False):
    """
        Utility function:
            from the target mode we need to bake orientation on the coneMaster and on the offset slider!
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint

    bakeUtils = BakeLookAtAnimationOnHeadGyroscope

    if gyroscope.ClassName() == 'fpsHeadGyroscope':
        bakeUtils = BakeLookAtAnimationOnFpsHeadGyroscope

    rotationDominance = bakeUtils(fpsCameraConstraintData,
                                  inputCharacter.LongName,
                                  startTime,
                                  endTime,
                                  enabledTargetRig=enabledTargetRig)

    return rotationDominance


def BakeLookAtAnimationOnFpsHeadGyroscope(fpsCameraConstraintData,
                                          characterName,
                                          startTime,
                                          endTime,
                                          enabledTargetRig=True):
    """
        Space conversion function between target mode / fk mode.
    """

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner
    coneOffset = gyroscope.ReferenceGet(GYROSCOPE_IO.index('ConeOffset'), 0)
    camNode = gyroscope.ReferenceGet(GYROSCOPE_IO.index('FpsCamera'), 0)

    useAimTargetProperty = coneMaster.PropertyList.Find('UseAimTarget')
    if useAimTargetProperty is None:
        if _report_MissingComponents:
            print '[BakeLookAtAnimationOnFpsHeadGyroscope] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    if enabledTargetRig:
        #enable aimTargetMode 
        useAimTargetProperty = coneMaster.PropertyList.Find('UseAimTarget')
        useAimTargetProperty.SetAnimated(True)
        targetAnimationNode = useAimTargetProperty.GetAnimationNode()
        useAimTargetProperty.Data = True
        targetAnimationNode.KeyCandidate(startTime)

    with TemporaryNulls() as nullUtils:
        useAimTargetProperty.Data = True
        disableProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
        disableProperty.SetLocked(False)
        disableProperty.Data = True
        disableProperty.SetLocked(True)

        Globals.Scene.Evaluate()

        coneMasterOrientNull = mobu.FBModelNull('coneMasterOrientNull1')
        coneMasterOrientNull.Visible = True
        coneMasterOrientNull.Show = True
        coneMasterOrientNull.QuaternionInterpolate = True

        coneMasterOrientNull2 = mobu.FBModelNull('coneMasterOrientNull2')
        coneMasterOrientNull2.Visible = True
        coneMasterOrientNull2.Show = True
        coneMasterOrientNull2.QuaternionInterpolate = True

        coneMasterSliderNull = mobu.FBModelNull('coneMasterSliderNull1')
        coneMasterSliderNull.Visible = True
        coneMasterSliderNull.Show = True
        coneMasterSliderNull.QuaternionInterpolate = True
        coneMasterSliderNull.Parent = coneMasterOrientNull
        Globals.Scene.Evaluate()

        nullUtils._nullArray.extend([coneMasterOrientNull, 
                                     coneMasterOrientNull2,
                                     coneMasterSliderNull])

        transferAnimationComponents = []
        with ikConstraintUtils.ExtractWorlspaceState([camNode],
                                                     None,
                                                     componentName='fps target components1') as stateUtils:
            transferAnimationComponents = stateUtils.targetNodeArray

        coneMasterNull = transferAnimationComponents[0].worldTarget
        coneMasterNull.QuaternionInterpolate = True
        transferAnimationComponents = []

        nullUtils._nullArray.append(coneMasterNull)

        rotationProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')
        rotationProperty.SetAnimated(True)
        targetAnimationNode = rotationProperty.GetAnimationNode()
        for node in targetAnimationNode.Nodes:
            node.FCurve.EditClear()

        rotationProperty.Data = mobu.FBVector3d(0, 0, 0)

        targetAnimationNode.KeyCandidate(startTime)
        with ikConstraintUtils.ExtractWorlspaceState([coneMaster],
                                                     None,
                                                     componentName='fps Base components2') as stateUtils:
            transferAnimationComponents = stateUtils.targetNodeArray

        coneMasterOrientNullParent = transferAnimationComponents[0].worldTarget
        Globals.Scene.Evaluate()

        nullUtils._nullArray.append(coneMasterOrientNullParent)

        sliderComponents = []
        with ikConstraintUtils.ExtractWorlspaceState([camNode],
                                                     None,
                                                     componentName='coneSlider components') as stateUtils:
            sliderComponents = stateUtils.targetNodeArray

        coneMasterOrientNull.Parent = coneMasterOrientNullParent
        coneMasterOrientNull.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterOrientNull.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(180.0, 0.0, 90.0)
        coneMasterOrientNull.QuaternionInterpolate = True

        coneMasterSliderWorld = sliderComponents[0].worldTarget
        coneMasterSliderWorld.Name = 'coneMasterSliderWorld1'
        coneMasterSliderWorld.QuaternionInterpolate = True
        Globals.Scene.Evaluate()

        nullUtils._nullArray.append(coneMasterSliderWorld)

        coneSliderConstraint = ApplyParentConstraint(coneMasterSliderNull, 
                                                     coneMasterSliderWorld)

        attributeArray = [AttributePair(coneMasterSliderNull.PropertyList.Find('Translation (Lcl)'),
                                        coneMaster.PropertyList.Find('Down/Up'),
                                        sampleIndex=1,
                                        reverseValue=1.0),
                          AttributePair(coneMasterSliderNull.PropertyList.Find('Translation (Lcl)'),
                                        coneMaster.PropertyList.Find('DollyOut/DollyIn'),
                                        sampleIndex=0,
                                        reverseValue=1.0),
                          AttributePair(coneMasterSliderNull.PropertyList.Find('Translation (Lcl)'),
                                        coneMaster.PropertyList.Find('Left/Right'),
                                        sampleIndex=2,
                                        reverseValue=-1.0)]

        playerControl = mobu.FBPlayerControl()
        Globals.Scene.Evaluate()

        coneMasterConvertNull = mobu.FBModelNull('coneMasterConvertNull1')
        coneMasterConvertNull.Parent = coneMasterSliderWorld
        coneMasterConvertNull.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterConvertNull.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterConvertNull.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(180.0, 0.0, 90.0)
        coneMasterConvertNull.QuaternionInterpolate = True
        Globals.Scene.Evaluate()
        playerControl.Goto(startTime)

        nullUtils._nullArray.append(coneSliderConstraint)

        nullUtils._nullArray.append(coneMasterConvertNull)

        #disable aimTargetMode and bake rotation on coneMaster
        useAimTargetProperty = coneMaster.PropertyList.Find('UseAimTarget')
        useAimTargetProperty.SetAnimated(True)
        targetAnimationNode = useAimTargetProperty.GetAnimationNode()
        targetAnimationNode.FCurve.EditClear()
        useAimTargetProperty.Data = False
        targetAnimationNode.KeyCandidate(startTime)

        rotationDominance = coneMaster.PropertyList.Find('RotationDominance')
        rotationDominance.SetAnimated(True)
        rotationDominanceNode = rotationDominance.GetAnimationNode()
        rotationDominanceNode.FCurve.EditClear()
        rotationDominance.Data = 1.0
        rotationDominanceNode.KeyCandidate(startTime)
        Globals.Scene.Evaluate()

        offsetComponents = []
        with ikConstraintUtils.ExtractWorlspaceState([coneMaster.Parent],
                                                     None,
                                                     componentName='offset parent components') as stateUtils:
            offsetComponents = stateUtils.targetNodeArray

        nullUtils._nullArray.append(offsetComponents[0].worldTarget)

        Globals.Scene.Evaluate()
        coneMasterOrientNull2.Parent = offsetComponents[0].worldTarget
        coneMasterOrientNull2.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterOrientNull2.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull2.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(180.0, 0.0, 90.0)
        coneMasterOrientNull2.QuaternionInterpolate = True

        coneMasterOrientNull2.PropertyList.Find('Rotation Pivot (Auto Offset)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull2.PropertyList.Find('RotationOffset').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull2.PropertyList.Find("Scaling Pivot (Auto Offset)").Data = mobu.FBVector3d(0, 0, 0)

        coneLocalRotation = mobu.FBModelNull('coneLocalRotation1')
        coneLocalRotation.Parent = coneMasterOrientNull2
        coneLocalRotation.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneLocalRotation.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneLocalRotation.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0.0, 0.0, 0.0)
        coneLocalRotation.QuaternionInterpolate = True

        nullUtils._nullArray.append(coneLocalRotation)

        localConstraint = ApplyParentConstraint(coneLocalRotation,
                                                coneMasterNull)

        for attribute in localConstraint.PropertyList:
            if re.search("Offset (R)", attribute.Name):
                attribute.Data = mobu.FBVector3d(180, 0, 90)

        localConstraint.PropertyList.Find('Affect Translation X').Data = False
        localConstraint.PropertyList.Find('Affect Translation Y').Data = False
        localConstraint.PropertyList.Find('Affect Translation Z').Data = False

        coneLocalRotation.PropertyList.Find('Rotation Pivot (Auto Offset)').Data = mobu.FBVector3d(0, 0, 0)
        coneLocalRotation.PropertyList.Find('RotationOffset').Data = mobu.FBVector3d(0, 0, 0)
        coneLocalRotation.PropertyList.Find("Scaling Pivot (Auto Offset)").Data = mobu.FBVector3d(0, 0, 0)

        nullUtils._nullArray.append(localConstraint)

        rotationProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')
        rotationProperty.SetLocked(False)

        sourceRotationProperty = coneLocalRotation.PropertyList.Find('Rotation (Lcl)')

        sourceRotationProperty.SetAnimated(True)
        rotationProperty.SetAnimated(True)

        Globals.Scene.Evaluate()

        PlotAnimation(coneLocalRotation)

        localConstraint.Active = False

        Globals.Scene.Evaluate()

        coneMasterOrientNull3 = mobu.FBModelNull('coneMasterOrientNull3')
        coneMasterOrientNull3.Parent = coneMaster.Parent
        coneMasterOrientNull3.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterOrientNull3.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull3.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0.0, 0.0, 0.0)
        coneMasterOrientNull3.QuaternionInterpolate = True

        nullUtils._nullArray.append(coneMasterOrientNull3)

        localConstraint2 = ApplyParentConstraint(coneMasterOrientNull3,
                                                 coneLocalRotation)

        PlotAnimation(coneMasterOrientNull3)

        localConstraint2.Active = False

        Globals.Scene.Evaluate()

        nullUtils._nullArray.append(localConstraint2)

        rotationProperty2 = coneMasterOrientNull3.PropertyList.Find('Rotation (Lcl)')
        rotationProperty2.SetAnimated(True)
        targetAnimationNode2 = rotationProperty2.GetAnimationNode()

        coneMasterAttributeArray = [AttributePair(coneMasterOrientNull3.PropertyList.Find('Rotation (Lcl)'),
                                                  coneMaster.PropertyList.Find('Rotation (Lcl)'))]

        TransferAttributes(playerControl,
                           startTime,
                           endTime,
                           coneMasterAttributeArray)

        Globals.Scene.Evaluate()

        coneMasterOrientNull.Parent = coneLocalRotation
        coneMasterOrientNull.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterOrientNull.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(180.0, 0.0, 90.0)

        PlotAnimation(coneMasterSliderNull)

        coneSliderConstraint.Active = False
        nullUtils._nullArray.append(coneSliderConstraint)

        Globals.Scene.Evaluate()

        PlotAttributes(playerControl,
                       startTime,
                       endTime,
                       attributeArray)

        for item in attributeArray:
            for key in item.targetPropertyAnimationNode.FCurve.Keys:
                key.TangentMode = mobu.FBTangentMode.kFBTangentModeAuto

        Globals.Scene.Evaluate()

        playerControl.Goto(startTime)

        rotationProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')
        rotationProperty.SetAnimated(True)

        targetAnimationNode = rotationProperty.GetAnimationNode()

        for node in targetAnimationNode.Nodes:
            for key in node.FCurve.Keys:
                key.TangentMode = mobu.FBTangentMode.kFBTangentModeAuto

        scaleProperty = coneMaster.PropertyList.Find('Scaling (Lcl)')
        scaleProperty.SetLocked(False)
        scaleProperty.Data = mobu.FBVector3d(1, 1, 1)
        scaleProperty.SetLocked(True)

        rotationDominance = coneMaster.PropertyList.Find('RotationDominance')
        rotationDominance.SetAnimated(True)
        rotationDominanceNode = rotationDominance.GetAnimationNode()
        rotationDominanceNode.FCurve.EditClear()
        rotationDominance.Data = 1.0
        rotationDominanceNode.KeyCandidate(startTime)

        playerControl.Goto(startTime)
        Globals.Scene.Evaluate()

        return rotationDominance


def BakeLookAtAnimationOnHeadGyroscope(fpsCameraConstraintData,
                                       characterName,
                                       startTime,
                                       endTime,
                                       enabledTargetRig=False):
    """
        Space conversion function between target mode / fk mode.

        the legacy plugin has slighty diffrent layout and the math involve will be a bit different
    """
    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner
    coneOffset = gyroscope.ReferenceGet(GYROSCOPE_IO.index('ConeOffset'), 0)
    camNode = gyroscope.ReferenceGet(GYROSCOPE_IO.index('FpsCamera'), 0)
    _fixInvalidFPSCameraScale(camNode)

    useAimTargetProperty = coneMaster.PropertyList.Find('UseAimTarget')
    if useAimTargetProperty is None:
        if _report_MissingComponents:
            print '[BakeLookAtAnimationOnHeadGyroscope] - Sorry, {} doesnt have the latest target rig installed'.format(characterName)

        return

    useAimTargetProperty.Data = True
    disableProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
    disableProperty.SetLocked(False)
    disableProperty.Data = True
    disableProperty.SetLocked(True)

    if enabledTargetRig:
        #enable aimTargetMode 
        useAimTargetProperty = coneMaster.PropertyList.Find('UseAimTarget')
        useAimTargetProperty.SetAnimated(True)
        targetAnimationNode = useAimTargetProperty.GetAnimationNode()
        useAimTargetProperty.Data = True
        targetAnimationNode.KeyCandidate(startTime)

    Globals.Scene.Evaluate()
    with TemporaryNulls() as nullUtils:
        coneMasterOrientNull = mobu.FBModelNull('coneMasterOrientNull1')
        coneMasterOrientNull.Visible = True
        coneMasterOrientNull.Show = True
        coneMasterOrientNull.QuaternionInterpolate = True

        coneMasterOrientNull2 = mobu.FBModelNull('coneMasterOrientNull2')
        coneMasterOrientNull2.Visible = True
        coneMasterOrientNull2.Show = True
        coneMasterOrientNull2.QuaternionInterpolate = True

        coneMasterSliderNull = mobu.FBModelNull('coneMasterSliderNull1')
        coneMasterSliderNull.Visible = True
        coneMasterSliderNull.Show = True
        coneMasterSliderNull.QuaternionInterpolate = True
        coneMasterSliderNull.Parent = coneMasterOrientNull
        Globals.Scene.Evaluate()

        transferAnimationComponents = []
        with ikConstraintUtils.ExtractWorlspaceState([camNode],
                                                     None,
                                                     componentName='fps target components1') as stateUtils:
            transferAnimationComponents = stateUtils.targetNodeArray

        coneMasterNull = transferAnimationComponents[0].worldTarget
        coneMasterNull.QuaternionInterpolate = True


        rotationProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')
        rotationProperty.SetAnimated(True)
        targetAnimationNode = rotationProperty.GetAnimationNode()
        for node in targetAnimationNode.Nodes:
            node.FCurve.EditClear()

        rotationProperty.Data = mobu.FBVector3d(0, 0, 0)

        targetAnimationNode.KeyCandidate(startTime)
        transferAnimationComponents = []
        with ikConstraintUtils.ExtractWorlspaceState([coneMaster],
                                                     None,
                                                     componentName='fps Base components2') as stateUtils:
            transferAnimationComponents = stateUtils.targetNodeArray

        coneMasterOrientNullParent = transferAnimationComponents[0].worldTarget
        Globals.Scene.Evaluate()

        sliderComponents = []
        with ikConstraintUtils.ExtractWorlspaceState([camNode],
                                                     None,
                                                     componentName='coneSlider components') as stateUtils:
            sliderComponents = stateUtils.targetNodeArray

        coneMasterOrientNull.Parent = coneMasterOrientNullParent
        coneMasterOrientNull.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterOrientNull.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(180.0, 0.0, 90.0)
        coneMasterOrientNull.QuaternionInterpolate = True

        coneMasterSliderWorld = sliderComponents[0].worldTarget
        coneMasterSliderWorld.Name = 'coneMasterSliderWorld1'
        coneMasterSliderWorld.QuaternionInterpolate = True
        Globals.Scene.Evaluate()

        coneSliderConstraint = ApplyParentConstraint(coneMasterSliderNull, 
                                                     coneMasterSliderWorld)

        attributeArray = [AttributePair(coneMasterSliderNull.PropertyList.Find('Translation (Lcl)'),
                                        coneMaster.PropertyList.Find('Down/Up'),
                                        sampleIndex=1,
                                        reverseValue=1.0),
                          AttributePair(coneMasterSliderNull.PropertyList.Find('Translation (Lcl)'),
                                        coneMaster.PropertyList.Find('DollyOut/DollyIn'),
                                        sampleIndex=0,
                                        reverseValue=1.0),
                          AttributePair(coneMasterSliderNull.PropertyList.Find('Translation (Lcl)'),
                                        coneMaster.PropertyList.Find('Left/Right'),
                                        sampleIndex=2,
                                        reverseValue=-1.0)]

        playerControl = mobu.FBPlayerControl()
        Globals.Scene.Evaluate()

        coneMasterConvertNull = mobu.FBModelNull('coneMasterConvertNull1')
        coneMasterConvertNull.Parent = coneMasterSliderWorld
        coneMasterConvertNull.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterConvertNull.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterConvertNull.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(180.0, 0.0, 90.0)
        coneMasterConvertNull.QuaternionInterpolate = True
        Globals.Scene.Evaluate()
        playerControl.Goto(startTime)

        #disable aimTargetMode and bake rotation on coneMaster
        useAimTargetProperty = coneMaster.PropertyList.Find('UseAimTarget')
        useAimTargetProperty.SetAnimated(True)
        targetAnimationNode = useAimTargetProperty.GetAnimationNode()
        targetAnimationNode.FCurve.EditClear()
        useAimTargetProperty.Data = False
        targetAnimationNode.KeyCandidate(startTime)

        offsetComponents = []
        with ikConstraintUtils.ExtractWorlspaceState([coneMaster.Parent],
                                                     None,
                                                     componentName='offset parent components') as stateUtils:
            offsetComponents = stateUtils.targetNodeArray

        Globals.Scene.Evaluate()
        coneMasterOrientNull2.Parent = offsetComponents[0].worldTarget
        coneMasterOrientNull2.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterOrientNull2.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull2.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(180.0, 0.0, 90.0)
        coneMasterOrientNull2.QuaternionInterpolate = True

        coneMasterOrientNull2.PropertyList.Find('Rotation Pivot (Auto Offset)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull2.PropertyList.Find('RotationOffset').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull2.PropertyList.Find("Scaling Pivot (Auto Offset)").Data = mobu.FBVector3d(0, 0, 0)

        coneLocalRotation = mobu.FBModelNull('coneLocalRotation1')
        coneLocalRotation.Parent = coneMasterOrientNull2
        coneLocalRotation.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneLocalRotation.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneLocalRotation.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0.0, 0.0, 0.0)
        coneLocalRotation.QuaternionInterpolate = True

        localConstraint = ApplyParentConstraint(coneLocalRotation,
                                                coneMasterNull)

        for attribute in localConstraint.PropertyList:
            if re.search("Offset (R)", attribute.Name):
                attribute.Data = mobu.FBVector3d(180, 0, 90)

        localConstraint.PropertyList.Find('Affect Translation X').Data = False
        localConstraint.PropertyList.Find('Affect Translation Y').Data = False
        localConstraint.PropertyList.Find('Affect Translation Z').Data = False

        coneLocalRotation.PropertyList.Find('Rotation Pivot (Auto Offset)').Data = mobu.FBVector3d(0, 0, 0)
        coneLocalRotation.PropertyList.Find('RotationOffset').Data = mobu.FBVector3d(0, 0, 0)
        coneLocalRotation.PropertyList.Find("Scaling Pivot (Auto Offset)").Data = mobu.FBVector3d(0, 0, 0)

        rotationProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')
        rotationProperty.SetLocked(False)

        sourceRotationProperty = coneLocalRotation.PropertyList.Find('Rotation (Lcl)')

        sourceRotationProperty.SetAnimated(True)
        rotationProperty.SetAnimated(True)

        Globals.Scene.Evaluate()

        PlotAnimation(coneLocalRotation)

        localConstraint.Active = False
        localConstraint.FBDelete()

        Globals.Scene.Evaluate()

        coneMasterOrientNull3 = mobu.FBModelNull('coneMasterOrientNull2')
        coneMasterOrientNull3.Parent = coneMaster.Parent
        coneMasterOrientNull3.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterOrientNull3.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull3.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0.0, 0.0, 0.0)
        coneMasterOrientNull3.QuaternionInterpolate = True

        localConstraint2 = ApplyParentConstraint(coneMasterOrientNull3,
                                                 coneLocalRotation)

        PlotAnimation(coneMasterOrientNull3)

        localConstraint2.Active = False
        localConstraint2.FBDelete()

        Globals.Scene.Evaluate()

        rotationProperty2 = coneMasterOrientNull3.PropertyList.Find('Rotation (Lcl)')
        rotationProperty2.SetAnimated(True)
        targetAnimationNode2 = rotationProperty2.GetAnimationNode()

        coneMasterAttributeArray = [AttributePair(coneMasterOrientNull3.PropertyList.Find('Rotation (Lcl)'),
                                                  coneMaster.PropertyList.Find('Rotation (Lcl)'))]

        TransferAttributes(playerControl,
                           startTime,
                           endTime,
                           coneMasterAttributeArray)

        Globals.Scene.Evaluate()

        coneMasterOrientNull.Parent = coneLocalRotation
        coneMasterOrientNull.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        coneMasterOrientNull.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        coneMasterOrientNull.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(180.0, 0.0, 90.0)

        PlotAnimation(coneMasterSliderNull)

        coneSliderConstraint.Active = False
        coneSliderConstraint.FBDelete()
        Globals.Scene.Evaluate()

        PlotAttributes(playerControl,
                       startTime,
                       endTime,
                       attributeArray)

        for item in attributeArray:
            for key in item.targetPropertyAnimationNode.FCurve.Keys:
                key.TangentMode = mobu.FBTangentMode.kFBTangentModeAuto

        Globals.Scene.Evaluate()

        playerControl.Goto(startTime)

        rotationProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')
        rotationProperty.SetAnimated(True)

        targetAnimationNode = rotationProperty.GetAnimationNode()

        for node in targetAnimationNode.Nodes:
            for key in node.FCurve.Keys:
                key.TangentMode = mobu.FBTangentMode.kFBTangentModeAuto

        scaleProperty = coneMaster.PropertyList.Find('Scaling (Lcl)')
        scaleProperty.SetLocked(False)
        scaleProperty.Data = mobu.FBVector3d(1, 1, 1)
        scaleProperty.SetLocked(True)

        nullUtils._nullArray.extend([coneMasterNull,
                                     coneMasterOrientNullParent,
                                     coneMasterOrientNull,
                                     coneMasterSliderNull,
                                     coneMasterSliderWorld,
                                     coneMasterOrientNull2,
                                     coneMasterConvertNull,
                                     coneMasterOrientNull3,
                                     coneLocalRotation,
                                     offsetComponents[0].worldTarget])

        playerControl.Goto(startTime)
        Globals.Scene.Evaluate()

        rotationDominance = coneMaster.PropertyList.Find('RotationDominance')
        rotationDominance.SetAnimated(True)
        rotationDominanceNode = rotationDominance.GetAnimationNode()
        rotationDominanceNode.FCurve.EditClear()
        rotationDominance.Data = 1.0
        rotationDominanceNode.KeyCandidate(startTime)

        playerControl.Goto(startTime)
        Globals.Scene.Evaluate()

        return rotationDominance


def ApplyKeyFilterOnConeMaster(inputCharacter,
                               inputFilter):
    """
        Filter aniamtion curve using the provided inputFilter datastructure
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        if _report_MissingComponents:
            print '[ApplyKeyFilterOnConeMaster] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    gyroscope = fpsCameraConstraintData.constraint
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    useAimTargetProperty = coneMaster.PropertyList.Find('UseAimTarget')
    if useAimTargetProperty is None:
        if _report_MissingComponents:
            print '[ApplyKeyFilterOnConeMaster] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    objectToFilter = [coneMaster]

    attributeArray = [coneMaster.PropertyList.Find('Down/Up'),
                      coneMaster.PropertyList.Find('DollyOut/DollyIn'),
                      coneMaster.PropertyList.Find('Left/Right')]

    for attribute in attributeArray:
        if not attribute.IsAnimated():
            attribute.SetAnimated(True)

    inputFilterManager = mobu.FBFilterManager()

    with ContextManagers.Undo(undoStackName='filterApply',
                              modelList=objectToFilter,
                              attributeArray=attributeArray):
        currentFilter = inputFilterManager.CreateFilter(inputFilter.type)
        if inputFilter.attribute is not None:
            currentFilter.PropertyList.Find(inputFilter.attribute).Data = inputFilter.value

        mobu.FBSystem().Scene.Evaluate()

        currentFilter.Apply(coneMaster.Rotation.GetAnimationNode(), True)

        for attribute in attributeArray:
            currentFilter.Apply(attribute.GetAnimationNode(), True)

        mobu.FBSystem().Scene.Evaluate()
        currentFilter.FBDelete()


DOF_ATTRIBUTES = ('DOF_MinX',
                  'DOF_MinY',
                  'DOF_MaxX',
                  'DOF_MaxY')


DOF_DEFAULT_VALUES = (-45.0,
                      -60.0,
                      45.0,
                      60.0)


DOF_RESET_VALUES = (0.0, 0.0, 0.0, 0.0)


def ProcessCameraDofs(inputCharacter,
                      dofValues):
    """
        Utility function which can salvage  DOF track.
        Can either reset to default value or force it to zero
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)

    if fpsCameraConstraintData is None:
        if _report_MissingComponents:
            print '[ProcessCameraDofs] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

        return

    gyroscope = fpsCameraConstraintData.constraint

    coneMaster = fpsCameraConstraintData.drivingPropertyOwner

    if coneMaster is None:
        return

    currentTake = mobu.FBSystem().CurrentTake
    layerCount = currentTake.GetLayerCount()

    for layerIndex in xrange(layerCount):
        currentTake.GetLayer(layerIndex).SelectLayer(True, 
                                                     True)

        currentTake.SetCurrentLayer(layerIndex)
        mobu.FBPlayerControl().GotoStart()

        attributeArray = []
        attributeValues = []

        for index, attribute in enumerate(DOF_ATTRIBUTES):
            property = coneMaster.PropertyList.Find(attribute)

            attributeArray.append(property)
            attributeValues.append(dofValues[index])

        with ContextManagers.Undo(undoStackName='ProcessCameraDofs',
                                  attributeArray=attributeArray):
            for index, property in enumerate(attributeArray):
                if not property:
                    continue

                if not property.IsAnimated():
                    property.SetAnimated(True)

                animationNode = property.GetAnimationNode()

                if animationNode.KeyCount > 0:
                    animationNode.FCurve.EditClear()

                if animationNode.FCurve:
                    animationNode.FCurve.EditBegin()

                property.Data = attributeValues[index]
                animationNode.KeyCandidate()

                if animationNode.FCurve:
                    animationNode.FCurve.EditEnd()


def ZeroCameraDofs(inputCharacter):
    ProcessCameraDofs(inputCharacter,
                      DOF_RESET_VALUES)


def RestoreCameraDofs(inputCharacter):
    ProcessCameraDofs(inputCharacter,
                      DOF_DEFAULT_VALUES)


def SetConeMasterProperty(inputCharacter,
                          propertyName, propertyValue,
                          autoKey=True,
                          clearExistingKeys=False):
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None or fpsCameraConstraintData.drivingPropertyOwner is None:
        raise FPSRigMissingComponentError(
            "coneMaster could not be found for character: {}".format(inputCharacter.LongName))
    
    coneMaster = fpsCameraConstraintData.drivingPropertyOwner
    targetProperty = coneMaster.PropertyList.Find(propertyName)

    if targetProperty is None:
        raise FPSRigMissingPropertyError(
            'coneMaster property "{}" could not be found for character: {}'.format(propertyName,
                                                                                   inputCharacter.LongName))

    if not targetProperty.IsAnimated():
        targetProperty.SetAnimated(True)

    with ContextManagers.Undo(undoStackName='SetAndKeyConeMasterProperty',
                              attributeArray=[targetProperty]):
        animationNode = targetProperty.GetAnimationNode()
        if clearExistingKeys and animationNode.KeyCount > 0:
            animationNode.FCurve.EditClear()
        targetProperty.Data = propertyValue
        animationNode.KeyCandidate()


def SnapStabilizationOriginOffsetToHead(inputCharacter):
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
    if fpsCameraConstraintData is None:
        raise FPSRigMissingComponentError(
            "FPS camera rig is invalid for: {}".format(inputCharacter.LongName))
        
    mover = fpsCameraConstraintData.constraint.ReferenceGet(GYROSCOPE_IO.index('MOVER'), 0)
    if mover is None:
        raise FPSRigMissingComponentError(
            "mover could not be found for character: {}".format(inputCharacter.LongName))
    
    headBone = None
    gyroscopeConstraint = fpsCameraConstraintData.constraint
    if gyroscopeConstraint is not None:
        headBone = gyroscopeConstraint.ReferenceGet(GYROSCOPE_IO.index('ParentAnchor'), 0)
    if headBone is None:
        raise FPSRigMissingComponentError(
            "Head bone could not be found for character: {}".format(inputCharacter.LongName))
    
    # Calculate the local offset from the mover origin to the head (assumes mover is upright in WS)
    moverPosWS = mobu.FBVector3d()
    mover.GetVector(moverPosWS)
    headPosWS = mobu.FBVector3d()
    headBone.GetVector(headPosWS)
    vOffset = headPosWS[1] - moverPosWS[1]
    
    SetConeMasterProperty(inputCharacter,
                          "Vertical Stabilization Offset",
                          vOffset)


def ResetStabilizationOriginOffset(inputCharacter):
    SetConeMasterProperty(inputCharacter,
                          "Vertical Stabilization Offset",
                          DEFAULT_VERTICAL_STABILIZATION_OFFSET)


def ToggleFpsSceneGroup(inputCharacter):
    """
        Enable/disable group visibility for the fps camera rig.
    """
    fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)

    if fpsCameraConstraintData is None:
        return

    gyroscope = fpsCameraConstraintData.constraint

    camera = gyroscope.ReferenceGet(GYROSCOPE_IO.index('FpsCamera'), 0)
    cameraGroup = []
    for index in range(camera.GetDstCount()):
        if isinstance(camera.GetDst(index),
                      mobu.FBPropertyListObject):
            continue

        if 'FPSCamera' not in camera.GetDst(index).Name:
            continue

        cameraGroup.append(camera.GetDst(index))

    mainGroup = []
    for node in cameraGroup:
        for index in range(node.GetDstCount()):
            if not isinstance(node.GetDst(index),
                              mobu.FBGroup):
                continue

            mainGroup.append(node)

    cameraGroup.extend(mainGroup)

    if not cameraGroup:
        return

    toggleState = not bool(cameraGroup[0].Show)

    for group in cameraGroup:
        group.Show = toggleState


class FpsCameraRigDiagnostic(object):
    """
        Data structure for fpsRig diagnostic
    """
    def __init__(self,
                 errorType,
                 picture,
                 caption,
                 message):
        self.errorType = errorType

        self.message = message

        self.caption = caption

        self.picture = picture

        self.inputCharacter = None


class FpsAnimationLayerData(object):
    """
        Data structure for animtion layer diagnostic:
        In order for the tool to work properly the preview Take must not have any layer set as override.
    """
    def __init__(self,
                 node):
        self.node = node

        self.modeType = None

        self.isInvalid = True

        self.validate()

    def validate(self):
        self.modeType = self.node.LayerMode

        if self.modeType == mobu.FBLayerMode.kFBLayerModeAdditive:
            self.isInvalid = False


class FpsAnimationTakeData(object):
    """
        Store take settings
    """
    def __init__(self,
                 node):
        self.node = node

        self.layers = []

        self.name = unicode(node.Name, "utf-8")

        self.isInvalid = True

        self.validate()

    def validate(self):
        self.layers = []

        if self.node.GetLayerCount() == 1:
            self.isInvalid = False

            return

        self.layers = [FpsAnimationLayerData(self.node.GetLayer(layerIndex))
                       for layerIndex in range(self.node.GetLayerCount())]

        invalidLayers = [layer
                         for layer in self.layers
                         if layer.isInvalid]

        if invalidLayers:
            return

        self.isInvalid = False
 

class FpsAnimationLayerValidator(object):
    """
        validation procedure for takes / layers
    """
    MISSING_TAKES = FpsCameraRigDiagnostic('missingTakes',
                                           'missingTakes.png',
                                           'missingTakes: please use the refresh button',
                                           ''.join(['Please unsure your UI Take list is synchronize']))

    UNSUPPORTED_BLENDING_MODES = FpsCameraRigDiagnostic('unsupportedLayers',
                                                        'unsupportedLayers.png',
                                                        'Please unsure the following takes are merge to the base layer',
                                                        ''.join(['Please unsure the following takes are merge to the base layer']))

    def __init__(self):
        self.takeArray = {}

        self.report = None

    def collect(self):
        self.takeArray = {}

        self.report = None

        for item in Globals.Scene.Takes:
            currentTakeData = FpsAnimationTakeData(item)

            self.takeArray[currentTakeData.name] = currentTakeData

    def prepareTakeLayerReport(self):
        reportArray = ['Please ensure the following takes are merged to the base layer']

        takeNames = [item.name 
                     for item in self.invalidTakes]

        reportArray.extend(takeNames)

        return '\n'.join(reportArray)

    def validateFromTakeNames(self,
                              takeNames):
        self.collect()

        existingTakeNames = list(set(takeNames).intersection(set(self.takeArray.keys())))

        if len(existingTakeNames) != len(takeNames):
            self.report = self.MISSING_TAKES

            return False

        self.invalidTakes = [self.takeArray[takeName]
                             for takeName in existingTakeNames
                             if self.takeArray[takeName].isInvalid]

        if self.invalidTakes:
            self.report = self.UNSUPPORTED_BLENDING_MODES

            self.report.message = self.prepareTakeLayerReport()

            return False

        return True


class FpsCameraRigValidator(object):
    """
        Validation procedure for the FpsCamera rig
    """
    MODEL_REVISION_FILE = '//rdr3/assets/anim/config/model_revisions.xml'

    CAMERA_GROUP = ('FpsCamera',
                    'FpsCameraPreview')

    FPS_CONSTRAINT_TYPES = ('fpsCameraConstraint',
                            'fpsHeadGyroscope')

    IK_TARGET_PROPERTIES = ('upVectorParentConstraint',
                            'targetParentConstraint')

    CHARACTER_MISMATCH = FpsCameraRigDiagnostic('characterMismatch',
                                                'characterMismatch.png',
                                                'Current character is different - please use the refresh button',
                                                ''.join(['Please unsure the current character in the toolbox',
                                                         '\nmatch the current scene character']))

    INCORRECT_CHARACTER_TYPE = FpsCameraRigDiagnostic('characterTypeMismatch',
                                                      'characterTypeMismatch.png',
                                                      'Invalid character detected - please use the refresh button',
                                                      ''.join(['Object clash',
                                                               '\nprovided inputCharacter is not of type FBCharacter']))

    MISSING_NAMESPACE = FpsCameraRigDiagnostic('namespaceMissing',
                                               'namespaceMissing.png',
                                               'Namespace missing: \nPlease use a referenced character and refresh the toolbox',
                                               ''.join(['Please unsure your character have a namespace']))

    EMPTY_NAMESPACE = FpsCameraRigDiagnostic('namespaceEmpty',
                                             'namespaceEmpty.png',
                                             'Namespace Empty: \nPlease use a referenced character and refresh the toolbox',
                                             ''.join(['Please unsure your character have a  valid namespace (seems empty)']))

    MISSING_FPS_CONSTRAINTS = FpsCameraRigDiagnostic('missingFpsConstraints',
                                                     'missingFpsConstraints.png',
                                                     'missingFpsConstraints: \nPlease force update your character and refresh the toolbox',
                                                     ''.join(['Please unsure both ',
                                                              'fpsCameraConstraint / ',
                                                              'fpsHeadGyroscope exists']))

    MISSING_CONEMASTER = FpsCameraRigDiagnostic('missingConeMaster',
                                                'missingConeMaster.png',
                                                'missing conemaster: \nPlease force update your character and refresh the toolbox',
                                                ''.join(['Please unsure your character have valid coneMaster object']))

    MISSING_HEAD_GYROSCOPE = FpsCameraRigDiagnostic('missingHeadGyroscope',
                                                    'missingHeadGyroscope.png',
                                                    'missing constraint: \nPlease force update your character and refresh the toolbox',
                                                    ''.join(['Please unsure your character have valid fpsHeadGyroscope constraint']))

    MISSING_IK_TARGET_CONSTRAINTS = FpsCameraRigDiagnostic('missingIkTargetConstraints',
                                                           'missingIkTargetConstraints.png',
                                                           'missing Ik Target Constraints: \nPlease force update your character and refresh the toolbox',
                                                           ''.join(['Please unsure both ',
                                                                    'upVectorParentConstraint / ',
                                                                    'targetParentConstraint exists']))

    REFERENCE_NAMESPACE_MISMATCH = FpsCameraRigDiagnostic('referenceNamespaceMismatch',
                                                          'referenceNamespaceMismatch.png',
                                                          'reference namespace mismatch: \nPlease force update your character and refresh the toolbox',
                                                          ''.join(['Please unsure your reference are clean up']))

    REFERENCE_BROKEN_NAMESPACE = FpsCameraRigDiagnostic('referenceBrokenNamespace',
                                                        'referenceBrokenNamespace.png',
                                                        'Broken Reference Namespace: \n\tPlease use the namespace correction tool to fix all the errors\n\ticon must be green',
                                                        ''.join(['Please unsure your reference are clean up']))

    NON_EXISTANT_REFERENCES = FpsCameraRigDiagnostic('noReferenceFound',
                                                     'noReferenceFound.png',
                                                     'missing references: \nPlease check your scene from missing referenced IG character',
                                                     ''.join(['could no find reference in scene']))

    REFERENCE_NOT_FOUND = FpsCameraRigDiagnostic('referenceUnknown',
                                                 'referenceUnknown.png',
                                                 'Unknown reference: \nPlease force update your character and refresh the toolbox',
                                                 ''.join(['Could not find reference from character',
                                                          'Please use reference editor fix up functions']))

    OBSOLETE_REFERENCE = FpsCameraRigDiagnostic('referenceObsolete',
                                                'referenceObsolete.png',
                                                'Obsolete character: \nPlease force update your character and refresh the toolbox',
                                                ''.join(['Please update your reference to the latest version']))

    NOT_INGAME_REFERENCE = FpsCameraRigDiagnostic('notIngameReference',
                                                  'notIngameReference.png',
                                                  'the selected character is not an Ingame reference: \nPlease Select an ingame character',
                                                  ''.join(['Please Select an ingame character']))

    MISSING_FPS_CAMERAS = FpsCameraRigDiagnostic('missingFpsCamera',
                                                 'missingFpsCamera.png',
                                                 'missing cameras: \nPlease force update your character and refresh the toolbox',
                                                 ''.join(['Please unsure both ',
                                                          'FpsCamera / ',
                                                          'FpsCameraPreview exists']))

    def __init__(self,
                 inputCharacter):
        self.namespace = None

        self.inputCharacter = inputCharacter

        self.headGyroscope = None

        self.fpscameraConstraint = None

        self.FpsCamera = None

        self.FpsCameraPreview = None

        self.coneMaster = None

        self.targetParentConstraint = None

        self.upVectorParentConstraint = None

        self.report = ""

        self.currentReference = None

    def assertCharacterMismatch(self):
        if not isinstance(self.inputCharacter,
                          mobu.FBCharacter):
            self.report = self.INCORRECT_CHARACTER_TYPE

            return True

        if mobu.FBApplication().CurrentCharacter != self.inputCharacter:
            self.report = self.CHARACTER_MISMATCH

            return True

        return False

    def assertMissingNamespace(self):
        characterNameSpace = Namespace.GetNamespace(self.inputCharacter)

        if characterNameSpace is None:
            self.report = self.MISSING_NAMESPACE

            return True

        if not characterNameSpace.strip():
            self.report = self.EMPTY_NAMESPACE

            return True

        self.namespace = characterNameSpace.strip()

        return False

    def assertConstraints(self):
        for constraint in Globals.Scene.Constraints:    
            if not constraint.LongName.lower().startswith(self.namespace.lower()):
                continue

            if constraint.ClassName().lower() == 'fpsCameraConstraint'.lower():
                self.fpscameraConstraint = constraint

            if constraint.ClassName().lower() == 'fpsHeadGyroscope'.lower():
                self.headGyroscope = constraint

        if not all([self.fpscameraConstraint,
                    self.headGyroscope]):
            self.report = self.MISSING_FPS_CONSTRAINTS

            return True

        return False

    def assertFpsCamera(self):
        for attribute in self.CAMERA_GROUP:
            camera = self.headGyroscope.ReferenceGet(GYROSCOPE_IO.index(attribute), 0)

            if camera is None:
                continue

            setattr(self,
                    attribute,
                    camera)

        if not all([self.FpsCamera,
                    self.FpsCameraPreview]):
            self.report = self.MISSING_FPS_CAMERAS

            return True

        return False

    def assertIkTargetConstraints(self):
        for attribute in self.IK_TARGET_PROPERTIES:
            componentProperty = self.coneMaster.PropertyList.Find(attribute)

            if not componentProperty:
                continue

            sourceCount = componentProperty.GetSrcCount()

            if not sourceCount:
                continue

            rigConstraint = componentProperty.GetSrc(0)

            setattr(self,
                    attribute,
                    rigConstraint)

        if not all([self.targetParentConstraint,
                    self.upVectorParentConstraint]):
            self.report = self.MISSING_IK_TARGET_CONSTRAINTS

            return True

        return False

    def assertConeMaster(self):
        fpsCameraConstraintData = ExtractFpsCameraConstraint(self.inputCharacter)

        if fpsCameraConstraintData is None:
            self.report = self.MISSING_HEAD_GYROSCOPE

            return True

        coneMaster = fpsCameraConstraintData.drivingPropertyOwner

        if not coneMaster:
            self.report = self.MISSING_CONEMASTER

            return True

        self.coneMaster = coneMaster

        return False

    def assertReferenceFromNamespace(self):
        startCaption = ['Broken Reference Namespace: \n\tPlease use the namespace correction tool to fix all the errors\n\ticon must be green']

        self.REFERENCE_BROKEN_NAMESPACE.caption = startCaption[0]

        sceneModelArray = mobu.FBComponentList()
        mobu.FBFindObjectsByName('RS_Null:*',
                                 sceneModelArray,
                                 True,
                                 True)

        sceneModelArray = list(set(sceneModelArray))

        if not sceneModelArray:
            self.report = self.NON_EXISTANT_REFERENCES

            return True

        nullSourceArray = [item
                           for item in sceneModelArray
                           if item.PropertyList.Find('rs_Asset_Type') is not None]

        referenceAnchors = [item
                            for item in nullSourceArray
                            if item.Parent.LongName.startswith('REFERENCE:')
                            and item.PropertyList.Find('rs_Asset_Type').Data.lower() == 'CharacterIngame'.lower()]

        duplicateNamespaceEntries = defaultdict(list)

        for node in referenceAnchors:
            namespaceProperty = node.PropertyList.Find('Namespace')

            if namespaceProperty is None:
                continue

            duplicateNamespaceEntries[namespaceProperty.Data].append(node.LongName)

        for item in duplicateNamespaceEntries.keys():
            if len(duplicateNamespaceEntries[item])>1:
                startCaption.append('\n\tPlease correct {} \n\tClashing namespace property value {} detected'.format(duplicateNamespaceEntries[item],
                                                                                                        item))

                self.REFERENCE_BROKEN_NAMESPACE.caption = ''.join(startCaption)

                self.report = self.REFERENCE_BROKEN_NAMESPACE

                return True

        return False

    def assertPerforceVersion(self):
        referenceManager = assetReferenceManager.Manager()

        self.currentReference = referenceManager.GetReferenceByNamespace(self.namespace)

        if not self.currentReference:
            self.report = self.REFERENCE_NOT_FOUND

            return True

        if self.currentReference.CurrentVersion != self.currentReference.LatestVersion:
            self.report = self.OBSOLETE_REFERENCE

            return True

        return False

    def assertFindReference(self):
        referenceManager = assetReferenceManager.Manager()

        self.currentReference = referenceManager.GetReferenceByNamespace(self.namespace)

        if not self.currentReference:
            self.report = self.REFERENCE_NOT_FOUND

            return True

        return False

    def assertIngameCharacter(self):
        if self.currentReference.PropertyTypeName != 'CharacterIngame':
            self.report = self.NOT_INGAME_REFERENCE

            return True

        return False

    def hasReferenceEditorIssues(self):
        if self.assertReferenceFromNamespace():
            return True

        if self.assertFindReference():
            return True

        if self.assertIngameCharacter():
            return True

        return False

    def hasValidFpsCameraRig(self):
        if self.assertCharacterMismatch():
            return False

        if self.assertMissingNamespace():
            return False

        if self.assertConstraints():
            if self.hasReferenceEditorIssues():
                return False

            return False

        if self.assertFpsCamera():
            if self.hasReferenceEditorIssues():
                return False

            return False

        if self.assertConeMaster():
            if self.hasReferenceEditorIssues():
                return False

            return False

        if self.assertIkTargetConstraints():
            if self.hasReferenceEditorIssues():
                return False

            return False

        return True


class FpsCameraValidator(object):
    """
        Validation procedure for the associated space switching constraints
    """
    ARROW_POINT_COUNT = 3

    def __init__(self,
                 inputCharacter):
        self.setting = FpsCameraTargetModeSettings(inputCharacter)

        self.namespace = None

        self.arrowLength = 6.0

        self.arrowWidth = 1.0

        self.arrowHeight = 2.125

        self.addArrow()

        self.refreshConstraints()

    def refreshConstraints(self):
        if not self.setting.targetConstraint:
            return

        self.setting.targetConstraint.FreezeSuggested()

        attributeArray = []

        for property in self.setting.targetConstraint.PropertyList:
            if property.Name.endswith('Offset T'):
                attributeArray.append(property)

            elif property.Name.endswith('Offset R'):
                attributeArray.append(property)

            elif property.Name.endswith('Offset S'):
                attributeArray.append(property)

        attributeArray.append(self.setting.targetConstraint.PropertyList.Find('Lock'))

        with ContextManagers.Undo(undoStackName='FpsCameraValidator/refreshConstraints',
                                  attributeArray=attributeArray):
            self.setting.targetConstraint.Lock = True

            self.setting.targetConstraint.Lock = False

            for property in self.setting.targetConstraint.PropertyList:
                if property.Name.endswith('Offset T'):
                    property.Data = mobu.FBVector3d(0, 0, 0)

                elif property.Name.endswith('Offset R'):
                    property.Data = mobu.FBVector3d(0, 0, 0)

                elif property.Name.endswith('Offset S'):
                    property.Data = mobu.FBVector3d(1, 1, 1)

            self.setting.targetConstraint.Lock = True

    def getNamespace(self,
                     inputNode):
        currentNamespace = Namespace.GetNamespace(inputNode)

        if len(currentNamespace)==0:
            return None

        return currentNamespace

    def createArrow(self,
                    arrowName,
                    scale=0.12):
        cloneMesh = mobu.FBMesh("cloneMesh")
        cloneMesh.GeometryBegin()

        cloneMesh.VertexInit(self.ARROW_POINT_COUNT,
                             False, 
                             False)

        vertexArray = [mobu.FBVertex(0.0, 0.0, -self.arrowWidth*scale, 0.0),
                       mobu.FBVertex(self.arrowLength*scale, 0.0, 0.0, 0.0),
                       mobu.FBVertex(0.0, 0.0, self.arrowWidth*scale, 0.0),
                       mobu.FBVertex(0.0, 0.0, 0.0, 0.0),
                       mobu.FBVertex(self.arrowLength*scale, 0.0, 0.0, 0.0),
                       mobu.FBVertex(0.0, self.arrowHeight*scale, 0.0, 0.0)]

        for vertexIndex in xrange(len(vertexArray)):
            cloneMesh.VertexAdd(vertexArray[vertexIndex])

        cloneMesh.PolygonBegin()

        for polyVtxIndex in xrange(self.ARROW_POINT_COUNT):
            cloneMesh.PolygonVertexAdd(polyVtxIndex)

        cloneMesh.PolygonEnd()

        cloneMesh.PolygonBegin()
        
        for polyVtxIndex in xrange(self.ARROW_POINT_COUNT):
            cloneMesh.PolygonVertexAdd(polyVtxIndex+self.ARROW_POINT_COUNT)

        cloneMesh.PolygonEnd()

        cloneMesh.GeometryEnd()

        output = mobu.FBModelCube(arrowName)
        output.Geometry = cloneMesh
        output.Show = True
        output.Visibility = True
        output.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire

        if self.namespace is not None:
            output.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBConcatNamespace,
                                          self.namespace)

        return output

    def addArrow(self):
        if not all([self.setting.coneMaster,
                    self.setting.moverUpVectorHook]):
            return

        self.namespace = self.getNamespace(self.setting.coneMaster)

        if len(self.setting.moverUpVectorHook.Children) == 0:
            output = self.createArrow('fpsUpVectorMoverSpaceArrow1')

            output.Parent = self.setting.moverUpVectorHook

            output.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

            output.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

            output.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)

            output.GeometricRotation = mobu.FBVector3d(0, 180.0, -90.0)

            output.PropertyList.Find('Translation (Lcl)').SetLocked(True)

            output.PropertyList.Find('Rotation (Lcl)').SetLocked(True)

            output.PropertyList.Find('Scaling (Lcl)').SetLocked(True)

            output.Pickable = False

    def setDefaultWeights(self,
                          inputConstraint,
                          setSecondTargetAsDefault=False):
        constraintWeight = [100.0, 0.0]

        if setSecondTargetAsDefault:
            constraintWeight = [0.0, 100.0]

        writeIndex = 0

        attributeArray = [item
                          for item in inputConstraint.PropertyList
                          if item.Name.endswith('.Weight')]

        startTime = mobu.FBPlayerControl().ZoomWindowStart

        with ContextManagers.Undo(undoStackName='FpsCameraValidator/setDefaultWeights',
                                  attributeArray=attributeArray):
            for item in attributeArray:
                if not item.IsAnimated():
                    item.SetAnimated(True)

                targetAnimationNode = item.GetAnimationNode()

                if targetAnimationNode.KeyCount > 0:
                    targetAnimationNode.FCurve.EditClear()
    
                if targetAnimationNode.FCurve:
                    targetAnimationNode.FCurve.EditBegin()

                item.Data = constraintWeight[writeIndex]
                targetAnimationNode.KeyCandidate(startTime)

                if targetAnimationNode.FCurve:
                    targetAnimationNode.FCurve.EditEnd()   

                writeIndex += 1

    def isInitialized(self,
                      inputConstraint):
        writeIndex = 0
        weightResult = 0.0

        for item in inputConstraint.PropertyList:
            if not item.Name.endswith('.Weight'):
                continue

            weightResult += item.Data
            writeIndex += 1

        return not(weightResult > 199.0)
    
    def consolidateConstraintWeights(self):
        if not all([self.setting.upVectorConstraint,
                    self.setting.targetConstraint]):
            return False

        if not self.isInitialized(self.setting.upVectorConstraint):
            self.setDefaultWeights(self.setting.upVectorConstraint)

        if not self.isInitialized(self.setting.targetConstraint):
            self.setDefaultWeights(self.setting.targetConstraint,
                                   setSecondTargetAsDefault=True)

        return True


class FpsCameraLimitModeSettings(object):
    """
        Utility class to manage override limit mode
    """
    def __init__(self,
                 inputCharacter):
        self.inputCharacter = inputCharacter

        self.gyroscope = None

        self.disableLimit = False

        self.collect()

    def collect(self):
        fpsCameraConstraintData = ExtractFpsCameraConstraint(self.inputCharacter)

        if fpsCameraConstraintData is None:
            return

        self.gyroscope = fpsCameraConstraintData.constraint

        self.coneMaster = fpsCameraConstraintData.drivingPropertyOwner

        disableLimitProperty = self.gyroscope.PropertyList.Find('Camera Limits')

        if disableLimitProperty:
            self.disableLimit = disableLimitProperty.Data

    def toggleState(self,
                    inputState):
        fpsCameraConstraintData = ExtractFpsCameraConstraint(self.inputCharacter)

        if fpsCameraConstraintData is None:
            return

        self.gyroscope = fpsCameraConstraintData.constraint

        self.coneMaster = fpsCameraConstraintData.drivingPropertyOwner

        disableLimitProperty = self.gyroscope.PropertyList.Find('Camera Limits')

        if disableLimitProperty:
            disableLimitProperty.Data = inputState


class FpsCameraTargetModeSettings(object):
    """
        Datastructure used to collect and set aim constraint mode property values
    """
    def __init__(self,
                 inputCharacter):
        self.inputCharacter = inputCharacter

        self.gyroscope = None

        self.coneMaster = None

        self.upVectorConstraint = None

        self.targetConstraint = None

        self.useAimTarget = False

        self.useWorldMover = False

        self.moverUpVectorHook = None

        self.upVectorHeadSpaceWeight = 100.0

        self.targetMoverSpaceWeight = 100.0

        self.rotationDominanceWeight = -1.0

        self.enableHeadStabilization = False
        
        self.stabilizeInAimSpace = False

        self.horizontalStabilization = 0.0

        self.verticalStabilization = 0.0

        self.verticalStabilizationProperty = None

        self.horizontalStabilizationProperty = None

        self.enableHeadStabilizationProperty = None
        
        self.stabilizeInAimSpaceProperty = None

        self.RotationDominanceProperty = None

        self.useWorldProperty = None

        self.useAimTargetProperty = None

        self.upVectorConstraint = None

        self.targetConstraint = None

        self.collect()
    
    def isCharacterValid(self):
        return self.inputCharacter is not None
    
    def _onCharacterUnbind(self, control, event):
        self.inputCharacter = None
        self.clear()
    
    def getHookComponent(self,
                         inputCharacter,
                         groupLabel):
        if not self.isCharacterValid():
            return None
        
        fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
        if fpsCameraConstraintData is None:
            return None

        gyroscope = fpsCameraConstraintData.constraint
        coneMaster = fpsCameraConstraintData.drivingPropertyOwner

        if not coneMaster:
            return None

        disableConeMasterRotationProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
        if disableConeMasterRotationProperty is None:
            if _report_MissingComponents:
                print '[getHookComponent] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

            return None

        componentProperty = coneMaster.PropertyList.Find(groupLabel)
        if not componentProperty:
            return None

        if componentProperty.GetSrcCount() == 0:
            return None

        return componentProperty.GetSrc(0)

    def getWeight(self,
                  constraint):
        for item in constraint.PropertyList:
            if not item.Name.endswith('.Weight'):
                continue

            return item.Data 

        return 100.0

    def collect(self):
        if not self.isCharacterValid():
            return
        
        self.inputCharacter.OnUnbind.Add(self._onCharacterUnbind)
        
        fpsCameraConstraintData = ExtractFpsCameraConstraint(self.inputCharacter)

        if fpsCameraConstraintData is None:
            return

        self.gyroscope = fpsCameraConstraintData.constraint

        self.coneMaster = fpsCameraConstraintData.drivingPropertyOwner

        self.moverUpVectorHook = self.getHookComponent(self.inputCharacter,
                                                       'moverUpVectorHook')

        self.upVectorConstraint = self.getHookComponent(self.inputCharacter,
                                                       'upVectorParentConstraint')

        self.targetConstraint = self.getHookComponent(self.inputCharacter,
                                                      'targetParentConstraint')

        self.useAimTargetProperty = self.coneMaster.PropertyList.Find('UseAimTarget')

        self.useWorldProperty = self.coneMaster.PropertyList.Find('UseWorldMover')

        self.RotationDominanceProperty = self.coneMaster.PropertyList.Find('RotationDominance')

        self.enableHeadStabilizationProperty = self.coneMaster.PropertyList.Find('Enable Head Stabilization')
        
        self.stabilizeInAimSpaceProperty = self.coneMaster.PropertyList.Find('Stabilize In Aim Space')

        self.horizontalStabilizationProperty = self.coneMaster.PropertyList.Find('Horizontal Stabilization')

        self.verticalStabilizationProperty = self.coneMaster.PropertyList.Find('Vertical Stabilization')

        self.refresh()

    def clear(self):
        if self.isCharacterValid():
            self.inputCharacter.OnUnbind.Remove(self._onCharacterUnbind)
        
        self.verticalStabilizationProperty = None

        self.horizontalStabilizationProperty = None

        self.enableHeadStabilizationProperty = None
        
        self.stabilizeInAimSpaceProperty = None

        self.RotationDominanceProperty = None

        self.useWorldProperty = None

        self.useAimTargetProperty = None

        self.upVectorConstraint = None

        self.targetConstraint = None

        self.inputCharacter = None

        self.gyroscope = None

        self.coneMaster = None

        self.upVectorConstraint = None

        self.targetConstraint = None

        self.moverUpVectorHook = None

    def refresh(self):
        if not self.isCharacterValid():
            return None
        
        if self.verticalStabilizationProperty is None:
            return
        try:
            if self.verticalStabilizationProperty:
                self.verticalStabilization = self.verticalStabilizationProperty.Data
    
            if self.horizontalStabilizationProperty:
                self.horizontalStabilization = self.horizontalStabilizationProperty.Data
    
            if self.enableHeadStabilizationProperty:
                self.enableHeadStabilization = self.enableHeadStabilizationProperty.Data
            
            if self.stabilizeInAimSpaceProperty:
                self.stabilizeInAimSpace = self.stabilizeInAimSpaceProperty.Data
    
            if self.RotationDominanceProperty:
                self.rotationDominanceWeight = float(self.RotationDominanceProperty.Data)
    
            if self.useWorldProperty:
                self.useWorldMover = bool(self.useWorldProperty.Data)
    
            if self.useAimTargetProperty:
                self.useAimTarget = bool(self.useAimTargetProperty.Data)
    
            if self.upVectorConstraint:
                self.upVectorHeadSpaceWeight = self.getWeight(self.upVectorConstraint)
    
            if self.targetConstraint:
                self.targetMoverSpaceWeight = self.getWeight(self.targetConstraint)
        except unbind.UnboundWrapperError:
            # Character or required component was deleted so disable refresh from scene properties
            self.clear()


class FpsFbxAnimationMapping(object):
    """
        Utility class which will get a set of fbx animation curve and fill out FBAnimation object accordingly
    """
    AXIS_SUFFIX_ARRAY = ('X', 'Y', 'Z')

    def __init__(self,
                 curve):
        self.motionbuilderCurve = mobu.FBFCurve()

        self.curveName = curve.GetName()

        self.modelName = None

        self.propertyName = None

        self.axis = -1

        self.hashKey = self.getHashKey()

        self.collect(curve)

    def collect(self,
                fbxCurve):
        self.motionbuilderCurve.EditBegin()

        for keyIndex in xrange(fbxCurve.KeyGetCount()):
            timeValue = mobu.FBTime(0, 0, 0, 0, 0, mobu.FBTimeMode.kFBTimeMode30Frames)

            timeValue.SetSecondDouble(fbxCurve.KeyGetTime(keyIndex).GetSecondDouble())

            self.motionbuilderCurve.KeyAdd(timeValue,
                                           fbxCurve.KeyGetValue(keyIndex))

        self.motionbuilderCurve.EditEnd()

    def getHashKey(self):
        mapArray = self.curveName.split('&')

        self.modelName = mapArray[0]

        self.propertyName = mapArray[1]

        if ':' in self.propertyName:
            self.propertyName = mapArray[1].split(':')[1]

        hashKey = '{0}&{1}'.format(self.modelName,
                                   self.propertyName)

        if hashKey[-1] in self.AXIS_SUFFIX_ARRAY:
            self.axis = self.AXIS_SUFFIX_ARRAY.index(hashKey[-1])

            hashKey = hashKey[:-1]

        return hashKey


class FpsAnimationNode(object):
    """
        Base utility class which will unpack animation and save it into an fbx container.
    """
    ANIMATION_CURVE_PATTERN = '{MODEL}&{PROPERTY}'

    AXIS_SUFFIX_ARRAY = ('X', 'Y', 'Z')

    def __init__(self,
                 inputModel,
                 inputPropertyName):
        self.inputModel = inputModel

        self.inputPropertyName = inputPropertyName

        self.property = self.inputModel.PropertyList.Find(inputPropertyName)

        self.property.SetAnimated(True)

        self.curveNodeArray = []

        self.fbxIoNodeArray = []

    def getHashKey(self):
        sourceProperty = self.inputPropertyName

        if ':' in sourceProperty:
            sourceProperty = self.inputPropertyName.split(':')[1]

        return '{0}&{1}'.format(self.inputModel.Name,
                                sourceProperty)

    def focusProperty(self):
        self.inputModel.Selected = True

        self.property.SetFocus(True)

    def releaseProperty(self):
        self.inputModel.Selected = False

        self.property.SetFocus(False)

    def collectCurveNodes(self):
        animationNode = self.property.GetAnimationNode()

        self.curveNodeArray = [node.FCurve
                               for node in animationNode.Nodes]

    def export(self,
               sceneHandle):
        self.transferKeyToFbxContainer(sceneHandle,
                                       self.curveNodeArray[0])

    def load(self):
        if not self.fbxIoNodeArray:
            return

        self.curveNodeArray[0].KeyReplaceBy(self.fbxIoNodeArray[0].motionbuilderCurve)

    def transferKeyToFbxContainer(self,
                                  sceneHandle,
                                  curveNode,
                                  suffix=''):
        curveName = self.ANIMATION_CURVE_PATTERN.format(MODEL=self.inputModel.Name,
                                                        PROPERTY='{0}{1}'.format(self.inputPropertyName,
                                                                                 suffix))

        fbxAnimationCurve = fbx.FbxAnimCurve.Create(sceneHandle, 
                                                    curveName)

        fbxAnimationCurve.KeyModifyBegin()

        writeTime = fbx.FbxTime()
        for keyIndex, curveKey in enumerate(curveNode.Keys):
            writeTime.SetSecondDouble(curveKey.Time.GetSecondDouble())

            fbxAnimationCurve.KeyAdd(writeTime)

            fbxAnimationCurve.KeySet(keyIndex, 
                                     writeTime, 
                                     curveKey.Value,
                                     fbx.FbxAnimCurveDef.eInterpolationLinear)

        fbxAnimationCurve.KeyModifyEnd()

    def __repr__(self):
        reportData = ['\n<{0}>'.format(self.__class__.__name__)]

        for attribute in ('inputModel',
                          'inputPropertyName',
                          'curveNodeArray',
                          'fbxIoNodeArray'):
            currentValue = getattr(self,
                                   attribute)

            if isinstance(currentValue, list):
                reportData.append('\n\t{0}:{1}'.format(attribute,
                                                       currentValue))
            elif isinstance(currentValue, basestring):
                reportData.append('\n\t{0}:{1}'.format(attribute,
                                                       currentValue))
            else:
                reportData.append('\n\t{0}:{1}'.format(attribute,
                                                       currentValue.Name))

        return ''.join(reportData)


class FpsVectorAnimationNode(FpsAnimationNode):
    def __init__(self,
                 inputModel,
                 inputPropertyName):
        super(FpsVectorAnimationNode, self).__init__(inputModel,
                                                     inputPropertyName)

    def export(self,
               sceneHandle):
        for index, curveNode in enumerate(self.curveNodeArray):
            self.transferKeyToFbxContainer(sceneHandle,
                                           self.curveNodeArray[index],
                                           suffix=self.AXIS_SUFFIX_ARRAY[index]) 

    def load(self):
        if not self.fbxIoNodeArray:
            return

        for index, curveNode in enumerate(self.curveNodeArray):
            writeIndex = self.fbxIoNodeArray[index].axis

            self.curveNodeArray[writeIndex].KeyReplaceBy (self.fbxIoNodeArray[index].motionbuilderCurve)


class FpsAttributeAnimationNode(FpsAnimationNode):
    def __init__(self,
                 inputModel,
                 inputPropertyName):
        super(FpsAttributeAnimationNode, self).__init__(inputModel,
                                                        inputPropertyName)

    def collectCurveNodes(self):
        animationNode = self.property.GetAnimationNode()

        self.curveNodeArray = [animationNode.FCurve]


class FpsAnimationManager(object):
    """
        This manager will be responsible to save/load animation for the fps camera rig.
    """
    CONE_MASTER_ATTRIBUTES = ('DOF_FOV',
                              'RotationDominance',
                              'radius',
                              'Down/Up',
                              'DollyOut/DollyIn',
                              'Left/Right',
                              'UseWorldMover',
                              'Dolly Offset Multiplier',
                              'Start Range Distance',
                              'UseAimTarget')

    def __init__(self,
                 inputCharacter):
        self.inputCharacter = inputCharacter

        self.gyroscope = None

        self.coneMaster = None

        self.upVectorConstraint = None

        self.targetConstraint = None

        self.upVectorSpaceHook = None

        self.targetSpaceHook = None

        self.upVectorNode = None

        self.targetNode = None

        self.animationNodes = []

        self.nodeMapping = {}

        self.collect()

    def getConstraintComponent(self,
                               inputCharacter,
                               groupLabel):
        fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
        if fpsCameraConstraintData is None:
            return

        gyroscope = fpsCameraConstraintData.constraint
        coneMaster = fpsCameraConstraintData.drivingPropertyOwner

        if not coneMaster:
            return

        disableConeMasterRotationProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
        if disableConeMasterRotationProperty is None:
            if _report_MissingComponents:
                print '[getConstraintComponent] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

            return

        component = gyroscope.ReferenceGet(GYROSCOPE_IO.index(groupLabel), 0)

        return component

    def getHookComponent(self,
                         inputCharacter,
                         groupLabel):
        fpsCameraConstraintData = ExtractFpsCameraConstraint(inputCharacter)
        if fpsCameraConstraintData is None:
            return None

        gyroscope = fpsCameraConstraintData.constraint
        coneMaster = fpsCameraConstraintData.drivingPropertyOwner

        if not coneMaster:
            return None

        disableConeMasterRotationProperty = coneMaster.PropertyList.Find('DisableConeMasterRotation')
        if disableConeMasterRotationProperty is None:
            if _report_MissingComponents:
                print '[getHookComponent] - Sorry, {} doesnt have the latest target rig installed'.format(inputCharacter.LongName)

            return None

        componentProperty = coneMaster.PropertyList.Find(groupLabel)
        if not componentProperty:
            return None

        if componentProperty.GetSrcCount() == 0:
            return None

        return componentProperty.GetSrc(0)

    def collect(self):
        fpsCameraConstraintData = ExtractFpsCameraConstraint(self.inputCharacter)

        if fpsCameraConstraintData is None:
            return

        self.gyroscope = fpsCameraConstraintData.constraint

        self.coneMaster = fpsCameraConstraintData.drivingPropertyOwner

        self.upVectorConstraint = self.getHookComponent(self.inputCharacter,
                                                       'upVectorParentConstraint')

        self.targetConstraint = self.getHookComponent(self.inputCharacter,
                                                      'targetParentConstraint')

        self.upVectorSpaceHook = self.getHookComponent(self.inputCharacter,
                                                       'moverUpVectorHook')

        self.targetSpaceHook = self.getHookComponent(self.inputCharacter,
                                                     'worldTargetHook')

        self.upVectorNode = self.getConstraintComponent(self.inputCharacter,
                                                        'Up Target')

        self.targetNode = self.getConstraintComponent(self.inputCharacter,
                                                      'Aim Target')
    
    def allRequiredComponentsExist(self):
        return all([self.upVectorSpaceHook,
                    self.targetSpaceHook,
                    self.upVectorNode,
                    self.targetNode])
    
    def collectAnimationNodes(self):
        if not self.allRequiredComponentsExist():
            return False
        
        self.animationNodes = []
        for model in (self.upVectorSpaceHook,
                      self.targetSpaceHook,
                      self.upVectorNode,
                      self.targetNode):
            self.animationNodes.append(FpsVectorAnimationNode(model,
                                                              'Translation (Lcl)'))

            self.animationNodes.append(FpsVectorAnimationNode(model,
                                                              'Rotation (Lcl)'))

        self.animationNodes.append(FpsVectorAnimationNode(self.coneMaster,
                                                          'Rotation (Lcl)'))

        for attribute in self.CONE_MASTER_ATTRIBUTES:
            self.animationNodes.append(FpsAttributeAnimationNode(self.coneMaster,
                                                                 attribute))

        for constraint in (self.upVectorConstraint,
                           self.targetConstraint):
            for item in constraint.PropertyList:
                if not item.Name.endswith('.Weight'):
                    continue

                self.animationNodes.append(FpsAttributeAnimationNode(constraint,
                                                                     item.Name))

        self.collectAllCurveNodes()

        return True
    
    def collectAllCurveNodes(self):
        for item in self.animationNodes:
            item.collectCurveNodes()

    def getPlotSettings(self):
        bakeSettings = mobu.FBPlotOptions()

        bakeSettings.ConstantKeyReducerKeepOneKey = True

        bakeSettings.PlotAllTakes = False

        bakeSettings.PlotOnFrame = True

        bakeSettings.PlotOnFrame = True

        bakeSettings.PlotTranslationOnRootOnly = False

        bakeSettings.PreciseTimeDiscontinuities = True

        bakeSettings.PlotLockedProperties = False

        bakeSettings.UseConstantKeyReducer = False

        bakeSettings.PlotPeriod = mobu.FBTime(0, 0, 0, 1)

        bakeSettings.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterNone

        bakeSettings.PlotTangentMode = mobu.FBPlotTangentMode.kFBPlotTangentModeAuto

        return bakeSettings

    def bakeAnimation(self):
        with ContextManagers.ClearModelSelection():
            for item in self.animationNodes:
                item.focusProperty()

            mobu.FBSystem().CurrentTake.PlotTakeOnSelectedProperties(self.getPlotSettings())

            for item in self.animationNodes:
                item.releaseProperty()

    def exportAnimation(self,
                        fbxFile):
        if not self.allRequiredComponentsExist():
            return False

        fbxManager = fbx.FbxManager.Create()

        sceneHandle = fbx.FbxScene.Create(fbxManager, "")

        defautTakeNode = fbx.FbxAnimStack.Create(sceneHandle, 
                                                 "take1")

        defaultLayer = fbx.FbxAnimLayer.Create(sceneHandle, 
                                               "Layer0")

        defautTakeNode.AddMember(defaultLayer)

        for item in self.animationNodes:
            item.export(sceneHandle)

        exporter = fbx.FbxExporter.Create(fbxManager, "")
                                                                      
        exporter.Initialize(fbxFile, -1)

        exporter.Export(sceneHandle)

        return True

    def importAnimation(self,
                        fbxFile):
        self.collectAnimationNodes()

        if not self.allRequiredComponentsExist():
            return False

        sceneHandle = pipelineFbx.getHandle(fbxFile)

        animationCurveArray = [FpsFbxAnimationMapping(sceneHandle.GetSrcObject(index))
                               for index in xrange(sceneHandle.GetSrcObjectCount())
                               if isinstance(sceneHandle.GetSrcObject(index),
                                             fbx.FbxAnimCurve)]

        self.nodeMapping = {node.getHashKey():node
                            for node in self.animationNodes}

        for item in animationCurveArray:
            self.nodeMapping[item.hashKey].fbxIoNodeArray.append(item)

        for node in self.animationNodes:
            node.load()

        return True