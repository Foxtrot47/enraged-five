"""
    Legacy widget which can helps swap old male_skeleton asset with a reference player_zero_warm_weather.

    almost deprecated.
"""

import inspect
import os
from functools import partial

from PySide import QtGui, QtCore
import pyfbsdk as mobu

from RS import Globals
from RS import Config, Core 

from RS.Utils.Scene import Take
from RS.Tools import UI
from RS.Utils import Namespace
from RS.Tools.FirstPersonController.Widget import fpsCharacterWidget

from RS import Config
from RS.Core.AssetSetup import fpsCameraPatcher


class PlayerZeroImporterWidget(QtGui.QWidget):
    def __init__(self,
                 characterWidget):
        self.characterWidget = characterWidget

        self.IgFolder = os.path.join(Config.Project.Path.Art,
                                     'animation',
                                     'resources',
                                     'characters',
                                     'Models',
                                     'ingame',
                                     'IG')

        self.IgFolder.replace('\\', '/')

        self.bipedAssets = ['Player_Zero/Player_Zero-META_OUTFIT_WARM_WEATHER.fbx']

        self.bipedNames = ['player_zero_warm_weather']

        super(PlayerZeroImporterWidget, self).__init__()

        self.groupLabel = "Replace IG Reference"

        self.buttonSize = 18

        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        self.mainLayout = QtGui.QVBoxLayout()
        characterLayout = QtGui.QHBoxLayout()

        self.InputGroupBox =  QtGui.QGroupBox(self.groupLabel)
        self.componentComboBox = QtGui.QComboBox(self)

        self.refreshPushButton = QtGui.QPushButton("")
        self.refreshPushButton.setMaximumHeight(self.buttonSize)
        self.refreshPushButton.setMaximumWidth(self.buttonSize)
        self.refreshPushButton.setToolTip('Replace current character reference')

        self.componentComboBox.setMinimumHeight(self.buttonSize)

        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.mainLayout.setSpacing(2)

        characterLayout.addWidget(self.componentComboBox)
        characterLayout.addWidget(self.refreshPushButton)

        characterLayout.setContentsMargins(6, 2, 6, 4)

        self.mainLayout.addWidget(self.InputGroupBox)
        self.InputGroupBox.setLayout(characterLayout)
        self.setLayout(self.mainLayout)

        self._setRefreshIcon()

        self.componentComboBox.clear()

        for name in self.bipedNames:
            self.componentComboBox.addItem(name)

        self.refreshPushButton.pressed.connect(self._mergeRig)

    def _setRefreshIcon(self):
        """
            Build Refresh namespace button
        """
        currentPath = os.path.dirname(inspect.getfile(self.setupUi)).replace('\\', '/')
        currentPath = currentPath.split('RS/Tools/') 

        imagePath =  os.path.join(currentPath[0],
                                  'RS',
                                  'Tools',
                                  'DressPicker',
                                  'Dialogs',
                                  'data',
                                  'CreateReference.png')

        self.refreshPushButton.setIcon(QtGui.QIcon(QtGui.QPixmap(QtGui.QImage(imagePath))))

    def _mergeRig(self):
        inputRigPath = os.path.join(self.IgFolder,
                                    self.bipedAssets[self.componentComboBox.currentIndex()])

        inputRigPath.replace('\\', '/')

        if len(inputRigPath)==0:
            return

        self._updatePlayerZero(inputRigPath)

        self.characterWidget._refreshCharacterComboBox()

    def _updatePlayerZero(self,
                          inputRigPath):
        utils = fpsCameraPatcher.Patcher()

        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return

        inputCharacter = None
        for character in Core.System.Scene.Characters:
            if str(character.LongName) != currentCharacter:
                continue

            inputCharacter = character

        if inputCharacter is None:
            return

        utils.referencePlayerRig(inputRigPath,
                                 inputCharacter)


class FpsRigFixUpWidget(QtGui.QWidget):
    def __init__(self,
                 characterWidget):
        self.characterWidget = characterWidget

        # Main window settings
        super(FpsRigFixUpWidget, self).__init__() 

        self.setupUi()

    def setupUi(self):
        """
        Setup the UI
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.setSpacing(5)

        # Create Widgets 
        self.importWidget = PlayerZeroImporterWidget(self)

        self.purgeButton = QtGui.QPushButton('Purge legacy FPCamera rigs')

        self.purgeButton.setMinimumHeight(40)

        mainLayout.addWidget(self._createSeparator())

        mainLayout.addWidget(self.purgeButton)

        mainLayout.addWidget(self.importWidget)

        # Add widgets to Layouts
        self.setLayout(mainLayout)

        self.purgeButton.pressed.connect(self._purgeLegacyRig)

    def _createSeparator(self):
        """
        Internal Method

        add line separator between widget
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _purgeLegacyRig(self):
        utils = fpsCameraPatcher.Patcher()

        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if not currentCharacter:
            return

        currentCharacterNamespace = utils.getNamespaceFromComponent(currentCharacter)

        utils.fixCurrentScene(currentCharacterNamespace)
