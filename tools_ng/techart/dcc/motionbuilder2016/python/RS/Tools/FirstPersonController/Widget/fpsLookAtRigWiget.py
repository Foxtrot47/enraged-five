"""
    Deprecated module, now the fpsCamera constraint have a Target mode.
"""
import inspect
import os
from functools import partial

from PySide import QtGui, QtCore
import pyfbsdk as mobu


from RS import Core 
from RS import Globals
from RS.Utils.Scene import Take
from RS.Utils import Namespace
from RS.Tools.FirstPersonController.Widget import fpsRigFixUpWiget
from RS.Core.Character import FPSManipulator



class FpsLookAtRigWiget(QtGui.QWidget):
    def __init__(self):
        super(FpsLookAtRigWiget, self).__init__() 

        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        mainLayout.setContentsMargins(0, 0, 0, 0)

        mainLayout.setSpacing(5)

        self.createRigButton = QtGui.QPushButton('Build FPSCamera Look At Rig')

        self.createRigButton.setMinimumHeight(40)

        self.characterWidget = fpsRigFixUpWiget.CharacterWidget(self)

        mainLayout.addWidget(self.characterWidget)

        mainLayout.addWidget(self._createSeparator())

        mainLayout.addWidget(self.createRigButton)

        self.setLayout(mainLayout)

        self.createRigButton.pressed.connect(self._buildRig)

    def _createSeparator(self):
        targetLine = QtGui.QFrame()

        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _buildRig(self):
        inputCharacterName = str(self.characterWidget.componentComboBox.currentText())
    
        currentCharacter = None

        for character in Core.System.Scene.Characters:
            if character.LongName == inputCharacterName:
                currentCharacter = character
                break

        if currentCharacter is None:
            return

        utils = FPSManipulator.FpsLookAtRig()

        utils.inputCharacter = currentCharacter

        utils.build()