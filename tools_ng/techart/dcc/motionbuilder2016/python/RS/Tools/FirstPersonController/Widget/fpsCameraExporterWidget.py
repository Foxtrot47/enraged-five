"""
    This UI will prepare the additional model need for the rockstar animation tool
    Equivalent to autopopulate but with fpsCamera rig components
"""

from PySide import QtGui, QtCore

from RS import Config, Core 
from RS.Tools import UI
from RS.Tools.FirstPersonController.Widget import fpsCharacterWidget
from RS.Tools.FirstPersonController.Widget import fpsProcessTakeWidget

import pyfbsdk as mobu
from RS.Utils import Namespace
from RS.Tools.FirstPersonController import Animation as ExportAnimation
from RS.Tools.FirstPersonController import Core as fpsCore 


class FpsCameraExporterWidget(QtGui.QWidget):
    def __init__(self,
                 characterWidget):
        self.disableLimitState = False

        self.characterWidget = characterWidget

        super(FpsCameraExporterWidget, self).__init__()

        self.setupUi()

    def _getLimitSettings(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return None

        return fpsCore.FpsCameraLimitModeSettings(currentCharacter)

    def setLimitSettings(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return

        settings = self._getLimitSettings()

        self.disableLimitState = True
        self.unlockLimitButton.setChecked(not settings.disableLimit)
        self.disableLimitState = False

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)

        self.processWidget = fpsProcessTakeWidget.FpsProcessTakeWidget(self.characterWidget,
                                                                       actionLabel='Zero Camera Degrees Of Freedom',
                                                                       batchActionLabel='Zero Camera Degrees Of Freedom On Selected Takes',
                                                                       secondaryActionLabel='Restore Camera Degrees Of Freedom',
                                                                       secondaryBatchActionLabel='Restore Camera Degrees Of Freedom On Selected Takes',
                                                                       collapseGroupLabel='Camera Degrees Of Freedom Utils',
                                                                       processDropDownLabels=['Process current Take', 
                                                                                              'Process Selected Takes'])

        self.unlockLimitButton = QtGui.QCheckBox("Export Camera Limits")

        self.openExporterButton = QtGui.QPushButton("Open exporter and set export setting")
        self.openExporterButton.setMinimumHeight(40)

        mainLayout.addWidget(self.unlockLimitButton)
        mainLayout.addWidget(self._createSeparator())
        mainLayout.addWidget(self.openExporterButton)
        mainLayout.addWidget(self.processWidget)

        self.setLayout(mainLayout)

        self.setLimitSettings()

        self.openExporterButton.pressed.connect(self._openAnimationExporter)

        self.unlockLimitButton.stateChanged.connect(self._updateCameraLimitState)

    def _updateCameraLimitState(self):
        if self.disableLimitState:
            return

        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return

        settings = fpsCore.FpsCameraLimitModeSettings(currentCharacter)

        settings.toggleState(not self.unlockLimitButton.isChecked())

    def _createSeparator(self):
        targetLine = QtGui.QFrame()

        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _openAnimationExporter(self):
        """
            Prepare export data and open Rage animation Exporter.
        """
        mobu.ShowToolByName("Rex Rage Animation Export")
        mobu.CloseToolByName("Rex Rage Animation Export")

        currentCharacter = mobu.FBApplication().CurrentCharacter
        characterNameSpace = Namespace.GetNamespace(currentCharacter)

        if characterNameSpace is None:
            characterNameSpace = ''

        mobu.ShowToolByName("Rex Rage Animation Export")

        utils = ExportAnimation.FpsCameraSettings()
        characterWasFoundInModelList = utils.prepareExportParameters(namespace=characterNameSpace)

        if characterWasFoundInModelList is False:
            errorStream = 'Please add your current character{} in the exporter modelList or use auto populate'.format(characterNameSpace) 
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)

            return

        if not utils.specFile.startswith(utils.LEGAL_SPECFILE_PREFIX):
            errorStream = 'Please choose among {} in order to have the correct export behaviour'.format(utils.LEGAL_SPECFILE_PREFIX) 
        
            QtGui.QMessageBox.information(None,
                                          'Spec File Information', 
                                          errorStream)
