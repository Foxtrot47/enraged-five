import os

import pyfbsdk as mobu
from PySide import QtGui, QtCore

from RS.Tools import UI
from RS.Tools.FirstPersonController.Widget import firstPersonControllerWidget


class FirstPersonControllerTool(UI.QtMainWindowBase):
    """
        main Tool UI
    """
    def __init__(self):
        """
            Constructor
        """
        #Data 
        self._toolName = 'FPS CH Tool'

        # Main window settings
        super(FirstPersonControllerTool, self).__init__(title=self._toolName, dockable=True)
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()

        # Create Widgets 
        banner = UI.BannerWidget(self._toolName)
        mainWidget = QtGui.QWidget()

        fpsWidget = firstPersonControllerWidget.FirstPersonControllerWidget()

        # Assign Layouts to WidgetStack
        mainLayout.addWidget(banner)
        mainLayout.addWidget(fpsWidget)

        mainWidget.setLayout(mainLayout)
        self.setCentralWidget(mainWidget)