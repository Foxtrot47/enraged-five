"""
    This UI will expose take processing options:
        - Some action are apply on the current take.
        - On other time user might wish to select a list of takes to plot some animation or
        execute other scripts.
"""

from PySide import QtGui, QtCore
import pyfbsdk as mobu

import RS
from RS.Utils.Scene import Take
from RS.Utils import Namespace
from RS.Tools.Animation.Anim2Fbx.Widgets import CollapsableWidget
from RS.Tools.FirstPersonController import Core as fpsCore 
from RS import Globals


class WidgetHolder(object):
    def __init__(self):
        self.layout = QtGui.QVBoxLayout()
        self.widget = QtGui.QWidget()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(10)


class FpsProcessTakeWidget(QtGui.QWidget):
    def __init__(self,
                 characterWidget,
                 actionLabel='Plot animation',
                 batchActionLabel='Batch Plot Target animation',
                 secondaryActionLabel=None,
                 secondaryBatchActionLabel=None,
                 collapseGroupLabel='Plot Utils',
                 processDropDownLabels=['Process current Take', 
                                        'Plot Target on Selected Takes']):
        self.characterWidget = characterWidget

        self.actionLabel = actionLabel

        self.batchActionLabel = batchActionLabel

        self.secondaryActionLabel = secondaryActionLabel

        self.secondaryBatchActionLabel = secondaryBatchActionLabel

        self.collapseGroupLabel = collapseGroupLabel

        self.processDropDownLabels = processDropDownLabels

        super(FpsProcessTakeWidget, self).__init__()

        self.collapsePlotState = False

        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        mainLayout.setContentsMargins(0, 0, 0, 0)

        mainLayout.setSpacing(5)

        self.collapseWidget = self.buildPlotControls()
        for widget in [self.collapseWidget]:
            mainLayout.addWidget(widget)

        self.setLayout(mainLayout)

        self._setTriggers()

    def _setTriggers(self):
        self.processingModeComboBox.currentIndexChanged.connect(self._swapVisibility)

        self.plotCollapseWidget._topButton.clicked.connect(self.collapsePlot)

    def collapsePlot(self):
        if self.collapsePlotState is False:
            self.plotCollapseWidget._topButton.setArrowType(QtCore.Qt.DownArrow)

            self.anchorPlotWidget.layout.addWidget(self.plotWidget)
        else:
            self.plotCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)

            self.widgetHidden.layout.addWidget(self.plotWidget)

        self.collapsePlotState = not self.collapsePlotState

        self.refreshToolBoxWindow()

    def _swapVisibility(self):
        targetIndex = self.processingModeComboBox.currentIndex() 

        if targetIndex == 0:
            self.anchorWidget.layout.addWidget(self.actionButton)
            if self.secondaryActionLabel:
                self.anchorWidget.layout.addWidget(self.secondaryActionButton)

            self.widgetHidden.layout.addWidget(self.takeGroupBox)
            self.widgetHidden.layout.addWidget(self.batchActionButton)

            if self.secondaryBatchActionLabel:
                self.widgetHidden.layout.addWidget(self.secondaryBatchActionButton)

        elif targetIndex == 1:
            self.anchorWidget.layout.addWidget(self.takeGroupBox)
            self.anchorWidget.layout.addWidget(self.batchActionButton)
            if self.secondaryBatchActionLabel:
                self.anchorWidget.layout.addWidget(self.secondaryBatchActionButton)

            self.widgetHidden.layout.addWidget(self.actionButton)
            if self.secondaryActionLabel:
                self.widgetHidden.layout.addWidget(self.secondaryActionButton)

        self.refreshToolBoxWindow()

    def _createSeparator(self):
        targetLine = QtGui.QFrame()

        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def buildPlotControls(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.setSpacing(5)

        self.actionButton = QtGui.QPushButton(self.actionLabel)
        self.actionButton.setMinimumHeight(38)

        self.buildProcessOptionControls()

        self.buildTakeControls()

        self.buildWidgetHolders()

        self.anchorWidget.layout.addWidget(self.actionButton)

        for widget in (self.actionModeGroupBox,
                       self._createSeparator(),
                       self.anchorWidget.widget):
            mainLayout.addWidget(widget)

        if self.secondaryActionLabel:
            self.secondaryActionButton = QtGui.QPushButton(self.secondaryActionLabel)
            self.secondaryActionButton.setMinimumHeight(38)
            self.anchorWidget.layout.addWidget(self.secondaryActionButton)
            self.secondaryActionButton.pressed.connect(self._restoreComponent)

        self.widgetHidden.layout.addWidget(self.takeGroupBox)
        self.widgetHidden.layout.addWidget(self.batchActionButton)

        if self.secondaryBatchActionLabel:
            self.widgetHidden.layout.addWidget(self.secondaryBatchActionButton)

        self.plotWidget = QtGui.QWidget()
        self.plotWidget.setLayout(mainLayout)

        collapseLayout = QtGui.QVBoxLayout()
        collapseLayout.setContentsMargins(0, 0, 0, 0)
        collapseLayout.setSpacing(0)

        collapseWidget = QtGui.QWidget()
        collapseWidget.setLayout(collapseLayout)

        self.plotCollapseWidget = CollapsableWidget.CollapsableWidget()
        self.plotCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)
        self.plotCollapseWidget._topButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.plotCollapseWidget._topButton.setText(self.collapseGroupLabel)

        collapseLayout.addWidget(self.plotCollapseWidget)
        collapseLayout.addWidget(self.anchorPlotWidget.widget)

        self.widgetHidden.layout.addWidget(self.plotWidget)

        self.actionButton.pressed.connect(self._processComponent)

        return collapseWidget

    def buildTakeControls(self):
        takeLayout = QtGui.QHBoxLayout()
        takeLayout.setContentsMargins(5, 3, 5, 5)
        self.takeGroupBox =  QtGui.QGroupBox('Take list')

        self.takeDataListWidget = QtGui.QListView() 
        self.takeGroupBox.setLayout(takeLayout)

        takeLayout.addWidget(self.takeDataListWidget)

        # Edit properties 
        self.takeDataListWidget.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.takeDataListWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

        self.batchActionButton = QtGui.QPushButton(self.batchActionLabel)
        self.batchActionButton.setMinimumHeight(38)

        self.takeDataModel = QtGui.QStandardItemModel()
        self.takeDataListWidget.setModel(self.takeDataModel)
        self.takeSelectionModel = self.takeDataListWidget.selectionModel()

        self.batchActionButton.pressed.connect(self._batchProcessComponent)

        if self.secondaryBatchActionLabel:
            self.secondaryBatchActionButton = QtGui.QPushButton(self.secondaryBatchActionLabel)
            self.secondaryBatchActionButton.setMinimumHeight(38)

            self.secondaryBatchActionButton.pressed.connect(self._batchRestoreComponent)

        self._refreshTakesModel()

    def _collectTakeFromName(self,
                             inputTakeName):
        inputTake = Take.GetTakeByName(inputTakeName)

        if not inputTake:
            QtGui.QMessageBox.information(self,
                                          'Missing Element', 
                                          u"Could not find {0} take in your scene".format(inputTakeName))
            return None

        return inputTake

    def terminateToolBoxWindow(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._terminateWindow()

    def hasValidRig(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return False

        rigUtils = fpsCore.FpsCameraRigValidator(currentCharacter)

        if not rigUtils.hasValidFpsCameraRig():
            print rigUtils.report.errorType, rigUtils.report.message

            QtGui.QMessageBox.warning(self, 
                                      rigUtils.report.errorType, 
                                      rigUtils.report.caption)

            self.disableShelveWidget()

            return False

        return True

    def refreshToolBoxWindow(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._refreshWindow()

    def getToolbox(self):
        toolbox = None

        currentParent = self.parent()

        for toolIndex in range(10):
            if not isinstance(currentParent,
                              RS.Tools.fpsCameraToolbox.FpsCameraToolBox):
                currentParent = currentParent.parent()

                continue

            toolbox = currentParent 

        return toolbox

    def disableShelveWidget(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._disableUI()
        currentParent._refreshWindow()

    def _batchProcessComponent(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        selectedTakes = self.getSelectedTakes()

        for takeName in selectedTakes:
            takeInput = self._collectTakeFromName(takeName)

            if takeInput is None:
                continue

            Globals.System.CurrentTake = takeInput

            fpsCore.ZeroCameraDofs(currentCharacter)

    def _processComponent(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        fpsCore.ZeroCameraDofs(currentCharacter)

    def _batchRestoreComponent(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        selectedTakes = self.getSelectedTakes()

        for takeName in selectedTakes:
            takeInput = self._collectTakeFromName(takeName)

            if takeInput is None:
                continue

            Globals.System.CurrentTake = takeInput

            fpsCore.RestoreCameraDofs(currentCharacter)

    def _restoreComponent(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        fpsCore.RestoreCameraDofs(currentCharacter)

    def buildProcessOptionControls(self):
        processingLayout = QtGui.QHBoxLayout()
        processingLayout.setContentsMargins(5, 0, 5, 5)

        self.actionModeGroupBox =  QtGui.QGroupBox('Processing Mode')
        self.processingModeComboBox = QtGui.QComboBox(self)

        self.actionModeGroupBox.setLayout(processingLayout)
        processingLayout.addWidget(self.processingModeComboBox)

        self.setProcessingOptions()

    def getSelectedTakes(self):
        return [row.data() for row in self.takeSelectionModel.selectedRows()]

    def _refreshTakesModel(self):
        self.globalSceneTakeList = Take.GetTakeList()
        self.takeDataModel.clear()
        
        #Now add Items
        for take in self.globalSceneTakeList:
            item = QtGui.QStandardItem(unicode(take.Name, "utf-8"))
            self.takeDataModel.appendRow(item)

    def _createTabHolder(self, widgetName):
        tabData = WidgetHolder()

        tabData.layout.setContentsMargins(2, 2, 2, 2) 
        tabData.layout.setSpacing(5) 

        tabData.widget.setLayout(tabData.layout)
        tabData.widget.setSizePolicy(QtGui.QSizePolicy.Minimum, 
                                     QtGui.QSizePolicy.Minimum)

        tabData.widget.setObjectName(widgetName)

        return tabData

    def buildWidgetHolders(self):
        self.anchorWidget = self._createTabHolder("{0}_Anchor".format('FirstPersonTargetControllerWidget'))    
        self.widgetHidden = self._createTabHolder("{0}_Hidden".format('FirstPersonTargetControllerWidget')) 
        self.widgetHidden.widget.hide()

        self.anchorPlotWidget = self._createTabHolder("{0}_Anchor".format('anchorPlotControllerWidget'))    

    def setProcessingOptions(self):
        for option in self.processDropDownLabels:
            self.processingModeComboBox.addItem(option)

        self.processingModeComboBox.setCurrentIndex(0)
