'''
## Path: RS\Tools\FirstPersonController\widgets\firstPersonControllerWidget.py
## Description: UI for firstPersonController Core functions
## bugTracker : 2635558 .
'''
import os
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Config, Core 

from RS import Globals
from RS.Utils.Scene import Take
from RS.Tools import UI
from RS.Utils import Namespace

from RS.Tools.FirstPersonController import Core as fpsCore 
from RS._ProjectsData import RDR
from RS.Utils.Scene import Character as characterUtils

__version__ = '0.0.16'
fpsToolTitle = 'FPS CH Tool'

#Compile .ui file to populate the main window
moduleFile = 'FPS_Layout'
uiRessourcePath = 'RS/Tools/FirstPersonController/widgets'

filePath = os.path.join(Config.Script.Path.Root,
                        uiRessourcePath,
                        moduleFile)

uiFile = '{0}.ui'.format(filePath)

compiledTarget = '{0}_compiled.py'.format(filePath)

if os.path.isfile(compiledTarget) :
    os.remove(compiledTarget)

UI.CompileUi(uiFile, compiledTarget)
from RS.Tools.FirstPersonController.widgets import FPS_Layout_compiled as FPS_LayoutCompiled 


#Clean up file once it is loaded into memory
if os.path.isfile(compiledTarget) :
    os.remove(compiledTarget)


class TakeInputsData(object):
    def __init__(self):
        self.inputTake = None
        self.targetTake = None
        self.inputCharacter = None
        self.activeComponents = []
        self.fpsCameraConstraints = []
        self.chBones = []


class ToolWidgetDialog(UI.QtMainWindowBase, FPS_LayoutCompiled.Ui_MainWindow):
    """
        main Tool UI
    """
    def __init__(self):
        """
            Constructor
        """
        # Main window settings
        UI.QtMainWindowBase.__init__(self, None, title = fpsToolTitle)

        # run ui from designer file
        self.setupUi(self)

        # add banner
        banner = UI.BannerWidget(name = 'FPS Toolbox', helpUrl ="")
        self.vBoxLayoutBanner.addWidget(banner)

        #Populates take list
        self.disabledTakeTrigger = True
        self._refreshTakesComboBox()

        #Populates character list
        self._refreshCharacterComboBox()

        # callbacks
        self.visualizeFpsPushButton.pressed.connect(self._visualizeFps)
        self.copyChKeysPushButton.pressed.connect(self._finalizeChKeys)
        self.refreshTakePushButton.pressed.connect(self._refreshTake)

        self.takesComboBox.activated[unicode].connect(self._inspectSelectedTake)
        self.characterComboBox.activated[unicode].connect(self._changeCurrentCharacter)

    def _refreshTakesComboBox(self):
        """
            Internal Method

            Refresh content of takesComboBox widget
        """
        globalSceneTakeList = Take.GetTakeList()

        self.disabledTakeTrigger = True
        #Clean Up widget content 
        self.takesComboBox.clear()
        
        #Now add Items
        for take in globalSceneTakeList:
            self.takesComboBox.addItem(take.Name)

        self.disabledTakeTrigger = False
            
        #Update Select in our dropdown list the current active Take
        activeTakeName = Globals.System.CurrentTake.Name
        takeCount = self.takesComboBox.count()

        for index in range(takeCount):
            if str(self.takesComboBox.itemText(index)) == str(activeTakeName):
                self.takesComboBox.setCurrentIndex(index)
                break

    def _refreshCharacterComboBox(self):
        """
            Internal Method

            Refresh content of characterComboBox widget
        """
        characterCount = len(Core.System.Scene.Characters)
        #Clean Up widget content 
        self.characterComboBox.clear()

        for index in range(characterCount):
            self.characterComboBox.addItem(Core.System.Scene.Characters[index].LongName)

        if mobu.FBApplication().CurrentCharacter is None:
            return

        activeCharacterName = mobu.FBApplication().CurrentCharacter.LongName

        for index in range(characterCount):
            if str(self.characterComboBox.itemText(index)) == str(activeCharacterName):
                self.characterComboBox.setCurrentIndex(index)
                break

    def _validateVisualizeFpsInputs(self):
        """
            Internal Method

            Assert if we can grab from the UI and scene the mandatory
            inputTake, inputCharacter, activeComponents ,chFpCameraConstraints arguments.
            
            activeComponents are the CH bone the animator want to work on(Hand check boxes).
            returns: (TakeInputsData())
        """
        if self.characterComboBox.count() < 1 :
            mobu.FBMessageBox('Missing Element', 
                              "Could not find a character in your scene",
                              'Ok')
            return None

        components = RDR.RdrData().GetFpsChBones()
        activeComponents = []

        for index,control in enumerate((self.rightHandCheckBox, self.leftHandCheckBox)):
            if control.isChecked() == True:
                activeComponents.append(components[index])

        inputCharacterName = self.characterComboBox.currentText() 
        inputCharacter = None

        for character in Core.System.Scene.Characters:
            if character.LongName == inputCharacterName:
                inputCharacter =  character
                break

        if inputCharacter is None :
            mobu.FBMessageBox('Missing Element', 
                              "Could not find {0} character in your scene".format(inputCharacterName),
                              'Ok')
            return None

        inputTakeName = self.takesComboBox.currentText() 
        inputTake = None

        for take in Core.System.Scene.Takes:
            if take.Name == inputTakeName:
                inputTake =  take
                break

        if inputTake is None :
            mobu.FBMessageBox('Missing Element', 
                              "Could not find {0} take in your scene".format(inputTakeName),
                              'Ok')
            return None

        chFpCameraConstraints = []
        missingCameraConstraintsCount = 0
        faultyChLink = []

        # Validate CH elements
        #(Can we find CH_*side*_Hand in the inputCharacter component list?)
        handComponents = []
        characterNameSpace = Namespace.GetNamespace(inputCharacter)

        #Fool proof check: most of the time character are reference but we can also inject
        #fpsCameraRig in character rig file
        if characterNameSpace is None:
            characterNameSpace = ''

        missingChBones = []
        for characterComponent in activeComponents:
            #Not all rig have a character extension setup the same for CH bone: 
            #We will now look at namespace:ch_bone name
            namespacePrefix = '{0}:{1}'.format(characterNameSpace, characterComponent)
            if len(characterNameSpace) == 0:
                namespacePrefix = str(characterComponent)

            controlModel = mobu.FBFindModelByLabelName(namespacePrefix)

            if controlModel is not None:
                handComponents.append(controlModel)
            else:
                missingChBones.append('{0}:{1}'.format(characterNameSpace, characterComponent))

        if len(missingChBones)>0:
            mobu.FBMessageBox('Missing Element', 
                              "Could not find {0}  in the provided {1} character".format(missingChBones, inputCharacter.LongName),
                              'Ok')
            return None

        # Validate IK wrist effector
        missingWristEffector = 0
        for control in handComponents:
            wristEffector = fpsCore.extractWristEffector(inputCharacter,control.Name)#keep control shortest name ie CH_R_Hand
            if wristEffector is None:
                missingWristEffector += 1

        if missingWristEffector > 0 :
            errorStream = "Could not find ik wrist effector for {0} in your scene\n".format(inputCharacter.FullName)
            mobu.FBMessageBox('Missing Element', 
                              errorStream,
                              'Ok')
            return None

        #Validate CH CameraConstraint
        for handComponent in handComponents:
            chConstraint = fpsCore.ExposeGyroscopeConstraintData(handComponent)
            if chConstraint is None:
                missingCameraConstraintsCount += 1
                faultyChLink.append(handComponent.LongName)
            else:
                chFpCameraConstraints.append(chConstraint)

        if missingCameraConstraintsCount > 0 :
            errorStream = ''
            for missingData in faultyChLink:
                errorStream += "Could not find a headGyroscope constraint to theFPCameraRig for {0} in your scene\n".format(missingData)

            mobu.FBMessageBox('Missing Element', 
                              errorStream,
                              'Ok')
            return None

        # Validate Source control rig
        currentControlRig = inputCharacter.GetCurrentControlSet()
        if currentControlRig is None:
            mobu.FBMessageBox('Missing Element', 
                              'Your character source use another rig, Please bake your animation on {0} control rig'.format(inputCharacterName), 
                              'Ok')
            return None

        takeInputs = TakeInputsData()

        takeInputs.inputTake = inputTake
        takeInputs.inputCharacter = inputCharacter
        takeInputs.activeComponents = activeComponents
        takeInputs.fpsCameraConstraints = chFpCameraConstraints
        takeInputs.chBones = handComponents

        return takeInputs

    def _validateFinalizeChKeysInputs(self):
        """
            Internal Method
            
            Assert if we can grab from the UI and scene the mandatory
            inputTake, inputCharacter, activeComponents.
            
            activeComponents are the CH bone the animator want to work on.
            returns: (tuple)
        """
        takeInputs = self._validateVisualizeFpsInputs()
        if takeInputs is None:
            return None

        if '_FirstPersonPreview' not in takeInputs.inputTake.Name:
            mobu.FBMessageBox('Missing Element', 
                              "Please select a FirstPersonPreview Take created by this tool",
                              'Ok')
            return None

        targetTake = None
        targetTakeName = takeInputs.inputTake.Name.replace('_FirstPersonPreview', '')
        for take in Core.System.Scene.Takes:
            if take.Name == targetTakeName:
                targetTake =  take
                break

        if targetTake is None :
            mobu.FBMessageBox('Missing Element', 
                              "Could not find {0} target Take in your scene".format(targetTakeName),
                              'Ok')
            return None

        takeInputs.targetTake = targetTake

        return takeInputs

    def _visualizeFps(self):
        """
            Internal Method

            visualize Fps CH keys on a new take layer
        """
        takeInputs = self._validateVisualizeFpsInputs()

        if takeInputs is None:
            return

        fpsTake = fpsCore.PrepareFpsTake(takeInputs.inputTake)

        Globals.System.CurrentTake = fpsTake 
        self._refreshTakesComboBox()

        fpsRigData = fpsCore.PrepareIntermediateRig(takeInputs.inputCharacter,
                                                    takeInputs.activeComponents,
                                                    takeInputs.fpsCameraConstraints,
                                                    takeInputs.chBones)

        fpsCore.BakeCHRigOnFpsTake(fpsRigData,
                                   takeInputs.inputCharacter)

    def _finalizeChKeys(self):
        """
            Internal Method

            Plot a clean CH keys from an fps take layer
        """
        takeInputs = self._validateFinalizeChKeysInputs()

        if takeInputs is None:
            return

        fpsCore.FinalizeFpsHandAnimation(takeInputs.inputTake,
                                         takeInputs.targetTake,
                                         takeInputs.chBones,
                                         takeInputs.fpsCameraConstraints)

        Globals.System.CurrentTake = takeInputs.targetTake 
        self._refreshTakesComboBox()

    def _inspectSelectedTake(self,inputTakeName):
        """
            Internal Method

            Change the current active take
        """
        if self.disabledTakeTrigger == True:
            return

        currentTakeResult = Take.SetCurrentTakeByName(inputTakeName)

    def _changeCurrentCharacter(self):
        """
            Internal Method

            sync UI currentCharacter with the FBSystem().Scene.CurrentCharacter
        """
        inputCharacterName = self.characterComboBox.currentText() 
    
        for character in Core.System.Scene.Characters:
            if character.LongName == inputCharacterName:
                mobu.FBApplication().CurrentCharacter = character
                break

    def _refreshTake(self):
        """
            Internal Method

            manually refresh takes in the UI
        """
        self._refreshTakesComboBox()
        self._refreshCharacterComboBox()
