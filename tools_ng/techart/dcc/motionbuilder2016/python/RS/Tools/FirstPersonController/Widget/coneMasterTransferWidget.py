"""
    This UI will help transfer animation from mover space to head space:
        the rotation dominance is weight property which will control the final constraint behaviour.

    when we export to game animation must be evaluated in head space.
"""

import os
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Config

from RS import Globals
from RS.Utils.Scene import Take
from RS.Utils import Namespace
from RS.Utils import ContextManagers
from RS.Tools.FirstPersonController import Core as fpsCore 


class ConeMasterTransferWidget(QtGui.QWidget):
    def __init__(self,
                 characterWidget):
        self.characterWidget = characterWidget

        super(ConeMasterTransferWidget, self).__init__()
        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        self.bakeCurrentFramePushButton = QtGui.QPushButton("Bake current frame")

        mainLayout.setContentsMargins(0, 0, 0, 0)
        self.setMinimumWidth(350)

        mainLayout.addWidget(self._buildPlotFpsWidgets())

        self.setLayout(mainLayout)

        self.bakeCurrentFramePushButton.pressed.connect(self._bakeCurrentFrame)

    def _createSeparator(self):
        targetLine = QtGui.QFrame()

        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _buildPlotFpsWidgets(self):
        plotControlsLayout = QtGui.QVBoxLayout()
        self.plotGroupBox =  QtGui.QGroupBox("Plot FPS Camera")

        buttonsControlsWidget = self._buildButtonsControls()
        plotRangeControl = self._buildPlotRangeControls()

        self.bakeCurrentFramePushButton.setMinimumHeight(32)

        plotControlsLayout.addWidget(self.bakeCurrentFramePushButton)
        plotControlsLayout.addWidget(self._createSeparator())
        plotControlsLayout.addWidget(plotRangeControl)
        plotControlsLayout.addWidget(buttonsControlsWidget)

        self.plotGroupBox.setLayout(plotControlsLayout)

        return self.plotGroupBox

    def _buildPlotRangeControls(self, groupLabel='Plotting range'):
        startRangeLayout = QtGui.QGridLayout()
        startRangeLayoutAnchor = QtGui.QHBoxLayout()

        plotRangeAnchor = QtGui.QGroupBox(groupLabel)
        self.startRangeSpinBox = QtGui.QSpinBox()
        self.endRangeSpinBox = QtGui.QSpinBox()

        startlabel = QtGui.QLabel()
        startlabel.setText("Start")

        endlabel = QtGui.QLabel()
        endlabel.setText("End")

        startRangeLayoutAnchor.setSpacing(5)
        startRangeLayoutAnchor.setContentsMargins(5, 5, 5, 5)

        self.startRangeSpinBox.setMinimumWidth(75)
        self.startRangeSpinBox.setMaximumWidth(75)

        self.startRangeSpinBox.setMinimum = (-50000)
        self.startRangeSpinBox.setMaximum = (500000)

        self.startRangeSpinBox.setRange(-50000, 50000)
        self.startRangeSpinBox.setValue(0)

        self.endRangeSpinBox.setMinimumWidth(75)
        self.endRangeSpinBox.setMaximumWidth(75)

        self.endRangeSpinBox.setMinimum = (-50000)
        self.endRangeSpinBox.setMaximum = (500000)

        self.endRangeSpinBox.setRange(-50000, 50000)
        self.endRangeSpinBox.setValue(100)

        startRangeLayoutAnchor.addWidget(startlabel)
        startRangeLayoutAnchor.addWidget(self.startRangeSpinBox)

        startRangeLayoutAnchor.addStretch()

        startRangeLayoutAnchor.addWidget(endlabel)
        startRangeLayoutAnchor.addWidget(self.endRangeSpinBox)

        plotRangeAnchor.setLayout(startRangeLayoutAnchor)

        return plotRangeAnchor

    def _buildButtonsControls(self):
        executeLayout = QtGui.QVBoxLayout()

        executeWidget = QtGui.QWidget()

        self.transferPushButton = QtGui.QPushButton("Bake Range")

        executeLayout.setContentsMargins(0, 0, 0, 0)
        self.transferPushButton.setMinimumHeight(40)

        executeLayout.addWidget(self.transferPushButton)
        executeWidget.setLayout(executeLayout)

        self.transferPushButton.pressed.connect(self._transferAnimation)

        return executeWidget

    def terminateToolBoxWindow(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._terminateWindow()

    def hasValidRig(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return False

        rigUtils = fpsCore.FpsCameraRigValidator(currentCharacter)

        if not rigUtils.hasValidFpsCameraRig():
            print rigUtils.report.errorType, rigUtils.report.message

            QtGui.QMessageBox.warning(self, 
                                      rigUtils.report.errorType, 
                                      rigUtils.report.caption)

            self.disableShelveWidget()

            return False

        return True

    def refreshToolBoxWindow(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._refreshWindow()

    def getToolbox(self):
        toolbox = None

        currentParent = self.parent()

        for toolIndex in range(10):
            if not isinstance(currentParent,
                              RS.Tools.fpsCameraToolbox.FpsCameraToolBox):
                currentParent = currentParent.parent()

                continue

            toolbox = currentParent 

        return toolbox

    def disableShelveWidget(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._disableUI()
        currentParent._refreshWindow()

    def _bakeCurrentFrame(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if not currentCharacter:
            return

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        fpsCore.BakeCurrentFrame(currentCharacter)

    def _transferAnimation(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if not currentCharacter:
            return   

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        if self.endRangeSpinBox.value() < self.startRangeSpinBox.value():
            QtGui.QMessageBox.information(self,
                                          'Incorrect Time Range', 
                                          "Please check your time range end value: last frame must be greater than first frame")
            return None

        if self.endRangeSpinBox.value() - self.startRangeSpinBox.value() < 2:
            QtGui.QMessageBox.information(self,
                                          'Incorrect Time Range', 
                                          "Please use a range of at least 3 frames")
            return None

        startTime = mobu.FBTime()
        startTime.SetFrame(self.startRangeSpinBox.value(), 
                           mobu.FBTimeMode.kFBTimeMode30Frames) 

        endTime = mobu.FBTime()
        endTime.SetFrame(self.endRangeSpinBox.value(), 
                         mobu.FBTimeMode.kFBTimeMode30Frames) 
        
        fpsCore.TransferConeMasterAnimationToMoverSpace(currentCharacter,
                                                        startTime,
                                                        endTime)