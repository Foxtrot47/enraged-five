"""
    This UI will helps animator visualize their hand in First person camera mode.

    CH_*_Hand helper will be used to store and inject offset between the camera and the wrist effectors.
"""

import os
from PySide import QtGui, QtCore

import pyfbsdk as mobu

import RS
from RS import Config

from RS import Globals
from RS.Utils.Scene import Take
from RS.Utils import Namespace

from RS.Tools.FirstPersonController import Core as fpsCore 
from RS import ProjectData
from RS.Utils.Scene import Character as characterUtils


class WidgetHolder(object):
    def __init__(self):
        self.layout = QtGui.QVBoxLayout()
        self.widget = QtGui.QWidget()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(10)


class TakeInputsData(object):
    def __init__(self):
        self.inputTake = None
        self.targetTake = None
        self.inputCharacter = None
        self.activeComponents = []
        self.fpsCameraConstraints = []
        self.chBones = []


class FirstPersonControllerWidget(QtGui.QWidget):
    def __init__(self):
        super(FirstPersonControllerWidget, self).__init__()

        self.takeDataModel = QtGui.QStandardItemModel()

        self.setupUi()

    def getCharacterFromWidget(self):
        if self.characterComboBox.count() < 1 :
            return None

        inputCharacterName = self.characterComboBox.currentText() 
        inputCharacter = None

        for character in Globals.Characters:
            if character.LongName == inputCharacterName:
                inputCharacter =  character
                break

        if inputCharacter is None :
            return None

        return inputCharacter

    def terminateToolBoxWindow(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._terminateWindow()

    def hasValidRig(self, currentCharacter=None):
        if currentCharacter is None:
            currentCharacter = self.getCharacterFromWidget()

        if currentCharacter is None:
            return False

        rigUtils = fpsCore.FpsCameraRigValidator(currentCharacter)

        if not rigUtils.hasValidFpsCameraRig():
            print rigUtils.report.errorType, rigUtils.report.message

            QtGui.QMessageBox.warning(self, 
                                      rigUtils.report.errorType, 
                                      rigUtils.report.caption)

            self.disableShelveWidget()

            return False

        return True

    def refreshToolBoxWindow(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._refreshWindow()

    def getToolbox(self):
        toolbox = None

        currentParent = self.parent()

        for toolIndex in range(10):
            if not isinstance(currentParent,
                              RS.Tools.fpsCameraToolbox.FpsCameraToolBox):
                currentParent = currentParent.parent()

                continue

            toolbox = currentParent 

        return toolbox

    def disableShelveWidget(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._disableUI()
        currentParent._refreshWindow()

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        self.toggleFpsCameraCenterCheckbox = QtGui.QCheckBox("Show camera centers")

        self.buildWidgetHolders()

        self.buildProcessOptionControls()

        self.buildTakeControls()

        self.buttonsControlsWidget = self.buildButtonsControls()
        characterControls = self.buildCharacterControls()

        mainLayout.setContentsMargins(0, 0, 0, 0)
        self.setMinimumWidth(350)

        mainLayout.addWidget(self.widgetHidden.widget)
        mainLayout.addWidget(characterControls)
        mainLayout.addWidget(self._createSeparator())
        mainLayout.addWidget(self.toggleFpsCameraCenterCheckbox)
        mainLayout.addWidget(self.InitializeChBonesButton)
        mainLayout.addWidget(self._createSeparator())
        mainLayout.addWidget(self.actionModeGroupBox)
        mainLayout.addWidget(self.anchorWidget.widget)

        self.anchorWidget.layout.addWidget(self.buttonsControlsWidget)
        self.widgetHidden.layout.addWidget(self.takeGroupBox)

        self.setLayout(mainLayout)

        self.toggleFpsCameraCenterCheckbox.stateChanged.connect(self._toggleCameraCenters)

        self.setProcessingOptions()

        self.processingModeComboBox.currentIndexChanged.connect(self._swapVisibility)

    def _swapVisibility(self):
        targetIndex = self.processingModeComboBox.currentIndex() 

        if targetIndex == 0:
            self.anchorWidget.layout.addWidget(self.buttonsControlsWidget)
            self.widgetHidden.layout.addWidget(self.takeGroupBox)
            self.widgetHidden.layout.addWidget(self.copyChKeysFromTakesPushButton)
            self.widgetHidden.layout.addWidget(self.visualizeToTakesPushButton)

        elif targetIndex == 1:
            self.widgetHidden.layout.addWidget(self.copyChKeysFromTakesPushButton)
            self.widgetHidden.layout.addWidget(self.buttonsControlsWidget)

            self.anchorWidget.layout.addWidget(self.visualizeToTakesPushButton)
            self.anchorWidget.layout.addWidget(self.takeGroupBox)

        elif targetIndex == 2:
            self.widgetHidden.layout.addWidget(self.visualizeToTakesPushButton)
            self.widgetHidden.layout.addWidget(self.buttonsControlsWidget)
            self.anchorWidget.layout.addWidget(self.copyChKeysFromTakesPushButton)
            self.anchorWidget.layout.addWidget(self.takeGroupBox)

        self.refreshToolBoxWindow()

    def buildWidgetHolders(self):
        self.anchorWidget = self._createTabHolder("{0}_Anchor".format('FirstPersonControllerWidget'))    
        self.widgetHidden = self._createTabHolder("{0}_Hidden".format('FirstPersonControllerWidget')) 
        self.widgetHidden.widget.hide()

    def _createTabHolder(self, widgetName):
        """
            Internal Method

            Placeholder widget used to defines border with provided widget list
            Args:
                widgetName (str): flag widget with the provided name.

            returns dict {QWidget,QVBoxLayout}
        """
        tabData = WidgetHolder()

        tabData.layout.setContentsMargins(2, 2, 2, 2) 
        tabData.layout.setSpacing(0) 

        tabData.widget.setLayout(tabData.layout)
        tabData.widget.setSizePolicy(QtGui.QSizePolicy.Minimum, 
                                     QtGui.QSizePolicy.Minimum)

        tabData.widget.setObjectName(widgetName)

        return tabData

    def buildTakeControls(self):
        takeLayout = QtGui.QHBoxLayout()
        takeLayout.setContentsMargins(5, 3, 5, 5)
        self.takeGroupBox =  QtGui.QGroupBox('Take list')

        self.takeDataListWidget = QtGui.QListView() 
        self.takeGroupBox.setLayout(takeLayout)

        takeLayout.addWidget(self.takeDataListWidget)

        # Edit properties 
        self.takeDataListWidget.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.takeDataListWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

        self.copyChKeysFromTakesPushButton = QtGui.QPushButton("Copy CH keys on Selected Takes")
        self.visualizeToTakesPushButton = QtGui.QPushButton("Visualize FPS Anim on new Takes")
        self.InitializeChBonesButton = QtGui.QPushButton("Initialize Ch Bones")

        self.takeDataListWidget.setModel(self.takeDataModel)
        self.takeSelectionModel = self.takeDataListWidget.selectionModel()

        self.visualizeToTakesPushButton.pressed.connect(self._batchVisualizeFps)
        self.copyChKeysFromTakesPushButton.pressed.connect(self._batchFinalizeChKeys)
        self.InitializeChBonesButton.pressed.connect(self._initializeKeys)

        self._refreshTakesModel()

    def buildProcessOptionControls(self):
        processingLayout = QtGui.QHBoxLayout()
        processingLayout.setContentsMargins(5, 0, 5, 5)

        self.actionModeGroupBox =  QtGui.QGroupBox('Processing Mode')
        self.processingModeComboBox = QtGui.QComboBox(self)

        self.actionModeGroupBox.setLayout(processingLayout)
        processingLayout.addWidget(self.processingModeComboBox)

    def setProcessingOptions(self):
        for option in ('Process inputTake', 
                       'Visualize Fps Anim on Selected Takes',
                       'Copy CH keys from Selected Takes'):
            self.processingModeComboBox.addItem(option)

        self.processingModeComboBox.setCurrentIndex(0)

    def _createSeparator(self):
        targetLine = QtGui.QFrame()

        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def buildCharacterControls(self):
        characterControlsLayout = QtGui.QVBoxLayout()
        chHandLayout = QtGui.QHBoxLayout()

        chHandWidget = QtGui.QWidget()
        self.inputGroupBox =  QtGui.QGroupBox("Input Character")

        self.leftHandCheckBox = QtGui.QCheckBox("Left Hand")
        self.rightHandCheckBox = QtGui.QCheckBox("Right Hand")

        self.refreshTakePushButton = QtGui.QPushButton("refresh")

        self.inputTakeLabel = QtGui.QLabel("input Take")

        self.takesComboBox = QtGui.QComboBox(self)
        self.takesComboBox.setModel(self.takeDataModel)
        self.characterComboBox = QtGui.QComboBox(self)

        self.leftHandCheckBox.setChecked(True)
        self.rightHandCheckBox.setChecked(True)
        self.leftHandCheckBox.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.rightHandCheckBox.setLayoutDirection(QtCore.Qt.LeftToRight)

        chHandLayout.setContentsMargins(0, 0, 0, 0)
        self.refreshTakePushButton.setMinimumWidth(160)

        chHandLayout.addWidget(self.rightHandCheckBox)
        chHandLayout.addWidget(self.leftHandCheckBox)
        chHandLayout.addWidget(self.refreshTakePushButton)

        characterControlsLayout.addWidget(self.characterComboBox)
        characterControlsLayout.addWidget(chHandWidget)
        characterControlsLayout.addWidget(self._createSeparator())
        characterControlsLayout.addWidget(self.inputTakeLabel)
        characterControlsLayout.addWidget(self.takesComboBox)

        self.inputGroupBox.setLayout(characterControlsLayout)
        chHandWidget.setLayout(chHandLayout)

        self.disabledTakeTrigger = True
        self._refreshTakesComboBox()

        self._refreshCharacterComboBox()

        self.takesComboBox.activated[unicode].connect(self._inspectSelectedTake)
        self.characterComboBox.activated[unicode].connect(self._changeCurrentCharacter)

        self.refreshTakePushButton.pressed.connect(self._refreshTake)

        return self.inputGroupBox

    def buildButtonsControls(self):
        executeLayout = QtGui.QVBoxLayout()

        executeWidget = QtGui.QGroupBox('')
        self.copyChKeysPushButton = QtGui.QPushButton("Copy CH keys back to Take")
        self.visualizeFpsPushButton = QtGui.QPushButton("Visualize FPS Anim on new Take")

        executeLayout.setContentsMargins(7, 7, 7, 7)
        self.copyChKeysPushButton.setMinimumHeight(40)
        self.visualizeFpsPushButton.setMinimumHeight(40)

        executeLayout.addWidget(self.visualizeFpsPushButton)
        executeLayout.addWidget(self._createSeparator())
        executeLayout.addWidget(self.copyChKeysPushButton)
        executeWidget.setLayout(executeLayout)

        self.visualizeFpsPushButton.pressed.connect(self._visualizeFps)
        self.copyChKeysPushButton.pressed.connect(self._finalizeChKeys)

        return executeWidget

    def _refreshTakesComboBox(self):
        if Globals.System.CurrentTake is None:
            return

        self.disabledTakeTrigger = True

        activeTakeName = unicode(Globals.System.CurrentTake.Name, "utf-8")
        takeCount = self.takesComboBox.count()

        for index in xrange(takeCount):
            takeItemName = self.takesComboBox.itemText(index)
            if self.takesComboBox.itemText(index) == activeTakeName:
                self.takesComboBox.setCurrentIndex(index)
                break

        self.disabledTakeTrigger = False

    def _refreshTakesModel(self):
        self.globalSceneTakeList = Take.GetTakeList()
        self.takeDataModel.clear()

        for take in self.globalSceneTakeList:
            item = QtGui.QStandardItem(unicode(take.Name, "utf-8"))
            self.takeDataModel.appendRow(item)

    def _refreshCharacterComboBox(self):
        """
            Refresh content of characterComboBox widget
        """
        characterCount = len(Globals.Characters)

        self.characterComboBox.clear()

        for index in xrange(characterCount):
            self.characterComboBox.addItem(Globals.Characters[index].LongName)

        if mobu.FBApplication().CurrentCharacter is None:
            return

        activeCharacterName = mobu.FBApplication().CurrentCharacter.LongName

        for index in xrange(characterCount):
            if self.characterComboBox.itemText(index) == activeCharacterName:
                self.characterComboBox.setCurrentIndex(index)
                break

    def _validateVisualizeFpsInputs(self,
                                    inputTakeName=None):
        """
            Internal Method

            Assert if we can grab from the UI and scene the mandatory
            inputTake, inputCharacter, activeComponents ,chFpCameraConstraints arguments.
            
            activeComponents are the CH bone the animator want to work on(Hand check boxes).
            returns: TakeInputsData()
        """
        if self.characterComboBox.count() < 1 :
            QtGui.QMessageBox.information(self,
                                          'Missing Element', 
                                          "Could not find a character in your scene")
            return None

        components = ProjectData.data.GetFpsChBones() 

        componentArray = (self.rightHandCheckBox, self.leftHandCheckBox)

        activeComponents = [components[componentArray.index(control)] for control in componentArray if control.isChecked() == True]

        inputCharacterName = self.characterComboBox.currentText() 
        inputCharacter = None

        for character in Globals.Characters:
            if character.LongName == inputCharacterName:
                inputCharacter =  character
                break

        if inputCharacter is None :
            QtGui.QMessageBox.information(self,
                                          'Missing Element', 
                                          "Could not find {0} character in your scene".format(inputCharacterName))
            return None

        if inputTakeName is None:
            inputTakeName = self.takesComboBox.currentText() 

        inputTake = Take.GetTakeByName(inputTakeName)

        if inputTake is None:
            QtGui.QMessageBox.information(self,
                                          'Missing Element', 
                                          u"Could not find {0} take in your scene".format(inputTakeName))
            return None

        chFpCameraConstraints = []
        missingCameraConstraintsCount = 0
        faultyChLink = []

        # Validate CH elements
        #(Can we find CH_*side*_Hand in the inputCharacter component list?)
        handComponents = []
        characterNameSpace = Namespace.GetNamespace(inputCharacter)

        #Fool proof check: most of the time character are reference but we can also inject
        #fpsCameraRig in character rig file
        if characterNameSpace is None:
            characterNameSpace = ''

        missingChBones = []
        for characterComponent in activeComponents:
            #Not all rig have a character extension setup the same for CH bone: 
            #We will now look at namespace:ch_bone name
            namespacePrefix = '{0}:{1}'.format(characterNameSpace, characterComponent)
            if len(characterNameSpace) == 0:
                namespacePrefix = '{}'.format(characterComponent)

            controlModel = mobu.FBFindModelByLabelName(namespacePrefix)

            if controlModel is not None:
                handComponents.append(controlModel)
            else:
                missingChBones.append('{0}:{1}'.format(characterNameSpace, characterComponent))

        if len(missingChBones)>0:
            QtGui.QMessageBox.information(self,
                                          'Missing Element', 
                                          "Could not find {0}  in the provided {1} character".format(missingChBones, inputCharacter.LongName))
            return None

        # Validate IK wrist effector
        missingWristEffector = 0
        for control in handComponents:
            wristEffector = fpsCore.extractWristEffector(inputCharacter,control.Name)#keep control shortest name ie CH_R_Hand
            if wristEffector is None:
                missingWristEffector += 1

        if missingWristEffector > 0 :
            errorStream = "Could not find ik wrist effector for {0} in your scene\n".format(inputCharacter.FullName)
            QtGui.QMessageBox.information(self,
                                          'Missing Element', 
                                           errorStream)
            return None

        #Validate CH CameraConstraint
        for handComponent in handComponents:
            chConstraint = fpsCore.ExposeGyroscopeConstraintData(handComponent)
            if chConstraint is None:
                missingCameraConstraintsCount += 1
                faultyChLink.append(handComponent.LongName)
            else:
                chFpCameraConstraints.append(chConstraint)

        if missingCameraConstraintsCount > 0 :
            errorStream = ''
            for missingData in faultyChLink:
                errorStream += "Could not find a headGyroscope constraint to theFPCameraRig for {0} in your scene\n".format(missingData)

            QtGui.QMessageBox.information(self,
                                          'Missing Element', 
                                          errorStream)
            return None

        # Validate Source control rig
        currentControlRig = inputCharacter.GetCurrentControlSet()
        if currentControlRig is None:
            QtGui.QMessageBox.information(self,
                                          'Missing Element', 
                                          'Your character source use another rig, Please bake your animation on {0} control rig'.format(inputCharacterName))
            return None

        takeInputs = TakeInputsData()

        takeInputs.inputTake = inputTake
        takeInputs.inputCharacter = inputCharacter
        takeInputs.activeComponents = activeComponents
        takeInputs.fpsCameraConstraints = chFpCameraConstraints
        takeInputs.chBones = handComponents

        return takeInputs

    def _validateFinalizeChKeysInputs(self,
                                      inputTakeName=None):
        """
            Internal Method
            
            Assert if we can grab from the UI and scene the mandatory
            inputTake, inputCharacter, activeComponents.
            
            activeComponents are the CH bone the animator want to work on.
            returns: (tuple)
        """
        takeInputs = self._validateVisualizeFpsInputs(inputTakeName=inputTakeName)

        if takeInputs is None:
            return None

        inputTakeName = unicode(takeInputs.inputTake.Name, 'utf-8')
        if u'_FirstPersonPreview' not in inputTakeName:
            QtGui.QMessageBox.information(self,
                                          'Missing Element',
                                          "Please select a FirstPersonPreview Take created by this tool")
            return None

        targetTakeName = inputTakeName.replace(u'_FirstPersonPreview', u'')
        targetTake = Take.GetTakeByName(targetTakeName)

        if targetTake is None:
            msg = u"Could not find {0} target Take in your scene".format(targetTakeName)
            QtGui.QMessageBox.information(self, 'Missing Element', msg)
            return None

        takeInputs.targetTake = targetTake
        
        targetTakeFrameCount = targetTake.LocalTimeSpan.GetDuration().GetFrame()
        inputTakeFrameCount = takeInputs.inputTake.LocalTimeSpan.GetDuration().GetFrame()
        
        lengthWarnMsg = None
        if inputTakeFrameCount < targetTakeFrameCount:
            frameCountDelta = targetTakeFrameCount - inputTakeFrameCount
            lengthWarnMsg = u"Preview take \"{0}\" has less frames than the original \"{1}\".\n\n" \
                            u"{2} frame(s) in \"{1}\" will not get CH data.\n\n" \
                            u"Do you want to continue?".format(inputTakeName,
                                                               targetTakeName,
                                                               frameCountDelta)
        elif inputTakeFrameCount > targetTakeFrameCount:
            frameCountDelta = inputTakeFrameCount - targetTakeFrameCount
            lengthWarnMsg = u"Preview take \"{0}\" has more frames than the original \"{1}\".\n\n" \
                            u"{2} frames(s) from \"{0}\" will be discarded.\n\n" \
                            u"Do you want to continue?".format(inputTakeName,
                                                               targetTakeName,
                                                               frameCountDelta)
        if lengthWarnMsg:
            buttonFlags = QtGui.QMessageBox.StandardButton.Yes | QtGui.QMessageBox.StandardButton.Abort
            result = QtGui.QMessageBox.question(self, "Take Length Mismatch", lengthWarnMsg,
                                                buttonFlags,
                                                QtGui.QMessageBox.StandardButton.Abort)
            if result != QtGui.QMessageBox.StandardButton.Yes:
                # User chose to cancel processing the take
                return None

        return takeInputs

    def _visualizeFps(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if not currentCharacter:
            return

        takeInputs = self._validateVisualizeFpsInputs()

        if takeInputs is None:
            return

        fpsTake = fpsCore.PrepareFpsTake(takeInputs.inputTake)

        Globals.System.CurrentTake = fpsTake
        self._refreshTakesComboBox()

        fpsRigData = fpsCore.PrepareIntermediateRig(takeInputs.inputCharacter,
                                                    takeInputs.activeComponents,
                                                    takeInputs.fpsCameraConstraints,
                                                    takeInputs.chBones)
        
        fpsCore.BakeCHRigOnFpsTake(fpsRigData,
                                   takeInputs.inputCharacter)

    def getSelectedTakes(self):
        return [row.data() for row in self.takeSelectionModel.selectedRows()]

    def _batchVisualizeFps(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if not currentCharacter:
            return

        selectedTakes = self.getSelectedTakes()

        for takeName in selectedTakes:
            takeInputs = self._validateVisualizeFpsInputs(inputTakeName=takeName)

            if takeInputs is None:
                continue

            fpsTake = fpsCore.PrepareFpsTake(takeInputs.inputTake)

            Globals.System.CurrentTake = fpsTake 

            fpsRigData = fpsCore.PrepareIntermediateRig(takeInputs.inputCharacter,
                                                        takeInputs.activeComponents,
                                                        takeInputs.fpsCameraConstraints,
                                                        takeInputs.chBones)

            fpsCore.BakeCHRigOnFpsTake(fpsRigData,
                                       takeInputs.inputCharacter)

        self._refreshTakesComboBox()

    def _initializeKeys(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if not currentCharacter:
            return

        takeInputs = self._validateVisualizeFpsInputs()

        if takeInputs is None:
            return

        fpsCore.InitializeChBones(takeInputs)

    def _finalizeChKeys(self):
        """
            Plot a clean CH keys from an fps take layer
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if not currentCharacter:
            return

        takeInputs = self._validateFinalizeChKeysInputs()

        if takeInputs is None:
            return

        layerUtils = fpsCore.FpsAnimationLayerValidator()

        if not layerUtils.validateFromTakeNames([unicode(takeInputs.inputTake.Name, 'utf-8')]):
            print layerUtils.report.errorType, layerUtils.report.message

            QtGui.QMessageBox.warning(self, 
                                      layerUtils.report.errorType, 
                                      layerUtils.report.message)

            return

        fpsCore.FinalizeFpsHandAnimation(takeInputs.inputTake,
                                         takeInputs.targetTake,
                                         takeInputs.chBones,
                                         takeInputs.fpsCameraConstraints)

        self._refreshTakesComboBox()

    def getCurrentCharacterIfValid(self):
        currentCharacter = self.getCharacterFromWidget()
        if currentCharacter is None:
            return None
        if not self.hasValidRig(currentCharacter):
            return None
        return currentCharacter

    def _batchFinalizeChKeys(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if not currentCharacter:
            return

        selectedTakes = self.getSelectedTakes()

        layerUtils = fpsCore.FpsAnimationLayerValidator()

        if not layerUtils.validateFromTakeNames(selectedTakes):
            print layerUtils.report.errorType, layerUtils.report.message

            QtGui.QMessageBox.warning(self, 
                                      layerUtils.report.errorType, 
                                      layerUtils.report.message)

            return

        for takeName in selectedTakes:
            takeInputs = self._validateFinalizeChKeysInputs(inputTakeName=takeName)

            if takeInputs is None:
                continue

            Globals.System.CurrentTake = takeInputs.inputTake 
            Globals.Scene.Evaluate()

            fpsCore.FinalizeFpsHandAnimation(takeInputs.inputTake,
                                             takeInputs.targetTake,
                                             takeInputs.chBones,
                                             takeInputs.fpsCameraConstraints)

        self._refreshTakesComboBox()

    def _inspectSelectedTake(self,inputTakeName):
        """
            Change the current active take
        """
        if self.disabledTakeTrigger == True:
            return

        currentTakeResult = Take.SetCurrentTakeByName(inputTakeName)

    def _changeCurrentCharacter(self):
        """
            Synchronize the currentCharacter in the UI with FBApplication().CurrentCharacter
        """
        inputCharacterName = self.characterComboBox.currentText()
    
        for character in Globals.Characters:
            if character.LongName == inputCharacterName:
                mobu.FBApplication().CurrentCharacter = character
                break

    def _toggleCameraCenters(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if not currentCharacter:
            return

        fpsCore.ToggleCameraCenters(currentCharacter,
                                    self.toggleFpsCameraCenterCheckbox.isChecked())

    def _refreshTake(self):
        """
            manually refresh takes in the UI
        """
        self._refreshCharacterComboBox()
        self._refreshTakesModel()
        self._refreshTakesComboBox()
