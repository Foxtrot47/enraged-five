"""
    This UI tries to expose all the option of the fps camera rig in aim constraint mode ( Target Mode).
"""

import inspect
import math
import os
from functools import partial

from PySide import QtGui, QtCore
import pyfbsdk as mobu
import RS

from RS import Core 
from RS import Globals
from PyExprSolver import RSExprSolver

from RS.Utils import Instrument, ContextManagers
from RS.Utils.Scene import Take
from RS.Utils import Namespace
from RS.Tools.FirstPersonController.Widget import fpsCharacterWidget
from RS.Tools.FirstPersonController import Core as fpsCore 
from RS.Tools.FirstPersonController.Widget import fpsFilterWidget
from RS.Tools.Animation.Anim2Fbx.Widgets import CollapsableWidget


class WidgetHolder(object):
    def __init__(self):
        self.layout = QtGui.QVBoxLayout()
        self.widget = QtGui.QWidget()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(10)


class FpsTargetWidget(QtGui.QWidget):
    """
        Expression solver will at some points crash when external script update a scene / property values.

        To counter this behaviour the property update will be registered withthe FBUndoManager.
        [done through ContextManagers.Undo context]
    """
    def __init__(self,
                 characterWidget):
        self.characterWidget = characterWidget

        self.disabledTakeTrigger = False

        self.collapseFilterState = False

        self.collapseTargetSpaceState = False

        self.collapseStabilizationState = False

        self.collapsePlotState = False

        self.dampPlotState = False

        self.rigModeState = False

        self.initializeSettings = False

        self.disableRigRefresh = False

        self.settings = None

        super(FpsTargetWidget, self).__init__() 

        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.setSpacing(5)

        self.buildWidgetHolders()

        self.selectConeMasterButton = QtGui.QPushButton('Select Cone Master')

        for widget in (self._createSeparator(),
                       self._buildToggleControls(),
                       self._createSeparator(),
                       self.selectConeMasterButton,
                       self._buildRigModeControls(),
                       self.widgetHidden.widget):
            mainLayout.addWidget(widget)

        self._swapCharacterSettings()

        self.setLayout(mainLayout)

        self.setProcessingOptions()

        self._setTriggers()

        self.initializeSettings = True

        self.characterWidget.characterComboBox.currentIndexChanged.connect(self._swapCharacterSettings)

    def hasValidRig(self, currentCharacter=None):
        """
            Every operation now run a validation test:
                If the rig state fail this test a modal dialog will be display
                to inform what is incorrect and the toll will then be closed.
        """
        if currentCharacter is None:
            currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return False

        rigUtils = fpsCore.FpsCameraRigValidator(currentCharacter)

        if not rigUtils.hasValidFpsCameraRig():
            print rigUtils.report.errorType, rigUtils.report.message

            QtGui.QMessageBox.warning(self,
                                      rigUtils.report.errorType,
                                      rigUtils.report.caption)

            self.disableShelveWidget()

            return False

        return True
    
    def getCurrentCharacterIfValid(self):
        if self.disableRigRefresh is True:
            return None
        currentCharacter = self.characterWidget.getCharacterFromWidget()
        if currentCharacter is None:
            return None
        if not self.hasValidRig(currentCharacter):
            return None
        return currentCharacter
    
    def _forceRotationDominance(self,
                                rotationDominance,
                                startTime):
        """
            We consider the rotationDominance to be a setting:
                we only want one value/key at the start of the take range
        """
        if not rotationDominance:
            return

        with ContextManagers.Undo(undoStackName='_bakeAnimation',
                                  attributeArray=[rotationDominance]):
            if not rotationDominance.IsAnimated():
                rotationDominance.SetAnimated(True)

            rotationDominanceNode = rotationDominance.GetAnimationNode()

            if all([rotationDominanceNode.KeyCount > 0,
                    rotationDominanceNode.FCurve is not None]):
                rotationDominanceNode.FCurve.EditClear()

            rotationDominance.Data = 1.0
            rotationDominanceNode.KeyCandidate(startTime)

    def _batchBakeAnimation(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        selectedTakes = self.getSelectedTakes()

        rotationDominance = None
        for takeName in selectedTakes:
            takeInput = self._collectTakeFromName(takeName)

            if takeInput is None:
                continue

            Globals.System.CurrentTake = takeInput

            startTime = mobu.FBPlayerControl().ZoomWindowStart

            endTime = mobu.FBPlayerControl().ZoomWindowStop

            self.disableSelection()

            rotationDominance = fpsCore.TransferLookAtAnimationToConeMaster(currentCharacter,
                                                                            startTime,
                                                                            endTime,
                                                                            enabledTargetRig=True)

            self._forceRotationDominance(rotationDominance,
                                         startTime)

        self.setTargetSettings()

    def _bakeAnimation(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        startTime = mobu.FBPlayerControl().ZoomWindowStart

        endTime = mobu.FBPlayerControl().ZoomWindowStop

        self.disableSelection()

        rotationDominance = None

        rotationDominance = fpsCore.TransferLookAtAnimationToConeMaster(currentCharacter,
                                                                        startTime,
                                                                        endTime)

        if not rotationDominance:
            return

        self._forceRotationDominance(rotationDominance,
                                     startTime)

        self.setTargetSettings()

    def _selectLookAtTarget(self):
        """
            Select Rig component from the UI [picker button]
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        self.disableSelection()

        fpsCore.SelectFpsConstraintComponent(currentCharacter,
                                             'Aim Target')

    def _selectConeMaster(self):
        """
            Select Rig component from the UI [picker button]
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        self.disableSelection()

        fpsCore.SelectFpsConstraintComponent(currentCharacter,
                                             'CameraPivot')

    def _selectWorldTargetHook(self):
        """
            Select Rig component from the UI [picker button]
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        self.disableSelection()

        fpsCore.SelectFpsConstraintHook(currentCharacter,
                                        'worldTargetHook')

    def _selectMoverUpHook(self):
        """
            Select Rig component from the UI [picker button]
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        self.disableSelection()

        fpsCore.SelectFpsConstraintHook(currentCharacter,
                                        'moverUpVectorHook')

    def _toggleRig(self):
        """
            The fpscamera rig expose two modes:
                - FK manipulation of the coneMaster
                - ik / aim constraint in Target mode.

            this method will enable the reuired one.
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        utils = fpsCore.FpsCameraValidator(currentCharacter)

        utils.consolidateConstraintWeights()

        fpsCore.ToggleTargetRig(currentCharacter,
                                self.enableLookAtRigWidget.isChecked())

    def _toggleWorldMoverState(self):
        """
            Some shots required the camera to inherits neither the skel head nor the mover orientations.

            An additional object can be used instead.

            This methods set  the correct option up for this.
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        useWorldMoverWidget = False
        if self.rotationDominanceWidget.currentIndex() > 0:
            useWorldMoverWidget = True

        fpsCore.ToggleWorldMoverMode(currentCharacter,
                                     useWorldMoverWidget)

    def _selectWorldMoverNode(self):
        """
            Select Rig component from the UI [picker button]
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        self.disableSelection()

        fpsCore.SelectFpsConstraintComponent(currentCharacter,
                                             'WorldMover')

    def _applyKeyFilter(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        propertyValue = None

        inputType = self.filterWidget.FILTER_KEYS[self.filterWidget.currentFilterOptionWidget.inputLabel]
        currentFilter = getattr(self.filterWidget.filterOptions, 
                                inputType)

        if self.filterWidget.currentFilterOptionWidget.valueWidget is not None:
            if self.filterWidget.currentFilterOptionWidget.buildCheckBox is False:
                propertyValue = self.filterWidget.currentFilterOptionWidget.valueWidget.value()
                if inputType != 'smooth':
                    propertyValue = float(propertyValue)
            else:
                propertyValue = self.filterWidget.currentFilterOptionWidget.valueWidget.isChecked()

        self.filterWidget.filterOptions.setValue(inputType, 
                                                 propertyValue)

        fpsCore.ApplyKeyFilterOnConeMaster(currentCharacter,
                                           currentFilter)
    
    def exceptionMessagePopup(self, exception):
        msg = "{}\n\nYou may need to update the reference to use this feature.".format(exception.message)
        QtGui.QMessageBox.warning(self, "Rig Error", msg)
    
    def _toggleStabilization(self):
        """
            The default behaviour for the camera is to follow the skel head position.

            Based on the action, this parameter can be toned downned.

            With the stabilization properties.
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return
        
        try:
            fpsCore.SetConeMasterProperty(currentCharacter,
                                          'Enable Head Stabilization',
                                          self.enableHeadStabilizationCheckBox.isChecked())
        except fpsCore.FPSRigError as ex:
            self.exceptionMessagePopup(ex)

    def _toggleStabilizeInAimSpace(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return
        try:
            fpsCore.SetConeMasterProperty(currentCharacter,
                                          'Stabilize In Aim Space',
                                          self.stabilizeInAimSpaceCheckBox.isChecked())
        except fpsCore.FPSRigError as ex:
            self.exceptionMessagePopup(ex)

    def _snapStabilizationHeight(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return
        try:
            fpsCore.SnapStabilizationOriginOffsetToHead(currentCharacter)
        except fpsCore.FPSRigError as ex:
            self.exceptionMessagePopup(ex)
    
    def _resetStabilizationHeight(self):
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return
        try:
            fpsCore.ResetStabilizationOriginOffset(currentCharacter)
        except fpsCore.FPSRigError as ex:
            self.exceptionMessagePopup(ex)

    def _buildRigModeControls(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.setSpacing(0)

        for widget in (self.rigFkModeWidget.widget,
                       self.rigTargetModeWidget.widget):
            mainLayout.addWidget(widget)

        currentWidget = QtGui.QWidget()
        currentWidget.setLayout(mainLayout)

        self.fkContainerWidget = self._buildFkControls()

        self.ikContainerWidget = self._buildTargetModeControls()

        settings = self._getTargetSettings()

        if settings is None:
            self.widgetHidden.layout.addWidget(self.ikContainerWidget)

            self.rigFkModeWidget.layout.addWidget(self.fkContainerWidget)
        else:
            self.rigModeState = bool(settings.useAimTarget)

            if settings.useAimTarget is True:
                self.rigTargetModeWidget.layout.addWidget(self.ikContainerWidget)
                self.widgetHidden.layout.addWidget(self.fkContainerWidget)
            else:
                self.rigFkModeWidget.layout.addWidget(self.fkContainerWidget)
                self.widgetHidden.layout.addWidget(self.ikContainerWidget)

        return currentWidget

    def _buildTargetModeControls(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(5, 5, 5, 5)
        mainLayout.setSpacing(5)

        self.selectLookAtButton = QtGui.QPushButton('Select Target Node')

        for widget in (self.selectLookAtButton,
                       self._buildDampeningControls(),
                       self._buildTargetControls(),
                       self._buildStabilizationControls(),
                       self._createSeparator(),
                       self.buildPlotControls(),
                       self._buildFilterOptions()):
            mainLayout.addWidget(widget)

        self.targetModeWidget = QtGui.QGroupBox('Target Mode')
        self.targetModeWidget.setLayout(mainLayout)

        return self.targetModeWidget

    def _buildFkControls(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(5, 5, 5, 5)
        mainLayout.setSpacing(5)

        self.fkHolderWidget, self.dominanceWeightSlider, self.dominanceWeightBox = self._createBlendSlider('Rotation Dominance (Head Rotation Dampening)',
                                                                                                           self._changeDominanceWeight,
                                                                                                           self._changeDominanceWeightSpinBox)

        self.useWorldMoverWidget = QtGui.QCheckBox("Use World Mover")

        self.selectWorldMoverWidget = QtGui.QPushButton("Select World Mover")

        self.rotationDominanceWidget = QtGui.QComboBox(self)
        for targetParent in ('Mover',
                             'World Mover'):
            self.rotationDominanceWidget.addItem(targetParent)

        self.rotationDominanceWidget.setCurrentIndex(0)

        for widget in (self.rotationDominanceWidget,
                       self.selectWorldMoverWidget,
                       self._createSeparator(),
                       self.fkHolderWidget):
            mainLayout.addWidget(widget)

        self.fkWidget = QtGui.QGroupBox('Fk Mode')
        self.fkWidget.setLayout(mainLayout)

        return self.fkWidget

    def _setTriggers(self):
        """
            Set up most callback here
        """
        self.selectConeMasterButton.clicked.connect(self._selectConeMaster)

        self.selectLookAtButton.clicked.connect(self._selectLookAtTarget)

        self.enableLookAtRigWidget.toggled.connect(self._toggleRig)

        self.enableLookAtRigWidget.toggled.connect(self._toggleRigMode)

        self.bakeButton.clicked.connect(self._bakeAnimation)

        self.filterWidget.keyFilterButton.pressed.connect(self._applyKeyFilter)

        self.selectWorldTargetButton.clicked.connect(self._selectWorldTargetHook)

        self.selectWorldUpButton.clicked.connect(self._selectMoverUpHook)

        self.targetSpaceWidget.activated.connect(self._switchTargetVectorWeight)

        self.processingModeComboBox.activated.connect(self._swapVisibility)

        self.characterWidget.refreshPushButton.clicked.connect(self.setTargetSettings)

        self.rotationDominanceWidget.activated.connect(self._toggleWorldMoverState)

        self.selectWorldMoverWidget.pressed.connect(self._selectWorldMoverNode)

        self.collapsableWidget._topButton.clicked.connect(self.collapseFilter)

        self.targetCollapseWidget._topButton.clicked.connect(self.collapseTarget)

        self.stabilizationCollapseWidget._topButton.clicked.connect(self.collapseStabilization)

        self.plotCollapseWidget._topButton.clicked.connect(self.collapsePlot)

        self.dampCollapseWidget._topButton.clicked.connect(self.collapseDamp)

        self.enableHeadStabilizationCheckBox.toggled.connect(self._toggleStabilization)
        
        self.stabilizeInAimSpaceCheckBox.toggled.connect(self._toggleStabilizeInAimSpace)
        
        self.snapStabilizationHeightBtn.clicked.connect(self._snapStabilizationHeight)
        
        self.resetStabilizationHeightBtn.clicked.connect(self._resetStabilizationHeight)

    def _changeDominanceWeightSpinBox(self):
        """
            Slider and spinner are interconnected:
            when one value change the associated widget will be updated

            In order to prevent infinite loop disableRigRefresh value will be activated.
            It will prevent the widget to send a new value to the rig component as well
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        sliderData = self.dominanceWeightBox.value()*100.0

        fpsCore.BlendRotationDominanceWeight(currentCharacter,
                                             100.0-sliderData)

    def _changeDominanceWeight(self):
        """
            Slider and spinner are interconnected:
            when one value change the associated widget will be updated

            In order to prevent infinite loop disableRigRefresh value will be activated.
            It will prevent the widget to send a new value to the rig component as well
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        sliderData = self.dominanceWeightSlider.value()/100.0 

        fpsCore.BlendRotationDominanceWeight(currentCharacter,
                                             100.0-sliderData)

    def _changeSpaceWeight(self):
        """
            Slider and spinner are interconnected:
            when one value change the associated widget will be updated

            In order to prevent infinite loop disableRigRefresh value will be activated.
            It will prevent the widget to send a new value to the rig component as well
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        fpsCore.ToggleTargetRig(currentCharacter,
                                True)

        fpsCore.BlendHookSpace(currentCharacter,
                               'upVectorParentConstraint',
                               float(self.upVectorWeightSlider.value()))

    def _changeSpaceWeightSpinBox(self):
        """
            Slider and spinner are interconnected:
            when one value change the associated widget will be updated

            In order to prevent infinite loop disableRigRefresh value will be activated.
            It will prevent the widget to send a new value to the rig component as well
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return

        fpsCore.ToggleTargetRig(currentCharacter,
                                True)

        fpsCore.BlendHookSpace(currentCharacter,
                               'upVectorParentConstraint',
                               float(self.upVectorWeightSlider.value()))

    def _changeHorizontalStabilization(self):
        """
            Slider and spinner are interconnected:
            when one value change the associated widget will be updated

            In order to prevent infinite loop disableRigRefresh value will be activated.
            It will prevent the widget to send a new value to the rig component as well
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return
        
        try:
            fpsCore.SetConeMasterProperty(currentCharacter,
                                          'Horizontal Stabilization',
                                          self.widthSlider.value())
        except fpsCore.FPSRigError as ex:
            self.exceptionMessagePopup(ex)

    def _changeVerticalStabilization(self):
        """
            Slider and spinner are interconnected:
            when one value change the associated widget will be updated

            In order to prevent infinite loop disableRigRefresh value will be activated.
            It will prevent the widget to send a new value to the rig component as well
        """
        currentCharacter = self.getCurrentCharacterIfValid()
        if currentCharacter is None:
            return
        
        try:
            fpsCore.SetConeMasterProperty(currentCharacter,
                                          'Vertical Stabilization',
                                          self.heigtSlider.value())
        except fpsCore.FPSRigError as ex:
            self.exceptionMessagePopup(ex)

    def _synchronizeSliderToSpinBox(self,
                                    slider,
                                    spinbox,
                                    multiplier=10000.0,
                                    *args):
        """
            Slider and spinner are interconnected:
            when one value change the associated widget will be updated

            In order to prevent infinite loop disableRigRefresh value will be activated.
            It will prevent the widget to send a new value to the rig component as well
        """
        targetValue = float(slider.value()/multiplier)

        self.disableRigRefresh = True

        spinbox.setValue(targetValue)

        self.disableRigRefresh = False

    def _synchronizeSpinBoxToSlider(self,
                                    spinbox,
                                    slider,
                                    multiplier=10000.0,
                                    *args):
        """
            Slider and spinner are interconnected:
            when one value change the associated widget will be updated

            In order to prevent infinite loop disableRigRefresh value will be activated.
            It will prevent the widget to send a new value to the rig component as well
        """
        targetValue = int(spinbox.value()*multiplier)

        slider.setValue(targetValue)

    def _buildDampeningControls(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.setSpacing(5)

        self.selectWorldUpButton = QtGui.QPushButton('Select Mover Up Vector Space')

        self.spaceWidget, self.upVectorWeightSlider, self.upVectorWeightBox = self._createBlendSlider('Dampening strength',
                                                                                                       self._changeSpaceWeight,
                                                                                                       self._changeSpaceWeightSpinBox,
                                                                                                       multiplier=1.0)
        self.upVectorWeightSlider.setRange(0.0, 100.0)
        self.upVectorWeightBox.setRange(0.0, 100.0)

        self.upVectorWeightSlider.setSingleStep(1.0)
        self.upVectorWeightBox.setSingleStep(1.0)

        for widget in (self.spaceWidget,
                       self.selectWorldUpButton):
            mainLayout.addWidget(widget)

        self.dampWidget = QtGui.QWidget()
        self.dampWidget.setLayout(mainLayout)

        self.widgetHidden.layout.addWidget(self.dampWidget)

        collapseLayout = QtGui.QVBoxLayout()
        collapseLayout.setContentsMargins(0, 0, 0, 0)
        collapseLayout.setSpacing(0)

        collapseWidget = QtGui.QWidget()
        collapseWidget.setLayout(collapseLayout)

        self.dampCollapseWidget = CollapsableWidget.CollapsableWidget()
        self.dampCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)
        self.dampCollapseWidget._topButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.dampCollapseWidget._topButton.setText('Head Rotation Dampening (Head space vs Mover space)')

        collapseLayout.addWidget(self.dampCollapseWidget)
        collapseLayout.addWidget(self.dampPlotWidget.widget)

        return collapseWidget

    def buildPlotControls(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.setSpacing(5)

        self.bakeButton = QtGui.QPushButton('Plot animation')
        self.bakeButton.setMinimumHeight(38)

        self.buildProcessOptionControls()

        self.buildTakeControls()

        for widget in (self.actionModeGroupBox,
                       self._createSeparator(),
                       self.anchorWidget.widget):
            mainLayout.addWidget(widget)

        self.anchorWidget.layout.addWidget(self.bakeButton)

        self.widgetHidden.layout.addWidget(self.takeGroupBox)
        self.widgetHidden.layout.addWidget(self.batchBakeButton)

        self.plotWidget = QtGui.QWidget()
        self.plotWidget.setLayout(mainLayout)

        collapseLayout = QtGui.QVBoxLayout()
        collapseLayout.setContentsMargins(0, 0, 0, 0)
        collapseLayout.setSpacing(0)

        collapseWidget = QtGui.QWidget()
        collapseWidget.setLayout(collapseLayout)

        self.plotCollapseWidget = CollapsableWidget.CollapsableWidget()
        self.plotCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)
        self.plotCollapseWidget._topButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.plotCollapseWidget._topButton.setText('Plot Utils')

        collapseLayout.addWidget(self.plotCollapseWidget)
        collapseLayout.addWidget(self.anchorPlotWidget.widget)

        self.widgetHidden.layout.addWidget(self.plotWidget)

        return collapseWidget

    def _buildFilterOptions(self):
        collapseLayout = QtGui.QVBoxLayout()
        collapseLayout.setContentsMargins(0, 0, 0, 0)
        collapseLayout.setSpacing(0)

        collapseWidget = QtGui.QWidget()
        collapseWidget.setLayout(collapseLayout)

        self.collapsableWidget = CollapsableWidget.CollapsableWidget()
        self.collapsableWidget._topButton.setArrowType(QtCore.Qt.RightArrow)
        self.collapsableWidget._topButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.collapsableWidget._topButton.setText('Animation Filter')

        self.filterWidget = fpsFilterWidget.FpsFilterWidget()
        self.widgetHidden.layout.addWidget(self.filterWidget)

        collapseLayout.addWidget(self.collapsableWidget)
        collapseLayout.addWidget(self.anchorFilterWidget.widget)

        return collapseWidget

    def _getTargetSettings(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        # currentCharacter can be None in order to initialise defaults
        return fpsCore.FpsCameraTargetModeSettings(currentCharacter)

    def _buildCharacterTargetSettings(self):
        """
            Rebuild settings object for the new character.
        """
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            # No character - initialise default settings to reset GUI
            self.settings = self._getTargetSettings()
            return

        utils = fpsCore.FpsCameraValidator(currentCharacter)

        utils.consolidateConstraintWeights()

        self.settings = self._getTargetSettings()

    def _swapCharacterSettings(self):
        """
            As the toolbox used more callback we need an optimization pass to update the UI with
            the latest rig components values.

            changing a character or a reference will collect the intitial settings of the character.

            time slider events, and UIIDLE will only need to package minimal property values to update the UI.
        """
        self._buildCharacterTargetSettings()

        self.setTargetSettings()

    def setTargetSettings(self):
        """
            unpack rig setting from the datastructure and assign it to the correct widget.

            Some value will be remapped to cover animators requests.
        """
        if self.settings is None:
            return

        self.settings.refresh()

        self.disableRigRefresh = True

        self.enableLookAtRigWidget.setChecked(self.settings.useAimTarget)

        self.targetSpaceWidget.setCurrentIndex(int((100.0-self.settings.targetMoverSpaceWeight)/100.0))

        self.rotationDominanceWidget.setCurrentIndex(int(self.settings.useWorldMover))

        weightData = 100.0-self.settings.upVectorHeadSpaceWeight

        self.upVectorWeightSlider.setValue(weightData)

        self.upVectorWeightBox.setValue(weightData)

        remapWeight = 1.0 - ((1.0 + self.settings.rotationDominanceWeight)*0.5)

        self.dominanceWeightSlider.setValue(remapWeight*10000)

        self.dominanceWeightBox.setValue(remapWeight)

        self.heigtSlider.setValue(self.settings.verticalStabilization)
        self.heigtBox.setValue(self.settings.verticalStabilization)

        self.widthSlider.setValue(self.settings.horizontalStabilization)
        self.widthBox.setValue(self.settings.horizontalStabilization)

        self.enableHeadStabilizationCheckBox.setChecked(self.settings.enableHeadStabilization)
        self.stabilizeInAimSpaceCheckBox.setChecked(self.settings.stabilizeInAimSpace)

        self.disableRigRefresh = False

    def _createBlendSlider(self,
                           labelName,
                           inputCommand,
                           spinBoxCommand,
                           multiplier=10000.0):
        """
            not float slider in QT...

            will use a bigger int range instead.

            in order to stabilize the tool.

            the rig component will be updated when the sliders are released,
            and when enter key is pressed inside the spinner widget.
        """
        mainWidget = QtGui.QWidget()

        mainLayout = QtGui.QHBoxLayout()
        mainLayout.setContentsMargins(8, 8, 20, 8)
        mainLayout.setSpacing(20)

        mainWidget.setLayout(mainLayout)

        label = QtGui.QLabel(labelName)
        slider = QtGui.QSlider(QtCore.Qt.Horizontal)

        slider.setRange(0, 10004)
        slider.setValue(0)

        spinBox = QtGui.QDoubleSpinBox()
        spinBox.setRange(0.0, 1.0)
        spinBox.setValue(0)
        spinBox.setDecimals(2)

        mainLayout.addWidget(label)
        mainLayout.addWidget(spinBox)
        mainLayout.addWidget(slider)

        slider.sliderReleased.connect(inputCommand)

        slider.sliderMoved.connect(partial(self._synchronizeSliderToSpinBox,
                                           slider,
                                           spinBox,
                                           multiplier))

        spinBox.valueChanged.connect(partial(self._synchronizeSpinBoxToSlider,
                                             spinBox,
                                             slider,
                                             multiplier))

        spinBox.editingFinished.connect(spinBoxCommand)

        return mainWidget, slider, spinBox

    def _collectTakeFromName(self,
                             inputTakeName):
        inputTake = None

        compatibleTakes = [take
                           for take in Globals.Takes
                           if unicode(take.Name, "utf-8") == inputTakeName]

        if not compatibleTakes:
            QtGui.QMessageBox.information(self,
                                          'Missing Element', 
                                          u"Could not find {0} take in your scene".format(inputTakeName))
            return None

        return compatibleTakes[0]

    def buildTakeControls(self):
        takeLayout = QtGui.QHBoxLayout()
        takeLayout.setContentsMargins(5, 3, 5, 5)
        self.takeGroupBox = QtGui.QGroupBox('Take list')

        self.takeDataListWidget = QtGui.QListView() 
        self.takeGroupBox.setLayout(takeLayout)

        takeLayout.addWidget(self.takeDataListWidget)

        # Edit properties 
        self.takeDataListWidget.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.takeDataListWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

        self.batchBakeButton = QtGui.QPushButton('Batch Plot Target animation')
        self.batchBakeButton.setMinimumHeight(38)

        self.takeDataModel = QtGui.QStandardItemModel()
        self.takeDataListWidget.setModel(self.takeDataModel)
        self.takeSelectionModel = self.takeDataListWidget.selectionModel()

        self.batchBakeButton.pressed.connect(self._batchBakeAnimation)

        self._refreshTakesModel()

    def getSelectedTakes(self):
        return [row.data() for row in self.takeSelectionModel.selectedRows()]

    def _refreshTakesModel(self):
        self.globalSceneTakeList = Take.GetTakeList()
        self.takeDataModel.clear()
        
        #Now add Items
        for take in self.globalSceneTakeList:
            item = QtGui.QStandardItem(unicode(take.Name, "utf-8"))
            self.takeDataModel.appendRow(item)

    def _createTabHolder(self, widgetName):
        """
            Internal Method

            Placeholder widget used to defines border with provided widget list
            Args:
                widgetName (str): flag widget with the provided name.

            returns dict {QWidget,QVBoxLayout}
        """
        tabData = WidgetHolder()

        tabData.layout.setContentsMargins(2, 2, 2, 2) 
        tabData.layout.setSpacing(0) 

        tabData.widget.setLayout(tabData.layout)
        tabData.widget.setSizePolicy(QtGui.QSizePolicy.Minimum, 
                                     QtGui.QSizePolicy.Minimum)

        tabData.widget.setObjectName(widgetName)

        return tabData

    def buildWidgetHolders(self):
        self.anchorWidget = self._createTabHolder("{0}_Anchor".format('FirstPersonTargetControllerWidget'))    
        self.widgetHidden = self._createTabHolder("{0}_Hidden".format('FirstPersonTargetControllerWidget')) 
        self.widgetHidden.widget.hide()

        self.anchorFilterWidget = self._createTabHolder("{0}_Anchor".format('FirstPersonFilterControllerWidget'))    
        self.anchorTargetSpaceWidget = self._createTabHolder("{0}_Anchor".format('anchorTargetSpaceControllerWidget'))
        self.anchorStabilizationSpaceWidget = self._createTabHolder("{0}_Anchor".format('anchorStabilizationControllerWidget'))  
        self.anchorPlotWidget = self._createTabHolder("{0}_Anchor".format('anchorPlotControllerWidget'))    
        self.dampPlotWidget = self._createTabHolder("{0}_Anchor".format('dampPlotControllerWidget'))

        self.rigFkModeWidget = self._createTabHolder("{0}_Anchor".format('rigFkModeControllerWidget'))
        self.rigTargetModeWidget = self._createTabHolder("{0}_Anchor".format('rigTargetModeControllerWidget'))

    def setProcessingOptions(self):
        for option in ('Process current Take', 
                       'Plot Target on Selected Takes'):
            self.processingModeComboBox.addItem(option)

        self.processingModeComboBox.setCurrentIndex(0)

    def _swapVisibility(self):
        targetIndex = self.processingModeComboBox.currentIndex() 

        if targetIndex == 0:
            self.anchorWidget.layout.addWidget(self.bakeButton)
            self.widgetHidden.layout.addWidget(self.takeGroupBox)
            self.widgetHidden.layout.addWidget(self.batchBakeButton)

        elif targetIndex == 1:
            self.anchorWidget.layout.addWidget(self.takeGroupBox)
            self.anchorWidget.layout.addWidget(self.batchBakeButton)
            self.widgetHidden.layout.addWidget(self.bakeButton)

        self.refreshToolBoxWindow()

    def collapseFilter(self):
        if self.collapseFilterState is False:
            self.collapsableWidget._topButton.setArrowType(QtCore.Qt.DownArrow)

            self.anchorFilterWidget.layout.addWidget(self.filterWidget)
        else:
            self.collapsableWidget._topButton.setArrowType(QtCore.Qt.RightArrow)

            self.widgetHidden.layout.addWidget(self.filterWidget)

        self.collapseFilterState = not self.collapseFilterState

        self.refreshToolBoxWindow()

    def collapseTarget(self):
        if self.collapseTargetSpaceState is False:
            self.targetCollapseWidget._topButton.setArrowType(QtCore.Qt.DownArrow)

            self.anchorTargetSpaceWidget.layout.addWidget(self.targetContainerWidget)
        else:
            self.targetCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)

            self.widgetHidden.layout.addWidget(self.targetContainerWidget)

        self.collapseTargetSpaceState = not self.collapseTargetSpaceState

        self.refreshToolBoxWindow()

    def collapseStabilization(self):
        if self.collapseStabilizationState is False:
            self.stabilizationCollapseWidget._topButton.setArrowType(QtCore.Qt.DownArrow)

            self.anchorStabilizationSpaceWidget.layout.addWidget(self.stabilizationContainerWidget)
        else:
            self.stabilizationCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)

            self.widgetHidden.layout.addWidget(self.stabilizationContainerWidget)

        self.collapseStabilizationState = not self.collapseStabilizationState

        self.refreshToolBoxWindow()

    def collapsePlot(self):
        if self.collapsePlotState is False:
            self.plotCollapseWidget._topButton.setArrowType(QtCore.Qt.DownArrow)

            self.anchorPlotWidget.layout.addWidget(self.plotWidget)
        else:
            self.plotCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)

            self.widgetHidden.layout.addWidget(self.plotWidget)

        self.collapsePlotState = not self.collapsePlotState

        self.refreshToolBoxWindow()

    def collapseDamp(self):
        if self.dampPlotState is False:
            self.dampCollapseWidget._topButton.setArrowType(QtCore.Qt.DownArrow)

            self.dampPlotWidget.layout.addWidget(self.dampWidget)
        else:
            self.dampCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)

            self.widgetHidden.layout.addWidget(self.dampWidget)

        self.dampPlotState = not self.dampPlotState

        self.refreshToolBoxWindow()

    def refreshToolBoxWindow(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._refreshWindow()

    def getToolbox(self):
        toolbox = None

        currentParent = self.parent()

        for toolIndex in range(10):
            if not isinstance(currentParent,
                              RS.Tools.fpsCameraToolbox.FpsCameraToolBox):
                currentParent = currentParent.parent()

                continue

            toolbox = currentParent 

        return toolbox

    def disableShelveWidget(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._disableUI()
        currentParent._refreshWindow()

    def _switchTargetVectorWeight(self):
        if self.disableRigRefresh is True:
            return

        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return

        fpsCore.ToggleTargetRig(currentCharacter,
                                True)

        fpsCore.SwitchHookSpace(currentCharacter,
                                'targetParentConstraint',
                                int(self.targetSpaceWidget.currentIndex()))

    def _createSeparator(self):
        """
        Internal Method

        add line separator between widget
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _buildToggleControls(self):
        self.enableLookAtRigWidget = QtGui.QCheckBox('Enable IK target rig')
        self.enableLookAtRigWidget.setMaximumWidth(122)

        return self.enableLookAtRigWidget

    def _buildTargetControls(self):
        collapseLayout = QtGui.QVBoxLayout()
        collapseLayout.setContentsMargins(0, 0, 0, 0)
        collapseLayout.setSpacing(0)

        collapseWidget = QtGui.QWidget()
        collapseWidget.setLayout(collapseLayout)

        groupLayout = QtGui.QVBoxLayout()
        groupLayout.setContentsMargins(5, 5, 5, 5)

        self.targetCollapseWidget = CollapsableWidget.CollapsableWidget()
        self.targetCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)
        self.targetCollapseWidget._topButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.targetCollapseWidget._topButton.setText('Target Space')

        self.selectWorldTargetButton = QtGui.QPushButton('Select World Target Space')

        self.targetSpaceWidget = QtGui.QComboBox(self)
        for targetParent in ('World space',
                             'Mover space'):
            self.targetSpaceWidget.addItem(targetParent)

        self.targetSpaceWidget.setCurrentIndex(0)

        groupLayout.addWidget(self.targetSpaceWidget)
        groupLayout.addWidget(self.selectWorldTargetButton)

        self.targetContainerWidget = QtGui.QGroupBox("")
        self.targetContainerWidget.setLayout(groupLayout)

        self.widgetHidden.layout.addWidget(self.targetContainerWidget)

        collapseLayout.addWidget(self.targetCollapseWidget)
        collapseLayout.addWidget(self.anchorTargetSpaceWidget.widget)

        return collapseWidget

    def _buildStabilizationControls(self):
        collapseLayout = QtGui.QVBoxLayout()
        collapseLayout.setContentsMargins(0, 0, 0, 0)
        collapseLayout.setSpacing(0)

        collapseWidget = QtGui.QWidget()
        collapseWidget.setLayout(collapseLayout)
        
        checkboxesLayout = QtGui.QHBoxLayout()

        groupLayout = QtGui.QVBoxLayout()
        groupLayout.setContentsMargins(5, 5, 5, 5)

        self.stabilizationCollapseWidget = CollapsableWidget.CollapsableWidget()
        self.stabilizationCollapseWidget._topButton.setArrowType(QtCore.Qt.RightArrow)
        self.stabilizationCollapseWidget._topButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.stabilizationCollapseWidget._topButton.setText('Stabilization')

        self.enableHeadStabilizationCheckBox = QtGui.QCheckBox('Enable Head Stabilization')
        self.stabilizeInAimSpaceCheckBox = QtGui.QCheckBox('Stabilize In Aim Space')

        self.widthStabilizationHolderWidget, self.widthSlider, self.widthBox = self._createBlendSlider('Horizontal Stabilization',
                                                                                                       self._changeHorizontalStabilization,
                                                                                                       self._changeHorizontalStabilization,
                                                                                                       multiplier=1.0)

        self.heightStabilizationHolderWidget, self.heigtSlider, self.heigtBox = self._createBlendSlider('Vertical Stabilization',
                                                                                                         self._changeVerticalStabilization,
                                                                                                         self._changeVerticalStabilization,
                                                                                                         multiplier=1.0)

        self.widthSlider.setRange(0.0, 100.0)
        self.widthBox.setRange(0.0, 100.0)
        self.widthBox.setSingleStep(1.0)

        self.heigtSlider.setRange(0.0, 100.0)
        self.heigtBox.setRange(0.0, 100.0)
        self.heigtBox.setSingleStep(1.0)
        
        self.stabilizationHeightGroupBox = QtGui.QGroupBox("Vertical Stabilization Height")
        self.snapStabilizationHeightBtn = QtGui.QPushButton("Snap To Current Head Height")
        self.snapStabilizationHeightBtn.setToolTip("Snaps the vertical stabilization offset to match the character's current head height.")
        self.resetStabilizationHeightBtn = QtGui.QPushButton("Reset Vertical Stabilization Height")
        self.resetStabilizationHeightBtn.setToolTip("Resets the vertical stabilization offset to its default height.")
        heightOffsetButtonsLayout = QtGui.QHBoxLayout()
        heightOffsetButtonsLayout.addWidget(self.snapStabilizationHeightBtn)
        heightOffsetButtonsLayout.addWidget(self.resetStabilizationHeightBtn)
        self.stabilizationHeightGroupBox.setLayout(heightOffsetButtonsLayout)
        
        checkboxesLayout.addWidget(self.enableHeadStabilizationCheckBox)
        checkboxesLayout.addWidget(self.stabilizeInAimSpaceCheckBox)
        groupLayout.addLayout(checkboxesLayout)
        groupLayout.addWidget(self.widthStabilizationHolderWidget)
        groupLayout.addWidget(self.heightStabilizationHolderWidget)
        groupLayout.addWidget(self.stabilizationHeightGroupBox)

        self.stabilizationContainerWidget = QtGui.QGroupBox("")
        self.stabilizationContainerWidget.setLayout(groupLayout)

        self.widgetHidden.layout.addWidget(self.stabilizationContainerWidget)

        collapseLayout.addWidget(self.stabilizationCollapseWidget)
        collapseLayout.addWidget(self.anchorStabilizationSpaceWidget.widget)

        return collapseWidget

    def buildProcessOptionControls(self):
        processingLayout = QtGui.QHBoxLayout()
        processingLayout.setContentsMargins(5, 0, 5, 5)

        self.actionModeGroupBox =  QtGui.QGroupBox('Processing Mode')
        self.processingModeComboBox = QtGui.QComboBox(self)

        self.actionModeGroupBox.setLayout(processingLayout)
        processingLayout.addWidget(self.processingModeComboBox)

    def disableSelection(self):
        currentSelection = mobu.FBModelList()
        mobu.FBGetSelectedModels(currentSelection)

        with ContextManagers.Undo(undoStackName='disableSelection',
                                  modelList=list(currentSelection)):
            for node in list(currentSelection):
                node.Selected = False

    def _toggleRigMode(self):
        """
            When the associated checkobx state changed we will swapcase

            UI parts here.
        """
        if self.rigModeState is False:
            self.rigTargetModeWidget.layout.addWidget(self.ikContainerWidget)
            self.widgetHidden.layout.addWidget(self.fkContainerWidget)

        else:
            self.rigFkModeWidget.layout.addWidget(self.fkContainerWidget)
            self.widgetHidden.layout.addWidget(self.ikContainerWidget)

        self.rigModeState = not self.rigModeState

        self.refreshToolBoxWindow()
