"""
    Some Utilities function will be exposed here for fpsCamera toolbox.
"""

import inspect
import os
from functools import partial

from PySide import QtGui, QtCore
import pyfbsdk as mobu
import RS

from RS import Core 
from RS import Config 
from RS import Globals
from RS.Utils import ContextManagers
from RS.Utils.Scene import Take
from RS.Utils import Namespace
from RS.Tools.FirstPersonController.Widget import fpsCharacterWidget
from RS.Tools.FirstPersonController import Core as fpsCore 
from RS.Core.Character import FPSCamera_CharacterRig


class FpsSettingsWidget(QtGui.QWidget):
    def __init__(self,
                 characterWidget):
        self.characterWidget = characterWidget

        super(FpsSettingsWidget, self).__init__() 

        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.setSpacing(5)

        self.toggleKeyableState = QtGui.QPushButton("Lock/Unlock Cone Master Rotation")
        self.restoreResolutionWidget = QtGui.QPushButton("Restore Camera resolution")
        self.setFovValue = QtGui.QPushButton("Restore DOF_FOV default value")
        self.toggleSceneGroupWidget = QtGui.QPushButton("Toggle FpsCamera Groups")

        for widget in (self.toggleKeyableState,
                       self.setFovValue,
                       self.restoreResolutionWidget,
                       self.toggleSceneGroupWidget):
            mainLayout.addWidget(widget)

        self.setLayout(mainLayout)

        self.toggleKeyableState.pressed.connect(self._toggleRotationLockState)
        self.setFovValue.pressed.connect(self._resetFovDefaultValue)
        self.restoreResolutionWidget.clicked.connect(self._resetResolution)
        self.toggleSceneGroupWidget.clicked.connect(self._toggleSceneGroup)

    def hasValidRig(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if currentCharacter is None:
            return False

        rigUtils = fpsCore.FpsCameraRigValidator(currentCharacter)

        if not rigUtils.hasValidFpsCameraRig():
            print rigUtils.report.errorType, rigUtils.report.message

            QtGui.QMessageBox.warning(self, 
                                      rigUtils.report.errorType, 
                                      rigUtils.report.caption)

            self.disableShelveWidget()

            return False

        return True

    def terminateToolBoxWindow(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._terminateWindow()

    def refreshToolBoxWindow(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._refreshWindow()

    def getToolbox(self):
        toolbox = None

        currentParent = self.parent()

        for toolIndex in range(10):
            if not isinstance(currentParent,
                              RS.Tools.fpsCameraToolbox.FpsCameraToolBox):
                currentParent = currentParent.parent()

                continue

            toolbox = currentParent 

        return toolbox

    def disableShelveWidget(self):
        currentParent = self.getToolbox()

        if currentParent is None:
            return

        currentParent._disableUI()
        currentParent._refreshWindow()

    def _createSeparator(self):
        targetLine = QtGui.QFrame()

        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _toggleRotationLockState(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if not currentCharacter:
            return   

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        fpsCore.ToggleRotationLockState(currentCharacter)

    def _resetFovDefaultValue(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if not currentCharacter:
            return   

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        fpsCore.RestoreFovDefaultValue(currentCharacter)

    def _toggleSceneGroup(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if not currentCharacter:
            return   

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        fpsCore.ToggleFpsSceneGroup(currentCharacter)

    def _resetResolution(self):
        currentCharacter = self.characterWidget.getCharacterFromWidget()

        if not currentCharacter:
            return   

        if not self.hasValidRig():
            self.terminateToolBoxWindow()

            return

        fpsCameraConstraintData = fpsCore.ExtractFpsCameraConstraint(currentCharacter)

        if fpsCameraConstraintData is None:
            return

        gyroscope = fpsCameraConstraintData.constraint

        cameraUtils = FPSCamera_CharacterRig.RigBuilder()
        cameraUtils.setResolution(gyroscope=gyroscope)
