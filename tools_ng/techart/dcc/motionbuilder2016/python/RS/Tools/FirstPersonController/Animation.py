"""
    This module helps with autopulate function for the animation exporter.

    It will also act as a bridge with the anim2Fbx and helps import camera track
    and do the correct space conversion for the fpscamera rig.
"""

import os


import pyfbsdk as mobu
import RexMBRagePython
from ctypes import cdll

from RS.Tools.FirstPersonController import Core as fpsCore
from RS.Core.Animation.DataManager import AnimTrack as sourceAnimTrack
from RS import Config, ProjectData
from RS.Core.Animation.Anim2Fbx import Apply as animApply


import RexMBAnim


class ExportRigSettings(object):
    SKEL_ROOT_PREFIX = 'SKEL_ROOT'

    ADDITIONAL_ATTRIBUTES = []

    def __init__(self,
                 overrideAttributes=[]):
        self.namespace = None

        self.specFile = ''

        self.skelRoot = None

        self.loadExportModule = False

        self.overrideAttributes = overrideAttributes

    def collectRexMBModel(self):
        rageModels = RexMBRagePython.GetModels()

        if self.namespace is None:
            for model in rageModels:
                if self.SKEL_ROOT_PREFIX == str(model.GetRootName()):
                    self.skelRoot = model.GetRootName()
                    self.specFile = model.GetControlFile()
                    break

        else:
            for model in rageModels:
                if str(model.GetRootName()).startswith(self.namespace) and \
                    str(model.GetRootName()).endswith(self.SKEL_ROOT_PREFIX):
                    self.skelRoot = model.GetRootName()
                    self.specFile = model.GetControlFile()
                    break

    def hasValidSkelRoot(self):
        self.skelRoot = None

        self.collectRexMBModel()

        if self.skelRoot is None:
            return False

        return True

    def prepareHeelHeight(self):
        if self.namespace is None:
            heelHeight = 'HeelHeight'
        else:
            heelHeight = '{}:HeelHeight'.format(self.namespace)

        if not cdll.rexMBRage.IsAnimationExporterInitialized_Py():
            if self.loadExportModule is True:
                from RS.Core import Export as animationExporter
                
                animationExporter.RexExport.InitializeAnimation()
            else:
                mobu.ShowToolByName("Rex Rage Animation Export")
                mobu.CloseToolByName("Rex Rage Animation Export")

        cdll.rexMBRage.AddAdditionalModelEntry_Py(self.skelRoot,
                                                  heelHeight,
                                                  'genericControl',
                                                  'HeelHeight')

    def getAdditionalAttributes(self):
        return self.ADDITIONAL_ATTRIBUTES

    def prepareAdditionalAttributes(self):
        for attribute in self.getAdditionalAttributes():
            if self.namespace is None:
                cdll.rexMBRage.AddAdditionalModelEntry_Py(self.skelRoot,
                                                          attribute,
                                                          'genericControl',
                                                          attribute)
            else:
                cdll.rexMBRage.AddAdditionalModelEntry_Py(self.skelRoot,
                                                          '{0}:{1}'.format(self.namespace,
                                                                           attribute),
                                                          'genericControl',
                                                          attribute)

    def prepareExportParameters(self,
                                namespace=''):
        if namespace.strip():
            self.namespace = namespace
        else:
            self.namespace = None

        if not self.hasValidSkelRoot():
            return False

        self.prepareHeelHeight()

        self.prepareAdditionalAttributes()

        return True


class WeaponStrapSettings(ExportRigSettings):
    ADDITIONAL_ATTRIBUTES = ('StrapClothPinning',
                             'ConstraintDistribution',
                             'ConstraintDistributionBack')

    def __init__(self):
        super(WeaponStrapSettings, self).__init__()


#bug [4819284]
class ObjectSpacePhSliderSettings(ExportRigSettings):
    ADDITIONAL_ATTRIBUTES = ('LObjectSpacePHSlider',
                             'RObjectSpacePHSlider')

    def __init__(self):
        super(ObjectSpacePhSliderSettings, self).__init__()


class FpsCameraSettings(ExportRigSettings):
    LEGAL_SPECFILE_PREFIX = 'FullBodyWithFPCamera'

    DOF_ATTRIBUTES = ('FirstPersonCamPivotAttrs_DOF_MaxX',
                      'FirstPersonCamPivotAttrs_DOF_MaxY',
                      'FirstPersonCamPivotAttrs_DOF_MaxZ',
                      'FirstPersonCamPivotAttrs_DOF_MinX',
                      'FirstPersonCamPivotAttrs_DOF_MinY',
                      'FirstPersonCamPivotAttrs_DOF_MinZ',
                      'FirstPersonCamPivotAttrs_DOF_FOV')

    def __init__(self):
        super(FpsCameraSettings, self).__init__()

    def prepareExportParameters(self,
                                namespace=''):
        if not super(FpsCameraSettings, self).prepareExportParameters(namespace=namespace):
            return False

        if self.namespace is None:
            firstPersonCamPivotAttrs = 'FirstPersonCamPivotAttrs'

        else:
            firstPersonCamPivotAttrs = '{}:FirstPersonCamPivotAttrs'.format(self.namespace)
    
        for attribute in self.DOF_ATTRIBUTES:
            cdll.rexMBRage.AddAdditionalModelEntry_Py(self.skelRoot,
                                                      firstPersonCamPivotAttrs,
                                                      'kTrackCameraLimit',
                                                      attribute)

        return True


class FpsAnimObject(object):
    SKELETON_DIRECTORY = ProjectData.data.GetSkelFilesDirectory()

    RIGGING_SKELETON_DIRECTORY = 'x:/rdr3/assets/anim/skeletons'

    TRANSLATION_ID_NAME = 'cameraTranslation'

    ROTATION_ID_NAME = 'cameraRotation'

    LIMIT_ID_NAME = 'cameraLimit'

    LEGAL_FPS_TRACKS = (TRANSLATION_ID_NAME,
                        ROTATION_ID_NAME,
                        LIMIT_ID_NAME)

    FPS_CAM_PIVOT_ID = 56194

    FPS_CAM_PIVOT_NAME = 'FirstPersonCamPivot'

    DOF_MAX_FOV_ID = 30929

    DOF_FOV_NAME = 'DOF_FOV'

    DOF_MAX_X_ID = 48389

    DOF_MAX_X_NAME = 'DOF_MaxX'

    DOF_MAX_Y_ID = 48390

    DOF_MAX_Y_NAME = 'DOF_MaxY'

    DOF_MAX_Z_ID = 48391

    DOF_MAX_Z_NAME = 'DOF_MaxZ'

    DOF_MIN_X_ID = 45733

    DOF_MIN_X_NAME = 'DOF_MinX'

    DOF_MIN_Y_ID = 45734

    DOF_MIN_Y_NAME = 'DOF_MinY'

    DOF_MIN_Z_ID = 45735

    DOF_MIN_Z_NAME = 'DOF_MinZ'

    NODE_DESCRIPTIONS = ('filepath',
                         'isFpsCameraAnimation',
                         'frameCount')

    NODE_ATTRIBUTES = ('cameraTranslationTrack',
                       'cameraRotationTrack')

    DOF_ATTRIBUTES = ('dofMaxXtrack',
                      'dofMaxYtrack',
                      'dofMinXtrack',
                      'dofMinYtrack',
                      'dofFovtrack')

    GYROSCOPE_DOFS = ('DOF_MaxX',
                      'DOF_MaxY',
                      'DOF_MinX',
                      'DOF_MinY',
                      'DOF_FOV')

    GYROSCOPE_CONE_MASTER = 'CameraPivot'

    GYROSCOPE_OFFSET_PROPERTIES = ('Down/Up',
                                   'DollyOut/DollyIn',
                                   'Left/Right')

    def __init__(self):
        self.bipedSkeleton = os.path.join(self.SKELETON_DIRECTORY,
                                          'human',
                                          'biped.skel')

        self.playerSkeleton = os.path.join(self.SKELETON_DIRECTORY,
                                           'human',
                                           'player.skel')

        self._rageAnim = RexMBAnim.RsRageAnim()

        self.frameCount = 0

        self.dofFovtrack = None

        self.cameraTranslationTrack = None

        self.cameraRotationTrack = None

        self.dofMaxXtrack = None

        self.dofMaxYtrack = None

        self.dofMinXtrack = None

        self.dofMinYtrack = None

        self.dofFovtrack = None

        self.inputTracks = []

        self.isFpsCameraAnimation = False

    def filterTrack(self,
                    track,
                    matchedId,
                    matchIdName,
                    writeAttribute):
        if not all([track.GetID() == matchedId,
                    track.GetIDName() == matchIdName]):
            return

        if track.GetIDName()=='cameraRotation':
            animTrack = sourceAnimTrack.AnimTrack()
            animTrack.track_id = track.GetID()
            animTrack.id = track.GetIDName()

            coneSpaceOffset = mobu.FBMatrix()
            coneSpaceOffsetRotation = mobu.FBVector3d(0.0, -90.0, 0.0)
            mobu.FBRotationToMatrix(coneSpaceOffset,
                                    coneSpaceOffsetRotation, 
                                    mobu.FBRotationOrder.kFBXYZ)


            moverWithConeOffsetInverse = mobu.FBMatrix(coneSpaceOffset)     
            moverWithConeOffsetInverse.Inverse()

            for frameIndex in xrange(self.frameCount):
                animValue = sourceAnimTrack.AnimTrackValues()
                animValue.frame = frameIndex
                animValue.value = track.GetValueArray(frameIndex)

                inQuaternion = mobu.FBVector4d(animValue.value[0],
                                               animValue.value[1],
                                               animValue.value[2],
                                               animValue.value[3])

                inRotation = mobu.FBVector3d()                
                mobu.FBQuaternionToRotation(inRotation,
                                            inQuaternion,
                                            mobu.FBRotationOrder.kFBXYZ)

                pivotMatrixAnim = mobu.FBMatrix()
                mobu.FBRotationToMatrix(pivotMatrixAnim,
                                        inRotation, 
                                        mobu.FBRotationOrder.kFBXYZ)
                        
                coneMasterPoseMatrix = moverWithConeOffsetInverse*pivotMatrixAnim*coneSpaceOffset

                coneMasterPoseRotation = mobu.FBVector3d()
                mobu.FBMatrixToRotation(coneMasterPoseRotation,
                                        coneMasterPoseMatrix, 
                                        mobu.FBRotationOrder.kFBXYZ)

                outQuaternion = mobu.FBVector4d()
                mobu.FBRotationToQuaternion(outQuaternion,
                                            coneMasterPoseRotation, 
                                            mobu.FBRotationOrder.kFBXYZ)

                animValue.value[0] = outQuaternion[0]
                animValue.value[1] = outQuaternion[1]
                animValue.value[2] = outQuaternion[2]
                animValue.value[3] = outQuaternion[3]

                animTrack.values.append(animValue)

            setattr(self,
                    writeAttribute,
                    animTrack)

        elif track.GetIDName()=='cameraTranslation':
            animTrack1 = sourceAnimTrack.AnimTrack()
            animTrack1.track_id = track.GetID()
            animTrack1.id = track.GetIDName()

            animTrack2 = sourceAnimTrack.AnimTrack()
            animTrack2.track_id = track.GetID()
            animTrack2.id = track.GetIDName()

            animTrack3 = sourceAnimTrack.AnimTrack()
            animTrack3.track_id = track.GetID()
            animTrack3.id = track.GetIDName()

            for frameIndex in xrange(self.frameCount):
                animValue1 = sourceAnimTrack.AnimTrackValues()
                animValue1.frame = frameIndex
    
                animValue2 = sourceAnimTrack.AnimTrackValues()
                animValue2.frame = frameIndex

                animValue3 = sourceAnimTrack.AnimTrackValues()
                animValue3.frame = frameIndex

                value = track.GetValueArray(frameIndex)

                sourcePosition = list(value)
                sourcePosition[0] = value[2]*100.0
                sourcePosition[1] = value[1]*100.0
                sourcePosition[2] = -value[0]*100.0

                outValue1 = RexMBAnim.FloatVector()
                outValue2 = RexMBAnim.FloatVector()
                outValue3 = RexMBAnim.FloatVector()

                outValue1.append(sourcePosition[0])
                outValue2.append(sourcePosition[1])
                outValue3.append(sourcePosition[2])

                animValue1.value = outValue1
                animTrack1.values.append(animValue1)

                animValue2.value = outValue2
                animTrack2.values.append(animValue2)

                animValue3.value = outValue3
                animTrack3.values.append(animValue3)

            setattr(self,
                    writeAttribute,
                    [animTrack1,
                     animTrack2,
                     animTrack3])

        else:
            animTrack = sourceAnimTrack.AnimTrack()
            animTrack.track_id = track.GetID()
            animTrack.id = track.GetIDName()

            for frameIndex in xrange(self.frameCount):
                animValue = sourceAnimTrack.AnimTrackValues()
                animValue.frame = frameIndex
                animValue.value = track.GetValueArray(frameIndex)
                animTrack.values.append(animValue)

            setattr(self,
                    writeAttribute,
                    animTrack)

    def collectFpsData(self,
                      filepath):
        self.filepath = filepath

        self._rageAnim.Load(filepath)

        self.inputTracks = [track
                            for track in self._rageAnim.GetTracks()
                            if track.GetIDName() in self.LEGAL_FPS_TRACKS]

        if not self.inputTracks:
            return

        self.isFpsCameraAnimation = True

        self.frameCount = self._rageAnim.GetNumInternalFrames()

        for track in self.inputTracks:
            self.filterTrack(track,
                             self.FPS_CAM_PIVOT_ID,
                             self.TRANSLATION_ID_NAME,
                             'cameraTranslationTrack')

            self.filterTrack(track,
                             self.FPS_CAM_PIVOT_ID,
                             self.ROTATION_ID_NAME,
                             'cameraRotationTrack')

            self.filterTrack(track,
                             self.DOF_MAX_X_ID ,
                             self.LIMIT_ID_NAME,
                             'dofMaxXtrack')

            self.filterTrack(track,
                             self.DOF_MAX_Y_ID ,
                             self.LIMIT_ID_NAME,
                             'dofMaxYtrack')

            self.filterTrack(track,
                             self.DOF_MIN_X_ID ,
                             self.LIMIT_ID_NAME,
                             'dofMinXtrack')

            self.filterTrack(track,
                             self.DOF_MIN_Y_ID ,
                             self.LIMIT_ID_NAME,
                             'dofMinYtrack')

            self.filterTrack(track,
                             self.DOF_MAX_FOV_ID ,
                             self.LIMIT_ID_NAME,
                             'dofFovtrack')

    def applyAnimation(self,
                       inputCharacter,
                       startFrame=0):
        if self.isFpsCameraAnimation is False:
            return

        if not isinstance(inputCharacter,
                          mobu.FBCharacter):
            return

        fpsConstraintData = fpsCore.ExtractFpsCameraConstraint(inputCharacter)

        if fpsConstraintData is None:
            return

        sourceHeadGyroscope = fpsConstraintData.constraint

        inputIndex = fpsCore.GYROSCOPE_IO.index(self.GYROSCOPE_CONE_MASTER)
        coneMaster = sourceHeadGyroscope.ReferenceGet(inputIndex, 0)

        for index, attribute in enumerate(self.GYROSCOPE_DOFS):
            dofProperty = coneMaster.PropertyList.Find(attribute)

            if dofProperty is None:
                continue

            currentTrack = getattr(self,
                                   self.DOF_ATTRIBUTES[index])

            if not isinstance(currentTrack,
                              sourceAnimTrack.AnimTrack):
                continue

            dofProperty.SetAnimated(True)
            dofAnimationNode = dofProperty.GetAnimationNode()

            animApply.ApplyTrack(coneMaster, 
                                 dofAnimationNode, 
                                 currentTrack, 
                                 startFrame)

        self.convertTranslation(coneMaster,
                                startFrame)

        self.convertRotation(coneMaster,
                             startFrame)

    def convertRotation(self,
                        coneMaster,
                        startFrame):
        currentTrack = getattr(self,
                               'cameraRotationTrack')

        if currentTrack is None:
            return

        dofProperty = coneMaster.PropertyList.Find('Rotation (Lcl)')

        if dofProperty is None:
            return

        dofProperty.SetAnimated(True)
        dofAnimationNode = dofProperty.GetAnimationNode()

        animApply.ApplyTrack(coneMaster, 
                             dofAnimationNode, 
                             currentTrack, 
                             startFrame)

    def convertTranslation(self,
                           coneMaster,
                           startFrame):
        currentTrack = getattr(self,
                               'cameraTranslationTrack')

        if currentTrack is None:
            return

        for index, currenProperty in enumerate(self.GYROSCOPE_OFFSET_PROPERTIES):
            dofProperty = coneMaster.PropertyList.Find(currenProperty)

            if dofProperty is None:
                return

            dofProperty.SetAnimated(True)
            dofAnimationNode = dofProperty.GetAnimationNode()

            animApply.ApplyTrack(coneMaster, 
                                 dofAnimationNode, 
                                 currentTrack[index], 
                                 startFrame)

    def __repr__(self):
        reportData = '\n\t<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_DESCRIPTIONS:
            currentAttribute = getattr(self, attribute)

            reportData += '\n\t\t{0}: {1}'.format(attribute,
                                                 currentAttribute)

        for attribute in self.NODE_ATTRIBUTES:
            currentAttribute = getattr(self, attribute)

            reportData += '\n\t\t{0}: {1}'.format(attribute,
                                                 'RS.Core.Animation.DataManager.AnimTrack.AnimTrack')

        for attribute in self.DOF_ATTRIBUTES:
            currentAttribute = getattr(self, attribute)

            reportData += '\n\t\t{0}: {1}'.format(attribute,
                                                 'RS.Core.Animation.DataManager.AnimTrack.AnimTrack')

        return reportData