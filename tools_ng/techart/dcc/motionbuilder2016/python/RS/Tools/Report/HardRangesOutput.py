###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_HardRangesOutput.py
## Written And Maintained By: Kathryn Bodey
## Contributors: 
## Description: Get Start and End Hard Ranges in an FBX
##              Output results into a spreadsheet
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
from time import strftime
import csv

import RS.Config
import RS.Utils.Path

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Function: Get Date, File Name and Start and End Hard Ranges
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def Run():
    lDate = strftime('%d_%m_%y')
    
    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName) 
    
    lStartTime = FBPlayerControl().LoopStart
    lStopTime = FBPlayerControl().LoopStop
    
    lStartFrame = lStartTime.GetTimeString()
    lStopFrame = lStopTime.GetTimeString()
    
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Function: Create Output List
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    lOutputList = []
    
    lOutputList.append(lFileName)
    lOutputList.append(lStartFrame)
    lOutputList.append(lStopFrame)
    
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Function: Output Start/Stop Frames into an Excel Sheet
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    lCSVFile = (open(RS.Config.Project.Path.Root + "\\art\\animation\\cutscene\\!!scenes\\HardRangesOutput_%s" %lDate + ".csv", "a"))
    lWriter = csv.writer(lCSVFile)
    lWriter.writerow(lOutputList)
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## End: Print Feeback
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    print lFileName + " done!"
    
    
