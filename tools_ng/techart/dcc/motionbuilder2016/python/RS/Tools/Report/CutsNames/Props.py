###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_CutsNames_Props
## Written And Maintained By: Kathryn Bodey
## Description: Functions to extract information from cut.xml files
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

import zipfile
from xml.dom import minidom
from time import strftime
import xml.dom.minidom
import csv 
import os

import RS.Config
import RS.Utils.Path


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: DEFINITION rs_unzip
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  

def rs_unzip(pZipFileName, pExtractPath = None):
    
    '''
    Description:
        Unzips a .zip file. Taken from Jason Hayes' 'unzipImpd' Function.
    '''
    
    if os.path.isfile(pZipFileName) and str(pZipFileName).endswith('.icd.zip'):
        lZipName = os.path.basename(str(pZipFileName).split('.')[0])
        
        # No extract path supplied, so make it relative to where the file currently lives.
        if pExtractPath == None:
            lExtractPath = '{0}\\{1}'.format(os.path.dirname(pZipFileName), lZipName)          
        
        else:
            lExtractPath = pExtractPath
        
        lZipObj = zipfile.ZipFile(pZipFileName)
        
        for iZipItem in lZipObj.namelist():   
            if iZipItem.endswith('/'):
                try:
                    os.makedirs('{0}\\{1}'.format(lExtractPath, iZipItem))
                    
                except WindowsError:
                    pass
                
            else:
                
                if iZipItem == 'data.cutxml':
                    lZipObj.extract(iZipItem, lExtractPath)
                    
        return lZipName
                         
    else:
        None
        

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: DEFINITION rs_datacutxmlParse
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_datacutxmlParse():
    
    lAssetList = []

    lDoc = minidom.parse(iFile)
    
    lCutObj = lDoc.getElementsByTagName("pCutsceneObjects")
    for iNodes in lCutObj:
        for iNodeChildren in iNodes.childNodes:
            if iNodeChildren.nodeType != iNodes.TEXT_NODE:
                if iNodeChildren.attributes["type"].value == "rage__cutfPropModelObject": 
                    for Node2 in iNodeChildren.childNodes:
                        if Node2.nodeType != iNodeChildren.TEXT_NODE:
                            if Node2.nodeName == "cName":
                                lName = Node2.childNodes[0].nodeValue
                                if ":" in lName:
                                    lNameSplit = lName.split(":")
                                    lAssetName = lNameSplit[0]
                                    lAssetList.append(lAssetName) 
                                else:
                                    lAssetList.append(lName)  
                                    print lName
                                
    return lAssetList
    

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: excludelist.txt Array
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

def Run():
    lExcludeArray = []
    
    #Get File Load Name
    lExcludedScenesTxt = ('{0}\\etc\\config\\cutscene\\dev\\excludedscenes.txt'.format( RS.Config.Tool.Path.Root ) )
    
    #Read Text File
    lFile = file(lExcludedScenesTxt ,"r")
    lTextLines = lFile.readlines()
    for iLine in lTextLines:
        if '\n' in iLine:
            lSplitName = iLine.split('\n')
            lExcludeArray.append(lSplitName[0])
        
        else: 
            lExcludeArray.append(iLine)
    
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: concatlist exclude Array
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ############################################################################################################### 
    
    lConcatExcludeArray = []
    
    #go through concatlist folder
    lConcatListDir = RS.Utils.Path.Walk("{0}//assets//cuts//!!Cutlists//".format( RS.Config.Project.Path.Root ), 1, None)
    
    for iConcatList in lConcatListDir:
        
        #Read Text File
        lFile = file(iConcatList ,"r")
        lTextLines = lFile.readlines()
        for iLine in lTextLines:
            if 'path' in iLine:
                lSplitLine = iLine.split('\\')
                lSplitLine2 = lSplitLine[-1].split('<')
                lConcatName = lSplitLine2[0]
                
                #create exclude list for concats
                for iExclude in lExcludeArray:
                    if iExclude.lower() == lConcatName.lower():
                        lConcatExcludeArray.append(lConcatName)           
            
            
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: concatparts Array
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ############################################################################################################### 
    
    lConcatPartsArray = []
    lApproved = None
    
    #go through concatlist folder
    lConcatDir = RS.Utils.Path.Walk("{0}//assets//cuts//!!Cutlists//".format( RS.Config.Project.Path.Root ), 1, None)
    
    for iConcatList in lConcatDir:
        
        #Get concatlist name
        lSplitLine = iConcatList.split('\\')
        lSplitLine2 = lSplitLine[-1].split('.')
        lConcatName = lSplitLine2[0]
    
        #check lConcatExcludeArray for any concatlists we dont need
        for iExclude in lConcatExcludeArray:
            if iExclude.lower() == lConcatName.lower():
                lConcatName = 'exclude'
            else:
                None
        
        if 'exclude' in lConcatName:
            None
        else:
    
            #Read Text File
            lFile = file(iConcatList ,"r")
            lTextLines = lFile.readlines()
            for iLine in lTextLines:
                if 'filename' in iLine:
                    lSplitLine = iLine.split('\\')
                    lPartName = lConcatName + ":" + lSplitLine[-2]
                    lConcatPartsArray.append(lPartName)
                    
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: lAssetsDir & csv output
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    lAssetsDir = RS.Utils.Path.Walk("{0}//assets//cuts//".format( RS.Config.Project.Path.Root ), 1, pPattern = 'data.cutxml')
    
    date = strftime('%d_%m')
    
    for iFile in lAssetsDir:
               
        lSceneNameSplit = iFile.split("\\")
        lSceneName = lSceneNameSplit[-2]
        
        #open/create csv
        lCSVFile = (open("C:\CUTFILE_Props_%s" %date + ".csv", "a"))
        lWriter = csv.writer(lCSVFile)
                   
        #compare lConcatPartsArray with lSceneNames
        for iConcatPart in lConcatPartsArray:
            lSplitConcatPart = iConcatPart.split(":")
            if lSplitConcatPart[-1].lower() == lSceneName.lower():
                lSceneName = "[" + lSplitConcatPart[0] + "] " + lSplitConcatPart[-1]
                  
        #compare lExcludeArray with lSceneNames
        for iExclude in lExcludeArray:
            if iExclude.lower() == lSceneName.lower():
                lSceneName = "EXCLUDEDSCENE_" + lSceneName
         
        #create asset array
        try:  
            
            if "shot_" in lSceneName.lower() or "test" in lSceneName.lower() or "part" in lSceneName.lower() or "excluded" in lSceneName.lower():
                None         
    
            else:
                lSceneNameList = []
                lSceneNameList.append(lSceneName)
                lAssetList = rs_datacutxmlParse() 
                
                #output csv
                if len(lAssetList) == 0:
                    lNoneString = "none"
                    lAssetList.append(lNoneString)
                    
                lWriter.writerow(lSceneNameList + lAssetList)      
                lCSVFile.close()        
        
        except:
            print "This data.cutxml does not work: " + lSceneName
            
