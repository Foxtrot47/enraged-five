###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_CutsNames_Props
## Written And Maintained By: Kathryn Bodey
## Description: Functions to extract information from cut.xml files
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from xml.dom import minidom
from time import strftime
import xml.dom.minidom
import csv 
import os

import RS.Config
import RS.Utils.Path

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Get File Directory
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################    

def Run():
    date = strftime('%d_%m')
    
    lDirectory = RS.Utils.Path.Walk("{0}//assets//cuts//".format( RS.Config.Project.Path.Root ), 1, pPattern = 'data.cutxml')
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Parse XML and Extract Asset
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    for iFile in lDirectory:
    
        lCSVFile = (open("C:\CUTFILE_Rayfire_%s" %date + ".csv", "a"))
        
        lWriter = csv.writer(lCSVFile)
    
        lAssetList = []
        lSceneNameList = []
        
        lSceneNameSplit = iFile.split("\\")
        lSceneName = lSceneNameSplit[-2]
        lSceneNameList.append(lSceneName)
        
        try: 
            
            if "shot_" in lSceneName.lower():
                None
                
            else:
                lDoc = minidom.parse(iFile)
                       
                lCutObj = lDoc.getElementsByTagName("pCutsceneObjects")
                for iNodes in lCutObj:
                    for iNodeChildren in iNodes.childNodes:
                        if iNodeChildren.nodeType != iNodes.TEXT_NODE:
                            if iNodeChildren.attributes["type"].value == "rage__cutfRayfireObject": 
                                for Node2 in iNodeChildren.childNodes:
                                    if Node2.nodeType != iNodeChildren.TEXT_NODE:
                                        if Node2.nodeName == "cName":
                                            lName = Node2.childNodes[0].nodeValue
                                            if ":" in lName:
                                                lNameSplit = lName.split(":")
                                                lAssetName = lNameSplit[0]
                                                lAssetList.append(lAssetName) 
                                            else:
                                                lAssetList.append(lName) 
                                
                if len(lAssetList) == 0:
                    lNoneString = "none"
                    lAssetList.append(lNoneString)
        
                lWriter.writerow(lSceneNameList + lAssetList)      
                lCSVFile.close() 
    
        except:
            print "This data.cutxml does not work: " + lSceneName
               