"""
Tool to allow users to dump Face Anims
"""
from PySide import QtGui
from RS.Core.Animation import AnimDumper


def DumpAnimFace():
    filePath, notCan = QtGui.QFileDialog.getSaveFileName(
                                                         None,
                                                         'Save export from Anim Face Dump',
                                                         AnimDumper.generateOutputFileName(),
                                                         "Anim Dumps (*.txt)"
                                                         )
    if not notCan:
        return
    AnimDumper.FacialDumper(exportFile=filePath)
    QtGui.QMessageBox.information(None, "Dump Anim Face", "Dump Complete")


def Run():
    DumpAnimFace()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    win = DumpAnimFace()
    sys.exit(app.exec_())
