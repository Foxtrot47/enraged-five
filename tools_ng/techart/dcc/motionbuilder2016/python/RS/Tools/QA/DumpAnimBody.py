"""
Tool to allow users to dump Body Anims
"""
from PySide import QtGui
from RS.Core.Animation import AnimDumper


def DumpAnimBody():
    filePath, notCan = QtGui.QFileDialog.getSaveFileName(
                                                         None,
                                                         'Save export from Anim Body Dump',
                                                         AnimDumper.generateOutputFileName(),
                                                         "Anim Dumps (*.txt)"
                                                         )
    if not notCan:
        return
    AnimDumper.AnimDumper(exportFile=filePath)
    QtGui.QMessageBox.information(None, "Dump Anim Body", "Dump Complete")


def Run():
    DumpAnimBody()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    win = DumpAnimBody()
    sys.exit(app.exec_())
