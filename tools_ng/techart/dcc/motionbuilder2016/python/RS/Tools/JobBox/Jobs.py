import os
import time
import pyfbsdk as mobu
import RS.Perforce as P4
from RS.Core.Camera import CamUtils, ClipTools, RangeTools
from RS.Core.Camera.KeyTools import camKeyPerforce, camKeyTasks
from RS.Core.Export.Export import RexExport
from RS.Core.Automation.Layout import LayoutTools
from RS.Core.Automation.FrameCapture import CapturePaths, FileTools
from RS.Utils import ContextManagers


def CreateJobBoxCL(sceneName):
    jobDesc = "Job Box - {0}".format(sceneName)
    # TODO - Below code doesn't work, because the P4 module is SHIT
    # localCLs = P4.GetLocalPendingChangelists()
    # for localCL in localCLs:
    #     localDesc = localCL["desc"].strip()
    #     if localDesc == jobDesc:
    #         return int(localCL["change"])
    newCL = P4.CreateChangelist(jobDesc)
    return newCL.Number


def MoveToJobBoxCL(filePath, newCL):
    cmdTxt = "p4 reopen -c {0} {1}".format(newCL, filePath)
    RangeTools.RunTerminalCommand(cmdTxt)
    # TODO - add error checking here? do we really need it?


def DetectJobBoxCL(filePath):
    fileState = P4.GetFileState(filePath)
    return fileState.Changelist
    # TODO - do we need error handling?


def CheckOutFile(filePath, jobBoxCL):
    fileState = P4.GetFileState(filePath)
    if not fileState:
        return "P4 Error: File path could not be read - {0}".format(filePath)
    if P4.IsOpenForEdit(fileState):
        MoveToJobBoxCL(filePath, jobBoxCL)
    else:
        fileExt = os.path.splitext(filePath)[1][1:].title()
        if fileState.OtherLocked:
            return "P4 Error: {0} is locked by another user's checkout.".format(fileExt)
        # TODO - alert user if current fbx is not latest
        P4.Sync(filePath, force=True)
        fileOpened = P4.Edit(filePath, changelistNum=jobBoxCL)
        if not fileOpened:
            return "P4 Error: {0} could not be checked out.".format(fileExt)
    # TODO: Move these to a job box changelist for the concat?
    return True


def CheckOutCuts(fbxPath, jobBoxCL):
    fbxName = os.path.splitext(os.path.basename(fbxPath))[0].upper()
    capPaths = CapturePaths.FastPath(fbxPath)
    cutFolder = capPaths.cutsPath
    exportCamAnim = os.path.join(cutFolder, "ExportCamera.anim")
    if P4.DoesFileExist(exportCamAnim):
        cutState = P4.GetFileState(exportCamAnim)
        if not cutState:
            return "P4 Error: Cut files could not be read - {0}".format(exportCamAnim)
        if P4.IsOpenForEdit(cutState):
            MoveToJobBoxCL(cutFolder + "/...", jobBoxCL)
        else:
            if cutState.OtherLocked:
                return "P4 Error: Cut files locked by another user's checkout."
            P4.Sync(cutFolder + "/...", force=True)
            openCutfiles = P4.Edit(cutFolder + "/...", changelistNum=jobBoxCL)
            if not openCutfiles:
                return "P4 Error: Cutfiles could not be checked out for {0}".format(fbxName)
            # Revert Toybox Files - stupid fix till I talk to Mike
            for root, dirs, files in os.walk(cutFolder):
                for filename in files:
                    if os.path.split(root)[1].lower() == "toybox":
                        if os.path.splitext(filename)[1].lower() == ".txt":
                            P4.Revert(os.path.join(root, filename))
    # TODO: Move these to a job box changelist for the concat?
    return True


def CheckOutZip(fbxPath, jobBoxCL):
    zipPath = CapturePaths.FastPath(fbxPath).zipPath
    if P4.DoesFileExist(zipPath):
        # Zip on P4 - try to check it out
        zipStatus = CheckOutFile(zipPath, jobBoxCL)
        if zipStatus is not True:
            return zipStatus
        else:
            return True
    else:
        # Zip not on P4 - return true for this pre-checkout, mark for add later
        return True


def CheckAddZip(fbxPath, jobBoxCL):
    zipPath = CapturePaths.FastPath(fbxPath).zipPath
    if P4.DoesFileExist(zipPath) is False:
        addSuccess = P4.Add(zipPath)
        if addSuccess:
            MoveToJobBoxCL(zipPath, jobBoxCL)
            return True
        return "Error: New zip could not be added."



def SyncZips(fbxPath):
    capPaths = CapturePaths.FastPath(fbxPath)
    zipFolder = capPaths.zipFolder
    # TODO: Might need to delete extra local zips here, but leaving commented for now
    # if os.path.exists(zipFolder):
    #     for zipName in os.listdir(zipFolder):
    #         zipPath = os.path.join(zipFolder, zipName)
    #         if P4.DoesFileExist(zipPath) is False:
    #             FileTools.DeletePath(zipPath)
    P4.Sync(zipFolder + "/...")


def SafeSyncCuts():
    fbxPath = mobu.FBApplication().FBXFileName
    capPaths = CapturePaths.FastPath(fbxPath)
    cutDataXml = os.path.join(capPaths.cutsPath, "data.cutxml")
    if P4.DoesFileExist(cutDataXml):
        cutState = P4.GetFileState(cutDataXml)
        if not cutState:
            return "P4 Error: Cut files could not be read - {0}".format(cutDataXml)
        if not P4.IsOpenForEdit(cutState):
            P4.Sync(capPaths.cutsPath + "/...", force=True)
    return True


def DetectBlankFbx():
    for comp in mobu.FBSystem().Scene.Components:
        if comp.LongName == "Scene" and hasattr(comp, "Children"):
            if len(comp.Children) > 0:
                return False
    return True


def DetectNewFbx():
    fbxPath = mobu.FBApplication().FBXFileName
    capPaths = CapturePaths.FastPath(fbxPath)
    cutDataXml = os.path.join(capPaths.cutsPath, "data.cutxml")
    if P4.DoesFileExist(cutDataXml) is False:
        return True
    return False


def CamExportSetup(fbxPath):
    # Suspend Messages
    with ContextManagers.SuspendMessages():

        # Set Paths
        fbxName = os.path.splitext(os.path.split(fbxPath)[-1])[0]
        capPaths = CapturePaths.FastPath(fbxPath)
        cutXmlPath = os.path.join(capPaths.cutsPath, "data.cutxml")

        # Open FBX
        mobu.FBApplication().FileOpen(fbxPath, True)
        # TODO - how to handle already open fbx?
        # openFbxPath = mobu.FBApplication().FBXFileName
        # if openFbxPath.lower() != fbxPath.lower():
        #     mobu.FBApplication().FileOpen(fbxPath, True)

        # Check for Empty Fbx
        if DetectBlankFbx():
            return "Fbx Error: The latest FBX for {0} is empty. Contact the last submitter.".format(fbxName)

        # Extra Merge Steps
        mergeScene = RangeTools.CheckForMergeFbx(fbxPath)
        if mergeScene:
            print "MERGE DETECTED - importing cams from main fbx."
            p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(mergeScene)
            P4.Sync(p4XmlPath, force=True)
            localXmlPath = camKeyPerforce.convertPerforcePathToLocal(p4XmlPath)
            camKeyTasks.DeleteAndLoadCamsFromXmlPath(localXmlPath)

        # Setup Autoclips, ExportCam, Toybox
        ClipTools.LoadCutsceneClips()
        CamUtils.SetupExportCam()
        LayoutTools.SetupToybox(showMessages=False)

        # Open Export Window
        mobu.ShowToolByName("Rex Rage Cut-Scene Export")
        # RexExport.RemoveAllCutsceneObjects()

        # Load Shots
        LayoutTools.LoadShotsFromEst()

        # Reload or Populate Cut Files
        if os.path.exists(cutXmlPath):
            RexExport.ImportCutFile(cutXmlPath)
        else:
            RexExport.RemoveAllCutsceneObjects()
            RexExport.AutoPopulateCutscene()
            # LayoutTools.SetupHandleFile()  # todo: seems this step is no longer needed?
            LayoutTools.SetSceneLocation()

        # Enable DOF - Gta only
        if capPaths.project == "GTA5":
            dofPassDetected = CamUtils.DetectDofPassComplete()
            RexExport.SetCutsceneShotEnableDOFFirstCut(0, dofPassDetected)

        # Save FBX Before Export
        mobu.FBApplication().FileSave(fbxPath)

        return True


def ExportAnims(camsOnly=False):
    project = os.environ.get("RS_PROJECT").upper()
    exportStart = time.time()

    # Export Anim/Cutfiles
    functDict = {True: RexExport.ExportCameraAnim, False: RexExport.ExportActiveCutscene}
    try:
        exportSuccess = functDict[camsOnly](True, True)  # no kwargs as spelling differs between projects
    except:
        exportSuccess = False

    # Export Failed - find export log and return path
    if not exportSuccess:
        logName = "export_active_cutscene.ulog"
        if project == "GTA5":
            logName = "export_{0}.ulog".format(RexExport.GetCutsceneSceneName())
        logPath = SearchForLog(logName)
        if logPath and os.path.getmtime(logPath) > exportStart:
            return logPath.lower()
        else:
            return "Error: Export anims failed, and log could not be found."

    # Check Zip Actually Updated
    fbxPath = mobu.FBApplication().FBXFileName
    capPaths = CapturePaths.FastPath(fbxPath)
    zipPath = capPaths.zipPath
    if not os.path.exists(zipPath) or os.path.getmtime(zipPath) < exportStart:
        return "Error: Exported anims, but zip was not updated."
    return True


def BuildRPF():
    try:
        rpfBuilt = RexExport.BuildActiveCutscene(True, False)  # kwarg spelling again
    except:
        rpfBuilt = False
    if not rpfBuilt:
        return "Error: Build RPF failed."
    # TODO: Maybe worth checking RPFs were updated?
    return True


def BuildPreview(sceneName, fbxPath):
    # Get PreviewPath from FbxPath
    capPaths = CapturePaths.FastPath(fbxPath)
    previewFolder = capPaths.previewPath

    # Backup, Build Preview, Restore
    buildStart = time.time()
    BackupPreviewData(previewFolder)
    try:
        buildSuccess = RexExport.BuildActiveCutscene(True, True)  # kwarg spelling again
    except:
        buildSuccess = False
    RestorePreviewData(previewFolder)

    # Build Failed - find export log and return path
    if not buildSuccess:
        logPath = SearchForLog("RSG.Pipeline.Convert")
        if logPath and os.path.getmtime(logPath) > buildStart:
            return logPath.lower()
        else:
            return "Error: Build preview failed, and log could not be found."

    # Check Preview Actually Updated
    previewCutName = sceneName.lower() + "."
    previewCutName = "cutscene@" + previewCutName if capPaths.project == "RDR3" else previewCutName
    if os.path.exists(previewFolder):
        for previewFileName in os.listdir(previewFolder):
            if previewFileName.startswith(previewCutName):
                previewCutPath = os.path.join(previewFolder, previewFileName)
                if os.path.getmtime(previewCutPath) > buildStart:
                    previewCount = len(os.listdir(previewFolder))
                    if previewCount < 4:
                        return "Error: Build preview ran, but only {0} preview files were built.".format(previewCount)
                    else:
                        return True
    return "Error: Build preview ran, but did not generate concat preview files."


def DeletePreviewData(fbxPath):
    capPaths = CapturePaths.FastPath(fbxPath)
    FileTools.DeleteFolderContents(capPaths.previewPath)


def BackupPreviewData(previewFolder):
    backupFolder = os.path.join(os.path.split(previewFolder)[0], "previewBackup")
    FileTools.DeletePath(backupFolder)
    FileTools.CopyFolderContents(previewFolder, backupFolder)


def RestorePreviewData(previewFolder):
    backupFolder = os.path.join(os.path.split(previewFolder)[0], "previewBackup")
    previewFiles = [pFile.lower() for pFile in os.listdir(previewFolder)] if os.path.exists(previewFolder) else []
    backupFiles = [bFile.lower() for bFile in os.listdir(backupFolder)] if os.path.exists(backupFolder) else []
    for backupFile in backupFiles:
        if backupFile not in previewFiles:
            backupPath = os.path.join(backupFolder, backupFile)
            FileTools.CopyPath(backupPath, previewFolder)
    FileTools.DeletePath(backupFolder)


def SearchForLog(logName):
    logs = []
    logRoot = os.path.join(os.environ.get("RS_TOOLSROOT"), "logs")
    for root, dirs, files in os.walk(logRoot):
        for filename in files:
            if logName.lower() in filename.lower():
                logs.append(os.path.join(root, filename))
    if logs:
        return max(logs, key=os.path.getmtime)
