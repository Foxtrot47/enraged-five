import os
import sys
import subprocess
from RS.Core.Automation.FrameCapture import CapturePaths
from RS.Core.Camera import ClipTools, RangeTools
from RS.Tools.JobBox import Jobs
import pyfbsdk as mobu

# Janky Pyside Import - allows same code on all projects
try:
    from PySide2 import QtCore, QtGui, QtWidgets
    from RS.PyCoreQt.Models import baseModel, baseModelItem
    qtProxyModel = QtCore.QSortFilterProxyModel
    qIcon = QtGui.QIcon
    qColor = QtGui.QColor
except ImportError:
    from PySide import QtCore, QtGui as QtWidgets
    from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem
    qtProxyModel = QtWidgets.QSortFilterProxyModel
    qIcon = QtWidgets.QIcon
    qColor = QtWidgets.QColor

pathParts = os.path.realpath(__file__).split(os.path.sep)
mobuPythonPath = os.path.sep.join(pathParts[:pathParts.index("RS")])
sys.path.insert(0, mobuPythonPath)
ICONSROOT = os.path.join(os.path.split(mobuPythonPath)[0], "images")
ICONPATH = os.path.join(ICONSROOT, "Rockstar_Games.png")
STYLESHEET = """
QTreeView { selection-background-color: navy;
            background-color: rgb(42,42,42);
            alternate-background-color: rgb(46,46,46);
            color: white; }

QTreeView::item:hover { background-color:grey; color:white; }
QTreeView::item:selected { background-color:darkBlue; color:white; }
QHeaderView::section { background-color:grey; }

QScrollBar::vertical { background: grey; }
QCheckBox { color: white; }
QLabel { color: white; }

QMessageBox { background-color: rgb(68,68,68); }
"""


def SearchP4FolderForFbx(p4Folder, fbxName):
    # Run P4 Search Via Command Line
    cmdTxt = "p4 files -e {0}.../{1}.fbx".format(p4Folder, fbxName)
    proc = subprocess.Popen(cmdTxt, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output, error = proc.communicate()
    outputLines = [line for line in output.splitlines()]

    # Return Error - if cmd fails or no files found
    if error or len(outputLines) == 1 and "no such file(s)" in outputLines[0]:
        return

    # Filter Paths, Return First - if only one item found
    pathLines = [line.split(" - ")[0] for line in outputLines]
    if len(pathLines) == 1:
        return pathLines[0].split("#")[0]

    # Multiple Paths - check which path has highest revision (invalid paths = low revs)
    maxRev = 0
    maxPath = None
    for pathLine in pathLines:
        pathText, pathRev = pathLine.split("#")
        if int(pathRev) > maxRev:
            maxPath = pathText
    return maxPath


def GetFbxPath(fbxName):
    # Get Directories
    capPaths = CapturePaths.FastPath()
    projectRoot = capPaths.projectRoot
    cutsceneRoot = capPaths.cutsceneRoot
    strandName = fbxName.split("_")[0]
    strandRoot = os.path.join(cutsceneRoot, strandName)
    os.chdir(projectRoot)

    # Fast Search First, then Full
    fbxPath = SearchP4FolderForFbx(strandRoot, fbxName)
    if fbxPath:
        return fbxPath
    fbxPath = SearchP4FolderForFbx(cutsceneRoot, fbxName)
    if fbxPath:
        return fbxPath


class JobStates(object):
    Error = -1
    Queued = 0
    Synced = 1
    Exported = 2
    Built = 3


class FbxItem(baseModelItem.BaseModelItem):
    _iconPath = os.path.join(ICONSROOT, "MotionBuilder", "take.png")

    def __init__(self, name, parent=None):
        super(FbxItem, self).__init__(parent=parent)
        self.name = name
        self.path = None
        self.status = JobStates.Queued

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column = index.column()
        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self.name
            if column == 1:
                return self.path
        elif role == QtCore.Qt.DecorationRole:
            if column == 0:
                return qIcon(self._iconPath)
        elif role == QtCore.Qt.BackgroundRole:
            if self.status == JobStates.Error:
                return qColor(150, 0, 0)
            if self.status == JobStates.Synced:
                return qColor(0, 116, 136)
            if self.status == JobStates.Exported:
                return qColor(0, 136, 122)
            if self.status == JobStates.Built:
                return qColor(0, 80, 0)


class MergeItem(FbxItem):
    _iconPath = os.path.join(ICONSROOT, "MotionBuilder", "material.png")


class SceneItem(baseModelItem.BaseModelItem):
    _iconPath = os.path.join(ICONSROOT, "MotionBuilder", "storyclip.png")

    def __init__(self, name, parent=None):
        super(SceneItem, self).__init__(parent=parent)
        self.name = name
        self.status = JobStates.Queued

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column = index.column()
        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self.name
        elif role == QtCore.Qt.DecorationRole:
            if column == 0:
                return qIcon(self._iconPath)
        elif role == QtCore.Qt.BackgroundRole:
            if self.status == JobStates.Error:
                return qColor(150, 0, 0)  # Red
            if self.status == JobStates.Synced:
                return qColor(0, 116, 136)  # Blue Green
            if self.status == JobStates.Exported:
                return qColor(0, 136, 122)  # Greener Blue
            if self.status == JobStates.Built:
                return qColor(0, 80, 0)  # Dark Green


class SceneModel(baseModel.BaseModel):
    def __init__(self, parent=None):
        super(SceneModel, self).__init__(parent=parent)

    def getHeadings(self):
        return ["Cutscenes"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def setupModelData(self, parent):
        fbxPath = mobu.FBApplication().FBXFileName.lower()
        sceneDict = RangeTools.GetRangesPickle(fbxPath)
        if sceneDict:
            for sceneName, scene in sorted(sceneDict.iteritems()):
                sceneItem = SceneItem(sceneName, parent)
                fbxList = []
                for track in scene.tracks:
                    fbxName = track.name
                    if fbxName not in fbxList:
                        fbxItem = FbxItem(fbxName, sceneItem)
                        sceneItem.appendChild(fbxItem)
                        fbxList.append(fbxName)
                        for mergePart in track.mergeParts:
                            mergeItem = MergeItem(mergePart, fbxItem)
                            fbxItem.appendChild(mergeItem)
                parent.appendChild(sceneItem)


class JobModel(baseModel.BaseModel):
    def __init__(self, parent=None):
        super(JobModel, self).__init__(parent=parent)

    def getHeadings(self):
        return ["Jobs"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def setupModelData(self, parent):
        return

    def addItem(self, newItem):
        if isinstance(newItem, FbxItem):
            fbxName = newItem.name
            sceneName = newItem.parent().name
            if isinstance(newItem, MergeItem):
                sceneName = newItem.parent().parent().name
            matchedScene = [child for child in self.rootItem.children() if child.name == sceneName]
            if matchedScene:
                sceneItem = matchedScene[0]
            else:
                sceneItem = SceneItem(sceneName, self.rootItem)
                self.rootItem.appendChild(sceneItem)
            matchedFbx = [child for child in sceneItem.children() if child.name == fbxName]
            if not matchedFbx:
                if isinstance(newItem, MergeItem):
                    fbxItem = MergeItem(fbxName, sceneItem)
                else:
                    fbxItem = FbxItem(fbxName, sceneItem)
                sceneItem.appendChild(fbxItem)
                self.layoutChanged.emit()

        elif isinstance(newItem, SceneItem):
            sceneName = newItem.name
            matchedScene = [child for child in self.rootItem.children() if child.name == sceneName]
            if matchedScene:
                sceneItem = matchedScene[0]
            else:
                sceneItem = SceneItem(sceneName, self.rootItem)
                self.rootItem.appendChild(sceneItem)
            for newFbx in newItem.children():
                if newFbx.name not in [child.name for child in sceneItem.children()]:
                    fbxItem = FbxItem(newFbx.name, sceneItem)
                    sceneItem.appendChild(fbxItem)
                    self.layoutChanged.emit()
                for mergeFbx in newFbx.children():
                    if mergeFbx.name not in [child.name for child in sceneItem.children()]:
                        mergeItem = MergeItem(mergeFbx.name, sceneItem)
                        sceneItem.appendChild(mergeItem)
                        self.layoutChanged.emit()

    def removeItem(self, oldItem):
        if isinstance(oldItem, FbxItem):
            sceneItem = oldItem.parent()
            print sceneItem.name, sceneItem
            print oldItem.name, oldItem
            # oldItem.parent = None
            # sceneItem.removeChild(oldItem)
            # self.layoutChange.emit()


class MainWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(MainWidget, self).__init__(parent=parent)
        self.project = CapturePaths.FastPath().project
        self.writeLog = False
        self.sceneViewer = QtWidgets.QTreeView()
        self.sceneModel = SceneModel()
        self.proxyModel = qtProxyModel()
        self.jobModel = JobModel()
        self.jobViewer = QtWidgets.QTreeView()
        self.camsOnly = False
        self.usePreview = True
        self.errorStop = True
        self.SetupUi()

    def SetupUi(self):
        mainLayout = QtWidgets.QVBoxLayout()
        sceneLayout = QtWidgets.QHBoxLayout()
        self.proxyModel.setSourceModel(self.sceneModel)
        self.proxyModel.sort(0, QtCore.Qt.AscendingOrder)
        self.sceneViewer.setModel(self.proxyModel)
        self.sceneViewer.setAlternatingRowColors(True)
        sceneLayout.addWidget(self.sceneViewer)

        buttonLayout = QtWidgets.QVBoxLayout()
        addButton = QtWidgets.QPushButton("Add")
        addButton.setFixedWidth(150)
        addButton.clicked.connect(self.AddPressed)
        buttonLayout.addWidget(addButton)
        resetButton = QtWidgets.QPushButton("Reset")
        resetButton.setFixedWidth(150)
        resetButton.clicked.connect(self.RemovePressed)
        buttonLayout.addWidget(resetButton)
        sceneLayout.addLayout(buttonLayout)
        mainLayout.addLayout(sceneLayout)

        jobLayout = QtWidgets.QHBoxLayout()
        self.jobViewer.setModel(self.jobModel)
        self.jobViewer.setAlternatingRowColors(True)
        jobLayout.addWidget(self.jobViewer)

        optionLayout = QtWidgets.QVBoxLayout()
        camsOnlyCheckBox = QtWidgets.QCheckBox("Cam Anims Only")
        camsOnlyCheckBox.stateChanged.connect(self.CamsOnlyPressed)
        optionLayout.addWidget(camsOnlyCheckBox)

        usePreviewCheckBox = QtWidgets.QCheckBox("Use Preview")
        usePreviewCheckBox.toggle()
        usePreviewCheckBox.stateChanged.connect(self.UsePreviewPressed)
        optionLayout.addWidget(usePreviewCheckBox)

        errorStopCheckBox = QtWidgets.QCheckBox("Stop on Errors")
        errorStopCheckBox.toggle()
        errorStopCheckBox.stateChanged.connect(self.ErrorStopPressed)
        optionLayout.addWidget(errorStopCheckBox)
        jobLayout.addLayout(optionLayout)
        mainLayout.addLayout(jobLayout)

        exportButton = QtWidgets.QPushButton("Export")
        exportButton.pressed.connect(self.ExportPressed)
        mainLayout.addWidget(exportButton)

        self.setLayout(mainLayout)
        self.setWindowTitle("Export Box")
        self.resize(600, 550)
        self.show()

        if len(self.sceneModel.rootItem.children()) == 0:
            buttons = [addButton, resetButton, exportButton, camsOnlyCheckBox, usePreviewCheckBox, errorStopCheckBox]
            for button in buttons:
                button.setEnabled(False)
            exportButton.setText("Error: No ranges found for this project or DLC pack.")
            exportButton.setStyleSheet('QPushButton {color: red;}')


    def PopUpMessage(self, titleText, infoText, iconType=QtWidgets.QMessageBox.Information):
        msgBox = QtWidgets.QMessageBox()
        mainWidgetPosition = self.geometry().center().toTuple()
        msgBox.setGeometry(mainWidgetPosition[0]-100, mainWidgetPosition[1]-50, 300, 300)
        msgBox.setWindowIcon(qIcon(ICONPATH))
        msgBox.setWindowTitle(titleText)
        msgBox.setIcon(iconType)
        msgBox.setStyleSheet(STYLESHEET)
        msgBox.setText("{}      ".format(infoText))
        msgBox.exec_()

    def AddPressed(self):
        indexes = self.sceneViewer.selectedIndexes()
        if indexes:
            item = self.proxyModel.mapToSource(indexes[0]).internalPointer()
            self.jobModel.addItem(item)
            self.jobViewer.expandAll()
            self.jobViewer.clearSelection()
            self.sceneViewer.clearSelection()

    def RemovePressed(self):
        self.jobModel.reset()
        # indexes = self.jobViewer.selectedIndexes()
        # if indexes:
        #     jobItem = indexes[0].internalPointer()
        #     self.jobModel.removeItem(jobItem)

    def CamsOnlyPressed(self, stateInt):
        self.camsOnly = False if stateInt is 0 else True

    def UsePreviewPressed(self, stateInt):
        self.usePreview = False if stateInt is 0 else True

    def ErrorStopPressed(self, stateInt):
        self.errorStop = False if stateInt is 0 else True

    def UpdateItemStatus(self, jobItems, jobState):
        jobItems = jobItems if type(jobItems) == list else [jobItems]
        for jobItem in jobItems:
            jobItem.status = jobState
        self.jobModel.layoutChanged.emit()

    def RefreshJobViewer(self):
        jobItems = []
        self.jobViewer.clearSelection()
        for sceneItem in self.jobModel.rootItem.children():
            jobItems.append(sceneItem)
            [jobItems.append(fbxItem) for fbxItem in sceneItem.children()]
        self.UpdateItemStatus(jobItems, JobStates.Queued)

    def SyncJobData(self):
        for sceneItem in self.jobModel.rootItem.children():
            jobBoxCL = Jobs.CreateJobBoxCL(sceneItem.name)
            for fbxItem in sceneItem.children():
                detectedPath = GetFbxPath(fbxItem.name)
                if not detectedPath:
                    return "Fbx Error: Fbx path not found."
                fbxItem.path = "x:{0}".format(detectedPath[1:].replace("/", "\\"))
                fbxStatus = Jobs.CheckOutFile(fbxItem.path, jobBoxCL)
                if fbxStatus is not True:
                    return fbxStatus
                cutStatus = Jobs.CheckOutCuts(fbxItem.path, jobBoxCL)
                if cutStatus is not True:
                    return cutStatus
                if self.project == "GTA5":
                    zipStatus = Jobs.CheckOutZip(fbxItem.path, jobBoxCL)
                    if zipStatus is not True:
                        return zipStatus
                self.UpdateItemStatus(fbxItem, JobStates.Synced)

            # Setup Zips and Concat
            firstFbx = sceneItem.children()[0]
            Jobs.SyncZips(firstFbx.path)
            concatStatus = RangeTools.CreateConcatList(firstFbx.path, clNumber=jobBoxCL)
            if concatStatus is not True:
                return concatStatus

        return True

    def HandleError(self, errorStatus):
        if self.errorStop:
            if errorStatus.endswith(".ulog"):
                subprocess.Popen(["start", errorStatus], shell=True)  # Using start forces window to front
            else:
                self.PopUpMessage("ExportBox", errorStatus)
            return
        # Rare Automation Case - we don't want to stop on errors, but can still show log
        elif errorStatus.endswith(".ulog"):
            subprocess.Popen(errorStatus, shell=True)

    def ExportPressed(self):
        self.RefreshJobViewer()
        syncStatus = self.SyncJobData()
        if syncStatus is not True:
            self.PopUpMessage("JobBox", syncStatus)
            return

        # TODO: Delete Preview Data - need to make this not crappy
        if len(self.jobModel.rootItem.children()) > 0:
            fbxPath = self.jobModel.rootItem.children()[0].children()[0].path
            Jobs.DeletePreviewData(fbxPath)

        # SCENE LOOP
        for sceneItem in self.jobModel.rootItem.children():
            # TODO: Below log stuff is temporary code for Duke
            if self.writeLog:
                with open("x:/{0}.txt".format(sceneItem.name), "w+") as logFile:
                    for fbxItem in sceneItem.children():
                        partName = fbxItem.name.replace(sceneItem.name, "")
                        partName = partName[1:] if len(partName) > 1 else partName
                        logFile.write(partName + "\n")

            # FBX LOOP
            for fbxItem in sceneItem.children():
                # Run Cam Export Setup
                setupStatus = Jobs.CamExportSetup(fbxItem.path)
                if setupStatus is not True:
                    self.UpdateItemStatus(fbxItem, JobStates.Error)
                    if self.errorStop:
                        self.PopUpMessage("JobBox", setupStatus)
                        return

                # Check for New Fbx - overrides to full export
                localBool = self.camsOnly
                if self.camsOnly and Jobs.DetectNewFbx():
                    localBool = False

                # Export Anims
                exportStatus = Jobs.ExportAnims(camsOnly=localBool)

                # Reopen Cuts/Zips/ias? to JobBox CL
                jobBoxCL = Jobs.DetectJobBoxCL(fbxItem.path)
                Jobs.CheckOutCuts(fbxItem.path, jobBoxCL)
                if self.project == "GTA5":
                    Jobs.CheckAddZip(fbxItem.path, jobBoxCL)

                # Export Success
                if exportStatus is True:
                    self.UpdateItemStatus(fbxItem, JobStates.Exported)

                # Export Error
                else:
                    self.UpdateItemStatus(fbxItem, JobStates.Error)
                    self.HandleError(exportStatus)
                    if self.errorStop:
                        return

            # Build Preview/RPF - if no errors encountered
            jobErrors = [fbxItem for fbxItem in sceneItem.children() if fbxItem.status == JobStates.Error]
            if not jobErrors:
                if self.usePreview:
                    fbxPath = sceneItem.children()[0].path
                    buildStatus = Jobs.BuildPreview(sceneItem.name, fbxPath)
                else:
                    buildStatus = Jobs.BuildRPF()

                # Build Success
                if buildStatus is True:
                    # jobItems = [sceneItem] + list([fbxItem for fbxItem in sceneItem.children()])
                    # self.UpdateItemStatus(jobItems, JobStates.Built)
                    self.UpdateItemStatus(sceneItem, JobStates.Built)

                # Build Error
                else:
                    self.UpdateItemStatus(sceneItem, JobStates.Error)
                    self.HandleError(buildStatus)



from RS.Tools.UI import Run
@Run("Export Box", url="https://hub.rockstargames.com/display/ANIM/InGame+Render+Tool")
def Run():
    ClipTools.CheckForRangeUpdate()
    wid = MainWidget()
    wid.resize(600, 550)
    return wid
