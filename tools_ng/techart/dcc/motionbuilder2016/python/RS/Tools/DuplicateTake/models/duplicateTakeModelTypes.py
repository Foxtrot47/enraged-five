from PySide import QtGui, QtCore

import os

import pyfbsdk as mobu

from RS.Tools.UI.Mocap import const
from RS import Config, Globals, Perforce
from RS.Utils import Scene
from RS.Tools.DuplicateTake.models import duplicateTakeModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class DuplicateTakeModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    takeNameChangedSignal = QtCore.Signal()
    audioNameChangedSignal = QtCore.Signal(object, object)
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(DuplicateTakeModel, self).__init__(parent=parent)

    def _handleTakeNameChangeSignal(self):
        self.takeNameChangedSignal.emit()

    def _handleAudioNameChangeSignal(self, audio, take):
        self.audioNameChangedSignal.emit(audio, take)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ['Take', 'Audio']

    def _getTakeList(self):
        '''
        Get list of takes in the scene
        Returns:
            List (FBTake)
        '''
        takeList = []
        for take in Globals.Takes:
            takeList.append(take)
        return takeList

    def _getAudioList(self):
        '''
        Get list of audio in the scene
        Returns:
            List (FBAudioClip)
        '''
        audioList = []
        for audioClip in Globals.AudioClips:
            audioList.append(audioClip)
        return audioList

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        takeList = self._getTakeList()
        audioList = self._getAudioList()
        for take in takeList:
            audioProperty = take.PropertyList.Find('audio match')
            if audioProperty and audioProperty.Data != "":
                dataItem = duplicateTakeModelItem.DuplicateTakeTextModelItem(take,
                                                                             str(audioProperty.Data),
                                                                             str(""),
                                                                             parent=parent)
                parent.appendChild(dataItem)
                continue
            audioFound = False
            for audio in audioList:
                takeProperty = audio.PropertyList.Find('Take')
                if len(takeProperty) > 0:
                    takeProperty = takeProperty[0]
                if take == takeProperty:
                    audioFound = True
                    pathProperty = audio.PropertyList.Find('Path')
                    dataItem = duplicateTakeModelItem.DuplicateTakeTextModelItem(take,
                                                                                 str(audio.Name),
                                                                                 str(pathProperty.Data),
                                                                                 parent=parent)
                    # add custom properties to takes found with audio
                    customProperty = take.PropertyCreate('audio match',
                                                               mobu.FBPropertyType.kFBPT_charptr,
                                                               "",
                                                               False,
                                                               True,
                                                               None)
                    customProperty.Data = audio.Name
                    customProperty.SetLocked(True)
                    # append tree view with item
                    parent.appendChild(dataItem)
                    break
            if audioFound != True:
                dataItem = duplicateTakeModelItem.DuplicateTakeTextModelItem(take,
                                                                             "None",
                                                                             '',
                                                                             parent=parent)
                # append tree view with item
                parent.appendChild(dataItem)

        # connect signal to instances
        for child in parent.children():
            child.takeNameChangedSignal.connect(self._handleTakeNameChangeSignal)
            child.audioNameChangedSignal.connect(self._handleAudioNameChangeSignal)
    def flags(self, index):
        '''
        Flags are added to determine column properties
        
        Args:
            index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        column = index.column()

        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable
        if column == 0:
            return flags | QtCore.Qt.ItemIsUserCheckable
        else:
            return flags