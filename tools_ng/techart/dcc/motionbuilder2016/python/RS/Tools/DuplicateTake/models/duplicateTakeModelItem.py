from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class DuplicateTakeTextModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    takeNameChangedSignal = QtCore.Signal()
    audioNameChangedSignal = QtCore.Signal(object, object)
    def __init__(self, take, audio, audioPath, parent=None):
        super(DuplicateTakeTextModelItem, self).__init__([], parent=parent)
        self._columnNumber = 2
        self._takeName = take.Name
        self._take = take
        self._audio = audio
        self._audioPath = audioPath
        self._checked = False

    def SetCheckedState(self, boolean):
        '''
        sets check state to False for item
        '''
        self._checked = boolean

    def GetCheckedState(self):
        '''
        Return:
            Checked State (Boolean): current check state of gs asset's checkbox in ui
        '''
        return self._checked

    def GetTakeName(self):
        '''
        Return:
            Take Name (string): name of the take
        '''
        return self._takeName

    def GetAudioName(self):
        '''
        Return:
            Take Name (string): name of the take
        '''
        return self._audio

    def GetAudioPath(self):
        '''
        Return:
            Path (string): path to point to for the popup's initial folder
        '''
        return self._audioPath

    def GetAudioPath(self):
        '''
        Return:
            Path (string): path to point to for the popup's initial folder
        '''
        return self._audioPath

    def columnCount(self):
        '''
        returns the number of columns in view
        '''
        return self._columnNumber

    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()
        if role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
            if column == 0:
                return self._takeName
            if column == 1:
                return self._audio

        elif role == QtCore.Qt.ToolTipRole:
            if column == 1:
                return self._audioPath

        elif role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if self._checked:
                    return QtCore.Qt.CheckState.Checked
                else:
                    return QtCore.Qt.CheckState.Unchecked
        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        model data updated based off of user interaction with the view
        '''
        column = index.column()
        if role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if value == QtCore.Qt.CheckState.Checked:
                    self._checked = True
                else:
                    self._checked = False
                return True

        elif role == QtCore.Qt.EditRole:
            if column == 0:
                self._takeName = value
                self._take.Name = str(self._takeName)
                self.takeNameChangedSignal.emit()
                return True
            if column == 1:
                self._audio = value
                self.audioNameChangedSignal.emit(value, self._take)
                return True
        return False
