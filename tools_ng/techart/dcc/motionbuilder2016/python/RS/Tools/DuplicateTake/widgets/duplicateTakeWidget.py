from PySide import QtGui, QtCore

import pyfbsdk as mobu

import os
from xml.dom import minidom

from RS import Globals, Perforce
from RS.Tools import UI
from RS.Tools.DuplicateTake.models import duplicateTakeModelTypes
from RS.Tools.CameraToolBox.PyCoreQt.Delegates import comboboxDelegate


class DuplicateTakeWidgetDialog(UI.QtMainWindowBase):
    '''
    Dialogue to run PropCharRootSnapWidget and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(DuplicateTakeWidgetDialog, self).__init__(parent=parent)
        self._settings = QtCore.QSettings( "RockstarSettings", "DuplicateTakeWidgetDialog" )
        duplicateTakeWidget = DuplicateTakeWidget()

        # create layout and add widget
        self.setCentralWidget(duplicateTakeWidget)

    def showEvent(self, event):
        '''
        Set previously saved widget size and position preferences
        '''
        super(DuplicateTakeWidgetDialog, self).showEvent(event)
        try:
            self.resize(self._settings.value("size", QtCore.QSize(800, 800)))
            self.move(self._settings.value("pos", QtCore.QPoint(800, 800)))
        except:
            pass

    def closeEvent(self, event):
        '''
        Save widget size and position preferences
        '''
        try:
            super(DuplicateTakeWidgetDialog, self).closeEvent(event)
            self._settings.setValue("size", self.size())
            self._settings.setValue("pos", self.pos())
        except:
            pass


class DuplicateTakeWidget(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(DuplicateTakeWidget, self).__init__(parent=parent)
        mobu.FBApplication().OnFileExit.Add(self._unregisterTakeEventHandler)
        mobu.FBSystem().Scene.OnTakeChange.Add(self._registerTakeEventHandler)
        self.setupUi()

    def _getXMlPath(self):
        fullFilePath = mobu.FBApplication().FBXFileName
        dirPath = os.path.dirname(fullFilePath)
        fileName = os.path.basename(fullFilePath)
        xmlPath = os.path.join(dirPath,
                               "TakeToAudioMatch.xml")
        return xmlPath

    def OutputXML(self):
        # get path for xml to save to
        xmlPath = self._getXMlPath()
        # check file out in perforce, if it exists already
        if os.path.exists(xmlPath):
            Perforce.Edit(xmlPath)
        # get take and audio information
        takeDict = {}
        for child in self.mainModel.rootItem.children():
            takeName = str(child.GetTakeName())
            audioName = str(child.GetAudioName())
            takeDict[takeName] = audioName

        # get info
        fileHandle = open(xmlPath, "wb")
        implementation = minidom.getDOMImplementation()
        base = implementation.createDocument(None, "Takes", None)
        baseElement = base.documentElement
        for key, value in takeDict.iteritems():
            tabNode = base.createElement("take")
            tabName = base.createElement("takeName")
            tabName.appendChild(base.createTextNode(key))
            scriptCode = base.createElement("audioName")
            scriptCode.appendChild(base.createTextNode(value))
            tabNode.appendChild(tabName)
            tabNode.appendChild(scriptCode)
            baseElement.appendChild(tabNode)

        # write to xml
        base.writexml(fileHandle)
        fileHandle.close()
        # make readable
        xmlDoc = minidom.parse(xmlPath)
        with open(xmlPath, "wb") as fileHandle:
            fileHandle.write(xmlDoc.toprettyxml())

        # mark for add in P4 if file is new
        Perforce.Add(xmlPath)

    def ParseXml(self):
        takeDict = {}
        # get path for xml to open
        xmlPath = self._getXMlPath()
        if os.path.exists(xmlPath):
            xmldoc = minidom.parse(xmlPath)
            # get take and audio info
            takes = xmldoc.getElementsByTagName("take")
            for take in takes:
                takeNameElement = take.getElementsByTagName("takeName")[0]
                audioNameElement = take.getElementsByTagName("audioName")[0]
                if takeNameElement.firstChild:
                    takeName = takeNameElement.firstChild.data
                else:
                    takeName = None
                if audioNameElement.firstChild:
                    audioName = audioNameElement.firstChild.data
                else:
                    audioName = None
                takeDict[takeName] = audioName
        return takeDict

    def _unregisterTakeEventHandler(self, control=None, event=None):
        mobu.FBSystem().Scene.OnTakeChange.Remove(self._registerTakeEventHandler)
        mobu.FBApplication().OnFileExit.Remove(self._registerTakeEventHandler)

    def _registerTakeEventHandler(self, control=None, event=None):
        # set current take to correct audio
        currentTake = mobu.FBSystem().CurrentTake

        # match audio with take, from xml
        takeDict = self.ParseXml()

        for take, audio in takeDict.iteritems():
            audioString = None
            if currentTake.Name == take:
                audioString = audio
                break
        if audioString == None:
            return
        audioFound = False
        for audioClip in Globals.AudioClips:
            if audioClip.Name == audioString:
                audioFound = True
                break
        if audioFound == False:
            return

        takeProperty = audioClip.PropertyList.Find('Take')
        takeProperty.removeAll()
        takeProperty.append(currentTake)

    def _getTakeComponent(self, name):
        takeComponent = None
        for take in Globals.Takes:
            if take.Name == name:
                takeComponent = take
                break
        return takeComponent

    def _getAudioComponent(self, name):
        audioComponent = None
        for audio in Globals.AudioClips:
            if audio.Name == name:
                audioComponent = audio
                break
        return audioComponent

    def _duplicateTake(self):
        takeDict = self.ParseXml()
        for key, value in takeDict.iteritems():
            for child in self.mainModel.rootItem.children():
                checked = child.GetCheckedState()
                takeName = str(child.GetTakeName())
                # get take component
                take = self._getTakeComponent(key)
                if take and takeName == take.Name and checked:
                    # get audio component
                    audio = self._getAudioComponent(value)
                    if audio != None:
                        audioProperty = take.PropertyList.Find('audio match')
                        if not audioProperty:
                            customProperty = take.PropertyCreate('audio match',
                                                                       mobu.FBPropertyType.kFBPT_charptr,
                                                                       "",
                                                                       False,
                                                                       True,
                                                                       None)
                            customProperty.Data = audio.Name
                            customProperty.SetLocked(True)
                        # copy take
                        copiedTake = take.CopyTake(take.Name)
                        customProperty = copiedTake.PropertyCreate('audio match',
                                                                   mobu.FBPropertyType.kFBPT_charptr,
                                                                   "",
                                                                   False,
                                                                   True,
                                                                   None)
                        customProperty.Data = audio.Name
                        customProperty.SetLocked(True)
                        takeProperty = audio.PropertyList.Find('Take')
                        if takeProperty:
                            takeProperty.removeAll()
                            takeProperty.append(copiedTake)
                        break
                    elif take:
                        # no audio, just do a take copy
                        take.CopyTake(take.Name)
        # reset model
        self.mainModel.reset()
        # output xml with latest data
        self.OutputXML()

    def _deleteDuplicateTakes(self):
        '''
        Iterates through the AudioClips list and deletes any duplicated
        '''
        deleteList = []
        for audioClip_a in Globals.AudioClips:
            for audioClip_b in Globals.AudioClips:
                if audioClip_a == audioClip_b:
                    break
                if audioClip_a.Name == audioClip_b.Name:
                    if audioClip_a.Name != audioClip_a.LongName:
                        deleteList.append(audioClip_a)
        for item in deleteList:
            item.FBDelete()

    def _handleTakeNameChangeSignal(self):
        self.OutputXML()

    def _handleAudioNameChangeSignal(self, audio, take):
        # get audio clip
        audioComponent = None
        audioComponent = self._getAudioComponent(audio)

        if audioComponent:
            # update take custom property
            audioProperty = take.PropertyList.Find('audio match')
            if audioProperty:
                oldAudioComponent = self._getAudioComponent(audioProperty.Data)
                audioProperty.SetLocked(False)
                audioProperty.Data = str(audio)
                audioProperty.SetLocked(True)
                # remove old audio
                if oldAudioComponent:
                    takeProperty = oldAudioComponent.PropertyList.Find('Take')
                    if takeProperty:
                        takeProperty.removeAll()
            # assign take to active in audio
            takeProperty = audioComponent.PropertyList.Find('Take')
            if takeProperty:
                takeProperty.removeAll()
                takeProperty.append(take)
            # regenerate xml
            self.OutputXML()
        else:
            # delete take custom property
            audioProperty = take.PropertyList.Find('audio match')
            if audioProperty:
                oldAudioComponent = self._getAudioComponent(audioProperty.Data)
                take.PropertyRemove(audioProperty)
                # remove take from audio
                if oldAudioComponent:
                    takeProperty = oldAudioComponent.PropertyList.Find('Take')
                    if takeProperty:
                        takeProperty.removeAll()
            # regenerate xml
            self.OutputXML()

    def _getAudioNameList(self):
        audioNameList = ["None"]
        for audio in self.mainModel._getAudioList():
            audioNameList.append(audio.Name)
        audioNameList = list(set(audioNameList))
        return audioNameList

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()

        # create widgets
        self.treeView = QtGui.QTreeView()
        horizontalLine = QtGui.QFrame()
        self.duplicateButton = QtGui.QPushButton()
        horizontalLine2 = QtGui.QFrame()
        self.deleteDupesButton = QtGui.QPushButton()

        # set model to treeview
        self.mainModel =  duplicateTakeModelTypes.DuplicateTakeModel()
        self.treeView.setModel(self.mainModel)

        # output xml of scene data
        self.OutputXML()

        # widget properties
        self.treeView.setAlternatingRowColors(True)
        self.treeView.setStyleSheet("QTreeView {\nalternate-background-color: rgb(56, 56, 56); \nbackground-color: rgb(43, 43, 43);\n} \n")
        self.treeView.setSortingEnabled(True)
        self.treeView.resizeColumnToContents(0)
        self.treeView.resizeColumnToContents(1)
        self.treeView.header().setSortIndicator(0, QtCore.Qt.AscendingOrder)
        horizontalLine.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine.setFrameShadow(QtGui.QFrame.Sunken)
        self.duplicateButton.setText('Duplicate Take(s)')
        horizontalLine2.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine2.setFrameShadow(QtGui.QFrame.Sunken)
        self.deleteDupesButton.setText('Delete Duplicate AudioClips')

        # set delegate for column 1 audio file names
        self.treeView._comboBoxDelegate = comboboxDelegate.ComboBoxDelegate(self._getAudioNameList())
        self.treeView.setItemDelegateForColumn(1, self.treeView._comboBoxDelegate)

        # set widgets to layout
        mainLayout.addWidget(self.treeView, 0, 0)
        mainLayout.addWidget(horizontalLine, 1, 0)
        mainLayout.addWidget(self.duplicateButton, 2, 0)
        mainLayout.addWidget(horizontalLine2, 3, 0)
        mainLayout.addWidget(self.deleteDupesButton, 4, 0)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        self.duplicateButton.released.connect(self._duplicateTake)
        self.deleteDupesButton.released.connect(self._deleteDuplicateTakes)
        self.mainModel.takeNameChangedSignal.connect(self._handleTakeNameChangeSignal)
        self.mainModel.audioNameChangedSignal.connect(self._handleAudioNameChangeSignal)
