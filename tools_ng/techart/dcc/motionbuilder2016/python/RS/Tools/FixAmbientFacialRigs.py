import RS.Core.Face.Lib
import RS.Globals

def Run():
    ambientUINulls = RS.Core.Face.Lib.collectAllAmbientFacialRigRoots()
    
    if ambientUINulls:
        RS.Globals.gRsUlog.flush()
        
        for obj in ambientUINulls:
            try:
                namespace, name = str( obj.LongName ).split( ':' )
            
            # Found multiple ':' characters, so deal with that case.
            except ValueError:
                names = str( obj.LongName ).split( ':' )
                name = names[ -1 ]
    
                namespace = names[ 0 ]
                namespaces = names[ 1 : -1 ]
                
                # Rebuild the namespace.
                for ns in namespaces:
                    namespace += ':{0}'.format( ns )
                
            RS.Core.Face.Lib.fixAmbientFacialRig( namespace )
            
        RS.Globals.gRsUlog.Show( False )