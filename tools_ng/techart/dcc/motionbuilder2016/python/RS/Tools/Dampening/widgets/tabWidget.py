from PySide import QtCore, QtGui

import pyfbsdk as mobu

from RS import Globals
from RS.Utils import Scene
from RS.Utils.Scene import RelationshipConstraintManager as rcm


class CreateDampeningWidget(QtGui.QWidget):
    createSignal = QtCore.Signal()
    def __init__(self, parent=None):
        super(CreateDampeningWidget, self).__init__(parent=parent)
        self._itemList = None
        self._createSpinBox = None
        self._constraintNameLineEdit = None
        self._constraintNamespaceLabel = None
        self._constraintNameString = '_Dampening_Relation'
        self._setupTab()

    def _populateSelectedInTree(self):
        '''
        Adds selected nulls from the scene to the tree.
        Each selection will be displayed in the tree as a string. They will all be at the same child
        level
        '''
        # get selected items from scene
        selectedItemsList = []
        mobu.FBBeginChangeAllModels()
        for component in Globals.Components:
            if component.Selected == True and isinstance(component, mobu.FBModel):
                selectedItemsList.append(component)
        mobu.FBEndChangeAllModels()

        # get existing list of items (user may add more items to an existing list)
        existingList = self._getListWidgetItemStrings()

        # add string items to list - ignoring any items that match ones in the existingList
        # adding items one at a time to assign user data to each one. User data is an FBModel object
        for item in selectedItemsList:
            if item.LongName not in existingList:
                newListItem = QtGui.QListWidgetItem()
                newListItem.setData(QtCore.Qt.UserRole, item)
                newListItem.setText(item.LongName)
                self._itemList.addItem(newListItem)

        # update namespace
        self._setConstraintNamespace()

    def _removeListItems(self):
        '''
        Removes the selected item(s) from the list
        '''
        # get selected item list
        selectedItemList = self._itemList.selectedIndexes()
        for idx in selectedItemList:
            # use takeItem to remove fromt he list - pass the row
            self._itemList.takeItem(idx.row())

    def _createDampeningConstraint(self):
        '''
        Creates a relation constraint for the dampening

        Returns:
                FBConstraintRelation
        '''
        constraintName = str(self._getConstraintFullName())
        dampeningValue = self._getCurrentSpinBoxValue()
        # create relation constraint
        relationConstraint = rcm.RelationShipConstraintManager(name=constraintName)
        relationConstraintProperty = relationConstraint.GetConstraint().PropertyCreate('Damping Relation', mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)
        relationConstraintProperty.Data = constraintName
        dampingValueProperty = relationConstraint.GetConstraint().PropertyCreate('Damping Value', mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)
        dampingValueProperty.Data = str(dampeningValue)

        integer = relationConstraint.AddFunctionBox( 'Number', 'Integer')
        integer.SetBoxPosition(-300, 0)
        integer.SetReceiverValueByName('a', int(dampeningValue))

        # populate relation constraint with scene items selected
        y = 0
        existingItemList = self._getListWidgetItems()
        for item in existingItemList:
            sceneItem = item.data(QtCore.Qt.UserRole)
            #sceneItem = self._itemList.itemData(item)
            if sceneItem is not None:
                sender = relationConstraint.AddSenderBox(sceneItem)
                sender.SetBoxPosition(0, y)

                receiver = relationConstraint.AddReceiverBox(sceneItem)
                receiver.SetBoxPosition(1000, y)

                damping = relationConstraint.AddFunctionBox('Other', 'Damping (3D)')
                damping.SetBoxPosition(600, y - 50)

                damping2 = relationConstraint.AddFunctionBox('Other', 'Damping (3D)')
                damping2.SetBoxPosition(600, y + 50)

                relationConstraint.ConnectBoxes(sender, 'Rotation', damping, 'P')
                relationConstraint.ConnectBoxes(damping, 'Result', receiver, 'Rotation')
                relationConstraint.ConnectBoxes(sender, 'Translation', damping2, 'P')
                relationConstraint.ConnectBoxes(damping2, 'Result', receiver, 'Translation')
                relationConstraint.ConnectBoxes(integer, 'Result', damping, 'Damping Factor')
                relationConstraint.ConnectBoxes(integer, 'Result', damping2, 'Damping Factor')

                y += 300
        relationConstraint.SetActive(True)

        # emit signal
        self.createSignal.emit()

        # reset the list, namespace and dampening value defaults
        self._itemList.clear()
        self._setConstraintNamespace()
        self._setConstraintPlaceholderName()
        self._createSpinBox.setValue(0)

    def _getListWidgetItemStrings(self):
        '''
        Gets a list of strings present in the list widget
        Returns:
            List [Strings]
        '''
        # get existing list of items (user may add more items to an existing list)
        existingItemList = []
        for idx in xrange(self._itemList.count()):
            existingItemList.append(str(self._itemList.item(idx).text()))
        return existingItemList

    def _getListWidgetItems(self):
        '''
        Gets a list of strings present in the list widget
        Returns:
            List [Strings]
        '''
        # get existing list of items (user may add more items to an existing list)
        existingItemList = []
        for idx in xrange(self._itemList.count()):
            existingItemList.append(self._itemList.item(idx))
        return existingItemList

    def _getCurrentSpinBoxValue(self):
        '''
        Get the current value of the spinbox

        Return:
            Int
        '''
        dampeningValue = int(self._createSpinBox.value())
        return dampeningValue

    def _getConstraintFullName(self):
        '''
        Get the current string of the line edit

        Return:
            String
        '''
        namespace = self._constraintNamespaceLabel.text()
        userEditName = str(self._constraintNameLineEdit.text())
        fullName = '{0}{1}{2}'.format(namespace, userEditName, self._constraintNameString)
        return fullName

    def _setConstraintNamespace(self):
        '''
        use the first item in the list to get the namespace for the constraint

        Return:
            String
        '''
        existingItemList = self._getListWidgetItemStrings()
        if not len(existingItemList) > 0:
            self._constraintNamespaceLabel.setText('[No Namespace Found]:')
            return
        # pick the first item in the list
        fullName = existingItemList[0]
        splitName = fullName.split(':')
        namespace = '{0}:'.format(splitName[0])
        self._constraintNamespaceLabel.setText(namespace)

    def _setConstraintPlaceholderName(self):
        '''
        Placeholder name for line edit box until the user edits it
        '''
        self._constraintNameLineEdit.setText('')
        self._constraintNameLineEdit.setPlaceholderText('[enter prefix]')

    def _setupTab(self):
        '''
        Setup of layouts, widgets and connects widgets to an action
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()

        # create widgets
        itemListLabel = QtGui.QLabel()
        itemListButton = QtGui.QPushButton()
        deleteItemListButton = QtGui.QPushButton()
        self._itemList = QtGui.QListWidget()
        spinBoxLabel = QtGui.QLabel()
        self._createSpinBox = QtGui.QSpinBox()
        constraintLabel = QtGui.QLabel()
        self._constraintNamespaceLabel = QtGui.QLabel()
        self._constraintNameLineEdit = QtGui.QLineEdit()
        constraintNameLabel = QtGui.QLabel()
        createButton = QtGui.QPushButton()
        spacerList = []
        for idx in range(4):
            spacerItem = QtGui.QSpacerItem(10, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
            spacerList.append(spacerItem)

        # configure widgets
        itemListLabel.setText('Select the null(s) you would like to create a dampening constraint for, from the scene:')
        itemListButton.setText('Add selected scene item(s) to the list')
        deleteItemListButton.setText('Remove selected list item(s)')
        self._itemList.setSortingEnabled(True)
        self._itemList.MultiSelect = True
        spinBoxLabel.setText('Set dampening value:')
        constraintLabel.setText('Set constraint namespace:')
        self._setConstraintNamespace()
        constraintNameLabel.setText(self._constraintNameString)
        self._setConstraintPlaceholderName()
        createButton.setText('Create Dampening Relation Constraint')

        # set widgets to layout
        mainLayout.addWidget(itemListLabel, 0, 0, 1, 3)
        mainLayout.addWidget(itemListButton, 1, 0, 1, 3)
        mainLayout.addItem(spacerList[0], 2, 0)
        mainLayout.addWidget(self._itemList, 3, 0, 1, 3)
        mainLayout.addWidget(deleteItemListButton, 4, 0, 1, 3)
        mainLayout.addItem(spacerList[1], 5, 0)
        mainLayout.addWidget(spinBoxLabel, 6, 0)
        mainLayout.addWidget(self._createSpinBox, 7, 0, 1, 3)
        mainLayout.addItem(spacerList[2], 8, 0)
        mainLayout.addWidget(constraintLabel, 9, 0, 1, 3)
        mainLayout.addWidget(self._constraintNamespaceLabel, 10, 0)
        mainLayout.addWidget(self._constraintNameLineEdit, 10, 1)
        mainLayout.addWidget(constraintNameLabel, 10, 2)
        mainLayout.addItem(spacerList[3], 11, 0)
        mainLayout.addWidget(createButton, 12, 0, 1, 3)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        itemListButton.released.connect(self._populateSelectedInTree)
        createButton.released.connect(self._createDampeningConstraint)
        deleteItemListButton.released.connect(self._removeListItems)
        


class EditDampeningWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(EditDampeningWidget, self).__init__(parent=parent)
        self._constraintListComboBox = None
        self._constraintActiveCheckBox = None
        self._editDampSpinBox = None
        self._setupTab()

    def _getDampeningConstraintDict(self):
        '''
        Find dampening constraints in the scene and add to a dictionary

        Returns:
            Dict[FBConstraintRelation] = String
        '''
        dampeningConstraintDict = {}
        for constraint in Globals.Constraints:
            dampeningProperty = constraint.PropertyList.Find('Damping Relation')
            if dampeningProperty is not None:
                dampeningConstraintDict[constraint] = constraint.LongName
        return dampeningConstraintDict

    def populateComboBox(self):
        '''
        Add constraint name strings to the combobox
        Also adds data to each item, which will be the FBConstraintRelation
        '''
        self._constraintListComboBox.clear()
        dampeningConstraintDict = self._getDampeningConstraintDict()
        for key, value in dampeningConstraintDict.iteritems():
            # adding items one at a time to assign user data to each one
            # the user data is the FBConstraintRelation object (key)
            self._constraintListComboBox.addItem(str(value), key)

    def _comboBoxIndexChanged(self):
        '''
        constraintListComboBox index has changed. We need to return which constraint object is 
        current and their current dampening value.
        We also need to set editDampSpinBox's current value as the current dampening value of the
        selected constraint.

        Returns:
                Dict[FBConstraintRelation] = Int
        '''
        constraint = self._getCurrentConstraintObject()
        if constraint is not None:
            # update default active state
            if constraint.Active == True:
                self._constraintActiveCheckBox.setCheckState(QtCore.Qt.CheckState.Checked)
            else:
                self._constraintActiveCheckBox.setCheckState(QtCore.Qt.CheckState.Unchecked)

            # update default dampening value
            valueProperty = constraint.PropertyList.Find('Damping Value')
            if valueProperty is not None:
                currentValue = int(valueProperty.Data)
                if isinstance(currentValue, int):
                    self._editDampSpinBox.setValue(currentValue)

    def _checkStateChanged(self):
        '''
        With the currently selected constraint, from constraintListComboBox, it allows the user to 
        activate or deactivate the constraint.
        '''
        constraint = self._getCurrentConstraintObject()
        if self._constraintActiveCheckBox.checkState() == QtCore.Qt.CheckState.Checked:
            constraint.Active = True
        else:
            constraint.Active = False

    def _updateDampeningValue(self):
        '''
        With the currently selected constraint, from constraintListComboBox, it allows the user to 
        change the int value passed as the dampening value.
        '''
        constraint = self._getCurrentConstraintObject()
        dampingValueProperty = constraint.PropertyList.Find('Damping Value')
        dampeningValue = int(self._editDampSpinBox.value())

        # iterate through boxes to get the integer box
        rcmConstraint = rcm.RelationShipConstraintManager(constraint)
        integerBoxList = rcmConstraint.GetBoxesByRegex("\A[iI]nteger ?[0-9]*\Z")
        if len(integerBoxList) == 1 and dampingValueProperty is not None:
            integerBoxList[0].SetReceiverValueByName('a', int(dampeningValue))
            dampingValueProperty.Data = str(dampeningValue)

    def _deleteConstraint(self):
        '''
        With the currently selected constraint, from constraintListComboBox, it allows the user to 
        delete this relation constraint.
        '''
        constraint = self._getCurrentConstraintObject()
        # double check the user wishes to remove the constraint
        result = QtGui.QMessageBox.warning(self,
                                             "",
                                            "Are you sure you wish to delete {0}?".format(constraint.LongName),
                                            QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if result == QtGui.QMessageBox.Yes:
            # delete constraint
            constraint.FBDelete()
            # reset values in UI
            self.populateComboBox()
            # reset Spinbox if the combobox is blank
            if self._constraintListComboBox.count() == 0:
                self._editDampSpinBox.setValue(0)

    def _getCurrentConstraintObject(self):
        '''
        Get the combobox's currently selected FBCOnstraintRelation object
        '''
        # get current index/constraint
        currentComboBoxIndex = self._constraintListComboBox.currentIndex()
        # using the user data, we can get the related constraint object to the current index
        constraint = self._constraintListComboBox.itemData(currentComboBoxIndex)
        return constraint

    def _setupTab(self):
        '''
        Setup of layouts, widgets and connects widgets to an action
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()

        # create widgets
        constraintListLabel = QtGui.QLabel()
        self._constraintListComboBox = QtGui.QComboBox()
        self._constraintActiveCheckBox = QtGui.QCheckBox()
        editDampLabel = QtGui.QLabel()
        self._editDampSpinBox = QtGui.QSpinBox()
        editDampButton = QtGui.QPushButton()
        constraintDeleteButton = QtGui.QPushButton()
        spacerItem = QtGui.QSpacerItem(10, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        spacerItem2 = QtGui.QSpacerItem(10, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)

        # configure widgets
        constraintListLabel.setText('Select constraint to edit:')
        self._constraintActiveCheckBox.setText('Relation Constraint Active')
        editDampLabel.setText('Adjust dampening value:')
        editDampButton.setText('Set New Value')
        constraintDeleteButton.setText('Delete Constraint')
        self.populateComboBox()
        self._comboBoxIndexChanged()

        # set widgets to layout
        mainLayout.addWidget(constraintListLabel, 0, 0)
        mainLayout.addWidget(self._constraintListComboBox, 0, 1, 1, 2)
        mainLayout.addWidget(self._constraintActiveCheckBox, 1, 0)
        mainLayout.addItem(spacerItem, 2, 0)
        mainLayout.addWidget(editDampLabel, 3, 0)
        mainLayout.addWidget(self._editDampSpinBox, 3, 1)
        mainLayout.addWidget(editDampButton, 3, 2)
        mainLayout.addItem(spacerItem2, 4, 0)
        mainLayout.addWidget(constraintDeleteButton, 5, 0, 1, 3)
        mainLayout.addItem(spacerItem3, 6, 0)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        self._constraintListComboBox.currentIndexChanged.connect(self._comboBoxIndexChanged)
        self._constraintActiveCheckBox.stateChanged.connect(self._checkStateChanged)
        editDampButton.released.connect(self._updateDampeningValue)
        constraintDeleteButton.released.connect(self._deleteConstraint)
