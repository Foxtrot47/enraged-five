from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Globals
from RS.Tools import UI
from RS.Tools.Dampening.widgets import tabWidget


class DampeningWidgetDialog(UI.QtMainWindowBase):
    '''
    Dialogue to run PropCharRootSnapWidget and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(DampeningWidgetDialog, self).__init__(parent=parent)
        self._settings = QtCore.QSettings( "RockstarSettings", "DampeningWidgetDialog" )
        dampeningWidget = DampeningWidget()

        # create layout and add widget
        self.setCentralWidget(dampeningWidget)

    def showEvent(self, event):
        '''
        Set previously saved widget size and position preferences
        '''
        super(DampeningWidgetDialog, self).showEvent(event)
        try:
            self.resize(self._settings.value("size", QtCore.QSize(800, 800)))
            self.move(self._settings.value("pos", QtCore.QPoint(800, 800)))
        except:
            pass

    def closeEvent(self, event):
        '''
        Save widget size and position preferences
        '''
        try:
            super(DampeningWidgetDialog, self).closeEvent(event)
            self._settings.setValue("size", self.size())
            self._settings.setValue("pos", self.pos())
        except:
            pass


class DampeningWidget(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(DampeningWidget, self).__init__(parent=parent)
        self._createTabClass = tabWidget.CreateDampeningWidget()
        self._editTabClass = tabWidget.EditDampeningWidget()
        self._tabs = None

        self._setupUi()

    def _handleCreateSignal(self):
        '''
        once the signal is caught it will run the 'populateComboBox' method from tabWidget.py
        populateComboBox:   Add constraint name strings to the combobox
                            Also adds data to each item, which will be the FBConstraintRelation
        '''
        self._editTabClass.populateComboBox()

    def _setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # deselect all
        mobu.FBBeginChangeAllModels()
        for component in Globals.Components:
            component.Selected = False
        mobu.FBEndChangeAllModels()

        # create layouts
        mainLayout = QtGui.QGridLayout()
        createTabLayout = QtGui.QGridLayout()
        editTabLayout = QtGui.QGridLayout()

        # create widgets
        self._tabs = QtGui.QTabWidget()
        createTab = QtGui.QWidget()
        self._editTab = QtGui.QWidget()

        # widget properties
        self._tabs.addTab(createTab, "Create Dampening")
        self._tabs.addTab(self._editTab, "Edit Dampening")

        # set widgets to layout
        createTabLayout.addWidget(self._createTabClass)
        editTabLayout.addWidget(self._editTabClass)
        mainLayout.addWidget(self._tabs)

        # set layout
        createTab.setLayout(createTabLayout)
        self._editTab.setLayout(editTabLayout)
        self.setLayout(mainLayout)

        # set connections
        self._createTabClass.createSignal.connect(self._handleCreateSignal)


