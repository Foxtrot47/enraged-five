import pyfbsdk as mobu
from RS import Globals

class PlayListMonitor(): 
    #Codebase from http://forums.autodesk.com/t5/motionbuilder-forum/time-changed-callback/m-p/4266320/highlight/true#M8744
    def __init__(self):
        self.previous_frame = 0
        self.current_frame = 0
        
        self.takeIdArray = []
        self.endFrames = []

        self.loopPlayist = False
        self.forceSceneRefresh = False 

        self.evaluationUtils = mobu.FBEvaluateManager()
        self.timePlayer = mobu.FBPlayerControl()
        self.currentTakePlayList = 0
        self.playlistTool = None

    def jumpToTargetTake(self, currentTake):
        self.stop()
        self.currentTakePlayList = int(currentTake)

        self.play()

    def setTakeArray(self, takeList, currentTake, playlistTool=None):
        self.currentTakePlayList = 0
        self.takeIdArray = takeList
        self.collectTakeEnd()
        self.currentTakePlayList = int(currentTake)
        self.playlistTool = playlistTool

    def synchronizeTimeline(self, control, event):
        self.current_frame = Globals.System.LocalTime.GetFrame()

        if self.current_frame != self.previous_frame:
            self.previous_frame = self.current_frame
            self.frameTrigger(self.current_frame)

    def collectTakeEnd(self):
        self.endFrames = []
        for takeData in self.takeIdArray:
            takeId = takeData.takeIndex 
            self.endFrames.append(Globals.System.Scene.Takes[takeId].LocalTimeSpan.GetStop().GetFrame()-1)
    
    def register(self):
        self.unregister()
        Globals.Callbacks.OnSynchronizationEvent.Add(self.synchronizeTimeline)
    
    def unregister(self):
        Globals.Callbacks.OnSynchronizationEvent.Remove(self.synchronizeTimeline)
    
    def frameTrigger(self, currentTime):
        if not self.timePlayer.IsPlaying:
            return

        if currentTime >= self.endFrames[self.currentTakePlayList]:
            loopTake = self.takeIdArray[self.currentTakePlayList].loopTake
            if loopTake is False:
                self.currentTakePlayList += 1

            if self.currentTakePlayList > (len(self.takeIdArray) - 1):
                if self.loopPlayist is False:
                    self.stop()

                    return

                self.currentTakePlayList = 0

            takeIndex = self.takeIdArray[self.currentTakePlayList].takeIndex
            Globals.System.CurrentTake = Globals.System.Scene.Takes[takeIndex]
            self.timePlayer.GotoStart()

            if self.forceSceneRefresh is True:
                Globals.System.Scene.Evaluate()

            if self.playlistTool is not None:
                self.playlistTool._setTakeIndex(self.currentTakePlayList)

            self.timePlayer.Play()

    def stop(self):
        self.unregister()

    def play(self):
        self.stop()
        takeIndex = self.takeIdArray[self.currentTakePlayList].takeIndex
        skipFrame = self.takeIdArray[self.currentTakePlayList].skipFrame

        nextFrame = Globals.System.Scene.Takes[takeIndex].LocalTimeSpan.GetStart().GetFrame()+1

        Globals.System.CurrentTake = Globals.System.Scene.Takes[takeIndex]

        if skipFrame is False:
            self.timePlayer.GotoStart()
        else:
            targetTime = mobu.FBTime()
            targetTime.SetFrame(nextFrame, self.timePlayer.GetTransportFps()) 

            self.timePlayer.Goto(targetTime)

        self.register()
        self.timePlayer.Play()
    
    def __del__(self):
        self.stop()