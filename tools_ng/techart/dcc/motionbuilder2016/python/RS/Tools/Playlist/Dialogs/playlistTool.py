import inspect
import os
from functools import partial

import pyfbsdk as mobu

from RS import Globals
from RS import Config
from RS.Tools import UI

from RS.Utils.Scene import Take
from RS.Tools.Playlist import Core as playlistCore
reload(playlistCore)


from PySide import QtGui, QtCore


class PlayListItemData(object):
    def __init__(self):
        self.skipFrame = False
        self.takeIndex = 0
        self.takeName = ''
        self.loopTake = False

    def extractWidgetParameters(self, inputWidget):
        self.skipFrame = inputWidget.skipFirstFrameWidget.isChecked()
        self.loopTake = inputWidget.loopButtonWidget.isChecked()
        self.takeName = str(inputWidget.itemLabel)


class PlaylistLabelWidget(QtGui.QWidget):
    def __init__(self, 
                 parent=None, 
                 widgetLabel='Source'):
        self.widgetLabel = widgetLabel
        super(PlaylistLabelWidget, self).__init__(parent=parent)
        self.setupUi() 

    def setupUi(self): 
        """
            Setup the Widget
        """
        self.setAcceptDrops(True)

        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(5)

        self.treeLabel = QtGui.QLabel(self.widgetLabel)
        layout.addWidget(self.treeLabel)

        self.setStyleSheet("QLabel {"
                           "font: normal 11px; "
                           "qproperty-alignment: AlignCenter;"
                           "}")

        self.setLayout(layout)


class PlayListItemManagerWidget(QtGui.QWidget):
    def __init__(self):
        super(PlayListItemManagerWidget, self).__init__(parent=None)
        self.setupUi() 

    def setupUi(self): 
        """
            Setup the Widget
        """
        toolPath = os.path.dirname(__file__)
        addIconPath = os.path.join(toolPath, 'add.png')
        deleteIconPath = os.path.join(toolPath, 'delete.png')

        addIcon = QtGui.QIcon()
        addIconPixmap = QtGui.QPixmap(addIconPath)

        addIcon.addPixmap(addIconPixmap,
                          QtGui.QIcon.Normal,
                          QtGui.QIcon.Off)

        deleteIcon = QtGui.QIcon()
        deleteIconPixmap = QtGui.QPixmap(deleteIconPath)

        deleteIcon.addPixmap(deleteIconPixmap,
                             QtGui.QIcon.Normal, 
                             QtGui.QIcon.Off)
        #PySide.QtGui.QIcon.Mode = Normal, PySide.QtGui.QIcon.State = Off)

        # Create Layouts
        self.itemButtonLayout = QtGui.QVBoxLayout()
        self.itemButtonWidget = QtGui.QWidget()

        # Create Widgets 
        self.addTakeButton = QtGui.QPushButton('')
        self.removeTakeButton = QtGui.QPushButton('')
        leftSpacer = QtGui.QSpacerItem(5, 5, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        rightSpacer = QtGui.QSpacerItem(5, 5, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)

        #Set properties
        self.itemButtonLayout.setContentsMargins(0, 0, 0, 0)
        buttonSize = 24
        targetSize = QtCore.QSize(buttonSize,
                                  buttonSize)

        self.addTakeButton.setFixedSize(targetSize)
        self.removeTakeButton.setFixedSize(targetSize)

        self.addTakeButton.setIcon(addIcon)
        self.removeTakeButton.setIcon(deleteIcon)

        self.addTakeButton.setFixedSize(targetSize)
        self.removeTakeButton.setFixedSize(targetSize)

        self.addTakeButton.setIconSize(QtCore.QSize(self.addTakeButton.width(), 
                                                    self.addTakeButton.height()))

        self.removeTakeButton.setIconSize(QtCore.QSize(self.removeTakeButton.width(), 
                                                       self.removeTakeButton.height()))

        # Add to Widgets 
        self.itemButtonLayout.addStretch()
        self.itemButtonLayout.addWidget(self.addTakeButton)
        self.itemButtonLayout.addWidget(self.removeTakeButton)
        self.itemButtonLayout.addStretch()

        # Assign Layouts to WidgetStack
        self.setLayout(self.itemButtonLayout)


class PlaylistTakeWidget(QtGui.QWidget):
    def __init__(self, 
                 parent=None, 
                 widgetLabel='Source',
                 addRefreshButton=True):
        self.widgetLabel = widgetLabel
        self.addRefreshButton = addRefreshButton

        super(PlaylistTakeWidget, self).__init__(parent=parent)
        #Data 
        self.takeDataModel = QtGui.QStringListModel()
        self.takeDataArray = []

        self.setupUi() 

    def setupUi(self): 
        """
            Setup the Widget
        """
        buttonSize = 19

        if self.addRefreshButton is True:
            iconFolderString = "{0}/ReferenceEditor/".format(Config.Script.Path.ToolImages)
            refreshLibraryIcon = QtGui.QIcon()
            refreshLibraryPixmap = QtGui.QPixmap("{0}UpdateReference.png".format(iconFolderString))

            refreshLibraryIcon.addPixmap(refreshLibraryPixmap,
                                         QtGui.QIcon.Normal, 
                                         QtGui.QIcon.Off)

            self.button = QtGui.QPushButton('')
            self.button.setIcon(refreshLibraryIcon);
            self.button.setIconSize(QtCore.QSize(14, 14))

            targetSize = QtCore.QSize(buttonSize,
                                      buttonSize)

            self.button.setFixedSize(targetSize)

        labelLayoutWidget = QtGui.QWidget()
        self.treeLabel = PlaylistLabelWidget(widgetLabel=self.widgetLabel)

        labelLayout = QtGui.QGridLayout()
        labelLayout.setContentsMargins(0, 0, 0, 0)
        labelLayout.setSpacing(1)

        labelLayoutWidget.setLayout(labelLayout)

        labelLayout.addWidget(self.treeLabel, 0, 0, QtCore.Qt.AlignLeft)

        if self.addRefreshButton is True:
            rightSpacer = QtGui.QSpacerItem(5,
                                            5,
                                            QtGui.QSizePolicy.Expanding,
                                            QtGui.QSizePolicy.Minimum)
            labelLayout.addItem(rightSpacer, 0, 1)
            labelLayout.addWidget(self.button, 0, 2, QtCore.Qt.AlignRight)

        else:
            rightSpacer = QtGui.QSpacerItem(5,
                                            5,
                                            QtGui.QSizePolicy.Expanding,
                                            QtGui.QSizePolicy.Minimum)
            labelLayout.addItem(rightSpacer, 0, 1)

            self.treeLabel.setMinimumHeight(buttonSize)

        self.layout = QtGui.QVBoxLayout()
        if self.addRefreshButton is True:
            self.takeArrayWidget = QtGui.QListView()

            self.takeArrayWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

            self.takeArrayWidget.setEditTriggers(0)

            self.takeArrayWidget.setModel(self.takeDataModel)
            self._populateTakeDataModel()

            # callbacks
            self.button.pressed.connect(self._populateTakeDataModel)
        else:
            self.takeArrayWidget = QtGui.QListWidget()
            self.takeArrayWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
            self.takeArrayWidget.setEditTriggers(0)

        self.layout.addWidget(labelLayoutWidget)
        self.layout.addWidget(self.takeArrayWidget)

        self.layout.setContentsMargins(5, 0, 0, 5)
        self.layout.setSpacing(5)

        self.setLayout(self.layout)

    def _populateTakeDataModel(self):
        """
        Internal Method
        Fill TakeDropDown menu
        """
        globalSceneTakeList = Take.GetTakeList()
       
        #Now add Items
        takeList = [take.Name for take in globalSceneTakeList]
        self.takeDataModel.setStringList(takeList)

    def flush(self):
        if self.takeArrayWidget.count() >0:
            self.layout.removeWidget(self.takeArrayWidget)
            self.takeArrayWidget.parent = None
            self.takeArrayWidget.clear()
            self.takeArrayWidget = None
            self.takeArrayWidget = QtGui.QListWidget()
            self.takeArrayWidget.setEditTriggers(0)

            self.layout.addWidget(self.takeArrayWidget)

        self.takeArrayWidget.setFocus()

    def _populateTakeWidgets(self):
        self.flush()

        for item in self.takeDataModel.stringList():
            takeComponent = QtGui.QListWidgetItem()
            takeComponent.setSizeHint(QtCore.QSize(100, 40))
            takeData = PlayListItemWidget(item)

            self.takeArrayWidget.addItem(takeComponent)
            self.takeArrayWidget.setItemWidget(takeComponent, takeData)

        self.takeArrayWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)


class PlaylistScrubControlWidget(QtGui.QWidget):
    ICON_NAMES = ('previous.png', 'stop.png', 'play.png', 'next.png')

    BUTTON_ATTRIBUTES = ('previousTakeWidget',
                         'stopPlayerWidget',
                         'startPlayerWidget',
                         'nextTakeWidget')
    def __init__(self):
        super(PlaylistScrubControlWidget, self).__init__(parent=None)
        self.previousTakeWidget = None
        self.stopPlayerWidget = None
        self.startPlayerWidget = None
        self.nextTakeWidget = None

        self.setupUi() 

    def setupUi(self): 
        """
            Setup the Widget
        """
        toolPath = os.path.dirname(__file__)

        
        buttonSize = 32
        targetSize = QtCore.QSize(buttonSize,
                                  buttonSize)

        layout = QtGui.QHBoxLayout()

        for iconIndex, iconName in enumerate(self.ICON_NAMES):
            iconPath = os.path.join(toolPath, iconName)

            icon = QtGui.QIcon()
            iconPixmap = QtGui.QPixmap(iconPath)

            icon.addPixmap(iconPixmap,
                           QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)

            buttonWidget = QtGui.QPushButton('')
            buttonWidget.setFixedSize(targetSize)

            buttonWidget.setIcon(icon)
            buttonWidget.setIconSize(QtCore.QSize(buttonSize, 
                                                  buttonSize))

            layout.addWidget(buttonWidget)
            setattr(self, self.BUTTON_ATTRIBUTES[iconIndex], buttonWidget)

        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(5)

        self.setLayout(layout)


class PlayListItemWidget(QtGui.QWidget):
    def __init__(self, itemLabel='Take 001'):
        super(PlayListItemWidget, self).__init__(parent=None)

        self.itemLabel = itemLabel
        self.setupUi() 

    def setupUi(self): 
        """
            Setup the Widget
        """
        toolPath = os.path.dirname(__file__)
        loopOffIconPath = os.path.join(toolPath, 'loopOff.png')
        loopOnIconPath = os.path.join(toolPath, 'loopOn.png')

        loopIcon = QtGui.QIcon()
        loopOffIconPixmap = QtGui.QPixmap(loopOffIconPath)
        loopOnIconPixmap = QtGui.QPixmap(loopOnIconPath)

        loopIcon.addPixmap(loopOffIconPixmap,
                           QtGui.QIcon.Normal,
                           QtGui.QIcon.Off)

        loopIcon.addPixmap(loopOnIconPixmap,
                           QtGui.QIcon.Normal,
                           QtGui.QIcon.On)

        layout = QtGui.QHBoxLayout()
        labelLayout = QtGui.QVBoxLayout()
        labelLayout.setContentsMargins(0, 0, 0, 0)
        labelLayout.setSpacing(0)

        labelWidget = QtGui.QWidget()
        labelWidget.setLayout(labelLayout)

        self.takeLabelWidget = QtGui.QLabel(self.itemLabel)
        self.takeLabelWidget.setMaximumHeight(18)

        labelLayout.addWidget(self.takeLabelWidget)
        labelLayout.addWidget(self._createSeparator())

        self.skipFirstFrameWidget = QtGui.QPushButton('Skp*')
        self.loopButtonWidget = QtGui.QPushButton('')

        layout.setContentsMargins(5, 5, 5, 5)
        layout.setSpacing(5)

        buttonSize = 24
        targetSize = QtCore.QSize(38, buttonSize)

        self.skipFirstFrameWidget.setMaximumWidth(38)
        self.loopButtonWidget.setMaximumWidth(38)
        self.loopButtonWidget.setIcon(loopIcon)
        self.loopButtonWidget.setFixedSize(targetSize)

        self.loopButtonWidget.setCheckable(True)
        self.loopButtonWidget.setChecked(False)

        self.skipFirstFrameWidget.setCheckable(True)
        self.skipFirstFrameWidget.setChecked(False)
        self.skipFirstFrameWidget.setStyleSheet("QPushButton:checked{background-color: black}")


        self.loopButtonWidget.setIconSize(QtCore.QSize(self.loopButtonWidget.width(), 
                                                       self.loopButtonWidget.height()))

        layout.addWidget(self.skipFirstFrameWidget)
        layout.addWidget(labelWidget)
        layout.addWidget(self.loopButtonWidget)

        self.setLayout(layout)

    def _createSeparator(self):
        """
        Internal Method

        add line separator between widget
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine


class PlaylistTool(UI.QtMainWindowBase):
    """
        main Tool UI
    """
    def __init__(self):
        """
            Constructor
        """
        self._toolName = "Playlist Tool"

        widgetList = QtGui.qApp.topLevelWidgets() 
        for eachTool in widgetList:
            if hasattr(eachTool, "windowTitle") and eachTool.windowTitle() == self._toolName:
                eachTool.deleteLater()

        # Main window settings
        super(PlaylistTool, self).__init__(title=self._toolName, dockable=False, size=(600,620))

        self.playListUtils = playlistCore.PlayListMonitor()
        self.setupUi()

    def showEvent(self, event):
        self.restoreGeometry(self._settings.value("geometry"))

    def closeEvent(self, event):
        self._settings.setValue("geometry", self.saveGeometry())
        self._settings.setValue("size", self.size())

        self.playListUtils.stop()

        return super(UI.QtMainWindowBase, self).closeEvent(event)

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        self.mainLayout = QtGui.QVBoxLayout()
        self.takeAnchorLayout = QtGui.QHBoxLayout()

        # Create Widgets 
        banner = UI.BannerWidget(self._toolName)#, helpUrl='https://hub.rockstargames.com/display/RSGTECHART/Hand+Pose+Tool')
        self.mainWidget = QtGui.QWidget()

        self.takeAnchorWidget = QtGui.QWidget()
        self.sceneTakeArrayWidget = PlaylistTakeWidget(widgetLabel='Source')
        self.reviewTakesWidget = PlaylistTakeWidget(widgetLabel='Review', addRefreshButton=False)
        self.itemManagerWidget = PlayListItemManagerWidget()
        self.scrubControls = PlaylistScrubControlWidget()

        #Set properties
        self.takeAnchorLayout.setContentsMargins(0, 8, 0, 8)

        # Add to Widgets 
        self.mainLayout.addWidget(banner)
        self.mainLayout.addWidget(self.takeAnchorWidget)
        self.mainLayout.addWidget(self.scrubControls)

        self.takeAnchorLayout.addWidget(self.sceneTakeArrayWidget)
        self.takeAnchorLayout.addWidget(self.itemManagerWidget)
        self.takeAnchorLayout.addWidget(self.reviewTakesWidget)

        # Assign Layouts to WidgetStack
        self.mainWidget.setLayout(self.mainLayout)
        self.takeAnchorWidget.setLayout(self.takeAnchorLayout)

        self.setCentralWidget(self.mainWidget)

        # callbacks
        self.itemManagerWidget.addTakeButton.pressed.connect(self._addTakeToList)
        self.itemManagerWidget.removeTakeButton.pressed.connect(self._removeTakeFromList)
        self.scrubControls.startPlayerWidget.pressed.connect(self._executeTakeList)
        self.scrubControls.stopPlayerWidget.pressed.connect(self._stopListPlayback)
        self.scrubControls.nextTakeWidget.pressed.connect(self._playNextTake)
        self.scrubControls.previousTakeWidget.pressed.connect(self._playPreviousTake)

    def _addTakeToList(self):
        sceneSelectionModel = self.sceneTakeArrayWidget.takeArrayWidget.selectionModel()
        currentTakeToAdd = [index.row() for index in sceneSelectionModel.selectedIndexes()]

        if len(currentTakeToAdd) == 0:
            return

        currentSceneTakeNames = self.sceneTakeArrayWidget.takeDataModel.stringList()
        reviewTakeNames = self.reviewTakesWidget.takeDataModel.stringList()

        for takeDataIndex in currentTakeToAdd:
            reviewTakeNames.append(currentSceneTakeNames[takeDataIndex])

        self.reviewTakesWidget.takeDataModel.setStringList(reviewTakeNames)

        self.reviewTakesWidget._populateTakeWidgets()

    def _removeTakeFromList(self):
        reviewSelectionModel = self.reviewTakesWidget.takeArrayWidget.selectionModel()
        currentTakeToRemove = [index.row() for index in reviewSelectionModel.selectedIndexes()]

        if len(currentTakeToRemove) == 0:
            return

        reviewTakeNames = self.reviewTakesWidget.takeDataModel.stringList()

        currentTakeToRemove = sorted(currentTakeToRemove)
        currentTakeToRemove.reverse()

        for takeDataIndex in currentTakeToRemove:
            reviewTakeNames.pop(takeDataIndex)

        self.reviewTakesWidget.takeDataModel.setStringList(reviewTakeNames)
        self.reviewTakesWidget._populateTakeWidgets()

    def _getTakeIndex(self):
        reviewSelectionModel = self.reviewTakesWidget.takeArrayWidget.selectionModel()
        currentTake = [index.row() for index in reviewSelectionModel.selectedIndexes()]
        
        currentTakePlayList = 0
        if len(currentTake) > 0:
            currentTakePlayList = currentTake[0]

        return currentTakePlayList

    def _setTakeIndex(self, targetIndex):
        selectionIndex = self.reviewTakesWidget.takeDataModel.index(targetIndex, 0)
        self.reviewTakesWidget.takeArrayWidget.clearSelection()
    
        reviewSelectionModel = self.reviewTakesWidget.takeArrayWidget.selectionModel()
        reviewSelectionModel.setCurrentIndex(selectionIndex ,QtGui.QItemSelectionModel.Select)

    def _playNextTake(self):
        reviewTakeIds = self._fillTakeList()

        if len(reviewTakeIds) == 0:
            self.playListUtils.stop()
            return

        currentTakePlayList = self._getTakeIndex()
        if len(self.playListUtils.takeIdArray) == 0:
            self.playListUtils.setTakeArray(reviewTakeIds, currentTakePlayList)

        currentTakePlayList += 1
        if currentTakePlayList > (len(self.playListUtils.takeIdArray) - 1):
            currentTakePlayList = 0

        self._setTakeIndex(currentTakePlayList)
        self.playListUtils.jumpToTargetTake(currentTakePlayList)

    def _playPreviousTake(self):
        reviewTakeIds = self._fillTakeList()

        if len(reviewTakeIds) == 0:
            self.playListUtils.stop()
            return

        currentTakePlayList = self._getTakeIndex()
        if len(self.playListUtils.takeIdArray) == 0:
            self.playListUtils.setTakeArray(reviewTakeIds, currentTakePlayList)

        currentTakePlayList -= 1
        if currentTakePlayList < 0:
            currentTakePlayList = (len(self.playListUtils.takeIdArray) - 1)

        self._setTakeIndex(currentTakePlayList)
        self.playListUtils.jumpToTargetTake(currentTakePlayList)

    def _fillTakeList(self):
        currentSceneTakeNames = self.sceneTakeArrayWidget.takeDataModel.stringList()
        reviewTakeNames = self.reviewTakesWidget.takeDataModel.stringList()

        reviewTakeIds = []
        for takeIndex, take in enumerate(reviewTakeNames):
            takeData = PlayListItemData()
            takeData.takeIndex = currentSceneTakeNames.index(take)

            takeDataItem = self.reviewTakesWidget.takeArrayWidget.item(takeIndex)
            takeDataWidget = self.reviewTakesWidget.takeArrayWidget.itemWidget(takeDataItem)

            takeData.extractWidgetParameters(takeDataWidget)
            reviewTakeIds.append(takeData)

        if len(reviewTakeIds) == 0:
            self.playListUtils.stop()
            return []

        return reviewTakeIds

    def _executeTakeList(self):
        reviewTakeIds = self._fillTakeList()

        if len(reviewTakeIds) == 0:
            self.playListUtils.stop()
            return

        currentTakePlayList = self._getTakeIndex()
        self._setTakeIndex(currentTakePlayList)

        self.playListUtils.setTakeArray(reviewTakeIds, 
                                        currentTakePlayList,
                                        playlistTool=self)
        self.playListUtils.play()

    def _stopListPlayback(self):
        self.playListUtils.timePlayer.Stop()
