from RS.Tools.Playlist.Dialogs import playlistTool
reload(playlistTool)

def Run(show=True):
    tool = playlistTool.PlaylistTool()

    if show: 
        tool.show()

    return tool
