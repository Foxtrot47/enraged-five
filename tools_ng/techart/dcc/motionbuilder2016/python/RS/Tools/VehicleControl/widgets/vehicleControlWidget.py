from PySide import QtGui, QtCore

import os

from functools import partial

import xml.dom.minidom as minidom

import pyfbsdk as mobu

from RS import Config, Globals, Perforce
from RS.Tools import UI
from RS.Utils import Scene
from RS.Utils.Scene import Property
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Vehicle import Vehicle
from RS.Core.ReferenceSystem.Types.Prop import Prop


class VehicleControlWidgetDialog(UI.QtMainWindowBase):
    '''
    Dialogue to run VehicleControlWidget and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=None):
        self._settings = QtCore.QSettings("RockstarSettings", "VehicleControlWidgetDialog")
        super(VehicleControlWidgetDialog, self).__init__(title="Vehicle Controls", parent=parent)
        vehicleControlWidget = VehicleControlWidget()
        self.setCentralWidget(vehicleControlWidget)

    def showEvent(self, event):
        self.restoreGeometry(self._settings.value("geometry"))

    def closeEvent(self, event):
        self._settings.setValue("geometry", self.saveGeometry())


class VehicleImageWidget(QtGui.QWidget):
    '''
    Class to generate images for the vehicles as a separate widget
    '''
    def __init__(self, vehicleName=None, parent=None):
        '''
        Constructor
        '''
        super(VehicleImageWidget, self).__init__(parent=parent)
        self.vehicleImage = QtGui.QImage()
        if vehicleName is not None:
            self.setImage(vehicleName)

        # for code use only - click on image to get position to place buttons with
        self._debugPrint = False

    def setImage(self, vehicleName):
        myPath = os.path.normpath(os.path.join(Config.Script.Path.ToolImages,
                                                    "VehicleControls",
                                                    vehicleName))
        self.vehicleImage = QtGui.QImage(myPath)

    def mousePressEvent(self, event):
        # getting the position of where you have clicked the left mouse button
        # (for populating the button positions)
        if event.button() == QtCore.Qt.MouseButton.LeftButton and self._debugPrint == True:
            UI.QPrint(event.pos())

    def paintEvent(self, event):
        painter = QtGui.QPainter()
        painter.begin(self)
        painter.drawImage(event.rect(), self.vehicleImage)
        painter.end()



class VehicleButtonWidget(VehicleImageWidget):
    buttonPressedSignal = QtCore.Signal(object)
    def __init__(self, vehicleXML=None, parent=None):
        super(VehicleButtonWidget, self).__init__(vehicleName=vehicleXML, parent=parent)

        self.buttonList = []

        if vehicleXML is not None:
            # parse xml of selected type and create buttons
            self._createButtons(vehicleXML)

    def setVehicleXML(self, vehicleXML):
        self._createButtons(vehicleXML)
        self.update()

    def _emitButtonSignal(self, button):
        # change button colour
        if button.styleSheet() == ("background-color: rgb(98,98,98);"):
            button.setStyleSheet("background-color: rgb(4,114,35);")
        else:
            button.setStyleSheet("background-color: rgb(98,98,98);")
        # if control is NOT held down at the time of click, we only want the currently clicked
        # button to be selected
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers != QtCore.Qt.ControlModifier:
            for b in self.buttonList:
                if b != button:
                    b.setStyleSheet("background-color: rgb(98,98,98);")
        # emit signal for button pressed
        self.buttonPressedSignal.emit(button)

    def _createButtons(self, xmlFile):
        # clear existing button list
        for button in [but for but in self.buttonList]:
            button.setParent(None)
            button.deleteLater()
            del button
        self.buttonList = []

        # parse file
        xmlFile = minidom.parse(xmlFile)
        rootData = xmlFile.getElementsByTagName("VehicleUISetup")
        buttonData =  rootData[0].getElementsByTagName("ButtonName")
        mainImageName =  rootData[0].getElementsByTagName("VehcileImageName")
        posHData = rootData[0].getElementsByTagName("PosH")
        posVData = rootData[0].getElementsByTagName("PosV")
        imageData = rootData[0].getElementsByTagName("ButtonImageName")
        self.setImage(mainImageName[0].childNodes[0].data)
        # create buttons
        for idx in range(len(buttonData)):
            buttonName = str(buttonData[idx].childNodes[0].data)
            positionH = int(posHData[idx].childNodes[0].data)
            positionV = int(posVData[idx].childNodes[0].data)
            imageFileName = str(imageData[idx].childNodes[0].data)

            self.imagePath = QtGui.QImage(os.path.join(Config.Script.Path.ToolImages,
                                                                        "VehicleControls",
                                                                        imageFileName))
            button = QtGui.QPushButton(parent=self)
            button.setIcon(QtGui.QIcon(QtGui.QPixmap(self.imagePath)))
            button.setStyleSheet("background-color: rgb(98,98,98);")
            button.setToolTip(buttonName)
            button.setGeometry(positionH-5, positionV-5, 16, 16)
            button.released.connect(partial(self._emitButtonSignal, button))
            self.buttonList.append(button)

        for but in self.buttonList:
            but.show()

    def _getButtonList(self):
        return self.buttonList


class VehicleControlWidget(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(VehicleControlWidget, self).__init__(parent=parent)
        self.manager = Manager()
        self.keyImage = QtGui.QImage(os.path.join(Config.Script.Path.ToolImages,
                                                    "Camera",
                                                    "icons",
                                                    "key.png"))
        self._propertyMonitor = Property.PropertyMonitor()
        self._keyBoolean = True
        self.vehicleImageWidget = None
        self._allButtonList = []

        self.setupUi()

    def _getVehicleList(self):
        '''
        Get list of characters in the scene
        Returns:
            List (Str) - namespaces of the characters in the scene
        '''
        vehicleNameList = []
        vehicles = self.manager.GetReferenceListByType(Vehicle)
        for vehicle in vehicles:
            vehicleNameList.append(vehicle.Namespace)
        return vehicleNameList

    def _getAccessoriesList(self):
        '''
        Get list of vehicle accessory controllers in the scene
        Returns:
            List (Str) - namespaces of the controllers in the scene
        '''
        accsList = []
        currentVehicleNamespace = self.vehicleCombo.currentText()
        if currentVehicleNamespace != "None":
            for component in Globals.Components:
                if component.LongName.startswith('{0}:acs_Controller_'.format(currentVehicleNamespace)) and isinstance(component, mobu.FBModel):
                    accsList.append(component.Name)
        accsList.sort()
        return accsList

    def _getMultiRefDict(self):
        multiRefDict = {}
        constraint = None
        currentVehicleNamespace = self.vehicleCombo.currentText()
        if currentVehicleNamespace != "None":
            for constraint in Globals.Constraints:
                if constraint.LongName.startswith('{0}:Multi Referential'.format(currentVehicleNamespace)):
                    childName = constraint.ReferenceGet(0)
                    for idx in range(constraint.ReferenceGetCount(1)):
                        parentName =  constraint.ReferenceGet(1, idx)
                        multiRefDict[idx+1] = parentName
                    multiRefDict.update({0: childName})
                    break
        if constraint:
            # add a new instance of the property monitor for the multi ref constraint
            activeRefProperty = constraint.PropertyList.Find('Active Reference')
            if activeRefProperty:
                self._propertyMonitor.Add(activeRefProperty, self._handleActiveReferenceProperty)
        return multiRefDict, constraint

    def _handleActiveReferenceProperty(self, targetProperty, previousValue):
        scenePropertyIdx = targetProperty.Data
        self._keyBoolean = False
        self.multiRefCombo.setCurrentIndex(scenePropertyIdx)
        self._keyBoolean = True


    def _getExtrasList(self):
        '''
        Get list of vehicle extra bones in the scene
        Returns:
            List (Str) - namespaces of the bones in the scene
        '''
        extrasList = []
        currentVehicleNamespace = self.vehicleCombo.currentText()
        if currentVehicleNamespace != "None":
            for component in Globals.Components:
                if component.LongName.startswith('{0}:extra_'.format(currentVehicleNamespace)) and isinstance(component, mobu.FBModel):
                    extrasList.append(component.Name)
        extrasList.sort()
        return extrasList

    def _populateVehicleComboBox(self):
        '''
        Populate the comboboxes with strings
        '''
        # remove existing items
        self.vehicleCombo.clear()
        # vehicle name list
        vehicleList = self._getVehicleList()
        if len(vehicleList) > 0:
            self.vehicleCombo.addItems(vehicleList)
        else:
            self.vehicleCombo.addItem("None")

    def _populateVehicleTypeComboBox(self):
        '''
        Populate the comboboxes with strings
        '''
        # remove existing items
        self.vehicleTypeCombo.clear()
        # get xml path for category
        xmlDir = os.path.join(Config.Tool.Path.TechArt,
                                    "dcc",
                                    "motionbuilder2014",
                                    "python",
                                    "RS",
                                    "Tools",
                                    "VehicleControl",
                                    "data")
        fileData = [(os.path.splitext(name)[0], os.path.join(xmlDir, name)) for name in os.listdir(xmlDir) if name.endswith(".xml")]
        # vehicle xml name list
        for prettyName, fullPath in fileData:
            if prettyName == 'categories':
                # categories isnt a vehicle type, but the file that holds the relationship between
                # the vehicle name and its type
                continue
            self.vehicleTypeCombo.addItem(prettyName, fullPath)

        # update category type based on current vehicle name
        self._setVehicleTypeComboBox()

    def _populateMultiRefComboBox(self):
        '''
        Populate the comboboxes with strings
        '''
        # clear combo
        self.multiRefCombo.clear()
        # populate
        multiRefDict, constraint = self._getMultiRefDict()
        if len(multiRefDict) > 0:
            for ref in multiRefDict.itervalues():
                self.multiRefCombo.addItem(ref.LongName)
            self.multiRefCombo.setEnabled(True)
            self.keyButton.setEnabled(True)
        else:
            self.multiRefCombo.addItem("None")
            self.multiRefCombo.setEnabled(False)
            self.keyButton.setEnabled(False)

    def _populateOtherComboBoxes(self):
        # clear combos
        self.accCombo.clear()
        self.extraCombo.clear()
        # accs list
        accsList = self._getAccessoriesList()
        self.accCombo.addItem("No Accessory Selected")
        if len(accsList) > 0:
            self.accCombo.addItems(accsList)
        # extras list
        extrasList = self._getExtrasList()
        if len(extrasList) > 0:
            self.extraCombo.addItems(extrasList)
            self.extraCombo.setEnabled(True)
            self.extraHideCheckBox.setEnabled(True)
        else:
            self.extraCombo.addItem("No Extra Bones in the scene")
            self.extraCombo.setEnabled(False)
            self.extraHideCheckBox.setEnabled(False)

    def _setVehicleTypeComboBox(self):
        # current vehicle name
        currentVehicleNamespace = str(self.vehicleCombo.currentText())
        splitName = currentVehicleNamespace.split('^')
        currentVehicleNamespace = splitName[0]
        #self.vehicleTypeCombo
        typeItemList = [str(self.vehicleTypeCombo.itemText(i)) for i in range(self.vehicleTypeCombo.count())]
        # get xml path for category
        xmlFile = os.path.join(Config.Tool.Path.TechArt,
                                "dcc",
                                "motionbuilder",
                                "python",
                                "RS",
                                "Tools",
                                "VehicleControl",
                                "data",
                                "categories.xml")
        # get latest on the file
        Perforce.Sync(xmlFile, force=True)
        if os.path.exists(xmlFile):
            # go through xml to find the vehicle name and type
            # parse file
            xmlFile = minidom.parse(xmlFile)
            rootData = xmlFile.getElementsByTagName("VehicleUISetup")
            itemData = rootData[0].getElementsByTagName("Item")
            vehicleName = rootData[0].getElementsByTagName("VehicleName")
            vehicleType = rootData[0].getElementsByTagName("Type")
            # set current index
            foundTypeMatch = False
            for idx in range(len(itemData)):
                nameItem = str(vehicleName[idx].childNodes[0].data)
                typeItem = str(vehicleType[idx].childNodes[0].data)
                if currentVehicleNamespace == nameItem:
                    for itemIdx in range(len(typeItemList)):
                        if typeItem == typeItemList[itemIdx]:
                            self.vehicleTypeCombo.setCurrentIndex(itemIdx)
                            foundTypeMatch = True
                            break
            if foundTypeMatch is False and currentVehicleNamespace not in ['None', '']:
                for itemIdx in range(len(typeItemList)):
                    if 'wagon' == typeItemList[itemIdx]:
                        self.vehicleTypeCombo.setCurrentIndex(itemIdx)
                        QtGui.QMessageBox.warning(None,
                                                  "Vehicle Type Warning",
                                                  "Cannot find a Vehicle Type match for vehicle: {0}, please contact\nFelipe Busquets to sort this.\n\nUsing 'wagon' Type as default for now.".format(currentVehicleNamespace),
                                                  QtGui.QMessageBox.Ok)
                        break

            # set type image
            self._vehicleTypeComboIndexChanged()

    def _getCurrentVehicleNamespace(self):
        return self.vehicleCombo.currentText()

    def _getCurrentExtraBoneNamespace(self):
        return self.extraCombo.currentText()

    def _vehicleComboIndexChanged(self):
        # de select all controllers
        Scene.DeSelectAll()
        # repopulate comboboxes
        self._populateOtherComboBoxes()
        self._populateMultiRefComboBox()
        self._handleAutoRotationStateChange()
        self._setVehicleTypeComboBox()

    def _vehicleTypeComboIndexChanged(self):
        # de select all controllers
        Scene.DeSelectAll()
        # change vehicle image
        idx = self.vehicleTypeCombo.currentIndex()
        vehicleTypePath = self.vehicleTypeCombo.itemData(idx)
        if vehicleTypePath is None:
            vehicleTypePath = self.vehicleTypeCombo.itemData(0)
        self.vehicleImageWidget.setVehicleXML(vehicleTypePath)
        self._allButtonList = self.vehicleImageWidget._getButtonList()

    def _setExtraCheckBoxState(self):
        currentVehicleNamespace = self._getCurrentVehicleNamespace()
        currentExtraBoneNamespace = self._getCurrentExtraBoneNamespace()
        extraBoneComponent = None
        if currentVehicleNamespace and currentExtraBoneNamespace:
            for component in Globals.Components:
                if component.LongName == '{0}:{1}'.format(currentVehicleNamespace, currentExtraBoneNamespace):
                    extraBoneComponent = component
                    break
        if extraBoneComponent:
            scalingVector = mobu.FBVector3d()
            extraBoneComponent.GetVector(scalingVector, mobu.FBModelTransformationType.kModelScaling)
            if scalingVector == mobu.FBVector3d(1,1,1):
                self.extraHideCheckBox.setCheckState(QtCore.Qt.CheckState.Checked)
            else:
                self.extraHideCheckBox.setCheckState(QtCore.Qt.CheckState.Unchecked)

    def _setConstraintKey(self):
        # multiRefCombo current text
        refIndex = self.multiRefCombo.currentIndex()
        # get multi ref constraint
        _, multiRefConstraint = self._getMultiRefDict()
        if multiRefConstraint and refIndex !=None:
            activeRefProperty = multiRefConstraint.PropertyList.Find('Active Reference')
            parentObject = multiRefConstraint.ReferenceGet(1)
            if activeRefProperty is not None and parentObject is not None:
                # get current frame
                currentFrame = mobu.FBSystem().LocalTime.GetFrame()
                # get fcurve
                activeRefProperty.SetAnimated(True)
                fCurve = activeRefProperty.GetAnimationNode().FCurve
                # add key
                activeRefProperty.GetAnimationNode().SetCandidate([refIndex])
                activeRefProperty.GetAnimationNode().KeyCandidate(mobu.FBTime(0, 0, 0, currentFrame))
                for key in fCurve.Keys:
                    key.Interpolation = mobu.FBInterpolation.kFBInterpolationConstant
                    key.FBTangentMode = mobu.FBTangentMode.kFBTangentModeUser

    def _handleMultiRefIndexChanged(self):
        if self._keyBoolean:
            self._setConstraintKey()

    def SelectVehicleController(self, button):
        currentVehicleNamespace = self._getCurrentVehicleNamespace()
        controllerName = button.toolTip()
        if currentVehicleNamespace != "None":
            controller = Scene.FindModelByName('{0}:{1}'.format(currentVehicleNamespace, controllerName), True)

            if controller is None and button.styleSheet() != ("background-color: rgb(98,98,98);"):
                QtGui.QMessageBox.warning(None,
                                          "Vehicle Select Warning",
                                          "Cannot find [{0}] for vehicle: [{0}].\n\Controller has not been selected".format(controllerName, currentVehicleNamespace),
                                          QtGui.QMessageBox.Ok)
                return
            if controller is None and button.styleSheet() == ("background-color: rgb(98,98,98);"):
                return

            if button.styleSheet() == ("background-color: rgb(98,98,98);"):
                controller.Selected = False
            else:
                controller.Selected = True
            # if control is NOT held down at the time of click, we only want the currently
            # clicked button to be selected
            modifiers = QtGui.QApplication.keyboardModifiers()
            if modifiers != QtCore.Qt.ControlModifier:
                controlBoneList = self._getControllerList()
                for controlBone in controlBoneList:
                    if controlBone != controller:
                        controlBone.Selected = False

    def SelectAccessoryController(self):
        accsList = self._getAccessoriesList()
        currentVehicleNamespace = self._getCurrentVehicleNamespace()
        accIdx = self.accCombo.currentIndex()
        accName = self.accCombo.currentText()
        # deselect all acc
        for acc in accsList:
            controller = Scene.FindModelByName('{0}:{1}'.format(currentVehicleNamespace, acc), True)
            if controller is not None:
                controller.Selected = False
        if currentVehicleNamespace != "None":
            # 0 = no accessories selected option
            if accIdx == 0:
                return
            # select the controller
            controller = Scene.FindModelByName('{0}:{1}'.format(currentVehicleNamespace, accName), True)
            if controller:
                controller.Selected = True

    def TurnPivotToTurnPivotRef(self):
        currentVehicleNamespace = self._getCurrentVehicleNamespace()
        if currentVehicleNamespace:
            turnPivot = Scene.FindModelByName('{0}:Turn Pivot'.format(currentVehicleNamespace), True)
            turnPivotRef = Scene.FindModelByName('{0}:TurnPivot_ref'.format(currentVehicleNamespace), True)
            if turnPivot and turnPivotRef:
                Scene.Align(turnPivot, turnPivotRef, True, True, True, False, False, False)

    def HideExtraBone(self):
        currentVehicleNamespace = self._getCurrentVehicleNamespace()
        currentExtraBoneNamespace = self._getCurrentExtraBoneNamespace()
        extraBoneComponent = None
        if currentVehicleNamespace and currentExtraBoneNamespace:
            for component in Globals.Components:
                if component.LongName == '{0}:{1}'.format(currentVehicleNamespace, currentExtraBoneNamespace):
                    extraBoneComponent = component
                    break
        if extraBoneComponent:
            if self.extraHideCheckBox.checkState() == QtCore.Qt.CheckState.Checked:
                extraBoneComponent.SetVector(mobu.FBVector3d(1, 1, 1), mobu.FBModelTransformationType.kModelScaling)
            else:
                extraBoneComponent.SetVector(mobu.FBVector3d(100, 100, 100), mobu.FBModelTransformationType.kModelScaling)

    def PlotVehicle(self):
        # current vehicle namespace
        currentVehicleNamespace = self._getCurrentVehicleNamespace()
        # get vehicle mover
        vehicleMover = Scene.FindModelByName("{0}:mover".format(currentVehicleNamespace), True)
        # get children of the mover
        if vehicleMover:
            vehicleMover.Selected = True
            vehicleHierarchyList = []
            Scene.GetChildren(vehicleMover, vehicleHierarchyList)
            # select appropriate children
            for child in vehicleHierarchyList:
                if isinstance(child, (mobu.FBModelSkeleton, mobu.FBModelNull)):
                    child.Selected = True
            # plot selected on current take
            options = mobu.FBPlotOptions()
            options.PlotOnFrame = True
            options.UseConstantKeyReducer = False
            mobu.FBSystem().CurrentTake.PlotTakeOnSelected(options)
        # deselect everything
        Scene.DeSelectAll()

    def _getControllerList(self):
        # get namespace of selected vehicle
        currentVehicleNamespace = self._getCurrentVehicleNamespace()

        # find main control
        controlBoneList = []
        mainControl = Scene.FindModelByName('{0}:Master_Controller'.format(currentVehicleNamespace), True)

        if mainControl is None:
            # try finding chassis control if master control doesnt exist
            mainControl = Scene.FindModelByName('{0}:Chassis_Control'.format(currentVehicleNamespace), True)
            if mainControl is None:
                return

        Scene.GetChildren(mainControl, controlBoneList)
        return controlBoneList

    def selectAllControls(self):
        '''
        selects all controls associated with the vehicle rig. Will select everything under
        'chassis_control'
        '''
        # deselect all
        Scene.DeSelectAll()

        controlBoneList = self._getControllerList()

        for control in controlBoneList:
            control.Selected = True

    def _handleAutoRotationStateChange(self):
        # current vehicle namespace
        currentVehicleNamespace = self._getCurrentVehicleNamespace()

        # get Chassis_Control
        chassisControl = Scene.FindModelByName('{0}:Chassis_Control'.format(currentVehicleNamespace), True)

        if chassisControl is not None:
            autoRotProperty = chassisControl.PropertyList.Find("Autorotation")
            if autoRotProperty is not None:
                if self.autorotationCheckbox.checkState() == QtCore.Qt.CheckState.Checked:
                    autoRotProperty.Data = 1
                else:
                    autoRotProperty.Data = 0

    def _handleRefreshPressed(self):
        self._populateVehicleComboBox()
        self._populateOtherComboBoxes()
        self._populateMultiRefComboBox()

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # deselect all in scene
        Scene.DeSelectAll()

        # create layouts
        mainLayout = QtGui.QGridLayout()
        bannerLayout = QtGui.QGridLayout()
        accessoriesLayout = QtGui.QGridLayout()
        extraBoneLayout = QtGui.QGridLayout()
        pivotLayout = QtGui.QGridLayout()

        mainLayout.setContentsMargins(0, 0, 0, 0)
        bannerLayout.setVerticalSpacing(0)

        # widgets
        banner = UI.BannerWidget(name='', helpUrl="")
        refreshButton = QtGui.QPushButton()
        self.vehicleLabel = QtGui.QLabel()
        self.vehicleCombo = QtGui.QComboBox()
        self.vehicleTypeLabel = QtGui.QLabel()
        self.vehicleTypeCombo = QtGui.QComboBox()
        horizontalLine = QtGui.QFrame()
        self.vehicleImageWidget = VehicleButtonWidget()
        self.emptyLabel = QtGui.QLabel()
        self.accLabel = QtGui.QLabel()
        self.accCombo = QtGui.QComboBox()
        horizontalLine6 = QtGui.QFrame()
        self.autorotationCheckbox = QtGui.QCheckBox()
        horizontalLine2 = QtGui.QFrame()
        self.extraCombo = QtGui.QComboBox()
        self.extraHideCheckBox = QtGui.QCheckBox()
        horizontalLine3 = QtGui.QFrame()
        self.alignPivotButton = QtGui.QPushButton()
        reflabel = QtGui.QLabel()
        self.keyButton = QtGui.QPushButton()
        self.multiRefCombo = QtGui.QComboBox()
        horizontalLine4 = QtGui.QFrame()
        selectControlsButton = QtGui.QPushButton()
        self.plotButton = QtGui.QPushButton()
        horizontalLine5 = QtGui.QFrame()

        # widget properties
        refreshButton.setText("Refresh")
        refreshButton.setMaximumHeight(15)
        self.vehicleLabel.setText(" Vehicle:")
        self.vehicleTypeLabel.setText(" Vehicle Type:")
        horizontalLine.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine.setFrameShadow(QtGui.QFrame.Sunken)
        self.vehicleImageWidget.setMinimumHeight(542)
        self.vehicleImageWidget.setMinimumWidth(400)
        self.vehicleImageWidget.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed))
        self._allButtonList = self.vehicleImageWidget._getButtonList()
        self.accLabel.setText(" Vehicle Accessories:")
        horizontalLine6.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine6.setFrameShadow(QtGui.QFrame.Sunken)
        self.autorotationCheckbox.setCheckState(QtCore.Qt.CheckState.Checked)
        self.autorotationCheckbox.setText("Auto-Rotation")
        self.autorotationCheckbox.setToolTip("checked: auto-rotation constraints weight to 100\nunchecked: auto-rotation constraints weight to 0")
        horizontalLine2.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine2.setFrameShadow(QtGui.QFrame.Sunken)
        self.extraHideCheckBox.setText("Hide Extra Bone")
        horizontalLine3.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine3.setFrameShadow(QtGui.QFrame.Sunken)
        self.alignPivotButton.setText("Align TurnPivot to TurnPivot_ref")
        reflabel.setText(' Multi Referential Constraint:')
        self.keyButton.setIcon(QtGui.QIcon(QtGui.QPixmap(self.keyImage)))
        self.multiRefCombo = QtGui.QComboBox()
        horizontalLine4.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine4.setFrameShadow(QtGui.QFrame.Sunken)
        self.plotButton.setText('Plot Vehicle')
        self._setExtraCheckBoxState()
        horizontalLine5.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine5.setFrameShadow(QtGui.QFrame.Sunken)
        selectControlsButton.setText('Select All Controls')

        # add widgets to layout
        bannerLayout.addWidget(banner, 0, 0)
        bannerLayout.addWidget(refreshButton, 1, 0)

        pivotLayout.addWidget(reflabel, 0, 0)
        pivotLayout.addWidget(self.keyButton, 0, 1)

        accessoriesLayout.addWidget(self.accLabel, 0, 0)
        accessoriesLayout.addWidget(self.accCombo, 0, 1, 1, 2)

        extraBoneLayout.addWidget(self.extraHideCheckBox, 0, 0)
        extraBoneLayout.addWidget(self.extraCombo, 0, 1, 1, 2)

        mainLayout.addLayout(bannerLayout, 0, 0, 1, 3)
        mainLayout.addWidget(self.vehicleLabel, 2, 0)
        mainLayout.addWidget(self.vehicleCombo, 2, 1, 1, 2)
        mainLayout.addWidget(self.vehicleTypeLabel, 3, 0)
        mainLayout.addWidget(self.vehicleTypeCombo, 3, 1,1, 2)
        mainLayout.addWidget(horizontalLine, 4, 0, 1, 3)
        mainLayout.addWidget(self.autorotationCheckbox, 5, 2)
        mainLayout.addWidget(self.vehicleImageWidget, 6, 0, 1, 3)
        mainLayout.addLayout(accessoriesLayout, 7, 0, 1, 3)
        mainLayout.addWidget(horizontalLine2, 8, 0, 1, 3)
        mainLayout.addLayout(extraBoneLayout, 9, 0, 1, 3)
        mainLayout.addWidget(horizontalLine3, 10, 0, 1, 3)
        mainLayout.addLayout(pivotLayout, 11, 0)
        mainLayout.addWidget(self.multiRefCombo, 11, 1, 1, 2)
        mainLayout.addWidget(horizontalLine6, 12, 0, 1, 3)
        mainLayout.addWidget(self.alignPivotButton, 13, 0, 1, 3)
        mainLayout.addWidget(horizontalLine4, 14, 0, 1, 3)
        mainLayout.addWidget(selectControlsButton, 15, 0, 1, 3)
        mainLayout.addWidget(self.plotButton, 16, 0, 1, 3)
        mainLayout.addWidget(horizontalLine5, 17, 0, 1, 3)

        # set layout
        self.setLayout(mainLayout)

        # populate comboboxes
        self._populateVehicleComboBox()
        self._populateVehicleTypeComboBox()
        self._populateOtherComboBoxes()
        self._populateMultiRefComboBox()
        self._handleAutoRotationStateChange()
        self._vehicleTypeComboIndexChanged()

        reflabel.setToolTip("Click 'K' Button to set a key to the\ncurrently selected Multi Ref option")
        self.keyButton.setToolTip("Click 'K' Button to set a key to the\ncurrently selected Multi Ref option")
        self.multiRefCombo.setToolTip("Click 'K' Button to set a key to the\ncurrently selected Multi Ref option")

        # set connections
        self.vehicleImageWidget.buttonPressedSignal.connect(self.SelectVehicleController)
        self.vehicleCombo.currentIndexChanged.connect(self._vehicleComboIndexChanged)
        self.vehicleTypeCombo.currentIndexChanged.connect(self._vehicleTypeComboIndexChanged)
        self.accCombo.currentIndexChanged.connect(self.SelectAccessoryController)
        self.plotButton.released.connect(self.PlotVehicle)
        self.alignPivotButton.released.connect(self.TurnPivotToTurnPivotRef)
        self.extraHideCheckBox.stateChanged.connect(self.HideExtraBone)
        self.extraCombo.currentIndexChanged.connect(self._setExtraCheckBoxState)
        self.keyButton.pressed.connect(self._setConstraintKey)
        self.multiRefCombo.currentIndexChanged.connect(self._handleMultiRefIndexChanged)
        refreshButton.released.connect(self._handleRefreshPressed)
        self.autorotationCheckbox.stateChanged.connect(self._handleAutoRotationStateChange)
        selectControlsButton.released.connect(self.selectAllControls)