"""
Description:
    The functionality for the Capture Player UI
    Mostly wrappers to get the correct video url

Authors:
    David Vega <david.vega@rockstargames.com>
"""

import os
import xml.etree.cElementTree as xml
from collections import namedtuple

import RS.Config
import RS.Utils.Scene
import RS.Core.DatabaseConnection
import RS.Utils.Logging.Universal

gLog = RS.Utils.Logging.Universal.UniversalLog("Motionbuilder_EST", False, True)

# Check to make sure the database is only accessed by Rockstar Studios and not Technicolor as they do not
# have access to the database at this time

DATABASE = None
if "R*" in RS.Config.User.Studio:
    DATABASE = RS.Core.DatabaseConnection.DatabaseConnection('{SQL Server}', 'NYCW-MHB-SQL', 'CaptureRenders', gLog)

CAPTURE_DATA = namedtuple("CAPTURE_DATA", ['url', 'fbxRevision', 'captureRevision', 'captureRequested',
                                           'captureUpdated', 'dlc'])


def GetCaptureData(filepath, isVideo=False):
    """
    Gets the path on the server where the capture .mov file lives based on the FBXFilename

    Arguments:
        filepath = string; name of the fbx file you are trying to access
        isVideo = boolean;  if the url provided is to the video file
    """
    data = [os.path.join(RS.Config.Tool.Path.TechArt, r"dcc\motionbuilder{0}\images\VideoPlayer\error.mp4".format(RS.Config.Script.TargetBuild)),
            0, 0, "", "", ""]

    if not filepath:
        return CAPTURE_DATA(*data)

    filepath = filepath.replace("\\", "/")

    # In the database, the fbx file paths are stored pointing to the x drive or to a relative path (//filepath)
    # So here we are generating the opposite path from the filepath provided to make sure we test for both
    # version of the file paths

    pathstart = ["X:/", "/"]
    filepathStartsWithX = filepath.lower().startswith("x:/")
    alternatePath = filepath.replace(pathstart[filepathStartsWithX-1], filepath[filepathStartsWithX])

    for path in [filepath, alternatePath]:
        query = "SELECT ServerPath, FBXRev, CaptureRev, CaptureRequested, CaptureUpdated, DLC_NAME FROM CaptureInfo_{} \
                 WHERE {} = '{}'".format(RS.Config.Project.Name[:3], ["FBXFilename", "ServerPath"][isVideo], path)

        result = DATABASE.sql_command(query)
        if result:
            # The results are returned as a string represting a tuple inside a list
            # it is simply easier to execute the string to access the correct data we need
            exec("data = list({}[0])".format(result))
            if data[1] is None: data[1] = 0
            break

    return CAPTURE_DATA(*data)


def GetAllVideoPaths():
    """ Gets all the FBX Files with videos on the CaptureRenders Database """
    if not DATABASE: return []

    results = DATABASE.ExecuteQuery("SELECT ServerPath, DLC_NAME FROM CaptureInfo_{} WHERE ProjectID = {}".format(
                                    RS.Config.Project.Name[:3], GetProjectIndex()))

    # We run the results through exec to convert None, numbers, and strings to their appropriate python types.

    exec("results = {}".format(results))

    if not results:
        results = []

    return [eachResult for eachResult in results if None not in eachResult]


def GetProjectIndex():
    """ Gets the index associated with the current project set in Motion Builder"""
    tree = xml.parse(os.path.join(RS.Config.Tool.Path.TechArt, r"etc\config\database\database.xml"))
    root = tree.getroot()
    return int(getattr(root.find("./CaptureRender/ProjectID/{}[@index]".format(RS.Config.Project.Name)),
                       "attrib", {"index": 0})["index"])