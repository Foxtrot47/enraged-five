import base64
import cPickle
from RS import Config

from RS.Tools.HandPose.Models import PoseControl
from RS.Tools.HandPose.Models import PoseControlModel
from RS.Tools.HandPose.Widgets import poseInterpolationWidget
from RS.Tools.HandPose import const

from PySide import QtGui, QtCore


class PoseLabelWidget(QtGui.QWidget):
    """ Creates the top widget (includes label and refresh button) for the widgets in the "Edit Pose Library" tab """

    def __init__(self, parent=None, widgetLabel=None):
        """
        Kwargs:
            parent (QtGui.QWidget): parent widget
            widgetLabel (str): Text label for the library tree list
        """
        super(PoseLabelWidget, self).__init__(parent=parent)
        self.treeModelLink = None
        self.widgetLabel = widgetLabel or "Library"
        self.setupUi()

    def setupUi(self):
        # Create Layout
        layout = QtGui.QHBoxLayout()

        # Create Widget
        self.treeLabel = QtGui.QLabel(self.widgetLabel)

        # Add Widget to Layout
        layout.addWidget(self.treeLabel)

        # Set Layout
        self.setLayout(layout)

        # Configure Layout
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(5)

        # Configure Widget
        self.setStyleSheet("QLabel {font: bold 11px; qproperty-alignment: AlignCenter;}")
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        """
        Accepts the drag enter event

        Args:
            event (QtGui.QDragEnterEvent): Event handler
        """
        event.accept()

    def dropEvent(self, event):
        """
        Runs the event when a pose has been moved/dropped to a new location in the library tree

        Args:
            event (QtGui.QDropEvent): Event handler
        """
        event.setDropAction(QtCore.Qt.CopyAction)
        event.accept()

        # Add data to the data structure
        dropData = event.mimeData()

        if len(dropData._ModelIndices) == 0:
            return None

        root = self.treeModelLink.rootData

        inputData = str(dropData.data("text/plain"))
        if len(inputData) == 0:
            return True

        items = cPickle.loads(inputData)
        if len(items) == 0:
            return True

        # Moves the pose to the new location/index
        offsetIndex = 0
        for currentPoseData in items :
            currentPoseData.movePoseTo(root, offsetIndex=offsetIndex)
            offsetIndex += 1

        # Deletes the old instance
        if dropData.category == "HandLibrary":
            self.treeModelLink.model._deleteSelection(dropData._ModelIndices)

        # Refreshes the tree with the new indices
        if self.treeModelLink is not None:
            self.treeModelLink.refreshViewFromPoseRoot()


class PoseControlWidget(QtGui.QWidget):
    """ Widget for displaying custom sets of anim files """
    selectionChangedSignal = QtCore.Signal(object)
    lastItemSelected = QtCore.Signal(object)

    def __init__(
        self,
        parent=None,
        widgetLabel=None,
        usePoseFromScene=True,
        layoutMode=None,
        treeHeight=380,
        treeWidth=190,
        userPoseLibrary=None,
        thumbnailSize=210,
    ):
        """
        Kwargs:
            parent (QtGui.QWidget): parent widget
            widgetLabel (str): Text label to give the widget at the top
            usePoseFromScene (bool): True or False, get pose(s) stored in MoBu's pose control
            layoutMode (str): Vertical or horizontal
            treeHeight (int): Height of the library tree
            treeWidth (int): Width of the library tree
            userPoseLibrary (str): Path to json pose library
            thumbnailSize (int): Size of the pose preview thumbnail
        """
        super(PoseControlWidget, self).__init__(parent=parent)

        self.rootData = None

        self.widgetLabel = widgetLabel or "Pose Control"
        self.usePoseFromScene = usePoseFromScene
        self.treeHeight = treeHeight
        self.treeWidth = treeWidth
        self.layoutMode = layoutMode or const.Settings.DEFAULT_LAYOUT_MODE
        self.thumbnailSize = thumbnailSize
        self.userPoseLibrary = userPoseLibrary or ""
        self.preventSceneThumbnailUpdate = True
        self.lerpWidget = None
        self.__currentSelection = tuple()
        self.collapsedFolders = []
        self.setupUi() 

    def setupUi(self):
        # Paths needed for widget construction
        iconFolderString = "{0}/ReferenceEditor/".format(Config.Script.Path.ToolImages)

        # Run outside functions for constructing UI
        self.imageFrame = self._buildThumbnailFrame()
        self.tree = self._buildPoseTree()
        self.lerpWidget = poseInterpolationWidget.PoseInterpolationWidget()

        # Create Layouts
        # Create Layouts
        labelLayout = QtGui.QGridLayout()
        if self.layoutMode == const.Settings.DEFAULT_LAYOUT_MODE:
            layout = QtGui.QVBoxLayout()
        else:
            layout = QtGui.QHBoxLayout()
            self.thumbnailLayout = QtGui.QVBoxLayout()

        # Create Widgets
        refreshLibraryIcon = QtGui.QIcon()
        refreshLibraryPixmap = QtGui.QPixmap("{0}UpdateReference.png".format(iconFolderString))
        button = QtGui.QPushButton("")
        labelLayoutWidget = QtGui.QWidget()

        if self.widgetLabel == "Library":
            self.treeLabel = PoseLabelWidget(widgetLabel=self.widgetLabel)
            self.treeLabel.treeModelLink = self
        else:
            self.treeLabel = QtGui.QLabel(self.widgetLabel)
            self.setStyleSheet("QLabel {font: bold 11px; qproperty-alignment: AlignCenter;}")

        leftSpacer = QtGui.QSpacerItem(5, 5, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        rightSpacer = QtGui.QSpacerItem(5, 5, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)

        if self.layoutMode is not const.Settings.DEFAULT_LAYOUT_MODE:
            thumbnailWidget = QtGui.QWidget()

        # Add Widgets
        refreshLibraryIcon.addPixmap(refreshLibraryPixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        labelLayout.addItem(leftSpacer, 0, 0)
        labelLayout.addWidget(self.treeLabel, 0, 1, QtCore.Qt.AlignCenter)
        labelLayout.addItem(rightSpacer, 0, 2)
        labelLayout.addWidget(button, 0, 3, QtCore.Qt.AlignRight)

        if self.layoutMode == const.Settings.DEFAULT_LAYOUT_MODE:
            layout.addWidget(labelLayoutWidget)
            layout.addWidget(self.tree)
            layout.addWidget(self.imageFrame)
        else:
            self.thumbnailLayout.addWidget(labelLayoutWidget)
            self.thumbnailLayout.addWidget(self.imageFrame)
            self.thumbnailLayout.addWidget(self.lerpWidget)
            layout.addWidget(thumbnailWidget)
            layout.addWidget(self.tree)

        # Set Layouts
        labelLayoutWidget.setLayout(labelLayout)
        self.setLayout(layout)
        if self.layoutMode is not const.Settings.DEFAULT_LAYOUT_MODE:
            thumbnailWidget.setLayout(self.thumbnailLayout)

        # Configure Layouts
        labelLayout.setContentsMargins(0, 0, 0, 0)
        labelLayout.setSpacing(5)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(5)

        if self.layoutMode is not const.Settings.DEFAULT_LAYOUT_MODE:
            self.thumbnailLayout.setContentsMargins(2, 2, 2, 2)
            self.thumbnailLayout.setSpacing(5)
            self.thumbnailLayout.addStretch()

        # Configure Widgets
        button.setIcon(refreshLibraryIcon)
        button.setIconSize(QtCore.QSize(14, 14))

        # Hook up connections
        self.tree.doubleClicked.connect(self._triggerDoubleClick)
        button.pressed.connect(self._refreshTreeDataStructure)

        # Refresh the pose tree
        self._refreshTreeDataStructure()

    def _buildPoseTree(self):
        """
        Creates the tree view where the poses will be stored for use

        Returns:
            QtGui.QTreeView
        """
        # Create model from PoseDataModel Class and store self as the tree view
        self.model = PoseControlModel.PoseDataModel()
        self.model.treeView = self

        # Create Widget
        poseTree = QtGui.QTreeView()

        # Configure Widget
        if self.layoutMode == const.Settings.DEFAULT_LAYOUT_MODE:
            poseTree.setDragEnabled(True)
            poseTree.setDropIndicatorShown(True)
            poseTree.setAcceptDrops(True)
            poseTree.setDragDropOverwriteMode(True)
            poseTree.setDragDropMode(poseTree.DragDrop)
            poseTree.setSelectionMode(poseTree.ExtendedSelection)
            poseTree.setSelectionBehavior(poseTree.SelectRows)

            if self.widgetLabel == "Pose Control":
                poseTree.setDropIndicatorShown(False)
                poseTree.setAcceptDrops(False)
                poseTree.setDragDropMode(poseTree.DragOnly)
            else:
                poseTree.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
                poseTree.customContextMenuRequested.connect(self._editItemMenu)

        poseTree.selectionChanged = self._selectionChanged
        poseTree.setExpandsOnDoubleClick(False)
        poseTree.setColumnWidth(0, 200)
        poseTree.setMinimumHeight(self.treeHeight)
        poseTree.setMinimumWidth(self.treeWidth)

        # Hook up connections
        poseTree.expanded.connect(self._storeExpansion)
        poseTree.collapsed.connect(self._updateExpansion)

        # Set model of the poseTree
        poseTree.setModel(self.model)

        return poseTree

    def _updateExpansion(self, inputTreeNode):
        """
        Triggered when a tree gets collapsed

        Args:
            inputTreeNode (QtCore.QModelIndex): Holds the PoseDataModel
        """
        collapsedNode = self.model.itemFromIndex(inputTreeNode)
        if collapsedNode not in self.collapsedFolders:
            return

        self.collapsedFolders.remove(collapsedNode)
        collapsedNode._isCollapsed = True

    def _storeExpansion(self, inputTreeNode):
        """
        Triggered when a tree gets expanded

        Args:
            inputTreeNode (QtCore.QModelIndex): Holds the PoseDataModel
        """
        expandedNode = self.model.itemFromIndex(inputTreeNode)
        self.collapsedFolders.append(expandedNode)

        expandedNode._isCollapsed = False

    def _editItemMenu(self, position):
        """
        Adds a QMenu and actions to tree

        Args:
            position (QtCore.QPoint): Position that the tree item is at
        """
        menu = QtGui.QMenu()
        menu.addAction(self.tr("Rename Element"), self._renameItemFromMenu)
        menu.addAction(self.tr("Create Folder)"), self._createFolder)
        menu.addAction(self.tr("Delete Item(s)"), self._deleteItemFromMenu)
        menu.exec_(self.tree.viewport().mapToGlobal(position))

    def _createFolder(self):
        """ Create folder function for the pose tree, added as an action to the "Create Folder(s)" menu item """
        if len(self.__currentSelection) == 0:
            return

        currentItem = self.model.itemFromIndex(self.__currentSelection[0])
        newItemName, cancelState = QtGui.QInputDialog.getText(
            self, "Create pose folder", "folder name:", text=currentItem.name(),
        )

        if cancelState is False:
            return

        poseCopy = PoseControl.PoseDataItem()
        poseCopy.isFolder = True
        poseCopy.Name = newItemName

        root = currentItem._parent
        poseCopy.movePoseTo(root)
        self.refreshViewFromPoseRoot()

    def _deleteItemFromMenu(self):
        """ Delete item function for the pose tree, added as an action to the "Delete Item(s)" menu item """
        self.model._deleteSelection(self.__currentSelection)
        self.refreshViewFromPoseRoot()

    def _renameItemFromMenu(self):
        """ Rename item function for the pose tree, added as an action to the "Rename Element" menu item """
        if len(self.__currentSelection) == 0:
            return

        currentItem = self.model.itemFromIndex(self.__currentSelection[0])
        newItemName, cancelState = QtGui.QInputDialog.getText(
            self, "Rename selected pose", "New Item name:", text=currentItem.name(),
        )

        if cancelState is False:
            return

        currentItem.setName(newItemName)
        self.refreshViewFromPoseRoot()

    def _triggerDoubleClick(self, index):
        """
        Connect applyPoseToSelection to pose double click behavior

        Args:
            index (QtCore.QPoint): Position of the current pose item
        """
        if len(self.tree.selectedIndexes()) == 0:
            return

        item = self.tree.selectedIndexes()[0]
        item.model().itemFromIndex(index).handPoseToolWindow = self.parentWidget() 
        item.model().itemFromIndex(index).applyPoseToSelection()

    def _buildThumbnailFrame(self):
        """
        Creates a QFrame that holds the pose preview thumbnail

        Returns:
            QtGui.QFrame
        """
        # Create Layouts
        imageFramelayout = QtGui.QHBoxLayout()
        if self.layoutMode == const.Settings.DEFAULT_LAYOUT_MODE:
            imageFramelayout2 = QtGui.QHBoxLayout()

        # Create Widgets
        imageFrame = QtGui.QFrame()
        if self.layoutMode == const.Settings.DEFAULT_LAYOUT_MODE:
            imageFrame2 = QtGui.QFrame()
        self.thumbNail = QtGui.QLabel()

        # Add Widget to Layouts
        imageFramelayout.addWidget(self.thumbNail)

        # Set Layouts to Widgets
        imageFrame.setLayout(imageFramelayout)
        if self.layoutMode == const.Settings.DEFAULT_LAYOUT_MODE:
            imageFrame2.setLayout(imageFramelayout2)

        # Configure Layouts
        imageFramelayout.setContentsMargins(0, 0, 0, 0)
        imageFramelayout.setSpacing(0)
        imageFramelayout.setAlignment(QtCore.Qt.AlignCenter)
        if self.layoutMode == const.Settings.DEFAULT_LAYOUT_MODE:
            imageFramelayout2.setContentsMargins(0, 0, 0, 0)
            imageFramelayout2.setSpacing(0)
            imageFramelayout2.setAlignment(QtCore.Qt.AlignCenter)
            imageFramelayout2.addStretch()
            imageFramelayout2.addWidget(imageFrame)
            imageFramelayout2.addStretch()

        # Configure Widgets
        imageFrame.setFrameShape(QtGui.QFrame.StyledPanel)
        imageFrame.setFrameShadow(QtGui.QFrame.Sunken)
        imageFrame.setLineWidth(2)
        # Set stylesheet
        frameColor = QtGui.QColor(190, 190, 190)
        imageFrame.setStyleSheet("QFrame { background-color: %s }" % frameColor.name())

        self.thumbNail.setMinimumSize(QtCore.QSize(self.thumbnailSize, self.thumbnailSize))
        self.thumbNail.setMaximumSize(QtCore.QSize(self.thumbnailSize, self.thumbnailSize))
        self.thumbNail.setFixedSize(QtCore.QSize(self.thumbnailSize, self.thumbnailSize))

        if self.layoutMode == const.Settings.DEFAULT_LAYOUT_MODE:
            imageFrame2.setMinimumSize(QtCore.QSize(self.thumbnailSize, self.thumbnailSize))
            return imageFrame2

        return imageFrame

    def _refreshTreeDataStructure(self):
        """ Re-populates the pose library tree """
        if self.usePoseFromScene:
            if self.preventSceneThumbnailUpdate:
                self.preventSceneThumbnailUpdate = False
                return
            self._populateView()
        else:
            self._populateLibrary(self.userPoseLibrary)

        self._refresh()
        self.model.reset()
        if self.rootData is None:
            return

        poseArray = []
        self.rootData.extractHierarchy(self.rootData, poseArray)

        for poseData in poseArray:
            if poseData[0]._isCollapsed is False and poseData[0].isFolder is True:
                expandedTreeNode = self.model.indexFromItem(poseData[0])
                self.tree.setExpanded(expandedTreeNode, True)

    def _populateView(self):
        """
        Creates a PoseManager class instance for storing pose data from MoBu's pose controls when
        populating/re-populating the tree view
        """
        poseFn = PoseControl.PoseManager()
        self.rootData = poseFn.extractPoseLibraryFromScene()
        self.rootData.setNameIsEditable(True)
        self.model.setRoot(self.rootData)

    def _populateLibrary(self, userPoseLibrary):
        """
        Creates a PoseManager class instance for storing pose data from a json pose file when
        populating/re-populating the tree view

        Args:
            userPoseLibrary (str): Path of the json pose file
        """
        poseFn = PoseControl.PoseManager()
        self.rootData = poseFn.extractPoseLibraryFromJson(userPoseLibrary)
        if self.rootData is None:
            self.model.reset()
            return

        self.rootData.setNameIsEditable(True)
        self.model.setRoot(self.rootData)

        self.model.poseCategory = "HandLibrary"

    def refreshViewFromPoseRoot(self):
        """ Refresh the view as it was, making sure to set folders as collapsed/not """
        self._refresh()
        self.model.reset()

        poseArray = []
        self.rootData.extractHierarchy(self.rootData, poseArray)

        for poseData in poseArray:
            if poseData[0]._isCollapsed is False and poseData[0].isFolder is True:
                expandedTreeNode = self.model.indexFromItem(poseData[0])
                self.tree.setExpanded(expandedTreeNode, True)

    def _refresh(self):
        """ Resets the focus on the UI so that its draw calls are ran properly """
        if self.parent() is not None:
            self.parent().setFocus()
        self.setFocus()

    def _selectionChanged(self, selectedItems, _):
        """
        Overrides built-in method

        When the selection changes store the new selection as the current selection and emit a signal

        Args:
            selectedItems (QtCore.QSelectedIndexes): the newly selected items
        """
        self.__currentSelection = []
        selectedIndexes = self.tree.selectionModel().selectedIndexes()
        [self.__currentSelection.append(index) for index in selectedIndexes if not index.column()]
        self.__currentSelection = tuple(self.__currentSelection)
        self.selectionChangedSignal.emit(self.__class__)
        self.lastItemSelected.emit([index for index in selectedItems.indexes()][:1])

        if len(self.__currentSelection) == 0:
            self._refresh()
            return

        self._updatePoseThumbnail()

    def _updatePoseThumbnail(self):
        """ Updates the pose preview thumbnail """
        currentPoseIndex = self.__currentSelection[0]
        currentPose = currentPoseIndex.internalPointer()

        rawData = currentPose.thumbnail

        if len(rawData) == 0:
            thumbNailPixMap = QtGui.QPixmap()
            self.thumbNail.setPixmap(thumbNailPixMap)
            self.thumbNail.setScaledContents(True)
        else:
            thumbNailImage = QtGui.QImage.fromData(base64.b64decode(rawData))
            thumbNailPixMap = QtGui.QPixmap.fromImage(thumbNailImage)
            self.thumbNail.setPixmap(thumbNailPixMap)
            self.thumbNail.setScaledContents(True)

        self._refresh()
