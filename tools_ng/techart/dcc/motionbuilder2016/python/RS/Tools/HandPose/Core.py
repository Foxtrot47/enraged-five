import re
from pprint import pformat

import pyfbsdk as mobu

from PySide import QtGui

from RS import Globals
from RS.Utils import ContextManagers
from RS.Core.Animation import Lib
from RS.Tools.HandPose import const


class FingerController(object):
    """ Class used to compute hand interpolation """

    def __init__(self, handNode, rotationAnimationNode, samplingIndex):
        """
        Args:
            handNode (pyfbsdk.FBModel): Finger node object
            rotationAnimationNode (pyfbsdk.FBPropertyList): PropertyList.Find("Rotation (Lcl)").GetAnimationNode()
            samplingIndex (int): The index of the finger node within the hand
        """
        self.handNode = handNode
        self.rotationAnimationNode = rotationAnimationNode
        self.samplingIndex = samplingIndex


class AnimSubRange(object):
    """ Class used to compute hand interpolation """

    def __init__(self, startTimeValue, endTimeValue, splitCount):
        """
        Args:
            startTimeValue (pyfbsdk.FBTime.GetSecondDouble)
            endTimeValue (pyfbsdk.FBTime.GetSecondDouble)
            splitCount (int)
        """
        self.startTimeValue = startTimeValue
        self.endTimeValue = endTimeValue
        self.timeRange = endTimeValue - startTimeValue
        self.splitRange = (endTimeValue - startTimeValue) / splitCount
        self.splitCount = splitCount


class PoseRange(object):
    """ Class used to compute hand interpolation """

    def __init__(self):
        self.startPose = Pose()
        self.endPose = Pose()
        self.handNodes = HandNodes()

    def __repr__(self):
        """
        Creates a debug string for pose range

        Returns:
            str
        """
        return "<PoseRange >\n\tstartPose: {0}\n\n\tendPose: {1}\n".format(self.startPose, self.endPose)


class HandInterpolationSettings(object):
    """ Class used to bundle hands settings """

    def __init__(self):
        self.boneInterpolation = "all"
        self.handInterpolation  = "all"
        self.boneInterpolationWeight = 1.0
        self.handInterpolationWeight = 1.0
        self.inputTake  = None
        self.startTime  = mobu.FBTime(0, 0, 0, 0, 0)
        self.endTime  = mobu.FBTime(0, 0, 0, 10, 0)
        self.frameRate = mobu.FBTimeMode.kFBTimeMode30Frames


class Pose(object):
    """ Class used to bundle fingers rotation and other metadata """

    def __init__(self):
        self.bonePoseArray = []

    def buildFromInputPose(self, inputPose):
        """
        Extract all fingers pose from current hand pose

        Args:
            inputPose (dict):  From PoseControl.extractPoseFromSelection, fingersPoseData (dict of all pose data)
        """
        self.nodeObjectName = ""
        self.nodeIndex = -1
        self.localRotation = None

        for poseDataIndex, poseData in enumerate(inputPose):
            handJointValue = BonePose()

            handJointValue.localRotation = poseData.localRotation
            handJointValue.nodeObjectName = poseData.nodeObjectName
            handJointValue.nodeIndex = poseDataIndex

            self.bonePoseArray.append(handJointValue)

    def buildFromCharacter(self, inputCharacter, leftBoneIdArray, rightBoneIdArray):
        """
        Extract all fingers pose from the input character.

        Args:
            inputCharacter (pyfbsdk.FBCharacter)
            leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
            rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)
        """
        self.bonePoseArray = []

        # Create a hand node container and store non-IK components in it
        handHandle = HandNodes()
        handNodes = handHandle.extractComponents(
            inputCharacter, leftBoneIdArray, rightBoneIdArray, extractFkControllers=True,
        )

        # Add joint data for each joint to the pose list
        for handJointIndex, handJoint in enumerate(handNodes):
            handJointValue = BonePose()
            handJointValue.localRotation = list(handJoint.PropertyList.Find("Rotation (Lcl)").Data)
            handJointValue.nodeObjectName = handJoint.Name
            handJointValue.nodeIndex = handJointIndex

            self.bonePoseArray.append(handJointValue)

    def __repr__(self):
        return pformat(vars(self), indent=4, width=4)


class BonePose(object):
    """ Data related to finger orientation """

    def __init__(self):
        self.nodeObjectName = ""
        self.nodeIndex = -1
        self.localRotation = None

    def __repr__(self):
        return pformat(vars(self), indent=4, width=4)


class HandNodes(object):
    """ Utility class used to identify hand parts """

    def __init__(self):
        self.PH_L_Hand = None
        self.PH_R_Hand = None

    def extractIKComponents(
        self,
        inputCharacter,
        leftFingerEffectorArray,
        rightFingerEffectorArray,
        appendRightSide=True,
        skipLeftSide=False,
    ):
        """
        This method exposes hand IK components

        Args:
            inputCharacter (FBCharacter)
            leftFingerEffectorArray (list): List of left side IK finger effectors
            rightFingerEffectorArray (list): List of right side IK finger effectors

        Kwargs:
            appendRightSide (bool): True or False, include right side
            skipLeftSide (bool): True or False, exclude left side

        Returns:
            list[mobu.FBEffectorId] or list[str]
        """
        handBones = []
        effectorArray = []
        if not skipLeftSide:
            effectorArray.extend(leftFingerEffectorArray)
        if appendRightSide:
            effectorArray.extend(rightFingerEffectorArray)
        # Add pyfbsdk.FBModelMarker objects for existing IK Effectors to the handBones list
        for effector in effectorArray:
            # If the item is of type FBEffectorId, get the model before you add it to the list
            if isinstance(effector, mobu.FBEffectorId):
                effector = inputCharacter.GetEffectorModel(effector)
            handBones.append(effector)

        return handBones

    def extractComponents(
        self,
        inputCharacter,
        leftBoneIdArray,
        rightBoneIdArray,
        appendRightSide=True,
        skipLeftSide=False,
        extractFkControllers=False,
        appendPhBones=True,
    ):
        """
        This method exposes hand components

        Args:
            inputCharacter (FBCharacter).
            leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
            rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)

        Kwargs:
            appendRightSide (bool): True or False, include right side
            skipLeftSide (bool): True or False, exclude left side
            extractFkControllers (bool): True or False, get the fk controllers/skeleton objects
            appendPhBones (bool): True or False, add the PH bones

        Returns:
            list[mobu.FBModelMarker]
        """
        handBones = []
        boneIdArray = []
        if not skipLeftSide:
            boneIdArray.extend(leftBoneIdArray)
        if appendRightSide:
            boneIdArray.extend(rightBoneIdArray)
        for boneId in boneIdArray:
            if not isinstance(boneId, mobu.FBBodyNodeId):
                boneId = getattr(mobu.FBBodyNodeId, boneId)
            if extractFkControllers:
                handBones.append(inputCharacter.GetCtrlRigModel(boneId))
            else:
                handBones.append(inputCharacter.GetModel(boneId))
        if appendPhBones:
            handBones.extend(self.getHandPhBones(inputCharacter))

        return handBones

    def getHandPhBones(self, inputCharacter):
        """
        This method exposes hand components

        Args:
            inputCharacter (FBCharacter)

        Returns:
            list[str] (ph hand bones)
        """
        handBones = [None, None]
        if inputCharacter is None:
            return handBones
        for extension in inputCharacter.CharacterExtensions:
            if extension.Label not in const.Parts.PH_COMPONENTS:
                continue

            currentObjectIndex = const.Parts.PH_COMPONENTS.index(extension.Label)
            phBone = extension.GetExtensionObjectWithLabelName(const.Parts.PH_COMPONENTS[currentObjectIndex])
            handBones[currentObjectIndex] = phBone

            if currentObjectIndex == 0:
                self.PH_L_Hand = phBone
            else:
                self.PH_R_Hand = phBone
        return handBones


class SelectedCharacterFingers(object):
    """ Utility class used to organize finger collections with their character owner """

    def __init__(self):
        self.fingerArray = []
        self.character = None

    def __repr__(self):
        fingerChainValue = ""
        for finger in self.fingerArray:
            fingerChainValue += "\n\t\t{0}".format(finger.LongName)
        return "<SelectedCharacterFingers \n\tcharacter:{0} \n\tfingerArray:{1}>".format(
            self.character.LongName, fingerChainValue,
        )


class FingerSet(object):
    """ Utility class used to consolidate a collection of fingers """

    def __init__(self):
        self.fingerArray = []
        self.character = None
        self.side = ""
        self.name = ""

    def __repr__(self):
        fingerChainValue = "".join("\n\t\t{0}".format(finger.LongName) for finger in self.fingerArray)
        fingerSetString = "<FingerSets \n\tname:{0} \n\tside:{1} \n\tcharacter:{2} \n\tfingerArray:{3}>"

        return fingerSetString.format(self.name, self.side, self.character.LongName, fingerChainValue)


class Finger(object):
    """ Utility class used to identify a finger part """

    def __init__(self):
        self.inputNode = None
        self.controlSet = None
        self.character = None
        self.handNodes = []

    def extractFingerChain(
        self,
        handUtils,
        leftBoneIdArray,
        rightBoneIdArray,
        leftFingerEffectorArray,
        rightFingerEffectorArray,
    ):
        """
        Method to extract a finger hierarchy

        Args:
            handUtils (HandNodes): HandNodes class Container
            leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
            rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)
            leftFingerEffectorArray (list): List of left side IK finger effectors as strings
            rightFingerEffectorArray (list): List of right side IK finger effectors as strings

        Returns:
            fingerChain: list[str] Fingers in chain/hierarchy
            "{0}_{1}".format(side, fingerName): Formatted string of side + finger name
            side: str - Left or Right
        """
        # Create the empty list to populate and declare side and fingerName variables
        fingerChain = []
        side = ""
        fingerName = ""

        # Grab all left and right hand node ids and IK effectors
        leftHandNodes = handUtils.extractComponents(
            self.character,
            leftBoneIdArray,
            rightBoneIdArray,
            appendRightSide=False,
            extractFkControllers=True,
            appendPhBones=False,
        )
        rightHandNodes = handUtils.extractComponents(
            self.character,
            leftBoneIdArray,
            rightBoneIdArray,
            skipLeftSide=True,
            extractFkControllers=True,
            appendPhBones=False,
        )
        leftIkHandNodes = handUtils.extractIKComponents(
            self.character, leftFingerEffectorArray, rightFingerEffectorArray, appendRightSide=False,
        )
        rightIkHandNodes = handUtils.extractIKComponents(
            self.character, leftFingerEffectorArray, rightFingerEffectorArray, skipLeftSide=True,
        )

        # fingerMapping is a list of finger bone segments in the form of ["Thumb", "Index", "Middle", "Ring", "Pinky"]
        # For example, if there are 2 thumb bones and 3 index bones -> ["Thumb", "Thumb", "Index", "Index", "Index"]
        # Example: ["Thumb", "Thumb", "Thumb", "Index", "Index", "Index", "Index",
        #           "Middle", "Middle", "Middle", "Middle", "Ring", "Ring", "Ring", "Ring",
        #           "Pinky", "Pinky", "Pinky", "Pinky"]
        fingerMappingFkParts = getFkParts(self.character, const.Sides.LEFT, asString=True)
        fingerMapping = getHandMapping(fingerMappingFkParts)

        # Filter the inputNode and add it to the fingerChain list
        if self.inputNode in leftIkHandNodes:
            side = const.Sides.LEFT.lower()
            mapIndexKey = leftIkHandNodes.index(self.inputNode)
            mapIndex = const.Parts.ALL_PARTS[mapIndexKey]
            fingerName = str(mapIndex)
            fingerChain.extend(leftHandNodes)
            fingerChain.extend(leftIkHandNodes)
            fingerChain.append(handUtils.PH_L_Hand)
        elif self.inputNode in rightIkHandNodes:
            side = const.Sides.RIGHT.lower()
            mapIndexKey = rightIkHandNodes.index(self.inputNode)
            mapIndex = const.Parts.ALL_PARTS[mapIndexKey]
            fingerName = str(mapIndex)
            fingerChain.extend(rightHandNodes)
            fingerChain.extend(rightIkHandNodes)
            fingerChain.append(handUtils.PH_R_Hand)
        elif self.inputNode in leftHandNodes:
            side = const.Sides.LEFT.lower()
            mapIndexKey = leftHandNodes.index(self.inputNode)
            mapIndex = fingerMapping[mapIndexKey]
            fingerName = str(mapIndex)
            fingerChain.extend(leftHandNodes)
            fingerChain.extend(leftIkHandNodes)
            fingerChain.append(handUtils.PH_L_Hand)
        elif self.inputNode in rightHandNodes:
            side = const.Sides.RIGHT.lower()
            mapIndexKey = rightHandNodes.index(self.inputNode)
            mapIndex = fingerMapping[mapIndexKey]
            fingerName = str(mapIndex)
            fingerChain.extend(rightHandNodes)
            fingerChain.extend(rightIkHandNodes)
            fingerChain.append(handUtils.PH_R_Hand)
        return fingerChain, "{0}_{1}".format(side, fingerName), side

    def __repr__(self):
        nodeName = None
        controlSet = None
        character = None

        nodeValues = [
            nodeName,
            controlSet,
            character,
        ]
        nodeAttributes = [
            self.inputNode,
            self.controlSet,
            self.character,
        ]

        for attributeIndex, attribute in enumerate(nodeAttributes):
            if attribute is not None:
                nodeValues[attributeIndex] = attribute.Name
        return "<Finger \n\tinputNode:{0} \n\tcontrolSet:{1} \n\tcharacter:{2}>".format(*nodeValues)


class CurveComponents(object):
    """ Container for storing curve data """

    SOURCE_ATTRIBUTES = [
        "xBufferCurve",
        "yBufferCurve",
        "zBufferCurve",
    ]
    TARGET_ATTRIBUTES = [
        "xTargetCurve",
        "yTargetCurve",
        "zTargetCurve",
    ]

    def __init__(self):
        self.rotationProperty = None

        self.startTime = None
        self.endTime = None

        self.xBufferCurve = None
        self.yBufferCurve = None
        self.zBufferCurve = None

        self.xTargetCurve = None
        self.yTargetCurve = None
        self.zTargetCurve = None

    def collectAndCleanTimeRange(self, inputNode, inputHandSettings):
        """
        Gathers rotation data from input node and edits/replaces the Keys for "cleanup"

        Args:
            inputNode (pyfbsdk.FBModel): Model object for either a finger or a PH bone
            inputHandSettings (HandInterpolationSettings): Container from custom settings class
        """
        self.rotationProperty = inputNode.PropertyList.Find("Rotation (Lcl)")

        self.startTime = inputHandSettings.startTime
        self.endTime = inputHandSettings.endTime

        rotationAnimNode = self.rotationProperty.GetAnimationNode()
        # rotationAnimNode.Nodes is a FBPropertyListAnimationNode object
        subAnimationNodes = rotationAnimNode.Nodes

        self.xBufferCurve = None
        self.yBufferCurve = None
        self.zBufferCurve = None

        self.xTargetCurve = None
        self.yTargetCurve = None
        self.zTargetCurve = None

        # Iterate through all subNodes
        for subNodeIndex, subNode in enumerate(subAnimationNodes):
            # subNode is a FBAnimationNode (X, Y, or Z)
            startCurve = mobu.FBFCurve()
            startCurve.EditBegin()
            startCurve.KeyReplaceBy(
                subNode.FCurve, mobu.FBTime.MinusInfinity, inputHandSettings.startTime, True, False,
            )
            startCurve.KeyReplaceBy(
                subNode.FCurve, inputHandSettings.endTime, mobu.FBTime.Infinity, True, False,
            )
            startCurve.EditEnd()

            subNode.FCurve.EditBegin()
            subNode.FCurve.KeyReplaceBy(
                startCurve, inputHandSettings.startTime, inputHandSettings.endTime, False, False,
            )
            subNode.FCurve.EditEnd()

            setattr(self, self.SOURCE_ATTRIBUTES[subNodeIndex], startCurve)
            setattr(self, self.TARGET_ATTRIBUTES[subNodeIndex], subNode.FCurve)

    def restoreAnimationRangeSideKeys(self):
        """ Set the start and end keys for the animation range """
        for subNodeIndex in xrange(3):
            startCurve = getattr(self, self.SOURCE_ATTRIBUTES[subNodeIndex])
            subNodeFCurve = getattr(self, self.TARGET_ATTRIBUTES[subNodeIndex])

            subNodeFCurve.EditBegin()
            subNodeFCurve.KeyReplaceBy(startCurve, self.startTime, self.startTime, True, False)
            subNodeFCurve.KeyReplaceBy(startCurve, self.endTime, self.endTime, True, False)
            subNodeFCurve.EditEnd()


def ExtractConnectionByType(inputObject, outputObjectType):
    """
    List the output connections of an input object which match outputObjectType

    Args:
        inputObject (FBControlSet or FBModelMarker (Finger Node))
        outputObjectType (FBPropertyListObject or FBCharacter)

    Returns:
        FBPropertyListObject or FBCharacter
    """
    markerOutputCount = inputObject.GetDstCount()
    for outputIndex in range(markerOutputCount):
        link = inputObject.GetDst(outputIndex)
        if not isinstance(link, outputObjectType):
            continue
        return link
    return None


def ExtractControlSetFromMarker(inputMarker):
    """
    Extract a character control set from an inputMarker

    Args:
        inputMarker (FBModelMarker)

    Returns:
        FBControlSet
    """
    controlSetlink = ExtractConnectionByType(inputMarker, mobu.FBPropertyListObject)
    if controlSetlink is None:
        return None

    controlSet = controlSetlink.GetOwner()
    return controlSet


def ExtractFingersFromSelection(
    leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray,
):
    """
    Filter the current animation controller selection in order to return finger hierarchies

    Args:
        leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
        rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)
        leftFingerEffectorArray (list): List of left side IK finger effectors as strings
        rightFingerEffectorArray (list): List of right side IK finger effectors as strings

    Returns:
        dict: validFingerNodesPerCharacter
    """
    # Create a container to hold hand nodes
    handUtils = HandNodes()

    # Get the selected models
    currentSelection = mobu.FBModelList()
    mobu.FBGetSelectedModels(currentSelection)
    if len(currentSelection) ==0:
        return None

    # Store all selected nodes in a list
    fkControlMarkerArray = []
    for node in currentSelection:
        if not isinstance(node, mobu.FBModelMarker):
            continue
        fkControlMarkerArray.append(node)

    # Create empty lists and dict for storing fingers and data
    fingerArray = []
    fingerSetArray = []
    fingersDict = {}

    # Iterate through all selected fingers
    for marker in fkControlMarkerArray:
        controlSet = ExtractControlSetFromMarker(marker)
        if controlSet is None:
            continue

        # Make sure there is a character
        if isinstance(controlSet, mobu.FBControlSet):
            inputCharacter = ExtractConnectionByType(controlSet, mobu.FBCharacter)
        else:
            inputCharacter = Globals.Application.CurrentCharacter

        if inputCharacter is None:
            continue

        # Get all finger nodes and IK effectors in both hands, then add them to a master list of valid finger nodes
        leftHandNodes = handUtils.extractComponents(
            inputCharacter, leftBoneIdArray, rightBoneIdArray, appendRightSide=False, extractFkControllers=True,
        )
        rightHandNodes = handUtils.extractComponents(
            inputCharacter, leftBoneIdArray, rightBoneIdArray, skipLeftSide=True, extractFkControllers=True,
        )
        leftIkHandNodes = handUtils.extractIKComponents(
            inputCharacter, leftFingerEffectorArray, rightFingerEffectorArray, appendRightSide=False,
        )
        rightIkHandNodes = handUtils.extractIKComponents(
            inputCharacter, leftFingerEffectorArray, rightFingerEffectorArray, skipLeftSide=True,
        )

        validFingers = list(leftHandNodes)
        validFingers.extend(rightHandNodes)
        validFingers.extend(leftIkHandNodes)
        validFingers.extend(rightIkHandNodes)

        # Make sure the current selected finger is valid
        if marker not in validFingers:
            continue

        # Create Finger container for the selected node, store data, and add to the fingerArray list
        fingerData = Finger()
        fingerData.inputNode = marker
        fingerData.controlSet = controlSet
        fingerData.character = inputCharacter
        fingerArray.append(fingerData)

    # Return None and error if a valid node is not found
    if len(fingerArray) == 0:
        errorStream = "Could not find a valid finger node in the current selection"
        QtGui.QMessageBox.information(None, "Missing Element", errorStream)
        return None

    # Iterate through the filtered list of selected fingers and their stored data
    # Finger Data Example:
    # <Finger
    # 	inputNode:LeftHandRing1
    # 	controlSet:Control Rig
    # 	character:PLAYER_ZERO>
    for finger in fingerArray:
        fingerChain, fingerName, side = finger.extractFingerChain(
            handUtils, leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray,
        )

        # Create a finger set container, store data, and add the container to the fingerSetArray list
        fingerSet = FingerSet()
        fingerSet.fingerArray = fingerChain
        fingerSet.character = finger.character
        fingerSet.side = side
        fingerSet.name = "{0}@{1}".format(fingerName, finger.character.LongName)
        fingerSet.handNodes = handUtils.extractComponents(fingerSet.character, leftBoneIdArray, rightBoneIdArray)

        fingerSetArray.append(fingerSet)
    # Return nothing if there is nothing in the fingerSet
    if len(fingerSetArray) == 0:
        return None

    # Add all fingers in list to the fingersDict
    for item in fingerSetArray:
        if item.name not in fingersDict.keys():
            fingersDict[item.name] = item

    # Return nothing if there is nothing in the fingersDict
    if len(fingersDict.keys()) == 0:
        return None

    # Iterate through the fingersDict and further sort/filter the finger data
    validFingerNodesPerCharacter = {}
    for fingerKey in fingersDict.keys():
        item = fingersDict[fingerKey]
        itemKey = item.character.LongName

        if itemKey not in validFingerNodesPerCharacter.keys():
            fingerData = SelectedCharacterFingers()
            fingerData.fingerArray = list(item.fingerArray)
            fingerData.character = item.character
            validFingerNodesPerCharacter[itemKey] = fingerData
        else:
            itemToAppend = []
            for itemNode in item.fingerArray:
                if itemNode in validFingerNodesPerCharacter[itemKey].fingerArray:
                    continue
                itemToAppend.append(itemNode)
            validFingerNodesPerCharacter[itemKey].fingerArray.extend(itemToAppend)

    # Make sure the validFingerNodesPerCharacter dict has data before returning it
    if len(validFingerNodesPerCharacter.keys()) == 0:
        return None
    return validFingerNodesPerCharacter


def PreparePoseRangeArray(selectedFingers, leftBoneIdArray, rightBoneIdArray, inputEndPose=None):
    """
    Creates a list with a single pose used for the end pose when interpolation is computed.

    Args:
        selectedFingers (dict): Gotten from ExtractFingersFromSelection()
        leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
        rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)

    Kwargs:
        inputEndPose (dict): Stored pose data includes localRotation, nodeIndex, and nodeObjectName

    Returns:
        list[PoseRange] Used to store poses for keyframe interpolation
    """
    # Create a HandNodes container and poseRangeArray list
    HandUtils = HandNodes()
    poseRangeArray = range(len(selectedFingers.keys()))

    for poseIndex, fingerKey in enumerate(selectedFingers.keys()):
        # fingerKey is the rig name
        fingers = selectedFingers[fingerKey]
        inputCharacter = fingers.character

        # Create a PoseRange container and add the fingers to it
        poseRange = PoseRange()
        poseRange.handNodes = HandUtils.extractComponents(
            inputCharacter, leftBoneIdArray, rightBoneIdArray, extractFkControllers=True,
        )

        # Store the end pose, if one does not exist then create it
        if inputEndPose is None:
            poseRange.endPose.buildFromCharacter(inputCharacter, leftBoneIdArray, rightBoneIdArray)
        else:
            poseRange.endPose = inputEndPose
        poseRangeArray[poseIndex] = poseRange

    return poseRangeArray


def CollectStartPoseArray(selectedFingers, poseRangeArray, inputPose):
    """
    Adds the start pose to the pose array used for interpolation

    Args:
        selectedFingers (dict): Gotten from ExtractFingersFromSelection()
        poseRangeArray (list): List of hand poses (at this point this list should only include the end pose)
        inputPose (dict): End pose data
    """
    for poseIndex, fingerKey in enumerate(selectedFingers.keys()):
        poseRangeArray[poseIndex].startPose = inputPose


def CollectAnimationCurves(inputHandSettings, poseRangeArray, selectedFingers):
    """
    Args:
        inputHandSettings (HandInterpolationSettings): HandInterpolationSettings() Class
        poseRangeArray (list): List of hand poses (at this point this list should only include the end pose)
        selectedFingers (dict): Gotten from ExtractFingersFromSelection()

    Returns:
        list[CurveComponents]
    """
    # Create empty list to add curve components to
    curveComponentsArray = []
    for poseIndex, fingerKey in enumerate(selectedFingers.keys()):
        # fingerKey is the rig name
        fingers = selectedFingers[fingerKey]
        # fingerControllers is a list of FingerController containers (returned by GetValidFingerController)
        fingerControllers = GetValidFingerController(
            poseRangeArray[poseIndex].startPose, poseRangeArray[poseIndex].handNodes, fingers,
        )
        currentHandCurveComponents = []
        # Iterate through fingerControllers and store the curve data for each in the curveComponentsArray list
        for fingerController in fingerControllers:
            curveComponents = CurveComponents()
            curveComponents.collectAndCleanTimeRange(fingerController.handNode, inputHandSettings)
            currentHandCurveComponents.append(curveComponents)
        curveComponentsArray.append(currentHandCurveComponents)

    return curveComponentsArray


def ComputeHandAnimation(
    handBlendingValue,
    inputHandSettings,
    inputStartPose,
    inputEndPose,
    leftBoneIdArray,
    rightBoneIdArray,
    leftFingerEffectorArray,
    rightFingerEffectorArray,
):
    """
    Figures out the selected nodes and then runs the interpolation computation

    Args:
        handBlendingValue (float): From the UI slider
        inputHandSettings (HandInterpolationSettings): HandInterpolationSettings() Class
        inputStartPose (dict): Start pose data
        inputEndPose (dict): End pose data
        leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
        rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)
        leftFingerEffectorArray (list): List of left side IK finger effectors as strings
        rightFingerEffectorArray (list): List of right side IK finger effectors as strings
    """
    # Get the selected models.
    currentSelection = mobu.FBModelList()
    mobu.FBGetSelectedModels(currentSelection)

    # Get all finger nodes
    fingerNodes = ExtractFingersFromSelection(
        leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray,
    )
    selectedFingersNodes = []
    for fingerKey in fingerNodes.keys():
        fingers = fingerNodes[fingerKey]
        selectedFingersNodes.extend(fingers.fingerArray)

    # Run the interpolation wrapped up in a single undo
    with ContextManagers.Undo(undoStackName="HandPoseDoubleClick", modelList=selectedFingersNodes):
        ComputeHandInterpolation(
            handBlendingValue,
            fingerNodes,
            inputHandSettings,
            inputStartPose,
            inputEndPose,
            leftBoneIdArray,
            rightBoneIdArray,
        )
        for selection in currentSelection:
            selection.Selected = True


def ComputeHandInterpolation(
    handBlendingValue,
    selectedFingers,
    inputHandSettings,
    inputStartPose,
    inputEndPose,
    leftBoneIdArray,
    rightBoneIdArray,
):
    """
    Runs the interpolation computation

    Args:
        handBlendingValue (float): From the UI slider
        selectedFingers (dict): Gotten from ExtractFingersFromSelection()
        inputHandSettings (HandInterpolationSettings): HandInterpolationSettings() Class
        inputStartPose (dict): Start pose data
        inputEndPose (dict): End pose data
        leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
        rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)
    """
    # Make sure there is something to compute
    if selectedFingers is None or len(selectedFingers.keys()) == 0:
        return

    layerIndex = mobu.FBSystem().CurrentTake.GetCurrentLayer()

    # Detect subAnimation layer mode
    baseAnimationMode = True
    if layerIndex > 0:
        baseAnimationMode = False

    # Create the pose list with the end pose
    poseRangeArray = PreparePoseRangeArray(
        selectedFingers, leftBoneIdArray, rightBoneIdArray, inputEndPose=inputEndPose,
    )

    # Add the start pose to the pose list
    CollectStartPoseArray(selectedFingers, poseRangeArray, inputStartPose)

    # Compute the keyframe offsets for interpolation
    offsetKeyframes = ComputeHandSegmentTimeRange(handBlendingValue, inputHandSettings)

    startFrames, endFrames = ComputeFingerSegmentTimeRange(
        offsetKeyframes[0], offsetKeyframes[1], handBlendingValue, inputHandSettings,
    )

    startOffsetKeyframes = list(startFrames)
    startOffsetKeyframes.extend(startFrames)
    endOffsetKeyframes = list(endFrames)
    endOffsetKeyframes.extend(endFrames)

    # Add 2 more to the list for the L & R PH bones, so the list matches the len of "poseData" list
    startOffsetKeyframes.append(startOffsetKeyframes[0])
    startOffsetKeyframes.append(startOffsetKeyframes[0])
    endOffsetKeyframes.append(endOffsetKeyframes[0])
    endOffsetKeyframes.append(endOffsetKeyframes[0])

    # Get list of fingers and their curve data
    curveComponentsArray = CollectAnimationCurves(inputHandSettings, poseRangeArray, selectedFingers)

    # Iterate through the fingers and
    for poseIndex, fingerKey in enumerate(selectedFingers.keys()):
        # Get the finger controls
        fingers = selectedFingers[fingerKey]
        fingerControllers = GetValidFingerController(
            poseRangeArray[poseIndex].startPose, poseRangeArray[poseIndex].handNodes, fingers,
        )

        # Remove data from finger effectors
        PruneFingerComponents(fingerControllers, fingers)

        # Apply animation with curve data to the fingers
        if baseAnimationMode is True:
            # This is is an animation layer other than BaseAnimation exists
            WriteAnimationCurve(
                fingerControllers,
                inputHandSettings,
                poseRangeArray[poseIndex],
                startOffsetKeyframes,
                endOffsetKeyframes,
            )
        else:
            RippleFingerKeyTimes(fingerControllers, inputHandSettings, startOffsetKeyframes, endOffsetKeyframes)
            ClampTangent(fingerControllers, inputHandSettings)
            for curveComponents in curveComponentsArray[poseIndex]:
                curveComponents.restoreAnimationRangeSideKeys()

    curveComponentsArray[:] = []


def ComputeHandSegmentTimeRange(blendWeight, inputHandSettings):
    """
    Calculate the interpolation between the start and end frames for "Hand Interpolation Order"

    Args:
        blendWeight (float): From the UI slider
        inputHandSettings (HandInterpolationSettings): HandInterpolationSettings() Class

    Returns:
        list[[start keyframe data], [end keyframe data]]
    """
    # Determine if interpolation starts from the thumb or the pinky
    reverseHandOrder = False
    if inputHandSettings.handInterpolation == "pinky":
        reverseHandOrder = True
    # Get a AnimSubRange container with necessary values for computation
    animSubRange = ComputeAnimSubRange(inputHandSettings.startTime, inputHandSettings.endTime, "hand")

    # Take values from animSubRange and run calculations used for keyframe values
    startShiftValue = float(animSubRange.startTimeValue)
    endShiftValue = float(animSubRange.endTimeValue)
    handRange = endShiftValue - startShiftValue
    segmentRatio = handRange / 5.0
    endSegmentRatio = handRange - ((handRange - segmentRatio) * blendWeight)
    startSegmentRatio = (handRange - endSegmentRatio) / 4.0

    startOffsets = []
    endOffsets = []

    keyframeValues = []

    # Get calculated values for start and end frames
    for fingerIndex in xrange(5):
        startFrame = fingerIndex * startSegmentRatio
        endFrame = endSegmentRatio + startFrame

        startFrameValue = startShiftValue + startFrame
        endFrameValue = startShiftValue + endFrame

        keyframeValues.append((startFrameValue, endFrameValue))
    if reverseHandOrder is True:
        keyframeValues.reverse()
    handComponentCount = 5

    # Create a list of tuples that contain where the first value is # of finger segments and the second value is
    # the index the first segment of the finger starts at in relation to full list of fingers
    # Example: [(3, 0), (4, 3), (4, 7), (4, 11), (4, 15)]
    fkPartsForRange = getFkParts(
        Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=True,
    )
    idRange = getIdRange(fkPartsForRange)

    # Add finger bones
    for fingerIndex in range(handComponentCount):
        currentFingerIds = idRange[fingerIndex]
        for boneIndex in range(currentFingerIds[0]):
            startOffsets.append(keyframeValues[fingerIndex][0])
            endOffsets.append(keyframeValues[fingerIndex][1])
    return startOffsets, endOffsets


def ComputeFingerOffsets(startShiftValue, endShiftValue, blendWeight, fingerBoneCount):
    """
    Used in ComputeFingerSegmentTimeRange to get keyframe values for finger interpolation

    Args:
        startShiftValue (float): Start keyframe offset
        endShiftValue (float): End keyframe offset
        blendWeight (float): From the UI slider
        fingerBoneCount (int): Number of bone nodes in the current finger

    Returns:
        list[(startFrameValue, endFrameValue)]
    """
    handRange = endShiftValue - startShiftValue
    segmentRatio = handRange / float(fingerBoneCount)
    endSegmentRatio = handRange - ((handRange - segmentRatio) * blendWeight)
    startSegmentRatio = (handRange-endSegmentRatio)/float(fingerBoneCount-1)
    keyframeValues = []

    for fingerIndex in xrange(fingerBoneCount):
        startFrame = fingerIndex*startSegmentRatio
        endFrame = endSegmentRatio+startFrame
        startFrameValue = startShiftValue + startFrame
        endFrameValue = startShiftValue + endFrame
        keyframeValues.append((startFrameValue, endFrameValue))

    return keyframeValues


def ComputeFingerSegmentTimeRange(startOffsetKeyframes, endOffsetKeyframes, blendWeight, inputHandSettings):
    """
    Calculate the interpolation between the start and end frames for "Bone Interpolation Order"

    Args:
        startOffsetKeyframes (list): Start keyframe data for fingers
        endOffsetKeyframes (list): End keyframe data for fingers
        blendWeight (float): From the UI slider
        inputHandSettings (HandInterpolationSettings): HandInterpolationSettings() Class

    Returns:
        list[(startKeyframes, endKeyframes)]
    """
    startKeyframes = range(len(startOffsetKeyframes))
    endKeyframes = range(len(endOffsetKeyframes))

    reverseHandOrder = False
    if inputHandSettings.boneInterpolation == "tipBone":
        reverseHandOrder = True

    handComponentCount = 5

    # Create a list of tuples that contain where the first value is # of finger segments and the second value is
    # the index the first segment of the finger starts at in relation to full list of fingers
    # Example: [(3, 0), (4, 3), (4, 7), (4, 11), (4, 15)]
    fkPartsForRange = getFkParts(Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=True)
    idRange = getIdRange(fkPartsForRange)

    # Iterate through fingers
    for fingerIndex in range(handComponentCount):
        currentFingerIds = idRange[fingerIndex]
        offsetIndex = currentFingerIds[1]
        fingerBoneCount = currentFingerIds[0]
        startShiftValue = float(startOffsetKeyframes[offsetIndex])
        endShiftValue = float(endOffsetKeyframes[offsetIndex])

        # Calculate the keyframe offsets
        keyframeValues = ComputeFingerOffsets(startShiftValue, endShiftValue, blendWeight, fingerBoneCount)

        if reverseHandOrder is True:
            keyframeValues.reverse()

        for boneIndex in range(currentFingerIds[0]):
            offsetIndex = currentFingerIds[1]+boneIndex
            startKeyframes[offsetIndex] = keyframeValues[boneIndex][0]
            endKeyframes[offsetIndex] = keyframeValues[boneIndex][1]
    return startKeyframes, endKeyframes


def ClampTangent(fingerControllers, inputHandSettings):
    """
    Set animation keyframes to tangent mode FBTangentMode.kFBTangentModeClampProgressive

    Args:
        fingerControllers (list): List of the finger controls
        inputHandSettings (HandInterpolationSettings): HandInterpolationSettings() Class
    """
    startValue = inputHandSettings.startTime.GetSecondDouble()
    endValue = inputHandSettings.endTime.GetSecondDouble()

    for fingerController in fingerControllers:
        rotationProperty = fingerController.handNode.PropertyList.Find("Rotation (Lcl)")
        rotationAnimNode = rotationProperty.GetAnimationNode()
        subAnimationNodes = rotationAnimNode.Nodes

        for subNode in subAnimationNodes:
            for animationKey in subNode.FCurve.Keys:
                inputTimeValue = animationKey.Time.GetSecondDouble()
                if inputTimeValue >= startValue and inputTimeValue <= endValue:
                    animationKey.TangentMode = mobu.FBTangentMode.kFBTangentModeClampProgressive


def RippleFingerKeyTimes(fingerControllers, inputHandSettings, startOffsetKeyframes, endOffsetKeyframes):
    """
    Applies the keyframe offsets to the finger controls

    Args:
        fingerControllers (list): List of the finger controls
        inputHandSettings (HandInterpolationSettings): HandInterpolationSettings() Class
        startOffsetKeyframes (list): List of start offset keys
        endOffsetKeyframes (list): List of end offset keys
    """
    if startOffsetKeyframes is None or endOffsetKeyframes is None:
        return
    startValue = inputHandSettings.startTime.GetSecondDouble()
    endValue = inputHandSettings.endTime.GetSecondDouble()

    for fingerController in fingerControllers:
        rotationProperty = fingerController.handNode.PropertyList.Find("Rotation (Lcl)")

        rotationAnimNode = rotationProperty.GetAnimationNode()
        subAnimationNodes = rotationAnimNode.Nodes

        startOffsetTime = mobu.FBTime()
        startOffsetTime.SetFrame(0, mobu.FBTimeMode.kFBTimeMode30Frames)
        startOffsetTime.SetSecondDouble(startOffsetKeyframes[fingerController.samplingIndex])

        endOffsetTime = mobu.FBTime()
        endOffsetTime.SetFrame(0, mobu.FBTimeMode.kFBTimeMode30Frames)
        endOffsetTime.SetSecondDouble(endOffsetKeyframes[fingerController.samplingIndex])

        for subNode in subAnimationNodes:
            for animationKey in subNode.FCurve.Keys:
                inputTimeValue = animationKey.Time.GetSecondDouble()
                if inputTimeValue >= startValue and inputTimeValue <= endValue:
                    if inputTimeValue > startValue:
                        animationKey.Time = endOffsetTime
                    if inputTimeValue < endValue:
                        animationKey.Time = startOffsetTime


def WriteAnimationCurve(
    fingerControllers, inputHandSettings, poseRange, startOffsetKeyframes=None, endOffsetKeyframes=None,
):
    """
    Run when there are animation layers and adds keyframes with cubic interpolation and clamped tangents

    Args:
        fingerControllers (list): List of the finger controls
        inputHandSettings (HandInterpolationSettings): HandInterpolationSettings() Class
        poseRange (list): List of stored poses

    Kwargs:
        startOffsetKeyframes (list): List of start offset keys
        endOffsetKeyframes (list): List of end offset keys
    """
    for fingerController in fingerControllers:
        startPoseValue = poseRange.startPose.bonePoseArray[fingerController.samplingIndex]
        endPoseValue = poseRange.endPose.bonePoseArray[fingerController.samplingIndex]

        if startOffsetKeyframes is not None and endOffsetKeyframes is not None:
            startOffsetTime = mobu.FBTime()
            startOffsetTime.SetFrame(0, mobu.FBTimeMode.kFBTimeMode30Frames)
            if not len(startOffsetKeyframes) <= fingerController.samplingIndex:
                startOffsetTime.SetSecondDouble(startOffsetKeyframes[fingerController.samplingIndex])
            endOffsetTime = mobu.FBTime()
            endOffsetTime.SetFrame(0, mobu.FBTimeMode.kFBTimeMode30Frames)
            if not len(endOffsetKeyframes) <= fingerController.samplingIndex:
                endOffsetTime.SetSecondDouble(endOffsetKeyframes[fingerController.samplingIndex])
            fingerController.rotationAnimationNode.KeyAdd(
                startOffsetTime,
                startPoseValue.localRotation,
                mobu.FBInterpolation.kFBInterpolationCubic,
                mobu.FBTangentMode.kFBTangentModeClampProgressive,
            )
            fingerController.rotationAnimationNode.KeyAdd(
                endOffsetTime,
                endPoseValue.localRotation,
                mobu.FBInterpolation.kFBInterpolationCubic,
                mobu.FBTangentMode.kFBTangentModeClampProgressive,
            )
        else:
            fingerController.rotationAnimationNode.KeyAdd(
                inputHandSettings.startTime,
                startPoseValue.localRotation,
                mobu.FBInterpolation.kFBInterpolationCubic,
                mobu.FBTangentMode.kFBTangentModeClampProgressive,
            )
            fingerController.rotationAnimationNode.KeyAdd(
                inputHandSettings.endTime,
                endPoseValue.localRotation,
                mobu.FBInterpolation.kFBInterpolationCubic,
                mobu.FBTangentMode.kFBTangentModeClampProgressive,
            )


def GetValidFingerController(poseData, handNodes, fingers):
    """
    Return a list of finger controls from selected hand(s)

    Args:
        poseData (dict): Pose data from a single pose
        handNodes (list): List of finger models
        fingers (dict): Container from SelectedCharacterFingers, includes character name and all finger names in a list

    Returns:
        list[FingerController]
    """
    fingerControllers = []
    handDict = {}
    for control in handNodes:
        if control:
            handDict[control.Name] = control
    for boneIndex, bone in enumerate(poseData.bonePoseArray):
        if handNodes[boneIndex] is None:
            continue
        if handNodes[boneIndex] not in fingers.fingerArray:
            continue
        rotationProperty = handNodes[boneIndex].PropertyList.Find("Rotation (Lcl)")
        if not rotationProperty.IsAnimated():
            rotationProperty.SetAnimated(True)
        rotationAnimNode = rotationProperty.GetAnimationNode()
        fingerController = FingerController(handNodes[boneIndex], rotationAnimNode, boneIndex)
        fingerControllers.append(fingerController)
    return fingerControllers


def ComputeAnimSubRange(startTime, endTime, offsetType):
    """
    Creates a container for storing data needed to run interpolation calculation

    Args:
        startTime (pyfbsdk.FBTime): HandInterpolationSettings.startTime
        endTime (pyfbsdk.FBTime): HandInterpolationSettings.endTime
        offsetType (str): thumb, hand, or finger

    Returns:
        AnimSubRange
    """
    startTimeValue = startTime.GetSecondDouble()
    endTimeValue = endTime.GetSecondDouble()

    splitCount = 1.0
    if offsetType == "thumb":
        splitCount = 3.0
    elif offsetType == "hand":
        splitCount = 5.0
    elif offsetType == "finger":
        splitCount = 4.0
    animSubRange = AnimSubRange(startTimeValue, endTimeValue, int(splitCount))

    return animSubRange


def PruneFingerComponents(fingerControllers, fingers):
    """
    Remove animation on finger effector

    Args:
        fingerControllers (list): List of finger controls found in the rig
        fingers (dict): Container from SelectedCharacterFingers, includes character name and all finger names in a list
    """
    fingerComponents = [finger.handNode for finger in fingerControllers]
    effectors = list(set(fingers.fingerArray) - set(fingerComponents))

    fingerHierarchy = []

    for finger in fingerControllers:
        fingerHierarchy.extend(finger.handNode.Children)
    endBones = list(set(fingerHierarchy) - set(fingerComponents))
    effectors.extend(endBones)

    for effector in effectors:
        if effector:
            if "Wrist" in effector.Name:
                continue
            rotationProperty = effector.PropertyList.Find("Rotation (Lcl)")

            rotationAnimNode = rotationProperty.GetAnimationNode()
            if rotationAnimNode is None:
                continue
            subAnimationNodes = rotationAnimNode.Nodes

            for subNode in subAnimationNodes:
                subNode.FCurve.EditClear()
            rotationProperty.SetAnimated(False)


def getIkParts(inputCharacter, side, asString=True, forceDynamicLookup=False):
    """
    Returns a list of ik finger effectors as strings, pyfbsdk.FBModelMarker objects, or pyfbsdk.FBEffectorId objects

    Args:
        inputCharacter (pyfbsdk.FBCharacter)
        side (str): Left/Right
        asString (bool): True/False
        forceDynamicLookup (bool): Override the static configuration

    Returns:
        list[mobu.FBEffector] or list[mobu.FBModelMarker] or list[str]
    """
    ikPartsList = []
    if inputCharacter:
        # If the current project has HandPoseInfo data, grab that
        if const.HandConfig.HAS_CONFIG and not forceDynamicLookup:
            if side == "Left":
                ikPartsList = const.HandConfig.LEFT_FINGER_EFFECTOR_ARRAY
            else:
                ikPartsList = const.HandConfig.RIGHT_FINGER_EFFECTOR_ARRAY
        # If no HandPoseInfo data is found for the current project, generate the data dynamically
        else:
            ikModelPartsList = Lib.GetIKControlsFromCharacter(inputCharacter)
            # Filter parts names into a new list of strings
            ikPartsDict = {}
            for part in ikModelPartsList:
                ikPartsList.append(part.Name)
                ikPartsDict[part.Name] = part
            ikPartsList = const.Parts().sortParts(ikPartsList, side)
            if not asString:
                partsList = []
                for ikPart in ikPartsList:
                    partsList.append(ikPartsDict[ikPart])
                ikPartsList = partsList
    return ikPartsList


def getFkParts(inputCharacter, side, asString=True, forceDynamicLookup=False):
    """
    Returns a list of fk finger components as either strings or pyfbsdk.FBModelMarker objects

    Args:
        inputCharacter (pyfbsdk.FBCharacter)
        side (str): Left/Right
        asString (bool): True/False
        forceDynamicLookup (bool): Override the static configuration

    Returns:
        list[mobu.FBModelMarker] or list[str]
    """
    fkPartsList = []
    if inputCharacter:
        # If the current project has HandPoseInfo data, grab that
        if const.HandConfig.HAS_CONFIG and not forceDynamicLookup:
            if side == "Left":
                fkPartsList = const.HandConfig.LEFT_CONTROL_NAMES
            else:
                fkPartsList = const.HandConfig.RIGHT_CONTROL_NAMES
        # If no HandPoseInfo data is found for the current project, generate the data dynamically
        else:
            fkModelPartsList = Lib.GetFKControlsFromCharacter(inputCharacter)

            # Filter parts names into a new list of strings but ignore anything without a child (end bones/nubs)
            fkPartsDict = {}

            # Need a better way to handle this check. GTA V does not always have "NUB" bones so can't filter by
            # checking for children
            useChildCheck = False

            for part in fkModelPartsList:
                if "InHand" in part.Name:
                    useChildCheck = True
                    break
            for part in fkModelPartsList:
                if useChildCheck:
                    if part.Children:
                        fkPartsList.append(part.Name)
                        fkPartsDict[part.Name] = part
                else:
                    fkPartsList.append(part.Name)
                    fkPartsDict[part.Name] = part
            fkPartsList = const.Parts().sortParts(fkPartsList, side)
            if not asString:
                partsList = []
                for fkPart in fkPartsList:
                    partsList.append(fkPartsDict[fkPart])
                fkPartsList = partsList
    return fkPartsList


def getFkIdNodes(partsList=None, side=None, forceDynamicLookup=False):
    """
    Returns a list of fk finger components as strings, pyfbsdk.FBModelMarker objects, or pyfbsdk.FBBodyNodeId objects

    Kwargs:
        partsList (list): List of finger parts as strings
        side (str): Left/Right
        forceDynamicLookup (bool): Override the static configuration

    Returns:
        list[mobu.FBBodyNodeId] or list[mobu.FBModelMarker] or list[str]
    """
    side = side or ""
    bodyNodes = {}
    fkIdNodes = []
    character = Globals.Application.CurrentCharacter
    if character:
        # If the current project has HandPoseInfo data, grab that
        if const.HandConfig.HAS_CONFIG and not forceDynamicLookup:
            if side == "Left":
                fkIdNodes = const.HandConfig.LEFT_ID_ARRAY
            else:
                fkIdNodes = const.HandConfig.RIGHT_ID_ARRAY
        # If no HandPoseInfo data is found for the current project, generate the data dynamically
        else:
            for bodyNodeIdName in dir(mobu.FBBodyNodeId):
                if "kFB" in bodyNodeIdName and not re.search("Translation|Invalid|Last", bodyNodeIdName):
                    rigModel = character.GetCtrlRigModel(getattr(mobu.FBBodyNodeId, bodyNodeIdName))
                    if rigModel:
                        if rigModel.Name in partsList:
                            bodyNodes[rigModel.Name] = bodyNodeIdName
            for part in partsList:
                fkIdNodes.append(bodyNodes[part])
    return fkIdNodes


def getIdRange(partsList=None):
    """
    Returns a list of "id range" tuples in the format of (# of finger segments, Index that first segment starts)

    Args:
        partsList (list): List of finger parts as strings

    Returns:
        list[(int), (int)]
    """
    idRange = []
    # If the current project has HandPoseInfo data, grab that
    if const.HandConfig.HAS_CONFIG:
        idRange = const.HandConfig.ID_RANGE
    # If no HandPoseInfo data is found for the current project, generate the data dynamically
    else:
        if partsList:
            indexCount = 0
            for partName in const.Parts.ALL_PARTS:
                matching = [part for part in partsList if partName in part]
                idRange.append((len(matching), indexCount))
                indexCount += len(matching)
    return idRange


def getHandMapping(partsList):
    """
    Returns a list of finger segments based on how many of each type exist

    Args:
        partsList (list): List of finger parts as strings

    Returns:
        list[str]
    """
    handMapping = []
    # If the current project has HandPoseInfo data, grab that
    if const.HandConfig.HAS_CONFIG:
        handMapping = const.HandConfig.HAND_MAPPING
    # If no HandPoseInfo data is found for the current project, generate the data dynamically
    else:
        if partsList:
            for partName in const.Parts.ALL_PARTS:
                matching = [part for part in partsList if partName in part]
                for piece in xrange(len(matching)):
                    handMapping.append(partName)
    return handMapping
