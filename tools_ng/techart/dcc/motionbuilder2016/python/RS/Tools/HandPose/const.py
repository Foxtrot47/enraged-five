from RS import ProjectData


class Settings(object):
    DEFAULT_LAYOUT_MODE = "vertical"


class Sides(object):
    LEFT = "Left"
    RIGHT = "Right"


class Parts(object):
    """ Contains all base part names from the hand and a function to sort them dynamically """
    THUMB = "Thumb"
    INDEX = "Index"
    MIDDLE = "Middle"
    RING = "Ring"
    PINKY = "Pinky"
    WRIST = "Wrist"
    ALL_PARTS = (THUMB, INDEX, MIDDLE, RING, PINKY, WRIST)
    PH_COMPONENTS = ("PH_L_Hand", "PH_R_Hand")

    @classmethod
    def sortParts(cls, partsList=None, side=None):
        """
        Sorts parts and stores them in a list based on a specific format the hand pose tool expects when applying a pose

        Args:
            partsList (list): List of parts as strings
            side (str): Left or Right

        Returns:
            list(str): List strings of body parts sorted based on this order -> Thumb, Index, Middle, Ring, Pinky, Wrist
        """
        # Lists that will be used to sort each part type together
        sortedPartsDict = {
            Parts.THUMB: [],
            Parts.INDEX: [],
            Parts.MIDDLE: [],
            Parts.RING: [],
            Parts.PINKY: [],
            Parts.WRIST: [],
        }

        # Do the sorting of parts into each respective list
        for bone in partsList:
            if bone.startswith(side):
                if Parts.THUMB in bone:
                    sortedPartsDict[Parts.THUMB].append(bone)
                elif Parts.INDEX in bone:
                    sortedPartsDict[Parts.INDEX].append(bone)
                elif Parts.MIDDLE in bone:
                    sortedPartsDict[Parts.MIDDLE].append(bone)
                elif Parts.RING in bone:
                    sortedPartsDict[Parts.RING].append(bone)
                elif Parts.PINKY in bone:
                    sortedPartsDict[Parts.PINKY].append(bone)
                elif Parts.WRIST in bone:
                    sortedPartsDict[Parts.WRIST].append(bone)

        # Sort each list to order everything correctly
        for finger in sortedPartsDict:
            sortedPartsDict[finger] = sorted(sortedPartsDict[finger])
            for bone in sortedPartsDict[finger]:
                # InHand bones should be first in the order if they exist
                if "InHand" in bone:
                    sortedPartsDict[finger].insert(0, sortedPartsDict[finger].pop(sortedPartsDict[finger].index(bone)))

        # Sort the parts into a list of lists where each separate list is a part type ([Thumb], [Index], etc)]
        sortedParts = (sortedPartsDict[part] for part in Parts.ALL_PARTS)

        # Take the sorted parts and return all of them sorted out in order in a single list
        return tuple(part for partList in sortedParts for part in partList)


class HandConfig(object):
    """ Contains data for the character hand configuration """

    # Create a bool to check for data
    if ProjectData.data.HAND_CONFIG:
        HAS_CONFIG = True
    else:
        HAS_CONFIG = False

    LEFT_FINGER_EFFECTOR_ARRAY = ProjectData.data.HAND_CONFIG.LeftFingerEffectorArray
    RIGHT_FINGER_EFFECTOR_ARRAY = ProjectData.data.HAND_CONFIG.RightFingerEffectorArray
    LEFT_CONTROL_NAMES = ProjectData.data.HAND_CONFIG.LeftControlNames
    RIGHT_CONTROL_NAMES = ProjectData.data.HAND_CONFIG.RightControlNames
    LEFT_ID_ARRAY = ProjectData.data.HAND_CONFIG.LeftIdArray
    RIGHT_ID_ARRAY = ProjectData.data.HAND_CONFIG.RightIdArray
    ID_RANGE = ProjectData.data.HAND_CONFIG.IdRange
    HAND_MAPPING = ProjectData.data.HAND_CONFIG.HandMapping
