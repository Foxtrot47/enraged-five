import inspect
import os

from PySide import QtGui, QtCore

from RS import Config
from RS import ProjectData
from RS.Tools import UI
from RS.Tools.HandPose.Models import PoseControl
from RS.Tools.HandPose.Widgets import poseControlWidget


class HandPoseUI(UI.QtMainWindowBase):

    def __init__(self):
        self._toolName = "Hand Pose Tool"

        super(HandPoseUI, self).__init__(title=self._toolName, size=(455, 500), dockable=False)
        self.setupUi()

    def setupUi(self):
        # Layouts
        self.mainLayout = QtGui.QVBoxLayout()

        # Widgets
        # Set the tab array and create the tab widget
        self.toolTabArray = [
            (self._buildPoseIterpolationTab(), "Pose Tools"),
            (self._buildPoseLibrarySetupTab(), "Edit Pose Library"),
        ]
        self._tabWidget = self._buildToolTabWidget(self.toolTabArray)

        banner = UI.BannerWidget(
            self._toolName, helpUrl="https://hub.rockstargames.com/display/RSGTECHART/Hand+Pose+Tool",
        )
        self.mainWidget = QtGui.QWidget()
        self.posePickerWidget = self._createPoseLibraryPicker()

        # Assign Layouts and Widgets
        self.mainLayout.addWidget(banner)
        self.mainLayout.addWidget(self.posePickerWidget)
        self.mainLayout.addWidget(self._tabWidget)
        self.mainWidget.setLayout(self.mainLayout)
        self.setCentralWidget(self.mainWidget)

        # Configure Widgets
        self.mainWidget.setAutoFillBackground(True)
        self._swapTabVisibility(0)

        # Connections
        self._tabWidget.currentChanged.connect(self._swapTabVisibility)

    def _buildToolTabWidget(self, toolTabArray):
        """
        Creates the main tab widget to organize the main tool and the edit/create pose file features

        Args:
            toolTabArray (list): List of tabs to be added to the main UI

        Returns:
            QtGui.QTabWidget()
        """
        self.hiddenTabArray = []
        self.tabWidgetArray = []

        # Main Tab Widget
        tabWidget = QtGui.QTabWidget()
        # Configure Main Tab Widget
        tabWidget.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)

        # Create tab "holders" that are used for show/hidden swapping of tabs when active/inactive
        for inputTool in toolTabArray:
            anchorWidget = self._createTabHolder("{0}_Anchor".format(inputTool[1]))
            widgetHidden = self._createTabHolder("{0}_Hidden".format(inputTool[1]))
            widgetHidden["widget"].hide()

            tabWidget.addTab(inputTool[0], inputTool[1])
            tabWidget.addTab(anchorWidget["widget"], inputTool[1])
            anchorWidget["layout"].addWidget(inputTool[0])

            self.mainLayout.addWidget(widgetHidden["widget"])

            self.hiddenTabArray.append(widgetHidden)
            self.tabWidgetArray.append(anchorWidget)

            inputTool[0].show()

        return tabWidget

    def _swapTabVisibility(self, tabIndex):
        """
        Adds all tabs to hiddenTabArray and adds tab at the passed index to tabWidgetArray which holds the anchor widget

        Args:
            tabIndex (int): Number representing a tab widget
        """
        for widgetIndex in range(len(self.hiddenTabArray)):
            self.hiddenTabArray[widgetIndex]["layout"].addWidget(self.toolTabArray[widgetIndex][0])

        self.tabWidgetArray[tabIndex]["layout"].addWidget(self.toolTabArray[tabIndex][0])
        self._refreshWindow()

    def _refreshWindow(self):
        """ Refreshes the UI when tabs are swapped """
        self._updateWindowHeight()
    
        QtGui.QApplication.processEvents()
        QtCore.QTimer.singleShot(0, self._updateWindowHeight)

    def _updateWindowHeight(self):
        """ Re-sizes the window and is called whenever a tab swap occurs """
        self._tabWidget.adjustSize() 
        self._tabWidget.updateGeometry()

        self.mainWidget.adjustSize() 
        self.mainWidget.updateGeometry()

        self.resize(self._tabWidget.sizeHint().width(),self._tabWidget.sizeHint().height())
        self.resize(self.mainLayout.sizeHint().width(),self.mainLayout.sizeHint().height())

    def _createTabHolder(self, widgetName):
        """
        Creates a tab "holder" that holds the main UI tabs that get swapped between active/inactive

        Args:
            widgetName (str): Name of widget (prepended with {0}_Anchor or {0}_Hidden)

        Returns:
            dict: Holds the widget and layout of the holder the tab widget is stored in
        """
        layout = QtGui.QVBoxLayout()
        widget = QtGui.QWidget()

        layout.setContentsMargins(0, 0, 0, 0) 
        layout.setSpacing(0) 

        widget.setLayout(layout)
        widget.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        widget.setObjectName(widgetName)

        return {
            "widget":widget,
            "layout":layout,
        }

    def _buildPoseLibrarySetupTab(self):
        """"
        Creates the main widget for the tab for editing/creating and saving a pose library file

        Returns:
            QtGui.QWidget: Holds the entire "edit pose library" tab content
        """
        # Create an empty pose control widget (to be populated with poses in MoBu's built in "Pose Controls")
        self.sceneControlWidget = poseControlWidget.PoseControlWidget()

        # Get the filepath to the active pose library
        userPoseLibrary = self._exposePoseLibrary()
        # Create a pose control widget that is populated with the active pose library
        self.editPoseLibraryControlWidget = poseControlWidget.PoseControlWidget(
            widgetLabel="Library", usePoseFromScene=False, userPoseLibrary=userPoseLibrary,
        )

        # Creates the widget for exporting/saving a pose library
        posePickerTab = self.createPoseLibraryExporter()

        # Layouts
        controlWidgetParentLayout = QtGui.QVBoxLayout()
        controlLayout = QtGui.QHBoxLayout()

        # Widgets
        controlWidgetParent = QtGui.QWidget()
        controlWidget = QtGui.QWidget()

        # Add Widgets to Layouts
        controlWidgetParentLayout.addWidget(controlWidget)
        controlWidgetParentLayout.addWidget(posePickerTab)
        controlWidgetParentLayout.addWidget(
            QtGui.QLabel("Before exporting, save/reopen your scene to generate thumbnails for new poses.")
        )
        controlLayout.addWidget(self.editPoseLibraryControlWidget)
        controlLayout.addWidget(self.sceneControlWidget)

        # Set Layouts to Widgets
        controlWidget.setLayout(controlLayout)
        controlWidgetParent.setLayout(controlWidgetParentLayout)

        # Configure Layout
        controlWidgetParentLayout.setContentsMargins(0, 0, 0, 0)
        controlWidgetParentLayout.setSpacing(5)

        return controlWidgetParent

    def _buildPoseIterpolationTab(self):
        """
        Creates the main widget for the tab for applying poses and interpolation

        Returns:
            QtGui.QWidget: Holds the entire "pose tools" tab contents
        """
        # Get the filepath to the active pose library
        userPoseLibrary = self._exposePoseLibrary()
        # Create a pose control widget that is populated with the active pose library
        self.poseLibraryControlWidget = poseControlWidget.PoseControlWidget(
            widgetLabel="Hand Pose Library",
            usePoseFromScene=False,
            layoutMode="Horizontal",
            treeWidth=315,
            userPoseLibrary=userPoseLibrary,
            thumbnailSize=215,
        )

        # Layout
        controlLayout = QtGui.QHBoxLayout()
        # Widget
        controlWidget = QtGui.QWidget()
        # Add Widget to Layout
        controlLayout.addWidget(self.poseLibraryControlWidget)
        # Set Layout to Widget
        controlWidget.setLayout(controlLayout)

        return controlWidget

    def _createPoseLibraryPicker(self):
        """
        Creates the widget for loading the active pose file

        Returns:
            QtGui.QWidget: Widget containing the pose file picker
        """
        # Create folder icon for pose file load button
        iconFolderString = "{0}/Watchstar/".format(Config.Script.Path.ToolImages)
        refreshLibraryIcon = QtGui.QIcon()
        refreshLibraryPixmap = QtGui.QPixmap("{0}folder.png".format(iconFolderString))
        refreshLibraryIcon.addPixmap(refreshLibraryPixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)

        # Layout
        posePickerLayout = QtGui.QHBoxLayout()

        # Widgets
        posePickerWidget = QtGui.QWidget()
        self.poseFileField = QtGui.QLineEdit()
        self.poseFileButton = QtGui.QPushButton("")

        # Add Widgets to Layout
        posePickerLayout.addWidget(QtGui.QLabel("Pose File:"))
        posePickerLayout.addWidget(self.poseFileField)
        posePickerLayout.addWidget(self.poseFileButton)

        # Set Layout to Main Widget
        posePickerWidget.setLayout(posePickerLayout)

        # Configure Layout
        posePickerLayout.setContentsMargins(0, 0, 0, 0)
        posePickerLayout.setSpacing(5)

        # Configure Widgets
        self.poseFileButton.setIcon(refreshLibraryIcon)
        self.poseFileField.setReadOnly(True)
        self.poseFileField.setText(self._exposePoseLibrary())

        # Hook up connection
        self.poseFileButton.pressed.connect(self._browsePoseFiles)

        return posePickerWidget

    def createPoseLibraryExporter(self):
        """
        Creates the widget for saving out pose files

        Returns:
            QtGui.QWidget: Widget containing the pose file exporter
        """
        # Create disk icon for pose file save button
        iconFolderString = "{0}/ReferenceEditor/".format(Config.Script.Path.ToolImages)
        refreshLibraryIcon = QtGui.QIcon()
        refreshLibraryPixmap = QtGui.QPixmap("{0}SaveReference.png".format(iconFolderString))
        refreshLibraryIcon.addPixmap(refreshLibraryPixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)

        # Layouts
        controlWidgetParentLayout = QtGui.QVBoxLayout()
        poseModeLayout = QtGui.QHBoxLayout()
        posePickerLayout = QtGui.QHBoxLayout()

        # Widgets
        controlWidgetParent = QtGui.QWidget()
        posePickerTab = QtGui.QWidget()
        poseModeTab = QtGui.QGroupBox("Save To disk:")
        self.saveLibraryRbn = QtGui.QRadioButton()
        self.savePoseControlRbn = QtGui.QRadioButton()
        self.poseExporterField = QtGui.QLineEdit()
        self.poseFileButton = QtGui.QPushButton("")

        # Add Widgets To Layouts
        controlWidgetParentLayout.addWidget(self._createSeparator())
        controlWidgetParentLayout.addWidget(poseModeTab)
        controlWidgetParentLayout.addWidget(posePickerTab)
        posePickerLayout.addWidget(self.poseExporterField)
        posePickerLayout.addWidget(self.poseFileButton)
        poseModeLayout.addWidget(self.saveLibraryRbn)
        poseModeLayout.addWidget(self.savePoseControlRbn)

        # Set Layouts to Widgets
        controlWidgetParent.setLayout(controlWidgetParentLayout)
        posePickerTab.setLayout(posePickerLayout)
        poseModeTab.setLayout(poseModeLayout)

        # Configure Widgets and Layouts
        controlWidgetParentLayout.setContentsMargins(5, 5, 5, 5)
        controlWidgetParentLayout.setSpacing(5)
        posePickerLayout.setContentsMargins(0, 0, 0, 0)
        posePickerLayout.setSpacing(5)
        poseModeLayout.setContentsMargins(5, 5, 5, 5)
        poseModeLayout.setSpacing(5)
        self.saveLibraryRbn.setText("Hand pose library")
        self.savePoseControlRbn.setText("Pose Control")
        self.saveLibraryRbn.setChecked(True)
        self.poseFileButton.setIcon(refreshLibraryIcon)
        self.poseExporterField.setReadOnly(True)

        # Hook up connections
        self.poseFileButton.pressed.connect(self._browsePoseExport)

        return controlWidgetParent

    def _createSeparator(self):
        """
        Add line separator between widget

        Returns:
            QtGui.QFrame: Visual UI separator
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _exposePoseLibrary(self):
        """
        Find the preferred pose repository

        Returns:
            str: Filepath of pose file
        """
        toolDirectory = (os.path.dirname(inspect.getfile(HandPoseUI)))
        userPoseDirectory = os.path.join(os.path.dirname(toolDirectory), "Poses")

        # Load a pose file specific to the current project
        userPoseLibrary = os.path.join(userPoseDirectory, ProjectData.data.GetDefaultHandPoseLibraryName() + ".jsn")
        userPoseLibrary = userPoseLibrary.replace("\\", "/")
    
        return userPoseLibrary

    def _browsePoseFiles(self):
        """ Refreshes the active pose library from user selection of pose file """
        # Get the filepath for the pose files
        toolDirectory = (os.path.dirname(inspect.getfile(HandPoseUI)))
        userPoseDirectory = os.path.join(os.path.dirname(toolDirectory), "Poses")

        # Call file dialog widget for user to select a pose file
        filePath, cancelState = QtGui.QFileDialog.getOpenFileName(
            self, "Open Hand pose File", userPoseDirectory, "*.jsn",
        )

        # Make sure any backslashes are forward slashes and set the pose file text
        filePath = filePath.replace("\\", "/")
        self.poseFileField.setText(filePath)

        # Set the pose file/library in both the main "Pose Tools" tab and the "Edit Pose Library" tab
        for control in [self.editPoseLibraryControlWidget, self.poseLibraryControlWidget]:
            control.userPoseLibrary = filePath
            control._refreshTreeDataStructure()

    def _browsePoseExport(self):
        """ Runs the prep for saving out a pose file and then saves it out """
        # Get the filepath for the pose files
        toolDirectory = (os.path.dirname(inspect.getfile(HandPoseUI)))
        userPoseDirectory = os.path.join(os.path.dirname(toolDirectory), "Poses")

        # Call file dialog widget for user to save a pose file and set text field with filepath of new/edited pose file
        filePath, cancelState = QtGui.QFileDialog.getSaveFileName(
            self, "Save Hand pose File", userPoseDirectory, "*.jsn",
        )
        self.poseExporterField.setText(filePath)

        # If there is no filepath (if the user cancels) then return
        if not os.path.isdir(os.path.dirname(filePath)):
            return

        # Call the PoseManager which will format and save the pose file out as json
        poseFn = PoseControl.PoseManager()
        if self.saveLibraryRbn.isChecked() and self.editPoseLibraryControlWidget.rootData:
            rootData = self.editPoseLibraryControlWidget.rootData
        else:
            rootData = self.sceneControlWidget.rootData
        poseFn.savePoseLibrary(rootData, filePath)