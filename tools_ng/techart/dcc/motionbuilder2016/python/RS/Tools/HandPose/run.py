from RS.Tools.HandPose.Dialogs import handPoseUI


def Run(show=True):
    tool = handPoseUI.HandPoseUI()

    if show:
        tool.show()

    return tool
