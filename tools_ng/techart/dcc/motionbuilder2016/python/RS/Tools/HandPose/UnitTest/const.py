import os


class ConfigPathDict(object):
    """Yaml config paths for unittests"""

    HANDPOSES_CONFIG_PATH = os.path.join(os.path.dirname(__file__), "configs", "handposes.yaml")
