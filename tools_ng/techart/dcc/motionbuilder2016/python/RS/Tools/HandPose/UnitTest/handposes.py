import unittest

from RS.Unittests import utils
from RS.Tools.HandPose.UnitTest import base


def Run():
    """Run unittests from this module"""
    unittest.TextTestRunner(verbosity=2).run(suite())


def suite():
    """A suite to hold all the tests from this module

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()

    for functionName in dir(Test_HandPoses):
        if functionName.startswith("test_") and not functionName.endswith(("_Scene", "_Referencing")):
            suite.addTest(Test_HandPoses(functionName))

    return suite


class Test_HandPoses(base.HandPoseBase):
    """Tests handPoses"""

    def test_Add(self):
        """Testing adding a pose"""
        self.assertCreatePose()

    def test_PoseLibrary(self):
        """Testing that pose library exists"""
        self.assertPoseDataExists()
