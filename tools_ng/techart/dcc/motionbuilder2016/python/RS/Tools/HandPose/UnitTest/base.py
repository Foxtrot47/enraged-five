import os

import pyfbsdk as mobu

from RS import Globals, Perforce
from RS.Utils import ContextManagers
from RS.Unittests import utils
from RS.Core.ReferenceSystem.UnitTest import base
from RS.Tools.HandPose import const
from RS.Tools.HandPose import Core as handPoseCore
from RS.Tools.HandPose.Dialogs import handPoseUI
from RS.Tools.HandPose.Models import PoseControl
from RS.Tools.HandPose.UnitTest import const as testConst


class HandPoseBase(base.Base):
    """Base class for the hand pose tests, it inherits the base UnitTest class from the Reference System Unit Tests
    so it can reference assets to use for tests
    """

    KEY = "assets"

    def setUp(self):
        """Clears the scene before running the test"""
        Globals.Application.FileNew()

        # get data
        handposeConfig = utils.loadYaml(testConst.ConfigPathDict.HANDPOSES_CONFIG_PATH)
        self.handposeData = utils.getConfigKey(self.KEY, handposeConfig)

        if not self.handposeData:
            print("No config key found for {} to test asset".format(self.KEY))
            return False

        self.characterPath = self.handposeData.get("characterPath", None)

        # make a tuple of paths, filtering None
        paths = tuple(filter(None, [self.characterPath]))

        # sync all the paths
        for path in paths:
            Perforce.Sync(path, force=True)
            if not os.path.exists(path):
                self.fail(
                    "{} path doesn't exist! Manually sync this file or edit the handposes.yaml config".format(path)
                )

    def tearDown(self):
        """Clears the scene after running the test"""
        Globals.Application.FileNew()

    def assertCreatePose(self, pose=None):
        """Asserts adding a pose"""
        pose = pose if pose else self.assertPoses()[0].child(0)
        reference = self.assertCreate(self.characterPath)
        character = reference.Component()
        character.CreateControlRig(True)

        ctrlName = '{0}_Ctrl:RightHandMiddleEffector'.format(character.Name)
        marker = self.assertExists(ctrlName)
        marker.Selected = True

        # Create lists of finger effectors as FBModelMarker objects
        leftFingerEffectorArray = handPoseCore.getIkParts(
            Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=False,
        )
        rightFingerEffectorArray = handPoseCore.getIkParts(
            Globals.Application.CurrentCharacter, const.Sides.RIGHT, asString=False,
        )

        leftControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=True,
        )
        rightControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.RIGHT, asString=True,
        )
        leftBoneIdArray = handPoseCore.getFkIdNodes(leftControlNames, side=const.Sides.LEFT, )
        rightBoneIdArray = handPoseCore.getFkIdNodes(rightControlNames, side=const.Sides.RIGHT, )

        # Grab all left/right fingers depending on what's selected
        selectedFingers = handPoseCore.ExtractFingersFromSelection(
            leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray)

        with ContextManagers.SuspendSceneParallelism():
            pose.applyPose(
                selectedFingers, leftControlNames, rightControlNames, leftBoneIdArray, rightBoneIdArray)

    def assertPoseDataExists(self):
        """Asserts that the pose library json file exists

        Returns:
            str: Path to user pose library
        """
        handPose = handPoseUI.HandPoseUI()
        userPoseLibrary = handPose._exposePoseLibrary()
        if not os.path.exists(userPoseLibrary):
            self.fail(
                "{} path doesn't exist! The tool needs a pose library json file to set data.".format(userPoseLibrary)
            )

        return userPoseLibrary

    def assertPoses(self):
        """Asserts that the poses exists

        Returns:
            (list of PostControl.PoseData): Poses of each one found in user pose library
        """
        userPoseLibrary = self.assertPoseDataExists()
        poseFn = PoseControl.PoseManager()
        pose = poseFn.extractPoseLibraryFromJson(userPoseLibrary)

        # get pose model objects
        poses = [pose.child(row) for row in xrange(pose.rowCount())]
        self.assertIsNot(poses, [])

        return poses

    def assertExists(self, name):
        """Asserts that a model exists

        Args:
            name (str): Name of model to assert

        Returns:
            mobu.FBModel: The model found by label name
        """
        model = mobu.FBFindModelByLabelName(name)
        self.assertIsNotNone(model)
        return model
