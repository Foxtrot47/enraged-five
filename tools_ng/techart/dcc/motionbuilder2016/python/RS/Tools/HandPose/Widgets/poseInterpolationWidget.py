from functools import partial

import pyfbsdk as mobu

from PySide import QtGui, QtCore

from RS.Tools.HandPose import Core as handPoseCore
from RS.Tools.HandPose.Models import PoseControl
from RS.Utils import ContextManagers
from RS import Globals
from RS.Tools.HandPose import const


class PoseInterpolationWidget(QtGui.QWidget): 
    """ Widget for displaying custom sets of anim files """
    selectionChangedSignal = QtCore.Signal(object)
    lastItemSelected = QtCore.Signal(object)

    def __init__(self, parent=None):
        """
        Args:
            parent (QtGui.QWidget): parent widget
        """
        super(PoseInterpolationWidget, self).__init__(parent=parent)

        # Create Widgets
        self.fingerWeight = QtGui.QDoubleSpinBox()
        self.handWeight = QtGui.QDoubleSpinBox()
        self.fingerSpin = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.handSpin = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.handBlendSpin = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.handBlendWeight = QtGui.QDoubleSpinBox()
        self.boneInterpolationOption1 = QtGui.QRadioButton("")
        self.boneInterpolationOption2 = QtGui.QRadioButton("")
        self.handInterpolationOption1 = QtGui.QRadioButton("")
        self.handInterpolationOption2 = QtGui.QRadioButton("")

        # Declare empty variables
        self.startHandPose = None
        self.__currentSelection = tuple()

        # Setup rest of the UI
        self.setupUi()

    def setupUi(self):
        # Setup UI from external functions
        blendControlEnd = self._buildBlendControls(
            " Hand Interpolation Order ",
            "Lead From",
            "Thumb",
            "Pinky",
            self.handInterpolationOption1,
            self.handInterpolationOption2,
        )
        blendControlStart = self._buildBlendControls(
            " Bone Interpolation Order ",
            "Lead From",
            "Base",
            "Tip",
            self.boneInterpolationOption1,
            self.boneInterpolationOption2,
        )
        startRangeControl = self._buildStartRangeControls()

        # Create Layout
        blendLayout = QtGui.QVBoxLayout()

        # Create Widget
        self.computeHandInterpolation = QtGui.QCheckBox("Compute Interpolation")

        # Set Layout
        self.setLayout(blendLayout)

        # Add Widgets to Layout
        blendLayout.addWidget(self.computeHandInterpolation)
        blendLayout.addWidget(self._createSeparator())
        blendLayout.addWidget(startRangeControl)
        blendLayout.addWidget(blendControlEnd)
        blendLayout.addWidget(blendControlStart)
        blendLayout.addWidget(self._buildHandInterpolationControls())

        # Configure Layout
        blendLayout.setSpacing(5)
        blendLayout.setContentsMargins(0, 0, 0, 0)

        # Configure Widgets
        self.setMinimumWidth(215)
        self.rangeSpinBox.setRange(-50000, 50000)
        self._manageInterpolationDisabling()

        # Hook up connection
        self.computeHandInterpolation.stateChanged.connect(self._manageInterpolationDisabling)

    @staticmethod
    def _createSeparator():
        """
        Creates a line separator

        Returns:
            QFrame
        """
        # Create Widget
        targetLine = QtGui.QFrame()

        # Configure Widget
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _manageInterpolationDisabling(self):
        """ Sets the current state of all widget components """
        activeState = self.computeHandInterpolation.isChecked()
        for uiWidget in [
            self.boneInterpolationOption1,
            self.boneInterpolationOption2,
            self.handInterpolationOption1,
            self.handInterpolationOption2,
            self.fingerWeight,
            self.handWeight,
            self.fingerSpin,
            self.handSpin,
            self.rangeSpinBox,
            self.startFrameButton,
        ]:
            uiWidget.setDisabled(not activeState) 

    def _buildHandInterpolationControls(self):
        """
        Creates the Hand Interpolation slider component

        Returns:
            QtGui.QGroupBox
        """
        # Setup UI from external functions
        previewControls, sliderControl = self._buildPosePreview(self.handBlendWeight, self.handBlendSpin, False)
        self.handBlendWeight = sliderControl

        # Create Layouts
        blendLayoutAnchor = QtGui.QVBoxLayout()
        blendTabAnchorLayout = QtGui.QHBoxLayout()

        # Create Widgets
        blendTab = QtGui.QGroupBox("Hand Interpolation")
        blendTabAnchor = QtGui.QWidget()

        # Assign Widgets to Layout
        blendLayoutAnchor.addWidget(previewControls)
        blendLayoutAnchor.addWidget(blendTabAnchor)

        # Set Layouts
        blendTab.setLayout(blendLayoutAnchor)
        blendTabAnchor.setLayout(blendTabAnchorLayout)

        # Configure Layouts
        blendTabAnchorLayout.setContentsMargins(5, 0, 5, 5)

        # Configure Widgets
        blendLayoutAnchor.setSpacing(0)
        blendLayoutAnchor.setContentsMargins(0, 0, 0, 0)

        return blendTab 

    def _buildBlendControls(
        self,
        blendGroupLabel,
        blendCategoryLabel,
        blendTypeLabel1,
        blendTypeLabel2,
        blendOriginOption1,
        blendOriginOption2,
    ):
        """
        Creates a group box with 2 radio buttons used for selecting interpolation options

        Args:
            blendGroupLabel (str): Group box title
            blendCategoryLabel (str): Text descriptor
            blendTypeLabel1 (str): Label for interpolation type
            blendTypeLabel2 (str): Label for interpolation type
            blendOriginOption1  (QtGui.QRadioButton): Option 1
            blendOriginOption2  (QtGui.QRadioButton): Option 2

        Returns:
            QtGui.QGroupBox
        """
        # Create Layouts
        blendLayout = QtGui.QGridLayout()
        blendLayoutAnchor = QtGui.QVBoxLayout()

        # Create Widgets
        blendTab = QtGui.QGroupBox(blendGroupLabel)
        blendTabAnchor = QtGui.QWidget()
        blendLabel = QtGui.QLabel(blendCategoryLabel)

        # Add Widgets to Layouts
        blendLayout.addWidget(blendLabel, 0, 0, QtCore.Qt.AlignCenter)
        blendLayout.addWidget(blendOriginOption1, 0, 1)
        blendLayout.addWidget(blendOriginOption2, 1, 1)
        blendLayoutAnchor.addWidget(blendTabAnchor)

        # Set Layouts
        blendTab.setLayout(blendLayoutAnchor)
        blendTabAnchor.setLayout(blendLayout)

        # Configure Layouts
        blendLayout.setSpacing(5)
        blendLayout.setContentsMargins(5, 5, 5, 5)
        blendLayoutAnchor.setSpacing(0)
        blendLayoutAnchor.setContentsMargins(0, 0, 0, 0)

        # Configure Widgets
        blendOriginOption1.setText(blendTypeLabel1)
        blendOriginOption2.setText(blendTypeLabel2)
        blendOriginOption1.setChecked(True) 

        return blendTab

    def _buildStartRangeControls(self):
        """
        Creates a group box with spinbox for inputting interpolation start frame

        Returns:
             QtGui.QGroupBox
        """
        # Create Layout
        startRangeLayoutAnchor = QtGui.QHBoxLayout()

        # Create Widgets 
        startRangeAnchor = QtGui.QGroupBox("Interpolation Start Frame")
        self.rangeSpinBox = QtGui.QSpinBox()
        self.startFrameButton = QtGui.QPushButton(">")

        # Add Widgets to Layout
        startRangeLayoutAnchor.addWidget(self.rangeSpinBox)
        startRangeLayoutAnchor.addWidget(self.startFrameButton)

        # Set Layouts
        startRangeAnchor.setLayout(startRangeLayoutAnchor)

        # Configure Layout
        startRangeLayoutAnchor.setSpacing(5)
        startRangeLayoutAnchor.setContentsMargins(5, 5, 5, 5)
        startRangeLayoutAnchor.addStretch()

        # Configure Widgets
        self.startFrameButton.setMaximumWidth(18)
        self.startFrameButton.setMaximumHeight(18)
        self.rangeSpinBox.setMinimumWidth(75)
        self.rangeSpinBox.setMaximumWidth(75)
        self.rangeSpinBox.setMaximumWidth(75)
        self.rangeSpinBox.setMinimum = -50000
        self.rangeSpinBox.setMaximum = 500000
        self.rangeSpinBox.setRange(-50000, 50000)

        # Hook up connection
        self.startFrameButton.pressed.connect(self._getCurrentTime)

        return startRangeAnchor

    def _buildPosePreview(
        self, weightValueSpinBox, slider, connectSlider=True, clampWeightValue=60.0, weightDefaultValue=25,
    ):
        """
        Args:
            weightValueSpinBox (QtGui.QDoubleSpinBox): Interpolation value
            slider (QtGui.QSlider): Slider to update the weightValueSpinBox

        Kwargs:
            connectSlider (bool): Show "Weight" text label or not
            clampWeightValue (float): Set the max weight value for interpolation
            weightDefaultValue (float): Set default value for interpolation

        Returns:
            QtGui.QWidget, QtGui.QDoubleSpinBox
        """
        # Create Layouts
        previewLayout = QtGui.QHBoxLayout()

        # Create Widgets
        previewWidget = QtGui.QWidget()
        if connectSlider is True:
            weightLabel = QtGui.QLabel("Weight")

        # Add Widgets to Layout
        previewLayout.addWidget(slider)
        previewLayout.addWidget(weightValueSpinBox)
        if connectSlider is True:
            previewLayout.addWidget(weightLabel)

        # Set Layout
        previewWidget.setLayout(previewLayout)

        # Configure Layout
        previewLayout.setSpacing(5)
        previewLayout.setContentsMargins(5, 5, 5, 5)

        # Configure Widgets
        weightValueSpinBox.setRange(0.0, clampWeightValue)
        weightValueSpinBox.setValue(weightDefaultValue)
        weightValueSpinBox.setSingleStep(0.1)
        weightValueSpinBox.setDecimals(2)
        slider.setRange(0, 10000)
        slider.setValue(0)
        slider.setFocusPolicy(QtCore.Qt.StrongFocus)
        slider.setSingleStep(1)
        slider.setRange(0.0, int(clampWeightValue * 100))
        slider.setValue(int(weightDefaultValue * 100))
        if connectSlider is True:
            weightLabel.setMinimumWidth(52)

        # Hook up connections
        slider.valueChanged.connect(partial(self._syncSpinner, weightValueSpinBox, connectSlider))
        weightValueSpinBox.valueChanged.connect(partial(self._syncSlider, slider))

        return previewWidget, weightValueSpinBox

    def _syncSpinner(self, spinner, controlTakeLayer, value):
        """
        Sets the value of the spinbox based on the slider value

        Args:
            spinner (QtGui.QDoubleSpinBox): The spinbox that gets updated by the slider
            controlTakeLayer (bool): True or False
            value (int): Value of the slider
        """
        targetValue = float(value)*0.01
        spinner.setValue(targetValue)

        if controlTakeLayer and self.interpolationLayer:
            self.interpolationLayer.Weight = targetValue

    def _syncSlider(self, slider, value):
        """
        Sets the value of the slider based on the spinbox value

        Args:
            slider (QtGui.QSlider): The slider that gets updated by the spinbox
            value (int): Value of the spinbox
        """
        slider.setValue(int(value*100))

    def exposeInterpolationSettings(self):
        """
        Get the settings from the UI used for interpolation

        Returns:
             handPoseCore.HandInterpolationSettings
        """
        if self.handInterpolationOption1.isChecked():
            handInterpolation = "thumb"
        else: 
            handInterpolation = "pinky"

        if self.boneInterpolationOption1.isChecked():
            boneInterpolation = "baseBone"
        else: 
            boneInterpolation = "tipBone"

        handSettings = handPoseCore.HandInterpolationSettings()
        handSettings.handInterpolation = handInterpolation
        handSettings.boneInterpolation = boneInterpolation

        handSettings.boneInterpolationWeight = 1.0
        handSettings.handInterpolationWeight = 1.0

        handSettings.inputTake = mobu.FBSystem().CurrentTake

        handSettings.startTime = mobu.FBTime()
        handSettings.startTime.SetFrame(self.rangeSpinBox.value(), handSettings.frameRate) 

        handSettings.endTime = mobu.FBSystem().LocalTime

        return handSettings

    def _getCurrentTime(self):
        """ Sets the interpolation start frame to the current time and sets the start pose to the current pose """
        currentTime = mobu.FBSystem().LocalTime.GetFrame()
        self.rangeSpinBox.setValue(currentTime)

        # Create lists of finger effectors as FBModelMarker objects
        # Example: [<pyfbsdk.FBModelMarker object at 0x000000006DD9DF98>,
        #           <pyfbsdk.FBModelMarker object at 0x000000006DD9DBA8>,
        #           <pyfbsdk.FBModelMarker object at 0x000000006DD9D438>]
        leftFingerEffectorArray = handPoseCore.getIkParts(
            Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=False,
        )
        rightFingerEffectorArray = handPoseCore.getIkParts(
            Globals.Application.CurrentCharacter, const.Sides.RIGHT, asString=False,
        )

        # Create lists of all finger bones as strings of their names ordered as Thumb, Index, Middle, Ring, Pinky
        # Example: ["LeftHandThumb1", "LeftHandThumb2", "LeftHandThumb3",
        #           "LeftInHandIndex", "LeftHandIndex1", "LeftHandIndex2", "LeftHandIndex3",
        #           "LeftInHandMiddle", "LeftHandMiddle1", "LeftHandMiddle2", "LeftHandMiddle3",
        #           "LeftInHandRing", "LeftHandRing1", "LeftHandRing2", "LeftHandRing3",
        #           "LeftInHandPinky", "LeftHandPinky1", "LeftHandPinky2", "LeftHandPinky3"]
        leftControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=True,
        )
        rightControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.RIGHT, asString=True,
        )

        # Create lists of all finger bones as strings of their Node Ids ordered as Thumb, Index, Middle, Ring, Pinky
        # Example: ["kFBLeftThumbANodeId", "kFBLeftThumbBNodeId", "kFBLeftThumbCNodeId",
        #           "kFBLeftIndexInNodeId", "kFBLeftIndexANodeId", "kFBLeftIndexBNodeId", "kFBLeftIndexCNodeId",
        #           "kFBLeftMiddleInNodeId", "kFBLeftMiddleANodeId", "kFBLeftMiddleBNodeId", "kFBLeftMiddleCNodeId",
        #           "kFBLeftRingInNodeId", "kFBLeftRingANodeId", "kFBLeftRingBNodeId","kFBLeftRingCNodeId",
        #           "kFBLeftPinkyInNodeId", "kFBLeftPinkyANodeId", "kFBLeftPinkyBNodeId", "kFBLeftPinkyCNodeId"]
        leftBoneIdArray = handPoseCore.getFkIdNodes(leftControlNames, side=const.Sides.LEFT)
        rightBoneIdArray = handPoseCore.getFkIdNodes(rightControlNames, side=const.Sides.RIGHT)

        self.startHandPose = self.getCurrentHandPose(
            leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray, keyAtCurrentTime=True,
        )

    def getCurrentHandPose(
        self,
        leftBoneIdArray,
        rightBoneIdArray,
        leftFingerEffectorArray,
        rightFingerEffectorArray,
        keyAtCurrentTime=False,
    ):
        """
        Returns the current hand pose data

        Args:
            leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
            rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)
            leftFingerEffectorArray (list): List of left side IK finger effectors as strings
            rightFingerEffectorArray (list): List of right side IK finger effectors as strings

        Kwargs:
            keyAtCurrentTime (bool): True or False, set a keyframe at the current time

        Returns:
            handPoseCore.Pose
        """
        # Gather hand Animation variable column row
        currentSelection = mobu.FBModelList()

        # Get the selected models.
        mobu.FBGetSelectedModels(currentSelection)
        if len(currentSelection) == 0:
            errorStream = "Nothing is selected:\nPlease select a finger ik control"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return None

        selectedFingers = handPoseCore.ExtractFingersFromSelection(
            leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray,
        )
        if selectedFingers is None:
            return None

        fingers = selectedFingers[selectedFingers.keys()[0]]
        inputCharacter = fingers.character

        poseData = PoseControl.PoseData()
        poseData.collectPoseControls(inputCharacter, leftBoneIdArray, rightBoneIdArray)

        currentPose = poseData.extractPoseFromSelection(selectedFingers)
        outputPose = handPoseCore.Pose()
        outputPose.buildFromInputPose(currentPose)

        if keyAtCurrentTime is True:
            self.keyHandOnCurrentTime(
                inputCharacter, selectedFingers,  outputPose, leftBoneIdArray, rightBoneIdArray,
            )

        return outputPose

    def keyHandOnCurrentTime(
        self,
        inputCharacter,
        selectedFingers,
        outputPose,
        leftBoneIdArray,
        rightBoneIdArray,
    ):
        """
        Args:
            inputCharacter (FBCharacter): Character model object
            selectedFingers (dict): Gotten from ExtractFingersFromSelection()
            outputPose (handPoseCore.Pose): Pose to apply
            leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
            rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)
        """
        HandUtils = handPoseCore.HandNodes()
        handNodes = HandUtils.extractComponents(
            inputCharacter, leftBoneIdArray, rightBoneIdArray, extractFkControllers=True,
        )
        selectedFingersNodes = []
        for fingerKey in selectedFingers.keys():
            fingers = selectedFingers[fingerKey]

            selectedFingersNodes.extend(fingers.fingerArray)

        with ContextManagers.Undo(undoStackName="HandPoseDoubleClick", modelList=selectedFingersNodes):
            for poseIndex, fingerKey in enumerate(selectedFingers.keys()):
                fingers = selectedFingers[fingerKey]
                fingerControllers = handPoseCore.GetValidFingerController(outputPose, handNodes, fingers)

                for fingerController in fingerControllers:
                    rotationProperty = fingerController.handNode.PropertyList.Find("Rotation (Lcl)")
                    if not rotationProperty.IsAnimated():
                        rotationProperty.SetAnimated(True)

                    rotationAnimNode = rotationProperty.GetAnimationNode()
                    rotationAnimNode.KeyCandidate()

            mobu.FBSystem().Scene.Evaluate()

    def _computeHandInterpolation(
        self, leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray,
    ):
        """
        Main function that runs everything necessary for calculating the hand interpolation

        Args:
            leftBoneIdArray (list): List of FBBodyNodeId objects in the left hand (as strings)
            rightBoneIdArray (list): List of FBBodyNodeId objects in the right hand (as strings)
            leftFingerEffectorArray (list): List of left side IK finger effectors as strings
            rightFingerEffectorArray (list): List of right side IK finger effectors as strings
        """
        if self.startHandPose is None:
            errorStream = "Start Pose is missing:\nPlease set your interpolation Start Frame"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return

        endPoseData = self.getCurrentHandPose(
            leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray,
        )
        if endPoseData is None:
            return

        handSettings = self.exposeInterpolationSettings()
        handBlendingValue = self.handBlendWeight.value()*0.01

        handPoseCore.ComputeHandAnimation(
            handBlendingValue,
            handSettings,
            self.startHandPose,
            endPoseData,
            leftBoneIdArray,
            rightBoneIdArray,
            leftFingerEffectorArray,
            rightFingerEffectorArray,
        )
