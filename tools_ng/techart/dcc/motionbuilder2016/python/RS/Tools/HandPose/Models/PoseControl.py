import base64
import cPickle
import json
from collections import OrderedDict
import os
import pprint

import pyfbsdk as mobu

from PySide import QtGui, QtCore

from RS import Config
from RS import Globals
from RS.Core.Animation import Lib
from RS.Tools.HandPose import Core as handPoseCore
from RS.Tools.HandPose import const
from RS.Utils import ContextManagers


class PoseControlData(object):

    def __init__(self):
        self.nodeObjectName = ""
        self.IsNodeLocalMatrix = True
        self.nodeMatrix = None
        self.nodeIndex = -1
        self.localRotation = None

    @staticmethod
    def convertListToFBMatrix(inputList):
        """
        Converts a list of matrix related transformation data into a pyfbsdk.FBMatrix

        Returns:
            pyfbsdk.FBMatrix
        """
        outputMatrix = mobu.FBMatrix()
        for index in range(len(inputList)):
            outputMatrix[index] = inputList[index]

        return outputMatrix

    def __repr__(self):
        """
        Creates a string that includes all pose data information

        Returns:
            str: pose data information
        """
        return "\n<{0}>\n\t{1}\n\t{2}:{3}\n\t{4}:{5}\n".format(
            self.__class__.__name__,
            self.nodeObjectName,
            "nodeIndex",
            self.nodeIndex,
            "localRotation",
            self.localRotation,
        )


class PoseData(object):
    """ Container for storing, saving, and gathering all pose data """

    def __init__(self):
        self.isLibraryRoot = False
        self.isFolder = False
        self.Name = ""
        self.thumbnail = ""
        self.folderPath = ""
        self.FBPoseControls = []
        self._children = []
        self._parent = None
        self._nameIsEditable = False
        self._isCollapsed = True

        self.rootItem = None
        self.computeIterpolation = False
        self.handPoseToolWindow = None

    def bundleDataToJson(self):
        """
        Serializes and encodes all pose data and bundles everything into a json dict for saving to a pose file

        Returns:
            dict: json dictionary containing all pose data
        """
        # Create the keys used by the tool for UI organization
        exportKeys = [
            "isLibraryRoot",
            "isFolder",
            "Name",
            "folderPath",
            "_nameIsEditable",
            "thumbnail",
            "_isCollapsed",
        ]
        exportData = {}
        for exportKey in exportKeys:
            exportData[exportKey] = self.__dict__[exportKey]

        # Iterate through all stored poses and add to list as serialized data
        poseControls = []
        for control in self.FBPoseControls:
            poseControls.append(cPickle.dumps(control))
        # Encode the serialized data and add to exportData dict as a new key
        poseStream = "@".join(poseControls)
        exportData["FBPoseControls"] = base64.b64encode(poseStream)

        # json creation
        jsonData = json.dumps(exportData, indent=4)

        return jsonData

    def loadDataFromJson(self, jsonData):
        """
        Decodes and "unpickles" all pose data and adds to the FBPoseControls list for the tool to use

        Args:
            jsonData (dict): json dictionary containing all pose data
        """
        # Create the keys used by the tool for UI organization
        importKeys = [
            "isLibraryRoot",
            "isFolder",
            "Name",
            "folderPath",
            "_nameIsEditable",
            "thumbnail",
            "_isCollapsed",
        ]
        for importKey in importKeys:
            if importKey not in jsonData.keys():
                continue
            self.__dict__[importKey] = jsonData[importKey]

        # Decode the pose data
        FBPoseControlStream = base64.b64decode(jsonData["FBPoseControls"])
        FBPoseControlStreams = FBPoseControlStream.split("@")

        # Make sure there are poses to work with before the process can continue
        controlCount = len(FBPoseControlStreams)
        if controlCount == 0:
            return

        # "Unpickle" all of the pose data and add to the FBPoseControls list
        for nodeIndex in range(len(FBPoseControlStreams)):
            objectData = str(FBPoseControlStreams[nodeIndex])
            if len(objectData) == 0:
                continue
            poseData = cPickle.loads(objectData)
            self.FBPoseControls.append(poseData)

    def name(self):
        """ Display name """
        return self.Name 

    def setName(self, name):
        """
        Set the display name

        Args:
            name (string): name to display
        """
        self.Name = name

    def isNameEditable(self):
        """ Is the display name editable """
        return self._nameIsEditable

    def setNameIsEditable(self, isEditable):
        """
        Set the editable state of the display name

        Args:
            isEditable (boolean): editable state to set for the display name
        """
        self._nameIsEditable = bool(isEditable)

    def isFile(self):
        """ Is the data stored that of a file """
        fileState = True
        if self.isLibraryRoot is True or self.isFolder is True:
            fileState = False

        return fileState

    def addChild(self, child):
        """ Add child to list """
        self._children.append(child)

    def appendChild(self, child):
        """
        Adds a new child to the PoseData

        Args:
            child (PoseData): child PoseData
        """
        self._children.append(child)
        child._parent = self

    def applyPoseToSelection(self):
        """ Applies pose to the selected hand(s) """
        # Make sure there is a character to work with and that the pose contains data
        sceneCharacters = mobu.FBSystem().Scene.Characters
        if len(sceneCharacters) == 0:
            errorStream = "No character in your scene file"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return
        if len(self.FBPoseControls) == 0:
            errorStream = "Current pose is empty"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return
        # Create lists of finger effectors as FBModelMarker objects
        leftFingerEffectorArray = handPoseCore.getIkParts(
            Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=False,
        )
        rightFingerEffectorArray = handPoseCore.getIkParts(
            Globals.Application.CurrentCharacter, const.Sides.RIGHT, asString=False,
        )

        # Create lists of all finger bones as strings of their names ordered as Thumb, Index, Middle, Ring, Pinky
        # Example: ["LeftHandThumb1", "LeftHandThumb2", "LeftHandThumb3",
        #           "LeftInHandIndex", "LeftHandIndex1", "LeftHandIndex2", "LeftHandIndex3",
        #           "LeftInHandMiddle", "LeftHandMiddle1", "LeftHandMiddle2", "LeftHandMiddle3",
        #           "LeftInHandRing", "LeftHandRing1", "LeftHandRing2", "LeftHandRing3",
        #           "LeftInHandPinky", "LeftHandPinky1", "LeftHandPinky2", "LeftHandPinky3"]
        leftControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=True,
        )
        rightControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.RIGHT, asString=True,
        )

        # Create lists of all finger bones as strings of their Node Ids ordered as Thumb, Index, Middle, Ring, Pinky
        # Example: ["kFBLeftThumbANodeId", "kFBLeftThumbBNodeId", "kFBLeftThumbCNodeId",
        #           "kFBLeftIndexInNodeId", "kFBLeftIndexANodeId", "kFBLeftIndexBNodeId", "kFBLeftIndexCNodeId",
        #           "kFBLeftMiddleInNodeId", "kFBLeftMiddleANodeId", "kFBLeftMiddleBNodeId", "kFBLeftMiddleCNodeId",
        #           "kFBLeftRingInNodeId", "kFBLeftRingANodeId", "kFBLeftRingBNodeId", "kFBLeftRingCNodeId",
        #           "kFBLeftPinkyInNodeId", "kFBLeftPinkyANodeId", "kFBLeftPinkyBNodeId", "kFBLeftPinkyCNodeId"]
        leftBoneIdArray = handPoseCore.getFkIdNodes(leftControlNames, side=const.Sides.LEFT,)
        rightBoneIdArray = handPoseCore.getFkIdNodes(rightControlNames, side=const.Sides.RIGHT,)

        # Grab all left/right fingers depending on what's selected
        selectedFingers = handPoseCore.ExtractFingersFromSelection(
            leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray,
        )
        # Make sure that a finger is selected
        if selectedFingers is None:
            errorStream = "No finger components are selected"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return
        # Get the hand pose tool window and error if it isn't found
        handTool = None
        if handTool is None:
            for widgetIndex in xrange(10):
                self.handPoseToolWindow = self.handPoseToolWindow.parentWidget()
                if "HandPoseUI" in str(type(self.handPoseToolWindow)):
                    handTool = self.handPoseToolWindow
                    break
        if handTool is None:
            errorStream = "Could not find hand pose tool window"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return
        # Grab data from the main widget UI
        poseLibraryWidget = handTool.poseLibraryControlWidget
        interpolationWidget = poseLibraryWidget.lerpWidget
        computeInterpolationValue = interpolationWidget.computeHandInterpolation.isChecked()

        # Apply pose
        # If user is applying pose on the main "Pose Tools" tab with Interpolation "on" compute the interpolation,
        # Otherwise just apply the pose
        if handTool._tabWidget.currentIndex() == 0 and computeInterpolationValue is True:
            with ContextManagers.SuspendSceneParallelism():
                self.applyPose(
                    selectedFingers,
                    leftControlNames,
                    rightControlNames,
                    leftBoneIdArray,
                    rightBoneIdArray,
                    createKeys=True,
                )
                interpolationWidget._computeHandInterpolation(
                    leftBoneIdArray, rightBoneIdArray, leftFingerEffectorArray, rightFingerEffectorArray,
                )
        else:
            with ContextManagers.SuspendSceneParallelism():
                self.applyPose(
                    selectedFingers, leftControlNames, rightControlNames, leftBoneIdArray, rightBoneIdArray,
                )

    def collectPoseControls(self, inputCharacter, leftBoneIdArray, rightBoneIdArray):
        """
        Gathers all finger rotation data

        Args:
            inputCharacter (pyfbsdk.FBCharacter): Character to get data from
            leftBoneIdArray (list): All left hand FK bone IDs
            rightBoneIdArray (list): All right hand FK bone IDs
        """
        # Store all FK finger components into a HandNodes container
        handHandle = handPoseCore.HandNodes()
        handNodes = handHandle.extractComponents(
            inputCharacter, leftBoneIdArray, rightBoneIdArray, extractFkControllers=True,
        )

        # Iterate through all fingers and store the joint rotation data in FBPoseControls list
        for fingerJointIndex, fingerJoint in enumerate(handNodes):
            if fingerJoint:
                fingerJointValue = handPoseCore.BonePose()
                fingerJointValue.localRotation = list(fingerJoint.PropertyList.Find("Rotation (Lcl)").Data)
                fingerJointValue.nodeObjectName = fingerJoint.Name
                fingerJointValue.nodeIndex = fingerJointIndex
                self.FBPoseControls.append(fingerJointValue)

    def extractPoseFromSelection(self, selectedFingers):
        """
        Args:
            selectedFingers (dict): Based from Core.SelectedCharacterFingers class data
                                    gathered using Core.ExtractFingersFromSelection function

        Return:
            fingersPoseData (dict): Stored pose data for all fingers
        """
        # Make sure we have finger data
        if selectedFingers is None:
            errorStream = "SelectedFingers structure is None"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return
        if len(selectedFingers.keys()) == 0:
            errorStream = "SelectedFingers structure is empty"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return

        # Get all finger names (list of str) and create a list to store pose data in
        # Need to force a dynamic lookup just in case the character joint names have been altered
        # For example if a rig is improperly brought into a scene the root index finger may be LeftHandIndex5 (not 1)

        # Use IDs to get actual control names
        leftControlNames = []
        leftControlNameIds = handPoseCore.getFkIdNodes(Globals.Application.CurrentCharacter, const.Sides.LEFT)
        rightControlNames = []
        rightControlNameIds = handPoseCore.getFkIdNodes(Globals.Application.CurrentCharacter, const.Sides.RIGHT)

        for name in leftControlNameIds:
            leftControlNames.append(
                Globals.Application.CurrentCharacter.GetCtrlRigModel(getattr(mobu.FBBodyNodeId, str(name), None)).Name
            )
        for name in rightControlNameIds:
            rightControlNames.append(
                Globals.Application.CurrentCharacter.GetCtrlRigModel(getattr(mobu.FBBodyNodeId, str(name), None)).Name
            )

        fingerNames = list(leftControlNames) + list(rightControlNames) + list(const.Parts.PH_COMPONENTS)
        fingersPoseData = range(len(self.FBPoseControls))

        # Iterate through all controls and store the data in fingersPoseData
        for control in self.FBPoseControls:
            if control:
                fingerIndex = fingerNames.index(control.nodeObjectName)
                fingersPoseData[fingerIndex] = control

        return fingersPoseData

    def applyPose(
        self, selectedFingers, leftControlNames, rightControlNames, leftBoneIdArray, rightBoneIdArray, createKeys=False,
    ):
        """
        Args:
            selectedFingers (dict): Based from Core.SelectedCharacterFingers class data
                                    gathered using Core.ExtractFingersFromSelection function
            leftControlNames (list): All left hand bone names (str)
            rightControlNames (list): All right hand bone names (str)
            leftBoneIdArray (list): All left hand bone IDs
            rightBoneIdArray (list): All right hand bone IDs
            createKeys (bool): True or False create keyframes
        """
        # Make sure we have finger data
        if selectedFingers is None:
            errorStream = "SelectedFingers structure is None"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return
        if len(selectedFingers.keys()) == 0:
            errorStream = "SelectedFingers structure is empty"
            QtGui.QMessageBox.information(None, "Missing Element", errorStream)
            return

        # Store all finger nodes in hand(s) that are selected
        selectedFingersNodes = []
        for fingerKey in selectedFingers.keys():
            # fingerKey represents the character as a whole with fingers
            fingers = selectedFingers[fingerKey]
            selectedFingersNodes.extend(fingers.fingerArray)

        # Get all finger names (list of str)
        fingerNames = list(leftControlNames) + list(rightControlNames) + list(const.Parts.PH_COMPONENTS)

        # Create a HandNodes container
        HandUtils = handPoseCore.HandNodes()

        # Wrap the actual apply pose work in a single undo
        with ContextManagers.Undo(undoStackName="HandPoseDoubleClick", modelList=selectedFingersNodes):
            for fingerKey in selectedFingers.keys():
                # fingerKey represents the character as a whole with fingers
                fingers = selectedFingers[fingerKey]
                inputCharacter = fingers.character

                # Get all bone ids
                handNodes = HandUtils.extractComponents(
                    inputCharacter, leftBoneIdArray, rightBoneIdArray, extractFkControllers=True,
                )

                # Iterate through all controls/fingers in the pose data
                for controlIndex, control in enumerate(self.FBPoseControls):
                    # Make sure the control from the pose data exists in the character
                    if control.nodeObjectName not in fingerNames:
                        continue

                    fingerIndex = fingerNames.index(control.nodeObjectName)
                    inputControl = handNodes[fingerIndex]

                    # Make sure that the input control exists
                    if inputControl is None:
                        continue
                    # Make sure the control from the character exists in the stored fingerArray
                    if inputControl not in fingers.fingerArray:
                        continue
                    # Make sure the control from the pose data has local rotation
                    if control.localRotation is None:
                        continue

                    # Get the local rotation data of the stored control and apply it to the control on the character
                    localRotation = mobu.FBVector3d(
                        control.localRotation[0], control.localRotation[1], control.localRotation[2],
                    )
                    rotationProperty = inputControl.PropertyList.Find("Rotation (Lcl)")
                    rotationProperty.Data = localRotation

                    # If position controls exist, apply that data
                    if "PH" in control.nodeObjectName and control.nodeObjectName.endswith("Hand"):
                        poseMatrix = control.convertListToFBMatrix(control.nodeMatrix)
                        localPosition = mobu.FBVector3d(poseMatrix[12], poseMatrix[13], poseMatrix[14])

                        positionProperty = inputControl.PropertyList.Find("Translation (Lcl)")
                        positionProperty.Data = localPosition

                        if createKeys is True:
                            if not positionProperty.IsAnimated():
                                positionProperty.SetAnimated(True)
                            positionAnimNode = positionProperty.GetAnimationNode()
                            positionAnimNode.KeyCandidate()

                    # Set keyframes on fingers if createKeys is True
                    if createKeys is True:
                        if not rotationProperty.IsAnimated():
                            rotationProperty.SetAnimated(True)
                        rotationAnimNode = rotationProperty.GetAnimationNode()
                        rotationAnimNode.KeyCandidate()

    def parent(self):
        """ Name of the parent """
        return self._parent

    def childCount(self):
        """ Number of children """
        return len(self._children)


class PoseDataUtils(PoseData):
    """ Inherits PoseData class (Container for storing, saving, and gathering all pose data) """

    def __init__(self):
        super(PoseDataUtils, self).__init__()

    def findRoot(self, poseRootData, rootTarget):
        """ Stores PoseDataItem object in index 0 and all parent folders leading back to the root folder in index 1 """
        if poseRootData._parent is not None:
            rootTarget[0] = poseRootData._parent
            rootTarget[1].append(poseRootData._parent.Name)
            self.findRoot(poseRootData._parent, rootTarget)

    def updateFolderPath(self):
        """ Sets folderPath variable to a list of all parent folders (can be used to find path of current folder) """
        rootTarget = [0, []]
        self.findRoot(self, rootTarget)
        rootTarget[1].reverse()
        self.folderPath = rootTarget[1]

    def extractHierarchy(self, poseAnchor, poseArray):
        """ Adds the current hierarchy of poses (as PoseDataItem objects) to an empty poseArray list """
        for pose in poseAnchor._children:
            poseArray.append([pose, '|'.join(pose.folderPath)])
            self.extractHierarchy(pose, poseArray)

    def layoutPoseStructure(self):
        """
        Bundles up all pose data that gets dumped to the json pose file that is saved out

        Returns:
            poseStructure (ordered dict): The dict used to hold all data for saving to the pose file
        """
        # Get the current hierarchy of the pose library
        poseArray = []
        self.extractHierarchy(self, poseArray)

        # Create an ordered dict and populate with pose data
        # poseIndex (int): Index of the object in the hierarchy
        # poseKey (str): The path of the pose (example - "Poses|Hand Poses|Gestures|Open" where "Open" is the pose
        #                                      and "Poses," "Hand Poses," and Gestures are the parent folders)
        # parentKey (str): The path of the poses parent folder (example - "Poses|Hand Poses|Gestures")
        # poseType (bool): True if a folder, False if a pose
        # poseOutput (dict): Dict returned from PoseData.bundleDataToJson
        poseStructure = OrderedDict()
        for poseIndex, poseData in enumerate(poseArray):
            poseKey = "{0}|{1}".format((poseData[1]), poseData[0].Name)
            parentKey = "|".join(poseData[0]._parent.folderPath)

            if len(poseData[0]._parent.folderPath) > 0:
                parentKey = "{0}|{1}".format(parentKey, poseData[0]._parent.Name)
            else:
                parentKey = poseData[0]._parent.Name

            poseType = poseData[0].isFolder
            poseOutput = poseData[0].bundleDataToJson()
            poseStructure[poseKey] = [
                poseIndex,
                poseKey,
                parentKey,
                poseType,
                poseOutput,
            ]

        return poseStructure

    def movePoseTo(self, targetParent, offsetIndex=0):
        """ Method called when a pose is moved to another location in the pose library """
        # Get the parent folder where the pose is being moved from
        sourceParent = self._parent

        # targetParent is where the pose is being moved/drag-and-dropped to
        if targetParent.isFolder is True or targetParent.isLibraryRoot is True:
            # Add the pose as a child of the targetParent and set the targetParent as the new parent of the pose
            targetParent._children.append(self)
            self._parent = targetParent
            self.updateFolderPath()
        else:
            # If the targetParent is a pose, then grab the parent folder and determine it's new index, then place it
            itemParent = targetParent._parent
            insertIndex = itemParent._children.index(targetParent) + offsetIndex

            itemParent._children.insert(insertIndex, self)
            self._parent = itemParent
            itemParent.updateFolderPath()

        if sourceParent is not None:
            sourceParent._children.remove(self)

    def removeFromTree(self):
        """ Remove pose data from the current library """
        self._parent._children.remove(self)


class PoseDataItem(PoseDataUtils):

    def __init__(self):
        super(PoseDataItem, self).__init__()

    def child(self, row):
        """
        Returns the child of the PoseData at the given row

        Args:
            row (int): row number

        Returns:
            PoseData or None
        """
        try:
            return self._children[row]
        except IndexError:
            return None

    def row(self):
        """ The row this object is located on"""
        if self._parent is not None:
            if self not in self._parent._children:
                return 0
            return self._parent._children.index(self)
        return 0

    def data(self, role=QtCore.Qt.DisplayRole):
        """
        Returns a name, QIcon, or pose data object based on role type

        Args:
            role (QtCore.Qt.DisplayRole)

        Returns:
            self.name (str): Name of object
            QtGui.QIcon (str): Path of icon
            self (PoseDataItem): PoseDataItem
        """
        if role in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):
            return self.name()
        elif role == QtCore.Qt.DecorationRole :
            isFile = self.isFile()
            filename = "folder.png"
            if isFile is True:
                filename = "sync.png"
            path = os.path.join(Config.Script.Path.ToolImages, "Perforce", filename)
            return QtGui.QIcon(path)
        elif role == QtCore.Qt.UserRole:
            return self

    def rowCount(self):
        """
        Number of rows this BaseModelItem has

        Returns:
            int
        """
        return self.childCount()


class PoseManager(object):
    """ Contains all the functions used to expose, extract, and save pose data """

    def __init__(self):
        self.printFormatter = pprint.PrettyPrinter(indent=4)
        self.handUtils = handPoseCore.HandNodes()
        self.currentCharacter = None
        self.handNodes = None
        self.poseOption = mobu.FBCharacterPoseOptions()
        self.poseOption.mCharacterPoseKeyingMode = mobu.FBCharacterPoseKeyingMode.kFBCharacterPoseKeyingModeFullBody 
        self.debug = 0

    def exposeFolderPaths(self, inputNode, folderPaths):
        """
        Used to grab all the poses and folders of the Pose Control MoBu Library when loading in the Hand Pose Tool UI

        Args:
            inputNode : Either a pyfbsdk.FBFolder or pyfbsdk.FBCharacterPose
            folderPaths (list): Empty list

        Returns:
            parentFolder (str): The parent pyfbsdk.FBFolder of the current pose/folder
        """
        folderLinkCount = inputNode.GetDstCount()
        if folderLinkCount == 0:
            return None

        parentFolder = None
        for linkIndex in range(folderLinkCount):
            if not isinstance(inputNode.GetDst(linkIndex), mobu.FBFolder):
                continue
            parentFolder = inputNode.GetDst(linkIndex)
            folderPaths.append(parentFolder.LongName)
            newFolder = self.exposeFolderPaths(parentFolder, folderPaths)
            if newFolder is None:
                return parentFolder
            parentFolder = newFolder
            break

        return parentFolder

    def extractPhData(self, phBone, nodeIndex, poseData):
        """
        Grabs PH object pose data and stores it in the poseData.FBPoseControls list

        Args:
            phBone (pyfbsdk.FBModelNull): The PH "Bone"
            nodeIndex (int): The index of the phBone object
            poseData (PoseDataItem): The pose data to add the PH bone pose data to
        """
        if phBone is None:
            return None

        # Call the PoseControlData container
        pose = PoseControlData()

        # Store the phBone name and index in the pose container
        pose.nodeObjectName = phBone.Name
        pose.nodeIndex = nodeIndex

        # Get and store the transform matrix and set IsNodeLocalMatrix to True
        nodeMatrix = mobu.FBMatrix()
        phBone.GetMatrix(nodeMatrix, mobu.FBModelTransformationType.kModelTransformation, False)
        pose.nodeMatrix = list(nodeMatrix)
        pose.IsNodeLocalMatrix = True

        # Get and store local rotation
        rotationProperty = phBone.PropertyList.Find("Rotation (Lcl)")
        localRotation = rotationProperty.Data 
        pose.localRotation = list(localRotation)

        # Add the pose to the poseData.FBPoseControls list
        poseData.FBPoseControls.append(pose)

    def exposePoseData(self, currentPose):
        """
        Creates a PoseDataItem and stores all hand pose data in it

        Args:
            currentPose (pyfbsdk.FBCharacterPose): Pose object

        Returns:
            poseData (PoseDataItem): Holds pose data for entire hand pose
        """
        # Create the poseData container and set the pose name
        poseData = PoseDataItem()
        poseData.Name = currentPose.LongName

        # Create an empty list for folderPaths, gather all folder path data, and store it in the poseData
        folderPaths = []
        self.exposeFolderPaths(currentPose, folderPaths)
        folderPaths.reverse()
        poseData.folderPath = folderPaths

        # Take the pose thumbnail from the MoBu scene, encode it, and store it
        poseData.thumbnail = self.uncompileThumbnailFromScene(currentPose)

        # If there is an active character, apply the pose
        if self.currentCharacter:
            currentPose.PastePose(self.currentCharacter, self.poseOption)
            mobu.FBSystem().Scene.Evaluate()
            currentPose.ApplyPoseCandidate() 

        # Get the left and right control names as strings
        leftControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=True,
        )
        rightControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.RIGHT, asString=True,
        )

        # Get the number of nodes total and iterate through each index
        poseNodeCount = currentPose.GetNodeCount()
        totalIndex = 0
        for nodeIndex in xrange(poseNodeCount):
            # Make sure there is a node object to work with and that the node is a hand control/finger
            nodeObject = currentPose.GetNodeObject(nodeIndex)
            if nodeObject:
                if nodeObject.LongName in leftControlNames or nodeObject.LongName in rightControlNames:
                    totalIndex = nodeIndex

                    # Create a pose container and set the pose name
                    pose = PoseControlData()
                    pose.nodeObjectName = nodeObject.LongName

                    # Get and store the transform matrix data and get IsNodeLocalMatrix
                    pose.nodeMatrix = list(currentPose.GetNodeMatrix(nodeIndex))
                    pose.nodeIndex = nodeIndex
                    pose.IsNodeLocalMatrix = currentPose.IsNodeLocalMatrix(nodeIndex)

                    # If there is an active character, find the current node/joint and store the localRotation
                    if self.currentCharacter:
                        jointIndex = self.handNodeNames.index(nodeObject.LongName)
                        if self.handNodes[jointIndex]:
                            rotationProperty = self.handNodes[jointIndex].PropertyList.Find("Rotation (Lcl)")
                            localRotation = rotationProperty.Data
                            pose.localRotation = list(localRotation)

                    # Add the pose to the poseData.FBPoseControls list
                    poseData.FBPoseControls.append(pose)

        # Add the PH data to poseData.FBPoseControls
        self.extractPhData(self.handUtils.PH_L_Hand, totalIndex+1, poseData)
        self.extractPhData(self.handUtils.PH_R_Hand, totalIndex+2, poseData)

        return poseData

    def getFolderContent(self, inputFolder, folderData, tabIndex=1):
        """
        Creates a PoseDataItem and stores all hand pose data in it

        Args:
            inputFolder (pyfbsdk.FBFolder): FBFolder object
            folderData (PoseDataItem): PoseDataItem
            tabIndex (int): Index of the tab in the main window of the tool
        """
        for folderItem in inputFolder.Items:
            # If the object is a folder, create a container, mark it True for "isFolder"
            # then store the name, path, and contents of the folder
            if isinstance(folderItem, mobu.FBFolder):
                subFolderData = PoseDataItem()
                subFolderData.isFolder = True
                subFolderData.Name = folderItem.LongName

                folderPaths = []
                self.exposeFolderPaths(folderItem, folderPaths)
                folderPaths.reverse()

                subFolderData.folderPath = folderPaths

                self.getFolderContent(folderItem, subFolderData, tabIndex=tabIndex+1)
                folderData.appendChild(subFolderData)
            # If the object is a character pose, grab the pose data and add it to the main pose data container
            elif isinstance(folderItem, mobu.FBCharacterPose):
                poseItemData = self.exposePoseData(folderItem)
                folderData.appendChild(poseItemData)

    def extractPoseLibraryFromScene(self):
        """
        Gets all pose data from the MoBu scene Pose Control

        Returns:
             poseRootData (PoseDataItem): Pose data
        """
        # Grab the poseRoot (always named "Poses")
        poseRoot = None
        for component in mobu.FBSystem().Scene.Folders:
            if component.Name == "Poses":
                poseRoot = component
                break

        # Create a container for the pose root, name it "Poses" and tag isLibraryRoot as True
        poseRootData = PoseDataItem()
        poseRootData.isLibraryRoot = True
        poseRootData.Name = "Poses"

        # Checks to see if there are any items that are children of the poseRoot
        folderCheck = 0
        for folderItem in poseRoot.Items:
            if isinstance(folderItem, mobu.FBFolder):
                folderCheck += 1
            if isinstance(folderItem, mobu.FBCharacterPose):
                folderCheck += 1
        # If no children found, an error pops up
        if folderCheck == 0:
            errorStream = "Unable to find Pose in the current scene"
            QtGui.QMessageBox.information(None, "Missing Character Pose", errorStream)

        # Check for an active character as well as if the character has a control rig with finger controls
        hasValidCharacterComponents = self.getCurrentCharacter()
        if folderCheck > 0 and hasValidCharacterComponents is False:
            return None

        # Get all pose content within the root folder
        self.getFolderContent(poseRoot, poseRootData, tabIndex=1)

        return poseRootData

    def savePoseLibrary(self, poseRootData, userPoseLibrary):
        """
        Saves pose data library to a json file

        Args:
            poseRootData (PoseDataItem): Container with all pose data
            userPoseLibrary (str): Path of json file library is being saved to
        """
        poseStructure = poseRootData.layoutPoseStructure()

        with open(userPoseLibrary, "w") as outputfile:
            json.dump(poseStructure, outputfile, sort_keys=True, indent=4)

    def uncompileThumbnailFromScene(self, inputPose):
        """
        Searches for a corresponding file for a thumbnail and returns it as encoded data

        Args:
            inputPose (pyfbsdk.FBCharacterPose): Current character pose

        Return:
            imageData (base64.b64encode): Encoded image data
        """
        # Get the image name (example: 196858 (65).tif)
        textureName = inputPose.PropertyList.Find("ImageName").Data

        # If there is no textureName or found filename, return an empty string
        if textureName is None:
            return ""
        if len(textureName) == 0:
            return ""
        currentFile = mobu.FBApplication().FBXFileName
        if os.path.isfile(currentFile) is False:
            return ""

        # Find the path(s) to the materials from the scene
        materialDirectories = []
        sceneLocation = os.path.dirname(currentFile)
        localFolders = os.listdir(sceneLocation)
        for folder in localFolders:
            if folder.endswith(".fbm"):
                materialDirectories.append(os.path.join(sceneLocation, folder))
        # If no path is found return an empty string
        if len(materialDirectories) == 0:
            return ""

        # Search for the applicable thumbnail file
        thumnbNailFile = None
        for materialFolder in materialDirectories:
            thumnbNailFile = os.path.join(materialFolder, textureName)
            if os.path.isfile(thumnbNailFile) is True:
                break            
            thumnbNailFile = None
        # If no thumbnail found, return an empty string
        if thumnbNailFile is None:
            return ""

        # Encode the image file data
        with open(thumnbNailFile, "rb") as imageFile:
            imageData = base64.b64encode(imageFile.read())

        return imageData

    def extractPoseLibraryFromJson(self, poseLibraryFile):
        """
        Transfers all pose data from a json pose library file into a PoseDataItem container

        Args:
            poseLibraryFile (str): Path of the json pose file

        Return:
            poseRootData (PoseDataItem): Container of pose data
        """
        if not os.path.exists(poseLibraryFile):
            return None

        # Create a container for the pose root, name it "Poses" and tag isLibraryRoot as True
        poseRootData = PoseDataItem()
        poseRootData.isLibraryRoot = True
        poseRootData.Name = "Poses"

        if os.path.isfile(poseLibraryFile) is False:
            return poseRootData

        # Get the pose data from the json pose file and sort it into a list
        with open(poseLibraryFile) as json_file:
            jsonData = json.load(json_file)
        sortedPoseList = range(len(jsonData.keys()))
        for jsonKey in jsonData.keys():
            sortedPoseList[jsonData[jsonKey][0]] = jsonData[jsonKey]

        # Store pose data inside key "Poses" of an ordered dict
        poseStructure = OrderedDict()
        poseStructure["Poses"] = poseRootData

        # Iterate through the pose list and store uncompiled/decoded pose data into a PoseDataItem container
        for pose in sortedPoseList:
            poseKey = pose[1]
            parentKey = pose[2]
            poseOutput = json.loads(pose[4])

            poseSample = PoseDataItem()
            poseSample.loadDataFromJson(poseOutput)
            poseStructure[poseKey] = poseSample
            parentFolder = poseStructure[parentKey]

            parentFolder.appendChild(poseSample)

        return poseRootData

    def getCurrentCharacter(self):
        """
        Find the current active character control rig and store all the finger nodes

        Returns:
             bool: True if a character exists in the scene, False if not
        """
        # Check if we have an existing FBCharacter in the current scene
        if len(mobu.FBSystem().Scene.Characters) == 0:
            errorStream = "The current scene has no valid character"
            QtGui.QMessageBox.information(None, "Missing Character", errorStream)
            return False

        # Check control rig matching the expected naming convention
        for character in Globals.Scene.Characters:
            ctrlRigHips = character.GetCtrlRigModel(mobu.FBBodyNodeId.kFBHipsNodeId)
            if ctrlRigHips is None:
                continue
            if ctrlRigHips.Name != "Hips":
                continue
            self.currentCharacter = character
            break

        # Error if active character not found
        if not self.currentCharacter:
            QtGui.QMessageBox.warning(
                None, "R* Error", "A control rig matching the expected naming convention was not found",
            )
            return False

        controlRigExists = Lib.HasControlRig(self.currentCharacter)
        # Error if control rig not found
        if controlRigExists is False:
            errorStream = "Please create a rig for the current character"
            QtGui.QMessageBox.information(None, "Missing Character Rig", errorStream)
            return False

        # Create lists of all finger bones as strings of their names ordered as Thumb, Index, Middle, Ring, Pinky
        # Example: ["LeftHandThumb1", "LeftHandThumb2", "LeftHandThumb3",
        #           "LeftInHandIndex", "LeftHandIndex1", "LeftHandIndex2", "LeftHandIndex3",
        #           "LeftInHandMiddle", "LeftHandMiddle1", "LeftHandMiddle2", "LeftHandMiddle3",
        #           "LeftInHandRing", "LeftHandRing1", "LeftHandRing2", "LeftHandRing3",
        #           "LeftInHandPinky", "LeftHandPinky1", "LeftHandPinky2", "LeftHandPinky3"]
        leftControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.LEFT, asString=True,
        )
        rightControlNames = handPoseCore.getFkParts(
            Globals.Application.CurrentCharacter, const.Sides.RIGHT, asString=True,
        )

        # Create lists of all finger bones as strings of their Node Ids ordered as Thumb, Index, Middle, Ring, Pinky
        # Example: ["kFBLeftThumbANodeId", "kFBLeftThumbBNodeId", "kFBLeftThumbCNodeId",
        #           "kFBLeftIndexInNodeId", "kFBLeftIndexANodeId", "kFBLeftIndexBNodeId", "kFBLeftIndexCNodeId",
        #           "kFBLeftMiddleInNodeId", "kFBLeftMiddleANodeId", "kFBLeftMiddleBNodeId", "kFBLeftMiddleCNodeId",
        #           "kFBLeftRingInNodeId", "kFBLeftRingANodeId", "kFBLeftRingBNodeId", "kFBLeftRingCNodeId",
        #           "kFBLeftPinkyInNodeId", "kFBLeftPinkyANodeId", "kFBLeftPinkyBNodeId", "kFBLeftPinkyCNodeId"]
        leftBoneIdArray = handPoseCore.getFkIdNodes(leftControlNames, side=const.Sides.LEFT)
        rightBoneIdArray = handPoseCore.getFkIdNodes(rightControlNames, side=const.Sides.RIGHT)

        # Get all finger nodes and names in the hand
        self.handNodes = self.handUtils.extractComponents(
            self.currentCharacter, leftBoneIdArray, rightBoneIdArray, extractFkControllers=True,
        )
        self.handNodeNames = [finger.Name for finger in self.handNodes if finger]

        return True
