"""
Model for displaying a hierarchy of anim files
"""
import cPickle

from PySide import QtCore


class PoseMimeData(QtCore.QMimeData):
    """ Mime Data for sharing model indexes """
    def __init__(self):
        super(PoseMimeData, self).__init__()
        self._ModelIndices = []
        self.category = ""

    def indices(self):
        """ The stored QModelIndexes """
        return self._ModelIndices

    def setIndices(self, indices):
        """
        Stores the indices within the class

        Args:
            indices (list(PySide.QtGui.QModelIndex)): QModelIndexes to store
        """
        self._ModelIndices = indices


class PoseDataModel(QtCore.QAbstractItemModel):
    """
    A drag and drop enabled, editable, hierarchical item model
    Source - AnimDataModel / https://www.youtube.com/watch?v=VcN94yMOkyU
    """
    def __init__(self):
        """ Instantiates the model with a root item """
        super(PoseDataModel, self).__init__()
        self.root = None  
        self._topLevelEditable = False
        self.poseCategory = "poseControl"
        self.treeView = None

    def Root(self):
        """ The root PoseDataItem of the Model """
        return self.root

    def setRoot(self, root):
        """
        Sets the root PoseDataItem for the model

        Args:
            root (PoseDataItem): PoseDataItem to use as the root for the model
        """
        self.root = root

    def rowCount(self, parent):
        """
        Returns the number of rows under the given parent

        Args:
            parent (QModelIndex)

        Returns:
            int: 0 or number of children
        """
        if not parent.isValid():
            parentItem = self.root
        else:
            parentItem = parent.internalPointer()

        if parentItem is None:
            return 0

        return parentItem.childCount()

    def columnCount(self, parent):
        """
        Returns the number of columns for the children of the given parent

        This model will have only one column

        Args:
            parent (QtCore.QModelIndex)

        Returns:
            int: 1
        """
        return 1

    def data(self, index, role):
        """
        Returns the data stored under the given role for the item referred to by the index

        Args:
            index (QtCore.QModelIndex): model index for the current item
            role (int): Qt.DisplayRole

        Returns:
             object: item.data(role)
        """
        if not index.isValid():
            return None

        item = index.internalPointer()
        return item.data(role)

    def headerData( self, section, orientation, role):
        """
        Args:
            section (int)
            orientation (Qt.Orientation)
            role (int): Qt.DisplayRole

        Returns:
             str: "Poses"
        """
        return "Poses"

    def flags(self, index):
        """
        Flags that tell the view how to handle the items

        Args:
            index (QtCore.QModelIndex): model index for the current item

        Returns:
            QtCore.Qt.ItemIsEnabled
            QtCore.Qt.ItemIsSelectable
            QtCore.Qt.ItemIsDragEnabled
            QtCore.Qt.ItemIsDropEnabled
        """
        # Invalid indices (open space in the view) are also drop enabled, so you can drop items onto the top level.
        if not index.isValid():
            return QtCore.Qt.ItemIsEnabled

        return (
            QtCore.Qt.ItemIsEnabled
            | QtCore.Qt.ItemIsSelectable
            | QtCore.Qt.ItemIsDragEnabled
            | QtCore.Qt.ItemIsDropEnabled
        )

    def parent(self, index):
        """
        Returns the parent of the model item with the given index.
        If the item has no parent, an invalid PySide.QtCore.QModelIndex is returned

        Args:
            index (QtCore.QModelIndex): model index for the current item

        Returns:
            ParentItem of QModelIndex
        """
        nodeItem = index.internalPointer()
        parentItem = nodeItem.parent()

        if not parentItem:
            return None
        elif parentItem == self.root:
            return QtCore.QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def itemFromIndex(self, index):
        """
        Returns the pose library item at a given index

        Args:
            index (QtCore.QModelIndex): model index for the current item

        Returns:
             TreeItem instance from a QModelIndex
        """
        return index.internalPointer() if index.isValid() else self.root

    def indexFromItem(self, item):
        """
        Returns the index of a given item in the pose library tree

        Args:
            item: PoseItem

        Returns:
            index (QtCore.QModelIndex)
        """
        parentItem = item.parent()
        if parentItem is None:
            return QtCore.QModelIndex()

        return self.createIndex(item.row(), 0, item)

    def index(self, row, column, parent):
        """
        Returns the index of the item in the model specified by the given row, column and parent index

        Args:
            row (int): index of row
            column (int): index of column
            parent (QtCore.QModelIndex): model index for the current item

        Returns:
            QtCore.QModelIndex
        """
        if not parent.isValid():
            parentItem = self.root
        else:
            parentItem = parent.internalPointer()

        nodeItem = parentItem.child(row)
        if nodeItem is None:
            return QtCore.QModelIndex()

        return self.createIndex(row, 0, nodeItem)

    def supportedDragActions(self):
        """
        Returns the actions supported by the data in this model

        Items can be moved and copied (but we only provide an interface for moving items in this example

        Returns:
            QtCore.Qt.MoveAction
            QtCore.Qt.CopyAction
        """
        return QtCore.Qt.MoveAction | QtCore.Qt.CopyAction

    def supportedDropActions(self):
        """
        Returns the drop actions supported by this model

        Items can be moved and copied (but we only provide an interface for moving items in this example

        Returns:
            QtCore.Qt.MoveAction
            QtCore.Qt.CopyAction
        """
        return QtCore.Qt.MoveAction | QtCore.Qt.CopyAction
        
    def mimeTypes(self):
        """
        Returns a list of MIME types that can be used to describe a list of model indexes

        The MimeType for the encoded data

        Returns:
            list: list of strings
        """
        return ["text/plain", u"application/x-qabstractitemmodeldatalist", u"application/x-qstandarditemmodeldatalist"]

    def mimeData(self, indices):
        """
        Encode serialized data from the item at the given index into a QMimeData object

        Returns:
            QtCore.QMimeData
        """
        data = None
        items = []
        [items.append(self.itemFromIndex(index)) for index in indices if self.itemFromIndex(index) not in items]

        try:
            data = cPickle.dumps(items)
        except Exception, e:
            print "MimeData catch"
            print e

            pass

        # Custom extended QtCore.MimeData class
        mimedata = PoseMimeData()
        if data:
            mimedata.setData("text/plain", data)
        mimedata.setIndices(indices)

        mimedata.category = self.poseCategory 

        return mimedata

    def dropMimeData(self, data, action, row, column, parentIndex):
        """
        Handles the dropping of an item onto the model.

        De-serializes the data into a TreeItem instance and inserts it into the model

        Args:
            data: QtCore.QMimeData
            action: QtCore.Qt.DropAction
            row: int
            column: int
            parentIndex: QtCore.QModelIndex
        """
        # Add data to the data structure
        inputData = str(data.data("text/plain"))
        if len(inputData) == 0:
            return True

        items = cPickle.loads(inputData)
        if len(items) == 0:
            return True

        dropIndex = self.itemFromIndex(parentIndex)
        sourceCategory = data.category

        moveModeIsEnabled = self.isMoveModeEnabled(sourceCategory)
        currentSelection = data.indices()

        offsetIndex = 0
        for item in items :
            item.movePoseTo(dropIndex, offsetIndex=offsetIndex)
            offsetIndex += 1

        if moveModeIsEnabled is True:
            self._deleteSelection(currentSelection)

        if self.treeView is not None:
            self.treeView.refreshViewFromPoseRoot()

        return True

    def isMoveModeEnabled(self, sourceCategory):
        """
        Returns True or False to enable deleting items after they've been moved

        Args:
            sourceCategory: QMimeData category

        Returns:
            bool: True or False
        """
        moveMode = False
        if sourceCategory == self.poseCategory:
            moveMode = True

        return moveMode

    def _deleteSelection(self, selection):
        """
        Deletes any selected pose/folder from the library

        Args:
            selection: Current selected model items in pose library tree
        """
        for index in selection:
            currentPoseData = self.itemFromIndex(index)
            currentPoseData.removeFromTree()