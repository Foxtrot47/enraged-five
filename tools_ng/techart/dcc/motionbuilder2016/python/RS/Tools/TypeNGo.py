"""
Logic for the Type&Go Motion Builder tool

Image:
    $TechArt/images/TypeAndGo.png
"""
import sys

import RS.Tools.UI.Commands


def Search(toolName, registeredTools):
    """
    Event handler for a menu item.  Based on the menu item name, looks up a RsToolMenuItem in the
    registeredTools dictionary.  Attempts to import/load the module and execute the script.

    Arguments:

        toolName (string): name of the tool
        registeredTools (list[string, etc.]): list of the names of the available R* tools

    """
    found = False
    message = ""
    
    if toolName in registeredTools:
        toolItem = registeredTools[toolName]

        # Import/reload the module.
        if toolItem.ModulePath:
            if toolItem.ModulePath not in sys.modules:
                __import__(toolItem.ModulePath, globals(), locals(), [], -1)
                module = sys.modules[toolItem.ModulePath]
                module.Run()

            else:
                module = sys.modules[toolItem.ModulePath]
                reload(module)
                module.Run()
            found = True
        # Execute a function from the commands module.    
        elif toolItem.Command:
            func = getattr(RS.Tools.UI.Commands, toolItem.Command)

            if func:
                func()
                found = True
            else:
                message = 'Could not find the command named "{}"!'.format(toolItem.Command)

        else:
            message = 'There is no command or module associated with this menu item!'
            
    return found, message