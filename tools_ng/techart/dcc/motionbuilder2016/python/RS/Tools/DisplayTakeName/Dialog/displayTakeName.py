"""
Dockable dialog for displaying the take number portion of the trial name.

Requested in url:bugstar:3591221.
"""
import re

from PySide import QtCore, QtGui

from RS import Globals
from RS.Tools import UI


class DisplayTakeNameDialog(UI.QtMainWindowBase):
    """
    Dialog for displaying the take name via a non-HUD element during playback
    for the director on the mo-cap stage.
    
    Tested against the following trial names:
        "000006_01_BE_captureBoss_Test_001",
        "000006_02_BE_captureBoss_Test_001",
        "000006_03_BE_captureBoss_Test_001",
        "000006_04_BE_captureBoss_Test_001",
        "000006_05_BE_captureBoss_Test_001",
        "000006_06_BE_captureBoss_Test_001",
        "000072_01_SD_WATCHSTAR_GRAB_TEST",
        "000072_02_SD_WATCHSTAR_GRAB_TEST",
        "000073_01_SD_CREATE_TAKE_W_LAYERING_TEST",
        "000073_02_SD_CREATE_TAKE_W_LAYERING_TEST",
    """
    TOOL_NAME = "Display Take Number"

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(DisplayTakeNameDialog, self).__init__(parent=parent,
                                                    title=self.TOOL_NAME,
                                                    dockable=True,
                                                    store=True)

        self._defaultFontSize = 30  # Matches height of Shelftastic.
        self._stepSize = 10  # Anything lower requires too many clicks.

        self.setupUi()

    def setupUi(self):
        """
        Set up the UI and attach the connections.
        """
        # Layouts.
        layout = QtGui.QHBoxLayout()
        sizeButtonsLayout = QtGui.QVBoxLayout()

        # Widgets.
        centralWidget = QtGui.QWidget()
        self._displayText = QtGui.QLabel()
        increaseSizeButton = QtGui.QPushButton("+")
        decreaseSizeButton = QtGui.QPushButton("-")

        # Configure widgets.
        self._setFontSize(self._defaultFontSize)
        self._updateDisplayText()
        self._displayText.setAlignment(QtCore.Qt.AlignCenter)
        self._displayText.font().setBold(True)
        increaseSizeButton.setFixedWidth(20)
        increaseSizeButton.setSizePolicy(QtGui.QSizePolicy.Fixed,
                                               QtGui.QSizePolicy.MinimumExpanding)
        decreaseSizeButton.setFixedWidth(20)
        decreaseSizeButton.setSizePolicy(QtGui.QSizePolicy.Fixed,
                                               QtGui.QSizePolicy.MinimumExpanding)

        # Add widgets to layouts.
        sizeButtonsLayout.addWidget(increaseSizeButton)
        sizeButtonsLayout.addWidget(decreaseSizeButton)

        layout.addWidget(self._displayText)
        layout.addLayout(sizeButtonsLayout)

        # Set layouts to widgets.
        self.setCentralWidget(centralWidget)
        centralWidget.setLayout(layout)

        # Hook up connections.
        increaseSizeButton.pressed.connect(self._handleIncreaseSize)
        decreaseSizeButton.pressed.connect(self._handleDecreaseSize)

        # Register callbacks; If refactored to be a widget, move to showEvent.
        self._registerCallbacks()

    def closeEvent(self, event):
        """
        Reimplements QtMainWindowBase method.
                
        Arguments:
            event (QtCore.QEvent): event invoked by the UI
        """
        self._unregisterCallbacks()
        event.accept()

    def _setFontSize(self, size):
        """
        Set the label's font size by replacing with a new font object.

        Arguments:
            size: The point size to use for the font.
        """
        newFont = QtGui.QFont("", size)
        newFont.setBold(True)
        self._displayText.setFont(newFont)
        self.layout().setSizeConstraint(QtGui.QLayout.SetFixedSize)

    def _handleIncreaseSize(self):
        """
        Handle the increaseSizeButton.pressed signal.
        """
        fontSize = self._displayText.font().pointSize()
        newFontSize = fontSize + self._stepSize
        self._setFontSize(newFontSize)

    def _handleDecreaseSize(self):
        """
        Handle the decreaseSizeButton.pressed signal.
        """
        fontSize = self._displayText.font().pointSize()
        newFontSize = fontSize - self._stepSize
        self._setFontSize(newFontSize)

    def _updateDisplayText(self):
        """
        Updates the display text based on the current take name.
        """
        # Current take will be set to None when new files are opened, etc.
        if Globals.System.CurrentTake is None:
            return

        currentTakeName = Globals.System.CurrentTake.Name

        # No speed gain from using re.compile here, so using re.search instead.
        result = re.search('^\d{6}_(\d{2})', currentTakeName)
        if result is not None:
            takeNumber = result.groups()[0]
        else:
            takeNumber = currentTakeName

        self._displayText.setText(takeNumber)

    def _updateDisplayTextCallback(self, control=None, event=None):
        """
        Callback for updating the widget's display text.

        Method signature matches requirements for MotionBuilder's callback
        system.

        Arguments:
            control (pyfbsdk.FBControl): UI or component that initiated the callback.
            event (pyfbsdk.FBEvent): the event that triggered the callback.
        """
        self._updateDisplayText()
        self.layout().setSizeConstraint(QtGui.QLayout.SetFixedSize)

    def _registerCallbacks(self):
        """
        Using the CallbackManager in Globals.Callbacks is preferred to using the 
        FBSystem().Scene.OnTakeChange.Add method.
        """
        Globals.Callbacks.OnTakeChange.Add(self._updateDisplayTextCallback)

    def _unregisterCallbacks(self):
        """
        Using the CallbackManager in Globals.Callbacks is preferred to using the 
        FBSystem().Scene.OnTakeChange.Remove method.
        """
        Globals.Callbacks.OnTakeChange.Remove(self._updateDisplayTextCallback)
