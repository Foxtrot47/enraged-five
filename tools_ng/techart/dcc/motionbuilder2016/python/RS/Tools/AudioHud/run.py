from RS.Tools.AudioHud.Widgets import AudioHud


def Run( show = True ):
    toolsDialog = AudioHud.MainWindow()
    
    if show:
        toolsDialog.show()

    return toolsDialog