'''
AudioHud
Tools
'''

import pyfbsdk as mobu
import os, shutil, subprocess, webbrowser

from PySide import QtGui, QtCore

import RS.Tools.UI
from RS import Config, Globals, Perforce


def openVideoHelper():
    ### open the video helper tool###
    # first, create a shortcut the subprocess likes
    # note: "Desktop\VideoHelper.exe - Shortcut.lnk" should have already been setup by the user as it is a requirement.
    # adding in a catch to deal with this
    try:
        old_file = os.path.join("C:\\Users", RS.Config.User.Name, "Desktop\VideoHelper.exe - Shortcut.lnk")
        new_file = os.path.join("C:\\Users", RS.Config.User.Name, "Desktop\VideoHelperShortcut.lnk")
        
        if not os.path.isfile(new_file):
            shutil.copy2(old_file, new_file)
        
        proc = subprocess.Popen(" ".join(["start", "/B" , new_file]), shell=True)
        proc.wait()
    except:
        QtGui.QMessageBox.warning(None, "VideoHelper Setup Error.",
                                "No VideoHelper setup on this PC.\nPlease make sure there is a shortcut on your desktop." )  


def help( ):
    webbrowser.open('https://hub.gametools.dev/display/ANIM/Audio+Hud')


def perforceFileCheck(p4File): 
    
    if p4File.lower().startswith("x:\\"):
        currentDepotFile = []
        
        filename = os.path.basename(p4File)
        dirPath = os.path.dirname(p4File)
        
        p4Path = p4File
        
        PerforceFileData = Perforce.Where([p4Path])# get perforce stats on our file

        for item in PerforceFileData:      
            depotFile = item['depotFile']
            currentDepotFile.append(depotFile.encode("utf-8"))

        if currentDepotFile:
            Perforce.Sync(currentDepotFile[0])
            Perforce.Edit(currentDepotFile[0])
        else:
            QtGui.QMessageBox.warning(None, "Perforce error.",
                                    p4Path + "\nNot a depot File" ) 
            return False   