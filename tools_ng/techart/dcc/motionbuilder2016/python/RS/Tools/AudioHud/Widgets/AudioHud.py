"""
AudioHud
Based of several other render tools that exist, but made specificially for GTAO
"""

import pyfbsdk as mobu
import xml.etree.ElementTree as ET
import getpass
import os
import sys
import shutil
import subprocess
import ctypes
import re
import time
import webbrowser

from xml.dom import minidom
from datetime import datetime, timedelta
from PySide import QtGui, QtCore
from operator import itemgetter

import RS.Tools.UI
from RS import Config, Globals, Perforce
from RS.Utils import Namespace
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Audio import Audio
from RS.Tools.UI import Application

from RS.Tools.AudioHud.Dialogs import AddAudioOffset
from RS.Tools.AudioHud.Tools import Tools

AUDIOHUD_CONFIG_PATH = os.path.join(os.path.expandvars("%APPDATA%"), "rockstar", "audiohud", "AudioHud_ConfigData.xml")
username = RS.Config.User.Name
uiDirectory = os.path.join(RS.Config.Script.Path.Root, "RS\\Tools\\UI\\AudioHud\\Qt\\")
iconDirectory = os.path.join(Config.Script.Path.ToolImages, 'AudioHud\\Icons\\')

RS.Tools.UI.CompileUi(uiDirectory + "AudioHud.ui", uiDirectory + "AudioHud_compiled.py")
import RS.Tools.UI.AudioHud.Qt.AudioHud_compiled as AudioHudCompiled


class AutoActivateHud(object):
    """
        class-object to hold the state of the Auto Activate option for the Hud display as we switch takes
    """
    Active = False


class RenderSettings(object):
    """
        class-object to hold the info for the Render Settings
    """
    Custom1Path = ""

    def __init__(self):
        self.data = []


# creating an instance of RenderSettings
RenderSettings = RenderSettings()


class MainWindow(RS.Tools.UI.QtMainWindowBase, AudioHudCompiled.Ui_MainWindow):
    def __init__(self):
        RS.Tools.UI.QtMainWindowBase.__init__(self, None, title='Audio Hud')

        self.setupUi(self)

        banner = RS.Tools.UI.BannerWidget('Audio Hud')

        self.splitter_hor.setSizes([600, 1500])  # set the split sizes
        self.splitter_ver.setSizes([500, 1000, 500])  # set the split sizes
        self.treeWidget_association.setColumnWidth(0, 300)
        self.treeWidget_association.setIconSize(QtCore.QSize(16, 16))

        self.treeWidget_association.setStyleSheet(
            "background-color: rgb(30, 40, 40);")  # the background/header/sidebar colour

        self.treeWidget_association.setAlternatingRowColors(1)

        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Window, QtGui.QColor(255, 255, 255))  # the checkbox boarder
        palette.setColor(QtGui.QPalette.Base, QtGui.QColor(60, 60, 60))  # the checkbox colour
        palette.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(50, 60, 60))  # the lighter alternate line colour
        self.treeWidget_association.setPalette(palette)

        self.Manager = Manager()

        self.wavIcon = QtGui.QIcon(iconDirectory + "wav_icon.png")
        self.animIcon = QtGui.QIcon(iconDirectory + "anim_icon.png")
        self.cameraIcon = QtGui.QIcon(iconDirectory + "camera_icon.png")
        self.cameraNoIcon = QtGui.QIcon(iconDirectory + "camera_no_icon.png")
        self.offsetIcon = QtGui.QIcon(iconDirectory + "offset_icon.png")

        self.label_headerIcon.setPixmap(iconDirectory + "AudioHud_logo.png")
        self.label_headerText.setPixmap(iconDirectory + "banner.png")

        icon = QtGui.QPixmap(iconDirectory + "AudioHud_help.png")
        self.pushButton_headerIcon_Help.setIcon(icon)
        self.pushButton_headerIcon_Help.clicked.connect(Tools.help)

        self.populateTakeList()
        self.pupulateAudioList()
        self.pupulateCameraList()
        self.pupulateRenderOptions()

        self.listWidget_audio.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.listWidget_audio.customContextMenuRequested.connect(self.showAudioRightClickMenu)
        self.listWidget_audio.setStyleSheet("background-color: rgb(30, 40, 40);")

        self.listWidget_take.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.listWidget_take.customContextMenuRequested.connect(self.showTakeRightClickMenu)
        self.listWidget_take.setStyleSheet("background-color: rgb(30, 40, 40);")

        self.listWidget_camera.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.listWidget_camera.customContextMenuRequested.connect(self.showCameraRightClickMenu)
        self.listWidget_camera.setStyleSheet("background-color: rgb(30, 40, 40);")

        self.treeWidget_association.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.treeWidget_association.customContextMenuRequested.connect(self.showAssociationRightClickMenu)
        self.treeWidget_association.itemClicked.connect(self.onClickItem)

        self.checkBox_addTodaysDate.stateChanged.connect(self.addTodaysDate)

        self.lineEdit_outputPath.textChanged.connect(self.writeToCustom2DataNode)

        self.lineEdit_prefix_suffix.textChanged.connect(self.writeToPrefixSuffixDataNode)
        self.radioButton_prefix.toggled.connect(self.writeToPrefixSuffixDataNode)
        self.radioButton_suffix.toggled.connect(self.writeToPrefixSuffixDataNode)

        self.pushButton_openOutputFolder.clicked.connect(self.openExplorer)

        # make a list of the radio buttons as its just easier to iterate through them
        self.renderDestRadioButtons = [self.radioButton_output_Desktop,
                                       self.radioButton_output_GTA,
                                       self.radioButton_output_Guess,
                                       self.radioButton_output_Custom,
                                       self.radioButton_output_Custom2]

        for radioButton in self.renderDestRadioButtons:
            radioButton.toggled.connect(self.getRenderDest)

        self.comboBox_pictureFormat.currentIndexChanged.connect(self.updateCustomFormat)
        self.comboBox_customRenderSettings.currentIndexChanged.connect(self.findCustomSettings)

        self.pushButton_startRender.clicked.connect(self.startRender)
        self.pushButton_saveRenderSettings.clicked.connect(self.saveConfig)
        self.pushButton_updateRenderSettings.clicked.connect(self.updateConfig)
        self.pushButton_deleteRenderSettings.clicked.connect(self.deleteConfig)

        # setup some lists to hold the treewidget data
        self.treeData = []
        self.treeCamera = []
        self.treeAudioOffset = []
        self.treeCheckState = []

        # first run of the methods that set up the tool
        self.createDataNode()
        self.readDataNode()
        self.colouriseWidgets()
        self.readConfigXML()
        self.getRenderDest()

    
    def closeEvent(self, event):
        # un-register callbacks
        if AutoActivateHud.Active:
            AutoActivateHud.Active = False
            Globals.Callbacks.OnTakeChange.Remove(self.onTakeChanged)

    
    def openExplorer(self):
        path = "explorer " + self.lineEdit_outputPath.text()
        subprocess.Popen(path)

    
    def showAudioRightClickMenu(self):

        menu = QtGui.QMenu()

        self.addAudio = QtGui.QAction("Start Making Associations", self)
        self.addAudio.triggered.connect(self.addAudioFile)

        self.sortList = QtGui.QAction("Sort List", self)
        self.sortList.triggered.connect(self.sortAudioFiles)

        self.updateList = QtGui.QAction("Update Audio List", self)
        self.updateList.triggered.connect(self.pupulateAudioList)

        self.createBlank = QtGui.QAction("Create *BLANK*", self)
        self.createBlank.triggered.connect(self.createBlankAudioFile)

        self.createCombinedSelect = QtGui.QAction("Create Combined Selection", self)
        self.createCombinedSelect.triggered.connect(self.combinedSelect)

        menu.addAction(self.addAudio)
        menu.addAction(self.sortList)
        menu.addAction(self.updateList)
        menu.addAction(self.createBlank)
        menu.addAction(self.createCombinedSelect)

        menu.exec_(QtGui.QCursor.pos())

    
    def showTakeRightClickMenu(self):

        menu = QtGui.QMenu()

        self.associateTake = QtGui.QAction("Associatiate with Audio File", self)
        self.associateTake.triggered.connect(self.addTakeToAudio)

        self.updateTake = QtGui.QAction("Update Take List", self)
        self.updateTake.triggered.connect(self.populateTakeList)

        self.createCombinedSelect = QtGui.QAction("Create Combined Selection", self)
        self.createCombinedSelect.triggered.connect(self.combinedSelect)

        menu.addAction(self.associateTake)
        menu.addAction(self.updateTake)
        menu.addAction(self.createCombinedSelect)

        menu.exec_(QtGui.QCursor.pos())

    
    def showCameraRightClickMenu(self):

        menu = QtGui.QMenu()

        self.associateCamera = QtGui.QAction("Associatiate with Take or Group", self)
        self.associateCamera.triggered.connect(self.associateCameraWithTake)

        self.updateCamera = QtGui.QAction("Update Camera List", self)
        self.updateCamera.triggered.connect(self.pupulateCameraList)

        self.createCombinedSelect = QtGui.QAction("Create Combined Selection", self)
        self.createCombinedSelect.triggered.connect(self.combinedSelect)

        menu.addAction(self.associateCamera)
        menu.addAction(self.updateCamera)
        menu.addAction(self.createCombinedSelect)

        menu.exec_(QtGui.QCursor.pos())

    
    def showAssociationRightClickMenu(self):

        menu = QtGui.QMenu()
        self.selectAllTakes = QtGui.QAction("Select All", self)
        self.selectAllTakes.triggered.connect(self.selectAssociationItem)
        self.selectNoTakes = QtGui.QAction("De-Select All", self)
        self.selectNoTakes.triggered.connect(self.deSelectAssociationItem)

        self.selectAllTakesInGroup = QtGui.QAction("Select Everything in Group", self)
        self.selectAllTakesInGroup.triggered.connect(self.selectAssociationGroupItem)

        self.selectNoTakesInGroup = QtGui.QAction("De-Select Everything in Group", self)
        self.selectNoTakesInGroup.triggered.connect(self.deSelectAssociationGroupItem)

        self.hudActiveToggle = QtGui.QAction("Make This Take Active", self)
        self.hudActiveToggle.triggered.connect(self.makeThisTakeActive)

        self.autoHud = QtGui.QAction("Automatically Update Hud", self, checkable=True)
        if AutoActivateHud.Active:
            self.autoHud.setChecked(True)
        self.autoHud.triggered.connect(self.hudActiveToggleisChecked)

        self.deleteHuds = QtGui.QAction("Delete All Audio Huds", self)
        self.deleteHuds.triggered.connect(self.deleteAudioRefs)
        self.deleteHuds.triggered.connect(self.deleteAudioHud)

        self.expand = QtGui.QAction("Expand All Cameras", self)
        self.expand.triggered.connect(self.expandAll)

        self.collapse = QtGui.QAction("Collapse All Cameras", self)
        self.collapse.triggered.connect(self.collapseAll)

        self.addOffset = QtGui.QAction("Add Audio Offset", self)
        self.addOffset.triggered.connect(self.addAudioOffsetWindow)

        self.deleteItem = QtGui.QAction("Delete Selected Item", self)
        self.deleteItem.triggered.connect(self.deleteAssociationItem)

        renderOptions = menu.addMenu("Render Selection Options")
        renderOptions.addAction(self.selectAllTakes)
        renderOptions.addAction(self.selectNoTakes)
        renderOptions.addSeparator().setText("")
        renderOptions.addAction(self.selectAllTakesInGroup)
        renderOptions.addAction(self.selectNoTakesInGroup)

        hudOptions = menu.addMenu("Hud Options")
        hudOptions.addAction(self.hudActiveToggle)
        hudOptions.addAction(self.autoHud)
        hudOptions.addAction(self.deleteHuds)

        menu.addSeparator().setText("")
        menu.addAction(self.expand)
        menu.addAction(self.collapse)
        menu.addAction(self.addOffset)
        menu.addAction(self.deleteItem)

        menu.exec_(QtGui.QCursor.pos())

    
    def addAudioOffsetWindow(self):
        # new dialog to add in audio offset (as we can't do this as part of QMenu
        dialogue = AddAudioOffset.AddAudioOffsetWindow()
        dialogue.addAudioOffsetSignal.connect(self.addAudioOffset)
        dialogue.show()
        dialogue.move(QtGui.QCursor.pos())

    
    def hudActiveToggleisChecked(self):
        # way to handle the AutoActivateHud state and the UI rightclickmenu for Associations
        if AutoActivateHud.Active:
            AutoActivateHud.Active = False
            self.deleteAudioHud()
        else:
            AutoActivateHud.Active = True

        self.autoActivateHud()  # this will setup or remove the callback accordingly

    
    def addAudioFile(self):

        audioFile = [item.text().encode("utf-8") for item in self.listWidget_audio.selectedItems()]
        getSelected = self.treeWidget_association.selectedItems()

        audioRoot = self.treeWidget_association.invisibleRootItem()
        child_count = audioRoot.childCount()

        treeItems = []

        if child_count == 0:
            root = QtGui.QTreeWidgetItem(self.treeWidget_association, audioFile)
            root.setIcon(0, self.wavIcon)
            treeItems.append(root.text(0).encode("utf-8"))
            self.treeData.append((audioFile))
            self.treeCamera.append(["audioGroup"])
            self.treeAudioOffset.append(["audioGroup"])

        for i in range(child_count):
            item = audioRoot.child(i)
            treeItems.append(item.text(0).encode("utf-8"))

        if not audioFile[0] in treeItems:
            root = QtGui.QTreeWidgetItem(self.treeWidget_association, audioFile)
            root.setIcon(0, self.wavIcon)
            treeItems.append(root.text(0).encode("utf-8"))
            self.treeData.append(audioFile)
            self.treeCamera.append(["audioGroup"])
            self.treeAudioOffset.append(["audioGroup"])

        self.listWidget_audio.clearSelection()

        self.getCheckState()
        self.writeToDataNode()
        self.colouriseWidgets()

    
    def addTakeToAudio(self):

        selectedTakes = [item.text().encode("utf-8") for item in self.listWidget_take.selectedItems()]
        getSelected = self.treeWidget_association.selectedItems()

        takeRoot = self.treeWidget_association.invisibleRootItem()
        root_count = takeRoot.childCount()

        addedTakes = []
        treeItems = []

        for i in range(root_count):
            item = takeRoot.child(i)
            child_count = item.childCount()
            for i in range(child_count):
                child = item.child(i)
                treeItems.append(child.text(0).encode("utf-8"))

        for item in self.treeWidget_association.selectedItems():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) == None:  # or it has no parent because its a root node (audio file)
                for audioGroup in range(len(self.treeData)):
                    if item.text(0) in self.treeData[audioGroup]:
                        baseNode = getSelected[0]
                        for take in selectedTakes:
                            if not take in treeItems:
                                addedTakes.append(take)
                                audioGroupIndex = self.treeData.index(self.treeData[audioGroup])
                                self.treeData[audioGroup].append(take)
                                self.treeCamera[audioGroup].append([''])
                                self.treeAudioOffset[audioGroup].append("")

                                takeindex = self.treeData[audioGroupIndex].index(take)

                                item = QtGui.QTreeWidgetItem(baseNode, [take])
                                item.setIcon(0, self.animIcon)
                                item.setIcon(1, self.cameraNoIcon)
                                item.setFlags(
                                    item.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)  # code to add the flag
                                item.setCheckState(0, QtCore.Qt.Unchecked)

            self.treeWidget_association.setItemExpanded(getSelected[0],
                                                        True)  # set newly added items expanded just in case the user wanted camera collapsed before adding new take

        self.listWidget_take.clearSelection()

        self.getCheckState()
        self.writeToDataNode()
        self.colouriseWidgets()

    
    def associateCameraWithTake(self):

        selectedCamera = [item.text().encode("utf-8") for item in self.listWidget_camera.selectedItems()]
        getSelected = self.treeWidget_association.selectedItems()

        for ix in self.treeWidget_association.selectedIndexes():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) != None:
                for audioGroup in self.treeData:
                    if ix.data(QtCore.Qt.DisplayRole) in audioGroup:
                        audioGroupIndex = self.treeData.index(audioGroup)
                        cameraIndex = self.treeData[audioGroupIndex].index(getSelected[0].data(0, 0))

                        if not str(selectedCamera[0]) in self.treeCamera[audioGroupIndex][cameraIndex]:
                            if self.treeCamera[audioGroupIndex][cameraIndex][-1] == '':
                                self.treeCamera[audioGroupIndex][cameraIndex][-1] = str(selectedCamera[0])
                            else:
                                self.treeCamera[audioGroupIndex][cameraIndex].append(str(selectedCamera[0]))

                            if len(self.treeCamera[audioGroupIndex][
                                       cameraIndex]) == 2:  # need to delete off the first camera association from adjacent to the take (so we can start a clean branch)
                                # print "len(self.treeCamera[audioGroupIndex][cameraIndex])", len(self.treeCamera[audioGroupIndex][cameraIndex])
                                getSelected[0].setText(1, '')  # clear the text
                                getSelected[0].setIcon(1, QtGui.QIcon())  # set an empty icon

                            if len(self.treeCamera[audioGroupIndex][cameraIndex]) > 1:
                                if getSelected[
                                    0].childCount() == 0:  # need to see if we have deleted off the first camera, but not re-added it as a child
                                    for item in self.treeCamera[audioGroupIndex][cameraIndex]:
                                        takeItem = QtGui.QTreeWidgetItem(getSelected[0], [0, item])
                                        takeItem.setText(1, item)
                                        takeItem.setIcon(1, self.cameraIcon)
                                        # takeItem.setFlags(takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)# code to add the flag
                                        takeItem.setCheckState(1, QtCore.Qt.Unchecked)
                                else:
                                    takeItem = QtGui.QTreeWidgetItem(getSelected[0], [0,
                                                                                      self.treeCamera[audioGroupIndex][
                                                                                          cameraIndex][-1]])
                                    takeItem.setText(1, self.treeCamera[audioGroupIndex][cameraIndex][-1])
                                    takeItem.setIcon(1, self.cameraIcon)
                                    # takeItem.setFlags(takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)# code to add the flag
                                    takeItem.setCheckState(1, QtCore.Qt.Unchecked)
                            else:
                                getSelected[0].setText(1, self.treeCamera[audioGroupIndex][cameraIndex][0])
                                getSelected[0].setIcon(1, self.cameraIcon)

            else:
                for audioGroup in self.treeData:
                    if ix.data(QtCore.Qt.DisplayRole) in audioGroup:
                        audioGroupIndex = self.treeData.index(audioGroup)

                        child_count = getSelected[0].childCount()
                        for i in range(child_count):
                            child = getSelected[0].child(i)

                            if not str(selectedCamera[0]) in self.treeCamera[audioGroupIndex][i + 1]:

                                if self.treeCamera[audioGroupIndex][i + 1][-1] == '':
                                    self.treeCamera[audioGroupIndex][i + 1][-1] = str(selectedCamera[0])
                                else:
                                    self.treeCamera[audioGroupIndex][i + 1].append(str(selectedCamera[0]))

                                if len(self.treeCamera[audioGroupIndex][
                                           i + 1]) == 2:  # need to delete off the first camera association from adjacent to the take (so we can start a clean branch)
                                    child.setText(1, '')  # clear the text
                                    child.setIcon(1, QtGui.QIcon())  # set an empty icon

                                if len(self.treeCamera[audioGroupIndex][i + 1]) > 1:

                                    if child.childCount() == 0:
                                        for item in self.treeCamera[audioGroupIndex][i + 1]:
                                            takeItem = QtGui.QTreeWidgetItem(child, [0, item])
                                            takeItem.setText(1, item)
                                            takeItem.setIcon(1, self.cameraIcon)
                                            # takeItem.setFlags(takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)# code to add the flag
                                            takeItem.setCheckState(1, QtCore.Qt.Unchecked)
                                    else:
                                        takeItem = QtGui.QTreeWidgetItem(child, [0, self.treeCamera[audioGroupIndex][
                                            i + 1][-1]])
                                        takeItem.setText(1, self.treeCamera[audioGroupIndex][i + 1][-1])
                                        takeItem.setIcon(1, self.cameraIcon)
                                        # takeItem.setFlags(takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)# code to add the flag
                                        takeItem.setCheckState(1, QtCore.Qt.Unchecked)

                                else:
                                    child.setText(1, self.treeCamera[audioGroupIndex][i + 1][0])
                                    child.setIcon(1, self.cameraIcon)

                            self.treeWidget_association.setItemExpanded(child,
                                                                        True)  # set newly added items expanded just in case the user wanted camera collapsed before adding new take

        self.getCheckState()
        self.writeToDataNode()
        self.colouriseWidgets()

    
    def combinedSelect(self):
        # make quick selections from the three asset lists (audio, takes, camera)

        if not self.listWidget_audio.selectedItems() or not self.listWidget_take.selectedItems():  # quick test to see if can continue (do we have everything selected?)
            return

        self.addAudioFile()

        takeRoot = self.treeWidget_association.invisibleRootItem()
        root_count = takeRoot.childCount()

        self.treeWidget_association.setCurrentItem(takeRoot.child(root_count - 1), 0)

        self.addTakeToAudio()
        self.associateCameraWithTake()

    
    def addAudioOffset(self, offset):

        getSelected = self.treeWidget_association.selectedItems()

        for ix in self.treeWidget_association.selectedIndexes():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) != None:
                for audioGroup in self.treeData:
                    if audioGroup.__contains__(ix.data(QtCore.Qt.DisplayRole)):

                        audioGroupIndex = self.treeData.index(audioGroup)

                        offsetIndex = self.treeData[audioGroupIndex].index(getSelected[0].data(0, 0))
                        if offset == "0":
                            self.treeAudioOffset[audioGroupIndex][offsetIndex] = ""
                            getSelected[0].setText(2, "")
                            getSelected[0].setIcon(2, QtGui.QIcon())  # set an empty icon
                        else:
                            self.treeAudioOffset[audioGroupIndex][offsetIndex] = str(offset)
                            getSelected[0].setText(2, offset)
                            getSelected[0].setIcon(2, self.offsetIcon)

        self.writeToDataNode()

    
    def sortAudioFiles(self):
        self.listWidget_audio.sortItems()  # ascending by default

    
    def createBlankAudioFile(self):

        itemList = [self.listWidget_audio.item(i).text() for i in range(self.listWidget_audio.count())]

        if not "*BLANK*" in itemList:
            self.listWidget_audio.addItem(str("*BLANK*"))

    
    def getParents(self, item):
        """
         Return a list containing all parent items of this item.
         The list is empty, if the item is a root item.
         """
        parents = []
        current_item = item
        current_parent = current_item.parent()

        # Walk up the tree and collect all parent items of this item
        while not current_parent is None:
            parents.append(current_parent)
            current_item = current_parent
            current_parent = current_item.parent()
        return parents

    
    def deleteAssociationItem(self):

        getSelected = self.treeWidget_association.selectedItems()
        associationRoot = self.treeWidget_association.invisibleRootItem()

        self.parents = []

        for item in self.treeWidget_association.selectedItems():
            self.parents = self.getParents(item)

        if len(self.parents) == 1:

            takeName = getSelected[0].data(0, QtCore.Qt.DisplayRole)

            for group in range(len(self.treeData)):

                if self.parents[0].data(0, QtCore.Qt.DisplayRole) == self.treeData[group][0]:

                    takeIndex = self.treeData[group].index('{}'.format(takeName))

                    del self.treeData[group][takeIndex]
                    del self.treeCamera[group][takeIndex]
                    del self.treeAudioOffset[group][takeIndex]

                    for item in self.treeWidget_association.selectedItems():
                        (item.parent() or associationRoot).removeChild(item)

                    self.getCheckState()
                    self.writeToDataNode()
                    self.colouriseWidgets()
                    return

        for ix in self.treeWidget_association.selectedIndexes():

            node = self.treeWidget_association.currentIndex()

            if (node.parent()).data(QtCore.Qt.DisplayRole) == None:

                for group in range(len(self.treeData)):

                    if node.data(QtCore.Qt.DisplayRole) == self.treeData[group][0]:
                        del self.treeData[group]
                        del self.treeCamera[group]
                        del self.treeAudioOffset[group]

                        for item in self.treeWidget_association.selectedItems():
                            (item.parent() or associationRoot).removeChild(item)

                        self.getCheckState()
                        self.writeToDataNode()
                        self.colouriseWidgets()
                        return

            elif getSelected[0].childCount() == 0:

                takeItem = node.parent()
                takeName = takeItem.data(QtCore.Qt.DisplayRole)

                audioGroupItem = takeItem.parent()
                audioGroupName = audioGroupItem.data(QtCore.Qt.DisplayRole)

                cameraName = getSelected[0].data(1, QtCore.Qt.DisplayRole)

                for group in range(len(self.treeData)):

                    if audioGroupName == self.treeData[group][0]:

                        takeIndex = self.treeData[group].index('{}'.format(takeName))
                        cameraIndex = self.treeCamera[group][takeIndex].index('{}'.format(cameraName))

                        del self.treeCamera[group][takeIndex][cameraIndex]

                        for item in self.treeWidget_association.selectedItems():
                            parent = item.parent()
                            (item.parent() or associationRoot).removeChild(item)

                        if len(self.treeCamera[group][takeIndex]) == 1:
                            parent.removeChild(parent.child(0))
                            parent.setText(1, self.treeCamera[group][takeIndex][0])
                            parent.setIcon(1, self.cameraIcon)

                        self.getCheckState()
                        self.writeToDataNode()
                        self.colouriseWidgets()
                        return

    
    def expandAll(self):
        self.treeWidget_association.expandToDepth(1)

    
    def collapseAll(self):
        self.treeWidget_association.expandToDepth(0)

    
    @QtCore.Slot(QtGui.QTreeWidgetItem, int)
    def onClickItem(self, it, col):
        '''
            callback when an item is clicked on the treewidget (we need to do some checking and unchecking of checkboxes) 
        '''
        parents = self.getParents(it)

        if len(parents) == 0:
            return

        if len(parents) == 2:
            if it.checkState(1):
                if not parents[0].checkState(0):
                    parents[0].setCheckState(0, QtCore.Qt.Checked)

        child_count = it.childCount()

        if child_count > 1:  # lets use the checkbox of a take with multiple cameras as a way to select/deselect all of its children directly
            if it.checkState(0):
                for i in range(child_count):
                    child = it.child(i)
                    child.setCheckState(1, QtCore.Qt.Checked)
            else:
                for i in range(child_count):
                    child = it.child(i)
                    child.setCheckState(1, QtCore.Qt.Unchecked)

        if it.data(1, QtCore.Qt.DisplayRole) == '' or it.data(1,
                                                              QtCore.Qt.DisplayRole) == None:  # no camera? no selection
            if not it.child(0):  # if it doesn't have a child there is definately no camera
                it.setCheckState(0, QtCore.Qt.Unchecked)

        self.getCheckState()

    
    def getCheckState(self):
        # time to store all the checked checkboxes 
        self.treeCheckState = []

        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidget_association)

        while iterator.value():
            item = iterator.value()

            column0Value = item.checkState(0)
            column1Value = item.checkState(1)

            if item.checkState(0):
                column0Value = True
            else:
                column0Value = False

            if item.checkState(1):
                column1Value = True
            else:
                column1Value = False

            self.treeCheckState.append([column0Value, column1Value])
            iterator += 1

        self.writeToDataNode()

    
    def setCheckState(self):
        # restore checkstate after tool re-open
        if not self.treeCheckState:
            return

        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidget_association)

        count = 0
        while iterator.value():
            item = iterator.value()

            if eval(self.treeCheckState[count][0]):
                item.setCheckState(0, QtCore.Qt.Checked)

            if eval(self.treeCheckState[count][1]):
                item.setCheckState(1, QtCore.Qt.Checked)

            iterator += 1
            count += 1

    
    def selectAssociationItem(self):
        # this handles the 'select all' option
        getSelected = self.treeWidget_association.selectedItems()
        associationRoot = self.treeWidget_association.invisibleRootItem()
        root_count = associationRoot.childCount()

        for audioGroup in range(root_count):
            take = associationRoot.child(audioGroup)
            child_count = take.childCount()
            for camera in range(child_count):
                takeChild = take.child(camera)
                takeChild_count = takeChild.childCount()

                if takeChild.data(1, QtCore.Qt.DisplayRole) != None and takeChild.data(1,
                                                                                       QtCore.Qt.DisplayRole) != '':  # select takes with no children
                    takeChild.setCheckState(0, QtCore.Qt.Checked)

                if takeChild_count > 1:  # select takes with children
                    for i in range(takeChild_count):
                        takeChild.setCheckState(0, QtCore.Qt.Checked)
                        childCamera = takeChild.child(i)
                        childCamera.setCheckState(1, QtCore.Qt.Checked)

        self.getCheckState()

    
    def deSelectAssociationItem(self):
        # this handles the 'deselect all' option
        getSelected = self.treeWidget_association.selectedItems()
        associationRoot = self.treeWidget_association.invisibleRootItem()
        root_count = associationRoot.childCount()

        for audioGroup in range(root_count):
            take = associationRoot.child(audioGroup)
            child_count = take.childCount()
            for camera in range(child_count):
                takeChild = take.child(camera)
                takeChild_count = takeChild.childCount()

                if takeChild.data(1, QtCore.Qt.DisplayRole) != None and takeChild.data(1,
                                                                                       QtCore.Qt.DisplayRole) != '':  # deselect takes with no children
                    takeChild.setCheckState(0, QtCore.Qt.Unchecked)

                if takeChild_count > 1:  # deselect takes with children
                    for i in range(takeChild_count):
                        takeChild.setCheckState(0, QtCore.Qt.Unchecked)
                        childCamera = takeChild.child(i)
                        childCamera.setCheckState(1, QtCore.Qt.Unchecked)

        self.getCheckState()

    
    def selectAssociationGroupItem(self):
        # this handles the 'select all' items in an audio group option
        getSelected = self.treeWidget_association.selectedItems()

        for ix in self.treeWidget_association.selectedIndexes():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) == None:
                takeChild_count = getSelected[0].childCount()
                for takes in range(takeChild_count):
                    takeChild = getSelected[0].child(takes)
                    takeChild_count = takeChild.childCount()
                    if takeChild.data(1, QtCore.Qt.DisplayRole) != None and takeChild.data(1,
                                                                                           QtCore.Qt.DisplayRole) != '':
                        takeChild.setCheckState(0, QtCore.Qt.Checked)
                    if takeChild_count > 1:  # select multiple cameras associated with take
                        for i in range(takeChild_count):
                            takeChild.setCheckState(0, QtCore.Qt.Checked)
                            childCamera = takeChild.child(i)
                            childCamera.setCheckState(1, QtCore.Qt.Checked)

        self.getCheckState()

    
    def deSelectAssociationGroupItem(self):
        # this handles the 'deselect all' items in an audio group option
        getSelected = self.treeWidget_association.selectedItems()

        for ix in self.treeWidget_association.selectedIndexes():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) == None:
                takeChild_count = getSelected[0].childCount()
                for takes in range(takeChild_count):
                    takeChild = getSelected[0].child(takes)
                    takeChild_count = takeChild.childCount()
                    if takeChild.data(1, QtCore.Qt.DisplayRole) != None and takeChild.data(1,
                                                                                           QtCore.Qt.DisplayRole) != '':
                        takeChild.setCheckState(0, QtCore.Qt.Unchecked)
                    if takeChild_count > 1:  # deselect multiple cameras associated with take
                        for i in range(takeChild_count):
                            takeChild.setCheckState(0, QtCore.Qt.Unchecked)
                            childCamera = takeChild.child(i)
                            childCamera.setCheckState(1, QtCore.Qt.Unchecked)

        self.getCheckState()

    
    def populateTakeList(self):
        # clear out listWidget if it is populated
        itemList = [self.listWidget_take.item(i) for i in range(self.listWidget_take.count())]
        for item in range(len(itemList)):
            self.listWidget_take.takeItem(self.listWidget_take.row(itemList[item]))

        for take in RS.Globals.Scene.Takes:
            self.listWidget_take.addItem(str(take.Name))

    
    def pupulateAudioList(self):
        audioFiles = []

        audioFiles = [sceneAudio.Namespace for sceneAudio in self.Manager.GetReferenceListByType(Audio)]

        # clear out listWidget if it is populated
        itemList = [self.listWidget_audio.item(i) for i in range(self.listWidget_audio.count())]
        for item in range(len(itemList)):
            self.listWidget_audio.takeItem(self.listWidget_audio.row(itemList[item]))

        for audioFile in audioFiles:
            self.listWidget_audio.addItem(str(audioFile))

        # mute audio for the referenced in audio files (new instances will default to the users normal output)    
        for lComp in Globals.System.Scene.Components:
            for audioFile in audioFiles:
                if re.search('{0}:{0}.{1}'.format(audioFile, 'wav'), lComp.LongName,
                             re.IGNORECASE):  # regexp search ftw # make it case insensitive
                    for lProp in lComp.PropertyList:
                        if lProp.Name == 'Destination':
                            lProp.Data = 0

        self.sortAudioFiles()

    
    def pupulateCameraList(self):

        currentCam = Globals.System.Scene.Renderer.CurrentCamera

        cameras = Globals.System.Scene.Cameras

        # clear out listWidget if it is populated
        itemList = [self.listWidget_camera.item(i) for i in range(self.listWidget_camera.count())]
        for item in range(len(itemList)):
            self.listWidget_camera.takeItem(self.listWidget_camera.row(itemList[item]))

        for camera in cameras:
            self.listWidget_camera.addItem(str(camera.Name))

        itemList = [self.listWidget_camera.item(i) for i in range(self.listWidget_camera.count())]
        for item in range(len(itemList)):
            if self.listWidget_camera.item(item).text() == currentCam.Name:
                self.listWidget_camera.item(item).setSelected(True)

    
    def pupulateRenderOptions(self):

        RenderSettings.data.append(["Default", "2", "1280", "720", "0", "-10", "0", "3.0", "2", "2", "1"])

        stockPictureFormats = ["3840 * 2160 (4K)", "1920 * 1080 (HD1080)", "1280 * 720 (HD720)", "720 * 480 (NTSCDVD)",
                               "640 * 480 (PALDVD)"]
        preferredFonts = ["Arial", "Helvetica", "Bookman Old Style", "Times New Roman", "Verdana", "Palatino",
                          "Garamond", "Tahoma"]
        availaileFonts = []

        justification = ["Left", "Right", "Center"]
        horizontal = ["Left", "Right", "Center"]
        vertical = ["Bottom", "Top", "Center"]

        for item in stockPictureFormats:
            self.comboBox_pictureFormat.addItem(item)

        # create a temp hud to get the font list (avaliable fonts might vary from PC to PC?)
        HUDTextElement = mobu.FBHUDTextElement("Audio Filepath")

        for item in HUDTextElement.GetFontList():
            if item in preferredFonts:
                availaileFonts.append(item)

        self.deleteAudioHud()  # delete the hud we generated specifically to find the availibale fonts

        availaileFonts = sorted(availaileFonts,
                                key=preferredFonts.index)  # sort list of availiable fonts in order according to the preferred fonts list

        for item in availaileFonts:
            self.comboBox_font.addItem(item)  # add the sorted avilaible fonts to the combobox

        for item in justification:
            self.comboBox_justification.addItem(item)

        for item in horizontal:
            self.comboBox_horizontalDock.addItem(item)

        for item in vertical:
            self.comboBox_verticalDock.addItem(item)

        self.setRenderOptions(0)
        self.comboBox_customRenderSettings.addItem(RenderSettings.data[0][0])

    
    def setRenderOptions(self, index):
        # read the items from the class object to change the values accordingly

        self.lineEdit_customRenderSettings.setText(RenderSettings.data[index][0])
        self.comboBox_pictureFormat.setCurrentIndex(int(RenderSettings.data[index][1]))
        self.spinBox_customWidth.setValue(int(RenderSettings.data[index][2]))
        self.spinBox_customHeight.setValue(int(RenderSettings.data[index][3]))
        self.spinBox_hud_X.setValue(int(RenderSettings.data[index][4]))
        self.spinBox_hud_Y.setValue(int(RenderSettings.data[index][5]))
        self.comboBox_font.setCurrentIndex(int(RenderSettings.data[index][6]))
        self.doubleSpinBox_fontScale.setValue(float(RenderSettings.data[index][7]))
        self.comboBox_justification.setCurrentIndex(int(RenderSettings.data[index][8]))
        self.comboBox_horizontalDock.setCurrentIndex(int(RenderSettings.data[index][9]))
        self.comboBox_verticalDock.setCurrentIndex(int(RenderSettings.data[index][10]))

    
    def findCustomSettings(self):
        index = self.comboBox_customRenderSettings.currentIndex()
        self.setRenderOptions(index)

    
    def updateCustomFormat(self):
        stockPictureFormats = self.comboBox_pictureFormat.currentText().split(" ")
        self.spinBox_customWidth.setValue(int(stockPictureFormats[0]))
        self.spinBox_customHeight.setValue(int(stockPictureFormats[2]))

    
    def colouriseWidgets(self):

        # colourise the tree and audio lists
        for index, take in enumerate(RS.Globals.Scene.Takes):
            if any(str(take.Name) in item for item in self.treeData):  # find any match within the nested lists
                self.listWidget_take.item(index).setBackground(
                    QtGui.QColor(0, 255, 0, 100))  # we can simply use index as we are never sorting this list
            else:
                self.listWidget_take.item(index).setBackground(QtGui.QColor(255, 0, 0, 150))

        audioFiles = []
        audioFiles = [sceneAudio.Namespace for sceneAudio in self.Manager.GetReferenceListByType(Audio)]

        for audioFile in audioFiles:
            if any(audioFile in item for item in self.treeData):  # find any match within the nested lists
                items = self.listWidget_audio.findItems(audioFile,
                                                        QtCore.Qt.MatchContains)  # we need to find the index by matching the name, this gives us the actual row in the listWidget
                self.listWidget_audio.item(self.listWidget_audio.row(items[0])).setBackground(
                    QtGui.QColor(0, 255, 0, 100))
            else:
                items = self.listWidget_audio.findItems(audioFile, QtCore.Qt.MatchContains)
                self.listWidget_audio.item(self.listWidget_audio.row(items[0])).setBackground(
                    QtGui.QColor(255, 0, 0, 150))


    
    def deselectComponents(self):
        for comp in RS.Globals.Scene.Components:
            comp.Selected = False


    
    def makeThisTakeActive(self):
        # we get a list of everything in the row, but we only want the first item, so we'll add them to a list an grab the first index
        # eventually we can use this to grab all the data we need for the camera and the offset
        takeData = []

        getSelected = self.treeWidget_association.selectedItems()
        if getSelected:
            for ix in self.treeWidget_association.selectedIndexes():
                takeData.append(ix.data(QtCore.Qt.DisplayRole))
                audioRef = ix.parent()
                audioRefName = audioRef.data(QtCore.Qt.DisplayRole)

        for take in RS.Globals.Scene.Takes:
            if takeData[0] == take.Name:
                Globals.System.CurrentTake = take
                self.applyAudiohud(audioRefName, takeData[0], takeData[1], takeData[2])


    def autoActivateHud(self):
        # manage the callback for take change.
        if AutoActivateHud.Active:
            Globals.Callbacks.OnTakeChange.Add(self.onTakeChanged)
        else:
            Globals.Callbacks.OnTakeChange.Remove(self.onTakeChanged)


    def onTakeChanged(self, source, event):
        # get rid of huds if there is no association - can't have this any other way, as it just kills the hud?
        self.deleteAudioRefs()
        self.deleteAudioHud()

        take = Globals.System.CurrentTake
        for audioGroup in range(len(self.treeData)):
            if take.Name in self.treeData[audioGroup]:
                audioGroupIndex = self.treeData.index(self.treeData[audioGroup])
                takeindex = self.treeData[audioGroupIndex].index(take.Name)

                audioRefName = self.treeData[audioGroup][0]
                cameraName = self.treeCamera[audioGroup][takeindex]
                audioOffset = self.treeAudioOffset[audioGroup][takeindex]

                self.applyAudiohud(audioRefName, take.Name, cameraName, audioOffset)


    def applyAudiohud(self, audioRefName, takeName, cameraName, offsetAmount):

        if audioRefName != "*BLANK*":
            audioRefModel = mobu.FBFindModelByLabelName('{0}:{1}'.format('RS_Null', audioRefName))
            audioRefPath = audioRefModel.PropertyList.Find('Reference Path')
            self.deleteAudioRefs()
            self.deleteAudioHud()
            self.createAudioHub(audioRefPath.Data, cameraName)
            self.applyAudio(audioRefName, takeName, audioRefPath.Data, offsetAmount)
        else:
            self.deleteAudioRefs()
            self.deleteAudioHud()
            self.getPaneCameras(cameraName)


    def applyAudio(self, audioRefName, takeName, audioRefPath, offsetAmount):
        # apply audio to take
        for take in Globals.System.Scene.Takes:
            if take.Name == takeName:
                Globals.System.CurrentTake = take

        lAudio = mobu.FBAudioClip(audioRefPath)
        lAudio.LongName = '{0}:{1}'.format(lAudio.LongName, 'AudioRef')

        if not offsetAmount == None and not offsetAmount == "":
            InPoint = lAudio.PropertyList.Find('InPoint')
            InPoint.Data = mobu.FBTime(0, 0, 0, int(offsetAmount))

    
    def deleteAudioRefs(self):
        # delete out any existing temp audio instances
        for lComp in Globals.System.Scene.Components:
            if re.search(str(".wav:AudioRef"), lComp.LongName,
                         re.IGNORECASE):  # regexp search ftw # make it case insensitive
                lComp.FBDelete()

        self.deleteStrayAudioData()

    
    def deleteAudioHud(self):

        deleteHuds = []
        deleteElements = []
        cameras = Globals.System.Scene.Cameras

        # delete the huds
        for camera in cameras:
            for camHud in camera.HUDs:
                if camHud.Name.startswith("IgRenderHud"):
                    deleteHuds.append(camHud)
                for hudElement in camHud.Elements:
                    if hudElement.Name.startswith("IgRenderAudio"):
                        deleteElements.append(hudElement)
        deleteList = deleteHuds + deleteElements

        for deleteItem in deleteList:
            deleteItem.FBDelete()

    
    def deleteStrayAudioData(self):
        # deleting the left over audio data that seems to not be connected to an audio file?
        deleteStrayAudio = []
        for lComp in Globals.System.Scene.Components:
            if 'Audio Filepath' in lComp.LongName:
                deleteStrayAudio.append(lComp)

        for strayAudioData in deleteStrayAudio:
            strayAudioData.FBDelete()


    def getPaneCameras(self, cameraName):
        
        cameras = Globals.System.Scene.Cameras

        for camera in cameras:
            if camera.Name == cameraName:
                Globals.System.Renderer.CurrentCamera = camera

        currentCam = Globals.System.Renderer.CurrentCamera

        # nifty code to figure out if we are using multiple panes (aka viewports) and deal with that (we need to set the current pane to the desired camera)
        self.paneCameras = []

        for paneCam in range(4):
            paneCamera = Globals.System.Scene.Renderer.GetCameraInPane(paneCam).Name
            self.paneCameras.append(paneCamera)
            Globals.System.Scene.Renderer.SetCameraInPane(currentCam, paneCam)

    
    def createAudioHub(self, audioName, cameraName):
        
        self.getPaneCameras(cameraName)
        
        cameras = Globals.System.Scene.Cameras

        for camera in cameras:
            if camera.Name == cameraName:
                Globals.System.Renderer.CurrentCamera = camera

        currentCam = Globals.System.Renderer.CurrentCamera

        #currentCamera = Globals.System.Renderer.CurrentCamera
        camHuds = list(currentCam.HUDs)
        if not camHuds:
            currentHud = mobu.FBHUD("IgRenderHud")
            Globals.System.Scene.ConnectSrc(currentHud)
        else:
            currentHud = camHuds[0]

        HUDTextElement = mobu.FBHUDTextElement("Audio Filepath")
        HUDTextElement.Content = audioName
        HUDTextElement.X = self.spinBox_hud_X.value()
        HUDTextElement.Y = self.spinBox_hud_Y.value()
        HUDTextElement.Font = str(self.comboBox_font.currentText())
        HUDTextElement.Height = float(self.doubleSpinBox_fontScale.text())

        if str(self.comboBox_justification.currentText()) == "Left":
            HUDTextElement.Justification = mobu.FBHUDElementHAlignment.kFBHUDLeft
        elif str(self.comboBox_justification.currentText()) == "Right":
            HUDTextElement.Justification = mobu.FBHUDElementHAlignment.kFBHUDRight
        elif str(self.comboBox_justification.currentText()) == "Center":
            HUDTextElement.Justification = mobu.FBHUDElementHAlignment.kFBHUDCenter

        if str(self.comboBox_horizontalDock.currentText()) == "Left":
            HUDTextElement.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDLeft
        elif str(self.comboBox_horizontalDock.currentText()) == "Right":
            HUDTextElement.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDRight
        elif str(self.comboBox_horizontalDock.currentText()) == "Center":
            HUDTextElement.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDCenter

        if str(self.comboBox_verticalDock.currentText()) == "Bottom":
            HUDTextElement.VerticalDock = mobu.FBHUDElementVAlignment.kFBHUDBottom
        elif str(self.comboBox_verticalDock.currentText()) == "Top":
            HUDTextElement.VerticalDock = mobu.FBHUDElementVAlignment.kFBHUDTop
        elif str(self.comboBox_verticalDock.currentText()) == "Center":
            HUDTextElement.VerticalDock = mobu.FBHUDElementVAlignment.kFBHUDCenter

        currentHud.ConnectSrc(HUDTextElement)
        currentCam.ConnectSrc(currentHud)

    
    def startRender(self):

        self.deselectComponents()

        associationRoot = self.treeWidget_association.invisibleRootItem()
        root_count = associationRoot.childCount()

        takeToRender = []
        renderCamera = []
        audioOffset = []

        for audioRoot in range(root_count):
            takeToRender.append([])
            renderCamera.append([])
            audioOffset.append([])
            takeToRender[audioRoot].append(self.treeData[audioRoot][0])
            renderCamera[audioRoot].append(self.treeCamera[audioRoot][0])
            audioOffset[audioRoot].append(self.treeAudioOffset[audioRoot][0])

            item = associationRoot.child(audioRoot)
            child_count = item.childCount()
            for animFile in range(child_count):
                animFileChild = item.child(animFile)
                if animFileChild.checkState(0):
                    if animFileChild.childCount() > 1:
                        cameraList_count = animFileChild.childCount()
                        selectedCameras = []
                        for cameraList in range(cameraList_count):
                            cameraChild = animFileChild.child(cameraList)
                            if cameraChild.checkState(1):
                                selectedCameras.append(
                                    '{0}{1}'.format("~sub~", cameraChild.data(1, QtCore.Qt.DisplayRole)))

                        if selectedCameras:
                            takeToRender[audioRoot].append(self.treeData[audioRoot][animFile + 1])
                            renderCamera[audioRoot].append(selectedCameras)
                            audioOffset[audioRoot].append(self.treeAudioOffset[audioRoot][animFile + 1])
                    else:
                        if self.treeCamera[audioRoot][animFile + 1] != ['']:
                            takeToRender[audioRoot].append(self.treeData[audioRoot][animFile + 1])
                            renderCamera[audioRoot].append(self.treeCamera[audioRoot][animFile + 1])
                            audioOffset[audioRoot].append(self.treeAudioOffset[audioRoot][animFile + 1])
                        else:
                            animFileChild.setCheckState(0, QtCore.Qt.Unchecked)
                            QtGui.QMessageBox.warning(None, "Selection error:",
                                                      "There were anims selected to render that had no camera allocated?\nThese have been de-selected.")

        for audioGroup in range(len(takeToRender)):
            if len(takeToRender[audioGroup]) != 1:
                audioRefName = takeToRender[audioGroup][0]
                for take in range(1, len(takeToRender[audioGroup])):
                    takeName = takeToRender[audioGroup][take]
                    cameraName = renderCamera[audioGroup][take]
                    offsetAmount = audioOffset[audioGroup][take]

                    for takes in Globals.System.Scene.Takes:
                        if takes.Name == takeName:
                            Globals.System.CurrentTake = takes
                            if len(cameraName) > 1:
                                for subCamera in cameraName:
                                    subCamera = subCamera.replace("~sub~", "")
                                    self.applyAudiohud(audioRefName, takeName, subCamera, offsetAmount)
                                    cameraName = subCamera.replace(" ", "_")
                                    saveName = self.createRenderName('{0}_{1}'.format(takeName, cameraName))
                                    self.render(saveName, takes)
                            else:
                                if "~sub~" in cameraName[0]:
                                    subCamera = cameraName[0].replace("~sub~", "")
                                    self.applyAudiohud(audioRefName, takeName, subCamera, offsetAmount)
                                    cameraName = subCamera.replace(" ", "_")
                                    saveName = self.createRenderName('{0}_{1}'.format(takeName, cameraName))
                                    self.render(saveName, takes)
                                else:
                                    self.applyAudiohud(audioRefName, takeName, cameraName, offsetAmount)
                                    saveName = self.createRenderName(takeName)
                                    self.render(saveName, takes)

        if self.checkBox_videoHelper.isChecked():
            try:
                Tools.openVideoHelper()
            except Exception, e:
                QtGui.QMessageBox.warning(None, "Video Helper error:",
                                          "There was an issue opening the Video Helper:\n\n" + str(e))

        self.deleteAudioRefs()
        self.deleteAudioHud()

    
    def createRenderName(self, userTakeName):
        # lets sort out the render filename
        if self.lineEdit_prefix_suffix.text() != "":
            if self.radioButton_prefix.isChecked():
                userTakeName = self.lineEdit_prefix_suffix.text() + userTakeName
            else:
                userTakeName = userTakeName + self.lineEdit_prefix_suffix.text()

        if self.checkBox_upper.isChecked():
            userTakeName = userTakeName.upper()
        else:
            userTakeName = userTakeName.lower()

        saveName = self.lineEdit_outputPath.text() + userTakeName

        if not os.path.isdir(self.lineEdit_outputPath.text()):
            os.makedirs(self.lineEdit_outputPath.text())

        return saveName

    
    def render(self, saveName, takeToRender):

        if self.checkBox_checkOut.isChecked():
            p4File = saveName + ".mov"
            Tools.perforceFileCheck(p4File)

        startFrame = Globals.System.CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        endFrame = Globals.System.CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        mgr = mobu.FBVideoCodecManager()
        mgr.SetDefaultCodec("mov", "mov")  # make sure we're using the .mov codec
        mgr.VideoCodecMode = mobu.FBVideoCodecMode.FBVideoCodecStored
        app = mobu.FBApplication()

        lCamera = Globals.System.Renderer.CurrentCamera
        lCamera.FrameSizeMode = mobu.FBCameraFrameSizeMode.kFBFrameSizeFixedResolution
        lCamera.ResolutionWidth = int(self.spinBox_customWidth.value())
        lCamera.ResolutionHeight = int(self.spinBox_customHeight.value())
        lCamera.PixelAspectRatio = 1

        videoOptions = mobu.FBVideoGrabber().GetOptions()
        videoOptions.FrameSizeMode = mobu.FBCameraFrameSizeMode.kFBFrameSizeFixedResolution
        videoOptions.CameraResolution = mobu.FBCameraResolutionMode.kFBResolutionCustom

        videoOptions.BitsPerPixel = mobu.FBVideoRenderDepth.FBVideoRender32Bits
        videoOptions.ShowTimeCode = True
        videoOptions.RenderAudio = True
        captureRange = mobu.FBTimeSpan()
        captureRange.Set(mobu.FBTime(0, 0, 0, startFrame), mobu.FBTime(0, 0, 0, endFrame))
        videoOptions.TimeSteps = mobu.FBTime(0, 0, 0, 1)
        videoOptions.TimeSpan = captureRange

        saveName = '{0}_{1}'.format(saveName, 'out.mov')

        videoOptions.OutputFileName = saveName

        # delete any files with existing name
        renderExist = os.path.exists(saveName)
        if renderExist:
            os.remove(saveName)

        # render take
        # must make take to render current take or videos will not render properly.
        Globals.System.CurrentTake = takeToRender
        app.FileRender(videoOptions)

        # convert render using ffmpeg
        ffmpeg = r'{0}\bin\video\ffmpeg.exe'.format(RS.Config.Tool.Path.Root)
        sourceMovie = saveName
        finalMovie = sourceMovie.replace('_out.mov', '.mov')

        if not os.path.exists(ffmpeg):
            QtGui.QMessageBox.warning(None, "Render Conversion error:",
                                      "ffmpeg library does not exist:\nPlease check for - ('<project>/tools/bin/video/ffmpeg.exe') " + str(
                                          e))
        else:
            # use convertion settings;
            # audio codec = MP3 (This was removed as ffmpeg would create playback errors. command was '-acodec libmp3lame')
            # number of audio tracks = 2 (stereo)
            # audio rate 44100 (cd quality)
            # video codec = H.264
            # GOP (group of pictures - helps looping) = 10
            # overwrite existing file = yes
            try:
                os.system(
                    '%s -i "%s" -ac 2 -ar 44100 -vcodec libx264 -g 10 -y "%s"' % (ffmpeg, sourceMovie, finalMovie))
                os.remove(sourceMovie)
            except Exception, e:
                QtGui.QMessageBox.warning(None, "Render issue:", "There was an issue rendering this file:\n\n" + str(e))

        ##### reset window size #####
        lCamera = Globals.System.Renderer.CurrentCamera
        lCamera.FrameSizeMode = mobu.FBCameraFrameSizeMode.kFBFrameSizeWindow

        self.copyTo(finalMovie)

        # dealing with multiple panes ( aka viewports ) set back to how they were before we started rendering
        cameras = Globals.System.Scene.Cameras
        for paneCamera in range(4):
            for camera in cameras:
                if camera.Name == self.paneCameras[paneCamera]:
                    Globals.System.Scene.Renderer.SetCameraInPane(camera, paneCamera)

    
    def getRenderDest(self):

        lModel = mobu.FBFindModelByLabelName("AUDIOHUD_DATA_NODE")
        custom2_checked = lModel.PropertyList.Find('AudioHud_Custom2_checked')
        custom2 = lModel.PropertyList.Find('AudioHud_Custom2_text')

        self.dstLocations = []

        # desktop filepath
        self.desktopPath = (os.path.join(os.path.expandvars("%userprofile%"), 'Desktop', "Renders" + "\\"))
        self.dstLocations.append(self.desktopPath)

        # gta filepath
        today = datetime.now().date()
        start = today - timedelta(days=today.weekday())
        end = start + timedelta(days=6)
        username = RS.Config.User.Name

        self.gtaPath = ("\\\\rockstar.t2.corp\\network\\Projects\\GTA5\\Animation\\WeeklyUpdates\\"
                        + str(start.strftime('%Y'))
                        + "\\Week Commencing "
                        + str(start)
                        + "\\Scripted\\"
                        + username + "\\")

        self.dstLocations.append(self.gtaPath)

        # guess filepath
        sourceFileName = RS.Globals.Application.FBXFileName
        dirPath, fileName = os.path.split(sourceFileName)
        dirPath = dirPath.replace("source_fbx", "resources\\renders")

        self.guessPath = (dirPath + "\\")
        self.dstLocations.append(self.guessPath)

        # custom filepath
        if self.radioButton_output_Desktop.isChecked():
            if RenderSettings.Custom1Path != "":
                self.lineEdit_outputPath.setText(RenderSettings.Custom1Path)
        self.dstLocations.append(RenderSettings.Custom1Path)

        # custom 2 filepath
        custom2 = lModel.PropertyList.Find('AudioHud_Custom2_text')
        self.custom2Path = (str(custom2.Data))
        self.dstLocations.append(self.custom2Path)

        if self.radioButton_output_Desktop.isChecked():
            custom2_checked.Data = "False"
            self.lineEdit_outputPath.setText(self.desktopPath)

        elif self.radioButton_output_GTA.isChecked():
            custom2_checked.Data = "False"
            self.lineEdit_outputPath.setText(self.gtaPath)

        elif self.radioButton_output_Guess.isChecked():
            custom2_checked.Data = "False"
            self.lineEdit_outputPath.setText(self.guessPath)

        elif self.radioButton_output_Custom.isChecked():
            custom2_checked.Data = "False"
            self.lineEdit_outputPath.setText(RenderSettings.Custom1Path)

        elif self.radioButton_output_Custom2.isChecked():
            self.lineEdit_outputPath.setText(self.custom2Path)

    
    def copyTo(self, finalMovie):

        copyToList = [self.pushButton_copyto_desktop, self.pushButton_copyto_gtaWeeklies, self.pushButton_copyto_guess,
                      self.pushButton_copyto_custom1, self.pushButton_copyto_custom2]

        filename = os.path.basename(finalMovie)

        try:
            for dst in range(len(copyToList)):
                if copyToList[dst].isChecked():
                    if not os.path.isdir(self.dstLocations[dst]):  # check to see if the copyTo folder exists
                        os.makedirs(self.dstLocations[dst])

                    copyToFile = os.path.join(self.dstLocations[dst], filename)
                    Tools.perforceFileCheck(copyToFile)  # see if this is a depot file
                    shutil.copyfile(finalMovie, copyToFile)
        except Exception, e:
            QtGui.QMessageBox.warning(None, "Copy issue:",
                                      "There was an issue copying the data to the other folders:\n\n" + str(e))

    
    def addTodaysDate(self):

        todaysDate = datetime.now().date()

        text = self.lineEdit_outputPath.text()
        if self.checkBox_addTodaysDate.isChecked():
            text = (os.path.join(text, str(todaysDate) + "\\"))
            self.lineEdit_outputPath.setText(text)
            self.checkBox_addTodaysDate.setCheckState(
                QtCore.Qt.CheckState(False))  # works better than - .setChecked(bool)

    
    def createDataNode(self):

        if not mobu.FBFindModelByLabelName("AUDIOHUD_DATA_NODE"):
            lModel = mobu.FBModelMarker("AUDIOHUD_DATA_NODE")
            lModel.Show = False
            lModel.PropertyCreate('AudioHud_Tree', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AudioHud_Camera', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AudioHud_Audio_Offset', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AudioHud_Checked_States', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AudioHud_prefix_option', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AudioHud_prefix_suffix_text', mobu.FBPropertyType.kFBPT_charptr, 'String', False,
                                  True, None),  # Not animatable
            lModel.PropertyCreate('AudioHud_Custom2_checked', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AudioHud_Custom2_text', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable

    
    def readDataNode(self):
        lModel = mobu.FBFindModelByLabelName("AUDIOHUD_DATA_NODE")

        rawTreeData = lModel.PropertyList.Find('AudioHud_Tree')
        rawCameraData = lModel.PropertyList.Find('AudioHud_Camera')
        rawAudioOffsetData = lModel.PropertyList.Find('AudioHud_Audio_Offset')
        rawCheckStateData = lModel.PropertyList.Find('AudioHud_Checked_States')
        prefix_option = lModel.PropertyList.Find('AudioHud_prefix_option')
        prefix_suffix_text = lModel.PropertyList.Find('AudioHud_prefix_suffix_text')
        custom2_checked = lModel.PropertyList.Find('AudioHud_Custom2_checked')
        custom2_text = lModel.PropertyList.Find('AudioHud_Custom2_text')

        if rawTreeData.Data != "":
            self.createTreeFromDataNode(rawTreeData.Data, rawCameraData.Data, rawAudioOffsetData.Data,
                                        rawCheckStateData.Data)

        if prefix_suffix_text.Data != "":
            self.lineEdit_prefix_suffix.setText(prefix_suffix_text.Data)

        if prefix_option.Data != "":
            if prefix_option.Data == "True":
                self.radioButton_prefix.setChecked(True)
            else:
                self.radioButton_suffix.setChecked(True)

        if custom2_checked.Data == "True":
            self.radioButton_output_Custom2.setChecked(True)
            self.lineEdit_outputPath.setText(custom2_text.Data)

    
    def writeToDataNode(self):
        # write to scene datanode   
        lModel = mobu.FBFindModelByLabelName("AUDIOHUD_DATA_NODE")
        rawTreeData = lModel.PropertyList.Find('AudioHud_Tree')
        rawCameraData = lModel.PropertyList.Find('AudioHud_Camera')
        rawAudioOffsetData = lModel.PropertyList.Find('AudioHud_Audio_Offset')
        rawCheckStateData = lModel.PropertyList.Find('AudioHud_Checked_States')
        rawTreeData.Data = ""
        rawCameraData.Data = ""
        rawAudioOffsetData.Data = ""
        rawCheckStateData.Data = ""
        textTreeData = ""
        textCameraData = ""
        textAudioOffsetData = ""
        textCheckStateData = ""

        for audioGroup in self.treeData:
            textTreeData = textTreeData + "audioGroup: " + audioGroup[0] + "\n"
            if len(audioGroup) != 1:
                for take in range(1, len(audioGroup)):
                    textTreeData = textTreeData + "   take: " + audioGroup[take] + "\n"

        # print "writeToDataNode start\n", self.treeCamera
        for audioGroup in self.treeCamera:
            # print audioGroup[0]
            textCameraData = textCameraData + "audioGroup: " + audioGroup[0] + "\n"
            if len(audioGroup) != 1:
                for camera in range(1, len(audioGroup)):
                    textCameraData = textCameraData + "   camera: " + (', '.join(audioGroup[camera])) + "\n"

        for audioGroup in self.treeAudioOffset:
            textAudioOffsetData = textAudioOffsetData + "audioGroup: " + audioGroup[0] + "\n"
            if len(audioGroup) != 1:
                for AudioOffset in range(1, len(audioGroup)):
                    textAudioOffsetData = textAudioOffsetData + "   audioOffset: " + audioGroup[AudioOffset] + "\n"

        if self.treeCheckState:
            for line in self.treeCheckState:
                textCheckStateData = '{0}{1}{2}{3}{4}'.format(textCheckStateData, line[0], "~", line[1], "\n")

        rawTreeData.Data = textTreeData
        rawCameraData.Data = textCameraData
        rawAudioOffsetData.Data = textAudioOffsetData
        rawCheckStateData.Data = textCheckStateData

    
    def writeToCustom2DataNode(self):
        lModel = mobu.FBFindModelByLabelName("AUDIOHUD_DATA_NODE")
        custom2_checked = lModel.PropertyList.Find('AudioHud_Custom2_checked')
        custom2_text = lModel.PropertyList.Find('AudioHud_Custom2_text')

        if self.radioButton_output_Custom.isChecked():
            RenderSettings.Custom1Path = str(self.lineEdit_outputPath.text())
            self.saveConfigXML()

        if self.radioButton_output_Custom2.isChecked():
            custom2_checked.Data = "True"
            custom2_text.Data = str(self.lineEdit_outputPath.text())

    
    def writeToPrefixSuffixDataNode(self):
        lModel = mobu.FBFindModelByLabelName("AUDIOHUD_DATA_NODE")
        prefix_option = lModel.PropertyList.Find('AudioHud_prefix_option')
        prefix_suffix_text = lModel.PropertyList.Find('AudioHud_prefix_suffix_text')
        prefix_suffix_text.Data = str(self.lineEdit_prefix_suffix.text())

        if self.radioButton_prefix.isChecked():
            prefix_option.Data = "True"
        elif self.radioButton_suffix.isChecked():
            prefix_option.Data = "False"

    
    def createTreeFromDataNode(self, rawTreeData, rawCameraData, rawAudioOffsetData, rawCheckStateData):

        rawTreeGroups = rawTreeData.split("\n")
        rawTreeGroups.pop()

        rawCameraGroups = rawCameraData.split("\n")
        rawCameraGroups.pop()

        rawAudioOffsetGroups = rawAudioOffsetData.split("\n")
        rawAudioOffsetGroups.pop()

        rawCheckStateGroups = rawCheckStateData.split("\n")
        rawCheckStateGroups.pop()

        self.treeData = []
        self.treeCamera = []
        self.treeAudioOffset = []
        self.treeCheckState = []

        step = 0

        for item in range(len(rawTreeGroups)):
            if "audioGroup: " in rawTreeGroups[item]:
                rawTreeGroup = rawTreeGroups[item].replace("audioGroup: ", "")
                self.treeData.append([rawTreeGroup])
                step += 1
            else:
                rawTake = rawTreeGroups[item].replace("   take: ", "")
                self.treeData[step - 1].append(rawTake)

        step = 0

        for item in range(len(rawCameraGroups)):
            if "audioGroup: " in rawCameraGroups[item]:
                rawCameraGroup = rawCameraGroups[item].replace("audioGroup: ", "")
                self.treeCamera.append([rawCameraGroup])
                step += 1
            else:
                rawCamera = rawCameraGroups[item].replace("   camera: ", "")
                rawCamera = rawCamera.split(", ")
                self.treeCamera[step - 1].append(rawCamera)

        step = 0

        for item in range(len(rawAudioOffsetGroups)):
            if "audioGroup: " in rawAudioOffsetGroups[item]:
                rawAudioOffsetGroup = rawAudioOffsetGroups[item].replace("audioGroup: ", "")
                self.treeAudioOffset.append([rawAudioOffsetGroup])
                step += 1
            else:
                rawAudioOffset = rawAudioOffsetGroups[item].replace("   audioOffset: ", "")
                self.treeAudioOffset[step - 1].append(rawAudioOffset)

        for item in range(len(rawCheckStateGroups)):
            line = rawCheckStateGroups[item].split("~")
            self.treeCheckState.append(line)

        self.treeWidget_association.clear()

        for index in range(len(self.treeData)):

            audioGroupName = self.treeData[index][0]

            root = QtGui.QTreeWidgetItem(self.treeWidget_association, [audioGroupName])
            root.setIcon(0, self.wavIcon)

            for takeName in range(1, len(self.treeData[index])):
                data = QtGui.QTreeWidgetItem(root, [self.treeData[index][takeName]])
                data.setIcon(0, self.animIcon)
                if self.treeCamera[index][takeName]:
                    if len(self.treeCamera[index][takeName]) == 1:
                        data.setFlags(
                            data.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)  # code to add the flag
                        data.setCheckState(0, QtCore.Qt.Unchecked)

                    if len(self.treeCamera[index][takeName]) > 1:
                        data.setFlags(
                            data.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)  # code to add the flag
                        data.setCheckState(0, QtCore.Qt.Unchecked)
                        for item in self.treeCamera[index][takeName]:
                            takeItem = QtGui.QTreeWidgetItem(data, [0, item])
                            takeItem.setText(1, item)
                            takeItem.setIcon(1, self.cameraIcon)
                            takeItem.setFlags(
                                takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)  # code to add the flag
                            takeItem.setCheckState(1, QtCore.Qt.Unchecked)
                    else:
                        data.setText(1, self.treeCamera[index][takeName][0])

                        if self.treeCamera[index][takeName][0] == '':
                            data.setIcon(1, self.cameraNoIcon)
                        else:
                            data.setIcon(1, self.cameraIcon)

                if self.treeAudioOffset[index][takeName] != '':
                    data.setText(2, self.treeAudioOffset[index][takeName])
                    data.setIcon(2, self.offsetIcon)

        self.treeWidget_association.expandAll()
        self.setCheckState()
        self.colouriseWidgets()

    
    def deleteConfig(self):

        if self.comboBox_customRenderSettings.currentText() == "Default":
            return

        comboBoxIndex = self.comboBox_customRenderSettings.currentIndex()

        renderSettingsText = self.comboBox_customRenderSettings.currentText()

        self.comboBox_customRenderSettings.removeItem(comboBoxIndex)

        index = None

        for customSetting in RenderSettings.data:
            if customSetting[0] == str(renderSettingsText):
                index = RenderSettings.data.index(customSetting)

                del RenderSettings.data[index]

                self.saveConfigXML()

    
    def updateConfig(self):

        if self.lineEdit_customRenderSettings.text() == "":
            return
        if self.lineEdit_customRenderSettings.text() == "Default":
            self.setRenderOptions(0)
            return

        index = None

        for customSetting in RenderSettings.data:
            if customSetting[0] == str(self.lineEdit_customRenderSettings.text()):
                index = RenderSettings.data.index(customSetting)

                RenderSettings.data[index] = ([str(self.lineEdit_customRenderSettings.text()),
                                               str(self.comboBox_pictureFormat.currentIndex()),
                                               str(int(self.spinBox_customWidth.value())),
                                               str(int(self.spinBox_customHeight.value())),
                                               str(int(self.spinBox_hud_X.value())),
                                               str(int(self.spinBox_hud_Y.value())),
                                               str(self.comboBox_font.currentIndex()),
                                               str(float(self.doubleSpinBox_fontScale.text())),
                                               str(self.comboBox_justification.currentIndex()),
                                               str(self.comboBox_horizontalDock.currentIndex()),
                                               str(self.comboBox_verticalDock.currentIndex())])

                self.saveConfigXML()

    
    def saveConfig(self):

        if self.lineEdit_customRenderSettings.text() == "":
            return

        for customSetting in RenderSettings.data:
            if str(
                    self.lineEdit_customRenderSettings.text()) in customSetting:  # so we don't create duplicate settings of the same name, the user can however update a existing setting by using the 'update' button
                return

        settings = [str(self.lineEdit_customRenderSettings.text()),
                    str(self.comboBox_pictureFormat.currentIndex()),
                    str(int(self.spinBox_customWidth.value())),
                    str(int(self.spinBox_customHeight.value())),
                    str(int(self.spinBox_hud_X.value())),
                    str(int(self.spinBox_hud_Y.value())),
                    str(self.comboBox_font.currentIndex()),
                    str(float(self.doubleSpinBox_fontScale.text())),
                    str(self.comboBox_justification.currentIndex()),
                    str(self.comboBox_horizontalDock.currentIndex()),
                    str(self.comboBox_verticalDock.currentIndex())]

        RenderSettings.data.append(settings)

        self.comboBox_customRenderSettings.addItem(str(self.lineEdit_customRenderSettings.text()))
        count = self.comboBox_customRenderSettings.count()
        self.comboBox_customRenderSettings.setCurrentIndex(count - 1)

        self.saveConfigXML()

    
    def saveConfigXML(self):
        # save the AudioHud Configs in pretty formatting :)
        # for the record - C:\Users\%username%\AppData\Roaming\rockstar\audiohud\
        dirPath = os.path.dirname(AUDIOHUD_CONFIG_PATH)

        if not os.path.isdir(dirPath):  # check to see if the config folder exists
            os.makedirs(dirPath)

        fileName = "AudioHud_ConfigData.xml"

        datafile = os.path.join(dirPath, fileName)

        root = ET.Element("AudioHud_Configuration_Settings")

        RenderCustom1 = ET.SubElement(root, "RenderCustom1")

        FilePath = ET.SubElement(RenderCustom1, "FilePath").text = str(RenderSettings.Custom1Path)

        CustomRenderSettings = ET.SubElement(root, "CustomRenderSettings")

        for renderSetting in range(1, len(RenderSettings.data)):
            SavedRenderSettings = ET.SubElement(CustomRenderSettings, "SavedRenderSettings",
                                                Name=RenderSettings.data[renderSetting][0])

            RenderFrameSize = ET.SubElement(SavedRenderSettings, "RenderFrameSize")

            ET.SubElement(RenderFrameSize, "PictureSize", Size=RenderSettings.data[renderSetting][1])
            ET.SubElement(RenderFrameSize, "CustomWidth", Width=RenderSettings.data[renderSetting][2])
            ET.SubElement(RenderFrameSize, "CustomHeight", Height=RenderSettings.data[renderSetting][3])

            HudAlignment = ET.SubElement(SavedRenderSettings, "HudAlignment")
            ET.SubElement(HudAlignment, "AlignX", X=RenderSettings.data[renderSetting][4])

            ET.SubElement(HudAlignment, "AlignY", Y=RenderSettings.data[renderSetting][5])
            ET.SubElement(HudAlignment, "Font", Font=RenderSettings.data[renderSetting][6])
            ET.SubElement(HudAlignment, "FontScale", Scale=RenderSettings.data[renderSetting][7])
            ET.SubElement(HudAlignment, "Justification", Justification=RenderSettings.data[renderSetting][8])
            ET.SubElement(HudAlignment, "HorizontalDock", HorizontalDock=RenderSettings.data[renderSetting][9])
            ET.SubElement(HudAlignment, "VerticalDock", VerticalDock=RenderSettings.data[renderSetting][10])

        xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ", encoding='UTF-8')

        with open(datafile, "w") as f:
            f.write(str(xmlstr.decode('UTF-8')))
            f.close()

    
    def readConfigXML(self):

        dirPath = os.path.dirname(AUDIOHUD_CONFIG_PATH)

        fileName = "AudioHud_ConfigData.xml"

        datafile = os.path.join(dirPath, fileName)

        if not os.path.isfile(datafile):
            self.saveConfigXML()
            return

        RenderSettings.data = RenderSettings.data[:1]  # remove all but first list

        try:
            tree = ET.parse(datafile)
        except Exception, e:
            QtGui.QMessageBox.warning(None, "Custom Render Settings issue:",
                                      "There was an issue reading the custom user render settings:\n\n" + str(e))

        root = tree.getroot()

        customPath = root.findall(".//RenderCustom1/FilePath")
        for child in customPath:
            RenderSettings.Custom1Path = child.text

        for item in root.findall(".//CustomRenderSettings/SavedRenderSettings"):
            SavedRenderSettings = []
            SavedRenderSettings.append(str(item.attrib['Name']))

            for child in item:
                for value in child:
                    if 'Size' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Size']))
                    if 'Width' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Width']))
                    if 'Height' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Height']))

                    if 'X' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['X']))
                    if 'Y' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Y']))
                    if 'Font' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Font']))
                    if 'Scale' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Scale']))
                    if 'Justification' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Justification']))
                    if 'HorizontalDock' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['HorizontalDock']))
                    if 'VerticalDock' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['VerticalDock']))

            RenderSettings.data.append(SavedRenderSettings)

        for settings in range(1, len(RenderSettings.data)):
            self.comboBox_customRenderSettings.addItem(RenderSettings.data[settings][0])


def Run():
    form = MainWindow()
    form.show()
