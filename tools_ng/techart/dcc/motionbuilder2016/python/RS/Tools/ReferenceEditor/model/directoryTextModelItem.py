from PySide import QtCore, QtGui

from RS.Tools.ReferenceEditor import const
from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class DirectoryTextModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    def __init__(self, data, path=None, folderBool=False, parent=None):
        super(DirectoryTextModelItem, self).__init__([], parent=parent)
        self.parentItem = parent
        self._columnNumber = 1
        self._copyNumber = 1
        self._resourceFile = data
        self._resourcePath = path
        self._isFolder = folderBool
        self.childItems = []
        self._proxyBool = 'False'
        self._itemIcon = None

        if self._isFolder:
            self._itemIcon = const.Icons.Folder
        else:
            self._itemIcon = const.Icons.File

    def appendChild(self, item):
        """
        add children to node
        """
        self.childItems.append(item)

    def child(self, row):
        """
        get child item at index row
        """
        return self.childItems[row]

    def childCount(self):
        """
        Get number of children
        """
        return len(self.childItems)

    def getName(self):
        return self._resourceFile

    def getPath(self):
        return self._resourcePath

    def getReferenceNumber(self):
        return self._copyNumber

    def getProxyBool(self):
        return self._proxyBool

    def getChildByName(self, name):
        if name == self.getName():
            return self
        for child in self.childItems:
            if child.getName() == name:
                return child
        return None

    def getAncestryByPartialName(self, name):
        returnList = []

        for child in self.childItems:
            returnList.extend(child.getAncestryByPartialName(name))

        if len(returnList) > 0:
            returnList.append(self)

        if name.lower() in self.getName().lower():
            returnList.append(self)

        return returnList

    def getAllChildren(self):
        childList = [self]
        for child in self.childItems:
            childList.extend(child.getAllChildren())
        return childList

    def columnCount(self):
        return max([child.columnCount() for child in self.childItems] or [self._columnNumber])

    def findIndexByChild(self, item):
        return self.childItems.index(item)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column = index.column()

        if role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
            if column == 0:
                return self._resourceFile
            if column == 1:
                return self._copyNumber
            if column == 2:
                return self._proxyBool

        if role == QtCore.Qt.DecorationRole:
            if column == 0:
                return self._itemIcon

        elif role == QtCore.Qt.UserRole:
            return self

        elif role == QtCore.Qt.FontRole:
            if column == 0:
                newFont = QtGui.QFont()
                newFont.setPointSize(10)
                return newFont

        elif role == QtCore.Qt.ToolTipRole:
            if column == 0:
                return self._resourcePath
            if column in [1, 2]:
                return 'Double click cell to edit'

        elif role == const.DirectoryModelDataIndex.folderFileRole:
            if self._resourcePath != None:
                return True

        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        column = index.column()
        if role == QtCore.Qt.EditRole:
            if column == 1:
                self._copyNumber = value
                return True
            if column == 2:
                self._proxyBool = value
                return True

        return False

