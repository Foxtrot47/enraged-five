import datetime

from PySide import QtGui, QtCore

from RS import Perforce
from RS.Tools.UI import Application


class SetRevisionDialog(QtGui.QDialog):
    """
    QDialog that allows you to set perforce revisions on a reference

    Arguments:
        references (RS.Core.ReferenceSystem.Types): Reference objects to check revisions on
        parent (QtWidget.QWidget) : Parent widget (defaults to application main window if not specified)
    """

    def __init__(self, references, parent=Application.GetMainWindow()):

        super(SetRevisionDialog, self).__init__(parent)

        self.references = references

        # window sizes
        self.width = 500
        self.height = 100
        self.minimumWidth = 400
        self.contentMargins = (10, 10, 10, 10)

        self.title = "Set Perforce Revision(s)"
        self.headerItems = ("Reference", "User", "Time", "Perforce Revision")
        self.numHeaderItems = len(self.headerItems)
        self.updateButtonText = "Update Revision(s)"
        self.cancelButtonText = "Cancel"

        self.setupUi()

    def setupUi(self):
        """
        Sets up layout and dialog widgets
        """
        # set size
        self.resize(self.width, self.height)
        self.setMinimumWidth(self.minimumWidth)

        # set window title
        self.setWindowTitle(self.title)

        # create main layout
        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.setContentsMargins(*self.contentMargins)
        self.setLayout(self.mainLayout)

        # perforce tree widget
        self.perforceTreeWidget = QtGui.QTreeWidget()
        self.perforceTreeWidget.setFocusPolicy(QtCore.Qt.NoFocus)  # remove focus outline
        self.perforceTreeWidget.setColumnCount(len(self.headerItems))
        self.perforceTreeWidget.setHeaderLabels(self.headerItems)

        # stretch first 3 columns
        for num in xrange(self.numHeaderItems):
            self.perforceTreeWidget.header().setResizeMode(num, QtGui.QHeaderView.Stretch)

        # make last column fixed
        self.perforceTreeWidget.header().setResizeMode(self.numHeaderItems - 1, QtGui.QHeaderView.Fixed)
        self.perforceTreeWidget.header().setStretchLastSection(False)

        # add references
        self.referenceItems = []
        for reference in self.references:
            self.referenceItems.append(ReferenceItem(reference, parent=self.perforceTreeWidget))

        self.mainLayout.addWidget(self.perforceTreeWidget)

        # buttons
        self.updateButton = QtGui.QPushButton(self.updateButtonText)
        self.updateButton.clicked.connect(self.accept)

        self.cancelButton = QtGui.QPushButton(self.cancelButtonText)
        self.cancelButton.clicked.connect(self.reject)

        self.buttonLayout = QtGui.QHBoxLayout()
        self.buttonLayout.addWidget(self.updateButton)
        self.buttonLayout.addWidget(self.cancelButton)

        self.mainLayout.addLayout(self.buttonLayout)

    def getRevisionList(self):
        """
        Returns:
            list of int: a list of all the revisions that should be used for each one of the given references.
        """
        return [int(referenceItem.currentVersion()) for ind, referenceItem in enumerate(self.referenceItems)]


class ReferenceItem(QtGui.QTreeWidgetItem):
    """
    Overridden QTreeWidgetItem that is displayed in the setRevisionDialog

    Arguments:
        reference (RS.Core.ReferenceSystem.Types): Reference object to check revisions on
        parent (QtWidget.QWidget) : Parent widget
    """

    def __init__(self, reference, parent=None):

        super(ReferenceItem, self).__init__(parent)

        self.parent = parent
        self.reference = reference
        self.setupUi()

    def setupUi(self):
        """
        Updates values on current reference
        """
        self.setText(0, self.reference.Name)

        # version combo
        self.versionComboBox = PerforceRevisionComboBox(self.reference, parent=self.parent)
        self.versionComboBox.versionChanged.connect(self.updateValues)
        self.parent.setItemWidget(self, 3, self.versionComboBox)

        # update user and time values
        self.updateValues(self.reference.CurrentVersion)

    def updateValues(self, version):
        """
        Updates user and time column values on perforce revision

        Arguments:
            version (str): The string version to set
        """

        # set user
        self.setText(1, Perforce.GetFileInfo(self.reference.Path, version, "user"))

        # set time
        self.setText(2, self.formatTime(Perforce.GetFileInfo(self.reference.Path, version, "time")))

    def formatTime(self, epochTime):
        """
        Formats Unix/Epoch time to local time

        Arguments:
            epochTime (int or str): Unix/Epoch time to format

        Returns:
            str: The Unix/Epoch formatted
        """
        dt = datetime.datetime.fromtimestamp(int(epochTime))

        return "{0} {1:02d}/{2:02d}/{3:04d} {4:02d}:{5:02d}".format(
            dt.strftime("%a"), dt.month, dt.day, dt.year, dt.hour, dt.minute
        )

    def currentVersion(self):
        """
        Current version set on reference in ui

        Returns:
            str: The current version set in the version combobox
        """
        return self.versionComboBox.currentVersion


class PerforceRevisionComboBox(QtGui.QComboBox):
    """
    Overridden QCombobox that stores perforce revisions and has a popup completer for ease of use

    Arguments:
        reference (RS.Core.ReferenceSystem.Types): Reference object to check revisions on
        parent (QtWidget.QWidget) : Parent widget

    Signals:
        versionChanged (str) : A callback that triggers when the combobox changes versions
    """

    versionChanged = QtCore.Signal(str)

    def __init__(self, reference, parent=None):

        super(PerforceRevisionComboBox, self).__init__(parent)

        self.reference = reference

        # set policies and ensure you can't insert new items
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setInsertPolicy(QtGui.QComboBox.NoInsert)
        self.setEditable(True)

        # make sort filter proxy
        self._pFilterModel = QtGui.QSortFilterProxyModel(self)
        self._pFilterModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self._pFilterModel.setSourceModel(self.model())

        # add completer
        self._completer = QtGui.QCompleter(self)
        self._completer.setModel(self._pFilterModel)
        self._completer.setCompletionMode(QtGui.QCompleter.UnfilteredPopupCompletion)
        self._completer.popup().setStyleSheet("outline: 0;")
        self.setCompleter(self._completer)

        # add versions
        self.fileState = self.reference.GetFileState()
        self.versions = [str(version) for version in xrange(self.fileState.HeadRevision, 0, -1)]
        self.currentVersion = str(self.reference.CurrentVersion)
        self.addItems(self.versions)
        self.setCurrentItem(self.currentVersion)

        # connect line edit to callbacks
        lineEdit = self.lineEdit()
        lineEdit.textEdited.connect(self.textEdited)
        lineEdit.editingFinished.connect(self.editingFinished)

        # completer callback
        self._completer.activated.connect(self.onCompleterActivated)

        # index change callback
        self.currentIndexChanged.connect(self.versionChanging)

    def versionChanging(self, index):
        """
        Slot to emit version that changed

        Arguments:
            index (int): index of combobox
        """
        self.currentVersion = self.currentText()
        self.versionChanged.emit(self.versions[index])

    def editingFinished(self):
        """
        Ensure we don't leave an incorrect value in the line edit
        """
        # check that the version
        currentVersion = self.currentText()
        if currentVersion not in self.versions:
            self.setCurrentItem(self.currentVersion)
            return

        self.currentVersion = currentVersion

    def onCompleterActivated(self, text):
        """
        Set the current text if the completer is activated

        Arguments:
            text (str): text of line edit
        """
        if text:
            self.setCurrentItem(text)

    def clicked(self):
        """
        Set the menu to be editable when clicked
        """
        self.setEditable(True)

    def textEdited(self, text):
        """
        Sets string pattern to match source model when text is edited

        Arguments:
            text (str): text of line edit
        """
        self._pFilterModel.setFilterFixedString(text)

    def setCurrentItem(self, text):
        """
        Sets current item based on text

        Arguments:
            text (str): text to set current item to
        """
        index = self.findText(text)
        if index != -1:
            self.setCurrentIndex(index)
