from PySide import QtCore, QtGui

from RS.Tools import UI
from RS.Tools.VirtualProduction.widgets import gsSkelMatchWidget


class GSSkeletonWidgetDialog(QtGui.QDialog):
    '''
    Dialogue to run PropCharRootSnapWidget and retain size settings for opening and closing of the widget
    '''
    gsMergeCompletedSelected = QtCore.Signal()
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        self._settings = QtCore.QSettings("RockstarSettings", "GS Skeleton Dialog")
        super(GSSkeletonWidgetDialog, self).__init__(parent=parent)
        self.setupUi()

    def setupUi(self):
        # widget
        gsSkelWidget = gsSkelMatchWidget.GSSkeletonMatchWidgets()
        self.setWindowTitle("GS Skeleton Match")

        # create layout and add widget
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(gsSkelWidget)
        self.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)
        # set modal - so the only active window is this one, until it is closed
        self.setModal(True)

        # connect signal so the window closes after the update button has run
        gsSkelWidget.gsMergeCompletedSelected.connect(self._createMultiReference)

    def _createMultiReference(self):
        '''
        emit signal to referenceWidget
        '''
        self.gsMergeCompletedSelected.emit()
        self._handleHideDialog()

    def _handleHideDialog(self):
        '''
        hide dialogue
        '''
        self.hide()

    def showEvent(self, event):
        super(GSSkeletonWidgetDialog, self).showEvent(event)
        self.resize(self._settings.value("size", QtCore.QSize(800, 300)))
        self.move(self._settings.value("pos", QtCore.QPoint(200, 200)))

    def closeEvent(self, event):
        super(GSSkeletonWidgetDialog, self).closeEvent(event)
        self._settings.setValue("size", self.size())
        self._settings.setValue("pos", self.pos())
