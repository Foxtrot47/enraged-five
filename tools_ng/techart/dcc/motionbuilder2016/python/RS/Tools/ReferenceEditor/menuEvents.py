import os

from PySide import QtGui, QtCore

import pyfbsdk as mobu

import fbx
import FbxCommon

from RS import Config, Perforce
from RS.Core.Lights import ParseLightXml
from RS.Tools.VehicleControl import run
from RS.Tools.ReferenceEditor import const
from RS.Tools.UI.NonReferenceItemView import NonReferenceItemView
from RS.Tools.ReferenceEditor.widget import directoryWidget
from RS.Tools.ReferenceEditor.widget import editNamespaceWidget
from RS.Tools.ReferenceEditor.widget import editPathWidget
from RS.Tools.ReferenceEditor.widget import charExtensionScaleWidget
from RS.Tools.ReferenceEditor.widget import plotSourceSelectionDialog
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character
from RS.Core.ReferenceSystem.Types.CharacterHelper import CharacterHelper
from RS.Core.ReferenceSystem.Types.CharacterCutscene import CharacterCutscene
from RS.Core.ReferenceSystem.Types.CharacterIngame import CharacterIngame
from RS.Core.ReferenceSystem.Types.CharacterAnimal import CharacterAnimal
from RS.Utils.Scene import Cameras, Huds
from RS.Utils import ContextManagers, Namespace
from RS.Tools.UI import CleanExtensions
from RS.Tools.ReferenceEditor.model import assetModelItemTypes, favouritesModels
from RS.Tools.ReferenceEditor.widget import (
    gsSkeletonDialogWidget,
    favouritesWidget,
    namespaceCorrectionWidget,
    setRevisionDialog,
)
from RS.Core.ReferenceSystem.Types.MocapGsSkeleton import MocapGsSkeleton
from RS.Core.ReferenceSystem.Types.MocapReviewTrial import MocapReviewTrial


class ContextMenuBase(object):
    # Key is the identifier for the plugin (from the called classes)
    KEY = None
    ICON = None

    # key_bindings should never be touched on subclass
    KEY_BINDINGS = {}

    _WIDGET_CLASS = None

    class __metaclass__(type):
        def __init__(cls, name, bases, dict):
            if not hasattr(cls, "KEY_BINDINGS"):
                cls.KEY_BINDINGS = {}
            else:
                if cls.KEY is None:
                    return
                cls.KEY_BINDINGS[cls.KEY] = cls

    @classmethod
    def Run(cls, idx):
        raise NotImplemented()

    @classmethod
    def GetBinding(cls, key):
        return cls.KEY_BINDINGS.get(key)

    @classmethod
    def GetAllBindings(cls):
        return cls.KEY_BINDINGS.keys()

    @classmethod
    def IsCheckable(cls):
        return False

    @classmethod
    def IsChecked(cls):
        return False

    @classmethod
    def GetReferences(cls, idxList, mainModel):
        """
        Gets the references from the provided QModelIndexes and QAbstractItemModel

        Arguments:
            idxList (list): list of QModelIndex
            mainModel (RS.Tools.ReferenceEditor.model.assetModelTypes.CategoryModel): model from the tree view

        Yields:
            object
        """
        for idx in idxList:
            item = mainModel.data(idx, const.AssetModelDataIndex.ItemRole)
            reference = item.GetReference()
            if reference:
                yield reference

    @classmethod
    def Widget(cls, parent=None):
        """
        Returns the widget associated with this action

        Needs to be reimplemented in inherited methods

        Arguments:
            parent (QtGui.QWidget): parent widget

        Return:
            PySide.QWidget or None
        """
        if cls._WIDGET_CLASS is None:
            return
        return cls._WIDGET_CLASS(parent=parent)


class CreateReference(ContextMenuBase):
    KEY = const.ContextMenuType.CreateReference
    ICON = None

    _WIDGET_CLASS = gsSkeletonDialogWidget.GSSkeletonWidgetDialog

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        createRefPathFilter = createRefPathFilter or "None"
        enabled = ContextManagers.PreventUIUpdateActive.enabled()
        ContextManagers.PreventUIUpdateActive.setEnabled(True)

        # check if the createRefPathFilter is a gsSkeletonType, if so, it gets a separate popup.
        if createRefPathFilter == MocapGsSkeleton.FormattedTypeName.lower():
            # launch gs skeleton dialog
            widget = cls.Widget(parent)
            widget.exec_()
            mainModel.UpdateCategory(MocapGsSkeleton.FormattedTypeName)
            return

        # get correct path
        pathList = const.CategoryPathDict.getByType(createRefPathFilter)

        if createRefPathFilter.lower() == MocapReviewTrial.FormattedTypeName.lower():
            # This was done in place of creating a new dialog as we do for MocapGsSkeleton.
            pathList = reversed(MocapReviewTrial.TypePathList)  # DLC path would need to be first.

        dirPath = None
        if pathList:
            for path in pathList:
                if os.path.isdir(path):
                    dirPath = path
                    # This is done for GTA5 to ensure the path is gta5/art/animation/ and not gta5/art/ng/animation/
                    if Config.Project.Path.Art != Config.CoreProject.GetBranchByName("dev").Path.Art:
                        dirPath = dirPath.replace(
                            Config.Project.Path.Art, Config.CoreProject.GetBranchByName("dev").Path.Art
                        )
                    break
                    
        if dirPath is None:
            # Default to resource root if creating a reference without a known category
            dirPath = os.path.join(Config.CoreProject.GetBranchByName("dev").Path.Art, "animation", "resources")
            
        # popup for user to select new reference
        popup = QtGui.QFileDialog(parent)
        popup.setWindowTitle("Select an asset to reference into the scene:")
        popup.setViewMode(QtGui.QFileDialog.List)
        popup.setNameFilters(["*.fbx", "*.FBX", "*.wav", "*.xml", "*.XML"])
        popup.setDirectory(dirPath)
        if popup.exec_():
            if len(popup.selectedFiles()) > 0:
                filePath = popup.selectedFiles()[0]
                try:
                    str(filePath)
                except UnicodeEncodeError:
                    QtGui.QMessageBox.warning(
                        parent,
                        "Reference File Name Error",
                        "This filename uses invalid characters.\n\n" "Please rename the file and try again.",
                    )
                    return

                filePath = str(os.path.normpath(filePath))
                if not os.path.isfile(filePath):
                    QtGui.QMessageBox.warning(
                        parent,
                        "Reference File Name Error",
                        "This file does not exist on disk.\n\n"
                        "It has either been deleted from P4, or P4 thinks you have it when you don't.\n\n"
                        "Please also check your file path is correct.",
                    )
                    return
                # check if reference is coming from a gs skel path. if so, launch gs skeleton dialog
                if os.path.dirname(str(filePath).lower()) in map(str.lower, MocapGsSkeleton.TypePathList):
                    result = QtGui.QMessageBox.information(
                        parent,
                        "Reference: GS Skeleton",
                        "To bring in a gs skeleton, you need to do this via the gs skeleton tool.\n\n"
                        "Would you like to open the gs skeleton tool?",
                        QtGui.QMessageBox.Yes,
                        QtGui.QMessageBox.No,
                    )
                    if result == QtGui.QMessageBox.Yes:
                        widget = cls.Widget(parent=parent)
                        widget.exec_()
                        mainModel.UpdateCategory(MocapGsSkeleton.FormattedTypeName)
                    return

                # check and sync texture paths before bringing the reference in
                # Initialize the fbx sdk and load the fbx file into memory
                sdkManager, scene = FbxCommon.InitializeSdkObjects()
                result = FbxCommon.LoadScene(sdkManager, scene, filePath)

                # get textures int he ref scene
                textureArray = fbx.FbxTextureArray()
                # fill array with textures
                scene.FillTextureArray(textureArray)

                # loop through textures and file paths
                for i in range(0, textureArray.GetCount()):
                    texture = textureArray.GetAt(i)
                    if texture.ClassId == fbx.FbxFileTexture.ClassId:
                        textureFilename = texture.GetFileName()
                        if not os.path.exists(textureFilename):
                            # sync texture paths
                            Perforce.Sync((textureFilename), True)
                # destroy sdkManager
                sdkManager.Destroy()

                # create reference
                referenceList = manager.CreateReferences(filePath)

                # reload ui data
                mainModel.UpdateCategory(referenceList[0].FormattedTypeName.lower())
        ContextManagers.PreventUIUpdateActive.setEnabled(enabled)


class CreateMultipleReferences(ContextMenuBase):
    KEY = const.ContextMenuType.CreateMultipleReferences
    ICON = None

    _WIDGET_CLASS = directoryWidget.DirectoryDialog

    @classmethod
    def Run(cls, idxList=None, treeView=None, mainModel=None, createRefPathFilter=None, parent=None):
        widget = cls.Widget(parent=parent)
        widget.exec_()
        mainModel.updateAll()


class DuplicateReference(ContextMenuBase):
    KEY = const.ContextMenuType.DuplicateReference
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.DuplicateReferences(list(cls.GetReferences(idxList, mainModel)))
        # reload ui data
        mainModel.updateAll()


class UpdateGroup(ContextMenuBase):
    KEY = const.ContextMenuType.UpdateGroup
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for idx in idxList:
            category = idx.data(0)
            mainModel.UpdateGroup(category)
            # reload ui data
            mainModel.UpdateCategory(category, checkPerforce=True)


class StripGroup(ContextMenuBase):
    KEY = const.ContextMenuType.StripGroup
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for idx in idxList:
            category = idx.data(0)
            mainModel.StripGroup(category)


class DeleteGroup(ContextMenuBase):
    KEY = const.ContextMenuType.DeleteGroup
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        result = QtGui.QMessageBox.information(
            parent,
            "Asset Deletion",
            "Are you sure you wish to delete the asset(s) from the scene?",
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
        )
        if not result == QtGui.QMessageBox.Yes:
            return
        for idx in idxList:
            category = mainModel.data(idx, QtCore.Qt.DisplayRole)
            if category is None:
                continue
            mainModel.DeleteGroup(category)
            mainModel.UpdateCategory(category)
        # reload ui data
        mainModel.updateAll()


class DeleteCategory(ContextMenuBase):
    KEY = const.ContextMenuType.DeleteCategory
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        result = QtGui.QMessageBox.information(
            parent,
            "Asset Deletion",
            "Are you sure you wish to delete the asset(s) from the scene?",
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
        )
        if not result == QtGui.QMessageBox.Yes:
            return

        for idx in idxList:
            # get category
            category = mainModel.data(idx, QtCore.Qt.DisplayRole)
            if category is None:
                continue
            # get sub-cats
            for referenceType in manager.GetReferenceHierarchyList():
                if referenceType.FormattedTypeName == category:
                    for subCat in referenceType.SubCats:
                        mainModel.DeleteGroup(subCat.FormattedTypeName)
                        mainModel.UpdateCategory(subCat.FormattedTypeName)

                    mainModel.UpdateCategory(category)
                    break

        # reload ui data
        mainModel.updateAll()


class StripAsset(ContextMenuBase):
    KEY = const.ContextMenuType.StripAsset
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.StripReferences(list(cls.GetReferences(idxList, mainModel)))

        # reload ui data
        for idx in idxList:
            item = mainModel.data(idx, const.AssetModelDataIndex.ItemRole)
            mainModel.Update(idx, item)


class UpdateAsset(ContextMenuBase):
    KEY = const.ContextMenuType.UpdateAsset
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()

        # set versions to latest
        references = list(cls.GetReferences(idxList, mainModel))
        manager.UpdateReferences(references)

        # reload ui data
        # check if the character has a face asset attached
        for characterReference in list(cls.GetReferences(idxList, mainModel)):
            faceRef = manager.CheckTargetFaceReference(characterReference)
            if faceRef is not None:
                mainModel.UpdateCategory("Face", True)
                break

        for index in idxList:
            item = mainModel.data(index, const.AssetModelDataIndex.ItemRole)
            mainModel.Update(index, item)


class ForceUpdateAsset(ContextMenuBase):
    KEY = const.ContextMenuType.ForceUpdateAsset
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        # check if the users wishes to proceed with the force update
        result = QtGui.QMessageBox.information(
            parent,
            "Asset Force Update",
            "Are you sure you wish to force update selected asset(s)?",
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
        )
        if not result == QtGui.QMessageBox.Yes:
            return

        # set versions to latest
        references = list(cls.GetReferences(idxList, mainModel))
        for reference in references:
            reference._setCurrentVersion(reference.LatestVersion)

        manager = Manager()
        manager.UpdateReferences(references, force=True)
        # reload ui data
        # check if the character has a face asset attached
        for characterReference in list(cls.GetReferences(idxList, mainModel)):
            faceRef = manager.CheckTargetFaceReference(characterReference)
            if faceRef is not None:
                mainModel.UpdateCategory("Face", True)
                break

        for index in idxList:
            item = mainModel.data(index, const.AssetModelDataIndex.ItemRole)
            mainModel.Update(index, item)


class UpdateScene(ContextMenuBase):
    KEY = const.ContextMenuType.UpdateScene
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.UpdateReferences(manager.GetReferenceListAll())
        # reload ui data
        mainModel.updateAll()


class SaveReference(ContextMenuBase):
    KEY = const.ContextMenuType.SaveReference
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        txtPath = manager.SaveSceneReferences()
        QtGui.QMessageBox.information(
            parent, "Reference Editor", "References list saved as:\n{0}".format(txtPath), QtGui.QMessageBox.Ok
        )


class LoadReference(ContextMenuBase):
    KEY = const.ContextMenuType.LoadReference
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        # popup for user to select new reference
        popup = QtGui.QFileDialog()
        popup.setWindowTitle("Select an asset to reference into the scene:")
        popup.setViewMode(QtGui.QFileDialog.List)
        popup.setNameFilters(["*.txt"])
        defaultDirPath = mobu.FBApplication().FBXFileName
        defaultDirPath = os.path.dirname(defaultDirPath)
        # launch popup
        if os.path.isdir(defaultDirPath):
            popup.setDirectory(defaultDirPath)
        if popup.exec_():
            if len(popup.selectedFiles()) > 0:
                filePath = popup.selectedFiles()[0]
                filePath = str(os.path.normpath(filePath))
                # Open and read in txt data
                txtFile = open(filePath, "r")
                txtFileDataList = txtFile.readlines()
                referenceList = []
                for path in txtFileDataList:
                    pathSplit = path.split("\n")
                    path = pathSplit[0]
                    referenceList.append(path)
                if len(referenceList) > 0:
                    manager.CreateReferences(referenceList)
        # reload ui data
        mainModel.updateAll()


class RefreshTextures(ContextMenuBase):
    KEY = const.ContextMenuType.RefreshTextures
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.ReloadTextures(list(cls.GetReferences(idxList, mainModel)))


class SceneTextures(ContextMenuBase):
    KEY = const.ContextMenuType.SceneTextures
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.ReloadTextures(manager.GetReferenceListAll(), True)


class DeleteTextures(ContextMenuBase):
    KEY = const.ContextMenuType.DeleteTextures
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.StripTextures(list(cls.GetReferences(idxList, mainModel)))


class SwapCharacter(ContextMenuBase):
    """
    used for swapping character models, namespace will be updated to reflect new model name
    """

    KEY = const.ContextMenuType.SwapCharacter
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        # show warning if multiple assets are selected
        if len(idxList) > 1:
            QtGui.QMessageBox.warning(
                parent,
                "Warning: too many characters selected",
                "Please select one character at a time when using this option.",
                QtGui.QMessageBox.Ok,
            )
            return
        modelIndex = idxList[0]
        # get asset type
        selectedDict = mainModel.data(modelIndex, const.AssetModelDataIndex.DataDict)
        item = mainModel.data(modelIndex, const.AssetModelDataIndex.ItemRole)

        formattedTypeName = selectedDict[const.AssetModelIndex.FormattedTypeName]

        # show warning if asset type is not a character
        if not formattedTypeName in [
            Character.FormattedTypeName,
            CharacterHelper.FormattedTypeName,
            CharacterCutscene.FormattedTypeName,
            CharacterIngame.FormattedTypeName,
            CharacterAnimal.FormattedTypeName,
        ]:
            QtGui.QMessageBox.warning(
                parent,
                "Warning: A character asset type has not been selected",
                "Sorry, this option only works for characters.",
                QtGui.QMessageBox.Ok,
            )
            return

        # get ref null
        originalReference = None
        refNull = selectedDict[const.AssetModelIndex.ReferenceNull]
        if refNull is not None:
            # popup for user to select new reference to swap to
            popup = QtGui.QFileDialog()
            popup.setWindowTitle("Select an asset to reference into the scene:")
            popup.setViewMode(QtGui.QFileDialog.List)
            popup.setNameFilters(["*.fbx", "*.FBX"])
            defaultDirPath = os.path.join(Config.Project.Path.Root, "art", "animation", "resources", "characters")
            popup.setDirectory(defaultDirPath)
            newReference = None
            if popup.exec_():
                if len(popup.selectedFiles()) > 0:
                    filePath = popup.selectedFiles()[0]
                    filePath = str(os.path.normpath(filePath))
                    originalReference = manager.GetReferenceByNamespace(refNull.Name)
                    if originalReference is None:
                        QtGui.QMessageBox.warning(
                            parent,
                            "Swap Reference Error",
                            "Unable to find a matching reference.\n\nThere must be an issue with the null 'RSNull:{0}' name matching the reference name in the UI. Please fix this.\n\nIf you are uncertain how to fix this, or have further issues, contact TechArt.".format(
                                refNull.Name
                            ),
                            QtGui.QMessageBox.Ok,
                        )
                        mainModel.updateAll(forceReload=True)
                        return
                    originalReferenceType = originalReference.FormattedTypeName
                    newReference = manager.SwapReference(originalReference, filePath)
                    if newReference is None:
                        QtGui.QMessageBox.warning(
                            parent,
                            "Swap Reference Error",
                            "New Reference for path:\n\n{0}\n\n...has failed. Aborting script.".format(filePath),
                            QtGui.QMessageBox.Ok,
                        )
                        return
                    newReferenceType = newReference.FormattedTypeName
                    # reload ui data
                    mainModel.Update(modelIndex, item, referenceToPush=newReference)
                    if originalReferenceType != newReferenceType:
                        # update categories if the character ref type has changed
                        mainModel.UpdateCategory(originalReferenceType)
                        mainModel.UpdateCategory(newReferenceType)

            return newReference


class ChangeCharacter(SwapCharacter):
    """
    maintains namespace - used for swapping outfits rather than character models
    """

    KEY = const.ContextMenuType.ChangeCharacter
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        # get original namespace so we can reapply it after the swap
        originalReferenceNamespace = list(cls.GetReferences(idxList, mainModel))[0].Namespace

        # swap character first
        reference = super(ChangeCharacter, cls).Run(idxList, treeView, mainModel, createRefPathFilter, parent)

        # change the namespace back to the original namespace
        manager = Manager()
        manager.ChangeNamespace(reference, str(originalReferenceNamespace))

        # update ui
        for modelIndex in idxList:
            mainModel.Update(modelIndex, modelIndex.data(const.AssetModelDataIndex.ItemRole))


class DeleteAsset(ContextMenuBase):
    KEY = const.ContextMenuType.DeleteAsset
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        result = QtGui.QMessageBox.information(
            None,
            "Asset Deletion",
            "Are you sure you wish to delete the asset(s) from the scene?",
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.Cancel,
        )
        if not result == QtGui.QMessageBox.Yes:
            return
        manager = Manager()
        manager.DeleteReferences(list(cls.GetReferences(idxList, mainModel)))
        # reload ui data
        mainModel.updateAll()


class AddLights(ContextMenuBase):
    KEY = const.ContextMenuType.AddLights
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        ParseLightXml.Lights()


class NonReferencedItems(ContextMenuBase):
    KEY = const.ContextMenuType.NonReferencedItems
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        NonReferenceItemView.Run()


class VehicleControlToolbox(ContextMenuBase):
    KEY = const.ContextMenuType.VehicleControlToolbox
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        run.Run()


class LoadReferenceVideo(ContextMenuBase):
    KEY = const.ContextMenuType.LoadReferenceVideo
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        QtGui.QMessageBox.warning(parent, "Warning", "Apologies, function is not ready.", QtGui.QMessageBox.Ok)


class PerforceSubmitNotes(ContextMenuBase):
    KEY = const.ContextMenuType.PerforceSubmitNotes
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        QtGui.QMessageBox.warning(parent, "Warning", "Apologies, function is not ready.", QtGui.QMessageBox.Ok)


class EditNamespace(ContextMenuBase):
    KEY = const.ContextMenuType.EditNamespace
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        refNullNamespace = None
        for idx in idxList:
            selectedDict = mainModel.data(idx, const.AssetModelDataIndex.DataDict)
            refNullNamespace = selectedDict[const.AssetModelIndex.Namespace]
        if refNullNamespace:
            editNamespaceWidgetClass = editNamespaceWidget.EditNamespaceDialog(
                refNullNamespace, mainModel, treeView, parent=parent
            )
            editNamespaceWidgetClass.show()


class EditPath(ContextMenuBase):
    KEY = const.ContextMenuType.EditPath
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            editPathWidgetClass = editPathWidget.EditPathDialog(reference, mainModel, treeView, parent=parent)
            editPathWidgetClass.show()


class ShowLog(ContextMenuBase):
    KEY = const.ContextMenuType.ShowLog
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.ShowLog()


class ToggleIgnoreProject(ContextMenuBase):
    KEY = const.ContextMenuType.ToggleIgnoreProject
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.IgnoreProject = not manager.IgnoreProject

    @classmethod
    def IsCheckable(cls):
        return True

    @classmethod
    def IsChecked(cls):
        manager = Manager()
        return manager.IgnoreProject


class DisableDOF(ContextMenuBase):
    KEY = const.ContextMenuType.DisableDOF
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.DisableRotDOF()


class DeleteProblematicFacialBones(ContextMenuBase):
    KEY = const.ContextMenuType.DeleteProblematicFacialBones
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.DeleteProblematicFacialBones()


class DeleteHeadObjects(ContextMenuBase):
    KEY = const.ContextMenuType.DeleteHeadObjects
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.DeleteHeadCamLightObjects()


class ResetEnvironment(ContextMenuBase):
    KEY = const.ContextMenuType.ResetEnvironment
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.ResetEnvironment()


class ResetRollBones(ContextMenuBase):
    KEY = const.ContextMenuType.ResetRollBones
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        import RS.Core.Animation.resetRollbones


class FixOHScale(ContextMenuBase):
    KEY = const.ContextMenuType.FixOHScale
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.FixOhScaleTranslateProblem()


class FixCharacterExtensionScale(ContextMenuBase):
    KEY = const.ContextMenuType.FixCharacterExtensionScale
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        # check if the list is empty, if not, launch the separate popup
        for reference in cls.GetReferences(idxList, mainModel):
            boneList = reference.GetProblemCharacterExtensions()
            if boneList:
                charExtensionScaleWidget.CharExtensionWidgetDialog(reference)
            else:
                QtGui.QMessageBox.information(
                    parent,
                    "Extension Scale Values",
                    "All bones, present in the character extensions, "
                    "already have a scale value of (1,1,1),\n"
                    "no further action will be required.",
                )
            return


class FixHierarchyName(ContextMenuBase):
    KEY = const.ContextMenuType.FixHierarchyName
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.FixHierarchyNameIssues()


class CleanCharacterExtensions(ContextMenuBase):
    KEY = const.ContextMenuType.CleanCharacterExtensions
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        # only one reference would be passed at a time, this feature is not available for multiple
        # reference launch
        for reference in cls.GetReferences(idxList, mainModel):
            cleanExtTool = CleanExtensions.CleanExtensionsToolUI(reference)
            cleanExtTool.show()
            break


class AdjustColor(ContextMenuBase):
    KEY = const.ContextMenuType.AdjustColor
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            colorDialog = QtGui.QColorDialog()
            result = colorDialog.exec_()
            # if user selected a color, continue with the script (1 = Okay, 0 =
            # Cancel)
            if result is 1:
                # get user-selected color
                color = colorDialog.currentColor()
                if color.isValid():
                    rgbColor = color.getRgb()
                    red, green, blue = [value / 256.0 for value in rgbColor[:3]]
                    # adjust stage color
                    reference.ApplyColor(mobu.FBColor(red, green, blue))


class MakeActiveStage(ContextMenuBase):
    KEY = const.ContextMenuType.MakeActiveStage
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.MakeActive()


class SelectModel(ContextMenuBase):
    KEY = const.ContextMenuType.SelectModel
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.SelectVisualModels()


class ShowModel(ContextMenuBase):
    KEY = const.ContextMenuType.ShowModel
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.UnhideVisualModels()


class HideModel(ContextMenuBase):
    KEY = const.ContextMenuType.HideModel
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.HideVisualModels()


class ToggleP4Sync(ContextMenuBase):
    KEY = const.ContextMenuType.ToggleP4Sync
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.PerforceSync = not manager.PerforceSync

    @classmethod
    def IsCheckable(cls):
        return True

    @classmethod
    def IsChecked(cls):
        manager = Manager()
        return manager.PerforceSync


class DuplicateTakesWithAudio(ContextMenuBase):
    KEY = const.ContextMenuType.DuplicateTakesWithAudio
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        from RS.Tools.DuplicateTake import run

        run.Run()


class SwitchOffExpressions(ContextMenuBase):
    KEY = const.ContextMenuType.SwitchOffExpressions
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.toggleExpressions(False)


class SwitchOnExpressions(ContextMenuBase):
    KEY = const.ContextMenuType.SwitchOnExpressions
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.toggleExpressions(True)


class OnlyShowGeometry(ContextMenuBase):
    KEY = const.ContextMenuType.OnlyShowGeometry
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.HideAllNonGeoGroups()


class ConvertExprDeviceToConst(ContextMenuBase):
    KEY = const.ContextMenuType.ConvertExprDeviceToConst
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.ConvertExprDeviceToConst()


class ClearNanKeys(ContextMenuBase):
    KEY = const.ContextMenuType.ClearNanKeys
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        result = QtGui.QMessageBox.information(
            parent,
            "Clear 'Not a Number' Keys",
            "This function will Save and Reload this file when it completes.\n\nDo you wish to proceed?",
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
        )
        if not result == QtGui.QMessageBox.Yes:
            return

        for idx in xrange(len(idxList)):
            # we don't want the file to save and reopen until it has done the last entry in the list
            saveBool = False
            if idxList[idx] == idxList[-1]:
                saveBool = True
            item = mainModel.data(idxList[idx], const.AssetModelDataIndex.ItemRole)
            reference = item.GetReference()
            if reference:
                reference.ClearNanKeys(saveFile=saveBool)


class FixZeroScaleNulls(ContextMenuBase):
    KEY = const.ContextMenuType.FixZeroScaleNulls
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.FixZeroScaleNulls()


class PlotToSkeletonCurrentTake(ContextMenuBase):
    KEY = const.ContextMenuType.PlotToSkeletonCurrentTake
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.PlotToSkeletonCurrentTake()


class PlotToControlRigCurrentTake(ContextMenuBase):
    KEY = const.ContextMenuType.PlotToControlRigCurrentTake
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.PlotToControlRigCurrentTake()


class PlotToSkeletonAllTakes(ContextMenuBase):
    KEY = const.ContextMenuType.PlotToSkeletonAllTakes
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.PlotToSkeletonAllTakes()


class PlotToControlRigAllTakes(ContextMenuBase):
    KEY = const.ContextMenuType.PlotToControlRigAllTakes
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.PlotToControlRigAllTakes()


class PlotReferenceCurrentTake(ContextMenuBase):
    KEY = const.ContextMenuType.PlotReferenceCurrentTake
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.PlotReferenceCurrentTake()


class PlotReferenceAllTakes(ContextMenuBase):
    KEY = const.ContextMenuType.PlotReferenceAllTakes
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.PlotReferenceAllTakes()


class CopyLocalAssetPath(ContextMenuBase):
    KEY = const.ContextMenuType.CopyLocalAssetPath
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        clipboardText = []
        for reference in cls.GetReferences(idxList, mainModel):
            # create text for setting to clipboard
            clipboardText.append(reference.Path)

        # copy text to clipboard
        clipboard = QtGui.QApplication.clipboard()
        clipboard.clear()
        clipboard.setText("\n".join(clipboardText))


class CopyDepotAssetPath(ContextMenuBase):
    KEY = const.ContextMenuType.CopyDepotAssetPath
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        clipboardText = []

        for reference in cls.GetReferences(idxList, mainModel):
            # get depot path
            fileState = Perforce.GetFileState(str(reference.Path))
            if not isinstance(fileState, list):
                depotPath = fileState.DepotFilename
                depotPath = depotPath.replace("%40", "@")
                clipboardText.append(str(depotPath))

        # copy to clipboard
        clipboard = QtGui.QApplication.clipboard()
        clipboard.clear()
        clipboard.setText("\n".join(clipboardText))


class AddToFavourites(ContextMenuBase):
    KEY = const.ContextMenuType.AddToFavourites
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            favsModel = favouritesModels.FavouriteReferencesModel()
            favsModel.addFavourite(str(reference.Path), autoSave=True)


class OpenFavouriteReferences(ContextMenuBase):
    KEY = const.ContextMenuType.OpenFavourites
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        favsWin = favouritesWidget.FavouriteReferencesWidget(referenceEditorModel=mainModel)
        favsWin.show()


class SceneCleanup(ContextMenuBase):
    KEY = const.ContextMenuType.SceneCleanup
    ICON = None

    _WIDGET_CLASS = namespaceCorrectionWidget.NamespaceCorrectionDialog

    @classmethod
    def Run(cls, idxList=None, treeView=None, mainModel=None, createRefPathFilter=None, parent=None):
        # Do check for broken namespaces
        if namespaceCorrectionWidget.NamespaceCorrectionWidget.GetBrokenNamespaceDict():
            dialog = cls._WIDGET_CLASS(parent=parent)
            if dialog.exec_() == 0:
                pass
        # Clean Scene
        Manager()._cleanScene()
        parent.setupBrokenNamespaceWidgets()


class ToggleSceneOptimization(ContextMenuBase):
    KEY = const.ContextMenuType.ToggleOptimzation
    ICON = None

    @classmethod
    def Run(cls, idxList=None, treeView=None, mainModel=None, createRefPathFilter=None, parent=None):
        manager = Manager()
        manager.Optimize = not manager.Optimize

    @classmethod
    def IsCheckable(cls):
        return True

    @classmethod
    def IsChecked(cls):
        manager = Manager()
        return manager.Optimize


class SetupPlotNull(ContextMenuBase):
    KEY = const.ContextMenuType.SetupPlotNull
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.SetupPlotNull()


class RefreshFpsCamConstraints(ContextMenuBase):
    KEY = const.ContextMenuType.RefreshFpsCamConstraints
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            reference.refreshFpsCameraConstraints()


class ExportIgnore(ContextMenuBase):
    KEY = const.ContextMenuType.ExportIgnore
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        for reference in cls.GetReferences(idxList, mainModel):
            manager.SetExportIgnore(reference)


class RemoveExportIgnore(ContextMenuBase):
    KEY = const.ContextMenuType.RemoveExportIgnore
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        for reference in cls.GetReferences(idxList, mainModel):

            # remove custom property
            result = manager.RemoveExportIgnore(reference)

            # if a custom property did not need to be removed, we can bypass the asset update
            if result != True:
                return

        # update reference
        manager.UpdateReferences(list(cls.GetReferences(idxList, mainModel)), force=True)

        # reload ui data
        for index in idxList:
            item = mainModel.data(index, const.AssetModelDataIndex.ItemRole)
            mainModel.Update(index, item)


class CreateSnapshot(ContextMenuBase):
    KEY = const.ContextMenuType.CreateSnapshot
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()
        cats = set()
        for reference in cls.GetReferences(idxList, mainModel):
            manager.CreateReferenceSnapshot(reference)
            cats.add(reference.FormattedTypeName.lower())
        for cat in cats:
            mainModel.UpdateCategory(cat)


class DeleteSnapshots(ContextMenuBase):
    KEY = const.ContextMenuType.DeleteSnapshots
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        manager = Manager()

        cats = set()
        assetIdxList = []
        snapshotItemList = []
        for idx in idxList:
            item = mainModel.data(idx, const.AssetModelDataIndex.ModelItemRole)
            if isinstance(item, assetModelItemTypes.AssetModelItem):
                assetIdxList.append(idx)
            elif isinstance(item, assetModelItemTypes.SnapshotAssetModelItem):
                snapshotItemList.append(item)

        for reference in cls.GetReferences(assetIdxList, mainModel):
            cats.add(reference.FormattedTypeName.lower())
            manager.DeleteReferenceSnapshots(reference)

        if len(snapshotItemList) > 0:
            snapShotDict = {}
            for snapshotItem in snapshotItemList:
                reference = snapshotItem.parent().GetReference()
                currentSnaps = snapShotDict.get(reference, [])
                currentSnaps.append(snapshotItem.modelNull())
                snapShotDict[reference] = currentSnaps

            for refItem, snaps in snapShotDict.iteritems():
                cats.add(refItem.FormattedTypeName.lower())
                manager.DeleteReferenceSnapshots(refItem, snaps)

        for cat in cats:
            mainModel.UpdateCategory(cat)


class SetP4Revision(ContextMenuBase):
    KEY = const.ContextMenuType.SetP4Revision
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):

        # grab revisions if there isn't just one
        references = []
        for reference in cls.GetReferences(idxList, mainModel):
            if not reference.LatestVersion == 1:
                references.append(reference)

        if references:
            # show revision dialog
            dialog = setRevisionDialog.SetRevisionDialog(references, parent=parent)
            result = dialog.exec_()

            # if changed, update references
            if result:
                revisionList = dialog.getRevisionList()

                for reference, revision in zip(references, revisionList):
                    reference._setCurrentVersion(revision)

                # update references
                manager = Manager()
                manager.UpdateReferences(references, force=True, updateToLatest=False)

                # reload ui data
                for index in idxList:
                    item = mainModel.data(index, const.AssetModelDataIndex.ItemRole)
                    mainModel.Update(index, item)

        else:
            QtGui.QMessageBox.warning(
                parent,
                "Needs More Revisions",
                "Selected references need more than one perforce revision to set.\n"
                "Refresh the asset browser if is not up to date.",
            )


class SetAsProducerPerspectiveHud(ContextMenuBase):
    KEY = const.ContextMenuType.SetAsProducerPerspectiveHud
    ICON = None

    @classmethod
    def Run(cls, idxList, treeView, mainModel, createRefPathFilter=None, parent=None):
        for reference in cls.GetReferences(idxList, mainModel):
            huds = reference.getHUDElements()
            if len(huds) != 1:
                QtGui.QMessageBox.warning(
                                        None,
                                        "Warning: too many HUDs in asset",
                                        "There are too many or too few HUD elements in the referenced asset",
                                        QtGui.QMessageBox.Ok
                                         )
                return
            Huds.assignHudToCamera(Cameras.getProducerPerspective(), huds[0])


class ContextMenuEventDict(object):
    # right click
    categoriesWithSubCats = [CreateReference, None, DeleteCategory]

    categories = [CreateReference, None, UpdateGroup, None, DeleteGroup]

    individual = [UpdateAsset, None, CreateSnapshot, DeleteSnapshots, None, DeleteAsset]

    characterCutscene = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        SwapCharacter,
        ChangeCharacter,
        None,
        UpdateAsset,
        StripAsset,
        ForceUpdateAsset,
        SetP4Revision,
        None,
        PlotToSkeletonCurrentTake,
        PlotToControlRigCurrentTake,
        PlotToSkeletonAllTakes,
        PlotToControlRigAllTakes,
        None,
        SetupPlotNull,
        None,
        ClearNanKeys,
        FixZeroScaleNulls,
        None,
        SwitchOffExpressions,
        SwitchOnExpressions,
        None,
        RefreshTextures,
        DeleteTextures,
        None,
        DuplicateReference,
        None,
        DeleteProblematicFacialBones,
        None,
        RefreshFpsCamConstraints,
        None,
        EditNamespace,
        FixHierarchyName,
        None,
        FixOHScale,
        FixCharacterExtensionScale,
        CleanCharacterExtensions,
        None,
        SelectModel,
        OnlyShowGeometry,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    characterIngame = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        SwapCharacter,
        ChangeCharacter,
        None,
        UpdateAsset,
        StripAsset,
        ForceUpdateAsset,
        SetP4Revision,
        None,
        PlotToSkeletonCurrentTake,
        PlotToControlRigCurrentTake,
        PlotToSkeletonAllTakes,
        PlotToControlRigAllTakes,
        None,
        SetupPlotNull,
        None,
        ClearNanKeys,
        FixZeroScaleNulls,
        None,
        SwitchOffExpressions,
        SwitchOnExpressions,
        None,
        RefreshTextures,
        DeleteTextures,
        None,
        DuplicateReference,
        None,
        DeleteProblematicFacialBones,
        None,
        RefreshFpsCamConstraints,
        None,
        EditNamespace,
        FixHierarchyName,
        None,
        FixOHScale,
        FixCharacterExtensionScale,
        CleanCharacterExtensions,
        None,
        SelectModel,
        OnlyShowGeometry,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    characterAnimal = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        SwapCharacter,
        ChangeCharacter,
        None,
        UpdateAsset,
        StripAsset,
        ForceUpdateAsset,
        SetP4Revision,
        None,
        PlotToSkeletonCurrentTake,
        PlotToControlRigCurrentTake,
        PlotToSkeletonAllTakes,
        PlotToControlRigAllTakes,
        PlotReferenceCurrentTake,
        PlotReferenceAllTakes,
        None,
        SetupPlotNull,
        None,
        ClearNanKeys,
        FixZeroScaleNulls,
        None,
        ConvertExprDeviceToConst,
        SwitchOffExpressions,
        SwitchOnExpressions,
        None,
        RefreshTextures,
        DeleteTextures,
        None,
        DuplicateReference,
        None,
        DeleteProblematicFacialBones,
        ResetRollBones,
        None,
        EditNamespace,
        FixHierarchyName,
        None,
        FixOHScale,
        FixCharacterExtensionScale,
        CleanCharacterExtensions,
        None,
        SelectModel,
        OnlyShowGeometry,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    characterHelper = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        StripAsset,
        SetP4Revision,
        None,
        RefreshTextures,
        DeleteTextures,
        None,
        DuplicateReference,
        None,
        None,
        DeleteAsset,
    ]

    face = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        ForceUpdateAsset,
        SetP4Revision,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        DeleteHeadObjects,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    ambientFace = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        ForceUpdateAsset,
        SetP4Revision,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        None,
        DeleteAsset,
    ]

    audio = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        DuplicateTakesWithAudio,
        None,
        UpdateAsset,
        ForceUpdateAsset,
        SetP4Revision,
        None,
        None,
        DeleteAsset,
    ]

    props = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        ForceUpdateAsset,
        StripAsset,
        SetP4Revision,
        None,
        RefreshTextures,
        DeleteTextures,
        None,
        PlotReferenceCurrentTake,
        PlotReferenceAllTakes,
        None,
        SetupPlotNull,
        None,
        DuplicateReference,
        None,
        ExportIgnore,
        RemoveExportIgnore,
        None,
        DisableDOF,
        None,
        EditNamespace,
        EditPath,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    sets = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        StripAsset,
        ForceUpdateAsset,
        SetP4Revision,
        None,
        DuplicateReference,
        None,
        ResetEnvironment,
        None,
        EditNamespace,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    rayfire = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        StripAsset,
        ForceUpdateAsset,
        SetP4Revision,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    vehicles = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        VehicleControlToolbox,
        None,
        UpdateAsset,
        ForceUpdateAsset,
        StripAsset,
        SetP4Revision,
        None,
        PlotReferenceCurrentTake,
        PlotReferenceAllTakes,
        None,
        SetupPlotNull,
        None,
        RefreshTextures,
        None,
        DeleteTextures,
        None,
        DuplicateReference,
        None,
        DisableDOF,
        None,
        EditNamespace,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    mocapBasePreviz = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        None,
        EditNamespace,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    mocapBlockingModel = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        EditNamespace,
        AdjustColor,
        None,
        UpdateAsset,
        SetP4Revision,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    mocapVirtualCamera = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        SetP4Revision,
        None,
        EditNamespace,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        DeleteAsset,
    ]

    mocapRefCamera = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        SetP4Revision,
        None,
        EditNamespace,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        DeleteAsset,
    ]

    mocapGsSkeleton = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        EditNamespace,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        PlotToSkeletonCurrentTake,
        PlotToControlRigCurrentTake,
        PlotToSkeletonAllTakes,
        PlotToControlRigAllTakes,
        None,
        SetupPlotNull,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    mocapReviewTrial = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        EditNamespace,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        DeleteAsset,
    ]

    mocapPropToy = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        UpdateAsset,
        SetP4Revision,
        None,
        EditNamespace,
        None,
        PlotReferenceCurrentTake,
        PlotReferenceAllTakes,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    mocapSlate = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        EditNamespace,
        None,
        UpdateAsset,
        SetP4Revision,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    mocapHUD = [CopyLocalAssetPath, CopyDepotAssetPath, None,
                  AddToFavourites, None,
                  EditNamespace, SetAsProducerPerspectiveHud, None, DeleteAsset]

    mocapStage = [
        CopyLocalAssetPath,
        CopyDepotAssetPath,
        None,
        AddToFavourites,
        None,
        EditNamespace,
        AdjustColor,
        MakeActiveStage,
        None,
        UpdateAsset,
        SetP4Revision,
        None,
        SelectModel,
        ShowModel,
        HideModel,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    mocapReferencePose = [
        CopyLocalAssetPath,
        None,
        AddToFavourites,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    mocapBuildAsset = [
        CopyLocalAssetPath,
        None,
        AddToFavourites,
        None,
        CreateSnapshot,
        DeleteSnapshots,
        None,
        DeleteAsset,
    ]

    snapshots = [DeleteSnapshots]

    # menu toolbar
    create = [CreateReference, CreateMultipleReferences, None, OpenFavouriteReferences]
    update = [UpdateScene, SceneTextures, None, PerforceSubmitNotes]
    save = [SaveReference, LoadReference]
    advanced = [
        SceneCleanup,
        None,
        AddLights,
        None,
        NonReferencedItems,
        None,
        LoadReferenceVideo,
        None,
        ToggleP4Sync,
        ToggleSceneOptimization,
        None,
        ToggleIgnoreProject,
        None,
        ShowLog,
    ]
