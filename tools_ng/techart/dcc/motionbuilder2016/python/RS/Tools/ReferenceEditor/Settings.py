"""
Updating the version number cheat sheet:
    Order - Major.Minor.Patch

        Major - Change the public API of system, where existing code might no longer function or work due to the changes introduced
        Minor - Feature Additions
        Patch - Fix an issue where the public API has not changed
"""
Version = "2.5.1"
