from PySide import QtCore, QtGui

from RS.Tools.ReferenceEditor.model import directoryTextModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class SelectedModelTypes(baseModel.BaseModel):
    def __init__(self, internalModel=None, parent=None):
        super(SelectedModelTypes, self).__init__(parent=parent)

    def setupModelData(self, parent):
        pass

    def getHeadings(self):
        return ["Selected Asset List", "#", 'Proxy']

    def columnCount(self, parent):
        return len(self.getHeadings())

    def AddSelectedPathTree(self, selectedDict):
        if len(selectedDict) == 0:
            return

        for key, value in selectedDict.iteritems():
            assetNameString = key
            pathString = value[0]
            isFile = value[1]
            idx = value[2]
            if isFile:
                childCount = self.rootItem.childCount()
                self.beginInsertRows(QtCore.QModelIndex(), childCount, childCount)
                node = directoryTextModelItem.DirectoryTextModelItem(assetNameString, pathString, parent=self.rootItem)
                self.rootItem.appendChild(node)
                self.endInsertRows()

    def RemoveSelectedPathTree(self, selectedList):
        if len(selectedList) == 0:
            return

        for idx, row in enumerate(set([idx.row() for idx in selectedList])):
            resolvedRowNumber = row - idx
            self.beginRemoveRows(QtCore.QModelIndex(), resolvedRowNumber, resolvedRowNumber)
            self.rootItem.removeChild(self.rootItem.child(resolvedRowNumber))
            self.endRemoveRows()

    def flags(self, index):
        column = index.column()
        if not column in [1, 2]:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable
