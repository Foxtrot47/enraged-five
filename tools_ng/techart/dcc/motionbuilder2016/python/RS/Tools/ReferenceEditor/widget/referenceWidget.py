from PySide import QtGui

from functools import partial

from RS import Globals

from RS.Tools.ReferenceEditor import const, menuEvents
from RS.Tools.ReferenceEditor.widget import assetTreeWidget
from RS.Tools.ReferenceEditor.model import assetModelTypes
from RS.Tools.ReferenceEditor.widget import namespaceCorrectionWidget

'''
TODO:
* use depot files to populate CreateReference Popup - make model of the depot
* issue with multi selecting items from diff categories and not giving the correct right click menu
* clear search lineedit when 'create refs' on show event - batch create
* add search button in filter - batch create
* add cancel button - batch create
* fix menu dict so we arent using fixed strings for asset name. can use [Type].Formatted type to match
'''


class ReferenceWidget(QtGui.QWidget):

    def __init__(self, parent=None):
        super(ReferenceWidget, self).__init__(parent=parent)

        self._shown = False
        self.treeRootList = []
        self.treeChildList = []
        self.projectName = None
        self._lineEditPath = None
        self._lineEditPerforce = None
        self._lineEditDate = None
        self._lineEditUser = None
        self._cleanSceneButton = None
        self.mainLayout = None

        self.setupUi()

    def showEvent(self, event):
        """
        Overrides built-in method

        Enables callbacks when the UI is shown for the first time

        Arguments:
            event (QtCore.QEvent): event invoked by the UI
        """
        if not self._shown:
            self._shown = True

            # These callbacks should only be be active during the lifespan of this widget
            Globals.Callbacks.OnFileOpenCompleted.Add(self._onFileOpenCompleted)
            Globals.Callbacks.OnFileNewCompleted.Add(self._onFileOpenCompleted)

        if assetModelTypes.CategoriesModel.NEW_SCENE:
            self.setupBrokenNamespaceWidgets()
            assetModelTypes.CategoriesModel.NEW_SCENE = False
        self.expandView()

        Globals.Callbacks.OnFileExit.Add(self.unregisterEvents)

    def closeEvent(self, event):
        """
        Overrides built-in method

        Disables callbacks when the UI is closed

        Arguments:
            event (QtCore.QEvent): event invoked by the UI
        """
        self.unregisterEvents()

    def unregisterEvents(self, control=None, event=None):
        Globals.Callbacks.OnFileOpenCompleted.Remove(self._onFileOpenCompleted)
        Globals.Callbacks.OnFileNewCompleted.Remove(self._onFileOpenCompleted)

    def _onFileOpenCompleted(self, source, event):
        """ Reload the UI when a new scene opens """
        if assetModelTypes.CategoriesModel.NEW_SCENE:
            self.setupBrokenNamespaceWidgets()
            self.expandView()
            assetModelTypes.CategoriesModel.NEW_SCENE = False

    def _setupMenuBar(self, menubar):
        menuActionList = []

        createMenu = menubar.addMenu('Create')
        updateMenu = menubar.addMenu('Update')
        saveMenu = menubar.addMenu('Save')
        advancedMenu = menubar.addMenu('Advanced')

        menuDict = {
            createMenu: menuEvents.ContextMenuEventDict.create,
            updateMenu: menuEvents.ContextMenuEventDict.update,
            saveMenu: menuEvents.ContextMenuEventDict.save,
            advancedMenu: menuEvents.ContextMenuEventDict.advanced
        }

        for menu, actionList in menuDict.iteritems():
            if actionList:
                for action in actionList:
                    if action is None:
                        menu.addSeparator()
                        continue
                    actionKey = menu.addAction(action.KEY)
                    if action.IsCheckable():
                        actionKey.setCheckable(True)
                        actionKey.setChecked(action.IsChecked())

                    menuActionList.append(actionKey)

        for menuAction in menuActionList:
            menuAction.triggered.connect(partial(self._handleMenuEvent, menuAction))

    def setupToolBar(self, banner):
        """
        Adds icon to the banner

        Arguments:
            banner (RS.Tools.UI.QBanner): banner to add tool buttons to
        """
        self._cleanSceneButton = banner.addButton(const.Icons.CleanScene, "Clean Scene",
                                                  partial(menuEvents.SceneCleanup.Run, parent=self))
        banner.addButton(const.Icons.Refresh, "Refreshes the asset list with latest information",
                         partial(self._mainModel.updateAll, checkPerforce=True))
        banner.addButton(const.Icons.CreateReference, "Add New References",
                         partial(menuEvents.CreateMultipleReferences.Run, mainModel=self._mainModel,  parent=self))
        self.setupBrokenNamespaceWidgets()

    def _handleMenuEvent(self, menuAction):
        menuString = menuEvents.ContextMenuBase.GetBinding(menuAction.text())
        if menuString:
            menuString.Run(None,
                           self.mainTreeView,
                           self._mainModel,
                           parent=self)

    def _handleAssetTreeWidgetContextMenu(self, indexList, menuString, categoryList):
        '''
        passing args to the menuEvents.py to run the menu item
        '''
        if menuString is None:
            return
        if len(categoryList) == 1:
            # category menu option selected and a filtername is to be passed
            # (if user selects multiple categories at once they get the default 'resource' filter
            menuString.Run(indexList,
                           self.mainTreeView,
                           self._mainModel,
                           str(categoryList[0]).lower(),
                           parent=self)
        else:
            # asset menu, or multiple categories selected
            menuString.Run(indexList,
                           self.mainTreeView,
                           self._mainModel,
                           parent=self)

    def _handleAssetTreeWidgetIconCommandEvent(self, index, data):
        # if double click finds a tooltip, then it has an icon and action
        toolTip = index.data(3)
        categoryString = None

        data = index.data(const.AssetModelDataIndex.CategoryString) or None
        if data:
            categoryString = data[0].FormattedTypeName
        if toolTip is None:
            return
        # link icon to action
        menuString = menuEvents.ContextMenuBase.GetBinding(toolTip)
        if menuString is None:
            return
        # run action
        menuString.Run([index],
                       self.mainTreeView,
                       self._mainModel,
                       createRefPathFilter=categoryString,
                       parent=self)

    def checkForBrokenNamespace(self):
        """
        An issue seen commonly in CC1 and randomly in CC7, where namespaces end up with an additional
        number in them. This results in the asset no longer matching the reference null, breaking the
        reference link. e.g. "Player_Zero 1"

        Returns:
        Dict: [string] = list(FBComponent)
        """
        return namespaceCorrectionWidget.NamespaceCorrectionWidget.GetBrokenNamespaceDict()

    def setupBrokenNamespaceWidgets(self):
        """
        add widgets for broken namespaces if required, remove any existing if no longer required
        """
        # extra widgets based off info found in scene
        # adding a try/except for runtime errors
        # e.g. RuntimeError: Internal C++ object (PySide.QtGui.QPushButton) already deleted.
        try:
            if self._cleanSceneButton and self.checkForBrokenNamespace():
                self._cleanSceneButton.setIcon(const.Icons.UnCleanScene)
                self._cleanSceneButton.setStyleSheet("QPushButton"
                                                     "{"
                                                     "border: solid;"
                                                     "border-width: 2px;"
                                                     "border-color: red;"
                                                     "border-radius: 3px;"
                                                     "}"
                                                     "QPushButton:hover"
                                                     "{"
                                                     "border: none;"
                                                     "background:red;"
                                                     "}")

            elif self._cleanSceneButton:
                self._cleanSceneButton.setIcon(const.Icons.CleanScene)
                self._cleanSceneButton.setStyleSheet("")

        except RuntimeError:
            return

    def correctNamespaces(self):
        """
        fixes namespaces dialog popup
        """
        dialog = namespaceCorrectionWidget.NamespaceCorrectionDialog()
        result = dialog.exec_()
        if result == 0:
            # check if there are no more namespaces left to correct - we want to remove the
            # ref editor button if so.
            self.setupBrokenNamespaceWidgets()

    def resetUI(self, force=False, expandAll=False):
        # reload ui data
        self._mainModel.updateAll(forceReload=force)
        self.setupBrokenNamespaceWidgets()
        if expandAll:
            self.expandView()

    def expandView(self):
        self.mainTreeView.expandAll()
        for idx, _ in enumerate(self._mainModel.getHeadings()):
            self.mainTreeView.resizeColumnToContents(idx)

    def setupUi(self):
        # build widgets
        self.mainLayout = QtGui.QGridLayout(self)

        # main models
        self._mainModel = assetModelTypes.CategoriesModel()
        self.mainTreeView = assetTreeWidget.AssetTreeWidget(model=self._mainModel)

        # create widgets
        menuBar = QtGui.QMenuBar()

        # If the height of the menubar is smaller than the size of the text then it collapses the menu items
        # This is an issue in 4k monitors where this menu bar in particular
        font = QtGui.QApplication.font()
        metrics = QtGui.QFontMetrics(font)
        fontHeight = metrics.height()
        menuBar.setMinimumHeight(fontHeight + 6)

        self.setStyleSheet("QMenuBar {background: #ffdc19;"
                           "font: bold;"
                           "color: black;}"
                           "QToolBar {background: #ffdc19;"
                           "font: bold;"
                           "color: black;}")

        self._setupMenuBar(menuBar)

        menuBar.setMaximumHeight(20)

        self.mainTreeView.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        font = QtGui.QFont('', 0)
        fontMetrics = QtGui.QFontMetrics(font)

        self.mainTreeView.setAlternatingRowColors(True)

        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.mainLayout.setSpacing(0)

        # add widgets
        self.mainLayout.addWidget(menuBar, 2, 0, 1, 2)
        self.mainLayout.addWidget(self.mainTreeView, 4, 0, 1, 2)

        # add actions
        self.mainTreeView.referenceMenuEvent.connect(self._handleAssetTreeWidgetContextMenu)
        self.mainTreeView.iconCommandEvent.connect(self._handleAssetTreeWidgetIconCommandEvent)

        # set layout
        self.setLayout(self.mainLayout)
