from PySide import QtCore, QtGui

import os
import re

from RS import Config, Perforce
from RS.Tools.ReferenceEditor import const
from RS.Tools.ReferenceEditor.model import directoryTextModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class FilesModel(baseModel.BaseModel):
    def __init__(self, parent=None):
        super(FilesModel, self).__init__(parent=parent)

    def _getDirectory(self, folderString, fileExtension="fbx"):
        """
        arg: String - a string with the name of a toplevel folder in the resources folder
        Get list of resource files from P4 depot view
        """
        fileList = []
        cmd = "files"

        dirPath = os.path.join(Config.CoreProject.GetBranchByName("dev").Path.Art, "animation", "resources")
        resourcePath = os.path.join(dirPath, folderString, "...{0}".format(fileExtension))
        args = ["-e", resourcePath]
        p4Files = Perforce.Run(cmd, args)
        if str("has been unloaded") in p4Files:
            None
        else:
            for record in p4Files.Records:
                pathPartsList = []
                path = record["depotFile"]
                try:
                    fileList.append(str(path))
                except UnicodeEncodeError:
                    continue
        return fileList

    def findIndexByChild(self, item):
        return self.childItems.index(item)

    def getHeadings(self):
        return ["Asset List"]

    def _createNodesFromFilePath(self, filePath, category, parentNode, fileExtension="fbx"):
        filePathSplit = filePath.lower().partition("/{0}".format(category))
        categoryPath = filePathSplit[-1]
        categoryPathSplit = categoryPath.split("/")
        categoryPathList = categoryPathSplit[1:]
        assetNameSplit = categoryPathList[-1].split(".")
        assetName = assetNameSplit[0]

        currentNode = parentNode
        for part in categoryPathList:

            childNode = currentNode.getChildByName(part)

            if childNode is None:
                if re.search(".{0}".format(fileExtension), part.lower(), re.I) is not None:
                    childNode = directoryTextModelItem.DirectoryTextModelItem(part, filePath, parent=currentNode)
                    currentNode.appendChild(childNode)
                else:
                    childNode = directoryTextModelItem.DirectoryTextModelItem(part, folderBool=True, parent=currentNode)
                    currentNode.appendChild(childNode)
            currentNode = childNode

    def getNodesByName(self, name):
        returnList = []
        for child in self.rootItem.children():
            returnList.extend(child.getAncestryByPartialName(name))
        return returnList


class AssetModel(FilesModel):
    def __init__(self, internalModel=None, parent=None):
        super(AssetModel, self).__init__(parent=parent)

    def setupModelData(self, parent):
        parents = [parent]

        # characters
        parentNode = directoryTextModelItem.DirectoryTextModelItem(
            const.DirectoryFolderString.Characters, folderBool=True, parent=parents[-1]
        )
        parents[-1].appendChild(parentNode)
        pathList = self._getDirectory(const.DirectoryFolderString.Characters)
        from RS.Tools import UI

        if parentNode:
            for path in pathList:
                if re.search("models", path, re.I) is not None:
                    self._createNodesFromFilePath(path, const.DirectoryFolderString.Characters, parentNode)

        # props
        parentNode = directoryTextModelItem.DirectoryTextModelItem(
            const.DirectoryFolderString.Props, folderBool=True, parent=parents[-1]
        )
        parents[-1].appendChild(parentNode)
        pathList = self._getDirectory(const.DirectoryFolderString.Props)
        if parentNode:
            for path in pathList:
                self._createNodesFromFilePath(path, const.DirectoryFolderString.Props, parentNode)

        # sets
        parentNode = directoryTextModelItem.DirectoryTextModelItem(
            const.DirectoryFolderString.Sets, folderBool=True, parent=parents[-1]
        )
        parents[-1].appendChild(parentNode)
        pathList = self._getDirectory(const.DirectoryFolderString.Sets)
        if parentNode:
            for path in pathList:
                self._createNodesFromFilePath(path, const.DirectoryFolderString.Sets, parentNode)

        # vehicles
        parentNode = directoryTextModelItem.DirectoryTextModelItem(
            const.DirectoryFolderString.Vehicles, folderBool=True, parent=parents[-1]
        )
        parents[-1].appendChild(parentNode)
        pathList = self._getDirectory(const.DirectoryFolderString.Vehicles)
        if parentNode:
            for path in pathList:
                self._createNodesFromFilePath(path, const.DirectoryFolderString.Vehicles, parentNode)

        # audio
        parentNode = directoryTextModelItem.DirectoryTextModelItem(
            const.DirectoryFolderString.Audio, folderBool=True, parent=parents[-1]
        )
        parents[-1].appendChild(parentNode)
        pathList = self._getDirectory(const.DirectoryFolderString.Audio, fileExtension="wav")
        if parentNode:
            for path in pathList:
                self._createNodesFromFilePath(path, const.DirectoryFolderString.Audio, parentNode, fileExtension="wav")


class AssetFilterProxyModel(QtGui.QSortFilterProxyModel):
    """"""

    def __init__(self, parent=None):
        super(AssetFilterProxyModel, self).__init__(parent=parent)
        self._filterText = ""

    def setFilterText(self, text):
        self._filterText = text
        self._filteredNodesList = self.sourceModel().getNodesByName(self._filterText)
        self.invalidate()

    def getFilterText(self):
        return self._filterText

    def filterAcceptsRow(self, row, parent=None):
        """
        Interal Method

        Main logic if a row is shown or hidden based off the properties in
        the AssetFilterProxyModel.
        """
        if self.sourceModel is None:
            return False

        if self._filterText == "":
            return True

        index = self.sourceModel().index(row, 0, parent)
        return self.sourceModel().data(index, QtCore.Qt.UserRole) in self._filteredNodesList
