from PySide import QtCore, QtGui

from RS.Tools import UI
from RS.Utils.Logging import const as loggingConst


class ProgressWindowDialog(UI.QtMainWindowBase):
    '''
    Dialogue to run ProgressWindow and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=None, safeToProcessEvents=False):
        '''
        Constructor
        '''
        super(ProgressWindowDialog, self).__init__(parent=parent,
                                                   size=(500, 65),
                                                   store=True
                                                   )

        # set dialog to center the screen
        dialogGeometry = self.geometry()
        dialogGeometry.moveCenter(QtGui.QApplication.desktop().availableGeometry().center())
        self.setGeometry(dialogGeometry)
        # remove frame and titlebar/buttons from dialog
        self.setWindowFlags(QtCore.Qt.Tool | QtCore.Qt.FramelessWindowHint)

        # create layout and add widget
        self._progressWindow = ProgressWindow(safeToProcessEvents=safeToProcessEvents)
        self.setCentralWidget(self._progressWindow)

    def AddText(self, text, textcolor=loggingConst.MessageTextColours.Default):
        self._progressWindow.AddText(text, textcolor)


class ProgressWindow(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the UI
    '''
    def __init__(self, parent=None, safeToProcessEvents=False):
        '''
        Constructor
        '''
        super(ProgressWindow, self).__init__(parent=parent)
        self._safeToProcessEvents = safeToProcessEvents
        self._setupUi()

    def _setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        layout = QtGui.QHBoxLayout()

        # create widgets
        self.console = QtGui.QTextEdit()

        # widget properties
        self.console.setReadOnly(True)
        self.console.setFrameStyle(QtGui.QFrame.NoFrame)
        self.console.setFontPointSize(10)

        # set widgets to layout
        layout.addWidget(self.console)

        # set layout
        self.setLayout(layout)

    def hide(self):
        self.console.clear()
        super(ProgressWindow, self).hide()

    def AddText(self, text, textColor):
        self.console.setTextColor(QtGui.QColor(textColor))
        self.console.setText('\n\n{0}'.format(text))

        self.repaint()
        if self._safeToProcessEvents:
            QtGui.qApp.processEvents(QtCore.QEventLoop.ExcludeUserInputEvents)
