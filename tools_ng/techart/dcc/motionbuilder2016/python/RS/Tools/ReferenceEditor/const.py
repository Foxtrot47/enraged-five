from PySide import QtCore, QtGui

import os

from RS import Config
from RS.Tools.VirtualProduction import const as vpConst


class AssetModelIndex(object):
    ReferenceNull = 0
    Path = 1
    Namespace = 2
    FormattedTypeName = 3
    HaveRevision = 4
    HeadRevision = 5
    HaveandHeadRevision = 6
    PerforceUserHaveRevision = 7
    ProxyAsset = 8


class AssetModelDataIndex(object):
    DataDict = QtCore.Qt.UserRole
    IconCommand = QtCore.Qt.UserRole + 1
    CategoryString = QtCore.Qt.UserRole + 2
    ItemRole = QtCore.Qt.UserRole + 3
    NamespaceRole = QtCore.Qt.UserRole + 4
    ModelItemRole = QtCore.Qt.UserRole + 5


class DirectoryModelDataIndex(object):
    selfReturn = QtCore.Qt.UserRole
    folderFileRole = QtCore.Qt.UserRole + 1
    spinBoxRole = QtCore.Qt.UserRole + 2


class IconCommandType(object):
    Refresh = 0
    Edit = 1
    Delete = 2


class Icons(object):
    _FOLDER = os.path.join(Config.Script.Path.ToolImages, "ReferenceEditor")

    CreateReference = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "CreateReference.png")))
    CreateMultipleReferences = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "CreateMultipleReferences.png")))
    UpdateReference = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "UpdateReference.png")))
    RefreshTextures = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "UpdateSceneTexture.png")))
    DeleteTextures = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "NoSync.png")))
    DeleteReference = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "DeleteReference.png")))
    InSync = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "InSync.png")))
    OutSync = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "OutSync.png")))
    NoSync = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "NoSync.png")))
    SaveReference = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "SaveReference.png")))
    PairingWarning = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "PairingWarning.png")))
    IsProxy = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "IsProxy.png")))
    Folder = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "Folder.png")))
    File = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "File.png")))
    Refresh = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "Refresh.png")))
    CleanScene = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "SceneClean.png")))
    UnCleanScene = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "SceneUnClean.png")))

    AssetTypeSnapshot = QtGui.QIcon(QtGui.QPixmap(os.path.join(_FOLDER, "AssetTypes_Snapshot.png")))


class ContextMenuType(object):
    CreateReference = "Create Reference"
    CreateMultipleReferences = "Create Multiple References"
    StripGroup = "Strip Group(s)"
    UpdateGroup = "Update Group(s)"
    DeleteGroup = "Delete Group(s)"
    DeleteCategory = "Delete Category (incl. sub-categories)"
    ForceUpdateGroup = "Force Update Group(s)"
    UpdateScene = "Update Scene References"
    StripAsset = "Strip Asset(s)"
    UpdateAsset = "Update Asset(s)"
    ForceUpdateAsset = "Force Update Asset(s)"
    SwapCharacter = "Swap Character / Outfit"
    ChangeCharacter = "Swap Character / Outfit - Maintain Namespace"
    RefreshTextures = "Sync Texture(s)"
    DeleteTextures = "Delete Texture(s)"
    SceneTextures = "Sync Scene Textures"
    DeleteAsset = "Delete Asset(s)"
    SaveReference = "Save Scene References"
    LoadReference = "Load Scene References"
    AddLights = "Add Cutscene Lights"
    SceneCleanup = "Scene Cleanup"
    ToggleOptimzation = "Enable Reference Optimization"
    ValidateCharacters = "Validate Character Setup"
    NonReferencedItems = "Non-Referenced Item Viewer"
    DuplicateReference = "Duplicate Reference(s)"
    VehicleControlToolbox = "Vehicle Controls"
    LoadReferenceVideo = "Load Reference Video"
    PerforceSubmitNotes = "Display P4 Submit Notes"
    EditNamespace = "Edit Namespace"
    ShowLog = "Show Log"
    ToggleIgnoreProject = "Ignore Project"
    DisableDOF = "Disable DOFs"
    FixHeel = "Fix High Heels"
    DeleteHeadObjects = "Delete headCam && headLight"
    ResetEnvironment = "Reset Environment"
    DeleteProblematicFacialBones = "Delete Problematic Facial Bones"
    ResetRollBones = "Reset Rollbones"
    FixOHScale = "Fix OH Scale"
    FixCharacterExtensionScale = "Fix Character Extension Scale"
    FixHierarchyName = "Fix Hierarchy Name"
    CleanCharacterExtensions = "Clean Extensions"
    AdjustColor = "Change Color"
    MakeActiveStage = "Make Active Stage"
    SelectModel = "Select Model"
    HideModel = "Hide Model"
    ShowModel = "Show Model"
    ToggleP4Sync = "Enable P4 Sync"
    DuplicateTakesWithAudio = "Duplicate Takes Toolbox"
    FixHandleNamespace = "Fix Handle Namespace"
    SwitchOffExpressions = "Switch Off Expressions"
    SwitchOnExpressions = "Switch On Expressions"
    OnlyShowGeometry = "Only Show Geometry"
    ConvertExprDeviceToConst = "Convert Expression Device to Constraint"
    ClearNanKeys = "Clear Nan Keys"
    FixZeroScaleNulls = "Fix Zero Scale"
    PlotWithStancePoseConversion = "Plot With Stance Pose Conversion"
    PlotToSkeletonCurrentTake = "Plot To Skeleton: Current Take"
    PlotToControlRigCurrentTake = "Plot To Control Rig: Current Take"
    PlotToSkeletonAllTakes = "Plot To Skeleton: All Takes"
    PlotToControlRigAllTakes = "Plot To Control Rig: All Takes"
    PlotReferenceCurrentTake = "Plot: Current Take"
    PlotReferenceAllTakes = "Plot: All Takes"
    CopyLocalAssetPath = "Copy Local Asset Path"
    CopyDepotAssetPath = "Copy Depot Asset Path"
    AddToFavourites = "Add To Favourites"
    OpenFavourites = "Favourites..."
    SetupPlotNull = "Resource: Set up 'Plotted Null'"
    BindPosePropertyControl = "BindPose Export Property Control"
    RefreshFpsCamConstraints = "FPS Camera: Refresh Constraints"
    EditPath = "Edit Path"
    ExportIgnore = "Tag for Export Ignore"
    RemoveExportIgnore = "Remove Export Ignore Tag"
    CreateSnapshot = "Create Snapshot"
    DeleteSnapshots = "Delete Snapshot(s)"
    SetP4Revision = "Set Perforce Revision(s)"
    SetAsProducerPerspectiveHud = "Set as Producer Perspective HUD"



class ContextMenuToolTipDict(object):
    tooltipDict = {
        ContextMenuType.BindPosePropertyControl: "Add/Remove animated helper bones on export. (Hats, PH bones, etc)",
        ContextMenuType.CleanCharacterExtensions: "Removes keys from character extensions",
        ContextMenuType.ClearNanKeys: "Removes Not a Number keys on selected references. Can help fix NaN export errors",
        ContextMenuType.ConvertExprDeviceToConst: "Updates our old expression device to a constraint",
        ContextMenuType.CopyDepotAssetPath: "Copy selected perforce depot path",
        ContextMenuType.CopyLocalAssetPath: "Copy selected local path",
        ContextMenuType.CreateReference: "Popup appears for you to select and add a new reference",
        ContextMenuType.DeleteAsset: "Completely removes all traces of the asset from the scene",
        ContextMenuType.DeleteHeadObjects: "Deletes the headCam and headLight objects from the selected character",
        ContextMenuType.DeleteProblematicFacialBones: "Deleted FaceRoot_Head_000_R and FaceRoot_Head_001_R. Please run Update Assets after",
        ContextMenuType.DeleteTextures: "Deletes the textures linked to the selected assets",
        ContextMenuType.DisableDOF: "Disables the DOFs for the movers of props, and vehicles",
        ContextMenuType.DuplicateReference: "Brings in an aditional reference of the selected assets. No animation is applied",
        ContextMenuType.DuplicateTakesWithAudio: "Opens the Duplicate Takes Toolbox",
        ContextMenuType.EditNamespace: "Change the name of an asset",
        ContextMenuType.ExportIgnore: "Text for ignore export",
        ContextMenuType.FixCharacterExtensionScale: "Sets the scale character extension sub objects to (1,1,1)",
        ContextMenuType.FixHierarchyName: "Fixes naming issues e.g. Player_One:Player_Zero",
        ContextMenuType.FixOHScale: "Fixes scale/translations - after an update or swap, from a model without Zeroing_Null",
        ContextMenuType.FixZeroScaleNulls: "Test for zero scale null",
        ContextMenuType.ForceUpdateAsset: "Updates the reference whether they're the latest Perforce version or not",
        ContextMenuType.HideModel: "Hides the FBModel objects of the selected reference",
        ContextMenuType.OnlyShowGeometry: "Only shows the Geo objects of a character",
        ContextMenuType.ResetRollBones: "Clears all animation on rollbones",
        ContextMenuType.SelectModel: "Selects the FBModel objects of the selected reference",
        ContextMenuType.ShowModel: "Shows the FBModel objects of the selected reference",
        ContextMenuType.StripAsset: "Removed the mesh, textures etc. from the scene. Can help speed the scene up",
        ContextMenuType.SwapCharacter: "Opens a dialogue allowing the FBX character / outfit to be changed",
        ContextMenuType.RefreshTextures: "Grabs and applies the latest mesh textures from Perforce",
        ContextMenuType.CreateMultipleReferences: "Popup appears for you to select and add multiple new references",
        ContextMenuType.StripGroup: "Strips geo and several 'safe' components from all assets in the group",
        ContextMenuType.PlotWithStancePoseConversion: "Plot animation from one character to another compensating for differences in stance pose",
    }


class DirectoryFolderString(object):
    Audio = "audio"
    Characters = "characters"
    Props = "props"
    Sets = "sets"
    Vehicles = "vehicles"


class DirectoryModelIndex(object):
    Name = 0
    Path = 1
    Category = 2


class CategoryPathDict(object):
    AMBIENTFACE_PATHLIST = (os.path.join(Config.Project.Path.Art, "animation", "resources", "face"),)

    AUDIO_PATHLIST = (
        os.path.join(Config.Project.Path.Art, "animation", "resources", "audio"),
        os.path.join(Config.VirtualProduction.Path.Previz, "globalAssets", "audio"),
        os.path.join(Config.Project.Path.Root, "audio", "dev", "assets", "waves"),
        os.path.join(Config.Project.Path.Root, "audio", "assets", "waves"),
        os.path.join(Config.Project.Path.Art, "animation", "ingame", "Renders"),
    )

    CHARACTER_PATHLIST = (os.path.join(Config.Project.Path.Art, "animation", "resources", "characters", "Models"),)

    CHARACTER_HELPER_PATHLIST = (
        os.path.join(Config.Project.Path.Art, "animation", "resources", "characters", "characterHelpers"),
    )

    CHARACTER_CUTSCENE_PATHLIST = (
        os.path.join(Config.Project.Path.Art, "animation", "resources", "characters", "Models", "cutscene"),
    )

    CHARACTER_INGAME_PATHLIST = (
        os.path.join(Config.Project.Path.Art, "animation", "resources", "characters", "Models", "ingame"),
    )

    CHARACTER_ANIMAL_PATHLIST = (
        os.path.join(Config.Project.Path.Art, "animation", "resources", "characters", "Models", "Animals"),
    )

    FACE_PATHLIST = (
        os.path.join(Config.Project.Path.Art, "animation", "cutscene", "facial"),
        os.path.join(Config.Project.Path.Art, "animation", "resources", "face"),
    )

    PROP_PATHLIST = (os.path.join(Config.Project.Path.Art, "animation", "resources", "props"),)

    STATEDANIMATION_PATHLIST = (os.path.join(Config.Project.Path.Art, "animation", "resources", "destruction"),)

    SET_PATHLIST = (os.path.join(Config.Project.Path.Art, "animation", "resources", "Sets"),)

    VEHICLE_PATHLIST = (os.path.join(Config.Project.Path.Art, "animation", "resources", "vehicles"),)

    MOCAP_BASEPREVIZ_PATHLIST = (
        os.path.join(Config.VirtualProduction.Path.Previz, "north", "basePreviz", "baseAssets"),
        os.path.join(Config.VirtualProduction.Path.Previz, "nyc", "basePreviz", "baseAssets"),
        os.path.join(Config.VirtualProduction.Path.Previz, "tor", "basePreviz", "baseAssets"),
        os.path.join(Config.VirtualProduction.Path.Previz, "sd", "basePreviz", "baseAssets"),
        os.path.join(Config.VirtualProduction.Path.Previz, "globalAssets", "lasers"),
    )

    MOCAP_BLOCKINGMODEL_PATHLIST = (
        os.path.join(Config.VirtualProduction.Path.Previz, "globalAssets", "blockingModels"),
    )

    MOCAP_CAMERA_PATHLIST = (
        os.path.join(Config.VirtualProduction.Path.Previz, "north", "cameras"),
        os.path.join(Config.VirtualProduction.Path.Previz, "nyc", "cameras"),
        os.path.join(Config.VirtualProduction.Path.Previz, "tor", "cameras"),
        os.path.join(Config.VirtualProduction.Path.Previz, "sd", "cameras"),
    )

    MOCAP_PROPTOY_PATHLIST = (
        os.path.join(Config.VirtualProduction.Path.Previz, "north", "propToys"),
        os.path.join(Config.VirtualProduction.Path.Previz, "nyc", "propToys"),
        os.path.join(Config.VirtualProduction.Path.Previz, "tor", "propToys"),
        os.path.join(Config.VirtualProduction.Path.Previz, "sd", "propToys"),
        os.path.join(Config.VirtualProduction.Path.Previz, "nyc", "gizmos"),
        os.path.join(Config.VirtualProduction.Path.Previz, "tor", "gizmos"),
        os.path.join(Config.VirtualProduction.Path.Previz, "sd", "gizmos"),
        os.path.join(Config.VirtualProduction.Path.Previz, "north", "gizmos"),
        os.path.join(Config.VirtualProduction.Path.Previz, "nyc", "props"),
        os.path.join(Config.VirtualProduction.Path.Previz, "tor", "props"),
        os.path.join(Config.VirtualProduction.Path.Previz, "sd", "props"),
        os.path.join(Config.VirtualProduction.Path.Previz, "north", "props"),
        os.path.join(Config.VirtualProduction.Path.Previz, "nyc", "propClones"),
        os.path.join(Config.VirtualProduction.Path.Previz, "tor", "propClones"),
        os.path.join(Config.VirtualProduction.Path.Previz, "sd", "propClones"),
        os.path.join(Config.VirtualProduction.Path.Previz, "north", "propClones"),
        os.path.join(Config.VirtualProduction.Path.Previz, "globalAssets", "propClones"),
    )

    MOCAP_SLATE_PATHLIST = (
        os.path.join(Config.VirtualProduction.Path.Previz, "north", "slate"),
        os.path.join(Config.VirtualProduction.Path.Previz, "nyc", "slate"),
        os.path.join(Config.VirtualProduction.Path.Previz, "tor", "slate"),
        os.path.join(Config.VirtualProduction.Path.Previz, "sd", "slate"),
    )

    MOCAP_STAGE_PATHLIST = (
        os.path.join(Config.VirtualProduction.Path.Previz, "north", "stages"),
        os.path.join(Config.VirtualProduction.Path.Previz, "nyc", "stages"),
        os.path.join(Config.VirtualProduction.Path.Previz, "tor", "stages"),
        os.path.join(Config.VirtualProduction.Path.Previz, "sd", "stages"),
    )

    MOCAP_REFERENCEPOSE_PATHLIST = (
        os.path.join(Config.VirtualProduction.Path.Previz, "globalAssets", "referencePoses"),
    )

    MOCAP_ESSENTIAL_PATHLIST = (
        os.path.join(Config.VirtualProduction.Path.Previz, vpConst.PrevizFolder().GetStudioFolderName()),
    )

    MOCAP_NONESSENTIAL_PATHLIST = (
        os.path.join(Config.VirtualProduction.Path.Previz, vpConst.PrevizFolder().GetStudioFolderName()),
    )

    MOCAP_BUILDASSET_PATHLIST = (os.path.join(Config.VirtualProduction.Path.Previz, "globalAssets", "preBuildAssets"),)

    RAYFIRE_PATHLIST = (os.path.join(Config.Project.Path.Root, "animation", "resources", "destruction"),)

    _pathDict = {
        "ambientface": AMBIENTFACE_PATHLIST,
        "audio": AUDIO_PATHLIST,
        "character": CHARACTER_PATHLIST,
        "character helper": CHARACTER_HELPER_PATHLIST,
        "cutscene character": CHARACTER_CUTSCENE_PATHLIST,
        "ingame character": CHARACTER_INGAME_PATHLIST,
        "animal": CHARACTER_ANIMAL_PATHLIST,
        "face": FACE_PATHLIST,
        "prop": PROP_PATHLIST,
        "rayfire": RAYFIRE_PATHLIST,
        "statedAnimation": STATEDANIMATION_PATHLIST,
        "set": SET_PATHLIST,
        "vehicle": VEHICLE_PATHLIST,
        "mocap basepreviz": MOCAP_BASEPREVIZ_PATHLIST,
        "mocap blockingmodel": MOCAP_BLOCKINGMODEL_PATHLIST,
        "mocapcamera": MOCAP_CAMERA_PATHLIST,
        "mocap proptoy": MOCAP_PROPTOY_PATHLIST,
        "mocap slate": MOCAP_SLATE_PATHLIST,
        "mocap stage": MOCAP_STAGE_PATHLIST,
        "mocap reference pose": MOCAP_REFERENCEPOSE_PATHLIST,
        "mocap essential": MOCAP_ESSENTIAL_PATHLIST,
        "mocap non essential": MOCAP_NONESSENTIAL_PATHLIST,
        "mocap build asset": MOCAP_BUILDASSET_PATHLIST,
    }

    @classmethod
    def getByType(cls, typeKey):
        return cls._pathDict.get(typeKey.lower(), None)
