import os.path
from PySide import QtCore, QtGui
from RS import Config
from RS.Core.ReferenceSystem import Manager
from RS.Tools import UI
from RS.Tools.ReferenceEditor.model import favouritesModels


class FavouriteReferencesWidget(UI.QtMainWindowBase):
    '''Main window for editing/viewing favourite references.'''
    
    Up_Arrow_Path = os.path.join(Config.Script.Path.ToolImages,
                                 'upArrow.png').replace('\\', '/')
    Down_Arrow_Path = os.path.join(Config.Script.Path.ToolImages,
                                   'downArrow.png').replace('\\', '/')
    Tree_View_Style = """
    QTreeView::item {{
        border: 0px;  padding: 0 3px;
    }}
    
    QTreeView::item:selected {{
        background-color: #678db2;
        color: white;
    }}
    
    QHeaderView::down-arrow {{
        image: url({0});
    }}
    
    QHeaderView::up-arrow {{
        image: url({1});
    }}
    """.format(Up_Arrow_Path, Down_Arrow_Path)
    
    def __init__(self, referenceEditorModel=None, parent=None):
        windowTitle = favouritesModels.FavouriteReferencesModel.Tool_Title
        super(FavouriteReferencesWidget, self).__init__(parent=parent,
                                                        title=windowTitle,
                                                        size=(400, 300),
                                                        store=True,
                                                        dockable=False
                                                        )
        # Keep a reference to the Reference Editor's model
        self._refEditorModel = referenceEditorModel
        
        # Main favourites model
        self._favouritesModel = favouritesModels.FavouriteReferencesModel()
        
        # Proxy model for sorting
        self._proxyModel = QtGui.QSortFilterProxyModel()
        self._proxyModel.setDynamicSortFilter(True)
        self._proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self._proxyModel.setSourceModel(self._favouritesModel)
        
        self._setupUi()
    
    def _setupUi(self):
        '''Constructs and sets up the widgets for this window.'''
        self._mainWidget = QtGui.QWidget()
        self.setCentralWidget(self._mainWidget)
        
        # Create layouts
        
        self._mainLayout = QtGui.QVBoxLayout()
        topRowLayout = QtGui.QHBoxLayout()
        
        # Create widgets
        
        self._favsView = QtGui.QTreeView()
        self._addReferencesBtn = QtGui.QPushButton('Add Selected References To Scene')
        addFavButton = QtGui.QPushButton()
        
        # Configure widgets
        
        addIconPath = os.path.join(Config.Script.Path.ToolImages,
                                   'ReferenceEditor',
                                   'CreateReference.png')
        addFavPixMap = QtGui.QPixmap(addIconPath.replace('\\', '/'))
        addFavIcon = QtGui.QIcon(addFavPixMap)
        addFavButton.setIcon(addFavIcon)
        addFavButton.setIconSize(QtCore.QSize(16, 16))
        addFavButton.setToolTip('Add a favourite reference')
        
        self._favsView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self._favsView.setSortingEnabled(True)
        self._favsView.setModel(self._proxyModel)
        self._favsView.sortByColumn(0, QtCore.Qt.SortOrder.AscendingOrder)
        
        self._favsView.header().setResizeMode(0, QtGui.QHeaderView.ResizeToContents)
        self._favsView.header().setResizeMode(1, QtGui.QHeaderView.ResizeToContents)
        self._favsView.setStyleSheet(self.Tree_View_Style)
        self._favsView.setSelectionMode(self._favsView.ExtendedSelection)
        
        # Layout
        
        topRowLayout.addWidget(addFavButton)
        topRowLayout.addStretch(1)
        self._mainLayout.addLayout(topRowLayout)
        self._mainLayout.addWidget(self._favsView)
        self._mainLayout.addWidget(self._addReferencesBtn)
        self._mainWidget.setLayout(self._mainLayout)
        
        # Connections
        
        addFavButton.clicked.connect(self._browseToAddFavourite)
        self._favsView.customContextMenuRequested.connect(self._onTreeViewContextMenu)
        self._addReferencesBtn.clicked.connect(self._createReferences)
    
    def _itemsAreSelected(self):
        '''
        Check if any items are selected in the favourites list.
        
        Return:
            True if items are selected or False if nothing is selected.
        '''
        items = self._favsView.selectedIndexes()
        if len(items) > 0:
            return True
        return False
    
    def _onTreeViewContextMenu(self, point):
        '''
        Constructs and displays the right click menu for the tree view.
        
        Parameters:
            point (QtCore.QPoint): The position in local widget coordinates that the menu was
                                   requested at.
        '''
        menu = QtGui.QMenu()
        if self._itemsAreSelected():
            deleteFavouritesAction = menu.addAction('Delete selected favourites')
            deleteFavouritesAction.triggered.connect(self._deleteSelectedFavourites)
        else:
            addFavouriteAction = menu.addAction('Add favourite...')
            addFavouriteAction.triggered.connect(self._browseToAddFavourite)
        menu.exec_(self._favsView.viewport().mapToGlobal(point))
    
    def _getSelectedSourceIndexes(self):
        '''
        Gets QIndexes for the items selected in the favourites list (converted to
        source model indexes instead of proxy model indexes from the SortFilterModel).
        
        Return:
            (list(QtCore.QModelIndex)): A list of the QModelIndexes from the source model which
                                        are currently selected in the tree view.
        '''
        selectedIndexes = self._favsView.selectedIndexes()
        return [self._proxyModel.mapToSource(idx) for idx in selectedIndexes]
    
    def _getSelectedItems(self):
        '''
        Pulls the underlying items from the selected QIndexes and returns them.
        
        Return:
            (list(FavouriteReferenceItem)): A list of the selected items.
        '''
        selectedIndexes = self._getSelectedSourceIndexes()
        return list(set([x.internalPointer() for x in selectedIndexes if x.isValid()]))
    
    def _createReferences(self):
        '''
        Gets the items selected in the favourites list and adds them to the scene as references.
        '''
        selectedItems = self._getSelectedItems()
        assetPaths = [item.getReferenceFilePath() for item in selectedItems]
        if not assetPaths:
            return
        manager = Manager.Manager()
        referenceList = manager.CreateReferences(assetPaths)
        manager.ReloadTextures(referenceList)
        
        if self._refEditorModel is not None:
            self._refEditorModel.updateAll()
    
    def _deleteSelectedFavourites(self):
        '''
        Deletes any items that are selected in the favourites list.
        '''
        selectedIndexes = self._getSelectedSourceIndexes()
        if not selectedIndexes:
            return
        affectedRows = []
        filteredIndexes = []
        # We're removing rows so filter off any additional indexes from the same row
        for idx in selectedIndexes:
            if idx.row() in affectedRows:
                continue
            filteredIndexes.append(idx)
            affectedRows.append(idx.row())
        # Sort by row and then reverse so we can remove them one by one without
        # indexes getting out of sync
        filteredIndexes.sort(key=lambda x: x.row(), reverse=True)
        for idx in filteredIndexes:
            self._favouritesModel.removeFavourite(idx)
    
    def _browseToAddFavourite(self):
        '''
        Opens a file dialogue for the user to choose a file to add to the favourite references list.
        '''
        resourcesDir = os.path.join(Config.Project.Path.Root, "art", "animation", "resources")
        popup = QtGui.QFileDialog()
        popup.setWindowTitle('Select an asset to add as a favourite')
        popup.setViewMode(QtGui.QFileDialog.List)
        popup.setNameFilters(["*.fbx", "*.FBX", "*.wav"])
        popup.setDirectory(resourcesDir)
        if popup.exec_():
            if len(popup.selectedFiles()) > 0:
                filePath = popup.selectedFiles()[0]
                self._favouritesModel.addFavourite(os.path.normpath(filePath))
    
    def closeEvent(self, event):
        '''
        Override to ensure favourites are saved to settings when the window is closed.
        '''
        self._favouritesModel.saveFavourites()
        
        super(FavouriteReferencesWidget, self).closeEvent(event)
