from RS.Tools import UI
from RS.Tools.ReferenceEditor import Settings
from RS.Tools.ReferenceEditor.widget import referenceWidget


@UI.Run(
    title="Reference Editor v{0}".format(Settings.Version),
    size=(450, 1000),
    dockable=False,
    url="https://hub.rockstargames.com/display/ANIM/Motionbuilder+Reference+Editor+UI",
)
def Run():
    return referenceWidget.ReferenceWidget()
