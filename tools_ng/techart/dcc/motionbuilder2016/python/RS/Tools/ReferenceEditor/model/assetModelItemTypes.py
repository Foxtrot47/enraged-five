import types

from PySide import QtCore, QtGui

from RS import Perforce
from RS.Tools.ReferenceEditor import const
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types.MocapGsSkeleton import MocapGsSkeleton
from RS.Core.ReferenceSystem.Types.MocapReviewTrial import MocapReviewTrial
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModelItem, textModelItem
from RS.Tools.UI.MovingCutscene.Model.Components import MovingCutscene


class AssetTextModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """

    def __init__(self, data, parent=None):
        super(AssetTextModelItem, self).__init__([], parent=parent)
        self.itemData = data
        self.item = data
        self.hasSubCategories = False

    def columnCount(self):
        return max([child.columnCount() for child in self.childItems] or [0])

    def findIndexByChild(self, item):
        return self.childItems.index(item)

    def childCount(self):
        return len(self.childItems)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column = index.column()
        if role == QtCore.Qt.DisplayRole:
            try:
                return self.itemData[column].FormattedTypeName
            except IndexError:
                return None

        elif role == QtCore.Qt.FontRole:
            if column == 0:
                newFont = QtGui.QFont()
                newFont.setBold(True)
                newFont.setPointSize(8)
                return newFont

        elif role == QtCore.Qt.DecorationRole:
            if column == 2:
                return const.Icons.CreateReference

        elif role == QtCore.Qt.ToolTipRole:
            if column == 2:
                return const.ContextMenuType.CreateReference

        elif role == const.AssetModelDataIndex.CategoryString:
            return self.itemData

        elif role == const.AssetModelDataIndex.ItemRole:
            try:
                return self.item[column]
            except IndexError:
                return None
        elif role == const.AssetModelDataIndex.ModelItemRole:
            return self
        else:
            return None

    def appendChild(self, item, index=-1):
        super(AssetTextModelItem, self).appendChild(item, index)
        if isinstance(item, AssetTextModelItem):
            self.hasSubCategories = True

    def __iter__(self):
        for child in self.childItems:
            yield child


class SnapshotAssetModelItem(textModelItem.TextModelItem):
    """
    Snapshot Asset Item for the model
    """

    def __init__(self, modelNull, parent=None):
        super(SnapshotAssetModelItem, self).__init__([], parent=parent)
        self._modelNull = modelNull
        self.text = self._modelNull.PropertyList.Find("RefName").Data

    def columnCount(self):
        return max([child.columnCount() for child in self.childItems] or [0])

    def findIndexByChild(self, item):
        return self.childItems.index(item)

    def childCount(self):
        return len(self.childItems)

    def modelNull(self):
        return self._modelNull

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column = index.column()
        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self.text
        elif role == QtCore.Qt.DecorationRole:
            if column == 0:
                return const.Icons.AssetTypeSnapshot
        elif role == const.AssetModelDataIndex.ItemRole:
            return self._modelNull
        elif role == const.AssetModelDataIndex.ModelItemRole:
            return self
        return None


class AssetModelItem(baseModelItem.BaseModelItem):
    """
    Asset Item which holds the display values for any asset in the scene
    """

    Manager = Manager.Manager()

    def __init__(
            self,
            reference,
            path=None,
            namespace=None,
            assetType=None,
            havePerforceVersion=None,
            headPerforceVersion=None,
            haveAndHeadPerforceVersion=None,
            perforceUserHaveRevision=None,
            proxyAsset=None,
            parent=None,
    ):
        super(AssetModelItem, self).__init__(parent=parent)
        self._columnNumber = 6
        self._reference = reference
        self._referenceNull = reference._modelNull
        self._path = path or ""
        self._namespace = namespace or ""
        self._assetType = assetType or ""
        self._havePerforceVersion = havePerforceVersion or 0
        self._headPerforceVersion = headPerforceVersion or 0
        self._haveAndHeadPerforceVersion = haveAndHeadPerforceVersion or "0/0"
        self._perforceUserHaveRevision = perforceUserHaveRevision or "Unknown"
        self._proxyAsset = proxyAsset
        self._versionIcon = None
        self._updateP4UserRevision = False
        self._isMovingCutScene = False
        self._cutSceneNamespace = ""
        self.childItems = []

    def setIconType(self, filestate=None):
        # check state of reference in perforce and assign appropriate icon
        if self._proxyAsset:
            # proxy reference
            self._versionIcon = const.Icons.IsProxy

        elif self._assetType in (MocapGsSkeleton.FormattedTypeName, MocapReviewTrial.FormattedTypeName):
            self._versionIcon = const.Icons.InSync

        # Actions 3 and 6 represent delete and move & delete on P4
        elif self._headPerforceVersion is None or filestate is None or (filestate and filestate.HeadAction in (3, 6)):
            # reference not in p4 (possibly deleted)
            self._versionIcon = const.Icons.NoSync

        elif self._headPerforceVersion is not None:
            if self._havePerforceVersion == self._headPerforceVersion:
                # reference latest version
                self._versionIcon = const.Icons.InSync

            elif self._headPerforceVersion is not None and self._havePerforceVersion < self._headPerforceVersion:
                # reference not latest version
                self._versionIcon = const.Icons.OutSync

    def __dict__(self):
        return {
            const.AssetModelIndex.ReferenceNull: self._referenceNull,
            const.AssetModelIndex.Path: self._path,
            const.AssetModelIndex.Namespace: self._namespace,
            const.AssetModelIndex.FormattedTypeName: self._assetType,
            const.AssetModelIndex.HaveRevision: self._havePerforceVersion,
            const.AssetModelIndex.HeadRevision: self._headPerforceVersion,
            const.AssetModelIndex.HaveandHeadRevision: self._haveAndHeadPerforceVersion,
            const.AssetModelIndex.PerforceUserHaveRevision: self._perforceUserHaveRevision,
        }

    def GetReference(self):
        return self._reference

    def GetReferenceNull(self):
        return self._referenceNull

    def GetNodeDict(self):
        return self.__dict__()

    def getUniqueSelectedIndexes(self, widget):
        return set(list(self.selectedIndexes()))

    def appendChild(self, item):
        self.childItems.append(item)

    def child(self, row):
        return self.childItems[row]

    def childCount(self):
        return len(self.childItems)

    def columnCount(self):
        return self._columnNumber

    def assetHasToolTip(self):
        return self.data(3)

    def namespaceInCutScene(self, cutScene):
        """
        Checks recursively for a namespace on a cutscene and all descendants

        Arguments:
            cutScene (MovingCutScene): The cutscene object to iterate over

        Returns:
            MovingCutScene: MovingCutScene or NoneType based if the namespace matches
        """
        if self._namespace == cutScene.name():
            return cutScene

        else:
            for child in cutScene.children():
                childCutscene = self.namespaceInCutScene(child)
                if childCutscene:
                    return childCutscene

        return None

    def isMovingCutScene(self):
        """
        Checks if item is moving cut scene object

        Returns:
            bool: True or False if found to be a cutscene object or apart of one
        """
        for movingCutscene in MovingCutscene.all():

            # if cutscene has the same namespace
            if self._namespace == movingCutscene.driverNamespace():
                self._cutSceneNamespace = movingCutscene.name()
                return True

            # recursively look
            foundCutscene = self.namespaceInCutScene(movingCutscene)
            if foundCutscene:
                self._cutSceneNamespace = movingCutscene.name()
                return True

        self._cutSceneNamespace = ""
        return False

    def HasChanged(self, checkPerforce=False):
        """ Has any of the cached data changed in respect to the reference object it stores """
        # path
        changed = self._path != self._reference.Path
        # namespace
        changed = changed or self._namespace != self._reference.Namespace
        # local revision
        changed = changed or self._havePerforceVersion != self._reference.CurrentVersion
        # moving cutscene
        changed = changed or self._isMovingCutScene != self.isMovingCutScene()
        # head revision
        # set LatestVersion to None first to force the property decorator to check p4 for latest
        # version
        if checkPerforce is True:
            self._reference.SetLatestVersionToNone()
        changed = changed or self._headPerforceVersion != self._reference.LatestVersion
        return changed

    def Update(self, reference=None, checkPerforce=True, filestate=None):
        """
        Updates the contents of the item

        Does not update the

        Arguments:
            reference (RS.Core.ReferenceSytem.Model.Base): reference system model that contains information about
                                        the reference.
            checkPerforce (bool): check perforce for the latest revision information
            filestate (RS.Perforce.Filestate): Filestate object to get revision information from
        """
        self._reference = reference or self._reference
        self._referenceNull = self._reference.ModelNull

        # proxy
        self._proxyAsset = None
        proxyTypes = ["Character", "Prop", "Vehicle"]
        if reference.__class__.__name__ in proxyTypes:
            self._proxyAsset = self._reference.IsProxy

        # path
        self._path = self._reference.Path
        # namespace
        self._namespace = self._reference.Namespace
        # type
        self._assetType = self._reference.FormattedTypeName
        # moving cutscene
        self._isMovingCutScene = self.isMovingCutScene()
        # revision
        self._havePerforceVersion = int(self._reference.CurrentVersion)

        if int(self._havePerforceVersion) < 0:
            self._havePerforceVersion = 0

        self._headPerforceVersion = self._headPerforceVersion or 0
        self._perforceUserHaveRevision = self._perforceUserHaveRevision or "None"

        if checkPerforce:
            if filestate is None:
                filestate = Perforce.GetFileState(self._path)
            if not isinstance(filestate, (list, types.NoneType)):
                self._headPerforceVersion = filestate.HeadRevision

        self._haveAndHeadPerforceVersion = self._haveAndHeadPerforceVersion or "proxy"
        self._updateP4UserRevision = True

        if not self._proxyAsset:
            self._haveAndHeadPerforceVersion = "{0}/{1}".format(self._havePerforceVersion, self._headPerforceVersion)

        self.setIconType(filestate=filestate)

    def GetUserForHaveRevision(self):
        """ Gets the user that submitted the have revision of this file """
        if self._updateP4UserRevision:
            recordSet = Perforce.Changes(["{}#{}".format(self._path, self._havePerforceVersion)])
            # perforce returns the latest revisions first
            records = [record for record in recordSet]
            records.reverse()
            if records and len(records) >= self._havePerforceVersion:
                self._perforceUserHaveRevision = records[self._havePerforceVersion - 1]["user"]
            self._updateP4UserRevision = False
        return self._perforceUserHaveRevision

    def GetHeadChangelistDescription(self):
        """
        Returns:
        String (description from head revision)
        """
        if self._havePerforceVersion == self._headPerforceVersion:
            return str("{0}'s reference is latest".format(self._namespace))

        # get reference path
        fileState = Perforce.GetFileState(self._path)

        if isinstance(fileState, list):
            return ""

        # get head description
        description = None
        descDict = Perforce.GetDescription(fileState.HeadChange)
        if "desc" in descDict:
            description = descDict["desc"]

        if description is None:
            return "Error with attempting to retrieve head revision description."

        referenceEditorDescription = "{0}'s head revision (revision#{1}) description:\n\n{2}".format(
            self._namespace, self._headPerforceVersion, description
        )
        return referenceEditorDescription

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._namespace
            elif column == 1:
                return self._haveAndHeadPerforceVersion
            elif column == 5:
                return self._cutSceneNamespace
            if column == 6:
                return self._path

        elif role == QtCore.Qt.EditRole:
            if column == 0:
                return self._namespace

        elif role == const.AssetModelDataIndex.DataDict:
            return self.__dict__()

        elif role == const.AssetModelDataIndex.ItemRole:
            return self

        elif role == const.AssetModelDataIndex.NamespaceRole:
            return self._namespace

        elif role == QtCore.Qt.DecorationRole:
            if column == 0:
                return self._versionIcon
            if column == 3:
                return const.Icons.UpdateReference
            if column == 4:
                return const.Icons.DeleteReference

        elif role == QtCore.Qt.ToolTipRole:
            if column == 0:
                return "Right Click to display menu"
            if column == 1:
                return self.GetHeadChangelistDescription()
            if column == 3:
                return const.ContextMenuType.UpdateAsset
            if column == 4:
                return const.ContextMenuType.DeleteAsset
            if column == 6:
                return "Have Perforce Revision ({0}) Last Updated By: {1}".format(
                    self._haveAndHeadPerforceVersion, self.GetUserForHaveRevision()
                )

        elif role == QtCore.Qt.TextAlignmentRole:
            if column in [1, 5]:
                return QtCore.Qt.AlignCenter
        elif role == const.AssetModelDataIndex.ModelItemRole:
            return self
        else:
            return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):

        if role == QtCore.Qt.EditRole:
            if index.column() == 0:
                value = str(value)
                if self._reference.Namespace != value:
                    namespace = self.Manager.ChangeNamespace(self._reference, value)
                    self._namespace = namespace
                    self.dataChanged.emit(index, index)
            return True

        return None

    def flags(self, index):
        """
        Overrides built-in method.

        Sets the modelItem to be editable

        Arguments:
            index (QtCore.QModelIndex): model index that represent an item in the UI

        Return:
            QtCore.QFlags
        """
        column = index.column()
        if column == 0:
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

        return super(AssetModelItem, self).flags(index)
