from PySide import QtGui
from RS.Tools.UI import Application
from RS import Globals


class CharacterSelectionDialog(QtGui.QDialog):
    def __init__(self, title, message, targetChar=None, parent=None):
        if parent is None:
            parent = Application.GetMainWindow()
        super(CharacterSelectionDialog, self).__init__(parent)

        self._title = title
        self._message = message
        self._targetChar = targetChar
        self._charSelectionCbbx = None
        
        self._charDict = {char.LongName: char for char in Globals.Characters if char is not self._targetChar}
        
        self.setupUi()
    
    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(10, 10, 10, 10)
        mainLayout.setSpacing(10)
        self.setLayout(mainLayout)
        
        self.setWindowTitle(self._title)
        
        messageLabel = QtGui.QLabel(self._message)
        mainLayout.addWidget(messageLabel)
        
        sourceLabel = QtGui.QLabel("Source")
        self._charSelectionCbbx = QtGui.QComboBox()
        self._charSelectionCbbx.addItems(self._charDict.keys())
        
        selectionLayout = QtGui.QHBoxLayout()
        selectionLayout.addWidget(sourceLabel)
        selectionLayout.addWidget(self._charSelectionCbbx)
        selectionLayout.setStretch(1, 1)
        mainLayout.addLayout(selectionLayout)

        if self._targetChar is not None:
            targetCharLabel = QtGui.QLabel("Target character is: {}".format(self._targetChar.LongName))
            mainLayout.addWidget(targetCharLabel)
        
        okBtn = QtGui.QPushButton("Ok")
        okBtn.clicked.connect(self.accept)
        cancelBtn = QtGui.QPushButton("Cancel")
        cancelBtn.clicked.connect(self.reject)

        bottomRowLayout = QtGui.QHBoxLayout()
        bottomRowLayout.addWidget(okBtn)
        bottomRowLayout.addWidget(cancelBtn)
        mainLayout.addLayout(bottomRowLayout)
    
    def getSelectedCharacter(self):
        selectedCharacterName = self._charSelectionCbbx.currentText()
        return self._charDict.get(selectedCharacterName, None)

    @classmethod
    def run(cls, title, message, targetChar=None):
        inst = cls(title, message, targetChar=targetChar)
        result = inst.exec_()
        if result != QtGui.QDialog.Accepted:
            return None

        return inst.getSelectedCharacter()
