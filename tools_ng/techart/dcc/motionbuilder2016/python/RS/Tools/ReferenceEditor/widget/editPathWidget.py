from PySide import QtGui, QtCore

import os

from RS import Perforce
from RS.Tools import UI
from RS.Core.ReferenceSystem.Manager import Manager


class EditPathDialog(QtGui.QDialog):
    '''
    Dialog for editpath window.
    Required so it is parented to Mobu and one only instance is available at a time.
    '''
    def __init__(self, reference, mainModel, treeView, parent=None):
        super(EditPathDialog, self).__init__(parent=parent)
        self._settings = QtCore.QSettings( "RockstarSettings", "EditPathDialog" )
        editPathMainWidget = EditPathMainWidget(reference, mainModel, treeView)
        # create layout and add widget
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(editPathMainWidget)
        self.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)
        # set modal - so the only active window is this one, until it is closed
        self.setModal(True)
        # connect signal so the window closes after the update button has run
        editPathMainWidget.updateEvent.connect(self.close)

    def showEvent(self, event):
        super(EditPathDialog, self).showEvent( event )
        self.resize(self._settings.value("size", QtCore.QSize(400, 150)))
        self.move(self._settings.value("pose", QtCore.QPoint(200, 200)))

    def closeEvent(self, event):
        try:
            super(EditPathDialog, self).closeEvent( event )
            self._settings.setValue("size", self.size())
            self._settings.setValue("pos", self.pos())
        except:
            pass


class EditPathMainWidget(QtGui.QWidget):
    '''
    Main window, with banner, for editpath popup dialog
    '''
    # signal to notify when the editpath update button has ran
    updateEvent = QtCore.Signal()
    def __init__(self, reference, mainModel, treeView, parent=None):
        super(EditPathMainWidget, self).__init__(parent=parent)
        self._settings = QtCore.QSettings("RockstarSettings", "EditPathMainWidget")
        self.editPathWidget = EditPathWidget(reference, mainModel, treeView)
        self.editPathWidget.updateEvent.connect(self._handleModelUpdate)
        self.setupUi()

    def _handleModelUpdate(self):
        self.updateEvent.emit()

    def setupUi(self):
        # add layout
        mainLayout = QtGui.QVBoxLayout()

        # add widgets
        banner = UI.BannerWidget(name='Namespace Editor', helpUrl="")

        # set widgets to be flush to the edges of the main window
        mainLayout.setContentsMargins(0, 0, 0, 0)

        # add widgets to mainlayout
        mainLayout.addWidget(banner)
        mainLayout.addWidget(self.editPathWidget)

        # set layout
        self.setLayout(mainLayout)
        self.modal = True

    def show(self):
        main = UI.QtMainWindowBase()
        main.setCentralWidget(self)
        main.show()


class EditPathWidget(QtGui.QWidget):
    '''
    Widgets for inside the main window of EditPathMainWidget
    '''
    manager = Manager()
    # signal to notify when the editpath update button has ran - passing it to main widget
    updateEvent = QtCore.Signal()
    def __init__(self, reference, mainModel, treeView, parent=None):
        super(EditPathWidget, self).__init__(parent=parent)
        self._reference = reference
        self._mainModel = mainModel
        self._treeView = treeView
        self._characterList = []
        self._pathLineEdit = None
        self._origPathLabel = None
        self.setupWidget()

    def _handleSetLineEditDefaultText(self):
        # set default path
        self._pathLineEdit.setText(selectedNamespace)
        return

    def _handleUpdatePath(self):
        # get old and new names
        newPath = str(self._pathLineEdit.text())

        # check path exists
        Perforce.Sync(newPath)
        if not os.path.exists(newPath):
            QtGui.QMessageBox.warning(None,
                                      "Edit Path Error",
                                      "Path passed does not exist.\n\nUnable to process with update.\n\nAborting script.")
            return

        # check that the type of asset is still the same, if not abort script
        if not self._reference.FormattedTypeName == self.manager.GetTypeFromPath(newPath).FormattedTypeName:
            QtGui.QMessageBox.warning(None,
                                      "Edit Path Error",
                                      "Path passed does not match the same Reference type as it was previously.\n\nUnable to process with update.\n\nAborting script.")
            return

        #Call the manager's Change path function
        self.manager.ChangePath(self._reference, newPath)

        self.updateEvent.emit()
        # reload ui data
        self._mainModel.reset()
        self._treeView.expandAll()

    def getReferencePath(self, reference):
        pathProperty = reference.PropertyList.Find("Reference Path")
        if pathProperty:
            return pathProperty.Data

    def setupWidget(self):
        # create layout
        layout = QtGui.QGridLayout()

        # create widgets
        alertLabel = QtGui.QLabel('Alert:\nThe asset you are changing the path to must have the same skeleton and rig setup as the original asset, otherwise you will experience\noffsets and issues. This tool is purely for redirecting the same asset to a different location.')
        pathLabel = QtGui.QLabel('Original Path:')
        self._origPathLabel = QtGui.QLabel()
        self._origPathLabel.setText(self._reference.Path)#self.getReferencePath(self._reference))
        updateLabel = QtGui.QLabel('New Path:')
        self._pathLineEdit = QtGui.QLineEdit()
        updatePushButton = QtGui.QPushButton('Update Path')

        # add widgets to layout
        layout.addWidget(pathLabel, 0, 0)
        layout.addWidget(self._origPathLabel, 0, 1)
        layout.addWidget(updateLabel, 1, 0)
        layout.addWidget(self._pathLineEdit, 1, 1)
        layout.addWidget(updatePushButton, 2, 1)
        layout.addWidget(alertLabel, 3, 0, 1, 2)

        # set layout
        self.setLayout(layout)

        # set signals
        updatePushButton.clicked.connect(self._handleUpdatePath)