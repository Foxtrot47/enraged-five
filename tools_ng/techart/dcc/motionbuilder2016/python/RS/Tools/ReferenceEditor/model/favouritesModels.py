import os.path
from PySide import QtCore
from RS.Utils import Path
from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem, baseModel
from RS.Tools.CameraToolBox.PyCore import pySideSingleton
from RS.Core.ReferenceSystem import Manager


class FavouriteReferenceItem(textModelItem.TextModelItem):
    '''Represents an item of the favourites list.'''
    def __init__(self, referenceName, referenceType, filePath, parent=None):
        columnTextList = []
        columnTextList.append(referenceName)
        self._referenceNameIdx = len(columnTextList) - 1
        columnTextList.append(referenceType)
        self._referenceTypeIdx = len(columnTextList) - 1
        columnTextList.append(filePath)
        self._filePathIdx = len(columnTextList) - 1
         
        super(FavouriteReferenceItem, self).__init__(columnTextList, parent=parent)
    
    def getReferenceName(self):
        return self.itemData.get(self._referenceNameIdx)
    
    def getReferenceType(self):
        return self.itemData.get(self._referenceTypeIdx)
    
    def getReferenceFilePath(self):
        return self.itemData.get(self._filePathIdx)


class FavouriteReferencesModel(baseModel.BaseModel):
    '''The container model for the list of favourites.'''
    
    Tool_Title = 'Favourite References'
    Favourites_List_Key = 'FavouritesList'
    Reference_Name_Key = 'ReferenceName'
    Reference_Type_Key = 'ReferenceType'
    File_Path_Key = 'FilePath'
    
    __metaclass__ = pySideSingleton.PySideSingleton
    
    def __init__(self, parent=None):
        self._settings = QtCore.QSettings("RockstarSettings", self.Tool_Title)
        
        super(FavouriteReferencesModel, self).__init__(parent=parent)
    
    def _clearFavourites(self):
        '''Empties the favourites list'''
        self.clear()
    
    def _loadFavourites(self):
        '''Loads the favourites data from settings into items in the model.'''
        self._clearFavourites()
        favData = self._settings.value(self.Favourites_List_Key, [])
        if not favData:
            return
        
        self.beginInsertRows(QtCore.QModelIndex(), 0, len(favData) - 1)
        for fav in favData:
            favItem = FavouriteReferenceItem(str(fav.get(self.Reference_Name_Key)),
                                             str(fav.get(self.Reference_Type_Key)),
                                             str(fav.get(self.File_Path_Key)))
            self.rootItem.appendChild(favItem)
        self.endInsertRows()
    
    def _getFavDataForSerialise(self):
        '''
        Converts the items from the model into a serialisable form and returns it.
        
        Return:
            (list(dict)): A list of dictionaries which contain the data from each favourites item.
        '''
        favData = []
        for item in self.rootItem.childItems:
            favDict = {}
            favDict[self.Reference_Name_Key] = item.getReferenceName()
            favDict[self.Reference_Type_Key] = item.getReferenceType()
            favDict[self.File_Path_Key] = item.getReferenceFilePath()
            favData.append(favDict)
        return favData
    
    def saveFavourites(self):
        '''
        Save the current list of favourites from this model into settings.
        '''
        favData = self._getFavDataForSerialise()
        self._settings.setValue(self.Favourites_List_Key, favData)
    
    def addFavourite(self, filePath, referenceName=None, autoSave=False):
        '''
        Adds an item of favourites to the model.
        
        Parameters:
            filePath (str): The absolute path to the reference asset.
            referenceName (str): The name of the reference (if none the file name will be used).
            autoSave (bool): If True the favourites will be saved after this item
                             is added (default False).
        '''
        # Skip adding if the asset is already in the favourites list
        for item in self.rootItem.childItems:
            normItemPath = os.path.normpath(item.getReferenceFilePath())
            if normItemPath.lower() == os.path.normpath(filePath).lower():
                return False
        
        if referenceName is None:
            referenceName = Path.GetBaseNameNoExtension(filePath)
        
        manager = Manager.Manager()
        refType = manager.GetTypeFromPath(filePath)
        refTypeName = 'None'
        try:
            refTypeName = refType.FormattedTypeName
        except AttributeError:
            pass
        
        favItem = FavouriteReferenceItem(referenceName, refTypeName, filePath)
        
        nextIdx = self.rowCount()
        self.beginInsertRows(QtCore.QModelIndex(), nextIdx, nextIdx)
        self.rootItem.appendChild(favItem)
        self.endInsertRows()
        
        if autoSave:
            self.saveFavourites()
            
        return True
        
    def removeFavourite(self, idx, autoSave=False):
        '''
        Removes the favourites item at the given index from the model.
        
        Parameters:
            idx (QtCore.QModelIndex): The model index representing the item to be removed.
            autoSave (bool): If True the favourites will be saved after this item
                             is removed (default False).
        Return:
            
        '''
        if not idx.isValid():
            return False
        
        item = idx.internalPointer()
        row = idx.row()
        self.beginRemoveRows(QtCore.QModelIndex(), row, row)
        self.rootItem.removeChild(item)
        self.endRemoveRows()
        
        if autoSave:
            self.saveFavourites()
        
        return True
    
    # baseModel.BaseModel interface:
    
    def setupModelData(self, parent):
        self._loadFavourites()
    
    def getHeadings(self):
        '''
        Heading strings to be displayed in the view.
        '''
        return ['Reference Name', 'Type', 'File Path']
