from RS.Tools.AnimRender.Widgets import AnimRender


def Run( show = True ):
    toolsDialog = AnimRender.MainWindow()
    
    if show:
        toolsDialog.show()

    return toolsDialog