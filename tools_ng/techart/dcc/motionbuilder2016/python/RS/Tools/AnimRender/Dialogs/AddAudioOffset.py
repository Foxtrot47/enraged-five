import sys
from PySide import QtCore, QtGui
from RS import Config
from RS.Tools.UI import Application

class AddAudioOffsetWindow(QtGui.QDialog):
    addAudioOffsetSignal = QtCore.Signal(str)
    
    def __init__(self, parent = None):
        
        if parent is None:
            parent = Application.GetMainWindow()
                    
        super(AddAudioOffsetWindow, self).__init__(parent)

        self.setWindowFlags( QtCore.Qt.Window | QtCore.Qt.WindowMinimizeButtonHint | QtCore.Qt.FramelessWindowHint )#| QtCore.Qt.ToolTip )#)# dont allow maximise
        
        self.installEventFilter(self)# make our right click dialog window aware of its 'Focus' state (i.e. is it Active?)
        
        layout = QtGui.QVBoxLayout()
        self.pushButton_addAudioOffset = QtGui.QPushButton("Add Audio Offset")
        self.pushButton_addAudioOffset.setObjectName("pushButton_addAudioOffset")    
        self.pushButton_addAudioOffset.clicked.connect(self.signalAddAudioOffset)    
        layout.addWidget(self.pushButton_addAudioOffset)
        
        self.spinBox_audioOffset = QtGui.QSpinBox()
        self.spinBox_audioOffset.setMinimum(-9999)
        self.spinBox_audioOffset.setMaximum(9999)
        self.spinBox_audioOffset.setProperty("value", 0)
        self.spinBox_audioOffset.setObjectName("spinBox_audioOffset")
        layout.addWidget(self.spinBox_audioOffset)
        
        self.setLayout(layout)

    def signalAddAudioOffset( self ): # after adding the offset, the dialogue will close
        self.addAudioOffsetSignal.emit(self.spinBox_audioOffset.text().encode("utf-8"))
        self.close()

    def eventFilter(self, object, event): # if this dialog loses focus, (if the user decides against adding an offset by selecting another window) this will close the dialog
        if event.type() == QtCore.QEvent.WindowDeactivate:
            self.close()
            return False
        return QtGui.QDialog.eventFilter(self, object, event)