"""
Anim Render
Based of several other render tools that exist, but made specifically for GTAO

Updated spring 2020 to be available for all projects, incorporating more features.
"""

import pyfbsdk as mobu
import xml.etree.ElementTree as ET
import getpass
import os
import ast
import sys
import shutil
import subprocess
import shlex
import ctypes
import re
import time
import webbrowser
import runpy

from xml.dom import minidom
from datetime import datetime, timedelta
from PySide import QtGui, QtCore
from operator import itemgetter

import RS.Tools.UI
from RS import Config, Globals, Perforce
from RS.Utils import Namespace
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Audio import Audio
from RS.Tools.UI import Application

from RS.Tools.AnimRender.Dialogs import AddAudioOffset
from RS.Tools.AnimRender.Tools import Tools


ANIMRENDER_CONFIG_PATH = os.path.join(os.path.expandvars("%APPDATA%"), "rockstar", "animrender", "AnimRender_ConfigData.xml")
username = RS.Config.User.Name
uiDirectory = os.path.join(RS.Config.Script.Path.Root, "RS\\Tools\\UI\\AnimRender\\Qt\\")
iconDirectory = os.path.join(Config.Script.Path.ToolImages, "AnimRender\\Icons\\")

RS.Tools.UI.CompileUi(uiDirectory + "AnimRender.ui", uiDirectory + "AnimRender_compiled.py")
import RS.Tools.UI.AnimRender.Qt.AnimRender_compiled as AnimRenderCompiled


class AutoActivateHud(object):
    """
    class-object to hold the state of the Auto Activate option for the Hud display as we switch takes
    """
    Active = False


class AutoAddCamera(object):
    """
    class-object to hold the state of the camera option in the basic tab takes window
    """
    Active = True


class AutoSelectAddedTake(object):
    """
    class-object to hold the state of the camera option in the basic tab takes window
    """
    Active = False


class RenderSettings(object):
    """
    class-object to hold the info for the Render Settings
    """
    Custom1Path = ""

    def __init__(self):
        self.data = []
# creating an instance of RenderSettings
RenderSettings = RenderSettings()


# tool name and version
version = '2.0.1a'
toolName = 'Anim Render v{0}'.format(version)


class MainWindow(RS.Tools.UI.QtMainWindowBase, AnimRenderCompiled.Ui_MainWindow):
    def __init__(self):
        RS.Tools.UI.QtMainWindowBase.__init__(self, None, title='Anim Render')

        self.setupUi(self)
        
        self.installEventFilter(self)
        
        self.fallbackVideoHelperDefaultValues = [['None', ''],
        ['Mock-Up', 'Aspirational content that proves as a benchmark for what we ultimately wish to achieve through future mechanics.  This content will serve as a style guide for the rest of the team, helping to guide development in the direction we wish to achieve.  It is designed to look like final game footage but is completely fake.  It cannot be used in practice.'],
        ['Prototype', 'This content has been hacked together using non-final methods to prove the potential functionality of a system.  Although it has been implemented into the engine, it may not scale for a final game.  The code and animation structure may need to be rewritten in order to deliver this as a fully functioning mechanic.'],
        ['Asset Production', "Examples of assets that have been created to fit a templated system.  The content could be shown in either the authoring package or the engine.  However, it's quite likely that this content is yet to be implemented by script or code."],
        ['Basic Implementation', 'The first implementation of a feature. A large amount of iteration is still planned, and many bugs will be present. Equates to Cinematic 2nd Pass Level - a large element of finessing still required.'],
        ['Iteration Phase', 'Continued development of the feature. Bugs are still likely, but review notes are being addressed. Represents the bulk of the iterative process. Equivalent of Cinematic 3rd Pass Level.'],
        ['Polish', "This represents the final iteration of any content.  It should be in a 'shippable' state but until the game is actually released we should expect final tweaks."], ['Custom', 'Add a custom subtitle to the video']]
        
        ######################################################
        ############## HUB PAGE NEEDS UPDATING ###############
        banner = RS.Tools.UI.BannerWidget(name=toolName, helpUrl=('https://hub.gametools.dev/display/ANIM/Audio+Hud'))
        self.horizontalLayout.addWidget(banner)
        
        self.splitter_basic_ver.setSizes([600, 1500])   # set the split sizes for basic tab
        self.splitter_basic_hor.setSizes([1000, 500])   # set the split sizes for basic tab
        self.treeWidget_basic_association.setColumnWidth(0, 250)
        self.treeWidget_basic_association.setIconSize(QtCore.QSize(16, 16))
        
        self.splitter_ver.setSizes([600, 1500])         # set the split sizes for advanced tab
        self.splitter_hor.setSizes([500, 1000, 500])    # set the split sizes for advanced tab
        self.treeWidget_association.setColumnWidth(0, 300)
        self.treeWidget_association.setIconSize(QtCore.QSize(16, 16))

        self.treeWidget_association.setStyleSheet(
            "background-color: rgb(30, 40, 40);")  # the background/header/sidebar colour

        self.treeWidget_association.setAlternatingRowColors(1)
        
        self.treeWidget_basic_association.setStyleSheet(
            "background-color: rgb(30, 40, 40);")  # the background/header/sidebar colour
            
        self.setStyleSheet("""QToolTip { 
                           background-color: black; 
                           color: Grey; 
                           border: black solid 1px
                           }""")

        self.treeWidget_basic_association.setAlternatingRowColors(1)

        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Window, QtGui.QColor(255, 255, 255))  # the checkbox boarder
        palette.setColor(QtGui.QPalette.Base, QtGui.QColor(60, 60, 60))  # the checkbox colour
        palette.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(50, 60, 60))  # the lighter alternate line colour
        self.treeWidget_association.setPalette(palette)
        self.treeWidget_basic_association.setPalette(palette)
        
        self.Manager = Manager()

        self.wavIcon = QtGui.QIcon(iconDirectory + "wav_icon.png")
        self.animIcon = QtGui.QIcon(iconDirectory + "anim_icon.png")
        self.cameraIcon = QtGui.QIcon(iconDirectory + "camera_icon.png")
        self.cameraNoIcon = QtGui.QIcon(iconDirectory + "camera_no_icon.png")
        self.offsetIcon = QtGui.QIcon(iconDirectory + "offset_icon.png")

        self.populateTakeList() # also handled by the callbacks
        self.populateAudioList() # also handled by the callbacks
        self.populateCameraList() # also handled by the callbacks
        self.populateRenderOptions()
        
        Globals.Callbacks.OnTakeChange.Add(self.generalTakeChanged)
        Globals.Callbacks.OnFileOpenCompleted.Add(self.onSceneChanged)
        Globals.Callbacks.OnFileNewCompleted.Add(self.onNewScene)

        # register callbacks
        #Globals.Callbacks.OnTakeChange.Add(self.populateTakeList)

        self.listWidget_audio.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.listWidget_audio.customContextMenuRequested.connect(self.showAudioRightClickMenu)
        self.listWidget_audio.setStyleSheet("background-color: rgb(30, 40, 40);")

        self.listWidget_take.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.listWidget_take.customContextMenuRequested.connect(self.showTakeRightClickMenu)
        self.listWidget_take.setStyleSheet("background-color: rgb(30, 40, 40);")
        
        self.listWidget_basic_take.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.listWidget_basic_take.customContextMenuRequested.connect(self.showTakeRightClickMenu_basic)
        self.listWidget_basic_take.setStyleSheet("background-color: rgb(30, 40, 40);")

        self.listWidget_camera.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.listWidget_camera.customContextMenuRequested.connect(self.showCameraRightClickMenu)
        self.listWidget_camera.setStyleSheet("background-color: rgb(30, 40, 40);")

        self.listWidget_basic_camera.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.listWidget_basic_camera.customContextMenuRequested.connect(self.showCameraRightClickMenu_basic)
        self.listWidget_basic_camera.setStyleSheet("background-color: rgb(30, 40, 40);")        
        
        self.treeWidget_association.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.treeWidget_association.customContextMenuRequested.connect(self.showAssociationRightClickMenu)
        self.treeWidget_association.itemClicked.connect(self.onClickItem)

        self.treeWidget_basic_association.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.treeWidget_basic_association.customContextMenuRequested.connect(self.showAssociationRightClickMenu_basic)
        #self.treeWidget_basic_association.setEditTriggers(self.treeWidget_basic_association.NoEditTriggers)
        self.treeWidget_basic_association.itemDoubleClicked.connect(self.setColumnEditable)
      
        self.treeWidget_basic_association.itemClicked.connect(self.getCheckState_basic)

        self.checkBox_addTodaysDate.stateChanged.connect(self.addTodaysDate)

        self.lineEdit_outputPath.textChanged.connect(self.writeToCustom2DataNode)

        self.lineEdit_prefix_suffix.textChanged.connect(self.writeToPrefixSuffixDataNode)
        self.radioButton_prefix.toggled.connect(self.writeToPrefixSuffixDataNode)
        self.radioButton_suffix.toggled.connect(self.writeToPrefixSuffixDataNode)

        self.pushButton_browse_outputPath.clicked.connect(self.browseOutputPath)
        self.pushButton_openOutputFolder.clicked.connect(self.openExplorer)

        # make a list of the radio buttons as its just easier to iterate through them
        self.renderDestRadioButtons = [self.radioButton_output_Desktop,
                                       self.radioButton_output_GTA,
                                       self.radioButton_output_Guess,
                                       self.radioButton_output_Custom,
                                       self.radioButton_output_Custom2]

        self.renderToMovCheckboxes = [self.checkBox_isMov_desktop,
                                       self.checkBox_isMov_weeklies,
                                       self.checkBox_isMov_guess,
                                       self.checkBox_isMov_local,
                                       self.checkBox_isMov_embeded]

        for radioButton in self.renderDestRadioButtons:
            radioButton.toggled.connect(self.getRenderDest)

        self.comboBox_pictureFormat.currentIndexChanged.connect(self.updateCustomFormat)
        self.comboBox_customRenderSettings.currentIndexChanged.connect(self.findCustomSettings)

        self.pushButton_startRender.clicked.connect(self.startRender)
        self.pushButton_saveRenderSettings.clicked.connect(self.saveConfig)
        self.pushButton_updateRenderSettings.clicked.connect(self.updateConfig)
        self.pushButton_deleteRenderSettings.clicked.connect(self.deleteConfig)

        # setup some lists to hold the treewidget data
        self.treeData = []
        self.treeCamera = []
        self.treeAudioOffset = []
        self.treeCheckState = []
        
        # more list for the basic tree widget
        self.treeData_basic = []
        self.treeCheckState_basic = []

        # first run of the methods that set up the tool
        self.readSubtitleXML() # running this first so we can add tooltips (if needs be)
        self.createDataNode()
        self.readDataNode()
        self.colouriseWidgets()
        self.readConfigXML()
        self.getRenderDest()

        self.pushButton_generate_confluence_page.clicked.connect(self.processConfluencePage)

        # put this here so it only gets connected after the treeWidget is ready
        self.treeWidget_basic_association.itemChanged.connect(self.storeCustomSubtitle)


    def processConfluencePage(self):

        parentPage = '{0}'.format(self.lineEdit_confluence_parent_page.text())
        pageName = '{0}'.format(self.lineEdit_confluence_new_page_name.text())
        perforceFolder = '{0}'.format(self.lineEdit_confluence_perforce_folder.text())

        if self.checkBox_confluence_use_children_folders.isChecked():
            useChildFolders = True
        else:
            useChildFolders = False

        pageID = '{0}'.format(self.lineEdit_confluence_pageID.text())
        
        CreateRenderTemplate = Tools.createRenderTemplate(perforceFolder, useChildFolders)
        
        if CreateRenderTemplate == 'Fail': # specifically check to see if it returned 'Fail' which would mean we have no data to process
            return

        Tools.createConfDataFile(parentPage, pageName, perforceFolder, useChildFolders, pageID)

        toolDirectory = os.path.join(RS.Config.Script.Path.Root, 'RS', 'Tools', 'AnimRender', 'Tools')
        RenderTemplate = os.path.join(toolDirectory, 'RenderTemplate.txt')
        confPageData = os.path.join(toolDirectory, 'ConfPageData.xml')

        destConfigFile = os.path.join(Config.Tool.Path.Root,
                                      "bin", "ConfluencePythonTools", "MiscFiles", "ScriptedAnimation", "ConfPageData.xml")
        destTemplateFile = os.path.join(Config.Tool.Path.Root,
                                        "bin", "ConfluencePythonTools", "MiscFiles", "ScriptedAnimation", "RenderTemplate.txt")

        if not os.path.exists(os.path.join(Config.Tool.Path.Root,
                                           "bin", "ConfluencePythonTools", "MiscFiles", "ScriptedAnimation")):
            os.mkdir(os.path.join(Config.Tool.Path.Root,
                                  "bin", "ConfluencePythonTools", "MiscFiles", "ScriptedAnimation"))

        shutil.copy2(confPageData, destConfigFile)
        shutil.copy2(RenderTemplate, destTemplateFile)

        pageGen = os.path.join(Config.Tool.Path.Root,
                               "bin", "ConfluencePythonTools", "ScriptedAnimPageGenerator.py")

        args = (os.path.join(Config.Tool.Path.Root,
                             "bin", "PythonLauncher", "PythonLauncher.exe -native_args ")) + pageGen

        p = subprocess.Popen(["start", "cmd", "/k", args], shell=True)  # Needs to be shell since start isn't an executable, its a shell cmd
        p.wait()  # I can wait until finished (although it too finishes after start finishes)

        #except Exception, e:
        #    QtGui.QMessageBox.warning(None, "Writing to Hub Error.",
        #                                str(e))


    def eventFilter(self, object, event):
        """
        event filter to see if:
            'delete' is pressed in the basic tab
            this will do a quick delete of selected items
            
            'return' is pressed in the basic tab
            this will check all selected items
            
            'shift + return' is pressed in the basic tab
            this will uncheck all the selected items
        """
        
        modifiers = QtGui.QApplication.keyboardModifiers()
        
        if event.type() == QtCore.QEvent.KeyPress:
            if self.tabWidget_tasks.currentIndex() == 0:
                if event.key() == QtCore.Qt.Key_Delete:# specify's the delete key
                    if self.tabWidget_tasks.currentIndex() == 0:
                        deleteIndexes = []
                        
                        getSelected = self.treeWidget_basic_association.selectedItems()
                        associationRoot = self.treeWidget_basic_association.invisibleRootItem()
                
                        for selectedTake in getSelected:
                            for takeItem in range(len(self.treeData_basic)):
                                if selectedTake.data(0, QtCore.Qt.DisplayRole) == self.treeData_basic[takeItem][0]:
                                    deleteIndexes.append(takeItem)
                        
                        # delete the items from the lists
                        for deleteIndex in reversed(range(len(deleteIndexes))):       
                            del self.treeData_basic[deleteIndexes[deleteIndex]]
                            del self.treeCheckState_basic[deleteIndexes[deleteIndex]]
    
                        # delete the items from the tree widget
                        associationRoot = self.treeWidget_basic_association.invisibleRootItem()
                        for item in self.treeWidget_basic_association.selectedItems():
                            (item.parent() or associationRoot).removeChild(item)
                            
                        self.getCheckState_basic()
                        self.writeToDataNode()

                if event.key() == QtCore.Qt.Key_Return:# specify's the return key
                    if modifiers & QtCore.Qt.ShiftModifier:# see if we're holding down shift, which means we de-select
                        getSelected = self.treeWidget_basic_association.selectedItems()
                        
                        for selectedTake in getSelected:
                            selectedTake.setCheckState(0, QtCore.Qt.Unchecked)            
                    else:
                        getSelected = self.treeWidget_basic_association.selectedItems()
      
                        for selectedTake in getSelected:
                            selectedTake.setCheckState(0, QtCore.Qt.Checked)
                    
                    self.getCheckState_basic()
                    self.writeToDataNode()

        return QtGui.QMainWindow.eventFilter(self, object, event)
                    

    def generalTakeChanged(self, source, event):
        """
        method to refresh the take lists that are connected to the contents of the scene
        """
        self.populateTakeList()
        self.populateAudioList()
        self.populateCameraList()
        self.colouriseWidgets()
        #self.populateRenderOptions()


    def onSceneChanged(self, source, event):
        """
        method to refresh the UI lists that are connected to the contents of the scene
        """
        self.populateTakeList()
        self.populateAudioList()
        self.populateCameraList()

        self.createDataNode()
        self.readDataNode()
        self.colouriseWidgets()


    def onNewScene(self, source, event):
        """
        method to refresh the UI lists that are connected to the contents of the scene
        """
        # this will essentially clear all the lists and scene data
        #self.populateTakeList()
        #self.populateAudioList()
        #self.populateCameraList()

        self.treeData = []
        self.treeCamera = []
        self.treeAudioOffset = []
        self.treeCheckState = []
        self.treeData_basic = []
        self.treeCheckState_basic = []

        self.populateAllLists()
        self.colouriseWidgets()

        self.treeWidget_association.clear()
        self.treeWidget_basic_association.clear()


    def closeEvent(self, event):
        """
        un-register callbacks
        """
        
        if AutoActivateHud.Active:
            AutoActivateHud.Active = False
            Globals.Callbacks.OnTakeChange.Remove(self.onTakeChanged)
        else:
            Globals.Callbacks.OnTakeChange.Remove(self.generalTakeChanged)
            Globals.Callbacks.OnFileOpenCompleted.Remove(self.onSceneChanged)
            Globals.Callbacks.OnFileNewCompleted.Remove(self.onNewScene)

    
    def openExplorer(self):
        path = "explorer " + self.lineEdit_outputPath.text()
        subprocess.Popen(path)


    def browseOutputPath(self):

        dirpath = self.lineEdit_outputPath.text()

        #if not os.path.isdir(dirpath):
         #   os.makedirs(dirpath)

        popup = QtGui.QFileDialog()
        popup.setOption(QtGui.QFileDialog.ShowDirsOnly)
        popup.setDirectory(dirpath)
        #if Directory:
        popup.setFileMode(QtGui.QFileDialog.Directory)
        selected_directory = QtGui.QFileDialog.getExistingDirectory(None, "Choose the Folder Path", "")
        if selected_directory:
            selected_directory = '{0}{1}'.format(selected_directory, '\\')
            self.lineEdit_outputPath.setText(selected_directory)


    ###########################
    ##### basic tab menus #####
    def showTakeRightClickMenu_basic(self):

        menu = QtGui.QMenu()

        self.addTakeToRenderList = QtGui.QAction("Add Take(s) To Render List", self)
        self.addTakeToRenderList.triggered.connect(self.addTakes_basic)

        self.autoSelectAddedTake = QtGui.QAction("Automatically Select Added Take", self, checkable=True)
        if AutoSelectAddedTake.Active:
            self.autoSelectAddedTake.setChecked(True)
        self.autoSelectAddedTake.triggered.connect(self.hudAutoSelectAddedTake)     
    
        self.autoCamera = QtGui.QAction("Automatically Add Selected Camera", self, checkable=True)
        if AutoAddCamera.Active:
            self.autoCamera.setChecked(True)
        self.autoCamera.triggered.connect(self.hudActiveCameraisChecked)

        menu.addAction(self.addTakeToRenderList)
        menu.addAction(self.autoSelectAddedTake)
        menu.addAction(self.autoCamera)
                        
        menu.exec_(QtGui.QCursor.pos())


    def showCameraRightClickMenu_basic(self):
        
        menu = QtGui.QMenu()
        
        self.addTakeToRenderList = QtGui.QAction("Add Take(s) To Render List", self)
        self.addTakeToRenderList.triggered.connect(self.addTakes_basic)

        self.autoSelectAddedTake = QtGui.QAction("Automatically Select Added Take", self, checkable=True)
        if AutoSelectAddedTake.Active:
            self.autoSelectAddedTake.setChecked(True)
        self.autoSelectAddedTake.triggered.connect(self.hudAutoSelectAddedTake)     
                
        self.associateBasicCamera = QtGui.QAction("Add or Replace Camera to Selected Take", self)
        self.associateBasicCamera.triggered.connect(self.addCamera_basic)

        menu.addAction(self.addTakeToRenderList)
        menu.addAction(self.autoSelectAddedTake)
        menu.addAction(self.associateBasicCamera)
     
        menu.exec_(QtGui.QCursor.pos())       


    def showAssociationRightClickMenu_basic(self):

        menu = QtGui.QMenu()
        self.selectAllTakes = QtGui.QAction("Select All", self)
        self.selectAllTakes.triggered.connect(self.selectAllTakes_basic)
        
        self.selectNoTakes = QtGui.QAction("De-Select All", self)
        self.selectNoTakes.triggered.connect(self.deSelectAllTakes_basic)

        self.deleteAllTakes = QtGui.QAction("Delete All", self)
        self.deleteAllTakes.triggered.connect(self.deleteAll_basic)

        menu.addAction(self.selectAllTakes)
        menu.addAction(self.selectNoTakes)
        menu.addAction(self.deleteAllTakes)

        subtitleOptions = menu.addMenu("Add Production Stage Subtitle")
        for subtitle in self.subtitleGroups:
            subtitleName = 'subtitle_' + '{}'.format(subtitle[0])
            subtitleName = QtGui.QAction('{}'.format(subtitle[0]), self)
            # need to use lambda function to call the item in the loop to make sure we pass the item clicked to the method
            subtitleName.triggered.connect(lambda stage=subtitle: self.addSubtitleOptions(stage))
            subtitleOptions.addAction(subtitleName)

        audioOptions = menu.addMenu("Add Audio")
        for items in range(self.listWidget_audio.count()):
            audioFile = self.listWidget_audio.item(items)
            audioFileName = '{}'.format(audioFile.text())
            audioFileName = QtGui.QAction('{}'.format(audioFileName), self)
            # need to use lambda function to call the item in the loop to make sure we pass the item clicked to the method
            audioFileName.triggered.connect(lambda audio=audioFile.text(): self.addAudioOptions(audio))
            audioOptions.addAction(audioFileName)
        
        self.deleteAudio = QtGui.QAction("Delete Audio", self)
        self.deleteAudio.triggered.connect(lambda audio='': self.addAudioOptions(audio))       
        menu.addAction(self.deleteAudio)
                    
        menu.exec_(QtGui.QCursor.pos())


    #############################
    ##### basic tab methods #####          
    def addTakes_basic(self):

        takeItems = [item.text().encode("utf-8") for item in self.listWidget_basic_take.selectedItems()]
        camera = [item.text().encode("utf-8") for item in self.listWidget_basic_camera.selectedItems()]

        for takeItem in takeItems:
            if not [x for x in self.treeData_basic if takeItem in x]: # search through all sublists in self.treeData_basic
                root = QtGui.QTreeWidgetItem(self.treeWidget_basic_association, [takeItem])
                root.setIcon(0, self.animIcon)
                
                if AutoSelectAddedTake.Active:
                    root.setCheckState(0, QtCore.Qt.Checked) 
                else:
                    root.setCheckState(0, QtCore.Qt.Unchecked)
                    
                self.treeData_basic.append([takeItem])
                takeIndex = self.treeData_basic.index([takeItem])
                if AutoAddCamera.Active:
                    root.setIcon(1, self.cameraIcon)
                    root.setText(1, camera[0])
                    self.treeData_basic[takeIndex].append(camera[0])
                else:
                    root.setIcon(1, self.cameraNoIcon)
                    self.treeData_basic[takeIndex].append('')
                root.setText(2, 'None')# add an extra blank slot for the subtitle
                self.treeData_basic[takeIndex].append('None')
                self.treeData_basic[takeIndex].append('')#add another blank slot ready for an optional audio file
        
        self.getCheckState_basic()
        self.writeToDataNode()

 
    def addCamera_basic(self):
        
        camera = [item.text().encode("utf-8") for item in self.listWidget_basic_camera.selectedItems()]

        for item in self.treeWidget_basic_association.selectedItems():
            for takeItem in self.treeData_basic:
                if item.text(0) in takeItem:
                    takeItem[1] = camera[0]
                    item.setIcon(1, self.cameraIcon)
                    item.setText(1, camera[0])
                    
        self.getCheckState_basic()
        self.writeToDataNode()


    def getCheckState_basic(self):
        # time to store all the checked checkboxes 
        self.treeCheckState_basic = []

        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidget_basic_association)

        while iterator.value():
            item = iterator.value()

            column0Value = item.checkState(0)

            if item.checkState(0):
                column0Value = True
            else:
                column0Value = False

            self.treeCheckState_basic.append(column0Value)
            iterator += 1

        self.writeToDataNode()


    def selectAllTakes_basic(self):
        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidget_basic_association)

        while iterator.value():
            item = iterator.value()

            column0Value = item.checkState(0)

            if not item.checkState(0):
                item.setCheckState(0, QtCore.Qt.Checked)

            iterator += 1
            
        self.getCheckState_basic()
        self.writeToDataNode()


    def deSelectAllTakes_basic(self):

        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidget_basic_association)

        while iterator.value():
            item = iterator.value()

            column0Value = item.checkState(0)

            if item.checkState(0):
                item.setCheckState(0, QtCore.Qt.Unchecked)

            iterator += 1
            
        self.getCheckState_basic()
        self.writeToDataNode()


    def deleteAll_basic(self):
        #delete all takes 
        self.treeData_basic = []
        self.treeCheckState_basic = []
        
        self.treeWidget_basic_association.clear()
        
        self.getCheckState_basic()
        self.writeToDataNode()


    def readSubtitleXML(self):

        dirPath = '{0}'.format("//americas/gametools/release/VideoHelper/DefaultValues.xml")

        Perforce.Sync([dirPath], force=True)

        if os.path.isfile("x:\\americas\\gametools\\release\\VideoHelper\\DefaultValues.xml"):
  
            try:
                tree = ET.parse("x:\\americas\\gametools\\release\\VideoHelper\\DefaultValues.xml")
            except Exception, e:
                QtGui.QMessageBox.warning(None, "parsing error", "error parsing VideoHelper\\DefaultValues.xml" + str(e)
                                        + "\n\nUsing a default set of values for the 'Production Stage Subtitles'.")
                return
    
            root = tree.getroot()
    
            for item in root.findall(".//PresetSubtitles"):
                self.subtitleGroups = []
                self.subtitleGroups.append(['None',''])
                for child in item:
                    subItem = []
                    for value in child:
                        if 'Name' in value.tag:
                            subItem.append(value.text)
                        if 'Description' in value.tag:
                            subItem.append(value.text)
                    self.subtitleGroups.append(subItem)
        else:
            QtGui.QMessageBox.warning(None, "filecheck error", "error parsing VideoHelper\\DefaultValues.xml"
                        + "\n\nUsing a default set of values for the 'Production Stage Subtitles'."
                        + "\n\nsee:- url:bugstar:6540581 - VideoHelper is not available for GTAO")
            self.subtitleGroups = []
            self.subtitleGroups = self.fallbackVideoHelperDefaultValues[:]


    def setColumnEditable(self, item, column):
        """
        Test if is this a custom edit.
        The list comprehension is a search for a non standard string already set in the custom column
        """
        if item.text(2) == 'Custom' or not [x for x in self.subtitleGroups if item.text(2) in x]:
            if column == 2 :
                item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable) 
            else:
                item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEditable)


    def addSubtitleOptions(self, stage):
        
        for item in self.treeWidget_basic_association.selectedItems():
            for takeItem in self.treeData_basic:
                if item.text(0) in takeItem:
                    item.setText(2, stage[0])
                    tooltip = stage[1]
                    tooltip = tooltip.replace('. ','.\n')
                    item.setToolTip(2, tooltip)
                    takeItem[2] = stage[0]
                    if item.text(2) != 'Custom':
                        item.setFlags(item.flags() & ~QtCore.Qt.ItemIsEditable)
                    
        self.writeToDataNode()


    def storeCustomSubtitle(self):
        """
        Make sure custom edits to the subtitles are stored and are present when we re-open the file, or re-run the tool.
        """
        commentData = []

        count = 0
        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidget_basic_association)

        while iterator.value():
            item = iterator.value()

            if item.text(2) == 'Custom' or not [x for x in self.subtitleGroups if item.text(2) in x]:
                commentData.append([count, '{}'.format(item.text(2))])

            count += 1
            iterator += 1

        for comment in commentData:
            self.treeData_basic[comment[0]][2] = comment[1]

        if commentData:
            self.writeToDataNode()


    def addAudioOptions(self, audio):

        for item in self.treeWidget_basic_association.selectedItems():
            for takeItem in self.treeData_basic:
                if item.text(0) in takeItem:
                    item.setText(3, audio)
                    takeItem[3] = audio

        self.writeToDataNode()


    ##############################
    ##### advanced tab menus #####
    def showAudioRightClickMenu(self):

        menu = QtGui.QMenu()

        self.addAudio = QtGui.QAction("Start Making Associations", self)
        self.addAudio.triggered.connect(self.addAudioFile)

        self.sortList = QtGui.QAction("Sort List", self)
        self.sortList.triggered.connect(self.sortAudioFiles)

        self.updateList = QtGui.QAction("Update All Lists", self)
        self.updateList.triggered.connect(self.populateAudioList)
        self.updateList.triggered.connect(self.populateCameraList)
        self.updateList.triggered.connect(self.populateTakeList)
        self.updateList.triggered.connect(self.colouriseWidgets)

        self.createBlank = QtGui.QAction("Create *BLANK*", self)
        self.createBlank.triggered.connect(self.createBlankAudioFile)

        self.createCombinedSelect = QtGui.QAction("Create Combined Selection", self)
        self.createCombinedSelect.triggered.connect(self.combinedSelect)

        menu.addAction(self.addAudio)
        menu.addAction(self.sortList)
        menu.addAction(self.updateList)
        menu.addAction(self.createBlank)
        menu.addAction(self.createCombinedSelect)

        menu.exec_(QtGui.QCursor.pos())

    
    def showTakeRightClickMenu(self):

        menu = QtGui.QMenu()

        self.associateTake = QtGui.QAction("Associatiate with Audio File", self)
        self.associateTake.triggered.connect(self.addTakeToAudio)

        self.updateTake = QtGui.QAction("Update All Lists", self)
        self.updateTake.triggered.connect(self.populateAudioList)
        self.updateTake.triggered.connect(self.populateCameraList)
        self.updateTake.triggered.connect(self.populateTakeList)
        self.updateTake.triggered.connect(self.colouriseWidgets)

        self.createCombinedSelect = QtGui.QAction("Create Combined Selection", self)
        self.createCombinedSelect.triggered.connect(self.combinedSelect)

        menu.addAction(self.associateTake)
        menu.addAction(self.updateTake)
        menu.addAction(self.createCombinedSelect)

        menu.exec_(QtGui.QCursor.pos())

    
    def showCameraRightClickMenu(self):

        menu = QtGui.QMenu()

        self.associateCamera = QtGui.QAction("Associatiate with Take or Group", self)
        self.associateCamera.triggered.connect(self.associateCameraWithTake)

        self.updateCamera = QtGui.QAction("Update All Lists", self)
        self.updateCamera.triggered.connect(self.populateAudioList)
        self.updateCamera.triggered.connect(self.populateCameraList)
        self.updateCamera.triggered.connect(self.populateTakeList)
        self.updateCamera.triggered.connect(self.colouriseWidgets)

        self.createCombinedSelect = QtGui.QAction("Create Combined Selection", self)
        self.createCombinedSelect.triggered.connect(self.combinedSelect)

        menu.addAction(self.associateCamera)
        menu.addAction(self.updateCamera)
        menu.addAction(self.createCombinedSelect)

        menu.exec_(QtGui.QCursor.pos())

    
    def showAssociationRightClickMenu(self):

        menu = QtGui.QMenu()
        self.selectAllTakes = QtGui.QAction("Select All", self)
        self.selectAllTakes.triggered.connect(self.selectAssociationItem)
        self.selectNoTakes = QtGui.QAction("De-Select All", self)
        self.selectNoTakes.triggered.connect(self.deSelectAssociationItem)

        self.selectAllTakesInGroup = QtGui.QAction("Select Everything in Group", self)
        self.selectAllTakesInGroup.triggered.connect(self.selectAssociationGroupItem)

        self.selectNoTakesInGroup = QtGui.QAction("De-Select Everything in Group", self)
        self.selectNoTakesInGroup.triggered.connect(self.deSelectAssociationGroupItem)

        self.hudActiveToggle = QtGui.QAction("Make This Take Active", self)
        self.hudActiveToggle.triggered.connect(self.makeThisTakeActive)

        self.autoHud = QtGui.QAction("Automatically Update Hud", self, checkable=True)
        if AutoActivateHud.Active:
            self.autoHud.setChecked(True)
        self.autoHud.triggered.connect(self.hudActiveToggleisChecked)

        self.deleteHuds = QtGui.QAction("Delete All Audio Huds", self)
        self.deleteHuds.triggered.connect(self.deleteAudioRefs)
        self.deleteHuds.triggered.connect(self.deleteAudioHud)

        self.expand = QtGui.QAction("Expand All Cameras", self)
        self.expand.triggered.connect(self.expandAll)

        self.collapse = QtGui.QAction("Collapse All Cameras", self)
        self.collapse.triggered.connect(self.collapseAll)

        self.addOffset = QtGui.QAction("Add Audio Offset", self)
        self.addOffset.triggered.connect(self.addAudioOffsetWindow)

        self.deleteItem = QtGui.QAction("Delete Selected Item", self)
        self.deleteItem.triggered.connect(self.deleteAssociationItem)

        renderOptions = menu.addMenu("Render Selection Options")
        renderOptions.addAction(self.selectAllTakes)
        renderOptions.addAction(self.selectNoTakes)
        renderOptions.addSeparator().setText("")
        renderOptions.addAction(self.selectAllTakesInGroup)
        renderOptions.addAction(self.selectNoTakesInGroup)

        hudOptions = menu.addMenu("Hud Options")
        hudOptions.addAction(self.hudActiveToggle)
        hudOptions.addAction(self.autoHud)
        hudOptions.addAction(self.deleteHuds)

        menu.addSeparator().setText("")
        menu.addAction(self.expand)
        menu.addAction(self.collapse)
        menu.addAction(self.addOffset)
        menu.addAction(self.deleteItem)

        menu.exec_(QtGui.QCursor.pos())

    
    def addAudioOffsetWindow(self):
        # new dialog to add in audio offset (as we can't do this as part of QMenu
        dialogue = AddAudioOffset.AddAudioOffsetWindow()
        dialogue.addAudioOffsetSignal.connect(self.addAudioOffset)
        dialogue.show()
        dialogue.move(QtGui.QCursor.pos())


    def hudActiveCameraisChecked(self):
        # way to handle the AutoActivateHud state and the UI rightclickmenu for Associations
        if AutoAddCamera.Active:
            AutoAddCamera.Active = False
        else:
            AutoAddCamera.Active = True


    def hudAutoSelectAddedTake(self):
        # way to handle the AutoSelectAddedTake state and the UI rightclickmenu for Basic Tab
        if AutoSelectAddedTake.Active:
            AutoSelectAddedTake.Active = False
        else:
            AutoSelectAddedTake.Active = True
            
    
    def hudActiveToggleisChecked(self):
        # way to handle the AutoActivateHud state and the UI rightclickmenu for Associations
        if AutoActivateHud.Active:
            AutoActivateHud.Active = False
            self.deleteAudioHud()
        else:
            AutoActivateHud.Active = True

        self.autoActivateHud()  # this will setup or remove the callback accordingly

    
    def addAudioFile(self):

        audioFile = [item.text().encode("utf-8") for item in self.listWidget_audio.selectedItems()]
        getSelected = self.treeWidget_association.selectedItems()

        audioRoot = self.treeWidget_association.invisibleRootItem()
        child_count = audioRoot.childCount()

        treeItems = []

        if child_count == 0:
            root = QtGui.QTreeWidgetItem(self.treeWidget_association, audioFile)
            root.setIcon(0, self.wavIcon)
            treeItems.append(root.text(0).encode("utf-8"))
            self.treeData.append((audioFile))
            self.treeCamera.append(["audioGroup"])
            self.treeAudioOffset.append(["audioGroup"])

        for i in range(child_count):
            item = audioRoot.child(i)
            treeItems.append(item.text(0).encode("utf-8"))

        if not audioFile[0] in treeItems:
            root = QtGui.QTreeWidgetItem(self.treeWidget_association, audioFile)
            root.setIcon(0, self.wavIcon)
            treeItems.append(root.text(0).encode("utf-8"))
            self.treeData.append(audioFile)
            self.treeCamera.append(["audioGroup"])
            self.treeAudioOffset.append(["audioGroup"])

        self.listWidget_audio.clearSelection()

        self.getCheckState()
        self.writeToDataNode()
        self.colouriseWidgets()

    
    def addTakeToAudio(self):

        selectedTakes = [item.text().encode("utf-8") for item in self.listWidget_take.selectedItems()]
        getSelected = self.treeWidget_association.selectedItems()

        takeRoot = self.treeWidget_association.invisibleRootItem()
        root_count = takeRoot.childCount()

        addedTakes = []
        treeItems = []

        for i in range(root_count):
            item = takeRoot.child(i)
            child_count = item.childCount()
            for i in range(child_count):
                child = item.child(i)
                treeItems.append(child.text(0).encode("utf-8"))

        for item in self.treeWidget_association.selectedItems():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) == None:  # or it has no parent because its a root node (audio file)
                for audioGroup in range(len(self.treeData)):
                    if item.text(0) in self.treeData[audioGroup]:
                        baseNode = getSelected[0]
                        for take in selectedTakes:
                            if not take in treeItems:
                                addedTakes.append(take)
                                audioGroupIndex = self.treeData.index(self.treeData[audioGroup])
                                self.treeData[audioGroup].append(take)
                                self.treeCamera[audioGroup].append([''])
                                self.treeAudioOffset[audioGroup].append("")

                                takeindex = self.treeData[audioGroupIndex].index(take)

                                item = QtGui.QTreeWidgetItem(baseNode, [take])
                                item.setIcon(0, self.animIcon)
                                item.setIcon(1, self.cameraNoIcon)
                                item.setFlags(
                                    item.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)  # code to add the flag
                                item.setCheckState(0, QtCore.Qt.Unchecked)

            self.treeWidget_association.setItemExpanded(getSelected[0],
                                                        True)  # set newly added items expanded just in case the user wanted camera collapsed before adding new take

        self.listWidget_take.clearSelection()

        self.getCheckState()
        self.writeToDataNode()
        self.colouriseWidgets()

    
    def associateCameraWithTake(self):

        selectedCamera = [item.text().encode("utf-8") for item in self.listWidget_camera.selectedItems()]
        getSelected = self.treeWidget_association.selectedItems()

        for ix in self.treeWidget_association.selectedIndexes():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) != None:
                for audioGroup in self.treeData:
                    if ix.data(QtCore.Qt.DisplayRole) in audioGroup:
                        audioGroupIndex = self.treeData.index(audioGroup)
                        cameraIndex = self.treeData[audioGroupIndex].index(getSelected[0].data(0, 0))

                        if not str(selectedCamera[0]) in self.treeCamera[audioGroupIndex][cameraIndex]:
                            if self.treeCamera[audioGroupIndex][cameraIndex][-1] == '':
                                self.treeCamera[audioGroupIndex][cameraIndex][-1] = str(selectedCamera[0])
                            else:
                                self.treeCamera[audioGroupIndex][cameraIndex].append(str(selectedCamera[0]))

                            if len(self.treeCamera[audioGroupIndex][
                                       cameraIndex]) == 2:  # need to delete off the first camera association from adjacent to the take (so we can start a clean branch)
                                # print "len(self.treeCamera[audioGroupIndex][cameraIndex])", len(self.treeCamera[audioGroupIndex][cameraIndex])
                                getSelected[0].setText(1, '')  # clear the text
                                getSelected[0].setIcon(1, QtGui.QIcon())  # set an empty icon

                            if len(self.treeCamera[audioGroupIndex][cameraIndex]) > 1:
                                if getSelected[
                                    0].childCount() == 0:  # need to see if we have deleted off the first camera, but not re-added it as a child
                                    for item in self.treeCamera[audioGroupIndex][cameraIndex]:
                                        takeItem = QtGui.QTreeWidgetItem(getSelected[0], [0, item])
                                        takeItem.setText(1, item)
                                        takeItem.setIcon(1, self.cameraIcon)
                                        # takeItem.setFlags(takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)# code to add the flag
                                        takeItem.setCheckState(1, QtCore.Qt.Unchecked)
                                else:
                                    takeItem = QtGui.QTreeWidgetItem(getSelected[0], [0,
                                                                                      self.treeCamera[audioGroupIndex][
                                                                                          cameraIndex][-1]])
                                    takeItem.setText(1, self.treeCamera[audioGroupIndex][cameraIndex][-1])
                                    takeItem.setIcon(1, self.cameraIcon)
                                    # takeItem.setFlags(takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)# code to add the flag
                                    takeItem.setCheckState(1, QtCore.Qt.Unchecked)
                            else:
                                getSelected[0].setText(1, self.treeCamera[audioGroupIndex][cameraIndex][0])
                                getSelected[0].setIcon(1, self.cameraIcon)

            else:
                for audioGroup in self.treeData:
                    if ix.data(QtCore.Qt.DisplayRole) in audioGroup:
                        audioGroupIndex = self.treeData.index(audioGroup)

                        child_count = getSelected[0].childCount()
                        for i in range(child_count):
                            child = getSelected[0].child(i)

                            if not str(selectedCamera[0]) in self.treeCamera[audioGroupIndex][i + 1]:

                                if self.treeCamera[audioGroupIndex][i + 1][-1] == '':
                                    self.treeCamera[audioGroupIndex][i + 1][-1] = str(selectedCamera[0])
                                else:
                                    self.treeCamera[audioGroupIndex][i + 1].append(str(selectedCamera[0]))

                                if len(self.treeCamera[audioGroupIndex][
                                           i + 1]) == 2:  # need to delete off the first camera association from adjacent to the take (so we can start a clean branch)
                                    child.setText(1, '')  # clear the text
                                    child.setIcon(1, QtGui.QIcon())  # set an empty icon

                                if len(self.treeCamera[audioGroupIndex][i + 1]) > 1:

                                    if child.childCount() == 0:
                                        for item in self.treeCamera[audioGroupIndex][i + 1]:
                                            takeItem = QtGui.QTreeWidgetItem(child, [0, item])
                                            takeItem.setText(1, item)
                                            takeItem.setIcon(1, self.cameraIcon)
                                            # takeItem.setFlags(takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)# code to add the flag
                                            takeItem.setCheckState(1, QtCore.Qt.Unchecked)
                                    else:
                                        takeItem = QtGui.QTreeWidgetItem(child, [0, self.treeCamera[audioGroupIndex][
                                            i + 1][-1]])
                                        takeItem.setText(1, self.treeCamera[audioGroupIndex][i + 1][-1])
                                        takeItem.setIcon(1, self.cameraIcon)
                                        # takeItem.setFlags(takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)# code to add the flag
                                        takeItem.setCheckState(1, QtCore.Qt.Unchecked)

                                else:
                                    child.setText(1, self.treeCamera[audioGroupIndex][i + 1][0])
                                    child.setIcon(1, self.cameraIcon)

                            self.treeWidget_association.setItemExpanded(child,
                                                                        True)  # set newly added items expanded just in case the user wanted camera collapsed before adding new take

        self.getCheckState()
        self.writeToDataNode()
        self.colouriseWidgets()

    
    def combinedSelect(self):
        # make quick selections from the three asset lists (audio, takes, camera)

        if not self.listWidget_audio.selectedItems() or not self.listWidget_take.selectedItems():  # quick test to see if can continue (do we have everything selected?)
            return

        self.addAudioFile()

        takeRoot = self.treeWidget_association.invisibleRootItem()
        root_count = takeRoot.childCount()

        self.treeWidget_association.setCurrentItem(takeRoot.child(root_count - 1), 0)

        self.addTakeToAudio()
        self.associateCameraWithTake()

    
    def addAudioOffset(self, offset):

        getSelected = self.treeWidget_association.selectedItems()

        for ix in self.treeWidget_association.selectedIndexes():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) != None:
                for audioGroup in self.treeData:
                    if audioGroup.__contains__(ix.data(QtCore.Qt.DisplayRole)):

                        audioGroupIndex = self.treeData.index(audioGroup)

                        offsetIndex = self.treeData[audioGroupIndex].index(getSelected[0].data(0, 0))
                        if offset == "0":
                            self.treeAudioOffset[audioGroupIndex][offsetIndex] = ""
                            getSelected[0].setText(2, "")
                            getSelected[0].setIcon(2, QtGui.QIcon())  # set an empty icon
                        else:
                            self.treeAudioOffset[audioGroupIndex][offsetIndex] = str(offset)
                            getSelected[0].setText(2, offset)
                            getSelected[0].setIcon(2, self.offsetIcon)

        self.writeToDataNode()

    
    def sortAudioFiles(self):
        self.listWidget_audio.sortItems()  # ascending by default


    def createBlankAudioFile( self ):
        itemList = [self.listWidget_audio.item(i).text() for i in range(self.listWidget_audio.count())]

        if not "*BLANK*" in itemList:
            self.listWidget_audio.addItem(str("*BLANK*"))


    def getParents(self, item):
        """
        Return a list containing all parent items of this item.
        The list is empty, if the item is a root item.
        """
        parents = []
        current_item = item
        current_parent = current_item.parent()

        # Walk up the tree and collect all parent items of this item
        while not current_parent is None:
            parents.append(current_parent)
            current_item = current_parent
            current_parent = current_item.parent()
        return parents

    
    def deleteAssociationItem(self):

        getSelected = self.treeWidget_association.selectedItems()
        associationRoot = self.treeWidget_association.invisibleRootItem()

        self.parents = []

        for item in self.treeWidget_association.selectedItems():
            self.parents = self.getParents(item)

        if len(self.parents) == 1:

            takeName = getSelected[0].data(0, QtCore.Qt.DisplayRole)

            for group in range(len(self.treeData)):

                if self.parents[0].data(0, QtCore.Qt.DisplayRole) == self.treeData[group][0]:

                    takeIndex = self.treeData[group].index('{}'.format(takeName))

                    del self.treeData[group][takeIndex]
                    del self.treeCamera[group][takeIndex]
                    del self.treeAudioOffset[group][takeIndex]

                    for item in self.treeWidget_association.selectedItems():
                        (item.parent() or associationRoot).removeChild(item)

                    self.getCheckState()
                    self.writeToDataNode()
                    self.colouriseWidgets()
                    return

        for ix in self.treeWidget_association.selectedIndexes():

            node = self.treeWidget_association.currentIndex()

            if (node.parent()).data(QtCore.Qt.DisplayRole) == None:

                for group in range(len(self.treeData)):

                    if node.data(QtCore.Qt.DisplayRole) == self.treeData[group][0]:
                        del self.treeData[group]
                        del self.treeCamera[group]
                        del self.treeAudioOffset[group]

                        for item in self.treeWidget_association.selectedItems():
                            (item.parent() or associationRoot).removeChild(item)

                        self.getCheckState()
                        self.writeToDataNode()
                        self.colouriseWidgets()
                        return

            elif getSelected[0].childCount() == 0:

                takeItem = node.parent()
                takeName = takeItem.data(QtCore.Qt.DisplayRole)

                audioGroupItem = takeItem.parent()
                audioGroupName = audioGroupItem.data(QtCore.Qt.DisplayRole)

                cameraName = getSelected[0].data(1, QtCore.Qt.DisplayRole)

                for group in range(len(self.treeData)):

                    if audioGroupName == self.treeData[group][0]:

                        takeIndex = self.treeData[group].index('{}'.format(takeName))
                        cameraIndex = self.treeCamera[group][takeIndex].index('{}'.format(cameraName))

                        del self.treeCamera[group][takeIndex][cameraIndex]

                        for item in self.treeWidget_association.selectedItems():
                            parent = item.parent()
                            (item.parent() or associationRoot).removeChild(item)

                        if len(self.treeCamera[group][takeIndex]) == 1:
                            parent.removeChild(parent.child(0))
                            parent.setText(1, self.treeCamera[group][takeIndex][0])
                            parent.setIcon(1, self.cameraIcon)

                        self.getCheckState()
                        self.writeToDataNode()
                        self.colouriseWidgets()
                        return

    
    def expandAll(self):
        self.treeWidget_association.expandToDepth(1)

    
    def collapseAll(self):
        self.treeWidget_association.expandToDepth(0)

    
    @QtCore.Slot(QtGui.QTreeWidgetItem, int)
    def onClickItem(self, it, col):
        '''
            callback when an item is clicked on the treewidget (we need to do some checking and unchecking of checkboxes) 
        '''
        parents = self.getParents(it)

        if len(parents) == 0:
            return

        if len(parents) == 2:
            if it.checkState(1):
                if not parents[0].checkState(0):
                    parents[0].setCheckState(0, QtCore.Qt.Checked)

        child_count = it.childCount()

        if child_count > 1:  # lets use the checkbox of a take with multiple cameras as a way to select/deselect all of its children directly
            if it.checkState(0):
                for i in range(child_count):
                    child = it.child(i)
                    child.setCheckState(1, QtCore.Qt.Checked)
            else:
                for i in range(child_count):
                    child = it.child(i)
                    child.setCheckState(1, QtCore.Qt.Unchecked)

        if it.data(1, QtCore.Qt.DisplayRole) == '' or it.data(1,
                                                              QtCore.Qt.DisplayRole) == None:  # no camera? no selection
            if not it.child(0):  # if it doesn't have a child there is definately no camera
                it.setCheckState(0, QtCore.Qt.Unchecked)

        self.getCheckState()

    
    def getCheckState(self):
        # time to store all the checked checkboxes 
        self.treeCheckState = []

        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidget_association)

        while iterator.value():
            item = iterator.value()

            column0Value = item.checkState(0)
            column1Value = item.checkState(1)

            if item.checkState(0):
                column0Value = True
            else:
                column0Value = False

            if item.checkState(1):
                column1Value = True
            else:
                column1Value = False

            self.treeCheckState.append([column0Value, column1Value])
            iterator += 1

        self.writeToDataNode()

    
    def setCheckState(self):
        # restore checkstate after tool re-open
        if not self.treeCheckState:
            return

        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidget_association)

        count = 0
        while iterator.value():
            item = iterator.value()

            if eval(self.treeCheckState[count][0]):
                item.setCheckState(0, QtCore.Qt.Checked)

            if eval(self.treeCheckState[count][1]):
                item.setCheckState(1, QtCore.Qt.Checked)

            iterator += 1
            count += 1

    
    def selectAssociationItem(self):
        # this handles the 'select all' option
        getSelected = self.treeWidget_association.selectedItems()
        associationRoot = self.treeWidget_association.invisibleRootItem()
        root_count = associationRoot.childCount()

        for audioGroup in range(root_count):
            take = associationRoot.child(audioGroup)
            child_count = take.childCount()
            for camera in range(child_count):
                takeChild = take.child(camera)
                takeChild_count = takeChild.childCount()

                if takeChild.data(1, QtCore.Qt.DisplayRole) != None and takeChild.data(1,
                                                                                       QtCore.Qt.DisplayRole) != '':  # select takes with no children
                    takeChild.setCheckState(0, QtCore.Qt.Checked)

                if takeChild_count > 1:  # select takes with children
                    for i in range(takeChild_count):
                        takeChild.setCheckState(0, QtCore.Qt.Checked)
                        childCamera = takeChild.child(i)
                        childCamera.setCheckState(1, QtCore.Qt.Checked)

        self.getCheckState()

    
    def deSelectAssociationItem(self):
        # this handles the 'deselect all' option
        getSelected = self.treeWidget_association.selectedItems()
        associationRoot = self.treeWidget_association.invisibleRootItem()
        root_count = associationRoot.childCount()

        for audioGroup in range(root_count):
            take = associationRoot.child(audioGroup)
            child_count = take.childCount()
            for camera in range(child_count):
                takeChild = take.child(camera)
                takeChild_count = takeChild.childCount()

                if takeChild.data(1, QtCore.Qt.DisplayRole) != None and takeChild.data(1,
                                                                                       QtCore.Qt.DisplayRole) != '':  # deselect takes with no children
                    takeChild.setCheckState(0, QtCore.Qt.Unchecked)

                if takeChild_count > 1:  # deselect takes with children
                    for i in range(takeChild_count):
                        takeChild.setCheckState(0, QtCore.Qt.Unchecked)
                        childCamera = takeChild.child(i)
                        childCamera.setCheckState(1, QtCore.Qt.Unchecked)

        self.getCheckState()

    
    def selectAssociationGroupItem(self):
        # this handles the 'select all' items in an audio group option
        getSelected = self.treeWidget_association.selectedItems()

        for ix in self.treeWidget_association.selectedIndexes():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) == None:
                takeChild_count = getSelected[0].childCount()
                for takes in range(takeChild_count):
                    takeChild = getSelected[0].child(takes)
                    takeChild_count = takeChild.childCount()
                    if takeChild.data(1, QtCore.Qt.DisplayRole) != None and takeChild.data(1,
                                                                                           QtCore.Qt.DisplayRole) != '':
                        takeChild.setCheckState(0, QtCore.Qt.Checked)
                    if takeChild_count > 1:  # select multiple cameras associated with take
                        for i in range(takeChild_count):
                            takeChild.setCheckState(0, QtCore.Qt.Checked)
                            childCamera = takeChild.child(i)
                            childCamera.setCheckState(1, QtCore.Qt.Checked)

        self.getCheckState()

    
    def deSelectAssociationGroupItem(self):
        # this handles the 'deselect all' items in an audio group option
        getSelected = self.treeWidget_association.selectedItems()

        for ix in self.treeWidget_association.selectedIndexes():
            getRole = self.treeWidget_association.currentIndex().parent()
            if getRole.data(QtCore.Qt.DisplayRole) == None:
                takeChild_count = getSelected[0].childCount()
                for takes in range(takeChild_count):
                    takeChild = getSelected[0].child(takes)
                    takeChild_count = takeChild.childCount()
                    if takeChild.data(1, QtCore.Qt.DisplayRole) != None and takeChild.data(1,
                                                                                           QtCore.Qt.DisplayRole) != '':
                        takeChild.setCheckState(0, QtCore.Qt.Unchecked)
                    if takeChild_count > 1:  # deselect multiple cameras associated with take
                        for i in range(takeChild_count):
                            takeChild.setCheckState(0, QtCore.Qt.Unchecked)
                            childCamera = takeChild.child(i)
                            childCamera.setCheckState(1, QtCore.Qt.Unchecked)

        self.getCheckState()


    def populateAllLists(self):
        audioFiles = []
        audioFiles = [sceneAudio.Namespace for sceneAudio in self.Manager.GetReferenceListByType(Audio)]
        cameras = Globals.System.Scene.Cameras

        self.listWidget_basic_take.clear()
        self.listWidget_basic_camera.clear()
        self.listWidget_audio.clear()
        self.listWidget_take.clear()
        self.listWidget_camera.clear()

        for take in RS.Globals.Scene.Takes:
            self.listWidget_basic_take.addItem(str(take.Name))
            self.listWidget_take.addItem(str(take.Name))

        for audioFile in audioFiles:
            self.listWidget_audio.addItem(str(audioFile))

        for camera in cameras:
            self.listWidget_basic_camera.addItem(str(camera.Name))
            self.listWidget_camera.addItem(str(camera.Name))


    def populateTakeList(self):
        """
        refresh the take lists
        """
        # clear out basic listWidget if it is populated
        itemList = [self.listWidget_basic_take.item(i) for i in range(self.listWidget_basic_take.count())]
        for item in range(len(itemList)):
            self.listWidget_basic_take.takeItem(self.listWidget_basic_take.row(itemList[item]))

        for take in RS.Globals.Scene.Takes:
            self.listWidget_basic_take.addItem(str(take.Name))
            
        # clear out listWidget if it is populated
        itemList = [self.listWidget_take.item(i) for i in range(self.listWidget_take.count())]
        for item in range(len(itemList)):
            self.listWidget_take.takeItem(self.listWidget_take.row(itemList[item]))

        for take in RS.Globals.Scene.Takes:
            self.listWidget_take.addItem(str(take.Name))

    
    def populateAudioList(self):
        audioFiles = []
        audioFiles = [sceneAudio.Namespace for sceneAudio in self.Manager.GetReferenceListByType(Audio)]

        if not audioFiles:
            return

        # clear out listWidget if it is populated
        itemList = [self.listWidget_audio.item(i) for i in range(self.listWidget_audio.count())]
        for item in range(len(itemList)):
            self.listWidget_audio.takeItem(self.listWidget_audio.row(itemList[item]))

        for audioFile in audioFiles:
            self.listWidget_audio.addItem(str(audioFile))

        # mute audio for the referenced in audio files (new instances will default to the users normal output)    
        for lComp in Globals.System.Scene.Components:
            for audioFile in audioFiles:
                if re.search('{0}:{0}.{1}'.format(audioFile, 'wav'), lComp.LongName,
                             re.IGNORECASE):  # regexp search ftw # make it case insensitive
                    for lProp in lComp.PropertyList:
                        if lProp.Name == 'Destination':
                            lProp.Data = 0

        self.sortAudioFiles()


    def populateCameraList(self):

        currentCam = Globals.System.Scene.Renderer.CurrentCamera

        cameras = Globals.System.Scene.Cameras

        # clear out basic_listWidget if it is populated
        itemList = [self.listWidget_basic_camera.item(i) for i in range(self.listWidget_basic_camera.count())]
        for item in range(len(itemList)):
            self.listWidget_basic_camera.takeItem(self.listWidget_basic_camera.row(itemList[item]))

        for camera in cameras:
            self.listWidget_basic_camera.addItem(str(camera.Name))

        itemList = [self.listWidget_basic_camera.item(i) for i in range(self.listWidget_basic_camera.count())]
        for item in range(len(itemList)):
            if self.listWidget_basic_camera.item(item).text() == currentCam.Name:
                self.listWidget_basic_camera.item(item).setSelected(True)

        # clear out listWidget if it is populated
        itemList = [self.listWidget_camera.item(i) for i in range(self.listWidget_camera.count())]
        for item in range(len(itemList)):
            self.listWidget_camera.takeItem(self.listWidget_camera.row(itemList[item]))

        for camera in cameras:
            self.listWidget_camera.addItem(str(camera.Name))

        itemList = [self.listWidget_camera.item(i) for i in range(self.listWidget_camera.count())]
        for item in range(len(itemList)):
            if self.listWidget_camera.item(item).text() == currentCam.Name:
                self.listWidget_camera.item(item).setSelected(True)

    
    def populateRenderOptions(self):

        RenderSettings.data.append(["Default", "2", "1280", "720", "0", "-10", "0", "3.0", "2", "2", "1"])

        stockPictureFormats = ["3840 * 2160 (4K)", "1920 * 1080 (HD1080)", "1280 * 720 (HD720)", "720 * 480 (NTSCDVD)",
                               "640 * 480 (PALDVD)"]
        preferredFonts = ["Arial", "Helvetica", "Bookman Old Style", "Times New Roman", "Verdana", "Palatino",
                          "Garamond", "Tahoma"]
        availaileFonts = []

        justification = ["Left", "Right", "Center"]
        horizontal = ["Left", "Right", "Center"]
        vertical = ["Bottom", "Top", "Center"]

        for item in stockPictureFormats:
            self.comboBox_pictureFormat.addItem(item)

        # create a temp hud to get the font list (available fonts might vary from PC to PC?)
        HUDTextElement = mobu.FBHUDTextElement("Audio Filepath")

        for item in HUDTextElement.GetFontList():
            if item in preferredFonts:
                availaileFonts.append(item)

        self.deleteAudioHud()  # delete the hud we generated specifically to find the available fonts

        availaileFonts = sorted(availaileFonts,
                                key=preferredFonts.index)  # sort list of availiable fonts in order according to the preferred fonts list

        for item in availaileFonts:
            self.comboBox_font.addItem(item)  # add the sorted available fonts to the combobox

        for item in justification:
            self.comboBox_justification.addItem(item)

        for item in horizontal:
            self.comboBox_horizontalDock.addItem(item)

        for item in vertical:
            self.comboBox_verticalDock.addItem(item)

        self.setRenderOptions(0)
        self.comboBox_customRenderSettings.addItem(RenderSettings.data[0][0])

    
    def setRenderOptions(self, index):
        # read the items from the class object to change the values accordingly

        self.lineEdit_customRenderSettings.setText(RenderSettings.data[index][0])
        self.comboBox_pictureFormat.setCurrentIndex(int(RenderSettings.data[index][1]))
        self.spinBox_customWidth.setValue(int(RenderSettings.data[index][2]))
        self.spinBox_customHeight.setValue(int(RenderSettings.data[index][3]))
        self.spinBox_hud_X.setValue(int(RenderSettings.data[index][4]))
        self.spinBox_hud_Y.setValue(int(RenderSettings.data[index][5]))
        self.comboBox_font.setCurrentIndex(int(RenderSettings.data[index][6]))
        self.doubleSpinBox_fontScale.setValue(float(RenderSettings.data[index][7]))
        self.comboBox_justification.setCurrentIndex(int(RenderSettings.data[index][8]))
        self.comboBox_horizontalDock.setCurrentIndex(int(RenderSettings.data[index][9]))
        self.comboBox_verticalDock.setCurrentIndex(int(RenderSettings.data[index][10]))

    
    def findCustomSettings(self):
        index = self.comboBox_customRenderSettings.currentIndex()
        self.setRenderOptions(index)

    
    def updateCustomFormat(self):
        stockPictureFormats = self.comboBox_pictureFormat.currentText().split(" ")
        self.spinBox_customWidth.setValue(int(stockPictureFormats[0]))
        self.spinBox_customHeight.setValue(int(stockPictureFormats[2]))

    
    def colouriseWidgets(self):

        # colourise the tree and audio lists
        for index, take in enumerate(RS.Globals.Scene.Takes):
            if any(str(take.Name) in item for item in self.treeData):  # find any match within the nested lists
                self.listWidget_take.item(index).setBackground(
                    QtGui.QColor(0, 255, 0, 100))  # we can simply use index as we are never sorting this list
            else:
                self.listWidget_take.item(index).setBackground(QtGui.QColor(255, 0, 0, 150))

        audioFiles = []
        audioFiles = [sceneAudio.Namespace for sceneAudio in self.Manager.GetReferenceListByType(Audio)]

        for audioFile in audioFiles:
            if any(audioFile in item for item in self.treeData):  # find any match within the nested lists
                items = self.listWidget_audio.findItems(audioFile, QtCore.Qt.MatchContains)  # we need to find the index by matching the name, this gives us the actual row in the listWidget
                self.listWidget_audio.item(self.listWidget_audio.row(items[0])).setBackground(
                    QtGui.QColor(0, 255, 0, 100))
            else:
                items = self.listWidget_audio.findItems(audioFile, QtCore.Qt.MatchContains)
                if items:
                    self.listWidget_audio.item(self.listWidget_audio.row(items[0])).setBackground(
                        QtGui.QColor(255, 0, 0, 150))

    
    def deselectComponents(self):
        for comp in RS.Globals.Scene.Components:
            comp.Selected = False

    
    def makeThisTakeActive(self):
        # we get a list of everything in the row, but we only want the first item, so we'll add them to a list an grab the first index
        # eventually we can use this to grab all the data we need for the camera and the offset
        takeData = []

        getSelected = self.treeWidget_association.selectedItems()
        if getSelected:
            for ix in self.treeWidget_association.selectedIndexes():
                takeData.append(ix.data(QtCore.Qt.DisplayRole))
                audioRef = ix.parent()
                audioRefName = audioRef.data(QtCore.Qt.DisplayRole)

        for take in RS.Globals.Scene.Takes:
            if takeData[0] == take.Name:
                Globals.System.CurrentTake = take
                self.applyAudiohud(audioRefName, takeData[0], takeData[1], takeData[2])


    def autoActivateHud(self):
        # manage the callback for take change.
        if AutoActivateHud.Active:
            Globals.Callbacks.OnTakeChange.Add(self.onTakeChanged)
            Globals.Callbacks.OnTakeChange.Remove(self.generalTakeChanged)
        else:
            Globals.Callbacks.OnTakeChange.Remove(self.onTakeChanged)
            Globals.Callbacks.OnTakeChange.Add(self.generalTakeChanged)


    def onTakeChanged(self, source, event):
        # get rid of huds if there is no association - can't have this any other way, as it just kills the hud?
        self.deleteAudioRefs()
        self.deleteAudioHud()

        take = Globals.System.CurrentTake
        for audioGroup in range(len(self.treeData)):
            if take.Name in self.treeData[audioGroup]:
                audioGroupIndex = self.treeData.index(self.treeData[audioGroup])
                takeindex = self.treeData[audioGroupIndex].index(take.Name)

                audioRefName = self.treeData[audioGroup][0]
                cameraName = self.treeCamera[audioGroup][takeindex]
                audioOffset = self.treeAudioOffset[audioGroup][takeindex]

                self.applyAudiohud(audioRefName, take.Name, cameraName, audioOffset)


    def applyAudiohud(self, audioRefName, takeName, cameraName, offsetAmount):

        if audioRefName != "*BLANK*":
            audioRefModel = mobu.FBFindModelByLabelName('{0}:{1}'.format('RS_Null', audioRefName))
            audioRefPath = audioRefModel.PropertyList.Find('Reference Path')
            self.deleteAudioRefs()
            self.deleteAudioHud()
            self.createHud(audioRefPath.Data, cameraName)
            self.applyAudio(audioRefName, takeName, audioRefPath.Data, offsetAmount)
        else:
            self.deleteAudioRefs()
            self.deleteAudioHud()
            self.getPaneCameras(cameraName)


    def applyAudio(self, audioRefName, takeName, audioRefPath, offsetAmount):
        # apply audio to take
        for take in Globals.System.Scene.Takes:
            if take.Name == takeName:
                Globals.System.CurrentTake = take

        lAudio = mobu.FBAudioClip(audioRefPath)
        lAudio.LongName = '{0}:{1}'.format(lAudio.LongName, 'AudioRef')

        if not offsetAmount == None and not offsetAmount == "":
            InPoint = lAudio.PropertyList.Find('InPoint')
            InPoint.Data = mobu.FBTime(0, 0, 0, int(offsetAmount))

    
    def deleteAudioRefs(self):
        # delete out any existing temp audio instances
        for lComp in Globals.System.Scene.Components:
            if re.search(str(".wav:AudioRef"), lComp.LongName,
                         re.IGNORECASE):  # regexp search ftw # make it case insensitive
                lComp.FBDelete()

        self.deleteStrayAudioData()

    
    def deleteAudioHud(self):

        deleteHuds = []
        deleteElements = []
        cameras = Globals.System.Scene.Cameras

        # delete the huds
        for camera in cameras:
            for camHud in camera.HUDs:
                if camHud.Name.startswith("IgRenderHud"):
                    deleteHuds.append(camHud)
                for hudElement in camHud.Elements:
                    if hudElement.Name.startswith("IgRenderAudio"):
                        deleteElements.append(hudElement)
        deleteList = deleteHuds + deleteElements

        for deleteItem in deleteList:
            deleteItem.FBDelete()

    
    def deleteStrayAudioData(self):
        # deleting the left over audio data that seems to not be connected to an audio file?
        deleteStrayAudio = []
        for lComp in Globals.System.Scene.Components:
            if 'Audio Filepath' in lComp.LongName:
                deleteStrayAudio.append(lComp)

        for strayAudioData in deleteStrayAudio:
            strayAudioData.FBDelete()


    def getPaneCameras(self, cameraName):
        
        cameras = Globals.System.Scene.Cameras

        for camera in cameras:
            if camera.Name == cameraName:
                Globals.System.Renderer.CurrentCamera = camera

        currentCam = Globals.System.Renderer.CurrentCamera

        # nifty code to figure out if we are using multiple panes (aka viewports) and deal with that (we need to set the current pane to the desired camera)
        self.paneCameras = []

        for paneCam in range(4):
            paneCamera = Globals.System.Scene.Renderer.GetCameraInPane(paneCam).Name
            self.paneCameras.append(paneCamera)
            Globals.System.Scene.Renderer.SetCameraInPane(currentCam, paneCam)

    
    def createHud(self, hudText, cameraName, basic=False):
        
        self.getPaneCameras(cameraName)
        
        cameras = Globals.System.Scene.Cameras

        for camera in cameras:
            if camera.Name == cameraName:
                Globals.System.Renderer.CurrentCamera = camera

        currentCam = Globals.System.Renderer.CurrentCamera

        camHuds = list(currentCam.HUDs)
        if not camHuds:
            currentHud = mobu.FBHUD("IgRenderHud")
            Globals.System.Scene.ConnectSrc(currentHud)
        else:
            currentHud = camHuds[0]

        if basic:
            HUDTextElement = mobu.FBHUDTextElement("Production Stage Subtitle")
            HUDTextElement.Content = hudText
            HUDTextElement.X = 0
            HUDTextElement.Y = 10
            HUDTextElement.Font = "Tahoma"
            HUDTextElement.Height = 6
            HUDTextElement.Scale = 2.5
            HUDTextElement.Justification = mobu.FBHUDElementHAlignment.kFBHUDCenter
            HUDTextElement.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDCenter
            HUDTextElement.VerticalDock = mobu.FBHUDElementVAlignment.kFBHUDBottom
            HUDTextElement.PropertyList.Find('Foreground Color').Data = mobu.FBColorAndAlpha(1, 0, 0, 1)
        else:
            HUDTextElement = mobu.FBHUDTextElement("Audio Filepath")
            HUDTextElement.Content = hudText
            HUDTextElement.X = self.spinBox_hud_X.value()
            HUDTextElement.Y = self.spinBox_hud_Y.value()
            HUDTextElement.Font = str(self.comboBox_font.currentText())
            HUDTextElement.Height = float(self.doubleSpinBox_fontScale.text())

            if str(self.comboBox_justification.currentText()) == "Left":
                HUDTextElement.Justification = mobu.FBHUDElementHAlignment.kFBHUDLeft
            elif str(self.comboBox_justification.currentText()) == "Right":
                HUDTextElement.Justification = mobu.FBHUDElementHAlignment.kFBHUDRight
            elif str(self.comboBox_justification.currentText()) == "Center":
                HUDTextElement.Justification = mobu.FBHUDElementHAlignment.kFBHUDCenter
    
            if str(self.comboBox_horizontalDock.currentText()) == "Left":
                HUDTextElement.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDLeft
            elif str(self.comboBox_horizontalDock.currentText()) == "Right":
                HUDTextElement.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDRight
            elif str(self.comboBox_horizontalDock.currentText()) == "Center":
                HUDTextElement.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDCenter
    
            if str(self.comboBox_verticalDock.currentText()) == "Bottom":
                HUDTextElement.VerticalDock = mobu.FBHUDElementVAlignment.kFBHUDBottom
            elif str(self.comboBox_verticalDock.currentText()) == "Top":
                HUDTextElement.VerticalDock = mobu.FBHUDElementVAlignment.kFBHUDTop
            elif str(self.comboBox_verticalDock.currentText()) == "Center":
                HUDTextElement.VerticalDock = mobu.FBHUDElementVAlignment.kFBHUDCenter

        currentHud.ConnectSrc(HUDTextElement)
        currentCam.ConnectSrc(currentHud)


    def startRender(self):
        """
        Choose the basic or advanced render options.
        """
        ext = ''
        for renderDest in range(len(self.renderDestRadioButtons)):
            if self.renderDestRadioButtons[renderDest].isChecked():
                if self.renderToMovCheckboxes[renderDest].isChecked():
                    ext = '.mov'
                else:
                    ext = '.mp4'

        if self.tabWidget_tasks.currentIndex() == 0:
            self.startBasicRender(ext)
        else:
            self.startAdvancedRender(ext)


    def inspectRenderTasks(self, takeToRender, renderCamera, audioRef):
        """
        method to test render tasks and report back issues.
        If any issues are found, the render process will end.
        :param takeToRender:
        :param renderCamera:
        :param audioRef:
        :return:
        """
        errors = []
        sceneTakes = []

        for take in Globals.System.Scene.Takes:
            sceneTakes.append(take.Name)

        for take in takeToRender:
            if take not in sceneTakes:
                errors.append('{0} {1} {2}'.format('Take does not exist: ', take, '\n'))

        if errors:
            errors.append('\n No renders have been made. Please fix these issues before trying again.')
            message = ''.join(errors)
            QtGui.QMessageBox.warning(None, "Render errors:", message)
            return False
        else:
            return True


    def startBasicRender(self, ext):
        """
        A simplified method to tackle the basic render task.
        """
        takeToRender = []
        renderCamera = []
        subtitle = []
        audioRef = []
        
        self.deselectComponents()

        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidget_basic_association)

        while iterator.value():
            item = iterator.value()

            column0Value = item.checkState(0)

            if item.checkState(0):
                #print item.text(0), "checked"
                if item.text(1) != '':
                    takeToRender.append('{}'.format(item.text(0)))
                    renderCamera.append('{}'.format(item.text(1)))
                if item.text(2) == 'None':
                    subtitle.append('')
                else:
                    subtitle.append('{}'.format(item.text(2)))
                if item.text(3) == '':
                    audioRef.append('')
                else:  
                    audioRef.append('{}'.format(item.text(3)))
            iterator += 1

        if not self.inspectRenderTasks(takeToRender, renderCamera, audioRef):
            print 'error'
            return

        for renderTask in range(len(takeToRender)):
            takeName = takeToRender[renderTask]
            cameraName = renderCamera[renderTask]
            subtitleText = subtitle[renderTask]
            audioRefName = audioRef[renderTask]
            
            for take in Globals.System.Scene.Takes:
                if take.Name == takeName:
                    Globals.System.CurrentTake = take
                    self.deleteAudioHud()
                    self.createHud(subtitleText, cameraName, basic=True)
                    
                    if audioRefName != '':# apply audio if we need to
                        self.deleteAudioRefs()
                        audioRefModel = mobu.FBFindModelByLabelName('{0}:{1}'.format('RS_Null', audioRefName))
                        audioRefPath = audioRefModel.PropertyList.Find('Reference Path')
                        self.applyAudio(audioRefName, takeName, audioRefPath.Data, offsetAmount=0)
                    
                    saveName = self.createRenderName(takeName)
                    self.render(saveName, take, ext)

        self.deleteAudioRefs()
        self.deleteAudioHud()


    def startAdvancedRender(self, ext):
        """
        The advanced render options.
        Which do not have the subtitles, but will incorporate the audio file location as an overlay.
        """
        self.deselectComponents()

        associationRoot = self.treeWidget_association.invisibleRootItem()
        root_count = associationRoot.childCount()

        takeToRender = []
        renderCamera = []
        audioOffset = []

        for audioRoot in range(root_count):
            takeToRender.append([])
            renderCamera.append([])
            audioOffset.append([])
            takeToRender[audioRoot].append(self.treeData[audioRoot][0])
            renderCamera[audioRoot].append(self.treeCamera[audioRoot][0])
            audioOffset[audioRoot].append(self.treeAudioOffset[audioRoot][0])

            item = associationRoot.child(audioRoot)
            child_count = item.childCount()
            for animFile in range(child_count):
                animFileChild = item.child(animFile)
                if animFileChild.checkState(0):
                    if animFileChild.childCount() > 1:
                        cameraList_count = animFileChild.childCount()
                        selectedCameras = []
                        for cameraList in range(cameraList_count):
                            cameraChild = animFileChild.child(cameraList)
                            if cameraChild.checkState(1):
                                selectedCameras.append(
                                    '{0}{1}'.format("~sub~", cameraChild.data(1, QtCore.Qt.DisplayRole)))

                        if selectedCameras:
                            takeToRender[audioRoot].append(self.treeData[audioRoot][animFile + 1])
                            renderCamera[audioRoot].append(selectedCameras)
                            audioOffset[audioRoot].append(self.treeAudioOffset[audioRoot][animFile + 1])
                    else:
                        if self.treeCamera[audioRoot][animFile + 1] != ['']:
                            takeToRender[audioRoot].append(self.treeData[audioRoot][animFile + 1])
                            renderCamera[audioRoot].append(self.treeCamera[audioRoot][animFile + 1])
                            audioOffset[audioRoot].append(self.treeAudioOffset[audioRoot][animFile + 1])
                        else:
                            animFileChild.setCheckState(0, QtCore.Qt.Unchecked)
                            QtGui.QMessageBox.warning(None, "Selection error:",
                                                      "There were anims selected to render that had no camera allocated?\nThese have been de-selected.")

        takesToRender = takeToRender[0][:]

        if not self.inspectRenderTasks(takesToRender[1:], None, None):
            print 'error'
            return

        for audioGroup in range(len(takeToRender)):
            if len(takeToRender[audioGroup]) != 1:
                audioRefName = takeToRender[audioGroup][0]
                for take in range(1, len(takeToRender[audioGroup])):
                    takeName = takeToRender[audioGroup][take]
                    cameraName = renderCamera[audioGroup][take]
                    offsetAmount = audioOffset[audioGroup][take]

                    for takes in Globals.System.Scene.Takes:
                        if takes.Name == takeName:
                            Globals.System.CurrentTake = takes
                            if len(cameraName) > 1:
                                for subCamera in cameraName:
                                    subCamera = subCamera.replace("~sub~", "")
                                    self.applyAudiohud(audioRefName, takeName, subCamera, offsetAmount)
                                    cameraName = subCamera.replace(" ", "_")
                                    saveName = self.createRenderName('{0}_{1}'.format(takeName, cameraName))
                                    self.render(saveName, takes, ext)
                            else:
                                if "~sub~" in cameraName[0]:
                                    subCamera = cameraName[0].replace("~sub~", "")
                                    self.applyAudiohud(audioRefName, takeName, subCamera, offsetAmount)
                                    cameraName = subCamera.replace(" ", "_")
                                    saveName = self.createRenderName('{0}_{1}'.format(takeName, cameraName))
                                    self.render(saveName, takes, ext)
                                else:
                                    self.applyAudiohud(audioRefName, takeName, cameraName, offsetAmount)
                                    saveName = self.createRenderName(takeName)
                                    if saveName:
                                        self.render(saveName, takes, ext)

        if self.checkBox_videoHelper.isChecked():
            try:
                Tools.openVideoHelper()
            except Exception, e:
                QtGui.QMessageBox.warning(None, "Video Helper error:",
                                          "There was an issue opening the Video Helper:\n\n" + str(e))

        self.deleteAudioRefs()
        self.deleteAudioHud()

    
    def createRenderName(self, userTakeName):
        # lets sort out the render filename
        if self.lineEdit_prefix_suffix.text() != "":
            if self.radioButton_prefix.isChecked():
                userTakeName = self.lineEdit_prefix_suffix.text() + userTakeName
            else:
                userTakeName = userTakeName + self.lineEdit_prefix_suffix.text()

        if self.checkBox_upper.isChecked():
            userTakeName = userTakeName.upper()
        else:
            userTakeName = userTakeName.lower()

        if not self.lineEdit_outputPath.text().endswith('\\'):
            self.lineEdit_outputPath.setText('{0}{1}'.format(self.lineEdit_outputPath.text(), '\\'))

        saveName = self.lineEdit_outputPath.text() + userTakeName

        if not os.path.isdir(self.lineEdit_outputPath.text()):
            try:
                os.makedirs(self.lineEdit_outputPath.text())
            except Exception, e:
                QtGui.QMessageBox.warning(None, "Output Path Error:",
                                          "There was an issue creating the output path:\n\n" + str(e))
                return False

        return saveName

    
    def render(self, saveName, takeToRender, ext):

        if self.checkBox_checkOut.isChecked():
            p4File = '{0}{1}'.format(saveName, '.mov')
            Tools.perforceFileCheck(p4File)

        startFrame = Globals.System.CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        endFrame = Globals.System.CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        mgr = mobu.FBVideoCodecManager()
        mgr.SetDefaultCodec("mov", "mov")  # make sure we're using the .mov codec
        mgr.VideoCodecMode = mobu.FBVideoCodecMode.FBVideoCodecStored
        app = mobu.FBApplication()

        lCamera = Globals.System.Renderer.CurrentCamera

        lResolution = lCamera.PropertyList.Find('Resolution')
        lWidth = lCamera.PropertyList.Find('Picture Width')
        lHeight = lCamera.PropertyList.Find('Picture Height')
        lPixel = lCamera.PropertyList.Find('Pixel Ratio')

        lResolution.SetLocked(False)
        lWidth.SetLocked(False)
        lHeight.SetLocked(False)
        lPixel.SetLocked(False)

        lCamera.FrameSizeMode = mobu.FBCameraFrameSizeMode.kFBFrameSizeFixedResolution
        lCamera.ResolutionWidth = int(self.spinBox_customWidth.value())
        lCamera.ResolutionHeight = int(self.spinBox_customHeight.value())
        lCamera.PixelAspectRatio = 1

        videoOptions = mobu.FBVideoGrabber().GetOptions()
        videoOptions.FrameSizeMode = mobu.FBCameraFrameSizeMode.kFBFrameSizeFixedResolution
        videoOptions.CameraResolution = mobu.FBCameraResolutionMode.kFBResolutionCustom

        videoOptions.BitsPerPixel = mobu.FBVideoRenderDepth.FBVideoRender32Bits
        videoOptions.ShowTimeCode = True
        videoOptions.RenderAudio = True
        videoOptions.AudioRenderFormat = 2113538  # Code for 48K
        captureRange = mobu.FBTimeSpan()
        captureRange.Set(mobu.FBTime(0, 0, 0, startFrame), mobu.FBTime(0, 0, 0, endFrame))
        videoOptions.TimeSteps = mobu.FBTime(0, 0, 0, 1)
        videoOptions.TimeSpan = captureRange

        saveName = '{0}_{1}'.format(saveName, 'out.mov')

        videoOptions.OutputFileName = saveName

        # delete any files with existing name
        renderExist = os.path.exists(saveName)
        if renderExist:
            os.remove(saveName)

        # render take
        # must make take to render current take or videos will not render properly.
        Globals.System.CurrentTake = takeToRender
        app.FileRender(videoOptions)

        # convert render using ffmpeg
        ffmpeg = r'{0}\bin\video\ffmpeg.exe'.format(RS.Config.Tool.Path.Root)
        sourceMovie = saveName

        finalMovie = sourceMovie.replace('_out.mov', ext)

        if not os.path.exists(ffmpeg):
            QtGui.QMessageBox.warning(None, "Render Conversion error:",
                                      "ffmpeg library does not exist:\nPlease check for - ('<project>/tools/bin/video/ffmpeg.exe') " + str(
                                          e))
        else:
            # use conversion settings;
            # audio codec = MP3 (This was removed as ffmpeg would create playback errors. command was '-acodec libmp3lame')
            # number of audio tracks = 2 (stereo)
            # audio rate 44100 (cd quality)
            # video codec = H.264
            # GOP (group of pictures - helps looping) = 10
            # overwrite existing file = yes
            try:
                os.system(
                    #'%s -i "%s" -ac 2 -ar 48000 -vcodec libx264 -g 10 -y "%s"' % (ffmpeg, sourceMovie, finalMovie))
                    
                    '%s -i "%s" -acodec pcm_s16le -ar 48000 -vcodec libx264 -g 1 -y "%s"' % (ffmpeg, sourceMovie, finalMovie)) # needs to be 16bit 48k
                os.remove(sourceMovie)
            except Exception, e:
                QtGui.QMessageBox.warning(None, "Render issue:", "There was an issue rendering this file:\n\n" + str(e))

        ##### reset window size #####
        lCamera = Globals.System.Renderer.CurrentCamera
        lCamera.FrameSizeMode = mobu.FBCameraFrameSizeMode.kFBFrameSizeWindow

        self.copyTo(finalMovie)

        # dealing with multiple panes ( aka viewports ) set back to how they were before we started rendering
        cameras = Globals.System.Scene.Cameras
        for paneCamera in range(4):
            for camera in cameras:
                if camera.Name == self.paneCameras[paneCamera]:
                    Globals.System.Scene.Renderer.SetCameraInPane(camera, paneCamera)

    
    def getRenderDest(self):

        lModel = mobu.FBFindModelByLabelName("ANIMRENDER_DATA_NODE")
        custom2_checked = lModel.PropertyList.Find('AnimRender_Custom2_checked')
        custom2 = lModel.PropertyList.Find('AnimRender_Custom2_text')

        self.dstLocations = []

        # desktop filepath
        self.desktopPath = (os.path.join(os.path.expandvars("%userprofile%"), 'Desktop', "Renders" + "\\"))
        self.dstLocations.append(self.desktopPath)

        # gta filepath
        today = datetime.now().date()
        start = today - timedelta(days=today.weekday())
        end = start + timedelta(days=6)
        username = RS.Config.User.Name

        self.gtaPath = ("\\\\rockstar.t2.corp\\network\\Projects\\GTA5\\Animation\\WeeklyUpdates\\"
                        + str(start.strftime('%Y'))
                        + "\\Week Commencing "
                        + str(start)
                        + "\\Scripted\\"
                        + username + "\\")

        self.dstLocations.append(self.gtaPath)

        # guess filepath
        sourceFileName = RS.Globals.Application.FBXFileName
        dirPath, fileName = os.path.split(sourceFileName)
        dirPath = dirPath.replace("source_fbx", "resources\\renders")

        self.guessPath = (dirPath + "\\")
        self.dstLocations.append(self.guessPath)

        # custom filepath
        if self.radioButton_output_Desktop.isChecked():
            if RenderSettings.Custom1Path != "":
                self.lineEdit_outputPath.setText(RenderSettings.Custom1Path)
        self.dstLocations.append(RenderSettings.Custom1Path)

        # custom 2 filepath
        custom2 = lModel.PropertyList.Find('AnimRender_Custom2_text')
        self.custom2Path = (str(custom2.Data))
        self.dstLocations.append(self.custom2Path)

        if self.radioButton_output_Desktop.isChecked():
            custom2_checked.Data = "False"
            self.lineEdit_outputPath.setText(self.desktopPath)

        elif self.radioButton_output_GTA.isChecked():
            custom2_checked.Data = "False"
            self.lineEdit_outputPath.setText(self.gtaPath)

        elif self.radioButton_output_Guess.isChecked():
            custom2_checked.Data = "False"
            self.lineEdit_outputPath.setText(self.guessPath)

        elif self.radioButton_output_Custom.isChecked():
            custom2_checked.Data = "False"
            self.lineEdit_outputPath.setText(RenderSettings.Custom1Path)

        elif self.radioButton_output_Custom2.isChecked():
            self.lineEdit_outputPath.setText(self.custom2Path)

    
    def copyTo(self, finalMovie):

        copyToList = [self.pushButton_copyto_desktop, self.pushButton_copyto_gtaWeeklies, self.pushButton_copyto_guess,
                      self.pushButton_copyto_custom1, self.pushButton_copyto_custom2]

        filename = os.path.basename(finalMovie)

        try:
            for dst in range(len(copyToList)):
                if copyToList[dst].isChecked():
                    if not os.path.isdir(self.dstLocations[dst]):  # check to see if the copyTo folder exists
                        os.makedirs(self.dstLocations[dst])

                    if self.renderToMovCheckboxes[dst].isChecked(): # see if we need to change file ext
                        filename = filename.replace('.mp4', '.mov')

                    copyToFile = os.path.join(self.dstLocations[dst], filename)
                    Tools.perforceFileCheck(copyToFile)  # see if this is a depot file
                    shutil.copyfile(finalMovie, copyToFile)
        except Exception, e:
            QtGui.QMessageBox.warning(None, "Copy issue:",
                                      "There was an issue copying the data to the other folders:\n\n" + str(e))

    
    def addTodaysDate(self):

        todaysDate = datetime.now().date()

        text = self.lineEdit_outputPath.text()
        if self.checkBox_addTodaysDate.isChecked():
            text = (os.path.join(text, str(todaysDate) + "\\"))
            self.lineEdit_outputPath.setText(text)
            self.checkBox_addTodaysDate.setCheckState(
                QtCore.Qt.CheckState(False))  # works better than - .setChecked(bool)

    
    def createDataNode(self):

        lModel = mobu.FBFindModelByLabelName("ANIMRENDER_DATA_NODE")

        if not lModel:
            lModel = mobu.FBModelMarker("ANIMRENDER_DATA_NODE")
            lModel.Show = False
            lModel.PropertyCreate('AnimRender_Tree', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AnimRender_Tree_basic', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AnimRender_Camera', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AnimRender_Audio_Offset', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AnimRender_Checked_States', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AnimRender_Checked_States_basic', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AnimRender_prefix_option', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AnimRender_prefix_suffix_text', mobu.FBPropertyType.kFBPT_charptr, 'String', False,
                                  True, None),  # Not animatable
            lModel.PropertyCreate('AnimRender_Custom2_checked', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable
            lModel.PropertyCreate('AnimRender_Custom2_text', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True,
                                  None),  # Not animatable

    
    def readDataNode(self):
        lModel = mobu.FBFindModelByLabelName("ANIMRENDER_DATA_NODE")

        rawTreeData = lModel.PropertyList.Find('AnimRender_Tree')
        rawTreeData_basic = lModel.PropertyList.Find('AnimRender_Tree_basic')        
        rawCameraData = lModel.PropertyList.Find('AnimRender_Camera')
        rawAudioOffsetData = lModel.PropertyList.Find('AnimRender_Audio_Offset')
        rawCheckStateData = lModel.PropertyList.Find('AnimRender_Checked_States')
        rawCheckStateData_basic = lModel.PropertyList.Find('AnimRender_Checked_States_basic')
        prefix_option = lModel.PropertyList.Find('AnimRender_prefix_option')
        prefix_suffix_text = lModel.PropertyList.Find('AnimRender_prefix_suffix_text')
        custom2_checked = lModel.PropertyList.Find('AnimRender_Custom2_checked')
        custom2_text = lModel.PropertyList.Find('AnimRender_Custom2_text')

        if rawTreeData.Data != "":
            self.createTreeFromDataNode(rawTreeData.Data, rawCameraData.Data, rawAudioOffsetData.Data,
                                        rawCheckStateData.Data)
        else:
            self.treeWidget_association.clear()

        if rawTreeData_basic.Data != "":
            self.createTreeFromDataNode_basic(rawTreeData_basic.Data, rawCheckStateData_basic.Data)
        else:
            self.treeWidget_basic_association.clear()
            
        if prefix_suffix_text.Data != "":
            self.lineEdit_prefix_suffix.setText(prefix_suffix_text.Data)

        if prefix_option.Data != "":
            if prefix_option.Data == "True":
                self.radioButton_prefix.setChecked(True)
            else:
                self.radioButton_suffix.setChecked(True)

        if custom2_checked.Data == "True":
            self.radioButton_output_Custom2.setChecked(True)
            self.lineEdit_outputPath.setText(custom2_text.Data)

    
    def writeToDataNode(self):
        # write to scene datanode   
        lModel = mobu.FBFindModelByLabelName("ANIMRENDER_DATA_NODE")
        if not lModel:
            self.createDataNode()
            lModel = mobu.FBFindModelByLabelName("ANIMRENDER_DATA_NODE")

        rawTreeData = lModel.PropertyList.Find('AnimRender_Tree')
        rawTreeData_basic = lModel.PropertyList.Find('AnimRender_Tree_basic')
        rawCameraData = lModel.PropertyList.Find('AnimRender_Camera')
        rawAudioOffsetData = lModel.PropertyList.Find('AnimRender_Audio_Offset')
        rawCheckStateData = lModel.PropertyList.Find('AnimRender_Checked_States')
        rawCheckStateData_basic = lModel.PropertyList.Find('AnimRender_Checked_States_basic')
        rawTreeData.Data = ""
        rawTreeData_basic.Data = ""
        rawCameraData.Data = ""
        rawAudioOffsetData.Data = ""
        rawCheckStateData.Data = ""
        textTreeData = ""
        textTreeData_basic = ""
        textCameraData = ""
        textAudioOffsetData = ""
        textCheckStateData = ""
        textCheckStateData_basic = ""

        for audioGroup in self.treeData:
            textTreeData = textTreeData + "audioGroup: " + audioGroup[0] + "\n"
            if len(audioGroup) != 1:
                for take in range(1, len(audioGroup)):
                    textTreeData = textTreeData + "   take: " + audioGroup[take] + "\n"

        for takeGroup_basic in self.treeData_basic:
            textTreeData_basic = '{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}'.format(textTreeData_basic, "take: "
                                    ,takeGroup_basic[0], "\n"
                                    , "   camera: ", takeGroup_basic[1], "\n"
                                    , "   subtitle: ", takeGroup_basic[2], "\n"
                                    , "   audio: ", takeGroup_basic[3], "\n")

        for audioGroup in self.treeCamera:
            textCameraData = textCameraData + "audioGroup: " + audioGroup[0] + "\n"
            if len(audioGroup) != 1:
                for camera in range(1, len(audioGroup)):
                    textCameraData = textCameraData + "   camera: " + (', '.join(audioGroup[camera])) + "\n"

        for audioGroup in self.treeAudioOffset:
            textAudioOffsetData = textAudioOffsetData + "audioGroup: " + audioGroup[0] + "\n"
            if len(audioGroup) != 1:
                for AudioOffset in range(1, len(audioGroup)):
                    textAudioOffsetData = textAudioOffsetData + "   audioOffset: " + audioGroup[AudioOffset] + "\n"

        if self.treeCheckState:
            for line in self.treeCheckState:
                textCheckStateData = '{0}{1}{2}{3}{4}'.format(textCheckStateData, line[0], "~", line[1], "\n")

        if self.treeCheckState_basic:
            for line in self.treeCheckState_basic:
                textCheckStateData_basic = '{0}{1}{2}'.format(textCheckStateData_basic, line, "\n")

        if self.treeData:
            rawTreeData.Data = textTreeData
        if self.treeData_basic:    
            rawTreeData_basic.Data = textTreeData_basic
        rawCameraData.Data = textCameraData
        rawAudioOffsetData.Data = textAudioOffsetData
        if self.treeCheckState:
            rawCheckStateData.Data = textCheckStateData
        if self.treeCheckState_basic:
            rawCheckStateData_basic.Data = textCheckStateData_basic

    
    def writeToCustom2DataNode(self):
        lModel = mobu.FBFindModelByLabelName("ANIMRENDER_DATA_NODE")
        custom2_checked = lModel.PropertyList.Find('AnimRender_Custom2_checked')
        custom2_text = lModel.PropertyList.Find('AnimRender_Custom2_text')

        if self.radioButton_output_Custom.isChecked():
            RenderSettings.Custom1Path = str(self.lineEdit_outputPath.text())
            self.saveConfigXML()

        if self.radioButton_output_Custom2.isChecked():
            custom2_checked.Data = "True"
            custom2_text.Data = str(self.lineEdit_outputPath.text())

    
    def writeToPrefixSuffixDataNode(self):
        lModel = mobu.FBFindModelByLabelName("ANIMRENDER_DATA_NODE")
        prefix_option = lModel.PropertyList.Find('AnimRender_prefix_option')
        prefix_suffix_text = lModel.PropertyList.Find('AnimRender_prefix_suffix_text')
        prefix_suffix_text.Data = str(self.lineEdit_prefix_suffix.text())

        if self.radioButton_prefix.isChecked():
            prefix_option.Data = "True"
        elif self.radioButton_suffix.isChecked():
            prefix_option.Data = "False"

    
    def createTreeFromDataNode(self, rawTreeData, rawCameraData, rawAudioOffsetData, rawCheckStateData):

        rawTreeGroups = rawTreeData.split("\n")
        rawTreeGroups.pop()

        rawCameraGroups = rawCameraData.split("\n")
        rawCameraGroups.pop()

        rawAudioOffsetGroups = rawAudioOffsetData.split("\n")
        rawAudioOffsetGroups.pop()

        rawCheckStateGroups = rawCheckStateData.split("\n")
        rawCheckStateGroups.pop()

        self.treeData = []
        self.treeCamera = []
        self.treeAudioOffset = []
        self.treeCheckState = []

        step = 0

        for item in range(len(rawTreeGroups)):
            if "audioGroup: " in rawTreeGroups[item]:
                rawTreeGroup = rawTreeGroups[item].replace("audioGroup: ", "")
                self.treeData.append([rawTreeGroup])
                step += 1
            else:
                rawTake = rawTreeGroups[item].replace("   take: ", "")
                self.treeData[step - 1].append(rawTake)

        step = 0

        for item in range(len(rawCameraGroups)):
            if "audioGroup: " in rawCameraGroups[item]:
                rawCameraGroup = rawCameraGroups[item].replace("audioGroup: ", "")
                self.treeCamera.append([rawCameraGroup])
                step += 1
            else:
                rawCamera = rawCameraGroups[item].replace("   camera: ", "")
                rawCamera = rawCamera.split(", ")
                self.treeCamera[step - 1].append(rawCamera)

        step = 0

        for item in range(len(rawAudioOffsetGroups)):
            if "audioGroup: " in rawAudioOffsetGroups[item]:
                rawAudioOffsetGroup = rawAudioOffsetGroups[item].replace("audioGroup: ", "")
                self.treeAudioOffset.append([rawAudioOffsetGroup])
                step += 1
            else:
                rawAudioOffset = rawAudioOffsetGroups[item].replace("   audioOffset: ", "")
                self.treeAudioOffset[step - 1].append(rawAudioOffset)

        for item in range(len(rawCheckStateGroups)):
            line = rawCheckStateGroups[item].split("~")
            self.treeCheckState.append(line)

        self.treeWidget_association.clear()

        for index in range(len(self.treeData)):

            audioGroupName = self.treeData[index][0]

            root = QtGui.QTreeWidgetItem(self.treeWidget_association, [audioGroupName])
            root.setIcon(0, self.wavIcon)

            for takeName in range(1, len(self.treeData[index])):
                data = QtGui.QTreeWidgetItem(root, [self.treeData[index][takeName]])
                data.setIcon(0, self.animIcon)
                if self.treeCamera[index][takeName]:
                    if len(self.treeCamera[index][takeName]) == 1:
                        data.setFlags(
                            data.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)  # code to add the flag
                        data.setCheckState(0, QtCore.Qt.Unchecked)

                    if len(self.treeCamera[index][takeName]) > 1:
                        data.setFlags(
                            data.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)  # code to add the flag
                        data.setCheckState(0, QtCore.Qt.Unchecked)
                        for item in self.treeCamera[index][takeName]:
                            takeItem = QtGui.QTreeWidgetItem(data, [0, item])
                            takeItem.setText(1, item)
                            takeItem.setIcon(1, self.cameraIcon)
                            takeItem.setFlags(
                                takeItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable)  # code to add the flag
                            takeItem.setCheckState(1, QtCore.Qt.Unchecked)
                    else:
                        data.setText(1, self.treeCamera[index][takeName][0])

                        if self.treeCamera[index][takeName][0] == '':
                            data.setIcon(1, self.cameraNoIcon)
                        else:
                            data.setIcon(1, self.cameraIcon)

                if self.treeAudioOffset[index][takeName] != '':
                    data.setText(2, self.treeAudioOffset[index][takeName])
                    data.setIcon(2, self.offsetIcon)

        self.treeWidget_association.expandAll()
        self.setCheckState()
        self.colouriseWidgets()

        
    def createTreeFromDataNode_basic(self, rawTreeData_basic, rawCheckStateData_basic):

        rawTreeGroups_basic = rawTreeData_basic.split("\n")
        rawTreeGroups_basic.pop()        

        rawCheckStateGroups_basic = rawCheckStateData_basic.split("\n")
        rawCheckStateGroups_basic.pop()

        self.treeData_basic = []
        self.treeCheckState_basic = []
        
        step = 0
        
        for item in range(len(rawTreeGroups_basic)):
            #print rawTreeGroups_basic[item]
            if "take: " in rawTreeGroups_basic[item]:
                rawTreeGroup_basic = rawTreeGroups_basic[item].replace("take: ", "")
                self.treeData_basic.append([rawTreeGroup_basic])
                step += 1
            else:
                if "camera" in  rawTreeGroups_basic[item]: 
                    rawCamera_basic = rawTreeGroups_basic[item].replace("   camera: ", "")
                    self.treeData_basic[step - 1].append(rawCamera_basic)
                if "subtitle" in  rawTreeGroups_basic[item]: 
                    rawSubtitle_basic = rawTreeGroups_basic[item].replace("   subtitle: ", "")
                    self.treeData_basic[step - 1].append(rawSubtitle_basic)
                if "audio" in  rawTreeGroups_basic[item]: 
                    rawAudio_basic = rawTreeGroups_basic[item].replace("   audio: ", "")
                    self.treeData_basic[step - 1].append(rawAudio_basic)

        for item in rawCheckStateGroups_basic:
            self.treeCheckState_basic.append(item)

        self.treeWidget_basic_association.clear()

        for index in range(len(self.treeData_basic)):
            treeGroupName = self.treeData_basic[index][0]
            camera = self.treeData_basic[index][1]
            subtitleText = self.treeData_basic[index][2]
            audioText = self.treeData_basic[index][3]

            root = QtGui.QTreeWidgetItem(self.treeWidget_basic_association, [treeGroupName])
            root.setIcon(0, self.animIcon)

            if self.treeCheckState_basic[index] == 'True':
                root.setCheckState(0, QtCore.Qt.Checked)
            else:
                root.setCheckState(0, QtCore.Qt.Unchecked)

            if camera != '':
                root.setText(1, camera)
                root.setIcon(1, self.cameraIcon)
            else:
                root.setIcon(1, self.cameraNoIcon)

            root.setText(2, subtitleText)

            for subtitle in self.subtitleGroups:
                if subtitleText == subtitle[0]:
                    tooltip = subtitle[1].replace('. ','.\n')
                    root.setToolTip(2, tooltip)

            root.setText(3, audioText)

  
    def deleteConfig(self):

        if self.comboBox_customRenderSettings.currentText() == "Default":
            return

        comboBoxIndex = self.comboBox_customRenderSettings.currentIndex()

        renderSettingsText = self.comboBox_customRenderSettings.currentText()

        self.comboBox_customRenderSettings.removeItem(comboBoxIndex)

        index = None

        for customSetting in RenderSettings.data:
            if customSetting[0] == str(renderSettingsText):
                index = RenderSettings.data.index(customSetting)

                del RenderSettings.data[index]

                self.saveConfigXML()

    
    def updateConfig(self):

        if self.lineEdit_customRenderSettings.text() == "":
            return
        if self.lineEdit_customRenderSettings.text() == "Default":
            self.setRenderOptions(0)
            return

        index = None

        for customSetting in RenderSettings.data:
            if customSetting[0] == str(self.lineEdit_customRenderSettings.text()):
                index = RenderSettings.data.index(customSetting)

                RenderSettings.data[index] = ([str(self.lineEdit_customRenderSettings.text()),
                                               str(self.comboBox_pictureFormat.currentIndex()),
                                               str(int(self.spinBox_customWidth.value())),
                                               str(int(self.spinBox_customHeight.value())),
                                               str(int(self.spinBox_hud_X.value())),
                                               str(int(self.spinBox_hud_Y.value())),
                                               str(self.comboBox_font.currentIndex()),
                                               str(float(self.doubleSpinBox_fontScale.text())),
                                               str(self.comboBox_justification.currentIndex()),
                                               str(self.comboBox_horizontalDock.currentIndex()),
                                               str(self.comboBox_verticalDock.currentIndex())])

                self.saveConfigXML()

    
    def saveConfig(self):

        if self.lineEdit_customRenderSettings.text() == "":
            return

        for customSetting in RenderSettings.data:
            if str(
                    self.lineEdit_customRenderSettings.text()) in customSetting:  # so we don't create duplicate settings of the same name, the user can however update a existing setting by using the 'update' button
                return

        settings = [str(self.lineEdit_customRenderSettings.text()),
                    str(self.comboBox_pictureFormat.currentIndex()),
                    str(int(self.spinBox_customWidth.value())),
                    str(int(self.spinBox_customHeight.value())),
                    str(int(self.spinBox_hud_X.value())),
                    str(int(self.spinBox_hud_Y.value())),
                    str(self.comboBox_font.currentIndex()),
                    str(float(self.doubleSpinBox_fontScale.text())),
                    str(self.comboBox_justification.currentIndex()),
                    str(self.comboBox_horizontalDock.currentIndex()),
                    str(self.comboBox_verticalDock.currentIndex())]

        RenderSettings.data.append(settings)

        self.comboBox_customRenderSettings.addItem(str(self.lineEdit_customRenderSettings.text()))
        count = self.comboBox_customRenderSettings.count()
        self.comboBox_customRenderSettings.setCurrentIndex(count - 1)

        self.saveConfigXML()

    
    def saveConfigXML(self):
        # save the AudioHud Configs in pretty formatting :)
        # for the record - C:\Users\%username%\AppData\Roaming\rockstar\animrender\
        dirPath = os.path.dirname(ANIMRENDER_CONFIG_PATH)

        if not os.path.isdir(dirPath):  # check to see if the config folder exists
            os.makedirs(dirPath)

        fileName = "AnimRender_ConfigData.xml"

        datafile = os.path.join(dirPath, fileName)

        root = ET.Element("AnimRender_Configuration_Settings")

        RenderCustom1 = ET.SubElement(root, "RenderCustom1")

        FilePath = ET.SubElement(RenderCustom1, "FilePath").text = str(RenderSettings.Custom1Path)

        CustomRenderSettings = ET.SubElement(root, "CustomRenderSettings")

        for renderSetting in range(1, len(RenderSettings.data)):
            SavedRenderSettings = ET.SubElement(CustomRenderSettings, "SavedRenderSettings",
                                                Name=RenderSettings.data[renderSetting][0])

            RenderFrameSize = ET.SubElement(SavedRenderSettings, "RenderFrameSize")

            ET.SubElement(RenderFrameSize, "PictureSize", Size=RenderSettings.data[renderSetting][1])
            ET.SubElement(RenderFrameSize, "CustomWidth", Width=RenderSettings.data[renderSetting][2])
            ET.SubElement(RenderFrameSize, "CustomHeight", Height=RenderSettings.data[renderSetting][3])

            HudAlignment = ET.SubElement(SavedRenderSettings, "HudAlignment")
            ET.SubElement(HudAlignment, "AlignX", X=RenderSettings.data[renderSetting][4])

            ET.SubElement(HudAlignment, "AlignY", Y=RenderSettings.data[renderSetting][5])
            ET.SubElement(HudAlignment, "Font", Font=RenderSettings.data[renderSetting][6])
            ET.SubElement(HudAlignment, "FontScale", Scale=RenderSettings.data[renderSetting][7])
            ET.SubElement(HudAlignment, "Justification", Justification=RenderSettings.data[renderSetting][8])
            ET.SubElement(HudAlignment, "HorizontalDock", HorizontalDock=RenderSettings.data[renderSetting][9])
            ET.SubElement(HudAlignment, "VerticalDock", VerticalDock=RenderSettings.data[renderSetting][10])

        xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ", encoding='UTF-8')

        with open(datafile, "w") as f:
            f.write(str(xmlstr.decode('UTF-8')))
            f.close()

    
    def readConfigXML(self):

        dirPath = os.path.dirname(ANIMRENDER_CONFIG_PATH)

        fileName = "AnimRender_ConfigData.xml"

        datafile = os.path.join(dirPath, fileName)

        if not os.path.isfile(datafile):
            self.saveConfigXML()
            return

        RenderSettings.data = RenderSettings.data[:1]  # remove all but first list

        try:
            tree = ET.parse(datafile)
        except Exception, e:
            QtGui.QMessageBox.warning(None, "Custom Render Settings issue:",
                                      "There was an issue reading the custom user render settings:\n\n" + str(e))

        root = tree.getroot()

        customPath = root.findall(".//RenderCustom1/FilePath")
        for child in customPath:
            RenderSettings.Custom1Path = child.text

        for item in root.findall(".//CustomRenderSettings/SavedRenderSettings"):
            SavedRenderSettings = []
            SavedRenderSettings.append(str(item.attrib['Name']))

            for child in item:
                for value in child:
                    if 'Size' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Size']))
                    if 'Width' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Width']))
                    if 'Height' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Height']))

                    if 'X' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['X']))
                    if 'Y' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Y']))
                    if 'Font' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Font']))
                    if 'Scale' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Scale']))
                    if 'Justification' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['Justification']))
                    if 'HorizontalDock' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['HorizontalDock']))
                    if 'VerticalDock' in value.attrib:
                        SavedRenderSettings.append(str(value.attrib['VerticalDock']))

            RenderSettings.data.append(SavedRenderSettings)

        for settings in range(1, len(RenderSettings.data)):
            self.comboBox_customRenderSettings.addItem(RenderSettings.data[settings][0])


def Run():
    form = MainWindow()
    form.setWindowTitle(toolName)
    form.show()