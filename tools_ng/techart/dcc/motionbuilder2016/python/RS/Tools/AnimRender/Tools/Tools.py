"""
AnimRender
Tools
"""

import pyfbsdk as mobu
import os, shutil, subprocess, webbrowser

import json
from HTMLParser import HTMLParser
from xml.etree import cElementTree
from xml.dom import minidom
import copy
import ntpath
import re
import xml.etree.ElementTree as ET

from PySide import QtGui, QtCore

import RS.Tools.UI
from RS import Config, Globals, Perforce

def openVideoHelper():
    ### open the video helper tool###
    # first, create a shortcut the subprocess likes
    # note: "Desktop\VideoHelper.exe - Shortcut.lnk" should have already been setup by the user as it is a requirement.
    # adding in a catch to deal with this
    try:
        old_file = os.path.join("C:\\Users", RS.Config.User.Name, "Desktop\VideoHelper.exe - Shortcut.lnk")
        new_file = os.path.join("C:\\Users", RS.Config.User.Name, "Desktop\VideoHelperShortcut.lnk")
        
        if not os.path.isfile(new_file):
            shutil.copy2(old_file, new_file)
        
        proc = subprocess.Popen(" ".join(["start", "/B" , new_file]), shell=True)
        proc.wait()
    except:
        QtGui.QMessageBox.warning(None, "VideoHelper Setup Error.",
                                "No VideoHelper setup on this PC.\nPlease make sure there is a shortcut on your desktop." )  


def help():
    webbrowser.open('https://hub.gametools.dev/display/ANIM/Audio+Hud')


def perforceFileCheck(p4File): 
    
    if p4File.lower().startswith("x:\\"):
        currentDepotFile = []
        
        filename = os.path.basename(p4File)
        dirPath = os.path.dirname(p4File)
        
        p4Path = p4File
        
        PerforceFileData = Perforce.Where([p4Path])# get perforce stats on our file

        for item in PerforceFileData:      
            depotFile = item['depotFile']
            currentDepotFile.append(depotFile.encode("utf-8"))

        if currentDepotFile:
            Perforce.Sync(currentDepotFile[0])
            Perforce.Edit(currentDepotFile[0])
        else:
            QtGui.QMessageBox.warning(None, "Perforce error.",
                                    p4Path + "\nNot a depot File" ) 
            return False


def createRenderTemplate(perforceFolder, useChildFolders):

    toolDirectory = os.path.join(RS.Config.Script.Path.Root, 'RS', 'Tools', 'AnimRender', 'Tools')
    templateFile = os.path.join(toolDirectory, 'hubTemplate.txt')
    RenderTemplate = os.path.join(toolDirectory, 'RenderTemplate.txt')

    templateEntry = []
    templateStart = '<ac:layout>'
    templateEnd = '</ac:layout>'
    templateSectionStart = '  <ac:layout-section ac:type="two_equal">'
    templateSectionEnd = '  </ac:layout-section>'

    if os.path.isfile(templateFile):
        with open(templateFile, "r") as f:
            for line in f:
                line = line.rstrip('\n')
                templateEntry.append(line)
            f.close

    if useChildFolders:
        p4Files = Perforce.Files([os.path.join(perforceFolder, '...')]) # perforce wildcard
    else:
        p4Files = Perforce.Files(['{}*'.format(perforceFolder)]) # not recursive

    depotFiles = []

    for p4File in p4Files:
        depotFile = p4File['depotFile']
        if depotFile:
            depotFiles.append((depotFile.lower()).encode("utf-8"))
            
    if not depotFiles:
        QtGui.QMessageBox.warning(None, "Perforce Folder Error.", '{0}{1}'.format("There are no files to upload to the hub?",
                                                                                    "\nPlease check and try again."))
        return 'Fail'

    renderTemplate = []

    for depotFile in depotFiles:
        for line in templateEntry:
            if '<h2>""</h2>' in line:
                lineEdit = depotFile.split(perforceFolder.lower())
                line = line.replace('<h2>""</h2>', '{0}{1}{2}'.format('<h2>', lineEdit[-1], '</h2>'))
                renderTemplate.append(line)
            elif 'TEMPLATE_NAME' in line:
                lineEdit = depotFile.split(perforceFolder.lower())
                fileName = lineEdit[-1]
                line = line.replace('TEMPLATE_NAME', '{}'.format(fileName))
                renderTemplate.append(line)
            elif 'TEMPLATE_FILENAME' in line:
                lineEdit = depotFile.split('/')
                fileName = lineEdit[-1]
                line = line.replace('TEMPLATE_FILENAME', '{}'.format(fileName))
                renderTemplate.append(line)
            else:
                renderTemplate.append(line)

    for index in range(len(renderTemplate)):

        if (index % 22) == 0:
            renderTemplate.insert(index, '{}'.format(templateSectionEnd))
            renderTemplate.insert(index+1, '{}'.format(templateSectionStart))
    
    renderTemplate.pop(0)

    if renderTemplate[-1] != templateSectionEnd:
        renderTemplate.append('{}'.format(templateSectionEnd))

    renderTemplate.insert(0, '{}'.format(templateStart))

    renderTemplate.append('{}'.format(templateEnd))

    with open(RenderTemplate, "w") as f:
        for line in renderTemplate:
            f.write('{0}{1}'.format(line, '\n'))
        f.close


def createConfDataFile(parentPage, pageName, perforceFolder, useChildFolders, pageID):

    toolDirectory = os.path.join(RS.Config.Script.Path.Root, 'RS', 'Tools', 'AnimRender', 'Tools')
    confPageData = os.path.join(toolDirectory, 'ConfPageData.xml')

    root = ET.Element("Confluence_Page_Data")

    if useChildFolders:
        p4Files = Perforce.Files([os.path.join(perforceFolder, '...')]) # perforce wildcard
    else:
        p4Files = Perforce.Files(['{}*'.format(perforceFolder)]) # not recursive

    depotFiles = []

    for p4File in p4Files:
        depotFile = p4File['depotFile']
        if depotFile:
            depotFiles.append((depotFile.lower()).encode("utf-8"))

    ParentPage = ET.SubElement(root, "ParentPage")
    ParentPageTitle = ET.SubElement(ParentPage, "ParentPageTitle").text = '{}'.format(parentPage)

    PageName = ET.SubElement(root, "PageName")
    NewPageTitle = ET.SubElement(PageName, "NewPageTitle").text = '{}'.format(pageName)

    if pageID == '':
        pageID = 'None'

    PageID = ET.SubElement(root, "PageID")
    NewPageID = ET.SubElement(PageID, "NewPageID").text = '{}'.format(pageID)

    RenderFiles = ET.SubElement(root, "RenderFiles")

    for depotFile in depotFiles:
        RenderFile = ET.SubElement(RenderFiles, "RenderFile", Name='{}'.format(depotFile))

    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ", encoding='UTF-8')

    with open(confPageData, "w") as f:
        f.write(str(xmlstr.decode('UTF-8')))
        f.close()