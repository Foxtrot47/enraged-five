import pyfbsdk as mobu

DEVICENAME = "SpaceBall"

class SpaceBall(object):
    def __init__(self):
        """

        :return:
        """
        self._device = self._finddevice(DEVICENAME)
        if not self._device:
            self._device = mobu.FBCreateObject("Browsing/Templates/Devices", DEVICENAME, DEVICENAME)
            mobu.FBSystem().Scene.Devices.append(self._device)

        # Disable Object Manipulation
        manipObjProp = [prop for prop in self._device.PropertyList if prop.Name == "Manipulate Selected Objects"]
        manipObjProp[0].Data = False

        # Set Live to Match Device
        self._device.Live = self._device.Online

    @property
    def device(self):
        """
        return device

        :return:
            Mobu FBdevice
        """
        return self._device

    @property
    def isEnabled(self):
        """
        Return if device is online or not

        :return:
            Bool
        """
        return self._device.Online



    def record(self):
        """
        Public Enable Record method

        :return:
            None
        """
        self._deviceRecord()


    def playback(self):
        """
        Public enable Playback method
        :return:
            None
        """
        self._devicePlayback()


    def _finddevice(self, name):
        """
        Find if we have already an existing device

        :param name:
            String: Name of device
        :return:
            Mobu FBDevice
        """
        result = None
        devicelist = mobu.FBSystem().Scene.Devices
        if devicelist:
          for device in devicelist:
            if device.Name == name:
              result = device
              break
        return result

    def _removedevice(self, name):
        """
        PlaceHolder

        :param name:
        :return:
            None
        """
        return


    def _deviceOnOff(self):
        """
        Enable/Disable recording, online and live options

        :return:
            None
        """
        if self._device.Online:
            self._device.Online = False
            self._device.Live = False
        else:
            self._device.Online = True
            self._device.Live = True


    def _deviceRecord(self):
        """
        Enable device to record

        :return:
            None
        """
        self._device.RecordMode = True


    def _devicePlayback(self):
        """
        Enable device in playback mode

        :return:
            None
        """
        self._device.RecordMode = False
