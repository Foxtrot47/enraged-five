'''
Used for adding and removing the relavant constraint
'''

import pyfbsdk as mobu
from RS import Globals
import Device
from RS.Utils.Scene import RelationshipConstraintManager as rcm
from RS.Tools.UI import QPrint
reload(rcm)

# TODO PAss this in
#spaceballConn = Device.SpaceBall()



class SpaceballConstraint(object):
    def __init__(self, spaceballDevice, remoteSignal):
        """

        :return:
        """
        self.spaceBallConstraint = None
        self.conManager = None
        self.remoteSignal = remoteSignal

        self._camera = None
        self.constraintName = "SpaceBallRelConstraint"
        self.senderDevice = spaceballDevice.device

        self.cameraSwitcher = mobu.FBCameraSwitcher()
        self._lastCamIndex = None

        self._spaceballBox = None
        self._InitSpaceBallConstraint()

    def _InitSpaceBallConstraint(self):
        """
        Add the spaceball Relationship Constraint

        :return
            None
        """
        # Create a New Constraint Group
        self.spaceBallConstraint = rcm.GetRelationShipConstraintByName(self.constraintName)
        if self.spaceBallConstraint is None:
            self.spaceBallConstraint = rcm.RelationShipConstraintManager(name=self.constraintName)

        # init our constraint Manager to work with htis group
        self.conManager = rcm.RelationShipConstraintManager(self.spaceBallConstraint.GetConstraint())

        # check if we already have a spaceball Box device sender
        _boxes = self.conManager.GetBoxesByRegex("SpaceBall")

        if len(_boxes) == 0:
            self._spaceballBox = self.spaceBallConstraint.AddSenderBox(self.senderDevice)
        else:
            self._spaceballBox = _boxes[0]

    @property
    def isEnabled(self):
        """
        Return if the spaceball device is active or not

        :return:
            Bool
        """
        return self.spaceBallConstraint.GetActive()


    def Remove(self):
        """
        Remove our Constraint

        :return:
            None
        """
        if not self.spaceBallConstraint is None:
            self.spaceBallConstraint.GetConstraint().FBDelete()


    def Enable(self):
        """
        Enable the SpaceBall Constraint

        :return:
            NOne
        """
        self.spaceBallConstraint.SetActive(True)

    def Disable(self):
        """
        Disable the SpaceBall Constraint

        :return:
            None
        """
        self.spaceBallConstraint.SetActive(False)


    def AddCameraBox(self, _camera):
        """

        :param camera:
        :return:
        """
        if self.spaceBallConstraint:
            # Remove any current cameras and add our new camera and link up
            cameraBoxes = self.conManager.GetBoxesByRegex("camera", caseSensitive=True)
            for camBox in cameraBoxes:
                camBox.GetBox().FBDelete()

            #Add our current switcher camera to teh constraint
            _cameraBox = self.spaceBallConstraint.AddReceiverBox(_camera)

            connections = {"Manipulator Camera Rotation":"Rotation",
                           "Manipulator Camera Translation":"Translation"}

            # TODO: Validate!!!
            for item in connections.iteritems():
                self.conManager.ConnectBoxes(
                                     self._spaceballBox,
                                     item[0],
                                    _cameraBox, # Inbox
                                     item[1])

    def AddCurrentCameraBox(self):
        """
        Add the selected camera to teh Spaceball relationship constraint
        We should only call this when the user presses the Record Button

        :return:
            None
        """
        # Get Active Camera
        if self._camera != self.cameraSwitcher.CurrentCamera:

            self._camera = self.cameraSwitcher.CurrentCamera
            self.AddCameraBox(self._camera)

    def RemoveCamera(self):
        """
        Placeholder: Remove the current camera to the Spaceball relationship constraint

        :return:
            None
        """
        return

    def OnConnectionKeyingNotify(self,  control, event ):
        """
        When teh switcher is updated with a new camera (via Multi camera viewer)
        we update the camera to the current camera
        :param control:
        :param event:
        :return:
        """

        #print event.Plug.ClassName()
        # TODO: DOes not work!!!
        mobu.FBCameraSwitcher().UseEvaluateSwitch()

        # Get our event
        if event.Plug.ClassName() == "FBCameraSwitcher" and event.Action == mobu.FBConnectionAction.kFBKeyingKey:
            print event.Plug.Name
            # Get camera switcher Index

            print mobu.FBCameraSwitcher().CurrentCameraIndex

            print mobu.FBCameraSwitcher().PropertyList.Find("Camera Index")


            self.AddCurrentCameraBox()



    def OnConnectionStateNotify(self, control, event ):
        """
        Called when the user clicks on an Camera we update the camera constraint
        :return:
        """
        #print event.Action
        if event.Action == mobu.FBConnectionAction.kFBSelect:
            #print event.Plug.Name

            if event.Plug.ClassName() == 'FBCamera' and not 'Producer' in event.Plug.Name:
                #QPrint(event.Plug.Name)
                self.remoteSignal.newCameraSelected(event.Plug.Name)
                self.AddCameraBox(event.Plug)

            #if event.Plug.ClassName() == 'FBCameraSwitcher':
            #    print event.Plug
                #self.AddCameraBox(event.Plug)
            '''
            if event.Plug.ClassName() == 'FBCameraSwitcher':
                self.CurrentCamera = event.Plug.CurrentCamera
            '''

    def OnSyncFrameUpdate( self, control, event):
        """
        Update per frame on playback
        """
        _camIndex =  self.cameraSwitcher.CurrentCameraIndex
        if self._lastCamIndex != _camIndex:
            self._lastCamIndex = self.cameraSwitcher.CurrentCameraIndex
            #print self._lastCamIndex


        return



    def Register(self):
        """

        :return:
        """
        print "registering callbacks"
        Globals.System.OnConnectionStateNotify.Add(self.OnConnectionStateNotify)
        #Globals.System.OnConnectionKeyingNotify.Add(self.OnConnectionKeyingNotify)
        # Per frame Update
        #Globals.EvaluateManager.OnSynchronizationEvent.Add(self.OnSyncFrameUpdate)

        Globals.App.OnFileExit.Add(self.Unregister)

    def Unregister(self, control=None, event=None):
        """

        :param control:
        :param event:
        :return:
        """
        print "unregistering callbacks"
        Globals.System.OnConnectionStateNotify.Remove(self.OnConnectionStateNotify)
        #Globals.System.OnConnectionKeyingNotify.Remove(self.OnConnectionKeyingNotify)
        # Per frame Update
        #Globals.EvaluateManager.OnSynchronizationEvent.Remove(self.OnSyncFrameUpdate)

        Globals.App.OnFileExit.Remove(self.Unregister)


