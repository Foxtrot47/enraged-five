import pyfbsdk as mobu
from RS import Globals


def plotCamera(fbcamera = None  ):
    '''

    :param fbcamera:
    :return:
    '''
    plotOptions = mobu.FBPlotOptions()
    plotOptions.ConstantKeyReducerKeepOneKey = True
    plotOptions.PlotAllTakes = True
    plotOptions.PlotOnFrame = True
    plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
    plotOptions.PlotTranslationOnRootOnly = True
    plotOptions.PreciseTimeDiscontinuities = True
    plotOptions.UseConstantKeyReducer = False


    Globals.System.CurrentTake.PlotTakeOnSelectedProperties( plotOptions )

