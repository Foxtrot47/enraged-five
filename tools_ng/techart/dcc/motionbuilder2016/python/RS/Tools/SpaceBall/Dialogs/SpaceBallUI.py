import pyfbsdk as mobu
import RS

import os
from PySide import QtGui, QtCore
from RS.Core.Camera import CamUtils
from RS.Tools.UI import QtBannerWindowBase, QPrint, QtMainWindowBase
from RS.Tools.SpaceBall.Models import Device
reload(Device)


# TODO: I just commented out the old stuff for now, will cleanup after testing on set. -Bbuck 2/12/18


class RemoteSignal(QtCore.QObject):
    """
        Simple class to handle any remote signal calls back to the UI if the game connection
        fails for whatever reason
    """

    cameraSelected = QtCore.Signal(str, str)

    def __init__(self):
        # Initialize the PunchingBag as a QObject
        QtCore.QObject.__init__(self)

    def newCameraSelected(self, state, msg=None):
        self.cameraSelected.emit(state, msg)

# remoteSignal = RemoteSignal()
# spaceballDevice = Device.SpaceBall()
# spaceballConstraint = Constraint.SpaceballConstraint(spaceballDevice, remoteSignal)


class MainWindow(QtBannerWindowBase):
    def __init__(self):
        """
        Initialize our layout and set up Events

        :return:
            None
        """
        super(MainWindow, self).__init__()
        self.Url = ""
        self.resize(160, 0)
        self.Layout.addWidget(SetupWidget())

        # spaceballConstraint.Register()

    def closeEvent(self, event):
        '''
        Placeholder

        :param event:
        :return:
            None
        '''
        # spaceballConstraint.Unregister()

        return


class SetupWidget(QtGui.QWidget):
    def __init__(self, parent=None, f=0):
        super(SetupWidget, self).__init__(parent=parent, f=f)

        # spaceballDevice.record()
        # self.recording = True
        self.spaceballDevice = Device.SpaceBall()
        self.recording = self.spaceballDevice._device.RecordMode

        mainLayout = QtGui.QGridLayout()
        buttonsLayout = QtGui.QVBoxLayout()

        # COnnect our REmote Signal
        # remoteSignal.cameraSelected.connect(self._setCameraLabel)

        #ICONS
        self.iconControllerOn = QtGui.QIcon(os.path.join(RS.Config.Script.Path.ToolImages,
                                                       "Devices", "3DMouse_On_84x84.png"))
        self.iconControllerOff = QtGui.QIcon(os.path.join(RS.Config.Script.Path.ToolImages,
                                                       "Devices", "3DMouse_Off_84x84.png"))

        # Style Sheets
        self.buttonUIrecord = "QPushButton:hover:enabled { color: white; }" \
                              "QPushButton { border-width: 2px;  background: qlineargradient(spread:pad, x1:0 y1:0, x2:0 y2:1, stop:0 rgba(200, 0, 0), stop:1 rgba(50, 0, 0));}" \
                               "QPushButton:checked { border-width: 2px;   }"
                            # border-style: inset;
                            #  "QPushButton:checked { border-width: 2px; border-radius: 10px; background-color: rgb(100,0,0); border-style: inset; }"

                             # " color: black; background-color: rgb(100,0,0);" \

        # Toggle Connect Button
        self.connectButton = QtGui.QPushButton('')

        self.connectButton.setCheckable(self.spaceballDevice.isEnabled)
        self.connectButton.setStyleSheet("QPushButton {  font-size: 12px;  }")
        self.connectButton.setIconSize(QtCore.QSize(84, 84))
        self.connectButton.clicked.connect(self._eventConnect)
        mainLayout.addWidget(self.connectButton, 0, 0)
        mainLayout.setAlignment(QtCore.Qt.AlignCenter)




        # Record Button
        self.recordButton = QtGui.QPushButton('Record Mode')
        self.recordButton.setCheckable(True)
        tempBool = False if self.recording else True  # A bit hacky - but couldn't get !self.recording to work
        self.recordButton.setChecked(tempBool)
        self.recordButton.setStyleSheet(self.buttonUIrecord)

        self.recordButton.setFixedHeight(50)
        self._SetConnectionText()

        self.recordButton.clicked.connect(self._eventMode)
        buttonsLayout.addWidget(self.recordButton)

        # Plot Camera Button
        self.PlotButton = QtGui.QPushButton('Plot Cams')
        self.PlotButton.clicked.connect(self._eventPlot)
        buttonsLayout.addWidget(self.PlotButton)

        # # Set Camera Button
        # self.SetCameraButton = QtGui.QLabel("No Camera Active")
        # #self.SetCameraButton.clicked.connect(self._eventPlot)
        # buttonsLayout.addWidget(self.SetCameraButton)

        # Current Camera Label



        # Add our layouts
        mainLayout.addLayout(buttonsLayout, 0, 1, 0)
        self.setLayout(mainLayout)


     # EVENTS

    def _setCameraLabel(self, state=None, msg=None):
        """

        :param state:
        :param msg:
        :return:
        """

        self.SetCameraButton.setText(state)




    def _eventConnect(self):
        '''
        Button Event to turn on/off Spaceball Device

        :return:
        '''
        self.spaceballDevice._deviceOnOff()
        self._SetConnectionText()


    def _eventMode(self):
        '''
        Toggle our main record or playmode

        :return:
            None
        '''
        self._toggleButtonMode()


    def _toggleButtonMode(self):
        '''
        Enable / Disable relevant options for teh Spaceball device depending on mode set
        :return:
        '''
        if self.recordButton.isChecked():
            self.recordButton.setText("Disabled")
            self.spaceballDevice.playback()
            self.recording = False
            # spaceballConstraint.Enable()

        else:
            self.recordButton.setText("Recording")
            self.spaceballDevice.record()
            self.recording = True
            # spaceballConstraint.AddCurrentCameraBox()
            # spaceballConstraint.Disable()


    def _SetConnectionText(self):
        """
        Set the Button Text based on teh button states

        :return:
            None
        """

        if self.spaceballDevice.isEnabled:
            self.connectButton.setIcon(self.iconControllerOn)
        else:
            self.connectButton.setIcon(self.iconControllerOff)

        if self.recording:
            self.recordButton.setText("Recording")
        else:
            self.recordButton.setText("Disabled")

    def _eventPlot(self):
        """

        :return:
        """
        CamUtils.PlotCamsToSpaceBall()







