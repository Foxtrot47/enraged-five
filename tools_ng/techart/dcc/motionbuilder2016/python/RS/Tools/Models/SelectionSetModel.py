from PySide import QtCore, QtGui
import RS.Config
import RS.Tools.Resources.icons_rc
import os

class SelectionSetNode(object):

    def __init__(self, name, parent =None, nodePath = None, animBranch = None):

        self._name = name
        self._children = []
        self._parent = parent

        self._path = nodePath
        self._branch = animBranch

        if parent is not None:
            parent.addChild(self) 
    

    def typeInfo(self):
        return "Selection Set"

    def addChild(self, child):
        self._children.append(child)
        
    def insertChild(self, position, child):
        if position <0 or position >len(self._children):
            return False
        
        self._children.insert(position, child)
        child._parent = self
        return True
        
    def removeChild(self, position):
        if position <0 or position >len(self._children):
            return False
        
        child = self._children.pop(position)
        child._parent = None
        
        return True

    # getters	
    def name(self):
        return self._name

    def displayName(self):
        lFileName = str(self._name)

        fName = lFileName.replace('%40','@')		
        return fName

    def child(self, row):
        return self._children[row]

    def childCount(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def path(self):
        return self._path
    
    def branch(self):
        return self._branch

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)
        

    
    # setters	
    def setName(self, name):
        self._name = name

class SelSetFolderNode(SelectionSetNode):

    def __init__(self, name, parent = None, nodePath = None, animBranch = None):
        super(SelSetFolderNode, self).__init__(name, parent, nodePath, animBranch)

    def typeInfo(self):
        return "File Folder"
        
class SelSetFileNode(SelectionSetNode):

    def __init__(self, name, parent = None, nodePath = None, animBranch = None):
        super(SelSetFileNode, self).__init__(name, parent, nodePath, animBranch)

    def typeInfo(self):
        return "anim depot File"

class SelSetFileWorkspaceNode(SelectionSetNode):

    def __init__(self, name, parent = None, nodePath = None, animBranch = None):
        super(SelSetFileWorkspaceNode, self).__init__(name, parent, nodePath, animBranch)

    def typeInfo(self):
        return "anim workspace File"

	
class SelectionSetModel(QtCore.QAbstractItemModel):
    """ Inputs: Node, QObject"""
    def __init__(self, root, parent = None):
        super(SelectionSetModel, self).__init__(parent)

        ### private attributes ###
        self._rootNode = root
        self._branchRootPath = None

    filterRole = QtCore.Qt.UserRole	
    typeRole = QtCore.Qt.UserRole +1
    pathRole = QtCore.Qt.UserRole + 2

    ### View needs to know how many items this model contains ###
    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def rowCount(self, parent = QtCore.QModelIndex()):
        if not parent.isValid():
            parentNode = self._rootNode
        else:
            parentNode = parent.internalPointer()

        return parentNode.childCount()

    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def columnCount(self, parent):	
        return 2

    """ Inputs: int, Qt::Orientation, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if section == 0:
                    return "Named Selection Sets"

                elif section == 1:
                    return "Branch"
                
                else:
                    return "Path"				



    """ Inputs: QModelIndex"""
    """ Outputs: int (flag)"""	
    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    """ Inputs: QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return the parent of the node with the given QModelIndex"""
    def parent(self, index):
        node = self.getNode(index)
        parentNode = node.parent()

        if parentNode == self._rootNode:
            #root node, create an empty QModelIndex
            return QtCore.QModelIndex()

        # wrap in a QModelIndex
        return self.createIndex(parentNode.row(), 0, parentNode)

    """ Inputs: int, int, QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return a QModelIndex that corresponds to the given row, column, and parent node"""	
    def index(self, row, column, parent):
        # is the parent valid (when QmodelIndex is empty)
        parentNode = self.getNode(parent)

        childItem = parentNode.child(row)

        #if there are no children
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()	


    ### returns True or False depending on whwther the data was valid or not    ###
    """ Inputs: QModelIndex, QVariant, int (flag)"""

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        if index.isValid():			
            if role == QtCore.Qt.EditRole:
                
                node = index.internalPointer()
                
                if index.column() == 0:
                    node.setName(value)
                    self.dataChanged.emit(index, index)
                return True

        return False

    """ Inputs: QModelIndex, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def data(self, index, role):

        if not index.isValid():
            return None
        node = index.internalPointer()

        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            if index.column() == 0:
                return node.name()
            elif index.column() == 1:
                return node.branch()
            elif index.column() == 2:
                return node.path()

        if role == QtCore.Qt.DecorationRole:
            if index.column() == 0:
                typeInfo = node.typeInfo()

                if typeInfo == "File Folder":
                    return QtGui.QIcon(QtGui.QPixmap(":/folder.png"))
                elif typeInfo == "anim depot File":
                    return QtGui.QIcon(QtGui.QPixmap(":/depotFile_icon.png"))
                else:
                    return QtGui.QIcon(QtGui.QPixmap(":/workspaceFile_icon.png"))

        # May be a hack, but this is to extract the data from the selected node for now
        if role == self.filterRole:

            return node.displayName()

        if role == self.pathRole:

            return node.path()

        if role == self.typeRole:

            return node.typeInfo()		


    """ Inputs: QModelIndex"""

    def getNode(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self._rootNode

    def setbranchRootPath(self, path):
        self._branchRootPath = path

    def branchRootPath(self):
        return self._branchRootPath
    
    #=====================================================================#
    # INSERTING AND REMOVING
    #=====================================================================#
    def insertRows(self, position, rows, node, parent = QtCore.QModelIndex()):
        #selSetModel.SelectionSetNode(setname, self._SetsRootnode, animBranch = self._dlcBranch)
        parentNode = self.getNode(parent)

        self.beginInsertRows(parent, position, position + rows - 1)

        for row in range(rows):

            childNode = node
            success = parentNode.insertChild(position, childNode)

        self.endInsertRows()

        return success

    def removeRows(self, position, rows, parent = QtCore.QModelIndex()):
        
        parentNode = self.getNode(parent)

        self.beginRemoveRows(parent, position, position + rows - 1)

        # DO Removing HERE
        for i in range(rows):
            success = parentNode.removeChild(position)

        self.endRemoveRows()	

        return success





