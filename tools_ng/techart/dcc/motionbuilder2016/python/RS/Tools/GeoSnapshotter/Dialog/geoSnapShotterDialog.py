"""
GeoSnapShotting dialog tool, contains the widgets to do the snapshotting logic for the currently
used method
"""
from PySide import QtGui

from RS.Tools import UI
from RS.Tools.GeoSnapshotter.Widgets import currentSelectionWidget, characterWidget, referenceSystemWidget


class GeoSnapShotterDialog(UI.QtMainWindowBase):
    def __init__(self, parent=None):
        """
        Constructor
        """
        self._toolName = "GeoSnapShotter"
        super(GeoSnapShotterDialog, self).__init__(title=self._toolName, dockable=True)
        self.SetupUi()

    def SetupUi(self):
        """
        Internal Method

        Setup the UI, setting up the connections
        """
        # Create Layout
        layout = QtGui.QVBoxLayout()
        widget = QtGui.QWidget()
        
        # Create Widgets
        self._snapshotTypesWidget = QtGui.QTabWidget()
        snapshotButton = QtGui.QPushButton("Snapshot!")
        self._currentSelectionWidget = currentSelectionWidget.CurrentSelectionWidget()
        self._characterWidget = characterWidget.CharacterWidget()
        self._refWidget = referenceSystemWidget.ReferenceSystemWidget()

        # Configure Widgets
        self._snapshotTypesWidget.addTab(self._currentSelectionWidget, "Current Selection")
        self._snapshotTypesWidget.addTab(self._characterWidget, "Character")
        self._snapshotTypesWidget.addTab(self._refWidget, "Reference System")
    
        # Add Widgets to Layouts
        layout.addWidget(self._snapshotTypesWidget)
        layout.addWidget(snapshotButton)
        widget.setLayout(layout)
        self.setCentralWidget(widget)
        
        # Setup Signals
        snapshotButton.pressed.connect(self._handleSnapshotButtonPress)

    def _handleSnapshotButtonPress(self):
        """
        Internal Method
        
        Handle the snapshot button being pressed
        """
        self._snapshotTypesWidget.currentWidget().runSnapShotLogic()