"""
Snapshotting geo widget for the characters. Lets users select the character and the group name to 
place the parts under
"""
from PySide import QtCore, QtGui

import pyfbsdk as mobu

from RS.Core.Geometry import Snapshot
from RS.Core.Scene.models import characterModelTypes


class CharacterWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(CharacterWidget, self).__init__(parent=parent)
        self.SetupUi()

    def SetupUi(self):
        """
        Internal Method

        Setup the UI, setting up the connections
        """
        layout = QtGui.QVBoxLayout()
        self._groupNameTextEdit = QtGui.QLineEdit()
        self._character = QtGui.QComboBox()
        
        self._model = characterModelTypes.CharacterModel()
        self._character.setModel(self._model)
        self._groupNameTextEdit.setText("Snapshot_")
        
        layout.addWidget(QtGui.QLabel("Character to Snapshot:"))
        layout.addWidget(self._character)
        layout.addWidget(QtGui.QLabel("New parent Prefix Name:"))
        layout.addWidget(self._groupNameTextEdit)
        self.setLayout(layout)

    def runSnapShotLogic(self):
        """
        Run the snapshotting logic for this widget
        """
        char = self._character.itemData(self._character.currentIndex(), QtCore.Qt.UserRole)
        if char is None:
            QtGui.QMessageBox.critical(self, "GeoSnapshotter", "No Character currently selected")
            return
        dupItems = Snapshot.Snapshot.shapshotCharacter(char)
        newParent = mobu.FBModelNull("{}{}".format(str(self._groupNameTextEdit.text()), char.Name))
        for dupItem in dupItems:
            dupItem.Parent = newParent
