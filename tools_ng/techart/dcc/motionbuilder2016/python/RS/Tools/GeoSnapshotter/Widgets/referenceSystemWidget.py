"""
Snapshotting geo widget for the characters. Lets users select the character and the group name to 
place the parts under
"""
from PySide import QtCore, QtGui

import pyfbsdk as mobu

from RS.Core.ReferenceSystem import Manager
from RS.Core.Geometry import Snapshot


class ReferenceSystemWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(ReferenceSystemWidget, self).__init__(parent=parent)
        self.SetupUi()

    def SetupUi(self):
        """
        Internal Method

        Setup the UI, setting up the connections
        """
        layout = QtGui.QVBoxLayout()
        refreshButton = QtGui.QPushButton("Refresh")
        self._refCombo = QtGui.QComboBox()
        
        layout.addWidget(QtGui.QLabel("Reference to Snapshot:"))
        layout.addWidget(refreshButton)
        layout.addWidget(self._refCombo)
        self.setLayout(layout)
        
        refreshButton.released.connect(self._populateReferenceCombo)
        
        self._populateReferenceCombo()
        
    def _populateReferenceCombo(self):
        self._refCombo.clear()
        for refItem in Manager.Manager().GetReferenceListAll():
            self._refCombo.addItem("{} ({})".format(refItem.Name, refItem.FormattedTypeName), refItem)

    def runSnapShotLogic(self):
        """
        Run the snapshotting logic for this widget
        """
        char = self._refCombo.itemData(self._refCombo.currentIndex(), QtCore.Qt.UserRole)
        if char is None:
            QtGui.QMessageBox.critical(self, "GeoSnapshotter", "No Reference selected")
            return
        Manager.Manager().CreateReferenceSnapshot(char)
        
        char.GetSnapshots()