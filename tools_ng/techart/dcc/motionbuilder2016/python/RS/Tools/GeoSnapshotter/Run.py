from RS.Tools.GeoSnapshotter.Dialog import geoSnapShotterDialog


def Run(show=True):
    toolsDialog = geoSnapShotterDialog.GeoSnapShotterDialog()
    if show:
        toolsDialog.show()
    return toolsDialog