"""
Snapshotting geo widget for the current selction. Lets users enter the group name to place the 
parts under
"""
from PySide import QtGui

import pyfbsdk as mobu

from RS.Core.Geometry import Snapshot
from RS.Utils import Scene


class CurrentSelectionWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(CurrentSelectionWidget, self).__init__(parent=parent)
        self.SetupUi()

    def SetupUi(self):
        """
        Internal Method

        Setup the UI, setting up the connections
        """
        layout = QtGui.QVBoxLayout()
        self._groupNameTextEdit = QtGui.QLineEdit()
        
        self._groupNameTextEdit.setText("Snapshot_")
        
        layout.addWidget(QtGui.QLabel("New parent Name:"))
        layout.addWidget(self._groupNameTextEdit)
        self.setLayout(layout)

    def runSnapShotLogic(self):
        """
        Run the snapshotting logic for this widget
        """
        items = Scene.GetSelection(mobu.FBModel)
        if len(items) == 0:
            QtGui.QMessageBox.critical(self, "GeoSnapshotter", "No Items currently selected")
            return
        dupItems = Snapshot.Snapshot.shapshotModels(items)
        newParent = mobu.FBModelNull(str(self._groupNameTextEdit.text()))
        for dupItem in dupItems:
            dupItem.Parent = newParent