'''
RS.Tools.UI.Watchstar

Author:

    Jason Hayes <jason.hayes@rockstarsandiego.com>
    Thanh Phan <thanh.phan@rockstarsandiego.com>

Description:

    GUI for Watchstar.

Wiki:

    https://devstar.rockstargames.com/wiki/index.php/Watchstar_Tool

'''

import os

from PySide import QtCore, QtGui
from pyfbsdk import *
from pythonidelib import *

import RS.Tools.UI
import RS.Config
import RS.Core.Face.BatchFaceware_Lib
import xml.etree.cElementTree


#reload( RS.Tools.UI )
#reload( RS.Config )
#reload( RS.Core.Face.BatchFaceware_Lib )

#compile .ui file to populate the main window
RS.Tools.UI.CompileUi( '{0}\\RS\\Tools\\UI\\Face\\QT\\batchTool_QTDesigner.ui'.format( RS.Config.Script.Path.Root ),'{0}\\RS\\Tools\\UI\\Face\\QT\\batchTool_QTDesigner.py'.format( RS.Config.Script.Path.Root ) )

import RS.Tools.UI.Face.QT.batchTool_QTDesigner as qtCompiled
reload( qtCompiled )

class BatchDefinition():
    def __init__ ( self, gui ):
        
        self.__batchName = ""
        
        self.__analyzer = gui.group_analyzer.isChecked()
        self.__pathVid = str( gui.edit_pathVid.text() )
        self.__neutralFrame = str( gui.edit_neutralFrame.text() )
        self.__pathResultAnal = str( gui.edit_pathResultAnal.text() )
        self.__sourceExtAnal = ""
        self.__rotateImage = gui.combo_rotateImage.currentIndex()
        self.__burnIn = gui.check_burnIn.isChecked()
        self.__includeResult = gui.check_includeResult.isChecked()
        self.__server = str( gui.edit_server.text() )
        self.__user = str( gui.edit_user.text() )
        self.__password = str( gui.edit_password.text() )
        
        self.__mb = gui.group_mb.isChecked()
        self.__charFBX = str( gui.edit_charFBX.text() )
        self.__charXML = str( gui.edit_charXML.text() )
        self.__pathFWR = str( gui.edit_pathFWR.text() )
        self.__pathResult = str( gui.edit_pathResult.text() )
        self.__testCTRL = str( gui.edit_testCTRL.text() )
        self.__sourceExt = ""
        self.__radio_singleFile = gui.radio_singleFile.isChecked()
        self.__radio_multiFile = gui.radio_multiFile.isChecked()
        self.__fileName = str( gui.edit_fileName.text() )
        
        
        if gui.edit_batchName.text() == "":
            self.__batchName = "** New Batch"
        else:
            self.__batchName = str( gui.edit_batchName.text() )
            
        if not gui.edit_sourceExt.text().startswith("."):
            self.__sourceExt = ".{0}".format( str( gui.edit_sourceExt.text() ) )
        else:
            self.__sourceExt = str( gui.edit_sourceExt.text() )
            
        if not gui.edit_sourceExtAnal.text().startswith("."):
            self.__sourceExtAnal = ".{0}".format( str( gui.edit_sourceExtAnal.text() ) )
        else:
            self.__sourceExtAnal = str( gui.edit_sourceExtAnal.text() )
        

    def printAll( self ):
        print "batch name: {0}".format( self.__batchName )
        print "char FBX: {0}".format( self.__charFBX )
        print "char XML: {0}".format( self.__charXML )
        print "path to FWR files: {0}".format( self.__pathFWR )
        print "path to save results: {0}".format( self.__pathResult )
        print "test controller: {0}".format( self.__testCTRL )
        print "source file ext: {0}".format( self.__sourceExt )
        print "radio for single: {0}".format( self.__radio_singleFile )
        print "radio for multi: {0}".format( self.__radio_multiFile )
        print "file name: {0}".format( self.__fileName )
        FlushOutput()

    def batchName( self ):
        return self.__batchName
    
    def analyzer( self ):
        return self.__analyzer
    
    def pathVid( self ):
        return self.__pathVid
    
    def neutralFrame( self ):
        return self.__neutralFrame
    
    def pathResultAnal( self ):
        return self.__pathResultAnal
    
    def sourceExtAnal( self ):
        return self.__sourceExtAnal
    
    def rotateImage( self ):
        return self.__rotateImage
    
    def burnIn( self ):
        return self.__burnIn
    
    def includeResult( self ):
        return self.__includeResult
    
    def server( self ):
        return self.__server
    
    def user( self ):
        return self.__user
    
    def password( self ):
        return self.__password
    
    def mb( self ):
        return self.__mb

    def charFBX( self ):
        return self.__charFBX

    def charXML( self ):
        return self.__charXML

    def pathFWR( self ):
        return self.__pathFWR

    def pathResult( self ):
        return self.__pathResult

    def testCTRL( self ):
        return self.__testCTRL

    def sourceExt( self ):
        return self.__sourceExt

    def radio_singleFile( self ):
        return self.__radio_singleFile

    def radio_multiFile( self ):
        return self.__radio_multiFile

    def fileName( self ):
        return self.__fileName





class BatchstarTool( RS.Tools.UI.QtMainWindowBase, qtCompiled.Ui_BatchTool ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, title = 'Watchstar', size = [ 900, 450 ] )

        self.setupUi( self )

        self.__batchArray = []
        
        self.group_analyzer.setChecked( False )
        self.group_mb.setChecked( False )
        print self.combo_rotateImage.currentIndex()
        FlushOutput()
        # Event Handler Bindings
        #

        self.but_browse_charFBX.released.connect( lambda: self.but_browse_file( self.edit_charFBX, "\\art\\animation\\resources\\characters\\Models", "*.fbx" ) )
        self.but_browse_charXML.released.connect( lambda: self.but_browse_file( self.edit_charXML, "\\art\\animation\\resources\\characters\\Models\\cutscene\\Faceware_Setup", "*.xml" ) )
        self.but_browse_pathFWR.released.connect( lambda: self.but_browse_path( self.edit_pathFWR, "" ) )
        self.but_browse_pathResult.released.connect( lambda: self.but_browse_path( self.edit_pathResult, "" ) )
        
        self.but_browse_neutralFrame.released.connect( lambda: self.but_browse_file( self.edit_neutralFrame, "", "*.fwlf" ) )
        self.but_browse_pathVid.released.connect( lambda: self.but_browse_path( self.edit_pathVid, "" ) )
        self.but_browse_pathResultAnal.released.connect( lambda: self.but_browse_path( self.edit_pathResultAnal, "" ) )
        
        self.but_createEntry.released.connect( self.but_createEntry_pressed )
        self.but_deleteEntry.released.connect( self.but_deleteEntry_pressed )
        self.but_processSelected.released.connect( self.but_processSelected_pressed )
        self.but_processAll.released.connect( self.but_processAll_pressed )
        self.but_saveList.released.connect( self.but_saveList_pressed )
        self.but_loadList.released.connect( self.but_loadList_pressed )
        
        self.list_entries.itemClicked.connect( self.but_editEntry_pressed )
        
        #
        # ==


    ### Event Handlers ###
    
    def but_createEntry_pressed ( self ):
        batchEntry = BatchDefinition( self )
        self.addEntry( batchEntry )
        self.refreshList()
        
    def but_editEntry_pressed ( self, item ):
        currentIndex = self.list_entries.currentRow()
        if currentIndex != -1:
            self.edit_batchName.setText( self.__batchArray[ currentIndex ].batchName() )
            
            self.group_analyzer.setChecked( self.__batchArray[ currentIndex ].analyzer() )
            self.edit_pathVid.setText( self.__batchArray[ currentIndex ].pathVid() )
            self.edit_neutralFrame.setText( self.__batchArray[ currentIndex ].neutralFrame() )
            self.edit_pathResultAnal.setText( self.__batchArray[ currentIndex ].pathResultAnal() )
            self.edit_sourceExtAnal.setText( self.__batchArray[ currentIndex ].sourceExtAnal() )
            self.combo_rotateImage.setCurrentIndex( self.__batchArray[ currentIndex ].rotateImage() )
            self.check_burnIn.setChecked( self.__batchArray[ currentIndex ].burnIn() )
            self.check_includeResult.setChecked( self.__batchArray[ currentIndex ].includeResult() )
            self.edit_server.setText( self.__batchArray[ currentIndex ].server() )
            self.edit_user.setText( self.__batchArray[ currentIndex ].user() )
            self.edit_password.setText( self.__batchArray[ currentIndex ].password() )
            
            self.group_mb.setChecked( self.__batchArray[ currentIndex ].mb() )
            self.edit_charFBX.setText( self.__batchArray[ currentIndex ].charFBX() )
            self.edit_charXML.setText( self.__batchArray[ currentIndex ].charXML() )
            self.edit_pathFWR.setText( self.__batchArray[ currentIndex ].pathFWR() )
            self.edit_pathResult.setText( self.__batchArray[ currentIndex ].pathResult() )
            self.edit_testCTRL.setText( self.__batchArray[ currentIndex ].testCTRL() )
            self.edit_sourceExt.setText( self.__batchArray[ currentIndex ].sourceExt() )
            self.edit_fileName.setText( self.__batchArray[ currentIndex ].fileName() )
            self.radio_singleFile.setChecked( self.__batchArray[ currentIndex ].radio_singleFile() )
            self.radio_multiFile.setChecked( self.__batchArray[ currentIndex ].radio_multiFile() )
            
    def but_deleteEntry_pressed ( self ):
        currentIndex = self.list_entries.currentRow()
        if currentIndex != -1:
            self.__batchArray.pop( currentIndex )
        self.refreshList()

    def but_browse_file ( self, textField, startPath, fileExt ):
        gProjRoot = RS.Config.Project.Path.Root
        lFilePopup = QtGui.QFileDialog.getOpenFileName(self, 'Open File', ( gProjRoot + startPath ), fileExt )
        if len( lFilePopup[0] ) > 0 :
            textField.setText( lFilePopup[0] )

    def but_browse_path ( self, textField, startPath ):
        gProjRoot = RS.Config.Project.Path.Root
        lFilePopup = QtGui.QFileDialog.getExistingDirectory(self, 'Open File', ( gProjRoot + startPath ) )
        if len( lFilePopup ) > 0 :
            textField.setText( lFilePopup )

    def addEntry ( self, batchEntry ):
        itemIndex = None
        for each in self.__batchArray:
            if batchEntry.batchName() == each.batchName():
                itemIndex = self.__batchArray.index( each )
        if itemIndex != None:
            reply = QtGui.QMessageBox.question(self, 'Message', "Job entry exists.  Overwrite?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                self.__batchArray.pop( itemIndex )
                self.__batchArray.insert( itemIndex, batchEntry )
            else:
                pass
        else:
            self.__batchArray.append( batchEntry )

    def but_loadList_pressed( self ):
        gProjRoot = RS.Config.Project.Path.Root
        lFilePopup = QtGui.QFileDialog.getOpenFileName(self, 'Load File', ( gProjRoot ), "*.fwb" )
        if len( lFilePopup[0] ) > 0:
            self.__batchArray = []
            doc = xml.etree.cElementTree.parse( lFilePopup[0] )
            root = doc.getroot()
            
            if root:
                jobs = root.findall( 'batchjob' )
                
                def str2bool(v):
                    return v.lower() in ("yes", "true", "t", "1")                
                for job in jobs:
                    self.edit_batchName.setText( job.get( 'name' ) )
                    
                    self.group_mb.setChecked( str2bool( job.get( 'mb' ) ) )
                    self.edit_charFBX.setText( job.get( 'charFBX' ) )
                    self.edit_charXML.setText( job.get( 'charXML' ) )
                    self.edit_pathFWR.setText( job.get( 'pathFWR' ) )
                    self.edit_pathResult.setText( job.get( 'pathResult' ) )
                    self.edit_fileName.setText( job.get( 'fileName' ) )
                    self.edit_testCTRL.setText( job.get( 'testCTRL' ) )
                    self.edit_sourceExt.setText( job.get( 'sourceExt' ) )
                    self.radio_singleFile.setChecked( str2bool( job.get( 'radio_single' ) ) )
                    self.radio_multiFile.setChecked( str2bool( job.get( 'radio_multi' ) ) )
                    
                    self.group_analyzer.setChecked( str2bool( job.get( 'analyzer' ) ) )
                    self.edit_pathVid.setText( job.get( 'pathVid' ) )
                    self.edit_neutralFrame.setText( job.get( 'neutralFrame' ) )
                    self.edit_pathResultAnal.setText( job.get( 'pathResultAnal' ) )
                    self.edit_sourceExtAnal.setText( job.get( 'sourceExtAnal' ) )
                    self.combo_rotateImage.setCurrentIndex( int( job.get( 'rotateImage' ) ) )
                    self.check_burnIn.setChecked( str2bool( job.get( 'burnIn' ) ) )
                    self.check_includeResult.setChecked( str2bool( job.get( 'includeResult' ) ) )
                    self.edit_server.setText( job.get( 'server' ) )
                    self.edit_user.setText( job.get( 'user' ) )
                    self.edit_password.setText( job.get( 'password' ) )
                    
                    batchEntry = BatchDefinition( self )
                    self.addEntry( batchEntry )
                
        self.refreshList()        
    
    def but_saveList_pressed( self ):
        gProjRoot = RS.Config.Project.Path.Root
        lFilePopup = QtGui.QFileDialog.getSaveFileName(self, 'Save File', ( gProjRoot ), "*.fwb" )
        if len( lFilePopup[0] ) > 0 :
            
            fstream = open( lFilePopup[0], 'w' )
            fstream.write( '<?xml version="1.0" encoding="utf-8"?>\n' )
            fstream.write( '<data>\n' )
            
            print len( self.__batchArray)
            FlushOutput()
            for i in range( 0,  len( self.__batchArray ) ) :
                x = self.__batchArray[ i ]
                print x.batchName()
                FlushOutput()
                fstream.write('\t<batchjob name="{0}" mb="{1}" charFBX="{2}" charXML="{3}" pathFWR="{4}" pathResult="{5}" fileName="{6}" testCTRL="{7}" sourceExt="{8}" radio_single="{9}" radio_multi="{10}" analyzer="{11}" pathVid="{12}" neutralFrame="{13}" pathResultAnal="{14}" sourceExtAnal="{15}" rotateImage="{16}" burnIn="{17}" includeResult="{18}" server="{19}" user="{20}" password="{21}" />\n'.format(
                    x.batchName(),
                    x.mb(),
                    x.charFBX(),
                    x.charXML(),
                    x.pathFWR(),
                    x.pathResult(),
                    x.fileName(),
                    x.testCTRL(),
                    x.sourceExt(),
                    x.radio_singleFile(),
                    x.radio_multiFile(),
                    x.analyzer(),
                    x.pathVid(),
                    x.neutralFrame(),
                    x.pathResultAnal(),
                    x.sourceExtAnal(),
                    x.rotateImage(),
                    x.burnIn(),
                    x.includeResult(),
                    x.server(),
                    x.user(),
                    x.password()
                    ))
            
            fstream.write( '</data>\n' )
            fstream.close()
    
    def refreshList ( self ):
        self.list_entries.clear()
        for each in self.__batchArray:
            self.list_entries.addItem( each.batchName() )
            
    def processJob ( self, i ):
        
        lGroup_analyzer = self.__batchArray[ i ].analyzer()
        lPerformancesPathAnal = self.__batchArray[ i ].pathVid()
        lSavePathAnal = self.__batchArray[ i ].pathResultAnal()
        lSourceExtAnal = self.__batchArray[ i ].sourceExtAnal()
        lImageRotate = self.__batchArray[ i ].rotateImage()
        lBurnIn = self.__batchArray[ i ].burnIn()
        lUser = self.__batchArray[ i ].user()
        lPassword = self.__batchArray[ i ].password()
        lNeutral = self.__batchArray[ i ].neutralFrame()
        lServer = self.__batchArray[ i ].server()
        lIncludeResult = self.__batchArray[ i ].includeResult()
        
        
        lGroup_mb = self.__batchArray[ i ].mb()
        lCharacterFile = self.__batchArray[ i ].charFBX()
        lCharacterXML = self.__batchArray[ i ].charXML()
        lPerformancesPath = self.__batchArray[ i ].pathFWR()
        lResultPath = self.__batchArray[ i ].pathResult()
        lResultFileName = "{0}.fbx".format( self.__batchArray[ i ].fileName() )
        lTestControl = self.__batchArray[ i ].testCTRL()
        lVideoExt = self.__batchArray[ i ].sourceExt()
        lSingleFile = self.__batchArray[ i ].radio_singleFile()
        lImportVid = not lSingleFile
        
        
        if lGroup_analyzer:
            print "Analyzer Processing this job: {0}".format( self.__batchArray[ i ].batchName() )
            
            RS.Core.Face.BatchFaceware_Lib.startAnalyzer( lPerformancesPathAnal, lSourceExtAnal, lSavePathAnal, lImageRotate, lBurnIn, lUser, lPassword, lNeutral, lServer, lIncludeResult )
        else:
            print "This job contains no Analyzer processing: {0}".format( self.__batchArray[ i ].batchName() )
        
        if lGroup_mb:
            print "Processing this job: {0}".format( self.__batchArray[ i ].batchName() )
            RS.Core.Face.BatchFaceware_Lib.createPerformance( lCharacterFile, lCharacterXML, lPerformancesPath, lResultPath, lResultFileName, lTestControl, lVideoExt, lSingleFile, lImportVid )
        else:
            print "This job contains no MotionBuilder processing: {0}".format( self.__batchArray[ i ].batchName() )
        
            
        FlushOutput()

    def but_processSelected_pressed ( self ):
        currentIndex = self.list_entries.currentRow()
        self.processJob( currentIndex )
        
    def but_processAll_pressed ( self ):
        for i in range (0, len( self.__batchArray ) ):
            self.processJob( i )
            
    def check_analyzer_stateChanged ( self ):
        self.group_analyzer.setChecked( bool(self.check_analyzer.isChecked()) )
        print self.group_analyzer.isChecked()
        print self.check_analyzer.isChecked()
        FlushOutput()
    
    ### Overrides ###
    def closeEvent( self, event ):

        QtGui.QMainWindow.closeEvent( self, event )


def Run():
    tool = BatchstarTool()
    tool.show()