"""
UI interface for layering previz animation
"""
import os

import pyfbsdk as mobu

from PySide import QtCore, QtGui

from RS.Tools import UI
from RS import Globals, Config
from RS.Core.Mocap import Clapper
from RS.Utils.Scene import Character, Groups
from RS.Core.Mocap.Previz import LayeringTool
from RS.Tools.UI.Mocap.Models import characterModel, propsModel
from RS.Tools.VirtualProduction import const
from RS.Tools.CameraToolBox.PyCoreQt.Delegates import comboboxDelegate


class LayeringToolUI(UI.QtMainWindowBase):
    """
    GUI Tool for the layering tool
    """
    def __init__(self):
        """
        Constructor
        """
        self._toolName = "Previz Animation Layering Tool"
        super(LayeringToolUI, self).__init__(title=self._toolName, dockable=True)

        self.setupUi()

    def setupUi(self):
        """
        Set up the UI and attach the connections
        """
        # Layouts
        mainWidget = QtGui.QWidget()
        layout = QtGui.QGridLayout()
        zeroControlLayout = QtGui.QGridLayout()

        # Widgets
        banner = UI.BannerWidget(self._toolName, helpUrl=r"https://hub.gametools.dev/display/RPERFCAP/Previs+Animation+Layering+Tool")
        self._characterTreeView = QtGui.QTreeView()
        self._propTreeView = QtGui.QTreeView()
        self._layerNameTextbox = QtGui.QLineEdit()
        createLayerButton = QtGui.QPushButton("Create Layer with Character Models\nfor Selected Characters")
        createLayerRefPoseButton = QtGui.QPushButton("Create Layer with Ref Poses\nfor Selected Characters")
        createLayerSlateButton = QtGui.QPushButton("Create Layer with Slate Only")
        zeroControlsBox = QtGui.QGroupBox("Zero Scene Controls")
        self._clapperBeepsRadio = QtGui.QRadioButton("Clapper Beeps")
        self._clapperBeepsDrop = QtGui.QComboBox()
        self._manualFrameRadio = QtGui.QRadioButton("Manual Frame Number")
        self._manualFrameSpinner = QtGui.QSpinBox()
        self._noZeroing = QtGui.QRadioButton("No Zeroing")
        horizontalLine = QtGui.QFrame()
        
        # set model to treeview
        self._characterModel =  characterModel.CharacterModel()
        self._propsModel = propsModel.PropsModel()
        self._characterTreeView.setModel(self._characterModel)
        self._propTreeView.setModel(self._propsModel)

        # set delegate for column 2 gender type combobox
        self._comboBoxDelegate = comboboxDelegate.ComboBoxDelegate(["Male", "Female"])
        self._characterTreeView.setItemDelegateForColumn(2, self._comboBoxDelegate)
        self._colorComboBoxDelegate = comboboxDelegate.ComboBoxDelegateWithIcons(const.Icons.colorDict)
        self._characterTreeView.setItemDelegateForColumn(3, self._colorComboBoxDelegate)

        # Configure Widgets
        self._characterTreeView.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self._characterTreeView.setAlternatingRowColors(True)
        self._characterTreeView.setSortingEnabled(True)
        self._propTreeView.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self._propTreeView.setAlternatingRowColors(True)
        self._propTreeView.setSortingEnabled(True)
        
        banner.setMaximumHeight(30)
        self._clapperBeepsRadio.setChecked(True)
        self._manualFrameSpinner.setEnabled(False)
        self._manualFrameSpinner.setMaximum(999999)
        self._manualFrameSpinner.setMinimum(-999999)
        horizontalLine.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine.setFrameShadow(QtGui.QFrame.Sunken)
        self.setupToolBar(banner)

        # Add Widgets to Layouts
        layout.addWidget(banner, 0, 0, 1, 2)
        layout.addWidget(self._characterTreeView, 1, 0, 1, 2)
        layout.addWidget(self._propTreeView, 2, 0, 1, 2)
        layout.addWidget(QtGui.QLabel("Layer Name:"), 3, 0, 1, 2)
        layout.addWidget(self._layerNameTextbox, 4, 0, 1, 2)
        layout.addWidget(zeroControlsBox, 5, 0, 1, 2)
        layout.addWidget(createLayerButton, 6, 0)
        layout.addWidget(createLayerRefPoseButton, 6, 1)
        layout.addWidget(horizontalLine, 7, 0, 1, 2)
        layout.addWidget(createLayerSlateButton, 8, 0, 1, 2)

        zeroControlLayout.addWidget(self._clapperBeepsRadio, 0, 0)
        zeroControlLayout.addWidget(self._clapperBeepsDrop, 0, 1)
        zeroControlLayout.addWidget(self._manualFrameRadio, 1, 0)
        zeroControlLayout.addWidget(self._manualFrameSpinner, 1, 1)
        zeroControlLayout.addWidget(self._noZeroing, 2, 0)

        # Set Layouts to Widgets
        zeroControlsBox.setLayout(zeroControlLayout)
        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)

        # Hook up connections
        createLayerButton.pressed.connect(self._handleCreateModelLayerButton)
        createLayerRefPoseButton.pressed.connect(self._handleCreateReferenceLayerButton)
        createLayerSlateButton.pressed.connect(self._handleCreateLayerSlateButton)
        self._clapperBeepsRadio.toggled.connect(self._handleRadioButtonStateChanged)
        self._manualFrameRadio.toggled.connect(self._handleRadioButtonStateChanged)
        self._noZeroing.toggled.connect(self._handleRadioButtonStateChanged)

        # Populate the UI
        self.rePopulateModels()

    def setupToolBar(self, banner):
        """
        Adds icon to the banner

        Arguments:
            banner (RS.Tools.UI.QBanner): banner to add tool buttons to
        """
        refreshIconPath = os.path.join(Config.Script.Path.ToolImages, "ReferenceEditor", "Refresh.png")
        refreshButton = QtGui.QIcon(QtGui.QPixmap(refreshIconPath))
        banner.addButton(refreshButton, "Refreshes the Virtual Production Toolbox", self._handleRefreshButton)

    def _handleRadioButtonStateChanged(self):
        """
        Internal Method

        Handle the state change on the radio buttons
        """
        self._clapperBeepsDrop.setEnabled(self._clapperBeepsRadio.isChecked())
        self._manualFrameSpinner.setEnabled(self._manualFrameRadio.isChecked())

    def _handleRefreshButton(self):
        """
        Internal Method

        Handle the character refresh button being pressed
        """
        self.rePopulateModels()

    def rePopulateModels(self):
        """
        Force an Update all the models in the UI such as the Characters, Props, Clapper index and
        Layer Name
        """
        self._characterModel.reset()
        self._propsModel.reset()
        self._populateBleeps()
        self._populateLayerName()

    def _handleCreateModelLayerButton(self):
        """
        create layer with models
        """
        self._createLayer(referenceModel=False)

    def _handleCreateReferenceLayerButton(self):
        """
        create layer with reference poses
        """
        self._createLayer(referenceModel=True)

    def _getSelectedProps(self):
        """
        Get the selected props
        
        returns:
            list of reference objects for the selected props
        """
        rows = self._propTreeView.selectionModel().selectedRows()
        return [self._propsModel.data(idx, QtCore.Qt.UserRole).getPropObject() for idx in rows]
    
    def _getSelectedCharacters(self):
        """
        Get the selected character
        
        returns:
            list of LayeringTool.CharacterAnimationLayerContainer for the selected characters
        """
        returnData = []
        rows = self._characterTreeView.selectionModel().selectedRows()
        for item in [self._characterModel.data(idx, QtCore.Qt.UserRole) for idx in rows]:
            colourRGB = const.Icons.colorDict[item.getUserColor()][-1]
            char = item.getCharacterObject()
            gender = item.getGender()
            returnData.append(LayeringTool.CharacterAnimationLayerContainer(char, gender, colourRGB))
        return returnData

    def _createLayer(self, referenceModel=False):
        """
        Internal Method

        Handle the layer button being pressed
        """
        if Clapper.GetTakeSlateGroup() is None:
            QtGui.QMessageBox.critical(self, self._toolName, "No Slate Group Detected. Please add the slate to the previz slate group. Aborting")
            return

        # Get the offset frame number
        offsetFrame = None
        if self._clapperBeepsRadio.isChecked():
            currentText = self._clapperBeepsDrop.currentText()
            if currentText == "":
                QtGui.QMessageBox.warning(self, self._toolName, "No Clapper Frame Choosen")
                return
            offsetFrame = int(currentText)
        elif self._manualFrameRadio.isChecked():
            offsetFrame = self._manualFrameSpinner.value()

        props = self._getSelectedProps()
        chars = self._getSelectedCharacters()

        if len(chars) == 0 and len(props) == 0:
            QtGui.QMessageBox.warning(self, self._toolName, "No Characters or Props Selected")
            return

        # Get the layer name text
        layerName = self._layerNameTextbox.text()

        # Run the library method
        LayeringTool.AnimationLayering.layerAnimation(chars, offsetFrame, layerName=layerName, props=props, referenceModel=referenceModel)

        # Refresh with changes and alert the user the job is complete
        self.rePopulateModels()
        self._noZeroing.setChecked(True)
        QtGui.QMessageBox.information(self, self._toolName, "Layering Complete")

    def _handleCreateLayerSlateButton(self):
        """
        Internal Method

        Handle the layer slate only button being pressed
        """
        if Clapper.GetTakeSlateGroup() is None:
            QtGui.QMessageBox.critical(self, self._toolName, "No Slate Group Detected. Please add the slate to the previz slate group. Aborting")
            return

        # Get the offset frame number
        offsetFrame = None
        if self._clapperBeepsRadio.isChecked():
            currentText = self._clapperBeepsDrop.currentText()
            if currentText == "":
                QtGui.QMessageBox.warning(self, self._toolName, "No Clapper Frame Choosen")
                return
            offsetFrame = int(currentText)
        elif self._manualFrameRadio.isChecked():
            offsetFrame = self._manualFrameSpinner.value()

        # Get the layer name text
        layerName = self._layerNameTextbox.text()

        # Run the library method
        LayeringTool.AnimationLayering.layerAnimation(offsetFrame=offsetFrame, layerName=layerName)

        # Refresh with changes and alert the user the job is complete
        self._noZeroing.setChecked(True)
        QtGui.QMessageBox.information(self, self._toolName, "Layering for the Slate is Complete.")

    def _populateLayerName(self):
        """
        Internal Method

        Get the current take name and set it to the new layer name edit text box
        """
        takeName = mobu.FBSystem().CurrentTake.Name
        self._layerNameTextbox.setText(takeName)

    def _populateBleeps(self):
        """
        Internal Method

        Get all the bleeps in the slate and populate the combobox with their frame numbers
        """
        self._clapperBeepsDrop.clear()

        # Get the current Take Slate Group
        slateGroup = Clapper.GetTakeSlateGroup()
        if slateGroup is None:
            return

        # See which slate is in the current take slate group
        for slateObj, beeps in Clapper.GetClaps().iteritems():
            if slateGroup in Groups.GetGroupsFromObject(slateObj):
                self._clapperBeepsDrop.addItems([str(beep) for beep in beeps])
                self._clapperBeepsDrop.setCurrentIndex(self._clapperBeepsDrop.count() - 1)
                return


def Run(show=True):
    layeringTool = LayeringToolUI()

    if show:
        layeringTool.show()

    return layeringTool

