"""
UI To allow artists to save, load and tweak facial poses on characters
"""
import os

from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Config
from RS.Core.Scene.models import characterModelTypes
from RS.Core.Face import facialPose
from RS.Tools import UI
from RS.Tools.CameraToolBox.PyCoreQt.Widgets import checkboxButton
from RS.Utils import Namespace


class PoseToolUI(UI.QtMainWindowBase):
    """
    Main Window UI for the Facial pose tool
    """
    def __init__(self):
        self._poseManager = facialPose.RsPoseManager()
        self._settings = QtCore.QSettings("RockstarSettings", "PoseToolUI")
        self._toolName = "Facial Pose Tool"
        self._keyPose = True
        self._resettingSliderValue = False
        super(PoseToolUI, self).__init__(title=self._toolName, dockable=True)
        self.setupUi()

    def setupUi(self):
        """
        Set up the UI
        """
        # Create Models
        self._charsModel = characterModelTypes.CharacterModel()

        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()
        ioLayout = QtGui.QHBoxLayout()
        posesLayout = QtGui.QHBoxLayout()
        actionLayout = QtGui.QGridLayout()

        # Create Widgets
        mainWidget = QtGui.QWidget()
        banner = UI.BannerWidget(self._toolName)
        self._characterSelector = QtGui.QComboBox()

        saveButton = QtGui.QPushButton("Save Pose File")
        loadButton = QtGui.QPushButton("Load Pose File")

        self._poseList = QtGui.QListWidget()
        self._poseThumbnail = QtGui.QLabel()

        addPoseButton = QtGui.QPushButton("Add Pose")
        capturePoseButton = QtGui.QPushButton("Re-Capture Pose")
        renamePoseButton = QtGui.QPushButton("Rename Pose")
        deletePoseButton = QtGui.QPushButton("Delete Pose")
        selectPoseControlsButton = QtGui.QPushButton("Select Pose Controls")
        setPoseButton = QtGui.QPushButton("Set to Pose")
        takeThumbnailButton = QtGui.QPushButton("Capture new Thumbnail")
        self._keyButton = checkboxButton.CheckboxButon(
                                                       "Key",
                                                       checked=self._keyPose,
                                                       downColour=QtGui.QColor(255, 0, 0)
                                                       )

        self._blendPoseSlider = QtGui.QSlider(QtCore.Qt.Horizontal)

        # Configure Widgets
        self._characterSelector.setModel(self._charsModel)
        self._blendPoseSlider.setMaximum(100)
        self._blendPoseSlider.setMinimum(0)

        # Configure Models
        self._charsModel.setCheckable(False)

        # Assign Widgets to Layouts
        mainLayout.addWidget(banner)
        mainLayout.addWidget(QtGui.QLabel("Character:"))
        mainLayout.addWidget(self._characterSelector)
        mainLayout.addItem(ioLayout)
        mainLayout.addItem(posesLayout)
        mainLayout.addItem(actionLayout)
        mainLayout.addWidget(QtGui.QLabel("Blend Pose"))
        mainLayout.addWidget(self._blendPoseSlider)

        ioLayout.addWidget(saveButton)
        ioLayout.addWidget(loadButton)

        posesLayout.addWidget(self._poseList)
        posesLayout.addWidget(self._poseThumbnail)

        actionLayout.addWidget(addPoseButton, 0, 0)
        actionLayout.addWidget(capturePoseButton, 1, 0)
        actionLayout.addWidget(renamePoseButton, 2, 0)
        actionLayout.addWidget(deletePoseButton, 3, 0)

        actionLayout.addWidget(selectPoseControlsButton, 0, 1)
        actionLayout.addWidget(setPoseButton, 1, 1)
        actionLayout.addWidget(takeThumbnailButton, 2, 1)
        actionLayout.addWidget(self._keyButton, 3, 1)

        # Assign Layouts to Widgets
        mainWidget.setLayout(mainLayout)
        self.setCentralWidget(mainWidget)

        # Setup signals
        self._characterSelector.currentIndexChanged.connect(self._handleCharacterSelectionChange)

        loadButton.pressed.connect(self._handleLoadButton)
        saveButton.pressed.connect(self._handleSaveButton)

        self._poseList.currentItemChanged.connect(self._handlePoseListSelectionChange)

        addPoseButton.pressed.connect(self._handleAddPoseButton)
        capturePoseButton.pressed.connect(self._handleCapturePoseButton)
        renamePoseButton.pressed.connect(self._handleRenamePoseButton)
        deletePoseButton.pressed.connect(self._handleDeletePoseButton)
        selectPoseControlsButton.pressed.connect(self._handleSelectControlsButton)
        setPoseButton.pressed.connect(self._handleSetPoseButton)
        takeThumbnailButton.pressed.connect(self._handleTakeThumbnailButton)
        self._keyButton.pressed.connect(self._handleKeyButton)

        self._blendPoseSlider.valueChanged.connect(self._handleBlendPoseSlider)
        self._blendPoseSlider.sliderPressed.connect(self._handleBlendPoseSliderStart)
        self._blendPoseSlider.sliderReleased.connect(self._handleBlendPoseSliderEnd)

        # Trigger updates to the UI
        self._updateThumbnail(None)
        self._handleCharacterSelectionChange(0)

    def getCurrentlySelectedPose(self):
        """
        Get the currently selected pose or None if no pose is selected

        return:
            facialPose.RsPose object for the currently selected pose
        """
        item = self._poseList.currentItem()
        if item is None:
            return None
        return self._poseList.currentItem().data(QtCore.Qt.UserRole)

    def _getCharacterPoseFolderPath(self, characterName):
        """
        Internal Method

        Get the folder location for the thumbnail image pose location for the given character name

        args:
            characterName (str): The name of the character

        returns:
            string path for the folder
        """
        return os.path.join(
                        Config.Project.Path.Art,
                        "animation",
                        "resources",
                        "characters",
                        "Models",
                        "cutscene",
                        "Faceware_Setup",
                        "shared pose data",
                        "poseThumbnails",
                        characterName
                        )

    def _updateThumbnail(self, imagePath=None):
        """
        Internal Method

        Update the thumbnail on display in the UI

        kwargs:
            imagePath (str): The image path to show
        """
        imageRoot = os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "FacialPose"))
        if imagePath is None:
            imagePath = os.path.join(imageRoot, "noPoseSelected.png")

        if not os.path.isfile(imagePath):
            imagePath = os.path.join(imageRoot, "noPoseImage.png")

        self._poseThumbnail.setPixmap(QtGui.QPixmap(imagePath))

    def _getLastOpenSaveDir(self):
        """
        Internal Method

        Get the last open save directory file path

        return:
            string file path
        """
        return self._settings.value("lastOpenSaveDir", "/home")

    def _setLastOpenSaveDir(self, newDir):
        """
        Internal Method

        Set the last open save directory file path

        args:
            newDir (str): The string file path to save
        """
        self._settings.setValue("lastOpenSaveDir", newDir)

    def _refreshPoseList(self):
        """
        Internal Method

        Refresh the model view of the pose list from the pose manager
        """
        self._poseList.clear()
        poseNames = self._poseManager.poses.keys()
        poseNames.sort()

        for poseName in poseNames:
            newItem = QtGui.QListWidgetItem(poseName)
            newItem.setData(QtCore.Qt.UserRole, self._poseManager.poses[poseName])
            self._poseList.addItem(newItem)

    def _handleCharacterSelectionChange(self, newIndex):
        """
        Internal Method

        Handle the character selection change
        """
        if newIndex < 0:
            return
        char = self._characterSelector.itemData(newIndex, QtCore.Qt.UserRole)
        self._poseManager.namespace = Namespace.GetNamespace(char)
        self._poseManager.thumbnailFolderPath = self._getCharacterPoseFolderPath(char.Name)

    def _handleLoadButton(self):
        """
        Internal Method

        Handle the load button being pressed
        """
        filePath, notCan = QtGui.QFileDialog.getOpenFileName(
                                                 self,
                                                 'Open Pose File',
                                                 self._getLastOpenSaveDir(),
                                                 "*.{0}".format(self._poseManager.fileExtension)
            )
        if not notCan:
            return
        self._setLastOpenSaveDir(os.path.dirname(filePath))
        self._poseManager.loadPoses(filePath)
        self._refreshPoseList()

    def _handlePoseListSelectionChange(self):
        """
        Internal Method

        Handle the post list selection changing
        """
        item = self.getCurrentlySelectedPose()
        if item is None:
            self._updateThumbnail(None)
            return
        self._updateThumbnail(item.thumbNailPath)

    def _handleSaveButton(self):
        """
        Internal Method

        Handle the save button being pressed
        """
        filePath, notCan = QtGui.QFileDialog.getSaveFileName(
                                                 self,
                                                 'Save Pose File',
                                                 self._getLastOpenSaveDir(),
                                                 "*.{0}".format(self._poseManager.fileExtension)
            )
        if not notCan:
            return
        self._setLastOpenSaveDir(os.path.dirname(filePath))
        self._poseManager.savePoses(filePath)

    def _handleAddPoseButton(self):
        """
        Internal Method

        Handle the add pose button being pressed
        """
        if len(self._poseManager.getModelSelection()) == 0:
            QtGui.QMessageBox.warning(self, self._toolName, "Please select at least one object")
            return
        poseName, notCan = QtGui.QInputDialog.getText(self, "Enter Pose Name", "Pose Name:")

        if not notCan:
            return

        if poseName in self._poseManager.poses:
            QtGui.QMessageBox.warning(
                                      self,
                                      self._toolName,
                                      "Pose name already exists\nPlease choose another."
                                      )
            return
        self._poseManager.createNewPose(poseName)
        self._refreshPoseList()
        self._handlePoseListSelectionChange()

    def _handleCapturePoseButton(self):
        """
        Internal Method

        Handle the capture pose button being pressed
        """
        pose = self.getCurrentlySelectedPose()
        if pose is None:
            QtGui.QMessageBox.warning(self, self._toolName, "No Pose selected to update")
            return

        poseName = pose.name

        msg = "Are you sure you want to update the pose ({0})?".format(poseName)
        reply = QtGui.QMessageBox.question(
                                           self,
                                           self._toolName,
                                           msg,
                                           QtGui.QMessageBox.Yes,
                                           QtGui.QMessageBox.No
                                           )
        if reply == QtGui.QMessageBox.Yes:
            self._poseManager.updatePose(poseName)
            self._handlePoseListSelectionChange()

    def _handleRenamePoseButton(self):
        """
        Internal Method

        Handle the rename pose button being pressed
        """
        pose = self.getCurrentlySelectedPose()
        if pose is None:
            QtGui.QMessageBox.warning(self, self._toolName, "No Pose selected to Rename")
            return

        poseName = pose.name

        newPoseName, notCan = QtGui.QInputDialog.getText(
                                                         self,
                                                         "Rename Pose",
                                                         "Pose Name:",
                                                         text=oldPoseName
                                                         )

        if not notCan:
            return

        if newPoseName in self._poseManager.poses:
            QtGui.QMessageBox.warning(
                                      self,
                                      self._toolName,
                                      "Pose name already exists\nPlease choose another."
                                      )
            return

        self._poseManager.renamePose(poseName, newPoseName)
        self._refreshPoseList()

    def _handleDeletePoseButton(self):
        """
        Internal Method

        Handle the delete pose button being pressed
        """
        pose = self.getCurrentlySelectedPose()
        if pose is None:
            QtGui.QMessageBox.warning(self, self._toolName, "Please select a pose to delete")
            return

        msg = "Are you sure you want to delete the pose ({0})?".format(pose.name)
        reply = QtGui.QMessageBox.question(
                                           self,
                                           "Delete Pose",
                                           msg,
                                           QtGui.QMessageBox.Yes,
                                           QtGui.QMessageBox.No
                                           )
        if reply == QtGui.QMessageBox.Yes:
            self._poseManager.deletePose(pose.name)
            self._refreshPoseList()

    def _handleSelectControlsButton(self):
        """
        Internal Method

        Handle the Select Controls button being pressed
        """
        pose = self.getCurrentlySelectedPose()
        if pose is None:
            QtGui.QMessageBox.warning(self, self._toolName, "Please select a pose from the list")
            return

        self._poseManager.selectPoseControls(pose.name)

    def _handleSetPoseButton(self):
        """
        Internal Method

        Handle the set pose button being pressed
        """
        pose = self.getCurrentlySelectedPose()
        if pose is None:
            QtGui.QMessageBox.warning(self, self._toolName, "Please select a pose")
            return
        self._poseManager.snapshot(pose.name)
        self._poseManager.blendPose(pose.name, 1.0, key=self._keyPose)

    def _handleTakeThumbnailButton(self):
        """
        Internal Method

        Handle the Take Thumbnail button being pressed
        """
        pose = self.getCurrentlySelectedPose()
        if pose is None:
            QtGui.QMessageBox.warning(self, self._toolName, "No Pose selected to Update Thumbnail")
            return
        self._poseManager.updatePoseThumbnail(pose.name)
        self._handlePoseListSelectionChange()

    def _handleKeyButton(self):
        """
        Internal Method

        Handle the Key button being pressed
        """
        self._keyPose = self._keyButton.isChecked()

    def _handleBlendPoseSlider(self, newValue):
        """
        Internal Method

        Handle the blend pose slider value being changed
        """
        if self._resettingSliderValue:
            return

        pose = self.getCurrentlySelectedPose()
        if pose is None:
            QtGui.QMessageBox.warning(self, self._toolName, "Please select a pose")
            return

        self._poseManager.blendPose(pose.name, float(newValue) / 100, key=False)

    def _handleBlendPoseSliderStart(self):
        """
        Internal Method

        Handle the start of the blend pose slider being moved
        """
        pose = self.getCurrentlySelectedPose()
        if pose is None:
            QtGui.QMessageBox.warning(self, self._toolName, "Please select a pose")
            return

        self._poseManager.snapshot(pose.name)

    def _handleBlendPoseSliderEnd(self):
        """
        Internal Method

        Handle the end of the blend pose slider being moved
        """
        pose = self.getCurrentlySelectedPose()
        if pose is None:
            QtGui.QMessageBox.warning(self, self._toolName, "Please select a pose")
            return
        self._resettingSliderValue = True
        self._blendPoseSlider.setValue(0)
        self._resettingSliderValue = False
        if self._keyPose is True:
            self._poseManager.keyPose(pose.name)


def Run(show=True):
    tool = PoseToolUI()

    if show:
        tool.show()

    return tool

Run()
