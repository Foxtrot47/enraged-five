from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Globals
import RS.Tools.ToyBox
import RS.Utils.Creation
import RS.Tools.UI
import RS.Core.Mocap.MocapCommands

import os
import inspect
import itertools

global gToyBoxMarkerArray
gToyBoxMarkerArray = []

reload(RS.Tools.ToyBox)

class ToyBoxTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 
                                   'ToyBox Relation Tool', 
                                   size = [ 300, 600 ], 
                                   helpUrl = 'https://devstar.rockstargames.com/wiki/index.php/Adding_A_Toybox_to_a_Cutscene' )

    def ListCallback( self, control, event ):
        '''
        When you select things in the callback, this is the callback that is triggered to store the values appropriately.
    
        *Arguments:*
            * ''control'' This is a handle to the control that triggered the callback.
            * ''event'' This is a handle to the control that triggered the callback.            
    
        *Keyword Arguments:*
            * ''keyword argument 1'' A description of the keyword argument.
    
        *Returns:*
            * ''None'' A description of the return value.
    
        '''      
        global gToyBoxMarkerArray, b, b1, gCharPropVehicleList
        del gToyBoxMarkerArray[:]
        
        for i in range(len(gCharPropVehicleList.Items)):
            if gCharPropVehicleList.IsSelected(i):
                gToyBoxMarkerArray.append(gCharPropVehicleList.Items[i])
        
        b.Name = str(gToyBoxMarkerArray)
        b1.Name = str(gToyBoxMarkerArray)
        b7.Name = str(gToyBoxMarkerArray)
        
    def Refresh( self, pControl, pEvent ):
        '''
        Refresh UI callback
    
        *Arguments:*
            * ''pObject'' This is a handle to the control that triggered the callback.
            * ''pEventName'' This is a handle to the control that triggered the callback.
        *Keyword Arguments:*
            * ''keyword argument 1'' A description of the keyword argument.
    
        *Returns:*
            * ''None'' A description of the return value.
    
        '''      
        del gToyBoxMarkerArray[:]
        self.Show( True )

    def CheckToyBoxConstraintStatus(self):
        toyboxFolder = None
        activeStatus = True
        for folder in RS.Globals.Folders:
            if folder.Name == 'ToyBoxFolder':
                toyboxFolder = folder
                break
        if toyboxFolder is not None:
            for item in toyboxFolder.Items:
                if item.Active == False:
                    activeStatus = False
                    break
        return activeStatus

    def Create( self, mainLyt ):
        '''
        UI creation
    
        *Arguments:*
            * ''pObject'' This is a handle to the control that triggered the callback.
            * ''pEventName'' This is a handle to the control that triggered the callback.            
    
        *Keyword Arguments:*
            * ''keyword argument 1'' A description of the keyword argument.
    
        *Returns:*
            * ''None'' A description of the return value.
    
        '''       
        # Refresh UI
        lRefreshUIButton = FBButton()
        lRefreshUIButton.Caption = "Click to Refresh UI"
        lRefreshUIButton.Justify = FBTextJustify.kFBTextJustifyCenter  
        lRefreshUIButtonX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lRefreshUIButtonY = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")# Allows for the Rockstar Banner which is 30 high
        lRefreshUIButtonW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lRefreshUIButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("lRefreshUIButton","lRefreshUIButton", lRefreshUIButtonX, lRefreshUIButtonY, lRefreshUIButtonW, lRefreshUIButtonH)
        mainLyt.SetControl("lRefreshUIButton",lRefreshUIButton)
        lRefreshUIButton.OnClick.Add( self.Refresh )    
        
        # Warning
        lReferenceListX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lReferenceListY = FBAddRegionParam(60,FBAttachType.kFBAttachTop,"")
        lReferenceListW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lReferenceListH = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")  
        mainLyt.AddRegion("warning","warning", lReferenceListX,lReferenceListY,lReferenceListW,lReferenceListH)   
        
        # Characters, Vehicles, Props
        # anchored to left (mainLyt), right (mainLyt) top (Title), Bottom (mainLyt), because it's anchored to all four of the boundaries this is where the first parameter comes into play so that the tool knows what size to make it. 
        charPropVehiclex = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        charPropVehicley = FBAddRegionParam(90,FBAttachType.kFBAttachTop,"")
        charPropVehiclew = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        charPropVehicleh = FBAddRegionParam(-205,FBAttachType.kFBAttachBottom,"")
        mainLyt.AddRegion("charPropVehicle","charPropVehicle", charPropVehiclex,charPropVehicley,charPropVehiclew,charPropVehicleh)
                
        # Scene Setup Buttons
        # Anchored to left (mainLyt), and bottom (charPropVehicle), this allows me to set a height and width variable, as it will be fixed size width and height wise.
        setupTitlex = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        setupTitley = FBAddRegionParam(-200,FBAttachType.kFBAttachBottom,"")
        setupTitlew = FBAddRegionParam(268,FBAttachType.kFBAttachBottom,"charPropVehicle")
        setupTitleh = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")    
        mainLyt.AddRegion("setupTitle","setupTitle", setupTitlex,setupTitley,setupTitlew,setupTitleh)
        
        # Anchored to left (mainLyt), and bottom (setupTitle), this allows me to set a height and width variable, as it will be fixed size width and height wise.
        butx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(-180,FBAttachType.kFBAttachBottom,"")
        butw = FBAddRegionParam(134,FBAttachType.kFBAttachBottom,"setupTitle")
        buth = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("but","but", butx,buty,butw,buth)
        
        # Anchored to left (mainLyt), and bottom (setupTitle), this allows me to set a height and width variable, as it will be fixed size width and height wise.
        but1x = FBAddRegionParam(145,FBAttachType.kFBAttachLeft,"")
        but1y = FBAddRegionParam(-180,FBAttachType.kFBAttachBottom,"")
        but1w = FBAddRegionParam(134,FBAttachType.kFBAttachBottom,"setupTitle")
        but1h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("but1","but1", but1x,but1y,but1w,but1h)
    
        # Instructions for export
        constraintsoffTitlex = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        constraintsoffTitley = FBAddRegionParam(-150,FBAttachType.kFBAttachBottom,"")
        constraintsoffTitlew = FBAddRegionParam(268,FBAttachType.kFBAttachBottom,"but")
        constraintsoffTitleh = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")    
        mainLyt.AddRegion("constraintsoffTitle","constraintsoffTitle", constraintsoffTitlex,constraintsoffTitley,constraintsoffTitlew,constraintsoffTitleh)
        
        # Anchored to left (mainLyt), and bottom (workingTitle), this allows me to set a height and width variable, as it will be fixed size width and height wise.
        but6x = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        but6y = FBAddRegionParam(-130,FBAttachType.kFBAttachBottom,"")
        but6w = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        but6h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("but6","but6", but6x,but6y,but6w,but6h)
        
        # Colour Blue Export
        blueTitlex = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        blueTitley = FBAddRegionParam(-100,FBAttachType.kFBAttachBottom,"")
        blueTitlew = FBAddRegionParam(268,FBAttachType.kFBAttachBottom,"but")
        blueTitleh = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")    
        mainLyt.AddRegion("blueTitle","blueTitle", blueTitlex,blueTitley,blueTitlew,blueTitleh)
        
        # Anchored to left (mainLyt), and bottom (workingTitle), this allows me to set a height and width variable, as it will be fixed size width and height wise.
        but7x = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        but7y = FBAddRegionParam(-80,FBAttachType.kFBAttachBottom,"")
        but7w = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        but7h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("but7","but7", but7x,but7y,but7w,but7h)    

        # Anchored to left (mainLyt), and bottom (workingTitle), this allows me to set a height and width variable, as it will be fixed size width and height wise.
        but8x = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        but8y = FBAddRegionParam(-30,FBAttachType.kFBAttachBottom,"")
        but8w = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        but8h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("but8","but8", but8x,but8y,but8w,but8h)  
            
        lWarning = FBLabel()
        lWarning.Justify = FBTextJustify.kFBTextJustifyCenter
        lWarning.Caption = "WARNING:\nDo Not Plot Character When Previewing ToyBox"
        mainLyt.SetControl("warning",lWarning) 
    
        lyt = FBVBoxLayout()
        lyt.Hint = "Characters, Props and Vehicles that could go in the ToyBox"
        mainLyt.SetControl("charPropVehicle",lyt)
            
        # Scene Setup Buttons
        sceneSetup = FBLabel()
        sceneSetup.Caption = "ToyBox Setup (select one or more):"
        mainLyt.SetControl("setupTitle", sceneSetup)
        
        global b    
        b = FBButton()
        b.Hint = "Creating relations for Characters, Props and Vehicles to work with ToyBox"
        b.Caption = "Make Relation(s)"
        mainLyt.SetControl("but",b)
        
        global b1   
        b1 = FBButton()
        b1.Hint = "Removing relations for Characters, Props and Vehicles so they cannot work with ToyBox"
        b1.Caption = "Delete Relation(s)"
        mainLyt.SetControl("but1",b1)  
    
        exportSetup = FBLabel()
        exportSetup.Caption = "Need to Turn Off All Constraints on Export:"
        mainLyt.SetControl("constraintsoffTitle", exportSetup)

        global b6    
        b6 = FBButton()
        b6.Hint = "Turns both constraints per object off/on"
        if self.CheckToyBoxConstraintStatus() == False:
            b6.Caption = "Turn All ToyBox Constraints On"
        else:
            b6.Caption = "Turn All ToyBox Constraints Off"
        mainLyt.SetControl("but6",b6)
    
        blueSetup = FBLabel()
        blueSetup.Caption = "[Optional] Blue Colour (select one or more):"
        mainLyt.SetControl("blueTitle", blueSetup)
        
        global b7    
        b7 = FBButton()
        b7.Hint = "Sometime it's easier to see an object if it is blue/unblue"
        b7.Caption = "Colour Character(s) Blue Toggle"
        mainLyt.SetControl("but7",b7)       

        global b8    
        b8 = FBButton()
        b8.Hint = "creates a toybox object in the scene"
        b8.Caption = "Create Toybox Object"
        mainLyt.SetControl("but8",b8)  
        
        global gCharPropVehicleList
        gCharPropVehicleList = FBList()
        gCharPropVehicleList.OnChange.Add(self.ListCallback)
        gCharPropVehicleList.Style = FBListStyle.kFBVerticalList
        gCharPropVehicleList.Selected(0, False)
        gCharPropVehicleList.MultiSelect = True      
        
        lMoverArray = []
        
        moverCtrl = FBComponentList()
        FBFindObjectsByName ("mover_Control", moverCtrl, False, True)
        chasssisCtrl = FBComponentList()
        FBFindObjectsByName ("chassis_Control", chasssisCtrl, False, True) 
        mergedlist = itertools.chain(moverCtrl, chasssisCtrl)
        
        for iComponent in mergedlist: 
            lParent = False
            # Every Component in the Scene
            lHasBeenDone = iComponent.LongName   
                  
            # Have to check for mover_Control for props and characters, and chassis_Control for Vehicles
            # Add chassis_Control
            if iComponent.Name == "chassis_Control":
                for i in range(iComponent.GetDstCount()):
                    if iComponent.GetDst(i) != None:
                        # Checks to see if the work is already done, if it is adds DONE to the end of the name
                        if "SendToToyBox" in iComponent.GetDst(i).Name and iComponent.GetDst(i).ClassName() == "FBConstraintRelation":
                            lHasBeenDone = lHasBeenDone + " (DONE)"
            
                #appends this Done to list
                gCharPropVehicleList.Items.append(lHasBeenDone)
            
            # Add mover_Control                
            else:                
                for iParent in iComponent.Parents:
                    if iParent.Name == "chassis_Control":
                        lParent = True

                if lParent == False:
                    for i in range(iComponent.GetDstCount()):
                        if iComponent.GetDst(i) != None:
                            # Checks to see if the work is already done, if it is adds DONE to the end of the name
                            if "SendToToyBox" in iComponent.GetDst(i).Name and iComponent.GetDst(i).ClassName() == "FBConstraintRelation":
                                lHasBeenDone = lHasBeenDone + " (DONE)"
            
                    #appends this Done to list
                    gCharPropVehicleList.Items.append(lHasBeenDone)                        
                        
        # Now we want to show the list in the UI   
        mainLyt.SetControl("charPropVehicle", gCharPropVehicleList)    
        
        b.OnClick.Add(RS.Tools.ToyBox.rs_MakeToyBoxRelations)
        b1.OnClick.Add(RS.Tools.ToyBox.rs_DeleteToyBoxRelations)
        b6.OnClick.Add(RS.Tools.ToyBox.rs_ExpressionOnOff)
        b7.OnClick.Add(RS.Tools.ToyBox.rs_ChangeObjectBlue)
        
        def CreateToybox( control, event ):
            
            toyboxBool = False
            
            for component in RS.Globals.gComponents:
                if component.Name == 'ToyBox':
                    toyboxBool = True
            
            if toyboxBool == True:
                FBMessageBox('R* Error', 'ToyBox Object already exists.', 'Ok')
            else:
                RS.Core.Mocap.MocapCommands.CreateToybox()
        
        b8.OnClick.Add( CreateToybox )
 
def Run( show = True ):
    tool = ToyBoxTool()
    
    if show:
        tool.Show()
        
    return tool
