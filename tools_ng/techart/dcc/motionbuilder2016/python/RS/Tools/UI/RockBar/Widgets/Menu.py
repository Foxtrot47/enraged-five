import os
import re
import functools
from xml.etree import cElementTree as xml

from PySide import QtGui, QtCore

from RS import Config
from RS.Tools.UI import Application
from RS.Utils.Logging import Database
from RS.Utils.Logging import MongoDatabase
# Uncomment this once we get newer version of Python that supports ssl.create_default_context
# from RS.Utils.Logging import ElasticDatabase


class RockBar(QtGui.QMenuBar, Application.QtBase):
    """
    Our custom menu bar.

    Inherits QtBase to distingish it from standard Qt widgets and our own when using isinstance
    """
    active = QtCore.Signal(object)
    settings = QtCore.QSettings("RockstarSettings", "RockstarMenu")

    _cache = {}
    _recentlyUsedString = "Recently Used"
    rockstarMenu = None

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): the parent widget
        """
        super(RockBar, self).__init__(parent=parent)

    def event(self, event):
        """
        Overrides built in method

        Triggers the active signal when the window becomes active

        Arguments:
            event (QtCore.QEvent): event being invoked
        Return:
            bool
        """
        value = super(RockBar, self).event(event)
        if event.type() == QtCore.QEvent.WindowActivate:
            self.active.emit(self)
        return value

    @classmethod
    def getActionsByName(cls, name, exact=False, menu=None, recursive=False, maxDepth=None, depth=0, regex=None):
        """
        Gets the action based on the name provided

        Arguments:
            name (string): name of the action to search for
            exact (bool): search fo the exact string.
                          Case-insensitive when false.
            menu (PySide.QtGui.QMenu): the menu to search in to search, by default looks at the top level actions of the menu
            recursive (bool): recursively search for child menus
            maxDepth (int): max recursive depth. By default it is None
            depth (int): current depth of the function
            regex (function): function to use to perform regex

        Return:
            generator
        """
        if regex is None:
            regex = re.compile(name, re.I).match if exact else re.compile(name).search

        for action in menu.actions():
            if regex(action.text().strip()):
                yield action
                if recursive and depth != maxDepth and action.menu() is not None:
                    cls.getActionByName(name, exact, action.menu(), recursive, maxDepth, depth+1, regex)

    # TODO: Move to a universal model system for shorctuts, shelf and menu
    @classmethod
    def _execString(cls, command, moduleName, toolName, path):
        """
        Wraps around the exec command so it can be used dynamically

        Arguments:
             command (string): command to run
             moduleName (string): import name of module being ran
             toolName (string): title/name of the tool
             path (string): path to the module being imported
        """
        exec(command)
        MongoDatabase.LogToolInfo(moduleName, Database.Context.Opened, toolName, modulePath=path)
        # Uncomment this once we get newer version of Python that supports ssl.create_default_context
        # ElasticDatabase.LogToolInfo(moduleName, toolName, modulePath=path)

    @classmethod
    def updateRecentlyUsed(cls, toolName):
        """
        Updates the recently used tool list and rebuilds the menu

        Arguments:
            toolName (string): name of the tool
        """

        recentlyUsedTools = cls.settings.value(cls._recentlyUsedString, [])
        if toolName in recentlyUsedTools:
            recentlyUsedTools.remove(toolName)
        recentlyUsedTools.insert(0, toolName)
        cls.settings.setValue(cls._recentlyUsedString, recentlyUsedTools[:5])
        cls.buildRecentlyUsedMenu(cls.rockstarMenu)

    @classmethod
    def buildMenuFromXml(cls, xmlPath, parent):
        """
        Builds the rockstar menu from an xml file.
        Currently uses the existing xml format, this method will change in the future.

        Arguments:
            xmlPath (string): path to xml to
            parent (QtGui.QMenu): parent menu
        """
        tree = xml.parse(xmlPath)
        root = tree.getroot()
        menu = QtGui.QMenu()
        menu.setTearOffEnabled(True)
        menu.setTitle("Rockstar")
        menu.setWindowTitle("Rockstar")
        menu.addSeparator()

        cls.rockstarMenu = menu
        for element in root:
            cls._recursiveBuild(element, menu, 1)
        # This needs to be ran after the initial menu is built to ensure we have cached the current contents of the menu
        cls.buildRecentlyUsedMenu(menu)
        parent.insertMenu(parent.actions()[-1], menu)

    @classmethod
    def buildRecentlyUsedMenu(cls, menu):
        """
        Builds the recently used menu for the rockstar menu

        Arguments:
            menu (QtGui.QMenu): menu to add the recently used submenu to
        """

        recentlyUsedTools = cls.settings.value(cls._recentlyUsedString, [])
        if not recentlyUsedTools:
            return

        currentActions = menu.actions()
        for action in currentActions:
            if action.text() == cls._recentlyUsedString:
                break
            action = None

        if action is None:
            action = QtGui.QAction(cls._recentlyUsedString, None)
            submenu = QtGui.QMenu()
            submenu.setTitle(cls._recentlyUsedString)
            submenu.setWindowTitle(cls._recentlyUsedString)
            submenu.setTearOffEnabled(True)
            menu.insertAction(menu.actions()[0], action)
            action.setMenu(submenu)
        else:
            submenu = action.menu()

        submenu.clear()
        for name in cls.settings.value(cls._recentlyUsedString, [])[:5]:
            data = cls._cache.get(name, None)
            if data is None:
                continue
            commandString, importString, path = data
            subaction = submenu.addAction(name)
            subaction.triggered.connect(functools.partial(cls._execString, commandString, importString, name, path))
            subaction.triggered.connect(functools.partial(cls.updateRecentlyUsed, name))

    @classmethod
    def _recursiveBuild(cls, element, menu, depth=0):
        """
        Recursively builds a menu based on the xml element provided

        Arguments:
            element(element): element to convert into an action
            menu (QtGui.QMenu): menu to add actions too
            depth (int): recursion depth
        """
        name = element.attrib.get("displayName", "")
        action = menu.addAction(name)

        commandString = None
        importString = element.attrib.get("modulePath")
        functionString = element.attrib.get("command", "")

        if importString or functionString:
            importString = importString or "RS.Tools.UI.Commands"
            moduleString, subModuleString = importString.rsplit(".", 1)
            functionString = functionString or "Run"

            path = os.path.join(Config.Tool.Path.TechArt, "dcc", "motionbuilder", "python", *importString.split())
            commandString = "from {0} import {1};{1}.{2}()".format(moduleString, subModuleString, functionString)

        if commandString:
            cls._cache[name] = (commandString, importString, path)
            action.triggered.connect(functools.partial(cls._execString, commandString, importString, name, path))
            action.triggered.connect(functools.partial(cls.updateRecentlyUsed, name))

        if "separator" in element.attrib:
            menu.addSeparator()

        if "sort" in element.attrib:
            element = sorted(element, key=lambda x: x.attrib.get("displayName", ""))

        for child in element:
            childMenu = action.menu()
            if not childMenu:
                childMenu = QtGui.QMenu()
                childMenu.setTitle(name)
                childMenu.setWindowTitle(name)
                action.setMenu(childMenu)
            childMenu.setTearOffEnabled(True)
            cls._recursiveBuild(child, childMenu, depth+1)


