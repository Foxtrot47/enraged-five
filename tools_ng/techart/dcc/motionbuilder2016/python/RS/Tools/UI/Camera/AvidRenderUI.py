import os
import shutil

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Utils.Path as path
import RS.Utils.UserPreferences as userPref
from RS.Core.Camera import CamUtils
from RS.Core.Automation.FrameCapture import VideoTools


class AvidUI(object):
    def __init__(self):
        self.toolBox = None
        self.eButt1 = None
        self.eButt2 = None
        self.filenameText = None
        self.seenError = False

        self.noteText = "(optional)"
        self.invalidNoteChars = [" ", ".", "/", "\\", "#"]
        self.lFbxName = path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        self.folderLoc = os.path.expanduser("~\\Desktop")

        if self.seenError == False:
            self.checkForRememberedFolder()
            self.createToolBox()

    def createToolBox(self):
        # MAKE BASIC BOX
        self.toolBox = FBCreateUniqueTool("AVID FBX RENDER")
        self.toolBox.StartSizeX = 400
        self.toolBox.StartSizeY = 300

        # LOCATION TEXT
        labelOne = FBLabel()
        labelOne.Caption = "Location:"
        labelOne.Style = FBTextStyle.kFBTextStyleBold
        labelOne.WordWrap = True

        x = FBAddRegionParam(5, FBAttachType.kFBAttachLeft, "")
        y = FBAddRegionParam(90, FBAttachType.kFBAttachTop, "")
        w = FBAddRegionParam(300, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion("labelOne", "labelOne", x, y, w, h)
        self.toolBox.SetControl("labelOne", labelOne)

        # LOCATION BOX
        e = FBEdit()
        # e.ReadOnly = True
        e.Text = self.folderLoc
        e.OnChange.Add(self.OnChangeFolder)
        """
        initCall = "%s()" % (style)
        e = eval( initCall )
        e.Text = self.folderLoc
        e.OnChange.Add(self.OnChangeFolder)
        """
        x = FBAddRegionParam(30, FBAttachType.kFBAttachLeft, "")
        y = FBAddRegionParam(5, FBAttachType.kFBAttachBottom, "labelOne")
        w = FBAddRegionParam(260, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion("path", "path", x, y, w, h)
        self.toolBox.SetControl("path", e)

        # BROWSE BUTTON
        b2 = FBButton()
        b2.Caption = "Browse..."
        b2.Justify = FBTextJustify.kFBTextJustifyCenter

        x = FBAddRegionParam(265, FBAttachType.kFBAttachLeft, "path")
        y = FBAddRegionParam(-5, FBAttachType.kFBAttachBottom, "labelOne")
        w = FBAddRegionParam(57, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(40, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion("but", "but", x, y, w, h)
        self.toolBox.SetControl("but", b2)

        self.eButt1 = e
        b2.OnClick.Add(self.browseTool)

        # FILENAME TTTLE
        labelTwo = FBLabel()
        labelTwo.Caption = "Filename:"
        labelTwo.Style = FBTextStyle.kFBTextStyleBold
        labelTwo.WordWrap = True

        x = FBAddRegionParam(5, FBAttachType.kFBAttachLeft, "")
        y = FBAddRegionParam(10, FBAttachType.kFBAttachTop, "")
        w = FBAddRegionParam(60, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion("labelTwo", "labelTwo", x, y, w, h)
        self.toolBox.SetControl("labelTwo", labelTwo)

        # FILENAME TEXT
        self.filenameText = FBLabel()
        self.updateFilenameText()

        # FILENAME BOX
        x = FBAddRegionParam(30, FBAttachType.kFBAttachLeft, "")
        y = FBAddRegionParam(20, FBAttachType.kFBAttachTop, "")
        w = FBAddRegionParam(320, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(50, FBAttachType.kFBAttachNone, "")

        regionName = "-"
        borderStyle = FBBorderStyle.kFBHighlightBorder

        self.toolBox.AddRegion(regionName, regionName, x, y, w, h)
        self.toolBox.SetBorder(regionName, borderStyle, True, False, 2, 2, 90, 0)

        # EXTRANOTES TEXT
        labelFour = FBLabel()
        labelFour.Caption = "Notes:"
        labelFour.Style = FBTextStyle.kFBTextStyleBold
        labelFour.WordWrap = True

        x = FBAddRegionParam(5, FBAttachType.kFBAttachLeft, "")
        y = FBAddRegionParam(150, FBAttachType.kFBAttachTop, "")
        w = FBAddRegionParam(200, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion("labelFour", "labelFour", x, y, w, h)
        self.toolBox.SetControl("labelFour", labelFour)

        # EXTRANOTES BOX
        edits = {}
        style = "FBEdit"
        labId = "Label" + style
        l = FBLabel()
        l.Caption = ""
        x = FBAddRegionParam(5, FBAttachType.kFBAttachLeft, "")
        y = FBAddRegionParam(170, FBAttachType.kFBAttachTop, "")
        w = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(15, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion(labId, labId, x, y, w, h)
        self.toolBox.SetControl(labId, l)

        editId = "Edit" + style
        initCall = "%s()" % (style)
        e = eval(initCall)
        edits[style] = e

        x = FBAddRegionParam(5, FBAttachType.kFBAttachRight, labId)
        y = FBAddRegionParam(170, FBAttachType.kFBAttachTop, "")
        w = FBAddRegionParam(325, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion(editId, editId, x, y, w, h)
        self.toolBox.SetControl(editId, e)

        attachType = FBAttachType.kFBAttachBottom
        anchor = labId
        e = edits['FBEdit']
        e.Text = self.noteText
        e.OnChange.Add(self.OnChangeNotes)

        # RENDER BUTTON
        b1 = FBButton()
        b1.Caption = "RENDER"

        x = FBAddRegionParam(90, FBAttachType.kFBAttachLeft, "")
        y = FBAddRegionParam(210, FBAttachType.kFBAttachNone, "")
        w = FBAddRegionParam(200, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(50, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion("renderBut", "renderBut", x, y, w, h)
        self.toolBox.SetControl("renderBut", b1)

        b1.OnClick.Add(self.PressRenderButton)

        # REFRESH FILENAME BUTTON
        b2 = FBButton()
        b2.Caption = "Reload"

        x = FBAddRegionParam(165, FBAttachType.kFBAttachLeft, "")
        y = FBAddRegionParam(63, FBAttachType.kFBAttachNone, "")
        w = FBAddRegionParam(40, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion("reloadBut", "reloadBut", x, y, w, h)
        self.toolBox.SetControl("reloadBut", b2)

        b2.OnClick.Add(self.reloadButton)

        # DISPLAY TOOL
        ShowTool(self.toolBox)


    """
    EVENT HANDLERS
    """

    def checkForRememberedFolder(self):
        savedPath = userPref.__Readini__("renderPaths", "avidrender", notFoundValue=None)
        if savedPath:
            self.folderLoc = savedPath

    def saveFolderLoc(self):
        userPref.__Saveini__("renderPaths", "avidrender", self.folderLoc)

    def updateFilenameText(self):
        self.lFbxName = path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        self.filenameText.Caption = self.lFbxName
        self.filenameText.Style = FBTextStyle.kFBTextStyleBold
        self.filenameText.WordWrap = True

        nameLen = self.filenameText.Caption.__len__()
        if nameLen < 35:
            nameOffsetX = 110
            nameOffsetH = 240
        else:
            nameOffsetX = 30
            nameOffsetH = 320

        x = FBAddRegionParam(nameOffsetX, FBAttachType.kFBAttachLeft, "")
        y = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "")
        w = FBAddRegionParam(nameOffsetH, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")

        self.toolBox.AddRegion("filenameText", "filenameText", x, y, w, h)
        self.toolBox.SetControl("filenameText", self.filenameText)

    def reloadButton(self, control, event):
        self.updateFilenameText()

    def browseTool(self, control, event):
        self.updateFilenameText()

        lFp = FBFolderPopup()
        lFp.Caption = "Select the save location:"
        lFp.Path = self.folderLoc

        lRes = lFp.Execute()
        if lRes:
            self.eButt1.Text = os.path.join(lFp.Path)
            self.folderLoc = os.path.join(lFp.Path)

    def OnChangeNotes(self, control, event):
        self.noteText = control.Text

    def OnChangeFolder(self, control, event):
        self.folderLoc = control.Text

    def PressRenderButton(self, control, event):
        self.StartRender()

    def StartRender(self):
        # Get Render Name and Output Folder - or exit if missing
        self.updateFilenameText()
        renderName = self.lFbxName
        outputFolder = self.eButt1.Text
        if not renderName or not outputFolder:
            return

        # Save Output Folder Locally
        self.saveFolderLoc()

        # Get Paths and Notes String
        frameFolder = os.path.join(outputFolder, "tempFrames")
        noteString = ""
        if self.noteText and self.noteText != "(optional)":
            noteString = "".join([char for char in self.noteText if char not in self.invalidNoteChars])
            renderName = "{0}${1}".format(self.lFbxName, noteString)
        outputPath = os.path.join(outputFolder, "{0}.mov".format(renderName))
        framePath = os.path.join(frameFolder, "{0}-.jpg".format(renderName))

        # Remove Previous Video
        if os.path.exists(outputPath):
            try:
                os.unlink(outputPath)
            except:
                errorText = "Error: Existing render {0} could not be deleted. \n\n\tIs it open in another program?"
                errorText = errorText.format(os.path.split(outputPath)[1])
                FBMessageBox("Avid Render", errorText, "Damn")
                return

        # Setup Frame Folder
        if os.path.exists(frameFolder):
            shutil.rmtree(frameFolder)
        os.makedirs(frameFolder)

        # Fix Hard Ranges - cutscene only
        fbxPath = FBApplication().FBXFileName.lower()
        cutsceneFolder = os.path.join("art", "animation", "cutscene")
        if cutsceneFolder in fbxPath:
            CamUtils.FixCutsceneHardRanges()

        # Create Frames and Fix Frame Path
        CamUtils.RenderFramesForAvid(framePath)
        frameName = [frame for frame in sorted(os.listdir(frameFolder))][0]
        framePath = os.path.join(frameFolder, frameName)

        # Create Video and Remove Frames
        VideoTools.CreateRdrAvidVideo(fbxPath, framePath, outputPath, noteText=noteString)
        shutil.rmtree(frameFolder)

        # Verify Render and Notify User
        if os.path.exists(outputPath):
            FBMessageBox("Avid Render", "The fbx has been successfully rendered.", "OK")
        else:
            FBMessageBox("Avid Render", "Error: Render failed.", "Damn")



def Run():
    _AvidUIObj_ = AvidUI()
