from functools import partial
from RS.Tools.UI import Run
from RS.Core.Automation.Layout import LayoutTools
from RS.Core.Audio import AudioTools
from RS.Core.Camera import CamUtils
from RS.Utils import ContentUpdate

# Janky Pyside Import - allows same code on all projects
try:
    from PySide2 import QtCore, QtGui, QtWidgets
except ImportError:
    from PySide import QtCore, QtGui as QtWidgets


def RunCommand(function):
    result = function()
    if isinstance(result, str) and result.lower().startswith("error"):
        QtWidgets.QMessageBox.information(None, "Error", result)
    else:
        QtWidgets.QMessageBox.information(None, "Notification", "Finished.")


@Run("Cutscene Layout Tools", url="https://hub.rockstargames.com/display/ANIM/Motionbuilder+Cutscene+Layout+Tools")
def ShowTool():
    wid = QtWidgets.QWidget()
    mainLayout = QtWidgets.QVBoxLayout()
    gridLayout = QtWidgets.QGridLayout()

    buttonData = [["Zero FBX", "Zeros the current FBX.",  LayoutTools.ZeroFBX],
                  ["Set Hard Ranges", "Sets the hard ranges to the slate keys.",  LayoutTools.SetSmartEndframe],
                  ["Delete Slates", "Removes all slate models and constraints.",  LayoutTools.DeleteSlates],
                  ["Setup Audio", "Syncs and adds audio to a cutscene FBX.",  AudioTools.SetupCutsceneAudio],
                  ["Add Toybox", "Adds a ToyBox object to the FBX.",  LayoutTools.SetupToybox],
                  ["Setup Export Cam", "Adds and plots an export cam.",  CamUtils.SetupExportCam],
                  ["Auto Populate Exporter", "Populates export data.",  LayoutTools.AutoPopulate],
                  ["Fix Missing Handles", "Fix missing handles data.",  LayoutTools.FixMissingHandles],
                  ["Update Handle File", "Adds missing handles to the handle file.",  LayoutTools.SetupHandleFile],
                  ["Update Cutscene Meta File", "Updates cutscene meta file with this scene.",  ContentUpdate.SetupGameContent],
                  ["Update Content Xml Files", "Updates content xml files with this scene.",  ContentUpdate.SetupToolsContent],
                  ["Set Exporter Shot", "Sets shot options in the exporter.",  LayoutTools.SetExporterShotData],
                  ["Set Scene Location", "Sets scene location in the exporter.",  LayoutTools.SetSceneLocation],
                  ["Export Cutscene", "Exports scene with current export settings.",  LayoutTools.ExportCutscene],
                  ["Auto Layout", "Runs all above steps automatically (except export).",  LayoutTools.AutoLayout]]

    for index, eachButton in enumerate(buttonData):
        function = partial(RunCommand, eachButton[-1])
        newButton = QtWidgets.QPushButton(eachButton[0])
        newButton.setToolTip(eachButton[1])
        newButton.clicked.connect(function)

        if index < 7:
            newButton.setFixedWidth(150)
            gridLayout.addWidget(newButton, index, 0)
        elif index < 14:
            newButton.setFixedWidth(150)
            gridLayout.addWidget(newButton, index-7, 1)
        else:
            newButton.setFixedHeight(60)
            mainLayout.addLayout(gridLayout)
            mainLayout.addWidget(newButton)

    wid.setLayout(mainLayout)
    return wid


def Run():
    ShowTool()
