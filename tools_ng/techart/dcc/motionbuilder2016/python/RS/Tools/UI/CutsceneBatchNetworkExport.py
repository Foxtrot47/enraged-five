"""
The cutscene batch exporter is a tool which allows you to export and build multiple scenes in a sequence

Image:
    $TechArt\Tools\CutsceneBatchNetworkExport.png

"""
import pyfbsdk as mobu
import pyfbsdk_additions as mobuAdditions

import RS.Tools.UI
import RS.Tools.CutsceneBatchNetworkExport


# # Constants # #

global tool
global mgr
global fileListCtrl
global fileListData
global currentFilenameCtrl

# Create an instance of the manager.
mgr = RS.Tools.CutsceneBatchNetworkExport.RsCutsceneBatchNetworkFileManager()
fileListData = []

__WIKI__ = "https://devstar.rockstargames.com/wiki/index.php/Cutscene_Batch_Exporter"


class CutsceneBatchNetworkExportTool(RS.Tools.UI.Base):
    def __init__(self):
        """ Constructor """
        RS.Tools.UI.Base.__init__(self, 'Cutscene Batch Network Export', size=[850, 400])

    # # Functions # #

    def RefreshFileList(self):
        """ Populates the UI with the list of files from batchxml """
        global mgr
        global fileListCtrl
        global fileListData
        global currentFilenameCtrl

        fileListCtrl.Clear()
        fileListData = []

        # Add columns
        fileListCtrl.ColumnAdd('')

        fileListCtrl.ColumnAdd('Use Source Control Files')
        fileListCtrl.ColumnAdd('FBX Filename')

        # Set column widths
        fileListCtrl.GetColumn(0).Width = 25
        fileListCtrl.GetColumn(0).Style = mobu.FBCellStyle.kFBCellStyleButton

        fileListCtrl.GetColumn(1).Width = 130
        fileListCtrl.GetColumn(1).Style = mobu.FBCellStyle.kFBCellStyle2StatesButton
        fileListCtrl.GetColumn(1).Justify = mobu.FBTextJustify.kFBTextJustifyCenter

        fileListCtrl.GetColumn(2).Width = 400
        fileListCtrl.GetColumn(2).ReadOnly = True

        if mgr.currentFile:
            currentFilenameCtrl.Caption = 'Current Filename: {0}'.format(mgr.currentFile.filename)

            row = 0

            cutsceneNames = [cutsceneName for cutsceneName in mgr.currentFile.entries.keys()]
            cutsceneNames.sort()

            for cutsceneName in cutsceneNames:
                entry = mgr.currentFile.entries[cutsceneName]

                fileListCtrl.RowAdd(cutsceneName.upper(), row)
                fileListData.append(cutsceneName)

                useSourceControlFiles = 0

                if entry.useSourceControlFiles:
                    useSourceControlFiles = 1

                fileListCtrl.SetCellValue(row, 0, 'X')
                fileListCtrl.SetCellValue(row, 1, useSourceControlFiles)

                raw = entry.fbxFilename.split('\\')[-4:]
                relativeFbxFilename = '...'

                for item in raw:
                    relativeFbxFilename += '\\{0}'.format(item)

                fileListCtrl.SetCellValue(row, 2, relativeFbxFilename)

                row += 1
        else:
            currentFilenameCtrl.Caption = 'Current Filename:'

    def SaveNewFile(self):
        """ Prompts the user to saves out a new batchxml """
        global mgr

        saveDialog = mobu.mobu.FBFilePopup()
        saveDialog.Caption = "Save a new Network Batch XML file"
        saveDialog.Style = mobu.mobu.FBFilePopupStyle.kFBFilePopupSave
        saveDialog.Filter = "*.batchxml"

        saveResult = saveDialog.Execute()

        if saveResult:
            batchXmlFilename = '{0}\\{1}'.format(saveDialog.Path, saveDialog.FileName)
            mgr.currentFile = RS.Tools.CutsceneBatchNetworkExport.RsCutsceneBatchNetworkFile(batchXmlFilename)
            mgr.currentFile.createBatchNetworkXml(mgr.currentFile.filename)

    def CheckForFileSave(self):
        """
        Checks if there are have been changes to the xml file since it was opened and warns the user if they want
        to save over it.

        Return:
            boolean
        """
        global mgr

        if mgr.currentFile:
            if mgr.currentFile.dirty:
                result = mobu.mobu.FBMessageBox('Rockstar', 'You have unsaved changes to the following file!\n\n'
                                                            '{0}\n\n'
                                                            'Would you like to save them now?'.format(
                    mgr.currentFile.filename), 'Yes', 'No', 'Cancel')

                if result == 1:
                    mgr.currentFile.createBatchNetworkXml(mgr.currentFile.filename)

                if result == 3:
                    return False

        return True

    # # Event Handlers # #

    def OnAddEntry_Clicked(self, control, event):
        """
        Adds an entry to the batchxml

        Arguments:
            control: Motion Builder object that is calling this method
            event (pyfbsdk.FBEvent)= Motion Builder event
        """
        global mgr
        global tool

        if not mgr.currentFile:
            self.SaveNewFile()

        if mgr.currentFile:
            openDialog = mobu.mobu.FBFilePopup()
            openDialog.Caption = "Choose a Cutscene FBX file"
            openDialog.Style = mobu.FBFilePopupStyle.kFBFilePopupOpen
            openDialog.Filter = "*.fbx"

            openResult = openDialog.Execute()

            if openResult:
                fbxFilename = '{0}\\{1}'.format(openDialog.Path, openDialog.FileName)
                mgr.currentFile.addEntry(fbxFilename, validate=True)
                self.RefreshFileList()

    def OnLoadFromFile_Clicked(self, control, event):
        """
        Loads list of files to batch process from a text or batchxml file

        Arguments:
            control: Motion Builder object that is calling this method
            event (pyfbsdk.FBEvent)= Motion Builder event
        """
        global mgr

        if self.CheckForFileSave():
            fp = mobu.FBFilePopup()
            fp.Caption = "Choose a file"
            fp.Style = mobu.mobu.FBFilePopupStyle.kFBFilePopupOpen
            fp.Filter = "*.txt; *.batchxml"

            result = fp.Execute()

            if result:
                filename = '{0}\\{1}'.format(fp.Path, fp.FileName)

                if filename.endswith('.txt'):
                    batchFileObj = mgr.loadFromTxtFile(filename)

                    # Choose a .batchxml filename to save the .txt file as.
                    saveDialog = mobu.mobu.FBFilePopup()
                    saveDialog.Caption = "Save a new Network Batch XML file"
                    saveDialog.Style = mobu.mobu.FBFilePopupStyle.kFBFilePopupSave
                    saveDialog.Filter = "*.batchxml"

                    saveResult = saveDialog.Execute()

                    if saveResult:
                        batchXmlFilename = '{0}\\{1}'.format(saveDialog.Path, saveDialog.FileName)
                        batchFileObj.filename = batchXmlFilename

                elif filename.endswith('.batchxml'):
                    mgr.loadFromBatchXmlFile(filename)

                self.RefreshFileList()

    def OnSaveBatchXmlFile_Clicked(self, control, event):
        """
        Saves out the current list of files to the batchxml file.

        Arguments:
            control: Motion Builder object that is calling this method
            event (pyfbsdk.FBEvent)= Motion Builder event

        """
        global mgr

        if mgr.currentFile:
            mgr.currentFile.createBatchNetworkXml(mgr.currentFile.filename)

    def OnSaveAsBatchXmlFile_Clicked(self, control, event):
        """
        Prompts the user to pick a file to save the current list of files into.

        Arguments:
            control: Motion Builder object that is calling this method
            event (pyfbsdk.FBEvent)= Motion Builder event
        """
        global mgr

        if mgr.currentFile:
            saveDialog = mobu.mobu.FBFilePopup()
            saveDialog.Caption = "Save as a new Network Batch XML file"
            saveDialog.Style = mobu.FBFilePopupStyle.kFBFilePopupSave
            saveDialog.Filter = "*.batchxml"

            saveResult = saveDialog.Execute()

            if saveResult:
                batchXmlFilename = '{0}\\{1}'.format(saveDialog.Path, saveDialog.FileName)
                mgr.currentFile.filename = batchXmlFilename
                mgr.currentFile.createBatchNetworkXml(mgr.currentFile.filename)

                self.RefreshFileList()

    def OnNewFile_Clicked(self, control, event):
        """
        Creates a new batchxml file
        Arguments:
            control: Motion Builder object that is calling this method
            event (pyfbsdk.FBEvent)= Motion Builder event
        """
        global mgr

        if mgr.currentFile:
            if mgr.currentFile.dirty:
                result = mobu.mobu.FBMessageBox('Rockstar', 'You have unsaved changes to the following file!\n\n'
                                                  '{0}\n\n'
                                                  'Would you like to save them now?'.format(mgr.currentFile.filename),
                                           'Yes', 'No', 'Cancel')

                if result == 1:
                    mgr.currentFile.createBatchNetworkXml(mgr.currentFile.filename)

                if result == 3:
                    return False

        saveDialog = mobu.FBFilePopup()
        saveDialog.Caption = "Create a new Network Batch XML file"
        saveDialog.Style = mobu.FBFilePopupStyle.kFBFilePopupSave
        saveDialog.Filter = "*.batchxml"

        saveResult = saveDialog.Execute()

        if saveResult:
            batchXmlFilename = '{0}\\{1}'.format(saveDialog.Path, saveDialog.FileName)
            mgr.currentFile = RS.Tools.CutsceneBatchNetworkExport.RsCutsceneBatchNetworkFile(batchXmlFilename)
            mgr.currentFile.createBatchNetworkXml(mgr.currentFile.filename)

            self.RefreshFileList()

    def OnFileList_Clicked(self, control, event):
        """
        Updates the UI to reflect the currently selected file in the file list

        Arguments:
            control: Motion Builder object that is calling this method
            event (pyfbsdk.FBEvent)= Motion Builder event
        """
        global fileListData
        global mgr

        val = control.GetCellValue(event.Row, event.Column)
        cutsceneName = fileListData[event.Row]

        # Delete
        if event.Column == 0:
            mgr.currentFile.deleteEntryByCutsceneName(cutsceneName)
            self.RefreshFileList()

        # Use source control files
        elif event.Column == 1:
            if val == 0:
                mgr.currentFile.setUseSourceControlFiles(cutsceneName, False)

            else:
                mgr.currentFile.setUseSourceControlFiles(cutsceneName, True)

    def OnMergeFromFile_Clicked(self, control, event):
        """
        Merges the contents of a txt or batchxml file into the currently loaded list

        Arguments:
            control: Motion Builder object that is calling this method
            event (pyfbsdk.FBEvent)= Motion Builder event
        """
        global mgr

        if mgr.currentFile:
            openDialog = mobu.FBFilePopup()
            openDialog.Caption = "Choose a file to merge"
            openDialog.Style = mobu.FBFilePopupStyle.kFBFilePopupOpen
            openDialog.Filter = "*.txt; *.batchxml"

            openResult = openDialog.Execute()

            if openResult:
                filename = '{0}\\{1}'.format(openDialog.Path, openDialog.FileName)
                mgr.mergeFromFile(filename)
                self.RefreshFileList()

        else:
            mobu.FBMessageBox('Rockstar', 'Please load a file first!', 'OK')

    def OnValidate_Clicked(self, control, event):
        """
        Validates the current file that is being used by the tool

        Arguments:
            control: Motion Builder object that is calling this method
            event (pyfbsdk.FBEvent)= Motion Builder event
        """
        global mgr

        if mgr.currentFile:
            mgr.currentFile.validate()
            self.RefreshFileList()

        else:
            mobu.FBMessageBox('Rockstar', 'Nothing to validate!  Please load a file first.', 'OK')

    # # GUI # #

    def Create(self, mainLyt):
        """
        Builds the UI

        Arguments:
            mainLyt (pyfbsdk.FBTool): parent of the UI
        """
        global fileListCtrl
        global currentFilenameCtrl

        lyt = mobuAdditions.FBVBoxLayout()

        x = mobu.FBAddRegionParam(10, mobu.FBAttachType.kFBAttachLeft, '')
        y = mobu.FBAddRegionParam(30, mobu.FBAttachType.kFBAttachTop, '')
        w = mobu.FBAddRegionParam(-10, mobu.FBAttachType.kFBAttachRight, '')
        h = mobu.FBAddRegionParam(-10, mobu.FBAttachType.kFBAttachBottom, '')
        mainLyt.AddRegion('main', 'main', x, y, w, h)

        mainLyt.SetControl('main', lyt)

        headerLyt = mobuAdditions.FBGridLayout()

        # New file
        newFileCtrl = mobu.FBButton()
        newFileCtrl.Caption = "New"
        newFileCtrl.OnClick.Add(self.OnNewFile_Clicked)

        # Load from file
        loadFromFileCtrl = mobu.FBButton()
        loadFromFileCtrl.Caption = "Load"
        loadFromFileCtrl.OnClick.Add(self.OnLoadFromFile_Clicked)

        # Merge from file
        mergeFromFileCtrl = mobu.FBButton()
        mergeFromFileCtrl.Caption = "Merge"
        mergeFromFileCtrl.OnClick.Add(self.OnMergeFromFile_Clicked)

        # Save batch xml
        saveBatchXmlCtrl = mobu.FBButton()
        saveBatchXmlCtrl.Caption = "Save"
        saveBatchXmlCtrl.OnClick.Add(self.OnSaveBatchXmlFile_Clicked)

        # SaveAs batch xml
        saveAsBatchXmlCtrl = mobu.FBButton()
        saveAsBatchXmlCtrl.Caption = "Save As..."
        saveAsBatchXmlCtrl.OnClick.Add(self.OnSaveAsBatchXmlFile_Clicked)

        # Add entry
        addEntryCtrl = mobu.FBButton()
        addEntryCtrl.Caption = "Add Entry"
        addEntryCtrl.OnClick.Add(self.OnAddEntry_Clicked)

        headerLyt.Add(newFileCtrl, 0, 0)
        headerLyt.Add(loadFromFileCtrl, 0, 1)
        headerLyt.Add(mergeFromFileCtrl, 0, 2)
        headerLyt.Add(saveBatchXmlCtrl, 0, 3)
        headerLyt.Add(saveAsBatchXmlCtrl, 0, 4)
        headerLyt.Add(addEntryCtrl, 0, 5)

        # Current filename
        currentFilenameCtrl = mobu.FBLabel()
        currentFilenameCtrl.Caption = 'Current Filename:'

        # File list spreadshet
        fileListCtrl = mobu.FBSpread()
        fileListCtrl.Caption = 'Cutscene Name'
        fileListCtrl.OnCellChange.Add(self.OnFileList_Clicked)

        lyt.Add(currentFilenameCtrl, 24)
        lyt.Add(headerLyt, 32, width=500)
        lyt.AddRelative(fileListCtrl, 1.0)


def Run(show=True):
    """
    Runs the tool

    Arguments:
        show (boolean): makes the tool visible

    Return:
        pyfbsdk.FBTool()

    """
    tool = CutsceneBatchNetworkExportTool()

    if show:
        tool.Show()

    return tool