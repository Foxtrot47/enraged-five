import pyfbsdk as mobu

from RS.Core.ReferenceSystem import Manager
from RS.Utils import Namespace
from RS.Core.Scene.models import ComponentModel
from RS.Core.Scene.models.modelItems import ComponentModelItem


class ReferenceModel(ComponentModel.ComponentModel):
    """
    Model for displaying valid references that can be attached to a moving cutscene
    """
    def __init__(self, referenceType, parent=None):
        """
        Constructor

        Arguments:
            referenceType (RS.Core.ReferenceSystem.Type): The class of the type of reference to filter by
            parent (QtCore.QObject): parent qobject
        """
        self._referenceType = referenceType
        self._manager = Manager.Manager()
        super(ReferenceModel, self).__init__(componentType=(mobu.FBModelNull,), parent=parent)

    def _references(self):
        """
        References in the scene of the valid reference type

        Returns:
            dictionary
        """
        return {reference.ModelNull:reference for reference in self._manager.GetReferenceListAll()
                if isinstance(reference, self._referenceType)}

    def _components(self):
        """
        Overrides built-in method

        Returns the model nulls used by the referenece editor

        Returns:
            list
        """
        references = self._references()
        return references.values()

    def setupModelData(self, parent):
        """
        model type data sent to model item
        """
        self._componentsDictionary = {}
        # convert generators to lists so the values do not change
        references = self._references()
        for reference in references.itervalues():
            item = ReferenceModelItem(reference, parent=parent)

            if item.isValid():
                parent.appendChild(item)
                self.itemCreatedSignal.emit(item)
                self._componentsDictionary[reference] = item


class ReferenceModelItem(ComponentModelItem.ComponentItem):

    _referenceObjectNames = {
        #TODO: Add more filters for other types of reference classes
        # Manager.MocapBasePreviz: "OFFSET",
        # Manager.MocapBuildAsset: "OFFSET",
        # Manager.MocapCamera: "OFFSET",
        # Manager.MocapComplex: "OFFSET",
        # Manager.MocapEssential: "OFFSET",
        # Manager.MocapReferencePose: "OFFSET",
        # Manager.MocapNonEssential:["OFFSET"],
        Manager.MocapGsSkeleton: ["([a-z0-9_]+)MocapOffset", "OFFSET"],
        Manager.MocapBlockingModel: ["Cube"],
        Manager.MocapPropToy: ["([a-z0-9_]+)Offset_Null", "([a-z0-9_]+)MocapOffset"],
        Manager.MocapStage: ["stage([a-z0-9_]+)"],
        Manager.MocapSlate: ["Mocap_Offset"]
        }

    def __init__(self, reference, parent=None):
        """
        Constructor

        Arguments:
            reference (RS.Core.RefernceSystem.Type): reference object
            parent (QtCore.QObject): the parent qobject
        """
        self._reference = reference
        component = self._getComponent()
        super(ReferenceModelItem, self).__init__(component, parent=parent)

    def _getComponent(self):
        """
        Gets a valid component that can be attached to the moving cutscene from the given reference

        Returns:
            mobu.FBComponent
        """

        namespace = self._reference.Namespace
        for name in self._referenceObjectNames.get(self._reference.__class__, ["OFFSET"]):
            fbnamespace = Namespace.GetFBNamespace(namespace)
            component = Namespace.FindComponentInNamespace(fbnamespace, name, ignoreCase=True, objectType=mobu.FBModel, exactType=False)
            if component is not None:
                return component

    def isValid(self):
        """
        Does the component have a valid component

        Returns:
            bool
        """
        return self._component is not None