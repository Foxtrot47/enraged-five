import unittest

from RS.Tools.UI.MovingCutscene.UnitTest import cutscenes


def getTestSuites():
    """Gathers suites together to test

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()
    suite.addTest(cutscenes.suite())
    return suite


def Run():
    """Runs unittests"""
    unittest.TextTestRunner(verbosity=2).run(getTestSuites())


if __name__ == "__main__":
    Run()
