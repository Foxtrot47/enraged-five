"""
UI for tool for setting keys on in-betweens of poses while animating
"""
import os

import pyfbsdk as mobu
from PySide import QtGui, QtCore

from RS import Config, Globals
from RS.Tools import UI
from RS.Tools.UI import InBetweener
from RS.Core.Scene.widgets import ComponentComboBox
from RS.Tools.UI.InBetweener.Models import core as core
from RS.Tools.UI.InBetweener import const


class SliderLayout(QtGui.QHBoxLayout):
    """
    Layout used for all slider based components of the InBetweener tool
    """
    def __init__(self, text, characterBox=None, part=None, side=None):
        """
        Constructor

        Args:
            text (str): Label for the slider Layout

        Keyword Args:
            characterBox (ComponentComboBox): Contains the current active character or None (for selected slider Layout)
            part (str): Body Part
            side (str): Left, Right, or None
        """
        super(SliderLayout, self).__init__()

        # Instance variables
        self.characterBox = characterBox
        self.part = part
        self.side = side or ""

        # Widgets
        label = QtGui.QLabel(text)
        self.spinbox = QtGui.QSpinBox()
        self.slider = QtGui.QSlider(QtCore.Qt.Orientation.Horizontal)
        minusButton = QtGui.QPushButton("-")
        plusButton = QtGui.QPushButton("+")

        # Configure Widgets
        # Format the text label so that all instances line up in the UI
        label.setFixedWidth(100)
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignCenter)

        # Assign the spinbox and slider a default min and max range
        self.spinbox.setMinimum(const.Values.DEFAULT_MIN)
        self.spinbox.setMaximum(const.Values.DEFAULT_MAX)
        self.slider.setMinimum(const.Values.DEFAULT_MIN)
        self.slider.setMaximum(const.Values.DEFAULT_MAX)

        # Format the +/- increment buttons to make sure they don't unnecessarily stretch wider
        minusButton.setFixedWidth(30)
        plusButton.setFixedWidth(30)

        # Add Widgets to Layout
        self.addWidget(label)
        self.addWidget(self.spinbox)
        self.addWidget(self.slider)
        self.addWidget(minusButton)
        self.addWidget(plusButton)

        # Connect Spinbox and Slider functions to update each other when one changes
        self.spinbox.valueChanged.connect(self.updateSlider)
        self.slider.valueChanged.connect(self.updateSpinbox)

        # Connect the set in between function to either the spinbox or slider
        self.spinbox.valueChanged.connect(self.setInBetween)

        # Connect increment up/down functions to the "quick nav" buttons
        minusButton.clicked.connect(self.subtractIncrement)
        plusButton.clicked.connect(self.addIncrement)

    def addIncrement(self):
        """ 'Quick nav button' increment up """
        self.spinbox.setValue(self.spinbox.value() + 10)

    def subtractIncrement(self):
        """ 'Quick nav button' increment down """
        self.spinbox.setValue(self.spinbox.value() - 10)

    def updateSpinbox(self):
        """ Set the spinbox value if the slider is changed """
        self.spinbox.setValue(self.slider.value())

    def updateSlider(self):
        """ Set the slider value if the spinbox is changed """
        self.slider.setValue(self.spinbox.value())

    def setInBetween(self):
        """
        If the characterBox is passed, set the in-between on the filtered components of the current character.
        If the characterBox is not passed, set the in-between on anything that is currently selected in the scene.
        """
        if self.characterBox and self.part:
            core.setInBetween(self.spinbox.value(), core.getComponents(self.characterBox.currentText(), self.part, self.side))
        else:
            core.setInBetween(self.spinbox.value(), Globals.SceneModels().Selected)


class Window(QtGui.QWidget):
    """
    Main window
    """
    FullBodyIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "motionbuilder", "etc", "HIKCharacterToolFullBody.png"))
    BodyPartIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "motionbuilder", "etc", "HIKCharacterToolBodyPart.png"))
    SelectionIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "motionbuilder", "etc", "HIKCharacterToolSelection.png"))

    def __init__(self, parent=None):
        """
        Constructor

        Args:
            parent (QtGui.QWidget): parent widget
        """
        super(Window, self).__init__(parent=parent)

        # Create the character drop down that has callbacks for auto-refreshing characters in the scene
        # This widget must be created before Layouts in order to pass it to each slider layout as an arg
        self.characterBox = ComponentComboBox.ComponentComboBox(componentType=mobu.FBCharacter)

        # Layouts
        # Tool options
        optionsLayout = QtGui.QVBoxLayout()
        customOvershootValueLayout = QtGui.QHBoxLayout()
        keyModeLayout = QtGui.QHBoxLayout()

        # Selection Layout is independent from character specific as it will control a global selection of components
        selectionLayout = SliderLayout("Selected")

        # Character specific Layouts get passed text, the character selector combobox, and 1 or 2 strings for filtering
        # out the specific rig controls for each respective part of the body
        fullBodyLayout = SliderLayout("Full Body", self.characterBox, const.Parts.ALL)
        hipsLayout = SliderLayout("Hips", self.characterBox, const.Parts.HIPS)
        spineLayout = SliderLayout("Spine", self.characterBox, const.Parts.SPINE)
        headLayout = SliderLayout("Head", self.characterBox, const.Parts.HEAD)
        leftLegLayout = SliderLayout("Left Leg", self.characterBox, const.Parts.LEG, const.Sides.LEFT)
        rightLegLayout = SliderLayout("Right Leg", self.characterBox, const.Parts.LEG, const.Sides.RIGHT)
        leftArmLayout = SliderLayout("Left Arm", self.characterBox, const.Parts.ARM, const.Sides.LEFT)
        rightArmLayout = SliderLayout("Right Arm", self.characterBox, const.Parts.ARM, const.Sides.RIGHT)
        leftFingersLayout = SliderLayout("Left Fingers", self.characterBox, const.Parts.FINGERS, const.Sides.LEFT)
        rightFingersLayout = SliderLayout("Right Fingers", self.characterBox, const.Parts.FINGERS, const.Sides.RIGHT)

        # Will contain all our character specific slider Layouts
        characterLayout = QtGui.QVBoxLayout()

        # Left/Right Layouts that will be set within a main Layout for visually organizing everything
        leftLayout = QtGui.QVBoxLayout()
        rightLayout = QtGui.QVBoxLayout()
        mainLayout = QtGui.QHBoxLayout()

        # Create list of slider layouts used to iterate through for both adding to the character layout and for function use
        self.sliderLayoutList = [hipsLayout, spineLayout, headLayout, leftLegLayout,rightLegLayout,
                                 leftArmLayout, rightArmLayout, leftFingersLayout, rightFingersLayout,
                                 selectionLayout, fullBodyLayout]

        # Widgets
        # For the Character/Selection sliders
        selectionGroupBox = QtGui.QGroupBox()
        characterGroupBox = QtGui.QGroupBox("Character")

        # For the options
        optionsGroupBox = QtGui.QGroupBox("Options")
        self.overshootToggle = QtGui.QCheckBox("Overshoot")
        self.overshootAmount = QtGui.QSpinBox()
        keyModeGroupBox = QtGui.QGroupBox("Key Mode")
        self.fullBodyKMButton = QtGui.QPushButton(self.FullBodyIcon, None)
        self.bodyPartKMButton = QtGui.QPushButton(self.BodyPartIcon, None)
        self.selectionKMButton = QtGui.QPushButton(self.SelectionIcon, None)

        # Add Widgets/Layouts To Layouts
        # For the Character/Selection sliders
        characterLayout.addWidget(self.characterBox)
        characterLayout.addWidget(self.addSeparator())
        characterLayout.addLayout(fullBodyLayout)
        characterLayout.addWidget(self.addSeparator())
        # Loop through the slider layouts to add them to the UI
        for layout in self.sliderLayoutList:
            if layout is not selectionLayout and layout is not fullBodyLayout:
                characterLayout.addLayout(layout)

        # For the options
        customOvershootValueLayout.addWidget(self.overshootToggle)
        customOvershootValueLayout.addWidget(self.overshootAmount)
        keyModeLayout.addWidget(self.fullBodyKMButton)
        keyModeLayout.addWidget(self.bodyPartKMButton)
        keyModeLayout.addWidget(self.selectionKMButton)
        optionsLayout.addLayout(customOvershootValueLayout)
        optionsLayout.addWidget(keyModeGroupBox)

        # Main Layout organization
        leftLayout.addWidget(selectionGroupBox)
        leftLayout.addWidget(characterGroupBox)
        rightLayout.addWidget(optionsGroupBox)
        mainLayout.addLayout(leftLayout)
        mainLayout.addLayout(rightLayout)

        # Set Layouts to Widgets
        selectionGroupBox.setLayout(selectionLayout)
        characterGroupBox.setLayout(characterLayout)
        optionsGroupBox.setLayout(optionsLayout)
        keyModeGroupBox.setLayout(keyModeLayout)

        # Set the main window layout
        self.setLayout(mainLayout)

        # Configure Widgets
        characterGroupBox.setAlignment(QtCore.Qt.AlignHCenter)
        self.overshootAmount.setRange(100, 999)
        self.overshootAmount.setValue(200)
        rightLayout.setAlignment(optionsGroupBox, QtCore.Qt.AlignTop)

        # Hook up connections
        self.overshootToggle.stateChanged.connect(self.setCustomOvershootOnOff)
        self.overshootAmount.valueChanged.connect(self.setCustomOvershoot)
        self.fullBodyKMButton.clicked.connect(lambda: self.activeKeyMode(keyModeString=const.KeyMode.FULL))
        self.bodyPartKMButton.clicked.connect(lambda: self.activeKeyMode(keyModeString=const.KeyMode.PART))
        self.selectionKMButton.clicked.connect(lambda: self.activeKeyMode(keyModeString=const.KeyMode.SELECTION))

        # Set the overshoot and active key mode options
        self.setCustomOvershootOnOff()
        self.activeKeyMode(str(core.getKeyingMode(core.getCharacterNode(self.characterBox.currentText()))))

    @staticmethod
    def addSeparator():
        """ Simple separator to plug in when we want to visually separate things """
        separator = QtGui.QFrame()
        separator.setFrameStyle(QtGui.QFrame.HLine | QtGui.QFrame.Sunken)
        separator.setLineWidth(2)

        return separator

    def activeKeyMode(self, keyModeString=None):
        """ """
        characterNode = core.getCharacterNode(self.characterBox.currentText())
        if characterNode:
            if const.KeyMode.FULL in keyModeString:
                self.fullBodyKMButton.setEnabled(False)
                self.bodyPartKMButton.setEnabled(True)
                self.selectionKMButton.setEnabled(True)
            elif const.KeyMode.PART in keyModeString:
                self.fullBodyKMButton.setEnabled(True)
                self.bodyPartKMButton.setEnabled(False)
                self.selectionKMButton.setEnabled(True)
            elif const.KeyMode.SELECTION in keyModeString:
                self.fullBodyKMButton.setEnabled(True)
                self.bodyPartKMButton.setEnabled(True)
                self.selectionKMButton.setEnabled(False)
            else:
                return
            core.setKeyingMode(characterNode=characterNode, keyModeString=keyModeString)

    def setCustomOvershootOnOff(self):
        """ Clear the character selector and repopulate it """
        if self.overshootToggle.isChecked():
            self.overshootAmount.setEnabled(True)
            # When the toggle is on, set the overshoot to the specified value in the spinbox
            self.setCustomOvershoot()
        else:
            self.overshootAmount.setEnabled(False)
            # When the toggle is off, set all sliders to default range
            for layout in self.sliderLayoutList:
                layout.slider.setRange(const.Values.DEFAULT_MIN, const.Values.DEFAULT_MAX)
                layout.spinbox.setRange(const.Values.DEFAULT_MIN, const.Values.DEFAULT_MAX)

    def setCustomOvershoot(self):
        """ Clear the character selector and repopulate it """
        for layout in self.sliderLayoutList:
            layout.slider.setRange(-self.overshootAmount.value(), self.overshootAmount.value())
            layout.spinbox.setRange(-self.overshootAmount.value(), self.overshootAmount.value())


@UI.Run("InBetweener v{}".format(InBetweener.__version__), url="https://hub.gametools.dev/display/ANIM/InBetweener")
def Run(show=None):
    """
    Show tool in Motion Builder
    Args:
        show (Boolean): should the window be visible
    Return:
        RS.Tools.UI.QtBannerWindowBase()
    """
    return Window()
