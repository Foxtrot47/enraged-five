'''

 Script Path: RS\Tools\UI\Camera\QT\FPS_Toolbox.py

 Written And Maintained By: Kathryn Bodey

 Created: 25.11.14

 Description: Toolbox for Faceware transfer
			  Modules in FacewareLive.py
			  url:bugstar:1822499

'''


from pyfbsdk import *

from PySide import QtGui
from PySide import QtCore

from RS import ProjectData
import RS.Config
import RS.Core.AssetSetup.FPS_Cam_Setup

reload(RS.Core.AssetSetup.FPS_Cam_Setup)

# compile .ui file to populate the main window
RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\UI\\Camera\\QT\\FPS_Toolbox.ui'.format(RS.Config.Script.Path.Root), '{0}\\RS\\Tools\\UI\\Camera\\QT\\FPS_Toolbox_compiled.py'.format(RS.Config.Script.Path.Root))

import RS.Tools.UI.Camera.QT.FPS_Toolbox_compiled as fpsCompiled
reload(fpsCompiled)


class MainWindow(RS.Tools.UI.QtMainWindowBase, fpsCompiled.Ui_MainWindow):
	def __init__(self):

		RS.Tools.UI.QtMainWindowBase.__init__(self, None, title='FPS Toolbox')

		self.setupUi(self)

		self.CharacterList = []
		self.CharacterCheckBoxDict = {}
		self.spinBox = None
		self.comboBoxTarget = None
		self.comboBoxSource = None

		# add banner
		banner = RS.Tools.UI.BannerWidget(name='FPS Toolbox', helpUrl="")
		self.vboxlayout_banner.addWidget (banner)

		# build takes list
		for i in RS.Globals.Characters:
			if i.LongName.endswith('_gs'):
				None
			else:
				self.CharacterList.append(i.LongName)

		# pushbuttons
		pushButtonDisable = QtGui.QPushButton(self.centralwidget)
		pushButtonDisable.setGeometry(QtCore.QRect(280, 255, 55, 14))
		pushButtonDisable.setText("Disable All")
		pushButtonEnable = QtGui.QPushButton(self.centralwidget)
		pushButtonEnable.setGeometry(QtCore.QRect(220, 255, 55, 14))
		pushButtonEnable.setText("Enable All")

		# create scroll area
		scrollSize = 165

		listLength = len(self.CharacterList)

		# 8 entries can sit in the default scrollarea size, any more than this need 20*
		# more space for the scroll bar to appear and have enough scroll to fit
		Label = QtGui.QLabel(self.centralwidget)
		Label.setGeometry(QtCore.QRect(10, 62, 500, 15))
		Label.setText('Character List:')

		if listLength > 8:
			excessList = listLength - 8
			scrollSize = scrollSize + (excessList * 20)

		scrollArea = QtGui.QScrollArea(self.centralwidget)
		scrollArea.setGeometry(QtCore.QRect(10, 80, 325, 171))
		scrollArea.setWidgetResizable(False)
		scrollAreaWidgetContents = QtGui.QWidget()
		scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 305, scrollSize))
		scrollArea.setWidget(scrollAreaWidgetContents)

		# create scrollarea take check boxes
		counter = 8
		if self.CharacterList:
			for i in self.CharacterList:
				# checkboxes
				characterNameCheckBox = QtGui.QCheckBox(scrollAreaWidgetContents)
				characterNameCheckBox.setGeometry(QtCore.QRect(5, counter, 13, 13.5))
				# labels
				characterNameLabel = QtGui.QLabel(scrollAreaWidgetContents)
				characterNameLabel.setGeometry(QtCore.QRect(25, counter, 500, 15))
				characterNameLabel.setText(i)

				self.CharacterCheckBoxDict[ characterNameLabel ] = characterNameCheckBox

				counter = counter + 20

				fps_exists = self.FPSExists(i)

				if fps_exists == True:
					characterNameCheckBox.setStyleSheet("color: black; background-color: rgb(229,255,204);")
				else:
					characterNameCheckBox.setStyleSheet("color: black; background-color: rgb(255,204,204);")

		# label key for checkboxes
		font = QtGui.QFont()
		font.setPointSize(7)

		redCheckBox = QtGui.QCheckBox(self.centralwidget)
		redCheckBox.setGeometry(QtCore.QRect(10, 257, 8, 8))
		redCheckBox.setStyleSheet("color: black; background-color: rgb(255,204,204);")
		redCheckBox.setEnabled(False)
		redLabel = QtGui.QLabel(self.centralwidget)
		redLabel.setGeometry(QtCore.QRect(26, 254, 150, 15))
		redLabel.setText('FPS does not currently exist')
		redLabel.setFont(font)

		greenCheckBox = QtGui.QCheckBox(self.centralwidget)
		greenCheckBox.setGeometry(QtCore.QRect(10, 273, 8, 8))
		greenCheckBox.setStyleSheet("color: black; background-color: rgb(229,255,204);")
		greenCheckBox.setEnabled(False)
		greenLabel = QtGui.QLabel(self.centralwidget)
		greenLabel.setGeometry(QtCore.QRect(26, 270, 190, 15))
		greenLabel.setText('FPS exists - it will be regenerated if selected')
		greenLabel.setFont(font)

		# callbacks
		self.pushbutton_refresh.pressed.connect(self.Refresh)
		pushButtonEnable.released.connect(self.EnableCheckboxes)
		pushButtonDisable.released.connect(self.DisableCheckboxes)
		self.pushButton.released.connect(self.GenerateFPS)


	def Refresh(self):
		self.close()
		Run()


	def EnableCheckboxes(self):
		for key, value in self.CharacterCheckBoxDict.iteritems():
			value.setChecked(True)


	def DisableCheckboxes(self):
		for key, value in self.CharacterCheckBoxDict.iteritems():
			value.setChecked(False)


	def FPSExists(self, characterName):
		found_object = False

		split_name = characterName.split(':')
		namespace = split_name[0]

		fpscampivot_null = RS.Utils.Scene.FindModelByName(namespace + ':' + 'FirstPersonCamPivot', True)

		if fpscampivot_null:
			found_object = True

		return found_object


	def GenerateFPS(self):
		# get user selected characetrs
		checkedCharacterList = []

		for key, value in self.CharacterCheckBoxDict.iteritems():
			if value.isChecked() == True:
				checkedCharacterList.append(key.text())

		if len(checkedCharacterList) > 0:
			RS.Core.AssetSetup.FPS_Cam_Setup.initialiseFPS_Cam_Setup(checkedCharacterList)
		else:
			FBMessageBox('R* Error', 'No characters were selected.', 'Ok')

		self.close()
		Run()



def Run():

	if not ProjectData.data.GetConfig("FPSTool", defaultValue=False):
		FBMessageBox('R* Error', 'FPS Toolbox is currently not available in the {0} project'.format(ProjectData.data.Project()), 'Ok')
	else:
		form = MainWindow()
		form.show()
