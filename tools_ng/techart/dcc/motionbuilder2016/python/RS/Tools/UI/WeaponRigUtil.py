from pyfbsdk import *
from PySide import QtCore, QtGui


import RS.Tools.UI
import RS.Core.AssetSetup.WeaponRigSetup

reload(RS.Core.AssetSetup.WeaponRigSetup)

class ComponentTreeItem( QtGui.QTreeWidgetItem ):
    def __init__( self, parent ):
        QtGui.QTreeWidgetItem.__init__( self )

     
        

class RigListWidget( QtGui.QTreeWidget ):
    def __init__(self):
        QtGui.QTreeWidget.__init__(self)
        self.setHeaderLabels( [ "Weapon Rig" ] )
        self.currentItemChanged.connect( self.ChangeTreeSelectionEvent )
        self.selectedItem = None

    def PopulateList(self, itemDict):
        self.clear()
        for itemKey in itemDict.keys():
            itemWidget = ComponentTreeItem( self )
            itemWidget.setText( 0, itemKey )
            itemWidget.setData( 0, QtCore.Qt.UserRole, itemDict[ itemKey ] )
            
            self.addTopLevelItem( itemWidget ) 
            
            
    def ChangeTreeSelectionEvent( self, item ):
        
        try:
            
            self.parent().parent().RefreshControllerList()
        except:
            pass

            
class RigControlsList( QtGui.QTreeWidget ):
    def __init__(self):
        QtGui.QTreeWidget.__init__(self)
        self.setHeaderLabels( ["Active", "Controls" ] ) 
        self.itemChanged.connect( self.ChangeTreeSelectionEvent )
        self.selectedItem = None
        
    def PopulateList(self, itemDict):
        self.clear()
        for itemKey in itemDict.keys():
            print itemDict[ itemKey ]
            itemWidget = ComponentTreeItem( self )
            itemWidget.setText( 1, itemKey )
            itemWidget.setData( 1, QtCore.Qt.UserRole, itemDict[ itemKey ] )

            iActive = itemDict[itemKey].GetActive()
            

            if iActive == True:
                itemWidget.setText( 0, str('X') )
            else:
                itemWidget.setText( 0, str('-') )
                
            self.addTopLevelItem( itemWidget )
            
    def ChangeTreeSelectionEvent( self, item ):
        print "CHANGING SELECTIONS!"
        try:
            self.selectedItem = item.data( 0, QtCore.Qt.UserRole )
        except:
            pass                 
                                  

class MainWidget( QtGui.QWidget ):
    def __init__(self):
        QtGui.QWidget.__init__( self )
        self.setLayout(QtGui.QGridLayout())
        

class WeaponRigTool( RS.Tools.UI.QtMainWindowBase ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, title = 'WeaponRigTool', size = [ 500, 600 ] )
        self.__rigs = {}
               
        
        self.testlabel = QtGui.QLabel()
        self.testlabel.setText("...")
        
        self.__rigManager =  RS.Core.AssetSetup.WeaponRigSetup.WeaponRigManager()
        
        
        self.__rigListWidget = RigListWidget()
        self.__rigControlListWidget = RigControlsList()
        self.__mainWidget = MainWidget()
        
        self.__refreshButton = QtGui.QPushButton( "Refresh", self )
        self.__refreshButton.pressed.connect( self.RefreshRigList )
        
        self.__addRigToSelButton = QtGui.QPushButton( "Add Rig To Selected", self )
        self.__addRigToSelButton.pressed.connect( self.AddRigToSelection )
        
        self.__activateControllerButton = QtGui.QPushButton( "Activate Controller", self )
        self.__activateControllerButton.pressed.connect( self.ActivateRigController )
        self.__mainWidget.layout().setColumnMinimumWidth(0, 250)
        self.__mainWidget.layout().setColumnMinimumWidth(1, 225)
        

        
        self.__mainWidget.layout().addWidget(self.__rigListWidget, 0,0)
        self.__mainWidget.layout().addWidget(self.__rigControlListWidget, 0,1)
        self.__mainWidget.layout().addWidget(self.__activateControllerButton, 0,2)
        
        self.__mainWidget.layout().addWidget(self.__addRigToSelButton, 1,0)
        self.__mainWidget.layout().addWidget(self.__refreshButton, 1,1)
        self.__mainWidget.layout().addWidget(self.testlabel, 2,0)
        
        
        
        self.setCentralWidget(self.__mainWidget)
        
        self.RefreshRigList()
        

    def ActivateRigController(self):

        if self.__rigControlListWidget.currentItem() and self.__rigListWidget.currentItem():

            selectedRig = self.__rigListWidget.currentItem().data( 0, QtCore.Qt.UserRole )
            selectedController = self.__rigControlListWidget.currentItem().data( 1, QtCore.Qt.UserRole )



            self.__rigManager.ActivateController(selectedRig.GetName(), selectedController.GetName())
            self.RefreshControllerList()

            

    def AddRigToSelection(self):
        sceneModelList = FBModelList()
        FBGetSelectedModels (sceneModelList, None, True)
        
        for model in sceneModelList:
            if 'dummy' in model.Name.lower():
                self.__rigManager.SetupRigOnComponent(model)
                    
        self.RefreshRigList()   
                 

    def RefreshControllerList(self):
        if self.__rigControlListWidget.currentItem():
            selectedItem = self.__rigControlListWidget.currentItem().data( 1, QtCore.Qt.UserRole )
            selectedControls = selectedItem.GetRigControllers()
            self.__rigControlListWidget.PopulateList(selectedControls)

    def RefreshRigList(self):
        self.__rigs = {}
        self.__rigManager.DetectRigsInScene()
        self.__rigs = self.__rigManager.GetWeaponRigs()     
        
            
        self.__rigListWidget.PopulateList(self.__rigs)
        
    
        
    def closeEvent( self, event ):
 
        
        QtGui.QMainWindow.closeEvent( self, event ) 
        
def Run():
    tool = WeaponRigTool()
    tool.show()
    
Run()    