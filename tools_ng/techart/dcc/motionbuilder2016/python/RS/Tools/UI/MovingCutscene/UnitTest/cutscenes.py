"""
Unit test for each individual type of reference
"""
import sys

import unittest
import pyfbsdk as mobu


from RS.Utils import Namespace
from RS.Unittests import utils
from RS.Utils.Scene import Component
from RS.Tools.UI.MovingCutscene.UnitTest import base
from RS.Tools.UI.MovingCutscene.Model import Components


def Run(className=None):
    """Run unittests from this module

    Args:
        className (str, optional): the name of the class to run
    """
    unittest.TextTestRunner(verbosity=0).run(suite(className))


def suite(className=None):
    """A suite to hold all the tests from this module

    Args:
        className (str, optional): the name of the class whose test should be run. Runs all the tests if None.
    Returns:
        unittest.TestSuite
    """
    module = sys.modules[__name__]
    return utils.getSuite(module, "test_Cutscene", className)


class Test_CreateCutscene(base.CutsceneBase):
    """Tests adding a cutscene"""

    def test_Cutscene(self):
        """Testing creating cutscene"""
        if not super(Test_CreateCutscene, self).test_Cutscene():
            return

        reference = self.assertCreate(self.vehiclePath)
        self.assertCutscene(":".join([reference.Namespace, self.driverName]))
        self.assertDelete(reference)


class Test_AddCharacter(base.CutsceneBase):
    """Tests adding a character to a cutscene"""

    def test_Cutscene(self):
        """Testing adding character"""
        if not super(Test_AddCharacter, self).test_Cutscene():
            return

        vehicleReference = self.assertCreate(self.vehiclePath)
        characterReference = self.assertCreate(self.characterPath)
        movingCutscene = self.assertCutscene(":".join([vehicleReference.Namespace, self.driverName]))

        markerName = ":".join([characterReference.Namespace, self.characterMarker])
        self.assertMarker(movingCutscene, markerName)

        self.assertDelete(characterReference)
        self.assertDelete(vehicleReference)


class Test_AddCamera(base.CutsceneBase):
    """Tests adding a camera to a cutscene"""

    def test_Cutscene(self):
        """Testing adding camera"""
        if not super(Test_AddCamera, self).test_Cutscene():
            return

        vehicleReference = self.assertCreate(self.vehiclePath)
        testCamera = mobu.FBCamera("test_Camera")

        movingCutscene = self.assertCutscene(":".join([vehicleReference.Namespace, self.driverName]))

        self.assertMarker(movingCutscene, testCamera.LongName)

        self.assertDelete(vehicleReference)
        testCamera.FBDelete()


class Test_AddProp(base.CutsceneBase):
    """Tests adding a prop to a cutscene"""

    def test_Cutscene(self):
        """Testing adding prop"""
        if not super(Test_AddProp, self).test_Cutscene():
            return

        vehicleReference = self.assertCreate(self.vehiclePath)
        propReference = self.assertCreate(self.propPath)
        movingCutscene = self.assertCutscene(":".join([vehicleReference.Namespace, self.driverName]))

        markerName = ":".join([propReference.Namespace, self.propMarker])
        self.assertMarker(movingCutscene, markerName)

        self.assertDelete(propReference)
        self.assertDelete(vehicleReference)


class Test_AddVehicle(base.CutsceneBase):
    """Tests adding a vehicle to a cutscene"""

    def test_Cutscene(self):
        """Testing adding vehicle"""
        if not super(Test_AddVehicle, self).test_Cutscene():
            return

        vehicleReferenceA = self.assertCreate(self.vehiclePath)
        vehicleReferenceB = self.assertCreate(self.vehiclePath)
        movingCutscene = self.assertCutscene(":".join([vehicleReferenceA.Namespace, self.driverName]))

        markerName = ":".join([vehicleReferenceB.Namespace, self.vehicleMarker])
        self.assertMarker(movingCutscene, markerName)

        self.assertDelete(vehicleReferenceB)
        self.assertDelete(vehicleReferenceA)


class Test_AddMocap(base.CutsceneBase):
    """Tests adding a mocap to a cutscene"""

    def test_Cutscene(self):
        """Testing adding mocap"""
        if not super(Test_AddMocap, self).test_Cutscene():
            return

        vehicleReference = self.assertCreate(self.vehiclePath)
        mocapReference = self.assertCreate(self.mocapPath)
        movingCutscene = self.assertCutscene(":".join([vehicleReference.Namespace, self.driverName]))

        markerName = ":".join([mocapReference.Namespace, self.mocapMarker])
        self.assertMarker(movingCutscene, markerName)

        self.assertDelete(mocapReference)
        self.assertDelete(vehicleReference)


class Test_FreezeCutscene(base.CutsceneBase):
    """Tests freezing a cutscene"""

    def test_Cutscene(self):
        """Testing adding cutscene"""
        if not super(Test_FreezeCutscene, self).test_Cutscene():
            return

        vehicleReference = self.assertCreate(self.vehiclePath)
        movingCutscene = self.assertCutscene(":".join([vehicleReference.Namespace, self.driverName]))
        movingCutscene.setFreezeActive(True)
        self.assertEqual(movingCutscene._freezenull.constraint().Active, 1.0)
        movingCutscene.setFreezeActive(False)

        self.assertDelete(vehicleReference)


class Test_SourceCutscene(base.CutsceneBase):
    """Tests setting a cutscene to source motion"""

    def test_Cutscene(self):
        """Testing adding cutscene"""
        if not super(Test_SourceCutscene, self).test_Cutscene():
            return

        vehicleReference = self.assertCreate(self.vehiclePath)
        movingCutscene = self.assertCutscene(":".join([vehicleReference.Namespace, self.driverName]))
        movingCutscene.setZeroActive(True)
        self.assertEqual(movingCutscene._zeronull.constraint().Active, 1.0)
        movingCutscene.setZeroActive(False)

        self.assertDelete(vehicleReference)


class Test_ToggleCutscene(base.CutsceneBase):
    """Tests setting a disabling and re-activating the cutscene"""

    def test_Cutscene(self):
        """Testing toggling cutscene"""
        if not super(Test_ToggleCutscene, self).test_Cutscene():
            return

        vehicleReference = self.assertCreate(self.vehiclePath)
        movingCutscene = self.assertCutscene(":".join([vehicleReference.Namespace, self.driverName]))
        movingCutscene.setActive(True)
        self.assertEqual(movingCutscene._zeronull.constraint().Active, 1.0)
        movingCutscene.setActive(False)

        self.assertDelete(vehicleReference)


class Test_RemoveCutscene(base.CutsceneBase):
    """Tests removing a cutscene"""

    def test_Cutscene(self):
        """Testing adding cutscene"""
        if not super(Test_RemoveCutscene, self).test_Cutscene():
            return

        vehicleReference = self.assertCreate(self.vehiclePath)
        movingCutscene = self.assertCutscene(":".join([vehicleReference.Namespace, self.driverName]))

        namespaces = []
        for child in reversed([movingCutscene] + movingCutscene.allChildren()):
            if isinstance(child, Components.MovingCutscene):
                namespaces.append(child.namespace())

        movingCutscene.delete()

        for namespace in namespaces:
            if Component.IsDestroyed(namespace):
                continue
            Namespace.Delete(namespace, deleteNamespaces=True)

        self.assertDelete(vehicleReference)
        self.assertEquals(list(Components.MovingCutscene.all()), [])


class Test_DoubleTransform(base.CutsceneBase):
    """Tests there aren't double transforms when adding a cutscene"""

    def test_Cutscene(self):
        """Testing double transforms on cutscene"""
        if not super(Test_DoubleTransform, self).test_Cutscene():
            return

        vehicleReference = self.assertCreate(self.vehiclePath)
        movingCutscene = self.assertCutscene(":".join([vehicleReference.Namespace, self.driverName]))

        testCamera = mobu.FBCamera("test_Camera")
        self.assertMarker(movingCutscene, testCamera.LongName)

        self.assertTransforms(movingCutscene, testCamera)

        self.assertDelete(vehicleReference)
        testCamera.FBDelete()
