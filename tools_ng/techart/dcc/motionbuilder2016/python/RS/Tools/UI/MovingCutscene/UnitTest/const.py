import os


class ConfigPathDict(object):
    """Yaml config paths for unittests"""

    CUTSCENES_CONFIG_PATH = os.path.join(os.path.dirname(__file__), "configs", "cutscenes.yaml")
