import inspect

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Globals as glo
import RS.Utils.Creation as cre
import RS.Tools.MoverSnap as snap
import RS.Tools.UI

reload( snap )

class MoverSnapTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Mover Snap', size = [ 200, 106 ] )
        
    def Create( self, mainLayout ):

        # Setup layout region.
        x = FBAddRegionParam( 6, FBAttachType.kFBAttachLeft, "" )
        y = FBAddRegionParam( 30, FBAttachType.kFBAttachTop, "" )
        w = FBAddRegionParam( -6, FBAttachType.kFBAttachRight, "" )
        h = FBAddRegionParam( -6, FBAttachType.kFBAttachBottom, "" )
        mainLayout.AddRegion( "main", "main", x, y, w, h )
        
        boxLayout = FBVBoxLayout()
        mainLayout.SetControl( "main", boxLayout )
        
        btnEnable = FBButton()
        btnEnable.Caption = "Snap the Mover"
        btnEnable.Style = FBButtonStyle.kFBPushButton
        btnEnable.OnClick.Add( snap.moveSnap )
        
        boxLayout.Add( btnEnable, 30 )  
    
def Run( show = True ):
    tool = MoverSnapTool()
    
    if show:
        tool.Show()
        
    return tool