
from PySide import QtCore, QtGui

from RS import Config


class Icons(object):
    '''
    class for creating a dictionary of QIcons and Vectors (for color)
    '''
    # create qicons
    Black = QtGui.QIcon()
    CornFlower = QtGui.QIcon()
    Green = QtGui.QIcon()
    NavyBlue = QtGui.QIcon()
    Orange = QtGui.QIcon()
    Purple = QtGui.QIcon()
    Red = QtGui.QIcon()
    Yellow = QtGui.QIcon()

    # default image path
    iconFolderString = "{0}/Mocap/".format( Config.Script.Path.ToolImages )

    # add icon image to qicon
    Black.addPixmap(QtGui.QPixmap("{0}actor_color_black.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off )
    CornFlower.addPixmap(QtGui.QPixmap("{0}actor_color_cornflower.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off )
    Green.addPixmap( QtGui.QPixmap("{0}actor_color_green.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off )
    NavyBlue.addPixmap( QtGui.QPixmap("{0}actor_color_navyblue.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off )
    Orange.addPixmap( QtGui.QPixmap("{0}actor_color_orange.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off )
    Purple.addPixmap( QtGui.QPixmap("{0}actor_color_purple.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off )
    Red.addPixmap( QtGui.QPixmap("{0}actor_color_red.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off )
    Yellow.addPixmap( QtGui.QPixmap("{0}actor_color_yellow.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off )

    # add both qicons and color vectors to a dictionary
    # need to divide rgb by 256.0 to get mobu-friendly rgb
    colorDict = {
            "Black":(Black, (0/256.0, 0/256.0, 0/256.0)),
            "CornFlower":(CornFlower, (147/256.0, 204/256.0, 207/256.0)),
            "Green":(Green, (35/256.0, 166/256.0, 54/256.0)),
            "NavyBlue":(NavyBlue, (44/256.0, 46/256.0, 158/256.0)),
            "Orange":(Orange, (255/256.0, 79/256.0, 43/256.0)),
            "Purple":(Purple, (102/256.0, 0/256.0, 255/256.0)),
            "Red":(Red, (181/256.0, 18/256.0, 48/256.0)),
            "Yellow":(Yellow, (232/256.0, 186/256.0, 0/256.0))
            }

class GSModelTypes(object):
    '''
    class for defining what type of gs asset an object is
    '''
    Character = 'Character'
    Prop = 'Prop'
    Slate = 'Slate'