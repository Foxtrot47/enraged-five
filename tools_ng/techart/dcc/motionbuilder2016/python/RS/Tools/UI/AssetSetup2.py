"""
Description:
    Temporary UI for the new AssetSetup methods

Authors:
    David Vega <david.vega@rockstargames.com>
"""

from pyfbsdk import *

import os
from PySide import QtGui, QtCore
from functools import partial

import RS.Config
import RS.Perforce
import RS.Tools.UI
import RS.Core.AssetSetup.Parser
#reload(RS.Tools.UI)
gDevelopment = False


class AssetSetupWindow(RS.Tools.UI.QtBannerWindowBase):

    def __init__(self, *arguments, **keywordArguments):
        super(AssetSetupWindow, self).__init__(*arguments, **keywordArguments)

        self.Namespace = NamespaceComboBox()
        self.Tree = QtGui.QTreeView()
        self.Grid = QtGui.QGridLayout()
        self._mainLayout.addWidget(self.Namespace)
        self._mainLayout.addWidget(self.Tree)
        self._mainLayout.addLayout(self.Grid)
        self._mainLayout.setSpacing(0)
        self.PopulateTreeView()

    def PopulateTreeView(self, path=""):
        """
        Populates the TreeView with the archetypes that are currently supported by the AssetSetup
        Tool

        Arguments:
            path = string; path to the folder that holds the skel metafiles. If not path is provided
                           it defaults to Project/Assets/metadata/characters/skeleton
        """
        if not path:
            path = os.path.join(RS.Config.Project.Path.Assets, "metadata\characters\skeleton")
        self.setStyleSheet("""
             background-color: #2d2d30;
             font: bold 12px;
             """)

        model = QtGui.QStandardItemModel()
        model.setColumnCount(1)
        model.setHeaderData(0, QtCore.Qt.Horizontal, "Archetype")
        [model.appendRow(QtGui.QStandardItem(os.path.split(each)[-1])) for each in
         self.GetDirectoriesFromPerforce(path)]

        self.Tree.setModel(model)
        self.Tree.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.Tree.setStyleSheet("""
             background-color: #252526;
                                """)
        # To access the selectionChanged slot we have to access the selection model of the treeview
        self.Tree.selectionModel().selectionChanged.connect(partial(self.UpdateGridLayout))

    def UpdateGridLayout(self, newSelection, *args):
        """
        Updates the Grid Layout to contain/enable QPushButtons represented by the setup styles in the
        Project/TechArt/etc/config/characters/skeleton/archetype folders as determined by the selected
        archetype by the user

        Arguments:
            newSelection =  QItemSelection; a QObject that contains the list of the currently selected
                                            QStandardItemModels from the QTreeView

         """

        # The selectionChanged slot returns a QItemSelection
        # Which contains a QItemSelectionRange
        # For which we have to call indexes() to get a list of the QStandardItemModels that represent our selection
        # Then we can call the data method of the first QStandardItemModel to get the text value inside of it

        archetype = newSelection[0].indexes()[0].data()

        configPath = os.path.join(os.path.join(RS.Config.Tool.Path.TechArt,
                                               "etc\config\characters\skeleton", archetype))
        selectable = []
        for each in getattr(RS.Perforce.Run("files", ['-e','{}...setup.xml'.format(configPath)]), "Records"):
            eachPath = each["depotFile"]
            directory = os.path.split(eachPath)[0][len(configPath):]
            RS.Tools.UI.QPrint(eachPath, directory, "Yo!")
            if directory:
                selectable.append(self.GetButton(directory))

        # Disable buttons that shouldn't be selectable
        [each.widget().setEnabled(False) for each in self.Buttons if each.widget() not in selectable]

    def GetButton(self, name):
        """
        Returns the button requested, if it doesn't exist it is created.
        Buttons, after they have been created once, can be accessed by calling the variable
        self._(nameofbutton)Button, ex. self._ingameButton

        Arguments:
            name = string; text/name of the button
        """
        button = getattr(self, "_{}Button".format(name), None)
        numberOfChildren = self.Grid.count()

        if not button:
            button = QtGui.QPushButton()

            setattr(self, "_{}Button".format(name), button)
            imagePath = os.path.join(RS.Config.Tool.Path.TechArt,
                                     'dcc\motionbuilder{0}\images\AssetSetup'.format(RS.Config.Script.TargetBuild),
                                     '{}.png'.format(name))

            button.setIcon(QtGui.QIcon(imagePath))
            button.setIconSize(QtCore.QSize(84, 75))
            button.setMinimumSize((QtCore.QSize(84, 75)))
            #button.setText(name)

            self.Grid.addWidget(button, 75 * (numberOfChildren / 2), 84 * (numberOfChildren % 2))

            button.clicked.connect(partial(self.Setup, name))

        button.setEnabled(True)
        return button

    def Setup(self, characterType="cutscene", *args):
        """
        Creates rig for a skeleton in motion builder based on the .skel file provided.
        The skel file must be an xml file generated by the 3DS Max Exporter.

        Arguments:
            filepath = string;  path to the skel file that you want to use to create the rig.
            characterType = string; the type of rig you are building, currently only supports
                                 'cutscene' or 'ingame'
        """

        archetype = self.SelectedData

        if not archetype: return

        for eachPath in ["etc\config\characters\skeleton", r"metadata\characters\skeleton"]:
            path = os.path.join(os.path.join(RS.Config.Project.Path.Assets, eachPath, archetype))
            RS.Perforce.Sync(path)

        skelPath = os.path.join(RS.Config.Project.Path.Assets, r"metadata\characters\skeleton", archetype)
        skelPath = RS.Perforce.Run("files",  ["-e", '{}...skel'.format(skelPath)]).Records[0]["depotFile"]
        skelPath = os.path.join("x:\\", skelPath[2:])

        RS.Core.AssetSetup.Parser.Setup(filepath=skelPath, characterType=characterType,
                                        namespace=self.Namespace.Namespace)

        FBMessageBox("Asset Setup",
                     "{} has now been prepped with {} setup".format(
                         RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName),
                         characterType),
                     "Ok")

    @property
    def Buttons(self):
        """ list of the buttons beneath the grid layout"""
        return [self.Grid.itemAt(each) for each in
                xrange(self.Grid.count())]

    @property
    def SelectedData(self):
        """ the string value of the currently selected item in the treeview"""
        archetype = self.Tree.selectedIndexes()
        if not archetype:
            return ''
        return archetype[0].data()

    def GetDirectoriesFromPerforce(self, path, filter="skel"):
        recordSet = RS.Perforce.Run("files", ['-e', '{}...{}'.format(path, filter)])

        if not recordSet:
            recordSet = []

        return [os.path.split(each["depotFile"])[0] for each in recordSet]


class NamespaceComboBox(QtGui.QWidget):

    def __init__(self, parent=None, model=None):
        super(NamespaceComboBox, self).__init__(parent=parent)
        layout = QtGui.QHBoxLayout()

        self.comboBox = QtGui.QComboBox()
        refreshButton = QtGui.QPushButton("Refresh")

        layout.addWidget(self.comboBox)
        layout.addWidget(refreshButton)

        refreshButton.clicked.connect(self.RefreshNamespaces)
        refreshButton.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)
        if model is not None:
            self.setModel(model)
        self.RefreshNamespaces()

    def RefreshNamespaces(self):
        self.comboBox.clear()

        model = self.comboBox.model()
        model.setItem(0, 0, QtGui.QStandardItem("- - Select Namespace - -"))
        [model.setItem(index + 1, 0, QtGui.QStandardItem(namespace.Name))
         for index, namespace in enumerate(FBSystem().Scene.Namespaces)]

    @property
    def Namespace(self):
        if self.comboBox.currentText() != "- - Select Namespace - -":
            return self.comboBox.currentText()
        return ""


def Run(show=True):
    tool = AssetSetupWindow(size=[168, 200])
    if show:
        try:
            tool.show_data()
        except Exception, e:
            #print_this(e)
            pass
        tool.show()
    return tool