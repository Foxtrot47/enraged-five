"""
Description:
    Code for accessing and manipulating qt widgets that make up the current Qt Application

Author:
    David Vega <david.vega@rockstargames.com>
"""

import re
import ctypes
import platform
from functools import partial

from RS.Tools.CameraToolBox.PyCore.Decorators import memorize

from PySide import QtGui, QtCore, shiboken


MAIN_WINDOW_TITLE = "motionbuilder [0-9]+"


class QtBase(QtCore.QObject):
    """ Base class for all our QtWidgets. Improves filtering of widgets in 3rd party applications """


class FakeWidget(QtBase):
    """ Class to catch error when a menu is found """

    def actions(self):
        """ returns an empty list """
        return []


class AbstractCustomBase(QtGui.QWidget, QtBase):
    """ Custom Base Widget for registering events as signals """

    EnterSignal = QtCore.Signal()
    LeaveSignal = QtCore.Signal()
    ResizeSignal = QtCore.Signal()
    EventSignal = QtCore.Signal(object, object)

    def __init__(self, parent=None):
        """ Constructor
        Arguments:
            parent = QWidget; parent widget
        """
        super(AbstractCustomBase, self).__init__(parent=parent)
        self.setLayout(QtGui.QVBoxLayout())

    def event(self, event):
        """ event that gets called when any event on the widget is activated """
        super(AbstractCustomBase, self).event(event)
        self.EventSignal.emit(self, event)

    def resizeEvent(self):
        """ event on the widget is activated when the widget is resized """
        self.ResizeSignal.emit()
        return True

    def addWidget(self, widget):
        """ adds widget to the current layout """
        self.layout().addWidget(widget)

    def addLayout(self, layout):
        """ adds a layout to the current layout"""
        self.layout().addLayout(layout)


@memorize.memoized
def GetMainWindow(title=MAIN_WINDOW_TITLE):
    """
    Returns the QWidget instance of the Motion Builder Application that ran this method

    Returns:
        None or PySide.QtGui.QMainWindow()
    """
    mainWindow = []
    widgetsDict = {}

    for widget in QtGui.qApp.topLevelWidgets():
        try:
            windowTitle = str(widget.windowTitle())
        except:
            continue
        if not windowTitle:
            continue
        widgetsDict.setdefault(windowTitle, []).append(widget)
        if re.search(title, str(windowTitle), re.I):
            mainWindow.append(widget)

    # Safety check to make sure that if no window is named Motion Builder but there is a window with a title
    # that it gets it returns that window. Thank Geoff for the suggestion

    if not mainWindow:
        for title, widgets in widgetsDict.iteritems():
            for widget in widgets:
                if widget.parent() is None and title.strip():
                    mainWindow.append(widget)
                    break

    if mainWindow:
        # Get the C++ pointer for the Main Window QWidget
        pointer = shiboken.getCppPointer(mainWindow[0])[0]
        # Recast it as a QMainWindow
        return shiboken.wrapInstance(pointer, QtGui.QMainWindow)


def GetMenu(window):
    """
    Returns the QMenuBar instance of the menu bar of the QMainWindow

    Return:
        QtGui.QMenuBar or None
    """
    menuWidget = window.menuWidget()
    if menuWidget and menuWidget.layout():
        menuLayout = menuWidget.layout()
        # For MotionBuilder 2017 and newer versions
        for itemIdx in xrange(menuLayout.count()):
            menuItem = menuLayout.itemAt(itemIdx)
            if not isinstance(menuItem, QtGui.QWidgetItem):
                continue
            widget = menuItem.widget()
            if isinstance(widget, QtGui.QMenuBar):
                return widget
    else:
        return window.menuBar()


def GetMainMenu(filterByMenuNames=("File",), window=None):
    """
    Returns the QMenuBar instance of the the main menu bar in Motion Builder

    Arguments:
        filterByMenuNames (list[string, etc.]): menu names to look for in the menu bar. This is as an added check to
                                                make sure that the right menu is returned if multiple menus
        window (QWidget): widget to look for menu actions, defaults to the current main window of the application

    Return:
        None or QMenuBar
    """
    if window is None:
        window = GetMainWindow()

    # Remove duplicates from filterByMenuNames, convert it to a list and sort it
    filterByMenuNames = list(set(filterByMenuNames))
    filterByMenuNames.sort()

    menuBar = GetMenu(window)
    if menuBar:
        # Return menu names that match the names to filter by
        menuNames = [str(action.text()) for action in menuBar.actions() if action.text() in filterByMenuNames]
        # We remove duplicate names and sort the list
        menuNames = list(set(menuNames))
        menuNames.sort()
        if menuNames == filterByMenuNames:
            return menuBar
    return FakeWidget()


def GetChildrenByWindowTitle(parent, title):
    """
    Gets the children of widget based on their window title

    Arguments:
        parent (QWidget): parent widget
        title (string): title of child window
    """
    return [child for child in parent.children() if hasattr(child, "windowTitle") and title in child.windowTitle()]


def GetChildrenByClass(parent, classType, exactClass=False):
    """
    Gets the children of widget based on type

    Arguments:
        parent (QWidget): parent widget
        classType (class): the class of the children
        exactClass (boolean): check for the exact class and not any subclass

    Return:
        list[QWidget, etc.]
    """
    if exactClass:
        return [child for child in parent.children() if child.__class__ is classType]
    else:
        return [child for child in parent.children() if isinstance(child, classType)]


def GetActionDictionary(widget):
    """
    Get all the actions from a widget in a nested dictionary

    Arguments:
        widget (QtGui.QWidget): widget to get actions from

    Return:
        dictionary {string : action, string {string:action}, etc.}
    """
    actionDictionary = {}
    for action in widget.actions():
        actionDictionary[action.text()] = GetNestedActionDictionary(action)
    return actionDictionary


def GetNestedActionDictionary(action):
    """
    Recursive method for getting all the child actions of an action
    Arguments:
        action: (QAction): the top level action to get the children from

    Return:
        dictionary {string : action, string {string:action}, etc.} or QAction
    """
    menu = action.menu()

    actionDictionary = {"__QAction": action}
    if menu and menu.actions():
        for eachAction in menu.actions():
            actionDictionary[eachAction.text()] = GetNestedActionDictionary(eachAction)
    return actionDictionary


def GetMainWindowActions():
    """  returns a dictionary with all the QActions of the main window of Motion Builder """
    return GetActionDictionary(GetMainWindow())


def GetMainMenuActions(window=None):
    """
    returns a dictionary with all the QActions of the main menu of Motion Builder

    Arguments:
        window (QWidget): widget to look for menu actions, defaults to the current main window of the application
    """
    if window is None:
        window = GetMainWindow()
    menu = GetMainMenu(window=window)
    if menu:
        return GetActionDictionary(menu)


def executeString(string=""):
    """ Wraps exec so execute the passed string
    Arguments:
        string (string): some python command to execute

    """
    exec string


def GetShortCut(keyCombination, widget=None):
    """
    Gets custom keyboard shortcuts currently active in Motion Builder

    Arguments:
        keyCombination (string): keyboard shortcut, case is ignored and combination of keys should be separated by +
                                signs.
        widget (QtGui.QWidget): widget to add shortcut to. If no widget is provided it defaults to the main window
                                of the application
    Return:
        QtGui.Action
    """
    if not widget:
        widget = GetMainWindow()

    actions = GetActionDictionary(widget)

    shortcut = actions.get(keyCombination, None)

    if not shortcut:
        shortcut = QtGui.QAction(keyCombination, widget)
        shortcut.setShortcut(keyCombination)
        widget.addAction(shortcut)
    else:
        shortcut = shortcut["__QAction"]
    return shortcut


def AddShortCut(keyCombination, command, widget=None):
    """
    Gets custom keyboard shortcuts currently active in Motion Builder

    Arguments:
        keyCombination (string): keyboard shortcut, case is ignored and combination of keys should be separated by +
                                signs.
        command (string or method): command to run when the keyboard shortcut is used
        widget (QtGui.QWidget): widget to add shortcut to. If no widget is provided it defaults to the main window
                                of the application

    Return:
        QtGui.Action
    """
    shortcut = GetShortCut(keyCombination, widget)
    shortcut.triggered.connect(executeString)
    shortcut.triggered.disconnect()

    if isinstance(command, basestring):
        command = partial(executeString, command)

    shortcut.triggered.connect(command)


def RemoveShortCut(keyCombination, widget=None):
    """
    Removes the custom keyboard shortcut currently active in Motion Builder

    Arguments:
        keyCombination (string): keyboard shortcut, case is ignored and combination of keys should be separated by +
                                signs.
        widget (QtGui.QWidget): widget to add shortcut to. If no widget is provided it defaults to the main window
                                of the application
    """
    shortcut = GetShortCut(keyCombination, widget)
    shortcut.triggered.connect(executeString)
    shortcut.triggered.disconnect()
    shortcut.deleteLater()


def RemoveAllShortCuts(widget=None):
    """
    Removes the custom keyboard shortcut currently active in Motion Builder

    Arguments:
        keyCombination (string): keyboard shortcut, case is ignored and combination of keys should be separated by +
                                signs.
        widget (QtGui.QWidget): widget to add shortcut to. If no widget is provided it defaults to the main window
                                of the application
    """
    if not widget:
        widget = GetMainWindow()

    for action in widget.actions():
        if action.shortcut():
            action.triggered.disconnect()
            action.deleteLater()


def CloseToolByTitle(title):
    """
    Closes all top level widgets and children widget of the main window based on their title

    Arguments:
        title: (string): title of the window(s) to close
    """
    activeTools = QtGui.qApp.topLevelWidgets() + GetMainWindow().children()
    activeTools = list(set(activeTools))
    for eachTool in activeTools:
        try:
            if isinstance(eachTool, QtBase) and eachTool.windowTitle() == title:
                eachTool.deleteLater()
                if eachTool.isVisible():
                    eachTool.close()
        except:
            pass


def CloseToolByRegexTitle(regexString, caseSensitive=False):
    """
     Closes all top level widgets and children widget of the main window based on their title
     Arguments:
         regexString: (string): regex name of the window(s) to close
    """
    flags = 0
    if caseSensitive:
        flags += re.I

    activeTools = QtGui.qApp.topLevelWidgets()
    activeTools = [child for child in GetMainWindow().children() if child not in activeTools]
    for eachTool in activeTools:
        if not hasattr(eachTool, "windowTitle"):
            continue

        if re.search(regexString, eachTool.windowTitle(), flags) is not None:
            eachTool.deleteLater()
            if eachTool.isVisible():
                eachTool.close()


def isUnderCursor(widget):
    """
    Checks if the mouse is under the given widget

    Arguments:
        widget (QtGui.QWidget): widget to check

    """
    pos = QtGui.QCursor.pos()
    corner = widget.pos()
    size = widget.size()
    isInHorizontalBoundary = corner.x() <= pos.x() <= (corner.x() + size.width())
    isInVerticalBoundary = corner.y() <= pos.y() <= (corner.y() + size.height())
    return isInHorizontalBoundary and isInVerticalBoundary


def GetWinId(widget):
    """
    Gets the widget's id

    Arguments:
        widget (QtGui.QWidget): widget to get id from

    Return:
        int or None
    """
    if platform.system().upper() == "WINDOWS":

        # Get the window id
        pycobject_hwnd = widget.effectiveWinId()

        # Bug in pyside returns a PyC object with no methods to convert into an int
        # This is a work around to convert the PyC object into an int
        ctypes.pythonapi.PyCObject_AsVoidPtr.restype = ctypes.c_void_p
        ctypes.pythonapi.PyCObject_AsVoidPtr.argtypes = [ctypes.py_object]
        return ctypes.pythonapi.PyCObject_AsVoidPtr(pycobject_hwnd)


def BringToFront(widget, force=False):
    """
    Brings the given widget to the front.

    Arguments:
        widget (QtGui.QWidget): widget to bring to front
        force (boolean): Force the widget to the front, this should only be used for bringing a widget in front of
                        other windows applications
    """

    widget.raise_()
    widget.activateWindow()
    widget.showNormal()
    widget.setWindowState(QtCore.Qt.WindowActive)

    if force and platform.system().upper() == "WINDOWS":

        import win32con
        import win32gui

        # Get the window id
        hwnd = GetWinId(widget)

        # mark it "always on top", just for a moment, to force it to the top
        win32gui.SetWindowPos(hwnd, win32con.HWND_TOPMOST, 0, 0, 0, 0, win32con.SWP_NOMOVE | win32con.SWP_NOSIZE| win32con.SWP_SHOWWINDOW)
        win32gui.SetWindowPos(hwnd, win32con.HWND_NOTOPMOST, 0, 0, 0, 0, win32con.SWP_NOMOVE | win32con.SWP_NOSIZE| win32con.SWP_SHOWWINDOW)


def BringMainWindowToFront(force=False):
    """ Brings the main window to the front """
    window = GetMainWindow()
    BringToFront(window, force)


def TriggerOpenRealityAction(menuString):
    # Open Multi Camera Viewer - this need to be done via the Open Reality menu so we can get the
    # tool to launch with maximise and minimise buttons url:bugstar:3395718
    menu = GetMainMenu(filterByMenuNames=("Open Reality",))
    menuDict = GetActionDictionary(menu)
    qAction = menuDict.get("Open Reality", {}).get("Tools", {}).get(menuString, {}).get('__QAction')
    if qAction:
        qAction.trigger()
