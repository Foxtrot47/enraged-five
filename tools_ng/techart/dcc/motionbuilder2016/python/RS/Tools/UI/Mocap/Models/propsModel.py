from PySide import QtCore

from RS import Globals
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, textModelItem
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types import Prop, MocapPropToy

REFERENCE_ROLE = QtCore.Qt.UserRole + 1
GROUP_ROLE = QtCore.Qt.UserRole + 2


class PropTextModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    def __init__(self, referenceObject, referenceGroup, parent=None):
        super(PropTextModelItem, self).__init__([], parent=parent)
        self._referenceObject = referenceObject
        self._referenceGroup = referenceGroup
        self._hidden = not referenceGroup.Show
        self._name = "{}:{}".format(referenceObject.Namespace, referenceObject.Name)

    def getPropObject(self):
        '''
        returns prop reference system object
        '''
        return self._referenceObject

    def getPropGroup(self):
        '''
        returns prop group object
        '''
        return self._referenceGroup

    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()

        if role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
            if column == 0:
                return self._name
            elif column == 1:
                return self._hidden
            
        elif role == REFERENCE_ROLE:
            return self._referenceObject
        
        elif role == GROUP_ROLE:
            return self._referenceGroup
        
        elif role == QtCore.Qt.UserRole:
            return self

        return None


class PropsModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    
    _refTypes = (Prop.Prop, MocapPropToy.MocapPropToy)
    
    def __init__(self, parent=None):
        self._checkable = True
        self._manager = Manager.Manager()
        super(PropsModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ["Prop Name", "Is Hidden"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def _getReferenceItems(self):
        """
        Internal Method
        
        get all the relevant reference system items
        
        returns:
            list of Reference System objects
        """
        return [refItem for refItem in self._manager.GetReferenceListAll() if isinstance(refItem, self._refTypes)]
        
    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        takeName = Globals.System.CurrentTake.Name

        for refItem in self._getReferenceItems():
            refGroup = refItem.getGroup()
            if refGroup is None:
                continue
            if takeName in refGroup.LongName:
                continue
            parent.appendChild(PropTextModelItem(refItem, refGroup, parent=parent))
