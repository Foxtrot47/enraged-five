import os
import webbrowser
import inspect

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.UI
import RS.Utils.Scene
import RS.Utils.Creation as cre
import RS.Config


'''

GTAV Control Rig List       RDR3 Control Rig List

0	Reference				0	Reference
1	Hips					1	Hips
2	LeftUpLeg				2	LeftUpLeg
3	LeftLeg					3	LeftLeg
4	LeftFoot				4	LeftFoot
5	LeftToeBase				5	LeftToeBase
6	RightUpLeg				6	RightUpLeg
7	RightLeg				7	RightLeg
8	RightFoot				8	RightFoot
9	RightToeBase			9	RightToeBase
10	Spine					10	Spine
11	Spine1					11	Spine1
12	Spine2					12	Spine2
13	Spine3					13	Spine3
14	LeftShoulder			14	Spine4
15	LeftArm					15	Spine5
16	LeftForeArm				16	Spine6
17	LeftHand				17	LeftShoulder
18	LeftHandThumb1			18	LeftArm
19	LeftHandThumb2			19	LeftForeArm
20	LeftHandThumb3			20	LeftHand
21	LeftHandThumb4			21	LeftHandThumb1
22	LeftHandIndex1			22	LeftHandThumb2
23	LeftHandIndex2			23	LeftHandThumb3
24	LeftHandIndex3			24	LeftHandThumb4
25	LeftHandIndex4			25	LeftInHandIndex
26	LeftHandMiddle1			26	LeftHandIndex1
27	LeftHandMiddle2			27	LeftHandIndex2
28	LeftHandMiddle3			28	LeftHandIndex3
29	LeftHandMiddle4			29	LeftHandIndex4
30	LeftHandRing1			30	LeftInHandMiddle
31	LeftHandRing2			31	LeftHandMiddle1
32	LeftHandRing3			32	LeftHandMiddle2
33	LeftHandRing4			33	LeftHandMiddle3
34	LeftHandPinky1			34	LeftHandMiddle4
35	LeftHandPinky2			35	LeftInHandRing
36	LeftHandPinky3			36	LeftHandRing1
37	LeftHandPinky4			37	LeftHandRing2
38	RightShoulder			38	LeftHandRing3
39	RightArm				39	LeftHandRing4
40	RightForeArm			40	LeftInHandPinky
41	RightHand				41	LeftHandPinky1
42	RightHandThumb1			42	LeftHandPinky2
43	RightHandThumb2			43	LeftHandPinky3
44	RightHandThumb3			44	LeftHandPinky4
45	RightHandThumb4			45	RightShoulder
46	RightHandIndex1			46	RightArm
47	RightHandIndex2			47	RightForeArm
48	RightHandIndex3			48	RightHand
49	RightHandIndex4			49	RightHandThumb1
50	RightHandMiddle1		50	RightHandThumb2
51	RightHandMiddle2		51	RightHandThumb3
52	RightHandMiddle3		52	RightHandThumb4
53	RightHandMiddle4		53	RightInHandIndex
54	RightHandRing1			54	RightHandIndex1
55	RightHandRing2			55	RightHandIndex2
56	RightHandRing3			56	RightHandIndex3
57	RightHandRing4			57	RightHandIndex4
58	RightHandPinky1			58	RightInHandMiddle
59	RightHandPinky2			59	RightHandMiddle1
60	RightHandPinky3			60	RightHandMiddle2
61	RightHandPinky4			61	RightHandMiddle3
62	Neck					62	RightHandMiddle4
63	Head					63	RightInHandRing
64	HipsEffector			64	RightHandRing1
65	LeftAnkleEffector		65	RightHandRing2
66	RightAnkleEffector		66	RightHandRing3
67	LeftWristEffector		67	RightHandRing4
68	RightWristEffector		68	RightInHandPinky
69	LeftKneeEffector		69	RightHandPinky1
70	RightKneeEffector		70	RightHandPinky2
71	LeftElbowEffector		71	RightHandPinky3
72	RightElbowEffector		72	RightHandPinky4
73	ChestOriginEffector		73	Neck
74	ChestEndEffector		74	Neck1
75	LeftFootEffector		75	Neck2
76	RightFootEffector		76	Head
77	LeftShoulderEffector	77	HipsEffector
78	RightShoulderEffector	78	LeftAnkleEffector
79	HeadEffector			79	RightAnkleEffector
80	LeftHipEffector			80	LeftWristEffector
81	RightHipEffector		81	RightWristEffector
82	LeftHandThumbEffector	82	LeftKneeEffector
83	LeftHandIndexEffector	83	RightKneeEffector
84	LeftHandMiddleEffector	84	LeftElbowEffector
85	LeftHandRingEffector	85	RightElbowEffector
86	LeftHandPinkyEffector	86	ChestOriginEffector
87	RightHandThumbEffector	87	ChestEndEffector
88	RightHandIndexEffector	88	LeftFootEffector
89	RightHandMiddleEffector	89	RightFootEffector
90	RightHandRingEffector	90	LeftShoulderEffector
91	RightHandPinkyEffector	91	RightShoulderEffector
							92	HeadEffector
							93	LeftHipEffector
							94	RightHipEffector
							95	LeftHandThumbEffector
							96	LeftHandIndexEffector
							97	LeftHandMiddleEffector
							98	LeftHandRingEffector
							99	LeftHandPinkyEffector
							100	RightHandThumbEffector
							101	RightHandIndexEffector
							102	RightHandMiddleEffector
							103	RightHandRingEffector
							104	RightHandPinkyEffector

'''

project = str( RS.Config.Project.Name ).lower()

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: CreateSelectionSetGroup
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def CreateSelectionSetGroup( namespace, buttonType ):
    
    group = None
    mainGroup = None
    found = False

    for i in RS.Globals.Groups:

        if i.LongName == namespace + ':Control_SelectionSet':
            mainGroup = i
            
        if i.LongName == namespace + ':' + buttonType + '_SelectionSet':
            found = True
            
    if found == False:
        
        group = FBGroup( namespace + ':' + buttonType + '_SelectionSet' )
        tag = group.PropertyCreate( 'fk/ik selection set', FBPropertyType.kFBPT_charptr, "", False, True, None )
        tag.Data = buttonType
        
        if mainGroup:
            mainGroup.ConnectSrc( group )
            
        else:
            mainGroup = FBGroup( namespace + ':Control_SelectionSet' )
            tag = group.PropertyCreate( 'fk/ik selection set', FBPropertyType.kFBPT_charptr, "", False, True, None )
            tag.Data = 'main'
            mainGroup.ConnectSrc( group )
                    
    else:
        FBMessageBox( 'R*', '{0} Group exists already'.format( buttonType ), 'Ok' )
             
    return group
   


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_CreateRadioButton
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CreateRadioButton(pButton, pLeft, pTop, pContainer, pContainerFollow, pListIndex):

    lRadioButton = FBButton()
    lRadioButton.Hint = pButton
    lRadioButton.Name = pListIndex
    lRadioButton.Style = FBButtonStyle.kFBRadioButton
    lRadioButtonX = FBAddRegionParam(pLeft,FBAttachType.kFBAttachLeft,"")
    lRadioButtonY = FBAddRegionParam(pTop,FBAttachType.kFBAttachTop, pContainerFollow)
    lRadioButtonW = FBAddRegionParam(13,FBAttachType.kFBAttachNone,"")
    lRadioButtonH = FBAddRegionParam(13,FBAttachType.kFBAttachNone,"")
    pContainer.AddRegion(pButton, pButton, lRadioButtonX, lRadioButtonY, lRadioButtonW, lRadioButtonH)
    pContainer.SetControl(pButton, lRadioButton)

    return lRadioButton


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_CreateLabel
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CreateLabel(pLabel, pLeft, pTop, pHeight, pWidth, pContainer, pContainerFollow):

    lLabel = FBLabel()
    lLabelX = FBAddRegionParam(pLeft,FBAttachType.kFBAttachLeft,pContainerFollow)
    lLabelY = FBAddRegionParam(pTop,FBAttachType.kFBAttachTop,"")
    lLabelW = FBAddRegionParam(pWidth,FBAttachType.kFBAttachNone,"")
    lLabelH = FBAddRegionParam(pHeight,FBAttachType.kFBAttachNone,"")
    pContainer.AddRegion(pLabel, pLabel, lLabelX, lLabelY, lLabelW, lLabelH)
    pContainer.SetControl(pLabel, lLabel)

    return lLabel


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_CheckCharacter
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CheckCharacter():

    lCurrentCharacter = FBApplication().CurrentCharacter

    if lCurrentCharacter:

        lCtrlHips = lCurrentCharacter.GetCtrlRigModel(FBBodyNodeId.kFBHipsNodeId)
        lRootRefNull = RS.Utils.Scene.GetParent(lCtrlHips)

        if lRootRefNull == None:
            lBool = False

        else:
            lBool = True 

    else:
        lBool = False

    return lBool


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_GetCurrentCharacterNamespace
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_GetCurrentCharacterNamespace(pCurrentCharacter):

    lSplit = pCurrentCharacter.LongName.split(":")
    lNewNamespace = lSplit[0]

    return lNewNamespace


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_KeyingMode
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_KeyingMode(pKeyingMode1, pKeyingMode2, pKeyingMode3):

    lCurrentCharacter = FBApplication().CurrentCharacter

    if pKeyingMode1.State == 1:
        lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingSelection
        lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingFullBody 
    if pKeyingMode2.State == 1:
        lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingSelection
        lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingBodyPart  
    if pKeyingMode3.State == 1:
        lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingBodyPart
        lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingSelection


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Non-Control Rig Callbacks
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

def rs_lMoverButtonCallback(pControl, pEvent):
    if rs_CheckCharacter() == True:
        lCurrentCharacter = FBApplication().CurrentCharacter
        mover = FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":mover")
        if mover:
            mover.Selected = True
        else:
            None
            pControl.State = 0
    else:
        lMover.State = 0
        FBMessageBox('Error', "Cannot find current character's control rig", 'Ok')            


def rs_lLPHHandButtonCallback(pControl, pEvent):
    if rs_CheckCharacter() == True:
        lCurrentCharacter = FBApplication().CurrentCharacter
        lHand = FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":PH_L_Hand")
        if lHand:
            lHand.Selected = True
        else:
            None
            pControl.State = 0
    else:
        lLPHHand.State = 0
        FBMessageBox('Error', "Cannot find current character's control rig", 'Ok')            


def rs_lRPHHandButtonCallback(pControl, pEvent):
    if rs_CheckCharacter() == True:
        lCurrentCharacter = FBApplication().CurrentCharacter
        rHand = FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":PH_R_Hand")
        if rHand:
            rHand.Selected = True
        else:
            None
            pControl.State = 0
    else:
        lRPHHand.State = 0
        FBMessageBox('Error', "Cannot find current character's control rig", 'Ok')                


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: UI Layout
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

class FKControlsTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "FK Controls - {0}".format( project ), size = [ 480, 535 ] )
        
        print project

    def Create( self, pMain ):
        lTabControl = FBTabControl()
        lTabControlX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lTabControlY = FBAddRegionParam(self.BannerHeight,FBAttachType.kFBAttachTop,"")
        lTabControlW = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        lTabControlH = FBAddRegionParam(-5,FBAttachType.kFBAttachBottom,"")
        pMain.AddRegion("lTabControl", "lTabControl",lTabControlX,lTabControlY,lTabControlW,lTabControlH)
        pMain.SetControl("lTabControl", lTabControl)
        

        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Body Tab
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################

        lFKBodyTabContainer = FBLayout()                
        lFKBodyTabContainerX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lFKBodyTabContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        lFKBodyTabContainerW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lFKBodyTabContainerH = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        lFKBodyTabContainer.AddRegion("lFKBodyTabContainer","lFKBodyTabContainer", lFKBodyTabContainerX, lFKBodyTabContainerY, lFKBodyTabContainerW, lFKBodyTabContainerH)
        lTabControl.Add("Body Controls",lFKBodyTabContainer)

        lFKBodyImageContainer = FBVisualContainer()
        lFKBodyImageContainerX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lFKBodyImageContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        lFKBodyImageContainerW = FBAddRegionParam(452,FBAttachType.kFBAttachNone,"")
        lFKBodyImageContainerH = FBAddRegionParam(440,FBAttachType.kFBAttachNone,"")
        lFKBodyTabContainer.AddRegion("lFKBodyImageContainer","lFKBodyImage", lFKBodyImageContainerX, lFKBodyImageContainerY, lFKBodyImageContainerW, lFKBodyImageContainerH)
        lFKBodyTabContainer.SetControl("lFKBodyImageContainer",lFKBodyImageContainer)

        lFKBodyImage = "{0}\\FK_Controls_Body.tif".format( RS.Config.Script.Path.ToolImages )
        lFKBodyImageContainer.Items.append("")
        lFKBodyImageContainer.ItemIconSet(0, lFKBodyImage)
        lFKBodyImageContainer.ItemWidth = 450
        lFKBodyImageContainer.ItemHeight = 455
        lFKBodyImageContainer.ItemWrap = True

        characterSelectDropDown = FBList()
        characterSelectDropDown.Style = FBListStyle.kFBDropDownList
        characterSelectDropDown.Hint = ''
        dropDownX = FBAddRegionParam( 5, FBAttachType.kFBAttachLeft, "" )
        dropDownY = FBAddRegionParam( 5, FBAttachType.kFBAttachTop, "lFKBodyImageContainer" )
        dropDownW = FBAddRegionParam( 180, FBAttachType.kFBAttachNone,"" )
        dropDownH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone,"" )            
        lFKBodyTabContainer.AddRegion( 'characterSelectDropDown', 'characterSelectDropDown', dropDownX, dropDownY, dropDownW, dropDownH )
        lFKBodyTabContainer.SetControl( 'characterSelectDropDown', characterSelectDropDown )     

        characterList = []     
        for i in RS.Globals.gCharacters:
            characterList.append( i )

        for i in characterList:
            characterSelectDropDown.Items.append( i.LongName )
           
        lCurrentCharacter = FBApplication().CurrentCharacter 
        for i in range( len( characterList ) ):
            if characterList[i].LongName == lCurrentCharacter.LongName:
                characterSelectDropDown.Selected( i, True )
            
        keyModeDropDown = FBList()
        keyModeDropDown.Style = FBListStyle.kFBDropDownList
        keyModeDropDown.Hint = ''
        dropDownX = FBAddRegionParam( 5, FBAttachType.kFBAttachLeft, "" )
        dropDownY = FBAddRegionParam( 5, FBAttachType.kFBAttachBottom, "characterSelectDropDown" )
        dropDownW = FBAddRegionParam( 180, FBAttachType.kFBAttachNone,"" )
        dropDownH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone,"" )            
        lFKBodyTabContainer.AddRegion( 'keyModeDropDown', 'keyModeDropDown', dropDownX, dropDownY, dropDownW, dropDownH )
        lFKBodyTabContainer.SetControl( 'keyModeDropDown', keyModeDropDown )     

        keyModeDropDown.Items.append( 'KeyMode: Full' )
        keyModeDropDown.Items.append( 'KeyMode: Body Part' )
        keyModeDropDown.Items.append( 'KeyMode: Selection' )
                
        if len( characterList ) <= 0:
            keyModeDropDown.Enabled = False

        if project == 'gta5': 
            lHead = rs_CreateRadioButton("Head", 224,23, lFKBodyTabContainer, "lFKBodyImageContainer", '63')
            lNeck = rs_CreateRadioButton("Neck", 224,85, lFKBodyTabContainer, "lFKBodyImageContainer", '62')
            lRShoulder = rs_CreateRadioButton("RightShoulder", 180,85, lFKBodyTabContainer, "lFKBodyImageContainer", '38') 
            lLShoulder = rs_CreateRadioButton("LeftShoulder", 270,85, lFKBodyTabContainer, "lFKBodyImageContainer", '14')   
            lRArm = rs_CreateRadioButton("RightArm", 150,85, lFKBodyTabContainer, "lFKBodyImageContainer", '39') 
            lLArm = rs_CreateRadioButton("LeftArm", 300,85, lFKBodyTabContainer, "lFKBodyImageContainer", '15')
            lRForeArm = rs_CreateRadioButton("RightForeArm", 115,85, lFKBodyTabContainer, "lFKBodyImageContainer", '40') 
            lLForeArm = rs_CreateRadioButton("LeftForeArm", 335,85, lFKBodyTabContainer, "lFKBodyImageContainer", '16')
            lRHand = rs_CreateRadioButton("RightHand", 80,85, lFKBodyTabContainer, "lFKBodyImageContainer", '41') 
            lLHand = rs_CreateRadioButton("LeftHand", 370,85, lFKBodyTabContainer, "lFKBodyImageContainer", '17')
            lRPHHand = rs_CreateRadioButton("PH_R_Hand", 80,110, lFKBodyTabContainer, "lFKBodyImageContainer", 'None') 
            lLPHHand = rs_CreateRadioButton("PH_L_Hand", 370,110, lFKBodyTabContainer, "lFKBodyImageContainer", 'None')   
            lSpine3 = rs_CreateRadioButton("Spine3", 224,155, lFKBodyTabContainer, "lFKBodyImageContainer", '13')      
            lSpine2 = rs_CreateRadioButton("Spine2", 224,170, lFKBodyTabContainer, "lFKBodyImageContainer", '12')      
            lSpine1 = rs_CreateRadioButton("Spine1", 224,185, lFKBodyTabContainer, "lFKBodyImageContainer", '11')      
            lSpine0 = rs_CreateRadioButton("Spine", 224,200, lFKBodyTabContainer, "lFKBodyImageContainer", '10')      
            lHips = rs_CreateRadioButton("Hips", 224,210, lFKBodyTabContainer, "lFKBodyImageContainer", '1')  
            lRUpperLeg = rs_CreateRadioButton("RightUpLeg", 201,235, lFKBodyTabContainer, "lFKBodyImageContainer", '6')        
            lLUpperLeg = rs_CreateRadioButton("LeftUpLeg", 247,235, lFKBodyTabContainer, "lFKBodyImageContainer", '2')  
            lRLeg = rs_CreateRadioButton("RightLeg", 197,320, lFKBodyTabContainer, "lFKBodyImageContainer", '7')        
            lLLeg = rs_CreateRadioButton("LeftLeg", 252,320, lFKBodyTabContainer, "lFKBodyImageContainer", '3') 
            lRFoot = rs_CreateRadioButton("RightFoot", 195,380, lFKBodyTabContainer, "lFKBodyImageContainer", '8')        
            lLFoot = rs_CreateRadioButton("LeftFoot", 255,380, lFKBodyTabContainer, "lFKBodyImageContainer", '4') 
            lRToe = rs_CreateRadioButton("RightToeBase", 185,400, lFKBodyTabContainer, "lFKBodyImageContainer", '9')        
            lLToe = rs_CreateRadioButton("LeftToeBase", 264,400, lFKBodyTabContainer, "lFKBodyImageContainer", '8') 
            lMover = rs_CreateRadioButton("mover", 22,419, lFKBodyTabContainer, "lFKBodyImageContainer", 'None') 
        
            lBodyRadioButtonList = [lHead, lNeck, lRShoulder, lLShoulder, lRArm, lLArm, lRForeArm, lLForeArm, lRHand, lLHand,
                                    lSpine3, lSpine2, lSpine1, lSpine0, lHips, lRUpperLeg, lLUpperLeg, lRLeg, lLLeg,
                                    lRFoot, lLFoot, lRToe, lLToe]        
        
        else:
            lHead = rs_CreateRadioButton("Head", 224,23, lFKBodyTabContainer, "lFKBodyImageContainer", '76')
            lNeck = rs_CreateRadioButton("Neck", 224,85, lFKBodyTabContainer, "lFKBodyImageContainer", '73')
            lNeck1 = rs_CreateRadioButton("Neck1", 224,71, lFKBodyTabContainer, "lFKBodyImageContainer", '74')
            lNeck2 = rs_CreateRadioButton("Neck2", 224,58, lFKBodyTabContainer, "lFKBodyImageContainer", '75')
            lRShoulder = rs_CreateRadioButton("RightShoulder", 180,85, lFKBodyTabContainer, "lFKBodyImageContainer", '45') 
            lLShoulder = rs_CreateRadioButton("LeftShoulder", 270,85, lFKBodyTabContainer, "lFKBodyImageContainer", '17')   
            lRArm = rs_CreateRadioButton("RightArm", 150,85, lFKBodyTabContainer, "lFKBodyImageContainer", '46') 
            lLArm = rs_CreateRadioButton("LeftArm", 300,85, lFKBodyTabContainer, "lFKBodyImageContainer", '18')
            lRForeArm = rs_CreateRadioButton("RightForeArm", 115,85, lFKBodyTabContainer, "lFKBodyImageContainer", '47') 
            lLForeArm = rs_CreateRadioButton("LeftForeArm", 335,85, lFKBodyTabContainer, "lFKBodyImageContainer", '19')
            lRHand = rs_CreateRadioButton("RightHand", 80,85, lFKBodyTabContainer, "lFKBodyImageContainer", '48') 
            lLHand = rs_CreateRadioButton("LeftHand", 370,85, lFKBodyTabContainer, "lFKBodyImageContainer", '20')
            lRPHHand = rs_CreateRadioButton("PH_R_Hand", 80,110, lFKBodyTabContainer, "lFKBodyImageContainer", 'None') 
            lLPHHand = rs_CreateRadioButton("PH_L_Hand", 370,110, lFKBodyTabContainer, "lFKBodyImageContainer", 'None')   
            lSpine6 = rs_CreateRadioButton("Spine6", 224,110, lFKBodyTabContainer, "lFKBodyImageContainer", '16')      
            lSpine5 = rs_CreateRadioButton("Spine5", 224,125, lFKBodyTabContainer, "lFKBodyImageContainer", '15')      
            lSpine4 = rs_CreateRadioButton("Spine4", 224,140, lFKBodyTabContainer, "lFKBodyImageContainer", '14')      
            lSpine3 = rs_CreateRadioButton("Spine3", 224,155, lFKBodyTabContainer, "lFKBodyImageContainer", '13')      
            lSpine2 = rs_CreateRadioButton("Spine2", 224,170, lFKBodyTabContainer, "lFKBodyImageContainer", '12')      
            lSpine1 = rs_CreateRadioButton("Spine1", 224,185, lFKBodyTabContainer, "lFKBodyImageContainer", '11')      
            lSpine0 = rs_CreateRadioButton("Spine", 224,200, lFKBodyTabContainer, "lFKBodyImageContainer", '10')      
            lHips = rs_CreateRadioButton("Hips", 224,210, lFKBodyTabContainer, "lFKBodyImageContainer", '1')  
            lRUpperLeg = rs_CreateRadioButton("RightUpLeg", 201,235, lFKBodyTabContainer, "lFKBodyImageContainer", '6')        
            lLUpperLeg = rs_CreateRadioButton("LeftUpLeg", 247,235, lFKBodyTabContainer, "lFKBodyImageContainer", '2')  
            lRLeg = rs_CreateRadioButton("RightLeg", 197,320, lFKBodyTabContainer, "lFKBodyImageContainer", '7')        
            lLLeg = rs_CreateRadioButton("LeftLeg", 252,320, lFKBodyTabContainer, "lFKBodyImageContainer", '3') 
            lRFoot = rs_CreateRadioButton("RightFoot", 195,380, lFKBodyTabContainer, "lFKBodyImageContainer", '8')        
            lLFoot = rs_CreateRadioButton("LeftFoot", 255,380, lFKBodyTabContainer, "lFKBodyImageContainer", '4') 
            lRToe = rs_CreateRadioButton("RightToeBase", 185,400, lFKBodyTabContainer, "lFKBodyImageContainer", '9')        
            lLToe = rs_CreateRadioButton("LeftToeBase", 264,400, lFKBodyTabContainer, "lFKBodyImageContainer", '8') 
            lMover = rs_CreateRadioButton("mover", 22,419, lFKBodyTabContainer, "lFKBodyImageContainer", 'None') 

            lBodyRadioButtonList = [lHead, lNeck, lNeck1, lNeck2, lRShoulder, lLShoulder, lRArm, lLArm, lRForeArm, lLForeArm, lRHand, lLHand,
                                    lSpine6, lSpine5, lSpine4, lSpine3, lSpine2, lSpine1, lSpine0, lHips, lRUpperLeg, lLUpperLeg, lRLeg, lLLeg,
                                    lRFoot, lLFoot, lRToe, lLToe]

        lMover.OnClick.Add(rs_lMoverButtonCallback)
        lLPHHand.OnClick.Add(rs_lLPHHandButtonCallback)
        lRPHHand.OnClick.Add(rs_lRPHHandButtonCallback)         


        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Hand Tab
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################

        lFKHandTabContainer = FBLayout()                
        lFKHandTabContainerX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lFKHandTabContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        lFKHandTabContainerW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lFKHandTabContainerH = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        lFKHandTabContainer.AddRegion("lFKHandTabContainer","lFKHandTabContainer", lFKHandTabContainerX, lFKHandTabContainerY, lFKHandTabContainerW, lFKHandTabContainerH)
        lTabControl.Add("Hand Controls",lFKHandTabContainer)

        lFKHandImageContainer = FBVisualContainer()
        lFKHandImageContainerX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lFKHandImageContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        lFKHandImageContainerW = FBAddRegionParam(452,FBAttachType.kFBAttachNone,"")
        lFKHandImageContainerH = FBAddRegionParam(440,FBAttachType.kFBAttachNone,"")
        lFKHandTabContainer.AddRegion("lFKHandImageContainer","lFKHandImage", lFKHandImageContainerX, lFKHandImageContainerY, lFKHandImageContainerW, lFKHandImageContainerH)
        lFKHandTabContainer.SetControl("lFKHandImageContainer",lFKHandImageContainer)

        lFKHandImage = "{0}\\FK_Controls_Hand.tif".format( RS.Config.Script.Path.ToolImages )
        lFKHandImageContainer.Items.append("")
        lFKHandImageContainer.ItemIconSet(0, lFKHandImage)
        lFKHandImageContainer.ItemWidth = 450
        lFKHandImageContainer.ItemHeight = 455
        lFKHandImageContainer.ItemWrap = True

        characterSelectDropDown2 = FBList()
        characterSelectDropDown2.Style = FBListStyle.kFBDropDownList
        characterSelectDropDown2.Hint = ''
        dropDownX = FBAddRegionParam( 5, FBAttachType.kFBAttachLeft, "" )
        dropDownY = FBAddRegionParam( 5, FBAttachType.kFBAttachTop, "lFKBodyImageContainer" )
        dropDownW = FBAddRegionParam( 180, FBAttachType.kFBAttachNone,"" )
        dropDownH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone,"" )            
        lFKHandTabContainer.AddRegion( 'characterSelectDropDown2', 'characterSelectDropDown2', dropDownX, dropDownY, dropDownW, dropDownH )
        lFKHandTabContainer.SetControl( 'characterSelectDropDown2', characterSelectDropDown2 )     

        characterList = []     
        for i in RS.Globals.gCharacters:
            characterList.append( i )

        for i in characterList:
            characterSelectDropDown2.Items.append( i.LongName )
           
        lCurrentCharacter = FBApplication().CurrentCharacter 
        for i in range( len( characterList ) ):
            if characterList[i].LongName == lCurrentCharacter.LongName:
                characterSelectDropDown2.Selected( i, True )

        keyModeDropDown2 = FBList()
        keyModeDropDown2.Style = FBListStyle.kFBDropDownList
        keyModeDropDown2.Hint = ''
        dropDownX = FBAddRegionParam( 5, FBAttachType.kFBAttachLeft, "" )
        dropDownY = FBAddRegionParam( 5, FBAttachType.kFBAttachBottom, "characterSelectDropDown2" )
        dropDownW = FBAddRegionParam( 180, FBAttachType.kFBAttachNone,"" )
        dropDownH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone,"" )            
        lFKHandTabContainer.AddRegion( 'keyModeDropDown2', 'keyModeDropDown2', dropDownX, dropDownY, dropDownW, dropDownH )
        lFKHandTabContainer.SetControl( 'keyModeDropDown2', keyModeDropDown2 )     

        keyModeDropDown2.Items.append( 'KeyMode: Full' )
        keyModeDropDown2.Items.append( 'KeyMode: Body Part' )
        keyModeDropDown2.Items.append( 'KeyMode: Selection' )
        
        if len( characterList ) <= 0:
            keyModeDropDown2.Enabled = False

        if project == 'gta5': 
            lLHand2 = rs_CreateRadioButton("LeftHand", 120,330, lFKHandTabContainer, "lFKHandImageContainer", '17')    
            lRHand2 = rs_CreateRadioButton("RightHand", 335,330, lFKHandTabContainer, "lFKHandImageContainer", '41')
            lRThumb1 = rs_CreateRadioButton("RightHandThumb1", 280,260, lFKHandTabContainer, "lFKHandImageContainer", '42')
            lRThumb2 = rs_CreateRadioButton("RightHandThumb2", 263,230, lFKHandTabContainer, "lFKHandImageContainer", '43')
            lRThumb3 = rs_CreateRadioButton("RightHandThumb3", 250,200, lFKHandTabContainer, "lFKHandImageContainer", '44')
            lRThumb4 = rs_CreateRadioButton("RightHandThumb4", 242,170, lFKHandTabContainer, "lFKHandImageContainer", '45')        
            lRIndex1 = rs_CreateRadioButton("RightHandIndex1", 292,174, lFKHandTabContainer, "lFKHandImageContainer", '46')
            lRIndex2 = rs_CreateRadioButton("RightHandIndex2", 282,144, lFKHandTabContainer, "lFKHandImageContainer", '47')
            lRIndex3 = rs_CreateRadioButton("RightHandIndex3", 274,114, lFKHandTabContainer, "lFKHandImageContainer", '48')    
            lRIndex4 = rs_CreateRadioButton("RightHandIndex4", 265,84, lFKHandTabContainer, "lFKHandImageContainer", '49')            
            lRMiddle1 = rs_CreateRadioButton("RightHandMiddle1", 319,150, lFKHandTabContainer, "lFKHandImageContainer", '50')
            lRMiddle2 = rs_CreateRadioButton("RightHandMiddle2", 317,120, lFKHandTabContainer, "lFKHandImageContainer", '51')
            lRMiddle3 = rs_CreateRadioButton("RightHandMiddle3", 315,90, lFKHandTabContainer, "lFKHandImageContainer", '52')         
            lRMiddle4 = rs_CreateRadioButton("RightHandMiddle4", 312,60, lFKHandTabContainer, "lFKHandImageContainer", '53')   
            lRRing1 = rs_CreateRadioButton("RightHandRing1", 347,165, lFKHandTabContainer, "lFKHandImageContainer", '54')
            lRRing2 = rs_CreateRadioButton("RightHandRing2", 351,135, lFKHandTabContainer, "lFKHandImageContainer", '55')
            lRRing3 = rs_CreateRadioButton("RightHandRing3", 355,105, lFKHandTabContainer, "lFKHandImageContainer", '56') 
            lRRing4 = rs_CreateRadioButton("RightHandRing4", 360,75, lFKHandTabContainer, "lFKHandImageContainer", '57') 
            lRPinky1 = rs_CreateRadioButton("RightHandPinky1", 372,193, lFKHandTabContainer, "lFKHandImageContainer", '58')
            lRPinky2 = rs_CreateRadioButton("RightHandPinky2", 377,163, lFKHandTabContainer, "lFKHandImageContainer", '59')
            lRPinky3 = rs_CreateRadioButton("RightHandPinky3", 384,133, lFKHandTabContainer, "lFKHandImageContainer", '60') 
            lRPinky4 = rs_CreateRadioButton("RightHandPinky4", 392,103, lFKHandTabContainer, "lFKHandImageContainer", '61')  
            lLThumb1 = rs_CreateRadioButton("LeftHandThumb1", 175,260, lFKHandTabContainer, "lFKHandImageContainer", '18')
            lLThumb2 = rs_CreateRadioButton("LeftHandThumb2", 190,230, lFKHandTabContainer, "lFKHandImageContainer", '19')
            lLThumb3 = rs_CreateRadioButton("LeftHandThumb3", 200,200, lFKHandTabContainer, "lFKHandImageContainer", '20')
            lLThumb4 = rs_CreateRadioButton("LeftHandThumb4", 215,170, lFKHandTabContainer, "lFKHandImageContainer", '21')        
            lLIndex1 = rs_CreateRadioButton("LeftHandIndex1", 163,175, lFKHandTabContainer, "lFKHandImageContainer", '22')
            lLIndex2 = rs_CreateRadioButton("LeftHandIndex2", 173,145, lFKHandTabContainer, "lFKHandImageContainer", '23')
            lLIndex3 = rs_CreateRadioButton("LeftHandIndex3", 183,115, lFKHandTabContainer, "lFKHandImageContainer", '24')    
            lLIndex4 = rs_CreateRadioButton("LeftHandIndex4", 192,85, lFKHandTabContainer, "lFKHandImageContainer", '25')            
            lLMiddle1 = rs_CreateRadioButton("LeftHandMiddle1", 136,150, lFKHandTabContainer, "lFKHandImageContainer", '26')
            lLMiddle2 = rs_CreateRadioButton("LeftHandMiddle2", 140,120, lFKHandTabContainer, "lFKHandImageContainer", '27')
            lLMiddle3 = rs_CreateRadioButton("LeftHandMiddle3", 142,90, lFKHandTabContainer, "lFKHandImageContainer", '28')         
            lLMiddle4 = rs_CreateRadioButton("LeftHandMiddle4", 144,60, lFKHandTabContainer, "lFKHandImageContainer'", '29')   
            lLRing1 = rs_CreateRadioButton("LeftHandRing1", 108,174, lFKHandTabContainer, "lFKHandImageContainer", '30')
            lLRing2 = rs_CreateRadioButton("LeftHandRing2", 105,144, lFKHandTabContainer, "lFKHandImageContainer", '31')
            lLRing3 = rs_CreateRadioButton("LeftHandRing3", 102,114, lFKHandTabContainer, "lFKHandImageContainer", '32') 
            lLRing4 = rs_CreateRadioButton("LeftHandRing4", 96,75, lFKHandTabContainer, "lFKHandImageContainer", '33') 
            lLPinky1 = rs_CreateRadioButton("LeftHandPinky1", 85,193, lFKHandTabContainer, "lFKHandImageContainer", '34')
            lLPinky2 = rs_CreateRadioButton("LeftHandPinky2", 80,163, lFKHandTabContainer, "lFKHandImageContainer", '35')
            lLPinky3 = rs_CreateRadioButton("LeftHandPinky3", 72,133, lFKHandTabContainer, "lFKHandImageContainer", '36') 
            lLPinky4 = rs_CreateRadioButton("LeftHandPinky4", 64,103, lFKHandTabContainer, "lFKHandImageContainer", '37') 
            
            lHandRadioButtonList = [lLHand2, lRHand2, lRThumb1, lRThumb2, lRThumb3, lRThumb4, lRIndex1, lRIndex2, lRIndex3, lRIndex4, 
                                    lRMiddle1, lRMiddle2, lRMiddle3, lRMiddle4, lRRing1, lRRing2, lRRing3, lRRing4, 
                                    lRPinky1, lRPinky2, lRPinky3, lRPinky4, lLThumb1, lLThumb2, lLThumb3, lLThumb4, lLIndex1, lLIndex2, 
                                    lLIndex3, lLIndex4, lLMiddle1, lLMiddle2, lLMiddle3, lLMiddle4, lLRing1, lLRing2, lLRing3, 
                                    lLRing4, lLPinky1, lLPinky2, lLPinky3, lLPinky4]
 
        else:
            lLHand2 = rs_CreateRadioButton("LeftHand", 120,330, lFKHandTabContainer, "lFKHandImageContainer", '20')    
            lRHand2 = rs_CreateRadioButton("RightHand", 335,330, lFKHandTabContainer, "lFKHandImageContainer", '48')
            lRThumb1 = rs_CreateRadioButton("RightHandThumb1", 280,260, lFKHandTabContainer, "lFKHandImageContainer", '49')
            lRThumb2 = rs_CreateRadioButton("RightHandThumb2", 263,230, lFKHandTabContainer, "lFKHandImageContainer", '50')
            lRThumb3 = rs_CreateRadioButton("RightHandThumb3", 250,200, lFKHandTabContainer, "lFKHandImageContainer", '51')
            lRThumb4 = rs_CreateRadioButton("RightHandThumb4", 242,170, lFKHandTabContainer, "lFKHandImageContainer", '52')        
            lRIndex = rs_CreateRadioButton("RightInHandIndex", 302,204, lFKHandTabContainer, "lFKHandImageContainer", '53')
            lRIndex1 = rs_CreateRadioButton("RightHandIndex1", 292,174, lFKHandTabContainer, "lFKHandImageContainer", '54')
            lRIndex2 = rs_CreateRadioButton("RightHandIndex2", 282,144, lFKHandTabContainer, "lFKHandImageContainer", '55')
            lRIndex3 = rs_CreateRadioButton("RightHandIndex3", 274,114, lFKHandTabContainer, "lFKHandImageContainer", '56')    
            lRIndex4 = rs_CreateRadioButton("RightHandIndex4", 265,84, lFKHandTabContainer, "lFKHandImageContainer", '57')            
            lRMiddle = rs_CreateRadioButton("RightInHandMiddle", 322,180, lFKHandTabContainer, "lFKHandImageContainer", '58')
            lRMiddle1 = rs_CreateRadioButton("RightHandMiddle1", 319,150, lFKHandTabContainer, "lFKHandImageContainer", '59')
            lRMiddle2 = rs_CreateRadioButton("RightHandMiddle2", 317,120, lFKHandTabContainer, "lFKHandImageContainer", '60')
            lRMiddle3 = rs_CreateRadioButton("RightHandMiddle3", 315,90, lFKHandTabContainer, "lFKHandImageContainer", '61')         
            lRMiddle4 = rs_CreateRadioButton("RightHandMiddle4", 312,60, lFKHandTabContainer, "lFKHandImageContainer", '62')   
            lRRing = rs_CreateRadioButton("RightInHandRing", 345,195, lFKHandTabContainer, "lFKHandImageContainer", '63')        
            lRRing1 = rs_CreateRadioButton("RightHandRing1", 347,165, lFKHandTabContainer, "lFKHandImageContainer", '64')
            lRRing2 = rs_CreateRadioButton("RightHandRing2", 351,135, lFKHandTabContainer, "lFKHandImageContainer", '65')
            lRRing3 = rs_CreateRadioButton("RightHandRing3", 355,105, lFKHandTabContainer, "lFKHandImageContainer", '66') 
            lRRing4 = rs_CreateRadioButton("RightHandRing4", 360,75, lFKHandTabContainer, "lFKHandImageContainer", '67') 
            lRPinky = rs_CreateRadioButton("RightInHandPinky", 370,223, lFKHandTabContainer, "lFKHandImageContainer", '68')        
            lRPinky1 = rs_CreateRadioButton("RightHandPinky1", 372,193, lFKHandTabContainer, "lFKHandImageContainer", '69')
            lRPinky2 = rs_CreateRadioButton("RightHandPinky2", 377,163, lFKHandTabContainer, "lFKHandImageContainer", '70')
            lRPinky3 = rs_CreateRadioButton("RightHandPinky3", 384,133, lFKHandTabContainer, "lFKHandImageContainer", '71') 
            lRPinky4 = rs_CreateRadioButton("RightHandPinky4", 392,103, lFKHandTabContainer, "lFKHandImageContainer", '72')  
            lLThumb1 = rs_CreateRadioButton("LeftHandThumb1", 175,260, lFKHandTabContainer, "lFKHandImageContainer", '21')
            lLThumb2 = rs_CreateRadioButton("LeftHandThumb2", 190,230, lFKHandTabContainer, "lFKHandImageContainer", '22')
            lLThumb3 = rs_CreateRadioButton("LeftHandThumb3", 200,200, lFKHandTabContainer, "lFKHandImageContainer", '23')
            lLThumb4 = rs_CreateRadioButton("LeftHandThumb4", 215,170, lFKHandTabContainer, "lFKHandImageContainer", '24')        
            lLIndex = rs_CreateRadioButton("LeftInHandIndex", 155,205, lFKHandTabContainer, "lFKHandImageContainer", '25')
            lLIndex1 = rs_CreateRadioButton("LeftHandIndex1", 163,175, lFKHandTabContainer, "lFKHandImageContainer", '26')
            lLIndex2 = rs_CreateRadioButton("LeftHandIndex2", 173,145, lFKHandTabContainer, "lFKHandImageContainer", '27')
            lLIndex3 = rs_CreateRadioButton("LeftHandIndex3", 183,115, lFKHandTabContainer, "lFKHandImageContainer", '28')    
            lLIndex4 = rs_CreateRadioButton("LeftHandIndex4", 192,85, lFKHandTabContainer, "lFKHandImageContainer", '29')            
            lLMiddle = rs_CreateRadioButton("LeftInHandMiddle", 136,180, lFKHandTabContainer, "lFKHandImageContainer", '30')
            lLMiddle1 = rs_CreateRadioButton("LeftHandMiddle1", 136,150, lFKHandTabContainer, "lFKHandImageContainer", '31')
            lLMiddle2 = rs_CreateRadioButton("LeftHandMiddle2", 140,120, lFKHandTabContainer, "lFKHandImageContainer", '32')
            lLMiddle3 = rs_CreateRadioButton("LeftHandMiddle3", 142,90, lFKHandTabContainer, "lFKHandImageContainer", '33')         
            lLMiddle4 = rs_CreateRadioButton("LeftHandMiddle4", 144,60, lFKHandTabContainer, "lFKHandImageContainer'", '34')   
            lLRing = rs_CreateRadioButton("LeftInHandRing", 113,204, lFKHandTabContainer, "lFKHandImageContainer", '35')        
            lLRing1 = rs_CreateRadioButton("LeftHandRing1", 108,174, lFKHandTabContainer, "lFKHandImageContainer", '36')
            lLRing2 = rs_CreateRadioButton("LeftHandRing2", 105,144, lFKHandTabContainer, "lFKHandImageContainer", '37')
            lLRing3 = rs_CreateRadioButton("LeftHandRing3", 102,114, lFKHandTabContainer, "lFKHandImageContainer", '38') 
            lLRing4 = rs_CreateRadioButton("LeftHandRing4", 96,75, lFKHandTabContainer, "lFKHandImageContainer", '39') 
            lLPinky = rs_CreateRadioButton("LeftInHandPinky", 90,223, lFKHandTabContainer, "lFKHandImageContainer", '40')        
            lLPinky1 = rs_CreateRadioButton("LeftHandPinky1", 85,193, lFKHandTabContainer, "lFKHandImageContainer", '41')
            lLPinky2 = rs_CreateRadioButton("LeftHandPinky2", 80,163, lFKHandTabContainer, "lFKHandImageContainer", '42')
            lLPinky3 = rs_CreateRadioButton("LeftHandPinky3", 72,133, lFKHandTabContainer, "lFKHandImageContainer", '43') 
            lLPinky4 = rs_CreateRadioButton("LeftHandPinky4", 64,103, lFKHandTabContainer, "lFKHandImageContainer", '44')  

            lHandRadioButtonList = [lLHand2, lRHand2, lRThumb1, lRThumb2, lRThumb3, lRThumb4, lRIndex, lRIndex1, lRIndex2, lRIndex3, lRIndex4, 
                                    lRMiddle, lRMiddle1, lRMiddle2, lRMiddle3, lRMiddle4, lRRing, lRRing1, lRRing2, lRRing3, lRRing4, lRPinky, 
                                    lRPinky1, lRPinky2, lRPinky3, lRPinky4, lLThumb1, lLThumb2, lLThumb3, lLThumb4, lLIndex, lLIndex1, lLIndex2, 
                                    lLIndex3, lLIndex4, lLMiddle, lLMiddle1, lLMiddle2, lLMiddle3, lLMiddle4, lLRing, lLRing1, lLRing2, lLRing3, 
                                    lLRing4, lLPinky, lLPinky1, lLPinky2, lLPinky3, lLPinky4]
                
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Selection Sets
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
    
        selectionSetTabContainer = FBLayout()                
        selectionSetTabContainerX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        selectionSetTabContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        selectionSetTabContainerW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        selectionSetTabContainerH = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        selectionSetTabContainer.AddRegion("selectionSetTabContainer","selectionSetTabContainer", selectionSetTabContainerX, selectionSetTabContainerY, selectionSetTabContainerW, selectionSetTabContainerH)
        lTabControl.Add("Selection Sets",selectionSetTabContainer)        

        characterSelectDropDown3 = FBList()
        characterSelectDropDown3.Style = FBListStyle.kFBDropDownList
        characterSelectDropDown3.Hint = ''
        dropDownX = FBAddRegionParam( 5, FBAttachType.kFBAttachLeft, "" )
        dropDownY = FBAddRegionParam( 5, FBAttachType.kFBAttachTop, "lFKBodyImageContainer" )
        dropDownW = FBAddRegionParam( 180, FBAttachType.kFBAttachNone,"" )
        dropDownH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone,"" )            
        selectionSetTabContainer.AddRegion( 'characterSelectDropDown3', 'characterSelectDropDown3', dropDownX, dropDownY, dropDownW, dropDownH )
        selectionSetTabContainer.SetControl( 'characterSelectDropDown3', characterSelectDropDown3 )     

        characterList = []     
        for i in RS.Globals.gCharacters:
            characterList.append( i )

        for i in characterList:
            characterSelectDropDown3.Items.append( i.LongName )
           
        lCurrentCharacter = FBApplication().CurrentCharacter 
        for i in range( len( characterList ) ):
            if characterList[i].LongName == lCurrentCharacter.LongName:
                characterSelectDropDown3.Selected( i, True )

        keyModeDropDown3 = FBList()
        keyModeDropDown3.Style = FBListStyle.kFBDropDownList
        keyModeDropDown3.Hint = ''
        dropDownX = FBAddRegionParam( 5, FBAttachType.kFBAttachLeft, "" )
        dropDownY = FBAddRegionParam( 5, FBAttachType.kFBAttachBottom, "characterSelectDropDown3" )
        dropDownW = FBAddRegionParam( 180, FBAttachType.kFBAttachNone,"" )
        dropDownH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone,"" )            
        selectionSetTabContainer.AddRegion( 'keyModeDropDown3', 'keyModeDropDown3', dropDownX, dropDownY, dropDownW, dropDownH )
        selectionSetTabContainer.SetControl( 'keyModeDropDown3', keyModeDropDown3 )     

        keyModeDropDown3.Items.append( 'KeyMode: Full' )
        keyModeDropDown3.Items.append( 'KeyMode: Body Part' )
        keyModeDropDown3.Items.append( 'KeyMode: Selection' )
        
        if len( characterList ) <= 0:
            keyModeDropDown3.Enabled = False

        allButton = FBButton()
        allButton.Caption = "Select All Sets"
        allButton.Name = 'select all'
        allButton.Style = FBButtonStyle.kFBRadioButton
        X = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        Y = FBAddRegionParam(70,FBAttachType.kFBAttachTop, "")
        W = FBAddRegionParam(100,FBAttachType.kFBAttachNone,"")
        H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        selectionSetTabContainer.AddRegion('allButton', 'allButton', X, Y, W, H)
        selectionSetTabContainer.SetControl('allButton', allButton)  

        noneButton = FBButton()
        noneButton.Caption = "Deselect All Sets"
        noneButton.Name = 'deselect all'
        noneButton.Style = FBButtonStyle.kFBRadioButton
        X = FBAddRegionParam(10,FBAttachType.kFBAttachRight,"allButton")
        Y = FBAddRegionParam(70,FBAttachType.kFBAttachTop, "")
        W = FBAddRegionParam(110,FBAttachType.kFBAttachNone,"")
        H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        selectionSetTabContainer.AddRegion('noneButton', 'noneButton', X, Y, W, H)
        selectionSetTabContainer.SetControl('noneButton', noneButton)   

        buttonGroup2 = FBButtonGroup()
        buttonGroup2.Add(allButton)
        buttonGroup2.Add(noneButton)

        handButton = FBButton()
        handButton.Name = 'hand'
        handButton.Caption = "Hand IK/FK Selection Set"
        handButton.Style = FBButtonStyle.kFBRadioButton
        X = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        Y = FBAddRegionParam(20,FBAttachType.kFBAttachBottom, "noneButton")
        W = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        selectionSetTabContainer.AddRegion('handButton', 'handButton', X, Y, W, H)
        selectionSetTabContainer.SetControl('handButton', handButton)                

        phButton = FBButton()
        phButton.Name = 'ph'
        phButton.Caption = "PH/IK Hand Selection Set"
        phButton.Style = FBButtonStyle.kFBRadioButton
        X = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        Y = FBAddRegionParam(10,FBAttachType.kFBAttachBottom, "handButton")
        W = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        selectionSetTabContainer.AddRegion('phButton', 'phButton', X, Y, W, H)
        selectionSetTabContainer.SetControl('phButton', phButton)  
        
        feetButton = FBButton()
        feetButton.Name = 'foot'
        feetButton.Caption = "Foot IK/FK Selection Set"
        feetButton.Style = FBButtonStyle.kFBRadioButton
        X = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        Y = FBAddRegionParam(10,FBAttachType.kFBAttachBottom, "phButton")
        W = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        selectionSetTabContainer.AddRegion('feetButton', 'feetButton', X, Y, W, H)
        selectionSetTabContainer.SetControl('feetButton', feetButton)        

        buttonGroup = FBButtonGroup()
        buttonGroup.Add(handButton)
        buttonGroup.Add(phButton)
        buttonGroup.Add(feetButton)        
        
        selectButton = FBButton()
        selectButton.Name = 'select'
        selectButton.Caption = "Select Set(s) in Scene"
        X = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        Y = FBAddRegionParam(35,FBAttachType.kFBAttachBottom, "feetButton")
        W = FBAddRegionParam(210,FBAttachType.kFBAttachNone,"")
        H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        selectionSetTabContainer.AddRegion('selectButton', 'selectButton', X, Y, W, H)
        selectionSetTabContainer.SetControl('selectButton', selectButton)     

        groupButton = FBButton()
        groupButton.Name = 'group'
        groupButton.Caption = "Create Group(s)"
        X = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        Y = FBAddRegionParam(10,FBAttachType.kFBAttachBottom, "selectButton")
        W = FBAddRegionParam(210,FBAttachType.kFBAttachNone,"")
        H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        selectionSetTabContainer.AddRegion('groupButton', 'groupButton', X, Y, W, H)
        selectionSetTabContainer.SetControl('groupButton', groupButton)  
        

        selectionSetRadioButtonList = [ allButton, noneButton, handButton, phButton, feetButton ]
        
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: SelectionSetCallback
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        def RadioSelectionsCallback( control, event ):        
            
            # radio selections
            if control.Name == 'select all':
                handButton.State = 1
                phButton.State = 1
                feetButton.State = 1
            
            if control.Name == 'deselect all':
                handButton.State = 0
                phButton.State = 0
                feetButton.State = 0
                
            if control.Name == 'hand' or control.Name == 'foot' or control.Name == 'ph':
                allButton.State = 0
                noneButton.State = 0
                
        def SelectionSetCallback( control, event ): 
          
            if rs_CheckCharacter() == True:
        
                SelectBodyCharacter( None, None )
                KeyingModeBody( None, None )
                
                lCurrentCharacter = FBApplication().CurrentCharacter
                nameSplit = lCurrentCharacter.LongName.split( ':' )                
                namespace = nameSplit[0]
                
                lCtrlHips = lCurrentCharacter.GetCtrlRigModel(FBBodyNodeId.kFBHipsNodeId)
                lRootRefNull = RS.Utils.Scene.GetParent(lCtrlHips)
                
                if lRootRefNull == None:
                    control.State = 0
                    FBMessageBox('Error', "Cannot find current character's control rig.", 'Ok')
        
                else:
                    
                    lRigList = []
                    
                    handList = []
                    phList = []
                    feetList = []
                    
                    RS.Utils.Scene.GetChildren(lRootRefNull, lRigList)      
                    
                    # ph list
                    lCurrentCharacter = FBApplication().CurrentCharacter
                    phLHand = FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":PH_L_Hand")
                    phRHand = FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":PH_R_Hand")
                    ikLHand = FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":IK_L_Hand")
                    ikRHand = FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":IK_R_Hand")
                    
                    if phLHand:
                        phList.append( phLHand )
                    if phRHand:
                        phList.append( phRHand )
                    if ikLHand:
                        phList.append( ikLHand )                               
                    if ikRHand:
                        phList.append( ikRHand )                     
                    
                    # hand list
                    for i in lHandRadioButtonList:
                        handList.append( lRigList[int( i.Name )] )
                    
                    if project == 'gta5': 
                        handList.append( lRigList[82] )#LeftHandThumbEffector
                        handList.append( lRigList[83] )#LeftHandIndexEffector
                        handList.append( lRigList[84] )#LeftHandMiddleEffector
                        handList.append( lRigList[85] )#LeftHandRingEffector
                        handList.append( lRigList[86] )#LeftHandPinkyEffector
                        handList.append( lRigList[87] )#RightHandThumbEffector
                        handList.append( lRigList[88] )#RightHandIndexEffector
                        handList.append( lRigList[89] )#RightHandMiddleEffector
                        handList.append( lRigList[90] )#RightHandRingEffector                        
                        handList.append( lRigList[91] )#RightHandPinkyEffector                        
                        handList.append( lRigList[67] )#LeftWristEffector
                        handList.append( lRigList[68] )#RightWristEffector  
                        
                        # foot list
                        feetList.append( lRigList[4] )#Left Foot
                        feetList.append( lRigList[5] )#Left ToeBase                            
                        feetList.append( lRigList[8] )#Right Foot 
                        feetList.append( lRigList[9] )#Right ToeBase
                        feetList.append( lRigList[65] )#LeftAnkleEffector
                        feetList.append( lRigList[66] )#RightAnkleEffector  
                        
                    else:                    
                        handList.append( lRigList[95] )#LeftHandThumbEffector
                        handList.append( lRigList[96] )#LeftHandIndexEffector
                        handList.append( lRigList[97] )#LeftHandMiddleEffector
                        handList.append( lRigList[98] )#LeftHandRingEffector
                        handList.append( lRigList[99] )#LeftHandPinkyEffector
                        handList.append( lRigList[100] )#RightHandThumbEffector
                        handList.append( lRigList[101] )#RightHandIndexEffector
                        handList.append( lRigList[102] )#RightHandMiddleEffector
                        handList.append( lRigList[103] )#RightHandRingEffector                        
                        handList.append( lRigList[104] )#RightHandPinkyEffector                        
                        handList.append( lRigList[80] )#LeftWristEffector
                        handList.append( lRigList[81] )#RightWristEffector  
                        
                        # foot list
                        feetList.append( lRigList[4] )#Left Toe
                        feetList.append( lRigList[5] )#Left ToeBase                            
                        feetList.append( lRigList[8] )#Right Toe 
                        feetList.append( lRigList[9] )#Right ToeBase
                        feetList.append( lRigList[78] )#LeftAnkleEffector
                        feetList.append( lRigList[79] )#RightAnkleEffector                          
                    
                    # hand selection set
                    if handButton.State == 1:  
                        if control.Name == 'select':                                
                            RS.Utils.Scene.DeSelectAll()                                                    
                            for i in handList:
                                i.Selected = True
                                
                        if control.Name == 'group':                            
                            group = CreateSelectionSetGroup( namespace, handButton.Name )                            
                            if group:
                                for i in handList:
                                    group.ConnectSrc( i )
                            
                    # ph/ik hand selection set 
                    if  phButton.State == 1:
                        if control.Name == 'select': 
                            for i in phList:
                                i.Selected = True
                                
                        if control.Name == 'group':                          
                            group = CreateSelectionSetGroup( namespace, phButton.Name )
                            if group:
                                for i in phList:
                                    group.ConnectSrc( i )
                                
                    # feet selection set
                    if  feetButton.State == 1:
                        if control.Name == 'select': 
                            for i in feetList:
                                i.Selected = True
                                
                        if control.Name == 'group': 
                            group = CreateSelectionSetGroup( namespace, feetButton.Name )
                            if group:
                                for i in feetList:
                                    group.ConnectSrc( i )
                            
            else:
                FBMessageBox( 'R* Error', 'Character or Control Rig not found.', 'Ok' )
                          
            
        allButton.OnClick.Add( RadioSelectionsCallback ) 
        noneButton.OnClick.Add( RadioSelectionsCallback )   
        handButton.OnClick.Add( RadioSelectionsCallback )
        phButton.OnClick.Add( RadioSelectionsCallback )
        feetButton.OnClick.Add( RadioSelectionsCallback )
        selectButton.OnClick.Add( SelectionSetCallback )
        groupButton.OnClick.Add( SelectionSetCallback )
        
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: RigSelectionCallback
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        def RigSelectionCallback( control, event ):
        
            if rs_CheckCharacter() == True:
        
                SelectBodyCharacter( None, None )
                KeyingModeBody( None, None )
                
                lCurrentCharacter = FBApplication().CurrentCharacter
                lCtrlHips = lCurrentCharacter.GetCtrlRigModel(FBBodyNodeId.kFBHipsNodeId)
                lRootRefNull = RS.Utils.Scene.GetParent(lCtrlHips)
        
                if lRootRefNull == None:
                    control.State = 0
                    FBMessageBox('Error', "Cannot find current character's control rig.", 'Ok')
        
                else:
                    lRigList = []
                    RS.Utils.Scene.GetChildren(lRootRefNull, lRigList)
                                        
                    for i in range( len( lRigList ) ):
                        lRigList[ int( control.Name ) ].Selected = True
                        
            else:
                control.State = 0
                FBMessageBox('Error', "Control Rig is missing.", 'Ok')   


        for button in lBodyRadioButtonList:
            button.OnClick.Add( RigSelectionCallback )
            
        for button in lHandRadioButtonList:
            button.OnClick.Add( RigSelectionCallback )
                
                
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Select Character Callbacks
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ############################################################################################################### 		

        def ClearRadioButtons( control, event ):
            
            for i in lHandRadioButtonList:
                i.State = 0
            
            for i in lBodyRadioButtonList:
                i.State = 0    
                
            for i in selectionSetRadioButtonList:
                i.State = 0                  
                
            RS.Utils.Scene.DeSelectAll()       
        
        #def CheckRig():
            
            #if rs_CheckCharacter() == True:      
                #lCurrentCharacter = FBApplication().CurrentCharacter            
                #lCtrlHips = lCurrentCharacter.GetCtrlRigModel(FBBodyNodeId.kFBHipsNodeId)
            
                #lRootRefNull = RS.Utils.Scene.GetParent(lCtrlHips)
                
                #if lRootRefNull:
                    #lRigList = []
                    #RS.Utils.Scene.GetChildren(lRootRefNull, lRigList)
                                    
                    #for button in lHandRadioButtonList:
                        #foundHandRigJoint = False 
                        #for i in range( len( lRigList ) ): 
                            #if lRigList[i].Name.endswith( button.Hint ):
                                #foundHandRigJoint = True
                                #button.Enabled = True
                        
                        #if foundHandRigJoint == False:   
                            #if 'mover' in button.Hint or 'PH_' in button.Hint:
                                #None
                            #else:
                                #button.Enabled = False
                    
                    #for button in lBodyRadioButtonList:
                        #foundBodyRigJoint = False 
                        #for i in range( len( lRigList ) ):
                            #if lRigList[i].Name.endswith( button.Hint ):
                                #foundBodyRigJoint = True
                                #button.Enabled = True
                                
                        #if foundBodyRigJoint == False:   
                            #if 'mover' in button.Hint or 'PH_' in button.Hint:
                                #None
                            #else:
                                #button.Enabled = False 
                            
        def SelectBodyCharacter( control, event ):
            
            if rs_CheckCharacter() == True: 
                selectedCharacter = None
                selectedIndex = None
                characterFound = False                
                
                for i in range( len( characterSelectDropDown.Items ) ):
                    if characterSelectDropDown.IsSelected( i ):
                        selectedCharacter = characterSelectDropDown.Items[i]
                        selectedIndex = i
                                            
                characterSelectDropDown2.Selected( selectedIndex, True )
                characterSelectDropDown3.Selected( selectedIndex, True )                

                for i in RS.Globals.gCharacters:            
                    
                    i.Selected = False                
                    
                    if selectedCharacter == i.LongName:
                        characterFound = True
                        FBApplication().CurrentCharacter = i
                        i.Selected = True
                
                if characterFound == False:
                    print 'cannot find character' 
                
                #CheckRig()            
            
        characterSelectDropDown.OnChange.Add( SelectBodyCharacter )
        characterSelectDropDown.OnChange.Add( ClearRadioButtons )


        def SelectHandCharacter( control, event ):
            
            if rs_CheckCharacter() == True: 
                selectedCharacter = None
                selectedIndex = None
                characterFound = False                
                
                for i in range( len( characterSelectDropDown2.Items ) ):
                    if characterSelectDropDown2.IsSelected( i ):
                        selectedCharacter = characterSelectDropDown2.Items[i]
                        selectedIndex = i
                        
                characterSelectDropDown.Selected( selectedIndex, True )
                characterSelectDropDown3.Selected( selectedIndex, True )                
                
                for i in RS.Globals.gCharacters:            
                    
                    i.Selected = False                
                    
                    if selectedCharacter == i.LongName:
                        characterFound = True
                        FBApplication().CurrentCharacter = i
                        i.Selected = True
                
                if characterFound == False:
                    print 'cannot find character'
                
                #CheckRig()
        
        characterSelectDropDown2.OnChange.Add( SelectHandCharacter )  
        characterSelectDropDown2.OnChange.Add( ClearRadioButtons )

        #CheckRig()
        
        def SelectSetsCharacter( control, event ):
            
            if rs_CheckCharacter() == True: 
                selectedCharacter = None
                selectedIndex = None
                characterFound = False                
                
                for i in range( len( characterSelectDropDown3.Items ) ):
                    if characterSelectDropDown3.IsSelected( i ):
                        selectedCharacter = characterSelectDropDown3.Items[i]
                        selectedIndex = i
                        
                characterSelectDropDown.Selected( selectedIndex, True )
                characterSelectDropDown2.Selected( selectedIndex, True )
                
                for i in RS.Globals.gCharacters:            
                    
                    i.Selected = False                
                    
                    if selectedCharacter == i.LongName:
                        characterFound = True
                        FBApplication().CurrentCharacter = i
                        i.Selected = True
                
                if characterFound == False:
                    print 'cannot find character'
                
                #CheckRig()
        
        characterSelectDropDown3.OnChange.Add( SelectSetsCharacter )  
        characterSelectDropDown3.OnChange.Add( ClearRadioButtons )

        #CheckRig()
        
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Keying Mode Callback
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ############################################################################################################### 
        
        def KeyingModeBody( control, event ):
            
            if rs_CheckCharacter() == True:             
            
                lCurrentCharacter = FBApplication().CurrentCharacter

                selectedIndex = None
                
                for i in range( len( keyModeDropDown.Items ) ):
                    if keyModeDropDown.IsSelected( i ):
                        selectedIndex = i
                        
                if selectedIndex == 0:
                    lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingFullBody 
                elif selectedIndex == 1:
                    lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingBodyPart
                else:
                    lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingSelection
            
                keyModeDropDown2.Selected( selectedIndex, True )
                keyModeDropDown3.Selected( selectedIndex, True )
                
            else:
                FBMessageBox( 'R* Error', 'MB Ctrl Rig not found.', 'Ok' )
                keyModeDropDown.Selected( 0, True )

        keyModeDropDown.OnChange.Add( KeyingModeBody )

        def KeyingModeHands( control, event ):
            
            if rs_CheckCharacter() == True:              
                
                lCurrentCharacter = FBApplication().CurrentCharacter
    
                selectedIndex = None
                
                for i in range( len( keyModeDropDown2.Items ) ):
                    if keyModeDropDown2.IsSelected( i ):
                        selectedIndex = i
                 
                if selectedIndex == 0:
                    lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingFullBody 
                elif selectedIndex == 1:
                    lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingBodyPart
                else:
                    lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingSelection
            
                keyModeDropDown.Selected( selectedIndex, True ) 
                keyModeDropDown3.Selected( selectedIndex, True )
            
            else:
                FBMessageBox( 'R* Error', 'MB Ctrl Rig not found.', 'Ok' )            
                keyModeDropDown2.Selected( 0, True )
                
        keyModeDropDown2.OnChange.Add( KeyingModeHands )

        def KeyingModeSelectionSet( control, event ):
            
            if rs_CheckCharacter() == True:              
                
                lCurrentCharacter = FBApplication().CurrentCharacter
    
                selectedIndex = None
                
                for i in range( len( keyModeDropDown3.Items ) ):
                    if keyModeDropDown3.IsSelected( i ):
                        selectedIndex = i
                 
                if selectedIndex == 0:
                    lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingFullBody 
                elif selectedIndex == 1:
                    lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingBodyPart
                else:
                    lCurrentCharacter.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingSelection
            
                keyModeDropDown.Selected( selectedIndex, True ) 
                keyModeDropDown2.Selected( selectedIndex, True ) 

            else:
                FBMessageBox( 'R* Error', 'MB Ctrl Rig not found.', 'Ok' )            
                keyModeDropDown3.Selected( 0, True )
                
        keyModeDropDown3.OnChange.Add( KeyingModeSelectionSet )        

        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Single Click Callbacks
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ############################################################################################################### 

        def rs_OnSingleClickCallback(pControl, pEvent):

            lSelectedList = FBModelList()
            FBGetSelectedModels(lSelectedList)

            for iSelected in lSelectedList :
                iSelected.Selected = False

            for iBodyRadioButton in lBodyRadioButtonList:
                iBodyRadioButton.State = 0 
                lMover.State = 0 
                lLPHHand.State = 0 
                lRPHHand.State = 0                
                
            for iHandRadioButton in lHandRadioButtonList:
                iHandRadioButton.State = 0               

        lFKBodyImageContainer.OnChange.Add(rs_OnSingleClickCallback) 
        lFKHandImageContainer.OnChange.Add(rs_OnSingleClickCallback)    


    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Define Tool and Launch UI
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################       

        lTabControl.SetContent( 0 )

def Run( show = True ):
    tool = FKControlsTool()

    if show:
        tool.Show()

    return tool






# not using finger groups atm
################################################################################################################
###* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###
### Description: rs_FingerGroupSelection
###
###* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
################################################################################################################

#def rs_FingerGroupSelection(pFingerButton, pFingerJoint1, pJointNumber1, pFingerJoint2, pJointNumber2, pFingerJoint3, pJointNumber3, pKeyingMode1, pKeyingMode2, pKeyingMode3):

    #if pFingerButton.State == 1:
        #pFingerJoint1.State = 1
        #pFingerJoint2.State = 1
        #pFingerJoint3.State = 1
        #rs_RigSelection(pFingerJoint1, pJointNumber1, pKeyingMode1, pKeyingMode2, pKeyingMode3)
        #rs_RigSelection(pFingerJoint2, pJointNumber2, pKeyingMode1, pKeyingMode2, pKeyingMode3)
        #rs_RigSelection(pFingerJoint3, pJointNumber3, pKeyingMode1, pKeyingMode2, pKeyingMode3) 

    #if pFingerButton.State == 0:
        #pFingerJoint1.State = 0
        #pFingerJoint2.State = 0
        #pFingerJoint3.State = 0
        #RS.Utils.Scene.DeSelectAll()


# Hiding Quadruped section for now as animators arent sure what they'd want for this as there are too many diff setups - custom and MB
################################################################################################################
###* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###
### Description: Quadruped Tab
###
###* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
################################################################################################################

#lFKQuadTabContainer = FBLayout()                
#lFKQuadTabContainerX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
#lFKQuadTabContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
#lFKQuadTabContainerW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
#lFKQuadTabContainerH = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
#lFKQuadTabContainer.AddRegion("lFKQuadTabContainer","lFKQuadTabContainer", lFKQuadTabContainerX, lFKQuadTabContainerY, lFKQuadTabContainerW, lFKQuadTabContainerH)
#lTabControl.Add("Quad",lFKQuadTabContainer)

#lFKQuadImageContainer = FBVisualContainer()
#lFKQuadImageContainerX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
#lFKQuadImageContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
#lFKQuadImageContainerW = FBAddRegionParam(452,FBAttachType.kFBAttachNone,"")
#lFKQuadImageContainerH = FBAddRegionParam(440,FBAttachType.kFBAttachNone,"")
#lFKQuadTabContainer.AddRegion("lFKQuadImageContainer","lFKBodyImage", lFKQuadImageContainerX, lFKQuadImageContainerY, lFKQuadImageContainerW, lFKQuadImageContainerH)
#lFKQuadTabContainer.SetControl("lFKQuadImageContainer",lFKQuadImageContainer)

#lFKQuadImage = "{0}\\FK_Controls_Quad_Body.tif".format( RS.Config.Script.Path.ToolImages )
#lFKQuadImageContainer.Items.append("")
#lFKQuadImageContainer.ItemIconSet(0, lFKQuadImage)
#lFKQuadImageContainer.ItemWidth = 450
#lFKQuadImageContainer.ItemHeight = 455
#lFKQuadImageContainer.ItemWrap = True

#lQuadHead = rs_CreateRadioButton("Head", 223,65, lFKQuadTabContainer, "lFKBodyImageContainer")
#lQuadNeck3 = rs_CreateRadioButton("Neck3", 223,85, lFKQuadTabContainer, "lFKBodyImageContainer")   
#lQuadNeck2 = rs_CreateRadioButton("Neck2", 223,100, lFKQuadTabContainer, "lFKBodyImageContainer")   
#lQuadNeck1 = rs_CreateRadioButton("Neck1", 223,115, lFKQuadTabContainer, "lFKBodyImageContainer")      
#lQuadRShoulder = rs_CreateRadioButton("RightShoulder", 200,125, lFKQuadTabContainer, "lFKBodyImageContainer") 
#lQuadLShoulder = rs_CreateRadioButton("LeftShoulder", 245,125, lFKQuadTabContainer, "lFKBodyImageContainer")   
#lQuadRArm = rs_CreateRadioButton("RightArm", 180,135, lFKQuadTabContainer, "lFKBodyImageContainer") 
#lQuadLArm = rs_CreateRadioButton("LeftArm", 265,135, lFKQuadTabContainer, "lFKBodyImageContainer")
#lQuadRForeArm = rs_CreateRadioButton("RightForeArm", 155,135, lFKQuadTabContainer, "lFKBodyImageContainer") 
#lQuadLForeArm = rs_CreateRadioButton("LeftForeArm", 290,135, lFKQuadTabContainer, "lFKBodyImageContainer")
#lQuadRHand = rs_CreateRadioButton("RightHand", 120,135, lFKQuadTabContainer, "lFKBodyImageContainer") 
#lQuadLHand = rs_CreateRadioButton("LeftHand", 325,135, lFKQuadTabContainer, "lFKBodyImageContainer")
#lQuadRFingerBase = rs_CreateRadioButton("RightFingerBase", 100,135, lFKQuadTabContainer, "lFKBodyImageContainer")
#lQuadLFingerBase = rs_CreateRadioButton("LeftFingerBase", 345,135, lFKQuadTabContainer, "lFKBodyImageContainer")
#lQuadRHandIndex = rs_CreateRadioButton("RightHandIndex", 80,135, lFKQuadTabContainer, "lFKBodyImageContainer")
#lQuadLHandIndex = rs_CreateRadioButton("LeftHandIndex", 365,135, lFKQuadTabContainer, "lFKBodyImageContainer")    
#lQuadSpine3 = rs_CreateRadioButton("Spine3", 223,160, lFKQuadTabContainer, "lFKBodyImageContainer")      
#lQuadSpine2 = rs_CreateRadioButton("Spine2", 223,190, lFKQuadTabContainer, "lFKBodyImageContainer")      
#lQuadSpine1 = rs_CreateRadioButton("Spine1", 223,210, lFKQuadTabContainer, "lFKBodyImageContainer")      
#lQuadSpine = rs_CreateRadioButton("Spine", 223,235, lFKQuadTabContainer, "lFKBodyImageContainer")  
#lQuadHips = rs_CreateRadioButton("Hips", 223,280, lFKQuadTabContainer, "lFKBodyImageContainer")  
#lQuadRUpperLeg = rs_CreateRadioButton("RightUpperLeg", 200,310, lFKQuadTabContainer, "lFKBodyImageContainer")        
#lQuadLUpperLeg = rs_CreateRadioButton("LeftUpperLeg", 245,310, lFKQuadTabContainer, "lFKBodyImageContainer")  
#lQuadRLeg = rs_CreateRadioButton("RightLeg", 165,310, lFKQuadTabContainer, "lFKBodyImageContainer")        
#lQuadLLeg = rs_CreateRadioButton("LeftLeg", 285,310, lFKQuadTabContainer, "lFKBodyImageContainer") 
#lQuadRFoot = rs_CreateRadioButton("RightFoot", 125,310, lFKQuadTabContainer, "lFKBodyImageContainer")        
#lQuadLFoot = rs_CreateRadioButton("LeftFoot", 320,310, lFKQuadTabContainer, "lFKBodyImageContainer") 
#lQuadRToeBase = rs_CreateRadioButton("RightToeBase", 100,310, lFKQuadTabContainer, "lFKBodyImageContainer")         
#lQuadLToeBase = rs_CreateRadioButton("LeftToeBase", 345,310, lFKQuadTabContainer, "lFKBodyImageContainer") 
#lQuadRFootIndex = rs_CreateRadioButton("RightFootIndex", 80,310, lFKQuadTabContainer, "lFKBodyImageContainer")         
#lQuadLFootIndex = rs_CreateRadioButton("LeftFootIndex", 365,310, lFKQuadTabContainer, "lFKBodyImageContainer")      
#lQuadTail01 = rs_CreateRadioButton("Tail01", 223,330, lFKQuadTabContainer, "lFKBodyImageContainer")      
#lQuadTail02 = rs_CreateRadioButton("Tail02", 223,342, lFKQuadTabContainer, "lFKBodyImageContainer")      
#lQuadTail03 = rs_CreateRadioButton("Tail03", 223,354, lFKQuadTabContainer, "lFKBodyImageContainer")      
#lQuadTail04 = rs_CreateRadioButton("Tail04", 223,366, lFKQuadTabContainer, "lFKBodyImageContainer")      
#lQuadTail05 = rs_CreateRadioButton("Tail05", 223,378, lFKQuadTabContainer, "lFKBodyImageContainer")          
#lQuadMover = rs_CreateRadioButton("mover", 22,419, lFKQuadTabContainer, "lFKBodyImageContainer") 

#lQuadRadioButtonList = [lQuadHead, lQuadNeck3, lQuadNeck2, lQuadNeck1, lQuadRShoulder, lQuadLShoulder, lQuadRArm, lQuadLArm, lQuadRForeArm, lQuadLForeArm, lQuadRHand, 
                        #lQuadLHand, lQuadRFingerBase, lQuadLFingerBase, lQuadRHandIndex, lQuadLHandIndex, lQuadSpine3, lQuadSpine2, lQuadSpine1, lQuadSpine, lQuadHips, 
                        #lQuadRUpperLeg, lQuadLUpperLeg, lQuadRLeg, lQuadLLeg, lQuadRFoot, lQuadLFoot, lQuadRToeBase, lQuadLToeBase, lQuadRFootIndex, lQuadLFootIndex, 
                        #lQuadTail01, lQuadTail02, lQuadTail03, lQuadTail04, lQuadTail05, lQuadMover]    

#lQuadMover.OnClick.Add(rs_lMoverButtonCallback)
#lQuadTail01.OnClick.Add(rs_lQuadTail01ButtonCallback) 
#lQuadTail02.OnClick.Add(rs_lQuadTail02ButtonCallback) 
#lQuadTail03.OnClick.Add(rs_lQuadTail03ButtonCallback)
#lQuadTail04.OnClick.Add(rs_lQuadTail04ButtonCallback) 
#lQuadTail05.OnClick.Add(rs_lQuadTail05ButtonCallback)    




#def rs_lQuadTail01ButtonCallback(pControl, pEvent):
    #if rs_CheckCharacter() == True:
        #lCurrentCharacter = FBApplication().CurrentCharacter
        #FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":SKEL_Tail_01").Selected = True
    #else:
        #lQuadTail01.State = 0
        #FBMessageBox('Error', "Cannot find current character's control rig", 'Ok')            


#def rs_lQuadTail02ButtonCallback(pControl, pEvent):
    #if rs_CheckCharacter() == True:
        #lCurrentCharacter = FBApplication().CurrentCharacter
        #FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":SKEL_Tail_02").Selected = True
    #else:
        #lQuadTail02.State = 0
        #FBMessageBox('Error', "Cannot find current character's control rig", 'Ok')            


#def rs_lQuadTail03ButtonCallback(pControl, pEvent):
    #if rs_CheckCharacter() == True:
        #lCurrentCharacter = FBApplication().CurrentCharacter
        #FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":SKEL_Tail_03").Selected = True
    #else:
        #lQuadTail03.State = 0
        #FBMessageBox('Error', "Cannot find current character's control rig", 'Ok')            


#def rs_lQuadTail04ButtonCallback(pControl, pEvent):
    #if rs_CheckCharacter() == True:
        #lCurrentCharacter = FBApplication().CurrentCharacter
        #FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":SKEL_Tail_04").Selected = True
    #else:
        #lQuadTail04.State = 0
        #FBMessageBox('Error', "Cannot find current character's control rig", 'Ok')            


#def rs_lQuadTail05ButtonCallback(pControl, pEvent):
    #if rs_CheckCharacter() == True:
        #lCurrentCharacter = FBApplication().CurrentCharacter
        #FBFindModelByLabelName(rs_GetCurrentCharacterNamespace(lCurrentCharacter) + ":SKEL_Tail_05").Selected = True
    #else:
        #lQuadTail05.State = 0
        #FBMessageBox('Error', "Cannot find current character's control rig", 'Ok')  