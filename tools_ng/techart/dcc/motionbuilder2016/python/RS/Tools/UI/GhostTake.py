import os
import inspect

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Globals as glo
import RS.Tools.GhostTake as core
import RS.Utils.Creation as cre
import RS.Tools.UI


reload(core)

class GhostTakeTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Ghost Take', size = [ 402, 330 ], helpUrl = 'https://devstar.rockstargames.com/wiki/index.php/Ghost_Take_Tool' )

    # @RS.Tools.UI.LogEvent
    def CreateGhost( self, control, event ): 
        # Gets the selected item from the list in the UI
        for i in range(len(lReferenceList.Items)):
            if lReferenceList.IsSelected(i):
                # If one is selected then pass that to Core functionality module
                core.rs_CreateGhost(lReferenceList.Items[i])
                # This read the variable to set in the UI, so people know what Ghost is in the scene
                lMover = FBFindModelByLabelName("GHOST:mover")    
                if lMover:
                    lProp = lMover.PropertyList.Find('Ghost Type') 
                    if lProp:
                        lCurrentGhostBox.Text = lProp.Data            
        lReferenceNullList = []                   
        # Objects to select from
        
        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        if lReferenceNull:
            RS.Utils.Scene.GetChildren(lReferenceNull, lReferenceNullList, "", False)        
        
        if len(lReferenceNullList) > 1:
            lReferenceList.Selected(-1, True)            
    
    # @RS.Tools.UI.LogEvent
    def DeleteGhost( self, control, event ):        
        lMover = FBFindModelByLabelName("GHOST:mover")
        if not lMover:
            FBMessageBox("Ghost Take", "There is no ghost is this file to remove", "OK")
            return
        
        core.rs_DeleteGhost(control, event)
        lMover = FBFindModelByLabelName("GHOST:mover")        
        if not lMover:
            lCurrentGhostBox.Text = ""
        lCurrentGhostTakeBox.Items.removeAll()
    
    # @RS.Tools.UI.LogEvent
    def CreateGhostTake( self, control, event ):
        # Gets the selected item from the list in the UI
        for i in range(len(lTakeList.Items)):
            if lTakeList.IsSelected(i):    
                core.rs_CreateGhostTake(lTakeList.Items[i], lEdit.Text)                
                lCurrentTake = FBSystem().CurrentTake
                lProp = lCurrentTake.PropertyList.Find('Ghost Take Anim')    
                if lProp:        
                    lProp.Data = lTakeList.Items[i]
                    for lCompletedTakeList in lCurrentGhostTakeBox.Items:                    
                        if lCompletedTakeList.startswith(lCurrentTake.Name + "("):
                            lCurrentGhostTakeBox.Items.remove(lCompletedTakeList)
                    lVal = lCurrentTake.Name + "(GHOST->" + lProp.Data + ")"
                    lCurrentGhostTakeBox.Items.append(lVal)         
        lReferenceNullList = []                   
        # Objects to select from
        
        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        if lReferenceNull:
            RS.Utils.Scene.GetChildren(lReferenceNull, lReferenceNullList, "", False)     

        if len(lReferenceNullList) > 1:
            lReferenceList.Selected(-1, True)    
    
    def Refresh( self, control, event ):
        self.Show( True )
    
    def Create( self, pMain ):

        # Refresh UI
        lRefreshUIButton = FBButton()
        lRefreshUIButton.Caption = "Click to Refresh UI"
        lRefreshUIButton.Justify = FBTextJustify.kFBTextJustifyCenter  
        lRefreshUIButtonX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lRefreshUIButtonY = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")
        lRefreshUIButtonW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lRefreshUIButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        pMain.AddRegion("lRefreshUIButton","lRefreshUIButton", lRefreshUIButtonX, lRefreshUIButtonY, lRefreshUIButtonW, lRefreshUIButtonH)
        pMain.SetControl("lRefreshUIButton",lRefreshUIButton)
        lRefreshUIButton.OnClick.Add( self.Refresh )          
           
        ######################## RIGHT SIDE OF UI #############################
        
        # Scene Setup Buttons
        # Anchored to left (mainLyt), and bottom (charPropVehicle), this allows me to set a height and width variable, as it will be fixed size width and height wise.
        sceneSetup = FBLabel()    
        sceneSetup.Caption = "Setup an object:"
        sceneSetup.Hint = "Select an object you would like to create as a ghost, one ghost per scene"
        setupTitlex = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        setupTitley = FBAddRegionParam(60,FBAttachType.kFBAttachTop,"")
        setupTitlew = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        setupTitleh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")    
        pMain.AddRegion("setupTitle","setupTitle", setupTitlex,setupTitley,setupTitlew,setupTitleh)
        pMain.SetControl("setupTitle", sceneSetup)

        lReferenceNullList = []                   
        # Objects to select from
        global lReferenceList
        lReferenceList = FBList()
        lReferenceList.Hint = "Select an object you would like to create as a ghost, one ghost per scene"      
        
        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        if lReferenceNull:
            RS.Utils.Scene.GetChildren(lReferenceNull, lReferenceNullList, "", False)
       
        if len(lReferenceNullList) >=1:
            for iRef in lReferenceNullList:
                lReferenceList.Items.append(iRef.Name)
    
        lReferenceList.Style = FBListStyle.kFBDropDownList 
        
        # if you only have one reference then, you dont' need that gap at the top of this list.
        if len(lReferenceNullList) > 1:
            lReferenceList.Selected(-1, True)
        lReferenceListX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lReferenceListY = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"setupTitle")
        lReferenceListW = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        lReferenceListH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"") 
        pMain.AddRegion( "lReferenceList", "lReferenceList", lReferenceListX, lReferenceListY, lReferenceListW, lReferenceListH )
        pMain.SetControl( "lReferenceList", lReferenceList )    
        
        # Create Ghost
        global lCreateButton
        lCreateButton = FBButton()
        lCreateButton.Caption = "Create Ghost"
        lCreateButton.Hint = "This will create a ghost object in your file from the above list"
        lCreateButton.Look = FBButtonLook.kFBLookPush
        lCreateButtonX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lCreateButtonY = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lReferenceList")
        lCreateButtonW = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        lCreateButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"") 
        pMain.AddRegion("lCreateButton","lCreateButton", lCreateButtonX, lCreateButtonY, lCreateButtonW, lCreateButtonH )
        pMain.SetControl("lCreateButton",lCreateButton) 
        lCreateButton.OnClick.Add( self.CreateGhost )
          
        # Delete Ghost
        lDeleteButton = FBButton()
        lDeleteButton.Caption = "Delete Ghost"
        lDeleteButton.Hint = "This will delete the ghost object in your file, you do not need to select it from the list"
        lDeleteButton.Look = FBButtonLook.kFBLookPush
        lDeleteButtonX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lDeleteButtonY = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lCreateButton")
        lDeleteButtonW = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        lDeleteButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"") 
        pMain.AddRegion("lDeleteButton","lDeleteButton", lDeleteButtonX, lDeleteButtonY, lDeleteButtonW, lDeleteButtonH )
        pMain.SetControl("lDeleteButton",lDeleteButton) 
        lDeleteButton.OnClick.Add( self.DeleteGhost )  
        
        #Select Ghost Take
        setupTakeTitle = FBLabel()
        setupTakeTitle.Caption = "Select take for ghost:"
        setupTakeTitle.Hint = "This will put the animation from the take you select onto the ghost and merge it into the current take"
        setupTakeTitlex = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        setupTakeTitley = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lDeleteButton")
        setupTakeTitlew = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        setupTakeTitleh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")    
        pMain.AddRegion("setupTakeTitle","setupTakeTitle", setupTakeTitlex,setupTakeTitley,setupTakeTitlew,setupTakeTitleh)   
        pMain.SetControl("setupTakeTitle", setupTakeTitle)  
        
        global lTakeList
        lTakeList = FBList()
        lTakeList.Hint = "Select a animation take you want to use with the ghost"   
        for i in range(len(glo.gTakes)):
            lName = glo.gTakes[i].Name
            lTakeList.Items.append(lName)
            
        lTakeList.Style = FBListStyle.kFBDropDownList
        if len(glo.gTakes) > 1:
            lTakeList.Selected(-1, True)
        lTakeListx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lTakeListy = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"setupTakeTitle")
        lTakeListw = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        lTakeListh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")   
        pMain.AddRegion( "lTakeList", "lTakeList", lTakeListx, lTakeListy, lTakeListw, lTakeListh )
        pMain.SetControl( "lTakeList", lTakeList )  
        
        #Start Location of Ghost Take
        startFrameTitle = FBLabel()
        startFrameTitle.Caption = "Opt. Start frame:"
        startFrameTitle.Hint = "Enter in the frame where you would like the animation to start for the ghost take"
        startFrameTitlex = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        startFrameTitley = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lTakeList")
        startFrameTitlew = FBAddRegionParam(85,FBAttachType.kFBAttachNone,"")
        startFrameTitleh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")    
        pMain.AddRegion("startFrameTitle","startFrameTitle", startFrameTitlex,startFrameTitley,startFrameTitlew,startFrameTitleh)  
        pMain.SetControl("startFrameTitle", startFrameTitle)      
    
        #Start Location of Ghost Take - enter frame box
        global lEdit
        lEdit = FBEdit()
        lEdit.Hint = "If you don't enter in a value it will take the start frame of the current take"
        lEditx = FBAddRegionParam(10,FBAttachType.kFBAttachRight,"startFrameTitle")
        lEdity = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lTakeList")
        lEditw = FBAddRegionParam(55,FBAttachType.kFBAttachNone,"")
        lEdith = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")    
        pMain.AddRegion("lEdit","lEdit", lEditx,lEdity,lEditw,lEdith)     
        pMain.SetControl("lEdit", lEdit)      
        
        global lSelectGhostTakeButton
        lSelectGhostTakeButton = FBButton()
        lSelectGhostTakeButton.Caption = "Create Ghost Take"
        lSelectGhostTakeButton.Hint = "This will put the animation from the take you selected onto the ghost and merge it into the current take"
        lSelectGhostTakeButton.Look = FBButtonLook.kFBLookPush
        lSelectGhostTakeButtonx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lSelectGhostTakeButtony = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"startFrameTitle")
        lSelectGhostTakeButtonw = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        lSelectGhostTakeButtonh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")    
        pMain.AddRegion("lSelectGhostTakeButton","lSelectGhostTakeButton", lSelectGhostTakeButtonx, lSelectGhostTakeButtony, lSelectGhostTakeButtonw, lSelectGhostTakeButtonh )
        pMain.SetControl("lSelectGhostTakeButton",lSelectGhostTakeButton) 
        lSelectGhostTakeButton.OnClick.Add( self.CreateGhostTake )
        
        ########################LEFT SIDE OF UI#############################
    
        # Current Ghost in the Scene Label      
        lCurrentGhost = FBLabel()
        lCurrentGhost.Caption = "Current ghost:"
        lCurrentGhostx = FBAddRegionParam(5,FBAttachType.kFBAttachRight,"setupTitle")
        lCurrentGhosty = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lRefreshUIButton")
        lCurrentGhostw = FBAddRegionParam(250,FBAttachType.kFBAttachNone,"")
        lCurrentGhosth = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"") 
        pMain.AddRegion( "lCurrentGhost", "lCurrentGhost", lCurrentGhostx, lCurrentGhosty, lCurrentGhostw, lCurrentGhosth )   
        pMain.SetControl("lCurrentGhost", lCurrentGhost)  
        
        # Current Ghost in the Scene
        global lCurrentGhostBox
        lCurrentGhostBox = FBEdit()
        lCurrentGhostBox.ReadOnly = True  
        lMover = FBFindModelByLabelName("GHOST:mover")    
        if lMover:
            lProp = lMover.PropertyList.Find('Ghost Type') 
            if lProp:
                lCurrentGhostBox.Text = lProp.Data
                
        lCurrentGhostBoxx = FBAddRegionParam(5,FBAttachType.kFBAttachRight,"lReferenceList")
        lCurrentGhostBoxy = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lCurrentGhost")
        lCurrentGhostBoxw = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        lCurrentGhostBoxh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")    
        pMain.AddRegion("lCurrentGhostBox","lCurrentGhostBox", lCurrentGhostBoxx,lCurrentGhostBoxy,lCurrentGhostBoxw,lCurrentGhostBoxh)    
        pMain.SetControl("lCurrentGhostBox", lCurrentGhostBox)            
        
        # Take(s) Ghost is using Label
        lCurrentGhostTake = FBLabel()
        lCurrentGhostTake.Caption = "Takes with ghost animation:"
        lCurrentGhostTake.Hint = "This is a list of animation with ghost animation in them"
        lCurrentGhostTakex = FBAddRegionParam(5,FBAttachType.kFBAttachRight,"lCreateButton")
        lCurrentGhostTakey = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lCurrentGhostBox")
        lCurrentGhostTakew = FBAddRegionParam(250,FBAttachType.kFBAttachBottom,"")
        lCurrentGhostTakeh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"") 
        pMain.AddRegion( "lCurrentGhostTake", "lCurrentGhostTake", lCurrentGhostTakex, lCurrentGhostTakey, lCurrentGhostTakew, lCurrentGhostTakeh )  
        pMain.SetControl("lCurrentGhostTake", lCurrentGhostTake)  
                  
        # Take Ghost is using 
        global lCurrentGhostTakeBox
        lCurrentGhostTakeBox = FBList()
        lCurrentGhostTakeBox.Style = FBListStyle.kFBVerticalList
        for lTake in glo.gTakes:    
            lProp = lTake.PropertyList.Find('Ghost Take Anim')
            if lProp and lProp.Data != "":           
                lVal = lTake.Name + "(GHOST->" + lProp.Data + ")"
                lCurrentGhostTakeBox.Items.append(lVal) 
                                                                    
        lCurrentGhostTakeBoxx = FBAddRegionParam(5,FBAttachType.kFBAttachRight,"lDeleteButton")
        lCurrentGhostTakeBoxy = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lCurrentGhostTake")
        lCurrentGhostTakeBoxw = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        lCurrentGhostTakeBoxh = FBAddRegionParam(-5,FBAttachType.kFBAttachBottom,"")    
        pMain.AddRegion("lCurrentGhostTakeBox","lCurrentGhostTakeBox", lCurrentGhostTakeBoxx,lCurrentGhostTakeBoxy,lCurrentGhostTakeBoxw,lCurrentGhostTakeBoxh)    
        pMain.SetControl("lCurrentGhostTakeBox", lCurrentGhostTakeBox)        

def Run( show = True ):
    tool = GhostTakeTool()
    
    if show:
        tool.Show()
        
    return tool
