# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'x:\wildwest\dcc\motionbuilder2014\python\RS\Tools\UI\Animation\QT\designer_PropDampingToolbox.ui'
#
# Created: Mon Aug 25 12:01:41 2014
#      by: pyside-uic 0.2.14 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(335, 238)
        self.verticalLayoutWidget_4 = QtGui.QWidget(MainWindow)
        self.verticalLayoutWidget_4.setGeometry(QtCore.QRect(0, 0, 341, 31))
        self.verticalLayoutWidget_4.setObjectName("verticalLayoutWidget_4")
        self.lytBannerWidget = QtGui.QVBoxLayout(self.verticalLayoutWidget_4)
        self.lytBannerWidget.setContentsMargins(0, 0, 0, 0)
        self.lytBannerWidget.setObjectName("lytBannerWidget")
        self.splitter_2 = QtGui.QSplitter(MainWindow)
        self.splitter_2.setGeometry(QtCore.QRect(0, 60, 751, 0))
        self.splitter_2.setOrientation(QtCore.Qt.Vertical)
        self.splitter_2.setObjectName("splitter_2")
        self.splitter = QtGui.QSplitter(self.splitter_2)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setObjectName("splitter")
        self.pushButtonRefresh = QtGui.QPushButton(MainWindow)
        self.pushButtonRefresh.setGeometry(QtCore.QRect(-4, 30, 341, 16))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.pushButtonRefresh.setFont(font)
        self.pushButtonRefresh.setObjectName("pushButtonRefresh")
        self.comboBox = QtGui.QComboBox(MainWindow)
        self.comboBox.setGeometry(QtCore.QRect(90, 70, 211, 22))
        self.comboBox.setObjectName("comboBox")
        self.label = QtGui.QLabel(MainWindow)
        self.label.setGeometry(QtCore.QRect(10, 73, 71, 16))
        self.label.setObjectName("label")
        self.pushButton = QtGui.QPushButton(MainWindow)
        self.pushButton.setGeometry(QtCore.QRect(160, 200, 141, 23))
        self.pushButton.setObjectName("pushButton")
        self.label_2 = QtGui.QLabel(MainWindow)
        self.label_2.setGeometry(QtCore.QRect(10, 113, 71, 16))
        self.label_2.setObjectName("label_2")
        self.spinBox = QtGui.QSpinBox(MainWindow)
        self.spinBox.setGeometry(QtCore.QRect(90, 110, 81, 22))
        self.spinBox.setMinimum(-1000)
        self.spinBox.setMaximum(1000)
        self.spinBox.setObjectName("spinBox")
        self.label_3 = QtGui.QLabel(MainWindow)
        self.label_3.setEnabled(True)
        self.label_3.setGeometry(QtCore.QRect(10, 150, 71, 16))
        self.label_3.setObjectName("label_3")
        self.spinBox_2 = QtGui.QSpinBox(MainWindow)
        self.spinBox_2.setEnabled(True)
        self.spinBox_2.setGeometry(QtCore.QRect(90, 147, 81, 22))
        self.spinBox_2.setMinimum(-1000)
        self.spinBox_2.setMaximum(1000)
        self.spinBox_2.setObjectName("spinBox_2")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonRefresh.setText(QtGui.QApplication.translate("MainWindow", "Refresh", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("MainWindow", "Select a Prop:", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton.setText(QtGui.QApplication.translate("MainWindow", "Set Value", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setToolTip(QtGui.QApplication.translate("MainWindow", "bigger the number the more time between eachd elay, resulting in a slower movement - lower the number the snappier the result", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("MainWindow", "Set Damping:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setToolTip(QtGui.QApplication.translate("MainWindow", "bigger the number the more time between eachd elay, resulting in a slower movement - lower the number the snappier the result", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("MainWindow", "Edit Damping:", None, QtGui.QApplication.UnicodeUTF8))

