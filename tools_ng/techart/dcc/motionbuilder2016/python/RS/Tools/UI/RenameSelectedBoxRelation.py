import os
import inspect

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Config
import RS.Tools.RenameSelectedBoxRelation
import RS.Tools.UI
import RS.Utils.Creation

reload(RS.Tools.RenameSelectedBoxRelation)


class RenameSelectedRelationTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Rename Selected Relation Box', size = [ 200, 125 ], minSize = [ 200, 125 ], helpUrl = 'https://devstar.rockstargames.com/wiki/index.php/Rename_Selected_Relation_Box')
    
    def rename_selected_relation_box(self, control, event):
        selected = False
        for relation in [c for c in FBSystem().Scene.Constraints if c.Is(FBConstraintRelation_TypeInfo())]:
            box = RS.Tools.RenameSelectedBoxRelation.get_first(lambda box: box.Selected, relation.Boxes)
            if box:
                RS.Tools.RenameSelectedBoxRelation.rename_selected_relation_box() 
                selected = True
        if selected == False:
            FBMessageBox( "Warning", "Nothing selected", "OK", None, None )
        
    
    def Create( self, pMain ):  
        lTabControl = FBTabControl()
        lTabControlX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lTabControlY = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")
        lTabControlW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lTabControlH = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        pMain.AddRegion("lTabControl", "lTabControl",lTabControlX,lTabControlY,lTabControlW,lTabControlH)
        pMain.SetControl("lTabControl", lTabControl)     
        
        ## TAB ONE
        lBoxRenameContainer = FBLayout()                
        lBoxRenameContainerX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lBoxRenameContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        lBoxRenameContainerW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lBoxRenameContainerH = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        lBoxRenameContainer.AddRegion("lBoxRenameContainer","lBoxRenameContainer", lBoxRenameContainerX, lBoxRenameContainerY, lBoxRenameContainerW, lBoxRenameContainerH)
        lTabControl.Add("Box Rename",lBoxRenameContainer) 
        
        # Instructions
        lStepOne = FBLabel()
        lStepOne.Caption = "First, select the box to rename."
        lStepOneX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lStepOneY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        lStepOneW = FBAddRegionParam(190,FBAttachType.kFBAttachNone,"")
        lStepOneH = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")  
        lBoxRenameContainer.AddRegion("lStepOne","lStepOne", lStepOneX,lStepOneY,lStepOneW,lStepOneH)      
        lBoxRenameContainer.SetControl("lStepOne",lStepOne)        
    
        lStepTwoButton = FBButton()
        lStepTwoButton.Hint = "Rename Relation Constraint Box"
        lStepTwoButton.Caption = "Rename Relation Constraint Box"
        lStepTwoButton.Look = FBButtonLook.kFBLookPush
        lStepTwoButtonX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lStepTwoButtonY = FBAddRegionParam(25,FBAttachType.kFBAttachTop,"")
        lStepTwoButtonW = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        lStepTwoButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lBoxRenameContainer.AddRegion("lStepTwoButton","lStepTwoButton", lStepTwoButtonX, lStepTwoButtonY, lStepTwoButtonW, lStepTwoButtonH)
        lBoxRenameContainer.SetControl("lStepTwoButton",lStepTwoButton) 
        lStepTwoButton.OnClick.Add(self.rename_selected_relation_box)
        
        ## TAB TWO        
        lHotKeyContainer = FBLayout()                
        lHotKeyContainerX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lHotKeyContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        lHotKeyContainerW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lHotKeyContainerH = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        lHotKeyContainer.AddRegion("lHotKeyContainer","lHotKeyContainer", lHotKeyContainerX, lHotKeyContainerY, lHotKeyContainerW, lHotKeyContainerH)
        lTabControl.Add("Hotkey Setup",lHotKeyContainer)            
        
        lAlternativeLabel = FBLabel()  
        lAlternativeLabel.Caption = "Alt, setup up a hotkey:"
        lAlternativeLabelX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lAlternativeLabelY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        lAlternativeLabelW = FBAddRegionParam(190,FBAttachType.kFBAttachNone,"")
        lAlternativeLabelH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"") 
        lHotKeyContainer.AddRegion( "lAlternativeLabel", "lAlternativeLabel", lAlternativeLabelX, lAlternativeLabelY, lAlternativeLabelW, lAlternativeLabelH )
        lHotKeyContainer.SetControl( "lAlternativeLabel", lAlternativeLabel )   
        
        lPathString = r'{0}\RS\Tools\RenameSelectedBoxRelation.py'.format( RS.Config.Script.Path.Root )
        lPath = FBMemo()
        sl = FBStringList()
        sl.Add (lPathString)       
        lPath.SetStrings(sl) 
        # some bizarre workaround to make the strings show up, by doing something else after the SetStrings :S
        lPath.Hint = "Path to use to set up your hotkey."        
        lPathX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lPathY = FBAddRegionParam(25,FBAttachType.kFBAttachTop,"")
        lPathW = FBAddRegionParam(190,FBAttachType.kFBAttachNone,"")
        lPathH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lHotKeyContainer.AddRegion("lPath","lPath", lPathX, lPathY, lPathW, lPathH)
        lHotKeyContainer.SetControl("lPath",lPath) 

        lTabControl.Add(lHotKeyContainer,lHotKeyContainer)
        
        lTabControl.SetContent(0)

def Run( show = True ):
    tool = RenameSelectedRelationTool()
    
    if show:
        tool.Show()
        
    return tool