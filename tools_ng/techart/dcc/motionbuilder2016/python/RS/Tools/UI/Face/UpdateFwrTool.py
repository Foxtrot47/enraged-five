"""
UI Front end to update FWR files
"""

from PySide import QtGui, QtCore

from RS import Config
from RS.Tools import UI
from RS.Core.Face import Lib


class UpdateFwrTool(UI.QtMainWindowBase):
    """
    Tool UI
    """
    def __init__(self):
        """
        Constructor
        """
        self._toolName = "Update Fwr"
        super(UpdateFwrTool, self).__init__(title=self._toolName, dockable=True)
        self.setupUi()

    def setupUi(self):
        """
        Setup the UI Components
        """
        # layout
        mainLayout = QtGui.QVBoxLayout()
        browseFwrLayout = QtGui.QHBoxLayout()
        browseMovieLayout = QtGui.QHBoxLayout()
        optionsLayout = QtGui.QVBoxLayout()

        # widget
        mainWidget = QtGui.QWidget()
        fwrLabel = QtGui.QLabel("FWR file to update:")
        movLabel = QtGui.QLabel("Movie file to update:")
        self._inputFwrFilePath = QtGui.QLineEdit()
        fwrFileBrowseButton = QtGui.QPushButton("...")

        self._inputMovieFilePath = QtGui.QLineEdit()
        movieFileBrowseButton = QtGui.QPushButton("...")

        updateFileButton = QtGui.QPushButton("Update FWR File")
        self._rotateVideo = QtGui.QCheckBox("Rotate Video")
        self._forceUpdate = QtGui.QCheckBox("Force Update")

        # Configure Widgets
        fwrFileBrowseButton.setMaximumWidth(30)
        movieFileBrowseButton.setMaximumWidth(30)
        fwrLabel.setMaximumHeight(50)
        movLabel.setMaximumHeight(50)

        # assign widgets to layouts
        browseFwrLayout.addWidget(self._inputFwrFilePath)
        browseFwrLayout.addWidget(fwrFileBrowseButton)
        browseMovieLayout.addWidget(self._inputMovieFilePath)
        browseMovieLayout.addWidget(movieFileBrowseButton)
        optionsLayout.addWidget(self._rotateVideo)
        optionsLayout.addWidget(self._forceUpdate)

        mainLayout.addWidget(fwrLabel)
        mainLayout.addLayout(browseFwrLayout)
        mainLayout.addWidget(movLabel)
        mainLayout.addLayout(browseMovieLayout)
        mainLayout.addLayout(optionsLayout)
        mainLayout.addWidget(updateFileButton)

        # connections
        fwrFileBrowseButton.pressed.connect(self._handleBrowseFwrButtonPressed)
        movieFileBrowseButton.pressed.connect(self._handleBrowseMovButtonPressed)
        updateFileButton.pressed.connect(self._handleUpdateButtonPressed)

        mainWidget.setLayout(mainLayout)
        self.setCentralWidget(mainWidget)

    def _handleBrowseFwrButtonPressed(self):
        filePath, notCan = QtGui.QFileDialog.getOpenFileName(self, "FWR file to update", Config.Project.Path.Root, "FWR File (*.fwr)")
        if not notCan:
            return
        self._inputFwrFilePath.setText(filePath)

    def _handleBrowseMovButtonPressed(self):
        filePath, notCan = QtGui.QFileDialog.getOpenFileName(self, "Mov file to update", Config.Project.Path.Root, "Mov File (*.mov)")
        if not notCan:
            return
        self._inputMovieFilePath.setText(filePath)

    def _handleUpdateButtonPressed(self):
        try:
            Lib.updateVideoInFwr(
                             self._inputFwrFilePath.text(),
                             self._inputMovieFilePath.text(),
                             rotate=self._rotateVideo.isChecked(),
                             force=self._forceUpdate.isChecked()
                             )
        except ValueError, err:
            QtGui.QMessageBox.critical(self, "Update FWR Tool", str(err))
            return
        QtGui.QMessageBox.information(self, "Update FWR Tool", "File has been updated!")


def Run(show=True):
    tool = UpdateFwrTool()

    if show:
        tool.show()

    return tool

