import pyfbsdk as mobu
from RS import Globals
from RS.Utils.Scene import Groups


class AmbientRig(object):
    def __init__(self):
        self.faceControls = None
        self.csFaceGroupName = ["Brows", "Eyes", "Mouth", "Lip Controls", "Tongue"]
        self.amientFaceGroupName = ["Brows", "Eyes", "Mouth", "Tongue"] 
        self.setItemList = [setItem.LongName for setItem in Globals.Sets]
        self.groupItemList = [setItem.LongName for setItem in Globals.Groups]
        self.ambient_FacialControls = {
            "Brows": ["Brow_Up_L", "Brow_Up_R", "Brows_Down", "CTRL_C_Brow", "CTRL_L_Brow", "CTRL_R_Brow"],
            "Eyes": ["CTRL_R_Eye", "CTRL_L_Eye", "CTRL_R_Blink", "CTRL_L_Blink", "Squint"],
            "Mouth": ["CTRL_MouthPinch", "CTRL_R_Mouth", "CTRL_Mouth", "CTRL_UpperLip", "CTRL_L_Mouth",
                      "CTRL_UpperLip_Curl", "CTRL_LowerLip", "CTRL_LowerLip_Curl", "CTRL_Jaw", "wide_pose", "FV", "PBM",
                      "ShCh", "W_pose", "open_pose", "CTRL_L_Cheek", "CTRL_R_Cheek", "IH"],
            "Tongue": ["CTRL_Tongue_In_Out", "CTRL_Tongue", "tBack_pose", "tTeeth_pose", "tRoof_pose"]
        }
        self.LateralFaceControls = {
            "Brows": ["outerBrow_R_CTRL", "innerBrow_R_CTRL", "innerBrow_L_CTRL", "outerBrow_L_CTRL"],
            "Eyes": ["eyelidUpperOuter_R_CTRL", "eyelidUpperInner_R_CTRL", "eyelidLowerInner_R_CTRL",
                     "eyelidLowerOuter_R_CTRL", "eyelidLowerInner_L_CTRL", "eyelidLowerOuter_L_CTRL",
                     "eyelidUpperOuter_L_CTRL", "eyelidUpperInner_L_CTRL", "eye_R_CTRL", "eye_C_CTRL", "eye_L_CTRL",
                     "CIRC_blinkR", "CIRC_blinkL", "CIRC_squeezeR", "CIRC_squeezeL",
                     "CIRC_squintInnerDR", "CIRC_squintInnerUR", "CIRC_openCloseDR", "CIRC_openCloseUR",
                     "CIRC_openCloseUL", "CIRC_openCloseDL", "CIRC_squintInnerUL", "CIRC_squintInnerDL"],
            "Mouth": ["nose_R_CTRL", "nose_L_CTRL",
                      "mouth_CTRL", "jaw_CTRL", "CIRC_smileR", "CIRC_smileL", "CIRC_openSmileR", "CIRC_openSmileL",
                      "CIRC_frownR", "CIRC_frownL", "CIRC_scream", "CIRC_lipsNarrowWideR", "CIRC_lipsNarrowWideL",
                      "CIRC_lipsStretchOpenR", "CIRC_lipsStretchOpenL", "CIRC_chinWrinkle", "CIRC_mouthSuckDR",
                      "CIRC_mouthSuckUR", "CIRC_funnelDR", "CIRC_funnelUR", "CIRC_oh", "CIRC_puckerL", "CIRC_puckerR",
                      "CIRC_closeOuterL", "CIRC_closeOuterR", "CIRC_chinRaiseLower", "CIRC_chinRaiseUpper",
                      "CIRC_pressR", "CIRC_pressL", "CIRC_dimpleR", "CIRC_dimpleL", "CIRC_suckPuffR", "CIRC_suckPuffL",
                      "CIRC_lipBite", "CIRC_funnelUL", "CIRC_funnelDL", "CIRC_mouthSuckUL", "CIRC_mouthSuckDL",
                      "CIRC_clench", "CIRC_backFwd", "CIRC_nasolabialFurrowL", "CIRC_nasolabialFurrowR", "cheek_R_CTRL", "cheek_L_CTRL"],
            "Lip Controls": ["lipCorner_R_CTRL", "upperLip_R_CTRL", "upperLip_C_CTRL", "upperLip_L_CTRL", "lipCorner_L_CTRL",
                      "lowerLip_L_CTRL", "lowerLip_C_CTRL", "lowerLip_R_CTRL"],
            "Tongue": ["tongueInOut_CTRL", "tongueMove_CTRL", "tongueRoll_CTRL", "CIRC_rollIn", "CIRC_press",
                       "CIRC_narrowWide"]
        }

    @staticmethod
    def getFacialModel(nameSpace, controllerNamsList):
        control = []
        for ambientBrowsControllerName in controllerNamsList:
            model = mobu.FBFindModelByLabelName("{0}:{1}".format(nameSpace, ambientBrowsControllerName))
            control.append(model)
        return control

    @staticmethod
    def getParentFacialSet(nameSpace):
        setName = "{0}:{0}".format(nameSpace)
        setItemList = [setItem.LongName for setItem in Globals.Sets]
        if setName in setItemList:
            for setItem2 in Globals.Sets:
                if setItem2.LongName == setName:
                    return setItem2
        else:
            parentSet = mobu.FBSet(setName)
            return parentSet

    @staticmethod
    def addFacialSets(nameSpace, groupName, facialParentGroup, controlList):
        faceCtrlGroup = mobu.FBSet("{0}:{1}".format(nameSpace, groupName))
        facialParentGroup.ConnectSrc(faceCtrlGroup)
        for iGeo in controlList:
            try:
                faceCtrlGroup.ConnectSrc(iGeo)
            except:
                pass

    @staticmethod
    def getAmbientFacial(nameSpace, faceControlsType):
        for facialGroup in Groups.GetGroupsFromNamespace(nameSpace):
            if facialGroup.Name == faceControlsType:
                return facialGroup

    def addFacialGroups(self, nameSpace, controlGroup, controlList, faceControlsType):
        facialGroup = self.getAmbientFacial(nameSpace, faceControlsType)
        #print controlGroup
        faceCtrlGroup = mobu.FBGroup("{0}:{1}".format(nameSpace, controlGroup))
        facialGroup.ConnectSrc(faceCtrlGroup)
        for faceControl in controlList:
            try:
                faceCtrlGroup.ConnectSrc(faceControl)
            except:
                pass

    def getFaceControls(self, nameSpace):
       
        faceControls = None
        faceControlsType = None
        controlGroup = None
        if "{0}:Ambient_Facial".format(nameSpace) in self.groupItemList:
            faceControlsType = "Ambient_Facial"
            faceControls = self.ambient_FacialControls
            controlGroup = self.amientFaceGroupName
            
        if "{0}:3Lateral".format(nameSpace) in self.groupItemList:
            faceControlsType = "3Lateral"
            faceControls = self.LateralFaceControls
            controlGroup = self.csFaceGroupName
        return faceControls, faceControlsType, controlGroup

    def setupGroupProperty(self, nameSpace):
        self.faceControls, faceControlsType, controlGroup = self.getFaceControls(nameSpace)
        groupList = ["{0}:{1}".format(nameSpace, setItem) for setItem in self.faceControls.keys()]
        for group in Globals.Groups:
            if group.LongName == "{0}:{1}".format(nameSpace, faceControlsType):
                group.Show = True
                group.Pickable = False
                group.Transformable = True
            if group.LongName in groupList:
                group.Show = True
                group.Pickable = True
                group.Transformable = True

    def setupFacialSets(self, nameSpace):
        self.faceControls, _, controlGroup = self.getFaceControls(nameSpace)
        facialParentSet = self.getParentFacialSet(nameSpace)
        for i in range(len(controlGroup)):
            facePartSet = controlGroup[i]
            #print facePartSet
            controlNames = self.faceControls[controlGroup[i]]
            if "{0}:{1}".format(nameSpace, facePartSet) not in self.setItemList:
                self.addFacialSets(nameSpace, facePartSet, facialParentSet,
                                   self.getFacialModel(nameSpace, controlNames))

    def setupFacialGroups(self, nameSpace):
        self.faceControls, faceControlsType, controlGroup = self.getFaceControls(nameSpace)
        # Check face group exists in the scean eg:(Brows, Eyes, Mouth, Tongue)
        checkFaceGroupExists = all(item in self.groupItemList for item in
                                   ["{0}:{1}".format(nameSpace, setItem) for setItem in controlGroup])
        if not checkFaceGroupExists:
            for i in xrange(len(controlGroup)):
                facePartSet = controlGroup[i]
                controllerNams = self.faceControls[facePartSet]
                if "{0}:{1}".format(nameSpace, facePartSet) not in self.groupItemList:
                    self.addFacialGroups(nameSpace, facePartSet, self.getFacialModel(nameSpace, controllerNams),
                                         faceControlsType)
        self.setupGroupProperty(nameSpace)


def Run(nameSpace):
    faceSet = AmbientRig()
    faceSet.setupFacialGroups(nameSpace)
    faceSet.setupFacialSets(nameSpace)
