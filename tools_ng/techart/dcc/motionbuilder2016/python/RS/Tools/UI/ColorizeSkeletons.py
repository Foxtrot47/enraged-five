from pyfbsdk import *
from pyfbsdk_additions import *

import inspect

import RS.Tools.UI

import RS.Tools.ColorizeSkeletons
import RS.Utils.Creation as cre


class ColorizeSkeletonsTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__(self, 'Colorize Skeletons', size=[200, 116],
                                helpUrl='https://devstar.rockstargames.com/wiki/index.php/Skeleton_Colorization_System')

    def OnEnableColorizationSystem_StateChanged( self, control, event ):
        if control.State == 1:
            control.Caption = 'Disable'
            RS.Tools.ColorizeSkeletons.enable( True )
            
        else:
            control.Caption = 'Enable'
            RS.Tools.ColorizeSkeletons.enable( False )

    def Create( self, mainLayout ):
        
        # Layout IDs.
        mainId = "main"
        
        # Setup layout region.
        x = FBAddRegionParam( 6, FBAttachType.kFBAttachLeft, "" )
        y = FBAddRegionParam( 36, FBAttachType.kFBAttachTop, "" )
        w = FBAddRegionParam( -6, FBAttachType.kFBAttachRight, "" )
        h = FBAddRegionParam( -6, FBAttachType.kFBAttachBottom, "" )
        mainLayout.AddRegion( mainId, mainId, x, y, w, h )
        
        boxLayout = FBVBoxLayout()
        mainLayout.SetControl( mainId, boxLayout )
        
        btnEnable = FBButton()
        btnEnable.Caption = "Enable"
        btnEnable.State = RS.Tools.ColorizeSkeletons.isEnabled()
        btnEnable.Style = FBButtonStyle.kFB2States
        btnEnable.Look = FBButtonLook.kFBLookColorChange
        btnEnable.SetStateColor( FBButtonState.kFBButtonState0, FBColor( 0.37, 0.37, 0.37 ) )
        btnEnable.SetStateColor( FBButtonState.kFBButtonState1, FBColor( 0.6, 0.0, 0.0 ) )
        btnEnable.OnClick.Add( self.OnEnableColorizationSystem_StateChanged )
        
        boxLayout.Add( btnEnable, 30 )

def Run( show = True ):
    #createRsColorizeSkeletonsTool()
    tool = ColorizeSkeletonsTool()
    
    if show:
        tool.Show()
        
    return tool