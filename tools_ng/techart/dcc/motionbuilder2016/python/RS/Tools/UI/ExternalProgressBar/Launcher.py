"""
Description:
    Hooks for launching and manipulating a Qt progress bar that lives outside of Motion Builder

Author:
    David Vega <david.vega@rockstargames.com>
"""

import os
import json

try:
    from RS.Core.Server import Client, Settings
    from RS.Tools.UI.ExternalProgressBar.Widget import ProgressBar as ProgressBar_

except ImportError:
    import Client
    import Settings
    import ProgressBar as ProgressBar_

COMMAND = """
import ProgressBar
import Client, Settings
Client.VISIBLE = True

try: ProgressServer = Client.Connection({portNumber})
except: ProgressServer = Client.Connection({portNumber})

command = (
    "import ProgressBar\\n",
    "from PySide import QtGui\\n",
    "Application = QtGui.QApplication(sys.argv)\\n",
    "Bar = ProgressBar.ProgressBarWidget( text='{text}', value=0, minimum={minimum}, maximum={maximum}, "
    "portNumber={portNumber})\\n",
    "Bar.show()\\n",
    "Bar.activateWindow()\\n"
    "Application.exec_()\\n",
    )

command = "".join(command)
ProgressServer.Sync(Client)
ProgressServer.Sync(ProgressBar)

ProgressServer.Run(command, log="Launching Progress Bar")
ProgressServer.Close()
"""


class ProgressBarController(object):
    """
    Wrapper for interacting with the external progress bar
    """
    def __init__(self, text="", minimum=0, maximum=100, useExistingProgressBar=0):
        """
        Constructor

        Arguments:
            text: (string): text to display on the progress bar
            minimum (int): minimum number the range the progress bar should accept
            maximum (int): maximum number the range the progress bar should accept
        """

        self._minimum = minimum
        self._maximum = maximum
        self._value = 0
        self._text = text
        self.MONITOR_FILE_PATH = os.path.join(os.path.dirname(__file__), "monitor.json")

        self.portNumber = 11091
        # Make sure that a port is open for the
        for number in xrange(11091, 11100):
            if number not in Settings.GetOpenPorts():
                self.portNumber = number
                break
        self.portNumber = useExistingProgressBar or self.portNumber

        self.MONITOR_FILE_PATH = os.path.join(os.path.dirname(__file__),
                                              "monitor{0:03}.json".format(self.portNumber - 11090))

        if not useExistingProgressBar or useExistingProgressBar not in Settings.GetOpenPorts():
            self.Update(text=text, minimum=minimum, maximum=maximum, value=0)
            self.Start()
        else:
            self.__CacheValues()

    def Start(self):
        """ Starts the progress bar """
        # Sometimes when starting the python.exe, it will not connect even though the local server has launched
        # However attempting to connect again after a failed attempt will work
        try:
            self.Server = Client.Connection(self.portNumber-10)

        except:
            self.Server = Client.Connection(self.portNumber-10)

        # Add modules to the external python interpreter so they can be called
        self.Server.Sync(Client)
        self.Server.Sync(ProgressBar_)

        # Add the proper data to the command that is going to be sent to the external python interpreter
        command = COMMAND.format(portNumber=self.portNumber, text=self._text, minimum=self._minimum,
                                 maximum=self._maximum)

        # Send the command
        self.Server.Thread(command, log="Sending signal to start Progress Bar")

    @property
    def Minimum(self):
        """ minimum range value"""
        return self._minimum

    @Minimum.setter
    def Minimum(self, value):
        """ set minimum range value"""
        self.Update(minimum=value)
        self._minimum = value

    @property
    def Maximum(self):
        """ maximum range value"""
        return self._maximum

    @Maximum.setter
    def Maximum(self, value):
        """ maximum range value"""
        self.Update(maximum=value)
        self._maximum = value

    @property
    def Range(self):
        """ range values"""
        return self._minimum, self._maximum

    @Range.setter
    def Range(self, value):
        """ sets range values"""
        self.Update(minimum=value[0], maxiumum=value[1])

    @property
    def Value(self):
        """raw progress bar value"""
        return self._value

    @Value.setter
    def Value(self, value):
        """sets raw progress bar value"""
        if value > self._maximum:
            value = self._maximum
        elif value < self._minimum:
            value = self._minimum

        self.Update(value=value)
        self._value = value

    @property
    def Text(self):
        """ progress bar text"""
        return self._text

    @Text.setter
    def Text(self, text):
        """ sets the progress bar text"""
        self.Update(text=text)
        self._text = text

    @property
    def Percent(self):
        """ progress bar percent"""
        return (self._value - self._minimum) / float(self._maximum - self._minimum) * 100

    @Percent.setter
    def Percent(self, percent):
        """ sets progress bar percent"""
        self.Value = (percent/100.0) * float(self._maximum - self._minimum)

    @property
    def StepValue(self):
        """ value that equals 1% in the progress bar"""
        return (1/float(self._maximum - self._minimum)) * 100

    def AddPercent(self, numberOfSteps=1):
        """
        Updates the progress bar by the given percent
        Argument:
            numberOfSteps : int; amount of steps
        """
        self.Percent += self.StepValue * numberOfSteps

    def setTextAndValue(self, text="", value=0):
        """
        Updates Progress Bar's text and value. This method is meant for being connected to QSignals

        Arguments:
            text (string): text to put on the progress bar
            value (int): value set progress bar to
        """
        self.Update(text=text, value=value)

    def __CacheValues(self, **values):
        """
        Stores the values of the json file in this class instance
        Keyword Arguments:
            **values(key=QProgressBar property : value=new value): properties that you want to set on the
                    QProgressBar that is being run externally.

                    Example: self.Update(text="newtext") updates the text on the progressbar

        """
        # Weird bug in Windows doesn't let you read and write a file with the same file instance when the w+ is passed
        # Instead you have to open and close the file separately for reading and writing
        # TODO: Implement locking mechanism so when multiple processs try to access the json file only one will
        # TODO: will read/write to it at a time.
        data = {}
        if os.path.exists(self.MONITOR_FILE_PATH):
            with open(self.MONITOR_FILE_PATH, "r") as monitorFile:
                data = json.load(monitorFile)

        data.update(values)

        # Cache values
        self.__dict__.update({"_{}".format(key): value for key, value in data.iteritems()})
        return data

    def Update(self, **values):
        """
        Updates the file that the progress bar monitors for changes

        Keyword Arguments:
            **values(key=QProgressBar property : value=new value): properties that you want to set on the
                    QProgressBar that is being run externally.

                    Example: self.Update(text="newtext") updates the text on the progressbar

        """
        data = self.__CacheValues(**values)
        with open(self.MONITOR_FILE_PATH, "w") as monitorFile:
            json.dump(data, monitorFile)


def Run(text="", minimum=0, maximum=100, useExistingProgressBar=0):
        """
        Run method for launching the progress bar

        Arguments:
            text (string): text to display on the progress bar
            minimum (int): minimum number the range the progress bar should accept
            maximum (int): maximum number the range the progress bar should accept
        """
        return ProgressBarController(text=text, minimum=minimum, maximum=maximum,
                                     useExistingProgressBar=useExistingProgressBar)
