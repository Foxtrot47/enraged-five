"""
Model for displaying data of the Moving Cutscene
"""
import pyfbsdk as mobu
from PySide import QtCore

from RS.Utils import ContextManagers, Namespace
from RS.Utils.Scene import Component
from RS.Core.Scene.models.modelItems import ComponentModelItem
from RS.Tools.UI.MovingCutscene.Model import SetupModelItem, MovingCutsceneModelItem
from RS.Tools.UI.MovingCutscene.Model import Components
from RS.Tools.CameraToolBox.PyCoreQt.Models import dragDropModel


class SetupModel(dragDropModel.DragDropModel):
    """
    Model for holding and displaying data of the Moving Cutscene
    """
    ItemRole = QtCore.Qt.UserRole + 3
    DragDropBeginSignal = QtCore.Signal()
    DragDropEndSignal = QtCore.Signal()
    CopySignal = QtCore.Signal(object)

    Copy = 1
    Drop = 2
    Move = 3

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QAbstractModelItem): parent model item
        """
        super(SetupModel, self).__init__(parent=parent)
        self._mimeType = ('moving_cutscene/markers')
        self._acceptableMimeTypes = ('scene/components', 'moving_cutscene/markers')

    def _reset(self):
        """ Clears the model """
        QtCore.QAbstractItemModel.reset(self)
        self.rootItem = SetupModelItem.SetupModelItem()
        self.rootItem.setHeader(("Moving Cutscenes", "Selected", "Active", "Weight", "Plot Skel"))
        self.rootItem.setParent(self)

    def reset(self):
        """ Clears the model and repopulates it """
        self._reset()
        self.refresh()

    def refresh(self):
        """ Populates the model """
        self.setupModelData(self.rootItem)

    def setupModelData(self, root):
        """
        Sets up the model data

        Arguments:
            root (QtGui.QAbstractModelItem): the root item for the model
        """
        [self.setupMovingCutsceneData(movingCutscene) for movingCutscene in Components.MovingCutscene.all()]

    def setupMovingCutsceneData(self, movingCutscene):
        """
        Sets up the moving cutscene items that make up a moving cutscene

        Arguments:
            movingCutscene (Components.MovingCutscene): moving cutscene to get data from
        """
        movingCutscene.setCreate(False)
        item = MovingCutsceneModelItem.MovingCutsceneModelItem(self.rootItem)
        item.setData(None, movingCutscene, item.DataRole)
        self._setupMarkerData(item)
        self.layoutChanged.emit()

    def _setupMarkerData(self, parentItem, depth=0):
        """
        Wraps a Component.Marker object into an item model

        Arguments:
            parentItem (Components.Marker): marker to wrap around
            depth (int): depth of recursion
        """
        parent = parentItem.data(None, SetupModelItem.SetupModelItem.DataRole)
        for child in parent.children():
            item = SetupModelItem.SetupModelItem(parentItem)
            item.setData(None, child, item.DataRole)
            self._setupMarkerData(item, depth+1)

    def moveItems(self, parentIndex, indicies, mimeType, row=None, copy=False):
        """
        Moves an items within the tree model.

        This method is meant to be used within the dropMimeData method

        Arguments:
            parentIndex (QtCore.QModelIndex): the parent model index
            indicies (list): list of QModelIndex that have been dragged to a new parent
            mimeType (string): the mimetype for the QModelIndexes
            row (int): row to copy/drop item too
            copy (boolean): should the items not be removed from their original parents

        Return:
            boolean
        """
        self.DragDropBeginSignal.emit()
        if copy:
            with ContextManagers.ClearModelSelection():
                result = super(SetupModel, self).moveItems(parentIndex, indicies, mimeType, row, copy)
        else:
            result = super(SetupModel, self).moveItems(parentIndex, indicies, mimeType, row, copy)
        self.DragDropEndSignal.emit()
        return result

    def moveItem(self, index, row, parentIndex, mimeType):
        """
        Logic for when an item is dropped within its current parent but in a different spot

        Arguments:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped

        """
        item = index.data(SetupModelItem.SetupModelItem.ItemRole)
        parent = parentIndex.data(SetupModelItem.SetupModelItem.ItemRole) or self.rootItem
        itemIndex = parent.childItems.index(item)

        self.beginMoveRows(parentIndex, itemIndex, itemIndex, parentIndex, row)
        parent.childItems.pop(itemIndex)
        insertRow = (row - (row > itemIndex))
        parent.appendChild(item, insertRow)
        self.endMoveRows()

    def copyItem(self, index, row, parentIndex, mimeType):
        """
        Logic for when an item is dropped and copied over on top of another item

        Arguments:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped

        """
        if "scene/components" in mimeType:
            dataRole = ComponentModelItem.ComponentItem.Component
        else:
            dataRole = SetupModelItem.SetupModelItem.DataRole

        parentItem = self.rootItem
        parent = None
        if parentIndex.isValid():
            parent = parentIndex.data(SetupModelItem.SetupModelItem.DataRole)
            parentItem = parentIndex.data(SetupModelItem.SetupModelItem.ItemRole)

        if isinstance(parent, Components.Marker):
            movingCutscene = parent.movingCutscene()
        else:
            movingCutscene = parent

        data = index.data(dataRole)

        self.beginInsertRows(parentIndex, 0, 0)
        name = data.LongName
        if ":" in name:
            name = ":".join(data.LongName.split(":")[:-1])

        # don't match at first for non-characters, will match on setComponent
        if isinstance(data, mobu.FBCharacter):
            marker = Components.Marker(movingCutscene, name, match=True)
            marker.setComponent(data)
            parent.addChild(marker)
        else:
            marker = Components.Marker(movingCutscene, name, match=False, isNew=True)
            parent.addChild(marker)
            marker.setComponent(data)

        item = SetupModelItem.SetupModelItem(parent=parentItem)
        item.setData(None, marker, SetupModelItem.SetupModelItem.DataRole)

        self.endInsertRows()
        self.CopySignal.emit(marker)

    def dropItem(self, index, row, parentIndex, mimeType):
        """
        Logic for when an item is dropped on top of another item

        Args:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped

        """
        parent = None
        parentItem = self.rootItem
        child = index.data(SetupModelItem.SetupModelItem.DataRole)
        itemRole = SetupModelItem.SetupModelItem.ItemRole

        item = index.data(itemRole)

        if parentIndex.isValid():
            parent = parentIndex.data(SetupModelItem.SetupModelItem.DataRole)
            parentItem = parentIndex.data(SetupModelItem.SetupModelItem.ItemRole)

        if item.parent() is not None:
            self.beginRemoveRows(index.parent(), index.row(), index.row()+1)
            item.parent().removeChild(item)
            child.setParent(None)
            self.endRemoveRows()

        self.beginInsertRows(parentIndex, 0, 0)
        parent.addChild(child)
        parentItem.appendChild(item, row)
        self.endInsertRows()

    def rowCount(self, parent=QtCore.QModelIndex()):
        """
        The number of top level rows this model has
        Args:
            parent (QtCore.QModelIndex()): parent index
        """
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer() or self.rootItem
        return parentItem.childCount()

    def flags(self, index):
        """
        flags that tell the view how to handle the items

        Args:
            index (QtCore.QModelIndex): model index for the current item

        """
        # Invalid indices (open space in the view) are also drop enabled, so you can drop items onto the top level.
        if index.column() == 3 and index.data(QtCore.Qt.EditRole) is not None:
            return QtCore.Qt.ItemIsEnabled|QtCore.Qt.ItemIsEditable
        elif index.column() == 0 and index.data(QtCore.Qt.EditRole) is not None:
            return self._itemFromIndex(index).flags(index)|QtCore.Qt.ItemIsEditable
        elif not index.isValid() or index.column() != 0:
            return QtCore.Qt.ItemIsEnabled

        # For items to be drag and drop enabled they need to return the
        # QtCore.Qt.ItemIsDropEnabled | QtCore.Qt.ItemIsDragEnabled flags

        return self._itemFromIndex(index).flags(index)

    def headerData(self, section, orientation, role):
        """
        Data to display in the header of the view

        Args:
            section (int): column index
            orientation (QtCore.Qt.Orientation): vertical or horizontal headers
            role (QtCore.Qt.Role): role being requested by Qt

        Return:
            object
        """
        if self.rootItem and role == QtCore.Qt.DisplayRole:
            return self.rootItem.headers()[section]

    def updateIndex(self, index):
        """
        Emits that data has changed on the given index

        Args:
            index (QtCore.QModelIndex): model index whose data has changed

        Return:
            None
        """
        return self.dataChanged.emit(index, index)

    def removeIndexes(self, indexDictionary):
        """
        Removes indexes from the model

        Args:
            indexDictionary (dictionary): dictionary of the indexes to remove, the keys are the parent index and the value
                                        is the list of indexes to remove.
        """
        for parentIndex, childIndexes in indexDictionary.iteritems():

            self.beginRemoveRows(parentIndex, 0, len(childIndexes))
            namespaces = []
            for index in childIndexes:
                if not index.isValid():
                    continue
                item = index.internalPointer()
                #item = index.data(SetupModelItem.SetupModelItem.ItemRole)
                data = index.data(SetupModelItem.SetupModelItem.DataRole)
                for child in reversed([data] + data.allChildren()):
                    if isinstance(data, Components.MovingCutscene):
                        namespaces.append(data.namespace())
                parentItem = index.parent().internalPointer()
                if parentItem is None:
                    parentItem = self.rootItem
                data.delete()
                parentItem.removeChild(item)
                
            self.endRemoveRows()
            for namespace in namespaces:
                if Component.IsDestroyed(namespace):
                    continue
                Namespace.Delete(namespace, deleteNamespaces=True)
            
