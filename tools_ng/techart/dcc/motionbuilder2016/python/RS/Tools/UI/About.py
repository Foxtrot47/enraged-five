"""
Information about the tools setup on the current machine

Author:
    Jason Hayes <jason.hayes@rockstarsandiego.com>

Image:
    $TechArt/Tools/About.png

"""
import RS.Config

import RS.Perforce
from RS.Tools.UI import Run

from PySide import QtGui


class AboutTool(QtGui.QWidget):
    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
        """
        super(AboutTool, self).__init__(parent=parent)
        projectInfo = [ ( 'Project', RS.Config.Project.Name ),
                        ( 'Project Root', RS.Config.Project.Path.Root ),
                        ( 'Tools Root', RS.Config.Tool.Path.Root ),
                        ( 'Tools Version', RS.Config.Tool.Version ),
                        ( 'Python Tools Root', RS.Config.Script.Path.Root ),
                        ( 'Tech Art Tools Root', RS.Config.Tool.Path.TechArt ),
                        ( 'Studio', RS.Config.User.Studio ),
                        ( 'User Type', RS.Config.User.Type ),
                        ( 'Outsource', RS.Config.User.IsExternal ),
                        ( 'P4 Connection', RS.Perforce.Connected() )]
        layout = QtGui.QGridLayout()
        for index, info in enumerate(projectInfo):
            label, value = info

            textWidget = QtGui.QLineEdit()
            textWidget.setText(str(value))
            textWidget.setReadOnly(True)

            layout.addWidget(QtGui.QLabel(label), index, 0)
            layout.addWidget(textWidget, index, 1)

        self.setLayout(layout)

@Run(title="About", size=(400, 300))
def Run(show=True):
    return AboutTool()
