'''

 Script Path: RS/Tools/UI/Camera/QT/Camera_Toolbox.py
 
 Written And Maintained By: Mark Harrison-Ball & Kathryn Bodey
 
 Created: 21 November 2013
 
 Description: New UI for Camera Tool, Using PySide

 Icons taken from here: https://www.iconfinder.com/free_icons

'''

import sys

from pyfbsdk import *

from PySide import QtGui
from PySide import QtCore

import RS.Config
import RS.Tools.UI
import RS.Core.Reference.Manager
import RS.Core.Camera.ToolStartup
from RS.Core.Camera import Lib as CamLib, LibGTA5, CamUtils
import RS.Core.Camera.Shaker as shake

reload ( RS.Core.Camera.ToolStartup )
reload ( RS.Config )

#compile .ui file to populate the main window
RS.Tools.UI.CompileUi( '{0}\\RS\\Tools\\UI\\Camera\\QT\\Camera_ToolBox_QT.ui'.format( RS.Config.Script.Path.Root ),'{0}\\RS\\Tools\\UI\\Camera\\QT\\Camera_Toolbox_compiledQTDesigner.py'.format( RS.Config.Script.Path.Root ) )

import RS.Tools.UI.Camera.QT.Camera_Toolbox_compiledQTDesigner as qtCamera

reload ( qtCamera )


################################################################################################
#
# Class for QMainWindow: passing a QMainWindow creation and the class from the compiled ui file
#
################################################################################################
class MainWindow( RS.Tools.UI.QtMainWindowBase, qtCamera.Ui_MainWindow ):

    
    ######################
    # initialiser method
    ######################
    def __init__(self ):

        RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = 'Camera Toolbox' )
        
        
        # launching the info to populate the main window with
        self.setupUi( self )
        
        
        # should be stored in the users folder, use OS environment to get windows temp folder (BOB TODO) 
        self.switchCamera = False
        self.AutoViewPlanes = True        

        # settings = core.camUpdate.camFunctions.Readini( 'C://rs_mbtools.ini' )
        settings = None
        
        if settings:
            switchCamera, AutoViewPlanes = settings
            self.switchCamera = switchCamera
            self.AutoViewPlanes = AutoViewPlanes
        
        
        # attributes
        self.SelectedCam = None # camera name
        self.ActiveCamera = None  # camera node
        self.pShakeListControl = None # holds reference to our shake control
        self.liveupdate = False    
        self.bShowAllCameras = False

 
        # icons for toolbar ( cannot use resource browser due to pathing issues )
        def AddIconToToolSet( iconBaseName, action ):
            icon = QtGui.QIcon()
            icon.addPixmap( QtGui.QPixmap( "{0}/Camera/icons/{1}.png".format( ( RS.Config.Script.Path.ToolImages ), iconBaseName ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
            action.setIcon( icon )            
        
        # MAKE THE IMAGES ON THE CAMERAS BIGGER! #
        AddIconToToolSet( 'camera_new', self.actionCreate_New_Camera )
        AddIconToToolSet( 'camera_newexport', self.actionCreate_Export_Camera )
        AddIconToToolSet( 'camera_update', self.actionUpdate_All_Cameras )
        AddIconToToolSet( 'camera_onlyshow', self.actionShow_Only_Cameras_Debug )            
        AddIconToToolSet( 'camera_delete', self.actionDelete_Selected_Cameras )  
        AddIconToToolSet( 'camera_unlock', self.actionUnlock_Cameras )  
        AddIconToToolSet( 'camera_plot', self.actionPlot_Switcher_to_Export_Camera_2 )
        AddIconToToolSet( 'camera_plotalltakes', self.actionPlot_Switcher_to_All_Takes_Export_Camera )
        AddIconToToolSet( 'camera_switcherselect', self.actionSelect_Switcher )
        AddIconToToolSet( 'plane_showall', self.actionShow_All )
        AddIconToToolSet( 'plane_hideall', self.actionHide_All )
        AddIconToToolSet( 'plane_showselected', self.actionShow_Selected )
        AddIconToToolSet( 'plane_hideselected', self.actionHide_Selected )  
        AddIconToToolSet( 'plane_unlock', self.actionUnlock_Planes )  
              
        # menu event handler binding
        self.actionCreate_New_Camera.triggered.connect( self.CameraCreate )
        self.actionCreate_Export_Camera.triggered.connect( self.ExportCameraCreate )
        self.actionUpdate_All_Cameras.triggered.connect( self.UpdateAllCameras )
        self.actionShow_Only_Cameras_Debug.triggered.connect( self.ShowOnlyCamerasDebug )
        self.actionUnlock_Cameras.triggered.connect( self.UnlockCameras )
        self.actionUnlock_Planes.triggered.connect( self.UnlockPlanes )
        self.actionDelete_Selected_Cameras.triggered.connect( self.DeleteSelectedCameras )
        self.actionSelect_Switcher.triggered.connect( self.SelectSwitcher )        
        self.actionPlot_Switcher_to_Export_Camera_2.triggered.connect( self.PlotSwitcherToExportCamera )        
        self.actionPlot_Switcher_to_All_Takes_Export_Camera.triggered.connect( self.PlotSwitcherToExportCameraAllTakes )        
        self.actionRemove_Non_Switcher_Cameras.triggered.connect( self.RemoveNonSwitcherCameras )        
        self.actionShow_All.triggered.connect( self.ShowAllPlanes )        
        self.actionHide_All.triggered.connect( self.HideAllPlanes )        
        self.actionShow_Selected.triggered.connect( self.ShowSelectedPlanes )        
        self.actionHide_Selected.triggered.connect( self.HideSelectedPlanes )        
        self.actionEnable_Auto_Switch_View_to_Camera.triggered.connect( self.EnableAutoSwitchViewToCamera )        
        self.actionEnable_Auto_Show_Camera_Planes.triggered.connect( self.EnableAutoShowCameraPlanes )               
        
        # create spinboxes
        self.focusSpinBox = QtGui.QSpinBox( self.tab )
        self.focusSpinBox.setGeometry( QtCore.QRect( 110, 140, 61, 22 ) ) 
        self.focusSpinBox.dofPropName = 'FOCUS (cm)'
        
        self.circleofConfusionSpinBox = QtGui.QSpinBox( self.tab )
        self.circleofConfusionSpinBox.setGeometry( QtCore.QRect( 250, 140, 61, 22 ) ) 
        self.circleofConfusionSpinBox.dofPropName = 'CoC'
        
        self.motionBlurSpinBox = QtGui.QSpinBox( self.tab )
        self.motionBlurSpinBox.setGeometry( QtCore.QRect( 390, 140, 61, 22 ) ) 
        self.motionBlurSpinBox.dofPropName = 'Motion Blur'
        
        self.farPlaneSpinBox = QtGui.QSpinBox( self.tab )
        self.farPlaneSpinBox.setGeometry( QtCore.QRect( 530, 140, 61, 22 ) ) 
        self.farPlaneSpinBox.dofPropName = 'Far Strength Override'
        
        self.nearPlaneSpinBox = QtGui.QSpinBox( self.tab )
        self.nearPlaneSpinBox.setGeometry( QtCore.QRect( 670, 140, 61, 22 ) ) 
        self.nearPlaneSpinBox.dofPropName = 'Near Strength Override'


        # create custom dials, set properties and set attributes to its spinboxes
        self.focusDial = CustomDial( self.tab )
        self.focusDial.setWrapping( True )
        #self.focusDial.setMaximum( 5000 )
        self.focusDial.setGeometry( QtCore.QRect( 65, 40, 141, 90 ) )
        self.focusDial.spinBox = self.focusSpinBox  
        
        self.circleofConfusionDial = CustomDial( self.tab )
        self.circleofConfusionDial.setWrapping( True )
        #self.circleofConfusionDial.setMinimum( 1 )
        #self.circleofConfusionDial.setMaximum( 15 )
        self.circleofConfusionDial.setGeometry( QtCore.QRect( 205, 40, 141, 90 ) )
        self.circleofConfusionDial.spinBox = self.circleofConfusionSpinBox

        self.motionBlurDial = CustomDial( self.tab )
        self.motionBlurDial.setWrapping( True )
        #self.motionBlurDial.setMaximum( 1 )
        self.motionBlurDial.setGeometry( QtCore.QRect( 345, 40, 141, 90 ) )  
        self.motionBlurDial.spinBox = self.motionBlurSpinBox 
          
        self.farPlaneDial = CustomDial( self.tab )
        self.farPlaneDial.setWrapping( True )
        #self.farPlaneDial.setMinimum( -1 )
        #self.farPlaneDial.setMaximum( 1 )        
        self.farPlaneDial.setGeometry( QtCore.QRect( 485, 40, 141, 90 ) ) 
        self.farPlaneDial.spinBox = self.farPlaneSpinBox   
        
        self.nearPlaneDial = CustomDial( self.tab )
        self.nearPlaneDial.setWrapping( True )
        #self.nearPlaneDial.setMinimum( -1 )
        #self.nearPlaneDial.setMaximum( 1 ) 
        self.nearPlaneDial.setGeometry( QtCore.QRect( 635, 40, 141, 90 ) ) 
        self.nearPlaneDial.spinBox = self.nearPlaneSpinBox 

                
        # change dial number as the dial moves
        self.focusDial.valueChanged.connect( self.OnDial_ValueChanged )
        self.focusSpinBox.valueChanged.connect( self.DOFProperty_ValueChange )

        self.circleofConfusionDial.valueChanged.connect( self.OnDial_ValueChanged )
        self.circleofConfusionSpinBox.valueChanged.connect( self.DOFProperty_ValueChange ) 
        
        self.motionBlurDial.valueChanged.connect( self.OnDial_ValueChanged )
        self.motionBlurSpinBox.valueChanged.connect( self.DOFProperty_ValueChange ) 
        
        self.farPlaneDial.valueChanged.connect( self.OnDial_ValueChanged )
        self.farPlaneSpinBox.valueChanged.connect( self.DOFProperty_ValueChange )        

        self.nearPlaneDial.valueChanged.connect( self.OnDial_ValueChanged )
        self.nearPlaneSpinBox.valueChanged.connect( self.DOFProperty_ValueChange )     


        # delete floating planes for export cam
        RS.Core.Camera.ToolStartup.CleanUpParentLessPlanes()
        
        
        # delete switcher constraints
        RS.Core.Camera.ToolStartup.CheckCameraSwitcherConstraints()
        
        
        # select camera in scene when the item is selected in the ui list
        self.cameraListCtrl.itemClicked.connect( self.OnSelectCameras_ItemClicked )        
        
        
        # refresh camera view if the view options change
        self.cameraComboBox.currentIndexChanged.connect( self.RefreshCamerasList )   
        
        
        # add latest scene cameras to the ui list
        self.RefreshCamerasList()          


    ########################
    # Menu: events
    ########################
    
    def CameraCreate( self ):
        # core.camUpdate.CreateCustomCamera()
        CamLib.MobuCamera.CreateRsCamera()
        self.RefreshCamerasList()
        
    def ExportCameraCreate( self ):
        # core.camUpdate.CreateExportCamera()
        CamLib.MobuCamera.CreateExportCamera()
        self.RefreshCamerasList()
    
    def UpdateAllCameras( self ):
        # core.camUpdate.rs_updateCameras()
        self.RefreshCamerasList()
    
    def ShowOnlyCamerasDebug( self ):
        # core.camUpdate.showOnlyCameras()
        LibGTA5.camUpdate.showOnlyCameras()
    
    def UnlockCameras( self ):
        # core.camUpdate.lockCameras
        pass
    
    def UnlockPlanes( self ):
        # core.camUpdate.lockPlanes
        pass
    
    def DeleteSelectedCameras( self ):
        # cameraNode = core.camUpdate.rs_SelectCameraByName( self.SelectedCam, switchView = self.switchCamera )
        # core.camUpdate.DeleteCamera( cameraNode, RemoveCam = True )
        cam = CamLib.MobuCamera(self.SelectedCam)
        cam.DeleteCamera()
        self.RefreshCamerasList()
    
    def SelectSwitcher( self ):
        # core.camUpdate.toggletSwitcherCamera( True )
        pass
    
    def PlotSwitcherToExportCamera( self ):
        # plo.rs_PlotCameras.plotCameras()
        CamUtils.PlotSwitcherToExportCamera()
    
    #not working
    def PlotSwitcherToExportCameraAllTakes( self ):
        result = FBMessageBox( "Warning" , ( "This will plot the camera switcher to all takes!" ), "Cancel", "Continue" )
        if result == 2:
            # plot.rs_PlotCameras.plotCameras( bSilent = False, bPlotAllTakes = True )
            CamUtils.PlotSwitcherToExportCamera()
    
    def RemoveNonSwitcherCameras( self ):
        # core.camUpdate.rs_RemoveNonSwitcherCameras()
        CamLib.Manager.DeleteNonSwitcherCams()
    
    def ShowAllPlanes( self ):
        # core.camUpdate.rs_ShowHideCameraPlanes( True, False, False )
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(True, False, False)
        
    def HideAllPlanes( self ):
        # core.camUpdate.rs_ShowHideCameraPlanes( False, False, False )
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, False, False)
        
    def ShowSelectedPlanes( self ):
        # core.camUpdate.rs_ShowHideCameraPlanes( True, True, False )
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(True, True, False)

    def HideSelectedPlanes( self ):
        # core.camUpdate.rs_ShowHideCameraPlanes( False, True, False )
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, True, False)
        
    def EnableAutoSwitchViewToCamera( self ):
        pass
    
    def EnableAutoShowCameraPlanes( self ):
        pass

    
    ########################
    # Shake: events
    ########################
    
    def ShakeWeight(self, pControl, pEvent):
        self.ShakeLabelControl.Caption = "Shake Phase: %s" % int(pControl.Value) +"%"

        shake.HandShake.UpdateShakeWeight(pControl.Value)
        # DO some crazy SHit
        if self.SelectedCam  != None:
            if (self.liveupdate):
                shake.HandShake.AddShakeSelected(self.SelectedCam )    
    
    def ShakeSelect(self, pControl, pEvent):            
        shake.HandShake.LoadShakeFile(pControl.Items[pControl.ItemIndex])
        
    ##############################
    # Camera List Functions
    ##############################  
    
    # clears the camera list
    def ClearCameraListControl( self ):
        numRows = self.cameraListCtrl.count()
        
        for row in range( numRows ):
            self.cameraListCtrl.takeItem( row )
    
    # builds the camera list
    def RefreshCamerasList( self ):
        FBSystem().CurrentTake.SetCurrentLayer(0)

        self.ClearCameraListControl()

        # core.camUpdate.camFunctions.UpdateActiveSwitcherCameras()
        
        if self.cameraComboBox.currentText() == 'Switcher Cameras':
            # for iCam in core.camUpdate.camFunctions.SwitcherCameras:
            switcherCams = [cam.camera for cam in CamLib.Manager.GetSwitcherCameras()]
            for iCam in switcherCams:
                self.cameraListCtrl.addItem( iCam.Name )
        else:
            for iCam in glo.gCameras:
                if not iCam.SystemCamera: 
                    self.cameraListCtrl.addItem( iCam.Name )
            
    # gets select camera name
    def GetSelectedCameras( self ):
        cameras = []
        items = self.cameraListCtrl.selectedItems()

        for item in items:
            cameras.append( str( item.text() ) )

        return cameras

    # links selected item name with camera scene node
    def OnSelectCameras_ItemClicked( self, item ):
        cameras = self.GetSelectedCameras()

        if cameras:
            self.SelectedCam = cameras[ 0 ]
            # iCameraNode = core.camUpdate.rs_SelectCameraByName( cameras[ 0 ], switchView = self.switchCamera, showPlanes = self.AutoViewPlanes )
            iCameraNode = LibGTA5.camUpdate.rs_SelectCameraByName(cameras[0], switchView=self.switchCamera, showPlanes=self.AutoViewPlanes)

            if iCameraNode != None:
                #Add check to enable constraint for camera planes in the scene - 1331929
                for i in range(iCameraNode.GetSrcCount()):
                    if iCameraNode.GetSrc(i).ClassName() == "FBConstraintRelation":
                        if iCameraNode.GetSrc(i).Active == False:
                            iCameraNode.GetSrc(i).Active = True    

                self.ActiveCamera = iCameraNode

                self.UpdateDOFSpinBoxes()

        
    ########################
    # DOF Control Functions
    ########################
    
    # set value of spinbox to match dial value
    def OnDial_ValueChanged( self, val ):

        widget = self.sender()
        
        if hasattr( widget, 'spinBox' ):
            widget.spinBox.setValue( val )     
    
    # set value of custom property
    def SetDOFPropertyValue( self, propertyName, val ):
        if self.ActiveCamera:
            camProperty = self.ActiveCamera.PropertyList.Find( propertyName )
            
            if camProperty:
                camProperty.Data = val
                
    # get value of custom property            
    def GetDOFPropertyValue( self, propertyName ):
        if self.ActiveCamera:
            camProperty = self.ActiveCamera.PropertyList.Find( propertyName )
            
            if camProperty:
                return camProperty.Data
    
    # set custom property value to match spinbox value
    def DOFProperty_ValueChange( self, val ): 
               
        if self.ActiveCamera != None:
            
            widget = self.sender()
            
            if hasattr( widget, 'dofPropName' ):
                self.SetDOFPropertyValue( widget.dofPropName, val )

    # update spinbox and dial values as the user changes the selected camera
    # disable any where custom properties are not available on active camera
    def UpdateDOFSpinBoxes( self ):

        def DialSpinValue( self, dial, spinbox ):
            
            if self.GetDOFPropertyValue( spinbox.dofPropName ) != None:
                dial.setEnabled( True )
                spinbox.setEnabled( True )
                spinbox.setReadOnly( False )
                spinbox.setValue( self.GetDOFPropertyValue( spinbox.dofPropName ) )
                dial.setValue( self.GetDOFPropertyValue( spinbox.dofPropName ) )            
            else:
                dial.setEnabled( False )
                spinbox.setEnabled( False )
                spinbox.setReadOnly( True )  
                spinbox.setValue( 0 )
                dial.setValue( 0 ) 
                
        if self.ActiveCamera != None:
            DialSpinValue( self, self.focusDial, self.focusSpinBox )
            DialSpinValue( self, self.circleofConfusionDial, self.circleofConfusionSpinBox )
            DialSpinValue( self, self.motionBlurDial, self.motionBlurSpinBox )
            DialSpinValue( self, self.nearPlaneDial, self.nearPlaneSpinBox )
            DialSpinValue( self, self.farPlaneDial, self.farPlaneSpinBox )
          
    
    ########################
    # Shake Functions
    ########################

    def rs_ShakeRefresh( self, shakeList ):
        
        shakeList.Items.removeAll() 
        
        dirList = os.listdir( shake.HandShake.path )
        
        dirList.sort()
        
        # Bit of a custom hack for now
        customList = ['Light.csf', 'Medium.csf','Heavy.csf']
        
        for i in dirList:
            if i not in customList:
                customList.append( i )

        for fname in customList:
            shakeList.Items.append( fname )   


########################
# Custom Dial Class
########################        
class CustomDial ( QtGui.QDial ):

    def __init__( self, *args, **kwargs ):    
        
        QtGui.QDial.__init__( self, *args, **kwargs )

        self.__dialImagePath = '{0}\\dial.png'.format( RS.Config.Script.Path.ToolImages )
        self.__dialImage = QtGui.QImage( self.__dialImagePath )
        self.__labelHeight = 24

    def paintEvent( self, event ):
        
        painter = QtGui.QPainter()
        
        painter.begin( self )
        
        widgetWidthCenter = self.width() * 0.5
        widgetHeightCenter = self.height() * 0.5
        
        painter.translate( widgetWidthCenter, widgetHeightCenter )
        
        painter.rotate( ( float( self.value() ) / float( self.maximum() ) ) * 360.0 )        

        if self.height() <= self.width():
            img = self.__dialImage.scaledToHeight( self.rect().height(), mode = QtCore.Qt.TransformationMode.SmoothTransformation )
            
        else:
            img = self.__dialImage.scaledToWidth( self.rect().width(), mode = QtCore.Qt.TransformationMode.SmoothTransformation )
        
        imageWidthCenter = img.width()
        imageHeightCenter = img.height()
        
        imageWidthOffset = -( imageWidthCenter * 0.50 )
        imageHeightOffset = -( imageHeightCenter * 0.50 )
                
        painter.drawImage( QtCore.QPoint( imageWidthOffset, imageHeightOffset ), img )

        painter.end()
        

########################
# Launch UI
########################
form = MainWindow()
form.show()
