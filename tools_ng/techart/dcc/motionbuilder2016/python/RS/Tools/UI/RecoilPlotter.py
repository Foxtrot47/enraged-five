'''
Author          Ross George

Description     Plots from one model to another for ploting down recoil to character bones.
'''

from pyfbsdk import *
from pyfbsdk_additions import *

import sys

import RS.Utils.Creation
import RS.Tools.UI

class RecoilPlotterTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Recoil Plotter', size = [ 425, 145 ])
    
    def Plot(self, control, event):
	
	try:
	    if len( self.TargetModelProperty ) != 1:
		raise Exception( "Please select a target model." )	    
	    if len( self.SourceModelProperty ) != 1:
		raise Exception( "Please select a source model." )	    
	    
	    targetModel = self.TargetModelProperty[0]
	    sourceModel = self.SourceModelProperty[0]

	    if type(targetModel) not in [FBModel, FBModelCube, FBModelMarker, FBModelNull, FBModelRoot, FBModelSkeleton, FBModelPlane ]:
		raise Exception("Target object is not a model." )
	    if type(sourceModel) not in [FBModel, FBModelCube, FBModelMarker, FBModelNull, FBModelRoot, FBModelSkeleton, FBModelPlane ]: 
		raise Exception( "Error", "Source object is not a model." )
	    if sourceModel == targetModel:
		raise Exception( "Source and target object are the same." )
	    
	    #Get our player
	    player = FBPlayerControl()
	    
	    #Get take timespan
	    timeSpan = RS.Globals.System.CurrentTake.LocalTimeSpan
	
	    #Go start of take
	    startTime = timeSpan.GetStart()
	    stopTime = timeSpan.GetStop()
	    player.Goto( timeSpan.GetStart() )
	    
	    #Get the starting global matrix of the source model - this will be used as our offset for rest of plot
	    initialSourceMatrix = FBMatrix()
	    sourceModel.GetMatrix( initialSourceMatrix )    
		
	    #Go through each frame
	    frameTime = FBTime()
	    for frameIndex in range ( startTime.GetFrame(), stopTime.GetFrame() + 1 ):
		
		#Create frame time and goto it
		frameTime.SetFrame(frameIndex)
		player.Goto( frameTime )
		
		#Get the current global matrix
		currentSourceMatrix = FBMatrix()
		sourceModel.GetMatrix( currentSourceMatrix )
		
		#Get the local matrix from this and the initial matrix
		currentTargetMatrix = FBMatrix()
		FBGetLocalMatrix( currentTargetMatrix, initialSourceMatrix, currentSourceMatrix )
	
		#Apply and evaluate
		targetModel.SetMatrix( currentTargetMatrix )
		RS.Globals.Scene.Evaluate()
		
		#Key translation and rotation
		targetModel.Translation.SetAnimated( True )
		targetModel.Rotation.SetAnimated( True )
		for prop in targetModel.Translation, targetModel.Rotation:
		    prop.GetAnimationNode().Nodes[0].KeyAdd( frameTime, prop.Data[0] )
		    prop.GetAnimationNode().Nodes[1].KeyAdd( frameTime, prop.Data[1] )
		    prop.GetAnimationNode().Nodes[2].KeyAdd( frameTime, prop.Data[2] )                                
	
	    #Go back to start
	    player.Goto( startTime )
	except Exception, e:
	    FBMessageBox( "Error", e.message, "Ok" ) 
    
    def Create(self, ui):
	
	#self.BackgroundNull = FBModelNull("TEST")
	
	ui.PropertyCreate( "SourceModel", FBPropertyType.kFBPT_object, "", False, False, None )
	ui.PropertyCreate( "TargetModel", FBPropertyType.kFBPT_object, "", False, False, None )
	
	self.SourceModelProperty = ui.PropertyList.Find("SourceModel")
	self.SourceModelProperty.SetSingleConnect(True)

	self.TargetModelProperty = ui.PropertyList.Find("TargetModel")
	self.TargetModelProperty.SetSingleConnect(True)
	
        al = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        ui.AddRegion("main","main", al, at, ar, ab)
        manualLyt = FBVBoxLayout()
        ui.SetControl("main",manualLyt)
        
	self.SourceModelEdit = FBEditProperty()
	self.SourceModelEdit.Caption = 'Source Model:'
	self.SourceModelEdit.Property = self.SourceModelProperty     

        al = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(35 ,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(400,FBAttachType.kFBAttachNone,"")
        an = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        ui.AddRegion("SourceModelEdit","SourceModelEdit", al, at, ar, an)
        ui.SetControl("SourceModelEdit", self.SourceModelEdit)        
	
	self.TargetModelEdit = FBEditProperty()
	self.TargetModelEdit.Caption = 'Target Model:'
        self.TargetModelEdit.Property = self.TargetModelProperty 
	
	al = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
	at = FBAddRegionParam(65 ,FBAttachType.kFBAttachTop,"")
	ar = FBAddRegionParam(400,FBAttachType.kFBAttachNone,"")
	an = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        ui.AddRegion("TargetModelEdit","TargetModelEdit", al, at, ar, an)
        ui.SetControl("TargetModelEdit",self.TargetModelEdit)         

	self.PlotButton = FBButton()
	self.PlotButton.Caption = 'Plot'
	self.PlotButton.OnClick.Add(self.Plot)       	
        
        al = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(95 ,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(400,FBAttachType.kFBAttachNone,"")
        an = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        ui.AddRegion("PlotButton","PlotButton", al, at, ar, an)
        ui.SetControl("PlotButton",self.PlotButton)    


def Run( show = True ):
    tool = RecoilPlotterTool()
    
    if show:
        tool.Show()
        
    return tool