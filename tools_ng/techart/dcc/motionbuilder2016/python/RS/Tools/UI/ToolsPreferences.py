"""
Tools Preferences Dialog so the user can tweak tools to their likings
"""
import time

import pyfbsdk as mobu

from PySide import QtGui

from RS.Tools import UI
from RS.Core.Menus import Cutscene
from RS.Utils import Exception, KeepDefaultSaveSettings, Validation, UserPreferences, Scene
from RS.Tools.UI.OpenFile.Configure import Setup
import RS.Core.Audio.AudioTools as AudioTools
from RS.Core.Camera import CamUtils


class ToolsPreferencesTool(UI.QtBannerWindowBase):
    """
    The Tools Preferences Dialog
    """
    HELP_URL = ""

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(ToolsPreferencesTool, self).__init__(title='Tools Preferences',
                                                         size=(250, 200),
                                                         helpUrl=self.HELP_URL,
                                                         parent=parent,
                                                         dockable=True)
        self._app = mobu.FBApplication()
        self.setupUi()

    def setupUi(self):
        """
        Method to setup the UI and create the connections
        """
        # Layouts
        layout = QtGui.QVBoxLayout()

        # Widgets
        mainWidget = QtGui.QWidget()
        self._errorEmailsCB = QtGui.QCheckBox("Send Error Emails")
        self._validationSystemCB = QtGui.QCheckBox("Enable Validation System")
        self._cutsceneLoadMenuCB = QtGui.QCheckBox("Enable Cutscene Load Menu")
        self._saveCameraMenuCB = QtGui.QCheckBox("Enable Save Camera Menu")
        self._defaultSaveSettingsCB = QtGui.QCheckBox("Keep Default Options in Save Settings")
        self._shelfOnStartupCB = QtGui.QCheckBox("Show Shelf on startup")
        self._disableDBLoggingCB = QtGui.QCheckBox("Disable Wildwest Database logging")
        self._enableProjectMenuColorCB = QtGui.QCheckBox("Enable Project Menu Color")
        self._enableCustomOpenFileDialogCB = QtGui.QCheckBox("Enable Custom Open File Dialog")
        self._enableAutoAudioCB = QtGui.QCheckBox("Enable Automatic Audio")
        self._enableSwitcherShortcuts = QtGui.QCheckBox("Enable Switcher Shortcuts")
        self._enableStager = QtGui.QCheckBox("Enable Stager Menu (Testing Only)")
        self._enableEmbedMedia = QtGui.QCheckBox("Disable embed Media on start up")

        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)

        # Widgets to Layouts
        for item in [self._errorEmailsCB, self._validationSystemCB, self._cutsceneLoadMenuCB,
                     self._saveCameraMenuCB, self._defaultSaveSettingsCB, self._shelfOnStartupCB,
                     self._disableDBLoggingCB, self._enableProjectMenuColorCB, self._enableCustomOpenFileDialogCB,
                     self._enableAutoAudioCB, self._enableSwitcherShortcuts, self._enableStager,self._enableEmbedMedia]:
            layout.addWidget(item)
        layout.addItem(spacerItem)

        # Connections
        self._errorEmailsCB.stateChanged.connect(self._handleErrorEmailsChangeState)
        self._validationSystemCB.stateChanged.connect(self._handleValidationSystemChangeState)
        self._cutsceneLoadMenuCB.stateChanged.connect(self._handleCutsceneLoadMenuChangeState)
        self._saveCameraMenuCB.stateChanged.connect(self._handleSaveCameraMenuChangeState)
        self._defaultSaveSettingsCB.stateChanged.connect(self._handleDefaultSaveSettingsChangeState)
        self._shelfOnStartupCB.stateChanged.connect(self._handleShelfOnStartupChangeState)
        self._disableDBLoggingCB.stateChanged.connect(self._handleDisableDBLogging)
        self._enableProjectMenuColorCB.stateChanged.connect(self._enableProjectMenuColorChangeState)
        self._enableCustomOpenFileDialogCB.stateChanged.connect(self._enableCustomOpenFileDialogChangeState)
        self._enableAutoAudioCB.stateChanged.connect(self._enableAutoAudioChangeState)
        self._enableSwitcherShortcuts.stateChanged.connect(self._handleSwitcherShortcutChangeState)
        self._enableStager.stateChanged.connect(self._handleEnableStagerChangeState)
        self._enableEmbedMedia.stateChanged.connect(self._enableEmbedMediaChangeState)
        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)

    def show(self):
        """
        Re-Implemented

        Show the populated dialog
        """
        self._populateSettings()
        return super(ToolsPreferencesTool, self).show()

    def _populateSettings(self):
        """
        Internal Method
        """
        self._errorEmailsCB.setChecked(Exception.ExceptionHook.sendExceptions)

        self._validationSystemCB.setChecked(Validation.ENABLE_VALIDATION)

        cutSceneVal = UserPreferences.__Readini__("menu", "Enable Cutscene Menu", True)
        if cutSceneVal is None:
            cutSceneVal = True
        self._cutsceneLoadMenuCB.setChecked(cutSceneVal)

        camSaveVal = UserPreferences.__Readini__("menu", "Enable Save Cameras Menu", True)
        if camSaveVal is None:
            camSaveVal = True
        self._saveCameraMenuCB.setChecked(camSaveVal)

        self._defaultSaveSettingsCB.setChecked(UserPreferences.__Readini__("menu", "KeepDefaultSaveSettings"))

        self._shelfOnStartupCB.setChecked(UserPreferences.__Readini__("shelftastic", "OpenShelfOnStartup"))

        self._disableDBLoggingCB.setChecked(UserPreferences.__Readini__("menu", "Disable WW DB Logging"))

        self._enableProjectMenuColorCB.setChecked(UserPreferences.__Readini__("menu", "Enable Project Menu Color",
                                                                              notFoundValue=True))

        self._enableCustomOpenFileDialogCB.setChecked(UserPreferences.__Readini__("menu",
                                                                                  "Enable Custom Open File Dialog",
                                                                                  notFoundValue=True))

        self._enableAutoAudioCB.setChecked(bool(UserPreferences.__Readini__("audioTools", "audioDest",
                                                                            notFoundValue=0)))
        
        self._enableSwitcherShortcuts.setChecked(bool(UserPreferences.__Readini__("shortcuts", "Enable Switcher Shortcuts",
                                                                                  notFoundValue=False)))

        self._enableStager.setChecked(UserPreferences.__Readini__("menu", "Enable Stager"))

        embedMediaVal = UserPreferences.__Readini__("menu", "Embed Media Overload", True,notFoundValue=None)
        if embedMediaVal is None:
            embedMediaVal = True
        self._enableEmbedMedia.setChecked(embedMediaVal)

    def _enableEmbedMediaChangeState(self):
        """
        Internal Method

        Handle thedisable EmbedMedia checkbox change
        """
        embedMediaVal = self._enableEmbedMedia.isChecked()
        UserPreferences.__Saveini__("menu", "Embed Media Overload", self._enableEmbedMedia.isChecked())

        applicationConfigFile = mobu.FBConfigFile("@Application.txt")
        if applicationConfigFile:
            if embedMediaVal == True:
                applicationConfigFile.Set("SaveLoad", "EmbedMedias", 'No')
            else:
                applicationConfigFile.Set("SaveLoad", "EmbedMedias", 'Yes')

    def _handleErrorEmailsChangeState(self):
        """
        Internal Method

        Handle the Error Email checkbox change
        """
        Exception.ExceptionHook.sendExceptions = self._errorEmailsCB.isChecked()
        UserPreferences.__Saveini__("Exception", "Send Exceptions", self._errorEmailsCB.isChecked())
        UserPreferences.__Saveini__("Exception", "Set Date", str(time.strftime("%d/%m/%Y")))

    def _handleDisableDBLogging(self):
        """
        Internal Method

        Handles the Database loggign checkbox
        """

        UserPreferences.__Saveini__("menu", "Disable WW DB Logging", self._disableDBLoggingCB.isChecked())

    def _handleValidationSystemChangeState(self):
        """
        Internal Method

        Handle the Validation system checkbox change
        """
        Validation.ENABLE_VALIDATION = self._validationSystemCB.isChecked()

    def _handleCutsceneLoadMenuChangeState(self):
        """
        Internal Method

        Handle the Cutscene Load menu checkbox change
        """
        UserPreferences.__Saveini__("menu", "Enable Cutscene Menu", self._cutsceneLoadMenuCB.isChecked())
        Cutscene.ToggleLoadCutsceneMenu()

    def _handleSaveCameraMenuChangeState(self):
        """
        Internal Method

        Handle the Save Camera Menu checkbox change
        """
        UserPreferences.__Saveini__("menu", "Enable Save Cameras Menu", self._saveCameraMenuCB.isChecked())
        Cutscene.ToggleSaveCamerasMenu()

    def _handleDefaultSaveSettingsChangeState(self):
        """
        Internal Method

        Handle the default save settings checkbox change state
        """
        UserPreferences.__Saveini__("menu", "KeepDefaultSaveSettings", self._defaultSaveSettingsCB.isChecked())
        if self._defaultSaveSettingsCB.isChecked():
            KeepDefaultSaveSettings.SetSaveSettingToDefaultOptions()
            # Changing the Save Options back to the Default
            self._app.OnFileSaveCompleted.Add(self.rs_onFileSaveCompleted)
        else:
            # Changing the Save Options back to the Default
            self._app.OnFileSaveCompleted.Remove(self.rs_onFileSaveCompleted)
            QtGui.QMessageBox.information(
                self,
                "Configuration file update confirmation",
                "The configuration file has been updated.\nYou must restart the application to load the new settings."
            )

    def _handleShelfOnStartupChangeState(self):
        """
        Internal Method

        Handle the shelf startup checkbox change
        """
        UserPreferences.__Saveini__("shelftastic", "OpenShelfOnStartup", self._shelfOnStartupCB.isChecked())

    def _handleEnableStagerChangeState(self):
        """
        Internal Method

        Handle the default Stager enable setting
        """
        UserPreferences.__Saveini__("menu", "Enable Stager", self._enableStager.isChecked())

        import RS.Core.Reference.New.BuildMenu
        RS.Core.Reference.New.BuildMenu.Run()

    def _enableProjectMenuColorChangeState(self):
        """
        Internal Method

        Handle the shelf startup checkbox change
        """
        UserPreferences.__Saveini__("menu", "Enable Project Menu Color", self._enableProjectMenuColorCB.isChecked())
        Scene.SetWindowTitle()

    def rs_onFileSaveCompleted(self, control, event):
        """
        Internal Method

        Event call back on save

        Arguments:
            control: Motion Builder object that is calling this method
            event (pyfbsdk.FBEvent)= Motion Builder event
        """
        KeepDefaultSaveSettings.SetSaveSettingToDefaultOptions()

    def _enableCustomOpenFileDialogChangeState(self):
        """
        Internal Method

        Changes the standard the open file menu option to use our custom file open
        """
        UserPreferences.__Saveini__("menu", "Enable Custom Open File Dialog",
                                    self._enableCustomOpenFileDialogCB.isChecked())
        Setup()

    def _enableAutoAudioChangeState(self):
        """
        Internal Method

        Enables the AutoAudio callbacks for syncing
        """
        enabled = self._enableAutoAudioCB.isChecked()
        UserPreferences.__Saveini__("audioTools", "audioDest", int(enabled))
        AudioTools.RegisterAutoAudio(enabled)
    
    def _handleSwitcherShortcutChangeState(self):
        UserPreferences.__Saveini__("shortcuts", "Enable Switcher Shortcuts", self._enableSwitcherShortcuts.isChecked())
        CamUtils.ToggleSwitcherShortcuts(self._enableSwitcherShortcuts.isChecked())



def Run(show=True):
    """
    Runs the tool

    Arguments:
        show (boolean): show the UI

    Return:
        QWidget()
    """
    tool = ToolsPreferencesTool()

    if show:
        tool.show()

    return tool