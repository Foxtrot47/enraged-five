'''

 Script Path: RS/Tools/UI/Mocap/Toolbox.py
 
 Written And Maintained By: Kathryn Bodey and Kristine Middlemiss
 
 Created: 
 
 Description: Toolbox UI to hold mocap tools:
              Stages, Decks, Blocking Models, Camera Relocate


'''

import inspect
from decimal import *

from pyfbsdk import *
from pyfbsdk_additions import *

import RS
import RS.Tools.UI
import RS.Core.Mocap.Decks
import RS.Utils.Creation as cre

###############################################################################################################
## Description: UI
###############################################################################################################

class MocapToolbox( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Mocap Decks", size = [ 430, 480 ])
        
    def Create( self, pMain ):
                
        #generate lists
        dropdownList = [ "4' x 8'", "4' x 4'", "3' x 6'", "2' x 6'", "4' Quarter_Round", "2' x 4'" ]
        
        deckDict = { "4' x 8'" : '4 x 8 x 7 4x8', 
                     "4' x 4'" : '4 x 4 x 7 4x4', 
                     "3' x 6'" : '3 x 6 x 7 3x6', 
                     "2' x 6'" : '2 x 6 x 7 2x6', 
                     "4' Quarter_Round" : '4 x 7 4_Quarter_Round',
                     "2' x 4'" : '2 x 4 x 7 2x4' }
    
        stageList = []

        if not RS.MOBU_IS_STARTING:
            for component in RS.Globals.gComponents:
                stageProperty = component.PropertyList.Find( 'Stages' )
                if stageProperty:
                    stageList.append( component.LongName )

        if len( stageList ) == 0:
            stageList.append( 'No Stages Present' )


        ###############################################################################################################
        ## Description: Mocap Decks
        ###############################################################################################################

        lMocapDeckTypeLabel = FBLabel()
        lMocapDeckTypeLabel.Caption = "Deck Type:"
        lMocapDeckTypeLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lMocapDeckTypeLabelY = FBAddRegionParam(40,FBAttachType.kFBAttachTop,"")
        lMocapDeckTypeLabelW = FBAddRegionParam(60,FBAttachType.kFBAttachNone,"")
        lMocapDeckTypeLabelH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lMocapDeckTypeLabel","lMocapDeckTypeLabel", lMocapDeckTypeLabelX, lMocapDeckTypeLabelY, lMocapDeckTypeLabelW, lMocapDeckTypeLabelH)
        pMain.SetControl("lMocapDeckTypeLabel",lMocapDeckTypeLabel)
        
        deckTypeDropDown = FBList()
        deckTypeDropDown.Style = FBListStyle.kFBDropDownList
        deckTypeDropDown.Hint = 'select the stage you wish to align your deck to'
        deckTypeDropDownX = FBAddRegionParam( 110,FBAttachType.kFBAttachLeft,"" )
        deckTypeDropDownY = FBAddRegionParam( 40,FBAttachType.kFBAttachTop, "" )
        deckTypeDropDownW = FBAddRegionParam( 150,FBAttachType.kFBAttachNone,"" )
        deckTypeDropDownH = FBAddRegionParam( 25,FBAttachType.kFBAttachNone,"" )            
        pMain.AddRegion( "deckTypeDropDown","deckTypeDropDown", deckTypeDropDownX, deckTypeDropDownY, deckTypeDropDownW, deckTypeDropDownH )
        pMain.SetControl( "deckTypeDropDown", deckTypeDropDown )
        for i in dropdownList:
            deckTypeDropDown.Items.append( i )
         
        lMocapDeckUserInputColor = FBEditColor()
        lMocapDeckUserInputColor.Value = FBColor(1,1,1)
        lMocapDeckUserInputColor.Hint = 'select a color you wish to be present on\nthe top and bottom faces of the deck'
        lMocapDeckUserInputColorX = FBAddRegionParam(10,FBAttachType.kFBAttachRight,"deckTypeDropDown")
        lMocapDeckUserInputColorY = FBAddRegionParam(40,FBAttachType.kFBAttachTop, "")
        lMocapDeckUserInputColorW = FBAddRegionParam(135,FBAttachType.kFBAttachNone,"")
        lMocapDeckUserInputColorH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")    
        pMain.AddRegion("lMocapDeckUserInputColor","lMocapDeckUserInputColor", lMocapDeckUserInputColorX, lMocapDeckUserInputColorY, lMocapDeckUserInputColorW, lMocapDeckUserInputColorH)
        pMain.SetControl("lMocapDeckUserInputColor", lMocapDeckUserInputColor)

        lMocapDeckTabLabel = FBLabel()
        lMocapDeckTabLabel.Caption = "Deck Namespace:"
        lMocapDeckTabLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lMocapDeckTabLabelY = FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"lMocapDeckTypeLabel")
        lMocapDeckTabLabelW = FBAddRegionParam(85,FBAttachType.kFBAttachNone,"")
        lMocapDeckTabLabelH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lMocapDeckTabLabel","lMocapDeckTabLabel", lMocapDeckTabLabelX, lMocapDeckTabLabelY, lMocapDeckTabLabelW, lMocapDeckTabLabelH)
        pMain.SetControl("lMocapDeckTabLabel",lMocapDeckTabLabel)
    
        lMocapDeckUserInputName = FBEdit()
        lMocapDeckUserInputName.Text = dropdownList[0]
        lMocapDeckUserInputName.Hint = 'name your deck'
        lMocapDeckUserInputNameX = FBAddRegionParam(110,FBAttachType.kFBAttachLeft,"")
        lMocapDeckUserInputNameY = FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"lMocapDeckTypeLabel")
        lMocapDeckUserInputNameW = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        lMocapDeckUserInputNameH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lMocapDeckUserInputName","lMocapDeckUserInputName", lMocapDeckUserInputNameX, lMocapDeckUserInputNameY, lMocapDeckUserInputNameW, lMocapDeckUserInputNameH)
        pMain.SetControl("lMocapDeckUserInputName",lMocapDeckUserInputName) 

        StageLabel = FBLabel()
        StageLabel.Caption = "Position to Stage:"
        StageLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        StageLabelY = FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"lMocapDeckUserInputName")
        StageLabelW = FBAddRegionParam(85,FBAttachType.kFBAttachNone,"")
        StageLabelH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("StageLabel","StageLabel", StageLabelX, StageLabelY, StageLabelW, StageLabelH)
        pMain.SetControl("StageLabel",StageLabel)
        
        StageDropDown = FBList()
        StageDropDown.Style = FBListStyle.kFBDropDownList
        StageDropDownX = FBAddRegionParam( 110,FBAttachType.kFBAttachLeft,"" )
        StageDropDownY = FBAddRegionParam( 10,FBAttachType.kFBAttachBottom, "lMocapDeckUserInputName" )
        StageDropDownW = FBAddRegionParam( 150,FBAttachType.kFBAttachNone,"" )
        StageDropDownH = FBAddRegionParam( 25,FBAttachType.kFBAttachNone,"" )            
        pMain.AddRegion( "StageDropDown","StageDropDown", StageDropDownX, StageDropDownY, StageDropDownW, StageDropDownH )
        pMain.SetControl( "StageDropDown", StageDropDown )
        for i in stageList:
            StageDropDown.Items.append( i )
        
        lMocapDeckButton = FBButton()
        lMocapDeckButton.Caption = "Create New Deck"
        lMocapDeckButton.Look = FBButtonLook.kFBLookPush
        lMocapDeckButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lMocapDeckButtonY = FBAddRegionParam(15,FBAttachType.kFBAttachBottom,"StageDropDown")
        lMocapDeckButtonW = FBAddRegionParam(395,FBAttachType.kFBAttachNone,"")
        lMocapDeckButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lMocapDeckButton","lMocapDeckButton", lMocapDeckButtonX, lMocapDeckButtonY, lMocapDeckButtonW, lMocapDeckButtonH)
        pMain.SetControl("lMocapDeckButton",lMocapDeckButton)  
                
        lDupeDeckButton = FBButton()
        lDupeDeckButton.Caption = "Duplicate Selected Deck(s)"
        lDupeDeckButton.Look = FBButtonLook.kFBLookPush
        lDupeDeckButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lDupeDeckButtonY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom,"lMocapDeckButton")
        lDupeDeckButtonW = FBAddRegionParam(395,FBAttachType.kFBAttachNone,"")
        lDupeDeckButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lDupeDeckButton","lDupeDeckButton", lDupeDeckButtonX, lDupeDeckButtonY, lDupeDeckButtonW, lDupeDeckButtonH)
        pMain.SetControl("lDupeDeckButton",lDupeDeckButton)
     
        lPrintXMLButton = FBButton()
        lPrintXMLButton.Caption = "Print Deck List (.csv)"
        lPrintXMLButton.Look = FBButtonLook.kFBLookPush
        lPrintXMLButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lPrintXMLButtonY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom,"lDupeDeckButton")
        lPrintXMLButtonW = FBAddRegionParam(395,FBAttachType.kFBAttachNone,"")
        lPrintXMLButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lPrintXMLButton","lPrintXMLButton", lPrintXMLButtonX, lPrintXMLButtonY, lPrintXMLButtonW, lPrintXMLButtonH)
        pMain.SetControl("lPrintXMLButton",lPrintXMLButton)

        loadXMLButton = FBButton()
        loadXMLButton.Caption = "Load Deck List"
        loadXMLButton.Look = FBButtonLook.kFBLookPush
        loadXMLButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        loadXMLButtonY = FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"lPrintXMLButton")
        loadXMLButtonW = FBAddRegionParam(395,FBAttachType.kFBAttachNone,"")
        loadXMLButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("loadXMLButton","loadXMLButton", loadXMLButtonX, loadXMLButtonY, loadXMLButtonW, loadXMLButtonH)
        pMain.SetControl("loadXMLButton",loadXMLButton) 

        stepMergeButton = FBButton()
        stepMergeButton.Caption = "Merge Step Measure"
        stepMergeButton.Look = FBButtonLook.kFBLookPush
        stepMergeButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        stepMergeButtonY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom,"loadXMLButton")
        stepMergeButtonW = FBAddRegionParam(395,FBAttachType.kFBAttachNone,"")
        stepMergeButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("stepMergeButton","stepMergeButton", stepMergeButtonX, stepMergeButtonY, stepMergeButtonW, stepMergeButtonH)
        pMain.SetControl("stepMergeButton",stepMergeButton) 

        measureLabel = FBLabel()
        measureLabel.Caption = "Measurement Label Display:\nToggle on and off to show, refresh & hide measurement labels in the viewport."
        measureLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        measureLabelY = FBAddRegionParam(20,FBAttachType.kFBAttachBottom,"stepMergeButton")
        measureLabelW = FBAddRegionParam(400,FBAttachType.kFBAttachNone,"")
        measureLabelH = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("measureLabel","measureLabel", measureLabelX, measureLabelY, measureLabelW, measureLabelH)
        pMain.SetControl("measureLabel",measureLabel)

        pointMeasureButton = FBButton()
        pointMeasureButton.Caption = "Point-to-Point Measurement (Inches)"
        pointMeasureButton.Look = FBButtonLook.kFBLookColorChange
        pointMeasureButton.Style = FBButtonStyle.kFB2States
        pointMeasureButton.SetStateColor( FBButtonState.kFBButtonState0,FBColor( 0.35, 0.35, 0.35 ) )
        pointMeasureButton.State = 0
        pointMeasureButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        pointMeasureButtonY = FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"measureLabel")
        pointMeasureButtonW = FBAddRegionParam(192,FBAttachType.kFBAttachNone,"")
        pointMeasureButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("pointMeasureButton","pointMeasureButton", pointMeasureButtonX, pointMeasureButtonY, pointMeasureButtonW, pointMeasureButtonH)
        pMain.SetControl("pointMeasureButton",pointMeasureButton)

        crossVectorMeasureButton = FBButton()
        crossVectorMeasureButton.Caption = "Cross Vector Measurement (Inches)"
        crossVectorMeasureButton.Look = FBButtonLook.kFBLookColorChange
        crossVectorMeasureButton.Style = FBButtonStyle.kFB2States
        crossVectorMeasureButton.SetStateColor( FBButtonState.kFBButtonState0,FBColor( 0.35, 0.35, 0.35 ) )
        crossVectorMeasureButton.State = 0
        crossVectorMeasureButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachRight,"pointMeasureButton")
        crossVectorMeasureButtonY = FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"measureLabel")
        crossVectorMeasureButtonW = FBAddRegionParam(192,FBAttachType.kFBAttachNone,"")
        crossVectorMeasureButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("crossVectorMeasureButton","crossVectorMeasureButton", crossVectorMeasureButtonX, crossVectorMeasureButtonY, crossVectorMeasureButtonW, crossVectorMeasureButtonH)
        pMain.SetControl("crossVectorMeasureButton",crossVectorMeasureButton)
        
        buttonGroup = FBButtonGroup()
        buttonGroup.Add( pointMeasureButton )
        buttonGroup.Add( crossVectorMeasureButton ) 
        
        # hide measurements by default when opening UI
        RS.Core.Mocap.Decks.DeckMeasurement( pointMeasureButton, None )
                    
        ###############################################################################################################
        ## MocapDecks Callbacks
        ###############################################################################################################
        
        # deck list
        def DeckListCallback(control, event):
                        
            for i in range(len(control.Items)):
                if control.IsSelected(i):
                    lMocapDeckUserInputName.Text = control.Items[i]
            
        deckTypeDropDown.OnChange.Add(DeckListCallback)


        # create deck
        def CreateDeckCallBack( control, event ):
            
            selectedDeck = None
            selectedStage = None
            
            for i in range(len(deckTypeDropDown.Items)):
                if deckTypeDropDown.IsSelected(i):
                    for key, value in deckDict.iteritems():
                        if key == deckTypeDropDown.Items[i]:
                            selectedDeck = value
            
            for i in range(len(StageDropDown.Items)):
                if StageDropDown.IsSelected(i):
                    if StageDropDown.Items[i] == 'No Stages Present':
                        None
                    else:
                        selectedStage = StageDropDown.Items[i]

            RS.Core.Mocap.Decks.CreateMocapDeck(lMocapDeckUserInputName.Text, lMocapDeckUserInputColor.Value, selectedDeck, selectedStage )

        lMocapDeckButton.OnClick.Add(CreateDeckCallBack)

        # dupe deck(s)
        lDupeDeckButton.OnClick.Add( RS.Core.Mocap.Decks.DuplicateMocapDeck )

        # print deck(s)
        lPrintXMLButton.OnClick.Add(RS.Core.Mocap.Decks.PrintMocapDecks)

        # load deck(s)
        loadXMLButton.OnClick.Add(RS.Core.Mocap.Decks.LoadMocapDecks)

        # step merge
        def StepMergeCallBack( control, event ):
            
            selectedStage = None
            
            for i in range( len( StageDropDown.Items ) ):
                if StageDropDown.IsSelected( i ):
                    if StageDropDown.Items[i] == 'No Stages Present':
                        None
                    else:
                        selectedStage = StageDropDown.Items[i]

            RS.Core.Mocap.Decks.MergeStepMeasure( selectedStage )
        
        stepMergeButton.OnClick.Add( StepMergeCallBack )

        # display/hide measurements
        pointMeasureButton.OnClick.Add(RS.Core.Mocap.Decks.DeckMeasurement)
        crossVectorMeasureButton.OnClick.Add(RS.Core.Mocap.Decks.DeckMeasurement)

def Run( show = True ):
    tool = MocapToolbox() 
    
    if show:
        tool.Show()
        
    return tool
