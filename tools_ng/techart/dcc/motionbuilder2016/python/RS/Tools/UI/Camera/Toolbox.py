import os
import inspect

from datetime import datetime
from os.path import join, getsize

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.UI
import RS.Core.Reference.Manager
from RS.Core.Camera import Lib as CamLib, CamUtils, LibGTA5
import RS.Core.Camera.CameraLocker as CameraLocker
import RS.Core.Camera.CameraCleaner as CameraCleaner
import RS.Core.Camera.SaveCameras as SaveCameras
import RS.Core.Camera.KeyTools.camKeyTasks as camKeyTasks
import RS.Core.Camera.CaptureRender as CaptureRender
import RS.Perforce
import RS.Globals as glo
from RS.Utils.Scene.Property import PropertyMonitor
import RS.Utils.Creation as cre
import RS.Utils.UserPreferences as userPref

# Shake Core
import RS.Core.Camera.Shaker as shake

# Config Parser
import ConfigParser, os

import RS.Utils.Scene.Camera
import RS.Utils.Scene.Take
import RS.Core.Camera

# USe for debugging (allows us to recompile changes)
reload(core)
reload(shake)
reload(RS.Core.Camera.Plot)
reload( RS.Utils.Scene.Camera )

"""
REF://rage/<project>/dev/rage/suite/tools/dcc/motionbuilder/RemoteConsole/RemoteConsolePlugin.cpp
MOTIONBUILDER_UNIT_SCALE   1.0f / 100.0f


Has been commented out in code, used to control the far and near planes, we coudl sue this to store the DOF Intensity
Need to keep compabability

//static const char* CameraDofStrengthWidgetName = "Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides/Cam DOF Strength";


We will break the tool up later to be more UI modal based
"""


# Rockstar Banner (amazingly simialr to mine ;:)
##toolName = "Camera UI  V1.9 (Shake rev 3)"
##emailTo = "RSG MotionBuilder SDK Support"

##projroot = os.getenv('RS_TOOLSROOT')
##print projroot
##projroot +'/wildwest/script/MotionBuilderPython/CoreFunctions/rs_p4Monitor/config.cfg'

##toolPath = projroot + "/wildwest/script/MotionBuilderPython/UserInterface/rs_CameraCoreFunctions/rs_CameraCoreFunctions_UI_2012.py"
##print toolPath
##cc = "mark.harrison-ball@rockstarlondon.com"
##wikipage = "https://devstar.rockstargames.com/wiki/index.php/Cameras_Ingame"
##shakePath = projroot + "/wildwest/script/MotionBuilderPython/External/CameraData/"
##print shakePath
gDevelopment = False

class CameraToolbox( RS.Tools.UI.Base ):
    def __init__( self ):

        wikipage = "https://devstar.rockstargames.com/wiki/index.php/Cameras_Ingame"
        self.shakePath = os.path.abspath( "{0}/etc/CameraData".format( RS.Config.Tool.Path.TechArt ) )    
        RS.Tools.UI.Base.__init__( self, "Camera Toolbox v2.0 (Shake Rev 3)", size = [ 684, 900 ], helpUrl = "https://devstar.rockstargames.com/wiki/index.php/Cameras_Ingame" )

        self.pListView = None
        self.pFCurveView = None
        self.pShakeListControl = None # Holds refernfce to our shake Control
        self.ShakeLabelControl = None
        self.SelectedCam = None # Camera Name
        self.CameraControls = None  # Holds camera Controls
        self.ActiveCamera = None  # Camera Node

        self.CsfFileControl = None # holds the handle to the spreadsheet layout
        self.mSpreadFolderListRegion = None # the region reference that contains the Camera Library spreadsheet
        self.CSFpath = None
        self.shakeRelativePath = None #relative to the shakePath
        self.BrowserControls = None #hold the handle to the Camera Shake Browser region and controls
        self.TabIndex = 0 #holds the index of the last selected tab

        self.liveupdate = False
        # Setting Flags
        self.switchCamera = False
        self.AutoViewPlanes = True
        self.FocusMarkerScale = 500
        self.bShowAllCameras = False        
    
        # store lens button list and user setting url:bugstar:1510245
        self.lenButtonList = []    
        self.LensSetting = None
        
        # Testing Colour Button Workaround
        self.GreyButton = FBColor(0.36, 0.36, 0.36)
        self.RedButton = FBColor(1.0, 0.0, 0.0)

        # we store system object becuase it's not singleton!
        self.mApplication = FBApplication()
        self.mAcceptCallback = True
        
        # variables for camera selection url:bugstar:1815189
        self.CameraCallbackEvent = None
        self.SpreadCameraDict = {}
        self.CameraListSpreadSheet = None  
        self.SwitcherMonitor = None
        
        self.SwitcherMonitor = RS.Utils.Scene.Camera.SwitcherMonitor()

        # Should be stored in the users folder, use OS enviorment to get windows temp folder (BOB TODO)
        self.switchCamera = userPref.__Readini__( "CameraToolbox","SwitchCamera", self.switchCamera )
        self.AutoViewPlanes = userPref.__Readini__( "CameraToolbox","ShowPlanes", self.AutoViewPlanes )
        self.FocusMarkerScale = userPref.__Readini__( "CameraToolbox","FocusMarkerScale", self.FocusMarkerScale )
        self.LensSetting = userPref.__Readini__( "CameraToolbox","LensSetting", self.LensSetting )
        
        if self.switchCamera == None:
            self.switchCamera = False 
    
        if self.AutoViewPlanes == None:
            self.AutoViewPlanes = True 
    
        if self.FocusMarkerScale == None:
            self.FocusMarkerScale = 500   
            
        # core.camUpdate.camFunctions.CleanUpParentLessPlanes()

        #Check if Switcher Camera has any constraints linked to it, give the user the option to delete them url:bugstar:1702371
        self.CheckCameraSwitcherConstraints()

        # script v.16/toolbox v2.0 updates url:bugstar:1733892
        # property monitor for auto-updating properties in UI and scene, so they stay in sync at all times       
        self._propertyMonitor = PropertyMonitor()

        # set user-unique focus marker scale
        for cam in RS.Globals.gCameras:
            if not cam.SystemCamera:
                if len( cam.Children ) > 0:
                    for child in cam.Children:
                        if 'focus_marker' in child.LongName.lower():
                            child.Size = float( self.FocusMarkerScale )
                            
        # Unlock Cams and Switcher
        camLockObj = CameraLocker.LockManager()
        if camLockObj.lockedCount != 0:
            camLockObj.setAllCamLocks(False)
        
        #Detect/Clean Cameras
        _fixCamObj_ = CameraCleaner.FixCamNames()
        
        # organise lateral cams into a folder
        self.OrganiseLateralCams()

    # callback for Camera selection url:bugstar:1815189
    def CameraSelectCallback( self, obj, evt ):
        
        if self.CameraCallbackEvent:
        
            if evt.Type == FBSceneChangeType.kFBSceneChangeSelect and evt.Component.ClassName() == 'FBCamera':  
                      
                cameraSelected =  evt.Component
                self.SelectedCam = cameraSelected
                
                if self.SpreadCameraDict:
                    
                    # get current cell and deselect before selected new
                    for key, value in self.SpreadCameraDict.iteritems():
                        try:
                            row = self.CameraListSpreadSheet.GetRow( value - 1 ) 
                            if row.RowSelected == True:
                                row.RowSelected = False
                                break
                        except:
                            pass
                    
                    # select new cell
                    for key, value in self.SpreadCameraDict.iteritems():
                        if key.Name == self.SelectedCam.Name:
                            try:
                                row = self.CameraListSpreadSheet.GetRow( value - 1 )
                                row.RowSelected = True
                            except:
                                pass
                                                      
                            # took this from '_on_SelectCamera'
                            # add check to enable constraint for camera planes in the scene - 1331929
                            for i in range(self.SelectedCam.GetSrcCount()):
                                if self.SelectedCam.GetSrc(i).ClassName() == "FBConstraintRelation":
                                    if self.SelectedCam.GetSrc(i).Active == False:
                                        self.SelectedCam.GetSrc(i).Active = True    
                            self.ActiveCamera = self.SelectedCam
                            if gDevelopment:
                                print self.ActiveCamera.Name
                            
                            try:    
                                self.refreshCamControlsView()   
                            except:
                                pass
                            break


    def OnFileNewCompleted(self, control, event):
        # This function has to finish as quickly as possible
        # And you should not print things in here, because application will have to stop evaluation/devices to be able to flush the buffer
        #
        #   Please do not use FBFindModelByLabelName or PropertyList.Find in here.
        #
        self.Show( True )
        self.mAcceptCallback = True

    def FileNewCompleted( self, pActivate ):
        if self.mApplication:
            # we always try to remove this notification function to prevent cases where we have it register twice.
            self.mApplication.OnFileNewCompleted.Remove(self.OnFileNewCompleted) 
            self.mApplication.OnFileOpenCompleted.Remove(self.OnFileNewCompleted)         
            if pActivate:        
                self.mApplication.OnFileNewCompleted.Add(self.OnFileNewCompleted)                
                self.mApplication.OnFileOpenCompleted.Add(self.OnFileNewCompleted)        

    # very important, used to unregister notification before tool will be destroyed!          
    def OnUnbindEvent(self, control, event):
        self.FileNewCompleted(False)    

    # Go through our cameras and see if tehy need to be updated
    # The update our Listview with the info
    def getProperties(self, iCam):        
        propList = []
        # TODO: Disabling requires update check for now
        # # 2 of the planes is a screenfade and a intrest fake point
        # planescount = (core.camUpdate.camFunctions.GetPlanesCount(iCam)-2)
        # if planescount < 0:
        #     planescount = 0
        #
        # propList.append(str(planescount))
        # propList.append(core.camUpdate.camFunctions.GetCameraVersion(iCam))
        # strUpdate = ""
        #
        # val = core.camUpdate.camFunctions.GetRequiresUpdate(iCam)
        # if val:
        #     strUpdate="invalid: %s" % val
        #
        # #if core.camUpdate.camFunctions.GetRequiresUpdate(iCam):
        # #    strUpdate = "Update me"
        # propList.append(strUpdate)#   str(self.GetRequiresUpdate(iCam)))
        # #propList.append("Show/Hide")#   str(self.GetRequiresUpdate(iCam)))
        # propList.append(iCam.Name)#   str(self.GetRequiresUpdate(iCam)))
        return propList

    def setCells(self, pControl,propArray, row):
        for i in range(len(propArray)):
            pControl.SetCellValue( row, i, propArray[i])

    def CreateHeaders(self,pControl):
        pControl.ColumnAdd( "DOF Planes",1 );
        pControl.ColumnAdd( "Version",2 );
        pControl.ColumnAdd( "Needs Update",3 ); 
        #pControl.ColumnAdd( "Planes",4 ); 
        pControl.ColumnAdd( "",4 ); 

        pControl.GetColumn( -1 ).Width = 130

        for i in range(3):            
            pControl.GetColumn( i ).Justify = FBTextJustify.kFBTextJustifyCenter
            pControl.GetColumn( i ).ReadOnly = True

        #pControl.GetColumn( 0 ).Style = FBCellStyle.kFBCellStyleButton
        pControl.GetColumn( 0 ).Width = 70
        pControl.GetColumn( 1 ).Width = 70
        pControl.GetColumn( 2 ).Width = 140
        pControl.GetColumn( 3 ).Width = 1

    def AddShakeFileList(self,pControl):

        pControl.ColumnAdd( "Size",1 );
        pControl.ColumnAdd( "Modified",2 );

        pControl.ColumnAdd( "",3 );

        pControl.GetColumn( -1 ).Width = 130

        for i in range(3):            
            pControl.GetColumn( i ).Justify = FBTextJustify.kFBTextJustifyCenter
            pControl.GetColumn( i ).ReadOnly = True

        pControl.GetColumn( -1 ).Width = 175
        pControl.GetColumn( 0 ).Width = 125
        pControl.GetColumn( 1 ).Width = 150
        pControl.GetColumn( 2 ).Width = 1

    #Check if Switcher Camera has any constraints linked to it, give the user the option to delete them url:bugstar:1702371
    def CheckCameraSwitcherConstraints( self ):

        switcherConstraintList = []

        for i in range( FBCameraSwitcher().GetSrcCount() ):
            if FBCameraSwitcher().GetSrc( i ).ClassName() == 'FBConstraint':
                switcherConstraintList.append( FBCameraSwitcher().GetSrc( i ) )

            if FBCameraSwitcher().GetSrc( i ).ClassName() == 'FBConstraintRelation':
                switcherConstraintList.append( FBCameraSwitcher().GetSrc( i ) )

        for i in range( FBCameraSwitcher().GetDstCount() ):
            if FBCameraSwitcher().GetDst( i ).ClassName() == 'FBConstraint':
                switcherConstraintList.append( FBCameraSwitcher().GetDst( i ) )

            if FBCameraSwitcher().GetDst( i ).ClassName() == 'FBConstraintRelation':
                switcherConstraintList.append( FBCameraSwitcher().GetDst( i ) )

        if len( switcherConstraintList ) > 0:

            messagebox = FBMessageBox('R* Camera Toolbox', 'Constraints have been found linked to the Switcher Camera,\nwould you like to delete these?', 'Yes', 'No')

            if messagebox == 1:
                for constraint in switcherConstraintList:
                    try:
                        constraint.FBDelete()
                    except:
                        pass 

            if messagebox == 2:
                print 'Camera Toolbox: user selected not to remove switcher camera constraints.'


    # Update our View, show only cameras in the Switcher
    def refreshCamerasView(self,pControl):
        FBSystem().CurrentTake.SetCurrentLayer(0)
        pControl.Clear()
        self.CreateHeaders(pControl)
        row = 0

        if not self.bShowAllCameras:

            # core.camUpdate.camFunctions.UpdateActiveSwitcherCameras()
            switcherCams = [cam.camera for cam in CamLib.Manager.GetSwitcherCameras()]
            for iCam in switcherCams:
                pControl.RowAdd( iCam.Name, row );
                propArray = self.getProperties(iCam)
                self.setCells(pControl, propArray, row)        
                row += 1    
                self.SpreadCameraDict[ iCam ] = ( row )                
                
        else:
            for iCam in glo.gCameras:
                if not iCam.SystemCamera:        
                    pControl.RowAdd( iCam.Name, row );
                    propArray = self.getProperties(iCam)
                    self.setCells(pControl, propArray, row)  
                    row += 1
                    
                    self.SpreadCameraDict[ iCam ] = ( row )

    #def refreshCurveView(self, pControl):
    #    pControl.AddProperty()

    #def rs_fCurveView(self, pMain):
        #mEditor = FBFCurveEditor()
        #pMain.AddRegion("FCurve","FCurve",
                        #FBAddRegionParam(10,FBAttachType.kFBAttachRight,"Spread"),
                        #FBAddRegionParam(70,FBAttachType.kFBAttachTop,"lBannerLabel"),
                        #FBAddRegionParam(0,FBAttachType.kFBAttachRight,""),
                        #FBAddRegionParam(0,FBAttachType.kFBAttachBottom,""))
        #pMain.SetControl("FCurve",mEditor)
        #return mEditor


    #def rs_collapseControls(self, pMain):
        #lHealthButton = FBButton()
        #lHealthButton.Caption = "\/"
        #lHealthButton.Justify = FBTextJustify.kFBTextJustifyCenter  
        #pMain.AddRegion("lHealthButton","lHealthButton", 
                        #FBAddRegionParam(-4,FBAttachType.kFBAttachLeft, ""),
                        #FBAddRegionParam(530,FBAttachType.kFBAttachTop,""),
                        #FBAddRegionParam(620,FBAttachType.kFBAttachNone,""),
                        #FBAddRegionParam(16,FBAttachType.kFBAttachNone,""))  
        #pMain.SetControl("lHealthButton",lHealthButton)        

    # Create Spreadsheet View
    def rs_spreadsheet(self, pMain):

        pMain.AddRegion("SpreadUI","SpreadUI",
                        FBAddRegionParam(250,FBAttachType.kFBAttachLeft,""),
                        FBAddRegionParam(100,FBAttachType.kFBAttachTop,"lBannerLabel"),
                        #FBAddRegionParam(0,FBAttachType.kFBAttachRight,""), # If you don't want to show the FCurve Editor
                        FBAddRegionParam(410,FBAttachType.kFBAttachNone,""), # For FCurve Editor
                        FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")) 


        FilterCameraList = FBList()
        FilterCameraList.Style = FBListStyle.kFBDropDownList
        pMain.SetControl("SpreadUI",FilterCameraList)         

        FilterCameraList.Items.append("Show Just Animated Switcher Cameras")
        FilterCameraList.Items.append("Show All Cameras")        
        FilterCameraList.OnChange.Add(self.rs_FilterCameas)        




        mSpreadView = FBSpread()
        self.CameraListSpreadSheet = mSpreadView
        
        
        mSpreadView.Caption        = "CameraList";
        #mSpreadView.MultiSelect    = False;

        self.CreateHeaders(mSpreadView)

        pMain.AddRegion("Spread","Spread",
                        FBAddRegionParam(250,FBAttachType.kFBAttachLeft,""),
                        FBAddRegionParam(30,FBAttachType.kFBAttachTop,"SpreadUI"),
                        #FBAddRegionParam(0,FBAttachType.kFBAttachRight,""), # If you don't want to show the FCurve Editor                    
                        FBAddRegionParam(410,FBAttachType.kFBAttachNone,""), # For FCurve Editor
                        FBAddRegionParam(290,FBAttachType.kFBAttachNone,""))

        mSpreadView.OnRowClick.Add(self._on_SelectCamera) 
        mSpreadView.OnCellChange.Add( self._on_CellSelected )


        pMain.SetControl("Spread",mSpreadView)     
        self.refreshCamerasView(mSpreadView)


        return mSpreadView

    """
    DYNAMINC CAMERA CONTROLS
    """

    def __removeCamControls__(self):     
        for control in core.gCamNewProperties:  
            self.CameraControls.ClearControl(control.Name)
            self.CameraControls.ClearControl("spinner"+control.Name)
            self.CameraControls.ClearControl("Check"+control.Name)
            self.CameraControls.ClearControl("label"+control.Name)                  


    def __removeBrowserControls__(self):

        if gDevelopment:
            print self.BrowserControls.Name
            print "__removeBrowserControls__"

        self.BrowserControls.ClearControl("camliblabel")
        self.BrowserControls.ClearControl("lAddFadeInButton")
        self.BrowserControls.ClearControl("csfmain")
        self.BrowserControls.ClearControl("csfSpread")  

        if gDevelopment:
            print self.BrowserControls.Name

    # script v.16/toolbox v2.0 updates url:bugstar:1733892
    # defs for auto updating UI to be in sync with custom property changes, thanks to Ross for Property Monitor!
    def UpdateSpinnerAndSlider( self, TargetProperty, PreviousValue ):
        TargetProperty._slider.Value = TargetProperty.Data
        TargetProperty._spinner.Value = TargetProperty.Data

    def UpdateBool( self, TargetProperty, PreviousValue ):
        TargetProperty._boolControl.State = TargetProperty.Data

    def UpdateEnum( self, TargetProperty, PreviousValue ):
        TargetProperty._dropDown.Selected( TargetProperty.Data, True )


    # Wanted to do this dynamically but MB is stupid so will have to go through all pos controls
    def __AddControls__(self): 

        # clear out existing callback
        self._propertyMonitor.Clear()        
        self._propertyMonitor.Deactivate() 

        if self.TabIndex != 1:

            # create sliders & spinners
            index = 0
            for Prop in core.gCamNewProperties:
                try:
                    fProp = self.ActiveCamera.PropertyList.Find(Prop.Name) # If we find our camera Propeerty
                    if (fProp):                

                        if (Prop.Type == FBPropertyType.kFBPT_double or Prop.Type == FBPropertyType.kFBPT_int): # Hack for now

                            # Create a Slider Control
                            slider = self.rs_sliderControl(fProp, index)  
                            setattr( fProp, '_slider', slider)  

                            # Create our spinner 
                            spinner = self.rs_spinnerControl(fProp, Prop, index)    
                            setattr( fProp, '_spinner', spinner)

                            # Add out label and Value                
                            self.rs_labelControl(Prop, index)

                            # Add to property monitor
                            self._propertyMonitor.Add( fProp, self.UpdateSpinnerAndSlider )

                            index+=1 
                except:
                    pass


            # create bool buttons
            indexH = 0       
            for Prop in core.gCamNewProperties:
                try:
                    fProp = self.ActiveCamera.PropertyList.Find(Prop.Name) # If we find our camera Propeerty
                    if (fProp):   
                        if (Prop.Type == FBPropertyType.kFBPT_bool):
                            # Add our checkboxes
                            boolControl = self.rs_boolControls( Prop, fProp, index, indexH )
                            setattr( fProp, '_boolControl', boolControl )
                            indexH+=1 

                            # Add to property monitor
                            self._propertyMonitor.Add( fProp, self.UpdateBool )
                except:
                    pass  


            # create dropdownlist for enum properties
            indexH = 0       
            for Prop in core.gCamNewProperties:
                try:
                    fProp = self.ActiveCamera.PropertyList.Find( Prop.Name )
                    if ( fProp ):   
                        if ( Prop.Type == FBPropertyType.kFBPT_enum ):
                            enumList = []
                            for i in fProp.GetEnumStringList():
                                enumList.append( i )
                            self.DropDownControls( Prop, fProp, index, indexH, enumList )
                            dropDown = self.DropDownControls( Prop, fProp, index, indexH, enumList )
                            setattr( fProp, '_dropDown', dropDown )
                            indexH+=1   

                            # Add to property monitor
                            self._propertyMonitor.Add( fProp, self.UpdateEnum )

                except:
                    pass  


            self.FocusMarkerControl() 



        # activate callback
        self._propertyMonitor.Activate()

    def __AddBrowserControls__(self):

        self.PopulateCSF(self.BrowserControls)
        self.CsfFileControl = self.PopulateFolderList(self.BrowserControls)

    # shallow and simple DOF buttons
    def rs_boolControls(self, Prop, fProp, i, iH):

        lchk = FBButton()
        lchk.Style = FBButtonStyle.kFB2States
        lchk.Look = FBButtonLook.kFBLookColorChange

        layoutName = "Check"+Prop.Name
        lchk.Name = layoutName        
        lchk.Caption = Prop.Name

        #lchk.SetStateColor(FBButtonState.kFBButtonState0,FBColor(1.0, 0.0, 0.5))
        #lchk.SetStateColor(FBButtonState.kFBButtonState1,FBColor(0.0, 1.0, 0.5))        


        self.CameraControls.AddRegion(layoutName ,layoutName ,
                                      FBAddRegionParam(10+(iH*100),FBAttachType.kFBAttachNone,Prop.Name), 
                                      FBAddRegionParam(-420+(60+(i*32)),FBAttachType.kFBAttachBottom,""),
                                      FBAddRegionParam(80,FBAttachType.kFBAttachNone,""), 
                                      FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")) 
        self.CameraControls.SetControl(layoutName ,lchk) 

        lchk.State = fProp.Data

        lchk.OnClick.Add(self.SelectCheck) 

        return lchk

    # toolbox v2.0  updates url:bugstar:1733892
    def DropDownControls( self, Prop, fProp, i, iH, enumList ):

        lensLabel = FBLabel()
        lensLabel.Caption = "Lens"     
        self.CameraControls.AddRegion( "lensLabel", "lensLabel" ,
                                       FBAddRegionParam( 560, FBAttachType.kFBAttachBottom, "" ), 
                                       FBAddRegionParam( 360, FBAttachType.kFBAttachLeft, "" ),
                                       FBAddRegionParam( 50, FBAttachType.kFBAttachNone, "" ), 
                                       FBAddRegionParam( 50, FBAttachType.kFBAttachNone, "" ) ) 
        self.CameraControls.SetControl( "lensLabel", lensLabel )              

        dropDown = FBList()
        dropDown.Style = FBListStyle.kFBDropDownList
        layoutName = "drop"+Prop.Name
        dropDown.Name = layoutName

        self.CameraControls.AddRegion( layoutName ,layoutName ,
                                       FBAddRegionParam( 65, FBAttachType.kFBAttachLeft, "" ), 
                                       FBAddRegionParam( -450+(60+(i*32)), FBAttachType.kFBAttachBottom,"" ),
                                       FBAddRegionParam( 480, FBAttachType.kFBAttachNone,"" ), 
                                       FBAddRegionParam( 25, FBAttachType.kFBAttachNone,"" ) ) 
        self.CameraControls.SetControl( layoutName ,dropDown ) 

        for i in enumList:
            dropDown.Items.append( i )  

        dropDown.Selected( fProp.Data, True )

        dropDown.OnChange.Add( self.EnumChange ) 

        return dropDown

    # url:bugstar:1777879
    def FocusMarkerControl( self ):

        focusMarkerLabel = FBLabel()
        focusMarkerLabel.Caption = "Focus Size"     
        self.CameraControls.AddRegion( "focusMarkerLabel", "focusMarkerLabel" ,
                                       FBAddRegionParam( 560, FBAttachType.kFBAttachBottom, "" ), 
                                       FBAddRegionParam( 338, FBAttachType.kFBAttachLeft, "" ),
                                       FBAddRegionParam( 50, FBAttachType.kFBAttachNone, "" ), 
                                       FBAddRegionParam( 30, FBAttachType.kFBAttachNone, "" ) ) 
        self.CameraControls.SetControl( "focusMarkerLabel", focusMarkerLabel )        

        focusSlider = FBSlider()    
        focusSlider.Orientation = FBOrientation.kFBHorizontal         
        focusSlider.Min = 100
        focusSlider.Max = 3000
        focusSlider.Value = float( self.FocusMarkerScale )
        self.CameraControls.AddRegion( 'focusSlider' ,'focusSlider' ,
                                       FBAddRegionParam( 64, FBAttachType.kFBAttachBottom, "" ), 
                                       FBAddRegionParam( 338, FBAttachType.kFBAttachLeft, "" ),
                                       FBAddRegionParam( 481, FBAttachType.kFBAttachNone, "" ), 
                                       FBAddRegionParam( 25, FBAttachType.kFBAttachNone, "" ) ) 
        self.CameraControls.SetControl( 'focusSlider' ,focusSlider )               

        focusSlider.OnChange.Add( self.FocusMarkerScaleChange ) 

    # Custom Label Control
    def rs_labelControl(self, Prop, i):
        l = FBButton()
        l.Look = FBButtonLook.kFBLookPush
        l.Caption = Prop.Name

        layoutName = "label"+Prop.Name
        l.Name = layoutName
        self.CameraControls.AddRegion(layoutName ,layoutName ,
                                      FBAddRegionParam(6,FBAttachType.kFBAttachRight,Prop.Name), 
                                      FBAddRegionParam(-480+(60+(i*32)),FBAttachType.kFBAttachBottom,""),
                                      FBAddRegionParam(80,FBAttachType.kFBAttachNone,""), 
                                      FBAddRegionParam(26,FBAttachType.kFBAttachNone,"")) 
        self.CameraControls.SetControl(layoutName ,l) 
        l.OnClick.Add(self.SelectDOFProperty) 

    # Custom Slider Control
    def rs_sliderControl(self, fProp, index):
        hsSlider = FBSlider()    
        hsSlider.Orientation = FBOrientation.kFBHorizontal   


        hsSlider.Min = fProp.GetMin()
        hsSlider.Max = fProp.GetMax()
        if fProp.GetMax() <= 10:
            hsSlider.SmallStep = 0.01
            hsSlider.LargeStep = 0.02
        else:
            hsSlider.SmallStep = 1
            hsSlider.LargeStep = 2                

        hsSlider.Value = fProp.Data

        hsSlider.Name = fProp.Name

        self.CameraControls.AddRegion(fProp.Name,fProp.Name,
                                      FBAddRegionParam(65,FBAttachType.kFBAttachLeft,"camliblabel"), 
                                      FBAddRegionParam(-480+(60+(index*32)),FBAttachType.kFBAttachBottom,"camliblabel"),
                                      FBAddRegionParam(480,FBAttachType.kFBAttachNone,""), 
                                      FBAddRegionParam(26,FBAttachType.kFBAttachNone,""))
        self.CameraControls.SetControl(fProp.Name,hsSlider)
        hsSlider.OnChange.Add(self.ValueChangeDOF)           

        # toolbox v2.0  updates url:bugstar:1733892
        if 'coc' in fProp.Name.lower():
            if 'override' not in fProp.Name.lower():
                hsSlider.Enabled = False
        else:
            hsSlider.Enabled = True   

        return hsSlider

    # Custom Spinner Control        
    def rs_spinnerControl(self, fProp, Prop, i):
        spin = FBEditNumber()                    

        spin.FBBorderStyle = FBBorderStyle.kFBNoBorder

        #l.Caption = str(fProp.Data)
        #print fProp.Data

        spin.Precision = 0.4321

        if fProp.GetMax() <= 10:
            spin.SmallStep = 0.0001
            spin.LargeStep = 0.0002
        else:
            spin.SmallStep = 1
            spin.LargeStep = 2         

        #print "spin.Precision %s " % (spin.Precision )
        spin.Value = fProp.Data
        spin.Min = fProp.GetMin()
        spin.Max = fProp.GetMax()

        layoutName = "spinner"+Prop.Name
        spin.Name = layoutName
        self.CameraControls.AddRegion(layoutName ,layoutName ,
                                      FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"camliblabel"), 
                                      FBAddRegionParam(-480+(60+(i*32)),FBAttachType.kFBAttachBottom,"camliblabel"),
                                      FBAddRegionParam(46,FBAttachType.kFBAttachNone,""), 
                                      FBAddRegionParam(26,FBAttachType.kFBAttachNone,"")) 
        self.CameraControls.SetControl(layoutName ,spin) 

        spin.OnChange.Add(self.ValueNumChanged) 

        return spin        

    def AddShakeLibRefreshButton(self, mainLyt):
        but = FBButton()

        but.Caption = "Refresh Folders"
        but.Look = FBButtonLook.kFBLookPush

        mainLyt.AddRegion("lAddFadeInButton","lAddFadeInButton",
                          FBAddRegionParam(-10,FBAttachType.kFBAttachLeft,"camliblabel"), 
                          FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"camliblabel"),
                          FBAddRegionParam(150,FBAttachType.kFBAttachNone,""), 
                          FBAddRegionParam(20,FBAttachType.kFBAttachNone,""))
        mainLyt.SetControl("lAddFadeInButton",but) 

        #TODO - Refresh the Folder Structure when the button is clicked
        but.OnClick.Add(self.ShakeLibraryRefresh)


        return but

    def CreateDynamicCameraLibFolders(self, tree, path, parentTreeNode, localPath):                  
        dirPath = path + localPath
        dirList = os.listdir(dirPath)
        for fname in dirList:            
            if not fname.endswith(".csf"):
                mname = tree.InsertLast(parentTreeNode, fname)
                mname.LongName = localPath

                subpath = localPath + fname + "/"
                self.CreateDynamicCameraLibFolders(tree, path, mname, subpath)        

    def ShakeTreePopulate(self, treeLyt):

        treeLyt.AddRegion("csfmain","csfmain", 
                          FBAddRegionParam(-10,FBAttachType.kFBAttachLeft,"camliblabel"), 
                          FBAddRegionParam(28,FBAttachType.kFBAttachBottom,"camliblabel"), 
                          FBAddRegionParam(150,FBAttachType.kFBAttachNone,""), 
                          FBAddRegionParam(250,FBAttachType.kFBAttachNone,""))

        treeLyt.SetBorder("csfmain",FBBorderStyle.kFBStandardBorder,False, False,1,0,20,0)

        t = FBTree()
        t.CheckBoxes = False
        t.AutoExpandOnDblClick = True
        t.AutoExpandOnDragOver = True
        t.AutoScroll = True
        t.AutoScrollOnExpand = True
        t.ItemHeight = 20
        t.DeselectOnCollapse = True
        t.EditNodeOn2Select = False
        t.HighlightOnRightClick = True
        t.MultiDrag = True
        t.MultiSelect = True
        t.NoSelectOnDrag = True
        t.NoSelectOnRightClick = False
        t.ShowLines = False

        treeLyt.SetControl("csfmain",t)

        root = t.GetRoot()        
        aTest = "\\"
        self.CreateDynamicCameraLibFolders(t, self.shakePath, root, aTest)

        return t

    def PopulateCameraLibFiles(self, control, event, fullPath):
        dirList = os.listdir(fullPath)
        files = []
        for fname in dirList:        
            if fname.endswith(".csf"):        
                files.append(fname)

        self.PopulateCSFFileListFromFolder(fullPath, files)

    def AddCameraLibLabel(self, camMainLyt):
        # Camera Library label
        camlabel = FBLabel()

##        self.cameraLibraryLabel = camlabel

        camlabel.Style = FBTextStyle.kFBTextStyleBold
        camlabel.Caption = "Camera Library"     
        camMainLyt.AddRegion("camliblabel" ,"Camera Library" ,
                             FBAddRegionParam(35,FBAttachType.kFBAttachLeft,"camlibRegion"), 
                             FBAddRegionParam(10,FBAttachType.kFBAttachTop,"Border1"),
                             FBAddRegionParam(150,FBAttachType.kFBAttachNone,""), 
                             FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")) 
        camMainLyt.SetControl("camliblabel" ,camlabel)

        return camlabel

    def PopulateCSF(self, lyt):
        #Create the Browser Label 
        labCtrl = self.AddCameraLibLabel(lyt)


        #add a button for refreshing folders
        refCtrl = self.AddShakeLibRefreshButton(lyt)


        #Create the folder tree using the root Shake folder                
        treeCtrl = self.ShakeTreePopulate(lyt)


        treeCtrl.OnSelect.Add(self.TreeSelectCallback)
        #treeCtrl.OnClickCheck.Add(self.TreeCheckCallback)

    def PopulateFolderList(self, pMain):

        mSpreadFolderList = FBSpread()
        self.mSpreadFolderListRegion = mSpreadFolderList
        mSpreadFolderList.Caption        = ".csf File List";
        mSpreadFolderList.MultiSelect    = False;

        self.AddShakeFileList(mSpreadFolderList)

        pMain.AddRegion("csfSpread","csfSpread",
                        FBAddRegionParam(145,FBAttachType.kFBAttachLeft,"camliblabel"),
                        FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"camliblabel"),
                        FBAddRegionParam(465,FBAttachType.kFBAttachNone,""),                 
                        FBAddRegionParam(275,FBAttachType.kFBAttachNone,""))
        pMain.SetBorder("csfSpread",FBBorderStyle.kFBStandardBorder,False, False, 1, 0, 20, 0)


        self.mSpreadFolderListRegion.OnRowClick.Add(self._on_SelectCSF) 
        self.mSpreadFolderListRegion.OnCellChange.Add( self._on_CellSelected )


        pMain.SetControl("csfSpread",mSpreadFolderList) 

        return mSpreadFolderList 

    def refreshSliderValues(self, Control, controlName):
        sliderControl = self.CameraControls.GetControl(controlName)
        if 'CoC' in Control.Name:
            sliderControl.Value = int( Control.Value )
        else:
            sliderControl.Value = Control.Value


    def refreshSPinnerValues(self,Control): 
        labelControl = self.CameraControls.GetControl("spinner"+Control.Name)

        # toolbox v2.0  updates url:bugstar:1733892 
        if 'CoC' in Control.Name:
            if Control.Value == 0:
                if 'Override' in Control.Name:
                    labelControl.Value = int( Control.Value )

                else:
                    cocProp = self.ActiveCamera.PropertyList.Find( Control.Name )
                    labelControl.Value = int( cocProp.Data ) 
            else:
                labelControl.Value = int( Control.Value )
        else:
            labelControl.Value = Control.Value

    def getControl(self, Name):
        return (self.CameraControls.GetControl(Name))

    # OK so we dynmcially create the controls for the camera    
    def refreshCamControlsView(self):
        if ( self.ActiveCamera ):
            
            self.CameraControls.Enabled = True
            self.__removeCamControls__()

            if self.TabIndex != 1:
                self.__removeBrowserControls__()
            self.__AddControls__()

        else:
            print self.ActiveCamera
            
            self.CameraControls.Enabled = False            

        return  

    """
          -----------------   CALLBACKS    -----------------------
    """
    def TreeSelectCallback(self, control, event):

        if gDevelopment:
            print "TreeSelectCallback"
        self.CSFpath = self.shakePath + event.TreeNode.LongName + event.TreeNode.Name
        self.shakeRelativePath = event.TreeNode.LongName + event.TreeNode.Name
        if gDevelopment:
            print self.CSFpath, self.shakeRelativePath
        self.CsfFileControl.Clear()
        self.AddShakeFileList(self.mSpreadFolderListRegion)

        # Populate the camera shake files of the selected folder
        self.PopulateCameraLibFiles(control, event, self.CSFpath)

        self.CsfFileControl.Refresh()

    """
          -----------------   CAM SETTINGS    -----------------------
      TODO:
      Modulize this bitch!!!

    """

    def rs_settings(self, pMain):        

        lCheckSwitchCameraButton = FBButton()
        lCheckSwitchCameraButton.Style = FBButtonStyle.kFB2States
        lCheckSwitchCameraButton.Look = FBButtonLook.kFBLookNormal 

        lCheckSwitchCameraButton.State = self.switchCamera 

        pMain.AddRegion("lCheckSwitchCameraButton","lCheckSwitchCameraButton",
                        FBAddRegionParam(0,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(6,FBAttachType.kFBAttachNone,""),
                        FBAddRegionParam(20,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("lCheckSwitchCameraButton",lCheckSwitchCameraButton) 

        lCheckSwitchCameraButton.OnClick.Add(self.ShakeCheckSwitchCamera)  

        # Weight label
        label = FBLabel()
        label.Style = FBTextStyle.kFBTextStyleBold
        label.Caption = "Enable Auto Switch View to Camera "     
        pMain.AddRegion("label" ,"label" ,
                        FBAddRegionParam(4,FBAttachType.kFBAttachRight,"lCheckSwitchCameraButton"), 
                        FBAddRegionParam(4,FBAttachType.kFBAttachNone,""),
                        FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")) 
        pMain.SetControl("label" ,label)

        # Turn on/off auto planes show
        lCheckSwitchPlanesButton = FBButton()
        lCheckSwitchPlanesButton.Style = FBButtonStyle.kFB2States
        lCheckSwitchPlanesButton.Look = FBButtonLook.kFBLookNormal 

        lCheckSwitchPlanesButton.State = self.AutoViewPlanes 

        pMain.AddRegion("lCheckSwitchPlanesButton","lCheckSwitchPlanesButton",
                        FBAddRegionParam(0,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(6,FBAttachType.kFBAttachBottom,"lCheckSwitchCameraButton"),
                        FBAddRegionParam(20,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("lCheckSwitchPlanesButton",lCheckSwitchPlanesButton) 

        lCheckSwitchPlanesButton.OnClick.Add(self.ShakePlanesSwitchCamera)  

        # Weight label
        mlabel = FBLabel()
        mlabel.Style = FBTextStyle.kFBTextStyleBold
        mlabel.Caption = "Enable Auto Show Camera Planes"     
        pMain.AddRegion("mlabel" ,"mlabel" ,
                        FBAddRegionParam(4,FBAttachType.kFBAttachRight,"lCheckSwitchPlanesButton"), 
                        FBAddRegionParam(4,FBAttachType.kFBAttachBottom,"lCheckSwitchCameraButton"),
                        FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")) 
        pMain.SetControl("mlabel" ,mlabel)        



    """
    CAM FADE
    """    
    def rs_CamFadeControls(self, pMain): 
        # Fade In Button
        lAddFadeInButton = FBButton()
        lAddFadeInButton.Caption = "Add Fade In Key"
        lAddFadeInButton.Look = FBButtonLook.kFBLookPush
        pMain.AddRegion("lAddFadeInButton","lAddFadeInButton",
                        FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                        FBAddRegionParam(10,FBAttachType.kFBAttachNone,""),
                        FBAddRegionParam(230,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("lAddFadeInButton",lAddFadeInButton) 
        lAddFadeInButton.OnClick.Add(self.SetFadeInKey)   

        # Fade Out Button
        lAddFadeOutButton = FBButton()
        lAddFadeOutButton.Caption = "Add Fade Out Key"
        lAddFadeOutButton.Look = FBButtonLook.kFBLookPush
        pMain.AddRegion("lAddFadeOutButton","lAddFadeOutButton",
                        FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                        FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"lAddFadeInButton"),
                        FBAddRegionParam(230,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("lAddFadeOutButton",lAddFadeOutButton) 
        lAddFadeOutButton.OnClick.Add(self.SetFadeOutKey)    


        mFadeSpreadView = FBSpread()
        mFadeSpreadView.Caption        = "ShotList";
        mFadeSpreadView.MultiSelect    = True;

        pMain.AddRegion("mFadeSpreadView","mFadeSpreadView",
                        FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""),
                        FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"lAddFadeOutButton"),
                        FBAddRegionParam(230,FBAttachType.kFBAttachNone,""),
                        FBAddRegionParam(200,FBAttachType.kFBAttachNone,""))    
        mFadeSpreadView.ColumnAdd( "Fade Type",1 );

        mFadeSpreadView.GetColumn( -1 ).Width = 100
        mFadeSpreadView.GetColumn( 0 ).Width = 118
        pMain.SetControl("mFadeSpreadView",mFadeSpreadView)   

        return


    """
     -----------------   UTILS    -----------------------
     TODO:
     Modulize this bitch!!!

    """ 
    def rs_utils(self, pMain):
        mTakeList = FBList()
        mTakeList.Style = FBListStyle.kFBVerticalList
        pMain.AddRegion("takeList","takeList",
                        FBAddRegionParam(0,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(5,FBAttachType.kFBAttachTop,""),
                        FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(140,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("takeList",mTakeList)  
        setattr( self, "on_takeList_selected", mTakeList )   


        lSystem = FBSystem()
        for lTake in lSystem.Scene.Takes:
            mTakeList.Items.append(lTake.Name)




        buttonlist = [
            ['lCopySwitchertoTakeButton','Copy Camera Switcher',10]
        ]
        anchor = "takeList"
        for pBut in buttonlist:
            butUtilId = pBut[0]            
            UtilFFButton = FBButton()
            UtilFFButton.Name = butUtilId
            UtilFFButton.Caption = pBut[1]
            UtilFFButton.Look = FBButtonLook.kFBLookPush
            lspc = pBut[2]

            pMain.AddRegion(butUtilId,butUtilId,
                            FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                            FBAddRegionParam(lspc,FBAttachType.kFBAttachBottom, anchor),
                            FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                            FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))

            pMain.SetControl(butUtilId,UtilFFButton) 
            UtilFFButton.OnClick.Add(self._event_CamButtonControl)
            anchor = butUtilId            

        return



    """
          -----------------   SHAKE UI 2.0    -----------------------
      TODO:
      Modulize this bitch!!!

    """
    def rs_CamShakeControls(self, pMain):

        #Component checkboxes
        component_grid = FBGridLayout()
        component_grid_col = 0

        #Translation label
        translation_label = FBLabel()
        translation_label.Style = FBTextStyle.kFBTextStyleBold
        translation_label.Caption = "Translation"     

        component_grid.AddRange( translation_label, 0, 0, 0, 2 )

        #Translation checkboxes
        translation_button_label_list = ["X","Y","Z"]

        for translation_button_label in translation_button_label_list:
            current_translation_label = FBLabel()
            current_translation_label.Style = FBTextStyle.kFBTextStyleBold
            current_translation_label.Caption = translation_button_label   

            component_grid.Add( current_translation_label, 1, component_grid_col )

            current_translation_button = FBButton()
            current_translation_button.State = True
            current_translation_button.Style = FBButtonStyle.kFB2States
            current_translation_button.Look = FBButtonLook.kFBLookNormal 

            component_grid.Add( current_translation_button, 2, component_grid_col, FBAttachType.kFBAttachLeft, FBAttachType.kFBAttachTop )

            #Add the button as a property so that it can be queried
            setattr( self, "translation_button_" + translation_button_label, current_translation_button )            
            component_grid_col += 1          

        #Rotation label
        rotation_label = FBLabel()
        rotation_label.Style = FBTextStyle.kFBTextStyleBold
        rotation_label.Caption = "Rotation"     

        component_grid.AddRange( rotation_label, 0, 0, 3, 5 )        

        #Rotation checkboxes
        rotation_button_label_list = ["X","Y","Z"]

        for rotation_button_label in rotation_button_label_list:
            current_rotation_label = FBLabel()
            current_rotation_label.Style = FBTextStyle.kFBTextStyleBold
            current_rotation_label.Caption = rotation_button_label   

            component_grid.Add( current_rotation_label, 1, component_grid_col )

            current_rotation_button = FBButton()
            current_rotation_button.State = True
            current_rotation_button.Style = FBButtonStyle.kFB2States
            current_rotation_button.Look = FBButtonLook.kFBLookNormal 

            component_grid.Add( current_rotation_button, 2, component_grid_col, FBAttachType.kFBAttachLeft, FBAttachType.kFBAttachTop )

            #Add the button as a property so that it can be queried
            setattr( self, "rotation_button_" + rotation_button_label, current_rotation_button )            
            component_grid_col += 1          

        #FOV label
        fov_label = FBLabel()
        fov_label.Style = FBTextStyle.kFBTextStyleBold
        fov_label.Caption = "FOV"  

        component_grid.AddRange( fov_label, 1, 1, 6, 6 )   

        #FOV checkbox
        current_fov_button = FBButton()
        current_fov_button.State = True
        current_fov_button.Style = FBButtonStyle.kFB2States
        current_fov_button.Look = FBButtonLook.kFBLookNormal 

        component_grid.Add( current_fov_button, 2, 6, FBAttachType.kFBAttachLeft, FBAttachType.kFBAttachTop )    

        setattr( self, "fov_button", current_fov_button )  

        pMain.AddRegion("translation_label", "translation_label",
                        FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"lRemoveSelShakeButton"), 
                        FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"lRemoveSelShakeButton"),
                        FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(55,FBAttachType.kFBAttachNone,""))

        pMain.SetControl("translation_label",component_grid)      

        """
        IF I was smart I would put this into a fucntion
        """
        anchor = "shakeList"

        lShakeButtonCamlist = [['lCreateShakeSelButton','Apply Cam Shake to Selected Cam',0],
                               ['lRemoveSelShakeButton','Remove Cam Shake on Selected Cam',34],
                               ['lCreateShakeButton','Apply Cam Shake to ALL Cameras',88],                          
                               ['lRemoveShakeButton','Remove ALL Cam Shake',34],
                               ['lExportCameraToShake','Export Camera as Shake Data',120]                   
                               ]

        for pBut in lShakeButtonCamlist:
            butId = pBut[0]            
            newFFButton = FBButton()
            newFFButton.Name = butId
            newFFButton.Caption = pBut[1]
            newFFButton.Look = FBButtonLook.kFBLookPush
            lspc = pBut[2]

            pMain.AddRegion(butId,butId,
                            FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                            FBAddRegionParam(lspc,FBAttachType.kFBAttachTop,anchor),
                            FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                            FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))

            pMain.SetControl(butId,newFFButton) 
            newFFButton.OnClick.Add(self._event_CamButtonControl)
            anchor = butId        



        # Weight label
        labelweight = FBLabel()
        labelweight.Style = FBTextStyle.kFBTextStyleBold
        labelweight.Caption = "Shake Phase: 1% "     
        pMain.AddRegion("labelweight" ,"labelweight" ,
                        FBAddRegionParam(0,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"lRemoveShakeButton"),
                        FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")) 
        pMain.SetControl("labelweight" ,labelweight)
        self.ShakeLabelControl = labelweight



        # Phase Slider Control        
        hsSlider = FBSlider()    
        hsSlider.Orientation = FBOrientation.kFBHorizontal   
        hsSlider.SmallStep = 1
        hsSlider.LargeStep = 2 
        hsSlider.Max = 100
        hsSlider.Min = 1
        hsSlider.Value = 1

        hsSlider.Name = "Shake Phase"
        pMain.AddRegion("hsSlider","hsSlider",
                        FBAddRegionParam(0,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(-5,FBAttachType.kFBAttachBottom,"labelweight"),
                        FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(30,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("hsSlider",hsSlider) 
        #hsSlider.Enabled = False
        hsSlider.OnChange.Add(self.ShakeWeight)



        # Live Update Check Button
        lCheckShakeSelButton = FBButton()        
        lCheckShakeSelButton.Caption = "Enable Live Update"
        lCheckShakeSelButton.Style = FBButtonStyle.kFB2States
        #lCheckShakeSelButton.Look = FBButtonLook.kFBLookNormal        

        pMain.AddRegion("lCheckShakeSelButton","lCheckShakeSelButton",
                        FBAddRegionParam(-2,FBAttachType.kFBAttachLeft,""), 
                        FBAddRegionParam(-2,FBAttachType.kFBAttachBottom,"hsSlider"),
                        FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("lCheckShakeSelButton",lCheckShakeSelButton) 
        lCheckShakeSelButton.OnClick.Add(self.ShakeCheckLiveUpdate)   


        # Check Button: Disbale local Transform
        lDisableExportLocals = FBButton()
        lDisableExportLocals.Caption = "Export Local Transform"
        lDisableExportLocals.Style = FBButtonStyle.kFB2States
        #lDisableExportLocals.Look = FBButtonLook.kFBLookPush
        lDisableExportLocals.State = True
        #lDisableExportLocals.Look = FBButtonLook.kFBLookColorChange
        pMain.AddRegion("lDisableExportLocals","lDisableExportLocals",
                        FBAddRegionParam(-2,FBAttachType.kFBAttachLeft,""), 
                        FBAddRegionParam(-2,FBAttachType.kFBAttachBottom,"lExportCameraToShake"),
                        FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("lDisableExportLocals",lDisableExportLocals) 
        lDisableExportLocals.OnClick.Add(self.DisableExportLocalTransforms)  

##        return mShakeList


    # Set our tabs
    def rs_CameraPopulateUI(self, pMain): 

        lTabControl = FBTabControl()
        lTabControl.TabPanel.TabStyle = 0
        pMain.AddRegion("lTabControl","lTabControl",
                        FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                        FBAddRegionParam(62,FBAttachType.kFBAttachTop,""),
                        FBAddRegionParam(250,FBAttachType.kFBAttachLeft,""),
                        FBAddRegionParam(360,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("lTabControl", lTabControl)

        # Add our layouts
        # Camera TAB
        lCameraTabContainer = FBLayout() 
        lCameraTabContainer.AddRegion("lCameraTabContainer","lCameraTabContainer",
                                      FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                                      FBAddRegionParam(0,FBAttachType.kFBAttachTop,""),
                                      FBAddRegionParam(0,FBAttachType.kFBAttachNone,""),
                                      FBAddRegionParam(0,FBAttachType.kFBAttachNone,""))    
        lTabControl.Add("Camera",lCameraTabContainer) 

        # Shake TAB
        lCameraShakeTabContainer = FBLayout() 
        lCameraShakeTabContainer.AddRegion("lCameraShakeTabContainer","lCameraShakeTabContainer",
                                           FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                                           FBAddRegionParam(0,FBAttachType.kFBAttachTop,""),
                                           FBAddRegionParam(0,FBAttachType.kFBAttachNone,""),
                                           FBAddRegionParam(0,FBAttachType.kFBAttachNone,""))    
        lTabControl.Add("Shake",lCameraShakeTabContainer) 

        # Fade Tab (Not enabled)
        lCameraFadeTabContainer = FBLayout() 
        lCameraFadeTabContainer.AddRegion("lCameraFadeTabContainer","lCameraFadeTabContainer",
                                          FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                                          FBAddRegionParam(0,FBAttachType.kFBAttachTop,""),
                                          FBAddRegionParam(0,FBAttachType.kFBAttachNone,""),
                                          FBAddRegionParam(0,FBAttachType.kFBAttachNone,""))    
        #lTabControl.Add("Fade",lCameraFadeTabContainer)         

        # Utils Tab
        lCameraUtilTabContainer = FBLayout() 
        lCameraUtilTabContainer.AddRegion("lCameraUtilTabContainer","lCameraUtilTabContainer",
                                          FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                                          FBAddRegionParam(0,FBAttachType.kFBAttachTop,""),
                                          FBAddRegionParam(0,FBAttachType.kFBAttachNone,""),
                                          FBAddRegionParam(0,FBAttachType.kFBAttachNone,"")) 
        lTabControl.Add("Utils",lCameraUtilTabContainer)  

        # Settings Tab        
        lCameraSettingsTabContainer = FBLayout() 
        lCameraSettingsTabContainer.AddRegion("lCameraSettingsTabContainer","lCameraSettingsTabContainer",
                                              FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                                              FBAddRegionParam(0,FBAttachType.kFBAttachTop,""),
                                              FBAddRegionParam(0,FBAttachType.kFBAttachNone,""),
                                              FBAddRegionParam(0,FBAttachType.kFBAttachNone,""))    
        lTabControl.Add("Settings",lCameraSettingsTabContainer)         




        lTabControl.SetContent(0) 
        lTabControl.TabPanel.OnChange.Add(self.rs_TabsChanged)


        #Disabled For now
        #self.rs_CamFadeControls(lCameraFadeTabContainer)

        # Add our Shake Control and create a Reference
        self.pShakeListControl =  self.rs_CamShakeControls(lCameraShakeTabContainer) 

        # Add our Camera Util
        self.rs_utils(lCameraUtilTabContainer)

        # Add our Settings Control
        self.rs_settings(lCameraSettingsTabContainer)

        #  adding scroll box 1915046
        scroll = FBScrollBox()
        scrollX = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"" )
        scrollY = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"" )
        scrollW = FBAddRegionParam( 0,FBAttachType.kFBAttachRight,"" )
        scrollH = FBAddRegionParam( 0,FBAttachType.kFBAttachBottom,"" )
        lCameraTabContainer.AddRegion( "scroll","scroll", scrollX, scrollY, scrollW, scrollH )

        lScrollContentX = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"scroll" )
        lScrollContentW = FBAddRegionParam( 0,FBAttachType.kFBAttachRight,"scroll" )
        
        lCameraTabContainer.SetControl( "scroll", scroll )   
        scroll.SetContentSize( 222, 650 )
        # layout inside of the scrollbox, buttons are put inside the content layout
        layoutContent = FBLayout()
        layoutContentX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"scroll")
        layoutContentY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"scroll")
        layoutContentW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"scroll")
        layoutContentH = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"scroll")
        scroll.Content.AddRegion("layoutContent", "layoutContent", layoutContentX, layoutContentY, layoutContentW, layoutContentH)
        scroll.Content.SetControl("layoutContent", layoutContent)
        

        # I can condense all this crap into a list of names
        anchor = ""
        lButtonCamlist = [['lCreateNewButton','Create New Camera',30],
                          ['lCreateManyButton','Create Mulitple Cameras', 50],
                          ['lCreateNewExportButton','Create New Export Camera',50],
                          ['lUpdateCamerasButton','Update All Cameras',39],                          
                          ['lDelCameraButton','Delete Selected Camera',39],                          
                          ['lPlotButton','Plot Switcher To Export Camera',39],     
                          ['lPlotTakesButton','Plot Switcher To ALL Takes Export Camera',25],  
                          ['lRemoveNonSwitcherCameraButton',"Remove Non-Switcher Cameras",39],  
                          ['lHideAllPlanesButton',"Hide all DOF Planes",39],
                          ['lShowAllPlanesButton',"Show all DOF Planes",25],
                          ['lHideSelectPlanesButton',"Hide Selected Cam's DOF Planes",25],
                          ['lShowSelectPlanesButton',"Show Selected Cam's DOF Planes",25],
                          ['lShowCamerasButton',"Show Just Cameras (Debug)",39],
                          ['lSelectSwitcherButton',"Select Switcher",25],
                          ['lLockCameraButton',"All Cameras are Unlocked",39], 
                          ['lLockPlanesButton',"All Camera Planes are Unlocked",25],
                          ['lForceUpdateCamerasButton','Force Update All Constraints',39]]

        for pBut in lButtonCamlist:
            butId = pBut[0]            
            newFFButton = FBButton()
            newFFButton.Name = butId
            newFFButton.Caption = pBut[1]
            # Need to do these for the special buttons
            if pBut[1] == "All Cameras are Unlocked":
                newFFButton.Look = FBButtonLook.kFBLookColorChange
                newFFButton.SetStateColor(FBButtonState.kFBButtonState0, self.GreyButton)
                newFFButton.SetStateColor(FBButtonState.kFBButtonState1, self.GreyButton)             
                lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
                # all re-opening logic
                if lReferenceNull:
                    lCamProp = lReferenceNull.PropertyList.Find('LockCamera')
                    if lCamProp:                
                        # if you re-open the UI, you need to make sure the UI still reflects that setting that were there                        
                        if lCamProp.Data:
                            newFFButton.State = 1
                            newFFButton.Caption = 'All Cameras are Locked'
                            # Need this for when the UI is re-opened.
                            newFFButton.SetStateColor(FBButtonState.kFBButtonState0, self.RedButton)
                            newFFButton.SetStateColor(FBButtonState.kFBButtonState1, self.RedButton)                                                    

            elif pBut[1] == "All Camera Planes are Unlocked":
                newFFButton.Look = FBButtonLook.kFBLookColorChange
                newFFButton.SetStateColor(FBButtonState.kFBButtonState0, self.GreyButton)
                newFFButton.SetStateColor(FBButtonState.kFBButtonState1, self.GreyButton)             
                lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
                # all re-opening logic
                if lReferenceNull:
                    lCamProp = lReferenceNull.PropertyList.Find('LockPlane')
                    if lCamProp:                
                        if lCamProp.Data:
                            newFFButton.State = 1
                            newFFButton.Caption = 'All Camera Planes are Locked'    
                            # Need this for when the UI is re-opened.
                            newFFButton.SetStateColor(FBButtonState.kFBButtonState0, self.RedButton)
                            newFFButton.SetStateColor(FBButtonState.kFBButtonState1, self.RedButton)                            
            # For every button but     lLockCameraButton and lLockPlanesButton        
            else:
                newFFButton.Look = FBButtonLook.kFBLookPush
            
            lspc = pBut[2]
            layoutContent.AddRegion(butId,butId,
                                          FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                                          FBAddRegionParam(lspc, FBAttachType.kFBAttachTop,anchor),
                                          FBAddRegionParam(220,FBAttachType.kFBAttachNone,""), 
                                          FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))

            layoutContent.SetControl(butId,newFFButton) 
            if pBut[1] == "All Cameras are Unlocked":
                # newFFButton.OnClick.Add(core.camUpdate.lockCameras)
                pass
            elif pBut[1] == "All Camera Planes are Unlocked":
                # newFFButton.OnClick.Add(core.camUpdate.lockPlanes)
                pass
            else: 
                newFFButton.OnClick.Add(self._event_CamButtonControl)
            anchor = butId        


        # adding in lens buttons url:bugstar:1510245  
        lensList = ['16mm', 
                    '25mm', 
                    '35mm', 
                    '50mm', 
                    '75mm',
                    '120mm']

        buttonGroup = FBButtonGroup()
        counter = 0
        for i in range( len( lensList ) ):
            lensButton = FBButton()
            lensButton.Caption = lensList[i]
            lensButton.Name = str( i )
            lensButton.Hint = 'sets this lens as default\nfor new camera created'
            lensButton.Style = FBButtonStyle.kFB2States
            lensButton.Look = FBButtonLook.kFBLookColorChange
            lensButton.SetStateColor( FBButtonState.kFBButtonState0,FBColor( .30, .30, .30 ) )
            lensButton.Justify = FBTextJustify.kFBTextJustifyCenter 
            
            X = FBAddRegionParam( counter, FBAttachType.kFBAttachLeft, "" )
            Y = FBAddRegionParam( 60, FBAttachType.kFBAttachTop, "" )
            if lensList[i] == '120mm':
                W = FBAddRegionParam( 37, FBAttachType.kFBAttachNone, "" )
            else:
                W = FBAddRegionParam( 31, FBAttachType.kFBAttachNone, "" )
            H = FBAddRegionParam( 12, FBAttachType.kFBAttachNone, "" )
            
            layoutContent.AddRegion( lensList[i], lensList[i], X, Y, W, H )
            layoutContent.SetControl( lensList[i], lensButton )  

            counter = counter + 37

            buttonGroup.Add( lensButton )
            
            self.lenButtonList.append( lensButton )
            
            if lensList[i] == self.LensSetting:
                lensButton.State = True
                        
            lensButton.OnClick.Add( self.CheckLensSetting )
            
        # adding sync button to toggle MB/toolbox camera selection on and off (callback) url:bugstar:1815189
        syncCamsButton = FBButton()
        syncCamsButton.Style = FBButtonStyle.kFBCheckbox
        syncCamsButton.Caption = "Sync Navigator Cam Selection" 
        syncCamsButton.Hint = 'Toggles the sync callback on and off.\n\nOn: When you select a camera in the navigator,\nit will automatically select the camera in\nthe toolbox.\nOff: Callback is turned off - can increase speed of\nworking in a file when it is off.'
        X = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        Y = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        W = FBAddRegionParam(219,FBAttachType.kFBAttachNone,"")
        H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        layoutContent.AddRegion("syncCamsButton","syncCamsButton", X, Y, W, H)
        layoutContent.SetControl("syncCamsButton",syncCamsButton)
        syncCamsButton.OnClick.Add(self.SyncToggle)


    def RefreshUI(self, pControl, pEvent):
        self.SwitcherMonitor.Unregister()
        self.CameraCallbackEvent = None         
        self.Show( True )

    def CamDataManager(self, pControl, pEvent):
        global tool

        # Get userChoice
        userChoice = pControl.Caption

        if userChoice == "Load Cams":
            camKeyTasks.LoadCamsInterface()

        # Save Key Data (if user chose to)
        if userChoice in ["Lock & Save", "Send Capture"]:
            camDataSaved = SaveCameras.Run()

            # Queue Capture (if user chose to)
            if camDataSaved and userChoice == "Send Capture":
                CaptureRender.QueueCaptureRender()

        # Lock Cams and Close Toolbox
        if userChoice in ["Lock Cams", "Lock & Save"]:
            camLockObj = CameraLocker.LockManager()
            camLockObj.setAllCamLocks(True)
            FBDestroyTool(tool)
    
    def SyncToggle( self, control, event ):   
        
        if control.State == 1:
            # callback on
            # get and add the camera select callback url:bugstar:1815189
            self.SwitcherMonitor.Unregister()
            self.SwitcherMonitor.Register()
            self.CameraCallbackEvent = FBSystem().Scene.OnChange
            self.CameraCallbackEvent.RemoveAll()
            self.CameraCallbackEvent.Add( self.CameraSelectCallback )

        else:
            # callback off
            self.SwitcherMonitor.Unregister()
            self.CameraCallbackEvent.RemoveAll()  
            self.CameraCallbackEvent = None 
            self.RefreshUI( None, None )
        
    
    def CreateRefreshButton(self):

        lRefreshUIButton = FBButton()
        lRefreshUIButton.Caption = "Click to Refresh UI"
        lRefreshUIButton.Justify = FBTextJustify.kFBTextJustifyCenter  
        lRefreshUIButtonX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lRefreshUIButtonY = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")# Allows for the Rockstar Banner which is 30 high
        lRefreshUIButtonW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lRefreshUIButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        self.AddRegion("lRefreshUIButton","lRefreshUIButton", lRefreshUIButtonX, lRefreshUIButtonY, lRefreshUIButtonW, lRefreshUIButtonH)
        self.SetControl("lRefreshUIButton",lRefreshUIButton)
        lRefreshUIButton.OnClick.Add(self.RefreshUI) 

    def PopulateCSFFileListFromFolder(self, mFolder, fileList):

        CsfFileControl = self.CsfFileControl
        row = 0
        if gDevelopment:
            print "PopulateCSFFileListFromFolder"
            print mFolder, fileList
        for fname in fileList:
            a = mFolder + '/'
            fullFileName = (a + fname)
            fullFileName = (mFolder + '/' + fname)
            fsize = os.path.getsize(fullFileName)
            modTime = datetime.fromtimestamp(os.path.getmtime(fullFileName))
            mTime = modTime.strftime("%m/%d/%Y %H:%M:%S")

            CsfFileControl.RowAdd(fname, row)
            CsfFileControl.SetCellValue(row, 0, str(fsize) + " bytes")
            CsfFileControl.SetCellValue(row, 1, str(mTime))
            CsfFileControl.SetCellValue(row, 2, fname)

            row +=1
        return

    def CreateShakeAndDOFRegion(self, pMain):

        pMain.AddRegion("Border1","Camera DOF Border",
                        FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"lTabControl"), 
                        FBAddRegionParam(-450,FBAttachType.kFBAttachBottom,""),
                        FBAddRegionParam(650,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(440,FBAttachType.kFBAttachNone,""))

        pMain.SetBorder("Border1",FBBorderStyle.kFBStandardBorder,False, False,1,0,20,0)

        return pMain

    def DOFLayout(self, pMain):
        if gDevelopment:
            print "DOF Layout got called"
        # Create a tab layout
        lyt = FBLayout()        

        lyt.AddRegion("DOFLayout","Camera DOF Controls",
                      FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"Border1"), 
                      FBAddRegionParam(10,FBAttachType.kFBAttachTop,"Border1"),
                      FBAddRegionParam(630,FBAttachType.kFBAttachNone,""), 
                      FBAddRegionParam(420,FBAttachType.kFBAttachNone,""))

        lyt.SetBorder("DOFLayout",FBBorderStyle.kFBStandardBorder,False, True,1,0,20,0)


        pMain.SetControl("Border1",lyt)
        if gDevelopment:
            print self.ActiveCamera
        if (self.ActiveCamera):
            self.__removeBrowserControls__()

        return  lyt

    def CameraLibraryLayout(self, pMain):

        #main layout to keep the Camera Shake Library stuff in        
        camMainLyt = FBLayout()        

        camMainLyt.AddRegion("camlibRegion" ,"Camera Library" ,
                             FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"Border1"), 
                             FBAddRegionParam(20,FBAttachType.kFBAttachTop,"Border1"),
                             FBAddRegionParam(635,FBAttachType.kFBAttachNone,""), 
                             FBAddRegionParam(280,FBAttachType.kFBAttachNone,""))

##        camMainLyt.SetBorder("camlibRegion", FBBorderStyle.kFBStandardBorder,True, True,1,0,20,0)        

        pMain.SetControl("camlibRegion" ,camMainLyt)        

        return  camMainLyt   


    def Create( self, mainLyt ):

        # Temporarly removed, because when we switched out code to be run right away on startup so that our tools can be remembered in Layout, 
        # but now the negative is this callback code get run when MotionBuilder launches, so anytime you open a file, you can the Camera Toolbox loading...need to come up with a solution - Kristine M.
        #self.OnUnbind.Add(self.OnUnbindEvent)    
        #self.FileNewCompleted(True)            

        lRefreshUIButton = FBButton()
        lRefreshUIButton.Caption = "Click to Refresh UI"
        lRefreshUIButton.Justify = FBTextJustify.kFBTextJustifyCenter  
        lRefreshUIButtonX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lRefreshUIButtonY = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"") # Allows for the Rockstar Banner which is 30 high
        lRefreshUIButtonW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lRefreshUIButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("lRefreshUIButton","lRefreshUIButton", lRefreshUIButtonX, lRefreshUIButtonY, lRefreshUIButtonW, lRefreshUIButtonH)
        mainLyt.SetControl("lRefreshUIButton",lRefreshUIButton)
        lRefreshUIButton.OnClick.Add(self.RefreshUI)

        camsUnlockedText = FBLabel()
        camsUnlockedText.Caption = "{0}  Cam Data Manager  {0}".format("-"*20)
        camsUnlockedText.Style = FBTextStyle.kFBTextStyleBold
        camsUnlockedText.WordWrap = True
        camsUnlockedTextX = FBAddRegionParam(297,FBAttachType.kFBAttachLeft,"")
        camsUnlockedTextY = FBAddRegionParam(56,FBAttachType.kFBAttachTop,"")
        camsUnlockedTextW = FBAddRegionParam(500,FBAttachType.kFBAttachNone,"")
        camsUnlockedTextH = FBAddRegionParam(15,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("camsUnlockedText","camsUnlockedText", camsUnlockedTextX, camsUnlockedTextY, camsUnlockedTextW, camsUnlockedTextH)
        mainLyt.SetControl("camsUnlockedText",camsUnlockedText)

        loadCamsButton = FBButton()
        loadCamsButton.Justify = FBTextJustify.kFBTextJustifyCenter
        loadCamsButton1X = FBAddRegionParam(290,FBAttachType.kFBAttachLeft,"")
        loadCamsButton1Y = FBAddRegionParam(2,FBAttachType.kFBAttachBottom,"camsUnlockedText")
        loadCamsButton1W = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
        loadCamsButton1H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("loadCamsButton","loadCamsButton", loadCamsButton1X, loadCamsButton1Y, loadCamsButton1W, loadCamsButton1H)
        mainLyt.SetControl("loadCamsButton",loadCamsButton)
        loadCamsButton.Look = FBButtonLook.kFBLookColorChange
        loadCamsButton.SetStateColor(FBButtonState.kFBButtonState0, FBColor(0.0, 0.1, 0.5))
        loadCamsButton.Caption = "Load Cams"
        loadCamsButton.OnClick.Add(self.CamDataManager)

        lockCamsButton1 = FBButton()
        lockCamsButton1.Justify = FBTextJustify.kFBTextJustifyCenter
        lockCamsButton1X = FBAddRegionParam(460,FBAttachType.kFBAttachLeft,"")
        lockCamsButton1Y = FBAddRegionParam(2,FBAttachType.kFBAttachBottom,"camsUnlockedText")
        lockCamsButton1W = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
        lockCamsButton1H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("lockCamsButton1","lockCamsButton1", lockCamsButton1X, lockCamsButton1Y, lockCamsButton1W, lockCamsButton1H)
        mainLyt.SetControl("lockCamsButton1",lockCamsButton1)
        lockCamsButton1.Look = FBButtonLook.kFBLookColorChange
        lockCamsButton1.SetStateColor(FBButtonState.kFBButtonState0, FBColor(0.4, 0.1, 0.1))
        lockCamsButton1.Caption = "Lock Cams"
        lockCamsButton1.OnClick.Add(self.CamDataManager)

        lockCamsButton2 = FBButton()
        lockCamsButton2.Justify = FBTextJustify.kFBTextJustifyCenter
        lockCamsButton2X = FBAddRegionParam(545,FBAttachType.kFBAttachLeft,"")
        lockCamsButton2Y = FBAddRegionParam(2,FBAttachType.kFBAttachBottom,"camsUnlockedText")
        lockCamsButton2W = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
        lockCamsButton2H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("lockCamsButton2","lockCamsButton2", lockCamsButton2X, lockCamsButton2Y, lockCamsButton2W, lockCamsButton2H)
        mainLyt.SetControl("lockCamsButton2",lockCamsButton2)
        lockCamsButton2.Look = FBButtonLook.kFBLookColorChange
        lockCamsButton2.SetStateColor(FBButtonState.kFBButtonState0, FBColor(0.6, 0.1, 0.1))
        lockCamsButton2.Caption = "Lock & Save"
        lockCamsButton2.OnClick.Add(self.CamDataManager)

        lockCamsButton3 = FBButton()
        lockCamsButton3.Justify = FBTextJustify.kFBTextJustifyCenter
        lockCamsButton3X = FBAddRegionParam(375,FBAttachType.kFBAttachLeft,"")
        lockCamsButton3Y = FBAddRegionParam(2,FBAttachType.kFBAttachBottom,"camsUnlockedText")
        lockCamsButton3W = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")
        lockCamsButton3H = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("lockCamsButton3","lockCamsButton3", lockCamsButton3X, lockCamsButton3Y, lockCamsButton3W, lockCamsButton3H)
        mainLyt.SetControl("lockCamsButton3",lockCamsButton3)
        lockCamsButton3.Look = FBButtonLook.kFBLookColorChange
        lockCamsButton3.SetStateColor(FBButtonState.kFBButtonState0, FBColor(0.0, 0.5, 0.1))
        lockCamsButton3.Caption = "Send Capture"
        lockCamsButton3.OnClick.Add(self.CamDataManager)
        
        self.rs_CameraPopulateUI( mainLyt ) # main tool
        #self.rs_collapseControls(lCameraUI)
        self.pListView = self.rs_spreadsheet( mainLyt ) # SpreadsheetVIewer

        # Curve Editor
        #self.pFCurveView = self.rs_fCurveView(mainLyt) 

        #Add the Camera DOF/Shake Browser Control Region
        self.BrowserControls = self.CreateShakeAndDOFRegion(mainLyt)
        print self.BrowserControls.Name

        self.CameraControls = self.DOFLayout(self.BrowserControls)
        self.CameraControls.Enabled = False 

        # Refresh our Dynnamic DOF Controls
        self.refreshCamControlsView()


    ###################################################
    ##           EVENT HANDLERS
    ################################################### 
    """
    TABS
    """
    # Future function
    def rs_TabsChanged(self, pControl, pEvent):
        if gDevelopment:
            print "rs_TabsChanged"
        self.__removeCamControls__()
        if pControl.ItemIndex == 1:
            self.TabIndex = 1
            #AddControls            
            if gDevelopment:
                print "Tab Index is set to ", self.TabIndex
                print "Shake Camera Library Visible"
            self.__AddBrowserControls__()

        else:
            #self.refreshCamControlsView() 
            if gDevelopment:
                print "Shake Camera Library Hidden"
            self.TabIndex = pControl.ItemIndex
            if gDevelopment:
                print "Tab Index is set to ", self.TabIndex
            self.__removeBrowserControls__()
            #self.ClearControl("camlibRegion")
            self.__AddControls__()

        return

    """
    Spreadshet controls
    """

    def rs_FilterCameas(self, pControl, pEvent):
        # This will onyl work for 2 items in the drop downlist, any more and we need to do a case
        self.bShowAllCameras = bool(pControl.ItemIndex)
        self.refreshCamerasView(self.pListView)


    """
    Base Camera Controls Callbacks
    ---------------------------------------------------------------------------
    """
    def _event_CamButtonControl(self, pControl, pEvent):
        getattr(self, "_on_{}".format(pControl.Name))()

    def _on_lCreateNewButton(self):
        # newCamera = core.camUpdate.CreateCustomCamera()
        newCamera = CamLib.MobuCamera.CreateRsCamera().camera
        
        # new lens default setting section url:bugstar:1510245
        lenProperty = newCamera.PropertyList.Find( 'Lens' )
        for i in self.lenButtonList:
            if i.State == 1:
                if lenProperty:
                    lenProperty.Data = int ( i.Name )


        self.refreshCamerasView( self.pListView )        

    def _on_lCreateManyButton(self):

        createCancel, numberOfCams = FBMessageBoxGetUserValue( "Number of Cameras", "How many cameras do you want to create ?",
                                            getattr(self, "_numberOfCameras", 1), FBPopupInputType.kFBPopupInt,
                                            "Create", "Cancel")

        self._numberOfCameras = numberOfCams

        #createCancel is 1 when ok is selected, 2 when cancel is selected
        #Subtracting by 2  will normalize bring the value to 0 or 1
        #xrange does nothing when 0 is passed and we avoid using an if statement

        for each in xrange(numberOfCams * (2-createCancel) * int(numberOfCams > -1)):
            self._on_lCreateNewButton()
            FBSystem().Scene.Evaluate()

    def _on_lCreateNewExportButton(self):
        # core.camUpdate.CreateExportCamera()
        CamLib.MobuCamera.CreateExportCamera()
        self.refreshCamerasView(self.pListView)        


    def _on_lUpdateCamerasButton(self):
        # core.camUpdate.rs_updateCameras()
        CamLib.Manager.UpdateCameras()
        self.RefreshUI( None, None )
        self.refreshCamerasView(self.pListView)        


    def _on_lDelCameraButton(self):
        # iCameraNode = core.camUpdate.rs_SelectCameraByName(self.SelectedCam, switchView=self.switchCamera )
        # core.camUpdate.DeleteCamera(iCameraNode, RemoveCam=True)
        cam = CamLib.MobuCamera(self.SelectedCam)
        cam.DeleteCamera()
        self.refreshCamerasView(self.pListView)  

    def _on_lPlotButton(self):
        # RS.Core.Camera.Plot.PlotSwitcherToExportCamera( RS.Globals.System.CurrentTake )
        CamUtils.PlotSwitcherToExportCamera()

    def _on_lPlotTakesButton(self):
        lResult = FBMessageBox("Warning" , "This will plot the camera switcher to all {0} takes!".format( len(RS.Globals.Scene.Takes) ), "Cancel", "Continue")
        if lResult == 2:
            # RS.Core.Camera.Plot.PlotSwitcherToExportCamera( RS.Globals.Scene.Takes )
            CamUtils.PlotSwitcherToExportCamera(RS.Globals.Scene.Takes)

    def _on_lRemoveNonSwitcherCameraButton(self):
        # core.camUpdate.rs_RemoveNonSwitcherCameras()
        CamLib.Manager.DeleteNonSwitcherCams()
        self.refreshCamerasView(self.pListView)         

    def _on_lHideSelectPlanesButton(self):
        # core.camUpdate.rs_ShowHideCameraPlanes(False, True, False)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, True, False)

    def _on_lShowSelectPlanesButton(self):
        # core.camUpdate.rs_ShowHideCameraPlanes(True, True, False)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(True, True, False)

    def _on_lHideAllPlanesButton(self):
        # core.camUpdate.rs_ShowHideCameraPlanes(False, False, False)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

    def _on_lShowAllPlanesButton(self):
        # core.camUpdate.rs_ShowHideCameraPlanes(True, False, False)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(True, False, False)

    def _on_lShowCamerasButton(self):
        # core.camUpdate.showOnlyCameras()
        LibGTA5.camUpdate.showOnlyCameras()

    def _on_lSelectSwitcherButton(self): 
        # core.camUpdate.toggletSwitcherCamera(True)
        LibGTA5.camUpdate.toggletSwitcherCamera(True)
        self.refreshCamControlsView()  

    def _on_lForceUpdateCamerasButton(self):
        # core.camUpdate.rs_updateCameras( force=True )
        self.RefreshUI( None, None )
        self.refreshCamerasView(self.pListView) 

    """
        LIST VIEW CONTROLS
        """

    def _on_SelectCamera(self, pControl, pEvent):  
                
        # delselect any currently selected cell when callback is active url:bugstar:1815189
        if self.CameraCallbackEvent:
            for key, value in self.SpreadCameraDict.iteritems():
                cell = self.CameraListSpreadSheet.GetRow( value - 1 ) 
                if cell.RowSelected == True:
                    cell.RowSelected = False
                    break        
        
        if gDevelopment:
            print "_on_SelectCamera" 
            
        self.SelectedCam = pControl.GetCellValue(pEvent.Row,3)
        # iCameraNode = core.camUpdate.rs_SelectCameraByName(self.SelectedCam, switchView=self.switchCamera, showPlanes=self.AutoViewPlanes  )
        iCameraNode = LibGTA5.camUpdate.rs_SelectCameraByName(self.SelectedCam, switchView=self.switchCamera, showPlanes=self.AutoViewPlanes)
        if iCameraNode != None:
            #Add check to enable constraint for camera planes in the scene - 1331929
            for i in range(iCameraNode.GetSrcCount()):
                if iCameraNode.GetSrc(i).ClassName() == "FBConstraintRelation":
                    if iCameraNode.GetSrc(i).Active == False:
                        iCameraNode.GetSrc(i).Active = True    
            self.ActiveCamera = iCameraNode
            if gDevelopment:
                print self.ActiveCamera.Name
        self.refreshCamControlsView()   

    def _on_CellSelected(self, pControl, pEvent):
        return

    def _on_SelectCSF(self, pControl, pEvent):
        if gDevelopment:
            print self.CSFpath
        selectedShakeFile = self.shakeRelativePath + '/' + pControl.GetCellValue(pEvent.Row,2)

        if gDevelopment:
            print "_on_SelectCSF", selectedShakeFile

        shake.HandShake.LoadShakeFile(selectedShakeFile)
        return  

    """
    Utils Camera COntrols Callbacks
    ---------------------------------------------------------------------------
    """  
    def _on_lCopySwitchertoTakeButton(self):
        lTakeName = None

        for i in range(len(self.on_takeList_selected.Items)):
            if self.on_takeList_selected.IsSelected(i):
                lTakeName = self.on_takeList_selected.Items[i]
                break

        if lTakeName:
            lResult = FBMessageBox("Warning" , ("This will copy camera switcher data from " + RS.System.CurrentTake.Name + " to " + lTakeName), "Cancel", "Continue")
            if lResult == 2:
                lTargetTake = RS.Take.GetTakeByName( lTakeName )
                if lTargetTake:
                    RS.Model.CopyAnimationBetweenTakes( RS.Core.Camera.Switcher, RS.System.CurrentTake, [ lTargetTake ], [ RS.Property.SwitcherIndex ] )




    """
    Shake Camera COntrols Callbacks
    ---------------------------------------------------------------------------
    """



    def ShakeWeight(self, pControl, pEvent):
        self.ShakeLabelControl.Caption = "Shake Phase: %s" % int(pControl.Value) +"%"

        shake.HandShake.UpdateShakeWeight(pControl.Value)
        # DO some crazy SHit
        if self.SelectedCam  != None:
            if (self.liveupdate):
                shake.HandShake.AddShakeSelected(self.SelectedCam )


    def _on_lCreateShakeButton(self):
        shake.HandShake.AddShake()


    def _on_lRemoveShakeButton(self):
        shake.HandShake.DeleteShake()           


    def _on_lCreateShakeSelButton(self):        
        if self.SelectedCam  != None:
            shake.HandShake.AddShakeSelected(self.SelectedCam )

    def _on_lRemoveSelShakeButton(self):        
        if self.SelectedCam  != None:
            shake.HandShake.DeleteShakeSelected(self.SelectedCam )
        else:
            FBMessageBox("Warning", "No Camera Selected", "OK")  

    def _on_lExportCameraToShake(self):
        shake.HandShake.ExportCamShake(self.SelectedCam)
        #self.rs_ShakeRefresh(self.pShakeListControl) # Update the UI   


    def ShakeSelect(self, pControl, pEvent):
        shake.HandShake.LoadShakeFile(pControl.Items[pControl.ItemIndex])

    # BOOLS        
    def ShakeCheckLiveUpdate(self, pControl, pEvent):
        self.liveupdate = bool(pControl.State)

    def DisableExportLocalTransforms(self, pControl, pEvent):
        shake.HandShake.DisableLocalExportTransforms(bool(pControl.State))

#MBlanchette @Begin
    def ShakeLibraryRefresh(self, pControl, pEvent):
        self.__removeBrowserControls__()
        self.__AddBrowserControls__()
#MBlanchete @End




    """
    FADE CONTROLS
    ---------------------------------------------------------------------------
    """  

    def SetFadeInKey(self, pControl, pEvent):
        return

    def SetFadeOutKey(self, pControl, pEvent):
        return
    """
    SETTINGS
    ---------------------------------------------------------------------------
    """
    def ShakeCheckSwitchCamera(self, pControl, pEvent):
        self.switchCamera = bool(pControl.State)
        userPref.__Saveini__( "CameraToolbox", "SwitchCamera", self.switchCamera )    

    def ShakePlanesSwitchCamera(self, pControl, pEvent):
        self.AutoViewPlanes = bool(pControl.State)
        userPref.__Saveini__( "CameraToolbox", "ShowPlanes", self.AutoViewPlanes )

    # url:bugstar:1777879
    def CheckFocusMarkerScale( self, control, event ):
        self.FocusMarkerScale = control.Value
        userPref.__Saveini__( "CameraToolbox", "FocusMarkerScale", self.FocusMarkerScale ) 
    
    # url:bugstar:1510245
    def CheckLensSetting( self, control, event ):
        if control.State == True:
            self.LensSetting = control.Caption
            userPref.__Saveini__( "CameraToolbox", "LensSetting", self.LensSetting ) 
        else:
            self.LensSetting = None
            userPref.__Saveini__( "CameraToolbox", "LensSetting", self.LensSetting ) 

    """
    Dynamic Camera Controls Callbacks
    ---------------------------------------------------------------------------
    """

    def SelectCheck(self, control, event):

        if self.ActiveCamera != None:
            controlName = control.Name.split('Check')[1] 
            fProp = self.ActiveCamera.PropertyList.Find(controlName)
            if (fProp):
                fProp.Data = control.State

    # url:bugstar:1777879
    def FocusMarkerScaleChange( self, control, event ):
        # get all focus markers and scale them together
        for cam in RS.Globals.gCameras:
            if not cam.SystemCamera:
                if len( cam.Children ) > 0:
                    for child in cam.Children:
                        if 'focus_marker' in child.LongName.lower():
                            child.Size = control.Value
                            self.CheckFocusMarkerScale( control, event )

    # toolbox v2.0  updates url:bugstar:1733892 
    def EnumChange( self, control, event ):
        if self.ActiveCamera != None:
            controlName = control.Name.split('drop')[1] 
            fProp = self.ActiveCamera.PropertyList.Find( controlName )
            if ( fProp ):
                for i in range( len( control.Items ) ):
                    if control.IsSelected( i ):
                        selectedIndex = i
                        fProp.Data = selectedIndex

    def SelectDOFProperty(self, control, event):
        if self.ActiveCamera != None:             
            controlName = control.Name.split('label')[1]  

            # Kristine Added - Should we not deselecting all the other properties, if not remove three lines again.
            for propAll in self.ActiveCamera.PropertyList:
                if propAll.IsAnimatable ():                
                    propAll.SetFocus(False)    
            # End

            fProp = self.ActiveCamera.PropertyList.Find(controlName)
            if (fProp):                                
                fProp.SetFocus(True) # Need to use this method to actually select the property in MotionBuilder            
                self.SetAnimationCurve(fProp)            


    def ValueNumChanged(self, control, event): 
        if self.ActiveCamera != None:            
            controlName = control.Name.split('spinner')[1]            
            fProp = self.ActiveCamera.PropertyList.Find(controlName)
            if (fProp):
                if 'CoC' in control.Name:
                    fProp.Data = int( control.Value )
                    self.refreshSliderValues(control, controlName)                
                else:
                    fProp.Data = control.Value
                    self.refreshSliderValues(control, controlName)   


    # Will need to add a cehck for the DOF strength    
    def ValueChangeDOF(self, control, event):        
        if self.ActiveCamera != None:  
            fProp = self.ActiveCamera.PropertyList.Find(control.Name)            
            if (fProp):  
                if 'CoC' in control.Name:
                    fProp.Data = int( control.Value )
                    self.refreshSPinnerValues(control)  
                else:
                    fProp.Data = control.Value
                    self.refreshSPinnerValues(control)

                # CoC Values are driven by the CoC Override. We need to force a refresh of the Coc Values in the UI for display.
                if 'CoC' in control.Name:
                    if 'Override' in control.Name:
                        splitName = control.Name.split( ' Override' )
                        cocName = splitName[0]
                        self.refreshSliderValues( control, cocName )
                        cocControl = self.getControl( cocName )                    

                        if cocControl:
                            self.refreshSPinnerValues( cocControl )                     

                            cocProp = self.ActiveCamera.PropertyList.Find( cocName )
                            cocControl.Value = cocProp.Data


    def SetAnimationCurve(self, control):
        self.pFCurveView.Clear()   
        animnode = control.GetAnimationNode()

        newAnimationNode = FBAnimationNode("Test")
        newAnimationNode.FCurve = animnode.FCurve

        self.pFCurveView.AddProperty(control)

        self.pFCurveView.AddAnimationNode(newAnimationNode)

        return

    def OrganiseLateralCams(self):
        lateralFolder = None
        createFolder = True
        for folder in RS.Globals.Folders:
            if folder.Name == 'LateralCameras':
                lateralFolder = folder
                createFolder = False
        if createFolder:    
            placeholderCam = FBCamera('Remove_Me')
            lateralFolder = FBFolder("LateralCameras", placeholderCam)
            tag = lateralFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = "rs_Folders"
            placeholderCam.FBDelete()

        append = True
        for camera in RS.Globals.Cameras:
            if camera.Name in ['3LateralCACamera', '3LateralUICamera'] and lateralFolder:
                for i in range(camera.GetDstCount()):
                    if isinstance(camera.GetDst(i), FBFolder):
                        append = False
                if append:
                    lateralFolder.Items.append(camera)


def Run( show = True ):
    global tool
    tool = CameraToolbox()  

    if show:
        tool.Show()

    shake.HandShake.UI = tool

    return tool