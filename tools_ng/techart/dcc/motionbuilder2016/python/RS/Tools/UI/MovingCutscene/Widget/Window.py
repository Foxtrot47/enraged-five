"""
Main UI for the Moving Cutscene tool
"""
import functools

import pyfbsdk as mobu
from PySide import QtGui, QtCore

from RS import Globals
from RS.Utils import Scene, ContextManagers, Namespace
from RS.Tools import UI
from RS.Tools.UI import Application
from RS.Utils.Scene import Component
from RS.Core.Animation import Lib
from RS.Core.ReferenceSystem.Types import MocapComplex
from RS.Core.Scene.models import SceneMonitor
from RS.Core.Scene.widgets import ComponentView
from RS.Core.ReferenceSystem import Manager
from RS.Tools.UI import MovingCutscene
from RS.Tools.UI.MovingCutscene.Model import Components, SetupModel, SetupModelItem
from RS.Tools.Animation.Anim2Fbx.Widgets import CollapsableWidget
from RS.Tools.CameraToolBox.PyCoreQt.Delegates import spinboxDelegate
from RS.Tools.UI.MovingCutscene.Widget import ReferenceView
from RS.Tools.UI.MovingCutscene import const

REFERENCE_MANAGER = Manager.Manager()
SCENE_MONITOR = SceneMonitor.SceneMonitor()


def _Selection(func):
    """
    Decorator for the Window class.
    It passes the list of selected indexes to the decorated function.

    Args:
        func (function): function to decorate

    Return:
        function
    """

    def wrapper(instance, *args, **kwargs):
        indexes = instance.setupView.selectionModel().selection().indexes()
        return func(instance, indexes, *args, **kwargs)

    return wrapper


class Window(QtGui.QWidget):

    _keyboardShorcuts = {"Maya": "s"}
    # first value is the background color, the second value is the font color
    AnimationModeColors = ("#ffdc19", "black", "Animation")
    CameraModeColors = ("#004684", "white", "Camera")

    def __init__(self, parent=None):
        """
        constructor

        Args:
            parent (QtGui.QWidget): parent widget
        """
        super(Window, self).__init__(parent=parent)
        self._spinBoxDelegate = spinboxDelegate.SpinBoxDelegate()
        self._spinBoxDelegate.setMin(0)
        self._spinBoxDelegate.setMax(100)
        self._modeLabel = QtGui.QLabel("Animation Mode")
        self._modeLabel.setAlignment(QtCore.Qt.AlignCenter)

        self.setupView = QtGui.QTreeView()
        self.setupView.doubleClicked.connect(self.doubleClick)

        self.settings = QtCore.QSettings("RockstarSettings", self.windowTitle())

        self.cameraView = ComponentView.ComponentView(mobu.FBCamera)
        self.characterView = ComponentView.ComponentView(mobu.FBCharacter)
        self.propView = ComponentView.ComponentView(mobu.FBModel)
        self.mocapView = ReferenceView.ReferenceView(MocapComplex.MocapComplex)
        self.vehicleView = ComponentView.ComponentView(mobu.FBModel)

        # catch names to filter out of props
        self.propView.setHeader("Prop")
        self.propView.setFilter(const.Filters.PROP_VIEW)
        self.propView.update()
        self.propView.setDragEnabled(True)
        self.propView.model().sourceModel().setCopy(True)

        self.characterView.setDragEnabled(True)
        self.characterView.setFilter(const.Filters.CHARACTER_VIEW)
        self.characterView.model().sourceModel().setCopy(True)
        self.characterView.model().sourceModel().setShowLongName(True)

        self.cameraView.model().sourceModel().setCopy(True)
        self.cameraView.setDragEnabled(True)

        self.mocapView.setHeader("Mocap")
        self.mocapView.model().sourceModel().setCopy(True)
        self.mocapView.model().sourceModel().setShowLongName(True)
        self.mocapView.model().sourceModel().setCopy(True)
        self.mocapView.setDragEnabled(True)

        self.vehicleView.setHeader("Vehicle")
        self.vehicleView.setFilter(const.Filters.VEHICLE_VIEW)
        self.vehicleView.update()
        self.vehicleView.setDragEnabled(True)
        self.vehicleView.model().sourceModel().setCopy(True)

        self.setupView.setSelectionMode(self.setupView.ExtendedSelection)
        self.setupView.setAcceptDrops(True)
        self.setupView.setDragEnabled(True)
        self.setupView.setDragDropMode(QtGui.QTreeView.DragDrop)
        self.setupView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.setupView.customContextMenuRequested.connect(self.contextMenu)
        model = SetupModel.SetupModel()
        model.setCopy(False)
        self.model = model
        self.setupView.setModel(model)
        self.setupView.setItemDelegateForColumn(3, self._spinBoxDelegate)

        splitter = QtGui.QSplitter()
        collapsable = CollapsableWidget.CollapsableWidget(direction=CollapsableWidget.CollapsableWidget.Up)

        viewSplitter = QtGui.QSplitter()
        viewSplitter.addWidget(self.cameraView)
        viewSplitter.addWidget(self.characterView)
        viewSplitter.addWidget(self.propView)
        viewSplitter.addWidget(self.mocapView)
        viewSplitter.addWidget(self.vehicleView)
        collapsable.addWidget(viewSplitter)

        splitter.addWidget(self.setupView)
        splitter.addWidget(collapsable)
        splitter.setOrientation(QtCore.Qt.Vertical)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self._modeLabel)
        layout.addWidget(splitter)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        self.setLayout(layout)
        self.model.DragDropBeginSignal.connect(SCENE_MONITOR.disableCallback)
        self.model.DragDropBeginSignal.connect(SCENE_MONITOR.enableCallback)
        self.model.CopySignal.connect(self._itemCopied)
        Globals.Callbacks.OnFileNew.Add(self.reset)
        Globals.Callbacks.OnFileOpen.Add(self.reset)
        Globals.Callbacks.OnFileOpenCompleted.Add(self.load)
        self.previousPlayingState = False
        self._setColors()
        self._disableCameraCallback = False

    def _setColors(self, mode=AnimationModeColors):
        styleSheet = (
            "QTreeView{{"
            "border-style: outset;"
            "border-width: 2px;"
            "border-color: {color};}}"
            "QLabel{{"
            "background: {color};"
            "color: {textColor};"
            "font: bold;"
            "}}".format(color=mode[0], textColor=mode[1])
        )
        self._modeLabel.setText(mode[-1])
        self.setStyleSheet(styleSheet)

    def contextMenu(self, position):
        """
        Builds the context menu

        Args:
            position (QtCore.QPos): position of the cursor
        """
        menu = QtGui.QMenu(self)
        index = self.setupView.indexAt(position)
        data = index.data(SetupModelItem.SetupModelItem.DataRole)
        isMovingCutscene = isinstance(data, Components.MovingCutscene)

        menu.addAction("Add Moving Cutscene", self.addMovingCutscene)

        menu.addSeparator()
        marker = None

        if data is not None:
            marker = data.marker()

        if marker is not None and not Component.IsDestroyed(marker):

            selected = data.marker().Selected
            menu.addAction(
                ("Select Marker", "Deselect Marker")[selected], functools.partial(self._selectMarker, selected is False)
            )

            if not isMovingCutscene:
                component = data.component()
                constraint = data.constraint() or data.driverConstraint()
                if data.isCamera():
                    menu.addAction("Unparent Camera", self._deleteMarker)
                else:
                    menu.addAction("Delete Marker", self._deleteMarker)
                menu.addSeparator()

                if constraint and not Component.IsDestroyed(constraint):
                    weight = constraint.PropertyList.Find("Weight")
                    active = constraint.Active
                    weightSelected = constraint.Selected and weight.IsFocused()
                    selectTitle = ("Activate Constraint", "Disable Constraint")[constraint.Active]
                    weightTitle = ("Select Weight", "Deselect Weight")[weight.IsFocused()]
                    menu.addAction(selectTitle, functools.partial(self._toggleConstraintActive, active is False))
                    menu.addAction(weightTitle, functools.partial(self._toggleWeight, weightSelected is False))
                    menu.addAction("Key Constraint Weight", self._keyConstraint)
                    menu.addSeparator()

                if data.movingCutscene().active() is True:
                    menu.addAction("Activate Cutscene", functools.partial(data.movingCutscene().setActive, False))
                elif data.movingCutscene().active() is False:
                    menu.addAction("Disable Cutscene", functools.partial(data.movingCutscene().setActive, True))

                # add parent camera convert when marker is a camera
                if isinstance(marker, mobu.FBCamera):
                    menu.addSeparator()
                    menu.addAction(
                        "Convert Camera to Marker", lambda *args: data.convert(delete=False)
                    )
                if component:
                    if isinstance(component, mobu.FBCharacter):
                        menu.addAction("Plot To Skeleton", self._plot)
                        menu.addAction("Merge Animation to Base Animation Layer", self._plotCharacterEdits)
                        menu.addAction("Plot To Control Rig While In Motion", self._resetCharacterControlRigAnimation)
                    else:
                        menu.addAction("Plot {}".format(data.name()), self._plot)
                        menu.addAction("Reverse Plot to Null".format(data.name()), self._reversePlot)
                        if isinstance(marker, mobu.FBModelMarker) and isinstance(component, mobu.FBCamera):
                            menu.addSeparator()
                            menu.addAction("Convert Marker to Camera", lambda *args: data.convert())
                        if isinstance(component, mobu.FBCamera) and self._cameraMode.isChecked():
                            menu.addSeparator()
                            menu.addAction("Key Marker At Camera Position", data.key)
                            menu.addAction("Simple Plot", lambda *args: self._disableCallbacks(data.simplePlot))
                            menu.addAction(
                                "Simple Reverse Plot", lambda *args: self._disableCallbacks(data.simpleReversePlot)
                            )
                            menu.addSeparator()
                            menu.addAction("Clear Marker Keys", data.clearKeys)

                    menu.addSeparator()
                    menu.addAction("Update Reference", self._updateReference)

            elif isMovingCutscene:
                menu.addAction("Delete Moving Cutscene", self._deleteMarker)
                menu.addSeparator()
                menu.addAction("Activate Rig", functools.partial(data.setActive, False))
                menu.addAction("Deactivate Rig", functools.partial(data.setActive, True))
                menu.addSeparator()
                if data.active() is False:
                    menu.addAction("Freeze Rig", functools.partial(data.setFreezeActive, True))
                    menu.addAction("Source Motion", functools.partial(data.setZeroActive, True))
                elif isMovingCutscene:
                    menu.addAction("Build Moving Cutscene")

        menu.addSeparator()
        menu.addAction("Freeze All Rigs", functools.partial(self._freezeAllCutscenes, True))
        menu.addAction("Source Motion All", functools.partial(self._zeroAllCutscenes, True))
        menu.addAction("Activate All Rigs", functools.partial(self._zeroAllCutscenes, False))

        menu.addAction("Plot Selected", Components.MovingCutscene.plotSelected)
        menu.exec_(self.setupView.viewport().mapToGlobal(position))

    def createMenu(self):
        """
        Creates the menu for the UI.
        This method is called by the UI.Run decorator.

        Return:
            QtGui.QMenuBar
        """
        self.menuBar = QtGui.QMenuBar(self)

        self.modesMenu = QtGui.QMenu("Modes")
        self.mode = QtGui.QActionGroup(self)
        self._cameraMode = QtGui.QAction("Camera Mode", self)
        self._animationMode = QtGui.QAction("Animation Mode", self)
        self._cameraMode.setCheckable(True)
        self._animationMode.setCheckable(True)
        self._animationMode.setChecked(True)

        self.mode.addAction(self._cameraMode)
        self.mode.addAction(self._animationMode)

        self.settingsMenu = QtGui.QMenu("Settings")

        self.menuBar.addMenu(self.settingsMenu)
        self.menuBar.addMenu(self.modesMenu)

        self._autoReversePlot = QtGui.QAction("Automatically Reverse Plot", self)
        self._autoReversePlot.setCheckable(True)
        self._autoReversePlot.setChecked(self.settings.value("autoReversePlot", "true") == "true")

        self._autoConstraintMasterMover = QtGui.QAction("Automatically Constraint Master Mover", self)
        self._autoConstraintMasterMover.setCheckable(True)
        self._autoConstraintMasterMover.setChecked(self.settings.value("autoConstraintMasterMover", "true") == "true")

        self.settingsMenu.addAction(self._autoReversePlot)
        self.settingsMenu.addAction(self._autoConstraintMasterMover)
        self.settingsMenu.addSeparator()
        self.modesMenu.addAction(self._animationMode)
        self.modesMenu.addAction(self._cameraMode)

        self._cameraMode.toggled.connect(self._cameraCallback)
        return self.menuBar

    def addMovingCutscene(self):
        """ Creates a moving cutscene for every fbmodel and fbcharacter selected  """
        SCENE_MONITOR.disableCallback()
        selection = Scene.GetSelection((mobu.FBModel,))
        message = "Nothing Valid Selected\n"
        buttons = QtGui.QMessageBox.Ok
        if selection:
            message = (
                "Do you want to create a moving cutscene setup for each of the following components:\n\n"
                "{}".format("\n".join([component.LongName for component in selection]))
            )
            buttons = QtGui.QMessageBox.Yes|QtGui.QMessageBox.No

        if QtGui.QMessageBox.warning(self, "Create Moving Cutscene", message, buttons) != QtGui.QMessageBox.Yes:
            return

        autoConstraint = True
        if not self._autoConstraintMasterMover.isChecked():
            message = (
                "Do you want to constraint the following components objects to their respective Master Mover:\n\n"
                "{}".format("\n".join([component.LongName for component in selection]))
            )
            buttons = QtGui.QMessageBox.Yes | QtGui.QMessageBox.No
            autoConstraint = (
                QtGui.QMessageBox.warning(self, "Create Moving Cutscene", message, buttons)
                == QtGui.QMessageBox.Yes
            )

        for driver in selection:
            movingCutscene = Components.MovingCutscene(driver=driver)
            movingCutscene.transferMotion()

            masternull = movingCutscene.masterNull()
            zeronull = movingCutscene.zeroNull()
            freezenull = movingCutscene.freezeNull()
            freezenull.match(driver, isNull=True, popup=False)

            # transfer motion onto zeronull
            Lib.TransferMotion(zeronull.marker(), masternull.marker())

            if not autoConstraint:
                masternull.constraint().FBDelete()
            Component.SetProperties(masternull.marker(), **{"Scaling (Lcl)": mobu.FBVector3d(1, 1, 1)})

            Lib.PlotOnFrame(driver, inverse=True)
            for name in const.Nulls.TOP_LEVEL_CHILDREN:
                component = Components.Marker(movingCutscene, name, masternull, match=True)
                marker = component.marker()
                Lib.PlotOnFrame(marker, frame=0)

            self.setupView.model().setupMovingCutsceneData(movingCutscene)

        SCENE_MONITOR.enableCallback()

    def load(self, control=None, event=None):
        """
        Loads up the view with the moving cutscenes in the scene.
        This method is meant to be attached to the Motion Builder OnFileOpenCompleted

        Args:
            control (mobu.FBObject): object that called the event
            event (mobu.FBEvent): event being called
        """
        self.setupView.model().refresh()

    def reset(self, control=None, event=None):
        """
        Clears the view of the model-
        This method is meant to be attached to the Motion Builder OnFileOpenCompleted

        Args:
            control (mobu.FBObject): object that called the event
            event (mobu.FBEvent): event being called
        """
        self.setupView.model()._reset()

    def closeEvent(self, event):
        """
        Removes motion builder callbacks when the tool is closed

        Args:
            event (QtGui.QEvent): event being called by Qt
        """
        self.settings.setValue("autoReversePlot", self._autoReversePlot.isChecked())

        self.cameraView.closeEvent(event)
        self.characterView.closeEvent(event)
        self.propView.closeEvent(event)
        self.mocapView.closeEvent(event)
        if self._cameraMode.isChecked():
            Globals.Callbacks.OnSynchronizationEvent.Remove(self.cameraCallback)

        Globals.Callbacks.OnFileNew.Remove(self.reset)
        Globals.Callbacks.OnFileOpen.Remove(self.reset)
        Globals.Callbacks.OnFileOpenCompleted.Remove(self.load)

    def doubleClick(self, index):
        """
        Selects the marker stored by the given index

        Args:
            index (QtCore.QModelIndex): modelIndex from the view
        """
        column = index.column()
        data = index.data(SetupModelItem.SetupModelItem.DataRole)
        if column <= 1:
            marker = data.marker()
            if marker and not Component.IsDestroyed(marker):
                marker.Selected = marker.Selected is False

        elif column == 2 and isinstance(data, Components.Marker):
            constraint = data.constraint() or data.driverConstraint()

            if constraint and not Component.IsDestroyed(constraint):
                component = data.component()
                if isinstance(component, mobu.FBCharacter) and component.ActiveInput is False:
                    Lib.ActivateControlRig(component, True)
                active = constraint.Active is False
                constraint.Active = active
                if isinstance(data.component(), mobu.FBCharacter) and active:
                    constraint.Snap()
                constraint.Lock = active

        elif column == 2:
            constraintData = data._lastActive

            constraint = constraintData.constraint() or constraintData.driverConstraint()
            if constraint and not Component.IsDestroyed(constraint):
                value = constraint.Active is False
                constraint.Active = value
                self.model.layoutChanged.emit()
                return

        elif column == 4 and isinstance(data.component(), mobu.FBCharacter):
            data.plot()

        self.model.updateIndex(index)

    def _itemCopied(self, component):
        """
        Reverse plots the marker of the item that is being copied if the auto reverse plot option is on and if
        the component associated with the marker isn't a character.

        Args:
            component (RS.Tool.UI.MovingCutscene.Models.Components.Marker): marker for the item being copied
        """
        if self._autoReversePlot.isChecked() and not isinstance(component.component(), mobu.FBCharacter):
            component.reversePlot()

        constraint = component.constraint()
        if constraint is not None:
            constraint.Lock = True

    def _zeroAllCutscenes(self, value):
        """
        Sets the value of the zero constraint for all the moving cutscenes in the scene

        Args:
            value (bool): value to set the zero constraint to
        """
        model = self.setupView.model()
        for row in xrange(model.rowCount()):
            index = model.index(row, 0)
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            if isinstance(data, Components.MovingCutscene):
                data.setZeroActive(value)

    def _freezeAllCutscenes(self, value):
        """
        Sets the value of the freeze constraint for all the moving cutscenes in the scene

        Args:
            value (bool): value to set the freeze constraint to
        """
        model = self.setupView.model()
        for row in xrange(model.rowCount()):
            index = model.index(row, 0)
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            if isinstance(data, Components.MovingCutscene):
                data.setFreezeActive(value)

    @_Selection
    def _selectConstraint(self, indexes, value):
        """
        Selects the marker stored by the given index

        Args:
            index (QtCore.QModelIndex): modelIndex from the view
        """
        for index in indexes:
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            constraint = data.constraint() or data.driverConstraint()
            if constraint and not Component.IsDestroyed(constraint):
                constraint.Selected = value

    @_Selection
    def _toggleWeight(self, indexes, value):
        """
        Selects the constraint and the weight property of the constraint

        Args:
            constraint (pyfbsdk.FBConstraint): The constraint whose weight property should be selected
        """
        for index in indexes:
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            constraint = data.constraint() or data.driverConstraint()
            weight = constraint.PropertyList.Find("Weight")
            weight.GetOwner().Selected = value
            weight.SetFocus(value)

    @_Selection
    def _toggleConstraintActive(self, indexes, value):
        """
        Toggles the active state of the constraint

        Args:
            constraint (mobu.FBConstraint): constraint

        """
        for index in indexes:
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            if isinstance(data, Components.MovingCutscene):
                data = data.zeroNull()
                value = value is False
            isCharacter = isinstance(data.component(), mobu.FBCharacter)
            constraint = data.constraint() or data.driverConstraint()
            if constraint is not None:
                constraint.Active = value
                if isCharacter and value:
                    constraint.Snap()
                constraint.Lock = value

    def _addMarker(self, index):
        """
        Adds a marker parented under the given index

        Args:
            index (QtCore.QModelIndex): modelIndex from the view
        """
        QtGui.QMessageBox.warning(self, "Warning", "Feature Not Supported Yet")

    @_Selection
    def _selectMarker(self, indexes, value):
        for index in indexes:
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            marker = data.marker()
            if marker and not Component.IsDestroyed(marker):
                marker.Selected = value

    @_Selection
    def _deleteMarker(self, indexes):
        """
        Deletes the marker from the given index

        Args:
            index (QtCore.QModelIndex): modelIndex from the view
        """
        parentIndexes = {}
        for index in indexes:
            parentIndexes.setdefault(index.parent(), []).append(index)

        self.setupView.model().removeIndexes(parentIndexes)

    @_Selection
    def _keyConstraint(self, indexes):
        """
        Keys the Weight property of the constraint

        Args:
            constraint (pyfbsdk.FBConstraint): constraint who's weight property should be keyed
        """

        currentFrame = Globals.System.LocalTime.GetFrame()
        for index in indexes:
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            constraint = data.constraint() or data.driverConstraint()
            if constraint:
                Lib.PlotOnFrame(constraint, ("Weight",), currentFrame, children=False)

    @_Selection
    def _resetCharacterControlRigAnimation(self, indexes):
        """
        Plots the character's control rig while the moving cutscene is in motion to fix any double transforms that
        may be happening

        Args:
            index (QtCore.QModelIndex): index from the view
        """
        result = QtGui.QMessageBox.warning(
            self,
            "Warning",
            "This will plot the skeleton animation back to the control rig while the moving cutscene is in motion.\n"
            "This is to ensure it aligns with the stage when it is double transforming.\n"
            "Do you wish to continue?",
            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)

        if result != QtGui.QMessageBox.Ok:
            return
        for index in indexes:
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            if not isinstance(data.component(), mobu.FBCharacter):
                continue
            if not data.plotCharacterToStage():
                QtGui.QMessageBox.warning(
                    self, "Plotting Error", "Mover for the character was not found.\n Terminating Plot"
                )

    @_Selection
    def _plot(self, indexes):
        """
        Plots animation from the control rig to the skeleton on the selected characters and for non-characterized components
        it just plots them directly.

        Args:
            indexes (list): list of qmodelindexes
        """
        for index in indexes:
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            data.plot()

    @_Selection
    def _reversePlot(self, indexes, active=False):
        """
        Resets the selection before plotting so that animation is only plotted to the item selected in the UI
        Args:
            indexes (list): list of qmodelindexes
            active (bool): should the moving cutscene be active when plotting
        """
        with ContextManagers.ClearModelSelection():
            for index in indexes:
                component = index.data(SetupModelItem.SetupModelItem.DataRole)
                if isinstance(component, Components.MovingCutscene):
                    component = component.component()
                if not isinstance(component.component(), mobu.FBCamera):
                    active = False
                component.reversePlot(active=active)

    @_Selection
    def _plotCharacterEdits(self, indexes):
        """
        Plots edits from the available animation layers to the base animation layer

        Args:
            indexes (list): list of qmodelindexes
        """
        for index in indexes:
            data = index.data(SetupModelItem.SetupModelItem.DataRole)
            if not isinstance(data.component(), mobu.FBCharacter):
                continue
            data.plotCharacterEdits()

    @_Selection
    @SceneMonitor.toggle
    def _updateReference(self, indexes):
        """
        Update the selected references through the Moving Cutscene UI so their connections to their moving cutscene
        isn't lost

        Args:
            indexes (list): list of qmodelindexes
        """
        states = {callback: callback.enabled for callback in
                  (Globals.Callbacks.OnFileNew, Globals.Callbacks.OnFileOpen,
                   Globals.Callbacks.OnFileOpenCompleted)}
        try:
            for callback in states:
                callback.enabled = False

            for index in indexes:
                data = index.data(SetupModelItem.SetupModelItem.DataRole)
                component = data.component()
                name = component.LongName
                constraint = data.constraint()

                namespace = Namespace.GetNamespace(component)
                reference = REFERENCE_MANAGER.GetReferenceByNamespace(namespace)
                if not reference:
                    return QtGui.QMessageBox.warning(
                        self, "{} Warning".format(self.windowTitle()), "This object is not referenced"
                    )
                REFERENCE_MANAGER.UpdateReferences([reference], True)
                component = mobu.FBFindModelByLabelName(name)
                constraint.ReferenceAdd(0, component)
        except:
            raise

        finally:
            for callback, state in states.iteritems():
                callback.enabled = state

    def _keySwitcherCamera(self):
        """ Set a key on the camera currently active on the switcher  """
        # Get Current Camera from switcher
        camera = Globals.CameraSwitcher.CurrentCamera
        # Get marker
        marker = Components.MovingCutscene.getMarkerByComponent(camera)
        if marker:
            marker.key()

    def _enableCamera(self, play=True, setState=True):
        """
        Attach camera when playing the scene

        Args:
            play (bool): Set the scene to play
            setState (bool): Set the camera constraint to active
        """
        # Get Current Camera from switcher
        camera = Globals.CameraSwitcher.CurrentCamera
        # Get marker
        marker = Components.MovingCutscene.getMarkerByComponent(camera)
        currentState = self.previousPlayingState
        self.previousConstraintState = True
        self.previousPlayingState = currentState
        self.previousCamera = Globals.CameraSwitcher.CurrentCamera

        if setState and marker:
            marker.setActive(True)
        if play:
            Globals.Player.Play()

    def _disableCamera(self):
        """ Unattach camera when not playing the scene """
        # Get Current Camera from switcher
        camera = Globals.CameraSwitcher.CurrentCamera
        # Get marker
        marker = Components.MovingCutscene.getMarkerByComponent(camera)
        if marker:
            marker.setActive(False)
        self.previousConstraintState = False
        self.previousCamera = Globals.CameraSwitcher.CurrentCamera

    def _cameraCallback(self):
        """ Enables and disables the camera callback """
        if self._cameraMode.isChecked():
            config = mobu.FBConfigFile("@Application.txt")
            keyboardConfig = config.Get("Keyboard", "Default")

            self.previousFrame = Globals.System.LocalTime.GetFrame()
            self._enableCamera(play=False, setState=False)
            self._setColors(self.AnimationModeColors)
            self._currentHotkey = self._keyboardShorcuts.get(keyboardConfig, "k")
            Application.AddShortCut(self._currentHotkey, self._keySwitcherCamera)
            self._setColors(self.CameraModeColors)
            Globals.Callbacks.OnSynchronizationEvent.Add(self.cameraCallback)
        else:
            self._setColors(self.AnimationModeColors)
            Application.RemoveShortCut(self._currentHotkey)
            Globals.Callbacks.OnSynchronizationEvent.Remove(self.cameraCallback)

    def _disableCallbacks(self, func, *args, **kwargs):
        state = self._disableCameraCallback
        self._disableCameraCallback = True
        try:
            func(*args, **kwargs)
        except:
            raise
        finally:
            self._disableCameraCallback = state

    def cameraCallback(self, control, event):
        """
        Callback for toggling moving cutscene camera constraints

        Args:
            control (pyfbsdk.FBControl):
            event ()
        """

        if not self._cameraMode.isChecked() or self._disableCameraCallback:
            return

        if self.previousPlayingState != Globals.Player.IsPlaying:
            self.previousPlayingState = Globals.Player.IsPlaying
            if Globals.Player.IsPlaying:
                self._enableCamera()
            else:
                self._disableCamera()

        # Triggers when scrubbing the timeline
        elif not self.previousPlayingState:
            mousePressed = QtCore.Qt.LeftButton == QtGui.QApplication.mouseButtons()
            currentFrame = Globals.System.LocalTime.GetFrame()

            # Enable constraint when scrubbing the timeline
            if mousePressed and currentFrame != self.previousFrame:
                self.previousFrame = currentFrame
                if self.previousCamera != Globals.CameraSwitcher.CurrentCamera or not self.previousConstraintState:
                    self._enableCamera(play=False)

            # Disable constraint when not scrubbing the timeline
            elif not mousePressed and self.previousConstraintState and currentFrame == self.previousFrame:
                self._disableCamera()


@UI.Run(
    "Moving Cutscene Manager v{}".format(MovingCutscene.__version__),
    url="https://hub.rockstargames.com/display/ANIM/Moving+Cutscene+Manager",
)
def Run(show=None):
    return Window()
