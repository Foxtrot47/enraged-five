import os
import pyfbsdk as mobu

from RS import Perforce as P4
# todo: CallbackEngine not on GTA5, replace with native commands?
from RS import CallbackEngine
from RS.Core.Camera import CamUtils
from RS.Core.VirtualProduction import tpvcLib
from RS.Tools.UI import Run

# Janky Pyside Import - allows same code on all projects
try:
    from PySide2 import QtCore, QtGui, QtWidgets
    qtProxyModel = QtCore.QSortFilterProxyModel
    qIcon = QtGui.QIcon
except ImportError:
    from PySide import QtCore, QtGui as QtWidgets
    qtProxyModel = QtWidgets.QSortFilterProxyModel
    qIcon = QtWidgets.QIcon

# Button StyleSheets
disabledStyle = "QPushButton:hover:enabled { color: white; } QPushButton:checked { border-width: 2px;   }" \
                "QPushButton { border-width: 2px;  background: qlineargradient(spread:pad, x1:0 y1:0, x2:0 y2:1, " \
                "stop:0 rgba(100, 100, 100), stop:1 rgba(50, 50, 50));}"
playbackStyle = "QPushButton:hover:enabled { color: white; } QPushButton:checked { border-width: 2px;   }" \
                "QPushButton { border-width: 2px;  background: qlineargradient(spread:pad, x1:0 y1:0, x2:0 y2:1, " \
                "stop:0 rgba(0, 200, 0), stop:1 rgba(0, 50, 0));}"
recordingStyle = "QPushButton:hover:enabled { color: white; } QPushButton:checked { border-width: 2px;   }" \
                 "QPushButton { border-width: 2px;  background: qlineargradient(spread:pad, x1:0 y1:0, x2:0 y2:1, " \
                 "stop:0 rgba(200, 0, 0), stop:1 rgba(50, 0, 0));}"


class VcamCmds(object):
    Record = "0"
    Stop = "1"
    Bake = "2"
    SkipBack = "3"
    SkipForward = "4"


class ModeStates(object):
    NoVcams = 0
    Playback = 1
    Recording = 2


class VcamBoxWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(VcamBoxWidget, self).__init__(parent=parent)
        self.state = ModeStates.NoVcams
        self.modeButton = QtWidgets.QPushButton("No Vcams")
        self.callbackCheckbox = QtWidgets.QCheckBox("Callbacks Enabled")
        self.SetupUI()

        # Sync Controller Bindings
        P4.Sync("//virtualproduction/wvcam/python/vcontrols/bindings/vcam_mobu.py")  # xbox_default.py too?

    def SetupUI(self):
        mainLayout = QtWidgets.QVBoxLayout()
        self.setLayout(mainLayout)

        # Mode Button
        mainLayout.addWidget(self.modeButton)
        self.modeButton.setIconSize(QtCore.QSize(200, 100))
        self.modeButton.setStyleSheet(disabledStyle)
        vcamDevice = self.GetVcamDevice()
        if vcamDevice:
            self.state = ModeStates.Playback
            if vcamDevice.device().Live:
                self.state = ModeStates.Recording
        self.RefreshScene()
        self.RefreshUI()
        self.modeButton.clicked.connect(self.ToggleRecording)

        # Create Vcam Button
        createVcamButton = QtWidgets.QPushButton("Create Vcam")
        mainLayout.addWidget(createVcamButton)
        createVcamButton.clicked.connect(self.CreateVcamPressed)

        # Bake Vcams Button
        bakeVcamsButton = QtWidgets.QPushButton("Bake Vcams")
        mainLayout.addWidget(bakeVcamsButton)
        bakeVcamsButton.clicked.connect(self.BakeVcamsPressed)

        # Delete Vcams Button
        deleteVcamsButton = QtWidgets.QPushButton("Delete Vcams")
        mainLayout.addWidget(deleteVcamsButton)
        deleteVcamsButton.clicked.connect(self.DeleteVcamsPressed)

        # Callbacks Button
        mainLayout.addWidget(self.callbackCheckbox)
        self.callbackCheckbox.stateChanged.connect(self.CallbacksToggled)
        self.callbackCheckbox.setCheckState(QtCore.Qt.CheckState.Checked)
        # self.callbackCheckbox.hide()  # todo: leaving checkbox on while testing, but shouldn't really need it?

    def RefreshScene(self):
        vcamDevice = self.GetVcamDevice()
        if not vcamDevice:
            self.state = ModeStates.NoVcams
        else:
            recording = True if self.state == ModeStates.Recording else False
            vcamDevice.device().Live = recording
            vcamDevice.device().Online = recording
            vcamDevice.device().RecordMode = recording
            if mobu.FBPlayerControl().IsRecording != recording:
                mobu.FBPlayerControl().Record(True)

            alpha = 1.0 if recording else 0.5
            tpvcLib.UpdateHudAlpha(vcamDevice, alpha)

    def RefreshUI(self):
        if self.state == ModeStates.NoVcams:
            self.modeButton.setText("No Vcams")
            self.modeButton.setStyleSheet(disabledStyle)
            self.modeButton.setEnabled(False)
        elif self.state == ModeStates.Playback:
            self.modeButton.setEnabled(True)
            self.modeButton.setText("Playback")
            self.modeButton.setStyleSheet(playbackStyle)
        elif self.state == ModeStates.Recording:
            self.modeButton.setEnabled(True)
            self.modeButton.setText("Recording")
            self.modeButton.setStyleSheet(recordingStyle)

    def GetVcamDevice(self):
        vcamDevices = tpvcLib.TpvcDevice.getAllTpvcDevices()
        if vcamDevices:
            return vcamDevices[-1]

    def ToggleRecording(self):
        self.state = ModeStates.Recording if self.state == ModeStates.Playback else ModeStates.Playback
        self.RefreshScene()
        self.RefreshUI()

    def CreateVcamPressed(self):
        newVcam = tpvcLib.TpvcDevice()
        tpvcLib.SetupTpvcDeviceCamera.setup(newVcam)
        boundCamera = newVcam.boundCamera()
        # boundCamera.ViewShowGrid = True  # todo: debug line for blank FBXs
        mobu.FBSystem().Scene.Renderer.SetCameraInPane(boundCamera, 0)
        self.state = ModeStates.Recording
        self.RefreshScene()
        self.RefreshUI()

    def BakeVcamsPressed(self):
        mobu.FBPlayerControl().Stop()
        mobu.FBSystem().Scene.Evaluate()
        CamUtils.DupeRtcsToRsCams()
        self.state = ModeStates.Playback
        self.RefreshScene()
        self.RefreshUI()

    def DeleteVcamsPressed(self):
        tpvcLib.DeleteVcamDevices()
        self.state = ModeStates.NoVcams
        self.RefreshScene()
        self.RefreshUI()

    def CallbacksToggled(self, stateInt):
        CallbackEngine.callbackEngine.removeCallbacks()
        if stateInt == QtCore.Qt.CheckState.Checked:
            CallbackEngine.callbackEngine.onUIIdle.connect(self.ReadVcamCmd)

    def closeEvent(self, event):
        CallbackEngine.callbackEngine.removeCallbacks()

    def ReadVcamCmd(self):
        cmdPath = "x:/vcamCmd.txt"
        if os.path.exists(cmdPath):
            cmdFile = open(cmdPath, "r")
            cmdTxt = cmdFile.read()
            cmdFile.close()
            try:
                os.unlink(cmdPath)
            except WindowsError:
                # print "delete blocked"
                return
            # print "cmdTxt:", cmdTxt
            if cmdTxt == VcamCmds.Record:
                recording = False if mobu.FBPlayerControl().IsRecording else True
                self.state = recording
                self.ToggleRecording()
            elif cmdTxt == VcamCmds.Stop:
                mobu.FBPlayerControl().Stop()
            elif cmdTxt == VcamCmds.Bake:
                self.BakeVcamsPressed()
            elif cmdTxt == VcamCmds.SkipBack:
                frameNumber = mobu.FBSystem().LocalTime.GetFrame()
                mobu.FBPlayerControl().Goto(mobu.FBTime(0, 0, 0, frameNumber - 500))
            elif cmdTxt == VcamCmds.SkipForward:
                frameNumber = mobu.FBSystem().LocalTime.GetFrame()
                mobu.FBPlayerControl().Goto(mobu.FBTime(0, 0, 0, frameNumber + 500))


@Run("VcamBox", url="https://hub.gametools.dev/display/RSGTECHART/VcamBox+Tool")
def Run():
    wid = VcamBoxWidget()
    wid.show()
    return wid