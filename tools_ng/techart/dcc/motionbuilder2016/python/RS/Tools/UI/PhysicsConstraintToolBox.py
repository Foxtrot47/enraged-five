'''

 Script Path: RS/Tools/UI/PhysicsConstraintToolBox.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 
 
 Description: Toolbox UI for user to merge in an fbx file ( specifically one created from physics ) 
              and auto-create parent/child constraints from it.

              Merged-in file should already have correct naming convention - although there are overrides in 
              place for the user to manually match up scene items for the parent/child constraints.


'''

from pyfbsdk import *
from pyfbsdk_additions import *

import os

import RS
import RS.Globals
import RS.Tools.UI
import RS.Utils.Scene
from RS.Utils.Scene import Constraint

global mergedComponentsList
global physicsNullList
global matchDict

mergedComponentsList = []
physicsNullList = []
matchDict = {}


###############################################################################################################
## Class: PhysicsConstraintToolBox
###############################################################################################################

class PhysicsConstraintToolBox( RS.Tools.UI.Base ):
    
    global mergedComponentsList
    global physicsNullList
    global matchDict
       
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Physics Constraint Setup", size = [ 440, 700 ], helpUrl = 'https://devstar.rockstargames.com/wiki/index.php/Auto-Constraint_Creator' )


        # saved presets dropdown - may not need

    ###########################################################################################################
    ## UI Defs
    ########################################################################################################### 

    ''' 
    dropdown create
    '''
    def dropDownCreate( self, main, top, left, length, height ):
                
        dropDown = FBList()
        dropDown.Style = FBListStyle.kFBDropDownList
        dropDown.Hint = ''
        dropDownX = FBAddRegionParam( left,FBAttachType.kFBAttachLeft, "" )
        dropDownY = FBAddRegionParam( top,FBAttachType.kFBAttachTop, "" )
        dropDownW = FBAddRegionParam( length,FBAttachType.kFBAttachNone,"" )
        dropDownH = FBAddRegionParam( height,FBAttachType.kFBAttachNone,"" )            
        main.AddRegion( str( top ), str( top ), dropDownX, dropDownY, dropDownW, dropDownH )
        main.SetControl( str( top ), dropDown )   
    
        return dropDown
        

    ''' 
    label create
    '''
    def labelCreate( self, main, nullName, top, left, length, height, labelHint = '' ):
                
        label = FBLabel()
        label.Caption = nullName
        label.Hint = labelHint
        labelX = FBAddRegionParam( left,FBAttachType.kFBAttachLeft, "" )
        labelY = FBAddRegionParam( top,FBAttachType.kFBAttachTop, "" )
        labelW = FBAddRegionParam( length,FBAttachType.kFBAttachNone,"" )
        labelH = FBAddRegionParam( height,FBAttachType.kFBAttachNone,"" )            
        main.AddRegion( labelHint, labelHint, labelX, labelY, labelW, labelH )
        main.SetControl( labelHint, label )

        return label


    ''' 
    button create
    '''
    def buttonCreate( self, main, buttonName, top, left, length, height, buttonHint = '' ):
                
        button = FBButton()
        button.Caption = buttonName
        button.Hint = buttonHint
        buttonX = FBAddRegionParam( left,FBAttachType.kFBAttachLeft, "" )
        buttonY = FBAddRegionParam( top,FBAttachType.kFBAttachTop, "" )
        buttonW = FBAddRegionParam( length,FBAttachType.kFBAttachNone,"" )
        buttonH = FBAddRegionParam( height,FBAttachType.kFBAttachNone,"" )            
        main.AddRegion( buttonHint, buttonHint, buttonX, buttonY, buttonW, buttonH )
        main.SetControl( buttonHint, button )

        return button
         
 
    ''' 
    Dictionary for matching null & skel 
    updates when a dropdown changes
    '''
    def OnDropDown_Changed( self, control, event ): 
        global matchDict
              
        if hasattr( control, 'nullItem' ):
            currentVal = None
            
            for i in range(len(control.Items)):
                if control.IsSelected(i):                    
                    currentVal = control.Items[i]
                            
            matchDict[ control.nullItem ] = currentVal        


    '''
    Create the parent Child constraints
    '''
    def CreatePCConstraints( self, control, event ): 
        
        currentCharacter = FBApplication().CurrentCharacter
        charName = currentCharacter.LongName.split( ":" )
        
        placeholder = FBConstraintRelation( 'placeholder' )
        conFolder = FBFolder( charName[0] + ":Physics_PCConstraints", placeholder )   
        placeholder.FBDelete()
        
        for null, skel in matchDict.iteritems():
            print null, skel
            
            sceneNull = RS.Utils.Scene.FindModelByName( null )
            sceneSkel = FBFindModelByLabelName( skel )
            
            if sceneNull:
                if sceneSkel:
                    pcConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
                    pcConstraint.Name = ( skel + "__" + null )
                    pcConstraint.ReferenceAdd ( 0,sceneSkel )
                    pcConstraint.ReferenceAdd ( 1,sceneNull )
                    
                    conFolder.ConnectSrc( pcConstraint )
                
        FBDestroyToolByName( "Physics Constraint Setup" ) 


    '''
    Reload UI
    '''
    def ReloadUI( self, control, event ): 
        
        FBDestroyToolByName( "Physics Constraint Setup" )
        Run()
    
    
    '''
    Create UI
    '''
    def Create( self, main ):  


        ''' 
        selects the current character to whatever user chooses.  
        This will also activate CharacterSkelList def 
        '''
        def SelectedCurrentCharacter( control, event ):
            
            RS.Utils.Scene.DeSelectAll()
            
            charExists = False
            
            for i in range(len(control.Items)):
                
                if control.IsSelected(i):
                    
                    for char in RS.Globals.gCharacters:
                        if char.LongName == control.Items[i]:
                            char.Selected = True
                            currentCharacter = char
                            charExists = True                          
            
            constraintButton.Enabled = False
            
            if charExists == True:
                NullSkelPopulate( currentCharacter )
                constraintButton.Enabled = True

            else:
                currentCharacter = None
                NullSkelPopulate( currentCharacter )
                constraintButton.Enabled = False
                            
            
        ''' 
        creates the null labels and skel drop downs.
        populates the SKEL drop down with the currently selected character. 
        it will match the skel name with the null name, if it exists, otherwise will default to index 0.
        '''
        def NullSkelPopulate( currentCharacter ):
            
            global physicsNullList
            global matchDict
              
            characterHierarchyList = []
            skelList = []
            
            if currentCharacter != None:
                            
                # find hips via character node and create list of SKEL bones for that char
                hipsSelectedChar = currentCharacter.GetModel( FBBodyNodeId.kFBHipsNodeId )
                parentSelectedChar = RS.Utils.Scene.GetParent( hipsSelectedChar )
                RS.Utils.Scene.GetChildren( parentSelectedChar, characterHierarchyList, "", True )
                
                for node in characterHierarchyList:
                    if 'SKEL_' in node.Name:
                        skelList.append( node )
                
                # create labels and dropdowns for selected char's info
                topLabel = 110
                topDropDown = 115

                for i in range( len( physicsNullList ) ):
                    
                    nullLabel = self.labelCreate( main, physicsNullList[i], topLabel, 15, 120, 25, physicsNullList[i] )
                    skelDropDown = self.dropDownCreate( main, topDropDown, 150, 200, 15 )
                    
                    # record when the user changes the dropdown select
                    skelDropDown.nullItem = physicsNullList[i]
                    skelDropDown.OnChange.Add( self.OnDropDown_Changed )
                    
                    skelDropDown.Items.append( 'no match' )
                    
                    for skel in skelList:
                        skelDropDown.Items.append( skel.LongName )

                    match = False

                    for index in range( len( skelDropDown.Items ) ):
                        nullSuffix = physicsNullList[i].split( 'NULL_' )
                        skelSuffix = skelDropDown.Items[index].split( 'SKEL_' )
                        
                        if nullSuffix[-1] == skelSuffix[-1]:
                            skelDropDown.Selected( index, False )
                            match = True
                            
                            matchDict[ physicsNullList[i] ] = skelDropDown.Items[index]
                            
                            # hacky way to cover existing UI layout - need a better solution, just want it to work for now
                            missingSkelLabel = self.labelCreate( main, '  ', topLabel, 360, 20, 25, physicsNullList[i] + ":" + skelDropDown.Items[index])
                    
                    if match == False:
                        matchDict[ physicsNullList[i] ] = skelDropDown.Items[0]
                        missingSkelLabel = self.labelCreate( main, '*', topLabel, 360, 20, 25, physicsNullList[i] + ':a skel match cannot be found.\nSelect a match from the dropdown.' )
                        
                    topDropDown += 25
                    topLabel += 25
                
            else:
                # hacky way to cover existing UI layout - need a better solution, just want it to work for now
                self.labelCreate( main, '', 110, 15, 400, 500, '.' )
  

        '''
        user selects an fbx merge file & UI is created
        '''
        
        # get ranges before merging
        startTime = FBPlayerControl().LoopStart
        stopTime = FBPlayerControl().LoopStop
        
        # popup for user to select merged file       
        filePath = FBFilePopup()
        filePath.Caption = "Select an fbx file to merge in:"
        filePath.Style = FBFilePopupStyle.kFBFilePopupOpen
        
        filePath.Filter = "*"
        filePath.Path = "x:\\gta5\\art\\animation\\resources\\RnD_Samples\\Physics\\"
    
        filePath.Execute()
        
        mergePath = filePath.FullFilename 
        options = FBFbxOptions( True )
        
        # Disable merging of all import file takes 
        for index in range( 0, options.GetTakeCount() ):
            options.SetTakeSelect( index, False )
        
        # Enable merging of only the first import file take
        options.SetTakeSelect( 0, True )
        
        options.NamespaceList = "PlaceholderNamespace"
        
        if os.path.isfile( mergePath ):
            
            FBApplication().FileMerge( mergePath, False, options )
            
            # reset ranges to be as they were before the merge
            FBSystem().CurrentTake.LocalTimeSpan = FBTimeSpan( stopTime, startTime )
                        
            # find the merged in components, make a list and remove placeholder namespace                
            for comp in RS.Globals.gComponents:
                if comp.LongName.startswith( "PlaceholderNamespace" ):
                    comp.ProcessObjectNamespace( FBNamespaceAction.kFBRemoveAllNamespace, '', '' )
                    mergedComponentsList.append( comp )
        
            if len( mergedComponentsList ) > 0:
                for i in mergedComponentsList:
                    if 'NULL_' in i.Name:
                        physicsNullList.append( i.LongName )
            

            ''' 
            character dropdown for user to select which character to activate
            '''           
            characterDropDown = self.dropDownCreate( main, 60, 10, 200, 25 )


            '''
            initial populate of character drop down
            '''
            RS.Utils.Scene.DeSelectAll()
            
            if len( RS.Globals.gCharacters ) > 0:
                
                RS.Globals.gCharacters[0].Selected = True
                
                characterList = []
                for i in RS.Globals.gCharacters:
                    characterList.append( i )
                
                characterDropDown.Items.append( 'Select character:' )

                for i in characterList:
                    characterDropDown.Items.append( i.LongName )

            else:
                characterDropDown.Items.append( 'no characters' )  
          
          
            # button for constraint
            constraintButton = self.buttonCreate( main, 'Create P/C Constraints', 60, 240, 150, 25, 'creates parent child constraints for matches below.\nconstraints are turned off by default.' )
            constraintButton.Enabled = False
            
            # button for reload
            reloadButton = self.buttonCreate( main, 'Reload', 30, 0, 450, 15, 'Note: reloading will go back to asking\nyou to merge a file in' )
            # crashes atm
            reloadButton.Enabled = False
            
            characterDropDown.OnChange.Add( SelectedCurrentCharacter )
            constraintButton.OnClick.Add( self.CreatePCConstraints )
            reloadButton.OnClick.Add( self.ReloadUI )

                        
        else:
            FBMessageBox( 'R* Error', 'You did not select an .fbx file.\n\nNo file has been merged in.', 'Ok' ) 
            #FBDestroyToolByName( "Physics Constraint Setup" ) 
            

###############################################################################################################
## Run UI
###############################################################################################################  
      
def Run( show = True ):
    tool = PhysicsConstraintToolBox()  
    
    if show:
        tool.Show()
        
    return tool   
