'''
## Path: RS\Tools\UI\Animation\QT\FaceReview_Toolbox.py
## Description: UI Tool for Playblast/Render Script for 2nd Pass Face FBXs
## bugTracker : 2515761.
'''

import os
import posixpath
import tempfile

import pyfbsdk as mobu

from PySide import QtGui
from PySide import QtCore
from RS.Tools import UI

import RS.Config
import RS.Utils.UserPreferences
from RS.Tools.UI import Application

from RS.Utils import Playblast
from RS.Tools import PlayblastFaceAnimation as reviewCore
from RS import Perforce

__author__ = 'cedric.bazillou@rockstarnorth.com'
__version__ = '0.1.1'

#Compile .ui file to populate the main window
moduleFile = 'FaceReview_Toolbox'
uiRessourcePath = 'RS/Tools/UI/Animation/QT'

uiFile = '{0}/{1}/{2}.ui'.format(RS.Config.Script.Path.Root, 
                                 uiRessourcePath,
                                 moduleFile)
                                 
compiledTarget = '{0}/{1}/{2}_compiled.py'.format(RS.Config.Script.Path.Root, 
                                                  uiRessourcePath,
                                                  moduleFile)

RS.Tools.UI.CompileUi(uiFile, compiledTarget)

import RS.Tools.UI.Animation.QT.FaceReview_Toolbox_compiled as faceReviewCompiled

if os.path.isfile(compiledTarget) :
    os.remove(compiledTarget)
    
reviewTitle = 'Face Animation Review Tool'

class mainWidget( UI.QtMainWindowBase, faceReviewCompiled.Ui_MainWindow ):
    """
    main Tool UI
    """
    def __init__(self ):
        """
        Constructor
        """
        # Main window settings
        RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = reviewTitle)

        # Class variables
        self._renderPath = ''
        self.forcePalSize = True

        # run ui from designer file
        self.setupUi( self )

        # add banner
        banner = RS.Tools.UI.BannerWidget(name = 'FPS Toolbox', helpUrl ="")
        self.vboxlayout_banner.addWidget ( banner )
        
        #Fill Setting at opening time as well...
        self._refreshSettings()
            
        # callbacks
        self.pushButton_pathDirectory.pressed.connect(self._handlePathSaveButton)
        self.pushButton_refreshSetting.pressed.connect(self._refreshSettings)
        self.pushButton_createReviewCamera.pressed.connect(self._createReviewCamera)
        self.pushButton_render.pressed.connect(self._render)
        self.pushButton_fitToHead.pressed.connect(self._fitCamera)
        self.pushButton_openRender.pressed.connect(self._openPlayBlast)

        self.spinBox_imageHeight.valueChanged.connect(self._updateReviewCamera)
        self.spinBox_imageWidth.valueChanged.connect(self._updateReviewCamera)
        self.lineEdit_LeadName.textEdited.connect(self._updateReviewCamera)

        self.forcePalSize = False

    def _handlePathSaveButton(self):
        """
        Internal Method

        Handle the file save button press for render file
        """
        renderDirectory = os.path.dirname(self._renderPath)
        
        filePath, cancelState = QtGui.QFileDialog.getSaveFileName(self, 'Save face playblast', QtCore.QDir.currentPath(), "*.mov")
        if not cancelState:
            return

        renderDirectory = os.path.dirname(filePath)


        QtCore.QDir.setCurrent(renderDirectory)
        self.label_savepath.setText(filePath)
        self._renderPath = filePath
        self.label_savepath.setToolTip(filePath)

    def _refreshSettings(self):
        """
        Internal Method

        Fill :
            - UI render path
            - FrameRange
            - include audio        
        """
        #Extract render path from current scene
        currentScene = mobu.FBApplication().FBXFileName

        self.label_savepath.setToolTip('')
        if len(currentScene)>0:
            renderDirectory = os.path.dirname(currentScene)
            fileName =  os.path.splitext(os.path.basename(currentScene))[0]

            suffix = '_rvw'
            if fileName.endswith(suffix):
                suffix = ''

            currentScene = os.path.join( renderDirectory, fileName + suffix + '.mov')


        self.label_savepath.setText(currentScene)
        self.label_savepath.setToolTip(currentScene)
        
        self._renderPath = currentScene
        renderDirectory = os.path.dirname(currentScene)

        QtCore.QDir.setCurrent(renderDirectory)
        
        currentCamera = mobu.FBSystem().Scene.Renderer.CurrentCamera
        
        #Extract Frame range from current scene
        playerCtrl = mobu.FBPlayerControl()
        self.spinBox_frameStart.setValue(int(playerCtrl.LoopStart.GetFrame()))
        self.spinBox_frameEnd.setValue(int(playerCtrl.LoopStop.GetFrame()))  

        #Camera Sizes
        imageResolution = reviewCore.computeResolution(currentCamera)
        if self.forcePalSize == True:
            self.spinBox_imageWidth.setValue(720)
            self.spinBox_imageHeight.setValue(576)
        else:
            self.spinBox_imageWidth.setValue(imageResolution[0])
            self.spinBox_imageHeight.setValue(imageResolution[1])
        self.lineEdit_LeadName.setText('')
        LeadName = reviewCore.extractHudData(currentCamera)
        if LeadName != None:
            self.lineEdit_LeadName.setText(LeadName)
        
    def _createReviewCamera(self):
        """
        Internal Method

        Prepare a camera with Hud element showing the Lead name
        """
        LeadName = self.lineEdit_LeadName.text()
        
        reviewCore.buildFaceCamera(720,
                                   576,
                                   LeadName)
                                   
    def _updateReviewCamera(self):
        """
        Internal Method

        Update a camera with Hud element and frame size settings
        """
        currentCamera = mobu.FBSystem().Scene.Renderer.CurrentCamera
        imageResolution = reviewCore.computeResolution(currentCamera)

        if imageResolution[2] == True:
            reviewCore.updateFaceCamera(self.spinBox_imageWidth.value(),
                                        self.spinBox_imageHeight.value())

            currentCamera = mobu.FBSystem().Scene.Renderer.CurrentCamera
            LeadName = self.lineEdit_LeadName.text()

            reviewCore.updateReviewHud(currentCamera,LeadName)

    def _fitCamera(self,
                   headPrefix='head_',
                   planePrefix='*Plane_'):
        """
        Internal Method

        Center camera bound to a head mesh
        """
        headMeshList = mobu.FBComponentList()
        
        headRegexList = [headPrefix,planePrefix]
        for prefix in [headPrefix,planePrefix]:
            headRegexList.append(prefix.capitalize())
            headRegexList.append(prefix.upper())
            headRegexList.append(prefix.lower())
        
        for prefix in headRegexList:
            catchHeadList = mobu.FBComponentList()
            mobu.FBFindObjectsByName ('{0}*'.format(prefix), 
                                      catchHeadList, 
                                      False, 
                                      True) 
            for head in catchHeadList:
                headMeshList.append(head)

        if headMeshList.count() == 0:
            return 

        headMeshList = list(set(headMeshList))
        
        sceneContent = mobu.FBComponentList()
        sceneSelection = mobu.FBComponentList()

        mobu.FBSystem().Scene.NamespaceGetContentList(sceneContent, 
                                                      None, 
                                                      mobu.FBPlugModificationFlag.kFBPlugAllContent ,
                                                      True,
                                                      0, 
                                                      False)

        for sceneObject in sceneContent:
            if sceneObject.Selected == True:
                sceneObject.Selected = False
                sceneSelection.append(sceneObject)

        for headModel in headMeshList:
            headModel.Selected = True

        mobu.FBSystem().Scene.Renderer.FrameCurrentCameraWithModels(False) 

        for headModel in headMeshList:
            headModel.Selected = False
            
        for sceneObject in sceneSelection:
            sceneObject.Selected = True
    
    def _render(self):
        """
        Internal Method

        Produce the playblast video file
        """
        currentCamera = mobu.FBSystem().Scene.Renderer.CurrentCamera
        fps = str(self.fpsComboBox.currentText())

        if len(self._renderPath)==0:
            return
            
        renderDirectory = os.path.dirname(self._renderPath)

        if os.path.exists(renderDirectory) == False:
            #TODO assert file error
            return

        includeAudio = bool(self.checkBox_includeaudio.isChecked() ) 

        Playblast.PlaybastRender(self._renderPath,
                                 self.spinBox_frameStart.value(),
                                 self.spinBox_frameEnd.value(),
                                 self.spinBox_imageWidth.value(),
                                 self.spinBox_imageHeight.value(),
                                 showtimeCode=True,
                                 camera=currentCamera,
                                 framerate=int(fps),
                                 includeAudio=includeAudio,
                                 codec = 'libx264',
                                 compression= ['-framerate',
                                 fps,
                                 '-vprofile',
                                 'high',
                                 '-level',
                                 '4.0',
                                 '-pix_fmt',
                                 'yuv420p' ])

        #Add video file to perforce with the correct file type modifier
        if os.path.exists(self._renderPath):
            Perforce.Add(self._renderPath, changelistNum=None, force=True, fileType='binary+S')
                                 
        self._openPlayBlast()

    def _openPlayBlast(self):

        """
        Internal Method

        open the playblast video file
        """
        if os.path.exists(self._renderPath):
            os.startfile(self._renderPath)

def Run(show=True):
    Application.CloseToolByTitle(reviewTitle)
    tool = mainWidget()

    if show: 
        tool.show()  
    return tool
