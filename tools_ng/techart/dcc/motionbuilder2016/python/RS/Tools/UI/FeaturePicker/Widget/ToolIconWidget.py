"""
Description:
    Button that represents and contains information on a tool. These buttons can be dragged into the R* Shelf
    (Shelftastic) and R* Hotkey Editor
"""
import os
import xml.etree.cElementTree as xml

from PySide import QtCore, QtGui

import RS.Config
from RS.Utils.Path import ResolvePath

DEFAULT_ICON_PATH = os.path.join(os.path.abspath(RS.Config.Script.Path.ToolImages), "Tools", "default.png")


class ToolIcon(QtGui.QPushButton):
    """ Custom Button that overlays text on top of an image """

    def __init__(self, parent=None, text="No Icon", imagePath=DEFAULT_ICON_PATH):
        """
        Constructor

        Arguments:
            parent (QWidget): parent widget
            imagePath (string): path to image to use

        """
        super(ToolIcon, self).__init__(parent=parent)
        self.title = ""
        self.module = ""
        self.command = "Run"
        self.description = ""
        self.changelist = ""
        self.changelistDescription = ""
        self.revision = "0"
        self.contributors = ()
        self.dependencies = ()
        self.pythonFilePath = ""

        self.setText(text)
        self.setToolTip(text)

        self.imagePath = imagePath
        self._image = QtGui.QImage(imagePath)
        self._clickedImage = QtGui.QImage(imagePath)
        self._clickedImage.convertToFormat(QtGui.QImage.Format_Indexed8)
        self._alignment = QtCore.Qt.AlignHCenter | QtCore.Qt.AlignBottom
        self._font = QtGui.QFont('Calibri', 8)
        self._font.setWeight(QtGui.QFont.Bold)
        self.size()

    @property
    def mimeTextData(self):
        """
        Mime datat that is dropped when an icon that is being dragged is let go

        Return:
            string

        """
        return ("<button>"
                "<name>{title}</name>"
                "<icon>{imagePath}</icon>"
                "<code>"
                "from {module} import {command}\n"
                "{command}()"
                "</code>"
                "<codeFile>{pythonFilePath}</codeFile>"
                "<runTypeIdx>0</runTypeIdx>"
                "</button>").format(title=self.title, module=self.module, command=self.command,
                                    imagePath=self.imagePath, pythonFilePath=self.pythonFilePath)

    def paintEvent(self, event):
        """
        Draws the widget; overloaded to support drawing text over images

        Arguments:
            event (QCore.QEvent): event called by the widget

        """
        painter = QtGui.QPainter()
        painter.begin(self)

        # Set Opacity
        if self.isDown():
            painter.setOpacity(.5)
        else:
            painter.setOpacity(1)

        # Draw Image
        rect = event.rect()

        # Draw Text

        if self.text():
            rect.setRect(0, -4, 50, 50)
            painter.drawImage(rect, self._image)

            painter.setPen(QtGui.QColor(255, 255, 255))
            painter.setFont(self._font)
            painter.drawText(event.rect(), self._alignment, self.text())

        else:
            rect.setRect(0, 0, 50, 50)
            painter.drawImage(rect, self._image)

        painter.end()

    def mouseMoveEvent(self, event):
        """
        overrides internal mouseMoveEvent method

        Adds support for dragging the buttons

        Arguments:
            event (QtCore.QEvent): event called when dragging is initiated

        """
        super(ToolIcon, self).mouseMoveEvent(event)
        if event.buttons() != QtCore.Qt.LeftButton:
            return

        mimeData = QtCore.QMimeData()
        mimeData.setText(self.mimeTextData)

        drag = QtGui.QDrag(self)
        drag.setMimeData(mimeData)
        drag.setHotSpot(event.pos() - self.rect().topLeft())

        drag.start(QtCore.Qt.MoveAction)

    def icon(self):
        """
        The path to the image used by the button

        Return:
            string
        """
        return self._image

    def setIcon(self, path):
        """
        sets the icon to be used by this button

        Arguments:
            path (string): path to the image to use
        """
        self._image.load(path)
        self._clickedImage.load(path)
        self._clickedImage.convertToFormat(QtGui.QImage.Format_Indexed8)
        # update is called to force the paint event
        self.update()

    def textAlignment(self):
        """ the text alignment """
        return self._alignment

    def setTextAlignment(self, alignment):
        """
        Sets the alignment for the text

        Arguments:
            alignment (QtCore.Qt.QAlign): alignment to set for the text

        """
        self._alignment = alignment

    def font(self):
        """ Font of the text """
        return self._font

    def setFont(self, font):
        """
        Sets the font for the text

        Arguments:
            font (string): name of the font to set for the text

        """
        self._font = font

    def weight(self):
        """
        The weight (thickness) of the font

        Return:
            int
        """
        return self._font.weight()

    def setWeight(self, weight):
        """
        sets the weight of the font

        Arguments:
            weight (int): weight (thickness) to set for the font

        """
        self._font.setWeight(weight)

    def storeMenuInformation(self, element):
        """
        Reads the content of the xml element from the menu.config and stores the information within this button

        Arguments:
            element (xml.etree.elementTree.Element): xml element with information about the command this button is
                                                    suppose to represent. The element should come from the menu.config
                                                    xml file.

        """
        self.title = element.attrib.get('displayName', "")
        self.module = element.attrib.get('modulePath', "RS.Tools.UI.Commands")
        self.command = element.attrib.get('command', "Run")
        self.pythonFilePath = os.path.join(RS.Config.Script.Path.RockstarRoot, *self.module.split(".")[1:])

        cachedDocPath = os.path.join(RS.Config.Script.Path.RockstarRoot, "Docs", "_cache", *self.module.split(".")[1:])
        if os.path.isdir(cachedDocPath):
            cachedDocPath = os.path.join(cachedDocPath, "__init__.xml")
        else:
            cachedDocPath = "{}.xml".format(cachedDocPath)

        if os.path.exists(cachedDocPath):
            tree = xml.parse(cachedDocPath)
            root = tree.getroot()
            self.revision = root.attrib.get("revision", "0")
            self.date = root.attrib.get("date", "0/0/0 00:00:00")
            self.changelist = root.attrib.get("changelist", "")
            self.changelistDescription = root.attrib.get("changelistDescription")
            self.description = root.find("description").text
            imagePath = ResolvePath(root.attrib.get("image", self.imagePath))

            if self.module == "RS.Tools.UI.Commands" and self.command != "Run":
                methodElement = root.find("methods/method[@name='{}']".format(self.command))
                imagePath = self.imagePath
                if methodElement is not None:
                    self.description = methodElement.text
                    imagePath = methodElement.attrib.get("image", self.imagePath)
                imagePath = ResolvePath(imagePath)

            if not self.description:
                self.description = "No Description Found"

            if imagePath != self.imagePath:
                self.imagePath = imagePath
                self.setIcon(self.imagePath)

                title = self.title.split(":")[-1]
                if len(title) > 10:
                    title = "{}..".format(title[:8])
                self.setText(title[:10])

            self.contributors = tuple("{} <{}>".format(contributor.attrib.get("name"), contributor.attrib.get("email"))
                                      for contributor in (root.find("authors") or []))

            self.dependencies = tuple(dependency.attrib.get("name") for dependency in (root.find("imports") or []))
            self.description = self.description.strip()
            self.setToolTip(self.title)