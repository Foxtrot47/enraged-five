"""
Tool for allowing user to clear character extension components of keys
"""
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS.Core.Scene.models import characterModelTypes, characterExtModelTypes
from RS.Tools import UI


class CleanExtensionsToolUI(UI.QtMainWindowBase):
    """
    GUI Tool for Cleaning Extensions
    """
    def __init__(self):
        """
        Constructor
        """
        self._toolName = "Clean Extensions Tool"
        super(CleanExtensionsToolUI, self).__init__(title=self._toolName, dockable=True)
        self.setupUi()

    def setupUi(self):
        """
        Set up the UI
        """
        # Create Models
        self._charsModel = characterModelTypes.CharacterModel()
        self._extModel = characterExtModelTypes.CharacterExtFromCharacterModel()
        self._proxyModel = QtGui.QSortFilterProxyModel(self)

        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()
        selectionLayout = QtGui.QHBoxLayout()
        clearTakesLayout = QtGui.QHBoxLayout()

        # Create Widgets
        mainWidget = QtGui.QWidget()
        banner = UI.BannerWidget(self._toolName)
        self._characterSelector = QtGui.QComboBox()
        self._charExtentsions = QtGui.QListView()
        clearKeysButton = QtGui.QPushButton("Clear Keys on Selected")
        clearAllTakesGroupBox = QtGui.QGroupBox("Clear All Takes?")
        selectAllButton = QtGui.QPushButton("Select All")
        selectNoneButton = QtGui.QPushButton("Select None")
        selectInvertButton = QtGui.QPushButton("Select Invert")
        self._clearYesRadio = QtGui.QRadioButton("Yes")
        self._clearNoRadio = QtGui.QRadioButton("No")

        # Configure Widgets
        self._characterSelector.setModel(self._charsModel)
        self._charExtentsions.setModel(self._proxyModel)
        self._charExtentsions.setAlternatingRowColors(True)
        banner.setMaximumHeight(30)
        self._clearYesRadio.setChecked(True)

        # Configure Models
        self._charsModel.setCheckable(False)
        self._extModel.setCheckable(True)
        self._proxyModel.setSourceModel(self._extModel)
        self._proxyModel.setDynamicSortFilter(True)
        self._proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self._proxyModel.sort(0, QtCore.Qt.AscendingOrder)

        # Assign Widgets to Layouts
        clearTakesLayout.addWidget(self._clearYesRadio)
        clearTakesLayout.addWidget(self._clearNoRadio)

        selectionLayout.addWidget(selectAllButton)
        selectionLayout.addWidget(selectNoneButton)
        selectionLayout.addWidget(selectInvertButton)

        mainLayout.addWidget(banner)
        mainLayout.addWidget(QtGui.QLabel("Character:"))
        mainLayout.addWidget(self._characterSelector)
        mainLayout.addWidget(self._charExtentsions)
        mainLayout.addLayout(selectionLayout)
        mainLayout.addWidget(clearAllTakesGroupBox)
        mainLayout.addWidget(clearKeysButton)

        # Assign Layouts to Widgets
        clearAllTakesGroupBox.setLayout(clearTakesLayout)
        mainWidget.setLayout(mainLayout)

        self.setCentralWidget(mainWidget)

        # Setup signals
        self._characterSelector.currentIndexChanged.connect(self._handleCharacterSelectIndexChange)
        selectAllButton.pressed.connect(self._handleSelectAll)
        selectNoneButton.pressed.connect(self._handleSelectNone)
        selectInvertButton.pressed.connect(self._handleSelectInvert)
        clearKeysButton.pressed.connect(self._handleClearKeys)

        self._handleCharacterSelectIndexChange(0)

    def getCurrentlySelectedCharacter(self):
        """
        Get the currently selected character in the drop down menu

        Returns:
            FBCharacter of the selected character
        """
        if self._characterSelector.count() > 0:
            return self._characterSelector.itemData(self._characterSelector.currentIndex(), QtCore.Qt.UserRole)
        return None

    def _handleCharacterSelectIndexChange(self, newIndex):
        """
        Internal Method

        Handle the character Selection change
        """
        self._proxyModel.sourceModel().setCharacter(self.getCurrentlySelectedCharacter())
        self._proxyModel.invalidate()

    def _handleSelectAll(self):
        """
        Internal Method

        select all the items in the model
        """
        self._proxyModel.sourceModel().checkAllItems()
        self._proxyModel.invalidate()

    def _handleSelectNone(self):
        """
        Internal Method

        deselect all the items in the model
        """
        self._proxyModel.sourceModel().uncheckAllItems()
        self._proxyModel.invalidate()

    def _handleSelectInvert(self):
        """
        Internal Method

        invert the selection of the items in the model
        """
        self._proxyModel.sourceModel().invertCheckedState()
        self._proxyModel.invalidate()

    def _handleClearKeys(self):
        """
        Internal Method

        Handle the clear keys buttonpring pressed
        """
        system = mobu.FBSystem()

        allCharExts = set()
        char = self.getCurrentlySelectedCharacter()
        if char is None:
            return

        # Get the char ext which are in control of the components to be cleared
        comps = [item.GetComponentObject() for item in self._extModel.getCheckedItems()]
        for charExt in char.CharacterExtensions:
            for comp in comps:
                if comp in charExt.Components:
                    allCharExts.add(charExt)

        if self._clearYesRadio.isChecked() is True:
            currentTake = system.CurrentTake
            for take in system.Scene.Takes:
                system.CurrentTake = take
                for ext in comps:
                    self._clearAnim(ext.AnimationNode)
            # restore take
            system.CurrentTake = currentTake

        else:
            for ext in comps:
                self._clearAnim(ext.AnimationNode)

        for charExt in allCharExts:
            charExt.GoToStancePose()

        # Need to move and reset the current frame to update the anim, evulate doesnt do it
        mobuPlayer = mobu.FBPlayerControl()
        mobuPlayer.Goto(mobu.FBSystem().LocalTime + 1)
        mobuPlayer.Goto(mobu.FBSystem().LocalTime - 1)

        QtGui.QMessageBox.information(self, "Clean Extensions Tool", "Extension keys cleared")

    def _clearAnim(self, node):
        """
        Internal Method

        Clear the animation on the given aniamtion node
        """
        # The FCurve property will not be null on a terminal node.
        # i.e. the 'Lcl Translation' node will not have any animation on it
        # directly... only the sub-nodes 'X', 'Y' or 'Z' may have animation.
        if node.FCurve:

            # Ah! there is a FCurve! Let's remove all the keys.
            node.FCurve.EditClear()
        else:
            # Then we are dealing with a parent node. Let's look at it
            # children nodes.
            for child in node.Nodes:
                # Recursively call ourselves to deal with sub-nodes.
                self._clearAnim(child)


def Run(show=True):
    cleanExtTool = CleanExtensionsToolUI()

    if show:
        cleanExtTool.show()

    return cleanExtTool
