"""
Removes empty folders from the scene

Image:
    $TechArt/Tools/CleanupEmptyFolders.png
"""
import pyfbsdk as mobu

from PySide import QtGui

from RS import Globals
from RS.Tools.UI import Run as Run_


class EmptyFolderCleanupTool(QtGui.QPushButton):
    def __init__(self):
        """ Constructor """
        super(EmptyFolderCleanupTool, self).__init__()
        
        self.setText("Delete Empty Folders")
        self.setToolTip("Delete Empty Folders")
        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.pressed.connect(self.OnCleanUp)  # Tells the button which function to call when clicked.

    def OnCleanUp(self):
        """ Removes characters from folders and deletes all empty folders from the scene """

        # Remove characters from folders
        [folder.Items.remove(item)
         for folder in Globals.gFolders for item in folder.Items if isinstance(item, mobu.FBCharacter)]

        # Get a list of folders that are not empty
        [lFolder.FBDelete() for lFolder in Globals.gFolders if not len(lFolder.Items)]

        QtGui.QMessageBox.warning(self, "Finished", "Removed all the empty folders")


@Run_(title='Empty Folder Cleanup', size=[200, 50])
def Run():
    return EmptyFolderCleanupTool()


