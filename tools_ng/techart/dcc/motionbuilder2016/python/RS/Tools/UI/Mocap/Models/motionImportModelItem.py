from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class MotionImportTextModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    def __init__(self, gsAssetName, gsAssetRoot, defaultPath, motionImportPath, motionImportName, gsType, parent=None):
        super(MotionImportTextModelItem, self).__init__([], parent=parent)
        self._columnNumber = 2
        self._gsType = gsType
        self._gsAssetName = gsAssetName
        self._gsAssetRoot = gsAssetRoot
        self._defaultPath = defaultPath
        self._motionImportName = motionImportName
        self._motionImportPath = motionImportPath
        self._checked = False

    def SetCheckedState(self, boolean):
        '''
        sets check state to False for item
        '''
        self._checked = boolean

    def GetCheckedState(self):
        '''
        Return:
            Checked State (Boolean): current check state of gs asset's checkbox in ui
        '''
        return self._checked

    def GetGSAssetName(self):
        '''
        Return:
            Asset Name(String): name of the gs asset found in the scene
        '''
        return self._gsAssetName

    def GetGSAssetRoot(self):
        '''
        Return:
            Root Asset(FBModel): Parent/Root of the hierarchy for the gs asset found in the scene
        '''
        return self._gsAssetRoot

    def GetDefaultPath(self):
        '''
        Return:
            Path (string): path to point to for the popup's initial folder
        '''
        return self._defaultPath

    def GetGSType(self):
        '''
        Return:
            Type (string): the type of gs asset the object is
        '''
        return self._gsType

    def columnCount(self):
        '''
        returns the number of columns in view
        '''
        return self._columnNumber

    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._gsAssetName
            if column == 1:
                return self._motionImportName

        elif role == QtCore.Qt.ToolTipRole:
            if column == 1:
                return self._motionImportPath

        elif role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if self._checked:
                    return QtCore.Qt.CheckState.Checked
                else:
                    return QtCore.Qt.CheckState.Unchecked
        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        model data updated based off of user interaction with the view
        '''
        column = index.column()
        if role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if value == QtCore.Qt.CheckState.Checked:
                    self._checked = True
                else:
                    self._checked = False
                return True

        elif role == QtCore.Qt.EditRole:
            if column == 1:
                self._motionImportName = value
                return True

        elif role == QtCore.Qt.ToolTipRole:
            if column == 1:
                self._motionImportPath = value
                return True
        return False
