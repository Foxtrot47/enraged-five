"""
Interface for the ReviewCamera Library
"""

from PySide import QtGui

from RS.Tools import UI
from RS.Core.Camera import ReviewCamera


class ReviewCameraUI(UI.QtMainWindowBase):
    """
    GUI for the Review Camera library.
    Very minimalistic at this point
    """
    def __init__(self):
        """
        Constructor
        """
        super(ReviewCameraUI, self).__init__(title='Review Camera Tool', dockable=True)
        self._toolName = "Review Camera Tool"
        self._errorEmail = "Geoff.Samuel@rockstarnorth.com"
        
        self.setupUi()
        
    def setupUi(self):
        """
        Set up the UI and attach the connections
        """
        mainWidget = QtGui.QWidget()
        layout = QtGui.QVBoxLayout()
        
        banner = UI.BannerWidget(self._toolName, 
                                 emailCC=self._errorEmail
                                 )
        createReviewButton = QtGui.QPushButton("Create Review from Selection")
        self._useMoverCheckbox = QtGui.QCheckBox("Use Mover to move Review Camera")
        deleteReviewButton = QtGui.QPushButton("Remove all Review Cameras")
        spacer = QtGui.QSpacerItem(20,40,QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        
        banner.setMaximumHeight(30)
        
        layout.addWidget(banner)
        layout.addWidget(createReviewButton)
        layout.addWidget(self._useMoverCheckbox)
        layout.addWidget(deleteReviewButton)
        layout.addItem(spacer)
        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)
        
        createReviewButton.pressed.connect(self._handleCreateReviewButtonPress)
        deleteReviewButton.pressed.connect(self._handleDeleteReviewButtonPress)
        
        self.setMaximumHeight(135)
        
    def _handleCreateReviewButtonPress(self):
        """
        Internal Method
        
        Handle the button press for the create review button
        """
        useMover = self._useMoverCheckbox.isChecked()
        try:
            ReviewCamera.ReviewCamera.CreateReviewFromSelection(useMover)
        except ValueError:
            QtGui.QMessageBox.critical(
                                       self, 
                                       "Review Camera Tool", 
                                       "Nothing is selected to drive review camera"
                                       )
    
    def _handleDeleteReviewButtonPress(self):
        """
        Internal Method
        
        Handle the button press for the delete review cameras button
        """
        for camera in ReviewCamera.ReviewCamera.GetReviewCameras():
            # Delete the cameras
            camera.DeleteCamera()
    
    
def Run(show=True):
    """
    Standard method to show the UI in mobu
    """
    tool = ReviewCameraUI()
    return tool