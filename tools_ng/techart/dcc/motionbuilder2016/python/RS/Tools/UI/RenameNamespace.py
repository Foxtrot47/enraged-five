"""
Tool for renaming namespaces
"""
import pyfbsdk as mobu
from PySide import QtGui

from RS.Utils import Namespace
from RS.Tools.UI import Run
from RS.Core.Scene.models import ComponentModel


class Window(QtGui.QWidget):

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QWidget): parent widget
        """
        super(Window, self).__init__(parent=parent)
        layout = QtGui.QVBoxLayout()
        self.oldnameLineEdit = QtGui.QComboBox()
        self.newnameLineEdit = QtGui.QLineEdit()
        button = QtGui.QPushButton("Rename")
        button.pressed.connect(self.rename)

        model = ComponentModel.ComponentModel(mobu.FBNamespace)
        self.oldnameLineEdit.setModel(model)

        layout.addWidget(QtGui.QLabel("Old Name"))
        layout.addWidget(self.oldnameLineEdit)
        layout.addWidget(QtGui.QLabel("New Name"))
        layout.addWidget(self.newnameLineEdit)
        layout.addWidget(button)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)

    def rename(self):
        """
        Renames the selected namespace to match the new name
        """
        old = str(self.oldnameLineEdit.currentText())
        new = str(self.newnameLineEdit.text())
        result = Namespace.LiteRename(old, new)
        if result:
            QtGui.QMessageBox.information(None, "Information", "Namespace Renamed")
        else:
            QtGui.QMessageBox.warning(None,
                                      "Warning",
                                      "The old namespace is not being tracked by the reference editor.\n"
                                      "Aborting rename.")
        self.oldnameLineEdit.model().refresh()


@Run(title="Rename Namespace Tool")
def Run():
    return Window()
