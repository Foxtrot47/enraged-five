from pyfbsdk import *
from pyfbsdk_additions import *

from PySide import QtGui

import inspect
import os
import webbrowser

import RS.Tools.UI

from RS.Utils import Path
import RS.Core.AssetSetup.Lib as ass
from RS import Config, Globals, Perforce
import RS.Core.AssetSetup.Environments as env
import RS.Core.AssetSetup.Props as pro
import RS.Core.AssetSetup.Characters as cha
import RS.Core.AssetSetup.Animals as ani
import RS.Core.AssetSetup.VehicleRig
import RS.Core.AssetSetup.Vehicles as veh
import RS.Core.AssetSetup.vehicleDofs as vehDofs
import RS.Core.AssetSetup.CharactersContactPositions as con
import RS.Core.AssetSetup.LayeredTexture as LayeredTexture
import RS.Core.AssetSetup.Rayfire as ray
import RS.Core.AssetSetup.EntitySetUI as entity
import RS.Core.Character.Setup.ButtonLaunch as geo
import RS.Core.Character.Setup.AmbientStory as amb
import RS.Core.Character.AnimateControlRig as ctrl
import RS.Core.Face.FixFace as fixface
import RS.Utils.Creation as cre
import RS.Core.Expression.ExpressionToRelationParseXML as exp
from RS.Core.AssetSetup import characterBones as boneId

from RS.Core.Expression import rollbones
from RS.Core.AssetSetup import Animals_2
from RS import ProjectData
from RS.Core.AssetSetup import controlRigGenerator

def rsCharExpressions():
    exp.rs_XMLPopup

class AssetSetupTool(RS.Tools.UI.Base):
    def __init__(self):
        RS.Tools.UI.Base.__init__(self, "Asset Setup", size=[ 402, 400 ], helpUrl='https://hub.gametools.dev/display/RSGGTAV/GTA+V+Scene+and+Asset+Setup')

        self.xmlSetupButton = None
    
    def findExpressionPath(self):
        Dummy = FBFindModelByLabelName("Dummy01")
        expressionPath = None
        if Dummy:
            maxProperty = Dummy.PropertyList.Find('UDP3DSMAX')
            if maxProperty:
                dataList = maxProperty.Data.split('\n')
                for data in dataList:
                    if 'REVISION' in data:
                        maxRevision = data.replace('\r', '')
                        maxRevisionResult = maxRevision.split(' ')[-1]
                    elif 'filename' in data:
                        maxPath = data.replace('\r', '')
                        maxPathResult = maxPath.split('= ')[-1]
                        maxFolder = maxPathResult.split(".max")[0]
                        expressionPath =  maxFolder + "_RSNXML.xml"
        return expressionPath

    def Create(self, pMain):
        lTabControl = FBTabControl()
        lTabControlX = FBAddRegionParam(0, FBAttachType.kFBAttachLeft, "")
        lTabControlY = FBAddRegionParam(self.BannerHeight, FBAttachType.kFBAttachTop, "")
        lTabControlW = FBAddRegionParam(-5, FBAttachType.kFBAttachRight, "")
        lTabControlH = FBAddRegionParam(-5, FBAttachType.kFBAttachBottom, "")
        pMain.AddRegion("lTabControl", "lTabControl", lTabControlX, lTabControlY, lTabControlW, lTabControlH)
        pMain.SetControl("lTabControl", lTabControl)

        # UI Tabs #

        # Animals
        lAnimalTabContainer = FBLayout()
        lAnimalTabContainerX = FBAddRegionParam(0, FBAttachType.kFBAttachLeft, "")
        lAnimalTabContainerY = FBAddRegionParam(0, FBAttachType.kFBAttachBottom, "")
        lAnimalTabContainerW = FBAddRegionParam(0, FBAttachType.kFBAttachRight, "")
        lAnimalTabContainerH = FBAddRegionParam(0, FBAttachType.kFBAttachTop, "")
        lAnimalTabContainer.AddRegion("lAnimalTabContainer", "lAnimalTabContainer", lAnimalTabContainerX, lAnimalTabContainerY, lAnimalTabContainerW, lAnimalTabContainerH)
        lTabControl.Add("Animals", lAnimalTabContainer)

        project = str(RS.Config.Project.Name).lower()
        if not project == 'gta5':
            self.animalSetupButton = FBButton()
            self.animalSetupButton.Name = 'Setup'
            self.animalSetupButton.Caption = "Animal Setup"
            self.animalSetupButton.Look = FBButtonLook.kFBLookPush
            x = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
            y = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "")
            w = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
            h = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("self.animalSetupButton", "self.animalSetupButton", x, y, w, h)
            lAnimalTabContainer.SetControl("self.animalSetupButton", self.animalSetupButton)
            self.animalSetupButton.OnClick.Add(self.SetupAnimals)

            self.xmlSetupButton = FBButton()
            self.xmlSetupButton.Name = 'XML'
            self.xmlSetupButton.Hint = "Saves off an xml from a manually characterized model.\n\nSetup will continue to finalise."
            self.xmlSetupButton.Caption = "Create Skeleton XML - setup will also complete"
            self.xmlSetupButton.Look = FBButtonLook.kFBLookPush
            x = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
            y = FBAddRegionParam(8, FBAttachType.kFBAttachBottom, "self.animalSetupButton")
            w = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
            h = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("self.xmlSetupButton", "self.xmlSetupButton", x, y, w, h)
            lAnimalTabContainer.SetControl("self.xmlSetupButton", self.xmlSetupButton)
            self.xmlSetupButton.OnClick.Add(self.SetupAnimals)
            self.xmlSetupButton.Enabled = False

            unlockImage = os.path.join(RS.Config.Script.Path.ToolImages,
                                               'AssetSetup',
                                               'unlocked.png')
            lockImage = os.path.join(RS.Config.Script.Path.ToolImages,
                                               'AssetSetup',
                                               'locked.png')
            self.lockButton = FBButton()
            self.lockButton.Hint = "allows user to force lock/unlock the\n'Prep/Create Skeleton XML' buttons."
            self.lockButton.Style = FBButtonStyle.kFBBitmap2States
            self.lockButton.SetImageFileNames(lockImage, unlockImage)
            x = FBAddRegionParam(8, FBAttachType.kFBAttachRight, "self.xmlSetupButton")
            y = FBAddRegionParam(13, FBAttachType.kFBAttachBottom, "self.animalSetupButton")
            w = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
            h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("self.lockButton", "self.lockButton", x, y, w, h)
            lAnimalTabContainer.SetControl("self.lockButton", self.lockButton)
            self.lockButton.OnClick.Add(self._lockClicked)

            expressionSetupButton = FBButton()
            expressionSetupButton.Caption = "Expression Setup"
            expressionSetupButton.Look = FBButtonLook.kFBLookPush
            x = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
            y = FBAddRegionParam(20, FBAttachType.kFBAttachBottom, "self.xmlSetupButton")
            w = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
            h = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("expressionSetupButton", "expressionSetupButton", x, y, w, h)
            lAnimalTabContainer.SetControl("expressionSetupButton", expressionSetupButton)
            expressionSetupButton.OnClick.Add(exp.rs_XMLPopup)

            self.controlRigSaveButton = FBButton()
            self.controlRigSaveButton.Caption = 'Save Out Control Rig XML'
            self.controlRigSaveButton.Hint = "Creates/Updates an xml with latest control rig present in scene"
            self.controlRigSaveButton.Look = FBButtonLook.kFBLookPush
            x = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
            y = FBAddRegionParam(20, FBAttachType.kFBAttachBottom, "expressionSetupButton")
            w = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
            h = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("self.controlRigSaveButton", "self.controlRigSaveButton", x, y, w, h)
            lAnimalTabContainer.SetControl("self.controlRigSaveButton", self.controlRigSaveButton)
            self.controlRigSaveButton.OnClick.Add(self._writeControlRigXML)
            self.controlRigSaveButton.Enabled = False

            self.lockButton2 = FBButton()
            self.lockButton2.Name = 'rig'
            self.lockButton2.Hint = "This button should only be\nused by Riggers."
            self.lockButton2.Style = FBButtonStyle.kFBBitmap2States
            self.lockButton2.SetImageFileNames(lockImage, unlockImage)
            x = FBAddRegionParam(8, FBAttachType.kFBAttachRight, "self.controlRigSaveButton")
            y = FBAddRegionParam(22, FBAttachType.kFBAttachBottom, "expressionSetupButton")
            w = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
            h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("self.lockButton2", "self.lockButton2", x, y, w, h)
            lAnimalTabContainer.SetControl("self.lockButton2", self.lockButton2)
            self.lockButton2.OnClick.Add(self._lockClicked)

        else:
            lAnimalAquaticSetupButton = FBButton()
            lAnimalAquaticSetupButton.Hint = "Sets up Aquatic Animals - shark, fish."
            lAnimalAquaticSetupButton.Caption = "Aquatic Animal Setup"
            lAnimalAquaticSetupButton.Look = FBButtonLook.kFBLookPush
            lAnimalAquaticSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
            lAnimalAquaticSetupButtonY = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "")
            lAnimalAquaticSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
            lAnimalAquaticSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("lAnimalAquaticSetupButton", "lAnimalAquaticSetupButton", lAnimalAquaticSetupButtonX, lAnimalAquaticSetupButtonY, lAnimalAquaticSetupButtonW, lAnimalAquaticSetupButtonH)
            lAnimalTabContainer.SetControl("lAnimalAquaticSetupButton", lAnimalAquaticSetupButton)
            lAnimalAquaticSetupButton.OnClick.Add(self.setupVAnimal)

            lAnimalAvianSetupButton = FBButton()
            lAnimalAvianSetupButton.Hint = "Sets up Avian Animals - hen, pigeon, seagull."
            lAnimalAvianSetupButton.Caption = "Avian Animal Setup"
            lAnimalAvianSetupButton.Look = FBButtonLook.kFBLookPush
            lAnimalAvianSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
            lAnimalAvianSetupButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lAnimalAquaticSetupButton")
            lAnimalAvianSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
            lAnimalAvianSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("lAnimalAvianSetupButton", "lAnimalAvianSetupButton", lAnimalAvianSetupButtonX, lAnimalAvianSetupButtonY, lAnimalAvianSetupButtonW, lAnimalAvianSetupButtonH)
            lAnimalTabContainer.SetControl("lAnimalAvianSetupButton", lAnimalAvianSetupButton)
            lAnimalAvianSetupButton.OnClick.Add(self.setupVAnimal)

            lAnimalQuadrupedSetupButton = FBButton()
            lAnimalQuadrupedSetupButton.Hint = "Sets up Quadruped Animals - dogs, horse, boar, cow, coyote, lion, pig, rat."
            lAnimalQuadrupedSetupButton.Caption = "Quadruped Animal Setup"
            lAnimalQuadrupedSetupButton.Look = FBButtonLook.kFBLookPush
            lAnimalQuadrupedSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
            lAnimalQuadrupedSetupButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lAnimalAvianSetupButton")
            lAnimalQuadrupedSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
            lAnimalQuadrupedSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("lAnimalQuadrupedSetupButton", "lAnimalQuadrupedSetupButton", lAnimalQuadrupedSetupButtonX, lAnimalQuadrupedSetupButtonY, lAnimalQuadrupedSetupButtonW, lAnimalQuadrupedSetupButtonH)
            lAnimalTabContainer.SetControl("lAnimalQuadrupedSetupButton", lAnimalQuadrupedSetupButton)
            lAnimalQuadrupedSetupButton.OnClick.Add(self.setupVAnimal)

            lAnimalBipedSetupButton = FBButton()
            lAnimalBipedSetupButton.Hint = "Sets up Biped Animals - chimps"
            lAnimalBipedSetupButton.Caption = "Biped Animal Setup"
            lAnimalBipedSetupButton.Look = FBButtonLook.kFBLookPush
            lAnimalBipedSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
            lAnimalBipedSetupButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lAnimalQuadrupedSetupButton")
            lAnimalBipedSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
            lAnimalBipedSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("lAnimalBipedSetupButton", "lAnimalBipedSetupButton", lAnimalBipedSetupButtonX, lAnimalBipedSetupButtonY, lAnimalBipedSetupButtonW, lAnimalBipedSetupButtonH)
            lAnimalTabContainer.SetControl("lAnimalBipedSetupButton", lAnimalBipedSetupButton)
            lAnimalBipedSetupButton.OnClick.Add(self.setupVAnimal)

            lAnimalDolphinSetupButton = FBButton()
            lAnimalDolphinSetupButton.Hint = "Sets up Dolphin style Animals - From Mr Skeleton 2 assets.."
            lAnimalDolphinSetupButton.Caption = "Mr Skel2 Dolphin"
            lAnimalDolphinSetupButton.Look = FBButtonLook.kFBLookPush
            lAnimalDolphinSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
            lAnimalDolphinSetupButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lAnimalBipedSetupButton")
            lAnimalDolphinSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
            lAnimalDolphinSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
            lAnimalTabContainer.AddRegion("lAnimalDolphinSetupButton", "lAnimalDolphinSetupButton", lAnimalDolphinSetupButtonX, lAnimalDolphinSetupButtonY, lAnimalDolphinSetupButtonW, lAnimalDolphinSetupButtonH)
            lAnimalTabContainer.SetControl("lAnimalDolphinSetupButton", lAnimalDolphinSetupButton)
            lAnimalDolphinSetupButton.OnClick.Add(self.setupVAnimal)

        # Characters
        lCharacterTabContainer = FBLayout()
        lCharacterTabContainerX = FBAddRegionParam(0, FBAttachType.kFBAttachLeft, "")
        lCharacterTabContainerY = FBAddRegionParam(0, FBAttachType.kFBAttachBottom, "")
        lCharacterTabContainerW = FBAddRegionParam(0, FBAttachType.kFBAttachRight, "")
        lCharacterTabContainerH = FBAddRegionParam(0, FBAttachType.kFBAttachTop, "")
        lCharacterTabContainer.AddRegion("lCharacterTabContainer", "lCharacterTabContainer", lCharacterTabContainerX, lCharacterTabContainerY, lCharacterTabContainerW, lCharacterTabContainerH)
        lTabControl.Add("Chars", lCharacterTabContainer)

        expressionEditLabel = FBLabel()
        expressionEditLabel.Caption = 'Set Expr File:'
        butx = FBAddRegionParam(58,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(25,FBAttachType.kFBAttachTop,"")
        butw = FBAddRegionParam(100,FBAttachType.kFBAttachNone,"")
        buth = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCharacterTabContainer.AddRegion("expressionEditLabel","expressionEditLabel", butx,buty,butw,buth)
        lCharacterTabContainer.SetControl("expressionEditLabel", expressionEditLabel)
        
        self.expressionEdit = FBEdit()
        self.expressionEdit.Hint = 'Paste a link to the expressions path here'
        butx = FBAddRegionParam(70,FBAttachType.kFBAttachLeft,"expressionEditLabel")
        buty = FBAddRegionParam(25,FBAttachType.kFBAttachTop,"")
        butw = FBAddRegionParam(193,FBAttachType.kFBAttachNone,"")
        buth = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCharacterTabContainer.AddRegion("self.expressionEdit","self.expressionEdit", butx,buty,butw,buth)
        lCharacterTabContainer.SetControl("self.expressionEdit", self.expressionEdit)
        self.expressionEdit.Text = self.findExpressionPath()

        expressionPathButton = FBButton()
        expressionPathButton.Hint = "Choose your Expression file"
        expressionPathButton.Caption = ">"
        expressionPathButton.Look = FBButtonLook.kFBLookPush
        x = FBAddRegionParam(10, FBAttachType.kFBAttachRight, "self.expressionEdit")
        y = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "")
        w = FBAddRegionParam(15, FBAttachType.kFBAttachNone, "")
        h = FBAddRegionParam(20, FBAttachType.kFBAttachNone, "")
        lCharacterTabContainer.AddRegion("expressionPathButton", "expressionPathButton", x, y, w, h)
        lCharacterTabContainer.SetControl("expressionPathButton", expressionPathButton)
        expressionPathButton.OnClick.Add(self.getExpressions)

        lCharacterCSSetupButton = FBButton()
        lCharacterCSSetupButton.Hint = "Character will be setup with a Cutscene Setup"
        lCharacterCSSetupButton.Caption = "Cutscene Character Setup"
        lCharacterCSSetupButton.Look = FBButtonLook.kFBLookPush
        lCharacterCSSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lCharacterCSSetupButtonY = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "self.expressionEdit")
        lCharacterCSSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lCharacterCSSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lCharacterTabContainer.AddRegion("lCharacterCSSetupButton", "lCharacterCSSetupButton", lCharacterCSSetupButtonX, lCharacterCSSetupButtonY, lCharacterCSSetupButtonW, lCharacterCSSetupButtonH)
        lCharacterTabContainer.SetControl("lCharacterCSSetupButton", lCharacterCSSetupButton)
        lCharacterCSSetupButton.OnClick.Add(self.setupCSCharacter)

        lCharacterIGSetupButton = FBButton()
        lCharacterIGSetupButton.Hint = "Character will be setup with an InGame Setup"
        lCharacterIGSetupButton.Caption = "InGame Character Setup"
        lCharacterIGSetupButton.Look = FBButtonLook.kFBLookPush
        lCharacterIGSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lCharacterIGSetupButtonY = FBAddRegionParam(30, FBAttachType.kFBAttachTop, "lCharacterCSSetupButton")
        lCharacterIGSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lCharacterIGSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lCharacterTabContainer.AddRegion("lCharacterIGSetupButton", "lCharacterIGSetupButton", lCharacterIGSetupButtonX, lCharacterIGSetupButtonY, lCharacterIGSetupButtonW, lCharacterIGSetupButtonH)
        lCharacterTabContainer.SetControl("lCharacterIGSetupButton", lCharacterIGSetupButton)
        lCharacterIGSetupButton.OnClick.Add(self.setupIGCharacter)

        lCharacterContactSaveSetupButton = FBButton()
        lCharacterContactSaveSetupButton.Hint = "After manually adjusting the contact positons\npress this button to save the details,\nso it can be quickly loaded next time this character\nneeds to be setup"
        lCharacterContactSaveSetupButton.Caption = "Save Character Contact Position"
        lCharacterContactSaveSetupButton.Look = FBButtonLook.kFBLookPush
        lCharacterContactSaveSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lCharacterContactSaveSetupButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lCharacterIGSetupButton")
        lCharacterContactSaveSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lCharacterContactSaveSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lCharacterTabContainer.AddRegion("lCharacterContactSaveSetupButton", "lCharacterContactSaveSetupButton", lCharacterContactSaveSetupButtonX, lCharacterContactSaveSetupButtonY, lCharacterContactSaveSetupButtonW, lCharacterContactSaveSetupButtonH)
        lCharacterTabContainer.SetControl("lCharacterContactSaveSetupButton", lCharacterContactSaveSetupButton)
        lCharacterContactSaveSetupButton.OnClick.Add(con.rs_SaveContactPostionLaunch)
        # lCharacterContactSaveSetupButton.Enabled = False

        lCharacterContactLoadSetupButton = FBButton()
        lCharacterContactLoadSetupButton.Hint = "This is will load the contact position if there is a previous\nsave with a character of this name."
        lCharacterContactLoadSetupButton.Caption = "Load Character Contact Position"
        lCharacterContactLoadSetupButton.Look = FBButtonLook.kFBLookPush
        lCharacterContactLoadSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lCharacterContactLoadSetupButtonY = FBAddRegionParam(30, FBAttachType.kFBAttachTop, "lCharacterContactSaveSetupButton")
        lCharacterContactLoadSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lCharacterContactLoadSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lCharacterTabContainer.AddRegion("lCharacterContactLoadSetupButton", "lCharacterContactLoadSetupButton", lCharacterContactLoadSetupButtonX, lCharacterContactLoadSetupButtonY, lCharacterContactLoadSetupButtonW, lCharacterContactLoadSetupButtonH)
        lCharacterTabContainer.SetControl("lCharacterContactLoadSetupButton", lCharacterContactLoadSetupButton)
        lCharacterContactLoadSetupButton.OnClick.Add(con.rs_LoadContactPostionLaunch)
        # lCharacterContactLoadSetupButton.Enabled = False

        saveCharGeoGroups = FBButton()
        saveCharGeoGroups.Hint = "This is will save the character geo groups."
        saveCharGeoGroups.Caption = "Save Variations"
        saveCharGeoGroups.Look = FBButtonLook.kFBLookPush
        saveCharGeoGroupsButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        saveCharGeoGroupsButtonY = FBAddRegionParam(30, FBAttachType.kFBAttachTop, "lCharacterContactLoadSetupButton")
        saveCharGeoGroupsButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        saveCharGeoGroupsButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lCharacterTabContainer.AddRegion("saveCharGeoGroups", "saveCharGeoGroups", saveCharGeoGroupsButtonX, saveCharGeoGroupsButtonY, saveCharGeoGroupsButtonW, saveCharGeoGroupsButtonH)
        lCharacterTabContainer.SetControl("saveCharGeoGroups", saveCharGeoGroups)
        saveCharGeoGroups.OnClick.Add(self.saveCharGeoGroups)

        # Facial
        lFaceTabContainer = FBLayout()
        lFaceTabContainerX = FBAddRegionParam(0, FBAttachType.kFBAttachLeft, "")
        lFaceTabContainerY = FBAddRegionParam(0, FBAttachType.kFBAttachBottom, "")
        lFaceTabContainerW = FBAddRegionParam(0, FBAttachType.kFBAttachRight, "")
        lFaceTabContainerH = FBAddRegionParam(0, FBAttachType.kFBAttachTop, "")
        lFaceTabContainer.AddRegion("lFaceTabContainer", "lFaceTabContainer", lFaceTabContainerX, lFaceTabContainerY, lFaceTabContainerW, lFaceTabContainerH)
        lTabControl.Add("Facial", lFaceTabContainer)

        l3LButton = FBButton()
        l3LButton.Hint = "Hooks up the face rig for CS & CSB Characters"
        l3LButton.Caption = "3Lateral Setup (incl. B-Rigs)"
        l3LButton.Look = FBButtonLook.kFBLookPush
        l3LButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        l3LButtonY = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "")
        l3LButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        l3LButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lFaceTabContainer.AddRegion("l3LButton", "l3LButton", l3LButtonX, l3LButtonY, l3LButtonW, l3LButtonH)
        lFaceTabContainer.SetControl("l3LButton", l3LButton)
        l3LButton.OnClick.Add(cha.rs_CSFacialSetup)

        lAmbientButton = FBButton()
        lAmbientButton.Hint = "Hooks up the face rig for Ambient Characters & Animals"
        lAmbientButton.Caption = "Ambient Setup (incl. Animals)"
        lAmbientButton.Look = FBButtonLook.kFBLookPush
        lAmbientButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lAmbientButtonY = FBAddRegionParam(30, FBAttachType.kFBAttachTop, "l3LButton")
        lAmbientButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lAmbientButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lFaceTabContainer.AddRegion("lAmbientButton", "lAmbientButton", lAmbientButtonX, lAmbientButtonY, lAmbientButtonW, lAmbientButtonH)
        lFaceTabContainer.SetControl("lAmbientButton", lAmbientButton)
        lAmbientButton.OnClick.Add(cha.rs_IGFacialSetup)

        lFaceBoneButton = FBButton()
        lFaceBoneButton.Hint = "Exports FaceBone Snapshot for other tools to repair broken facial in cutscenes."
        lFaceBoneButton.Caption = "Export FaceBone Snapshot"
        lFaceBoneButton.Look = FBButtonLook.kFBLookPush
        lFaceBoneButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lFaceBoneButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lAmbientButton")
        lFaceBoneButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lFaceBoneButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lFaceTabContainer.AddRegion("lFaceBoneButton", "lFaceBoneButton", lFaceBoneButtonX, lFaceBoneButtonY, lFaceBoneButtonW, lFaceBoneButtonH)
        lFaceTabContainer.SetControl("lFaceBoneButton", lFaceBoneButton)

        def lFaceBoneButtonClicked (pControl, pEvent):
            fixface.exportMinMax(None)
        lFaceBoneButton.OnClick.Add(lFaceBoneButtonClicked)

        lBlushButton = FBButton()
        lBlushButton.Hint = "Sets up blush for faces."
        lBlushButton.Caption = "Setup Blush/Tinting"
        lBlushButton.Look = FBButtonLook.kFBLookPush
        lBlushButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lBlushButtonY = FBAddRegionParam(30, FBAttachType.kFBAttachTop, "lFaceBoneButton")
        lBlushButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lBlushButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lFaceTabContainer.AddRegion("lBlushButton", "lBlushButton", lBlushButtonX, lBlushButtonY, lBlushButtonW, lBlushButtonH)
        lFaceTabContainer.SetControl("lBlushButton", lBlushButton)

        def SetupBlush (pControl, pEvent):
            try: LayeredTexture.SetupBlush()
            except LayeredTexture.NoHeadModelsFoundError as e:
                FBMessageBox("Error", e.message, "OK")
            except LayeredTexture.MissingBlushTextureError as e:
                FBMessageBox("Error", e.message, "OK")
            except LayeredTexture.MissingBlushControlerError as e:
                FBMessageBox("Error", e.message, "OK")
        lBlushButton.OnClick.Add(SetupBlush)

        setup3L_b1 = FBButton()
        setup3L_b1.Hint = "Sets up constraints for characters with UFC"
        setup3L_b1.Caption = "Setup Placeholder UFC"
        setup3L_b1.Look = FBButtonLook.kFBLookPush
        setup3L_b1X = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        setup3L_b1Y = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lBlushButton")
        setup3L_b1W = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        setup3L_b1H = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lFaceTabContainer.AddRegion("setup3L_b1", "setup3L_b1", setup3L_b1X, setup3L_b1Y, setup3L_b1W, setup3L_b1H)
        lFaceTabContainer.SetControl("setup3L_b1", setup3L_b1)
        buttonState = ProjectData.data.UFCAssetSetupButtonState()
        setup3L_b1.Enabled = buttonState

        def setup3L_b1_Clicked (pControl, pEvent):
            import RS.Core.Face.UFC_Setup as ufc
            lNamespace = "UFC_Test"
            ufc.setupUFCConstraints()
            ufc.get3LFaceRig(lNamespace)
            ufc.setupUFCPreRotation(lNamespace)
            ufc.setupSceneForUFC(lNamespace)
            # FBSystem().Scene.NamespaceDeleteContent( lNamespace, FBPlugModificationFlag.kFBPlugAllContent, True )
        setup3L_b1.OnClick.Add(setup3L_b1_Clicked)

        lCharExprSolverButton = FBButton()
        lCharExprSolverButton.Hint = "This will setup the expression solver file for facial."
        lCharExprSolverButton.Caption = "PreSetup - Expr Solver"
        lCharExprSolverButton.Look = FBButtonLook.kFBLookPush
        lCharExprSolverButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lCharExprSolverButtonY = FBAddRegionParam(5, FBAttachType.kFBAttachBottom, "setup3L_b1")
        lCharExprSolverButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lCharExprSolverButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lFaceTabContainer.AddRegion("lCharExprSolverButton", "lCharExprSolverButton", lCharExprSolverButtonX, lCharExprSolverButtonY, lCharExprSolverButtonW, lCharExprSolverButtonH)
        lFaceTabContainer.SetControl("lCharExprSolverButton", lCharExprSolverButton)
        # lCharExprSolverButton.Enabled = False

        def lCharExprSolverButton_Clicked (pControl, pEvent):
            import RS.Core.Face.UFC_Setup as ufc
            lNamespace = "UFC_Test"
            ufc.setupUFCConstraints()
            ufc.get3LFaceRig(lNamespace)
            # ufc.setupUFCPreRotation( lNamespace )
            ufc.setupSceneForUFC(lNamespace)
            FBSystem().Scene.NamespaceDeleteContent(lNamespace, FBPlugModificationFlag.kFBPlugAllContent, True)
        lCharExprSolverButton.OnClick.Add(lCharExprSolverButton_Clicked)

        setup3L_b2 = FBButton()
        setup3L_b2.Hint = "Sets up facial groups"
        setup3L_b2.Caption = "Setup Facial Groups"
        setup3L_b2.Look = FBButtonLook.kFBLookPush
        setup3L_b2X = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        setup3L_b2Y = FBAddRegionParam(15, FBAttachType.kFBAttachBottom, "lCharExprSolverButton")
        setup3L_b2W = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        setup3L_b2H = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lFaceTabContainer.AddRegion("setup3L_b2", "setup3L_b2", setup3L_b2X, setup3L_b2Y, setup3L_b2W, setup3L_b2H)
        lFaceTabContainer.SetControl("setup3L_b2", setup3L_b2)
        buttonState = ProjectData.data.UFCAssetSetupButtonState()
        setup3L_b2.Enabled = buttonState

        def setup3L_b2_Clicked (pControl, pEvent):
            import RS.Core.Face.UFC_Setup as ufc
            ufc.setupUFCFacialGroups()
        setup3L_b2.OnClick.Add(setup3L_b2_Clicked)

        lCharacterScaleButton = FBButton()
        lCharacterScaleButton.Hint = "This will scale the head of the character."
        lCharacterScaleButton.Caption = "Scale Character's Head"
        lCharacterScaleButton.Look = FBButtonLook.kFBLookPush
        lCharacterScaleButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lCharacterScaleButtonY = FBAddRegionParam(15, FBAttachType.kFBAttachBottom, "setup3L_b2")
        lCharacterScaleButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lCharacterScaleButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lFaceTabContainer.AddRegion("lCharacterScaleButton", "lCharacterScaleButton", lCharacterScaleButtonX, lCharacterScaleButtonY, lCharacterScaleButtonW, lCharacterScaleButtonH)
        lFaceTabContainer.SetControl("lCharacterScaleButton", lCharacterScaleButton)
        lCharacterScaleButton.Enabled = False

        def lCharacterScaleButton_Clicked (pControl, pEvent):
            import RS.Core.Face.UFC_Setup as ufc
            ufc.rs_CSScaleHead()

        lCharacterScaleButton.OnClick.Add(lCharacterScaleButton_Clicked)

        # ROM
        lROMTabContainer = FBLayout()
        lROMTabContainerX = FBAddRegionParam(0, FBAttachType.kFBAttachLeft, "")
        lROMTabContainerY = FBAddRegionParam(0, FBAttachType.kFBAttachBottom, "")
        lROMTabContainerW = FBAddRegionParam(0, FBAttachType.kFBAttachRight, "")
        lROMTabContainerH = FBAddRegionParam(0, FBAttachType.kFBAttachTop, "")
        lROMTabContainer.AddRegion("lROMTabContainer", "lROMTabContainer", lROMTabContainerX, lROMTabContainerY, lROMTabContainerW, lROMTabContainerH)
        lTabControl.Add("ROM", lROMTabContainer)

        lROMButton = FBButton()
        lROMButton.Hint = "Runs a serious of aniamtions on every joint in the CTRL RIG.\n\nNOTE: Save before Running this, as you dont want to check\nthe file in with the rom still attached!"
        lROMButton.Caption = "Character ROM Test"
        lROMButton.Look = FBButtonLook.kFBLookPush
        lROMButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lROMButtonY = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "")
        lROMButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lROMButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lROMTabContainer.AddRegion("lROMButton", "lROMButton", lROMButtonX, lROMButtonY, lROMButtonW, lROMButtonH)
        lROMTabContainer.SetControl("lROMButton", lROMButton)
        lROMButton.OnClick.Add(ctrl.rs_RunROMTest)

        lAnimalROMButton = FBButton()
        lAnimalROMButton.Hint = "Runs a serious of aniamtions on every joint in the CTRL RIG.\n\nNOTE: Save before Running this, as you dont want to check\nthe file in with the rom still attached!"
        lAnimalROMButton.Caption = "Animal ROM Test (save before pressing)"
        lAnimalROMButton.Look = FBButtonLook.kFBLookPush
        lAnimalROMButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lAnimalROMButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lROMButton")
        lAnimalROMButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lAnimalROMButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lROMTabContainer.AddRegion("lAnimalROMButton", "lAnimalROMButton", lAnimalROMButtonX, lAnimalROMButtonY, lAnimalROMButtonW, lAnimalROMButtonH)
        lROMTabContainer.SetControl("lAnimalROMButton", lAnimalROMButton)
        lAnimalROMButton.Enabled = False
        # lAnimalROMButton.OnClick.Add()

        # Props
        lPropsTabContainer = FBLayout()
        lPropsTabContainerX = FBAddRegionParam(0, FBAttachType.kFBAttachLeft, "")
        lPropsTabContainerY = FBAddRegionParam(0, FBAttachType.kFBAttachBottom, "")
        lPropsTabContainerW = FBAddRegionParam(0, FBAttachType.kFBAttachRight, "")
        lPropsTabContainerH = FBAddRegionParam(0, FBAttachType.kFBAttachTop, "")
        lPropsTabContainer.AddRegion("lPropsTabContainer", "lPropsTabContainer", lPropsTabContainerX, lPropsTabContainerY, lPropsTabContainerW, lPropsTabContainerH)
        lTabControl.Add("Props", lPropsTabContainer)

        lPropsButton = FBButton()
        lPropsButton.Hint = "Setups up all props - skinned, non-skinned, weapons & CS"
        lPropsButton.Caption = "Prop Setup"
        lPropsButton.Look = FBButtonLook.kFBLookPush
        lPropsButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lPropsButtonY = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "")
        lPropsButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lPropsButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lPropsTabContainer.AddRegion("lPropsButton", "lPropsButton", lPropsButtonX, lPropsButtonY, lPropsButtonW, lPropsButtonH)
        lPropsTabContainer.SetControl("lPropsButton", lPropsButton)
        lPropsButton.OnClick.Add(pro.rs_AssetSetupProps)

        # Rayfire
        lRayfireTabContainer = FBLayout()
        lRayfireTabContainerX = FBAddRegionParam(0, FBAttachType.kFBAttachLeft, "")
        lRayfireTabContainerY = FBAddRegionParam(0, FBAttachType.kFBAttachBottom, "")
        lRayfireTabContainerW = FBAddRegionParam(0, FBAttachType.kFBAttachRight, "")
        lRayfireTabContainerH = FBAddRegionParam(0, FBAttachType.kFBAttachTop, "")
        lRayfireTabContainer.AddRegion("lRayfireTabContainer", "lRayfireTabContainer", lRayfireTabContainerX, lRayfireTabContainerY, lRayfireTabContainerW, lRayfireTabContainerH)
        lTabControl.Add("RF", lRayfireTabContainer)

        lRayfireSetupButton = FBButton()
        lRayfireSetupButton.Hint = "Setups up rayfire"
        lRayfireSetupButton.Caption = "RayFire Setup"
        lRayfireSetupButton.Look = FBButtonLook.kFBLookPush
        lRayfireSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lRayfireSetupButtonY = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "lVehicleSetupButton")
        lRayfireSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lRayfireSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lRayfireTabContainer.AddRegion("lRayfireSetupButton", "lRayfireSetupButton", lRayfireSetupButtonX, lRayfireSetupButtonY, lRayfireSetupButtonW, lRayfireSetupButtonH)
        lRayfireTabContainer.SetControl("lRayfireSetupButton", lRayfireSetupButton)
        lRayfireSetupButton.OnClick.Add(ray.rs_AssetSetupRayfire)

        # Environments
        lSetTabContainer = FBLayout()
        lSetTabContainerX = FBAddRegionParam(0, FBAttachType.kFBAttachLeft, "")
        lSetTabContainerY = FBAddRegionParam(0, FBAttachType.kFBAttachBottom, "")
        lSetTabContainerW = FBAddRegionParam(0, FBAttachType.kFBAttachRight, "")
        lSetTabContainerH = FBAddRegionParam(0, FBAttachType.kFBAttachTop, "")
        lSetTabContainer.AddRegion("lSetTabContainer", "lSetTabContainer", lSetTabContainerX, lSetTabContainerY, lSetTabContainerW, lSetTabContainerH)
        lTabControl.Add("Sets", lSetTabContainer)

        lSets3DSButton = FBButton()
        lSets3DSButton.Hint = "3dsMax Set setup"
        lSets3DSButton.Caption = "3dsMax Environment Setup"
        lSets3DSButton.Look = FBButtonLook.kFBLookPush
        lSets3DSButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lSets3DSButtonY = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "")
        lSets3DSButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lSets3DSButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lSetTabContainer.AddRegion("lSets3DSButton", "lSets3DSButton", lSets3DSButtonX, lSets3DSButtonY, lSets3DSButtonW, lSets3DSButtonH)
        lSetTabContainer.SetControl("lSets3DSButton", lSets3DSButton)
        lSets3DSButton.OnClick.Add(env.maxEnvironmentSetup)
        
        lSetsCategoryButton = FBButton()
        lSetsCategoryButton.Hint = "Updates the existing category file for the set"
        lSetsCategoryButton.Caption = "Save Updated Category File"
        lSetsCategoryButton.Look = FBButtonLook.kFBLookPush
        lSetsCategoryButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lSetsCategoryButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lSets3DSButton")
        lSetsCategoryButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lSetsCategoryButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lSetTabContainer.AddRegion("lSetsCategoryButton", "lSetsCategoryButton", lSetsCategoryButtonX, lSetsCategoryButtonY, lSetsCategoryButtonW, lSetsCategoryButtonH)
        lSetTabContainer.SetControl("lSetsCategoryButton", lSetsCategoryButton)
        lSetsCategoryButton.OnClick.Add(self.setupCategories)
        
        lSetsForceCategoryButton = FBButton()
        lSetsForceCategoryButton.Hint = "Completely rewrites the category file for the set"
        lSetsForceCategoryButton.Caption = "Force Update Category File"
        lSetsForceCategoryButton.Look = FBButtonLook.kFBLookPush
        lSetsForceCategoryButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lSetsForceCategoryButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lSetsCategoryButton")
        lSetsForceCategoryButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lSetsForceCategoryButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lSetTabContainer.AddRegion("lSetsForceCategoryButton", "lSetsForceCategoryButton", lSetsForceCategoryButtonX, lSetsForceCategoryButtonY, lSetsForceCategoryButtonW, lSetsForceCategoryButtonH)
        lSetTabContainer.SetControl("lSetsForceCategoryButton", lSetsForceCategoryButton)
        lSetsForceCategoryButton.OnClick.Add(self.setupCategories)
        
        lSetsEntityButton = FBButton()
        lSetsEntityButton.Hint = "Opens the UI to configure Entity Sets for the environment"
        lSetsEntityButton.Caption = "Entity Set Group Setup"
        lSetsEntityButton.Look = FBButtonLook.kFBLookPush
        lSetsEntityButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lSetsEntityButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lSetsForceCategoryButton")
        lSetsEntityButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lSetsEntityButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lSetTabContainer.AddRegion("lSetsEntityButton", "lSetsEntityButton", lSetsEntityButtonX, lSetsEntityButtonY, lSetsEntityButtonW, lSetsEntityButtonH)
        lSetTabContainer.SetControl("lSetsEntityButton", lSetsEntityButton)
        lSetsEntityButton.OnClick.Add(entity.rs_setupEntitySets)

        lSetsHandleButton = FBButton()
        lSetsHandleButton.Hint = "Opens a UI which will allow you to assigns a\ncustomised handle to a selected object"
        lSetsHandleButton.Caption = "Handle Widget"
        lSetsHandleButton.Look = FBButtonLook.kFBLookPush
        lSetsHandleButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lSetsHandleButtonY = FBAddRegionParam(40, FBAttachType.kFBAttachTop, "lSetsEntityButton")
        lSetsHandleButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lSetsHandleButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lSetTabContainer.AddRegion("lSetsHandleButton", "lSetsHandleButton", lSetsHandleButtonX, lSetsHandleButtonY, lSetsHandleButtonW, lSetsHandleButtonH)
        lSetTabContainer.SetControl("lSetsHandleButton", lSetsHandleButton)
        lSetsHandleButton.Enabled = False

        # Vehicles
        lVehiclesTabContainer = FBLayout()
        lVehiclesTabContainerX = FBAddRegionParam(0, FBAttachType.kFBAttachLeft, "")
        lVehiclesTabContainerY = FBAddRegionParam(0, FBAttachType.kFBAttachBottom, "")
        lVehiclesTabContainerW = FBAddRegionParam(0, FBAttachType.kFBAttachRight, "")
        lVehiclesTabContainerH = FBAddRegionParam(0, FBAttachType.kFBAttachTop, "")
        lVehiclesTabContainer.AddRegion("lVehiclesTabContainer", "lVehiclesTabContainer", lVehiclesTabContainerX, lVehiclesTabContainerY, lVehiclesTabContainerW, lVehiclesTabContainerH)
        lTabControl.Add("Vehicles", lVehiclesTabContainer)

        lVehicleSetupButton = FBButton()
        lVehicleSetupButton.Hint = "Setups up Vehicles"
        lVehicleSetupButton.Caption = "Vehicle Setup"
        lVehicleSetupButton.Look = FBButtonLook.kFBLookPush
        lVehicleSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lVehicleSetupButtonY = FBAddRegionParam(25, FBAttachType.kFBAttachTop, "")
        lVehicleSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lVehicleSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lVehiclesTabContainer.AddRegion("lVehicleSetupButton", "lVehicleSetupButton", lVehicleSetupButtonX, lVehicleSetupButtonY, lVehicleSetupButtonW, lVehicleSetupButtonH)
        lVehiclesTabContainer.SetControl("lVehicleSetupButton", lVehicleSetupButton)
        lVehicleSetupButton.OnClick.Add(veh.rs_AssetSetupVehicles)

        lVehicleRigSetupButton = FBButton()
        lVehicleRigSetupButton.Hint = "Setups up suspension rig on vehicles"
        lVehicleRigSetupButton.Caption = "Suspension Rig Setup"
        lVehicleRigSetupButton.Look = FBButtonLook.kFBLookPush
        lVehicleRigSetupButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lVehicleRigSetupButtonY = FBAddRegionParam(30, FBAttachType.kFBAttachTop, "lVehicleSetupButton")
        lVehicleRigSetupButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lVehicleRigSetupButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lVehiclesTabContainer.AddRegion("lVehicleRigSetupButton", "lVehicleRigSetupButton", lVehicleRigSetupButtonX, lVehicleRigSetupButtonY, lVehicleRigSetupButtonW, lVehicleRigSetupButtonH)
        lVehiclesTabContainer.SetControl("lVehicleRigSetupButton", lVehicleRigSetupButton)
        lVehicleRigSetupButton.OnClick.Add(veh.rs_AssetSetupRigVehicles)

        lVehicleSetup4WheelButton = FBButton()
        lVehicleSetup4WheelButton.Hint = "Create and hooks up a rig for 4 wheeled vehicles (with various door configurations)."
        lVehicleSetup4WheelButton.Caption = "Setup 4 Wheeled Vehicle Rig"
        lVehicleSetup4WheelButton.Look = FBButtonLook.kFBLookPush
        lVehicleSetup4WheelButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        lVehicleSetup4WheelButtonY = FBAddRegionParam(30, FBAttachType.kFBAttachTop, "lVehicleRigSetupButton")
        lVehicleSetup4WheelButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        lVehicleSetup4WheelButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lVehiclesTabContainer.AddRegion("lVehicleSetup4WheelButton", "lVehicleSetup4WheelButton", lVehicleSetup4WheelButtonX, lVehicleSetup4WheelButtonY, lVehicleSetup4WheelButtonW, lVehicleSetup4WheelButtonH)
        lVehiclesTabContainer.SetControl("lVehicleSetup4WheelButton", lVehicleSetup4WheelButton)
        lVehicleSetup4WheelButton.OnClick.Add(RS.Core.AssetSetup.VehicleRig.Setup4WheelTrigger)

        vehicleDofButton = FBButton()
        vehicleDofButton.Hint = "Configures Degrees of Freedom limits on vehicle bones and controllers based on the data in the vehicle's skel file."
        vehicleDofButton.Caption = "Vehicle DOF Setup"
        vehicleDofButton.Look = FBButtonLook.kFBLookPush
        vehicleDofButtonX = FBAddRegionParam(58, FBAttachType.kFBAttachLeft, "")
        vehicleDofButtonY = FBAddRegionParam(30, FBAttachType.kFBAttachTop, "lVehicleSetup4WheelButton")
        vehicleDofButtonW = FBAddRegionParam(265, FBAttachType.kFBAttachNone, "")
        vehicleDofButtonH = FBAddRegionParam(25, FBAttachType.kFBAttachNone, "")
        lVehiclesTabContainer.AddRegion("vehicleDofButton", "vehicleDofButton", vehicleDofButtonX, vehicleDofButtonY, vehicleDofButtonW, vehicleDofButtonH)
        lVehiclesTabContainer.SetControl("vehicleDofButton", vehicleDofButton)
        vehicleDofButton.OnClick.Add(vehDofs.SetupVehicleDofs)

        lTabControl.SetContent(0)
    
    def getExpressions(self, control, event):
        charName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        expressionFile = self.runFilePopup(charName, control, event)
        self.expressionEdit.Text = expressionFile

    def setupCategories(self, control, event):
        fileName = Path.GetBaseNameNoExtension(Globals.Application.FBXFileName)
        env.saveSetCategories(fileName)

    def forceSetupCategories(self, control, event):
        fileName = Path.GetBaseNameNoExtension(Globals.Application.FBXFileName)
        env.saveSetCategories(fileName, force=True)
        

    def SetupAnimals(self, control, event):
        ass.rs_CleanupMaterials()
        modelSetup = Animals_2.ModelSetup()
        if control.Name == 'Setup':
            characterize = modelSetup.AnimalSetup()
            if characterize:
                self.animalSetupButton.Enabled = False
                self.xmlSetupButton.Enabled = True
                self.lockButton.State = 1
        elif control.Name == 'Prep':
            modelSetup.PrepCharacterForManualCharacterization()
        elif control.Name == 'XML':
            setupComplete = modelSetup.CreateSkeletonFBXSetupMetaFile()
            if not setupComplete:
                return
            self.animalSetupButton.Enabled = True
            self.xmlSetupButton.Enabled = False
            self.lockButton.State = 0

    def setupCSCharacter(self, control, event):
        lFileName = FBApplication().FBXFileName
        charName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        try:
            Perforce.AddOrEditFiles(lFileName, force=True, fileType = "binary+lm")
        except Perforce.P4Exception:
            Perforce.EnsureFileWritable(lFileName)
        ass.rs_CleanupMaterials()
        faceCompleted = False
        #main character setup
        cha.rs_AssetSetupCSCharacters(None, None)
        #geo setup
        geo.GeoSetup(None, None)
        
        #Define where our geo variation path lives
        geoVarPath = os.path.join(Config.Project.Path.Root,
                                  "art",
                                  "animation",
                                  "resources",
                                  "characters",
                                  "geovariations")
        
        # Define our character specific output file
        varFilePath = os.path.join(geoVarPath, (charName.lower() + ".xml"))
        
        # Sync latest on it from Perforce
        Perforce.Sync(["-f", os.path.join(geoVarPath, "...")])
        
        if os.path.exists(varFilePath):
            if amb.restoreCharGeoGroups(charName, varFilePath) == True:
                feedbackMessage = "Geo Variations restored from .xml. Please check these are correct."
            else:
                feedbackMessage = "Geo Variations are default. Please check these are configured as expected and then hit Save Variations."
        else:
            feedbackMessage = "Geo Variations are default. Please check these are configured as expected and then hit Save Variations."
        
        # Contact points
        if con.rs_LoadContactPostionLaunch(control, event) == True:
            feedbackMessage += "\nContact Points have been loaded."
        else:
            feedbackMessage += "\nContact Points need to be set up."
        
        # Setup face
        ambientFace = FBFindModelByLabelName("Ambient_UI")
        csFace = FBFindModelByLabelName("faceControls_OFF")
        if csFace:
            cha.rs_CSFacialSetup(control, event)
            faceCompleted = True
        elif ambientFace:
            cha.rs_IGFacialSetup(control, event)
            faceCompleted = True
        
        # Detect any Hash ID clashes
        boneId.findBoneClashes()
        
        # Setup expressions
        expressionFile = self.expressionEdit.Text
        if not os.path.exists(expressionFile):
            setupExpressions = FBMessageBox("Expressions Missing", "No expressions could be found. \nDo you want to choose an expression file?", "Select expressions", "Skip")
            if setupExpressions == 1:
                expressionFile = self.runFilePopup(charName, control, event)
        if not expressionFile == "":
            #rollbone setup
            rollboneCon = rollbones.RollBoneConstraints()
            rollboneCon.createRelationConstraints(expressionFile)
            #fix up expressions
            ProjectData.data.fixupExpressions()
            feedbackMessage += "\nExpressions constraints created."
        else:
            feedbackMessage += "\nExpressions setup DID NOT run to to a missing or incorrect expressions path."
        if faceCompleted == True:
            QtGui.QMessageBox.information(None, "Setup Complete", "Setup is complete, including facial setup. \n" + feedbackMessage +  "\nPlease check over the rig.")
        else:
            QtGui.QMessageBox.information(None, "Setup Complete", "Setup is complete. Facial setup did NOT complete. \n" + feedbackMessage +  "\nPlease check over the rig.")
        self.expressionEdit.Text = ""
        
        return

    def setupIGCharacter(self, control, event):
        lFileName = FBApplication().FBXFileName
        charName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        
        if Perforce.DoesFileExist(lFileName) == True:
            Perforce.Edit(lFileName)
        else:
            Perforce.Add(lFileName)
        
        ass.rs_CleanupMaterials()
        faceCompleted = False
        #main character setup
        cha.rs_AssetSetupIGCharacters(None, None)
        #geo setup
        geo.GeoSetup(None, None)
        
        #Define where our geo variation path lives
        geoVarPath = os.path.join(Config.Project.Path.Root,
                                  "art",
                                  "animation",
                                  "resources",
                                  "characters",
                                  "geovariations")
        
        # Define our character specific output file
        varFilePath = os.path.join(geoVarPath, (charName.lower() + ".xml"))
        
        # Sync latest on it from Perforce
        Perforce.Sync(["-f", os.path.join(geoVarPath, "...")])
        
        if os.path.exists(varFilePath):
            if amb.restoreCharGeoGroups(charName, varFilePath) == True:
                feedbackMessage = "Geo Variations restored from .xml. Please check these are correct."
            else:
                feedbackMessage = "Geo Variations are default. Please check these are configured as expected and then hit Save Variations."
        else:
            feedbackMessage = "Geo Variations are default. Please check these are configured as expected and then hit Save Variations."
        
        # Contact points
        if con.rs_LoadContactPostionLaunch(control, event) == True:
            feedbackMessage += "\nContact Points have been loaded."
        else:
            feedbackMessage += "\nContact Points need to be set up."
        
        # Setup face
        ambientFace = FBFindModelByLabelName("Ambient_UI")
        csFace = FBFindModelByLabelName("faceControls_OFF")
        if csFace:
            cha.rs_CSFacialSetup(control, event)
            faceCompleted = True
        elif ambientFace:
            cha.rs_IGFacialSetup(control, event)
            faceCompleted = True
        
        # Detect any Hash ID clashes
        boneId.findBoneClashes()
        
        # Setup expressions
        expressionFile = self.expressionEdit.Text
        if not os.path.exists(expressionFile):
            setupExpressions = FBMessageBox("Expressions Missing", "No expressions could be found. \nDo you want to choose an expression file?", "Select expressions", "Skip")
            if setupExpressions == 1:
                expressionFile = self.runFilePopup(charName, control, event)
        if not expressionFile == "":
            #rollbone setup
            rollboneCon = rollbones.RollBoneConstraints()
            rollboneCon.createRelationConstraints(expressionFile)
            #fix up expressions
            ProjectData.data.fixupExpressions()
            feedbackMessage += "\nExpressions constraints created."
        else:
            feedbackMessage += "\nExpressions setup DID NOT run to to a missing or incorrect expressions path."
        if faceCompleted == True:
            QtGui.QMessageBox.information(None, "Setup Complete", "Setup is complete, including facial setup. \n" + feedbackMessage +  "\nPlease check over the rig.")
        else:
            QtGui.QMessageBox.information(None, "Setup Complete", "Setup is complete. Facial setup did NOT complete. \n" + feedbackMessage +  "\nPlease check over the rig.")
        self.expressionEdit.Text = ""
    
        return
    
    def saveCharGeoGroups(self, control, event):
        characterName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        geoVarPath = os.path.join(Config.Project.Path.Root,
                                "art",
                                "animation",
                                "resources",
                                "characters",
                                "geovariations")
        
        # Define our character specific output file
        varFilePath = os.path.join(geoVarPath, (characterName.lower() + ".xml"))
        amb.saveCharGeoGroups(characterName, varFilePath)
    
    def setupVAnimal(self, control, event):
        lFileName = FBApplication().FBXFileName
        charName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        
        if Perforce.DoesFileExist(lFileName) == True:
            Perforce.Edit(lFileName)
        else:
            Perforce.Add(lFileName)
        
        expressionFile = self.expressionEdit.Text
        
        ass.rs_CleanupMaterials()
        faceCompleted = False
        #main character setup
        if control.Caption == "Quadruped Animal Setup":
            print "Running quadruped setup"
            ani.rs_QuadrupedAnimalSetup(control, event)
            FBSystem().Scene.Evaluate()
        elif control.Caption == "Avian Animal Setup":
            ani.rs_AvianAnimalSetup(control, event)
            FBSystem().Scene.Evaluate()
        elif control.Caption == "Aquatic Animal Setup":
            ani.rs_AquaticAnimalSetup(control, event)
            FBSystem().Scene.Evaluate()
        elif control.Caption == "Biped Animal Setup":
            ani.rs_BipedAnimalSetup(control, event)
            FBSystem().Scene.Evaluate()
        elif control.Caption == "Mr Skel2 Dolphin":
            ani.rs_DolphinAnimalSetup(control, event)
            FBSystem().Scene.Evaluate()
        
        #Define where our geo variation path lives
        geoVarPath = os.path.join(Config.Project.Path.Root,
                                  "art",
                                  "animation",
                                  "resources",
                                  "characters",
                                  "geovariations")
        
        varFilePath = os.path.join(geoVarPath, (charName.lower() + ".xml"))
        
        # Setup geo variations
        if os.path.exists(varFilePath):
            if amb.restoreCharGeoGroups(charName, varFilePath) == True:
                feedbackMessage = "Geo Variations restored from .xml. Please check these are correct."
            else:
                feedbackMessage = "Geo Variations are default. Please check these are configured as expected and then hit Save Variations."
        else:
            feedbackMessage = "Geo Variations are default. Please check these are configured as expected and then hit Save Variations."
        
        # Setup ambient face
        ambientFace = FBFindModelByLabelName("Ambient_UI")
        if ambientFace:
            cha.rs_IGFacialSetup(control, event)
            faceCompleted = True
        
        # Detect any Hash ID clashes
        boneId.findBoneClashes()
        
        # Setup expressions
        expressionFile = self.expressionEdit.Text
        if not os.path.exists(expressionFile):
            setupExpressions = FBMessageBox("Expressions Missing", "No expressions could be found. \nDo you want to choose an expression file?", "Select expressions", "Skip")
            if setupExpressions == 1:
                expressionFile = self.runFilePopup(charName, control, event)
        if not expressionFile == "":
            #rollbone setup
            rollboneCon = rollbones.RollBoneConstraints()
            rollboneCon.createRelationConstraints(expressionFile)
            #fix up expressions
            ProjectData.data.fixupExpressions()
            feedbackMessage += "\nExpressions constraints created."
        else:
            feedbackMessage += "\nExpressions setup DID NOT run to to a missing or incorrect expressions path."
        if faceCompleted == True:
            QtGui.QMessageBox.information(None, "Setup Complete", "Setup is complete, including facial setup. \n" + feedbackMessage +  "\nPlease check over the rig.")
        else:
            QtGui.QMessageBox.information(None, "Setup Complete", "Setup is complete. Facial setup did NOT complete. \n" + feedbackMessage +  "\nPlease check over the rig.")
        self.expressionEdit.Text = ""
        
        return

    def runFilePopup(self, characterName, control, event):
        expressionFile = self.expressionEdit.Text
        expressionPath = expressionFile.split(characterName)[0] + characterName + "\\"
        filePopup = FBFilePopup();
        filePopup.Caption = "Chose your character's expression XML"
        filePopup.Filter = '*.xml'
        filePopup.Style = FBFilePopupStyle.kFBFilePopupOpen

        # Set the default path.
        
        filePopup.Path = expressionPath

        filePath = ""

        if filePopup.Execute():
            filePath = filePopup.FullFilename
        else:
            FBMessageBox("Warning:", "Selection canceled!", "OK")
        
        return filePath


    def _lockClicked(self, control, event):
        if control.Name == 'rig':
            if self.lockButton2.State == 0:
                self.controlRigSaveButton.Enabled = False
            else:
                self.controlRigSaveButton.Enabled = True
        else:
            if control.State == 0:
                self.xmlSetupButton.Enabled = False
                self.animalSetupButton.Enabled = True
            if control.State == 1:
                self.xmlSetupButton.Enabled = True
                self.animalSetupButton.Enabled = False

    def _writeControlRigXML(self, control, event):
        # check if a rig exists first
        if Globals.Characters[0] and Globals.Characters[0].GetCtrlRigModel(FBBodyNodeId.kFBHipsNodeId):
            # check if model is an animal or human
            isAnimalModel = False
            filePath = FBApplication().FBXFileName
            filPathSplit = filePath.split("\\")
            for split in filPathSplit:
                if "animal" in split.lower():
                    isAnimalModel = True
                    break
            # get character name
            characterName = Globals.Characters[0].Name
            # converts the control rig into an xml
            controlRig = controlRigGenerator.ControlRigGenerator()
            # check if the character's folder exists, if not create it
            if not isAnimalModel:
                characterFolderPath = os.path.join(Config.Tool.Path.Root,
                                                    "etc",
                                                    "config",
                                                    "characters",
                                                    "skeleton",
                                                    "male")
            else:
                characterFolderPath = os.path.join(Config.Tool.Path.Root,
                                                    "etc",
                                                    "config",
                                                    "characters",
                                                    "skeleton",
                                                    "animals",
                                                    "{0}".format(characterName))
            if not os.path.exists(characterFolderPath):
                os.makedirs(characterFolderPath)
            # controlrig file
            controlRigFile = os.path.join(characterFolderPath,
                                          "controlrig.xml")
            # if file exists check it out for saving over
            if os.path.exists(controlRigFile):
                Perforce.Edit(controlRigFile)
            controlRig.Write(controlRigFile)
            # Add to P4 if it is a new file
            Perforce.Add(controlRigFile)
            # disable button, lock icon
            self.controlRigSaveButton.Enabled = False
            self.lockButton2.State = 0
        else:
            QtGui.QMessageBox.warning(None,
                                      "Warning",
                                      "A Control Rig is missing.\n\nUnable to output any control rig information.",
                                      QtGui.QMessageBox.Ok)

def Run(show=True):
    tool = AssetSetupTool()

    if show:
        tool.Show()

    return tool
