import time
import re
import inspect

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.UI
import RS.Globals as glo
import RS.Utils.Creation as cre
import RS.Core.EST.Edit as core
import RS.Utils.Path


class EstEditTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "EST Edit", size = [ 971, 510 ] )
        
        self.pListView = None
        self.pLightView = None
        self.label = None
        self.renameControl = None 
        self.SelectedShot = None
        
        
    def RunEST(self, pControl, pEvent):
            print "Starting" 
            """ Can't even update the view to let the user know your doing stuff!!!"""
            #self.label.Caption = "Processing, Please Wait............."
            
            #self.UpdateReport(2,3)
            #return
            if (core.Est_edit.start()):
                 #core.Est_edit.utils.getESTNames()
                self.RefreshView(2,3)
                self.label.Caption = "EST Shot Track has been updated!"
            
            #FBMessageBox("EST Shot Set Up", "Shot Track Set Up !", "OK")   
            return
        
    def RenameClip(self, pControl, pEvent):
        # get selected index

        lStory = FBStory()
        lStory.LockedShot = False
        lTrackContainer = lStory.RootEditFolder.Tracks        
        
        for iTrack in lTrackContainer:
            if iTrack.Name.upper().startswith("SHOT_") and iTrack.Name.upper().endswith("_EST"):
                gTrack = iTrack 
                for clip in gTrack.Clips:
                    if clip.ShotCamera != None:
                        if self.SelectedShot == clip.Name:
                            # Need some validation
                            print clip.Name
                            lnewclipName = "AutoClip" + str(int(self.renameControl.Value)) +"_"+ RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
                            print "AutoClip" + str(int(self.renameControl.Value)) +"_"+ RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
                            clip.Name = lnewclipName
        
      
        self.RefreshView(0,1)
        self.RefreshView(2,3)
        return
    
    def SelectShot(self, pControl, pEvent):
        self.SelectedShot = pControl.GetCellValue(pEvent.Row,0)  
        
    def SetCells(self, subvalue, row, indexA, IndexB):
            #print self.pListView
            self.pListView.SetCellValue( row, indexA, subvalue.Name)  
            self.pListView.SetCellValue( row, IndexB, subvalue.CameraName)   
            
    def RefreshView(self, rowIndexA, rowIndexB):
        core.Est_edit.utils.getESTNames()
        row = 0
        for track in core.Est_edit.utils._oldTrackList:
            for clip in track.Clips:                
                bHasRow = self.pListView.GetCellValue(row, 0)
                                
                if bHasRow == None:
                    self.pListView.RowAdd( track.Name, row );  
                
                self.SetCells( clip, row, rowIndexA ,rowIndexB)
                #print "%s : %s : %s" % (track.Name, clip.Name, clip.CameraName) 
                row += 1                      
    
    def UpdateReport(self, rowIndexA, rowIndexB):
        core.Est_edit.utils.getESTNames()
        #print dir(self.pListView)
        print dir(self.pListView.GetRow)
        return
    
        for row in self.pListView.Row:
            print dir(row)
            GetRow    
        
    ## Layouts ##
    
    def CreateHeaders(self,pControl):
        pControl.ColumnAdd( "ClipName",1 );
        pControl.ColumnAdd( "Camera",2 ); 
        pControl.ColumnAdd( "Updated ClipName",3 ); 
        pControl.ColumnAdd( "Updated Camera",4 ); 
        #pControl.ColumnAdd( "Cutscene Light",5 ); 
        
        pControl.GetColumn( -1 ).Width = 130
        pControl.GetColumn( 1 ).Width = 130
        pControl.GetColumn( 3 ).Width = 130
        #pControl.GetColumn( 5 ).Width = 40
        """
        for i in range(1):
            pControl.GetColumn( i ).Width = 80
            pControl.GetColumn( i ).Justify = FBTextJustify.kFBTextJustifyCenter
            pControl.GetColumn( i ).ReadOnly = True

            """  

    def CreateEstRenameControls(self, pMain):
        lRenameNewButton = FBButton()
        lRenameNewButton.Hint = "Rename Selected"
        lRenameNewButton.Caption = "Rename Selected Clip Index"
        lRenameNewButton.Look = FBButtonLook.kFBLookPush
        pMain.AddRegion("lRenameNewButton","lRenameNewButton",
                    FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                    FBAddRegionParam(-30,FBAttachType.kFBAttachBottom,""),
                    FBAddRegionParam(140,FBAttachType.kFBAttachNone,""), 
                    FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("lRenameNewButton",lRenameNewButton) 
        lRenameNewButton.OnClick.Add(self.RenameClip)  
        
        lrenamebox = FBEditNumber()
        #lrenamebox = FBEdit()
        lrenamebox.Text = "test"
        lrenamebox.Hint = "Rename Selected"
        pMain.AddRegion("lrenamebox","lrenamebox",
                        FBAddRegionParam(4,FBAttachType.kFBAttachRight,"lRenameNewButton"), 
                        FBAddRegionParam(-30,FBAttachType.kFBAttachBottom,""),
                        FBAddRegionParam(100,FBAttachType.kFBAttachNone,""), 
                        FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))  
        
        lrenamebox.Precision = 1
        lrenamebox.Value = 0        
        
        pMain.SetControl("lrenamebox",lrenamebox) 
        
        self.renameControl = lrenamebox

        return    

    def CreateSpreadsheetControl(self, pMain):
        mSpreadView = FBSpread()

        mSpreadView.Caption        = "Shot Track";
        mSpreadView.MultiSelect    = True;
    
        self.CreateHeaders(mSpreadView)
    
        pMain.AddRegion("Spread","Spread",
                        FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""),
                        FBAddRegionParam(40,FBAttachType.kFBAttachTop,"lCreateNewButton"),
                        FBAddRegionParam(-200,FBAttachType.kFBAttachRight,""),
                        FBAddRegionParam(-36,FBAttachType.kFBAttachBottom,""))
        
        #mSpreadView.OnRowClick.Add(self.rs_SelectCamera)
        mSpreadView.OnRowClick.Add(self.SelectShot)
        pMain.SetControl("Spread",mSpreadView) 
    
        return mSpreadView
    
    def CreateListviewControl(self, pMain):
        mLabel2 = FBButton()
        mLabel2.Caption = "Exported Light Names" 
        #mLabel2.Style = FBTextStyle.kFBTextStyleBold
        pMain.AddRegion("mLabel2" ,"mLabel2" ,
                        FBAddRegionParam(-200,FBAttachType.kFBAttachRight,""),
                        FBAddRegionParam(40,FBAttachType.kFBAttachTop,"lCreateNewButton"),
                        FBAddRegionParam(-2,FBAttachType.kFBAttachRight,""), 
                        FBAddRegionParam(22,FBAttachType.kFBAttachNone,"")) 
        pMain.SetControl("mLabel2" ,mLabel2)        

        
        mListView = FBList()
        mListView.Style = FBListStyle.kFBVerticalList
        
        pMain.AddRegion("ListView","ListView",
                        FBAddRegionParam(-200,FBAttachType.kFBAttachRight,""),
                        FBAddRegionParam(62,FBAttachType.kFBAttachTop,"lCreateNewButton"),
                        FBAddRegionParam(-2,FBAttachType.kFBAttachRight,""),
                        FBAddRegionParam(-36,FBAttachType.kFBAttachBottom,"")) 
    
        pMain.SetControl("ListView",mListView) 
        
        return mListView
        
    def PopulateUI(self, pMain):
        lCreateNewButton = FBButton()
        lCreateNewButton.Hint = "RUN EST"
        lCreateNewButton.Caption = "RUN EST"
        lCreateNewButton.Look = FBButtonLook.kFBLookPush
        pMain.AddRegion("lCreateNewButton","lCreateNewButton",
                    FBAddRegionParam(10,FBAttachType.kFBAttachLeft,""), 
                    FBAddRegionParam(40,FBAttachType.kFBAttachTop,"lBannerLabel"),
                    FBAddRegionParam(160,FBAttachType.kFBAttachNone,""), 
                    FBAddRegionParam(25,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("lCreateNewButton",lCreateNewButton) 
        lCreateNewButton.OnClick.Add(self.RunEST) 
        
        
        
        label = FBLabel()        
        label.Caption = "Press To Start EST Processing\n(Please check your Start and End frame ranges are Valid before processing)"
        label.Style = FBTextStyle.kFBTextStyleBold
        label.WordWrap = True
        pMain.AddRegion("label","label",
                           FBAddRegionParam(10,FBAttachType.kFBAttachRight,"lCreateNewButton"), 
                           FBAddRegionParam(39,FBAttachType.kFBAttachTop,"lBannerLabel"),
                           FBAddRegionParam(460,FBAttachType.kFBAttachNone,""), 
                           FBAddRegionParam(29,FBAttachType.kFBAttachNone,""))
        pMain.SetControl("label",label)
        self.label = label
        
    def Create( self, lEstUI ):
        self.PopulateUI(lEstUI) # main tool
        self.pListView = self.CreateSpreadsheetControl(lEstUI) # SpreadsheetVIewer 
        self.pLightView = self.CreateListviewControl(lEstUI) # Ligth ListView
        self.CreateEstRenameControls(lEstUI)
        # 1st time run methods
        self.pListView.Clear()
        self.CreateHeaders(self.pListView)
        core.Est_edit.utils.getESTNames()
        core.Est_edit.utils.getLightNames()  
        self.pLightView.Items.removeAll()
        for lightname in core.Est_edit.utils.GetCurrentLightCameraNames:
            self.pLightView.Items.append(str(lightname))        
        
        self.RefreshView(0,1)
    
def Run( show = True ):
    tool = EstEditTool()
    
    if show:
        tool.Show()
        
    return tool