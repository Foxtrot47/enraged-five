'''
RS.Tools.UI

Base class module for building GUI's.

Author:

    Jason Hayes <jason.hayes@rockstarsandiego.com>

Examples:

    # Native MotionBuilder tool.
    import RS.Tools.UI
    
    class MyTool( RS.Tools.UI.Base ):
        def __init__( self ):
            RS.Tools.UI.Base.__init__( self, 'My Awesome Tool' )
            
    
    tool = MyTool()
    tool.Show( True )  # Set to True to have the previous tool destroyed.
    
    

    # PyQt-based MotionBuilder tool.  Requires you to subclass our QtBaseWidget class.  This version
    # embeds a QtWidget inside of an FBTool.
    import RS.Tools.UI
    
    # You can compile the .ui module into a .py file by doing this:
    RS.Tools.UI.CompileUi( theUiFilename, thePyFilename )
    
    # Need to import the compiled Qt Designer .ui Python module.
    import RS.Tools.UI.MyQtUiModule
    
    class MyQtWidget( RS.Tools.UI.QtBaseWidget, RS.Tools.UI.MyQtUiModule.Ui_Class ):
        def __init__( self, parent ):
            RS.Tools.UI.QtBaseWidget.__init__( self, parent )
            
        # Define any bound event handlers here that are in the .ui file.
    
    class MyTool( RS.Tools.UI.QtBase ):
        def __init__( self ):
        
            RS.Tools.UI.QtBase.__init__( self, 'My Awesome Tool', 'UiFilename' )
            
    RS.Tools.UI.Show( MyTool )
    
    
    # PyQt-based MotionBuilder tool.  This creates a Qt Dialog, but the frame isn't dockable inside of MotionBuilder.
    import RS.Tools.UI
    
    # Inheriting from the .ui Python module is optional.
    class MyDialog( RS.Tools.UI.QtDialogBase, RS.Tools.UI.MyQtUiModule.Ui_Class ):
        def __init__( self ):
            RS.Tools.UI.QtDialogBase.__init__( self )
            
            # Call the .ui Python modules setup function to create the GUI.
            self.setupUi()
            
        # Define any bound event handlers here that are in the .ui file.
'''
from pyfbsdk import *
from pyfbsdk_additions import *
from PySide import QtCore, QtGui, QtUiTools, shiboken
from functools import wraps

import os
import stat
import sys
import inspect
import pysideuic
import datetime
import webbrowser

import RS.Utils.Widgets
import RS.Utils.Logging
import RS.Config


## Functions ##

def LogEvent( func ):
    '''
    Decorator function for logging events.
    '''
    @wraps( func )
    def wrapper( self, *args, **kwargs ):
        
        # Timestamp when the function was called.
        timestampCalled = datetime.datetime.now()
         
        # Call the wrapped function.
        func( self, *args, **kwargs  )
        
        # Log the called function, and how long it took.
        RS.Utils.Logging.Log( func.__module__, func.__name__, recordType = RS.Utils.Logging.Type.EventHandler, timeStamp = timestampCalled, filename = func.func_code.co_filename, applicationId = RS.Utils.Logging.Application.MotionBuilder )
        
    return wrapper

def CompileUi( uiFilename, pyFilename = None ):
    '''
    Compiles a Qt Designer .ui file into a .py file.
    '''
    
    # If no python filename to compile to is supplied, then set it to the same filename and location as the .ui filename, but obviously
    # with a .py file extension.
    if pyFilename == None:
        pyFilename = '{0}\\{1}.py'.format( os.path.dirname( uiFilename ), os.path.basename( uiFilename ).split( '.' )[ 0 ] ) 
        
    if not os.path.isfile( pyFilename ):
        tempses = open( pyFilename, 'w' )
        tempses.close()

    # See if the python file is writable first.
    pyMod = os.stat( pyFilename )[ 0 ]
    
    if ( pyMod & stat.S_IWRITE ):
        uiFile = open( uiFilename )
        pyFile = open( pyFilename, 'w' )
        
        pysideuic.compileUi( uiFile, pyFile )
        
        uiFile.close()
        pyFile.close()
        
    else:
        sys.stdout.write( '[PySide Compile Warning] "{0}" needs to be writable in order to be compiled!\n'.format( pyFilename ) )

def Show( tool, destroy = True ):
    '''
    Shows a tool.
    '''
    if destroy:
        FBDestroyToolByName( tool.Name )
        
        tool = tool()
        FBAddTool( tool )
        ShowTool( tool )
        
    else:
        tool = FBToolList[ tool.Name ]
        ShowTool( tool )

## Classes ##

class QtDialogBase( QtGui.QDialog ):
    def __init__( self, parent = None ):
        
        # If no parent supplied, set MotionBuilder application window as the parent.
        if parent == None:
            mobuWindow = [ x for x in QtGui.qApp.topLevelWidgets() if str( x.windowTitle() ).lower().startswith( "Motionbuilder {0}".format(RS.Config.Script.TargetBuild.lower()) ) ]
            
            if mobuWindow:
                parent = mobuWindow[ 0 ]
        
        super( QtDialogBase, self ).__init__( parent )
        
class QtMainWindowBase( QtGui.QMainWindow ):
    def __init__( self, parent = None, title = 'Rockstar Tool', size = [ 400, 400 ] ):
        
        # Close previous version of the tool.
        for widget in QtGui.qApp.topLevelWidgets():
            if str( widget.windowTitle() ) == title:
                if widget.isVisible():
                    widget.close()
        
        # If no parent supplied, set MotionBuilder application window as the parent.
        if parent == None:
            mobuWindow = [ widget for widget in QtGui.qApp.topLevelWidgets() if str( widget.windowTitle() ).lower().startswith( "Motionbuilder {0}".format(RS.Config.Script.TargetBuild.lower())) ]
            
            if mobuWindow:
                parent = mobuWindow[ 0 ]
        
        super( QtMainWindowBase, self ).__init__( parent )
        
        self.setWindowTitle( title )
        self.resize( size[ 0 ], size[ 1 ] )

        # Set the window icon.
        windowIcon = QtGui.QIcon()
        windowIcon.addPixmap( QtGui.QPixmap( '{0}\\Rockstar_Games_24x24.png'.format( RS.Config.Script.Path.ToolImages ) ) )
        QtGui.qApp.setWindowIcon( windowIcon )
        
        self.__openedTimeStamp = datetime.datetime.now()
        
        RS.Utils.Logging.Log( self.__module__, RS.Utils.Logging.Context.Opened, filename = inspect.getfile( inspect.currentframe().f_back ), applicationId = RS.Utils.Logging.Application.MotionBuilder )
        
    def closeEvent( self, event ):
        RS.Utils.Logging.Log( self.__module__, RS.Utils.Logging.Context.Closed, timeStamp = self.__openedTimeStamp, filename = inspect.getfile( inspect.currentframe() ), applicationId = RS.Utils.Logging.Application.MotionBuilder )
        
        QtGui.QMainWindow.closeEvent( self, event )
        
class QtBaseWidget( QtGui.QWidget ):
    '''
    Base class for a Qt widget object.  Whatever inherits from this class also needs
    to inherit its .ui Python module.  The setupUi() function will be automatically
    called by this class.
    
    Example usage:
    
    class MyWidget( RS.Tools.UI.QtBaseWidget, RS.Tools.UI.MyQtUiModule ):
        def __init__( self, parent ):
            RS.Tools.UI.QtBaseWidget.__init__( self, parent )
    '''
    def __init__( self, parent ):
        QtGui.QWidget.__init__( self, parent )
        
        self.setupUi( self )
        
class QtBaseWidgetHolder( FBWidgetHolder ):
    '''
    Native Qt widget holder.  This is what allows us to embed a QtWidget into our MotionBuilder interfaces.  This gets
    automatically used by the QtBase class.
    '''
    def __init__( self, widget, *args, **kwargs ):
        FBWidgetHolder.__init__( self, *args, **kwargs )
        
        self.__widget = widget
        
    def WidgetCreate( self, parent ):
        self.__nativeQtWidget = self.__widget( shiboken.wrapInstance( parent, QtGui.QWidget ) )
        return shiboken.getCppPointer( self.__nativeQtWidget )[ 0 ]

class QtBase( FBTool ):
    '''
    Base class for a PyQt-based tool.
    
    Arguments:
    
        name: The name of the tool.
        widget: The QtBaseWidget subclass.
                        
    Keyword Arguments:
    
        size: The starting size of the tool.
    '''
    Name = 'Rockstar - Default Tool'
    
    def __init__( self, name, widget, size = [ 400, 400 ] ):
        FBTool.__init__( self, name )
        
        self.__qtWidgetHolder = QtBaseWidgetHolder( widget )
   
        x = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( 0, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( 0, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( 0, FBAttachType.kFBAttachBottom, '' )
        
        self.AddRegion( 'main', 'main', x, y, w, h )
        self.SetControl( 'main', self.__qtWidgetHolder )
        
        self.StartSizeX = size[ 0 ]
        self.StartSizeY = size[ 1 ]
         
class Base( object ):
    '''
    Base class for tools GUI.  The MotionBuilder SDK recommends to not use FBTool directly, but to instead use
    CreateUniqueTool and CreateTool to generate tools.
    '''
    def __init__( self, 
                  name, 
                  size = [ 200, 200 ],
                  minSize = [ 10, 10 ],
                  pos = [ 0, 0 ], 
                  createBanner = True, 
                  emailTo = 'RSG MotionBuilder SDK Support', 
                  emailCC = None, 
                  helpUrl = None ):
        
        # Tool properties.
        self.__size = size
        self.__minSize = minSize
        self.__toolName = name
        self.__tool = None
        
        # Center tool in the screen.
        # TODO: Looks like FBSystem().DesktopSize is broken in 2014.  WTF.
        screenWidth, screenHeight, huh = FBSystem().DesktopSize.GetList()
                
        if pos == [ 0, 0 ]:
            
            # Set to some default location until the desktop size bug can be looked into.
            self.__pos = [ 400, 400 ]
            #self.__pos = [ int( ( screenWidth / 2.0 ) - ( self.__size[ 0 ] / 2.0 ) ) , int( ( screenHeight / 2.0 ) - ( self.__size[ 1 ] / 2.0 ) ) ]     
            
        else:
            self.__pos = pos
        
        # Banner properties.
        self.__banner = None
        self.__createBanner = createBanner
        self.__emailTo = emailTo
        self.__emailCC = emailCC
        self.__helpUrl = helpUrl
    
    ## Public Properties ##
    
    @property
    def OnUnbind( self ):
        '''
        Pass the OnUnbind event handler.
        '''
        return self.__tool.OnUnbind

    @property
    def Name( self ):
        return self.__toolName
    
    @property
    def MainLayout( self ):
        return self.__tool
    
    @property
    def Size( self ):
        return self.__size
    
    @property
    def BannerHeight( self ):
        return 0 if not self.__banner else self.__banner.Height
    
    @property
    def Tool( self ):
        return self.__tool
        

    ## Private Methods ##
    
    def _CreateUniqueTool( self ):
        self.__tool = FBCreateUniqueTool( self.__toolName )
        self.__tool.OnShow.Add( self.__OnShow )
                
        # Start size.
        self.__tool.StartSizeX = self.__size[ 0 ]
        self.__tool.StartSizeY = self.__size[ 1 ]
        
        # Minimum size.
        self.__tool.MinSizeX = self.__minSize[ 0 ]
        self.__tool.MinSizeY = self.__minSize[ 1 ]
        
        # Start position.
        self.__tool.StartPosX = self.__pos[ 0 ]
        self.__tool.StartPosY = self.__pos[ 1 ]
        
        # Create banner.
        if self.__createBanner:
            self.__banner = RS.Utils.Widgets.Banner( self.__tool, self.__toolName, self.__emailTo, inspect.getfile( inspect.currentframe().f_back ), self.__emailCC, self.__helpUrl )       
            self.__banner.Create()
        
        self.Create( self.__tool )        
    
    def __CreateTool( self ):
        self._CreateUniqueTool()

        ShowTool( self.__tool )
        
    def __OnShow( self, source, event ):
        if not event.Shown:
            
            # For some reason, when a tool is closed, the OnShow event handler is called twice.  But we don't
            # want to log the close twice, so we track it via a ref counter.
            if self.__closedRefCount == 0:
                RS.Utils.Logging.Log( self.__module__, RS.Utils.Logging.Context.Closed, timeStamp = self.__openedTimeStamp, filename = inspect.getfile( inspect.currentframe() ), applicationId = RS.Utils.Logging.Application.MotionBuilder )
                
                self.__closedRefCount += 1
            
        else:
            self.__closedRefCount = 0
            self.__openedTimeStamp = datetime.datetime.now()
            

    ## Public Methods ##
    
    def FileNewCompleted( self, state ):
        self.__tool.FileNewCompleted( state )
    
    def Create( self, mainLayout ):
        raise NotImplementedError, 'You must implement this method!'
    
    def Show( self, destroy = True ):
        if destroy:
            FBDestroyToolByName( self.__toolName )
            self.__CreateTool()

        else:
            if self.__toolName in FBToolList:
                ShowToolByName( self.__toolName )
                
            else:
                self.__CreateTool()
                
class BannerWidget( QtGui.QWidget ):
    def __init__( self,
                  name,
                  helpUrl = 'https://rsgediwiki1/bob/index.php/Main_Page',
                  emailTo = 'RSG MotionBuilder SDK Support',
                  emailCC = 'mike.blanchette@rockstarnewengland.com',
                  ):
        
        QtGui.QWidget.__init__(self)
        
        self.__toolName = name
        
        # Banner properties.
        self.__emailTo = emailTo
        self.__emailCC = emailCC
        self.__helpUrl = helpUrl        

        header = QtGui.QHBoxLayout()
        #print dir(self.__header)        
        header.setAlignment( QtCore.Qt.AlignTop ) #Align the whole Layout at the top.
        header.setSpacing(0)
        header.setContentsMargins(0,0,0,0)           
        #self.setLayout( header )        
                
        ## Logo ##
        logo = QtGui.QLabel()
        logo.setPixmap(QtGui.QPixmap("{0}\\header_YellowRSLogo_30x30.png".format( RS.Config.Script.Path.ToolImages )))  
        logo.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed) # Do not change size
        logo.setAlignment( QtCore.Qt.AlignLeft ) #Align the logo to the left
        header.addWidget( logo )
        
        ## Bannner Filler ##
        banner = QtGui.QLabel()
        banner.setPixmap(QtGui.QPixmap("{0}\\header_YellowSpacerBanner_3591x30".format( RS.Config.Script.Path.ToolImages )))  
        banner.setSizePolicy(QtGui.QSizePolicy.Ignored, QtGui.QSizePolicy.Fixed)  
        banner.resize(100, 30)
        header.addWidget( banner )          
        
        ## Email Link ##        
        emailIcon = QtGui.QIcon()
        emailIcon.addPixmap( QtGui.QPixmap(QtGui.QPixmap("{0}\\header_YellowEmailIcon_35x30.png".format( RS.Config.Script.Path.ToolImages ))), QtGui.QIcon.Normal, QtGui.QIcon.Off )        
        email = QtGui.QPushButton(self)
        email.resize( 35, 30 )
        email.setToolTip("Email Tech Art.")  
        email.setFlat(True)
        email.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed) # Do not change size        
        email.setIcon(emailIcon)
        email.setIconSize( QtCore.QSize( 35, 30 ) )
        email.setMaximumWidth( 35 )   
        email.setMaximumHeight( 30 )           
        header.addWidget( email )
        email.pressed.connect( self.OnEmail_Clicked )  # Needs to called after addWidget call             
        
        ## Info Link ##
        infoIcon = QtGui.QIcon()
        infoIcon.addPixmap( QtGui.QPixmap(QtGui.QPixmap("{0}\\header_YellowInfoIcon_35x30.png".format( RS.Config.Script.Path.ToolImages ))), QtGui.QIcon.Normal, QtGui.QIcon.Off )        
        info = QtGui.QPushButton(self)
        info.resize( 35, 30 )
        info.setToolTip("Link to tool documentation.")  
        info.setFlat(True)
        info.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed) # Do not change size        
        info.setIcon(infoIcon)
        info.setIconSize( QtCore.QSize( 35, 30 ) )
        info.setMaximumWidth( 35 ) 
        info.setMaximumHeight( 30 )          
        header.addWidget( info )        
        info.pressed.connect( self.OnInfo_Clicked )
                

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0,0,0,0) 
        mainLayout.addLayout(header)
        #mainLayout.addLayout(body)
        self.setLayout(mainLayout)  
        
    def OnEmail_Clicked( self ):
        os.startfile("mailto: " + self.__emailTo + "&Subject= " +  self.__toolName)
                  
    def OnInfo_Clicked( self):
        webbrowser.open(self.__helpUrl) 
