class Parts(object):
    """
    Body parts
    """
    # Main body part constants
    ALL = "All"
    HIPS = "Hips"
    SPINE = "Spine"
    HEAD = "Head"
    FINGERS = "Fingers"
    ARM = "Arm"
    LEG = "Leg"
    HAND = "Hand"

    _keywordFilters = {
        HIPS: ["Hips"],
        SPINE: ["Spine", "Waist", "Chest"],
        HEAD: ["Head", "Neck"],
        FINGERS: ["Finger", "Thumb", "Index", "Middle", "Ring", "Pinky", "Middle", "InHand"],
        ARM: ["Shoulder", "Arm", "Elbow", "Wrist"],
        LEG: ["Leg", "Toe", "Ankle", "Foot", "Knee", "HipEffector"]
    }

    @classmethod
    def getFilters(cls, name):
        """
        Returns:
            list: List of body parts based on the main body part
        """
        return cls._keywordFilters.get(name)


class Sides(object):
    """
    Sides
    """
    LEFT = "Left"
    RIGHT = "Right"


class KeyMode(object):
    """
    Key modes
    """
    FULL = "Full"
    PART = "Part"
    SELECTION = "Selection"


class Values(object):
    """
    Spinbox/Slider Values
    """
    # Setting default values to 100 because set to 100, the in-between value will equal the keyframe it precedes
    DEFAULT_MIN = -100
    DEFAULT_MAX = 100
