'''

 Script Path: RS/Tools/UI/Animation/SelectsFrameSetToolbox.py

 Written And Maintained By: Kathryn Bodey

 Created: 03.06.2014

 Description: 

'''


from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Config
import RS.Tools.UI
import RS.Core.Animation.SelectsFrameSet as core

reload( core )


class SelectsFrameSetToolbox( RS.Tools.UI.Base ):
    def __init__( self ):
        
        RS.Tools.UI.Base.__init__( self, 'SelectsFrameSet Toolbox', size = [ 290, 425 ] )
         
        self.frameStartEdit = None
        self.frameEndEdit = None 
        self.spreadSheet = None
        self.resetSoftRangeButton = None
        self.RangeList = []
        self.PropertyLimit = 10
        self.SelectFrameRangeNull = None


    def RefreshCallback( self, control, event ): 
        self.RangeList = []
        self.Show( True )
    

    def GenerateRangeList( self ):

        # check the range null exists    
        for i in RS.Globals.gComponents:
            hudProperty = i.PropertyList.Find( 'Frame Selects' )
            
            # get property range list
            if hudProperty:        
                for properties in i.PropertyList:
                    if properties.GetPropertyTypeName() == 'charptr':
                        if 'Select Range' in properties.Name:
                            self.SelectFrameRangeNull = i
                            self.RangeList.append( properties )


    def PopulateSpread( self ):

        self.GenerateRangeList()

        if self.SelectFrameRangeNull:
        
            if len( self.RangeList ) > 0:
    
                # add entries into spreadsheet
                row = 0 
                    
                for i in range( len( self.RangeList ) ):
    
                    propertyData = self.RangeList[i].Data
                    
                    splitName = propertyData.split( '-' )
                    startFrame = splitName[0]
                    endFrame = splitName[-1]
                    
                    self.spreadSheet.RowAdd( propertyData, row )
                    self.spreadSheet.Justify = FBTextJustify.kFBTextJustifyRight 
                    
                    startFrameCell = self.spreadSheet.GetSpreadCell( row, 0 )
                    startFrameCell.Style = FBCellStyle.kFBCellStyleButton
                    self.spreadSheet.SetCellValue( row, 0, str( startFrame ) )    
        
                    endFrameCell = self.spreadSheet.GetSpreadCell( row, 1 )
                    endFrameCell.Style = FBCellStyle.kFBCellStyleButton
                    self.spreadSheet.SetCellValue( row, 1, str( endFrame ) )
                    
                    softRangeCell = self.spreadSheet.GetSpreadCell( row, 2 )
                    softRangeCell.Style = FBCellStyle.kFBCellStyleButton
                    self.spreadSheet.SetCellValue( row, 2, str( propertyData ) )
        
                    deleteCell = self.spreadSheet.GetSpreadCell( row, 3 )
                    deleteCell.Style = FBCellStyle.kFBCellStyleButton
                    self.spreadSheet.SetCellValue( row, 3, str( 'X' ) )
                                    
                    row += 1
            
            # recreate constraint if 'SelectFrameRangeNull' exists
            core.CreateSelectsRangesConstraint( self.SelectFrameRangeNull )

    
    def CurrentStartFrame( self, control, event ):
        
        self.frameStartEdit.Value = FBSystem().LocalTime.GetFrame()
        
        
        
    def CurrentEndFrame( self, control, event ):
        
        self.frameEndEdit.Value = FBSystem().LocalTime.GetFrame()
                
        
    def AddFrames( self, control, event ):
                
        if self.frameStartEdit.Value < self.frameEndEdit.Value:
            
            # check the limit hasnt been hit on number of properties    
            if len( self.RangeList ) == self.PropertyLimit:
                FBMessageBox( 'R* Error', 'You have hit the Property limit: [{0}]\nIf you need more ranges speak to Tech Art to update the scripts,\nor delete existing ranges fromt he list if no longer needed.'.format( self.PropertyLimit ), 'Ok' )
            
            # check if selects_frame_ranges null exists
            nullExists = False            
            
            for i in RS.Globals.gComponents:
                hudProperty = i.PropertyList.Find( 'Frame Selects' )
                
                if hudProperty:
                    nullExists = True
                    self.SelectFrameRangeNull = i
                    
            if nullExists == True:        
                # add new property range to existing null
                propertyData = core.AddFrameProperty( self.SelectFrameRangeNull, self.frameStartEdit.Value, self.frameEndEdit.Value )        
                
            else:
                # create the selects_frame_ranges null and add initial property
                self.SelectFrameRangeNull = core.CreateSelectsRangesNull()
                propertyData = core.AddFrameProperty( self.SelectFrameRangeNull, self.frameStartEdit.Value, self.frameEndEdit.Value )
                
            # reset rangelist & reload UI to regenerate the frame list
            self.RangeList = []
            self.Show( True )
        
        else:
            
            FBMessageBox( 'R* Error', 'Start frame is less than or equal to the End frame.\n\nThis is not a valid frame range.', 'Ok' )


    def SpreadsheetButtonCallback( self, control, event ):
        
        playerControl = FBPlayerControl()        
        
        cell = control.GetRow( event.Row )
        cellName = cell.Caption        
        
        splitName = cellName.split( '-' )
        start = int( splitName[0] )
        end = int( splitName[-1] )
        
        startFrame = FBTime( 0,0,0, int( start ) )
        endFrame = FBTime( 0,0,0, int( end ) )
        
        # go to start range
        if event.Column == 0:
            playerControl.Goto( startFrame )
            
        # go to end range
        if event.Column == 1: 
            playerControl.Goto( endFrame )
        
        # set soft range
        if event.Column == 2: 
            startTime = startFrame.Get() 
            endTime = endFrame.Get() 
            playerControl.ZoomWindowStart = FBTime( startTime )
            playerControl.ZoomWindowStop = FBTime( endTime )
            
            playerControl.Goto( startFrame )
        
        # delete property
        if event.Column == 3:
            cell = control.GetRow( event.Row )
            propertyDelete = None        
            
            for i in self.RangeList:
                if i.Data == cellName:
                    propertyDelete = i
                    self.RangeList.remove( i )
                    
            if propertyDelete:
                try:
                    self.SelectFrameRangeNull.PropertyRemove( propertyDelete )
                    cell.Remove()
                    
                    # reset rangelist & reload UI to regenerate the frame list
                    self.RangeList = []
                    self.Show( True )
                    
                except:
                    pass


    def ResetSoftRanges( self, control, event ):
        
        playerControl = FBPlayerControl()
        start = FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame() 
        end = FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame() 
        startTime = FBTime( 0,0,0, int( start ) )
        endTime = FBTime( 0,0,0, int( end ) )
        
        startTime = startTime.Get() 
        endTime = endTime.Get() 
        playerControl.ZoomWindowStart = FBTime( startTime )
        playerControl.ZoomWindowStop = FBTime( endTime )        


    def Create( self, mainLayout ):

        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachBottom,"" )
        w = FBAddRegionParam( 0,FBAttachType.kFBAttachRight,"" )
        h = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"" )
        mainLayout.AddRegion( "mainLayout", "mainLayout", x, y, w, h ) 
                          
        addButton = FBButton()
        addButton.Caption = 'add'
        addButton.Hint = 'adds the range to the list'
        X = FBAddRegionParam( 10, FBAttachType.kFBAttachRight, "self.frameEndEdit" )
        Y = FBAddRegionParam( 10, FBAttachType.kFBAttachBottom, "editLabel" )
        W = FBAddRegionParam( 30, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 20, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "addButton", "addButton", X, Y, W, H )
        mainLayout.SetControl( "addButton", addButton ) 
        
        refreshButton = FBButton()
        refreshButton.Caption = 'Refresh'
        refreshButton.Hint = 'Reloads the UI'
        X = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, "" )
        Y = FBAddRegionParam( 30, FBAttachType.kFBAttachTop, "" )
        W = FBAddRegionParam( 275, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "refreshButton", "refreshButton", X, Y, W, H )
        mainLayout.SetControl( "refreshButton", refreshButton )            
        
        editLabel = FBLabel()
        editLabel.Caption = 'Input a start and end frame:'
        X = FBAddRegionParam( 6, FBAttachType.kFBAttachLeft, "" )
        Y = FBAddRegionParam( 15, FBAttachType.kFBAttachBottom, "refreshButton" )
        W = FBAddRegionParam( 275, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "editLabel", "editLabel", X, Y, W, H )
        mainLayout.SetControl( "editLabel", editLabel ) 
        
        self.frameStartEdit = FBEditNumber()
        self.frameStartEdit.Hint = 'Frame Start'
        self.frameStartEdit.Value = 0
        self.frameStartEdit.Precision = 1
        X = FBAddRegionParam( 5, FBAttachType.kFBAttachLeft, "" )
        Y = FBAddRegionParam( 10, FBAttachType.kFBAttachBottom, 'editLabel' )
        W = FBAddRegionParam( 80, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 20, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "self.frameStartEdit", "self.frameStartEdit", X, Y, W, H )
        mainLayout.SetControl( "self.frameStartEdit", self.frameStartEdit )                  
        
        startCurrentFrameButton = FBButton()
        startCurrentFrameButton.Caption = 'current frame'
        startCurrentFrameButton.Hint = 'current frame as start range'
        X = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, "" )
        Y = FBAddRegionParam( 5, FBAttachType.kFBAttachBottom, "self.frameStartEdit" )
        W = FBAddRegionParam( 70, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 10, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "startCurrentFrameButton", "startCurrentFrameButton", X, Y, W, H )
        mainLayout.SetControl( "startCurrentFrameButton", startCurrentFrameButton )  
        
        self.frameEndEdit = FBEditNumber()
        self.frameEndEdit.Hint = 'Frame End'
        self.frameEndEdit.Value = 0
        self.frameEndEdit.Precision = 1
        X = FBAddRegionParam( 10, FBAttachType.kFBAttachRight, 'self.frameStartEdit' )
        Y = FBAddRegionParam( 10, FBAttachType.kFBAttachBottom, 'editLabel' )
        W = FBAddRegionParam( 80, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 20, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "self.frameEndEdit", "self.frameEndEdit", X, Y, W, H )
        mainLayout.SetControl( "self.frameEndEdit", self.frameEndEdit ) 
        
        endCurrentFrameButton = FBButton()
        endCurrentFrameButton.Caption = 'current frame'
        endCurrentFrameButton.Hint = 'current frame as end range'
        X = FBAddRegionParam( 20, FBAttachType.kFBAttachRight, "startCurrentFrameButton" )
        Y = FBAddRegionParam( 5, FBAttachType.kFBAttachBottom, "self.frameEndEdit" )
        W = FBAddRegionParam( 70, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 10, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "endCurrentFrameButton", "endCurrentFrameButton", X, Y, W, H )
        mainLayout.SetControl( "endCurrentFrameButton", endCurrentFrameButton )          
        
        self.spreadSheet = FBSpread()
        self.spreadSheet.Caption = "Frame Start - End"
        self.spreadSheet.ColumnAdd( 'Go to Start' )    
        self.spreadSheet.ColumnAdd( 'Go to End' )    
        self.spreadSheet.ColumnAdd( 'Set as Soft Range' )
        self.spreadSheet.ColumnAdd( 'Delete' )

        self.spreadSheet.GetColumn( -1 ).Width = 0
        self.spreadSheet.GetColumn( 0 ).Width = 57
        self.spreadSheet.GetColumn( 1 ).Width = 57
        self.spreadSheet.GetColumn( 2 ).Width = 100
        self.spreadSheet.GetColumn( 3 ).Width = 50
        
        X = FBAddRegionParam( -5,FBAttachType.kFBAttachLeft,"" )
        Y = FBAddRegionParam( 30,FBAttachType.kFBAttachBottom,"self.frameEndEdit" )
        W = FBAddRegionParam( 297,FBAttachType.kFBAttachNone,"" )
        H = FBAddRegionParam( 233,FBAttachType.kFBAttachNone,"" )   
        mainLayout.AddRegion( "self.spreadSheet","self.spreadSheet", X, Y, W, H )
        mainLayout.SetControl( "self.spreadSheet", self.spreadSheet ) 
        
        self.PopulateSpread()

        self.resetSoftRangeButton = FBButton()
        self.resetSoftRangeButton.Caption = 'Reset Soft Range'
        self.resetSoftRangeButton.Hint = 'Resets soft range to go back to the hard range'
        X = FBAddRegionParam( 122, FBAttachType.kFBAttachLeft, "" )
        Y = FBAddRegionParam( 2, FBAttachType.kFBAttachBottom, "self.spreadSheet" )
        W = FBAddRegionParam( 100, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 20, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "self.resetSoftRangeButton", "self.resetSoftRangeButton", X, Y, W, H )
        mainLayout.SetControl( "self.resetSoftRangeButton", self.resetSoftRangeButton ) 


        refreshButton.OnClick.Add( self.RefreshCallback )
        startCurrentFrameButton.OnClick.Add( self.CurrentStartFrame )
        endCurrentFrameButton.OnClick.Add( self.CurrentEndFrame )
        addButton.OnClick.Add( self.AddFrames )
        self.spreadSheet.OnCellChange.Add( self.SpreadsheetButtonCallback )
        self.resetSoftRangeButton.OnClick.Add( self.ResetSoftRanges )
        
    
def Run( show = True ):
    tool = SelectsFrameSetToolbox()

    if show:
        tool.Show()

    return tool