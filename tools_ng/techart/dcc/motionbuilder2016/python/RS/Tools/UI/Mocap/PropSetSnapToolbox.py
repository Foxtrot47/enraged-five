from pyfbsdk import *
from pyfbsdk_additions import *

from PySide import QtGui
from PySide import QtCore

import RS.Config
import RS.Core.Mocap.MocapCommands as mocapCommand

RS.Tools.UI.CompileUi( '{0}\\RS\\Tools\\UI\\Mocap\\Qt\\PropSetSnap.ui'.format( RS.Config.Script.Path.Root ), '{0}\\RS\\Tools\\UI\\Mocap\\Qt\\PropSetSnap_compiled.py'.format( RS.Config.Script.Path.Root ) )

import RS.Tools.UI.Mocap.Qt.PropSetSnap_compiled as PropSetSnapQT

reload( PropSetSnapQT )


class PropSetSnapToolbox(  RS.Tools.UI.QtMainWindowBase, PropSetSnapQT.Ui_MainWindow ):
	def __init__(self, parent = None):

		RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = 'Prop Set Snap' )

		self.ButtonGroupDict = {}

		# build designer complied UI
		self.setupUi( self )
		
		# refresh
		self.pushButtonRefresh.pressed.connect( self.Refresh )

		# banner 
		_banner = RS.Tools.UI.BannerWidget(name = 'Range Select Toolbox', helpUrl ="")
		self.lytBannerWidget.addWidget (_banner )		

		# scroll area
		self.scrollArea = QtGui.QScrollArea(self.centralwidget)
		self.scrollArea.setGeometry(QtCore.QRect(0, 90, 921, 161))
		self.scrollArea.setWidgetResizable(False)
		self.scrollAreaWidgetContents = QtGui.QWidget()
		self.scrollArea.setWidget(self.scrollAreaWidgetContents)		

		# scroll area populate
		propDictionary = mocapCommand.PropSetDictionarySnap()

		scrollSize = 159
		dictLength = len( list( propDictionary.keys() ) )
		if dictLength > 5:
			excessList = dictLength - 5
			scrollSize = scrollSize + ( excessList * 20 ) 				
		self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 899, scrollSize))

		label = 12
		combo = 9
		button = 8			
		for key, value in propDictionary.iteritems():
			namespace = key
			propName = value[0]
			mainControl = value[1]
			setList = value[2]
			if len( setList ) > 1:

				self.label = QtGui.QLabel(self.scrollAreaWidgetContents)
				self.label.setGeometry(QtCore.QRect(10, label, 321, 16))
				self.label.setText( mainControl.LongName )

				self.comboBox = QtGui.QComboBox(self.scrollAreaWidgetContents)
				self.comboBox.setGeometry(QtCore.QRect(350, combo, 451, 22))
				for i in setList:
					self.comboBox.addItem( i.LongName )

				self.pushButton = QtGui.QPushButton(self.scrollAreaWidgetContents)
				self.pushButton.setGeometry(QtCore.QRect(810, button, 75, 23))
				self.pushButton.setText( "Snap" )

				self.ButtonGroupDict[ self.pushButton ] = ( self.comboBox, self.label )

				label = label + 30
				combo = combo + 30
				button = button + 30

		for key, value in self.ButtonGroupDict.iteritems():
			key.pressed.connect( self.PropSnapCallback )


	def PropSnapCallback( self ):
		sceneSetObject = None
		sceneProp = None

		for key, value in self.ButtonGroupDict.iteritems():
			snapButton = key
			comboBox = value[0]
			label = value[1]

			if self.sender() == snapButton:
				sceneSetObject = RS.Utils.Scene.FindModelByName( str( comboBox.currentText() ), includeNamespace=True )
				sceneProp = RS.Utils.Scene.FindModelByName( str( label.text() ), includeNamespace=True )

		if sceneSetObject and sceneProp:
			RS.Utils.Scene.Align( sceneProp, sceneSetObject )


	def Refresh( self ):
		self.close()
		Run()	


def Run():   

	form = PropSetSnapToolbox()
	form.show()  