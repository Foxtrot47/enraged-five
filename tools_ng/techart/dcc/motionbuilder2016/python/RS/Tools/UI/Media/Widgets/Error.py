"""
Phonon sometimes fails to load due to a an issue when switching between projects, so this widget replaces the video
player widget and only shows images
"""
import os

from PySide import QtGui

from RS import Config


class Error(QtGui.QLabel):
    """
    Class for when a phonon dependent class fails to import, this replaces it.
    """
    error = ""

    def __init__(self, error="", parent=None, **kwargs):
        """
        Constructor

        Arguments:
            error (string): error message
            parent (QtGui.QWidget): parent widget
            **kwargs (dictionary): grabs extra keywords arguments from the class that it is suppose to be replacing
        """
        super(Error, self).__init__(parent=parent)
        self._path = os.path.join(Config.Script.Path.ToolImages, "VideoPlayer", "error.png")
        self.setText(("<html>"
              "<center>"
              "<img src='{}'>"
              "<p><b>Error : {}</b></p>"
              "<center>"
              "</html>").format(self._path, self.error))

    @property
    def path(self):
        """ returns the path for the error video """
        return self._path

    @path.setter
    def path(self, path):
        """
        This method does nothing. This image class is meant to replace the Video Player when Phonon fails to
        load and it will need to have some of the same methods even if they do nothing to avoid breaking
        existing code
        """
        pass

    def play(self):
        """
        This method does nothing. This image class is meant to replace the Video Player when Phonon fails to
        load and it will need to have some of the same methods even if they do nothing to avoid breaking
        existing code
        """

    def stop(self):
        """
        This method does nothing. This image class is meant to replace the Video Player when Phonon fails to
        load and it will need to have some of the same methods even if they do nothing to avoid breaking
        existing code
        """

    def pause(self):
        """
        This method does nothing. This image class is meant to replace the Video Player when Phonon fails to
        load and it will need to have some of the same methods even if they do nothing to avoid breaking
        existing code
        """

    def setCodecErrorEnabled(self, enabled):
        """
        This method does nothing. This image class is meant to replace the Video Player when Phonon fails to
        load and it will need to have some of the same methods even if they do nothing to avoid breaking
        existing code
        """

    def goToFrame(self, frame):
        """
        This method does nothing. This image class is meant to replace the Video Player when Phonon fails to
        load and it will need to have some of the same methods even if they do nothing to avoid breaking
        existing code
        """

    def loop(self):
        """
        This method does nothing. This image class is meant to replace the Video Player when Phonon fails to
        load and it will need to have some of the same methods even if they do nothing to avoid breaking
        existing code
        """

    def setLoop(self, loop):
        """
        This method does nothing. This image class is meant to replace the Video Player when Phonon fails to
        load and it will need to have some of the same methods even if they do nothing to avoid breaking
        existing code
        """


