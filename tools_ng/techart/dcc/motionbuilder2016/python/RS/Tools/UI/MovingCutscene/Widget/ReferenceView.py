import pyfbsdk as mobu

from PySide import QtGui

from RS.Core.Scene.widgets import ComponentView
from RS.Tools.UI.MovingCutscene.Model import ReferenceModel


class ReferenceView(ComponentView.ComponentView):
    """
    Shows a filtered view of the refereneces in the scene if they have valid FBModel that can be attached to a moving
    cutscene
    """
    def __init__(self, referenceType, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent
        """
        self._referenceType = referenceType
        # Avoid calling the init for the inherited class as we are changing how it is setup
        super(ReferenceView, self).__init__(componentType=mobu.FBModelNull, parent=parent)


    def _createModel(self):
        """ Creates the model that is used for this view at initialization """
        return ReferenceModel.ReferenceModel(referenceType=self._referenceType)