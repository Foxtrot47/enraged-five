from PySide import QtCore

from RS.Utils.Scene import Character
from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem

REFERENCE_ROLE = QtCore.Qt.UserRole + 1


class CharacterTextModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    def __init__(self, referenceObject, gender=None, refColorString=None, parent=None):
        super(CharacterTextModelItem, self).__init__([], parent=parent)
        self._referenceObject = referenceObject
        self._hidden = Character.CharacterHidden(referenceObject.GetMobuCharacter())
        self._gender = gender or "Male"
        self._colorComboBoxString = refColorString or "Black"
        self._characterName = "{}:{}".format(referenceObject.Namespace, referenceObject.Name)

    def getCharacterNamespace(self):
        '''
        returns character namespace for item
        '''
        return self._characterName

    def getCharacterObject(self):
        '''
        returns character node for item
        '''
        return self._referenceObject

    def getGender(self):
        '''
        returns gender str for item
        '''
        return self._gender

    def columnCount(self):
        '''
        returns the number of columns in view
        '''
        return self._columnNumber

    def getHidden(self):
        """
        returns hidden boolean
        """
        return self._hidden

    def getUserColor(self):
        '''
        returns current color string
        '''
        return str(self._colorComboBoxString)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()

        if role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
            if column == 0:
                return self._characterName
            elif column == 1:
                return self._hidden
            elif column == 2:
                return self._gender
            elif column == 3:
                return self._colorComboBoxString

        elif role == REFERENCE_ROLE:
            return self._referenceObject

        elif role == QtCore.Qt.UserRole:
            return self

        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        model data updated based off of user interaction with the view
        '''
        column = index.column()
        if role == QtCore.Qt.EditRole:
            if column == 2:
                self._gender = value
                return True
            elif column == 3:
                self._colorComboBoxString = value
                return True

        return None
