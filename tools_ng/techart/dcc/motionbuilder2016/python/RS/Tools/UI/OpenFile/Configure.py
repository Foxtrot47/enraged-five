"""
Setups Motion Builder to launch the custom file open dialog
"""
from functools import partial
import pyfbsdk as mobu

from PySide import QtGui

from RS import Config
from RS.Tools.UI import Application
from RS.Utils import UserPreferences

from RS.Tools.UI.OpenFile.Widgets import Dialog

OPEN_FILE_TEXT = "&Open..."
ROCKSTAR_OPEN_TEXT = "R* Open..."
HOTKEY = "Ctrl+O"


def Setup():
    """ 
    Graphically replaces the command launched by the Open menu item in the main Motion Builder menu bar with our own
    Keep the original one there under a different name and hidden so we can still call it
    """
    if "R*" in Config.User.Studio:

        setHotkey = UserPreferences.__Readini__("menu", "Enable Custom Open File Dialog", default_value=False)

        # Get the window and its action menus
        actions = Application.GetMainMenuActions()

        # Find the original action
        openAction = None
        rockstarAction = None

        for title, value in actions["File"].iteritems():
            if title.startswith(OPEN_FILE_TEXT):
                openAction = value["__QAction"]
            elif title.startswith(ROCKSTAR_OPEN_TEXT):
                rockstarAction = value["__QAction"]

        # Rename the orginal action to ensure name is consistent

        # New action, give it the same name as the original one and connect it to the handler
        if not rockstarAction:
            rockstarAction = QtGui.QAction(None)
            rockstarAction.setText(ROCKSTAR_OPEN_TEXT)
            rockstarAction.triggered.connect(Dialog.Run)
            actions["File"]["__QAction"].menu().insertAction(openAction, rockstarAction)
            actions["File"]["__QAction"].menu().insertAction(rockstarAction, openAction)

        if setHotkey:
            Application.AddShortCut(HOTKEY, Dialog.Run)
            openAction.setText(OPEN_FILE_TEXT)
            rockstarAction.setText("{}\t{}".format(ROCKSTAR_OPEN_TEXT, HOTKEY))

        else:
            Application.RemoveShortCut(HOTKEY)
            rockstarAction.setText(ROCKSTAR_OPEN_TEXT)
            openAction.setText("{}\t{}".format(OPEN_FILE_TEXT, HOTKEY))

        # Add callback to store file names when any file is opened or saved successfully

        # mobu.FBApplication().OnFileOpenCompleted.Add(Dialog.Dialog.StoreCallback)
        # mobu.FBApplication().OnFileSaveCompleted.Add(Dialog.Dialog.StoreCallback)


def Run(force=True):
    """
    Launches our custom open file window dialog if it is enabled otherwise launch the original
    file open dialog using the original open file action

    """
    useCustomDialog = UserPreferences.__Readini__("menu", "Enable Custom Open File Dialog", default_value=False)

    if useCustomDialog or force:
        Dialog.Run()

    else:
        actions = Application.GetMainMenuActions()
        fileMenu = actions.get("File", None)
        if fileMenu:
            fileMenu[OPEN_FILE_TEXT]["__QAction"].trigger()
