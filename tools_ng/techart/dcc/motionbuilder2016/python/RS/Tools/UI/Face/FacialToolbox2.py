from pyfbsdk import *
from pythonidelib import *
from functools import partial

from RS import Globals
from RS.Utils import Scene

from PySide import QtCore, QtGui
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character
from RS.Tools.UI.Face import AmbientCSBGroupSetup

import RS.Tools.UI
import RS.Core.Face.Lib
import RS.Core.Face.Toolbox
import RS.Core.Face.FixFace
import RS.Core.Face.FixJawNull
import RS.Core.Face.ffxPosition

import os
import sys
import fnmatch
import subprocess
import pyfbsdk as mobu 
from RS import Perforce
from RS.Utils import Scene
from xml.dom import minidom
from RS import Config
from pyfbsdk_additions import *
from time import gmtime, strftime
from RS.Core.ReferenceSystem.Types import Face
import RS.Perforce as p4
import xml.etree.ElementTree as ET

class MainWidget( QtGui.QWidget ):
    def __init__( self, *args, **kwargs ):
        QtGui.QWidget.__init__( self, *args, **kwargs )
        
        # Setting main layout to vertical box layout
        mainLayout = QtGui.QVBoxLayout( )
        self.setLayout( mainLayout )
        self.setAutoFillBackground( True )
        
        # Label for title
        label = QtGui.QLabel( )
        label = QtGui.QLabel( " Character(s) in the shot:   ")
        mainLayout.addWidget( label )
        # Node to search for in the scene to find cutscene characters
        self.idControl = "FACIAL_*acialRoot"
        self.idControl_Ambient = "Ambient_UI"
        
        # GUI Element Shared Properties
        lButtonLook = FBButtonLook.kFBLookNormal
        
        # List creation
        self.ctrlsList = QtGui.QListWidget( )
        self.ctrlsList.MultiSelect = False
        self.ctrlsList.setFixedHeight( 70 )
        mainLayout.addWidget( self.ctrlsList, 100 )
        
        
        # fill the list with data
        manager = Manager( )
        
        characters = manager.GetReferenceListByType( Character )
        
        for character in characters:
            self.ctrlsList.addItem( character.Namespace )
            
        # Tab creation
        bottomLeftTabWidget = QtGui.QTabWidget( )
        bottomLeftTabWidget.setSizePolicy( QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Ignored )
        mainLayout.addWidget( bottomLeftTabWidget,1 )
        
        
        # Tab 1: Facial Anim Tab
        tab1 = QtGui.QWidget( )
        tab1hbox = QtGui.QVBoxLayout( )
        tab1.setLayout( tab1hbox )
        
        # Tab 2: Facil Anim UFC Tab
        tab2 = QtGui.QWidget( )
        tab2hbox = QtGui.QVBoxLayout( )
        tab2.setLayout( tab2hbox )
        
        # Tab 3: Facial Rigging Tab
        tab3 = QtGui.QWidget( )
        tab3hbox = QtGui.QVBoxLayout( )
        tab3.setLayout(tab3hbox)
        
        # Calling tab names
            
        # Tab 4: Facial Utilities
        tab4 = QtGui.QWidget( )
        tab4hbox = QtGui.QVBoxLayout( )
        tab4.setLayout( tab4hbox )
            
        bottomLeftTabWidget.addTab( tab4, self.tr( "Utils" ) )       
        bottomLeftTabWidget.addTab( tab1, self.tr( "Facial Anim UFC" ) )
        bottomLeftTabWidget.addTab( tab2, self.tr( "Facial Rigging" ) )
        bottomLeftTabWidget.addTab( tab3, self.tr( "&Facial Anim" ) )
        
            
        
        # Buttons in Tab 12: Facial Anim UFC Tab **************************************************************************
        
        # Create Horizontal Group Box 7: Tagging
        horizontalGroupLayout_7 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Tagging" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_7 )
        
        horizontalGroupLayout_7.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_7.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_7.addLayout( verticalGroupLayout_tab1_03 )
        tab1hbox.addWidget( self.groupBox_tab1 )
        
        # Button -- Facial Root
        self.but_selectFRoot_UFC = QtGui.QPushButton( "Facial Root", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_selectFRoot_UFC )
        self.but_selectFRoot_UFC.setMaximumSize( 800,60 )
        self.but_selectFRoot_UFC.setToolTip( "Select the Facial Root of the selected character" )
        self.but_selectFRoot_UFC.Look = lButtonLook
        self.but_selectFRoot_UFC.Enabled = False
        
        # Button -- Facial Controllers
        self.but_selectFacial_UFC = QtGui.QPushButton( "Facial Controllers", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_selectFacial_UFC )
        self.but_selectFacial_UFC.setMaximumSize( 800,60 )
        self.but_selectFacial_UFC.setToolTip( "Select the Facial Controllers of the selected character" )
        self.but_selectFacial_UFC.Look = lButtonLook
        self.but_selectFacial_UFC.Enabled = False
        
        # Button -- Space Adder
        self.but_spaceAdder = QtGui.QLabel( "", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_spaceAdder )
        self.but_spaceAdder.setMaximumSize( 800,60 )

         
        # Create Horizontal Group Box 8: Cutscene Animation
        horizontalGroupLayout_8 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Cutscene Animation" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_8 )
        
        horizontalGroupLayout_8.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_8.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_8.addLayout( verticalGroupLayout_tab1_03 )
        tab1hbox.addWidget( self.groupBox_tab1 )
        
        # Button -- Create Movie Plane
        self.but_createMoviePlane_UFC = QtGui.QPushButton( "Create Movie Plane", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_createMoviePlane_UFC )
        self.but_createMoviePlane_UFC.setMaximumSize( 800,60 )
        self.but_createMoviePlane_UFC.setToolTip( "Create a movie plane for the selected character" )
        self.but_createMoviePlane_UFC.Look = lButtonLook
        self.but_createMoviePlane_UFC.Enabled = False
        
        # Button -- Create Movie Plane
        self.but_deleteMoviePlane_UFC = QtGui.QPushButton( "Delete Movie Plane", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_deleteMoviePlane_UFC )
        self.but_deleteMoviePlane_UFC.setMaximumSize( 800,60 )
        self.but_deleteMoviePlane_UFC.setToolTip( "Delete a movie plane for the selected character" )
        self.but_deleteMoviePlane_UFC.Look = lButtonLook
        self.but_deleteMoviePlane_UFC.Enabled = False
        
        # Button -- Create Movie Plane
        self.but_resizeMoviePlane_UFC = QtGui.QPushButton( "Resize Movie Plane", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_resizeMoviePlane_UFC )
        self.but_resizeMoviePlane_UFC.setMaximumSize( 800,60 )
        self.but_resizeMoviePlane_UFC.setToolTip( "Reposition a movie plane for the selected character" )
        self.but_resizeMoviePlane_UFC.Look = lButtonLook
        self.but_resizeMoviePlane_UFC.Enabled = False
        
        
        # Create Horizontal Group Box 9: Create/Delete
        horizontalGroupLayout_9 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Create/Delete" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_9 )
        
        horizontalGroupLayout_9.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_9.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_9.addLayout( verticalGroupLayout_tab1_03 )
        tab1hbox.addWidget( self.groupBox_tab1 )
        
        # Button -- Create Headcam
        self.but_headCam_UFC = QtGui.QPushButton( "Create Headcam", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_headCam_UFC )
        self.but_headCam_UFC.setMaximumSize( 800,60 )
        self.but_headCam_UFC.setToolTip( "Create a movie plane for the selected character" )
        self.but_headCam_UFC.Look = lButtonLook
        self.but_headCam_UFC.Enabled = False
        
        # Button -- Delete Headcam
        self.but_delHeadCam_UFC = QtGui.QPushButton( "Delete Headcam", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_delHeadCam_UFC )
        self.but_delHeadCam_UFC.setMaximumSize( 800,60 )
        self.but_delHeadCam_UFC.setToolTip( "Remove all headcams & lights from the scene" )
        self.but_delHeadCam_UFC.Look = lButtonLook
        self.but_delHeadCam_UFC.Enabled = False
        
        # Button -- Space Adder
        self.but_spaceAdder = QtGui.QLabel( "", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_spaceAdder )
        self.but_spaceAdder.setMaximumSize( 800,60 )
        
        
        # Create Horizontal Group Box 10: Miscellaneous
        horizontalGroupLayout_10 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Miscellaneous" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_10 )
        
        horizontalGroupLayout_10.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_10.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_10.addLayout( verticalGroupLayout_tab1_03 )
        tab1hbox.addWidget( self.groupBox_tab1 )
        
        # Button -- Zero Selection
        self.but_zeroSelected_UFC = QtGui.QPushButton( "Zero Selection", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_zeroSelected_UFC )
        self.but_zeroSelected_UFC.setMaximumSize( 800,60 )
        self.but_zeroSelected_UFC.setToolTip( "Sets selected object's translation and rotation values to 0" )
        self.but_zeroSelected_UFC.Look = lButtonLook
        self.but_zeroSelected_UFC.Enabled = False
        
        # Button -- Space Adder
        self.but_spaceAdder = QtGui.QLabel( "", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_spaceAdder )
        self.but_spaceAdder.setMaximumSize( 800,60 )
        
        # Button -- Space Adder
        self.but_spaceAdder = QtGui.QLabel( "", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_spaceAdder )
        self.but_spaceAdder.setMaximumSize( 800,60 )
       
       
        
        # Buttons in Tab 3: Facial Anim UFC Tab **************************************************************************
            
        # Create Horizontal Group Box 10: Miscellaneous
        horizontalGroupLayout_11 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Miscellaneous" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_11 )
        
        horizontalGroupLayout_11.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_11.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_11.addLayout( verticalGroupLayout_tab1_03 )
        tab2hbox.addWidget( self.groupBox_tab1 )
    
        # Button -- Swap CTL CTRL
        self.but_swapCTLCTRL = QtGui.QPushButton( "Swap CTL CTRL", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_swapCTLCTRL )
        self.but_swapCTLCTRL.setMaximumSize( 800,60 )
        self.but_swapCTLCTRL.setToolTip( "Swaps the naming conventions of ALL objects in scene for facial testing" )
        self.but_swapCTLCTRL.Look = lButtonLook
        self.but_swapCTLCTRL.Enabled = False
        
        # Button -- Swap v11/v12
        self.but_swapVersion = QtGui.QPushButton( "Swap v11/v12", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_swapVersion )
        self.but_swapVersion.setMaximumSize( 800,60)
        self.but_swapVersion.setToolTip( "Swaps the naming conventions of relevant controllers from v11 to v12" )
        self.but_swapVersion.Look = lButtonLook
        self.but_swapVersion.Enabled = False
        
        # Button -- Setup Delivery
        self.but_setup3L = QtGui.QPushButton( "Setup Delivery", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_setup3L )
        self.but_setup3L.setMaximumSize( 800,60 )
        self.but_setup3L.setToolTip( "Sets up cameras and groups for new 3L Facial Rig" )
        self.but_setup3L.Look = lButtonLook
        self.but_setup3L.Enabled = False
          
        
        # Buttons in Tab 3: Facial Anim Tab **************************************************************************
        
        # Create Horizontal Group Box 1: Select
        horizontalGroupLayout_12 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Tagging" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_12 )
        
        horizontalGroupLayout_12.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_12.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_12.addLayout( verticalGroupLayout_tab1_03 )
        tab3hbox.addWidget( self.groupBox_tab1 )
    
        # Button -- Facial GUI
        self.but_selectFGUI = QtGui.QPushButton( "Facial GUI", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_selectFGUI )
        self.but_selectFGUI.setMaximumSize( 800,60 )
        self.but_selectFGUI.setToolTip( "Selects the facial gui for facial tagging ")
        self.but_selectFGUI.Look = lButtonLook
        self.but_selectFGUI.Enabled = False
        
        # Button -- Facial Root
        self.but_selectFRoot = QtGui.QPushButton( "Facial Root", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_selectFRoot )
        self.but_selectFRoot.setMaximumSize( 800,60 )
        self.but_selectFRoot.setToolTip( "Select the facial root of the selected character" )
        self.but_selectFRoot.Look = lButtonLook
        self.but_selectFRoot.Enabled = False
        
        # Button -- Facial Controllers
        self.but_selectFacial = QtGui.QPushButton( "Facial Controllers", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_selectFacial )
        self.but_selectFacial.setMaximumSize( 800,60 )
        self.but_selectFacial.setToolTip( "Select the facial controllers of the selected character" )
        self.but_selectFacial.Look = lButtonLook
        self.but_selectFacial.Enabled = False
        
        
        # Create Group Box 2: Create/Delete
        horizontalGroupLayout_13 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Create/Delete" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_13 )
        
        horizontalGroupLayout_13.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_13.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_13.addLayout( verticalGroupLayout_tab1_03 )
        tab3hbox.addWidget( self.groupBox_tab1 )
        
        # Button -- Create Headcam
        self.but_headCam = QtGui.QPushButton( " Create Headcam", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_headCam )
        self.but_headCam.setMaximumSize( 800,60 ) 
        self.but_headCam.setToolTip( "Create a Headcam & a Light constrained to the character's head" )
        self.but_headCam.Look = lButtonLook
        self.but_headCam.Enabled = False
        
        # Button -- Delete Headcam
        self.but_delHeadCam = QtGui.QPushButton( "Delete Headcam", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_delHeadCam )
        self.but_delHeadCam.setMaximumSize( 800,60 )
        self.but_delHeadCam.setToolTip( "Remove all headcams & lights from the scene" )
        self.but_delHeadCam.Look = lButtonLook
        self.but_delHeadCam.Enabled = False
        
        # Button -- Delete Facial GUI
        self.but_deleteFaceGUIGEO = QtGui.QPushButton( "Delete Facial GUI", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_deleteFaceGUIGEO )
        self.but_deleteFaceGUIGEO.setMaximumSize( 800,60 )
        self.but_deleteFaceGUIGEO.setToolTip( "Deletes facial GUI and GEO" )
        self.but_deleteFaceGUIGEO.Look = lButtonLook
        self.but_deleteFaceGUIGEO.Enabled = False
        
            
        # Create Horizontal Group Box 3: Show/Hide
        horizontalGroupLayout_15 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_04 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Select Face Controls" )
        
        # Button -- Create selection sets
        self.but_createSets = QtGui.QPushButton( "Create Selection Sets", parent=self )

        # Button -- Hide On-Face Controls
        self.selectMouthButton= QtGui.QPushButton( "Mouth", parent=self )
        self.selectMouthButton.setMaximumSize( 800,60 )
        self.selectMouthButton.setToolTip( "Show on-face GUI Control Circles" )
        self.selectMouthButton.Look = lButtonLook
        self.selectMouthButton.Enabled = False

        # Button -- Hide On-Face Controls
        self.selectEyesButton= QtGui.QPushButton( "Eyes", parent=self )
        self.selectEyesButton.setMaximumSize( 800,60 )
        self.selectEyesButton.setToolTip( "Show on-face GUI Control Circles" )
        self.selectEyesButton.Look = lButtonLook
        self.selectEyesButton.Enabled = False

        # Button -- Hide On-Face Controls
        self.selectBrowsButton= QtGui.QPushButton( "Brows", parent=self )
        self.selectBrowsButton.setMaximumSize( 800,60 )
        self.selectBrowsButton.setToolTip( "Show on-face GUI Control Circles" )
        self.selectBrowsButton.Look = lButtonLook
        self.selectBrowsButton.Enabled = False
        
        # Button -- Hide On-Face Controls
        self.selectTongueButton= QtGui.QPushButton( "Tongue", parent=self )
        self.selectTongueButton.setMaximumSize( 800,60 )
        self.selectTongueButton.setToolTip( "Show on-face GUI Control Circles" )
        self.selectTongueButton.Look = lButtonLook
        self.selectTongueButton.Enabled = False

        horizontalGroupLayout_15.addWidget( self.selectMouthButton )
        horizontalGroupLayout_15.addWidget( self.selectEyesButton )
        horizontalGroupLayout_15.addWidget( self.selectBrowsButton )
        horizontalGroupLayout_15.addWidget( self.selectTongueButton )
        
        verticalGroupLayout_tab1_01.addWidget( self.but_createSets )
        
        verticalGroupLayout_tab1_01.addLayout( horizontalGroupLayout_15 )
        self.groupBox_tab1.setLayout( verticalGroupLayout_tab1_01 )
        
        tab3hbox.addWidget( self.groupBox_tab1 )

        # Create Horizontal Group Box 3: Show/Hide
        horizontalGroupLayout_14 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Show/Hide On-Face Controls" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_14 )
        
        horizontalGroupLayout_14.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_14.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_14.addLayout( verticalGroupLayout_tab1_03 )
        tab3hbox.addWidget( self.groupBox_tab1 )
                
        # Button -- Hide On-Face Controls
        self.but_showFace= QtGui.QPushButton( "Show Controls", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_showFace )
        self.but_showFace.setMaximumSize( 800,60 )
        self.but_showFace.setToolTip( "Show on-face GUI Control Circles" )
        self.but_showFace.Look = lButtonLook
        self.but_showFace.Enabled = False
        
        # Button -- Delete Facial GUIHide On-Face Controls
        self.but_hideFace = QtGui.QPushButton( "Hide Controls", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_hideFace )
        self.but_hideFace.setMaximumSize( 800,60 )
        self.but_hideFace.setToolTip( "Hide on-face GUI Control Circles" )
        self.but_hideFace.Look = lButtonLook
        self.but_hideFace.Enabled = False

        # Button -- Delete Facial GUIHide On-Face Controls
        self.but_SelectionSets = QtGui.QPushButton( "3L/Ambient Selection Sets", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_SelectionSets )
        self.but_SelectionSets.setMaximumSize( 800,60 )
        self.but_SelectionSets.setToolTip( "Hide on-face GUI Control Circles" )
        self.but_SelectionSets.Look = lButtonLook
        self.but_SelectionSets.Enabled = False

       
        # Create Horizontal Group Box 4: Enable/Disable All Facial Rig Constraints
        horizontalGroupLayout_15 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Enable/Disable All Facial Rig Constraints" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_15 )
        
        horizontalGroupLayout_15.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_15.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_15.addLayout( verticalGroupLayout_tab1_03 )
        tab3hbox.addWidget( self.groupBox_tab1 )

    
        # Button -- Enable All Contorllers
        self.but_enableFaceRigs = QtGui.QPushButton( "Enable All", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_enableFaceRigs )
        self.but_enableFaceRigs.setMaximumSize( 800,60 )
        self.but_enableFaceRigs.setToolTip ( "Enables all the ambient Face Rigging Constraints to improve performance" )
        self.but_enableFaceRigs.Look = lButtonLook
        self.but_enableFaceRigs.Enabled = False
        
        # Button -- Disable All Constraints
        self.but_disableFaceRigs = QtGui.QPushButton( "Disable All", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_disableFaceRigs )
        self.but_disableFaceRigs.setMaximumSize( 800,60 )
        self.but_disableFaceRigs.setToolTip("Disables all the Ambient Face Rigging Constraints to improve performance" )
        self.but_disableFaceRigs.Look = lButtonLook
        self.but_disableFaceRigs.Enabled = False
        
        # Button -- Space Adder
        self.but_spaceAdder = QtGui.QLabel( "", parent=self )
        horizontalGroupLayout_15.addWidget( self.but_spaceAdder )
        self.but_spaceAdder.setMaximumSize( 800,60 )       
        
        # Create Horizontal Group Box 5: Enable/Disable Selected Facial Rig Constraints
        horizontalGroupLayout_16 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Enable/Disable Selected Facial Rig Constraints" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_16 )
        
        horizontalGroupLayout_16.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_16.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_16.addLayout( verticalGroupLayout_tab1_03 )
        tab3hbox.addWidget( self.groupBox_tab1 )
    
        # Button -- Enable Selected Constraints
        self.but_enableselectedFaceRigs = QtGui.QPushButton( "Enable Selected", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_enableselectedFaceRigs )
        self.but_enableselectedFaceRigs.setMaximumSize( 800,60 )
        self.but_enableselectedFaceRigs.setToolTip( "Create a movie plane for the selected character" )
        self.but_enableselectedFaceRigs.Look = lButtonLook
        self.but_enableselectedFaceRigs.Enabled = False
        
        # Button -- Disable Selected Constraints
        self.but_disableselectedFaceRigs = QtGui.QPushButton( "Disable Selected", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_disableselectedFaceRigs )
        self.but_disableselectedFaceRigs.setMaximumSize( 800,60 )
        self.but_disableselectedFaceRigs.setToolTip( "Create a movie plane for the selected character" )
        self.but_disableselectedFaceRigs.Look = lButtonLook
        self.but_disableselectedFaceRigs.Enabled = False
        
        # Button -- Space Adder
        self.but_spaceAdder = QtGui.QLabel( "", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_spaceAdder )
        self.but_spaceAdder.setMaximumSize( 800,60 )
        
        # ------------------------------------------ #
        # Adding Face Finder To The Existing Toolbox #
        # Create Horizontal Group Box : Face Finder  #
        # Contributor: Supratim Chaudhury            #
        # Date: 13-Jan-2022                          #
        # ------------------------------------------ #
        
        horizontalGroupLayout_ff = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Face Finder" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_ff )
        
        horizontalGroupLayout_ff.addLayout( verticalGroupLayout_tab1_01 )
        tab3hbox.addWidget( self.groupBox_tab1 )
        
        # Button -- Face Finder
        self.but_faceFinder = QtGui.QPushButton( "Face Finder", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_faceFinder )
        self.but_faceFinder.setMaximumSize( 800,60 )
        self.but_faceFinder.setToolTip( "Finds which rig to be implemented on which character" )
        self.but_faceFinder.Look = lButtonLook
        self.but_faceFinder.Enabled = False
        
        # ------------------------------------------ #
        
        # Create Horizontal Group Box 6: Miscellaneous
        horizontalGroupLayout_17 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab1_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_02 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab1_03 = QtGui.QVBoxLayout( )
        
        self.groupBox_tab1 = QtGui.QGroupBox( "Miscellaneous" )
        self.groupBox_tab1.setLayout( horizontalGroupLayout_17 )
        
        horizontalGroupLayout_17.addLayout( verticalGroupLayout_tab1_01 )
        horizontalGroupLayout_17.addLayout( verticalGroupLayout_tab1_02 )
        horizontalGroupLayout_17.addLayout( verticalGroupLayout_tab1_03 )
        tab3hbox.addWidget( self.groupBox_tab1 )
    
        # Button -- Fix Face
        self.but_fixFace = QtGui.QPushButton( "Fix Face", parent=self )
        verticalGroupLayout_tab1_01.addWidget( self.but_fixFace )
        self.but_fixFace.setMaximumSize( 800,60 )
        self.but_fixFace.setToolTip( "Fixes joint-related issues on face" )
        self.but_fixFace.Look = lButtonLook
        self.but_fixFace.Enabled = False
        
        # Button -- Fix FACIAL_jaw_Null
        self.but_fixJawNull = QtGui.QPushButton( "Fix FACIAL_jaw_Null", parent=self )
        verticalGroupLayout_tab1_02.addWidget( self.but_fixJawNull )
        self.but_fixJawNull.setMaximumSize( 800,60 )
        self.but_fixJawNull.setToolTip( "Fixes the common FACIAL_jaw_Null issue" )
        self.but_fixJawNull.Look = lButtonLook
        self.but_fixJawNull.Enabled = False
        
        # Button -- Zero Selection
        self.but_zeroSelected = QtGui.QPushButton( "Zero Selection", parent=self )
        verticalGroupLayout_tab1_03.addWidget( self.but_zeroSelected )
        self.but_zeroSelected.setMaximumSize( 800,60 )
        self.but_zeroSelected.setToolTip( "Sets selected object's translation and rotation values to 0" )
        self.but_zeroSelected.Look = lButtonLook
        self.but_zeroSelected.Enabled = False
        
        # Buttons in Tab 4: Utils Tab **************************************************************************
    
        # Create Horizontal Group Box 7: Select
        horizontalGroupLayout_tab4_01 = QtGui.QHBoxLayout( )
        verticalGroupLayout_tab4_01 = QtGui.QVBoxLayout( )
        verticalGroupLayout_tab4_02 = QtGui.QVBoxLayout( )
        
        groupBox_tab4_faceDevice = QtGui.QGroupBox( "Face Device Sample Rate" )
        groupBox_tab4_faceDevice.setLayout( horizontalGroupLayout_tab4_01 )
        
        groupBox_tab4_all = QtGui.QGroupBox( "ALL" )
        groupBox_tab4_all.setLayout( verticalGroupLayout_tab4_01 )
        
        groupBox_tab4_sel = QtGui.QGroupBox( "Selected Character" )
        groupBox_tab4_sel.setLayout( verticalGroupLayout_tab4_02 )
        
        horizontalGroupLayout_tab4_01.addWidget( groupBox_tab4_all )
        horizontalGroupLayout_tab4_01.addWidget( groupBox_tab4_sel )
        
        tab4hbox.addWidget( groupBox_tab4_faceDevice )        
        
        # Button -- Set Sample Rate All Back to 60
        self.but_setSampleRate_resetAll = QtGui.QPushButton( "Set ALL to 60", parent=self )
        self.but_setSampleRate_resetAll.setMaximumSize( 800,60 )
        self.but_setSampleRate_resetAll.setToolTip( "Set the sample rate for All Characters" )
        self.but_setSampleRate_resetAll.Look = lButtonLook
        self.but_setSampleRate_resetAll.Enabled = False
        verticalGroupLayout_tab4_01.addWidget( self.but_setSampleRate_resetAll )
        
        # Button -- Set Sample Rate All Down to 20
        self.but_setSampleRate_lowerAll = QtGui.QPushButton( "Set ALL to 15", parent=self )
        self.but_setSampleRate_lowerAll.setMaximumSize( 800,60 )
        self.but_setSampleRate_lowerAll.setToolTip( "Set the sample rate for All Characters" )
        self.but_setSampleRate_lowerAll.Look = lButtonLook
        self.but_setSampleRate_lowerAll.Enabled = False
        verticalGroupLayout_tab4_01.addWidget( self.but_setSampleRate_lowerAll )
        
        # Button -- Set Sample Rate All Back to 60
        self.but_setSampleRate_resetSel = QtGui.QPushButton( "Set Selected to 60", parent=self )
        self.but_setSampleRate_resetSel.setMaximumSize( 800,60 )
        self.but_setSampleRate_resetSel.setToolTip( "Set the sample rate for selected Character" )
        self.but_setSampleRate_resetSel.Look = lButtonLook
        self.but_setSampleRate_resetSel.Enabled = False
        verticalGroupLayout_tab4_02.addWidget( self.but_setSampleRate_resetSel )
        
        # Button -- Set Sample Rate Selected Down to 20
        self.but_setSampleRate_lowerSel = QtGui.QPushButton( "Set Selected to 15", parent=self )
        self.but_setSampleRate_lowerSel.setMaximumSize( 800,60 )
        self.but_setSampleRate_lowerSel.setToolTip( "Set the sample rate for selected Character" )
        self.but_setSampleRate_lowerSel.Look = lButtonLook
        self.but_setSampleRate_lowerSel.Enabled = False
        verticalGroupLayout_tab4_02.addWidget( self.but_setSampleRate_lowerSel )
        
        #
        # Misc Section
        #
        verticalGroupLayout_tab4_02 = QtGui.QVBoxLayout( )
        groupBox_tab4_misc = QtGui.QGroupBox( "Misc" )
        groupBox_tab4_misc.setLayout( verticalGroupLayout_tab4_02 )
        tab4hbox.addWidget( groupBox_tab4_misc )
        
        # Misc Section Button -- Tag All Analogs
        self.but_tagAllAnalogs = QtGui.QPushButton( "Tag All Analog Controllers", parent=self )
        self.but_tagAllAnalogs.setMaximumSize( 800,60 )
        self.but_tagAllAnalogs.setToolTip( "Tags all analog controls related to all characters" )
        self.but_tagAllAnalogs.Look = lButtonLook
        #self.but_tagAllAnalogs.Enabled = False
        verticalGroupLayout_tab4_02.addWidget( self.but_tagAllAnalogs )
        
        # Misc Section Button -- Tag Ears Analogs
        self.but_tagEarAnalogs = QtGui.QPushButton( "Tag Ear Analog Controllers", parent=self )
        self.but_tagEarAnalogs.setMaximumSize( 800,60 )
        self.but_tagEarAnalogs.setToolTip( "Tags all ear analog controls related to all characters" )
        self.but_tagEarAnalogs.Look = lButtonLook
        #self.but_tagEarAnalogs.Enabled = False
        verticalGroupLayout_tab4_02.addWidget( self.but_tagEarAnalogs )
        
        # New temporary facefx ui toggle
        self.but_toggleFaceFXUI = QtGui.QPushButton( "Toggle FaceFX Layout", parent=self )
        verticalGroupLayout_tab4_02.addWidget( self.but_toggleFaceFXUI )
         
        # Space adder at end of each tab
        
        self.CreateHorizontalBoxwithNullButton( tab1hbox, " " )
        self.CreateHorizontalBoxwithNullButton( tab1hbox, " " )
        
        self.CreateHorizontalBoxwithNullButton( tab2hbox, " " )
        self.CreateHorizontalBoxwithNullButton( tab2hbox, " " )
        self.CreateHorizontalBoxwithNullButton( tab2hbox, " " )
        self.CreateHorizontalBoxwithNullButton( tab2hbox, " " )
        self.CreateHorizontalBoxwithNullButton( tab2hbox, " " )
        
        self.CreateHorizontalBoxwithNullButton( tab4hbox, " " )
        self.CreateHorizontalBoxwithNullButton( tab4hbox, " " )
        self.CreateHorizontalBoxwithNullButton( tab4hbox, " " )
        self.CreateHorizontalBoxwithNullButton( tab4hbox, " " )
        self.CreateHorizontalBoxwithNullButton( tab4hbox, " " )        
        
        # Bind event handlers.
        
        self.but_resizeMoviePlane_UFC.clicked.connect( self.ResizeMoviePlane_Clicked )
        self.but_swapVersion.clicked.connect( self.SwapVersion_Clicked )
        self.but_setup3L.clicked.connect( self.Setup3L_Clicked )
        self.but_swapCTLCTRL.clicked.connect( self.SwapCTLCTRL_Clicked)
        self.but_deleteFaceGUIGEO.clicked.connect( self.DeleteFaceGUIGEO_Clicked )
        self.but_showFace.clicked.connect( self.ShowFace_Clicked )
        self.but_hideFace.clicked.connect( self.HideFace_Clicked )
        self.but_SelectionSets.clicked.connect( self.ambientSelectionSetsSetups )       
        self.but_headCam.clicked.connect( self.HeadCam_Clicked )
        self.but_headCam_UFC.clicked.connect( self.HeadCam_Clicked )
        self.but_enableFaceRigs.clicked.connect( self.EnableFaceRigs_Clicked )
        self.but_disableFaceRigs.clicked.connect( self.DisableFaceRigs_Clicked )
        self.but_enableselectedFaceRigs.clicked.connect( self.EnableselectedFaceRigs_Clicked )
        self.but_disableselectedFaceRigs.clicked.connect( self.DisableselectedFaceRigs_Clicked ) 
        
        # ------------EDIT 13-JAN-2022-------------- #
        self.but_faceFinder.clicked.connect( self.FaceFinder_Clicked)
        # ------------------------------------------ #
        
        self.but_fixFace.clicked.connect( self.FixFace_Clicked )
        self.but_fixJawNull.clicked.connect( self.FixJawNull_Clicked )
        self.but_createMoviePlane_UFC.clicked.connect( self.CreateMoviePlane_Clicked )
        self.but_deleteMoviePlane_UFC.clicked.connect( self.DeleteMoviePlane_Clicked )
        self.but_delHeadCam.clicked.connect( self.DelHeadCam_Clicked )
        self.but_delHeadCam_UFC.clicked.connect( self.DelHeadCam_Clicked )
        self.but_selectFGUI.clicked.connect( self.SelectFGUI_Clicked )
        self.but_selectFacial.clicked.connect( self.SelectFacial_Clicked )
        self.but_selectFacial_UFC.clicked.connect( self.SelectFacial_Clicked )
        self.but_selectFRoot.clicked.connect( self.SelectFRoot_Clicked )
        self.but_selectFRoot_UFC.clicked.connect( self.SelectFRoot_Clicked )
        self.but_zeroSelected.clicked.connect( self.ZeroSelected_Clicked ) 
        self.but_zeroSelected_UFC.clicked.connect( self.ZeroSelected_Clicked ) 
        self.but_setSampleRate_resetAll.clicked.connect( self.setSampleRate_resetAll_Clicked )
        self.but_setSampleRate_lowerAll.clicked.connect( self.setSampleRate_lowerAll_Clicked )
        self.but_setSampleRate_resetSel.clicked.connect( self.setSampleRate_resetSel_Clicked )
        self.but_setSampleRate_lowerSel.clicked.connect( self.setSampleRate_lowerSel_Clicked )
        self.but_tagAllAnalogs.clicked.connect( self.tagAllAnalogs_Clicked )
        self.but_tagEarAnalogs.clicked.connect( self.tagEarAnalogs_Clicked )
        self.but_toggleFaceFXUI.clicked.connect( self.toggleFaceFXUI_Clicked )
        self.but_createSets.clicked.connect(self.createSets)
        self.selectMouthButton.clicked.connect(partial(self.selectSets, "Mouth"))
        self.selectEyesButton.clicked.connect(partial(self.selectSets, "Eyes"))
        self.selectBrowsButton.clicked.connect(partial(self.selectSets, "Brows"))
        self.selectTongueButton.clicked.connect(partial(self.selectSets, "Tongue"))

    ## Functions ##
    def ambientSelectionSetsSetups(self):
        """
        Setup SelectionSet and FaceControl groupe
        """        
        tNamespace_index = self.ctrlsList.currentRow( )
        nameSpace = self.ctrlsList.item( tNamespace_index).text( )
        AmbientCSBGroupSetup.Run(nameSpace)
        
    def createSets(self):
        tNamespace_index = self.ctrlsList.currentRow( )
        namespace = self.ctrlsList.item( tNamespace_index).text( )
        if namespace:
            RS.Core.Face.Toolbox.fixFacialGroups(namespace)
        
    def selectSets(self, setName):
        """
        Select models from SetGroups with selected namespace
        """
        Scene.DeSelectAll()
        tNamespace_index = self.ctrlsList.currentRow( )
        namespace = self.ctrlsList.item( tNamespace_index).text( )
        if namespace:
            RS.Core.Face.Toolbox.fixFacialGroups(namespace)
        setName = "{0}:{1}".format(namespace, setName)
        for setObject in Globals.Sets:
            if setName == setObject.LongName:
                for model in setObject.Items:
                    model.Selected = True
                    
    # On Click - Swap Raise Lift
    def ResizeMoviePlane_Clicked( self ) :
        """
        Swaps the naming conventions of relevant controllers from v11 to v12.
        Renames all facial controllers with name Lift to Raise.
        """
        print ( "\nSwap v11/v12 button clicked" )
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item( tNamespace_index).text( )
        
        tNamespace_item_str = str( tNamespace_item + ":" + self.idControl )
        
        if self.idControl in tNamespace_item_str:
        
            tList = FBComponentList( )
            FBFindObjectsByName( "SKEL_Head", tList, False, True)
            
            if len( tList ) == 1 :            
                RS.Core.Face.Toolbox.deselectAll( )
                RS.Core.Face.Toolbox.findHeadByNamespace( tNamespace_item )
                
                tList[0].Selected = True
                
            facewareList = FBComponentList( )
            FBFindObjectsByName( "FacewareMoviePlane_CS*", facewareList, False, True)
            
            if len( facewareList ) == 1 :            
                RS.Core.Face.Toolbox.deselectAll( )
                facewareList[0].Selected = True
                
                facewareList[0].Parent = tList[0]
                
                offset = FBVector3d( 0,0,-0.25)
                facewareList[0].Translation.Data = tList[0].Translation.Data + offset
                print tList[0].Translation.Data
                print facewareList[0].Translation.Data
                
                  
  
    # On Click - Swap Raise Lift
    def SwapVersion_Clicked( self ) :
        """
        Swaps the naming conventions of relevant controllers from v11 to v12.
        Renames all facial controllers with name Lift to Raise.
        """
        print ( "\nSwap v11/v12 button clicked" )
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = str( self.ctrlsList.item( tNamespace_index).text( ) )
        
        RS.Core.Face.Lib.switch_Raise_Lift( )
        print ( self.tNamespace_item + " : Swapped v11/v12 Done." )
        
    
    # On Click - but_setup3L_Clicked
    def Setup3L_Clicked( self ) :
        """
       Sets up the facial rig for delivery to animators for easy navigation and control.
        """
        print ( "\nSetup Delivery button clicked" )
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = str ( self.ctrlsList.item( tNamespace_index).text( ) )
        
        tNamespace_item_str = str( tNamespace_item + ":" + self.idControl )
    
        RS.Core.Face.Lib.setupCams( )
        RS.Core.Face.Lib.setupGroups_Master( )   
        print ( tNamespace_item + " : Delivery Setup Done." )
         
        
    # On Click - Swap CTL CTRL
    def SwapCTLCTRL_Clicked( self ) :
        """
        Swaps the naming conventions of ALL objects in scene for facial testing.
        """
        print ( "\nSwap CTL CTRL button clicked" )
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = str ( self.ctrlsList.item( tNamespace_index).text( ) )
        
        tNamespace_item_str = str( tNamespace_item + ":" + self.idControl )
        
        RS.Core.Face.Lib.switch_CTL_CTRL()
        print ( tNamespace_item + " : Swapped CTL CTR." )
    
   
    # On Click - Delete Facial GUI & GEO
    def DeleteFaceGUIGEO_Clicked( self ):
        """
        Deletes Facial GUI and GEO.
        """
        print ( "\nDelete Facial GUI & GEO button clicked" )
        
        tNamespace_index = ctrlsList.currentRow( )
        tNamespace_item = str( self.ctrlsList.item( tNamespace_index).text( ) )
        
        idControl1 = "FACIAL_facialRoot"
        idControl2 = "FACIAL_C_FacialRoot"
        
        tNamespace_item_str1 = str( tNamespace_item + ":" + idControl1 )
        tNamespace_item_str2 = str( tNamespace_item + ":" + idControl2 )

        if idControl1 in tNamespace_item_str1:
            tList = FBComponentList( )
            FBFindObjectsByName( tNamespace_item_str1, tList, True, True )
            
            if len( tList ) == 1 :         
                RS.Core.Face.Toolbox.destroyFacialStuff( tNamespace_item )
                tList[0].Selected = True  
                print "sucess"
                
        if idControl2 in tNamespace_item_str2:
            print ( tNamespace_item_str2 )
            tList = FBComponentList( )
            FBFindObjectsByName( tNamespace_item_str1, tList, True, True )
            
            if len( tList ) == 1 :                             
                print ( "fail" )
         

    # On Click - Show Face Ctrls
    def ShowFace_Clicked( self ):
        """
        Shows all on-face gui control circles.
        """
        print "\nShow Face Ctrls button clicked"
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = str ( self.ctrlsList.item( tNamespace_index).text( ) )
        
        idControl1 = "*ACIAL_facialRoot"
        tNamespace_item_str1 = str( tNamespace_item + ":" + idControl1 )
        
        if idControl1 in tNamespace_item_str1:
            
            tList = FBComponentList( )
            FBFindObjectsByName( tNamespace_item_str1, tList, True, True )
            
            if len( tList ) == 1 :
                RS.Core.Face.Toolbox.showHideFaceCtrls( tNamespace_item + ":")
                tList[0].Selected = True
                print "{0} : Facial Controls Shown.".format( tNamespace_item )
            
    
    # On Click - Hide Face Ctrls
    def HideFace_Clicked( self ):
        """
        Hides all on-face gui control circles.
        """
        print "\nHide Face Ctrls button clicked"
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = str ( self.ctrlsList.item( tNamespace_index ).text( ) )
        
        idControl1 = "*ACIAL_facialRoot"
        tNamespace_item_str1 = str( tNamespace_item + ":" + idControl1 )
    
        if idControl1 in tNamespace_item_str1:
            
            tList = FBComponentList( )
            FBFindObjectsByName( tNamespace_item_str1, tList, True, True )
            
            if len( tList ) == 1 :
                RS.Core.Face.Toolbox.showHideFaceCtrls( tNamespace_item + ":")
                tList[0].Selected = True
                print "{0} : Facial Controls Hidden.".format( tNamespace_item )
            
   
    # On Click - Create Movie Plane
    def CreateMoviePlane_Clicked ( self ) :
        """
        Creates a movie plane.
        """
        print "\nCreate Movie Plane button clicked"
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = str( self.ctrlsList.item( tNamespace_index).text( ))
        
        #tNamespace_item_str = str( tNamespace_item + ":" + self.idControl )

        if tNamespace_item :
            RS.Core.Face.Lib.createFaceMoviePlane( tNamespace_item )
        print tNamespace_item + " : Movie Plane Created."
                     
    
    # On Click - Delete Movie Plane
    def DeleteMoviePlane_Clicked ( self ) :
        """
        Deletes a movie plane.
        """
        print "\nDelete Movie Plane button clicked"
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = str( self.ctrlsList.item( tNamespace_index).text( ))
        
        #tNamespace_item_str = str( tNamespace_item + ":" + self.idControl )

        if tNamespace_item :
            RS.Core.Face.Lib.removeFaceMoviePlane( tNamespace_item )
        print tNamespace_item + " : Movie Plane Deleted."
                                   
                     
    # On Click - Create Headcam & Lights
    def HeadCam_Clicked ( self ) :
        """
        Creates a headcam & a light constrained to the character's head.
        """
        print "\nCreate Headcam & Lights button clicked"
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = str( self.ctrlsList.item( tNamespace_index).text( ))
        
        tNamespace_item_str = str( tNamespace_item + ":" + self.idControl )
        
        tParent = RS.Core.Face.Toolbox.findHeadByNamespace(tNamespace_item)
        RS.Core.Face.Toolbox.createConstrainedHeadCam(tNamespace_item,tParent)
        print tNamespace_item + " : Headcam & Lights Created."
                     
    
    # On Click - Enable all Facial Rigging Constraints
    def EnableFaceRigs_Clicked ( self ):
        """
        Enables all character's the ambient face rigging constraints to improve performance.
        """
        print "\nEnable Facial Rigging Constraints for All Characters button clicked"
        
        for i in RS.Globals.Constraints:
            
            if i.Name == 'Ambient_anim 3Lateral':
                i.Active = True
            
            if i.Name == 'Viseme Relation':
                i.Active = True       
                print "{0} : Enabled All Facial Rigging Constraints Characters.".format( i.Name )   
        
        
    # On Click - Disable all Facial Rigging Constraints
    def DisableFaceRigs_Clicked ( self ):
        """
        Disables all character's the ambient face rigging constraints to improve performance.
        """
        print "\nDisable All Facial Rigging Constraints button clicked"
        
        for i in RS.Globals.Constraints:
            
            if i.Name == 'Ambient_anim 3Lateral':
                i.Active = False
            
            if i.Name == 'Viseme Relation':
                i.Active = False    
                print "{0} : Disabled Facial Rigging Constraints for All Characters.".format( i.Name )   
    
    
    # On Click - Enable selected Facial Rigging Constraints
    def EnableselectedFaceRigs_Clicked( self ):            
        """
        Enables selected character's the ambient face rigging constraints to improve performance.
        """
        print "\nEnable Selected Facial Rigging Constraints button clicked"
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item( tNamespace_index).text( )
        tNamespace_item_str = str( tNamespace_item + ":" + self.idControl )
        
        if self.idControl in tNamespace_item_str:
            print tNamespace_item
            
            for i in RS.Globals.Constraints:
                
                if i.LongName == tNamespace_item + ':' + 'Ambient_anim 3Lateral':
                    i.Active = True
                
                if i.LongName == tNamespace_item + ':' + 'Viseme Relation':
                    i.Active = True
                    print "{0} : Enabled Facial Rigging Constraints for Selected Character.".format( i.Name )   
    
   
    # On Click - Disable selected Facial Rigging Constraints
    def DisableselectedFaceRigs_Clicked( self ):            
        """
        Disables selected character's the ambient face rigging constraints to improve performance.
        """
        print "\nDisable Selected Facial Rigging Constraints button clicked"
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item( tNamespace_index).text( )
        
        tNamespace_item_str = str( tNamespace_item + ":" + self.idControl )
        
        if self.idControl in tNamespace_item_str:
            print tNamespace_item
            
            for i in RS.Globals.Constraints:
                
                if i.LongName == tNamespace_item + ':' + 'Ambient_anim 3Lateral':
                    i.Active = False
                
                if i.LongName == tNamespace_item + ':' + 'Viseme Relation':
                    i.Active = False
                    print "{0} : Disabled Facial Rigging Constraints for Selected Character.".format( i.Name )   

        
    # ------------EDIT 13-JAN-2022-------------- #
    # On Click - Face Finder
    
    def FaceFinder_Clicked( self ):
        """
        Finds which face-rig is to be implemented on which character in the scene
        """
        print "\nFace Finder button clicked"
        
        from RS.Core.Face.FaceFinder import run
        run.Run()
        
    # ------------------------------------------ #    
    
    # On Click - Fix Face
    def FixFace_Clicked( self ):
        """
        Fixes joint-related issues on face.
        """
        print "\nFix Face button clicked"
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item( tNamespace_index).text( )
        
        RS.Core.Face.FixFace.applyMinMax( tNamespace_item )
        print tNamespace_item + " : Fixed Face."
    
   
    # On Click - Fix Jaw Null
    def FixJawNull_Clicked( self ):
        """
        Fixes the common FACIAL_jaw_Null issue.
        """
        print "\nFix Jaw Null button clicked"
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = str( self.ctrlsList.item( tNamespace_index).text(  ) )
        
        RS.Core.Face.FixJawNull.rs_fixJawNull( tNamespace_item )
        print tNamespace_item + " : Fixed Jaw Null."          
    
    
    # On Click - Delete Headcam & Lights
    def DelHeadCam_Clicked ( self ) :
        """
        Deletes all headcams & lights in the scene.
        """
        print "\nDelete Headcam & Lights button clicked"
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item( tNamespace_index).text( )
        
        RS.Core.Face.Toolbox.deleteAllConstrainedStuff()
        print "{0} : Headcam & Lights deleted.".format( tNamespace_item )
        
    
    # On Click - Select Facial GUI
    def SelectFGUI_Clicked ( self ) :
        """
        Select the facial gui of the selected character.
        """
        print "\nSelect Facial GUI button clicked"
        
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item( tNamespace_index).text( )
        
        idControl1 = "FACIAL_facialRoot"
        tagObject = "faceControls_OFF"
       
        tNamespace_item_str = str( tNamespace_item + ":" + tagObject )  
        
        fGUI=FBFindModelByLabelName(tNamespace_item_str)
        fGUI.Selected = True
        print tNamespace_item_str + " : Facial GUI Selected."
            
            
    # On Click - Select All Facial Controls
    def SelectFacial_Clicked ( self ) :
        """
        Selects the facial controllers of the selected character.
        """
        print "\nSelect Facial Controllers button clicked"
        
        tNamespace_index = self.ctrlsList.currentRow()
        tNamespace_item = str( self.ctrlsList.item(tNamespace_index).text( ) ) 
        
        idControl1 = "FACIAL_facialRoot"
        idControl2 = "FACIAL_C_FacialRoot"
        
        tNamespace_item_str1 = str( tNamespace_item + ":" + idControl1 )
        tNamespace_item_str2 = str( tNamespace_item + ":" + idControl2 )
        
        if idControl1 in tNamespace_item_str1:
            
            tList = FBComponentList( )
            FBFindObjectsByName( tNamespace_item_str1, tList, True, True )
            
            if len( tList ) == 1 :               
                RS.Core.Face.Toolbox.selectFacialCtrls(tNamespace_item)
                tList[0].Selected = True
                print "{0} : Facial Controllers Selected.".format( tNamespace_item )
            
        tNamespace_item_str1 = str( tNamespace_item + ":" + idControl1 )
        
        if idControl2 in tNamespace_item_str2:
            
            tList = FBComponentList( )
            FBFindObjectsByName( tNamespace_item_str2, tList, True, True )
            
            if len( tList ) == 1 :               
                
                RS.Core.Face.Toolbox.selectUFCFacialCtrls(tNamespace_item)
                tList[0].Selected = True
                print "{0} : Facial Controllers Selected.".format( tNamespace_item )
    
    
    # On Click - Select Facial Root node
    def SelectFRoot_Clicked ( self ) :
        """
        Select the facial root of the selected character.
        """
        print "\nSelect Facial Root button clicked."
       
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item( tNamespace_index).text( )
        
        tNamespace_item_str = str( tNamespace_item + ":" + self.idControl )
        
        if self.idControl in tNamespace_item_str:
        
            tList = FBComponentList( )
            FBFindObjectsByName( tNamespace_item_str, tList, True, True )
            
            if len( tList ) == 1 :            
                RS.Core.Face.Toolbox.deselectAll( )
                tList[0].Selected = True
                print "{0} Selected.".format( tList[0].LongName )
        else :
            tList = FBComponentList( )
            FBFindObjectsByName( tNamespace_item_str + ":" + self.idControl, tList, True, True )
           
            if len(tList) == 1 :            
                RS.Core.Face.Toolbox.deselectAll( )
                tList[0].Selected = True
                print "{0} Selected.".format( tList[0].LongName )             
            
            
    # On Click - Zero Selected
    def ZeroSelected_Clicked( self ): 
        """
        Sets selected object's translation and rotation values to 0.
        """
        print "\nZero selected button clicked."
       
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item(tNamespace_index).text( )
    
        if tNamespace_item :
            RS.Core.Face.Toolbox.zeroSelectedControls( )
            print "{0} : Zero Selection.".format( tNamespace_item )  
            
    def setSampleRate_resetAll_Clicked ( self ):
        RS.Core.Face.Lib.setAllSamplingRate_UFC( 60 )
    
    def setSampleRate_lowerAll_Clicked ( self ):
        RS.Core.Face.Lib.setAllSamplingRate_UFC( 15 )
    
    def setSampleRate_resetSel_Clicked ( self ):
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item( tNamespace_index )
        
        if tNamespace_item :
            RS.Core.Face.Lib.setSingleSamplingRate_UFC( 60, tNamespace_item.text( ) )
    
    def setSampleRate_lowerSel_Clicked ( self ):
        tNamespace_index = self.ctrlsList.currentRow( )
        tNamespace_item = self.ctrlsList.item( tNamespace_index )
        
        if tNamespace_item :
            RS.Core.Face.Lib.setSingleSamplingRate_UFC( 15, tNamespace_item.text( ) )
            
    def tagAllAnalogs_Clicked( self ):
        RS.Core.Face.Lib.tagAllAnalogs( "headGui" )
        FBMessageBox( "Tag All Analogs", "Tagging Complete!  Double check the models have been added.", "OK" )
            
    def tagEarAnalogs_Clicked( self ):
        RS.Core.Face.Lib.tagEarAnalogs( "headGui" )
        FBMessageBox( "Tag Ear Analogs", "Tagging Complete!  Double check the models have been added.", "OK" )
        
    def toggleFaceFXUI_Clicked( self ):
        RS.Core.Face.ffxPosition.run()
        
    # Space adder
    def CreateHorizontalBoxwithNullButton( self, tabGroupLayout, labelName ):
        """
        This function add a spacer at the end of each tab.
     
        Arguments:
            self: entity to check
            tabGroupLayout: tab group name
            labelName: label name attached to the tab
        
        Returns: A label in the specified tab group
        """
        verticalGroupLayout = QtGui.QVBoxLayout( )
        self.group = QtGui.QLabel( labelName )
        
        self.group.setLayout( verticalGroupLayout )
        tabGroupLayout.addWidget( self.group )

        self.group.setFixedWidth(112)      
        
        
class Facial_Toolbox( RS.Tools.UI.QtMainWindowBase ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, title = 'Facial Toolbox', size = [ 153, 750 ] )
       
        self.centralWidget = MainWidget()
        self.setCentralWidget( self.centralWidget )
       

def Run():    
    tool = Facial_Toolbox()
    tool.show()