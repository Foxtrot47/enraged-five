from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS.Tools.UI.Mocap.Models import gsSkelMatchModelTypes
from RS.Tools.UI.Mocap import const
from RS.Tools.CameraToolBox.PyCoreQt.Delegates import comboboxDelegate
from RS.Core.Mocap import MocapCommands
from RS.Utils import Scene


class GSSkeletonInformation:
    '''
    Class to hold a list of properties relating to a character.
    '''
    def __init__(self,
                 CharacterNamespace,
                 CharacterNode,
                 GSSkeletonPath,
                 ActorString,
                 CheckedState,
                 UserColor):

        self.CharacterNamespace = CharacterNamespace
        self.CharacterNode = CharacterNode
        self.GSSkeletonPath = GSSkeletonPath
        self.ActorString = ActorString
        self.CheckedState = CheckedState
        self.UserColor = UserColor


class GSSkeletonMatchWidgets(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        super(GSSkeletonMatchWidgets, self).__init__(parent=parent)
        self.setupUi()

    def setupUi(self):
        '''
        Sets up the widgets and their properties
        '''
        # create layout
        mainLayout = QtGui.QVBoxLayout()

        # model and tree widget
        self.mainModel =  gsSkelMatchModelTypes.GSSkelInfoModel()
        self.treeView = QtGui.QTreeView()
        self.treeView.setModel(self.mainModel)

        # button widget
        pushButton = QtGui.QPushButton("Merge Selected GS Skeletons")

        # tree properties
        self.treeView.setAlternatingRowColors(True)
        self.treeView.setStyleSheet("QTreeView {\nalternate-background-color: rgb(56, 56, 56); \nbackground-color: rgb(43, 43, 43);\n} \n")
        self.treeView.setSortingEnabled(True)
        self.treeView.header().setSortIndicator(0, QtCore.Qt.AscendingOrder)
        self.treeView.header().resizeSection(0, 200)
        self.treeView.header().resizeSection(1, 200)
        self.treeView.header().resizeSection(2, 190)

        # tree delegates
        self.treeView._comboBoxDelegate = comboboxDelegate.ComboBoxDelegate(
                                                                       self.mainModel.GetGsSkelNameList()
                                                                            )
        self.treeView.setItemDelegateForColumn(1, self.treeView._comboBoxDelegate)
        self.treeView._colorComboBoxDelegate = comboboxDelegate.ComboBoxDelegateWithIcons(
                                                                                     const.Icons.colorDict
                                                                                          )
        self.treeView.setItemDelegateForColumn(3, self.treeView._colorComboBoxDelegate)

        # add widgets to layout
        mainLayout.addWidget(self.treeView)
        mainLayout.addWidget(pushButton)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        pushButton.released.connect(self._handleMergeGSSkeletons)

    def GetCharacterInformationList(self):
        '''
        Generates the Character info List, using the 'GSSkeletonInformation' class
        
        returns: List
        '''
        CharacterInformationList = []
        # generate character information list
        for child in self.mainModel.rootItem.children():
            # get character namespace
            characterNamespace = child.GetCharacterNamespace()
            # get character node
            characterNode = child.GetCharacterNode()
            # get gs skeleton path
            gsSkelPath = child.GetGSSkelPath()
            # get user actor name
            userActorString = child.GetUserActorString()
            # get checked state
            checkedState = child.GetCheckedState()
            # reset checked state
            if checkedState:
                child.SetCheckedState()
            # get user color
            userColor = child.GetUserColor()

            # build list
            CharacterInformationList.append(GSSkeletonInformation(
                                                                  characterNamespace,
                                                                  characterNode,
                                                                  gsSkelPath,
                                                                  userActorString,
                                                                  checkedState,
                                                                  userColor
                                                                 ))
        return CharacterInformationList

    def _handleMergeGSSkeletons(self):
        '''
        Handle for connecting with pushButton.
        
        Merges in the gs Skeleton and sets the appropriate properties based of what the user has 
        selected.
        '''
        # get list of character info
        CharacterInformationList = self.GetCharacterInformationList()

        if not len(CharacterInformationList) > 0:
            return

        for info in CharacterInformationList:
            if info.CheckedState:
                # set options
                options = mobu.FBFbxOptions(True)
                options.Materials = mobu.FBElementAction.kFBElementActionDiscard
                options.NamespaceList = '{0}:{1}'.format(info.ActorString, info.CharacterNamespace)
                options.CamerasAnimation = False
                options.Cameras = mobu.FBElementAction.kFBElementActionDiscard
                # merge file
                mobu.FBApplication().FileMerge(info.GSSkeletonPath, False, options)

                # get gs skel character node
                gsCharacter = mobu.FBSystem().Scene.Characters[-1]

                # get gsskel's hips, then find mesh
                hierarchyList = []
                meshNode = None
                gsSkelHips = gsCharacter.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
                Scene.GetChildren(gsSkelHips, hierarchyList, "", True)
                for node in hierarchyList:
                    if '_mesh' in node.Name.lower():
                        meshNode = node
                        break
                for key, value in const.Icons.colorDict.iteritems():
                    if info.UserColor == key:
                        if meshNode !=None:
                            # add colored material based on usercolor
                            material = mobu.FBMaterial('{0}:{1}'.format(
                                                                        info.ActorString,
                                                                        info.CharacterNamespace
                                                                        ))
                            material.Diffuse = mobu.FBColor(value[1])
                            meshNode.Materials.append(material)
                        # get character hips and assign custom properties with user color and
                        # matching gs skel
                        characterHips = info.CharacterNode.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
                        if characterHips:
                            colorProperty = characterHips.PropertyCreate(
                                                                        'actor color',
                                                                         mobu.FBPropertyType.kFBPT_ColorRGB,
                                                                         "",
                                                                         False,
                                                                         True,
                                                                         None
                                                                         )
                            colorProperty.Data = mobu.FBColor(value[1])
                            colorProperty.SetLocked(True)
                            colorStrProperty = characterHips.PropertyCreate(
                                                                            'actor color string',
                                                                            mobu.FBPropertyType.kFBPT_charptr,
                                                                            "",
                                                                            False,
                                                                            True,
                                                                            None
                                                                            )
                            colorStrProperty.Data = info.UserColor
                            colorStrProperty.SetLocked(True)
                            gsMatchProperty = characterHips.PropertyCreate(
                                                                          'gs match namespace',
                                                                           mobu.FBPropertyType.kFBPT_charptr,
                                                                           "",
                                                                           False,
                                                                           True,
                                                                           None
                                                                           )
                            gsMatchProperty.Data = '{0}:{1}'.format(
                                                                    info.ActorString,
                                                                    info.CharacterNamespace
                                                                    )
                            gsMatchProperty.SetLocked(True)
                            # assign custom properties to gsSkel with matching character namespace
                            customProperty = gsSkelHips.PropertyCreate(
                                                                      'character match namespace',
                                                                       mobu.FBPropertyType.kFBPT_charptr,
                                                                       "",
                                                                       False,
                                                                       True,
                                                                       None
                                                                        )
                            customProperty.Data = '{0}:{1}'.format(info.ActorString, info.CharacterNamespace)
                            customProperty.SetLocked(True)
                            colorProperty = gsSkelHips.PropertyCreate(
                                                                        'actor color',
                                                                         mobu.FBPropertyType.kFBPT_ColorRGB,
                                                                         "",
                                                                         False,
                                                                         True,
                                                                         None
                                                                         )
                            colorProperty.Data = mobu.FBColor(value[1])
                            colorProperty.SetLocked(True)
                            colorStrProperty = gsSkelHips.PropertyCreate(
                                                                            'actor color string',
                                                                            mobu.FBPropertyType.kFBPT_charptr,
                                                                            "",
                                                                            False,
                                                                            True,
                                                                            None
                                                                            )
                            colorStrProperty.Data = info.UserColor
                            colorStrProperty.SetLocked(True)
                            break

                #character to character with character node
                info.CharacterNode.InputCharacter = gsCharacter
                info.CharacterNode.InputType = mobu.FBCharacterInputType.kFBCharacterInputCharacter
                info.CharacterNode.ActiveInput = True

                # snap to active stage
                activeStage = MocapCommands.FindActiveStage()
                gsSkelParent = Scene.GetParent(gsSkelHips)
                if activeStage:
                    Scene.AlignTranslation(gsSkelParent, activeStage, True, True, True)

        # deselected any row which may be selected
        self.treeView.clearSelection()