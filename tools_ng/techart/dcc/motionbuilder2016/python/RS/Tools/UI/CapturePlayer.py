"""
Description:
    Video Player that displays the latest capture from the current scene in motion builder.

Authors:
    David Vega <david.vega@rockstargames.com>
"""

from pyfbsdk import *

import os

from PySide import QtGui
from collections import OrderedDict

import RS.Config
import RS.Tools.UI
import RS.Tools.UI.VideoPlayer
import RS.Tools.CapturePlayer

DEFAULT_VIDEO = ("-" * 39) + " Select Video " +  ("-" * 39)

class CapturePlayer(RS.Tools.UI.QtBannerWindowBase):

    __information_dictionary = OrderedDict((("Date", "Capture Date"),
                                            ("Capture", "Capture Revision"),
                                            ("FBX", "FBX Revision"),))
    __VideoDictionary = {DEFAULT_VIDEO: os.path.join(RS.Config.Tool.Path.TechArt,
                                                r"dcc\motionbuilder2014\images\VideoPlayer\error.mp4")}

    def __init__(self, *args, **kwargs):
        super(CapturePlayer, self).__init__(*args, **kwargs)

        self.infoLayout = QtGui.QHBoxLayout()
        self.infoLayout.setSpacing(5)
        self.infoLayout.setContentsMargins(10, 0, 10, 0)

        self.dropdown = QtGui.QComboBox()
        self.dropdown.setFixedHeight(30)
        for each in self.__information_dictionary:

            setattr(self, "{}Label".format(each), QtGui.QLabel(self.__information_dictionary[each]))
            setattr(self, "{}Field".format(each), QtGui.QLineEdit(self))
            getattr(self, "{}Field".format(each)).setDisabled(True)
            getattr(self, "{}Field".format(each)).setDisabled(True)

            self.infoLayout.addWidget(getattr(self, "{}Label".format(each)))
            self.infoLayout.addWidget(getattr(self, "{}Field".format(each)))

        self.CaptureField.setFixedWidth(22)
        self.FBXField.setFixedWidth(22)
        self.player = RS.Tools.UI.VideoPlayer.VideoPlayer()

        self.reloadButton = QtGui.QPushButton(self)
        self.reloadButton.setText("Reset")
        self.reloadButton.setIcon(QtGui.QIcon(os.path.join(RS.Config.Tool.Path.TechArt,
                                               r"dcc\motionbuilder2014\images\Camera\icons\Reset.png")))
        self.reloadButton.clicked.connect(self.Reset)

        self.dropdown.currentIndexChanged.connect(self.UpdateDropdown)
        self._mainLayout.setSpacing(0)
        self._mainLayout.addWidget(self.dropdown)
        self._mainLayout.addLayout(self.infoLayout)
        self._mainLayout.addWidget(self.player)
        self._mainLayout.addWidget(self.reloadButton)

        self.Reset()

        self.setStyleSheet("""

        QLabel[text="FBX Revision"]{
            font: bold 12px;
            color: #FFE7A3}
        QLineEdit{
             background-color: #252526;
             font: bold 12px;
             color: white}

        QComboBox{
            background-color: #BFBFBF;
            font: bold 12px;
            color: #2d2d30;}

        QPushButton{
            font: bold 12px;}
        """)
    def Reset(self):
        """ Updates the dropdown menu and attempts to select the capture that matches the filename"""
        text = os.path.splitext(os.path.split(FBApplication().FBXFileName)[-1])[0]

        index = self.dropdown.findText(text)
        self.PopulateDropdown()

        if index < 0:
            index = 0
        self.dropdown.setCurrentIndex(index)

    def Update(self, url, isVideo=False):
        """ Queries the database for the video url and updates the UI """

        self.data = RS.Tools.CapturePlayer.GetCaptureData(url, isVideo=isVideo)

        self.player.path = self.data.url

        self.FBXField.setText(str(self.data.fbxRevision))
        self.DateField.setText(str(self.data.captureUpdated))
        self.CaptureField.setText(str(self.data.captureRevision))

        self.player.Play()

    def PopulateDropdown(self):
        """ Updates the dropdown menu to include all the latest .mov files on the database """
        self.dropdown.clear()
        self.__VideoDictionary.update({"{} : {}".format(each[1], each[0].split("/")[-1].split(".")[0]): each[0] for each
                                      in RS.Tools.CapturePlayer.GetAllVideoPaths()})
        #Implement File Delegation
        self.dropdown.addItems(sorted(self.__VideoDictionary.keys()))

    def UpdateDropdown(self, *args):
        """ Updates the video playing whenever a new selection is made on the dropdown menu"""
        self.Update(self.__VideoDictionary.get(self.dropdown.currentText(), DEFAULT_VIDEO), isVideo=True)


def Run(show=True, *args):
    tool = CapturePlayer(title=__name__.split(".")[-1], size=[460, 440])
    if show:
        try:
            tool.show_data()
        except Exception, e:
            #print_this(e)
            pass
        tool.show()
    return tool