from pyfbsdk import *
from pyfbsdk_additions import *

import os

import RS.Tools.UI
import RS.Utils.Profiler
import RS.Config

profileTree = None
profilesList = None


class ProfilerTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Profiler", size = [ 500, 700 ] )

    ## Functions ##
    
    def RecursePopulateProfileTree( self, parentTreeNode, parentEvent ):
        global profileTree
        
        for event in parentEvent.subEvents:
            treeNode = profileTree.InsertLast( parentTreeNode, '{1} seconds - {0}'.format( event.context, event.totalTime ) )
            
            self.RecursePopulateProfileTree( treeNode, event )
    
    def PopulateProfileTree( self, events ):
        global profileTree
        profileTree.Clear()
        
        root = profileTree.GetRoot()
        
        for event in events:
            node = profileTree.InsertLast( root, '{1} seconds - {0}'.format( event.context, event.totalTime ) )
            
            self.RecursePopulateProfileTree( node, event )


    ## Event Handlers ##
    
    def OnProfile_Changed( self, source, event ):
        global profilesList
        
        profile = profilesList.Items[ profilesList.ItemIndex ]
        
        events = RS.Utils.Profiler.loadProfileEvents( '{0}\\profiler\\{1}.xml'.format( RS.Config.Script.Path.Root, profile ) )
        self.PopulateProfileTree( events )
    
    
    ## Layouts ##
    
    def Create( self, mainLyt ):
        global profileTree
        global profilesList
        
        ctrlLyt = FBVBoxLayout()
        
        x = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( self.BannerHeight, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( 0, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( 0, FBAttachType.kFBAttachBottom, '' )
        mainLyt.AddRegion( 'main', 'main', x, y, w, h )
        mainLyt.SetControl( 'main', ctrlLyt )
        
        profileTree = FBTree()
        
        profilesList = FBList()
        profilesList.Style = FBListStyle.kFBDropDownList
        profilesList.OnChange.Add( self.OnProfile_Changed )
        
        # Populate profiles list.
        profiles = os.listdir( '{0}\\profiler\\'.format( RS.Config.Script.Path.Root ) )
        
        for profile in profiles:
            profilesList.Items.append( str( profile ).split( '.' )[ 0 ] )
        
        ctrlLyt.Add( profilesList, 24 )
        ctrlLyt.AddRelative( profileTree, 1 )

def Run( show = True ):
    tool = ProfilerTool()
    
    if show:
        tool.Show()
        
    return tool
