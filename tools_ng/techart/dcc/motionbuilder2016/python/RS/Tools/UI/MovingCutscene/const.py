class Nulls(object):
    """
    List of null names used by the MCS tool
    """
    MASTER_MOVER_NAME = "MASTER_MOVER"
    MASTER_CAMERA_NAME = "MASTER_CAMERA"
    MASTER_CHARACTER_NAME = "MASTER_CHARACTER"
    MASTER_PROP_NAME = "MASTER_PROP"
    ZERO_NULL_NAME = "SOURCE"
    FREEZE_NULL_NAME = "FREEZE"

    TOP_LEVEL_CHILDREN = (MASTER_CAMERA_NAME, MASTER_CHARACTER_NAME, MASTER_PROP_NAME)

    # Used to catch possible names for nulls that should be renamed
    _nullRenameDict = {"Camera": MASTER_CAMERA_NAME,
                       "Cameras": MASTER_CAMERA_NAME,
                       "Character": MASTER_CHARACTER_NAME,
                       "Characters": MASTER_CHARACTER_NAME,
                       "Prop": MASTER_PROP_NAME,
                       "Props": MASTER_PROP_NAME}

    @classmethod
    def getNewName(cls, name):
        """ Returns the new name when you pass an old name """
        return cls._nullRenameDict.get(name)

    @classmethod
    def getOldNames(cls, name):
        """ Returns the old names when you pass a new name """
        return [oldName for oldName, newName in cls._nullRenameDict.items() if newName == name]


class Filters(object):
    """
    List of filters for different views
    """
    PROP_FILTER_NAMES = "|".join(["MC_", "PLAYER_", "chassis_", "Ped_", "mover_", "PH_"])
    PROP_VIEW = r"^((?!{}).)*(Control_|_Control$)".format(PROP_FILTER_NAMES)
    CHARACTER_VIEW = r"^(?!.*_gs$).*$"
    VEHICLE_VIEW = r"(.+chassis_Control$)"
