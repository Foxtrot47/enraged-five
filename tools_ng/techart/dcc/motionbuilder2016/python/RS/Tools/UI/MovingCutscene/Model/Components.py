"""
Module that contains wrappers around FBModelMarkers that make up the Moving Cutscene rig setup
"""
import re
import types
import collections

import pyfbsdk as mobu

from PySide import QtGui

from RS import Globals
from RS.Utils import Scene, Creation, Namespace, ContextManagers
from RS.Utils.Scene import Component, Constraint
from RS.Core.Animation import Lib
from RS.Tools.UI.MovingCutscene import const
from RS.Tools.UI import Application


class AbstractObject(object):
    """
    Abstract class that handles constraint folder creation and parenting
    """
    MatchList = ("skel_saddle",)

    def _getConstraintFolder(self, namespace, name):
        """
        Gets the constraint folder, if it isn't found then it creates it

        Args:
            namespace (string): namespace to look for the folder in
            name (string):  name of the folder

        Return:
            pyfbsdk.FBFolder
        """
        if self._folder is None or Component.IsDestroyed(self._folder):
            folderName = "{}:{}".format(namespace, name)
            folders = filter(lambda folder: folderName in [folder.Name, folder.LongName], Globals.Scene.Folders)
            if folders:
                self._folder = folders[0]
            else:
                self._folder = Creation.rs_CreateFolder(folderName, "Constraints")
                self._parentFolder(self._folder, self._parent)
        return self._folder

    def _parentConstraint(self, constraint, parent):
        """
        Parents the constraint under the folder of the parent

        Args:
            constraint (pyfbsdk.FBConstraint): constraint to parent
            parent (Marker): marker object that the constraint is being parented under
        """
        for parentFolder in Scene.GetOutgoingConnections(constraint, [mobu.FBFolder]):
            parentFolder.DisconnectSrc(constraint)

        folder = parent.constraintFolder()
        if folder is not None:
            folder.ConnectSrc(constraint)

    def _parentFolder(self, folder, parent):
        """
        Parents the folder under the folder of the parent

        Args:
            folder (pyfbsdk.FBFolder): constraint to parent
            parent (Marker): marker object that the constraint is being parented under
        """
        if not parent:
            return

        parentFolder = parent.constraintFolder()
        if parentFolder is not None:
            for currentParentFolder in Scene.GetOutgoingConnections(folder, [mobu.FBFolder]):
                if parentFolder == currentParentFolder:
                    return
                parentFolder.DisconnectSrc(currentParentFolder)
            parentFolder.ConnectSrc(folder)

    def _getChildren(self, parent, depth=0):
        """
        Recursive method for getting all the children of a Marker

        Args:
            parent (Marker): marker to get children from
            depth (int): recursion depth

        Return:
            list[Marker, etc.]
        """
        children = []
        for child in parent.children():
            children.append(child)
            children.extend(self._getChildren(child, depth=depth+1))
        return children

    def delete(self):
        """ Deletes the marker and associated constraints & folders """
        with ContextManagers.PreventUIUpdateActive():
            marker = self.marker()
            if Component.IsDestroyed(marker):
                return

            folders = [self.constraintFolder()]
            constraints = Scene.GetIncomingConnections(marker, filterByType=[mobu.FBConstraint]) + Scene.GetOutgoingConnections(marker, filterByType=[mobu.FBConstraint])
            for constraint in constraints:
                constraint.FBDelete()
            marker.FBDelete()

            # Remove Folders from the scene
            while folders:
                folder = folders[0]
                if Component.IsDestroyed(folder):
                    break
                folders = filter(lambda parent: isinstance(parent, mobu.FBFolder), folder.Parents)
                if not folder.Items:
                    folder.FBDelete()
                else:
                    break
            parent = self.parent()
            if parent:
                parent.removeChild(self)

    def setProperties(self, component, force=False, **properties):
        """
        Sets properties on the component without erroring out

        Args:
            component (mobu.FBComponent): compoenent to set property values on
            force (bool): Set the property even if the property doesn't exist
            **properties (dictionary): property names and their values
        """
        debug = Component.DEBUG
        Component.DEBUG = False
        try:
            Component.SetProperties(component, force, **properties)
        except:
            raise
        finally:
            Component.DEBUG = debug

    def isCamera(self):
        """ Is the marker for this object a camera """
        return False


class MovingCutscene(AbstractObject):
    """
    Class for interacting and creating the initial structure of the moving cutscene setup
    """
    _prefix = "MC"
    _cache = {}

    def __new__(cls, driver=None, namespace=None):
        """
        Overrides built-in method

        Caches the namespace used so the same instance of the MovingCutscene can be returned. Useful for
        storing data between sessions of the tool ui.

        Args:
            driver (mobu.FBComponent): component that is driving the moving cutscene
            namespace (string): namespace for the moving cutscene. Accepts pyfbsdk.FBNamespaces as well.
        """
        namespaceString = namespace
        if isinstance(namespace, mobu.FBNamespace):
            namespaceString = namespace.LongName
        elif not namespace and driver:
            namespaceString = "{}_{}".format(cls._prefix, Namespace.GetNamespace(driver) or driver.Name)

        instance = cls._cache.get(namespaceString, None)

        if instance is None or Component.IsDestroyed(instance.namespace()):
            instance = object.__new__(cls)
        return instance

    def __init__(self, driver=None, namespace=None):
        """
        Constructor

        Caches the namespace used so the same instance of the MovingCutscene can be returned. Useful for
        storing data between sessions of the tool ui.

        Args:
            driver (mobu.FBComponent): component that is driving the moving cutscene
            namespace (string): namespace for the moving cutscene. Accepts pyfbsdk.FBNamespaces as well.
        """
        namespaceString = namespace
        if isinstance(namespace, mobu.FBNamespace):
            namespaceString = namespace.LongName
        elif not namespace and driver:
            namespaceString = "{}_{}".format(self._prefix, Namespace.GetNamespace(driver) or driver.Name)
            namespace = namespaceString

        instance = MovingCutscene._cache.get(namespaceString, None)
        if instance is not None and not Component.IsDestroyed(instance.namespace()):
            return
        elif instance is not None:
            MovingCutscene._cache[namespaceString] = self

        if isinstance(namespace, basestring):
            namespace = Namespace.GetFBNamespace(namespace)
            if not namespace:
                namespace = mobu.FBNamespace(namespaceString, None)

        self._parent = None
        self._driver = None
        self._name = namespace.LongName
        self._namespace = namespace
        self._mastermover = None
        self._zeronull = None
        self._freezenull = None
        self._folder = None
        self._connect = True
        self._create = True
        self.masterNull()
        self.zeroNull()
        self.freezeNull()

        currentDriver = self.driver()
        isExistingDriver = driver == currentDriver
        if not isExistingDriver and driver:
            self.setDriver(driver)

        if self._mastermover:
            for null in (self._zeronull, self._freezenull):

                if not null.component() and not null.constraint():
                    null.setComponent(self._mastermover.marker(), isNull=True)
                    # Deactivate constraint so it's not set to follow source/freeze nulls
                    null.constraint().Active = False
                    # Lock constraint position so it won't change offset
                    null.constraint().Lock = True
                    self._parentConstraint(null.constraint(), self)

        # grab _freezeNull if active else grab _zeronull
        self._lastActive = self._zeronull if not self._freezenull.constraint().Active else self._freezenull

    def marker(self):
        """
        The FBModelMarker that drives the moving cutscene

        Return:
            pyfbsdk.FBModelMarker
        """
        if self.masterNull():
            return self.masterNull().marker()

    def masterNull(self):
        """
        The Marker object that represents the Master Null.
        Creates the master null for this moving cutscene if it doesn't exist.

        Return:
            Marker
        """
        if self._mastermover is None:
            self._mastermover = self._getComponent(const.Nulls.MASTER_MOVER_NAME)
            self._mastermover.marker().Visibility = True
            self._mastermover.marker().Show = True
        return self._mastermover

    def zeroNull(self):
        """
        The Marker object that represents the Zero Null.
        Creates the zero null for this moving cutscene if it doesn't exist.

        Return:
            Marker
        """
        if self._zeronull is None:
            self._zeronull = self._getComponent(const.Nulls.ZERO_NULL_NAME)
        return self._zeronull

    def freezeNull(self):
        """
        The Marker object that represents the Freeze Null.
        Creates the freeze null for this moving cutscene if it doesn't exist.

        Return:
            Marker
        """
        if self._freezenull is None:
            self._freezenull = self._getComponent(const.Nulls.FREEZE_NULL_NAME)
        return self._freezenull

    def _getComponent(self, name):
        """
        Attempts to find the FBModel with the given name, creates it if it doesn't exist and wraps it around a
        Marker instance.

        Args:
            name (string): name of the FBModel object to find

        Return:
            Marker
        """
        for component in self.component():
            if component.Name == name:
                return Marker(self, component.LongName)
        return Marker(self, "{}:{}".format(self._namespace.LongName, name))

    def children(self):
        """
        The immediate children of the master null Marker

        Return:
            list[Marker, etc.]
        """
        return self.masterNull().children()

    def allChildren(self):
        """
        The all children of the master null Marker

        Return:
            list[Marker, etc.]
        """
        return self._getChildren(self.masterNull())

    def parent(self):
        return self._mastermover.parent()

    def setParent(self, parent):
        return self._mastermover.setParent(parent)

    def component(self):
        """
        list of all the components found under the moving cutscene namespace

        Return:
            list[pyfbsdk.FBComponent, etc.]
        """
        if self.namespace():
            return Namespace.GetContents(self._namespace)

    def create(self):
        """ Create missing components of the moving cutscene"""
        return self._create

    def setCreate(self, value):
        """ Sets if components of the moving cutscene should be created when not found"""
        self._create = value

    def delete(self):
        """
        Overrides inherited method

        Also deletes the namespace for the moving cutscene rig
        """
        for child in self.allChildren():
            if child.isCamera():
                child.reversePlot(active=True)
        super(MovingCutscene, self).delete()
        self._deleteNamespace(self.namespace())

    def _deleteNamespace(self, namespace):
        """
        Recursively deletes child namespaces

        Args:
            namespace (pyfbsdk.FBNamespace): namespace to delete
        """
        for child in namespace.ChildrenNamespaces:
            self._deleteNamespace(child)
        namespace.FBDelete()

    def connect(self):
        """ Connect the parent and children of the parent constraints when it is created """
        return self._connect

    def setConnect(self, value):
        """ Sets if the parent and children of the parent constraints should be connected when it is created """
        self._connect = value

    def build(self):
        """ Creates a constraint for all the children of the moving cutscene if possible """
        self.setConnect(True)
        for child in self.allChildren():
            child.constraint()

    def name(self):
        """Name of the of the moving cutscene"""
        if self.namespace():
            return self._namespace.LongName
        return ""

    def setName(self, value):
        """
        Sets the name of the namespace of the moving cutscene

        Args:
            value (string): name to set for the marker
        """
        if self.namespace():
            if not re.match("MC_|MOVING_CUTSCENE_", value, re.I):
                value = "MC_{}".format(value)
            self._namespace.LongName = value

    def namespace(self):
        """The fbnamespace of the moving cutscene"""
        try:
            if Component.IsDestroyed(self._namespace):
                self._namespace = None
        except Exception:
            self._namespace = None

        return self._namespace

    def namespaceString(self):
        """ The name of the namespace of the moving cutscene """
        if self.namespace():
            return self._namespace.LongName
        return ""

    def constraintFolder(self):
        """ The FBFolder for the moving cutscene """
        if self.namespace():
            folder = self._getConstraintFolder(self._namespace.LongName, "Constraints")
            if not Component.IsDestroyed(folder):
                return folder

    def driver(self):
        """ The FBModel that is driving the moving cutscene """
        if (not self._driver or Component.IsDestroyed(self._driver)) and self._mastermover is not None:
            driverProperty = self._mastermover.marker().PropertyList.Find("Driver")
            if driverProperty:
                self._driver = mobu.FBFindModelByLabelName(driverProperty.Data)
                if not self._driver:
                    for character in Globals.Characters:
                        if character.LongName == driverProperty.Data:
                            self._driver = character
                            break

        return self._driver

    def setDriver(self, driver):
        """
        Sets the driver that is suppose to drive the moving cutscene

        Args:
            driver (pyfbsdk.FBComponent): The FBModel or FBCharacter component that will drive the moving cutscene
        """
        if isinstance(driver, (mobu.FBModel, mobu.FBCharacter)):
            self._driver = driver
            if self._mastermover is not None:
                self.setProperties(self.masterNull().marker(), force=True, driver=driver.LongName)
                self.masterNull().setComponent(self._driver)

    def driverNamespace(self):
        """ The name of namespace of the driver object """
        if self.driver():
            return Namespace.GetNamespace(self._driver).strip() or self._driver.Name
        return "Nothing"

    @classmethod
    def all(cls):
        """ Generator that returns all the moving cutscenes in the current scene """
        for namespace in Globals.Namespaces:
            movingCutscene = cls._cache.get(namespace.LongName, None)
            if movingCutscene and Component.IsDestroyed(movingCutscene.namespace()):
                cls._cache.pop(namespace.LongName)
                movingCutscene = None

            if not movingCutscene and re.match("MOVING_CUTSCENE|MC_", namespace.LongName) and namespace.LongName.count(":") == 0:
                yield MovingCutscene(namespace=namespace)

            elif movingCutscene:
                yield movingCutscene

            continue

    def addChild(self, child):
        """
        Adds the Marker as a child of the moving cutscene

        Args:
            child (Marker): marker object to parent under the moving cutscene
        """
        self.masterNull().addChild(child)

    def active(self):
        """ Is the zero or freeze null active for the moving cutscene active """
        for null in (self._zeronull, self._freezenull):
            if null and null.hasConstraint() and not Component.IsDestroyed(null.constraint()):
                if null.constraint().Active:
                    return True
        return False

    def setFreezeActive(self, value):
        """Set if the freeze null of the moving cutscene should be active or not

        Args:
            value (bool): set constraint active or not
        """
        if self._freezenull and self._freezenull.hasConstraint() and not Component.IsDestroyed(
                self._freezenull.constraint()):
            self._freezenull.constraint().Active = value
            if value:  # turn off zero null if activating freeze
                self.setZeroActive(False)
                self._lastActive = self._freezenull

    def setZeroActive(self, value):
        """Set if the zero null of the moving cutscene should be active or not

        Args:
            value (bool): set constraint active or not
        """
        if self._zeronull and self._zeronull.hasConstraint() and not Component.IsDestroyed(self._zeronull.constraint()):
            self._zeronull.constraint().Active = value
            if value:  # turn off freeze null if activating zero
                self.setFreezeActive(False)
                self._lastActive = self._zeronull

    def setActive(self, value):
        """Set if the zero/freeze null of the moving cutscene should be active or not

        Args:
            value (bool): set constraint active or not
        """
        for null in (self._zeronull, self._freezenull):
            if null and null.hasConstraint() and not Component.IsDestroyed(null.constraint()):

                # record last active so it remembers which to activate/deactivate
                if not value and null.constraint().Active:
                    self._lastActive = null
                    null.constraint().Active = value
                else:
                    if null is self._lastActive:
                        null.constraint().Active = value

    def transferMotion(self):
        """ Transfer the motion from the driver to the moving cutscene """
        if self._mastermover and isinstance(self._driver, mobu.FBModel):
            constraint = self._mastermover.constraint()
            if constraint:
                active = constraint.Active
                constraint.Active = False
            Lib.TransferMotion(self.marker(), self._driver)
            if constraint:
                constraint.Active = active

    @classmethod
    def plotSelected(cls):
        """
        Plot the components of the selected markers of all the moving cutscene objects in the scene.
        All the moving cutscene's are zero'd out before any plotting is done
        """
        selection = {}
        active = {}
        plotComponents = []
        for movingCutscene in cls.all():
            active[movingCutscene] = movingCutscene.active()
            movingCutscene.setActive(True)
            for child in movingCutscene.allChildren():
                marker = child.marker()
                component = child.component()
                selected = marker.Selected
                if selected:
                    if isinstance(component, mobu.FBCharacter):
                        child.plot()
                    elif component is not None:
                        plotComponents.append(component)
                    selection[marker] = True
                    marker.Selected = False

        with ContextManagers.ClearModelSelection(ignoreList=plotComponents) as _:
            Lib.PlotObjects(plotComponents)

        # restore previous active state for the moving cutscenes
        for component, selected in selection.iteritems():
            component.Selected = selected
        for movingCutscene, active in active.iteritems():
            movingCutscene.setActive(active)

    @classmethod
    def getMarkersByNamespace(cls, namespace):
        """
        Finds marker objects based on the namespace of the component they manage

        Args:
            namespace (string): namespace of the component a marker currently manipulates. This value can also be a
                                pyfbsdk.FBNamespace object

        Return:
            list
        """
        if isinstance(namespace, mobu.FBNamespace):
            namespace = namespace.LongName

        markers = []
        for movingCutscene in cls.all():
            for marker in movingCutscene.allChildren():
                component = marker.component()
                if not component:
                    continue
                componentNamespace = Namespace.GetNamespace(component)
                if componentNamespace == namespace:
                    markers.append(marker)
        return markers

    @classmethod
    def getMarkerByName(cls, name):
        """
        Finds marker objects based on the namespace of the component they manage

        Args:
            name (string): namespace of the component a marker currently manipulates.

        Return:
            Marker
        """
        for movingCutscene in cls.all():
            for marker in movingCutscene.allChildren():
                component = marker.component()
                if not component:
                    continue
                componentNamespace = component.LongName
                if componentNamespace != name:
                    return marker

    @classmethod
    def getMarkerByComponent(cls, component):
        """
        Finds marker objects based on the namespace of the component they manage

        Args:
            name (string): namespace of the component a marker currently manipulates.

        Return:
            Marker
        """
        for movingCutscene in cls.all():
            for marker in movingCutscene.allChildren():
                if component == marker.component():
                    return marker


class Marker(AbstractObject):

    def __init__(self, movingCutscene, markerName, parent=None, match=False, isNew=False):
        """Constructor

        Args:
            movingCutscene (MovingCutscene): the object that represents the moving cutscene
            marker (string): name of the marker
            parent (Marker): the parent marker of this object
            match (bool): If True will match marker to driver
            isNew (bool): If True will ensure cameras when created will default to null setup
        """
        namespace = movingCutscene.namespaceString()

        # check for marker first if it's a camera
        marker = mobu.FBFindModelByLabelName(markerName)
        if not marker and not isinstance(marker, mobu.FBCamera) or isNew:
            # grab null otherwise
            if not markerName.startswith(namespace):
                markerName = "{}:{}".format(namespace, markerName)

            marker = mobu.FBFindModelByLabelName(markerName)

        if not marker:
            marker = mobu.FBModelMarker(markerName)
            marker.Size = 5000
            marker.PropertyList.Find("LookUI").Data = 1
            marker.Visibility = False

        self._movingCutscene = movingCutscene
        self._marker = marker
        self._parent = parent
        self._component = None
        self._componentConstraint = None
        self._driver = None
        self._driverConstraint = None
        self._children = collections.OrderedDict()
        self._folder = None
        self._connect = False
        self._characterComponent = None
        if isinstance(parent, (Marker, MovingCutscene)):
            # Catch old names for child markers and rename them
            newMarkerName = const.Nulls.getNewName(marker.Name)
            if newMarkerName:
                marker.Name = newMarkerName
            self._parent = parent
            parent.addChild(self)
        if match:
            self.match()

    def name(self):
        """ Name of the marker """
        if self.marker():
            return self._marker.Name
        return "-- DELETED --"

    def setName(self, value):
        """
        Sets the name of the marker

        Args:
            value (string): name to set for the marker
        """
        if self.marker():
            self._marker.Name = value

    def namespace(self):
        """ pyfbsdk.FBNamespace of the marker """
        return self._movingCutscene.namespace()

    def marker(self):
        """ The FBModelMarker object that this Marker object wraps around """
        if self._marker is not None and Component.IsDestroyed(self._marker):
            self._marker = None
        return self._marker

    def component(self):
        """ The pyfbsdk.FBComponent that this marker drives """
        marker = self.marker()

        if marker is not None and self._component is None:
            # Get Constraints attached to the marker where the marker is the driver
            self._component, self._componentConstraint = self._validateComponent(self._marker, self._componentConstraint, isDestination=False)
        elif marker is not None and Component.IsDestroyed(self._component):
            componentProperty = self._marker.PropertyList.Find("componentName")
            self._component = None
            if componentProperty is not None:
                self._component = mobu.FBFindModelByLabelName(componentProperty.Data)
        return self._component

    def setComponent(self, component, isNull=False):
        """
        Sets the component that this marker should drive.

        Args:
            component (pyfbsdk.FBComponent): component that should be constrained to this marker
            isNull (bool): If True will match as as a folder null and not copy animation
        """
        if self._componentConstraint is not None:
            self._componentConstraint.FBDelete()
            self._componentConstraint = None

        self._component = component
        name = component.LongName if component is None else ""
        self.setProperties(component, force=True, componentName=name)

        self.match(component, isNull=isNull)

        if self._movingCutscene.connect():
            self._componentConstraint = self.constraint()

    def driver(self):
        """ The pyfbsdk.FBComponent that drives this marker """
        if not self._driver and self.marker():
            self._driver, self._driverConstraint = self._validateComponent(self._marker, self._driverConstraint, isDestination=True)
        return self._driver

    def setDriver(self, driver):
        """ Sets the pyfbsdk.FBComponent that should drive this marker

        Args:
            driver (pyfbsdk.FBComponent): the component to drive this marker
        """
        if self._driverConstraint is not None:
            self._driverConstraint.FBDelete()

        if self.marker():
            self._driverConstraint = self._createConstraint(driver, self._marker)
            self._driver = driver

    def _getCharacterComponent(self, character):
        """
        If the component is a character, return a valid component to match position from

        Args:
            character (mobu.FBCharacter): character to get a valid component from
        """
        # Search for pre-defined components that take precedence within the namespace of the component
        if self._characterComponent is None or Component.IsDestroyed(self._characterComponent):
            self._characterComponent = None
            namespace = Namespace.GetFBNamespace(Namespace.GetNamespace(character))
            if namespace:
                _components = {}
                matchListLength = len(self.MatchList)
                for _component in Namespace.GetContentsByType(namespace, mobu.FBModel, False):
                    if _component.Name.lower() in self.MatchList:
                        _components[_component.Name.lower()] = _component
                        if len(_components) == matchListLength:
                            break

                for name in self.MatchList:
                    _component = _components.get(name, None)
                    if _component is not None:
                        self._characterComponent = _component
                        break
        return self._movingCutscene.marker()

    def plotCharacterEdits(self):
        """
        Plots the Control Rig to itself and merges down animation from the animation layers to the base layer
        """
        with ContextManagers.ClearModelSelection():
            Lib.ActivateControlRig(self._component, True)
            components = Lib.GetControlsFromCharacter(self._component)
            Lib.GetAnimatableComponents(components, properties=("Lcl Translation", "Lcl Rotation", "Lcl Scaling", "Ik Blend T", "Ik Blend R", "Ik Pull"), setFocus=True)
            Lib.FlattenLayers(Globals.System.CurrentTake, selection=True)

    def plotCharacterToStage(self):
        """
        Plots the character's control rig while the moving cutscene is in motion to fix any double transforms that
        may be happening

        Args:
            index (QtCore.QModelIndex): index from the view
        """

        character = self.component()
        if not isinstance(character, mobu.FBCharacter):
            return False

        namespace = Namespace.GetNamespace(character)

        # Find root of the character skeleton
        root = mobu.FBFindModelByLabelName(":".join(filter(None, (namespace, "mover"))))
        if not root:
            return False

        # Turn Moving Cutscene On
        currentFrame = Globals.System.LocalTime.GetFrame()
        Globals.Player.GotoStart()
        active = self.active()
        movingCutsceneActive = self._movingCutscene.active()
        self._movingCutscene.setActive(True)
        constraint = self.constraint()
        if constraint:
            constraint.Active = False
            constraint.Lock = False
            for fbpropery in constraint.PropertyList:
                if fbpropery.Name.endswith("Offset T") or fbpropery.Name.endswith("Offset R"):
                    fbpropery.Data = mobu.FBVector3d(0, 0, 0)
            constraint.Active = True
            constraint.Lock = True
        # Switch Character To Skeleton
        Lib.ActivateControlRig(character, False)

        # Plot from Skeleton to Control Rig
        Lib.Plot(character, "rig", UseConstantKeyReducer=False)

        # Set Cutscene Back to original state
        self._movingCutscene.setActive(movingCutsceneActive)
        self.setActive(active)
        Globals.Player.Goto(mobu.FBTime(0, 0, 0, currentFrame))
        return True

    def children(self):
        """
        The children of this marker

        Return:
            list[Marker, etc.]
        """
        # Bad code to ensure children of RS Cameras don't get picked up by the moving cutscene
        if self.isCamera() or not self.marker():
            return []
        # remove stored children that are no longer a child of the marker
        children = list(self._marker.Children)
        for child in self._children.keys():
            if Component.IsDestroyed(child) or child not in children:
                self._children.pop(child)

        # add new children of the marker
        for child in children:
            if child not in self._children:
                Marker(self._movingCutscene, child.LongName, parent=self)
        return self._children.values()

    def allChildren(self):
        """ returns all the children ei. children, grandchilren, etc. of this marker """
        return self._getChildren(self)

    def addChild(self, child):
        """
        Adds a marker as a child

        Args:
            child (Marker): marker to add as as child
        """
        if child.setParent(self):
            self._children[child.marker()] = child

    def removeChild(self, child):
        """
        Removes the component as being a child

        Args:
            child (Marker): marker to remove from the child list
        """
        if child in self._children:
            self._children.pop(child)

    def parent(self):
        """ The parent marker """
        return self._parent

    def setParent(self, parent):
        """
        Sets the parent for this marker

        Args:
            parent (Marker): the marker to use as the parent
        """
        if self._parent:
            self._parent.removeChild(self)
        if isinstance(parent, Marker) and self.marker():
            self._marker.Parent = parent.marker()
            self._parent = parent
            if self._componentConstraint and not Component.IsDestroyed(self._componentConstraint):
                self._parentConstraint(self._componentConstraint, self._parent)
            return True
        elif parent is None:
            self._parent = None
        return False

    def delete(self):
        """
        Deletes the Marker.
        In the case of a camera unparents it and plots the animation to the world.
        """
        super(Marker, self).delete()

    def plot(self, editSelection=True):
        """
        Plots the component

        Args:
            editSelection (boolean): clear out the current selection so that only the component is selected for plotting
        """
        if not self._component:
            return

        if isinstance(self._component, mobu.FBCharacter):
            return Lib.Plot(self._component, "skeleton", UseConstantKeyReducer=False)

        # Unselect objects that shouldn't be plotted
        components = []
        if editSelection:
            for component in Globals.Components:
                if component.Selected and component != self._component:
                    components.append(component)
                    component.Selected = False
        # Plot the object
        Lib.PlotObjects([self._component], children=False)

        if editSelection:
            # Reselect objects that were unselected
            for component in components:
                component.Selected = True

    def reversePlot(self, active=False):
        """
        Plot the marker with the animation from the component it is driving

        Args:
            active (boolean): Should the moving cutscene be on or off when reverse plotting
        """
        marker = self.marker()
        isCamera = isinstance(marker, mobu.FBCamera)

        if marker is None:
            return

        # Setting the moving cutscene to active sets the zero constraint on
        target = self._component
        isActive = self._movingCutscene.active()
        isConstraintActive = False
        self._movingCutscene.setActive(active is False)

        if isCamera:
            # Copy the camera animation to a temporary null
            target = self.convert(delete=False)
            marker.Parent = None

        else:
            isConstraintActive = self._componentConstraint.Active
            self._componentConstraint.Active = False

        Globals.Scene.Evaluate()

        currentFrame = Globals.System.LocalTime.GetFrame()
        Globals.Player.GotoStart()

        Globals.Scene.Evaluate()

        Lib.TransferMotion(marker, target)

        if isCamera:
            # Delete the temporary null
            target.FBDelete()
        else:
            self._componentConstraint.Active = isConstraintActive

        self._movingCutscene.setActive(isActive)
        Globals.Player.Goto(mobu.FBTime(0, 0, 0, currentFrame))


    def simplePlot(self):
        """ Only set keys on the component on frames where the marker has keys """
        self._simplePlot(self.marker(), self.component(), True)

    def simpleReversePlot(self):
        """ Only set keys on the marker on frames where the camera has keys """
        self._simplePlot(self.component(), self.marker(), False)

    def _simplePlot(self, source, target, active):
        """
        Sets keys on both source and target where the source has keys.
        The position of the source is also matched by the target

        Args:
            source (pyfbsdk.FBModel): component to get frames with keys on and to match position at
            target (pyfbsdk.FBModel): component that should match frames with keys and position of the source
            active (boolean): set the state of the constraint on this Marker object
        """
        currentFrame = Globals.System.LocalTime.GetFrame()
        currentState = self.active()
        self.setActive(active)

        for frame in Lib.FramesWithKeys(source, ("Lcl Translation", "Lcl Rotation")):
            Globals.Player.Goto(mobu.FBTime(0, 0, 0, frame))
            self._key(source, target)

        Globals.Player.Goto(mobu.FBTime(0, 0, 0, currentFrame))
        self.setActive(currentState)

    def _key(self, source, target):
        """ Sets an individual key on the target based on the current position of the source """

        if Component.IsDestroyed(source) or Component.IsDestroyed(target):
            return False

        Globals.Scene.Evaluate()

        Lib.PlotOnFrame(source, frame=Globals.System.LocalTime.GetFrame())

        sourceMatrix = mobu.FBMatrix()
        source.GetMatrix(sourceMatrix)

        target.SetMatrix(sourceMatrix)
        Globals.Scene.Evaluate()

        Lib.PlotOnFrame(target, frame=Globals.System.LocalTime.GetFrame())
        return True

    def key(self):
        """ Sets an individual key on the marker based on the current position of the component """
        currentState = self.active()
        self.setActive(False)
        self._key(self.component(), self.marker())
        self.setActive(currentState)

    def clearKeys(self):
        """ Clear the keys on the marker """
        Lib.ClearAnimation(self.marker(), allProperties=True)

    def active(self):
        """ Activate state of the component constraint """
        constraint = self.constraint()
        if constraint:
            return constraint.Active

    def setActive(self, value):
        """ Set the active state of the component constraint """
        constraint = self.constraint()
        if constraint:
            constraint.Active = value

    def constraintFolder(self):
        """ The constraint folder associated with this marker """
        if self._parent:
            folder = self._getConstraintFolder(self._movingCutscene.namespaceString(), self.name())
        else:
            folder = self._movingCutscene.constraintFolder()
        return folder

    def constraint(self):
        """The parent constraint driving the component"""
        if isinstance(self._marker, (mobu.FBCamera, types.NoneType)) or Component.IsDestroyed(self._marker):
            if self._componentConstraint is not None and Component.IsDestroyed(self._componentConstraint):
                self._componentConstraint = None
            return self._componentConstraint

        if self._componentConstraint is None or Component.IsDestroyed(self._componentConstraint):
            constraint = self._getConstraint(self._marker)
            # Check that constraint found has the marker as the parent and the component as the child
            if constraint:
                isParent = self._marker in Scene.GetIncomingConnections(constraint, filterByType=(mobu.FBModel,), exactType=False)
                isChild = self._component in Scene.GetOutgoingConnections(constraint, filterByType=(mobu.FBModel,), exactType=False)
                self._componentConstraint = constraint if isParent and isChild else None

        if not self._componentConstraint and self.component() and self._movingCutscene.connect():
            # check for driver constraint and zero on creation
            if self._marker == self._movingCutscene.marker():
                self._componentConstraint = self._createConstraint(
                    parent=self._marker, child=self._component, lock=True)
                self.zeroConstraint(self._componentConstraint, self._marker)

            else:
                self._componentConstraint = self._createConstraint(parent=self._marker, child=self._component)

        return self._componentConstraint

    def driverConstraint(self):
        """The parent constraint driving the marker"""
        if isinstance(self._marker, (mobu.FBCamera, types.NoneType)) or Component.IsDestroyed(self._marker):
            if self._driverConstraint is not None and Component.IsDestroyed(self._driverConstraint):
                self._driverConstraint = None
            return self._driverConstraint

        if self._driverConstraint is None or Component.IsDestroyed(self._driverConstraint):
            constraint = self._getConstraint(self._marker, isDestination=True)
            # Check that constraint found has the marker as the parent and the driver as the child
            if constraint:
                isChild = self._marker in Scene.GetIncomingConnections(constraint, filterByType=(mobu.FBModel,), exactType=False)
                isParent = self._driver in Scene.GetOutgoingConnections(constraint, filterByType=(mobu.FBModel,), exactType=False)
                self._driverConstraint = constraint if isParent and isChild else None

        if not self._driverConstraint and self._driver and self._movingCutscene.connect():
            self._driverConstraint = self._createConstraint(self._driver, self._marker, lock=True)
            self.zeroConstraint(self._driverConstraint, self._driver)

        return self._driverConstraint

    def hasConstraint(self):
        """ The parent constraint driving the component """
        constraint = self.constraint() or self.driverConstraint()
        return constraint is not None

    def movingCutscene(self):
        """ The MovingCutscene object that this marker belongs to """
        return self._movingCutscene

    def _createConstraint(self, parent, child, lock=False, active=True, offset=False):
        """
        Creates a parent constraint

        Args:
            parent (pyfbsdk.FBComponent): the object that is the parent
            child (pyfbsdk.FBComponent): the object that is to be driven
            lock (boolean): lock the constraint
            active (boolean): activate the constraint
        """
        namespace = self._movingCutscene.namespaceString()
        isParentCharacter = isinstance(parent, mobu.FBCharacter)
        isCharacter = isinstance(child, mobu.FBCharacter)

        if isParentCharacter:
            parent = Lib.GetControlRigReferenceNull(parent)
            if parent is None:
                return None

        if isCharacter:
            child = Lib.GetControlRigReferenceNull(child)
            if not child:
                return None
            childMatrix = mobu.FBMatrix()
            child.GetMatrix(childMatrix)

        name = "{}:{}_CONSTRAINT".format(namespace, parent.Name)
        constraint = Constraint.Constraint(Constraint.PARENT_CHILD, name,
                                           child, parent, Active=active, Lock=lock)
        if isCharacter:
            child.SetMatrix(childMatrix)
            Globals.Scene.Evaluate()

        self.constraintFolder().ConnectSrc(constraint)
        return constraint

    def zeroConstraint(self, constraint, parent):
        """
        Zeros a constraint's offset

        Args:
            constraint (pyfbsdk.FBConstraint): constraint to zero
            parent (pyfbsdk.FBModel): parent of constraint
        """
        # Set offsets and "zero" the constraint
        constraint.PropertyList.Find(
            ".".join([parent.LongName, "Offset T"])).Data = mobu.FBVector3d(0, 0, 0)
        constraint.PropertyList.Find(
            ".".join([parent.LongName, "Offset R"])).Data = mobu.FBVector3d(0, 0, 0)
        constraint.PropertyList.Find(
            ".".join([parent.LongName, "Offset S"])).Data = mobu.FBVector3d(1, 1, 1)

    def match(self, component=None, isNull=False, popup=True):
        """
        Matches the marker to the position of the driver of the moving cutscene

        Args:
            component (mobu.FBModel): component whose position should be matched
            isNull (bool): Whether the component is a null to match to that is static
            popup (bool): If True will show popups
        """
        with ContextManagers.SuspendCallbacks():
            driver = self._movingCutscene.driver()

            if not driver:
                return False

            component = component or self.component()
            marker = self.marker()

            if component is None and marker:
                Lib.MatchPosition(driver, [marker])
                marker.PropertyList.Find("Lcl Scaling").Data = mobu.FBVector3d(1, 1, 1)
                return True

            # Search for pre-defined components that take precedence within the namespace of the component
            if isinstance(component, mobu.FBCharacter):

                # check for control rig add warning if exists
                hasControlRig = Lib.HasControlRig(component)
                if hasControlRig and component.ActiveInput:

                    result = 0
                    if popup:
                        editMsgBox = PlotEditMessageBox()
                        result = editMsgBox.exec_()

                    if result == 2:
                        raise RuntimeError("Cancelled.")

                    elif result == 0:

                        # check for layers
                        currentTake = Globals.System.CurrentTake
                        if currentTake.GetLayerCount() > 1:
                            plotMsgBox = PlotLayersMessageBox()
                            if plotMsgBox.exec_() == 1:
                                raise RuntimeError("Cancelled.")

                        Lib.Plot(component, "skeleton", CreateControlRig=False, AllTakes=True)

                # Always set to skeleton to bake
                Lib.ActivateControlRig(component, active=False)
                controlRigNull = Lib.GetControlRigReferenceNull(component)
                constraint = self.constraint()

                # De-activate the constraints and match the position of the reference null to master null
                constraint.Active = False
                Lib.MatchPosition(marker, [controlRigNull])

                # Set offsets and "zero" the constraint then re-activate and plot to rig
                constraint.PropertyList.Find("{}.{}".format(marker.LongName, "Offset T")).Data = mobu.FBVector3d(0, 0, 0)
                constraint.PropertyList.Find("{}.{}".format(marker.LongName, "Offset R")).Data = mobu.FBVector3d(0, 0, 0)
                constraint.PropertyList.Find("{}.{}".format(marker.LongName, "Offset S")).Data = mobu.FBVector3d(1, 1, 1)
                constraint.Active = True

                componentNull = self._getCharacterComponent(component) or Lib.GetControlRigReferenceNull(component)

                result = 0
                if popup:
                    reversePlotMsgBox = ReversePlotMessageBox()
                    result = reversePlotMsgBox.exec_()

                if result == 0:
                    # make temp nulls to reverse plot
                    tempNullOffset = mobu.FBModelNull("TMP__NULL__OFFSET")
                    tempNullMarker = mobu.FBModelNull("TMP__NULL__MARKER")

                    tempNullMarker.Parent = tempNullOffset

                    # parent offset null to parent dropped to (such as MASTER_CHARACTER)
                    if marker.Parent:
                        tempNullOffset.Parent = marker.Parent

                    # set same position as parent
                    tempNullOffset.Translation.Data = mobu.FBVector3d(0, 0, 0)
                    tempNullOffset.Rotation.Data = mobu.FBVector3d(0, 0, 0)
                    Globals.Scene.Evaluate()

                    # transfer anim from component in parent space
                    Lib.TransferMotion(tempNullMarker, controlRigNull)

                    # transfer that to marker to reverse
                    Lib.TransferMotion(marker, tempNullMarker)

                    # delete nulls that are temp
                    tempNullMarker.FBDelete()
                    tempNullOffset.FBDelete()

                else:
                    Lib.Plot(component, "rig", UseConstantKeyReducer=False)

                    scale = marker.PropertyList.Find("Lcl Scaling").Data

                    # match position
                    Lib.MatchPosition(componentNull, [marker])

                    # Remove Scaling Artifacts from matching matricies
                    marker.PropertyList.Find("Lcl Scaling").Data = scale

                return True

            elif component and marker:
                scale = marker.PropertyList.Find("Lcl Scaling").Data

                # match position
                Lib.MatchPosition(component, [marker])

                # if it has keys copy this over
                if Lib.HasKeyframes(component) and not isNull:

                    result = 0
                    if popup:
                        editMsgBox = PlotComponentMessageBox(component.Name)
                        result = editMsgBox.exec_()

                    if result == 2:
                        raise RuntimeError("Cancelled.")

                    elif result == 0:
                        # make temp nulls to copy values additively in the parent space that it was dropped
                        tempNullOffset = mobu.FBModelNull("TMP__NULL__OFFSET")
                        tempNullMarker = mobu.FBModelNull("TMP__NULL__MARKER")
                        tempNullMarker.Parent = tempNullOffset

                        # transfer anim to temp marker, then parent to offset
                        Lib.TransferMotion(tempNullMarker, component)
                        tempNullMarker.Parent = tempNullOffset

                        # parent offset null to parent dropped to (such as MASTER_PROP)
                        if marker.Parent:
                            tempNullOffset.Parent = marker.Parent

                        # transfer anim to marker as it'll be in the space of the parent
                        Lib.TransferMotion(marker, tempNullMarker)

                        # delete nulls that are temp
                        tempNullMarker.FBDelete()
                        tempNullOffset.FBDelete()

                # Remove Scaling Artifacts from matching matricies
                marker.PropertyList.Find("Lcl Scaling").Data = scale
                return True

        return False

    def _getConstraint(self, component, isDestination=False):
        """
        Get the constraint connected to the component

        Args:
            component (pyfbsdk.FBComponent): component to get connected constraint for
            isDestination (boolean): is the component the destination for the constraint or the source

        Return:
            None or pyfbsdk.FBConstraint
        """
        if isDestination:
            constraints = Scene.GetIncomingConnections(component, filterByType=[mobu.FBConstraint])
        else:
            constraints = Scene.GetOutgoingConnections(component, filterByType=[mobu.FBConstraint])
        if constraints:
            return constraints[0]

    def _validateComponent(self, component, constraint=None, isDestination=False):
        """
        Validates that the component and constraint exists in the scene

        Args:
            component (pyfbsdk.FBComponent): component to validate
            constraint (pyfbsdk.FBConstraint): constraint to validate
            isDestination (boolean): when checking the constraints connected to the component, is the component the
                            destination for the constraint or the source
        Return:
            list[pyfbsdk.FBComponent, pyfbsdk.FBConstraint]
        """
        # Get Constraints attached to the marker
        if (constraint is None or Component.IsDestroyed(constraint)) and self.marker() is not None:
            constraint = self._getConstraint(self._marker, isDestination=isDestination)

        if constraint is None:
            component = None
        else:
            # Get the parent child constraint
            # The child is always the first reference
            _component = constraint.ReferenceGet(0)
            # Get the child
            if _component is not None:
                component = _component

        character = Lib.GetCharacterFromControlRigReferenceNull(component)
        component = character if character else component
        return component, constraint

    def convert(self, delete=True):
        """ Converts the marker to a camera and vice versa """
        if not self.marker():
            return

        currentMarker = self._marker
        currentMarkerName = self._marker.Name
        # check if camera is the marker or not
        if not self.isCamera():
            constraint = self.constraint()
            newMarker = self.component()
            if constraint:
                self._componentConstraint = None
                self._driverConstraint = None
                constraint.FBDelete()
        else:
            newMarker = mobu.FBModelMarker("{}:{}".format(
                self.movingCutscene().namespaceString(), currentMarkerName))
            newMarker.Size = 5000
            newMarker.PropertyList.Find("LookUI").Data = 1
            newMarker.Visibility = False

        newMarker.Parent = currentMarker.Parent
        self._marker = newMarker

        if delete:
            Lib.TransferMotion(newMarker, currentMarker)
            currentMarker.FBDelete()
        else:
            Lib.TransferMotion(newMarker, currentMarker)
            currentMarker.Parent = None
            self._component = currentMarker
            self._componentConstraint = self._createConstraint(parent=self._marker, child=self._component)

        return newMarker

    def isCamera(self):
        """ Is this marker a camera """
        return isinstance(self._marker, mobu.FBCamera)


class PlotEditMessageBox(QtGui.QMessageBox):
    """Message box for plotting edits"""

    def __init__(self, parent=None):
        parent = Application.GetMainWindow() if not parent else parent
        super(PlotEditMessageBox, self).__init__(parent)

        self.setWindowTitle("PLOT EDITS FROM CONTROL RIG?")
        self.setText("\nThere is an active control rig on this character.\n\n"
                     "Are there rig edits that need to be kept?\n\n")
        yesButton = self.addButton("Yes, plot rig edits", QtGui.QMessageBox.YesRole)
        noButton = self.addButton("No, use skeleton data", QtGui.QMessageBox.NoRole)
        cancelButton = self.addButton("Cancel", QtGui.QMessageBox.RejectRole)
        self.setIcon(QtGui.QMessageBox.Warning)


class PlotLayersMessageBox(QtGui.QMessageBox):
    """Message box for plotting layers"""

    def __init__(self, parent=None):
        parent = Application.GetMainWindow() if not parent else parent
        super(PlotLayersMessageBox, self).__init__(parent)

        self.setWindowTitle("PLOT LAYERS?")
        self.setText("All layer data for this ped will be plotted.")
        plotYesButton = self.addButton('Proceed', QtGui.QMessageBox.YesRole)
        plotCancelButton = self.addButton('Cancel', QtGui.QMessageBox.RejectRole)
        self.setIcon(QtGui.QMessageBox.Warning)


class PlotComponentMessageBox(QtGui.QMessageBox):
    """Message box for plotting edits oon component"""

    def __init__(self, componentName, parent=None):
        parent = Application.GetMainWindow() if not parent else parent
        super(PlotComponentMessageBox, self).__init__(parent)

        self.setWindowTitle("PLOT EDITS FROM COMPONENT?")
        self.setText("\nThere are keyframes on the component {}.\n\n"
                     "Are there edits that need to be kept?\n\n".format(componentName))
        yesButton = self.addButton("Yes, plot edits", QtGui.QMessageBox.YesRole)
        noButton = self.addButton("No, do not plot edits", QtGui.QMessageBox.NoRole)
        cancelButton = self.addButton("Cancel", QtGui.QMessageBox.RejectRole)
        self.setIcon(QtGui.QMessageBox.Warning)


class ReversePlotMessageBox(QtGui.QMessageBox):
    """Message box asking if you'd like to reverse plot the animation onto the character to remove double transformations"""

    def __init__(self, parent=None):
        parent = Application.GetMainWindow() if not parent else parent
        super(ReversePlotMessageBox, self).__init__(parent)

        self.setWindowTitle("REVERSE PLOT FROM MOVING CUTSCENE?")
        self.setText("Would you like to reverse plot your character to the moving cutscene rig?")
        yesButton = self.addButton("Yes, reverse plot", QtGui.QMessageBox.YesRole)
        noButton = self.addButton("No, do NOT reverse plot", QtGui.QMessageBox.NoRole)
        self.setIcon(QtGui.QMessageBox.Warning)
