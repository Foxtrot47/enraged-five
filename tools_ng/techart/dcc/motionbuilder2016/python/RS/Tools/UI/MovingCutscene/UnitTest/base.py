import os

import unittest
import pyfbsdk as mobu

from RS import Globals, Perforce
from RS.Unittests import utils
from RS.Core.Animation import Lib
from RS.Core.ReferenceSystem.UnitTest import base
from RS.Tools.UI.MovingCutscene import const as uiConst
from RS.Tools.UI.MovingCutscene.UnitTest import const
from RS.Tools.UI.MovingCutscene.Model import Components
from RS.Utils.Scene import Component


class CutsceneBase(base.Base):
    """
    Base class for the MovingCutscene tests, it inherits the base UnitTest class from the Reference System Unit Tests
    so it can reference assets to use for tests
    """

    KEY = "assets"

    def setUp(self):
        """Clears the scene before running the test"""
        Globals.Application.FileNew()

    def tearDown(self):
        """Clears the scene after running the test"""
        Globals.Application.FileNew()

    def test_Cutscene(self):
        """Test importing of moving cutscene"""

        self.KEY = self.KEY if self.KEY else self.__class__.__name__.split("_")[-1]

        # get data
        cutscenesConfig = utils.loadYaml(const.ConfigPathDict.CUTSCENES_CONFIG_PATH)
        self.cutsceneData = utils.getConfigKey(self.KEY, cutscenesConfig)

        if not self.cutsceneData:
            print("No config key found for {} to test asset".format(self.KEY))
            return False

        # get asset paths
        self.vehiclePath = self.cutsceneData.get("vehiclePath", None)
        self.vehicleMarker = self.cutsceneData.get("vehicleMarker", None)

        self.characterPath = self.cutsceneData.get("characterPath", None)
        self.characterMarker = self.cutsceneData.get("characterMarker", None)

        self.propPath = self.cutsceneData.get("propPath", None)
        self.propMarker = self.cutsceneData.get("propMarker", None)

        self.mocapPath = self.cutsceneData.get("mocapPath", None)
        self.mocapMarker = self.cutsceneData.get("mocapMarker", None)

        self.driverName = self.cutsceneData.get("driverName", None)

        # make a tuple of paths, filtering None
        paths = tuple(filter(None, [self.vehiclePath, self.characterPath, self.propPath]))

        # sync all the paths
        for path in paths:
            Perforce.Sync(path, force=True)
            if not os.path.exists(path):
                self.fail(
                    "{} path doesn't exist! Manually sync this file or edit the cutscenes.yaml config".format(path)
                )

        return True

    def assertExists(self, name):
        """Asserts that a model exists

        Args:
            name (str): Name of model to assert

        Returns:
            mobu.FBModel: The model found by label name
        """
        model = mobu.FBFindModelByLabelName(name)
        self.assertIsNotNone(model)
        return model

    def assertMarker(self, movingCutscene, markerName):
        """Test for marker

        Args:
            movingCutscene (Components.MovingCutscene): Cutscene object
            markerName (str): Name of marker source

        Returns:
            Components.Marker: The marker object
        """
        markerComponent = self.assertExists(markerName)

        marker = Components.Marker(movingCutscene, markerName, movingCutscene.masterNull(), match=True)
        marker.setComponent(markerComponent)

        return marker

    def assertCutscene(self, driverName):
        """Create or get moving cutscene based on driver name

        Args:
            driverName (str): Name of model to assert
        """
        self.driver = self.assertExists(driverName)

        allCutscenes = Components.MovingCutscene.all()
        movingCutscene = None

        for cutscene in allCutscenes:
            if cutscene.driverName() == self.driver:
                movingCutscene = cutscene
                break

        if not movingCutscene:
            # create cutscene
            movingCutscene = Components.MovingCutscene(driver=self.driver)
            movingCutscene.transferMotion()

            masternull = movingCutscene.masterNull()
            zeronull = movingCutscene.zeroNull()
            freezenull = movingCutscene.freezeNull()
            freezenull.match(self.driver)

            # transfer motion onto zeronull
            Lib.TransferMotion(zeronull.marker(), masternull.marker())

            Component.SetProperties(masternull.marker(), **{"Scaling (Lcl)": mobu.FBVector3d(1, 1, 1)})

            Lib.PlotOnFrame(self.driver, inverse=True)
            for name in uiConst.Nulls.TOP_LEVEL_CHILDREN:
                component = Components.Marker(movingCutscene, name, masternull, match=True)
                marker = component.marker()
                Lib.PlotOnFrame(marker, frame=0)

        return movingCutscene

    def assertTransforms(self, movingCutscene, component):
        """Asserts that transforms are not double transforming on a movingcutscene

        Args:
            movingCutscene (Components.MovingCutscene): The moving cutscene to test
            component (mobu.FBModel): The component to test
        """
        # get master mover
        masterMover = movingCutscene.masterNull().marker()

        # get positions of mover and component
        startComponentPos = mobu.FBVector3d()
        component.GetVector(startComponentPos, mobu.FBModelTransformationType.kModelTranslation)
        startMoverPos = mobu.FBVector3d()
        masterMover.GetVector(startMoverPos, mobu.FBModelTransformationType.kModelTranslation)

        # make an offset on mover
        offsetPos = mobu.FBVector3d(10, 10, 10)
        resultPosition = startMoverPos + offsetPos
        masterMover.Translation = resultPosition

        # get new position
        Globals.Scene.Evaluate()
        endComponentPos = mobu.FBVector3d()
        component.GetVector(endComponentPos, mobu.FBModelTransformationType.kModelTranslation)
        self.assertEquals((endComponentPos - offsetPos), startComponentPos)
