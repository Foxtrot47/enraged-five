"""
Dialog for the Mocap Trial Notes Widget
"""
import os

import pyfbsdk as mobu

from PySide import QtGui

from RS import Config, Globals
from RS.Tools import UI
from RS.Core.AnimData import Context
from RS.Core.AnimData.Widgets import contextWidget
from RS.Core.ReferenceSystem import Manager


class WatchstarTrialDialog(UI.QtMainWindowBase):
    """
    GUI Tool for getting and setting the trial IDs
    """
    def __init__(self):
        """
        Constructor
        """
        self._toolName = "Watchstar Trial Tool"
        super(WatchstarTrialDialog, self).__init__(title=self._toolName, dockable=True)

        self.setupUi()
        self._registerCallback()

    def setupUi(self):
        """
        Set up the UI and attach the connections
        """
        # Layouts
        layout = QtGui.QGridLayout()

        # Widgets
        mainWidget = QtGui.QWidget()
        banner = UI.BannerWidget(self._toolName, helpUrl=r"")
        self._currentTake = QtGui.QLineEdit()
        self._trialName = QtGui.QLineEdit()
        setTrialButton = QtGui.QPushButton("Set Trial")
        self._contextWidget = contextWidget.ContextColumnView()

        # Configure Widgets
        self.setupToolBar(banner)
        self._contextWidget.setStyleSheet("color: #ffffff;")
        self._contextWidget.setFixedCoulmnCount(4)
        self._trialName.setReadOnly(True)
        self._currentTake.setReadOnly(True)
        
        # Add Widgets to Layout
        layout.addWidget(banner, 0, 0, 1, 2)
        layout.addWidget(QtGui.QLabel("Take:"), 1, 0)
        layout.addWidget(self._currentTake, 1, 1)
        layout.addWidget(QtGui.QLabel("Trial:"), 2, 0)
        layout.addWidget(self._trialName, 2, 1)
        layout.addWidget(self._contextWidget, 3, 0, 1, 2)
        layout.addWidget(setTrialButton, 4, 0, 1, 2)
        
        # Set Layouts to Widgets
        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)
        
        # Connect Signals
        setTrialButton.pressed.connect(self._handleSetTrialButton)
        
        self._updateTakeLogic()

    def closeEvent(self, event):
        """
        Overrides built-in method.
        Removes callbacks when UI is closed

        Arguments:
            event (QtGui.QEvent): the event being triggered by Qt
        """
        self._unregisterCallback()

    def setupToolBar(self, banner):
        """
        Adds icon to the banner

        Arguments:
            banner (RS.Tools.UI.QBanner): banner to add tool buttons to
        """
        refreshIconPath = os.path.join(Config.Script.Path.ToolImages, "ReferenceEditor", "Refresh.png")
        refreshButton = QtGui.QIcon(QtGui.QPixmap(refreshIconPath))
        banner.addButton(refreshButton , "Refreshes the Virtual Production Toolbox", self._handleRefreshButton)

    def _updateTakeLogic(self):
        """
        Internal Method
        
        Update logic to get the Trial ID from the current take and set it into the view widget
        """
        self._trialName.setText("No Trial")
        self._trialName.setText("No Take")
        curTake = Globals.System.CurrentTake
        if curTake is None:
            return
        self._currentTake.setText(str(curTake.Name))
        trialID = Manager.Manager().TrialID
        if trialID not in [-1, 0, None]:
            trial = Context.animData.getTrialByID(trialID)
            self._trialName.setText(str(trial))
            self._contextWidget.setContext(trial, setFitlerText=False)

    def _handleRefreshButton(self):
        """
        Internal Method
        
        Handle the refresh button being pressed
        """
        self._updateTakeLogic()
    
    def _handleSetTrialButton(self):
        """
        Internal Method
        
        Handle the Set Trial button being pressed
        """
        selectedTrials = self._contextWidget.getSelectedTrials()
        if len(selectedTrials) == 0:
            QtGui.QMessageBox.warning(self, self._toolName, "No Trial Selected")
            return
        elif len(selectedTrials) > 1:
            QtGui.QMessageBox.warning(self, self._toolName, "Too many Trials Selected")
            return
        trial = selectedTrials[0]
        self._trialName.setText(str(trial))
        Manager.Manager().TrialID = trial.id
    
    def _handleTakeChange(self, control=None, event=None):
        """
        Overrides inherited method.
        
        Handle the take change

        Arguments:
            control (obejct): Motion Builder object calling this method
            event (pyfbsdk.FBEvent): Motion Builder event that triggered this method
        """
        self._updateTakeLogic()

    def _registerCallback(self):
        """
        Adds callback to update the model whenever the takes change in the scenee
        """
        Globals.Callbacks.OnTakeChange.Add(self._handleTakeChange)

    def _unregisterCallback(self):
        """
        Removes callback to update the model whenever the takes change in the scenee
        """
        Globals.Callbacks.OnTakeChange.Remove(self._handleTakeChange)


def Run(show=True):
    notesDialog = WatchstarTrialDialog()

    if show:
        notesDialog.show()

    return notesDialog
