from PySide import QtCore

from RS import Globals
from RS.Tools.UI.Mocap.Models import characterModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types import MocapGsSkeleton


class CharacterModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    
    _refTypes = (MocapGsSkeleton.MocapGsSkeleton)
    
    def __init__(self, parent=None):
        self._checkable = True
        self._manager = Manager.Manager()
        super(CharacterModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ["GS Name", "Character Hidden", "Reference Pose Gender", "Colour"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def _getReferenceItems(self):
        """
        Internal Method
        
        get all the relevant reference system items
        
        returns:
            list of Reference System objects
        """
        return [refItem for refItem in self._manager.GetReferenceListAll() if isinstance(refItem, self._refTypes)]

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        for refItem in self._getReferenceItems():
            parent.appendChild(characterModelItem.CharacterTextModelItem(refItem, parent=parent))

    def flags(self, index):
        """
        flags that tell the view how to handle the items
        Arguments:
            index (QtCore.QModelIndex): model index for the current item
        """
        column = index.column()
        if column in [2,3]:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable

        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
