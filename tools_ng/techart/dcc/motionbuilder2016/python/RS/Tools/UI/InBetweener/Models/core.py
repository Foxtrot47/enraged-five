"""
Functions that include the method to create an in-between keyframe of any object(s) based off user input and methods to
help gather the information necessary to do so
"""
import pyfbsdk as mobu

from RS import Globals
from RS.Core.Animation import Lib
from RS.Tools.UI.InBetweener import const


def getKeyingMode(characterNode=None):
    """
    Find the current keying mode

    Keyword Args:
        characterNode (pyfbsdk.FBCharacter): Character as specified by ComponentComboBox

    Returns:
        pyfbsdk.FBCharacter.KeyingMode
    """
    # Because the tool can be run in a scene without characters and it looks for the current keying mode during instantiation,
    # this function should check if a character actually exists in the scene
    if characterNode:
        return characterNode.KeyingMode


def setKeyingMode(characterNode, keyModeString):
    """
    Sets the keying mode to full, body part, or selection

    Args:
        characterNode (pyfbsdk.FBCharacter): Character as specified by ComponentComboBox
        keyModeString (str): Full, Part, Selection, or None
    """
    if const.KeyMode.FULL in keyModeString:
        characterNode.KeyingMode = mobu.FBCharacterKeyingMode.kFBCharacterKeyingFullBody
    elif const.KeyMode.PART in keyModeString:
        characterNode.KeyingMode = mobu.FBCharacterKeyingMode.kFBCharacterKeyingBodyPart
    elif const.KeyMode.SELECTION in keyModeString:
        characterNode.KeyingMode = mobu.FBCharacterKeyingMode.kFBCharacterKeyingSelection


def getCharacterNode(characterString):
    """
    Get a characters model from a string.

    Args:
        characterString (str): Character as specified by text from ComponentComboBox

    Returns:
        pyfbsdk.FBCharacter
    """
    for character in Globals.Characters:
        if character.LongName == characterString:
            return character


def getComponents(characterString, part, side):
    """
    Filters out controls/components of a specified character based on body part/location and side (if applicable)

    Args:
        characterString (str): Character as specified by current text from ComponentComboBox
        part (str): Body part
        side (str): Left, Right, or None

    Returns:
        list: List of pyfbsdk.FBModel objects
    """
    partsList = []
    if part == const.Parts.ALL:
        for character in Globals.Characters:
            if character.LongName == characterString:
                partsList = Lib.GetControlsFromCharacter(character)
    else:
        for character in Globals.Characters:
            if character.LongName == characterString:
                controls = Lib.GetControlsFromCharacter(character)
                for filterName in const.Parts.getFilters(part):
                    partsList.extend([ctrl for ctrl in controls if filterName in ctrl.Name and ctrl.Name.startswith(side)])
                # Since the string "Hand" is included in most finger controls, we add it here explicitly
                if part == const.Parts.ARM:
                    partsList.extend([ctrl for ctrl in controls if ctrl.Name.endswith(const.Parts.HAND) and ctrl.Name.startswith(side)])
    return partsList


def setInBetween(bias, components):
    """
    Takes in an int value used to calculate and set the in-between of a new keyframe.

    Args:
        bias (int): Value as set from the widget by the user
        components (list): List of pyfbsdk.FBModel objects to set the in-between keyframe on
    """
    # Save the current time
    currentTime = Globals.System.LocalTime
    currentFrame = Globals.System.LocalTime.GetFrame()

    for component in components:
        # Iterate through XYZ of the TRS nodes if there is something keyed
        for transform in [component.Translation.GetAnimationNode(), component.Rotation.GetAnimationNode(), component.Scaling.GetAnimationNode()]:
            # Check to make sure the transform is keyed
            if transform:
                # We need to grab the fCurve data for each XYZ channel of each transform since keyframes can
                # vary for each individual axis of each transform
                for axis in xrange(3):
                    fCurve = transform.Nodes[axis].FCurve
                    if len(fCurve.Keys) > 1:
                        # Start the before index at 0 and the after index at an unrealistic high for iteration purposes
                        beforeKeyIndex = 0
                        afterKeyIndex = 10000000000

                        # Store all keyframes for the current transform
                        allKeys = []
                        for keyIndex in xrange(len(fCurve.Keys)):
                            thisFrame = fCurve.Keys[keyIndex].Time.GetFrame()
                            allKeys.append(thisFrame)

                        # Find the closest keyframe to the current frame
                        closestKeyframe = min(allKeys, key=lambda x:abs(x-currentFrame))

                        # If the closest keyframe is the current frame, we just have to get the index before and after
                        # the current so long as it is not the first or last keyframe in the keyframe list (in which
                        # case it would not be an in-between)
                        if currentFrame == closestKeyframe and allKeys.index(closestKeyframe) != 0 and allKeys.index(closestKeyframe) + 1 != len(allKeys):
                            beforeKeyIndex = allKeys.index(closestKeyframe) - 1
                            afterKeyIndex = allKeys.index(closestKeyframe) + 1
                        else:
                            # If the closest keyframe is NOT the current frame, grab the before and after key indexes
                            # and make sure that the current frame is not going to be the first or last keyframe
                            closestKeyframeIndex = allKeys.index(closestKeyframe)
                            if closestKeyframe < currentFrame and allKeys.index(closestKeyframe) + 1 != len(allKeys):
                                beforeKeyIndex = closestKeyframeIndex
                                afterKeyIndex = closestKeyframeIndex + 1
                            elif closestKeyframe > currentFrame and allKeys.index(closestKeyframe) != 0:
                                beforeKeyIndex = closestKeyframeIndex - 1
                                afterKeyIndex = closestKeyframeIndex

                        # If the after index value is not equal to the initial value, we know that it has changed
                        # and that the control is able to be manipulated with an in-between
                        if afterKeyIndex != 10000000000:
                            beforeKey = fCurve.Keys[beforeKeyIndex]
                            afterKey = fCurve.Keys[afterKeyIndex]

                            # Calculate the value of the in-between based off the user input (based from "Tween Machine" with cm to m "fix")
                            newKeyValue = (beforeKey.Value + ((afterKey.Value - beforeKey.Value) * bias / 100))

                            # Add a new keyframe at the current frame
                            newKey = fCurve.KeyAdd(currentTime, 0.0)
                            newKey = fCurve.Keys[newKey]

                            # Plug in the new value and set all key settings based on previous keyframe
                            newKey.Value = newKeyValue
                            newKey.Interpolation = beforeKey.Interpolation
                            newKey.TangentMode = beforeKey.TangentMode
                            newKey.TangentConstantMode = beforeKey.TangentConstantMode
                            newKey.LeftDerivative = beforeKey.LeftDerivative
                            newKey.RightDerivative = beforeKey.RightDerivative
                            newKey.LeftTangentWeight = beforeKey.LeftTangentWeight
                            newKey.RightTangentWeight = beforeKey.RightTangentWeight

    # Refresh the timeline to visually see the update
    Globals.Player.Goto(currentTime + mobu.FBTime(0, 0, 0, 1, 0))
    Globals.Player.Goto(currentTime)
