from PySide import QtGui, QtCore

import os

import pyfbsdk as mobu

from RS.Tools.UI.Mocap import const
from RS import Config, Globals, Perforce
from RS.Utils import Scene
from RS.Tools.UI.Mocap.Models import motionImportModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class MotionImportModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(MotionImportModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ['GS Asset', 'Clean Motion Imported']

    def _getGSAssetDict(self):
        '''
        Goes through the scene and selects the root node of each gs asset. Adds these to a list.
        
        Returns:
            List (FBModels): list of selected FBModel items
        '''
        assetInfoList = []
        gsSlateInfoDict = {}
        gsCharacterInfoDict = {}
        gsPropInfoDict = {}
        # get slate
        slateString = 'slate_bone'
        for component in Globals.Components:
            if slateString == component.Name.lower() and isinstance(component, mobu.FBModel):
                # check if component already has clean data applied
                cleanDataPathProperty = component.PropertyList.Find('Clean Data Path')
                cleanDataNameProperty = component.PropertyList.Find('Clean Data Name')
                if cleanDataPathProperty:
                    # add info to dict
                    gsSlateInfoDict[component.LongName] = component, cleanDataPathProperty.Data, cleanDataNameProperty.Data, const.GSModelTypes.Slate
                else:
                    # add info to dict
                    gsSlateInfoDict[component.LongName] = component, "", "", const.GSModelTypes.Slate

        # get gs skels
        gsSkelString = 'skel_root'
        gsSkelPropertyString = 'RTSource'
        gsSkelPropertyString2 = 'GiantAnim'
        gsSkelPropertyString3 = 'character match namespace'
        for component in Globals.Components:
            if gsSkelString in component.Name.lower() and isinstance(component, mobu.FBModel):
                rtSourceProperty = component.PropertyList.Find(gsSkelPropertyString)
                rtSourceProperty3 = component.PropertyList.Find(gsSkelPropertyString3)
                if rtSourceProperty or rtSourceProperty3:
                    parentOffset = component.Parent
                    # double check parentOffset is the top of the hierarchy, if it isnt, it has
                    # picked a non-gs model and ignore it!
                    rootParent = Scene.GetParent(component)
                    if parentOffset != rootParent:
                        continue
                    if parentOffset and parentOffset.Children[0]:
                        # check if component already has clean data applied
                        cleanDataPathProperty = parentOffset.Children[0].PropertyList.Find('Clean Data Path')
                        cleanDataNameProperty = parentOffset.Children[0].PropertyList.Find('Clean Data Name')
                        if cleanDataPathProperty:
                            # add info to dict
                            gsCharacterInfoDict[parentOffset.LongName] = parentOffset.Children[0], cleanDataPathProperty.Data, cleanDataNameProperty.Data, const.GSModelTypes.Character
                        else:
                            # add info to dict
                            gsCharacterInfoDict[parentOffset.LongName] = parentOffset.Children[0], "", "", const.GSModelTypes.Character

        # get gs props
        propToyString = 'prop'
        weaponString = 'gun_'
        nullString = '_null'
        moverString = '_mover'
        for component in Globals.Components:
            gsComponentFound = False
            # check if a gs prop exists
            if component.Name.lower().startswith(propToyString) and nullString in component.Name.lower() and isinstance(component, mobu.FBModel):
                gsComponentFound = True
            # check if a gs weapon exists
            if component.Name.lower().startswith(weaponString) and nullString in component.Name.lower() and isinstance(component, mobu.FBModel):
                gsComponentFound = True
            # check if a gs weapon exists (old naming)
            if component.Name.lower().startswith(weaponString) and moverString in component.Name.lower() and isinstance(component, mobu.FBModel):
                gsComponentFound = True
            # if gs assets exist, show details in the ui
            if gsComponentFound == True:
                print component.LongName
                if len(component.Children) > 0:
                    # check if component already has clean data applied
                    cleanDataPathProperty = component.Children[0].PropertyList.Find('Clean Data Path')
                    cleanDataNameProperty = component.Children[0].PropertyList.Find('Clean Data Name')
                    if cleanDataPathProperty:
                        # add info to dict
                        gsPropInfoDict[component.LongName] = component.Children[0], cleanDataPathProperty.Data, cleanDataNameProperty.Data, const.GSModelTypes.Prop
                    else:
                        # add info to dict
                        gsPropInfoDict[component.LongName] = component.Children[0], "", "", const.GSModelTypes.Prop

        # add dicts to a list
        assetInfoList.append(gsSlateInfoDict)
        assetInfoList.append(gsCharacterInfoDict)
        assetInfoList.append(gsPropInfoDict)
        return assetInfoList

    def _getCurrentTakeName(self):
        '''
        Get current take name

        Return:
            string: The name of the current take
        '''
        return mobu.FBSystem().CurrentTake.Name

    def _getFinalFBXFolderPath(self):
        '''
        Get default file path specific to the current scene. This file path will point towards the
        correct final fbx folder, so make it easier for the user to select the files needed for
        importing. If a match is not found, the default path will just point towards the root of 
        fbx_final folder. This method will also get latest on the files in the returned folder.

        Return:
            string: file path
        '''
        # get current take name and matxch to subfolder name
        currentTakeName = self._getCurrentTakeName()

        # get project name
        project = str(Config.Project.Name).lower()
        gsPath = None
        # paths to gs skel files
        if project == 'rdr3':
            gsPath = os.path.join("x:\\",
                                    "projects",
                                    "bob",
                                    "fbx_final")
        elif project == 'gta5':
            gsPath = os.path.join("x:\\",
                                    "projects",
                                    "paradise_dlc",
                                    "fbx_final",
                                    "*")
        if not gsPath:
            return

        # find
        defaultPath = None
        srcFolder = os.path.normpath(gsPath)
        for (root, directories, files) in os.walk(srcFolder):
                splitRoot = root.split('\\')
                subFolderName = splitRoot[-1]
                if subFolderName == currentTakeName:
                    defaultPath = root
                    break

        if not defaultPath:
            defaultPath = gsPath
            return defaultPath

        # get latest on files in folder
        updateFileList = []
        for (root, directories, files) in os.walk(srcFolder):
            for eachFile in files:
                if root == defaultPath:
                    updateFileList.append(os.path.join(root, eachFile))
                    continue

        for item in updateFileList:
            Perforce.Sync(item)

        return defaultPath

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        defaultPath = self._getFinalFBXFolderPath()
        assetInfoList = self._getGSAssetDict()
        for assetDictionary in assetInfoList:
            for key, value in assetDictionary.iteritems():
                dataItem = motionImportModelItem.MotionImportTextModelItem(key,
                                                                           value[0],
                                                                           defaultPath,
                                                                           value[1],
                                                                           value[2],
                                                                           value[3],
                                                                           parent=parent)
                # append tree view with item
                parent.appendChild(dataItem)

    def flags(self, index):
        '''
        Flags are added to determine column properties
        
        Args:
            index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        column = index.column()

        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        if column == 0:
            return flags | QtCore.Qt.ItemIsUserCheckable
        else:
            return flags