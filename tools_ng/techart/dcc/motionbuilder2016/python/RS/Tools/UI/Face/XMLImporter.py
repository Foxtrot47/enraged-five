from pyfbsdk import *
from pyfbsdk_additions import *

import os
import RS.Perforce as p4
import RS.Core.BulkMetaEditor.BulkMetaEditor as bulk
import RS.Core.Face.Lib
import RS.Core.Face.XMLImporter

class setupDxyz_UI ( object ):
    def __init__( self ):
        p4.Sync( RS.Config.Project.Path.Root + "art\\animation\\resources\\face\\dxyz\\config\\batch\\..." )
        mainLyt = FBCreateUniqueTool( "Setup Dynamixyz File" )
        
        ## Main Tool specs
        mainLyt.StartSizeX = 700
        mainLyt.StartSizeY = 700
        
        ## Layout elements, standard sizing
        col_lbl = 75
        col_num = 35
        but1_w = 125
        but1_h = 25
        but2_h = 50
        but3_h = 35
        buff_w = 10
        buff_h = 10
        
        # Load/Save...
        # create a button
        job_open_btn = FBButton()
        job_open_btn.Caption = "Open..."
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachLeft,"" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachTop,"" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but3_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "job_open_btn", "job_open_btn", x, y, w, h )
        mainLyt.SetControl( "job_open_btn", job_open_btn )
        job_open_btn.OnClick.Add( self.Open_Job )
        
        # create a button
        job_save_btn = FBButton()
        job_save_btn.Caption = "Save..."
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"job_open_btn" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"job_open_btn" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but3_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "job_save_btn", "job_save_btn", x, y, w, h )
        mainLyt.SetControl( "job_save_btn", job_save_btn )
        job_save_btn.OnClick.Add( self.Save_Job )
        
        ## Current Scene
        # create a label
        current_lbl = FBLabel()
        current_lbl.Caption = "Setup Current Scene:"
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachLeft,"" )
        y = FBAddRegionParam( but1_h,FBAttachType.kFBAttachBottom,"job_open_btn" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "current_lbl", "current_lbl", x, y, w, h )
        mainLyt.SetControl( "current_lbl", current_lbl )
        
        
        ## Anim XML File
        # create a label
        anim_lbl = FBLabel()
        anim_lbl.Caption = "Anim XML File:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"current_lbl" )
        y = FBAddRegionParam( but1_h,FBAttachType.kFBAttachBottom,"current_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "anim_lbl", "anim_lbl", x, y, w, h )
        mainLyt.SetControl( "anim_lbl", anim_lbl )
        
        # create a button
        anim_load = FBButton()
        anim_load.Caption = "Load AnimXML File..."
        
        x = FBAddRegionParam( -1 * but1_w - buff_w,FBAttachType.kFBAttachRight,"" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"anim_lbl" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "anim_load", "anim_load", x, y, w, h )
        mainLyt.SetControl( "anim_load", anim_load )
        anim_load.OnClick.Add( self.Load_Anim )
        
        # create a textfield
        self.anim_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"anim_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"anim_lbl" )
        w = FBAddRegionParam( -1 * buff_w,FBAttachType.kFBAttachLeft,"anim_load" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "anim_edit", "anim_edit", x, y, w, h )
        mainLyt.SetControl( "anim_edit", self.anim_edit )
        self.anim_edit.OnChange.Add( self.checkBtns )
        
        
        ## Performer File
        # create a label
        perf_lbl = FBLabel()
        perf_lbl.Caption = "Performer File:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"anim_lbl" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"anim_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "perf_lbl", "perf_lbl", x, y, w, h )
        mainLyt.SetControl( "perf_lbl", perf_lbl )
        
        # create a button
        perf_load = FBButton()
        perf_load.Caption = "Load Performer File..."
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"anim_load" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"anim_load" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "perf_load", "perf_load", x, y, w, h )
        mainLyt.SetControl( "perf_load", perf_load )
        perf_load.OnClick.Add( self.Load_Perf )
        
        # create a textfield
        self.perf_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"perf_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"perf_lbl" )
        w = FBAddRegionParam( -1 * buff_w,FBAttachType.kFBAttachLeft,"perf_load" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "perf_edit", "perf_edit", x, y, w, h )
        mainLyt.SetControl( "perf_edit", self.perf_edit )
        self.perf_edit.OnChange.Add( self.checkBtns )
        
        
        ## Video Path
        # create a label
        video_lbl = FBLabel()
        video_lbl.Caption = "Video Path:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"perf_lbl" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"perf_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "video_lbl", "video_lbl", x, y, w, h )
        mainLyt.SetControl( "video_lbl", video_lbl )
        
        # create a button
        video_load = FBButton()
        video_load.Caption = "Load Video Folder..."
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"perf_load" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"perf_load" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "video_load", "video_load", x, y, w, h )
        mainLyt.SetControl( "video_load", video_load )
        video_load.OnClick.Add( self.Load_Video )
        
        # create a textfield
        self.video_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"video_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"video_lbl" )
        w = FBAddRegionParam( -1 * buff_w,FBAttachType.kFBAttachLeft,"video_load" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "video_edit", "video_edit", x, y, w, h )
        mainLyt.SetControl( "video_edit", self.video_edit )
        self.video_edit.OnChange.Add( self.checkBtns )
        
        
        ## Offset
        # create a label
        offset_lbl = FBLabel()
        offset_lbl.Caption = "Offset:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"video_lbl" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"video_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "offset_lbl", "offset_lbl", x, y, w, h )
        mainLyt.SetControl( "offset_lbl", offset_lbl )
        
        # create a textfield
        self.offset_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"offset_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"offset_lbl" )
        w = FBAddRegionParam( col_num,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "offset_edit", "offset_edit", x, y, w, h )
        mainLyt.SetControl( "offset_edit", self.offset_edit )
        self.offset_edit.Text = str(-10)
        self.offset_edit.OnChange.Add( self.checkBtns )
        
        
        
        ## Setup Current Scene
        # create a button
        self.single_btn = FBButton()
        self.single_btn.Caption = "Setup Current Scene"
        
        x = FBAddRegionParam( -1 * but1_w,FBAttachType.kFBAttachLeft,"video_load" )
        y = FBAddRegionParam( -10,FBAttachType.kFBAttachBottom,"offset_lbl" )
        w = FBAddRegionParam( 2 * but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but2_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "single_btn", "single_btn", x, y, w, h )
        mainLyt.SetControl( "single_btn", self.single_btn )
        self.single_btn.Enabled = False
        self.single_btn.OnClick.Add( self.Process_Current )
        
        ## Setup Facial File
        # create a button
        self.setup_face_btn = FBButton()
        self.setup_face_btn.Caption = "Setup Facial File..."
        
        x = FBAddRegionParam( -1 * but1_w - buff_w,FBAttachType.kFBAttachLeft,"single_btn" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"single_btn" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "setup_face_btn", "setup_face_btn", x, y, w, h )
        mainLyt.SetControl( "setup_face_btn", self.setup_face_btn )
        self.setup_face_btn.OnClick.Add( self.Setup_Face )
        # If a character exists:
        cl = FBComponentList()
        FBFindObjectsByName( "*SKEL_Head*", cl, False, True )
        if len( cl ) != 0:
            self.setup_face_btn.Enabled = False
        else:
            self.setup_face_btn.Enabled = True
        
        
        
        
        ## Batch Scenes
        # create a label
        batch_lbl = FBLabel()
        batch_lbl.Caption = "Setup Batch Scenes:"
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachLeft,"" )
        y = FBAddRegionParam( but1_h,FBAttachType.kFBAttachBottom,"single_btn" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "batch_lbl", "batch_lbl", x, y, w, h )
        mainLyt.SetControl( "batch_lbl", batch_lbl )
        
        ## Save Name
        # create a label
        save_lbl = FBLabel()
        save_lbl.Caption = "Save As:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"batch_lbl" )
        y = FBAddRegionParam( but1_h,FBAttachType.kFBAttachBottom,"batch_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "save_lbl", "save_lbl", x, y, w, h )
        mainLyt.SetControl( "save_lbl", save_lbl )
        
        # create a textfield
        self.save_B_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"save_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"save_lbl" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "save_B_edit", "save_B_edit", x, y, w, h )
        mainLyt.SetControl( "save_B_edit", self.save_B_edit )
        self.save_B_edit.Text = "DXYZ"
        self.save_B_edit.OnChange.Add( self.checkBtns )
        
        
        ## Performer File - Batch
        # create a label
        perf_B_lbl = FBLabel()
        perf_B_lbl.Caption = "Performer File:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"save_lbl" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"save_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "perf_B_lbl", "perf_B_lbl", x, y, w, h )
        mainLyt.SetControl( "perf_B_lbl", perf_B_lbl )
        
        # create a button
        perf_B_load = FBButton()
        perf_B_load.Caption = "Load Performer File..."
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"video_load" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"save_lbl" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "perf_B_load", "perf_B_load", x, y, w, h )
        mainLyt.SetControl( "perf_B_load", perf_B_load )
        perf_B_load.OnClick.Add( self.Load_Perf )
        
        # create a textfield
        self.perf_B_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"perf_B_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"perf_B_lbl" )
        w = FBAddRegionParam( -1 * buff_w,FBAttachType.kFBAttachLeft,"perf_B_load" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "perf_B_edit", "perf_B_edit", x, y, w, h )
        mainLyt.SetControl( "perf_B_edit", self.perf_B_edit )
        self.perf_B_edit.OnChange.Add( self.checkBtns )
        
        
        ## Video Path
        # create a label
        video_B_lbl = FBLabel()
        video_B_lbl.Caption = "Video Path:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"perf_B_lbl" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"perf_B_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "video_B_lbl", "video_B_lbl", x, y, w, h )
        mainLyt.SetControl( "video_B_lbl", video_B_lbl )
        
        # create a button
        video_B_load = FBButton()
        video_B_load.Caption = "Load Video Folder..."
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"perf_B_load" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"perf_B_load" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "video_B_load", "video_B_load", x, y, w, h )
        mainLyt.SetControl( "video_B_load", video_B_load )
        video_B_load.OnClick.Add( self.Load_Video )
        
        # create a textfield
        self.video_B_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"video_B_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"video_B_lbl" )
        w = FBAddRegionParam( -1 * buff_w,FBAttachType.kFBAttachLeft,"video_B_load" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "video_B_edit", "video_B_edit", x, y, w, h )
        mainLyt.SetControl( "video_B_edit", self.video_B_edit )
        self.video_B_edit.OnChange.Add( self.checkBtns )
        
        
        ## Target Path
        # create a label
        target_B_lbl = FBLabel()
        target_B_lbl.Caption = "Target Path:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"video_B_lbl" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"video_B_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "target_B_lbl", "target_B_lbl", x, y, w, h )
        mainLyt.SetControl( "target_B_lbl", target_B_lbl )
        
        # create a button
        target_B_load = FBButton()
        target_B_load.Caption = "Load Target Folder..."
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"video_B_load" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"video_B_load" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "target_B_load", "target_B_load", x, y, w, h )
        mainLyt.SetControl( "target_B_load", target_B_load )
        target_B_load.OnClick.Add( self.Load_Target )
        
        # create a textfield
        self.target_B_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"target_B_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"target_B_lbl" )
        w = FBAddRegionParam( -1 * buff_w,FBAttachType.kFBAttachLeft,"target_B_load" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "target_B_edit", "target_B_edit", x, y, w, h )
        mainLyt.SetControl( "target_B_edit", self.target_B_edit )
        self.target_B_edit.OnChange.Add( self.checkBtns )
        
        
        ## Character Path
        # create a label
        char_B_lbl = FBLabel()
        char_B_lbl.Caption = "Character FBX:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"target_B_lbl" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"target_B_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "char_B_lbl", "char_B_lbl", x, y, w, h )
        mainLyt.SetControl( "char_B_lbl", char_B_lbl )
        
        # create a button
        char_B_load = FBButton()
        char_B_load.Caption = "Load Char FBX..."
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"target_B_load" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"target_B_load" )
        w = FBAddRegionParam( but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "char_B_load", "char_B_load", x, y, w, h )
        mainLyt.SetControl( "char_B_load", char_B_load )
        char_B_load.OnClick.Add( self.Load_Char )
        
        # create a textfield
        self.char_B_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"char_B_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"char_B_lbl" )
        w = FBAddRegionParam( -1 * buff_w,FBAttachType.kFBAttachLeft,"char_B_load" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "char_B_edit", "char_B_edit", x, y, w, h )
        mainLyt.SetControl( "char_B_edit", self.char_B_edit )
        self.char_B_edit.OnChange.Add( self.checkBtns )
        
        
        ## Offset
        # create a label
        offset_B_lbl = FBLabel()
        offset_B_lbl.Caption = "Offset:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"char_B_lbl" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"char_B_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "offset_B_lbl", "offset_B_lbl", x, y, w, h )
        mainLyt.SetControl( "offset_B_lbl", offset_B_lbl )
        
        # create a textfield
        self.offset_B_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"offset_B_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"offset_B_lbl" )
        w = FBAddRegionParam( col_num,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "offset_B_edit", "offset_B_edit", x, y, w, h )
        mainLyt.SetControl( "offset_B_edit", self.offset_B_edit )
        self.offset_B_edit.Text = str(-10)
        self.offset_B_edit.OnChange.Add( self.checkBtns )
        
        
        ## Max Takes
        # create a label
        max_lbl = FBLabel()
        max_lbl.Caption = "Max Per FBX:"
        
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"offset_B_lbl" )
        y = FBAddRegionParam( buff_h,FBAttachType.kFBAttachBottom,"offset_B_lbl" )
        w = FBAddRegionParam( col_lbl,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "max_lbl", "max_lbl", x, y, w, h )
        mainLyt.SetControl( "max_lbl", max_lbl )
        
        # create a textfield
        self.max_B_edit = FBEdit()
        
        x = FBAddRegionParam( buff_w,FBAttachType.kFBAttachRight,"max_lbl" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"max_lbl" )
        w = FBAddRegionParam( col_num,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but1_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "max_B_edit", "max_B_edit", x, y, w, h )
        mainLyt.SetControl( "max_B_edit", self.max_B_edit )
        self.max_B_edit.Text = str(25)
        self.max_B_edit.OnChange.Add( self.checkBtns )
        
        
        ## Setup Batch Scenes
        # create a button
        self.batch_btn = FBButton()
        self.batch_btn.Caption = "Setup Batch Scenes"
        
        x = FBAddRegionParam( -1 * but1_w,FBAttachType.kFBAttachLeft,"video_load" )
        y = FBAddRegionParam( -40,FBAttachType.kFBAttachBottom,"max_lbl" )
        w = FBAddRegionParam( 2 * but1_w,FBAttachType.kFBAttachNone,"" )
        h = FBAddRegionParam( but2_h,FBAttachType.kFBAttachNone,"" )
        mainLyt.AddRegion( "batch_btn", "batch_btn", x, y, w, h )
        mainLyt.SetControl( "batch_btn", self.batch_btn )
        self.batch_btn.Enabled = False
        self.batch_btn.OnClick.Add( self.Process_Batch )
        
        
        
        
        ShowTool( mainLyt )
        mainTool = mainLyt
        
        
            
    def Open_Job( self, control, event ):
        lDefault = os.path.join( RS.Config.Project.Path.Root, "art\\animation\\resources\\face\\dxyz\\config\\batch" )
        lRes = self.popupFileOpen( "Open File...", "*.dxyz", lDefault )
        if lRes:
            f = open( lRes, 'r' )
            self.anim_edit.Text = f.readline().split("\n")[0]
            self.perf_edit.Text = f.readline().split("\n")[0]
            self.video_edit.Text = f.readline().split("\n")[0]
            self.offset_edit.Text = f.readline().split("\n")[0]
            self.save_B_edit.Text = f.readline().split("\n")[0]
            self.perf_B_edit.Text = f.readline().split("\n")[0]
            self.video_B_edit.Text = f.readline().split("\n")[0]
            self.target_B_edit.Text = f.readline().split("\n")[0]
            self.char_B_edit.Text = f.readline().split("\n")[0]
            self.max_B_edit.Text = f.readline().split("\n")[0]
            self.offset_B_edit.Text = f.readline().split("\n")[0]
            f.close()
            
            
        
    def Save_Job( self, control, event ):
        lDefault = os.path.join( RS.Config.Project.Path.Root, "art\\animation\\resources\\face\\dxyz\\config\\batch" )
        lRes = self.popupFileSave( "Save File...", "*.dxyz", lDefault )
        if lRes:
            f = open( lRes, 'w' )
            f.truncate()
            f.write( self.anim_edit.Text + "\n" )
            f.write( self.perf_edit.Text + "\n" )
            f.write( self.video_edit.Text + "\n" )
            f.write( self.offset_edit.Text + "\n" )
            f.write( self.save_B_edit.Text + "\n" )
            f.write( self.perf_B_edit.Text + "\n" )
            f.write( self.video_B_edit.Text + "\n" )
            f.write( self.target_B_edit.Text + "\n" )
            f.write( self.char_B_edit.Text + "\n" )
            f.write( self.max_B_edit.Text + "\n" )
            f.write( self.offset_B_edit.Text + "\n" )
            f.close()
            
        
    def Load_Anim( self, control, event ):
        lDefault = RS.Config.Project.Path.Root
        if len( self.anim_edit.Text ) != 0:
            lDef = os.path.dirname( self.anim_edit.Text )
            if os.path.exists( lDef ):
                lDefault = lDef
        else:
            if len( self.perf_edit.Text ) != 0:
                lDef = os.path.dirname( self.perf_edit.Text )
                if os.path.exists( lDef ):
                    lDefault = lDef
        lRes = self.popupFileOpen( "Find RS_Anim XML file:", "*.xml", lDefault )
        self.anim_edit.Text = lRes
        self.checkBtns( control, event )
            
    def Load_Perf( self, control, event):
        lDefault = RS.Config.Project.Path.Root
        if len( self.perf_edit.Text ) != 0:
            lDef = os.path.dirname( self.perf_edit.Text )
            if os.path.exists( lDef ):
                lDefault = lDef
        lRes = self.popupFileOpen( "Find Performer2SV file:", "*.perfproj", lDefault )
        self.perf_edit.Text = lRes
        self.perf_B_edit.Text = lRes
        self.checkBtns( control, event )
        
    def Load_Video( self, control, event):
        lDefault = RS.Config.Project.Path.Root
        lDef = self.video_edit.Text
        if len( lDef ) != 0:
            if os.path.exists( lDef ):
                lDefault = lDef
        lRes = self.popupFolderPath( "Select Video Folder:", lDefault )
        self.video_edit.Text = lRes
        self.video_B_edit.Text = lRes
        self.checkBtns( control, event )
        
    def Load_Target( self, control, event):
        lDefault = RS.Config.Project.Path.Root
        lDef = self.target_B_edit.Text
        if len( lDef ) != 0:
            if os.path.exists( lDef ):
                lDefault = lDef
        lRes = self.popupFolderPath( "Select Target Folder:", lDefault )
        self.target_B_edit.Text = lRes
        self.checkBtns( control, event )
        
    def Load_Char( self, control, event):
        lDefault = os.path.join( RS.Config.Project.Path.Root, "art\\animation\\resources\\characters\\models\\cutscene" )
        lRes = self.popupFileOpen( "Which character FBX to use?", "*.fbx", lDefault )
        self.char_B_edit.Text = lRes
        self.checkBtns( control, event )
        
    def checkBtns( self, control, event ):
        # Check For Current
        lEnable = True
        lFiles = [
            self.anim_edit.Text,
            self.perf_edit.Text
        ]
        for lFile in lFiles:
            if not os.path.isfile( lFile ):
                lEnable = False
                
        lFolders = [
            self.video_edit.Text
        ]
        for lFolder in lFolders:
            if not os.path.exists( lFolder ):
                lEnable = False
        
        lIntegers = [
            self.offset_edit.Text
        ]
        for lInt in lIntegers:
            try:
                lInt = int(lInt)
            except:
                pass
            if not isinstance( lInt, int ):
                lEnable = False
        
        # If a character exists:
        cl = FBComponentList()
        FBFindObjectsByName( "*SKEL_Head*", cl, False, True )
        if len( cl ) != 0:
            self.setup_face_btn.Enabled = False
        else:
            lEnable = False
            
            
        if lEnable == True:
            self.single_btn.Enabled = True
            self.setup_face_btn.Enabled = False
        else:
            self.single_btn.Enabled = False
            
            
        # Check for Batch
        lEnable = True
        lFiles = [
            self.perf_B_edit.Text,
            self.char_B_edit.Text
        ]
        for lFile in lFiles:
            if not os.path.isfile( lFile ):
                lEnable = False
                
        lFolders = [
            self.video_B_edit.Text,
            self.target_B_edit.Text
        ]
        for lFolder in lFolders:
            if not os.path.exists( lFolder ):
                lEnable = False
        
        lIntegers = [
            self.offset_B_edit.Text,
            self.max_B_edit.Text
        ]
        for lInt in lIntegers:
            try:
                lInt = int(lInt)
            except:
                pass
            if not isinstance( lInt, int ):
                lEnable = False
        
        lNames = [
            self.save_B_edit.Text
        ]
        for lName in lNames:
            if not len( lName ) > 0:
                lEnable = False
                
        if lEnable == True:
            self.batch_btn.Enabled = True
        else:
            self.batch_btn.Enabled = False
            
    def Setup_Face( self, control, event ):
        RS.Core.Face.Lib.SetupFacialFile( syncTxt = True )
        self.checkBtns( control, event )
        
        
    def Process_Current( self, control, event ):
        lAnimFile = self.anim_edit.Text
        lPerfFile = self.perf_edit.Text
        lVideoPath = self.video_edit.Text
        lOffset = int( self.offset_edit.Text )
        RS.Core.Face.XMLImporter.setupSingleFile( lAnimFile, lPerfFile, lVideoPath, lOffset )
        
    def Process_Batch( self, control, event ):
        lSaveName_B = self.save_B_edit.Text
        lPerfFile_B = self.perf_B_edit.Text
        lVideoPath_B = self.video_B_edit.Text
        lTargetPath_B = self.target_B_edit.Text
        lCharPath_B = self.char_B_edit.Text
        lMaxTakes_B = int( self.max_B_edit.Text )
        lOffset_B = int( self.offset_B_edit.Text )
        RS.Core.Face.XMLImporter.setupFolderFiles( lSaveName_B, lPerfFile_B, lVideoPath_B, lTargetPath_B, lCharPath_B, lMaxTakes_B, lOffset_B )
        

    def popupFolderPath( self, lCaption, lDefault = RS.Config.Project.Path.Root ) :
        #
        # Browse for folder
        #
        lRes = None
        lFp = FBFolderPopup()
        lFp.Caption = lCaption
        lFp.Path = lDefault
        lRes = lFp.Execute()
        if lRes:
            return lFp.Path
            
        
    def popupFileOpen( self, lCaption, lExtensions, lDefault = RS.Config.Project.Path.Root ) :
        #
        # Browse for file
        #
        lRes = None
        lFp = FBFilePopup()
        lFp.Caption = lCaption
        lFp.Filter = lExtensions
        lFp.Path = lDefault
        lRes = lFp.Execute()
        if lRes:
            return os.path.join( lFp.Path, lFp.FileName )
    
    def popupFileSave( self, lCaption, lExtensions, lDefault = RS.Config.Project.Path.Root ) :
        #
        # Save file
        #
        lRes = None
        lFp = FBFilePopup()
        lFp.Caption = lCaption
        lFp.Filter = lExtensions
        lFp.Path = lDefault
        lFp.Style = FBFilePopupStyle.kFBFilePopupSave
        lRes = lFp.Execute()
        if lRes:
            return os.path.join( lFp.Path, lFp.FileName )
        

def Run():
    setupDxyz = setupDxyz_UI()
    