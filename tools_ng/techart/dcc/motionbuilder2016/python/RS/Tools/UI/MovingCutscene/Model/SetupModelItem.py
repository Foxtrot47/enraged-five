"""
Model item that wraps around the Components objects that make up the moving cutscene setup
"""
import os
import pyfbsdk as mobu

from PySide import QtGui, QtCore

from RS import Config, Globals
from RS.Core.Animation import Lib
from RS.Utils.Scene import Component
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModelItem


class SetupModelItem(baseModelItem.BaseModelItem):

    RedColor = QtGui.QColor(205, 0, 0)
    GreenColor = QtGui.QColor(0, 155, 0)
    YellowColor = QtGui.QColor(190, 110, 0)
    BlueColor = QtGui.QColor(20, 100, 200)
    PinkColor = QtGui.QColor(150, 80, 80)

    DataRole = QtCore.Qt.UserRole + 2
    ItemRole = QtCore.Qt.UserRole + 3

    FolderIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "motionbuilder", "folder.png"))
    ActiveIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "Camera", "dial", "dial_dark_mini_light.png"))
    CharacterInactiveIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "Camera", "dial", "dial_dark_mini_light_yellow.png"))
    InactiveIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "Camera", "dial", "dial_dark_mini_light_Disabled.png"))
    UnavailableIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "Facestar", "delete.png"))
    SkeletonIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "motionbuilder", "etc", "HIKCharacterToolSkeleton.png"))
    CharacterIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "motionbuilder", "etc", "HIKCharacterToolFullBody.png"))

    _Icons = {}

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QAbstractItemModel): parent item
        """
        super(SetupModelItem, self).__init__(parent=parent, autoAppend=True)
        self._roles = {}
        self._headers = []
        self._data = None

    def data(self, index=QtCore.QModelIndex(), role=QtCore.Qt.DisplayRole):
        """
        Overrides inherited method

        Displays data based on the index and role passed in by the view

        Arguments:
            index (QtCore.QModelIndex): index being called in the view
            role (QtCore.Qt.Role): role being requested by the view

        Return:
            object
        """
        column = index.column() if index is not None and index.isValid() else 0
        data = self._data

        if role == self.ItemRole:
            return self

        elif role == self.DataRole:
            return data

        elif role == QtCore.Qt.DisplayRole and self._headers:
            return self._headers[column]

        marker = data.marker()

        if role == QtCore.Qt.DisplayRole:

            if marker is None:
                return "--- DELETED ---"
            elif column == 0:
                return data.name()
            elif column == 1:
                return ("Unselected", "Selected")[self._data.marker().Selected]
            elif column == 4 and isinstance(data.component(), mobu.FBCharacter):
                return

            if data.hasConstraint():
                constraint = data.constraint() or data.driverConstraint()
                if column == 2:
                    return ("Disabled", "Active")[constraint.Active]
                elif column == 3:
                    return constraint.Weight.Data
            if column < 4:
                return "N/A"

        elif role == QtCore.Qt.EditRole:
            if column == 0:
                return data.name()
            elif column == 3:
                constraint = data.constraint() or data.driverConstraint()
                return constraint.Weight.Data if constraint else None

        elif role == QtCore.Qt.DecorationRole:
            component = data.component()
            constraint = data.constraint() or data.driverConstraint()
            if not column:
                if not isinstance(marker, mobu.FBCamera) and not component:
                    return self.FolderIcon
                elif isinstance(marker, mobu.FBCamera):
                    component = marker
                icon = self._Icons.get(component.__class__, None)
                if not icon:
                    icon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "motionbuilder", "{}.png".format(component.__class__.__name__[2:].lower())))
                    self._Icons[component.__class__] = icon
                return icon

            elif column == 1:
                if marker is not None and marker.Selected:
                    return self.ActiveIcon
                return self.InactiveIcon

            elif column == 2 and constraint is not None and not Component.IsDestroyed(constraint) and constraint.Active:
                if isinstance(component, mobu.FBCharacter) and (component.ActiveInput is False or component.InputType != mobu.FBCharacterInputType.kFBCharacterInputMarkerSet):
                    return self.CharacterInactiveIcon
                return self.ActiveIcon

            elif column in (2, 3) and not constraint:
                return self.UnavailableIcon

            elif column == 2:
                return self.InactiveIcon

            elif column == 4:
                if isinstance(component, mobu.FBCharacter):
                    return self.SkeletonIcon
                return self.UnavailableIcon

        elif role == QtCore.Qt.BackgroundRole:
            if not column:
                movingcutscene = data.movingCutscene()
                data = self._data

                active = movingcutscene.active()

                if data.driver() and data.driverConstraint():
                    return QtGui.QBrush(self.BlueColor)
                elif not data.hasConstraint():
                    return QtGui.QBrush(self.YellowColor)
                elif active is not None and active:
                    return QtGui.QBrush(self.RedColor)
                elif data.hasConstraint():
                    return QtGui.QBrush(self.GreenColor)

            elif column == 3 and self._data.hasConstraint():
                constraint = self._data.constraint() or self._data.driverConstraint()
                weightProperty = constraint.PropertyList.Find("Weight")
                if weightProperty.IsAnimated():
                    for key in weightProperty.GetAnimationNode().FCurve.Keys:
                        if key.Time.GetFrame() == Globals.System.LocalTime.GetFrame():
                            return self.RedColor
                    return self.PinkColor

        return self._roles.get(role, None)

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        Overrides inherited method

        Sets data on the item based on the index and role passed in by the view

        Arguments:
            index (QtCore.QModelIndex): index used by the view
            value (object): value to store/set
            role (QtCore.Qt.Role): role being requested by the view
        """
        if role == self.DataRole:
            self._data = value
        elif role == QtCore.Qt.EditRole:
            if index and index.column() == 0:
                self._data.setName(str(value))
            if index and index.column() == 3:
                constraint = self._data.constraint() or self._data.driverConstraint()
                if constraint:
                    constraint.Weight.Data = value
                    Globals.Scene.Evaluate()
        else:
            self._roles[role] = value
        return True

    def rowCount(self):
        """
        The number of rows for this item

        Return:
            int
        """
        return self.childCount()

    def columnCount(self):
        """
        Overrides inherited method

        returns the number of columns this item uses
        """
        return len(self._headers) or 5

    def flags(self, index=None):
        """
        Overrides inherited method

        Returns if the item should be enabled or not

        Arguments:
            index (QtCore.QModelIndex): the index used by the view

        Return:
            QtCore.Qt.Flags
        """
        if index and index.isValid() and index.column() == 0:
            return super(SetupModelItem, self).flags(index)
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def headers(self):
        """
        The headers associated with this item

        Returns:
            list[strings, etc.]
        """
        return self._headers

    def setHeader(self, headers):
        """
        Sets a list of headers for this item to use

        Arguments:
            headers (list): list of strings to use as headers
        """
        self._headers = headers
