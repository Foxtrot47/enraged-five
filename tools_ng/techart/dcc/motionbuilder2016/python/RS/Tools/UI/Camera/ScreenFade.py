from pyfbsdk import *
from PySide import QtCore, QtGui
import os
import webbrowser

import RS.Tools.UI
import RS.Config
import RS.Tools.UI.Commands as cmds
gDevelopment = False

if gDevelopment:
    print '{0}\\RS\\Tools\\UI\\Camera\\QT\\Screen_Fade_QT.ui'.format(RS.Config.Script.Path.Root)

RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\UI\\Camera\\QT\\Screen_Fade_QT.ui'.format(RS.Config.Script.Path.Root), '{0}\\RS\\Tools\\UI\\Camera\\Screen_Fade_QT.py'.format(RS.Config.Script.Path.Root))

import RS.Tools.UI.Camera.Screen_Fade_QT as fade

reload( fade )
class BannerWidget( QtGui.QWidget ):
    def __init__( self ):
        QtGui.QWidget.__init__( self )
        header = QtGui.QHBoxLayout()
        #print dir(self.__header)        
        header.setAlignment( QtCore.Qt.AlignTop ) #Align the whole Layout at the top.
        header.setSpacing(0)
        header.setContentsMargins(0,0,0,0)           
        #self.setLayout( header )        
                
        ## Logo ##
        logo = QtGui.QLabel()
        logo.setPixmap(QtGui.QPixmap("{0}\\header_YellowRSLogo_30x30.png".format( RS.Config.Script.Path.ToolImages )))  
        logo.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed) # Do not change size
        logo.setAlignment( QtCore.Qt.AlignLeft ) #Align the logo to the left
        header.addWidget( logo )
        
        ## Bannner Filler ##
        banner = QtGui.QLabel()
        banner.setPixmap(QtGui.QPixmap("{0}\\header_YellowSpacerBanner_3591x30".format( RS.Config.Script.Path.ToolImages )))  
        banner.setSizePolicy(QtGui.QSizePolicy.Ignored, QtGui.QSizePolicy.Fixed)  
        banner.resize(100, 30)
        header.addWidget( banner )          
        
        ## Email Link ##        
        emailIcon = QtGui.QIcon()
        emailIcon.addPixmap( QtGui.QPixmap(QtGui.QPixmap("{0}\\header_YellowEmailIcon_35x30.png".format( RS.Config.Script.Path.ToolImages ))), QtGui.QIcon.Normal, QtGui.QIcon.Off )        
        email = QtGui.QPushButton(self)
        email.resize( 35, 30 )
        email.setToolTip("Email Tech Art.")  
        email.setFlat(True)
        email.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed) # Do not change size        
        email.setIcon(emailIcon)
        email.setIconSize( QtCore.QSize( 35, 30 ) )
        email.setMaximumWidth( 35 )   
        email.setMaximumHeight( 30 )           
        header.addWidget( email )
        email.pressed.connect( self.OnEmail_Clicked )  # Needs to called after addWidget call             
        
        ## Info Link ##
        infoIcon = QtGui.QIcon()
        infoIcon.addPixmap( QtGui.QPixmap(QtGui.QPixmap("{0}\\header_YellowInfoIcon_35x30.png".format( RS.Config.Script.Path.ToolImages ))), QtGui.QIcon.Normal, QtGui.QIcon.Off )        
        info = QtGui.QPushButton(self)
        info.resize( 35, 30 )
        info.setToolTip("Link to tool documentation.")  
        info.setFlat(True)
        info.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed) # Do not change size        
        info.setIcon(infoIcon)
        info.setIconSize( QtCore.QSize( 35, 30 ) )
        info.setMaximumWidth( 35 ) 
        info.setMaximumHeight( 30 )          
        header.addWidget( info )        
        info.pressed.connect( self.OnInfo_Clicked )
                

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.setContentsMargins(0,0,0,0) 
        mainLayout.addLayout(header)
        #mainLayout.addLayout(body)
        self.setLayout(mainLayout)        

    def OnEmail_Clicked( self ): 
        gCc = None
        if gCc == None:
            os.startfile( "mailto:kmiddlemiss@gmail.com")# + self.gEmailTo + "&Subject= " + self.gToolName + "&Body=" + self.gToolPath )
            
        else:
            os.startfile( "mailto:kmiddlemiss@gmail.com")
            #os.startfile( "mailto:" + self.gEmailTo + "?CC=" + self.gCc + "&Subject= " + self.gToolName + "&Body=" + self.gToolPath )
                    
    def OnInfo_Clicked( self):
        gWikiLink = "https://devstar.rockstargames.com/wiki/index.php/Adding_Screen_Fades_to_the_Cutscene_Exporter"
        if gWikiLink == None:
            FBMessageBox( "Information", "Sorry, there is no Wiki page for this UI...YET :)", "Ok" )
            
        else:
            lNew = 2
            webbrowser.open(gWikiLink) 
            
            
class MainWindow( RS.Tools.UI.QtMainWindowBase, fade.Ui_MainWindow ):
    
    def __init__(self):
        RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = 'Test Material Settings UI')
        self.centralWidget = BannerWidget()
        self.setCentralWidget( self.centralWidget )
        
        self.setupUi(self)
        
        self.transparencyAnimationNode = None
        self.startKey = []
        self.stopKey = []
        self.camera = None
        self.transparencyNode = None 
        self.lSystem = FBSystem()  
        
        #disable key buttons (Read-Only)
        self.fadeInKey.setEnabled(False)
        self.fadeOutKey.setEnabled(False)
        
        #set pixmaps
        self.matBkgrd.setPixmap(QtGui.QPixmap('{0}\\RS\\Tools\\UI\\Camera\\QT\\material_background.png'.format(RS.Config.Script.Path.Root)))
        self.matBall.setPixmap(QtGui.QPixmap('{0}\\RS\\Tools\\UI\\Camera\\QT\\material_ball.png'.format(RS.Config.Script.Path.Root)))
        
        # Connect our widgets
        self.transparencyValue.valueChanged.connect(self.setTransparency)
        self.setFadeIn.pressed.connect(self.setKey1)        
        self.delFadeIn.pressed.connect(self.delKey1)
        self.delFadeOut.pressed.connect(self.delKey2)  
        self.sanityCheckButton.pressed.connect(self.checkIfKeysCorrect)
        
        
        self.setTransparency()
        
        #Check for the Export camera
        cam = self.GetExportCamera()
        
        if cam:
            node = cam.Roll.GetAnimationNode()
            self.startKey, self.stopKey = self.GetScreenFadeNodeKeys(cam)
            
        self.SetButtons(self.startKey, self.stopKey)
        
        self.refreshKeys()
        
        self.mSystem = FBSystem()
        self.mApp = FBApplication()
        self.mAcceptCallback = True        

        self.Register()
        
        #if self.fadeInButton.isChecked():
            #print "Fade In"
        #elif self.fadeOutButton.isChecked():
            #print "Fade Out"
        #else:
            #print "concat"

    def ConnectionDataNotify( self, pActivate ):
        if self.mSystem:
            # we always try to remove this notification function to prevent cases where we have it register twice.
            self.mSystem.OnConnectionDataNotify.Remove(self.OnConnectionDataNotify) 
        
            if pActivate:
                self.mSystem.OnConnectionDataNotify.Add(self.OnConnectionDataNotify)
                if gDevelopment:
                    print "Data Notification Activated"
            else:
                print "Data Notification Deactivated"      

    def closeEvent( self, event ): 
        if gDevelopment:
            print 'closeEvent'
        self.ConnectionDataNotify(False)
##        self.mSystem.Scene.OnChange.Remove(self.SceneChanged)
##        FBApplication().OnFileNewCompleted.Remove(self.OnFileNewCallback)
        self.Unregister()
        QtGui.QWidget.closeEvent( self, event )
        #QtGui.QMainWindow.closeEvent( self, event )
        


    def GetExportCamera(self):
        '''
        Grabs a list of cameras with the name "Export Camera" and verifies there is only one. Returns a handle to this camera
        '''
        #loop trough cameras
        camera = FBFindModelByLabelName('ExportCamera')
        if not camera:
            self.status.setText('Export Camera Not Found')
            
            #disable set and delete keys
            self.setFadeIn.setEnabled(False)
            self.delFadeIn.setEnabled(False)
            self.delFadeOut.setEnabled(False)

            return None
            
        else:
            camera.Selected = True
            self.status.setText('Export Camera Found')
            
            # enable set and delete keys
            self.setFadeIn.setEnabled(True)
            self.delFadeIn.setEnabled(True)
            self.delFadeOut.setEnabled(True)            
            
            #return the Export Camera handle
            return camera 
        
    def GetScreenFadeNodeKeys(self, exportCam):
        '''
        Validates there is no more than 2 keys in the Transparency Channel. Returns the first key, if any, as the startKey and the second, if any,
        as the stopKey. 
        '''
        startKey = [] 
        stopKey = []
##    
        # Get the "ExportCamera ScreenFadeMat" node    
        searchForCams = FBComponentList() 
        mat = None
        # FBFindObjectsByName() Parameters:
        #    pattern - The pattern we are looking for in the object name.
        #    cl - The list which will contain the search results.
        #    False - We do not wish to include the namespace in our search.
        #    True - We are only looking for instances of FBModel (which is a subclass of FBComponent)
        #   FBFindObjectsByName(pattern, cl, False, True)
        
        FBFindObjectsByName("ExportCamera ScreenFadeMat", searchForCams, True, False)
        
        if len(searchForCams) == 0:
           FBFindObjectsByName("ExportCam_ScreenFadeMaterial", searchForCams, True, False) 
        
        if len(searchForCams) > 0:
            mat = searchForCams[0]
        if mat:
            mat.HardSelect()

            # Get the Transparency channel node 
            self.transparencyAnimationNode = mat.PropertyList.Find("Transparency") 
            if not self.transparencyAnimationNode.IsAnimated():
                self.transparencyAnimationNode.SetAnimated(True)
            
           
            animNode = self.transparencyAnimationNode.GetAnimationNode()
            self.transparencyNode = animNode
            self.transparencyNode.HardSelect()
            
            #FBVector3D
            rCurve = animNode.Nodes[0].FCurve
            gCurve = animNode.Nodes[1].FCurve
            bCurve = animNode.Nodes[2].FCurve
            
            #How many keys do we have?
            redKeysTime = rCurve.Keys
            greenKeysTime = gCurve.Keys
            blueKeysTime = bCurve.Keys
    
            if (len(redKeysTime) > 2) | (len(greenKeysTime) > 2) | (len(blueKeysTime) > 2):
                FBMessageBox('Warning', 'More than 2 keys on the ExportCamera ScreenFadeMat', 'OK')
            else:
    
                #look for keys and assign them to the buttons, like in the definition FindLimits in example Tasks/StartKeysAtCurrentTime.py
                if len(redKeysTime) > 0:

                    startKey.append(redKeysTime[0])
                    startKey.append(greenKeysTime[0])
                    startKey.append(blueKeysTime[0])
                    
                    
                if len(redKeysTime) > 1:

                    stopKey.append(redKeysTime[1])
                    stopKey.append(greenKeysTime[1])
                    stopKey.append(blueKeysTime[1])
            
        return startKey, stopKey
        
    def SetButtons(self, startKey, stopKey):
        '''
        # If both keys are not set, change the status text to read "only one key set, will not be seen in game", "No keys set", or "Keys set"
        Returns the button status
        '''

        if len(startKey) > 0:
            self.fadeInKey.setText('Frame = ' + unicode(startKey[0].Time.GetFrame()) + ' Value = ' + unicode(startKey[0].Value))
            if gDevelopment:
                print 'startKey = ', startKey[0]
        else:        
            self.fadeInKey.setText('No Fade In Key Set')
            if gDevelopment:
                print 'startKey = None'
            
        if len(stopKey) > 0:
            self.fadeOutKey.setText('Frame = ' + unicode(stopKey[0].Time.GetFrame()) + ' Value = ' + unicode(stopKey[0].Value))
            if gDevelopment:
                print 'stopKey = ', stopKey[0]
        else: 
            self.fadeOutKey.setText('No Fade Out Key Set')       
            if gDevelopment:
                print 'stopKey = None'
            
        return
        
    def setTransparency(self):
        if self.transparencyValue.value() == 1:
            self.matBall.setVisible(False)
        else:
            self.matBall.setVisible(True)
        self.status.setText("Set Transparency") 
        
    def setKey1(self):
        # No keys are set
        if len(self.startKey) == 0 :
            self.setKeyAtCurrentFrame()
            
        # Only one key is set
        elif len(self.startKey) != 0 and len(self.stopKey) == 0:
            if self.GetCurrentFrame() != self.startKey[0].Time.GetFrame():
                self.setKeyAtCurrentFrame()

            else: 
                messagebox = FBMessageBox('Warning', 'Override key at current frame?', 'Yes', 'No')
                if messagebox == 1:
                    self.delKey1()
                    self.setKeyAtCurrentFrame()


        # We have both keys set
        elif len(self.startKey) != 0 and len(self.stopKey) != 0: 
            # If our current frame matches the frame of the first key
            if self.GetCurrentFrame() == self.startKey[0].Time.GetFrame():
                messagebox = FBMessageBox('Warning', 'Override key at current frame?', 'Yes', 'No')
                if messagebox == 1:
                    self.delKey1()
                    self.setKeyAtCurrentFrame()

            # If our current frame matches the frame of the second key, and not the first
            elif self.GetCurrentFrame() == self.stopKey[0].Time.GetFrame():
                messagebox = FBMessageBox('Warning', 'Override key at current frame?', 'Yes', 'No')
                if messagebox == 1:
                    self.delKey2()
                    self.setKeyAtCurrentFrame()
                
            # If we get here, we are on a frame that doesn't match either frame, and we need more logic
            # Is the value we are trying to set matching the first keys value?
            elif self.transparencyValue.value() == self.startKey[0].Value:
                messagebox = FBMessageBox('Warning', 'Override Fade Start Key?', 'Yes', 'No')
                if messagebox == 1:
                    self.delKey1()
                    self.setKeyAtCurrentFrame() 
                    
            # Is the value we are trying to set matching the second keys value?
            elif self.transparencyValue.value() == self.stopKey[0].Value:
                messagebox = FBMessageBox('Warning', 'Override Fade Stop Key?', 'Yes', 'No')
                if messagebox == 1:
                    self.delKey2()
                    self.setKeyAtCurrentFrame()   
            
            # The value we are trying to set does not match either value and the frame does not match either        
            else:
                messagebox = FBMessageBox('Warning', 'Which Key shall I overwrite?\nNote: The key you choose will be deleted and set to current frame and value, then the keys will be reordered chronologically.', 'Set First Key', 'Set Second Key', 'Ignore')
                if messagebox == 1:
                    self.delKey1()
                    self.setKeyAtCurrentFrame()
                elif messagebox == 2:
                    self.delKey2()
                    self.setKeyAtCurrentFrame()
                    
                    
            
            #elif self.transparencyValue.value() == 1 and self.stopKey[0].Value == 1:
                #messagebox = FBMessageBox('Warning', 'Override Fade Stop Key?', 'Yes', 'No')
                #if messagebox == 1:
                    #self.delKey2()
                    #self.setKeyAtCurrentFrame()                    

            #elif self.transparencyValue.value() == 0 and self.startKey[0].Value == 0:
                #messagebox = FBMessageBox('Warning', 'Override Fade Start Key?', 'Yes', 'No')
                #if messagebox == 1:
                    #self.delKey1()
                    #self.setKeyAtCurrentFrame()                    
           

    def setKey2(self):
        
        if len(self.stopKey) == 0:
            self.setKeyAtCurrentFrame()
        else:
            messagebox = FBMessageBox('Warning', 'Override current key?', 'Yes', 'No')
            
            if messagebox == 1:
                self.delKey2()
                self.setKey2()
                
    def delKey1(self):
        if len(self.startKey) == 0:
            FBMessageBox('Warning', 'No key to delete', 'OK')
        else:
            self.startKey = self.deleteKey(self.startKey)
            self.startKey = []

    def delKey2(self):
        
        if len(self.stopKey) == 0:
            FBMessageBox('Warning', 'No key to delete', 'OK')
        else:
            self.stopKey = self.deleteKey(self.stopKey)
            self.stopKey = []
        
    def refreshKeys(self):
        FBSystem().Scene.Evaluate()
        cam = self.GetExportCamera()
        if cam:
            self.startKey, self.stopKey = self.GetScreenFadeNodeKeys(cam)
        else:
            self.startKey = []
            self.stopKey = []
        self.SetButtons(self.startKey, self.stopKey)
        
        if len(self.startKey) != 0 and len(self.stopKey) != 0:
            self.sanityCheckButton.setEnabled(True)
        else:
            self.sanityCheckButton.setEnabled(False)
        
        
    def GetCurrentFrame(self):
        lCurrentFrame = self.lSystem.LocalTime.GetFrame()
        return lCurrentFrame
        
    def setKeyAtCurrentFrame(self, value = 0.0):
        # Need the current frame and the Transparency value from the spinner to set the key here        
        # First, what frame are we on?
        frame = self.GetCurrentFrame()
        
        # What is the current Transparency value in the spinner?
        val = self.transparencyValue.value()
        self.status.setText('Frame ' + unicode(frame) + ', Value ' + unicode(val))
        
        # Add a key at these settings
        if self.transparencyNode:
            self.status.setText('frame class name is ' + unicode(self.transparencyNode.Nodes))
            
            nodes = self.transparencyNode.Nodes
            
            for node in nodes:
                
                node.KeyAdd(FBTime(0, 0, 0, frame), val)

        self.refreshKeys()
        
        # switch the spinner value
        if self.transparencyValue.value() == 0:
            self.transparencyValue.setProperty("value", 1)
        else:
            self.transparencyValue.setProperty("value", 0)

        return
    
    def checkIfKeysCorrect(self):
        reversed = False
        
        if len(self.startKey) != 0 and len(self.stopKey) != 0:
            if self.startKey[0].Value == self.stopKey[0].Value:
                messagebox = FBMessageBox("Sanity Check", "Keys set for Screen Fade appear to be the same\n and should be fixed before export", 'OK')
                reversed = True
            if self.fadeInButton.isChecked(): 
                #if FadeIn, self.startKey value will be 0 and self.stopKey value will be 1
                #if concat, self.stopKey will == None
                if self.startKey[0].Value > self.stopKey[0].Value:
                    messagebox = FBMessageBox("Sanity Check", "Keys set for Screen Fade appear to be reversed.\nShall I fix them?", 'Yes', 'No' )
                    reversed = True
                    if messagebox == 1:
                        self.startKey[0].Value = 0
                        self.startKey[1].Value = 0
                        self.startKey[2].Value = 0
                        self.stopKey[0].Value = 1
                        self.stopKey[1].Value = 1
                        self.stopKey[2].Value = 1 
                        
            elif self.fadeOutButton.isChecked():
                # if FadeOut, self.stopKey value will be 0 and self.startKey value will be 1
                if self.stopKey[0].Value > self.startKey[0].Value:
                    messagebox = FBMessageBox("Sanity Check", "Keys set for Screen Fade appear to be reversed.\nShall I fix them?", 'Yes', 'No' )
                    reversed = True
                    if messagebox == 1:
                        self.startKey[0].Value = 1
                        self.startKey[1].Value = 1
                        self.startKey[2].Value = 1
                        self.stopKey[0].Value = 0
                        self.stopKey[1].Value = 0
                        self.stopKey[2].Value = 0 
                        
            elif self.concatButton.isChecked():
                messagebox = FBMessageBox("Sanity Check", "One too many keys for a concat.\nHow shall we proceed?", 'Delete first key', 'Delete second key', 'Do nothing' )
                reversed = True
                if messagebox == 1:
                    self.delKey1()
                elif messagebox == 2:
                    self.delKey2()

        if reversed:
            self.refreshKeys()
        else:
            messagebox = FBMessageBox("Sanity Check", "Key values look correct", 'OK') 
                    
        
    def deleteKeyAtCurrentFrame(self, thisKeyFrame):
     
        
        if self.transparencyNode:
            nodes = self.transparencyNode.Nodes
            
            for node in nodes:
                lText = (unicode(thisKeyFrame[0].Value) + ' ' + unicode(thisKeyFrame[0].Time.GetFrame())
                + ' ' + unicode(thisKeyFrame[1].Value) + ' ' + unicode(thisKeyFrame[1].Time.GetFrame()) + ' ' 
                + unicode(thisKeyFrame[2].Value) + ' ' + unicode(thisKeyFrame[2].Time.GetFrame()))
                
                node.KeyRemoveAt(FBTime(0, 0, 0, thisKeyFrame[2].Time.GetFrame()))
                self.status.setText(lText)
        
        FBSystem().Scene.Evaluate() 

        self.refreshKeys()
        
    def deleteKey(self, thisKey):
        # what frame is thisKey at? 
       
        if self.transparencyNode:
            nodes = self.transparencyNode.Nodes
            
            for node in nodes:
                self.status.setText(unicode(thisKey[0].Value) + ' ' + unicode(thisKey[0].Time.GetFrame())
                + ' ' + unicode(thisKey[1].Value) + ' ' + unicode(thisKey[1].Time.GetFrame()) + ' ' 
                + unicode(thisKey[2].Value) + ' ' + unicode(thisKey[2].Time.GetFrame()))
                node.KeyRemoveAt(FBTime(0, 0, 0, thisKey[2].Time.GetFrame()))
        
        FBSystem().Scene.Evaluate() 

        self.refreshKeys()

    def SceneChanged(self, scene, event):
        if event.ChildComponent != None and event.ChildComponent.Is(FBCamera_TypeInfo()):
            if event.ChildComponent.Name == "ExportCamera":
                if gDevelopment:
                    print "ExportCamera", event.Type 
                if event.Type == FBSceneChangeType.kFBSceneChangeRemoveChild or event.Type == FBSceneChangeType.kFBSceneChangeDetach:
                    self.refreshKeys()
                
        
    def OnFileNewCallback(self, control, event):
        if gDevelopment:
            print 'OnFileNewCallback'
        self.refreshKeys() 
        
    def OnFileOpenCallback(self, control, event):
        if gDevelopment:
            print 'OnFileOpenCallback'
        self.refreshKeys()
    
    def OnConnectionDataNotify(self, control, event):
        if gDevelopment:
            print 'OnConnectionDataNotify'
        print event.Plug.Name
        if self.mAcceptCallback and event.Action == FBConnectionAction.kFBCandidated and event.Plug.Name == 'ExportCamera':
            if event.Plug.Data == True:
                self.refreshKeys() 
                
    def OnConnectionStateNotifyCallback(self, control, event):
        
        if event.Action == FBConnectionAction.kFBDestroy:
            if gDevelopment:
                print event.Plug.ClassName(), event.Action
            
            if event.Plug.ClassName() == 'FBCamera':
                #self.camera = event.Plug
                self.refreshKeys()

                
                    
    def Register(self):
        # Register for scene event
        self.mSystem.Scene.OnChange.Add(self.SceneChanged)
        self.mSystem.OnConnectionStateNotify.Add(self.OnConnectionStateNotifyCallback)
        self.mApp.OnFileNewCompleted.Add(self.OnFileNewCallback)
        self.mApp.OnFileOpenCompleted.Add(self.OnFileOpenCallback)
        #self.ConnectionDataNotify(True)
        self.mApp.OnFileExit.Add(self.Unregister)
        
    def Unregister(self, control = None, event = None):
        if gDevelopment:
            print 'Unregistering stuff'
        FBSystem().Scene.Evaluate()
        self.mSystem.Scene.OnChange.Remove(self.SceneChanged)
        self.mSystem.OnConnectionStateNotify.Remove(self.OnConnectionStateNotifyCallback)
        self.mApp.OnFileNewCompleted.Remove(self.OnFileNewCallback)
        self.mApp.OnFileExit.Remove(self.Unregister)

    """
    ---------------------------------------------------------------------------
    """

def Run( show = True ):
    if gDevelopment:
        cmds.ConnectToWing()
    form = MainWindow()
    form.show()
           

