'''
Script Name     RSGTOR_PhaseCalculator.py
Author          Stephen Sloper, Rockstar Toronto.  (Modified by Kristine Middlemiss to be compliant with MotionBuilder 2014)

Description     Calculates the phase value of a take. Similarily, will also calculate the frame number from the phase value.
'''

from pyfbsdk import *
from pyfbsdk_additions import *

import sys

import RS.Utils.Creation
import RS.Tools.UI

class PhaseCalculatorTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Phase Calculator', size = [ 295, 267 ])
                                   
    def RSGTOR_PhaseCalculator_doMath(self,control, event):
        startFrame = g_phaseCalculator_startNum.Value
        endFrame = g_phaseCalculator_endNum.Value
        if g_phaseCalculator_phaseRadioBut.State == True:
            currentFrame = g_phaseCalculator_frameNum.Value
            '''
            #math to normalise a number: phase = (value - min)/(max - min)
            '''
            a = currentFrame - startFrame
            b = endFrame - startFrame
	    # We can't divide by zero!
	    if float(b) != 0:
		normalizedValue = float(a)/float(b)
		g_phaseCalculator_phaseNum.Value = normalizedValue
        else:
            #math to normalise a number: frame = ((max - min) * phase) + min
            phase = g_phaseCalculator_phaseNum.Value
            a = endFrame - startFrame
            b = a * phase
            frameNum = b + startFrame
            g_phaseCalculator_frameNum.Value = frameNum
    
    def RSGTOR_PhaseCalculator_useTakeValues(self,control, event):
        currentTake = FBSystem().CurrentTake
        startFrame = currentTake.LocalTimeSpan.GetStart().GetFrame()
        endFrame = currentTake.LocalTimeSpan.GetStop().GetFrame()
        currentFrame = FBSystem().LocalTime.GetTimeString()
        currentFrame = currentFrame.replace('*','')
        #FIND NEW WAY OF FINDING CURRENT FRAME, MUST GET SUBFRAME TOO
        
        g_phaseCalculator_startNum.Value = startFrame
        g_phaseCalculator_endNum.Value = endFrame
        
        if g_phaseCalculator_phaseRadioBut.State == True:
            g_phaseCalculator_frameNum.Value = int(currentFrame)
            g_phaseCalculator_phaseNum.ReadOnly = True
            g_phaseCalculator_frameNum.ReadOnly = False
        else:
            g_phaseCalculator_phaseNum.ReadOnly = False
            g_phaseCalculator_frameNum.ReadOnly = True
    
    def RSGTOR_PhaseCalculator_setPhaseMode(self, control, event):
        g_phaseCalculator_phaseNum.ReadOnly = True
        g_phaseCalculator_frameNum.ReadOnly = False
    
    def RSGTOR_PhaseCalculator_setFrameMode(self, control, event):
        g_phaseCalculator_phaseNum.ReadOnly = False
        g_phaseCalculator_frameNum.ReadOnly = True
        
    def RSGTOR_PhaseCalculator_goToFrame(self, control, event):
        frame = g_phaseCalculator_frameNum.Value
    	FBPlayerControl().Goto(FBTime(0,0,0,int(round(frame))))
    
    def Create(self, ui):
        #create the main adjustable layout --------------------------------------------------
        al = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        ui.AddRegion("main","main", al, at, ar, ab)
        manualLyt = FBVBoxLayout()
        ui.SetControl("main",manualLyt)
        
        butGroup = FBButtonGroup()

	global g_phaseCalculator_phaseRadioBut        
        g_phaseCalculator_phaseRadioBut = FBButton()
        g_phaseCalculator_phaseRadioBut.Caption = 'Calculate Phase'
        g_phaseCalculator_phaseRadioBut.Style = FBButtonStyle.kFBRadioButton
        g_phaseCalculator_phaseRadioBut.State = True
        g_phaseCalculator_phaseRadioBut.OnClick.Add(self.RSGTOR_PhaseCalculator_setPhaseMode)
        butGroup.Add(g_phaseCalculator_phaseRadioBut)
        
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(30 ,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(100,FBAttachType.kFBAttachNone,"")
        an = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        ui.AddRegion("g_phaseCalculator_phaseRadioBut","g_phaseCalculator_phaseRadioBut", al, at, ar, an)
        ui.SetControl("g_phaseCalculator_phaseRadioBut",g_phaseCalculator_phaseRadioBut)
        
	global g_phaseCalculator_frameRadioBut        
        g_phaseCalculator_frameRadioBut = FBButton()
        g_phaseCalculator_frameRadioBut.Caption = 'Calculate Frame'
        g_phaseCalculator_frameRadioBut.Style = FBButtonStyle.kFBRadioButton
        g_phaseCalculator_frameRadioBut.OnClick.Add(self.RSGTOR_PhaseCalculator_setFrameMode)
        butGroup.Add(g_phaseCalculator_frameRadioBut)
        
        al = FBAddRegionParam(150,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(30 ,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(100,FBAttachType.kFBAttachNone,"")
        an = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        ui.AddRegion("g_phaseCalculator_frameRadioBut","g_phaseCalculator_frameRadioBut", al, at, ar, an)
        ui.SetControl("g_phaseCalculator_frameRadioBut",g_phaseCalculator_frameRadioBut)
        
	#create the seperator1  layout -------------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(60,FBAttachType.kFBAttachTop,"")
        seperator1Lyt = FBHBoxLayout()
        ui.AddRegion("seperator1","seperator1", al, at, ar, ab)
        ui.SetControl("seperator1",seperator1Lyt)
        
        sep1Label = FBLabel()
        sep1Label.Caption = '--------------------------------------------------------------'
        seperator1Lyt.Add(sep1Label,370)
        
        #create the label layout ---------------------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(80,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        labelLyt = FBHBoxLayout()
        ui.AddRegion("label","label", al, at, ar, ab)
        ui.SetControl("label",labelLyt)
    
        startLabel = FBLabel()
        startLabel.Caption = 'Start'
        labelLyt.Add(startLabel,60)
    
        endLabel = FBLabel()
        endLabel.Caption = 'End'
        labelLyt.Add(endLabel,60)
        
        frameLabel = FBLabel()
        frameLabel.Caption = 'Frame'
        labelLyt.Add(frameLabel,60) 
        
        phaseLabel = FBLabel()
        phaseLabel.Caption = 'Phase'
        labelLyt.Add(phaseLabel,60)
    
        #create the edit number layout ---------------------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(100,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        editNumberLyt = FBHBoxLayout()
        ui.AddRegion("editNum","editNum", al, at, ar, ab)
        ui.SetControl("editNum",editNumberLyt)
    
	global g_phaseCalculator_startNum        
        g_phaseCalculator_startNum = FBEditNumber()
        g_phaseCalculator_startNum.Value = 0
        g_phaseCalculator_startNum.Precision = 0
        g_phaseCalculator_startNum.Min = 0
        g_phaseCalculator_startNum.SmallStep = 10
        editNumberLyt.Add(g_phaseCalculator_startNum,59)
        
	global g_phaseCalculator_endNum
        g_phaseCalculator_endNum = FBEditNumber()
        g_phaseCalculator_endNum.Value = 0
        g_phaseCalculator_endNum.Precision = 0
        g_phaseCalculator_endNum.Min = 0
        g_phaseCalculator_endNum.SmallStep = 10
        editNumberLyt.Add(g_phaseCalculator_endNum,59)
        
	global g_phaseCalculator_frameNum        
        g_phaseCalculator_frameNum = FBEditNumber()
        g_phaseCalculator_frameNum.Value = 1
        g_phaseCalculator_frameNum.Precision = 0.2
        g_phaseCalculator_frameNum.Min = 0
        g_phaseCalculator_frameNum.SmallStep = 10
        editNumberLyt.Add(g_phaseCalculator_frameNum,59)
        
	global g_phaseCalculator_phaseNum
        g_phaseCalculator_phaseNum = FBEditNumber()
        g_phaseCalculator_phaseNum.Value = 0
        g_phaseCalculator_phaseNum.Precision = 0.4
        g_phaseCalculator_phaseNum.Min = 0
        g_phaseCalculator_phaseNum.Max = 1
        g_phaseCalculator_phaseNum.LargeStep = 0.001
        editNumberLyt.Add(g_phaseCalculator_phaseNum,59)
        
        at = FBAddRegionParam(120,FBAttachType.kFBAttachTop,"")
        seperator2Lyt = FBHBoxLayout()
        ui.AddRegion("seperator2","seperator2", al, at, ar, ab)
        ui.SetControl("seperator2",seperator2Lyt)
        
        #create the seperator2 layout ---------------------------------------------------------------
        sep2Label = FBLabel()
        sep2Label.Caption = '--------------------------------------------------------------'
        seperator2Lyt.Add(sep2Label,370)
        
        #create the button layout ---------------------------------------------------------------
        at = FBAddRegionParam(135,FBAttachType.kFBAttachTop,"")
        ab = FBAddRegionParam(100,FBAttachType.kFBAttachNone,"")
        calcButLyt = FBVBoxLayout()
        ui.AddRegion("calcBut","calcBut", al, at, ar, ab)
        ui.SetControl("calcBut",calcButLyt)
        
        refreshBut = FBButton()
        refreshBut.Caption = 'Insert Values from Current Take'
        refreshBut.OnClick.Add(self.RSGTOR_PhaseCalculator_useTakeValues)
        calcButLyt.Add(refreshBut,20)
    
        manualCalcBut = FBButton()
        manualCalcBut.Caption = '- CALCULATE -'
        manualCalcBut.OnClick.Add(self.RSGTOR_PhaseCalculator_doMath)
        manualCalcBut.Look = FBButtonLook.kFBLookColorChange
        manualCalcBut.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.3, 0.6, 0.6))
        calcButLyt.Add(manualCalcBut,40)
        
        goToBut = FBButton()
        goToBut.Caption = 'Go to frame'
        goToBut.OnClick.Add(self.RSGTOR_PhaseCalculator_goToFrame)
        calcButLyt.Add(goToBut,20)

def Run( show = True ):
    tool = PhaseCalculatorTool()
    
    if show:
        tool.Show()
        
    return tool