"""
The ModelItem that wraps around Components.MovingCutscene objects
"""
import os

from PySide import QtGui, QtCore

from RS import Config
from RS.Tools.UI.MovingCutscene.Model import SetupModelItem
from RS.Utils.Scene import Component


class MovingCutsceneModelItem(SetupModelItem.SetupModelItem):

    def data(self, index=None, role=QtCore.Qt.DisplayRole):
        """
        Overrides inherited method

        Displays data based on the index and role passed in by the view

        Arguments:
        index (QtCore.QModelIndex): index being called in the view
            role (QtCore.Qt.Role): role being requested by the view

        Return:
            object
        """
        column = index.column() if index is not None else 0
        if role == self.ItemRole:
            return self

        elif role == self.DataRole:
            return self._data

        elif role == QtCore.Qt.DisplayRole and self._headers:
            return self._headers[column]

        elif role == QtCore.Qt.DisplayRole:
            marker = self._data.marker()
            zeroNull = self._data.zeroNull()
            freezeNull = self._data.freezeNull()
            constraintZero = zeroNull.constraint() or zeroNull.driverConstraint()
            constraintFreeze = freezeNull.constraint() or freezeNull.driverConstraint()

            if marker is None:
                return "--- DELETED ---"
            elif column == 0:
                return self._data.name()
            elif column == 1:
                return ("Unselected", "Selected")[self._data.marker().Selected]
            if constraintZero is not None and constraintFreeze is not None:
                if column == 2:
                    return ("Disabled", "Active")[not any((constraintZero.Active, constraintFreeze.Active))]
                elif column == 3:
                    null = self._data._lastActive
                    constraint = null.constraint() or null.driverConstraint()
                    return constraint.Weight.Data

            if column < 4:
                return "N/A"

        elif role == QtCore.Qt.EditRole:
            if column == 0:
                return self._data.name()
            if column == 3:
                null = self._data._lastActive
                constraint = null.constraint() or null.driverConstraint()
                return constraint.Weight.Data if constraint else 0

        elif role == QtCore.Qt.DecorationRole and not column:
            return self.FolderIcon

        elif role == QtCore.Qt.DecorationRole:
            marker = self._data.marker()
            data = self._data.masterNull()
            zeroNull = self._data.zeroNull()
            freezeNull = self._data.freezeNull()

            component = data.component()
            zeroConstraint = zeroNull.constraint() or zeroNull.driverConstraint()
            freezeConstraint = freezeNull.constraint() or freezeNull.driverConstraint()

            if not column:
                if not component:
                    return self.FolderIcon
                icon = self._Icons.get(component.__class__, None)
                if not icon:
                    icon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "motionbuilder", "{}.png".format(component.__class__.__name__[2:].lower())))
                    self._Icons[component.__class__] = icon
                return icon

            elif column == 1:
                if marker is not None and marker.Selected:
                    return self.ActiveIcon
                return self.InactiveIcon

            elif column == 2:
                destroyedZero = zeroConstraint and Component.IsDestroyed(zeroConstraint)
                destroyedFreeze = freezeConstraint and Component.IsDestroyed(freezeConstraint)
                zeroActive = False if not zeroConstraint else not zeroConstraint.Active
                freezeActive = False if not freezeConstraint else not freezeConstraint.Active

                if all((not destroyedZero, zeroConstraint, zeroActive,
                       not destroyedFreeze, freezeConstraint, freezeActive)):
                    return self.ActiveIcon

                elif not zeroConstraint and not freezeConstraint:
                    return self.UnavailableIcon

                return self.InactiveIcon
            return self.UnavailableIcon

        elif role == QtCore.Qt.BackgroundRole and not column:
            movingcutscene = self._data
            data = self._data._lastActive

            active = movingcutscene.active()

            if data.driver() and data.driverConstraint():
                return QtGui.QBrush(self.BlueColor)
            elif not data.hasConstraint():
                return QtGui.QBrush(self.YellowColor)
            elif active is not None and active:
                return QtGui.QBrush(self.RedColor)
            elif data.hasConstraint():
                return QtGui.QBrush(self.GreenColor)

        return self._roles.get(role, None)

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        Overrides inherited method

        Sets data on the item based on the index and role passed in by the view

        Arguments:
            index (QtCore.QModelIndex): index used by the view
            value (object): value to store/set
            role (QtCore.Qt.Role): role being requested by the view
        """
        if role == self.DataRole:
            self._data = value

        elif role == QtCore.Qt.EditRole:

            if index and index.column() == 0:
                self._data.setName(str(value))

            if index and index.column() == 3:
                null = self._data._lastActive
                constraint = null.constraint() or null.driverConstraint()
                if constraint:
                    constraint.Weight.Data = value

        else:
            self._roles[role] = value

        return True
