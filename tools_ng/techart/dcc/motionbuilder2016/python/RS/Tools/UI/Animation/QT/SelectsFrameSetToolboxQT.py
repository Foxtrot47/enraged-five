'''

 Script Path: RS/Tools/UI/Animation/QT/SelectsFrameSetToolboxQT.py

 Written And Maintained By: Kathryn Bodey

 Created: 15.07.14

 Description: 

'''

from pyfbsdk import *
from pyfbsdk_additions import *

from PySide import QtGui
from PySide import QtCore

import RS.Config
import RS.Tools.UI
import RS.Core.Animation.SelectsFrameSet as core

reload( core )

RS.Tools.UI.CompileUi( '{0}\\RS\\Tools\\UI\\Animation\\QT\\designer_SelectsFrameSetToolbox.ui'.format( RS.Config.Script.Path.Root ), '{0}\\RS\\Tools\\UI\\Animation\\QT\\SelectsFrameSetToolbox_compiled.py'.format( RS.Config.Script.Path.Root ) )

import RS.Tools.UI.Animation.QT.SelectsFrameSetToolbox_compiled as toolboxQT

reload( toolboxQT )


class SelectsFrameSetToolbox(  RS.Tools.UI.QtMainWindowBase, toolboxQT.Ui_MainWindow ):
	def __init__(self, parent = None):

		RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = 'Plot Selected Takes' )

		# build designer complied UI
		self.setupUi( self )

		# class variables
		self.RangeList = []
		self.PropertyLimit = 10
		self.SelectFrameRangeNull = None
		self.RangeSelectDict = {}
		self.UserLabelDict = {}
		self.EnumList = []
		self.ButtonList = []
		self.SoftRangeButtonList = []

		# generate existing data
		self.GetSelectsNullData()
		
		# re build constraint each time the UI updates
		if self.SelectFrameRangeNull:
			core.CreateSelectsRangesConstraint( self.SelectFrameRangeNull )
		
		# hide options group boxes
		self.SelectsOptionsGroupBox.setVisible(False)
		self.SelectsSaveLoadGroupBox.setVisible(False)
		
		# set max & min for frame edit boxes	
		startTime = FBPlayerControl().LoopStart
		sceneStartHardRange = startTime.GetFrame()
		endTime = FBPlayerControl().LoopStop
		sceneEndHardRange = endTime.GetFrame()
		
		self.StartFrameSpinBox.setMaximum( int( sceneEndHardRange ) )
		self.StartFrameSpinBox.setMinimum ( int( sceneStartHardRange ) )
		self.EndFrameSpinBox.setMaximum( int( sceneEndHardRange ) )
		self.EndFrameSpinBox.setMinimum( int( sceneStartHardRange ) )	

		# create banner
		def BuildUI_Banner():
			verticalLayoutWidget_4 = QtGui.QWidget(self.centralwidget)
			verticalLayoutWidget_4.setGeometry(QtCore.QRect(-1, -1, 530, 30))
			verticalLayoutWidget_4.setObjectName("verticalLayoutWidget_4")		
			vBoxLayoutBanner = QtGui.QVBoxLayout( verticalLayoutWidget_4 )
			vBoxLayoutBanner.setContentsMargins( 0, 0, 0, 0 )
			banner = RS.Tools.UI.BannerWidget(name = 'Range Select Toolbox', helpUrl ="")
			vBoxLayoutBanner.addWidget ( banner )	

		BuildUI_Banner()
		
		# create & populate RangesScrollArea
		def PopulateRangeDisplay():
			# build scroll area
			self.RangesScrollArea = QtGui.QScrollArea( self.RangeSelectsGroupBox )
			self.RangesScrollArea.setGeometry( QtCore.QRect( 10, 70, 491, 265 ) )
			self.RangesScrollArea.setWidgetResizable( False )
			
			scrollSize = 121
			dictLength = len( self.RangeSelectDict )
			if dictLength > 3:
				excessList = dictLength - 3
				scrollSize = scrollSize + ( excessList * 28 ) 	
				
			self.scrollAreaWidgetContents = QtGui.QWidget()
			self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 470, scrollSize))
			
			self.RangesScrollArea.setWidget(self.scrollAreaWidgetContents)		
			
			#add reset soft ranges button
			self.ResetSoftRangePushButton = QtGui.QPushButton( self.RangeSelectsGroupBox )
			self.ResetSoftRangePushButton.setGeometry( QtCore.QRect( 208, 50, 58, 14 ) )
			self.ResetSoftRangePushButton.setText( "reset" )			
			
			# set select data
			counter = 10
			labelCounter = 10
			for key, value in self.RangeSelectDict.iteritems():	
				index = str( key ) + "."
				startFrame = str( value[0] )
				endFrame = str( value[1] )
				label = str( value[2] )
	
				self.IndexLabel = QtGui.QLabel( self.scrollAreaWidgetContents )
				self.IndexLabel.setGeometry( QtCore.QRect( 5, counter, 191, 16 ) )
				self.IndexLabel.setText( index )
	
				self.StartPushButton = QtGui.QPushButton( self.scrollAreaWidgetContents )
				self.StartPushButton.setGeometry( QtCore.QRect( 33, counter, 51, 18 ) )
				self.StartPushButton.setText( startFrame )			
				self.ButtonList.append( self.StartPushButton )			
				
				self.EndPushButton = QtGui.QPushButton( self.scrollAreaWidgetContents )
				self.EndPushButton.setGeometry( QtCore.QRect( 106, counter, 61, 18 ) )
				self.EndPushButton.setText( endFrame )				
				self.ButtonList.append( self.EndPushButton )
				
				self.SoftRangePushButton = QtGui.QPushButton( self.scrollAreaWidgetContents )
				self.SoftRangePushButton.setGeometry( QtCore.QRect(183, counter, 91, 18 ) )
				self.SoftRangePushButton.setText( startFrame + '~' + endFrame )
				self.SoftRangeButtonList.append( self.SoftRangePushButton )
				
				self.Label = QtGui.QLabel( self.scrollAreaWidgetContents )
				self.Label.setGeometry( QtCore.QRect( 290, counter, 191, 16 ) )
				self.Label.setText( label )		
				
				counter = counter + 25
		
		PopulateRangeDisplay()

		if self.RangeSelectDict:
			for key, value in self.RangeSelectDict.iteritems():
				index = key
				startFrame = str( value[0] )
				endFrame = str( value[1] )
				label = str( value[2] )		
				
				string = 'Start:{0}  End:{1}  Label:{2}'.format( startFrame, endFrame, label )
				self.SelectsOptionsComboBox.addItem( string )			

		# event controls
		self.pushButtonRefresh.pressed.connect( self.Refresh )	
		self.StartCurrentFramePushButton.pressed.connect( self.CurrentStartFrame )
		self.EndCurrentFramePushButton.pressed.connect( self.CurrentEndFrame )
		self.SelectsOptionsPushButton.pressed.connect( self.GroupVisibility )
		self.AddRangePushButton.pressed.connect( self.AddRange )
		self.ResetSoftRangePushButton.pressed.connect( self.ResetSoftRanges )
		self.DeleteSelectsPushButton.pressed.connect( self.DeleteRange )
		self.CreateTakePushButton.pressed.connect( self.CreateNewTake )
		self.SaveSelectsPushButton.pressed.connect( self.SaveSelects )
		self.LoadSelectsPushButton.pressed.connect( self.LoadSelects )
		for i in self.ButtonList:
			i.pressed.connect( self.GotToFrame )
		for i in self.SoftRangeButtonList:
			i.pressed.connect( self.SetSoftRanges )
			

	''' 
	Callbacks  
	'''
	
	def GetSelectsNullData( self ):
		# find selects_frame_ranges
		for i in RS.Globals.gComponents:
			hudProperty = i.PropertyList.Find( 'Frame Selects' )
			
			if hudProperty:
				self.SelectFrameRangeNull = i
				
		if self.SelectFrameRangeNull:
			
			# update old setup if null was generated from v1 of the tool
			core.UpdateOldSetup( self.SelectFrameRangeNull )			
			
			# gather information on existing selects
			index = 0
			for i in self.SelectFrameRangeNull.PropertyList:
				if i.Name.startswith( 'Select Range' ):
					frameSplit = i.Data.split( '~' )
					startFrame = frameSplit[0]
					endFrame = frameSplit[1]
					label = frameSplit[-1]
					
					self.RangeSelectDict[index + 1] = ( startFrame, endFrame, label )
					
					index = index + 1
					
				# populate 'selects options' combo box
				if i.Name == 'HUD Text Display':
					enumList = i.GetEnumStringList(True)
					for i in enumList:
						self.EnumList.append( i )			
		

	def Refresh( self ):
		self.close()
		Run()	
		

	def GroupVisibility( self ):
		if self.sender() == self.SelectsOptionsPushButton:
			if self.SelectsOptionsGroupBox.isVisible():
				self.SelectsOptionsGroupBox.setVisible(False)
				self.SelectsSaveLoadGroupBox.setVisible(False)
				self.SelectsOptionsPushButton.setText('+' )
			else:
				self.SelectsOptionsGroupBox.setVisible(True)
				self.SelectsSaveLoadGroupBox.setVisible(True)
				self.SelectsOptionsPushButton.setText('-' )		
				

	def CurrentStartFrame( self ):
		self.StartFrameSpinBox.setValue( FBSystem().LocalTime.GetFrame() )


	def CurrentEndFrame( self ):
		self.EndFrameSpinBox.setValue( FBSystem().LocalTime.GetFrame() )


	def GotToFrame( self ):
		buttonText = self.sender().text()		
		frame = FBTime( 0,0,0, int( buttonText ) )	
		playerControl = FBPlayerControl()
		playerControl.Goto( frame )
		
		
	def SetSoftRanges( self ):
		buttonText = self.sender().text()	
		splitText = buttonText.split( '~' )
		startText = splitText[0]
		endText = splitText[1]
		startFrame = FBTime( 0,0,0, int( startText ) )	
		endFrame = FBTime( 0,0,0, int( endText ) )			
		startTime = startFrame.Get() 
		endTime = endFrame.Get() 
		playerControl = FBPlayerControl()
		playerControl.ZoomWindowStart = FBTime( startTime )
		playerControl.ZoomWindowStop = FBTime( endTime )		


	def DeleteLabel( self ):
		# delete by index
		selectionText = self.LableEditComboBox.currentText()
		selectionIndex = self.LableEditComboBox.currentIndex()

		for i in self.SelectFrameRangeNull.PropertyList:
			if i.Name == 'HUD Text Display':
				enumList = i.GetEnumStringList(True)
				enumList.Remove( str( selectionText ) )
		

	def ValidityPopupMessage( self, label=True, frame=True, labelLimit=True, ranges=True, numberOfEntries=True,  ):
		
		errorString = None
		
		if label == True and frame == True and labelLimit == True and ranges == True and numberOfEntries == True:
			None
		else:
			invalidLabelString = ''
			invalidFrameString = ''
			invalidLabelLimitString = ''
			invalidRangeString = ''
			invalidNumberOfEntriesString = ''
			
			if numberOfEntries == False:
				invalidNumberOfEntriesString = 'Invalid Number of Entries: You cannot have more than 20 Selects at a time.\nContact Tech Art if more entries are required.\n\n'			
			if label == False:
				invalidLabelString = 'Invalid Label: You cannot leave the label blank.\n\n'
			if frame == False:
				invalidFrameString = 'Invalid Frame: You cannot have the end range lower than the start range.\n\n'
			if labelLimit == False:
				invalidLabelLimitString = 'Invalid Label Limit: Label is too long. You have entered {0} characters,\nthere is a 30 character limit.\n\n'.format( len( self.LableLineEdit.text() ) )
			if ranges == False:
				invalidRangeString = 'Invalid Range: Entered Select falls into the same range as an exisiting Select.\nPlease do not overlap ranges.\n\n'

			errorString = invalidNumberOfEntriesString + invalidLabelString + invalidFrameString + invalidLabelLimitString + invalidRangeString
						
		return errorString 
		

	def AddRange( self ):	
		# check data is valid
		validLabel = True	
		validFrame = True
		validLabelLimit = True
		validNumberOfEntries = True
		validRange = True
		
		if self.LableLineEdit.text() == '':
			validLabel = False
			
		if self.EndFrameSpinBox.value() < self.StartFrameSpinBox.value():
			validFrame = False	
			
		if len( self.LableLineEdit.text() ) > 30:
			validLabelLimit = False			
		
		if len( self.SoftRangeButtonList ) == 20:
			validNumberOfEntries = False
			
		for key, value in self.RangeSelectDict.iteritems():
			existingStartFrame = value[0]
			existingEndFrame = value[1]
			startFrame = self.StartFrameSpinBox.value()
			endFrame = self.EndFrameSpinBox.value()			
			
			if int( existingStartFrame ) <= int( startFrame ) <= int( existingEndFrame ):
				validRange = False
			elif int( existingStartFrame ) <= int( endFrame ) <= int( existingEndFrame ):
				validRange = False
			
		# populate error message if any of the user-entered options are invalid
		result = self.ValidityPopupMessage( label=validLabel, frame=validFrame, labelLimit=validLabelLimit, ranges=validRange, numberOfEntries=validNumberOfEntries )	
		

		if result == None:
			startFrame = self.StartFrameSpinBox.value()
			endFrame = self.EndFrameSpinBox.value()
			label = str( self.LableLineEdit.text() )
			
			# create/update self.SelectFrameRangeNull			
			if self.SelectFrameRangeNull:
				enumProperty = self.SelectFrameRangeNull.PropertyList.Find( 'HUD Text Display' )
				if enumProperty:
					enumList = enumProperty.GetEnumStringList(True)
					enumList.Add( label )			
								
				# add new property range to existing null
				propertyData = core.AddFrameProperty( self.SelectFrameRangeNull, startFrame, endFrame, label )        
				
			else:
				# create the selects_frame_ranges null and add initial property
				self.SelectFrameRangeNull = core.CreateSelectsRangesNull()
				
				enumProperty = self.SelectFrameRangeNull.PropertyList.Find( 'HUD Text Display' )
				if enumProperty:
					enumList = enumProperty.GetEnumStringList(True)
					enumList.Add( label )	
					
				propertyData = core.AddFrameProperty( self.SelectFrameRangeNull, startFrame, endFrame, label ) 
			
			# reset label edit box
			self.LableLineEdit.setText( "" )
			
			self.Refresh()	
		
		else:
			FBMessageBox( 'R* Error', '{0}'.format( result ), 'Ok' )
							
		
	def DeleteRange( self ):
		
		result = FBMessageBox( 'R* Warning', 'Are you sure you wish to delete this Select?', 'Yes', 'No' )		
		if result == 1:
			currentSelection = self.SelectsOptionsComboBox.currentText()
			if currentSelection != '':
				split = currentSelection.split( '  ' )
				startSplit = split[0].split( ':' )
				endSplit = split[1].split( ':' )
				labelSplit = split[-1].split( ':' )
				
				start = startSplit[-1]
				end = endSplit[-1]
				label = labelSplit[-1]
				
				string = start + '~' + end + '~' + label
						
				for i in self.SelectFrameRangeNull.PropertyList:
					if i.Name.startswith( 'Select Range' ):
						if i.Data == string:
							self.SelectFrameRangeNull.PropertyRemove( i )
						
				self.Refresh()	
		

	def ResetSoftRanges( self ):
		playerControl = FBPlayerControl()
		start = FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame() 
		end = FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame() 
		startTime = FBTime( 0,0,0, int( start ) )
		endTime = FBTime( 0,0,0, int( end ) )

		startTime = startTime.Get() 
		endTime = endTime.Get() 
		playerControl.ZoomWindowStart = FBTime( startTime )
		playerControl.ZoomWindowStop = FBTime( endTime )  
		
			
	def CreateNewTake( self ):	
		
		currentSelection = self.SelectsOptionsComboBox.currentText()
		if currentSelection != '':
			split = currentSelection.split( '  ' )
			startSplit = split[0].split( ':' )
			endSplit = split[1].split( ':' )
			labelSplit = split[-1].split( ':' )
			
			start = startSplit[-1]
			end = endSplit[-1]
			label = labelSplit[-1]
			
			core.CreateNewTake( str( label ), int( start ), int( end ) )
			
	def SaveSelects( self ):
		
		core.CreateMetaFile( self.RangeSelectDict )
		
		
	def LoadSelects( self ):
		
		core.OpenMetaFile()
		self.Refresh()


def Run():   

	form = SelectsFrameSetToolbox()
	form.show()  

