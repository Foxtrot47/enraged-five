"""
RS.Tools.UI

Base class module for building GUI's.

Author:

    Jason Hayes <jason.hayes@rockstarsandiego.com>

Examples:

    # Native MotionBuilder tool.
    import RS.Tools.UI

    class MyTool(RS.Tools.UI.Base):
        def __init__(self):
            RS.Tools.UI.Base.__init__(self, 'My Awesome Tool')


    tool = MyTool()
    tool.Show(True)  # Set to True to have the previous tool destroyed.



    # PyQt-based MotionBuilder tool.  Requires you to subclass our QtBaseWidget class.  This version
    # embeds a QtWidget inside of an mobu.FBTool.
    import RS.Tools.UI

    # You can compile the .ui module into a .py file by doing this:
    RS.Tools.UI.CompileUi(theUiFilename, thePyFilename)

    # Need to import the compiled Qt Designer .ui Python module.
    import RS.Tools.UI.MyQtUiModule

    class MyQtWidget(RS.Tools.UI.QtBaseWidget, RS.Tools.UI.MyQtUiModule.Ui_Class):
        def __init__(self, parent):
            RS.Tools.UI.QtBaseWidget.__init__(self, parent)

        # Define any bound event handlers here that are in the .ui file.

    class MyTool(RS.Tools.UI.QtBase):
        def __init__(self):

            RS.Tools.UI.QtBase.__init__(self, 'My Awesome Tool', 'UiFilename')

    RS.Tools.UI.Show(MyTool)


    # PyQt-based MotionBuilder tool.  This creates a Qt Dialog, but the frame isn't dockable inside of MotionBuilder.
    import RS.Tools.UI

    # Inheriting from the .ui Python module is optional.
    class MyDialog(RS.Tools.UI.QtDialogBase, RS.Tools.UI.MyQtUiModule.Ui_Class):
        def __init__(self):
            RS.Tools.UI.QtDialogBase.__init__(self)

            # Call the .ui Python modules setup function to create the GUI.
            self.setupUi()

        # Define any bound event handlers here that are in the .ui file.
"""

# TODO: Empty init and move QT Base classes into their own module

import os
import re
import sys
import stat
import types
import inspect
import pysideuic
import webbrowser
from xml.etree import cElementTree as xml

import functools

from PySide import QtCore, QtGui, shiboken

from RS import Config
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize

import Application

try:
    import pyfbsdk as mobu
    import pyfbsdk_additions as mobu_

except ImportError:
    mobu = None

try:
    import pythonidelib
except:
    pythonidelib = None

# Globals
HELP_URL = "https://hub.rockstargames.com/category/rsg"
EMAIL = 'TECHART Support'

# backgroundColor, textColor = ProjectData.data.GetConfig("MenuColour", defaultValue=("#ffdc19", "black"))
BACKGROUND_COLOR, TEXT_COLOR = ("#ffdc19", "black") # Yellow and Black Text

# Decorators

DOCK_AREAS = {
              4: QtCore.Qt.TopDockWidgetArea,
              8: QtCore.Qt.BottomDockWidgetArea,
              2: QtCore.Qt.RightDockWidgetArea,
              1: QtCore.Qt.LeftDockWidgetArea
              }


def Run(title="", size=(100, 100), url=None, emailTo=EMAIL, emailCC='', dockable=True, dock=False,
        dockArea=QtCore.Qt.TopDockWidgetArea, dialog=False):
    """
    Decorator for showing custom Qt Widgets in Motion Builder.
    This decorator accepts arguments.

    Arguments:
        title (string): title of the window
        url (string): url to the help page on devstar
        email (string): email addresses to autofill emai with
        dockable (boolean): if the window is dockable
        dock (boolean): if the window should be docked when first displayed.
        dockArea (QCore.Qt.DockAre): where to dock the window.
                   Accepts QtCore.Qt.TopDockWidgetArea, QtCore.Qt.LeftDockWidgetArea, QtCore.Qt.RightDockWidgetArea,
                   QtCore.Qt.BottomDockWidgetArea
    Return:
        function
    """
    def funcWrap(func):
        """
        Stores the decorated function
        Arguments:
            func (method): the decorated function
        """
        def wrap(show=True, *args, **kwargs):
            """
            Wrapper for showing the widget of the decorated function.
            Arguments:
                show (boolean): boolean; should the UI be shown
                *args (list): arguments that the decorated function accepts
                *kwargs (dictionary): keyword arguments that the decorated function accepts

            return:
                QWidget()
            """

            # we can set the margins of the widget to zero to remove the empty border around widgets that appear when
            # they are added to layouts

            # We can also add logging here once we move forward with the tool bar menu.
            # Having logging tied to the show event in our base classes causes tools with multiple widgets that inherit
            # them to be logged multiple times. Right now we have logging enabled only through the Rockstar Menu, to
            # avoid the previous issue but it means that tools launched outside of it won't be logged.

            # Resolve tool title if no title is provided

            toolTitle = title
            functionModule = sys.modules[func.__module__]
            if not toolTitle:
                # Split camel case in the module name into a list where the case changes
                splitName = re.split("([A-Z][a-z0-9]+)", functionModule.__name__.split(".")[-1])
                # Add spaces between the changes in case and we throw way the last character from the title as it
                # will always be an empty space
                toolTitle = "".join("{} ".format(eachName) for eachName in splitName)[:-1]

            # Close previous version of the tool.
            # We call it outside the QtMainWindowBase class to store the state of the tool if it has been implemented
            Application.CloseToolByTitle(title)

            window = QtBannerWindowBase(parent=None, title=toolTitle, size=size, helpUrl=url, emailTo=emailTo,
                                        emailCC=emailCC, dockable=dockable, dock=dock,
                                        dockArea=dockArea, dialog=dialog, store=True, closeExistingWindow=False)
            # create widget
            widget = func(*args, **kwargs)

            # window.move(_position or window.pos())

            menuMethod = getattr(widget, "CreateMenu", None) or getattr(widget, "createMenu", None)
            if menuMethod is not None:
                # Add menu if a menu option exists
                menu = menuMethod()
                menu.setFixedHeight(18)

                window.Layout.addWidget(menu)
                window.Layout.setSpacing(0)
                window.Layout.setContentsMargins(0, 0, 0, 0)

                window.setStyleSheet("QMenuBar{"
                                     "background-color: %s;"
                                     "color: %s;"
                                     "font: bold 11px;"
                                     "}" % (BACKGROUND_COLOR, TEXT_COLOR) )

            toolbarMethod = getattr(widget, "setupToolBar", None) or getattr(widget, "SetupToolBar", None)
            if toolbarMethod is not None:
                widget.setupToolBar(window.Banner)

            window.Layout.addWidget(widget)
            window.setCloseEvent(widget.closeEvent)
            widget.setWindowTitle(toolTitle)

            if hasattr(widget, "storeWindowState"):
                window.StoreSignal.connect(widget.storeWindowState)

            if hasattr(widget, "restoreWindowState"):
                widget.restoreWindowState(QtCore.QSettings("RockstarSettings", window.windowTitle()))

            if hasattr(widget, "setWindowCallbacks"):
                widget.setWindowCallbacks(window)

            if show:
                window.show()

            return window
        return wrap
    return funcWrap


# # Functions # #


def MotionBuilderWindow():
    """
    Returns the QWidget instance of the Motion Builder Application that ran this method

    Returns:
        None or PySide.QtGui.QWidget()
    """
    return Application.GetMainWindow()


def CompileUi(uiFilename, pyFilename=None):
    """
    Compiles a Qt Designer .ui file into a .py file.

    Arguments:
        uiFilename (string): path to the QtDesigner file
        pyFilename (string): path to where the python file should be generated from the Qt Designer file

    """

    # If no python filename to compile to is supplied, then set it to the same filename and location as the .ui filename
    # , but obviously with a .py file extension.
    if pyFilename is None:
        pyFilename = '{0}\\{1}.py'.format(os.path.dirname(uiFilename), os.path.basename(uiFilename).split('.')[0])

    if not os.path.isfile(pyFilename):
        tempses = open(pyFilename, 'w')
        tempses.close()

    # See if the python file is writable first.
    pyMod = os.stat(pyFilename)[0]

    if pyMod & stat.S_IWRITE:
        uiFile = open(uiFilename)
        pyFile = open(pyFilename, 'w')

        pysideuic.compileUi(uiFile, pyFile)

        uiFile.close()
        pyFile.close()

    else:
        sys.stdout.write(
            '[PySide Compile Warning] "{0}" needs to be writable in order to be compiled!\n'.format(pyFilename))


def QPrint(*args):
    """  Prints passed arguments into the console when working with QT """
    print args
    if pythonidelib:
        pythonidelib.FlushOutput()


def makeDockable(widget, window, dockArea=QtCore.Qt.LeftDockWidgetArea, hide=True, float=True, areas=QtCore.Qt.AllDockWidgetAreas):
    """
    Makes the given widget dockable on the provided main window

    Arguments:
        widget (QtGui.QWidget): widget to dock
        window (QtGui.QMainWindow): window to dock widget to
        dockArea (QtCore.Qt.DockArea): area to dock widget in
        hide (boolean): hide widget after being made dockable
        float (boolean): keep the widget floating independently of the window where it was docked

    Return:
        QtGui.QDockWidget
    """
    dock = QtDockBase()
    dock.setWindowTitle(widget.windowTitle())
    dock.setObjectName(widget.windowTitle())
    dock.setWidget(widget)
    window.addDockWidget(dockArea, dock)
    dock.setFloating(float)
    dock.setAllowedAreas(areas)
    if hide:
        dock.hide()
    else:
        dock.show()
    return dock

# # Classes # #


class LayoutManager(object):
    __DEFAULT_LAYOUT = "Default Layout"

    def __init__(self, window, menubar=None, settings=None):
        """
        Constructor
        
        Arguments: 
            window (QtGui.QWidget): widget whose layout needs to be managed
            menubar (QtGui.QMenuBar): menu bar for to put layout menu in
            settings (QtCore.QSettings): settings to get stored layout information from 
        """
        self._window = window
        self._menubar = menubar
        self._settings = settings or QtCore.QSettings(window.windowTitle())
        self._lastLayout = self._settings.value("lastWindowLayout") or self.__DEFAULT_LAYOUT
        self._layouts = self._settings.value("windowLayouts") or {self.__DEFAULT_LAYOUT: (self._window.saveState(), self._window.saveGeometry())}
        self._resetlayout = (self._window.saveState(), self._window.saveGeometry())
        self._builtMenu = False

    def _setupActions(self):
        """
        Internal Method

        Setup all the actions, with the connections and icons
        """
        # File actions
        self.ac_defaultLayout = QtGui.QAction('Default Layout', self._window)
        self.ac_defaultLayout.triggered.connect(self._handleDefaultlayout)
        self.ac_saveLayout = QtGui.QAction('Save Layout...', self._window)
        self.ac_saveLayout.triggered.connect(self._handleSaveLayout)
        self.ac_deleteLayout = QtGui.QAction('Delete Layout...', self._window)
        self.ac_deleteLayout.triggered.connect(self._handleDeleteLayout)
        self.ac_resetLayout = QtGui.QAction('Reset Layout', self._window)
        self.ac_resetLayout.triggered.connect(self._handleResetLayout)

    def _addActionsToMenu(self, menuToAdd, actions):
        """
        Internal Method

        Helper method to add a list of actions to a menu
        if an action in the list of actions is None, then a separator is added
        """
        for action in actions:
            if action is None:
                menuToAdd.addSeparator()
                continue
            menuToAdd.addAction(action)

    def buildCustomLayoutsMenu(self):
        """ Builds the layout menu to store layouts in """
        if not self._window:
            return False

        menubar = self._menubar or self._window.layout().menuBar()
        if not menubar:
            menubar = QtGui.QMenuBar()
            self._window.setMenuBar(menubar)

        if not self._builtMenu:
            self._setupActions()

        layoutMenuName = 'Layout'
        menuDict = {str(item.title()): item for item in menubar.children() if isinstance(item, QtGui.QMenu)}

        layoutMenu = menuDict.get(layoutMenuName)
        if layoutMenu is None:
            layoutMenu = menubar.addMenu('Layout')
        else:
            layoutMenu.clear()

        self._addActionsToMenu(layoutMenu, [self.ac_defaultLayout])
        self._addActionsToMenu(layoutMenu, [None])
        self._addActionsToMenu(layoutMenu, [self.ac_saveLayout])
        self._addActionsToMenu(layoutMenu, [None])
        customLayouts = self._customLayouts()
        if len(customLayouts) == 0:
            noCustomLayout = QtGui.QAction('No Custom Layouts', self._window)
            noCustomLayout.setEnabled(False)
            self._addActionsToMenu(layoutMenu, [noCustomLayout])
        else:
            for item in customLayouts:
                customLayout = QtGui.QAction(item, self._window)
                customLayout.triggered.connect(functools.partial(self._handleCustomLayoutLoad, item))
                self._addActionsToMenu(layoutMenu, [customLayout])
        self._addActionsToMenu(layoutMenu, [None])
        self._addActionsToMenu(layoutMenu, [self.ac_deleteLayout])
        self._addActionsToMenu(layoutMenu, [None])
        self._addActionsToMenu(layoutMenu, [self.ac_resetLayout])
        self._builtMenu = True
        return True

    def _handleCustomLayoutLoad(self, layout):
        """
        Internal Method

        Handle the Loading of a custom layout the default layout button press
        """
        state, geo = self._layouts.get(layout, (None, None))
        self._window.restoreState(state)
        self._window.restoreGeometry(geo)
        self._lastLayout = layout

    def _handleDefaultlayout(self):
        """
        Internal Method

        Handle the Loading the default layout button press
        """
        self._handleCustomLayoutLoad(self.__DEFAULT_LAYOUT)

    def _handleSaveLayout(self):
        """
        Internal Method

        Handle the Save layout button press
        """
        try:
            index = self._layouts.keys().index(self._lastLayout)
        except ValueError:
            index = 0
        text, ok = QtGui.QInputDialog.getItem(self._window, "Layout to save", "Layout:", self._layouts.keys(), index, True)
        if ok is False:
            return
        self._layouts[text] = (self._window.saveState(), self._window.saveGeometry())
        self.buildCustomLayoutsMenu()
        self._lastLayout = text

    def _handleDeleteLayout(self):
        """
        Internal Method

        Handle the delete layout button press
        """
        items = self._customLayouts()
        if len(items) == 0:
            QtGui.QMessageBox.critical(self, "Unable to delete layout", "No Custom Layouts to delete")
            return

        try:
            index = items.index(self._lastLayout)
        except ValueError:
            index = 0

        text, ok = QtGui.QInputDialog.getItem(self._window, "Layout to Delete", "Layout:", items, index, False)
        if ok is False:
            return
        self._layouts.pop(text)
        self.buildCustomLayoutsMenu()

    def _handleResetLayout(self):
        """
        Internal Method

        Handle the Reset layout button press
        """
        state, geo = self._resetlayout
        self._window.restoreState(state)
        self._window.restoreGeometry(geo)

    def _customLayouts(self):
        """
        Internal Method

        Get a string list of all the custom layouts

        returns:
            list of string layout names
        """
        return [item for item in self._layouts.keys() if item != self.__DEFAULT_LAYOUT]

    def saveLastLayout(self):
        """
        Saves the current layout as the last layout
        """
        self._settings.setValue("lastWindowlayout", self._lastLayout)
        self._settings.setValue("windowLayouts", self._layouts)

    @property
    def currentLayout(self):
        """ The current layout in use by the manager """
        return self._lastLayout


class QtDialogBase(QtGui.QDialog):
    """ Base Widget class for a dialog widget """
    ShowSignal = QtCore.Signal(object)
    CloseSignal = QtCore.Signal(object)
    HideSignal = QtCore.Signal(object)

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent(QtGui.QWidget): parent widget

        """
        super(QtDialogBase, self).__init__(parent=parent)
        self.Signals = {
            QtCore.QEvent.Type.Hide: self.HideSignal,
            QtCore.QEvent.Type.Show: self.ShowSignal,
            QtCore.QEvent.Type.Close: self.CloseSignal}

    def event(self, event):
        """
        Emits signals for the Show, Hide and Close events

        Arguments:
            event (QtCore.QEvent): event being called by the UI

        Return:
            boolean
        """
        result = QtGui.QDialog.event(self, event)
        signal = self.Signals.get(event.type(), None)
        if signal:
            signal.emit(event)
        return result


class QtDockBase(QtGui.QDockWidget, Application.QtBase):
    """ Base Widget class for a dock widget """
    ShowSignal = QtCore.Signal(object)
    CloseSignal = QtCore.Signal(object)
    HideSignal = QtCore.Signal(object)

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent(QtGui.QWidget): parent widget

        """
        super(QtDockBase, self).__init__(parent=parent)
        self.Signals = {
            QtCore.QEvent.Type.Hide: self.HideSignal,
            QtCore.QEvent.Type.Show: self.ShowSignal,
            QtCore.QEvent.Type.Close: self.CloseSignal}

    def event(self, event):
        """
        Emits signals for the Show, Hide and Close events

        Arguments:
            event (QtCore.QEvent): event being called by the UI

        Return:
            boolean
        """
        if not isinstance(event, QtCore.QEvent):
            toolTitle = self.windowTitle()
            widgetModuleName = None
            widgetModuleFilePath = None
            
            try:
                # Try to get the name of the module that the dock's widget was defined in
                widgetModuleName = self.widget().__module__
            except AttributeError:
                pass
            if widgetModuleName:
                # Try to find the full file path to the module that defines the dock's widget
                widgetModule = sys.modules.get(self.widget().__module__)
                if widgetModule:
                    try:
                        widgetModuleFilePath = widgetModule.__file__
                    except AttributeError:
                        pass
            msg = "Invalid event type: {}".format(type(event).__name__)
            if toolTitle:
                msg += "\nTool Title: {}".format(toolTitle)
            if widgetModuleName:
                msg += "\nWidget's module name: {}".format(widgetModuleName)
            if widgetModuleFilePath:
                msg += "\nWidget's module file path: {}".format(widgetModuleFilePath)
            
            raise TypeError(msg)
        
        result = QtGui.QDockWidget.event(self, event)
        signal = self.Signals.get(event.type(), None)
        if signal:
            signal.emit(event)
        return result


class QtMainWindowBase(QtGui.QMainWindow, Application.QtBase):
    """ Base Window class for showing Qt UI in Motion Builder """

    StoreSignal = QtCore.Signal(object)

    def __init__(self,
                 parent=None,
                 title='Rockstar Tool',
                 size=[400, 400],
                 dockable=False,
                 dock=False,
                 dockArea=QtCore.Qt.TopDockWidgetArea,
                 dialog=False,
                 store=False,
                 windowStaysOnTop=False,
                 closeExistingWindow=True
                 ):
        """
        Constructor

        Arguments:
            parent (QWidget): parent widget
            title (string): title for the window
            size (list[int/float, int/float]): width and height for the window
            dockable (boolean): if the window is dockable
            dock (boolean): if the window should be docked when first displayed.
            dockArea (QCore.Qt.DockAre): where to dock the window.
                       Accepts QtCore.Qt.TopDockWidgetArea, QtCore.Qt.LeftDockWidgetArea, QtCore.Qt.RightDockWidgetArea,
                       QtCore.Qt.BottomDockWidgetArea
            store (boolean): should the previous window layout & size be restored
            windowStaysOnTop (boolean): if the window should be forced to the front, infront of all other windows.
            closeExistingWindows (boolean)
        """
        # Close previous version of the tool.
        if closeExistingWindow:
            Application.CloseToolByTitle(title)

        # If no parent supplied, set MotionBuilder application window as the parent.
        if parent is None:
            parent = Application.GetMainWindow()
        
        self._dock = None
        self._dialog = None
        
        super(QtMainWindowBase, self).__init__(parent)

        self.setWindowTitle(title)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        if windowStaysOnTop is True:
            self.setWindowFlags(QtCore.Qt.Tool | QtCore.Qt.WindowStaysOnTopHint)

        # Set the window icon.
        windowIcon = QtGui.QIcon()
        windowIcon.addPixmap(QtGui.QPixmap('{}\\Rockstar_Games_24x24.png'.format(Config.Script.Path.ToolImages)))
        QtGui.qApp.setWindowIcon(windowIcon)
        
        self._floatOnShow = not dock
        self._store = store
        self._showEvents = []
        self._closeEvents = []
        self._storeState = False
        self._settings = QtCore.QSettings("RockstarSettings", title)

        if dialog:
            self._dialog = QtDialogBase()
            layout = QtGui.QVBoxLayout()
            layout.setSpacing(0)
            layout.setContentsMargins(0, 0, 0, 0)

            layout.addWidget(self)
            self._dialog.setWindowTitle(title)
            self._dialog.setLayout(layout)

        elif (dockable or dock) and isinstance(parent, QtGui.QMainWindow):
            # Create DockWidget and parent this window (self) to it
            self._dock = QtDockBase()
            self._dock.setWindowTitle(title)
            self._dock.topLevelChanged.connect(functools.partial(self._ShowMinimizeMaximizeIcons, self._dock))
            self._dock.setWidget(self)

            # Add the dock widget to the layout of the parent and set it to floating
            layout = parent.layout()
            layout.addWidget(self._dock)
            parent.addDockWidget(dockArea, self._dock)

            # Since the dock is the top widget, when it is closed we call the close event for this widget
        self.setCloseEvent(self.closeEvent)

        self._parent = self._dock or self._dialog
        self.resize(*size)

        if self._store:
            self.restoreWindowState()

    def resize(self, width, height):
        """
        Overrides the built-in resize method

        If the window has a dockWidget, it resizes the dock widget as well

        Arguments:
            width (int): width for the window
            height (int): height for the window

        """
        super(QtMainWindowBase, self).resize(width, height)
        if self._parent:
            self._parent.resize(width, height)

    def restoreGeometry(self, geometry):
        """
        Overrides internal restoreGeometry method

        Calls restoreGeometry on the dock widget instead if a dock widget is available

        Arguments:
            geometry (string): size to restore widget to
        """
        super(QtMainWindowBase, self).restoreGeometry(geometry)
        if self._parent:
            self._parent.restoreGeometry(geometry)

    def isFloating(self):
        """
        is the dock widget floating, if the widget has no dock widget it will return the visibility state of the widget
        instead
        """
        if self._dock:
            return self._dock.isFloating()

        else:
            return self.isVisible()

    def setFloating(self, float):
        """
        sets the floating state for the window. If the window is not dockable then it will set the visibility

        Arguments:
            float (boolean): if the window should be floating
        """
        if self._dock:
            self._floatOnShow = float
            self._dock.setFloating(float)
        else:
            self.setVisible(float)

    def setWindowTitle(self, newTitle):
        if self._dialog is not None:
            self._dialog.setWindowTitle(newTitle)
        if self._dock is not None:
            self._dock.setWindowTitle(newTitle)
        super(QtMainWindowBase, self).setWindowTitle(newTitle)
    
    def allowedAreas(self):
        """ allowed docking areas for this widget """
        if self._dock:
            return self._dock.allowedAreas()
        return QtCore.Qt.NoDockWidgetArea

    def setAllowedAreas(self, areas=QtCore.Qt.AllDockWidgetAreas):
        """
        allowed docking areas for this widget
        Arguments:
            areas (QtCore.Qt.DockAreas): areas the widget is allowed to dock in
        """
        if self._dock:
            self._dock.setAllowedAreas(areas)

    def changeDockArea(self, dockArea):
        """
        change where the widget is docked
        Arguments:
            dockArea (QCore.Qt.DockArea): where to dock the window.
               Accepts QtCore.Qt.TopDockWidgetArea, QtCore.Qt.LeftDockWidgetArea, QtCore.Qt.RightDockWidgetArea,
               QtCore.Qt.BottomDockWidgetArea
        """
        window = self._dock.parent()
        if isinstance(window, QtGui.QMainWindow):
            window.addDockWidget(dockArea, self._dock)

    def dockWidget(self):
        """ the dock widget attached to this widget """
        return self._dock

    def show(self):
        """
        Overrides built-in method; If the parent of the window is a dialog widget, then run exec_ instead of show on
        it.
        """
        if self._dialog:
            return self._dialog.exec_()

        elif self._dock:
            if self._floatOnShow != self._dock.isFloating():
                self.setFloating(self._floatOnShow)
            return self._dock.show()

        super(QtMainWindowBase, self).show()

    def setShowEvent(self, func):
        """
        Sets the show event on the correct top level widget so it gets called.

        Arguments:
            func (function): method to connect as the showEvent method

        """
        if func == self.showEvent:
            return

        if func not in self._showEvents:
            self._showEvents.append(func)

        window = self._dialog or self._dock
        if window:
            try:
                # Removes existing connections on the close signal
                # This errors out if the signal doesn't have anything connected to it
                window.ShowSignal.disconnect()
            except RuntimeError:
                pass
            for showEvent in self._showEvents:
                window.ShowSignal.connect(showEvent)

    def closeEvent(self, event):
        """
        Overrides built-in method

        Calls store window state to save the state of the window

        Arguments:
            event (QtGui.QEvent): event being called
        """
        try:
            super(QtMainWindowBase, self).closeEvent(event)
            if not self._dialog and not self._dock:
                if self.closeEvent in self._closeEvents:
                    self._closeEvents.remove(self.closeEvent)
                for event_ in self._closeEvents:
                    event_(event)
            if self._store:
                self.storeWindowState()
        except RuntimeError:
            pass

    def setCloseEvent(self, func):
        """
        Sets close event for the window

        Arguments:
            func (function): function to add as the close event
        """
        if func not in self._closeEvents:
            self._closeEvents.append(func)

        window = self._dialog or self._dock
        if window:
            try:
                # Removes existing connections on the close signal
                # This errors out if the signal doesn't have anything connected to it
                window.CloseSignal.disconnect()
            except RuntimeError:
                pass
            for closeEvent in self._closeEvents:
                window.CloseSignal.connect(closeEvent)

    def storeWindowState(self):
        """
        Stores settings for the given widget

        Arguments:
            toolInstance (QtGui.QWidget): widget instance to store values from
            toolTitle (string): title of the widget

        """
        if not self._storeState:
            return

        # Get top parent that isn't the main window
        parent = self._parent or self

        self._settings.setValue("geometry", parent.saveGeometry())
        self._settings.setValue("position", parent.pos())
        self._settings.setValue("size", parent.size())
        if hasattr(parent, "saveState"):
            self._settings.setValue("windowState", parent.saveState())

        if self._dock and not self._dock.isFloating():
            _parent = parent.parent()
            while not isinstance(_parent, (QtGui.QMainWindow, types.NoneType)):
                _parent = _parent.parent()
            if _parent:
                self._settings.setValue("dockArea", int(parent.parent().dockWidgetArea(parent)))

        else:
            self._settings.setValue("dockArea", None)

        self.StoreSignal.emit(self._settings)
        self._storeState = False

    def restoreWindowState(self):
        """
        Restores the size and position from the stored settings of the given widget

        Arguments:
            toolInstance (QtGui.QWidget): widget instance to store values from
            toolTitle (string): title of the widget

        """
        desktop = QtGui.QApplication.instance().desktop()

        # Get top parent that isn't the main window
        parent = self._parent or self
        size = self._settings.value("size") or parent.size()
        position = self._settings.value("position") or parent.pos()

        parent.restoreGeometry(self._settings.value("geometry"))

        # Check if the stored position of the tool places it out of the view of the monitors in use
        visible = [desktop.availableGeometry(index).contains(position)
                   for index in xrange(desktop.screenCount())]

        if not any(visible):
            # Find the position that would place the tool in the middle of the primary monitor
            rect = desktop.availableGeometry(desktop.primaryScreen())
            x = rect.bottomLeft().x() + rect.width()/2.0 - size.width()/2.0
            y = rect.height()/2.0 - size.height()/2.0
            position = QtCore.QPoint(x, y)
        parent.move(position)

        self.resize(size.width(), size.height())
        parent.resize(size.width(), size.height())

        if hasattr(parent, "restoreState"):
            parent.restoreState(self._settings.value("windowState") or parent.saveState())

        if self._dock and self._settings.value("dockArea"):
            parent.parent().addDockWidget(DOCK_AREAS[self._settings.value("dockArea")], self._dock)
            self._dock.setFloating(False)
        self._storeState = True

    @staticmethod
    def _ShowMinimizeMaximizeIcons(dock, *args):
        """
        Sets the state of the dock widget to that of a window so it can be maximized and minimized
        Arguments:
            dock (QDockWidget): dock widget to add maximize and minimize buttons to
            *args (list): this is only hear to accept miscellaneous unneeded arguments from signals
        """
        if dock.isFloating():
            dock.setWindowFlags(QtCore.Qt.Window)
            # If show isn't called then the window doesn't appear when switching from dock to floating
            dock.show()

    @property
    def Layout(self):
        """ the main layout for the tool """
        return self.centralWidget().layout()


class QtBannerWindowBase(QtMainWindowBase):
    """
    Window class that includes the Banner by default.
    """

    Form = None

    def __init__(self, parent=None, title='Rockstar Tool', size=[400, 400], helpUrl=None, emailTo=EMAIL, emailCC='',
                 dockable=False, dock=False, dockArea=QtCore.Qt.TopDockWidgetArea, dialog=False, store=False,
                 closeExistingWindow=True):
        """
        constructor

        Arguments:
            parent (QWidget): parent widget
            title (string): title for the window
            size (list[int/float, int/float]): width and height for the window
            helpUrl (string): url to the wiki page for the tool
            emailTo (string): email address to send email to
            emailCC (string): email cc to send email to
            dockable (boolean): if the window is dockable
            dock (boolean): if the window should be docked when first displayed.
            dockArea (QCore.Qt.DockAre): where to dock the window.
                       Accepts QtCore.Qt.TopDockWidgetArea, QtCore.Qt.LeftDockWidgetArea, QtCore.Qt.RightDockWidgetArea,
                       QtCore.Qt.BottomDockWidgetArea



        """
        super(QtBannerWindowBase, self).__init__(parent=parent, title=title, size=size, dockable=dockable, dock=dock,
                                                 dockArea=dockArea, dialog=dialog, store=store,
                                                 closeExistingWindow=closeExistingWindow)
        centralWidget = QtGui.QWidget()
        layout = QtGui.QVBoxLayout()
        centralWidget.setLayout(layout)
        self.setCentralWidget(centralWidget)

        self._banner = BannerWidget(name=title, helpUrl=helpUrl, emailTo=emailTo, emailCC=emailCC)

        # Set the banner as the central widget and use it's VBoxLayout to
        # organize our UI beneath it. If we create another Layout to hold
        # the banner, the banner doesn't stretch out to fit the window
        # completely

        layout.addWidget(self._banner)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        # To support code using _mainLayout, moving forward Layout should be called
        self._mainLayout = layout

        layout.setSpacing(0)

        # Where we load the UIC files
        if self.Form:
            self.__UI()

        # Size is a custom attribute that gets generated in the _UI method if a Form is associated with the class
        # If the form is not used, then Size is not created, so we default to size parameter passed in
        if not store:
            self.resize(*getattr(self, "Size", size))

    def __UI(self):
        """
        Generates the UI from the module generated by the QT Designer .uic file and
        adds it to the window
        """
        class UI(QtGui.QMainWindow, self.Form):
            def __init__(self, *arguments):
                super(UI, self).__init__(*arguments)
                self.setupUi(self)

        self.widget = UI()
        self.Layout.addWidget(self.widget)
        self.Size = self.widget.size().toTuple()

        # The Height of the banner is 35
        self.Size = [self.Size[0], self.Size[1] + 35]

    @property
    def Url(self):
        """ help url for the tool """
        return self._banner.helpUrl

    @Url.setter
    def Url(self, url):
        """
        set the help url for the tool

        Arguments:
            url (string): help url for the tool
        """
        self._banner.helpUrl = url

    @property
    def EmailTo(self):
        """ the email to list for the tool """
        return self._banner.emailTo

    @EmailTo.setter
    def EmailTo(self, email):
        """
        Set the email to list for the tool

        Arguments:
            email (string): email address(es) to send email too. Email addresses should be separated by colons (;)
        """
        self._banner.emailTo = email

    @property
    def EmailCC(self):
        """ the email cc to list for the tool """
        return self._banner.emailCC

    @EmailCC.setter
    def EmailCC(self, emails):
        """
        set the email cc to list for the tool

        Arguments:
            emails (string): email address(es) to cc email too. Email addresses should be separated by colons (;)
        """

        self._banner.emailCC = emails

    @property
    def Banner(self):
        return self._banner

class QtBaseWidget(QtGui.QWidget):
    """
    Base class for a Qt widget object.  Whatever inherits from this class also needs
    to inherit its .ui Python module.  The setupUi() function will be automatically
    called by this class.

    Example:
        class MyWidget(RS.Tools.UI.QtBaseWidget, RS.Tools.UI.MyQtUiModule):
            def __init__(self, parent):
                RS.Tools.UI.QtBaseWidget.__init__(self, parent)
        """
    def __init__(self, parent):
        """
        Constructor

        Arguments:
            parent (QWidget): parent widget
        """
        QtGui.QWidget.__init__(self, parent)

        self.setupUi(self)


class BannerWidget(QtGui.QLabel, Application.QtBase):
    """ Banner for the Rockstar Tools"""

    def __init__(self,
                 name,
                 helpUrl=None,
                 emailTo=EMAIL,
                 emailCC=None,
                 matterMostUrl=None
                 ):
        """
        Constructor

        Arguments:
            name (string): name of the tool using the banner
            helpUrl (string): url to the wiki page for the tool
            emailTo: (string): email address to send email to
            emailCC (string): email cc to send email to
            matterMostUrl (string): url to the wiki page for the tool - NOT USED ANYMORE
        """
        super(BannerWidget, self).__init__()

        self.__toolName = name
        self.__helpUrl = self.getWikiUrl(helpUrl)
        self.__initilizing = True
        # Banner properties
        self.emailTo = emailTo
        self.emailCC = emailCC or EMAIL
        self.matterMostUrl = matterMostUrl

        layout = QtGui.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignTop)  # Align the whole Layout at the top.
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        # self.setLayout(header)

        # # Logo # #
        logo = QtGui.QLabel()
        logo.setPixmap(QtGui.QPixmap(os.path.join(Config.Script.Path.ToolImages, "header_RSLogo_30x30.png")))
        logo.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed) # Do not change size

        # Space widget to ensure banner is always on screen left
        spacer = QtGui.QWidget()
        spacer.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)

        self._toolbar = QtGui.QToolBar()
        self._toolbar.setIconSize(QtCore.QSize(35, 30))

        # # Email # #
        emailIcon = QtGui.QIcon()
        emailIcon.addPixmap(QtGui.QPixmap(os.path.join(Config.Script.Path.ToolImages, "header_EmailIcon_35x30.png")))
        self.addButton(emailIcon, "Email Tech Art", self.OnEmail_Clicked)

        # # Info Link # #
        infoIcon = QtGui.QIcon()
        infoIcon.addPixmap(QtGui.QPixmap(os.path.join(Config.Script.Path.ToolImages, "header_InfoIcon_35x30.png")))
        self.addButton(infoIcon, "Tool Information", self.OnInfo_Clicked)


        layout.addWidget(logo, 0)
        layout.addWidget(spacer, 1)
        layout.addWidget(self._toolbar, 0)

        self.setFixedHeight(35)
        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        self.setLayout(layout)
        self.setStyleSheet(
                           "QWidget "
                           "{"
                           "background-color: #ffdc19;"
                           "}"
                           "QPushButton:hover"
                           "{"
                           "border: solid;"
                           "border-width: 1px;"
                           "border-color: black;"
                           "border-radius: 3px;"
                           "}"
                           "QPushButton"
                           "{"
                           "border: none;"
                           "}")
        self.__initilizing = False

    def addButton(self, icon, text, function=None):
        """
        Adds a button to the banner.
        Buttons added outside of the initialization of the banner are added before the default buttons.

        Arguments:
            icon (QtGui.QIcon): icon to attach to button
            text (string): text to add to the button
            function (func): method to attach to the triggered signal of the button

        Return:
            QtGui.QPushButton
        """
        button = QtGui.QPushButton(self)
        button.resize(35, 30)
        button.setToolTip(text)
        button.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed) # Do not change size
        button.setIcon(icon)
        button.setIconSize(QtCore.QSize(35, 30))
        if function is not None:
            button.pressed.connect(function)
        if self.__initilizing:
            self._toolbar.addWidget(button)
        else:
            self._toolbar.insertWidget(self._toolbar.actions()[0], button)
        return button

    @property
    def helpUrl(self):
        """
        Looks up the name of the tool in the wikilookup.xml to locate the xml page that is associated with that tool.

        The wikilook up file is located at:
        $PROJECT/tools/release/wildwest/etc/config/general/WikiLookup.xml

        Return:
            string
        """
        return self.__helpUrl

    @helpUrl.setter
    def helpUrl(self, value):
        self.__helpUrl = value

    @memorize.memoized
    def getWikiUrl(self, url):
        if url:
            return url

        wikiPath = os.path.join(Config.Tool.Path.Root, "wildwest", "etc", "config", "general", "WikiLookup.xml")
        if not os.path.exists(wikiPath):
            return HELP_URL
        
        tree = xml.parse(wikiPath)
        element = tree.getroot()
        wikiElement = element.find("WikiPages/Page[@name='{}']".format(self.__toolName.replace(" ", "_")))

        if wikiElement is None:
            return HELP_URL

        page = wikiElement.attrib["mainPage"]
        element = element.find("WikiRoots/MainPage[@name='{}']".format(page))
        path = element.attrib["uri"]
        path = os.path.join(path, wikiElement.attrib["uri"])
        return path

    def OnEmail_Clicked(self):
        """ Open the local email application to send an email"""
        os.startfile("mailto: " + self.emailTo + "&Subject= " + self.__toolName)

    def OnInfo_Clicked(self):
        """ Opens the website determined by te helpUrl attribute"""
        webbrowser.open(self.helpUrl)


class SortFilterProxyModel(QtGui.QSortFilterProxyModel):
    def __init__(self, parent=None, filterString=''):
        super(SortFilterProxyModel, self).__init__(parent)
        self.setFilterRegExp(filterString)
        self.filterFunctions = {}

    def addFilterFunction(self, name, new_func):
        '''
        Adds a filter function to the proxy model for the filtering.

        :param name: Hashable identifier for the new function
        :param new_func: Function with two args, the current filterString, and the row to be tested against.
        :return: None
        '''

        self.filterFunctions[name] = new_func
        self.invalidateFilter()

    def filterAcceptsRow(self, row, parent):
        '''
        Filters the rows of matching tools from the criteria

        :param row: The row of the matching tool
        :param parent: The parent of the source
        :return: searchStr <str> The string that is contained in the filtered model string
        '''

        index = self.sourceModel().index(row, 0, parent)
        match = re.search('(?<=){0}'.format(self.filterRegExp().pattern()), str(self.sourceModel().data(index, QtCore.Qt.DisplayRole)), re.I)

        return bool(match)


class QtCompleter(QtGui.QCompleter):
    def __init__(self, parent=None):
        super(QtCompleter, self).__init__(parent)
        self.sourceModel = None

    def setModel(self, model):
        '''
        Sets the QStringModel

        :param model: QStringModel
        :return: None
        '''

        self.sourceModel = model
        super(QtCompleter, self).setModel(self.sourceModel)

    def setFilterRegExp(self, text):
        '''
        Updates the search model for filtering.

        :return: searchStr <str> The string that is contained in the filtered model string
        '''

        # Create the proxy model for filtering
        text = re.sub('[A-Z]+', lambda m: m.group(0).lower(), text)
        proxyModel = SortFilterProxyModel(filterString=text)
        proxyModel.setSourceModel(self.sourceModel)

        super(QtCompleter, self).setModel(proxyModel)

    def splitPath(self, text):
        '''
        Splits the text

        :param text: <str> Text entered
        :return: ''
        '''

        self.setFilterRegExp(str(text))
        return ''


# TODO: Phase out from code base
if mobu is not None:

    import pyfbsdk as mobu
    import pyfbsdk_additions as mobu_

    from RS.Utils import Widgets

    def Show(tool, destroy=True):
        """
        Shows a tool built with the pyfbsdk ui commands

        Arguments:
            tool (pydbsdk.FBTool): pyfbsdk tool ui to show
            destroy (boolean): destroy other tools that share the same name as the current tool
        """
        if destroy:
            mobu_.FBDestroyToolByName(tool.Name)

            tool = tool()
            mobu.FBAddTool(tool)
            mobu.ShowTool(tool)

        else:
            tool = mobu_.FBToolList[tool.Name]
            mobu.ShowTool(tool)


    class QtBaseWidgetHolder(mobu.FBWidgetHolder):
        """
        Native Qt widget holder.  This is what allows us to embed a QtWidget into our MotionBuilder interfaces.  This gets
        automatically used by the QtBase class.
        """
        def __init__(self, widget, *args, **kwargs):
            """
            Constructor

            Arguments:
                widget (QtGui.QWidget): widget to attach
                *args (list): positional arguments accepted by FBWidgetHolder
                **kwargs (dictionary): keyword arguments accepted by FBWidgetHolder
            """
            mobu.FBWidgetHolder.__init__(self, *args, **kwargs)

            self.__widget = widget

        def WidgetCreate(self, parent):
            self.__nativeQtWidget = self.__widget(shiboken.wrapInstance(parent, QtGui.QWidget))
            return shiboken.getCppPointer(self.__nativeQtWidget)[0]

    # TODO: Phase out from code base
    class QtBase(mobu.FBTool):
        """
        Base class for a PyQt-based tool.
        """
        Name = 'Rockstar - Default Tool'

        def __init__(self, name, widget, size = [400, 400]):
            """
            Constructor

            Arguments:
                name (string): The name of the tool.
                widget (RS.Tools.UI.QBaseWidget): The QtBaseWidget subclass.

            Keyword Arguments:

                size: The starting size of the tool.
            """
            mobu.FBTool.__init__(self, name)

            self.__qtWidgetHolder = QtBaseWidgetHolder(widget)

            x = mobu.FBAddRegionParam(0, mobu.FBAttachType.kmobu.FBAttachLeft, '')
            y = mobu.FBAddRegionParam(0, mobu.FBAttachType.kmobu.FBAttachTop, '')
            w = mobu.FBAddRegionParam(0, mobu.FBAttachType.kmobu.FBAttachRight, '')
            h = mobu.FBAddRegionParam(0, mobu.FBAttachType.kmobu.FBAttachBottom, '')

            self.AddRegion('main', 'main', x, y, w, h)
            self.SetControl('main', self.__qtWidgetHolder)

            self.StartSizeX = size[0]
            self.StartSizeY = size[1]


    class Base(object):
        """
        Base class for tools GUI.  The MotionBuilder SDK recommends to not use pyfbsdk.FBTool directly, but to instead use
        CreateUniqueTool and CreateTool to generate tools.
        """
        def __init__(self,
                     name,
                     size=[200, 200],
                     minSize=[10, 10],
                     pos=[0, 0],
                     createBanner=True,
                     emailTo=EMAIL,
                     emailCC=None,
                     helpUrl=None):

            # Tool properties.
            self.__size = size
            self.__minSize = minSize
            self.__toolName = name
            self.__tool = None

            # Center tool in the screen.
            # TODO: Looks like mobu.FBSystem().DesktopSize is broken in 2014.  WTF.
            screenWidth, screenHeight, huh = mobu.FBSystem().DesktopSize.GetList()

            if pos == [0, 0]:

                # Set to some default location until the desktop size bug can be looked into.
                self.__pos = [400, 400]

            else:
                self.__pos = pos

            # Banner properties.
            self.__banner = None
            self.__createBanner = createBanner
            self.__emailTo = emailTo
            self.__emailCC = emailCC
            self.__helpUrl = helpUrl

        # # Public Properties # #

        @property
        def OnUnbind(self):
            """
            Pass the OnUnbind event handler.
            """
            return self.__tool.OnUnbind

        @property
        def Name(self):
            return self.__toolName

        @property
        def MainLayout(self):
            return self.__tool

        @property
        def Size(self):
            return self.__size

        @property
        def BannerHeight(self):
            return 0 if not self.__banner else self.__banner.Height

        @property
        def Tool(self):
            return self.__tool

        # # Private Methods # #

        def _CreateUniqueTool(self):
            self.__tool = mobu_.FBCreateUniqueTool(self.__toolName)
            self.__tool.OnShow.Add(self.__OnShow)

            # Start size.
            self.__tool.StartSizeX = self.__size[0]
            self.__tool.StartSizeY = self.__size[1]

            # Minimum size.
            self.__tool.MinSizeX = self.__minSize[0]
            self.__tool.MinSizeY = self.__minSize[1]

            # Start position.
            self.__tool.StartPosX = self.__pos[0]
            self.__tool.StartPosY = self.__pos[1]

            # Create banner.
            if self.__createBanner:
                self.__banner = Widgets.Banner(self.__tool, self.__toolName, self.__emailTo,
                                                        inspect.getfile(inspect.currentframe().f_back),
                                                        self.__emailCC, self.__helpUrl)
                self.__banner.Create()

            self.Create(self.__tool)

        def __CreateTool(self):
            self._CreateUniqueTool()

            mobu.ShowTool(self.__tool)

        def __OnShow(self, source, event):
            self.__closedRefCount = 0

        # # Public Methods # #

        def FileNewCompleted(self, state):
            self.__tool.FileNewCompleted(state)

        def Create(self, mainLayout):
            raise NotImplementedError, 'You must implement this method!'

        def Show(self, destroy=True):
            if destroy:
                mobu_.FBDestroyToolByName(self.__toolName)
                self.__CreateTool()

            else:
                if self.__toolName in mobu.FBToolList:
                    mobu.ShowToolByName(self.__toolName)

                else:
                    self.__CreateTool()
