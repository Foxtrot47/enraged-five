from pyfbsdk import *
from pyfbsdk_additions import *

import inspect

import RS.Tools.UI
import RS.Core.Face.Toolbox
import RS.Core.Face.FixJawNull
import RS.Core.Face.PreRotation
import RS.Core.Face.Lib
import RS.Core.Face.FixFace
import RS.Utils.Creation


class CutsceneFacialToolbox( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Cutscene Facial Toolbox", size = [ 200, 736 ] )
        

    def Create( self, getCharsLyt ):
        
        # The object to search for in the scene to find cutscene characters
        idControl = "FACIAL_facialRoot"
        idControl_Ambient = "Ambient_UI"
        
        # GUI Element Shared Properties
        lButtonLook = FBButtonLook.kFBLookNormal
        
        
        # The main Horizontal FBox Layout
        lytHoriz = FBHBoxLayout()
        x = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(self.BannerHeight,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        h = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        getCharsLyt.AddRegion("main","main", x,y,w,h)
        getCharsLyt.SetControl("main",lytHoriz)
        
        # The main Vertical FBox Layout
        lyt = FBVBoxLayout()
        lytHoriz.Add(lyt,175)
        
        # LABEL - Cutscene Characters
        label1 = FBLabel()
        label1.Caption = "Cutscene Characters:"
        lyt.Add(label1,20)
        
        # List creation
        global ctrlsList
        ctrlsList = FBList()
        ctrlsList.Style = FBListStyle.kFBVerticalList
        ctrlsList.MultiSelect = False
        lyt.Add(ctrlsList, 100)
        
        # fill the list with data
        lChars = []
        
        lCharCtrls = FBComponentList()
        FBFindObjectsByName(idControl, lCharCtrls,False, False)
        for iEach in lCharCtrls :
            lName = iEach.LongName.split(":",1)
            lChars.append(lName[0])
            
        lCharCtrls_Amb = FBComponentList()
        FBFindObjectsByName(idControl_Ambient, lCharCtrls_Amb,False, False)
        for iEach in lCharCtrls_Amb :
            lName = iEach.LongName.split(":",1)
            lChars.append(lName[0] + " -AMBIENT")
            
        for each in lChars :
            ctrlsList.Items.append(each)
        ctrlsList.Selected(0, True)
        
        
        # Button -- Refresh Tool
        but_close = FBButton()
        but_close.Caption = "^ Refresh Character List ^"
        but_close.Hint = "Update the list above"
        but_close.Look = lButtonLook
        lyt.Add(but_close,20)
        
        # Spacer
        lbl_00 = FBLabel()
        lyt.Add(lbl_00,1)
        
        # Button -- Fix Movie Plane
        but_fixMoviePlane = FBButton()
        but_fixMoviePlane.Caption = "Fix Movie Plane"
        but_fixMoviePlane.Hint = "This will move the movieplane into position for animation."
        but_fixMoviePlane.Look = lButtonLook
        lyt.Add(but_fixMoviePlane,25)
        but_fixMoviePlane.Enabled = False
        
        # Button -- Fix Face
        but_fixFace = FBButton()
        but_fixFace.Caption = 'Fix Face'
        but_fixFace.Hint = 'Fixes joint-related issues on face.'
        but_fixFace.Look = lButtonLook
        lyt.Add(but_fixFace,25)
        
        # Button -- Fix Jaw Null
        but_fixJawNull = FBButton()
        but_fixJawNull.Caption = 'Fix FACIAL_jaw_Null'
        but_fixJawNull.Hint = 'Fixes the common FACIAL_jaw_Null issue'
        but_fixJawNull.Look = lButtonLook
        lyt.Add(but_fixJawNull,25)
        
        # Spacer
        lbl_01 = FBLabel()
        lyt.Add(lbl_01,1)
        
        # Button -- Create Headcam
        but_headCam = FBButton()
        but_headCam.Caption = "Create Headcam"
        but_headCam.Hint = "Create a Headcam & a Light constrained to the character's head"
        but_headCam.Look = lButtonLook
        lyt.Add(but_headCam,25)
        
        # Button -- Delete Headcams
        but_delHeadCam = FBButton()
        but_delHeadCam.Caption = "Delete ALL Headcams"
        but_delHeadCam.Hint = "Remove all headcams & lights from the scene"
        but_delHeadCam.Look = lButtonLook
        lyt.Add(but_delHeadCam,25)
        
        # Spacer
        lbl_04 = FBLabel()
        lyt.Add(lbl_04,1)
        
        # Button -- Create movie plane
        but_createMoviePlane = FBButton()
        but_createMoviePlane.Caption = "Create Movie Plane"
        but_createMoviePlane.Hint = "Create a movie plane for the selected character"
        but_createMoviePlane.Look = lButtonLook
        lyt.Add(but_createMoviePlane,25)
        
        # Button -- Delete movie plane
        but_deleteMoviePlane = FBButton()
        but_deleteMoviePlane.Caption = "Delete Movie Plane"
        but_deleteMoviePlane.Hint = "Delete a movie plane for the selected character"
        but_deleteMoviePlane.Look = lButtonLook
        lyt.Add(but_deleteMoviePlane,25)
        
        # Spacer
        lbl_02 = FBLabel()
        lyt.Add(lbl_02,1)
        
        # Button -- Show/Hide Face Ctrls
        but_showHideFace = FBButton()
        but_showHideFace.Caption = "Show/Hide On-Face Ctrls"
        but_showHideFace.Hint = "Toggle Show/Hide of the on-face GUI Control Circles"
        but_showHideFace.Look = lButtonLook
        lyt.Add(but_showHideFace,25)
        
        # Spacer
        lbl_05 = FBLabel()
        lyt.Add(lbl_05,1)
        
        # Button -- Select Facial Controls
        but_selectFacial = FBButton()
        but_selectFacial.Caption = "Select Facial Controls"
        but_selectFacial.Hint = "Select the facial controllers of the selected character"
        but_selectFacial.Look = lButtonLook
        lyt.Add(but_selectFacial,25)
        
        # Button -- Select Facial Root
        but_selectFRoot = FBButton()
        but_selectFRoot.Caption = "Select Facial Root"
        but_selectFRoot.Hint = "Select the facial Root of the selected character"
        but_selectFRoot.Look = lButtonLook
        lyt.Add(but_selectFRoot,25)
        
        # Button -- Select Facial Root
        but_selectFGUI = FBButton()
        but_selectFGUI.Caption = "Select Facial GUI (Tagging)"
        but_selectFGUI.Hint = "Selects the facial GUI needed for facial tagging."
        but_selectFGUI.Look = lButtonLook
        lyt.Add(but_selectFGUI,25)
        
        # Spacer
        lbl_06 = FBLabel()
        lyt.Add(lbl_06,1)
        
        # Button -- Zero Selected
        but_zeroSelected = FBButton()
        but_zeroSelected.Caption = "Zero Selected Objects"
        but_zeroSelected.Hint = "Sets selected object's translation and rotation values to 0"
        but_zeroSelected.Look = lButtonLook
        lyt.Add(but_zeroSelected,25)
        
        # Button -- Delete Facial Stuff
        but_deleteFaceGUIGEO = FBButton()
        but_deleteFaceGUIGEO.Caption = 'Delete Face GUI and GEO'
        but_deleteFaceGUIGEO.Hint = 'Deletes facial GUI and GEO'
        but_deleteFaceGUIGEO.Look = lButtonLook
        lyt.Add(but_deleteFaceGUIGEO,25)
        #but_deleteFaceGUIGEO.Enabled = False
        
        # Spacer
        lbl_06 = FBLabel()
        lyt.Add(lbl_06,1)
        
        # Button -- Disable all Facial Rigging Constraints
        but_disableFaceRigs = FBButton()
        but_disableFaceRigs.Caption = 'Disable ALL Facial Rig Constraints'
        but_disableFaceRigs.Hint = 'Disables all the Ambient Face Rigging Constraints to improve performance'
        but_disableFaceRigs.Look = lButtonLook
        lyt.Add(but_disableFaceRigs,25)
        
        # Button -- Enable all Facial Rigging Constraints
        but_enableFaceRigs = FBButton()
        but_enableFaceRigs.Caption = 'Enable ALL Facial Rig Constraints'
        but_enableFaceRigs.Hint = 'Enables all the Ambient Face Rigging Constraints to improve performance'
        but_enableFaceRigs.Look = lButtonLook
        lyt.Add(but_enableFaceRigs,25)
        
        # Button -- Disable Selected Facial Rigging Constraints
        but_disableselectedFaceRigs = FBButton()
        but_disableselectedFaceRigs.Caption = 'Disable Selected Facial Rig Constraints'
        but_disableselectedFaceRigs.Hint = 'Disables the Ambient Face Rigging Constraints to improve performance in the selected character'
        but_disableselectedFaceRigs.Look = lButtonLook
        lyt.Add(but_disableselectedFaceRigs,25)
        
        # Button -- Enable Selected Facial Rigging Constraints
        but_enableselectedFaceRigs = FBButton()
        but_enableselectedFaceRigs.Caption = 'Enable Selected Facial Rig Constraints'
        but_enableselectedFaceRigs.Hint = 'Enables the Ambient Face Rigging Constraints to improve performance in the selected character'
        but_enableselectedFaceRigs.Look = lButtonLook
        lyt.Add(but_enableselectedFaceRigs,25)
        
        
        
        # Label -- Ambient
        lbl_ambient = FBLabel()
        lbl_ambient.Caption = "*GENERAL TOOLS*"
        #lyt.Add(lbl_ambient,30)
        
        # Button -- Swap "CTRL" and "CTL"
        but_swapCTLCTRL = FBButton()
        but_swapCTLCTRL.Caption = '*NEW 3L* Swap CTL CTRL'
        but_swapCTLCTRL.Hint = 'Swaps the naming conventions of ALL objects in scene for facial testing'
        but_swapCTLCTRL.Look = lButtonLook
        #lyt.Add(but_swapCTLCTRL,25)
        
        # Button -- Setup 3L Delivery
        but_setup3L = FBButton()
        but_setup3L.Caption = '*NEW 3L* Setup Delivery'
        but_setup3L.Hint = 'Sets up cameras and groups for new 3L Facial Rig: Player and/or Wolf'
        but_setup3L.Look = lButtonLook
        #lyt.Add(but_setup3L,25)
        
        # Button -- Setup 3L Delivery
        but_setup3LWolf = FBButton()
        but_setup3LWolf.Caption = '*NEW 3L* Setup Wolf'
        but_setup3LWolf.Hint = 'Sets up cameras and groups for new 3L Facial Rig: Wolf'
        but_setup3LWolf.Look = lButtonLook
        #lyt.Add(but_setup3LWolf,25)
        
        
        
        
        
        
        # Button -- Delete 3L Alpha Shaders
        but_del3LAlphas = FBButton()
        but_del3LAlphas.Caption = 'Delete 3L Alpha Shaders'
        but_del3LAlphas.Hint = 'Removes the 3L Alpha shaders from the cutscene to prevent crashing'
        but_del3LAlphas.Look = lButtonLook
        #lyt.Add(but_del3LAlphas,25)
        but_del3LAlphas.Enabled = False
        
        # Button -- Make Parent Constraints
        but_parentConstraint = FBButton()
        but_parentConstraint.Caption = "Create Parent Constraint"
        but_parentConstraint.Hint = "Create a parent contraint to fix the on-face GUI issue for selected character"
        but_parentConstraint.Look = lButtonLook
        #lyt.Add(but_parentConstraint,25)
        but_parentConstraint.Enabled = False
        
        # Button -- Create 3L Groups
        but_3LGroups = FBButton()
        but_3LGroups.Caption = "Create 3L Groups"
        but_3LGroups.Hint = "Create subgroups within the 3Lateral Group for selected character"
        but_3LGroups.Look = lButtonLook
        #lyt.Add(but_3LGroups,25)
        but_3LGroups.Enabled = False
        
        # Button -- Create Ambient Groups
        but_ambGroups = FBButton()
        but_ambGroups.Caption = "Create Ambient Groups"
        but_ambGroups.Hint = "Creates selection groups for Ambient facial controls"
        but_ambGroups.Look = lButtonLook
        #lyt.Add(but_ambGroups,25)
        but_ambGroups.Enabled = False
        
        # Button -- Export Ambient Animation
        but_exportAmbient = FBButton()
        but_exportAmbient.Caption = "Export Ambient Facial"
        but_exportAmbient.Hint = "This will export all of the ambient facial UI"
        but_exportAmbient.Look = lButtonLook
        #lyt.Add(but_exportAmbient,25)
        but_exportAmbient.Enabled = False
        
        
        
        
        '''
        
        # Button -- Fix jaw Null Selected
        but_jawNull = FBButton()
        but_jawNull.Caption = "Fix jaw Null Selected*"
        but_jawNull.Hint = ""
        but_jawNull.Look = lButtonLook
        lyt.Add(but_jawNull,25)
        
        # Button -- Fix Open Jaw Selected
        but_jawOpen = FBButton()
        but_jawOpen.Caption = "Fix Open Jaw Selected*"
        but_jawOpen.Hint = ""
        but_jawOpen.Look = lButtonLook
        lyt.Add(but_jawOpen,25)
        
        # Button -- Fix Scale Offset Selected
        but_scaleOffset = FBButton()
        but_scaleOffset.Caption = "Fix Scale Offset Selected*"
        but_scaleOffset.Hint = ""
        but_scaleOffset.Look = lButtonLook
        lyt.Add(but_scaleOffset,25)
    
        
        '''
            
        #-------------#
        # Definitions #
        #-------------#
        
        # On Click - Disable all Facial Rigging Constraints
        def but_disableFaceRigs_Clicked (Control, event):
            for i in RS.Globals.Constraints:
                if i.Name == 'Ambient_anim 3Lateral':
                    i.Active = False
                if i.Name == 'Viseme Relation':
                    i.Active = False       
        
        but_disableFaceRigs.OnClick.Add( but_disableFaceRigs_Clicked )
        
        # On Click - Enable all Facial Rigging Constraints
        def but_enableFaceRigs_Clicked (Control, event):
            for i in RS.Globals.Constraints:
                if i.Name == 'Ambient_anim 3Lateral':
                    i.Active = True
                if i.Name == 'Viseme Relation':
                    i.Active = True       
        
        but_enableFaceRigs.OnClick.Add( but_enableFaceRigs_Clicked )        
        
        # On Click - Disable selected Facial Rigging Constraints
        def but_disableselectedFaceRigs_Clicked( Control, event ):            
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace :
                print tNamespace
                for i in RS.Globals.Constraints:
                    if i.LongName == tNamespace + ':' + 'Ambient_anim 3Lateral':
                        i.Active = False
                    if i.LongName == tNamespace + ':' + 'Viseme Relation':
                        i.Active = False
                        
        but_disableselectedFaceRigs.OnClick.Add( but_disableselectedFaceRigs_Clicked )
        
        # On Click - Enable selected Facial Rigging Constraints
        def but_enableselectedFaceRigs_Clicked( Control, event ):            
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace :
                print tNamespace
                for i in RS.Globals.Constraints:
                    if i.LongName == tNamespace + ':' + 'Ambient_anim 3Lateral':
                        i.Active = True
                    if i.LongName == tNamespace + ':' + 'Viseme Relation':
                        i.Active = True
                        
        but_enableselectedFaceRigs.OnClick.Add( but_enableselectedFaceRigs_Clicked )
       
        
        # On CLick - Refresh Toolbox
        def but_close_Clicked (control, event) :
            CloseTool(getCharsLyt)
            tool = CutsceneFacialToolbox()
            tool.Show()
            
        but_close.OnClick.Add(but_close_Clicked)
        
            
        # On Click - Fix Lumpy Face
        def but_deleteFaceGUIGEO_Clicked( control, event ):
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace :
                RS.Core.Face.Toolbox.destroyFacialStuff( tNamespace )
            
        but_deleteFaceGUIGEO.OnClick.Add( but_deleteFaceGUIGEO_Clicked )
        
            
        # On Click - Fix Face
        def but_fixFace_Clicked( control, event ):
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace :
                RS.Core.Face.FixFace.applyMinMax( tNamespace )
            
        but_fixFace.OnClick.Add( but_fixFace_Clicked )
        
            
        # On Click - Fix Face
        def but_fixJawNull_Clicked( control, event ):
            
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace :
                RS.Core.Face.FixJawNull.rs_fixJawNull( tNamespace )
                        
        but_fixJawNull.OnClick.Add( but_fixJawNull_Clicked )
        
            
        # On Click - Del 3L Alpha Shaders
        def but_del3LAlphas_Clicked( control, event ):
            
            RS.Core.Face.Toolbox.deleteAlphaShaders()
                        
        but_del3LAlphas.OnClick.Add( but_del3LAlphas_Clicked )
        
        
        
        
        # On Click - Create movie plane
        def but_createMoviePlane_Clicked( control, event ):
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace :
                RS.Core.Face.Lib.createFaceMoviePlane( tNamespace )
            
        but_createMoviePlane.OnClick.Add( but_createMoviePlane_Clicked )
        
        
        # On Click - Delete movie plane
        def but_deleteMoviePlane_Clicked( control, event ):
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace :
                RS.Core.Face.Lib.removeFaceMoviePlane( tNamespace )
            
        but_deleteMoviePlane.OnClick.Add( but_deleteMoviePlane_Clicked )
        
        
        # On Click - Create 3L Groups
        def but_3LGroups_Clicked( control, event ):
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            RS.Core.Face.Toolbox.make3LGroups(tNamespace)
            
        but_3LGroups.OnClick.Add( but_3LGroups_Clicked )
        
        
        # On Click - Create Headcam & Lights
        def but_headCam_Clicked (control, event) :
            
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            
            if tNamespace == idControl :
                print "Need to update script for files without namespaces."
                
            else :
                tParent = RS.Core.Face.Toolbox.findHeadByNamespace(tNamespace)
                if tParent :
                    RS.Core.Face.Toolbox.createConstrainedHeadCam(tNamespace,tParent)
                else :
                    print "no proper head rig."
            
        but_headCam.OnClick.Add(but_headCam_Clicked)
        
        
        # On Click - Delete Headcam & Lights
        def but_delHeadCam_Clicked (control, event) :
            RS.Core.Face.Toolbox.deleteAllConstrainedStuff()
            
        but_delHeadCam.OnClick.Add(but_delHeadCam_Clicked)
        
        
        # On Click - Create facial parent constraint
        def but_parentConstraint_Clicked (control, event) :
        
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            
            if tNamespace == idControl :
                
                theParent = "FACIAL_facialRoot"
                theChild = "facialRoot_C_OFF"
                theName = "3LateralfacialRoot_Parent"
                
                theList = FBComponentList()
                FBFindObjectsByName(theParent, theList, True, True)
                FBFindObjectsByName(theChild, theList, True, True)
                
                if len(theList) == 2 :
                    lSrc = theList[0]
                    lDst = theList[1]
                    RS.Core.Face.Toolbox.createParentConstraint( lSrc, lDst, theName, pMoveToSource=False )
                else :
                    None
                    
            else :
                    
                theNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
                    
                theParent = theNamespace + ":FACIAL_facialRoot"
                theChild = theNamespace + ":facialRoot_C_OFF"
                theName = theNamespace + ":3LateralfacialRoot_Parent"
                
                theList = FBComponentList()
                FBFindObjectsByName(theParent, theList, True, True)
                FBFindObjectsByName(theChild, theList, True, True)
                
                if len(theList) == 2 :
                    lSrc = theList[0]
                    lDst = theList[1]
                    RS.Core.Face.Toolbox.createParentConstraint( lSrc, lDst, theName, pMoveToSource=False )
                else :
                    None
            
        but_parentConstraint.OnClick.Add(but_parentConstraint_Clicked)
        
        
        # On Click - Show/Hide Face Ctrls
        def but_showHideFace_Clicked( control, event ):
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace == idControl :
                RS.Core.Face.Toolbox.showHideFaceCtrls("")
            elif tNamespace :
                RS.Core.Face.Toolbox.showHideFaceCtrls( tNamespace + ":")
            
        but_showHideFace.OnClick.Add( but_showHideFace_Clicked )
        
        
        # On Click - Select All Facial Controls
        def but_selectFacial_Clicked (control, event) :
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            RS.Core.Face.Toolbox.selectFacialCtrls(tNamespace)
            
        but_selectFacial.OnClick.Add(but_selectFacial_Clicked)
        
        
        # On Click - Zero Selected
        def but_zeroSelected_Clicked( control, event ):
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace :
                RS.Core.Face.Toolbox.zeroSelectedControls()
            
        but_zeroSelected.OnClick.Add( but_zeroSelected_Clicked )
        
        
        # On Click - Select Facial Root node
        def but_selectFRoot_Clicked (control, event) :
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace == idControl :
                print tNamespace + " Selected."
                tList = FBComponentList()
                FBFindObjectsByName( tNamespace, tList, True, True )
                if len(tList) == 1 :            
                    RS.Core.Face.Toolbox.deselectAll()
                    tList[0].Selected = True
            else :
                print tNamespace + " Selected."
                tList = FBComponentList()
                FBFindObjectsByName( tNamespace + ":" + idControl, tList, True, True )
                if len(tList) == 1 :            
                    RS.Core.Face.Toolbox.deselectAll()
                    tList[0].Selected = True
                
        but_selectFRoot.OnClick.Add(but_selectFRoot_Clicked)
        
        
        # On Click - Select Facial GUI
        def but_selectFGUI_Clicked (control, event) :
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            tObject = "faceControls_OFF"
            if tNamespace == idControl :
                print tObject + " Selected."
                tList = FBComponentList()
                FBFindObjectsByName( tObject, tList, True, True )
                if len(tList) == 1 :            
                    RS.Core.Face.Toolbox.deselectAll()
                    tList[0].Selected = True
            else :
                print tNamespace + " Selected."
                tList = FBComponentList()
                FBFindObjectsByName( tNamespace + ":" + tObject, tList, True, True )
                if len(tList) == 1 :            
                    RS.Core.Face.Toolbox.deselectAll()
                    tList[0].Selected = True
                
        but_selectFGUI.OnClick.Add(but_selectFGUI_Clicked)
        
        
        # On Click - Swap CTL CTRL *** GENERAL TOOLS ***
        def but_swapCTLCTRL_Clicked( control, event ) :
            RS.Core.Face.Lib.switch_CTL_CTRL()
        
        but_swapCTLCTRL.OnClick.Add(but_swapCTLCTRL_Clicked)
        
        # On Click - but_setup3L_Clicked *** GENERAL TOOLS ***
        def but_setup3L_Clicked( control, event ) :
            RS.Core.Face.Lib.setupCams()
            RS.Core.Face.Lib.setupGroups_Master()
        
        but_setup3L.OnClick.Add( but_setup3L_Clicked )
        
        # On Click - but_setup3LWolf_Clicked *** GENERAL TOOLS ***
        def but_setup3LWolf_Clicked( control, event ) :
            RS.Core.Face.Lib.setupCams()
            RS.Core.Face.Lib.setupGroups_Wolf()
        
        but_setup3LWolf.OnClick.Add( but_setup3LWolf_Clicked )
        
        but_setup3LWolf
        
        
        
        # On Click - Create Ambient Groups ***AMBIENT ONLY***
        def but_ambGroups_Clicked (control, event) :
            tNamespace = (ctrlsList.Items[ctrlsList.ItemIndex]).split(" -AMBIENT",1)[0]
            if tNamespace == idControl_Ambient :
                tNamespace = ""
            RS.Core.Face.Toolbox.makeAmbientGroups(tNamespace)
            
        but_ambGroups.OnClick.Add(but_ambGroups_Clicked)
            
        # On Click - Create Ambient Groups ***AMBIENT ONLY***
        def but_exportAmbient_Clicked (control, event) :
            tNamespace = (ctrlsList.Items[ctrlsList.ItemIndex]).split(" -AMBIENT",1)[0]
            if tNamespace == idControl_Ambient :
                tNamespace = ""
            RS.Core.Face.Toolbox.exportAmbient(tNamespace)
            
        but_exportAmbient.OnClick.Add(but_exportAmbient_Clicked)
        
        

def Run( show = True ):
    global tool
    tool = CutsceneFacialToolbox()
    
    if show:
        tool.Show()
        
    return tool
