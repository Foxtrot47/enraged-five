import inspect

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.UI
import RS.Utils.Creation as cre

class TrackInfo:
    TrackStart = None,
    TrackEnd = None

class TrackToolbox( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Shot Track Tools", size = [ 176, 100 ] )
        
        self.lSystem = FBSystem()
        self.Story = FBStory()
        self.lPlayer = FBPlayerControl()
        
        self.Tracklist = []
        self.collectTracks()
        

    def Create( self, mainLyt ):
        lSetPreviousButton = FBButton()
        lSetPreviousButton.Caption = "< shotStart"
        lSetPreviousButton.Look = FBButtonLook.kFBLookFlat
        mainLyt.AddRegion("lSetPreviousButton","lSetPreviousButton",
                    FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                    FBAddRegionParam(self.BannerHeight,FBAttachType.kFBAttachTop,""),
                    FBAddRegionParam(80,FBAttachType.kFBAttachNone,""), 
                    FBAddRegionParam(0,FBAttachType.kFBAttachBottom,""))
        mainLyt.SetControl("lSetPreviousButton",lSetPreviousButton) 
        lSetPreviousButton.OnClick.Add(self.snaptoClipStart)      
        
        lSetNextButton = FBButton()
        lSetNextButton.Caption = "shotEnd >"
        lSetNextButton.Look = FBButtonLook.kFBLookFlat
        mainLyt.AddRegion("lSetNextButton","lSetNextButton",
                    FBAddRegionParam(0,FBAttachType.kFBAttachRight,"lSetPreviousButton"), 
                    FBAddRegionParam(38,FBAttachType.kFBAttachTop,""),
                    FBAddRegionParam(80,FBAttachType.kFBAttachNone,""), 
                    FBAddRegionParam(0,FBAttachType.kFBAttachBottom,""))
        mainLyt.SetControl("lSetNextButton",lSetNextButton) 
        lSetNextButton.OnClick.Add(self.snaptoClipEnd)
        
    def snaptoClipEnd(self, pControl, pEvent):
        self.snaptoClip(False)
        
    def snaptoClipStart(self, pControl, pEvent):
        self.snaptoClip(True)
        
    def collectTracks(self):
        lTrackContainer = self.Story.RootEditFolder.Tracks
        gTrack = None
        for iTrack in lTrackContainer:
            if iTrack.Name.endswith("EST") and iTrack.Name.startswith("Shot"):
                gTrack = iTrack 
                if gTrack != None:    
                    for lClip in gTrack.Clips:  
                        trackArray = []
                        trackArray.append(lClip.Start.GetFrame())
                        trackArray.append(lClip.Stop.GetFrame())
                        """
                        NewTrackInfo = TrackInfo()
                        NewTrackInfo.TrackStart = lClip.Start.GetFrame( True )
                        NewTrackInfo.TrackEnd = lClip.Stop.GetFrame( True )
                        """
                        self.Tracklist.append(trackArray)
        
        self.Tracklist.sort()
       
        
    def snaptoClip(self, GotoStart):
        # Get current frame
        
        lCurrentTime = self.lSystem.LocalTime.GetFrame()

        # Collect where all the Autoclips are at
        
        setStartTIme = None
        skip = False
        
        if lCurrentTime < self.Tracklist[0][0]:
            setStartTIme = self.Tracklist[0][0]
        elif lCurrentTime > self.Tracklist[len(self.Tracklist)-1][1]:
            setStartTIme = self.Tracklist[len(self.Tracklist)-1][1]
        else:
            for iTrack in self.Tracklist:
                if lCurrentTime >= iTrack[0] and lCurrentTime <= iTrack[1] and not skip:    
                    
                    if GotoStart:
                        setStartTIme = iTrack[0]
                        skip = True
                    else:
                        setStartTIme = iTrack[1]

        self.lPlayer.Goto(FBTime(0,0,0,setStartTIme))

def Run( show = True ):
    tool = TrackToolbox()
    
    if show:
        tool.Show()
        
    return tool
