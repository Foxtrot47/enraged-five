import re

from PySide import QtGui, QtCore

from RS.Core.ShortcutKeys import core


class KeyAssignWidget(QtGui.QWidget):
    
    VALID_KEYS = ("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R","S", "T", "U", "V", "W", "X", "Y", "Z", 
                  "0", "1", "2", "3", "4", "5", "6", "7", "8","9", 
                  "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10","F11","F12",
                  "ENTR", "DOWN", "LEFT", "RIGHT", "UP", "DEL", "ESC", "TAB", "INS", "HOME", 'END', "NADD", 'NSUB'
                  )
    
    def __init__(self, parent=None):
        self._shortcutManager = core.ShortcutKeysManager()
        self._currentShortcut = None
        super(KeyAssignWidget, self).__init__(parent=parent)
        self.setupUi()
        self.setMaximumWidth(150)
    
    def setupUi(self):
        # Layout
        mainLayout = QtGui.QGridLayout()
        
        # Widgets
        self._keysBox = QtGui.QComboBox()
        self._modiferCtrl = QtGui.QCheckBox("Control")
        self._modiferAlt = QtGui.QCheckBox("Alt")
        self._modiferCommand = QtGui.QCheckBox("Command")
        self._modiferShift = QtGui.QCheckBox("Shift")
        self._statusBar = QtGui.QLineEdit()
        keysLabel = QtGui.QLabel("Keys:")
        modifierLabel = QtGui.QLabel("Modifiers:")
        
        # Configure Wdigets
        self._statusBar.setReadOnly(True)
        for wid in [keysLabel, modifierLabel]:
            wid.setMaximumWidth(50)
        self._keysBox.addItems(self.VALID_KEYS)
        
        # Assign Widgets to layotus
        mainLayout.addWidget(keysLabel, 0, 0)
        mainLayout.addWidget(self._keysBox, 0, 1, 1, 2)
        mainLayout.addWidget(modifierLabel, 1, 0)
        mainLayout.addWidget(self._modiferCtrl, 1, 1)
        mainLayout.addWidget(self._modiferAlt, 1, 2)
        mainLayout.addWidget(self._modiferCommand, 2, 1)
        mainLayout.addWidget(self._modiferShift, 2, 2)
        mainLayout.addWidget(self._statusBar, 3, 0, 1, 3)

        self.setLayout(mainLayout)
        
        # Signals
        self._keysBox.currentIndexChanged.connect(self._handleUpdateKey)
        self._modiferCtrl.stateChanged.connect(self._handleUpdateKey)
        self._modiferAlt.stateChanged.connect(self._handleUpdateKey)
        self._modiferCommand.stateChanged.connect(self._handleUpdateKey)
        
        self.checkForConflicts("")
        
    def _handleUpdateKey(self):
        self.checkForConflicts(self.generateKeySequence())
    
    def checkForConflicts(self, inputSequence):
        result = self._shortcutManager.checkForKeyConflicts(self._currentShortcut, inputSequence)
        if result is None:
            self._statusBar.setText("No Conflicts")
        else:
            self._statusBar.setText("Conflicts with {0}::{1}".format(result.category(), result.name()))
    
    def clear(self):
        self._keysBox.setCurrentIndex(-1)
        self._modiferCtrl.setChecked(False)
        self._modiferAlt.setChecked(False)
        self._modiferCommand.setChecked(False)
    
    def generateKeySequence(self):
        keySequence = self._keysBox.currentText().upper()
        if self._modiferCtrl.isChecked():
            keySequence = "Ctrl+{0}".format(keySequence)
        
        if self._modiferAlt.isChecked():
            keySequence = "Alt+{0}".format(keySequence)
        
        if self._modiferCommand.isChecked():
            keySequence = "Meta+{0}".format(keySequence)
            
        if self._modiferShift.isChecked():
            keySequence = "Shift+{0}".format(keySequence)
            
        return keySequence

    def setCurrentShortcut(self, newShortcut):
        self._currentShortcut = newShortcut
        self.populateFromKeySequence(self._currentShortcut.keys())

    def populateFromKeySequence(self, inputSequence):
        results = re.match("(?P<ctrl>Ctrl\+)*(?P<meta>Meta\+)*(?P<command>Command\+)*(?P<alt>Alt\+)*(?P<shift>Shift\+)*(?P<keys>[a-zA-Z0-9]+)", inputSequence, re.I)
        if results is None:
            return
        resDict = results.groupdict()
        self._keysBox.setCurrentIndex(self._keysBox.findText(resDict['keys']) or -1)
        self._modiferCtrl.setChecked(resDict['ctrl'] != None)
        self._modiferAlt.setChecked(resDict['alt'] != None)
        self._modiferCommand.setChecked(resDict['meta'] != None or resDict['command'] != None)
        self._modiferShift.setChecked(resDict['shift'] != None)
        

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    wid = KeyAssignWidget()
    wid.show()
    wid.populateFromKeySequence("Ctrl+M")
    sys.exit(app.exec_())
