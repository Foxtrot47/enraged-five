"""
Converted from the Demo C++ Qt Application
http://doc.qt.io/qt-4.8/qt-widgets-codeeditor-example.html
"""

from PySide import QtGui, QtCore


class LineNumberArea(QtGui.QWidget):
    """
    Line number area widget
    """
    def __init__(self, editor):
        super(LineNumberArea, self).__init__(parent=editor)
        self._codeEditor = editor
    
    def sizeHint(self):
        """
        Reimplemented
        
        This property holds the recommended size for the widget.
        
        returns:
            QSize of the recommend size for the widget
        """
        return QtCore.QSize(self._codeEditor.lineNumberAreaWidth(), 0)

    def paintEvent(self, event):
        """
        Reimplemented
        
        The main paint event, which will draw the number area
        
        args:
            event (QEvent): The paint event
        """
        self._codeEditor.lineNumberAreaPaintEvent(event)


class CodeEditor(QtGui.QPlainTextEdit):
    """
    Main code editor widget, with current line highlighting and line numbers
    """
    def __init__(self, parent=None):
        super(CodeEditor, self).__init__(parent=parent)
        self._lineNumberArea = LineNumberArea(self)
        
        self.blockCountChanged.connect(self.updateLineNumberAreaWidth)
        self.updateRequest.connect(self.updateLineNumberArea)
        self.cursorPositionChanged.connect(self.highlightCurrentLine)
        
        self.updateLineNumberAreaWidth(0)
        self.highlightCurrentLine()
    
    def updateLineNumberAreaWidth(self, newBlockCount):
        """
        Update the line number area width based off the new block count
        
        args:
            newBlockCount (int): The new block count
        """
        self.setViewportMargins(self.lineNumberAreaWidth(), 0, 0, 0)
    
    def updateLineNumberArea(self, rect, dy):
        """
        Update the line number area
        
        args:
            rect (QRect): The rect to update
            dy (int): the number of pixes the viewport was scrolled
        """
        if dy:
            self._lineNumberArea.scroll(0, dy)
        else:
            self._lineNumberArea.update(0, rect.y(), self._lineNumberArea.width(), rect.height())
            
        if rect.contains(self.viewport().rect()):
            self.updateLineNumberAreaWidth(0)
    
    def lineNumberAreaWidth(self):
        """
        Calculate the line number area width
        
        return:
            int of the line number area size
        """
        digits = 1
        _max = max(1, self.blockCount())
        while (_max >= 10):
            _max /= 10
            digits += 1
            
        return 3 + self.fontMetrics().width('9') * digits
    
    def resizeEvent(self, event):
        """
        ReImplemented
        
        Resize Event
        
        args:
            event (QEvent): The Resize event
        """
        super(CodeEditor, self).resizeEvent(event)
        
        cr = self.contentsRect()
        self._lineNumberArea.setGeometry(QtCore.QRect(cr.left(), cr.top(), self.lineNumberAreaWidth(), cr.height()))
    
    def highlightCurrentLine(self):
        """
        Set the current selected line background colour
        """
        extraSelections = []
        if self.isReadOnly() is False:
            selection = QtGui.QTextEdit.ExtraSelection()
            lineColour = QtGui.QColor(QtCore.Qt.blue)
            
            selection.format.setBackground(lineColour)
            selection.format.setProperty(QtGui.QTextFormat.FullWidthSelection, True)
            selection.cursor = self.textCursor()
            selection.cursor.clearSelection()
            extraSelections.append(selection)
            
        self.setExtraSelections(extraSelections)
        
    def lineNumberAreaPaintEvent(self, event):
        """
        Paint Line Number Area and Number
        
        args:
            event (QEvent): The paint event
        """
        painter = QtGui.QPainter(self._lineNumberArea)
        painter.fillRect(event.rect(), QtCore.Qt.lightGray)
         
        block = self.firstVisibleBlock()
        blockNumber = block.blockNumber()
        top = int(self.blockBoundingGeometry(block).translated(self.contentOffset()).top())
        bottom = top + int(self.blockBoundingRect(block).height())

        while(block.isValid() and top <= event.rect().bottom()):
            if block.isVisible() and bottom >= event.rect().top():
                number = str(blockNumber + 1)
                painter.setPen(QtCore.Qt.black)
                painter.drawText(0, top, self._lineNumberArea.width(), self.fontMetrics().height(), QtCore.Qt.AlignRight, number)
                 
            block = block.next()
            top = bottom
            bottom = top + int(self.blockBoundingRect(block).height())
            blockNumber += 1
        
        
if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    wid = CodeEditor()
    wid.show()
    sys.exit(app.exec_())
