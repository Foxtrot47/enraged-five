from RS.Tools.ShortcutKeys.Dialogs import shortcutKeysDialog


def Run( show = True ):
    toolsDialog = shortcutKeysDialog.ShortcutKeysDialog()
    
    if show:
        toolsDialog.show()

    return toolsDialog

