from pyfbsdk import *
from pyfbsdk_additions import *

from PySide import QtCore, QtGui


import RS.Tools.Face.Facestar.Sliders
import RS.Tools.Face.Facestar.Attributes
import RS.Tools.Face.Facestar.Viewports
import RS.Tools.Face.Facestar.ViewManager

reload( RS.Tools.Face.Facestar.Sliders )
reload( RS.Tools.Face.Facestar.Attributes )
reload( RS.Tools.Face.Facestar.Viewports )


class Character( QtGui.QWidget ):
    '''
    Represents the highest-level tab within the interface.  Each character in the scene
    will receive its own tab, based on the supplied defintion.
    
    Arguments:
    
        parent: The parent QWidget.
        defintion: A RS.Core.Face.Facestar.Definition object.
    '''
    def __init__( self, parent, definition ):
        QtGui.QWidget.__init__( self, parent )
        
        self.__definition = definition
        
        mainLayout = QtGui.QVBoxLayout( self )
        self.setLayout( mainLayout )
        
        self.__tabWidget = QtGui.QTabWidget( self )
        mainLayout.addWidget( self.__tabWidget )
        
        self.__offFaceControlsTab = OffFace( self, definition )
        self.__onFaceControlsTab = OnFace( self, definition )
        self.__poseTab = QtGui.QWidget( self )
        
        self.__tabWidget.addTab( self.__offFaceControlsTab, 'OFF Controls' )
        self.__tabWidget.addTab( self.__onFaceControlsTab, 'ON Controls' )
        self.__tabWidget.addTab( self.__poseTab, 'Pose' )
        
        RS.Tools.Face.Facestar.ViewManager.RegisterView( self.__definition, self )
        
    
    ## Properties ##
    
    @property
    def Definition( self ):
        return self.__definition
    
    @property
    def OffFaceTab( self ):
        return self.__offFaceControlsTab
    
    @property
    def OnFaceTab( self ):
        return self.__onFaceControlsTab
    
    @property
    def PoseTab( self ):
        return self.__poseTab
        
        
    ## Methods ##
    
    def ZoomExtents( self ):
        self.__offFaceControlsTab.ZoomExtents()
    
    def ZoomSelected( self ):
        self.__offFaceControlsTab.ZoomSelected()
    
    def SaveDefinition( self ):
        '''
        Save the current definition.
        '''
        
        # Update the off-face objects in the defintion.
        for slider in self.__offFaceControlsTab.Sliders:
            namespace, name = str( slider.ModelName ).split( ':' )
            
            marker = self.__definition.GetMarker( name )
            
            if marker:
                marker.Position = [ slider.pos().x(), slider.pos().y() ]
                marker.Rotation = slider.Angle
                
        # Update the on-face objects in the defintion.
        for markerItem in self.__onFaceControlsTab.Markers:
            namespace, name = str( markerItem.ModelName ).split( ':' )
            
            marker = self.__definition.GetMarker( name )
            
            if marker:
                marker.Position = [ markerItem.pos().x(), markerItem.pos().y() ]
            
        self.__definition.Save()
    
    def SetMarkerEditMode( self, state ):
        self.__offFaceControlsTab.SetMarkerEditMode( state )
        self.__onFaceControlsTab.SetMarkerEditMode( state )
        
        
class OffFace( QtGui.QWidget ):
    '''
    Widget for the off-face controls.
    
    Arguments:
    
        parent: The parent QWidget.
        defintion: A RS.Core.Face.Facestar.Definition object.
    '''
    def __init__( self, parent, definition ):
        QtGui.QWidget.__init__( self, parent )
        
        self.__definition = definition
        self.__zoomFactor = 1.1
        
        # Setup the scene and graphics view.
        self.__scene = QtGui.QGraphicsScene()
        self.__view = RS.Tools.Face.Facestar.Viewports.OffFaceViewport( self.__scene, self, definition )
        self.__onFaceView = OnFace( self, definition )

        mainSplitter = QtGui.QSplitter( QtCore.Qt.Horizontal )
        
        mainLayout = QtGui.QVBoxLayout( self )
        mainLayout.addWidget( mainSplitter )
        
        viewportSplitter = QtGui.QSplitter( QtCore.Qt.Horizontal )
        
        # Character image.
        self.__image = QtGui.QPixmap( self.__definition.OffFaceImageFilename )
        self.__imageItem = QtGui.QGraphicsPixmapItem( self.__image )
        self.__scene.addItem( self.__imageItem )
        
        # Setup sliders.
        self.__sliders = []
        
        for markerName, marker in self.__definition.OffFaceMarkers.iteritems():
            if marker.Visible:
                self.__sliders.append( RS.Tools.Face.Facestar.Sliders.OffFaceSlider( marker, definition, self.__view ) )
                
            # if marker is positioned at 0,0.
        
        for slider in self.__sliders:
            self.__scene.addItem( slider )
        
        # Attribute sliders widget.   
        self.__attributeSlidersSidebar = RS.Tools.Face.Facestar.Attributes.Sidebar( self, definition )
        
        # Add widgets to the splitter.
        #mainSplitter.addWidget( self.__view )
        #viewportSplitter.moveSplitter( -100,0 )
        viewportSplitter.addWidget( self.__onFaceView )
        viewportSplitter.addWidget( self.__view )
        
        mainSplitter.addWidget( viewportSplitter )
        mainSplitter.addWidget( self.__attributeSlidersSidebar )
        
        mainSplitter.setStretchFactor( 0, 1 )
        viewportSplitter.setSizes([1, 150])
       
       
    ## Properties ##
    
    @property
    def Sliders( self ):
        return self.__sliders
    
    @property
    def Viewport( self ):
        return self.__view
    
    @property
    def OnFaceView( self ):
        return self.__onFaceView
    
    @property
    def Scene( self ):
        return self.__scene
    
        
    ## Methods ##
    
    def ZoomExtents( self ):            
        self.Viewport.fitInView( self.__image.rect(), aspectRadioMode = QtCore.Qt.AspectRatioMode.KeepAspectRatio )
    
    def ZoomSelected( self ):
        selectedSliders = RS.Tools.Face.Facestar.SelectionManager.GetSelectedOffFaceMarkers( self.__definition )
        
        if selectedSliders:
            x = []
            y = []
            
            for slider in selectedSliders:
                x.append( slider.sceneBoundingRect().left() )
                x.append( slider.sceneBoundingRect().right() )
                y.append( slider.sceneBoundingRect().top() )
                y.append( slider.sceneBoundingRect().bottom() )
                
            minX = min( x )
            maxX = max( x )
            minY = min( y )
            maxY = max( y )
            
            # Extra space around the zoom area, in graphics view units.
            zoomBuffer = 20
            
            zoomRect = QtCore.QRect( minX - ( zoomBuffer / 2.0 ), minY - ( zoomBuffer / 2.0 ), 1, 1 )
            zoomRect.setBottomRight( QtCore.QPoint( maxX + ( zoomBuffer / 2.0 ), maxY + ( zoomBuffer / 2.0 ) ) )
            
            sceneRect = self.Viewport.rect()            
            sceneAspectRatio = float( sceneRect.width() ) / float( sceneRect.height() )
            
            oldCenter = zoomRect.center()

            if zoomRect.width() > zoomRect.height():
                zoomRect.setHeight( float( zoomRect.width() ) / sceneAspectRatio )
            
            elif zoomRect.width() <= zoomRect.height():
                zoomRect.setWidth( float( zoomRect.height() ) * sceneAspectRatio )
                
            zoomRect.moveCenter( oldCenter )

            self.Viewport.fitInView( zoomRect, aspectRatioMode = QtCore.Qt.AspectRatioMode.KeepAspectRatioByExpanding )
            
            #item = QtGui.QGraphicsRectItem( zoomRect )
            #view.OffFaceTab.Scene.addItem( item )    
    
    def SelectMarker( self, markerName ):
        for slider in self.__sliders:
            if slider.MarkerObject.Name == markerName:
                slider.Marker.setSelected( True )
    
    def GetSelectedSliders( self ):
        sliders = []
        
        for slider in self.__sliders:
            if slider.Marker.isSelected():
                sliders.append( slider )
                
        return sliders

    def SetMarkerEditMode( self, state ):
        '''
        Enable/disable marker edit mode.
        
        Arguments:
        
            state: Boolean of whether to enable/disable the editing mode.
        '''
        for slider in self.__sliders:
            slider.setFlag( QtGui.QGraphicsItem.ItemIsMovable, state )
            slider.setFlag( QtGui.QGraphicsItem.ItemIsSelectable, state )
            
            slider.Marker.setFlag( QtGui.QGraphicsItem.ItemIsMovable, not state )
            slider.Marker.setFlag( QtGui.QGraphicsItem.ItemIsSelectable, not state )

    
class OnFace( QtGui.QWidget ):
    '''
    Widget for the on-face controls.
    
    Arguments:
    
        parent: The parent QWidget.
        defintion: A RS.Core.Face.Facestar.Definition object.
    '''
    def __init__( self, parent, definition ):
        QtGui.QWidget.__init__( self, parent )
        
        self.__scene = QtGui.QGraphicsScene()
        self.__view = RS.Tools.Face.Facestar.Viewports.OnFaceViewport( self.__scene, self, definition )
        
        layout = QtGui.QHBoxLayout( self )
        layout.addWidget( self.__view )
        
        self.__definition = definition
        
        self.__image = QtGui.QPixmap( self.__definition.OnFaceImageFilename )
        self.__imageItem = QtGui.QGraphicsPixmapItem( self.__image )
        self.__scene.addItem( self.__imageItem )
        
        #self.__faceVideo = QtGui.QPixmap( 'x:\\temp\\Tracey-Family_1_INTp1_IM_frames\\Tracey-Family_1_INTp1_IM.0000.jpg' )
        #self.__faceVideoItem = QtGui.QGraphicsPixmapItem( self.__faceVideo )
        #self.__scene.addItem( self.__faceVideoItem )
        
        self.__markers = []
        
        for markerName, marker in self.__definition.OnFaceMarkers.iteritems():
            if marker.Visible:
                onFaceMarker = RS.Tools.Face.Facestar.Markers.OnFace( marker, parent, definition, marker.Position )
                self.__markers.append( onFaceMarker )
                self.__scene.addItem( onFaceMarker )
    
    
    ## Properties ##
    
    @property
    def Markers( self ):
        return self.__markers
    
    @property
    def Viewport( self ):
        return self.__view
    
    
    ## Methods ##
    
    def SetMarkerEditMode( self, state ):
        '''
        Enable/disable marker edit mode.
        
        Arguments:
        
            state: Boolean of whether to enable/disable the editing mode.
        '''
        for marker in self.__markers:
            marker.setFlag( QtGui.QGraphicsItem.ItemIsMovable, state )
            
    def GetSelectedMarkers( self ):
        markers = []
        
        for marker in self.__markers:
            if marker.isSelected():
                markers.append( marker )
                
        return markers