from pyfbsdk import *
from pyfbsdk_additions import *

from PySide import QtCore, QtGui

import RS.Tools.Face.Facestar

class OffFaceViewport( QtGui.QGraphicsView ):
    def __init__( self, scene, parent, definition, *args, **kwargs ):
        QtGui.QGraphicsView.__init__( self, scene, parent, *args, **kwargs )
        
        self.setRenderHint( QtGui.QPainter.Antialiasing, True )
        self.setViewportUpdateMode( QtGui.QGraphicsView.FullViewportUpdate )
        self.setBackgroundBrush( QtGui.QBrush( QtGui.QColor( 50, 50, 50 ) ) )
        self.setDragMode( QtGui.QGraphicsView.NoDrag )
        self.setHorizontalScrollBarPolicy( QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff )
        self.setVerticalScrollBarPolicy( QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff )
        self.setFocusPolicy( QtCore.Qt.NoFocus )
        
        self.__parent = parent
        self.__scene = scene
        self.__definition = definition
        
        self.__zoomFactor = 1.1
        
        self.__boxSelection = None
        self.__middlePressed = False
        self.__leftPressed = False
        self.__controlPressed = False
        
        self.CreateActions()
        
    @property
    def Parent ( self ):
        return self.__parent
        
    
    ## Methods ##
    
    def CreateActions( self ):
        self.__actionNewSelectionSet = QtGui.QAction( 'New', self, triggered = self.CreateNewSelectionSet )
        
    def StartBoxSelection( self, pos ):
        RS.Tools.Face.Facestar.MODE_BOX_SELECTION = True
        
        self.__boxSelectionStartPos = pos
        
        self.__boxSelection = QtGui.QGraphicsRectItem()
        self.__boxSelection.setRect( pos.x(), pos.y(), 1, 1 )
        
        pen = QtGui.QPen()
        pen.setWidth( 1 )
        pen.setStyle( QtCore.Qt.DashLine )
        
        self.__boxSelection.setPen( pen )
        
        self.__scene.addItem( self.__boxSelection )
    
    def EndBoxSelection( self ):
        RS.Tools.Face.Facestar.MODE_BOX_SELECTION = False
        
        for item in self.__parent.Sliders:
            if self.__boxSelection.boundingRect().intersects( item.Marker.sceneBoundingRect() ):
                RS.Tools.Face.Facestar.SelectionManager.Select( self.__definition, item )
        
        self.__scene.removeItem( self.__boxSelection )
        
    
    ## Event Handlers ##
    
    def CreateNewSelectionSet( self ):
        text, okResult = QtGui.QInputDialog.getText( self, 'New Selection Set', 'Name:' )
                
        if okResult:
            selectionSet = RS.Tools.Face.Facestar.LocalConfig.CreateNewSelectionSet( text )
            
            selectedSliders = self.__parent.GetSelectedSliders()
            
            for slider in selectedSliders:
                selectionSet.AddOffFaceMarker( slider.MarkerObject.Name )
                
            RS.Tools.Face.Facestar.LocalConfig.Save()
    
    def AddToSelectionSet( self ):
        selectionSet = self.sender().data()
        selectedSliders = self.__parent.GetSelectedSliders()
        
        if selectionSet and selectedSliders:
            for slider in selectedSliders:
                selectionSet.AddOffFaceMarker( slider.MarkerObject.Name )
                
            RS.Tools.Face.Facestar.LocalConfig.Save()
    
    def SelectSelectionSet( self ):
        selectionSet = self.sender().data()
        
        for itemName in selectionSet.OffFaceMarkers:
            self.__parent.SelectMarker( itemName )
            
    def ReselectAllMarkers( self ):
        for item in self.__parent.Sliders:
            item.Marker.setSelected( False )
        for item in RS.Tools.Face.Facestar.SelectionManager.GetSelected( self.__definition ):
            item.setSelected( True )
            
    def UpdateSelection( self ):
        RS.Tools.Face.Facestar.SelectionManager.UpdateSelection( self.__definition, self.__parent.Sliders )
            
    def CapturePosition( self ):
        for each in RS.Tools.Face.Facestar.SelectionManager.GetSelected( self.__definition ):
            each.CapturePosition()
            
    def CaptureCurrentPosition( self ):
        for each in RS.Tools.Face.Facestar.SelectionManager.GetSelected( self.__definition ):
            each.CaptureCurrentPosition()
        
    ## Overrides ##
    
    '''
    def keyPressEvent( self, event ):
        if event.key() == QtCore.Qt.Key.Key_Control:
            RS.Tools.Face.Facestar.ControlPressed = True
            
        elif event.key() == QtCore.Qt.Key.Key_Alt:
            RS.Tools.Face.Facestar.AltPressed = True
            
        elif event.key() == QtCore.Qt.Key.Key_Shift:
            RS.Tools.Face.Facestar.ShiftPressed = True
            
        elif event.key() == QtCore.Qt.Key.Key_Z:
            selectedItems = RS.Tools.Face.Facestar.SelectionManager.GetSelectedOffFaceMarkers( self.GetCurrentDefinition() )
            
            if selectedItems:
                self.ZoomSelected()
                
            else:
                self.ZoomExtents()
        
        QtGui.QGraphicsView.keyPressEvent( self, event )
        
    def keyReleaseEvent( self, event ):
        if event.key() == QtCore.Qt.Key.Key_Control:
            RS.Tools.Face.Facestar.ControlPressed = False
            
        elif event.key() == QtCore.Qt.Key.Key_Alt:
            RS.Tools.Face.Facestar.AltPressed = False
            
        elif event.key() == QtCore.Qt.Key.Key_Shift:
            RS.Tools.Face.Facestar.ShiftPressed = False
            
        QtGui.QGraphicsView.keyReleaseEvent( self, event )
    '''
    
    def contextMenuEvent( self, event ):
        menu = QtGui.QMenu( self )
        
        # Add to selection set.
        if len( self.__parent.GetSelectedSliders() ) > 0:
            selectionSetsMenu = QtGui.QMenu( 'Add to Selection Set', self )
            selectionSetsMenu.addAction( self.__actionNewSelectionSet )
            
            if RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
                selectionSetsMenu.addSeparator()
                
                for selectionSet in RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
                    if selectionSet.OffFaceMarkers:
                        actionSelectionSet = QtGui.QAction( selectionSet.Name, self, triggered = self.AddToSelectionSet )
                        actionSelectionSet.setData( selectionSet )
                        selectionSetsMenu.addAction( actionSelectionSet )
            
            menu.addMenu( selectionSetsMenu )
            
        # Select selection set.
        selectSelectionSetsMenu = QtGui.QMenu( 'Select Selection Set', self )
        
        if RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
            for selectionSet in RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
                if selectionSet.OffFaceMarkers:
                    actionSelectionSet = QtGui.QAction( selectionSet.Name, self, triggered = self.SelectSelectionSet )
                    actionSelectionSet.setData( selectionSet )
                    selectSelectionSetsMenu.addAction( actionSelectionSet )
        
        menu.addMenu( selectSelectionSetsMenu )
        menu.exec_( event.globalPos() )
        
    def mousePressEvent( self, event ):
        self.__lastMousePos = event.pos()
        
        self.__middlePressed = True if event.button() == QtCore.Qt.MouseButton.MiddleButton else False
        self.__leftPressed = True if event.button() == QtCore.Qt.MouseButton.LeftButton else False
        self.__rightPressed = True if event.button() == QtCore.Qt.MouseButton.RightButton else False

        if self.__leftPressed:
            self.setDragMode( QtGui.QGraphicsView.RubberBandDrag )
            if RS.Tools.Face.Facestar.ControlPressed:
                self.__controlPressed = True
                #RS.Tools.Face.Facestar.SelectionManager.ReselectAllMarkers( self.__definition )
                
        elif self.__middlePressed:
            self.setDragMode( QtGui.QGraphicsView.ScrollHandDrag )
            
        else:
            self.setDragMode( QtGui.QGraphicsView.NoDrag )
        
        QtGui.QGraphicsView.mousePressEvent( self, event )
        
        self.UpdateSelection()
        
        #if self.__rightPressed or RS.Tools.Face.Facestar.ControlPressed:
        #    RS.Tools.Face.Facestar.SelectionManager.ReselectAllMarkers( self.__definition )
            
    
    def mouseMoveEvent( self, event ):
        if self.__middlePressed:
            delta = event.pos() - self.__lastMousePos
            
            hBar = self.horizontalScrollBar()
            vBar = self.verticalScrollBar()
            
            hBar.setValue( hBar.value() - delta.x() )
            vBar.setValue( vBar.value() - delta.y() )
            
            self.__lastMousePos = event.pos()
        
        if self.__controlPressed:
            pass
            
        QtGui.QGraphicsView.mouseMoveEvent( self, event )
        
    def mouseReleaseEvent( self, event ):
        if self.__controlPressed :
            self.__controlPressed = False
        
        if self.__middlePressed:
            self.setDragMode( QtGui.QGraphicsView.NoDrag )
        
        self.__leftPressed = False
        self.__middlePressed = False
        self.__rightPressed = False
        
        self.UpdateSelection()
        
        QtGui.QGraphicsView.mouseReleaseEvent( self, event )
        
        RS.Tools.Face.Facestar.SelectionManager.PrintStoredItems()
    
    def wheelEvent( self, event ):
        self.setTransformationAnchor( QtGui.QGraphicsView.ViewportAnchor.AnchorUnderMouse )
        
        if event.delta() > 0:
            self.scale( self.__zoomFactor, self.__zoomFactor )
            
        else:
            self.scale( 1.0 / self.__zoomFactor, 1.0 / self.__zoomFactor )
            
    def drawForeground( self, painter, rect ):
        if RS.Tools.Face.Facestar.MODE_EDIT_MARKERS:
            pen = QtGui.QPen()
            painter.setPen( pen )
            
            if hasattr( self.__parent, 'Sliders' ):
                for item in self.__parent.Sliders:
                    pos = item.sceneBoundingRect().center()
                    painter.drawText( pos.x() + 4, pos.y(), item.MarkerObject.DisplayName )
                    
                    

class OnFaceViewport( QtGui.QGraphicsView ):
    def __init__( self, scene, parent, definition, *args, **kwargs ):
        QtGui.QGraphicsView.__init__( self, scene, parent, *args, **kwargs )
        
        self.setRenderHint( QtGui.QPainter.Antialiasing, True )
        self.setViewportUpdateMode( QtGui.QGraphicsView.FullViewportUpdate )
        self.setBackgroundBrush( QtGui.QBrush( QtGui.QColor( 50, 50, 50 ) ) )
        self.setDragMode( QtGui.QGraphicsView.NoDrag )
        self.setHorizontalScrollBarPolicy( QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff )
        self.setVerticalScrollBarPolicy( QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff )
        self.setFocusPolicy( QtCore.Qt.NoFocus )
        
        self.__parent = parent
        self.__scene = scene
        self.__definition = definition
        
        self.__zoomFactor = 1.1
        
        self.__boxSelection = None
        self.__middlePressed = False
        self.__leftPressed = False
        self.__controlPressed = False
        
        self.CreateActions()
        
        
    ## Methods ##
    
    def CreateActions( self ):
        self.__actionNewSelectionSet = QtGui.QAction( 'New', self, triggered = self.CreateNewSelectionSet )
        
    def StartBoxSelection( self, pos ):
        RS.Tools.Face.Facestar.MODE_BOX_SELECTION = True
        
        self.__boxSelectionStartPos = pos
        
        self.__boxSelection = QtGui.QGraphicsRectItem()
        self.__boxSelection.setRect( pos.x(), pos.y(), 1, 1 )
        
        pen = QtGui.QPen()
        pen.setWidth( 1 )
        pen.setStyle( QtCore.Qt.DashLine )
        
        self.__boxSelection.setPen( pen )
        
        self.__scene.addItem( self.__boxSelection )
    
    def EndBoxSelection( self ):
        RS.Tools.Face.Facestar.MODE_BOX_SELECTION = False
        
        for item in self.__parent.Markers:
            if self.__boxSelection.boundingRect().intersects( item.sceneBoundingRect() ):
                RS.Tools.Face.Facestar.SelectionManager.Select( self.__definition, item )
        
        self.__scene.removeItem( self.__boxSelection )
            
    def ReselectAllMarkers( self ):
        '''
        for item in self.__parent.Markers:
            item.setSelected( False )
        '''
        for item in RS.Tools.Face.Facestar.SelectionManager.GetSelected( self.__definition ):
            item.setSelected( True )
            
    def UpdateSelection( self ):
        
        RS.Tools.Face.Facestar.SelectionManager.UpdateSelection( self.__definition, self.__parent.Markers )
        
        '''
        RS.Tools.Face.Facestar.SelectionManager.DeselectAllSceneObj()
        RS.Tools.Face.Facestar.SelectionManager.DeselectOffFaceMarkers( self.__definition )
        RS.Tools.Face.Facestar.SelectionManager.DeselectAttributes( self.__definition )
        RS.Tools.Face.Facestar.SelectionManager.ClearStoredItems( self.__definition )
        
        items = []
        for item in self.__parent.Markers:
            if item.isSelected() == True:
                items.append( item )
                item.Model.Selected = True
        
        for item in items:
            RS.Tools.Face.Facestar.SelectionManager.Select( self.__definition, item )
        '''
    
    
    ## Event Handlers ##
            
    def CreateNewSelectionSet( self ):
        text, okResult = QtGui.QInputDialog.getText( self, 'New Selection Set', 'Name:' )
        
        if okResult:
            selectionSet = RS.Tools.Face.Facestar.LocalConfig.CreateNewSelectionSet( text )
            
            selectedSliders = self.__parent.OnFace.GetSelectedSliders()
            
            for slider in selectedSliders:
                selectionSet.AddOffFaceMarker( slider.MarkerObject.Name )
            
            RS.Tools.Face.Facestar.LocalConfig.Save()
    
    def AddToSelectionSet( self ):
        selectionSet = self.sender().data()
        selectedSliders = self.__parent.OnFace.GetSelectedSliders()
        
        if selectionSet and selectedSliders:
            for slider in selectedSliders:
                selectionSet.AddOffFaceMarker( slider.MarkerObject.Name )
                
            RS.Tools.Face.Facestar.LocalConfig.Save()
            
    def SelectSelectionSet( self ):
        selectionSet = self.sender().data()
        
        for itemName in selectionSet.OnFaceMarkers:
            self.__parent.OnFace.SelectMarker( itemName )
    
    
    ## Overrides ##
    
    '''
    def keyPressEvent( self, event ):
        if event.key() == QtCore.Qt.Key.Key_Control:
            RS.Tools.Face.Facestar.ControlPressed = True
            
        elif event.key() == QtCore.Qt.Key.Key_Alt:
            RS.Tools.Face.Facestar.AltPressed = True
            
        elif event.key() == QtCore.Qt.Key.Key_Shift:
            RS.Tools.Face.Facestar.ShiftPressed = True
            
        elif event.key() == QtCore.Qt.Key.Key_Z:
            selectedItems = RS.Tools.Face.Facestar.SelectionManager.GetSelectedOffFaceMarkers( self.GetCurrentDefinition() )
            
            if selectedItems:
                self.ZoomSelected()
                
            else:
                self.ZoomExtents()
        
        QtGui.QGraphicsView.keyPressEvent( self, event )
    
    
    def keyReleaseEvent( self, event ):
        if event.key() == QtCore.Qt.Key.Key_Control:
            RS.Tools.Face.Facestar.ControlPressed = False
            
        elif event.key() == QtCore.Qt.Key.Key_Alt:
            RS.Tools.Face.Facestar.AltPressed = False
            
        elif event.key() == QtCore.Qt.Key.Key_Shift:
            RS.Tools.Face.Facestar.ShiftPressed = False
            
        QtGui.QGraphicsView.keyReleaseEvent( self, event )
    '''
    
    def contextMenuEvent( self, event ):
        menu = QtGui.QMenu( self )
        
        # Add to selection set.
        if len( self.__parent.GetSelectedSliders() ) > 0:
            selectionSetsMenu = QtGui.QMenu( 'Add to Selection Set', self )
            selectionSetsMenu.addAction( self.__actionNewSelectionSet )
            
            if RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
                selectionSetsMenu.addSeparator()
                
                for selectionSet in RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
                    if selectionSet.OffFaceMarkers:
                        actionSelectionSet = QtGui.QAction( selectionSet.Name, self, triggered = self.AddToSelectionSet )
                        actionSelectionSet.setData( selectionSet )
                        selectionSetsMenu.addAction( actionSelectionSet )
            
            menu.addMenu( selectionSetsMenu )
            
        # Select selection set.
        selectSelectionSetsMenu = QtGui.QMenu( 'Select Selection Set', self )
        
        if RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
            for selectionSet in RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
                if selectionSet.OffFaceMarkers:
                    actionSelectionSet = QtGui.QAction( selectionSet.Name, self, triggered = self.SelectSelectionSet )
                    actionSelectionSet.setData( selectionSet )
                    selectSelectionSetsMenu.addAction( actionSelectionSet )
        
        menu.addMenu( selectSelectionSetsMenu )
        menu.exec_( event.globalPos() )
        
    def mousePressEvent( self, event ):
        
        self.__lastMousePos = event.pos()
        
        self.__middlePressed = True if event.button() == QtCore.Qt.MouseButton.MiddleButton else False
        self.__leftPressed = True if event.button() == QtCore.Qt.MouseButton.LeftButton else False
        self.__rightPressed = True if event.button() == QtCore.Qt.MouseButton.RightButton else False
        
        if self.__leftPressed:
            if RS.Tools.Face.Facestar.ControlPressed:
                self.__controlPressed = True
                #RS.Tools.Face.Facestar.SelectionManager.ReselectAllMarkers( self.__definition )
            self.setDragMode( QtGui.QGraphicsView.RubberBandDrag )
            
        elif self.__middlePressed:
            self.setDragMode( QtGui.QGraphicsView.ScrollHandDrag )
            
        else:
            self.setDragMode( QtGui.QGraphicsView.NoDrag )
        
        QtGui.QGraphicsView.mousePressEvent( self, event )
        
        self.UpdateSelection()
        
        if self.__rightPressed or RS.Tools.Face.Facestar.ControlPressed:
            RS.Tools.Face.Facestar.SelectionManager.ReselectAllMarkers( self.__definition )
            
    def mouseMoveEvent( self, event ):
        if self.__middlePressed:
            delta = event.pos() - self.__lastMousePos
            
            hBar = self.horizontalScrollBar()
            vBar = self.verticalScrollBar()
            
            hBar.setValue( hBar.value() - delta.x() )
            vBar.setValue( vBar.value() - delta.y() )
            
            self.__lastMousePos = event.pos()
            
        QtGui.QGraphicsView.mouseMoveEvent( self, event )
    
    def mouseReleaseEvent( self, event ):
        if self.__middlePressed:
            self.setDragMode( QtGui.QGraphicsView.NoDrag )
            
        self.__leftPressed = False
        self.__middlePressed = False
        self.__controlPressed = False
        
        self.UpdateSelection()
        
        QtGui.QGraphicsView.mouseReleaseEvent( self, event )
        
        RS.Tools.Face.Facestar.SelectionManager.PrintStoredItems()

    def wheelEvent( self, event ):
        self.setTransformationAnchor( QtGui.QGraphicsView.ViewportAnchor.AnchorUnderMouse )
        
        if event.delta() > 0:
            self.scale( self.__zoomFactor, self.__zoomFactor )
            
        else:
            self.scale( 1.0 / self.__zoomFactor, 1.0 / self.__zoomFactor )
            
    def drawForeground( self, painter, rect ):
        if RS.Tools.Face.Facestar.MODE_EDIT_MARKERS:
            pen = QtGui.QPen()
            painter.setPen( pen )
            
            if hasattr( self.__parent, 'Sliders' ):
                for item in self.__parent.Sliders:
                    pos = item.sceneBoundingRect().center()
                    painter.drawText( pos.x() + 4, pos.y(), item.MarkerObject.DisplayName )