
from PySide import QtCore, QtGui

import RS.Core.Face.Facestar
reload( RS.Core.Face.Facestar )

import RS.Tools.Face.Facestar


class ManagerDialog( QtGui.QDialog ):
    def __init__( self, parent, definition ):
        QtGui.QDialog.__init__( self, parent )
        
        self.__definition = definition
        
        self.resize( 600, 500 )
        self.setWindowTitle( 'Facestar - Selection Set Manager' )
        self.setModal( True )
        
        mainLayout = QtGui.QVBoxLayout()
        self.setLayout( mainLayout )
        
        treeListLayout = QtGui.QHBoxLayout()
        addDelLayout = QtGui.QVBoxLayout()
        newRenameDelLayout = QtGui.QHBoxLayout()
        
        self.__selectionSetsDropdown = QtGui.QComboBox( self )
        #self.__selectionSetsDropdown.setMaximumWidth( 200 )
        
        # New selection set button.
        self.__newSelectionSetButton = QtGui.QPushButton( self )
        newSelectionSetIcon = QtGui.QIcon()
        newSelectionSetIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/newSelectionSet.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.__newSelectionSetButton.setIcon( newSelectionSetIcon )
        self.__newSelectionSetButton.setIconSize( QtCore.QSize( 24, 24 ) )
        self.__newSelectionSetButton.resize( 24, 24 )
        self.__newSelectionSetButton.setMaximumWidth( 40 )
        self.__newSelectionSetButton.setToolTip( 'Create New Selection Set' )
        
        # Rename selection set button.
        self.__renameSelectionSetButton = QtGui.QPushButton( self )
        renameSelectionSetIcon = QtGui.QIcon()
        renameSelectionSetIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/renameSelectionSet.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.__renameSelectionSetButton.setIcon( renameSelectionSetIcon )
        self.__renameSelectionSetButton.setIconSize( QtCore.QSize( 24, 24 ) )
        self.__renameSelectionSetButton.resize( 24, 24 )
        self.__renameSelectionSetButton.setMaximumWidth( 40 )
        self.__renameSelectionSetButton.setToolTip( 'Rename Selection Set' )        

        # Delete selection set button.
        self.__deleteSelectionSetButton = QtGui.QPushButton( self )
        deleteSelectionSetIcon = QtGui.QIcon()
        deleteSelectionSetIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/trash.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.__deleteSelectionSetButton.setIcon( deleteSelectionSetIcon )
        self.__deleteSelectionSetButton.setIconSize( QtCore.QSize( 24, 24 ) )
        self.__deleteSelectionSetButton.resize( 24, 24 )
        self.__deleteSelectionSetButton.setMaximumWidth( 40 )
        self.__deleteSelectionSetButton.setToolTip( 'Delete Selection Set' )         

        self.__availableChoicesView = QtGui.QTreeWidget( self )
        self.__selectionSetView = QtGui.QTreeWidget( self )
        
        self.__addButton = QtGui.QPushButton( self )
        self.__addButton.resize( 24, 24 )
        
        addIcon = QtGui.QIcon()
        addIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/addItem.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.__addButton.setIcon( addIcon )
        self.__addButton.setIconSize( QtCore.QSize( 24, 24 ) )
        
        self.__delButton = QtGui.QPushButton( self )
        self.__delButton.resize( 24, 24 )
        self.__delButton.setIconSize( QtCore.QSize( 24, 24 ) )
        
        deleteIcon = QtGui.QIcon()
        deleteIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/delete.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.__delButton.setIcon( deleteIcon )
        
        self.__availableChoicesView.setColumnCount( 1 )
        self.__availableChoicesView.setHeaderLabel( 'Available' )
        
        self.__selectionSetView.setColumnCount( 1 )
        self.__selectionSetView.setHeaderLabel( 'Selection Set' )

        addDelLayout.addWidget( self.__addButton )
        addDelLayout.addWidget( self.__delButton )
        addDelLayout.setAlignment( self.__addButton, QtCore.Qt.AlignBottom )
        addDelLayout.setAlignment( self.__delButton, QtCore.Qt.AlignTop )
        
        newRenameDelLayout.addWidget( self.__selectionSetsDropdown )
        newRenameDelLayout.addWidget( self.__newSelectionSetButton )
        newRenameDelLayout.addWidget( self.__renameSelectionSetButton )
        newRenameDelLayout.addWidget( self.__deleteSelectionSetButton )
        #newRenameDelLayout.setAlignment( self.__selectionSetsDropdown, QtCore.Qt.AlignLeft )
        #newRenameDelLayout.setAlignment( self.__newSelectionSetButton, QtCore.Qt.AlignLeft )
        #newRenameDelLayout.setAlignment( self.__renameSelectionSetButton, QtCore.Qt.AlignLeft )
        #newRenameDelLayout.setAlignment( self.__deleteSelectionSetButton, QtCore.Qt.AlignLeft )

        treeListLayout.addWidget( self.__availableChoicesView )
        treeListLayout.addLayout( addDelLayout )
        treeListLayout.addWidget( self.__selectionSetView )
        
        mainLayout.addLayout( newRenameDelLayout )
        mainLayout.addLayout( treeListLayout )
        
        # Bind event handlers.
        self.__selectionSetsDropdown.currentIndexChanged.connect( self.OnSelectionSetPicked )
        self.__addButton.pressed.connect( self.AddSelectedToSelectionSet )
        self.__delButton.pressed.connect( self.RemoveSelectedFromSelectionSet )
        self.__newSelectionSetButton.pressed.connect( self.OnNewSelectionSetPressed )
        self.__renameSelectionSetButton.pressed.connect( self.OnRenameSelectionSetPressed )
        self.__deleteSelectionSetButton.pressed.connect( self.OnDeleteSelectionSetPressed )
        
        # Tree node icon.
        self.__treeIcon = QtGui.QIcon()
        self.__treeIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/group.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        
        # Populate selection sets.
        self.RefreshAll()
        
    
    ## Methods ##
    
    def GetCurrentSelectionSet( self ):
        if RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
            return RS.Tools.Face.Facestar.LocalConfig.SelectionSets[ self.__selectionSetsDropdown.currentIndex() ]
        
        return None
                    
    def RefreshAll( self ):
        self.RefreshSelectionSetsDropdown()
        self.RefreshAvailableChoicesView() 
        self.RefreshCurrentSelectionSetView()
        self.RefreshButtons()
        
    def RefreshCurrentSelectionSetView( self ):
        selectionSet = self.GetCurrentSelectionSet()
        
        self.__selectionSetView.clear()
        self.__selectionSetView.setHeaderLabel( 'Selection Set' )
        
        if selectionSet:
            self.__selectionSetView.setHeaderLabel( selectionSet.Name )
        
            # Off-face markers.
            rootOffFace = QtGui.QTreeWidgetItem( self.__selectionSetView )
            rootOffFace.setText( 0, 'Off Face' )
            rootOffFace.setIcon( 0, self.__treeIcon )
            
            markerNames = selectionSet.OffFaceMarkers
            markerNames.sort()
            
            for markerName in markerNames:
                marker = self.__definition.GetMarker( markerName )
                
                rootMarker = QtGui.QTreeWidgetItem( rootOffFace )
                rootMarker.setText( 0, marker.DisplayName )
                rootMarker.setData( 0, QtCore.Qt.UserRole, marker )            
            
            # On-face markers.
            rootOnFace = QtGui.QTreeWidgetItem( self.__selectionSetView )
            rootOnFace.setText( 0, 'On Face' )
            rootOnFace.setIcon( 0, self.__treeIcon )
            
            markerNames = selectionSet.OnFaceMarkers
            markerNames.sort()
            
            for markerName in markerNames:
                marker = self.__definition.GetMarker( markerName )
                
                rootMarker = QtGui.QTreeWidgetItem( rootOnFace )
                rootMarker.setText( 0, marker.DisplayName )
                rootMarker.setData( 0, QtCore.Qt.UserRole, marker )            
            
            # Attributes.
            rootAttributes = QtGui.QTreeWidgetItem( self.__selectionSetView )
            rootAttributes.setText( 0, 'Attributes' )
            rootAttributes.setIcon( 0, self.__treeIcon )
            
            attrNames = selectionSet.Attributes
            attrData = {}
            
            for attrName in attrNames:
                result = self.__definition.GetAttribute( attrName )
                
                if result:
                    attrGroup, attribute = result
                    
                    if attrGroup.Name not in attrData:
                        attrData[ attrGroup.Name ] = [ attribute ]
                        
                    else:
                        attrData[ attrGroup.Name ].append( attribute )
                    
            for attrGroupName, attributes in attrData.iteritems():
                rootAttrGroup = QtGui.QTreeWidgetItem( rootAttributes )
                rootAttrGroup.setText( 0, attrGroupName )
                rootAttrGroup.setIcon( 0, self.__treeIcon )
                #rootAttrGroup.setData( 0, QtCore.Qt.UserRole, attrGroup )
                
                for attribute in attributes:
                    rootAttribute = QtGui.QTreeWidgetItem( rootAttrGroup )
                    rootAttribute.setText( 0, attribute.DisplayName )
                    rootAttribute.setData( 0, QtCore.Qt.UserRole, attribute )        
                    
            self.__selectionSetView.expandAll()
    
    def RefreshAvailableChoicesView( self ):
        self.__availableChoicesView.clear()
        selectionSet = self.GetCurrentSelectionSet()
        
        if selectionSet:
            
            # Off-face markers.
            rootOffFace = QtGui.QTreeWidgetItem( self.__availableChoicesView )
            rootOffFace.setText( 0, 'Off Face' )
            rootOffFace.setIcon( 0, self.__treeIcon )
            
            markerNames = self.__definition.OffFaceMarkers.keys()
            markerNames.sort()
            
            for markerName in markerNames:
                marker = self.__definition.OffFaceMarkers[ markerName ]
                
                rootMarker = QtGui.QTreeWidgetItem( rootOffFace )
                rootMarker.setText( 0, marker.DisplayName )
                rootMarker.setData( 0, QtCore.Qt.UserRole, marker )
                
                # Disable if marker already in current selection set.
                if selectionSet.HasItem( marker.Name ):
                    rootMarker.setDisabled( True )
            
            # On-face markers.
            rootOnFace = QtGui.QTreeWidgetItem( self.__availableChoicesView )
            rootOnFace.setText( 0, 'On Face' )
            rootOnFace.setIcon( 0, self.__treeIcon )
            
            markerNames = self.__definition.OnFaceMarkers.keys()
            markerNames.sort()
            
            for markerName in markerNames:
                marker = self.__definition.OnFaceMarkers[ markerName ]
                
                rootMarker = QtGui.QTreeWidgetItem( rootOnFace )
                rootMarker.setText( 0, marker.DisplayName )
                rootMarker.setData( 0, QtCore.Qt.UserRole, marker )
                
                # Disable if marker already in current selection set.
                if selectionSet.HasItem( marker.Name ):
                    rootMarker.setDisabled( True )            
            
            # Attributes.
            rootAttributes = QtGui.QTreeWidgetItem( self.__availableChoicesView )
            rootAttributes.setText( 0, 'Attributes' )
            rootAttributes.setIcon( 0, self.__treeIcon )
            
            for attrGroup in self.__definition.AttributeGroups:
                rootAttrGroup = QtGui.QTreeWidgetItem( rootAttributes )
                rootAttrGroup.setText( 0, attrGroup.Name )
                rootAttrGroup.setData( 0, QtCore.Qt.UserRole, attrGroup )
                rootAttrGroup.setIcon( 0, self.__treeIcon )
                
                for attribute in attrGroup.Attributes:
                    rootAttribute = QtGui.QTreeWidgetItem( rootAttrGroup )
                    rootAttribute.setText( 0, attribute.DisplayName )
                    rootAttribute.setData( 0, QtCore.Qt.UserRole, attribute )
                    
                    # Disable if marker already in current selection set.
                    if selectionSet.HasItem( attribute.Name ):
                        rootAttribute.setDisabled( True )
    
    def RefreshSelectionSetsDropdown( self ):
        self.__selectionSetsDropdown.clear()
        
        for selectionSet in RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
            self.__selectionSetsDropdown.addItem( selectionSet.Name )
            
    def RefreshButtons( self ):
        state = True if RS.Tools.Face.Facestar.LocalConfig.SelectionSets else False
        
        self.__renameSelectionSetButton.setEnabled( state )
        self.__deleteSelectionSetButton.setEnabled( state )
        self.__addButton.setEnabled( state )
        self.__delButton.setEnabled( state )
        self.__selectionSetsDropdown.setEnabled( state )
        self.__availableChoicesView.setEnabled( state )
        self.__selectionSetView.setEnabled( state )
            
    def __RecurseUpdateAvailableItemsView( self, treeNode ):
        selectionSet = self.GetCurrentSelectionSet()
        
        if selectionSet:
            childCount = treeNode.childCount()
                        
            for childIdx in range( childCount ):
                child = treeNode.child( childIdx )
                data = child.data( 0, QtCore.Qt.UserRole )
                
                if data:
                    child.setDisabled( selectionSet.HasItem( data.Name ) )
                        
                self.__RecurseUpdateAvailableItemsView( child )
            
    def UpdateAvailableItemsView( self ):
        numTopLevelItems = self.__availableChoicesView.topLevelItemCount()
        
        for i in range( numTopLevelItems ):
            item = self.__availableChoicesView.topLevelItem( i )
            
            self.__RecurseUpdateAvailableItemsView( item )
            
    def GetSelectedAvailableItem( self ):
        if self.__availableChoicesView.currentItem():
            return self.__availableChoicesView.currentItem().data( 0, QtCore.Qt.UserRole )
        
        return None
    
    def GetSelectedSelectionSetItem( self ):
        if self.__selectionSetView.currentItem():
            return self.__selectionSetView.currentItem().data( 0, QtCore.Qt.UserRole )
        
        return None
    
    def SetCurrentSelectionSet( self, selectionSetName ):
        itemIdx = self.__selectionSetsDropdown.findText( selectionSetName )
        self.__selectionSetsDropdown.setCurrentIndex( itemIdx )
        self.RefreshCurrentSelectionSetView()
        self.UpdateAvailableItemsView()
        
    def SaveSelectionSets( self ):
        RS.Tools.Face.Facestar.LocalConfig.Save()
        
    
    ## Event Handlers ##
    
    def RemoveSelectedFromSelectionSet( self ):
        item = self.GetSelectedSelectionSetItem()
        selectionSet = self.GetCurrentSelectionSet()
        
        if selectionSet and item:
            selectionSet.RemoveItem( item.Name )
            self.SaveSelectionSets()
            self.UpdateAvailableItemsView()
            
            parent = self.__selectionSetView.currentItem()
            parent.removeChild( self.__selectionSetView.currentItem() )
    
    def AddSelectedToSelectionSet( self ):
        item = self.GetSelectedAvailableItem()
        
        if item:
            selectionSet = self.GetCurrentSelectionSet()
            
            if selectionSet:
                if isinstance( item, RS.Core.Face.Facestar.Marker ):
                    if item.Type == RS.Core.Face.Facestar.MarkerType.OnFace:
                        selectionSet.AddOnFaceMarker( item.Name )
                        
                    else:
                        selectionSet.AddOffFaceMarker( item.Name )
                
                elif isinstance( item, RS.Core.Face.Facestar.Attribute ):
                    selectionSet.AddAttribute( item.Name )
                    
                self.RefreshCurrentSelectionSetView()
                self.SaveSelectionSets()
                self.__availableChoicesView.currentItem().setDisabled( True )
            
    def OnSelectionSetPicked( self, event ):
        self.RefreshCurrentSelectionSetView()
        self.UpdateAvailableItemsView()
        
    def OnDeleteSelectionSetPressed( self ):
        flags = QtGui.QMessageBox.StandardButton.Yes
        flags |= QtGui.QMessageBox.StandardButton.No
        
        result = QtGui.QMessageBox.question( self, 'Facestar', 'Are you sure you want to delete the selection set?', flags )
        
        if result == QtGui.QMessageBox.Yes:
            RS.Tools.Face.Facestar.LocalConfig.DeleteSelectionSet( self.GetCurrentSelectionSet().Name )
            self.RefreshSelectionSetsDropdown()
            
            if RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
                self.SetCurrentSelectionSet( RS.Tools.Face.Facestar.LocalConfig.SelectionSets[ 0 ].Name )
                
            else:
                self.RefreshAvailableChoicesView()
                
            self.SaveSelectionSets()
            self.RefreshButtons()
    
    def OnRenameSelectionSetPressed( self ):
        text, okResult = QtGui.QInputDialog.getText( self, 'Rename Selection Set', 'Name:' )
        
        if okResult:
            selectionSet = self.GetCurrentSelectionSet()
            selectionSet.SetName( text )
            
            self.RefreshSelectionSetsDropdown()
            self.SetCurrentSelectionSet( text )
            self.SaveSelectionSets()            
    
    def OnNewSelectionSetPressed( self ):
        text, okResult = QtGui.QInputDialog.getText( self, 'New Selection Set', 'Name:' )
        
        if okResult:
            RS.Tools.Face.Facestar.LocalConfig.CreateNewSelectionSet( text )
            
            if len( RS.Tools.Face.Facestar.LocalConfig.SelectionSets ) == 1:
                self.RefreshAvailableChoicesView()
                
            self.RefreshSelectionSetsDropdown()
            self.SetCurrentSelectionSet( text )
            self.SaveSelectionSets()
            self.RefreshButtons()
