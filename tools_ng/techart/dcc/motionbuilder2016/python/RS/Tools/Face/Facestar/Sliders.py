from pyfbsdk import *
from pyfbsdk_additions import *

from PySide import QtCore, QtGui

import RS.Tools.Face.Facestar
import RS.Tools.Face.Facestar.Markers
import RS.Tools.Face.Facestar.SelectionSet
#import RS.Tools.Face.Facestar.SelectionManager

reload( RS.Tools.Face.Facestar )
reload( RS.Tools.Face.Facestar.Markers )
reload( RS.Tools.Face.Facestar.SelectionSet )
#reload( RS.Tools.Face.Facestar.SelectionManager )


class AttributeSlider( QtGui.QWidget ):
    def __init__( self, definition, attribute, parent ):
        QtGui.QWidget.__init__( self, parent )
        
        self.__parent = parent
        
        self.setMinimumHeight( 20 )
        self.setMaximumHeight( 20 )
        
        self.__ctrlKeyPressed = False
        
        self.__selected = False
        
        self.__definition = definition
        self.__attribute = attribute
        
        self.__model = FBFindModelByLabelName( '{0}:{1}'.format( self.__definition.Namespace, self.__attribute.Name ) )
        
        if not self.__model:
            raise AttributeError( 'Could not find the model named "{0}:{1}"!'.format( self.__definition.Namespace, self.__attribute.Name ) )
        
        self.__modelAnimationNode = self.__model.PropertyList.Find( 'Lcl Translation' ).GetAnimationNode()
        
        self.__timerSpeed = 30
        self.__timer = QtCore.QTimer()
        self.__timer.timeout.connect( self.Update )
        self.__timer.start( self.__timerSpeed )
        
        self.CreateActions()
        

    ## Properties ##
    
    @property
    def IsSelected( self ):
        return self.__selected
    
    
    ## Methods ##
    
    def AddToSelectionSet( self ):
        selectionSet = self.sender().data()
        selectionSet.AddAttribute( self.__attribute.Name )
        RS.Tools.Face.Facestar.LocalConfig.Save()
        
    def ResetToZero( self ):
        self.SetCurrentModelValue( 0 )
    
    def Select( self ):
        self.__selected = True
        RS.Tools.Face.Facestar.SelectionManager.Select( self.__definition, self )
        self.update()
        
    def Deselect( self ):
        self.__selected = False
        RS.Tools.Face.Facestar.SelectionManager.Deselect( self.__definition, self )
        self.repaint()
        
    def GetZeroPosX( self ):
        if self.__attribute.Min != 0:
            return self.width() / 2.0
            
        return 0
        
    def GetCurrentModelValue( self ):
        modelPos = FBVector3d( self.__model.Translation.Data )
        return modelPos[ 0 ]
    
    def SetCurrentModelValue( self, value ):
        if value <= self.__attribute.Max or value >= self.__attribute.Min:
            modelPos = FBVector3d( self.__model.Translation.Data )
            modelPos[ 0 ] = value
            self.__model.Translation.Data = modelPos

    def Update( self ):
        self.update()
        
    def GetPixelPosFromCurrentValue( self ):
        w = self.width()
        currentVal = self.GetCurrentModelValue()
            
        cX = currentVal - self.__attribute.Min
        nX = cX / ( self.__attribute.Max - self.__attribute.Min )
        return int( nX * w + self.__attribute.Min )
    
    def GetValueFromPixelPos( self, posX ):
        width = self.width()
                    
        pX = posX
        nX = pX / ( float( width ) )
        return nX * ( float( self.__attribute.Max ) - float( self.__attribute.Min ) ) + self.__attribute.Min
    
    def HasKeysAtCurrentTime( self ):
        '''
        Return whether or not the model object for this marker has a keyframe at the current time.
        '''
        return self.__modelAnimationNode.IsKey()
    
    def NewSelectionSet( self ):
        dialog = RS.Tools.Face.Facestar.SelectionSet.ManagerDialog( self, self.__definition )
        dialog.show()
    
    def CreateActions( self ):
        self.__actionNewSelectionSet = QtGui.QAction( 'New', self, triggered = self.NewSelectionSet )
        self.__actionResetToZero = QtGui.QAction( 'Reset to Zero', self, triggered = self.ResetToZero )
        #self.__actionAddToSelectionSet = QtGui.QAction( 'Add to Selection Set', self, triggered = self.AddToSelectionSet )
    
    
    ## Overrides ##
    
    def contextMenuEvent( self, event ):
        menu = QtGui.QMenu( self )
        menu.addAction( self.__actionResetToZero )
        
        selectionSetsMenu = QtGui.QMenu( 'Add to Selection Set', self )
        selectionSetsMenu.addAction( self.__actionNewSelectionSet )
        
        if RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
            selectionSetsMenu.addSeparator()
            
            for selectionSet in RS.Tools.Face.Facestar.LocalConfig.SelectionSets:
                if selectionSet.Attributes:
                    actionSelectionSet = QtGui.QAction( selectionSet.Name, self, triggered = self.AddToSelectionSet )
                    actionSelectionSet.setData( selectionSet )
                    selectionSetsMenu.addAction( actionSelectionSet )
        
        menu.addMenu( selectionSetsMenu )
        
        menu.exec_( event.globalPos() )
    
    '''
    def keyPressEvent( self, event ):
        if event.key() == QtCore.Qt.Key.Key_Control:
            #self.__ctrlKeyPressed = True
            RS.Tools.Face.Facestar.SelectionManager.ControlPressed = True
    
    def keyReleaseEvent( self, event ):
        #self.__ctrlKeyPressed = False
        RS.Tools.Face.Facestar.SelectionManager.ControlPressed = False
    '''
    
    def mousePressEvent( self, event ):
        self.__parent.setFocus()
        
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            if not RS.Tools.Face.Facestar.ControlPressed:
                RS.Tools.Face.Facestar.DeselectAttributes( self.__definition )
                
            
            selectedItems = RS.Tools.Face.Facestar.SelectionManager.GetSelected( self.__definition )
            
            if not self in selectedItems:
                self.Select()
                
            self.__timer.stop()
            
            value = self.GetValueFromPixelPos( event.pos().x() )
            self.SetCurrentModelValue( value )
            self.update()
        
        QtGui.QWidget.mousePressEvent( self, event )
    
    def mouseMoveEvent( self, event ):
        if self.IsSelected:
            x = event.pos().x()
            
            # Translate current pixel position under mouse into a frame number.
            value = self.GetValueFromPixelPos( event.pos().x() )
            self.SetCurrentModelValue( value )
            self.update()
            
        QtGui.QWidget.mouseMoveEvent( self, event )
        
    def mouseReleaseEvent( self, event ):
        self.__timer.start( self.__timerSpeed ) 
        QtGui.QWidget.mouseReleaseEvent( self, event )     
    
    def paintEvent( self, event ):
        painter = QtGui.QPainter()
        painter.begin( self )
        
        normalGradient = QtGui.QLinearGradient( QtCore.QPointF( 0, 0 ), QtCore.QPointF( 0, self.height() ) )
        normalGradient.setColorAt( 0, QtGui.QColor( 128, 128, 128 ) )
        normalGradient.setColorAt( 1, QtGui.QColor( 90, 90, 90 ) )
        
        # Fill slider background.
        painter.setPen( QtGui.QColor( 60, 60, 60 ) )
        
        if self.IsSelected:
            selectedGradient = QtGui.QLinearGradient( QtCore.QPointF( 0, 0 ), QtCore.QPointF( 0, self.height() ) )
            selectedGradient.setColorAt( 0, QtGui.QColor( 102, 139, 175 ) )
            selectedGradient.setColorAt( 1, QtGui.QColor( 90, 117, 143 ) )
            painter.setBrush( selectedGradient )
            
        else:
            painter.setBrush( QtGui.QColor( 70, 70, 70 ) )
            
        
        painter.drawRect( 0, 0, self.width(), self.height() )
        
        x = self.GetPixelPosFromCurrentValue()
        defaultX = self.GetZeroPosX()
        
        if defaultX != 0:
            if x < defaultX:
                valueRect = QtCore.QRect( x, 0, ( defaultX - x ), self.height() )
                
            else:
                valueRect = QtCore.QRect( defaultX, 0, ( x - defaultX ), self.height() )
                
        else:
            valueRect = QtCore.QRect( 0, 0, x, self.height() )
        if self.__modelAnimationNode != None:
            if self.HasKeysAtCurrentTime():
                keyGradient = QtGui.QLinearGradient( QtCore.QPointF( 0, 0 ), QtCore.QPointF( 0, self.height() ),  )
                keyGradient.setColorAt( 0, QtGui.QColor( 255, 0, 0 ) )
                keyGradient.setColorAt( 1, QtGui.QColor( 160, 0, 0 ) )
                
                painter.setBrush( keyGradient )
                
            else:
                painter.setBrush( normalGradient )
        else:
            painter.setBrush( normalGradient )
            
        # Draw value.
        painter.setPen( QtGui.QColor( 60, 60, 60 ) )
        painter.drawRect( valueRect )
        
        painter.end()
        
        

class TimeSlider( QtGui.QWidget ):
    def __init__( self ):
        QtGui.QWidget.__init__( self )
        
        self.setMinimumHeight( 30 )
        self.setMaximumHeight( 30 )
        self.setFocusPolicy( QtCore.Qt.NoFocus )
        
        self.__timerSpeed = 30
        self.__timer = QtCore.QTimer()
        self.__timer.timeout.connect( self.Update )
        self.__timer.start( self.__timerSpeed )                
        
        self.__system = FBSystem()
        self.__playerControl = FBPlayerControl()
        
        self.__startFrame = self.__playerControl.LoopStart.GetFrame()
        self.__stopFrame = self.__playerControl.LoopStop.GetFrame()
        self.__currentFrame = self.__system.LocalTime.GetFrame()
        
        self.__sliderPosX = 0.0
        self.__sliderMarkerWidth = 22
        
        self.__sliderRect = None
        self.__sliderSelected = False
        
        # Every n frames, draw a major tick.
        self.__majorTickFrameResolution = 150
        self.__majorTickPixelResolution = 0
        
        self.__startTickPosX = self.__sliderMarkerWidth
        self.__stopTickPosX = self.__sliderMarkerWidth
        
        self.__font = QtGui.QFont( 'Helvetica', 8 )
        self.__fontMetrics = QtGui.QFontMetrics( self.__font )
        
        self.Update()
        
    
    ## Methods ##
        
    def Update( self ):
        self.__startFrame = self.__playerControl.LoopStart.GetFrame()
        self.__stopFrame = self.__playerControl.LoopStop.GetFrame()      
        self.__currentFrame = self.__system.LocalTime.GetFrame()
        widgetWidth = self.geometry().width()
        
        pX = float( self.__majorTickFrameResolution ) / ( float( self.__stopFrame ) - float( self.__startFrame ) )  
        self.__majorTickPixelResolution = pX * ( ( widgetWidth - self.__stopTickPosX ) - self.__startTickPosX ) + self.__startTickPosX 
                        
        self.__sliderPosX = self.GetPixelPosFromCurrentFrame()
         
        self.update()
        
    def GetPixelPosFromCurrentFrame( self ):
        widgetWidth = self.geometry().width()
        
        nX = float( self.__currentFrame ) / ( float( self.__stopFrame ) - float( self.__startFrame ) )  
        return nX * ( ( widgetWidth - self.__stopTickPosX ) - self.__startTickPosX ) + self.__startTickPosX        
        
    def GetFrameFromPixelPos( self, posX ):
        widgetWidth = self.geometry().width()
                    
        pX = posX - self.__startTickPosX
        nX = pX / ( ( float( widgetWidth ) - float( self.__stopTickPosX ) ) - self.__startTickPosX )
        frame = int( nX * ( float( self.__stopFrame ) - float( self.__startFrame ) ) )
        
        if frame > self.__stopFrame:
            frame = self.__stopFrame
            
        elif frame < self.__startFrame:
            frame = self.__startFrame
            
        return frame
    
    def GoToFrame( self, frame ):
        currentTime = FBTime( 0 )
        currentTime.SetFrame( frame )
                    
        self.__playerControl.Goto( currentTime )
        
    
    ## Overrides ##
    
    def mousePressEvent( self, event ):
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            
            # Perform hittest on our slider to determine if the user selected it.
            if self.__sliderRect.contains( event.pos() ):
                self.__sliderSelected = True
                self.__timer.stop()
                
            else:
                self.__sliderSelected = False
                
                frame = self.GetFrameFromPixelPos( event.pos().x() )
                self.GoToFrame( frame )              
        
        QtGui.QWidget.mousePressEvent( self, event )
    
    def mouseMoveEvent( self, event ):
        if self.__sliderSelected:
            x = event.pos().x()
            
            # Restrict movement within our start and end tick range.
            if x < self.__startTickPosX:
                x = self.__startTickPosX
                
            elif x > ( self.geometry().width() - self.__stopTickPosX ):
                x = self.geometry().width() - self.__stopTickPosX
            
            # Translate current pixel position under mouse into a frame number.
            frame = self.GetFrameFromPixelPos( x )
            self.GoToFrame( frame )
            self.Update()
            
        QtGui.QWidget.mouseMoveEvent( self, event )
        
    def mouseReleaseEvent( self, event ):
        self.__sliderSelected = False
        self.__timer.start( self.__timerSpeed ) 
        QtGui.QWidget.mouseReleaseEvent( self, event )    
        
    def paintEvent( self, event ):
        rect = self.geometry()
        self.__sliderRect = QtCore.QRect( self.__sliderPosX - self.__sliderMarkerWidth, rect.height() / 2.0 - 6, self.__sliderMarkerWidth * 2.0, rect.height() - 9 )
        
        painter = QtGui.QPainter()
        painter.begin( self )
        
        painter.setBrush( QtGui.QColor( 128, 128, 128 ) )
        painter.drawRect( 0, 0, rect.width(), rect.height() )
        
        # Draw slider marker.
        painter.setBrush( QtGui.QColor( 30, 30, 30, 100 ) )
        painter.setPen( QtGui.QColor( 30, 30, 30, 50 ) )
        painter.drawRect( self.__sliderRect )
        
        # Make it look embossed like the real one.  This is just fluff because I'm being fancy pants.
        painter.setPen( QtGui.QColor( 255, 255, 255, 120 ) )
        painter.drawLine( self.__sliderRect.bottomLeft(), self.__sliderRect.topLeft() )
        painter.drawLine( self.__sliderRect.topLeft(), self.__sliderRect.topRight() )
        
        painter.setPen( QtGui.QColor( 20, 20, 20, 100 ) )
        painter.drawLine( self.__sliderRect.bottomLeft(), self.__sliderRect.bottomRight() )
        painter.drawLine( self.__sliderRect.bottomRight(), self.__sliderRect.topRight() )        
        
        # Draw ticks.
        painter.setPen( QtGui.QColor( 70, 70, 70 ) )
        painter.drawLine( self.__startTickPosX , 0, self.__startTickPosX , rect.height() - 4 )
        painter.drawLine( rect.width() - self.__stopTickPosX , 0, rect.width() - self.__stopTickPosX , rect.height() - 4 )
        
        '''
        currentFrame = self.__currentFrame
        currentMajorTickPosX = self.__startTickPosX
        
        while currentMajorTickPosX < rect.width():
            painter.drawLine( currentMajorTickPosX , 0, currentMajorTickPosX , rect.height() - 6 )
            currentMajorTickPosX += self.__majorTickPixelResolution
        '''
        
        # Draw frame numbers.
        painter.setPen( QtGui.QColor( 0, 0, 0 ) )
        
        painter.setFont( self.__font )
        painter.drawText( self.__startTickPosX + 2, rect.height() - 4, str( self.__startFrame ) )       
        painter.drawText( ( rect.width() - self.__stopTickPosX ) - self.__fontMetrics.width( str( self.__stopFrame ) ) - 2, rect.height() - 4, str( self.__stopFrame ) )
        
        # Draw green line representing current frame on slider.
        painter.setPen( QtGui.QColor( 0, 255, 0 ) )
        painter.drawLine( self.__sliderPosX , 0, self.__sliderPosX , rect.height() )        
        
        painter.end() 


class OffFaceSlider( QtGui.QGraphicsItem ):
    '''
    Represents a slider object for an off-face control.
    
    Arguments:
    
        marker: A RS.Core.Face.Facestar.Marker object.
    '''
    def __init__( self, marker, definition, viewport, *args, **kwargs ):
        QtGui.QGraphicsItem.__init__( self, *args, **kwargs )
        
        self.setAcceptHoverEvents( True )
        
        self.__marker = marker
        self.__definition = definition
        self.__viewport = viewport
        
        self.__modelName = '{0}:{1}'.format( definition.Namespace, marker.Name )
        self.__model = FBFindModelByLabelName( self.__modelName )
        
        self.__mouseButtonPressed = None
        
        # Widget sizes
        self.__width = 20
        self.__height = marker.ArrowHeight
        
        self.__arrowHeadHeight = 14
        self.__arrowHeadWidth = 4
        
        # Min / Max values for the slider.
        self.__min = QtCore.QPointF( marker.Min[ 0 ], marker.Min[ 1 ] )
        self.__max = QtCore.QPointF( marker.Max[ 0 ], marker.Max[ 1 ] )
        
        # Build constraint rect.
        x = self.__min.x() * marker.ConstraintWidth
        y = self.__min.y() * self.__height
        
        if self.__min.x() == 0 and self.__max.x() == 0:
            w = 0
        
        else:
            w = self.__max.x() * marker.ConstraintWidth if self.__min.x() > 0.0 else marker.ConstraintWidth
            
        if self.__min.y() == 0 and self.__max.y() == 0:
            h = 0
            
        else:
            h = self.__max.y() * self.__height
        
        # Marker constraint rect.
        self.__constraintRect = QtCore.QRectF( x, y, 1, 1 )
        self.__constraintRect.setBottomRight( QtCore.QPointF( w, h ) )
        
        # Setup the bounding rect.  This is used for selection/redraw purposes.
        self.__boundingRect = self.__constraintRect
        
        # If the constraint rect width is 0, we still need a bounding rect that does have some width, so build one.
        if w == 0:
            self.__boundingRect = QtCore.QRectF( x - 12.5, y, 1, 1 )
            self.__boundingRect.setBottomRight( QtCore.QPointF( 12.5, h ) )
        
        # Slider marker.
        self.__markerWidget = RS.Tools.Face.Facestar.Markers.OffFace( self.__definition, self, 8, self.__modelName )
        self.__markerWidget.setToolTip( marker.DisplayName )

        # Set initial transform.
        self.setPos( marker.Position[ 0 ], marker.Position[ 1 ] )
        self.rotate( marker.Rotation )
        
        self.__angle = marker.Rotation


    ## Properties ##
    
    @property
    def Angle( self ):
        return self.__angle
        
    @property
    def Model( self ):
        return self.__model
        
    @property
    def ModelName( self ):
        return self.__modelName
        
    @property
    def ConstraintRect( self ):
        return self.__constraintRect
        
    @property
    def Height( self ):
        return self.__height
        
    @property
    def Min( self ):
        return self.__min
        
    @property
    def Max( self ):
        return self.__max
    
    @property
    def MarkerObject( self ):
        return self.__marker
        
    @property
    def Marker( self ):
        return self.__markerWidget
    
    @property
    def Viewport( self ):
        return self.__viewport
    
    ## Methods ##
    
    def Deselect( self ):
        pass
        
        
    ## Overrides ##
    
    def mousePressEvent( self, event ):
        self.__mouseButtonPressed = event.button()
        
        QtGui.QGraphicsItem.mousePressEvent( self, event )
    
    def mouseMoveEvent( self, event ):
        if self.__mouseButtonPressed == QtCore.Qt.MouseButton.RightButton:
            delta = event.lastPos() - event.pos()
            self.__angle += delta.x()
            
            if self.__angle > 360:
                self.__angle = 0
            
            self.rotate( delta.x() )
            
        QtGui.QGraphicsItem.mouseMoveEvent( self, event )
        
    def mouseReleaseEvent( self, event ):
        self.__mouseButtonPressed = None
        QtGui.QGraphicsItem.mouseReleaseEvent( self, event )
    
    def boundingRect( self ):
        return self.__boundingRect

    def paint( self, painter, option, widget ):
        if not RS.Tools.Face.Facestar.MODE_EDIT_MARKERS:
            if self.__markerWidget.isSelected() or self.isSelected():
                brush = QtGui.QBrush( QtGui.QColor( 0, 0, 0, 25 ) )
                pen = QtGui.QPen( QtGui.QColor( 0, 0, 0, 100 ) )
                painter.setPen( pen )
                painter.setBrush( brush )
                painter.drawRect( self.__constraintRect )
                
        else:
            if self.isSelected():
                brush = QtGui.QBrush( QtGui.QColor( 0, 255, 0, 25 ) )
                pen = QtGui.QPen( QtGui.QColor( 0, 255, 0 ) )
                
            else:
                brush = QtGui.QBrush( QtGui.QColor( 0, 0, 255, 25 ) )
                pen = QtGui.QPen( QtGui.QColor( 0, 0, 255, 100 ) )
                
            painter.setPen( pen )
            painter.setBrush( brush )
            painter.drawRect( self.__constraintRect )
            
            if self.isSelected():
                brush = QtGui.QBrush( QtGui.QColor( 0, 255, 0 ) )
                
            else:
                brush = QtGui.QBrush( QtGui.QColor( 0, 0, 255 ) )
                
            painter.setBrush( brush )
            painter.drawEllipse( self.__constraintRect.center(), 2, 2 )