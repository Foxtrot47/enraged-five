

__DefinitionViews = {}

def GetView( defintion ):
    global __DefinitionViews
    
    if defintion.Namespace in __DefinitionViews:
        return __DefinitionViews[ defintion.Namespace ]
    
    return None

def RegisterView( definition, view ):
    global __DefinitionViews
    
    __DefinitionViews[ definition.Namespace ] = view