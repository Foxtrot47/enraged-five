import inspect
import json
import math
import os
import tempfile

import pyfbsdk as mobu
from RS import Globals

from RS.Utils import Scene
from RS.Utils import Path
from RS.Utils.Scene import RelationshipConstraintManager as relationConstraintManager
from RS.Core.AssetSetup.Sliders import addAssetSliders
from RS.Core.AssetSetup.Sliders import utils as sliderUtils
from RS.Utils import Namespace
from RS.Utils.Scene import Constraint
from RS.Tools.IkSpline import Utils as ikUtils


reload(ikUtils)


def GetMainGroup():
    fileName = Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName)

    for group in Globals.Groups:
        if group.Name == fileName:
            return group

    group = mobu.FBGroup(fileName)

    return group


class ChainRange(object):
    """
        Class used to compute hand interpolation
    """
    NODE_ATTRIBUTES = ('startTimeValue',
                       'endTimeValue',
                       'timeRange',
                       'splitRange',
                       'splitCount',
                       'startSegmentRatio',
                       'endSegmentRatio',
                       'keyframeValues')
    
    def __init__(self):
        self.startTimeValue = 0.0

        self.endTimeValue = 0.0

        self.timeRange = 0.0

        self.splitRange = 0.0

        self.splitCount = 0

        self.startSegmentRatio = 0.0

        self.endSegmentRatio = 0.0

        self.keyframeValues = []

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)
        reportData += '\n\t{0}'.format('<Components:>')

        for attribute in self.NODE_ATTRIBUTES:
            currentAttribute = getattr(self, attribute)

            reportData += '\n\t\t{0}:{1}'.format(attribute,
                                                 currentAttribute)
        return reportData

    def getAnimationRange(self,
                          startTimeValue,
                          endTimeValue,
                          splitCount):
        self.timeRange = (endTimeValue - startTimeValue)
        self.splitRange = self.timeRange / float(splitCount)

        self.startTimeValue = startTimeValue
        self.endTimeValue = endTimeValue
        self.splitCount = int(splitCount)

    def extractChainPropagation(self,
                                startTimeValue,
                                endTimeValue,
                                splitCount,
                                blendWeight):
        self.getAnimationRange(startTimeValue,
                               endTimeValue,
                               splitCount)
        self.endSegmentRatio = self.timeRange-((self.timeRange-self.splitRange)*blendWeight)

        self.startSegmentRatio = (self.timeRange-self.endSegmentRatio)/float(splitCount-1)

        keyframeValues = []

        for jointIndex in xrange(splitCount):
            startFrame = jointIndex*self.startSegmentRatio
            endFrame = self.endSegmentRatio+startFrame

            startFrameValue = self.startTimeValue + startFrame
            endFrameValue = self.endTimeValue + endFrame

            self.keyframeValues.append((startFrameValue/200.0, 
                                        endFrameValue/200.0))


class SuspenderWeights(object):
    def __init__(self,
                 tangentPivot,
                 handlePoint,
                 startPoint,
                 endPoint):
        self.tangentPivot = tangentPivot

        self.handlePoint = handlePoint

        self.startPoint = startPoint

        self.endPoint = endPoint

        self.height = 0.0

        self.startElevation = 0.0

        self.endElevation = 0.0

        self.lineWeights = [0.0, 0.0]

        self.solveTriangle()

    def solveTriangle(self):
        self.height = (self.handlePoint-self.tangentPivot).Length()

        lineLength = (self.endPoint-self.startPoint).Length()

        segmentLength = (self.tangentPivot-self.startPoint).Length()

        self.lineWeights[1] = (segmentLength/lineLength)
        self.lineWeights[0] = 1.0 - self.lineWeights[1]


class ChainComponent(object):
    HORIZONTAL_OFFSET = 500

    VERTICAL_OFFSET = 100

    SHIFT_COMPONENT_OFFSET = 600

    SCALE_PREFIX = '{}_Scale1'

    OUTPUT_PREFIX = '{}_Output1'

    INTERPOLATE_PREFIX = '{}_Blend1'

    WEIGHT1_PREFIX = '{}_WeightSourceA'

    WEIGHT2_PREFIX = '{}_WeightSourceB'

    WEAPON_PROPERTY = 'Body_Weapon_DOF_{0:02d}'

    def __init__(self,
                 increment,
                 relationConstraint,
                 inputStrapData,
                 strengthSlider,
                 strapPointIndex):
        self.node = None

        self.interpolateNode = None

        self.outputNode = None

        self.scaleNode2 = None

        self.increment = increment

        self.relationConstraint = relationConstraint

        self.strapAnchor = inputStrapData
        
        self.constraintPrefix = inputStrapData.name

        self.strengthSlider = strengthSlider

        self.strapPointIndex = strapPointIndex

    def createInputSource(self,
                          targetAttribute,
                          targetAttribute2,
                          offsetPosition,
                          targetName,
                          inputConnection1,
                          inputConnection2):
        sender1 = self.relationConstraint.AddSenderBox(self.strapAnchor.blendDriver)

        self.relationConstraint.ConnectBoxes(sender1, 
                                             inputConnection1,
                                             self.interpolateNode, 
                                             targetAttribute)

        self.relationConstraint.ConnectBoxes(sender1, 
                                             inputConnection2,
                                             self.interpolateNode, 
                                             targetAttribute2)

        sender1.SetBoxPosition(-600, 
                               offsetPosition[1])

    def layoutBlendComponents(self):
        self.interpolateNode = self.relationConstraint.AddFunctionBox("Rotation",
                                                                      "Interpolate")

        self.interpolateNode.SetBoxPosition(self.HORIZONTAL_OFFSET, 
                                            self.increment*\
                                            self.SHIFT_COMPONENT_OFFSET)

        self.interpolateNode.GetBox().Name = self.INTERPOLATE_PREFIX.format(self.constraintPrefix)

    def prepareInputComponents(self):
        self.createInputSource('Ra',
                               'Rb',
                               [0, 
                                self.increment*\
                                self.SHIFT_COMPONENT_OFFSET+\
                                int(self.VERTICAL_OFFSET*-1.5)],
                                self.WEIGHT1_PREFIX.format(self.constraintPrefix),
                                'sourceA_Setting1',
                                'sourceA_Setting2')

    def findAnimationNode(self, inputComponent, attributeName):
        for node in inputComponent.Nodes:
            if node.Name == attributeName:
                return node

        return None

    def setupOutputComponents(self):
        self.scaleNode = self.relationConstraint.AddFunctionBox("Vector",
                                                                "Scale (a x V)")

        inPlugs = self.findAnimationNode(self.scaleNode.GetBox().AnimationNodeInGet(),
                                         'Number')

        inPlugs.WriteData([100.0])

        self.scaleNode.SetBoxPosition(self.HORIZONTAL_OFFSET*2, 
                                      self.increment*\
                                      self.SHIFT_COMPONENT_OFFSET)

        self.outputNode = self.relationConstraint.AddFunctionBox("Converters",
                                                                 "Vector To Number")

        self.outputNode.SetBoxPosition(self.HORIZONTAL_OFFSET*3, 
                                       self.increment*\
                                       self.SHIFT_COMPONENT_OFFSET)

        self.relationConstraint.ConnectBoxes(self.interpolateNode, 
                                             'Result', 
                                             self.scaleNode, 
                                             'Vector')

        self.relationConstraint.ConnectBoxes(self.scaleNode, 
                                             'Result', 
                                             self.outputNode, 
                                             'V')

        self.scaleNode.GetBox().Name = self.SCALE_PREFIX.format(self.constraintPrefix)

        self.outputNode.GetBox().Name = self.OUTPUT_PREFIX.format(self.constraintPrefix)

    def connectIoBlocks(self):
        receiver = self.relationConstraint.AddReceiverBox(self.strapAnchor.targetConstraint)
        self.strapAnchor.ikAnchor.weightProperty.SetAnimated(True)
        self.strapAnchor.baseAnchor.weightProperty.SetAnimated(True)

        self.relationConstraint.ConnectBoxes(self.outputNode, 
                                             'X', 
                                             receiver, 
                                             self.strapAnchor.ikAnchor.weightProperty.Name)

        self.relationConstraint.ConnectBoxes(self.outputNode, 
                                             'Y', 
                                             receiver, 
                                             self.strapAnchor.baseAnchor.weightProperty.Name)

        receiver.SetBoxPosition(self.HORIZONTAL_OFFSET*4, 
                                self.increment*\
                                self.SHIFT_COMPONENT_OFFSET)

    def connectWeightDriver(self):
        sender2 = self.relationConstraint.AddSenderBox(self.strengthSlider)
        propertyName = self.WEAPON_PROPERTY.format(self.strapPointIndex)
        senderProperty = self.strengthSlider.PropertyList.Find(propertyName)
        senderProperty.SetAnimated(True)

        self.relationConstraint.ConnectBoxes(sender2, 
                                             propertyName, 
                                             self.interpolateNode, 
                                             'c')

        offsetPosition = [0, int(self.increment*self.SHIFT_COMPONENT_OFFSET+self.VERTICAL_OFFSET*-0.5)]

        sender2.SetBoxPosition(offsetPosition[0], 
                               offsetPosition[1])

    def buildConstraint(self):
        self.layoutBlendComponents()

        self.prepareInputComponents()

        self.setupOutputComponents()

        self.connectIoBlocks()

        self.connectWeightDriver()


class ChainBlender(object):
    DUMMY_NAME = 'Dummy01'

    STRAP_GROUP_NAME = 'strapInterpolationDrivers1'

    PROPS_FLAG = 'rs_Dummy'

    ASSET_FLAG = 'rs_Type'

    COMPONENT_FLAG = 'Asset_Type'

    PROPS_VALUE = 'Props'

    WEIGHT_DRIVER_NAME = 'ConstraintStrength'

    DISTRIBUTION_DRIVER_NAME = 'ConstraintDistribution'

    BACK_DISTRIBUTION_DRIVER_NAME = 'ConstraintDistributionBack'

    STRAP_SLIDER_NAMES = ('ConstraintStrength',
                          'ConstraintDistribution',
                          'ClothPinning')

    STRAP_SLIDER_GROUP_NAME = 'Sliders'

    CHAINBLENDER_METADATA_ROOT_NAME = 'chainBlenderMetaRoot'

    CHAINBLENDER_METADATA_VALUE = 'chainBlenderRoot'

    STRAP_RIG_GROUPSET_NAMES = ('dummy',
                                'ikSplineRoot',
                                'strapRelationConstraint',
                                'strapConstraintFolder',
                                'chainComponents',
                                'baseRoot',
                                'baseRootConstraint',
                                'serialConstraint',
                                'ConstraintDistribution',
                                'handleConstraints',
                                'weaponConstraints',
                                'targetCharacter',
                                'targetWeapon',
                                'targetCharacterSlider')

    CONSTRAINT_FOLDER_NAME = 'Constraints 1'

    def __init__(self):
        self.relationConstraint = None

        self.constraintPrefix = 'strap1'

        self.increment = 0

        self.strapPointArray = []

        self.chainDriverGroup = None

        self.strengthSlider = None

        self.dummy = None

        self.rootProperty = None

        self.constraintFolder = None

        self.useAlternatePoint = False

        self.strenghtSlider = None

        self.serialUtils = SerialChainDriver()

    def createMetaRoot(self):
        if self.chainDriverGroup is None:
            return

        self.rootProperty = self.chainDriverGroup.PropertyList.Find(self.CHAINBLENDER_METADATA_ROOT_NAME)
        if self.rootProperty is None:
            self.rootProperty = self.chainDriverGroup.PropertyCreate(self.CHAINBLENDER_METADATA_ROOT_NAME,
                                                                     mobu.FBPropertyType.kFBPT_charptr, 
                                                                     'String', 
                                                                     False,
                                                                     True,
                                                                     None)

        self.rootProperty.Data = 'chainBlenderRoot'

        inputAttribute = self.chainDriverGroup.PropertyList.Find('useAlternatePoint')
        if inputAttribute is None:
            inputAttribute = self.chainDriverGroup.PropertyCreate('useAlternatePoint', 
                                                             mobu.FBPropertyType.kFBPT_bool, 
                                                             'Bool', 
                                                             True, 
                                                             True, 
                                                             None)
        inputAttribute.SetAnimated(True)
        inputAttribute.Data = self.useAlternatePoint 

        for objectSet in self.STRAP_RIG_GROUPSET_NAMES:
            objectSetProperty = self.chainDriverGroup.PropertyList.Find(objectSet)

            if objectSetProperty is not None:
                continue
    
            self.chainDriverGroup.PropertyCreate(objectSet, 
                                                 mobu.FBPropertyType.kFBPT_object, 
                                                 'Object', 
                                                 False, 
                                                 True, 
                                                 None )

    def connectMetaData(self,
                        inputObject,
                        objectSetName):
        metaRootProperty = self.chainDriverGroup.PropertyList.Find(objectSetName)

        if metaRootProperty is None:
            return

        inputObject.ConnectDst(metaRootProperty)

    def getPropsNamespace(self,
                          inputDummy):
        currentNamespace = Namespace.GetNamespace(inputDummy)

        if len(currentNamespace)==0:
            return None

        return currentNamespace

    def getPropsRootNode(self,
                         inputDummy=None):
        if inputDummy is None:
            sceneModels = [component for component in Globals.Scene.Components if isinstance(component, mobu.FBModelNull)]
            dummyArray = []
            for node in sceneModels:
                rsTypeProperty = node.PropertyList.Find(self.ASSET_FLAG)
                if rsTypeProperty is None:
                    continue

                if rsTypeProperty.Data != self.PROPS_FLAG:
                    continue

                rsAssetProperty = node.PropertyList.Find(self.COMPONENT_FLAG)
                if rsTypeProperty is None:
                    continue

                if rsAssetProperty.Data != self.PROPS_VALUE:
                    continue

                dummyArray.append(node)

            return dummyArray

        else:
            dummyNodeList = mobu.FBComponentList()
            mobu.FBFindObjectsByName(inputDummy,
                                     dummyNodeList,
                                     True,
                                     True)

            return dummyNodeList[0]

    def findConstraints(self, control):
        """
            Find all constraints driving the list of control provided.

            Args:  
                rigControls (list of FBModel): input parent node.

            returns (list of FBConstraint).
        """
        drivingConstrains = []
        sourceCount = control.GetSrcCount()

        for sourceIndex in range(sourceCount):
            sourceObject = control.GetSrc(sourceIndex)

            if isinstance(sourceObject, mobu.FBConstraintRelation) is True:
                drivingConstrains.append(sourceObject)

        sceneConstraints = [constraint for constraint in Globals.Scene.Constraints]


        constrainedGroupIndex = 0
        for constraint in sceneConstraints:
            drivenCount = constraint.ReferenceGetCount(constrainedGroupIndex)
            
            for drivenIndex in range(drivenCount):
                drivenObject = constraint.ReferenceGet(constrainedGroupIndex, drivenIndex)

                if drivenObject is None:
                    continue

                if drivenObject.FullName == control.FullName:
                    drivingConstrains.append(constraint)

                    break

        drivingConstrains = list(set(drivingConstrains))

        return drivingConstrains 

    def collectSlider(self):
        propsNameSpace = self.getPropsNamespace(self.dummy)

        sliderNodeList = mobu.FBComponentList()
        if propsNameSpace is None:
            sampleName = str(self.WEIGHT_DRIVER_NAME)
            mobu.FBFindObjectsByName(sampleName,
                                     sliderNodeList,
                                     False,
                                     True)

        else:
            sampleName = '{0}:{1}'.format(propsNameSpace,
                                          self.WEIGHT_DRIVER_NAME)


            mobu.FBFindObjectsByName(sampleName,
                                     sliderNodeList,
                                     True,
                                     True)

        self.strengthSlider = sliderNodeList[0]

    def collectStrenghtSliderFromNamespace(self):
        propsNameSpace = self.getPropsNamespace(self.dummy)

        sliderNodeList = mobu.FBComponentList()
        if propsNameSpace is None:
            sampleName = str(self.DISTRIBUTION_DRIVER_NAME)
            mobu.FBFindObjectsByName(sampleName,
                                     sliderNodeList,
                                     False,
                                     True)

        else:
            sampleName = '{0}:{1}'.format(propsNameSpace,
                                          self.DISTRIBUTION_DRIVER_NAME)


            mobu.FBFindObjectsByName(sampleName,
                                     sliderNodeList,
                                     True,
                                     True)

        if sliderNodeList:
            self.strengthSlider = sliderNodeList[0]
        else:
            self.strengthSlider = None

    def collectStrenghtSlider(self):
        propsNameSpace = self.getPropsNamespace(self.dummy)

        sliderNodeList = mobu.FBComponentList()
        if propsNameSpace is None:
            sampleName = str('strengthSlider1')
            mobu.FBFindObjectsByName(sampleName,
                                     sliderNodeList,
                                     False,
                                     True)

        else:
            sampleName = '{0}:{1}'.format(propsNameSpace,
                                          'strengthSlider1')


            mobu.FBFindObjectsByName(sampleName,
                                     sliderNodeList,
                                     True,
                                     True)


        sliderNodeList = list(sliderNodeList)
        if not sliderNodeList:
            return

        self.strengthSlider = sliderNodeList[0]

        self.serialUtils.constraintDistribution = self.strengthSlider

    def collectStrapSliders(self):
        propsNameSpace = self.getPropsNamespace(self.dummy)

        for assetIndex, assetName in enumerate(self.STRAP_SLIDER_NAMES):
            componentUtils = sliderUtils.Slider()
            componentUtils.collectFromScene(assetName,
                                            inputNamespace=propsNameSpace)

            if componentUtils.animationControl is None:
                continue

            limitConstraint = self.findConstraints(componentUtils.animationControl)

            if len(limitConstraint)==0:
                continue

            for constraint in limitConstraint:
                self.constraintFolder.ConnectSrc(constraint)

                self.connectMetaData(constraint,
                                     'strapConstraintFolder')

    def createWeightNode(self,
                         splineIkRigRoot,
                         constraintPrefix='',
                         inputJointArray=[],
                         buildSliders=False):
        if len(constraintPrefix)==0:
            constraintPrefix = str(self.constraintPrefix)
        else:
            self.constraintPrefix = str(constraintPrefix)

        # create relation constraint
        relationConstraintName = '{}_Relation'.format(constraintPrefix)
        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)
        self.relationConstraint.SetActive(True)

        self.dummy = self.getPropsRootNode()[0]

        self.collectStrenghtSlider()

        self.chainDriverGroup = mobu.FBModelNull(self.STRAP_GROUP_NAME)

        self.createMetaRoot()

        self.connectMetaData(self.dummy,
                             'dummy')

        self.connectMetaData(self.relationConstraint.GetConstraint(),
                             'strapRelationConstraint')

        self.constraintFolder = None
        for folder in Globals.Folders:
            if folder.Name == 'Constraints 1':
                self.constraintFolder = folder

        if self.constraintFolder is None:
            self.constraintFolder = mobu.FBFolder(self.CONSTRAINT_FOLDER_NAME,
                                                  self.relationConstraint.GetConstraint())
        else:
            self.constraintFolder.ConnectSrc(self.relationConstraint.GetConstraint())

        self.connectMetaData(self.constraintFolder,
                             'strapConstraintFolder')

        if buildSliders is True:
            addAssetSliders.Build(self.STRAP_SLIDER_NAMES,
                                  self.STRAP_SLIDER_GROUP_NAME)

        self.connectMetaData(splineIkRigRoot,
                             'ikSplineRoot')

        self.collectStrapSliders()
        
        ikSplineConstraintFolder = self.getIkSplineConstraintFolder(splineIkRigRoot)

        if ikSplineConstraintFolder is not None:
            self.constraintFolder.ConnectSrc(ikSplineConstraintFolder)

        rigGroup = GetMainGroup()

        for group in Globals.Groups:
            if group.Name == self.STRAP_SLIDER_GROUP_NAME:
                rigGroup.ConnectSrc(group)

        self.serialUtils.build(self.chainDriverGroup,
                               inputJointArray,
                               blendWeight=1.0)

        self.connectMetaData(self.serialUtils.relationConstraint.GetConstraint(),
                             'serialConstraint')
 
        self.connectMetaData(self.serialUtils.constraintDistribution,
                             'ConstraintDistribution')

        self.constraintFolder.ConnectSrc(self.serialUtils.relationConstraint.GetConstraint())

    def getIkSplineConstraintFolder(self,
                                    splineIkRigRoot):
        metaData = splineIkRigRoot.PropertyList.Find('anchorConstraint')

        if metaData is None:
            return None

        groupMembersCount = metaData.GetSrcCount()
        if groupMembersCount == 0:
            return None

        return metaData.GetSrc(0)

    def buildNetwork(self):
        self.increment = 0

        limitIndex = len(self.strapPointArray)-1
        for strapPointIndex, strapPoint in enumerate(self.strapPointArray):
            driverIndex = strapPointIndex
            if strapPointIndex == limitIndex:
                driverIndex = limitIndex-1

            chainPoint = ChainComponent(self.increment,
                                        self.relationConstraint,
                                        strapPoint,
                                        self.strengthSlider,
                                        driverIndex)

            chainPoint.buildConstraint()
            chainPoint.strapAnchor.blendDriver.Parent = self.chainDriverGroup

            self.connectMetaData(chainPoint.strapAnchor.blendDriver,
                                 'chainComponents')

            self.increment += 1

        self.chainDriverGroup.Parent = self.dummy


class SegmentRangeCondition(object):
    WEIGHT_PROPERTY = 'ConstraintDistribution'#'inputWeight'

    SHIFT_COMPONENT_OFFSET = 300

    HORIZONTAL_OFFSET = 350

    VERTICAL_OFFSET = 750

    WEAPON_PROPERTY = 'Body_Weapon_DOF_{0:02d}'

    def __init__(self,
                 inputNode,
                 outputNode,
                 receiver,
                 index,
                 relationConstraint,
                 lowerBound,
                 upperBound):
        self.relationConstraint = relationConstraint

        self.lowerBound = lowerBound

        self.upperBound = upperBound

        self.upperlimitCheck = None

        self.lowerlimitCheck = None

        self.condition1 = None

        self.condition2 = None

        self.senderA = None

        self.senderB = None

        self.senderC = None

        self.substract = None

        self.divide = None

        self.index = index

        self.inputNode = inputNode

        self.outputNode = outputNode

        self.receiver = receiver

        self.create(self.inputNode,
                    self.outputNode)

        self.setLayout(None)

    def findAnimationNode(self, inputComponent, attributeName):
        for node in inputComponent.Nodes:
            if node.Name == attributeName:
                return node

        return None

    def create(self,
               inputNode,
               outputNode):
        self.substract = self.relationConstraint.AddFunctionBox("Number",
                                                                "Subtract (a - b)")

        self.divide = self.relationConstraint.AddFunctionBox("Number",
                                                             "Divide (a/b)")

        self.senderA = self.relationConstraint.AddSenderBox(inputNode)

        self.senderB = self.relationConstraint.AddSenderBox(inputNode)

        self.senderC = self.relationConstraint.AddSenderBox(inputNode)

        self.condition1 = self.relationConstraint.AddFunctionBox("Number",
                                                                 "If Cond Then A Else B")

        self.condition2 = self.relationConstraint.AddFunctionBox("Number",
                                                                 "If Cond Then A Else B")

        self.condition3 = self.relationConstraint.AddFunctionBox("Number",
                                                                 "If Cond Then A Else B")

        self.clampWeight = self.relationConstraint.AddFunctionBox("Number",
                                                                  "Is Less (a < b)")

        self.lowerlimitCheck = self.relationConstraint.AddFunctionBox("Number",
                                                                      "Is Greater or Equal (a >= b)")

        self.upperlimitCheck = self.relationConstraint.AddFunctionBox("Number",
                                                                      "Is Less (a < b)")

        lowerlimitBplugs = self.findAnimationNode(self.lowerlimitCheck.GetBox().AnimationNodeInGet(),
                                                  'b')

        lowerlimitBplugs.WriteData([self.lowerBound])

        upperlimitBplugs = self.findAnimationNode(self.upperlimitCheck.GetBox().AnimationNodeInGet(),
                                                  'b')

        upperlimitBplugs.WriteData([self.upperBound])

        contition1Bplugs = self.findAnimationNode(self.condition1.GetBox().AnimationNodeInGet(),
                                                  'b')

        contition1Bplugs.WriteData([self.lowerBound])

        contition3Aplugs = self.findAnimationNode(self.condition3.GetBox().AnimationNodeInGet(),
                                                  'a')

        contition3Aplugs.WriteData([0.0])

        clampWeightBplugs = self.findAnimationNode(self.clampWeight.GetBox().AnimationNodeInGet(),
                                                  'b')

        clampWeightBplugs.WriteData([0.0])

        contition2Bplugs = self.findAnimationNode(self.condition2.GetBox().AnimationNodeInGet(),
                                                  'b')

        contition2Bplugs.WriteData([self.upperBound])

        divideBplugs = self.findAnimationNode(self.divide.GetBox().AnimationNodeInGet(),
                                              'b')

        rangeRatio = (self.upperBound - self.lowerBound)
        divideBplugs.WriteData([rangeRatio])

        subtractBplugs = self.findAnimationNode(self.substract.GetBox().AnimationNodeInGet(),
                                                'b')

        subtractBplugs.WriteData([self.lowerBound])

        self.relationConstraint.ConnectBoxes(self.senderA, 
                                             self.WEIGHT_PROPERTY, 
                                             self.lowerlimitCheck, 
                                             'a')

        self.relationConstraint.ConnectBoxes(self.senderB, 
                                             self.WEIGHT_PROPERTY, 
                                             self.condition1, 
                                             'a')

        self.relationConstraint.ConnectBoxes(self.senderC, 
                                             self.WEIGHT_PROPERTY, 
                                             self.condition2, 
                                             'a')

        self.relationConstraint.ConnectBoxes(self.lowerlimitCheck, 
                                             'Result', 
                                             self.condition1, 
                                             'Cond')

        self.relationConstraint.ConnectBoxes(self.condition1, 
                                             'Result', 
                                             self.upperlimitCheck, 
                                             'a')

        self.relationConstraint.ConnectBoxes(self.upperlimitCheck, 
                                             'Result', 
                                             self.condition2, 
                                             'Cond')

        self.relationConstraint.ConnectBoxes(self.condition2, 
                                             'Result', 
                                             self.substract, 
                                             'a')

        self.relationConstraint.ConnectBoxes(self.substract, 
                                             'Result', 
                                             self.divide, 
                                             'a')

        self.relationConstraint.ConnectBoxes(self.divide, 
                                             'Result', 
                                             self.clampWeight, 
                                             'a')

        self.relationConstraint.ConnectBoxes(self.clampWeight, 
                                             'Result', 
                                             self.condition3, 
                                             'Cond')

        self.relationConstraint.ConnectBoxes(self.divide, 
                                             'Result', 
                                             self.condition3, 
                                             'b')

        targetProperty = None
        for property in outputNode.PropertyList:
            if self.WEAPON_PROPERTY.format(self.index) in property.Name:
                targetProperty = property

        if targetProperty is None:
            targetProperty = outputNode.PropertyCreate(self.WEAPON_PROPERTY.format(self.index),
                                                       mobu.FBPropertyType.kFBPT_double,
                                                       'Number',
                                                       True,
                                                       True,
                                                       None)


        targetProperty.SetAnimated(True)
        targetProperty.SetMax(1.0)

        #bug 4145513 we want to reverse ConstraintDistribution behaviour
        self.weightReverseSubstract = self.relationConstraint.AddFunctionBox("Number",
                                                                             "Subtract (a - b)")

        weightReverseSubstractAplugs = self.findAnimationNode(self.weightReverseSubstract.GetBox().AnimationNodeInGet(),
                                                              'a')

        weightReverseSubstractAplugs.WriteData([1.0])

        self.relationConstraint.ConnectBoxes(self.condition3, 
                                             'Result', 
                                             self.weightReverseSubstract, 
                                             'b')

        #bug 4368705 - code request to revert to previous behaviour
        #(0 -> strap following the weapon/ 1-> strap following the body).
        self.relationConstraint.ConnectBoxes(self.condition3, 
                                             'Result', 
                                             self.receiver, 
                                             targetProperty.Name)

    def rename(self,
               inputPrefix):
        pass

    def setLayout(self,
                  inputTransform):
        self.senderA.SetBoxPosition(-self.HORIZONTAL_OFFSET, self.VERTICAL_OFFSET*self.index)

        self.lowerlimitCheck.SetBoxPosition(0, 80+self.VERTICAL_OFFSET*self.index)

        self.senderB.SetBoxPosition(0, -80+self.VERTICAL_OFFSET*self.index)

        self.condition1.SetBoxPosition(self.HORIZONTAL_OFFSET, self.VERTICAL_OFFSET*self.index)

        self.upperlimitCheck.SetBoxPosition(self.HORIZONTAL_OFFSET*2, 80+self.VERTICAL_OFFSET*self.index)

        self.senderC.SetBoxPosition(self.HORIZONTAL_OFFSET*2, -80+self.VERTICAL_OFFSET*self.index)

        self.condition2.SetBoxPosition(self.HORIZONTAL_OFFSET*3, self.VERTICAL_OFFSET*self.index)

        self.substract.SetBoxPosition(self.HORIZONTAL_OFFSET*4, self.VERTICAL_OFFSET*self.index)

        self.divide.SetBoxPosition(self.HORIZONTAL_OFFSET*5, self.VERTICAL_OFFSET*self.index)

        self.clampWeight.SetBoxPosition(self.HORIZONTAL_OFFSET*6, -80+self.VERTICAL_OFFSET*self.index)

        self.condition3.SetBoxPosition(self.HORIZONTAL_OFFSET*7, self.VERTICAL_OFFSET*self.index)

        self.receiver.SetBoxPosition(self.HORIZONTAL_OFFSET*8, 1500)


class SerialChainDriver(object):
    SERIAL_GROUP_NAME = 'serialDrivers1'

    WEIGHT_PROPERTY = 'inputWeight'

    START_CHAIN_RANGE = 0.0

    END_CHAIN_RANGE = 100.0

    def __init__(self):
        self.driverGroup = None

        self.utils = ikUtils.BindDataUtils()

        self.jointArray = []

        self.chainLength = 0.0

        self.upperSegmentBounds = [0.0]

        self.chainSegments = [0.0]

        self.chainRatios = []

        self.segmentRange = []

        self.relationConstraint = None

        self.weightProperty = None

        self.constraintDistribution = None

        self.chainUtils = ChainRange()

    def collectSegment(self):
        chainData = self.utils.collectChainSegments(self.jointArray)
        self.chainLength = chainData['chainLength']

        previousChainLength = 0.0
        for chainKnot in chainData['chainSegments']:
            previousChainLength+= chainKnot
            self.upperSegmentBounds.append(previousChainLength)
            self.chainSegments.append(chainKnot)

        for chainKnot in self.upperSegmentBounds:
            self.chainRatios.append((chainKnot/self.chainLength))

        for chainKnotIndex in xrange(1, len(self.chainRatios)):
            self.segmentRange.append(self.chainRatios[chainKnotIndex]-self.chainRatios[chainKnotIndex-1])

    def buildConstraintWithChainLength(self):
        constraintPrefix = 'serialBlender'
        relationConstraintName = '{}_Relation'.format(constraintPrefix)
        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)
        self.relationConstraint.SetActive(True)

        limitIndex = len(self.chainRatios)-1
        ouputNode = Scene.FindModelByName('ConstraintDistribution')
        ouputNodeProperty = ouputNode.PropertyList.Find('ConstraintDistribution')

        if ouputNodeProperty is None:
            ouputNodeProperty = outputNode.PropertyCreate(self.WEAPON_PROPERTY.format(self.index),
                                                          mobu.FBPropertyType.kFBPT_double,
                                                          'Number',
                                                          True,
                                                          True,
                                                          None)

        ouputNodeProperty.SetAnimated(True)

        self.constraintDistribution = ouputNode

        receiver = self.relationConstraint.AddReceiverBox(ouputNode)
        for index in xrange(limitIndex):
            nextIndex = index + 1
            if nextIndex > limitIndex:
                nextIndex = limitIndex

            rangeData = SegmentRangeCondition(ouputNode,
                                              ouputNode,
                                              receiver,
                                              index,
                                              self.relationConstraint,
                                              self.chainRatios[index],
                                              self.chainRatios[nextIndex])

    def buildConstraint(self,
                        blendWeight):
        constraintPrefix = 'serialBlender'
        relationConstraintName = '{}_Relation'.format(constraintPrefix)
        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)
        self.relationConstraint.SetActive(True)

        limitIndex = len(self.jointArray)
        ouputNodeProperty = self.constraintDistribution.PropertyList.Find('ConstraintDistribution')

        if ouputNodeProperty is None:
            ouputNodeProperty = outputNode.PropertyCreate(self.WEAPON_PROPERTY.format(self.index),
                                                          mobu.FBPropertyType.kFBPT_double,
                                                          'Number',
                                                          True,
                                                          True,
                                                          None)

        ouputNodeProperty.SetAnimated(True)

        self.chainUtils.extractChainPropagation(self.START_CHAIN_RANGE, 
                                                self.END_CHAIN_RANGE, 
                                                len(self.jointArray), 
                                                blendWeight)

        receiver = self.relationConstraint.AddReceiverBox(self.constraintDistribution)
        for index in xrange(limitIndex):
            rangeData = SegmentRangeCondition(self.constraintDistribution,
                                              self.constraintDistribution,
                                              receiver,
                                              index,
                                              self.relationConstraint,
                                              self.chainUtils.keyframeValues[index][0],
                                              self.chainUtils.keyframeValues[index][1])

    def build(self,
              targetParent,
              jointArray,
              blendWeight=0.75):
        self.jointArray = jointArray 

        mobu.FBSystem().Scene.Evaluate()

        self.buildConstraint(blendWeight)

