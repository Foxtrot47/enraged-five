import inspect
import json
import math
import os
import tempfile

from PySide import QtGui, QtCore

import pyfbsdk as mobu
from RS import Globals

from RS.Utils import Scene

from RS.Tools.IkSpline import Core as IkSplineCore
from RS.Tools.IkSpline import Utils as IkUtils
from RS.Tools.IkSpline import Rigging as IkRigging
from RS.Tools.IkSpline import Settings as ikSettings
from RS.Utils import ContextManagers, Namespace
from RS.Utils.Scene import Constraint
from RS.Utils.Scene import RelationshipConstraintManager as relationConstraintManager
from RS.Tools.IkSpline import Tetrahedron
from RS.Utils.Logging import const
from RS.Tools.IkSpline.assetSetup import ikFkParallelChainBuilder
from RS.Utils import Scene


reload(ikFkParallelChainBuilder)
reload(IkUtils)

class Build(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    DUMMY_NAME = 'Dummy01'

    def __init__(self):
        self.ikSplineUtils = IkSplineCore.Setup()

        self.constraintUtils = IkUtils.ConstraintUtils()

        self.nurbsUtils = ikFkParallelChainBuilder.Build()

        self.ropeJointArray = []

        self.lassoJointArray = []

        self.ropeRootName = 'Main_p_cs_rope02x'  #'p_cs_rope02x_Bone_01'

        self.ropeRoot = None

        self.lassoRootName = 'Lasso_main_bone' #'p_cs_rope02x_Bone_35'

        self.lassoRoot = None

        self.defaultFbxPath = 'X:/rdr3/art/animation/resources/props/cutscene/p_cs_ropeLasso01x.fbx'

        self.fkControlRootName = 'Root_p_cs_rope02x_Control'

        self.controlConstraintName = 'Bone_Relation_Constraint'

        self.toyBoxName = 'ToyBox Marker'

        self.rigName = 'rope'

        self.pathName = 'ropePath01'

        self.legacyControlRoot = None

        self.controlConstraint = None

        self.upReferenceVector = mobu.FBVector3d(0.0, 10.0, 0.0)

        self.numberOfLassoCurveController = 8

        self.sceneConstraints = []

    def getPart(self,
                namePattern,
                getModelOnly=True,
                inputNameSpace=None):
        outputControl = mobu.FBComponentList()

        if inputNameSpace is None:
            mobu.FBFindObjectsByName(namePattern,
                                     outputControl,
                                     getModelOnly,
                                     False)
        else:
            sampleName = '{0}:{1}'.format(inputNameSpace,
                                          namePattern)


            mobu.FBFindObjectsByName(sampleName,
                                     outputControl,
                                     getModelOnly,
                                     True)

        if len(outputControl)==0:
            return None

        return outputControl[0]

    def getHierachy(self,
                    currentNode,
                    nodeArray):
        for currentNode in currentNode.Children:
            nodeArray.append(currentNode)

            self.getHierachy(currentNode,
                             nodeArray)

    def purgeFkRig(self):
        self.dummy = self.getPart(self.DUMMY_NAME)

        self.controlConstraint = self.getPart(self.controlConstraintName,
                                              getModelOnly=False)

        if self.controlConstraint is not None:
            if isinstance(self.controlConstraint, mobu.FBConstraint):
                self.controlConstraint.Active = False

                self.controlConstraint.FBDelete()

        self.legacyControlRoot = self.getPart(self.fkControlRootName)

        if self.legacyControlRoot is None:
            return

        nodeArray = []

        self.getHierachy(self.legacyControlRoot,
                         nodeArray)

        nodeArray.reverse()

        for node in nodeArray:
            node.FBDelete()

        self.legacyControlRoot.FBDelete()

        toyBox = self.getPart(self.toyBoxName)
        if toyBox is not None:
            toyBox.FBDelete()

        toyBoxConstraint = self.getPart('mover_Relation_Constraint',
                                        getModelOnly=False)

        if toyBoxConstraint is not None:
            if isinstance(toyBoxConstraint,
                          mobu.FBConstraint):
                toyBoxConstraint.Active = False

                toyBoxConstraint.FBDelete()

    def setupRigOptions(self,
                        aimToNextJointInRig=False,
                        addExtensionToCharacter=None,
                        rigStorage=None,
                        useMhJoints=False):
        ikSplineSettings = ikSettings.IkSplineSettings()

        ikSplineSettings.hasSnakeCompensationLayer = True
        ikSplineSettings.createTweaker = True
        ikSplineSettings.tweakerControlsSize = 50
        ikSplineSettings.ikControlsSize = 250 # 7.2
        ikSplineSettings.numberOfCurveController = 12
        ikSplineSettings.rigAnchor = 'mover' 

        ikSplineSettings.aimToNextJoint = aimToNextJointInRig 
        ikSplineSettings.createFkControls = False 
        ikSplineSettings.maintainChainLength = True 
        ikSplineSettings.bindMethod = 'knotTranslation' # other options 'knotTranslation'/'cluster'
        ikSplineSettings.excludeThreshold = 0.001
        ikSplineSettings.createIntermediateGroup = False

        ikSplineSettings.reverseChain = False

        ikSplineSettings.rigStorage = rigStorage 

        ikSplineSettings.rigAnchor = 'mover' 

        ikSplineSettings.ikPathName = 'ropeIkPath1'

        self.ikSplineUtils.ikSplineSettings = ikSplineSettings

    def createIkSplineRope(self):
        jointUtils = IkUtils.JointChain()

        self.ropeJointArray = jointUtils.getDelimitedJointChain(self.ropeRootName,
                                                                self.lassoRootName)

        self.setupRigOptions(rigStorage=self.DUMMY_NAME)

        ikSplineStructureSettings = self.ikSplineUtils.buildRig(None, 
                                                                self.ropeJointArray)

        utils = IkUtils.AnimUtils()

        tweakerGroup = utils.exposeDrivenJoints(ikSplineStructureSettings.splineIkRig,
                                                ['tweakerGroup'])

        tweakers = [node for node in tweakerGroup if node.Name.endswith('_Tweaker')]
        self.endTweaker = tweakers[-1] 

        anchorGroup = utils.exposeDrivenJoints(ikSplineStructureSettings.splineIkRig,
                                               ['anchorConstraint'])

        for node in anchorGroup:
            if node.ClassName() != 'target':
                continue

            node.Active = False
            node.FBDelete()

        self.splineIkRig = ikSplineStructureSettings.splineIkRig

    def computeCurves(self):
        rigName = str(self.rigName)
        self.nurbsUtils.rigName = str(self.rigName)

        splineObject = self.nurbsUtils.createPathFromJoints(self.ropeJointArray,
                                                            name=str(self.rigName))

        self.nurbsUtils.splineObject = self.nurbsUtils.rebuildCurve(splineObject,
                                                                    self.numberOfLassoCurveController,
                                                                    deleteInputCurve=False)

        self.nurbsUtils.splineObject.Name = self.pathName

        return splineObject

    def computeBarycentricNurbsCurve(self,
                                     controlList,
                                     splineObject,
                                     usePositionContraint=False):
        sourceCurvePointPositions = self.nurbsUtils.getCurvePoints(splineObject,
                                                                   createNull=False)

        curveWeightUtils = Tetrahedron.SplinePathSamples()
        curveWeightUtils.computeWeights()

        curveWeightUtils.computePathInterpolation(controlList)

        previewPath = curveWeightUtils.buildPreviewPath()
        previewPath.Name = 'Preview01'

        curveWeightUtils.getCurvePoints()

        self.nurbsUtils.pointLimit = len(curveWeightUtils.curvePoints)-1

        self.nurbsUtils.curveSamples = curveWeightUtils.curvePoints[:]

        self.nurbsUtils.startIndex = 0

        self.knotDataArray = []

        for pointIndex, point in enumerate(sourceCurvePointPositions):
            knotData = self.nurbsUtils.getClosestPointOnCurve(point,
                                                              threshold=0.001)

            knotData.weightData = [curveWeightUtils.curveKnotWeights[knotData.samplingIndex],
                                   curveWeightUtils.curveKnotWeights[knotData.samplingIndex+1]]

            knotData.sourcePoint = point

            knotData.blendWeights()

            self.knotDataArray.append(knotData)

        rigPrefix = ''
        self.pointNodeArray = []
        self.pointNodeConstraintArray = []
        self.pointNodeArrayGroup = mobu.FBModelNull('{0}{1}_pathPoints'.format(rigPrefix,
                                                                               self.nurbsUtils.rigName))

        self.attachFolder = None

        attachMethod = Constraint.PARENT_CHILD
        if usePositionContraint:
            attachMethod = Constraint.POSITION

        for knotIndex, knotData in enumerate(self.knotDataArray):
            pointNode, pointConstraint = self.nurbsUtils.emulateSkinCluster(knotData,
                                                                            self.nurbsUtils.curveSamples[knotIndex],
                                                                            controlList,
                                                                            constraintType=attachMethod)

            pointNode.Name = '{0}{1}_pathPoint1'.format(rigPrefix,
                                                        self.nurbsUtils.rigName)

            pointConstraint.Name = '{0}_Attach1'.format(pointNode.Name)

            self.pointNodeArray.append(pointNode)

            self.pointNodeConstraintArray.append(pointConstraint)

            pointNode.Parent = self.pointNodeArrayGroup

            if knotIndex == 0:
                self.attachFolder = mobu.FBFolder('{0}_attachConstraints 1'.format(self.nurbsUtils.rigName),
                                                                                   pointConstraint)

                self.attachFolder.ConnectSrc(pointConstraint)
            else:
                self.attachFolder.ConnectSrc(pointConstraint)

        return previewPath

    def createTangentRig(self,
                         inputNodeArray,
                         controlList,
                         offsetRotation=mobu.FBVector3d(0, 0, 0),
                         previousIndexOffset=1):
        self.chainNodes = []

        self.tangentDataArray = []

        offsetMatrix = mobu.FBMatrix()
        
        mobu.FBRotationToMatrix(offsetMatrix,   
                                offsetRotation, 
                                mobu.FBRotationOrder.kFBXYZ)

        for node in inputNodeArray:
            cloneNode = mobu.FBModelNull('{0}_chainPoint1'.format(self.nurbsUtils.rigName))

            nodeMatrix = mobu.FBMatrix()
            node.GetMatrix(nodeMatrix)

            nodeMatrix = nodeMatrix*offsetMatrix

            cloneNode.Parent = node

            cloneNode.SetMatrix(nodeMatrix)

            self.chainNodes.append(cloneNode)

        for nodeIndex in xrange(len(self.chainNodes)):
            previousIndex = nodeIndex-previousIndexOffset
            if previousIndex < 0:
                previousIndex = 0

            nextIndex = nodeIndex+1
            if nextIndex > len(self.chainNodes)-1:
                nextIndex = len(self.chainNodes)-1

            tangent = ikFkParallelChainBuilder.TangentSegment()

            tangent.buildFromNodes(self.chainNodes[nodeIndex],
                                   self.chainNodes[previousIndex],
                                   self.chainNodes[nextIndex])

            if nodeIndex == 0:
                self.tangentFolder = mobu.FBFolder('{0} tangentConstraints 1'.format(self.nurbsUtils.rigName),
                                                                                     tangent.targetConstraint)

                self.tangentFolder.ConnectSrc(tangent.orientOrient)
            else:
                self.tangentFolder.ConnectSrc(tangent.targetConstraint)

                self.tangentFolder.ConnectSrc(tangent.orientOrient)

            self.tangentDataArray.append(tangent)

        mobu.FBSystem().Scene.Evaluate()

        lastControl = controlList[len(controlList)-1]
        alignNode = self.constraintUtils.constraintParts(self.tangentDataArray[-1].aimTarget,
                                                         lastControl)

        alignNode.Name = '{0}_position1'.format(self.tangentDataArray[-1].aimTarget)
        self.tangentFolder.ConnectSrc(alignNode)

        mobu.FBSystem().Scene.Evaluate()

    def createOffsetGroup(self,
                          joint):
        offset = mobu.FBModelNull('{0}_zro1'.format(joint.Name))

        offset.Show = True
        offset.PropertyList.Find('Look').Data = 0

        nodeMatrix = mobu.FBMatrix()
        joint.GetMatrix(nodeMatrix)
        offset.SetMatrix(nodeMatrix)

        joint.Parent = offset

        return offset

    def attachToPath(self):
        driverArray = [node.tangent for node in self.tangentDataArray]

        self.curveFolder = None

        self.nurbsDriverConstraints = []
        attachState = False
        nodeIndex = 0

        for index in xrange(1, len(self.tweakControls)):
            alignNode = self.constraintUtils.constraintParts(self.ropeJointArray[index],
                                                             self.tweakControls[index],
                                                             activateConstraint=attachState,
                                                             preserveOffset=True)

            alignNode.Name = '{0}_nurbsDriver1'.format(self.ropeJointArray[index].Name)

            if nodeIndex == 0:
                self.curveFolder = mobu.FBFolder('{0}_curveConstraints1'.format(self.nurbsUtils.rigName),
                                                                                alignNode)

                self.curveFolder.ConnectSrc(alignNode)
            else:
                self.curveFolder.ConnectSrc(alignNode)

            nodeIndex += 1
            self.nurbsDriverConstraints.append(alignNode)

        mobu.FBSystem().Scene.Evaluate()

        for constraint in self.nurbsDriverConstraints:
            constraint.Active = True

    def createHierarchy(self,
                          controlList,
                          parentOffset=False):
        offsetArray = []
        for node in controlList:
            offsetArray.append(self.createOffsetGroup(node))

        if parentOffset is False:
            self.ikControlGroup = mobu.FBModelNull('{0}_lassoControls'.format(self.nurbsUtils.rigName))

            self.ikControlGroup .Show = True
            self.ikControlGroup .PropertyList.Find('Look').Data = 0

            for node in offsetArray:
                node.Parent = self.ikControlGroup 

            return

        for index in xrange(1, len(offsetArray)):
            offsetArray[index].Parent = controlList[index-1]

    def getJointsWithPrefix(self,
                            namePattern,
                            root):
        jointUtils = IkUtils.JointChain()

        ropeJointArray = jointUtils.getJointChainFromPrefix(namePattern)
        ropeData = {}
        for node in ropeJointArray:
            ropeData[node.Name] = node

        ropeJointArray = [ropeData[key] for key in sorted(ropeData.keys())] 

        self.ropeJointArray = [Scene.FindModelByName('Lasso_main_bone')]
        self.ropeJointArray.extend(ropeJointArray)

    def prepareCurveKnotAimLayer(self,
                                 controlList):
        self.nurbsUtils.pointLimit = len(controlList)-1

        self.nurbsUtils.curveSamples = [node.PropertyList.Find('Translation (Lcl)').Data for node in controlList]

        sourceCurvePointPositions = [node.PropertyList.Find('Translation (Lcl)').Data for node in self.pointNodeArray]

        self.nurbsUtils.startIndex = 0

        self.segmentknotDataArray = []

        for pointIndex, point in enumerate(sourceCurvePointPositions):
            knotData = self.nurbsUtils.getClosestPointOnCurve(point,
                                                              threshold=0.001)

            knotData.sourcePoint = point

            knotData.blendWeights()

            self.segmentknotDataArray.append(knotData)

        self.controlTangentFolder = None

        self.controlTangentDataArray = []

        for controlIndex in xrange(len(controlList)-1):
            tangent = ikFkParallelChainBuilder.TangentSegment()

            tangent.buildFromNodes(controlList[controlIndex],
                                   controlList[controlIndex],
                                   controlList[controlIndex+1])

            if controlIndex == 0:
                self.controlTangentFolder = mobu.FBFolder('{0} controlAimConstraints 1'.format(self.nurbsUtils.rigName),
                                                                                            tangent.targetConstraint)

                self.controlTangentFolder.ConnectSrc(tangent.orientOrient)
            else:
                self.controlTangentFolder.ConnectSrc(tangent.targetConstraint)

                self.controlTangentFolder.ConnectSrc(tangent.orientOrient)

            self.controlTangentDataArray.append(tangent)

        tangentCountLimit = len(self.controlTangentDataArray)-1

        self.knotTwistFolder = None
        self.knotTwistContraints = []

        for pointIndex, knotData in enumerate(self.segmentknotDataArray):
            parentIndex = knotData.samplingIndex
            if knotData.segmentWeights > 0.9999:
                parentIndex = knotData.samplingIndex + 1

            alignNode = self.constraintUtils.constraintParts(self.pointNodeArray[pointIndex],
                                                             self.controlTangentDataArray[parentIndex].tangent,
                                                             constraintType=Constraint.ROTATION)

            alignNode.Name = '{0}_knotTwist1'.format(self.pointNodeArray[pointIndex].Name)

            if pointIndex == 0:
                self.knotTwistFolder = mobu.FBFolder('{0} knotTwistConstraints 1'.format(self.nurbsUtils.rigName),
                                                                                         alignNode)

                self.knotTwistFolder.ConnectSrc(alignNode)
            else:
                self.knotTwistFolder.ConnectSrc(alignNode)

            self.knotTwistContraints.append(alignNode)

    def alignControlEnds(self,
                         controlList):
        lastControl = controlList[len(controlList)-1]

        mobu.FBSystem().Scene.Evaluate()

        lastControl.PropertyList.Find('Translation (Lcl)').Data = controlList[0].PropertyList.Find('Translation (Lcl)').Data
        lastControl.PropertyList.Find('Rotation (Lcl)').Data = controlList[0].PropertyList.Find('Rotation (Lcl)').Data

        mobu.FBSystem().Scene.Evaluate()

    def createLassoTweakers(self):
        self.tweakControls = []

        for boneIndex, bone in enumerate(self.ropeJointArray):
            tweakerOffsetCtrl = self.ikSplineUtils.ikSplineLayer.createControl('{0}_Tweaker'.format(bone.Name),
                                                                                                    'Tweaker',
                                                                                                    80)

            tweakerOffsetCtrl.Parent = self.tangentDataArray[boneIndex].tangent

            constraintNode = self.constraintUtils.alignWithConstraint(tweakerOffsetCtrl,
                                                                      bone)

            self.constraintUtils.bakeConstraintValue(constraintNode)

            self.tweakControls.append(tweakerOffsetCtrl)

    def connectMetaData(self,
                        rootNode,
                        category,
                        childModel):
        metaRootProperty = rootNode.PropertyList.Find(category)

        if metaRootProperty is None:
            return

        childModel.ConnectDst(metaRootProperty)

    def finalizeLassoRig(self):
        self.controlList[0].Visibility = False

        self.controlList[-1].Visibility = False

        for node in self.pointNodeArray:
            node.Show = False

        self.lassoIkGroup = mobu.FBGroup('{0}_lassoIkControls'.format(self.rigName))

        for control in self.controlList:
            self.lassoIkGroup.ConnectSrc(control)

        self.lassoTweakerGroup = mobu.FBGroup('{0}_lassoTweakers'.format(self.rigName))

        for tweaker in self.tweakControls:
            self.lassoTweakerGroup.ConnectSrc(tweaker)

        controlsGroup = None
        for group in mobu.FBSystem().Scene.Groups:
            if group.Name != 'Controls':
                continue

            controlsGroup = group

        if controlsGroup is None:
            return

        controlsGroup.ConnectSrc(self.lassoIkGroup)

        controlsGroup.ConnectSrc(self.lassoTweakerGroup)

        self.tweakControls[0].FBDelete()

        self.tweakControls.pop(0)

        self.lassoRigRoot = mobu.FBModelNull('{0}{1}_Rig'.format('',
                                                                        self.nurbsUtils.rigName))

        for component in [self.pointNodeArrayGroup,
                          self.ikControlGroup]:
            component.Parent = self.lassoRigRoot

        root = self.lassoRigRoot.PropertyCreate('ikSplineMetaRoot', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True, None)
        root.Data = 'lassoIkRoot'

        for lassoAttribute in ['lassoFolders',
                               'ikControls',
                               'tweakers',
                               'attachConstraints',
                               'controlAimConstraints',
                               'knotTwistConstraints',
                               'tangentConstraints',
                               'curveConstraints']:
            self.lassoRigRoot.PropertyCreate(lassoAttribute, 
                                             mobu.FBPropertyType.kFBPT_object, 
                                             'Object', 
                                             False,
                                             True,
                                             None)

        controlCategories = ['ikControls',
                             'tweakers']

        for index, inputArray in enumerate([self.controlList,
                                            self.tweakControls]):
            for node in inputArray:
                self.connectMetaData(self.lassoRigRoot,
                                     controlCategories[index],
                                     node)

        for node in [self.attachFolder,
                     self.tangentFolder,
                     self.curveFolder,
                     self.controlTangentFolder,
                     self.knotTwistFolder]:
            self.connectMetaData(self.lassoRigRoot,
                                 'lassoFolders',
                                 node)

        for node in self.nurbsDriverConstraints:
            self.connectMetaData(self.lassoRigRoot,
                                 'curveConstraints',
                                 node)

        for node in self.knotTwistContraints:
            self.connectMetaData(self.lassoRigRoot,
                                 'knotTwistConstraints',
                                 node)

        for node in self.pointNodeConstraintArray:
            self.connectMetaData(self.lassoRigRoot,
                                 'attachConstraints',
                                 node)

        for tangent in self.tangentDataArray:
            self.connectMetaData(self.lassoRigRoot,
                                 'tangentConstraints',
                                 tangent.targetConstraint)

            self.connectMetaData(self.lassoRigRoot,
                                 'tangentConstraints',
                                 tangent.orientOrient)

        for tangent in self.controlTangentDataArray:
            self.connectMetaData(self.lassoRigRoot,
                                 'controlAimConstraints',
                                 tangent.targetConstraint)

            self.connectMetaData(self.lassoRigRoot,
                                 'controlAimConstraints',
                                 tangent.orientOrient)

    def createLassoRig(self):
        self.getJointsWithPrefix('w_melee_lasso03x_Bone',
                                 'Lasso_main_bone')

        splineObject = self.computeCurves()

        ikControlsSize = 250

        self.controlList = self.nurbsUtils.ikSplineUtils.ikSplineLayer.buildTwistDriverFromCurve(self.nurbsUtils.splineObject.Name,
                                                                                            ikControlsSize)

        self.alignControlEnds(self.controlList)
                         
        previewPath = self.computeBarycentricNurbsCurve(self.controlList,
                                                        splineObject)

        self.prepareCurveKnotAimLayer(self.controlList)

        self.createTangentRig(self.pointNodeArray,
                              self.controlList,
                              previousIndexOffset=1)

        self.createLassoTweakers()

        self.attachToPath()

        mobu.FBSystem().Scene.Evaluate()

        self.createHierarchy(self.controlList)

        for node in [splineObject,
                     self.nurbsUtils.splineObject,
                     previewPath]:
            node.FBDelete()

        self.finalizeLassoRig()

    def createLassoManipulator(self):
        self.lassoManipulator = self.ikSplineUtils.ikSplineLayer.createControl('{0}_lassoCtrl'.format(self.nurbsUtils.rigName),
                                                                               'Tweaker',
                                                                               450)

        self.lassoManipulator.PropertyList.Find('Look').Data = 0

        self.lassoManipulator.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire

        mobu.FBSystem().Scene.Evaluate()
        constraintNode = self.constraintUtils.alignWithConstraint(self.lassoManipulator,
                                                                  self.endTweaker)

        self.constraintUtils.bakeConstraintValue(constraintNode)

        lassoOffset = self.createOffsetGroup(self.lassoManipulator)

        endConstraint = self.constraintUtils.constraintParts(lassoOffset,
                                                             self.endTweaker,
                                                             constraintType=Constraint.POSITION,
                                                             preserveOffset=True)

        endConstraint.Name = '{0}_offsetLassoHook1'.format(self.nurbsUtils.rigName)

        ikGroupHook = self.constraintUtils.constraintParts(self.ikControlGroup,
                                                           self.lassoManipulator,
                                                           preserveOffset=True)

        ikGroupHook.Name = '{0}_ikGroupHook1'.format(self.nurbsUtils.rigName)

        mainLassoHook = Scene.FindModelByName('Lasso_main_bone')

        boneHook = self.constraintUtils.constraintParts(mainLassoHook,
                                                        self.lassoManipulator,
                                                        preserveOffset=True)

        boneHook.Name = '{0}_boneHook1'.format(self.nurbsUtils.rigName)

        boneRoot = 'Root_p_cs_rope02x'

    def createRig(self,
                  loadDefaultAsset=True,
                  purgeRig=True,
                  createIkSpline=True,
                  createLasso=True):
        if loadDefaultAsset is True:
            mobu.FBApplication().FileNew()
            assetFile = os.path.normpath(self.defaultFbxPath).replace('\\', '/')

            mobu.FBApplication().FileOpen(self.defaultFbxPath, False)

        if purgeRig:
            self.purgeFkRig()

        if createIkSpline:
            self.sceneConstraints = [node for node in mobu.FBSystem().Scene.Constraints]

            self.createIkSplineRope()

        if createLasso:
            self.createLassoRig()

        if createIkSpline:
            self.createLassoManipulator()

            self.lassoRigRoot.Parent = self.splineIkRig

            newConstraints = [node for node in mobu.FBSystem().Scene.Constraints]
            newConstraints = list(set(newConstraints) - set(self.sceneConstraints))

            for node in newConstraints:
                node.Lock = True