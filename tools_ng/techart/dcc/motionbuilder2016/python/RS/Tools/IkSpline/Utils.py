import json
import pyfbsdk as mobu

from RS.Utils.Scene import Constraint
from RS import Globals

from RS.Utils.Logging import const
from RS.Core.AssetSetup.Sliders import addAssetSliders
from RS.Tools.IkSpline import Solver as ikCSolver
from RS.Core.Animation import Lib
from RS.Utils import ContextManagers
from RS.Utils import Namespace
from RS.Tools.IkSpline.Widgets import plotProgressWidget
from RS.Utils import Scene

from PySide import QtGui, QtCore

reload(plotProgressWidget)


class SimpleSplinePlotter(object):
    def __init__(self):
        self.constraintState = []

    def collectConstraintState(self,
                               jointComponents):
        constraintManager = ConstraintUtils()

        for joint in jointComponents:
            drivingConstraints = constraintManager.findConstraints(joint)

            for constraint in drivingConstraints:
                self.constraintState.append([constraint,
                                             constraint.Active])

    def restoreConstraintState(self):
        for constraintData in self.constraintState:
            constraintData[0].Active = constraintData[1]

        Globals.Scene.Evaluate()

    def plotSimpleSplineRig(self,
                            jointComponents,
                            processAllTakes=False):
        if processAllTakes is False:
            self.processSimpleSplineRig(jointComponents)
        else:
            self.collectConstraintState(jointComponents)

            for take in Globals.Scene.Takes:
                mobu.FBSystem().CurrentTake = take

                Globals.Scene.Evaluate()

                self.restoreConstraintState()

                self.processSimpleSplineRig(jointComponents)

    def processSimpleSplineRig(self,
                               jointComponents):
        with RestoreConstraints():
            animUtils = JointCloud()

            animUtils.bakeJointComponents(jointComponents)

        constraintManager = ConstraintUtils()

        constraintManager.disableConstraints(jointComponents)

        Globals.Scene.Evaluate()


class RestoreConstraints(object):
    def __init__(self):
        self.constraintState = []

    def __enter__(self):
        for constraint in Globals.Scene.Constraints:
            if constraint.Active is False:
                continue

            self.constraintState.append([constraint,
                                         constraint.Lock])

            constraint.Lock = True

    def __exit__(self,
                 exceptionType,
                 value,
                 traceback):
        for constraintData in self.constraintState:
            constraintData[0].Lock = constraintData[1]


class MoverTransfertComponents(object):
    def __init__(self,
                 mode,
                 moverNode=None,
                 sourceContactPoint=None,
                 targetContactPoint=None):
        self.mode = mode

        self.moverNode = moverNode

        self.sourceContactPoint = sourceContactPoint

        self.targetContactPoint = targetContactPoint

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)

        reportData += '\n\t{0}:{1}'.format('mode',
                                           self.mode)

        return reportData


class JointCloud(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    def __init__(self):
        self.pointArray = []

        self.jointArray = []

        self.splineIkRig = None

        self.constraintUtils = ConstraintUtils()

        self.animUtils = AnimUtils()

    def exposeTakeRange(self, usePlayerControl=True):
        lPlayer = mobu.FBPlayerControl()
        startTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        stopTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        return startTime, stopTime, lPlayer

    def collectRigRoot(self,
                       splineIkRigName):
        self.splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if self.splineIkRig is None:
            return

    def exposeDrivenJoints(self,
                           splineIkRig,
                           jointLinkArray):
        jointArray = []

        for jointLink in jointLinkArray:
            metaData = splineIkRig.PropertyList.Find(jointLink)
            numberOfDrivenJoints = metaData.GetSrcCount()

            for memberIndex in range(numberOfDrivenJoints):
                member = metaData.GetSrc(memberIndex)
                if member is None:
                    continue

                jointArray.append(member)

        return jointArray

    def exposeSkelRoot(self):
        jointArray = []
        jointProperty = self.splineIkRig.PropertyList.Find('skelRootModelArray')

        groupMembersCount = jointProperty.GetSrcCount()

        for memberIndex in range(groupMembersCount):
            member = jointProperty.GetSrc(memberIndex)

            jointArray.append(member)

        return jointArray[0]

    def alignComponents(self,
                        inputNode,
                        targetNode):
        alignConstraint = self.constraintUtils.alignWithConstraint(inputNode,
                                                                   targetNode)
        alignConstraint.Active = True
        self.constraintUtils.bakeConstraintValue(alignConstraint)

    def createMH(self,
                 splineIkRigName):
        self.collectRigRoot(splineIkRigName)

        if self.splineIkRig is None:
            return

        self.jointArray = self.exposeDrivenJoints(self.splineIkRig,
                                                  ['joints'])

        skelRoot = self.exposeSkelRoot()

        anchorProperty = self.splineIkRig.PropertyCreate('mhPointsAnchors',
                                                         mobu.FBPropertyType.kFBPT_object,
                                                         'Object',
                                                         False,
                                                         True,
                                                         None)

        for sourceJoint in self.jointArray:
            null = mobu.FBModelNull('{}'.format(sourceJoint.Name.replace('SKEL',
                                                                         'MH')))

            null.Parent = skelRoot
            null.QuaternionInterpolate = True

            null.ConnectDst(anchorProperty)

            self.alignComponents(null,
                                 sourceJoint)

    def bakeMhComponents(self,
                         splineIkRigName):
        self.collectRigRoot(splineIkRigName)

        if self.splineIkRig is None:
            return

        skelRootModelArray = self.exposeDrivenJoints(self.splineIkRig,
                                                     ['skelRootModelArray'])

        skelRoot = None
        for node in skelRootModelArray:
            if 'SKEL_ROOT' in node.Name:
                skelRoot = node
                break

        self.sourceArray = [skelRoot]

        self.jointArray = self.exposeDrivenJoints(self.splineIkRig,
                                                  ['joints'])

        self.transferAnimationComponents = []

        with ExtractWorlspaceState(self.jointArray,
                                   self.splineIkRig) as stateUtils:
            self.transferAnimationComponents = stateUtils.targetNodeArray

        self.pointArray = self.exposeDrivenJoints(self.splineIkRig,
                                                  ['mhPointsAnchors'])

        components = []
        for nodeIndex in xrange(len(self.pointArray)):
            components.append(TransferComponent(self.transferAnimationComponents[nodeIndex].worldTarget,
                                                self.pointArray[nodeIndex]))

        isValid = False
        with TransferWorlspaceState(components):
            isValid = True

        for node in self.transferAnimationComponents:
            node.worldTarget.FBDelete()

    def prepareMoverAttachment(self,
                               contactPointComponents):
        currentTake = mobu.FBSystem().CurrentTake
        startTime = currentTake.ReferenceTimeSpan.GetStart()
        mobu.FBPlayerControl().Goto(startTime)

        Globals.Scene.Evaluate()

        if contactPointComponents.mode ==0:
            return

        if contactPointComponents.mode ==1 or contactPointComponents.mode ==2:
            constraintType = Constraint.POSITION

            if contactPointComponents.mode ==2:
                constraintType = Constraint.PARENT_CHILD

            targetConstraint = Constraint.CreateConstraint(constraintType)
            targetConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                          contactPointComponents.moverNode)

            targetConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                          contactPointComponents.sourceContactPoint)

            targetConstraint.Active = True

            targetConstraint.Lock = True

            contactPointComponents.moverNode.Selected = True

            contactPointComponents.moverNode.QuaternionInterpolate = True

            lOptions = mobu.FBPlotOptions()
            lOptions.PlotAllTakes = False
            lOptions.PlotLockedProperties = False
            lOptions.PreciseTimeDiscontinuities = True
            lOptions.PlotTranslationOnRootOnly = False
            lOptions.PreciseTimeDiscontinuities = True
            lOptions.UseConstantKeyReducer = False
            lOptions.PlotOnFrame = True

            lOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterNone
            currentTake.PlotTakeOnSelected(lOptions)

        if contactPointComponents.mode ==3:
            constraintType = Constraint.PARENT_CHILD

            targetConstraint = Constraint.CreateConstraint(constraintType)
            targetConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                          contactPointComponents.moverNode)

            targetConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                          contactPointComponents.sourceContactPoint)

            targetConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                          contactPointComponents.targetContactPoint)

            sourceWeight = None
            targetWeight = None
            animationNodeA = None
            animationNodeB = None
            for property in targetConstraint.PropertyList:
                if property.Name.endswith('{}.Weight'.format(contactPointComponents.sourceContactPoint.Name)):
                    property.SetAnimated(True)
                    sourceWeight = property
                    property.SetFocus(True)
                    animationNodeA = property.GetAnimationNode()

                if property.Name.endswith('{}.Weight'.format(contactPointComponents.targetContactPoint.Name)):
                    property.SetAnimated(True)
                    targetWeight = property
                    property.SetFocus(True)
                    animationNodeB = property.GetAnimationNode()


                if '.Offset T' in property.Name:
                    property.Data = mobu.FBVector3d()
                    property.SetLocked(True)
                    print  property.Name,  property.Data
                if '.Offset R' in property.Name:
                    property.Data = mobu.FBVector3d()
                    property.SetLocked(True)
                    print  property.Name,  property.Data

            targetConstraint.Active = True

            animationNodes = []
            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = contactPointComponents.moverNode.PropertyList.Find(property)

                if not inputAttribute.IsAnimated():
                    inputAttribute.SetAnimated(True)

                animationNode = inputAttribute.GetAnimationNode()

                for node in animationNode.Nodes:
                    if node.FCurve is None:
                        continue

                    node.FCurve.EditClear()

                animationNodes.append(animationNode)
                inputAttribute.SetFocus(True)

            currentTake = mobu.FBSystem().CurrentTake

            startTime = currentTake.ReferenceTimeSpan.GetStart()

            endTime = currentTake.ReferenceTimeSpan.GetStop()

            mobu.FBPlayerControl().Goto(startTime)

            sourceWeight.Data = 100.0

            targetWeight.Data = 0.0

            mobu.FBPlayerControl().Key()

            mobu.FBPlayerControl().Goto(endTime)

            sourceWeight.Data = 0.0

            targetWeight.Data = 100.0

            mobu.FBPlayerControl().Key()

            sourceWeight.SetFocus(False)

            targetWeight.SetFocus(False)

            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = contactPointComponents.moverNode.PropertyList.Find(property)

                inputAttribute.SetFocus(False)

            contactPointComponents.moverNode.QuaternionInterpolate = True

            targetConstraint.Active = False

        targetConstraint.Active = False

        targetConstraint.FBDelete()

        contactPointComponents.moverNode.Selected = False

        Globals.Scene.Evaluate()

    def prepareSkelRootStabilization(self,
                                     skelRoot,
                                     deleteRotationLastkeys=False,
                                     keyInclusiveRange=False,
                                     setStaticOrientation=False):
        currentTake = mobu.FBSystem().CurrentTake

        startTime = currentTake.ReferenceTimeSpan.GetStart()

        endTime = currentTake.ReferenceTimeSpan.GetStop()

        #getTail0 to constraint this NULL
        # as the parentconstraint can malfunction with certain take/layer action
        gameConstraintArray = self.exposeDrivenJoints(self.splineIkRig,
                                                      ['gameJointConstraints'])

        skelRootConstraint = None
        for node in gameConstraintArray:
            if 'SKEL_ROOT' in node.Name:
                skelRootConstraint = node
                break

        skelTail0 = skelRootConstraint.ReferenceGet(self.PARENT_NODE_INDEX, 0)
        attributePrefix = skelTail0.LongName

        #BUG [4162610]- the skel root and Tail Zero - was not in the same spaceLoc1
        #we need to restore the parent constraint offset to get the null in correct orientation
        offsetRotationValue = mobu.FBVector3d(0.0, 0.0, 0.0)
        for property in skelRootConstraint.PropertyList:
            if not property.Name.startswith(attributePrefix):
                continue

            if not property.Name.endswith('.Offset R'):
                continue

            offsetRotationValue = property.Data

        null = mobu.FBModelNull('skelRootStabilizer1')

        targetConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)#
        targetConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                      null)

        targetConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                      skelTail0)

        targetConstraint.Active = True

        for property in targetConstraint.PropertyList:
            if not property.Name.startswith(attributePrefix):
                continue

            if not property.Name.endswith('.Offset R'):
                continue

            property.Data = offsetRotationValue

        targetConstraint.Lock = True

        animationNodes = []

        for property in ('Translation (Lcl)',
                         'Rotation (Lcl)'):
            inputAttribute = null.PropertyList.Find(property)

            if not inputAttribute.IsAnimated():
                inputAttribute.SetAnimated(True)

            animationNode = inputAttribute.GetAnimationNode()

            animationNodes.append(animationNode)
            inputAttribute.SetFocus(True)

        mobu.FBPlayerControl().Goto(startTime)

        mobu.FBPlayerControl().Key()

        if setStaticOrientation is False:
            mobu.FBPlayerControl().Goto(endTime)

            mobu.FBPlayerControl().Key()
        else:
            mobu.FBPlayerControl().Goto(endTime)

            inputAttribute = null.PropertyList.Find('Translation (Lcl)')
            animationNode = inputAttribute.GetAnimationNode()

            for animationNode in animationNode.Nodes:
                animationNode.KeyCandidate()

        targetConstraint.FBDelete()

        if keyInclusiveRange is True:
            positionConstraint = Constraint.CreateConstraint(Constraint.POSITION)
            positionConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                          null)

            positionConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                          skelTail0)

            positionConstraint.Active = True

            positionConstraint.Lock = True

            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = null.PropertyList.Find(property)

                if not inputAttribute.IsAnimated():
                    inputAttribute.SetAnimated(True)

                animationNode = inputAttribute.GetAnimationNode()

                animationNodes.append(animationNode)
                inputAttribute.SetFocus(True)

            startTime, stopTime, lPlayer = self.exposeTakeRange()

            for timeIndex in range(startTime, stopTime+1):
                targetTime = mobu.FBTime(0, 0, 0, timeIndex, 0)
                mobu.FBPlayerControl().Goto(targetTime)

                mobu.FBPlayerControl().Key()

            positionConstraint.FBDelete()

        for property in ('Translation (Lcl)',
                         'Rotation (Lcl)'):
            inputAttribute = null.PropertyList.Find(property)

            inputAttribute.SetFocus(False)

        if deleteRotationLastkeys is True:
            for node in animationNodes[1].Nodes:
                node.FCurve.KeyDelete(1, 1)

        Globals.Scene.Evaluate()

        targetConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)#
        targetConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                      skelRoot)

        targetConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                      null)

        targetConstraint.Active = True

        targetConstraint.Lock = True

        self.skelRootData = [null,
                             targetConstraint]

        self.disableSkelRootIkRigConstraint()

    def bakeControllerInWorldSpace(self):
        ikControlGroup = self.splineIkRig.PropertyList.Find('IkControlGroup').GetSrc(0)

        tweakerGroup = self.splineIkRig.PropertyList.Find('tweakerGroup').GetSrc(0)

        ikControlArray = [node
                          for node in ikControlGroup.Items]

        tweakerArray = [node
                        for node in tweakerGroup.Items]

        self.ikSplineArrayControls = []

        self.ikSplineArrayControls.extend(ikControlArray)

        self.ikSplineArrayControls.extend(tweakerArray)

        self.transferIkAnimationComponents = []
        with ExtractWorlspaceState(self.ikSplineArrayControls,
                                   self.splineIkRig,
                                   componentName='Snake IkSpline components') as stateIkUtils:
            self.transferIkAnimationComponents = stateIkUtils.targetNodeArray

    def restoreControllerFromWorldSpace(self):
        isValid = False
        with TransferWorlspaceState(self.transferIkAnimationComponents,
                                    componentName='Snake IkSpline components'):
            isValid = True

        for node in self.transferIkAnimationComponents:
            node.worldTarget.FBDelete()

    def disableSkelRootIkRigConstraint(self):
        gameConstraintArray = self.exposeDrivenJoints(self.splineIkRig,
                                                      ['gameJointConstraints'])
        skelRootConstraint = None
        for node in gameConstraintArray:
            if 'SKEL_ROOT' in node.Name:
                skelRootConstraint = node
                break

        skelRootConstraint.Active= False

    def consolidateRootConstraints(self):
        gameConstraintArray = self.exposeDrivenJoints(self.splineIkRig,
                                                      ['gameJointConstraints'])

        tweakerArray = self.exposeDrivenJoints(self.splineIkRig,
                                                      ['tweakerGroup'])

        targetParentArray = []
        for gameConstraint in gameConstraintArray:
            drivenBone = gameConstraint.ReferenceGet(self.DRIVEN_NODE_INDEX, 0)
            if 'SKEL_ROOT' in drivenBone.Name:
                continue

            targetParent = gameConstraint.ReferenceGet(self.PARENT_NODE_INDEX, 0)

            bindNode = None
            for node in tweakerArray:
                if targetParent.Name in node.Name:
                    bindNode = node.Children[0]

            targetParentArray.append([drivenBone,
                                      bindNode])

        targetConstraintArray = []
        for targetParentData in targetParentArray:
            targetConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
            targetConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                          targetParentData[0])

            targetConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                          targetParentData[1])

            targetConstraint.Active = True

            targetConstraint.Lock = True

            targetConstraintArray.append(targetConstraint)

        return targetConstraintArray

    def purgeSplineIkRigRoot(self,
                             splineIkRig):
        currentTake = mobu.FBSystem().CurrentTake
        startTime = currentTake.ReferenceTimeSpan.GetStart()
        mobu.FBPlayerControl().Goto(startTime)

        resetData = []
        for index, propertyName in enumerate(['Translation (Lcl)',
                                              'Rotation (Lcl)',
                                              'Scaling (Lcl)']):
            property = splineIkRig.PropertyList.Find(propertyName)

            resetData.append(property.Data)

        for index, propertyName in enumerate(['Translation (Lcl)',
                                              'Rotation (Lcl)',
                                              'Scaling (Lcl)']):
            property = splineIkRig.PropertyList.Find(propertyName)

            if not property.IsAnimated():
                property.Data = resetData[index]
                continue

            animationData = property.GetAnimationNode()

            for animationNode in animationData.Nodes:
                animationNode.FCurve.EditClear()

            property.Data = resetData[index]

    def bakeMhInGlobalspace(self,
                            splineIkRigName,
                            contactPointComponents,
                            keyInclusiveRange=False,
                            setStaticOrientation=False):
        self.collectRigRoot(splineIkRigName)

        if self.splineIkRig is None:
            return

        skelRootModelArray = self.exposeDrivenJoints(self.splineIkRig,
                                                     ['skelRootModelArray'])

        skelRoot = None
        for node in skelRootModelArray:
            if 'SKEL_ROOT' in node.Name:
                skelRoot = node
                break

        contactPointComponents.moverNode = skelRoot.Parent

        dummyNode = contactPointComponents.moverNode.Parent

        #Snake/Hawk tweak
        if contactPointComponents.mode > 0:
            currentTake = mobu.FBSystem().CurrentTake
            startTime = currentTake.ReferenceTimeSpan.GetStart()
            mobu.FBPlayerControl().Goto(startTime)

            self.bakeControllerInWorldSpace()

        #Enable ikRig.
        setDisableRigValue = False
        self.animUtils.toggleIkSplineRigWeights(self.splineIkRig,
                                                setDisableRigValue)

        self.prepareSkelRootStabilization(skelRoot,
                                          keyInclusiveRange=keyInclusiveRange,
                                          setStaticOrientation=setStaticOrientation)

        self.jointArray = skelRootModelArray[:]

        self.sourceArray = self.exposeDrivenJoints(self.splineIkRig,
                                                   ['joints'])

        self.jointArray.extend(self.sourceArray)

        self.transferAnimationComponents = []

        for node in self.jointArray:
            node.QuaternionInterpolate = True

        Globals.Scene.Evaluate()

        self.purgeSplineIkRigRoot(self.splineIkRig)

        with ExtractWorlspaceState(self.jointArray,
                                   self.splineIkRig,
                                   componentName='Snake components') as stateUtils:
            self.transferAnimationComponents = stateUtils.targetNodeArray

        isValid = False

        self.prepareMoverAttachment(contactPointComponents)

        Globals.Scene.Evaluate()

        with TransferWorlspaceState(self.transferAnimationComponents,
                                    componentName='Snake components'):
            isValid = True

        for node in self.transferAnimationComponents:
            node.worldTarget.FBDelete()

        self.skelRootData[0].FBDelete()

        self.skelRootData[1].Active = False
        self.skelRootData[1].FBDelete()

        Globals.Scene.Evaluate()

        skelRoot.QuaternionInterpolate = True

        #Disable ikRig.
        setDisableRigValue = True
        self.animUtils.toggleIkSplineRigWeights(self.splineIkRig,
                                                setDisableRigValue)

        #Snake/Hawk tweak
        if contactPointComponents.mode > 0:
            self.restoreControllerFromWorldSpace()

    def bakeJointComponents(self,
                            jointComponents,
                            translateRotateOnly=False):
        self.transferAnimationComponents = []

        with ExtractWorlspaceState(jointComponents,
                                   self.splineIkRig,
                                   componentName='Joint components',
                                   translateRotateOnly=translateRotateOnly) as stateUtils:
            self.transferAnimationComponents = stateUtils.targetNodeArray

        isValid = False

        Globals.Scene.Evaluate()

        with TransferWorlspaceState(self.transferAnimationComponents,
                                    componentName='Joint components'):
            isValid = True

        for node in self.transferAnimationComponents:
            node.worldTarget.FBDelete()


class PlotAnimationFactory(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    SKEL_ROOT_NAME = 'SKEL_ROOT'

    DUMMY_GROUP = 'Dummy01'

    def __init__(self):
        self.splineIkRig = None

        self.jointArray = []

        self.characterComponents = []

        self.plotComponents = []

        self.constraintUtils = ConstraintUtils()

        self.transferAnimationComponents = []

        self.animationNodes = []

        self.sourceRotationProperty = []

        self.jointRotationProperty = []

        self.skelRoot = None

        self.animUtils = AnimUtils()

    def collectRigRoot(self,
                       splineIkRigName):
        self.splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if self.splineIkRig is None:
            return

    def exposeDrivenJoints(self,
                           splineIkRig,
                           jointLinkArray):
        jointArray = []

        for jointLink in jointLinkArray:
            metaData = splineIkRig.PropertyList.Find(jointLink)
            numberOfDrivenJoints = metaData.GetSrcCount()

            for memberIndex in range(numberOfDrivenJoints):
                member = metaData.GetSrc(memberIndex)
                if member is None:
                    continue

                jointArray.append(member)

        return jointArray

    def getCharacterNamespace(self,
                              inputCharacter):
        if inputCharacter is None:
            return None

        currentNamespace = Namespace.GetNamespace(inputCharacter)

        if len(currentNamespace)==0:
            return None

        return currentNamespace

    def collectComponents(self):
        self.jointArray = self.exposeDrivenJoints(self.splineIkRig,
                                                  ['joints'])

        self.characterComponents = self.exposeDrivenJoints(self.splineIkRig,
                                                           ['skelRootModelArray'])

        self.plotComponents = self.exposeDrivenJoints(self.splineIkRig,
                                                      ['skelRootModelArray',
                                                       'joints'])

        currentNamespace = self.getCharacterNamespace(self.splineIkRig)
        if currentNamespace is None:
            sampleName = '{0}'.format(self.SKEL_ROOT_NAME)

            targetNodeList = mobu.FBComponentList()
            mobu.FBFindObjectsByName(sampleName,
                                     targetNodeList,
                                     False,
                                     True)

            self.skelRoot = targetNodeList[0]


            sampleName = '{0}'.format(self.DUMMY_GROUP)

            targetNodeList = mobu.FBComponentList()
            mobu.FBFindObjectsByName(sampleName,
                                     targetNodeList,
                                     False,
                                     True)

            self.dummy = targetNodeList[0]
        else:
            sampleName = '{0}:{1}'.format(currentNamespace,
                                          self.SKEL_ROOT_NAME)

            targetNodeList = mobu.FBComponentList()
            mobu.FBFindObjectsByName(sampleName,
                                     targetNodeList,
                                     True,
                                     True)

            self.skelRoot = targetNodeList[0]

            sampleName = '{0}:{1}'.format(currentNamespace,
                                          self.DUMMY_GROUP)

            targetNodeList = mobu.FBComponentList()
            mobu.FBFindObjectsByName(sampleName,
                                     targetNodeList,
                                     True,
                                     True)

            self.dummy = targetNodeList[0]

        constraintLink = self.splineIkRig.PropertyList.Find('ikConstraint')
        self.ikConstraint = constraintLink.GetSrc(0)

    def plotAnimation(self):
        with ContextManagers.ClearModelSelection():
            self.processAnimation()

    def processAnimation(self):
        self.collectComponents()

        #Enabled  ikRig before we plot animation
        self.animUtils.toggleIkSplineRigWeights(splineIkRig, False)

        with ExtractWorlspaceState(self.plotComponents,
                                   self.splineIkRig) as stateUtils:
            self.transferAnimationComponents = stateUtils.targetNodeArray

        components = []
        for nodeIndex in xrange(len(self.transferAnimationComponents)):
            components.append(TransferComponent(self.transferAnimationComponents[nodeIndex].worldTarget,
                                                self.transferAnimationComponents[nodeIndex].sourceJoint))

        isValid = False
        with TransferWorlspaceState(components):
            isValid = True

        for node in self.transferAnimationComponents:
            node.worldTarget.FBDelete()

        #Disable ikRig.
        self.animUtils.toggleIkSplineRigWeights(splineIkRig,
                                      True)


class ExtractWorlspaceState(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    DEFAULT_TIME_DATA = 0

    def __init__(self,
                 sourceComponentArray,
                 splineIkRig,
                 plotKeys=True,
                 componentName='Pelt components',
                 translateRotateOnly=False):
        self.splineIkRig = splineIkRig

        self.sourceComponentArray = sourceComponentArray

        self.plotKeys = plotKeys
        
        self.translateRotateOnly = translateRotateOnly

        self.matchingConstraintArray = []

        self.targetNodeArray = []

        self.animationNodes = []

        self.assetProgress = None

        self.ikConstraint = None

        self.componentName = componentName

    def exposeTakeRange(self, usePlayerControl=True):
        lPlayer = mobu.FBPlayerControl()
        startTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        stopTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        return startTime, stopTime, lPlayer

    def keyCurrentFrame(self,
                        frameIndex,
                        lPlayer,
                        subSamples=1.0):
        sourceTime = mobu.FBTime(self.DEFAULT_TIME_DATA,
                                 self.DEFAULT_TIME_DATA,
                                 self.DEFAULT_TIME_DATA,
                                 (frameIndex-1),
                                 self.DEFAULT_TIME_DATA,
                                 mobu.FBTimeMode.kFBTimeMode30Frames)

        targetTime = mobu.FBTime(self.DEFAULT_TIME_DATA,
                                 self.DEFAULT_TIME_DATA,
                                 self.DEFAULT_TIME_DATA,
                                 frameIndex,
                                 self.DEFAULT_TIME_DATA,
                                 mobu.FBTimeMode.kFBTimeMode30Frames)

        sourceValue = sourceTime.GetSecondDouble()

        targetValue = targetTime.GetSecondDouble()

        samplingTime = mobu.FBTime(self.DEFAULT_TIME_DATA,
                                   self.DEFAULT_TIME_DATA,
                                   self.DEFAULT_TIME_DATA,
                                   0,
                                   self.DEFAULT_TIME_DATA,
                                   mobu.FBTimeMode.kFBTimeMode30Frames)

        samplingIncrement = (targetValue-sourceValue)/subSamples
        sampleCount = int(subSamples) + 1

        for shiftIndex in xrange(sampleCount):
            samplingValue = sourceValue+shiftIndex*samplingIncrement

            samplingTime.SetSecondDouble(samplingValue)
            lPlayer.Goto(samplingTime)

            Globals.Scene.Evaluate()
            if self.ikConstraint is not None:
                self.ikConstraint.FreezeSuggested()

            for animationNode in self.animationNodes:
                animationNode.KeyCandidate()

            mobu.FBSystem().Scene.Evaluate()
            
            if not self.translateRotateOnly:
                lPlayer.Key()

    def forceAnimationPlot(self):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        self.animationNodes =[]
        for control in self.targetNodeArray:
            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = control.worldTarget.PropertyList.Find(property)

                if not inputAttribute.IsAnimated():
                    inputAttribute.SetAnimated(True)

                animationNode = inputAttribute.GetAnimationNode()
                self.animationNodes.append(animationNode)
                inputAttribute.SetFocus(True)

            control.worldTarget.Selected = True

        reportMessage = '   MESSAGE [Setting keys on {componentName}] \n\t  percent done:{percent}\n\t  Processing frame{itemName}'
        progressRatio = 1.0/float(stopTime-startTime)
        progressIncrement = 0

        self.uiTextColor = const.MessageTextColours.Minor_Update

        self.assetProgress.show()

        for timeIndex in range(startTime, stopTime+1):
            progressValue = progressIncrement*progressRatio*100.0
            self.keyCurrentFrame(timeIndex,
                                 lPlayer)

            progressIncrement+=1
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                                            itemName=timeIndex,
                                                            componentName=self.componentName),
                                                            textcolor=self.uiTextColor)

            self.assetProgress._forceDrawOverride()

        self.assetProgress.hide()

    def __enter__(self):
        if self.splineIkRig is not None:
            constraintLink = self.splineIkRig.PropertyList.Find('ikConstraint')
            if constraintLink is not None:
                self.ikConstraint = constraintLink.GetSrc(0)

        componentCount = len(self.sourceComponentArray)-1
        if componentCount == 0:
            componentCount = 1

        progressRatio = 1.0/float(componentCount)
        progressValue = 0.0

        self.assetProgress = plotProgressWidget.PeltProgressWindowDialog()
        self.assetProgress.show()

        reportMessage = '   MESSAGE [Preparing worldspace components] \n\t  percent done:{percent}\n\t  Processing {itemName}'

        for nodeIndex, node in enumerate(self.sourceComponentArray):
            null = mobu.FBModelNull('{}_worldSpace1'.format(node.Name))

            null.Show = True
            null.Visible = True
            null.QuaternionInterpolate = True

            node.QuaternionInterpolate = True

            animationData = TransferComponent(null,
                                              node)

            metaProperty = node.PropertyList.Find('worldTarget')

            if metaProperty is None:
                metaProperty = node.PropertyCreate('worldTarget',
                                                   mobu.FBPropertyType.kFBPT_object,
                                                   'Object',
                                                   False,
                                                   True,
                                                   None)

            null.ConnectDst(metaProperty)

            self.targetNodeArray.append(animationData)

            matchingConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
            matchingConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                            null)

            matchingConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                            node)

            matchingConstraint.Active = True

            self.matchingConstraintArray.append(matchingConstraint)

            progressValue = int(nodeIndex*progressRatio*100)
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                                            itemName=node.Name))

            self.assetProgress._forceDrawOverride()

        self.assetProgress.hide()

        if self.plotKeys is True:
            self.forceAnimationPlot()

        return self

    def __exit__(self, exceptionType, value, traceback):
        self.assetProgress.close()

        mobu.FBSystem().Scene.Evaluate()

        for constraint in self.matchingConstraintArray:
            constraint.Active = False

            constraint.FBDelete()

        for control in self.targetNodeArray:
            control.worldTarget.Selected = False

            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = control.worldTarget.PropertyList.Find(property)
                inputAttribute.SetFocus(False)

        mobu.FBSystem().Scene.Evaluate()


class TransferComponent(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    NODE_ATTRIBUTES = ('worldTarget',
                       'sourceJoint',
                       'helper')

    def __init__(self,
                 inputNode,
                 sourceJoint,
                 helper=None):
        self.worldTarget = inputNode

        self.sourceJoint = sourceJoint

        self.helper = helper

    def __repr__(self):
        reportData = '\n<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_ATTRIBUTES:
            currentComponent = getattr(self, attribute)

            if currentComponent is None:
                reportData += '\n\t{0}:None'.format(attribute)
            else:
                reportData += '\n\t{0}:{1}'.format(attribute,
                                                   currentComponent.LongName)

        return reportData


class TransferWorlspaceState(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    def __init__(self,
                 inputComponentArray,
                 componentName='Pelt components'):
        self.assetProgress = None

        self.inputComponentArray = inputComponentArray

        self.targetConstraintArray = []

        self.componentName = componentName

    def bindBone(self,
                 sourceJoint,
                 worldTarget):
        sourceJoint.QuaternionInterpolate = True

        targetConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)#
        targetConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                      sourceJoint)

        targetConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                      worldTarget)

        targetConstraint.Active = True

        targetConstraint.Lock = True

        self.targetConstraintArray.append(targetConstraint)

    def exposeTakeRange(self, usePlayerControl=True):
        lPlayer = mobu.FBPlayerControl()
        startTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        stopTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        return startTime, stopTime, lPlayer

    def keyCurrentFrame(self,
                        frameIndex,
                        lPlayer):
        targetTime = mobu.FBTime(0, 0, 0, frameIndex, 0)
        lPlayer.Goto(targetTime)

        Globals.Scene.Evaluate()
        for animationNode in self.animationNodes:
            animationNode.KeyCandidate()

        lPlayer.Key()

    def forceAnimationPlot(self):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        self.animationNodes =[]

        reportMessage = '   MESSAGE [Setting keys on {componentName}] \n\t  percent done:{percent}\n\t  Processing frame{itemName}'
        progressRatio = 1.0/float(stopTime-startTime)
        progressIncrement = 0

        self.uiTextColor = const.MessageTextColours.Minor_Update

        self.assetProgress.show()

        for control in self.inputComponentArray:
            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = control.sourceJoint.PropertyList.Find(property)

                if not inputAttribute.IsAnimated():
                    inputAttribute.SetAnimated(True)

                animationNode = inputAttribute.GetAnimationNode()
                self.animationNodes.append(animationNode)
                inputAttribute.SetFocus(True)

            control.sourceJoint.Selected = True

        for timeIndex in range(startTime, stopTime+1):
            progressValue = progressIncrement*progressRatio*100.0

            self.keyCurrentFrame(timeIndex,
                                 lPlayer)

            progressIncrement+=1
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                                            itemName=timeIndex,
                                                            componentName=self.componentName),
                                                            textcolor=self.uiTextColor)

            self.assetProgress._forceDrawOverride()
        self.assetProgress.hide()

    def __enter__(self):
        componentCount = len(self.inputComponentArray)-1
        if componentCount == 0:
            componentCount = 1

        progressRatio = 1.0/float(componentCount)
        progressValue = 0.0

        self.assetProgress = plotProgressWidget.PeltProgressWindowDialog()
        self.assetProgress.show()

        reportMessage = '   MESSAGE [Transferring worldspace->Source joint] \n\t  percent done:{percent}\n\t  Processing {itemName}'


        for nodeIndex, node in enumerate(self.inputComponentArray):
            self.bindBone(node.sourceJoint,
                          node.worldTarget)

            progressValue = int(nodeIndex*progressRatio*100)
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                                            itemName=node.sourceJoint.Name))

            self.assetProgress._forceDrawOverride()

        self.assetProgress.hide()

        self.forceAnimationPlot()

        return self

    def __exit__(self, exceptionType, value, traceback):
        self.assetProgress.close()

        mobu.FBSystem().Scene.Evaluate()

        for constraint in self.targetConstraintArray:
            constraint.Active = False

            constraint.FBDelete()

        for control in self.inputComponentArray:
            control.sourceJoint.Selected = False

            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = control.sourceJoint.PropertyList.Find(property)
                inputAttribute.SetFocus(False)

        mobu.FBSystem().Scene.Evaluate()


class ChainComponent(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    def __init__(self,
                 chainParent,
                 sourceJoint,
                 sourceKnot):
        self.constraintUtils = ConstraintUtils()

        self.inputNode = mobu.FBModelSkeleton('{}_knot1'.format(sourceJoint.Name))

        self.inputNode.PropertyList.Find('Size').Data = 50.0

        self.inputNode.RotationActive = True

        self.inputNode.RotationOrder = mobu.FBModelRotationOrder.kFBEulerXZY

        self.chainParent = chainParent

        self.sourceJoint = sourceJoint

        self.sourceKnot = sourceKnot

        #self.inputNode.QuaternionInterpolate = True

        #self.sourceJoint.QuaternionInterpolate = True

        self.targetConstraint = None

        self.bindBone()

    def setParent(self,
                  inputNode,
                  parentNode):
        inputNode.Parent = parentNode

        inputNode.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d()

        inputNode.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d()

    def bindBone(self):
        self.targetConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        self.targetConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                           self.inputNode)

        self.targetConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                           self.sourceKnot)

        self.targetConstraint.Active = True
        self.targetConstraint.Lock = True

    def alignComponents(self,
                        inputNode,
                        targetNode):
        alignConstraint = self.constraintUtils.alignWithConstraint(inputNode,
                                                                   targetNode)
        alignConstraint.Active = True
        self.constraintUtils.bakeConstraintValue(alignConstraint)

    def snapPosition(self,
                     drivenObject,
                     targetParent):
        pointConstraint = Constraint.CreateConstraint(Constraint.POSITION)
        pointConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                     drivenObject)

        pointConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                     targetParent)

        pointConstraint.Active = True
        self.constraintUtils.bakeConstraintValue(pointConstraint)

    def buildRig(self):
        if self.chainParent is not None:
            self.setParent(self.inputNode,
                           self.chainParent)

        self.sourceKnot.Show = True
        self.inputNode.Show = True


class JointChain(object):
    def __init__(self):
        pass

    def getJointChainFromSelection(self):
        """
            This method return a joint hierarchy form the current ordered selected Items
        """
        rootModelList = mobu.FBModelList()
        mobu.FBGetSelectedModels(rootModelList,None,True,True)

        return rootModelList

    def getJointChainFromPrefix(self,
                                jointPrefix):
        """
            This method return a joint hierarchy to bind to an ikSpline constraint
            Args:
                jointPrefix (str): prefix string to use to extract a hierarchy

            return type FBModelList
        """
        jointChain = mobu.FBComponentList()

        mobu.FBFindObjectsByName('{0}*'.format(jointPrefix),
                                 jointChain,
                                 False,
                                 True)

        rootModelList = mobu.FBModelList()
        for obj in jointChain:
            rootModelList.append(obj)

        return rootModelList

    def getJointChainFromRoot(self,rootName):
        """
            This method return a joint hierarchy to bind to an ikSpline constraint
            Args:
                jointPrefix (str): prefix string to use to extract a hierarchy

            return type FBModelList
        """
        includeNamespace = False

        if ':' in rootName:
            includeNamespace = True

        jointRoot = Scene.FindModelByName(rootName,
                                          includeNamespace=includeNamespace)

        if jointRoot is None:
            raise ValueError('Invalid root name{0} , no object with this name exist in this file'.format(rootName))
            return

        rootModelList = mobu.FBModelList()
        rootModelList.append(jointRoot)

        jointChainList = self.GetHierachy(rootModelList)
        jointChain = mobu.FBModelList()
        jointChain.append(jointRoot)

        for obj in jointChainList:
            jointChain.append(obj)

        return jointChain

    def getDelimitedJointChain(self,startBoneName,endBoneName):
        startBone = mobu.FBFindModelByLabelName( startBoneName )

        if startBone is None:
            raise ValueError('Invalid root name{0} , no object with this name exist in this file'.format(startBoneName))
            return

        endBone = mobu.FBFindModelByLabelName( endBoneName )

        if endBone is None:
            raise ValueError('Invalid root name{0} , no object with this name exist in this file'.format(endBoneName))
            return

        jointHierarchy = self.getJointChainFromRoot(startBoneName)
        jointChain = mobu.FBModelList()

        for obj in jointHierarchy:
            jointChain.append(obj)
            if obj.Name == endBoneName:
                break

        return jointChain

    def getDelimitedJointChainFromNodes(self,
                                        startBone,
                                        endBone):

        jointHierarchy = self.getJointChainFromRoot(startBone.LongName)
        jointChain = mobu.FBModelList()

        for node in jointHierarchy:
            jointChain.append(node)

            if node.LongName == endBone.LongName:
                break

        return jointChain

    def getChainFromNameList(self,nameList):
        jointChain = mobu.FBModelList()

        for obj in nameList:
            currentBone = mobu.FBFindModelByLabelName(obj)
            if currentBone is not None:
                jointChain.append(currentBone)

        return jointChain

    def GetHierachy(self, parentModelList, recursive = True ):
        '''
            Method derived RS.Utils.Scene.Model in order to preserve hierarchy order
        '''
        returnSet = []
        for parentModel in parentModelList:
            for childmodel in parentModel.Children:
                returnSet.append( childmodel )
                if recursive:
                    returnSet.extend( self.GetHierachy([childmodel], True ) )
        return returnSet


class AnimUtils(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    def __init__(self):
        self.constraintUtils = ConstraintUtils()
        self.aimUtils = ikCSolver.MVector()

    def extractSplineIkConstraint(self, splineIkRig):
        linkArray = ['anchorConstraint','snakeCompensationGroup','tweakerGroup','bufferGroup']
        nodeArray = []
        for jointLink in linkArray:
            metaData = splineIkRig.PropertyList.Find(jointLink)
            numberOfDrivenJoints = metaData.GetSrcCount()

            for memberIndex in range(numberOfDrivenJoints):
                member = metaData.GetSrc(memberIndex)
                if member is None:
                    continue

                if not isinstance(member, mobu.FBConstraint):
                    continue

                nodeArray.append(member)

        return nodeArray

    def prepareTargetConstraint(self,
                                cloneArray,
                                cloneJointIndex,
                                drivenIndex,
                                targetIndex,
                                jointLimit,
                                jointDict):
        joint = cloneArray[cloneJointIndex][drivenIndex]
        baseControl = cloneArray[cloneJointIndex][targetIndex]
        nullArray = []

        #At this point we expect to work with chain element( so at least one child is needed)
        jointChildren = joint.Children[0]

        if jointChildren.Name in jointDict:
            aimControl  = jointDict[jointChildren.Name]
        else:
            targetParent = jointDict[joint.Name]
            targetSource = jointChildren

            aimControl = mobu.FBModelNull('{}_AimBind1'.format(targetSource.Name))
            aimControl.Show = True
            aimControl.Visible = True

            alignConstraint = self.constraintUtils.alignWithConstraint(aimControl, targetSource)
            alignConstraint.Active = True
            self.RefreshScene()

            self.constraintUtils.bakeConstraintValue(alignConstraint)
            aimControl.Parent = targetSource
            nullArray.append(aimControl)

        reverseAim = False

        bindObject = mobu.FBModelNull('{}_SourceBind1'.format(baseControl.Name))
        bindObject.Show = True
        bindObject.Visible = True

        alignConstraint = self.constraintUtils.alignWithConstraint(bindObject, baseControl)
        alignConstraint.Active = True
        self.RefreshScene()

        self.constraintUtils.bakeConstraintValue(alignConstraint)

        self.RefreshScene()
        bindObject.Parent = baseControl

        targetConstraintData = self.constraintUtils.createTargetConstraint(bindObject, aimControl, spaceObject=baseControl, reverseAim=reverseAim)
        targetConstraintData[0].Name = '{}_target1'.format(bindObject.Name)
        self.RefreshScene()

        orientConstraint = Constraint.CreateConstraint(Constraint.ROTATION)
        orientConstraint.ReferenceAdd(0, joint)
        orientConstraint.ReferenceAdd(1, bindObject)

        orientConstraint.Snap()
        orientConstraint.Active = True
        self.RefreshScene()

        nullArray.append(bindObject)

        return orientConstraint, targetConstraintData[0], nullArray

    def initKeyinggroup(self,
                        cloneArray,
                        drivenIndex=0,
                        targetIndex=1,
                        freeze=True,
                        rotationOnlyCount=0,
                        plotTrChannels=False):
        localKeyGroup = mobu.FBKeyingGroup("splineIkLocalKeyGroup", mobu.FBKeyingGroupType.kFBKeyingGroupLocal)
        jointList = []
        constraintArray = []
        lockList = []

        jointLimit =  rotationOnlyCount - 1
        jointDict = {}
        nullArray = []

        for cloneJointData in cloneArray:
            jointDict[cloneJointData[drivenIndex].Name] = cloneJointData[targetIndex]

        for cloneJointIndex, cloneJointData in enumerate(cloneArray):
            null = cloneJointData[drivenIndex]
            null.Selected = True

            translationProperty = null.PropertyList.Find('Lcl Translation')
            rotationProperty = null.PropertyList.Find('Rotation (Lcl)')

            useParentConstraint = False

            if rotationOnlyCount == 0:
                useParentConstraint = True

            if cloneJointIndex < rotationOnlyCount:
                useParentConstraint = False
            else:
                useParentConstraint = True

            if plotTrChannels is True:
                useParentConstraint = True

            constraint = None
            if useParentConstraint is True:
                constraint = self.constraintUtils.alignWithConstraint(cloneJointData[drivenIndex],
                                                                      cloneJointData[targetIndex],
                                                                      freeze=freeze)
            else:
                #----------------------------------------------we will use hierarchy topology as a rule
                joint = cloneArray[cloneJointIndex][drivenIndex]
                jointChildren = joint.Children

                if len(jointChildren) > 0:
                    constraint, target, nullList = self.prepareTargetConstraint(cloneArray,
                                                                                 cloneJointIndex,
                                                                                 drivenIndex,
                                                                                 targetIndex,
                                                                                 jointLimit,
                                                                                 jointDict)
                    constraintArray.append(target)
                    nullArray.extend(nullList)
                else:
                    constraint = Constraint.CreateConstraint(Constraint.ROTATION)
                    constraint.ReferenceAdd(0, cloneJointData[drivenIndex])
                    constraint.ReferenceAdd(1, cloneJointData[targetIndex])

                    constraint.Snap()
                    constraint.Active = True

            constraintArray.append(constraint)
            jointList.append(cloneJointData[targetIndex])

            if useParentConstraint is True:
                localKeyGroup.AddProperty(translationProperty)
                translationProperty.Selected = True
                translationProperty.SetFocus(True)
            else:
                translationProperty.Selected = False
                translationProperty.SetFocus(False)
                translationProperty.SetAnimated(False)

            localKeyGroup.AddProperty(rotationProperty)
            rotationProperty.Selected = True
            rotationProperty.SetFocus(True)

        localKeyGroup.SetEnabled (True)
        localKeyGroup.SetActive(True)

        return jointList, constraintArray, localKeyGroup, nullArray

    def releaseKeyinggroup(self, constraintArray, localKeyGroup, nullArray):
        for constraint in constraintArray:
            if constraint is None:
                continue
            constraint.Active = False
            constraint.FBDelete()

        for null in nullArray:
            null.FBDelete()

        localKeyGroup.RemoveAllProperties()
        localKeyGroup.FBDelete()

    def exposeTakeRange(self, usePlayerControl=True):
        lPlayer = mobu.FBPlayerControl()
        startTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        stopTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        if usePlayerControl is True:
            startTime = lPlayer.ZoomWindowStart.GetFrame()
            stopTime = lPlayer.ZoomWindowStop.GetFrame()

        return startTime, stopTime, lPlayer

    def plotTake(self):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        targetTime = mobu.FBTime(0, 0, 0, startTime, 0)
        mobu.FBPlayerControl().Goto(targetTime)

        lOptions = mobu.FBPlotOptions()
        lOptions.PlotAllTakes = False
        lOptions.PlotLockedProperties = False
        lOptions.PreciseTimeDiscontinuities = True
        lOptions.PlotTranslationOnRootOnly = False
        lOptions.PreciseTimeDiscontinuities = True
        lOptions.UseConstantKeyReducer = False
        lOptions.PlotOnFrame = False

        lOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll
        mobu.FBSystem().CurrentTake.PlotTakeOnSelectedProperties(lOptions)#PlotTakeOnSelectedProperties/PlotTakeOnSelected

    def jumpToTakeFirstFrame(self):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        targetTime = mobu.FBTime(0, 0, 0, startTime, 0)
        mobu.FBPlayerControl().Goto(targetTime)
        mobu.FBSystem().Scene.Evaluate()

    def forceAnimationPlot(self, jointList, ikConstraint, progressBar):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        for timeIndex in range(startTime, stopTime+1):
            if progressBar is not None:
                progressBar.Percent = int((timeIndex - startTime) / float(stopTime - startTime) * 100)
            self.keyCurrentFrame(timeIndex,
                                 jointList,
                                 ikConstraint,
                                 lPlayer)

    def bakeAnimation(self,
                      splineIkRig,
                      cloneArray,
                      selectIndex,
                      progressBar,
                      drivenIndex=0,
                      targetIndex=1,
                      preserveConstraint=False,
                      releaseRig=True,
                      positionOnlyCount=0,
                      plotTrChannels=False):
        self.jumpToTakeFirstFrame()
        rotationOnlyCount = len(cloneArray) - positionOnlyCount

        if positionOnlyCount == 0:
            rotationOnlyCount = 0

        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        ikConstraint = constraintLink.GetSrc(0)

        #setup keyinggroup
        jointList, constraintArray, localKeyGroup, nullArray = self.initKeyinggroup(cloneArray,
                                                                                    drivenIndex=drivenIndex,
                                                                                    targetIndex=targetIndex,
                                                                                    freeze=False,
                                                                                    rotationOnlyCount=rotationOnlyCount,
                                                                                    plotTrChannels=plotTrChannels)

        for joint in jointList:
            joint.QuaternionInterpolate = True

        if preserveConstraint is False:
            self.forceAnimationPlot(jointList, ikConstraint, progressBar)
        else:
            selectionList = mobu.FBModelList()
            mobu.FBGetSelectedModels(selectionList, None, True, False)
            for model in selectionList:
                selectionList.Selected = False

            for joint in jointList:
                joint.Selected = True

            self.plotTake()

        if releaseRig is True:
            self.releaseKeyinggroup(constraintArray, localKeyGroup, nullArray)

    def exposeDrivenJoints(self, splineIkRig, jointLinkArray):
        jointArray = []

        for jointLink in jointLinkArray:
            metaData = splineIkRig.PropertyList.Find(jointLink)
            numberOfDrivenJoints = metaData.GetSrcCount()

            for memberIndex in range(numberOfDrivenJoints):
                member = metaData.GetSrc(memberIndex)
                if member is None:
                    continue

                jointArray.append(member)
        return jointArray

    def cloneHierarchy(self, splineIkRig, jointLinkArray):
        startTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        stopTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        targetTime = mobu.FBTime(0, 0, 0, startTime, 0)
        mobu.FBPlayerControl().Goto(targetTime)
        mobu.FBSystem().Scene.Evaluate()

        drivenJoints = self.exposeDrivenJoints(splineIkRig, jointLinkArray)

        cloneArray = []
        for joint in drivenJoints:
            null = mobu.FBModelNull('{}_clone1'.format(joint.Name))
            constraint = self.constraintUtils.alignWithConstraint(null, joint)
            self.constraintUtils.bakeConstraintValue(constraint)

            cloneArray.append([null, joint])
            null.Show = True
            null.Visible = True

        return cloneArray

    def plotAnimation(self, splineIkRig, plotTranslation, deleteMarker=True, refreshConnections=False):
        metaRoot = splineIkRig.PropertyList.Find('ikSplineMetaRoot')
        if metaRoot is None:
            return

        jointArray = self.exposeDrivenJoints(splineIkRig, ['joints', 'skelRootModelArray'])
        constraintStates = self.collectConstraintState(jointArray)

        if refreshConnections is True:
            self.refreshConstraint(constraintStates)

        self.jumpToTakeFirstFrame()
        for constraintData in constraintStates:
            constraintData[0].Active = True

        #Enabled  ikRig before we plot animation
        self.toggleIkSplineRigWeights(splineIkRig, False)

        progressBar = mobu.FBProgress()
        progressBar.ProgressBegin()

        cloneArray = self.cloneHierarchy(splineIkRig, ['joints', 'skelRootModelArray'])
        self.RefreshScene()

        self.bakeAnimation(splineIkRig,
                           cloneArray,
                           0,
                           progressBar,
                           drivenIndex=0,
                           targetIndex=1)

        self.RefreshScene()
        self.jumpToTakeFirstFrame()

        for constraintData in constraintStates:
            constraintData[0].Active = False

        self.RefreshScene()

        positionOnlyCount = len(self.exposeDrivenJoints(splineIkRig, ['skelRootModelArray']))

        self.bakeAnimation(splineIkRig,
                           cloneArray,
                           1,
                           progressBar,
                           drivenIndex=1,
                           targetIndex=0,
                           preserveConstraint=True,
                           releaseRig=True,
                           positionOnlyCount=positionOnlyCount,
                           plotTrChannels=plotTranslation)

        self.RefreshScene()

        progressBar.ProgressDone()
        self.RefreshScene()

        if deleteMarker is True:
            for nullData in cloneArray:
                nullData[0].FBDelete()

        #Keep ikRig disabled to check plotting
        self.toggleIkSplineRigWeights(splineIkRig,
                                      True,
                                      refreshConnections=refreshConnections)

        self.RefreshScene()

    def collectConstraintState(self, jointArray):
        constraintStates = []
        constraintArray = self.exposeExternalConstraint(jointArray)
        for constraintList in constraintArray:
            for constraint in constraintList:
                constraintStates.append([constraint, constraint.Active])

        return constraintStates

    def exposeExternalConstraint(self, jointArray):
        constraintArray = []
        for joint in jointArray:
            constraints = self.constraintUtils.findConstraints(joint)
            if len(constraints)>0:
                constraintArray.append(constraints)

        return constraintArray

    def refreshConstraint(self, constraintStates):
        for constraintData in constraintStates:
            constraint = constraintData[0]
            if constraint.ClassName() == 'FBConstraint':
                drivenNode = constraint.ReferenceGet(0,0)
                sourceNode = constraint.ReferenceGet(1,0)
                constraint.Lock = False

                constraint.ReferenceRemove(0, drivenNode)
                constraint.ReferenceRemove(1, sourceNode)

                mobu.FBSystem().Scene.Evaluate
                constraint.ReferenceAdd(0, drivenNode)
                constraint.ReferenceAdd(1, sourceNode)

    def resetRig(self, splineIkRig):
        metaRoot = splineIkRig.PropertyList.Find('ikSplineMetaRoot')
        if metaRoot is None:
            return

        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        inputRigIkConstraint = constraintLink.GetSrc(0)

        inputRigIkConstraint.Active = False
        self.RefreshScene()

        groupList = ['ikConstraint',
                     'splineMatrix',
                     'snakeCompensationGroup',
                     'tweakerGroup',
                     'bufferGroup',
                     'FkControlGroup',
                     'IkControlGroup',
                     'SplineCurve',
                     'anchorConstraint',
                     'folder',
                     'gameJointConstraints']

        destroyArray = []
        for group in groupList:
            metaData = splineIkRig.PropertyList.Find(group)

            if metaData is None:
                continue

            groupMembersCount = metaData.GetSrcCount()
            if groupMembersCount == 0:
                continue

            for memberIndex in range(groupMembersCount):
                member = metaData.GetSrc(memberIndex)
                if member is None:
                    continue

                destroyArray.append(member)

        for objectModel in destroyArray:
            objectModel.FBDelete()

    def keyCurrentFrame(self,
                        frameIndex,
                        jointList,
                        ikConstraint,
                        lPlayer):
        targetTime = mobu.FBTime(0, 0, 0, frameIndex, 0)
        mobu.FBPlayerControl().Goto(targetTime)

        Globals.Scene.Evaluate()
        if ikConstraint is not None:
            ikConstraint.FreezeSuggested()

        self.forceFrameUpdate()
        lPlayer.Key()

    def alignAndkeyCurrentFrame(self,
                                frameIndex,
                                cloneArray,
                                lPlayer,
                                targetIndex,
                                drivenIndex):
        targetTime = mobu.FBTime(0, 0, 0, frameIndex, 0)
        mobu.FBPlayerControl().Goto(targetTime)

        Globals.Scene.Evaluate()
        scaleSource = mobu.FBVector3d()
        for cloneJointData in cloneArray:
            sourceMatrix = mobu.FBMatrix()
            cloneJointData[drivenIndex].GetMatrix(sourceMatrix)

            targetMatrix = mobu.FBMatrix()
            scaleVector = mobu.FBSVector()

            mobu.FBMatrixToScaling(scaleVector, sourceMatrix)
            scaleSource[0] = scaleVector[0]
            scaleSource[1] = scaleVector[1]
            scaleSource[2] = scaleVector[2]

            cloneJointData[targetIndex].GetMatrix(targetMatrix)
            cloneJointData[drivenIndex].SetMatrix(targetMatrix)
            cloneJointData[drivenIndex].SetVector(scaleSource,
                                                  mobu.FBModelTransformationType.kModelScaling)

        lPlayer.Key()

    def RefreshScene(self):
        """
            Force Evaluation of current scene .
        """
        mobu.FBSystem().Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

    def forceFrameUpdate(self):
        lPlayer = mobu.FBPlayerControl()
        currentFrame = mobu.FBSystem().LocalTime.GetFrame()

        targetTime = mobu.FBTime(0, 0, 0, currentFrame+1, 0)
        mobu.FBPlayerControl().Goto(targetTime)
        mobu.FBSystem().Scene.Evaluate()

        targetTime = mobu.FBTime(0, 0, 0, currentFrame, 0)
        mobu.FBPlayerControl().Goto(targetTime)
        mobu.FBSystem().Scene.Evaluate()

    def collectIkState(self, splineIkRig):
        jointArray = self.exposeDrivenJoints(splineIkRig, ['joints', 'skelRootModelArray'])
        constraintStates = self.collectConstraintState(jointArray)

        return constraintStates[1][1]

    def toggleIkSplineRigWeights(self,
                                 splineIkRig,
                                 toggleState):
        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        if constraintLink.GetSrcCount() == 0:
            warningMessage =  '{0} \n'.format('this rig ikSpline constraint was deleted at some point, \nPlease update your reference to fix it')

            boxwidget = QtGui.QMessageBox()
            boxwidget.setWindowTitle( "Broken rig warning")
            boxwidget.setText(warningMessage)
            
            boxwidget.exec_()
            return

        jointArray = self.exposeDrivenJoints(splineIkRig, ['joints', 'skelRootModelArray'])
        constraintStates = self.collectConstraintState(jointArray)

        for constraintData in constraintStates:
            constraintData[0].Active = not toggleState

        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        inputRigIkConstraint = constraintLink.GetSrc(0)
        inputRigIkConstraint.Active = True

        self.forceFrameUpdate()

    def rebuildAndOrientCurve(self, inputCurve):
        controlPath = mobu.FBModelPath3D('{}_Rebuild1'.format(inputCurve.Name))
        controlPath.Show = True
        controlPath.Visible = True

        #clean mobu embarrsing default shape with two points...
        controlPath.PathKeyClear()

        #OrienCurve here
        #@---
        FirstCv4dSource = inputCurve.PathKeyGet(0)
        SecondCv4dSource = inputCurve.PathKeyGet(1)

        FirstCv4d = mobu.FBVector4d()
        SecondCv4d = mobu.FBVector4d()

        inMatrix = mobu.FBMatrix()
        inputCurve.GetMatrix(inMatrix)

        mobu.FBVectorMatrixMult(FirstCv4d, inMatrix, FirstCv4dSource)
        mobu.FBVectorMatrixMult(SecondCv4d, inMatrix, SecondCv4dSource)

        FirstCv = mobu.FBVector3d(FirstCv4d[0], FirstCv4d[1], FirstCv4d[2])
        SecondCv = mobu.FBVector3d(SecondCv4d[0], SecondCv4d[1], SecondCv4d[2])

        upPos = mobu.FBModelNull('upPos')
        upPos.Show = True

        basePos = mobu.FBModelNull('basePos')
        basePos.Show = True

        targetPos = mobu.FBModelNull('targetPos')
        targetPos.Show = True

        upPos.Parent = basePos
        upPos.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0,10.0,0)
        basePos.SetVector(FirstCv)
        targetPos.SetVector(SecondCv)
        self.RefreshScene()

        aimConstraint = Constraint.CreateConstraint(Constraint.AIM)

        aimConstraint.ReferenceAdd (0, controlPath)
        aimConstraint.ReferenceAdd (1, targetPos)
        aimConstraint.ReferenceAdd (2, upPos)

        aimConstraint.PropertyList.Find('World Up Type').Data = 1

        posConstraint = Constraint.CreateConstraint(Constraint.POSITION)
        posConstraint.ReferenceAdd (0, controlPath)
        posConstraint.ReferenceAdd (1, basePos)

        self.RefreshScene()
        posConstraint.Active = True
        aimConstraint.Active = True

        self.RefreshScene()
        self.constraintUtils.bakeConstraintValue(posConstraint)
        self.constraintUtils.bakeConstraintValue(aimConstraint)

        self.RefreshScene()
        curveMatrix = mobu.FBMatrix()
        controlPath.GetMatrix(curveMatrix)

        pointCount = inputCurve.PathKeyGetCount()
        positionInCurveSpace = mobu.FBVector4d()
        positionInWorldSpace = mobu.FBVector4d()
        curveMatrix.Inverse()

        for cvIndex in range(pointCount):
            cvPoint = inputCurve.PathKeyGet(cvIndex)
            mobu.FBVectorMatrixMult(positionInWorldSpace, inMatrix, cvPoint)

            mobu.FBVectorMatrixMult(positionInCurveSpace, curveMatrix, positionInWorldSpace)
            if cvIndex == 0:
                controlPath.PathKeyStartAdd(positionInCurveSpace)
            else:
                controlPath.PathKeyEndAdd(positionInCurveSpace)

        for model in [basePos, targetPos, upPos]:
            model.FBDelete()

        return controlPath

    def snapRigOn3dCurve(self, splineIkRig, inputCurve, offset=0.0):
        ikControlArray = mobu.FBModelList()

        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        inputRigIkConstraint = constraintLink.GetSrc(0)

        for ikControlIndex in range(inputRigIkConstraint.ReferenceGetCount(2)):
            ikControlArray.append(inputRigIkConstraint.ReferenceGet(2,ikControlIndex))

        #TODO : reset ik control
        for control in ikControlArray:
            control.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d()
            control.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d()

        controlPath = self.rebuildAndOrientCurve(inputCurve)
        self.RefreshScene()

        sourceNumberOfDrivenJoints = controlPath.PathKeyGetCount()
        uParameters = BindDataUtils().collectCurveparameters(controlPath)

        numberOfDrivenJoints = len(ikControlArray)
        chainLengthArray = BindDataUtils().collectChainSegments(ikControlArray)

        ikConstraintForControl = self.constraintUtils.createIkConstraint(controlPath, None)
        tempArray = []
        for chainLengthIndex in range(sourceNumberOfDrivenJoints):
            loc = mobu.FBModelNull('locator1')
            loc.Show = True
            ikConstraintForControl.ReferenceAdd(1,loc)
            tempArray.append(loc)

        self.RefreshScene()
        for chainLengthIndex in range(sourceNumberOfDrivenJoints):
            uValueProperty = ikConstraintForControl.PropertyList.Find('uValue{}'.format((chainLengthIndex+1)))
            uValueProperty.Data = uParameters[chainLengthIndex]

        self.RefreshScene()
        ikConstraintForControl.Active  = True
        self.RefreshScene()

        ikConstraint = self.constraintUtils.createIkConstraint(controlPath, None)

        locArray = []
        for chainLengthIndex in range(numberOfDrivenJoints):
            loc = mobu.FBModelNull('locator1')
            loc.Show = True
            ikConstraint.ReferenceAdd(1, loc)
            locArray.append(loc)

        self.RefreshScene()
        for twistDriver in tempArray:
            ikConstraint.ReferenceAdd(2, twistDriver)

        self.RefreshScene()
        for chainLengthIndex in range(numberOfDrivenJoints-1):
            chainProperty = ikConstraint.PropertyList.Find('chainLength{}'.format((chainLengthIndex+1)))
            chainProperty.Data = chainLengthArray['chainSegments'][chainLengthIndex]

        ikConstraint.PropertyList.Find('ApplyIK_Chain_rules').Data = True
        ikConstraint.PropertyList.Find('offset').Data = offset

        settingAttribute = splineIkRig.PropertyList.Find('rigSettings')
        rigSettings = json.loads(str(settingAttribute.Data))

        ikConstraint.PropertyList.Find("Reverse Chain").Data = rigSettings['reverseChain']

        ikConstraint.Active  = True
        inputRigIkConstraint.PropertyList.Find('offset').Data = 0.0

        self.RefreshScene()
        for chainLengthIndex in range(numberOfDrivenJoints):
            chainProperty = ikConstraint.PropertyList.Find('chainValue{}'.format((chainLengthIndex+1)))
            uValueProperty = ikConstraint.PropertyList.Find('uValue{}'.format((chainLengthIndex+1)))
            uValueProperty.Data = chainProperty.Data

        self.RefreshScene()
        #ikConstraint.PropertyList.Find('ApplyIK_Chain_rules').Data = False

        self.RefreshScene()
        for jointIndex in range(numberOfDrivenJoints):
            self.snapTransformAndPreserveScale(ikControlArray[jointIndex], locArray[jointIndex])

        self.RefreshScene()

        for constraint in [ikConstraintForControl, ikConstraint]:
            constraint.Active = False
            constraint.FBDelete()

        for model in locArray:
            model.FBDelete()

        for model in tempArray:
            model.FBDelete()

        controlPath.FBDelete()

        self.RefreshScene()

    def snapTransformAndPreserveScale(self, drivenObject, targetObject):
        '''
        alignConstraint = self.constraintUtils.alignWithConstraint(drivenObject, targetObject)
        self.RefreshScene()
        self.constraintUtils.bakeConstraintValue(alignConstraint)

        return
        '''
        sourceMatrix = mobu.FBMatrix()
        scaleSource = mobu.FBVector3d()
        drivenObject.GetMatrix(sourceMatrix)

        targetMatrix = mobu.FBMatrix()
        scaleVector = mobu.FBSVector()

        mobu.FBMatrixToScaling(scaleVector, sourceMatrix)
        scaleSource[0] = scaleVector[0]
        scaleSource[1] = scaleVector[1]
        scaleSource[2] = scaleVector[2]

        targetObject.GetMatrix(targetMatrix)
        drivenObject.SetMatrix(targetMatrix)
        drivenObject.SetVector(scaleSource, mobu.FBModelTransformationType.kModelScaling)

    def matchRigOn3dCurve(self, splineIkRig, inputCurve, offset=0.0):
        ikControlArray = mobu.FBModelList()

        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        inputRigIkConstraint = constraintLink.GetSrc(0)

        for ikControlIndex in range(inputRigIkConstraint.ReferenceGetCount(2)):
            ikControlArray.append(inputRigIkConstraint.ReferenceGet(2,ikControlIndex))

        numberOfDrivenJoints = len(ikControlArray)
        uParameters = BindDataUtils().collectUparameters(inputCurve, numberOfDrivenJoints)

        controlPath = self.rebuildAndOrientCurve(inputCurve)
        self.RefreshScene()
        ikConstraint = self.constraintUtils.createIkConstraint(controlPath, None)

        locArray = []

        for chainLengthIndex in range(numberOfDrivenJoints):
            loc = mobu.FBModelNull('locator1')
            loc.Show = True
            ikConstraint.ReferenceAdd(1,loc)
            locArray.append(loc)

        self.RefreshScene()
        for chainLengthIndex in range(numberOfDrivenJoints):
            chainProperty = ikConstraint.PropertyList.Find('uValue{}'.format((chainLengthIndex+1)))
            chainProperty.Data = uParameters[chainLengthIndex]

        ikConstraint.PropertyList.Find('ApplyIK_Chain_rules').Data = False
        inputRigIkConstraint.PropertyList.Find('offset').Data = offset
        ikConstraint.Active  = True

        self.RefreshScene()
        for jointIndex in range(numberOfDrivenJoints):
            self.snapTransformAndPreserveScale(ikControlArray[jointIndex], locArray[jointIndex])

        self.RefreshScene()
        ikConstraint.Active = False
        ikConstraint.FBDelete()

        for model in locArray:
            model.FBDelete()

        controlPath.FBDelete()
        self.RefreshScene()

    def toggleControllerAxis(self, splineIkRig, toggleState):
        ikControlArray = []

        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        if constraintLink.GetSrcCount() == 0:
            return

        inputRigIkConstraint = constraintLink.GetSrc(0)

        for ikControlIndex in range(inputRigIkConstraint.ReferenceGetCount(2)):
            ikControl = inputRigIkConstraint.ReferenceGet(2,ikControlIndex)

            ikControl.PropertyList.Find('RotationAxisVisibility').Data = toggleState
            ikControl.PropertyList.Find('PivotsVisibility').Data = 2
            ikControl.PropertyList.Find('ReferentialSize').Data = 16.0

    def setAimControlKeyingGroup(self, ikControlArray):
        localKeyGroup = mobu.FBKeyingGroup("splineIkAimControlKeyGroup",
                                           mobu.FBKeyingGroupType.kFBKeyingGroupLocal)

        for ikControl in ikControlArray:
            rotationProperty = ikControl.PropertyList.Find('Rotation (Lcl)')

            localKeyGroup.AddProperty(rotationProperty)
            rotationProperty.Selected = True
            rotationProperty.SetFocus(True)

        localKeyGroup.SetEnabled (True)
        localKeyGroup.SetActive(True)

        return localKeyGroup

    def aimControlsOnTakeRange(self, splineIkRig):
        self.jumpToTakeFirstFrame()
        progressBar = mobu.FBProgress()
        progressBar.ProgressBegin()

        startTime, stopTime, lPlayer = self.exposeTakeRange()
        ikControlArray = []

        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        inputRigIkConstraint = constraintLink.GetSrc(0)

        for ikControlIndex in range(inputRigIkConstraint.ReferenceGetCount(2)-1):
            ikControl = inputRigIkConstraint.ReferenceGet(2,ikControlIndex)

            ikControlArray.append(ikControl)

        localKeyGroup = self.setAimControlKeyingGroup(ikControlArray)

        for timeIndex in range(startTime, stopTime+1):
            progressBar.Percent = int((timeIndex - startTime) / float(stopTime - startTime) * 100)

            targetTime = mobu.FBTime(0, 0, 0, timeIndex, 0)
            lPlayer.Goto(targetTime)

            self.aimControls(splineIkRig)
            lPlayer.Key()

        progressBar.ProgressDone()
        localKeyGroup.RemoveAllProperties()
        localKeyGroup.FBDelete()

        self.RefreshScene()

    def aimControls(self, splineIkRig):
        ikControlArray = []

        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        inputRigIkConstraint = constraintLink.GetSrc(0)

        for ikControlIndex in range(inputRigIkConstraint.ReferenceGetCount(2)):
            ikControl = inputRigIkConstraint.ReferenceGet(2,ikControlIndex)

            ikControlArray.append(ikControl)

        self.RefreshScene()
        aimDataArray = []
        for ikControlIndex in range(len(ikControlArray)-1):
            aimData = self.constraintUtils.createTargetConstraint(ikControlArray[ikControlIndex],
                                                                  ikControlArray[ikControlIndex+1])
            aimDataArray.append(aimData)

        self.RefreshScene()

        for ikControlIndex in range(len(ikControlArray)-1):
            aimRotation = ikControlArray[ikControlIndex].PropertyList.Find('Rotation (Lcl)').Data

            self.constraintUtils.bakeConstraintValue(aimDataArray[ikControlIndex][0])
            aimDataArray[ikControlIndex][1].FBDelete()
            ikControlArray[ikControlIndex].PropertyList.Find('Rotation (Lcl)').Data = aimRotation

        self.RefreshScene()

    def batchPlotAnimation(self,
                           splineIkRig,
                           takeArray,
                           plotTranslation):
        for take in takeArray:
            mobu.FBSystem().CurrentTake = take
            self.jumpToTakeFirstFrame()

            self.RefreshScene()

            if plotTranslation is False:
                self.plotAnimation(splineIkRig,
                                   False)
            else:
                splineIkRigName = splineIkRig.LongName

                utils = PlotAnimationFactory()
                utils.collectRigRoot(splineIkRigName)
                utils.plotAnimation()

    def consolidateGlueComponents(self,
                                  splineIkRig):
        glueProperty = splineIkRig.PropertyList.Find('gameJointConstraints')

        offsetArray = (mobu.FBVector3d(-90, -90, 0),
                       mobu.FBVector3d(-180, 0, 90),
                       mobu.FBVector3d(180, 0, 0))

        for index in range(glueProperty.GetSrcCount()):
            if index > len(offsetArray)-1:
                continue

            glueConstraint = glueProperty.GetSrc(index)

            sourceParentNode = glueConstraint.ReferenceGet(self.PARENT_NODE_INDEX, 0)
            attributePrefix = sourceParentNode.LongName
            glueConstraint.Lock = False

            for property in glueConstraint.PropertyList:
                if not property.Name.startswith(attributePrefix):
                    continue

                if property.Name.endswith('.Offset R'):
                    property.Data = offsetArray[index]

                if property.Name.endswith('.Offset T'):
                    property.Data = mobu.FBVector3d(0, 0, 0)

            glueConstraint.Lock = True


class ConstraintUtils(object):
    def __init__(self):
        self.PARENT_CHILD = 'Parent/Child'
        self.cstMngr = mobu.FBConstraintManager()
        self.ikSplineConstraintName = 'SplineIk'

    def RefreshScene(self):
        """
            Force Evaluation of current scene .
        """
        mobu.FBSystem().Scene.Evaluate()
        return
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

    def bakeConstraintValue(self, constraint):
        """
            Store TRS channels on object driven by a constraint.

            Args:
                constraint (FBConstraint).
        """
        if constraint != None:
            drivenObject = constraint.ReferenceGet(0, 0)

            constraint.FreezeSRT(drivenObject, True, True, True)
            constraint.Active = False
            constraint.FBDelete()

    def disableConstraints(self, rigControls):
        """
            Disable all constraints driving the list of control provided.

            Args:
                rigControls (list of FBModel): input parent node.

        """
        for control in rigControls:
            constraintArray = self.findConstraints(control)
            if len(constraintArray) < 1 :
                continue

            for constraint in constraintArray:
                constraint.Active = False

    def findConstraints(self, control):
        """
            Find all constraints driving the list of control provided.

            Args:
                rigControls (list of FBModel): input parent node.

            returns (list of FBConstraint).
        """
        drivingConstrains = []
        sourceCount = control.GetSrcCount()

        for sourceIndex in range(sourceCount):
            sourceObject = control.GetSrc(sourceIndex)

            if isinstance(sourceObject, mobu.FBConstraintRelation) is True:
                drivingConstrains.append(sourceObject)

        sceneConstraints = [constraint for constraint in Globals.Scene.Constraints]


        constrainedGroupIndex = 0
        for constraint in sceneConstraints:
            drivenCount = constraint.ReferenceGetCount(constrainedGroupIndex)

            for drivenIndex in range(drivenCount):
                drivenObject = constraint.ReferenceGet(constrainedGroupIndex, drivenIndex)

                if drivenObject is None:
                    continue

                if drivenObject.FullName == control.FullName:
                    drivingConstrains.append(constraint)

                    break

        drivingConstrains = list(set(drivingConstrains))

        return drivingConstrains

    def constraintParts(self, 
                        drivenObject, 
                        targetParent, 
                        constraintType=Constraint.PARENT_CHILD,
                        activateConstraint=True,
                        preserveOffset=False):
        """
            Build list exposing all children of inputNode.

            Args:
                drivenObject (FBModel).
                targetParent (FBModel).

            returns:
                (FBConstraint).
        """
        anchor = Constraint.CreateConstraint(constraintType)
        anchor.ReferenceAdd(0,drivenObject)
        anchor.ReferenceAdd(1,targetParent)

        anchor.Active = False

        if preserveOffset is False:
            if constraintType == Constraint.PARENT_CHILD:
                drivenObject.PropertyList.Find('Rotation Pivot').Data = mobu.FBVector3d()
                drivenObject.PropertyList.Find('Scaling Pivot').Data = mobu.FBVector3d()
                drivenObject.PropertyList.Find('Rotation Offset').Data = mobu.FBVector3d()
        else:
            anchor.Snap()
            anchor.Active = False

        if activateConstraint is False:
            return anchor

        anchor.Active = True
        return anchor

    def alignWithConstraint(self, drivenObject, targetParent, preserveOffset=False, freeze=True):
        parentConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        parentConstraint.ReferenceAdd(0,drivenObject)
        parentConstraint.ReferenceAdd(1,targetParent)

        if preserveOffset is False:
            parentConstraint.Active = True

            drivenObject.PropertyList.Find('Rotation Pivot').Data = mobu.FBVector3d()
            drivenObject.PropertyList.Find('Scaling Pivot').Data = mobu.FBVector3d()
            drivenObject.PropertyList.Find('Rotation Offset').Data = mobu.FBVector3d()

            for attribute in parentConstraint.PropertyList:
                if 'Offset T' in attribute.Name or 'Offset R' in attribute.Name:
                    attribute.Data = mobu.FBVector3d()

            if freeze is True:
                self.RefreshScene()
                parentConstraint.FreezeSRT(drivenObject, True, True, True)

        return parentConstraint

    def layoutTweakerAimRig(self, drivenObject, baseControl, aimControl, reverseAim):
        bindObject = mobu.FBModelNull('{}_bind1'.format(baseControl.Name))
        bindObject.Show = True
        bindObject.Visible = True

        alignConstraint = self.alignWithConstraint(bindObject, baseControl)
        alignConstraint.Active = True
        self.RefreshScene()

        self.bakeConstraintValue(alignConstraint)

        self.RefreshScene()
        bindObject.Parent = baseControl

        targetConstraintData = self.createTargetConstraint(bindObject, aimControl, spaceObject=baseControl, reverseAim=reverseAim)
        targetConstraintData[0].Name = '{}_target1'.format(drivenObject.Name)
        parentConstraint = self.alignWithConstraint(drivenObject, bindObject, preserveOffset=True)

        return bindObject, targetConstraintData[0], parentConstraint

    def validateAimTarget(self,
                          baseControl,
                          aimControl):
        baseVector = mobu.FBVector3d()
        aimVector = mobu.FBVector3d()

        baseControl.GetVector(baseVector)
        aimControl.GetVector(aimVector)

        baseVector = mobu.FBVector4d(baseVector[0], baseVector[1], baseVector[2], 1.0)
        aimVector = mobu.FBVector4d(aimVector[0], aimVector[1], aimVector[2], 1.0)

        pResult =  mobu.FBVector4d()
        mobu.FBSub(pResult, aimVector , baseVector)

        lengthCheck = mobu.FBLength(pResult)

        return False

    def createIkConstraint(self,
                           spline,
                           drivenObject):
        """
            This method builds a splineIk/motionPath constraint
            Args:
                spline (FBModelPath3d): CurveShape use by
                the ikSpline constraint

                drivenObject (FBModel): object we want to attach
        """
        constraintIndex = -1
        for k in range(self.cstMngr.TypeGetCount ()):
            if self.cstMngr.TypeGetName(k) == self.ikSplineConstraintName :
                constraintIndex = k
                ikConstraint = self.cstMngr.TypeCreateConstraint( constraintIndex )
                ikConstraint.Show = True

                splineGroup =  ikConstraint.ReferenceGroupGetName(0)
                ConstrainedGroup = ikConstraint.ReferenceGroupGetName(1)

                ikConstraint.ReferenceAdd(0,spline)
                ikConstraint.PropertyList.Find('Use Percentage').Data = True
                ikConstraint.PropertyList.Find('follow Path').Data = True

                if drivenObject !=None:
                    ikConstraint.ReferenceAdd(1,drivenObject)

                return ikConstraint


        return None

    def createTargetConstraint(self,
                               drivenObject,
                               targetObject,
                               spaceObject=None,
                               reverseAim=False):
        #-----------------------------------------------------C++ group order
        '''
            m_GroupConstrained  = ReferenceGroupAdd("Constrained Object", 1)
            m_GroupTarget = ReferenceGroupAdd("Target Object", 1)
            m_GroupSpace = ReferenceGroupAdd("Space Object", 1)
        '''
        anchor = None
        for k in range(self.cstMngr.TypeGetCount ()):
            if self.cstMngr.TypeGetName(k) == 'target' :
                constraintIndex = k
                anchor = self.cstMngr.TypeCreateConstraint(constraintIndex)

        if spaceObject is None:
            spaceObject = mobu.FBModelNull('spaceLoc1')
            spaceObject.Show = True
            spaceObject.Visible = True

            alignConstraint = self.alignWithConstraint(spaceObject, drivenObject)
            self.bakeConstraintValue(alignConstraint)

        inputArray = [drivenObject,
                      targetObject,
                      spaceObject]

        for inputIndex in range(3):
            anchor.ReferenceAdd(inputIndex, inputArray[inputIndex])

        anchor.Show = True
        anchor.PropertyList.Find('Reverse Aim').Data = reverseAim
        anchor.Active = True

        return [anchor, spaceObject]

    def createTwistConstraint(self,
                              drivenNodeArray,
                              twistTargetObject,
                              spaceObject=None,
                              reverseMulptiplier=1.0,
                              weightArray=None):
        if len(drivenNodeArray) == 0:
            return None

        twistConstraint = None
        for k in range(self.cstMngr.TypeGetCount ()):
            if self.cstMngr.TypeGetName(k) == 'twist' :
                constraintIndex = k
                twistConstraint = self.cstMngr.TypeCreateConstraint(constraintIndex)
                twistConstraint.Show = True

        mobu.FBSystem().Scene.Evaluate()


        inputArray = [twistTargetObject,
                      spaceObject]

        for inputIndex in range(len(inputArray)):
            twistConstraint.ReferenceAdd(inputIndex+1, inputArray[inputIndex])


        for orientNode in drivenNodeArray:
            twistConstraint.ReferenceAdd(0, orientNode)

        twistConstraint.PropertyList.Find('twist Multiplier').Data = reverseMulptiplier
        mobu.FBSystem().Scene.Evaluate()

        if weightArray is None:
            mobu.FBSystem().Scene.Evaluate()

            weightArray = []
            divider = len(drivenNodeArray) - 1
            if divider == 0:
                divider = 1

            weightRatio = 1.0 / float(divider)
            for nodeIndex, orientNode in enumerate(drivenNodeArray):
                weightValue = nodeIndex*weightRatio
                weightProperty = 'twistWeight{0}'.format((nodeIndex+1))

                twistConstraint.PropertyList.Find(weightProperty).Data = weightValue

        twistConstraint.Active = True
        return twistConstraint


class BindDataUtils(object):
    def __init__(self):
        self.constraintUtils = ConstraintUtils()

    def collectCurveparameters(self, splineObject):
        curveMaxRange = splineObject.PathKeyGetCount()

        uvalues = []
        for jointIndex  in range(curveMaxRange):
            uvalues.append(jointIndex*100.0)

        return uvalues

    def collectUparameters(self, splineObject, numberOfCurveController):
        curveMaxRange = splineObject.PathKeyGetCount()-1
        uRatio = 100.0  / float( numberOfCurveController-1)

        uvalues = []
        for jointIndex  in range(numberOfCurveController):
            kPercentSource =  jointIndex*uRatio
            kPercent = splineObject.ConvertTotalPercentToSegmentPercent(kPercentSource)

            uvalues.append(kPercent*100.0)
        return uvalues

    def sampleCurveKnots(self, splineObject):
        curveMaxRange = splineObject.PathKeyGetCount()-1
        subSegmentSamples = 10

        chordRatio = 10.0 / (subSegmentSamples *1.0)
        knotPoints = []
        rollCount = curveMaxRange*subSegmentSamples + 1

        for k in range(rollCount):
            knotPoints.append(0)

        for k in range(curveMaxRange):
            knotIndex = (subSegmentSamples*k)
            for j in range(subSegmentSamples):
                uValueToSample = (knotIndex+j)*chordRatio*0.1 ;
                knotPoints[knotIndex+j] = splineObject.Segment_GlobalPathEvaluate( uValueToSample )

        knotPoints[len(knotPoints)-1] = splineObject.Segment_GlobalPathEvaluate( curveMaxRange ) ;

        return knotPoints

    def sampleCurveUvalues(self, splineObject):
        curveMaxRange = splineObject.PathKeyGetCount()-1
        subSegmentSamples = 10

        chordRatio = 10.0 / (subSegmentSamples *1.0)
        uvalues = []

        for k in range(curveMaxRange):
            knotIndex = (subSegmentSamples*k)
            for j in range(subSegmentSamples):
                uValueToSample = (knotIndex+j)*chordRatio*0.1 ;
                uvalues.append(uValueToSample*100.0)

        uvalues.append(curveMaxRange*100.0)

        return uvalues

    def extractUvaluesFromChain(self,
                                splineObject,
                                jointObjectArray):
        knotPoints = self.sampleCurveKnots(splineObject)
        segmentData = self.collectChainSegments(jointObjectArray)

        #Apply chain solver to get the true chain constraint
        solverFn = ikCSolver.Chain()

        solverState = False
        solverFn.debugSolver = solverState
        solverFn.debugSubSegment = False

        curveAttributes = solverFn.build(splineIkPathName='',
                                         knotPoints=knotPoints,
                                         segmentData=segmentData,
                                         splinePath=splineObject,
                                         drawShape=False,
                                         sampleSubSegment=True)


        return curveAttributes

    def RefreshScene(self):
        """
            Force Evaluation of current scene .
        """
        mobu.FBSystem().Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

    def extractJointOffsets(self,
                            splineObject,
                            jointObjectArray,
                            chainParameters,
                            aimToNextJoint,
                            reverseChain=False):
        ikConstraint = self.constraintUtils.createIkConstraint(splineObject,
                                                               None)

        mobu.FBSystem().Scene.Evaluate()
        ikConstraint.PropertyList.Find('Use Percentage').Data = False

        ikConstraint.PropertyList.Find("applyIK_Chain_rules").Data = True
        ikConstraint.PropertyList.Find("Reverse Chain").Data = reverseChain

        mobu.FBSystem().Scene.Evaluate()

        jointMatrix = mobu.FBMatrix()
        neutralMatrix = mobu.FBMatrix()
        mobu.FBSystem().Scene.Evaluate()

        #Compute Offsets
        uValueArray = []
        positionOffsets = []
        rotationOffsets = []
        constraintData = []
        offsetLocArray = []

        localMatrix = mobu.FBMatrix()
        numberOfJoints = len(jointObjectArray)

        for jointIndex , joint in enumerate(jointObjectArray):
            offsetLoc = mobu.FBModelNull('offsetBuffer1')
            offsetLoc.Show = True
            offsetLoc.Visible = True
            offsetLocArray.append(offsetLoc)

        mobu.FBSystem().Scene.Evaluate()
        for jointIndex , joint in enumerate(jointObjectArray):
            ikConstraint.ReferenceAdd(1, offsetLocArray[jointIndex])

        self.RefreshScene()
        ikConstraint.Active  = True
        self.RefreshScene()

        breakIndex = -1
        #-----------------------------------------------------------------Write chainSegments
        for chainIndex in range(len(chainParameters)):
            chainLengthHandle = ikConstraint.PropertyList.Find('chainLength{0}'.format((chainIndex+1)))

            if chainParameters[chainIndex] < 0.01:
                breakIndex = chainIndex
                continue

            chainLengthHandle.Data = chainParameters[chainIndex]

        self.RefreshScene()

        ikConstraint.PropertyList.Find("Aim To Next Joint").Data = aimToNextJoint
        self.RefreshScene()

        for jointIndex , joint in enumerate(jointObjectArray):
            #Dont forget the ui slider is 0 to 100.0 based range so uvalue 7.0 will be 700.0
            offsetLocal= mobu.FBModelNull('{0}_offsetLocal'.format(joint.Name))
            offsetLocal.Show = True
            offsetLocal.Visible = True

            knot= mobu.FBModelNull('{0}_knot'.format(joint.Name))
            knot.Show = True
            knot.Visible = True

            uValueArray.append(ikConstraint.PropertyList.Find('chainValue{0}'.format(jointIndex+1)).Data)

            offsetLocal.Parent = knot
            alignConstraint = self.constraintUtils.alignWithConstraint(knot, offsetLocArray[jointIndex])
            offsetCheckConstraint = self.constraintUtils.alignWithConstraint(knot, offsetLocArray[jointIndex])
            offsetLocalConstraint = self.constraintUtils.alignWithConstraint(offsetLocal, joint)

            mobu.FBSystem().Scene.Evaluate()

            for constraint in [offsetCheckConstraint, alignConstraint, offsetLocalConstraint]:
                self.constraintUtils.bakeConstraintValue(constraint)

            positionOffsets.append(offsetLocal.PropertyList.Find('Translation (Lcl)').Data)
            rotationOffsets.append(offsetLocal.PropertyList.Find('Rotation (Lcl)').Data)

            for model in [offsetLocal, knot]:
                model.FBDelete()
            mobu.FBSystem().Scene.Evaluate()

        #Clean UP temporary elelements
        self.RefreshScene()

        ikConstraint.Active = False
        ikConstraint.FBDelete()

        for loc in offsetLocArray:
            loc.FBDelete()

        self.RefreshScene()
        return {'positionOffset':positionOffsets,
                'rotationOffset':rotationOffsets,
                'uValueArray':uValueArray}

    def collectChainSegments(self,
                             jointObjectArray):
        """
            This method exposes chain length data
            Args:
                jointObjectArray (modelList): the object list
                containing our joint chain

            return dict
        """
        chainLength = 0.0

        jointMatrix = mobu.FBMatrix()
        jointPositionList = []

        #Collect worldMatrices of our jointChain
        for jointObject in jointObjectArray:
            mobu.FBSystem().Scene.Evaluate()
            jointPosition = mobu.FBVector4d()

            jointObject.GetMatrix(jointMatrix,
                                  mobu.FBModelTransformationType.kModelTransformation,
                                  True )

            mobu.FBMatrixToTranslation(jointPosition,jointMatrix)
            jointPositionList.append(jointPosition)

        chainSegments = []
        segmentLength = 0.0
        for k in xrange(len(jointObjectArray)-1):
            segmentLength = (jointPositionList[k+1] - jointPositionList[k]).Length()
            if segmentLength < 0.00001:
                segmentLength = 0.0

            chainLength += segmentLength
            chainSegments.append(segmentLength)

        return {'chainLength':chainLength,
                'chainSegments':chainSegments}

    def colorizeObject(self, objectToColor, color):
        """
        colorize an object
        """
        colorProperty = objectToColor.PropertyList.Find('Color RGB')
        if colorProperty is None:
            colorProperty = objectToColor.PropertyList.Find('Curve Color')
        newColour = mobu.FBColor(color)
        colorProperty.Data = newColour

    def createPathFromJoints(self,
                             jointObjectArray,
                             name='splineIkPath1',
                             numberOfCurveController=3,
                             deleteJointPath=True,
                             excludeThreshold=0.001,
                             debugDistance=False,
                             skipCurveRebuilding=False,
                             reverseCurve=False):
        """
            Method create a curve path for a list of joints
            Args:
                jointObjectArray (modelList): the object list we want
                to drive

                name                    (str): Name of the computed curve
                numberOfCurveController (int): parameter to resample the path

            return type FBModelPath3d
        """
        if not isinstance( jointObjectArray,mobu.FBModelList):
            raise ValueError('Invalid jointObjectArray  : you must provide a FBModelList' )
            return None

        if reverseCurve is True:
            jointList = []
            for jointIndex, joint in enumerate(jointObjectArray):
                jointList.append(joint)

            jointList.reverse()
            for jointIndex, joint in enumerate(jointList):
                jointObjectArray[jointIndex] = joint

        curveName = '{0}_HighRes'.format(str(name))
        controlPath_HIGH = mobu.FBModelPath3D(curveName)
        controlPath_HIGH.Show = True
        controlPath_HIGH.Visible = True

        jointMatrix = mobu.FBMatrix()
        CV_position = mobu.FBVector4d()

        position = mobu.FBVector3d()
        positionOld = mobu.FBVector3d()

        #clean mobu embarrsing default shape with two points...
        controlPath_HIGH.PathKeyClear()

        self.RefreshScene()
        #Create a CV_Point for each joint
        for jointIndex , joint in enumerate(jointObjectArray):
            addCv = True
            jointObjectArray[jointIndex].GetVector(position)
            CV_position[0] = position[0]
            CV_position[1] = position[1]
            CV_position[2] = position[2]

            if jointIndex > 0:
                jointObjectArray[jointIndex-1].GetVector(positionOld)

                CV_position[0] = position[0]
                CV_position[1] = position[1]
                CV_position[2] = position[2]

                segment = (position - positionOld)
                segmentLength = segment.Length()

                if segmentLength < excludeThreshold:
                    addCv = False

            if addCv is True:
                if jointIndex == 0:
                    controlPath_HIGH.PathKeyStartAdd(CV_position)
                else:
                    controlPath_HIGH.PathKeyEndAdd(CV_position)

        if numberOfCurveController<2:
            numberOfCurveController=2

        if skipCurveRebuilding is True:
            numberOfCurveController = jointObjectArray.count()

        uRatio = 100.0  / float( numberOfCurveController-1)

        controlPath = mobu.FBModelPath3D(name)
        controlPath.Show = True
        controlPath.Visible = True

        #clean mobu embarrsing default shape with two points...
        controlPath.PathKeyClear()

        #OrienCurve here
        #@---
        FirstCv = mobu.FBVector3d()
        SecondCv = mobu.FBVector3d()

        jointObjectArray[0].GetVector(FirstCv)
        jointObjectArray[1].GetVector(SecondCv)

        upPos = mobu.FBModelNull('upPos')
        upPos.Show = True

        basePos = mobu.FBModelNull('basePos')
        basePos.Show = True

        targetPos = mobu.FBModelNull('targetPos')
        targetPos.Show = True

        upPos.Parent = basePos
        upPos.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0,10.0,0)
        basePos.SetVector(FirstCv)
        targetPos.SetVector(SecondCv)
        self.RefreshScene()

        aimConstraint = Constraint.CreateConstraint(Constraint.AIM)

        aimConstraint.ReferenceAdd (0, controlPath)
        aimConstraint.ReferenceAdd (1, targetPos)
        aimConstraint.ReferenceAdd (2, upPos)

        aimConstraint.PropertyList.Find('World Up Type').Data = 1

        posConstraint = Constraint.CreateConstraint(Constraint.POSITION)
        posConstraint.ReferenceAdd (0, controlPath)
        posConstraint.ReferenceAdd (1, basePos)

        self.RefreshScene()
        posConstraint.Active = True
        aimConstraint.Active = True

        self.RefreshScene()
        self.constraintUtils.bakeConstraintValue(posConstraint)
        self.constraintUtils.bakeConstraintValue(aimConstraint)

        self.RefreshScene()
        curveMatrix = mobu.FBMatrix()
        curveSpace =  controlPath.GetMatrix(curveMatrix)

        loc = mobu.FBModelNull('locator1')
        loc.Show = True

        self.RefreshScene()
        ikConstraint = self.constraintUtils.createIkConstraint(controlPath_HIGH,
                                                               loc)

        ikConstraint.Active  = True
        uValueHandle = ikConstraint.PropertyList.Find('uValue1')

        positionInCurveSpace = mobu.FBVector4d()
        curveMatrix.Inverse()
        #Now resample the curve to the target numberOfCurveController
        for jointIndex  in range(numberOfCurveController):
            kPercentSource =  jointIndex*uRatio
            kPercent = controlPath_HIGH.ConvertTotalPercentToSegmentPercent(kPercentSource)

            if skipCurveRebuilding is True:
                kPercent = float(jointIndex)

            uValueHandle.Data = kPercent*100.0
            mobu.FBSystem().Scene.Evaluate()

            loc.GetVector(position)

            CV_position[0] = position[0]
            CV_position[1] = position[1]
            CV_position[2] = position[2]

            mobu.FBVectorMatrixMult(positionInCurveSpace, curveMatrix, CV_position)
            if jointIndex == 0:
                controlPath.PathKeyStartAdd(positionInCurveSpace)
            else:
                controlPath.PathKeyEndAdd(positionInCurveSpace)

        #Clean up unused elements
        ikConstraint.Active = False
        ikConstraint.FBDelete()

        if deleteJointPath is True:
            controlPath_HIGH.FBDelete()

        for model in [basePos, targetPos, loc, upPos]:
            model.FBDelete()

        #Adjust collor a selectionMode
        self.colorizeObject(controlPath,(255,255,0))
        controlPath.PropertyList.Find('Enable Selection').Data = 0
        controlPath.PropertyList.Find('Enable Transformation').Data = 0

        self.RefreshScene()
        return controlPath

