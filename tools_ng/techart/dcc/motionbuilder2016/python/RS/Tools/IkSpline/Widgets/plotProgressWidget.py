from PySide import QtGui, QtCore

from RS.Tools.UI import Application
from RS.Utils.Logging import const as loggingConst


class PeltProgressWindowDialog(QtGui.QMainWindow):
    '''
    Dialogue to run ProgressWindow and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=Application.GetMainWindow()):
        '''
        Constructor
        '''
        super(PeltProgressWindowDialog, self).__init__(parent=parent)

        # set dialog to center the screen
        #dialogGeometry.moveCenter(QtGui.QApplication.desktop().availableGeometry().center())

        # remove frame and titlebar/buttons from dialog
        self.setWindowFlags(QtCore.Qt.Tool | QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)

        # create layout and add widget
        self._progressWindow = PeltProgressWindow()
        self.setCentralWidget(self._progressWindow)

    def AddText(self, text, textcolor=loggingConst.MessageTextColours.Default):
        self._progressWindow.AddText(text, textcolor)

    def _forceDrawOverride(self):
        self.resize(500, 65)
        self.updateGeometry()


class PeltProgressWindow(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PeltProgressWindow, self).__init__(parent=parent)
        self._setupUi()

    def _setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        layout = QtGui.QHBoxLayout()

        # create widgets
        self.console = QtGui.QTextEdit()

        # widget properties
        self.console.setReadOnly(True)
        self.console.setFrameStyle(QtGui.QFrame.NoFrame)
        self.console.setFontPointSize(10)

        # set widgets to layout
        layout.addWidget(self.console)

        # set layout
        self.setLayout(layout)

    def hide(self):
        self.console.clear()
        super(ProgressWindow, self).hide()

    def AddText(self, text, textColor):
        self.console.setTextColor(QtGui.QColor(textColor))
        self.console.setText('\n\n{0}'.format(text))
        self.repaint()

