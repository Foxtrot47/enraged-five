import json
import os

from functools import partial
import pyfbsdk as mobu

from RS import Globals
from RS.Tools import UI

from RS.Tools.IkSpline.Widgets import motionCurveWidget
from RS.Tools.IkSpline.Widgets import ikSplinePlotAnimationWidget
from RS.Tools.IkSpline.Widgets import ikSplineSettingsWidget
from RS.Tools.IkSpline.Widgets import rebuildIkSplineWidget
from RS.Tools.IkSpline.Widgets import ikRigWidget
from RS.Tools.IkSpline.Widgets import tabWidget

from RS.Tools.IkSpline import Core as  IkSplineCore
from RS.Tools.IkSpline import Utils as ikUtils


from PySide import QtGui, QtCore


class IkSplineTool(UI.QtMainWindowBase):
    """
        main Tool UI
    """
    def __init__(self):
        """
            Constructor
        """
        self._toolName = "Dynamic spline Rig v{0}".format('1.2.16')

        # Main window settings
        super(IkSplineTool, self).__init__(title=self._toolName, 
                                           dockable=True)
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()

        # Create Widgets 
        self.mainWidget = QtGui.QWidget()
        banner = UI.BannerWidget(self._toolName)
        self.rootWidget = ikRigWidget.IkRigWidget(self)

        self.plotWidget = ikSplinePlotAnimationWidget.PlotAnimationWidget(self)

        plotAnimationTool = tabWidget.ToolData(self.plotWidget, 
                                               'Plot animation')

        self.rebuildWidget = rebuildIkSplineWidget.RebuildIkSplineWidget(self)

        rebuildTool = tabWidget.ToolData(self.rebuildWidget, 
                                         'Rebuild')

        self.settingWidget = ikSplineSettingsWidget.IkSplineSettingsWidget(self)
        curveSettings = tabWidget.ToolData(self.settingWidget, 
                                          'Settings')

        motionCurveTool = tabWidget.ToolData(motionCurveWidget.MotionCurveWidget(self), 
                                             'MotionCurve')

        toolUI = tabWidget.ShelvesTabWidget()
        toolUI._appendToTabWidget(plotAnimationTool)
        toolUI._appendToTabWidget(motionCurveTool)
        toolUI._appendToTabWidget(curveSettings)
        toolUI._appendToTabWidget(rebuildTool)

        # Set properties

        # Assign Widgets to layouts 
        mainLayout.addWidget(banner)
        mainLayout.addWidget(self.rootWidget)
        mainLayout.addWidget(toolUI)
        mainLayout.addStretch(1)

        # Add widgets to Layouts
        self.mainWidget.setLayout(mainLayout)

        # Assign Layouts
        self.setCentralWidget(self.mainWidget)


def Run(show=True):
    tool = IkSplineTool()

    if show: 
        tool.show()  
    return tool
