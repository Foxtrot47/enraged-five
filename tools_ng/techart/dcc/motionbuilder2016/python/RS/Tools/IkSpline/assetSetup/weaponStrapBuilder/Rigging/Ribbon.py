import inspect
import json
import math
import os
import tempfile

import pyfbsdk as mobu
from RS import Globals
from RS.Utils import Scene
from RS.Utils.Scene import RelationshipConstraintManager as relationConstraintManager
from RS.Utils import Namespace
from RS.Utils.Scene import Constraint

from RS.Tools.IkSpline.assetSetup.weaponStrapBuilder.Rigging import Chain
from RS.Tools.IkSpline.assetSetup.weaponStrapBuilder.Rigging import Components

reload(Chain)
reload(Components)


class StrapHull(object):
    DUMMY_NAME = 'Dummy01'

    START_SEARCH_INDEX = 0

    HORIZONTAL_OFFSET = 380

    SHIFT_COMPONENT_OFFSET = 150

    TWEAKERS_METADATA_NAME = 'tweakerControls'

    IK_CONTROLS_METADATA_NAME = 'ikControls'

    CONSTRAINT_FOLDER_METADATA_NAME = 'constraint_folder'

    ZERO_GROUP_METADATA_NAME = 'zero_groups'

    ANCHOR_METADATA_NAME = 'anchors'

    ANCHOR_CONSTRAINT_METADATA_NAME = 'anchorConstraint'

    OFFSET_METADATA_NAME = 'offsets'

    JOINTS_METADATA_NAME = 'joints'

    JOINTS_MISC_METADATA_NAME = 'skelRootModelArray'

    CONSTRAINT_FOLDER_NAME = 'Constraints 1'

    HULL_CONSTRAINT_FOLDER_NAME = 'HullConstraints1'

    TARGET_CONSTRAINT_FOLDER_NAME = 'TargetConstraints1'

    ATTACH_CONSTRAINT_FOLDER_NAME = 'AttachConstraints1'

    CORNER_CONSTRAINT_FOLDER_NAME = 'CornerConstraints1'

    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    WEIGHT_PROPERTY_SUFFIX = 'Weight'

    def __init__(self):
        self.markers = []

        self.pivotMarker = None

        self.leftHandleMarker = None

        self.rightHandleMarker = None

        self.strapJointArray = []

        self.lowerHull = Components.ArcHull()

        self.upperHull = Components.ArcHull()

        self.END_SEARCH_INDEX = -1

        self.markerSize = 240.0

        self.tweakerSize = 0.825

        self.tweakerArray = []

        self.targetArray = []

        self.tweakerControlGroup = None

        self.animationControlGroup = None

        self.strapRoot = None

        self.motionpathData = []

        self.relationConstraint = None

        self.splineIkRigRootName = 'strapIkPath1_constraint_Rig'

        self.tweakerRootProperty = None

        self.hullConstraintFolder = None

        self.attachFolder = None

        self.constraintFolder = None

        self.targetConstraints = []

        self.attachConstraints = []

        self.cornerHandleConstraints = []

        self.suspendHandles = []

    def createChainSegments(self, splitChainIndex=4,
                            debugHullRegion=False):
        self.END_SEARCH_INDEX = (len(self.strapJointArray)-1)
        
        self.lowerHull.collectComponents(self.START_SEARCH_INDEX, 
                                         2, 
                                         splitChainIndex,
                                         self.strapJointArray)

        self.upperHull.collectComponents(splitChainIndex, 
                                         6, 
                                         self.END_SEARCH_INDEX,
                                         self.strapJointArray)

        if debugHullRegion is False:
            return

        self.lowerHull.drawHull()

        self.upperHull.drawHull()

    def prepareBindData(self):
        self.lowerHull.extractWeights()

        self.upperHull.extractWeights()

    def colorizeObject(self, objectToColor, color):
        """
        colorize an object
        """
        colorProperty = objectToColor.PropertyList.Find('Color RGB')
        if colorProperty is None:
            colorProperty = objectToColor.PropertyList.Find('Curve Color')
        newColour = mobu.FBColor(color)
        colorProperty.Data = newColour

    def createControl(self,
                      name,
                      type,
                      size):
        marker = mobu.FBModelMarker(name)
        zroGroup = mobu.FBModelNull('{0}_zro1'.format(name))

        if type=='IK_Control':
            marker.PropertyList.Find('Look').Data = 1
            self.colorizeObject(marker,(0,1.0,0.0))
        elif type=='UpVector':
            marker.PropertyList.Find('Look').Data = 3
            self.colorizeObject(marker,(0,0.0,1.0))
        elif type=='Tweaker':
            marker.PropertyList.Find('Look').Data = 8
            self.colorizeObject(marker,(255,0,0))

        marker.Show = True
        marker.Size = size

        marker.Parent = zroGroup

        return marker, zroGroup

    def createControllers(self,
                          controlName,
                          alignRotation,
                          targetPosition,
                          childArray):
        control, zroGroup = self.createControl(controlName, 
                                               'IK_Control', 
                                               self.markerSize)

        mobu.FBSystem().Scene.Evaluate()
        zroGroup.Rotation = alignRotation
        zroGroup.Translation = targetPosition

        mobu.FBSystem().Scene.Evaluate()

        for child in childArray:
            inputMatrix = mobu.FBMatrix()
            child.GetMatrix(inputMatrix)
            child.Parent = control
            child.SetMatrix(inputMatrix)

            mobu.FBSystem().Scene.Evaluate()

        return control

    def createTweakerLayer(self):
        mobu.FBSystem().Scene.Evaluate()

        self.tweakerControlGroup = mobu.FBModelNull('tweakerGroup1')

        for joint in self.strapJointArray:
            control, zroGroup = self.createControl('CTRL_{0}_Tweaker1'.format(joint.Name), 
                                                   'Tweaker', 
                                                   self.tweakerSize)
            inputMatrix = mobu.FBMatrix()
            joint.GetMatrix(inputMatrix)
            zroGroup.Parent = self.tweakerControlGroup
            zroGroup.SetMatrix(inputMatrix)

            self.tweakerArray.append(control)

            self.lowerHull.tweakerArray.append(control)

            self.upperHull.tweakerArray.append(control)

            pathData = Components.MotionpathComponent()

            pathData.zeroGroup = zroGroup
            pathData.controlNode = control
            pathData.jointNode = joint

            self.motionpathData.append(pathData)

        mobu.FBSystem().Scene.Evaluate()
 
    def collectHandlePoints(self,
                            inputHandles):
        handlePoints = []

        for handle in inputHandles:
            handlePoint = mobu.FBVector3d()
            handle.GetVector(handlePoint)

            handlePoints.append(handlePoint)

        return handlePoints

    def extractSuspenderSide(self):
        linePoints = self.collectHandlePoints([self.lowerHull.handleArray[3]])
        linePoints.append(self.upperHull.pointPositions[3])

        handlePoints = self.collectHandlePoints(self.suspendHandles)

        startPoint = linePoints[0]

        endPoint = linePoints[1]

        lineVector = endPoint-startPoint

        pointDataArray = []

        for pivotPoint in handlePoints:
            angleVector = pivotPoint-startPoint

            tangentPivot = self.lowerHull.projectPivotOnTangentLine(startPoint,
                                                                    lineVector,
                                                                    angleVector)

            pointData = Chain.SuspenderWeights(tangentPivot,
                                               pivotPoint,
                                               startPoint,
                                               endPoint)

            pointDataArray.append(pointData)

        return pointDataArray

    def collectWeightAttributes(self,
                                inputConstraint):
        if inputConstraint is None:
            return None

        targetCount = inputConstraint.ReferenceGetCount(self.PARENT_NODE_INDEX)
        if targetCount ==0:
            return None

        weightProperties = []
        for targetIndex in xrange(targetCount):
            targetNode = inputConstraint.ReferenceGet(self.PARENT_NODE_INDEX,
                                                      targetIndex)

            labelName = targetNode.LongName

            spaceLabel = '{0}.{1}'.format(labelName,
                                          self.WEIGHT_PROPERTY_SUFFIX)

            for component in inputConstraint.PropertyList:
                if str(component.Name) != spaceLabel:
                    continue

                weightProperty = component

                weightProperties.append(weightProperty)

        return weightProperties

    def attachSuspenderHandlesToIkControl(self):
        pointDataArray = self.extractSuspenderSide()

        startHandle = self.lowerHull.handleArray[3]
        endHandle = self.upperHull.handleArray[3]

        for handleIndex, handle in enumerate(self.suspendHandles):
            constraint = self.createAttachConstraint(handle,
                                                     startHandle,
                                                     preserveOffset=True)

            constraint.Active = False

            constraint.ReferenceAdd(self.PARENT_NODE_INDEX, 
                                    endHandle)

            mobu.FBSystem().Scene.Evaluate()

            weightProperties = self.collectWeightAttributes(constraint)

            weightProperties[0].Data = pointDataArray[handleIndex].lineWeights[0] * 100.0

            weightProperties[1].Data = pointDataArray[handleIndex].lineWeights[1] * 100.0

            constraint.Snap()

            constraint.Active = True

            self.hullConstraintFolder.ConnectSrc(constraint)

    def createControlLayer(self):
        self.animationControlGroup = mobu.FBModelNull('ikControlGroup1')

        alignMatrixArray = []
        for joint in (self.strapJointArray[0],
                      self.strapJointArray[2],
                      self.strapJointArray[-1]):
            alignMatrix = mobu.FBMatrix()
            joint.GetMatrix(alignMatrix)

            nodeRotation = mobu.FBVector3d()
            mobu.FBMatrixToRotation(nodeRotation, 
                                    alignMatrix, 
                                    mobu.FBRotationOrder.kFBXYZ)

            alignMatrixArray.append(nodeRotation)

        self.suspendHandles = [self.lowerHull.handleArray[4],
                               self.upperHull.handleArray[0],
                               self.upperHull.handleArray[1],
                               self.upperHull.handleArray[2]]

        self.leftHandleMarker = self.createControllers('CTRL_strapLeftHandle1',
                                                        alignMatrixArray[0],
                                                        self.lowerHull.pointPositions[0],
                                                       [self.lowerHull.handleArray[0]])

        self.pivotMarker = self.createControllers('CTRL_strapTangent1',
                                                  alignMatrixArray[1],
                                                  self.lowerHull.pointPositions[2],
                                                  (self.lowerHull.handleArray[1],
                                                   self.lowerHull.handleArray[2],
                                                   self.lowerHull.handleArray[3]))

        rightHandleComponents = [self.upperHull.handleArray[3],
                                 self.upperHull.handleArray[4]]

        rightHandleComponents.extend(self.suspendHandles)

        self.rightHandleMarker = self.createControllers('CTRL_strapRightHandle1',
                                                        alignMatrixArray[2],
                                                        self.upperHull.pointPositions[-1],
                                                        rightHandleComponents)

        self.attachSuspenderHandlesToIkControl()

        for control in (self.pivotMarker,
                        self.leftHandleMarker,
                        self.rightHandleMarker):
            control.Parent.Parent = self.animationControlGroup

        self.createCornerAnchors()

    def createHullLayer(self):
        self.lowerHull.buildRig('lowerHullConstraint1')

        self.upperHull.buildRig('upperHullConstraint1',
                                skipJointIndex=1)

        self.hullConstraintFolder = mobu.FBFolder(self.HULL_CONSTRAINT_FOLDER_NAME,
                                                  self.lowerHull.relationConstraint.GetConstraint())

        self.hullConstraintFolder.ConnectSrc(self.upperHull.relationConstraint.GetConstraint())

    def createRibbonLayer(self):
        mobu.FBSystem().Scene.Evaluate()

        knotLimit = len(self.tweakerArray)-1
        knotType = 'anchor1'

        inConstraintFolder = None
        for tweakerIndex in xrange(knotLimit):
            if tweakerIndex == 0:
                knotData = Components.CurveKnot(self.strapJointArray[tweakerIndex],
                                                self.tweakerArray[tweakerIndex+1].Parent,
                                                self.tweakerArray[tweakerIndex].Parent,
                                                self.tweakerArray[tweakerIndex].Parent,
                                                knotType=knotType)

                inConstraintFolder = knotData.constraintFolder
            else:
                knotData = Components.CurveKnot(self.strapJointArray[tweakerIndex],
                                                self.tweakerArray[tweakerIndex+1].Parent,
                                                self.tweakerArray[tweakerIndex].Parent,
                                                self.tweakerArray[tweakerIndex].Parent,
                                                knotType=knotType,
                                                inConstraintFolder=inConstraintFolder)

            self.motionpathData[tweakerIndex].anchorNode = knotData.drivenNode

            self.targetArray.append(knotData)
            mobu.FBSystem().Scene.Evaluate()

        knotData = Components.CurveKnot(self.strapJointArray[knotLimit],
                                        self.tweakerArray[knotLimit-1].Parent,
                                        self.targetArray[knotLimit-1].drivenNode,
                                        self.tweakerArray[knotLimit].Parent,
                                        knotType=knotType,
                                        reverseAim=True,
                                        inConstraintFolder=inConstraintFolder)

        self.targetConstraints.append(inConstraintFolder)
        self.targetArray.append(knotData)
        self.motionpathData[knotLimit].anchorNode = knotData.drivenNode

        mobu.FBSystem().Scene.Evaluate()

        for tweakerIndex in xrange(len(self.tweakerArray)):
            inputMatrix = mobu.FBMatrix()
            self.tweakerArray[tweakerIndex].GetMatrix(inputMatrix)
            self.tweakerArray[tweakerIndex].Parent = self.targetArray[tweakerIndex].drivenNode
            self.tweakerArray[tweakerIndex].SetMatrix(inputMatrix)

            self.tweakerArray[tweakerIndex].PropertyList.Find('Rotation Offset').Data = mobu.FBVector3d() 
            self.tweakerArray[tweakerIndex].PropertyList.Find('Rotation Pivot').Data = mobu.FBVector3d() 
            mobu.FBSystem().Scene.Evaluate()
            self.tweakerArray[tweakerIndex].PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0) 

            mobu.FBSystem().Scene.Evaluate()

    def createTargetLayer(self):
        mobu.FBSystem().Scene.Evaluate()

        knotLimit = len(self.tweakerArray)-1
        knotType = 'offset1'

        inConstraintFolder = None
        for tweakerIndex in xrange(knotLimit):
            knotData = None
            if tweakerIndex == 0:
                knotData = Components.CurveKnot(self.strapJointArray[tweakerIndex],
                                                self.tweakerArray[tweakerIndex+1],
                                                self.tweakerArray[tweakerIndex],
                                                self.tweakerArray[tweakerIndex],
                                                knotType=knotType)

                inConstraintFolder = knotData.constraintFolder
            else:
                knotData = Components.CurveKnot(self.strapJointArray[tweakerIndex],
                                                self.tweakerArray[tweakerIndex+1],
                                                self.tweakerArray[tweakerIndex],
                                                self.tweakerArray[tweakerIndex],
                                                knotType=knotType,
                                                inConstraintFolder=inConstraintFolder)

            self.motionpathData[tweakerIndex].offsetNode = knotData.drivenNode
            self.targetArray.append(knotData)
            mobu.FBSystem().Scene.Evaluate()

        knotData = Components.CurveKnot(self.strapJointArray[knotLimit],
                                        self.tweakerArray[knotLimit-1],
                                        self.targetArray[knotLimit-1].drivenNode,
                                        self.tweakerArray[knotLimit],
                                        knotType=knotType,
                                        reverseAim=True,
                                        inConstraintFolder=inConstraintFolder)
 
        self.targetConstraints.append(inConstraintFolder)

        self.motionpathData[knotLimit].offsetNode = knotData.drivenNode

    def setControlRenderType(self):
        self.markers = [self.pivotMarker,
                          self.leftHandleMarker,
                          self.rightHandleMarker]

        for pointData in self.markers:
            pointData.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire

    def createGlueLayerWithRelationConstraint(self):
        constraintPrefix = 'strapBindConstraint1'
        relationConstraintName = '{}_Relation'.format(constraintPrefix)
        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)
        self.relationConstraint.SetActive(True)

        increment = 0
        for pointDataIndex, pointData in enumerate(self.motionpathData):
            position = (-100,increment*self.SHIFT_COMPONENT_OFFSET)

            sender = self.relationConstraint.AddSenderBox(pointData.offsetNode)

            receiver = self.relationConstraint.AddReceiverBox(pointData.jointNode)

            self.relationConstraint.ConnectBoxes(sender, 
                                                 'Translation', 
                                                 receiver, 
                                                 'Translation')

            self.relationConstraint.ConnectBoxes(sender, 
                                                 'Rotation', 
                                                 receiver, 
                                                 'Rotation')

            sender.SetBoxPosition(position[0],
                                  position[1])

            receiver.SetBoxPosition(position[0]+self.HORIZONTAL_OFFSET,
                                    position[1])

            increment+=1

        self.hullConstraintFolder.ConnectSrc(self.relationConstraint.GetConstraint())

    def createAttachConstraint(self,
                               drivenNode,
                               parentNode,
                               preserveOffset=False):
        pointConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

        pointConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX, 
                                     drivenNode)

        pointConstraint.ReferenceAdd(self.PARENT_NODE_INDEX, 
                                     parentNode)

        if preserveOffset is True:
            pointConstraint.Snap()

        pointConstraint.Active = True

        pointConstraint.Name = '{0}_attach1'.format(drivenNode.Name)

        return pointConstraint

    def createGlueLayer(self):
        for pointDataIndex, pointData in enumerate(self.motionpathData):
            parentConstraint = self.createAttachConstraint(pointData.jointNode,
                                                           pointData.offsetNode)

            if pointDataIndex ==0:
                self.attachFolder = mobu.FBFolder(self.ATTACH_CONSTRAINT_FOLDER_NAME,
                                                  parentConstraint)
            else:
                self.attachFolder.ConnectSrc(parentConstraint)

            self.attachConstraints.append(parentConstraint)

    def createMetaRoot(self, splineIkRig):
        root = splineIkRig.PropertyCreate('ikSplineMetaRoot', 
                                          mobu.FBPropertyType.kFBPT_charptr, 
                                          'String',
                                          False,
                                          True,
                                          None)

        root.Data = 'strapRibbon'

        rigProperties = (self.TWEAKERS_METADATA_NAME,
                         self.IK_CONTROLS_METADATA_NAME,
                         self.CONSTRAINT_FOLDER_METADATA_NAME,
                         self.ZERO_GROUP_METADATA_NAME,
                         self.ANCHOR_METADATA_NAME,
                         self.OFFSET_METADATA_NAME,
                         self.JOINTS_METADATA_NAME,
                         self.JOINTS_MISC_METADATA_NAME,
                         'ikConstraint',
                         self.ANCHOR_CONSTRAINT_METADATA_NAME,
                         self.CORNER_CONSTRAINT_FOLDER_NAME)

        for attribute in rigProperties:
            splineIkRig.PropertyCreate(attribute,
                                       mobu.FBPropertyType.kFBPT_object,
                                       'Object',
                                       False,
                                       True,
                                       None)

    def createRoot(self):
        self.strapRoot = mobu.FBModelNull(self.splineIkRigRootName)

        self.tweakerControlGroup.Parent = self.strapRoot

        self.animationControlGroup.Parent = self.strapRoot

        self.createMetaRoot(self.strapRoot)

    def connectMetaData(self,
                        inputObject,
                        objectSetName):
        metaRootProperty = self.strapRoot.PropertyList.Find(objectSetName)

        if metaRootProperty is None:
            return

        inputObject.ConnectDst(metaRootProperty)

    def finalizeRig(self):
        for pointData in self.motionpathData:
            self.connectMetaData(pointData.controlNode,
                                 self.TWEAKERS_METADATA_NAME)

            self.connectMetaData(pointData.anchorNode,
                                 self.ANCHOR_METADATA_NAME)

            self.connectMetaData(pointData.offsetNode,
                                 self.OFFSET_METADATA_NAME)

            self.connectMetaData(pointData.jointNode,
                                 self.JOINTS_METADATA_NAME)

            self.connectMetaData(pointData.zeroGroup,
                                 self.ZERO_GROUP_METADATA_NAME)

        for marker in self.markers:
            self.connectMetaData(marker,
                                 self.IK_CONTROLS_METADATA_NAME)

        self.createConstraintFolder()

        self.connectMetaData(self.upperHull.relationConstraint.GetConstraint(),
                             'ikConstraint')

        rigGroup = Chain.GetMainGroup()

        ikGroup = mobu.FBGroup('Ik Controls')
        tweakerGroup = mobu.FBGroup('Tweakers')

        rigGroup.ConnectSrc(ikGroup)

        rigGroup.ConnectSrc(tweakerGroup)

        for node in self.tweakerArray:
            tweakerGroup.ConnectSrc(node)

        for marker in self.markers:
            ikGroup.ConnectSrc(marker)

        for attach in self.attachConstraints:
            self.connectMetaData(attach,
                                 self.ANCHOR_CONSTRAINT_METADATA_NAME)

        dummyConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        cornerFolder = mobu.FBFolder(self.CORNER_CONSTRAINT_FOLDER_NAME,
                                     dummyConstraint)

        self.constraintFolder.ConnectSrc(cornerFolder)
        dummyConstraint.FBDelete()
        for constraint in self.cornerHandleConstraints:
            cornerFolder.ConnectSrc(constraint)
            self.connectMetaData(constraint,
                                 self.CORNER_CONSTRAINT_FOLDER_NAME)

    def createConstraintFolder(self):
        self.constraintFolder = None
        for folder in Globals.Folders:
            if folder.Name == 'Constraints 1':
                self.constraintFolder = folder

        if self.constraintFolder is None:
            dummyConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
            self.constraintFolder = mobu.FBFolder(self.CONSTRAINT_FOLDER_NAME,
                                                  dummyConstraint)

            dummyConstraint.FBDelete()

        self.constraintFolder.ConnectSrc(self.hullConstraintFolder)

        for constraint in self.targetConstraints:
            self.constraintFolder.ConnectSrc(constraint)

        self.constraintFolder.ConnectSrc(self.attachFolder)

    def createCornerAnchors(self):
        for cornerHandle in (self.leftHandleMarker,
                             self.rightHandleMarker):
            parentConstraint = self.createAttachConstraint(cornerHandle.Parent,
                                                           self.animationControlGroup ,
                                                           preserveOffset=True)

            self.cornerHandleConstraints.append(parentConstraint)

    def buildHullRig(self):
        self.createTweakerLayer()

        self.createHullLayer()

        self.createRibbonLayer()

        self.createControlLayer()

        self.createTargetLayer()

        self.createGlueLayer()

        self.setControlRenderType()

        self.createRoot()

        self.finalizeRig()

        return self.strapRoot


class StrapAnchor(object):
    NODE_ATTRIBUTES = ('parentNode', 'weightProperty')

    def __init__(self):
        self.parentNode = None

        self.weightProperty = None

        self.blendingNode = None

        self.name = 'attach1'

    def buildInterpolator(self):
        pass

    def __repr__(self):
        reportData = '\n\t<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_ATTRIBUTES:
            currentAttribute = getattr(self, attribute)

            if currentAttribute is None:
                reportData += '\n\t\t{0}:None'.format(attribute)
            else:
                reportData += '\n\t\t{1}:{0}'.format(currentAttribute.Name,
                                                   attribute)

        return reportData


class StrapPointRig(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    NODE_ATTRIBUTES = ('ikAnchor', 'baseAnchor')

    GROUP_INDEX_ARRAY = (PARENT_NODE_INDEX, PARENT_NODE_INDEX, PARENT_NODE_INDEX)

    COMPONENT_INDEX_ARRAY = (0, 1, 2)

    WEIGHT_PROPERTY_SUFFIX = 'Weight'

    SETTING1_SOURCE_A = mobu.FBVector3d(1.0, 0.0, 0.0)

    SETTING2_SOURCE_A = mobu.FBVector3d(0.0, 1.0, 0.0)

    SETTING1_SOURCE_B = mobu.FBVector3d(0.0, 1.0, 0.0)

    SETTING2_SOURCE_B = mobu.FBVector3d(0.0, 0.0, 1.0)

    SETTING1_NAME_A = 'sourceA_Setting1'

    SETTING2_NAME_A = 'sourceA_Setting2'

    SETTING1_NAME_B = 'sourceB_Setting1'

    SETTING2_NAME_B = 'sourceB_Setting2'

    def __init__(self):
        self.ikAnchor = None

        self.baseAnchor = None

        self.alternateAnchor = None

        self.targetConstraint = None

        self.blendDriver = None

        self.name = 'attach1'

        self.blendSetting1_inputA = None

        self.blendSetting2_inputA = None

        self.blendSetting1_inputB = None

        self.blendSetting2_inputB = None

        self.skipRelationConstraintSetup = False

    def getPropsNamespace(self,
                          inputDummy):
        currentNamespace = Namespace.GetNamespace(inputDummy)

        if len(currentNamespace)==0:
            return None

        return currentNamespace

    def createSwapProperty(self):
        inputAttribute = self.blendDriver.PropertyList.Find('swapSettings')
        if inputAttribute is None:
            inputAttribute = self.blendDriver.PropertyCreate('swapSettings', 
                                                             mobu.FBPropertyType.kFBPT_bool, 
                                                             'Bool', 
                                                             True, 
                                                             True, 
                                                             None)
        inputAttribute.SetAnimated(True)

    def addMetaData(self, 
                    inputPropertyName,
                    inputValue):
        inputAttribute = self.blendDriver.PropertyList.Find(inputPropertyName)
        if inputAttribute is None:
            inputAttribute = self.blendDriver.PropertyCreate(inputPropertyName, 
                                                             mobu.FBPropertyType.kFBPT_Vector3D, 
                                                            'Vector', 
                                                             True, 
                                                             True, 
                                                             None)

        inputAttribute.Data = inputValue
        inputAttribute.SetAnimated(True)

        return inputAttribute

    def createInterpolationProperties(self):
        self.blendSetting1_inputA = self.addMetaData(self.SETTING1_NAME_A,
                                                     self.SETTING1_SOURCE_A)

        self.blendSetting2_inputA = self.addMetaData(self.SETTING2_NAME_A,
                                                     self.SETTING2_SOURCE_A)

        self.blendSetting1_inputB = self.addMetaData(self.SETTING1_NAME_B,
                                                     self.SETTING1_SOURCE_B)

        self.blendSetting2_inputB = self.addMetaData(self.SETTING2_NAME_B,
                                                     self.SETTING2_SOURCE_B)

        self.createSwapProperty()

    def extractParentComponents(self,
                                attribute,
                                componentIndex):
        strapAnchor = StrapAnchor()
        assetNamespace = self.getPropsNamespace(self.targetConstraint)

        strapAnchor.parentNode = self.targetConstraint.ReferenceGet(self.GROUP_INDEX_ARRAY[componentIndex],
                                                                    self.COMPONENT_INDEX_ARRAY[componentIndex])

        if assetNamespace is None:
            weightPropertyName = '{0}.{1}'.format(strapAnchor.parentNode.Name,
                                                  self.WEIGHT_PROPERTY_SUFFIX)

            weightPropertyBufferName = '{0}_Buffer.{1}'.format(strapAnchor.parentNode.Name,
                                                               self.WEIGHT_PROPERTY_SUFFIX)
        else:
            weightPropertyName = '{0}.{1}'.format(strapAnchor.parentNode.LongName,
                                                  self.WEIGHT_PROPERTY_SUFFIX)

            weightPropertyBufferName = '{0}_Buffer.{1}'.format(strapAnchor.parentNode.LongName,
                                                               self.WEIGHT_PROPERTY_SUFFIX)

        for component in self.targetConstraint.PropertyList:
            if str(component.Name) == weightPropertyName:
                strapAnchor.weightProperty = component
                setattr(self, 
                        attribute, 
                        strapAnchor)

                break

            if str(component.Name) == weightPropertyBufferName:
                strapAnchor.weightProperty = component

                setattr(self, 
                        attribute, 
                        strapAnchor)

                break

    def collectFromConstraint(self,
                              inputConstraint):
        self.targetConstraint = inputConstraint
        assetNamespace = self.getPropsNamespace(inputConstraint)

        if assetNamespace is None:
            self.blendDriver = mobu.FBModelNull('{}_Driver1'.format(inputConstraint.Name))
            self.createInterpolationProperties()
        else:
            blendDriverList = mobu.FBComponentList()

            blendDriverName = '{}_Driver1'.format(inputConstraint.Name)
            blendDriverName = '{0}:{1}'.format(assetNamespace,
                                               blendDriverName)

            mobu.FBFindObjectsByName(blendDriverName,
                                     blendDriverList,
                                     True,
                                     True)

            if len(blendDriverList)>0:
                self.blendDriver = blendDriverList[0]
                self.skipRelationConstraintSetup = True
            else:
                self.blendDriver = mobu.FBModelNull(blendDriverName)
                self.createInterpolationProperties()

        for attributeIndex, attribute in enumerate(self.NODE_ATTRIBUTES):
            self.extractParentComponents(attribute,
                                         attributeIndex)

    def __repr__(self):
        reportData = '\n<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_ATTRIBUTES:
            currentAttribute = getattr(self, attribute)

            if currentAttribute is None:
                reportData += '\n\t{0}:None'.format(attribute)
            else:
                reportData += '\n\t{1}:{0}'.format(currentAttribute,
                                                   attribute)

        return reportData
