import os

from functools import partial

from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS.Utils import ContextManagers
from RS import Config

from RS.Tools.IkSpline import Utils as ikUtils
reload(ikUtils)


class JointListWidget(QtGui.QWidget):
    """
        main Tool UI
    """
    def __init__(self,
                 toolBox):
        """
            Constructor
        """
        self.toolBox = toolBox

        self.jointListModel = QtGui.QStringListModel()
        self.jointSelectModel = QtGui.QStringListModel()

        # Main window settings
        super(JointListWidget, self).__init__()
        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        widgetLayout = QtGui.QVBoxLayout()
        widgetLayout.setContentsMargins(0, 0, 0, 0)

        self.widgetGroupBox = QtGui.QGroupBox('Joint list')

        self.jointListWidget = QtGui.QListView() 
   
        self.jointListWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.jointListWidget.setEditTriggers(0)

        self.jointListWidget.setModel(self.jointListModel)  

        self.AddToJointListButtonWidget = QtGui.QPushButton("Add to list")

        self.RemoveFromJointListButtonWidget = QtGui.QPushButton("Remove from list")

        widgetLayout.addWidget(self.widgetGroupBox)

        mainLayout.addWidget(self.jointListWidget)
        mainLayout.addWidget(self.RemoveFromJointListButtonWidget)
        mainLayout.addWidget(self._createSeparator())
        mainLayout.addItem(self._createSelectionModeControls())
        mainLayout.addWidget(self._createSeparator())
        mainLayout.addWidget(self.AddToJointListButtonWidget)

        mainLayout.setContentsMargins(5, 5, 5, 5)
        self.setMinimumWidth(250)
        self.AddToJointListButtonWidget.setMinimumHeight(32)

        self.widgetGroupBox.setLayout(mainLayout)

        self.setLayout(widgetLayout)

        self.AddToJointListButtonWidget.pressed.connect(self._appendSelectedJoints)

        self.RemoveFromJointListButtonWidget.pressed.connect(self._removeFromList)

    def _removeFromList(self):
        boneList = self.jointListModel.stringList() 
        if len(boneList)<1:
            return
        
        boneList = list(boneList)

        jointSelectionModel = self.jointListWidget.selectionModel()
        currentJointToRemove = [index.row() for index in jointSelectionModel.selectedIndexes()]

        if len(currentJointToRemove) == 0:
            return

        jointNames = self.jointListModel.stringList()

        currentJointToRemove = sorted(currentJointToRemove)
        currentJointToRemove.reverse()

        for jointIndex in currentJointToRemove:
            jointNames.pop(jointIndex)

        self.jointListModel.setStringList(jointNames)

    def _createSelectionModeControls(self):
        selectModeLayout = QtGui.QHBoxLayout()

        self.addJointMode = QtGui.QComboBox()
        self.addJointMode.setModel(self.jointSelectModel)

        jointSelectionModeLabel = QtGui.QLabel("Selection Mode")

        # Assign Widgets to Layouts
        selectModeLayout.addWidget(jointSelectionModeLabel)
        selectModeLayout.addWidget(self.addJointMode)

        self.jointSelectModel.setStringList(['Selected Items'])
        self.addJointMode.setCurrentIndex(0)

        return selectModeLayout

    def _writeJointListModel(self, joinList):
        boneList = self.jointListModel.stringList() 

        if len(boneList) < 1:
            self.jointListModel.setStringList(joinList)
        else:
            boneList = list(boneList)
            boneList.extend(joinList)
            self.jointListModel.setStringList(boneList)

    def _appendSelectedJoints(self):
        currentSelection = ikUtils.JointChain().getJointChainFromSelection()

        if len(currentSelection)<1:
            return

        joinList = []
        for joint in currentSelection:
            joinList.append(str(joint.LongName))

        self._writeJointListModel(joinList)

    def _collectJointListFromWidget(self):
        boneList = self.jointListModel.stringList() 

        if len(boneList)<1:
            return []

        jointList = []
        for bone in boneList:
            jointChain = mobu.FBComponentList()       
            mobu.FBFindObjectsByName('{0}'.format(str(bone)),
                                     jointChain,
                                     True,
                                     True)
            if len(jointChain) < 1 :
                continue

            jointList.append(jointChain[0])

        return jointList

    def _createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine


class PlotAnimationWidget(QtGui.QWidget):
    """
        main Tool UI
    """
    def __init__(self,
                 toolBox):
        """
            Constructor
        """
        self.toolBox = toolBox

        self.plotModeModel = QtGui.QStringListModel()


        # Main window settings
        super(PlotAnimationWidget, self).__init__()
        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        widgetLayout = QtGui.QVBoxLayout()
        widgetLayout.setContentsMargins(0, 0, 0, 0)

        self.widgetGroupBox = QtGui.QGroupBox('Bake bones attached to spline')

        self.batchPlotCheckBox = QtGui.QCheckBox("Process All Takes")

        self.plotButtonWidget = QtGui.QPushButton("Plot Animation")

        self.plotModeComboBox = QtGui.QComboBox(self)

        self.plotModeModel.setStringList(['Plot rotation and translation', 
                                          'Plot rotation only on chain links'])

        self.plotModeComboBox.setModel(self.plotModeModel)

        mainLayout.setContentsMargins(5, 5, 5, 5)
        self.setMinimumWidth(250)
        self.plotButtonWidget.setMinimumHeight(42)

        mainLayout.addWidget(self.plotModeComboBox)

        mainLayout.addWidget(self._createSeparator())

        mainLayout.addWidget(self.batchPlotCheckBox)

        mainLayout.addWidget(self.plotButtonWidget)

        widgetLayout.addWidget(self.widgetGroupBox)

        #Populates character list
        self.widgetGroupBox.setLayout(mainLayout)

        self.setLayout(widgetLayout)

        self.plotButtonWidget.pressed.connect(self._plotAnimation)

    def _plotAnimation(self):
        jointComponents = self.toolBox.jointWidget._collectJointListFromWidget()

        if len(jointComponents) == 0:
            return

        utils = ikUtils.SimpleSplinePlotter()
        utils.plotSimpleSplineRig(jointComponents,
                                  self.batchPlotCheckBox.isChecked())

    def _createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine
