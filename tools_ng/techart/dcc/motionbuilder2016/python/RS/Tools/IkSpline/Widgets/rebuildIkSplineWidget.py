import os
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Config
from RS.Tools.IkSpline import Core as IkSplineCore
from RS.Tools.IkSpline import Utils as ikUtils


class RebuildIkSplineWidget(QtGui.QWidget):
    """
        main Tool UI
    """
    def __init__(self,
                 toolBox):
        """
            Constructor
        """
        self.toolBox = toolBox

        # Main window settings
        super(RebuildIkSplineWidget, self).__init__()
        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        self.resetIkSplineButtonWidget = QtGui.QPushButton("reset IkSpline")

        self.rebuildIkSplineButtonWidget = QtGui.QPushButton("Rebuild IkSpline")

        mainLayout.setContentsMargins(0, 0, 0, 0)
        self.setMinimumWidth(310)

        mainLayout.addWidget(self.resetIkSplineButtonWidget)

        mainLayout.addWidget(self._createSeparator())

        mainLayout.addWidget(self.rebuildIkSplineButtonWidget)

        #Populates character list
        self.setLayout(mainLayout)

        self.resetIkSplineButtonWidget.clicked.connect(self._resetRig)

        self.rebuildIkSplineButtonWidget.clicked.connect(self._rebuildRig)

    def _createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _replaceMhPrefix(self):
        pass

    def _resetRig(self):
        motionUtils = IkSplineCore.Setup()
        splineIkRig = mobu.FBFindModelByLabelName(str(self.toolBox.rootWidget.ikRootField.text()))
        if splineIkRig is None:
            return

        motionUtils.resetRig(splineIkRig)

    def _rebuildRig(self):
        motionUtils = IkSplineCore.Setup()
        motionUtils.rebuildRig(str(self.toolBox.rootWidget.ikRootField.text()),
                               overrideControllerCount=self.toolBox.settingWidget.numberOfCurveControllerWidget.value())

        self._replaceMhPrefix()
