import inspect
import json
import math
import os
import tempfile

import pyfbsdk as mobu
from RS import Globals

from RS.Utils import Scene

from RS.Tools.IkSpline import Core as  IkSplineCore
from RS.Utils.Scene import RelationshipConstraintManager as relationConstraintManager


from RS.Utils import Namespace
from RS.Utils.Scene import Constraint
from RS.Tools.IkSpline import Utils as ikUtils


def GetMainGroup():
    fileName = Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName)

    for group in Globals.Groups:
        if group.Name == fileName:
            return group

    group = mobu.FBGroup(fileName)

    return group


class WeightSlider(object):
    def __init__(self):
        self.targetNode = None
        self.translationOffsetProperty = None
        self.weightProperty = None
        self.slider = None

    def keyProperty(self, inputProperty):
        if inputProperty is None:
            return

        if 'Unbound' in inputProperty.__class__.__name__:
            return

        if not inputProperty.IsAnimated():
            inputProperty.SetAnimated(True)

        weightAnimationNode = inputProperty.GetAnimationNode()
        weightAnimationNode.KeyCandidate()

    def keyAllProperties(self):
        self.keyProperty(self.weightProperty)

        self.keyProperty(self.translationOffsetProperty)


class PointData(object):
    def __init__(self):
        self.weights = mobu.FBVector3d()
        self.indexArray = mobu.FBVector3d()

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)
        reportData += '\n\t{0}'.format('<Components:>')

        reportData += '\n\n\t\t{0}:weights'.format(self.weights)

        reportData += '\n\n\t\t{0}:indexArray'.format(self.indexArray)

        return reportData


class Barycenters(object):
    def __init__(self):
        self.pivotPoint = mobu.FBVector3d()

        self.aimPoint = mobu.FBVector3d()

        self.upPoint = mobu.FBVector3d()

        self.samplingPoint = mobu.FBVector3d()

        self.frameArea = 0.0

        self.pointIndexArray = []

    def getPointWeight(self,
                       pivotPoint):
        subTriangleAimSegment = self.samplingPoint - pivotPoint

        subTriangleUpSegment = self.upPoint - pivotPoint

        return subTriangleAimSegment.CrossProduct(subTriangleUpSegment).Length()

    def getWeights(self,
                   inputSamplingPoint):
        self.samplingPoint = inputSamplingPoint

        upPointArea = self.getPointWeight(self.pivotPoint)

        pivotPointArea = self.getPointWeight(self.aimPoint)

        upPointWeight = upPointArea/self.frameArea

        pivotPointWeight = pivotPointArea/self.frameArea

        aimPointWeight = 1.0 - upPointWeight - pivotPointWeight

        pointComponent = PointData()

        pointComponent.weights = mobu.FBVector3d(pivotPointWeight,
                                                 upPointWeight,
                                                 aimPointWeight)

        pointComponent.indexArray = mobu.FBVector3d(self.pointIndexArray[0],
                                                    self.pointIndexArray[1],
                                                    self.pointIndexArray[2])

        return pointComponent

    def collectInputData(self,
                         pointIndexArray,
                         inputFramePoints):
        self.pointIndexArray = pointIndexArray

        self.pivotPoint = inputFramePoints[pointIndexArray[0]]

        self.aimPoint = inputFramePoints[pointIndexArray[1]]

        self.upPoint = inputFramePoints[pointIndexArray[2]]

        triangleAimSegment = self.aimPoint - self.pivotPoint

        triangleUpSegment = self.upPoint - self.pivotPoint

        self.frameArea = triangleAimSegment.CrossProduct(triangleUpSegment).Length()


class ArcHull(object):
    HORIZONTAL_OFFSET = 380

    VERTICAL_OFFSET = 100

    SHIFT_COMPONENT_OFFSET = 350

    LAYOUT_OFFSETS = (-100, 0, 100)

    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    def __init__(self):
        self.limitPoints = []

        self.pointWeights = []

        self.frameIndexArray = []

        self.pointIndexArray = []

        self.pointPositions = []

        self.tangentPivotIndex = 2

        self.lowerWeightUtils = Barycenters()

        self.upperWeightUtils = Barycenters()

        self.relationConstraint = None

        self.handleArray = []

        self.increment = 0

        self.inputJointArray = []

        self.tweakerArray = []

        self.constraintPrefix = 'lowerHullConstraint1'

        self.pointConstraint = None

    def buildHullConstraint(self,
                            drivenNode,
                            parentArray):
        self.pointConstraint = Constraint.CreateConstraint(Constraint.POSITION)

        self.pointConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                     drivenNode)

        for parentTarget in parentArray:
            self.pointConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                         parentTarget)

        self.pointConstraint.Snap()
        self.pointConstraint.Active = True

        self.pointConstraint.Name = 'positionRivet_{0}'.format(self.constraintPrefix)
        return self.pointConstraint

    def getMatrixPivot(self,
                       inputMatrix):
        return mobu.FBVector3d(inputMatrix[12],
                               inputMatrix[13],
                               inputMatrix[14])

    def collectJointPoints(self,
                           inputJointArray):
        self.inputJointArray = inputJointArray
        inMatrix1 = mobu.FBMatrix()
        self.pointIndexArray = []
        self.pointPositions = []

        for jointIndex in xrange(self.frameIndexArray[0],
                                 self.frameIndexArray[len(self.frameIndexArray)-1]):
            inputJointArray[jointIndex].GetMatrix(inMatrix1)

            self.pointIndexArray.append(jointIndex)
            self.pointPositions.append(self.getMatrixPivot(inMatrix1))

    def projectPivotOnTangentLine(self,
                                  startPoint,
                                  lineVector,
                                  angleVector):
        lineVectorNormal = mobu.FBVector3d().CopyFrom(lineVector).Normalize()
        angleVectorNormal = mobu.FBVector3d().CopyFrom(angleVector).Normalize()

        elevationAngle =  math.acos(lineVectorNormal.DotProduct(angleVectorNormal))
        lastAngle =  math.radians(90.0 - math.degrees(elevationAngle))
        segmentLength = math.cos(elevationAngle)*angleVector.Length()

        projectedPoint = startPoint+lineVectorNormal*segmentLength

        return projectedPoint

    def collectComponents(self,
                          hullStartIndex,
                          inputIndex,
                          hullEndIndex,
                          strapJointArray):
        inMatrix1 = mobu.FBMatrix()
        strapJointArray[hullStartIndex].GetMatrix(inMatrix1)

        inMatrix2 = mobu.FBMatrix()
        strapJointArray[hullEndIndex].GetMatrix(inMatrix2)

        hullStart = self.getMatrixPivot(inMatrix1)
        hullEnd = self.getMatrixPivot(inMatrix2)

        pivotMatrix = mobu.FBMatrix()
        strapJointArray[inputIndex].GetMatrix(pivotMatrix)

        startMatrix = mobu.FBMatrix()
        strapJointArray[inputIndex-1].GetMatrix(startMatrix)

        endMatrix = mobu.FBMatrix()
        strapJointArray[inputIndex+1].GetMatrix(endMatrix)

        pivotPoint = self.getMatrixPivot(pivotMatrix)
        startPoint = self.getMatrixPivot(startMatrix)
        endPoint = self.getMatrixPivot(endMatrix)

        lineVector = endPoint-startPoint
        angleVector = pivotPoint-startPoint

        arcData = ArcHull()
        tangentPivot = self.projectPivotOnTangentLine(startPoint,
                                                      lineVector,
                                                      angleVector)

        offsetVectorForKnots = pivotPoint - tangentPivot
        offsetStartPoint = startPoint+offsetVectorForKnots
        offsetEndPoint = endPoint+offsetVectorForKnots

        self.limitPoints = [hullStart,
                            offsetStartPoint,
                            pivotPoint,
                            offsetEndPoint,
                            hullEnd]

        self.frameIndexArray = [hullStartIndex,
                                inputIndex,
                                hullEndIndex+1]

        self.collectJointPoints(strapJointArray)

    def extractWeights(self):
        self.lowerWeightUtils.collectInputData([0, 1, 2],
                                               self.limitPoints)

        self.upperWeightUtils.collectInputData([2, 3, 4],
                                               self.limitPoints)

        for pointIndex in xrange(len(self.pointPositions)):
            if pointIndex < self.tangentPivotIndex:
                self.pointWeights.append(self.lowerWeightUtils.getWeights(self.pointPositions[pointIndex]))
            else:
                self.pointWeights.append(self.upperWeightUtils.getWeights(self.pointPositions[pointIndex]))

    def buildLine(self,
                  startPoint,
                  endPoint):
        strapLine = mobu.FBModelPath3D('line1')
        strapLine.PathKeyClear()

        if not isinstance(startPoint, mobu.FBVector4d):
            startPoint = mobu.FBVector4d(startPoint[0],
                                         startPoint[1],
                                         startPoint[2], 1.0)

        strapLine.PathKeyEndAdd(startPoint)

        if not isinstance(endPoint, mobu.FBVector4d):
            endPoint = mobu.FBVector4d(endPoint[0],
                                         endPoint[1],
                                         endPoint[2], 1.0)

        strapLine.PathKeyEndAdd(endPoint)

        strapLine.Show = True

        return strapLine

    def drawHull(self):
        self.buildLine(self.limitPoints[0],
                       self.limitPoints[-1])

        for pointIndex in xrange(len(self.limitPoints)-1):
            self.buildLine(self.limitPoints[pointIndex],
                           self.limitPoints[pointIndex+1])

    def createHandle(self):
        return mobu.FBModelNull('{0}_handle1'.format(self.constraintPrefix))

    def findAnimationNode(self, inputComponent, attributeName):
        for node in inputComponent.Nodes:
            if node.Name == attributeName:
                return node

        return None

    def buildRig(self,
                 constraintPrefix,
                 skipJointIndex=0,
                 showLocator=False):
        self.constraintPrefix = constraintPrefix
        relationConstraintName = '{}_Relation'.format(constraintPrefix)
        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)
        self.relationConstraint.SetActive(True)

        for point in self.limitPoints:
            handle = self.createHandle()
            handle.Show = showLocator
            handle.Translation = point
            self.handleArray.append(handle)

        attributeArray = ('a', 'b', 'c')

        for pointDataIndex, pointData in enumerate(self.pointWeights):
            if pointDataIndex < skipJointIndex:
                continue

            position = (-100,self.increment*self.SHIFT_COMPONENT_OFFSET)
            sumNode = self.relationConstraint.AddFunctionBox("Vector",
                                                             "Sum 10 vectors")

            sumNode.SetBoxPosition(position[0]+self.HORIZONTAL_OFFSET*2,
                                   position[1]+self.LAYOUT_OFFSETS[0])

            sumNode.GetBox().Name = 'jointOutput{0}'.format(pointDataIndex)

            for senderIndex in xrange(3):
                pivot = self.handleArray[int(pointData.indexArray[senderIndex])]
                pivotSender = self.relationConstraint.AddSenderBox(pivot)
                pivotSender.GetBox().Name = '{0}_input1'.format(pivot.Name)

                pivotSender.SetBoxPosition(position[0],
                                           position[1]+self.LAYOUT_OFFSETS[senderIndex])

                sizeSender = self.relationConstraint.AddFunctionBox("Vector",
                                                                    "Scale (a x V)")

                pivotSender.GetBox().Name = '{0}_weight1'.format(pivot.Name)

                inPlugs = self.findAnimationNode(sizeSender.GetBox().AnimationNodeInGet(),
                                                 'Number')

                inPlugs.WriteData([pointData.weights[senderIndex]])

                sizeSender.SetBoxPosition(position[0]+self.HORIZONTAL_OFFSET,
                                          position[1]+self.LAYOUT_OFFSETS[senderIndex])

                self.relationConstraint.ConnectBoxes(pivotSender,
                                                     'Translation',
                                                     sizeSender,
                                                     'Vector')

                self.relationConstraint.ConnectBoxes(sizeSender,
                                                     'Result',
                                                     sumNode,
                                                     attributeArray[senderIndex])

            receiver = self.relationConstraint.AddReceiverBox(self.tweakerArray[self.pointIndexArray[pointDataIndex]].Parent)

            receiver.SetBoxPosition(position[0]+self.HORIZONTAL_OFFSET*3,
                                    position[1]+self.LAYOUT_OFFSETS[0])

            self.relationConstraint.ConnectBoxes(sumNode,
                                                 'Result',
                                                 receiver,
                                                 'Translation')

            self.increment += 1


class MotionpathComponent(object):
    def __init__(self):
        self.zeroGroup = None
        self.anchorNode = None
        self.controlNode = None
        self.offsetNode = None
        self.jointNode = None

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)
        reportData += '\n\t{0}'.format('<Components:>')

        reportData += '\n\t\tjointNode:{0}'.format(self.jointNode.LongName)

        reportData += '\n\t\tzeroGroup:{0}'.format(self.zeroGroup.LongName)

        reportData += '\n\t\tanchorNode:{0}'.format(self.anchorNode.LongName)

        reportData += '\n\t\tcontrolNode{0}:'.format(self.controlNode.LongName)

        reportData += '\n\t\toffsetNode{0}:'.format(self.offsetNode.LongName)

        return reportData


class CurveKnot(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    TARGET_CONSTRAINT_FOLDER_NAME = 'TargetConstraints1'

    def __init__(self,
                 sourceNode,
                 targetNode,
                 spaceNode,
                 spaceParent,
                 knotType='anchor1',
                 reverseAim=False,
                 inConstraintFolder=None):
        self.constraintUtils =  ikUtils.ConstraintUtils()

        self.sourceNode = sourceNode
        self.drivenNode = mobu.FBModelNull('{0}_{1}'.format(sourceNode.Name,
                                                            knotType))

        self.targetNode = targetNode

        self.spaceParent = spaceParent

        self.spaceNode = spaceNode

        self.reverseAim = reverseAim

        self.targetConstraint = None

        self.constraintFolder = None

        self.inConstraintFolder = inConstraintFolder

        self.attachComponents()

    def attachComponents(self):
        inputMatrix = mobu.FBMatrix()
        self.sourceNode.GetMatrix(inputMatrix)

        self.drivenNode.Parent = self.spaceParent

        self.drivenNode.SetMatrix(inputMatrix)

        self.drivenNode.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d()

        if self.reverseAim is False:
            self.targetConstraint = self.constraintUtils.createTargetConstraint(self.drivenNode,
                                                                                self.targetNode,
                                                                                spaceObject=self.spaceNode,
                                                                                reverseAim=self.reverseAim)[0]
        else:
            self.targetConstraint = Constraint.CreateConstraint(Constraint.ROTATION)

            self.targetConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                               self.drivenNode)

            self.targetConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                               self.spaceNode)

            self.targetConstraint.Active = True

        self.targetConstraint.Name = '{0}_{1}'.format(self.sourceNode.Name,
                                                      'targetConstraint1')

        if self.inConstraintFolder is None:
            self.constraintFolder = mobu.FBFolder(self.TARGET_CONSTRAINT_FOLDER_NAME,
                                                  self.targetConstraint)

            self.constraintFolder.ConnectSrc(self.targetConstraint)

        else:
            self.inConstraintFolder.ConnectSrc(self.targetConstraint)
            self.constraintFolder = self.inConstraintFolder