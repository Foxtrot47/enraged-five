import pyfbsdk as mobu


class AxisController(object):
    def __init__(self):
        self.pointArray = (
                            mobu.FBVector3d(-0.505661, -5.5, 0.0),
                            mobu.FBVector3d(0.494339, -5.5, 0.0),
                            mobu.FBVector3d(-0.5, 2.42424, 0.0),
                            mobu.FBVector3d(0.5, 2.42424,-3.80505e-007),
                            mobu.FBVector3d(-1.00794, 2.43018, 0.0),
                            mobu.FBVector3d(1.00794, 2.43018, 0.0),
                            mobu.FBVector3d(0, 4.96459, 0),
                            mobu.FBVector3d(-0.505661, -5.5, 0),
                            mobu.FBVector3d(-0.505661, -4.5, 0),
                            mobu.FBVector3d(-9.10255, -5.5, 0.0),
                            mobu.FBVector3d(-9.10255, -4.5, 0.0),
                            mobu.FBVector3d(-9.10849, -6.00794, 0.0),
                            mobu.FBVector3d(-9.10849, -3.99206, 0.0),
                            mobu.FBVector3d(-9.96256, -5, 0.0),
                            mobu.FBVector3d(0.362329, -5, 0),
                            mobu.FBVector3d(-0.362329, -5, 0),
                            mobu.FBVector3d(0.362329, -5, 9.10286),
                            mobu.FBVector3d(-0.362329, -5, 9.10286),
                            mobu.FBVector3d(1.00794, -5, 9.1088),
                            mobu.FBVector3d(-1.00794, -5, 9.1088),
                            mobu.FBVector3d(0, -5, 9.96287) 
                          )

        self.faceArray = (  [1,2,4],
                            [4,3,1],
                            [3,4,6],
                            [6,5,3],
                            [5,6,7],
                            [8,9,11],
                            [11,10,8],
                            [10,11,13],
                            [13,12,10],
                            [12,13,14],
                            [15,16,18],
                            [18,17,15],
                            [17,18,20],
                            [20,19,17],
                            [19,20,21] )

    def create(self):
        controllerMesh = mobu.FBMesh("axisMesh1")
        controllerMesh.GeometryBegin()
        controllerMesh.VertexArrayInit(len(self.pointArray), 
                                       False, 
                                       mobu.FBGeometryArrayID.kFBGeometryArrayID_Color)

        vertexPositions = []

        for point in self.pointArray:
            vertexPositions.append(point[0]*-0.1)
            vertexPositions.append((point[1]+5)*1.5*0.1)
            vertexPositions.append(point[2]*0.1)

        controllerMesh.SetPositionsArray(vertexPositions)

        for face in self.faceArray:
            faceConvertion = [face[0]-1,
                              face[1]-1,
                              face[2]-1]

            controllerMesh.TriangleListAdd(3, faceConvertion, 1)

        #   15/21 X
        #   1/7 Y
        #   8/14 Z

        colorArray = []
        for pointIndex in xrange(0, 7):
            colorArray.extend([0.0, 1.0, 0.0, 1.0])

        for pointIndex in xrange(7, 14):
            colorArray.extend([1.0, 0.0, 0.0, 1.0])

        for pointIndex in xrange(14, 21):
            colorArray.extend([0, 0.0, 1.0, 1.0])

        controllerMesh.SetVertexColorsDirectArray(colorArray)

        controllerMesh.GeometryEnd()

        output = mobu.FBModelCube('test1')#mobu.FBModelMarker
        output.Geometry = controllerMesh
        output.Show = True
        output.Visibility = True

        output.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire

        return output