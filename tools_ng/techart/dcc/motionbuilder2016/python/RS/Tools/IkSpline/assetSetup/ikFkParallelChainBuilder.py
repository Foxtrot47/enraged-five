import inspect
import json
import math
import os
import tempfile

from PySide import QtGui, QtCore

import pyfbsdk as mobu
from RS import Globals

from RS.Utils import Scene

from RS.Tools.IkSpline import Core as IkSplineCore
from RS.Tools.IkSpline import Utils as IkUtils
from RS.Tools.IkSpline import Rigging as IkRigging
from RS.Tools.IkSpline import Settings as ikSettings
from RS.Utils import ContextManagers, Namespace
from RS.Utils.Scene import Constraint
from RS.Utils.Scene import RelationshipConstraintManager as relationConstraintManager
from RS.Tools.IkSpline import Tetrahedron
from RS.Utils.Logging import const



class TangentSegment(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    def __init__(self):
        self.orientOrient = None
        self.aimTarget = None
        self.tangent = None
        self.spaceReference = None

        self.targetConstraint = None

        self.constraintUtils = IkUtils.ConstraintUtils()

    def createHelper(self,
                     jointName,
                     knotNode,
                     knotSuffix):
        aimPoint = mobu.FBModelNull('{0}_{1}1'.format(jointName,
                                                      knotSuffix))

        aimPoint.Show = True
        aimPoint.PropertyList.Find('Look').Data = 0
        aimPoint.Parent = knotNode

        alignNode = self.constraintUtils.alignWithConstraint(aimPoint,
                                                             knotNode)

        self.constraintUtils.bakeConstraintValue(alignNode)

        return aimPoint

    def buildFromNodes(self,
                       knotNode,
                       previousNode,
                       targetNode):
        self.aimTarget = self.createHelper(knotNode.Name,
                                           targetNode,
                                           'AimTarget')

        self.spaceReference = self.createHelper(knotNode.Name,
                                                previousNode,
                                                'SpaceReference')

        self.orientOrient = Constraint.CreateConstraint(Constraint.ROTATION)
        self.orientOrient.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                       self.spaceReference)

        self.orientOrient.ReferenceAdd(self.PARENT_NODE_INDEX,
                                       knotNode)

        self.orientOrient.Active = True

        self.tangent = self.createHelper(knotNode.Name,
                                         knotNode,
                                         'tangent')

        self.targetConstraint = self.constraintUtils.createTargetConstraint(self.tangent,
                                                                            self.aimTarget,
                                                                            spaceObject=self.spaceReference)[0]

        self.orientOrient.Name = '{0}_Orient1'.format(knotNode.Name)

        self.targetConstraint.Name = '{0}_Target1'.format(knotNode.Name)


class CurveKnot(object):
    NODE_ATTRIBUTES = ('segmentPoints',
                       'goalPoint',
                       'samplingIndex',
                       'weightData',
                       'segmentWeights')

    def __init__(self):
        self.segmentPoints = [mobu.FBVector3d(),
                              mobu.FBVector3d()]

        self.goalPoint = mobu.FBVector3d()

        self.sourcePoint = mobu.FBVector3d()

        self.samplingIndex = 0

        self.weightData = []

        self.pointNode = None

        self.segmentWeights = 0.0

    def blendWeights(self):
        segmentLength = (self.segmentPoints[1] - self.segmentPoints[0]).Length()

        goalLength = (self.goalPoint - self.segmentPoints[0]).Length()

        self.segmentWeights = goalLength / segmentLength

    def createPoint(self,
                    useSource=True):
        self.pointNode = mobu.FBModelMarker('pathPoint1')
        self.pointNode.PropertyList.Find('Look').Data = 1
        self.pointNode.Show = True
        self.pointNode.Visibility = True

        self.pointNode.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire

        colorProperty = self.pointNode.PropertyList.Find('Color RGB')

        newColour = mobu.FBColor((0,255,0))
        colorProperty.Data = newColour

        if useSource is True:
            self.pointNode.Translation = self.sourcePoint
        else:
            self.pointNode.Translation = self.goalPoint

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_ATTRIBUTES:
            currentValue = getattr(self, attribute)

            reportData += '\n\t{0}:{1}'.format(attribute,
                                               currentValue)

        return reportData


class PointOnCurve(object):
    def __init__(self,
                 inputPoint,
                 startPoint,
                 endPoint):
        self.inputPoint = inputPoint

        self.startPoint = startPoint

        self.endPoint = endPoint

        self.startLength = (startPoint - inputPoint).Length()

        self.endLength = (endPoint - inputPoint).Length()

        self.segmentLineA = self.endPoint - self.startPoint

        self.sampleLineA = self.inputPoint - self.startPoint

        self.startAngle = 0.0

        self.endAngle = 0.0

        self.dotValue = 0.0

    def computeFrame(self):
        segmentLineB = self.startPoint - self.endPoint
        sampleLineB = self.inputPoint - self.endPoint

        segmentLineA_Normalize = mobu.FBVector3d(self.segmentLineA[0],
                                                 self.segmentLineA[1],
                                                 self.segmentLineA[2])

        segmentLineB_Normalize = mobu.FBVector3d(segmentLineB[0],
                                                 segmentLineB[1],
                                                 segmentLineB[2])

        sampleLineA_Normalize = mobu.FBVector3d(self.sampleLineA[0],
                                                self.sampleLineA[1],
                                                self.sampleLineA[2])

        sampleLineB_Normalize = mobu.FBVector3d(sampleLineB[0],
                                                sampleLineB[1],
                                                sampleLineB[2])

        segmentLineA_Normalize.Normalize()
        segmentLineB_Normalize.Normalize()

        sampleLineA_Normalize.Normalize()
        sampleLineB_Normalize.Normalize()

        self.dotValue = segmentLineA_Normalize.DotProduct(sampleLineA_Normalize)

        self.startAngle = math.degrees(math.acos(self.dotValue))

        self.endAngle = math.degrees(math.acos(segmentLineB_Normalize.DotProduct(sampleLineB_Normalize)))

    def getClosestPoint(self):
        return (self.startPoint+(self.segmentLineA*(self.dotValue*(self.sampleLineA.Length()/self.segmentLineA.Length()))))


class PeltProgressWindowDialog(QtGui.QMainWindow):
    '''
    Dialogue to run ProgressWindow and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PeltProgressWindowDialog, self).__init__(parent=parent)

        # set dialog to center the screen
        #dialogGeometry.moveCenter(QtGui.QApplication.desktop().availableGeometry().center())

        # remove frame and titlebar/buttons from dialog
        self.setWindowFlags(QtCore.Qt.Tool | QtCore.Qt.FramelessWindowHint)

        # create layout and add widget
        self._progressWindow = PeltProgressWindow()
        self.setCentralWidget(self._progressWindow)

    def AddText(self, text, textcolor=const.MessageTextColours.Default):
        self._progressWindow.AddText(text, textcolor)


class PeltProgressWindow(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PeltProgressWindow, self).__init__(parent=parent)
        self._setupUi()

    def _setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        layout = QtGui.QHBoxLayout()

        # create widgets
        self.console = QtGui.QTextEdit()

        # widget properties
        self.console.setReadOnly(True)
        self.console.setFrameStyle(QtGui.QFrame.NoFrame)
        self.console.setFontPointSize(10)

        # set widgets to layout
        layout.addWidget(self.console)

        # set layout
        self.setLayout(layout)

    def hide(self):
        self.console.clear()
        super(ProgressWindow, self).hide()

    def AddText(self, text, textColor):
        self.console.setTextColor(QtGui.QColor(textColor))
        self.console.setText('\n\n{0}'.format(text))
        self.repaint()


class BakePeltWorlspace(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    DEFAULT_TIME_DATA = 0

    def __init__(self,
                 sourceComponentArray,
                 plotKeys=True,
                 deleteAnchors=True):
        self.sourceComponentArray = sourceComponentArray

        self.plotKeys = plotKeys

        self.deleteAnchors = deleteAnchors

        self.matchingConstraintArray = []

        self.targetNodeArray = []

        self.animationNodes = []

        self.assetProgress = None



        self.uiTextColor = const.MessageTextColours.Default

        self.useWorldTarget = True

    def exposeTakeRange(self, usePlayerControl=True):
        lPlayer = mobu.FBPlayerControl()
        startTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        stopTime = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        return startTime, stopTime, lPlayer

    def keyCurrentFrame(self,
                        frameIndex,
                        lPlayer,
                        subSamples=1.0,
                        refreshScene=False):
        sourceTime = mobu.FBTime(self.DEFAULT_TIME_DATA,
                                 self.DEFAULT_TIME_DATA,
                                 self.DEFAULT_TIME_DATA,
                                 (frameIndex-1),
                                 self.DEFAULT_TIME_DATA,
                                 mobu.FBTimeMode.kFBTimeMode30Frames)

        targetTime = mobu.FBTime(self.DEFAULT_TIME_DATA,
                                 self.DEFAULT_TIME_DATA,
                                 self.DEFAULT_TIME_DATA,
                                 frameIndex,
                                 self.DEFAULT_TIME_DATA,
                                 mobu.FBTimeMode.kFBTimeMode30Frames)

        sourceValue = sourceTime.GetSecondDouble()

        targetValue = targetTime.GetSecondDouble()

        samplingTime = mobu.FBTime(self.DEFAULT_TIME_DATA,
                                   self.DEFAULT_TIME_DATA,
                                   self.DEFAULT_TIME_DATA,
                                   0,
                                   self.DEFAULT_TIME_DATA,
                                   mobu.FBTimeMode.kFBTimeMode30Frames)

        samplingIncrement = (targetValue-sourceValue)/subSamples
        sampleCount = int(subSamples) + 1

        for shiftIndex in xrange(sampleCount):
            samplingValue = sourceValue+shiftIndex*samplingIncrement

            samplingTime.SetSecondDouble(samplingValue)
            lPlayer.Goto(samplingTime)

            if refreshScene is True:
                mobu.FBSystem().Scene.Evaluate()

                for controlIndex, control in enumerate(self.targetNodeArray):
                    targetMatrix = mobu.FBMatrix()

                    translateBuffer = control.worldTarget.Translation
                    rotateBuffer = control.worldTarget.Rotation

                    translateValue = mobu.FBVector4d(translateBuffer[0],
                                                     translateBuffer[1],
                                                     translateBuffer[2],
                                                     1.0)

                    rotateValue = mobu.FBVector3d(rotateBuffer[0],
                                                  rotateBuffer[1],
                                                  rotateBuffer[2])

                    mobu.FBTRSToMatrix(targetMatrix,
                                       translateValue,
                                       rotateValue,
                                       mobu.FBSVector())

                    self.animationNodes[controlIndex][1].KeyCandidate()
                    self.animationNodes[controlIndex][2].KeyCandidate()
            else:
                for animationData in self.animationNodes:
                    animationData[1].KeyCandidate()
                    animationData[2].KeyCandidate()

            lPlayer.Key()

    def forceAnimationPlot(self,
                           refreshScene=False):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        self.animationNodes =[]
        for control in self.targetNodeArray:
            currentNode = control.worldTarget
            if self.useWorldTarget is False:
                currentNode = control.sourceJoint

            animationData = [currentNode]

            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = currentNode.PropertyList.Find(property)

                if not inputAttribute.IsAnimated():
                    inputAttribute.SetAnimated(True)

                animationNode = inputAttribute.GetAnimationNode()

                inputAttribute.SetFocus(True)
                animationData.append(animationNode)

            self.animationNodes.append(animationData)

            currentNode.Selected = True

        self.assetProgress.show()

        reportMessage = '   MESSAGE [Setting keys on Pelt components] \n\t  percent done:{percent}\n\t  Processing frame{itemName}'
        progressRatio = 1.0/float(stopTime-startTime)

        progressIncrement = 0

        Globals.Scene.Evaluate()

        for timeIndex in range(startTime, stopTime+1):
            progressValue = progressIncrement*progressRatio*100.0

            self.keyCurrentFrame(timeIndex,
                                 lPlayer,
                                 refreshScene=refreshScene)

            progressIncrement+=1
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                                            itemName=timeIndex),
                                       textcolor=self.uiTextColor)

            QtCore.QTimer.singleShot(0,
                                     self._refreshProgress)

            self._forceDrawOverride()

        self.assetProgress.hide()

    def _bakeAnimation(self):
        startTime, stopTime, lPlayer = self.exposeTakeRange()

        takePlotSettings = mobu.FBPlotOptions()
        takePlotSettings.PlotAllTakes = False
        takePlotSettings.PlotLockedProperties = False
        takePlotSettings.PreciseTimeDiscontinuities = True
        takePlotSettings.PlotTranslationOnRootOnly = False
        takePlotSettings.PreciseTimeDiscontinuities = True
        takePlotSettings.UseConstantKeyReducer = False
        takePlotSettings.PlotOnFrame = False

        takePlotSettings.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll
        mobu.FBSystem().CurrentTake.PlotTakeOnSelectedProperties(takePlotSettings)

    def _forceDrawOverride(self):
        self.assetProgress.resize(500, 65)
        self.assetProgress.updateGeometry()

    def _refreshProgress(self):
        self.assetProgress.updateGeometry()

    def _extractWorldSpaceState(self,
                                progressRatio):
        reportMessage = '   MESSAGE [Preparing worldspace Pelt components] \n\t  percent done:{percent}\n\t  Processing {itemName}'

        for nodeIndex, node in enumerate(self.sourceComponentArray):
            progressValue = nodeIndex*progressRatio*100.0
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                       itemName=node.Name))

            null = mobu.FBModelNull('{}_worldSpace1'.format(node.Name))

            null.Show = True
            null.Visible = True
            null.QuaternionInterpolate = True

            animationData = IkUtils.TransferComponent(null,
                                                      node)

            self.targetNodeArray.append(animationData)

            matchingConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
            matchingConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                            null)

            matchingConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                            node)

            matchingConstraint.Active = True

            self.matchingConstraintArray.append(matchingConstraint)

        mobu.FBSystem().Scene.Evaluate()

    def _transferWorldToSourceJoint(self,
                                    progressRatio):
        reportMessage = '   MESSAGE [Transferring worldspace components to source Joints] \n\t  percent done:{percent}\n\t  Processing {itemName}'

        for nodeIndex, node in enumerate(self.sourceComponentArray):
            progressValue = nodeIndex*progressRatio*100.0
            self.assetProgress.AddText(reportMessage.format(percent=progressValue,
                                       itemName=node.sourceJoint.Name))

            animationData = IkUtils.TransferComponent(node.worldTarget,
                                                      node.sourceJoint)

            self.targetNodeArray.append(animationData)

            matchingConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
            matchingConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                            node.sourceJoint)

            matchingConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                            node.worldTarget)

            matchingConstraint.Active = True

            self.matchingConstraintArray.append(matchingConstraint)

        mobu.FBSystem().Scene.Evaluate()

    def __enter__(self):
        componentCount = len(self.sourceComponentArray)
        if componentCount == 0:
            componentCount = 1

        progressRatio = 1.0/float(componentCount-1)
        progressValue = 0.0

        self.assetProgress = PeltProgressWindowDialog()

        if not isinstance(self.sourceComponentArray[0],
                          IkUtils.TransferComponent):
            self.useWorldTarget = True
            self.uiTextColor = const.MessageTextColours.Warning

            self.assetProgress.show()

            self._extractWorldSpaceState(progressRatio)

            self.assetProgress.hide()

            if self.plotKeys is True:
                self.forceAnimationPlot()
        else:
            self.useWorldTarget = False
            self.uiTextColor = const.MessageTextColours.Minor_Update

            if self.plotKeys is True:
                self._bakeAnimation()

                self.assetProgress.show()

                self._transferWorldToSourceJoint(progressRatio)

                self.assetProgress.hide()

                self.forceAnimationPlot(refreshScene=True)

                self._bakeAnimation()

        return self

    def __exit__(self, exceptionType, value, traceback):
        mobu.FBSystem().Scene.Evaluate()

        self.assetProgress.close()

        if self.deleteAnchors is False:
            mobu.FBSystem().Scene.Evaluate()
            return

        for constraint in self.matchingConstraintArray:
            constraint.Active = False

            constraint.FBDelete()

        for control in self.targetNodeArray:
            currentNode = control.worldTarget
            if self.useWorldTarget is False:
                currentNode = control.sourceJoint

            currentNode.Selected = False

            for property in ('Translation (Lcl)',
                             'Rotation (Lcl)'):
                inputAttribute = currentNode.PropertyList.Find(property)
                inputAttribute.SetFocus(False)

        mobu.FBSystem().Scene.Evaluate()


class PeltComponent(object):
    def __init__(self,
                 driver=None,
                 joint=None,
                 constraint=None):
        self.driver = driver

        self.joint = joint

        self.constraint = constraint

        self.ActiveState = True

        if self.constraint is not None:
            self.ActiveState = self.constraint.Active


class FkIkRigStructure(object):
    METADATA_ROOT_TAG = 'ikSplineMetaRoot'

    METADATA_ROOT_VALUE = 'fkikSplineRoot'

    METADATA_FK_RIG_CONTROLS = 'fkRigControls'

    METADATA_IK_RIG_CONTROLS = 'IkRigControls'

    METADATA_JOINTS = 'joints'

    UDP_PROPERTY = 'UDP3DSMAX'

    FLAG_PROPERTY = 'rsProjectedBone'

    NODE_ATTRIBUTES = ('fkRigRoot',
                       'ikRigRoot',
                       'fkRigControls',
                       'ikRigControls',
                       'joints',
                       'gameInfluences',
                       'ikConstraint')

    def __init__(self):
        self.fkRigRoot = None

        self.ikRigRoot = None

        self.fkRigControls = []

        self.ikRigControls = []

        self.joints = []

        self.fkIkRigRoot = None

        self.modelList = []

        self.ikConstraint = []

        self.gameInfluences = []

        self.transferAnimationComponents = []

        self.jointConstraints = []

        self.assetsComponents = []

        self.rigIsActive = False

        self.constraintUtils = IkUtils.ConstraintUtils()

    def refreshGameInfluences(self):
        constraintComponents = self.exposeObjectSetComponents('ikConstraint')
        if constraintComponents is None:
            return

        components = self.exposeObjectSetComponents('gameInfluences')
        if components is not None:
            return

        for constraint in self.ikConstraint:
            targetJoint = constraint.ReferenceGet(0, 0)
            if targetJoint is None:
                continue

            self.gameInfluences.append(targetJoint)

        for node in self.gameInfluences:
            self.connectMetaData(self.fkIkRigRoot,
                                 'gameInfluences',
                                 node)

    def refreshConstraints(self):
        constraintComponents = self.exposeObjectSetComponents('ikConstraint')
        if constraintComponents is not None:
            return

        for joint in self.joints:
            if joint.GetDstCount() == 0:
                continue

            targetJoint = None
            for index in xrange(1, joint.GetDstCount()):
                if not isinstance(joint.GetDst(index),
                                  mobu.FBConstraint):
                    continue

                target = joint.GetDst(index).ReferenceGet(0, 0)

                udpAttribute = target.PropertyList.Find(self.UDP_PROPERTY)
                if udpAttribute is None:
                    continue

                if udpAttribute.Data == None:
                    continue

                if len(udpAttribute.Data) == 0:
                    continue

                if self.FLAG_PROPERTY in udpAttribute.Data:
                    targetJoint = joint.GetDst(index)

            if targetJoint is None:
                continue

            self.ikConstraint.append(targetJoint)

        for node in self.ikConstraint:
            self.connectMetaData(self.fkIkRigRoot,
                                 'ikConstraint',
                                 node)

    def getRootFromName(self,
                        inputName):
        targetNodeList = mobu.FBComponentList()
        sampleName = str(inputName)
        if ':' in sampleName:
            mobu.FBFindObjectsByName('{0}'.format(sampleName),
                                     targetNodeList,
                                     True,
                                     True)
        else:
            mobu.FBFindObjectsByName('{0}'.format(sampleName),
                                     targetNodeList,
                                     True,
                                     False)

        if len(targetNodeList)==0:
            return None

        return targetNodeList[0]

    def clearComponents(self):
        self.fkRigRoot = None

        self.ikRigRoot = None

        self.fkRigControls = []

        self.ikRigControls = []

        self.joints = []

        self.fkIkRigRoot = None

    def createMetaRoot(self,
                       splineIkRig):
        root = splineIkRig.PropertyCreate(self.METADATA_ROOT_TAG,
                                          mobu.FBPropertyType.kFBPT_charptr,
                                          'String',
                                          False,
                                          True,
                                          None)

        root.Data = self.METADATA_ROOT_VALUE

        splineIkRig.PropertyCreate(self.METADATA_JOINTS,
                                   mobu.FBPropertyType.kFBPT_object,
                                   'Object',
                                   False,
                                   True,
                                   None)

        splineIkRig.PropertyCreate('skelRootModelArray',
                                   mobu.FBPropertyType.kFBPT_object,
                                   'Object',
                                   False,
                                   True,
                                   None)

        splineIkRig.PropertyCreate('ikConstraint',
                                   mobu.FBPropertyType.kFBPT_object,
                                   'Object',
                                   False,
                                   True,
                                   None)

        splineIkRig.PropertyCreate('fkRigRoot',
                                   mobu.FBPropertyType.kFBPT_object,
                                   'Object',
                                   False,
                                   True,
                                   None)

        splineIkRig.PropertyCreate('ikRigRoot',
                                   mobu.FBPropertyType.kFBPT_object,
                                   'Object',
                                   False,
                                   True,
                                   None)

        splineIkRig.PropertyCreate(self.METADATA_FK_RIG_CONTROLS,
                                   mobu.FBPropertyType.kFBPT_object,
                                   'Object',
                                   False,
                                   True,
                                   None)

        splineIkRig.PropertyCreate(self.METADATA_IK_RIG_CONTROLS,
                                   mobu.FBPropertyType.kFBPT_object,
                                   'Object',
                                   False,
                                   True,
                                   None)

        splineIkRig.PropertyCreate('gameInfluences',
                                   mobu.FBPropertyType.kFBPT_object,
                                   'Object',
                                   False,
                                   True,
                                   None)

    def connectMetaData(self,
                        splineIkRig,
                        category,
                        childModel):
        metaRootProperty = splineIkRig.PropertyList.Find(category)

        if metaRootProperty is None:
            return

        childModel.ConnectDst(metaRootProperty)

    def build(self,
              fkIkRigRoot,
              fkRigRoot,
              ikRigRoot,
              fkControls,
              ikControls,
              inputJointArray):
        self.createMetaRoot(fkIkRigRoot)

        self.connectMetaData(fkIkRigRoot,
                             'fkRigRoot',
                             fkRigRoot)

        self.connectMetaData(fkIkRigRoot,
                             'ikRigRoot',
                             ikRigRoot)

        for node in fkControls:
            self.connectMetaData(fkIkRigRoot,
                                 'fkRigControls',
                                 node)

        for node in ikControls:
            self.connectMetaData(fkIkRigRoot,
                                 'ikRigControls',
                                 node)

        for node in inputJointArray:
            self.connectMetaData(fkIkRigRoot,
                                 'joints',
                                 node)

        for node in inputJointArray:
            self.connectMetaData(fkIkRigRoot,
                                 'gameInfluences',
                                 node)

    def collectFkIkRoots(self):
        fkIkRoots = []

        for node in mobu.FBSystem().Scene.Components:
            rootProperty = node.PropertyList.Find(self.METADATA_ROOT_TAG)
            if rootProperty is None:
                continue

            if rootProperty.Data != self.METADATA_ROOT_VALUE:
                continue

            fkIkRoots.append(node)

        if len(fkIkRoots)==0:
            return None

        return fkIkRoots

    def exposeObjectSetComponents(self,
                                  metadataLink):
        if self.fkIkRigRoot is None:
            return

        componentArray = []

        metaData = self.fkIkRigRoot.PropertyList.Find(metadataLink)
        if metaData is None:
            self.fkIkRigRoot.PropertyCreate(metadataLink,
                                            mobu.FBPropertyType.kFBPT_object,
                                            'Object',
                                            False,
                                            True,
                                            None)

            return []

        numberOfDrivenJoints = metaData.GetSrcCount()

        for memberIndex in range(numberOfDrivenJoints):
            member = metaData.GetSrc(memberIndex)
            if member is None:
                continue

            componentArray.append(member)

        if len(componentArray)==0:
            return None

        if len(componentArray)==1:
            return componentArray[0]

        return componentArray

    def populateRigStructure(self,
                             inputRootNode):
        self.clearComponents()

        self.fkIkRigRoot = inputRootNode

        for attribute in self.NODE_ATTRIBUTES:
            components = self.exposeObjectSetComponents(attribute)
            if components is None:
                continue

            setattr(self,
                    attribute,
                    components)

        self.refreshConstraints()

        self.refreshGameInfluences()

        self.collectJointConstraints()

        self.rigIsActive = self.collectRigState()

        self.modelList = []
        self.modelList.extend(self.fkRigControls)
        self.modelList.extend(self.ikRigControls)

    def collectRigState(self):
        if len(self.jointConstraints)==0:
            return False

        rigActivateValueArray = []
        constraintCount = len(self.jointConstraints)/2
        for node in self.jointConstraints:
            if node.Active is False:
                continue

            rigActivateValueArray.append(True)

        if len(rigActivateValueArray)>=constraintCount:
            return True

        return False

    def alignAnimationControllers(self,
                                  fkToIk=True,
                                  setkeys=False):
        drivenChain = []
        targetChain = []

        if fkToIk is True:
            drivenChain = [node for node in self.fkRigControls]
            targetChain = [node for node in self.ikRigControls]
        else:
            drivenChain = [node for node in self.ikRigControls]
            targetChain = [node for node in self.fkRigControls]

        for nodeIndex, node in enumerate(drivenChain):
            targetMatrix = mobu.FBMatrix()
            targetChain[nodeIndex].GetMatrix(targetMatrix)
            node.SetMatrix(targetMatrix)

            mobu.FBSystem().Scene.Evaluate()

        return
        if setkeys is False:
            return

        for inputNode in self.modelList:
            if inputNode is None:
                return

            self.setKeysOnProperties(inputNode,
                                     'Translation (Lcl)')

            self.setKeysOnProperties(inputNode,
                                     TARGET_ANIMATION_PROPERTIES[1])

            self.setKeysOnProperties(inputNode,
                                     TARGET_ANIMATION_PROPERTIES[2])

    def exposeRigElements(self,
                          showFkControllers=True,
                          showIkControllers=False):
        visibilityState = [showFkControllers for node in self.fkRigControls]
        visibilityState.extend([showIkControllers for node in self.ikRigControls])

        for inputNodeIndex, inputNode in enumerate(self.modelList):
            inputNode.PropertyList.Find('Show').Data = visibilityState[inputNodeIndex]
            inputNode.PropertyList.Find('Visibility').Data = visibilityState[inputNodeIndex]

    def collectPlotElements(self):
        plotDataArray = []
        for jointIndex in xrange(len(self.joints)):
            plotData = PeltComponent(driver=self.joints[jointIndex],
                                     joint=self.gameInfluences[jointIndex],
                                     constraint=self.ikConstraint[jointIndex])

            plotDataArray.append(plotData)

        return plotDataArray

    def plotAnimation(self,
                      disableConstraint=True,
                      deleteWorlstate=True):
        self.processAnimation(disableConstraint=disableConstraint,
                              deleteWorlstate=deleteWorlstate)

        self.tranposeMatrixProperties(disableConstraint=disableConstraint,
                                      deleteWorlstate=deleteWorlstate)

    def collectJointConstraints(self):
        self.assetsComponents = []

        self.assetsComponents = [node for node in self.gameInfluences]

        if len(self.assetsComponents)==0:
            return

        self.jointConstraints = []
        for node in self.assetsComponents:
            self.jointConstraints.extend(self.constraintUtils.findConstraints(node))

    def processAnimation(self,
                         disableConstraint=True,
                         deleteWorlstate=False):
        self.assetsComponents = []

        self.assetsComponents = [node for node in self.gameInfluences]

        if len(self.assetsComponents)==0:
            return

        self.collectJointConstraints()

        for constraint in self.ikConstraint:
            constraint.Active = True

        self.transferAnimationComponents = []
        with BakePeltWorlspace(self.assetsComponents) as stateUtils:
            self.transferAnimationComponents = stateUtils.targetNodeArray

    def tranposeMatrixProperties(self,
                                 disableConstraint=True,
                                 deleteWorlstate=True):
        if disableConstraint is True:
            for constraint in self.jointConstraints:
                constraint.Active = False

        for constraint in self.ikConstraint:
            constraint.Active = False

        Globals.Scene.Evaluate()

        bufferArray = []

        with ContextManagers.SuspendSceneParallelism():
            with BakePeltWorlspace(self.transferAnimationComponents,
                                   deleteAnchors=True) as transferUtils:
                bufferArray = transferUtils.targetNodeArray

        if deleteWorlstate is False:
            return

        for component in self.transferAnimationComponents:
            if not isinstance(component.worldTarget,
                              mobu.FBModelNull):
                continue

            component.worldTarget.FBDelete()

    def toggleRigState(self,
                       activeValue):
        self.assetsComponents = []

        self.assetsComponents = [node for node in self.gameInfluences]

        if len(self.assetsComponents)==0:
            return

        self.jointConstraints = []
        for node in self.assetsComponents:
            self.jointConstraints.extend(self.constraintUtils.findConstraints(node))

        for constraint in self.jointConstraints:
            constraint.Active = activeValue

        mobu.FBSystem().Scene.Evaluate()

    def getRigConstraintFolder(self):
        attachConstraintFolder = None

        attachConstraint = None
        for index in xrange(self.joints[0].GetSrcCount()):
            destinationItem = self.joints[0].GetSrc(index)

            if not isinstance(destinationItem,
                              mobu.FBConstraint):
                continue

            attachConstraint = destinationItem

        for index in xrange(attachConstraint.GetDstCount()):
            destinationItem = attachConstraint.GetDst(index)

            if not isinstance(destinationItem, mobu.FBFolder):
                continue

            attachConstraintFolder = destinationItem
            break

        rigConstraintFolder = None
        for index in xrange(attachConstraintFolder.GetDstCount()):
            destinationItem = attachConstraintFolder.GetDst(index)

            if not isinstance(destinationItem, mobu.FBFolder):
                continue

            rigConstraintFolder = destinationItem
            break

        return rigConstraintFolder

    def collectFkIkRootsInNamespace(self,
                                    targetNamespace):
        fkIkRoots = []

        rigComponents = mobu.FBComponentList()

        Globals.Scene.NamespaceGetContentList(rigComponents,
                                              targetNamespace)

        for node in rigComponents:
            rootProperty = node.PropertyList.Find(self.METADATA_ROOT_TAG)
            if rootProperty is None:
                continue

            if rootProperty.Data != self.METADATA_ROOT_VALUE:
                continue

            fkIkRoots.append(node)

        fkIkRoots = list(set(fkIkRoots))

        return fkIkRoots

    def deleteIkFkRigInNamespace(self,
                                 fkIkRoots,
                                 removeReference=True):
        if len(fkIkRoots)==0:
            return

        componentsArray = []
        for index, inputRootNode in enumerate(fkIkRoots):
            print inputRootNode.LongName

            if index>0:
                self.populateRigStructure(inputRootNode)

            componentsArray.extend(self.deleteIkFkRig(deleteComponent=False,
                                                      excludeRoot=fkIkRoots))

            inputRootNode.FBDelete()

        Globals.Scene.Evaluate()

        componentsArray = list(set(componentsArray))

        if removeReference is False:
            return

        self.removeRigReference(targetNamespace)

        Globals.Scene.Evaluate()

    def deleteIkFkRig(self,
                      deleteComponent=True,
                      excludeRoot=None):
        rigConstraintFolder = self.getRigConstraintFolder()

        nodeArray = []

        self.getHierachy(self.fkIkRigRoot,
                         nodeArray)

        self.deleteIkFkConstraints(rigConstraintFolder)

        if deleteComponent is False:
            Globals.Scene.Evaluate()

            return nodeArray

        nodeArray = list(set(nodeArray))
        nodeArray.reverse()

        for node in nodeArray:
            if node is None:
                continue

            if 'Unbound' in str(type(node)):
                continue

            if isinstance(node, mobu.FBCharacter):
                continue

            if excludeRoot is not None:
                if node in excludeRoot:
                    continue

            node.FBDelete()

        Globals.Scene.Evaluate()

    def getHierachy(self,
                    currentNode,
                    nodeArray):
        for currentNode in currentNode.Children:
            nodeArray.append(currentNode)

            self.getHierachy(currentNode,
                             nodeArray)

    def deleteIkFkConstraints(self,
                              rigConstraintFolder):
        itemArray = []
        self.collectAllConstraints(rigConstraintFolder,
                                   itemArray)

        for item in itemArray:
            item.Lock = False
            item.Active = False

        itemArray.reverse()
        for item in itemArray:
            if not isinstance(item,
                              mobu.FBConstraint):
                continue

            if item is None:
                continue

            if 'Unbound' in str(type(item)):
                continue

            if isinstance(item,
                          mobu.FBCharacter):
                continue

            item.FBDelete()

        Globals.Scene.Evaluate()

    def collectAllConstraints(self,
                              currentFolder,
                              itemArray):
        for item in currentFolder.Items:
            if not isinstance(item,
                              mobu.FBFolder):
                itemArray.append(item)
            else:
                self.collectAllConstraints(item,
                                           itemArray)

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)
        reportData += '\n\t{0}'.format('<Components:>')

        attribute = 'fkIkRigRoot'
        currentAttribute = getattr(self, attribute)
        if currentAttribute is None:
            reportData += '\n\t\t{0}:None'.format(attribute)
        else:
            reportData += '\n\t\t{0}:{1}'.format(attribute,
                                                 currentAttribute.LongName)

        for attribute in self.NODE_ATTRIBUTES:
            currentAttribute = getattr(self, attribute)
            if not isinstance(currentAttribute, list):
                if currentAttribute is None:
                    reportData += '\n\t\t{0}:None'.format(attribute)
                else:
                    reportData += '\n\t\t{0}:{1}'.format(attribute,
                                                         currentAttribute.LongName)
            else:
                if len(currentAttribute)==0:
                    reportData += '\n\t\t{0}:[]'.format(attribute)
                else:
                    reportData += '\n\t\t{0}:'.format(attribute)
                    for component in currentAttribute:
                        reportData += '\n\t\t\t{0}'.format(component.LongName)

        return reportData


class SplineSamples(object):
    def __init__(self):
        self.splineObject = None
        self.ikControls = []


class BindPose(object):
    def __init__(self,
                 inputBone):
        self.node = inputBone
        self.matrix = mobu.FBMatrix()

        rotationProperty = inputBone.PropertyList.Find('Pre Rotation')
        self.preRotation = rotationProperty.Data

        self.node.GetMatrix(self.matrix)


class Build(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    IK_SPLINE_DRIVEN_TARGET = 1

    HORIZONTAL_OFFSET = 480

    VERTICAL_OFFSET = 100

    SHIFT_COMPONENT_OFFSET = 300

    WEIGHT_COMPONENT_OFFSET = 150

    WEIGHT_PROPERTY_SUFFIX = 'Weight'

    indexAttributes = ('pointIndex0',
                       'pointIndex1',
                       'pointIndex2',
                       'pointIndex3')

    weightAttributes = ('weight0',
                        'weight1',
                        'weight2',
                        'weight3')

    def __init__(self):
        self.ikSplineUtils = IkSplineCore.Setup()

        self.jointArray = []

        self.fkjointArray = []

        self.ikjointArray = []

        self.bindPoses = []

        self.splineObject = None

        self.fkLineObject = None

        self.ikLineObject = None

        self.fkjointRoot = None

        self.ikjointRoot = None

        self.rigName = None

        self.fkRigRoot = None

        self.ikRigRoot = None

        self.fkControlsRoot = None

        self.ikControlsRoot = None

        self.ikBuildSettings = None

        self.fkBuildSettings = None

        self.rigConstraintFolder = None

        self.rigAttachFolder = None

        self.relationConstraint = None

        self.fkIkRigRoot = None

        self.blendAttribute = None

        self.blendSource = None

        self.blendTarget = None

        self.increment = 0

        self.attachConstraints = []

        self.knotDataArray = []

        self.offsetVector = mobu.FBVector3d(90.0, 0.0, 0.0)

        self.constraintUtils = IkUtils.ConstraintUtils()

    def saveBindPose(self,
                     jointArray):
        mobu.FBSystem().Scene.Evaluate()
        for joint in self.jointArray:
            self.bindPoses.append(BindPose(joint))

    def restoreBindPose(self,
                        bindPoseArray):
        for pose in bindPoseArray:
            rotationProperty = pose.node.PropertyList.Find('Pre Rotation')
            rotationProperty.Data = pose.preRotation

    def createNeutralPose(self,
                          parentMatrix,
                          boneMatrix):
        targetMatrix = mobu.FBMatrix(parentMatrix.matrix)

        for channelIndex in (12, 13, 14):
            targetMatrix[channelIndex] = boneMatrix.matrix[channelIndex]

        #boneMatrix.matrix = targetMatrix
        return targetMatrix

        spaceNull = mobu.FBModelNull('space1')
        spaceNull.SetMatrix(parentMatrix.matrix)

        targetNull = mobu.FBModelNull('target1')
        targetNull.SetMatrix(targetMatrix)

        targetNull.Parent = spaceNull

        spaceNull.Translation = mobu.FBVector3d(boneMatrix.matrix[12],
                                                boneMatrix.matrix[13],
                                                boneMatrix.matrix[14])

        return targetMatrix

    def straightenJointArray(self):
        mobu.FBSystem().Scene.Evaluate()
        for jointIndex in xrange(1, len(self.bindPoses)):
            rotationProperty = self.bindPoses[jointIndex].node.PropertyList.Find('Pre Rotation')
            rotationProperty.Data = mobu.FBVector3d(0, 0, 0)

        mobu.FBSystem().Scene.Evaluate()

    def createIkPath(self):
        numberOfCurveController = self.ikSplineUtils.ikSplineSettings.numberOfCurveController
        splineObject = self.ikSplineUtils.createPathFromJoints(self.jointArray,
                                                               numberOfCurveController=numberOfCurveController)

        return splineObject

    def createFkRig(self,
                    splineObject,
                    temporaryPath):
        self.ikSplineUtils.ikSplineSettings.fkOffset = mobu.FBVector3d(0.0, 0.0, 90.0)

        ikControlsSize = self.ikSplineUtils.ikSplineSettings.ikControlsSize

        controlList = self.ikSplineUtils.ikSplineLayer.buildTwistDriverFromCurve(temporaryPath.Name,
                                                                                 ikControlsSize)

        self.attachJointCloneToSpline(splineObject,
                                      None,
                                      controlList,
                                      skipAfterSplineDistrution=True)

        ikControlsData = SplineSamples()
        ikControlsData.ikControls = [node for node in controlList]

        self.aimChainComponents(controlList)

        fkControlSettings = self.ikSplineUtils.ikSplineLayer.createFkRig(ikControlsData,
                                                                         self.ikSplineUtils.ikSplineSettings,
                                                                         parentIkcontrollers=False)

        for node in controlList:
            childArray = list(node.Children)
            childArray.reverse()

            if len(childArray)>0:
                for nodeIndex in xrange(len(childArray)):
                    childArray[nodeIndex].FBDelete()

            node.FBDelete()

        return fkControlSettings

    def aimChainComponents(self,
                           ikControlArray):
        aimDataArray = []
        for ikControlIndex in range(len(ikControlArray)-1):
            aimData = self.constraintUtils.createTargetConstraint(ikControlArray[ikControlIndex],
                                                                  ikControlArray[ikControlIndex+1])
            aimDataArray.append(aimData)

        Globals.Scene.Evaluate()

        for ikControlIndex in range(len(ikControlArray)-1):
            aimRotation = ikControlArray[ikControlIndex].PropertyList.Find('Rotation (Lcl)').Data

            self.constraintUtils.bakeConstraintValue(aimDataArray[ikControlIndex][0])
            aimDataArray[ikControlIndex][1].FBDelete()
            ikControlArray[ikControlIndex].PropertyList.Find('Rotation (Lcl)').Data = aimRotation

        Globals.Scene.Evaluate()

    def attachJointCloneToSpline(self,
                                 splineObject,
                                 inputControls,
                                 inputNodeArray,
                                 preserveLength=True,
                                 skipAfterSplineDistrution=False):
        segmentData = self.ikSplineUtils.bindDataUtils.collectChainSegments(inputNodeArray)

        ikConstraint = self.ikSplineUtils.constraintUtils.createIkConstraint(splineObject, None)
        ikConstraint.Active = False
        mobu.FBSystem().Scene.Evaluate()

        ikConstraint.PropertyList.Find("Aim To Next Joint").Data = True
        ikConstraint.Name = splineObject.Name + '_constraint'
        ikConstraint.PropertyList.Find('Use Percentage').Data = False
        ikConstraint.PropertyList.Find("Reverse Chain").Data = False
        ikConstraint.PropertyList.Find("applyIK_Chain_rules").Data = True

        for joint in inputNodeArray:
            ikConstraint.ReferenceAdd(self.IK_SPLINE_DRIVEN_TARGET,
                                      joint)

        mobu.FBSystem().Scene.Evaluate()

        for chainIndex in xrange(len(segmentData['chainSegments'])):
            chainLengthHandle = ikConstraint.PropertyList.Find('chainLength{0}'.format((chainIndex+1)))
            if segmentData['chainSegments'][chainIndex] < 0.01:
                continue

            chainLengthHandle.Data = segmentData['chainSegments'][chainIndex]

        ikConstraint.Active = True
        Globals.Scene.Evaluate()

        if skipAfterSplineDistrution is True:
            ikConstraint.Active = False
            ikConstraint.FBDelete()

            return

        uvalues = []
        for chainIndex in xrange(len(segmentData['chainSegments'])):
            uValueHandle = ikConstraint.PropertyList.Find('chainValue{0}'.format((chainIndex+1)))
            uvalues.append(float(uValueHandle.Data))

        uvalues.append((splineObject.PathKeyGetCount()-1)*100.0)

        ikConstraint.Active = False
        ikConstraint.PropertyList.Find("applyIK_Chain_rules").Data = False

        for chainIndex in xrange(len(uvalues)):
            uValueHandle = ikConstraint.PropertyList.Find('uValue{0}'.format((chainIndex+1)))
            uValueHandle.Data = uvalues[chainIndex]

        self.ikSplineUtils.ikSplineLayer.connectTwistPropagation(splineObject,
                                                                 ikConstraint,
                                                                 inputControls)

        ikConstraint.Active = True
        Globals.Scene.Evaluate()

        if preserveLength is True:
            ikConstraint.PropertyList.Find("applyIK_Chain_rules").Data = True

        #------------------------------------------------------------- Bundle rig Data here
        ikSplineStructureSettings = ikSettings.IkSplineStructureSettings()

        ikSplineStructureSettings.splineObject  = splineObject
        ikSplineStructureSettings.curveDeformer = None
        ikSplineStructureSettings.ikConstraint = ikConstraint
        ikSplineStructureSettings.jointObjectArray = [node for node in inputNodeArray]
        ikSplineStructureSettings.fkControls = [node for node in inputControls]

        return ikSplineStructureSettings

    def getCurvePoints(self,
                       splineObject,
                       createNull=True,
                       useMarkerShape=False):
        curveMatrix = mobu.FBMatrix()
        curveSpace = splineObject.GetMatrix(curveMatrix)

        positionInWorldSpace = mobu.FBVector4d()
        CV_position = mobu.FBVector4d()

        controlCount = splineObject.PathKeyGetCount()
        controls = []

        for pointIndex in xrange(controlCount):
            point = splineObject.PathKeyGet(pointIndex)

            CV_position[0] = point[0]
            CV_position[1] = point[1]
            CV_position[2] = point[2]

            mobu.FBVectorMatrixMult(positionInWorldSpace,
                                    curveMatrix,
                                    CV_position)

            if createNull is True:
                if useMarkerShape is False:
                    control = mobu.FBModelNull('point1')
                else:
                    control = mobu.FBModelMarker('point1')

                control.Translation = mobu.FBVector3d(positionInWorldSpace[0],
                                                      positionInWorldSpace[1],
                                                      positionInWorldSpace[2])

                control.Show = True
                control.Visible = True

                controls.append(control)
            else:
                controls.append(mobu.FBVector3d(positionInWorldSpace[0],
                                                positionInWorldSpace[1],
                                                positionInWorldSpace[2]))

        return controls

    def purgeTemporaryPaths(self,
                            components):
        for node in components:
            node.FBDelete()

    def computePointOnCurve(self,
                            inputPoint,
                            sampleIndex,
                            threshold=0.03,
                            offsetIndex=1):
        pointData = PointOnCurve(inputPoint,
                                 self.curveSamples[sampleIndex],
                                 self.curveSamples[sampleIndex+offsetIndex])

        if pointData.startLength < threshold:
            knotData = CurveKnot()

            knotData.goalPoint = self.curveSamples[sampleIndex]

            knotData.samplingIndex = sampleIndex

            knotData.segmentPoints[0] = self.curveSamples[sampleIndex]

            knotData.segmentPoints[1] = self.curveSamples[sampleIndex+offsetIndex]

            return knotData

        if pointData.endLength < threshold:
            knotData = CurveKnot()

            knotData.goalPoint = self.curveSamples[sampleIndex+offsetIndex]

            knotData.samplingIndex = sampleIndex

            knotData.segmentPoints[0] = self.curveSamples[sampleIndex]

            knotData.segmentPoints[1] = self.curveSamples[sampleIndex+offsetIndex]

            return knotData

        pointData.computeFrame()

        if pointData.startAngle <= 90.0 and pointData.endAngle <= 95.0:
            self.startIndex = sampleIndex

            closestPoint = pointData.getClosestPoint()
            knotData = CurveKnot()

            knotData.samplingIndex = sampleIndex

            knotData.segmentPoints[0] = self.curveSamples[sampleIndex]

            knotData.segmentPoints[1] = self.curveSamples[sampleIndex+offsetIndex]

            if offsetIndex ==2:
                subData1 = PointOnCurve(closestPoint,
                                        self.curveSamples[sampleIndex],
                                        self.curveSamples[sampleIndex+1])

                subData1.computeFrame()

                subData2 = PointOnCurve(closestPoint,
                                        self.curveSamples[sampleIndex+1],
                                        self.curveSamples[sampleIndex+2])

                subData2.computeFrame()

                if subData1.startAngle <= 90.0 and subData1.endAngle <= 90.0:
                    closestPoint = subData1.getClosestPoint()

                    knotData.segmentPoints[0] = self.curveSamples[sampleIndex]

                    knotData.segmentPoints[1] = self.curveSamples[sampleIndex+1]

                else:
                    closestPoint = subData2.getClosestPoint()

                    knotData.samplingIndex = sampleIndex+1

                    knotData.segmentPoints[0] = self.curveSamples[sampleIndex+1]

                    knotData.segmentPoints[1] = self.curveSamples[sampleIndex+2]

            knotData.goalPoint = closestPoint

            return knotData

        return None

    def getClosestPointOnCurve(self,
                               inputPoint,
                               threshold=0.03):
        offsetIndex = 1
        for sampleIndex in xrange(self.startIndex,
                                  self.pointLimit):
            result = self.computePointOnCurve(inputPoint,
                                              sampleIndex,
                                              offsetIndex=offsetIndex,
                                              threshold=threshold)

            if result is not None:
                return result

        print 'Error', inputPoint, self.startIndex
        return None

        '''
            offsetIndex = 2
            for sampleIndex in xrange(self.startIndex,
                                      self.pointLimit-1):
                result = self.computePointOnCurve(inputPoint,
                                                  sampleIndex,
                                                  offsetIndex=offsetIndex,
                                                  threshold=threshold)

                if result is not None:
                    return result

            print 'Error', inputPoint, self.startIndex
            return None
        '''

    def createKnotNetwork(self,
                          knotData,
                          animationControllers,
                          blockOffset=0,
                          cvIndex=0):
        increment = 0
        indexArray = ['pointIndex0',
                      'pointIndex1',
                      'pointIndex2',
                      'pointIndex3']

        weightProperties = ['weight0',
                            'weight1',
                            'weight2',
                            'weight3']

        sumProperties = 'abcdefghij'

        offsetNode = self.relationConstraint.AddFunctionBox("Vector",
                                                            "Add (V1 + V2)")

        sumNode = self.relationConstraint.AddFunctionBox("Vector",
                                                         "Sum 10 Vectors")

        influenceArray = [1.0-knotData.segmentWeights,
                          knotData.segmentWeights]

        for knotIndex in xrange(2):
            for boxIndex in xrange(4):
                multiplyNode = self.relationConstraint.AddFunctionBox("Vector",
                                                                      "Scale (a x V)")

                controllerIndex = getattr(knotData.weightData[knotIndex],
                                          indexArray[boxIndex])

                sender = self.relationConstraint.AddSenderBox(animationControllers[controllerIndex])

                weightPlug = self.findFBboxInputPlug(multiplyNode.GetBox(),
                                                     'Number')

                weight = getattr(knotData.weightData[knotIndex],
                                 weightProperties[boxIndex])

                weightPlug.WriteData([weight*influenceArray[knotIndex]])

                self.relationConstraint.ConnectBoxes(sender,
                                                     'Translation',
                                                     multiplyNode,
                                                     'Vector')

                self.relationConstraint.ConnectBoxes(multiplyNode,
                                                     'Result',
                                                     sumNode,
                                                     sumProperties[increment])

                sender.SetBoxPosition(0,
                                      increment*\
                                      self.WEIGHT_COMPONENT_OFFSET+blockOffset)

                multiplyNode.SetBoxPosition(self.HORIZONTAL_OFFSET,
                                            increment*\
                                            self.WEIGHT_COMPONENT_OFFSET+blockOffset)

                increment+=1

        self.relationConstraint.ConnectBoxes(sumNode,
                                             'Result',
                                             offsetNode,
                                             'V2')

        offsetValue = knotData.sourcePoint - knotData.goalPoint
        offsetPlug = self.findFBboxInputPlug(offsetNode.GetBox(),
                                             'V1')

        offsetPlug.WriteData([offsetValue[0],
                              offsetValue[1],
                              offsetValue[2]])

        offsetPlug.WriteData([0, 0, 0])

        sumNode.SetBoxPosition(self.HORIZONTAL_OFFSET*2,
                               increment/3*\
                               self.WEIGHT_COMPONENT_OFFSET+blockOffset)

        offsetNode.SetBoxPosition(self.HORIZONTAL_OFFSET*3,
                                  increment/3*\
                                  self.WEIGHT_COMPONENT_OFFSET+blockOffset)

        knotData.createPoint()

        receiver = self.relationConstraint.AddReceiverBox(knotData.pointNode)

        self.relationConstraint.ConnectBoxes(offsetNode,
                                             'Result',
                                             receiver,
                                             'Translation')

        receiver.SetBoxPosition(self.HORIZONTAL_OFFSET*4,
                                increment/3*\
                                self.WEIGHT_COMPONENT_OFFSET+blockOffset)

        return knotData.pointNode

    def findFBboxInputPlug(self,
                           relationConstraintBox,
                           attributeName):
        for node in relationConstraintBox.AnimationNodeInGet().Nodes:
            if node.Name != attributeName:
                continue

            return node

        return None

    def emulateSkinCluster(self,
                           knotData,
                           sourceCurvePoint,
                           animationControllers,
                           constraintType=Constraint.PARENT_CHILD):
        weightList = [1.0-knotData.segmentWeights,
                      knotData.segmentWeights]

        influenceArraySource = {}

        for dataIndex, data in enumerate(knotData.weightData):
            for index in xrange(4):
                inControlIndex = getattr(data,
                                         self.indexAttributes[index])

                inWeight = getattr(data,
                                   self.weightAttributes[index])

                if inControlIndex not in influenceArraySource.keys():
                    influenceArraySource[inControlIndex] = inWeight*weightList[dataIndex]
                else:
                    influenceArraySource[inControlIndex] = influenceArraySource[inControlIndex] + inWeight*weightList[dataIndex]

        knotData.createPoint()

        matchingConstraint = Constraint.CreateConstraint(constraintType)
        matchingConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                        knotData.pointNode)

        influenceArray = {}
        for key in influenceArraySource:
            if influenceArraySource[key] < 0.01:
                continue
            influenceArray[key] = influenceArraySource[key]

        sortedInfluences = sorted(influenceArray.keys())

        controllerNames = []
        propertyArray = []
        for index in sortedInfluences:
            matchingConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                            animationControllers[index])
            controllerNames.append(animationControllers[index].LongName)

            propertyArray.append(0)

        for property in matchingConstraint.PropertyList:
            if not property.Name.endswith('.Weight'):
                continue

            propertyBase = property.Name.replace('.Weight',
                                                 '')

            writeIndex = controllerNames.index(propertyBase)

            propertyArray[writeIndex] = property

        matchingConstraint.Snap()

        for index, influence in enumerate(sortedInfluences):
            propertyArray[index].Data = influenceArray[influence]*100.0

        matchingConstraint.Active = True

        return knotData.pointNode, matchingConstraint

    def prepareCurveDrivers(self,
                            splineObject,
                            numberOfCurveController,
                            alignNodes,
                            rigPrefix='FK_'):
        self.curveSamples = []

        animPath = self.rebuildCurve(splineObject,
                                     numberOfCurveController,
                                     deleteInputCurve=False)

        animationControllers = self.getCurvePoints(animPath,
                                                   useMarkerShape=True)

        self.animationControllerGroup = mobu.FBModelNull('{0}{1}_curvePoints'.format(rigPrefix,
                                                                                     self.rigName))

        for controllerIndex, controller in enumerate(animationControllers):
            controller.Name = '{0}{1}_curvePoints1'.format(rigPrefix,
                                                            self.rigName)
            controller.Parent = self.animationControllerGroup

            targetMatrix = mobu.FBMatrix()
            alignNodes[controllerIndex].GetMatrix(targetMatrix)
            controller.SetMatrix(targetMatrix)

            constraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
            constraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                    controller)

            constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                    alignNodes[controllerIndex])

            constraint.Active = True
            constraint.Name = '{}_Attach1'.format(controller.Name)

            self.tangentFolder.ConnectSrc(constraint)

        animPath.FBDelete()

        curveWeightUtils = Tetrahedron.SplinePathSamples()
        curveWeightUtils.computeWeights()

        curveWeightUtils.computePathInterpolation(animationControllers)

        sourceCurvePointPositions = self.getCurvePoints(splineObject,
                                                        createNull=False)

        self.curveSamples = curveWeightUtils.curvePoints[:]

        self.pointLimit = len(self.curveSamples)-1

        self.startIndex = 0

        self.knotDataArray = []

        for pointIndex, point in enumerate(sourceCurvePointPositions):
            knotData = self.getClosestPointOnCurve(point)
            if knotData is None:
                print self.sourceCurvePointPositions

            knotData.weightData = [curveWeightUtils.curveKnotWeights[knotData.samplingIndex],
                                   curveWeightUtils.curveKnotWeights[knotData.samplingIndex+1]]

            knotData.sourcePoint = point

            knotData.blendWeights()

            self.knotDataArray.append(knotData)

        self.pointNodeArray = []
        self.pointNodeConstraintArray = []
        self.pointNodeArrayGroup = mobu.FBModelNull('{0}{1}_pathPoints'.format(rigPrefix,
                                                                               self.rigName))

        for knotIndex, knotData in enumerate(self.knotDataArray):
            pointNode, pointConstraint = self.emulateSkinCluster(knotData,
                                                                 sourceCurvePointPositions[knotIndex],
                                                                 animationControllers)

            pointNode.Name = '{0}{1}_pathPoint1'.format(rigPrefix,
                                                        self.rigName)

            pointConstraint.Name = '{0}_Attach1'.format(pointNode.Name)

            self.pointNodeArray.append(pointNode)

            self.pointNodeConstraintArray.append(pointConstraint)

            pointNode.Parent = self.pointNodeArrayGroup

        return animationControllers

    def buildTetrahedralConstraint(self,
                                   splineObject,
                                   animationControllers,
                                   rigPrefix):
        pointNodeArray = []

        relationConstraintName = '{0}_{1}_CurveDeform1'.format(rigPrefix,
                                                               self.rigName)
        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)
        self.relationConstraint.SetActive(True)

        blockOffset = self.WEIGHT_COMPONENT_OFFSET*8
        for knotIndex, knotData in enumerate(self.knotDataArray):
            pointNode = self.createKnotNetwork(knotData,
                                               animationControllers,
                                               blockOffset=blockOffset*knotIndex)

            pointNodeArray.append(pointNode)

        utils = IkRigging.IkSplineLayer()

        utils.constrainSplineToControls(splineObject,
                                        pointNodeArray)

        return animationControllers

    def rebuildCurve(self,
                     splineObject,
                     numberOfCurveController,
                     deleteInputCurve=True):
        jointMatrix = mobu.FBMatrix()
        CV_position = mobu.FBVector4d()

        position = mobu.FBVector3d()
        uRatio = 100.0  / float( numberOfCurveController-1)

        controlPath = mobu.FBModelPath3D('test1')
        controlPath.Show = True
        controlPath.Visible = True

        #clean mobu embarrsing default shape with two points...
        controlPath.PathKeyClear()
        mobu.FBSystem().Scene.Evaluate()

        curveMatrix = mobu.FBMatrix()
        splineObject.GetMatrix(curveMatrix)
        controlPath.SetMatrix(curveMatrix)

        loc = mobu.FBModelNull('locator1')
        loc.Show = True

        ikConstraint = self.ikSplineUtils.constraintUtils.createIkConstraint(splineObject,
                                                                             loc)

        mobu.FBSystem().Scene.Evaluate()

        ikConstraint.Active  = True
        uValueHandle = ikConstraint.PropertyList.Find('uValue1')

        positionInCurveSpace = mobu.FBVector4d()
        curveMatrix.Inverse()

        #Now resample the curve to the target numberOfCurveController
        for jointIndex  in range(numberOfCurveController):
            kPercentSource =  jointIndex*uRatio
            kPercent = splineObject.ConvertTotalPercentToSegmentPercent(kPercentSource)

            uValueHandle.Data = kPercent*100.0
            mobu.FBSystem().Scene.Evaluate()

            loc.GetVector(position)

            CV_position[0] = position[0]
            CV_position[1] = position[1]
            CV_position[2] = position[2]

            mobu.FBVectorMatrixMult(positionInCurveSpace, curveMatrix, CV_position)
            if jointIndex == 0:
                controlPath.PathKeyStartAdd(positionInCurveSpace)
            else:
                controlPath.PathKeyEndAdd(positionInCurveSpace)

        ikConstraint.Active = False
        ikConstraint.FBDelete()

        loc.FBDelete()

        if deleteInputCurve is True:
            splineObject.FBDelete()

        return controlPath

    def cloneComponents(self,
                        inputNodeArray):
        cloneNodeArray = []
        mobu.FBSystem().Scene.Evaluate()

        for node in inputNodeArray:
            targetMatrix = mobu.FBMatrix()
            cloneNode = mobu.FBModelNull('clone1')

            node.GetMatrix(targetMatrix)
            cloneNode.SetMatrix(targetMatrix)

            cloneNodeArray.append(cloneNode)

        mobu.FBSystem().Scene.Evaluate()

        return cloneNodeArray

    def prepareFkTargets(self):
        splineFkControlSettings = self.createFkRig(self.splineObject)

        fkTargets = self.cloneComponents(splineFkControlSettings.fk_controlList)

        self.attachJointCloneToSpline(self.splineObject,
                                      None,
                                      fkTargets,
                                      skipAfterSplineDistrution=True)

        fkZeroGroupArray = []
        for node in splineFkControlSettings.fk_controlList:
            fkZeroGroupArray.append(node.Parent)
            node.FBDelete()

        for node in fkZeroGroupArray:
            node.FBDelete()

        splineFkControlSettings.fkGroup.FBDelete()

        self.splineObject.FBDelete()

        self.splineObject = None

        return fkTargets

    def buildFkSpline(self,
                      numberOfCurveController,
                      pathObject):
        temporaryPath = None

        self.fkLineObject = self.rebuildCurve(pathObject,
                                              self.exposeCurvePointCount(),
                                              deleteInputCurve=False)
        Globals.Scene.Evaluate()

        temporaryPath = self.rebuildCurve(pathObject,
                                          numberOfCurveController,
                                          deleteInputCurve=False)

        Globals.Scene.Evaluate()

        lineFkControlSettings = self.createFkRig(self.fkLineObject,
                                                 temporaryPath=temporaryPath)

        temporaryPath.FBDelete()

        mobu.FBSystem().Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

        self.fkjointArray = self.cloneComponents(self.jointArray)

        mobu.FBSystem().Scene.Evaluate()

        fkSettings = self.attachJointCloneToSpline(self.fkLineObject,
                                                   lineFkControlSettings.fk_controlList,
                                                   self.fkjointArray)

        self.fkControlsRoot = mobu.FBModelNull('{0}_splineFkControls'.format(self.rigName))

        lineFkControlSettings.fk_controlList[0].Parent.Parent = self.fkControlsRoot

        self.packageFkRig(fkSettings)

        fkSettings.ikConstraint.Name = '{0}_FkSplineConstraint1'.format(self.rigName)
        fkSettings.anchorFolder = lineFkControlSettings.fkGroup

        return fkSettings

    def snapControllerToTargets(self,
                                fkTargets,
                                ikControllers,
                                fkControllers):
        mobu.FBSystem().Scene.Evaluate()

        for nodeIndex in xrange(len(fkTargets)):
            targetMatrix = mobu.FBMatrix()

            fkTargets[nodeIndex].GetMatrix(targetMatrix)
            fkControllers[nodeIndex].Parent.SetMatrix(targetMatrix)
            ikControllers[nodeIndex].SetMatrix(targetMatrix)

            mobu.FBSystem().Scene.Evaluate()

        mobu.FBSystem().Scene.Evaluate()
        for node in fkTargets:
            node.FBDelete()

        mobu.FBSystem().Scene.Evaluate()

    def attachGameBones(self):
        mobu.FBSystem().Scene.Evaluate()
        self.attachConstraints = []

        for jointIndex in xrange(len(self.pointNodeConstraintArray)):
            if jointIndex == 0 :
                self.curvePointFolder = mobu.FBFolder('{0}_curvePointConstraints1'.format(self.rigName),
                                                      self.pointNodeConstraintArray[jointIndex])
            else:
                self.curvePointFolder.ConnectSrc(self.pointNodeConstraintArray[jointIndex])

        for jointIndex in xrange(len(self.jointArray)):
            pointConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
            pointConstraint.Name = '{}_attach1'.format(self.jointArray[jointIndex].Name)

            pointConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                         self.jointArray[jointIndex])

            pointConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                         self.fkjointArray[jointIndex])

            pointConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                         self.ikjointArray[jointIndex])

            if jointIndex == 0 :
                self.rigAttachFolder = mobu.FBFolder('{0}_AttachConstraints1'.format(self.rigName),
                                                    pointConstraint)
            else:
                self.rigAttachFolder.ConnectSrc(pointConstraint)

            self.attachConstraints.append(pointConstraint)
            pointConstraint.Active = True
            pointConstraint.Lock = True

    def packageFkRig(self,
                     fkSettings):
        self.fkjointRoot = mobu.FBModelNull('{0}_splineFkJoints'.format(self.rigName))

        for nodeIndex, node in enumerate(self.fkjointArray):
            node.Parent = self.fkjointRoot
            node.Name = '{}_splineFkJoint{:02d}'.format(self.rigName,
                                                         nodeIndex)

        for controllerIndex, node in enumerate(fkSettings.fkControls):
            controlName = '{}_FkCtrl{:02d}'.format(self.rigName,
                                                  (controllerIndex+1))

            zroGrpName = '{}_zero'.format(controlName)
            node.Name = controlName
            node.Parent.Name = zroGrpName

    def setupIkRigOptions(self,
                          ikSplineUtils,
                          numberOfCurveController):
        ikSplineUtils.ikSplineSettings.ikPathName = 'strapIkPath1'
        ikSplineUtils.ikSplineSettings.numberOfCurveController = numberOfCurveController
        ikSplineUtils.ikSplineSettings.aimToNextJoint = True
        ikSplineUtils.ikSplineSettings.createFkControls = False
        ikSplineUtils.ikSplineSettings.createTweaker = False
        #ikSplineUtils.ikSplineSettings.rigStorage = 'Dummy01'
        ikSplineUtils.ikSplineSettings.ikControlsSize = 350
        ikSplineUtils.ikSplineSettings.maintainChainLength = True
        ikSplineUtils.ikSplineSettings.buildRigControl = True
        ikSplineUtils.ikSplineSettings.hideDrivenJoints = False
        ikSplineUtils.ikSplineSettings.createIntermediateGroup = False

    def buildIkSpline(self,
                      numberOfCurveController,
                      sourceControls,
                      pathObject=None):
        if pathObject is None:
            self.ikLineObject = self.createIkPath()

            self.ikLineObject = self.rebuildCurve(self.ikLineObject,
                                                  numberOfCurveController)
        else:
            self.ikLineObject = self.rebuildCurve(pathObject,
                                                  self.exposeCurvePointCount(),
                                                  deleteInputCurve=False)

        self.ikjointArray = self.cloneComponents(self.jointArray)

        ikSplineUtils = IkSplineCore.Setup()

        self.ikControlsRoot = mobu.FBModelNull('{0}_splineIkControls'.format(self.rigName))
        ikGroup = mobu.FBGroup('splineIk_IkControls')

        for node in self.ikjointArray:
            node.Visibility = True
            node.Show = True
            node.PropertyList.Find('Size').Data = 0.45

        ikTargets = []
        for controllerIndex, node in enumerate(sourceControls):
            controlName = '{}_IkCtrl{:02d}'.format(self.rigName,
                                                  (controllerIndex+1))

            ikControl = self.ikSplineUtils.ikSplineLayer.createControl(controlName,
                                                                       'IK_Control',
                                                                       250)

            ikTargets.append(ikControl)

            targetMatrix = mobu.FBMatrix()

            node.GetMatrix(targetMatrix)

            zroGrp = mobu.FBModelNull('{}_zero'.format(controlName))

            mobu.FBSystem().Scene.Evaluate()
            ikControl.Parent = zroGrp
            zroGrp.Parent = self.ikControlsRoot

            zroGrp.SetMatrix(targetMatrix)
            zroGrp.SetMatrix(targetMatrix)

            ikGroup.ConnectSrc(ikControl)

        ikSettings = self.attachJointCloneToSpline(self.ikLineObject,
                                                   ikTargets,
                                                   self.ikjointArray)

        self.ikjointRoot = mobu.FBModelNull('{0}_splineIkJoints'.format(self.rigName))
        for nodeIndex, node in enumerate(self.ikjointArray):
            node.Parent = self.ikjointRoot
            node.Name = '{}_splineIkJoint{:02d}'.format(self.rigName,
                                                         nodeIndex)

        ikSettings.ikConstraint.Name = '{0}_IkSplineConstraint1'.format(self.rigName)
        ikSettings.anchorFolder = ikGroup

        return ikSettings

    def prepareRigStructure(self,
                            inputPath,
                            controlsRoot,
                            jointsRoot,
                            rigRootName):
        rigRoot = mobu.FBModelNull(rigRootName)

        for node in (inputPath,
                     controlsRoot,
                     jointsRoot):
            if node is None:
                continue
            node.Parent = rigRoot

        return rigRoot

    def layoutRigChains(self):
        self.fkRigRoot = self.prepareRigStructure(self.fkLineObject,
                                                  self.fkControlsRoot,
                                                  self.fkjointRoot,
                                                  '{0}_fkRig'.format(self.rigName))

        self.ikRigRoot = self.prepareRigStructure(self.ikLineObject,
                                                  self.ikControlsRoot,
                                                  self.ikjointRoot,
                                                  '{0}_ikRig'.format(self.rigName))

        self.rigConstraintFolder = mobu.FBFolder('{0} Constraint 1'.format(self.rigName),
                                                 self.fkBuildSettings.ikConstraint)

        self.rigConstraintFolder.ConnectSrc(self.ikBuildSettings.ikConstraint)

        self.rigConstraintFolder.ConnectSrc(self.rigAttachFolder)

        self.rigConstraintFolder.ConnectSrc(self.curvePointFolder)

        self.rigConstraintFolder.ConnectSrc(self.tangentFolder)

        self.rigConstraintFolder.ConnectSrc(self.bindFolder)

        rigGroup = mobu.FBGroup('{0}_rig1'.format(self.rigName))
        rigGroup.ConnectSrc(self.fkBuildSettings.anchorFolder)
        rigGroup.ConnectSrc(self.ikBuildSettings.anchorFolder)

        self.fkIkRigRoot = mobu.FBModelNull('{}_RigRoot'.format(self.rigName))
        self.blendAttribute = self.fkIkRigRoot.PropertyCreate('fk/ik Weight',
                                                                  mobu.FBPropertyType.kFBPT_double,
                                                                  'Number',
                                                                  True,
                                                                  True,
                                                                  None)
        self.blendAttribute.SetAnimated(True)
        self.blendAttribute.SetMax(1.0)

        self.blendSource = self.fkIkRigRoot.PropertyCreate('weightSource',
                                                            mobu.FBPropertyType.kFBPT_Vector3D,
                                                            'Vector',
                                                            True,
                                                            True,
                                                            None)

        self.blendSource.Data = mobu.FBVector3d(1.0, 0.0, 0.0)
        self.blendSource.SetAnimated(True)

        self.blendTarget = self.fkIkRigRoot.PropertyCreate('weightTarget',
                                                            mobu.FBPropertyType.kFBPT_Vector3D,
                                                           'Vector',
                                                            True,
                                                            True,
                                                            None)

        self.blendTarget.Data = mobu.FBVector3d(0.0, 1.0, 0.0)
        self.blendTarget.SetAnimated(True)

        self.fkRigRoot.Parent = self.fkIkRigRoot
        self.ikRigRoot.Parent = self.fkIkRigRoot

        self.animationControllerGroup.Parent = self.fkIkRigRoot

        self.pointNodeArrayGroup.Parent = self.fkIkRigRoot

        self.chainPointsGroup.Parent = self.fkIkRigRoot

        self.animationControllerGroup.Visibility = False

        self.pointNodeArrayGroup.Visibility = False

    def createChainBlender(self):
        relationConstraintName = '{}_Relation'.format(self.rigName)
        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)
        self.relationConstraint.SetActive(True)

        self.rigConstraintFolder.ConnectSrc(self.relationConstraint.GetConstraint())

        for nodeIndex, attachNode in enumerate(self.attachConstraints):
            bindData = None
            if nodeIndex <= len(self.bindProperties)-1:
                bindData = self.bindProperties[nodeIndex]

            self.appendWeightBlenderNode(attachNode,
                                         bindData)
            self.increment += 1

    def appendWeightBlenderNode(self,
                                attachNode,
                                bindProperties):
        interpolateNode = self.relationConstraint.AddFunctionBox("Rotation",
                                                                 "Interpolate")

        sender1 = self.relationConstraint.AddSenderBox(self.fkIkRigRoot)

        self.relationConstraint.ConnectBoxes(sender1,
                                             'weightSource',
                                             interpolateNode,
                                             'Ra')

        self.relationConstraint.ConnectBoxes(sender1,
                                             'weightTarget',
                                             interpolateNode,
                                             'Rb')

        self.relationConstraint.ConnectBoxes(sender1,
                                             'fk/ik Weight',
                                             interpolateNode,
                                             'c')

        sender1.SetBoxPosition(0,
                               self.increment*\
                               self.SHIFT_COMPONENT_OFFSET)

        weightProperties = []
        receiver = self.relationConstraint.AddReceiverBox(attachNode)

        for property in attachNode.PropertyList:
            if not property.Name.endswith('.{0}'.format(self.WEIGHT_PROPERTY_SUFFIX)):
                continue

            property.SetAnimated(True)
            weightProperties.append(property.Name)

        outputNode = self.relationConstraint.AddFunctionBox("Converters",
                                                            "Vector To Number")

        self.relationConstraint.ConnectBoxes(interpolateNode,
                                             'Result',
                                             outputNode,
                                             'V')

        self.relationConstraint.ConnectBoxes(outputNode,
                                             'X',
                                             receiver,
                                             weightProperties[0])

        self.relationConstraint.ConnectBoxes(outputNode,
                                             'Y',
                                             receiver,
                                             weightProperties[1])

        outputNode.SetBoxPosition(2*self.HORIZONTAL_OFFSET,
                                  self.increment*\
                                  self.SHIFT_COMPONENT_OFFSET)

        receiver.SetBoxPosition(3*self.HORIZONTAL_OFFSET,
                                self.increment*\
                                self.SHIFT_COMPONENT_OFFSET + 60)

        if bindProperties is not None:
            receiver2 = self.relationConstraint.AddReceiverBox(bindProperties[0])

            receiver2.SetBoxPosition(3*self.HORIZONTAL_OFFSET,
                                     self.increment*\
                                     self.SHIFT_COMPONENT_OFFSET - 60)

            self.relationConstraint.ConnectBoxes(outputNode,
                                                 'X',
                                                 receiver2,
                                                 bindProperties[1])

            self.relationConstraint.ConnectBoxes(outputNode,
                                                 'Y',
                                                 receiver2,
                                                 bindProperties[2])

        interpolateNode.SetBoxPosition(self.HORIZONTAL_OFFSET,
                                       self.increment*\
                                       self.SHIFT_COMPONENT_OFFSET)

        interpolateNode.GetBox().Name = '{}_interpolate1'.format(attachNode.Name)

        outputNode.GetBox().Name = '{}_vectorToNumber1'.format(attachNode.Name)

    def hideIntermediates(self):
        for node in self.fkjointArray:
            node.PropertyList.Find('Look').Data = 0
            node.PropertyList.Visibility = False
            node.PropertyList.Show = False

        for node in self.ikjointArray:
            node.PropertyList.Find('Look').Data = 0
            node.PropertyList.Visibility = False
            node.PropertyList.Show = False

    def bindRigLayers(self,
                      rigStorage,
                      restorePose=False):
        if restorePose is True:
            self.restoreBindPose(self.bindPoses)

            self.snapControllerToTargets(self.fkTargets,
                                         self.ikBuildSettings.fkControls,
                                         self.fkBuildSettings.fkControls)

        self.attachGameBones()

        self.layoutRigChains()

        self.createChainBlender()

        self.hideIntermediates()

        finalizeUtils = FkIkRigStructure()

        finalizeUtils.build(self.fkIkRigRoot,
                            self.fkRigRoot,
                            self.ikRigRoot,
                            self.fkBuildSettings.fkControls,
                            self.ikBuildSettings.fkControls,
                            self.jointArray)

        rigStorageNode = Scene.FindModelByName(rigStorage)

        if rigStorageNode is not None:
            self.fkIkRigRoot.Parent = rigStorageNode

    def exposeCurvePointCount(self):
        return self.splineObject.PathKeyGetCount()

    def createPathFromJoints(self,
                             jointObjectArray,
                             name='splineIkPath1',
                             upReferenceVector=mobu.FBVector3d(0.0,
                                                               10.0,
                                                               0.0)):
        controlPath = mobu.FBModelPath3D('{}_Spline1'.format(name))
        controlPath.Show = True
        controlPath.Visible = True

        jointMatrix = mobu.FBMatrix()
        CV_position = mobu.FBVector4d()

        position = mobu.FBVector3d()
        positionOld = mobu.FBVector3d()

        #clean mobu embarrsing default shape with two points...
        controlPath.PathKeyClear()

        Globals.Scene.Evaluate()

        #OrienCurve here
        FirstCv = mobu.FBVector3d()
        SecondCv = mobu.FBVector3d()

        jointObjectArray[0].GetVector(FirstCv)
        jointObjectArray[1].GetVector(SecondCv)

        if (SecondCv-FirstCv).Length() < 0.0001:
            jointObjectArray[3].GetVector(SecondCv)

        upPos = mobu.FBModelNull('upPos')
        upPos.Show = True

        basePos = mobu.FBModelNull('basePos')
        basePos.Show = True

        targetPos = mobu.FBModelNull('targetPos')
        targetPos.Show = True

        upPos.Parent = basePos
        upPos.PropertyList.Find('Translation (Lcl)').Data = upReferenceVector
        basePos.SetVector(FirstCv)
        targetPos.SetVector(SecondCv)

        posConstraint = Constraint.CreateConstraint(Constraint.POSITION)
        posConstraint.ReferenceAdd (0, controlPath)
        posConstraint.ReferenceAdd (1, basePos)

        Globals.Scene.Evaluate()
        posConstraint.Active = True

        controlPath.LookAt = targetPos
        controlPath.UpVector = upPos

        Globals.Scene.Evaluate()
        rotation = controlPath.PropertyList.Find('Rotation (lcl)').Data

        Globals.Scene.Evaluate()

        constraintUtils = IkUtils.ConstraintUtils()
        constraintUtils.bakeConstraintValue(posConstraint)

        for model in [basePos,
                      targetPos,
                      upPos]:
            model.FBDelete()

        Globals.Scene.Evaluate()
        controlPath.PropertyList.Find('Rotation (Lcl)').Data = rotation 

        Globals.Scene.Evaluate()

        curveMatrix = mobu.FBMatrix()
        curveSpace = controlPath.GetMatrix(curveMatrix)

        controlPath.SetMatrix(curveMatrix)
        positionInCurveSpace = mobu.FBVector4d()
        curveMatrix.Inverse()

        controlPath.PropertyList.Find('Rotation (Lcl)').Data = rotation 
        Globals.Scene.Evaluate()

        #Create a CV_Point for each joint
        for jointIndex, joint in enumerate(jointObjectArray):
            jointObjectArray[jointIndex].GetVector(position)
            CV_position[0] = position[0]
            CV_position[1] = position[1]
            CV_position[2] = position[2]

            mobu.FBVectorMatrixMult(positionInCurveSpace,
                                    curveMatrix,
                                    CV_position)


            if jointIndex == 0:
                controlPath.PathKeyStartAdd(positionInCurveSpace)
            else:
                controlPath.PathKeyEndAdd(positionInCurveSpace)


        Globals.Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

        return controlPath

    def attachCurves(self,
                     numberOfCurveController):
        self.createTangentRig(self.fkBuildSettings.fkControls)

        alignNodes = [node.tangent for node in self.tangentDataArray]

        splineKnots = self.prepareCurveDrivers(self.splineObject,
                                               numberOfCurveController,
                                               alignNodes,
                                               rigPrefix='')

        utils = IkRigging.IkSplineLayer()

        utils.constrainSplineToControls(self.fkLineObject,
                                        self.pointNodeArray)

        utils.constrainSplineToControls(self.ikLineObject,
                                        self.pointNodeArray)

        self.bindToFkIkControls()

        self.fkLineObject.Name = '{0}_fkRigPath'.format(self.rigName)

        self.ikLineObject.Name = '{0}_ikRigPath'.format(self.rigName)

    def bindToFkIkControls(self):
        self.bindProperties = []

        for nodeIndex in xrange(len(self.chainNodes)):
            bindConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
            bindConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                        self.chainNodes[nodeIndex])

            bindConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                        self.fkBuildSettings.fkControls[nodeIndex])

            bindConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                        self.ikBuildSettings.fkControls[nodeIndex])

            bindConstraint.Name = '{}_bind1'.format(self.chainNodes[nodeIndex].Name)

            bindConstraint.Active = True

            weightProperties = [bindConstraint]
            for property in bindConstraint.PropertyList:
                if not property.Name.endswith('.{0}'.format(self.WEIGHT_PROPERTY_SUFFIX)):
                    continue

                property.SetAnimated(True)
                weightProperties.append(property.Name)

            self.bindProperties.append(weightProperties)

            if nodeIndex == 0:
                self.bindFolder = mobu.FBFolder('{0} bindConstraints 1'.format(self.rigName),
                                                                               bindConstraint)
            else:
                self.bindFolder.ConnectSrc(bindConstraint)

    def createTangentRig(self,
                         inputNodeArray,
                         offsetRotation=mobu.FBVector3d(0, 0, 0)):
        self.chainNodes = []

        self.tangentDataArray = []

        self.chainPointsGroup = mobu.FBModelNull('{0}_chainPoints'.format(self.rigName))

        offsetMatrix = mobu.FBMatrix()
        
        mobu.FBRotationToMatrix(offsetMatrix,   
                                offsetRotation, 
                                mobu.FBRotationOrder.kFBXYZ)

        for node in inputNodeArray:
            cloneNode = mobu.FBModelNull('{0}_chainPoint1'.format(self.rigName))

            nodeMatrix = mobu.FBMatrix()
            node.GetMatrix(nodeMatrix)

            nodeMatrix = nodeMatrix*offsetMatrix

            cloneNode.Parent = self.chainPointsGroup

            cloneNode.SetMatrix(nodeMatrix)

            self.chainNodes.append(cloneNode)

        for nodeIndex in xrange(len(self.chainNodes)):
            previousIndex = nodeIndex-1
            if previousIndex < 0:
                previousIndex = 0

            nextIndex = nodeIndex+1
            if nextIndex > len(self.chainNodes)-1:
                nextIndex = len(self.chainNodes)-1

            tangent = TangentSegment()

            tangent.buildFromNodes(self.chainNodes[nodeIndex],
                                   self.chainNodes[previousIndex],
                                   self.chainNodes[nextIndex])

            if nodeIndex == 0:
                self.tangentFolder = mobu.FBFolder('{0} tangentConstraints 1'.format(self.rigName),
                                                                                     tangent.targetConstraint)

                self.tangentFolder.ConnectSrc(tangent.orientOrient)
            else:
                self.tangentFolder.ConnectSrc(tangent.targetConstraint)

                self.tangentFolder.ConnectSrc(tangent.orientOrient)

            self.tangentDataArray.append(tangent)

    def createFromPath(self,
                       rootName,
                       rigName,
                       numberOfCurveController,
                       rigStorage,
                       upReferenceVector=mobu.FBVector3d(0.0,
                                                         10.0,
                                                         0.0)):
        self.rigName = rigName

        rootNode = Scene.FindModelByName(rootName)

        if rootNode is None:
            print 'Sorry, could not find {} in your scene'.format(rootName)

            return None

        self.jointArray = self.ikSplineUtils.jointChain.getJointChainFromRoot(rootName)

        self.splineObject = self.createPathFromJoints(self.jointArray,
                                                      name=rigName,
                                                      upReferenceVector=upReferenceVector)

        self.ikSplineUtils.ikSplineSettings.numberOfCurveController = numberOfCurveController

        numberOfCurveController = self.ikSplineUtils.ikSplineSettings.numberOfCurveController

        self.fkBuildSettings = self.buildFkSpline(numberOfCurveController,
                                                  pathObject=self.splineObject)

        self.ikBuildSettings = self.buildIkSpline(numberOfCurveController,
                                                  self.fkBuildSettings.fkControls,
                                                  pathObject=self.splineObject)

        self.attachCurves(numberOfCurveController)

        self.splineObject.FBDelete()

        self.bindRigLayers(rigStorage,
                           restorePose=False)

        #parentJoint under skelRoot in order to be compatible with pelt/snake rig plot tool
        print 'Joint under {} were rigged properly'.format(rootName)

        return self.fkIkRigRoot


class AnimationManager(object):
    def __init__(self):
        self.fkIkRigRoot = None

        self.currentCharacter = None

        self.assetNamespace = None

        self.inputChainRigs = []

        self.rigUtils = FkIkRigStructure()

    def collectChainRig(self):
        fkIkRoots = self.rigUtils.collectFkIkRoots()

        if fkIkRoots is None:
            return None

        return fkIkRoots

    def exposeDrivenJoints(self,
                           splineIkRig,
                           jointLinkArray):
        jointArray = []

        for jointLink in jointLinkArray:
            metaData = splineIkRig.PropertyList.Find(jointLink)
            numberOfDrivenJoints = metaData.GetSrcCount()

            for memberIndex in range(numberOfDrivenJoints):
                member = metaData.GetSrc(memberIndex)
                if member is None:
                    continue

                jointArray.append(member)

        return jointArray

    def getIkFkChains(self):
        ikFkChains = []

        targetNodeList = mobu.FBComponentList()
        mobu.FBFindObjectsByName('*',
                                 targetNodeList,
                                 False,
                                 True)

        for modelNode in targetNodeList:
            property = modelNode.PropertyList.Find('ikSplineMetaRoot')
            if property == None:
                continue

            if property.Data != FkIkRigStructure.METADATA_ROOT_VALUE:
                continue

            ikFkChains.append(modelNode.LongName)

        return ikFkChains

    def getRootFromName(self,
                        inputName):
        targetNodeList = mobu.FBComponentList()
        sampleName = str(inputName)
        if ':' in sampleName:
            mobu.FBFindObjectsByName('{0}'.format(sampleName),
                                     targetNodeList,
                                     True,
                                     True)
        else:
            mobu.FBFindObjectsByName('{0}'.format(sampleName),
                                     targetNodeList,
                                     True,
                                     False)

        if len(targetNodeList)==0:
            return None

        return targetNodeList[0]

    def collectComponents(self,
                          inputChainNames):
        self.inputChainRigs = []

        for chain in inputChainNames:
            inputRootNode = self.getRootFromName(chain)

            if inputRootNode is None:
                continue

            rigUtils = FkIkRigStructure()
            rigUtils.populateRigStructure(inputRootNode)
            self.inputChainRigs.append(rigUtils)

    def alignAnimationControllers(self,
                                  fkToIk=True,
                                  setkeys=False):
        if len(self.inputChainRigs)==0:
            return

        chainControllers = []
        for chain in self.inputChainRigs:
            for inputNode in chain.modelList:
                chainControllers.append(inputNode)

        if len(chainControllers)==0:
            return

        attributeArray = []
        for chain in self.inputChainRigs:
            for inputNode in chain.modelList:
                attributeArray.append(inputNode.PropertyList.Find('Translation (Lcl)'))
                attributeArray.append(inputNode.PropertyList.Find('Rotation (Lcl)'))
                attributeArray.append(inputNode.PropertyList.Find('Scaling (Lcl)'))

        for property in attributeArray:
            if not property.IsAnimated():
                property.SetAnimated(True)

        with ContextManagers.Undo(undoStackName='alignIkFkChains',
                                  attributeArray=attributeArray):
            for chain in self.inputChainRigs:
                chain.alignAnimationControllers(fkToIk=fkToIk)

            if setkeys is True:
                for property in attributeArray:
                    self.setKeysOnProperties(property)

    def setKeysOnProperties(self,
                            animationAttribute):
        animationNode = animationAttribute.GetAnimationNode()
        if animationNode is None:
            return

        animationNode.KeyCandidate()

    def exposeRigElements(self,
                          showFkControllers=True,
                          showIkControllers=True):
        if len(self.inputChainRigs)==0:
            return

        attributeArray = []
        for chain in self.inputChainRigs:
            for inputNode in chain.modelList:
                attributeArray.append(inputNode.PropertyList.Find('Show'))
                attributeArray.append(inputNode.PropertyList.Find('Visibility'))

        with ContextManagers.Undo(undoStackName='toggleIkFkChains',
                                  attributeArray=attributeArray):
            for chain in self.inputChainRigs:
                chain.exposeRigElements(showFkControllers=showFkControllers,
                                        showIkControllers=showIkControllers)

    def plotAnimation(self):
        pass