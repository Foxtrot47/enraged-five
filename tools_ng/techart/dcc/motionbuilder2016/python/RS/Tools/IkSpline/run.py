from RS.Tools.IkSpline.Dialogs import ikSplineToolDialog

reload(ikSplineToolDialog)
def Run(show=True):
    tool = ikSplineToolDialog.IkSplineTool()

    if show: 
        tool.show()

    return tool
