"""
## Description : This module will create a splineIK rig.
## Summary    : Ik Spline in MotionBuilder.
## bugTracker : 2515761.
"""

import pyfbsdk as mobu
import math
import collections
import copy


class MVector(object):
    """
    Data are expexted to be distance in cm mostly
    """
    def __init__(self,x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z
    
    def normalize(self,inputVector):
        targetLength = mobu.FBLength(inputVector)
        normValue = 1.0/targetLength
        inputVector[0] *= normValue
        inputVector[1] *= normValue
        inputVector[2] *= normValue

    def rotateTo(self, targetPoint):
        xAxis = mobu.FBVector4d(1.0,0.0,0.0,1.0)
        self.normalize(targetPoint)

        quaternionAxis = mobu.FBVector4d()
        mobu.FBMult(quaternionAxis,xAxis,targetPoint)
        self.normalize(quaternionAxis)

        quaternionAngle = math.acos(mobu.FBDot(xAxis,targetPoint))
        aimQuaternion = self.axisAngleToQuaternion(quaternionAxis,quaternionAngle)

        return aimQuaternion
    
    def getAimVector(self, aimObject, parentSpace):
        targetVector = mobu.FBVector3d()
        targetPoint = mobu.FBVector4d()

        aimObject.GetVector(targetVector,
                            mobu.FBModelTransformationType.kModelTranslation,
                            True)
        targetPoint[0] = targetVector[0]
        targetPoint[1] = targetVector[1]
        targetPoint[2] = targetVector[2]

        aimPoint = mobu.FBVector4d()
        mobu.FBVectorMatrixMult(aimPoint,
                                parentSpace,
                                targetPoint)

        return aimPoint

    def aimObjects(self,
                   startObject, 
                   endObject):
        parentSpace = mobu.FBMatrix()
        parentSpaceInverse = mobu.FBMatrix()
        startObject.GetMatrix(parentSpace)
        startObject.GetMatrix(parentSpaceInverse)

        parentSpaceInverse.Inverse()

        targetPoint = self.getAimVector(endObject, parentSpaceInverse)
        aimQuaternion = self.rotateTo(targetPoint)

        quatFn = Quaternion()
        aimValue = aimQuaternion.toEulerAngles()
        aimOffsetRotation = mobu.FBVector3d(aimValue.x,aimValue.y,aimValue.z)

        loc = mobu.FBModelNull('loc1')
        loc.Parent = startObject

        loc.SetVector(mobu.FBVector3d(), mobu.FBModelTransformationType.kModelRotation, False)
        loc.SetVector(mobu.FBVector3d(), mobu.FBModelTransformationType.kModelTranslation, False)
        loc.SetVector(aimOffsetRotation, mobu.FBModelTransformationType.kModelRotation, False)

        loc.GetVector(aimOffsetRotation, mobu.FBModelTransformationType.kModelRotation, True)
        loc.FBDelete()

        startObject.SetVector(aimOffsetRotation, mobu.FBModelTransformationType.kModelRotation, True)

    def axisAngleToQuaternion(self,axis,angle):
        #http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
        outputQuat = Quaternion()
        s = math.sin(angle*0.5)

        outputQuat.x = axis[0] * s;
        outputQuat.y = axis[1] * s;
        outputQuat.z = axis[2] * s;
        outputQuat.w = math.cos(angle*0.5)
        
        return outputQuat


class euler3f(object):
    """
    Data are expexted to be angle in radians
    """
    def __init__(self,x, y, z):
        self.x = x
        self.y = y
        self.z = z


class Quaternion(object):
    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        self.w = 1.0
        
    def getAimMatrix(self, localMatrix, inputPoint):
        localMatrixInverse.FBMatrix(localMatrix)
        localMatrixInverse.Inverse()

        targetPoint = mobu.FBVector4d()
        FBVectorMatrixMult(targetPoint, localMatrixInverse, inputPoint)
        aimDistance = mobu.FBLength(targetPoint)

        if  aimDistance < 0.0001 :
            return mobu.FBMatrix(localMatrix)

        aimMatrix = self.rotateTo(targetPoint)

        return mobu.FBMatrix(localMatrix*aimMatrix)

    def crossProduct(self, left, right):
        planeNormal = mobu.FBVector4d()

        planeNormal[0]= left[1]*right[2]- left[2]*right[1]
        planeNormal[1]= left[2]*right[0]- left[0]*right[2]
        planeNormal[2]= left[0]*right[1]- left[1]*right[0]

        return planeNormal
    
    def rotateTo(self, targetPoint):
        xAxis[0] = 1.0;
        xAxis[1] = 0.0;
        xAxis[2] = 0.0;
        xAxis[3] = 1.0;
        #BUILD quaternionAxis
        targetPointCopy = self.normalFromVector(targetPoint)
        quaternionAxis = self.crossProduct(xAxis,targetPointCopy)
        quaternionAxisNormalized = self.normalFromVector(quaternionAxis)

        #Extract angle between
        quaternionAngle = self.vectorAngle(xAxis, targetPointCopy)
        #Extract Quaternion
        outputQuat = self.axisAngleToQuaternion(quaternionAxisNormalized,
                                           quaternionAngle)

        #Convert into a matrix
        aimMatrix = self.outputAimMatrix()

    def axisAngleToQuaternion(self, 
                              quaternionAxisNormalized,
                              quaternionAngle):
        s = math.sin(quaternionAngle*0.5)

        outputQuat = Quaternion()
        outputQuat.x = quaternionAxisNormalized[0] * s
        outputQuat.y = quaternionAxisNormalized[1] * s
        outputQuat.z = quaternionAxisNormalized[2] * s
        outputQuat.w = cos(quaternionAngle*0.5)
        
        return outputQuat

    def outputAimMatrix(self, outputQuat):
        aimRotation = outputQuat.toEulerAngles()

        resultOrientation = mobu.FBRVector()

        resultOrientation[0] = aimRotation.x;
        resultOrientation[1] = aimRotation.y;
        resultOrientation[2] = aimRotation.z;

        aimMatrix = mobu.FBMatrix()

        mobu.FBRotationToMatrix(aimMatrix,resultOrientation)
        return aimMatrix

    def normalFromVector(self, inputVector,reverse):
        normal = mobu.FBVector4d(inputVector[0], 
                                 inputVector[1],
                                 inputVector[2],
                                 1.0)

        if reverse is True:
            normal[0] = inputVector[0] * -1.0;
            normal[1] = inputVector[1] * -1.0;
            normal[2] = inputVector[2] * -1.0;

        len = mobu.FBLength(normal)

        if len != 0.0:
            len = 1.0 / len;
            normal[0] *= len;
            normal[1] *= len;
            normal[2] *= len;

        return normal

    def vectorAngle(self, edgeA, edgeB):
        #Unfortunately dotProduct in mobu needs normalized vector(python).... ( OpenMaya.MVector.angle doesnt)
        edgeA_Normal = self.normalFromVector(edgeA)
        edgeB_Normal = self.normalFromVector(edgeB)

        out_dotProduct = mobu.FBDot(edgeA_Normal,edgeB_Normal)
        if (out_dotProduct > -1.0 and out_dotProduct < 1.0 ):
            return math.acos(out_dotProduct)

        if ( out_dotProduct > 0.0):
            return 0.0;

        if ( out_dotProduct < 0.0):
            return math.pi + 0.0

        return 0.0

    def copyFrom(self, sourceQuat):
        self.x = sourceQuat.x
        self.y = sourceQuat.y
        self.z = sourceQuat.z
        self.w = sourceQuat.w

    def quaternionFromAngle(self, angles):    
        #http://willperone.net/Code/quaternion.php
        cos_z_2 = math.cos(0.5*math.radians(angles.z))
        cos_y_2 = math.cos(0.5*math.radians(angles.y))
        cos_x_2 = math.cos(0.5*math.radians(angles.x))
        sin_z_2 = math.sin(0.5*math.radians(angles.z))
        sin_y_2 = math.sin(0.5*math.radians(angles.y))
        sin_x_2 = math.sin(0.5*math.radians(angles.x))
        
        v = Quaternion()
        #// and now compute quaternion
        v.w   = cos_z_2*cos_y_2*cos_x_2 + sin_z_2*sin_y_2*sin_x_2;
        v.x =  cos_z_2*cos_y_2*sin_x_2 - sin_z_2*sin_y_2*cos_x_2;
        v.y = cos_z_2*sin_y_2*cos_x_2 + sin_z_2*cos_y_2*sin_x_2;
        v.z =  sin_z_2*cos_y_2*cos_x_2 - cos_z_2*sin_y_2*sin_x_2;    
        
        return v

    def scaleIt(self, weight):
        self.x = self.x*weight
        self.y = self.y*weight
        self.z = self.z*weight
        self.w = self.w*weight
    
    def combine(self, secondQuat):
        self.x += secondQuat.x 
        self.y += secondQuat.y 
        self.z += secondQuat.z 
        self.w += secondQuat.w 
    
    def negateIt(self):
        self.x *= -1.0 
        self.y *= -1.0 
        self.z *= -1.0 
        self.w *= -1.0 

    def quaternionSlerp(self, startQuaternion, endQuaternion, blendValue):
        #https://groups.google.com/forum/#!topic/python_inside_maya/Dvkj1OCFcX0
        cos = self.quaternionDot(startQuaternion, endQuaternion)
        if cos < 0.0:
            endQuaternion.negateIt()
            cos = self.quaternionDot(startQuaternion, endQuaternion)
        theta = math.acos(cos)
        sin = math.sin(theta)

        if sin > 0.001:
            weight1 = math.sin((1.0 - blendValue) * theta) / sin
            weight2 = math.sin(blendValue * theta) / sin
        else:
            weight1 = 1.0 - blendValue
            weight2 = blendValue

        startQuaternionCopy = Quaternion()
        endQuaternionCopy = Quaternion()

        startQuaternionCopy.copyFrom(startQuaternion)
        endQuaternionCopy.copyFrom(endQuaternion)
        
        startQuaternionCopy.scaleIt(weight1)
        endQuaternionCopy.scaleIt(weight2)

        startQuaternionCopy.combine(endQuaternionCopy)

        return startQuaternionCopy

    def quaternionDot(self, q1, q2):
        return (q1.x * q2.x) + (q1.y * q2.y) + (q1.z * q2.z) + (q1.w * q2.w)
        
    def toEulerAngles(self, homogenous=True):
        sqw = self.w*self.w;    
        sqx = self.x*self.x;    
        sqy = self.y*self.y;    
        sqz = self.z*self.z;    

        euler = euler3f(0,0,0)
        if  homogenous is True:
            euler.x = math.atan2(float(2.0)* (self.x*self.y + self.z*self.w), sqx - sqy - sqz + sqw) 
            euler.y = math.asin(-float(2.0) * (self.x*self.z - self.y*self.w)) 
            euler.z = math.atan2(float(2.0) * (self.y*self.z + self.x*self.w), -sqx - sqy + sqz + sqw)   
        else:
            euler.x = math.atan2(float(2.0) * (self.z*self.y + self.x*self.w), 1 - 2*(sqx + sqy)) 
            euler.y = math.asin(-float(2.0) * (self.x*self.z - self.y*self.w)) 
            euler.z = math.atan2(float(2.0) * (self.x*self.y + self.z*self.w), 1 - 2*(sqy + sqz)) 

        dup  = [math.degrees(euler.x),math.degrees(euler.y),math.degrees(euler.z)]
        euler.x = dup[2]
        euler.y = dup[1]
        euler.z = dup[0]

        return euler 

    def interpolatePos(self, objA,objB,weight):
        e1Vec = mobu.FBVector3d()
        objA.GetVector(e1Vec, mobu.FBModelTransformationType.kModelTranslation, False)

        e2Vec = mobu.FBVector3d()
        objB.GetVector(e2Vec, mobu.FBModelTransformationType.kModelTranslation, False)
        
        e3Vec = mobu.FBVector3d()
        
        for k in range(3):
            e3Vec[k] = (e1Vec[k] * (1.0-weight)) +  (e2Vec[k] * ( weight))
        
        return e3Vec

    def blendObjectTransformation(self,
                                  startObjectName, 
                                  endObjectName,
                                  blendObjectName):

        startObjectList = mobu.FBComponentList()
        mobu.FBFindObjectsByName('{0}*'.format(startObjectName),
                                 startObjectList,
                                 False,
                                 False)

        endObjectList = mobu.FBComponentList()
        mobu.FBFindObjectsByName('{0}*'.format(endObjectName),
                                 endObjectList,
                                 False,
                                 False)

        blendObjectList = mobu.FBComponentList()
        mobu.FBFindObjectsByName('{0}*'.format(blendObjectName),
                                 blendObjectList,
                                 False,
                                 False)

        e1Vec = mobu.FBVector3d()
        startObjectList[0].GetVector(e1Vec, mobu.FBModelTransformationType.kModelRotation, False)

        e2Vec = mobu.FBVector3d()
        endObjectList[0].GetVector(e2Vec, mobu.FBModelTransformationType.kModelRotation, False)

        quatFn = Quaternion()

        e1 = euler3f(e1Vec[0],e1Vec[1],e1Vec[2])
        e2 = euler3f(e2Vec[0],e2Vec[1],e2Vec[2])

        q1 = quatFn.quaternionFromAngle(e1)
        q2 = quatFn.quaternionFromAngle(e2)

        ratio = 1.0/9.0
        for copyIndex in range(10):
            dup = copy.copy(blendObjectList[0])
            wgt = copyIndex*ratio
            q3 = quatFn.quaternionSlerp(q1, q2, wgt)
            e3 = q3.toEulerAngles()
            e3Vec = mobu.FBVector3d(e3.x,e3.y,e3.z)

            dup.SetVector(e3Vec, mobu.FBModelTransformationType.kModelRotation, False)
            
            e3Pos = self.interpolatePos(startObjectList[0],endObjectList[0],wgt)
            dup.SetVector(e3Pos, mobu.FBModelTransformationType.kModelTranslation, False)


class TriangleFrame(object):
    def __init__(self):
        self.startEdge_length = 0.0 #distance
        self.endEdge_length = 0.0  #distance
        self.slideEdge_length = 0.0  #distance
        
        self.angleI = 0.0 #radians...
        self.angleJ = 0.0 #radians...
        self.angleK = 0.0 #radians...

        self.pointI = mobu.FBVector3d()
        self.pointJ = mobu.FBVector3d()
        self.pointK = mobu.FBVector3d()

        self.startEdge = mobu.FBVector3d()
        self.endEdge = mobu.FBVector3d()
        self.slideEdge = mobu.FBVector3d()

        self.slideEdgeNormalize = mobu.FBVector3d()
        self.wedgeAngle = 0.0
        self.sliceAngle = 0.0

        self.currentChainSegment = 0.0
        self.targetPoint  = mobu.FBVector3d()
        self.sweepAmount = 0.0

        self.debugSolver = False

    def setpoints(self,pointI,pointJ,pointK):
        self.pointI = mobu.FBVector3d(pointI)
        self.pointJ = mobu.FBVector3d(pointJ)
        self.pointK = mobu.FBVector3d(pointK)

    def vectorAngle(self, edgeA, edgeB):
        #Unfortunately dotProduct in mobu needs normalize vector.... ( OpenMaya.MVector.angle doesnt)
        dotProduct = edgeA.DotProduct(edgeB)

        outputAngle = 0.0

        if dotProduct > -1.0 and dotProduct < 1.0:
            outputAngle = math.acos(dotProduct)
            return outputAngle

        if dotProduct > 0.0:
            return  0.0

        if dotProduct < 0.0:
            return math.pi

    def reverseVector(self, inputVector):
        reverse = mobu.FBVector3d(inputVector)

        reverse[0] = inputVector[0] * -1.0
        reverse[1] = inputVector[1] * -1.0
        reverse[2] = inputVector[2] * -1.0
            
        return inputVector

    def normalFromVector(self,inputVector,reverse=False):
        normal = mobu.FBVector3d(inputVector)

        if reverse is True:
            normal[0] = inputVector[0] * -1.0
            normal[1] = inputVector[1] * -1.0
            normal[2] = inputVector[2] * -1.0

        normal.Normalize()

        return normal

    def build(self):
        #------------------------------------------------------------Collect segment
        #startEdge is line from sampling point I to start for sweeping segment
        self.startEdge = self.pointJ - self.pointI

        #endEdge is line from sampling point I to end for sweeping segment
        self.endEdge = self.pointK - self.pointI
        
        #slideEdge is our sweeping segment 
        self.slideEdge = self.pointK - self.pointJ  
        
        #------------------------------------------------------------Collect segment length
        self.startEdge_length = self.startEdge.Length()
        self.endEdge_length = self.endEdge.Length()
        self.slideEdge_length = self.slideEdge.Length()

        #------------------------------------------------------------Collect angles
        self.angleI = self.vectorAngle(self.normalFromVector(self.startEdge),
                                       self.normalFromVector(self.endEdge))

        self.angleJ = self.vectorAngle(self.normalFromVector(self.startEdge),
                                       self.normalFromVector(self.slideEdge,reverse=True))

        self.angleK = math.pi - ( self.angleI + self.angleJ )#clamp angles sum to 180 degrees...)

        self.slideEdgeNormalize = self.normalFromVector(self.slideEdge)

        if self.debugSolver is True:
            self.exposeValues()

    def solveSsaTriangle(self, currentChainSegment):
        """
        On a practical side this means we can use SSA Triangles rule:
        https://www.mathsisfun.com/algebra/trig-solving-ssa-triangles.html
        """
        self.sweepAmount = 0.0
        self.currentChainSegment = currentChainSegment

        
        #Our 3 points are aligned on 1 line dont do any further computations
        if self.angleJ == math.pi :
            self.sliceAngle = 0.0
            self.wedgeAngle = 0.0
            self.targetPoint = self.pointI + self.normalFromVector(self.endEdge) * currentChainSegment

            return self.targetPoint

        # This IK solver will evaluate inside this TriangleFrame a smaller triangleWedge

        # We try to find a targetPoint which lies on the TriangleFrame.slideEdge
        # and which defines as well an internal sweepEdge with lenght equal to currentChainSegment
        segmentProjection = (self.startEdge_length * math.sin(self.angleJ))
        segmentRatio = segmentProjection / currentChainSegment 

        self.sliceAngle = math.asin(segmentRatio)
        self.wedgeAngle = math.pi - (self.sliceAngle + self.angleJ)

        edgeRatio = (math.sin(self.wedgeAngle) * currentChainSegment)
        self.sweepAmount = edgeRatio / math.sin(self.angleJ)

        self.targetPoint = self.pointJ + self.slideEdgeNormalize * self.sweepAmount

        return self.targetPoint

    def exposeValues(self):
        print '{ ----- Triangle DATA ----- }'
        print '\tangleI {0}'.format(math.degrees(self.angleI))
        print '\tangleJ {0}'.format(math.degrees(self.angleJ))
        print '\tangleK {0}'.format(math.degrees(self.angleK))
        print '\t------------------------------------------------------------' 
        print '\tslideEdge {0}'.format(self.slideEdge)
        print '\t------------------------------------------------------------' 
        print '\tstartEdge_length {0}'.format(self.startEdge_length)
        print '\tendEdge_length {0}'.format(self.endEdge_length)
        print '\tslideEdge_length {0}'.format(self.slideEdge_length)
        print '\t------------------------------------------------------------' 
        print '\tstartEdge {0}'.format(self.startEdge)
        print '\tendEdge {0}'.format(self.endEdge)
        print '\tslideEdge {0}'.format(self.slideEdge)
        print '\t------------------------------------------------------------' 
        print '\tpointI {0}'.format(self.pointI)
        print '\tpointJ {0}'.format(self.pointJ)
        print '\tpointK {0}'.format(self.pointK)

        print '\t------------------------------------------------------------' 
        print '\tcurrentChainSegment {0}'.format(self.currentChainSegment)
        print '\t------------------------------------------------------------' 
        print '\twedgeAngle {0}'.format(math.degrees(self.wedgeAngle))
        print '\tsliceAngle {0}'.format(math.degrees(self.sliceAngle))

        print '\t------------------------------------------------------------' 
        print '\ttargetPoint {0}'.format(self.targetPoint)


class SweepSegment(object):
    def __init__(self):
        self.recurseIndex = 0
        self.currentBoneLength = 0
        self.sweepLength = 0
        self.targetPoint = mobu.FBVector3d()
        self.targetPointIsOnCurve = True
        self.uValue = 0.0
        self.solvedMode = 0
        self.subSegmentRatio = 0.1
        
        self.uStartClamp = 0.0
        self.uEndClamp = 0.0
        
        self.segmentStartPointSrc = mobu.FBVector3d()
        self.segmentEndPointSrc = mobu.FBVector3d()

        self.pruneId = -1.0


class ChordFrame(object):
    def __init__(self):
        self.subStep = 20
        self.knotIndex = 0
        self.knotLimit = 0
        self.subSegmentLength = 0

        self.curveRange = 1.0
        self.splineObject = None

        self.sourcePoint = mobu.FBVector3d()
        self.samplingPoint = mobu.FBVector3d()

        self.closestPoint = mobu.FBVector3d()
        self.curveLength  = 0.0

        self.segmentStart = mobu.FBVector3d()
        self.segmentEnd = mobu.FBVector3d()

        self.chordStep = 0.0 
        self.subRangeSamples = []
        self.pointList = []

    def updateSubStep(self, subStep):
        self.subStep = subStep
        self.chordStep = 0.1 / ((self.subStep -1)*0.01)

        self.subRangeSamples = []
        self.pointList = []

        for k in range(subStep):
            self.subRangeSamples.append(0.0)
            self.pointList.append(mobu.FBVector3d())

    def computeUvalue(self):
        segmentStart = self.splineObject.Segment_GlobalPathEvaluate(self.knotIndex*0.1)
        self.segmentStart[0] = segmentStart[0]
        self.segmentStart[1] = segmentStart[1]
        self.segmentStart[2] = segmentStart[2]
        
        segmentEnd = self.splineObject.Segment_GlobalPathEvaluate((self.knotIndex+1)*0.1)
        self.segmentEnd[0] = segmentEnd[0]
        self.segmentEnd[1] = segmentEnd[1]
        self.segmentEnd[2] = segmentEnd[2]

        self.subSegmentLength = (self.segmentEnd - self.segmentStart).Length()

        #Prune uvalue if we are snapped at a UValue
        #Check Start Point
        pruneStartLength = (self.samplingPoint - self.segmentStart).Length()
        if pruneStartLength < 0.000000000001:
            uValueToSample = self.knotIndex * 0.1
            return uValueToSample

        #Check End Point
        pruneEndtLength = (self.samplingPoint - self.segmentEnd).Length()
        if pruneEndtLength < 0.000000000001:
            uValueToSample = ((self.knotIndex+1)*0.1)
            return uValueToSample

        return -1.0

    def subdivideSegment(self):
        uValueToSample = self.knotIndex * 0.1
        
        for k in range(self.subStep):
            self.subRangeSamples[k] = (k*self.chordStep*0.01) + uValueToSample
            sample = self.splineObject.Segment_GlobalPathEvaluate(self.subRangeSamples[k])
            tempPoint = mobu.FBVector3d()
            tempPoint[0] = sample[0]
            tempPoint[1] = sample[1]
            tempPoint[2] = sample[2]
            
            self.pointList[k] = tempPoint

    def exposeValues(self,
                     includeUvalues=False,
                     includeSegmentPoints=False):
        print '{ ----- ChordFrame DATA ----- }'
        print '\tsourcePoint {0}'.format(self.sourcePoint)
        print '\tsamplingPoint {0}'.format(self.samplingPoint)
        print '\t------------------------------------------------------------' 
        print '\tsknotIndex {0}'.format(self.knotIndex)
        print '\tcurveRange {0}'.format(self.curveRange)
        print '\tknotLimit {0}'.format(self.knotLimit)
        print '\t------------------------------------------------------------' 
        print '\tsegmentStart {0}'.format(self.segmentStart)
        print '\tsegmentEnd {0}'.format(self.segmentEnd)
        print '\tsubSegmentLengtht {0}'.format(self.subSegmentLength)
        print '\t------------------------------------------------------------' 
        print '\tchordStep {0}'.format(self.chordStep)
        print '\tchordStep {0}'.format(self.subStep)
        if includeUvalues is True:
            print '\t------------------------------------------------------------' 
            print '\tU VALUES' 
            for valueIndex,value in enumerate(self.subRangeSamples):
                print '\t\t uvalue {0}: {1}'.format(valueIndex, value)
        if includeSegmentPoints is True:
            print '\t------------------------------------------------------------' 
            print '\tU SEGMENT POINTS' 
            for value in self.pointList:
                print '\t\t point: {0}'.format(value)


class SplineSegment(object):
    def __init__(self):
        self.boneLengths = []
        self.knotPoints = []
        self.chordFrame = ChordFrame()
        self.triangleData = TriangleFrame()
        self.sweepSegment = SweepSegment()
        self.chainSegmentStartPoint = mobu.FBVector3d()
        self.startKnotIndex = 0
        self.boneIndex = 0
        self.knotSegmentsCount = 0
        self.sampleSubSegment = False
        self.debugSolver = False
        self.subSegmentRatio = 0.1
        self.uParameters = []

    def bundleData(self,
                   boneLengths,
                   knotPoints,
                   sampleSubSegment,
                   chainStart,
                   splineObject,
                   subStep):
        self.boneLengths = boneLengths
        self.knotPoints = knotPoints
        
        self.sweepSegment.targetPointIsOnCurve = True
        self.chainSegmentStartPoint[0] = chainStart[0]
        self.chainSegmentStartPoint[1] = chainStart[1]
        self.chainSegmentStartPoint[2] = chainStart[2]

        self.knotSegmentsCount = len(knotPoints)
        self.sampleSubSegment = sampleSubSegment

        self.startKnotIndex = 0

        self.sweepSegment.targetPoint[0] = self.chainSegmentStartPoint[0] 
        self.sweepSegment.targetPoint[1] = self.chainSegmentStartPoint[1] 
        self.sweepSegment.targetPoint[2] = self.chainSegmentStartPoint[2] 

        #--------------------------------------ChordFrame setup
        self.chordFrame.splineObject = splineObject

        if sampleSubSegment is True:
            self.chordFrame.knotLimit = self.knotSegmentsCount-1
            self.chordFrame.curveRange = splineObject.PathKeyGetCount()-1

            self.chordFrame.updateSubStep(subStep)


class Chain(object):
    def __init__(self):
        self.debugSolver = False
        self.debugSubSegment = False
        self.debugLineValues = False
        self.drawSubSegment = False
        
        #Critical parameter set to FALSE in order to avoid infinite recursion
        self.disableSampleSubSegment = False  
        self.emptyParameters = SplineSegment()
        self.badAnglesegment = math.radians(178.0)

    def sampleCurveKnots(self, splineObject):
        curveMaxRange = splineObject.PathKeyGetCount()-1
        subSegmentSamples = 10

        chordRatio = 10.0 / (subSegmentSamples *1.0)
        knotPoints = []
        rollCount = curveMaxRange*subSegmentSamples + 1

        for k in range(rollCount):
            knotPoints.append(0)

        for k in range(curveMaxRange):
            knotIndex = (subSegmentSamples*k)  
            for j in range(subSegmentSamples):
                uValueToSample = (knotIndex+j)*chordRatio*0.1 ;
                knotPoints[knotIndex+j] = splineObject.Segment_GlobalPathEvaluate( uValueToSample )

        knotPoints[len(knotPoints)-1] = splineObject.Segment_GlobalPathEvaluate( curveMaxRange ) ;

        return knotPoints

    def extractChainData(self,
                         splineIkPathName,
                         inknotPoints=None,
                         inSegmentData=None,
                         inSplinePath=None):
        if inSegmentData is None:
            splineIKList = mobu.FBComponentList()

            mobu.FBFindObjectsByName('{0}*'.format(splineIkPathName),
                                     splineIKList,
                                     False,
                                     False)

            splineIK = splineIKList[0] 
            
            mGroupRoll = 3 #From c++ node
            rollCount = splineIK.ReferenceGetCount(mGroupRoll)
            
            splinePath = splineIK.ReferenceGet(0,0)

            jointDrivenCount = int(splineIK.PropertyList.Find('drivenCount').Data)

            boneLengths = [0]
            shiftIndex = jointDrivenCount-1 

            for k in range(shiftIndex):
                segment = splineIK.PropertyList.Find('chainLength{0}'.format((k+1))).Data
                boneLengths.append(segment)
        else:
            jointDrivenCount = len(inSegmentData['chainSegments'])
            splinePath = inSplinePath
            boneLengths = [0]
            boneLengths.extend(inSegmentData['chainSegments'])
            rollCount = 2
            splineIK = None

        if rollCount < 1 and inknotPoints is None:
            splineObject = splineIK.ReferenceGet(0, 0)
            inknotPoints = self.sampleCurveKnots(splineObject)
    
        knotPoints = []
        if inknotPoints !=None:
            for k in range(len(inknotPoints)):
                tempPoint = mobu.FBVector3d()
                tempPoint[0] = inknotPoints[k][0]
                tempPoint[1] = inknotPoints[k][1]
                tempPoint[2] = inknotPoints[k][2]

                knotPoints.append(tempPoint)

        splineKnots = []
        if inknotPoints is None:
            splineKnots = []

            for knotIndex in range(rollCount):
                splineKnots.append(splineIK.ReferenceGet(mGroupRoll,knotIndex))

   
            for idx, knot in enumerate(splineKnots):
                knotPoint = mobu.FBVector3d()
                knot.GetVector(knotPoint)
                knotPoints.append(knotPoint)

        return {'knotPoints':knotPoints,
                'boneLengths':boneLengths,
                'splineIK':splineIK,
                'jointDrivenCount':jointDrivenCount,
                'splinePath':splinePath,
                'splineKnots':splineKnots} 

    def computeChainStart(self,
                          splineObject,
                          chainOffset):
        
        chainStart = splineObject.Total_GlobalPathEvaluate(chainOffset)
        uValuesConversion = splineObject.ConvertTotalPercentToSegmentPercent(chainOffset)

        knotSegmentIndex = int(uValuesConversion * 10.0)

        return {'chainStart':chainStart,
                'knotSegmentIndex':knotSegmentIndex}

    def extractKnotSegmentEnd(self,
                       samplingIndex, 
                       clampIndex):
        aimIndex = samplingIndex +1;

        if aimIndex > clampIndex:
            return clampIndex 

        return aimIndex

    def computeChainPoints(self,
                           splineObject,
                           boneLengths,
                           drivenCount,
                           knotPoints,
                           sampleSubSegment=False,
                           useClosestChainOnSPline=False,
                           subStep=12,
                           chainOffset=0.0):
        #-----------------------------------------------------------------------
        knotSegmentsCount = len(knotPoints)#knotSegments
        chainPoints = []  
        uParameters = []

        #In the future the chain will be able to slide on ikSpline
        startData = self.computeChainStart(splineObject,chainOffset)

        #-----------------------------------------------------------------------
        sweepParameters = SplineSegment()
        #-----------------------------------------------------------------------
        sweepParameters.bundleData(boneLengths,
                                   knotPoints,
                                   sampleSubSegment,
                                   startData['chainStart'],
                                   splineObject,
                                   subStep)

        sweepParameters.startKnotIndex = startData['knotSegmentIndex']
        sweepParameters.sweepSegment.recurseIndex  = startData['knotSegmentIndex']

        sweepParameters.debugSolver = self.debugSolver

        #-----------------------------------------------------------------------
        subSegmentParameters = SplineSegment()
        subSegmentParameters.debugSolver = self.debugSubSegment

        #-----------------------------------------------------------------------

        for boneIndex in range(drivenCount):
            sweepParameters.chainSegmentStartPoint[0] = sweepParameters.sweepSegment.targetPoint[0]
            sweepParameters.chainSegmentStartPoint[1] = sweepParameters.sweepSegment.targetPoint[1]
            sweepParameters.chainSegmentStartPoint[2] = sweepParameters.sweepSegment.targetPoint[2]
            
            sweepParameters.boneIndex = boneIndex+0

            targetPoint = self.solvePoint(sweepParameters,
                                          subSegmentParameters,
                                          drivenCount-1)

            sweepParameters.startKnotIndex = sweepParameters.sweepSegment.recurseIndex + 0

            #Write final point Result
            chainPoints.append(sweepParameters.sweepSegment.targetPoint)
            uParameters.append(sweepParameters.sweepSegment.uValue)    

        return [chainPoints,uParameters]

    def solvePoint(self,
                   sweepParameters,
                   subSegmentParameters,
                   drivenCount,
                   exposeSubSegment=False):

        if exposeSubSegment is True:
            if self.debugSolver or self.debugSubSegment:
                print '################# Processing subPoint {0} / {1}'.format(sweepParameters.boneIndex, drivenCount)
        else:
            if self.debugSolver or self.debugSubSegment:
                print '########################################################### Processing bone{0} / {1}'.format(sweepParameters.boneIndex, drivenCount)

        #1. the chain is longer on the spline path 
        #and our previous point was clamped at the end of it
        #skip evaluation and return endCurvePoint

        if sweepParameters.sweepSegment.targetPointIsOnCurve is False:
            if self.debugSolver:
                print '@@--------------- POINT CLAMP:'
                print '\t\tsweepParameters.startKnotIndex {0} '.format(sweepParameters.sweepSegment.recurseIndex)
                print '\t\tuValue {0}'.format(sweepParameters.sweepSegment.uValue)
            
            sweepParameters.sweepSegment.solvedMode = 2

            sweepParameters.sweepSegment.segmentStartPointSrc = mobu.FBVector3d(sweepParameters.knotPoints[sweepParameters.sweepSegment.recurseIndex])
            sweepParameters.sweepSegment.segmentEndPointSrc = mobu.FBVector3d(sweepParameters.knotPoints[sweepParameters.sweepSegment.recurseIndex])
            
            return mobu.FBVector3d(sweepParameters.knotPoints[len(sweepParameters.knotPoints)-1])

        #-------------------------------------------------------------------------------------BREAK 1
        #2.the current chain segment is Null --> most of the time for the first chain point
        if sweepParameters.boneLengths[sweepParameters.boneIndex] == 0.0:
            sweepParameters.sweepSegment.solvedMode = 3
            
            sweepParameters.sweepSegment.segmentStartPointSrc = mobu.FBVector3d(sweepParameters.sweepSegment.targetPoint)
            sweepParameters.sweepSegment.segmentEndPointSrc = mobu.FBVector3d(sweepParameters.sweepSegment.targetPoint)

            if self.debugSolver:
                print '@@--------------- ChainLenght is NULL:'
                print '\tchainSegmentStartPoint {0}'.format(sweepParameters.chainSegmentStartPoint)
            
            return mobu.FBVector3d(sweepParameters.sweepSegment.targetPoint)

        #-------------------------------------------------------------------------------------BREAK 2
        #3.Supported chain range
        sweepParameters.sweepSegment.currentBoneLength = sweepParameters.boneLengths[sweepParameters.boneIndex] 
        segmentEndIndex = self.extractKnotSegmentEnd(sweepParameters.startKnotIndex, sweepParameters.knotSegmentsCount-1)

        self.findPointOnCurve(sweepParameters.sweepSegment,
                              sweepParameters.chainSegmentStartPoint,
                              segmentEndIndex,
                              sweepParameters.knotPoints,
                              sweepParameters.triangleData,
                              sweepParameters.debugSolver)

        if sweepParameters.sampleSubSegment is False:
            sweepParameters.sweepSegment.segmentStartPointSrc = mobu.FBVector3d(sweepParameters.knotPoints[len(sweepParameters.knotPoints)-1])
            sweepParameters.sweepSegment.segmentEndPointSrc = mobu.FBVector3d(sweepParameters.knotPoints[len(sweepParameters.knotPoints)-1])
            
            return mobu.FBVector3d(sweepParameters.sweepSegment.targetPoint)

        #-------------------------------------------------------------------------------------SUCCESSFULL EXIT 1
        if sweepParameters.sampleSubSegment is True:
            if sweepParameters.sweepSegment.solvedMode  == 2:
                return mobu.FBVector3d(sweepParameters.sweepSegment.targetPoint)
            
            #Find closest point on curve + uParameter
            sweepParameters.chordFrame.sourcePoint = mobu.FBVector3d(sweepParameters.chainSegmentStartPoint)
            sweepParameters.chordFrame.samplingPoint = mobu.FBVector3d(sweepParameters.sweepSegment.targetPoint)
            sweepParameters.chordFrame.knotIndex = sweepParameters.sweepSegment.recurseIndex + 0

            #Iterate on SubSegment
            sweepParameters.chordFrame.subdivideSegment()

            if self.debugSubSegment is True:
                print '\n------------------------------------------------------------------------------------'
                sweepParameters.chordFrame.exposeValues()

            subStartKnotIndex = 0
            subBoneIndex = 0
            subSegmentStartPoint = mobu.FBVector3d(sweepParameters.chordFrame.sourcePoint)
            subKnotSegmentsCount = len(sweepParameters.chordFrame.pointList)
            subBoneLengths = [sweepParameters.sweepSegment.currentBoneLength+0.0]
            firstPassUvalue = sweepParameters.sweepSegment.uValue + 0.0

            subSegmentParameters.subSegmentRatio = sweepParameters.chordFrame.chordStep*0.1
            subSegmentParameters.sweepSegment.pruneId = -1.0
            
            subSegmentParameters.bundleData(subBoneLengths,
                                            sweepParameters.chordFrame.pointList,
                                            self.disableSampleSubSegment, #No recursion
                                            subSegmentStartPoint, 
                                            None,#No curve
                                            0)

            if self.debugSubSegment is True:
                print '\n{ ----- Recursion DATA ----- }'
            
            subPointCount = len(sweepParameters.chordFrame.pointList)-1
            targetPoint = self.solvePoint(subSegmentParameters,
                                          self.emptyParameters,
                                          subPointCount,
                                          exposeSubSegment=True)

            sweepParameters.sweepSegment.targetPoint = mobu.FBVector3d(targetPoint)
            subRangeValue = sweepParameters.chordFrame.subRangeSamples[subSegmentParameters.sweepSegment.recurseIndex]


            uValue = self.computeUparameter(sweepParameters.chordFrame.pointList[subSegmentParameters.sweepSegment.recurseIndex],
                                            sweepParameters.chordFrame.pointList[subSegmentParameters.sweepSegment.recurseIndex+1],
                                            targetPoint,
                                            subSegmentParameters.sweepSegment.recurseIndex,
                                            subgmentValue=subRangeValue,
                                            subSegmentRatio=sweepParameters.chordFrame.chordStep*0.01)

            sweepParameters.sweepSegment.uValue = uValue
            
            sweepParameters.sweepSegment.pruneId = uValue+0.0

            sweepParameters.sweepSegment.uStartClamp = sweepParameters.chordFrame.subRangeSamples[subSegmentParameters.sweepSegment.recurseIndex] 
            sweepParameters.sweepSegment.uEndClamp =  sweepParameters.chordFrame.subRangeSamples[subSegmentParameters.sweepSegment.recurseIndex+1]
            
            sweepParameters.sweepSegment.segmentStartPointSrc = sweepParameters.chordFrame.pointList[subSegmentParameters.sweepSegment.recurseIndex]
            sweepParameters.sweepSegment.segmentEndPointSrc = sweepParameters.chordFrame.pointList[subSegmentParameters.sweepSegment.recurseIndex+1]

            if self.debugSubSegment is True:
                print '\tsubKnot in segment Mode {0}'.format(subRangeValue)
                print '\tfirstPassUvalue         {0}'.format(firstPassUvalue)
                print '\tuValue in segment Mode  {0}'.format(sweepParameters.sweepSegment.uValue)
                print '\t------------------------------------------------------------' 
                print '\tuStartClamp in segment Mode  {0}'.format(sweepParameters.sweepSegment.uStartClamp)
                print '\tuEndClamp in segment Mode  {0}'.format(sweepParameters.sweepSegment.uEndClamp)

            if self.drawSubSegment is True:
                self.previewShape(sweepParameters.chordFrame.pointList)

            #-------------------------------------------------------------------------------------SUCCESSFULL EXIT 2
            return mobu.FBVector3d(targetPoint)

    #--------------------------------chainInteration
    def findPointOnCurve(self,
                         sweepSegment,
                         chainSegmentStartPoint,
                         startKnotIndex,
                         knotPoints,
                         triangleData,
                         debugSolver):

        sweepLength = 0.0

        targetPoint = mobu.FBVector3d()
        sweepVector = mobu.FBVector3d()
        sweepVector = knotPoints[startKnotIndex] - chainSegmentStartPoint
        segmentCheck = sweepVector.Length()
        currentChainSegment = sweepSegment.currentBoneLength*1.0

        #2. Spline walker: each time a chain is found we update the starting index
        knotPointCount = len(knotPoints)
        outIndex = startKnotIndex+0

        for knotIndex in range(startKnotIndex,knotPointCount):
            outIndex = knotIndex+0
            sweepVector = knotPoints[knotIndex] - chainSegmentStartPoint
            sweepVectorLength = sweepVector.Length()

            if sweepVectorLength > currentChainSegment:
                triangleData.setpoints(chainSegmentStartPoint,
                                       knotPoints[knotIndex-1],
                                       knotPoints[knotIndex])
                triangleData.build()

                if triangleData.angleI > self.badAnglesegment :
                    continue

                targetPoint = triangleData.solveSsaTriangle(currentChainSegment)

                if debugSolver:
                    triangleData.exposeValues()

                previousUvalue = sweepSegment.uValue+0.0

                sweepSegment.uValue = self.computeUparameter(knotPoints[knotIndex-1],
                                                             knotPoints[knotIndex],
                                                             targetPoint,
                                                             sweepSegment.recurseIndex+0,
                                                             subSegmentRatio=sweepSegment.subSegmentRatio)

                sweepSegment.recurseIndex = knotIndex-1
                sweepSegment.sweepLength = sweepVectorLength
                sweepSegment.targetPoint = mobu.FBVector3d(targetPoint)
                sweepSegment.targetPointIsOnCurve = True
                sweepSegment.solvedMode = 1

                sweepSegment.segmentStartPointSrc  = knotPoints[knotIndex-1]
                sweepSegment.segmentEndPointSrc    = knotPoints[knotIndex]

                previousIndex = knotIndex-1

                sweepSegment.uStartClamp = (previousIndex)*0.1
                sweepSegment.uEndClamp =  (knotIndex)*0.1

                if debugSolver:
                    triangleData.exposeValues()

                if debugSolver:
                    chainLenghtCheck = (targetPoint-chainSegmentStartPoint).Length()
                    print '\n@@--------------- Spline walker:'
                    print '\t\tcurrentChainSegment {0} \n\t\tsweepVectorLength: {1}'.format(currentChainSegment, 
                                                                                            sweepVectorLength)
                    print '\t\tstartKnotIndex {0} '.format(knotIndex)
                    print '\t\tsweepAmount {0} '.format(triangleData.sweepAmount)
                    print '\t\tchainLenghtCheck {0} '.format(chainLenghtCheck)
                    print '\t\tEpsilonError {0} '.format(currentChainSegment-chainLenghtCheck)
                    print '\t------------------------------------------------------------' 
                    print '\ttargetPoint {0}'.format(targetPoint)
                    print '\t------------------------------------------------------------' 
                    print '\tuValue {0}'.format(sweepSegment.uValue)

                    print '\tuStartClamp  {0}'.format(sweepSegment.uStartClamp)
                    print '\tuEndClamp  {0}'.format(sweepSegment.uEndClamp)
                    print '\tknotCount {0}'.format(knotPointCount)
                    
                    print '\t------------------------------------------------------------' 

                    print '\t------------------------------------------------------------' 
                return

        #3.Clamped Point
        sweepSegment.recurseIndex = outIndex #knotIndex--------------------------------------------------TO TEST
        sweepSegment.sweepLength = 0.0
        sweepSegment.targetPoint = mobu.FBVector3d(knotPoints[len(knotPoints)-1])
        sweepSegment.targetPointIsOnCurve = False
        sweepSegment.solvedMode = 2
        sweepSegment.uValue = outIndex*0.1
        
        
        sweepSegment.segmentStartPointSrc  = mobu.FBVector3d(knotPoints[len(knotPoints)-1])
        sweepSegment.segmentEndPointSrc    = mobu.FBVector3d(knotPoints[len(knotPoints)-1])

        mobu.FBVector3d(sweepSegment.targetPoint)

        if debugSolver:
            print '\n@@--------------- END of line:'
            print '\t\tstartKnotIndex {0} '.format(knotIndex)
            print '\t\tcurrentChainSegment {0} \n\t\tsweepVectorLength: {1}'.format(currentChainSegment, 
                                                                                    sweepVectorLength)
            print '\t\tknotPointsCount limit {0} '.format(len(knotPoints)-1)
            print '\t\tClamp Target point at the end of spline'
            print '\t------------------------------------------------------------' 
            print '\tuValue {0}'.format(sweepSegment.uValue)
            print '\t------------------------------------------------------------' 
            print '\tchainSegmentStartPoint {0}'.format(chainSegmentStartPoint)
            print '\tspineEndPoint {0}'.format(knotPoints[len(knotPoints)-1])

        return

    def computeUparameter(self,
                          segmentStartPoint,
                          segmentEndPoint,
                          targetPoint,
                          segmentStartIndex,
                          subSegmentRatio=0.1,
                          subgmentValue=None):
        if subgmentValue is None:
            segmentVector = segmentEndPoint - segmentStartPoint
            segmentVectorLength = segmentVector.Length()

            if segmentVectorLength == 0.0:
                return segmentStartIndex*0.1

            sweepVector = targetPoint - segmentStartPoint
            sweepVectorLength = sweepVector.Length()
            
            sweepAmount = sweepVectorLength / segmentVectorLength

            uValue = (segmentStartIndex*subSegmentRatio) + (sweepAmount*subSegmentRatio)

            return uValue
        else:
            segmentVector = segmentEndPoint - segmentStartPoint
            segmentVectorLength = segmentVector.Length()

            if segmentVectorLength == 0.0:
                return segmentStartIndex*0.1

            sweepVector = targetPoint - segmentStartPoint
            sweepVectorLength = sweepVector.Length()

            sweepAmount = sweepVectorLength / segmentVectorLength

            uValue = subgmentValue + sweepAmount*subSegmentRatio
            return uValue

    def build(self,
              splineIkPathName='splineIkPath1_constraint', 
              knotPoints=None,
              segmentData=None,
              splinePath=None,
              sampleSubSegment=False,
              useClosestChainOnSPline=False,
              drawShape=False,
              chainOffset=0.0):

        curveData = self.extractChainData(splineIkPathName,
                                          inknotPoints=knotPoints,
                                          inSegmentData=segmentData,
                                          inSplinePath=splinePath)

        curveAttributes = self.computeChainPoints(curveData['splinePath'],
                                                  curveData['boneLengths'],
                                                  curveData['jointDrivenCount'],
                                                  curveData['knotPoints'],
                                                  sampleSubSegment=sampleSubSegment,
                                                  useClosestChainOnSPline=useClosestChainOnSPline,
                                                  chainOffset=chainOffset)

        if drawShape is True:
            curvePoints = curveAttributes[0]

            previewShape = self.previewShape(curvePoints)

        return curveAttributes

    def previewShape(self,curvePoints):
        curveObj = mobu.FBModelPath3D('ChainPreview1')
        curveObj.Show = True
        curveObj.Visible = True

        for pKeyIndex in range(len(curvePoints)):
            position = mobu.FBVector4d(curvePoints[pKeyIndex][0],
                                       curvePoints[pKeyIndex][1],
                                       curvePoints[pKeyIndex][2],
                                       1.0)
            try:
                # We call PathKeyGet to check if a key already exists
                # it will error out if it doesn't
                if curveObj.PathKeyGet(pKeyIndex):
                    curveObj.PathKeySet(pKeyIndex, position)

            except IndexError:
                curveObj.Segment_PathKeyAdd(pKeyIndex, position)
                curveObj.PathKeySet(pKeyIndex,position)

            curveObj.PathKeySetLeftRightTangent(pKeyIndex, 
                                                   position, 
                                                   position, 
                                                   position, 
                                                   True) 
        return curveObj

    def buildShapeFromUvalues(self,
                              splineObject,
                              uValues):
        curveObj = mobu.FBModelPath3D('U_ChainPreview1')
        curveObj.Show = True
        curveObj.Visible = True

        for pKeyIndex in range(len(uValues)):
            knotPoint = splineObject.Segment_GlobalPathEvaluate(uValues[pKeyIndex])
            position = mobu.FBVector4d(knotPoint[0],
                                       knotPoint[1],
                                       knotPoint[2],
                                       1.0)
            try:
                # We call PathKeyGet to check if a key already exists
                # it will error out if it doesn't
                if curveObj.PathKeyGet(pKeyIndex):
                    curveObj.PathKeySet(pKeyIndex, position)

            except IndexError:
                curveObj.Segment_PathKeyAdd(pKeyIndex, position)
                curveObj.PathKeySet(pKeyIndex,position)

            curveObj.PathKeySetLeftRightTangent(pKeyIndex, 
                                                   position, 
                                                   position, 
                                                   position, 
                                                   True) 
        return curveObj

    def previewChainConstraint(self,
                               splineIkPathName='splineIkPath1_constraint'):
        chainData = self.extractChainData(splineIkPathName)

        mGroupConstrain = 1
        jointDrivenCount = int(chainData['jointDrivenCount'])

        pointList = []
        for k in range(jointDrivenCount):
            loc = chainData['splineIK'].ReferenceGet( mGroupConstrain, k ) 
            tempPos = mobu.FBVector3d()
            loc.GetVector(tempPos) 

            pointList.append(tempPos)
            
        curveObj = mobu.FBModelPath3D('ChainPreview1')
        curveObj.Show = True
        curveObj.Visible = True

        for pKeyIndex in range(len(pointList)):
            position = mobu.FBVector4d(pointList[pKeyIndex][0],
                                       pointList[pKeyIndex][1],
                                       pointList[pKeyIndex][2],
                                       1.0)
            try:
                # We call PathKeyGet to check if a key already exists
                # it will error out if it doesn't
                if curveObj.PathKeyGet(pKeyIndex):
                    curveObj.PathKeySet(pKeyIndex, position)

            except IndexError:
                curveObj.Segment_PathKeyAdd(pKeyIndex, position)
                curveObj.PathKeySet(pKeyIndex,position)

            curveObj.PathKeySetLeftRightTangent(pKeyIndex, 
                                                   position, 
                                                   position, 
                                                   position, 
                                                   True) 
        mGroupChain = 6
        previousChainCurve = chainData['splineIK'].ReferenceGet( mGroupChain, 0 ) 
        if previousChainCurve != None:
            chainData['splineIK'].ReferenceRemove(mGroupChain,previousChainCurve)

        chainData['splineIK'].ReferenceAdd(mGroupChain,curveObj)
