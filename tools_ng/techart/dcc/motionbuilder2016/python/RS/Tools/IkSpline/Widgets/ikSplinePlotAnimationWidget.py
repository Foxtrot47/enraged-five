import os

from functools import partial

from PySide import QtGui, QtCore

import pyfbsdk as mobu
from RS import Globals
from RS.Utils import ContextManagers
from RS import Config

from RS.Tools.IkSpline import Utils as ikUtils


class TabHolder(object):
    def __init__(self):
        self.layout = QtGui.QVBoxLayout()
        self.widget = QtGui.QWidget()


class PlotAnimationWidget(QtGui.QWidget):
    """
        main Tool UI
    """
    def __init__(self,
                 toolBox):
        """
            Constructor
        """
        self.toolBox = toolBox

        self.plotModeModel = QtGui.QStringListModel()

        self.sourceContactPointField = QtGui.QLineEdit()

        self.targetContactPointField = QtGui.QLineEdit()

        # Main window settings
        super(PlotAnimationWidget, self).__init__()
        self.setupUi()

    def createPlotOptions(self):
        self.plotTranslationModeWidget = QtGui.QComboBox()
        self.plotTranslationModeModel = QtGui.QStringListModel()
        self.plotTranslationModeWidget.setModel(self.plotTranslationModeModel)
        self.plotTranslationModeModel.setStringList(['Plot rotation and translation', 'Plot rotation only on chain links'])
        self.plotTranslationModeWidget.setCurrentIndex(0)

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        self.toggleControllerAxisWidget = QtGui.QCheckBox("Toggle Controller Axis") 

        self.plotSkelRootIndermediateRange = QtGui.QCheckBox("Plot SkelRoot indermediate Range") 
        self.plotSkelRootIndermediateRange.setChecked(True)

        self.setStaticSkelRootOrientation = QtGui.QCheckBox("Set static SkelRoot Orientation") 
        self.setStaticSkelRootOrientation.setChecked(False)

        self.batchPlotAllTakes = QtGui.QCheckBox("Process All Takes") 
        self.batchPlotAllTakes.setChecked(False)

        self.plotButtonWidget = QtGui.QPushButton("Plot Animation")

        self.plotModeComboBox = QtGui.QComboBox(self)

        self.plotModeModel.setStringList(['Bake animation to skeleton',
                                          'Match Mover to Contact point\'s Translation',
                                          'ParentConstraint Mover to Contact Point',
                                          'Blend Mover betweens Contact Points'])

        self.plotModeComboBox.setModel(self.plotModeModel)

        mainLayout.setContentsMargins(0, 0, 0, 0)
        self.setMinimumWidth(310)
        self.plotButtonWidget.setMinimumHeight(42)

        mainLayout.addWidget(self.toggleControllerAxisWidget)

        mainLayout.addWidget(self._createSeparator())

        mainLayout.addWidget(self.plotSkelRootIndermediateRange)

        mainLayout.addWidget(self.setStaticSkelRootOrientation)

        mainLayout.addWidget(self._createSeparator())

        mainLayout.addWidget(self.plotModeComboBox)

        mainLayout.addWidget(self._createSeparator())

        mainLayout.addWidget(self._buildContactPointWidgets())

        mainLayout.addWidget(self._createSeparator())

        mainLayout.addWidget(self.batchPlotAllTakes)

        mainLayout.addWidget(self.plotButtonWidget)

        #Populates character list
        self.setLayout(mainLayout)

        self.toggleControllerAxisWidget.stateChanged.connect(self._toggleControllerAxis)

        self.plotButtonWidget.pressed.connect(self._plotAnimation)

        self.plotModeComboBox.currentIndexChanged.connect(self._filterContactPointWidgets)

    def _filterContactPointWidgets(self):
        self._swapTabVisibility(self.plotModeComboBox.currentIndex())

        self.window().adjustSize() 
        self.window().updateGeometry()
        QtGui.QApplication.processEvents()

    def _buildContactPointWidgets(self):
        self.anchorWidget = self._createTabHolder('ContactPointAnchor')

        self.hiddenWidget = self._createTabHolder('HiddenContactPointAnchor')

        self.hiddenWidget.widget.hide()

        pickerLayout = QtGui.QVBoxLayout()

        pickerLayout.setContentsMargins(0, 0, 0, 0)

        self.contactPointData = self._buildPicker(self.sourceContactPointField,
                                                  "Contact Point:")

        self.targetPointData = self._buildPicker(self.targetContactPointField,
                                                 "Target Contact:")

        self.hiddenWidget.layout.addWidget(self.contactPointData)

        self.hiddenWidget.layout.addWidget(self.targetPointData)

        pickerLayout.addWidget(self.anchorWidget.widget)

        pickerLayout.addWidget(self.hiddenWidget.widget)

        outputTab = QtGui.QWidget()

        outputTab.setLayout(pickerLayout)

        return outputTab

    def _swapTabVisibility(self, menuIndex):
        self.hiddenWidget.layout.addWidget(self.contactPointData)

        self.hiddenWidget.layout.addWidget(self.targetPointData)

        if menuIndex ==0:
            return

        self.anchorWidget.layout.addWidget(self.contactPointData)

        if menuIndex <= 2:
            return

        self.anchorWidget.layout.addWidget(self.targetPointData)

    def _createTabHolder(self, widgetName):
        """
            Internal Method

            Placeholder widget used to defines border with provided widget list
            Args:
                widgetName (str): flag widget with the provided name.

            returns dict {QWidget,QVBoxLayout}
        """
        tabData = TabHolder()

        tabData.layout.setContentsMargins(0, 0, 0, 0) 
        tabData.layout.setSpacing(0) 

        tabData.widget.setLayout(tabData.layout)
        tabData.widget.setSizePolicy(QtGui.QSizePolicy.Minimum, 
                                     QtGui.QSizePolicy.Minimum)

        tabData.widget.setObjectName(widgetName)

        return tabData

    def _buildPicker(self,
                     inputLine,
                     inputLabel):
        snapCurveLayout = QtGui.QHBoxLayout()
        snapCurveLayout.setContentsMargins(0, 0, 0, 0)
        snapCurveLayout.setSpacing(5)


        pickButton = QtGui.QPushButton(">")

        snapCurveLayout.addWidget(QtGui.QLabel(inputLabel))
        snapCurveLayout.addWidget(inputLine)
        snapCurveLayout.addWidget(pickButton)

        pickButton.setMaximumHeight(18)
        pickButton.setMaximumWidth(18)

        outputTab = QtGui.QWidget()
        outputTab.setLayout(snapCurveLayout)

        pickButton.clicked.connect(partial(self._setFieldFromSelection,
                                           inputLine))

        return outputTab

    def _setFieldFromSelection(self, field):
        field.setText('')
        currentSelection = ikUtils.JointChain().getJointChainFromSelection()

        if len(currentSelection)<1:
            return

        field.setText(currentSelection[0].LongName)

    def _toggleControllerAxis(self):
        splineIkRigName = str(self.toolBox.rootWidget.ikRootField.text())

        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        ikUtils.AnimUtils().toggleControllerAxis(splineIkRig, 
                                                 self.toggleControllerAxisWidget.isChecked())

    def _plotAnimation(self):
        processAllTakes = self.batchPlotAllTakes.isChecked()

        splineIkRigName = str(self.toolBox.rootWidget.ikRootField.text())
        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        contactPointComponents = self._validateContactPoints()
        if contactPointComponents is None:
            return

        setDisabledValue = False

        ikUtils.AnimUtils().consolidateGlueComponents(splineIkRig)

        if processAllTakes is False:
            ikUtils.AnimUtils().toggleIkSplineRigWeights(splineIkRig, 
                                                         setDisabledValue)

            Globals.Scene.Evaluate()

            utils = ikUtils.JointCloud()

            with ContextManagers.ClearModelSelection():
                utils.bakeMhInGlobalspace(splineIkRigName,
                                          contactPointComponents,
                                          keyInclusiveRange=self.plotSkelRootIndermediateRange.isChecked(),
                                          setStaticOrientation=self.setStaticSkelRootOrientation.isChecked())

        else:
            for take in Globals.Scene.Takes:
                mobu.FBSystem().CurrentTake = take

                Globals.Scene.Evaluate()

                ikUtils.AnimUtils().toggleIkSplineRigWeights(splineIkRig, 
                                                             setDisabledValue)

                Globals.Scene.Evaluate()

                utils = ikUtils.JointCloud()

                with ContextManagers.ClearModelSelection():
                    utils.bakeMhInGlobalspace(splineIkRigName,
                                              contactPointComponents,
                                              keyInclusiveRange=self.plotSkelRootIndermediateRange.isChecked(),
                                              setStaticOrientation=self.setStaticSkelRootOrientation.isChecked())

    def _validateContactPoints(self):
        if self.plotModeComboBox.currentIndex() == 0:
            return ikUtils.MoverTransfertComponents(self.plotModeComboBox.currentIndex())

        contactPointName = str(self.sourceContactPointField.text())
        contactPoint = None

        if self.plotModeComboBox.currentIndex() == 1 or self.plotModeComboBox.currentIndex() == 2:
            if len(contactPointName)==0:
                return None

            contactPoint = mobu.FBFindModelByLabelName(contactPointName)

            if contactPoint is None:
                return None

            outputData = ikUtils.MoverTransfertComponents(self.plotModeComboBox.currentIndex(),
                                                          sourceContactPoint=contactPoint)

            return outputData

        if len(contactPointName)==0:
            return None

        contactPoint = mobu.FBFindModelByLabelName(contactPointName)

        if contactPoint is None:
            return None

        targetPointName = str(self.targetContactPointField.text())
        if len(targetPointName)==0:
            return None

        targetPoint = mobu.FBFindModelByLabelName(targetPointName)

        if targetPoint is None:
            return None

        outputData = ikUtils.MoverTransfertComponents(self.plotModeComboBox.currentIndex(),
                                                      sourceContactPoint=contactPoint,
                                                      targetContactPoint=targetPoint)

        return outputData

    def _createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine
