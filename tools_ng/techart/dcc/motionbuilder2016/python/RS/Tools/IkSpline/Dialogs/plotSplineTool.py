import json
import os

from functools import partial
import pyfbsdk as mobu

from RS import Globals
from RS.Tools import UI

from RS.Tools.IkSpline.Widgets import splinePlotAnimationWidget
reload(splinePlotAnimationWidget)


from RS.Tools.IkSpline import Core as  IkSplineCore
from RS.Tools.IkSpline import Utils as ikUtils


from PySide import QtGui, QtCore


class PlotSplineTool(UI.QtMainWindowBase):
    """
        main Tool UI
    """
    def __init__(self):
        """
            Constructor
        """
        self._toolName = "Plot Spline Tool v{0}".format('0.0.07')

        # Main window settings
        super(PlotSplineTool, self).__init__(title=self._toolName, 
                                             dockable=True,
                                             size=[290, 520])
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()

        # Create Widgets 
        self.mainWidget = QtGui.QWidget()
        banner = UI.BannerWidget(self._toolName)

        self.plotWidget = splinePlotAnimationWidget.PlotAnimationWidget(self)

        self.jointWidget = splinePlotAnimationWidget.JointListWidget(self)

        # Set properties

        # Assign Widgets to layouts 
        mainLayout.addWidget(banner)

        mainLayout.addWidget(self.jointWidget)

        mainLayout.addWidget(self.plotWidget)

        # Add widgets to Layouts
        self.mainWidget.setLayout(mainLayout)

        # Assign Layouts
        self.setCentralWidget(self.mainWidget)


def Run(show=True):
    tool = PlotSplineTool()

    if show: 
        tool.show()  
    return tool
