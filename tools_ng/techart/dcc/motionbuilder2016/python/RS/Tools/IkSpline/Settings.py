class IkSplineStructureSettings(object):
    def __init__(self):
        self.splineObject  = None
        self.curveDeformer = None
        self.ikConstraint = None
        self.constraintFolder = None
        self.anchorFolder   = None
        self.tweakerGroup = None
        self.rigRoot = None

        self.fkControls = []
        self.jointObjectArray = []
        self.tweakerOffsetList  = []
        self.bufferList = []
        self.anchorList = []
        self.splineKnots = []
        self.tweakerList = []
        self.offsetList = []
        self.gameJointConstraintArray  = []
        self.controlsToDriveByMotionPath = []
        self.skelRootModelArray = []
        self.skelRootModelAnchor = None

        self.ikControlsData  = {}
        self.fkControlsData  = FkControlSettings()
        self.splineIkRig = None
        self.ikSplineSettings = None
        self.addExtensionToCharacter = None


class IkSplineSettings(object):
    def __init__(self):
        self.STOP_AT_CHAIN_SOLVER = False
        self.lockFirstIkTranslation = False
        self.rigAnchor = None
        self.skelRootArray = []
        self.skelRootAnchor = None
        self.showSplineMatrix = False
        self.previewChainCurve = False
        self.bindMethod = 'knotTranslation'#Posible method 'knotTranslation'#/'cluster'
        self.upVectorOffset = 20.0 
        self.debugKnotLength = 20.0 
        self.maintainChainLength = True
        self.fkOffset = [180,-90,90]
        self.fkControlsSize = 550
        self.ikControlsSize = 250
        self.tweakerControlsSize = 1 
        self.ikPathName = 'splineIkPath1'
        self.numberOfCurveController = 7
        self.applyPositionOffset = True
        self.applyRotationOffset = True
        self.aimToNextJoint = True
        self.lockSelection = False
        self.hideDrivenJoints = False
        self.buildUpVector = False
        self.createTweaker = False
        self.hidePath = False
        self.renderGeometry = []
        self.hasSnakeCompensationLayer = False
        self.createFkControls = False
        self.createIntermediateGroup = True
        self.connectTwistDriver = True
        self.excludeThreshold = 0.0001
        self.isMotionPath = False
        self.rigStorage = None
        self.reverseList = False
        self.addExtensionToCharacter = None
        self.buildRigControl = False
        self.reverseChain = False
        self.isSimplifiedRopeRig = False


class IkControlSettings(object):
    def __init__(self):
        self.ikControls = None
        self.UpVectorControls = None
        self.UpVectorLines = None
        self.ikGroup = None
        self.UpVectorGroup = None


class FkControlSettings(object):
    def __init__(self):
        self.fk_controlList = None
        self.fkZeroGroups = None
        self.fkGroup = None


class GameBonesSettings(object):
    def __init__(self):
        self.skelRootArray = None
        self.skelRootAnchor = None
        self.constraintArray = None
