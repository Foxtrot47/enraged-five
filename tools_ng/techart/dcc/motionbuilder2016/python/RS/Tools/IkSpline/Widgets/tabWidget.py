from PySide import QtGui, QtCore

class ToolData(object):
    def __init__(self, toolWidget, toolTabName):
        self.toolWidget = toolWidget
        self.toolTabName = toolTabName


class TabHolder(object):
    def __init__(self):
        self.layout = QtGui.QVBoxLayout()
        self.widget = QtGui.QWidget()


class ShelvesTabWidget(QtGui.QWidget):
    """
        main Tool UI
    """
    def __init__(self):
        """
            Constructor
        """
        # Main window settings
        super(ShelvesTabWidget, self).__init__()
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        self.mainLayout = QtGui.QVBoxLayout()

        # Create Widgets 
        self.toolTabArray = []

        self._tabWidget = self._buildToolTabWidget()
        # Assign Layouts to WidgetStack
        self.mainLayout.addWidget(self._tabWidget)

        self.setLayout(self.mainLayout)

        self._swapTabVisibility(0)
        self._tabWidget.currentChanged.connect(self._swapTabVisibility)

        grayColor = 83
        widgetPalette = self._tabWidget.palette()
        widgetPalette.setColor(self._tabWidget.backgroundRole(), 
                               QtGui.QColor(grayColor, grayColor, grayColor))

        self._tabWidget.setPalette(widgetPalette)
        self._tabWidget.setAutoFillBackground(True)


        grayColor = 83
        widgetPalette = self.palette()
        widgetPalette.setColor(self.backgroundRole(), 
                               QtGui.QColor(grayColor, grayColor, grayColor))

        self.setPalette(widgetPalette)
        self.setAutoFillBackground(True)

    def _buildToolTabWidget(self):
        """
            Internal Method

            Prepare content for the toolbox tabControl widget.
            Args:
                list of tupples (QWidget, tabName)
            
            returns QTabWidget.
        """
        tabWidget = QtGui.QTabWidget()
        tabWidget.setSizePolicy(QtGui.QSizePolicy.Minimum,
                                QtGui.QSizePolicy.Minimum)
        self.hiddenTabArray = []
        self.tabWidgetArray = []

        return tabWidget

    def _appendToTabWidget(self, inputToolData):
        self.toolTabArray.append(inputToolData)
        self._tabWidget.addTab(inputToolData.toolWidget,
                               inputToolData.toolTabName)

        anchorWidget = self._createTabHolder("{0}_Anchor".format(inputToolData.toolTabName))    
        widgetHidden = self._createTabHolder("{0}_Hidden".format(inputToolData.toolTabName)) 
        widgetHidden.widget.hide()

        self.mainLayout.addWidget(widgetHidden.widget)

        grayColor = 83
        widgetPalette = anchorWidget.widget.palette()
        widgetPalette.setColor(anchorWidget.widget.backgroundRole(), 
                               QtGui.QColor(grayColor, grayColor, grayColor))

        anchorWidget.widget.setPalette(widgetPalette)
        anchorWidget.widget.setAutoFillBackground(True)


        anchorWidget.layout.addWidget(inputToolData.toolWidget)
        self._tabWidget.addTab(anchorWidget.widget, 
                               inputToolData.toolTabName)

        self.hiddenTabArray.append(widgetHidden)
        self.tabWidgetArray.append(anchorWidget)

        inputToolData.toolWidget.show()
        self._swapTabVisibility(0)

    def _swapTabVisibility(self, tabIndex):
        """
            Internal Method

            Visibility handler for a QTabWidget. 
            Force also the main UI to take into account each tab initial size
            Args:
                tabIndex (int). index to used to define the current displayer Tab
            
            returns QTabWidget.
        """
        if len(self.tabWidgetArray) == 0:
            return

        for widgetIndex in xrange(len(self.hiddenTabArray)):
            self.hiddenTabArray[widgetIndex].layout.addWidget(self.toolTabArray[widgetIndex].toolWidget)

        self.tabWidgetArray[tabIndex].layout.addWidget(self.toolTabArray[tabIndex].toolWidget)
        self._refreshWindow()

    def _refreshWindow(self):
        """
            Internal Method

            Refresh visibility and size of the tool + its components.
        """
        self._updateWindowHeight()
    
        QtGui.QApplication.processEvents()
        QtCore.QTimer.singleShot(0, self._updateWindowHeight)

    def _updateWindowHeight(self):  
        """
            Internal Method

            Refresh size of the tool components.
        """
        self._tabWidget.adjustSize() 
        self._tabWidget.updateGeometry()

        self.window().adjustSize() 
        self.window().updateGeometry()

        self.resize(self._tabWidget.sizeHint().width(),self._tabWidget.sizeHint().height())
        self.resize(self.mainLayout.sizeHint().width(),self.mainLayout.sizeHint().height())

    def _createTabHolder(self, widgetName):
        """
            Internal Method

            Placeholder widget used to defines border with provided widget list
            Args:
                widgetName (str): flag widget with the provided name.

            returns dict {QWidget,QVBoxLayout}
        """
        tabData = TabHolder()

        tabData.layout.setContentsMargins(6, 6, 6, 6) 
        tabData.layout.setSpacing(0) 

        tabData.widget.setLayout(tabData.layout)
        tabData.widget.setSizePolicy(QtGui.QSizePolicy.Minimum, 
                                     QtGui.QSizePolicy.Minimum)

        tabData.widget.setObjectName(widgetName)

        return tabData

