import os
from PySide import QtGui, QtCore
from functools import partial

import pyfbsdk as mobu

from RS import Config
from RS.Tools.IkSpline import Utils as ikUtils
from RS.Tools.IkSpline import Core as  IkSplineCore


class MotionCurveWidget(QtGui.QWidget):
    """
        main Tool UI
    """
    def __init__(self,
                 toolBox):
        """
            Constructor
        """
        self.toolBox = toolBox

        # Main window settings
        super(MotionCurveWidget, self).__init__()

        self.setupUi()

    def setupUi(self):
        self.mainLayout = QtGui.QVBoxLayout()

        picker = self._buildMotionCurvePicker()

        settings = self._buildOffsetSettingControl()

        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.setMinimumWidth(310)

        self.mainLayout.addWidget(picker)

        self.mainLayout.addWidget(self._createSeparator())

        self.mainLayout.addWidget(settings)

        self.mainLayout.addWidget(self._createSeparator())

        self._createButtonControls()

        #Populates character list
        self.setLayout(self.mainLayout)

    def _buildMotionCurvePicker(self):
        snapCurveLayout = QtGui.QHBoxLayout()
        snapCurveLayout.setContentsMargins(0, 0, 0, 0)
        snapCurveLayout.setSpacing(5)

        self.snapCurveField = QtGui.QLineEdit()
        self.snapCurveButton = QtGui.QPushButton(">>")

        snapCurveLayout.addWidget(QtGui.QLabel("Motion Curve:"))
        snapCurveLayout.addWidget(self.snapCurveField)
        snapCurveLayout.addWidget(self.snapCurveButton)

        outputTab = QtGui.QWidget()
        outputTab.setLayout(snapCurveLayout)

        self.snapCurveButton.clicked.connect(partial(self._appendSelectedCurve,
                                                     self.snapCurveField))

        return outputTab

    def _buildOffsetSettingControl(self):
        snapCurveOffsetLayout = QtGui.QHBoxLayout()
        snapCurveOffsetLayout.setContentsMargins(0, 0, 0, 0)
        snapCurveOffsetLayout.setSpacing(5)

        self.snapToCurveOffsetWidget = QtGui.QDoubleSpinBox(self)
        self.snapToCurveOffsetWidget.setRange(0.0,100.0)
        self.snapToCurveOffsetWidget.setValue(0.0)

        snapCurveOffsetLayout.addWidget(QtGui.QLabel("Offset rig along curve:"))
        snapCurveOffsetLayout.addWidget(self.snapToCurveOffsetWidget)

        snapCurveOffsetTab = QtGui.QWidget()
        snapCurveOffsetTab.setLayout(snapCurveOffsetLayout)

        return snapCurveOffsetTab

    def _createButtonControls(self):
        self.snapRigButtonWidget = QtGui.QPushButton("Snap Rig to Curve")
        self.mainLayout.addWidget(self.snapRigButtonWidget)

        self.mainLayout.addWidget(self._createSeparator())

        self.matchRigButtonWidget = QtGui.QPushButton("Match Rig to Curve")

        self.mainLayout.addWidget(self.matchRigButtonWidget)

        self.snapRigButtonWidget.clicked.connect(self._snapRigOn3dCurve)

        self.matchRigButtonWidget.clicked.connect(self._matchRigOn3dCurve)

    def _appendSelectedCurve(self, 
                             field):
        field.setText('')
        currentSelection = ikUtils.JointChain().getJointChainFromSelection()

        if len(currentSelection)<1:
            field.setText('')
            return

        if not isinstance(currentSelection[0], mobu.FBModelPath3D):
            field.setText('')
            return

        field.setText(currentSelection[0].LongName)

    def _snapRigOn3dCurve(self):
        splineIkRigName = str(self.toolBox.rootWidget.ikRootField.text())

        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        motionCurveName = str(self.snapCurveField.text())
        if motionCurveName is None:
            return
        
        if len(motionCurveName) < 0:
            return

        motionCurve = mobu.FBFindModelByLabelName(motionCurveName)
        if motionCurve is None:
            return
        ikUtils.AnimUtils().snapRigOn3dCurve(splineIkRig, motionCurve, offset=self.snapToCurveOffsetWidget.value())

    def _matchRigOn3dCurve(self):
        splineIkRigName = str(self.toolBox.rootWidget.ikRootField.text())

        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        motionCurveName = str(self.snapCurveField.text())
        if motionCurveName is None:
            return
        
        if len(motionCurveName) < 0:
            return

        motionCurve = mobu.FBFindModelByLabelName(motionCurveName)
        if motionCurve is None:
            return
        ikUtils.AnimUtils().matchRigOn3dCurve(splineIkRig, motionCurve, offset=self.snapToCurveOffsetWidget.value())

    def _createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine
