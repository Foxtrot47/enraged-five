import copy
import json
import math
import collections

import pyfbsdk as mobu

from RS.Utils import Scene
from RS.Utils.Scene import Constraint
from RS.Tools.IkSpline import Utils as ikUtils
from RS.Tools.IkSpline import Settings as ikSettings
from RS.Tools.IkSpline import Controllers
reload(Controllers)


class twistReader(object):
    def __init__(self):
        self.twistReference = None
        self.spaceObject = None
        self.pointOrient = None
        self.aimTarget = None
        self.targetConstraint = None
        self.angleConstraint = None

    def buildFromSegment(self, segment):
        self.spaceObject, self.pointOrient = self.createSpacePoint(segment.twistNode,
                                                                   segment.aimNode)

        jointName = segment.knotNode.Name.replace('_twistPoint1', '')
        self.aimTarget = self.createHelper(jointName,
                                           segment.twistNode,
                                           'AimTarget')

        self.twistReference = self.createHelper(jointName,
                                                segment.twistNode,
                                                'TwistReference')

        self.aimTarget.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(2.0, 0.0, 0.0)

        targetConstraint = self.constraintUtils.createTargetConstraint(self.twistReference,
                                                                       self.aimTarget,
                                                                       spaceObject=self.spaceObject)

        self.angleConstraint = mobu.FBConstraintRelation('{0}_angleExtract1'.format(knotNode.Name))


class curveSegment(object):
    def __init__(self):
        self.segmentStartParameter = -1.0
        self.segmentEndParameter = -1.0
        self.uValue = -1.0
        self.reverseAim = False

        self.inputNode = None
        self.targetNode = None
        self.spaceNode = None
        self.twistNode = None
        self.knotNode = None
        self.aimNode = None
        self.orientNode = None

        self.startControlIndex = 0
        self.endControlIndex = 0

    def setData(self,
                uValue,
                clampValue,
                inputNode,
                knotNode,
                ikControls):
        self.uValue = uValue*0.01
        self.segmentStartParameter = math.floor(uValue*0.01)

        self.segmentEndParameter = self.segmentStartParameter + 1.0

        if self.segmentEndParameter > clampValue:
            self.segmentEndParameter = clampValue

        self.inputNode = inputNode
        self.knotNode = knotNode

        self.startControlIndex = int(self.segmentStartParameter)
        self.endControlIndex = int(self.segmentEndParameter)

        self.spaceNode = ikControls[self.startControlIndex]
        self.twistNode = ikControls[self.endControlIndex]

    def __repr__(self):
        inputNode = None
        targetNode = None
        spaceNode = None
        knotNode = None
        twistNode = None
        aimNode = None
        orientNode = None

        nodeValues = [inputNode, targetNode, spaceNode, knotNode, twistNode, aimNode, orientNode]
        nodeAttributes = [self.inputNode, self.targetNode, self.spaceNode, self.knotNode, self.twistNode, self.aimNode, self.orientNode]

        for attributeIndex, attribute in enumerate(nodeAttributes):
            if attribute is not None:
                nodeValues[attributeIndex] = attribute.Name

        debugStream = "\n<curveSegment \n\tuValue:{0}".format(self.uValue)
        debugStream += "\n\tsegmentStartParameter:{0}".format(self.segmentStartParameter)
        debugStream += "\n\tsegmentEndParameter:{0}".format(self.segmentEndParameter)
        debugStream += "\n"
        debugStream += "\n\tstartControlIndex:{0}".format(self.startControlIndex)
        debugStream += "\n\tendControlIndex:{0}".format(self.endControlIndex)
        debugStream += "\n"
        debugStream += "\n\tinputNode:{0} \n\ttargetNode:{1} \n\tspaceNode:{2} \n\ttwistNode:{4} \n\tknotNode:{3} \n\taimNode:{5} \n\torientNode:{6}".format(*nodeValues)
        debugStream += ">"

        return debugStream


class SetupCoralSnake(object):
    jointSources = ['SKEL_Head',
                    'SKEL_Spine13',
                    'SKEL_Spine12',
                    'SKEL_Spine11',
                    'SKEL_Spine10',
                    'SKEL_Spine9',
                    'SKEL_Spine8',
                    'SKEL_Spine7',
                    'SKEL_Spine6',
                    'SKEL_Spine5',
                    'SKEL_Spine4',
                    'SKEL_Spine3',
                    'SKEL_Spine2',
                    'SKEL_Spine1',
                    'SKEL_Spine0',
                    'SKEL_Tail0',
                    'SKEL_Tail1',
                    'SKEL_Tail2',
                    'SKEL_Tail3',
                    'SKEL_Tail4',
                    'SKEL_Tail5',
                    'SKEL_Tail6',
                    'SKEL_Tail7',
                    'SKEL_Tail8',
                    'SKEL_Tail9',
                    'SKEL_Tail10',
                    'SKEL_Tail11',
                    'SKEL_Tail12',
                    'SKEL_Tail13',
                    'SKEL_Tail14',
                    'SKEL_Tail15',
                    'SKEL_Tail15_NUB']

    BONE_PREFIX = 'SKEL_'

    HELPER_PREFIX = 'MH_'

    def __init__(self):
        self.constraintUtils = ikUtils.ConstraintUtils()
        self.ikSplineStructure = IkSplineStructure()

    def prepareSnakeIkRig(self, 
                          startIndex=0,
                          reverseCurve=True,
                          useMhJoints=False):
        jointNameSources = self.jointSources[:]
        jointList = mobu.FBModelList()

        jointNames = []

        if reverseCurve is True:
            jointNameSources.reverse()

        if useMhJoints is False:
            for objIndex in range(startIndex, len(jointNameSources)):
                joint = mobu.FBFindModelByLabelName(jointNameSources[objIndex])    
                jointList.append(joint)
                jointNames.append(jointNameSources[objIndex])
        else:
            for objIndex in range(startIndex, len(jointNameSources)):
                jointName = jointNameSources[objIndex].replace(self.BONE_PREFIX,
                                                               self.HELPER_PREFIX)

                joint = mobu.FBFindModelByLabelName(jointName)

                jointList.append(joint)
                jointNames.append(jointName)


        return jointList, jointNames

    def compensateSnakeSpineOrient(self,
                                   spineBones,
                                   spineTweakers,
                                   tweakerTargets,
                                   constraintFolder,
                                   splineIkRig):
        aimFolder = None 
        bindObjectList = []
        anchorList = []

        self.RefreshScene()
        reverseAim = True

        jointLimit = len(spineBones)

        if len(spineTweakers) < jointLimit:
            jointLimit = len(spineTweakers)

        if len(tweakerTargets) < jointLimit:
            jointLimit = len(tweakerTargets)

        for boneIndex in range(jointLimit):
            joint = spineBones[boneIndex]
            baseControl = spineTweakers[boneIndex]
            aimControl = tweakerTargets[boneIndex]

            bindObject, targetConstraint , anchor = self.constraintUtils.layoutTweakerAimRig(joint, baseControl, aimControl, reverseAim)
            anchor.Name = "{0}_attach1".format(joint.LongName)
            anchorList.append(anchor)

            self.ikSplineStructure.connectMetaData(splineIkRig, 'snakeCompensationGroup', anchor)
            self.ikSplineStructure.connectMetaData(splineIkRig, 'snakeCompensationGroup', bindObject)
            self.ikSplineStructure.connectMetaData(splineIkRig, 'snakeCompensationGroup', targetConstraint)

            if boneIndex == 0:
                aimFolder = mobu.FBFolder('ikSpline_aimConstraints1',anchor)
            else:
                aimFolder.Items.append(anchor)

            aimFolder.Items.append(targetConstraint)
            bindObjectList.append(bindObject)
            bindObject.Show = False
            bindObject.Visibility = False

        constraintFolder.Items.append(aimFolder)
        self.RefreshScene()

        for anchor in anchorList:
            anchor.Snap()
            anchor.Active = True
            anchor.Lock = True

        if aimFolder is not None:
            self.ikSplineStructure.connectMetaData(splineIkRig,'snakeCompensationGroup',aimFolder)

        self.RefreshScene()
        return bindObjectList

    def prepareSnakeSpine(self, 
                          jointNames, 
                          constraintFolder, 
                          rigStructure, 
                          startIndex=0):
        endIndexSource = 16 + 1
        endIndex = endIndexSource - startIndex
        #We want to correct SKEL_Tail0 as well
        spineBones = mobu.FBModelList()
        for obj in jointNames[1:endIndex]:
            joint = mobu.FBFindModelByLabelName(obj)    
            spineBones.append(joint)

        endIndex = (endIndexSource - 1) - startIndex
        tweakerTargets = mobu.FBModelList()
        for obj in jointNames[0:endIndex]:
            joint = mobu.FBFindModelByLabelName('{}_Tweaker'.format(obj))    
            tweakerTargets.append(joint)

        endIndex = endIndexSource - startIndex 
        spineTweakers = mobu.FBModelList()
        for obj in jointNames[1:endIndex]:
            joint = mobu.FBFindModelByLabelName('{}_Tweaker'.format(obj))    
            spineTweakers.append(joint)

        bindObjectList = self.compensateSnakeSpineOrient(spineBones,
                                                         spineTweakers,
                                                         tweakerTargets,
                                                         constraintFolder,
                                                         rigStructure)

        for bindObject in bindObjectList:
            bindObject.Show = False

    def setupHeadControl(self, 
                         jointList, 
                         constraintFolder, 
                         splineData, 
                         splineIkRig,
                         useEndBone=True):
        headOrient = Constraint.CreateConstraint(Constraint.ROTATION)
        headOrient.Name = '{}_Orient1'.format(jointList[0].Name)

        ctrl = mobu.FBFindModelByLabelName(splineData[0][0]) 
        bindJoint = jointList[0]
        if useEndBone is True:
            ctrl = mobu.FBFindModelByLabelName(splineData[-1][0]) 

        headOrient.ReferenceAdd (0, bindJoint)
        headOrient.ReferenceAdd (1,ctrl) 
        headOrient.Snap()
        headOrient.Active = True

        self.ikSplineStructure.connectMetaData(splineIkRig,'snakeCompensationGroup',headOrient)
        constraintFolder.Items.append(headOrient)

    def correctSpineOrients(self,
                            splineData, 
                            rigStructureSettings,
                            startIndex=0, 
                            skipHeadAndExtension=True,
                            skipCompensation=False,
                            useMhJoints=False):
        splineIkRig = mobu.FBFindModelByLabelName(splineData[1])

        jointList = [] 
        jointNames = []

        metaData = splineIkRig.PropertyList.Find('snakeCompensationGroup')

        groupMembersCount = metaData.GetSrcCount()

        if groupMembersCount == 0:
            jointList, jointNames = self.prepareSnakeIkRig(startIndex=startIndex,
                                                           reverseCurve=True,
                                                           useMhJoints=useMhJoints)
        else:
            for memberIndex in range(groupMembersCount):
                member = metaData.GetSrc(memberIndex)
                if member is None:
                    continue

                jointList.append(member)
                jointNames.append(member.LongName)
        
        constraintFolder = None

        for grp in mobu.FBSystem().Scene.Folders:
            if grp.Name == str(splineData[2]):
                constraintFolder = grp

        if constraintFolder is None:
            return

        if skipCompensation is False:
            self.prepareSnakeSpine(jointNames, 
                                   constraintFolder, 
                                   splineIkRig,
                                   startIndex=startIndex)

        self.addCharacterExtension(rigStructureSettings, 
                                   splineIkRig)

        if skipHeadAndExtension is True:
            return

        self.setupHeadControl(jointList, 
                              constraintFolder, 
                              splineData, 
                              splineIkRig)

    def addCharacterExtension(self, splineData, splineIkRig,skipConnection=False):
        #check if we have a valid FBCharacter
        metaRoot = splineIkRig.PropertyList.Find('ikSplineMetaRoot')
        if metaRoot is None:
            return

        characterLink = splineIkRig.PropertyList.Find('characterGroup')
        snakeCharacter = characterLink.GetSrc(0)

        if snakeCharacter is None:
            return


        if skipConnection is True:
            return

        #disable Constraints Here
        anchorConstraint = splineIkRig.PropertyList.Find('anchorConstraint')
        numberOfAnchorConstraint = anchorConstraint.GetSrcCount()

        aimConstraint = splineIkRig.PropertyList.Find('snakeCompensationGroup')
        numberOfAimConstraint = aimConstraint.GetSrcCount()

        constraintList = []
        for memberIndex in range(numberOfAnchorConstraint):
            member = anchorConstraint.GetSrc(memberIndex)
            if member is None:
                continue

            if not isinstance(member, mobu.FBConstraint):
                continue

            constraintList.append(member)
            member.Active = False

        for memberIndex in range(numberOfAimConstraint):
            member = aimConstraint.GetSrc(memberIndex)
            if member is None:
                continue

            if not isinstance(member, mobu.FBConstraint):
                continue

            constraintList.append(member)
            member.Active = False

        mobu.FBSystem().Scene.Evaluate()
        snakeExtension = mobu.FBCharacterExtension("snakeIkControls")
        snakeCharacter.AddCharacterExtension(snakeExtension)

        mobu.FBSystem().Scene.Evaluate()

        for constraint in constraintList:
            constraint.Snap()
            constraint.Active = True

        mobu.FBSystem().Scene.Evaluate()

        ikControls = splineData.ikControlsData.ikControls

        for control in ikControls:
            if control is None:
                continue
            mobu.FBConnect(control, snakeExtension)

        ikControls = splineData.tweakerList

        for control in ikControls:
            if control is None:
                continue

            mobu.FBConnect(control[1], snakeExtension)

        structureFn = IkSplineStructure()
        structureFn.connectMetaData(splineIkRig, 
                                    'characterExtensionGroup',
                                    snakeExtension)

    def connectOffset(self, ikSplineStructure):
        if ikSplineStructure is None:
            return

        if isinstance(ikSplineStructure, ikSettings.IkSplineStructureSettings) is not True:
            return

        if ikSplineStructure.ikControlsData is None:
            return

        if ikSplineStructure.ikControlsData.ikControls is None:
            return

        if len(ikSplineStructure.ikControlsData.ikControls)<1:
            return

        if ikSplineStructure.ikConstraint is None:
            return

        for objectModel in ikSplineStructure.ikControlsData.ikControls:
            if objectModel != None:
                #Connect motionPath offset
                offsetAttribute = ikSplineStructure.ikConstraint.PropertyList.Find('Offset')

                slideReference = objectModel.PropertyCreate('slide_offset',
                                                            mobu.FBPropertyType.kFBPT_Reference,
                                                            '',
                                                            True,
                                                            True,
                                                            offsetAttribute)
        self.RefreshScene()

    def RefreshScene(self):
        """
            Force Evaluation of current scene . 
        """
        mobu.FBSystem().Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()


class IkSplineLayer(object):
    def __init__(self):
        self.constraintUtils =  ikUtils.ConstraintUtils()
        self.ikSplineStructure = IkSplineStructure()

    def RefreshScene(self):
        """
            Force Evaluation of current scene . 
        """
        mobu.FBSystem().Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

    def buildSplineMatrix(self,
                          splineObject,
                          ikConstraint,
                          ikSplineSettings):
        '''
            Method to understand the splineMatrix behavior

            Args:  
                splineObject (FBModelPath3d): CurveShape use by
                the ikSpline constraint
                
                ikConstraint (FBConstraint): ikSpline constraint
        '''
        curveMaxRange = splineObject.PathKeyGetCount()-1
        subSegmentSamples = 10
        rollCount = curveMaxRange*subSegmentSamples + 1
            
        debug_Grp = IkSplineStructure().createRigGroup('splineMatrix_debug_Grp')
        splineKnots = []
        for knotIndex in range(rollCount):
            nullObject = mobu.FBModelNull('splineMatrix_debug{0}'.format(knotIndex))

            nullObject.PropertyList.Find('RotationAxisVisibility').Data = True
            nullObject.PropertyList.Find('Look').Data = 1
            nullObject.PropertyList.Find('PivotsVisibility').Data = 2
            nullObject.PropertyList.Find('ReferentialSize').Data = ikSplineSettings.upVectorOffset 

            nullObject.Show = True
            ikConstraint.ReferenceAdd(3,nullObject)

            nullObject.Parent = debug_Grp
            splineKnots.append(nullObject)


        splineKnots.append(debug_Grp)
            
        return splineKnots

    def buildTwistDriverFromCurve(self, 
                                  splineName,
                                  controlSize,
                                  overrideCurveDivision=None):
        includeNamespace = False

        if ':' in splineName:
            includeNamespace = True

        splineObject = Scene.FindModelByName(splineName,
                                             includeNamespace=includeNamespace)

        if splineObject is None:
            return
        
        curveMaxRange = splineObject.PathKeyGetCount()-1
        ikConstraint = self.constraintUtils.createIkConstraint(splineObject,None)

        numberOfCurveController = splineObject.PathKeyGetCount()
        uRatio = float( numberOfCurveController ) / float( numberOfCurveController-1) 

        if overrideCurveDivision is not None:
            uRatio = float( numberOfCurveController ) / float( overrideCurveDivision-1) 
            numberOfCurveController = int(overrideCurveDivision)

        controlList = []
        matrixList = []

        for controllerIndex  in range(numberOfCurveController):
            loc = mobu.FBModelNull('bakeLoc1')
            loc.Show = True
            loc.Visible = True

            ikConstraint.ReferenceAdd(1, loc)  
            controlList.append(loc)

        ikConstraint.Active  = True

        for controllerIndex  in range(numberOfCurveController):
            uValueHandle = ikConstraint.PropertyList.Find('uValue{0}'.format(controllerIndex+1)) 
            kPercent =  controllerIndex*100.0
            if overrideCurveDivision is not None:
                kPercentSource =  controllerIndex*uRatio
                kPercent =  kPercentSource *100.0 

            uValueHandle.Data = kPercent

        for controllerIndex  in range(numberOfCurveController):
            controlMatrix = mobu.FBMatrix()
            dupControl = copy.copy(controlList[controllerIndex])

            controlList[controllerIndex].GetMatrix(controlMatrix)
            dupControl.SetMatrix(controlMatrix)

            matrixList.append(controlMatrix)
            dupControl.FBDelete()

        ikConstraint.FBDelete()
        for controllerIndex  in range(numberOfCurveController):
            controlList[controllerIndex].FBDelete()

        controlList = []
        for controllerIndex  in range(numberOfCurveController):
            controlName = '{}_Ctrl{:02d}'.format(splineObject.Name,(controllerIndex+1))
            control = self.createControl(controlName,
                                         'IK_Control',
                                         controlSize)
            control.Show = True
            control.Visibility = True
            control.SetMatrix(matrixList[controllerIndex])
            controlList.append(control)

        return controlList

    def buildChainCurveFromConstraint(self,splineIkPathName='splineIkPath1_constraint'):
        chainData = self.extractChainData(splineIkPathName)

        mGroupConstrain = 1
        jointDrivenCount = int(chainData['jointDrivenCount'])

        pointList = []
        for k in range(jointDrivenCount):
            loc = chainData['splineIK'].ReferenceGet( mGroupConstrain, k ) 
            tempPos = mobu.FBVector3d()
            loc.GetVector(tempPos) 

            pointList.append(tempPos)
            
        curveObj = mobu.FBModelPath3D('ChainPreview1')
        curveObj.Show = True
        curveObj.Visible = True

        for pKeyIndex in range(len(pointList)):
            position = mobu.FBVector4d(pointList[pKeyIndex][0],
                                       pointList[pKeyIndex][1],
                                       pointList[pKeyIndex][2],
                                       1.0)
            try:
                # We call PathKeyGet to check if a key already exists
                # it will error out if it doesn't
                if curveObj.PathKeyGet(pKeyIndex):
                    curveObj.PathKeySet(pKeyIndex, position)

            except IndexError:
                curveObj.Segment_PathKeyAdd(pKeyIndex, position)
                curveObj.PathKeySet(pKeyIndex,position)

            curveObj.PathKeySetLeftRightTangent(pKeyIndex, 
                                                   position, 
                                                   position, 
                                                   position, 
                                                   True) 
        mGroupChain = 6
        previousChainCurve = chainData['splineIK'].ReferenceGet( mGroupChain, 0 ) 
        if previousChainCurve != None:
            chainData['splineIK'].ReferenceRemove(mGroupChain,previousChainCurve)

        chainData['splineIK'].ReferenceAdd(mGroupChain,curveObj)

    def createIkRig(self,
                    splineObject,
                    jointObjectArray,
                    ikSplineSettings):
        """
            This method will create controls from a curve path knot
            list, and use it as cluster deformer
            
            splineObject (FBModelPath3d): CurveShape use by
            the ikSpline constraint

        """

        ikGroup = mobu.FBGroup('{0}_IkControls'.format(splineObject.Name))

        controlList = self.buildTwistDriverFromCurve(splineObject.Name,
                                                    ikSplineSettings.ikControlsSize)

        for control  in controlList:
            ikGroup.ConnectSrc(control)

        shiftVector = mobu.FBVector3d(0,ikSplineSettings.upVectorOffset,0)

        #Hierarchy created will be
        #   UpVecTwistCntrl_1
        #   UpVectorCntrlGrp_1
        #   UpVectorCntrl_1
        UpVectorLines = mobu.FBModelList()
        UpVectorControls = mobu.FBModelList()
        TwistCntrlArray = []
        UpVectorGroup = None

        if ikSplineSettings.buildUpVector is True:
            numberOfCurveController = len(controlList)
            UpVectorGroup = mobu.FBGroup('{0}_UpVectorControls'.format(splineObject.Name))
            UpVectorGroup.Show = True

            for jointIndex  in range(numberOfCurveController):
                TwistCntrlName = '{}UpVecTwistCntrl{:02d}'.format(splineObject.Name,(jointIndex+1))
                TwistCntrl = self.ikSplineStructure.createRigGroup( TwistCntrlName)
                TwistCntrl.Show = True
                
                UpVectorCntrlGrpName = '{}UpVectorCntrlGrp{:02d}'.format(splineObject.Name,(jointIndex+1))
                UpVectorCntrlGrp = self.ikSplineStructure.createRigGroup( UpVectorCntrlGrpName)
                UpVectorCntrlGrp.Show = True
                
                UpVectorCntrlGrp.Parent = TwistCntrl
                if ikSplineSettings.buildUpVector:
                    UpVectorCntrlName = '{}UpVectorCntrl{:02d}'.format(splineObject.Name,(jointIndex+1))
                    UpVectorCntrl = self.createControl(UpVectorCntrlName,
                                                       'UpVector',
                                                       ikSplineSettings.ikControlsSize*0.8)
                    UpVectorCntrl.Show = True
                    UpVectorCntrl.Parent = UpVectorCntrlGrp
                
                controlMatrix = mobu.FBMatrix()
                controlList[jointIndex].GetMatrix(controlMatrix)
                TwistCntrl.SetMatrix(controlMatrix)

                #controlList[jointIndex].SetMatrix(matrixList[jointIndex])
                
                TwistCntrl.Parent = controlList[jointIndex]
                UpVectorCntrlGrp.PropertyList.Find('Translation (Lcl)').Data = shiftVector
                
                upVectorElements = mobu.FBModelList()
                upVectorElements.append(TwistCntrl)

                upVectorElements.append(UpVectorCntrl)

                UpVectorLineName = '{}UpVectorLine{:02d}'.format(splineObject.Name,(jointIndex+1))
                if ikSplineSettings.buildUpVector:
                    UpVectorLine = self.createUpVectorLine(upVectorElements,
                                                           UpVectorLineName)
                                            
                    UpVectorLines.append(UpVectorLine)
                    UpVectorControls.append(UpVectorCntrl)
                    UpVectorGroup.ConnectSrc(UpVectorCntrl)
                
                for object in [TwistCntrl ]:
                    rotateHandle = object.PropertyList.Find('Rotation (Lcl)') 
                    translateHandle = object.PropertyList.Find('Translation (Lcl)') 
                    translateHandle.SetLocked = True
                    
                    for j in range(3):
                        translateHandle.SetMemberLocked(j,True)
                
                TwistCntrlArray.append(TwistCntrl)
                TwistCntrlArray.append(UpVectorCntrlGrp)

        if ikSplineSettings.lockFirstIkTranslation:
            rotateHandle = controlList[0].PropertyList.Find('Rotation (Lcl)') 
            translateHandle = controlList[0].PropertyList.Find('Translation (Lcl)') 
            
            rotateHandle.SetLocked = True
            translateHandle.SetLocked = True
            
            for j in range(3):
                rotateHandle.SetMemberLocked(j,True)
                translateHandle.SetMemberLocked(j,True)
        
        ikControlSettings = ikSettings.IkControlSettings()

        ikControlSettings.ikControls = controlList
        ikControlSettings.UpVectorControls = UpVectorControls
        ikControlSettings.UpVectorLines = UpVectorLines
        ikControlSettings.ikGroup = ikGroup
        ikControlSettings.UpVectorGroup = UpVectorGroup

        return ikControlSettings

    def createFkRig(self,
                    ikControlsData,
                    ikSplineSettings,
                    parentIkcontrollers=True):
        """
            This method will setup FK rig
            Args:
                ikControls (FBModelList): list of IK Objects used to align the FK hierarchy
                fkOffset (double3 list) : value to orient the fk controller
                size (float)
                
            return type (dictionary)
        """
        ikControls = ikControlsData.ikControls
        fk_controlList = mobu.FBModelList()
        
        fkZeroGrps  = []
        fkOffset_List  = mobu.FBModelList()
        rotateOffsetValue = mobu.FBVector3d(ikSplineSettings.fkOffset)

        #We will organize the fk into this hierarchy
        # * ZRO
        # * --> OFFSET
        # * ----> CONTROLS

        fkGroup = mobu.FBGroup('splineIk_FkControls')
        if ikSplineSettings.lockFirstIkTranslation:
            rotateHandle = ikControls[0].PropertyList.Find('Rotation (Lcl)') 
            translateHandle = ikControls[0].PropertyList.Find('Translation (Lcl)') 
            
            rotateHandle.SetLocked = False
            translateHandle.SetLocked = False
            
            for channelIndex in range(3):
                rotateHandle.SetMemberLocked(channelIndex, False)
                translateHandle.SetMemberLocked(channelIndex, False)

        constraintList = []
        for pKeyIndex,ikControl in enumerate(ikControls):
            controlName = '{}_fkCtrl{:02d}'.format('splineIK',(pKeyIndex+1))
            control = self.createControl(controlName,
                                         'FK_Control',
                                         ikSplineSettings.fkControlsSize)

            zeroGroup = self.ikSplineStructure.createRigGroup('{0}_OffsetZro'.format(controlName))    


            control.Parent = zeroGroup
            constraint = self.constraintUtils.alignWithConstraint(zeroGroup, ikControl)
            fkGroup.ConnectSrc(control)
            constraintList.append(constraint)
            
            fkZeroGrps.append(zeroGroup)
            fk_controlList.append(control)

        self.RefreshScene()

        for pKeyIndex,ikControl in enumerate(ikControls):
            self.constraintUtils.bakeConstraintValue(constraintList[pKeyIndex])

        for groupIndex in range(1,len(fkZeroGrps)):
            fkZeroGrps[groupIndex].Parent = fk_controlList[groupIndex-1]

        #Now parent IK_Control under fk hierachy to speed up rigs
        if parentIkcontrollers is True:
            for groupIndex in range( len(fkZeroGrps)):
                rotateHandle = fk_controlList[groupIndex].PropertyList.Find('GeometricRotation') #'Rotation (Lcl)'
                rotateHandle.Data = rotateOffsetValue
                
                ikControls[groupIndex].Parent = fk_controlList[groupIndex]
                ikControls[groupIndex].PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d() 
                
                translateHandle = fk_controlList[groupIndex].PropertyList.Find('Translation (Lcl)')
                translateHandle.SetLocked = True
                
                for channelIndex in range(3):
                    translateHandle.SetMemberLocked(channelIndex, True)
        else:
            for groupIndex in range( len(fkZeroGrps)):
                rotateHandle = fk_controlList[groupIndex].PropertyList.Find('GeometricRotation') #'Rotation (Lcl)'
                rotateHandle.Data = rotateOffsetValue

        if ikSplineSettings.lockFirstIkTranslation:
            rotateHandle = ikControls[0].PropertyList.Find('Rotation (Lcl)') 
            translateHandle = ikControls[0].PropertyList.Find('Translation (Lcl)') 
            
            rotateHandle.SetLocked = True
            translateHandle.SetLocked = True
            
            for channelIndex in range(3):
                rotateHandle.SetMemberLocked(channelIndex, True)
                translateHandle.SetMemberLocked(channelIndex, True)

        fkControlSettings = ikSettings.FkControlSettings()

        fkControlSettings.fk_controlList = fk_controlList
        fkControlSettings.fkZeroGroups = fkZeroGrps
        fkControlSettings.fkGroup = fkGroup

        return fkControlSettings

    def createUpVectorLine(self, 
                           upVectorElements,
                           lineName):
        """
            This method will build a line from 2 transform
            Args:
                upVectorElements (FBModelList): transform used for start/end points
                lineName (str): name of the line we will create
                
            return FBModelPath3D
        """
        # create a path to use as an ikSPline
        line = mobu.FBModelPath3D(lineName)
        line.Show = True

        # clear all existing path keys to delete the verts on the curve
        line.PathKeyClear()

        for idx in range (len(upVectorElements)):
            line.PathKeyEndAdd(mobu.FBVector4d(0.0,0.0,0.0, 1.0))
            pointPlug = line.PropertyList.Find('Point_{}'.format(idx))
            pointPlug.Data = mobu.FBVector4d()
            
            pointPlug.ConnectSrc(upVectorElements[idx])

        self.colorizeObject(line,(0,0.0,1.0))
        return line

    def constrainSplineToControls(self,
                                  splineObject,
                                  controlList,
                                  bindMethod='knotTranslation'):
        """
            This method will use controls as cluster deformer for
            the provided splineObject
            
            splineObject (FBModelPath3d): CurveShape use by
            the ikSpline constraint
            
            controlList (list of FBModel): mobu FBModel which will deform the curve
                
            bindMethod (enum): ('knotTranslation'/'cluster')
            the method use to deform splineObject path
        """
        curveDeformer = None
        if bindMethod=='knotTranslation':
            for pKeyIndex  in range(len(controlList)):           
                pointPlug = splineObject.PropertyList.Find('Point_{}'.format(pKeyIndex))
                if pointPlug is None:
                    continue
                pointPlug.Data = mobu.FBVector4d()
                
                pointPlug.ConnectSrc(controlList[pKeyIndex])
        if bindMethod=='cluster':
            curveDeformer = self.deformSplineIkPath(splineObject,
                                                       controlList)

        return curveDeformer

    def connectUpVectorOffset(self,
                              ikConstraint,
                              UpVectorControls,
                              connectionMode='relationship'):
        if connectionMode=='direct':
            for index,control in enumerate(UpVectorControls):
                upVectorPlug = ikConstraint.PropertyList.Find('upVector{}'.format(index+1))    
                controlTranslateHandle = control.PropertyList.Find('Translation (Lcl)')             
                connectionState = upVectorPlug.ConnectSrc(controlTranslateHandle)

                upVectorOffsetPlug = ikConstraint.PropertyList.Find('upVectorOffset{}'.format(index+1))   
                controlParentTranslateHandle = control.Parent.PropertyList.Find('Translation (Lcl)')   
                connectionState =  upVectorOffsetPlug.ConnectSrc(controlParentTranslateHandle)
        if connectionMode=='relationship':
            lRelation = mobu.FBConstraintRelation("splineIk_UP_VectorConnections1") 
            MotionPathBox = lRelation.ConstrainObject( ikConstraint )
            
            for index,upCtrl in enumerate(UpVectorControls):
                upCtrlBox = lRelation.SetAsSource(upCtrl)
                upCtrlBox.UseGlobalTransforms = False
                controlTranslateHandle = upCtrlBox.AnimationNodeOutGet().Nodes[0] 
                
                upCtrlOffsetBox = lRelation.SetAsSource(upCtrl.Parent)
                upCtrlOffsetBox.UseGlobalTransforms = False
                controlOffsetTranslateHandle = upCtrlOffsetBox.AnimationNodeOutGet().Nodes[0] 
                
                
                upVectorPlug = None
                upVectorOffsetPlug = None
                for property in MotionPathBox.AnimationNodeInGet().Nodes:
                    if property.Name == 'upVector{}'.format(index+1):
                        upVectorPlug = property
                    if property.Name == 'upVectorOffset{}'.format(index+1):
                        upVectorOffsetPlug = property

                mobu.FBConnect(controlTranslateHandle ,upVectorPlug )  
                mobu.FBConnect(controlOffsetTranslateHandle ,upVectorOffsetPlug )  
                
            lRelation.Active = True
        if connectionMode=='constraintGroup':
            for index,upCtrl in enumerate(UpVectorControls):
                ikConstraint.ReferenceAdd	(4,upCtrl)
                ikConstraint.ReferenceAdd	(5,upCtrl.Parent)

    def createControl(self,
                      name,
                      type,
                      size):
        marker = mobu.FBModelMarker(name)

        if type=='FK_Control':
            marker.PropertyList.Find('Look').Data = 6
            self.colorizeObject(marker,(255,0,0))

        if type=='IK_Control':
            marker.PropertyList.Find('Look').Data = 1
            axis = Controllers.AxisController().create()

            axis.Name = '{0}_Axis'.format(name)

            axis.Show = True

            axis.GeometricScaling = mobu.FBVector3d(size*0.03,
                                                    size*0.03,
                                                    size*0.03)

            axis.Parent = marker

            axis.Pickable = False

            axis.Transformable = False

        if type=='UpVector':
            marker.PropertyList.Find('Look').Data = 3
            self.colorizeObject(marker,(0,0.0,1.0))

        if type=='Tweaker':
            marker.PropertyList.Find('Look').Data = 8
            self.colorizeObject(marker,(255,0,0))

        marker.Show = True
        marker.Size = size

        return marker

    def colorizeObject(self, objectToColor, color):
        """
        colorize an object
        """
        colorProperty = objectToColor.PropertyList.Find('Color RGB')
        if colorProperty is None:
            colorProperty = objectToColor.PropertyList.Find('Curve Color')
        newColour = mobu.FBColor(color)
        colorProperty.Data = newColour

    def attachRigToAnchor(self,
                          anchorTarget,
                          rigStructure):
        if anchorTarget is None:
            return {'parentConstraint':None,
                    'scaleConstraint':None}
            
        anchorList = mobu.FBComponentList()
        mobu.FBFindObjectsByName('{0}*'.format(anchorTarget),
                                 anchorList,
                                 False,
                                 False)
                                 
        if anchorList.count() == 0:
            return {'parentConstraint':None,
                    'scaleConstraint':None}

        anchorTargetObject = anchorList[0] 

        curveFromNullArrayConstraintName = 'Parent/Child'
        cstMngr = mobu.FBConstraintManager()
        
        constraintIndex = -1
        parentConstraint = None

        for k in range(cstMngr.TypeGetCount ()):
            if cstMngr.TypeGetName(k) == curveFromNullArrayConstraintName:
                constraintIndex = k
                parentConstraint = cstMngr.TypeCreateConstraint( constraintIndex )   
                parentConstraint.Name = '{}_Anchor1'.format(anchorTarget)
                
                parentConstraint.ReferenceAdd(0,rigStructure)
                parentConstraint.ReferenceAdd(1,anchorTargetObject)
                
                parentConstraint.Snap()
                parentConstraint.Active = True
                
        scaleConstraint = Constraint.CreateConstraint(Constraint.SCALE)
        scaleConstraint.Name = '{}_Scale1'.format(anchorTarget)

        scaleConstraint.ReferenceAdd (0,rigStructure)
        scaleConstraint.ReferenceAdd (1,anchorTargetObject) 
        scaleConstraint.Snap()
        scaleConstraint.Active = True

        return {'parentConstraint':parentConstraint,
                'scaleConstraint':scaleConstraint}

    def deformSplineIkPath(self,
                           splineObject,
                           controlList):
        '''
            This method will bind a curve using curveFromNullArray constraint deformer
        '''
       
        #splineObject = mobu.FBFindModelByLabelName( splineObjectName )        
        curveFromNullArrayConstraintName = 'curveFromNullArray'
        cstMngr = mobu.FBConstraintManager()
        
        constraintIndex = -1
        curveFromNullArray = None

        for k in range(cstMngr.TypeGetCount ()):
            if cstMngr.TypeGetName(k) == curveFromNullArrayConstraintName:
                constraintIndex = k
                break

        curveFromNullArray = cstMngr.TypeCreateConstraint( constraintIndex )    
        curveFromNullArray.Name = splineObject.Name + '_Deformer1'

        mobu.FBSystem().Scene.Evaluate()
        curveFromNullArray.ReferenceAdd	(0,splineObject)  

        for i in range(len(controlList)):
            curveFromNullArray.ReferenceAdd	(1,controlList[i])  

        '''
        myExposer = mobu.FBCreateObject( 'Browsing/Templates/Elements', 'RockstarPathExposer', 'myRockstarPathExposer' )
        pathLengthProperty = splineObject.PropertyList.Find("PathLength")
        myExposer.PropertyAddReferenceProperty(pathLengthProperty)
        '''

        mobu.FBSystem().Scene.Evaluate()

        curveFromNullArray.Active = True
        return curveFromNullArray

    def connectTwistPropagation(self,
                                splineObject,
                                ikConstraint,
                                ikControls):
        """
            This method will setup ikSpline twist
            Args:
                splineObject (FBModelPath3d): CurveShape use by
                the ikSpline constraint
                
                ikConstraint   (custom ikSpline constraint)
                
                ikControls (modelList): ik Twist Drivers which
                influence the segment they are connected to
            
            return None
        """
        for ikControl in ikControls:
            ikConstraint.ReferenceAdd(2, ikControl)

    def RefreshScene(self):
        """
            Force Evaluation of current scene . 
        """
        mobu.FBSystem().Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

    def attachGameBones(self, 
                        gameBonesSettings,
                        ikSplineStructureSettings):
        jointArray = []
        constraintArray = []

        if ikSplineStructureSettings.rigRoot is None:
            if gameBonesSettings.skelRootArray is None:
                return

            if gameBonesSettings.skelRootAnchor is None:
                return

            skelRootAnchorObject = mobu.FBFindModelByLabelName(str(gameBonesSettings.skelRootAnchor))
            if skelRootAnchorObject is None:
                return constraintArray

            anchorList = self.constraintUtils.findConstraints(skelRootAnchorObject)
            if anchorList is None:
                return constraintArray

            if len(anchorList) <0:
                return constraintArray

            targetGroup = 1
            targetParent = anchorList[0].ReferenceGet(targetGroup, 0) 

            if targetParent is None:
                return constraintArray

            for joint in gameBonesSettings.skelRootArray :
                print joint
                jointObject = mobu.FBFindModelByLabelName(str(joint))

                if jointObject is None:
                    continue
                
                jointArray.append(jointObject)

            ikSplineStructureSettings.skelRootModelArray = jointArray
            ikSplineStructureSettings.skelRootModelAnchor = skelRootAnchorObject

        else:
            skelRootAnchorObject = None
            rootProperty = ikSplineStructureSettings.rigRoot.PropertyList.Find('skelRootModelAnchor')

            if rootProperty.GetSrcCount() > 0:
                skelRootAnchorObject = rootProperty.GetSrc(0)

            if skelRootAnchorObject is None:
                return

            jointArray = []
            jointProperty = ikSplineStructureSettings.rigRoot.PropertyList.Find('skelRootModelArray')
        
            groupMembersCount = jointProperty.GetSrcCount()

            for memberIndex in range(groupMembersCount):
                member = jointProperty.GetSrc(memberIndex)
                if member is None:
                    continue

                jointArray.append(member)

            ikSplineStructureSettings.skelRootModelArray = jointArray
            ikSplineStructureSettings.skelRootModelAnchor = skelRootAnchorObject

        if len(jointArray) < 1:
            return

        for joint in jointArray:
            parentConstraint = self.constraintUtils.constraintParts(joint, 
                                                                    skelRootAnchorObject,
                                                                    preserveOffset=True)
            parentConstraint.Name = '{0}_Glue1'.format(joint.Name)
            constraintArray.append(parentConstraint)

        ikSplineStructureSettings.gameJointConstraintArray = constraintArray

        if ikSplineStructureSettings.splineIkRig is not None:
            metadataList = [True , 'gameJointConstraints', constraintArray]
            for objectModel in metadataList[2]:
                self.connectMetaData(ikSplineStructureSettings.splineIkRig, metadataList[1], objectModel)

    def applyNamespace(self, splineIkRig):
        metaRoot = splineIkRig.PropertyList.Find('ikSplineMetaRoot')
        if metaRoot is None:
            return

        namespaceSegments = splineIkRig.LongName.split(":")

        if len(namespaceSegments) < 2:
            return 

        namespacePrefix = namespaceSegments[0]

        groupList = ['ikConstraint',
                     'splineMatrix',
                     'snakeCompensationGroup',
                     'tweakerGroup',
                     'bufferGroup',
                     'FkControlGroup',
                     'IkControlGroup',
                     'SplineCurve',
                     'anchorConstraint',
                     'folder',
                     'gameJointConstraints',
                     'characterExtensionGroup']

        renameArray = []
        for group in groupList:
            metaData = splineIkRig.PropertyList.Find(group)

            if metaData is None:
                continue

            groupMembersCount = metaData.GetSrcCount()
            if groupMembersCount == 0:
                continue

            for memberIndex in range(groupMembersCount):
                member = metaData.GetSrc(memberIndex)
                if member is None:
                    continue

                renameArray.append(member)

        for objectModel in renameArray:
            if namespacePrefix in objectModel.LongName:
                continue
            objectModel.Name = "{0}:{1}".format(namespacePrefix,objectModel.Name)

    def buildRootControls(self, tootNode):
        pass


class IkSplineStructure(object):
    def __init__(self):
        self.constraintUtils = ikUtils.ConstraintUtils()

    def createMetaRoot(self, splineIkRig, ikSplineSettings):
        root = splineIkRig.PropertyCreate('ikSplineMetaRoot', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True, None)
        root.Data = 'ikSplineRoot'

        rigSettings = json.dumps(ikSplineSettings.__dict__)
        settingAttribute = splineIkRig.PropertyCreate('rigSettings', mobu.FBPropertyType.kFBPT_charptr, 'String', False, True, None)
        settingAttribute.Data = rigSettings

        splineIkRig.PropertyCreate('controlsToDriveByMotionPath', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('layeredMotionPathGroup', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('snakeCompensationGroup', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('tweakerGroup', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('bufferGroup', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('FkControlGroup', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('IkControlGroup', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('SplineCurve', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('ikConstraint', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('anchorConstraint', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('folder', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('joints', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('splineMatrix', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('motionPathSlots', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('gameJointConstraints', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('skelRootModelArray', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('skelRootModelAnchor', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('rigStorageGroup', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('characterGroup', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )
        splineIkRig.PropertyCreate('characterExtensionGroup', mobu.FBPropertyType.kFBPT_object, 'Object', False, True, None )

    def connectMetaData(self,
                        splineIkRig,
                        category,
                        childModel):
        metaRootProperty = splineIkRig.PropertyList.Find(category)

        if metaRootProperty is None:
            return

        childModel.ConnectDst(metaRootProperty)

    def build(self,
              splineObject,
              ikSplineSettings,
              ikSplineStructureSettings,
              debugMode=False):
        """
            This method will organize your ikSpline rig
            Args:
                splineObject (FBModelPath3d): CurveShape use by
                the ikSpline constraint
                
                jointObjectArray (modelList): the object list
                we want to drive
                
                ikControlsData (modelList): ik  Drivers which
                influence the segment they are connected to
            
            return type dict
        """
        if ikSplineStructureSettings.jointObjectArray is None:
            return

        if ikSplineStructureSettings.rigRoot is None:
            splineIkRig = self.createRigGroup('{}_Rig'.format(ikSplineStructureSettings.ikConstraint.Name))
            self.createMetaRoot(splineIkRig,
                                ikSplineSettings)

            ikSplineStructureSettings.rigRoot = splineIkRig
        else:
            splineIkRig = ikSplineStructureSettings.rigRoot
            rigSettings = json.dumps(ikSplineSettings.__dict__)

            settingAttribute = splineIkRig.PropertyList.Find('rigSettings')
            settingAttribute.Data = rigSettings

        metadataList = [(False, 'anchorConstraint', ikSplineStructureSettings.constraintFolder),
                        (False, 'anchorConstraint', ikSplineStructureSettings.anchorFolder),
                        (False, 'ikConstraint', ikSplineStructureSettings.curveDeformer),
                        (False, 'SplineCurve', splineObject),
                        (False, 'ikConstraint', ikSplineStructureSettings.ikConstraint),
                        (False, 'IkControlGroup', ikSplineStructureSettings.ikControlsData.ikGroup),
                        (False, 'IkControlGroup', ikSplineStructureSettings.ikControlsData.UpVectorGroup),
                        (False, 'tweakerGroup', ikSplineStructureSettings.tweakerGroup),
                        (False, 'characterGroup', ikSplineStructureSettings.addExtensionToCharacter),
                        (True , 'anchorConstraint', ikSplineStructureSettings.anchorList),
                        (True , 'joints', ikSplineStructureSettings.jointObjectArray),
                        (True , 'splineMatrix', ikSplineStructureSettings.splineKnots),
                        (True , 'tweakerGroup', ikSplineStructureSettings.offsetList),
                        (True , 'IkControlGroup', ikSplineStructureSettings.ikControlsData.ikControls),
                        (True , 'IkControlGroup', ikSplineStructureSettings.ikControlsData.UpVectorControls),
                        (True , 'FkControlGroup', ikSplineStructureSettings.fkControls.fk_controlList),
                        (True , 'FkControlGroup', ikSplineStructureSettings.fkControls.fkZeroGroups),
                        (True , 'gameJointConstraints', ikSplineStructureSettings.gameJointConstraintArray),
                        (True , 'skelRootModelArray', ikSplineStructureSettings.skelRootModelArray),
                        (False , 'skelRootModelAnchor', ikSplineStructureSettings.skelRootModelAnchor),
                        (True , 'controlsToDriveByMotionPath', ikSplineStructureSettings.controlsToDriveByMotionPath)]

        for component in metadataList:
            if component[2] is not None:
                if component[0] is False:
                    self.connectMetaData(splineIkRig,component[1],component[2])
                else:
                    for objectModel in component[2]:
                        self.connectMetaData(splineIkRig, component[1], objectModel)

        if ikSplineStructureSettings.tweakerList is not None:
            for object in ikSplineStructureSettings.tweakerList:
                self.connectMetaData(splineIkRig,'tweakerGroup',object[1])
                object[1].Parent.Show = False

        controlRepositories = [('{0}_buffers'.format(ikSplineStructureSettings.ikConstraint.Name),
                                'bufferGroup', ikSplineStructureSettings.bufferList, False ),
                               ('{0}_tweakerControls'.format(ikSplineStructureSettings.ikConstraint.Name),
                               'tweakerGroup', ikSplineStructureSettings.tweakerOffsetList, True )]

        for component in controlRepositories:
            if len(component[2]) > 0:
                bufferGrp = self.createRigGroup(component[0])
                bufferGrp.Parent = splineIkRig

                for object in component[2]:
                    object.Parent = bufferGrp
                    self.connectMetaData(splineIkRig,component[1],object)

                bufferGrp.Visibility = component[3]
                self.connectMetaData(splineIkRig,'bufferGroup',bufferGrp)

        splineFKControlsGrp = None
        splineIKControlsGrp = None

        if ikSplineStructureSettings.fkControls.fkZeroGroups != None:
            splineFKControlsGrp = self.createRigGroup('{0}_splineFKControls'.format(ikSplineStructureSettings.ikConstraint.Name))
            splineFKControlsGrp.Parent = splineIkRig
            
            self.connectMetaData(splineIkRig,'FkControlGroup',splineFKControlsGrp)
        else:
            splineIKControlsGrp = self.createRigGroup('{0}_splineIKControls'.format(ikSplineStructureSettings.ikConstraint.Name))
            splineIKControlsGrp.Parent = splineIkRig

            self.connectMetaData(splineIkRig,'IkControlGroup',splineIKControlsGrp)
            
            zeroControlsGrpArray = []
            for control in ikSplineStructureSettings.ikControlsData.ikControls:
                zeroControlsGrp = self.createRigGroup('{0}_OffsetZero'.format(control.Name))
                zeroControlsGrp.Parent = splineIKControlsGrp

                self.connectMetaData(splineIkRig,'IkControlGroup',zeroControlsGrp)
                zeroControlsGrpArray.append(zeroControlsGrp)

            mobu.FBSystem().Scene.Evaluate()

            for jointIndex, control in enumerate(ikSplineStructureSettings.ikControlsData.ikControls):
                objectMatrix = mobu.FBMatrix()
                control.GetMatrix(objectMatrix)
                zeroControlsGrpArray[jointIndex].SetMatrix(objectMatrix)

            mobu.FBSystem().Scene.Evaluate()

            constraintArray = []
            for jointIndex, control in enumerate(ikSplineStructureSettings.ikControlsData.ikControls):
                control.Parent = zeroControlsGrpArray[jointIndex]
                objectMatrix = mobu.FBMatrix()

                constraintArray.append(self.constraintUtils.alignWithConstraint(control, 
                                                                                zeroControlsGrpArray[jointIndex], 
                                                                                preserveOffset=False))
            self.RefreshScene()
            for constraintIndex in range(len(constraintArray)):
                self.constraintUtils.bakeConstraintValue(constraintArray[constraintIndex])

                drivenObject = ikSplineStructureSettings.ikControlsData.ikControls[constraintIndex]
                drivenObject.PropertyList.Find('Rotation Pivot').Data = mobu.FBVector3d()
                drivenObject.PropertyList.Find('Scaling Pivot').Data = mobu.FBVector3d()
                drivenObject.PropertyList.Find('Rotation Offset').Data = mobu.FBVector3d()
            self.RefreshScene()

        upVecSplinesGrp = None
        if len(ikSplineStructureSettings.ikControlsData.UpVectorLines)>0:
            upVecSplinesGrp = self.createRigGroup('{0}_upVecSplinesGrp'.format(ikSplineStructureSettings.ikConstraint.Name))        
            upVecSplinesGrp.Parent = splineIkRig

            for object in ikSplineStructureSettings.ikControlsData.UpVectorLines:
                object.Parent = upVecSplinesGrp
                object.PropertyList.Find('Enable Transformation').Data = 0
                object.PropertyList.Find('Enable Selection').Data = 0

        splineObject.Parent = splineIkRig

        if ikSplineStructureSettings.fkControls.fkZeroGroups != None:
            ikSplineStructureSettings.fkControls.fkZeroGroups[0].Parent = splineFKControlsGrp

        for object in [splineFKControlsGrp,splineIkRig,upVecSplinesGrp]:
            if object != None:
                object.PropertyList.Find('Enable Transformation').Data = 0

        if ikSplineSettings.showSplineMatrix:
            ikSplineStructureSettings.splineKnots[-1].Parent = splineIkRig

        ikSplineStructureSettings.splineIkRig = splineIkRig

        if ikSplineSettings.rigStorage is None:
            return

        rigStorageObject = mobu.FBFindModelByLabelName(ikSplineSettings.rigStorage)
        
        if rigStorageObject is None:
            return

        splineIkRig.Parent = rigStorageObject

        self.connectMetaData(splineIkRig, 
                             'rigStorageGroup',
                             rigStorageObject)

    def RefreshScene(self):
        """
            Force Evaluation of current scene . 
        """
        mobu.FBSystem().Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

    def createRigGroup(self,
                       groupName):
        """
            This method create an empty group use for organizing your rig
            Args:
                groupName (str)
            return FBModelMarker            
        """
        Null = mobu.FBModelNull(groupName)
        Null.Show = True
        Null.Visibility = True
        #Null.Show = False
        Null.PropertyList.Find('Look').Data = 0

        return Null

    def initGroups(self, splineIkRig, splineIkGroupName='splineIkGroup1'):
        mainGroup = mobu.FBGroup('{0}Group1'.format(splineIkRig.Name))
        self.connectMetaData(splineIkRig,'folder',mainGroup)

        rigGroups = {'mainGroup':mainGroup}

        groupCategory = ['{0}_Controls'.format(splineIkRig.Name),
                         '{0}_Plot Group'.format(splineIkRig.Name),
                         '{0}_Geo'.format(splineIkRig.Name)]

        for group in groupCategory:
            rigGroups[group] = mobu.FBGroup(group)
            mainGroup.ConnectSrc(rigGroups[group])
            self.connectMetaData(splineIkRig,'folder',rigGroups[group])

        return rigGroups

    def buildConstraintFolder(self, folderItems, constraintFolder):
        for item in folderItems:
            if item != None:
                constraintFolder.Items.append(item)

    def validateRig(self):
        pass

    def prepareRigSettings(self):
        pass

    def finalize(self,
                 ikSplineSettings,
                 ikSplineStructureSettings,
                 parentToMoverControl=False):
                 
        rigGroups = self.initGroups(ikSplineStructureSettings.splineIkRig)
        splineIkRig = ikSplineStructureSettings.splineIkRig 

        for item in [ikSplineStructureSettings.fkControls.fkGroup, 
                     ikSplineStructureSettings.ikControlsData.ikGroup,
                     ikSplineStructureSettings.tweakerGroup, 
                     ikSplineStructureSettings.ikControlsData.UpVectorGroup]:
            if item != None:
                rigGroups['{0}_Controls'.format(splineIkRig.Name)].ConnectSrc(item)

        for item in ikSplineStructureSettings.jointObjectArray:
            rigGroups['{0}_Plot Group'.format(splineIkRig.Name)].ConnectSrc(item)

        anchorTargetObject = None
        if ikSplineSettings.rigAnchor != None:
            anchors = mobu.FBComponentList()
            mobu.FBFindObjectsByName('{0}*'.format(ikSplineSettings.rigAnchor),
                                     anchors,
                                     False,
                                     False)

            anchorTargetObject = anchors[0] 
        
            rigGroups['{0}_Plot Group'.format(splineIkRig.Name)].ConnectSrc(anchorTargetObject)
            
            #Try to add the mover(if any)
            mobu.FBFindObjectsByName('{0}*'.format('mover'),
                                     anchors,
                                     False,
                                     False)

            if len(anchors)>0:        
                rigGroups['{0}_Plot Group'.format(splineIkRig.Name)].ConnectSrc(anchors[0])
                
        if len(ikSplineSettings.renderGeometry)>0:
            anchors = mobu.FBComponentList()
            for item in ikSplineSettings.renderGeometry:
                mobu.FBFindObjectsByName('{0}*'.format(item),
                                         anchors,
                                         False,
                                         False)

                if len(anchors)>0: 
                    rigGroups['{0}_Geo'.format(splineIkRig.Name)].ConnectSrc(anchors[0])

        if ikSplineSettings.hidePath :
            splineObject.Visibility = False

        #--------------------------------------------ParentConstraint rig to the provided anchor
        parentConstraint = None
        scaleConstraint = None
        
        if ikSplineSettings.rigAnchor !=None:
            if ikSplineSettings.buildRigControl is False:
                anchorConstraints = IkSplineLayer().attachRigToAnchor(ikSplineSettings.rigAnchor,
                                                                      ikSplineStructureSettings.splineIkRig)

                parentConstraint = anchorConstraints['parentConstraint']
                scaleConstraint = anchorConstraints['scaleConstraint']

                self.connectMetaData(splineIkRig,'anchorConstraint',parentConstraint)
                self.connectMetaData(splineIkRig,'anchorConstraint',scaleConstraint)
            else:
                rootMarker = mobu.FBModelMarker('Root_control')
                rootMarker.PropertyList.Find('Look').Data = 1

                moverMarker = mobu.FBModelMarker('Mover_control')
                moverMarker.PropertyList.Find('Look').Data = 8

                self.connectMetaData(splineIkRig,'anchorConstraint',rootMarker)
                self.connectMetaData(splineIkRig,'anchorConstraint',moverMarker)

                moverMarker.Parent = rootMarker 
                if splineIkRig.Parent is not None:
                    rootMarker.Parent = splineIkRig.Parent

                if anchorTargetObject is not None:
                    rotateHandle = splineIkRig.PropertyList.Find('Rotation (Lcl)') 
                    translateHandle = splineIkRig.PropertyList.Find('Translation (Lcl)') 
                    
                    rotateHandle.SetLocked = False
                    translateHandle.SetLocked = False

                    alignContraint = self.constraintUtils.alignWithConstraint(rootMarker, 
                                                                              anchorTargetObject,
                                                                              preserveOffset=False)
                    self.constraintUtils.bakeConstraintValue(alignContraint)

                    parentConstraint = self.constraintUtils.alignWithConstraint(anchorTargetObject, moverMarker, preserveOffset=False)
                    parentConstraint.Name = '{}_Anchor1'.format(ikSplineStructureSettings.ikConstraint.Name)

                    scaleConstraint = Constraint.CreateConstraint(Constraint.SCALE)
                    scaleConstraint.Name = '{}_Scale1'.format(ikSplineStructureSettings.ikConstraint.Name)

                    scaleConstraint.ReferenceAdd (0,anchorTargetObject)
                    scaleConstraint.ReferenceAdd (1,moverMarker) 
                    scaleConstraint.Snap()
                    scaleConstraint.Active = True

                    self.connectMetaData(splineIkRig,'anchorConstraint',parentConstraint)
                    self.connectMetaData(splineIkRig,'anchorConstraint',scaleConstraint)

                    if parentToMoverControl is True:
                        splineIkRig.Parent = moverMarker
                    else:
                        anchorConstraints = IkSplineLayer().attachRigToAnchor(moverMarker.LongName,
                                                                              ikSplineStructureSettings.splineIkRig)

                        parentConstraint = anchorConstraints['parentConstraint']
                        scaleConstraint = anchorConstraints['scaleConstraint']

                        self.connectMetaData(splineIkRig,'anchorConstraint',parentConstraint)
                        self.connectMetaData(splineIkRig,'anchorConstraint',scaleConstraint)

        folderItems = [ parentConstraint, 
                        scaleConstraint,
                        ikSplineStructureSettings.curveDeformer, 
                        ikSplineStructureSettings.anchorFolder]

        #-----------------------------------------------------------Finalize rig presentation
        self.buildConstraintFolder(folderItems, ikSplineStructureSettings.constraintFolder)


class SetupRope(object):
    def __init__(self):
        self.constraintUtils = ikUtils.ConstraintUtils()
        self.ikSplineStructure = IkSplineStructure()
        self.bindDataUtils = ikUtils.BindDataUtils()
        self.ikSplineLayer = IkSplineLayer()

    def RefreshScene(self):
        """
            Force Evaluation of current scene . 
        """
        mobu.FBSystem().Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

    def validateRigSettings(self, jointObjectArray):
        if jointObjectArray is None:
            return False

        if len(jointObjectArray) < 3:
            return False

        return True

    def toggleJointAxis(self, jointObjectArray):
        for joint in jointObjectArray:
            joint.PropertyList.Find('RotationAxisVisibility').Data = True
            joint.PropertyList.Find('Look').Data = 1
            joint.PropertyList.Find('PivotsVisibility').Data = 2
            joint.PropertyList.Find('ReferentialSize').Data = 12.0

    def extractMotionPathData(self,
                              splineObject,
                              jointObjectArray):
        #Extract boneLengths
        segmentData = self.bindDataUtils.collectChainSegments(jointObjectArray)

        #Extract curveParameter
        bindData = self.bindDataUtils.extractJointOffsets(splineObject,
                                                          jointObjectArray,
                                                          segmentData['chainSegments'],
                                                          False)

        return bindData['uValueArray']

    def FindAnimationNode(self, inputBox, attributeName):
        drivenProperty = None
        for currentNode in inputBox.Nodes:
            if currentNode.Name == attributeName:
                drivenProperty = currentNode
                break
        return drivenProperty
   
    def lockUparameter(self,
                       jointObjectArray,
                       pathConstraintArray,
                       uValueArray,
                       constraintPrefix):
        constraint = mobu.FBConstraintRelation('{0}_relationLock1'.format(constraintPrefix))

        for jointIndex in range(len(pathConstraintArray)):
            joint = jointObjectArray[jointIndex]
            
            jointUvalueProperty = joint.PropertyCreate('uValue',
                                                   mobu.FBPropertyType.kFBPT_double,
                                                   'Number',
                                                   True,
                                                   True,
                                                   None)        

            jointUvalueProperty.SetAnimated(True)
            jointUvalueProperty.Data = uValueArray[jointIndex]*0.01

            jointSource = constraint.SetAsSource(joint)
            constraintTarget = constraint.ConstrainObject(pathConstraintArray[jointIndex])

            uValuePlug = self.FindAnimationNode(jointSource.AnimationNodeOutGet(), 'uValue')
            warpPlug = self.FindAnimationNode(constraintTarget.AnimationNodeInGet(), 'Warp')

            mobu.FBConnect(uValuePlug, warpPlug)

        constraint.Active = True
        return constraint

    def attachNodeToPath(self,
                         jointName,
                         knotSuffix,
                         constraintSuffix,
                         knotRoot,
                         splineObject,
                         uValue,
                         joint=None,
                         bakeOffset=False):
            knot = mobu.FBModelNull('{0}_{1}1'.format(jointName,
                                                      knotSuffix))

            knot.Show = True
            knot.PropertyList.Find('Look').Data = 0
            knot.Parent = knotRoot

            if joint is not None:
                alignNode = self.constraintUtils.alignWithConstraint(knot,
                                                                     joint)

                self.constraintUtils.bakeConstraintValue(alignNode)

            pathConstraint = Constraint.CreateConstraint(Constraint.PATH)
            pathConstraint.Show = True
            pathConstraint.ReferenceAdd(0, knot)
            pathConstraint.ReferenceAdd(1, splineObject)

            pathConstraint.PropertyList.Find('Warp Mode').Data = 1
            warpAttribute = pathConstraint.PropertyList.Find('Warp')
            warpAttribute.SetAnimated(False)

            animNode = warpAttribute.GetAnimationNode()
            if isinstance(animNode, mobu.FBAnimationNode):
                animNode.FCurve.EditClear()
                animNode.FBDelete()

            pathConstraint.PropertyList.Find('Warp').Data = uValue * 0.01
            warpAttribute = pathConstraint.PropertyList.Find('Warp')
            warpAttribute.SetLocked(True)

            if joint is not None:
                pathConstraint.Snap()

            if bakeOffset is False:
                pathConstraint.Active = True

            pathConstraint.Name = '{0}_{1}'.format(jointName,
                                                   constraintSuffix)



            linkProperty = knot.PropertyCreate('motionPathObject', 
                                               mobu.FBPropertyType.kFBPT_object,
                                               'Object',
                                               False,
                                               True,
                                               None )

            pathConstraint.ConnectDst(linkProperty)
                
            return pathConstraint, knot

    def createMotionpathLayer(self,
                              splineObject,
                              jointObjectArray,
                              ikControls,
                              constraintSuffix='pathConstraint1',
                              knotSuffix='knot',
                              useJointNode=True):
        curveMaxRange = splineObject.PathKeyGetCount() -1

        bakeOffset = False
        if useJointNode is True:
            uValueArray = self.extractMotionPathData(splineObject,
                                                     jointObjectArray)
            bakeOffset = True
        else:
            uValueArray = self.bindDataUtils.sampleCurveUvalues(splineObject)

        pathConstraintArray = []
        knotArray = []

        constraintFolder = None

        knotRoot = mobu.FBModelNull('{0}_{1}Grp1'.format(splineObject.Name,
                                                         knotSuffix))

        knotRoot.Visibility = True
        knotRoot.PropertyList.Find('Look').Data = 0
        uValueConstraint = None

        for jointIndex in range(len(uValueArray)):
            joint = None
            if useJointNode is True:
                joint = jointObjectArray[jointIndex]
                jointName = joint.Name
            else:
                jointName = '{0}_curvePoint{1}'.format(splineObject.Name,
                                                       jointIndex)

            pathConstraint, knot = self.attachNodeToPath(jointName,
                                                         knotSuffix,
                                                         constraintSuffix,
                                                         knotRoot,
                                                         splineObject,
                                                         uValueArray[jointIndex],
                                                         joint=joint,
                                                         bakeOffset=bakeOffset)
                         
            if jointIndex == 0:
                constraintFolder = mobu.FBFolder('{0}_{1}'.format(splineObject.Name, constraintSuffix), pathConstraint)
                constraintFolder.Show = True

            pathConstraintArray.append(pathConstraint)
            constraintFolder.ConnectSrc(pathConstraint)
            knotArray.append(knot)

        if jointObjectArray is None:
            uValueConstraint = self.lockUparameter(knotArray,
                                                   pathConstraintArray,
                                                   uValueArray,
                                                   '{0}_{1}'.format(splineObject.Name,
                                                                    constraintSuffix))

        else:
            uValueConstraint = self.lockUparameter(jointObjectArray,
                                                   pathConstraintArray,
                                                   uValueArray,
                                                   '{0}_{1}'.format(splineObject.Name,
                                                                    constraintSuffix))

        constraintFolder.ConnectSrc(uValueConstraint)

        return {'uValueArray':uValueArray,
                'pathConstraintArray':pathConstraintArray,
                'constraintFolder':constraintFolder,
                'ikControls':ikControls,
                'knotArray':knotArray,
                'knotRoot':knotRoot,
                'curveMaxRange':curveMaxRange,
                'uValueConstraint':uValueConstraint}

    def extractTargetNode(self, segmentArray):
        segmentCount = len(segmentArray)
        segmentCountLimit = segmentCount - 1

        for segmentIndex in range(len(segmentArray)):
            nextIndex = segmentIndex+1
            isReverse = False

            if nextIndex > segmentCountLimit:
                nextIndex = segmentCountLimit - 1
                isReverse = True

            segmentArray[segmentIndex].targetNode = segmentArray[nextIndex].knotNode
            segmentArray[segmentIndex].reverseAim = isReverse

    def createSpacePoint(self,
                         positionNode,
                         orientNode):
        jointName = positionNode.Name.replace('_splinePoint1', '')
        point = self.createHelper(jointName,
                                  positionNode,
                                  'spacePoint')

        #match orientation of orientNode
        pointOrient = Constraint.CreateConstraint(Constraint.ROTATION)
        pointOrient.Name = '{}_Orient1'.format(point.Name)

        pointOrient.ReferenceAdd(0, point)
        pointOrient.ReferenceAdd(1, orientNode) 

        pointOrient.Active = True
        pointOrient.Lock = True

        return point, pointOrient

    def createHelper(self,
                     jointName,
                     knotNode,
                     knotSuffix):
        aimPoint = mobu.FBModelNull('{0}_{1}1'.format(jointName,
                                                      knotSuffix))

        aimPoint.Show = True
        aimPoint.PropertyList.Find('Look').Data = 0
        aimPoint.Parent = knotNode

        alignNode = self.constraintUtils.alignWithConstraint(aimPoint,
                                                             knotNode)

        self.constraintUtils.bakeConstraintValue(alignNode)

        return aimPoint

    def createSplineMatrixSegment(self,
                                  splineObject,
                                  splineSegment,
                                  splineMatrixFolder):
        aimArray = []
        orientArray = []
        constraintFolder = None
        orientFolder = None

        for segmentIndex, segment in enumerate(splineSegment):
            previousIndex = segmentIndex - 1
            spaceNode = None

            if previousIndex < 0:
                spaceNode = segment.spaceNode
            else:
                spaceNode = splineSegment[previousIndex].inputNode

            jointName = segment.knotNode.Name.replace('_splinePoint1', '')
            aimPoint = self.createHelper(jointName,
                                         segment.knotNode,
                                         'aimPoint')

            orientPoint = self.createHelper(jointName,
                                            aimPoint,
                                            'orientPoint')

            segment.inputNode = aimPoint
            segment.aimNode = aimPoint
            segment.orientNode = orientPoint

            targetObject = segment.targetNode
            spaceObject, pointOrient = self.createSpacePoint(segment.knotNode, spaceNode)

            targetConstraint = self.constraintUtils.createTargetConstraint(aimPoint,
                                                                           segment.targetNode,
                                                                           spaceObject=spaceObject,
                                                                           reverseAim=segment.reverseAim)

            if segmentIndex == 0:
                constraintFolder = mobu.FBFolder('{0}_{1}'.format(splineObject.Name, 'targetConstraints1'), targetConstraint[0])
                constraintFolder.Show = True

                orientFolder = mobu.FBFolder('{0}_{1}'.format(splineObject.Name, 'orientConstraints1'), pointOrient)
                orientFolder.Show = True

            constraintFolder.ConnectSrc(targetConstraint[0])
            orientFolder.ConnectSrc(pointOrient)

            targetConstraint[0].Name = '{0}_target1'.format(aimPoint.Name)
            aimArray.append(aimPoint)
            orientArray.append(orientPoint)

        segment = splineSegment[-1]
        twistConstraint = self.constraintUtils.createTwistConstraint(orientArray,
                                                                     segment.twistNode,
                                                                     segment.aimNode)
        twistConstraint.Name = '{0}_{1}'.format(splineObject.Name, 'twistDriver1')
        return constraintFolder, orientFolder, twistConstraint

    def createSplineMatrixLayer(self,
                                splineObject,
                                jointObjectArray,
                                ikControls):
        splineMatrixData = self.createMotionpathLayer(splineObject,
                                                      None,
                                                      ikControls,
                                                      constraintSuffix='splineConstraint1',
                                                      knotSuffix='splinePoint',
                                                      useJointNode=False)

        splineSegment, segmentArray = self.extractSplineSegment(splineObject, 
                                                                splineMatrixData,
                                                                splineMatrixData['knotArray'])

        self.extractTargetNode(segmentArray)

        dummyOrient = Constraint.CreateConstraint(Constraint.ROTATION)
        splineMatrixFolder = mobu.FBFolder('{0}_{1}'.format(splineObject.Name, 'splineMatrixConstraints1'), dummyOrient)

        constraintFolderArray = []
        orientFolderArray = []
        twistConstraintArray = []

        for segment in splineSegment:
            constraintFolder, orientFolder, twistConstraint = self.createSplineMatrixSegment(splineObject,
                                                                                             segment,
                                                                                             splineMatrixFolder)
            if constraintFolder is None:
                continue

            constraintFolderArray.append(constraintFolder)
            orientFolderArray.append(orientFolder)
            twistConstraintArray.append(twistConstraint)

        for constraintFolder in constraintFolderArray:
            splineMatrixFolder.ConnectSrc(constraintFolder)

        for orientFolder in orientFolderArray:
            splineMatrixFolder.ConnectSrc(orientFolder)

        dummyOrient.FBDelete()

        dummyOrient = Constraint.CreateConstraint(Constraint.ROTATION)
        twistFolder = mobu.FBFolder('{0}_{1}'.format(splineObject.Name, 'twistConstraints1'), dummyOrient)

        for twistConstraint in twistConstraintArray:
            twistFolder.ConnectSrc(twistConstraint)

        dummyOrient.FBDelete()

        return {'constraintFolderArray':constraintFolderArray,
                'orientFolder':orientFolder,
                'splineMatrixFolder':splineMatrixFolder,
                'splineMatrixData':splineMatrixData,
                'splineSegment':splineSegment,
                'segmentArray':segmentArray,
                'root':splineMatrixData['knotRoot'],
                'twistFolder':twistFolder}

    def createAimLayer(self,
                       splineObject,
                       motionpathData,
                       jointObjectArray):
        splineSegment, segmentArray = self.extractSplineSegment(splineObject, 
                                                                motionpathData,
                                                                jointObjectArray)

        self.extractTargetNode(segmentArray)

    def extractSplineSegment(self,
                             splineObject,
                             motionpathData,
                             jointObjectArray):
        curveMaxRange = motionpathData['curveMaxRange']

        splineSegment = []
        for segmentIndex in range(curveMaxRange+1):
            splineSegment.append([])

        segmentArray = []
        for index, uValue in enumerate(motionpathData['uValueArray']):
            segment = curveSegment()

            segment.setData(uValue,
                            curveMaxRange,
                            jointObjectArray[index],
                            motionpathData['knotArray'][index],
                            motionpathData['ikControls'])

            segmentArray.append(segment)
            splineSegment[segment.startControlIndex].append(segment)

        return splineSegment, segmentArray

    def buildSegmentDictionnary(self, inputSplineSegment):
        ouputDict = collections.OrderedDict()
        for segment in inputSplineSegment:
            ouputDict[float(segment[0].segmentStartParameter)] = segment

        return ouputDict

    def assignTwistRange(self,
                         segmentStartParameter,
                         sourceData,
                         targetData,
                         curveMaxRange):
        bindData = targetData[int(segmentStartParameter)]
        twistWeight = (sourceData.uValue - segmentStartParameter)
        floorIndex = int(math.floor(twistWeight*10.0))
        nextIndex = floorIndex + 1

        lowerTwist = bindData[floorIndex]

        if nextIndex > 9:
            upperIndex = int(segmentStartParameter)+1
            if upperIndex > curveMaxRange:
                upperIndex = curveMaxRange

            upperTwist = targetData[upperIndex][0]
        else:
            if nextIndex > len(bindData) - 1:
                nextIndex = len(bindData) - 1
            upperTwist = bindData[nextIndex]

        return {'lowerTwist':lowerTwist.orientNode,
                'drivenNode':sourceData.knotNode,
                'upperTwist':upperTwist.orientNode,
                'twistWeight':twistWeight}

    def createBlendVector(self, rangeData):
        jointName = rangeData['drivenNode'].Name.replace('_splinePoint1', '')
        vectorPoint = self.createHelper(jointName,
                                        rangeData['lowerTwist'],
                                        'upVectorPoint')

        vectorPoint.Parent = None
        
        vectorOrient = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        vectorOrient.Name = '{}_upVectorOrient1'.format(rangeData['drivenNode'].Name)

        vectorOrient.ReferenceAdd(0, vectorPoint)
        vectorOrient.ReferenceAdd(1, rangeData['lowerTwist']) 
        vectorOrient.ReferenceAdd(1, rangeData['upperTwist'])

        upperWeight = rangeData['twistWeight']
        lowerWeight = 1.0 - rangeData['twistWeight']

        vectorOrient.PropertyList.Find('{}.Weight'.format(rangeData['lowerTwist'].Name)).Data = lowerWeight
        vectorOrient.PropertyList.Find('{}.Weight'.format(rangeData['upperTwist'].Name)).Data = upperWeight

        vectorOrient.Active = True

        return vectorPoint, vectorOrient

    def updateMotionPathUpVector(self, 
                                 rangeData,
                                 upVectorRoot,
                                 upVectorFolder):
        pathConstraint = rangeData['drivenNode'].PropertyList.Find('motionPathObject').GetSrc(0)
        vectorPoint, vectorOrient = self.createBlendVector(rangeData)

        pathConstraint.PropertyList.Find('Follow Path').Data = True
        pathConstraint.PropertyList.Find('Up Vector').Data = 2 # Y
        pathConstraint.PropertyList.Find('Front Vector').Data = 0 # X

        pathConstraint.PropertyList.Find('Front Vector').Data = 0 # X
        pathConstraint.PropertyList.Find('World Up Type').Data = 2 # Object Rotate
        linkProperty = pathConstraint.PropertyList.Find('World Up Object')

        pathConstraint.PropertyList.Find('Roll').Data = 0.0
        pathConstraint.PropertyList.Find('Pitch').Data = 0.0
        pathConstraint.PropertyList.Find('Yaw').Data = 0.0

        pathConstraint.PropertyList.Find('Translation Offset').Data = mobu.FBVector3d()

        vectorPoint.ConnectSrc(linkProperty)
        pathConstraint.Active = True
        

        vectorPoint.Parent = upVectorRoot
        upVectorFolder.ConnectSrc(vectorOrient)

        warpAttribute = pathConstraint.PropertyList.Find('Warp')
        warpAttribute.SetLocked = True
        return rangeData['drivenNode']

    def bindMotionPathToSplineMatrix(self,
                                     splineObject,
                                     motionpathData,
                                     splineMatrixLayer):
        splineSegmentSource, segmentArraySource = self.extractSplineSegment(splineObject, 
                                                                            motionpathData,
                                                                            motionpathData['knotArray'])

        splineSegmentTarget = splineMatrixLayer['splineSegment']
        segmentArrayTarget = splineMatrixLayer['segmentArray']

        splineSegmentSourceDict = self.buildSegmentDictionnary(splineSegmentSource)
        splineSegmentTargetDict = self.buildSegmentDictionnary(splineSegmentTarget)

        curveMaxRange = splineObject.PathKeyGetCount() -1
        upVectorRoot = mobu.FBModelNull('{0}_{1}Grp1'.format(splineObject.Name,
                                                             'upVector'))

        dummyOrient = Constraint.CreateConstraint(Constraint.ROTATION)
        upVectorFolder = mobu.FBFolder('{0}_{1}'.format(splineObject.Name, 'upVectorConstraints1'), dummyOrient)

        #Delete any key on pathConstraint warp properties
        mobu.FBSystem().CurrentTake.ClearAllProperties(False)

        drivenNodeArray = []
        for inputData in splineSegmentSourceDict.keys():
            segmentStartParameter = inputData

            for segment in splineSegmentSourceDict[inputData]:
                rangeData = self.assignTwistRange(segmentStartParameter,
                                                  segment,
                                                  splineSegmentTargetDict,
                                                  curveMaxRange)

                drivenNode = self.updateMotionPathUpVector(rangeData,
                                                           upVectorRoot,
                                                           upVectorFolder)

                drivenNodeArray.append(drivenNode)

        #self.toggleJointAxis(drivenNodeArray)
        dummyOrient.FBDelete()

        return {'drivenNodeArray':drivenNodeArray,
                'upVectorRoot':upVectorRoot,
                'upVectorFolder':upVectorFolder}

    def createTweakers(self,
                       jointObjectArray,
                       drivenNodeArray,
                       splineObject,
                       curveDeformer,
                       ikSplineSettings,
                       ikControlsData):
        tweakerList = []
        tweakerArray = []
        tweakerOffsets = []
        tweakerGroup = None

        self.RefreshScene()
        tweakerGroup = mobu.FBGroup('rope_tweakerControls')
        tweakerGroup.Show = True

        dummyOrient = Constraint.CreateConstraint(Constraint.ROTATION)
        bindFolder = mobu.FBFolder('{0}_{1}'.format(splineObject.Name, 'bindConstraints1'), dummyOrient)

        for jointIndex in range(jointObjectArray.count()):
            tweakerOffset = mobu.FBModelNull('{0}_offset'.format(jointObjectArray[jointIndex].Name))
            tweakerOffsetCtrl = self.ikSplineLayer.createControl('{0}_Tweaker'.format(jointObjectArray[jointIndex].Name),
                                                                 'Tweaker',
                                                                 ikSplineSettings.tweakerControlsSize )
            tweakerOffsetCtrl.Show = True
            tweakerOffsetCtrl.Parent = tweakerOffset
            tweakerList.append([tweakerOffset,tweakerOffsetCtrl])

            tweakerGroup.ConnectSrc(tweakerOffsetCtrl)
            parentConstraint = self.constraintUtils.constraintParts(tweakerOffset,
                                                                    drivenNodeArray[jointIndex])

            parentConstraint.Name = '{}_constraintBind1'.format(jointObjectArray[jointIndex].Name)
            bindFolder.ConnectSrc(parentConstraint)

            tweakerOffsets.append(tweakerList[jointIndex][0])
            tweakerArray.append(tweakerList[jointIndex][1])
            
        dummyOrient.FBDelete()
        self.RefreshScene()

        dummyOrient = Constraint.CreateConstraint(Constraint.ROTATION)
        attachFolder = mobu.FBFolder('{0}_{1}'.format(splineObject.Name, 'attachConstraints1'), dummyOrient)

        for jointIndex in range(jointObjectArray.count()):
            parentConstraint = self.constraintUtils.alignWithConstraint(jointObjectArray[jointIndex],
                                                                        tweakerList[jointIndex][1],
                                                                        preserveOffset=True)
            parentConstraint.Snap()
            parentConstraint.Active = True
            parentConstraint.Name = '{}_Attach1'.format(jointObjectArray[jointIndex].Name)
            attachFolder.ConnectSrc(parentConstraint)

        dummyOrient.FBDelete()

        return {'tweakerGroup':tweakerGroup,
                'tweakerList':tweakerList, 
                'tweakerOffsets':tweakerOffsets, 
                'attachFolder':attachFolder,
                'bindFolder':bindFolder,
                'tweakerArray':tweakerArray}

    def prepareIkRig(self,
                     splineObject,
                     curveDeformer,
                     tweakerGroup,
                     tweakerList,
                     tweakerOffsets,
                     anchorFolder,
                     ikControlsData,
                     ikSplineSettings,
                     rigRoot):
        #------------------------------------------------------------- Bundle rig Data here
        ikSplineStructureSettings = ikSettings.IkSplineStructureSettings()
        ikSplineStructureSettings.rigRoot = rigRoot

        ikSplineStructureSettings.splineObject  = splineObject
        ikSplineStructureSettings.curveDeformer = curveDeformer

        ikConstraint = self.constraintUtils.createIkConstraint(splineObject, None)
        ikConstraint.Name = 'ropeIk'
        ikSplineStructureSettings.ikConstraint = ikConstraint

        ikSplineStructureSettings.anchorFolder   = anchorFolder
        ikSplineStructureSettings.tweakerGroup = tweakerGroup

        ikSplineStructureSettings.tweakerList = tweakerList
        
        ikSplineStructureSettings.tweakerOffsetList  = tweakerOffsets

        ikSplineStructureSettings.ikControlsData  = ikControlsData

        ikSplineStructureSettings.ikSplineSettings = ikSplineSettings

        ikSplineStructureSettings.fkControls = ikSettings.FkControlSettings()

        ikSplineStructureSettings.constraintFolder = mobu.FBFolder('{0}_constraints1'.format(splineObject.Name),
                                                                   ikSplineStructureSettings.ikConstraint)

        return ikSplineStructureSettings

    def addRigElement(self,
                      ikSplineStructureSettings,
                      setupGroups,
                      setupConstraints):
        rigFn = IkSplineStructure()
        utilFn = ikUtils.JointChain()

        rigComponents = []
        for setupElement in setupGroups:
            setupHierarchy = list(utilFn.getJointChainFromRoot(setupElement.LongName))
            rigComponents.extend(setupHierarchy)

        for objectModel in rigComponents:
            rigFn.connectMetaData(ikSplineStructureSettings.splineIkRig,
                                  'bufferGroup',
                                  objectModel)

        constraintComponents = []
        for constraintGroup in setupConstraints:
            self.getFolderContent(constraintGroup, constraintComponents)

        constraintComponents.extend(setupConstraints)
        constraintComponents = list(set(constraintComponents))

        for constraint in constraintComponents:
            rigFn.connectMetaData(ikSplineStructureSettings.splineIkRig,
                                  'anchorConstraint',
                                  constraint) 
            '''
            if not isinstance(constraint, mobu.FBFolder):
                constraint.Lock = True
            '''

    def getFolderContent(self, inputFolder, folderData):
        for folderItem in inputFolder.Items:
            if isinstance(folderItem, mobu.FBFolder):
                self.getFolderContent(folderItem, folderData)
                folderData.append(folderItem)
            else:
                folderData.append(folderItem)

    def finalizeRig(self, 
                    ikSplineStructureSettings,
                    motionpathData,
                    splineMatrixData,
                    upVectorRoot,
                    upVectorFolder,
                    tweakerFolder,
                    attachFolder,
                    tweakerArray):

        setupGroups = [motionpathData['knotRoot'], 
                      splineMatrixData['root'],
                      upVectorRoot]

        for group in setupGroups:
            group.Parent = ikSplineStructureSettings.rigRoot

        storageFolder = ikSplineStructureSettings.constraintFolder 

        setupConstraints = [motionpathData['constraintFolder'],
                            splineMatrixData['orientFolder'],
                            splineMatrixData['splineMatrixFolder'],
                            splineMatrixData['splineMatrixData']['constraintFolder'],
                            upVectorFolder,
                            tweakerFolder,
                            splineMatrixData['twistFolder'],
                            attachFolder]

        for folder in setupConstraints:
            storageFolder.ConnectSrc(folder)

        self.addRigElement(ikSplineStructureSettings,
                           setupGroups,
                           setupConstraints)

        controls = list(ikSplineStructureSettings.ikControlsData.ikControls)
        tweakerArray
        utilFn = ikUtils.JointChain()

        rigComponents = []
        rigComponents = list(utilFn.getJointChainFromRoot(ikSplineStructureSettings.splineIkRig.LongName))
        rigComponents = list(set(rigComponents) - set(controls))
        rigComponents = list(set(rigComponents) - set(tweakerArray))

        for model in rigComponents:
            model.Pickable = False
            model.Transformable = False
            
            translateHandle = model.PropertyList.Find('Translation (Lcl)')
            translateHandle.SetLocked = True
            rotationHandle = model.PropertyList.Find('Rotation (Lcl)')
            rotationHandle.SetLocked = True
            
            for channelIndex in range(3):
                translateHandle.SetMemberLocked(channelIndex, True)
                rotationHandle.SetMemberLocked(channelIndex, True)

    def buildRig(self,
                 jointObjectArray,
                 ikSplineSettings,
                 rigRoot=None):
        continueRigBuilding = self.validateRigSettings(jointObjectArray)

        if continueRigBuilding is False:
            return

        curveName = name=str(ikSplineSettings.ikPathName)
        controlerCount = ikSplineSettings.numberOfCurveController
        splineObject = self.bindDataUtils.createPathFromJoints(jointObjectArray,
                                                               curveName,
                                                               numberOfCurveController=controlerCount)

        self.RefreshScene()

        #----------------------------------------------------------------------Build ikControls
        ikControlsData = self.ikSplineLayer.createIkRig(splineObject,
                                                        jointObjectArray,
                                                        ikSplineSettings)
        ikControls = list(ikControlsData.ikControls)
        self.RefreshScene()

        #----------------------------------------------------------------------Attach join to curve
        motionpathData = self.createMotionpathLayer(splineObject,
                                                    jointObjectArray,
                                                    ikControls)

        #----------------------------------------------------------------------Deform Curve
        curveDeformer = self.ikSplineLayer.constrainSplineToControls(splineObject,
                                                                     ikControls,
                                                                     bindMethod='knotTranslation')

        #----------------------------------------------------------------------Create spline matrix
        splineMatrixLayer = self.createSplineMatrixLayer(splineObject,
                                                         jointObjectArray,
                                                         ikControls)

        #-------------------------------------------Use spline matrix with joint motionpath constraints
        bindData = self.bindMotionPathToSplineMatrix(splineObject,
                                                     motionpathData,
                                                     splineMatrixLayer)

        #----------------------------------------------------------------------Create Tweakers
        tweakerData = self.createTweakers(jointObjectArray, 
                                          bindData['drivenNodeArray'],
                                          splineObject,
                                          curveDeformer,
                                          ikSplineSettings,
                                          ikControlsData)

        #----------------------------------------------------------------------Prepare metadata
        ikSplineStructureSettings = self.prepareIkRig(splineObject,
                                                      curveDeformer,
                                                      tweakerData['tweakerGroup'],
                                                      tweakerData['tweakerList'],
                                                      tweakerData['tweakerOffsets'],
                                                      tweakerData['attachFolder'],
                                                      ikControlsData,
                                                      ikSplineSettings,
                                                      rigRoot)

        ikSplineStructureSettings.jointObjectArray = jointObjectArray

        #-----------------------------------------------------Parent and organize Setup elements
        IkSplineStructure().build(splineObject,
                                  ikSplineSettings,
                                  ikSplineStructureSettings)

        #--------------------------------------Group/Set setup here Controls / Plot Group / Geo
        IkSplineStructure().finalize(ikSplineSettings,
                                     ikSplineStructureSettings)

        for ikControl in ikControls:
            ikControl.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire

        self.finalizeRig(ikSplineStructureSettings,
                         motionpathData,
                         splineMatrixLayer,
                         bindData['upVectorRoot'],
                         bindData['upVectorFolder'],
                         tweakerData['bindFolder'],
                         tweakerData['attachFolder'],
                         tweakerData['tweakerArray'])

        return ikSplineStructureSettings.splineIkRig
