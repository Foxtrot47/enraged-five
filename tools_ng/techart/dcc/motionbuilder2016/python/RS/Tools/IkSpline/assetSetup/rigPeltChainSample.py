from RS.Tools.IkSpline.assetSetup  import ikFkParallelChainBuilder
reload(ikFkParallelChainBuilder)

import pyfbsdk as mobu
import os
                                
chainUtils =  ikFkParallelChainBuilder.Build()

rigStorageNode = 'Dummy01'
numberOfCurveController = 8

#upReferenceVector will be perpendicular vector to your chain direction ( so your bison was Z axis the deer its X axis)
upReferenceVector=mobu.FBVector3d(10.0, 0.0, 0.0)


chainRootName = 'PeltA_rib06_0'

#chainRigPrefix is roughly the same as the root chain : try to remove extra part like Null: 
chainRigPrefix = 'PeltA_rib06_0'

chainUtils.createFromPath(chainRootName,
                          chainRigPrefix,
                          numberOfCurveController,
                          rigStorageNode,
                          upReferenceVector=upReferenceVector)