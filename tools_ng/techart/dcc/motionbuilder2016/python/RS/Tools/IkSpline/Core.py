"""
## Description : This module will create a splineIK rig.
## Summary    : Ik Spline in MotionBuilder.
## bugTracker : 2515761.
"""
import copy
import math
import json

import pyfbsdk as mobu

from RS.Utils.Scene import Model
from RS.Utils.Scene import Constraint
from RS.Utils import Scene
from RS import Globals
from RS.Core.Animation import Lib

from RS.Tools.IkSpline import Utils as ikUtils
from RS.Tools.IkSpline import Settings as ikSettings
from RS.Tools.IkSpline import Rigging as ikRigging


MAX_TWIST_OBJECT = 4000


class Setup(object):
    """
        Class to bind a joint hierarchy to a custom
        ikSpline constraint in motionbuilder
    """
    def __init__(self):
        self.ikSplineSettings = ikSettings.IkSplineSettings()
        self.jointChain = ikUtils.JointChain()
        self.animUtils = ikUtils.AnimUtils()
        self.constraintUtils = ikUtils.ConstraintUtils()
        self.ikSplineLayer = ikRigging.IkSplineLayer()
        self.bindDataUtils = ikUtils.BindDataUtils()
        self.ikSplineStructure = ikRigging.IkSplineStructure()
        self.gameBonesSettings = ikSettings.GameBonesSettings()

    def snapToMotionPath(self, rigRootName, inputCurveName):
        splineIkRig = mobu.FBFindModelByLabelName(rigRootName)
        if splineIkRig is None:
            return

        motionPathObject = mobu.FBFindModelByLabelName(rigRootName)
        if motionPathObject is None:
            return 

    def prepareMotionPathSettings(self,
                                  rigStructureSettings,
                                  aimToNextJointInMotionPath,
                                  rigStorage):
        #motionPathSettings = ikSettings.IkSplineSettings()
        motionPathSettings = self.createIkMotionPathRigOptions(aimToNextJointInRig=aimToNextJointInMotionPath)

        motionPathSettings.ikPathName = 'motionPath1'
        motionPathSettings.ikControlsSize = rigStructureSettings.ikSplineSettings.ikControlsSize * 1.2
        motionPathSettings.createIntermediateGroup = True
        motionPathSettings.isMotionPath = True

        motionPathSettings.createTweaker=False
        motionPathSettings.reverseList = True

        motionPathSettings.hasSnakeCompensationLayer = True
        motionPathSettings.rigStorage = rigStorage

        return motionPathSettings

    def createIkMotionPathRigOptions(self,
                                     rigAnchor=None,
                                     aimToNextJointInRig=False):
        ikSplineSettings = ikSettings.IkSplineSettings()

        ikSplineSettings.hasSnakeCompensationLayer = False
        ikSplineSettings.createTweaker = False
        ikSplineSettings.tweakerControlsSize = 50
        ikSplineSettings.numberOfCurveController = 32
        ikSplineSettings.rigAnchor = rigAnchor

        ikSplineSettings.aimToNextJoint = aimToNextJointInRig 
        ikSplineSettings.createFkControls = False 
        ikSplineSettings.maintainChainLength = True 
        ikSplineSettings.bindMethod = 'knotTranslation' # other options 'knotTranslation'/'cluster'
        ikSplineSettings.excludeThreshold = 0.001
        ikSplineSettings.createIntermediateGroup = False

        return ikSplineSettings

    def buildIkMotionPathLayeredRig(self,
                                    ikRigDivision,
                                    motionPathDivisions,
                                    chainRoot=None,
                                    rigAnchor=None,
                                    rigStorage=None,
                                    ikRigMaintainChainLength=True,
                                    createTweaker=False,
                                    debugMode=False):
        #-----------------------------------------------------------------------------------------------IK RIG ONLY
        rigSettings = self.createIkMotionPathRigOptions(rigAnchor=rigAnchor,
                                                        aimToNextJointInRig=True)

        rigSettings.hideDrivenJoints = True
        rigSettings.numberOfCurveController = ikRigDivision
        rigSettings.rigStorage = rigStorage
        rigSettings.createTweaker = createTweaker
        rigSettings.maintainChainLength = ikRigMaintainChainLength

        self.ikSplineSettings = rigSettings

        jointList = ikUtils.JointChain().getJointChainFromRoot(chainRoot) 
        rigStructureSettings = self.buildRig(None, jointList, debugMode=True)
        if debugMode is True:
            print 'Outout build', type(rigStructureSettings) 

        motionPathSettings = self.prepareMotionPathSettings(rigStructureSettings,
                                                            False,
                                                            rigStorage)
        #-------------------------------------------------------------------------------------------MOTIONPATH LAYER
        motionPathSettings.rigStorage = rigStorage
        motionPathSettings.rigAnchor = rigAnchor
        
        motionPathSettings.reverseList = False
        motionPathSettings.numberOfCurveController = motionPathDivisions

        self.ikSplineSettings = motionPathSettings

        if debugMode is True:
            print 'Before mopath', type(rigStructureSettings)

        motionData = self.createMotionPath(rigStructureSettings.ikControlsData.ikControls,
                                           motionPathDivisions,
                                           ikRigRootToConnect=rigStructureSettings.splineIkRig.LongName,
                                           debugMode=debugMode)

    def createPathFromJoints(self,
                             inputJointObjectArray,
                             name='splineIkPath1',
                             numberOfCurveController=3,
                             deleteJointPath=True,
                             excludeThreshold=0.001,
                             debugDistance=False,
                             skipCurveRebuilding=False,
                             reverseCurve=False):
        """
            Method create a curve path for a list of joints
            Args:
                jointObjectArray (modelList): the object list we want
                to drive
                
                name                    (str): Name of the computed curve
                numberOfCurveController (int): parameter to resample the path
                
            return type FBModelPath3d
        """
        if not isinstance( inputJointObjectArray,mobu.FBModelList):
            raise ValueError('Invalid jointObjectArray  : you must provide a FBModelList' )
            return None

        jointObjectArray = [joint for joint in inputJointObjectArray]

        if reverseCurve is True:
            jointObjectArray.reverse()

        if jointObjectArray[0].Name.startswith('MH_'):
            sourceBoneName = jointObjectArray[0].LongName.replace('MH_', 'SKEL_')

            sourceBone = Scene.FindModelByName(sourceBoneName, 
                                               includeNamespace=True)

            if sourceBone:
                targetMatrix = mobu.FBMatrix()
                sourceBone.GetMatrix(targetMatrix)
                jointObjectArray[0].SetMatrix(targetMatrix)

        curveName = '{0}_HighRes'.format(str(name))
        controlPath_HIGH = mobu.FBModelPath3D(curveName)
        controlPath_HIGH.Show = True
        controlPath_HIGH.Visible = True

        jointMatrix = mobu.FBMatrix()
        CV_position = mobu.FBVector4d()

        position = mobu.FBVector3d()
        positionOld = mobu.FBVector3d()

        #clean mobu embarrsing default shape with two points...
        controlPath_HIGH.PathKeyClear()

        self.RefreshScene()

        curveMatrix = mobu.FBMatrix()
        curveSpace = controlPath_HIGH.GetMatrix(curveMatrix)

        controlPath_HIGH.SetMatrix(curveMatrix)
        positionInCurveSpace = mobu.FBVector4d()
        curveMatrix.Inverse()

        #Create a CV_Point for each joint
        for jointIndex, joint in enumerate(jointObjectArray):
            jointObjectArray[jointIndex].GetVector(position)
            CV_position[0] = position[0]
            CV_position[1] = position[1]
            CV_position[2] = position[2]

            mobu.FBVectorMatrixMult(positionInCurveSpace,
                                    curveMatrix,
                                    CV_position)


            if jointIndex == 0:
                controlPath_HIGH.PathKeyStartAdd(positionInCurveSpace)
            else:
                controlPath_HIGH.PathKeyEndAdd(positionInCurveSpace)

        self.RefreshScene()

        if numberOfCurveController<2:
            numberOfCurveController=2

        if skipCurveRebuilding is True:
            numberOfCurveController = jointObjectArray.count()

        uRatio = 100.0  / float( numberOfCurveController-1) 

        controlPath = mobu.FBModelPath3D(name)
        controlPath.Show = True
        controlPath.Visible = True

        self.RefreshScene()
        #clean mobu embarrsing default shape with two points...
        controlPath.PathKeyClear()

        #OrienCurve here
        #@---
        FirstCv = mobu.FBVector3d()
        SecondCv = mobu.FBVector3d()

        self.RefreshScene()
        jointObjectArray[0].GetVector(FirstCv)
        jointObjectArray[1].GetVector(SecondCv)

        upPos = mobu.FBModelNull('upPos')
        upPos.Show = True

        basePos = mobu.FBModelNull('basePos')
        basePos.Show = True

        targetPos = mobu.FBModelNull('targetPos')
        targetPos.Show = True

        upPos.Parent = basePos
        upPos.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0,10.0,0)
        basePos.SetVector(FirstCv)
        targetPos.SetVector(SecondCv)
        self.RefreshScene()
        
        aimConstraint = Constraint.CreateConstraint(Constraint.AIM)

        aimConstraint.ReferenceAdd (0, controlPath)
        aimConstraint.ReferenceAdd (1, targetPos) 
        aimConstraint.ReferenceAdd (2, upPos) 
        
        aimConstraint.PropertyList.Find('World Up Type').Data = 1

        posConstraint = Constraint.CreateConstraint(Constraint.POSITION)
        posConstraint.ReferenceAdd (0, controlPath)
        posConstraint.ReferenceAdd (1, basePos) 
        
        self.RefreshScene()
        posConstraint.Active = True
        aimConstraint.Active = True

        self.RefreshScene()
        self.constraintUtils.bakeConstraintValue(posConstraint)
        self.constraintUtils.bakeConstraintValue(aimConstraint)

        self.RefreshScene()
        curveMatrix = mobu.FBMatrix()
        curveSpace =  controlPath.GetMatrix(curveMatrix)

        loc = mobu.FBModelNull('locator1')
        loc.Show = True

        self.RefreshScene()
        ikConstraint = self.constraintUtils.createIkConstraint(controlPath_HIGH,
                                                               loc)

        ikConstraint.Active  = True     
        uValueHandle = ikConstraint.PropertyList.Find('uValue1') 

        positionInCurveSpace = mobu.FBVector4d()
        curveMatrix.Inverse()
        #Now resample the curve to the target numberOfCurveController
        for jointIndex  in range(numberOfCurveController):
            kPercentSource =  jointIndex*uRatio
            kPercent = controlPath_HIGH.ConvertTotalPercentToSegmentPercent(kPercentSource)

            if skipCurveRebuilding is True:
                kPercent = float(jointIndex)

            uValueHandle.Data = kPercent*100.0
            mobu.FBSystem().Scene.Evaluate()

            loc.GetVector(position)

            CV_position[0] = position[0]
            CV_position[1] = position[1]
            CV_position[2] = position[2]

            mobu.FBVectorMatrixMult(positionInCurveSpace, curveMatrix, CV_position)
            if jointIndex == 0:
                controlPath.PathKeyStartAdd(positionInCurveSpace)
            else:
                controlPath.PathKeyEndAdd(positionInCurveSpace)

        #Clean up unused elements
        ikConstraint.Active = False 
        ikConstraint.FBDelete()

        if deleteJointPath is True:
            controlPath_HIGH.FBDelete()

        for model in [basePos, targetPos, loc, upPos]:
            model.FBDelete()

        #Adjust collor a selectionMode
        self.ikSplineLayer.colorizeObject(controlPath,(255,255,0))
        controlPath.PropertyList.Find('Enable Selection').Data = 0
        controlPath.PropertyList.Find('Enable Transformation').Data = 0

        self.RefreshScene()
        return controlPath

    def attachToIkSpline(self,
                         splineObject,
                         jointObjectArray,
                         ikControls,
                         debugMode=False):
        """
            This method exposes chain length data 
            Args:
                splineObject (FBModelPath3d): CurveShape use by
                the ikSpline constraint

                jointObjectArray (modelList): the object list
                containing our joint chain
                
            return ikConstraint (constraint)
        """   
        if debugMode is True:
            print ikControls

        #Extract boneLengths
        segmentData = self.bindDataUtils.collectChainSegments(jointObjectArray)

        #Create motionPath
        ikConstraint = self.constraintUtils.createIkConstraint(splineObject, None)
        ikConstraint.Active = False
        self.RefreshScene()

        ikConstraint.PropertyList.Find("Aim To Next Joint").Data = self.ikSplineSettings.aimToNextJoint
        ikConstraint.Name = splineObject.Name + '_constraint'
        ikConstraint.PropertyList.Find('Use Percentage').Data = False
        ikConstraint.PropertyList.Find("Reverse Chain").Data = self.ikSplineSettings.reverseChain 

        anchorFolder = None
        anchorList = []
        bufferList= []

        tweakerList = []
        tweakerArray = []
        tweakerGroup = None

        #-------------------------------------------Assign driven object to path
        if self.ikSplineSettings.createTweaker is False:
            if self.ikSplineSettings.createIntermediateGroup is False:
                for jointIndex in range(jointObjectArray.count()):
                    ikConstraint.ReferenceAdd(1,jointObjectArray[jointIndex])

            if self.ikSplineSettings.createIntermediateGroup is True:
                targetParentArray = []
                constraintArray = []

                for jointIndex in range(jointObjectArray.count()):
                    targetParent = mobu.FBModelNull('{0}_Buffer'.format(jointObjectArray[jointIndex].Name))
                    targetParent.Show = True
                    targetParent.Visible = False

                    ikConstraint.ReferenceAdd(1,targetParent)
                    targetParentArray.append(targetParent)
                    bufferList.append(targetParent)
                    
                self.RefreshScene()

                for jointIndex in range(jointObjectArray.count()):
                    parentConstraint = self.constraintUtils.alignWithConstraint(jointObjectArray[jointIndex],
                                                                                targetParentArray[jointIndex],
                                                                                preserveOffset=True)
                    
                    parentConstraint.Name = '{0}_attach1'.format(jointObjectArray[jointIndex].LongName)
                    anchorList.append(parentConstraint)

                    if jointIndex == 0:
                        anchorFolder = mobu.FBFolder('{0}_Anchors1'.format(ikConstraint.Name),parentConstraint)
                        anchorFolder.Show = True
                    else:
                        anchorFolder.Items.append(parentConstraint)

                self.RefreshScene()

        if self.ikSplineSettings.createTweaker is True:
            tweakerGroup = mobu.FBGroup('splineIk_tweakerControls')
            tweakerGroup.Show = True

            for jointIndex in range(jointObjectArray.count()):
                tweakerOffset = mobu.FBModelNull('{0}_offset'.format(jointObjectArray[jointIndex].Name))
                tweakerOffsetCtrl = self.ikSplineLayer.createControl('{0}_Tweaker'.format(jointObjectArray[jointIndex].Name),
                                                                     'Tweaker',
                                                                     self.ikSplineSettings.tweakerControlsSize )
                tweakerOffsetCtrl.Show = True
                tweakerOffsetCtrl.Parent = tweakerOffset
                tweakerList.append([tweakerOffset,tweakerOffsetCtrl])

                tweakerGroup.ConnectSrc(tweakerOffsetCtrl)

            self.RefreshScene()
            for jointIndex in range(jointObjectArray.count()):
                ikConstraint.ReferenceAdd(1,tweakerList[jointIndex][0])

        self.RefreshScene()
        #-----------------------------------------------------------------Write chainSegments
        for chainIndex in range(len(segmentData['chainSegments'])):
            chainLengthHandle = ikConstraint.PropertyList.Find('chainLength{0}'.format((chainIndex+1)))
            if segmentData['chainSegments'][chainIndex] < 0.01:
                continue

            chainLengthHandle.Data = segmentData['chainSegments'][chainIndex]

        self.RefreshScene()

        bindData = self.bindDataUtils.extractJointOffsets(splineObject,
                                                          jointObjectArray,
                                                          segmentData['chainSegments'],
                                                          self.ikSplineSettings.aimToNextJoint,
                                                          reverseChain=self.ikSplineSettings.reverseChain)

        #-----------------------------------------------------------------Write Offsets
        if self.ikSplineSettings.createIntermediateGroup is False:
            for jointIndex in range(jointObjectArray.count()):
                if self.ikSplineSettings.applyPositionOffset == True:
                    posOffsetHandle = ikConstraint.PropertyList.Find('positionOffset{0}'.format((jointIndex+1)))
                    posOffsetHandle.Data = mobu.FBVector3d(bindData['positionOffset'][jointIndex])

                if self.ikSplineSettings.applyRotationOffset == True:
                    rotOffsetHandle = ikConstraint.PropertyList.Find('rotationOffset{0}'.format((jointIndex+1)))
                    rotOffsetHandle.Data = bindData['rotationOffset'][jointIndex]

        self.RefreshScene()
        #-----------------------------------------------------------------Write uParameters (uValue in constraint is pathValue * 100.0)
        for chainIndex in range(len(bindData['uValueArray'])):
            chainValueHandle = ikConstraint.PropertyList.Find('uValue{0}'.format(chainIndex+1))
            chainValueHandle.Data = bindData['uValueArray'][chainIndex]

        self.RefreshScene()

        #Now activate chain length preservation
        ikConstraint.PropertyList.Find("applyIK_Chain_rules").Data = self.ikSplineSettings.maintainChainLength

        bones_worldMatrix = []
        localMatrix = mobu.FBMatrix()
        jointMatrix = mobu.FBMatrix()
 
        for joint in jointObjectArray:
            joint.GetMatrix(jointMatrix)
            bones_worldMatrix.append(jointMatrix)

        #---------------------------------------------------------Connect IK to the spline shape
        curveDeformer = self.ikSplineLayer.constrainSplineToControls(splineObject,
                                                                     ikControls,
                                                                     bindMethod=self.ikSplineSettings.bindMethod)

        #----------------------------------------------Connect TwistDrivers into ikSpline solver              
        self.ikSplineLayer.connectTwistPropagation(splineObject,
                                                   ikConstraint,
                                                   ikControls)

        #--------------------------------------------------------------------------Enable constraint Here
        ikConstraint.Active = True

        #-------------------------------------------Lock driven Joints
        for joint in jointObjectArray :
            joint.PropertyList.Find('Enable Selection').Data = not self.ikSplineSettings.lockSelection 

        #-------------------------------------------Hide driven Joints
        for joint in jointObjectArray :
            joint.Visibility = not self.ikSplineSettings.hideDrivenJoints

        self.RefreshScene()
        #---------------------------------------Additional Data when user want to create a tweak Layer
        offsetList = []
        tweakerOffsets = []
        aimTargets = []
        aimObjects = []
        if self.ikSplineSettings.createTweaker is True:
            for jointIndex, joint in enumerate(jointObjectArray):
                nullConstraint = self.constraintUtils.alignWithConstraint(tweakerList[jointIndex][1], tweakerList[jointIndex][1].Parent)
                self.RefreshScene()
                self.constraintUtils.bakeConstraintValue(nullConstraint)

                orientNull = mobu.FBModelNull('{0}_orient'.format(joint.Name))
                orientNull.Show = True
                
                #Parent *_orient under *_offset
                orientNull.Parent = tweakerList[jointIndex][0]
                orientNullConstraint = self.constraintUtils.alignWithConstraint(orientNull, tweakerList[jointIndex][0])
                self.RefreshScene()
                self.constraintUtils.bakeConstraintValue(orientNullConstraint)

                constraint = Constraint.CreateConstraint(Constraint.POSITION)
                constraint.ReferenceAdd(0, orientNull)
                constraint.ReferenceAdd(1, joint)
                constraint.Active = True
                self.RefreshScene()
                self.constraintUtils.bakeConstraintValue(constraint)

                orientNullPosition = mobu.FBVector3d()
                joint.GetVector(orientNullPosition)
                orientNull.SetVector(orientNullPosition)

                constraint = Constraint.CreateConstraint(Constraint.POSITION)
                constraint.ReferenceAdd(0, tweakerList[jointIndex][1])
                constraint.ReferenceAdd(1, joint)
                constraint.Active = True
                self.RefreshScene()
                self.constraintUtils.bakeConstraintValue(constraint)

                #Parent *_TweakerCtrontrol under *_orient under  
                tweakerList[jointIndex][1].Parent = orientNull

                offsetList.append(orientNull)

                tweakerOffsets.append(tweakerList[jointIndex][0])
                tweakerArray.append(tweakerList[jointIndex][1])

            jointLimit = len(jointObjectArray) - 1
            for jointIndex, joint in enumerate(jointObjectArray):
                reverseAim = False
                aimIndex = jointIndex + 1

                if aimIndex > jointLimit :
                    aimIndex = jointLimit - 1
                    reverseAim = True

                baseControl = tweakerArray[jointIndex] 
                aimControl  = tweakerArray[aimIndex] 

                jumpToNextTarget = self.constraintUtils.validateAimTarget(baseControl, aimControl)
                if jumpToNextTarget is True:
                    aimIndex += 1
                    aimControl  = tweakerArray[aimIndex] 

                bindObject, targetConstraint , anchor = self.constraintUtils.layoutTweakerAimRig(joint, baseControl, aimControl, reverseAim)
                aimObjects.append(bindObject)
                aimTargets.append(targetConstraint)

                anchor.Name = "{0}_attach1".format(joint.LongName)
                anchorList.append(anchor)

                if jointIndex == 0:
                    anchorFolder = mobu.FBFolder('{0}_ikSpline_Anchors1'.format(splineObject.Name), anchor)
                else:
                    anchorFolder.Items.append(anchor)

                anchorFolder.Items.append(targetConstraint)

        #---------------------------------------------------------------------------Snap/Activate tweakers
        for anchor in anchorList:
            anchor.Snap()
            anchor.Active = True
            anchor.Lock = True

        anchorList.extend(aimTargets)
        anchorList.extend(aimObjects)

        self.RefreshScene()
        for tweakObject in aimObjects:
            tweakObject.Show = False

        #------------------------------------------------------------- Bundle rig Data here
        ikSplineStructureSettings = ikSettings.IkSplineStructureSettings()

        ikSplineStructureSettings.splineObject  = splineObject
        ikSplineStructureSettings.curveDeformer = curveDeformer
        ikSplineStructureSettings.ikConstraint = ikConstraint

        ikSplineStructureSettings.anchorFolder   = anchorFolder
        ikSplineStructureSettings.tweakerGroup = tweakerGroup

        ikSplineStructureSettings.jointObjectArray = jointObjectArray
        ikSplineStructureSettings.tweakerOffsetList  = tweakerOffsets
        ikSplineStructureSettings.bufferList = bufferList
        ikSplineStructureSettings.anchorList = anchorList

        ikSplineStructureSettings.tweakerList = tweakerList
        ikSplineStructureSettings.offsetList = offsetList
        
        
        if self.ikSplineSettings.isMotionPath is False or self.ikSplineSettings.isMotionPath is None :
            if self.ikSplineSettings.createIntermediateGroup :
                ikSplineStructureSettings.controlsToDriveByMotionPath = list(bufferList)
            else:
                ikSplineStructureSettings.controlsToDriveByMotionPath = list(ikControls)
            
        return  ikSplineStructureSettings

    def RefreshScene(self):
        """
            Force Evaluation of current scene . 
        """
        mobu.FBSystem().Scene.Evaluate()
        mobu.FBApplication().UpdateAllWidgets()
        mobu.FBApplication().FlushEventQueue()

    def resetRig(self,splineIkRig):
        metaRoot = splineIkRig.PropertyList.Find('ikSplineMetaRoot')
        if metaRoot is None:
            return

        constraintLink = splineIkRig.PropertyList.Find('ikConstraint')
        if constraintLink.GetSrcCount() > 0:
            inputRigIkConstraint = constraintLink.GetSrc(0)

            inputRigIkConstraint.Weight = 0.0
            self.RefreshScene()

            inputRigIkConstraint.Active = False
            self.RefreshScene()

        groupList = ['ikConstraint',
                     'splineMatrix',
                     'snakeCompensationGroup',
                     'tweakerGroup',
                     'bufferGroup',
                     'FkControlGroup',
                     'IkControlGroup',
                     'SplineCurve',
                     'anchorConstraint',
                     'folder',
                     'gameJointConstraints',
                     'characterExtensionGroup']

        destroyArray = []
        for group in groupList:
            metaData = splineIkRig.PropertyList.Find(group)

            if metaData is None:
                continue

            groupMembersCount = metaData.GetSrcCount()
            if groupMembersCount == 0:
                continue

            for memberIndex in range(groupMembersCount):
                member = metaData.GetSrc(memberIndex)
                if member is None:
                    continue

                destroyArray.append(member)

        for objectModel in destroyArray:
            objectModel.FBDelete()


        mobu.FBSystem().Scene.Evaluate()

    def rebuildLayeredRig(self, 
                          rigRootName,
                          deleteRig=True,
                          controllerCount=-1,
                          motionPathControllerCount=-1):
        inputSplineIkRig = mobu.FBFindModelByLabelName(rigRootName)
        if inputSplineIkRig is None:
            return

        metaRootProperty = inputSplineIkRig.PropertyList.Find('ikSplineMetaRoot')
        if metaRootProperty is None:
            return

        layeredMotionPathProperty = inputSplineIkRig.PropertyList.Find('layeredMotionPathGroup')
        rigMode = layeredMotionPathProperty.GetSrcCount()

        if rigMode == 0:
            return

        rigLink = layeredMotionPathProperty.GetSrc(0)

        settingAttribute = inputSplineIkRig.PropertyList.Find('rigSettings')
        rigSettings = json.loads(str(settingAttribute.Data))

        ikRig = None
        motionPathRig = None

        if rigSettings['isMotionPath'] is True:
            ikRig = rigLink
            motionPathRig = inputSplineIkRig
        else:
            ikRig = inputSplineIkRig
            motionPathRig = rigLink

        if deleteRig is True:
            #Notice the inverse order
            self.resetRig(motionPathRig)
            self.resetRig(ikRig)

        settingAttribute = ikRig.PropertyList.Find('rigSettings')
        rigSettings = json.loads(str(settingAttribute.Data))

        for attribute in rigSettings:
            if isinstance(rigSettings[attribute] , unicode) is True:
                setattr(self.ikSplineSettings, attribute, str(rigSettings[attribute]))
            else:
                setattr(self.ikSplineSettings, attribute, rigSettings[attribute])

        if controllerCount > -1:
            self.ikSplineSettings.numberOfCurveController = controllerCount

        jointObjectArray = mobu.FBModelList()
        jointProperty = ikRig.PropertyList.Find('joints')
    
        groupMembersCount = jointProperty.GetSrcCount()

        for memberIndex in range(groupMembersCount):
            member = jointProperty.GetSrc(memberIndex)
            if member is None:
                continue

            jointObjectArray.append(member)
        hasSnakeCompensationLayer = self.ikSplineSettings.hasSnakeCompensationLayer 

        rigStructureSettings = self.buildRig(None, jointObjectArray, rigRoot=ikRig)

        self.RefreshScene()

        aimToNextJointInMotionPath = True
        motionPathSettings = self.prepareMotionPathSettings(rigStructureSettings,
                                                            aimToNextJointInMotionPath,
                                                            'A_C_Snakecoral_1')

        settingAttribute = motionPathRig.PropertyList.Find('rigSettings')
        rigSettings = json.loads(str(settingAttribute.Data))

        for attribute in rigSettings:
            if isinstance(rigSettings[attribute] , unicode) is True:
                setattr(motionPathSettings, attribute, str(rigSettings[attribute]))
            else:
                setattr(motionPathSettings, attribute, rigSettings[attribute])

        if motionPathControllerCount > -1:
            motionPathSettings.numberOfCurveController = motionPathControllerCount

        self.ikSplineSettings =  motionPathSettings
        motionData = self.createMotionPath(rigStructureSettings.ikControlsData.ikControls,
                                           motionPathControllerCount,
                                           ikRigRootToConnect=None,
                                           rigRoot=motionPathRig)

        self.RefreshScene()

        if hasSnakeCompensationLayer is True:
            ikControls = rigStructureSettings.ikControlsData.ikControls
            ikControlsName = []

            for control in ikControls:
                ikControlsName.append(control.LongName)
                
            rigStructure = rigStructureSettings.splineIkRig.LongName
            constraintFolder = rigStructureSettings.constraintFolder.LongName

            splineData = [ikControlsName, 
                          rigStructure, 
                          constraintFolder]

            ikRigging.SetupCoralSnake().correctSpineOrients(splineData, rigStructureSettings)

    def rebuildRig(self, 
                   rigRootName,
                   deleteRig=True,
                   overrideControllerCount=-1):
        splineIkRig = mobu.FBFindModelByLabelName(rigRootName)
        if splineIkRig is None:
            return

        metaRootProperty = splineIkRig.PropertyList.Find('ikSplineMetaRoot')
        if metaRootProperty is None:
            return

        self.RefreshScene()

        jointObjectArray = mobu.FBModelList()
        jointProperty = splineIkRig.PropertyList.Find('joints')
    
        groupMembersCount = jointProperty.GetSrcCount()

        for memberIndex in range(groupMembersCount):
            member = jointProperty.GetSrc(memberIndex)
            if member is None:
                continue

            jointObjectArray.append(member)

        settingAttribute = splineIkRig.PropertyList.Find('rigSettings')
        rigSettings = json.loads(str(settingAttribute.Data))

        for attribute in rigSettings:
            if isinstance(rigSettings[attribute] , unicode) is True:
                setattr(self.ikSplineSettings, attribute, str(rigSettings[attribute]))
            else:
                setattr(self.ikSplineSettings, attribute, rigSettings[attribute])

        if overrideControllerCount > -1:
            self.ikSplineSettings.numberOfCurveController = overrideControllerCount

        if deleteRig is True:
            self.resetRig(splineIkRig)

        #take into account the input character for snake
        characterGroupProperty = splineIkRig.PropertyList.Find('characterGroup')
        characterGroupCount = characterGroupProperty.GetSrcCount()

        if characterGroupCount > 0:
            snakeCharacter = characterGroupProperty.GetSrc(0)
            self.ikSplineSettings.addExtensionToCharacter = snakeCharacter.FullName

        if self.ikSplineSettings.isSimplifiedRopeRig is True:
            ropeFn = ikRigging.SetupRope()
            ropeFn.buildRig(jointObjectArray,
                            self.ikSplineSettings)
                        
            self.ikSplineLayer.applyNamespace(splineIkRig)

            return

        rigStructureSettings = self.buildRig(None, jointObjectArray, rigRoot=splineIkRig, debugMode=False)

        if self.ikSplineSettings.hasSnakeCompensationLayer is False:
            ikControls = rigStructureSettings.ikControlsData.ikControls
            ikControlsName = []

            for control in ikControls:
                ikControlsName.append(control.LongName)

            rigStructure = rigStructureSettings.splineIkRig.LongName
            constraintFolder = rigStructureSettings.constraintFolder.LongName

            splineData = [ikControlsName, 
                          rigStructure, 
                          constraintFolder]

            ikRigging.SetupCoralSnake().correctSpineOrients(splineData, rigStructureSettings)

        self.ikSplineLayer.applyNamespace(splineIkRig)

    def validateRigSettings(self, jointObjectArray):
        if jointObjectArray is None:
            return False

        if self.ikSplineSettings.createTweaker == True:
            self.ikSplineSettings.applyPositionOffset = False
            self.ikSplineSettings.applyRotationOffset = False

        if self.ikSplineSettings.numberOfCurveController > MAX_TWIST_OBJECT:
            self.ikSplineSettings.numberOfCurveController = MAX_TWIST_OBJECT

        if self.ikSplineSettings.numberOfCurveController < 4:
            self.ikSplineSettings.numberOfCurveController = 3

        self.gameBonesSettings.skelRootArray = self.ikSplineSettings.skelRootArray
        self.gameBonesSettings.skelRootAnchor = self.ikSplineSettings.skelRootAnchor

        return True

    def connectLayeredMotionPathRigs(self, 
                                     ikRigRoot,
                                     motionPathRigRoot):
        if ikRigRoot is None:
            return

        ikRigRootObject = mobu.FBFindModelByLabelName(ikRigRoot)  

        ikRigMetaRoot = ikRigRootObject.PropertyList.Find('ikSplineMetaRoot')
        if ikRigMetaRoot is None:
            return

        motionPathRigMetaRoot = motionPathRigRoot.PropertyList.Find('ikSplineMetaRoot')
        if motionPathRigMetaRoot is None:
            return

        self.ikSplineStructure.connectMetaData(ikRigRootObject, 'layeredMotionPathGroup', motionPathRigRoot)
        self.ikSplineStructure.connectMetaData(motionPathRigRoot , 'layeredMotionPathGroup', ikRigRootObject)

    def buildRig(self,
                 splineObject,
                 jointObjectArray,
                 deformCurve=True,
                 rigRoot=None,
                 debugMode=False,
                 skipCurveRebuilding=False):
        """
            This method will setup your joint chain to a curve path
            Args:
                splineObject (FBModelPath3d): CurveShape use by
                the ikSpline constraint.
                
                jointObjectArray (modelList): the object list
                we want to drive
            
            return type dict
        """
        continueRigBuilding = self.validateRigSettings(jointObjectArray)

        if continueRigBuilding is False:
            return

        #----------------------------------------------------------------------Build or validate curve path
        if splineObject is None:
            splineObject = self.createPathFromJoints(jointObjectArray,
                                                     name=str(self.ikSplineSettings.ikPathName),
                                                     numberOfCurveController=\
                                                     self.ikSplineSettings.numberOfCurveController,
                                                     skipCurveRebuilding=skipCurveRebuilding)

        self.RefreshScene()

        #----------------------------------------------------------------------Build ikControls
        ikControlsData = self.ikSplineLayer.createIkRig(splineObject,
                                                        jointObjectArray,
                                                        self.ikSplineSettings)

        self.RefreshScene()

        ikControls = list(ikControlsData.ikControls)
        ikSplineStructureSettings = self.attachToIkSpline(splineObject,
                                                          jointObjectArray,
                                                          ikControls)

        self.RefreshScene()
        if self.ikSplineSettings.addExtensionToCharacter is not None:
            characterObject = mobu.FBFindObjectByFullName(self.ikSplineSettings.addExtensionToCharacter)
            ikSplineStructureSettings.addExtensionToCharacter = characterObject

        ikSplineStructureSettings.ikControlsData  = ikControlsData
        self.RefreshScene()

        ikSplineStructureSettings.constraintFolder = mobu.FBFolder('{0}_constraints1'.format(splineObject.Name),
                                                                   ikSplineStructureSettings.ikConstraint)

        ikSplineStructureSettings.rigRoot = rigRoot
        ikSplineStructureSettings.ikSplineSettings = self.ikSplineSettings
        self.RefreshScene()

        if debugMode is True:
            print type(ikSplineStructureSettings) 

        #--------------------------------------------------------------------Build fkControls
        if self.ikSplineSettings.createFkControls is True:
            ikSplineStructureSettings.fkControls = self.ikSplineLayer.createFkRig(ikControlsData, self.ikSplineSettings)
        else:
            ikSplineStructureSettings.fkControls = ikSettings.FkControlSettings()
        if debugMode is True:
            print 'FKCtrls', type(ikSplineStructureSettings) 
        self.RefreshScene()

        #------------------------------Now connect upVector controls to  upVector attributes
        if self.ikSplineSettings.buildUpVector is True:
            self.ikSplineLayer.connectUpVectorOffset(ikSplineStructureSettings.ikConstraint,
                                                     ikControlsData.UpVectorControls,
                                                     connectionMode='constraintGroup')
        self.RefreshScene()

        #------------------------------Display an optional curve space upvector array
        if self.ikSplineSettings.showSplineMatrix is True:
            ikSplineStructureSettings.splineKnots = self.ikSplineLayer.buildSplineMatrix(splineObject,
                                                                                         ikSplineStructureSettings.ikConstraint,
                                                                                         self.ikSplineSettings)

        #------------------------------Constraint some mandatory joint in the hierarchy in
        # ------------------------------------------order to be able to export translation
        print "GAME BONES", 
        self.ikSplineLayer.attachGameBones(self.gameBonesSettings,
                                           ikSplineStructureSettings)

        self.RefreshScene()

        #-----------------------------------------------------Parent and organize Setup elements
        ikRigging.IkSplineStructure().build(splineObject,
                                            self.ikSplineSettings,
                                            ikSplineStructureSettings)

        for ikControl in ikControls:
            ikControl.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire

        self.RefreshScene()
        if debugMode is True:
            print 'IkSplineStructure build', type(ikSplineStructureSettings) 
            print ikSplineStructureSettings.__dict__

        #--------------------------------------Group/Set setup here Controls / Plot Group / Geo
        self.ikSplineStructure.finalize(self.ikSplineSettings,
                                        ikSplineStructureSettings)

        self.RefreshScene()
        ikRigging.SetupCoralSnake().connectOffset(ikSplineStructureSettings)

        if self.ikSplineSettings.isMotionPath is True:
            ikRigging.SetupCoralSnake().connectOffset(ikSplineStructureSettings)
            
            for control in ikSplineStructureSettings.ikControlsData.ikControls:
                control.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
                control.PropertyList.Find('Look').Data = 9

        if debugMode is True:
            print 'SetupCoralSnake ', type(ikSplineStructureSettings) 


        return ikSplineStructureSettings

    def extractMotionPathObjectArray(self,
                                     sourceObjectArray,
                                     ikRigRootToConnect,
                                     reverseList):
        drivenObjectArray = mobu.FBModelList()
        if reverseList is False:
            for controlIndex in range(len(sourceObjectArray)):
                if sourceObjectArray[controlIndex].Parent is None:
                    drivenObjectArray.append(sourceObjectArray[controlIndex])
                else:
                    drivenObjectArray.append(sourceObjectArray[controlIndex].Parent)
        else:
            for controlIndex in range(len(sourceObjectArray)-1,-1,-1):
                if sourceObjectArray[controlIndex].Parent is None:
                    drivenObjectArray.append(sourceObjectArray[controlIndex])
                else:
                    drivenObjectArray.append(sourceObjectArray[controlIndex].Parent)

        return drivenObjectArray

    def createMotionPath(self,
                         sourceObjectArray,
                         controlCount,
                         ikRigRootToConnect=None,
                         rigRoot=None,
                         debugMode=False):

        for model in sourceObjectArray:
            model.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire

        drivenObjectArray = self.extractMotionPathObjectArray(sourceObjectArray,
                                                              ikRigRootToConnect,
                                                              self.ikSplineSettings.reverseList)

        self.RefreshScene()
        self.ikSplineSettings.numberOfCurveController = controlCount

        #Fill game joint data
        self.gameBonesSettings.skelRootArray = self.ikSplineSettings.skelRootArray
        self.gameBonesSettings.skelRootAnchor = self.ikSplineSettings.skelRootAnchor

        motionPathCurve = self.createPathFromJoints(drivenObjectArray,
                                                    name=str(self.ikSplineSettings.ikPathName),
                                                    numberOfCurveController=controlCount)

        self.RefreshScene()

        ikControlsData = self.ikSplineLayer.createIkRig(motionPathCurve,
                                                        drivenObjectArray,
                                                        self.ikSplineSettings)

        if debugMode is True:
            print type(ikControlsData)
            print  type(motionPathCurve)

        self.RefreshScene()
        ikControls = ikControlsData.ikControls

        if debugMode is True:
            print ikControls, type(ikControls)

        for control in ikControlsData.ikControls:
            control.PropertyList.Find('Look').Data = 9

        self.RefreshScene()

        ikControls = list(ikControlsData.ikControls)

        ikSplineStructureSettings = self.attachToIkSpline(motionPathCurve,
                                                          drivenObjectArray,
                                                          ikControls,
                                                          debugMode=debugMode)

        if debugMode is True:
            print type(ikSplineStructureSettings)

        ikSplineStructureSettings.ikControlsData  = ikControlsData

        ikSplineStructureSettings.constraintFolder = mobu.FBFolder('{0}_constraints1'.format(motionPathCurve.Name),
                                                                   ikSplineStructureSettings.ikConstraint)

        self.RefreshScene()

        ikSplineStructureSettings.rigRoot = rigRoot

        ikSplineStructureSettings.ikSplineSettings = self.ikSplineSettings

        #--------------------------------------------------------------------Fill fkControls
        ikSplineStructureSettings.fkControls = ikSettings.FkControlSettings()


        #------------------------------Constraint some mandatory joint in the hierarchy in
        # ------------------------------------------order to be able to export translation
        self.ikSplineLayer.attachGameBones(self.gameBonesSettings,
                                           ikSplineStructureSettings)

        self.RefreshScene()
        if debugMode is True:
            if isinstance(ikSplineStructureSettings, ikSettings.IkSplineStructureSettings) is False:
                print ikSplineStructureSettings
                return

        #-----------------------------------------------------Parent and organize Setup elements

        ikRigging.IkSplineStructure().build(motionPathCurve,
                                            self.ikSplineSettings,
                                            ikSplineStructureSettings,
                                            debugMode=debugMode)

        if debugMode is True:
            if isinstance(ikSplineStructureSettings, ikSettings.IkSplineStructureSettings) is False:
                print ikSplineStructureSettings
                return

        self.RefreshScene()

        self.connectLayeredMotionPathRigs(ikRigRootToConnect,
                                          ikSplineStructureSettings.splineIkRig)

        #--------------------------------------Group/Set setup here Controls / Plot Group / Geo
        self.ikSplineStructure.finalize(self.ikSplineSettings,
                                        ikSplineStructureSettings)

        #--------------------------------------Setup sliding parameter
        ikRigging.SetupCoralSnake().connectOffset(ikSplineStructureSettings)

        return ikSplineStructureSettings