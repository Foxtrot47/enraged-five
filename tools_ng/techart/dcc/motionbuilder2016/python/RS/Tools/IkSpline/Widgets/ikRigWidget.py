import json
import os

from functools import partial
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Config

from RS.Tools.IkSpline import Core as  IkSplineCore
from RS.Tools.IkSpline import Utils as ikUtils


class IkRigWidget(QtGui.QWidget):
    """
        main Tool UI
    """
    def __init__(self,
                 toolBox):
        """
            Constructor
        """
        self.toolBox = toolBox

        # Main window settings
        super(IkRigWidget, self).__init__()
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()
        anchorLayout = QtGui.QHBoxLayout()
        groupAnchorLayout = QtGui.QVBoxLayout()

        # Create Widgets 
        self.groupAnchor = QtGui.QWidget()
        self.pickerAnchor = QtGui.QWidget()

        self.widgetGroupBox = QtGui.QGroupBox('IkRig Group:')
        self.ikRootField = QtGui.QLineEdit()

        self.toggleRigWidget = QtGui.QToolButton(self)

        # Edit properties 
        mainLayout.setContentsMargins(0, 0, 0, 0)
        anchorLayout.setContentsMargins(0, 0, 0, 0)
        groupAnchorLayout.setContentsMargins(5, 5, 5, 5)
        
        self.toggleRigWidget.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding, 
                                           QtGui.QSizePolicy.Maximum)

        self.toggleRigWidget.setMinimumHeight(26)

        self.anchorRootButton = QtGui.QPushButton(">>")

        self.toggleRigWidget.setCheckable(True)
        self.toggleRigWidget.setText("IkSpline Rig Enabled")

        # Assign Layouts to WidgetStack
        anchorLayout.addWidget(self.ikRootField)
        anchorLayout.addWidget(self.anchorRootButton)

        groupAnchorLayout.addWidget(self.pickerAnchor)
        groupAnchorLayout.addWidget(self.toggleRigWidget)

        mainLayout.addWidget(self.widgetGroupBox)

        #Populates character list
        self.widgetGroupBox.setLayout(groupAnchorLayout)
        self.pickerAnchor.setLayout(anchorLayout)
        self.setLayout(mainLayout)

        # callbacks
        self.toggleRigWidget.clicked[bool].connect(self._toggleRig)

        self.anchorRootButton.clicked.connect(partial(self._appendSelectedIkRoot,
                                                      self.ikRootField))

    def _toggleRig(self, clickState):
        splineIkRigName = str(self.ikRootField.text())

        if clickState is False:
            self.toggleRigWidget.setText("IkSpline Rig Enabled")
        else:
            self.toggleRigWidget.setText("IkSpline Rig Disabled")

        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        ikUtils.AnimUtils().toggleIkSplineRigWeights(splineIkRig, clickState)

        ikUtils.AnimUtils().consolidateGlueComponents(splineIkRig)

    def findIkSplineRootRoot(self, model):
        metaRootProperty = model.PropertyList.Find('ikSplineMetaRoot')
        if metaRootProperty is not None:
            return model

        parent = model.Parent
        if parent == None:
            return None
            
        metaRootProperty = parent.PropertyList.Find('ikSplineMetaRoot')
        if metaRootProperty is not None:
            return parent

        root = self.findIkSplineRootRoot(parent)
        if root is None:
            return parent
        else:
            return root

    def _extractAnimationController(self, propertyLink, controlType):
        if self.splineIkRig is None:
            return

        controlLink = self.splineIkRig.PropertyList.Find(propertyLink)
        if controlLink is None:
            return

        groupMembersCount = controlLink.GetSrcCount()
        if groupMembersCount == 0:
            return

        if controlType == 'ik':
            self.animIkControls = []

        if controlType == 'tweaker':
            self.animTweakerControls = []

        for memberIndex in range(groupMembersCount):
            member = controlLink.GetSrc(memberIndex)
            controlNamesplit = member.Name.split('_')

            if controlType == 'ik':
                if 'Ctrl' in controlNamesplit[-1]:
                    self.animIkControls.append(member)

            if controlType == 'tweaker':
                if 'Tweaker' in controlNamesplit[-1]:
                    self.animTweakerControls.append(member)

    def _appendSelectedIkRoot(self, field):
        field.setText('')
        currentSelection = list(ikUtils.JointChain().getJointChainFromSelection())

        self.splineIkRig = None
        self.rigSettings = None

        self.animIkControls = None
        self.animTweakerControls = None

        if len(currentSelection)<1:
            field.setText('')
            return

        ikRigRoot = self.findIkSplineRootRoot(currentSelection[0])
        if ikRigRoot is None:
            field.setText('')
            return

        currentSelection[0] = ikRigRoot

        metaRootProperty = currentSelection[0].PropertyList.Find('ikSplineMetaRoot')
        if metaRootProperty is None:
            field.setText('')
            return

        field.setText(currentSelection[0].LongName)
        self.splineIkRig = currentSelection[0]
        settingAttribute = self.splineIkRig.PropertyList.Find('rigSettings')
        self.rigSettings = json.loads(str(settingAttribute.Data))

        ikControlsSize = self.rigSettings['ikControlsSize']
        numberOfCurveController = self.rigSettings['numberOfCurveController']
        tweakerControlsSize = self.rigSettings['tweakerControlsSize']

        self.toolBox.settingWidget.animIkSizeWidget.setValue(ikControlsSize)
        self.toolBox.settingWidget.animTweakerSizeWidget.setValue(tweakerControlsSize)
        self.toolBox.settingWidget.numberOfCurveControllerWidget.setValue(numberOfCurveController)

        self._extractAnimationController('tweakerGroup', 'tweaker')
        self._extractAnimationController('IkControlGroup', 'ik')

        constraintLink = self.splineIkRig.PropertyList.Find('ikConstraint')
        
        inputRigIkConstraintCheck = constraintLink.GetSrcCount()
        if inputRigIkConstraintCheck == 0:
            return

        inputRigIkConstraint = constraintLink.GetSrc(0)

        ikUtils.AnimUtils().consolidateGlueComponents(self.splineIkRig)

        skipState = (self.toggleRigWidget.isChecked() ==  (not inputRigIkConstraint.Active))
        if self.toggleRigWidget.isDown() == False:
            if inputRigIkConstraint.Active == True:
                self.toggleRigWidget.setChecked(0)
                self.toggleRigWidget.setText("IkSpline Rig Enabled")

        if self.toggleRigWidget.isDown() == False:
            if inputRigIkConstraint.Active == False:
                self.toggleRigWidget.setChecked(1)
                self.toggleRigWidget.setText("IkSpline Rig Disabled")

    def _createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine