import inspect
import os
import uuid
import pyfbsdk as mobu
import json


from RS.Tools.IkSpline.assetSetup.weaponStrapBuilder.Rigging import Builder
reload(Builder)


def UpdateRig():
    strpUtils = Builder.StrapBuilder()

    assetFile = 'x:/rdr3/art/animation/resources/props/Review/w_leftshoulder_strap01A.fbx'
    mobu.FBApplication().FileOpen(assetFile, False)

    strpUtils.useAlternatePoint = True
    strpUtils.createStrapRig(loadFile=False,
                             purgeRig=False)

'''
    HOW TO:

    from RS.Tools.IkSpline.assetSetup.weaponStrapBuilder import createLeftStrap
    from RS.Tools.IkSpline.assetSetup.weaponStrapBuilder import createRegularStrap

    reload(createLeftStrap)
    reload(createRegularStrap)

    leftMode = True


    if leftMode is False:
        createRegularStrap.UpdateRig()
    else:
        createLeftStrap.UpdateRig()
'''