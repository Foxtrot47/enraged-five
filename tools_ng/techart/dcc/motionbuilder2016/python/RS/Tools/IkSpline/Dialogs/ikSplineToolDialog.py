import json
import os

from functools import partial
import pyfbsdk as mobu

from RS import Globals
from RS.Tools import UI

from RS.Tools.IkSpline import Core as  IkSplineCore
from RS.Tools.IkSpline import Utils as ikUtils
from RS.Tools.IkSpline.Unittests.Utils import debugSplineIkOnRessource

reload(IkSplineCore)
reload(ikUtils)
reload(debugSplineIkOnRessource)

from PySide import QtGui, QtCore


class IkSplineTool(UI.QtMainWindowBase):
    """
        main Tool UI
    """
    def __init__(self, animationMode=False):
        """
            Constructor
        """
        self._tabWidget = None
        self.ressourceListWidget = None
        self.jointListLabel = None
        self.jointListWidget = None
        self._sourcePath = None
        self.resourceModel = QtGui.QStandardItemModel()
        self.ressourceSelectionModel = None
        self.numberOfCurveControllerWidget = None
        self.rigNumberOfCurveControllerWidget = None
        self.controllerTargetLine = None
        self.aimToNextJointWidget = None
        self.maintainChainLengthWidget = None
        self.createTweakerWidget = None
        self.lockSelectionWidget = None
        self.hideDrivenJointWidget = None
        self.hidePathWidget = None
        self.fkControlsSizeWidget = None
        self.ikControlsSizeWidget = None
        self.tweakerControlsSizeWidget = None
        self.buildIkSplineButtonWidget = None
        self.optionalSettingsTab = None
        self.optionalVisibilityTab = None
        self.optionalSizeTab = None
        self.controllerTab = None
        self.jointListTab = None
        self.AddToJointListButtonWidget = None
        self.RemoveFromJointListButtonWidget = None
        self.mandatoryJointTab = None 
        self.addJointMode = None
        self.jointRootField = None
        self.jointRootButton = None
        self.jointChainFromRootButtonWidget = None

        #Data 
        self.resourceData = []
        self.jointSelectModel = QtGui.QStringListModel()
        self.jointListModel = QtGui.QStringListModel()
        self.plotTranslationModeModel = QtGui.QStringListModel()

        self._toolName = "IK spline Tool"
        self.animationMode = animationMode
        if animationMode is True:
            self._toolName = "Dynamic spline Rig v{0}".format('1.1.1')
        self.tabSizeHeight = 0

        self.rigSettings = None
        self.splineIkRig = None

        self.animIkControls = None
        self.animTweakerControls = None

        # Main window settings
        super(IkSplineTool, self).__init__(title=self._toolName, dockable=False, size=[310, 280])#310, 280
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        self.mainWidget = QtGui.QWidget()
        mainLayout = QtGui.QVBoxLayout()
        self._tabWidget = QtGui.QTabWidget()

        # Create Widgets 
        banner = UI.BannerWidget(self._toolName)
        self.setupTab = self.createSetupTab()
        self.ressourceTab = self.createRessourceTab()
        self.animTab = self.createAnimTab()

        # Set properties
        if self.animationMode is False:
            self._tabWidget.addTab(self.setupTab , "Create")
            self._tabWidget.addTab(self.ressourceTab, "Resources")   
        else:
            self._tabWidget.addTab(self.animTab , 'Dynamic rig')

        # Assign Widgets to layouts 
        mainLayout.addWidget(banner)
        mainLayout.addWidget(self._tabWidget)

        # Assign Layouts to WidgetStack
        self.mainWidget.setLayout(mainLayout)
        self.setCentralWidget(self.mainWidget)

        self._tabWidget.currentChanged.connect(self.resizeTab)

    def createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def createAnimTab(self):
        # Create Layouts
        animLayout = QtGui.QGridLayout()
        animIkSizeLayout = QtGui.QHBoxLayout()
        animTweakerSizeLayout = QtGui.QHBoxLayout()
        controllerCountLayout = QtGui.QHBoxLayout()
        motionPathControllerCountLayout = QtGui.QHBoxLayout()
        anchorLayout = QtGui.QHBoxLayout()

        ## Create TabContainer[subLayouts]
        animTakeLayout = QtGui.QHBoxLayout()
        animTakeLayout.setContentsMargins(0, 0, 0, 0)
        animTakeLayout.setSpacing(5)

        self.refreshTakeWidget = QtGui.QPushButton("Refresh Take list")
        self.batchPlotModeWidget = QtGui.QCheckBox("Plot Selected Takes") 
        self.plotTranslationModeWidget = QtGui.QComboBox()
        self.plotTranslationModeWidget.setModel(self.plotTranslationModeModel)
        self.plotTranslationModeModel.setStringList(['Plot rotation and translation', 'Plot rotation only on chain links'])
        self.plotTranslationModeWidget.setCurrentIndex(0)

        animTakeLayout.addWidget(self.batchPlotModeWidget)
        animTakeLayout.addWidget(self.refreshTakeWidget)

        animTakeTab = QtGui.QWidget()
        animTakeTab.setLayout(animTakeLayout)

        snapCurveOffsetLayout = QtGui.QHBoxLayout()
        snapCurveOffsetLayout.setContentsMargins(0, 0, 0, 0)
        snapCurveOffsetLayout.setSpacing(5)

        self.snapToCurveOffsetWidget = QtGui.QDoubleSpinBox(self)
        self.snapToCurveOffsetWidget.setRange(0.0,100.0)
        self.snapToCurveOffsetWidget.setValue(0.0)

        snapCurveOffsetLayout.addWidget(QtGui.QLabel("Offset rig along curve:"))
        snapCurveOffsetLayout.addWidget(self.snapToCurveOffsetWidget)

        snapCurveOffsetTab = QtGui.QWidget()
        snapCurveOffsetTab.setLayout(snapCurveOffsetLayout)

        snapCurveLayout = QtGui.QHBoxLayout()
        snapCurveLayout.setContentsMargins(0, 0, 0, 0)
        snapCurveLayout.setSpacing(5)

        self.snapCurveField = QtGui.QLineEdit()
        self.snapCurveButton = QtGui.QPushButton(">>")

        snapCurveLayout.addWidget(QtGui.QLabel("Motion Curve:"))
        snapCurveLayout.addWidget(self.snapCurveField)
        snapCurveLayout.addWidget(self.snapCurveButton)

        snapCurveTab = QtGui.QWidget()
        snapCurveTab.setLayout(snapCurveLayout)

        self.animMotionPathField = QtGui.QLineEdit()
        self.ikRootField = QtGui.QLineEdit()
        self.anchorRootButton = QtGui.QPushButton(">>")
        assignMotionPathButton = QtGui.QPushButton(">>")

        animTab = QtGui.QWidget() 
        animIkSizeTab = QtGui.QWidget()
        animTweakerSizeTab = QtGui.QWidget()
        self.controllerTab = QtGui.QWidget()
        self.motionPathControllerTab = QtGui.QWidget()
        anchorTab = QtGui.QWidget()

        self.numberOfCurveControllerWidget = QtGui.QSpinBox(self)
        self.numberOfCurveControllerWidget.setRange(3,400)
        self.numberOfCurveControllerWidget.setValue(5)

        self.animIkSizeWidget = QtGui.QDoubleSpinBox(self)
        self.animIkSizeWidget.setRange(0.0001,10000.0)
        self.animIkSizeWidget.setValue(500.0)

        self.animTweakerSizeWidget = QtGui.QDoubleSpinBox(self)
        self.animTweakerSizeWidget.setRange(0.0001,10000.0)
        self.animTweakerSizeWidget.setValue(500.0)

        self.numberOfMotionPathControllerWidget = QtGui.QSpinBox(self)
        self.numberOfMotionPathControllerWidget.setRange(3,400)
        self.numberOfMotionPathControllerWidget.setValue(15)

        self.layeredModeCheckboxWidget = QtGui.QCheckBox("")
        self.toggleRigWidget = QtGui.QToolButton(self)#
        self.alignXaxisButtonWidget = QtGui.QPushButton("Align animation controllers x Axis")
        self.snapRigButtonWidget = QtGui.QPushButton("Snap Rig to Curve")
        self.matchRigButtonWidget = QtGui.QPushButton("Match Rig to Curve")
        self.plotButtonWidget = QtGui.QPushButton("Plot Animation")
        self.resetIkSplineButtonWidget = QtGui.QPushButton("reset IkSpline")
        self.rebuildIkSplineButtonWidget = QtGui.QPushButton("Rebuild IkSpline")
        self.rebuildLayeredRigButtonWidget = QtGui.QPushButton("Rebuild Layered Rig")
        self.takeListWidget = QtGui.QListView() 

        self.takeArrayModel = QtGui.QStandardItemModel()
        self.takeListWidget.setModel(self.takeArrayModel)
        self.takeListWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.takeArraySelectionModel = self.takeListWidget.selectionModel()

        self.toggleControllerAxisWidget = QtGui.QCheckBox("Toggle Controller Axis") 
        self.plotAimController = QtGui.QCheckBox("Plot Aim Controller on Current Take") 

        self.plotButtonWidget.setMinimumHeight(42)
        self.snapRigButtonWidget.setMinimumHeight(30)
        self.rebuildLayeredRigButtonWidget.setMinimumHeight(36)

        self.alignXaxisButtonWidget.setMinimumHeight(36)

        self.toggleRigWidget.setCheckable(True)
        self.toggleRigWidget.setText("IkSpline Rig Enabled")
        self.toggleRigWidget.setMinimumWidth(292)
        self.rebuildLayeredRigButtonWidget.setMinimumHeight(24)

        self.plotButtonWidget.setMinimumHeight(42)
        anchorLayout.setContentsMargins(0, 0, 0, 0)
        anchorLayout.setSpacing(5)

        anchorLayout.setContentsMargins(0, 0, 0, 0)
        anchorLayout.setSpacing(5)
        self.anchorRootButton.setMaximumHeight = 16

        self.layeredModeCheckboxWidget.setChecked(1)
        self.layeredModeCheckboxWidget.setVisible(0)

        anchorLayout.addWidget(QtGui.QLabel("IkRig Group:"))
        anchorLayout.addWidget(self.ikRootField)
        anchorLayout.addWidget(self.anchorRootButton)

        controllerCountLayout.addWidget(QtGui.QLabel("Curve controller Count"))
        controllerCountLayout.addWidget(self.numberOfCurveControllerWidget)

        animIkSizeLayout.addWidget(QtGui.QLabel("Ik controller size"))
        animIkSizeLayout.addWidget(self.animIkSizeWidget)

        animTweakerSizeLayout.addWidget(QtGui.QLabel("Tweaker controller size"))
        animTweakerSizeLayout.addWidget(self.animTweakerSizeWidget)

        motionPathControllerCountLayout.addWidget(QtGui.QLabel("Motion path controller Count"))
        motionPathControllerCountLayout.addWidget(self.numberOfMotionPathControllerWidget )

        self.separatorList = []
        for k in range(11):
            self.separatorList.append(self.createSeparator())

        animLayout.addWidget(anchorTab)
        animLayout.addWidget(self.separatorList[0])
        animLayout.addWidget(self.layeredModeCheckboxWidget)
        animLayout.addWidget(self.resetIkSplineButtonWidget)
        animLayout.addWidget(self.separatorList[1])
        animLayout.addWidget(self.controllerTab)
        animLayout.addWidget(animIkSizeTab)
        animLayout.addWidget(animTweakerSizeTab)
        animLayout.addWidget(self.separatorList[2])
        animLayout.addWidget(self.motionPathControllerTab)
        animLayout.addWidget(self.separatorList[3])
        animLayout.addWidget(self.rebuildIkSplineButtonWidget)
        animLayout.addWidget(self.separatorList[4])
        animLayout.addWidget(self.rebuildLayeredRigButtonWidget)
        animLayout.addWidget(self.separatorList[5])

        animLayout.addWidget(self.toggleControllerAxisWidget)
        animLayout.addWidget(self.alignXaxisButtonWidget)
        animLayout.addWidget(self.plotAimController)

        animLayout.addWidget(self.separatorList[10])
        animLayout.addWidget(QtGui.QLabel("Animation Takes:"))
        animLayout.addWidget(self.takeListWidget)
        animLayout.addWidget(animTakeTab)

        animLayout.addWidget(self.plotTranslationModeWidget)
        animLayout.addWidget(self.plotButtonWidget)
        animLayout.addWidget(self.separatorList[6])
        animLayout.addWidget(self.toggleRigWidget)
        animLayout.addWidget(self.separatorList[7])
        animLayout.addWidget(snapCurveTab)
        animLayout.addWidget(self.separatorList[8])
        animLayout.addWidget(snapCurveOffsetTab)
        animLayout.addWidget(self.snapRigButtonWidget)
        animLayout.addWidget(self.separatorList[9])
        animLayout.addWidget(self.matchRigButtonWidget)

        self.IkRigWidgets = [self.resetIkSplineButtonWidget,
                             self.separatorList[1],
                             self.rebuildIkSplineButtonWidget,
                             self.separatorList[4]]

        self.layeredRigWidgets = [self.motionPathControllerTab,
                                  self.separatorList[3],
                                  self.rebuildLayeredRigButtonWidget,
                                  self.separatorList[5]]

        animTweakerSizeTab.setLayout(animTweakerSizeLayout)
        animIkSizeTab.setLayout(animIkSizeLayout)
        self.controllerTab.setLayout(controllerCountLayout)
        self.motionPathControllerTab.setLayout(motionPathControllerCountLayout)
        anchorTab.setLayout(anchorLayout)

        # Assign Layouts to WidgetStack
        animTab.setLayout(animLayout)
        animTab.setMinimumWidth(310)

        self.snapCurveButton.clicked.connect(partial(self._appendSelectedCurve,self.snapCurveField))
        self.snapRigButtonWidget.clicked.connect(self._snapRigOn3dCurve)
        self.matchRigButtonWidget.clicked.connect(self._matchRigOn3dCurve)

        self.anchorRootButton.clicked.connect(partial(self._appendSelectedIkRoot,self.ikRootField))
        self.resetIkSplineButtonWidget.clicked.connect(self._resetRig)
        self.rebuildIkSplineButtonWidget.clicked.connect(self._rebuildRig)
        self.rebuildLayeredRigButtonWidget.clicked.connect(self._rebuildLayeredRig)
        self.plotButtonWidget.clicked.connect(self._plotRigAnimation)
        self.toggleRigWidget.clicked[bool].connect(self._toggleRig)

        self.layeredModeCheckboxWidget.stateChanged.connect(self._switchRigType)
        self.layeredModeCheckboxWidget.setChecked(0)

        self.refreshTakeWidget.clicked.connect(self.refreshTakeList)
        self.toggleControllerAxisWidget.stateChanged.connect(self._toggleControllerAxis)
        self.alignXaxisButtonWidget.clicked.connect(self._aimControls)

        self.animTweakerControls = []

        self.animIkSizeWidget.valueChanged.connect(partial(self._updateControllerSize, 'ik'))
        self.animTweakerSizeWidget.valueChanged.connect(partial(self._updateControllerSize, 'tweaker'))

        self.refreshTakeList()

        return animTab

    def _batchPlotAnimation(self, splineIkRig, plotTranslation):
        selectedTakesIds = self.takeArraySelectionModel.selectedIndexes()
        if len(selectedTakesIds) == 0:
            return

        takeArray = []
        for takeIndex in selectedTakesIds:
            takeName = str(takeIndex.data())

            for take in Globals.Scene.Takes:
                if take.Name == takeName:
                    takeArray.append(take)

        ikUtils.AnimUtils().batchPlotAnimation(splineIkRig, takeArray, plotTranslation)

    def _aimControls(self):
        splineIkRigName = str(self.ikRootField.text())

        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        plotTake = self.plotAimController.isChecked()
        if plotTake is False:
            ikUtils.AnimUtils().aimControls(splineIkRig)
        else:
            ikUtils.AnimUtils().aimControlsOnTakeRange(splineIkRig)

    def _toggleControllerAxis(self):
        splineIkRigName = str(self.ikRootField.text())

        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        ikUtils.AnimUtils().toggleControllerAxis(splineIkRig, 
                                                 self.toggleControllerAxisWidget.isChecked())

    def refreshTakeList(self):
        self.takeArrayModel.clear()
        takeArray = []
        for take in Globals.Scene.Takes:
            item = QtGui.QStandardItem(take.Name)
            self.takeArrayModel.appendRow(item)

        selectionIndex = self.takeArrayModel.index(0,0) 
        self.takeArraySelectionModel.setCurrentIndex(selectionIndex ,QtGui.QItemSelectionModel.Select)

    def _snapRigOn3dCurve(self):
        splineIkRigName = str(self.ikRootField.text())

        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        motionCurveName = str(self.snapCurveField.text())
        if motionCurveName is None:
            return
        
        if len(motionCurveName) < 0:
            return

        motionCurve = mobu.FBFindModelByLabelName(motionCurveName)
        if motionCurve is None:
            return
        ikUtils.AnimUtils().snapRigOn3dCurve(splineIkRig, motionCurve, offset=self.snapToCurveOffsetWidget.value())

    def _matchRigOn3dCurve(self):
        splineIkRigName = str(self.ikRootField.text())

        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        motionCurveName = str(self.snapCurveField.text())
        if motionCurveName is None:
            return
        
        if len(motionCurveName) < 0:
            return

        motionCurve = mobu.FBFindModelByLabelName(motionCurveName)
        if motionCurve is None:
            return
        ikUtils.AnimUtils().matchRigOn3dCurve(splineIkRig, motionCurve, offset=self.snapToCurveOffsetWidget.value())

    def _updateControllerSize(self, controllerType, targetSize):
        if self.splineIkRig is None:
            return

        if controllerType == 'ik':
            if self.animIkControls is None:
                return

            for control in self.animIkControls:
                control.PropertyList.Find('Size').Data = targetSize

        if controllerType == 'tweaker':
            if self.animTweakerControls is None:
                return

            for control in self.animTweakerControls:
                control.PropertyList.Find('Size').Data = targetSize

    def writeIkRigSettings(self, controllerType, targetSize):
        if self.splineIkRig is None:
            return

        if self.rigSettings is None:
            return

        if controllerType == 'ik':
            self.rigSettings['ikControlsSize'] = targetSize

        if controllerType == 'tweaker':
            self.rigSettings['tweakerControlsSize'] = targetSize

    def _toggleRig(self, clickState):
        splineIkRigName = str(self.ikRootField.text())

        if clickState is False:
            self.toggleRigWidget.setText("IkSpline Rig Enabled")
        else:
            self.toggleRigWidget.setText("IkSpline Rig Disabled")

        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        ikUtils.AnimUtils().toggleIkSplineRigWeights(splineIkRig, clickState)

    def _plotRigAnimation(self):
        splineIkRigName = str(self.ikRootField.text())
        if splineIkRigName is None:
            return
        
        if len(splineIkRigName) < 0:
            return

        splineIkRig = mobu.FBFindModelByLabelName(splineIkRigName)
        if splineIkRig is None:
            return

        plotTranslation = False
        if self.plotTranslationModeWidget.currentIndex() == 0:
            plotTranslation = True

        if self.batchPlotModeWidget.isChecked() is True:
            self._batchPlotAnimation(splineIkRig, plotTranslation)
        else:
            ikUtils.AnimUtils().plotAnimation(splineIkRig, plotTranslation)

    def _switchRigType(self, *args):
        if self.layeredModeCheckboxWidget.isChecked() is True:
            for widget in self.layeredRigWidgets:
                widget.setVisible(1)

            for widget in self.IkRigWidgets:
                widget.setVisible(0)
        else:
            for widget in self.layeredRigWidgets:
                widget.setVisible(0)

            for widget in self.IkRigWidgets:
                widget.setVisible(1)

    def createSetupTab(self):
        """
        Internal Method

        Setup the load tab
        """
        # Create Layouts
        loadLayout = QtGui.QVBoxLayout()

        # Create Widgets
        self._buildJointLayout()
        self._buildControlsSizeSettings()
        self._buildIksplineSettings()
        self._buildVisibilitySettings()
        self.createCurveControllerCount()

        ## Create TabContainer[subLayouts]
        loadTab = QtGui.QToolBox() 
        loadTabFrame = QtGui.QFrame() 

        self.createMotionPathCheckBox  = QtGui.QCheckBox("Create motionpath")
        self.buildIkSplineButtonWidget = QtGui.QPushButton("Build IkSpline")
        self.buildIkSplineButtonWidget.setMinimumHeight(36)

        # Assign Widgets to Layouts
        ##Object Driven by the ikSPline
        loadTab.addItem(self.mandatoryJointTab, 'Rig components')
        loadTab.addItem(self.optionalSettingsTab, 'Ikspline settings:')
        loadTab.addItem(self.optionalVisibilityTab, 'Selection/visibility settings:')
        loadTab.addItem(self.optionalSizeTab, 'Size Settings:')

        loadTab.setMinimumWidth(320)
        
        loadLayout.addWidget(loadTab)
        loadLayout.addWidget(self._addSeparator())
        loadLayout.addWidget(self.controllerTab)
        loadLayout.addWidget(self._addSeparator())
        loadLayout.addWidget(self.createMotionPathCheckBox )

        loadLayout.addWidget(self.buildIkSplineButtonWidget)

        loadTab.setMinimumHeight(350)
        # Assign Layouts to WidgetStack
        loadTabFrame.setLayout(loadLayout)

        self.buildIkSplineButtonWidget.clicked.connect(self._buildIkSplineRig)

        return loadTabFrame

    def _rebuildRig(self):
        motionUtils = IkSplineCore.Setup()
        motionUtils.rebuildRig(str(self.ikRootField.text()),
                               overrideControllerCount=self.numberOfCurveControllerWidget.value())

    def _resetRig(self):
        motionUtils = IkSplineCore.Setup()
        splineIkRig = mobu.FBFindModelByLabelName(str(self.ikRootField.text()))
        if splineIkRig is None:
            return

        motionUtils.resetRig(splineIkRig)

    def _rebuildLayeredRig(self):
        motionUtils = IkSplineCore.Setup()
        splineIkRig = mobu.FBFindModelByLabelName(str(self.ikRootField.text()))
        if splineIkRig is None:
            return

        motionUtils.rebuildLayeredRig(str(self.ikRootField.text()),
                                      controllerCount=self.numberOfCurveControllerWidget.value(),
                                      motionPathControllerCount=self.numberOfMotionPathControllerWidget.value())

    def createCurveControllerCount(self):
        controllerCountLayout = QtGui.QHBoxLayout()
        self.controllerTab = QtGui.QWidget()

        self.rigNumberOfCurveControllerWidget = QtGui.QSpinBox(self)
        
        self.rigNumberOfCurveControllerWidget.setRange(3,50)
        self.rigNumberOfCurveControllerWidget.setValue(5)

        controllerCountLayout.addWidget(QtGui.QLabel("Curve controller Count"))
        controllerCountLayout.addWidget(self.rigNumberOfCurveControllerWidget )
        self.controllerTab.setLayout(controllerCountLayout)

    def _buildJointLayout(self):
        # Create Layouts

        jointListLayout = QtGui.QVBoxLayout()
        selectRootLayout = QtGui.QHBoxLayout()
        selectModeLayout = QtGui.QHBoxLayout()
        addModeLayout = QtGui.QHBoxLayout()
        mandatoryJointLayout = QtGui.QVBoxLayout() 

        # Create Widgets
        self.addJointMode = QtGui.QComboBox()

        self.jointRootField = QtGui.QLineEdit()
        self.jointRootButton = QtGui.QPushButton(">>")

        self.AddToJointListButtonWidget = QtGui.QPushButton("Add to list")
        self.jointChainFromRootButtonWidget = QtGui.QPushButton("Get joint chain from root")

        self.RemoveFromJointListButtonWidget = QtGui.QPushButton("Remove from list")
        
        self.jointListWidget = QtGui.QListView() 
        self.jointListLabel = QtGui.QLabel("Joint List")
        jointSelectionModeLabel = QtGui.QLabel("Selection Mode")

        ## Create TabContainer[subLayouts]

        self.jointListTab = QtGui.QWidget()
        self.mandatoryJointTab = QtGui.QWidget() 
        
        self.selectRootTab = QtGui.QWidget()

        # Set properties
        self.jointListLabel.setAlignment(QtCore.Qt.AlignCenter)

        self.jointListWidget.setMinimumHeight(180)

        jointListLayout.setContentsMargins(0, 0, 0, 0)
        jointListLayout.setSpacing(5)

        selectRootLayout.setContentsMargins(0, 0, 0, 0)
        selectRootLayout.setSpacing(5)
        self.jointRootButton.setMaximumHeight(21)

        self.addJointMode.setModel(self.jointSelectModel)

        self.AddToJointListButtonWidget.setMinimumHeight(32)
        self.jointChainFromRootButtonWidget.setMinimumHeight(32)

        self.jointListWidget.setModel(self.jointListModel)     

        # Assign Widgets to Layouts
        selectModeLayout.addWidget(jointSelectionModeLabel)
        selectModeLayout.addWidget(self.addJointMode)

        selectRootLayout.addWidget(QtGui.QLabel("Root:"))
        selectRootLayout.addWidget(self.jointRootField)
        selectRootLayout.addWidget(self.jointRootButton)

        jointListLayout.addWidget(self.jointListLabel)
        jointListLayout.addWidget(self.jointListWidget)
        jointListLayout.addWidget(self.RemoveFromJointListButtonWidget)

        jointListLayout.addItem(selectModeLayout)
        jointListLayout.addWidget(self.selectRootTab)

        jointListLayout.addWidget(self.AddToJointListButtonWidget)
        jointListLayout.addWidget(self.jointChainFromRootButtonWidget)

        mandatoryJointLayout.addWidget(self.jointListTab)


        # Assign Layouts to WidgetStack

        self.jointListTab.setLayout(jointListLayout)
        self.selectRootTab.setLayout(selectRootLayout)
        self.mandatoryJointTab.setLayout(mandatoryJointLayout)

        self._populateJointSelectionMode()

        # Setup Signals
        self.addJointMode.currentIndexChanged.connect(self._addJointModeSwap)
        self._addJointModeSwap()

        # callbacks
        self.jointRootButton.clicked.connect(partial(self._setFieldFromSelection,
                                                     self.jointRootField))

        self.jointChainFromRootButtonWidget.clicked.connect(self._appendJointHierarchyFromRoot)
        self.AddToJointListButtonWidget.clicked.connect(self._appendSelectedJoints)

        self.RemoveFromJointListButtonWidget.clicked.connect(self._removeFromList)

    def _addJointModeSwap(self,*args):
        if self.addJointMode.currentIndex() == 0:
            self.selectRootTab.setVisible(False)
            self.jointChainFromRootButtonWidget.setVisible(False)
            self.AddToJointListButtonWidget.setVisible(True)
        else:
            self.selectRootTab.setVisible(True)
            self.jointChainFromRootButtonWidget.setVisible(True)
            self.AddToJointListButtonWidget.setVisible(False)
            
    def _addSeparator(self):
        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setFrameShadow(QtGui.QFrame.Sunken)

        return line

    def _populateJointSelectionMode(self):
        self.jointSelectModel.setStringList(['Selected Items','Hierachy From Root'])
        self.addJointMode.setCurrentIndex(0)

    def _buildControlsSizeSettings(self):
        # Create Layouts
        controllerSizeLayout = QtGui.QVBoxLayout()
        optionalSizeLayout = QtGui.QHBoxLayout()

        # Create Widgets
        self.fkControlsSizeWidget = QtGui.QDoubleSpinBox(self)
        self.ikControlsSizeWidget = QtGui.QDoubleSpinBox(self)
        self.tweakerControlsSizeWidget = QtGui.QDoubleSpinBox(self)

        ## Create TabContainer[subLayouts]
        sizeTab = QtGui.QFrame()
        self.optionalSizeTab = QtGui.QFrame()

        # Set properties
        self.fkControlsSizeWidget.setRange(0.0001,50000)
        self.ikControlsSizeWidget.setRange(0.0001,50000)
        self.tweakerControlsSizeWidget.setRange(0.0001,50000)

        self.fkControlsSizeWidget.setValue(550.0)
        self.ikControlsSizeWidget.setValue(250.0)
        self.tweakerControlsSizeWidget.setValue(35.0)

        # Assign Widgets to Layouts
        controllerSizeLayout.addWidget(QtGui.QLabel("FK Controls Size"))
        controllerSizeLayout.addWidget(self.fkControlsSizeWidget)
        controllerSizeLayout.addWidget(QtGui.QLabel("IK Controls Size"))
        controllerSizeLayout.addWidget(self.ikControlsSizeWidget)
        controllerSizeLayout.addWidget(QtGui.QLabel("Tweaker Controls Size"))
        controllerSizeLayout.addWidget(self.tweakerControlsSizeWidget)

        optionalSizeLayout.addWidget(sizeTab)

        # Assign Layouts to WidgetStack       
        sizeTab.setLayout(controllerSizeLayout)
        self.optionalSizeTab.setLayout(optionalSizeLayout)

    def _buildVisibilitySettings(self):
        # Create Layouts
        selectionLayout = QtGui.QVBoxLayout()
        optionalVisibilityLayout = QtGui.QVBoxLayout()

        # Create Widgets
        self.lockSelectionWidget = QtGui.QCheckBox('Lock elements')
        self.hideDrivenJointWidget = QtGui.QCheckBox('Hide driven joints')
        self.hidePathWidget = QtGui.QCheckBox('Hide Spline')

        ## Create TabContainer[subLayouts]
        selectionSettingsTab = QtGui.QGroupBox()
        self.optionalVisibilityTab = QtGui.QWidget()

        # Set properties
        self.lockSelectionWidget.setChecked(1)
        self.hideDrivenJointWidget.setChecked(0)
        self.hidePathWidget.setChecked(1)

        # Assign Widgets to Layouts
        ##Object Driven by the ikSPline       
        selectionLayout.addWidget(self.lockSelectionWidget)
        selectionLayout.addWidget(self.hideDrivenJointWidget)
        selectionLayout.addWidget(self.hidePathWidget)

        optionalVisibilityLayout.addWidget(selectionSettingsTab)
        selectionSettingsTab.setLayout(selectionLayout)
        self.optionalVisibilityTab.setLayout(optionalVisibilityLayout)

    def _buildIksplineSettings(self):
        # Create Layouts
        ikSettingsLayout = QtGui.QVBoxLayout()
        anchorLayout = QtGui.QHBoxLayout()
        optionalSettingsLayout= QtGui.QVBoxLayout()

        # Create Widgets
        self.anchorRootField = QtGui.QLineEdit()
        self.anchorRootButton = QtGui.QPushButton(">>")

        self.createTweakerWidget = QtGui.QCheckBox('Create tweakers')
        self.maintainChainLengthWidget = QtGui.QCheckBox('Maintain chain length')
        self.aimToNextJointWidget = QtGui.QCheckBox('Aim to next Joint')
        self.createFkWidget = QtGui.QCheckBox('Create fk controller')

        ## Create TabContainer[subLayouts]
        ikSettingsTab = QtGui.QFrame()
        self.optionalSettingsTab = QtGui.QFrame()
        anchorTab = QtGui.QWidget()

        # Set properties
        anchorLayout.setContentsMargins(0, 0, 0, 0)
        anchorLayout.setSpacing(5)

        self.createTweakerWidget.setChecked(0)
        self.maintainChainLengthWidget.setChecked(1)
        self.aimToNextJointWidget.setChecked(1)

        # Assign Widgets to Layouts
        anchorLayout.addWidget(QtGui.QLabel("Anchor:"))
        anchorLayout.addWidget(self.anchorRootField)
        anchorLayout.addWidget(self.anchorRootButton)

        ikSettingsLayout.addWidget(anchorTab)
        ikSettingsLayout.addWidget(self.aimToNextJointWidget)
        ikSettingsLayout.addWidget(self.maintainChainLengthWidget)
        ikSettingsLayout.addWidget(self.createTweakerWidget)
        ikSettingsLayout.addWidget(self.createFkWidget)
        
        optionalSettingsLayout.addWidget(anchorTab)
        optionalSettingsLayout.addWidget(ikSettingsTab)

        # Assign Layouts to WidgetStack
        ikSettingsTab.setLayout(ikSettingsLayout)
        anchorTab.setLayout(anchorLayout)
        self.optionalSettingsTab.setLayout(optionalSettingsLayout)

        # callbacks
        self.anchorRootButton.clicked.connect(partial(self._setFieldFromSelection,self.anchorRootField))

    def createRessourceTab(self):
        """
        Internal Method

        Setup the load tab
        """
        # Create Layouts
        loadLayout = QtGui.QGridLayout()

        # Create Widgets
        loadTab = QtGui.QWidget()
        self._sourcePath = QtGui.QLineEdit()
        loadButton = QtGui.QPushButton("Load")
        self.ressourceListWidget = QtGui.QListView() 

        # Set properties
        loadButton.setMinimumHeight(38)
        self.ressourceListWidget.setModel(self.resourceModel)     
        self._populateRessourceModel()

        # Assign Widgets to Layouts
        loadLayout.addWidget(self.ressourceListWidget)
        loadLayout.addWidget(QtGui.QLabel("Ressource File:"))
        loadLayout.addWidget(self._sourcePath)
        loadLayout.addWidget(loadButton)

        # Assign Layouts to WidgetStack
        loadTab.setLayout(loadLayout)

        # Setup Signals
        self.ressourceSelectionModel.currentChanged.connect(self._updateSourcePath)
        loadButton.pressed.connect(self._createRigFromRessource)

        return loadTab

    def _updateSourcePath(self, current, previous):
        self._sourcePath.setText(self.resourceData[current.row()][1])

    def _populateRessourceModel(self):
        ressources = debugSplineIkOnRessource.RessourceData()
        self.resourceData = ressources.exposeValues()

        for ressource in self.resourceData :
            item = QtGui.QStandardItem(ressource[0])
            self.resourceModel.appendRow(item)

        self._sourcePath.setText(self.resourceData[0][1])
        self.ressourceSelectionModel = self.ressourceListWidget.selectionModel()

        selectionIndex = self.resourceModel.index(0,0) 
        self.ressourceSelectionModel.setCurrentIndex(selectionIndex ,QtGui.QItemSelectionModel.Select)

    def _createRigFromRessource(self):
        ikUtils = debugSplineIkOnRessource.Rig()
        ressourceKey = self.ressourceSelectionModel.currentIndex().data()

        ikUtils.build(ressourceKey)

    def _setFieldFromSelection(self, field):
        field.setText('')
        currentSelection = ikUtils.JointChain().getJointChainFromSelection()
        if len(currentSelection)<1:
            return

        field.setText(currentSelection[0].LongName)

    def _appendJointHierarchyFromRoot(self):
        rootName = str(self.jointRootField.text())

        if len(rootName) < 1:
            return

        rootList = mobu.FBComponentList()

        mobu.FBFindObjectsByName('{0}*'.format(rootName),
                                 rootList,
                                 False,
                                 False)

        if len(rootList) < 1:
            return

        chainList = ikUtils.JointChain().getJointChainFromRoot(rootName)
        if len(chainList) < 1:
            return

        joinList = []
        for joint in chainList:
            joinList.append(str(joint.LongName))

        self._writeJointListModel(joinList)

    def _appendSelectedJoints(self):
        currentSelection = ikUtils.JointChain().getJointChainFromSelection()
        if len(currentSelection)<1:
            return

        joinList = []
        for joint in currentSelection:
            joinList.append(str(joint.LongName))

        self._writeJointListModel(joinList)

    def _appendSelectedCurve(self, field):
        field.setText('')
        currentSelection = ikUtils.JointChain().getJointChainFromSelection()

        if len(currentSelection)<1:
            field.setText('')
            return

        if not isinstance(currentSelection[0], mobu.FBModelPath3D):
            field.setText('')
            return

        field.setText(currentSelection[0].LongName)

    def findIkSplineRootRoot(self, model):
        metaRootProperty = model.PropertyList.Find('ikSplineMetaRoot')
        if metaRootProperty is not None:
            return model

        parent = model.Parent
        if parent == None:
            return None
            
        metaRootProperty = parent.PropertyList.Find('ikSplineMetaRoot')
        if metaRootProperty is not None:
            return parent

        root = self.findIkSplineRootRoot(parent)
        if root is None:
            return parent
        else:
            return root

    def _appendSelectedIkRoot(self, field):
        field.setText('')
        currentSelection = list(ikUtils.JointChain().getJointChainFromSelection())

        self.splineIkRig = None
        self.rigSettings = None

        self.animIkControls = None
        self.animTweakerControls = None

        if len(currentSelection)<1:
            field.setText('')
            self.layeredModeCheckboxWidget.setChecked(0)
            return

        ikRigRoot = self.findIkSplineRootRoot(currentSelection[0])
        if ikRigRoot is None:
            field.setText('')
            self.layeredModeCheckboxWidget.setChecked(0)
            return

        currentSelection[0] = ikRigRoot

        metaRootProperty = currentSelection[0].PropertyList.Find('ikSplineMetaRoot')
        if metaRootProperty is None:
            field.setText('')
            self.layeredModeCheckboxWidget.setChecked(0)
            return

        layeredMotionPathProperty = currentSelection[0].PropertyList.Find('layeredMotionPathGroup')
        rigMode = layeredMotionPathProperty.GetSrcCount()

        if rigMode == 0:
            self.layeredModeCheckboxWidget.setChecked(0)
        else:
            self.layeredModeCheckboxWidget.setChecked(1)

        field.setText(currentSelection[0].LongName)
        self.splineIkRig = currentSelection[0]
        settingAttribute = self.splineIkRig.PropertyList.Find('rigSettings')
        self.rigSettings = json.loads(str(settingAttribute.Data))

        ikControlsSize = self.rigSettings['ikControlsSize']
        numberOfCurveController = self.rigSettings['numberOfCurveController']
        tweakerControlsSize = self.rigSettings['tweakerControlsSize']

        self.animIkSizeWidget.setValue(ikControlsSize)
        self.animTweakerSizeWidget.setValue(tweakerControlsSize)
        self.numberOfCurveControllerWidget.setValue(numberOfCurveController)

        self._extractAnimationController('tweakerGroup', 'tweaker')
        self._extractAnimationController('IkControlGroup', 'ik')

        constraintLink = self.splineIkRig.PropertyList.Find('ikConstraint')
        
        inputRigIkConstraintCheck = constraintLink.GetSrcCount()
        if inputRigIkConstraintCheck == 0:
            return

        inputRigIkConstraint = constraintLink.GetSrc(0)

        skipState = (self.toggleRigWidget.isChecked() ==  (not inputRigIkConstraint.Active))
        if self.toggleRigWidget.isDown() == False:
            if inputRigIkConstraint.Active == True:
                self.toggleRigWidget.setChecked(0)
                self.toggleRigWidget.setText("IkSpline Rig Enabled")

        if self.toggleRigWidget.isDown() == False:
            if inputRigIkConstraint.Active == False:
                self.toggleRigWidget.setChecked(1)
                self.toggleRigWidget.setText("IkSpline Rig Disabled")

    def _extractAnimationController(self, propertyLink, controlType):
        if self.splineIkRig is None:
            return

        controlLink = self.splineIkRig.PropertyList.Find(propertyLink)
        if controlLink is None:
            return

        groupMembersCount = controlLink.GetSrcCount()
        if groupMembersCount == 0:
            return

        if controlType == 'ik':
            self.animIkControls = []

        if controlType == 'tweaker':
            self.animTweakerControls = []

        for memberIndex in range(groupMembersCount):
            member = controlLink.GetSrc(memberIndex)
            controlNamesplit = member.Name.split('_')

            if controlType == 'ik':
                if 'Ctrl' in controlNamesplit[-1]:
                    self.animIkControls.append(member)

            if controlType == 'tweaker':
                if 'Tweaker' in controlNamesplit[-1]:
                    self.animTweakerControls.append(member)

    def _removeFromList(self):
        boneList = self.jointListModel.stringList() 
        if len(boneList)<1:
            return
        
        boneList = list(boneList)

        selectionIndex = self.jointListModel.index(0,0)
        
        boneList.remove(boneList[selectionIndex.row()])
        self.jointListModel.setStringList(boneList)

        jointListSelectionModel = self.ressourceListWidget.selectionModel()
        jointListSelectionModel.setCurrentIndex(selectionIndex ,QtGui.QItemSelectionModel.Select)

    def _writeJointListModel(self, joinList):
        boneList = self.jointListModel.stringList() 

        if len(boneList) < 1:
            self.jointListModel.setStringList(joinList)
        else:
            boneList = list(boneList)
            boneList.extend(joinList)
            self.jointListModel.setStringList(boneList)

    def _buildIkSplineRig(self):
        jointList = self._collectJointListFromWidget()
        if len(jointList)<1:
            return

        controlsSize =  self._collectControlsSize()
        visibilitySettings =  self._collectVisibilitySettings()
        ikSettingsValue = self._collectIkSettings()

        #-----------------------------------------------------------------------
        #Build splineIk Setup here
        #-----------------------------------------------------------------------   
        motionUtils = IkSplineCore.Setup()
        motionUtils.ikSplineSettings.numberOfCurveController = self.rigNumberOfCurveControllerWidget.value()
        motionUtils.ikSplineSettings.createFkControls = ikSettingsValue[3] 

        if len(self.anchorRootField.text()) > 0:
            motionUtils.ikSplineSettings.rigAnchor = self.anchorRootField.text()
        else:
            motionUtils.ikSplineSettings.rigAnchor = None

        motionUtils.ikSplineSettings.createTweaker= ikSettingsValue[0]
        motionUtils.ikSplineSettings.tweakerControlsSize = controlsSize[2]

        motionUtils.ikSplineSettings.fkControlsSize = controlsSize[0]
        motionUtils.ikSplineSettings.ikControlsSize = controlsSize[1]
   
        motionUtils.ikSplineSettings.showSplineMatrix = False
        motionUtils.ikSplineSettings.aimToNextJoint = ikSettingsValue[2] 
        
        motionUtils.ikSplineSettings.maintainChainLength = ikSettingsValue[1]

        motionUtils.ikSplineSettings.lockSelection = visibilitySettings[0]
        motionUtils.ikSplineSettings.hideDrivenJoints = visibilitySettings[1]
        motionUtils.ikSplineSettings.hidePath = visibilitySettings[2]
        
        ikControlData = motionUtils.buildRig(None,jointList)

    def _collectJointListFromWidget(self):
        boneList = self.jointListModel.stringList() 
        if len(boneList)<1:
            return mobu.FBModelList()

        jointList = mobu.FBModelList()
        for bone in boneList:
            jointChain = mobu.FBComponentList()       
            mobu.FBFindObjectsByName('{0}*'.format(str(bone)),
                                     jointChain,
                                     False,
                                     True)
            if len(jointChain) < 1 :
                continue

            jointList.append(jointChain[0])

        return jointList

    def _collectControlsSize(self):
        return [self.fkControlsSizeWidget.value(),
                self.ikControlsSizeWidget.value(),
                self.tweakerControlsSizeWidget.value()]

    def _collectVisibilitySettings(self):
        return [self.lockSelectionWidget.isChecked(),
                self.hideDrivenJointWidget.isChecked(),
                self.hidePathWidget.isChecked()]

    def _collectIkSettings(self):
        return [self.createTweakerWidget.isChecked(),
                self.maintainChainLengthWidget.isChecked(),
                self.aimToNextJointWidget.isChecked(),
                self.createFkWidget.isChecked()]

    def resizeTab(self, *args):
        if args[0] == 0:
            self.setupTab.setVisible(1)
        else:
            self.setupTab.setVisible(0)

        width = self.size().width()
        self.resize(width, 0)
