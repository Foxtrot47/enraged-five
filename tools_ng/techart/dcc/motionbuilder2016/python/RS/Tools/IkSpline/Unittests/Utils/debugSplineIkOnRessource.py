import inspect
import os
import sys 

import pyfbsdk as mobu


from RS.Tools.IkSpline import Core as  IkSplineCore
from RS.Tools.IkSpline import Settings as ikSettings
from RS.Tools.IkSpline import Utils as ikUtils

reload(ikUtils)
reload(ikSettings)
reload(IkSplineCore)

class RessourceSettings(ikSettings.IkSplineSettings):
    def __init__(self):
        ikSettings.IkSplineSettings.__init__(self)
        self.setDefaultValues()

    def setDefaultValues(self):
        self.fkControlsSize=2000.0
        self.ikControlsSize=450.0
        self.upVectorOffset=self.fkControlsSize*0.0095
        self.numberOfCurveController=5
        self.applyRotationOffset=True
        self.bindMethod='knotTranslation'#/'cluster'
        self.showSplineMatrix = False
        self.lockFirstIkTranslation = True
        self.fkOffset = [180,-90,90]
        self.aimToNextJoint = False
        self.lockSelection = True
        self.rigAnchor = None
        self.buildUpVector=False
        self.createTweaker=False
        self.maintainChainLength=True
        self.hidePath = False
        self.renderGeometry = []
        self.hideDrivenJoints = False
        self.tweakerControlsSize = 1.0
        self.debugKnotLength = 21.0
        self.createIntermediateGroup=True
        self.previewChainCurve = False
        self.ikPathName='splineIkPath1'
        self.connectTwistDriver = True
        self.createFkControls = True
        self.reverseChain = False

    def _exposeValues(self,cls):
        """
            internal method used by json.
        """
        return cls.__dict__

    def createSettingFromIndex(self, indexSetting):
        if indexSetting == 0:# --> chain
            self.fkControlsSize = 2000.0
            self.ikControlsSize = 450.0
            self.upVectorOffset = self.fkControlsSize*0.0095
            self.numberOfCurveController = 5
            self.lockSelection = False
            self.aimToNextJoint = True
            self.applyRotationOffset=False
            self.maintainChainLength = True
            self.tweakerControlsSize = 50
            self.showSplineMatrix = True
            '''
            self.bindMethod= 'cluster'
            '''

        if indexSetting == 1: # --> snake
            self.fkControlsSize = 320.0
            self.ikControlsSize = 80
            self.upVectorOffset = self.fkControlsSize*0.01
            self.debugKnotLength = 6.0
            self.numberOfCurveController = 9
            self.applyRotationOffset = True
            self.bindMethod='knotTranslation'
            self.showSplineMatrix = False
            self.lockFirstIkTranslation = False
            
            self.buildUpVector = False
            self.lockSelection = False
            self.maintainChainLength = False

        if indexSetting == 2: # --> snakeRig
            self.fkControlsSize = 7800.0
            self.ikControlsSize = 2200.0
            self.upVectorOffset = 75.0
            self.tweakerControlsSize = 1500
            self.numberOfCurveController = 8
            self.applyRotationOffset = True
            self.bindMethod= 'cluster'
            self.showSplineMatrix = False
            self.lockFirstIkTranslation = False
            
            self.buildUpVector = False
            self.lockSelection = False
            self.createTweaker = True
            self.maintainChainLength = True

            self.aimToNextJoint = True
            self.rigAnchor = 'mover'

        if indexSetting == 3: # --> rope
            self.fkControlsSize = 2000.0
            self.ikControlsSize = 300.0
            self.upVectorOffset = self.fkControlsSize*0.01
            self.numberOfCurveController = 7
            #self.bindMethod= 'cluster'

            self.lockFirstIkTranslation = False
            self.aimToNextJoint = True
            
            self.applyRotationOffset=False
            self.lockSelection = True
            self.rigAnchor = 'mover'
            self.buildUpVector = False
            self.createTweaker = True
            self.tweakerControlsSize = 180
            self.maintainChainLength = True
            self.hidePath = False
            self.showSplineMatrix = True
            self.hideDrivenJoints = True
            self.renderGeometry = ['p_cs_rope05x']
    
        if indexSetting == 4: # --> alligator
            self.buildUpVector=False
            self.lockSelection = True
            self.aimToNextJoint = True
            self.rigAnchor = 'SKEL_Pelvis'
            self.createTweaker = True
            self.tweakerControlsSize = 200

        
class RessourceData(object):
    def __init__(self):
        self.ressourceList = {'chain':['chainStart.fbx', 0],
                              'snakeRig':['snake_rigStart.FBX', 2],
                              'rope':['ropeStart.fbx', 3],
                              'aligator':['alligatorTail.fbx', 4]}
                 
        self.chainRootList = ['roll_jnt_02',
                              'snake_jnt01',
                              'SKEL_joint001',
                              'Main_p_cs_rope05x',
                              'SKEL_Tail0']
    
    def exposeValues(self):
        resourcePackage = {}
        resourceArray = []

        for key in self.ressourceList:
            resourcePackage[self.ressourceList[key][1]] = [key,self.ressourceList[key][0]]

        for key in sorted(resourcePackage.keys()):
            resourceArray.append(resourcePackage[key])
        
        return resourceArray


class Rig(object):
    def exposeRessource(self):
        ressourceValue = '''
        #-----------------------------------------------------------
        #RessourceList
        #-----------------------------------------------------------
        # chain         --> 'chainStart.fbx'
        # snake         --> 'snake_v001.fbx'
        # snakeRig      --> 'snake_rigStart.fbx'
        # rope          --> 'ropeStart.fbx'
        # aligator      --> 'alligatorTail.fbx'
        #-----------------------------------------------------------
        '''
        print ressourceValue

    def loadRessource(self, ressourceName):
        """
            This method open an fbx file from the ressource folder for Rigger
            Args:  
                ressourceName (str): file name to open
        """
        #Normally if the python module is loaded the file and its parent direntory exists...
        moduleDirectory = os.path.dirname(os.path.dirname(inspect.getfile(self.__class__ )))

        jointFilePath = os.path.normpath(os.path.join(moduleDirectory, 'ressources', ressourceName))     
        

        if os.path.isfile(jointFilePath)==False:
            raise ValueError('Invalid Ressource file {0} '.format(jointFilePath))
            return

        mobu.FBApplication().FileNew()
        jointFilePath = os.path.normpath(jointFilePath).replace('\\', '/')
        mobu.FBApplication().FileOpen(jointFilePath, False)

    def build(self, ressourceKey):
        ressourceData = RessourceData()
        
        if ressourceKey not in ressourceData.ressourceList:
            print 'Sorry {0} is not a supported ressource'
            return

        ressourceAsset = ressourceData.ressourceList[ressourceKey]
        ressourceToLoad = ressourceAsset[0]
        chainRootIndex = ressourceAsset[1]

        #-----------------------------------------------------------------------
        motionUtils = IkSplineCore.Setup()
        self.loadRessource(ressourceToLoad)

        #-----------------------------------------------------------------------
        #Setup splineIK options here
        #-----------------------------------------------------------------------
        settings = RessourceSettings()
        settings.createSettingFromIndex(chainRootIndex)
        motionUtils.ikSplineSettings = settings

        #-----------------------------------------------------------------------
        #Build splineIk Setup here
        #-----------------------------------------------------------------------        
        jointList = ikUtils.JointChain().getJointChainFromRoot(ressourceData.chainRootList[chainRootIndex]) 

        ikControlData = motionUtils.buildRig(None, jointList)
