import json
import os

from functools import partial
import pyfbsdk as mobu

from RS import Globals
from RS.Tools import UI

from RS.Tools.IkSpline.assetSetup  import ikFkParallelChainBuilder
reload(ikFkParallelChainBuilder)


from PySide import QtGui, QtCore


class IkFkRigListWidget(QtGui.QWidget):
    def __init__(self,
                 parent=None):
        super(IkFkRigListWidget, self).__init__(parent=parent)
        #Data
        self.ikFkRootArrayDataModel = QtGui.QStandardItemModel()
        self.rigDataArray = []

        self.animUtils =  ikFkParallelChainBuilder.AnimationManager()

        self.setupUi()

    def setupUi(self):
        #https://stackoverflow.com/questions/846684/a-listview-of-checkboxes-in-pyqt
        self.layout = QtGui.QVBoxLayout()

        refreshbutton = QtGui.QPushButton('Refresh Pelt Rig List')
        refreshbutton.setMinimumHeight(26)

        self.rigArrayWidget = QtGui.QListView()
        self.rigArrayWidget.setEditTriggers(0)

        self.layout.setContentsMargins(5, 0, 0, 5)
        self.layout.setSpacing(5)

        self.layout.addWidget(refreshbutton)
        self.layout.addWidget(self.rigArrayWidget)

        self.setLayout(self.layout)

        self.rigArrayWidget.setModel(self.ikFkRootArrayDataModel)

        self._populateRigDataModel()

        refreshbutton.pressed.connect(self._populateRigDataModel)

        self.ikFkRootArrayDataModel.itemChanged.connect(self._updateRigState)

        self.rigArrayWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def _updateRigState(self,
                        item):
        rigRootName = str(item.text())
        activateRig = False
        if item.checkState() == QtCore.Qt.CheckState.Checked:
            activateRig = True

        inputRootNode = self.animUtils.getRootFromName(rigRootName)

        if inputRootNode is None:
            return

        rigUtils = ikFkParallelChainBuilder.FkIkRigStructure()
        rigUtils.populateRigStructure(inputRootNode)

        rigUtils.toggleRigState(activateRig)

    def _populateRigDataModel(self):
        """
            Internal Method

            Synchronize QStandardItemModel with the visibility settings in the current scene.
        """
        self.ikFkRootArrayDataModel.clear()
        rootNames = self.animUtils.getIkFkChains()

        if len(rootNames)==0:
            return None

        for node in rootNames:
            item = QtGui.QStandardItem(node)
            item.setSizeHint(QtCore.QSize(100, 28))
            item.setCheckable(True)
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ikFkRootArrayDataModel.appendRow(item)

        self.rigArrayWidget.setCurrentIndex(self.ikFkRootArrayDataModel.index(0, 0))

        self._exposeRigState()

    def _exposeRigState(self):
        rootNames = []#item.text() for item in self.ikFkRootArrayDataModel]

        for index in xrange(self.ikFkRootArrayDataModel.rowCount()):
            currentIndex = self.ikFkRootArrayDataModel.index(index, 0)
            currentItem = self.ikFkRootArrayDataModel.itemFromIndex(currentIndex)

            rootNames.append(currentItem.text())

        inputChainRigs = []
        for node in rootNames:
            inputRootNode = self.animUtils.getRootFromName(node)

            if inputRootNode is None:
                continue

            rigUtils = ikFkParallelChainBuilder.FkIkRigStructure()
            rigUtils.populateRigStructure(inputRootNode)
            inputChainRigs.append(rigUtils)

        for rigIndex, node in enumerate(inputChainRigs):
            rigIsActive = node.collectRigState()

            checkValue = QtCore.Qt.CheckState.Checked
            if rigIsActive is False:
                checkValue = QtCore.Qt.CheckState.Unchecked

            currentIndex = self.ikFkRootArrayDataModel.index(rigIndex, 0)
            currentItem = self.ikFkRootArrayDataModel.itemFromIndex(currentIndex)
            currentItem.setCheckState(checkValue)


class IkFkChainBlendingWidget(QtGui.QWidget):
    """
        character selection widget
    """
    def __init__(self, toolBox):
        self.toolBox = toolBox

        # Main window settings
        super(IkFkChainBlendingWidget, self).__init__()

        self.setupUi()

    def setupUi(self):
        """
        Setup the UI
        """
        mainLayout = QtGui.QVBoxLayout()
        groupLayout = QtGui.QVBoxLayout()

        self.inputGroupBox =  QtGui.QGroupBox("Ik/Fk Chain Blending")
        self.weightSlider = QtGui.QSlider()
        self.weightSlider.setOrientation(QtCore.Qt.Horizontal)
        button = QtGui.QPushButton('Key Chain Weights')

        self.inputGroupBox.setLayout(groupLayout)
        groupLayout.setContentsMargins(6, 6, 6, 6)
        groupLayout.addWidget(self.weightSlider)
        groupLayout.addWidget(self.createSeparator())
        groupLayout.addWidget(button)

        mainLayout.addWidget(self.inputGroupBox)

        # Add widgets to Layouts
        self.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)

    def createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets
        targetLine = QtGui.QFrame()

        # Edit properties
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine


class IkFkChainMatchingWidget(QtGui.QWidget):
    """
        character selection widget
    """
    def __init__(self, toolBox):
        self.toolBox = toolBox
        self.controllerMatchingDataModel = QtGui.QStringListModel()

        # Main window settings
        super(IkFkChainMatchingWidget, self).__init__()

        self.setupUi()

    def setupUi(self):
        """
        Setup the UI
        """
        mainLayout = QtGui.QVBoxLayout()
        groupLayout = QtGui.QVBoxLayout()

        self.inputGroupBox =  QtGui.QGroupBox("Ik/Fk Chain Matching")

        self.inputGroupBox.setLayout(groupLayout)
        groupLayout.setContentsMargins(6, 6, 6, 6)

        groupLayout.addWidget(self._createMatchingOptions())
        groupLayout.addWidget(self._createToggleControls())

        mainLayout.addWidget(self.inputGroupBox)

        # Add widgets to Layouts
        self.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)

    def _createToggleControls(self):
        mainWidget =  QtGui.QWidget()
        mainLayout = QtGui.QHBoxLayout()

        button = QtGui.QPushButton('Snap Controllers')
        button.setMinimumHeight(32)

        self.setKeyOptionBox = QtGui.QCheckBox('')
        self.setKeyOptionBox.setMaximumHeight(18)
        self.setKeyOptionBox.setMaximumWidth(18)

        mainLayout.addWidget(button)
        #mainLayout.addWidget(self.setKeyOptionBox)

        mainWidget.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)

        button.pressed.connect(self._snapControllers)

        return mainWidget

    def _createMatchingOptions(self):
        mainWidget =  QtGui.QWidget()
        mainLayout = QtGui.QVBoxLayout()

        self.controllerMatchingBox = QtGui.QComboBox()
        mainLayout.addWidget(self.controllerMatchingBox)

        # Set properties
        self.controllerMatchingBox.setModel(self.controllerMatchingDataModel)
        self.controllerMatchingDataModel.setStringList(['Align Ik to Fk',
                                                        'Align Fk to Ik'])

        self.controllerMatchingBox.setCurrentIndex(0)

        mainWidget.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)

        return mainWidget

    def createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets
        targetLine = QtGui.QFrame()

        # Edit properties
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _snapControllers(self):
        if self.toolBox is None:
            return

        currentChains = self.toolBox._collectCurrentChain()
        if len(currentChains)==0:
            return

        chainNames = [chain.data() for chain in currentChains]

        setkeys = self.setKeyOptionBox.isChecked()
        fkToIk = True

        if self.controllerMatchingBox.currentIndex()==0:
            fkToIk = False

        self.toolBox.animUtils.collectComponents(chainNames)

        self.toolBox.animUtils.alignAnimationControllers(fkToIk=fkToIk,
                                                         setkeys=setkeys)


class IkFkChainVisibilityWidget(QtGui.QWidget):
    """
        character selection widget
    """
    def __init__(self, toolBox):
        self.toolBox = toolBox

        # Main window settings
        super(IkFkChainVisibilityWidget, self).__init__()

        self.setupUi()

    def setupUi(self):
        """
        Setup the UI
        """
        mainLayout = QtGui.QVBoxLayout()
        groupLayout = QtGui.QVBoxLayout()

        self.inputGroupBox =  QtGui.QGroupBox("Ik/Fk Chain Visibility")

        groupLayout.addWidget(self._createVisibilityOptions())
        groupLayout.addWidget(self.createSeparator())
        groupLayout.addWidget(self._createToggleControls())

        self.inputGroupBox.setLayout(groupLayout)
        groupLayout.setContentsMargins(6, 6, 6, 6)

        mainLayout.addWidget(self.inputGroupBox)

        # Add widgets to Layouts
        self.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)

    def _createToggleControls(self):
        mainWidget =  QtGui.QWidget()
        mainLayout = QtGui.QHBoxLayout()

        button = QtGui.QPushButton('Change controller visibility')
        self.setKeyOptionBox = QtGui.QCheckBox('')
        self.setKeyOptionBox.setMaximumHeight(18)
        self.setKeyOptionBox.setMaximumWidth(18)

        mainLayout.addWidget(button)
        #mainLayout.addWidget(self.setKeyOptionBox)

        mainWidget.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)

        button.pressed.connect(self._toggleVisibility)

        return mainWidget

    def _createVisibilityOptions(self):
        mainWidget =  QtGui.QWidget()
        mainLayout = QtGui.QHBoxLayout()

        self.fkOptionBox = QtGui.QCheckBox('Fk Controllers')
        self.ikOptionBox = QtGui.QCheckBox('Ik Controllers')

        mainLayout.addWidget(self.fkOptionBox)
        mainLayout.addWidget(self.ikOptionBox)
        mainLayout.addStretch(1)

        mainWidget.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)

        return mainWidget

    def createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets
        targetLine = QtGui.QFrame()

        # Edit properties
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _toggleVisibility(self):
        if self.toolBox is None:
            return

        currentChains = self.toolBox._collectCurrentChain()
        if len(currentChains)==0:
            return

        chainNames = [chain.data() for chain in currentChains]

        showFkControllers = self.fkOptionBox.isChecked()
        showIkControllers = self.ikOptionBox.isChecked()

        self.toolBox.animUtils.collectComponents(chainNames)

        self.toolBox.animUtils.exposeRigElements(showFkControllers=showFkControllers,
                                                 showIkControllers=showIkControllers)


class IkFkChainTool(UI.QtMainWindowBase):
    """
        main Tool UI
    """
    DEFAULT_WIDTH = 350

    def __init__(self, animationMode=False):
        """
            Constructor
        """
        self._toolName = "FK/IK chain Tool"
        self.ikFkRootArrayDataModel = QtGui.QStringListModel()
        self.animUtils =  ikFkParallelChainBuilder.AnimationManager()

        # Main window settings
        super(IkFkChainTool, self).__init__(title=self._toolName,
                                            dockable=True,
                                            size=(self.DEFAULT_WIDTH, 300))
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        self.mainWidget = QtGui.QWidget()
        mainLayout = QtGui.QVBoxLayout()
        self.ikFkListWidget = IkFkRigListWidget()
        self.ikFkVisibilityWidget = IkFkChainVisibilityWidget(self)
        self.ikFkMatchingWidget = IkFkChainMatchingWidget(self)
        self.ikFkBlendingWidget = IkFkChainBlendingWidget(self)

        # Create Widgets
        banner = UI.BannerWidget(self._toolName)
        plotbutton = QtGui.QPushButton('Plot selected Pelt Rig')
        plotbutton.setMinimumHeight(40)

        # Set properties

        # Assign Widgets to layouts
        mainLayout.addWidget(banner)
        mainLayout.addWidget(self.ikFkListWidget)
        mainLayout.addWidget(self.ikFkVisibilityWidget)
        mainLayout.addWidget(self.ikFkMatchingWidget)
        mainLayout.addWidget(self.ikFkBlendingWidget)
        mainLayout.addWidget(plotbutton)

        # Assign Layouts to WidgetStack
        self.mainWidget.setLayout(mainLayout)
        self.setCentralWidget(self.mainWidget)

        plotbutton.pressed.connect(self._plotAnimation)

    def createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets
        targetLine = QtGui.QFrame()

        # Edit properties
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _collectCurrentChain(self):
        currentItems = self.ikFkListWidget.rigArrayWidget.selectionModel().selectedIndexes()

        return currentItems

    def _plotAnimation(self):
        currentChains = self._collectCurrentChain()
        if len(currentChains)==0:
            return

        currentPeltRigs = [chain.data() for chain in currentChains]

        inputChainRigs = []

        for chain in currentPeltRigs:
            inputRootNode = self.animUtils.getRootFromName(chain)

            if inputRootNode is None:
                continue

            rigUtils = ikFkParallelChainBuilder.FkIkRigStructure()
            rigUtils.populateRigStructure(inputRootNode)
            inputChainRigs.append(rigUtils)

        for peltRig in inputChainRigs:
            peltRig.plotAnimation()

        self.ikFkListWidget._exposeRigState()


def Run(show=True):
    tool = IkFkChainTool()

    if show:
        tool.show()
    return tool
