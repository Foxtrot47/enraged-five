import json
import os

from functools import partial
import pyfbsdk as mobu

from RS import Globals
from RS.Tools import UI
from PySide import QtGui, QtCore


from RS.Tools.IkSpline.assetSetup  import peltSegmentBuilder
reload(peltSegmentBuilder)


from RS.Tools.IkSpline.Widgets import pickerWidget
reload(pickerWidget)

from RS.Utils import Scene


class ChainRigTool(UI.QtMainWindowBase):
    """
        main Tool UI
    """
    DEFAULT_WIDTH = 430

    def __init__(self):
        """
            Constructor
        """
        self._toolName = "Chain Rig Tool"

        # Main window settings
        super(ChainRigTool, self).__init__(title=self._toolName,
                                           dockable=True,
                                           size=(self.DEFAULT_WIDTH, 150))
        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        self.mainWidget = QtGui.QWidget()
        mainLayout = QtGui.QVBoxLayout()

        # Create Widgets
        banner = UI.BannerWidget(self._toolName)
        createRigbutton = QtGui.QPushButton('Create Chain Rig')
        createRigbutton.setMinimumHeight(40)

        self.startJointpickerWidget = pickerWidget.PickerWidget('Start Joint')

        self.endJointpickerWidget = pickerWidget.PickerWidget('End Joint')

        # Set properties

        # Assign Widgets to layouts
        mainLayout.addWidget(banner)

        mainLayout.addWidget(self.startJointpickerWidget)

        mainLayout.addWidget(self.endJointpickerWidget)

        mainLayout.addWidget(self.createSeparator())
        mainLayout.addWidget(self.createSpacingControls())

        mainLayout.addWidget(self.createSeparator())

        self.createFkHierarchyWidget = QtGui.QCheckBox('Create Fk hierarchy')
        self.createFkHierarchyWidget.setChecked(True)

        self.allowWorldOrientFkWidget = QtGui.QCheckBox('Set World Orientation on Fk hierarchy')
        self.allowWorldOrientFkWidget.setChecked(False)

        mainLayout.addWidget(self.createFkHierarchyWidget)

        mainLayout.addWidget(self.allowWorldOrientFkWidget)

        mainLayout.addWidget(self.createSeparator())
        mainLayout.addWidget(createRigbutton)

        # Assign Layouts to WidgetStack
        self.mainWidget.setLayout(mainLayout)
        self.setCentralWidget(self.mainWidget)

        createRigbutton.pressed.connect(self._createRig)

    def createSpacingControls(self):
        mainWidget = QtGui.QWidget()
        mainLayout = QtGui.QHBoxLayout()
        mainWidget.setLayout(mainLayout)

        mainLayout.setContentsMargins(0, 0, 0, 0)

        self.spacingAlongChainWidget = QtGui.QSpinBox()
        self.spacingAlongChainWidget.setMaximum(12)
        self.spacingAlongChainWidget.setMinimum(2)
        self.spacingAlongChainWidget.setValue(3)

        self.spacingAlongChainWidget.setMinimumWidth(90)

        mainLayout.addWidget(QtGui.QLabel('Spacing along chain'))
        mainLayout.addWidget(self.spacingAlongChainWidget)

        mainLayout.addStretch(0)

        return mainWidget

    def createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets
        targetLine = QtGui.QFrame()

        # Edit properties
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine

    def _createRig(self):
        startJointName = str(self.startJointpickerWidget.handleTextLine.text())
        endJointName = str(self.endJointpickerWidget.handleTextLine.text())

        rigUtils =  peltSegmentBuilder.PeltChainRig()

        rigUtils.reset()

        rigUtils.createFkHierarchy = self.createFkHierarchyWidget.isChecked()

        rigUtils.allowWorldOrientFk = self.allowWorldOrientFkWidget.isChecked()

        rigUtils.rigName = startJointName

        rigUtils.jointRootName = startJointName

        rigUtils.jointEndName = endJointName

        rigUtils.numberOfControllers = self.spacingAlongChainWidget.value()

        rigUtils.build()


def Run(show=True):
    tool = ChainRigTool()

    if show:
        tool.show()
    return tool
