import inspect
import json
import math
import os


from PySide import QtGui


import pyfbsdk as mobu
from RS import Globals

from RS.Utils import Scene

from RS.Tools.IkSpline import Core as  IkSplineCore

from RS.Utils import Namespace
from RS.Utils.Scene import Constraint

from RS.Utils import Path
from RS.Tools.IkSpline.assetSetup.weaponStrapBuilder.Rigging import Ribbon
from RS.Tools.IkSpline.assetSetup.weaponStrapBuilder.Rigging import Chain
from RS.Tools.weaponStrap import Core as weaponStrapCore
from RS.Tools.weaponStrap import SpaceSwitching
from RS.Core.AssetSetup.Sliders import addAssetSliders
from RS.Utils.Scene import RelationshipConstraintManager as relationConstraintManager

reload(Chain)
reload(Ribbon)
reload(weaponStrapCore)
reload(SpaceSwitching)


class WeaponHandle(object):
    DUMMY_TARGET_METADATA_NAME = 'dummyTarget'

    BIND_TIME_METADATA_NAME = 'bindTime'

    def __init__(self,
                 inputMarker,
                 weaponDummy):
        self.controlNode = None

        self.inputMarker = inputMarker

        self.weaponDummy = weaponDummy

        self.createControl()

    def createControl(self):
        mobu.FBSystem().Scene.Evaluate()
        targetMatrix = mobu.FBMatrix()
        self.inputMarker.Parent.GetMatrix(targetMatrix)

        dummyMatrix = mobu.FBMatrix()
        self.weaponDummy.GetMatrix(dummyMatrix)

        self.controlNode = mobu.FBModelNull('weaponHandle1')
        targetNode = mobu.FBModelNull('{}_dummyTarget1'.format(self.controlNode.Name))

        targetNode.Parent = self.controlNode
        targetNode.SetMatrix(dummyMatrix)

        self.controlNode.PropertyList.Find('Size').Data = 280

        self.controlNode.SetMatrix(targetMatrix)
        mobu.FBSystem().Scene.Evaluate()

        self.controlNode.Show = True
        self.controlNode.Selected = True

        self.createMetaRoot(self.controlNode)

        self.connectMetaData(targetNode,
                             self.DUMMY_TARGET_METADATA_NAME)

    def connectMetaData(self,
                        inputObject,
                        objectSetName):
        metaRootProperty = self.controlNode.PropertyList.Find(objectSetName)

        if metaRootProperty is None:
            return

        inputObject.ConnectDst(metaRootProperty)

    def createMetaRoot(self, 
                       inputNode):
        flagProperty = inputNode.PropertyCreate('{0}MetaRoot'.format(self.__class__.__name__), 
                                                mobu.FBPropertyType.kFBPT_charptr, 
                                                'String',
                                                False,
                                                True,
                                                None)

        flagProperty.Data = 'strapDynamicParent'

        inputNode.PropertyCreate(self.DUMMY_TARGET_METADATA_NAME,
                                 mobu.FBPropertyType.kFBPT_object,
                                 'Object',
                                 False,
                                 True,
                                 None)

        timeAttribute = inputNode.PropertyCreate(self.BIND_TIME_METADATA_NAME,
                                                 mobu.FBPropertyType.kFBPT_Time,
                                                 'Time',
                                                 True,
                                                 True,
                                                 None)

        timeAttribute.Data = mobu.FBSystem().LocalTime


class CoreBuilder(object):
    RIFFLE_ROOT_NAME = 'Gun_RifleStrap_01'

    ALTERNATE_SUFFIX = 'Alt1'

    def __init__(self,
                 isLeftStrap=False):
        self.motionUtils = IkSplineCore.Setup()
        self.motionPathDivisions = 5
        self.defaultFile = 'x:/rdr3/art/animation/resources/props/ingame/weapons/w_repeater_strap01.FBX'

        self.strapControlRootName = 'AAPStrap_Control'

        self.strapRootName = 'AAPStrap' 

        if isLeftStrap is False:
            self.rifleRootName = 'Gun_RifleStrap_01'
        else:
            self.rifleRootName = 'Gun_RifleStrap_01_Alt1'

        self.bodyAttachmentName = 'Body_Weapon_Constraint'

        self.splineIkRigRootName = 'strapIkPath1_constraint_Rig'

        self.strapRoot = None

        self.strapHullUtils = Ribbon.StrapHull()

    def loadAsset(self, assetFile):
        if len(assetFile)==0:
            assetFile=str(self.defaultFile)

        print 'Loading \n\t{0}'.format(assetFile)

        mobu.FBApplication().FileNew()
        assetFile = os.path.normpath(assetFile).replace('\\', '/')
        mobu.FBApplication().FileOpen(assetFile, False)

    def setupRigOptions(self, 
                        ikSplineUtils):
        ikSplineUtils.ikSplineSettings.ikPathName = 'strapIkPath1'
        ikSplineUtils.ikSplineSettings.numberOfCurveController = 5
        ikSplineUtils.ikSplineSettings.aimToNextJoint = True
        ikSplineUtils.ikSplineSettings.createFkControls = False
        ikSplineUtils.ikSplineSettings.createTweaker = False
        ikSplineUtils.ikSplineSettings.rigStorage = 'Dummy01'
        ikSplineUtils.ikSplineSettings.ikControlsSize = 350
        ikSplineUtils.ikSplineSettings.createFkControls = False
        ikSplineUtils.ikSplineSettings.maintainChainLength = False
        ikSplineUtils.ikSplineSettings.buildRigControl = True 
        ikSplineUtils.ikSplineSettings.hideDrivenJoints = True

    def findConstraints(self, control):
        """
            Find all constraints driving the list of control provided.

            Args:  
                rigControls (list of FBModel): input parent node.

            returns (list of FBConstraint).
        """
        drivingConstrains = []
        sourceCount = control.GetSrcCount()

        for sourceIndex in range(sourceCount):
            sourceObject = control.GetSrc(sourceIndex)

            if isinstance(sourceObject, mobu.FBConstraintRelation) is True:
                drivingConstrains.append(sourceObject)

        sceneConstraints = [constraint for constraint in Globals.Scene.Constraints]


        constrainedGroupIndex = 0
        for constraint in sceneConstraints:
            drivenCount = constraint.ReferenceGetCount(constrainedGroupIndex)
            
            for drivenIndex in range(drivenCount):
                drivenObject = constraint.ReferenceGet(constrainedGroupIndex, drivenIndex)

                if drivenObject is None:
                    continue

                if drivenObject.FullName == control.FullName:
                    drivingConstrains.append(constraint)

                    break

        drivingConstrains = list(set(drivingConstrains))

        return drivingConstrains 

    def purgePreviousRig(self):
        jointList = []
        controlCheck = Scene.FindModelByName(self.strapControlRootName)

        if controlCheck is None:
            print 'No control found:\n\t aborting - Intitial Rig Clean up:'
            return

        print 'Intitial Rig Clean up:'
        print '\tRemoving previous strap controls' 

        jointList = self.motionUtils.jointChain.getJointChainFromRoot(self.strapControlRootName)
        if len(jointList)>0:
            for joint in jointList:
                joint.FBDelete()

        self.strapRoot = Scene.FindModelByName(self.strapRootName)
        if self.strapRoot is not None:
            print '\tRemoving previous strap constraints' 
            constraintList = self.findConstraints(self.strapRoot)
            for constraint in constraintList:
                constraint.Weight = 0.0
                constraint.Active = False
                constraint.FBDelete()

        sceneConstraints = [constraint for constraint in Globals.Scene.Constraints]
        bodyRelations = []
        for constraint in sceneConstraints:
            if constraint.Name != self.bodyAttachmentName:
                continue
            bodyRelations.append(constraint)

        for constraint in bodyRelations:
            constraint.Weight = 0.0
            constraint.Active = False
            constraint.FBDelete()

    def getSplineIkRig(self,
                       rootName=''):
        self.splineIkRigRoot = None

        if len(rootName)==0:
            rootName = str(self.splineIkRigRootName)

        self.splineIkRigRoot = Scene.FindModelByName(rootName)

    def setup(self,
              ropeFile='',
              loadFile=True):
        if loadFile is True:
            self.loadAsset(ropeFile)

        self.purgePreviousRig()

        strapJoints = self.motionUtils.jointChain.getJointChainFromRoot(self.rifleRootName) 

        ikSplineUtils = IkSplineCore.Setup()

        ikSplineSettings = self.setupRigOptions(ikSplineUtils)

        splineIkRig = ikSplineUtils.buildRig(None, 
                                             strapJoints)

        if self.strapRoot is not None:
            splineIkRig.rigRoot.Parent = self.strapRoot

        self.splineIkRigRoot = splineIkRig.rigRoot

        return splineIkRig

    def createManipulators(self,
                           ropeFile='',
                           loadFile=True,
                           purgeRig=True,
                           debugHullRegion=False):
        if loadFile is True:
            self.loadAsset(ropeFile)

        if purgeRig is True:
            self.purgePreviousRig()
        else:
            self.strapRoot = Scene.FindModelByName(self.strapRootName)

        self.strapHullUtils.strapJointArray = self.motionUtils.jointChain.getJointChainFromRoot(self.rifleRootName)

        self.strapHullUtils.createChainSegments(debugHullRegion=debugHullRegion)

        self.strapHullUtils.prepareBindData()

        rigRoot = self.strapHullUtils.buildHullRig()

        if self.strapRoot is not None:
            rigRoot.Parent = self.strapRoot

        return rigRoot


class StrapBuilder(object):
    def __init__(self):
        self.splineIkRigRoot = None

        self.useAlternatePoint = False

        self.rigSettings = None

        self.splineIkRigRootName = 'strapIkPath1_constraint_Rig'

        self.chainBlender = Chain.ChainBlender()

        self.strenghtSlider = None

    def getSplineIkRig(self,
                       rootName=''):
        self.splineIkRigRoot = None

        if len(rootName)==0:
            rootName = str(self.splineIkRigRootName)

        self.splineIkRigRoot = Scene.FindModelByName(rootName)

    def buildChainBlender(self,
                          inputJointArray=[]):
        self.strapAnchors = []

        self.getSplineIkRig()

        self.chainBlender.useAlternatePoint = self.useAlternatePoint

        self.chainBlender.createWeightNode(self.splineIkRigRoot,
                                           inputJointArray=inputJointArray)

        self.chainBlender.buildNetwork()

        for constraint in self.builder.strapHullUtils.cornerHandleConstraints:
            self.chainBlender.connectMetaData(constraint,
                                              'handleConstraints')

    def buildSplineIkRig(self,
                         loadAnchors=False,
                         loadDefaultStrap=False):
        builder = CoreBuilder()
        if loadDefaultStrap is True:
            builder.loadAsset('')

        if loadAnchors is True:
            self.loadAttachPoints()

        self.getSplineIkRig()

        if self.splineIkRigRoot is not None:
            motionUtils = IkSplineCore.Setup()
            motionUtils.resetRig(self.splineIkRigRoot)

            self.splineIkRigRoot.FBDelete()

            self.splineIkRigRoot = None

        builder.createManipulators(loadFile=False)

        self.buildChainBlender()

        self.chainBlender.chainDriverGroup.PropertyList.Find('useAlternatePoint').SetLocked(True)

        self.chainBlender.chainDriverGroup.PropertyList.Find('chainBlenderMetaRoot').SetLocked(True)

    def createStrenghtSlider(self):
        self.strenghtSlider = mobu.FBModelNull('strengthSlider1')
        self.chainBlender.serialUtils.constraintDistribution = self.strenghtSlider

        attributeType = mobu.FBPropertyType.kFBPT_double

        sliderProperty = self.strenghtSlider.PropertyCreate(self.chainBlender.STRAP_SLIDER_NAMES[1],
                                                            attributeType,
                                                            "",
                                                            True,
                                                            True,
                                                            None)
        sliderProperty.SetMax(1.0)
        sliderProperty.SetMin(0.0)
        sliderProperty.SetAnimated(True)

    def createStrapRig(self,
                       loadDefaultStrap=True,
                       loadFile=True,
                       purgeRig=True,
                       createSlider=False,
                       isLeftStrap=False):
        if createSlider is True:
            addAssetSliders.Build(['ConstraintStrength',
                                   'ConstraintDistribution',
                                   'ConstraintDistributionBack'],
                                   'ClothPinning')

        self.builder = CoreBuilder(isLeftStrap=isLeftStrap)

        self.createStrenghtSlider()

        self.builder.createManipulators(loadFile=loadFile,
                                        purgeRig=purgeRig)

        self.getSplineIkRig()

        self.strenghtSlider.Parent = self.splineIkRigRoot

        self.buildChainBlender(inputJointArray=self.builder.strapHullUtils.strapJointArray)

        self.chainBlender.chainDriverGroup.PropertyList.Find('useAlternatePoint').SetLocked(True)

        self.chainBlender.chainDriverGroup.PropertyList.Find('chainBlenderMetaRoot').SetLocked(True)


class StrapRig(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    WEIGHT_PROPERTY_SUFFIX = 'Weight'

    ANCHOR_CONSTRAINT_TYPE = 'anchorConstraint'

    DEFAULT_STRAP_BONE_COUNT = 9

    HAND_HELPERS = ('PH_R_Hand',
                    'PH_L_Hand')

    BASE_RIFFLES_POINTS = ('PH_RifleInverted',
                           'PH_RifleInverted_Alt1')

    WEAPON_STRAP_ALIGN = 'WAPStrap_Control'

    WEAPON_GRIP_CONTROL = 'Gun_GripR_Control'

    WEAPON_MAIN_BONE = 'Gun_Main_Bone_Control'

    WEAPON_ROOT_CONTROL = 'Gun_Root_Control'

    WEAPON_ALTERNATE_GRIP = 'Gun_GripL_Control'

    DUMMY_NAME = 'Dummy01'

    MOVER_NAME = 'mover'

    DISTRIBUTION_DRIVER_NAME = 'ConstraintDistribution'

    BACK_DISTRIBUTION_DRIVER_NAME = 'ConstraintDistributionBack'

    def __init__(self):
        self.strapManager = None

        self.skipRelationCount = 0

        self.attachConstraints = []

        self.strapAnchorsWeightProperties = []

        self.characterNamespace = None

        self.dummy = None

        self.mover = None

        self.distributionSlider = None

        self.rootControl = None

        self.leftGripControl = None

        self.baseRigPoint = None

        self.rightGripControl = None

        self.mainBoneControl = None

    def collectStrapComponents(self):
        if len(self.strapManager.strapAnchors)==0:
            return

        self.collectParentWeightProperties()

        self.strapManager.chainBlender.strapPointArray = []

        for constraint in self.attachConstraints:
            pointData = Ribbon.StrapPointRig()
            pointData.name = constraint.Name
            pointData.collectFromConstraint(constraint)

            self.strapManager.chainBlender.strapPointArray.append(pointData)

            if self.strapManager.strapRigRoot is None:
                continue

            pointData.blendDriver.Parent = self.strapManager.strapRigRoot
            if pointData.skipRelationConstraintSetup is True:
                self.skipRelationCount += 1

    def extractAttachConstraints(self):
        self.attachConstraints = []

        if self.strapManager is None:
            return

        if not isinstance(self.strapManager, 
                          weaponStrapCore.StrapManager):
            return

        if self.strapManager.splineIkRigRoot is None:
             return

        controlLink = self.strapManager.splineIkRigRoot.PropertyList.Find(self.ANCHOR_CONSTRAINT_TYPE)

        if controlLink is None:
            return

        groupMembersCount = controlLink.GetSrcCount()
        if groupMembersCount == 0:
            return
        
        for memberIndex in range(groupMembersCount):
            member = controlLink.GetSrc(memberIndex)

            self.attachConstraints.append(member)

    def addBodyComponents(self):
        if len(self.strapManager.strapAnchors)==0:
            return

        for constraintIndex, constraint in enumerate(self.attachConstraints):
            constraint.ReferenceAdd(self.PARENT_NODE_INDEX,   
                                    self.strapManager.strapAnchors[constraintIndex+1])

    def collectParentWeightProperties(self):
        self.strapAnchorsWeightProperties = []


        self.strapAnchorsWeightProperties = ['{0}.{1}'.format(node.Name,
                                                              self.WEIGHT_PROPERTY_SUFFIX) for node in self.strapManager.strapAnchors]

    def resetParentWeights(self):
        self.collectParentWeightProperties()

        driverWeightProperties = [] 
        driverWeightProperties.extend(self.strapAnchorsWeightProperties)

        propertyToReset = []
        for constraint in self.attachConstraints:
            for component in constraint.PropertyList:
                if str(component.Name) in driverWeightProperties:
                    component.Data = 0.0

    def buildBaseRootConstraint(self,
                                propsNamespace):
        self.baseRigPoint = self.strapManager.getStrapMetadata(self.strapManager.strapRigRoot,
                                                               'baseRoot')

        self.alternateBaseRigPoint = self.strapManager.getStrapMetadata(self.strapManager.strapRigRoot,
                                                                        'alternateBaseRoot')

        self.pointConstraint = self.strapManager.getStrapRootConstraint(propsNamespace)
        if self.pointConstraint is not None:
            return

        self.pointConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        self.pointConstraint.Name = '{1} > {0}'.format(self.strapManager.strapManipulator.Name,
                                                       'strapConstraint')

        self.pointConstraint.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBConcatNamespace, 
                                                    propsNamespace)

        self.pointConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX, 
                                          self.strapManager.strapManipulator)

        self.pointConstraint.ReferenceAdd(self.PARENT_NODE_INDEX, 
                                          self.baseRigPoint)

        self.strapManager.chainBlender.connectMetaData(self.pointConstraint,
                                                       'baseRootConstraint')

        self.pointConstraint.Active = True
        self.strapManager.chainBlender.constraintFolder.ConnectSrc(self.pointConstraint)

    def attachToBasePoints(self,
                           propsNamespace):
        if self.strapManager is None:
            return

        if not isinstance(self.strapManager, 
                          weaponStrapCore.StrapManager):
            return

        rifflePoint = self.BASE_RIFFLES_POINTS[0]
        if self.strapManager.useAlternatePoint is True:
            rifflePoint = self.BASE_RIFFLES_POINTS[1]

        dummyNodeList = mobu.FBComponentList()
        sampleName = '{0}:{1}'.format(self.characterNamespace,
                                      rifflePoint)

        mobu.FBFindObjectsByName(sampleName,
                                 dummyNodeList,
                                 True,
                                 True)

        self.baseRigPoint = dummyNodeList[0]

        self.strapManager.chainBlender.connectMetaData(dummyNodeList[0],
                                                       'baseRoot')

        self.strapManager.getStrapManipulator()

        self.buildBaseRootConstraint(propsNamespace)

    def attachToBodyPoints(self):
        self.skipRelationCount = 0

        self.extractAttachConstraints()

        self.addBodyComponents()

        self.resetParentWeights()

        self.collectStrapComponents()

        if self.skipRelationCount == self.DEFAULT_STRAP_BONE_COUNT:
            return

        self.strapManager.chainBlender.buildNetwork()

    def setupSliderAttribute(self,
                             propsNamespace):
        self.strapWeightNode = self.strapManager.getStrapMetadata(self.strapManager.strapRigRoot,
                                                                  'ConstraintDistribution')

        if self.strapWeightNode is None:
            return

        relationConstraintName = '{}_Relation'.format(self.DISTRIBUTION_DRIVER_NAME)

        relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)

        relationConstraint.SetActive(True)

        sender = relationConstraint.AddSenderBox(self.distributionSlider)

        receiver = relationConstraint.AddReceiverBox(self.strapWeightNode)

        senderPropertyName = str(self.DISTRIBUTION_DRIVER_NAME)

        useAlternatePoint = self.strapManager.strapRigRoot.PropertyList.Find('useAlternatePoint').Data

        if useAlternatePoint is False:
            senderPropertyName = str(self.BACK_DISTRIBUTION_DRIVER_NAME)

        relationConstraint.ConnectBoxes(sender, 
                                        senderPropertyName, 
                                        receiver,
                                        self.DISTRIBUTION_DRIVER_NAME)

        strapConstraintFolder = self.strapManager.getStrapMetadata(self.strapManager.strapRigRoot,
                                                                   'strapConstraintFolder')

        sliderConstraint = relationConstraint.GetConstraint()
        sliderConstraint.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBConcatNamespace, 
                                                propsNamespace)

        if strapConstraintFolder:
            strapConstraintFolder.Items.append(sliderConstraint)

    def bindDistributionDriver(self,
                               inputCharacter,
                               propsNamespace):
        if self.characterNamespace is None:
            return None

        sliderName = str(self.DISTRIBUTION_DRIVER_NAME)

        useAlternatePoint = self.strapManager.strapRigRoot.PropertyList.Find('useAlternatePoint').Data

        if useAlternatePoint is False:
            sliderName = str(self.BACK_DISTRIBUTION_DRIVER_NAME)

        sliderList = mobu.FBComponentList()
        sampleName = '{0}:{1}'.format(self.characterNamespace,
                                      sliderName)

        mobu.FBFindObjectsByName(sampleName,
                                 sliderList,
                                 True,
                                 True)

        if len(sliderList) == 0:
            return None

        self.distributionSlider = sliderList[0]
        self.strapManager.chainBlender.connectMetaData(self.distributionSlider,
                                                       'targetCharacterSlider')

        self.setupSliderAttribute(propsNamespace)

    def attachToCharacter(self,
                          inputCharacter,
                          propsNamespace):
        if self.strapManager is None:
            return

        if not isinstance(self.strapManager, 
                          weaponStrapCore.StrapManager):
            return

        weaponIsValid = self.strapManager.collectWeaponData(propsNamespace)
        if not weaponIsValid:
            return

        self.characterNamespace = None

        if inputCharacter is not None:
            self.characterNamespace = self.strapManager.getPropsNamespace(inputCharacter)

        self.strapManager.collectFromScene(namespacePrefix=self.characterNamespace)

        rifflePoint = self.BASE_RIFFLES_POINTS[0]
        if self.strapManager.useAlternatePoint is True:
            rifflePoint = self.BASE_RIFFLES_POINTS[1]

        dummyNodeList = mobu.FBComponentList()
        sampleName = '{0}:{1}'.format(self.characterNamespace,
                                      rifflePoint)

        mobu.FBFindObjectsByName(sampleName,
                                 dummyNodeList,
                                 True,
                                 True)

        if len(dummyNodeList)==0:
            errorStream = 'Sorry Could not find {}.\nPlease check your reference/ Rig'.format(sampleName)
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)
            return None

        self.baseRigPoint = dummyNodeList[0]

        self.bindDistributionDriver(inputCharacter,
                                    propsNamespace)

        self.strapManager.chainBlender.connectMetaData(dummyNodeList[0],
                                                       'baseRoot')

        self.attachToBodyPoints()

        self.attachToBasePoints(propsNamespace)

        self.strapManager.chainBlender.connectMetaData(inputCharacter,
                                                       'targetCharacter')

        mobu.FBSystem().Scene.Evaluate()

        self.strapManager = None

    def findConstraints(self, control):
        """
            Find all constraints driving the list of control provided.

            Args:  
                rigControls (list of FBModel): input parent node.

            returns (list of FBConstraint).
        """
        drivingConstrains = []
        sourceCount = control.GetSrcCount()

        for sourceIndex in range(sourceCount):
            sourceObject = control.GetSrc(sourceIndex)

            if isinstance(sourceObject, mobu.FBConstraintRelation) is True:
                drivingConstrains.append(sourceObject)

        sceneConstraints = [constraint for constraint in Globals.Scene.Constraints]


        constrainedGroupIndex = 0
        for constraint in sceneConstraints:
            drivenCount = constraint.ReferenceGetCount(constrainedGroupIndex)
            
            for drivenIndex in range(drivenCount):
                drivenObject = constraint.ReferenceGet(constrainedGroupIndex, drivenIndex)

                if drivenObject is None:
                    continue

                if drivenObject.FullName == control.FullName:
                    drivingConstrains.append(constraint)

                    break

        drivingConstrains = list(set(drivingConstrains))

        return drivingConstrains 

    def createAnchorConstraint(self,
                               drivenNode,
                               parentNode,
                               preserveOffset=False):
        pointConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

        pointConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX, 
                                     drivenNode)

        pointConstraint.ReferenceAdd(self.PARENT_NODE_INDEX, 
                                     parentNode)

        if preserveOffset is True:
            pointConstraint.Snap()

        pointConstraint.Active = True

        pointConstraint.Name = '{0}_attach1'.format(drivenNode.Name)

        return pointConstraint

    def getPart(self,
                namePattern,
                inputNameSpace):
        sampleName = '{0}:{1}'.format(inputNameSpace,
                                      namePattern)

        outputControl = mobu.FBComponentList()
        mobu.FBFindObjectsByName(sampleName,
                                 outputControl,
                                 True,
                                 True)

        if len(outputControl)==0:
            return None

        return outputControl[0]

    def deleteConstraints(self,
                          inputNode,
                          deletePreviousConstraint):
        previousConstraints = self.findConstraints(inputNode)

        for constraint in previousConstraints:
            constraint.Active = False

            if deletePreviousConstraint is False:
                continue

            constraint.FBDelete()

    def disableConstraints(self,
                          inputNode):
        previousConstraints = self.findConstraints(inputNode)

        for constraint in previousConstraints:
            constraint.Active = False

    def getExtensionWithName(self,
                             inputCharacter,
                             inputName):
        extensions = [extension for extension in inputCharacter.CharacterExtensions \
                      if extension.Label == inputName]

        return extensions[0].GetExtensionObjectWithLabelName(inputName) 

    def getHandBones(self, 
                     attachNodeIndex,
                     inputCharacter,
                     addSkeletonBones=False):
        targetHandBones = []

        if addSkeletonBones is True:
            targetHandBones.append(inputCharacter.GetModel(mobu.FBBodyNodeId.kFBRightWristNodeId))
            targetHandBones.append(inputCharacter.GetModel(mobu.FBBodyNodeId.kFBLeftWristNodeId))

        targetHandBones.append(self.getExtensionWithName(inputCharacter,
                                                         self.HAND_HELPERS[attachNodeIndex]))

        return targetHandBones

    def attachStrapCorner(self,
                          inputWeaponNameSpace):
        sampleName = '{0}:{1}'.format(inputWeaponNameSpace,
                                      self.WEAPON_GRIP_CONTROL)

        weaponControl = mobu.FBComponentList()
        mobu.FBFindObjectsByName(sampleName,
                                 weaponControl,
                                 True,
                                 True)

        if len(weaponControl)==0:
            errorStream = 'Sorry Could not find {}.\nPlease check your weapon reference/Rig'.format(sampleName)
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)
            return None

        handleConstraints = self.strapManager.getStrapMetadata(self.strapManager.strapRigRoot,
                                                               'handleConstraints',
                                                               collectArray=True)

        for constraint in handleConstraints:
            constraint.ReferenceAdd(self.PARENT_NODE_INDEX, 
                                    weaponControl[0])

        mobu.FBSystem().Scene.Evaluate()

        anchorUtils = SpaceSwitching.Anchor()

        anchorUtils.strapRigRoot = self.strapManager.strapRigRoot

        for constraint in handleConstraints:
            targetDataArray = anchorUtils.collectParentSpaceData(constraint)

            for parentSpace in targetDataArray:
                parentSpace.weightProperty.Data = 0.0

            targetDataArray[0].weightProperty.Data = 100.0

        mobu.FBSystem().Scene.Evaluate()

        for constraint in handleConstraints:
            targetDataArray = anchorUtils.collectParentSpaceData(constraint)

            constraint.Active = False

            mobu.FBSystem().Scene.Evaluate()

            constraint.Snap()

            for parentSpace in targetDataArray:
                parentSpace.weightProperty.Data = 0.0

            targetDataArray[-1].weightProperty.Data = 100.0

            constraint.Active = True
            constraint.Lock = True

            mobu.FBSystem().Scene.Evaluate()

    def attachWeaponToCharacterComponents(self,
                                          inputCharacter,
                                          bodyPoint,
                                          inputWeaponNameSpace,
                                          propsNamespace,
                                          attachNodeIndex,
                                          disableConstraints=False,
                                          deletePreviousConstraint=False):
        targetHandBones = self.getHandBones(attachNodeIndex,
                                            inputCharacter)

        boneAnchor = targetHandBones[0]
        self.mover = self.strapManager.mover

        targetHandBones.append(self.mover)

        self.rightGripControl = self.getPart(self.WEAPON_GRIP_CONTROL,
                                             inputWeaponNameSpace)

        if self.rightGripControl is None:
            errorStream = 'Sorry Could not find {}.\nPlease check your weapon reference/Rig'.format(self.WEAPON_GRIP_CONTROL)
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)
            return None

        self.mainBoneControl = self.getPart(self.WEAPON_MAIN_BONE,
                                            inputWeaponNameSpace)

        if self.mainBoneControl is None:
            errorStream = 'Sorry Could not find {}.\nPlease check your weapon reference/Rig'.format(self.WEAPON_MAIN_BONE)
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)
            return None

        self.dummy = self.getPart(self.DUMMY_NAME,
                                  inputWeaponNameSpace)

        if self.dummy is None:
            errorStream = 'Sorry Could not find {}.\nPlease check your weapon reference/Rig'.format(self.DUMMY_NAME)
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)
            return None

        self.rootControl = self.getPart(self.WEAPON_ROOT_CONTROL,
                                        inputWeaponNameSpace)

        if self.rootControl is None:
            errorStream = 'Sorry Could not find {}.\nPlease check your weapon reference/Rig'.format(self.WEAPON_ROOT_CONTROL)
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)
            return None

        self.leftGripControl = self.getPart(self.WEAPON_ALTERNATE_GRIP,
                                            inputWeaponNameSpace)

        if self.leftGripControl is None:
            errorStream = 'Sorry Could not find {}.\nPlease check your weapon reference/Rig'.format(self.WEAPON_ALTERNATE_GRIP)
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)
            return None

        mobu.FBSystem().Scene.Evaluate()

        anchorUtils = SpaceSwitching.Anchor()

        anchorUtils.strapRigRoot = self.strapManager.strapRigRoot

        anchorUtils.storeWeaponBindpose(self.rootControl,
                                        self.mainBoneControl,
                                        self.rightGripControl,
                                        self.leftGripControl)

        weaponAnchorNode = self.rightGripControl

        if disableConstraints is True:
            self.disableConstraints(weaponAnchorNode)

        anchorConstraint = self.createAnchorConstraint(weaponAnchorNode,
                                                       bodyPoint)
                                                       

        for targetHandBone in targetHandBones:
            anchorConstraint.ReferenceAdd(self.PARENT_NODE_INDEX, 
                                          targetHandBone)

        self.strapManager.chainBlender.connectMetaData(weaponAnchorNode,
                                                       'targetWeapon')

        self.strapManager.chainBlender.connectMetaData(anchorConstraint,
                                                       'weaponConstraints')

        targetDataArray = anchorUtils.collectParentSpaceData(anchorConstraint)

        currentParent = None
        for targetData in targetDataArray:
             targetData.weightProperty.Data = 0.0
             if targetData.labelName == boneAnchor.LongName:
                currentParent = targetData
 
        if currentParent is not None:
            currentParent.weightProperty.Data = 100.0

        mobu.FBSystem().Scene.Evaluate()

        bodyPointIndex = 0
        anchorUtils.computeWeaponHandOffsets(self.mainBoneControl,
                                             weaponAnchorNode,
                                             anchorConstraint,
                                             bodyPointIndex)

        phNodeIndex = 1
        if attachNodeIndex == 0:
            #right hand attachment have no offset
            anchorUtils.computeWeaponHandOffsets(self.rightGripControl,
                                                 weaponAnchorNode,
                                                 anchorConstraint,
                                                 phNodeIndex)

        if attachNodeIndex == 1:
            anchorUtils.computeWeaponHandOffsets(self.leftGripControl,
                                                 weaponAnchorNode,
                                                 anchorConstraint,
                                                 phNodeIndex)

        self.strapManager.chainBlender.constraintFolder.ConnectSrc(anchorConstraint)
        anchorConstraint.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBConcatNamespace, 
                                                propsNamespace)

        for inputData in targetDataArray:
            inputData.disable()

        currentParent.enable()

        anchorUtils.storeConstraintOffsetBindPose(anchorConstraint)

        for inputData in targetDataArray:
            inputData.disable()

        currentParent.enable()

        return True

    def attachToWeapon(self,
                       inputWeapon,
                       propsNamespace,
                       attachNodeIndex,
                       connectWeapon=True):
        if self.strapManager is None:
            print 'Sorry  strapManager is not initialize'
            return

        if not isinstance(self.strapManager, 
                          weaponStrapCore.StrapManager):
            print 'Sorry  self.strapManager is not of type weaponStrapCore.StrapManager'
            return

        weaponIsValid = self.strapManager.collectWeaponData(propsNamespace)
        if not weaponIsValid:
            print 'Sorry weapon Is not Valid'
            print self.strapManager
            return

        self.skipRelationCount = 0

        self.pointConstraint = self.strapManager.getStrapRootConstraint(propsNamespace)

        if self.pointConstraint is None:
            errorStream = 'Please Attach your weapon to a character first.' 
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)
            return

        sampleName = '{0}:{1}'.format(inputWeapon,
                                      self.WEAPON_STRAP_ALIGN)

        weaponAlign = mobu.FBComponentList()
        mobu.FBFindObjectsByName(sampleName,
                                 weaponAlign,
                                 True,
                                 True)

        if len(weaponAlign)==0:
            errorStream = 'Sorry Could not find {}.\nPlease check your weapon reference/Rig'.format(sampleName)
            QtGui.QMessageBox.information(None,
                                          'Missing Element', 
                                          errorStream)
            return

        inputCharacter = self.strapManager.getStrapMetadata(self.strapManager.chainBlender.chainDriverGroup,
                                                            'targetCharacter')

        if inputCharacter is None:
            errorStream = 'Could not find character from contact point namespace.' 
            QtGui.QMessageBox.information(None,
                                          'Missing Character', 
                                          errorStream)
            return

        bodyPoint = self.pointConstraint.ReferenceGet(self.PARENT_NODE_INDEX, 0)

        if connectWeapon:
            weaponState = self.attachWeaponToCharacterComponents(inputCharacter,
                                                                 bodyPoint,
                                                                 inputWeapon,
                                                                 propsNamespace,
                                                                 attachNodeIndex)

            if weaponState is None:
                return

            self.pointConstraint.Lock = False
            self.pointConstraint.ReferenceAdd(self.PARENT_NODE_INDEX, 
                                              weaponAlign[0])

            for attribute in self.pointConstraint.PropertyList:
                if 'Offset T' in attribute.Name or 'Offset R' in attribute.Name:
                    attribute.Data = mobu.FBVector3d()

            self.pointConstraint.Lock = True

            anchorUtils = SpaceSwitching.Anchor()

            anchorUtils.strapRigRoot = self.strapManager.strapRigRoot

            targetDataArray = anchorUtils.collectParentSpaceData(self.pointConstraint)

            if targetDataArray is None:
                return

            for targetData in targetDataArray:
                 targetData.weightProperty.Data = 0.0
     
            targetDataArray[-1].weightProperty.Data = 100.0

        self.attachStrapCorner(inputWeapon)

        self.strapManager = None

    def attachToHandle(self,
                       inputHandle,
                       anchorConstraint,
                       weaponNode,
                       strapRigRoot):
        if inputHandle is None:
            return

        if anchorConstraint is None:
            return

        anchorUtils = SpaceSwitching.Anchor()

        initialTargetDataArray = anchorUtils.collectParentSpaceData(anchorConstraint)

        handleIsAlreadyAttached = False

        phNodeIndex = 0
        currentDriver = None

        matrixData = anchorUtils.collectWeaponWorldspaceMatrix(weaponNode)

        for index, inputData in enumerate(initialTargetDataArray):
            if inputData.labelName!= inputHandle.LongName:
                continue

            handleIsAlreadyAttached = True
            phNodeIndex = index

        for inputData in initialTargetDataArray:
            if inputData.weightProperty.Data < 99.0:
                continue

            currentDriver = inputData

        if not handleIsAlreadyAttached:
            phNodeIndex = 0

            anchorConstraint.ReferenceAdd(self.PARENT_NODE_INDEX, 
                                          inputHandle)

            targetDataArray = anchorUtils.collectParentSpaceData(anchorConstraint)

            for index, inputData in enumerate(targetDataArray):
                inputData.disable()

                if inputData.labelName!= inputHandle.LongName:
                    continue

                phNodeIndex = index

            targetDataArray[phNodeIndex].enable()

            anchorUtils.maintainWeaponInWorldspace(matrixData)

            targetDataArray[phNodeIndex].disable()

            currentDriver.enable()

            mobu.FBSystem().Scene.Evaluate()

            anchorUtils.appendHandleToConstraintManifest(targetDataArray[phNodeIndex],
                                                         strapRigRoot)

    def createWeaponHandle(self,
                           ikControlMarker,
                           weaponDummy):
        if ikControlMarker is None:
            return

        weaponHandle = WeaponHandle(ikControlMarker,
                                    weaponDummy)