import inspect
import os
import tempfile

import pyfbsdk as mobu
import copy

from RS import Globals
from RS.Utils import Scene
from RS.Core.AssetSetup.Sliders import addAssetSliders

from RS.Tools.IkSpline import Core as  IkSplineCore
from RS.Tools.IkSpline import Rigging as ikRigging
from RS.Tools.IkSpline import Settings as ikSettings
from RS.Tools.IkSpline import Utils as ikUtils


class Build():
    def __init__(self):
        self.motionUtils = IkSplineCore.Setup()
        self.motionPathDivisions = 6

    def prepareMotionPathSettings(self,
                                  rigStructureSettings,
                                  aimToNextJointInMotionPath,
                                  rigStorage):
        #motionPathSettings = ikSettings.IkSplineSettings()
        motionPathSettings = self.motionUtils.createIkMotionPathRigOptions(aimToNextJointInRig=aimToNextJointInMotionPath)

        motionPathSettings.ikPathName = 'motionPath1'
        motionPathSettings.ikControlsSize = rigStructureSettings.ikSplineSettings.ikControlsSize * 1.2
        motionPathSettings.createIntermediateGroup = True
        motionPathSettings.isMotionPath = True

        motionPathSettings.createTweaker=False
        motionPathSettings.reverseList = True

        motionPathSettings.hasSnakeCompensationLayer = True
        motionPathSettings.rigStorage = rigStorage

        return motionPathSettings

    def setupMotionPath(self,
                        rigStructureSettings,
                        aimToNextJointInMotionPath):
        motionPathSettings = self.prepareMotionPathSettings(rigStructureSettings,
                                                            aimToNextJointInMotionPath,
                                                            'A_C_Snakecoral_1')
        
        motionPathSettings.reverseList = True
        motionPathSettings.rigAnchor = 'mover' 
        self.motionUtils.ikSplineSettings =  motionPathSettings

        motionData = self.motionUtils.createMotionPath(rigStructureSettings.ikControlsData.ikControls,
                                                       self.motionPathDivisions,
                                                       ikRigRootToConnect=rigStructureSettings.splineIkRig.LongName)

        return motionData

    def setupRigOptions(self,
                        aimToNextJointInRig=False,
                        addExtensionToCharacter=None,
                        rigStorage=None,
                        useMhJoints=False):
        ikSplineSettings = ikSettings.IkSplineSettings()

        ikSplineSettings.hasSnakeCompensationLayer = True
        ikSplineSettings.createTweaker = True
        ikSplineSettings.tweakerControlsSize = 50
        ikSplineSettings.ikControlsSize = 250 # 7.2
        ikSplineSettings.numberOfCurveController = 12
        ikSplineSettings.rigAnchor = 'mover' 

        ikSplineSettings.aimToNextJoint = aimToNextJointInRig 
        ikSplineSettings.createFkControls = False 
        ikSplineSettings.maintainChainLength = True 
        ikSplineSettings.bindMethod = 'knotTranslation' # other options 'knotTranslation'/'cluster'
        ikSplineSettings.excludeThreshold = 0.001
        ikSplineSettings.createIntermediateGroup = False

        ikSplineSettings.reverseChain = True

        if useMhJoints is False:
            ikSplineSettings.skelRootArray = ['SKEL_ROOT', 
                                              'SKEL_Spine_Root',
                                              'SKEL_Pelvis']

            ikSplineSettings.skelRootAnchor = 'SKEL_Tail0_Tweaker_bind1'
        else:
            ikSplineSettings.skelRootArray = ['SKEL_ROOT']

            ikSplineSettings.skelRootArray = ['SKEL_ROOT', 
                                              'MH_Spine_Root',
                                              'MH_Pelvis']

            ikSplineSettings.skelRootAnchor = 'MH_Tail0_Tweaker_bind1'

        ikSplineSettings.rigStorage = rigStorage 

        ikSplineSettings.rigAnchor = 'mover' 

        if addExtensionToCharacter is not None:
            currentChar = mobu.FBApplication().CurrentCharacter
            ikSplineSettings.addExtensionToCharacter = currentChar.FullName

        self.motionUtils.ikSplineSettings = ikSplineSettings

    def loadAsset(self,assetFile):
        mobu.FBApplication().FileNew()
        assetFile = os.path.normpath(assetFile).replace('\\', '/')
        mobu.FBApplication().FileOpen(assetFile, False)

    def rigSplineLayer(self, 
                       aimToNextJointInRig=False,
                       startIndex=0,
                       addExtensionToCharacter=None,
                       rigStorage=None,
                       reverseCurve=False,
                       useMhJoints=False):
        self.setupRigOptions(aimToNextJointInRig, 
                             addExtensionToCharacter=addExtensionToCharacter,
                             rigStorage=rigStorage,
                             useMhJoints=useMhJoints)

        jointList, jointNames = ikRigging.SetupCoralSnake().prepareSnakeIkRig(startIndex=startIndex,
                                                                              reverseCurve=reverseCurve,
                                                                              useMhJoints=useMhJoints)

        ikSplineStructureSettings = self.motionUtils.buildRig(None, jointList)

        ikControlData = ikSplineStructureSettings.ikControlsData
        
        ikControls = ikControlData.ikControls
        ikControlsName = []

        for control in ikControls:
            ikControlsName.append(control.LongName)
            
        rigStructure = ikSplineStructureSettings.splineIkRig.LongName
        constraintFolder = ikSplineStructureSettings.constraintFolder.LongName

        return ikSplineStructureSettings

    def expandTempPath(self, inputFilePath):
        """
            Remove tilde from local path in order to have a stable motionbuilder import.
           
            Args:  
                inputFilePath (str): source file path.

            returns (str)
        """
        filePath =  inputFilePath.replace('\\','/')

        if '~' in filePath:
            resultPath = ''
            pathSplit = filePath.split('/')
            startIndex = 0

            for tildeIndex in range(len(pathSplit)):
                if '~' in pathSplit[tildeIndex]:
                    startIndex = tildeIndex
                    break

            #os.path.expanduser doesn't seem to work on 'c:/cedric~.baz/data/temp/tempExportFile.fbx'
            resultPath = os.path.expanduser('~')
            resultPath =  resultPath.replace('\\','/')

            fixedPath = os.path.join(*pathSplit[startIndex+1:])
            resultPath = os.path.join(resultPath, fixedPath)

            filePath = os.path.realpath(resultPath)
        
        filePath = filePath.replace('\\','/')

        return filePath

    def reloadTempAsset(self, filePath):
        fileOptions = mobu.FBFbxOptions(False)
        fileOptions.ShowFileDialog = False
        fileOptions.ShowOptionsDialog = False

        mobu.FBApplication().FBXFileName = filePath
        mobu.FBApplication().FileSave(filePath, fileOptions)

        mobu.FBApplication().FileNew()
        mobu.FBApplication().FileOpen(filePath, False)

    def correctSpineOrients(self,
                            rigStructureSettings,
                            startIndex,
                            useMhJoints):
        ikControls = rigStructureSettings.ikControlsData.ikControls
        ikControlsName = []

        for control in ikControls:
            ikControlsName.append(control.LongName)

        rigStructure = rigStructureSettings.splineIkRig.LongName
        constraintFolder = rigStructureSettings.constraintFolder.LongName

        splineData = [ikControlsName, 
                      rigStructure, 
                      constraintFolder]

        ikRigging.SetupCoralSnake().correctSpineOrients(splineData, 
                                                        rigStructureSettings,
                                                        startIndex=startIndex,
                                                        useMhJoints=useMhJoints)

    def revertMhPrefix(self,
                       ikSplineStructureSettings):
        ikControlData = ikSplineStructureSettings.ikControlsData
        
        ikControls = ikControlData.ikControls
        ikControlsName = []

        for control in ikControls:
            control.Name = control.Name .replace('MH', 
                                                 'SKEL')

        for nodeData in ikSplineStructureSettings.tweakerList:
            nodeData[1].Name = nodeData[1].Name.replace('MH', 
                                                        'SKEL')

    def sanitizeMhHeadBone(self):
        MH_Head = Scene.FindModelByName('MH_Head')

        MH_Spine13 = Scene.FindModelByName('MH_Spine13')

        SKEL_Head = Scene.FindModelByName('SKEL_Head')


        if MH_Head is None or MH_Head == '':
            MH_Head = copy.copy(MH_Spine13)
            MH_Head.Name = 'MH_Head'

        MH_Head.Parent = MH_Spine13.Parent

        targetMatrix = mobu.FBMatrix()

        SKEL_Head.GetMatrix(targetMatrix)

        MH_Head.SetMatrix(targetMatrix) 

        tailNub = Scene.FindModelByName('SKEL_Tail15_NUB')

        MH_tailNub = Scene.FindModelByName('MH_Tail15_NUB')

        MH_Tail15 = Scene.FindModelByName('MH_Tail15')

        if MH_tailNub is None or MH_tailNub == '':
            MH_tailNub = copy.copy(MH_Tail15)
            MH_tailNub.Name = 'MH_Tail15_NUB'

        tailNub.GetMatrix(targetMatrix)

        MH_tailNub.SetMatrix(targetMatrix) 

    def cleanupSkelPelvisCyclicConstraint(self):
        for constraint in Globals.Scene.Constraints:
            if 'SkelRoot_Pelvis' not in constraint.Name:
                continue

            constraint.Active = False

            constraint.FBDelete()

    def customSetup(self,
                    assetFile,
                    forceReload=False,
                    addMotionPathLayer=False,
                    correctSpine=True,
                    aimToNextJointInRig=True,
                    startIndex=0,
                    addExtensionToCharacter=None,
                    rigStorage=None,
                    reverseCurve=True,
                    useMhJoints=False):
        if forceReload is True:
            self.loadAsset(assetFile)

        if useMhJoints is True:
            self.sanitizeMhHeadBone()

            self.cleanupSkelPelvisCyclicConstraint()

        rigStructureSettings = self.rigSplineLayer(aimToNextJointInRig=aimToNextJointInRig,
                                                   startIndex=startIndex,
                                                   addExtensionToCharacter=addExtensionToCharacter,
                                                   rigStorage=rigStorage,
                                                   reverseCurve=reverseCurve,
                                                   useMhJoints=useMhJoints)

        #Connect MH expression slider
        if useMhJoints is True:
            addAssetSliders.Build(['GlobalSpaceSwitch'],
                                   'gloablSpace')

            slider = mobu.FBFindModelByLabelName('GlobalSpaceSwitch')
            if slider is not None:
                slider.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0.0, 1.0, 0.0)

            oldSliderParent = mobu.FBFindModelByLabelName('GlobalSpaceSwitch_PARENT')
            if oldSliderParent is not None:
                oldSliderParent.FBDelete()

        self.correctSpineOrients(rigStructureSettings,
                                 startIndex,
                                 useMhJoints)

        if useMhJoints is True:
            for node in rigStructureSettings.jointObjectArray:
                node.Visibility = False

            self.revertMhPrefix(rigStructureSettings)

            MH_tailNub = Scene.FindModelByName('MH_Tail15_NUB')

            MH_tailNub.Parent = rigStructureSettings.splineIkRig

        ikUtils.AnimUtils().consolidateGlueComponents(rigStructureSettings.splineIkRig)

        return rigStructureSettings
