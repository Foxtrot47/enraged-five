import os
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Config


class IkSplineSettingsWidget(QtGui.QWidget):
    """
        main Tool UI
    """
    def __init__(self,
                 toolBox):
        """
            Constructor
        """
        self.toolBox = toolBox

        # Main window settings
        super(IkSplineSettingsWidget, self).__init__()
        self.setupUi()

    def createSettingControls(self,
                              targetLayout,
                              inputRange,
                              defaultValue,
                              spinnerCaption,
                              spinnerType='integer'):
        spinnerLayout = QtGui.QHBoxLayout()
        spinnerGroup = QtGui.QWidget()

        if spinnerType == 'integer':
            spinnerWidget = QtGui.QSpinBox(self)
        else:
            spinnerWidget = QtGui.QDoubleSpinBox(self)

        spinnerWidget.setRange(inputRange[0],
                               inputRange[1])

        spinnerWidget.setValue(defaultValue)

        spinnerLayout.addWidget(QtGui.QLabel(spinnerCaption))
        spinnerLayout.addWidget(spinnerWidget)

        spinnerGroup.setLayout(spinnerLayout)

        targetLayout.addWidget(spinnerGroup)

        spinnerLayout.setContentsMargins(0, 0, 0, 0)

        return spinnerWidget

    def setupUi(self):
        mainLayout = QtGui.QVBoxLayout()

        self.numberOfCurveControllerWidget = self.createSettingControls(mainLayout,
                                                                        (3,400),
                                                                        5,
                                                                        "Curve controller Count")

        mainLayout.addWidget(self._createSeparator())

        self.animIkSizeWidget = self.createSettingControls(mainLayout,
                                                           (0.0001,10000.0),
                                                           500.0,
                                                           "Ik controller size",
                                                           spinnerType='double')

        mainLayout.addWidget(self._createSeparator())

        self.animTweakerSizeWidget = self.createSettingControls(mainLayout,
                                                                (0.0001,10000.0),
                                                                500.0,
                                                                "Tweaker controller size",
                                                                spinnerType='double')

        mainLayout.setContentsMargins(0, 0, 0, 0)
        self.setMinimumWidth(310)


        #Populates character list
        self.setLayout(mainLayout)

    def _createSeparator(self):
        """
            Creates a line separator.

            returns (QFrame).
        """
        # Create Widgets 
        targetLine = QtGui.QFrame()

        # Edit properties 
        targetLine.setFrameShape(QtGui.QFrame.HLine)
        targetLine.setFrameShadow(QtGui.QFrame.Sunken)

        return targetLine
