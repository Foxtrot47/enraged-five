import pyfbsdk as mobu
import math


class SplineWeights(object):
    NODE_ATTRIBUTES = ('weight0',
                       'weight1',
                       'weight2',
                       'weight3',
                       'pointIndex0',
                       'pointIndex1',
                       'pointIndex2',
                       'pointIndex3')

    def __init__(self):
        self.weight0 = 0.0
        self.weight1 = 0.0
        self.weight2 = 0.0
        self.weight3 = 0.0

        self.pointIndex0 = 0
        self.pointIndex1 = 0
        self.pointIndex2 = 0
        self.pointIndex3 = 0

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_ATTRIBUTES:
            currentValue = getattr(self, attribute)

            reportData += '\n\t{0}:{1}'.format(attribute,
                                                 currentValue)

        return reportData


class SplinePathSamples(object):

    FRAME_POINTS = ((1, 0, 0),
                    (1, 0, 1),
                    (0, 0, 1),
                    (0, 1.0, 0),
                    (1, 0, 0))

    START_SEGMENT = ((1.0, 0.00014636162144943121, 0.0),
                     (1.0, 0.00041139423796120872, 0.14045088700607983),
                     (0.98722107882451482, 0.0, 0.29232723275939504),
                     (0.9604258325414794, 0.0011329003374821551, 0.43102278319014081),
                     (0.92336233521597055, 0.0020357029415641881, 0.57482905857746103),
                     (0.86170732865169131, 0.0062451601426540227, 0.70491884793945969),
                     (0.7726652501841309, 0.013524566317613392, 0.82351159148556607),
                     (0.64697901481952391, 0.030222024930531607, 0.90185938783947817),
                     (0.50334074741554258, 0.059273191156767116, 0.92439435749790055),
                     (0.36345425383024821, 0.10430526438296171, 0.89579560660882807),
                     (0.25, 0.16681302828811609, 0.83333333333333337))

    INNER_SEGMENT = ((0.25, 0.16681302828811609, 0.83333333333333337),
                     (0.1860720008587895, 0.2170811412868793, 0.78291667134947041),
                     (0.13306794759538562, 0.26852515719959136, 0.73097271987921186),
                     (0.099484494403330126, 0.32340574815154643, 0.67489487387001512),
                     (0.068942597076600359, 0.3878286871941744, 0.60505444209283843),
                     (0.053890669503920532, 0.44897531602156876, 0.53566782987739947),
                     (0.048204371238845446, 0.50781242057692122, 0.46547693325928868),
                     (0.053587215261416563, 0.56508024374641608, 0.39168022965654065),
                     (0.073075479057989112, 0.61504606030915909, 0.31542854516574209),
                     (0.11149592595373146, 0.65142496389520077, 0.23451757520821453),
                     (0.16666666666666666, 0.66678863468454119, 0.16666666666666669))

    INNER_SEGMENT_A = ((0.16666666666666666, 0.66678863468454119, 0.16666666666666669),
                       (0.23647444074455462, 0.65534570986630947, 0.10799139528949001),
                       (0.31021198839705821, 0.61806111595065782, 0.074165858691026987),
                       (0.38443403243767921, 0.57023397464992953, 0.05149072965759724),
                       (0.45513645890541221, 0.51517878051248245, 0.043515088970252691),
                       (0.52497838623951343, 0.45976417265907932, 0.039819215122269193),
                       (0.59197866715506819, 0.39908608966595122, 0.047354848528811999),
                       (0.65675097245444269, 0.34027461793925751, 0.063622149337636463),
                       (0.71963567893259794, 0.27924567175891218, 0.083896455419602417),
                       (0.78091640292032172, 0.22136597510483413, 0.12167963864330246),
                       (0.83333333333333326, 0.16669106027024164, 0.16666666666666666))

    INNER_SEGMENT_B = ((0.83333333333333326, 0.16669106027024164, 0.16666666666666666),
                       (0.87566299092286803, 0.12558027282705947, 0.21426824072864958),
                       (0.91167706433042273, 0.087489951845823297, 0.27121147417865754),
                       (0.93572361725904518, 0.061419061751318602, 0.340447315947916),
                       (0.95308288562539156, 0.037970112160497768, 0.40915609458847663),
                       (0.96052720193069563, 0.022372679303586551, 0.48514954884299222),
                       (0.955088534546083, 0.012302850316003309, 0.56005601669717642),
                       (0.94400921624516598, 0.0044542806626882573, 0.63543539938437121),
                       (0.92017717892144824, 0.0016504206328608491, 0.7080622031229471),
                       (0.88219370528875751, 0.0, 0.77519935005827001),
                       (0.83333333333333337, 0.0, 0.83333333333333326))

    INNER_SEGMENT_C = ((0.83333333333333337, 0.0, 0.83333333333333326),
                       (0.77519839676926749, 0.0, 0.88219473918343283),
                       (0.70806000542924108, 0.0016817736105759558, 0.92017797510290777),
                       (0.6354325172261639, 0.0044996406688994966, 0.94400988522072671),
                       (0.56005302561872217, 0.012360961058113868, 0.9550888615208315),
                       (0.48514691444823033, 0.02244273774428323, 0.96052715354908824),
                       (0.40915452233121297, 0.038050051985090663, 0.9530827373033901),
                       (0.34044664413813769, 0.061506622880756652, 0.93572317550274153),
                       (0.27121196602377723, 0.087583394719592964, 0.91167735370277569),
                       (0.21426837286284917, 0.12567698337181699, 0.87566307967911239),
                       (0.16666666666666669, 0.16678863468454116, 0.83333333333333337))

    INNER_SEGMENT_D = ((0.16666666666666669, 0.16678863468454116, 0.83333333333333337),
                       (0.12165704016108102, 0.22146056380332022, 0.78091808058364187),
                       (0.08391407271422513, 0.27933391530596546, 0.71964071834942944),
                       (0.063618822517293047, 0.34035650383043259, 0.6567559609716227),
                       (0.047360070653204134, 0.39916075132177753, 0.59198456976910685),
                       (0.039818801454504317, 0.4598309012284989, 0.52498327920264654),
                       (0.043514822572156032, 0.51523630843499346, 0.45514034849442586),
                       (0.051490166414110757, 0.57028123720724511, 0.38443609006042545),
                       (0.074165904772689598, 0.61809616062999317, 0.31021143202208651),
                       (0.10799230115185959, 0.65536518140072342, 0.23647281675341),
                       (0.16666666666666666, 0.66678863468454119, 0.16666666666666669))

    OUTER_SEGMENT_A = ((0.16666666666666669, 0.16678863468454119, 0.83333333333333337),
                       (0.12268636159378737, 0.2200747971225194, 0.78027435862232719),
                       (0.084123438606259815, 0.27998168968857023, 0.71827875273218511),
                       (0.063604041287177046, 0.34127160459938938, 0.65435398072645512),
                       (0.051934048706554312, 0.3988421447973417, 0.58681147351692831),
                       (0.047655529987910228, 0.45783183013086143, 0.5174119506493392),
                       (0.057259897405561797, 0.51056372384135051, 0.44272006752636628),
                       (0.079685479775325085, 0.55752210834613802, 0.36920940515515993),
                       (0.12022411945568266, 0.59146765332690299, 0.28996929860596443),
                       (0.17777388431075458, 0.59792656942763622, 0.22399507359661749),
                       (0.25, 0.58344310454942061, 0.16666666666666669))

    OUTER_SEGMENT_B = ((0.25, 0.58344310454942061, 0.16666666666666669),
                       (0.37764207341819733, 0.52204934069110864, 0.09990422878051565),
                       (0.51054883992932087, 0.43172352578160311, 0.076871328204169295),
                       (0.63283869074504207, 0.33438838717635144, 0.096925696322309379),
                       (0.74851950183555105, 0.23526268461339681, 0.1550546214442152),
                       (0.83632714361608951, 0.1529289087803585, 0.25919819416972223),
                       (0.90971222479021119, 0.090324911175890726, 0.38458650345398748),
                       (0.96050431396181168, 0.044865419359485875, 0.53648086129526507),
                       (0.98358884946443992, 0.019803969362900645, 0.68137840790974713),
                       (0.99575610534456827, 0.0025704462554247565, 0.84833068323143346),
                       (1, 0, 1.0))

    VERTEX0_INDEX_ARRAY = [1, 2, 3, 4]

    VERTEX1_INDEX_ARRAY = [2, 3, 4, 1]

    VERTEX2_INDEX_ARRAY = [3, 4, 1, 2]

    VERTEX3_INDEX_ARRAY = [0, 1, 2, 3]

    VERTEX_MAPPING = (VERTEX0_INDEX_ARRAY,
                      VERTEX1_INDEX_ARRAY,
                      VERTEX2_INDEX_ARRAY,
                      VERTEX3_INDEX_ARRAY)

    TETRAHEDRON_MAPPING = (0, 1, 2, 3)

    ANCHOR_INDEX = 1

    UP_INDEX = 0

    AIM_INDEX = 2

    HEIGHT_INDEX = 3

    def __init__(self):
        self.endSegmentWeights = []

        self.innerSegmentWeights = []

        self.innerSegmentA_Weights = []

        self.innerSegmentB_Weights = []

        self.innerSegmentC_Weights = []

        self.innerSegmentD_Weights = []

        self.outerSegmentA_Weights = []

        self.outerSegmentB_Weights = []

        self.curvePoints = []

        self.curveSegmentPoints = []

        self.curveSegmentWeights = []

        self.curveKnotWeights = []

    def exposePointAsPath(self):
        positionInCurveSpace = mobu.FBVector4d()

        for curve in [self.FRAME_POINTS,
                      self.START_SEGMENT,
                      self.INNER_SEGMENT,
                      self.INNER_SEGMENT_A,
                      self.INNER_SEGMENT_B,
                      self.INNER_SEGMENT_C,
                      self.INNER_SEGMENT_D,
                      self.OUTER_SEGMENT_A,
                      self.OUTER_SEGMENT_B]:
            controlPath = mobu.FBModelPath3D('test1')
            controlPath.Show = True
            controlPath.Visible = True

            controlPath.PathKeyClear()

            for pointIndex in xrange(len(curve)):
                positionInCurveSpace[0] = curve[pointIndex][0]*100.0
                positionInCurveSpace[1] = curve[pointIndex][1]*100.0
                positionInCurveSpace[2] = curve[pointIndex][2]*100.0

                if pointIndex == 0:
                    controlPath.PathKeyStartAdd(positionInCurveSpace)
                else:
                    controlPath.PathKeyEndAdd(positionInCurveSpace)

            for pointIndex in xrange(len(curve)):
                positionInCurveSpace[0] = curve[pointIndex][0]*100.0
                positionInCurveSpace[1] = curve[pointIndex][1]*100.0
                positionInCurveSpace[2] = curve[pointIndex][2]*100.0

                controlPath.PathKeySetLeftRightTangent(pointIndex,
                                                       positionInCurveSpace,
                                                       positionInCurveSpace,
                                                       positionInCurveSpace) 

    def computeWeights(self):
        self.endSegmentWeights = []

        self.innerSegmentWeights = []

        vertexIndexArray = [self.VERTEX0_INDEX_ARRAY,
                            self.VERTEX1_INDEX_ARRAY,
                            self.VERTEX2_INDEX_ARRAY]

        for point in self.START_SEGMENT:
            heightPoint = mobu.FBVector4d(point[0],
                                          point[1],
                                          point[2],
                                          1.0)

            weightData = self.extractPointData(heightPoint,
                                               vertexIndexArray)

            self.endSegmentWeights.append(weightData)

        vertexIndexArray = [self.VERTEX1_INDEX_ARRAY,
                            self.VERTEX2_INDEX_ARRAY,
                            self.VERTEX3_INDEX_ARRAY]

        for point in self.INNER_SEGMENT:
            heightPoint = mobu.FBVector4d(point[0],
                                          point[1],
                                          point[2],
                                          1.0)

            weightData = self.extractPointData(heightPoint,
                                               vertexIndexArray)

            self.innerSegmentWeights.append(weightData)

        vertexIndexArray = [self.VERTEX2_INDEX_ARRAY,
                            self.VERTEX3_INDEX_ARRAY,
                            self.VERTEX0_INDEX_ARRAY]

        for point in self.INNER_SEGMENT_A:
            heightPoint = mobu.FBVector4d(point[0],
                                          point[1],
                                          point[2],
                                          1.0)

            weightData = self.extractPointData(heightPoint,
                                               vertexIndexArray)

            self.innerSegmentA_Weights.append(weightData)

        vertexIndexArray = [self.VERTEX3_INDEX_ARRAY,
                            self.VERTEX0_INDEX_ARRAY,
                            self.VERTEX1_INDEX_ARRAY]

        for point in self.INNER_SEGMENT_B:
            heightPoint = mobu.FBVector4d(point[0],
                                          point[1],
                                          point[2],
                                          1.0)

            weightData = self.extractPointData(heightPoint,
                                               vertexIndexArray)

            self.innerSegmentB_Weights.append(weightData)

        vertexIndexArray = [self.VERTEX0_INDEX_ARRAY,
                            self.VERTEX1_INDEX_ARRAY,
                            self.VERTEX2_INDEX_ARRAY]

        for point in self.INNER_SEGMENT_C:
            heightPoint = mobu.FBVector4d(point[0],
                                          point[1],
                                          point[2],
                                          1.0)

            weightData = self.extractPointData(heightPoint,
                                               vertexIndexArray)

            self.innerSegmentC_Weights.append(weightData)

        vertexIndexArray = [self.VERTEX1_INDEX_ARRAY,
                            self.VERTEX2_INDEX_ARRAY,
                            self.VERTEX3_INDEX_ARRAY]

        for point in self.INNER_SEGMENT_D:
            heightPoint = mobu.FBVector4d(point[0],
                                          point[1],
                                          point[2],
                                          1.0)

            weightData = self.extractPointData(heightPoint,
                                               vertexIndexArray)

            self.innerSegmentD_Weights.append(weightData)

        vertexIndexArray = [self.VERTEX1_INDEX_ARRAY,
                            self.VERTEX2_INDEX_ARRAY,
                            self.VERTEX3_INDEX_ARRAY]

        for point in self.OUTER_SEGMENT_A:
            heightPoint = mobu.FBVector4d(point[0],
                                          point[1],
                                          point[2],
                                          1.0)

            weightData = self.extractPointData(heightPoint,
                                               vertexIndexArray)

            self.outerSegmentA_Weights.append(weightData)

        vertexIndexArray = [self.VERTEX2_INDEX_ARRAY,
                            self.VERTEX3_INDEX_ARRAY,
                            self.VERTEX0_INDEX_ARRAY]

        for point in self.OUTER_SEGMENT_B:
            heightPoint = mobu.FBVector4d(point[0],
                                          point[1],
                                          point[2],
                                          1.0)

            weightData = self.extractPointData(heightPoint,
                                               vertexIndexArray)

            self.outerSegmentB_Weights.append(weightData)

    def extractPointData(self,
                         heightPoint,
                         vertexIndexArray,
                         pointOffset=0):
        weightData = SplineWeights()

        weightData.weight0 = self.processWeight(vertexIndexArray[0],
                                                heightPoint=heightPoint)

        weightData.weight1 = self.processWeight(vertexIndexArray[1],
                                                heightPoint=heightPoint)

        weightData.weight2 = self.processWeight(vertexIndexArray[2],
                                                heightPoint=heightPoint)

        weightData.weight3 = 1.0 - (weightData.weight0+weightData.weight1+weightData.weight2)

        if weightData.weight3 < 0.0:
            weightData.weight3 = 0.0

        weightData.pointIndex0 = self.TETRAHEDRON_MAPPING[0]+pointOffset
        weightData.pointIndex1 = self.TETRAHEDRON_MAPPING[1]+pointOffset
        weightData.pointIndex2 = self.TETRAHEDRON_MAPPING[2]+pointOffset
        weightData.pointIndex3 = self.TETRAHEDRON_MAPPING[3]+pointOffset

        return weightData

    def inspectWeight(self,
                      weightData,
                      pointPositions,
                      pointIndexArray=None,
                      createLocator=False):
        weightList = [weightData.weight0,
                      weightData.weight1,
                      weightData.weight2,
                      weightData.weight3]

        if pointIndexArray is None:
            pointIndexArray = [weightData.pointIndex0,
                               weightData.pointIndex1,
                               weightData.pointIndex2,
                               weightData.pointIndex3]

        targetPoint = mobu.FBVector3d()

        for index in xrange(4):
            cagePoint = mobu.FBVector4d(pointPositions[pointIndexArray[index]][0],
                                        pointPositions[pointIndexArray[index]][1],
                                        pointPositions[pointIndexArray[index]][2],
                                        1.0)

            targetPoint[0] += cagePoint[0]*weightList[index]
            targetPoint[1] += cagePoint[1]*weightList[index]
            targetPoint[2] += cagePoint[2]*weightList[index]

        if createLocator is False:
            return targetPoint

        null = mobu.FBModelNull('point1')

        null.Visible = True

        null.Translation = targetPoint

        null.Show = True

        return targetPoint

    def buildKnotWeights(self,
                         sampleIndexArray,
                         weightArray):
        segmentWeights = []
        for weightData in weightArray:
            knotData = SplineWeights()

            knotData.weight0 = weightData.weight0 
            knotData.weight1 = weightData.weight1
            knotData.weight2 = weightData.weight2
            knotData.weight3 = weightData.weight3

            knotData.pointIndex0 = sampleIndexArray[0]
            knotData.pointIndex1 = sampleIndexArray[1]
            knotData.pointIndex2 = sampleIndexArray[2]
            knotData.pointIndex3 = sampleIndexArray[3]

            segmentWeights.append(knotData)

        self.curveSegmentWeights.append(segmentWeights) 

    def det4x4(self, inputMattrix):
        #http://code.activestate.com/recipes/578108-determinant-of-matrix-of-any-order/
        n=len(inputMattrix)
        if (n>2):
            i=1
            t=0
            sum=0
            while t<=n-1:
                d={}
                t1=1
                while t1<=n-1:
                    m=0
                    d[t1]=[]
                    while m<=n-1:
                        if (m==t):
                            u=0
                        else:
                            d[t1].append(inputMattrix[t1][m])
                        m+=1
                    t1+=1
                l1=[d[x] for x in d]
                sum=sum+i*(inputMattrix[0][t])*(self.det4x4(l1))
                i=i*(-1)
                t+=1
            return  sum
        else:
            return  (inputMattrix[0][0]*inputMattrix[1][1]-inputMattrix[0][1]*inputMattrix[1][0])

    def processWeight(self,
                      inputMapping,
                      heightPoint=None):
        anchor = mobu.FBVector4d(self.FRAME_POINTS[inputMapping[self.ANCHOR_INDEX]][0],
                                 self.FRAME_POINTS[inputMapping[self.ANCHOR_INDEX]][1],
                                 self.FRAME_POINTS[inputMapping[self.ANCHOR_INDEX]][2],
                                 1.0)

        upPoint = mobu.FBVector4d(self.FRAME_POINTS[inputMapping[self.UP_INDEX]][0],
                                  self.FRAME_POINTS[inputMapping[self.UP_INDEX]][1],
                                  self.FRAME_POINTS[inputMapping[self.UP_INDEX]][2],
                                  1.0)

        aimPoint = mobu.FBVector4d(self.FRAME_POINTS[inputMapping[self.AIM_INDEX]][0],
                                   self.FRAME_POINTS[inputMapping[self.AIM_INDEX]][1],
                                   self.FRAME_POINTS[inputMapping[self.AIM_INDEX]][2],
                                   1.0)

        if heightPoint is None:
            heightPoint = mobu.FBVector4d(self.FRAME_POINTS[inputMapping[self.HEIGHT_INDEX]][0],
                                          self.FRAME_POINTS[inputMapping[self.HEIGHT_INDEX]][1],
                                          self.FRAME_POINTS[inputMapping[self.HEIGHT_INDEX]][2],
                                          1.0)


        upVector = upPoint - anchor

        aimVector = aimPoint - anchor

        heightVector = heightPoint - anchor

        baseVolume = self.det4x4([[upVector[0], upVector[1], upVector[2], 0.0],
                                 [heightVector[0], heightVector[1], heightVector[2], 0.0],
                                 [aimVector[0], aimVector[1], aimVector[2], 0.0],
                                 [anchor[0], anchor[1], anchor[2], 1.0]])

        return math.fabs(baseVolume)

    def collectControls(self,
                        namePattern):
        controls = mobu.FBComponentList()
        mobu.FBFindObjectsByName('{}*'.format(namePattern),
                                 controls,
                                 False,
                                 True)

        return controls

    def computeSplineChord(self,
                           targetPoint,
                           controlPositions,
                           weightArray):
        goalPoint = mobu.FBVector3d(targetPoint[0],
                                    targetPoint[1],
                                    targetPoint[2])

        sampleIndexArray = []
        for index in xrange(self.sampleIndex, 
                            self.sampleIndex+self.rowCount):
            sampleIndexArray.append(index)

        pointPositions = []

        for index in sampleIndexArray:
            pointPositions.append(controlPositions[index])

        segmentPoints = []
        for weightData in weightArray:
            targetPoint = self.inspectWeight(weightData,
                                             pointPositions,
                                             pointIndexArray=self.mappingIndexArray)

            segmentPoints.append(targetPoint)

        self.buildKnotWeights(sampleIndexArray,
                              weightArray)

        segmentOffset = goalPoint - segmentPoints[0]
        for pointIndex in xrange(len(segmentPoints)):
            segmentPoints[pointIndex] = segmentPoints[pointIndex]+segmentOffset*self.rampArray[pointIndex]

        self.curveSegmentPoints.append(segmentPoints)

        return targetPoint

    def computeSplineChordArray(self,
                                controlPositions):
        targetPoint = controlPositions[0]

        segmentDataSource = ['innerSegmentA_Weights',
                             'innerSegmentB_Weights',
                             'innerSegmentC_Weights',
                             'innerSegmentD_Weights']

        self.mappingIndexArray = [0,1,2,3]

        self.sampleIndex = 0

        self.rowCount = 4

        endSegmentSize = 4

        segmentCount = len(controlPositions)-self.rowCount-endSegmentSize

        weightRatio = 1.0/float(len(self.START_SEGMENT)-1)

        segmentData = ['endSegmentWeights']

        self.rampArray = []

        for weightIndex in xrange(len(self.START_SEGMENT)):
            self.rampArray.append(1.0-(weightIndex*weightRatio))

        if len(controlPositions) == 6:
            segmentData.extend(['innerSegmentWeights',
                                'outerSegmentB_Weights'])
        elif len(controlPositions) == 7:
            segmentData.extend(['innerSegmentWeights',
                                'outerSegmentA_Weights',
                                'outerSegmentB_Weights'])
        else:
            segmentData.extend(['innerSegmentWeights',
                                'innerSegmentA_Weights'])

            if segmentCount > 0:
                readIndex = -1

                for segmentIndex in xrange(segmentCount):
                    readIndex+=1
                    if readIndex > 3:
                        readIndex = 0

                    segmentData.append(segmentDataSource[readIndex])

            segmentData.append('outerSegmentA_Weights')
            segmentData.append('outerSegmentB_Weights')

        for segmentIndex in xrange(len(segmentData)):
            weightArray = getattr(self,
                                  segmentData[segmentIndex])

            targetPoint = self.computeSplineChord(targetPoint,
                                                  controlPositions,
                                                  weightArray)

            self.sampleIndex += 1

    def buildPreviewPath(self):
        controlPath = mobu.FBModelPath3D('test1')
        controlPath.Show = True
        controlPath.Visible = True

        controlPath.PathKeyClear()
        positionInCurveSpace = mobu.FBVector4d()

        for pointIndex in xrange(len(self.curvePoints)):
            positionInCurveSpace[0] = self.curvePoints[pointIndex][0] 
            positionInCurveSpace[1] = self.curvePoints[pointIndex][1] 
            positionInCurveSpace[2] = self.curvePoints[pointIndex][2] 

            if pointIndex == 0:
                controlPath.PathKeyStartAdd(positionInCurveSpace)
            else:
                controlPath.PathKeyEndAdd(positionInCurveSpace)


        for pointIndex in xrange(len(self.curvePoints)):
            positionInCurveSpace[0] = self.curvePoints[pointIndex][0] 
            positionInCurveSpace[1] = self.curvePoints[pointIndex][1] 
            positionInCurveSpace[2] = self.curvePoints[pointIndex][2] 

            controlPath.PathKeySetLeftRightTangent(pointIndex,
                                                   positionInCurveSpace,
                                                   positionInCurveSpace,
                                                   positionInCurveSpace) 
        return controlPath

    def getCurvePoints(self):
        self.curvePoints = []
        
        self.curveKnotWeights = []

        for segmentIndex, segment in enumerate(self.curveSegmentWeights):
            if segmentIndex > 0:
                self.curveKnotWeights.extend(segment[1:len(segment)])
            else:
                self.curveKnotWeights.extend(segment)

        pathInterpolation = []
        for segmentIndex, segment in enumerate(self.curveSegmentPoints):
            if segmentIndex > 0:
                pathInterpolation.extend(segment[1:len(segment)])
            else:
                pathInterpolation.extend(segment)

        self.curvePoints = [mobu.FBVector3d(points[0],
                                            points[1],
                                            points[2])
                            for points in pathInterpolation]

    def computePathInterpolation(self,
                                 controls):
        controlPositions = [[control.Translation[0],
                             control.Translation[1],
                             control.Translation[2]] for control in controls]

        if controlPositions < 6:
            print 'Please assign at least 6 controls'
            return

        self.curveSegmentPoints = []

        self.curveSegmentWeights = []

        self.computeSplineChordArray(controlPositions)

        self.getCurvePoints()