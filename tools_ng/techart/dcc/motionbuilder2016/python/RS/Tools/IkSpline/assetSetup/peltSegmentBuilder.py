import inspect
import json
import math
import os
import tempfile


import pyfbsdk as mobu

from RS import Globals

from RS.Utils import Scene
from RS.Utils.Scene import Constraint
from RS.Tools.IkSpline import Utils as IkUtils
from RS.Tools.IkSpline import Rigging as IkRigging

from RS.Utils.Scene import RelationshipConstraintManager as relationConstraintManager
from RS.Tools.IkSpline.assetSetup import ikFkParallelChainBuilder


reload(ikFkParallelChainBuilder)


reload(IkUtils)
reload(IkRigging)


class PeltTangentOffset(object):
    NODE_ATTRIBUTES = ('node',
                       'sampler',
                       'hook',
                       'animationController')

    def __init__(self,
                 node,
                 inputSampler,
                 animationController,
                 relationConstraint,
                 inputWeight,
                 smoothWeight,
                 weightHolder,
                 useStartHermiteCurve=True):
        self.weightHolder = weightHolder

        self.node = node

        self.inputWeight = inputWeight

        self.smoothWeight = smoothWeight

        self.useStartHermiteCurve = useStartHermiteCurve

        self.sampler = inputSampler

        self.animationController = animationController

        self.relationConstraint = relationConstraint

        self.hook = self.createGroup()

        self.offset = self.createGroup(suffix='offset1',
                                       parentToNode=True)

        Globals.Scene.Evaluate()

        self.createNetwork()

        inputSampler.Visibility = False

    def createGroup(self,
                    suffix='hook1',
                    parentToNode=False):
        offset = mobu.FBModelNull('{0}_{1}'.format(self.node.Name,
                                                   suffix))

        offset.Show = True
        offset.PropertyList.Find('Look').Data = 0

        nodeMatrix = mobu.FBMatrix()
        self.node.GetMatrix(nodeMatrix)
        offset.SetMatrix(nodeMatrix)

        if parentToNode is False:
            self.node.Parent = offset
        else:
           offset.Parent = self.node.Parent

           self.node.Parent = offset

        return offset

    def prepareAnimationFields(self,
                               inputArray):
        for node in inputArray:
            if not node.PropertyList.Find('Translation (Lcl)').IsAnimated():
                node.PropertyList.Find('Translation (Lcl)').SetAnimated(True)

    def connectHook(self):
        jointSender = self.relationConstraint.AddSenderBox(self.animationController.node)

        jointReceiver = self.relationConstraint.AddReceiverBox(self.hook)

        self.relationConstraint.ConnectBoxes(jointSender,
                                             'Translation',
                                             jointReceiver,
                                             'Translation')

    def connectSampler(self):
        jointSender = self.relationConstraint.AddSenderBox(self.sampler)

        jointReceiver = self.relationConstraint.AddReceiverBox(self.node)

        jointSender.SetGlobalTransforms(False)

        jointReceiver.SetGlobalTransforms(False)

        self.relationConstraint.ConnectBoxes(jointSender,
                                             'Lcl Translation',
                                             jointReceiver,
                                             'Lcl Translation')

    def findFBboxInputPlug(self,
                           relationConstraintBox,
                           attributeName):
        for node in relationConstraintBox.AnimationNodeInGet().Nodes:
            if node.Name != attributeName:
                continue

            return node

        return None

    def createStartHermiteCurve(self):
        '''
            start = h_10 --> (t)=t^{3}-2t^{2}+t
        '''
        cubicExponentNode = self.relationConstraint.AddFunctionBox("Number",
                                                                   "Exponent (a^b)")

        aPlug = self.findFBboxInputPlug(cubicExponentNode.GetBox(), 'a')
        aPlug.WriteData([self.inputWeight])

        bPlug = self.findFBboxInputPlug(cubicExponentNode.GetBox(), 'b')
        bPlug.WriteData([3.0])

        squareExponentNode = self.relationConstraint.AddFunctionBox("Number",
                                                                    "Exponent (a^b)")

        aPlug = self.findFBboxInputPlug(squareExponentNode.GetBox(), 'a')
        aPlug.WriteData([self.inputWeight])

        bPlug = self.findFBboxInputPlug(squareExponentNode.GetBox(), 'b')
        bPlug.WriteData([2.0])

        addNode = self.relationConstraint.AddFunctionBox("Number",
                                                         "Add (a + b)")

        aPlug = self.findFBboxInputPlug(addNode.GetBox(), 'a')
        aPlug.WriteData([self.inputWeight])

        substractNode = self.relationConstraint.AddFunctionBox("Number",
                                                               "Subtract (a - b)")

        multiplyNode = self.relationConstraint.AddFunctionBox("Number",
                                                              "Multiply (a x b)")

        aPlug = self.findFBboxInputPlug(multiplyNode.GetBox(), 'a')
        aPlug.WriteData([2.0])

        self.relationConstraint.ConnectBoxes(squareExponentNode,
                                             'Result',
                                             multiplyNode,
                                             'b')

        self.relationConstraint.ConnectBoxes(cubicExponentNode,
                                             'Result',
                                             substractNode,
                                             'a')

        self.relationConstraint.ConnectBoxes(multiplyNode,
                                             'Result',
                                             substractNode,
                                             'b')

        self.relationConstraint.ConnectBoxes(substractNode,
                                             'Result',
                                             addNode,
                                             'b')

        weightSender = self.relationConstraint.AddSenderBox(self.weightHolder)

        weightmultiplyNode = self.relationConstraint.AddFunctionBox("Number",
                                                                    "Multiply (a x b)")

        self.relationConstraint.ConnectBoxes(addNode,
                                             'Result',
                                             multiplyNode,
                                             'a')

        self.relationConstraint.ConnectBoxes(weightSender,
                                             'weight',
                                             multiplyNode,
                                             'b')

        return weightmultiplyNode

    def createEndHermiteCurve(self):
        '''
            end = h_11 --> (t)=t^{3}-t^{2}
        '''
        cubicExponentNode = self.relationConstraint.AddFunctionBox("Number",
                                                                   "Exponent (a^b)")

        aPlug = self.findFBboxInputPlug(cubicExponentNode.GetBox(), 'a')
        aPlug.WriteData([self.inputWeight])

        bPlug = self.findFBboxInputPlug(cubicExponentNode.GetBox(), 'b')
        bPlug.WriteData([3.0])

        squareExponentNode = self.relationConstraint.AddFunctionBox("Number",
                                                                    "Exponent (a^b)")

        aPlug = self.findFBboxInputPlug(squareExponentNode.GetBox(), 'a')
        aPlug.WriteData([self.inputWeight])

        bPlug = self.findFBboxInputPlug(squareExponentNode.GetBox(), 'b')
        bPlug.WriteData([2.0])

        substractNode = self.relationConstraint.AddFunctionBox("Number",
                                                               "Subtract (a - b)")

        self.relationConstraint.ConnectBoxes(cubicExponentNode,
                                             'Result',
                                             substractNode,
                                             'a')

        self.relationConstraint.ConnectBoxes(squareExponentNode,
                                             'Result',
                                             substractNode,
                                             'b')

        return substractNode

    def connectRotation(self,
                        useHermite=False):
        jointSender = self.relationConstraint.AddSenderBox(self.animationController.node)

        jointReceiver = self.relationConstraint.AddReceiverBox(self.offset)

        jointSender.SetGlobalTransforms(False)

        jointReceiver.SetGlobalTransforms(False)

        interpolateNode = self.relationConstraint.AddFunctionBox("Rotation",
                                                                 "Interpolate")

        if useHermite is True:
            if self.useStartHermiteCurve is True:
                weightNode = self.createStartHermiteCurve()
            else:
                weightNode = self.createEndHermiteCurve()

        else:
            weightSender = self.relationConstraint.AddSenderBox(self.weightHolder)

            weightNode = self.relationConstraint.AddFunctionBox("Number",
                                                                "Multiply (a x b)")

            aPlug = self.findFBboxInputPlug(weightNode.GetBox(), 'a')
            aPlug.WriteData([self.inputWeight])


            self.relationConstraint.ConnectBoxes(weightSender,
                                                 'weight',
                                                 weightNode,
                                                 'b')

        self.relationConstraint.ConnectBoxes(weightNode,
                                             'Result',
                                             interpolateNode,
                                             'c')

        self.relationConstraint.ConnectBoxes(jointSender,
                                             'Lcl Rotation',
                                             interpolateNode,
                                             'Rb')

        self.relationConstraint.ConnectBoxes(interpolateNode,
                                             'Result',
                                             jointReceiver,
                                             'Lcl Rotation')

    def createNetwork(self):
        inputArray = [self.animationController.node,
                      self.animationController.offsetNode,
                      self.node,
                      self.sampler]

        self.prepareAnimationFields(inputArray)

        self.connectHook()

        self.connectSampler()

        self.connectRotation()

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_ATTRIBUTES:
            currentValue = getattr(self, 
                                   attribute)

            if not isinstance(currentValue,
                              PeltAnimationController):
                reportData += '\n\t{0}:{1}'.format(attribute,
                                                   currentValue.Name)

            else:
                reportData += '\n\t{0}:{1}'.format(attribute,
                                                   currentValue)

        return reportData


class PeltAnimationController(object):
    NODE_ATTRIBUTES = ('node',
                       'offsetNode')

    def __init__(self,
                 node,
                 createAttribute=True):
        self.node = node

        self.offsetNode = None

        if createAttribute is False:
            return

        self.createWeightDriver()

    def createWeightDriver(self):
        attributeType = mobu.FBPropertyType.kFBPT_double
        weightProperty = self.node.PropertyList.Find('weight')

        if weightProperty is None:
            weightProperty = self.node.PropertyCreate('weight',
                                                       attributeType,
                                                       "",
                                                       True,
                                                       True,
                                                       None)
            weightProperty.SetMax(1.0)
            weightProperty.SetMin(0.0)
            weightProperty.SetAnimated(True)

            weightProperty.Data = 1.0

    def createOffsetGroup(self):
        offset = mobu.FBModelNull('{0}_zro1'.format(self.node.Name))

        offset.Show = True
        offset.PropertyList.Find('Look').Data = 0

        nodeMatrix = mobu.FBMatrix()
        self.node.GetMatrix(nodeMatrix)
        offset.SetMatrix(nodeMatrix)

        self.node.Parent = offset

        self.offsetNode = offset

    def __repr__(self):
        reportData = '\n\t\t<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_ATTRIBUTES:
            currentValue = getattr(self, 
                                   attribute)

            reportData += '\n\t\t\t{0}:{1}'.format(attribute,
                                               currentValue.Name)

        return reportData


class PeltSegment(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    HORIZONTAL_OFFSET = 480

    VERTICAL_OFFSET = 190

    NODE_ATTRIBUTES = ('startNode',
                       'startTangent',
                       'endNode',
                       'endTangent',
                       'jointArray')

    def __init__(self):
        self.constraintUtils = IkUtils.ConstraintUtils()

        self.startNode = None

        self.startTangent = None

        self.endNode = None

        self.endTangent = None

        self.startSamplerAnchor = None

        self.endSamplerAnchor = None

        self.jointArray = []

        self.startSamplerJointArray = []

        self.endSamplerJointArray = []

        self.startPeltTangentOffsetArray = []

        self.endPeltTangentOffsetArray = []

        self.resultJointArray = []

        self.resultAimJointArray = []

        self.parameters = []

        self.attachConstraints = []

        self.pointConstraints = []

        self.peltFinalConstraints = []

        self.initialSegmentLength = 0.0

        self.parentConstraint = None

        self.relationConstraint = None

        self.bezierConstraint = None

        self.tangentConstraint = None
    
        self.hermiteConstraint = None

        self.targetSegmentStart = None

        self.axisIndex = 0

    def findFBboxInputPlug(self,
                           relationConstraintBox,
                           attributeName):
        for node in relationConstraintBox.AnimationNodeInGet().Nodes:
            if node.Name != attributeName:
                continue

            return node

        return None

    def modulateVector(self,
                       inputWeight,
                       inputSender,
                       blockOffset,
                       shiftOffset,
                       offsetIndex=1):
        multiplyNode = self.relationConstraint.AddFunctionBox("Vector",
                                                              "Scale (a x V)")

        weightPlug = self.findFBboxInputPlug(multiplyNode.GetBox(),
                                             'Number')

        weightPlug.WriteData([inputWeight])

        self.relationConstraint.ConnectBoxes(inputSender,
                                             'Translation',
                                             multiplyNode,
                                             'Vector')

        inputSender.SetBoxPosition(shiftOffset,
                                   self.VERTICAL_OFFSET*offsetIndex+blockOffset)

        multiplyNode.SetBoxPosition(self.HORIZONTAL_OFFSET+shiftOffset,
                                    self.VERTICAL_OFFSET*offsetIndex+blockOffset)

        return multiplyNode

    def sumVectors(self,
                   pointA,
                   pointB,
                   outputReceivers,
                   blockOffset,
                   shiftOffset):
        sumNode = self.relationConstraint.AddFunctionBox("Vector",
                                                         "Add (V1 + V2)")

        self.relationConstraint.ConnectBoxes(pointA,
                                             'Result',
                                             sumNode,
                                             'V1')

        self.relationConstraint.ConnectBoxes(pointB,
                                             'Result',
                                             sumNode,
                                             'V2')

        sumNode.SetBoxPosition(self.HORIZONTAL_OFFSET*2+shiftOffset,
                               int(self.VERTICAL_OFFSET+self.VERTICAL_OFFSET*0.5)+blockOffset)

        for outputReceiver in outputReceivers:
            self.relationConstraint.ConnectBoxes(sumNode,
                                                 'Result',
                                                 outputReceiver,
                                                 'Translation')

            outputReceiver.SetBoxPosition(self.HORIZONTAL_OFFSET*3+shiftOffset,
                                          int(self.VERTICAL_OFFSET+self.VERTICAL_OFFSET*0.5)+blockOffset)

    def interpolateRotation(self,
                            inputSenderA,
                            inputSenderB,
                            outputReceivers,
                            inputWeight,
                            blockOffset,
                            shiftOffset):
        interpolateNode = self.relationConstraint.AddFunctionBox("Rotation",
                                                                 "Interpolate")

        self.relationConstraint.ConnectBoxes(inputSenderA,
                                             'Rotation',
                                             interpolateNode,
                                             'Ra')

        self.relationConstraint.ConnectBoxes(inputSenderB,
                                             'Rotation',
                                             interpolateNode,
                                             'Rb')

        weightPlug = self.findFBboxInputPlug(interpolateNode.GetBox(),
                                             'c')

        weightPlug.WriteData([inputWeight])

        for outputReceiver in outputReceivers:
            self.relationConstraint.ConnectBoxes(interpolateNode,
                                                 'Result',
                                                 outputReceiver,
                                                 'Rotation')

            interpolateNode.SetBoxPosition(self.HORIZONTAL_OFFSET+shiftOffset,
                                           self.VERTICAL_OFFSET+blockOffset-self.VERTICAL_OFFSET)

    def prepareAnimationFields(self,
                               inputJoint):
        for node in [self.startNode.node,
                     self.endNode.node,
                     inputJoint]:
            if not node.PropertyList.Find('Translation (Lcl)').IsAnimated():
                node.PropertyList.Find('Translation (Lcl)').SetAnimated(True)

    def createJointNetwork(self,
                           jointData,
                           jointIndex,
                           blockOffset,
                           shiftOffset,
                           inheritsRotation=True):
        for inputJoint in jointData:
            self.prepareAnimationFields(inputJoint)

        startNodeSender = self.relationConstraint.AddSenderBox(self.startNode.node)

        endNodeSender = self.relationConstraint.AddSenderBox(self.endNode.node)

        jointReceivers = [self.relationConstraint.AddReceiverBox(inputJoint) 
                          for inputJoint in jointData]

        currentWeight = self.parameters[jointIndex].segmentWeights

        pointA = self.modulateVector(1.0-currentWeight,
                                     startNodeSender,
                                     blockOffset,
                                     shiftOffset)

        pointB = self.modulateVector(currentWeight,
                                     endNodeSender,
                                     blockOffset,
                                     shiftOffset,
                                     offsetIndex=2)

        self.sumVectors(pointA,
                        pointB,
                        jointReceivers,
                        blockOffset,
                        shiftOffset)

        if inheritsRotation is False:
            return

        self.interpolateRotation(startNodeSender,
                                 endNodeSender,
                                 jointReceivers,
                                 currentWeight,
                                 blockOffset,
                                 shiftOffset)

    def snapShotNode(self,
                     inputNode,
                     suffix,
                     showNode=True):
        cloneNode = mobu.FBModelNull('{0}_{1}'.format(inputNode.Name,
                                                      suffix))

        targetMatrix = mobu.FBMatrix()
        inputNode.GetMatrix(targetMatrix)
        cloneNode.SetMatrix(targetMatrix)
        cloneNode.Show = True

        cloneNode.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        return cloneNode

    def cloneJoints(self,
                    targetAttribute,
                    suffix):
        targetArray = [self.snapShotNode(joint,
                                         suffix) for joint in self.jointArray]

        setattr(self,
                targetAttribute,
                targetArray)

    def createSamplerAnchor(self):
        self.startSamplerAnchor = self.createMarker(self.startNode.node,
                                                    'RightAnchor',
                                                    mobu.FBVector3d(0, 0, 0))

        self.startSamplerAnchor.Parent = self.startNode.offsetNode

        self.endSamplerAnchor = self.createMarker(self.endNode.node,
                                                  'LeftAnchor',
                                                  mobu.FBVector3d(0, 0, 0))

        self.endSamplerAnchor.Parent = self.endNode.offsetNode

        jointSender = self.relationConstraint.AddSenderBox(self.startNode.node)

        jointReceiver = self.relationConstraint.AddReceiverBox(self.startSamplerAnchor)

        self.relationConstraint.ConnectBoxes(jointSender,
                                             'Translation',
                                             jointReceiver,
                                             'Translation')

        jointSender2 = self.relationConstraint.AddSenderBox(self.endNode.node)

        jointReceiver2 = self.relationConstraint.AddReceiverBox(self.endSamplerAnchor)

        self.relationConstraint.ConnectBoxes(jointSender2,
                                             'Translation',
                                             jointReceiver2,
                                             'Translation')

    def prepareLayer(self,
                     attribute,
                     targetRoot):
        self.cloneJoints('{}JointArray'.format(attribute),
                         attribute)

        targetArray = getattr(self,
                              '{}JointArray'.format(attribute))

        for joint in targetArray:
            joint.Parent = targetRoot

    def prepareSamplers(self,
                        createModels=True):
        if createModels:
            self.createSamplerAnchor()

        self.prepareLayer('startSampler',
                          self.startSamplerAnchor)

        self.prepareLayer('endSampler',
                          self.endSamplerAnchor)

    def createPositionLayer(self,
                            shiftIndex=1):
        self.prepareSamplers()

        blockOffset = self.VERTICAL_OFFSET*3

        for jointIndex, joint in enumerate(self.startSamplerJointArray):
            jointData = [self.startSamplerJointArray[jointIndex],
                         self.endSamplerJointArray[jointIndex]]

            self.createJointNetwork(jointData,
                                    jointIndex,
                                    blockOffset*jointIndex,
                                    shiftIndex*self.HORIZONTAL_OFFSET*4)

    def bindAndWeightJoint(self,
                           joint,
                           inputWeight):
        constraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

        constraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                joint)

        constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                self.startSamplerAnchor)

        constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                self.endSamplerAnchor)

        constraint.Snap()

        constraint.Name = '{}_point1'.format(self.relationConstraint.GetConstraint().Name)
        constraint.Active = True

        self.pointConstraints.append(constraint)

        startWeightProperty = None

        endWeightProperty = None

        for property in constraint.PropertyList:
            if not property.Name.endswith('Weight'):
                continue

            if self.startNode.node.LongName in property.Name:
                startWeightProperty = property

            if self.endNode.node.LongName in property.Name:
                endWeightProperty = property

        startWeightProperty.Data = (1.0-inputWeight)*100.0

        endWeightProperty.Data = inputWeight*100.0

    def createPositionConstraints(self):
        self.prepareSamplers()

        for jointIndex, joint in enumerate(self.startSamplerJointArray):
            self.bindAndWeightJoint(joint,
                                    self.parameters[jointIndex].segmentWeights)

        for jointIndex, joint in enumerate(self.endSamplerJointArray):
            self.bindAndWeightJoint(joint,
                                    self.parameters[jointIndex].segmentWeights)

    def buildAimComponent(self,
                          joint,
                          reverseAim=False):
        target1 = mobu.FBModelNull('{}_targetSegment1'.format(joint.LongName))
        targetHook1 = mobu.FBModelNull('{}_targetSegmentHook1'.format(joint.LongName))

        targetHook1.Parent = target1

        target1.Parent = self.startNode.node
        target1.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
        Globals.Scene.Evaluate()

        hookArray = [self.startNode.node,
                     self.startNode.node]

        if reverseAim is True:
            hookArray = [self.startNode.node,
                         self.endNode.node]

        constraintSuffixArray = ['position1',
                                 'rotation1']

        for index, constraintType in enumerate([Constraint.POSITION,
                                                Constraint.ROTATION]):
            constraint = Constraint.CreateConstraint(constraintType)

            constraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                    target1)

            constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                    hookArray[index])

            constraint.Name = '{0}_{1}'.format(target1.LongName,
                                               constraintSuffixArray[index])

            constraint.Active = True

            constraint.Lock = True

            self.pointConstraints.append(constraint)

        targetConstraint = self.constraintUtils.createTargetConstraint(targetHook1,
                                                                       self.endNode.node,
                                                                       spaceObject=hookArray[0])

        targetConstraint[0].Name = '{0}_{1}'.format(targetHook1.LongName,
                                                    'targetConstraint1')

        self.pointConstraints.append(targetConstraint[0])

        return targetHook1

    def distributeBetweenNodes(self,
                               joint,
                               targetHook,
                               inputWeight):
        blockOffset = 0
        shiftOffset = 0

        knot = mobu.FBModelNull('{}_knot1'.format(joint.LongName))
        knot.Parent = targetHook
        knot.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        knot.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
        knot.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)

        startNodeSender = self.relationConstraint.AddSenderBox(self.startNode.node)

        endNodeSender = self.relationConstraint.AddSenderBox(self.endNode.node)

        if inputWeight == 0.0:
            constraint = Constraint.CreateConstraint(Constraint.POSITION)

            constraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                    knot)

            constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                    self.startNode.node)

            constraint.Name = '{0}_{1}'.format(knot.LongName,
                                               'point1')

            constraint.Active = True

            self.pointConstraints.append(constraint)

        elif inputWeight == 1.0:
            constraint = Constraint.CreateConstraint(Constraint.POSITION)

            constraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                    knot)

            constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                    self.endNode.node)

            constraint.Name = '{0}_{1}'.format(knot.LongName,
                                               'point1')

            constraint.Active = True

            self.pointConstraints.append(constraint)

        else:
            pointA = self.modulateVector(1.0-inputWeight,
                                         startNodeSender,
                                         blockOffset,
                                         shiftOffset)

            pointB = self.modulateVector(inputWeight,
                                         endNodeSender,
                                         blockOffset,
                                         shiftOffset,
                                         offsetIndex=2)

            jointReceivers = [self.relationConstraint.AddReceiverBox(knot)]

            self.sumVectors(pointA,
                            pointB,
                            jointReceivers,
                            blockOffset,
                            shiftOffset)

            self.sumVectors(pointA,
                            pointB,
                            jointReceivers,
                            blockOffset,
                            shiftOffset)

        Globals.Scene.Evaluate()
        #joint.Parent = knot

        constraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

        constraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                joint)

        constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                knot)

        constraint.Snap()

        constraint.Name = '{}_glue1'.format(knot.LongName)
        constraint.Active = True

        self.pointConstraints.append(constraint)

    def createTargetConstraints(self):
        self.prepareSamplers()

        self.targetSegmentStart = self.buildAimComponent(self.startSamplerJointArray[0])

        Globals.Scene.Evaluate()

        for jointIndex, joint in enumerate(self.startSamplerJointArray):
            self.distributeBetweenNodes(joint,
                                        self.targetSegmentStart,
                                        self.parameters[jointIndex].segmentWeights)

        Globals.Scene.Evaluate()

        targetSegment = self.buildAimComponent(self.startSamplerJointArray[0],
                                               reverseAim=True)

        for jointIndex, joint in enumerate(self.endSamplerJointArray):
            self.distributeBetweenNodes(joint,
                                        targetSegment,
                                        self.parameters[jointIndex].segmentWeights)

        Globals.Scene.Evaluate()

    def createMarker(self,
                     inputNode,
                     suffix,
                     offsetVector):
        marker = mobu.FBModelNull('{0}_{1}'.format(inputNode.Name,
                                                   suffix))

        marker.Parent = inputNode

        marker.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

        marker.PropertyList.Find('Translation (Lcl)').Data = offsetVector

        return marker

    def createBezierJointNetwork(self,
                                 inputJoint,
                                 jointIndex,
                                 blockOffset,
                                 shiftOffset):
        self.prepareAnimationFields(inputJoint)

        senderArray = []

        for nodeIndex, node in enumerate([self.startNode.node,
                                         self.startTangent,
                                         self.endTangent,
                                         self.endNode.node]):
            sender = self.bezierConstraint.AddSenderBox(node)

            senderArray.append(sender)

            sender.SetBoxPosition(self.HORIZONTAL_OFFSET*shiftOffset,
                                  self.VERTICAL_OFFSET*nodeIndex+blockOffset)

        bezierNode = self.bezierConstraint.AddFunctionBox("Other",
                                                          "Bezier Curve")

        bezierNode.SetBoxPosition(self.HORIZONTAL_OFFSET*shiftOffset+self.HORIZONTAL_OFFSET,
                                  blockOffset+int(self.VERTICAL_OFFSET*1.4))

        for index, driver in enumerate(['Control Point 1',
                                        'Control Point 2',
                                        'Control Point 3',
                                        'Control Point 4']):
            self.bezierConstraint.ConnectBoxes(senderArray[index],
                                               'Translation',
                                                bezierNode,
                                                driver)

        currentWeight = self.parameters[jointIndex].segmentWeights

        weightPlug = self.findFBboxInputPlug(bezierNode.GetBox(),
                                             'Position Ratio [0, 100]')

        weightPlug.WriteData([currentWeight*100.0])

        jointReceiver = self.bezierConstraint.AddReceiverBox(inputJoint)

        jointReceiver.SetBoxPosition(self.HORIZONTAL_OFFSET*shiftOffset+self.HORIZONTAL_OFFSET,
                                     blockOffset+int(self.VERTICAL_OFFSET*0.3))

        self.bezierConstraint.ConnectBoxes(bezierNode,
                                           'Result',
                                           jointReceiver,
                                           'Translation')

    def createTangentControls(self):
        segmentVector = self.endNode.Translation.Data - self.startNode.Translation.Data

        self.initialSegmentLength = segmentVector.Length()

        offsetVector = mobu.FBVector3d(0, 0, 0)

        offsetVector[self.axisIndex] = self.initialSegmentLength*0.33334

        self.startTangent = self.createMarker(self.startNode,
                                              'RightTgnt',
                                              offsetVector)

        offsetVector[self.axisIndex] = self.initialSegmentLength*-0.33334

        self.endTangent = self.createMarker(self.endNode,
                                            'LeftTgnt',
                                            offsetVector)

    def createBezierLayer(self,
                          shiftIndex=0):
        self.createTangentControls()

        blockOffset = self.VERTICAL_OFFSET*3

        for jointIndex, joint in enumerate(self.jointArray):
            self.createBezierJointNetwork(joint,
                                          jointIndex,
                                          blockOffset*jointIndex,
                                          shiftIndex*self.HORIZONTAL_OFFSET*4)

    def createTangentLayer(self,
                           shiftIndex=0,
                           smoothWeight=0.5):
        targetArray = [self.snapShotNode(joint,
                                         'startPeltTangent') 
                       for joint in self.jointArray]

        self.startPeltTangentOffsetArray = []

        for samplerIndex, sampler in enumerate(self.startSamplerJointArray):
            offsetdata = PeltTangentOffset(targetArray[samplerIndex],
                                           sampler,
                                           self.startNode,
                                           self.tangentConstraint,
                                           1.0-self.parameters[samplerIndex].segmentWeights,
                                           smoothWeight,
                                           self.startNode.node) 

            self.startPeltTangentOffsetArray.append(offsetdata)

            offsetdata.hook.Parent = self.startSamplerAnchor

            offsetdata.hook.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
            offsetdata.hook.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

            offsetdata.hook.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
            offsetdata.offset.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)

        targetArray = [self.snapShotNode(joint,
                                         'endPeltTangent') 
                       for joint in self.jointArray]

        self.endPeltTangentOffsetArray = []

        for samplerIndex, sampler in enumerate(self.endSamplerJointArray):
            offsetdata = PeltTangentOffset(targetArray[samplerIndex],
                                           sampler,
                                           self.endNode,
                                           self.tangentConstraint,
                                           self.parameters[samplerIndex].segmentWeights,
                                           smoothWeight,
                                           self.startNode.node,
                                           useStartHermiteCurve=True)

            self.endPeltTangentOffsetArray.append(offsetdata)

            offsetdata.hook.Parent = self.endSamplerAnchor
            offsetdata.hook.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
            offsetdata.hook.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

            offsetdata.offset.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)
            offsetdata.hook.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1, 1, 1)

        self.tangentConstraint.SetActive(True)

    def createStartHermiteCurve(self):
        '''
            start = h_10 --> (t)=t^{3}-2t^{2}+t
        '''
        cubicExponentNode = self.relationConstraint.AddFunctionBox("Number",
                                                                   "Exponent (a^b)")

        aPlug = self.findFBboxInputPlug(cubicExponentNode.GetBox(), 'a')
        aPlug.WriteData([self.inputWeight])

        bPlug = self.findFBboxInputPlug(cubicExponentNode.GetBox(), 'b')
        bPlug.WriteData([3.0])

        squareExponentNode = self.relationConstraint.AddFunctionBox("Number",
                                                                    "Exponent (a^b)")

        aPlug = self.findFBboxInputPlug(squareExponentNode.GetBox(), 'a')
        aPlug.WriteData([self.inputWeight])

        bPlug = self.findFBboxInputPlug(squareExponentNode.GetBox(), 'b')
        bPlug.WriteData([2.0])

        addNode = self.relationConstraint.AddFunctionBox("Number",
                                                         "Add (a + b)")

        aPlug = self.findFBboxInputPlug(addNode.GetBox(), 'a')
        aPlug.WriteData([self.inputWeight])

        substractNode = self.relationConstraint.AddFunctionBox("Number",
                                                               "Subtract (a - b)")

        multiplyNode = self.relationConstraint.AddFunctionBox("Number",
                                                              "Multiply (a x b)")

        aPlug = self.findFBboxInputPlug(multiplyNode.GetBox(), 'a')
        aPlug.WriteData([2.0])

        self.relationConstraint.ConnectBoxes(squareExponentNode,
                                             'Result',
                                             multiplyNode,
                                             'b')

        self.relationConstraint.ConnectBoxes(cubicExponentNode,
                                             'Result',
                                             substractNode,
                                             'a')

        self.relationConstraint.ConnectBoxes(multiplyNode,
                                             'Result',
                                             substractNode,
                                             'b')

        self.relationConstraint.ConnectBoxes(substractNode,
                                             'Result',
                                             addNode,
                                             'b')

        return addNode

    def createHermiteNetwork(self,
                             jointData,
                             finalJoint,
                             jointIndex,
                             connectRotation=False):
        for inputJoint in jointData:
            self.prepareAnimationFields(inputJoint.node)

        self.prepareAnimationFields(finalJoint)

        startNodeSender = self.hermiteConstraint.AddSenderBox(jointData[0].node)

        endNodeSender = self.hermiteConstraint.AddSenderBox(jointData[1].node)

        blendNode = self.hermiteConstraint.AddFunctionBox("Vector",
                                                           "Middle Point")

        self.hermiteConstraint.ConnectBoxes(startNodeSender,
                                             'Translation',
                                             blendNode,
                                             'v1 (Position)')

        self.hermiteConstraint.ConnectBoxes(endNodeSender,
                                             'Translation',
                                             blendNode,
                                             'v2 (Position)')

        currentWeight = self.parameters[jointIndex].segmentWeights
        weightPlug = self.findFBboxInputPlug(blendNode.GetBox(),
                                             'ratio')

        weightPlug.WriteData([currentWeight])

        jointReceiver = self.hermiteConstraint.AddReceiverBox(finalJoint) 

        self.hermiteConstraint.ConnectBoxes(blendNode,
                                             'Result',
                                             jointReceiver,
                                             'Translation')

        if connectRotation is True:
            inputSenderA = self.hermiteConstraint.AddSenderBox(self.startNode.node)

            inputSenderB = self.hermiteConstraint.AddSenderBox(self.endNode.node)
            
            interpolateNode = self.hermiteConstraint.AddFunctionBox("Rotation",
                                                                     "Interpolate")

            self.hermiteConstraint.ConnectBoxes(inputSenderA,
                                                 'Rotation',
                                                 interpolateNode,
                                                 'Ra')

            self.hermiteConstraint.ConnectBoxes(inputSenderB,
                                                 'Rotation',
                                                 interpolateNode,
                                                 'Rb')

            weightPlug = self.findFBboxInputPlug(interpolateNode.GetBox(),
                                                 'c')

            weightPlug.WriteData([currentWeight])

            self.hermiteConstraint.ConnectBoxes(interpolateNode,
                                                'Result',
                                                jointReceiver,
                                                'Rotation')

        for inputJoint in jointData:
            inputJoint.node.Visibility = False

    def createHermiteLayer(self,
                           rigName,
                           segmentIndex):
        offsetGroup = mobu.FBModelNull('{0}_Segment{1}_zro'.format(rigName,
                                                                   segmentIndex))

        self.prepareLayer('result',
                          offsetGroup)

        for jointIndex, joint in enumerate(self.startSamplerJointArray):
            jointData = [self.startPeltTangentOffsetArray[jointIndex],
                         self.endPeltTangentOffsetArray[jointIndex]]

            self.createHermiteNetwork(jointData,
                                      self.resultJointArray[jointIndex],
                                      jointIndex)

            self.resultJointArray[jointIndex].Visibility = False

        startNodeSender = self.hermiteConstraint.AddSenderBox(self.targetSegmentStart)
        jointReceiver = self.hermiteConstraint.AddReceiverBox(offsetGroup) 

        self.hermiteConstraint.ConnectBoxes(startNodeSender,
                                            'Translation',
                                            jointReceiver,
                                            'Translation')

        self.hermiteConstraint.ConnectBoxes(startNodeSender,
                                            'Rotation',
                                            jointReceiver,
                                            'Rotation')

        self.hermiteConstraint.SetActive(True)

        for joint in self.resultJointArray:
            joint.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

        return offsetGroup

    def prepareAimLayer(self,
                        rigName,
                        segmentIndex):
        offsetGroup = mobu.FBModelNull('{0}_AimSegment{1}_zro'.format(rigName,
                                                                      segmentIndex))
        self.prepareLayer('resultAim',
                          offsetGroup)

        for index in xrange(len(self.resultJointArray)):
            self.resultAimJointArray[index].Parent = self.resultJointArray[index]

            self.resultAimJointArray[index].PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

            self.resultAimJointArray[index].PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

            if self.parameters[index].segmentWeights == 1.0:
                constraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

                constraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                        self.resultAimJointArray[index])

                constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                        self.endNode.node)

                constraint.Active = True

                constraint.Name = '{}_attach1'.format(self.resultAimJointArray[index].Name)

                self.attachConstraints.append(constraint)

            else:
                targetObject = None
                nextIndex = index+1
                if nextIndex > len(self.resultJointArray)-1:
                    targetObject = self.endNode.node
                else:
                    targetObject = self.resultJointArray[nextIndex]

                targetConstraint = self.constraintUtils.createTargetConstraint(self.resultAimJointArray[index],
                                                                               targetObject,
                                                                               spaceObject=self.resultJointArray[index])

                targetConstraint[0].Name = '{}_attach1'.format(self.resultAimJointArray[index].Name)

                self.attachConstraints.append(targetConstraint[0])
            
        Globals.Scene.Evaluate()

        offsetGroup.FBDelete()

    def attachGameJoints(self):
        for index, bone in enumerate(self.jointArray):
            parentConstraint = self.constraintUtils.constraintParts(bone,
                                                                    self.resultAimJointArray[index],
                                                                    preserveOffset=True)

            parentConstraint.Name = '{}_attach1'.format(bone.Name)

            self.peltFinalConstraints.append(parentConstraint)

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_ATTRIBUTES:
            currentValue = getattr(self, 
                                   attribute)

            if currentValue is None:
                reportData += '\n\t{0}:{1}'.format(attribute,
                                                   None)
                continue

            if isinstance(currentValue, 
                          list):
                reportData += '\n\t{0}:'.format(attribute)
                for node in currentValue:
                    reportData += '\n\t\t{0}'.format(node.Name)
            else:
                reportData += '\n\t{0}:{1}'.format(attribute,
                                                   currentValue.node.Name)

        return reportData


class PeltChainRig(object):
    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    def __init__(self):
        self.constraintUtils = IkUtils.ConstraintUtils()

        self.nurbsUtils = ikFkParallelChainBuilder.Build()

        self.numberOfControllers = 8

        self.createFkHierarchy = False

        self.allowWorldOrientFk = False

        self.rigName = None

        self.chainPrefix = None

        self.jointRootName = None

        self.jointRoot = None

        self.jointEndName = None

        self.jointEnd = None

        self.applyOnWholeChain = False

        self.root = None

        self.relationConstraint = None

        self.bezierConstraint = None

        self.tangentConstraint = None

        self.hermiteConstraint = None

        self.ikControlsGroup = None

        self.driversGroup = None

        self.attachFolder = None

        self.peltjointArray = []

        self.animationcontrollers = []

        self.fkControllers = []

        self.fkZeroWorldOrients = []

        self.segments = []

        self.distributedJoints = []

        self.peltFinalConstraints = []

        self.zeroWorldOrientConstraints = []

        self.rootConstraint = None

    def reset(self):
        self.distributedJoints = []

        self.numberOfControllers = 8

        self.createFkHierarchy = False

        self.rigName = None

        self.chainPrefix = None

        self.jointRootName = None

        self.jointRoot = None

        self.jointEndName = None

        self.jointEnd = None

        self.root = None

        self.relationConstraint = None

        self.bezierConstraint = None

        self.tangentConstraint = None

        self.hermiteConstraint = None

        self.ikControlsGroup = None

        self.driversGroup = None

        self.attachFolder = None

        self.peltjointArray = []

        self.animationcontrollers = []

        self.fkZeroWorldOrients = []

        self.zeroWorldOrientConstraints = []

        self.segments = []

    def collectSkinJointsFromRoot(self):
        jointUtils = IkUtils.JointChain()

        self.peltjointArray = list(jointUtils.getJointChainFromRoot(self.jointRootName))

        self.peltjointArray.pop(0)

    def collectSkinJoints(self):
        jointUtils = IkUtils.JointChain()

        includeNamespace = False
        if ':' in self.jointRootName:
            includeNamespace = True

        startJoint = Scene.FindModelByName(self.jointRootName,
                                           includeNamespace=includeNamespace)

        includeNamespace = False
        if ':' in self.jointEndName:
            includeNamespace = True

        endJoint = Scene.FindModelByName(self.jointEndName,
                                         includeNamespace=includeNamespace)

        self.peltjointArray = jointUtils.getDelimitedJointChainFromNodes(startJoint,
                                                                         endJoint)

    def initializeRelationConstraint(self):
        relationConstraintName = '{0}_segmentConstraint1'.format(self.rigName)

        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)

        self.relationConstraint.SetActive(True)

    def initializeTangentConstraint(self):
        relationConstraintName = '{0}_tangentConstraint1'.format(self.rigName)

        self.tangentConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)

    def initializeHermiteConstraint(self):
        relationConstraintName = '{0}_hermiteConstraint1'.format(self.rigName)

        self.hermiteConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)

    def initializeBezierConstraint(self):
        relationConstraintName = '{0}_bezierConstraint1'.format(self.rigName)

        self.bezierConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)

        self.bezierConstraint.SetActive(True)

    def setCurveToLinear(self,
                         splineObject):
        for index in xrange(splineObject.PathKeyGetCount()):
            curvePoint = splineObject.PathKeyGet(index)

            splineObject.PathKeySetLeftRightTangent(index,
                                                    curvePoint,
                                                    curvePoint,
                                                    curvePoint)

    def bundleControllers(self,
                          controllers):
        self.animationcontrollers = [PeltAnimationController(node) for node in controllers]

    def createControllersFromPath(self):
        splineObject = self.nurbsUtils.createPathFromJoints(self.peltjointArray,
                                                            name=str(self.rigName))

        self.setCurveToLinear(splineObject)

        splineObject = self.nurbsUtils.rebuildCurve(splineObject,
                                                    self.numberOfControllers,
                                                    deleteInputCurve=True)

        self.setCurveToLinear(splineObject)

        splineObject.Name = '{}'.format(self.rigName)

        ikControlsSize = 250

        controllers = IkRigging.IkSplineLayer().buildTwistDriverFromCurve(splineObject.Name,
                                                                          ikControlsSize)

        splineObject.FBDelete()

        return controllers

    def distributeAlongChain(self):
        startIndex = 0
        lastControlIndex = 0
        lastJoint = self.peltjointArray[len(self.peltjointArray)-1]

        self.distributedJoints = []
        for index in xrange(startIndex,
                            len(self.peltjointArray),
                            self.numberOfControllers):
            self.distributedJoints.append(self.peltjointArray[index])

            lastControlIndex = index

        if lastControlIndex < len(self.peltjointArray)-1:
            self.distributedJoints.append(lastJoint)

        splineObject = self.nurbsUtils.createPathFromJoints(self.distributedJoints,
                                                            name=str(self.rigName))

        self.setCurveToLinear(splineObject)

        splineObject.Name = '{}'.format(self.rigName)

        ikControlsSize = 250

        controllers = IkRigging.IkSplineLayer().buildTwistDriverFromCurve(splineObject.Name,
                                                                          ikControlsSize)

        for node in controllers:
            node.Name = '{}_IK'.format(node.Name)
        splineObject.FBDelete()

        return controllers

    def createAnimationControllers(self):
        controllers = self.distributeAlongChain()

        self.bundleControllers(controllers)

    def collectSegments(self):
        self.nurbsUtils.pointLimit = len(self.animationcontrollers)-1

        self.nurbsUtils.curveSamples = []

        for controller in self.animationcontrollers:
            position = mobu.FBVector3d(0, 0, 0)

            controller.node.GetVector(position)
            self.nurbsUtils.curveSamples.append(position)

        sourceCurvePointPositions = []
        for node in self.peltjointArray:
            position = mobu.FBVector3d(0, 0, 0)

            node.GetVector(position)
            sourceCurvePointPositions.append(position)

        self.nurbsUtils.startIndex = 0

        self.segments = []

        for nodeIndex in xrange(len(self.animationcontrollers)-1):
            currentSegment = PeltSegment()

            self.segments.append(currentSegment)

        for pointIndex, point in enumerate(sourceCurvePointPositions):
            knotData = self.nurbsUtils.getClosestPointOnCurve(point,
                                                              threshold=0.03)

            #knotData.createPoint(useSource=False)

            knotData.sourcePoint = point

            knotData.blendWeights()

            self.segments[knotData.samplingIndex].parameters.append(knotData)

            self.segments[knotData.samplingIndex].startNode = self.animationcontrollers[knotData.samplingIndex]

            self.segments[knotData.samplingIndex].endNode = self.animationcontrollers[knotData.samplingIndex+1]

            self.segments[knotData.samplingIndex].jointArray.append(self.peltjointArray[pointIndex])

        for node in self.segments:
            node.relationConstraint = self.relationConstraint 

        for controller in self.animationcontrollers:
            controller.createOffsetGroup()

    def collectChainSegments(self):
        self.nurbsUtils.pointLimit = len(self.animationcontrollers)-1

        self.nurbsUtils.curveSamples = []

        for controller in self.animationcontrollers:
            position = mobu.FBVector3d(0, 0, 0)

            controller.node.GetVector(position)
            self.nurbsUtils.curveSamples.append(position)

        sourceCurvePointPositions = []
        for node in self.peltjointArray:
            position = mobu.FBVector3d(0, 0, 0)

            node.GetVector(position)
            sourceCurvePointPositions.append(position)

        self.nurbsUtils.startIndex = 0

        self.segments = []

        lastSegmentRemainingBones = len(self.animationcontrollers) % self.numberOfControllers

        sampleLimit = len(sourceCurvePointPositions)-1

        for sampleIndex in xrange(len(self.animationcontrollers)-1):
            currentSegment = PeltSegment()

            curveSegmentLength = 0.0
            lengthArray = []
            for index in xrange(self.numberOfControllers):
                pointIndex = sampleIndex*self.numberOfControllers + index
                if lastSegmentRemainingBones > 0:
                    if pointIndex > sampleLimit:
                        break

                if pointIndex > sampleLimit:
                    pointIndex = sampleLimit

                nextIndex = pointIndex+1
                if nextIndex > sampleLimit:
                    nextIndex = sampleLimit

                lengthArray.append(curveSegmentLength)
                curveSegmentLength += (sourceCurvePointPositions[pointIndex]-sourceCurvePointPositions[nextIndex]).Length()

                knotData = ikFkParallelChainBuilder.CurveKnot()

                knotData.goalPoint = sourceCurvePointPositions[pointIndex]

                knotData.samplingIndex = pointIndex

                currentSegment.parameters.append(knotData)

                currentSegment.jointArray.append(self.peltjointArray[pointIndex])

            for dataIndex in xrange(len(currentSegment.parameters)):
                currentSegment.parameters[dataIndex].segmentWeights = 0.0

                if curveSegmentLength> 0.0:
                    currentSegment.parameters[dataIndex].segmentWeights = lengthArray[dataIndex] / curveSegmentLength

            currentSegment.startNode = self.animationcontrollers[sampleIndex]

            currentSegment.endNode = self.animationcontrollers[sampleIndex+1]

            self.segments.append(currentSegment)

        for node in self.segments:
            node.relationConstraint = self.relationConstraint 

        for controller in self.animationcontrollers:
            controller.createOffsetGroup()

    def alignControllerToSourceJoint(self):
        constraintUtils = IkUtils.ConstraintUtils()

        for index, controller in enumerate(self.animationcontrollers):
            constraintNode = constraintUtils.alignWithConstraint(controller.offsetNode,
                                                                 self.distributedJoints[index])

            constraintUtils.bakeConstraintValue(constraintNode)

    def createPositionLayerWithRelationConstraint(self):
        self.initializeRelationConstraint()

        for segmentIndex, segment in enumerate(self.segments):
            segment.relationConstraint = self.relationConstraint

            segment.createPositionLayer(shiftIndex=segmentIndex)

    def createPositionLayer(self):
        self.initializeRelationConstraint()

        for segmentIndex, segment in enumerate(self.segments):
            segment.relationConstraint = self.relationConstraint

            segment.createTargetConstraints()

    def createBezierLayer(self):
        for segmentIndex, segment in enumerate(self.segments):
            segment.bezierConstraint = self.bezierConstraint

            segment.createBezierLayer(shiftIndex=segmentIndex)

    def createTangentLayer(self,
                           smoothWeight=0.5):
        self.initializeTangentConstraint()

        for segmentIndex, segment in enumerate(self.segments):
            segment.tangentConstraint = self.tangentConstraint

            segment.createTangentLayer(shiftIndex=segmentIndex,
                                       smoothWeight=smoothWeight)

    def createHermiteLayer(self):
        self.initializeHermiteConstraint()

        self.driversGroup = mobu.FBModelNull('{}_driverRoot1'.format(self.rigName))

        for segmentIndex, segment in enumerate(self.segments):
            segment.hermiteConstraint = self.hermiteConstraint

            offsetGroup = segment.createHermiteLayer(self.rigName,
                                                     segmentIndex)

            offsetGroup.Parent = self.driversGroup

    def bindGameJoints(self):
        for segment in self.segments:
            segment.attachGameJoints()

    def createAimLayer(self):
        for segmentIndex, segment in enumerate(self.segments):
            segment.prepareAimLayer(self.rigName,
                                    segmentIndex)

    def createWorldOrientConstraint(self,
                                    joint,
                                    chainRoot):
        constraint = Constraint.CreateConstraint(Constraint.ROTATION)

        constraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                joint)

        constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                chainRoot)

        constraint.Snap()

        constraint.Name = '{}_worldOrient1'.format(joint.Name)
        constraint.Active = True

        self.zeroWorldOrientConstraints.append(constraint)
        constraint.Lock = True

    def createKnotConstraint(self,
                             knot,
                             target):
        constraint = Constraint.CreateConstraint(Constraint.POSITION)

        constraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                knot)

        constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                target)

        constraint.Name = '{}_knotConstraint1'.format(knot.Name)
        constraint.Active = True

        self.zeroWorldOrientConstraints.append(constraint)
        constraint.Lock = True

    def createFkHierarchyControls(self):
        self.fkControllers = []

        self.fkZeroWorldOrients = []

        for controlIndex, control in enumerate(self.animationcontrollers):
            marker = mobu.FBModelMarker(control.node.Name.replace('IK','FK'))
            marker.PropertyList.Find('Look').Data = 7
            marker.PropertyList.Find('Size').Data = control.node.PropertyList.Find('Size').Data * 0.35

            targetMatrix = mobu.FBMatrix()
            control.node.GetMatrix(targetMatrix)
            marker.SetMatrix(targetMatrix)

            marker.Show = True

            self.fkControllers.append(marker)
            if controlIndex == 0:
                continue

            marker.Parent = self.fkControllers[controlIndex-1]
            marker.SetMatrix(targetMatrix)

        for controlIndex in xrange(len(self.fkControllers)):
            drivenBone = self.animationcontrollers[controlIndex].offsetNode
            targetParent = self.fkControllers[controlIndex]
            
            parentConstraint = self.constraintUtils.constraintParts(drivenBone,
                                                                    targetParent)

            parentConstraint.Name = '{}_glue1'.format(targetParent.LongName)

            self.driverFolder.ConnectSrc(parentConstraint)

        Globals.Scene.Evaluate()

        offset = mobu.FBModelNull('{0}_fkControls'.format(self.rigName))

        offset.Show = True
        offset.PropertyList.Find('Look').Data = 0

        nodeMatrix = mobu.FBMatrix()
        self.fkControllers[0].GetMatrix(nodeMatrix)
        offset.SetMatrix(nodeMatrix)

        self.fkControllers[0].Parent = offset

        offset.Parent = self.root

        if self.allowWorldOrientFk is False:
            for controlIndex in xrange(1, len(self.fkControllers)):
                self.fkControllers[controlIndex].PropertyList.Find('Enable Rotation DOF').Data = True
                self.fkControllers[controlIndex].PreRotation = self.fkControllers[controlIndex].PropertyList.Find('Rotation (Lcl)').Data 
                self.fkControllers[controlIndex].PropertyList.Find('Rotation Offset').Data= self.fkControllers[controlIndex].PropertyList.Find('Translation (Lcl)').Data

                self.fkControllers[controlIndex].PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
                self.fkControllers[controlIndex].PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0) 

        else:
            controlCount = len(self.fkControllers) - 1

            for controlIndex in xrange(1, len(self.fkControllers)):
                zeroGroup = mobu.FBModelNull(self.fkControllers[controlIndex].Name.replace('FK',
                                                                                           'ZeroWorldOrient'))

                zeroGroup.PropertyList.Find('Look').Data = 0
                zeroGroup.PropertyList.Find('Size').Data = 0.0

                self.fkZeroWorldOrients.append(zeroGroup)

                targetMatrix = mobu.FBMatrix()
                self.fkControllers[controlIndex].GetMatrix(targetMatrix)
                zeroGroup.SetMatrix(targetMatrix)

                zeroGroup.Parent = self.fkControllers[controlIndex].Parent
                self.fkControllers[controlIndex].Parent = zeroGroup

                self.fkControllers[controlIndex].PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)
                self.fkControllers[controlIndex].PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0) 

                self.createWorldOrientConstraint(zeroGroup,
                                                 offset)

                if controlIndex >= controlCount:
                    continue

                knot = mobu.FBModelMarker(self.fkControllers[controlIndex].Name.replace('FK',
                                                                                        'Knot'))

                knot.PropertyList.Find('Look').Data = 7
                knot.PropertyList.Find('Size').Data = self.fkControllers[controlIndex].PropertyList.Find('Size').Data

                self.fkControllers[controlIndex+1].GetMatrix(targetMatrix)
                knot.SetMatrix(targetMatrix)
                knot.Show = True
                knot.Parent = self.fkControllers[controlIndex]

                knot.PropertyList.Find('Size').Data = 0.0

                self.createKnotConstraint(knot,
                                          self.fkControllers[controlIndex+1])

        self.rigfkGroup = mobu.FBGroup('{0}_FkControls'.format(self.root.Name))

        for control in self.fkControllers:
            self.rigfkGroup.ConnectSrc(control)

        self.rigGroup.ConnectSrc(self.rigfkGroup)

    def packageComponents(self):
        self.root = mobu.FBModelNull('{0}_chainRig1'.format(self.rigName))

        self.ikControlsGroup = mobu.FBModelNull('{0}_ikControls'.format(self.rigName))

        for controller in self.animationcontrollers:
            controller.offsetNode.Parent = self.ikControlsGroup

        for node in [self.ikControlsGroup,
                     self.driversGroup]:
            node.Parent = self.root

        self.rigFolder = mobu.FBFolder('{0}_Constraints1'.format(self.rigName),
                                                                 self.segments[0].attachConstraints[0])

        self.attachFolder = mobu.FBFolder('{0}_attachs1'.format(self.rigName),
                                                                self.segments[0].attachConstraints[0])

        attachConstraints = []
        for segment in self.segments:
            attachConstraints.extend(segment.attachConstraints)

        attachConstraints.extend(self.zeroWorldOrientConstraints)

        self.peltFinalConstraints = []
        for segment in self.segments:
            self.peltFinalConstraints.extend(segment.peltFinalConstraints)

        for constraint in self.peltFinalConstraints:
            self.attachFolder.ConnectSrc(constraint)

        for constraint in attachConstraints:
            self.attachFolder.ConnectSrc(constraint)

        self.driverFolder = mobu.FBFolder('{0}_drivers1'.format(self.rigName),
                                                                          self.relationConstraint.GetConstraint())

        for constraint in [self.relationConstraint,
                           self.tangentConstraint,
                           self.hermiteConstraint]:
            self.driverFolder.ConnectSrc(constraint.GetConstraint())

        self.rigFolder.Items.remove(self.segments[0].attachConstraints[0])

        pointConstraints = []
        for segment in self.segments:
            pointConstraints.extend(segment.pointConstraints)

        for constraint in [self.attachFolder,
                           self.driverFolder]:
            self.rigFolder.Items.append(constraint)

        for constraint in pointConstraints:
            self.driverFolder.Items.append(constraint)

        self.rigGroup = mobu.FBGroup('{0}_rig'.format(self.root.Name))
        self.rigIkGroup = mobu.FBGroup('{0}_IkControls'.format(self.root.Name))

        for control in self.animationcontrollers:
            self.rigIkGroup.ConnectSrc(control.node)

        self.rigGroup.ConnectSrc(self.rigIkGroup)

        Globals.Scene.Evaluate()
        for segment in self.segments:
            for parentConstraint in segment.attachConstraints:
                parentConstraint.Lock = True

        if self.createFkHierarchy is False:
            return

        self.createFkHierarchyControls()

        for constraint in self.zeroWorldOrientConstraints:
            self.attachFolder.ConnectSrc(constraint)

    def collectChain(self,
                     startJointName,
                     endJointName):
        for name in [startJointName,
                     endJointName]:
            includeNamespace = False

            if ':' in startJointName:
                includeNamespace = True

            startJoint = Scene.FindModelByName(startJointName,
                                               includeNamespace=includeNamespace)

            if startJoint is None:
                return False

        self.collectSkinJoints()

        return True

    def collectChainFromRoot(self,
                             startJointName):
        includeNamespace = False

        if ':' in startJointName:
            includeNamespace = True

        startJoint = Scene.FindModelByName(startJointName,
                                           includeNamespace=includeNamespace)

        if startJoint is None:
            return False

        jointUtils = IkUtils.JointChain()
        self.peltjointArray = jointUtils.getJointChainFromRoot(startJointName)

        return True

    def computeOffset(self,
                      targetNode,
                      drivenNode,
                      inputConstraint):
        offsetTproperty = None

        offsetRproperty = None

        for property in inputConstraint.PropertyList:
            if not property.Name.startswith(targetNode.LongName):
                continue

            if property.Name.endswith('.Offset T'):
                offsetTproperty = property

            if property.Name.endswith('.Offset R'):
                offsetRproperty = property

        targetWorldMatrix = mobu.FBMatrix()

        targetNode.GetMatrix(targetWorldMatrix)

        targetWorldMatrix.Inverse()

        drivenWorldMatrix = mobu.FBMatrix()

        drivenNode.GetMatrix(drivenWorldMatrix)

        offsetMatrix = targetWorldMatrix*drivenWorldMatrix

        offsetTranslation = mobu.FBVector4d()

        mobu.FBMatrixToTranslation(offsetTranslation, 
                                   offsetMatrix)

        offsetRotation = mobu.FBVector3d()

        mobu.FBMatrixToRotation(offsetRotation, 
                                offsetMatrix, 
                                mobu.FBRotationOrder.kFBXYZ)

        offsetTranslation = mobu.FBVector3d(offsetTranslation[0],
                                            offsetTranslation[1],
                                            offsetTranslation[2])

        offsetTproperty.Data = offsetTranslation*100.0 

        offsetRproperty.Data = offsetRotation

    def findFBboxInputPlug(self,
                           relationConstraintBox,
                           attributeName):
        for node in relationConstraintBox.AnimationNodeInGet().Nodes:
            if node.Name != attributeName:
                continue

            return node

        return None

    def connectWeighProperty(self,
                             inputSlider,
                             inputConstraint,
                             relationConstraint,
                             currentDriver,
                             receiver,
                             driverConfiguration):
        weightProperty = None

        for property in inputConstraint.PropertyList:
            if not property.Name.startswith(currentDriver.LongName):
                continue

            if not property.Name.endswith('.Weight'):
                continue

            weightProperty = property

        if weightProperty is None:
            return

        if weightProperty.IsAnimated() is False:
            weightProperty.SetAnimated(True)

        inputSlider.PropertyList.Find('Translation (Lcl)').SetAnimated(True)

        sender = relationConstraint.AddSenderBox(inputSlider)

        sender.SetGlobalTransforms(False)

        vectorNode = relationConstraint.AddFunctionBox("Converters",
                                                       "Vector to Number")


        multiplyNode = relationConstraint.AddFunctionBox("Number",
                                                         "Multiply (a x b)")

        relationConstraint.ConnectBoxes(sender,
                                        'Lcl Translation',
                                        vectorNode,
                                        'V')

        if driverConfiguration is False:
            relationConstraint.ConnectBoxes(vectorNode,
                                            'Y',
                                            multiplyNode,
                                            'a')

        else:
            substractNode = relationConstraint.AddFunctionBox("Number",
                                                              "Subtract (a - b)")

            aPlug = self.findFBboxInputPlug(substractNode.GetBox(), 'a')
            aPlug.WriteData([1.0])

            relationConstraint.ConnectBoxes(vectorNode,
                                            'Y',
                                            substractNode,
                                            'b')

            relationConstraint.ConnectBoxes(substractNode,
                                            'Result',
                                            multiplyNode,
                                            'a')

        bPlug = self.findFBboxInputPlug(multiplyNode.GetBox(), 'b')
        bPlug.WriteData([100.0])

        relationConstraint.ConnectBoxes(multiplyNode,
                                        'Result',
                                        receiver,
                                        weightProperty.Name)

    def driveWeighsProperty(self,
                            inputSlider,
                            inputConstraint,
                            relationConstraint):
        if not isinstance(inputConstraint,
                          mobu.FBConstraint):
            return None

        driverCount = inputConstraint.ReferenceGetCount(self.PARENT_NODE_INDEX)

        driverConfigurations = [True, 
                                False]

        if driverCount > 2:
            driverCount = 2

        receiver = relationConstraint.AddReceiverBox(inputConstraint)

        for index in xrange(driverCount):
            currentDriver = inputConstraint.ReferenceGet(self.PARENT_NODE_INDEX,
                                                         index)

            if currentDriver is None:
                return

            self.connectWeighProperty(inputSlider,
                                      inputConstraint,
                                      relationConstraint,
                                      currentDriver,
                                      receiver,
                                      driverConfigurations[index])

    def attachToAsset(self,
                      dummyNode,
                      overrideFolderName,
                      parentTargetName,
                      rootTarget,
                      inputSlider):
        if self.root is None:
            return

        self.root.Parent = dummyNode

        self.rigFolder.Name = overrideFolderName

        if not self.peltjointArray:
            return

        alignParent = Scene.FindModelByName(parentTargetName)

        if alignParent is None:
            return

        if inputSlider is None:
            rootTarget = None

        if rootTarget is None:
            parentConstraint = self.constraintUtils.constraintParts(self.root,
                                                                    alignParent,
                                                                    preserveOffset=True)

        else:
            parentConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)

            parentConstraint.ReferenceAdd(self.DRIVEN_NODE_INDEX,
                                          self.root)

            parentConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                          alignParent)

            parentConstraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                          rootTarget)

            scaleProperty = parentConstraint.PropertyList.Find('Scaling Affects Translation')

            scaleProperty.Data = False

            Globals.Scene.Evaluate()

            self.computeOffset(alignParent,
                               self.root,
                               parentConstraint)

            self.computeOffset(rootTarget,
                               self.root,
                               parentConstraint)

            parentConstraint.Active = True

            parentConstraint.Lock = True

            relationConstraintName = '{0}_blendWeight1'.format(self.root.Name)

            relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)

            relationConstraint.SetActive(True)

            self.driveWeighsProperty(inputSlider,
                                     parentConstraint,
                                     relationConstraint)

            self.rigFolder.ConnectSrc(relationConstraint.GetConstraint())

        parentConstraint.Name = '{}_attach1'.format(self.root.Name)

        self.rigFolder.ConnectSrc(parentConstraint)

        self.rootConstraint = parentConstraint

    def build(self):
        if self.applyOnWholeChain is True:
            chainWasCollectedProperly = self.collectChainFromRoot(self.jointRootName)

        else:
            chainWasCollectedProperly = self.collectChain(self.jointRootName,
                                                          self.jointEndName)

        if chainWasCollectedProperly is False:
            return

        self.createAnimationControllers()

        self.collectChainSegments()

        self.alignControllerToSourceJoint()

        self.createPositionLayer()

        self.createTangentLayer()

        self.createHermiteLayer()

        self.createAimLayer()

        self.bindGameJoints()

        self.packageComponents()


class TestBuilder(object):
    def __init__(self):
        self.rigComponent = PeltChainRig()

        self.rigName = 'peltSegment'

        self.numberOfControllers = 4

        self.endJoint = None

    def createCurveChain(self):
        positions = [mobu.FBVector3d(13.56, 56.56, 0.0),
                     mobu.FBVector3d(19.44, 74.62, 0.0),
                     mobu.FBVector3d(33.42, 83.35, 0.0),
                     mobu.FBVector3d(48.69, 86.20, 0.0),
                     mobu.FBVector3d(64.30, 86.20, 0.0),
                     mobu.FBVector3d(78.98, 79.97, 0.0),
                     mobu.FBVector3d(87.32, 72.04, 0.0),
                     mobu.FBVector3d(92.28, 61.51, 0.0),
                     mobu.FBVector3d(87.65, 44.12, 0.0),
                     mobu.FBVector3d(78.78, 31.73, 0.0),
                     mobu.FBVector3d(63.69, 17.65, 0.0),
                     mobu.FBVector3d(52.75, 6.26, 0.0)]

        rotation = [mobu.FBVector3d(0,0,67.29),
                    mobu.FBVector3d(0,0,45.64),
                    mobu.FBVector3d(0,0,19.20),
                    mobu.FBVector3d(0,0,-0.72),
                    mobu.FBVector3d(0,0,-4.97),
                    mobu.FBVector3d(0,0,-20.14),
                    mobu.FBVector3d(0,0,-57.62),
                    mobu.FBVector3d(0,0,-87.66),
                    mobu.FBVector3d(0,0,-108.77),
                    mobu.FBVector3d(0,0,-136.77),
                    mobu.FBVector3d(0,0,-123.75),
                    mobu.FBVector3d(0,0,-126.47)]

        jointArray = []

        for jointIndex in xrange(12):
            joint = mobu.FBModelNull('{}_joint{}'.format(self.rigName,
                                                         jointIndex+1))

            joint.PropertyList.Find('Translation (Lcl)').Data = positions[jointIndex]

            joint.PropertyList.Find('Rotation (Lcl)').Data = rotation[jointIndex]

            joint.Show = True

            jointArray.append(joint)

            self.endJoint = joint.LongName

            if jointIndex == 0:
                self.rigComponent.jointRootName = joint.LongName     

        for jointIndex in xrange(1, 12):
            jointArray[jointIndex].Parent = jointArray[jointIndex-1]

    def createTestChain(self):
        jointRoot = mobu.FBModelNull('{}_joints'.format(self.rigName))

        for jointIndex in xrange(40):
            joint = mobu.FBModelNull('{}_joint{}'.format(self.rigName,
                                                         jointIndex+1))

            joint.Translation = mobu.FBVector3d(10.0*jointIndex, 0.0, 0.0)

            joint.Parent = jointRoot

            joint.Show = True

    def updateRigParameters(self):
        self.rigComponent.rigName = self.rigName

        self.rigComponent.jointRootName = 'peltSegment_joints'

        self.rigComponent.numberOfControllers = self.numberOfControllers

        self.rigComponent.collectSkinJoints()

    def createRig(self,
                  loadDefaultAsset=True,
                  createBezierRig=False):
        if loadDefaultAsset is True:
            mobu.FBApplication().FileNew()

            self.createTestChain()

        self.updateRigParameters()

        self.rigComponent.build()

    def createRigFromDeer(self):
        self.rigComponent.rigName = self.rigName

        self.rigComponent.jointRootName = 'peltSegment_joints'

        self.rigComponent.numberOfControllers = 3

        self.rigComponent.build()

    def createRigFromCurvePoints(self):
        mobu.FBApplication().FileNew()

        self.createCurveChain()

        self.rigComponent.jointEndName = self.endJoint 

        self.rigComponent.rigName = self.rigName

        self.rigComponent.numberOfControllers = 3

        self.rigComponent.createFkHierarchy = True

        self.rigComponent.build()