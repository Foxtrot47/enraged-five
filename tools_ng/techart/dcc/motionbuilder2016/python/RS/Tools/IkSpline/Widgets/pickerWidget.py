import inspect
import os
from functools import partial

import pyfbsdk as mobu


from PySide import QtGui, QtCore

from RS import Globals
from RS.Tools import UI
from RS import Config, Core 


class PickerWidget(QtGui.QWidget):
    """
        character selection widget
    """
    def __init__(self, 
                 groupLabel):
        # Main window settings
        self.groupLabel = groupLabel

        super(PickerWidget, self).__init__()
        
        self.buttonSize = 18

        self.setupUi()

    def setupUi(self):
        """
        Internal Method

        Setup the UI
        """
        # Create Layouts
        createHandleButton = QtGui.QPushButton('Create weapon handle')
        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.setContentsMargins(0, 0, 0, 0)

        self.mainLayout.addWidget(self._buildNodePicker())
        self.setLayout(self.mainLayout)

    def _buildNodePicker(self):
        pickObjectWidget = QtGui.QGroupBox(self.groupLabel)

        self.handleTextLine = QtGui.QLineEdit()
        selectHandleButton = QtGui.QPushButton("<")

        pickObjectLayout = QtGui.QHBoxLayout()
        pickObjectLayout.setContentsMargins(8, 4, 8, 4)

        selectHandleButton.setMaximumWidth(self.buttonSize)
        selectHandleButton.setMaximumHeight(self.buttonSize)
        selectHandleButton.setToolTip('Pick selected Joint')

        self.handleTextLine.setReadOnly(True)

        pickObjectLayout.addWidget(self.handleTextLine)
        pickObjectLayout.addWidget(selectHandleButton)

        pickObjectWidget.setLayout(pickObjectLayout)

        selectHandleButton.pressed.connect(partial(self._setHandleTarget,
                                                   self.handleTextLine))

        return pickObjectWidget

    def _setHandleTarget(self, 
                         lineControl):
        """
            Internal Method
            Set the current selection name into the qlineedit text field
            
            Args:
                lineControl (qlineedit).
        """
        selectedModels = mobu.FBModelList()
        mobu.FBGetSelectedModels(selectedModels)

        if len(selectedModels)<1:
            return

        lineControl.setText(selectedModels[0].LongName)
