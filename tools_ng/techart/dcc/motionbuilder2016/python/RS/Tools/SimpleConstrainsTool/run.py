from RS.Tools.SimpleConstrainsTool import SimpleConstrainTool
def Run(show=True):
    tool = SimpleConstrainTool.SimpleConstraintsTool()

    if show:
        tool.show()
    return tool
