from PySide import QtCore, QtGui
from RS.Tools import UI
import pyfbsdk as mobu
from RS import Globals
from RS.Core.Scene.models import searchSceneByTag
from RS.Utils.Scene import Constraint
from RS.Utils import Scene
from functools import partial


class TabType(object):
    RenameConstraintTab = 0
    FindConstraintTab = 1


class SimpleConstraintsTool(UI.QtMainWindowBase):

    def __init__(self, parent=None):
        super(SimpleConstraintsTool, self).__init__(title="Simple Constraints Tool", size=(300, 200), dockable=False,
                                                    parent=parent)
        self.parentModelNameLabelText = "Parent Model"
        self.childModelNameLabelText = "Child Model"
        self._setupUi()

    def _setupUi(self):
        
        # create widgets
        mainWidget = QtGui.QWidget()
        self.tabWidget = QtGui.QTabWidget()
        self.createConstraintTab = QtGui.QWidget()
        self.parentGroupBox = QtGui.QGroupBox(self.createConstraintTab)
        self.parentGroupBoxVerticalLayout = QtGui.QVBoxLayout(self.parentGroupBox)
        self.childGroupBox = QtGui.QGroupBox(self.createConstraintTab)
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.childGroupBox)
        self.constraintGroupBox = QtGui.QGroupBox(self.createConstraintTab)
        self.parentChildPushButton = QtGui.QPushButton(self.constraintGroupBox)
        self.rotationConstraintPushButton = QtGui.QPushButton(self.constraintGroupBox)
        self.translationConstraintPushButton = QtGui.QPushButton(self.constraintGroupBox)
        self.propertiesCheckBox = QtGui.QCheckBox(self.constraintGroupBox)
        self.constraintSettingGroupBox = QtGui.QGroupBox(self.createConstraintTab)
        self.constraintNamelineEdit = QtGui.QLineEdit(self.constraintSettingGroupBox)
        self.parentModelNameLabel = QtGui.QLabel(self.parentGroupBox)
        self.parentGroupBoxVerticalLayout.addWidget(self.parentModelNameLabel)
        self.getParentModelPushButton = QtGui.QPushButton(self.parentGroupBox)
        self.parentGroupBoxVerticalLayout.addWidget(self.getParentModelPushButton)      
        self.childModelNameLabel = QtGui.QLabel(self.childGroupBox)
        self.verticalLayout_3.addWidget(self.childModelNameLabel)
        self.getChlidModelPushButton = QtGui.QPushButton(self.childGroupBox)
        self.verticalLayout_3.addWidget(self.getChlidModelPushButton)
        self.swapNodesPushButton = QtGui.QPushButton(self.constraintSettingGroupBox)
        self.getParentChildPushButton = QtGui.QPushButton(self.constraintSettingGroupBox)
        self.activeWithSnapCheckBox = QtGui.QCheckBox(self.constraintSettingGroupBox)
        self.activeWitoutSanpCheckBox = QtGui.QCheckBox(self.constraintSettingGroupBox)
        self.lockedCheckBox = QtGui.QCheckBox(self.constraintSettingGroupBox)
        self.weightLabel = QtGui.QLabel(self.constraintSettingGroupBox)
        self.horizontalSlider = QtGui.QSlider(self.constraintSettingGroupBox)
        self.selecterCheckBox = QtGui.QCheckBox(self.constraintSettingGroupBox)
        self.parentNodeGroupBox = QtGui.QGroupBox(self.createConstraintTab)
        self.parentNodesListWidget = QtGui.QListWidget(self.parentNodeGroupBox)
        self.findParentNodeLineEdit = QtGui.QLineEdit(self.parentNodeGroupBox)
        self.childNodeGroupBox = QtGui.QGroupBox(self.createConstraintTab)
        self.childNodesListWidget = QtGui.QListWidget(self.childNodeGroupBox)
        self.findChildNodeLineEdit = QtGui.QLineEdit(self.childNodeGroupBox)
        self.renameConstraintTab = QtGui.QWidget()
        self.renameConstraintGroupBox = QtGui.QGroupBox(self.renameConstraintTab)
        self.searchConstraintByPatternLabel = QtGui.QLabel(self.renameConstraintGroupBox)
        self.constraintNameLabel = QtGui.QLabel(self.renameConstraintGroupBox)
        self.constraintsListWidget = QtGui.QListWidget(self.renameConstraintGroupBox)
        self.autoCreateNameCheckBox = QtGui.QCheckBox(self.renameConstraintGroupBox)
        self.referencedConstraintsCheckBox = QtGui.QCheckBox(self.renameConstraintGroupBox)
        self.searchConstraintLineEdit = QtGui.QLineEdit(self.renameConstraintGroupBox)
        self.constraintRenameLineEdit = QtGui.QLineEdit(self.renameConstraintGroupBox)
        self.renamePushButton = QtGui.QPushButton(self.renameConstraintGroupBox)
        self.activeDeactiveRenameConstraintTabPushButton = QtGui.QPushButton(self.renameConstraintGroupBox)
        self.selectRenameConstraintTabPushButton = QtGui.QPushButton(self.renameConstraintGroupBox)
        self.deleteRenameConstraintTabPushButton = QtGui.QPushButton(self.renameConstraintGroupBox)
        self.findConstraintTab = QtGui.QWidget()
        self.selectedModelGroupBox = QtGui.QGroupBox(self.findConstraintTab)
        self.findConstraintsPushButton = QtGui.QPushButton(self.selectedModelGroupBox)
        self.selectedModelLabel = QtGui.QLabel(self.selectedModelGroupBox)
        self.constraintsPropertiesGroupBox = QtGui.QGroupBox(self.findConstraintTab)
        self.findConstraintsListWidget = QtGui.QListWidget(self.constraintsPropertiesGroupBox)
        self.selectFindConstraintTabPushButton = QtGui.QPushButton(self.constraintsPropertiesGroupBox)
        self.deleteFindConstraintTabPushButton = QtGui.QPushButton(self.constraintsPropertiesGroupBox)
        self.activeDeactiveFindConstraintTabPushButton = QtGui.QPushButton(self.constraintsPropertiesGroupBox)

        # create layout
        self.mainLayout = QtGui.QGridLayout()
        self.constraintGridLayout = QtGui.QGridLayout(self.createConstraintTab)
        self.constraintGroupBoxGridLayout = QtGui.QGridLayout(self.constraintGroupBox)
        self.constraintSettingGridLayout = QtGui.QGridLayout(self.constraintSettingGroupBox)
        self.parentNodeGridLayout = QtGui.QGridLayout(self.parentNodeGroupBox)
        self.childNodeGridLayout = QtGui.QGridLayout(self.childNodeGroupBox)
        self.renameConstraintTabGridLayout = QtGui.QGridLayout(self.renameConstraintTab)
        self.renameConstraintGroupGridLayout = QtGui.QGridLayout(self.renameConstraintGroupBox)
        self.findConstraintTabGridLayout = QtGui.QGridLayout(self.findConstraintTab)
        self.selectedModelGroupBoxGridLayout = QtGui.QGridLayout(self.selectedModelGroupBox)
        self.constraintsPropertiesGroupBoxGridLayout = QtGui.QGridLayout(self.constraintsPropertiesGroupBox)

        # Configure Widgets
        self.tabWidget.addTab(self.createConstraintTab, "Create Constraint")
        self.constraintGroupBox.setTitle("Constrains")
        self.parentChildPushButton.setText("Parent/Child")
        self.rotationConstraintPushButton.setText("Rotation")
        self.translationConstraintPushButton.setText("Translation")
        self.propertiesCheckBox.setText("Properties")
        self.constraintSettingGroupBox.setTitle("Constraint Setting")
        self.constraintNamelineEdit.setObjectName("constrainNamelineEdit")
        self.parentGroupBox.setTitle("Parent")
        self.parentModelNameLabel.setText(self.parentModelNameLabelText)
        self.getParentModelPushButton.setText("Get Selected Parent")
        self.childGroupBox.setTitle("Child")
        self.getChlidModelPushButton.setText("Get _selected Child")
        self.swapNodesPushButton.setText("Swap Nodes")
        self.getParentChildPushButton.setText("Get Parent And Child")
        self.activeWithSnapCheckBox.setText("Active With Snap")
        self.activeWitoutSanpCheckBox.setText("Active WithOut Snap")
        self.lockedCheckBox.setText("Locked")
        self.weightLabel.setText("Weight: ")

        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setMaximum(100)
        self.horizontalSlider.setSliderPosition(100)
        self.horizontalSlider.setTracking(True)
        self.horizontalSlider.setTickPosition(QtGui.QSlider.TicksAbove)
   
        self.selecterCheckBox.setText("Selector")    
        self.parentNodeGroupBox.setTitle("Parent Node")
        self.childNodeGroupBox.setTitle("Child Node")      
        self.tabWidget.addTab(self.renameConstraintTab, "Rename Constraint")
        self.renameConstraintGroupBox.setTitle("Rename Constrains")
        self.autoCreateNameCheckBox.setText("Auto Create Name")
        self.referencedConstraintsCheckBox.setText("Include Referenced Constraint")
        self.renamePushButton.setText("Rename")
        self.activeDeactiveRenameConstraintTabPushButton.setText("Active/Deactive")
        self.selectRenameConstraintTabPushButton.setText("Select")
        self.deleteRenameConstraintTabPushButton.setText("Delete")
        self.tabWidget.addTab(self.findConstraintTab, "Find Constraint")
        self.selectedModelGroupBox.setTitle("Find Constraints For Selected Models")
        self.findConstraintsPushButton.setText("Find Constraints")
        self.selectedModelLabel.setText("Selected Model")
        self.searchConstraintByPatternLabel.setText("Search Constraint By Pattern")
        self.constraintNameLabel.setText("Constraint Name")      
        self.constraintsPropertiesGroupBox.setTitle("Constraints")
        self.selectFindConstraintTabPushButton.setText("Select Constraint")
        self.deleteFindConstraintTabPushButton.setText("Delete Constraint")
        self.activeDeactiveFindConstraintTabPushButton.setText("Active/De-Active Constraint")

        # Assign Layouts to widgets
        self.constraintGroupBoxGridLayout.addWidget(self.parentChildPushButton, 0, 0, 1, 2)
        self.constraintGroupBoxGridLayout.addWidget(self.rotationConstraintPushButton, 1, 0, 1, 1)
        self.constraintGroupBoxGridLayout.addWidget(self.translationConstraintPushButton, 1, 1, 1, 1)
        self.constraintGroupBoxGridLayout.addWidget(self.propertiesCheckBox, 2, 0, 1, 1)
        self.constraintGridLayout.addWidget(self.constraintGroupBox, 0, 0, 1, 1)
        self.constraintSettingGridLayout.addWidget(self.constraintNamelineEdit, 0, 0, 1, 2)
        self.constraintSettingGridLayout.addWidget(self.parentGroupBox, 1, 0, 1, 1)
        self.constraintSettingGridLayout.addWidget(self.childGroupBox, 1, 1, 1, 1)
        self.constraintSettingGridLayout.addWidget(self.swapNodesPushButton, 2, 1, 1, 1)
        self.constraintSettingGridLayout.addWidget(self.getParentChildPushButton, 2, 0, 1, 1)
        self.constraintSettingGridLayout.addWidget(self.activeWithSnapCheckBox, 3, 0, 1, 1)
        self.constraintSettingGridLayout.addWidget(self.activeWitoutSanpCheckBox, 4, 0, 1, 1)
        self.constraintSettingGridLayout.addWidget(self.lockedCheckBox, 5, 0, 1, 1)
        self.constraintSettingGridLayout.addWidget(self.weightLabel, 6, 0, 1, 1)
        self.constraintSettingGridLayout.addWidget(self.horizontalSlider, 7, 0, 1, 2)
        self.constraintSettingGridLayout.addWidget(self.selecterCheckBox, 8, 0, 1, 1)
        self.constraintGridLayout.addWidget(self.constraintSettingGroupBox, 1, 0, 1, 1)
        self.parentNodeGridLayout.addWidget(self.parentNodesListWidget, 1, 0, 1, 1)
        self.parentNodeGridLayout.addWidget(self.findParentNodeLineEdit, 0, 0, 1, 1)
        self.constraintGridLayout.addWidget(self.parentNodeGroupBox, 3, 0, 1, 1)
        self.childNodeGridLayout.addWidget(self.childNodesListWidget, 1, 0, 1, 1)   
        self.childNodeGridLayout.addWidget(self.findChildNodeLineEdit, 0, 0, 1, 1)
        self.constraintGridLayout.addWidget(self.childNodeGroupBox, 4, 0, 1, 1)
        self.renameConstraintGroupGridLayout.addWidget(self.referencedConstraintsCheckBox, 0, 0, 1, 1)
        self.renameConstraintGroupGridLayout.addWidget(self.searchConstraintByPatternLabel, 1, 0, 1, 2)
        self.renameConstraintGroupGridLayout.addWidget(self.searchConstraintLineEdit, 2, 0, 1, 2)
        self.renameConstraintGroupGridLayout.addWidget(self.constraintsListWidget, 3, 0, 1, 2)
        self.renameConstraintGroupGridLayout.addWidget(self.autoCreateNameCheckBox, 4, 0, 1, 1)       
        self.renameConstraintGroupGridLayout.addWidget(self.constraintNameLabel, 5, 0, 1, 2)        
        self.renameConstraintGroupGridLayout.addWidget(self.constraintRenameLineEdit, 6, 0, 1, 2)
        self.renameConstraintGroupGridLayout.addWidget(self.renamePushButton, 7, 0, 1, 1)
        self.renameConstraintGroupGridLayout.addWidget(self.activeDeactiveRenameConstraintTabPushButton, 7, 1, 1, 1)
        self.renameConstraintGroupGridLayout.addWidget(self.selectRenameConstraintTabPushButton, 8, 0, 1, 1)
        self.renameConstraintGroupGridLayout.addWidget(self.deleteRenameConstraintTabPushButton, 8, 1, 1, 1)
        self.renameConstraintTabGridLayout.addWidget(self.renameConstraintGroupBox, 0, 0, 1, 1)
        self.selectedModelGroupBoxGridLayout.addWidget(self.findConstraintsPushButton, 0, 0, 1, 1)
        self.selectedModelGroupBoxGridLayout.addWidget(self.selectedModelLabel, 1, 0, 1, 1)
        self.findConstraintTabGridLayout.addWidget(self.selectedModelGroupBox, 0, 0, 1, 1)
        self.constraintsPropertiesGroupBoxGridLayout.addWidget(self.findConstraintsListWidget, 0, 0, 1, 2)
        self.constraintsPropertiesGroupBoxGridLayout.addWidget(self.selectFindConstraintTabPushButton, 1, 0, 1, 1)
        self.constraintsPropertiesGroupBoxGridLayout.addWidget(self.deleteFindConstraintTabPushButton, 1, 1, 1, 1)
        self.constraintsPropertiesGroupBoxGridLayout.addWidget(self.activeDeactiveFindConstraintTabPushButton, 2, 0, 1, 2)
        self.findConstraintTabGridLayout.addWidget(self.constraintsPropertiesGroupBox, 1, 0, 1, 1)
        self.mainLayout.addWidget(self.tabWidget, 0, 0, 1, 1)

        self.propertyVisibility()  # Property UI visibility is Off
        self.selectedVisibility()  # Selector UI visibility is Off

        mainWidget.setLayout(self.mainLayout)
        self.setCentralWidget(mainWidget)
        
        # Signals
        self.propertiesCheckBox.stateChanged.connect(self.propertyVisibility)
        self.selecterCheckBox.stateChanged.connect(self.selectedVisibility)       
        self.findParentNodeLineEdit.textChanged.connect(self.getMatchParentModels)
        self.findChildNodeLineEdit.textChanged.connect(self.getMatchChildModels)
        self.parentNodesListWidget.itemDoubleClicked.connect(self.parentNodesListWidgetDoubleClicked)
        self.childNodesListWidget.itemDoubleClicked.connect(self.childNodesListWidgetDoubleClicked)
        self.getParentModelPushButton.pressed.connect(self.setSelectedParentNode)
        self.getChlidModelPushButton.pressed.connect(self.setSelectedChildNode)
        self.getParentChildPushButton.pressed.connect(self.setParentChildNodes)
        self.swapNodesPushButton.pressed.connect(self.swapNodes)
        self.horizontalSlider.valueChanged.connect(self.displayWeight)
        self.parentChildPushButton.pressed.connect(self.parentChildConstraintSetup)
        self.rotationConstraintPushButton.pressed.connect(self.rotationConstraintSetup)
        self.translationConstraintPushButton.pressed.connect(self.translationConstraintSetup)
        self.tabWidget.currentChanged.connect(self.refreshRenameConstraintTab)
        self.constraintsListWidget.itemSelectionChanged.connect(self.setConstraintName)
        self.renamePushButton.pressed.connect(self.renameConstraint)
        self.findChildNodeLineEdit.textChanged.connect(self.getMatchChildModels)
        self.searchConstraintLineEdit.textChanged.connect(self.refreshRenameConstraintTab)
        self.findConstraintsPushButton.clicked.connect(self.findConstraints)
        self.referencedConstraintsCheckBox.stateChanged.connect(self.refreshRenameConstraintTab)
        self.autoCreateNameCheckBox.stateChanged.connect(self.setConstraintName)
        self.activeDeactiveRenameConstraintTabPushButton.pressed.connect(partial(self.activeDeactive, TabType.RenameConstraintTab))
        self.activeDeactiveFindConstraintTabPushButton.pressed.connect(partial(self.activeDeactive, TabType.FindConstraintTab))
        self.selectRenameConstraintTabPushButton.pressed.connect(partial(self.selectConstraint, TabType.RenameConstraintTab))
        self.selectFindConstraintTabPushButton.pressed.connect(partial(self.selectConstraint, TabType.FindConstraintTab))
        self.deleteRenameConstraintTabPushButton.pressed.connect(partial(self.deleteConstraint, TabType.RenameConstraintTab))
        self.deleteFindConstraintTabPushButton.pressed.connect(partial(self.deleteConstraint, TabType.FindConstraintTab))

    def findConstraints(self):
        """
        Find Constraint on selected Model
        """
        selectedModels = Globals.Models.Selected              
        constraintList = []
        if len(selectedModels) == 1:
            selectedModel = selectedModels[0].LongName 
            self.selectedModelLabel.setText(selectedModel)
            self.findConstraintsListWidget.clear()
            for constraint in Globals.Constraints:
                for i in xrange(constraint.ReferenceGroupGetCount()):
                    for idx in xrange(constraint.ReferenceGetCount(i)):
                        if constraint.ReferenceGet(i,idx).LongName == selectedModel:
                            constraintList.append(constraint.LongName)
        else:
            QtGui.QMessageBox.warning(None, "R* Error", "Select Only One Node")

        constraintList = list(dict.fromkeys(constraintList))
        constraintList.sort()
        self.findConstraintsListWidget.addItems(constraintList)
        
    def selectedConstraint(self, tabName):
        """
        get available Constrain by given Name
        """

        if tabName == TabType.RenameConstraintTab:
            name = self.constraintsListWidget.currentItem()
        else:
            name = self.findConstraintsListWidget.currentItem()
    
        if name is None:
            QtGui.QMessageBox.warning(None, "R* Error", "Select any Constraint from the list")
            return
        constraint = Scene.GetComponentByNameAndType(mobu.FBConstraint, str(name.text()))
        return constraint

    def activeDeactive(self, tabName):
        """
        Control Active and Deactivate property of Constrain
        """
        constraint = self.selectedConstraint(tabName)
        constraint.Active = not constraint.Active

    def selectConstraint(self, tabName):
        """
        Select Constraint by given Name
        """
        Scene.DeSelectAll()
        constraint = self.selectedConstraint(tabName)
        if constraint is None:
            return
        constraint.Selected = True

    def deleteConstraint(self, tabType):
        """
        Delete Constraint by given Name
        """
        constraint = self.selectedConstraint(tabType)
        constraint.FBDelete()
        
        if tabType == TabType.FindConstraintTab:
            self.findConstraintsListWidget.takeItem(self.findConstraintsListWidget.currentRow())
        else:
            self.refreshRenameConstraintTab()

    def renameConstraint(self):
        """
        Rename Constraint by given Name
        """
        constraint = self.selectedConstraint(TabType.RenameConstraintTab)
        if constraint is None:
            return            
        constraint.LongName = str(self.constraintRenameLineEdit.text())
        self.refreshRenameConstraintTab()

    def setConstraintName(self):
        """
        Get Constrain name from parent child Nodes
        """
        self.constraintRenameLineEdit.clear()
        if self.autoCreateNameCheckBox.isChecked():
            constraint = self.selectedConstraint(TabType.RenameConstraintTab)
            try:
                parent = constraint.ReferenceGet(0, 0)
                child = constraint.ReferenceGet(1, 0)         
                name = '{0}-to-{1}'.format(parent.LongName, child.LongName)
                self.constraintRenameLineEdit.setText(name)

            except AttributeError: 
                self.constraintRenameLineEdit.clear()
                
    def refreshRenameConstraintTab(self):
        """
        Refresh Tab items
        """
        if self.renameConstraintTab is self.tabWidget.currentWidget():
            self.constraintsListWidget.clear()
            
            if self.referencedConstraintsCheckBox.isChecked():
                constraintModels = [constraintModels.LongName for constraintModels in Globals.Constraints if not isinstance(constraintModels, mobu.FBStoryTrack)]
            else:
                folderConstraintItems = [folderConstraintItems.Items for folderConstraintItems in Globals.Folders if 'Constraints 1' in folderConstraintItems.LongName or 'OH_Constraints' in folderConstraintItems.LongName]
                folderConstraintItemNames = [y.LongName for x in folderConstraintItems for y in x] 
                constraintModels = [constraintModels.LongName for constraintModels in Globals.Constraints if constraintModels.LongName not in folderConstraintItemNames and not isinstance(constraintModels, mobu.FBStoryTrack) and 'META_OUTFIT_DEFAULT' not in str(constraintModels.LongName) and 'FPS_Cam_Constraint1' not in str(constraintModels.LongName)]

            pattern = self.searchConstraintLineEdit.text()
            constraintWithPattern = [constraintWithPattern for constraintWithPattern in constraintModels if pattern.lower() in constraintWithPattern.lower()]

            self.constraintsListWidget.addItems(constraintWithPattern)

    def displayWeight(self):
        """
        Display Weight Value in UI
        """
        weightValue = self.horizontalSlider.value()
        self.weightLabel.setText('Weight: {0}'.format(str(weightValue)))

    def parentChildConstraintSetup(self):
        """
        Setup Parent/Child Constraint
        """
        constraintType = Constraint.PARENT_CHILD
        self.setupConstraint(constraintType)

    def rotationConstraintSetup(self):
        """
        Setup Rotation Constraint
        """
        constraintType = Constraint.ROTATION
        self.setupConstraint(constraintType)

    def translationConstraintSetup(self):
        """
        Setup Position Constrain
        """
        constraintType = Constraint.POSITION
        self.setupConstraint(constraintType)

    def setupConstraint(self, constraintType):
        """
        Setup Constraints
        """
        if self.propertiesCheckBox.isChecked():
            parent = mobu.FBFindModelByLabelName(str(self.parentModelNameLabel.text()))
            child = mobu.FBFindModelByLabelName(str(self.childModelNameLabel.text()))
            name = str(self.constraintNamelineEdit.text())
            active = self.activeWitoutSanpCheckBox.isChecked()
            lock = self.lockedCheckBox.isChecked()
            snap = self.activeWithSnapCheckBox.isChecked()
            weight = int(self.horizontalSlider.value())
            if snap:
                active = False
            if Scene.GetComponentByNameAndType(mobu.FBConstraint, name) is not None:

                result = QtGui.QMessageBox.warning(None, "R* Warning", "Same name of constraint already existing \n Do you want to continue", QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
                if result == QtGui.QMessageBox.Cancel:
                    return

            constraint = Constraint.Constraint(constraintType, name, child, parent, Active=active, Lock=lock, Weight=weight)
            if snap:
                constraint.Snap()
        else:
            selectedModel = Globals.Models.Selected
            if len(selectedModel) == 2:
                parent = selectedModel[0]
                child = selectedModel[1]
                name = '{0}-to-{1}'.format(child.LongName, parent.LongName)
                if Scene.GetComponentByNameAndType(mobu.FBConstraint, name) is not None:
                    result = QtGui.QMessageBox.warning(None, "R* Warning", "Same name of constraint already existing \n Do you want to continue", QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
                    if result == QtGui.QMessageBox.Cancel:
                        return
                constraint = Constraint.Constraint(constraintType, name, child, parent, Active=False, Lock=True)
                constraint.Snap()
            else:
                QtGui.QMessageBox.warning(None, "R* Error", "Select Two Nodes \n\n First = Parent \n Second = Child")

    def swapNodes(self):
        """
        SwapNode name in UI
        """
        parent = str(self.parentModelNameLabel.text())
        child = str(self.childModelNameLabel.text())
        parent, child = child, parent
        self.parentModelNameLabel.setText(parent)
        self.childModelNameLabel.setText(child)
        self.updateConstraintName()

    def setParentChildNodes(self):
        """
        Set Parent and Child nodes in UI
        """
        selectedModel = Globals.Models.Selected
        if len(selectedModel)== 2:
            self.parentModelNameLabel.setText(selectedModel[0].LongName)
            self.childModelNameLabel.setText(selectedModel[1].LongName)
            self.updateConstraintName()
        else:
            QtGui.QMessageBox.warning(None, "R* Error", "Select Two Nodes \n\n First = Parent \n Second = Child")

    def setSelectedParentNode(self):
        """
        Set Parent node in UI
        """
        selectedModel = Globals.Models.Selected
        if len(selectedModel) == 1:
            self.parentModelNameLabel.setText(selectedModel[0].LongName)
            self.updateConstraintName()
        else:
            QtGui.QMessageBox.warning(None, "R* Error", "Select Only One Node")

    def setSelectedChildNode(self):
        """
        Set Child node in UI
        """
        selectedModel = Globals.Models.Selected
        if len(selectedModel) == 1:
            self.childModelNameLabel.setText(selectedModel[0].LongName)
            self.updateConstraintName()
        else:
            QtGui.QMessageBox.warning(None, "R* Error", "Select Only One Node")

    def parentNodesListWidgetDoubleClicked(self):
        """
        Set parent node name in UI
        """
        self.parentModelNameLabel.setText(self.parentNodesListWidget.currentItem().text())
        self.updateConstraintName()

    def childNodesListWidgetDoubleClicked(self):
        """
        Set parent node name in UI
        """
        self.childModelNameLabel.setText(self.childNodesListWidget.currentItem().text())
        self.updateConstraintName()

    def getMatchParentModels(self):
        """
        Search models by Pattern for Parent Nodes
        """
        self.parentNodesListWidget.clear()
        pattern = self.findParentNodeLineEdit.text()
        models = [matchModels.LongName for matchModels in searchSceneByTag.getObjsByPattern(Globals.Scene.RootModel, pattern, foundObjs=[])]
        self.parentNodesListWidget.addItems(models)

    def getMatchChildModels(self):
        """
        Search models by Pattern for Child Nodes
        """
        self.childNodesListWidget.clear()
        pattern = self.findChildNodeLineEdit.text()
        models = [matchModels.LongName for matchModels in searchSceneByTag.getObjsByPattern(Globals.Scene.RootModel, pattern, foundObjs=[])]
        self.childNodesListWidget.addItems(models)

    def updateConstraintName(self):
        """
        Update Constraint name In UI
        """
        parent = self.parentModelNameLabel.text()
        child = self.childModelNameLabel.text()
        if parent == self.parentModelNameLabelText:
            parent = ""
        if child == self.childModelNameLabelText:
            child = ""
        constraintName = '{0}-to-{1}'.format(child, parent)
        self.constraintNamelineEdit.setText(constraintName)

    def propertyVisibility(self):
        """
        Control Visibility of Property UI
        """
        if self.propertiesCheckBox.isChecked():
            self.constraintSettingGroupBox.show()
        else:
            self.constraintSettingGroupBox.hide()
            QtGui.QApplication.processEvents()
            #updating the window size is deferred here to ensure the changes in widget visibility above propagate through the hierarchy before it happens
            QtCore.QTimer.singleShot(1, self._updateWindowSize)

    def _updateWindowSize(self):
        """
        Adjust Window size when control Property and selector part in UI.
        """
        self.centralWidget().adjustSize()
        self.centralWidget().updateGeometry()
        self.window().adjustSize()
        self.window().updateGeometry()
        self.resize(330, 150)

    def selectedVisibility(self):
        """
        Control UI Properties in Selector tab
        """
        if self.selecterCheckBox.isChecked():
            self.parentNodeGroupBox.show()
            self.childNodeGroupBox.show()
            self.propertiesCheckBox.setEnabled(False)
            self.getChlidModelPushButton.setEnabled(False)
            self.getParentModelPushButton.setEnabled(False)
            self.getParentChildPushButton.setEnabled(False)
        else:
            self.parentNodeGroupBox.hide()
            self.childNodeGroupBox.hide()
            QtGui.QApplication.processEvents()
            #updating the window size is deferred here to ensure the changes in widget visibility above propagate through the hierarchy before it happens
            QtCore.QTimer.singleShot(1, self._updateWindowSize)
            self.propertiesCheckBox.setEnabled(True)
            self.getChlidModelPushButton.setEnabled(True)
            self.getParentModelPushButton.setEnabled(True)
            self.getParentChildPushButton.setEnabled(True)
