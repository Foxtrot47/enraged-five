import os

import pyfbsdk as mobu

from PySide import QtGui

from RS import Config, Globals, Perforce
from RS.Utils import ContextManagers, Scene
from RS.Utils.Scene import Component
from RS.Core.ReferenceSystem.Types.Set import Set
from RS.Core.ReferenceSystem.Manager import Manager


class RedMarkupExportWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(RedMarkupExportWidget, self).__init__(parent=parent)
        self.setupUi()

    def getReferenceEnvironmentList(self):
        """
        Returns:
        list (RS.Core.ReferenceSystem.Types.Set.Set)
        """
        manager = Manager()
        return manager.GetReferenceListByType(Set)

    def populateEnvironmentComboBox(self):
        """
        populates the environment QCombobox with strings
        """
        envRefList = self.getReferenceEnvironmentList()

        for environment in envRefList:
            self._environmentComboBox.addItem(str(environment.Namespace))

    def populateRedMarkupList(self):
        """
        populates the RedMarkup qlistwidget with objects that have the red markup custom properties
        """
        redMarkupObjectList = []
        for component in Globals.Components:
            property = component.PropertyList.Find("ClonedSetObject")
            if isinstance(component, mobu.FBModel) and property is not None:
                redMarkupObjectList.append(component)

        for markupObject in redMarkupObjectList:
            item = QtGui.QListWidgetItem()
            item.setText(markupObject.LongName)
            self._redMarkupList.insertItem(0, item)

    def getRedMarkupListObjects(self):
        """
        Get list of text objects in the redMarkup QListWidget.

        Returns:
            str: list of Red Markup items.
        """
        reMarkupItemList = []
        for index in xrange(self._redMarkupList.count()):
            reMarkupItemList.append(self._redMarkupList.item(index).text())
        return reMarkupItemList

    def removeObjectFromList(self):
        """
        Removes QListWidget selected item(s) from QListWidget.
        """
        for item in self._redMarkupList.selectedItems():
            self._redMarkupList.takeItem(self._redMarkupList.row(item))

    def addObjectToList(self):
        """
        Adds scene selected item(s) to the QListWidget.
        """
        # Get existing list of items as strings.
        reMarkupItemList = self.getRedMarkupListObjects()

        modelSelectedList = mobu.FBModelList()
        mobu.FBGetSelectedModels(modelSelectedList, None, True)

        for model in modelSelectedList:
            if model.LongName in reMarkupItemList:
                continue
            item = QtGui.QListWidgetItem()
            item.setText(model.LongName)
            self._redMarkupList.insertItem(0, item)

    def saveSelected(self):
        """
        Saves selected objects in FBX 2014/2015 format.
        """
        # Need context manager to speed up this process (saves 2mins!).
        with ContextManagers.PreventUIUpdateActive():
            Scene.DeSelectAll()

        # Select environment objects.
        environmentNamespace = self._environmentComboBox.currentText()

        if environmentNamespace != "":
            # Select environment.
            envNullList = Scene.Component.GetComponents(
                Namespace=environmentNamespace,
                TypeList=[mobu.FBModel]
                )

            for null in envNullList:
                null.Selected = True

        # Select red markup objects.
        for item in self._redMarkupList.selectedItems():
            redMarkupString = item.text()
            redMarkupNull = Scene.FindModelByName(str(redMarkupString), True)
            if redMarkupNull is not None:
                redMarkupNull.Selected = True

        # Save selected options.
        filePath = os.path.join(
                    Config.Project.Path.Art,
                    "animation",
                    "resources",
                    "Sets",
                    "SetChanges",
                    '{0}.FBX'.format(environmentNamespace))

        if not os.path.exists(os.path.dirname(filePath)):
            os.makedirs(os.path.dirname(filePath))

        Perforce.Sync(filePath)

        if os.path.exists(filePath):
            Perforce.Edit(filePath)

        saveOptions = mobu.FBFbxOptions(False)
        saveOptions.SaveSelectedModelsOnly = True
        saveOptions.FileFormatAndVersion = mobu.FBFileFormatAndVersion.kFBFBX2014_2015
        saveOptions.UseASCIIFormat = True
        saveOptions.BaseCameras = False
        saveOptions.CameraSwitcherSettings = False
        saveOptions.CurrentCameraSettings = False
        saveOptions.GlobalLightingSettings = False
        saveOptions.TransportSettings = False

        for index in xrange(saveOptions.GetTakeCount()):
            if saveOptions.GetTakeName(index) == mobu.FBSystem().CurrentTake.Name:
                saveOptions.SetTakeSelect(index, True)
            else:
                saveOptions.SetTakeSelect(index, False)
        
        # Save FBX.
        if mobu.FBApplication().FileSave(filePath, saveOptions):
            # Mark file for add if new.
            Perforce.Add(filePath)

            Scene.DeSelectAll()
            msg = ("Red Markup Saved As:\n{0}\n\nSaved in FBX 2014/2015 format.\n\n"
                "File is now present in your Pending Changelist.".format(str(filePath)))
            QtGui.QMessageBox.information(None, "Red Markup Export", msg)
        else:
            Scene.DeSelectAll()
            msg = "Failed to save file: {0}".format(str(file))
            QtGui.QMessageBox.warning(None, "Red Markup Export", msg)

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # Setting main layout to vertical box layout
        mainLayout = QtGui.QGridLayout()
        self.setLayout(mainLayout)

        # create widgets
        infoLabel = QtGui.QLabel("Select the environment to save out, from the drop down, and add/remove\nany extra markup objects you wish to export with this environment.")

        environmentLabel = QtGui.QLabel("Environment:")
        self._environmentComboBox = QtGui.QComboBox()
        redMarkupLabel = QtGui.QLabel("Red Markup Object(s):")
        self._redMarkupList = QtGui.QListWidget()
        addButton = QtGui.QPushButton(">>")
        removeButton = QtGui.QPushButton("<<")
        exportButton = QtGui.QPushButton("Save as FBX 2014/2015")
        horizontalLine = QtGui.QFrame()

        # set widget properties
        addButton.setToolTip("add scene selected object(s)")
        removeButton.setToolTip("remove list selected object(s)")
        horizontalLine.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine.setFrameShadow(QtGui.QFrame.Sunken)
        self._redMarkupList.setSelectionMode(QtGui.QListView.ExtendedSelection)

        # populate widgets
        self.populateEnvironmentComboBox()
        self.populateRedMarkupList()

        # add widgets to layout
        mainLayout.addWidget(infoLabel, 0, 0, 1, 3)
        mainLayout.addWidget(horizontalLine, 1, 0, 1, 3)
        mainLayout.addWidget(environmentLabel, 2, 0)
        mainLayout.addWidget(self._environmentComboBox, 2, 1, 1, 2)
        mainLayout.addWidget(redMarkupLabel, 3, 0, 1, 2)
        mainLayout.addWidget(addButton, 4, 1)
        mainLayout.addWidget(removeButton, 5, 1)
        mainLayout.addWidget(self._redMarkupList, 3, 2, 4, 1)
        mainLayout.addWidget(exportButton, 7, 0, 1, 3)

        # event callback
        exportButton.clicked.connect(self.saveSelected)
        removeButton.clicked.connect(self.removeObjectFromList)
        addButton.clicked.connect(self.addObjectToList)



