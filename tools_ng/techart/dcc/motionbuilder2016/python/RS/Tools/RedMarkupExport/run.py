from RS.Tools import UI
from RS.Tools.RedMarkupExport.widgets import redMarkupExportWidget


@UI.Run(title="Red Markup Export", size=(200, 300), dockable=False, dock=False)
def Run():
    return redMarkupExportWidget.RedMarkupExportWidget()