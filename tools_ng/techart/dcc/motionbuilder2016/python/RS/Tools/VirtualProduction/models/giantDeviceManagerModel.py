from PySide import QtCore

import pyfbsdk as mobu

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize
from RS.Core.ReferenceSystem import Manager
from RS.Core.Mocap import MocapCommands
from RS.Core.ReferenceSystem.Types import MocapPropToy, MocapBasePreviz, MocapGsSkeleton, MocapSlate


REFERENCE_ROLE = QtCore.Qt.UserRole + 1


@memorize.memoizeWithExpiry(30)
def getAttachableNodeFromReferenceSystem(refItem):
    """
    Get the attachable Node from a reference system item
    
    args:
        refItem (RS.Core.ReferenceSystem.Types.Base): The reference system item
        
    return:
        FBModel of the attachable node for the reference system node, if any are found
    """
    topLevel = None
    for rootItem in mobu.FBSystem().Scene.RootModel.Children: 
        if rootItem.LongName.startswith(refItem.Namespace):
            topLevel = rootItem
            break

    if topLevel is None:
        # Debug print until the logic is approved by Previz
        print "Unable to find top level node for '{} ({})'!".format(refItem.Name, refItem.__class__.__name__)
        return None
    
    attachBones = topLevel.Children
    if len(attachBones) == 0:
        # Debug print until the logic is approved by Previz
        print "No bones to attach"
        return None
    return attachBones[0]


class ColumnRoles(object):
    """
    Enum for the model columns  
    """
    NAME = "Reference Name"
    REF_TYPE = "Reference Type"
    GIANT_ATTACHMENT = "Giant Attachment"


class GiantDevicesBaseModelItem(baseModelItem.BaseModelItem):
    """
    Model Item Base 
    """
    def __init__(self, referenceItem, giantAttachment=None, parent=None):
        """
        Constructor
        
        args:
            refItem (RS.Core.ReferenceSystem.Types.Base): The reference system item
        
        kwargs:
            giantAttachment (FBModel): The Motion Builder model that is attached to this node
        """
        super(GiantDevicesBaseModelItem, self).__init__(parent=parent)
        self._referenceItem = referenceItem
        self._name = "{}:{}".format(referenceItem.Namespace, referenceItem.Name) 
        self._class = referenceItem.__class__.__name__
        self._giantAttachment = giantAttachment

    def giantAttachment(self):
        """
        The current giant attachment object for the item
        
        return:
            FBModel of the Motion Builder model that is attached to this node
        """
        return self._giantAttachment
    
    def setGiantAttachment(self, newAttachment):
        """
        Set the current giant attachment object for the item
        
        args:
            newAttachment (FBModel): The Motion Builder model that is attached to this node
        """
        self._giantAttachment = newAttachment

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.model().getHeadings()[index.column()]

        if role == QtCore.Qt.DisplayRole:
            if column == ColumnRoles.NAME:
                return self._name
            elif column == ColumnRoles.REF_TYPE:
                return self._class
            elif column == ColumnRoles.GIANT_ATTACHMENT:
                return self._giantAttachment

        elif role == QtCore.Qt.EditRole:
            if column == ColumnRoles.GIANT_ATTACHMENT:
                return self._giantAttachment

        elif role == QtCore.Qt.UserRole:
            return self
        elif role == REFERENCE_ROLE:
            return self._referenceItem
 
    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """
        ReImplemented from Qt
        """
        column = index.model().getHeadings()[index.column()]

        if role == QtCore.Qt.EditRole:
            if column == ColumnRoles.GIANT_ATTACHMENT:
                if self._giantAttachment is not None:
                        MocapCommands.GiantDeviceManager.SetAttachedObject(self._giantAttachment, None)
                if value is not None:
                    MocapCommands.GiantDeviceManager.SetAttachedObject(value, getAttachableNodeFromReferenceSystem(self._referenceItem))
                self._giantAttachment = value
                return True
        return False

    def rowCount(self, parent=None):
        """
        ReImplemented from Qt
        """
        return 0

    def hasChildren(self, parent=None):
        """
        ReImplemented from Qt
        """
        return False

    def flags(self, index):
        """
        Flags added to determine column properties.

        Args:
            index (QtCore.QModelIndex): Index of the item in the model.
        """
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        column = index.model().getHeadings()[index.column()]
        flags = super(GiantDevicesBaseModelItem, self).flags(index)
        if column in [ColumnRoles.GIANT_ATTACHMENT]:
            flags |= QtCore.Qt.ItemIsEditable
        return flags


class GiantDevicesBaseModel(baseModel.BaseModel):
    """
    Base Model for the Giant Devices
    """
    
    _giantRefTypes = (MocapPropToy.MocapPropToy, MocapBasePreviz.MocapBasePreviz, MocapGsSkeleton.MocapGsSkeleton,
                      MocapSlate.MocapSlate)
    
    def __init__(self, parent=None):
        """
        Constructor
        """
        self._manager = Manager.Manager()
        super(GiantDevicesBaseModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Get the headings of model
        
        return:
            list of string column names
        """
        return [ColumnRoles.NAME, ColumnRoles.REF_TYPE, ColumnRoles.GIANT_ATTACHMENT]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        ReImplemented from Qt
        """
        return len(self.getHeadings())

    def _getReferenceItems(self):
        """
        Internal Method
        
        get all the relevant reference system items
        
        returns:
            list of Reference System objects
        """
        return [refItem for refItem in self._manager.GetReferenceListAll() if isinstance(refItem, self._giantRefTypes)]

    def setupModelData(self, parent):
        """
        setup the model data
        """
        hookups = {value: key for key, value in MocapCommands.GiantDeviceManager.GetAttachedObjects().iteritems() if value is not None}
        for refItem in self._getReferenceItems():
            giantAttchment = hookups.get(getAttachableNodeFromReferenceSystem(refItem))
            parent.appendChild(GiantDevicesBaseModelItem(refItem, giantAttchment, parent))

    def refreshGiantAttachments(self):
        """
        Refresh the Giant Attachments on the reference system items, clearing out stale attachments and
        show new attachments
        """
        self.layoutAboutToBeChanged.emit()
        hookups = {value: key for key, value in MocapCommands.GiantDeviceManager.GetAttachedObjects().iteritems() if value is not None}
        for item in self.rootItem.childItems:
            newGiantAttachment = hookups.get(getAttachableNodeFromReferenceSystem(item._referenceItem))
            if newGiantAttachment != item.giantAttachment():
                item.setGiantAttachment(newGiantAttachment) 
        self.layoutChanged.emit()
