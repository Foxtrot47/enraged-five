"""
Contains the DuringShootWidget.
"""
import os
import traceback

import pyfbsdk as mobu

from PySide import QtCore, QtGui

from RS import Globals, Perforce
from RS.Core.AnimData._internal import contexts
from RS.Tools.CameraToolBox.PyCoreQt.Widgets import comboBox
from RS.Core.Mocap import MocapCommands, previzIO
from RS.Utils import Path
from RS.Core.Camera import LibGTA5, CamUtils
from RS.Core.Face import FacewareLive
from RS.Core.Mocap import AudioRecording
from RS.Core.Mocap import realtimeConnection
from RS.Core.ReferenceSystem import Manager
from RS.Core.VirtualProduction import tpvcLib
from RS.Utils.Scene import Component, Constraint, Devices
from RS.Core.Scene.models import takeModelTypes
from RS.Tools.VirtualProduction import const


class P4ChangeListModeOptions(object):
    CREATE_NEW = "Create New Change List"
    ADD_TO_EXISTING = "Add to Existing Change List"


class DuringShootWidget(QtGui.QWidget):
    """
    Widget with UI elements that automates tasks within our mocap shoot
    workflows and extends the toolset to allow connecting to our other stage
    related systems.

    Attributes:
        TITLE (str): The tool's title for use in title bars.
        TOOL_NAME (str): The tool's name for use in settings/config files.

    Signals:
        takeNameChanged (trial): Emitted when the take name has changed.
        connectionStatusChanged (trial): Emitted when the connection status has changed.
    """
    takeNameChanged = QtCore.Signal(object)
    connectionStatusChanged = QtCore.Signal(object)

    TITLE = "During Shoot"

    def __init__(self, parent=None):
        super(DuringShootWidget, self).__init__(parent=parent)

        self._pendingChangeLists = {}
        self._currentTrial = None

        self.TOOL_NAME = self.__class__.__name__
        self._settings = QtCore.QSettings("RockstarSettings", self.TOOL_NAME)

        # Logging to MotionBuilder app level log file with widget as context to
        # filter by. Set on instance to allow more than one tool to define
        # itself as context in their respective setupUi methods.
        self.log = Globals.Ulog

        # Set up realtime connection.
        self._realtimeConnection = realtimeConnection.RealtimeConnection()
        self._realtimeConnection.realTimeConnected.connect(self._handleRealtimeConnect)
        self._realtimeConnection.realTimeDisconnected.connect(self._handleRealtimeDisconnect)
        self._realtimeConnection.autoTakeUpdated.connect(self._handleRealtimeAutoUpdate)
        self._realtimeConnection.manualTakeUpdated.connect(self._handleRealtimeManualUpdate)
        self._realtimeConnection.trialDoesNotExist.connect(self._handleTrialDoesNotExist)

        # Set up UI.
        self.setupUi()

    def onClose(self):
        """
        To be called during parent widget's closeEvent.
        """
        self._handleRemoveRecordingStopCallback()

    def _handleCreateTakeWithoutLayerClicked(self):
        """
        Notes:
            Any changes to this method should be mirrored in the shortcut script:
                RS\Core\Mocap\Previz\GrabWatchstarCreateTakeNameWithNoLayering.py
        """
        # Refresh trial data in case the layer fields have been updated.
        self._currentTrial = self._currentTrial.project().getTrialByID(self._currentTrial.captureId)
        self._setCurrentLayer(self._currentTrial)

        try:
            # Create new take.
            self._realtimeConnection.grabTakeName(False)
        except realtimeConnection.TakeAlreadExistsException:
            QtGui.QMessageBox.warning(
                self,
                self.TITLE,
                "Take already exists with the current trial name. You can't create a new take with the same name!"
            )
            return
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

    def _handleCreateTakeWithLayerClicked(self):
        """
        Notes:
            Any changes to this method should be mirrored in the shortcut script:
                RS\Core\Mocap\Previz\GrabWatchstarCreateTakeNameWithLayering.py
        """
        # Refresh trial data in case the layer fields have been updated.
        self._currentTrial = self._currentTrial.project().getTrialByID(self._currentTrial.captureId)
        self._currentTrial.refresh()
        self._setCurrentLayer(self._currentTrial)

        # Make sure that the Watchstar Op has set a Layer Parent on the current trial.
        if self._currentTrial.layerComposite() is None:
            QtGui.QMessageBox.warning(self, self.TITLE, "The trial's Layer Parent is not set in Watchstar.")

        try:
            # Create new take.
            self._realtimeConnection.grabTakeName(True)

            # Nuke switcher keys if needed.
            if self._nukeSwitcherCheckBox.checkState() == QtCore.Qt.CheckState.Checked:
                switcherNode = mobu.FBCameraSwitcher().PropertyList.Find("Camera Index").GetAnimationNode()
                switcherFCurve = switcherNode.FCurve
                switcherFCurve.EditClear()
        except realtimeConnection.TakeAlreadExistsException:
            QtGui.QMessageBox.warning(
                self,
                self.TITLE,
                "Take already exists with the current trial name. You can't create a new take with the same name!"
            )
            return
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

    def _handleBakeCams(self):
        """Bakes out vCams to RS cams."""
        dupeCountStart = len(CamUtils.GetUndupedVcams())
        # No Vcams - exit early
        if dupeCountStart == 0:
            QtGui.QMessageBox.information(self, self.TITLE, "No vCams found to bake.")
            return
        # Dupe and Check Vcams to Dupe Again
        CamUtils.DupeRtcsToRsCams()
        dupeCount = len(CamUtils.GetUndupedVcams())
        if dupeCount > 0:
            # Error: Vcams still remain unduped
            QtGui.QMessageBox.information(self, self.TITLE, "Error: {0} vCams failed to bake.".format(dupeCount))
            return
        # Dupe Success
        QtGui.QMessageBox.information(self, self.TITLE, "{0} vCams baked.".format(dupeCountStart))


    def _handleDeleteDevicesClicked(self):
        """Checks that all TPVCams are baked and then deletes all devices except the SpaceBall."""
        # Check Unduped Vcams
        undupedCount = len(CamUtils.GetUndupedVcams())
        if undupedCount > 0:
            result = QtGui.QMessageBox.question(
                self,
                self.TITLE,
                "{0} vcams have not been baked. Delete vcams anyway?".format(undupedCount),
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                QtGui.QMessageBox.No,
            )
            if result == QtGui.QMessageBox.No:
                QtGui.QMessageBox.information(self, self.TITLE, "Devices were not deleted.")
                return

        # Delete Vcam Devices
        tpvcLib.DeleteVcamDevices()

        # Delete all the devices and the assets in the Navigator related to those devices.
        spaceBallDevices = Devices.GetAllDeviceByType("FBDeviceSpaceBall")
        MocapCommands.DeleteDevices(ignoredDevices=spaceBallDevices)

        # Delete Success
        QtGui.QMessageBox.information(self, self.TITLE, "All devices are now deleted except for the SpaceBall.")

    def PrepForPlayback(self):
        # go offline with all devices
        MocapCommands.GiantDeviceOff()
        MocapCommands.NonGiantDevicesOff()
        # plot faceware and timeshift -5 frames
        FacewareClass = FacewareLive.FacewareConstraint()
        FacewareClass.TimeShift(-5)
        # bring in audio file that matches the take name into story mode and turn story mode on for playback
        fileFullPath = mobu.FBApplication().FBXFileName
        if fileFullPath == "":
            QtGui.QMessageBox.critical(None,
                                      "Virtual Production Toolbox",
                                      "No FBX file path. Unable to prep for playback.\nSave file first!",
                                      QtGui.QMessageBox.Ok)
            return
        baseName = Path.GetBaseNameNoExtension(fileFullPath)
        extension = Path.GetFileExtension(fileFullPath)
        fileName = baseName + extension
        filePathSplit = fileFullPath.split(fileName)
        audioPathPrefix = filePathSplit[0]

        currentTakeName = mobu.FBSystem().CurrentTake.Name
        audioPath = audioPathPrefix + '\\' + currentTakeName + '.wav'

        if os.path.exists(audioPath):
            audioTrack = mobu.FBStoryTrack(mobu.FBStoryTrackType.kFBStoryTrackAudio, None)
            audioTrack.Name = currentTakeName
            audioClip = mobu.FBAudioClip(audioPath)
            audioClip.PropertyList.Find("AccessMode").Data = 1
            mobu.FBStoryClip(audioClip, audioTrack, mobu.FBTime(0, 0, 0, 0))
            story = mobu.FBStory()
            story.Mute = False

        else:
            QtGui.QMessageBox.warning(
                self,
                self.TITLE,
                "Unable to find an audio file that matches the current Take Name\n\n{0}".format(audioPath),
                QtGui.QMessageBox.Ok
            )

    def BackFromPlaybackTakeNoLayering(self):
        # delete the audio story track and delete the audio file from the scene
        MocapCommands.AudioClipAndTrackDelete()

        # create new take without animation
        self._realtimeConnection.grabTakeName(False)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

        # go back online with all devices
        MocapCommands.NonGiantDevicesOn()
        MocapCommands.GiantDeviceOn()

    def BackFromPlaybackTakeLayering(self):
        # delete the audio story track and delete the audio file from the scene
        MocapCommands.AudioClipAndTrackDelete()

        # create new take without animation
        self._realtimeConnection.grabTakeName(True)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

        # go back online with all devices
        MocapCommands.GiantDeviceOn()
        MocapCommands.NonGiantDevicesOn()

    def _handleRecordingStopCallback(self, control, event):
        if event.Type == mobu.FBPlayerControlChangeType.kFBPlayerControlStop:
            if len(Globals.AudioClips) > 0:
                currentAudio = Globals.AudioClips[-1]
                accessProperty = currentAudio.PropertyList.Find("AccessMode")
                if accessProperty is not None:
                    accessProperty.Data = 1
                self._handleRemoveRecordingStopCallback()

    def _handleRemoveRecordingStopCallback(self):
        Globals.Callbacks.PlayerOnChange.Remove(self._handleRecordingStopCallback)

    def SetAudioRecordingFilePath(self):
        """
        Set the audio recording file path based off what is in CaptureBoss
        """
        try:
            Globals.Callbacks.PlayerOnChange.Add(self._handleRecordingStopCallback)
            # set the recording path
            AudioRecording.SetAudioRecordingFilePath()
        except ValueError as error:
            QtGui.QMessageBox.information(self, self.TITLE, str(error))
            return
        except realtimeConnection.TakeAlreadExistsException:
            QtGui.QMessageBox.information(
                self,
                self.TITLE,
                "Take already exists with the current trial name. You can't create a new take with the same name!"
            )
            return

    def addAudioCue(self):
        """
        Adds an audio file into story
        """
        # check if audio already exists
        for audio in Globals.AudioClips:
            if Component.GetNamespace(audio) == "adrCue":
                QtGui.QMessageBox.information(self, "Audio Cue", "Audio Cue already exists in the scene.")
                return

        # create audio reference
        manager = Manager.Manager()
        referenceList = manager.CreateReferences(const.getAudioCuePath())

        # variable for newly added audio
        audioCue = referenceList[0].getAudioClip()

        if audioCue is None:
            return

        # create track
        track = mobu.FBStoryTrack(mobu.FBStoryTrackType.kFBStoryTrackAudio)
        track.Name = referenceList[0].Namespace

        # add audio clip to track
        mobu.FBStoryClip(audioCue, track, mobu.FBTime(0, 0, 0 , 0))

    def _handleGrabTakeButton(self):
        self._currentTake.setText(self._realtimeConnection.grabTakeName(rawName=True) or "<No Active Take>")

    def _setWatchstarButtonState(self, val):
        self._createTakeWithLayerButton.setEnabled(val)
        self._createTakeWithoutLayerButton.setEnabled(val)
        self._nukeSwitcherCheckBox.setEnabled(val)
        self._setAudioFileLocButton.setEnabled(val)

    def setupUi(self):
        """
        Sets up the widgets, their properties and launches call events
        """
        # create layouts
        mainLayout = QtGui.QGridLayout()
        operatorGroupBoxGridLayout = QtGui.QGridLayout()
        playbackGroupBoxGridLayout = QtGui.QGridLayout()
        currentTrialNameLayout = QtGui.QVBoxLayout()

        fileSaveLayout = QtGui.QGridLayout()
        takeSelectLayout = QtGui.QVBoxLayout()
        tkSelectionButtonsLayout = QtGui.QHBoxLayout()
        p4OptionsLayout = QtGui.QGridLayout()

        # create widgets
        operatorGroupBox = QtGui.QGroupBox('CaptureBoss Connection')
        self._currentTake = QtGui.QLineEdit()
        self._currentID = QtGui.QLineEdit()
        self._currentLayer = QtGui.QLineEdit()
        self._currentProjectConfig = QtGui.QLineEdit()
        self._currentSession = QtGui.QLineEdit()
        self._currentProject = QtGui.QLineEdit()
        self._currentMode = QtGui.QLabel("NOT CONNECTED")
        self._createTakeWithLayerButton = QtGui.QPushButton("Create Take with Layering")
        self._createTakeWithoutLayerButton = QtGui.QPushButton("Create Take without Layering")
        self._nukeSwitcherCheckBox = QtGui.QCheckBox("Nuke Switcher Keys")
        self._setAudioFileLocButton = QtGui.QPushButton("Set Audio Recording path")
        self._bakeCamsButton = QtGui.QPushButton("Bake Vcams")
        self._deleteDevicesButton = QtGui.QPushButton("Delete Devices and TPVC Cameras")

        playbackGroupBox = QtGui.QGroupBox('Playback')
        playbackButton = QtGui.QPushButton('Prepare for Playback')
        layerPlaybackButton = QtGui.QPushButton('Return from Playback with Layering')
        noLayeringPlayback = QtGui.QPushButton('Return from Playback without Layering')
        addAudioCueButton = QtGui.QPushButton("Add Audio Cue")

        fileMngtGroupBox = QtGui.QGroupBox('File Management')

        takeSelectGroupBox = QtGui.QGroupBox('Select Takes  ')  # These extra spaces are a hack to help with spacing.
        takeSelectAllButton = QtGui.QPushButton('Select All')
        takeSelectInvertButton = QtGui.QPushButton('Select Inverted')
        takeSelectNoneButton = QtGui.QPushButton('Select None')
        self._takeModel = takeModelTypes.TakeModel(register=True)
        self._takeView = QtGui.QTreeView()

        p4OptionsGroupBox = QtGui.QGroupBox("Set Perforce Options")
        p4CLModeLabel = QtGui.QLabel("Change List Mode:")
        self._p4CLModeCmbox = QtGui.QComboBox()
        self._p4CLPrefixLabel = QtGui.QLabel("New Change List Prefix:")
        self._p4CLPrefixLEdit = QtGui.QLineEdit(const.P4DescriptionTokens.DURINGSHOOTWIDGET_SAVE_CHANGELIST_PREFIX)
        self._p4SelectedCLLabel = QtGui.QLabel("Selected Change List:")
        self._p4SelectedCLCmbox = comboBox.ComboBox()
        p4SubmitLabel = QtGui.QLabel("Submit After Saving:")
        self._p4SubmitCheckBox = QtGui.QCheckBox()
        self._postProcessingLabel = QtGui.QLabel("Post Processing:")
        self._postProcessingCheckBox = QtGui.QCheckBox()
        self._saveButton = QtGui.QPushButton("Save")

        operatorSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        playbackSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        fileSaveSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)

        # Configure widgets
        self.setWindowTitle("During Shoot")
        self._nukeSwitcherCheckBox.setChecked(False)
        currentTrialNameLayout.setContentsMargins(0, 0, 0, 0)
        self._currentID.setReadOnly(True)
        self._currentProjectConfig.setReadOnly(True)
        self._currentProject.setReadOnly(True)
        self._currentSession.setReadOnly(True)
        self._currentTake.setReadOnly(True)
        self._currentLayer.setReadOnly(True)
        self._takeView.setModel(self._takeModel)
        self._takeView.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self._takeView.setAlternatingRowColors(True)
        takeSelectGroupBox.setFlat(True)
        p4OptionsGroupBox.setFlat(True)

        takeSelectLayout.setSpacing(3)
        tkSelectionButtonsLayout.setSpacing(3)
        p4OptionsLayout.setSpacing(3)

        self._p4CLModeCmbox.addItems([P4ChangeListModeOptions.CREATE_NEW, P4ChangeListModeOptions.ADD_TO_EXISTING])
        self._handleP4SelectedCLCmboxClicked()
        self._postProcessingCheckBox.setChecked(True)
        self._p4SelectedCLLabel.setEnabled(False)
        self._p4SelectedCLCmbox.setEnabled(False)

        # Configure layouts
        currentTrialNameLayout.addWidget(self._currentMode)
        currentTrialNameLayout.addWidget(QtGui.QLabel("Current Project:"))
        currentTrialNameLayout.addWidget(self._currentProject)
        currentTrialNameLayout.addWidget(QtGui.QLabel("Current Session:"))
        currentTrialNameLayout.addWidget(self._currentSession)
        currentTrialNameLayout.addWidget(QtGui.QLabel("Current Trial in Realtime:"))
        currentTrialNameLayout.addWidget(self._currentTake)
        currentTrialNameLayout.addWidget(QtGui.QLabel("Current Trial ID:"))
        currentTrialNameLayout.addWidget(self._currentID)
        currentTrialNameLayout.addWidget(QtGui.QLabel("Current Trial Layer:"))
        currentTrialNameLayout.addWidget(self._currentLayer)
        currentTrialNameLayout.addWidget(QtGui.QLabel("Current Project Config:"))
        currentTrialNameLayout.addWidget(self._currentProjectConfig)

        operatorGroupBoxGridLayout.addLayout(currentTrialNameLayout, 1, 0, 1, 2)
        operatorGroupBoxGridLayout.addWidget(self._createTakeWithLayerButton, 5, 0)
        operatorGroupBoxGridLayout.addWidget(self._nukeSwitcherCheckBox, 5, 1)
        operatorGroupBoxGridLayout.addWidget(self._createTakeWithoutLayerButton, 6, 0, 1, 2)
        operatorGroupBoxGridLayout.addWidget(self._setAudioFileLocButton, 7, 0, 1, 2)
        operatorGroupBoxGridLayout.addItem(operatorSpacerItem, 8, 0, 1, 2)

        playbackGroupBoxGridLayout.addWidget(playbackButton, 0, 2)
        playbackGroupBoxGridLayout.addWidget(layerPlaybackButton, 1, 2)
        playbackGroupBoxGridLayout.addWidget(noLayeringPlayback, 2, 2)
        playbackGroupBoxGridLayout.addWidget(addAudioCueButton, 3, 2)
        playbackGroupBoxGridLayout.addItem(playbackSpacerItem, 4, 2)

        tkSelectionButtonsLayout.addWidget(takeSelectAllButton)
        tkSelectionButtonsLayout.addWidget(takeSelectInvertButton)
        tkSelectionButtonsLayout.addWidget(takeSelectNoneButton)

        takeSelectLayout.addWidget(self._takeView)
        takeSelectLayout.addLayout(tkSelectionButtonsLayout)

        p4OptionsLayout.addWidget(p4CLModeLabel, 0, 0)
        p4OptionsLayout.addWidget(self._p4CLModeCmbox, 0, 1)
        p4OptionsLayout.addWidget(self._p4CLPrefixLabel, 1, 0)
        p4OptionsLayout.addWidget(self._p4CLPrefixLEdit, 1, 1)
        p4OptionsLayout.addWidget(self._p4SelectedCLLabel, 2, 0)
        p4OptionsLayout.addWidget(self._p4SelectedCLCmbox, 2, 1)
        p4OptionsLayout.addWidget(self._postProcessingLabel, 3, 0)
        p4OptionsLayout.addWidget(self._postProcessingCheckBox, 3, 1)
        p4OptionsLayout.addWidget(p4SubmitLabel, 4, 0)
        p4OptionsLayout.addWidget(self._p4SubmitCheckBox, 4, 1)

        fileSaveLayout.addWidget(takeSelectGroupBox, 0, 0, 1, 2)
        fileSaveLayout.addWidget(p4OptionsGroupBox, 1, 0, 1, 2)
        fileSaveLayout.addWidget(self._bakeCamsButton, 2, 0, 1, 2)
        fileSaveLayout.addWidget(self._deleteDevicesButton, 3, 0, 1, 2)
        fileSaveLayout.addWidget(self._saveButton, 4, 0, 1, 2)
        fileSaveLayout.addItem(fileSaveSpacerItem, 5, 0, 1, 2)

        operatorGroupBox.setLayout(operatorGroupBoxGridLayout)
        playbackGroupBox.setLayout(playbackGroupBoxGridLayout)
        takeSelectGroupBox.setLayout(takeSelectLayout)
        p4OptionsGroupBox.setLayout(p4OptionsLayout)
        fileMngtGroupBox.setLayout(fileSaveLayout)

        mainLayout.addWidget(operatorGroupBox, 0, 0, 2, 1)
        mainLayout.addWidget(playbackGroupBox, 0, 1)
        mainLayout.addWidget(fileMngtGroupBox, 1, 1)

        self.setLayout(mainLayout)

        # Connections
        self._createTakeWithLayerButton.pressed.connect(self._handleCreateTakeWithLayerClicked)
        self._createTakeWithoutLayerButton.pressed.connect(self._handleCreateTakeWithoutLayerClicked)
        playbackButton.pressed.connect(self.PrepForPlayback)
        layerPlaybackButton.pressed.connect(self.BackFromPlaybackTakeLayering)
        noLayeringPlayback.pressed.connect(self.BackFromPlaybackTakeNoLayering)
        self._setAudioFileLocButton.pressed.connect(self.SetAudioRecordingFilePath)
        addAudioCueButton.pressed.connect(self.addAudioCue)
        self._bakeCamsButton.pressed.connect(self._handleBakeCams)
        self._deleteDevicesButton.pressed.connect(self._handleDeleteDevicesClicked)

        self._takeView.pressed.connect(self._handleTakeItemPressed)
        takeSelectAllButton.clicked.connect(self._handleSelectAllButtonClick)
        takeSelectInvertButton.clicked.connect(self._handleSelectInvertedButtonClick)
        takeSelectNoneButton.clicked.connect(self._handleSelectNoneButtonClick)

        self._saveButton.clicked.connect(self._handleSaveButtonClick)

        self._p4CLModeCmbox.currentIndexChanged.connect(self._handleP4CLModeChanged)
        self._p4SelectedCLCmbox.clicked.connect(self._handleP4SelectedCLCmboxClicked)

        # Realtime connection.
        if self._realtimeConnection.isConnected() is True:
            self._setUiConnect()
        else:
            self._setUiDisconnect()

        # Must initialize with these buttons disabled.
        self._setWatchstarButtonState(False)

    def isConnected(self):
        return self._realtimeConnection.isConnected()

    def _handleRealtimeConnect(self):
        self.connectionStatusChanged.emit(True)

    def _handleRealtimeDisconnect(self):
        """
        Do not disable saving here. The trial ID is stored as a custom property on an object in the take, so saving
        never needs a live connection to work.
        """
        self._currentTrial = None
        self.connectionStatusChanged.emit(False)

        self._currentMode.setText("")
        self._currentProject.setText("")
        self._currentSession.setText("")
        self._currentTake.setText("")
        self._currentID.setText("")
        self._currentProjectConfig.setText("")
        self._currentLayer.setText("")

    def _setCurrentLayer(self, trial):
        """
        Set the Current Trial Layer in the UI.

        Args:
            trial (contexts.Trial): The current trial.

        """
        currentLayer = "None"
        if isinstance(trial, contexts.MotionTrialBase):
            if trial.isLayer():
                if trial.layerComposite() is not None:
                    currentLayer = trial.layerComposite().name
                else:
                    currentLayer = "UNSET IN WATCHSTAR"
        self._currentLayer.setText(currentLayer)

    def _handleRealtimeAutoUpdate(self, trial):
        self._currentTrial = trial

        self.takeNameChanged.emit(trial.name)
        self._currentMode.setText("DRIVEN BY WATCHSTAR")
        self._currentTake.setText(trial.name)
        self._currentProject.setText(trial.project().name)
        self._currentSession.setText(trial.session().name)
        self._currentID.setText(str(trial.captureId))
        self._setCurrentLayer(trial)

        if isinstance(trial, contexts.CutSceneTrial):
            rootPath = trial.projectConfig().getCSFbxSceneRootPath()
        else:
            rootPath = trial.projectConfig().getIGFbxSceneRootPath()
        projectConfig = "{} ({})".format(str(trial.projectConfig().name), rootPath)
        self._currentProjectConfig.setText(projectConfig)

        self._setWatchstarButtonState(True)

    def _handleRealtimeManualUpdate(self, project, session, trialName):
        self._currentTrial = None
        self.takeNameChanged.emit(trialName)

        self._currentMode.setText("OPERATOR IN MANUAL MODE")
        self._currentTake.setText(trialName or "None")
        self._currentProject.setText(project)
        self._currentSession.setText(session)
        self._currentID.setText("None")
        self._currentProjectConfig.setText("None")
        self._currentLayer.setText("None")

        self._setWatchstarButtonState(False)

    def _setUiConnect(self):
        """
        Internal Method

        Set the UI to a connected state
        """
        self._setWatchstarButtonState(True)

    def _setUiDisconnect(self):
        """
        Internal Method

        Set the UI to a disconnected state
        """
        self._setWatchstarButtonState(False)

    def _handleTakeItemPressed(self, idx):
        """
        Internal Method

        Handle the select all button being pressed.
        """
        isChecked = self._takeModel.data(idx, role=QtCore.Qt.CheckStateRole)
        checkState = QtCore.Qt.CheckState.Checked
        if isChecked:
            checkState = QtCore.Qt.CheckState.Unchecked
        self._takeModel.setData(idx, checkState, role=QtCore.Qt.CheckStateRole)
        self._takeModel.dataChanged.emit(idx, idx)

    def _handleSelectAllButtonClick(self):
        """
        Internal Method

        Handle the select all button being pressed.
        """
        for row in xrange(self._takeModel.rowCount()):
            idx = self._takeModel.index(row, 0)
            self._takeModel.setData(idx, QtCore.Qt.CheckState.Checked, role=QtCore.Qt.CheckStateRole)
            self._takeModel.dataChanged.emit(idx, idx)

    def _handleSelectInvertedButtonClick(self):
        """
        Internal Method

        Handle the select inverted button being pressed.
        """
        for row in xrange(self._takeModel.rowCount()):
            idx = self._takeModel.index(row, 0)
            isChecked = self._takeModel.data(idx, role=QtCore.Qt.CheckStateRole)
            checkState = QtCore.Qt.CheckState.Checked
            if isChecked:
                checkState = QtCore.Qt.CheckState.Unchecked
            self._takeModel.setData(idx, checkState, role=QtCore.Qt.CheckStateRole)
            self._takeModel.dataChanged.emit(idx, idx)

    def _handleSelectNoneButtonClick(self):
        """
        Internal Method

        Handle the select none button being pressed.
        """
        for row in xrange(self._takeModel.rowCount()):
            idx = self._takeModel.index(row, 0)
            self._takeModel.setData(idx, QtCore.Qt.CheckState.Unchecked, role=QtCore.Qt.CheckStateRole)
            self._takeModel.dataChanged.emit(idx, idx)

    def _handleP4SelectedCLCmboxClicked(self):
        """
        Handle the combobox click by populating the items with changelist descriptions.
        """
        localCls = Perforce.GetLocalPendingChangelists()
        if localCls is not None:
            self._pendingChangeLists = {"{0}: {1}".format(item["change"], item["desc"].rstrip()): item["change"] for item in localCls}
            self._p4SelectedCLCmbox.clear()
            self._p4SelectedCLCmbox.addItems(self._pendingChangeLists.keys())

    def _handleSaveButtonClick(self):
        """
        Internal Method

        Handle the save button being pressed.

        Note:
            * Currently no way to resize PySide's QMessageBox to details content,
            apparently by design. A custom widget is required for this.
        """
        selectedIndexes = []
        for row in xrange(self._takeModel.rowCount()):
            idx = self._takeModel.index(row, 0)
            isChecked = self._takeModel.data(idx, role=QtCore.Qt.CheckStateRole)
            if isChecked:
                selectedIndexes.append(idx)

        if len(selectedIndexes) < 1:
            msg = "At least one take must be selected before renaming!"
            QtGui.QMessageBox.warning(self, self.TOOL_NAME, msg)
            self.log.LogWarning(msg, context=self.TOOL_NAME)
            return

        # Get/create the p4 change list.
        if self._p4CLModeCmbox.currentText() == P4ChangeListModeOptions.CREATE_NEW:
            p4Description = self._p4CLPrefixLEdit.text()
            if self._postProcessingCheckBox.isChecked():
                p4Description = "{}\n{}".format(p4Description, const.P4DescriptionTokens.POST_PROCESS_PREVIZ_SAVE)
            changelistNumber = Perforce.CreateChangelist(p4Description).Number
        elif self._p4CLModeCmbox.currentText() == P4ChangeListModeOptions.ADD_TO_EXISTING:
            changelistNumber = self._pendingChangeLists.get(self._p4SelectedCLCmbox.currentText())

        # Add files, and optionally submit to p4 depot.
        savedTakes = {}
        takes = [idx.data(QtCore.Qt.UserRole) for idx in selectedIndexes]
        for take in takes:
            self.log.LogMessage("Processing take: {}".format(take.Name), context=self.TOOL_NAME)
            # Catch and display unexpected errors; do not reraise - we must be able to process all selected takes.
            try:
                # Catch expected errors and apply additional handling.
                try:
                    # Attempt to save file with overwrite option off.
                    path = previzIO.SaveTake(take, changelistNumber, overwriteFbx=False)
                    savedTakes[take.Name] = path
                except IOError as error:
                    if not "File already exists:" in error.message:
                        raise  # Error isn't what we're interested in - reraise.
                    # The FBX path already exists, get user consent to overwrite file.
                    self.log.LogWarning("FBX file already exists. Get user input...", context=self.TOOL_NAME)
                    msg = "\n\n".join([error.message, "Do you want to overwrite the FBX file?"])
                    buttons = QtGui.QMessageBox.Save | QtGui.QMessageBox.Cancel
                    results = QtGui.QMessageBox.question(self, self.TOOL_NAME, msg, buttons=buttons)
                    if results == QtGui.QMessageBox.Save:
                        # User ok'd it - attempt to save file with overwrite option on.
                        self.log.LogMessage("User confirmed overwriting .FBX file.", context=self.TOOL_NAME)
                        path = previzIO.SaveTake(take, changelistNumber, overwriteFbx=True)
                        savedTakes[take.Name] = path
                    else:
                        # User cancelled - move on to next take in the list.
                        self.log.LogWarning("User cancelled overwriting .FBX file.", context=self.TOOL_NAME)
            except Exception as error:
                # Handle all unexpected errors here.
                tracebackMsg = traceback.format_exc()
                self.log.LogWarning(tracebackMsg, context=self.TOOL_NAME)

                msg = "Error: {0}\nSkipping save for take: '{1}'".format(error.message or error.args[1], take.Name)
                QtGui.QMessageBox.warning(self, self.TOOL_NAME, msg)
                self.log.LogWarning(msg, context=self.TOOL_NAME)

        if not savedTakes:
            return
        msg = "{0} files saved:\n-{1}".format(len(savedTakes), "\n* ".join(savedTakes.values()))
        self.log.LogMessage(msg, context=self.TOOL_NAME)

        if self._p4SubmitCheckBox.isChecked():
            self.log.LogMessage("Submitting files to Perforce...", context=self.TOOL_NAME)
            Perforce.Submit(changelistNumber)
            self.log.LogMessage("Files have been submitted to Perforce.", context=self.TOOL_NAME)

        # User feedback.
        msgPt1 = "{0} files saved and added to changelist ({1}) for:".format(len(savedTakes), changelistNumber)
        msgPt2 = "\n".join(["* {}".format(takeName) for takeName in savedTakes])
        msg = "\n".join([msgPt1, msgPt2])
        detailedText = "\n".join(["* {}".format(path) for path in savedTakes.values()])
        msgBox = QtGui.QMessageBox()
        msgBox.setWindowTitle(self.windowTitle())
        msgBox.setText(msg)
        msgBox.setDetailedText(detailedText)
        msgBox.exec_()

    def _handleP4CLModeChanged(self):
        """
        Handler for updating subwidgets based on the p4 changelist mode combobox's currentIndexChanged signal.
        """
        if self._p4CLModeCmbox.currentText() == P4ChangeListModeOptions.CREATE_NEW:
            self._p4CLPrefixLEdit.setEnabled(True)
            self._p4CLPrefixLabel.setEnabled(True)
            self._p4SelectedCLLabel.setEnabled(False)
            self._p4SelectedCLCmbox.setEnabled(False)
            self._postProcessingLabel.setEnabled(True)
            self._postProcessingCheckBox.setEnabled(True)
        elif self._p4CLModeCmbox.currentText() == P4ChangeListModeOptions.ADD_TO_EXISTING:
            self._p4SelectedCLLabel.setEnabled(True)
            self._p4SelectedCLCmbox.setEnabled(True)
            self._p4CLPrefixLEdit.setEnabled(False)
            self._p4CLPrefixLabel.setEnabled(False)
            self._postProcessingLabel.setEnabled(False)
            self._postProcessingCheckBox.setEnabled(False)

    def _handleTrialDoesNotExist(self, trialId):
        """
        Handler for Realtime connection's trialDoesNotExist signal.
        """
        msg = "Received trial ID that does not exist in Watchstar: {}".format(trialId)
        self.log.LogWarning(msg, context=self.TOOL_NAME)
        title = "Trial Not Found!"
        msg = "\n".join([
            msg,
            "It is possible that a trial was created and deleted before MotionBuilder was notified by CaptureBoss.",
            "Check with the Watchstar/CaptureBoss operator(s) or email Techart!"
        ])
        QtGui.QMessageBox.warning(None, title, msg, QtGui.QMessageBox.Ok)
