from PySide import QtCore, QtGui

from RS.Core.AnimData.Widgets import onSetNotesTool


class TrialNotesWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(TrialNotesWidget, self).__init__(parent=parent) 
        self.setupUi()

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QHBoxLayout()

        # create widgets
        notesWidget = onSetNotesTool.MobuOnSetMocapNotesTool()

        # add widgets
        mainLayout.addWidget(notesWidget)

        # set layout
        self.setLayout(mainLayout)
