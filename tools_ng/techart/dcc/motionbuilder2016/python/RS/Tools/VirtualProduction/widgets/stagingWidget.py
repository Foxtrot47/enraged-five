import os
import numpy as np
import pyfbsdk as mobu
from RS import Config, Globals, Perforce
from RS.Tools.UI.Mocap import PropSetSnapToolbox
from RS.Utils import IO, Namespace, Scene
from RS.Utils.Scene import Component, Model, Cameras, Huds
from RS.Core.Mocap import MocapCommands
from RS.Core.VirtualProduction import tpvcLib
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Tools.VirtualProduction import const
from RS.Core.ReferenceSystem.Types.MocapStage import MocapStage
from RS.Tools.DisplayTakeName import Run as DisplayTakeNameRun

# Janky Pyside Import - allows same code on all projects
# NOTE: we wouldn't need this section if we used qt.py; Try this someday: https://github.com/digitaldomain/QtPyConvert
try:
    from PySide2 import QtCore, QtGui, QtWidgets
except ImportError:
    from PySide import QtCore, QtGui as QtWidgets


class StagingWidget(QtWidgets.QWidget):
    stageSignal = QtCore.Signal()

    def __init__(self, parent=None):
        super(StagingWidget, self).__init__(parent=parent)
        self.currentStage = None
        self.mocapStage = None
        self.manager = Manager()
        self.setupUi()

    def _getStageList(self):
        """Get list of stage references in the scene.

        Notes:
            Layout people have broken stages references in most files now by renaming the object
            manually, rather than through the ref editor, therefore when looking for a reference to find
            a scene object, the names no longer match.  Gathering stages via the references is therefore
            not reliable.

        Returns:
            list
        """
        # get stage list via ref editor int he first instance
        refStageList = self.manager.GetReferenceListByType(MocapStage, force=True)

        if len(refStageList) != 0:
            stageList = []
            for component in mobu.FBSystem().Scene.RootModel.Children:
                for refStage in refStageList:
                    if refStage.Namespace == Component.GetNamespace(component):
                        stageList.append(component)
            return stageList

        # if ref list is empty, try old custom property
        stageList = [component for component in Globals.Components
                     if component.PropertyList.Find("Stages") is not None
                     and isinstance(component, mobu.FBModel)]

        return stageList

    def setCurrentStage(self, stage):
        self.currentStage = stage

    def setMocapStage(self, stage):
        self.mocapStage = stage

    def modelSelectionList(self):
        return ['Stage', 'Blocking Models',]

    def getCurrentName(self):
        currentName = str(self.nameEdit.text())
        return currentName

    def checkForExtraVcams(self):
        if self.mocapStage:
            if self.mocapStage.location().name == "Bethpage, NY":
                if self.mocapStage.name in ["West Stage", "East Stage"]:
                    return True
        return False

    # TODO this should be data in watchstar - not values in the code; speak with Watchstar team and get it added.
    def getVcamIpDict(self):
        ipDict = {"Red": ("239.255.40.33", "9005")}
        useIpOverride = self.checkForExtraVcams()
        if useIpOverride:
            if self.mocapStage.name == "West Stage":
                ipDict["Red"] = ("10.41.101.28", "9007")
                ipDict["Blue"] = ("10.41.101.26", "9005")
                ipDict["Yellow"] = ("10.41.101.27", "9006")
            elif self.mocapStage.name == "East Stage":
                ipDict["Red"] = ("10.41.102.26", "9006")
                ipDict["Blue"] = ("10.41.102.30", "9007")
                ipDict["Orange"] = ("10.41.102.25", "9005")
        return ipDict

    def populateBlockingModelsList(self):
        """
        populate blocking models combobox
        """
        self.blockingModelsComboBox.clear()
        for fileName, filePath in const.blockingModelsList():
            self.blockingModelsComboBox.addItem(fileName, filePath)

    def populateColorComboBox(self):
        """
        populate color combobox
        """
        self.colorComboBox.clear()
        self.colorComboBox2.clear()
        for key, value in const.StageIcons().colorDict.iteritems():
            self.colorComboBox.addItem(value[0], key, value[1])
            self.colorComboBox2.addItem(value[0], key, value[1])

    def populateBuildAssetsComboBox(self):
        """
        populates build assets combobox
        """
        self.buildModelsComboBox.clear()
        for fileName, filePath in const.buildAssetsList():
            self.buildModelsComboBox.addItem(fileName, filePath)

    def _handleCreateStage(self):
        stagePath = const.PrevizFolder().GetStageAssetForStageLocation()
        if stagePath is None:
            loc = const.PrevizFolder().getLocationName()
            stg = const.PrevizFolder().getStageName()
            msg = "Unable to create stage, no asset for {} at {}".format(stg, loc)
            QtWidgets.QMessageBox.critical(self, "Virtual production Toolbox", msg)
            print(msg)
            return

        stageName = os.path.splitext(os.path.basename(stagePath))[0]
        colorName = str(self.colorComboBox.currentText())
        colorIndex = self.colorComboBox.currentIndex()
        colorVector = self.colorComboBox.itemData(colorIndex)

        # create stage
        Perforce.Sync(stagePath)
        fileInfo = Perforce.GetFileState(stagePath)
        localPath = fileInfo.get_ClientFilename()

        if os.path.exists(localPath):
            # create stage reference
            referenceList = self.manager.CreateReferences(localPath)

            # we will only have created 1 reference, so lets get a variable on that
            reference = referenceList[0]

            # change color
            reference.ApplyColor(mobu.FBColor(colorVector))

            # add custom property to record the color
            stageNull = None
            for stage in self._getStageList():
                if Component.GetNamespace(stage) == stageName:
                    stageNull = stage
                    break
            if stageNull is not None:
                colorProperty = stageNull.PropertyCreate('Original Color', mobu.FBPropertyType.kFBPT_charptr, "", False,
                                                         True, None)
                colorProperty.Data = colorName

            # change namespace
            self.manager.ChangeNamespace(reference, colorName, includeIteration=True)

            # fix double namespace issue - autodesk bug
            reference.FixHandleDoubleNamespace()

            # send signal that a stage has been created
            self.stageSignal.emit()
        else:
            print("Unable to create Stage, local path '{}' does not exist".format(localPath))

    def _handleCreateBlockingModel(self):
        """
        create blocking model
        """
        modelName = str(self.blockingModelsComboBox.currentText())
        userName = str(self.nameEdit.text())
        if userName == "":
            userName = modelName
        colorName = str(self.colorComboBox2.currentText())
        colorIndex = self.colorComboBox2.currentIndex()
        colorVector = self.colorComboBox2.itemData(colorIndex)

        # get path to selected blocking model
        buildAssetPath = self.blockingModelsComboBox.itemData(self.blockingModelsComboBox.currentIndex())
        Perforce.Sync(buildAssetPath)
        fileInfo = Perforce.GetFileState(buildAssetPath)
        localPath = fileInfo.get_ClientFilename()

        if os.path.exists(localPath):
            # create blocking model reference
            referenceList = self.manager.CreateReferences(localPath)
            # we will only have created 1 reference, so lets get a variable on that
            reference = referenceList[0]
            # change color
            reference.ApplyColor(mobu.FBColor(colorVector))
            # change namespace
            self.manager.ChangeNamespace(self.manager.GetReferenceByNamespace(reference.Name),
                                         userName)
            # fix double namespace issue - autodesk bug
            reference.FixHandleDoubleNamespace()
            # align to stage
            reference.AlignReferenceRootToActiveStage()

        # reset lineEdit and color
        self.nameEdit.setText("")

    def _handleVcamOptionUpdate(self):
        buttonEnabled = (
            self.vcamRedCheckBox.isChecked()
            or self.vcamYellowCheckBox.isChecked()
            or self.vcamBlueCheckBox.isChecked()
            or self.vcamOrangeCheckBox.isChecked()
        )
        self.vcamSetupButton.setEnabled(buttonEnabled)

    def _handleVcamSetup(self):
        """
        Handle the build and setup Virtual Cameras button
        """
        ipDict = self.getVcamIpDict()
        useIpOverride = self.checkForExtraVcams()
        newVcams = []
        if self.vcamRedCheckBox.isChecked():
            newVcams.append("Red")
        if self.vcamYellowCheckBox.isChecked():
            newVcams.append("Yellow")
        if self.vcamBlueCheckBox.isChecked():
            newVcams.append("Blue")
        if self.vcamOrangeCheckBox.isChecked():
            newVcams.append("Orange")
        for vcamColor in newVcams:
            vcamDevice = tpvcLib.TpvcDevice(color=vcamColor)
            tpvcLib.SetupTpvcDeviceCamera.setup(vcamDevice)
            if useIpOverride:
                ipTuple = ipDict[vcamColor]
                print(ipTuple)
                tpvcLib.SetupTpvcDeviceCamera.setMocapIP(vcamDevice, ipTuple)

    def _handleCreateBuildAssetModel(self):
        """
        create build asset
        """
        # get path to selected blocking model
        buildAssetPath = self.buildModelsComboBox.itemData(self.buildModelsComboBox.currentIndex())
        Perforce.Sync(buildAssetPath)
        fileInfo = Perforce.GetFileState(buildAssetPath)
        localPath = fileInfo.get_ClientFilename()
        if os.path.exists(localPath):
            # create build asset reference
            referenceList = self.manager.CreateReferences(localPath)

    def _handleAddSlate(self):
        """Add correct slate to the scene. The slate is the same for all stages."""
        slatePaths = []
        for slatePath in const.slatePaths():
            Perforce.Sync(slatePath)
            fileInfo = Perforce.GetFileState(slatePath)
            localPath = fileInfo.get_ClientFilename()
            if os.path.exists(localPath):
                slatePaths.append(localPath)

        if len(slatePaths) > 0:
            slateRefList = self.manager.CreateReferences(slatePaths)

        else:
            print("Unable to create Slate, local paths '{}' does not exist".format(slatePaths))

    def _handleAddBasePreviz(self):
        """Add in the base previz assets - everything under the base previz directory.
        Also adds in the slate, which is now a separate type from the base previz
        """
        basePrevizFileList = []

        # Get the assets for the staged, sync them and get their local file paths
        for p4Path in const.PrevizFolder().GetBasePrevisAssetsForStageLocation():
            Perforce.Sync(p4Path)
            fileInfo = Perforce.GetFileState(p4Path)
            localPath = fileInfo.get_ClientFilename()
            if os.path.exists(localPath):
                basePrevizFileList.append(localPath)

        # create references
        newRefs = self.manager.CreateReferences(basePrevizFileList)

    def _handleAddPrevizHud(self):
        """Adds in the previz hud for the current location, setting it on the producer perspective camera"""
        hudPath = const.PrevizFolder().GetHudForLocation()
        if not hudPath:
            loc = const.PrevizFolder().getLocationName()
            msg = "Unable to find HUD for {}".format(loc)
            QtWidgets.QMessageBox.critical(self, const.AppInfo.name, msg)
            print(msg)
            return

        # Get the asset for the hud, sync them and get their local file paths
        Perforce.Sync(hudPath)
        fileInfo = Perforce.GetFileState(hudPath)
        localPath = str(fileInfo.get_ClientFilename())
        if not os.path.exists(localPath):
            msg = "Unable to find '{}' on disk".format(localPath)
            QtWidgets.QMessageBox.critical(self, const.AppInfo.name, msg)
            print(msg)
            return

        # Load in via ref editor
        newRefs = self.manager.CreateReferences([localPath])
        if len(newRefs) != 1:
            msg = "Unable to load in asset '{}'".format(localPath)
            QtWidgets.QMessageBox.critical(self, const.AppInfo.name, msg)
            print(msg)
            return
        huds = newRefs[0].getHUDElements()

        # assign the hud to the producer camera
        Huds.assignHudToCamera(Cameras.getProducerPerspective(), huds[0])

    def _handleSnapPropsToSet(self):
        popup = MocapCommands.PropSetSnap()
        if popup == True:
            PropSetSnapToolbox.Run()
        else:
            QtWidgets.QMessageBox.information(None,
                                              "",
                                              "All found prop matches have been aligned.",
                                              QtWidgets.QMessageBox.Ok)

    def _handleRedMarkup(self):
        MocapCommands.TurnRedToggle()

    def _convertToInches(self, distanceValue, dimensionType=""):
        """Convert worldSpace boundingbox measurements of an FBModel (originally in cm) to feet/inches.

        Args:
            distanceValue (double): Value (cm) to convert to imperial system.
            dimensionType (str, optional): Length, width, or height.

        Returns:
            list:  feetValue (double), inchesValue (double), distanceValue (double), dimensionString (double)
        """
        feetToCm = 0.0328084  # ft = cm * 0.0328084
        feetValue = distanceValue * feetToCm

        feetRounded = np.modf(feetValue)[1]
        feetDecimal = np.modf(feetValue)[0]
        inchesValue = np.around(feetDecimal * 12, 1)

        dimensionType = "      {0}: ".format(dimensionType)
        dimensionString = "{0}{1} ft {2} in".format(dimensionType, feetRounded, inchesValue)

        return (feetValue, inchesValue, distanceValue, dimensionString)

    def _handleDimensionOfSelected(self):
        """
        Pop up window with the dimensions of the selected model in feet/inches and cm.
        """
        # Get the selected models.
        components = mobu.FBModelList()
        mobu.FBGetSelectedModels(components)

        if len(components) == 0:
            message = "Nothing is selected."
        else:
            message = ""
            for component in components:
                if type(component) == mobu.FBModel:
                    cleanUp_Model = Model.ProcessModelForBoundingBox(component, axisAligned=True)

                    boundingBox = Model.ComputeBoundingBox(cleanUp_Model)
                    boundingBox["sourceName"] = component.Name
                    cleanUp_Model.FBDelete()

                    # The boundingBox contains the cm values
                    lengthInCm = boundingBox["length"]
                    widthInCm = boundingBox["width"]
                    heightInCm = boundingBox["height"]

                    # Converting from centimeters to feet and inches
                    _, _, _, lengthInImperial = self._convertToInches(lengthInCm, dimensionType="Length")
                    _, _, _, widthInImperial = self._convertToInches(widthInCm, dimensionType="Width")
                    _, _, _, heightInImperial = self._convertToInches(heightInCm, dimensionType="Height")

                    # Create string for message
                    message = "{0}{1}\n{2} ({3} cm)\n{4} ({5} cm)\n{6} ({7} cm)\n".format(
                        message,
                        component.Name,
                        heightInImperial,
                        np.around(heightInCm, 1),
                        widthInImperial,
                        np.around(widthInCm, 1),
                        lengthInImperial,
                        np.around(lengthInCm, 1),
                    )

        QtWidgets.QMessageBox.information(self, "Dimensions of Selected Model", message, QtWidgets.QMessageBox.Ok)

    def _handleLeadCams(self):
        MocapCommands.SetupLeadinLeadOutConstraints()

    def _handleResetCamFarPlane(self):
        cameraList = [mobu.FBSystem().Scene.Cameras[0]]
        cameraList.extend([cam for cam in mobu.FBSystem().Scene.Cameras if cam.LongName.lower().startswith("tpvc")])
        for camera in cameraList:
            # Turn Off Center and Set Color - skipping export cam
            camera.ViewOpticalCenter = False
            if "exportcamera" not in camera.Name.lower():
                camera.FrameColor = mobu.FBColor(0, 0, 0)

            # Remove Back Plate Texture
            plateProp = camera.PropertyList.Find("Background Texture")
            plateProp.DisconnectAllSrc()

            farPlaneProperty = camera.PropertyList.Find("Far Plane")
            gridProperty = camera.PropertyList.Find("Grid")
            if farPlaneProperty != None:
                farPlaneProperty.SetLocked(False)
                farPlaneProperty.Data = 4000000.0
                farPlaneProperty.SetLocked(True)
            if gridProperty != None:
                gridProperty.SetLocked(False)
                gridProperty.Data = False
                gridProperty.SetLocked(True)

    def _handleCamLabelVisibility(self):
        MocapCommands.TurnOffProducePerspectiveLabel()

    def _handleSetStageBoxName(self):
        try:
            MocapCommands.SetStageBoxName()
        except ValueError, errorMsg:
            QtWidgets.QMessageBox.critical(self, "Virtual Production ToolBox - ERROR", errorMsg.message)

    def _handleDisplayTakeNumberButton(self):
        try:
            DisplayTakeNameRun.Run()
        except ValueError, errorMsg:
            QtWidgets.QMessageBox.critical(self, "Virtual Production ToolBox - ERROR", errorMsg.message)

    def setDefaultStageName(self):
        self.nameEdit.clear()
        filename = Scene.GetSceneName()
        self.nameEdit.setText(filename)

    def refreshVcamButtons(self):
        # Uncheck all by default.
        self.vcamRedCheckBox.setCheckState(QtCore.Qt.CheckState.Unchecked)
        self.vcamYellowCheckBox.setCheckState(QtCore.Qt.CheckState.Unchecked)
        self.vcamBlueCheckBox.setCheckState(QtCore.Qt.CheckState.Unchecked)
        self.vcamOrangeCheckBox.setCheckState(QtCore.Qt.CheckState.Unchecked)

        # Enable/Disable Extra Cams
        useExtraCams = self.checkForExtraVcams()
        blueEnabled = True if useExtraCams and self.mocapStage.name in ("West Stage", "East Stage") else False
        self.vcamBlueCheckBox.setEnabled(blueEnabled)
        yellowEnabled = True if useExtraCams and self.mocapStage.name in ("West Stage") else False
        self.vcamYellowCheckBox.setEnabled(yellowEnabled)
        orangeEnabled = True if useExtraCams and self.mocapStage.name in ("East Stage") else False
        self.vcamOrangeCheckBox.setEnabled(orangeEnabled)

    def refreshUi(self):
        self.setDefaultStageName()
        self.refreshVcamButtons()

    def setupUi(self):
        """Sets up the widgets, their properties and launches call events"""
        # create layouts
        mainLayout = QtWidgets.QGridLayout()
        stagingGroupBoxGridLayout = QtWidgets.QGridLayout()
        blockingGroupBoxGridLayout = QtWidgets.QGridLayout()
        buildAssetsGroupBoxGridLayout = QtWidgets.QGridLayout()
        preShootGroupBoxGridLayout = QtWidgets.QVBoxLayout()
        vcamSetupLayout = QtWidgets.QVBoxLayout()
        vcamCheckBoxLayout = QtWidgets.QHBoxLayout()

        # create widgets
        # Add a warning widget if user doesn't have certain permissions.
        permissionsLabel = None
        if const.PrevizFolder.hasVirutalProductionRoot() is False:
            permissionsLabel = QtWidgets.QLabel(
                "User does not have access to //virtualproduction/ or it is not in the current workspace mappings.\n"
                "Check with IT that you have access and talk to Tech Art for any additional assistance. "
                "Some features will be disabled.",
            )
            permissionsLabel.setStyleSheet("QLabel { color : #D00000; font-size : 10pt; }")
            self.setEnabled(False)

        # staging
        stagingGroupBox = QtWidgets.QGroupBox()
        colorLabel = QtWidgets.QLabel()
        self.colorComboBox = QtWidgets.QComboBox()
        self.createStageButton = QtWidgets.QPushButton()

        # build assets
        buildGroupBox = QtWidgets.QGroupBox()
        buildLabel = QtWidgets.QLabel()
        self.buildModelsComboBox = QtWidgets.QComboBox()
        self.createbuildButton = QtWidgets.QPushButton()

        # blocking
        blockingGroupBox = QtWidgets.QGroupBox()
        modelLabel = QtWidgets.QLabel()
        self.blockingModelsComboBox = QtWidgets.QComboBox()
        nameLabel = QtWidgets.QLabel()
        self.nameEdit = QtWidgets.QLineEdit()
        colorLabel2 = QtWidgets.QLabel()
        self.colorComboBox2 = QtWidgets.QComboBox()
        self.createModelButton = QtWidgets.QPushButton()

        # pre-shoot
        preShootGroupBox = QtWidgets.QGroupBox()
        basePrevizButton = QtWidgets.QPushButton()
        previzHudButton = QtWidgets.QPushButton()
        slateButton = QtWidgets.QPushButton()
        snapPropButton = QtWidgets.QPushButton()
        redMarkupButton = QtWidgets.QPushButton()
        dimensionsButton = QtWidgets.QPushButton()
        multiCamButton = QtWidgets.QPushButton()
        leadInOutCamButton = QtWidgets.QPushButton()
        resetCamFarPlaneButton = QtWidgets.QPushButton()
        visibilityCheckBox = QtWidgets.QCheckBox()
        setStageBoxName = QtWidgets.QPushButton("Set Stage Box Name")
        displayTakeNumberButton = QtWidgets.QPushButton("Display Take Number")

        # virtual cameras
        virtualCamerasGroupBox = QtWidgets.QGroupBox("Virtual Cameras Setup")
        self.vcamRedCheckBox = QtWidgets.QCheckBox()
        self.vcamRedCheckBox.setText("Red")
        self.vcamRedCheckBox.stateChanged.connect(self._handleVcamOptionUpdate)
        self.vcamYellowCheckBox = QtWidgets.QCheckBox()
        self.vcamYellowCheckBox.setText("Yellow")
        self.vcamYellowCheckBox.stateChanged.connect(self._handleVcamOptionUpdate)
        self.vcamBlueCheckBox = QtWidgets.QCheckBox()
        self.vcamBlueCheckBox.setText("Blue")
        self.vcamBlueCheckBox.stateChanged.connect(self._handleVcamOptionUpdate)
        self.vcamOrangeCheckBox = QtWidgets.QCheckBox()
        self.vcamOrangeCheckBox.setText("Orange")
        self.vcamOrangeCheckBox.stateChanged.connect(self._handleVcamOptionUpdate)
        self.vcamSetupButton = QtWidgets.QPushButton("Setup Vcams")
        self.vcamSetupButton.setEnabled(False)

        self.refreshVcamButtons()

        # widget properties
        self.setDefaultStageName()
        stagingGroupBox.setTitle("Staging")
        blockingGroupBox.setTitle("Blocking Models")
        buildGroupBox.setTitle("Build Assets")
        modelLabel.setText('Model:')
        buildLabel.setText('Model:')
        nameLabel.setText('Name:')
        colorLabel.setText('Color:')
        colorLabel2.setText('Color:')
        self.createStageButton.setText('Create Stage')
        self.createbuildButton.setText('Create Build Asset')
        self.createModelButton.setText('Create Model')
        preShootGroupBox.setTitle("Pre-Shoot Options")
        basePrevizButton.setText('Add Base Previz')
        previzHudButton.setText('Add Previz Hud')
        slateButton.setText('Add Slate')
        snapPropButton.setText('Snap Props to Set Object')
        redMarkupButton.setText('Red Markup')
        dimensionsButton.setText('Dimensions of Selected Model')
        multiCamButton.setText('Open Multi-Camera Viewer')
        leadInOutCamButton.setText('Lead Cams')
        resetCamFarPlaneButton.setText("Reset Cameras")
        visibilityCheckBox.setText('Visibility for Perspective Camera Label')
        stageSpacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        blockingSpacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        virtualCameraSpacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        preshootSpacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        centerSpacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.buildModelsComboBox.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum)
        self.colorComboBox.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum)

        # populate comboboxes
        self.populateBlockingModelsList()
        self.populateColorComboBox()
        self.populateBuildAssetsComboBox()

        # add widgets to groupBox
        stagingGroupBoxGridLayout.addWidget(colorLabel, 0, 0)
        stagingGroupBoxGridLayout.addWidget(self.colorComboBox, 0, 1)
        stagingGroupBoxGridLayout.addWidget(self.createStageButton, 1, 0, 1, 2)

        blockingGroupBoxGridLayout.addWidget(modelLabel, 0, 0)
        blockingGroupBoxGridLayout.addWidget(self.blockingModelsComboBox, 0, 1)
        blockingGroupBoxGridLayout.addWidget(nameLabel, 1, 0)
        blockingGroupBoxGridLayout.addWidget(self.nameEdit, 1, 1)
        blockingGroupBoxGridLayout.addWidget(colorLabel2, 2, 0)
        blockingGroupBoxGridLayout.addWidget(self.colorComboBox2, 2, 1)
        blockingGroupBoxGridLayout.addWidget(self.createModelButton, 4, 0, 1, 2)

        buildAssetsGroupBoxGridLayout.addWidget(buildLabel, 0, 0)
        buildAssetsGroupBoxGridLayout.addWidget(self.buildModelsComboBox, 0, 1)
        buildAssetsGroupBoxGridLayout.addWidget(self.createbuildButton, 1, 0, 1, 2)

        preShootGroupBoxGridLayout.addWidget(basePrevizButton)
        preShootGroupBoxGridLayout.addWidget(previzHudButton)
        preShootGroupBoxGridLayout.addWidget(slateButton)
        preShootGroupBoxGridLayout.addWidget(snapPropButton)
        preShootGroupBoxGridLayout.addWidget(redMarkupButton)
        preShootGroupBoxGridLayout.addWidget(dimensionsButton)
        preShootGroupBoxGridLayout.addWidget(multiCamButton)
        preShootGroupBoxGridLayout.addWidget(leadInOutCamButton)
        preShootGroupBoxGridLayout.addWidget(resetCamFarPlaneButton)
        preShootGroupBoxGridLayout.addWidget(visibilityCheckBox)
        preShootGroupBoxGridLayout.addWidget(setStageBoxName)
        preShootGroupBoxGridLayout.addWidget(displayTakeNumberButton)
        preShootGroupBoxGridLayout.addItem(preshootSpacerItem)

        vcamCheckBoxLayout.addWidget(self.vcamRedCheckBox)
        vcamCheckBoxLayout.addWidget(self.vcamBlueCheckBox)
        vcamCheckBoxLayout.addWidget(self.vcamYellowCheckBox)
        vcamCheckBoxLayout.addWidget(self.vcamOrangeCheckBox)
        vcamSetupLayout.addLayout(vcamCheckBoxLayout)
        vcamSetupLayout.addWidget(self.vcamSetupButton)

        # set groupBox layout
        stagingGroupBox.setLayout(stagingGroupBoxGridLayout)
        blockingGroupBox.setLayout(blockingGroupBoxGridLayout)
        buildGroupBox.setLayout(buildAssetsGroupBoxGridLayout)
        preShootGroupBox.setLayout(preShootGroupBoxGridLayout)
        virtualCamerasGroupBox.setLayout(vcamSetupLayout)

        # add to layout
        if permissionsLabel:
            mainLayout.addWidget(permissionsLabel, 0, 0, 1, 2)
        mainLayout.addWidget(stagingGroupBox, 1, 0)
        mainLayout.addItem(stageSpacerItem, 2, 0)
        mainLayout.addWidget(buildGroupBox, 3, 0)
        mainLayout.addItem(blockingSpacerItem, 4, 0)
        mainLayout.addWidget(blockingGroupBox, 5, 0)
        mainLayout.addItem(virtualCameraSpacerItem, 6, 0)
        mainLayout.addWidget(virtualCamerasGroupBox, 7, 0)
        mainLayout.addWidget(preShootGroupBox, 1, 1, 7, 1)
        mainLayout.addItem(centerSpacerItem, 1, 1)

        # set layout
        self.setLayout(mainLayout)

        # event callbacks
        self.createStageButton.pressed.connect(self._handleCreateStage)
        self.createbuildButton.pressed.connect(self._handleCreateBuildAssetModel)
        self.createModelButton.pressed.connect(self._handleCreateBlockingModel)
        basePrevizButton.pressed.connect(self._handleAddBasePreviz)
        previzHudButton.pressed.connect(self._handleAddPrevizHud)
        slateButton.pressed.connect(self._handleAddSlate)
        snapPropButton.pressed.connect(self._handleSnapPropsToSet)
        redMarkupButton.pressed.connect(self._handleRedMarkup)
        dimensionsButton.pressed.connect(self._handleDimensionOfSelected)
        multiCamButton.pressed.connect(MocapCommands.OpenMultiCameraViewer)
        leadInOutCamButton.pressed.connect(self._handleLeadCams)
        resetCamFarPlaneButton.pressed.connect(self._handleResetCamFarPlane)
        visibilityCheckBox.pressed.connect(self._handleCamLabelVisibility)
        setStageBoxName.pressed.connect(self._handleSetStageBoxName)
        displayTakeNumberButton.released.connect(self._handleDisplayTakeNumberButton)
        self.vcamSetupButton.released.connect(self._handleVcamSetup)
