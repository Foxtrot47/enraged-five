import os
import collections
import json
import datetime

from dateutil import parser
from PySide import QtCore, QtGui

from RS import Config, Perforce, Globals, ProjectData
from RS.Core.AnimData import Context
from RS.Tools.CameraToolBox.PyCore.Metaclasses import singleton
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize


class AppInfo(object):
    """Enum for application info."""
    NAME = "Virtual Production Toolbox"
    SHOOT_PREP_TEST_PLAN_URL = "https://hub.gametools.dev/x/-vMTMQ"
    SHOOT_DAY_TEST_PLAN_URL = "https://hub.gametools.dev/x/1vMTMQ"


class PrevizFolder(object):
    """
    directs the user's current location to the correct previz folder
    """
    __metaclass__ = singleton.Singleton

    PERFORCE_ROOT = "//virtualproduction/PrevizAssets"

    def __init__(self, currentStage=None):
        super(PrevizFolder, self).__init__()
        self._settingDict = self._getAssetSettingsDict()
        self._currentStage = currentStage

    def setCurrentStage(self, newStage):
        self._currentStage = newStage

    def currentStage(self):
        return self._currentStage

    def currentLocation(self):
        if self._currentStage is None:
            return None
        return self._currentStage.location()

    def getStageName(self):
        if self._currentStage is None:
            return "<NO STAGE SET>"
        return self._currentStage.name

    def getLocationName(self):
        loc = self.currentLocation()
        if loc is None:
            return "<NO LOCATION SET>"
        return loc.name

    def GetStudioFolderName(self):
        """
        Returns:
            String - user's location mocap path dir
        """
        loc = self.currentLocation()
        if loc is None:
            return "<NO LOCATION SET>"
        return os.path.basename(os.path.normpath(loc.previsFolderPath()))

    def GetGlobalFolderName(self):
        """
        Returns:
            String - global dir path
        """
        return os.path.join(Config.VirtualProduction.Path.Previz, 'GlobalAssets')

    def GetCityLocation(self):
        """
        Returns:
            String - name of user's city
        """
        return Context.animData.getCurrentLocation().city()

    @staticmethod
    def getAssetSettingsPath():
        return os.path.join(Config.VirtualProduction.Path.Previz, "virtualProductionTool.json")

    @memorize.memoized
    @classmethod
    def hasVirutalProductionRoot(cls):
        """
        Checks to see if the user has access to the virtual prodction root

        Returns:
            bool
        """
        with Perforce.EnableExceptions():
            try:
                Perforce.Dirs([cls.PERFORCE_ROOT])
                return True
            except Perforce.P4Exception:
                return False

    @classmethod
    def _getAssetSettingsDict(cls):
        """
        Gets the asset settings data.

        As this const is used in a few places and not all users have access to the depot virtualProductionTool.json
        lives in, we need to have handling that won't raise an error when the file can't be found.

        Returns:
            dict
        """
        if cls.hasVirutalProductionRoot() is False:
            return {}

        filePath = cls.getAssetSettingsPath()
        with Perforce.EnableExceptions():
            try:
                Perforce.Sync([filePath])
            except Perforce.P4Exception:
                Globals.Ulog.LogWarning("Error Syncing file at '{}'".format(filePath), context="VirtualProductionToolbox")
                Globals.Ulog.Flush()  # Force the file to be written to disk.
                return {}

        if not os.path.exists(filePath):
            Globals.Ulog.LogWarning("Unable to find settings file at '{}'".format(filePath), context="VirtualProductionToolbox")
            Globals.Ulog.Flush()  # Force the file to be written to disk.
            return {}

        with open(filePath) as fileHandle:
            settingsDict = json.load(fileHandle)
        return settingsDict

    def GetBasePrevisAssetsForStageLocation(self):
        return self._findInDict(self._settingDict, ["stageBased", "basePrevis", self.getLocationName(), self.getStageName()], [])

    def GetHudForLocation(self):
        return self._findInDict(self._settingDict, ["locationBased", "stageHuds", self.getLocationName()], None)

    def GetAssetsForLocation(self):
        return self._findInDict(self._settingDict, ["locationBased", "stageAssets", self.getLocationName()], [])

    def GetStageAssetForStageLocation(self):
        return self._findInDict(self._settingDict, ["stageBased", "stages", self.getLocationName(), self.getStageName()], None)

    @staticmethod
    def _findInDict(dictToUse, items, defaultValue=None):
        currentLevel = dictToUse
        for item in items:
            res = currentLevel.get(item)
            if res is None:
                return defaultValue
            currentLevel = res
        return currentLevel


class SpectatorFileExtentions(object):
    """Constants for the supported file extensions"""
    AUTODESK_FBX = ".fbx"
    GIANT_MAPPINGS = ".gmap"


class SpectatorFolder(object):
    """Consts and methods related to the spectator folder."""

    ROOT = os.path.join("N:\\", "Projects", "All", "Mocap", "pcapspectator")
    DEFAULT_PROJECT_CODENAME = "ProjectCodeName"

    @memorize.memoized
    @classmethod
    def hasAccess(cls):
        """
        Checks to see if the user has permissions to access the spectator folder
        """
        try:
            os.listdir(cls.ROOT)
            return True
        except:
            pass
        return False

    @classmethod
    def getStageFolder(cls, stage, date):
        """Get the stage folder for the stage and session.

        Notes:
            * Example path: N:\Projects\All\Mocap\pcapspectator\SD\StudioA\2020-10-20

        Args:
            stage (stages.Stage): The stage base naming off of.
            date (datetime.datetime): Datetime representation of the shoot date.

        Returns:
            str
        """
        location = stage.location()
        return os.path.join(
                cls.ROOT, location.locationAbbreviation(), stage.name.replace(" ", ""), date.strftime("%Y-%m-%d"),
            )

    @classmethod
    def getPath(cls, stage, ext=None):
        """Get a file path for the stage and .fbx.

        Notes:
            * Example path: N:\Projects\All\Mocap\pcapspectator\SD\StudioA\2020-10-20\ProjectCodeName_SpectatorScene.fbx

        Args:
            stage (stages.Stage): The stage base naming off of.
            ext (str, optional): The file extension for the path; Default: SpectatorFileExtentions.AUTODESK_FBX

        Returns:
            str
        """
        ext = ext or SpectatorFileExtentions.AUTODESK_FBX

        session = Context.animData.getCurrentSessionByStage(stage)
        project = None
        projectCodename = cls.DEFAULT_PROJECT_CODENAME
        if session is not None:
            date = parser.parse(session.sessionDate())  # Works regardless of current project unless dev are tools used.
            project = session.project()
        else:
            date = datetime.datetime.now()  # Need local date.
            # Try to find a DLC Watchstar project by filtering based on name.
            adProjectName = ProjectData.data.GetAnimDataProjectName().lower()
            for possibleMatch in Context.animData.getAllProjects():
                possibleMatchName = possibleMatch.name.lower()
                if possibleMatchName.startswith(adProjectName) and possibleMatchName.endswith("dlc"):
                    project = possibleMatch
                    break
            if project is None:  # There isn't a DLC Watchstar project yet, try to use the main project.
                project = Context.animData.getCurrentProject()
        if project:
            projectCodename = project.codeName()

        stageFolder = cls.getStageFolder(stage, date)

        # Example "ProjectCodeName_SpectatorScene.fbx"
        fileName = "{}_SpectatorScene{}".format(projectCodename, ext).replace(" ", "_")

        return os.path.join(stageFolder, fileName)


class Icons(object):
    """
    class for creating a dictionary of QIcons and Vectors (for color)
    """
    # create qicons
    Blue = QtGui.QIcon()
    LightBlue = QtGui.QIcon()
    DarkBlue = QtGui.QIcon()
    Black = QtGui.QIcon()
    Orange = QtGui.QIcon()
    DarkOrange = QtGui.QIcon()
    Red = QtGui.QIcon()
    LightPurple = QtGui.QIcon()
    DarkPurple = QtGui.QIcon()
    LightYellow = QtGui.QIcon()
    DarkYellow = QtGui.QIcon()
    DarkGrey = QtGui.QIcon()
    LightGreen = QtGui.QIcon()
    Green = QtGui.QIcon()
    DarkGreen = QtGui.QIcon()
    DarkBrown = QtGui.QIcon()
    LightBrown = QtGui.QIcon()
    Pink = QtGui.QIcon()
    DarkPink = QtGui.QIcon()
    White = QtGui.QIcon()
    LightPink = QtGui.QIcon()
    Purple = QtGui.QIcon()
    SteelBlue = QtGui.QIcon()
    Olive = QtGui.QIcon()

    Folder = QtGui.QIcon()
    File = QtGui.QIcon()

    # default image path
    mocapImageFolder = os.path.join(Config.Script.Path.ToolImages, "Mocap")
    refEditorImageFolder = os.path.join(Config.Script.Path.ToolImages, "ReferenceEditor")

    # add icon image to qicon
    Blue.addPixmap(QtGui.QPixmap("{0}actor_color_blue.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                   QtGui.QIcon.Off)
    LightBlue.addPixmap(QtGui.QPixmap("{0}actor_color_lightblue.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                        QtGui.QIcon.Off)
    DarkBlue.addPixmap(QtGui.QPixmap("{0}actor_color_darkblue.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                       QtGui.QIcon.Off)
    Black.addPixmap(QtGui.QPixmap("{0}actor_color_black.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                    QtGui.QIcon.Off)
    Orange.addPixmap(QtGui.QPixmap("{0}actor_color_orange.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                          QtGui.QIcon.Off)
    DarkOrange.addPixmap(QtGui.QPixmap("{0}actor_color_darkorange.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                         QtGui.QIcon.Off)
    Red.addPixmap(QtGui.QPixmap("{0}actor_color_red.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    LightPurple.addPixmap(QtGui.QPixmap("{0}actor_color_lightpurple.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                          QtGui.QIcon.Off)
    DarkPurple.addPixmap(QtGui.QPixmap("{0}actor_color_darkpurple.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                         QtGui.QIcon.Off)
    LightYellow.addPixmap(QtGui.QPixmap("{0}actor_color_lightyellow.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                          QtGui.QIcon.Off)
    DarkYellow.addPixmap(QtGui.QPixmap("{0}actor_color_darkyellow.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                         QtGui.QIcon.Off)
    DarkGrey.addPixmap(QtGui.QPixmap("{0}actor_color_darkgrey.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                       QtGui.QIcon.Off)
    LightGreen.addPixmap(QtGui.QPixmap("{0}actor_color_lightgreen.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                         QtGui.QIcon.Off)
    Green.addPixmap(QtGui.QPixmap("{0}actor_color_green.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                    QtGui.QIcon.Off)
    DarkGreen.addPixmap(QtGui.QPixmap("{0}actor_color_darkgreen.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                        QtGui.QIcon.Off)
    DarkBrown.addPixmap(QtGui.QPixmap("{0}actor_color_darkbrown.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                    QtGui.QIcon.Off)
    LightBrown.addPixmap(QtGui.QPixmap("{0}actor_color_lightbrown.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                    QtGui.QIcon.Off)
    Pink.addPixmap(QtGui.QPixmap("{0}actor_color_pink.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                   QtGui.QIcon.Off)
    DarkPink.addPixmap(QtGui.QPixmap("{0}actor_color_darkpink.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                       QtGui.QIcon.Off)
    White.addPixmap(QtGui.QPixmap("{0}actor_color_white.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                    QtGui.QIcon.Off)
    LightPink.addPixmap(QtGui.QPixmap("{0}actor_color_lightpink.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                    QtGui.QIcon.Off)
    Purple.addPixmap(QtGui.QPixmap("{0}actor_color_purple.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                    QtGui.QIcon.Off)
    SteelBlue.addPixmap(QtGui.QPixmap("{0}actor_color_steelblue.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                    QtGui.QIcon.Off)
    Olive.addPixmap(QtGui.QPixmap("{0}actor_color_olive.png".format(mocapImageFolder)), QtGui.QIcon.Normal,
                    QtGui.QIcon.Off)

    Folder.addPixmap(QtGui.QPixmap("{0}Folder.png".format(refEditorImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    File.addPixmap(QtGui.QPixmap("{0}File.png".format(refEditorImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)

    # add both qicons and color vectors to a dictionary
    # need to divide rgb by 256.0 to get mobu-friendly rgb
    # order dict based on url:bugstar:3043112
    colorDict = collections.OrderedDict()

    colorDict["Blue"] = (Blue, (0.0 / 256.0, 99.84 / 256.0, 253.44 / 256.0))
    colorDict["LightBlue"] = (LightBlue, (97.28 / 256.0, 184.32 / 256.0, 186.88 / 256.0))
    colorDict["DarkBlue"] = (DarkBlue, (23.04 / 256.0, 25.6 / 256.0, 112.64 / 256.0))
    colorDict["Green"] = (Green, (40.96 / 256.0, 199.68 / 256.0, 71.68 / 256.0))
    colorDict["LightGreen"] = (LightGreen, (115.2 / 256.0, 220.16 / 256.0, 30.72 / 256.0))
    colorDict["DarkGreen"] = (DarkGreen, (0 / 256.0, 64 / 256.0, 0 / 256.0))
    colorDict["LightYellow"] = (LightYellow, (255 / 256.0, 237 / 256.0, 105 / 256.0))
    colorDict["DarkYellow"] = (DarkYellow, (232 / 256.0, 186 / 256.0194, 0 / 256.0))
    colorDict["LightPurple"] = (LightPurple, (194 / 256.0, 153 / 256.0, 255 / 256.0))
    colorDict["DarkPurple"] = (DarkPurple, (97.28 / 256.0, 0 / 256.0, 153.6 / 256.0))
    colorDict["Orange"] = (Orange, (256 / 256.0, 128 / 256.0, 0 / 256.0))
    colorDict["DarkOrange"] = (DarkOrange, (256 / 256.0, 81.92 / 256.0, 0 / 256.0))
    colorDict["Pink"] = (Pink, (255 / 256.0, 128.0 / 256.0, 204.8 / 256.0))
    colorDict["DarkPink"] = (DarkPink, (166 / 256.0, 94 / 256.0, 105 / 256.0))
    colorDict["Red"] = (Red, (202.24 / 256.0, 35.84 / 256.0, 35.84 / 256.0))
    colorDict["LightBrown"] = (LightBrown, (225.28 / 256.0, 189.44 / 256.0, 128 / 256.0))
    colorDict["DarkBrown"] = (DarkBrown, (112.64 / 256.0, 48.64 / 256.0, 5.12 / 256.0))
    colorDict["DarkGrey"] = (DarkGrey, (82 / 256.0, 82 / 256.0, 82 / 256.0))
    colorDict["Black"] = (Black, (20 / 256.0, 20 / 256.0, 20 / 256.0))
    colorDict["White"] = (White, (256.0 / 256.0, 256.0 / 256.0, 256.0 / 256.0))
    colorDict["LightPink"] = (LightPink, (256.0 / 256.0, 0 / 256.0, 204.8 / 256.0))
    colorDict["Purple"] = (Purple, (168.96 / 256.0, 43.52 / 256.0, 256.0 / 256.0))
    colorDict["SteelBlue"] = (SteelBlue, (51.2 / 256.0, 120.32 / 256.0, 161.28 / 256.0))
    colorDict["Olive"] = (Olive, (117.76 / 256.0, 163.84 / 256.0, 81.92 / 256.0))


class ColorNameChange(object):
    '''
    some of the color names were changed, so we need a dictionary of the before and after names,
    to ensure we are using the latest names.
    url:bugstar:3043112

    Return:
    Dict [string]=string
    '''
    colorNameChangeDict = {"CornFlower": "LightBlue",
                           "Brown": "DarkBrown",
                           "Beige": "LightBrown",
                           "DarkGrey": "Gray",
                           "NavyBlue": "DarkBlue",
                           "TexasOrange": "DarkOrange",
                           "LightOrange": "Orange",
                           "Yellow": "DarkYellow",
                           "Grey": "DarkGrey",
                           "MiddleGreen": "DarkGreen",
                           "NeonPink": "Pink",
                           "DustyRose": "DarkPink",
                           "Violet": "LightPurple",
                           }


class StageIcons(object):
    """
    class for creating a dictionary of QIcons and Vectors (for color)
    """
    # create qicons
    red = QtGui.QIcon()
    mintGreen = QtGui.QIcon()
    carolinaBlue = QtGui.QIcon()
    magenta = QtGui.QIcon()
    pink = QtGui.QIcon()
    green = QtGui.QIcon()
    purple = QtGui.QIcon()
    white = QtGui.QIcon()
    grey = QtGui.QIcon()
    blue = QtGui.QIcon()

    # default image path
    stageImageFolder = "{0}/Mocap/stages/".format(Config.Script.Path.ToolImages)

    # add icon image to qicon
    red.addPixmap(QtGui.QPixmap("{0}stage_color_red.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    mintGreen.addPixmap(QtGui.QPixmap("{0}stage_color_mintgreen.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    carolinaBlue.addPixmap(QtGui.QPixmap("{0}stage_color_carolinablue.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    magenta.addPixmap(QtGui.QPixmap("{0}stage_color_magenta.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    pink.addPixmap(QtGui.QPixmap("{0}stage_color_pink.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    green.addPixmap(QtGui.QPixmap("{0}stage_color_green.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    purple.addPixmap(QtGui.QPixmap("{0}stage_color_purple.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    white.addPixmap(QtGui.QPixmap("{0}stage_color_white.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    grey.addPixmap(QtGui.QPixmap("{0}stage_color_grey.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    blue.addPixmap(QtGui.QPixmap("{0}stage_color_blue.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)

    # add both qicons and fbcolor values to a dictionary
    colorDict = {
            "Red":(red, (1, 0, 0)),
            "MintGreen":(mintGreen, (0.5, 1, 0.5)),
            "CarolinaBlue":(carolinaBlue, (0, 0.75, 1)),
            "Magenta":(magenta, (1, 0, 0.8)),
            "Pink":(pink, (1, 0.5, 0.75)),
            "Green":(green, (0, 0.4, 0)),
            "Purple":(purple, (0.65, 0.15, 1)),
            "White":(white, (1, 1, 0.8)),
            "Grey":(grey, (0.5, 0.5, 0.5)),
            "Blue":(blue, (0, 0.3, 1)),
            }


@memorize.memoized
def getAudioCuePath():
    """
    Get the audio cue local path on disk

    Raises:
        ValueError: If unable to find and sync the file.

    Returns:
        str
    """
    if PrevizFolder.hasVirutalProductionRoot() is False:
        return None

    adrCue = "/".join([PrevizFolder.PERFORCE_ROOT, "Global", "Audio", "AdrCue.wav"])
    Perforce.Sync(adrCue)
    fileInfo = Perforce.GetFileState(adrCue)
    localPath = fileInfo.get_ClientFilename()
    if os.path.exists(localPath):
        return localPath
    raise ValueError("Unable to sync audio cue at '{}'".format(adrCue))


def stagesList():
    '''
    generates a list of stages the location's root directory contains

    Returns:
    list((string, string)): base name with no file ext and the full p4 path to load in
    '''
    assetList = []
    for filePath in PrevizFolder().GetStagesForLocation():
        fileName, _ = os.path.splitext(os.path.basename(filePath))
        assetList.append((fileName, filePath))
    return assetList


def buildAssetsList():
    """
    generates a list of build assets the location's root directory contains

    Returns:
    list((string, string)): base name with no file ext and the full p4 path to load in
    """
    if PrevizFolder.hasVirutalProductionRoot() is False:
        return []

    p4Path = "/".join([PrevizFolder.PERFORCE_ROOT, "Global", "PreBuildAssets", "..."])
    assetList = []
    for p4Record in Perforce.Files([p4Path]):
        p4FilePath = p4Record['depotFile']
        fileName, _ = os.path.splitext(os.path.basename(p4FilePath))
        assetList.append((fileName, p4FilePath))
    return assetList


def blockingModelsList():
    """
    generates a list of blocking assets the location's root directory contains

    Returns:
    list((string, string)): base name with no file ext and the full p4 path to load in
    """
    if PrevizFolder.hasVirutalProductionRoot() is False:
        return []

    p4Path = "/".join([PrevizFolder.PERFORCE_ROOT, "Global", "BlockingModels", "..."])
    assetList = []
    for p4Record in Perforce.Files([p4Path]):
        p4FilePath = p4Record['depotFile']
        fileName, _ = os.path.splitext(os.path.basename(p4FilePath))
        assetList.append((fileName, p4FilePath))
    return assetList


def slatePaths():
    """
    Get a list of string p4 path to the slates

    Returns:
        list of strings for the perfoce path to the slate objects
    """
    return ["/".join([PrevizFolder.PERFORCE_ROOT, "Global", "Slate", "SlateClapper.fbx"]),
            "/".join([PrevizFolder.PERFORCE_ROOT, "Global", "Slate", "SlateActionClock.fbx"])]


class GSModelTypes(object):
    """
    class for defining what type of gs asset an object is
    """
    Character = 'Character'
    Prop = 'Prop'
    Slate = 'Slate'


class PropModelDataIndex(object):
    """
    User roles for the PropToyModelItem.
    """
    selfReturn = QtCore.Qt.UserRole
    folderFileRole = QtCore.Qt.UserRole + 1


class P4DescriptionTokens(object):
    """
    P4 description tokens: hashtags, etc.
    """
    DURINGSHOOTWIDGET_SAVE_CHANGELIST_PREFIX = "Base Previz - "
    POST_PROCESS_PREVIZ_SAVE = "#PostProcessPrevizSave"
