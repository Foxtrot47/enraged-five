import pyfbsdk as mobu

from PySide import QtCore, QtGui

import os

from RS import Globals
from RS.Utils import Scene
from RS.Core.Mocap import Clapper
from RS.Core.Face import FacewareLive
from RS.Utils.Scene import AnimationNode, Component
from RS.Core.Mocap import MocapCommands
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.MocapSlate import MocapSlate
from RS.Core.ReferenceSystem.Types.MocapBasePreviz import MocapBasePreviz
from RS.Core.Camera import CameraLocker, CamUtils


class LayoutWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(LayoutWidget, self).__init__(parent=parent)
        self._takeList = []
        self._checkBoxList = []
        self._scrollAreaWidgetContents = None
        self.setupUi()
        self._manager = Manager()

    def _getTakeList(self):
        self._takeList = Globals.Takes

    def _populateScrollAreaContents(self):
        self._checkBoxList = []
        counter = 5
        for take in self._takeList:
            takeCheckBox = QtGui.QCheckBox(self._scrollAreaWidgetContents)
            takeCheckBox.setGeometry(QtCore.QRect(5, counter, 350, 12))
            takeCheckBox.setText(take.Name)
            self._checkBoxList.append(takeCheckBox)
            counter = counter + 20

    def SaveSelectedTakes(self):
        # get file path, without file name
        fileFullPath = mobu.FBApplication().FBXFileName
        splitPathName = fileFullPath.split('\\')
        sceneFileName = splitPathName[-1]
        filePathNoName = fileFullPath.replace(sceneFileName, '')

        # create a list for selected takes and a list for takes to delete
        selectedTakeStringList = []
        for checkBox in self._checkBoxList:
            if checkBox.checkState() == QtCore.Qt.CheckState.Checked:
                selectedTakeStringList.append(str(checkBox.text()))

        # get take and audio based off string
        takeIdxList = []
        for takeString in selectedTakeStringList:
            for idx in range(len(Globals.Takes)):
                if Globals.Takes[idx].Name == takeString:
                    takeIdxList.append(idx)
                    break

        # select all models in scene - need to save selected to get the audio that we need
        Scene.DeSelectAll()
        for component in Globals.Components:
            if not component.ClassName() in ['FBAudioClip', 'FBAudioOut', 'FBTake']:
                component.Selected = True

        # set options
        options = mobu.FBFbxOptions(False)

        # go through values and save out any selected takes
        for idx in takeIdxList:
            take = Globals.Takes[idx]
            take.Selected = True
            for index in range(len(Globals.Takes)):
                if index == idx:
                    options.SetTakeSelect(index, True)
                else:
                    options.SetTakeSelect(index, False)
            # get take name
            takeName = take.Name
            # set current take
            mobu.FBSystem().CurrentTake = take
            # select audio
            audioSelected = None
            for audio in Globals.AudioClips:
                audioNameSplit = audio.Name.split('.')
                audioNameNoExtension = audioNameSplit[0]
                if takeName == audioNameNoExtension:
                    audioSelected = audio
                    audioSelected.Selected = True

                    break
            # set options
            options.SetTakeSelect(idx, True)
            options.SetTakeName(idx, takeName)
            options.SaveSelectedModelsOnly = True
            # path to save
            takePath = os.path.join(
                                        filePathNoName,
                                        takeName + '.fbx',
                                    )
            # save
            mobu.FBApplication().FileSave(takePath, options)
            # deselect audio
            if audioSelected:
                audioSelected.Selected = False
            #deselect take
            take.Selected = False

        # deselect everything
        Scene.DeSelectAll()

        # remove audio
        deleteList = []
        for audio in Globals.AudioClips:
            deleteList.append(audio)

        for item in deleteList:
            item.FBDelete()

        # save original file
        options = mobu.FBFbxOptions(False)
        for index in range(len(Globals.Takes)):
                options.SetTakeSelect(index, True)
        mobu.FBApplication().FileSave(fileFullPath, options)

        # uncheck selections
        for checkBox in self._checkBoxList:
            checkBox.setCheckState(QtCore.Qt.CheckState.Unchecked)

    def PopulateBeepsFromSlate(self):
        """
        Get all the bleeps in the slate and populate the combobox with their frame numbers
        """
        slateString = self.slateComboBox.currentText()
        if slateString == None:
            return
        self.frameComboBox.clear()
        clapsDict = Clapper.GetClaps()
        if len(clapsDict) == 0:
            return
        for key, value in clapsDict.iteritems():
            if str(slateString) == key.LongName:
                for clap in value:
                    self.frameComboBox.addItem(str(clap))

    def ZeroToSlate(self):
        # unlock camera properties
        lockManager = CameraLocker.LockManager()
        lockManager.setAllCamLocks(locked=False)

        # time shift
        if self.autoGenerateCheckBox.isChecked() == True:
            frame = self.frameComboBox.currentText()
            if frame != "":
                MocapCommands.ZeroOutScene(int(frame))

        if self.manualGenerateCheckBox.isChecked() == True:
            frame = self.frameSpinBox.value()
            MocapCommands.ZeroOutScene(frame)

        # lock camera properties
        lockManager.setAllCamLocks(locked=True)

    def ZeroToSlateCheckBoxes(self):
        if self.manualGenerateCheckBox.checkState() == QtCore.Qt.CheckState.Checked:
            self.frameSpinBox.setEnabled(True)
            self.frameComboBox.setEnabled(False)
            self.slateComboBox.setEnabled(False)
        else:
            self.frameComboBox.setEnabled(True)
            self.slateComboBox.setEnabled(True)
            self.frameSpinBox.setEnabled(False)

    def populateSlateComboBox(self):
        self.slateComboBox.clear()
        clapsDict = Clapper.GetClaps()
        if len(clapsDict) == 0:
            return
        for key in clapsDict.iterkeys():
            self.slateComboBox.addItem(key.LongName)
        self.PopulateBeepsFromSlate()

    def resetTree(self):
        # scroll area scroll size
        scrollSize = 180
        listLength = len(self._takeList)
        if listLength > 8:
            excessList = listLength - 8
            scrollSize = scrollSize + (excessList * 20)
        # create scroll contents
        if self._scrollAreaWidgetContents:
            self._scrollAreaWidgetContents.close()
            self._scrollAreaWidgetContents = None
        self._scrollAreaWidgetContents = QtGui.QWidget()
        self._scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 580, scrollSize))
        self._scrollArea.setWidget(self._scrollAreaWidgetContents)
        self._populateScrollAreaContents()

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        takesGroupBoxGridLayout = QtGui.QGridLayout()
        zeroGroupBoxGridLayout = QtGui.QGridLayout()

        # get take list
        self._getTakeList()

        # scroll area widgets
        self._scrollArea = QtGui.QScrollArea()
        self._scrollArea.setWidgetResizable(False)

        # create other widgets
        takesGroupBox = QtGui.QGroupBox()
        saveTakeButton = QtGui.QPushButton()
        zeroGroupBox = QtGui.QGroupBox()
        self.autoGenerateCheckBox = QtGui.QCheckBox()
        self.slateComboBox = QtGui.QComboBox()
        self.frameComboBox = QtGui.QComboBox()
        self.manualGenerateCheckBox = QtGui.QCheckBox()
        self.frameSpinBox = QtGui.QSpinBox()
        timeShiftButton = QtGui.QPushButton()
        zeroButtonGroup = QtGui.QButtonGroup(self)

        # other widget properties
        self.resetTree()
        takesGroupBox.setTitle("Take Select")
        zeroGroupBox.setTitle("Zero to Slate")
        saveTakeButton.setText("Save Selected Take(s)")
        self.autoGenerateCheckBox.setText("Auto-generate frame(s)")
        self.manualGenerateCheckBox.setText("Manually set frame")
        timeShiftButton.setText("Timeshift Scene")

        self.frameSpinBox.setMinimum(0)
        self.frameSpinBox.setMaximum(99999.0)
        self.frameSpinBox.setValue(0)
        self.autoGenerateCheckBox.setCheckState(QtCore.Qt.CheckState.Checked)
        self.populateSlateComboBox()
        self.frameSpinBox.setEnabled(False)
        zeroButtonGroup.addButton(self.autoGenerateCheckBox)
        zeroButtonGroup.addButton(self.manualGenerateCheckBox)
        
        # add widgets to groupBox
        takesGroupBoxGridLayout.addWidget(self._scrollArea, 0, 0)
        takesGroupBoxGridLayout.addWidget(saveTakeButton, 1, 0)

        zeroGroupBoxGridLayout.addWidget(self.autoGenerateCheckBox, 0, 0, 1, 2)
        zeroGroupBoxGridLayout.addWidget(self.slateComboBox, 1, 0)
        zeroGroupBoxGridLayout.addWidget(self.frameComboBox, 1, 1)
        zeroGroupBoxGridLayout.addWidget(self.manualGenerateCheckBox, 2, 0, 1, 2)
        zeroGroupBoxGridLayout.addWidget(self.frameSpinBox, 3, 0)
        zeroGroupBoxGridLayout.addWidget(timeShiftButton, 4, 0, 1, 2)

        # set groupBox layout
        takesGroupBox.setLayout(takesGroupBoxGridLayout)
        zeroGroupBox.setLayout(zeroGroupBoxGridLayout)

        # add groupbox to mainlayout
        mainLayout.addWidget(takesGroupBox, 0, 0, 2, 2)
        mainLayout.addWidget(zeroGroupBox, 0, 2, 1, 2)

        # set layout
        self.setLayout(mainLayout)

        # handles for callbacks
        saveTakeButton.released.connect(self.SaveSelectedTakes)
        self.slateComboBox.currentIndexChanged.connect(self.PopulateBeepsFromSlate)
        timeShiftButton.released.connect(self.ZeroToSlate)
        self.manualGenerateCheckBox.stateChanged.connect(self.ZeroToSlateCheckBoxes)
        self.autoGenerateCheckBox.stateChanged.connect(self.ZeroToSlateCheckBoxes)
