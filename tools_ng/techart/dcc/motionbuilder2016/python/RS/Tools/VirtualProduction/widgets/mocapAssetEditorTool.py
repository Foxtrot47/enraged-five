import os
import json

from PySide import QtGui, QtCore

from RS import Perforce
from RS.Tools import UI
from RS.Core.AnimData.Models import stageModel


class MocapAssetEditorTool(UI.QtMainWindowBase):

    _LOCATION_BASED = "locationBased"
    _STAGE_BASED = "stageBased"

    _TOOLS_TO_EXCLUDE = ("stages",)

    def __init__(self, configFile, parent=None):
        """
        Consturctor

        args:
            configFile (str): The config file path to load into the dialog
        """
        super(MocapAssetEditorTool, self).__init__(parent=parent, title="Virtual Production - Asset Editor", dockable=True)
        self.setupUi()
        self._configPath = configFile
        self.loadConfig(configFile)

    def setupUi(self):
        """
        Setup the UI
        """
        #Layout
        mainLayout = QtGui.QVBoxLayout()
        contextStageLayout = QtGui.QGridLayout()
        contextLocationLayout = QtGui.QGridLayout()
        widgetButtonLayout = QtGui.QHBoxLayout()
        itemButtonLayout = QtGui.QHBoxLayout()

        # Widgets
        mainWidget = QtGui.QWidget()
        self._stageToolsWidget = QtGui.QWidget()
        self._locationToolsWidget = QtGui.QWidget()
        self._tabWidget = QtGui.QTabWidget()

        self._toolLocTypeDropdown = QtGui.QComboBox()
        self._toolLocLocationDropdown = QtGui.QComboBox()
        self._toolStgTypeDropdown = QtGui.QComboBox()
        self._toolStgLocationDropdown = QtGui.QComboBox()
        self._toolStgStageDropdown = QtGui.QComboBox()

        self._items = QtGui.QListWidget()
        self._currentPathLabel = QtGui.QLabel("<No Config Loaded>")
        self._addItem = QtGui.QPushButton("Add Asset Path")
        editItem = QtGui.QPushButton("Edit Selected Asset")
        self._duplicateItem = QtGui.QPushButton("Duplicate Selected Asset")
        self._removeItem = QtGui.QPushButton("Remove Seletected Asset")

        revertChangedButton = QtGui.QPushButton("Revert Changes")
        saveChangedButton = QtGui.QPushButton("Save Changes")

        # Models
        self._stageModel = stageModel.StageModel(includeOffsite=True, includeLocalLocation=True)

        # Configure Widgets
        self._currentPathLabel.setAlignment(QtCore.Qt.AlignCenter)
        self._tabWidget.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self._tabWidget.addTab(self._locationToolsWidget, "Location Based")
        self._tabWidget.addTab(self._stageToolsWidget, "Stage Based")
        self._toolLocLocationDropdown.setModel(self._stageModel)
        self._toolStgLocationDropdown.setModel(self._stageModel)
        #self._toolStgStageDropdown.setRootModelIndex(self._stageModel.index(0, 0, QtCore.QModelIndex()))
        self._toolStgStageDropdown.setCurrentIndex(-1)

        # Add Widgets to Layouts
        widgetButtonLayout.addWidget(revertChangedButton)
        widgetButtonLayout.addWidget(saveChangedButton)

        itemButtonLayout.addWidget(self._addItem)
        itemButtonLayout.addWidget(editItem)
        itemButtonLayout.addWidget(self._duplicateItem)
        itemButtonLayout.addWidget(self._removeItem)

        contextLocationLayout.addWidget(QtGui.QLabel("Tool Type"), 0, 0)
        contextLocationLayout.addWidget(self._toolLocTypeDropdown, 1, 0)
        contextLocationLayout.addWidget(QtGui.QLabel("Location"), 0, 1)
        contextLocationLayout.addWidget(self._toolLocLocationDropdown, 1, 1)

        contextStageLayout.addWidget(QtGui.QLabel("Tool Type"), 0, 0)
        contextStageLayout.addWidget(self._toolStgTypeDropdown, 1, 0)
        contextStageLayout.addWidget(QtGui.QLabel("Location"), 0, 1)
        contextStageLayout.addWidget(self._toolStgLocationDropdown, 1, 1)
        contextStageLayout.addWidget(QtGui.QLabel("Stage"), 0, 2)
        contextStageLayout.addWidget(self._toolStgStageDropdown, 1, 2)

        mainLayout.addWidget(self._currentPathLabel)
        mainLayout.addWidget(self._tabWidget)
        mainLayout.addWidget(self._items)
        mainLayout.addLayout(itemButtonLayout)
        mainLayout.addLayout(widgetButtonLayout)

        mainWidget.setLayout(mainLayout)
        self._locationToolsWidget.setLayout(contextLocationLayout)
        self._stageToolsWidget.setLayout(contextStageLayout)
        self.setCentralWidget(mainWidget)

        # Set up Connections
        self._toolLocTypeDropdown.currentIndexChanged.connect(self._handleLocTypeDropDown)#_handleTooleTypeDropDownSelection)
        self._toolLocLocationDropdown.currentIndexChanged.connect(self._handleLocLocationPicked)#_handleTooleTypeDropDownSelection)
        self._toolStgTypeDropdown.currentIndexChanged.connect(self._handleStgTypeDropDown)#_handleTooleStageDropDownSelection)
        self._toolStgLocationDropdown.currentIndexChanged.connect(self._handleStgLocationPicked)
        self._toolStgStageDropdown.currentIndexChanged.connect(self._handleStgStagePicked)

        self._addItem.pressed.connect(self._handleAddItem)
        editItem.pressed.connect(self._handleEditItem)
        self._removeItem.pressed.connect(self._handleRemoveItem)
        self._duplicateItem.pressed.connect(self._handleDuplicateItem)
        revertChangedButton.pressed.connect(self._handleRevertChanged)
        saveChangedButton.pressed.connect(self._handleSaveChanged)
        self._tabWidget.currentChanged.connect(self._handleTabWidgetChange)

    def _handleStgLocationPicked(self, newIndex):
        if newIndex != -1:
            idx = self._stageModel.index(newIndex, 0, QtCore.QModelIndex())
            self._toolStgStageDropdown.setModel(self._stageModel)
            self._toolStgStageDropdown.setRootModelIndex(idx)
            self._toolStgStageDropdown.setCurrentIndex(-1)

    def _handleStgStagePicked(self, newIndex):
        self._populateItems()

    def _handleLocLocationPicked(self, newIndex):
        self._populateItems()

    def _handleTabWidgetChange(self):
        self._populateItems()

    def loadConfig(self, configFile):
        """
        Load a config into the tool

        args:
            configFile (str): The path to the config file to load into
        """
        if not os.path.exists(configFile):
            raise ValueError("Unable to find Virtual Production settings file at '{}'".format(configFile))
        self._configPath = configFile

        with open(configFile) as fileHandle:
            self._settingsDict = json.load(fileHandle)

        self._currentPathLabel.setText(configFile)

        self._toolLocTypeDropdown.clear()
        self._toolStgTypeDropdown.clear()
        self._toolLocLocationDropdown.setCurrentIndex(-1)
        self._toolStgLocationDropdown.setCurrentIndex(-1)
        self._toolStgStageDropdown.setCurrentIndex(-1)
        self._items.clear()

        locBasedToolTypes = self._settingsDict[self._LOCATION_BASED]
        locBasedToolTypes = [name for name in locBasedToolTypes.keys() if name not in self._TOOLS_TO_EXCLUDE]
        locBasedToolTypes.sort()
        self._toolLocTypeDropdown.addItems(locBasedToolTypes)

        stgBasedToolTypes = self._settingsDict[self._STAGE_BASED]
        stgBasedToolTypes = [name for name in stgBasedToolTypes.keys() if name not in self._TOOLS_TO_EXCLUDE]
        stgBasedToolTypes.sort()
        self._toolStgTypeDropdown.addItems(stgBasedToolTypes)

    def _handleLocTypeDropDown(self):
        self._toolLocLocationDropdown.setCurrentIndex(-1)
        self._populateItems()

    def _handleStgTypeDropDown(self):
        self._toolStgLocationDropdown.setCurrentIndex(-1)
        self._toolStgStageDropdown.setCurrentIndex(-1)
        self._populateItems()

    @staticmethod
    def _findInDict(dictToUse, items, defaultValue=None):
        currentLevel = dictToUse
        for item in items:
            res = currentLevel.get(item)
            if res is None:
                return defaultValue
            currentLevel = res
        return currentLevel

    @staticmethod
    def _setInDict(dictToUse, items, newValue):
        currentLevel = dictToUse
        for item in items[:-1]:
            currentLevel = currentLevel.setdefault(item, {})
        currentLevel[items[-1]] = newValue

    def _getCurrentPathParts(self):
        parts = []
        if self._tabWidget.currentWidget() == self._locationToolsWidget:
            parts.append(self._LOCATION_BASED)
            parts.append(self._toolLocTypeDropdown.currentText())
            parts.append(self._toolLocLocationDropdown.currentText())

        else:
            parts.append(self._STAGE_BASED)
            parts.append(self._toolStgTypeDropdown.currentText())
            parts.append(self._toolStgLocationDropdown.currentText())
            parts.append(self._toolStgStageDropdown.currentText())
        return parts

    def _isPathValid(self):
        return all([True if part != "" else None for part in self._getCurrentPathParts()])

    def _populateItems(self):
        parts = self._getCurrentPathParts()

        self._items.clear()
        self._addItem.setEnabled(True)
        self._duplicateItem.setEnabled(True)
        self._removeItem.setEnabled(True)

        res = self._findInDict(self._settingsDict, parts, None)
        if res == None:
            return

        allItems = res
        allItems.sort()
        self._items.addItems(allItems)

    def _handleAddItem(self):
        """
        Internal method

        Handle the Add button being pressed
        """
        if self._isPathValid() is False:
            return

        parts = self._getCurrentPathParts()
        currentItems = self._findInDict(self._settingsDict, parts, [])
        txt = ""
        while True:
            txt, ok = QtGui.QInputDialog.getText(self, "New {}".format("->".join(parts)), "P4 Path for asset:", text=txt)
            if ok is False:
                return

            # Check for Validation
            if not txt.endswith(".fbx"):
                QtGui.QMessageBox.warning(self, "Bad Path", "The file path '{}' is not valid.\nNeeds to end in .fbx".format(txt))
                continue
            if not txt.startswith("//"):
                QtGui.QMessageBox.warning(self, "Bad Path", "The file path '{}' is not valid.\nNeeds to start with //".format(txt))
                continue
            if txt in currentItems:
                QtGui.QMessageBox.warning(self, "Bad Path", "The file path '{}' is already in use for this tool and stage".format(txt))
                continue

            break

        currentItems.append(txt)
        self._setInDict(self._settingsDict, parts, currentItems)
        self._items.addItem(txt)

    def _handleRemoveItem(self):
        """
        Internal method

        Handle the Remove button being pressed
        """
        if self._isPathValid() is False:
            return

        parts = self._getCurrentPathParts()

        for item in self._items.selectedItems():
            currentItems = self._findInDict(self._settingsDict, parts, [])
            currentItems.remove(item.text())
            self._setInDict(self._settingsDict, parts, currentItems)

            self._items.takeItem(self._items.row(item))
            del item

    def _handleEditItem(self):
        """
        Internal method

        Handle the Edit button being pressed
        """
        if self._isPathValid() is False:
            return

        parts = self._getCurrentPathParts()
        currentItems = self._findInDict(self._settingsDict, parts, [])

        for item in self._items.selectedItems():
            itemText = item.text()
            while True:
                txt, ok = QtGui.QInputDialog.getText(self, "Update {}".format("->".join(parts)), "P4 Path for asset:", text=itemText)
                if ok is False:
                    return

                # Check for Validation
                if not txt.endswith(".fbx"):
                    QtGui.QMessageBox.warning(self, "Bad Path", "The file path '{}' is not valid.\nNeeds to end in .fbx".format(txt))
                    continue
                if not txt.startswith("//"):
                    QtGui.QMessageBox.warning(self, "Bad Path", "The file path '{}' is not valid.\nNeeds to start with //".format(txt))
                    continue
                if txt in currentItems:
                    QtGui.QMessageBox.warning(self, "Bad Path", "The file path '{}' is already in use for this tool and stage".format(txt))
                    continue
                break
            item.setText(txt)
            currentItems.remove(itemText)
            currentItems.append(txt)
            self._setInDict(self._settingsDict, parts, currentItems)

    def _handleDuplicateItem(self):
        """
        Internal method

        Handle the Duplicate button being pressed
        """
        if self._isPathValid() is False:
            return

        parts = self._getCurrentPathParts()
        currentItems = self._findInDict(self._settingsDict, parts, [])

        for item in self._items.selectedItems():
            itemText = item.text()
            curNum = 0
            dupName = str(itemText)

            while True:
                dupName = "{}^{}".format(itemText, curNum)
                if dupName not in currentItems:
                    break
                curNum += 1
            currentItems.append(dupName)
            self._items.addItem(dupName)
        self._setInDict(self._settingsDict, parts, currentItems)

    def _handleRevertChanged(self):
        """
        Internal method

        Handle the Revert button being pressed
        """
        result = QtGui.QMessageBox.question(
                self,
                "Revert Changes",
                "Are you sure you want to revert any unsaved changes?",
                QtGui.QMessageBox.Yes,
                QtGui.QMessageBox.No)
        if result == QtGui.QMessageBox.Yes:
            self.loadConfig(self._configPath)

    def _handleSaveChanged(self):
        """
        Internal method

        Handle the Save button being pressed
        """
        configFile = self._configPath
        if not os.access(configFile, os.W_OK):
            result = QtGui.QMessageBox.question(
                self,
                "Checkout File",
                "The config file is currently not checked out, would you like to check it out to the Default Changelist?",
                QtGui.QMessageBox.Yes,
                QtGui.QMessageBox.No)

            if result == QtGui.QMessageBox.No:
                QtGui.QMessageBox.information(self, "Data NOT Saved", "Unable to save data")
                return

            # P4 Checkout
            Perforce.AddOrEditFiles(configFile)

        with open(configFile, "w") as handler:
            json.dump(self._settingsDict, handler, sort_keys=True, indent=4)
        QtGui.QMessageBox.information(self, "Data Saved", "Previs Asset config has been saved to:\n'{}'".format(configFile))
