from PySide import QtCore

import pyfbsdk as mobu

from RS import Globals
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types import MocapGsSkeleton
from RS.Utils import Namespace


REFERENCE_ROLE = QtCore.Qt.UserRole + 1


class ColumnRoles(object):
    """
    Enum for the model columns  
    """
    NAME = "Reference Name"
    HANDLER_TEXT = "Handler Text"


class GsHandlerModelItem(baseModelItem.BaseModelItem):
    """
    Model Item Base 
    """
    
    HANDLE_NAME = "PREVIZ_NAME_HANDLER"
    
    def __init__(self, referenceItem, parent=None):
        """
        Constructor
        
        args:
            refItem (RS.Core.ReferenceSystem.Types.Base): The reference system item
        """
        super(GsHandlerModelItem, self).__init__(parent=parent)
        self._referenceItem = referenceItem
        self._skelRoot = mobu.FBFindModelByLabelName("{0}:SKEL_ROOT".format(self._referenceItem.Namespace))
        self._name = "{}:{}".format(referenceItem.Namespace, referenceItem.Name) 

        currentHandle = self._getCurrentHandle()
        if currentHandle is not None:
            labelProp = currentHandle.PropertyList.Find("Label")
            self._handlerText = labelProp.Data
        else:
            self._handlerText = ""

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.model().getHeadings()[index.column()]

        if role == QtCore.Qt.DisplayRole:
            if column == ColumnRoles.NAME:
                return self._name
            elif column == ColumnRoles.HANDLER_TEXT:
                return self._handlerText

        elif role == QtCore.Qt.EditRole:
            if column == ColumnRoles.HANDLER_TEXT:
                return self._handlerText

        elif role == QtCore.Qt.UserRole:
            return self
        
        elif role == REFERENCE_ROLE:
            return self._referenceItem
 
    def _getCurrentHandle(self):
        refNamespace = Namespace.GetFBNamespace(self._referenceItem.Namespace)
        return Namespace.FindComponentInNamespace(refNamespace, self.HANDLE_NAME, objectType=mobu.FBHandle)
 
    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """
        ReImplemented from Qt
        """
        column = index.model().getHeadings()[index.column()]
        value = str(value)
        if role == QtCore.Qt.EditRole:
            if column == ColumnRoles.HANDLER_TEXT:
                # Get current handlers
                currentHandle = self._getCurrentHandle()
                
                if value == "":
                    # Delete it if it exists
                    if currentHandle is not None:
                        currentHandle.FBDelete()
                        self._handlerText = ""
                        return True
                else:
                    # Create one if not present
                    if currentHandle is None:
                        currentHandle = mobu.FBHandle("{}:{}".format(self._referenceItem.Namespace, self.HANDLE_NAME))
                        currentHandle.PropertyList.Find("Auto-Size to Viewer").Data = True
                        currentHandle.PropertyList.Find("Visibility Mode").Data = 1
                        currentHandle.PropertyList.Find("Translation Offset").Data = mobu.FBVector3d(0, 100, 0)
                        currentHandle.PropertyList.Find("2D Display Color").Data = self._skelRoot.PropertyList.Find("actor color").Data#mobu.FBColor(0.80, 0.10, 0.10)
                        
                        currentHandle.Follow.append(self._skelRoot)
                        currentHandle.Manipulate.append(self._skelRoot)

                    # Edit the text
                    labelProp = currentHandle.PropertyList.Find("Label")
                    if labelProp.SetString(value) is True:
                        self._handlerText = value
                        return True
        return False

    def rowCount(self, parent=None):
        """
        ReImplemented from Qt
        """
        return 0

    def hasChildren(self, parent=None):
        """
        ReImplemented from Qt
        """
        return False

    def flags(self, index):
        """
        Flags added to determine column properties.

        Args:
            index (QtCore.QModelIndex): Index of the item in the model.
        """
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        column = index.model().getHeadings()[index.column()]
        flags = super(GsHandlerModelItem, self).flags(index)
        if column in [ColumnRoles.HANDLER_TEXT]:
            flags |= QtCore.Qt.ItemIsEditable
        return flags


class GsHandlerModel(baseModel.BaseModel):
    """
    Base Model for the Giant Devices
    """
    def __init__(self, parent=None):
        """
        Constructor
        """
        self._manager = Manager.Manager()
        super(GsHandlerModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Get the headings of model
        
        return:
            list of string column names
        """
        return [ColumnRoles.NAME, ColumnRoles.HANDLER_TEXT]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        ReImplemented from Qt
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data
        """
        for refItem in [refItem for refItem in self._manager.GetReferenceListAll() if isinstance(refItem, MocapGsSkeleton.MocapGsSkeleton)]:
            parent.appendChild(GsHandlerModelItem(refItem, parent))
