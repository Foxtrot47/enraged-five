import os
import re
import glob

import fbx
import FbxCommon

import pyfbsdk as mobu

from PySide import QtGui, QtCore

from RS import Globals, ProjectData
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Tools.CameraToolBox.PyCoreQt.Delegates import comboboxDelegate
from RS.Core.Mocap import MocapCommands, realtimeConnection
from RS.Tools.VirtualProduction import const
from RS.Tools.VirtualProduction.models import gsSkelMatchModelTypes
from RS.Utils import Path, Scene, ContextManagers
from RS.Utils.Scene import Component


class GSSkeletonInformation:
    '''
    Class to hold a list of properties relating to a character.
    '''
    def __init__(self,
                 CharacterNamespace,
                 CharacterNode,
                 GSSkeletonPath,
                 ActorString,
                 CheckedState,
                 UserColor):

        self.CharacterNamespace = CharacterNamespace
        self.CharacterNode = CharacterNode
        self.GSSkeletonPath = GSSkeletonPath
        self.ActorString = ActorString
        self.CheckedState = CheckedState
        self.UserColor = UserColor


class GSSkeletonMatchWidgets(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    # signal to send to the ref editor
    gsMergeCompletedSelected = QtCore.Signal()
    def __init__(self, parent=None):
        super(GSSkeletonMatchWidgets, self).__init__(parent=parent)
        self.setupUi()

    def GetCharacterInformationList(self):
        '''
        Generates the Character info List, using the 'GSSkeletonInformation' class

        returns: List
        '''
        CharacterInformationList = []
        # generate character information list
        for child in self._mainModel.rootItem.children():
            # get character namespace
            characterNamespace = child.GetCharacterNamespace()
            # get character node
            characterNode = child.GetCharacterNode()
            # get gs skeleton path
            gsSkelPath = child.GetGSSkelPath()
            # get user actor name
            userActorString = child.GetUserActorString()
            # get checked state
            checkedState = child.GetCheckedState()
            # reset checked state
            if checkedState:
                child.SetCheckedState()
            # get user color
            userColor = child.GetUserColor()

            # build list
            CharacterInformationList.append(GSSkeletonInformation(
                                                                  characterNamespace,
                                                                  characterNode,
                                                                  gsSkelPath,
                                                                  userActorString,
                                                                  checkedState,
                                                                  userColor
                                                                 ))
        return CharacterInformationList

    def _getGSSkelDict(self):
        '''
        Returns:
        a dict of gsSkel Names and paths
        '''
        gsSkelDirDict = {}
        # paths to gs skel files
        gsPath = '{0}\\*.fbx'.format(ProjectData.data.GetGsSkeletonPath())

        # using local files for this. Having problems accessing depot files with the mocap server.
        tempFileList = glob.glob(gsPath)
        for filePath in tempFileList:
            #if '_gs' in filePath: WAITING ON BUG 2329722
            # make a list of all gs skel path names
            itemSplitName = str(filePath).split('\\')
            fbxNameSplit = itemSplitName[-1].split('.')
            gsSkelString = fbxNameSplit[0]
            # append gsSkeleton lists
            if gsSkelString.lower().endswith('_gs') or re.match("^M[0-9]+", gsSkelString):
                gsSkelDirDict[gsSkelString] = filePath
        return gsSkelDirDict

    def _populateGSSkelCombo(self):
        '''
        populate the combobox with gs skel names for user to select from
        '''
        self._actorNameComboBox.clear()
        self._actorNameComboBox.setEnabled(True)
        gsSkelDirDict = self._getGSSkelDict()
        for key in sorted(gsSkelDirDict):
            self._actorNameComboBox.addItem(key, gsSkelDirDict[key])
        
        if len(gsSkelDirDict) == 0:
            self._actorNameComboBox.addItem("No Skels found in {}".format(ProjectData.data.GetGsSkeletonPath()))
            self._actorNameComboBox.setEnabled(False)

    def _populateActorNames(self):
        """
        Populate the actor names
        """
        talents = []
        take = realtimeConnection.RealtimeConnection().currentTrial()
        if take is not None:
            for talent in take.session().getTalentForSession():
                characterFirstName = talent.firstName().replace(" ", "").lower()
                characterLastName = talent.lastName().replace(" ", "").lower()
                talents.append("{}{}".format(characterFirstName.capitalize(), characterLastName.capitalize()))

        self._actorName.clear()
        self._actorName.addItems(talents)
        self._actorName.setCurrentIndex(-1)
        self._actorComboBoxDelegate.setItems(talents)

    def _populateGSSkelColorCombo(self):
        '''
        populate the combobox with skel colours for user to select from
        '''
        for key, value in const.Icons().colorDict.iteritems():
            self._colorComboBox.addItem(value[0], key, value[1])

    def _getCurrentColorString(self):
        '''
        return current user selection
        '''
        # get current index/constraint
        return self._colorComboBox.currentText()

    def _getCurrentColorItemData(self):
        '''
        return current user selection
        '''
        # get current index/constraint
        currentComboBoxIndex = self._colorComboBox.currentIndex()
        # using the user data, we can get the related constraint object to the current index
        colorList = self._colorComboBox.itemData(currentComboBoxIndex)
        return colorList

    def _getCurrentActorName(self):
        '''
        return current user selection
        '''
        return self._actorName.currentText()

    def _getCurrentGSPathSelection(self):
        '''
        return current user selection for 'self._actorNameComboBox'
        '''
        # using the user data, we can get the related constraint object to the current index
        currentComboBoxIndex = self._actorNameComboBox.currentIndex()
        return self._actorNameComboBox.itemData(currentComboBoxIndex)

    def _resetUserOptions(self):
        '''
        reset all of the user options to default state
        '''
        self._actorName.clear()
        self._colorComboBox.setCurrentIndex(0)
        self._actorNameComboBox.setCurrentIndex(0)

    def _getCharacterComponent(self, namespace):
        '''
        Gets the mobu.FBCharacter component for the reference - should be 1.
        '''
        characterComponents = [component for component in Globals.Characters
                               if Component.GetNamespace(component) == namespace]
        if len(characterComponents) > 1:
            return
        elif len(characterComponents) < 1:
            return
        else:
            return characterComponents[0]

    def _handleMergeGSSkeletons(self):
        '''
        Handle for connecting with pushButton.

        Merges in a gs Skeleton and sets the appropriate properties based of what the user has
        selected - is not displayed in the treeview, as it does not get a matching character.
        '''
        manager = Manager()
        # get user selections
        gsPath = self._getCurrentGSPathSelection()
        color = self._getCurrentColorItemData()
        colorString = self._getCurrentColorString()
        actorString = self._getCurrentActorName()

        if gsPath is None:
            QtGui.QMessageBox.warning(self,
                                      "Warning",
                                      "No GS Path selected. Aborting",
                                      QtGui.QMessageBox.Ok)

        # give actor string a default name if it is blank
        if actorString == '':
            actorString = 'defaultName'

        # get new namespace
        namespace = Path.GetBaseNameNoExtension(gsPath)
        # get original gs name, from fbx file via sdk, if it exists
        sdkManager, scene = FbxCommon.InitializeSdkObjects()
        FbxCommon.LoadScene(sdkManager, scene, gsPath)
        fbxCharacter = scene.GetCharacter(0)
        if fbxCharacter is not None:
            link = fbx.FbxCharacterLink()
            fbxCharacter.GetCharacterLink(fbxCharacter.eHips, link)
            fullName = link.mNode.GetName()
            parts = re.findall("^(?:gs:|^)([a-zA-Z0-9^_]+):", fullName)
            if len(parts) > 0:
                namespace = parts[0]
        
        # new namespace
        newNamespace = '{}:{}:{}'.format(actorString, "gs", namespace)
        sdkManager.Destroy()

        # create reference
        referenceList = manager.CreateReferences(gsPath)
        # rename with actor:filename namespace
        manager.ChangeNamespace(referenceList[0], newNamespace)

        # need to update the newNamespace variable in case the anme received an iteration due to the
        # passed namespace already existing
        newNamespace = referenceList[0].Namespace

        # get gsskel's hips, then find mesh
        gsCharacter = self._getCharacterComponent(newNamespace)
        if gsCharacter is None:
            print "No GS Character found!"
            return

        hierarchyList = []
        meshNode = None

        gsSkelHips = gsCharacter.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)

        gsSkelParent = Scene.GetParent(gsSkelHips)

        Scene.GetChildren(gsSkelParent, hierarchyList, "", True)
        for node in hierarchyList:
            if '_mesh' in node.Name.lower():
                meshNode = node
                break

        if meshNode is None:
            return

        # detach old materials first
        for oldMaterial in meshNode.Materials:
            oldMaterial.FBDelete()

        # add colored material based on usercolor
        material = mobu.FBMaterial('{0}:Material'.format(newNamespace))
        material.Diffuse = mobu.FBColor(color[0], color[1], color[2])
        meshNode.Materials.append(material)

        # assign custom properties to gsSkel
        customProperty = gsSkelHips.PropertyCreate(
                                                  'character match namespace',
                                                   mobu.FBPropertyType.kFBPT_charptr,
                                                   "",
                                                   False,
                                                   True,
                                                   None
                                                    )
        customProperty.Data = 'None'
        customProperty.SetLocked(True)
        colorProperty = gsSkelHips.PropertyCreate(
                                                    'actor color',
                                                     mobu.FBPropertyType.kFBPT_ColorRGB,
                                                     "",
                                                     False,
                                                     True,
                                                     None
                                                     )
        colorProperty.Data = mobu.FBColor(color[0], color[1], color[2])
        colorProperty.SetLocked(True)
        colorStrProperty = gsSkelHips.PropertyCreate(
                                                        'actor color string',
                                                        mobu.FBPropertyType.kFBPT_charptr,
                                                        "",
                                                        False,
                                                        True,
                                                        None
                                                        )
        colorStrProperty.Data = str(colorString)
        colorStrProperty.SetLocked(True)

        # reset user options
        self._resetUserOptions()

        # send a signal when compelted - for use with the ref editor
        self.gsMergeCompletedSelected.emit()

    def _handleMergeMatchingGSSkeletons(self):
        '''
        Handle for connecting with pushButton.

        Merges in the Character's matching gs Skeleton and sets the appropriate properties based of
        what the user has selected. Details are displayed in the treeview.
        '''
        # get list of character info
        CharacterInformationList = self.GetCharacterInformationList()

        if not len(CharacterInformationList) > 0:
            return

        charGSDict = {}
        for info in CharacterInformationList:
            if info.CheckedState:
                # check path exists
                if not os.path.exists(info.GSSkeletonPath):
                    return
                # gs skel path base name
                namespace = Path.GetBaseNameNoExtension(info.GSSkeletonPath)

                # get original gs name, from fbx file via sdk, if it exists
                sdkManager, scene = FbxCommon.InitializeSdkObjects()
                FbxCommon.LoadScene(sdkManager, scene, info.GSSkeletonPath)
                fbxCharacter = scene.GetCharacter(0)
                if fbxCharacter is not None:
                    link = fbx.FbxCharacterLink()
                    fbxCharacter.GetCharacterLink(fbxCharacter.eHips, link)
                    fullName = link.mNode.GetName()
                    parts = re.findall("^(?:gs:|^)([a-zA-Z0-9^_]+):", fullName)
                    if len(parts) > 0:
                        namespace = parts[0]
                # new namespace
                newNamespace = '{}:{}:{}'.format(info.ActorString, "gs", namespace)
                sdkManager.Destroy()

                # create reference
                manager = Manager()
                referenceList = manager.CreateReferences(info.GSSkeletonPath)
                # rename with actor:character:filename namespace
                manager.ChangeNamespace(referenceList[0], newNamespace)

                # get gs skel character node
                gsCharacter = None
                for character in Globals.Characters:
                    if character.LongName.startswith('{0}:'.format(newNamespace)):
                        gsCharacter = character
                        break

                if gsCharacter is None:
                    return

                # get gsskel's hips, then find mesh
                hierarchyList = []
                meshNode = None
                gsSkelHips = gsCharacter.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
                Scene.GetChildren(gsSkelHips, hierarchyList, "", True)
                for node in hierarchyList:
                    if '_mesh' in node.Name.lower():
                        meshNode = node
                        break

                if meshNode is None:
                    QtGui.QMessageBox.warning(None,
                                              "Warning",
                                              "Unable to find '_mesh' node./n/nAll gsSkel source files should contain this node.\n\nUnable to continue with material setup of this gsSkeleton, please check the source file has the correct setup.",
                                              QtGui.QMessageBox.Ok)
                    return

                for key, value in const.Icons.colorDict.iteritems():
                    # remove existing materials on mesh
                    for material in meshNode.Materials:
                        material.FBDelete()

                    if info.UserColor == key:
                        # add colored material based on usercolor
                        material = mobu.FBMaterial('{0}:Material'.format(newNamespace))
                        material.Diffuse = mobu.FBColor(value[1])
                        meshNode.Materials.append(material)
                        # get character hips and assign custom properties with user color and
                        # matching gs skel
                        characterHips = info.CharacterNode.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
                        if characterHips:
                            colorProperty = characterHips.PropertyCreate(
                                                                        'actor color',
                                                                         mobu.FBPropertyType.kFBPT_ColorRGB,
                                                                         "",
                                                                         False,
                                                                         True,
                                                                         None
                                                                         )
                            colorProperty.Data = mobu.FBColor(value[1])
                            colorProperty.SetLocked(True)

                            colorStrProperty = characterHips.PropertyCreate(
                                                                            'actor color string',
                                                                            mobu.FBPropertyType.kFBPT_charptr,
                                                                            "",
                                                                            False,
                                                                            True,
                                                                            None
                                                                            )
                            colorStrProperty.Data = info.UserColor
                            colorStrProperty.SetLocked(True)

                            gsMatchProperty = characterHips.PropertyCreate(
                                                                          'gs match namespace',
                                                                           mobu.FBPropertyType.kFBPT_charptr,
                                                                           "",
                                                                           False,
                                                                           True,
                                                                           None
                                                                           )
                            gsMatchProperty.Data = '{0}:{1}'.format(
                                                                    info.ActorString,
                                                                    newNamespace
                                                                    )
                            gsMatchProperty.SetLocked(True)

                            # assign custom properties to gsSkel with matching character namespace
                            customProperty = gsSkelHips.PropertyCreate(
                                                                      'character match namespace',
                                                                       mobu.FBPropertyType.kFBPT_charptr,
                                                                       "",
                                                                       False,
                                                                       True,
                                                                       None
                                                                        )
                            customProperty.Data = '{0}:{1}'.format(info.ActorString, newNamespace)
                            customProperty.SetLocked(True)

                            colorProperty = gsSkelHips.PropertyCreate(
                                                                        'actor color',
                                                                         mobu.FBPropertyType.kFBPT_ColorRGB,
                                                                         "",
                                                                         False,
                                                                         True,
                                                                         None
                                                                         )
                            colorProperty.Data = mobu.FBColor(value[1])
                            colorProperty.SetLocked(True)

                            colorStrProperty = gsSkelHips.PropertyCreate(
                                                                            'actor color string',
                                                                            mobu.FBPropertyType.kFBPT_charptr,
                                                                            "",
                                                                            False,
                                                                            True,
                                                                            None
                                                                            )
                            colorStrProperty.Data = info.UserColor
                            colorStrProperty.SetLocked(True)
                            break

                #character to character with character node
                info.CharacterNode.InputCharacter = gsCharacter
                info.CharacterNode.InputType = mobu.FBCharacterInputType.kFBCharacterInputCharacter

                info.CharacterNode.ActiveInput = True

                # snap to active stage
                activeStage = MocapCommands.FindActiveStage()
                gsSkelParent = Scene.GetParent(gsSkelHips)
                if activeStage:
                    Scene.AlignTranslation(gsSkelParent, activeStage, True, True, True)

            # deselected any row which may be selected
            self.treeView.clearSelection()

            # send a signal when compelted - for use with the ref editor
            self.gsMergeCompletedSelected.emit()


    def showEvent(self, event):
        self.resetTree()
        super(GSSkeletonMatchWidgets, self).showEvent(event)

    def resetTree(self):
        self._populateGSSkelCombo()
        self._mainModel.reset()
        
        # get actor names
        self._populateActorNames()
    
    def setupUi(self):
        '''
        Sets up the widgets and their properties
        '''
        # create layout
        mainLayout = QtGui.QGridLayout()
        tableLayout = QtGui.QVBoxLayout()
        gsSkelLayout = QtGui.QGridLayout()

        self.setWindowTitle("GS Skeleton Match")
        # model and tree widget
        self._mainModel =  gsSkelMatchModelTypes.GSSkelInfoModel()
        self.treeView = QtGui.QTreeView()
        self.treeView.setModel(self._mainModel)

        # other widgets
        pushButton = QtGui.QPushButton("Merge Selected GS Skeletons")
        self._actorName = QtGui.QComboBox()
        self._colorComboBox = QtGui.QComboBox()
        self._actorNameComboBox = QtGui.QComboBox()
        gsSkelButton = QtGui.QPushButton("Add a new GS Skel")
        tableGroupBox = QtGui.QGroupBox("Add a new GS Skel, with a Character match:")
        gsSkelGroupBox = QtGui.QGroupBox("Add a new GS Skel, without a Character match:")

        # other widgets properties
        self._actorName.setEditable(True)
        self._actorName.lineEdit().setPlaceholderText("enter actor name")
        self._actorName.setMaximumWidth(300)
        self._colorComboBox.setMaximumWidth(300)
        gsSkelButton.setMaximumWidth(300)
        self._populateGSSkelColorCombo()
        self._populateGSSkelCombo()

        # tree properties
        self.treeView.setAlternatingRowColors(True)
        self.treeView.setStyleSheet("QTreeView {\nalternate-background-color: rgb(56, 56, 56); \nbackground-color: rgb(43, 43, 43);\n} \n")
        self.treeView.setSortingEnabled(True)
        self.treeView.header().setSortIndicator(0, QtCore.Qt.AscendingOrder)
        self.treeView.header().resizeSection(0, 200)
        self.treeView.header().resizeSection(1, 200)
        self.treeView.header().resizeSection(2, 190)

        # tree delegates
        self._comboBoxDelegate = comboboxDelegate.ComboBoxDelegate(self._mainModel.GetGsSkelNameList())
        self._colorComboBoxDelegate = comboboxDelegate.ComboBoxDelegateWithIcons(const.Icons.colorDict)
        self._actorComboBoxDelegate = comboboxDelegate.EditableComboBoxDelegate([], "enter actor name")
        
        self.treeView.setItemDelegateForColumn(1, self._comboBoxDelegate)
        self.treeView.setItemDelegateForColumn(2, self._actorComboBoxDelegate)
        self.treeView.setItemDelegateForColumn(3, self._colorComboBoxDelegate)

        # add widgets to layout
        tableLayout.addWidget(self.treeView, 0, 0)
        tableLayout.addWidget(pushButton, 1, 0)
        tableGroupBox.setLayout(tableLayout)
        gsSkelLayout.addWidget(self._actorNameComboBox, 0, 0)
        gsSkelLayout.addWidget(self._actorName, 0, 1)
        gsSkelLayout.addWidget(self._colorComboBox, 0, 2)
        gsSkelLayout.addWidget(gsSkelButton, 0, 3)
        gsSkelGroupBox.setLayout(gsSkelLayout)
        mainLayout.addWidget(tableGroupBox)
        mainLayout.addWidget(gsSkelGroupBox)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        pushButton.released.connect(self._handleMergeMatchingGSSkeletons)
        gsSkelButton.released.connect(self._handleMergeGSSkeletons)
