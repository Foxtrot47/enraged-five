import re

import pyfbsdk as mobu

from PySide import QtCore, QtGui

from RS import Globals, Perforce
from RS.Utils import Namespace, Scene
from RS.Core.Mocap import MocapCommands
from RS.Utils.Scene import Component, Constraint
from RS.Tools.VirtualProduction import const
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Tools.VirtualProduction.models import propToyModelTypes
from RS.Core.ReferenceSystem.Types.MocapPropToy import MocapPropToy


class BottomUpFilterProxyModel(QtGui.QSortFilterProxyModel):
    """
    Custom QSortFilterProxyModel so that when a match is found it also finds the parents
    """
    def filterAcceptsRow(self, sourceRow, sourceParent):
        """
        """
        if not self.filterRegExp().isEmpty():
            sourceIndex = self.sourceModel().index(sourceRow, self.filterKeyColumn(), sourceParent)
            if sourceIndex.isValid():
                for index in xrange(self.sourceModel().rowCount(sourceIndex)):
                    if self.filterAcceptsRow(index, sourceIndex):
                        return True
                currentString = str(self.sourceModel().data(sourceIndex, self.filterRole()))

                return self.filterRegExp().pattern().lower() in currentString.lower()

        return super(BottomUpFilterProxyModel, self).filterAcceptsRow(sourceRow, sourceParent)


class PropToyWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PropToyWidget, self).__init__(parent=parent)
        self.currentStage = None
        self.manager = Manager()
        self.setupUi()

    def setCurrentStage(self, stage):
        self.currentStage = stage

    def populateTreeView(self):
        # reset view
        self.propListTreeView.reset()
        # add model to tree widget
        self._mainModel =  propToyModelTypes.PropToyModel()
        self._proxyModel = BottomUpFilterProxyModel()
        self._proxyModel.setSourceModel(self._mainModel)
        self._proxyModel.sort(0, QtCore.Qt.AscendingOrder)
        #proxyself._proxyModelModel->sort(2, Qt::AscendingOrder);
        self.propListTreeView.setModel(self._proxyModel)

        self.propListTreeView.expandAll()

    def populateScenePropToyComboBoxList(self):
        self.availablePropToysComboBox.clear()
        proptoyList = self._getScenePropToys()
        self.availablePropToysComboBox.addItems(proptoyList)

    def populateConstraintTypeComboBoxList(self):
        self.constraintComboBox.clear()
        constraintList = ["Rotation", "Parent"]
        self.constraintComboBox.addItems(constraintList)

    def populateDriverTreeList(self):
        self.driversTree.clear()

        referenceList = self.manager.GetReferenceListAll()

        nonPropToyAssetDict = {}

        for reference in referenceList:
            if type(reference) == MocapPropToy:
                continue

            namespace = Globals.Scene.NamespaceGet(reference.Namespace)
            characterTargetList = Namespace.GetContentsByType(namespace, objectType=mobu.FBModel, exactType=False)

            # find dummy null
            dummy = None
            for content in characterTargetList:
                if content.Name == 'Dummy01' or content.Name == 'SetRoot':
                    dummy = content
                    break

            if dummy is not None:
                childList = []
                Scene.GetChildren(dummy, childList, "", False)

                nonPropToyAssetDict[dummy] = childList

        for key, value in nonPropToyAssetDict.iteritems():
            root = QtGui.QTreeWidgetItem(self.driversTree)
            root.setText(0, key.LongName)
            for i in value:
                child = QtGui.QTreeWidgetItem(root)
                child.setText(0, i.LongName)

    def _getScenePropToys(self):
        '''
        Gathers a list of proptoys in the scene. Finds both Ref objects and ones not referenced.
        Reurns:
            list[str]
        '''
        propToyNameList = []
        # get a list of reference proptoy objects
        propToyReferences = self.manager.GetReferenceListByType(MocapPropToy)

        for reference in propToyReferences:
            namespace = Globals.Scene.NamespaceGet(reference.Namespace)
            characterTargetList = Namespace.GetContentsByType(namespace, objectType=mobu.FBModel, exactType=False)
            for content in characterTargetList:
                if re.search('_null|_bone', content.Name) is not None:
                    propToyNameList.append(content.LongName)

        return propToyNameList

    def _handleAddSelectedPropToys(self):
        # get list of selected indexes
        selectedIdxs = {idx.row():idx for idx in self.propListTreeView.selectedIndexes() if idx.column() == 0}.values()

        # if index is a file, add to list
        fileList = []
        for index in selectedIdxs:
            # 3 = string: path to the asset, that is in the tree row
            path = index.data(3)
            # is the selected idx a file, rather than a folder?
            isFile = index.data(const.PropModelDataIndex.folderFileRole)
            if isFile:
                Perforce.Sync(str(path))
                fileInfo = Perforce.GetFileState(str(path))
                fileList.append(fileInfo.get_ClientFilename())

        # add selected prop(s) to scene and align to active stage
        referenceList = self.manager.CreateReferences(fileList) or []
        for reference in referenceList:
            reference.AlignReferenceRootToActiveStage()

        # clear selection
        self.propListTreeView.clearSelection()

        # re-populate proptoy combobox, with new prop toys int he scene
        self.populateScenePropToyComboBoxList()

    def _handleCreateConstraint(self):
        parentString = str(self.availablePropToysComboBox.currentText())

        if parentString == '':
            QtGui.QMessageBox.warning(None,
                                      "Proptoy Constraints",
                                      "Please select one PropToy object to create a constraint.",
                                      QtGui.QMessageBox.Ok)
            return

        if self.driversTree.currentItem() is not None:
            childString = str(self.driversTree.currentItem().text(0))
        else:
            QtGui.QMessageBox.warning(None,
                                      "Proptoy Constraints",
                                      "Please select one Driver object to create a constraint.",
                                      QtGui.QMessageBox.Ok)
            return

        childComponent = mobu.FBFindModelByLabelName(childString)
        # Force motion builder to evaluate the scene to ensure the matrix data is accurate
        mobu.FBSystem().Scene.Evaluate()
        matrix = mobu.FBMatrix()
        childComponent.GetMatrix(matrix)
        constraintType = self.constraintComboBox.currentIndex()
        if constraintType == 1:
            constraint = MocapCommands.CreateConstraint(childString, parentString, Constraint.PARENT_CHILD)
        elif constraintType == 0:
            constraint = MocapCommands.CreateConstraint(childString, parentString, Constraint.ROTATION)
        if self.snapCheckbox.checkState() == QtCore.Qt.CheckState.Checked:
            constraint.Snap()

        if self.retainPositionCheckbox.isEnabled() and self.retainPositionCheckbox.isChecked():
            childComponent.SetMatrix(matrix)
            # Force motion builder to evaluate the scene to ensure the matrix data is set
            mobu.FBSystem().Scene.Evaluate()

        # clear selection
        self.driversTree.clearSelection()
        # collapse tree
        self.driversTree.collapseAll()
        # reset snap checkbox
        self.snapCheckbox.setCheckState(QtCore.Qt.CheckState.Unchecked)

    def setupUi(self):
        """Sets up the widgets, their properties and launches call events"""
        # create layouts
        mainLayout = QtGui.QGridLayout()
        proptoyCreateLayout = QtGui.QVBoxLayout()
        constraintGroupBoxGridLayout = QtGui.QGridLayout()

        # create widgets
        # Add a warning widget if user doesn't have certain permissions.
        permissionsLabel = None
        if const.PrevizFolder.hasVirutalProductionRoot() is False:
            permissionsLabel = QtGui.QLabel(
                "User does not have access to //virtualproduction/ or it is not in the current workspace mappings.\n"
                "Check with IT that you have access and talk to Tech Art for any additional assistance. "
                "Some features will be disabled.",
            )
            permissionsLabel.setStyleSheet("QLabel { color : #D00000; font-size : 10pt; }")
            self.setEnabled(False)
        splitter = QtGui.QSplitter()
        self._filterSearchBox = QtGui.QLineEdit()
        self.propListTreeView = QtGui.QTreeView()
        proptoyGroupBox = QtGui.QGroupBox()
        emptyLabel = QtGui.QLabel()
        addPropToyButton = QtGui.QPushButton()
        constraintGroupBox = QtGui.QGroupBox()
        driversLabel = QtGui.QLabel()
        self.driversTree = QtGui.QTreeWidget()
        availablePropToysLabel = QtGui.QLabel()
        self.availablePropToysComboBox = QtGui.QComboBox()
        constraintLabel = QtGui.QLabel()
        self.constraintComboBox = QtGui.QComboBox()
        self.snapCheckbox = QtGui.QCheckBox()
        self.retainPositionCheckbox = QtGui.QCheckBox()


        createConstraintButton = QtGui.QPushButton()

        # widget properties
        self._filterSearchBox.setPlaceholderText("Set Name Filter")
        proptoyGroupBox.setTitle("Proptoy Create")
        addPropToyButton.setText('Add Selected PropToys to the Scene')
        constraintGroupBox.setTitle("Proptoy Constraints")
        driversLabel.setText('Select one Driver:')
        self.propListTreeView.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.propListTreeView.header().setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.propListTreeView.header().setStretchLastSection(True)
        self.driversTree.header().hide()
        self.driversTree.header().setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.driversTree.header().setResizeMode(QtGui.QHeaderView.ResizeToContents)
        self.driversTree.header().setStretchLastSection(False)
        self.populateDriverTreeList()
        availablePropToysLabel.setText('Scene PropToys:')
        self.populateScenePropToyComboBoxList()
        constraintLabel.setText('Type of Constraint:')
        self.populateConstraintTypeComboBoxList()
        self.snapCheckbox.setText('Snap (Activate) Constraint')
        self.retainPositionCheckbox.setText('Keep Child Position the Same')
        self.retainPositionCheckbox.setEnabled(False)
        self.snapCheckbox.toggled.connect(lambda checked: self.retainPositionCheckbox.setEnabled(checked))
        createConstraintButton.setText('Create Constraint')
        
        splitter.addWidget(proptoyGroupBox)
        splitter.addWidget(constraintGroupBox)

        # add widgets to groupBox
        proptoyCreateLayout.addWidget(self._filterSearchBox)
        proptoyCreateLayout.addWidget(self.propListTreeView)
        proptoyCreateLayout.addWidget(addPropToyButton)
        
        constraintGroupBoxGridLayout.addWidget(driversLabel, 0, 0)
        constraintGroupBoxGridLayout.addWidget(self.driversTree, 1, 0, 1, 2)
        constraintGroupBoxGridLayout.addWidget(availablePropToysLabel, 2, 0, 1, 2)
        constraintGroupBoxGridLayout.addWidget(self.availablePropToysComboBox, 3, 0, 1, 2)
        constraintGroupBoxGridLayout.addWidget(constraintLabel, 4, 0, 1, 2)
        constraintGroupBoxGridLayout.addWidget(self.constraintComboBox, 5, 0, 1, 2)
        constraintGroupBoxGridLayout.addWidget(emptyLabel, 6, 0, 1, 2)
        constraintGroupBoxGridLayout.addWidget(self.snapCheckbox, 7, 0)
        constraintGroupBoxGridLayout.addWidget(self.retainPositionCheckbox, 7, 1)
        constraintGroupBoxGridLayout.addWidget(createConstraintButton, 8, 0, 1, 2)

        # set groupBox layout
        proptoyGroupBox.setLayout(proptoyCreateLayout)
        constraintGroupBox.setLayout(constraintGroupBoxGridLayout)

        # add to layout
        if permissionsLabel:
            mainLayout.addWidget(permissionsLabel)
            mainLayout.setRowStretch(1, 2)
        mainLayout.addWidget(splitter)

        # set layout
        self.setLayout(mainLayout)

        # handles for callbacks
        addPropToyButton.pressed.connect(self._handleAddSelectedPropToys)
        createConstraintButton.pressed.connect(self._handleCreateConstraint)
        self._filterSearchBox.textEdited.connect(self._handlePropToyCreateFilterTextChange)

        # populate proptoy list
        self.populateTreeView()

    def _handlePropToyCreateFilterTextChange(self, newText):
        self._proxyModel.setFilterRegExp(newText)

