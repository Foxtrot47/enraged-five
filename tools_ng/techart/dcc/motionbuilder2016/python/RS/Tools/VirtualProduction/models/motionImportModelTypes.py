from PySide import QtGui, QtCore

import os

import pyfbsdk as mobu

from RS.Tools.VirtualProduction import const
from RS import Config, Globals, Perforce, ProjectData
from RS.Utils import Namespace
from RS.Tools.VirtualProduction.models import motionImportModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types import MocapPropToy, MocapGsSkeleton, MocapSlate


class MotionImportModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(MotionImportModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ["Name", "Type", "Clean Motion Imported"]

    def _getSlates(self):
        assets = []
        for item in [refItem for refItem in Manager.Manager().GetReferenceListAll() if isinstance(refItem, MocapSlate.MocapSlate)]:
            if item.Path.lower().endswith("slateclapper.fbx") is False:
                continue
            assets.append(item)
        return assets
    
    def _getCharacters(self):
        assets = []
        for item in [refItem for refItem in Manager.Manager().GetReferenceListAll() if isinstance(refItem, MocapGsSkeleton.MocapGsSkeleton)]:
            assets.append(item)
        return assets
    
    def _getProps(self):
        assets = []
        for item in [refItem for refItem in Manager.Manager().GetReferenceListAll() if isinstance(refItem, MocapPropToy.MocapPropToy)]:
            assets.append(item)
        return assets

    def _getFinalFBXFolderPath(self):
        '''
        Get default file path specific to the current scene. This file path will point towards the
        correct final fbx folder, so make it easier for the user to select the files needed for
        importing. If a match is not found, the default path will just point towards the root of
        fbx_final folder. This method will also get latest on the files in the returned folder.

        Return:
            string: file path
        '''
        # get current take name and matxch to subfolder name
        currentTakeName = mobu.FBSystem().CurrentTake.Name

        # path to gs skel files
        gsPath = ProjectData.data.FinalFBXFolderPath()

        if not os.path.isdir(gsPath):
            os.makedirs(gsPath)

        # find
        defaultPath = None
        srcFolder = os.path.normpath(gsPath)
        for (root, directories, files) in os.walk(srcFolder):
                splitRoot = root.split('\\')
                subFolderName = splitRoot[-1]
                if subFolderName == currentTakeName:
                    defaultPath = root
                    break

        if not defaultPath:
            defaultPath = gsPath
            return defaultPath

        # get latest on files in folder
        updateFileList = []
        for (root, directories, files) in os.walk(srcFolder):
            for eachFile in files:
                if root == defaultPath:
                    updateFileList.append(os.path.join(root, eachFile))
                    continue

        for item in updateFileList:
            Perforce.Sync(item)

        return defaultPath

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        defaultPath = self._getFinalFBXFolderPath()
        
        for item in self._getSlates():
            parent.appendChild(motionImportModelItem.MotionImportSlateItem(item, defaultPath, parent))
            
        for item in self._getCharacters():
            parent.appendChild(motionImportModelItem.MotionImportCharacterItem(item, defaultPath, parent))
            
        for item in self._getProps():
            parent.appendChild(motionImportModelItem.MotionImportPropItem(item, defaultPath, parent))

    def flags(self, index):
        '''
        Flags are added to determine column properties

        Args:
            index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        column = index.column()

        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        if column == 0:
            return flags | QtCore.Qt.ItemIsUserCheckable
        else:
            return flags