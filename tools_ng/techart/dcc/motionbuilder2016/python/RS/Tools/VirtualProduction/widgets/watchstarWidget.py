"""
Contains the WatchstarWidget that is found in the Rockstar menu and in the 
VirtualProductionDialog.
"""
import os
import datetime

from PySide import QtCore, QtGui

from RS import Globals, Perforce
from RS.Core.AnimData.Widgets import shotContextWidget, contextWidget, sessionContextWidget, stageContextWidget
from RS.Core.AnimData.Views import columnFilterView
from RS.Core.AnimData.Models import projectSessionDateTrialsModel, projectSessionDateShotsModel
from RS.Core.AnimData._internal import contexts
from RS.Core.ReferenceSystem import Manager
from RS.Tools.VirtualProduction.models import watchstarModel


class WatchstarWidget(QtGui.QWidget):
    """
    Widget with views for navigating to/loading shots, trials, and sessions.
    """

    def __init__(self, parent=None):
        """
        Constructor
        """
        self._manager = Manager.Manager()
        super(WatchstarWidget, self).__init__(parent=parent)
        self.currentStudio = None
        self.setupUi()

    def setupUi(self):
        # Layouts
        mainLayout = QtGui.QVBoxLayout()
        buttonLayout = QtGui.QHBoxLayout()
        locationDateTrialsLayout = QtGui.QGridLayout()
        locationDateShotsLayout = QtGui.QGridLayout()
        
        # Widgets
        contextTab = QtGui.QTabWidget()
        self._sortShotsCheckbox = QtGui.QCheckBox("Sort Ascending")
        self._sortTrialsCheckbox = QtGui.QCheckBox("Sort Ascending")
        self._sortSessionsCheckbox = QtGui.QCheckBox("Sort Ascending")
        self._shotsContextWidget = shotContextWidget.ShotContextColumnView()
        self._trialContextWidget = contextWidget.ContextColumnView()
        self._sessionContextWidget = sessionContextWidget.ContextSessionColumnView()
        self._assetTreeView = QtGui.QTreeView()
        self._currentContextLabel = QtGui.QLabel("No Context Selected")
        self._buildSceneButton = QtGui.QPushButton("Build Whole Scene")
        self._importSelectedButton = QtGui.QPushButton("Import Selected")

        self._locationDateShots = QtGui.QWidget()
        self._locationDateShotsStage = stageContextWidget.StageContextWidget()
        self._locationDateShotsDate = QtGui.QDateEdit(QtCore.QDate.currentDate())
        self._locationDateShotsView = columnFilterView.ColumModelView()

        self._locationDateTrials = QtGui.QWidget()
        self._locationDateTrialsStage = stageContextWidget.StageContextWidget()
        self._locationDateTrialsDate = QtGui.QDateEdit(QtCore.QDate.currentDate())
        self._locationDateTrialsView = columnFilterView.ColumModelView()

        # Models
        self._model = watchstarModel.AssetWatchstarModel()
        self._locationDateTrialsModel = projectSessionDateTrialsModel.ProjectSessionDateTrialsModel(date=datetime.datetime.now())
        self._locationDateShotsModel = projectSessionDateShotsModel.ProjectSessionDateShotsModel(date=datetime.datetime.now())

        # Configure
        tabWidgetSets = [
            (self._sortShotsCheckbox, self._shotsContextWidget, "Shots"),
            (self._sortTrialsCheckbox, self._trialContextWidget, "Trials"),
            (self._sortSessionsCheckbox, self._sessionContextWidget, "Sessions"),
        ]
        for chbox, ctxWdg, tabTitle in tabWidgetSets:
            chbox.setChecked(True)
            ctxWdg.layout().setContentsMargins(0, 0, 0, 0)
            newTabLayout = QtGui.QVBoxLayout()
            newTabLayout.addWidget(chbox)
            newTabLayout.addWidget(ctxWdg)
            newTabWidget = QtGui.QWidget()
            newTabWidget.setLayout(newTabLayout)
            contextTab.addTab(newTabWidget, tabTitle)
        
        contextTab.addTab(self._locationDateShots, "Shots by Location && Date")
        contextTab.addTab(self._locationDateTrials, "Trials by Location && Date")
        
        self.setStyleSheet("color: #ffffff;")
        self._currentContextLabel.setAlignment(QtCore.Qt.AlignCenter)
        self._buildSceneButton.setEnabled(False)
        self._importSelectedButton.setEnabled(False)
        self._assetTreeView.setAlternatingRowColors(True)
        self._assetTreeView.setModel(self._model)
        self._assetTreeView.setSelectionMode(QtGui.QTreeView.ExtendedSelection)
        self._locationDateTrialsStage.showColumnTitles(False)
        self._locationDateTrialsStage.hideBox(1)
        self._locationDateTrialsView.setModel(self._locationDateTrialsModel)
        self._locationDateTrialsView.setAlternatingRowColors(True)
        self._locationDateTrialsDate.setCalendarPopup(True)
        self._locationDateShotsStage.showColumnTitles(False)
        self._locationDateShotsStage.hideBox(1)
        self._locationDateShotsView.setModel(self._locationDateShotsModel)
        self._locationDateShotsView.setAlternatingRowColors(True)
        self._locationDateShotsDate.setCalendarPopup(True)
        
        # Widgets to layouts
        buttonLayout.addWidget(self._buildSceneButton)
        buttonLayout.addWidget(self._importSelectedButton)
        mainLayout.addWidget(contextTab)
        mainLayout.addWidget(self._currentContextLabel)
        mainLayout.addWidget(self._assetTreeView)
        mainLayout.addLayout(buttonLayout)

        locationDateTrialsLayout.addWidget(QtGui.QLabel("Location:"), 0, 0)
        locationDateTrialsLayout.addWidget(self._locationDateTrialsStage, 1, 0)
        locationDateTrialsLayout.addWidget(QtGui.QLabel("Date:"), 0, 1)
        locationDateTrialsLayout.addWidget(self._locationDateTrialsDate, 1, 1)
        locationDateTrialsLayout.addWidget(self._locationDateTrialsView, 2, 0, 2, 2)

        locationDateShotsLayout.addWidget(QtGui.QLabel("Location:"), 0, 0)
        locationDateShotsLayout.addWidget(self._locationDateShotsStage, 1, 0)
        locationDateShotsLayout.addWidget(QtGui.QLabel("Date"), 0, 1)
        locationDateShotsLayout.addWidget(self._locationDateShotsDate, 1, 1)
        locationDateShotsLayout.addWidget(self._locationDateShotsView, 2, 0, 2, 2)

        # Layouts to widgets
        self.setLayout(mainLayout)
        self._locationDateShots.setLayout(locationDateShotsLayout)
        self._locationDateTrials.setLayout(locationDateTrialsLayout)
        
        # Connections
        self._sortShotsCheckbox.stateChanged.connect(self._handleShotsSortChanged)
        self._sortTrialsCheckbox.stateChanged.connect(self._handleTrialsSortChanged)
        self._sortSessionsCheckbox.stateChanged.connect(self._handleSessionsSortChanged)
        self._shotsContextWidget.shotSelected.connect(self._handleContexSelect)
        self._trialContextWidget.trialSelected.connect(self._handleContexSelect)
        self._sessionContextWidget.sessionSelected.connect(self._handleContexSelect)
        self._locationDateTrialsView.clicked.connect(self._handleLocationDateTrialContexSelect)
        self._locationDateShotsView.clicked.connect(self._handleLocationDateShotsContexSelect)
        self._buildSceneButton.pressed.connect(self._handleBuildScene)
        self._importSelectedButton.pressed.connect(self._handleImportSelected)
       
        self._locationDateTrialsStage.locationSelected.connect(self._locationDateTrialsStageChange)
        self._locationDateTrialsDate.dateChanged.connect(self._locationDateDateTrialsChanged)
        self._locationDateShotsStage.locationSelected.connect(self._locationDateShotsStageChange)
        self._locationDateShotsDate.dateChanged.connect(self._locationDateDateShotsChanged)

    def _locationDateDateTrialsChanged(self, date):
        self._locationDateTrialsModel.setDate(datetime.datetime(date.year(), date.month(), date.day()))
    
    def _locationDateTrialsStageChange(self, newLoc):
        self._locationDateTrialsModel.setLocation(newLoc)

    def _locationDateDateShotsChanged(self, date):
        self._locationDateShotsModel.setDate(datetime.datetime(date.year(), date.month(), date.day()))
    
    def _locationDateShotsStageChange(self, newLoc):
        self._locationDateShotsModel.setLocation(newLoc)

    def setCurrentStudio(self, studio):
        self.currentStudio = studio

    def _handleShotsSortChanged(self):
        sortAscending = self._sortShotsCheckbox.isChecked()
        self._shotsContextWidget.setFilterOptions(sortAssending=sortAscending)

    def _handleTrialsSortChanged(self):
        sortAscending = self._sortTrialsCheckbox.isChecked()
        self._trialContextWidget.setFilterOptions(sortAssending=sortAscending)

    def _handleSessionsSortChanged(self):
        sortAscending = self._sortSessionsCheckbox.isChecked()
        self._sessionContextWidget.setFilterOptions(sortAssending=sortAscending)

    def _handleLocationDateTrialContexSelect(self, index):
        val = index.data(QtCore.Qt.UserRole)
        if isinstance(val, contexts._trialBase):
            self._handleContexSelect(val)

    def _handleLocationDateShotsContexSelect(self, index):
        val = index.data(QtCore.Qt.UserRole)
        if isinstance(val, contexts.Shot):
            self._handleContexSelect(val)

    def _handleContexSelect(self, newContext):
        self._model.setInputConext(newContext)
        if newContext is None:
            self._currentContextLabel.setText("No Context Selected")
            return
        self._currentContextLabel.setText("{0} ({1})".format(newContext.name, type(newContext).__name__))
        self._assetTreeView.expandAll()
        self._buildSceneButton.setEnabled(True)
        self._importSelectedButton.setEnabled(True)

    def _handleImportSelected(self):
        if self._model.getInputContext() is None:
            QtGui.QMessageBox.critical(self, "Watchstar", "No Shot/Trial Context selected")
            return
        items = list(set([idx.data(QtCore.Qt.UserRole) for idx in self._assetTreeView.selectedIndexes()]))
        for item in items:
            self._referenceAssetItem(item)
        self._updateView()

    def _handleBuildScene(self):
        if self._model.getInputContext() is None:
            QtGui.QMessageBox.critical(self, "Watchstar", "No Shot/Trial Context selected")
            return
        for refTypes in [self._model.audioItems(), self._model.characterItems(),
                         self._model.enviromentItems(), self._model.propItems(),
                         self._model.vehiclesItems()]:
            for item in refTypes:
                self._referenceAssetItem(item)
        self._updateView()

    def _updateView(self):
        self._model.update()
        self._assetTreeView.expandAll()

    def _referenceAssetItem(self, item):
        if not isinstance(item, watchstarModel.AssetWatchstarModelItem):
            return
        fileState = item.getFileState()
        if fileState is None:
            return
        
        # Sync it!
        Perforce.Sync(str(fileState.DepotFilename))
        
        # Handle Audio
        if fileState.ClientFilename.lower().endswith('.wav'):
            self._createAudio(fileState)
        # Handle FBX (reference)
        else:
            print "Adding Referene: {}".format(str(fileState.ClientFilename))
            self._manager.CreateReferences(str(fileState.ClientFilename))

    def _createAudio(self, audioFile, showPopups=True):
        """
        Creates Audio reference.
        
        Args:
            audioFile (p4 version object): The Perforce version object to 
                reference in.
            showPopups (bool, optional): True to allow popup, False to suppress.  
        """
        # Check for dupes - quiet mode optional
        audioDupe = None
        for each in Globals.gScene.AudioClips:
            if each.LongName == os.path.basename(str(audioFile.ClientFilename)):
                audioDupe = each
                break

        if showPopups:
            if audioDupe:
                flags = QtGui.QMessageBox.StandardButton.Yes | QtGui.QMessageBox.StandardButton.No
                question = "This audio file already exists within the scene.  Would you like to overwrite?"
                response = QtGui.QMessageBox.question(self, "Duplicate Audio", question, flags)
                if response == QtGui.QMessageBox.Yes:
                    audioDupe.FBDelete()
                else:
                    return
        else:
            if audioDupe:
                audioDupe.FBDelete()
        print "Adding Audio Referene: {}".format(str(audioFile.ClientFilename))
        self._manager.CreateReferences(str(audioFile.ClientFilename))
