import os

from PySide import QtCore

import pyfbsdk as mobu

from RS.Tools.VirtualProduction import const
from RS import Config, Globals, Perforce
from RS.Utils import Scene
from RS.Tools.VirtualProduction.models import propToyModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class PropToyModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PropToyModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        # need 2 headers for this widget as the horizontal scrollbar seems to require it :(
        return ['PropToys']

    def syncDepotPaths(self):
        """
        get all proptoy paths via p4 and sync them
        """
        Perforce.Sync(const.PrevizFolder().GetAssetsForLocation())

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        propDict = {fullPath.replace("{}/".format(const.PrevizFolder().PERFORCE_ROOT), ""):fullPath for 
                                    fullPath in const.PrevizFolder().GetAssetsForLocation()}
        
        buildTree = {}
        for folderStruct, fullPath in propDict.iteritems():
            currentPath = []
            currentNode = None
            for part in folderStruct.split("/"):
                # Get where we are
                newPath = "/".join(currentPath + [part])
                
                if newPath == folderStruct:
                    # This is the file
                    newNode = propToyModelItem.PropToyModelItem(os.path.splitext(part)[0],
                                                                  fullPath=fullPath,
                                                                  folderBool=False,
                                                                  parent=currentNode)
                    currentNode.appendChild(newNode)
                else:
                    
                    # check if we have this folder node already
                    newPathNode = buildTree.get(newPath)
                    if newPathNode is None:
                        # Build it if we dont
                        newNode = propToyModelItem.PropToyModelItem(part,folderBool=True,parent=currentNode or parent)
                        (currentNode or parent).appendChild(newNode)
                        buildTree[newPath] = newNode
                        currentNode = newNode
                    else:
                        # use it if we do
                        currentNode = newPathNode
                    currentPath.append(part)

    def flags(self, index):
        '''
        Flags are added to determine column properties

        Args:
            index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        column = index.column()
        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

        return flags