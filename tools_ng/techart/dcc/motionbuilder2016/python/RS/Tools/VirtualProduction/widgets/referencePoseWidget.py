import pyfbsdk as mobu

from PySide import QtCore, QtGui

import os

from RS import Config, Globals
from RS.Core.Mocap import MocapCommands
from RS.Utils.Scene import Character
from RS.Tools.VirtualProduction import const
from RS.Tools.VirtualProduction.models import referencePoseModelTypes


class CharacterCheckboxStruct:
    def __init__(self, CharacterCheckBoxObject, GenderComboBoxObject, CheckboxBool, CharacterString, GenderString, CharacterNode, CharacterRGBColor, RefCountSpinBoxObject, RefCountInt):
        self.CharacterCheckBoxObject = CharacterCheckBoxObject
        self.GenderComboBoxObject = GenderComboBoxObject
        self.CheckboxBool = CheckboxBool
        self.CharacterString = CharacterString
        self.GenderString = GenderString
        self.CharacterNode = CharacterNode
        self.CharacterRGBColor = CharacterRGBColor
        self.RefCountSpinBoxObject = RefCountSpinBoxObject
        self.RefCountInt = RefCountInt


class ReferencePoseWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(ReferencePoseWidget, self).__init__(parent=parent)
        self.CharacterCheckboxList = []
        self._characterList = []
        self._refPoseList = []
        self._scrollAreaWidgetContents = None
        self._currentFrameIcon = QtGui.QIcon(QtGui.QPixmap(os.path.join(Config.Script.Path.ToolImages,
                                                                        "Mocap",
                                                                        "currentFrameLine.png")))
        self.setupUi()

    def _getCharacterLists(self):
        self._refPoseList = []
        self._characterList = []
        for character in Globals.Characters:
            refPoseProperty = character.PropertyList.Find('Ref Pose Component')
            if refPoseProperty:
                self._refPoseList.append(character)
            else:
                self._characterList.append(character)

    def populateScrollAreaContents(self):
        self.CharacterCheckboxList = []
        counter = 5
        if self._characterList:
            for idx in range(len(self._characterList)):
                split = self._characterList[idx].LongName.split(":")
                if len(split) >2:
                    characterName = '{0}:{1}'.format(split[0], split[1])
                else:
                    characterName = split[0]

                # get hips to find color
                rgb = "205, 205, 205"
                colorString = 'color unassigned'
                skelRoot = self._characterList[idx].GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
                if skelRoot:
                    colorProperty = skelRoot.PropertyList.Find('actor color')
                    colorStringProperty = skelRoot.PropertyList.Find('actor color string')
                    if colorProperty and colorStringProperty:
                        r = int(colorProperty.Data[0] *256)
                        g = int(colorProperty.Data[1] *256)
                        b = int(colorProperty.Data[2] *256)
                        rgb = "{0}, {1}, {2}".format(str(r), str(g), str(b))
                        colorString = colorStringProperty.Data.lower()

                characterNameCheckBox = QtGui.QCheckBox(self._scrollAreaWidgetContents)
                characterNameCheckBox.setGeometry(QtCore.QRect(5, counter, 12, 12))

                # double check color string as color names changed
                colorNameDict = const.ColorNameChange.colorNameChangeDict

                for key, value in colorNameDict.iteritems():
                    if key.lower() == colorString:
                        colorString = value.lower()

                colorIconPath = "{0}\\Mocap\\actor_color_{1}.png".format(Config.Script.Path.ToolImages, colorString)
                if os.path.exists(colorIconPath):
                    colorIconLabel = QtGui.QLabel(self._scrollAreaWidgetContents)
                    colorIconLabel.setGeometry(QtCore.QRect(30, counter, 70, 12))
                    colorIconLabel.setText("[{0}]".format(colorString))
                    colorIconLabel.setStyleSheet("color: rgb({0});".format(rgb))

                characterNameLabel = QtGui.QLabel(self._scrollAreaWidgetContents)
                characterNameLabel.setGeometry(QtCore.QRect(120, counter, 400, 12))
                characterNameLabel.setText(characterName)

                genderDropDown = QtGui.QComboBox(self._scrollAreaWidgetContents)
                genderDropDown.setGeometry(QtCore.QRect(460, counter, 60, 17))
                genderDropDown.addItem("Male")
                genderDropDown.addItem("Female")
                genderDropDown.setToolTip('Select gender for reference pose\nfor checked character')

                refCountDropdown = QtGui.QSpinBox(self._scrollAreaWidgetContents)
                refCountDropdown.setGeometry(QtCore.QRect(530, counter, 35, 17))
                refCountDropdown.setMaximum(30)
                refCountDropdown.setMinimum(1)
                refCountDropdown.setToolTip('Select number of reference poses you\nwould like for checked character')

                # get character details
                isPedFemale = Character.IsPedFemale(self._characterList[idx])
                if isPedFemale == True:
                    genderDropDown.setCurrentIndex(1)

                if characterNameCheckBox.checkState() == QtCore.Qt.CheckState.Checked:
                    checked = True
                else:
                    checked = False

                name = str(characterNameLabel.text())

                gender = str(genderDropDown.currentText())

                refPoseCount = refCountDropdown.value()

                characterNode = self._characterList[idx]

                # add character info to a struct list
                self.CharacterCheckboxList.append(CharacterCheckboxStruct(characterNameCheckBox, genderDropDown, checked, name, gender, characterNode, rgb, refCountDropdown, refPoseCount))

                counter += 21

    def MergeRefPose(self):
        playerControl = mobu.FBPlayerControl()
        # get current play/snap status
        originalSnapMode = playerControl.SnapMode
        # get current time slider frame position
        currentFrame = mobu.FBSystem().LocalTime.GetFrame()
        # merge ref pose
        MocapCommands.MergeRefPose(self.CharacterCheckboxList)
        # set time back as it was before referencing
        playerControl.Goto(mobu.FBTime(0, 0, 0, currentFrame))
        # ensure you're still on the original play/snap status
        playerControl.SnapMode = originalSnapMode
        # re-populate lists
        self._getCharacterLists()
        # uncheck character checkboxes
        if self.CharacterCheckboxList:
            for character in self.CharacterCheckboxList:
                character.CharacterCheckBoxObject.setCheckState(QtCore.Qt.CheckState.Unchecked)
                # reset counter spinboxes
                character.RefCountSpinBoxObject.setValue(1)
        # re set refPose character tree
        self.populateTreeView()

    def PlotRefPose(self):
        characterPlotList = []
        for child in self._mainModel.rootItem.children():
            checked = child.GetCheckedState()
            if checked is True:
                character = child.GetCharacter()
                characterPlotList.append(character)
        # get current play/snap status
        originalSnapMode = mobu.FBPlayerControl().SnapMode
        # plot
        MocapCommands.PlotRefPose(characterPlotList, self.plotWholeTakeCheckBox, self.frameSpinBox)
        # re-populate lists
        self._getCharacterLists()
        # reset treeview
        self.populateTreeView()
        # reset counter spinbox
        self.frameSpinBox.setValue(0)
        # ensure you're still on the original play/snap status
        mobu.FBPlayerControl().SnapMode = originalSnapMode

    def ReferenceCheckboxOnChange(self):
        if self.CharacterCheckboxList:
            for character in self.CharacterCheckboxList:
                if self.sender() == self.checkAllButton:
                    character.CharacterCheckBoxObject.setCheckState(QtCore.Qt.CheckState.Checked)
                else:
                    character.CharacterCheckBoxObject.setCheckState(QtCore.Qt.CheckState.Unchecked)

    def addCurrentFrame(self):
        currentFrame = mobu.FBSystem().LocalTime.GetFrame()
        self.frameSpinBox.setValue(currentFrame)

    def FrameSpinboxEnable(self):
        if self.plotFrameCheckBox.isChecked() == True:
            self.frameSpinBox.setEnabled(True)
            self.currentFrameButton.setEnabled(True)
            self.addCurrentFrame()
        else:
            self.frameSpinBox.setEnabled(False)
            self.currentFrameButton.setEnabled(False)

    def CharacterCheckboxOnChange(self):
        if self.CharacterCheckboxList:
            for character in self.CharacterCheckboxList:
                if character.CharacterCheckBoxObject.checkState() == QtCore.Qt.CheckState.Checked:
                    character.CheckboxBool = True
                else:
                    character.CheckboxBool = False

    def GenderComboboxOnChange(self):
        if self.CharacterCheckboxList:
            for character in self.CharacterCheckboxList:
                character.GenderString = str(character.GenderComboBoxObject.currentText())

    def RefCountSpinBoxOnChange(self):
        if self.CharacterCheckboxList:
            for character in self.CharacterCheckboxList:
                character.RefCountInt = int(character.RefCountSpinBoxObject.value())

    def populateTreeView(self):
        # reset view
        self.refPoseTreeView.reset()
        # add model to tree widget
        self._mainModel = referencePoseModelTypes.ReferencePoseModel()
        self.refPoseTreeView.setModel(self._mainModel)

    def resetScrollArea(self):
        self._getCharacterLists()
        if self._scrollAreaWidgetContents:
            self._scrollAreaWidgetContents.close()
            self._scrollAreaWidgetContents = None
        # scroll area scroll size
        scrollSize = 180
        listLength = len(self._characterList)
        if listLength > 8:
            excessList = listLength - 8
            scrollSize = scrollSize + (excessList * 20)

        self._scrollAreaWidgetContents = QtGui.QWidget()
        self._scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 580, scrollSize))
        self._scrollArea.setWidget(self._scrollAreaWidgetContents)
        self.populateScrollAreaContents()
        for widget in self._scrollAreaWidgetContents.children():
            widget.show()

        # handles
        if self.CharacterCheckboxList:
            for character in self.CharacterCheckboxList:
                character.CharacterCheckBoxObject.stateChanged.connect(self.CharacterCheckboxOnChange)
                character.GenderComboBoxObject.currentIndexChanged.connect(self.GenderComboboxOnChange)
                character.RefCountSpinBoxObject.valueChanged.connect(self.RefCountSpinBoxOnChange)

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        plotGroupBoxGridLayout = QtGui.QGridLayout()
        refGroupBoxGridLayout = QtGui.QGridLayout()

        # populate character and refPose lists
        self._getCharacterLists()

        # create widgets
        self._scrollArea = QtGui.QScrollArea()
        self._scrollArea.setWidgetResizable(False)
        referenceGroupBox = QtGui.QGroupBox()
        self.checkAllButton = QtGui.QPushButton()
        self.uncheckAllButton = QtGui.QPushButton()
        loadNewRefPoseButton = QtGui.QPushButton()
        plotGroupBox = QtGui.QGroupBox()
        refPoseLabel = QtGui.QLabel()
        self.refPoseTreeView = QtGui.QTreeView()
        self.refPoseTreeView.setAlternatingRowColors(True)
        self.refPoseTreeView.setStyleSheet("QTreeView {\nalternate-background-color: rgb(56, 56, 56); \nbackground-color: rgb(43, 43, 43);\n} \n")
        self.refPoseTreeView.setSortingEnabled(True)
        self.plotWholeTakeCheckBox = QtGui.QCheckBox()
        self.plotFrameCheckBox = QtGui.QCheckBox()
        self.frameSpinBox = QtGui.QSpinBox()
        self.currentFrameButton = QtGui.QPushButton()
        plotReferenceButton = QtGui.QPushButton()
        plotButtonGroup = QtGui.QButtonGroup(self)

        # other widget properties
        self.resetScrollArea()
        self.populateTreeView()
        referenceGroupBox.setTitle("Load Reference Pose")
        self.checkAllButton.setText("Check All")
        self.uncheckAllButton.setText("Uncheck All")
        loadNewRefPoseButton.setText("Load Reference Pose(s) for Selected")
        plotGroupBox.setTitle("Plot Reference Pose")
        self.plotWholeTakeCheckBox.setText("Whole take")
        self.plotFrameCheckBox.setText("Frame")
        refPoseLabel.setText("Select a Reference Pose Character:")
        self.refPoseTreeView.header().hide()
        self.refPoseTreeView.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)
        plotReferenceButton.setText("Plot Reference")
        plotButtonGroup.addButton(self.plotWholeTakeCheckBox)
        plotButtonGroup.addButton(self.plotFrameCheckBox)
        self.plotWholeTakeCheckBox.setCheckState(QtCore.Qt.CheckState.Checked)
        self.frameSpinBox.setEnabled(False)
        self.frameSpinBox.setMinimum(-500000)
        self.frameSpinBox.setMaximum(500000)
        self.currentFrameButton.setEnabled(False)
        self.currentFrameButton.setIcon(self._currentFrameIcon)
        self.currentFrameButton.setMaximumSize(QtCore.QSize(25, 13))
        self.currentFrameButton.setIconSize(QtCore.QSize(10, 8))
        self.currentFrameButton.setToolTip("set current frame to spinbox")
        plotSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)

        # add widgets to groupBox
        plotGroupBoxGridLayout.addWidget(refPoseLabel, 0, 0)
        plotGroupBoxGridLayout.addWidget(self.refPoseTreeView, 1, 0, 1, 3)
        plotGroupBoxGridLayout.addWidget(self.plotWholeTakeCheckBox, 2, 0)
        plotGroupBoxGridLayout.addWidget(self.plotFrameCheckBox, 3, 0)
        plotGroupBoxGridLayout.addWidget(self.frameSpinBox, 3, 1)
        plotGroupBoxGridLayout.addWidget(self.currentFrameButton, 3, 2)
        plotGroupBoxGridLayout.addItem(plotSpacerItem, 4, 0)
        plotGroupBoxGridLayout.addWidget(plotReferenceButton, 5, 0, 1 ,3)
        refGroupBoxGridLayout.addWidget(self._scrollArea, 0, 0, 1, 6)
        refGroupBoxGridLayout.addWidget(self.checkAllButton, 1, 0)
        refGroupBoxGridLayout.addWidget(self.uncheckAllButton, 1, 1)
        refGroupBoxGridLayout.addWidget(loadNewRefPoseButton, 2, 0, 1, 6)

        # set groupBox layout
        plotGroupBox.setLayout(plotGroupBoxGridLayout)
        referenceGroupBox.setLayout(refGroupBoxGridLayout)

       # add other widgets to layout
        mainLayout.addWidget(referenceGroupBox, 0, 0, 1, 2)
        mainLayout.addWidget(plotGroupBox, 0, 2)

        # set layout
        self.setLayout(mainLayout)

        # handles
        loadNewRefPoseButton.released.connect(self.MergeRefPose)
        plotReferenceButton.released.connect(self.PlotRefPose)
        self.checkAllButton.released.connect(self.ReferenceCheckboxOnChange)
        self.uncheckAllButton.released.connect(self.ReferenceCheckboxOnChange)
        self.plotFrameCheckBox.stateChanged.connect(self.FrameSpinboxEnable)
        self.currentFrameButton.pressed.connect(self.addCurrentFrame)
