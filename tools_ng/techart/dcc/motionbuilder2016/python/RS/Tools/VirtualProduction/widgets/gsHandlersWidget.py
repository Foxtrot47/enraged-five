from PySide import QtGui

import pyfbsdk as mobu

from RS.Tools import UI
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Delegates import comboboxDelegate
from RS.Tools.VirtualProduction.models import gsHandlersModel


class GsHandleWidget(QtGui.QWidget):
    """
    The widget which holds the UI and Model for the Giant Device Manager Widget Tool
    """
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(GsHandleWidget, self).__init__(parent=parent)
        self._realtimeAttachments = {}
        self.setupUi()
    
    def setupUi(self):
        # Layouts
        layout = QtGui.QVBoxLayout()
        
        # Widgets
        self._mainView = QtGui.QTreeView()
        
        # Models
        self._mainModel = gsHandlersModel.GsHandlerModel()
        
        # configure widgets
        self._mainView.setModel(self._mainModel)
        
        handlerNames = [
            "", # Blank so its blank at the start
            "Player 1",
            "Player 2",
            "Player 3",
            "Player 4",
            ]
        
        self._comboBoxDelegate = comboboxDelegate.EditableComboBoxDelegate(handlerNames, "Enter/Select Handle Text")
        self._mainView.setItemDelegateForColumn(1, self._comboBoxDelegate)
        
        # Assign Widgets to Layouts
        layout.addWidget(self._mainView)
        
        # Apply Layouts to Widgets
        self.setLayout(layout)

    def refreshData(self):
        """
        Re-sets the model
        """
        self._mainModel.reset()


def Run():
    mainWin = UI.QtMainWindowBase()
    widget = GsHandleWidget()
    mainWin.setCentralWidget(widget)
    mainWin.show()
