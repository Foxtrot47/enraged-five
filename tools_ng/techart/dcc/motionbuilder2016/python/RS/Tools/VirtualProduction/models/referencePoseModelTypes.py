from PySide import QtGui, QtCore

from RS import Config, Globals, Perforce
from RS.Utils import Scene
from RS.Tools.VirtualProduction.models import referencePoseModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class ReferencePoseModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(ReferencePoseModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ['']

    def _getReferencePoseCharacters(self):
        '''
        Get list of reference pose characters list
        
        Returns:
            Dict (String:FBCharacter): dictionary of character name string and the fb character node
        '''
        refPoseList = []
        for character in Globals.Characters:
            refPoseProperty = character.PropertyList.Find('Ref Pose Component')
            if refPoseProperty is not None and '[plotted]' not in refPoseProperty.Name:
                refPoseList.append(character)
        return refPoseList

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        for character in self._getReferencePoseCharacters():
            parentNode = referencePoseModelItem.ReferencePoseModelItem(character,
                                                                       character.LongName,
                                                                       parent=parent)
            parent.appendChild(parentNode)

    def flags(self, index):
        '''
        Flags are added to determine column properties
        
        Args:
            index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        column = index.column()
        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsUserCheckable

        return flags