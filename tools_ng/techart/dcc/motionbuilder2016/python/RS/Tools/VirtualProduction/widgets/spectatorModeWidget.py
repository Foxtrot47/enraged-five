"""Contains the SpectatorModeWidget."""
# Ensures current thread is set up to run STA COM concurrency model rather than the default MTA.
# This must be called before clr is imported otherwise Qt application initialisation will fail
# if it happens after this. e.g.: "Qt: Could not initialize OLE (error 80010106)" (url:bugstar:5722186)
import ctypes
ctypes.windll.ole32.CoInitialize(None)

import os
import shutil

from PySide import QtCore, QtGui

from RS import Globals, Config
from RS.Core.Mocap import MocapCommands
from RS.Tools.CameraToolBox.PyCoreQt.Dialogs import progressMessageDialog
from RS.Tools.VirtualProduction import const


class SpectatorModeWidget(QtGui.QWidget):
    """Widget with options to help setup and manage Spectator Mode.

    Notes:
        * Designed to have setCurrentStage connected to a signal on the stage setting widget to get updates.

    Attributes:
        TITLE (str): The tool's title for use in title bars.
        TOOL_NAME (str): The tool's name for use in settings/config files.
    """

    TITLE = "Spectator Mode"

    def __init__(self, stage=None, parent=None):
        """Initialize class.

        Args:
            stage (stages.Stage): The stage to read/write files for. Default: None.
            parent (QtGui.QWidget): The parent widget. Default: None.
        """
        super(SpectatorModeWidget, self).__init__(parent=parent)

        self._stage = stage
        self._scenePath = None
        self.TOOL_NAME = self.__class__.__name__

        # Logging to MotionBuilder app level log file with widget as context to filter by. Set on instance to allow more
        # than one tool to define itself as context in their respective setupUi methods.
        self.log = Globals.Ulog

        # Set up UI.
        self.setupUi()

    def setCurrentStage(self, stage):
        """Set the current stage for the widget.

        Args:
            stage (stages.Stage): The animdata stage context.
        """
        self._stage = stage

    def setupUi(self):
        """Create layouts and widgets."""
        # Widgets.
        tabWidget = QtGui.QTabWidget()
        operatorTab = QtGui.QWidget()
        spectatorTab = QtGui.QWidget()
        saveSceneBttn = QtGui.QPushButton("Save All")
        saveMappingsBttn = QtGui.QPushButton("Save Mappings Only")
        self._loadFromLocalFileChbox = QtGui.QCheckBox("Use Local File")
        loadSceneBttn = QtGui.QPushButton("Load All")
        loadMappingsSpectorBttn = QtGui.QPushButton("Load Mappings Only")
        loadMappingsBttn = QtGui.QPushButton("Load Mappings Only")
        addGiantBttn = QtGui.QPushButton("Add Giant Device")
        operatorSpacerItem = QtGui.QSpacerItem(10, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        spectatorSpacerItem = QtGui.QSpacerItem(10, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)

        # Configure widgets.
        loadSceneBttn.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum))
        tabWidget.addTab(operatorTab, "Operator")
        tabWidget.addTab(spectatorTab, "Spectator")

        # Layouts.
        operatorTabLayout = QtGui.QVBoxLayout()
        operatorTabLayout.addWidget(saveSceneBttn)
        operatorTabLayout.addWidget(loadMappingsSpectorBttn)
        operatorTabLayout.addWidget(saveMappingsBttn)
        operatorTabLayout.addItem(operatorSpacerItem)
        operatorTab.setLayout(operatorTabLayout)

        loadAllLayout = QtGui.QHBoxLayout()
        loadAllLayout.addWidget(self._loadFromLocalFileChbox)
        loadAllLayout.addWidget(loadSceneBttn)

        spectatorTabLayout = QtGui.QVBoxLayout()
        spectatorTabLayout.addLayout(loadAllLayout)
        spectatorTabLayout.addWidget(addGiantBttn)
        spectatorTabLayout.addWidget(loadMappingsBttn)
        spectatorTabLayout.addItem(spectatorSpacerItem)
        spectatorTab.setLayout(spectatorTabLayout)

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(tabWidget)
        self.setLayout(mainLayout)

        # Connections.
        saveSceneBttn.released.connect(self._handleSaveBttnReleased)
        loadMappingsSpectorBttn.released.connect(self._handleLoadMappingsBttnReleased)
        saveMappingsBttn.released.connect(self._handleSaveMappingsBttnReleased)
        loadSceneBttn.released.connect(self._handleLoadBttnReleased)
        addGiantBttn.released.connect(MocapCommands.AddGiantDevice)
        loadMappingsBttn.released.connect(self._handleLoadMappingsBttnReleased)

    def _checkForSpectatorPermissions(self):
        if const.SpectatorFolder.hasAccess() is True:
            return True
        else:
            link = "https://hub.gametools.dev/display/RPERFCAP/Shoot%3A+Spectator+Mode"
            msg = "<br>".join(["Unable to access spectator folder, permission denied", 
                   "More information and troubleshooting, can be found <a href=\"{}\">here</a>.".format(link)])
            QtGui.QMessageBox.critical(self, self.TITLE, msg)
            return False

    def _handleSaveBttnReleased(self):
        if self._checkForSpectatorPermissions() is False:
            return
        
        if self._stage is None:
            QtGui.QMessageBox.warning(self, self.TITLE, "Please select a stage!")
            return

        # Save scene.
        defaultFbxPath = const.SpectatorFolder.getPath(self._stage, ext=".fbx")
        defaultFbxDir = os.path.split(defaultFbxPath)[0]
        if not os.path.exists(defaultFbxDir):
            os.makedirs(defaultFbxDir)
        Globals.Application.FileSave(str(defaultFbxPath))  # FileSave can't handle unicode.

        # Export mappings.
        mappingsPath = defaultFbxPath.replace(".fbx", const.SpectatorFileExtentions.GIANT_MAPPINGS)
        MocapCommands.GiantDeviceManager.ExportMappingToFile(mappingsPath)

        QtGui.QMessageBox.information(self, self.TITLE, "Save all complete!")

    def _toggleGiantDevice(self):
        giantDevice = MocapCommands.GetGiantDevice()
        if giantDevice is None:
            raise MocapCommands.GiantDeviceMissingError("Giant device is missing from the scene!")

        giantDevice.Live = False
        giantDevice.Live = True

    def _handleLoadBttnReleased(self):
        # Validation.
        if self._checkForSpectatorPermissions() is False:
            return
        
        if self._stage is None:
            QtGui.QMessageBox.warning(self, self.TITLE, "Please select a stage!")
            return

        # Safety checks.
        result = QtGui.QMessageBox.warning(
            self,
            self.TITLE,
            "Are you sure you would like to Load All?",
            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel,
        )
        if result == QtGui.QMessageBox.Cancel:
            return

        # Resolve .FBX path.
        defaultFbxPath = const.SpectatorFolder.getPath(self._stage, ext=const.SpectatorFileExtentions.AUTODESK_FBX)
        if not os.path.exists(defaultFbxPath):
            QtGui.QMessageBox.warning(
                self,
                self.TITLE,
                "Spectator file has not been created yet. Please contact your Previz operator for an update.",
            )
            return

        resolvedFbxPath = defaultFbxPath
        if self._loadFromLocalFileChbox.isChecked():  # Copy locally if requested.
            baseName = os.path.basename(defaultFbxPath)
            tempDir = Config.User.tempDir()
            resolvedFbxPath = os.path.join(tempDir, self.TITLE.replace(" ", ""), baseName)

            msgDialog = progressMessageDialog.ProgressMessageDialog(self.TITLE, "")
            if os.path.exists(resolvedFbxPath):
                # Remove local copy from previous run.
                # Note that the isFile checks prevent accidental deletion of directory trees in case of a bug upstream.
                if os.path.exists(resolvedFbxPath) and os.path.isfile(resolvedFbxPath):
                    msgDialog.setMessage("Removing existing local .FBX...")
                    msgDialog.show()
                    os.remove(resolvedFbxPath)
                    msgDialog.hide()

            # Copy from network to users temp dir
            resolvedFbxDir = os.path.dirname(resolvedFbxPath)
            if not os.path.exists(resolvedFbxDir):
                os.makedirs(resolvedFbxDir)
            msgDialog.setMessage("Copying .FBX from network to local machine...")
            msgDialog.show()
            shutil.copy2(defaultFbxPath, resolvedFbxPath)
            msgDialog.hide()
            if not os.path.exists(resolvedFbxPath):
                QtGui.QMessageBox.warning(
                    self,
                    self.TITLE,
                    "Failed to copy .FBX to local machine. Try manually copying '{}' to '{}' and run again.".format(
                        defaultFbxPath, resolvedFbxPath
                    ),
                )
                return

        # Load scene.
        fileSizeInMB = os.stat(resolvedFbxPath).st_size / 1024 / 1024
        msgDialog = progressMessageDialog.ProgressMessageDialog(
            self.TITLE,
            "Loading '{}' ({} MB)".format(resolvedFbxPath, fileSizeInMB),  # The Windows Explorer> Properties value.
        )
        msgDialog.show()
        Globals.Application.FileOpen(str(resolvedFbxPath))  # FileOpen doesn't support unicode in py2.
        msgDialog.hide()

        # Import mappings.
        mappingsPath = defaultFbxPath.replace(".fbx", const.SpectatorFileExtentions.GIANT_MAPPINGS)
        if not os.path.exists(mappingsPath):
            QtGui.QMessageBox.warning(
                self,
                self.TITLE,
                "Giant mappings file has not been created yet. Please contact your Previz operator for an update.",
            )
            return

        MocapCommands.GiantDeviceManager.ImportMappingFromFile(mappingsPath)

        # Giant device: Toggle the device so the mappings take effect and make sure spectator mode is turned on.
        try:
            self._toggleGiantDevice()
            if not MocapCommands.GiantDeviceManager.GetSpectatorMode():
                MocapCommands.GiantDeviceManager.SetSpectatorMode(True)
        except MocapCommands.GiantDeviceMissingError:
            QtGui.QMessageBox.warning(self, self.TITLE, "Giant device is missing from the scene!")
            return

        MocapCommands.SetStageBoxName(spectator=True)
        QtGui.QMessageBox.information(self, self.TITLE, "Load all complete!")

    def _handleSaveMappingsBttnReleased(self):
        if self._checkForSpectatorPermissions() is False:
            return
        
        if self._stage is None:
            QtGui.QMessageBox.warning(self, self.TITLE, "Please select a stage!")
            return

        # Save file.
        defaultPath = const.SpectatorFolder.getPath(self._stage, ext=const.SpectatorFileExtentions.GIANT_MAPPINGS)
        defaultDir = os.path.split(defaultPath)[0]
        if not os.path.exists(defaultDir):
            os.makedirs(defaultDir)

        # Export mappings.
        MocapCommands.GiantDeviceManager.ExportMappingToFile(defaultPath)

        QtGui.QMessageBox.information(self, self.TITLE, "Save Giant mappings complete!")

    def _handleLoadMappingsBttnReleased(self):
        if self._checkForSpectatorPermissions() is False:
            return
        
        if self._stage is None:
            QtGui.QMessageBox.warning(self, self.TITLE, "Please select a stage!")
            return

        # Import mappings.
        defaultPath = const.SpectatorFolder.getPath(self._stage, ext=const.SpectatorFileExtentions.GIANT_MAPPINGS)
        if not os.path.exists(defaultPath):
            QtGui.QMessageBox.warning(
                self,
                self.TITLE,
                "Giant mappings file has not been created yet. Please contact your Previz operator for an update.",
            )
            return

        MocapCommands.GiantDeviceManager.ImportMappingFromFile(defaultPath)

        # Giant device: Toggle the device so the mappings take effect
        try:
            self._toggleGiantDevice()
        except MocapCommands.GiantDeviceMissingError:
            QtGui.QMessageBox.warning(self, self.TITLE, "Giant device is missing from the scene!")
            return

        QtGui.QMessageBox.information(self, self.TITLE, "Load Giant mappings complete!")


if __name__ == "__builtin__":  # For testing in MotionBuilder.
    win = SpectatorModeWidget()
    win.show()
