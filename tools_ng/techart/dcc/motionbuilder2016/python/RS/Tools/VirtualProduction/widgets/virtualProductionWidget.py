import os
import sys
import webbrowser

from PySide import QtGui, QtCore
import pyfbsdk as mobu

from RS import Config, Globals
from RS.Utils.Scene import Component
from RS.Core.AnimData import Context
from RS.Core.AnimData.Models import stageModel
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.Mocap import realtimeConnection
from RS.Tools import UI
from RS.Tools.VirtualProduction import const
from RS.Tools.VirtualProduction.widgets import (watchstarWidget,
                                                stagingWidget,
                                                propToyWidget,
                                                gsSkelMatchWidget,
                                                duringShootWidget,
                                                spectatorModeWidget,
                                                referencePoseWidget,
                                                layoutWidget,
                                                motionImportWidget,
                                                giantDeviceManagerWidget,
                                                gsHandlersWidget,
                                                mocapAssetEditorTool,)


class ClickableLabel(QtGui.QLabel):
    clicked = QtCore.Signal()

    def mousePressEvent(self, event):
        self.clicked.emit()


class _SettingsKeys(object):
    DOCK_STATE = "DockState"
    CAPTUREBOSS_CONNECTED_STATE = "CaptureBoss Connected State"
    CAPTUREBOSS_IP_ADDRESS = "CaptureBoss IP Address"
    DISPLAY_CAPTUREBOSS_IP_ADDRESS = "Display CaptureBoss IP Address"
    LOCATION_STAGE = "Stage Location"
    MOTION_IMPORT = "Motion Import Widget"
    INCLUDE_OFFSITE_STAGES = "Include Offsite Stages"



class VirtualProductionDialog(UI.QtMainWindowBase):
    """Dialog to run VirtualProductionWidget and retain size settings for opening and closing of the widget."""
    def __init__(self, parent=None):
        self._settings = QtCore.QSettings("RockstarSettings", "VirtualProductionDialog")
        super(VirtualProductionDialog, self).__init__(parent=parent, title="Virtual Production", dockable=True)
        self._dock.closeEvent = self.closeEvent
        self._dock.resize(self._settings.value("size", QtCore.QSize(900, 500)))
        self._virtualProductionWidget = VirtualProductionWidget()
        self._virtualProductionWidget.restoreState(self._settings.value("settings", {}) or {})
        self._virtualProductionWidget.windowNameChange.connect(self._handleWindowNameChange)
        self._virtualProductionWidget.updateWindowName()

        # Create layout and add widget
        self.setCentralWidget(self._virtualProductionWidget)

    def _handleWindowNameChange(self, newWindowName):
        self.setWindowTitle(newWindowName)

    def showEvent(self, event):
        self._dock.restoreGeometry(self._settings.value("geometry"))

    def closeEvent(self, event):
        self._settings.setValue("geometry", self._dock.saveGeometry())
        self._settings.setValue("size", self._dock.size())
        self._settings.setValue("settings", self._virtualProductionWidget.storeState())
        self._virtualProductionWidget.onClose()
        return super(VirtualProductionDialog, self).closeEvent(event)


class VirtualProductionWidget(QtGui.QWidget):
    """Class to generate the widgets and their functions for the Toolbox UI"""

    windowNameChange = QtCore.Signal(object)

    _CONNECT_BTTN_TEXT = "Connect"
    _DISCONNECT_BTTN_TEXT = "Disconnect"

    def __init__(self, parent=None):
        super(VirtualProductionWidget, self).__init__(parent=parent)

        iconPath = os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "Mocap"))
        self._disconnectedIcon = QtGui.QPixmap(os.path.join(iconPath, "VP_Disconnected.png"))
        self._connectedIcon = QtGui.QPixmap(os.path.join(iconPath, "VP_Connected.png"))

        self._assetsUptoDateIcon = QtGui.QPixmap(os.path.join(iconPath, "Assets_UptoDate.png"))
        self._assetsNeedToCheckIcon = QtGui.QPixmap(os.path.join(iconPath, "Assets_NeedToCheck.png"))
        self._assetsOutOfDateIcon = QtGui.QPixmap(os.path.join(iconPath, "Assets_OutOfDatepng.png"))

        timerMins = 5
        timerSecs = 0
        self._assetCheckerTimer = QtCore.QTimer()
        self._assetCheckerTimer.setInterval((timerMins*(60*1000)) + (timerSecs*1000))
        self._assetCheckerTimer.timeout.connect(self._handleAssetTimerUpdate)

        self._docWidgets = {}
        self._watchstarTab = None
        self._stagingTab = None
        self._propToyTab = None
        self._gsSkelMatchTab = None
        self._duringShootTab = None
        self._referencePoseTab = None
        self._layoutTab = None
        self._motionImportTab = None
        self._realtimeConnection = realtimeConnection.RealtimeConnection()
        self.setupUi()
        self._currentlyRefreshing = False
        self.manager = Manager()

    def storeState(self):
        """
        Settings used by dialogs/mainwindows to serialized and used when restoring.

        Returns:
            dict
        """
        stageID = None
        stage = self.getCurrentMocapLocationStage()
        if stage is not None:
            stageID = stage.stageId
        return {
            _SettingsKeys.DOCK_STATE: self._dockHolderWidget.saveState(),
            _SettingsKeys.INCLUDE_OFFSITE_STAGES: self.ac_optionsIncludeOffsiteStages.isChecked(),
            _SettingsKeys.LOCATION_STAGE: stageID,
            _SettingsKeys.CAPTUREBOSS_IP_ADDRESS: self._mocapStageServerAddress.text(),
            _SettingsKeys.DISPLAY_CAPTUREBOSS_IP_ADDRESS: self.ac_optionsDisplayServerAddressEntry.isChecked(),
            _SettingsKeys.CAPTUREBOSS_CONNECTED_STATE: self._realtimeConnection.isConnected(),
            _SettingsKeys.MOTION_IMPORT: self._motionImportTab.storeState(),
        }

    def restoreState(self, settings):
        """
        Apply previously serialized settings.

        Args:
            settings (dict): All data needed to restore previous state.
        """
        value = settings.get(_SettingsKeys.DOCK_STATE, None)
        if value is not None:
            self._dockHolderWidget.restoreState(value)

        value = settings.get(_SettingsKeys.INCLUDE_OFFSITE_STAGES)
        if value is not None:
            self.ac_optionsIncludeOffsiteStages.setChecked(value)
            self._handleOptionsIncludeOffsiteStages()

        value = settings.get(_SettingsKeys.LOCATION_STAGE, None)
        if value is not None:
            self.setCurrentMocapLocationStage(Context.animData.getStageFromID(
                value, includeLocal=True, includeOffsite=True, includeNonActive=False)
            )

        value = settings.get(_SettingsKeys.CAPTUREBOSS_IP_ADDRESS, None)
        if value is not None:
            self._mocapStageServerAddress.setText(value)

        value = settings.get(_SettingsKeys.DISPLAY_CAPTUREBOSS_IP_ADDRESS, None)
        if value is not None:
            self.ac_optionsDisplayServerAddressEntry.setChecked(value)
            self._handleOptionsDisplayServerAddressEntry()

        value = settings.get(_SettingsKeys.CAPTUREBOSS_CONNECTED_STATE, False)
        if value is True:
            self._connect()

        value = settings.get(_SettingsKeys.MOTION_IMPORT)
        if value is not None:
            self._motionImportTab.restoreState(value)

    def onClose(self):
        if self._duringShootTab is not None:
            self._duringShootTab.onClose()

    def onStageChange(self):
        if self._currentlyRefreshing is True:
            return
        currentStageModel = None
        currentStage = self.activeStageComboBox.currentText()
        for stage in self.getStageList():
            if stage.Name == str(currentStage):
                currentStageModel = stage
                break
        self._stagingTab.setCurrentStage(currentStageModel)
        self._propToyTab.setCurrentStage(currentStageModel)

    def refreshUI(self, rebuildStageSpecificModels=False):
        # refresh tab info where applicable
        self.populateStageComboBox()

        # staging
        self._stagingTab.refreshUi()

        # proptoys
        self._propToyTab.populateDriverTreeList()
        self._propToyTab.populateScenePropToyComboBoxList()

        # gs skel match
        self._gsSkelMatchTab.resetTree()

        # reference pose
        self._referencePoseTab.resetScrollArea()
        self._referencePoseTab.populateTreeView()

        # layout
        self._layoutTab.populateSlateComboBox()
        self._layoutTab.resetTree()

        # motion import
        self._motionImportTab.populateSlateComboBox()
        self._motionImportTab.resetTree()

        # Giant Device Manager
        self._giantDeviceMangerTab.refreshData()

        # GS Handlers tab
        self._gsHandlersTab.refreshData()

        if rebuildStageSpecificModels is True:
            self._propToyTab.populateTreeView()
            self._stagingTab.populateBlockingModelsList()
            self._stagingTab.populateBuildAssetsComboBox()

        self.updateWindowName()

    def updateWindowName(self):
        coreProjectName = Config.CoreProject.Name
        currentProjectName = Config.Project.Name
        projectStr = coreProjectName
        if coreProjectName == currentProjectName:
            projectStr = "{}".format(coreProjectName)
        else:
            projectStr = "{}({})".format(coreProjectName, currentProjectName)
        self.windowNameChange.emit("Virtual Production - {}".format(projectStr))

    def getStageList(self):
        modelList = []
        stageList = []
        # model list is only up to 2 children - stages shouldnt be parented to anything, but we are
        # going to search a few children deep just in case
        for model in Globals.Scene.RootModel.Children:
            modelList.append(model)
            for subChild in [child for child in model.Children]:
                modelList.append(subChild)
                modelList.extend([child for child in subChild.Children])
        # iterate through model list to find stage(s)
        for model in modelList:
            stageProperty = model.PropertyList.Find('Stages')
            if stageProperty:
                stageList.append(model)
        return stageList

    def populateStageComboBox(self):
        self._currentlyRefreshing = True
        self.activeStageComboBox.clear()
        stageList = self.getStageList()
        activeStage = self.getActiveStage(stageList)
        comboBoxItemList = [item.LongName for item in stageList if item != activeStage]
        if activeStage is not None:
            activeStageName = activeStage.LongName
            comboBoxItemList = [activeStageName] + comboBoxItemList + ["Not Set"]
        else:
            comboBoxItemList = ["Not Set"] + comboBoxItemList
        self.activeStageComboBox.clear()
        self.activeStageComboBox.addItems(comboBoxItemList)
        self.activeStageComboBox.setCurrentIndex(0)
        self._currentlyRefreshing = False

    def setActiveStage(self):
        if self._currentlyRefreshing is True:
            return

        activeStage = None

        currentActiveStageSelection = str(self.activeStageComboBox.currentText())
        for stage in self.getStageList():
            activeProperty = stage.PropertyList.Find('Active Stage')
            if activeProperty != None:
                if stage.LongName == currentActiveStageSelection:
                    activeProperty.Data = True
                    activeStage = stage
                else:
                    activeProperty.Data = False

        if activeStage is None or self.manager.IsReferenced(Component.GetNamespace(activeStage)) is False:
            return

        reference = self.manager.GetReferenceByNamespace(Component.GetNamespace(activeStage))
        reference.SnapMocapConstraints()

    def getActiveStage(self, stageList):
        for stage in stageList:
            activeProperty = stage.PropertyList.Find('Active Stage')
            if activeProperty and activeProperty.Data == True:
                activeStage = stage
                return activeStage
        return None

    def _handleRenameStage(self):
        # get current take's name and active stage
        takeName = mobu.FBSystem().CurrentTake.Name
        stageList = self.getStageList()
        activeStage = self.getActiveStage(stageList)
        if activeStage is not None:
            activeStageNamespace = Component.GetNamespace(activeStage)
            if activeStageNamespace is None:
                activeStageNamespace = activeStage.Name
            reference = self.manager.IsReferenced(activeStageNamespace)
            if reference is not True:
                activeStageComponentList = []
                stageMaterial = None
                # find all component related to the stage and add to a list
                for idx in range(activeStage.GetSrcCount()):
                    activeStageComponentList.append(activeStage.GetSrc(idx))
                    if isinstance(activeStage.GetSrc(idx), mobu.FBMaterial):
                        stageMaterial = activeStage.GetSrc(idx)
                for idx in range(activeStage.GetDstCount()):
                    activeStageComponentList.append(activeStage.GetDst(idx))
                activeStageComponentList.append(activeStage)
                # get the components which arent picked up through a src/dst connection
                # handle
                for handle in Globals.Handles:
                    if handle.Name == activeStage.Name:
                        activeStageComponentList.append(handle)
                # folder
                for folder in Globals.Folders:
                    if folder.Name == activeStage.Name or folder.Name == '{0} 1'.format(activeStage.Name):
                        activeStageComponentList.append(folder)
                # textures
                if stageMaterial:
                    stageTexture = stageMaterial.GetTexture()
                    if stageTexture:
                        activeStageComponentList.append(stageTexture)
                # rename all components
                for component in activeStageComponentList:
                    component.Name = component.Name.replace(activeStageNamespace, takeName)
                # re activate stage
                activeProperty = activeStage.PropertyList.Find('Active Stage')
                activeProperty.Data = True
            else:
                self.manager.ChangeNamespace(self.manager.GetReferenceByNamespace(activeStageNamespace), takeName)

            # reset active stage list with new names
            self.populateStageComboBox()

    def _handleRealtimeConnectionStatusChange(self, val):
        if val is True:
            self._realtimeConnectionStatus.setPixmap(self._connectedIcon)
            self._realtimeConnectionStatus.setToolTip("Connected to CaptureBoss")
        else:
            self._realtimeConnectionStatus.setPixmap(self._disconnectedIcon)
            self._realtimeConnectionStatus.setToolTip("Not Currently Connected to CaptureBoss")

    def _setupActions(self):
        """Setup all the actions and connections."""
        # Options Actions
        self.ac_optionsDisplayServerAddressEntry = QtGui.QAction("Display Stage Address", self)
        self.ac_optionsDisplayServerAddressEntry.setCheckable(True)
        self.ac_optionsDisplayServerAddressEntry.setChecked(False)
        self.ac_optionsDisplayServerAddressEntry.triggered.connect(self._handleOptionsDisplayServerAddressEntry)
        self.ac_optionsIncludeOffsiteStages = QtGui.QAction("Include Offsite Stages", self)
        self.ac_optionsIncludeOffsiteStages.setCheckable(True)
        self.ac_optionsIncludeOffsiteStages.triggered.connect(self._handleOptionsIncludeOffsiteStages)

        # Tools Actions
        self.ac_mocapAssetEditorWindow = QtGui.QAction("Mocap Asset Editor Tool", self)
        self.ac_mocapAssetEditorWindow.triggered.connect(self._handleToolsMocapAssetEditorWindow)

        # Test Plan Actions
        self.ac_testShootPrepPage = QtGui.QAction("Test Plan (Shoot Prep)", self)
        self.ac_testShootPrepPage.triggered.connect(self._handleShootPrepPage)
        self.ac_testShootDayPage = QtGui.QAction("Test Plan (Shoot Day)", self)
        self.ac_testShootDayPage.triggered.connect(self._handleShootDayPage)

    def _setupTabs(self):
        """Create tab widgets/layout"""
        self._watchstarTab = watchstarWidget.WatchstarWidget()
        self.__watchstarDockWidget = self._buildDockWidget("Watchstar", self._watchstarTab)

        self._stagingTab = stagingWidget.StagingWidget()
        self.__stagingDockWidget = self._buildDockWidget("Previz: Staging && Pre-Shoot", self._stagingTab)

        self._propToyTab = propToyWidget.PropToyWidget()
        self.__propToyDockWidget = self._buildDockWidget("Previz: PropToys", self._propToyTab)

        self._gsSkelMatchTab = gsSkelMatchWidget.GSSkeletonMatchWidgets()
        self.__gsSkelMatchDockWidget = self._buildDockWidget("Previz: GS Skel Match", self._gsSkelMatchTab)

        self._duringShootTab = duringShootWidget.DuringShootWidget()
        self.__duringShootDockWidget = self._buildDockWidget("Shoot: During Shoot", self._duringShootTab)

        self._spectatorModeTab = spectatorModeWidget.SpectatorModeWidget()
        self.__spectatorModeDockWidget = self._buildDockWidget("Shoot: Spectator Mode", self._spectatorModeTab)

        self._referencePoseTab = referencePoseWidget.ReferencePoseWidget()
        self.__referencePoseDockWidget = self._buildDockWidget("Shoot: Reference Pose", self._referencePoseTab)

        self._layoutTab = layoutWidget.LayoutWidget()
        self.__layoutDockWidget = self._buildDockWidget("Post: Layout", self._layoutTab)

        self._motionImportTab = motionImportWidget.MotionImportWidget()
        self.__motionImportDockWidget = self._buildDockWidget("Post: Motion Import", self._motionImportTab)

        self._giantDeviceMangerTab = giantDeviceManagerWidget.GiantDeviceManagerWidget()
        self.__giantDeviceMangerDockWidget = self._buildDockWidget("Giant Device Manager", self._giantDeviceMangerTab)

        self._gsHandlersTab = gsHandlersWidget.GsHandleWidget()
        self.__gsHandlersDockWidget = self._buildDockWidget("Shoot: GS Handlers", self._gsHandlersTab)

        # Assign Layouts to Widget
        for wid in self._docWidgets.itervalues():
            self._dockHolderWidget.addDockWidget(QtCore.Qt.BottomDockWidgetArea, wid)

        # Setup Windows Menu
        optionsMenu = self._menuBar.addMenu("Options")
        self._addActionsToMenu(
            optionsMenu,
            [self.ac_optionsDisplayServerAddressEntry, self.ac_optionsIncludeOffsiteStages],
        )

        toolsMenu = self._menuBar.addMenu("Tools")
        self._addActionsToMenu(toolsMenu, [self.ac_mocapAssetEditorWindow])

        windowsMenu = self._menuBar.addMenu("Windows")
        for widget in self._docWidgets.values():
            windowsMenu.addAction(widget.toggleViewAction())

        testPlansMenu = self._menuBar.addMenu("Test Plans")
        self._addActionsToMenu(testPlansMenu, [self.ac_testShootPrepPage, self.ac_testShootDayPage])

        self._dockHolderWidget.tabifiedDockWidgets(self.__stagingDockWidget)

        self._dockHolderWidget.tabifyDockWidget(self.__stagingDockWidget, self.__watchstarDockWidget)
        self._dockHolderWidget.tabifyDockWidget(self.__stagingDockWidget, self.__propToyDockWidget)
        self._dockHolderWidget.tabifyDockWidget(self.__stagingDockWidget, self.__gsSkelMatchDockWidget)
        self._dockHolderWidget.tabifyDockWidget(self.__stagingDockWidget, self.__giantDeviceMangerDockWidget)
        self._dockHolderWidget.tabifyDockWidget(self.__stagingDockWidget, self.__duringShootDockWidget)
        self._dockHolderWidget.tabifyDockWidget(self.__stagingDockWidget, self.__spectatorModeDockWidget)
        self._dockHolderWidget.tabifyDockWidget(self.__stagingDockWidget, self.__referencePoseDockWidget)
        self._dockHolderWidget.tabifyDockWidget(self.__stagingDockWidget, self.__layoutDockWidget)
        self._dockHolderWidget.tabifyDockWidget(self.__stagingDockWidget, self.__motionImportDockWidget)

        self.__stagingDockWidget.raise_()

        self._dockHolderWidget.setDockOptions(QtGui.QMainWindow.AllowTabbedDocks |
                            QtGui.QMainWindow.AllowNestedDocks | QtGui.QMainWindow.AnimatedDocks)
        self._dockHolderWidget.setTabPosition(QtCore.Qt.AllDockWidgetAreas, QtGui.QTabWidget.North)

    def _setupDialogs(self):
        # Mocap Asset Editor Window
        configFile = const.PrevizFolder.getAssetSettingsPath()
        if not os.path.exists(configFile):
            self.ac_mocapAssetEditorWindow.setEnabled(False)

    def _addActionsToMenu(self, menuToAdd, actions):
        """
        Internal Method

        Helper method to add a list of actions to a menu
        if an action in the list of actions is None, then a separator is added
        """
        for action in actions:
            if action is None:
                menuToAdd.addSeparator()
                continue
            menuToAdd.addAction(action)

    def _buildDockWidget(self, name, widget):
        widget.setObjectName("{} widget".format(name))
        newDockWidget = QtGui.QDockWidget(name)
        newDockWidget.setWidget(widget)
        newDockWidget.setObjectName(name)
        newDockWidget.setFeatures(QtGui.QDockWidget.AllDockWidgetFeatures)
        widget.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        self._docWidgets[name] = newDockWidget
        return newDockWidget

    def _handleToolsMocapAssetEditorWindow(self):
        configFile = const.PrevizFolder.getAssetSettingsPath()
        if os.path.exists(configFile):
            mocapAssetEditorWindow = mocapAssetEditorTool.MocapAssetEditorTool(configFile, parent=self)
            mocapAssetEditorWindow.show()

    def _handleShootPrepPage(self):
        """Handle the Test Plan (Shoot Prep) button press"""
        webbrowser.open(const.AppInfo.SHOOT_PREP_TEST_PLAN_URL)
    def _handleShootDayPage(self):
        """Handle the Test Plan (Shoot Day) button press"""
        webbrowser.open(const.AppInfo.SHOOT_DAY_TEST_PLAN_URL)

    def _handleOptionsDisplayServerAddressEntry(self):
        self._mocapStageServerAddressLabel.setVisible(self.ac_optionsDisplayServerAddressEntry.isChecked())
        self._mocapStageServerAddress.setVisible(self.ac_optionsDisplayServerAddressEntry.isChecked())

    def _handleOptionsIncludeOffsiteStages(self):
        self._stageModel.includeOffsite(self.ac_optionsIncludeOffsiteStages.isChecked())
        self._stageModel.includeLocalLocation(self.ac_optionsIncludeOffsiteStages.isChecked())

    def stageLockIcon(self):
        lockIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "lock.png"))
        return lockIcon

    def stageUnLockIcon(self):
        unlockIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "unlock.png"))
        return unlockIcon

    def setStageLockState(self):
        isLocked = self.getCurrentLockState()
        if isLocked:
            # stages are currently locked, we need to unlock them
            for stage in self.getStageList():
                transformProperty = stage.PropertyList.Find('Enable Transformation')
                if transformProperty:
                    transformProperty.Data = True
        else:
            # stages are currently unlocked, we need to lock them
            for stage in self.getStageList():
                transformProperty = stage.PropertyList.Find('Enable Transformation')
                if transformProperty:
                    transformProperty.Data = False
        self.setStageLockIcon()

    def getCurrentLockState(self):
        locked = False
        for stage in self.getStageList():
            transformProperty = stage.PropertyList.Find('Enable Transformation')
            if transformProperty and transformProperty.Data==False:
                locked = True
                break
        return locked

    def setStageLockIcon(self):
        isLocked = self.getCurrentLockState()
        if isLocked:
            self.stageLockButton.setIcon(self.stageLockIcon())
            self.stageLockButton.setToolTip("All Stages are Locked")
        else:
            self.stageLockButton.setIcon(self.stageUnLockIcon())
            self.stageLockButton.setToolTip("All Stages are Unlocked")

    def setupToolBar(self, banner):
        """
        Adds icon to the banner

        Arguments:
            banner (RS.Tools.UI.QBanner): banner to add tool buttons to
        """
        refreshIconPath = os.path.join(Config.Script.Path.ToolImages, "ReferenceEditor", "Refresh.png")
        refreshButton = QtGui.QIcon(QtGui.QPixmap(refreshIconPath))
        banner.addButton(refreshButton , "Refreshes the Virtual Production Toolbox", self.refreshUI)

    def _handleAssetTimerUpdate(self):
        self._updateAssetStatus(None)

    def _handleAssetStatusPress(self):
        if self._assetCheckerTimer.isActive():
            self._assetCheckerTimer.stop()

        for asset in self.manager.GetReferenceListAll():
            asset.UpdateReferenceP4Version()
            if (asset.CurrentVersion < asset.LatestVersion) is True:
                self._updateAssetStatus(False)
                return

        self._updateAssetStatus(True)

    def _handleCaptureBossPress(self):
        pass
#         if self._realtimeConnectionStatus.
#         self._realtimeConnection.connectToHost(ipAddress)

    def _updateAssetStatus(self, assetStatus):
        if assetStatus is True:
            icon = self._assetsUptoDateIcon
            toolTip = "All Referenced Assets are upto Date"
        elif assetStatus is False:
            icon = self._assetsOutOfDateIcon
            toolTip = "Referenced Assets need to be updated"
        elif assetStatus is None:
            icon = self._assetsNeedToCheckIcon
            toolTip = "Asset might be out of date"

        self._assetStatus.setPixmap(icon)
        self._assetStatus.setToolTip(toolTip)
        self._assetCheckerTimer.start()

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        bannerLayout = QtGui.QGridLayout()
        topLayout = QtGui.QHBoxLayout()
        gsStageLayout = QtGui.QGridLayout()
        assetStatusLayout = QtGui.QVBoxLayout()
        wvCamLayout = QtGui.QVBoxLayout()
        mocapStageLayout = QtGui.QGridLayout()
        captureBossStatusLayout = QtGui.QVBoxLayout()

        # set layout properties
        bannerLayout.setVerticalSpacing(0)
        mainLayout.setContentsMargins(0, 0, 0, 0)

        # create widgets
        self._mocapLocationWidget = QtGui.QComboBox()
        self._mocapLocationStageWidget = QtGui.QComboBox()
        self._mocapStageServerAddressLabel = QtGui.QLabel("IP Address:")
        self._mocapStageServerAddress = QtGui.QLineEdit()

        self._dockHolderWidget = QtGui.QMainWindow()
        banner = UI.BannerWidget(name="", helpUrl=r"https://hub.gametools.dev/display/RPERFCAP/Previs%3A+Virtual+Production+Toolbox")
        activeStageLabel = QtGui.QLabel('Active Stage:')
        self._realtimeConnectionStatus = ClickableLabel()

        self._assetStatus = ClickableLabel()
        self.activeStageComboBox = QtGui.QComboBox()
        stageLockLabel = QtGui.QLabel('Stages Locker:')
        self.stageLockButton = QtGui.QPushButton()
        stageRenamerButton = QtGui.QPushButton('Rename to Stage to Current Take Name')
        self._menuBar = QtGui.QMenuBar()
        self._connectButton = QtGui.QPushButton(self._CONNECT_BTTN_TEXT)
        wvCamButtonLaunch = QtGui.QPushButton("Launch")

        gsStageGroupBox = QtGui.QGroupBox("GS Stage")
        self._mocapStageGroupBox = QtGui.QGroupBox("Mocap Stage")
        wvCamGroupBox = QtGui.QGroupBox("WV Cam")
        assetStatusGroupBox = QtGui.QGroupBox("Asset Status")
        captureBossStatusGroupBox = QtGui.QGroupBox("CaptureBoss")

        self._stageModel = stageModel.StageModel()

        # setup widget info
        self._dockHolderWidget.setObjectName("DockHolder")
        self.populateStageComboBox()
        self.setStageLockIcon()
        self.stageLockButton.setMaximumSize(QtCore.QSize(26, 20))
        self.stageLockButton.setIconSize(QtCore.QSize(22, 22))
        self._setupActions()
        self._setupTabs()
        self._setupDialogs()
        self.onStageChange()
        self._realtimeConnectionStatus.setAlignment(QtCore.Qt.AlignCenter)
        self._assetStatus.setAlignment(QtCore.Qt.AlignCenter)
        self._dockHolderWidget.setCorner(QtCore.Qt.TopLeftCorner, QtCore.Qt.TopDockWidgetArea)
        self._dockHolderWidget.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.setStyleSheet("QMenuBar {background: #ffdc19;"
                                "font: bold;"
                                "color: black;}"
                           "QToolBar {background: #ffdc19;"
                                "font: bold;"
                                "color: black;}")
        self.setupToolBar(banner)
        self._updateAssetStatus(None)

        self._mocapLocationWidget.setModel(self._stageModel)
        self._mocapLocationStageWidget.setModel(self._stageModel)
        self._mocapLocationStageWidget.setRootModelIndex(self._stageModel.index(0, 0, QtCore.QModelIndex()))
        self._mocapLocationStageWidget.setCurrentIndex(-1)

        self._mocapStageServerAddressLabel.hide()
        self._mocapStageServerAddress.hide()

        # add widgets to layouts
        mocapStageLayout.addWidget(QtGui.QLabel("Location:"), 0, 0)
        mocapStageLayout.addWidget(self._mocapLocationWidget, 0, 1)
        mocapStageLayout.addWidget(QtGui.QLabel("Stage:"), 1, 0)
        mocapStageLayout.addWidget(self._mocapLocationStageWidget, 1, 1)
        mocapStageLayout.addWidget(self._mocapStageServerAddressLabel, 2, 0)
        mocapStageLayout.addWidget(self._mocapStageServerAddress, 2, 1)

        wvCamLayout.addWidget(wvCamButtonLaunch)
        assetStatusLayout.addWidget(self._assetStatus)
        captureBossStatusLayout.addWidget(self._realtimeConnectionStatus)
        captureBossStatusLayout.addWidget(self._connectButton)

        topLayout.addWidget(gsStageGroupBox)
        topLayout.addWidget(self._mocapStageGroupBox)
        topLayout.addSpacerItem(QtGui.QSpacerItem(15, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Maximum))
        topLayout.addWidget(wvCamGroupBox)
        topLayout.addWidget(assetStatusGroupBox)
        topLayout.addWidget(captureBossStatusGroupBox)

        gsStageLayout.addWidget(activeStageLabel, 0, 0)
        gsStageLayout.addWidget(self.activeStageComboBox, 0, 1, 1, 3)
        gsStageLayout.addWidget(stageRenamerButton, 0, 4)
        gsStageLayout.addWidget(stageLockLabel, 1, 0)
        gsStageLayout.addWidget(self.stageLockButton, 1, 1)

        bannerLayout.addWidget(banner, 0, 0, 1, 2)
        bannerLayout.addWidget(self._menuBar, 1, 0)

        mainLayout.addLayout(bannerLayout, 1, 0, 1, 2)
        mainLayout.addLayout(topLayout, 2, 0)
        mainLayout.addWidget(self._dockHolderWidget, 3, 0)

        # set layout
        wvCamGroupBox.setLayout(wvCamLayout)
        assetStatusGroupBox.setLayout(assetStatusLayout)
        captureBossStatusGroupBox.setLayout(captureBossStatusLayout)
        gsStageGroupBox.setLayout(gsStageLayout)
        self._mocapStageGroupBox.setLayout(mocapStageLayout)

        self.setLayout(mainLayout)

        # callbacks
        self._duringShootTab.connectionStatusChanged.connect(self._handleRealtimeConnectionStatusChange)
        self.activeStageComboBox.currentIndexChanged.connect(self.onStageChange)
        self.activeStageComboBox.currentIndexChanged.connect(self.setActiveStage)
        self.stageLockButton.pressed.connect(self.setStageLockState)
        self._stagingTab.stageSignal.connect(self.populateStageComboBox)
        stageRenamerButton.pressed.connect(self._handleRenameStage)
        self._handleRealtimeConnectionStatusChange(self._duringShootTab.isConnected())
        self._assetStatus.clicked.connect(self._handleAssetStatusPress)
        self._realtimeConnectionStatus.clicked.connect(self._handleCaptureBossPress)
        self._mocapLocationWidget.currentIndexChanged.connect(self._handleMocapLocationPicked)
        self._mocapLocationStageWidget.currentIndexChanged.connect(self._handleMocapLocationStagePicked)
        self._connectButton.pressed.connect(self._handleConnectButton)
        wvCamButtonLaunch.pressed.connect(self._handleWvCamButtonLaunch)

        self.updateWindowName()

        # Realtime connection.
        self._mocapStageServerAddress.setText(self._realtimeConnection.currentIpAddress())
        if self._realtimeConnection.isConnected() is True:
            self._setUiConnect()
        else:
            self._setUiDisconnect()

    def _handleWvCamButtonLaunch(self):
        appPath = os.path.join("X:\\", "applications", "ILM", "WVcam", "2.7.5", "wvcam.exe")
        wvCamLibsPath = os.path.join("X:\\", "Program Files", "WVcam", "python", "lib", "site-packages", "numpy", ".libs")
        if wvCamLibsPath not in sys.path:
            sys.path.append(wvCamLibsPath)
        if not os.path.exists(appPath):
            msg = "Unable to find wvcam at: \n'{}'\nPlease check that its been setup correctly".format(appPath)
            QtGui.QMessageBox.critical(self, "Virtal Production Toolbox", msg)
            return
        QtCore.QProcess.startDetached(appPath)

    def _disconnect(self):
        """
        Internal Method

        Disconnect from the server
        """
        self._setUiDisconnect()
        self._realtimeConnection.disconnectFromHost()

    def _connect(self):
        """
        Internal Method

        Connect to a server
        """
        self._realtimeConnection.connectToHost(self._mocapStageServerAddress.text())
        self._setUiConnect()

    def _handleConnectButton(self):
        """
        Internal Method

        Handle a connection button press
        """
        if self._connectButton.text() == self._CONNECT_BTTN_TEXT:
            self._connect()
        else:
            self._disconnect()

    def _setUiConnect(self):
        """
        Internal Method

        Set the UI to a connected state
        """
        self._mocapStageGroupBox.setEnabled(False)
        self._connectButton.setText(self._DISCONNECT_BTTN_TEXT)

    def _setUiDisconnect(self):
        """
        Internal Method

        Set the UI to a disconnected state
        """
        self._mocapStageGroupBox.setEnabled(True)
        self._connectButton.setText(self._CONNECT_BTTN_TEXT)

    def _handleMocapLocationPicked(self, newIndex):
        idx = self._stageModel.index(newIndex, 0, QtCore.QModelIndex())
        self._mocapLocationStageWidget.setRootModelIndex(idx)
        self._mocapLocationStageWidget.setCurrentIndex(-1)

    def _handleMocapLocationStagePicked(self, newIndex):
        idx = self._stageModel.index(newIndex, 0, self._mocapLocationStageWidget.rootModelIndex())
        stage = idx.data(QtCore.Qt.UserRole)
        self._stagingTab.setMocapStage(stage)

        # Update the PrevizFolder singleton first as the proptoy widget's model uses it to query for assets.
        const.PrevizFolder().setCurrentStage(stage)

        if stage is None:
            self.refreshUI(rebuildStageSpecificModels=True)
            self._mocapStageServerAddress.setText("0.0.0.0")
        else:
            self.refreshUI(rebuildStageSpecificModels=True)
            self._mocapStageServerAddress.setText(stage.captureWorkstationIp())

        self._spectatorModeTab.setCurrentStage(stage)

    def getCurrentMocapLocationStage(self):
        return self._mocapLocationStageWidget.itemData(self._mocapLocationStageWidget.currentIndex(), QtCore.Qt.UserRole)

    def setCurrentMocapLocationStage(self, stage):
        idx = self._stageModel.findStageContextIndex(stage)

        self._mocapLocationWidget.setCurrentIndex(idx.parent().row())
        self._mocapLocationStageWidget.setCurrentIndex(idx.row())

