from PySide import QtGui, QtCore

import os
import re

import pyfbsdk as mobu

from RS import Globals
from RS.Tools.VirtualProduction import const
from RS.Tools.VirtualProduction.models import motionImportModelTypes
from RS.Core.Mocap import Clapper, MocapCommands
from RS.Core.VirtualProduction import processScene
from RS.Utils import Scene
from RS.Core.Animation import Lib
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.Face import FacewareLive
from RS.Core.Camera import CameraLocker, CamUtils
from RS.Core.Scene.models import takeModelTypes
from RS.Core.Scene.models.modelItems.ComponentModelItem import ComponentItem


class MotionImportWidget(QtGui.QWidget):
    """Class to generate the widgets, and their functions, for the Toolbox UI"""
    def __init__(self, parent=None):
        super(MotionImportWidget, self).__init__(parent=parent)
        self.setupUi()

    def _handleSelectAll(self):
        for child in self._mainModel.rootItem.children():
            idx = self._mainModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Checked
            self._mainModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self._mainModel.dataChanged.emit(index0, index1)

    def _handleDeselectAll(self):
        for child in self._mainModel.rootItem.children():
            idx = self._mainModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Unchecked
            self._mainModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self._mainModel.dataChanged.emit(index0, index1)

    def _setEnableOnCheckBoxes(self):
        '''
        Sets the enable properties of the timeshift-related widgets, based off
        of which checkbox is active.
        '''
        if self.autoShiftCheckbox.isChecked():
            self.slateListCombobox.setEnabled(True)
            self.frameCombobox.setEnabled(True)
            self.frameSpinBox.setEnabled(False)
        else:
            self.slateListCombobox.setEnabled(False)
            self.frameCombobox.setEnabled(False)
            self.frameSpinBox.setEnabled(True)

    def _getGSAssetInfoDict(self):
        '''
        Generates the GS Asset info List.

        Returns:
            list [dict]: List of GS asset info where key is the GS asset's null and value
            is a tuple of the default path, GS asset index, GS asset, and GS type.
        '''
        assetInfoList = []
        gsSlateInfoDict = {}
        gsCharacterInfoDict = {}
        gsPropInfoDict = {}

        # get items from model
        for child in self._mainModel.rootItem.children():
            if child.GetCheckedState():
                # get index of selected item
                itemRow = self._mainModel.rootItem.childItems.index(child)
                childIndex = child.createIndex(itemRow, 1, child)

                # get item null, default path, and asset type
                itemRoot = child.GetGSAssetRoot()
                defaultPath = child.GetDefaultPath()
                assetType = child.GetGSType()

                # add info to dict
                if assetType == const.GSModelTypes.Slate:
                    gsSlateInfoDict[itemRoot] = defaultPath, childIndex, child, assetType
                if assetType == const.GSModelTypes.Character:
                    gsCharacterInfoDict[itemRoot] = defaultPath, childIndex, child, assetType
                if assetType == const.GSModelTypes.Prop:
                    gsPropInfoDict[itemRoot] = defaultPath, childIndex, child, assetType

        # add dicts to a list
        assetInfoList.append(gsSlateInfoDict)
        assetInfoList.append(gsCharacterInfoDict)
        assetInfoList.append(gsPropInfoDict)
        return assetInfoList

    def _getNonGSCharacters(self):
        '''
        Finds all of the non-gs characters in the scene

        Returns:
            List (FBCharacter): list of character nodes found, which are not gs-related
        '''
        characterModelList = []
        for character in Globals.Characters:
            if 'gs' not in character.Name and not re.match("^M[0-9]+", character.Name):
                characterModelList.append(character)
        return characterModelList

    def _handleTakesComboBoxIndexChanged(self):
        '''
        When the take combo box index changes, set the current take in Mobu to be what is selected.
        '''
        selectedTake = self._takesComboBox.itemData(self._takesComboBox.currentIndex(), role=QtCore.Qt.UserRole)
        if selectedTake is not None and selectedTake != Globals.System.CurrentTake:
            Globals.System.CurrentTake = selectedTake

    def _handleAddTimecodeHudToCurrentCamera(self):
        """Add a HUD for post-production. This HUD displays the timecode stored on the SlateClapper_bone."""
        MocapCommands.addTimecodeHudToCurrentCamera()

    def _handleRunPostProcessPrevizSave(self):
        """Run PostProcessPrevizSave and display a dialog with the results."""
        reportData = processScene.Run(updateWatchstar=False)
        QtGui.QMessageBox.information(None, "PostProcessPrevizSave", "\n".join(reportData))

    def _handleTakesModelReset(self):
        '''
        When the model is reset, change the index to be the current take.
        '''
        take = Globals.System.CurrentTake
        if take is not None:
            index = list(Globals.Takes).index(take)
            self._takesComboBox.setCurrentIndex(index)

    def _handleMotionImport(self):
        '''
        Motion import used to bring in clean data onto selected assets, from the UI.
        User selects the path to the clean data file for each asset, via a QFileDialog
        '''
        assetInfoList = self._getGSAssetInfoDict()
        try:
            for assetDictionary in assetInfoList:
                for key, value in assetDictionary.iteritems():
                    key.Name
        except:
            QtGui.QMessageBox.warning(self, "Motion Import Tool Error", "Please refresh the UI for the current scene's data.")
            return

        # Deselect all scene items
        Scene.DeSelectAll()

        # Motion import for each selected item
        for assetDictionary in assetInfoList:
            for key, value in assetDictionary.iteritems():
                defaultPath, index, _, gsType = value
                if self._newTakeRadioButton.isChecked():
                    defaultPath = os.path.dirname(value[0])

                # Select gs item's root/bone
                key.Selected = True

                # User selects the file to motion import to selected
                fileName = None;
                filePath = None
                filePopup = QtGui.QFileDialog()
                filePopup.setWindowTitle("{0}".format(key.LongName.upper()))
                filePopup.setViewMode(QtGui.QFileDialog.List)
                filePopup.setNameFilters(["*.fbx", "*.FBX"])
                filePopup.setDirectory(defaultPath)

                if filePopup.exec_() and len(filePopup.selectedFiles()) > 0:
                    filePath = filePopup.selectedFiles()[0]
                    fileInfo = QtCore.QFileInfo(filePath)
                    fileName = str(fileInfo.baseName())
                    # Normalise path and turn into a string
                    filePath = str(os.path.normpath(filePath))

                    if gsType == const.GSModelTypes.Slate:
                        # Import slate
                        Globals.Application.FileImport(filePath, True, False)
                        self.populateSlateComboBox()
                    else:
                        # Import motion
                        Globals.Application.FileImport(filePath)

                # Deselect gs item's root/bone
                key.Selected = False

                # Create properties with user selected clean data path and name
                if fileName and filePath:
                    cleanDataPathProperty = key.PropertyCreate('Clean Data Path',
                                                               mobu.FBPropertyType.kFBPT_charptr,
                                                               "", False,
                                                               True,
                                                               None)
                    cleanDataPathProperty.Data = filePath
                    cleanDataNameProperty = key.PropertyCreate('Clean Data Name',
                                                               mobu.FBPropertyType.kFBPT_charptr,
                                                               "",
                                                               False,
                                                               True,
                                                               None)
                    cleanDataNameProperty.Data = fileName

                if index != None:
                    self._mainModel.setData(index, fileName, QtCore.Qt.EditRole)
                    self._mainModel.setData(index, filePath, QtCore.Qt.ToolTipRole)
                    dataValue = False
                    self._mainModel.setData(index, dataValue, QtCore.Qt.CheckStateRole)

        # Deselect any row which may be selected
        self.treeView.clearSelection()

    def _handlePlotCharactersToSkeleton(self):
        '''
        Plots all non-gs characters to their skeletons
        '''
        characterModelList = self._getNonGSCharacters()
        for character in characterModelList:
            plotOptions = mobu.FBPlotOptions()
            plotOptions.PlotOnFrame = True
            plotOptions.UseConstantKeyReducer = False
            plotOptions.PlotAllTakes = True
            plotOptions.PlotTranslationOnRootOnly = True
            plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll
            plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)

            if character.ActiveInput == True:
                character.PlotAnimation(mobu.FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, plotOptions)
                character.ActiveInput == False

    def _handlePlotAll(self):
        '''
        Plots characters, props and vehicles.
        Will only find and plot the models that have references.
        '''
        # plot character models
        self._handlePlotCharactersToSkeleton()

        # plot everything else
        manager = Manager()
        nonCharacterReferenceList = []
        # deselect all
        Scene.DeSelectAll()
        # get references
        referenceList = manager.GetReferenceListAll()
        for reference in referenceList:
            # only plot for prop and vehicle references
            if reference.FormattedTypeName in ['Prop', 'Vehicle']:
                referenceNamespace = reference.Name
                for component in Globals.Components:
                    if '{0}:'.format(referenceNamespace) in component.LongName and isinstance(component, mobu.FBModel):
                        nonCharacterReferenceList.append(component)
                        break
        # iterate through found references, select all items in their heirarchy
        heirarchyList = []
        for reference in nonCharacterReferenceList:
            parent = Scene.GetParent(reference)
            Scene.GetChildren(parent, heirarchyList, "", False)
        # select items in heirarchy
        for item in heirarchyList:
            item.Selected = True
        # plot selected
        Scene.Plot.PlotCurrentTakeonSelected()
        # deselect all
        Scene.DeSelectAll()

    def _handleNewTakeSelection(self):
        # Disables take combo box if user chooses to make a new take
        self._takesComboBox.setEnabled(False)

    def _handleExistingTakeSelection(self):
        # Enables combo box that allows a user to select an existing take
        self._takesComboBox.setEnabled(True)

    def ZeroToSlateCheckBoxes(self):
        if self.manualGenerateCheckBox.checkState() == QtCore.Qt.CheckState.Checked:
            self.frameSpinBox.setEnabled(True)
            self.frameComboBox.setEnabled(False)
            self.slateComboBox.setEnabled(False)
        else:
            self.frameComboBox.setEnabled(True)
            self.slateComboBox.setEnabled(True)
            self.frameSpinBox.setEnabled(False)

    def populateSlateComboBox(self):
        self.slateComboBox.clear()
        clapsDict = Clapper.GetClaps()
        if len(clapsDict) == 0:
            return
        for key in clapsDict.iterkeys():
            self.slateComboBox.addItem(key.LongName)
        self.PopulateBeepsFromSlate()

    def PopulateBeepsFromSlate(self):
        """
        Get all the bleeps in the slate and populate the combobox with their frame numbers
        """
        slateString = self.slateComboBox.currentText()
        if slateString == None:
            return
        self.frameComboBox.clear()
        clapsDict = Clapper.GetClaps()
        if len(clapsDict) == 0:
            return
        for key, value in clapsDict.iteritems():
            if str(slateString) == key.LongName:
                for clap in value:
                    self.frameComboBox.addItem(str(clap))

    def ZeroToSlate(self):
        # unlock camera properties
        lockManager = CameraLocker.LockManager()
        lockManager.setAllCamLocks(locked=False)

        # time shift
        if self.autoGenerateCheckBox.isChecked() == True:
            frame = self.frameComboBox.currentText()
            if frame != "":
                MocapCommands.ZeroOutScene(int(frame))

        if self.manualGenerateCheckBox.isChecked() == True:
            frame = self.frameSpinBox.value()
            MocapCommands.ZeroOutScene(frame)

        # lock camera properties
        lockManager.setAllCamLocks(locked=True)

    def resetTree(self):
        self._mainModel.reset()

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        timeShiftLayout = QtGui.QGridLayout()
        motionImportGroupBoxGridLayout = QtGui.QGridLayout()
        plotGroupBoxGridLayout = QtGui.QGridLayout()
        zeroGroupBoxGridLayout = QtGui.QGridLayout()
        tidyGroupBoxGridLayout = QtGui.QGridLayout()

        # create widgets
        motionImportGroupBox = QtGui.QGroupBox()
        self._mainModel =  motionImportModelTypes.MotionImportModel()
        self.treeView = QtGui.QTreeView()
        self.treeView.setModel(self._mainModel)
        selectAllPushButton = QtGui.QPushButton()
        deselectAllPushButton = QtGui.QPushButton()
        plotGroupBox = QtGui.QGroupBox()
        motionImportPushButton = QtGui.QPushButton()
        plotPushButton = QtGui.QPushButton()
        plotCharactersPushButton = QtGui.QPushButton()
        plotSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        zeroGroupBox = QtGui.QGroupBox()
        self.autoGenerateCheckBox = QtGui.QCheckBox()
        self.slateComboBox = QtGui.QComboBox()
        self.frameComboBox = QtGui.QComboBox()
        self.manualGenerateCheckBox = QtGui.QCheckBox()
        self.frameSpinBox = QtGui.QSpinBox()
        timeShiftButton = QtGui.QPushButton()
        zeroButtonGroup = QtGui.QButtonGroup(self)
        self._takesModel = takeModelTypes.TakeModel(register=True)
        self._takesComboBox = QtGui.QComboBox()
        self._newTakeRadioButton = QtGui.QRadioButton("Create New Take")
        self._existingTakeRadioButton = QtGui.QRadioButton("Use Existing Take")

        tidyGroupBox = QtGui.QGroupBox("Tidy Scene")
        addTimecodeHud = QtGui.QPushButton("Add Timecode HUD to Current Camera")
        postProcessPrevizSaveButton = QtGui.QPushButton("Run PostProcessPrevizSave")
        bakeCamsButton = QtGui.QPushButton("Bake RTC to New Cameras")
        swapSwitcherButton = QtGui.QPushButton("Swap Switcher to New Cameras")
        plotMocapButton = QtGui.QPushButton("Plot Mocap Assets")
        cleanPrevizButton = QtGui.QPushButton("Delete Base Previz Objects")
        exportRedMarkupButton = QtGui.QPushButton("Save Environment Red Markup Data as 2014/15")
        self._timeShiftFaceSpinBox = QtGui.QSpinBox()
        timeShiftFaceButton = QtGui.QPushButton("Plot && Timeshift Faceware")

        # widget properties
        self.treeView.setAlternatingRowColors(True)
        self.treeView.setStyleSheet("QTreeView {\nalternate-background-color: rgb(56, 56, 56); \nbackground-color: rgb(43, 43, 43);\n} \n")
        self.treeView.setSortingEnabled(True)
        self.treeView.header().setSortIndicator(0, QtCore.Qt.AscendingOrder)
        motionImportPushButton.setText("Bring in Clean Data for Selected")
        plotPushButton.setText("Plot all")
        selectAllPushButton.setText("Select all")
        deselectAllPushButton.setText("Deselect all")
        plotPushButton.setToolTip("Plots all non-gs models")
        plotCharactersPushButton.setText("Plot all Characters to Skeleton")
        plotPushButton.setToolTip("Plots all character models (non-gs), props and vehicles to skeleton")
        motionImportGroupBox.setTitle("Motion Import")
        plotGroupBox.setTitle("Plotting")
        zeroGroupBox.setTitle("Zero to Slate")
        self.autoGenerateCheckBox.setText("Auto-generate frame(s)")
        self.manualGenerateCheckBox.setText("Manually set frame")
        timeShiftButton.setText("Timeshift Scene")
        self.frameSpinBox.setMinimum(0)
        self.frameSpinBox.setMaximum(99999.0)
        self.frameSpinBox.setValue(0)
        self.autoGenerateCheckBox.setCheckState(QtCore.Qt.CheckState.Checked)
        self.populateSlateComboBox()
        self.frameSpinBox.setEnabled(False)
        zeroButtonGroup.addButton(self.autoGenerateCheckBox)
        zeroButtonGroup.addButton(self.manualGenerateCheckBox)
        self._takesComboBox.setModel(self._takesModel)
        self._existingTakeRadioButton.setChecked(True)

        self._timeShiftFaceSpinBox.setMinimum(-10000.0)
        self._timeShiftFaceSpinBox.setMaximum(10000.0)
        self._timeShiftFaceSpinBox.setSingleStep(1)
        self._timeShiftFaceSpinBox.setValue(-5)

        # add widgets to layout
        motionImportGroupBoxGridLayout.addWidget(self._existingTakeRadioButton, 0, 0, 1, 1)
        motionImportGroupBoxGridLayout.addWidget(self._newTakeRadioButton, 0, 3, 1, 1)
        motionImportGroupBoxGridLayout.addWidget(self._takesComboBox, 1, 0, 1, 6)
        motionImportGroupBoxGridLayout.addWidget(self.treeView, 2, 0, 1, 6)
        motionImportGroupBoxGridLayout.addWidget(selectAllPushButton, 3, 0)
        motionImportGroupBoxGridLayout.addWidget(deselectAllPushButton, 3, 1)
        motionImportGroupBoxGridLayout.addWidget(motionImportPushButton, 4, 0, 1, 6)
        plotGroupBoxGridLayout.addWidget(plotCharactersPushButton, 0, 0)
        plotGroupBoxGridLayout.addWidget(plotPushButton, 1, 0)
        plotGroupBoxGridLayout.addItem(plotSpacerItem, 2, 0)
        zeroGroupBoxGridLayout.addWidget(self.autoGenerateCheckBox, 0, 0, 1, 2)
        zeroGroupBoxGridLayout.addWidget(self.slateComboBox, 1, 0)
        zeroGroupBoxGridLayout.addWidget(self.frameComboBox, 1, 1)
        zeroGroupBoxGridLayout.addWidget(self.manualGenerateCheckBox, 2, 0, 1, 2)
        zeroGroupBoxGridLayout.addWidget(self.frameSpinBox, 3, 0)
        zeroGroupBoxGridLayout.addWidget(timeShiftButton, 4, 0, 1, 2)

        tidyGroupBoxGridLayout.addWidget(addTimecodeHud, 0, 0, 1, 2)
        tidyGroupBoxGridLayout.addWidget(bakeCamsButton, 1, 0, 1, 2)
        tidyGroupBoxGridLayout.addWidget(postProcessPrevizSaveButton, 2, 0, 1, 2)
        tidyGroupBoxGridLayout.addWidget(swapSwitcherButton, 3, 0, 1, 2)
        tidyGroupBoxGridLayout.addWidget(plotMocapButton, 4, 0, 1, 2)
        tidyGroupBoxGridLayout.addWidget(cleanPrevizButton, 5, 0, 1, 2)
        tidyGroupBoxGridLayout.addWidget(exportRedMarkupButton, 6, 0, 1, 2)
        tidyGroupBoxGridLayout.addWidget(self._timeShiftFaceSpinBox, 7, 0)
        tidyGroupBoxGridLayout.addWidget(timeShiftFaceButton, 8, 1)

        # set groupBox layout
        motionImportGroupBox.setLayout(motionImportGroupBoxGridLayout)
        plotGroupBox.setLayout(plotGroupBoxGridLayout)
        zeroGroupBox.setLayout(zeroGroupBoxGridLayout)
        tidyGroupBox.setLayout(tidyGroupBoxGridLayout)

        # add groupbox to mainlayout
        mainLayout.addWidget(motionImportGroupBox, 0, 0, 3, 2)
        mainLayout.addWidget(zeroGroupBox, 0, 2)
        mainLayout.addWidget(plotGroupBox, 1, 2)
        mainLayout.addWidget(tidyGroupBox, 2, 2)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        motionImportPushButton.released.connect(self._handleMotionImport)
        plotCharactersPushButton.released.connect(self._handlePlotCharactersToSkeleton)
        plotPushButton.released.connect(self._handlePlotAll)
        selectAllPushButton.released.connect(self._handleSelectAll)
        deselectAllPushButton.released.connect(self._handleDeselectAll)
        self.manualGenerateCheckBox.stateChanged.connect(self.ZeroToSlateCheckBoxes)
        self.autoGenerateCheckBox.stateChanged.connect(self.ZeroToSlateCheckBoxes)
        self.slateComboBox.currentIndexChanged.connect(self.PopulateBeepsFromSlate)
        timeShiftButton.released.connect(self.ZeroToSlate)
        self._newTakeRadioButton.toggled.connect(self._handleNewTakeSelection)
        self._existingTakeRadioButton.toggled.connect(self._handleExistingTakeSelection)
        self._takesModel.modelReset.connect(self._handleTakesModelReset)
        self._takesComboBox.activated.connect(self._handleTakesComboBoxIndexChanged)

        addTimecodeHud.released.connect(self._handleAddTimecodeHudToCurrentCamera)
        bakeCamsButton.released.connect(CamUtils.DupeRtcsToRsCams)
        postProcessPrevizSaveButton.released.connect(self._handleRunPostProcessPrevizSave)
        swapSwitcherButton.released.connect(MocapCommands.SwapCamRocksInSwitcher)
        plotMocapButton.released.connect(MocapCommands.PlotMocapAssets)
        cleanPrevizButton.released.connect(MocapCommands.DeleteBasePreviz)
        exportRedMarkupButton.released.connect(self._exportRedMarkupForEnvironments)
        timeShiftFaceButton.released.connect(self._plotTimeShiftFaceware)

        # set the take combo box to have the current take
        self._handleTakesModelReset()

    def storeState(self):
        """
        Settings used by dialogs/mainwindows to serialized and used when restoring.

        Returns:
            dict
        """
        return {
            "HeaderState":self.treeView.header().saveState()
        }

    def restoreState(self, settings):
        """
        Apply previously serialized settings.

        Args:
            settings (dict): All data needed to restore previous state.
        """
        value = settings.get("HeaderState")
        if value is not None:
            self.treeView.header().restoreState(value)

    def _exportRedMarkupForEnvironments(self):
        """
        run UI for saving out red markups and environment to a new file for environment team
        url:bugstar:3351472
        """
        from RS.Tools.RedMarkupExport import run
        run.Run()

    def _plotTimeShiftFaceware(self):
        frame = self._timeShiftFaceSpinBox.value()
        facewareConstraintClass = FacewareLive.FacewareConstraint()
        facewareConstraintClass.TimeShift(frame)
