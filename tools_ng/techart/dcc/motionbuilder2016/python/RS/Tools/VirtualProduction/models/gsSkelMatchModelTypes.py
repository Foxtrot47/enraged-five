from PySide import QtCore, QtGui

import pyfbsdk as mobu

import os
import re
import glob

from RS import Config, Globals, ProjectData
from RS.Tools.VirtualProduction import const
from RS.Tools.UI.Mocap.Models import gsSkelMatchModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class GSSkelInfoModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    def __init__(self, parent=None):
        self._defaultGSSkelPath = None
        self._defaultGSSkelName = ProjectData.data.GetDefaultGSSkelName()
        self._gsSkeletonNameList = []
        self._gsSkeletonPathList = []
        super(GSSkelInfoModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ['Character Name', 'GS Skeleton Match', 'Actor Name', 'Actor Color']

    def _getCharacterList(self):
        '''
        returns: a list of FBCharacter nodes in the scene
        '''
        characterList = []
        for character in Globals.Characters:
            if 'gs' in character.Name or re.match("^M[0-9]+", character.Name):
                continue
            characterList.append(character)
        return characterList

    def _getGSSkelLists(self):
        '''
        returns: a dict of gsSkel Names and paths
        '''
        # paths to gs skel files
        gsPath = '{0}\\*.fbx'.format(ProjectData.data.GetGsSkeletonPath())

        # using local files for this. Having problems accessing depot files with the mocap server.
        tempFileList = glob.glob(gsPath)
        for filePath in tempFileList:
            #if '_gs' in filePath: WAITING ON BUG 2329722
            # make a list of all gs skel path names
            itemSplitName = str(filePath).split('\\')
            fbxNameSplit = itemSplitName[-1].split('.')
            gsSkelString = fbxNameSplit[0]
            # append gsSkeleton lists
            if gsSkelString.lower().endswith('_gs') or re.match("^M[0-9]+", gsSkelString):
                self._gsSkeletonNameList.append(gsSkelString)
                self._gsSkeletonPathList.append(filePath)

    def GetGsSkelNameList(self):
        '''
        returns a list of strings
        '''
        return self._gsSkeletonNameList

    def findIndexByChild(self, item):
        '''
        returns index (int)
        '''
        return self.childItems.index(item)

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        gsSkeletonInformationDict = {}
        self._getGSSkelLists()
        characterList = self._getCharacterList()

        for character in characterList:
            gsSkelMatched = False
            characterNamespace = character.LongName.split(":")[0]

            # Get the character heights to display in the characterName column in parenthesis.
            height = None
            height = re.search("\d{3}cm", character.Name)

            if not height:
                inputCharacter = character.InputCharacter
                if inputCharacter:
                    inputCharacterName = inputCharacter.Name
                    height = re.search("m\d{3}", inputCharacterName)

            # Some characters don't have heights specified. If not then leave as is.
            if height:
                characterHeight = height.group(0)
                characterHeight = re.findall(r"(\d+)", characterHeight)[0]
                characterNamespace = "{0}  ({1} cm)".format(characterNamespace, characterHeight)

            # check if a gs skel has already been matched with this character and has a color
            actorName = None
            actorColorString = 'Black'
            characterHips = character.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
            if characterHips:
                actorNamespaceProperty = characterHips.PropertyList.Find('gs match namespace')
                actorColorStringProperty = characterHips.PropertyList.Find('actor color string')
                if actorNamespaceProperty:
                    actorNamespace = actorNamespaceProperty.Data
                    actorNamespaceSplit = actorNamespace.split(':')
                    actorName = actorNamespaceSplit[0]
                if actorColorStringProperty:
                    actorColorString = actorColorStringProperty.Data

            # double check color string as color names changed
            colorNameDict = const.ColorNameChange.colorNameChangeDict

            for key, value in colorNameDict.iteritems():
                if key == actorColorString:
                    actorColorString = value

            # get matching gs name
            for gsSkelName in self._gsSkeletonNameList:
                if ('-' in character.Name):
                    character.Name = character.Name.split('-')[0]
                if '{0}_'.format(character.Name) in gsSkelName:
                    gsSkelMatched = True
                    # create dataItem for treeview
                    dataItem = gsSkelMatchModelItem.gsSkelMatchTextModelItem(
                                                                             characterNamespace,
                                                                             character,
                                                                             gsSkelName,
                                                                             self._gsSkeletonPathList,
                                                                             actorName,
                                                                             actorColorString,
                                                                             parent=parent
                                                                            )
                    # append tree view with item
                    parent.appendChild(dataItem)
                    break
            if not gsSkelMatched:
                # if character name doesnt match any gs skel name, use default name 'M01'
                dataItem = gsSkelMatchModelItem.gsSkelMatchTextModelItem(
                                                                        characterNamespace,
                                                                         character,
                                                                         self._defaultGSSkelName,
                                                                         self._gsSkeletonPathList,
                                                                         actorName,
                                                                         actorColorString,
                                                                         parent=parent
                                                                        )
                # append tree view with item
                parent.appendChild(dataItem)

    def flags(self, index):
        '''
        flags added to determine column properties

        args: index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        column = index.column()
        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        if column == 0:
            return flags | QtCore.Qt.ItemIsUserCheckable
        elif column in [1, 2, 3]:
            return flags | QtCore.Qt.ItemIsEditable
        else:
            return flags
