from PySide import QtCore, QtGui

from RS.Utils import Scene
from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem
from RS.Tools.VirtualProduction import const


class _MotionImportTextModelItem(textModelItem.TextModelItem):
    """Text Item to display text in columns for the model"""
    def __init__(self, referenceObject, defaultPath, parent=None):
        super(_MotionImportTextModelItem, self).__init__([], parent=parent)
        
        self._referenceObject = referenceObject
        self._defaultPath = defaultPath
        gsAssetRoot, motionImportPath, motionImportName = self._processReference(referenceObject)
        
        self._gsAssetName = "{}:{}".format(referenceObject.Namespace, referenceObject.Name)
        self._gsAssetRoot = gsAssetRoot
        self._motionImportName = motionImportName
        self._motionImportPath = motionImportPath
        self._checked = False

    @classmethod
    def _processReference(cls, referenceAsset):
        raise NotImplementedError("Method not implemented!")

    def SetCheckedState(self, boolean):
        '''
        sets check state to False for item
        '''
        self._checked = boolean

    def GetCheckedState(self):
        '''
        Return:
            Checked State (Boolean): current check state of gs asset's checkbox in ui
        '''
        return self._checked

    def GetGSAssetName(self):
        '''
        Return:
            Asset Name(String): name of the gs asset found in the scene
        '''
        return self._gsAssetName

    def GetGSAssetRoot(self):
        '''
        Return:
            Root Asset(FBModel): Parent/Root of the hierarchy for the gs asset found in the scene
        '''
        return self._gsAssetRoot

    def GetDefaultPath(self):
        '''
        Return:
            Path (string): path to point to for the popup's initial folder
        '''
        return self._defaultPath

    @staticmethod
    def GetGSType():
        '''
        Return:
            Type (string): the type of gs asset the object is
        '''
        raise NotImplementedError("Method not implemented!")

    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._gsAssetName
            elif column == 1:
                return self.GetGSType()
            elif column == 2:
                return self._motionImportName

        elif role == QtCore.Qt.ToolTipRole:
            if column == 1:
                return self._motionImportPath

        elif role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if self._checked:
                    return QtCore.Qt.CheckState.Checked
                else:
                    return QtCore.Qt.CheckState.Unchecked
        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        model data updated based off of user interaction with the view
        '''
        column = index.column()
        if role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if value == QtCore.Qt.CheckState.Checked:
                    self._checked = True
                else:
                    self._checked = False
                return True

        elif role == QtCore.Qt.EditRole:
            if column == 1:
                self._motionImportName = value
                return True

        elif role == QtCore.Qt.ToolTipRole:
            if column == 1:
                self._motionImportPath = value
                return True
        return False



class MotionImportSlateItem(_MotionImportTextModelItem):
    @classmethod
    def _processReference(cls, referenceObject):
        refItems= referenceObject.GetReferenceModels()
        for item in refItems:
            if item.Name.lower() in ['slate_bone', 'slateclapper_bone']:
                # check if model already has clean data applied
                cleanDataPathProperty = item.PropertyList.Find('Clean Data Path')
                cleanDataNameProperty = item.PropertyList.Find('Clean Data Name')
                if cleanDataPathProperty:
                    # add info to dict
                    return item, cleanDataPathProperty.Data, cleanDataNameProperty.Data
                else:
                    return item, "", ""
    
    @staticmethod
    def GetGSType():
        return const.GSModelTypes.Slate


class MotionImportCharacterItem(_MotionImportTextModelItem):
    @classmethod
    def _processReference(cls, referenceObject):
        gsSkelString = 'skel_root'
        gsSkelPropertyString = 'RTSource'
        gsSkelPropertyString2 = 'GiantAnim'
        gsSkelPropertyString3 = 'character match namespace'
        
        skelRoots = [item for item in referenceObject.GetReferenceModels() if gsSkelString in item.Name.lower()]
        for item in skelRoots:
            rtSourceProperty = item.PropertyList.Find(gsSkelPropertyString)
            rtSourceProperty3 = item.PropertyList.Find(gsSkelPropertyString3)
            if rtSourceProperty or rtSourceProperty3:
                parentOffset = item.Parent
                # double check parentOffset is the top of the hierarchy, if it isnt, it has
                # picked a non-gs model and ignore it!
                rootParent = Scene.GetParent(item)
                if parentOffset != rootParent:
                    continue
                
                if parentOffset and parentOffset.Children[0]:
                    # check if model already has clean data applied
                    cleanDataPathProperty = parentOffset.Children[0].PropertyList.Find('Clean Data Path')
                    cleanDataNameProperty = parentOffset.Children[0].PropertyList.Find('Clean Data Name')
                    if cleanDataPathProperty:
                        # add info to dict
                        return parentOffset.Children[0], cleanDataPathProperty.Data, cleanDataNameProperty.Data
                    else:
                        # add info to dict
                        return parentOffset.Children[0], "", ""
        return "", "", ""

    @staticmethod
    def GetGSType():
        return const.GSModelTypes.Character


class MotionImportPropItem(_MotionImportTextModelItem):
    @classmethod
    def _processReference(cls, referenceObject):
        topItems = [item for item in referenceObject.GetReferenceModels() if item.Parent is None and len(item.Children) > 0]
        for item in topItems:
            rootItem = item.Children[0]
            cleanDataPathProperty = rootItem.PropertyList.Find('Clean Data Path')
            cleanDataNameProperty = rootItem.PropertyList.Find('Clean Data Name')
            if cleanDataPathProperty:
                return rootItem, cleanDataPathProperty.Data, cleanDataNameProperty.Data
        if not topItems:
            return "", "", ""
        return topItems[0].Children[0], "", ""

    @staticmethod
    def GetGSType():
        return const.GSModelTypes.Prop
