from PySide import QtCore, QtGui

import pyfbsdk as mobu

from RS import Globals
from RS.Core.Mocap import MocapCommands
from RS.Core.ReferenceSystem import Manager
from RS.Tools import UI
from RS.Tools.VirtualProduction.models import giantDeviceManagerModel


class _GiantErrors(object):
    NO_DEVICE_IN_SCENE = 0
    MORE_THAN_ONE_DEVICE_IN_SCENE = 1
    DEVICE_NOT_ONLINE = 2


class GiantDeviceComboBoxDelegate(QtGui.QItemDelegate):
    """
    Creates a combobox delgate where the items are the Giant Device Attachment names
    """
    _CLEAR_ITEM_TEXT = "<Clear>"
    refreshConnetions = QtCore.Signal()
    
    def __init__(self, parent=None):
        super(GiantDeviceComboBoxDelegate, self).__init__(parent=parent)

    def createEditor(self, parent, option, index):
        """
        Creates QCombobox and sets the items
        """
        editor = QtGui.QComboBox(parent)
        editor.addItem(self._CLEAR_ITEM_TEXT, None)
        attachDict = MocapCommands.GiantDeviceManager.GetAttachedObjects()
        attachPoints = attachDict.keys()
        attachPoints.sort()
        for key in attachPoints:
            name = key
            value = attachDict.get(key)
            if value is not None:
                name = "{} - Assinged to '{}'".format(key, value.LongName)
            editor.addItem(name, key)
        return editor

    def setEditorData(self, comboBox, index):
        """
        Reads the value from the Model Data and sets the current index of the combobox accordingly
        
        Model Data -> ComboBox
        """
        value = index.model().data(index, QtCore.Qt.DisplayRole)
        for idx in xrange(comboBox.count()):
            if comboBox.itemData(idx, QtCore.Qt.UserRole) == value:
                comboBox.setCurrentIndex(idx)
                break

    def setModelData(self, comboBox, model, index):
        """
        Reads the current text of the combobox and pushes that information to the Model Data
        
        ComboBox -> Model SetData
        """
        value = comboBox.itemData(comboBox.currentIndex(), QtCore.Qt.UserRole)
        model.setData(index, value, QtCore.Qt.EditRole)
        self.refreshConnetions.emit()

    def updateEditorGeometry(self, editor, option, index):
        """
        Sets the combobox's position
        """
        editor.setGeometry(option.rect)


class GiantDeviceManagerWidget(QtGui.QWidget):
    """
    The widget which holds the UI and Model for the Giant Device Manager Widget Tool
    """
    _DEFAULT_MAPPING_DIR = r"X:\virtualproduction\GiantMapping"
    
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(GiantDeviceManagerWidget, self).__init__(parent=parent)
        self._realtimeAttachments = {}
        self._errorEnum = None
        self.setupUi()
    
    def setupUi(self):
        # Layouts
        layout = QtGui.QVBoxLayout()
        self._warningLayout = QtGui.QHBoxLayout()
        buttonLayout = QtGui.QHBoxLayout()
        
        # Widgets
        self._warningText = QtGui.QLabel("<NO WARNING>")
        self._warningButton = QtGui.QPushButton("Resolve")
        self._warningWidget = QtGui.QWidget()
        self._mainView = QtGui.QTreeView()
        saveMappingButton = QtGui.QPushButton("Save Mapping")
        loadMappingButton = QtGui.QPushButton("Load Mapping")
        
        # Models
        self._mainModel = giantDeviceManagerModel.GiantDevicesBaseModel()
        self._proxyModel = QtGui.QSortFilterProxyModel()
        self._giantAttachmentComboDelegate = GiantDeviceComboBoxDelegate()
        
        # configure widgets
        self._warningText.setAlignment(QtCore.Qt.AlignCenter)
        self._warningButton.setSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Maximum)
        self._warningWidget.setObjectName("warningWidget")
        self._warningWidget.setAttribute(QtCore.Qt.WA_StyledBackground, True)
        self.setStyleSheet("QWidget#warningWidget{background-color:transparent; background-color: rgb(255, 128, 0);}")
        
        self._proxyModel.setSourceModel(self._mainModel)
        colIdx = self._mainModel.getHeadings().index(giantDeviceManagerModel.ColumnRoles.NAME)
        self._proxyModel.setFilterKeyColumn(colIdx)

        colIdx = self._mainModel.getHeadings().index(giantDeviceManagerModel.ColumnRoles.GIANT_ATTACHMENT)
        self._mainView.setItemDelegateForColumn(colIdx, self._giantAttachmentComboDelegate)
        self._mainView.setSortingEnabled(True)

        # configure models
        self._mainView.setModel(self._proxyModel)
        self._mainView.setAlternatingRowColors(True)

        # Assign Widgets to Layouts
        self._warningLayout.addWidget(self._warningText)
        self._warningLayout.addWidget(self._warningButton)
        
        buttonLayout.addWidget(saveMappingButton)
        buttonLayout.addWidget(loadMappingButton)
        
        layout.addWidget(self._warningWidget)
        layout.addWidget(self._mainView)
        layout.addLayout(buttonLayout)
        
        # Apply Layouts to Widgets
        self._warningWidget.setLayout(self._warningLayout)
        self.setLayout(layout)
        
        # Setup Connections
        self._giantAttachmentComboDelegate.refreshConnetions.connect(self._handleRefreshGiantConnections)
        self._warningButton.released.connect(self._resolveIssues)
        saveMappingButton.released.connect(self._handleSaveMapping)
        loadMappingButton.released.connect(self._handleLoadMapping)
        
        self._populateView()
        self._checkForErrors()

    def _handleSaveMapping(self):
        fileName, _ = QtGui.QFileDialog.getSaveFileName(self, "Giant Mapping Load", self._DEFAULT_MAPPING_DIR, "Giant Mapping (*.json)")
        if fileName is not None:
            MocapCommands.GiantDeviceManager.ExportMappingToFile(fileName)
        
    def _handleLoadMapping(self):
        fileName, _ = QtGui.QFileDialog.getOpenFileName(self, "Giant Mapping Load", self._DEFAULT_MAPPING_DIR, "Giant Mapping (*.json)")
        if fileName is not None:
            MocapCommands.GiantDeviceManager.ImportMappingFromFile(fileName)
            self.refreshData()

    def _setError(self, text, errorEnum):
        self._errorEnum = errorEnum
        self._warningText.setText(text)
        self._warningWidget.show()

    def _clearError(self):
        self._errorEnum = 0
        self._warningWidget.hide()

    def _addAndSetupDevice(self):
        giantDevice = MocapCommands.AddGiantDevice()
        # Setup and configure it

    def _resolveIssues(self):
        if self._errorEnum == _GiantErrors.NO_DEVICE_IN_SCENE:
            self._addAndSetupDevice()
        elif self._errorEnum == _GiantErrors.MORE_THAN_ONE_DEVICE_IN_SCENE:
            MocapCommands.DeleteGiantDevice()
            self._addAndSetupDevice()
        elif self._errorEnum == _GiantErrors.DEVICE_NOT_ONLINE:
            MocapCommands.GiantDeviceOn()
            self.refreshData()
        
        # Check at the end
        self._checkForErrors()

    def _checkForErrors(self):
        # Get all the giant devices
        giantDevices = MocapCommands.GetGiantDevices()
        if len(giantDevices) == 0:
            self._setError("No Giant Device in scene, click to add one", _GiantErrors.NO_DEVICE_IN_SCENE)
            return
        if len(giantDevices) > 1:
            self._setError("Too many Giant Device in scene, click to clear and re-add them", _GiantErrors.MORE_THAN_ONE_DEVICE_IN_SCENE)
            return
        if giantDevices[0].Online is False:
            self._setError("Giant Device is Offline, click to turn it on", _GiantErrors.DEVICE_NOT_ONLINE)
            return
        self._clearError()

    def _populateView(self):
        """
        Internal Method
        
        Re-set the model and re-query the data for the model
        """
        self._mainModel.reset()
        self._checkForErrors()

    def _handleRefreshGiantConnections(self):
        """
        Internal Method
        
        Handle the refreshing the Giant Connections when values in the drop down delegates changes
        """
        self._mainModel.refreshGiantAttachments()
        self._checkForErrors()

    def refreshData(self):
        """
        Re-sets the model
        """
        self._populateView()
        self._checkForErrors()


def Run():
    mainWin = UI.QtMainWindowBase()
    widget = GiantDeviceManagerWidget()
    mainWin.setCentralWidget(widget)
    mainWin.show()
