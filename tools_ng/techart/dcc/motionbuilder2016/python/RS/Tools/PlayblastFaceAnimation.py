"""
## Summary : This module will setup playblast render for face review.
## Summary    : Ik Spline in MotionBuilder.
## bugTracker : url:bugstar:2589504 .

##Description
    We need a script that sets and runs a quick Motionbuilder playblast render of the scene for when 
    lead animators in TC approve a 2nd pass animation 
    so we have a much faster way of reviewing this in case 2nd pass is lacking.
"""

import os
import tempfile
import uuid
import shutil
import subprocess

import pyfbsdk as mobu
from RS.Utils import Playblast
import RS.Config 
from RS.Core.Video import videoManager

__author__ = 'cedric.bazillou@rockstarnorth.com'
__version__ = '0.0.2'

def buildFaceCamera(resolutionWidth,
                    resolutionHeight,
                    LeadName):
    """
        Creates a camera for facial animation review
        Args:  
            resolutionWidth (int): Width value
            resolutionHeight(int): Height value
    """
    reviewCamera = mobu.FBCamera('faceReview_camera1')

    reviewCamera.Visible = True
    reviewCamera.Show = True
    
    #Now align on current view 
    sourceMatrix = mobu.FBMatrix()
    mobu.FBSystem().Scene.Renderer.CurrentCamera.GetMatrix(sourceMatrix)
    reviewCamera.SetMatrix(sourceMatrix)
    
    
    reviewCamera.PropertyList.Find('DisplaySafeArea').Data = True
    reviewCamera.UseFrameColor = True
    reviewCamera.FrameColor = mobu.FBColor(0,0,0)
    #ResolutionMode
    
    reviewCamera.PropertyList.Find('UseAntialiasing').Data = True
    reviewCamera.PropertyList.Find('CameraFormat').Data = 4
    reviewCamera.PropertyList.Find('ResolutionMode').Data = 2
    reviewCamera.PropertyList.Find('AspectH').Data = resolutionHeight
    reviewCamera.PropertyList.Find('AspectW').Data = resolutionWidth
    
    #Now prepare Hud element
    buildReviewHud(reviewCamera,LeadName)

    #Switch finally the viewport
    mobu.FBSystem().Scene.Renderer.CurrentCamera = reviewCamera
    
def computeResolution(reviewCamera):
    """
        This method defines what resolution to use for the reviewCamera
        Args:  
            reviewCamera (FBCamera): input camera object
    """
    propertyToConnect = reviewCamera.PropertyList.Find('LEAD_LABEL')
    if propertyToConnect is None:
        return [reviewCamera.WindowWidth,reviewCamera.WindowHeight,False]
    else:
        return [reviewCamera.PropertyList.Find('AspectW').Data,
                reviewCamera.PropertyList.Find('AspectH').Data,
                True]

def buildReviewHud(reviewCamera,
                   LeadName):
    """
        This method will add a hud elements to the input camera
        Args:  
            reviewCamera (FBCamera): input camera object
            LeadName (str) 
    """
    HUD = mobu.FBHUD("faceReview_Hud1")   
    LeadHud = mobu.FBHUDTextElement('Lead_Hud1')

    mobu.FBSystem().Scene.ConnectSrc(HUD)
    mobu.FBSystem().Scene.ConnectSrc(LeadHud)
    
    LeadHud.Content =  "Lead : {0}".format(LeadName)
    LeadHud.Height = 5.0
    LeadHud.VerticalDock = mobu.FBHUDElementVAlignment.kFBHUDTop
    HUD.ConnectSrc(LeadHud)
    
    reviewCamera.ConnectSrc(HUD)
    metaDataArray = ['FACE_HUD','LEAD_LABEL']
    sourceArray = [HUD,LeadHud]

    for index, metadata in enumerate(metaDataArray):
        propertyToConnect = reviewCamera.PropertyList.Find(metadata)
        if propertyToConnect is None:
            propertyToConnect = reviewCamera.PropertyCreate(metadata,mobu.FBPropertyType.kFBPT_object ,'Object',False,True,None)
        propertyToConnect.ConnectSrc(sourceArray[index])
    
def updateReviewHud(reviewCamera,
                    LeadName):
    """
        This method will update LEAD_LABEL hudTextElement attached to input reviewCamera
        Args:  
            reviewCamera (FBCamera): input camera object
            LeadName (str) 
            
        return LEAD_LABEL.context string property
    """
    propertyToConnect = reviewCamera.PropertyList.Find('LEAD_LABEL')
    if propertyToConnect is not None:
        if propertyToConnect.GetSrcCount() > 0 :
            LeadHud = propertyToConnect.GetSrc(0) 
            LeadHud.Content = 'Lead : {0}'.format(LeadName)

def extractHudData(reviewCamera):
    """
        This method extract the text value of a HudElementText attached to input reviewCamera
        Args:  
            reviewCamera (FBCamera): input camera object
            
        return LEAD_LABEL.context string property
    """
    propertyToConnect = reviewCamera.PropertyList.Find('LEAD_LABEL')
    if propertyToConnect is not None:
        if propertyToConnect.GetSrcCount() > 0 :
            LeadHud = propertyToConnect.GetSrc(0) 
            return LeadHud.Content.replace('Lead : ','')
            
    return None
    
def updateFaceCamera(resolutionWidth,
                     resolutionHeight):
    """
        This method will update the provided camera metadata and hud
        Args:  
            reviewCamera (FBCamera): input camera object
            resolutionWidth   (int): Width value
            resolutionHeight  (int): Height value
    """
    currentCamera = mobu.FBSystem().Scene.Renderer.CurrentCamera
    
    currentCamera.PropertyList.Find('ResolutionMode').Data = 2
    currentCamera.PropertyList.Find('AspectH').Data = resolutionHeight
    currentCamera.PropertyList.Find('AspectW').Data = resolutionWidth

    

