"""
Dialog for catching and reporting exceptions
"""
import os

from PySide import QtGui, QtCore

from RS import Config
from RS.Tools.Animation.Anim2Fbx.Widgets import CollapsableWidget


class Dialog(QtGui.QDialog):

    _defaultComment = "Add comments about the error to include in the email & bug here"
    _pixmap = QtGui.QPixmap(os.path.join(Config.Script.Path.ToolImages, "Bugstar_128x128.png"))

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
        """
        super(Dialog, self).__init__(parent=parent)

        self._set = False
        self._width = 0
        self._previousHeight = None

        layout = QtGui.QVBoxLayout()
        textLayout = QtGui.QVBoxLayout()
        
        self._descriptionLabel = QtGui.QLabel()
        self._errorTextEdit = QtGui.QTextEdit()
        self._commentTextEdit = QtGui.QTextEdit()
        self._errorTextEdit.setReadOnly(True)

        self._commentTextEdit.setText(self._defaultComment)
        self._commentTextEdit.setMinimumHeight(50)

        icon = QtGui.QLabel()
        icon.setPixmap(self._pixmap.scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        self._button = QtGui.QPushButton("Ok")
        self._button.setIconSize(QtCore.QSize(289, 69))
        self._button.setMaximumHeight(50)

        self._button.pressed.connect(self.accept)

        collapsableWidget = CollapsableWidget.CollapsableWidget()
        collapsableWidget.setLength(50)
        collapsableWidget.addWidget(self._errorTextEdit)
        collapsableWidget.preclicked.connect(self.storePreviousSize)
        collapsableWidget.clicked.connect(self.showError)
        collapsableWidget.setText("Show Error")
        collapsableWidget.setClickedText("Hide Error")

        horizontalLayout = QtGui.QHBoxLayout()
        horizontalLayout.addWidget(icon)
        horizontalLayout.addWidget(self._descriptionLabel, 1)
        horizontalLayout.setSpacing(10)
        horizontalLayout.setContentsMargins(10, 5, 10, 5)

        layout.addLayout(horizontalLayout)
        layout.addWidget(collapsableWidget)
        layout.addWidget(self._commentTextEdit, 1)
        layout.addWidget(self._button)

        textLayout.setSpacing(4)
        textLayout.setContentsMargins(4, 4, 4, 0)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        self.setLayout(layout)
        self.setWindowTitle("Rockstar - Tools Error")
        self.setStyleSheet("QLabel {"
                           "font-size: 14px; "
                           "qproperty-alignment: AlignCenter;"
                           "}"
                           "QPushButton {"
                           "font: bold 12px;"
                           "qproperty-alignment: AlignCenter;"
                           "}")

    def showError(self, visible):
        """
        Resizes the window so that the full error is shown

        Arguments:
            visible (boolean): is the error visible
        """
        if visible and not self._set:
            self._errorTextEdit.document().setTextWidth(self._width)
            height = self._errorTextEdit.document().size().height()
            self._errorTextEdit.setMinimumWidth(self._width)
            self._errorTextEdit.setMinimumHeight(height if height <= 1000 else 1000)
            self._errorTextEdit.resize(self._width, height)

        elif not visible:
            self.resize(self.width(), self._previousHeight)

    def storePreviousSize(self, visible):
        """
        Stores the height of the widget before the error was visible

        Arguments:
            visible (boolean): is the error visible
        """
        if not visible:
            self._previousHeight = self.height()

    def description(self):
        """ The description of the dialog """
        return self._descriptionLabel.text()
        
    def setDescripion(self, description):
        """
        Sets the description for the dialog

        Arguments:
            description (string): the description to set
        """
        self._descriptionLabel.setText(description)

    def buttonText(self):
        """ Label of the button """
        return self._button.text()

    def setButtonText(self, text):
        """
        Sets the text of the button
        Arguments:
            text (string): string to set as the label of the button
        """
        self._button.setText(text)

    def text(self):
        """ Error being shown to the user  """
        self._errorTextEdit.text()

    def setText(self, text):
        """
        Sets the text/error as html to show to the user

        Arguments:
            text (string): text/html that contains the error to show to the user
        """
        self._errorTextEdit.insertHtml(text)
        self._errorTextEdit.document().setDocumentMargin(0)

    def fontSize(self):
        """ The font size """
        return self._errorTextEdit.document().defaultFont().pointSize()

    def setFontSize(self, size):
        """ Sets the font size """
        return self._errorTextEdit.document().defaultFont().setPointSize(size)

    def textWidth(self):
        """ The width for error html texteditbox """
        return self._width / self.fontSize()

    def setTextWidth(self, width):
        """ Sets the width for the html text edit box"""
        self._width = width * self.fontSize()

    def comment(self):
        """ Comment from the comment field """
        return self._commentTextEdit.toPlainText()

    def setComment(self, text):
        """
        Sets a comment on the comment field

        Arguments:
            text (string): comment to set on the comment field
        """
        self._commentTextEdit.setText(text)

    def isCommentVisible(self):
        """
        Is the comment edit box visible

        Return:
            Bool
        """
        return self._commentTextEdit.isVisible()

    def setCommentVisible(self, value):
        """
        Sets the visibility of the comment edit box

        Arguments:
            value (bool): visibility of the comment edit box
        """
        self._commentTextEdit.setVisible(value)

    @classmethod
    def exception(cls, parent=None, title="Rockstar - Tools Error",
                  description="An unhandled exception was caught by Rockstar!",  text="", width=100, buttonText="Ok",
                  suppressComment=False):
        """
        Show the exception dialog

        Arguments:
            text (string): error to show in the dialog

        Return:
            tuple(boolean, string)
        """
        dialog = Dialog(parent)
        dialog.setWindowTitle(title)
        dialog.setDescripion(description)
        dialog.setText(text)
        dialog.setTextWidth(width)
        dialog.setButtonText(buttonText)
        dialog.setCommentVisible(not suppressComment)
        # 40 is roughly the size of the comment text widget
        dialog.resize(382, 110 + (40 * int(not suppressComment)))
        dialog.exec_()
        comment = dialog.comment()
        comment = comment if comment.lower().strip() not in cls._defaultComment.lower() else ""
        return bool(dialog.result()), comment