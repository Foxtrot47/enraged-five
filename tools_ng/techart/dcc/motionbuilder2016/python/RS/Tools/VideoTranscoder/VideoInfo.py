'''
Utility to Analyze the video and construct a video stream wrapper

'''




import subprocess, os, json, platform
from RS import Config

LINUX_CONST = "Linux"
WINDOWS_CONST = "Windows"


class StreamBase(object):
    def __init__(self, strm ):
        """
        Base Shared info acoross steram types
        :param strm:
        :return:
        """
        self._index = strm['index']
        self._type = strm['codec_type']
        self._start_time = strm['start_time']
        self._duration = strm['duration']
        self._frames = strm['nb_frames']

    @property
    def index(self):return self._index

    @property
    def type(self):return  self._type

    @property
    def start_time(self):return  self._start_time

    @property
    def duration(self):return  self._duration

    @property
    def frames(self):return  self._frames




class StreamVideo(StreamBase):
    def __init__(self, strm ):
        """
        Stream Video Object inherited from Stream Base
        :param strm:
        :return:
        """
        super(StreamVideo, self).__init__(strm)
        self._codecname = strm['codec_name']
        self._width = strm['width']
        self._height = strm['height']
        self._pix_fmt = strm['pix_fmt']
        self._frame_rate = strm['r_frame_rate']

    @property
    def codecname(self):return self._codecname

    @property
    def width(self):return self._width

    @property
    def height(self):return self._height

    @property
    def pix_fmt(self):return self._pix_fmt

    @property
    def frame_rate(self):return self._frame_rate




class StreamAudio(StreamBase):
    def __init__(self, strm):
        """
        Stream Audio Object inherited from Stream Base
        :param strm:
        :return:
        """
        super(StreamAudio, self).__init__(strm)
        self._codecname = strm['codec_name']
        self._channels = strm['channels']
        self._sample_rate = strm['sample_rate']

    @property
    def codecname(self):return self._codecname

    @property
    def channels(self):return self._channels

    @property
    def sample_rate(self):return self._sample_rate



class StreamMeta(StreamBase):
    def __init__(self,strm):
        """
        Stream Meta Object inherited from Stream Base
        :param strm:
        :return:
        """
        super(StreamMeta, self).__init__(strm)
        self._timecode = None

        if "tags" in strm:
            self._parseTags_(strm['tags'])

    def _parseTags_(self, tags):
        """

        :param tags: pass in our Tags Array
        :return: Nothing
        """

        if 'timecode' in tags:
            self._timecode = tags['timecode']

    @property
    def timecode(self):return self._timecode


class GetVideoInfo(object):
    def __init__(self, filename ):
        """

        :param filename: Media filename
        :return: VideoInfo Container
        """
        self._filename = filename
        self._streams = dict()
        self._GetInfo_()

    @property
    def filename(self):return self._filename

    @property
    def hasStreams(self):return len(self._streams) > 0

    @property
    def streams(self):return self._streams

    def _GetInfo_( self,  ):
        """

        :param filename: Media filename
        :return:    Return a videoInfo object containing info about our streams
        """
        if not os.path.isfile(self._filename): return
        #FFProbe_exe = os.path.join(Config.Tool.Path.Bin, "video", "ffmpeg",Config.AppConfig.FFMPEG, "ffprobe.exe" )
        if platform.system() == LINUX_CONST:
            FFProbe_exe = os.path.join("/usr","bin", "ffprobe")
        else:
            FFProbe_exe = os.path.join("x:\\","RDR3","Tools","Bin","Video","ffmpeg","2_7_0", "ffprobe.exe")

        argsToRun = [FFProbe_exe, self._filename, "-print_format", "json", "-show_streams"]
        proc = subprocess.Popen(argsToRun, stdout=subprocess.PIPE, shell=False)
        output = proc.communicate()
        resultString, _ = output
        jdict = json.loads(resultString)
        if len(jdict) > 0:
            for strm in  jdict['streams']:
                if 'codec_type' in strm:
                    strmMeta = None
                    if strm['codec_type'] == "video":
                        strmMeta = StreamVideo(strm)
                    elif strm['codec_type'] == "audio":
                        strmMeta = StreamAudio(strm)
                    elif strm['codec_type'] == "data":
                        strmMeta = StreamMeta(strm)

                if strmMeta:
                    self._streams[strmMeta.type] = strmMeta







'''
Example Usage:
    from RS.Tools.VideoTranscoder.VideoInfo import GetVideoInfo
    mediaInfo = GetVideoInfo(r'R:\HDTest\HD_Source_bk\Capture0000.mov')
    if mediaInfo.hasStreams:
        print mediaInfo.streams['audio'].index
        print mediaInfo.streams['video'].index
        print mediaInfo.filename
        print mediaInfo.streams['audio'].frames
        print mediaInfo.streams['audio'].duration
        print mediaInfo.streams['data'].index


'''
