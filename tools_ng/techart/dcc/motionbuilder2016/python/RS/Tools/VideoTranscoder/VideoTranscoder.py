import re
import ast
import os
import datetime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import tempfile
import subprocess
import uuid
import socket
import re
from xml.dom import minidom

from RS.Core.IncrediBuild import IncrediBuild
from RS.Core.IncrediBuild.Modules import VideoProcessing, Filer
from RS.Core.Video import videoManager
from RS import ProjectData

from RS import Config


class VideoDoesNotExist(Exception):
    pass


class CorruptVideoFile(Exception):
    pass


class _VideoTransaction(object):
    def __init__(self, inputFilename, outputFilename):
        self.inputFilename = inputFilename
        self.outputFilename = outputFilename


class _TaskTransaction(object):
    def __init__(self, inputPath, outputPath):
        self.inputPath = inputPath
        self.outputPath = outputPath
        self.errors = []
        self.files = []
        self.segmentSize = None
        self.config = ""


def VideoTranscode(xmlFile):
    xmldoc = minidom.parse(xmlFile)
    manifest = xmldoc.getElementsByTagName("videoTranscodeManifest")[0]
    manifestDict = dict(manifest.attributes.items())
    emailList = ast.literal_eval(manifestDict.get("emailList", "[]"))

    videoProc = IncrediBuild.IncrediBuildGraph(manifestDict.get("Name", "Video Transcoding Process"))
    showOutput = manifestDict.get("showOutput", True)
    transactions = []

    for task in [pt for pt in manifest.childNodes if not isinstance(pt, minidom.Text)]:
        taskType = str(task.localName)

        taskDict = dict(task.attributes.items())
        inputDir = taskDict.get("inputDir", None)
        outputDir = taskDict.get("outpurDir", None)

        taskTransaction = _TaskTransaction(inputDir, outputDir)
        taskTransaction.config = taskType

        if taskType == "transcodeJob":
            cahceDir = taskDict.get("cacheDir", None)
            segmentSizeStr = taskDict.get("segSize", None)
            if segmentSizeStr is not None:
                segmentSize = int(segmentSizeStr)
            else:
                segmentSize = None
            taskTransaction.segmentSize = segmentSize
            # Either handle as a single file or a files in a folder
            if os.path.isdir(inputDir):
                for fileName in os.listdir(inputDir):
                    inFilePath = os.path.join(inputDir, fileName)
                    outFilePath = os.path.join(outputDir, fileName)
                    cacheFilePath = os.path.join(cahceDir, fileName)
                    try:
                        BuildTranscodeSplitVideoTasks(inFilePath, outFilePath, segmentSize=segmentSize, cachePath=cacheFilePath, buildGraph=videoProc)
                        taskTransaction.files.append(_VideoTransaction(inFilePath, outFilePath))
                    except (VideoDoesNotExist, CorruptVideoFile), error:
                        taskTransaction.errors.append(error)
            else:
                try:
                    BuildTranscodeSplitVideoTasks(inputDir, outputDir, segmentSize=segmentSize, cachePath=cahceDir, buildGraph=videoProc)
                    taskTransaction.files.append(_VideoTransaction(inputDir, outputDir))
                except (VideoDoesNotExist, CorruptVideoFile), error:
                    taskTransaction.errors.append(error)

        elif taskType == "convertJob":
            # Either handle as a single file or a files in a folder
            if os.path.isdir(inputDir):
                for fileName in os.listdir(inputDir):
                    inFilePath = os.path.join(inputDir, fileName)
                    outFilePath = os.path.join(outputDir, fileName)
                    try:
                        BuildTranscodeVideoTasks(inFilePath, outFilePath, buildGraph=videoProc)
                        taskTransaction.files.append(_VideoTransaction(inFilePath, outFilePath))
                    except (VideoDoesNotExist, CorruptVideoFile), error:
                        taskTransaction.errors.append(error)
            else:
                try:
                    BuildTranscodeVideoTasks(inputDir, outputDir, buildGraph=videoProc)
                    taskTransaction.files.append(_VideoTransaction(inputDir, outputDir))
                except (VideoDoesNotExist, CorruptVideoFile), error:
                    taskTransaction.errors.append(error)
        elif taskType == "fakeJob":
            # Not a real job type
            if os.path.isdir(inputDir):
                for fileName in os.listdir(inputDir):
                    inFilePath = os.path.join(inputDir, fileName)
                    outFilePath = os.path.join(outputDir, fileName)
                    if os.path.isfile(inFilePath):
                        taskTransaction.files.append(_VideoTransaction(inFilePath, outFilePath))
                    else:
                        taskTransaction.errors.append("File does not exist %r" % inFilePath)
            else:
                if os.path.isfile(inputDir):
                    taskTransaction.files.append(_VideoTransaction(inputDir, outputDir))
                else:
                    taskTransaction.errors.append("File does not exist %r" % inputDir)
        else:
            taskTransaction.errors.append("Unknow Job Type '{0}'".format(taskType))

        transactions.append(taskTransaction)

    startTime = datetime.datetime.now()
    # Wait for the job to finish
    videoProc.runJob(showOutput=showOutput, waitForFinish=True, raiseIfFailed=False)

    # Get the end time so we're not reporting the job took longer due to the email step
    endTime = datetime.datetime.now()
    for actions in transactions:
        # TODO: Move this into a task node
        _sendEmail(videoProc, emailList, startTime, endTime, actions)

    p4Root = Config.Project.Path.Root

    os.chdir(p4Root)
    changelistNumber = _createP4Changelist(p4Root, "VideoTranscoder: Adding Transcoded Videos")
    for actions in transactions:
        for fileTrans in actions.files:
            _addToP4(p4Root, changelistNumber, fileTrans.inputFilename)
            _addToP4(p4Root, changelistNumber, fileTrans.outputFilename)
    # _submitP4Changelist(p4Root, changelistNumber)

    print videoProc.logFilePath


def BuildTranscodeSplitVideoTasks(filePath, outputPath, segmentSize=None, cachePath=None, buildGraph=None, runRemoteWherePermitted=False):
    """
    Transcode a video using the farm processor, can break up into segments to allow for faster
    processing

    Args:
        filePath (str): File path to the video to transcode
        outputPath (str): File path to where the output file should be saved

    Kwargs:
        segmentSize (int): Segment size to break up the video in seconds
        cachePath (str): The path to the cache to store the segments, this can be auto generated
        buildGraph (IncrediBuildGraph): Add to exsiting graph is supplied
        runRemoteWherePermitted (bool): Run certain processes on remote boxes where permitted
    """
    if not os.path.isfile(filePath):
        raise VideoDoesNotExist("'{0}' is not a valid file path".format(filePath))

    # Generate Cache path is needed and create the dir if needed
    if cachePath is None:
        cachePath = os.path.join(os.path.dirname(outputPath), "cache")

    if not os.path.isdir(cachePath):
        os.makedirs(cachePath)

    # Create the output dir if needed
    if not os.path.isdir(os.path.dirname(outputPath)):
        os.makedirs(os.path.dirname(outputPath))

    # Create the graph
    videoProc = buildGraph or IncrediBuild.IncrediBuildGraph("Video Transcoding Process")
    prj = videoProc.project
    partsToMerge = []
    allTempFiles = []

    # Create the task groups
    extractCompressTaskGroup = prj.addNewTaskGroup("Extract", None)
    mergeTaskGroup = prj.addNewTaskGroup("Merge", None, dependsOn=(extractCompressTaskGroup,))

    audioExtractOutput = os.path.join(cachePath, "{0}_audio.mov".format(os.path.basename(filePath)))
    audioExtractTask = VideoProcessing.extractAudioTask(
                                                filePath,
                                                audioExtractOutput,
                                                prj,
                                                runRemote=runRemoteWherePermitted
                                                )
    allTempFiles.append(audioExtractOutput)

    # We have slightly different logic if the file is to be split or not
    if segmentSize is not None:
        # Work out the timing splits based off the seconds in the video and the split
        hours, minutes, seconds, frames = videoManager.VideoManager.getVideoLengthAsTuple(filePath)
        hoursInSecs = (hours * 60) * 60
        minsInSecs = minutes * 60
        totalSecs = seconds + minsInSecs + hoursInSecs

        splits = (totalSecs / segmentSize) + 1

        for idx in xrange(splits):
            startTimeCode = idx * 5
            if idx == splits - 1:
                endTimeCode = totalSecs - (idx * 5) + (float(frames) / 100)
            else:
                endTimeCode = segmentSize

            # Output paths
            segSplitOutput = os.path.join(cachePath, "split_{0}.mov".format(idx))
            segCompOutput = os.path.join(cachePath, "comp_{0}.mov".format(idx))

            # Split Task
            splitTask = VideoProcessing.extractVideoTask(
                                             filePath,
                                             segSplitOutput,
                                             prj,
                                             extractCompressTaskGroup,
                                             runRemote=runRemoteWherePermitted,
                                             startTimecode=startTimeCode,
                                             endTimecode=endTimeCode)
            allTempFiles.append(segSplitOutput)

            # Compress Task
            compressTask = VideoProcessing.compressTask(
                                                        segSplitOutput,
                                                        segCompOutput,
                                                        prj,
                                                        extractCompressTaskGroup,
                                                        dependsOn=(splitTask,),
                                                        runRemote=runRemoteWherePermitted
                                                        )
            partsToMerge.append(segCompOutput)
            allTempFiles.append(segCompOutput)
    else:
        segCompOutput = os.path.join(cachePath, "comp_{0}.mov".format(os.path.basename(filePath)))

        # Compress Task
        compressTask = VideoProcessing.compressTask(
                                                    filePath,
                                                    segCompOutput,
                                                    prj,
                                                    extractCompressTaskGroup,
                                                    runRemote=runRemoteWherePermitted
                                                    )
        partsToMerge.append(segCompOutput)
        allTempFiles.append(segCompOutput)

    segConcatOutput = os.path.join(cachePath, "{0}_video.mov".format(os.path.basename(filePath)))
    concatTask = VideoProcessing.concatTask(
                                            partsToMerge,
                                            segConcatOutput,
                                            prj,
                                            mergeTaskGroup,
                                            dependsOn=(extractCompressTaskGroup,),
                                            runRemote=runRemoteWherePermitted
                                            )
    allTempFiles.append(segConcatOutput)

    # Merge the video and audio back together
    mergeTask = VideoProcessing.mergeTask(
                                          segConcatOutput,
                                          audioExtractOutput,
                                          outputPath,
                                          prj,
                                          mergeTaskGroup,
                                          dependsOn=(concatTask,),
                                          runRemote=runRemoteWherePermitted
                                          )

    cleanupTaskGroup = prj.addNewTaskGroup("Cleanup", None, dependsOn=(mergeTaskGroup,))
    # Clean up all temp files
    for tempPath in allTempFiles:
        Filer.removeFileTask(
                            tempPath,
                            prj,
                            cleanupTaskGroup,
                            runRemote=False
                            )


def BuildTranscodeVideoTasks(filePath, outputPath, buildGraph=None, runRemoteWherePermitted=False):
    """
    Transcode a video using the farm processor

    Args:
        filePath (str): File path to the video to transcode
        outputPath (str): File path to where the output file should be saved

    Kwargs:
        buildGraph (IncrediBuildGraph): Add to exsiting graph is supplied
        runRemoteWherePermitted (bool): Run certain processes on remote boxes where permitted
    """
    if not os.path.isfile(filePath):
        raise VideoDoesNotExist("'{0}' is not a valid file path".format(filePath))

    # Create the output dir if needed
    if not os.path.isdir(os.path.dirname(outputPath)):
        os.makedirs(os.path.dirname(outputPath))

    # Create the graph
    videoProc = buildGraph or IncrediBuild.IncrediBuildGraph("Video Transcoding Process")
    prj = videoProc.project

    # Compress Task
    compressTask = VideoProcessing.compressTask(
                                                filePath,
                                                outputPath,
                                                prj,
                                                extractCompressTaskGroup,
                                                runRemote=runRemoteWherePermitted
                                                )
    partsToMerge.append(segCompOutput)


def _sendEmail(buildGraph, emailList, startTime, endTime, videoTransactions):
    templateFile = os.path.join(Config.Tool.Path.TechArt, "etc", "config", "notificationTemplates", "TranscodingTemplate.html")

    toStr = ", ".join(emailList)

    # Format the exception for email.
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Transcoding Job Finished Report: {0}'.format(videoTransactions.inputPath)
    message['To'] = toStr
    message['From'] = Config.User.Email

    jobTime = endTime - startTime
    days, hours, minutes = (jobTime.days, jobTime.seconds // 3600, jobTime.seconds // 60 % 60)
    seconds = jobTime.seconds - ((minutes + (hours * 60)) * 60)

    with open(buildGraph.logFilePath, "r") as logFile:
        logText = logFile.read()

    resultStringRegex = re.compile("Build: \d succeeded, \d failed, \d up-to-date, \d skipped\n\Z")
    results = resultStringRegex.findall(logText)
    if len(results) == 1:
        results = results[0].replace("\n", "")
    else:
        results = "No Incredbuild Log results found"

    geenColour = "rgb(3,126,0)"
    blueColour = "rgb(55,112,157)"

    statGraphTemplate = "<td style=\"background-color: #COLOUR#\"></td>"
    statTemplate = """
    <tr>
        <td> #FILENAME# - #COMPRESSIONPERCENT#% -  #COMPRESSEDSIZE#MB/#UNCOMPRESSEDSIZE#Mb</td>
    </tr>
    <tr>
        <td>
        <table cellpadding="5" cellspacing="0"  width="100%">
        <tr>
            #STATGRAPH#
        </tr>
        </table>
        </td>
    </tr>
    """
    statText = ""
    convertedFiles = 0
    uncompressFileSize = 0
    compressedFileSize = 0
    for fileTrans in videoTransactions.files:
        if os.path.isfile(fileTrans.inputFilename) and os.path.isfile(fileTrans.outputFilename):
            convertedFiles += 1
            uncompressedSize = round((os.path.getsize(fileTrans.inputFilename) / 1024.0) / 1024.0, 2)
            compressedSize = round((os.path.getsize(fileTrans.outputFilename) / 1024.0) / 1024.0, 2)

            compPercent = int(((float(compressedSize)) / uncompressedSize) * 100)
            # Copy Text so we leave the template alone
            fileStatText = str(statTemplate)
            fileStatText = fileStatText.replace("#FILENAME#", os.path.basename(fileTrans.inputFilename))
            fileStatText = fileStatText.replace("#COMPRESSIONPERCENT#", str(compPercent))
            fileStatText = fileStatText.replace("#COMPRESSEDSIZE#", str(compressedSize))
            fileStatText = fileStatText.replace("#UNCOMPRESSEDSIZE#", str(uncompressedSize))

            statGraph = ""
            for idx in xrange(100):
                graphPoint = str(statGraphTemplate)
                colour = geenColour
                if idx >= compPercent:
                    colour = blueColour
                graphPoint = graphPoint.replace("#COLOUR#", colour)
                statGraph += graphPoint

            fileStatText = fileStatText.replace("#STATGRAPH#", statGraph)
            statText += fileStatText
            uncompressFileSize += uncompressedSize
            compressedFileSize += compressedSize

    with open(templateFile, 'r') as handler:
        body = handler.read()
        body = body.replace("#PROJECT#", ProjectData.data.Project())
        body = body.replace("#USERNAME#", Config.User.Name)
        body = body.replace("#TIMESTAMP#", startTime.strftime('%d/%m/%Y @ %H:%M:%S'))
        body = body.replace("#JOBTIME#", "{0}d {1}h {2}m {3}s".format(days, hours, minutes, seconds))
        body = body.replace("#FILESPROCESSED#", str(convertedFiles))
        body = body.replace("#UNCOMPRESSEDFILESIZE#", str(uncompressFileSize))
        body = body.replace("#COMPRESSEDFILESIZE#", str(compressedFileSize))
        body = body.replace("#INPUTDIR#", videoTransactions.inputPath)
        body = body.replace("#OUTPUTDIR#", videoTransactions.outputPath)
        body = body.replace("#SEGMENTSIZE#", str(videoTransactions.segmentSize) or "0")
        body = body.replace("#COMPSTATS#", statText)
        body = body.replace("#JOBRESULTS#", results)
        body = body.replace("#ERRORLIST#", "<br>".join([str(msg) for msg in videoTransactions.errors] or ["No Errors"]))
        body = body.replace("#BUILDCONFIG#", videoTransactions.config)

    htmlBody = MIMEText(body, 'html')
    message.attach(htmlBody)

    for attachment in [buildGraph.logFilePath, buildGraph.graphFilePath, buildGraph.outputFilePath]:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(attachment, 'r').read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename={0}'.format(os.path.basename(attachment)))
        message.attach(part)

    server = smtplib.SMTP(Config.User.ExchangeServer)
    server.sendmail(Config.User.Email, emailList, message.as_string())
    server.quit()


def _createP4Changelist(p4Root, description):
    p4FilePath = os.path.join(tempfile.gettempdir(), "videoTranscoder", "{0}".format(str(uuid.uuid1())))
    if not os.path.exists(os.path.dirname(p4FilePath)):
        os.makedirs(os.path.dirname(p4FilePath))

    changeText = "new"
    clientText = str(socket.gethostname())
    userText = os.getenv("USERNAME")
    statusText = "new"
    descriptionText = str(description)

    handler = open(p4FilePath, "w")
    handler.write("Change:\t{0}\n\n".format(changeText))
    handler.write("Client:\t{0}\n\n".format(clientText))
    handler.write("User:\t{0}\n\n".format(userText))
    handler.write("Status:\t{0}\n\n".format(statusText))
    handler.write("Description:\n\t{0}\n".format(descriptionText))

    handler.close()

    argsToRun = "\"C:\\Program Files\\Perforce\\p4.exe\" change -i < {0}".format(p4FilePath)
    proc = subprocess.Popen(argsToRun, stdout=subprocess.PIPE, shell=True, cwd=p4Root)
    proc.wait()
    output = proc.communicate()
    resultString, _ = output
    resultStringRegex = re.compile("Change [\d]+ created.\r\n")
    if (re.compile("Change [\d]+ created.\r\n").findall(resultString)) == 0:
        raise ValueError("Unable to create p4 changelist")

    os.remove(p4FilePath)

    return int(re.compile("[\d]+").search(resultString).group(0))


def _addToP4(p4Root, changelistNumber, filePath):
    argsToRun = "\"C:\\Program Files\\Perforce\\p4.exe\" add -c {0} {1}".format(changelistNumber, filePath)
    proc = subprocess.Popen(argsToRun, stdout=subprocess.PIPE, shell=True, cwd=p4Root)
    proc.wait()


# VideoTranscode(r"C:\Users\geoff.samuel\Desktop\videoTranscodeTask.xml")
# TranscodeVideo(r"\\EDIW-GSAMUEL\Users\Public\shared\0657_A_M_M_Poor_White_16_Tom_Dheere0004.mov", r"\\EDIW-GSAMUEL\Users\Public\shared\output\0657_A_M_M_Poor_White_16_Tom_Dheere0004.mov", segmentSize=5, runRemoteWherePermitted=False)
