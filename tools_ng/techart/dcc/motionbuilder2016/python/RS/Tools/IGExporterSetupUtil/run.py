from RS.Tools.IGExporterSetupUtil.Widgets import IGExporterSetupUtil


def Run( show = True ):
    toolsDialog = IGExporterSetupUtil.MainWindow()
    
    if show:
        toolsDialog.show()

    return toolsDialog