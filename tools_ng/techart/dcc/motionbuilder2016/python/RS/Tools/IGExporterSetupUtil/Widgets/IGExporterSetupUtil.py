import os

import pyfbsdk as mobu
from PySide import QtGui, QtCore

import RS.Tools.UI
from RS import Config, Globals

from RS.Core import Export as animationExporter

from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Prop import Prop
from RS.Core.ReferenceSystem.Types.Character import Character
from RS.Core.AssetSetup.FPS_Cam_Setup import addAttributesToExporter

uiDirectory = os.path.join(RS.Config.Script.Path.Root, "RS\\Tools\\UI\\IGExporterSetupUtil\\QT\\")

RS.Tools.UI.CompileUi( uiDirectory + "IGExporterSetupUtil.ui",
                    uiDirectory + "IGExporterSetupUtil_compiled.py" )

import RS.Tools.UI.IGExporterSetupUtil.QT.IGExporterSetupUtil_compiled as IGExporterSetupUtilCompiled

class MainWindow(RS.Tools.UI.QtMainWindowBase, IGExporterSetupUtilCompiled.Ui_MainWindow):
    def __init__(self):
        # main window settings
        RS.Tools.UI.QtMainWindowBase.__init__(self, None)
        
        self.setupUi(self)
        
        banner = RS.Tools.UI.BannerWidget('IG Exporter Setup Util', helpUrl=None)
        self.horizontalLayout.addWidget( banner )
        
        self.pushButton_Refresh.clicked.connect(self.populateLists)
        
        self.pushButton_AddSelected.clicked.connect(self.addSelected)
        
        self.Manager = Manager()
        
        self.populateLists()

 
    def populateLists(self):

        # populate Characters ListView
        characters = []          
        characters = [sceneCharacter.Namespace for sceneCharacter in self.Manager.GetReferenceListByType(Character)]
        
        model = QtGui.QStandardItemModel()
        self.listView_Characters.setModel(model)

        # remove items first (if they exist)
        for row in xrange(model.rowCount()):
            item = model.item(row)  
            model.removeRow(row)

        for character in characters:
            item = QtGui.QStandardItem(character)
            if not "Mover" in item.text():
                model.appendRow(item)
        
        # populate Props ListView
        props = []
        props = [sceneProp.Namespace for sceneProp in self.Manager.GetReferenceListByType(Prop)]
        
        model = QtGui.QStandardItemModel()
        self.listView_Props.setModel(model)
        
        # remove items first (if they exist)
        for row in xrange(model.rowCount()):
            item = model.item(row)  
            model.removeRow(row)
        
        for prop in props:
            item = QtGui.QStandardItem(prop)
            model.appendRow(item)        
        
        # populate Cameras ListView 
        cameras = Globals.System.Scene.Cameras
                
        model = QtGui.QStandardItemModel()
        self.listView_Cameras.setModel(model)
        
        # remove items first (if they exist)
        for row in xrange(model.rowCount()):
            item = model.item(row)  
            model.removeRow(row)
        
        for camera in cameras:
            item = QtGui.QStandardItem(camera.LongName)
            if not "Producer" in item.text() and not "Mover" in item.text():
                model.appendRow(item)          


    def addSelected(self):

        # add selected Characters
        selectedCharacters = self.listView_Characters.selectedIndexes()
        
        for data in selectedCharacters:
            
            characterName = data.data()
            skelRoot = mobu.FBFindModelByLabelName('{0}:SKEL_ROOT'.format(characterName))
            mover = mobu.FBFindModelByLabelName('{0}:mover'.format(characterName))
            
            if not mover: # pass this model if the mover does not exist, as this pretty much determines if the model is valid for the exporter
                QtGui.QMessageBox.warning(None, "IGSeporterSetupUtil Character Error:", "There was an issue finding the mover for this character: '" 
                                + str(characterName)
                                + "\n Please check and try again.")
                return

            if '{0}:{1}'.format(characterName, 'FirstPersonCamPivot_MoverSpace') in [child.LongName for child in mover.Children]:
                skelFile = 'Player_withFirstPerson.skel'
                specFile = 'FullBody_FirstPerson_Spec.xml'
            else:
                skelFile = 'player.skel'
                specFile = 'FullBody_Spec.xml'
                
            heelHeight = '{}:HeelHeight'.format(characterName)
            
            animationExporter.RexExport.EditRootEntry(skelRoot.LongName,
                                                      mover.LongName, 
                                                      skelFile,
                                                      'Body', 
                                                      specFile,
                                                      '')
        
            animationExporter.RexExport.AddAdditionalModelEntry(skelRoot.LongName,
                                                                heelHeight,
                                                                'genericControl',
                                                                'HeelHeight')                                                
            animationExporter.RexExport.SetExportModel(skelRoot.LongName,
                                                                True)
            animationExporter.RexExport.SetMergeFacial(skelRoot.LongName,
                                                                False)
  
            if skelFile == 'Player_withFirstPerson.skel':
                addAttributesToExporter('{}'.format(characterName))
            
        # add selected Props
        selectedProps = self.listView_Props.selectedIndexes()
        
        for data in selectedProps:
            
            propsName = data.data()
            mover = mobu.FBFindModelByLabelName('{0}:mover'.format(propsName))
            if not mover: # pass this model if the mover does not exist, as this pretty much determines if the model is valid for the exporter
                QtGui.QMessageBox.warning(None, "IGSeporterSetupUtil Character Error:", "There was an issue finding the mover for this prop: '" 
                                + str(propsName)
                                + "\n Please check and try again.")
                return
            
            moverChild = mover.Children
            
            if len(moverChild) != 1:
                QtGui.QMessageBox.warning(None, "IGSeporterSetupUtil Prop Name Error:", "There was an issue adding the prop: '" 
                                + str(propsName)
                                + "\n Please check and try again.")
                return
            
            skelRoot = mobu.FBFindModelByLabelName(moverChild[0].LongName)

            try:
                animationExporter.RexExport.EditRootEntry(skelRoot.LongName,
                                                          mover.LongName, 
                                                          'tr3.skel',
                                                          'Body', 
                                                          'Prop_RotTrans_spec.xml',
                                                          '')
                animationExporter.RexExport.SetExportModel(skelRoot.LongName,
                                                                    True)
                animationExporter.RexExport.SetMergeFacial(skelRoot.LongName,
                                                                    False)

            except Exception, e:
                QtGui.QMessageBox.warning(None, "IGSeporterSetupUtil Prop Name Error:", "There was an issue adding the prop: "
                                                + str(propsName) + "\n" + str(e)) 

        # add selected Cameras
        selectedCameras = self.listView_Cameras.selectedIndexes()
        
        for data in selectedCameras:
            
            cameraName = data.data()
            camera = mobu.FBFindModelByLabelName('{}'.format(cameraName))
            mover = mobu.FBFindModelByLabelName('{}'.format(cameraName))

            animationExporter.RexExport.EditRootEntry(camera.LongName,
                                                        mover.LongName,
                                                        'tr3.skel',
                                                        'Camera', 
                                                        'Camera_Export_spec.xml',
                                                        '')
            animationExporter.RexExport.SetExportModel(camera.LongName,
                                                        True)
            animationExporter.RexExport.SetMergeFacial(camera.LongName,
                                                        False)

        mobu.ShowToolByName("Rex Rage Animation Export")# open IG exporter
        

def Run():
    form = MainWindow()
    form.show()