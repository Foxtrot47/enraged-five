from RS.Tools.MocapReviewTool.Widgets import mocapReviewTool
from RS.Tools import UI


HUB_PAGE_URL = "https://hub.gametools.dev/display/ANIM/Motionbuilder+Mocap+Review+Tool"


@UI.Run(title=mocapReviewTool.MocapReviewToolWidget.TOOL_TITLE, dockable=True, url=HUB_PAGE_URL)
def Run():
    return mocapReviewTool.MocapReviewToolWidget()
