"""Contains the widget for the Mocap Review Tool.

Warnings:
    * Imports external libs from the streams, which could potentially conflict with those in the MoBu codebase.
        * If this becomes an issue, pick one of these options:
            * Refactor to run YAAS entity creation entirely in a subprocess and get the task id from stdout.
            * Copy YAAS into the MoBu codebase.

Contributing:
    * Before submitting changes:
        * Update the MocapReviewToolWidget.VERSION number (uses semantic versioning).
        * Update the changelog section in https://hub.gametools.dev/display/ANIM/Motionbuilder+Mocap+Review+Tool.
"""
import os
import subprocess
import logging
import traceback
import re

import pyfbsdk as mobu
from PySide import QtCore, QtGui

from RS import Perforce, Config, Globals
from RS._Perforce import serversTemp as servers
from RS.Core.AnimData import Context, fileManager
from RS.Core.Mocap import Clapper, MocapCommands
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types import MocapStage, MocapReviewTrial
from RS.Tools.CameraToolBox.PyCoreQt.Widgets import dragableDoubleSpinBox
from RS.Utils import Namespace
from RS.Utils.Scene import Character


LOGGER = logging.getLogger("MocapReviewTool")


class TaskStatus(object):
    """Enum for task statuses.

    Ported from yaas.const
    """

    CREATED = "Created"  # Not yet ready be processed; This is the default status.
    # TODO implement BLOCKED and ON_HOLD, we'll need a centralized server/queing system or a service on each node.
    # BLOCKED = "Blocked"  # Waiting for blockers to all finish.
    # ON_HOLD = "On-Hold"
    PENDING = "Pending"  # Has been submitted to an integrated system/retried; Available to be processed.
    PROCESSING = "Processing"
    COMPLETED = "Completed"
    ERRORED = "Errored"
    CANCELLED = "Cancelled"  # User cancelled.


class GetTrialFromSceneError(Exception):
    """Raise when the trial cannot be query from scene data."""


class TechartDepotDirs(object):
    """Consts for related TechArt directories."""

    _STREAM_NAME = Config.Project.Name
    ROOT = os.path.join("X:\\", "techart", _STREAM_NAME)
    PYTHON = os.path.join(ROOT, "python")
    EXTERNAL_LIBS = os.path.join(ROOT, "externalLibs", "Windows", "x64", "2.7")
    JENKINS = os.path.join(ROOT, "python", "rockstarJenkins")
    PROJECTS = os.path.join("X:\\", "projects")


class YaasRunners(object):
    """Consts for the file paths of the YAAS runner scripts required to submit the YAAS task to Jenkins."""

    PYTHON = os.path.join(TechartDepotDirs.ROOT, "bat", "runPy.bat")
    BUILDER = os.path.join(TechartDepotDirs.JENKINS, "jobs", "generateRsrefineFbx", "runners", "buildJob.py")
    SUBMITTER = os.path.join(TechartDepotDirs.JENKINS, "yaasComponents", "runners", "submitLinuxPythonTask.py")
    STATUS_CHECKER = os.path.join(TechartDepotDirs.JENKINS, "yaasComponents", "runners", "getTaskStatus.py")
    MESSAGE_CHECKER = os.path.join(TechartDepotDirs.JENKINS, "yaasComponents", "runners", "getTaskMessage.py")


class CommandExecutionError(Exception):
    """Raise when an error is encountered when running a command.

    Primarily exists to consistently format the cmd, stdout, and stderr text into a single message.
    """

    def __init__(self, cmd, stdout=None, stderr=None, indent=4, indentLevel=1):
        indentChars = " " * indent
        if stdout is not None:
            stdout = "\n{}".format(indentChars * (indentLevel + 1)).join(stdout.splitlines())
        if stderr is not None:
            stderr = "\n{}".format(indentChars * (indentLevel + 1)).join(stderr.splitlines())
        messageTokens = (
            " ".join(cmd),
            "{}STDOUT:\n{}{}".format(indentChars * indentLevel, indentChars * (indentLevel + 1), stdout or "N/A"),
            "{}STDERR:\n{}{}".format(indentChars * indentLevel, indentChars * (indentLevel + 1), stderr or "N/A"),
        )
        super(CommandExecutionError, self).__init__("\n".join(messageTokens))
        self.stdout = stdout
        self.stderr = stderr


class RunnerThreadBase(QtCore.QThread):
    """Base class for threads that need to run commands.

    Signals:
        errorRaised (object, object): Emitted when an error is encountered when running a thread.
    """

    # Signals
    errorRaised = QtCore.Signal(object, object)

    @staticmethod
    def runCmd(cmd, logger=None, verbose=True):
        """Wraps the execution of the provided command.

        Args:
            cmd (str): The command to run.
            logger (logging.Logger, optional): The logger to log to.
            verbose (bool, optional): If True, log the cmd and stdout, stderr as debug messages.

        Returns:
            tuple[subprocess.Popen, str, str]: the process, stdout, stderr

        Notes:
            * Logs to info level, as debug isn't getting written to the log file and these commands aren't called often.
        """
        logger = logger or logging.getLogger()
        if verbose:
            logger.debug("CMD: '{}'".format(cmd))
        # TODO use QProcess instead.
        createNoWindow = 0x08000000
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, creationflags=createNoWindow)
        stdout, stderr = proc.communicate()
        if proc.returncode:
            raise CommandExecutionError(cmd, stdout=stdout, stderr=stderr)
        if verbose:
            logger.debug("CMD stdout:\n    {}".format("\n    ".join(stdout.splitlines())))
            logger.debug("CMD stderr:\n    {}".format("\n    ".join(stderr.splitlines())))
        return proc, stdout, stderr


class JobSubmissionThread(RunnerThreadBase):
    """Create YAAS task and triggers the Jenkins build via command line utility.

    Signals:
        taskCreated (str): Emits the new task ID, once the YAAS entities have been built.
    """

    taskCreated = QtCore.Signal(str)

    def __init__(self, parent, trial):
        super(JobSubmissionThread, self).__init__(parent)  # Add parent here to allow proper garbage collection.
        self._trial = trial
        self.finished.connect(self.deleteLater)
        
    def run(self):
        try:
            cmd = [YaasRunners.PYTHON, YaasRunners.BUILDER, str(self._trial.id)]
            proc, stdout, stderr = self.runCmd(cmd, logger=LOGGER)

            taskId = None
            result = re.search("TaskID: '([a-zA-Z0-9_]*)'", stderr)
            if result:
                taskId = result.groups()[0]
            else:
                raise ValueError("Failed to parse TaskID from logging output!")

            cmd = (YaasRunners.PYTHON, YaasRunners.SUBMITTER, taskId)
            proc, stdout, stderr = self.runCmd(cmd, logger=LOGGER)

            self.taskCreated.emit(taskId)
        except Exception as error:
            self.errorRaised.emit(traceback.format_exc(), error)


class TaskWatcherThread(RunnerThreadBase):
    """
    Thread for tracking the status of a specific YAAS task.

    Ported from rockstar.yaas.threadWatchstar in TechArt depot to workaround PySide/PySide2 differences in codebases.

    Signals:
        statusChanged (str): Emits the current status of the job if it is different from the previous run.
    """

    statusChanged = QtCore.Signal(str, str)

    def __init__(self, parent, taskId, status=None, sleep=5000):
        """Initialize the class.

        Args:
            parent (QtCore.QObject): The parent object.
            taskId (str): The task ID.
            status (str): The status to start comparing with.
            sleep (int): Amount of time (milliseconds) to wait before running query again.
        """
        super(TaskWatcherThread, self).__init__(parent)  # Parent required for garbage collection.
        self._taskId = taskId
        self._sleep = sleep
        self._status = status

        self.finished.connect(self.deleteLater)

    def run(self):
        while True:
            self.msleep(self._sleep)
            try:
                cmd = [YaasRunners.PYTHON, YaasRunners.STATUS_CHECKER, self._taskId]
                proc, stdout, stderr = self.runCmd(cmd, logger=LOGGER)

                result = re.search("Task Status: '([a-zA-Z0-9_]*)'", stderr)
                if result:
                    currentStatus = result.groups()[0]
                    if currentStatus != self._status:
                        self._status = currentStatus
                        self.statusChanged.emit(self._status, self._taskId)
                else:
                    raise ValueError("Failed to parse Task Status from logging output!")
            except Exception as error:
                self.errorRaised.emit(traceback.format_exc(), error)


class TaskMessageThread(RunnerThreadBase):
    """Query a YAAS task for the status message."""

    taskMessage = QtCore.Signal(str)

    def __init__(self, parent, taskId):
        super(TaskMessageThread, self).__init__(parent)  # Add parent here to allow proper garbage collection.
        self._taskId = taskId

        self.finished.connect(self.deleteLater)

    def run(self):
        try:
            cmd = [YaasRunners.PYTHON, YaasRunners.MESSAGE_CHECKER, str(self._taskId)]
            proc, stdout, stderr = self.runCmd(cmd, logger=LOGGER)

            taskMessage = None
            result = re.search("Task Message: '(.*)'", stderr)
            if result:
                taskMessage = result.groups()[0]
            else:
                try:
                    taskMessage = stderr.split("Task Message: '")[-1].rstrip("'").strip()
                except Exception as error:
                    raise ValueError("Failed to parse task message from logging output!")

            self.taskMessage.emit(taskMessage)
        except Exception as error:
            self.errorRaised.emit(traceback.format_exc(), error)


class SyncAndLoadRefineFileWidget(QtGui.QDialog):
    """Create a popup window for the user to decide how to load a refine FBX file"""

    def __init__(self, parent=None):
        super(SyncAndLoadRefineFileWidget, self).__init__(parent=parent)

        # Set window title, flags, and width
        self.setWindowTitle("Sync And Load Refine File")
        self.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowStaysOnTopHint)
        self.setMinimumWidth(1100)

        # Set instance variable for filePath to None and findManually to False by default
        self.filePath = None
        self.findManually = False

        # Create and set the layout for the dialog window
        layout = QtGui.QVBoxLayout()
        self.setLayout(layout)

        # Create a group box for the user inputted path option
        importRefineGroupBox = QtGui.QGroupBox("Paste path for refine file to sync and load.")
        importRefineGroupBoxLayout = QtGui.QHBoxLayout()

        # Create line edit for inputting the filePath
        self.filePathInput = QtGui.QLineEdit()

        # Create button to register that the user wants to sync the given file and load it
        syncAndLoadButton = QtGui.QPushButton("Sync and Load")
        syncAndLoadButton.setDefault(True)

        # Set layout for the group box and add widgets to it
        importRefineGroupBox.setLayout(importRefineGroupBoxLayout)
        importRefineGroupBoxLayout.addWidget(self.filePathInput)
        importRefineGroupBoxLayout.addWidget(syncAndLoadButton)

        # Create a group box to hold the button for registering that the user wants to find and load a local file
        findFileGroupBox = QtGui.QGroupBox("Manually load local refine file.")
        findFileGroupBoxLayout = QtGui.QHBoxLayout()
        findButton = QtGui.QPushButton("Find Refine File")

        # Set layout for the group box and add widgets to it
        findFileGroupBox.setLayout(findFileGroupBoxLayout)
        findFileGroupBoxLayout.addWidget(findButton)

        # Add both group boxes to the main layout
        layout.addWidget(importRefineGroupBox)
        layout.addWidget(findFileGroupBox)

        # Connect functions for setting the file path and returning fileManually to their respective buttons
        syncAndLoadButton.clicked.connect(self.getFilePath)
        findButton.clicked.connect(self.getFileManually)

    def getFilePath(self):
        """Gets the inputted filePath and closes the prompt window"""
        self.filePath = self.filePathInput.text() or None
        self.close()

    def getFileManually(self):
        """Sets findManually to True and closes the prompt window"""
        self.findManually = True
        self.close()


class MocapReviewToolWidget(QtGui.QWidget):
    """Tool with options to help the user review mocap data."""

    TOOL_NAME = "Mocap Review Tool"
    VERSION = "0.5.0"
    TOOL_TITLE = "{} (v{})".format(TOOL_NAME, VERSION)

    TASK_BUILDING_STATUS = "Creating Task"

    def __init__(self, parent=None):
        super(MocapReviewToolWidget, self).__init__(parent=parent)

        self._localFbxPath = None  # Store local path for multiple reloads.
        self._jobSubmissionThread = None
        self._taskWatcherThread = None
        self._copyFromNetwork = False

        self._log = logging.getLogger(self.TOOL_NAME)

        self.settings = QtCore.QSettings("Rockstar", "MocapReview")
        self.mostRecentDir = self.settings.value("mostRecentDir", TechartDepotDirs.PROJECTS)

        self._refineDict = {}
        self.manager = Manager.Manager()
        self.setupUi()

        self.syncDependencies()  # TODO Remove once the launcher is syncing the streams.

    def syncDependencies(self):
        Perforce.Sync([TechartDepotDirs.PYTHON, TechartDepotDirs.EXTERNAL_LIBS])

    def setupUi(self):
        """Set up the UI and attach the connections"""
        # Layouts
        layout = QtGui.QVBoxLayout()
        importRefineLayout = QtGui.QVBoxLayout()
        stageLayout = QtGui.QGridLayout()
        lowerLayout = QtGui.QGridLayout()
        slateLayout = QtGui.QGridLayout()
        loadLayout = QtGui.QHBoxLayout()
        displayLayout = QtGui.QVBoxLayout()
        visibilityLayout = QtGui.QVBoxLayout()
        meshLayout = QtGui.QVBoxLayout()
        colorLayout = QtGui.QVBoxLayout()

        # Widgets
        importRefineGBox = QtGui.QGroupBox("Import Trial's Unedited Source Motion (AKA: rsrefine.anm)")
        alignRefineGBox = QtGui.QGroupBox("Align Refined Data to a Stage:")
        offsetSlateGBox = QtGui.QGroupBox("Shift Story Track to Match Slate Offset:")
        displayGBox = QtGui.QGroupBox("Display Options:")
        visibilityGBox = QtGui.QGroupBox("Set Visibility:")
        meshGBox = QtGui.QGroupBox("Set Mesh Display:")
        colorGBox = QtGui.QGroupBox("Set Mesh Colors:")
        self._generateRefineBttn = QtGui.QPushButton("Generate Refine")
        self._jobStatusLabel = QtGui.QLabel()
        self._loadRefineBttn = QtGui.QPushButton("Load Refine")
        self._loadRefineManualBttn = QtGui.QPushButton("Choose Refine File")
        self._refineWidget = QtGui.QComboBox()
        self._refNullWidget = QtGui.QListWidget()
        self._alignRefine = QtGui.QPushButton("Align")
        self._resetAlignment = QtGui.QPushButton("Reset")
        self._offsetAlignment = QtGui.QCheckBox("Offset Manually")
        self._xMod = dragableDoubleSpinBox.DragableDoubleSpinBox(increment=1.0, useDoubleClick=True)
        self._yMod = dragableDoubleSpinBox.DragableDoubleSpinBox(increment=1.0, useDoubleClick=True)
        self._zMod = dragableDoubleSpinBox.DragableDoubleSpinBox(increment=1.0, useDoubleClick=True)
        self._stageWidget = QtGui.QComboBox()
        self._offsetTrack = QtGui.QPushButton("Offset")
        self._keysWidget = QtGui.QComboBox()
        self._useManualOffset = QtGui.QCheckBox("Offset Manually")
        self._manualFrame = QtGui.QSpinBox()
        self._showRefine = QtGui.QRadioButton("Show")
        self._hideRefine = QtGui.QRadioButton("Hide")
        self._defaultDisplay = QtGui.QRadioButton("Default")
        self._wireDisplay = QtGui.QRadioButton("Wire-Frame")
        self._openColorWheel = QtGui.QPushButton("Additional Color Options")
        self._applyColor = QtGui.QPushButton("Apply Color")
        self._colorPicker = QtGui.QColorDialog()
        self._hiddenColorPicker = QtGui.QColorDialog()

        for widget in (self._xMod, self._yMod, self._zMod):
            widget.setMaximum(100.0)
            widget.setMinimum(-100.0)
            widget.setValue(0.0)
            widget.setEnabled(False)
            widget.valueChanged.connect(self._handleAlignRefineBttnClicked)

        # Hide buttons in the color dialog that we don't need.
        # We want to keep the default colors, and the "pick screen color" button
        wellToKeep = None
        for child in self._colorPicker.children():
            childName = child.metaObject().className()
            if "Layout" in childName:
                child.setSizeConstraint(QtGui.QLayout.SetFixedSize)
            if childName == "QWellArray" and not wellToKeep:
                wellToKeep = child
                wellToKeep.setMaximumHeight(50)
            elif childName not in ("QVBoxLayout", "QTimer"):
                child.hide()

        divider = QtGui.QFrame()

        # Configure Widgets
        self._setJobStatusWidget(self.TASK_BUILDING_STATUS)
        self._jobStatusLabel.setVisible(False)
        self._loadRefineBttn.setEnabled(False)
        self._defaultDisplay.setChecked(True)
        self._showRefine.setChecked(True)
        divider.setFrameShape(QtGui.QFrame.HLine)
        divider.setFrameShadow(QtGui.QFrame.Sunken)

        self._manualFrame.setEnabled(False)
        existing = self.manager.GetReferenceListByType(MocapReviewTrial.MocapReviewTrial)
        if existing:
            Globals.Story.Mute = False
            self._repopulateAll()
        else:
            self._toggleButtons(False)

        self._refNullWidget.setMinimumHeight(50)
        self._refNullWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

        self._populateStages()

        self._manualFrame.setRange(-10000, 10000)
        self._manualFrame.setValue(0)

        for widget in (self._colorPicker, self._hiddenColorPicker):
            widget.setOption(QtGui.QColorDialog.ShowAlphaChannel, False)
        self._hiddenColorPicker.setGeometry(QtCore.QRect(300, 300, 400, 200))

        for child in self._hiddenColorPicker.children():
            childName = child.metaObject().className()
            if childName in ("QPushButton", "QWellArray") or (
                childName == "QLabel" and child.text() in ("&Basic colors", "&Custom colors")
            ):
                child.hide()

        # Add Widgets to Layouts
        importRefineLayout.addWidget(self._generateRefineBttn)
        importRefineLayout.addWidget(self._jobStatusLabel)
        importRefineLayout.addLayout(loadLayout)
        slateLayout.addWidget(QtGui.QLabel("Slate Clap Keyframes:"), 0, 0, 1, 2)
        slateLayout.addWidget(self._keysWidget, 0, 2, 1, 2)
        slateLayout.addWidget(self._useManualOffset, 1, 0, 1, 2)
        slateLayout.addWidget(self._manualFrame, 1, 2, 1, 2)
        slateLayout.addWidget(self._offsetTrack, 2, 0, 1, 4)
        stageLayout.addWidget(self._refNullWidget, 0, 0, 1, 3)
        stageLayout.addWidget(self._stageWidget, 1, 0, 1, 3)
        stageLayout.addWidget(self._alignRefine, 2, 0, 1, 3)
        stageLayout.addWidget(self._resetAlignment, 3, 0, 1, 3)
        stageLayout.addWidget(self._offsetAlignment, 4, 0, 1, 3)
        stageLayout.addWidget(self._xMod, 5, 0, 1, 1)
        stageLayout.addWidget(self._yMod, 5, 1, 1, 1)
        stageLayout.addWidget(self._zMod, 5, 2, 1, 1)
        stageLayout.addWidget(QtGui.QLabel("Scroll or double-click-and-drag to scrub values"), 6, 0, 1, 3)
        visibilityLayout.addWidget(self._showRefine)
        visibilityLayout.addWidget(self._hideRefine)
        meshLayout.addWidget(self._defaultDisplay)
        meshLayout.addWidget(self._wireDisplay)
        loadLayout.addWidget(self._loadRefineBttn)
        loadLayout.addWidget(self._loadRefineManualBttn)
        colorLayout.setSizeConstraint(QtGui.QLayout.SetFixedSize)
        colorLayout.addWidget(self._colorPicker)
        colorLayout.addWidget(self._openColorWheel)
        colorLayout.addWidget(self._applyColor)

        layout.addWidget(importRefineGBox)
        layout.addWidget(divider)
        layout.addWidget(self._refineWidget)
        displayLayout.addWidget(visibilityGBox)
        displayLayout.addWidget(meshGBox)
        lowerLayout.addWidget(alignRefineGBox, 0, 0, 1, 1)
        lowerLayout.addWidget(offsetSlateGBox, 0, 1, 1, 1)
        lowerLayout.addWidget(displayGBox, 1, 0, 1, 1)
        lowerLayout.addWidget(colorGBox, 1, 1, 1, 1)
        layout.addLayout(lowerLayout)

        # Set Layouts to Widgets
        importRefineGBox.setLayout(importRefineLayout)
        alignRefineGBox.setLayout(stageLayout)
        offsetSlateGBox.setLayout(slateLayout)
        displayGBox.setLayout(displayLayout)
        visibilityGBox.setLayout(visibilityLayout)
        meshGBox.setLayout(meshLayout)
        colorGBox.setLayout(colorLayout)
        self.setLayout(layout)

        # Hook up connections
        self._hiddenColorPicker.currentColorChanged.connect(self._hiddenColorChanged)
        self._hiddenColorPicker.colorSelected.connect(self._colorMeshItems)
        self._generateRefineBttn.clicked.connect(self._handleGenerateRefineBttnClicked)
        self._loadRefineBttn.clicked.connect(self._handleLoadRefineBttnClicked)
        self._loadRefineManualBttn.clicked.connect(self._handleLoadRefineManualBttnClicked)
        self._alignRefine.clicked.connect(self._handleAlignRefineBttnClicked)
        self._wireDisplay.toggled.connect(self._handleToggleWireFrame)
        self._defaultDisplay.toggled.connect(self._handleToggleWireFrame)
        self._showRefine.toggled.connect(self._handleToggleVisible)
        self._hideRefine.toggled.connect(self._handleToggleVisible)
        self._openColorWheel.clicked.connect(self._showHiddenColorPicker)
        self._applyColor.clicked.connect(self._colorMeshItems)
        self._refineWidget.currentIndexChanged.connect(self._refineTextChanged)
        self._useManualOffset.toggled.connect(self._displayManualOffset)
        self._offsetAlignment.toggled.connect(self._displayOffsetAlignment)
        self._offsetTrack.clicked.connect(self._offsetStoryTrack)
        self._resetAlignment.clicked.connect(self._handleReset)

        Globals.Callbacks.OnFileOpenCompleted.Add(self._onSceneChanged)
        Globals.Callbacks.OnFileNewCompleted.Add(self._onNewScene)

    def closeEvent(self, event):
        """un-register callbacks"""
        Globals.Callbacks.OnFileOpenCompleted.Remove(self._onSceneChanged)
        Globals.Callbacks.OnFileNewCompleted.Remove(self._onNewScene)

    def _onSceneChanged(self, source, event):
        """Callback for when a new scene is loaded"""
        existing = self.manager.GetReferenceListByType(MocapReviewTrial.MocapReviewTrial)
        if existing:
            self._populateRefineOptions()
            self._populateSlateKeys()

        self._toggleButtons(bool(existing))
        self._populateRefineOptions()
        self._populateRefineItems()
        self._populateSlateKeys()
        self._populateStages()

    def _onNewScene(self, source, event):
        """Callback for when a new file is made"""
        self._toggleButtons(False)
        self._populateRefineOptions()
        self._populateRefineItems()
        self._populateSlateKeys()
        self._refNullWidget.clear()
        self._stageWidget.clear()

    def _setJobStatusWidget(self, status):
        """Update the job status widget.

        Args:
            status (str): The status of the job.
        """
        self._jobStatusLabel.setText("Job Status: '{}'".format(status))

    def _handleThreadError(self, formattedExc, error):
        self._log.critical(formattedExc)

        self._setJobStatusWidget(TaskStatus.ERRORED)  # Only visible until the user hits ok in the messagebox.

        QtGui.QMessageBox.critical(self, self.TOOL_NAME, "Error Encountered:\n\n{}".format(formattedExc))

        self._jobStatusLabel.setVisible(False)
        self._generateRefineBttn.setVisible(True)
        self._generateRefineBttn.setEnabled(True)

        raise error

    def _handleTaskCreated(self, taskId):
        try:
            self._jobSubmissionThread.quit()
        except RuntimeError as error:
            pass  # extra handling for "RuntimeError: Internal C++ object (JobSubmissionThread) already deleted."
        self._jobSubmissionThread = None

        self._taskWatcher = TaskWatcherThread(self, taskId)
        self._taskWatcher.statusChanged.connect(self._handleTaskStatusChanged)
        self._taskWatcher.start()
        self._log.info("YAAS task watcher started")

    def _handleTaskStatusChanged(self, status, taskId):
        """Handler for the task watcher's taskStatusChanged signal.

        Args:
            status (str): The new status of the YAAS task.
            taskId (str): The YAAS task id to work with.
        """
        self._log.info("Status updated: {}".format(status))
        self._setJobStatusWidget(status)

        if status in (TaskStatus.CREATED, TaskStatus.PENDING):
            return  # These are the most common statuses; return early to optimize.
        elif status == TaskStatus.COMPLETED:
            try:
                self._taskWatcher.quit()
            except RuntimeError as error:
                pass  # extra handling for "RuntimeError: Internal C++ object (TaskWatcherThread) already deleted."
            self._taskWatcher = None

            try:
                trial = self._getTrial()
            except GetTrialFromSceneError as error:
                QtGui.QMessageBox.warning(self, self.TOOL_NAME, str(error))
                return
            self._depotFbxPath = fileManager.getRsrefineFbxForTrial(trial)
            self._log.info("FBX depot path: {}".format(self._depotFbxPath))

            self._loadRefineBttn.setEnabled(True)

            QtGui.QMessageBox.information(self, self.TOOL_NAME, "The .FBX is ready! Please click 'Load Refine'")
        elif status == TaskStatus.CANCELLED:
            # Keep the thread polling.
            QtGui.QMessageBox.information(self, self.TOOL_NAME, "The job was cancelled! Please contact TechArt!")
        elif status == TaskStatus.ERRORED:
            try:
                self._taskWatcher.quit()
            except RuntimeError as error:
                pass  # extra handling for "RuntimeError: Internal C++ object (TaskWatcherThread) already deleted."
            self._taskMessageThread = TaskMessageThread(self, taskId)
            self._taskMessageThread.errorRaised.connect(self._handleThreadError)
            self._taskMessageThread.taskMessage.connect(self._handleTaskMessage)
            self._taskMessageThread.start()
            self._log.info("Task message thread started")

    def _handleTaskMessage(self, message):
        """Currently just a way to get an error message back into the tool from the YAAS task.

        Args:
            message (str): The task status message, i.e. error message.
        """
        QtGui.QMessageBox.critical(
            self,
            self.TOOL_NAME,
            "Error generating .FBX. Please contact TechArt if unable to resolve:\n'{}'".format(message),
        )

        try:
            self._taskMessageThread.quit()
        except RuntimeError as error:
            pass  # extra handling for "RuntimeError: Internal C++ object (TaskMessageThread) already deleted."
        self._taskMessageThread = None

    @classmethod
    def _loadFile(cls, localPath):
        """Load file into the Reference Editor tool.

        Args:
            localPath (str): abs path to file on local machine
        Returns:
            list[MocapReviewTrial.MocapReviewTrial]: The newly created reference.
        """
        manager = Manager.Manager()
        return manager.CreateReferences(localPath)

    def _getTrial(self):
        manager = MocapCommands.Manager()
        trialId = manager.TrialID
        if trialId <= 0:
            raise GetTrialFromSceneError(
                "Unable to get trial from scene! Use Mocap> Watchstar Trial Tool to set it manually or contact TechArt."
            )
        trial = Context.animData.getTrialByID(trialId)
        if trial is None:
            raise GetTrialFromSceneError("Could not find animdata trial with ID: {}".format(trialId))
        return trial

    def _toggleButtons(self, setting):
        """Enable or disable settings widgets

        Args:
            setting (bool): Whether to enable or disable the widgets
        """
        self._alignRefine.setEnabled(setting)
        self._refNullWidget.setEnabled(setting)
        self._applyColor.setEnabled(setting)
        self._offsetTrack.setEnabled(setting)
        self._useManualOffset.setEnabled(setting)
        self._resetAlignment.setEnabled(setting)
        self._openColorWheel.setEnabled(setting)
        self._offsetAlignment.setEnabled(setting)

    def _handleGenerateRefineBttnClicked(self):
        self._generateRefineBttn.setEnabled(False)  # Don't let the user click it multiple times.
        try:
            trial = self._getTrial()
            self._jobSubmissionThread = JobSubmissionThread(self, trial)
            self._jobSubmissionThread.errorRaised.connect(self._handleThreadError)
            self._jobSubmissionThread.taskCreated.connect(self._handleTaskCreated)
            self._jobSubmissionThread.start()
            self._log.info("YAAS job submission thread started")
        except GetTrialFromSceneError as error:
            QtGui.QMessageBox.warning(self, self.TOOL_NAME, str(error))
            self._generateRefineBttn.setEnabled(True)
            return
        except Exception as error:
            self._log.critical(traceback.format_exc())
            self._setJobStatusWidget("Error Submitting Job")
            self._generateRefineBttn.setEnabled(True)
            raise error  # Reraise to trigger error report email.

        self._generateRefineBttn.setEnabled(True)
        self._generateRefineBttn.setVisible(False)
        self._setJobStatusWidget("Submitting Job")
        self._jobStatusLabel.setVisible(True)

    def _handleLoadRefineBttnClicked(self):
        # Ensure that the user is logged into the mocap depot (rsgperforce:1777)
        servers.mocapDepot.instance.login()

        if self.settings.value("checkPerforce", "false") == "true":
            if not servers.mocapDepot.exists(self._depotFbxPath):
                QtGui.QMessageBox.critical(
                    self,
                    self.TOOL_NAME,
                    ".FBX is missing from Perforce! Close and regenerate refine. If issue persists, contact TechArt.",
                )
                return

            results = servers.mocapDepot.P4.sync(self._depotFbxPath)
            if len(results) < 1 and self._localFbxPath is None:  # Hasn't been loaded in this session; try again.
                results = servers.mocapDepot.P4.sync(self._depotFbxPath)
                if len(results) < 1:
                    QtGui.QMessageBox.critical(
                        self,
                        self.TOOL_NAME,
                        "Failed to sync: {}\n\nTry loading again. If issue persists, contact TechArt.".format(
                            self._depotFbxPath
                        ),
                    )
                    return
            if self._localFbxPath is None:  # The first run of the session needs the local path populated.
                self._localFbxPath = servers.mocapDepot.getClientPath(self._depotFbxPath)
                self._log.info(self._localFbxPath)

        if not os.path.exists(self._localFbxPath):
            QtGui.QMessageBox.critical(
                self,
                self.TOOL_NAME,
                "Local .FBX is missing for '{}'!\n\n"
                "Close and regenerate refine. If issue persists, contact TechArt.".format(self._depotFbxPath),
            )
            return

        # Load file and update UI
        try:
            self._loadFile(self._localFbxPath)

            # Enable align button and color items red for easy identification
            self._repopulateAll()
            self._toggleButtons(True)

            # At time of implementation, there wasn't a clean way to refresh the Reference Editor tool.
            QtGui.QMessageBox.information(
                self,
                self.TOOL_NAME,
                "Loading complete! Please refresh the Reference Editor.",
            )
        except Exception as error:
            msg = "Error loading MocapRefinedTrial: {}".format(self._localFbxPath)
            tracebackMsg = traceback.format_exc()
            self._log.debug(tracebackMsg)
            self._log.warning(msg)
            QtGui.QMessageBox.critical(self, self.TOOL_NAME, "{}\n\n".format(tracebackMsg))

    def _handleLoadRefineManualBttnClicked(self):
        """Loads widget that allows user to input a path to a file or look a local file up manually."""
        syncAndLoadPopup = SyncAndLoadRefineFileWidget()
        syncAndLoadPopup = SyncAndLoadRefineFileWidget()
        syncAndLoadPopup.exec_()

        # Run a process based on the button pressed in the popup UI
        if syncAndLoadPopup.findManually:
            self._loadFromLocalRefineFile()
        elif syncAndLoadPopup.filePath:
            self._loadFromPath(syncAndLoadPopup.filePath)

    def _loadFromPath(self, filePath):
        """Takes a path in and sets both local and depot fbx paths.

        Args:
            filePath (str): Path of the file to sync and/or load refine data from
        """
        # If a path uses backslashes, convert them to forward slashes
        filePath = "/".join(filePath.split("\\"))

        # Assumes that one of 2 paths is given, a local path starting with x: or a depot path starting with //depot
        if filePath.startswith(("x:", "X:")):
            self._localFbxPath = filePath
            self._depotFbxPath = filePath.replace("x:", "//depot").replace("X:", "//depot")
        elif filePath.startswith("//depot"):
            self._depotFbxPath = filePath
            self._localFbxPath = filePath.replace("//depot", "X:")
        self._handleLoadRefineBttnClicked()

    def _loadFromLocalRefineFile(self):
        """Opens up a file dialog when the manual load button is pressed. Calls regular load handler afterwards."""
        popup = QtGui.QFileDialog()
        popup.setWindowTitle("Select an FBX file")
        popup.setNameFilters(["*.rsrefine.fbx", "*.RSREFINE.FBX"])
        popup.setDirectory(self.mostRecentDir)
        popup.setFileMode(QtGui.QFileDialog.ExistingFile)

        if popup.exec_() and popup.selectedFiles() and popup.selectedFiles()[0]:
            self.mostRecentDir = os.path.dirname(popup.selectedFiles()[0])
            self.settings.setValue("mostRecentDir", self.mostRecentDir)
            filePath = popup.selectedFiles()[0]
            self._localFbxPath = filePath
            # To get the depot path, replace X: with //depot
            self._depotFbxPath = filePath.replace("x:", "//depot").replace("X:", "//depot")
            self._handleLoadRefineBttnClicked()

    def _handleAlignRefineBttnClicked(self):
        """Align items from the selected reference to the select stage

        Args:
            value (double): Value passed by the manual offset spin boxes.
        """
        stage = self.stages.get(self._stageWidget.currentText())
        if not stage:
            return
        stage = stage.GetReferenceModels()[0]
        refItem = self._getRefItem()
        selected = [self._refNullDict.get(item.text()) for item in self._refNullWidget.selectedItems()]
        if refItem and stage:
            alignTransPos = mobu.FBVector3d()
            alignRotPos = mobu.FBVector3d()
            stage.GetVector(alignTransPos, mobu.FBModelTransformationType.kModelTranslation, True)
            stage.GetVector(alignRotPos, mobu.FBModelTransformationType.kModelRotation, True)

            if self._offsetAlignment.isChecked():
                bonusTranslation = mobu.FBVector3d(self._xMod.value(), self._yMod.value(), self._zMod.value())
                alignTransPos = alignTransPos + bonusTranslation

            for item in selected:
                item.SetVector(alignTransPos, mobu.FBModelTransformationType.kModelTranslation, True)
                item.SetVector(alignRotPos, mobu.FBModelTransformationType.kModelRotation, True)

    def _displayManualOffset(self, value=None):
        """Toggle whether the manual keyframe offset widget is enabled

        Args:
            value (bool): Whether or not to enable/disable the widget
        """
        self._manualFrame.setEnabled(value)

    def _displayOffsetAlignment(self, value=None):
        """Toggle whether the alignment offset widgets are enabled

        Args:
            value (bool): Whether or not to enable/disable the widgets
        """
        self._xMod.setEnabled(value)
        self._yMod.setEnabled(value)
        self._zMod.setEnabled(value)
        self._handleAlignRefineBttnClicked()

    def _handleReset(self):
        """Reset the current refined data"""
        stage = self.stages.get(self._stageWidget.currentText()).GetReferenceModels()[0]
        refItem = self._getRefItem()
        if refItem and stage:
            for idx in xrange(self._refNullWidget.count() - 1):
                item = self._refNullDict.get(self._refNullWidget.item(idx).text())
                item.SetVector(mobu.FBVector3d(0, 0, 0), mobu.FBModelTransformationType.kModelTranslation, True)
                item.SetVector(mobu.FBVector3d(0, 0, 0), mobu.FBModelTransformationType.kModelRotation, True)

    def _handleToggleWireFrame(self):
        """Set all items in the scene to wireframe view or normal view"""
        refItem = self._getRefItem()
        for item in refItem.GetReferenceModels():
            if isinstance(item, mobu.FBModel):
                if self._wireDisplay.isChecked():
                    item.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
                else:
                    item.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingTexture

    def _handleToggleVisible(self):
        refItem = self._getRefItem()
        for item in refItem.GetReferenceModels():
            if isinstance(item, mobu.FBModel):
                item.Show = self._showRefine.isChecked()

    def _colorMeshItems(self):
        """Color mesh items in refine data reference"""
        refItem = self._getRefItem()
        if not refItem:
            # Can't color meshes if there aren't any
            return

        red = self._colorPicker.currentColor().red() / 255.0
        green = self._colorPicker.currentColor().green() / 255.0
        blue = self._colorPicker.currentColor().blue() / 255.0
        namespaceItems = Namespace.GetContents(refItem.GetFBNamespace())
        materials = [item for item in namespaceItems if isinstance(item, mobu.FBMaterial)]
        joints = [item for item in namespaceItems if isinstance(item, mobu.FBModelSkeleton)]
        unlisted = [
            component
            for component in Globals.Components
            if isinstance(component, mobu.FBMaterial) and re.search("RefineColor ", component.Name)
        ]
        materials.extend(unlisted)

        toSet = mobu.FBColor(red, green, blue)
        for material in materials:
            material.Diffuse = toSet
        for joint in joints:
            joint.PropertyList.Find("Color RGB").Data = toSet

        Globals.Scene.Evaluate()

    def _hiddenColorChanged(self, color):
        """Sync the main color picker up with the popup color picker"""
        self._colorPicker.setCurrentColor(color)

    def _showHiddenColorPicker(self):
        """Display the popup color wheel"""
        self._hiddenColorPicker.show()

    def _repopulateAll(self):
        """Populate refine data combo box, refine items list, and slate combo box"""
        self._populateRefineOptions()
        self._populateRefineItems()
        self._populateSlateKeys()
        self._populateStages()

    def _populateStages(self):
        """Repopulate the list of stages to align to"""
        self._stageWidget.clear()
        stages = self.manager.GetReferenceListByType(MocapStage.MocapStage)
        self.stages = {":".join([stage.Namespace, stage.Name]): stage for stage in stages}
        self._stageWidget.addItems([stage for stage in self.stages])

    def _refineTextChanged(self):
        """Populate refine items list, and slate combo box"""
        self._populateRefineItems()
        self._populateSlateKeys()

    def _populateRefineOptions(self):
        """Populate the refine data combo box"""
        original = self._refineDict
        self._refineDict = {}
        self._refineWidget.clear()
        existing = self.manager.GetReferenceListByType(MocapReviewTrial.MocapReviewTrial)
        if existing:
            self._refineWidget.addItems([":".join([refine.Namespace, refine.Name]) for refine in existing])
            self._refineDict = {":".join([refine.Namespace, refine.Name]): refine for refine in existing}
            self._refineWidget.setCurrentIndex(0)
            newItems = [ref for ref in self._refineDict.keys() if ref not in original]
            if newItems:
                self._refineWidget.setCurrentIndex(self._refineWidget.findText(newItems[0]))

    def _populateRefineItems(self):
        """Repopulate list of offset nulls for the selected refine data"""
        refItem = self._refineDict.get(self._refineWidget.currentText())
        if refItem and refItem.GetReferenceModels():
            self._refNullWidget.clear()
            topItem = Character.GetTopLevelItem(refItem.GetReferenceModels()[0])
            filteredItems = topItem.Children
            self._refNullWidget.addItems([item.Name for item in filteredItems])
            self._refNullDict = {item.Name: item for item in filteredItems}

    def _populateSlateKeys(self):
        """Populate the slate keyframes combo box."""
        self._keysWidget.clear()
        self._keysWidget.addItems([str(key) for key in self._getSlateKeys()])

    def _getSlateKeys(self, maxClumpSeperation=120):
        """Get keyframes at which there are slate claps in the current refine data reference.
        TODO WIP: This is largely taken from the Clapper code, which requires exact name matches,
            talk to Matt about how to approach this/whether to update Clapper

        Args:
            maxClumpSeperation (int, optional): The number of frames to clump groups of clumps together
        Returns:
            list[int]: List of clap keyframes
        """
        refItem = self._getRefItem()
        if not refItem:
            # No refine data has been imported
            return []
        track = refItem.GetStoryTrack()
        clip = refItem.GetStoryClip() or track.Components[0]
        clip.MakeWritable()
        track.AcceptKey = True

        # Get the first slate clapper in the reference
        slate = None
        for item in refItem.GetReferenceModels():
            if "SlateClapperTop_bone" in item.Name and isinstance(item, mobu.FBModelSkeleton):
                slate = item
                break
        slateAnimationNode = clip.GetAffectedAnimationNodes(slate)[0]

        claps = []
        for channel in slateAnimationNode.Nodes:
            if str(channel.Name).lower() != "z":
                continue

            clapperDict = {}
            for idx in xrange(channel.FCurve.Keys[0].Time.GetFrame(), channel.FCurve.Keys[-1].Time.GetFrame()):
                clapperDict[idx] = channel.FCurve.Evaluate(mobu.FBTime(0, 0, 0, idx))
            sortedClapperList = Clapper._AnalyzeClapperData(clapperDict)

            # Dynamically sort the claps into "clumps"
            clapParts = []
            currentClapPart = []
            for part in sortedClapperList:
                if len(currentClapPart) == 0:
                    currentClapPart.append(part)
                    continue
                if (part - currentClapPart[0]) <= maxClumpSeperation:
                    currentClapPart.append(part)
                else:
                    clapParts.append(currentClapPart)
                    currentClapPart = [part]
            clapParts.append(currentClapPart)

            # Now get the last one from each "clump"
            claps.extend([clapPart[-1] for clapPart in clapParts if len(clapPart) > 0])

        track.AcceptKey = False
        return claps

    def _offsetStoryTrack(self):
        """Move the story track by the amount specified by the user"""
        # Check for missing keyframe option
        if not self._keysWidget.currentText() and not self._useManualOffset.isChecked():
            return

        # Get ref item, clip and track
        refItem = self._getRefItem()
        track = refItem.GetStoryTrack()
        clip = refItem.GetStoryClip() or track.Components[0]

        # Get the desired offset keyframe
        offsetKey = int(self._keysWidget.currentText())
        if self._useManualOffset.isChecked():
            offsetKey = self._manualFrame.value()

        # Apply offset
        if offsetKey is not None:
            clip.MoveTo(mobu.FBTime(0, 0, 0, -offsetKey), True)

    def _getRefItem(self):
        """Use most recent refined import or the first one in the reference editor

        Returns:
            MocapReviewTrial.MocapReviewTrial: The current MocapReviewTrial reference selected from the combo box
        """
        if not self._refineWidget.currentText() or not self._refineDict:
            return None
        return self._refineDict.get(self._refineWidget.currentText())

    def createMenu(self):
        """Creates a menu bar for the widget

        Returns:
            QtGui.QMenuBar: The menu widget containing a settings menu
        """
        self.menuBar = QtGui.QMenuBar(self)

        self.settingsMenu = QtGui.QMenu("Settings")
        self.menuBar.addMenu(self.settingsMenu)

        self.checkPerforce = QtGui.QAction("Check for files in Perforce before loading", self)
        self.checkPerforce.setCheckable(True)
        self.checkPerforce.setChecked(self.settings.value("checkPerforce", "false") == "true")
        self.checkPerforce.triggered.connect(
            lambda *args: self.settings.setValue("checkPerforce", self.checkPerforce.isChecked())
        )

        self.settingsMenu.addAction(self.checkPerforce)

        return self.menuBar


if __name__ == "__builtin__":  # Run from the Python editor when developing.
    dialog = MocapReviewToolWidget()
    dialog.show()
