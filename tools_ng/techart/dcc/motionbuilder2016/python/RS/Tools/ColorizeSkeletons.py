'''
Author: Jason Hayes <jason.hayes@rockstarsandiego.com>

This module is for visualizing bone angle thresholds.  For more information on
how this system works, please visit the following page:
    
https://devstar.rockstargames.com/wiki/index.php/Skeleton_Colorization_System
'''

import os
import sys
import xml.etree.cElementTree
import re
import math

from pyfbsdk import *

import RS.Utils.Vector
import RS.Config


# XML related names.
XML_TAG_SKELETON            = 'skeleton'
XML_TAG_BONE                = 'bone'

XML_ATTR_ERROR_ANGLE        = 'errorAngle'
XML_ATTR_WARNING_THRESHOLD  = 'warningThreshold'
XML_ATTR_NAMESPACE          = 'nameSpace'


# Holds the bones defined in the config file.
global refBones
refBones = {}

# Which bones to colorize.  This gets populates while each skeleton is being evaluated.
global bonesToColorize
bonesToColorize = {}

global configFilename
configFilename = RS.Config.Tool.Path.Root + '\\techart\\etc\\config\\characters\\skeletonColorizationSettings.xml'

if not os.path.isfile( configFilename ):
    sys.stderr.write( 'Could not find the skeleton colorization settings file!  Expected it here: {0}\n'.format( configFilename ) )

# Holds all of the skeletons for the current scene.
global skeletons
skeletons = {}

# Track whether the system is enabled.
global __enabled
__enabled = False

# Track previous enabled state of the system.
global __oldEnabledState
__oldEnabledState = False

global __systemInitialized
__systemInitialized = False

# Keep track of when we added the callback, since apparantly MoBu
# isn't tracking this itself.  We don't ever want to add more than one.
global __onUIIdleRefCount
__onUIIdleRefCount = 0


## Classes ##

class RsSkeletonBone( object ):
    '''
    Represents a skeleton bone, and contains pointers to the actual FBSkeletonModel object.
    '''
    def __init__( self, parent, skeletonNode ):
        self.name = skeletonNode.Name
        
        self.color = FBColor( 1, 1, 0 )
        self.originalColor = FBColor( skeletonNode.Color.GetList()[ 0 ], skeletonNode.Color.GetList()[ 1 ], skeletonNode.Color.GetList()[ 2 ] )
        
        # Skeleton node
        self.skeletonNode = skeletonNode
        self.skeletonNode.OnUnbind.Add( self.onUnbind )
        
        self.errorAngle = 75.0
        self.warningThreshold = 20.0
        
        self.parent = parent
        self.children = []
    
        
    def onUnbind( self, source, event ):
        print '({0} is being unbound!\n'.format( self.skeletonNode )
        
        
    def isWithinErrorThreshold( self, angle ):
        '''
        Determine if the supplied angle is within the error threshold.
        '''
        return angle <= self.errorAngle
 
        
    def isWithinWarningThreshold( self, angle ):
        '''
        Determine if the supplied angle is within the warning threshold.
        '''
        return angle > self.errorAngle and angle <= ( self.errorAngle + self.warningThreshold )
    
        
    def resetColor( self ):
        '''
        Resets the FBSkeletonModel object back to its original color.
        '''
        
        # When opening a new scene, and the system is enabled, we hit
        # unbound versions of the skeleton node pointers.  Unfortunately,
        # there is no way for me to trap and clean this up beforehand, so
        # I have to wrap it up in a try/except. :(
        try:
            self.skeletonNode.Color = self.originalColor
            
        except:
            pass
    
        
    def getAngle( self, nextBone ):
        '''
        Calculates the angle between this bone and the supplied bone.
        '''
        currentBone = self.skeletonNode
        
        # The angle to return.
        theta = None
        
        if len( nextBone.children ) > 0:
            
            # TODO: Deal with multiple children.
            nextBoneChild = nextBone.children[ 0 ].skeletonNode
            
            nextBone = nextBone.skeletonNode
            
            currentBoneVec = FBVector3d()
            nextBoneVec = FBVector3d()
            nextBoneChildVec = FBVector3d()
            
            currentBone.GetVector( currentBoneVec, FBModelTransformationType.kModelTranslation )
            nextBone.GetVector( nextBoneVec, FBModelTransformationType.kModelTranslation )
            nextBoneChild.GetVector( nextBoneChildVec, FBModelTransformationType.kModelTranslation )
            
            # Create vector from current bone to next joint.
            a = RS.Utils.Vector.RsVector3()
            a.convertFromFBVector3d( currentBoneVec )
            
            b = RS.Utils.Vector.RsVector3()
            b.convertFromFBVector3d( nextBoneVec )
            
            c = RS.Utils.Vector.RsVector3()
            c.convertFromFBVector3d( nextBoneChildVec )
            
            currentBoneVec = a - b
            nextBoneVec = c - b
            
            currentBoneVec.normalize()
            nextBoneVec.normalize()
        
            theta = math.degrees( currentBoneVec.angle( nextBoneVec ) )
            
        return theta
        


class RsSkeleton( object ):
    '''
    Represents the skeletal hierarchy of a character in the scene.
    '''
    def __init__( self, namespace, rootNode ):
        self.namespace  = namespace
        self.version    = None
        self.root       = RsSkeletonBone( None, rootNode )
        self.bones      = {}
        
        # Build the skeleton hierarchy.
        self.recurseBuildSkeleton( rootNode, self.root )
        
    
    def __recurseColorize( self, bone ):
        '''
        Recursively iterates over the skeleton hierarchy and colorizes the bones
        based on the criteria.
        '''
        global bonesToColorize
                
        for child in bone.children:
            angle = bone.getAngle( child )
            
            if bone.isWithinErrorThreshold( angle ) and angle != None:
                bonesToColorize[ bone ] = FBColor( 1, 0, 0 )
                bonesToColorize[ child ] = FBColor( 1, 0, 0 )
                
            elif bone.isWithinWarningThreshold( angle ) and angle != None:
                delta = ( angle - bone.errorAngle ) / ( ( bone.errorAngle + bone.warningThreshold ) - bone.errorAngle )
                bonesToColorize[ bone ] = FBColor( 1, delta, 0 )
                bonesToColorize[ child ] = FBColor( 1, delta, 0 )
                
            else:
                bone.skeletonNode.Color = FBColor( 1, 1, 0 )
                
            self.__recurseColorize( child )
            
        
    def colorize( self ):
        '''
        Evaluates the virtual skeleton and colorizes the bones
        based on the criteria.
        '''
        global bonesToColorize
        bonesToColorize = {}
        
        self.__recurseColorize( self.root )
        
        for bone, color in bonesToColorize.iteritems():
            bone.skeletonNode.Color = color
            
    
    def resetColors( self ):
        '''
        Reset all bones in the hierarchy back to their original color.
        '''
        for boneName, bone in self.bones.iteritems():
            bone.resetColor()
            
          
    def recurseBuildSkeleton( self, currentBone, currentRsBone ):
        '''
        Recursively iterates over the skeletal hierachy of a character.  This will
        only add bones to this hierarchy if the bone exists in the config file.
        '''
        global refBones
        
        for childBone in currentBone.Children:
            if isinstance( childBone, FBModelSkeleton ):
                
                # See if we should add this bone to the skeleton or not.
                refBoneName = None
                
                # Search to see if any of the bones in the mapping are
                # a match to the current bone name.  Unfortunately, we have to do
                # this to get around bones that get numbers appended to the end of them.
                for boneName, boneData in refBones.iteritems():
                    if boneName in childBone.Name:
                        refBoneName = boneName
                        break
                        
                # We found a match, so add the bone to our skeleton. 
                if refBoneName:
                    
                    # Get the data.
                    errorAngle, warningThreshold = refBones[ refBoneName ]
                    
                    # Create a new RsSkeletonBone object.
                    rsBone = RsSkeletonBone( currentRsBone, childBone )
                    rsBone.errorAngle = errorAngle
                    rsBone.warningThreshold = warningThreshold
                    
                    self.bones[ rsBone.name ] = rsBone
                    
                    # Add the current RsSkeletonBone as a child of the incoming bone.
                    currentRsBone.children.append( rsBone )
                            
                    # Keep searching...
                    self.recurseBuildSkeleton( childBone, rsBone )
                    
                # Bone wasn't in the mapping from the config file, so continue searching...
                else:
                    self.recurseBuildSkeleton( childBone, currentRsBone )
                                


## Callbacks ##
    
def onFileOpenCallback( source, event ):
    '''
    Registered to fire off when someone clicks open.
    '''
    global __enabled
    global __oldEnabledState
    
    # Store off the current state of the system, so that we can
    # restore it later.
    __oldEnabledState = __enabled
    
    # Turn the system off, at least temporarily.
    enable( False )
    
    
def onFileOpenCompletedCallback( source, event ):
    '''
    Registered to fire off whenever an open file has completed.  The purpose here
    is to update the system with new skeletons.
    
    Note: This callback does not get called in Motionbuilder 2012 because of a bug.  This
    is apparently fixed in 2013.
    '''
    global __enabled
    global __oldEnabledState
    
    # Restore system, if previously on.
    __enabled = __oldEnabledState
    enable( __enabled )
    
    
def onFileSaveCallback( source, event ):
    '''
    Disable the system temporarily so that we don't save the current file with the bone colors.
    '''
    global __enabled
    global __oldEnabledState
    
    # Restore system, if previously on.
    __oldEnabledState = __enabled
    enable( False )
    
    
def onFileSaveCompletedCallback( source, event ):
    '''
    Restore the system after a save was completed.
    '''
    global __enabled
    global __oldEnabledState
    
    # Restore system, if previously on.
    __enabled = __oldEnabledState
    enable( __enabled )
    
    
def colorizeSkeletonsCallback( source, event ):
    '''
    Registered as an FBApplication().OnUIIdle callback.  This function
    is evaluated all of the time, and should be as fast as possible.
    '''
    global skeletons
    global __enabled
    
    if __enabled:
        for name, skeleton in skeletons.iteritems():
            skeleton.colorize()

 
## Functions ##

def unregisterCallbacks():
    '''
    Unregisters our callbacks when Motionbuilder closes so that we don't
    crash the application.
    '''
    FBApplication().OnFileExit.Remove( colorizeSkeletonsCallback )
    FBApplication().OnFileExit.Remove( onFileOpenCallback )
    FBApplication().OnFileExit.Remove( onFileOpenCompletedCallback )
    FBApplication().OnFileExit.Remove( onFileSaveCallback )
    FBApplication().OnFileExit.Remove( onFileSaveCompletedCallback )
     
  
def initialize():
    '''
    Initialize the colorization system.
    '''
    global __systemInitialized
    
    if not __systemInitialized:
        loadReferenceSkeletonsFromConfig( configFilename )
        
        # Register callbacks.
        
        # This will be evaluated on every cycle update from the application.     
        FBSystem().OnUIIdle.Add( colorizeSkeletonsCallback )
        
        # Re-evaluate the scene skeletons after someone has opened a new file.
        FBApplication().OnFileOpen.Add( onFileOpenCallback )
        
        # BUG: This is apparently never called in 2012.  Supposedly fixed in 2013 SP-1.  Autodesk bug #MOBU-4329
        FBApplication().OnFileOpenCompleted.Add( onFileOpenCompletedCallback )
        
        # Turn the system off, and restore on a save.
        FBApplication().OnFileSave.Add( onFileSaveCallback )
        FBApplication().OnFileSaveCompleted.Add( onFileSaveCompletedCallback )
        
        # Unregister our callbacks when the application closes.
        FBApplication().OnFileExit.Add( unregisterCallbacks )
        
        __systemInitialized = True

   
def isEnabled():
    '''
    Get whether or not the system is enabled.
    '''
    global __enabled
    return __enabled
    
   
def enable( state ):
    '''
    Enable or disable the colorization system.
    '''
    global __systemInitialized
    global __enabled
    global __onUIIdleRefCount
    
    global skeletons
    
    if not __systemInitialized:
        initialize()
        __systemInitialized = True
    
    # When turning the colorization on, evaluate the scene and update the skeletons.
    if state:
        __enabled = True
        
        if __onUIIdleRefCount == 0:
            FBSystem().OnUIIdle.Add( colorizeSkeletonsCallback )
            __onUIIdleRefCount += 1
            
        # Reset colors first before we collect the skeletons.
        for skeletonName, skeleton in skeletons.iteritems():
            skeleton.resetColors()
            
        # Collect the current scene skeletons.
        collectSceneSkeletons()
        
        # Force them to colorize.
        colorizeSkeletonsCallback( None, None )
        
    else:
        __enabled = False
        
        # Reset the bone colors back to their original state.
        for skeletonName, skeleton in skeletons.iteritems():
            skeleton.resetColors()
        
        # Remove the callback.
        if __onUIIdleRefCount == 1:
            FBSystem().OnUIIdle.Remove( colorizeSkeletonsCallback )
            __onUIIdleRefCount -= 1
    

def reloadConfigSettings():
    '''
    Reloads settings from the config file and re-evaluates the skeletons in the
    scene.  Use this if you want to iterate on settings in the config file and see
    the changes propagate to the scene.
    '''
    global __enabled
    global bonesToColorize
    bonesToColorize = {}
    
    prevEnabledState = __enabled
    
    enable( False )
    
    loadReferenceSkeletonsFromConfig( configFilename )
    collectSceneSkeletons()
    
    enable( prevEnabledState )
 
 
def collectSceneSkeletons():
    '''
    For each character in the scene, create an RsSkeleton object that
    virtually represents the hierarchy for each.  The bones in the hierarchy are
    based on the bones that are defined in the config file.
    '''
    global skeletons
    skeletons = {}
    
    characters = FBSystem().Scene.Characters
    
    for character in characters:
        
        # Locate the skeleton root, and begin constructing our virtual
        # RsSkeleton from the root bone.
        hipsLink = character.PropertyList.Find( 'HipsLink' )
        
        if hipsLink:
            for obj in hipsLink:
                if obj.Name == 'SKEL_ROOT':
                    
                    # By instantiating the RsSkeleton object, we are automatically
                    # building the hierarchy from the root bone.
                    skeleton = RsSkeleton( character.Name, obj )
                    
                    # Get the skeleton version.
                    rawTags = obj.PropertyList.Find( 'UDP3DSMAX' )
                    
                    if rawTags:
                        tags = re.compile( '[\n\r]' ).split( rawTags.Data )
                        
                        for tag in tags:
                            if 'SkeletonVersionTag' in tag:
                                skeleton.version = float( str( tag ).split( '=' )[ -1 ] )
                                
                    # Store the skeleton so that it can be later evaluated.
                    skeletons[ skeleton.namespace ] = skeleton

        
def loadReferenceSkeletonsFromConfig( filename ):
    '''
    Parse the config which defines what bones the system should colorize, as well as
    other criteria.
    '''
    global refBones
    
    if os.path.isfile( configFilename ):
        doc = xml.etree.cElementTree.parse( filename )
        root = doc.getroot()
        
        if root:
            refBones = {}
            
            xmlSkeletons = root.findall( XML_TAG_SKELETON )
            
            for xmlSkeleton in xmlSkeletons:
                xmlBones = xmlSkeleton.findall( XML_TAG_BONE )
                namespace = xmlSkeleton.get( XML_ATTR_NAMESPACE )
                
                for xmlBone in xmlBones:
                    errorAngle = float( xmlBone.get( XML_ATTR_ERROR_ANGLE ) )
                    warningThreshold = float( xmlBone.get( XML_ATTR_WARNING_THRESHOLD ) )
                    boneName = namespace + xmlBone.text
                    
                    if boneName not in refBones:
                        refBones[ boneName ] = ( errorAngle, warningThreshold )
                    
                    else:
                        print "Found a duplicate bone name ({0})!".format( boneName )
      