import unittest

from RS.Tools.Shelftastic.UnitTest import base


def Run():
    """Run unittests from this module"""
    unittest.TextTestRunner(verbosity=0).run(suite())


def suite():
    """A suite to hold all the tests from this module

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()

    for functionName in dir(Test_Shelf):
        if functionName.startswith("test_"):
            suite.addTest(Test_Shelf(functionName))

    return suite


class Test_Shelf(base.ShelftasticBase):
    """Tests shelf"""

    def test_Add(self):
        """Testing adding a shelf"""
        self.assertCreateTab(tabName="testShelf")

    def test_Delete(self):
        """Testing deleting a shelf"""
        tab = self.assertCreateTab(tabName="testShelf")
        self.assertDeleteTab(tab)

    def test_Rename(self):
        """Testing renaming a shelf"""
        self.assertRenameTab("testShelf2")

    def test_Load(self):
        """Testing loading a shelf"""
        self.assertLoadTab()
