"""
The Editor widget for editing shelf buttons
"""
import os

from PySide import QtCore, QtGui

from RS import Config
from RS.Tools.Shelftastic import ShelfComponents


class ShelftasticEditorWidget(QtGui.QMainWindow):
    """
    A Widget designed for users to edit a button, setting its icon path, name and any code it runs
    when executed
    """
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(ShelftasticEditorWidget, self).__init__(parent=parent)
        self.setWindowModality(QtCore.Qt.WindowModal)
        self.setupUi()
        
    def clear(self):
        """
        Method to clear the current button
        """
        self.setButton(ShelfComponents.ScriptButton(None, "", "", ""))
        
    def setButton(self, buttonCommand):
        """
        Method to set the current button being edited
        
        Args:
            buttonCommand (ScriptButton): The button to be edited
        """
        self._button = buttonCommand
        self._nameEditBox.setText(buttonCommand.Name())
        self._iconEditBox.setText(buttonCommand.IconPath())
        self._codeTextEdit.setText(buttonCommand.Code())
        self._codeFileTextEdit.setText(buttonCommand.CodeFilepath())
        
        if buttonCommand.RunType() == ShelfComponents.ShelftasticButtonRunTypeIndex.RunCode:
            self._codeRadioButton.setChecked(True)
        elif buttonCommand.RunType() == ShelfComponents.ShelftasticButtonRunTypeIndex.RunFile:
            self._filePathRadioButton.setChecked(True)
        
        self._handleCodeRaidoButtonChange()
    def getIcon(self, newText):
        """
        Get the Icon from a file path, if it exists
        
        Args:
            nextText (str): The file path
            
        Return:
            QImage: The image of the given filepath
        """
        if os.path.isfile(newText):
            return QtGui.QImage(newText)
        return QtGui.QImage()
        
    def saveButton(self):
        """
        Method to save the current edits to the button being edited
        """
        self._button.SetName(self._nameEditBox.text())
        self._button.SetCode(self._codeTextEdit.toPlainText())
        self._button.SetCodeFilePath(self._codeFileTextEdit.text())
        self._button.SetIconPath(self._iconEditBox.text())
        
        if self._codeRadioButton.isChecked():
            self._button.SetRunType(ShelfComponents.ShelftasticButtonRunTypeIndex.RunCode)
        elif self._filePathRadioButton.isCheckable():
            self._button.SetRunType(ShelfComponents.ShelftasticButtonRunTypeIndex.RunFile)
        
        self.hide()
        
    def setupUi(self):
        """
        Set up the UI and attach the connections
        """
        # Layouts
        layout = QtGui.QGridLayout()
        buttonLayout = QtGui.QHBoxLayout()
        codeLayout = QtGui.QGridLayout()
        codeStackWidgetLayout = QtGui.QVBoxLayout()
        fileStackWidgetLayout = QtGui.QGridLayout()
        
        # Widgets
        mainWidget = QtGui.QWidget()
        self._nameEditBox = QtGui.QLineEdit()
        self._iconEditBox = QtGui.QLineEdit()
        self._codeTextEdit = QtGui.QTextEdit()
        self._codeFileTextEdit = QtGui.QLineEdit()
        browseForFileButton = QtGui.QPushButton("...")
        self._iconPreview = QtGui.QLabel()
        browseForIconButton = QtGui.QPushButton("...")
        self._codeStackWidget = QtGui.QStackedWidget()
        self._codeRadioButton = QtGui.QRadioButton("Run Code")
        self._filePathRadioButton = QtGui.QRadioButton("Run File")
        
        saveButton = QtGui.QPushButton("Save")
        closeButton = QtGui.QPushButton("Close")

        # Configure Widgets
        self._iconPreview.setMaximumSize(QtCore.QSize(35,35))
        self._iconPreview.setMinimumSize(QtCore.QSize(35,35))
        codeLayout.setContentsMargins(0,0,0,0)
        buttonLayout.setContentsMargins(0,0,0,0)
        browseForFileButton.setMaximumWidth(20)
        browseForIconButton.setMaximumWidth(20)
        self._codeRadioButton.setChecked(True)
        
        # Assign Widgets to layouts
        layout.addWidget(QtGui.QLabel("Name:"), 0, 0)
        layout.addWidget(self._nameEditBox, 0, 1, 1, 3)
        layout.addWidget(QtGui.QLabel("Icon Path:"), 1, 0)
        layout.addWidget(self._iconEditBox, 1, 1)
        layout.addWidget(browseForIconButton, 1, 2)
        layout.addWidget(self._iconPreview, 2, 1, 1, 3)
        layout.addWidget(QtGui.QLabel("Icon Preview:"), 2, 0)
        layout.addWidget(self._iconPreview, 2, 1)
        layout.addLayout(codeLayout, 3, 0, 1, 3)
        
        buttonLayout.addWidget(closeButton)
        buttonLayout.addWidget(saveButton)
        
        codeLayout.addWidget(self._codeRadioButton, 0, 0)
        codeLayout.addWidget(self._filePathRadioButton, 0, 1)
        codeLayout.addWidget(self._codeStackWidget, 2, 0, 2, 2)
        
        codeStackWidgetLayout.addWidget(QtGui.QLabel("Code:"))
        codeStackWidgetLayout.addWidget(self._codeTextEdit)
        
        fileStackWidgetLayout.addWidget(QtGui.QLabel("FilePath:"), 0, 0)
        fileStackWidgetLayout.addWidget(self._codeFileTextEdit, 1 ,0)
        fileStackWidgetLayout.addWidget(browseForFileButton,1,1)
        
        for stackedLay in [codeStackWidgetLayout, fileStackWidgetLayout]:
            newWid = QtGui.QWidget()
            newWid.setLayout(stackedLay)
            self._codeStackWidget.addWidget(newWid)
        
        layout.addLayout(buttonLayout, 5, 0, 1, 3)
        
        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)
        
        # Connections
        saveButton.pressed.connect(self.saveButton)
        closeButton.pressed.connect(self.hide)
        self._iconEditBox.textChanged.connect(self._handleIconPathChange)
        browseForIconButton.pressed.connect(self._handleBrowseForIconPress)
        browseForFileButton.pressed.connect(self._handleBrowseForRunFilePress)
        self._codeRadioButton.clicked.connect(self._handleCodeRaidoButtonChange)
        self._filePathRadioButton.clicked.connect(self._handleCodeRaidoButtonChange)

    def _handleIconPathChange(self, newText):
        """
        Internal Method
        
        Handle the icon path changing and update the icon preview accordingly
        
        Args:
            newText (str): The new text in the edit box
        """
        resolvedPath = ShelfComponents.ShelfPathResolver.ResolvePath(newText)
        pixMap = QtGui.QPixmap.fromImage(self.getIcon(resolvedPath))
        if not pixMap.isNull():
            pixMap = pixMap.scaled(35,35)
        self._iconPreview.setPixmap(pixMap)

    def _handleCodeRaidoButtonChange(self):
        """
        Internal Method
        
        Handle the radio button change state, showing the code or the file path options
        """ 
        index = {self._codeRadioButton: 0,
                 self._filePathRadioButton: 1}
        for item, idx in index.iteritems():
            if item.isChecked():
                self._codeStackWidget.setCurrentIndex(idx)

    def _handleBrowseForRunFilePress(self):
        """
        Internal Method
        
        Handle the browsing of the run file dialog
        """
        filePath, notCan = QtGui.QFileDialog.getOpenFileName(self, 'File to Run','/home', "Python (*.py *.pyc)")
        if not notCan:
            return
        self._codeFileTextEdit.setText(filePath)

    def _handleBrowseForIconPress(self):
        """
        Internal Method
        
        Handle the browsing of the icon file dialog
        """
        defaultDir = "\\".join([Config.Script.Path.ToolImages, "Shelftastic"])
        filePath, notCan = QtGui.QFileDialog.getOpenFileName(self, 'Pick Image', defaultDir, "Images (*.png *.xpm *.jpg)")
        if not notCan:
            return
        self._iconEditBox.setText(filePath)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
   
    win = ShelftasticEditorWidget()
    win.show()
    sys.exit(app.exec_())