import unittest

from RS.Tools.Shelftastic.UnitTest import buttons, shelves


def getTestSuites():
    """Gathers suites together to test

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()
    suite.addTest(buttons.suite())
    suite.addTest(shelves.suite())
    return suite


def Run():
    """Runs unittests"""
    unittest.TextTestRunner(verbosity=2).run(getTestSuites())


if __name__ == "__main__":
    Run()
