"""
The shelf components which make up the underlying side of the shelf widget
"""
import copy
import os
import functools
import traceback
from xml.dom import minidom

from PySide import QtCore, QtGui

from RS.Tools.Shelftastic import FlowLayout
from RS import Config
from RS.Utils.Logging import MongoDatabase, Database


class ThisIsNotAShelftasticError(Exception):
    def __init__(self, value, code=None, filePath=None):
        self.value = value
        self.code = code
        self.filePath = filePath

    def __str__(self):
        moreInfo = ""
        if self.code is not None:
            moreInfo = "Code Run: {0}".format(self.code)
        if self.filePath is not None:
            moreInfo = "File Run: {0}".format(self.filePath)
        return "\nThis is not a Shelftastic Error:\n\n{0}\n\nOriginal Error:\n{1}".format(moreInfo, self.value)


class ShelftasticButtonRunTypeIndex(object):
    """
    Enum class for the button running of Code or File
    """
    RunCode = 0
    RunFile = 1


def Singleton(cls):
    """
    Singleton decorator

    Any class with this decorator will only be constructed once and any addational calls to
    constructed will return 1st instanced object
    """
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance


@Singleton
class ShelftasticToolAuditor(object):
    """
    Singleton class

    Class to store and get button names. Used to check if a button is new or hasnt been run before
    """
    def __init__(self):
        """
        Constructor
        """
        self._settings = QtCore.QSettings("RockstarSettings", "ShelftasticToolAuditor")
        self._commandsRun = []
        for cmd in self._settings.value("commandsRun", []):
            try:
                self._commandsRun.append(str(cmd))
            except UnicodeEncodeError, error:
                # Nothing to do here, but move on
                pass

    def HasCommandBeenRun(self, commandName):
        """
        Check if a command has been run before

        Args:
            commandName (str): The name of the command to check

        Return:
            bool: If the command has been run before
        """
        return commandName in self._commandsRun

    def CommandRun(self, commandName):
        """
        Store a command so it doesnt flag as 'new'

        Args:
            commandName (str): The name of the command to store
        """
        if self.HasCommandBeenRun(commandName):
            return
        self._commandsRun.append(commandName)
        self._settings.setValue("commandsRun", self._commandsRun)


class ShelfPathResolver(object):
    """
    Class to resolve a path with config paths in it

    Example Useage:
        ShelfPathResolver.ResolvePath("$Root/derp/derp2/derp.dep")

    """
    # Mapping dict, string to replace to config path
    _mappings = {
                "$TechArt" : Config.Script.Path.ToolImages,
                "$Root" : Config.Script.Path.Root,
                }

    def __init__(self):
        """
        Constructor
        """
        pass

    @classmethod
    def ResolvePath(cls, filePath):
        """
        Class Method

        Resolves a file path string with a config path in it.

        Example:
            "$Root/derp/derp2/derp.dep" => C:\\FolderRoot\\Folder\\derp\derp2\derp.dep"

        Args:
            filePath (string): The input file path

        Return:
            string: The final output string with config tags resolved
        """
        for key, path in cls._mappings.iteritems():
            filePath = filePath.replace(key, path)

        return os.path.abspath(filePath)


class DropableWidget(QtGui.QWidget):
    """
    Widget that allows for text data to be dropped on to it and to emit a signal with the dropped
    data

    Signals:
        dataDropped (str): Data has been dropped onto the widget, re-emit with the data dropped

    """
    dataDropped = QtCore.Signal(str)

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(DropableWidget, self).__init__(parent=parent)
        self.setAcceptDrops(True)
        self._isLocked = False

    def setLocked(self, value):
        """
        Set the lock state of the widget, if it will accept drops or not.

        Args:
            value (bool): If the tab is locked or not and should accept drops
        """
        self._isLocked = value

    def dragEnterEvent(self, event):
        """
        Re-implemented

        Event of the drag over the widget and it is allowed to be dropped
        """
        if not self._isLocked and event.mimeData().hasFormat("text/plain"):
            event.acceptProposedAction()

    def dropEvent(self, event):
        """
        Re-implemented

        Event of the drop on the widget, re-emit to the parent shelf widget
        """
        if self._isLocked:
            return

        self.dataDropped.emit(event.mimeData().text())
        event.acceptProposedAction()


class ScrollableFlowWidget(QtGui.QScrollArea):
    """
    Scrollable widget designed to house the flow widget, allowing for access to a scrollbar
    when using the flow layout

    Signal:
        horizontalScrollShown (): When the Horizontal scroll bar is shown
        horizontalScrollHidden (): When the Horizontal scroll bar is hidden
    """

    horizontalScrollShown = QtCore.Signal()
    horizontalScrollHidden = QtCore.Signal()

    def __init__(self, orientation=QtCore.Qt.Vertical, parent=None):
        """
        Constructor
        """
        super(ScrollableFlowWidget, self).__init__(parent=parent)
        self._layout = FlowLayout.FlowLayout()
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._scrollWidget = QtGui.QWidget()

        self._scrollOrientation = orientation

        self.setFrameShape(QtGui.QFrame.NoFrame)
        self.setFrameShadow(QtGui.QFrame.Plain)
        self.setLineWidth(0)
        self._horizontalScrollShownStatus = False

        self._scrollWidget.setLayout(self._layout)
        self.setWidget(self._scrollWidget)
        self.setWidgetResizable(True)

    def setOrientation(self, orientation):
        """
        Set the scrolling orientation of the shelf, if its vertical or horizontal

        Args:
            orientation (QtCore.Qt.Vertical|QtCore.Qt.Horizontal): The way to scroll
        """
        self._scrollOrientation = orientation

    def resizeEvent(self, event):
        """
        ReImplemented

        Resize the widget holding the flow layout to the size of the scroll area, and update the
        scroll area to show the scroll bars if needed

        Args:
            event (QEvent): Data from the event
        """
        if self._scrollOrientation == QtCore.Qt.Vertical:
            width = self.viewport().width()
            flowSize = QtCore.QSize(width, self._layout.heightForWidth(width))
            topLeft = self.viewport().rect().topLeft()
            self._scrollWidget.setMinimumWidth(0)
            self._scrollWidget.setGeometry(topLeft.x(), topLeft.y(), flowSize.width(), flowSize.height())
            self.viewport().update()
            if self._horizontalScrollShownStatus:
                self._horizontalScrollShownStatus = False
                self.horizontalScrollHidden.emit()

        elif self._scrollOrientation == QtCore.Qt.Horizontal:
            height = self.viewport().height()
            flowSize = QtCore.QSize(self._layout.maxWidth(), height)
            topLeft = self.viewport().rect().topLeft()
            self._scrollWidget.setMinimumWidth(flowSize.width())
            self._scrollWidget.setGeometry(topLeft.x(), topLeft.y(), flowSize.width(), flowSize.height())
            self.viewport().update()

            if flowSize.width() > self.viewport().width():
                if not self._horizontalScrollShownStatus:
                    self._horizontalScrollShownStatus = True
                    self.horizontalScrollShown.emit()
            else:
                if self._horizontalScrollShownStatus:
                    self._horizontalScrollShownStatus = False
                    self.horizontalScrollHidden.emit()

        return QtGui.QScrollArea.resizeEvent(self, event)

    def addWidget(self, widget):
        """
        Add a widget to the scroll area. Will get appended to the end of the widgets

        Args:
            widget (QWidget): Widget to add the scroll area layout
        """
        self._layout.addWidget(widget)

    def removeWidget(self, widget):
        """
        Remove a widget for the scroll area

        Args:
            widget (QWidget): Widget to remove from the scroll area layout
        """
        self._layout.removeWidget(widget)

    def getLayout(self):
        """
        Get the underlying layout used for the scrollableFlowWdiget

        Return
            FlowLayout: The layout the widget is using
        """
        return self._layout


class ScriptTab(QtCore.QObject):
    """
    Object to represent the script tab which makes up the shelf

    Signals:
        dataChanged (): The tab has changed, be it the adding/removing or editing of a button
        buttonToBeEdited (ScriptButton): A button is wanting to be edited
        lockStatusChanged (ScriptTab, bool): The Lock status on a script tab has changed.
        horizontalScrollShown (): When the Horizontal scroll bar is shown
        horizontalScrollHidden (): When the Horizontal scroll bar is hidden
    """
    dataChanged = QtCore.Signal()
    buttonToBeEdited = QtCore.Signal(object)
    lockStatusChanged = QtCore.Signal(object, object)
    horizontalScrollShown = QtCore.Signal()
    horizontalScrollHidden = QtCore.Signal()

    def __init__(self, name, sourceLocation=None):
        """
        Constructor

        Args:
            name (str): The name of the tab
            sourceLocation (str): The file location to the xml on disk which tab data is saved to
        """
        super(ScriptTab, self).__init__()
        self._name = name
        self._buttons = []
        self._tab = None
        self._tabWidget = None
        self._setupUi()
        self._sourceLocation = sourceLocation
        self.IsLocked()
        self._smallButtons = False
        self._lableButtons = False

    def SmallButton(self):
        """
        Get if the use of smaller button icons on or off

        Returns:
            bool: The True/False value to use Small or Big buttons
        """
        return self._smallButtons

    def ButtonLabels(self):
        """
        Get if the button labels on or off

        Returns:
            bool: The True/False value to show or hide the icon
        """
        return self._lableButtons

    def SetSmallButton(self, value):
        """
        Set the use of smaller button icons on or off

        Kargs:
            Value (bool): The True/False value to use Small or Big buttons
        """
        self._smallButtons = value
        for button in self._buttons:
            button.SetSmallButton(value)

    def SetButtonLabels(self, value):
        """
        Show or hide the button labels.
        True will Show them, False will Hide them

        Args:
            Value (bool): The True/False value to show or hide the icon
        """
        self._lableButtons = value
        for button in self._buttons:
            button.SetButtonLabels(value)

    def GetSourceLocation(self):
        """
        Get the location of the xml file

        Return:
            str: The file location to the xml on disk which tab data is saved to
        """
        return self._sourceLocation or ""

    def SetSourceLocation(self, newLocation):
        """
        Set the location of the xml file

        Args:
            newLocation (str): The new file location to the xml on disk which tab data is saved to
        """
        self._sourceLocation = newLocation

    def IsLocked(self):
        """
        Get the current Locked state of the source XML file, and if the tab can be edited.
        This is tied to the read-only state of the source file

        Return:
            bool: If the source file is read-only/locked
        """
        if not os.path.isfile(self.GetSourceLocation()):
            return False
        locked = not os.access(self.GetSourceLocation(), os.W_OK)
        self._updateLockedStatus(locked)
        return locked

    def _updateLockedStatus(self, state):
        """
        Internal Method

        Update the right click context menu to hide/show the menu depending on the given state.
        A state of True would hide the context menu, whereas a state of False would allow the menu
        to be shown

        Args:
            state (bool): If the tab is locked or not
        """
        policy = QtCore.Qt.NoContextMenu
        if not state:
            policy = QtCore.Qt.ActionsContextMenu
        self._tab.setContextMenuPolicy(policy)
        self._tab.setLocked(state)
        for button in self._buttons:
            button.Button().setContextMenuPolicy(policy)
        self.lockStatusChanged.emit(self, state)

    def Name(self):
        """
        Get the name of the tab

        Return:
            str: the name of the tab
        """
        return self._name

    def SetName(self, newName):
        """
        Set the tab name. This will NOT update the name on the shelf, that must be done at the shelf
        level

        Args:
            newName (str): the new tab name
        """
        self._name = newName

    def GetButtons(self):
        """
        Get the tab buttons

        Return:
            list of ScriptButton: The buttons which are on the tab
        """
        return self._buttons

    def GetTab(self):
        """
        Get the underlying tab object

        Return:
            QWidget: The widget which makes up the tab
        """
        return self._tab

    def SetScrollingOrientation(self, orientation):
        """
        Set the scrolling orientation of the shelf, if its vertical or horizontal

        Args:
            orientation (QtCore.Qt.Vertical|QtCore.Qt.Horizontal): The way to scroll
        """
        self._tabWidget.setOrientation(orientation)

    def _setupUi(self):
        """
        Set up the ui components
        """
        # Create Layouts
        tabLayout = QtGui.QVBoxLayout()

        # Create Widgets
        self._tabWidget = ScrollableFlowWidget()
        self._tab = DropableWidget()

        # Configure Widgets & Layouts
        tabLayout.setContentsMargins(0, 0, 0, 0)
        self._tab.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)

        # Assign layouts to widgets
        tabLayout.addWidget(self._tabWidget)
        self._tab.setLayout(tabLayout)

        # connect signals
        self._tab.dataDropped.connect(self._addNewButtonFromDrop)
        self._tabWidget.horizontalScrollShown.connect(self._handleHorizontalScrollShown)
        self._tabWidget.horizontalScrollHidden.connect(self._handleHorizontalScrollHidden)

        # Right click context menu
        newButtonAction = QtGui.QAction(self._tab)
        newButtonAction.setText("Add New Button")
        newButtonAction.triggered.connect(functools.partial(
                                                            self._handleNewButtonPress,
                                                            self._tabWidget.getLayout()
                                                            )
                                          )
        self._tab.addAction(newButtonAction)

    def _handleHorizontalScrollShown(self):
        """
        Internal Method

        Re-emit the showing of the horizontal scroll bar from the scroll area widget
        """
        self.horizontalScrollShown.emit()

    def _handleHorizontalScrollHidden(self):
        """
        Internal Method

        Re-emit the hiding of the horizontal scroll bar from the scroll area widget
        """
        self.horizontalScrollHidden.emit()

    def _handleNewButtonPress(self, layout):
        """
        Internal Method

        New button pressed, add a new button to the shelf and get the parent to load the
        editorDialog with the new button

        Args:
            layout (QLayout): The layout to add the widget to
        """
        newBut = ScriptButton(self, "", "", "")
        self.AddButton(newBut)
        self.buttonToBeEdited.emit(newBut)

    def RemoveButton(self, button):
        """
        Remove a give button from the shelf

        Args:
            button (ScriptButton): The button to remove
        """
        self._buttons.remove(button)
        self._tabWidget.removeWidget(button.Button())
        button.Button().deleteLater()
        del(button)
        self.dataChanged.emit()

    def AddButton(self, button):
        """
        Add a ScriptButton to shelf

        Args:
            button (ScriptButton): the button to add
        """
        self._buttons.append(button)
        button.SetTabParent(self)
        self._tabWidget.addWidget(button.Button())
        button.dataChanged.connect(self._handleScriptButtonDataChanged)
        button.buttonToBeEdited.connect(self._handleButtonToBeEdited)
        self.IsLocked()

    def _addNewButtonFromDrop(self, xmlCode):
        """
        Internal Method

        Add a button from xml code, used for the drop action on the shelf

        Expected format for the XML string data to add buttons to the shelf.

        <button>
            <name>Referencing System</name>
            <icon>$TechArt\Shelftastic\referenceEditor.png</icon>
            <code>
from RS.Tools.UI import ReferenceEditor
ReferenceEditor.Run()
            </code>
            <codeFile>C:\Derp\Derp.py</codeFile>
            <runTypeIdx>0</runTypeIdx>
        </button>

        """
        try:
            xmldoc = minidom.parseString(xmlCode)

            buttonData = xmldoc.childNodes[0]
            expButton = {str(node.nodeName): node.childNodes[0].nodeValue for node in
                                        buttonData.childNodes if not isinstance(node, minidom.Text)}
        except:
            raise ValueError("XML Data string is ill-formated. {0}".format(xmlCode))
        scrButton = ScriptButton(
                                 None,
                                 expButton.get("name", "NoName"),
                                 expButton.get("icon"),
                                 expButton.get("code", ""),
                                 expButton.get("codeFile", ""),
                                 int(expButton.get("runTypeIdx", "0"))
                                 )
        self.AddButton(scrButton)
        self.dataChanged.emit()

    def AddNewButton(self, name, code, iconPath):
        """
        Add a new button to the shelf

        Args:
            name (str): The name of the button
            code (str): The code to run on the buttons execution
            iconPath (str): The filePath to the icon

        Return:
            ScriptButton: The newly created button
        """
        newButton = ScriptButton(self, name, code, iconPath)
        self.AddButton(newButton)
        return newButton

    def _handleScriptButtonDataChanged(self):
        """
        Internal Method

        Method to pick up dataChanged events in any children buttons and re-emit them
        """
        self.dataChanged.emit()

    def _handleButtonToBeEdited(self, button):
        """
        Internal Method

        Method to pick up buttonToBeEdited events in any children buttons and re-emit them
        """
        self.buttonToBeEdited.emit(button)


class ScriptButton(QtCore.QObject):
    """
    Object to represent the script button which makes up the Tab, which in turn makes up the shelf

    Signals:
        dataChanged (): The button has been edited
        buttonToBeEdited (ScriptButton): The button is wanting to be edited
    """
    dataChanged = QtCore.Signal()
    buttonToBeEdited = QtCore.Signal(object)

    NoIconIcon = None

    def __init__(self, tabParent, name=None, icon=None, code=None,
                 codeFilePath=None, runTypeIndex=None):
        """
        Constructor

        Args:
            tabParent (ScriptTab): The parent tab the button will sit in

        Kwargs:
            name (str): The name of the button
            code (str): The code to run on the buttons execution
            iconPath (str): The filePath to the icon
            codeFilePath (str): The code file path to run
            runTypeIndex (ShelftasticButtonRunTypeIndex): if the code or file is run
        """
        super(ScriptButton, self).__init__()
        self._toolAuditor = ShelftasticToolAuditor()

        if self.NoIconIcon is None:
            iconPath = os.path.join(Config.Script.Path.ToolImages, "Shelftastic", "noIcon.png")
            self.NoIconIcon = QtGui.QIcon(iconPath)

        self._name = name
        self._iconPath = icon or ""
        self._code = code or ""
        self._codeFilePath = codeFilePath or ""
        self._button = self._createNewButton()
        self._tabParent = tabParent

        preset = None
        if self._tabParent is None:
            preset = False
        self.SetSmallButton(preset)
        self.SetButtonLabels(preset)

        self._runType = runTypeIndex or ShelftasticButtonRunTypeIndex.RunCode
        if runTypeIndex is None and codeFilePath is not None:
            self._runType = ShelftasticButtonRunTypeIndex.RunFile

    def SetSmallButton(self, value=None):
        """
        Set the use of smaller button icons on or off

        Kargs:
            Value (bool): The True/False value to use Small or Big buttons
        """
        if value is None:
            value = self._tabParent.SmallButton()

        size = 30
        if value:
            size = 20

        self._button.setIconSize(QtCore.QSize(size, size))

    def SetButtonLabels(self, value=None):
        """
        Set the button labels on or off

        Kargs:
            Value (bool): The True/False value to show or hide the icon
        """
        if value is None:
            value = self._tabParent.ButtonLabels()

        setting = QtCore.Qt.ToolButtonIconOnly
        if value:
            setting = QtCore.Qt.ToolButtonTextUnderIcon
        self._button.setToolButtonStyle(setting)

    def Name(self):
        """
        Get the name of the button

        Return:
            str: The name of the button command
        """
        return self._name

    def SetName(self, newName):
        """
        Set the name of the button. Will also set the tooltip to the name

        Args:
            newName (str): The new button name
        """
        self._name = newName
        self._button.setText(self._name)
        self._button.setToolTip(self._name)
        self.dataChanged.emit()

    def Code(self):
        """
        Get the code to run when the button is pressed

        Return:
            str: the button code which will be executed on press
        """
        return self._code

    def SetCode(self, newCode):
        """
        Set the button code

        Args:
            newCode (str): The new code to be run when the button is pressed
        """
        self._code = newCode
        self.dataChanged.emit()

    def CodeFilepath(self):
        """
        Get the code to run when the button is pressed

        Return:
            str: the button code which will be executed on press
        """
        return self._codeFilePath

    def SetCodeFilePath(self, newCode):
        """
        Set the button code file path

        Args:
            newCode (str): The new file path to be run when the button is pressed
        """
        self._codeFilePath = newCode
        self.dataChanged.emit()

    def RunType(self):
        """
        Get if the one push the code or the file will get executed

        Return:
            ShelftasticButtonRunTypeIndex: the run type index
        """
        return self._runType

    def SetRunType(self, newCode):
        """
        Set if the one push the code or the file will get executed

        Args:
            newType (ShelftasticButtonRunTypeIndex): The new run index
        """
        self._runType = newCode
        self.dataChanged.emit()

    def IconPath(self):
        """
        Get the button's Icon Path

        Return:
            str: The path to the button icon
        """
        return self._iconPath

    def SetIconPath(self, newPath):
        """
        Set the button's icon path

        Args:
            newPath (str): the new icon path
        """
        self._iconPath = newPath
        self._button.setIcon(self._GetQIcon())
        self.dataChanged.emit()

    def GetIcon(self):
        """
        Get the icon from the button

        Return:
            QIcon: The icon with the iconpath, or the empty icon if no path is set
        """
        return self._GetQIcon()

    def _GetQIcon(self):
        """
        Internal Method

        Get the icon from the button

        Return:
            QIcon: The icon with the iconpath, or the empty icon if no path is set
        """
        resolvedPath = ShelfPathResolver.ResolvePath(self._iconPath)
        if not os.path.isfile(resolvedPath):
            return self.NoIconIcon

        return QtGui.QIcon(resolvedPath)

    def SetTabParent(self, newParent):
        """
        Set the parent tab of the button

        Args:
            newParent (ScriptTab): The new script tab parent of the button
        """
        self._tabParent = newParent
        self._button.setParent(newParent.GetTab())

    def GetTabParent(self):
        """
        Get the parent tab of the button

        Return:
            ScriptTab: The script tab parent of the button
        """
        return self._tabParent

    def Button(self):
        """
        Get the underlying button for the scriptButton

        Return:
            QToolButton: The underlying button
        """
        return self._button

    def IsNewTool(self):
        """
        Get the state of the tool if its a new tool from the ToolAuditor

        Return:
            bool: if the tool auditor has a record of the tool being run before
        """
        return not self._toolAuditor.HasCommandBeenRun(self._name)

    def _createNewButton(self):
        """
        Internal Method

        Create a new underlying button object and connect it up

        Return:
            QToolButton: A new button to be used.
        """
        newBut = QtGui.QToolButton()
        newBut.pressed.connect(functools.partial(self._handleButtonPress))

        borderStyle = ""
        # Shade the border green if its a new tool
        if self.IsNewTool():
            borderStyle = """
            QToolButton::menu-indicator { image: none; }
            QToolButton {
                border-radius: 5px;
                border: 2px solid rgb(100, 255, 100);
            }
            """
        else:
            borderStyle = "QToolButton::menu-indicator { image: none; }"
        newBut.setStyleSheet(borderStyle)

        newBut.setIcon(self._GetQIcon())
        newBut.setAutoRaise(True)
        newBut.setToolTip(self._name)
        newBut.setText(self._name)

        # Setup right click context menu
        newBut.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        editButtonAction = QtGui.QAction(newBut)
        editButtonAction.setText("Edit Button")
        editButtonAction.triggered.connect(functools.partial(self._handleEditButtonPress))
        newBut.addAction(editButtonAction)

        deleteButtonAction = QtGui.QAction(newBut)
        deleteButtonAction.setText("Delete Button")
        deleteButtonAction.triggered.connect(functools.partial(self._handleDeleteButtonPress))
        newBut.addAction(deleteButtonAction)

        return newBut

    def _handleEditButtonPress(self):
        """
        Internal Method

        Handler for the edit button press
        """
        self.buttonToBeEdited.emit(self)

    def _handleDeleteButtonPress(self):
        """
        Internal Method

        Handler for the delete button press
        """
        self._tabParent.RemoveButton(self)

    def _handleButtonPress(self):
        """
        Internal Method

        Handler for the execute button press
        """
        self._toolAuditor.CommandRun(self._name)
        if self._runType == ShelftasticButtonRunTypeIndex.RunCode:
            self._runCode()
        elif self._runType == ShelftasticButtonRunTypeIndex.RunFile:
            self._runFile()
        MongoDatabase.LogToolInfo(self._name, Database.Context.Opened, self._name, modulePath=self._codeFilePath)

    def _runFile(self):
        """
        Internal Method

        Run and execute the given code file.

        ANY ERRORS FROM THIS FUNCTION ARE LIKELY DUE TO ERRORS OR PROBLEMS IN THE EXCUTED CODE FILE
        AND NOT FROM THIS FUNCTION.
        """
        if not os.path.isfile(self._codeFilePath):
            QtGui.QMessageBox.critical(self.Button(), self._name, "{0} file does not exist".format(self._codeFilePath))
        try:
            # Python "Querk" exec doesnt work with __buildtin__ so globals and import * have issues. workaround
            glob = copy.copy(globals())
            execfile(self._codeFilePath, glob, glob)
        except Exception, ex:
            localTraceback = traceback.format_exc()
            msgBox = QtGui.QMessageBox(
                                       QtGui.QMessageBox.Critical,
                                       self._name,
                                       "An Error occured running the file",
                                       QtGui.QMessageBox.Ok
                                       )
            msgBox.setWindowFlags(msgBox.windowFlags() & ~QtCore.Qt.WindowCloseButtonHint)
            msgBox.setDetailedText(localTraceback )
            msgBox.exec_()
            raise ThisIsNotAShelftasticError(localTraceback, filePath=self._codeFilePath)

    def _runCode(self):
        """
        Internal Method

        Run and execute the given code text.

        ANY ERRORS FROM THIS FUNCTION ARE LIKELY DUE TO ERRORS OR PROBLEMS IN THE EXCUTED CODE FILE
        AND NOT FROM THIS FUNCTION.
        """
        try:
            exec(str(self._code))
        except Exception, ex:
            localTraceback = traceback.format_exc()
            msgBox = QtGui.QMessageBox(
                                       QtGui.QMessageBox.Critical,
                                       self._name,
                                       "An Error occured running the script",
                                       QtGui.QMessageBox.Ok
                                       )
            msgBox.setWindowFlags(msgBox.windowFlags() & ~QtCore.Qt.WindowCloseButtonHint)
            msgBox.setDetailedText(localTraceback)
            msgBox.exec_()
            raise ThisIsNotAShelftasticError(localTraceback, code=str(self._code))
