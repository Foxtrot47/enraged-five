import unittest

from RS.Tools import UI
from RS.Tools.Shelftastic import Shelftastic


class ShelftasticBase(unittest.TestCase):
    """
    Base class for the Shelftastic tests
    """
    def __init__(self, *args, **kwargs):
        super(ShelftasticBase, self).__init__(*args, **kwargs)
        self.tabs = []

    def setUp(self):
        """Setup shelf with test tab"""
        self.shelftastic = self.assertShelftastic()
        self.tab = self.assertCreateTab()
        self.button = self.assertCreateButton()

    def tearDown(self):
        """Clears the scene after running the test"""
        for tab in self.tabs:
            self.assertDeleteTab(tab)

    def assertShelftastic(self):
        """Runs shelftastic

        Returns:
            QtMainWindow : Shelftastic window
        """
        shelftastic = Shelftastic.Shelftastic()
        UI.Run(shelftastic)

        self.assertIsNotNone(shelftastic)
        return shelftastic

    def assertCreateTab(self, tabName="Unittest_Tab"):
        """Create a tab

        Args:
            tabName (str, optional): Name of the tab

        Returns:
            ShelfComponents.ScriptTab: The newly created tab or already created tab
        """
        for tab in self.shelftastic.GetTabs():
            if tab.Name() == tabName:
                if tab not in self.tabs:
                    self.tabs.append(tab)

                return tab

        tab = self.shelftastic.AddTab(tabName)
        self.assertIsNotNone(tab)
        self.tabs.append(tab)

        return tab

    def assertDeleteTab(self, tab=None):
        """Deletes tab

        Args:
            tab (ShelfComponents.ScriptTab, optional): The tab to remove from the shelf
        """
        tab = self.tab if not tab else tab
        self.shelftastic.DeleteTab(tab)

        self.assertEqual(tab.GetTab() not in self.shelftastic.GetTabs(), True)
        self.tabs.remove(tab)

    def assertCreateButton(self, tabParent=None, buttonName="unittestButton"):
        """Creates a button under tab

        Args:
            tabParent (ShelfComponents.ScriptTab, optional): The tab to create the button under
            buttonName (str, optional): Name of button
        """
        tabParent = self.tab if not tabParent else tabParent
        button = tabParent.AddNewButton(buttonName, "print(\"Unittest!\")", None)
        self.assertIsNotNone(button)

        return button

    def assertDeleteButton(self, button=None):
        """Deletes a button under tab

        Args:
            button (ShelfComponents.ScriptButton, optional): The button to delete
        """
        button = self.button if not button else button
        self.tab.RemoveButton(button)

        self.assertEquals(button not in self.tab.GetButtons(), True)

    def assertMoveButton(self, button=None):
        """Moves a button

        Args:
            button (ShelfComponents.ScriptButton, optional): The button to move
        """
        button = self.button if not button else button
        currentIndex = self.tab._buttons.index(button)

        self.tab.MoveButton(button, True)
        movedIndex = self.tab._buttons.index(button)

        self.assertNotEqual(movedIndex, currentIndex)

        self.tab.MoveButton(button, False)
        movedBackIndex = self.tab._buttons.index(button)
        self.assertEqual(currentIndex, movedBackIndex)

    def assertExecuteButton(self, button=None):
        """Deletes a button under tab

        Args:
            button (ShelfComponents.ScriptButton, optional): The button to move
        """
        button = self.button if not button else button
        button._handleButtonPress()

    def assertRenameTab(self, newName, tab=None):
        """Renames a tab

        Args:
            newName (str): The name to rename
            tab (ShelfComponents.ScriptTab, optional): The tab to rename
        """
        tab = self.tab if not tab else tab
        self.shelftastic.RenameTab(tab, newName)

        self.assertEqual(tab.Name(), newName)

    def assertLoadTab(self, filePath=None):
        """Loads shelf from file

        Args:
            filePath (str, optional): XML file to load
        """
        filePath = self.shelftastic.GetCustomMenuFile() if not filePath else filePath
        self.shelftastic.LoadTabsFromFile(filePath)
