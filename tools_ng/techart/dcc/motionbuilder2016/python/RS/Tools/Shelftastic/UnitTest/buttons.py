import unittest

from RS.Tools.Shelftastic.UnitTest import base


def Run():
    """Run unittests from this module"""
    unittest.TextTestRunner(verbosity=0).run(suite())


def suite():
    """A suite to hold all the tests from this module

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()

    for functionName in dir(Test_Button):
        if functionName.startswith("test_"):
            suite.addTest(Test_Button(functionName))

    return suite


class Test_Button(base.ShelftasticBase):
    """Tests buttons"""

    def test_Add(self):
        """Testing adding a button"""
        self.assertCreateButton(buttonName="testButton")

    def test_Edit(self):
        """Testing editing code of button"""
        newCommand = "print \"Edited!\""
        self.button.SetCode(newCommand)
        self.assertEquals(self.button.Code(), newCommand)

    def test_Move(self):
        """Testing moving a button"""
        button = self.assertCreateButton(buttonName="moveButton")
        self.assertMoveButton(button)

    def test_Execute(self):
        """Testing executing a button"""
        self.assertExecuteButton()

    def test_Delete(self):
        """Testing deleting a button"""
        button = self.assertCreateButton(buttonName="deleteMe")
        self.assertDeleteButton(button)
