import csv
import os

from PySide import QtGui, QtCore
import pyfbsdk as mobu


from RS import Globals, Perforce
from RS.Tools import UI

from RS.Tools.FaceMovieplanePreview.Widgets import faceAdjustmentWindow as faceAdjWnd
from RS.Tools.FaceMovieplanePreview import params

class DragDropView(QtGui.QTableView):
    def __init__(self, parent=None):
        super(DragDropView, self).__init__(parent)
        
    def dropEvent(self, event):
        """
        Custom Implementation of Drop Event in TableView
        Updates the attached model based on drag/drop action.
        """
        # Gather info on source and target from the drop action.
        selectedRowNumber = self.selectionModel().selectedIndexes()[0].row()
        targetrowNum = self.rowAt(event.pos().y())
        
        # Grab Source and Target Data
        lSourceItemIndex0 = self.model().index(selectedRowNumber, 0).data()
        lSourceItemIndex1 = self.model().index(selectedRowNumber, 1).data()
        lTargetItemIndex0 = self.model().index(targetrowNum, 0).data()
        lTargetItemIndex1 = self.model().index(targetrowNum, 1).data()
        
        # Perform a different action depending on which row was dragged to which
        if lSourceItemIndex0 == "" and lTargetItemIndex1 == "":
            lSourceItemData1 = self.model().item(selectedRowNumber, 1).data()
            
            # Set Dragged item properties to target item
            lModelItem = QtGui.QStandardItem(lSourceItemIndex1)
            lModelItem.setData(lSourceItemData1)
            self.model().setItem(targetrowNum, 1, lModelItem)
            
            # Remove Dragged item
            self.model().removeRow(selectedRowNumber)
            
        elif lSourceItemIndex1 == "" and lTargetItemIndex0 == "":
            lTargetItemData1 = self.model().item(targetrowNum, 1).data()
            
            # Set Target item properties to Dragged item
            lModelItem = QtGui.QStandardItem(lTargetItemIndex1)
            lModelItem.setData(lTargetItemData1)
            self.model().setItem(selectedRowNumber, 1, lModelItem)
            
            # Remove Target item
            self.model().removeRow(targetrowNum)
            
        elif (lSourceItemIndex0 and lTargetItemIndex0) or (lSourceItemIndex1 and lTargetItemIndex1) :
            lSourceItemData1 = self.model().item(selectedRowNumber, 1).data()
            lTargetItemData1 = self.model().item(targetrowNum, 1).data()
            
            # Swap item properties
            lModelItemSource = QtGui.QStandardItem(lTargetItemIndex1)
            lModelItemSource.setData(lTargetItemData1)
            self.model().setItem(selectedRowNumber, 1, lModelItemSource)
            
            lModelItemTarget = QtGui.QStandardItem(lSourceItemIndex1)
            lModelItemTarget.setData(lSourceItemData1)
            self.model().setItem(targetrowNum, 1, lModelItemTarget)
            

class ActorFacePreviewWindow(UI.QtBannerWindowBase):
    """
    Main window for Actor Face Preview Tool
    """
    MOBU_SYSTEM = mobu.FBSystem()
    VIDPARAMS = params.VideoParams()
    
    def __init__(self, parent=None):
        winWidth = 600
        winHeight = 500
        super(ActorFacePreviewWindow, self).__init__(parent=parent,
                                                           title="Actor Face Preview Tool",
                                                           size=[winWidth, winHeight],
                                                           dockable=False,
                                                           dialog=False,
                                                           store=True,
                                                           closeExistingWindow = True)
        self.setupUi()
    
    def _setupMenuBar(self):
        fileMenu = QtGui.QMenu("File")
        advMenu = QtGui.QMenu("Advanced")
        
        nukeAllData = QtGui.QAction("Nuke All Preview Data", self)
        nukeAllData.triggered.connect(self.NukeEverything)
        advMenu.addAction(nukeAllData)
        
        font = QtGui.QApplication.font()
        metrics = QtGui.QFontMetrics(font)
        fontHeight = metrics.height()
        self.menuBar.setMinimumHeight(fontHeight + 6)
        self.menuBar.setMaximumHeight(20)
        
        self.menuBar.addMenu(fileMenu)
        self.menuBar.addMenu(advMenu)
    
    def setupUi(self):
        """
        Setup for main UI.
        """
        # Style Presets
        btnHeight = 30
        btnHeightTall = 50
        stdMargin = 10
        self.setStyleSheet("QMenuBar {background: #ffdc19;"
                           "font: bold;"
                           "color: black;}"
                           )
        
        # Menu Bar
        self.menuBar = QtGui.QMenuBar()
        self._setupMenuBar()
        
        # Layouts
        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.setContentsMargins(stdMargin, stdMargin, stdMargin, stdMargin)
        self.toggleLayout = QtGui.QHBoxLayout()
        self.toggleLayout.setContentsMargins(0, 0, 0, stdMargin)
        self.resetLayout = QtGui.QHBoxLayout()
        self.resetLayout.setContentsMargins(0, 0, 0, 0)
        self.setupLayout = QtGui.QHBoxLayout()
        self.setupLayout.setContentsMargins(0, 0, 0, 0)
        self.autoAdjustLayout = QtGui.QHBoxLayout()
        self.autoAdjustLayout.setContentsMargins(0, 0, 0, 0)
        
        # Buttons
        self.toggleBtn = QtGui.QPushButton("Toggle Previews")
        self.toggleBtn.setFixedHeight(btnHeightTall)
        self.setupBtn = QtGui.QPushButton("Save and Create")
        self.setupBtn.setFixedHeight(btnHeightTall)
        self.resetSelBtn = QtGui.QPushButton("Reset Selected")
        self.resetSelBtn.setFixedHeight(btnHeight)
        self.resetAllBtn = QtGui.QPushButton("Reset All")
        self.resetAllBtn.setFixedHeight(btnHeight)
        self.adjustBtn = QtGui.QPushButton("Adjust Selected")
        self.adjustBtn.setFixedHeight(btnHeight)
        self.syncBtn = QtGui.QPushButton("Sync Videos")
        self.syncBtn.setFixedHeight(btnHeight)
        self.autoAdjustSelBtn = QtGui.QPushButton("Auto-Adjust Selected")
        self.autoAdjustSelBtn.setFixedHeight(btnHeightTall)
        self.autoAdjustAllBtn = QtGui.QPushButton("Auto-Adjust All")
        self.autoAdjustAllBtn.setFixedHeight(btnHeightTall)
        
        # TableView Model
        self.mappingTableModel = QtGui.QStandardItemModel()
        self.mappingTableModel.setHorizontalHeaderLabels(['Char Namespace', 'Video Path'])
        
        # TableView
        self.mappingTableView = DragDropView()
        self.mappingTableView.setModel(self.mappingTableModel)
        self.mappingTableView.setSelectionMode(QtGui.QListView.SingleSelection)
        self.mappingTableView.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.mappingTableView.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.mappingTableView.verticalHeader().setVisible(False)
        self.mappingTableView.setDragEnabled(True)
        self.mappingTableView.setAcceptDrops(True)
        
        self.fillTable()
        
        # Setup Layouts
        self.toggleLayout.addWidget(self.toggleBtn, 2)
        self.toggleLayout.addWidget(self.setupBtn, 1)
        self.resetLayout.addWidget(self.resetSelBtn)
        self.resetLayout.addWidget(self.resetAllBtn)
        self.setupLayout.addWidget(self.syncBtn)
        self.setupLayout.addWidget(self.adjustBtn)
        self.autoAdjustLayout.addWidget(self.autoAdjustSelBtn)
        self.autoAdjustLayout.addWidget(self.autoAdjustAllBtn)
        
        self.mainLayout.addLayout(self.toggleLayout)
        self.mainLayout.addWidget(self.mappingTableView)
        self.mainLayout.addLayout(self.resetLayout)
        self.mainLayout.addLayout(self.setupLayout)
        self.mainLayout.addLayout(self.autoAdjustLayout)
        
        self.Layout.addWidget(self.menuBar)
        self.Layout.addLayout(self.mainLayout, 0)
        
        # Signals
        self.mappingTableView.doubleClicked.connect(self.copyPath)
        self.mappingTableView.doubleClicked.connect(self.selectFaceGeo)
        
        self.toggleBtn.pressed.connect(self.toggleMoviePlanes)
        self.resetSelBtn.pressed.connect(self.resetSelectedItems)
        self.resetAllBtn.pressed.connect(self.resetAllItems)
        self.syncBtn.pressed.connect(self.syncFiles)
        self.setupBtn.pressed.connect(self.setupAllMovieplanes)
        self.adjustBtn.pressed.connect(self.adjustSelected)
        self.autoAdjustSelBtn.pressed.connect(self.autoAdjustSel)
        self.autoAdjustAllBtn.pressed.connect(self.autoAdjustAll)
        
    def importTextureTransforms(self, nullRoot, videoPath):
        videoFolder = os.path.dirname(videoPath)
        syncPath = "{0}/*{1}".format(videoFolder, self.VIDPARAMS.DATA_SEARCH)
        Perforce.Sync(syncPath)
        csvFilePath = videoPath.replace(self.VIDPARAMS.VIDEO_SEARCH, self.VIDPARAMS.DATA_SEARCH)
        if os.path.isfile(csvFilePath):
            with open(csvFilePath) as csvFile:
                csvReader = csv.reader(csvFile, delimiter=',')
                csvHeading = next(csvReader)
                for row in csvReader:
                    if len(row) > 2:
                        frameNum = int(row[0])
                        moveEyesX = float(row[1])
                        moveEyesY = float(row[2])
                        upperScaleValue = float(row[3])
                        moveNoseX = float(row[4])
                        moveNoseY = float(row[5])
                        lowerScaleValue = float(row[6])
                        
                        topTxt, botTxt = faceAdjWnd.FaceAdjustmentWindow.getTexture(nullRoot)
                        
                        if topTxt:
                            topTxt.Translation.SetAnimated(True)
                            topTxt.Translation.GetAnimationNode().KeyAdd(mobu.FBTime(0,0,0,frameNum),
                                                                         [moveEyesX, moveEyesY, 0])
                            topTxt.Scaling.SetAnimated(True)
                            topTxt.Scaling.GetAnimationNode().KeyAdd(mobu.FBTime(0,0,0,frameNum),
                                                                     [upperScaleValue, upperScaleValue, upperScaleValue])
                        
                        if botTxt:
                            botTxt.Translation.SetAnimated(True)
                            botTxt.Translation.GetAnimationNode().KeyAdd(mobu.FBTime(0,0,0,frameNum),
                                                                         [moveNoseX, moveNoseY, 0])
                            botTxt.Scaling.SetAnimated(True)
                            botTxt.Scaling.GetAnimationNode().KeyAdd(mobu.FBTime(0,0,0,frameNum),
                                                                     [lowerScaleValue, lowerScaleValue, lowerScaleValue])
    
    def importTextureTransforms_Zoom(self, nullRoot, videoPath):
        videoFolder = os.path.dirname(videoPath)
        syncPath = "{0}/*{1}".format(videoFolder, self.VIDPARAMS.DATA_SEARCH)
        Perforce.Sync(syncPath)
        for fileExt in self.VIDPARAMS.WEB_SEARCH:
            csvFilePath = videoPath.replace(fileExt, self.VIDPARAMS.DATA_SEARCH)
        for geo in nullRoot.Children:
            for vidMat in geo.Materials:
                vidTxt = vidMat.GetTexture()
                if os.path.isfile(csvFilePath):
                    with open(csvFilePath) as csvFile:
                        csvReader = csv.reader(csvFile, delimiter=',')
                        csvHeading = next(csvReader)
                        for row in csvReader:
                            if len(row) > 2:
                                frameNum = int(row[0])
                                moveFaceX = float(row[1])
                                moveFaceY = float(row[2])
                                scaleVal = float(row[3])
                                
                                vidTxt.Translation.SetAnimated(True)
                                vidTxt.Translation.GetAnimationNode().KeyAdd(mobu.FBTime(0,0,0,frameNum),
                                                                        [moveFaceX, moveFaceY, 0])
                                
                                vidTxt.Scaling.SetAnimated(True)
                                vidTxt.Scaling.GetAnimationNode().KeyAdd(mobu.FBTime(0,0,0,frameNum),
                                                                        [scaleVal, scaleVal, scaleVal])
    
    def resetTextureTransforms(self, nullRoot):
        for geo in nullRoot.Children:
            geo.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingAll
            for vidMat in geo.Materials:
                makeItGlow = vidMat.PropertyList.Find("EmissiveColor")
                makeItGlow.Data = mobu.FBColor(1.0, 1.0, 1.0)
                vidTxt = vidMat.GetTexture()
                if vidTxt:
                    vidTxt.Translation.Data = mobu.FBVector3d(0, 0, 0)
                    vidTxt.Scaling.Data = mobu.FBVector3d(1,1,1)
                    vidTxt.PropertyList.Find("WrapModeU").Data = 1 #set to non-repeat
                    vidTxt.PropertyList.Find("WrapModeV").Data = 1 #set to non-repeat
                    vidTxt.Translation.SetAnimated(False)
                    vidTxt.Scaling.SetAnimated(False)
        
    def autoAdjustAll(self):
        # If there is a pair, move the column 1 data to a new row.
        for rowNum in range(self.mappingTableModel.rowCount()):
            # Store all data from the row
            charNamespace = self.mappingTableModel.index(rowNum,0).data()
            videoPath = self.mappingTableModel.item(rowNum,1).data()
            if charNamespace and videoPath:
                videoRoot = mobu.FBFindModelByLabelName("{0}:{1}_root".format(charNamespace, self.VIDPARAMS.VID_NAME))
                zoomRoot = mobu.FBFindModelByLabelName("{0}:{1}_root".format(charNamespace, self.VIDPARAMS.WEB_NAME))
                if videoPath and videoRoot:
                    self.resetTextureTransforms(videoRoot)
                    self.importTextureTransforms(videoRoot, videoPath)
                elif videoPath and zoomRoot:
                    self.resetTextureTransforms(zoomRoot)
                    self.importTextureTransforms_Zoom(zoomRoot, videoPath)
        self.MOBU_SYSTEM.Scene.Evaluate()
        QtGui.QMessageBox.information(self, "Auto-Fit Texture", "Completed.")
        
    def autoAdjustSel(self):
        getSelectedRow = self.mappingTableView.selectionModel().selectedIndexes()
        if len(getSelectedRow) == 2:
            rowNum = getSelectedRow[0].row()
            charNamespace = self.mappingTableModel.index(rowNum,0).data()
            videoPath = self.mappingTableModel.item(rowNum,1).data()
            if charNamespace and videoPath:
                videoRoot = mobu.FBFindModelByLabelName("{0}:{1}_root".format(charNamespace, self.VIDPARAMS.VID_NAME))
                zoomRoot = mobu.FBFindModelByLabelName("{0}:{1}_root".format(charNamespace, self.VIDPARAMS.WEB_NAME))
                if videoPath and videoRoot:
                    self.resetTextureTransforms(videoRoot)
                    self.importTextureTransforms(videoRoot, videoPath)
                elif videoPath and zoomRoot:
                    self.resetTextureTransforms(zoomRoot)
                    self.importTextureTransforms_Zoom(zoomRoot, videoPath)
        self.MOBU_SYSTEM.Scene.Evaluate()
        QtGui.QMessageBox.information(self, "Auto-Fit Texture", "Completed.")
        
    def adjustSelected(self):
        getSelectedRow = self.mappingTableView.selectionModel().selectedIndexes()
        if len(getSelectedRow) == 2:
            rowNum = getSelectedRow[0].row()
            charNamespace = self.mappingTableModel.index(rowNum,0).data()
            videoRoot = mobu.FBFindModelByLabelName("{0}:{1}_root".format(charNamespace, self.VIDPARAMS.VID_NAME))
            videoPath = self.mappingTableModel.item(rowNum,1).data()
            if videoPath and videoRoot:
                self.resetTextureTransforms(videoRoot)
                self.MOBU_SYSTEM.Scene.Evaluate()
                adjustmentWindow = faceAdjWnd.FaceAdjustmentWindow(videoRoot)
                adjustmentWindow.show()
    
    def NukeEverything(self):
        message = "This will delete all components related to the Actor Face Preview Tool, including texture position info.\n\nAre you sure?"
        choice = QtGui.QMessageBox.question(self, 'Confirm Nuke', message,
                                                    QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        
        if choice == QtGui.QMessageBox.Yes:
            deleteOrder = [
                "FBVideoClip",
                "FBTexture",
                "FBMaterial",
                "FBModel",
                "FBModelNull",
            ]
            
            allItems = mobu.FBComponentList()
            mobu.FBFindObjectsByName("ActorFace*", allItems, False, False)
            
            for className in deleteOrder:
                for mobuComponent in allItems:
                    if mobuComponent.ClassName() == className:
                        mobuComponent.FBDelete()
                        
            self.fillTable()
        else:
            pass
    
    def exportTextureOffset(self, rowNum, clNumber):
        namespace = self.mappingTableModel.index(rowNum, 0).data()
        videoPath = self.mappingTableModel.item(rowNum, 1).data()
        csvPath = videoPath.replace(self.VIDPARAMS.VIDEO_SEARCH, self.VIDPARAMS.DATA_SEARCH)
        if namespace and videoPath and csvPath.endswith(self.VIDPARAMS.DATA_SEARCH):
            textureList = mobu.FBComponentList()
            mobu.FBFindObjectsByName("{0}:{1}_*_Txt".format(namespace, self.VIDPARAMS.VID_NAME), textureList, True, False)
            if textureList:
                propList = []
                headVideoGeoRoot = mobu.FBFindModelByLabelName("{0}:{1}_root".format(namespace, self.VIDPARAMS.VID_NAME))
                if headVideoGeoRoot:
                    for prop in headVideoGeoRoot.PropertyList:
                        if prop.IsUserProperty():
                            propData = "{0}={1}".format(prop.Name, prop.Data)
                            propList.append(propData)
                Perforce.Edit(csvPath, clNumber)
                with open(csvPath, "wb") as csvFile:
                    Perforce.Add(csvPath, clNumber)
                    
                    csvWriter = csv.writer(csvFile, delimiter=',')
                    csvWriter.writerow(propList)
                    csvWriter.writerow(["Name", "TX", "TY", "Scale"])
                    
                    for txt in textureList:
                        csvWriter.writerow([txt.Name, txt.Translation.Data[0], txt.Translation.Data[1], txt.Scaling.Data[0]])
    
    def exportAllTextureOffsets(self):
        # 
        newCL = Perforce.CreateChangelist(self.VIDPARAMS.P4_COMMENTS)
        if newCL:
            for rowNumber in range(self.mappingTableModel.rowCount()):
                self.exportTextureOffset(rowNumber, newCL.Number)
            self.fillTable()
            
            fileList = Perforce.GetFilesInChangelist(newCL.Number)
            if fileList:
                Perforce.Submit(newCL.Number)
            else:
                Perforce.DeleteChangelist(newCL.Number, True)
            QtGui.QMessageBox.information(self, "Export Texture Data", "Completed.")
    
    def syncFiles(self):
        """
        Syncs Face Reference videos relative to the current fbx
        """
        fbxFilepath = Globals.Application.FBXFileName
        fbxFolder = os.path.dirname(fbxFilepath)
        faceRootFolder = os.path.join(fbxFolder, "Face")
        syncPaths = []
        syncPaths.append( "{0}/.../*{1}".format(faceRootFolder, self.VIDPARAMS.VIDEO_SEARCH) )
        for fileExt in self.VIDPARAMS.WEB_SEARCH:
            syncPaths.append( "{0}/.../*{1}".format(faceRootFolder, fileExt) )
        Perforce.Sync(syncPaths)
        self.fillTable()
        
        QtGui.QMessageBox.information(self, "Sync", "File Sync Completed.")
    
    def copyPath(self):
        """
        Copies video (depot path) stored in model item to clipboard
        """
        rowNum = self.mappingTableView.selectionModel().selectedIndexes()[0].row()
        videoPath = self.mappingTableModel.item(rowNum, 1).data()
        #Convert local to depot path
        videoPath = videoPath.lower().replace("x:","/")
        QtGui.QApplication.instance().clipboard().setText(videoPath)
    
    def selectFaceGeo(self):
        """
        Exclusively selects the face geo for selected namespace
        """
        # Get namespace from selected model item
        rowNum = self.mappingTableView.selectionModel().selectedIndexes()[0].row()
        namespace = self.mappingTableModel.index(rowNum, 0).data()
        videoPath = self.mappingTableModel.item(rowNum, 1).data()
        
        # Deselect everything and select the face proxy geo root or movieplane
        if namespace:
            foundFreehead = False
            for fileExt in self.VIDPARAMS.WEB_SEARCH:
                if fileExt in videoPath.lower():
                    foundFreehead = True
            if foundFreehead:
                headPlane = mobu.FBFindModelByLabelName("{0}:{1}".format(namespace, self.VIDPARAMS.WEB_NAME))
                if headPlane:
                    for model in Globals.Models.Selected:
                        model.Selected = False
                    headPlane.Selected = True
            else:
                headVideoGeoRoot = mobu.FBFindModelByLabelName("{0}:{1}_root".format(namespace, self.VIDPARAMS.VID_NAME))
                if headVideoGeoRoot:
                    for model in Globals.Models.Selected:
                        model.Selected = False
                    
                    headVideoGeoRoot.Selected = True
    
    def toggleGeoBoolNamespace(self, toggleBool, namespace):
        """
        Toggle head geo to bool, if within the matching namespace.
        """
        if "mockup" not in mobu.FBApplication().FBXFileName.lower():
            headGeoList = mobu.FBComponentList()
            for geoSearch in self.VIDPARAMS.GEO_SEARCH:
                mobu.FBFindObjectsByName(geoSearch, headGeoList, False, True)
            for headGeo in headGeoList:
                if (":" in headGeo.LongName) and (headGeo.LongName.startswith(namespace)):
                    headGeo.Show = toggleBool
    
    def toggleMoviePlanesBool(self, toggleBool):
        """
        Toggle all movieplanes to the bool value. 
        Toggle head geo of matching namespaces to the opposite bool.
        """
        if toggleBool:
            # Toggle valid movieplanes on.
            validNamespaces = []
            headVideoGeoList = mobu.FBComponentList()
            mobu.FBFindObjectsByName("{0}_root".format(self.VIDPARAMS.VID_NAME), headVideoGeoList, False, True)
            mobu.FBFindObjectsByName("{0}_root".format(self.VIDPARAMS.WEB_NAME), headVideoGeoList, False, True)
            for headVideoRoot in headVideoGeoList:
                videoPathProp = headVideoRoot.PropertyList.Find(self.VIDPARAMS.VID_PATH_PROP)
                if videoPathProp.Data:
                    validNamespace = headVideoRoot.LongName.split(":")[0]
                    validNamespaces.append(validNamespace)
                    for child in headVideoRoot.Children:
                        child.Show = True
            
            # Toggle all geo on, then toggle valid geo namespaces to False.
            if "mockup" not in mobu.FBApplication().FBXFileName.lower():
                headGeoList = mobu.FBComponentList()
                for geoSearch in self.VIDPARAMS.GEO_SEARCH:
                    mobu.FBFindObjectsByName(geoSearch, headGeoList, False, True)
                for headGeo in headGeoList:
                    geoNamespace = headGeo.LongName.split(":")[0]
                    if geoNamespace in validNamespaces:
                        headGeo.Show = False
                    else:
                        headGeo.Show = True
            
        else:
            # Toggle all movieplanes to Hidden.
            headVideoGeoList = mobu.FBComponentList()
            mobu.FBFindObjectsByName("{0}*".format(self.VIDPARAMS.VID_NAME), headVideoGeoList, False, True)
            mobu.FBFindObjectsByName("{0}*".format(self.VIDPARAMS.WEB_NAME), headVideoGeoList, False, True)
            for headVideoGeo in headVideoGeoList:
                headVideoGeo.Show = False
            
            # Toggle all geo on.
            headGeoList = mobu.FBComponentList()
            for geoSearch in self.VIDPARAMS.GEO_SEARCH:
                mobu.FBFindObjectsByName(geoSearch, headGeoList, False, True)
            for headGeo in headGeoList:
                headGeo.Show = True
                
    def toggleMoviePlanes(self):
        """
        Get current visibility and toggle all movieplanes.
        """
        # Get Visibility of all movieplane
        headVideoGeoList = mobu.FBComponentList()
        mobu.FBFindObjectsByName("{0}*".format(self.VIDPARAMS.VID_NAME), headVideoGeoList, False, True)
        mobu.FBFindObjectsByName("{0}*".format(self.VIDPARAMS.WEB_NAME), headVideoGeoList, False, True)
        currentVisibility = False
        for headVideoGeo in headVideoGeoList:
            if (headVideoGeo.Show and (headVideoGeo.ClassName() == "FBModel")) or (
                headVideoGeo.Show and (headVideoGeo.ClassName() == "FBModelPlane")):
                
                currentVisibility = True
        
        # Toggle all movieplanes to the opposite
        self.toggleMoviePlanesBool(not currentVisibility)
            
    def resetItem(self, rowNum):
        """
        If the row contains a namespace and a video, separate and move the video info to a new row.
        """
        # Store all data from the row
        col0Value = self.mappingTableModel.index(rowNum,0).data()
        col1Value = self.mappingTableModel.index(rowNum,1).data()
        col1Data = self.mappingTableModel.item(rowNum,1).data()
        
        # If there is a pair, move the column 1 data to a new row.
        if col0Value and col1Value:
            newModelItem = QtGui.QStandardItem(col1Value)
            newModelItem.setData(col1Data)
            newRowNumber = self.mappingTableModel.rowCount()
            self.mappingTableModel.setItem(newRowNumber, 0, QtGui.QStandardItem(""))
            self.mappingTableModel.setItem(newRowNumber, 1, newModelItem)
            self.mappingTableModel.setItem(rowNum, 1, QtGui.QStandardItem(""))
            
            # Destroy the matching texture, videoclip, and videoPath data
            cl = mobu.FBComponentList()
            mobu.FBFindObjectsByName("{0}*".format(self.VIDPARAMS.VID_NAME), cl, False, False)
            mobu.FBFindObjectsByName("{0}*".format(self.VIDPARAMS.WEB_NAME), cl, False, False)
            for item in cl:
                if item.LongName.startswith(col0Value):
                    if (item.ClassName() == "FBTexture") or (item.ClassName() == "FBVideoClip"):
                        item.FBDelete()
                    elif item.ClassName() == "FBModelPlane":
                        item.Show = False
                    elif item.ClassName() == "FBModel":
                        item.Show = False
                    elif item.ClassName() == "FBModelNull":
                        videoPathProperty = item.PropertyList.Find(self.VIDPARAMS.VID_PATH_PROP)
                        if videoPathProperty:
                            videoPathProperty.Data = ""
                        
                        
            # Toggle matching heads back on
            self.toggleGeoBoolNamespace(True, col0Value)
        
    def resetSelectedItems(self):
        """
        Get selected row.
        If the row contains a namespace and a video, separate and move the video info to a new row.
        """
        # Get selected row
        selectedRowGroup = self.mappingTableView.selectionModel().selectedIndexes()
        if selectedRowGroup:
            selectedRowNumber = selectedRowGroup[0].row()
            
            # Reset that row
            self.resetItem(selectedRowNumber)
            
            # re-filling the table will re-sort the items
            self.fillTable()
    
    def resetAllItems(self):
        """
        Unpair all namespace/videopath pairs in all rows of the model
        """
        # Reset All and refill table to sort list
        for rowNumber in range(self.mappingTableModel.rowCount()):
            self.resetItem(rowNumber)
        self.fillTable()
        
    @staticmethod
    def getFileList(rootFolder, fileExt):
        """
        Generic function to walk a folder for a list of files with a specific extension
        
        Arguments:
            rootFolder (str):  Path to check
            fileExt (str):  File extension to look for
        
        Returns:
            list of str:  Final list of files that match the file extension
        """
        fileList = []
        for root, directories, fileNames in os.walk(rootFolder):
            for fileName in fileNames:
                filePath = os.path.join(root, fileName)
                if filePath.lower().endswith(fileExt.lower()):
                    fileList.append(filePath.replace("\\","/"))
        return fileList
    
    @staticmethod
    def findAllMoviePlanes(searchString, videoPathPropertyName):
        """
        Find all movieplanes in the scene.
        
        Returns:
            tuple[dict, list]:  the namespace, video filepath and object in a dict and the video filepaths in a list
        """
        movieplaneDict = {}
        existingPaths = []
        
        foundMovieplanes = mobu.FBComponentList()
        mobu.FBFindObjectsByName("{0}*".format(searchString), foundMovieplanes, False, True)
        for movieplane in foundMovieplanes:
            videoPath = ""
            videoPathProperty = movieplane.PropertyList.Find(videoPathPropertyName)
            if videoPathProperty:
                videoPath = videoPathProperty.Data
                if videoPath not in existingPaths:
                    existingPaths.append(videoPath)
                    
                namespace = ""
                if ":" in movieplane.LongName:
                    namespace = movieplane.LongName.split(":")[0]
                    movieplaneDict[namespace] = [videoPath, movieplane]
                    
        return movieplaneDict, existingPaths
    
    def getDisplayNameFromPath(self, videoPath):
        """
        Converts videopath to Display Name for TableView.
        Has 2 display versions, 1 for ref videos and 1 for non-ref videos.
        
        Example:
            .../Face/Jason/INT_LUCIA_MCS1_PICKUP_P1_T14.jcrot.top.mov
            converts to:
            Jason    [ INT_LUCIA_MCS1_PICKUP_P1_T14.jcrot.top.mov ] *BAD CODEC*
            
            .../Face/Jason/RS/REF/INT_LUCIA_MCS1_PICKUP_P1_T14.jcrot.ref.mov
            converts to:
            Jason    [ INT_LUCIA_MCS1_PICKUP_P1_T14.jcrot.ref.mov ]
        """
        fileName = os.path.basename(videoPath)
        
        foundFreehead = False
        for fileExt in self.VIDPARAMS.WEB_SEARCH:
            if fileExt in fileName.lower():
                foundFreehead = True
                
        if self.VIDPARAMS.VIDEO_SEARCH in fileName.lower():
            dirName = os.path.dirname(videoPath).rsplit("/",3)[1]
            displayName = "{0}    [ {1} ]".format(dirName, fileName)
            return displayName
        
        elif foundFreehead:
            dirName = os.path.dirname(videoPath).rsplit("/",1)[1]
            displayName = "{0} **ZOOM/GOPRO VID**    [ {1} ]".format(dirName, fileName)
            return displayName
        
        else:
            dirName = os.path.dirname(videoPath).rsplit("/",1)[1]
            displayName = "{0}    [ {1} ] **BAD CODEC**".format(dirName, fileName)
            return displayName
    
    @staticmethod
    def findAllNamespacesFromComponent(searchString):
        """
        Finds all matching components from search string and returns list of namespaces
        Ignores _gs skeleton components
        """
        foundComponents = mobu.FBComponentList()
        mobu.FBFindObjectsByName(searchString, foundComponents, False, True)
        
        foundNamespaces = []
        for component in foundComponents:
            if (":" in component.LongName) and ("_gs:" not in component.LongName):
                componentNamespace = component.LongName.split(":")[0]
                if componentNamespace not in foundNamespaces:
                    foundNamespaces.append(componentNamespace)
        return foundNamespaces
    
    def isValidPath(self, filepath):
        """
        Takes filepath and returns False if it finds anything in the excluded list
        Used to single out bot/top video pairs.
        Also weeds out incorrectly placed videos.
        
        Arguments:
            filepath (str): filepath to check
            
        Returns:
            bool: Returns True if meets all folder/naming
            
        Example:
            Proper ref video's parent folders should be
            .../Face/[ Char Name ]/RS/REF/*.ref.mov
        """
        dirList = filepath.split("/")
        lastIndex = len(dirList)-1
        
        # Ignore REF video if parent folders does not match: .../Face/[ Ignore ]/RS/REF/
        if self.VIDPARAMS.VIDEO_SEARCH in filepath.lower():
            if (dirList[lastIndex-1].lower() != "ref") ^ (
                dirList[lastIndex-2].lower() != "rs") ^ (
                dirList[lastIndex-4].lower() != "face"):
                return False
        
        # exclude web files not in the right subfolder
        for fileExt in self.VIDPARAMS.WEB_SEARCH:
            if fileExt in filepath.lower():
                if (dirList[lastIndex-2].lower() != "face"):
                    return False
            
        return True
    
    @staticmethod
    def removeNonRefVideos(videoList):
        """
        Takes video list, and removes non-ref video duplicates (bot/top)
        
        Arguments:
            videoList (list): list of filepaths (str)
            
        Returns:
            list of str: List of filepaths (str)
        
        Example:
            TEST_MCS2_P1_T10.jcrot.ref.mov
            TEST_MCS2_P1_T10.jcrot.top.mov
            
        """
        pathsToRemove = []
        for videoPath in videoList:
            videoBaseName = os.path.basename(videoPath)
            videoName = ".".join(videoBaseName.split(".",2)[:2])
            for vidPath in videoList:
                vidBaseName = os.path.basename(vidPath)
                vidName = ".".join(vidBaseName.split(".",2)[:2])
                
                # If it's a duplicate non-ref name, but not the ref path, remove it from original list.
                if (videoName == vidName) and (".ref." in videoBaseName.lower()) and (videoBaseName != vidBaseName):
                    if vidPath not in pathsToRemove:
                        if ".gopro." not in vidPath.lower():
                            pathsToRemove.append(vidPath)
                        
                # Likewise if it's a top/bot duplicate and not a ref
                if (videoName == vidName) and (".top." in videoBaseName.lower()) and (videoBaseName != vidBaseName):
                    if (".ref." not in vidBaseName.lower()):
                        if vidPath not in pathsToRemove:
                            if ".gopro." not in vidPath.lower():
                                pathsToRemove.append(vidPath)
                            
        for videoPath in pathsToRemove:
            videoList.remove(videoPath)
            
        return videoList
    
    def fillTable(self):
        """
        Populates table view by building the data into the model, row by row.
        
        TableViewModel has 2 columns:
            Namespace:  Potential targets for movieplanes found by searching for CHARACTER_SEARCH
            Video Path:  Display text to represent video path.
                Video Path.data():  There is also the actual filepath attached to the QStandardItem in the 2nd column
        
        To fill the table, this performs the following in order:
            1. Pulls namespaces and videopaths from existing movie proxy geo first.
            2. Finds any new potential target namespaces not already in step 1.
            3. Scans local disk for all videopaths, and adds any not already found in step 1.
        
        """
        # Clear table model
        self.mappingTableModel.removeRows( 0, self.mappingTableModel.rowCount() )
        
        # Find existing movieplanes
        moviePlaneData, assignedVideos = self.findAllMoviePlanes(self.VIDPARAMS.VID_NAME, self.VIDPARAMS.VID_PATH_PROP)
        
        # Find existing webcam Planes
        webPlaneData, webAssignedVideos = self.findAllMoviePlanes(self.VIDPARAMS.WEB_NAME, self.VIDPARAMS.VID_PATH_PROP)
        
        # temporary dict to store values for sorting later
        tempTable = {}
        
        # 1 of 3: Populate list with data from existing movieplanes
        for charNamespace in moviePlaneData:
            charDisplay = QtGui.QStandardItem(charNamespace)
            vidDisplay = QtGui.QStandardItem("")
            vidDisplay.setData("")
            
            foundVideoPath = moviePlaneData[charNamespace][0]
            if os.path.isfile(foundVideoPath):
                vidDisplay = QtGui.QStandardItem(self.getDisplayNameFromPath(foundVideoPath))
                vidDisplay.setData(foundVideoPath)
                
            tempTable[charNamespace] = [charDisplay, vidDisplay]
        
        for charNamespace in webPlaneData:
            charDisplay = QtGui.QStandardItem(charNamespace)
            vidDisplay = QtGui.QStandardItem("")
            vidDisplay.setData("")
            
            foundVideoPath = webPlaneData[charNamespace][0]
            if os.path.isfile(foundVideoPath):
                vidDisplay = QtGui.QStandardItem(self.getDisplayNameFromPath(foundVideoPath))
                vidDisplay.setData(foundVideoPath)
            
            if charNamespace not in tempTable:
                tempTable[charNamespace] = [charDisplay, vidDisplay]
        
        # 2 of 3: Find all other potential namespaces (Ignores _gs skeleton namespaces)
        allNamespaces = self.findAllNamespacesFromComponent(self.VIDPARAMS.CHARACTER_SEARCH)
        for charNamespace in allNamespaces:
            if charNamespace not in moviePlaneData and charNamespace not in webPlaneData:
                charDisplay = QtGui.QStandardItem(charNamespace)
                vidDisplay = QtGui.QStandardItem("")
                vidDisplay.setData("")
                
                tempTable[charNamespace] = [charDisplay, vidDisplay]
        
        # Sort and add rows for data found in steps 1 and 2
        currentRow = 0
        for tableData in sorted(tempTable):
            self.mappingTableModel.setItem(currentRow, 0, tempTable[tableData][0])
            self.mappingTableModel.setItem(currentRow, 1, tempTable[tableData][1])
            currentRow+=1
        
        # clear the temporary dict to store values for sorting later
        tempTable = {}
        
        # 3 of 3: Find all other potential videos from local disk
        fbxFilepath = Globals.Application.FBXFileName
        fbxFolder = os.path.dirname(fbxFilepath)
        faceRootFolder = os.path.join(fbxFolder, "Face")
        if os.path.isdir(faceRootFolder):
            
            # Find videos and exclude files which are not valid
            videoPathList = self.getFileList(faceRootFolder, self.VIDPARAMS.VIDEO_SEARCH)
            for fileExt in self.VIDPARAMS.WEB_SEARCH:
                videoPathList = videoPathList + self.getFileList(faceRootFolder, fileExt)
            videoPathList = list(filter(self.isValidPath, videoPathList))
            
            # Also exclude duplicate bot/top videos to ref videos
            videoPathList = self.removeNonRefVideos(videoPathList)
            
            # Also exclude videos already found in steps 1 and 2
            for videoPath in videoPathList:
                if videoPath not in assignedVideos and videoPath not in webAssignedVideos:
                    charDisplay = QtGui.QStandardItem("")
                    vidDisplay = QtGui.QStandardItem(self.getDisplayNameFromPath(videoPath))
                    vidDisplay.setData(videoPath)
                    
                    tempTable[self.getDisplayNameFromPath(videoPath)] = [charDisplay, vidDisplay]
            
        # Sort and add rows from data found in step 3
        for tableData in sorted(tempTable):
            self.mappingTableModel.setItem(currentRow, 0, tempTable[tableData][0])
            self.mappingTableModel.setItem(currentRow, 1, tempTable[tableData][1])
            currentRow+=1
            
    def setupMovieplane(self, namespace, videoPath):
        """
        Function to create a single movieplane
        
        Arguments:
            namespace (str):  character namespace
            videoPath (str):  path of video
        """
        parentRootName = "{0}:{1}".format(namespace, self.VIDPARAMS.CHARACTER_SEARCH)
        parentRoot = mobu.FBFindModelByLabelName(parentRootName)
        if parentRoot:
            planeHeadName = "{0}:{1}".format(namespace, self.VIDPARAMS.WEB_NAME)
            planeHeadRoot = mobu.FBFindModelByLabelName("{0}_root".format(planeHeadName))
            planeHead = mobu.FBFindModelByLabelName(planeHeadName)
            
            # Create movieplane setup if it doesn't exist
            if not planeHeadRoot:
                # Create null
                planeHeadRoot = mobu.FBModelNull("{0}_root".format(planeHeadName))
                planeHeadRoot.Parent = parentRoot
                planeHeadRoot.Translation = mobu.FBVector3d(0,0,0)
                planeHeadRoot.Rotation = mobu.FBVector3d(0,0,0)
                
                # Create custom property to store video path
                videoPathProp = planeHeadRoot.PropertyCreate(self.VIDPARAMS.VID_PATH_PROP, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
                
                self.MOBU_SYSTEM.Scene.Evaluate()
            
            if not planeHead:
                # Create movieplane
                planeHead = mobu.FBCreateObject( "Browsing/Templates/Elements/Primitives", "Cylinder", "Cylinder" )
                planeHead.LongName = planeHeadName
                planeHead.Parent = planeHeadRoot
                planeHead.Translation.Data = self.VIDPARAMS.WEB_TRANS
                planeHead.Rotation.Data = self.VIDPARAMS.WEB_ROT
                planeHead.Scaling.Data = self.VIDPARAMS.WEB_SCALE
                planeHead.Show = True
                
            # Texture
            videoTexture = mobu.FBTexture(videoPath)
            videoTexture.LongName = "{0}_Txt".format(planeHeadName)
            videoTexture.Mapping = mobu.FBTextureMapping.kFBTextureMappingCylindrical
            videoTexture.PropertyList.Find("WrapModeU").Data = 1 #set to non-repeat
            videoTexture.PropertyList.Find("WrapModeV").Data = 1 #set to non-repeat
            #videoTexture.Rotation.Data = self.VIDPARAMS.TXT_ROT
            
            # Rename Video Clip
            for videoClip in self.MOBU_SYSTEM.Scene.VideoClips:
                if videoClip.Name == os.path.basename(videoPath):
                    videoClip.LongName = "{0}_Vid".format(planeHeadName)
                    
            # Material
            videoMaterial = None
            if len(planeHead.Materials) == 0:
                videoMaterial = mobu.FBMaterial("{0}_Mat".format(planeHeadName))
                planeHead.Materials.append(videoMaterial)
            else:
                videoMaterial = planeHead.Materials[0]
            videoMaterial.SetTexture(videoTexture, mobu.FBMaterialTextureType.kFBMaterialTextureDiffuse)
            makeItGlow = videoMaterial.PropertyList.Find("EmissiveColor")
            makeItGlow.Data = mobu.FBColor(1.0, 1.0, 1.0)
            
            planeHead.VisibilityInheritance = False
            planeHead.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingAll
            
            # Set Path Property
            videoProperty = planeHeadRoot.PropertyList.Find(self.VIDPARAMS.VID_PATH_PROP)
            if videoProperty:
                videoProperty.Data = videoPath
        
    def setupProxyGeo(self, namespace, videoPath):
        """
        Function to create a single video proxy geo
        
        Arguments:
            namespace (str):  character namespace
            videoPath (str):  path of video
        """
        # Check for valid parent
        headParentName = "{0}:{1}".format(namespace, self.VIDPARAMS.CHARACTER_SEARCH)
        headParent = mobu.FBFindModelByLabelName(headParentName)
        if headParent:
            proxyHeadName = "{0}:{1}".format(namespace, self.VIDPARAMS.VID_NAME)
            proxyHeadRoot = mobu.FBFindModelByLabelName("{0}_root".format(proxyHeadName))

            # Merge Geo if it doesn't exist
            if not proxyHeadRoot:
                mergeOptions = mobu.FBFbxOptions(True)
                mergeOptions.SetAll( mobu.FBElementAction.kFBElementActionMerge, False )
                mergeOptions.NamespaceList = namespace
                mergeOptions.Models = mobu.FBElementAction.kFBElementActionMerge
                mergeOptions.Materials = mobu.FBElementAction.kFBElementActionMerge
                Globals.Application.FileMerge(self.VIDPARAMS.VID_FILE, False, mergeOptions)
                proxyHeadRoot = mobu.FBFindModelByLabelName("{0}_root".format(proxyHeadName))
                
                if proxyHeadRoot:
                    skelRootName = "{0}:{1}".format(namespace, self.VIDPARAMS.CHARACTER_ROOT)
                    skelRoot = mobu.FBFindModelByLabelName(skelRootName)
                    proxyHeadRoot.Parent = headParent
                    proxyHeadRoot.Translation.Data = self.VIDPARAMS.VID_TRANS
                    proxyHeadRoot.Rotation.Data = self.VIDPARAMS.VID_ROT
                    proxyHeadRoot.Scaling.Data = self.VIDPARAMS.VID_SCALE
                    
                    # Create custom property to store video path
                    videoPathProp = proxyHeadRoot.PropertyCreate(self.VIDPARAMS.VID_PATH_PROP, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
                    videoPathProp.Data = videoPath
                    
                    # Create custom properties to store placement adjustments
                    transformProps = [
                        self.VIDPARAMS.VID_TOP_TX_PROP,
                        self.VIDPARAMS.VID_TOP_TY_PROP,
                        self.VIDPARAMS.VID_TOP_SCALE_PROP,
                        self.VIDPARAMS.VID_BOT_TX_PROP,
                        self.VIDPARAMS.VID_BOT_TY_PROP,
                        self.VIDPARAMS.VID_BOT_SCALE_PROP,
                        ]
                    
                    for propName in transformProps:
                        propInt = proxyHeadRoot.PropertyCreate(propName, mobu.FBPropertyType.kFBPT_int, "Integer", False, True, None)
                        propInt.Data = 0
                        
                    scaleInt = proxyHeadRoot.PropertyCreate(self.VIDPARAMS.VID_GEO_SCALE_PROP, mobu.FBPropertyType.kFBPT_int, "Integer", False, True, None)
                    scaleInitInt = proxyHeadRoot.PropertyCreate(self.VIDPARAMS.VID_GEO_SCALE_INIT_PROP, mobu.FBPropertyType.kFBPT_int, "Integer", False, True, None)
                    
                    bindPoseTrans = skelRoot.PropertyList.Find("Bind Pose Translation")
                    if bindPoseTrans:
                        if bindPoseTrans[2] > 0.04:
                            scaleInt.Data = 10
                            scaleInitInt.Data = 10
                        else:
                            scaleInt.Data = -10
                            scaleInitInt.Data = -10
                    else:
                        scaleInt.Data = 0
                        scaleInitInt.Data = 0
                    
                    # Set the scale for the proxy head
                    proxyHeadRoot.Scaling.Data = faceAdjWnd.FaceAdjustmentWindow.convertScaleToVector(scaleInt.Data)
                    
                    propBool = proxyHeadRoot.PropertyCreate(self.VIDPARAMS.VID_BOTH_LOCK, mobu.FBPropertyType.kFBPT_bool, "Bool", False, True, None)
                    propBool.Data = True
                    
            proxyHeadRoot.VisibilityInheritance = False
            proxyHeadGeo = mobu.FBComponentList()
            mobu.FBFindObjectsByName("{0}*".format(proxyHeadName), proxyHeadGeo, True, True)
            for comp in proxyHeadGeo:
                if comp != proxyHeadRoot:
                    # Texture
                    videoTexture = mobu.FBTexture(videoPath)
                    videoTexture.LongName = "{0}:{1}_Txt".format(namespace, comp.Name)
                    videoTexture.PropertyList.Find("WrapModeU").Data = 1 #set to non-repeat
                    videoTexture.PropertyList.Find("WrapModeV").Data = 1 #set to non-repeat
                    videoTexture.Translation.Data = self.VIDPARAMS.TXT_TRANS
                    videoTexture.Scaling.Data = self.VIDPARAMS.TXT_SCALE
                    
                    # Rename Video Clip
                    for videoClip in self.MOBU_SYSTEM.Scene.VideoClips:
                        if videoClip.Name == os.path.basename(videoPath):
                            videoClip.LongName = "{0}:{1}_Vid".format(namespace, comp.Name)
                            
                    # Material
                    videoMaterial = comp.Materials[0]
                    topBotName = comp.Name.rsplit("_",1)[1]
                    videoMaterial.Name = "{0}:{1}_{2}_Mat".format(namespace, self.VIDPARAMS.VID_NAME, topBotName)
                    videoMaterial.SetTexture(videoTexture, mobu.FBMaterialTextureType.kFBMaterialTextureDiffuse)
                    makeItGlow = videoMaterial.PropertyList.Find("EmissiveColor")
                    makeItGlow.Data = mobu.FBColor(1.0, 1.0, 1.0)
                    comp.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingAll
                    
            # Set Path Property
            videoProperty = proxyHeadRoot.PropertyList.Find(self.VIDPARAMS.VID_PATH_PROP)
            if videoProperty:
                videoProperty.Data = videoPath
            
            # Setup Texture Placement
            topX = proxyHeadRoot.PropertyList.Find(self.VIDPARAMS.VID_TOP_TX_PROP).Data
            topY = proxyHeadRoot.PropertyList.Find(self.VIDPARAMS.VID_TOP_TY_PROP).Data
            topScale = proxyHeadRoot.PropertyList.Find(self.VIDPARAMS.VID_TOP_SCALE_PROP).Data
            botX = proxyHeadRoot.PropertyList.Find(self.VIDPARAMS.VID_BOT_TX_PROP).Data
            botY = proxyHeadRoot.PropertyList.Find(self.VIDPARAMS.VID_BOT_TY_PROP).Data
            botScale = proxyHeadRoot.PropertyList.Find(self.VIDPARAMS.VID_BOT_SCALE_PROP).Data
            
            topTxt, botTxt = faceAdjWnd.FaceAdjustmentWindow.getTexture(proxyHeadRoot)
            newScaleTop, newTransTop = faceAdjWnd.FaceAdjustmentWindow.getNewTransforms(topScale, topX, topY, topTxt.Width, topTxt.Height)
            newScaleBot, newTransBot = faceAdjWnd.FaceAdjustmentWindow.getNewTransforms(botScale, botX, botY, botTxt.Width, botTxt.Height)
            
            if topTxt and botTxt:
                topTxt.Scaling.Data = newScaleTop
                topTxt.Translation.Data = newTransTop
                botTxt.Scaling.Data = newScaleBot
                botTxt.Translation.Data = newTransBot
    
    def setupAllMovieplanes(self):
        """
        Create a video proxy geo for each namespace with a paired videopath
        """
        # Destroy all existing Textures, VideoClips, and videclip properties
        headVideoGeoList = mobu.FBComponentList()
        mobu.FBFindObjectsByName("{0}*".format(self.VIDPARAMS.VID_NAME), headVideoGeoList, False, False)
        mobu.FBFindObjectsByName("{0}*".format(self.VIDPARAMS.WEB_NAME), headVideoGeoList, False, False)
        for headVideoGeo in headVideoGeoList:
            if (headVideoGeo.ClassName() == "FBTexture") or (headVideoGeo.ClassName() == "FBVideoClip"):
                headVideoGeo.FBDelete()
            elif headVideoGeo.ClassName() == "FBModelNull":
                videoPathProperty = headVideoGeo.PropertyList.Find(self.VIDPARAMS.VID_PATH_PROP)
                if videoPathProperty:
                    videoPathProperty.Data = ""
        
        # Destroy all old data
        oldDataList = mobu.FBComponentList()
        mobu.FBFindObjectsByName("{0}*".format(self.VIDPARAMS.OLD_VID_NAME), oldDataList, False, False)
        for oldData in oldDataList:
            oldData.FBDelete()
        
        # Sync Preview Face Proxy Geo OBJ
        Perforce.Sync(self.VIDPARAMS.VID_FILE)
        
        # If namespace and videopath in TableViewModel is paired
        for rowIndex in range(self.mappingTableModel.rowCount()):
            namespace = str(self.mappingTableModel.index(rowIndex,0).data())
            videoPath = str(self.mappingTableModel.item(rowIndex,1).data())
            if (namespace != "") and (videoPath != ""):
                foundFreehead = False
                for fileExt in self.VIDPARAMS.WEB_SEARCH:
                    if fileExt in videoPath.lower():
                        foundFreehead = True
                if foundFreehead:
                    # Create Movieplane
                    self.setupMovieplane(namespace, videoPath)
                else:
                    # Create proxy geo
                    self.setupProxyGeo(namespace, videoPath)
        
        # Hide ALL video heads and show only valid video heads
        self.toggleMoviePlanesBool(False)
        self.toggleMoviePlanesBool(True)
        self.MOBU_SYSTEM.Scene.Evaluate()
        
        QtGui.QMessageBox.information(self, "Save and Create", "Completed.")
        
    def resizeEvent(self, event):
        """
        Override to adjust tableview column widths dynamically to window size.
        """
        toolWidth = self.width()
        colWidth = int(toolWidth * 0.35)
        self.mappingTableView.setColumnWidth(0, colWidth)
        self.mappingTableView.setColumnWidth(1, toolWidth - colWidth - 40)
        QtGui.QMainWindow.resizeEvent(self, event)