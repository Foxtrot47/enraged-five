import pyfbsdk as mobu

def roundOffVideoFrameRate(videoComponent):
    """
    Adjusts the video frame rate to the nearest whole float value.
    
    Example:
        59.9197 rounds to 60.0
        29.97 rounds to 30.0
        
    Args:
        vidObj (pyfbsdk.FBVideoClip): The video component
        
    Returns:
        updated video frame rate (float)
    """
    videoComponent.FrameRate = round(videoComponent.FrameRate)
    return videoComponent.FrameRate

def Run():
    lSystem = mobu.FBSystem()
    validVideoList = [clip for clip in lSystem.Scene.VideoClips if ".ref" in clip.Filename.lower()]
    validVideoList = validVideoList + [clip for clip in lSystem.Scene.VideoClips if ".zoom" in clip.Filename.lower()]
    validVideoList = validVideoList + [clip for clip in lSystem.Scene.VideoClips if ".gopro" in clip.Filename.lower()]
    for videoClip in validVideoList:
        oldFrameRate = videoClip.FrameRate
        newFrameRate = roundOffVideoFrameRate(videoClip)
        print "frame rate of {0} changed from {1} to {2}".format(videoClip.Name,
                                                                 oldFrameRate,
                                                                 newFrameRate)