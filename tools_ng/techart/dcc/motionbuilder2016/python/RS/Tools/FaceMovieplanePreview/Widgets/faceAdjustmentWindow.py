import os

from PySide import QtGui, QtCore
import pyfbsdk as mobu

from RS import Globals, Perforce
from RS.Tools import UI
from RS.Tools.FaceMovieplanePreview import params

class AdjustSlider (QtGui.QSlider):
    def __init__(self, orientation, parent=None):
        super(AdjustSlider, self).__init__(orientation, parent)
    
    def mouseReleaseEvent(self, event):
        # Reset to 0
        if event.button() == QtCore.Qt.MouseButton.RightButton:
            self.setValue(0)

class FaceAdjustmentWindow(UI.QtBannerWindowBase):
    """
    Main window for Actor Face Preview Slider Adjustments
    """
    
    VIDPARAMS = params.VideoParams()
    
    def __init__(self, videoRoot, parent=None):
        winWidth = 200
        winHeight = 300
        
        self.VideoRoot = videoRoot
        self.Namespace = ""
        if ":" in videoRoot.LongName:
            self.Namespace = videoRoot.LongName.split(":")[0]
            
        super(FaceAdjustmentWindow, self).__init__(parent=parent,
                                                   title="Adjust Video Placement",
                                                           size=[winWidth, winHeight],
                                                           dockable=False,
                                                           dialog=False,
                                                           store=True,
                                                           closeExistingWindow = True)
        self.setupUi()
        
    def setupUi(self):
        """
        Setup for main UI.
        """
        
        # Style Presets
        btnHeight = 30
        btnHeightTall = 50
        lblWidth = 50
        stdMargin = 15
        widgetSpacing = 10
        
        sliderLength = 175
        sliderWidth = 30
        sliderValueMin = -40
        sliderValueMax = 175
        
        styleSheet = (      "QMenuBar {"
                            "background: #ffdc19;"
                                "font: bold;"
                                "color: black;"
                                "}"
                                "QSlider::horizontal {"
                            "min-width: S_LENGTH px;"
                                "min-height: S_WIDTH px;"
                                "max-width: S_LENGTH px;"
                                "max-height: S_WIDTH px;"
                                "}"
                                "QSlider::groove:horizontal {"
                            "background: gray;"
                                "height: 2px;"
                                "margin: 8px;"
                                "}"
                                "QSlider::handle:horizontal {"
                            "border: 3px solid black;"
                                "background: #ffdc19;"
                                "width: 13px;"
                                "margin: -8px;"
                                "border-radius: 4px;"
                                "}"
                                ).replace("S_LENGTH", str(sliderLength)
                                  ).replace("S_WIDTH", str(sliderWidth)
                                  )
        self.setStyleSheet(styleSheet)
        
        # Layouts
        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.setContentsMargins(stdMargin, stdMargin, stdMargin, stdMargin)
        self.mainLayout.setSpacing(widgetSpacing)
        self.geoLayout = QtGui.QVBoxLayout()
        self.geoScaleLayout = QtGui.QHBoxLayout()
        self.radioLayout = QtGui.QHBoxLayout()
        self.vidLayout = QtGui.QVBoxLayout()
        self.vidTransXLayout =  QtGui.QHBoxLayout()
        self.vidTransYLayout =  QtGui.QHBoxLayout()
        self.vidScaleLayout = QtGui.QHBoxLayout()
        self.btnLayout = QtGui.QHBoxLayout()
        
        # Widgets
        self.namespaceLbl = QtGui.QLabel("Tweaking video texture position for:")
        self.namespaceEdit = QtGui.QLineEdit()
        self.namespaceEdit.setReadOnly(True)
        self.namespaceEdit.setText(self.Namespace)
        
        self.geoAdjustGroup = QtGui.QGroupBox("Adjust Geo")
        self.geoScaleLbl = QtGui.QLabel("Scale:")
        self.geoScaleLbl.setFixedWidth(lblWidth)
        self.geoScaleSlider = AdjustSlider(QtCore.Qt.Horizontal)
        self.geoScaleSlider.setMinimum(sliderValueMin)
        self.geoScaleSlider.setMaximum(sliderValueMax)
        
        self.vidAdjustGroup = QtGui.QGroupBox("Adjust Video Placement")
        self.checkBoth = QtGui.QCheckBox("Both")
        self.checkBoth.setFixedWidth(lblWidth)
        self.radioTop = QtGui.QRadioButton("Upper")
        self.radioTop.setFixedWidth(lblWidth)
        self.radioBot = QtGui.QRadioButton("Lower")
        self.radioBot.setFixedWidth(lblWidth)
        self.vidTransXLbl = QtGui.QLabel("TX:")
        self.vidTransXLbl.setFixedWidth(lblWidth)
        self.vidTransYLbl = QtGui.QLabel("TY:")
        self.vidTransYLbl.setFixedWidth(lblWidth)
        self.vidScaleLbl = QtGui.QLabel("Scale:")
        self.vidScaleLbl.setFixedWidth(lblWidth)
        self.vidTransXSlider = AdjustSlider(QtCore.Qt.Horizontal)
        self.vidTransXSlider.setMinimum(sliderValueMin)
        self.vidTransXSlider.setMaximum(sliderValueMax)
        self.vidTransYSlider = AdjustSlider(QtCore.Qt.Horizontal)
        self.vidTransYSlider.setMinimum(sliderValueMin)
        self.vidTransYSlider.setMaximum(sliderValueMax)
        self.vidScaleSlider = AdjustSlider(QtCore.Qt.Horizontal)
        self.vidScaleSlider.setMinimum(sliderValueMin)
        self.vidScaleSlider.setMaximum(sliderValueMax)
        
        self.selectBtn = QtGui.QPushButton("Select/Deselect Geo")
        self.selectBtn.setFixedHeight(btnHeight)
        self.closeBtn = QtGui.QPushButton("Close")
        self.closeBtn.setFixedHeight(btnHeight)
        
        
        # Setup Layouts
        self.geoScaleLayout.addWidget(self.geoScaleLbl)
        self.geoScaleLayout.addWidget(self.geoScaleSlider)
        self.geoScaleLayout.addStretch(10)
        self.geoLayout.addLayout(self.geoScaleLayout)
        self.geoAdjustGroup.setLayout(self.geoLayout)

        self.radioLayout.addSpacing(lblWidth + widgetSpacing)
        self.radioLayout.addWidget(self.checkBoth)
        self.radioLayout.addWidget(self.radioTop)
        self.radioLayout.addWidget(self.radioBot)
        self.radioLayout.addStretch(10)
        self.vidTransXLayout.addWidget(self.vidTransXLbl)
        self.vidTransXLayout.addWidget(self.vidTransXSlider)
        self.vidTransXLayout.addStretch(10)
        self.vidTransYLayout.addWidget(self.vidTransYLbl)
        self.vidTransYLayout.addWidget(self.vidTransYSlider)
        self.vidTransYLayout.addStretch(10)
        self.vidScaleLayout.addWidget(self.vidScaleLbl)
        self.vidScaleLayout.addWidget(self.vidScaleSlider)
        self.vidScaleLayout.addStretch(10)
        self.vidLayout.addLayout(self.radioLayout)
        self.vidLayout.addLayout(self.vidTransXLayout)
        self.vidLayout.addLayout(self.vidTransYLayout)
        self.vidLayout.addLayout(self.vidScaleLayout)
        self.vidAdjustGroup.setLayout(self.vidLayout)
        
        self.btnLayout.addWidget(self.selectBtn, 5)
        self.btnLayout.addWidget(self.closeBtn, 2)
        
        self.mainLayout.addWidget(self.namespaceLbl)
        self.mainLayout.addWidget(self.namespaceEdit)
        self.mainLayout.addSpacing(widgetSpacing*2)
        self.mainLayout.addWidget(self.geoAdjustGroup)
        self.mainLayout.addSpacing(widgetSpacing)
        self.mainLayout.addWidget(self.vidAdjustGroup)
        self.mainLayout.addSpacing(widgetSpacing)
        self.mainLayout.addLayout(self.btnLayout)
        self.mainLayout.addStretch(10)
        
        self.Layout.addLayout(self.mainLayout, 0)
        
        # Set UI Options
        self.initializeOptions()
        
        # Signals
        self.selectBtn.pressed.connect(self.toggleGeoSelect)
        self.closeBtn.pressed.connect(self.closeWindow)
        
        self.checkBoth.stateChanged.connect(self.updateValuesCheckBoth)
        self.radioTop.released.connect(self.refreshOptions)
        self.radioBot.released.connect(self.refreshOptions)
        
        self.geoScaleSlider.valueChanged.connect(self.geoScaleSlider_Changed)
        self.vidTransXSlider.valueChanged.connect(self.updateValuesTX)
        self.vidTransYSlider.valueChanged.connect(self.updateValuesTY)
        self.vidScaleSlider.valueChanged.connect(self.updateValuesScale)
        
        self.vidTransXSlider.valueChanged.connect(self.vidPosition)
        self.vidTransYSlider.valueChanged.connect(self.vidPosition)
        self.vidScaleSlider.valueChanged.connect(self.vidPosition)
        
        self.vidPosition()
        
    def refreshOptions(self):
        dataDict = self.getTransformProps()
        self.geoScaleSlider.setValue(dataDict[self.VIDPARAMS.VID_GEO_SCALE_PROP])
        
        if self.radioTop.isChecked():
            self.vidTransXSlider.setValue(dataDict[self.VIDPARAMS.VID_TOP_TX_PROP])
            self.vidTransYSlider.setValue(dataDict[self.VIDPARAMS.VID_TOP_TY_PROP])
            self.vidScaleSlider.setValue(dataDict[self.VIDPARAMS.VID_TOP_SCALE_PROP])
        else:
            self.vidTransXSlider.setValue(dataDict[self.VIDPARAMS.VID_BOT_TX_PROP])
            self.vidTransYSlider.setValue(dataDict[self.VIDPARAMS.VID_BOT_TY_PROP])
            self.vidScaleSlider.setValue(dataDict[self.VIDPARAMS.VID_BOT_SCALE_PROP])
        
    def initializeOptions(self):
        isLockChecked = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_BOTH_LOCK)
        self.checkBoth.setChecked(isLockChecked.Data)
        self.radioTop.setChecked(True)
        self.refreshOptions()
    
    def getTransformProps(self):
        '''
        vidPath = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_PATH_PROP)
        geoScale = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_GEO_SCALE_PROP)
        vidTopTX = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_TOP_TX_PROP)
        vidTopTY = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_TOP_TY_PROP)
        vidTopScale = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_TOP_SCALE_PROP)
        vidBotTX = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_BOT_TX_PROP)
        vidBotTY = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_BOT_TY_PROP)
        vidBotScale = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_BOT_SCALE_PROP)
        '''
        transformPropNames = [
            self.VIDPARAMS.VID_GEO_SCALE_PROP,
            self.VIDPARAMS.VID_TOP_TX_PROP,
            self.VIDPARAMS.VID_TOP_TY_PROP,
            self.VIDPARAMS.VID_TOP_SCALE_PROP,
            self.VIDPARAMS.VID_BOT_TX_PROP,
            self.VIDPARAMS.VID_BOT_TY_PROP,
            self.VIDPARAMS.VID_BOT_SCALE_PROP,
            ]
        
        transformProps = {}
        
        for propName in transformPropNames:
            prop = self.VideoRoot.PropertyList.Find(propName)
            if prop:
                transformProps[propName] = prop.Data
        return transformProps
        
    def setTransformProps(self):
        pass
        
    def closeWindow(self):
        self.hide()
    
    def toggleGeoSelect(self):
        """
        """
            
        currentSelectBool = False
        for child in self.VideoRoot.Children:
            if child.Selected:
                currentSelectBool = True
        
        for model in Globals.Models.Selected:
            model.Selected = False
            
        for child in self.VideoRoot.Children:
            child.Selected = not currentSelectBool
    
    @staticmethod
    def getTexture(videoRoot):
        """
        """
        topTxt = None
        botTxt = None
        
        for geo in videoRoot.Children:
            if geo.LongName.endswith("_Top"):
                for mat in geo.Materials:
                    topTxt = mat.GetTexture()
            else:
                for mat in geo.Materials:
                    botTxt = mat.GetTexture()
        
        return topTxt, botTxt
    
    @staticmethod
    def convertScaleToVector(scalar):
        newValue = 0.000025*float(scalar) + 0.01025
        scaleVector = mobu.FBVector3d(newValue, newValue, newValue)
        return scaleVector
    
    def geoScaleSlider_Changed(self):
        vidScaleProp = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_GEO_SCALE_PROP)
        vidScaleProp.Data = self.geoScaleSlider.value()
        self.VideoRoot.Scaling.Data = self.convertScaleToVector(self.geoScaleSlider.value())
        
    def updateValuesCheckBoth(self):
        bothProp = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_BOTH_LOCK)
        bothProp.Data = self.checkBoth.isChecked()
        self.updateValuesTX()
        self.updateValuesTY()
        self.updateValuesScale()
        self.vidPosition()
    
    def updateValuesTX(self):
        if self.radioTop.isChecked() or self.checkBoth.isChecked():
            transXProp = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_TOP_TX_PROP)
            transXProp.Data = self.vidTransXSlider.value()
        if self.radioBot.isChecked() or self.checkBoth.isChecked():
            transXProp = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_BOT_TX_PROP)
            transXProp.Data = self.vidTransXSlider.value()
    
    def updateValuesTY(self):
        if self.radioTop.isChecked() or self.checkBoth.isChecked():
            transYProp = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_TOP_TY_PROP)
            transYProp.Data = self.vidTransYSlider.value()
        if self.radioBot.isChecked() or self.checkBoth.isChecked():
            transYProp = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_BOT_TY_PROP)
            transYProp.Data = self.vidTransYSlider.value()
    
    def updateValuesScale(self):
        if self.radioTop.isChecked() or self.checkBoth.isChecked():
            scaleProp = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_TOP_SCALE_PROP)
            scaleProp.Data = self.vidScaleSlider.value()
        if self.radioBot.isChecked() or self.checkBoth.isChecked():
            scaleProp = self.VideoRoot.PropertyList.Find(self.VIDPARAMS.VID_BOT_SCALE_PROP)
            scaleProp.Data = self.vidScaleSlider.value()
            
    @staticmethod
    def getNewTransforms(valScale, valTX, valTY, w, h):
        newValScaleY = ((-1*valScale/50.) + 1.0) * (500/h)
        newValScaleX = newValScaleY * (710/w)
        newValTX = (-1*valTX/400.) + (valScale/225.)
        newValTY = (-1*valTY/400.) + (valScale/100.)
        
        newScale = mobu.FBVector3d(newValScaleX, newValScaleY, 0)
        newTrans = mobu.FBVector3d(newValTX, newValTY, 0)
        
        return newScale, newTrans
    
    def vidPosition(self):
        textureList = []
        topTxt, botTxt = self.getTexture(self.VideoRoot)
        
        newScale, newTrans = self.getNewTransforms(self.vidScaleSlider.value(), self.vidTransXSlider.value(),
                                                   self.vidTransYSlider.value(), topTxt.Width, topTxt.Height)
        
        if self.radioTop.isChecked() or self.checkBoth.isChecked():
            if topTxt:
                topTxt.Scaling.Data = newScale
                topTxt.Translation.Data = newTrans
                
        if self.radioBot.isChecked() or self.checkBoth.isChecked():
            if botTxt:
                botTxt.Scaling.Data = newScale
                botTxt.Translation.Data = newTrans
        