import pyfbsdk as mobu
import os
import RS.Config

class VideoParams():
    # Node used to 'find' characters, also used as Parent to the movieplane
    CHARACTER_SEARCH = "SKEL_Head"
    CHARACTER_ROOT = "SKEL_ROOT"
    CHARACTER_SPINE = "SKEL_Spine4"
    
    P4_COMMENTS = "Actor Face Preview - Texture position data, please submit."
    
    # Movieplane parameters
    VID_FILE = os.path.join(RS.Config.Project.Path.Art,
                            "animation",
                            "resources",
                            "characters",
                            "characterHelpers",
                            "obj",
                            "ActorFaceMask.obj")
    VID_NAME = "ActorFaceMask"
    WEB_NAME = "ActorFacePlane"
    OLD_VID_NAME = "ActorFacePreview_MovieCyl"
    VID_PATH_PROP = "videoPath"
    VID_GEO_SCALE_PROP = "geoScale"
    VID_GEO_SCALE_INIT_PROP = "geoScale_Init"
    VID_TOP_TX_PROP = "vid_top_TX"
    VID_TOP_TY_PROP = "vid_top_TY"
    VID_TOP_SCALE_PROP = "vid_top_Scale"
    VID_BOT_TX_PROP = "vid_bot_TX"
    VID_BOT_TY_PROP = "vid_bot_TY"
    VID_BOT_SCALE_PROP = "vid_bot_Scale"
    VID_BOTH_LOCK = "vid_lock_topbot"
    VID_TRANS = mobu.FBVector3d(0.0, 0.0, 0.0)
    VID_ROT = mobu.FBVector3d(0, -90, -90)
    VID_SCALE = mobu.FBVector3d(0.01, 0.01, 0.01)
    WEB_TRANS = mobu.FBVector3d(0.6, 3.0, 0.0)
    WEB_ROT = mobu.FBVector3d(0, 90, -90)
    WEB_SCALE = mobu.FBVector3d(2.5,1.2,2.5)
    TXT_TRANS = mobu.FBVector3d(-0.0833, 0.0, 0.0)
    TXT_ROT = mobu.FBVector3d(0.0, 0.0, 90.0)
    TXT_SCALE = mobu.FBVector3d(1.25, 1.0, 1.0)
    
    # Geometry search filters
    GEO_SEARCH = [
        "*head_*",
        "*Head_*",
        "*hair_*",
        "*Hair_*",
        "*beard_*",
        "*Beard_*",
        "*teeth_*",
        "*Teeth_*",
        "*teef_*",
        "*Teef_*",
        "*eyes_*",
        "*Eyes_*",
        "*eyelashes_*",
        "*Eyelashes_*",
        ]
    
    # Video search filter
    VIDEO_SEARCH = ".ref.mov"
    WEB_SEARCH = [".zoom.mov", ".gopro.mov"]
    DATA_SEARCH = ".trans.csv"