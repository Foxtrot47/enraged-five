from PySide import QtGui, QtCore

import os

import pyfbsdk as mobu

from RS import Config, Globals
from RS.Tools import UI
from RS.Utils import Scene
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character
from RS.Core.ReferenceSystem.Types.Prop import Prop


class PropCharRootSnapWidgetDialog(UI.QtMainWindowBase):
    '''
    Dialogue to run PropCharRootSnapWidget and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PropCharRootSnapWidgetDialog, self).__init__(parent=parent)
        self._settings = QtCore.QSettings( "RockstarSettings", "PropCharRootSnapWidgetDialog" )
        propCharRootSnapWidget = PropCharRootSnapWidget()

        # create layout and add widget
        self.setCentralWidget(propCharRootSnapWidget)

    def showEvent(self, event):
        '''
        Set previously saved widget size and position preferences
        '''
        super(PropCharRootSnapWidgetDialog, self).showEvent( event )
        self.resize(self._settings.value("size", QtCore.QSize(300, 80)))
        self.move(self._settings.value("pos", QtCore.QPoint(200, 550)))

    def closeEvent(self, event):
        '''
        Save widget size and position preferences
        '''
        try:
            super(PropCharRootSnapWidgetDialog, self).closeEvent( event )
            self._settings.setValue("size", self.size())
            self._settings.setValue("pos", self.pos())
        except:
            pass


class PropCharRootSnapWidget(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PropCharRootSnapWidget, self).__init__(parent=parent)
        self.manager = Manager()
        self.setupUi()

    def _getSkelRootByNamespace(self, namespace):
        skelRootFound = None
        for character in Globals.Characters:
            if character.LongName.startswith('{0}:'.format(namespace)):
                characterFound = character
                skelRootFound = character.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
                break
        return skelRootFound

    def _getPropMeshByNamespace(self, namespace):
        propFound = None
        splitNamespace = namespace.split('^')
        name = splitNamespace[0]
        for component in Globals.Components:
            if component.LongName == '{0}:{1}'.format(namespace, name) and isinstance(component, mobu.FBModel):
                propFound = component
                break
        return propFound

    def _getCharacterList(self):
        '''
        Get list of characters in the scene
        Returns:
            List (Str) - namespaces of the characters in the scene
        '''
        characterNameList = []
        characters = self.manager.GetReferenceListByType(Character)
        for character in characters:
            characterNameList.append(character.Namespace)
        return characterNameList

    def _getPropList(self):
        '''
        Get list of props in the scene
        Returns:
            List (Str) - namespaces of the props in the scene
        '''
        propNameList = []
        props = self.manager.GetReferenceListByType(Prop)
        for prop in props:
            propNameList.append(prop.Namespace)
        return propNameList

    def _populateComboBoxes(self):
        '''
        Populate the comboboxes with strings
        '''
        self.characterCombo.addItem('None')
        if len(self._getCharacterList()) > 0:
            self.characterCombo.addItems(self._getCharacterList())

        self.propCombo.addItem('Use Scene Selected Object')
        if len(self._getPropList()) > 0:
            self.propCombo.addItems(self._getPropList())

    def _snapPropCharRoot(self):
        characterSkelRoot = None
        comboCharacterString = None
        prop = None
        comboPropName = None

        # if selected character string is 'None', launch popup warning
        if self.characterCombo.currentIndex() == 0:
            QtGui.QMessageBox.warning(None,
                                      "Warning",
                                      "No character is selected.",
                                      QtGui.QMessageBox.Ok)
            return
        else:
            # selected character string exists, find the FBCharacter model
            comboCharacterString = str(self.characterCombo.currentText())
            characterSkelRoot = self._getSkelRootByNamespace(comboCharacterString)


        # if selected prop is 'Use Selected', check what is selected
        if self.propCombo.currentIndex() == 0:
            selectedModelList = mobu.FBModelList()
            mobu.FBGetSelectedModels (selectedModelList, None, True)
            if len(selectedModelList) > 1:
                QtGui.QMessageBox.warning(None,
                                          "Warning",
                                          "Too many objects are selected, only select 1.",
                                          QtGui.QMessageBox.Ok)
                return
            elif len(selectedModelList) == 0:
                QtGui.QMessageBox.warning(None,
                                          "Warning",
                                          "An object has not been selected.\nPlease select 1 object from the scene.",
                                          QtGui.QMessageBox.Ok)
                return
            else:
                prop = selectedModelList[0]
        else:
            # selected prop is in list, find model in scene
            comboPropName = str(self.propCombo.currentText())
            prop = self._getPropMeshByNamespace(comboPropName)

        # if mover, characterSkelRoot and prop exist, proceed with null setup
        mover = Scene.FindModelByName('{0}:mover'.format(comboCharacterString), True)
        if characterSkelRoot and prop and mover:
            skelRootNull = mobu.FBModelNull('NULL - {0}_SkelRoot'.format(comboCharacterString))
            propRootNull = mobu.FBModelNull('NULL - {0}_{1}_PropRoot'.format(comboCharacterString, prop.Name))
            moverCube = mobu.FBModelCube('CUBE - {0}_Mover'.format(comboCharacterString))
            # element settings
            skelRootNull.Show = True
            propRootNull.Show = True
            moverCube.Show = True
            skelRootNull.Scaling = mobu.FBVector3d(14,14,14)
            propRootNull.Scaling = mobu.FBVector3d(14,14,14)
            moverCube.Scaling = mobu.FBVector3d(14,14,14)
            moverCube.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
            skelRootNullProperty = skelRootNull.PropertyCreate('CharPropSnap',
                                                                    mobu.FBPropertyType.kFBPT_charptr,
                                                                    "",
                                                                    False,
                                                                    True,
                                                                    None)
            skelRootNullProperty.Data = comboCharacterString
            propRootNullProperty = propRootNull.PropertyCreate('CharPropSnap',
                                                                    mobu.FBPropertyType.kFBPT_charptr,
                                                                    "",
                                                                    False,
                                                                    True,
                                                                    None)
            propRootNullProperty.Data = prop.Name
            moverCubeProperty = moverCube.PropertyCreate('CharPropSnap',
                                                                    mobu.FBPropertyType.kFBPT_charptr,
                                                                    "",
                                                                    False,
                                                                    True,
                                                                    None)
            moverCubeProperty.Data = 'mover'
            # parent and align
            Scene.Align(propRootNull, prop, True, True, True, True, True, True)
            Scene.Align(skelRootNull, characterSkelRoot, True, True, True, True, True, True)
            Scene.Align(moverCube, mover, True, True, True, True, True, True)
            mobu.FBSystem().Scene.Evaluate()

            skelRootNull.Parent = propRootNull
            moverCube.Parent = propRootNull

            # deselect all
            Scene.DeSelectAll()
            # select all object to save out 
            skelRootNull.Selected = True
            propRootNull.Selected = True
            moverCube.Selected = True

            # options for saving
            options = mobu.FBFbxOptions( False )
            options.SaveSelectedModelsOnly = True
            for index in range(0, options.GetTakeCount()):
                options.SetTakeSelect(index, False)
            # popup for saving
            scenePath = mobu.FBApplication().FBXFileName
            print scenePath
            popup = QtGui.QFileDialog()
            popup.setWindowTitle('Set a Save Location')
            popup.setAcceptMode(QtGui.QFileDialog.AcceptSave)
            popup.setViewMode(QtGui.QFileDialog.List)
            popup.setDirectory(scenePath)
            popup.setNameFilters(["*.fbx", "*.FBX"])
            popup.selectFile("NULL_SETUP - {0}.fbx".format(comboCharacterString))
            if popup.exec_() :
                # save selected
                savePath = popup.selectedFiles()[0]
                savePath = str(os.path.normpath(savePath))
                if savePath.lower().endswith('.fbx'):
                    mobu.FBApplication().FileSave(savePath, options)
                else:
                    mobu.FBApplication().FileSave('{0}.fbx'.format(savePath), options )
                QtGui.QMessageBox.warning(None,
                                          "Save Complete",
                                          "Selected Saved:\n\n{0}".format(savePath),
                                          QtGui.QMessageBox.Ok)

    def _deleteAllNulls(self):
        result = QtGui.QMessageBox.warning(None,
                                          "Warning",
                                          "Are you sure you wish to delete all of the null Setups?",
                                          QtGui.QMessageBox.Yes|QtGui.QMessageBox.Cancel)
        if not result == QtGui.QMessageBox.Yes:
            return

        deleteList = []
        for component in Globals.Components:
            nullProperty = component.PropertyList.Find("CharPropSnap")
            if nullProperty:
                deleteList.append(component)

        for item in deleteList:
            item.FBDelete()

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # deselect all
        Scene.DeSelectAll()

        # create layouts
        mainLayout = QtGui.QGridLayout()

        # widgets
        self.characterLabel = QtGui.QLabel()
        self.characterCombo = QtGui.QComboBox()
        self.propLabel = QtGui.QLabel()
        self.propCombo = QtGui.QComboBox()
        self.snapButton = QtGui.QPushButton()
        horizontalLine = QtGui.QFrame()
        self.deleteButton = QtGui.QPushButton()

        # widget properties
        self.characterLabel.setText("Character:")
        self.propLabel.setText("Prop:")
        self.propCombo.setToolTip("Select a prop from this dropdown or manually\nselect a prop's root from the scene.")
        self.snapButton.setText("Setup Nulls")
        horizontalLine.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine.setFrameShadow(QtGui.QFrame.Sunken)
        self.deleteButton.setText("   Delete all CharacterRoot-PropRoot Nulls   ")

        # populate comboboxes
        self._populateComboBoxes()

        # add widgets to layout
        mainLayout.addWidget(self.characterLabel, 0, 0)
        mainLayout.addWidget(self.characterCombo, 0, 1)
        mainLayout.addWidget(self.propLabel, 1, 0)
        mainLayout.addWidget(self.propCombo, 1, 1)
        mainLayout.addWidget(self.snapButton, 2, 1)
        mainLayout.addWidget(horizontalLine, 3, 1)
        mainLayout.addWidget(self.deleteButton, 4, 1)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        self.snapButton.released.connect(self._snapPropCharRoot)
        self.deleteButton.released.connect(self._deleteAllNulls)