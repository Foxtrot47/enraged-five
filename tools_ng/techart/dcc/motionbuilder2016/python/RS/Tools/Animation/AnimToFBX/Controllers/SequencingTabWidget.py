from pyfbsdk import *
from PySide import QtCore, QtGui
from pyfbsdk_additions import *

import RS.Tools.UI


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gProjRoot = RS.Config.Project.Path.Root
gProjName = str(RS.Config.Project.Name)

RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\Animation\\AnimToFBX\\UI\\SequencingTabWidget_QT.ui'.format(RS.Config.Script.Path.Root), '{0}\\RS\\Tools\\Animation\\AnimToFBX\\ViewWidgets\\SequencingTabWidget_QT.py'.format(RS.Config.Script.Path.Root))
import RS.Tools.Animation.AnimToFBX.ViewWidgets.SequencingTabWidget_QT as seqTab
reload(seqTab)


class SequencingTabWidget( RS.Tools.UI.QtMainWindowBase, seqTab.Ui_Form  ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, None)
        
        # Required to create the ui from Qt Designer
        self.setupUi(self)
	
