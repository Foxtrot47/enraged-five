"""
This module contains code that retrieves or sets data from the QT UI of the AnimToFBX Tool.

The main purpose of this module is to separate the functionality of the tool from the UI components.
"""
import os
import clr

import pyfbsdk as mobu

import RS.Perforce as p4
from RS.Utils import Scene
from RS.Tools.Animation.AnimToFBX.Controllers import AnimUtils

clr.AddReference("RSG.Base.Configuration")
from RSG.Base.Configuration import ConfigFactory

_Config = ConfigFactory.CreateConfig()


def syncSelectedFile(filename=None):
    """
    Syncs the latest version of the provided file from perforce

    Arguments:
        filename (string): name of the file to sync
    """
    # its a folder if it doesn't end with ".anim"  
    if not filename.endswith("anim"):
        cmd = "files"
        fName = filename.replace('/', '\\')
        mName = fName.replace('@', '%40')
        args = ["-e", "{0}/...anim".format(mName)]
        p4Files = p4.Run(cmd, args)
        for record in  p4Files.Records:
            rPath = record['depotFile']
            p4.Sync(rPath)

    # is file read-only
    elif not os.access(filename, os.W_OK):
        # sync
        fName = filename.replace('@', '%40')
        p4.Sync(fName)


def GetAnimTreeWidget(mainWidget, widgetName):
    """
    Utility method for getting widgets on the animTree widget.

    Arguments:
        mainWidget: (QtGui.QWidget) the Anim2Fbx Main Window Widget
        widgetName: (string) the name of the widget

    Return:
        QT Widget instance
    """
    if getattr(mainWidget, "_animTree", None):
        mainWidget = mainWidget._animTree

    return getattr(mainWidget, widgetName)


def GetAnimTreeCheckboxValue(mainWidget, checkboxName):
    """
    Gets the value of the checkbox 
    Arguments:
        mainWidget (QtGui.QWidget): the Anim2Fbx Main Window Widget
        cehckboxName (string): the name of the checkkbox widget

    Return:
        Boolean
    """
    return GetAnimTreeWidget(mainWidget, checkboxName).isChecked()


def LoadAnim(mainWidget, selection=[]):
    """
    Imports an animation with the settings selected on the UI

    Arguments:
        mainWidget (QtGui.QWidget): the Anim2Fbx Main Window Widget

    Keyword Arguments:
        selection (list[string, etc.]): path to animfiles or folders containing animfiles
    """
    currentSelection = Scene.GetSelection(filterByType=mobu.FBCharacter)
    if not currentSelection:
        mobu.FBMessageBox("Warning", 'No Character was selected through the Navigator.', "OK")
        return

    namespaces = Scene.GetNamespacesFromSelection(filterByType=mobu.FBCharacter)
    if not namespaces:
        mobu.FBMessageBox("Warning", 'No Character was not referenced.', "OK")
        return

    # get the state of the cbImportToCurrentTake and syncCheckbox
    useTake = GetAnimTreeCheckboxValue(mainWidget, "cbImportToCurrentTake")
    sync = GetAnimTreeCheckboxValue(mainWidget, "syncCheckbox")
    skelFile = ""

    if GetAnimTreeCheckboxValue(mainWidget, "useSkelCheckbox"):
        skelFile = mainWidget.GetSkeletonPath()
        
    # what is the name, type info , and path of selected
    if selection and not isinstance(selection, list):
        selection = [selection]

    elif not selection:
        selection = [mainWidget._proxyModel.mapToSource(each).data(34)
                     for each in mainWidget.treeView.selectedIndexes()]

    for each_selection in selection:
        # Determine if the each_selection is a file

        if not each_selection:
            continue

        each_selection = str(each_selection)
        each_selection = each_selection.replace("%40", "@")
        is_file = ".anim" in each_selection

        folder_file = ["Folder", "File"][is_file]

        # Get the correct method to load the file/folder
        load_method_name = "Load{}".format(folder_file)
        load_method = getattr(AnimUtils, load_method_name)

        # Run method
        load_method(each_selection, skeletonPath=skelFile, sync=sync, useCurrentTake=useTake, namespaces=namespaces)


def ClearSelectionList(mainWidget):
    """
    Clears the Selection
    Arguments:
        mainWidget (QtGui.QWidget): widget that contains a list widget under the variable _listWidget
    """
    mList = mainWidget._listWidget._selModel.getRoot()
    mainWidget._listWidget._selModel.removeRows(0, mList.childCount())


def adjustSelectionSetsColumnwidth(viewWidget):
    """
    Adjust the width of the column for the provided viewWidget

    Arguments:
        viewWidget (QtGui.QListWidget): widget to adjust
    """
    viewWidget.expandAll()
    width = viewWidget.sizeHintForColumn(0) + 5
    viewWidget.setColumnWidth(0, width)
    viewWidget.collapseAll()
