"""
Tree Widget for displaying files on the local machine and from Perforce

MOVE TO GENERIC PLACE
"""
import os

from PySide import QtGui, QtCore

from RS import Perforce, ProjectData

from RS.Tools.Animation.Anim2Fbx.Widgets import FileTree
from RS.Tools.Animation.Anim2Fbx.Models import ModelManager
from RS.Tools.Animation.Anim2Fbx.Models import PerforceFilterModel

PROGRESSBAR_COLOR, _ = ProjectData.data.GetConfig("MenuColour", defaultValue=("#ffdc19", "black"))


class ModelManagerTree(FileTree.FileTreeWidget):
    """
    Tree widget for displaying files from perforce
    """

    def __init__(self, title="Open Cutscene", directory="", parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
            title (string): title for the widget
            rootDirectory (string): root directory where the files that should be displayed live
        """
        QtGui.QWidget.__init__(self, parent=parent)

        self._block = False
        self._disableProgressBar = False
        self._excludeFilters = []
        self._fileExtensions = []
        self._expandMinimum = 2
        self._ShowLocal = False

        self._currentSelection = tuple()
        self.rootDirectory = ""
        self.perforceDirectory = ""

        self.manager = ModelManager.ModelManager()
        self.filter = PerforceFilterModel.PerforceFilterModel()
        self.filter.setFilterRole(ModelManager.AnimStandardItem.Path)
        self.setDirectory(directory)
        self.filter.setSourceModel(self.manager.CurrentModel())

        self.filterBox = QtGui.QLineEdit()
        self.filterBox.textChanged.connect(self.textChanged)
        self.filterBox.returnPressed.connect(lambda *args: self.textChanged(self.filterBox.text()))

        expandAllButton = QtGui.QPushButton("+")
        collapseAllButton = QtGui.QPushButton("-")

        self.stack = QtGui.QStackedWidget()

        self.progressBar = QtGui.QProgressBar()
        self.progressBar.setOrientation(QtCore.Qt.Vertical)
        self.progressBar.setTextVisible(False)
        self.progressBar.setInvertedAppearance(True)

        self.tree = QtGui.QTreeView()
        self.tree.setModel(self.filter)
        self.tree.selectionChanged = self.selectionChanged

        self.setWindowTitle(title)

        expandAllButton.setFixedWidth(20)
        collapseAllButton.setFixedWidth(20)

        layout = QtGui.QVBoxLayout()
        filterLayout = QtGui.QHBoxLayout()

        filterLayout.addWidget(QtGui.QLabel(" Filter "))
        filterLayout.addWidget(self.filterBox)
        filterLayout.addWidget(expandAllButton)
        filterLayout.addWidget(collapseAllButton)

        self.stack.addWidget(self.progressBar)
        self.stack.addWidget(self.tree)

        layout.addLayout(filterLayout)
        layout.addWidget(self.stack)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        self.tree.expanded.connect(self.expanded)
        self.tree.collapsed.connect(self.expanded)

        expandAllButton.clicked.connect(self.tree.expandAll)
        collapseAllButton.clicked.connect(self.tree.collapseAll)

        self.tree.setSelectionMode(self.tree.SingleSelection)
        self.tree.setMouseTracking(False)
        self.tree.setDragEnabled(False)
        self.tree.setAcceptDrops(False)
        self.setLayout(layout)
        self.setStyleSheet(
            "QProgressBar {"
            "    border: 2px solid grey;"
            "    border-radius: 5px;"
            "    text-align: center;"
            "}"
            "QProgressBar::chunk {"
            "    background-color: %s;"
            "    height: 10px;"
            "    margin: 0.5px;"
            "}" % PROGRESSBAR_COLOR
        )

    def UpdateView(self):
        """ Populates the tree view """

        if self.filter.local() is not self.ShowLocal:
            self.filter.setLocal(self.ShowLocal)

        self.manager.CurrentModel().setHeaderData(0, QtCore.Qt.Horizontal, self.windowTitle())
        self.stack.setCurrentIndex(1)

    def Model(self):
        return self.manager.CurrentModel()

    def Select(self, path):
        """
        Selects the file on the tree view with the given path

        Arguments:
            path (string): path of the file to select on the tree view

        """
        if self.rootDirectory not in path:
            path = path.replace(self.perforceDirectory, self.rootDirectory).replace(os.path.sep, "\\").lower()

        standardItem = self.manager.Items.get(path, None)

        if standardItem:
            modelIndex = self.manager.Model().indexFromItem(standardItem)
            filterModelIndex = self.filter.mapFromSource(modelIndex)
            selectionModel = self.tree.selectionModel()
            selectionModel.setCurrentIndex(filterModelIndex, selectionModel.Select)
            return filterModelIndex

    def setDirectory(self, path):
        """
        Sets the directory to look for files to display

        Arguments:
            path (string): path to directory

        """
        if path != self.rootDirectory:
            progressBar = None
            if not self._disableProgressBar:
                progressBar = self.progressBar
                progressBar.reset()
                self.stack.setCurrentIndex(0)
            self.rootDirectory = path
            self.perforceDirectory = Perforce.ConvertRecordSetToDictionary(
                Perforce.Run('dirs', [path])).get("dir", self.rootDirectory)

            self.manager.CurrentRoot = path

            if self.manager.CurrentModel() is None:
                self.manager.AddModel(path, QtGui.QStandardItemModel())
                self.manager.BuildModelItems(progressBar=progressBar)

            self.filter.setSourceModel(self.manager.CurrentModel())

    def expanded(self, modelIndex):
        """
        When shift is selected, expand all the children in one go

        Arguments:
            modelIndex (QtGui.QModelIndex): the item that was just expanded

        """
        modifiers = QtGui.QApplication.keyboardModifiers()
        maxDepth = None if modifiers == QtCore.Qt.ShiftModifier else 0

        if not modelIndex.data(ModelManager.AnimStandardItem.Loaded):
            filterModelIndex = self.filter.mapToSource(modelIndex)
            item = self.manager.CurrentModel().itemFromIndex(filterModelIndex)
            self.manager.BuildModelRecursively(directory=modelIndex.data(ModelManager.AnimStandardItem.Path),
                                               parent=item, maxDepth=maxDepth)
            if item.child(0) and item.child(0).data(ModelManager.AnimStandardItem.Delete) is True:
                item.removeRow(0)
            item.setData(True, ModelManager.AnimStandardItem.Loaded)
        self.filter.setLocal(self.ShowLocal)
        super(ModelManagerTree, self).expanded(modelIndex)
