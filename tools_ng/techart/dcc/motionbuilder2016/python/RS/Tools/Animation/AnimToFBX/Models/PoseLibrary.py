from PySide import QtCore, QtGui
import RS.Config
import RS.Tools.Animation.AnimToFBX.Resources.resources
import os

class PoseLibrary(QtCore.QAbstractItemModel):
    """ Inputs: Node, QObject"""
    def __init__(self, root, parent = None):
        super(PoseLibrary, self).__init__(parent)

        ### private attributes ###
        self._rootNode = root

    ### View needs to know how many items this model contains ###
    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def rowCount(self, parent):
        if not parent.isValid():
            parentNode = self._rootNode
        else:
            parentNode = parent.internalPointer()

        return parentNode.childCount()

    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def columnCount(self, parent):	
        return 1

    """ Inputs: int, Qt::Orientation, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if section == 0:
                    return "Pose"

                elif section == 1:
                    return "Anim"

                elif section == 2:
                    return "Frame"
                
                elif section == 3:
                    return "Date"
                
                elif section == 4:
                    return "User"
                
    """ Inputs: QModelIndex"""
    """ Outputs: int (flag)"""	
    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    """ Inputs: QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return the parent of the node with the given QModelIndex"""
    def parent(self, index):
        node = self.getNode(index)
        parentNode = node.parent()

        if parentNode == self._rootNode:
            #root node, create an empty QModelIndex
            return QtCore.QModelIndex()

        # wrap in a QModelIndex
        return self.createIndex(parentNode.row(), 0, parentNode)

    """ Inputs: int, int, QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return a QModelIndex that corresponds to the given row, column, and parent node"""	
    def index(self, row, column, parent):
        # is the parent valid (when QmodelIndex is empty)
        parentNode = self.getNode(parent)

        if parentNode.childCount() != 0:
            childItem = parentNode.child(row)
        else:
            childItem = None

        #if there are no children
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()	


    ### returns True or False depending on whwther the data was valid or not    ###
    """ Inputs: QModelIndex, QVariant, int (flag)"""

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        
        if index.isValid():
            
            node = index.internalPointer()
            
            if role == QtCore.Qt.EditRole:
                
                if index.column() == 0:
                    node.setName(value)
                    self.dataChanged.emit(index, index)
                return True

        return False

    """ Inputs: QModelIndex, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def data(self, index, role):

        if not index.isValid():
            return None
        node = index.internalPointer()

        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            if index.column() == 0:
                return node.name()
            elif index.column() == 1:
                return node.typeInfo()
                #return node.anim()
            elif index.column() == 2:
                return node.frame()
            elif index.column() == 3:
                return node.date()
            else:
                return node.user()

        if role == QtCore.Qt.DecorationRole:
            if index.column() == 0:
                typeInfo = node.typeInfo()

                if typeInfo == "Category":
                    return QtGui.QIcon(QtGui.QPixmap(":/folder.png"))
                elif typeInfo == "Pose":
                    return QtGui.QIcon(QtGui.QPixmap(":/depotFile_icon.png"))

    """ Inputs: QModelIndex"""

    def getNode(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self._rootNode