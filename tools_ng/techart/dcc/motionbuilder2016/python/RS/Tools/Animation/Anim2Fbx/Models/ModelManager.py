"""
Singleton to manage multiple models at time and be able to create/access them in different locations
"""
import os
import time

from PySide import QtGui, QtCore

from RS import Perforce, Config
from RS.Tools.CameraToolBox.PyCore.Decorators import timeIt


class SingletonDecorator:
    """
    Decorator to convert a class into a singleton
    """
    def __init__(self, _class):
        """
        Constructor

        Arguments:
            _class (class): class to convert into a singleton
        """
        self._class =_class
        self._instance = None

    def __call__(self, *args, **kwds):
        """
        Overrides built-in method. When the class is called, if an instance was already created, return that instance.
        """
        if self._instance is None:
            self._instance = self._class(*args, **kwds)
        return self._instance


class AnimStandardItem(QtGui.QStandardItem):
    """
    Custom QStandardItem for displaying the icon that represents the state of a file on perforce
    """

    Local = QtCore.Qt.UserRole + 1
    Perforce = QtCore.Qt.UserRole + 2
    Icon = QtCore.Qt.UserRole + 3
    Path = QtCore.Qt.UserRole + 4
    Delete = QtCore.Qt.UserRole + 5
    Loaded = QtCore.Qt.UserRole + 6
    Synched = QtCore.Qt.UserRole + 7
    IsFile = QtCore.Qt.UserRole + 8

    def data(self, role=QtCore.Qt.DisplayRole):
        """
        Overrides built-in method
        When QtCore.Qt.DisplayRole is passed, the icon that represents the state of the file this item
        represent is returned

        Arguments:
            role (QtCore.Qt.Role): role being requested of the Standard Item
        """
        # Display the proper icon
        if role == QtCore.Qt.DecorationRole:
            local = super(AnimStandardItem, self).data(self.Local)
            perforce = super(AnimStandardItem, self).data(self.Perforce)
            # Get if the local and head revisions match
            synched = super(AnimStandardItem, self).data(self.Synched)

            # Get if item is representing a file
            isFile = super(AnimStandardItem, self).data(self.IsFile)

            filename = "folder.png"
            if isFile and not local and perforce:
                filename = "perforce.png"

            elif isFile and not local and not perforce:
                filename = "missing.png"

            elif isFile and local and not perforce:
                filename = "local.png"

            elif isFile and local and perforce and synched:
                filename = "sync.png"

            elif isFile and local and perforce and not synched:
                filename = "outofdate.png"
            path = os.path.join(Config.Script.Path.ToolImages, "Perforce", filename)
            return QtGui.QIcon(path)
        return super(AnimStandardItem, self).data(role)


@SingletonDecorator
class ModelManager(object):

    """
    This class manages model creation for the FileTreeWidget. It caches model creation result and makes sure that
    we only recreate the models if they changed or no longer exist. This also uses a singleton pattern so that the
    models can be built outside of the main tool if needed.
    """

    def __init__(self, root="", fileExtension="anim"):
        """ Constructor """
        self.items = {}
        self._root = root
        self._roots = []
        self.fileExtension = fileExtension

    @property
    def CurrentRoot(self):
        """ The current root path  """
        return self._root

    @CurrentRoot.setter
    def CurrentRoot(self, path):
        """
        Sets the current root path

        Arguments:
            path (string): root path for the directory that should be made into a QAbstractModel
        """
        self._root = path
        if path not in self._roots:
            self._roots.append(path)

    def Roots(self):
        """
        List of paths that have been added as roots

        Return:
            list[string, etc.]
        """
        return self._roots

    def CurrentModel(self):
        """
        The current QStandardItemModel

        Return:
            QStandardItemModel or None
        """
        return self.items.get(self._root, None)

    def SetCurrentModel(self, model):
        """
        Associates a QStandardItemModel with the current root path

        Arguments:
            model (QtGui.QStandardItemModel): Item Model to associate with the current root path
        """
        self.items[self._root.lower()] = model

    def AddModel(self, path, model):
        """
        Associates a model or model item with the given path

        Arguments:
            path (string): path to a file/directory
            model (QtGui.QStandardItem or QtGui.QStandardItemModel): model or model item to associate with the given
                                                                     path

        """
        self.items[path] = model

    def LocalFiles(self, path, fileExtensionFilter="anim"):
        """
        List of local files in the local directory

        Arguments:
            path: (string): path to local directory with local files

        """
        join = os.path.join
        return [join(directory, file).lower() for directory, folders, files in os.walk(path)
                for file in files if file.endswith(fileExtensionFilter)]

    def PerforceFiles(self, path, fileExtensionFilter="anim"):
        """
        List of local files in the local directory

        Arguments:
            path: (string): path to local directory with local files

        """
        p4Path = os.path.join(path, "...{}".format(fileExtensionFilter))
        return ["X:{}".format(filepath['depotFile'][1:].replace("/", "\\").lower())
                for filepath in Perforce.Run("files", ["-e", p4Path])]

    def AllFiles(self):
        """
        Returns all the files available on Perforce and that are on the local machine
        """
        localFiles = self.LocalFiles(self.CurrentRoot, fileExtensionFilter=self.FileExtension)
        perforceFiles = self.PerforceFiles(self.CurrentRoot, fileExtensionFilter=self.FileExtension)
        allFiles = {localfile: {"local": True, "perforce": False} for localfile in localFiles}
        for perforceFile in perforceFiles:
            allFiles.setdefault(perforceFile, {"local": False})
            allFiles[perforceFile]["perforce"] = True

        return allFiles, localFiles, perforceFiles

    @timeIt.timeIt
    def BuildModelItems(self, progressBar=None):
        """
        Builds the model items

        Arguments:
            progressBar (QtGui.QProgressBar): progress bar to update

        """
        self.BuildModelRecursively(self.CurrentRoot, self.CurrentModel())


    def BuildPathItem(self, path):
        """
        Creates the standard items for the tree view
        Arguments:
            path (string): path to the file
        """
        path = path.replace(os.path.sep, "/").replace("%40", "@")
        _, filename = os.path.split(path)

        item = AnimStandardItem(filename)
        item.setEditable(False)
        item.setToolTip(path)

        isFile = os.path.splitext(filename.lower())[-1].endswith(self.fileExtension)

        item.setData(path, item.Path)
        item.setData(False, item.Delete)
        item.setData(isFile, item.Loaded)
        item.setData(isFile, item.IsFile)

        return item

    def BuildModelRecursively(self, directory, parent=None, maxDepth=0, depth=0):
        """
        Recursively builds the model items based on the folder structure of of the given path if it is a directory

        Arguments:
            directory (string): path to the directory or file
            parent (QtGui.QStandardItem): the parent item for the items that will be created
            maxDepth (int): max recursion depth to go to. Defaults to 0. When None is passed, it will traverse to the
                            bottom of each directory it finds.
            depth (int): the current depth of the recursion

        Return:
            boolean
        """
        parent = parent or self.items.get(os.path.dirname(directory).lower(), None)
        p4Directory = directory.replace("@", "%40")

        if not parent:
            return False

        for command, filter, key, isFolder in (("dirs", "", "dir", True),
                                               ("files", self.fileExtension, "depotFile", False)):
            isCommand = (os.path.isfile, os.path.isdir)[isFolder]

            paths = {}
            # If the directory/file exists on the user's machine, store that it is local
            if os.path.exists(directory):
                paths = {os.path.join(directory, path).replace("/depot", "").replace("/", "\\").lower():
                             {"local": True, "perforce": False}
                         for path in os.listdir(directory)
                         if os.path.exists(os.path.join(directory, path)) and
                         isCommand(os.path.join(directory, path)) and path.lower().endswith(filter)}

            # Get the files from perforce
            for record in Perforce.Run(command, [r"{directory}\*{filter}".format(directory=p4Directory, filter=filter)]):

                # Skip file that have been removed from P4
                try:
                    if record["type"] == "delete":
                        continue
                except:
                    pass

                # Convert the %40 that P4 returns in the file names to @ display the correct file/folder names

                path = "X:{}".format(record[key][1:].replace("/depot", "").replace("/", "\\")).lower().replace("%40", "@")
                paths.setdefault(path, {"local": False})
                paths[path]["perforce"] = True

            # Build the actual model item
            for path in sorted(paths.iterkeys()):
                values = paths[path]
                item = self.items.get(path, None)

                if not item and parent is not None:
                    item = self.BuildPathItem(path)
                    self.items[path.lower()] = item
                    parent.appendRow(item)

                item.setData(values["local"], item.Local)
                item.setData(values["perforce"], item.Perforce)
                item.setData(False, item.Synched)

                if values["local"] and values["perforce"] and not isFolder:
                    filestate = Perforce.GetFileState(path)
                    item.setData(filestate.HeadRevision == filestate.HaveRevision, item.Synched)

                if isFolder and (maxDepth is None or maxDepth > depth):
                    self.BuildModelRecursively(directory=path, parent=item, maxDepth=maxDepth, depth=depth+1)
                    item.setData(True, self.Loaded)

                elif isFolder and maxDepth == depth:
                    # Build dummy to be able to open folders
                    _item = QtGui.QStandardItem("Not Loaded")
                    _item.setEditable(False)
                    _item.setToolTip(path)
                    _item.setData(values["local"], AnimStandardItem.Local)
                    _item.setData(values["perforce"], AnimStandardItem.Perforce)
                    _item.setData(path, AnimStandardItem.Path)
                    _item.setData(True, AnimStandardItem.Delete)
                    item.appendRow(_item)

        return True
