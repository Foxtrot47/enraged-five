from pyfbsdk import *
from PySide import QtCore, QtGui
from pyfbsdk_additions import *

import RS.Tools.UI


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gProjRoot = RS.Config.Project.Path.Root
gProjName = str(RS.Config.Project.Name)

RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\Animation\\AnimToFBX\\UI\\SelectionListWidget_QT.ui'.format(RS.Config.Script.Path.Root), '{0}\\RS\\Tools\\Animation\\AnimToFBX\\ViewWidgets\\SelectionListWidget_QT.py'.format(RS.Config.Script.Path.Root))
import RS.Tools.Animation.AnimToFBX.ViewWidgets.SelectionListWidget_QT as SelList
reload(SelList)

#Import model and Data Nodes
import RS.Tools.Animation.AnimToFBX.Data.AnimNodes as animNodes
import RS.Tools.Animation.AnimToFBX.Models.SelectionList as listModel
import RS.Tools.Animation.AnimToFBX.Models.SelectionSet as setsModel
reload(animNodes)
reload(listModel)
reload(setsModel)


class SelectionListWidget( RS.Tools.UI.QtMainWindowBase, SelList.Ui_Form  ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, None)
        
        # Required to create the ui from Qt Designer
        self.setupUi(self)
	
	self.ConnectSelModelToSelView()
	
	self._nodeDataMapper = QtGui.QDataWidgetMapper()	
	
	#setting the selectionlistview SelectionMode
	self.selectionListView.setSelectionMode(QtGui.QTreeView.ExtendedSelection)	
	
	#self.lLoadSelectionList.pressed.connect(self.lLoadSelection)
	
    def setModel(self, model):
	self._model = model
	self._nodeDataMapper.setModel(model)

	self._nodeDataMapper.addMapping(self.lItemName, 0)
	self._nodeDataMapper.addMapping(self.lItemPath, 2)	
	
    def setSelection(self, current, old):
	parent = current.parent()
	self._nodeDataMapper.setRootIndex(parent)
	
	self._nodeDataMapper.setCurrentModelIndex(current)
	
    def ConnectSelModelToSelView(self):

	self.nodeTree = animNodes.animNode("root")
	
	self._selModel = listModel.SelectionList(self.nodeTree)
	#self._selModel = setsModel.SelectionSet(self.nodeTree)

	self.selectionListView.setModel(self._selModel)	
	
