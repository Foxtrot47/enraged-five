"""
Widget for showing saved sets of .anim files
"""
import os
from xml.dom import minidom
from xml.etree import cElementTree as xml

from PySide import QtGui, QtCore

from RS import Perforce
from RS.Tools.UI import Run, QPrint
from RS.Tools.Animation.Anim2Fbx.Models.AnimData import AnimData
from RS.Tools.Animation.Anim2Fbx.Models.AnimDataModel import AnimDataModel


class SetsWidget(QtGui.QWidget):
    """
    Widget for displaying custom sets of anim files
    """

    selectionChangedSignal = QtCore.Signal(object)
    lastItemSelected = QtCore.Signal(object)

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget

        """
        super(SetsWidget, self).__init__(parent=parent)

        self.__currentSelection = tuple()

        layout = QtGui.QVBoxLayout()

        title = QtGui.QLabel("  Anim Sets")
        self.syncButton = QtGui.QPushButton("Sync")
        self.syncButton.setFixedWidth(40)
        self.syncButton.setToolTip("Grabs the latest Core.xml from P4 and reloads the set xmls")
        self.tree = QtGui.QTreeView()

        self.model = AnimDataModel()
        self.model.dataChanged.connect(self.saveView)
        self.tree.setModel(self.model)
        self.tree.setAcceptDrops(True)
        self.tree.setDragEnabled(True)
        self.tree.setDropIndicatorShown(True)
        self.tree.setDragDropOverwriteMode(False)
        self.tree.setSelectionMode(self.tree.ExtendedSelection)
        self.tree.setSelectionBehavior(self.tree.SelectRows)
        self.tree.setDragDropMode(self.tree.DragDrop)
        self.tree.selectionChanged = self.selectionChanged
        self.tree.setExpandsOnDoubleClick(False)
        self.tree.setColumnWidth(0, 200)
        self.tree.edit = self.edit
        self.tree.expanded.connect(self.expanded)

        titleLayout = QtGui.QHBoxLayout()
        titleLayout.addWidget(title)
        titleLayout.addWidget(self.syncButton)
        titleLayout.setContentsMargins(0, 0, 0, 0)
        titleLayout.setSpacing(0)

        layout.addLayout(titleLayout)
        layout.addWidget(self.tree)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        self.syncButton.clicked.connect(self.syncCore)
        self.populateView()
        self.setStyleSheet("QLabel {"
                           "font: bold 11px; "
                           "qproperty-alignment: AlignCenter;"
                           "}")
        self.setLayout(layout)

    def populateView(self):
        """ Populate the Tree View """
        root = AnimData("root")
        root.setNameIsEditable(True)
        toolDirectory = os.path.dirname(os.path.dirname(__file__))
        setsDirectory = os.path.join(toolDirectory, "Sets")
        setsXMLList = os.listdir(setsDirectory)
        if "Local.xml" not in setsXMLList:
            rootElement = xml.Element("Sets")
            rootElement.text = "Local"
            tree = xml.ElementTree(rootElement)
            tree.write(os.path.join(setsDirectory, "Local.xml"))

        for eachFile in os.listdir(setsDirectory):
            path = os.path.join(setsDirectory, eachFile)
            root.appendChild(self.parseSetXML(path))

        self.model.setRoot(root)

    def saveView(self):
        """ Saves the content of the TreeView into XMLs """
        toolDirectory = os.path.dirname(os.path.dirname(__file__))
        setsDirectory = os.path.join(toolDirectory, "Sets")
        for child in self.model.Root().children():
            path = os.path.join(setsDirectory, "{}.xml".format(child.name()))
            isWriteable = os.access(path, os.W_OK)
            if isWriteable:
                self.saveSetXML(path, child)

    def parseSetXML(self, path):
        """
        Reads XML to create a Model hierarchy to display in the Tree View

        Arguments:
            path (string): path to the xml file to read

        Return:
            AnimData
        """
        name, _ = os.path.splitext(os.path.basename(path))
        tree = xml.parse(path)
        return self.recursiveParse(tree.getroot())

    def recursiveParse(self, element, parentData=None, level=0):
        """
        Recursively converts xml.elementTree.ElementS into AnimData

        Arguments:
            element (xml.elementTree.Element): element to convert into AnimData
            parentData (AnimData): animData to add converted data as a child to
            level (int): recursion depth

        Return:
            AnimData
        """
        customName = element.attrib.get("name", None)

        animData = AnimData(element.text.strip() or element.tag)
        animData.setName(customName if customName != "None" else None)
        animData.setFrames([int(element.attrib.get("frame", 0))])
        animData.setNameIsEditable(element.tag in ('File', 'Folder'))

        if parentData:
            parentData.appendChild(animData)
        for child in element:
            self.recursiveParse(child, animData, level+1)
        return animData

    def saveSetXML(self, path, animData):
        """
        Saves the top folders of the tree view into xmls

        Arguments:
            path (string): path to save xml at
            animData (AnimData): AnimData content to save out
        """
        tree = xml.ElementTree(self.recursiveSave(animData))
        tree.write(path)
        xmlDoc = minidom.parse(path)
        with open(path, "wb") as fileHandle:
            fileHandle.write(xmlDoc.toprettyxml())

    def recursiveSave(self, animData, level=0):
        """
        Recursively stores the data from AnimData into elementTree Elements

        Arguments:
            animData (AnimData): AnimData whose data needs to be stored
            level (int): level of recursion

        Return:
            xml.elementTree.Element
        """
        if level:
            element = xml.Element(("Folder", "File")[animData.isFile()])
            element.attrib['name'] = animData.name()
            element.attrib['frame'] = str(animData.frames()[0] if animData.frames() else 0)
        else:
            element = xml.Element("Sets")

        element.text = animData.path()

        for child in animData.children():
            element.append(self.recursiveSave(animData=child, level=level+1))
        return element

    @property
    def doubleClickSignal(self):
        """ Double Click Signal """
        return self.tree.doubleClicked

    def getHierarchy(self, item):
        """
        Returns the hierarchy of the given item with the item as the first item in the list

        Arguments:
            item (AnimData): The item to get the hierarchy from

        """
        children = [item]
        for child in item.children():
            children.extend(self.getHierarchy(child))
        return children
    
    def selectedItems(self):
        """ The selected items from the Tree View """
        selectedItems = []
        foundItems = []
        for modelIndex in self.__currentSelection:
            item = self.model.itemFromIndex(modelIndex)
            foundItems.extend(self.getHierarchy(item))
        [selectedItems.append(item) for item in foundItems if item not in selectedItems and item.isFile()]
        return selectedItems

    def selectedFiles(self):
        """ The selected files from the Tree View """
        return [selectedItem.path() for selectedItem in self.selectedItems()]

    def selectedFrames(self):
        """ The selected frames from the Tree View """
        return [selectedItem.frames() for selectedItem in self.selectedItems()]

    def updateStateOfSelectedFiles(self):
        """ Updates the state of the files on the Tree View """
        [item.updateStateOfFile() for item in self.selectedItems()]

    def recursivePathExtraction(self, item):
        """
        Recursively gathers the file paths from the selected AnimData in the TreeView

        Arguments:
            item(AnimData): AnimData to get filepath from is the data represents a file

        Return:
            list[string, etc.]
        """
        paths = []
        path = item.path()
        if (path.lower().startswith("x:") or path.startswith("//")) and item.isFile():
            paths.append(item.path())
        for child in item.children():
            paths.extend(self.recursivePathExtraction(child))
        return paths

    def selectionChanged(self, selectedItems, _):
        """
        Overrides built-in method.
        When the selection changes store the new selection as the current selection and emit a signal

        Arguments:
            selectedItems (QtCore.QSelectedIndexes): the newly selected items

        """
        try:
            self.__currentSelection = []
            [self.__currentSelection.append(index) for index in self.tree.selectionModel().selectedIndexes()
             if not index.column()]
            self.__currentSelection = tuple(self.__currentSelection)
            self.selectionChangedSignal.emit(self.__class__)
            self.lastItemSelected.emit([index for index in selectedItems.indexes()][:1])

        except:
            pass

        self.refresh()

    def addFolder(self):
        """ Adds an anim to the model that represents a folder """
        if self.__currentSelection:
            name, result = QtGui.QInputDialog.getText(None, "Add Folder", "Folder Name")
            parentIndex = self.__currentSelection[-1]
            parentItem = self.model.itemFromIndex(parentIndex)

            if not result or not name:
                return
            elif parentItem.isFile():
                QtGui.QMessageBox.warning(None, "Warning", "Can not create folders under files")
                return

            item = AnimData(name)
            parentItem.appendChild(item)
            self.model.moveItems(parentIndex=parentIndex, items=[item], copy=False)

    def deleteSelection(self):
        """ Removes selection from the model """
        rootIndex = QtCore.QModelIndex()
        for index in self.__currentSelection:
            self.model.removeRows(index.row(), 1, index.parent())
        self.model.dataChanged.emit(rootIndex, rootIndex)

    def edit(self, index, trigger, event):
        """
        Overrides the built-in edit event of the Tree View.
        Adds support for shift key so editor mode is only triggered when the shift key is held.

        Arguments:
            index (QModelIndex): index of the item that is being edited
            trigger (QtGui.QAbstractItemView.Trigger): view trigger being called
            event (QtCore.QEvent): event calling this method

        Return:
            boolean
        """
        modifiers = QtGui.QApplication.keyboardModifiers()
        if trigger == QtGui.QAbstractItemView.DoubleClicked and modifiers != QtCore.Qt.ShiftModifier:
            return False
        return QtGui.QTreeView.edit(self.tree, index, trigger, event)

    def syncCore(self):
        """ Syncs the Core.xml from Perforce and updates the View """
        path = os.path.dirname(__file__)
        path = os.path.dirname(path)
        path = os.path.join(path, "Sets", "Core.xml")
        filestate = Perforce.GetFileState(path)
        if filestate.OpenAction != 0 and QtGui.QMessageBox.StandardButton.Yes == QtGui.QMessageBox.question(
                None, "Warning", "Synching will overwrite your current changes to "
                                 "the Core.xml and remove it from any pending "
                                 "changelists.\n"
                                 "\n"
                                 "Do you wish to continue?",
                                 QtGui.QMessageBox.Yes | QtGui.QMessageBox.No):

            Perforce.Revert(path)
        Perforce.Sync(path, force=True)
        self.model.clear()
        self.populateView()

    def refresh(self):
        """
        Resets the focus on the UI so that its draw calls are ran properly
        """
        if self.parent():
            self.parent().setFocus()
            self.setFocus()

    def expanded(self, index):
        """
        Updates the the state of the icon associated with the child items that are being expanded

        Arguments:
            index (QtGui.QModelIndex): index of the item being expanded

        """
        item = index.data(QtCore.Qt.UserRole)
        for child in item.children():
            child.updateStateOfFile()


@Run(title="SetsWidget")
def Run():
    return SetsWidget()
