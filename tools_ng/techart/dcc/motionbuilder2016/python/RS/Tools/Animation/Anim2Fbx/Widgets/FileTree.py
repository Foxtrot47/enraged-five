"""
Tree Widget for displaying files on the local machine and from Perforce

MOVE TO GENERIC PLACE
"""
import os

from PySide import QtGui, QtCore

from RS import Perforce, Config, ProjectData
from RS.Tools.Animation.Anim2Fbx.Models import FileFilterModel

PROGRESSBAR_COLOR, TEXT_COLOR = ProjectData.data.GetConfig("MenuColour", defaultValue=("#ffdc19", "black"))


class FileTreeWidget(QtGui.QWidget):
    """
    Tree widget for displaying files from perforce
    """

    selectionChangedSignal = QtCore.Signal(object)
    lastItemSelected = QtCore.Signal(object)

    def __init__(self, parent=None, title="Open Cutscene", directory=""):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
            title (string): title for the widget
            rootDirectory (string): root directory where the files that should be displayed live
        """
        super(FileTreeWidget, self).__init__(parent=parent)

        self._block = False
        self._disableProgressBar = False
        self._excludeFilters = []
        self._fileExtensions = []
        self._expandMinimum = 2
        self._showLocal = False
        self.files = {}
        self.model = QtGui.QStandardItemModel()
        self.filter = FileFilterModel.FileFilterModel()
        self.filter.setSourceModel(self.model)

        self.filterBox = QtGui.QLineEdit()
        self.filterBox.textChanged.connect(self.textChanged)
        self.filterBox.returnPressed.connect(lambda *args: self.textChanged(self.filterBox.text()))

        expandAllButton = QtGui.QPushButton("+")
        collapseAllButton = QtGui.QPushButton("-")

        self.Stack = QtGui.QStackedWidget()

        self.ProgressBar = QtGui.QProgressBar()
        self.ProgressBar.setOrientation(QtCore.Qt.Vertical)
        self.ProgressBar.setTextVisible(False)
        self.ProgressBar.setInvertedAppearance(True)

        self.tree = QtGui.QTreeView()
        self.tree.setModel(self.filter)
        self.tree.selectionChanged = self.selectionChanged

        self._currentSelection = tuple()
        self.rootDirectory = ""
        self.perforceDirectory = ""
        self.setDirectory(directory)

        self.setWindowTitle(title)

        expandAllButton.setFixedWidth(20)
        collapseAllButton.setFixedWidth(20)

        layout = QtGui.QVBoxLayout()
        filterLayout = QtGui.QHBoxLayout()

        filterLayout.addWidget(QtGui.QLabel(" Filter "))
        filterLayout.addWidget(self.filterBox)
        filterLayout.addWidget(expandAllButton)
        filterLayout.addWidget(collapseAllButton)

        self.Stack.addWidget(self.ProgressBar)
        self.Stack.addWidget(self.tree)

        layout.addLayout(filterLayout)
        layout.addWidget(self.Stack)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        self.tree.expanded.connect(self.expanded)
        self.tree.collapsed.connect(self.expanded)

        expandAllButton.clicked.connect(self.tree.expandAll)
        collapseAllButton.clicked.connect(self.tree.collapseAll)

        self.tree.setSelectionMode(self.tree.SingleSelection)
        self.tree.setMouseTracking(False)
        self.tree.setDragEnabled(False)
        self.tree.setAcceptDrops(False)
        self.setLayout(layout)
        self.setStyleSheet(
            "QProgressBar {"
            "    border: 2px solid grey;"
            "    border-radius: 5px;"
            "    text-align: center;"
            "}"
            "QProgressBar::chunk {"
            "    background-color: %s;"
            "    height: 10px;"
            "    margin: 0.5px;"
            "}" % PROGRESSBAR_COLOR
        )

    def PopulateView(self):
        """ Populates the tree view """
        self.model.clear()
        if not self._disableProgressBar:
            self.ProgressBar.reset()
            self.Stack.setCurrentIndex(0)

        Files = {self.rootDirectory.lower(): {"_QStandardItem": self.model},
                 self.perforceDirectory.lower(): {"_QStandardItem": self.model}}

        pathGenerator = [self.PerforceFiles, self.LocalFiles][self.ShowLocal]
        dirname = os.path.dirname

        paths = pathGenerator(self.rootDirectory)
        numberOfPaths = len(paths)
        self.ProgressBar.setMaximum(numberOfPaths-2)
        for index, path in enumerate(paths):

            item = self.BuildPathItem(path)
            Files[path.lower()] = {"_QStandardItem": item}

            directory = dirname(path)
            parentDictionary = Files.get(directory.lower(), None)
            while not parentDictionary:
                parentItem = self.BuildPathItem(directory)

                parentItem.appendRow(item)
                Files[directory.lower()] = {"_QStandardItem": parentItem}

                item = parentItem
                directory = dirname(directory)
                parentDictionary = Files.get(directory.lower(), None)
            if not self._disableProgressBar:
                self.ProgressBar.setValue(index)
            parentDictionary["_QStandardItem"].appendRow(item)

        self.files = Files
        self.model.setHeaderData(0, QtCore.Qt.Horizontal, self.windowTitle())
        self.Stack.setCurrentIndex(1)

    def LocalFiles(self, path):
        """
        List of local files in the local directory

        Arguments:
            path: (string): path to local directory with local files

        """
        join = os.path.join
        return [join(directory, file) for directory, folders, files in os.walk(path)
                for file in files if file.endswith(self._fileExtensions[0])]

    def PerforceFiles(self, path):
        """
        List of local files in the local directory

        Arguments:
            path: (string): path to local directory with local files

        """
        p4Path = os.path.join(path, "...{}".format(self._fileExtensions[0]))
        return [filepath['depotFile'] for filepath in Perforce.Run("files", ["-e", p4Path])]

    def BuildPathItem(self, path):
        """
        Creates the standard items for the tree view
        Arguments:
            path (string): path to the file
        """
        path = path.replace("\\", "/").replace("%40", "@")
        _, filename = os.path.split(path)

        item = QtGui.QStandardItem(filename)
        item.setEditable(False)
        item.setToolTip(path)

        isFile = os.path.splitext(filename.lower())[-1] in self._fileExtensions
        iconImage = ["folder.png", "media.png"][isFile]
        icon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "Watchstar", iconImage))
        item.setIcon(icon)

        return item

    @property
    def ShowLocal(self):
        """  Flag for showing local files """
        return self._showLocal

    @ShowLocal.setter
    def ShowLocal(self, value):
        """
        Set the show local value
        Arguments:
            value (boolean): value to set for show local property, non-bool values get converted to booleans

        """
        self._showLocal = bool(value)

    def Select(self, path):
        """
        Selects the file on the tree view with the given path

        Arguments:
            path (string): path of the file to select on the tree view

        """
        if self.rootDirectory in path:
            alternatePath = path.replace(self.rootDirectory, self.perforceDirectory).replace("\\", "/")
        else:
            alternatePath = path.replace(self.perforceDirectory, self.rootDirectory).replace("/", "\\")

        dictionary = self.files.get(path, {}) or self.files.get(alternatePath, {})
        standardItem = dictionary.get("_QStandardItem", None)

        if standardItem:
            modelIndex = self.model.indexFromItem(standardItem)
            filterModelIndex = self.filter.mapFromSource(modelIndex)
            selectionModel = self.tree.selectionModel()
            selectionModel.setCurrentIndex(filterModelIndex, selectionModel.Select)
            return filterModelIndex

    def textChanged(self, text):
        """
        Overrides built-in method.
        When the text in the text field is greater than two, expand the children of all the
        folders.

        Arguments:
            text (string): text from the text field

        """
        self.filter.setFilterFixedString(text)
        if len(text) > self._expandMinimum:
            self.tree.expandAll()
        else:
            self.tree.collapseAll()

    def selectionChanged(self, selectedItems, _):
        """
        Overrides built-in method.
        When the selection changes store the new selection as the current selection and emit a signal

        Arguments:
            selectedItems (QtCore.QSelectedIndexes): the newly selected items

        """
        try:
            self._currentSelection = self.tree.selectionModel().selectedIndexes()
            if self.parent():
                self.parent().setFocus()
                self.setFocus()
            self.selectionChangedSignal.emit(self.__class__)
            self.lastItemSelected.emit([index for index in selectedItems.indexes()][:1])

        except:
            pass

    def expanded(self, modelIndex):
        """
        When shift is selected, expand all the children in one go

        Arguments:
            modelIndex (QtGui.QModelIndex): the item that was just expanded

        """
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ShiftModifier and self.tree.isExpanded(modelIndex) and not self._block:
            self._block = True
            self.expandChildren(modelIndex, 0)
            self._block = False

    def expandChildren(self, modelIndex, depth=0):
        """
        Recursively expands/collapses the children of an item in the tree view

        Arguments:
            modelIndex (QtGui.QModelIndex): model index that corresponds to the item that should have its children
                                            expanded
        """

        if not modelIndex.isValid():
            return

        index = 0
        child = modelIndex.child(0, 0)
        while child.isValid():
            if not child.data() in self._excludeFilters:
                self.expandChildren(child, depth=depth + 1)

            index += 1
            child = modelIndex.child(index, 0)

        if self.tree.isExpanded(modelIndex) and depth:
            self.tree.collapse(modelIndex)

        elif not self.tree.isExpanded(modelIndex):
            self.tree.expand(modelIndex)

    def addFileExtension(self, extension):
        """
        Adds file extension to use filter files shown

        Arguments:
            extension (string): file extension

        """
        if extension not in self._excludeFilters:
            self._fileExtensions.append(extension)

    def removeFileExtension(self, extension):
        """
        Removes file extension used to filter files shown

        Arguments:
            extension (string): file extension

        """

        if extension in self._excludeFilters:
            self._fileExtensions.remove(extension)

    def addFilter(self, filter):
        """
        Adds word to use filter files shown

        Arguments:
            filter (string): word to search for in file paths

        """
        if filter not in self._excludeFilters:
            self._excludeFilters.append(filter)

    def removeFilter(self, filter):
        """
        Removes word used to filter files shown

        Arguments:
            filter (string): word to search for in file paths

        """
        if filter in self._excludeFilters:
            self._excludeFilters.remove(filter)

    def setDirectory(self, path):
        """
        Sets the directory to look for files to display

        Arguments:
            path (string): path to directory

        """
        if os.path.exists(path):
            self.rootDirectory = path
            self.perforceDirectory = Perforce.ConvertRecordSetToDictionary(
                Perforce.Run('dirs', [path])).get("dir", self.rootDirectory)

    def setProgressBar(self, status):
        """
        Sets if the progress bar should be shown when adding new folders updated

        Arguments:
            status (boolean): status to set the progress bar to; Enabled (True) or disabled (False)

        """
        self._disableProgressBar = status

    def setExpandMinimum(self, minimum):
        """
        Sets the minimum value to expand files by when using the filter text field

        Arguments:
            minimum (int): minimum number of characters to accept before showing the filtering results

        """
        if minimum >= 0:
            self._expandMinimum = minimum

    @property
    def selectedItems(self):
        """ Returns the last item that was selected """
        if self._currentSelection is not None:
            return self._currentSelection

    def setAutoFilter(self, status):
        """
        Sets whether the AutoFilter feature is on or off for the tool

        Arguments:
            status (boolean): whether the filter option is on or off
        """
        filler = lambda *args: None

        self.filterBox.textChanged.connect(filler)
        self.filterBox.textChanged.disconnect()
        if status:
            self.filterBox.textChanged.connect(self.textChanged)
