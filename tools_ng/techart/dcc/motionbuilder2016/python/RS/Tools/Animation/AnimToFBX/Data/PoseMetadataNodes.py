from PySide import QtCore, QtGui
#import RS.Config
#import RS.Tools.Resources.resources
#import os

class PoseMetadataNode(object):

    def __init__(self, 
                 name, 
                 parent =None, 
                 anim = None, 
                 frame = None, 
                 date = None, 
                 user = None):

        self._name = name
        self._children = []
        self._parent = parent
        
        self._anim = anim
        self._frame = frame
        self._date = date
        self._user = user       

        if parent is not None:
            parent.addChild(self)

    def typeInfo(self):
        return "PoseMetadataNode"

    def addChild(self, child):
        self._children.append(child)

    # getters	
    def name(self):
        return self._name
    
    def anim(self):
        return self._anim
    
    def frame(self):
        return self._frame
    
    def date(self):
        return self._date
    
    def user(self):
        return self._user

    def child(self, row):
        return self._children[row]

    def childCount(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)
    # setters	
    def setName(self, name):
        self._name = name
        
        
class LibraryCategoryNode(PoseMetadataNode):

    def __init__(self, 
                 name, 
                 parent =None, 
                 anim = None, 
                 frame = None, 
                 date = None, 
                 user = None):
        super(LibraryCategoryNode, self).__init__(name, parent, anim, frame, date, user)

    def typeInfo(self):
        return "Category"
    
class LibraryPoseNode(PoseMetadataNode):

    def __init__(self, 
                 name, 
                 parent =None, 
                 anim = None, 
                 frame = None, 
                 date = None, 
                 user = None):
        super(LibraryPoseNode, self).__init__(name, parent, anim, frame, date, user)

    def typeInfo(self):
        return "Pose" 