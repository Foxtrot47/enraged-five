import functools

from PySide import QtCore, QtGui

from RS.Tools.UI import Application
from RS.Tools.SceneSplitEditor import const


class BaseWidget(QtGui.QWidget):
    """Base Widget for map widgets

    Attributes:
        labelText (str): Label name
    """

    def __init__(self, labelText=None, parent=None, flags=None):
        """Initializes ui

        Args:
            labelText (str): Label name
            parent (QtGui.QWidget): Parent widget
            flags (QtCore.Qt.WindowFlags): Window flags
        """
        flags = flags if flags else QtCore.Qt.WindowFlags()
        super(BaseWidget, self).__init__(parent=parent, f=flags)

        self.labelText = labelText if labelText else ""

        self.setupUi()

    def setupUi(self):
        """Sets up ui, must be overridden"""
        raise NotImplementedError("Need to implement setupUi method")


class RemapWidget(BaseWidget):
    """Remap Widget that is a button, a spacerItem, and a label

    This widget is a child of the MapWidget that's used to when remapping.

    Signals:
        buttonPressed (str, QtGui.QWidget): Emits a labelText and this widget when button pressed

    Attributes:
        labelText (str): Label name
    """

    buttonPressed = QtCore.Signal(str, QtGui.QWidget)

    def setupUi(self):
        """Sets up ui"""
        # Creates main layout
        self.layout = QtGui.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)
        self.setLayout(self.layout)

        # add arrow left toggle button
        self.toggleBtn = QtGui.QPushButton()
        self.toggleBtn.setStyleSheet("background-color: rgb(70, 70, 70);")
        toolTip = "Remove mapping from this asset and add it back to unmapped assets"
        self.toggleBtn.setToolTip("<font color=white>{}</font>".format(toolTip))
        self.toggleBtn.setIcon(const.Icons.REMOVE)
        self.toggleBtn.setFixedSize(24, 24)
        self.toggleBtn.setIconSize(QtCore.QSize(20, 20))
        self.toggleBtn.clicked.connect(self.buttonPress)
        self.layout.addWidget(self.toggleBtn)

        # add spacer
        self.spacerItem = QtGui.QSpacerItem(10, 0, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.layout.addItem(self.spacerItem)

        # add label
        self.label = QtGui.QLabel(self.labelText)
        self.label.setAlignment(QtCore.Qt.AlignVCenter)
        self.layout.addWidget(self.label)

    def buttonPress(self):
        """Run when the toggleBtn is clicked and emits buttonPressed"""
        self.buttonPressed.emit(self.labelText, self)

    def setText(self, text):
        """Set the labelText attribute and QLabel's text

        Args:
            text (str): Text to set label to
        """
        self.labelText = text
        self.label.setText(text)


class MapWidget(BaseWidget):
    """Map Widget that is a label and a button. This widget represents an asset to map to.

    Signals:
        addedItem (str, QtGui.QWidget): Emits a labelText and widget of added asset
        removedItem (str, str): Emits a labelText removed and the source labelText it was removed from

    Attributes:
        labelText (str): Label name
    """

    addedItem = QtCore.Signal(str, QtGui.QWidget)
    removedItem = QtCore.Signal(str, str)

    def setupUi(self):
        """Sets up the ui"""
        self.remapWidget = None

        self.layout = QtGui.QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)
        self.setLayout(self.layout)

        self.labelLayout = QtGui.QHBoxLayout()
        self.labelLayout.setContentsMargins(0, 0, 0, 0)
        self.labelLayout.setSpacing(0)
        self.layout.addLayout(self.labelLayout)

        self.label = QtGui.QLabel(self.labelText)
        self.label.setAlignment(QtCore.Qt.AlignVCenter)
        self.labelLayout.addWidget(self.label)

        self.toggleBtn = QtGui.QPushButton()
        self.toggleBtn.setIcon(const.Icons.ADD)
        toolTip = "Map currently selected asset to this available asset"
        self.toggleBtn.setToolTip("<font color=white>{}</font>".format(toolTip))
        self.toggleBtn.setStyleSheet("background-color: rgb(70, 70, 70);")
        self.toggleBtn.setFixedSize(24, 24)
        self.toggleBtn.setIconSize(QtCore.QSize(20, 20))
        self.labelLayout.addWidget(self.toggleBtn)
        self.toggleBtn.clicked.connect(functools.partial(self.addedItem.emit, self.labelText, self))

        self.assetMapLayout = QtGui.QVBoxLayout()
        self.assetMapLayout.setContentsMargins(10, 5, 0, 0)
        self.assetMapLayout.setSpacing(0)
        self.layout.addLayout(self.assetMapLayout)

    def setText(self, text):
        """Set the labelText attribute and QLabel's text

        Args:
            text (str): Text to set label to
        """
        self.label.setText(text)

    def addMapping(self, assetName):
        """Adds a asset to MapWidget

        Args:
            assetName (str): Asset Name
        """
        # remove current mapping if it exists
        if self.remapWidget is not None:
            self.removeMapping(self.remapWidget.labelText, self.remapWidget)

        # make a new remap widget
        self.remapWidget = RemapWidget(assetName)
        self.remapWidget.buttonPressed.connect(self.removeMapping)
        self.assetMapLayout.addWidget(self.remapWidget)

    def removeMapping(self, assetName, widget):
        """Remove an asset mapping and it's widget

        Args:
            assetName (str): Asset Name
            widget (QtGui.QWidget): Widget to remove
        """
        try:
            widget.deleteLater()
        except RuntimeError:
            pass

        # set remap widget to None
        self.remapWidget = None
        # emit remove so AssetMismatchDialog will refresh source items
        self.removedItem.emit(self.labelText, assetName)


class AssetMismatchDialog(QtGui.QDialog):
    """Asset mismatch dialog

    This ui allows you to remap two lists of asset namespaces. It is modal meaning when run it blocks input to other
    visible windows until it's closed/accepted. Refer to to bottom of this script for example usage.
    The getMappings method or assetMappings attribute should be used as the result after the dialog is run

    Attributes:
        assetMappings (dict): Dictionary representing remapped items after dialog is run
    """

    def __init__(self, sourceAssets=None, destAssets=None, parent=None, flags=None):
        """Initializes ui

        Args:
            sourceAssets (list): Assets that aren't matching/mapped
            destAssets (list): Assets that are available to match
            labelText (str): Label name
            parent (QtGui.QWidget): Parent widget
            flags (QtCore.Qt.WindowFlags): Window flags
        """
        parent = parent if parent else Application.GetMainWindow()
        flags = flags if flags else QtCore.Qt.WindowFlags(QtCore.Qt.WindowCloseButtonHint)

        super(AssetMismatchDialog, self).__init__(parent=parent, f=flags)
        self.setWindowTitle("Asset Mismatch Dialog")
        self.resize(600, 400)
        self.setModal(True)  # blocks input to other visible windows
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)

        self.assetMappings = {}

        self._sourceAssets = sourceAssets if sourceAssets else []
        self._destAssets = destAssets if destAssets else []
        self.setStyleSheet("background-color: rgb(68, 68, 68);")

        self.setupUi()

        self.setMappings(self._sourceAssets, self._destAssets)

    def setupUi(self):
        """Sets up the ui"""
        self.layout = QtGui.QVBoxLayout()
        self.layout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(self.layout)

        self.assetWidgetsLayout = QtGui.QHBoxLayout()
        self.assetWidgetsLayout.setContentsMargins(0, 0, 0, 0)
        self.layout.addLayout(self.assetWidgetsLayout)

        self.sourceLayout = QtGui.QVBoxLayout()
        self.sourceLayout.setContentsMargins(0, 0, 0, 0)

        self.sourceLabel = QtGui.QLabel("Unmapped Assets")
        self.sourceLabel.setStyleSheet("color: rgb(228, 228, 228);")

        self.sourceLayout.addWidget(self.sourceLabel)
        self.sourceLayout.setSpacing(5)

        self.sourceWidget = QtGui.QListWidget()
        self.sourceWidget.setStyleSheet("background-color: rgb(77, 77, 77); padding-left: 5px;")
        self.sourceWidget.setFocusPolicy(QtCore.Qt.NoFocus)
        self.sourceLayout.addWidget(self.sourceWidget)

        self.destLayout = QtGui.QVBoxLayout()
        self.destLayout.setContentsMargins(0, 0, 0, 0)

        self.destLabel = QtGui.QLabel("Available Assets")
        self.destLabel.setStyleSheet("color: rgb(228, 228, 228);")
        self.destLayout.addWidget(self.destLabel)

        self.destWidget = QtGui.QScrollArea()
        self.destWidget.setStyleSheet("background-color: rgb(77, 77, 77);")
        self.destWidgetLayout = QtGui.QVBoxLayout(self.destWidget)
        self.destWidgetLayout.setContentsMargins(5, 5, 5, 5)
        self.destWidgetLayout.setSpacing(5)
        self.destLayout.addWidget(self.destWidget)

        self.assetWidgetsLayout.addLayout(self.sourceLayout)
        self.assetWidgetsLayout.addLayout(self.destLayout)

        self.buttonBox = QtGui.QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok)

        for button in self.buttonBox.buttons():
            button.setStyleSheet("background-color: rgb(100, 100, 100);")

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.layout.addWidget(self.buttonBox)

    def accept(self):
        """Overridden method when accepting dialog"""
        self.setResult(const.MismatchDialogResult.ACCEPTED)
        super(AssetMismatchDialog, self).accept()

    def reject(self):
        """Overridden method of reject. Runs also when closing the ui"""
        self.assetMappings = {}
        self.setResult(const.MismatchDialogResult.REJECTED)
        super(AssetMismatchDialog, self).reject()

    def getMappings(self):
        """Get remapped assets after dialog is run

        Returns:
            dict: Assets that were remapped from the dialog
        """
        return self.assetMappings

    def setMappings(self, sourceAssets, destAssets):
        """Set mappings of items

        Args:
            sourceAssets (list): Assets that aren't matching/mapped
            destAssets (list): Assets that are available to match
        """
        self._sourceAssets = sourceAssets
        self._destAssets = destAssets

        # clear widgets
        self.sourceWidget.clear()
        # clear children of dest layout
        [item.deleteLater() for item in self.destWidgetLayout.children()]

        # add assets to remap
        for srcAsset in self._sourceAssets:
            widgetItem = QtGui.QListWidgetItem()
            widgetItem.setText(srcAsset)
            widgetItem.setSizeHint(QtCore.QSize(QtGui.QSizePolicy.Preferred, 30))
            self.sourceWidget.addItem(widgetItem)

        # keep items sorted
        self.sourceWidget.sortItems()

        # add source assets
        for destAsset in self._destAssets:
            mapWidget = MapWidget(destAsset)
            mapWidget.addedItem.connect(self.setAssetMapping)
            mapWidget.removedItem.connect(self.removeAssetMapping)
            self.destWidgetLayout.addWidget(mapWidget)

        # stretch so items are top aligned
        self.destWidgetLayout.addStretch()

    def setAssetMapping(self, sourceAsset, widget):
        """Set mappings of items

        Args:
            sourceAsset (str): Asset to store mapping
            widget (QtGui.QWidget): Widget to remove source asset from
        """
        # get selection
        selectedAssets = self.sourceWidget.selectedItems()
        if not selectedAssets:
            return

        # only use first selection since only one asset should map to another
        destAsset = selectedAssets[0].text()
        # set mapping in dictionary
        self.assetMappings[sourceAsset] = destAsset
        # add mapping to widget
        widget.addMapping(destAsset)

        # Remove the item source since it's been mapped
        index = self.sourceWidget.indexFromItem(selectedAssets[0])
        item = self.sourceWidget.takeItem(index.row())
        del item
        # clear the selection
        self.sourceWidget.clearSelection()

    def removeAssetMapping(self, sourceAsset, removedAsset):
        """Removes and asset that's mapped

        Args:
            sourceAsset (str): Asset that has mapping
            removedAsset (str): Asset to remove
        """
        # delete asset mapping
        try:
            del self.assetMappings[sourceAsset]
        except KeyError:
            pass

        # search for removed asset in source widget add back if not in there
        if not self.sourceWidget.findItems(removedAsset, QtCore.Qt.MatchExactly):
            widgetItem = QtGui.QListWidgetItem()
            widgetItem.setText(removedAsset)
            widgetItem.setSizeHint(QtCore.QSize(QtGui.QSizePolicy.Preferred, 30))
            self.sourceWidget.addItem(widgetItem)
            # always sort
            self.sourceWidget.sortItems()


# Example of use
if __name__ == "__builtin__":

    # make dialog
    app = AssetMismatchDialog(
        sourceAssets=["ZZ_Female_172cm_Regular_Proxy_001"],
        destAssets=["ZZ_Female_172cm_Regular_Proxy_000"],
    )
    # run with exec and wait for result
    result = app.exec_()
    if result == const.MismatchDialogResult.ACCEPTED:
        # get mappings if result
        print(app.getMappings())
