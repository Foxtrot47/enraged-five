import stat
import functools
import subprocess
import traceback
import os

import pyfbsdk as mobu
from PySide import QtCore, QtGui

from RS import Globals, Perforce
from RS.Tools import UI
from RS.Utils.Scene import Time
from RS.Core.SceneSplitter import exceptions, workflow, validation, settings, core
from RS.Tools.SceneSplitEditor import const, modelItem, model, delegate
from RS.PyCoreQt.Widgets import timelineWidget
from RS.PyCoreQt.Models import trackModelItem


class SplitSceneLoggingUIContext(object):
    """
    Context manager that handles showing the ulog's message widget while active and also logs
    any exceptions that happen within the context
    """
    def __init__(self):
        self.ulog = core.SPLIT_SCENE_LOG

    def _ensureProgressWindowVisible(self, sender=None, event=None):
        """Refresh the progress widget after scene changes to ensure it is still visible"""
        self.ulog.ProgressWindowOutput = False
        self.ulog.ProgressWindowOutput = True

    def __enter__(self):
        """Called when the context manager's scope is entered"""
        self.ulog.ProgressWindowOutput = True

        Globals.Callbacks.OnFileOpenCompleted.Add(self._ensureProgressWindowVisible)
        Globals.Callbacks.OnFileSaveCompleted.Add(self._ensureProgressWindowVisible)
        
        return self.ulog

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Called when the context manager's scope is exited"""
        Globals.Callbacks.OnFileOpenCompleted.Remove(self._ensureProgressWindowVisible)
        Globals.Callbacks.OnFileSaveCompleted.Remove(self._ensureProgressWindowVisible)
        
        self.ulog.ProgressWindowOutput = False

        if exc_type is not None:
            self.ulog.LogError(str(exc_val), exc_type.__name__)
            self.ulog.LogError(''.join(traceback.format_exception(exc_type, exc_val, exc_tb)), exc_type.__name__)


def checkSceneSaved(func):
    @functools.wraps(func)
    def wrapper(self, *args, **kwargs):
        if not Globals.Application.FBXFileName:
            QtGui.QMessageBox.information(self, "Scene not saved",
                                          "The current scene must be saved before editing split settings.")
            return
        return func(self, *args, **kwargs)
    return wrapper


class SceneSplitTimeline(timelineWidget.Timeline):
    """Timeline with additonal settings button

    Signals:
        saveSettings: Emitted when save settings button is pressed
    """
    saveSettings = QtCore.Signal()

    def __init__(self, timelineRange, parent=None, defaultSetup=False):
        """Initializer override to add button

        Args:
            timelineRange (list of int): Start and end frame as a list
            parent (QtGui.QWidget): Parent of widget
            defaultSetup (bool): Whether or not to emit default callbacks on signals
        """
        super(SceneSplitTimeline, self).__init__(timelineRange, parent=parent, defaultSetup=defaultSetup)

        self.saveSettingsBtn = QtGui.QPushButton()
        self.saveSettingsBtn.setIcon(const.Icons.SAVE_SETTINGS)
        self.saveSettingsBtn.setToolTip("Save Scene Settings")
        self.saveSettingsBtn.setFixedSize(26, 26)
        self.saveSettingsBtn.setIconSize(QtCore.QSize(24, 24))
        self.saveSettingsBtn.clicked.connect(self.onSaveSettings)
        self._scene._topBar.buttonWidget.layout.addWidget(self.saveSettingsBtn)

    def onSaveSettings(self):
        """Emit on save settings"""
        self.saveSettings.emit()


class SplitSettingsView(QtGui.QTreeView):
    """TreeView for resource items to add to splits"""
    removeResources = QtCore.Signal()

    def __init__(self, *args, **kwargs):
        super(SplitSettingsView, self).__init__(*args, **kwargs)
        self.setSortingEnabled(True)
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.setDragEnabled(True)
        self.setDropIndicatorShown(True)
        self.viewport().setAcceptDrops(True)
        self.setAlternatingRowColors(True)

    def dropEvent(self, dropEvent):
        """Drop event for adding items back

        Args:
            dropEvent (QtCore.QDropEvent): Drop event
        """
        dropEvent.acceptProposedAction()
        self.removeResources.emit()


class SceneSplitEditorWidget(QtGui.QWidget):
    """
    The main widget for the Scene Split Editor tool
    """
    TOOL_TITLE = "Scene Splitter"
    TOOL_HELP_URL = r"https://hub.gametools.dev/display/ANIM/Motionbuilder+Scene+Splitter+Tool"
    PRE_SPLIT_PLOT_CHARACTERS_SETTING = "PreSplitPlotCharacters"
    PRE_MERGE_PLOT_CHARACTERS_SETTING = "PreMergePlotCharacters"
    PRE_SPLIT_PLOT_PROPS_SETTING = "PreSplitPlotProps"
    PRE_SPLIT_PLOT_VEHICLES_SETTING = "PreSplitVehiclesProps"
    PRE_MERGE_PLOT_PROPS_SETTING = "PreMergePlotProps"
    PRE_MERGE_PLOT_VEHICLES_SETTING = "PreMergePlotVehicles"
    PRESERVE_GS_SKEL_CHARACTERIZATION_SETTING = "PreserveGsSkelCharacterization"
    CREATE_POSES_VERTICAL_SPLIT_SETTING = "CreatePosesVerticalSplit"
    AUTO_SAVE_SCENE_SETTING = "AutoSaveSceneSettings"
    SYNC_TIMELINE_SETTING = "SyncTimeline"

    def __init__(self, parent=None):
        super(SceneSplitEditorWidget, self).__init__(parent)

        self.splitFoldersModel = None
        self.splitFolderComboBox = None
        self._lastSelectedFolderIndex = 0
        self._settingsLoadInProgress = False
        self.splitSettingsModel = None
        self.splitSettingsView = None
        self.cbPlotRigsBeforeSplit = None
        self.cbPlotRigsBeforeMerge = None
        self.cbPlotPropsBeforeSplit = None
        self.cbPlotVehiclesBeforeSplit = None
        self.cbPlotPropsBeforeMerge = None
        self.cbPlotVehiclesBeforeMerge = None
        self.cbPreserveGsSkelCharacterization = None
        self.cbCreatePosesVerticalSplit = None
        self.autoSaveSceneSetting = None
        self.timelineSyncSetting = None
        self._fileOpenInProgress = False

        self._activeWorkflow = workflow.CinematicPassesSplitWorkflow()
        self._sceneSplitSettings = settings.SceneSplitSettings.newFromFile(activeWorkflow=self._activeWorkflow)
        self._settings = QtCore.QSettings("RockstarSettings", self.TOOL_TITLE)
        self.setupUi()
        self._contextMenu = self._createContextMenu()
        self._registerCallbacks()

    def setupUi(self):
        """Creates and initialises the widgets that make up the UI"""
        # Container layout for the rest of the UI
        mainLayout = QtGui.QVBoxLayout()
        self.setLayout(mainLayout)
        mainLayout.setSpacing(0)
        mainLayout.setContentsMargins(0, 0, 0, 0)

        # Top row with controls for split folders
        splitFolderControlsLayout = QtGui.QHBoxLayout()
        
        self.splitFoldersModel = model.SplitFoldersModel(self._sceneSplitSettings)
        self.splitFoldersModel.requestSaveSettings.connect(self._onSplitSettingsSaveRequested)
        
        splitFoldersLabel = QtGui.QLabel("Active Split Folder")
        self.splitFolderComboBox = QtGui.QComboBox()
        self.splitFolderComboBox.setModel(self.splitFoldersModel)
        self.splitFolderComboBox.setCurrentIndex(self._sceneSplitSettings.activeSplitFolderIndex)
        self.splitFolderComboBox.currentIndexChanged[int].connect(self._onActiveSplitFolderIndexChanged)
        splitFolderControlsLayout.addWidget(splitFoldersLabel)
        splitFolderControlsLayout.addWidget(self.splitFolderComboBox)

        mainLayout.addLayout(splitFolderControlsLayout)

        splitFolderControlsLayout.setSpacing(5)
        splitFolderControlsLayout.setContentsMargins(5, 5, 5, 5)
        splitFolderControlsLayout.setStretch(1, 1)
        
        # Central layout for split settings editing widgets
        editSettingsSplitter = QtGui.QSplitter()
        editSettingsSplitter.setContentsMargins(25, 0, 0, 0)
        editSettingsSplitter.setOrientation(QtCore.Qt.Vertical)
        mainLayout.addWidget(editSettingsSplitter)

        # Create the split settings tree view
        self.splitSettingsView = SplitSettingsView()
        self.separatorItemDelegate = delegate.SeparatorItemDelegate()
        self.splitSettingsView.setItemDelegate(self.separatorItemDelegate)

        self.splitSettingsView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.splitSettingsView.customContextMenuRequested.connect(self._showContextMenu)

        self.splitSettingsModel = model.SceneSplitSettingsModel(self._sceneSplitSettings)
        self.splitSettingsView.setModel(self.splitSettingsModel)

        # delegate for frame range
        self.frameRangeDelegate = delegate.FrameRangeDelegate()
        self.splitSettingsView.setItemDelegateForColumn(
            const.SceneSplitColumns.FRAME_RANGE, self.frameRangeDelegate)

        # timeline
        self.timeline = SceneSplitTimeline(Time.getSceneFrameRange(), parent=self)
        self.timeline.trackAdded.connect(self._onCreateHorizontalSplitItem)
        self.timeline.trackRemoved.connect(self._onRemoveSelectedSplits)
        self.timeline.horizontalSplit.connect(self._onCreateHorizontalSplitItem)
        self.timeline.verticalSplit.connect(self._onCreateVerticalSplit)
        self.timeline.horizontalMerge.connect(functools.partial(self._onMergeSplit, False))
        self.timeline.verticalMerge.connect(functools.partial(self._onMergeSplit, True))
        self.timeline.saveSettings.connect(self._onSaveSceneSettings)
        self.timeline.frameChanged.connect(self._onFrameChanged)
        self._onTimelineChanged()

        self._refreshDelegates()
        self.splitSettingsView.expandAll()

        self.splitSettingsModel.rowsInserted.connect(self._onSplitModelRowsInserted)
        self.splitSettingsModel.rowsMoved.connect(self._onSplitModelRowsMoved)
        self.splitSettingsModel.modelRefreshed.connect(self._onSplitModelRefreshed)
        self.splitSettingsModel.requestSaveSettings.connect(self._onSplitSettingsSaveRequested)
        self.splitSettingsView.removeResources.connect(self._onRemoveSelectedSplits)
        self.splitSettingsModel.settingsAboutToBeLoaded.connect(self._onSettingsAboutToBeLoaded)
        self.splitSettingsModel.settingsLoaded.connect(self._onSettingsLoadComplete)

        editSettingsSplitter.addWidget(self.splitSettingsView)
        editSettingsSplitter.addWidget(self.timeline)
        editSettingsSplitter.setSizes([1, 2])
        editSettingsSplitter.setStretchFactor(1, 1)

        # Split scene options checkboxes
        self.cbPlotRigsBeforeSplit = QtGui.QCheckBox("Plot active control rigs before splitting")
        tTip = "Check the base scene for active control rigs and plot to skeleton before splitting."
        self.cbPlotRigsBeforeSplit.setToolTip(tTip)

        self.cbPlotPropsBeforeSplit = QtGui.QCheckBox("Plot props before splitting")
        tTip = "Plot all props in the base scene before splitting."
        self.cbPlotPropsBeforeSplit.setToolTip(tTip)

        self.cbPlotVehiclesBeforeSplit = QtGui.QCheckBox("Plot vehicles before splitting")
        tTip = "Plot all vehicles in the base scene before splitting."
        self.cbPlotVehiclesBeforeSplit.setToolTip(tTip)

        self.cbPreserveGsSkelCharacterization = QtGui.QCheckBox("Preserve characterization on GS skeletons")
        tTip = "If checked, characterization on GS skeletons will not be removed when the scene is split."
        self.cbPreserveGsSkelCharacterization.setToolTip(tTip)

        # Merge Scene options checkboxes
        self.cbPlotRigsBeforeMerge = QtGui.QCheckBox("Plot active control rigs before merging")
        tTip = "Check all split scenes for active control rigs and plot to skeleton before merging - this may be slow."
        self.cbPlotRigsBeforeMerge.setToolTip(tTip)
        
        self.cbPlotPropsBeforeMerge = QtGui.QCheckBox("Plot props before merging")
        tTip = "Plot all props in split scenes before merging - this may be slow."
        self.cbPlotPropsBeforeMerge.setToolTip(tTip)

        self.cbPlotVehiclesBeforeMerge = QtGui.QCheckBox("Plot vehicles before merging")
        tTip = "Plot all vehicles in split scenes before merging - this may be slow."
        self.cbPlotVehiclesBeforeMerge.setToolTip(tTip)

        self.cbCreatePosesVerticalSplit = QtGui.QCheckBox("Create poses for vertical split merges")
        tTip = ("Create pose controls for the merged in animation from a split scene. "
                "This is to fix pops that may occur when scenes are merged together.")
        self.cbCreatePosesVerticalSplit.setToolTip(tTip)

        # Create split/merge buttons
        splitScenesBtn = QtGui.QPushButton("Split Scenes")
        splitScenesBtn.clicked.connect(self._onSplitScenes)
        splitSelectedBtn = QtGui.QPushButton("Split Selected Scenes")
        splitSelectedBtn.clicked.connect(self._onSplitSelectedScenes)
        cameraUpdateBtn = QtGui.QPushButton("Update Cameras in Split Scenes")
        cameraUpdateBtn.clicked.connect(self._onCameraUpdate)
        mergeSelectedSplitsBtn = QtGui.QPushButton("Merge Selected Splits")
        mergeSelectedSplitsBtn.clicked.connect(self._onMergeSelectedSplits)
        mergeAllSplitsBtn = QtGui.QPushButton("Merge All Splits")
        mergeAllSplitsBtn.clicked.connect(self._onMergeSplits)

        # Merge and split options group boxes
        splitAndMergeOptionsLayout = QtGui.QHBoxLayout()

        splitOptionsGroupBox = QtGui.QGroupBox("Split")
        splitOptionsLayout = QtGui.QVBoxLayout()
        splitOptionsLayout.setSpacing(2)
        splitOptionsGroupBox.setLayout(splitOptionsLayout)

        mergeOptionsGroupBox = QtGui.QGroupBox("Merge")
        mergeOptionsLayout = QtGui.QVBoxLayout()
        mergeOptionsLayout.setSpacing(2)
        mergeOptionsGroupBox.setLayout(mergeOptionsLayout)

        splitAndMergeOptionsLayout.addWidget(splitOptionsGroupBox)
        splitAndMergeOptionsLayout.addWidget(mergeOptionsGroupBox)
        
        # Add split controls
        splitOptionsLayout.addWidget(self.cbPlotRigsBeforeSplit)
        splitOptionsLayout.addWidget(self.cbPlotPropsBeforeSplit)
        splitOptionsLayout.addWidget(self.cbPlotVehiclesBeforeSplit)
        splitOptionsLayout.addWidget(self.cbPreserveGsSkelCharacterization)
        splitOptionsLayout.addWidget(splitScenesBtn)
        splitOptionsLayout.addWidget(splitSelectedBtn)
        splitOptionsLayout.addWidget(cameraUpdateBtn)

        # Add merge controls
        mergeOptionsLayout.addWidget(self.cbPlotRigsBeforeMerge)
        mergeOptionsLayout.addWidget(self.cbPlotPropsBeforeMerge)
        mergeOptionsLayout.addWidget(self.cbPlotVehiclesBeforeMerge)
        mergeOptionsLayout.addWidget(self.cbCreatePosesVerticalSplit)
        mergeOptionsLayout.addWidget(mergeSelectedSplitsBtn)
        mergeOptionsLayout.addWidget(mergeAllSplitsBtn)

        mainLayout.addLayout(splitAndMergeOptionsLayout)

        # Reload checkbox states from settings
        self.cbPlotRigsBeforeSplit.setChecked(bool(self._settings.value(self.PRE_SPLIT_PLOT_CHARACTERS_SETTING,
                                                                        defaultValue=False)))
        self.cbPlotRigsBeforeMerge.setChecked(bool(self._settings.value(self.PRE_MERGE_PLOT_CHARACTERS_SETTING,
                                                                        defaultValue=False)))
        self.cbPlotPropsBeforeSplit.setChecked(bool(self._settings.value(self.PRE_SPLIT_PLOT_PROPS_SETTING,
                                                                         defaultValue=False)))
        self.cbPlotVehiclesBeforeSplit.setChecked(bool(self._settings.value(self.PRE_SPLIT_PLOT_VEHICLES_SETTING,
                                                                         defaultValue=False)))
        self.cbPlotPropsBeforeMerge.setChecked(bool(self._settings.value(self.PRE_MERGE_PLOT_PROPS_SETTING,
                                                                         defaultValue=False)))
        self.cbPlotVehiclesBeforeMerge.setChecked(bool(self._settings.value(self.PRE_MERGE_PLOT_VEHICLES_SETTING,
                                                                            defaultValue=False)))
        self.cbPreserveGsSkelCharacterization.setChecked(
            bool(self._settings.value(self.PRESERVE_GS_SKEL_CHARACTERIZATION_SETTING, defaultValue=False))
        )
        self.cbCreatePosesVerticalSplit.setChecked(
            bool(self._settings.value(self.CREATE_POSES_VERTICAL_SPLIT_SETTING, defaultValue=False))
        )

        # Configure tree view column widths
        splitSettingsHeader = self.splitSettingsView.header()
        splitSettingsHeader.setResizeMode(QtGui.QHeaderView.Stretch)
        splitSettingsHeader.setResizeMode(const.SceneSplitColumns.TYPE,
                                          QtGui.QHeaderView.ResizeToContents)
        splitSettingsHeader.setResizeMode(const.SceneSplitColumns.FRAME_RANGE,
                                          QtGui.QHeaderView.ResizeToContents)
        splitSettingsHeader.setResizeMode(const.SceneSplitColumns.BUG_STATUS,
                                          QtGui.QHeaderView.ResizeToContents)
        splitSettingsHeader.setResizeMode(const.SceneSplitColumns.BUG_ID,
                                          QtGui.QHeaderView.ResizeToContents)
        splitSettingsHeader.setStretchLastSection(False)

        self._onRefresh()

    def createMenu(self):
        """
        Creates the menu bar for the tool window. Called by the UI.Run decorator.
        
        Returns:
            QtGui.QMenuBar: The menu bar for the tool window.
        """
        menuBar = QtGui.QMenuBar(self)
        fileMenu = QtGui.QMenu("File")
        fileMenu.addAction("View Log...", self._onShowLogViewer)
        fileMenu.addAction("P4 Sync Settings", self._onSyncSplitSettings)
        # auto save
        autoSaveAction = QtGui.QAction("Auto Save Scene Settings...", self)
        autoSaveAction.setCheckable(True)
        autoSaveAction.setChecked(bool(self._settings.value(self.AUTO_SAVE_SCENE_SETTING, defaultValue=True)))
        autoSaveAction.toggled.connect(self._onAutoSaveSceneSettings)
        fileMenu.addAction(autoSaveAction)

        # timeline sync
        syncTimelineAction = QtGui.QAction("Sync Timeline", self)
        syncTimelineAction.setCheckable(True)
        syncTimelineAction.setChecked(bool(self._settings.value(self.SYNC_TIMELINE_SETTING, defaultValue=False)))
        syncTimelineAction.toggled.connect(self._onTimelineSyncSetting)
        fileMenu.addAction(syncTimelineAction)

        fileMenu.addAction("Show Settings File In Explorer...", self._onSettingsFileInExplorer)
        menuBar.addMenu(fileMenu)

        splitFoldersMenu = QtGui.QMenu("Folders")
        splitFoldersMenu.addAction("Add Split Folder", self._onAddSplitFolder)
        splitFoldersMenu.addAction("Remove Split Folder", self._onRemoveSplitFolder)
        menuBar.addMenu(splitFoldersMenu)

        return menuBar
    
    def _createContextMenu(self):
        """
        Creates the right-click context menu used by the split settings tree view.
        
        Returns:
            QtGui.QMenu: The context menu
        """
        menu = QtGui.QMenu()
        menu.addAction("Add New Split Scene", self._onCreateHorizontalSplitItem)
        menu.addAction("Remove Selected Split Scenes", self._onRemoveSelectedSplits)
        menu.addSeparator()
        menu.addAction("Refresh", self._onRefresh)
        return menu

    def autoSaveSetting(self):
        """Gets the autosave setting value

        Returns:
            bool: Auto save scene setting
        """
        if self.autoSaveSceneSetting is None:
            autoSaveSetting = bool(self._settings.value(self.AUTO_SAVE_SCENE_SETTING, defaultValue=True))
        else:
            autoSaveSetting = self.autoSaveSceneSetting

        return autoSaveSetting

    def syncTimelineSetting(self):
        """Gets the syncTimeline setting value

        Returns:
            bool: Sync Timeline setting
        """
        if self.timelineSyncSetting is None:
            syncTimelineSetting = bool(self._settings.value(self.SYNC_TIMELINE_SETTING, defaultValue=False))
        else:
            syncTimelineSetting = self.timelineSyncSetting

        return syncTimelineSetting

    @QtCore.Slot(object)
    def _onSplitSettingsSaveRequested(self, splitSettings):
        """
        Called when settings data has changed requiring it to be saved to disk. This is triggered by signals from
        the models that allow editing of settings data.
        """
        if not isinstance(splitSettings, settings.SceneSplitSettings):
            raise TypeError("splitSettings parameter must be of type SceneSplitSettings")

        if not self.autoSaveSetting():
            return

        settingsFilePath = splitSettings.getSplitSettingsFilePath()
        if not settingsFilePath:
            return

        if os.path.isfile(settingsFilePath) and not os.access(settingsFilePath, os.W_OK):
            # Warn user that the settings file is not writeable - ask to make writable
            buttons = QtGui.QMessageBox.Yes | QtGui.QMessageBox.Cancel
            msg = ("Scene split settings file is not writable.\n\n"
                   "File:\n    - {}\n\n"
                   "Do you want to clear the read-only flag?").format(settingsFilePath)
            dialogResult = QtGui.QMessageBox.warning(self, "Settings Not Writable", msg, buttons)
    
            if dialogResult == QtGui.QMessageBox.Yes:
                os.chmod(settingsFilePath, stat.S_IWRITE)
            else:
                # Save aborted, reload unmodified settings
                self._onRefresh()
                return

        try:
            splitSettings.writeToFile()
        except exceptions.SceneSplitSettingsIOError as ex:
            # Warn user that the settings failed to save
            msg = "Failed to save settings.\n\nError:\n    - {}\n\nFile:    - {}".format(str(ex), settingsFilePath)
            QtGui.QMessageBox.critical(self, "Scene Split Settings", msg)

            # Reload settings
            self._onRefresh()

    @QtCore.Slot(int)
    def _onActiveSplitFolderIndexChanged(self, newFolderIndex):
        """Slot called when the folder selection combobox is changed"""
        if self._settingsLoadInProgress:
            # Ignore updates to the combo box index during settings load
            return

        self._sceneSplitSettings.activeSplitFolderIndex = newFolderIndex
        self.splitSettingsModel.refreshModel()

    def _onSettingsAboutToBeLoaded(self):
        """Slot called just before settings data is loaded from disk"""
        self._settingsLoadInProgress = True

    def _onSettingsLoadComplete(self):
        """Slot called once settings have finished being loaded from disk"""
        self.splitFolderComboBox.setCurrentIndex(self._sceneSplitSettings.activeSplitFolderIndex)
        self._settingsLoadInProgress = False

    @checkSceneSaved
    def _onAddSplitFolder(self):
        """Slot called when the user wants to add a new split folder"""
        nextIndex = self._sceneSplitSettings.splitFolderCount
        self.splitFoldersModel.addFolder()
        self.splitFolderComboBox.setCurrentIndex(nextIndex)
        self._sceneSplitSettings.activeSplitFolderIndex = nextIndex
        self.splitSettingsModel.refreshModel()

    @checkSceneSaved
    def _onRemoveSplitFolder(self):
        """Slot called when the user wants to remove a split folder"""
        folderCount = self._sceneSplitSettings.splitFolderCount
        if folderCount == 1:
            QtGui.QMessageBox.critical(self, "Remove Split Folder", "Removing the last split folder is not allowed")
            return

        indexToRemove = folderCount - 1
        
        # Dialog to check the name of the folder to be removed is what the user expects
        #  (may not be obvious that it removes the last one from the list as it may not be visible in the combobox)
        folderNameToRemove = self._sceneSplitSettings.getSplitFolderNameByIndex(indexToRemove)
        buttons = QtGui.QMessageBox.Yes | QtGui.QMessageBox.Cancel
        msg = ("Permanently delete all split settings for folder \"{}\"?").format(folderNameToRemove)
        dialogResult = QtGui.QMessageBox.warning(self, "Delete Split Folder Settings", msg, buttons)
        if dialogResult != QtGui.QMessageBox.Yes:
            return
        
        activeIndex = self._sceneSplitSettings.activeSplitFolderIndex
        if indexToRemove == activeIndex:
            self._sceneSplitSettings.activeSplitFolderIndex = activeIndex - 1
            self.splitSettingsModel.refreshModel()

        self.splitFoldersModel.removeFolder(indexToRemove)
    
    def _onSyncSplitSettings(self):
        """Slot called when the user asks to sync the split settings file in Perforce"""
        settingsFilePath = self.splitSettingsModel.getSettingsFilePath()
        if not settingsFilePath:
            return

        msg = "Sync split settings from Perforce?\n\nAny local changes will be lost."
        buttons = QtGui.QMessageBox.Yes | QtGui.QMessageBox.Cancel
        dialogResult = QtGui.QMessageBox.warning(self, "Sync Split Settings", msg, buttons)
        if dialogResult != QtGui.QMessageBox.Yes:
            return

        try:
            Perforce.Sync(settingsFilePath, force=True)
        except Perforce.P4Exception as ex:
            msg = "Failed to sync settings file: {}".format(ex)
            QtGui.QMessageBox.critical(self, "Sync Split Settings", msg)
            return
        
        # Reload from disk
        self._onRefresh()

    def _onSettingsFileInExplorer(self):
        """
        Shows the split settings file in an explorer window if it exists.
        """
        settingsFilePath = self.splitSettingsModel.getSettingsFilePath()
        if not settingsFilePath or not os.path.isfile(settingsFilePath):
            QtGui.QMessageBox.information(self, "Split Settings", "No split settings file exists for the current scene")
            return

        explorerPath = os.path.join(os.getenv("WINDIR"), "explorer.exe")
        settingsFilePath = os.path.normpath(settingsFilePath)
        args = "{} /select,\"{}\"".format(explorerPath, settingsFilePath)
        subprocess.Popen(args)

    def _onSaveSceneSettings(self):
        """Save scene settings callback"""
        splitSettings = self.splitSettingsModel.getSettings()

        settingsFilePath = splitSettings.getSplitSettingsFilePath()
        if not settingsFilePath:
            return

        try:
            splitSettings.writeToFile()
        except exceptions.SceneSplitSettingsIOError as ex:
            # Warn user that the settings failed to save
            msg = "Failed to save settings.\n\nError:\n    - {}\n\nFile:    - {}".format(str(ex), settingsFilePath)
            QtGui.QMessageBox.critical(self, "Scene Split Settings", msg)

            # Reload settings
            self._onRefresh()

    def _onAutoSaveSceneSettings(self, autoSave):
        """Setting autoSaveSceneSetting variable on callback

        Args:
            autoSave (bool): Whether to save settings automatically or not
        """
        self.autoSaveSceneSetting = autoSave

    def _onTimelineSyncSetting(self, sync):
        """Setting autoSaveSceneSetting variable on callback

        Args:
            sync (bool): Whether to sync timeline or not
        """
        self.timelineSyncSetting = sync

    def _onRefresh(self):
        """Reload settings from file and update resource items from the current scene"""
        if self.autoSaveSetting():
            self.splitSettingsModel.loadSettings()

        self.timeline.setTimeline(self.getTimelineData(), model=self.splitSettingsModel)

    def getTimelineData(self):
        """Gathers track data into a timelineData object

        Returns:
            trackModelItem.TimelineData: Timeline data from scene settings
        """
        data = self.splitSettingsModel.getSettings()
        fileName = os.path.basename(os.path.splitext(Globals.Application.FBXFileName)[0])
        tracks = []
        for row, splitSection in enumerate(data.splitSections):
            tracks.append(
                trackModelItem.TrackDataItem(
                    splitSection.splitFrameRange,
                    data.splitFrameRange,
                    trackTitle="_".join([fileName, splitSection.splitSuffix]),
                    trackTitleChildren=splitSection.resourceNamespaces,
                    modelIndex=self.splitSettingsModel.getModelIndexByRow(row, columnIndex=1),
                )
            )

        return trackModelItem.TimelineDataItem(tracks=tracks)

    def _showContextMenu(self, position):
        """Called when the user right clicks on the settings tree view. Displays the context menu"""
        self._contextMenu.exec_(self.splitSettingsView.viewport().mapToGlobal(position))

    def _onSplitModelRowsInserted(self, parentModelIdx, startRow, endRow):
        """Auto-expand items when new rows are inserted"""
        self.splitSettingsView.expand(parentModelIdx)

    def _onSplitModelRowsMoved(self, sourceParentModelIdx, sourceStartRow, sourceEndRow,
                               destinationParentModelIdx, destinationRow):
        """Auto-expand the target when items are moved into it"""
        self.splitSettingsView.expand(destinationParentModelIdx)

    def _onSplitModelRefreshed(self):
        """Expand all items in the view when it is reloaded"""
        self.splitSettingsView.expandAll()
        self.splitSettingsView.setItemDelegateForColumn(
           const.SceneSplitColumns.FRAME_RANGE, self.frameRangeDelegate)
        self._refreshDelegates()
        self.timeline.setTimeline(self.getTimelineData(), model=self.splitSettingsModel)

    def _refreshDelegates(self):
        """Set item delegates to open persistent editor

        Notes:
            In the PySide2 2.0.0.0 Alpha there isn't the isPersistentEditorOpen method
            so we just close and reopen instead
        """
        for row, childItem in enumerate(self.splitSettingsModel.masterSplitItem.childItems):
            modelIndex = self.splitSettingsModel.createIndex(
                row, const.SceneSplitColumns.FRAME_RANGE, childItem)
            self.splitSettingsView.closePersistentEditor(modelIndex)
            self.splitSettingsView.openPersistentEditor(modelIndex)

    def _checkValidationResult(self, validationResult):
        """
        Helper method that displays validation results to the user and asks if they want to continue where relevant.
        
        Args:
            validationResult (validation.ValidationResults): The validation results object to check.

        Returns:
            bool: True if the operation being validated should continue or False if not
        """
        shouldContinue = True
        if validationResult.hasErrors():
            # Show validation error report dialog
            errorsMsg = "\n    - ".join(validationResult.errors())
            errorsMsg = "Errors:\n    - {}\n\n".format(errorsMsg)
            warningsMsg = ""
            if validationResult.hasWarnings():
                warningsMsg = "\n    - ".join(validationResult.warnings())
                warningsMsg = "Warnings:\n    - {}\n\n".format(warningsMsg)
            msg = "{}{}Please fix the errors above and try again.".format(warningsMsg, errorsMsg)
            QtGui.QMessageBox.critical(self, "Scene Split Validation Error", msg)
            shouldContinue = False
        elif validationResult.hasWarnings():
            # Show validation warning dialog and ask if user wants to continue
            buttons = QtGui.QMessageBox.Yes | QtGui.QMessageBox.No
            warningsMsg = "\n    - ".join(validationResult.warnings())
            msg = "Warnings:\n    - {}\n\nDo you want to continue?".format(warningsMsg)
            dialogResult = QtGui.QMessageBox.warning(self, "Scene Split Validation Warning", msg, buttons)
            shouldContinue = dialogResult == QtGui.QMessageBox.Yes
        
        return shouldContinue
    
    def _showErrorDialog(self, title, message):
        """
        Displays a message box with a button to allow the user to open the ulog for more detail
        
        Args:
            title (str): The title for the message box
            message (str): The message for the body of the message box
        """
        buttons = QtGui.QMessageBox.Yes | QtGui.QMessageBox.Ignore
        msg = "{}\n\nView log?".format(message)
        dialogResult = QtGui.QMessageBox.critical(self, title, msg, buttons)
        if dialogResult == QtGui.QMessageBox.Yes:
            self._onShowLogViewer()

    def _onCameraUpdate(self):
        """Updates all cameras data in split scenes based on this fbx including story tracks"""
        # Ensure current settings are saved to disk
        self.splitSettingsModel.saveSettings()

        sceneSplitSettings = self.splitSettingsModel.getSettings()

        try:
            # Run the scene split
            core.updateCameras(sceneSplitSettings,
                               ulog=None)
        except Exception as ex:
            self._showErrorDialog("Camera Update Error", "Error while updating scene:\n\n    - {}".format(ex))

            if not isinstance(ex, exceptions.SceneSplitUserError):
                # Don't report setup errors via email/bugstar as they should be fixable by the user
                raise
            return

        QtGui.QMessageBox.information(self, "Camera Update", "Cameras, tracks, and clips updated successfully.")

    def _onSplitSelectedScenes(self):
        """Split animation from the split scenes that are selected in the tree view"""
        selectedIndexes = self.timeline.getSelection()
        # Items are row-based so filter out additional column indexes to avoid processing the same item twice
        modelIndexList = [idx for idx in selectedIndexes if idx.isValid() and idx.column() == 0]
        # Sort by row order
        modelIndexList.sort(key=lambda modelIdx: modelIdx.row())
        selectedSplitSectionSettings = []
        for modelIndex in modelIndexList:
            sceneSplitItem = modelIndex.internalPointer()
            if not isinstance(sceneSplitItem, modelItem.SplitSceneItem):
                # Ignore any selected items that are not scene splits
                # (e.g. base scene item and resource items are not relevant to merges)
                continue
            selectedSplitSectionSettings.append(sceneSplitItem.getSectionSettings())
        self._onSplitScenes(selectedSplitSections=selectedSplitSectionSettings)

    def _onSplitScenes(self, selectedSplitSections=None):
        """
        Performs validation and displays issues to the user. Then, if validation passes, creates the "split" scenes
        from the base fbx and performs relevant markup and optimisation operations on them.
        """
        # Ensure current settings are saved to disk
        self.splitSettingsModel.saveSettings()

        sceneSplitSettings = self.splitSettingsModel.getSettings()

        # Validate scene split settings and report errors
        validationResult = validation.ValidationResults()
        validationResult = validation.preSplitValidation(sceneSplitSettings, validationResult)
        if not self._checkValidationResult(validationResult):
            # Abort - validation unsuccessful
            return

        doPreSplitCharacterPlot = self.cbPlotRigsBeforeSplit.isChecked()
        doPreSplitPropPlot = self.cbPlotPropsBeforeSplit.isChecked()
        doPreSplitVehiclePlot = self.cbPlotVehiclesBeforeSplit.isChecked()
        preserveGsSkelCharacterization = self.cbPreserveGsSkelCharacterization.isChecked()

        try:
            # Run the scene split
            core.splitScene(sceneSplitSettings,
                            selectedSplitSections=selectedSplitSections,
                            ulog=None,
                            plotBaseSceneActiveCharacterRigs=doPreSplitCharacterPlot,
                            plotBaseSceneProps=doPreSplitPropPlot,
                            plotBaseSceneVehicles=doPreSplitVehiclePlot,
                            preserveGsSkelCharacterization=preserveGsSkelCharacterization)
        except Exception as ex:
            self._showErrorDialog("Split Scene Error", "Error while splitting scene:\n\n    - {}".format(ex))

            if not isinstance(ex, exceptions.SceneSplitUserError):
                # Don't report setup errors via email/bugstar as they should be fixable by the user
                raise
            return

        QtGui.QMessageBox.information(self, "Scene Split", "Split scene completed successfully")

    def _onMergeSplits(self, selectedSplitSections=None):
        """
        Performs validation and displays issues to the user. Then, if validation passes, merges
        animation from split scenes.
        
        Keyword Args:
            selectedSplitSections (list of settings.SceneSplitSectionSettings): The split sections that
                should be merged. If not specified, all splits will be merged.
        """
        
        # Ensure current settings are saved to disk
        self.splitSettingsModel.saveSettings()
        sceneSplitSettings = self.splitSettingsModel.getSettings()

        # Run scene merge
        doPreMergeCharacterPlot = self.cbPlotRigsBeforeMerge.isChecked()
        doPreMergePropPlot = self.cbPlotPropsBeforeMerge.isChecked()
        doPreMergeVehiclePlot = self.cbPlotVehiclesBeforeMerge.isChecked()
        doCreatePosesVerticalSplit = self.cbCreatePosesVerticalSplit.isChecked()

        try:
            core.mergeSplits(sceneSplitSettings,
                             selectedSplitSections=selectedSplitSections,
                             ulog=None,
                             prePlotAnyActiveControlRigs=doPreMergeCharacterPlot,
                             prePlotProps=doPreMergePropPlot,
                             prePlotVehicles=doPreMergeVehiclePlot,
                             createPosesVerticalSplit=doCreatePosesVerticalSplit)
            # make sure to reload ui incase of changes
            self._onRefresh()

        except Exception as ex:
            self._showErrorDialog("Merge Splits Error", "Error while merging splits:\n\n    - {}".format(ex))

            if not isinstance(ex, exceptions.SceneSplitUserError):
                # Don't report setup errors via email/bugstar as they should be fixable by the user
                raise
            return

        QtGui.QMessageBox.information(self, "Merge Splits", "Merge splits completed successfully")

    def _onMergeSelectedSplits(self):
        """Merge animation from the split scenes that are selected in the tree view"""
        selectedIndexes = self.timeline.getSelection()

        # Items are row-based so filter out additional column indexes to avoid processing the same item twice
        modelIndexList = [idx for idx in selectedIndexes if idx.isValid() and idx.column() == 0]
        # Sort by row order
        modelIndexList.sort(key=lambda modelIdx: modelIdx.row())

        selectedSplitSectionSettings = []
        for modelIndex in modelIndexList:
            sceneSplitItem = modelIndex.internalPointer()
            if not isinstance(sceneSplitItem, modelItem.SplitSceneItem):
                # Ignore any selected items that are not scene splits
                # (e.g. base scene item and resource items are not relevant to merges)
                continue
            selectedSplitSectionSettings.append(sceneSplitItem.getSectionSettings())

        self._onMergeSplits(selectedSplitSections=selectedSplitSectionSettings)

    @staticmethod
    def _onShowLogViewer():
        """Display the Scene Split ulog in the log viewer"""
        core.SPLIT_SCENE_LOG.Flush()
        core.SPLIT_SCENE_LOG.Show(modal=False)

    @checkSceneSaved
    def _onCreateHorizontalSplitItem(self, namespaces=None):
        """Adds a new split item to the split settings tree"""
        self.splitSettingsModel.createNewSceneSplit(namespaces=namespaces)
        self._onRefresh()

    @checkSceneSaved
    def _onCreateVerticalSplit(self):
        """Adds a new split item to the split settings tree"""
        selectedIndexes = self.timeline.getSelection()

        modelIndexes = []
        for modelIndex in selectedIndexes:
            if modelIndex.isValid() and modelIndex.column() == 0:
                modelIndexes.append(modelIndex)

        modelIndexes.sort(key=lambda modelIndex: modelIndex.row())

        splitFrame = self.timeline.currentFrame()
        for modelIndex in modelIndexes:
            self.splitSettingsModel.createNewVerticalSplit(modelIndex, splitFrame)

        self._onRefresh()

    def _onMergeSplit(self, verticalMerge=False):
        """Merges two or more split items

        Args:
            verticalMerge (bool): If True will only merge vertical splits
        """
        selectedIndexes = self.timeline.getSelection()
        self.splitSettingsModel.mergeSplits(selectedIndexes, verticalMerge=verticalMerge)
        self._onRefresh()

    @checkSceneSaved
    def _onRemoveSelectedSplits(self, trackTitle=None, namespaces=None):
        """Deletes the selected split items from the split settings tree"""
        if trackTitle and namespaces:
            selectedIndexes = self.timeline.getItemsByNamespaces(trackTitle, namespaces)
        else:
            selectedIndexes = self.timeline.getSelection()

        if not selectedIndexes:
            QtGui.QMessageBox.information(self,
                                          "Nothing Selected",
                                          "Please select at least one Split FBX item from the list.")
            return

        self.splitSettingsModel.removeSceneSplits(selectedIndexes)
        self._onRefresh()

    def showEvent(self, event):
        """Called when the widget is shown"""
        super(SceneSplitEditorWidget, self).showEvent(event)
        self.timeline.frameAll()

    def closeEvent(self, event):
        """
        Called when the wrapping window widget is closed
        
        Args:
            event (QtCore.QCloseEvent): The Qt close event
        """
        # Remove callbacks
        self._removeCallbacks()
        
        # Save settings
        if self.cbPlotRigsBeforeSplit is not None:
            self._settings.setValue(self.PRE_SPLIT_PLOT_CHARACTERS_SETTING, int(self.cbPlotRigsBeforeSplit.isChecked()))
        if self.cbPlotRigsBeforeMerge is not None:
            self._settings.setValue(self.PRE_MERGE_PLOT_CHARACTERS_SETTING, int(self.cbPlotRigsBeforeMerge.isChecked()))
        if self.cbPlotPropsBeforeSplit is not None:
            self._settings.setValue(self.PRE_SPLIT_PLOT_PROPS_SETTING, int(self.cbPlotPropsBeforeSplit.isChecked()))
        if self.cbPlotVehiclesBeforeSplit is not None:
            self._settings.setValue(self.PRE_SPLIT_PLOT_VEHICLES_SETTING, int(self.cbPlotVehiclesBeforeSplit.isChecked()))
        if self.cbPlotPropsBeforeMerge is not None:
            self._settings.setValue(self.PRE_MERGE_PLOT_PROPS_SETTING, int(self.cbPlotPropsBeforeMerge.isChecked()))
        if self.cbPlotVehiclesBeforeMerge is not None:
            self._settings.setValue(self.PRE_MERGE_PLOT_VEHICLES_SETTING, int(self.cbPlotVehiclesBeforeMerge.isChecked()))
        if self.cbPreserveGsSkelCharacterization is not None:
            self._settings.setValue(self.PRESERVE_GS_SKEL_CHARACTERIZATION_SETTING,
                                    int(self.cbPreserveGsSkelCharacterization.isChecked()))
        if self.cbCreatePosesVerticalSplit is not None:
            self._settings.setValue(self.CREATE_POSES_VERTICAL_SPLIT_SETTING,
                                    int(self.cbCreatePosesVerticalSplit.isChecked()))
        if self.autoSaveSceneSetting is not None:
            self._settings.setValue(self.AUTO_SAVE_SCENE_SETTING,
                                    int(self.autoSaveSceneSetting))
        if self.timelineSyncSetting is not None:
            self._settings.setValue(self.SYNC_TIMELINE_SETTING,
                                    int(self.timelineSyncSetting))

        autoSaveValue = self.autoSaveSetting()
        if not autoSaveValue:
            buttons = QtGui.QMessageBox.Yes | QtGui.QMessageBox.Cancel
            msg = ("Scene split settings not autosaved.\n\n"
                   "Do you want to save now?")
            dialogResult = QtGui.QMessageBox.warning(self, "Auto Save Settings", msg, buttons)
            if dialogResult == QtGui.QMessageBox.Yes:
                self._onSaveSceneSettings()

        # we try to do a closeEvent but due to our RS.Tools.UI.Run setup this can fail because it already got deleted
        try:
            super(SceneSplitEditorWidget, self).closeEvent(event)
        except TypeError:
            pass

    def _onFileNewCompleted(self, source=None, event=None):
        if self._fileOpenInProgress:
            # Old scene has just been flushed as part of file open so ignore this update
            return

        # Load for empty new scene (reset)
        self._onRefresh()

    def _onFileOpen(self, source=None, event=None):
        self._fileOpenInProgress = True
    
    def _onFileOpenCompleted(self, source=None, event=None):
        """Handle changes in mobu scene contents (e.g. file open/new)"""
        self._fileOpenInProgress = False
        self._onRefresh()

    def _onTimelineChanged(self, control=None, event=None):
        """Updates frame if mobu timeline changed

        Args:
            control (mobu.FBPlayerControl): Motion Builder object calling the event
            event (mobu.FBEvent): the event being called
        """
        if self.syncTimelineSetting():
            currentFrame = Globals.System.LocalTime.GetFrame()
            if self.timeline.currentFrame != currentFrame:
                self.timeline.updateFrame(currentFrame)

    def _onFrameChanged(self, frame):
        """Updates frame if scene splitter time changed

        Args:
            frame (int): Value to set timeline to
        """
        if self.syncTimelineSetting() and Globals.System.LocalTime.GetFrame() != frame:
            Globals.Callbacks.enabled = False
            Globals.Player.Goto(mobu.FBTime(0, 0, 0, frame, 0))
            Globals.Scene.Evaluate()
            Globals.Callbacks.enabled = True

    def _registerCallbacks(self):
        """Add callbacks to respond to changes in the Motionbuilder scene"""
        Globals.Callbacks.OnFileOpen.Add(self._onFileOpen)
        Globals.Callbacks.OnFileNewCompleted.Add(self._onFileNewCompleted)
        Globals.Callbacks.OnFileOpenCompleted.Add(self._onFileOpenCompleted)
        Globals.Callbacks.OnSynchronizationEvent.Add(self._onTimelineChanged)
        self.splitSettingsModel.registerCallbacks()

    def _removeCallbacks(self):
        """Remove all callbacks"""
        Globals.Callbacks.OnFileOpen.Remove(self._onFileOpen)
        Globals.Callbacks.OnFileNewCompleted.Remove(self._onFileNewCompleted)
        Globals.Callbacks.OnFileOpenCompleted.Remove(self._onFileOpenCompleted)
        Globals.Callbacks.OnSynchronizationEvent.Remove(self._onTimelineChanged)
        self.splitSettingsModel.removeCallbacks()


@UI.Run(title=SceneSplitEditorWidget.TOOL_TITLE,
        url=SceneSplitEditorWidget.TOOL_HELP_URL,
        size=(500, 350), dockable=False)
def Run():
    return SceneSplitEditorWidget()
