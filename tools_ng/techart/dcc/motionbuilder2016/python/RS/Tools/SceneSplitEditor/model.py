import re
import string
import collections

from PySide import QtCore

from RS.Utils.Scene import Time
from RS.Core.ReferenceSystem import Manager
from RS.PyCoreQt.Models import dragDropModel
from RS.Core.SceneSplitter import settings, util
from RS.Tools.SceneSplitEditor import modelItem, const


class SceneSplitSettingsModel(dragDropModel.DragDropModel):
    """The split settings model"""
    HEADERS = ["Name", "Range", "Type", "Bug ID", "Bug Status"]
    DEFAULT_SUFFIXES = tuple(string.ascii_uppercase)

    settingsAboutToBeLoaded = QtCore.Signal()
    settingsLoaded = QtCore.Signal()
    modelRefreshed = QtCore.Signal()
    preWriteSettingsFile = QtCore.Signal(str)
    writeSettingsFileFailed = QtCore.Signal(str, str)
    requestSaveSettings = QtCore.Signal(object)

    def __init__(self, sceneSplitSettings, parent=None):
        self._sceneSplitSettings = sceneSplitSettings
        self.refManager = Manager.Manager()
        self.baseSceneSectionItem = None
        self.dragMode = False

        super(SceneSplitSettingsModel, self).__init__(parent)

    def checkReferencesChanged(self):
        """
        Called when referenced are added/deleted/renamed within the current scene.
        Prune any items that represent references that no longer exist in the scene and add items for any new ones.
        """
        self.checkReferencesRemoved()
        self.checkReferencesAdded()
        self.checkVerticalSplitsAdded()

    def checkVerticalSplitsAdded(self):
        """Add any resources to vertical splits"""
        splitPartSuffixGroups = collections.defaultdict(list)
        for splitPart in self.splitPartsItem.childItems:
            splitPartSuffixGroups[splitPart.getSuffix()].append(splitPart)

        for splitPartGroup in splitPartSuffixGroups.itervalues():

            # get all resource namespaces
            refNamespaces = set()
            for splitPart in splitPartGroup:
                for resourceItem in splitPart.childItems:
                    refNamespaces.add(resourceItem.namespace)

            # go through each split and check that namespaces match
            for row, splitPart in enumerate(splitPartGroup):
                for namespace in refNamespaces:
                    resourceNamespaces = [resourceItem.namespace for resourceItem in splitPart.childItems]

                    if namespace not in resourceNamespaces:
                        resourceItem = modelItem.ResourceItem(namespace)

                        targetRow = len(resourceNamespaces)
                        modelIndex = self.createIndex(
                            targetRow, const.SceneSplitColumns.NAME, splitPart)
                        self.beginInsertRows(modelIndex.parent(), modelIndex.row(), modelIndex.row())
                        splitPart.appendChild(resourceItem)
                        self.endInsertRows()

    def checkVerticalSplitsRemoved(self, suffix, namespace):
        """Removes resource item under split scenes with same suffix

        Args:
            suffix (str): Split scene suffix
            namespace (str): Namespace to remove
        """
        modelIndexesToRemove = []
        for row, splitPart in enumerate(self.splitPartsItem.childItems):
            if splitPart.getSuffix() != suffix:
                continue

            for resourceItem in reversed(splitPart.childItems):
                if resourceItem.namespace == namespace:
                    splitParentIndex = self.createIndex(
                        row, const.SceneSplitColumns.NAME, splitPart)

                    self.beginRemoveRows(splitParentIndex, resourceItem.row(), resourceItem.row())
                    splitPart.removeChild(resourceItem)
                    self.endRemoveRows()

    def checkReferencesRemoved(self):
        """Checks the current contents of the model against the scene to find any references that no longer exist
        and then removes those items from the model.
        """
        allRefNamespaces = self._sceneSplitSettings.getAllSplittableNamespaces()
        saveRequired = False

        # check base model
        baseModelIndexesToRemove = []
        for resourceRowIdx, resourceItem in enumerate(self.baseSceneSectionItem.childItems):
            if resourceItem.namespace not in allRefNamespaces:
                # This item's namespace no longer exists in the scene record its model index
                baseModelIndexesToRemove.append(self.baseSceneSectionItem.index(resourceRowIdx, 0))
                saveRequired = True

        # remove from base model
        for baseModelIndex in reversed(baseModelIndexesToRemove):
            resourceItem = baseModelIndex.internalPointer()
            self.beginRemoveRows(baseModelIndex.parent(), baseModelIndex.row(),  baseModelIndex.row())
            self.baseSceneSectionItem.removeChild(resourceItem)
            self.endRemoveRows()

        # check scene split
        splitPartIndicesToRemove = []
        for splitPart in self.splitPartsItem.childItems:
            for resourceRowIdx, resourceItem in enumerate(splitPart.childItems):
                if resourceItem.namespace not in allRefNamespaces:
                    # This item's namespace no longer exists in the scene record its model index
                    try:
                        resourceSplitItem = self.splitPartsItem.index(resourceRowIdx, 0)
                        splitPartIndicesToRemove.append(resourceSplitItem)
                        saveRequired = True
                    except AttributeError:
                        pass

        for modelIndex in reversed(splitPartIndicesToRemove):
            parentIdx = modelIndex.parent()
            if not parentIdx.isValid():
                continue

            parentItem = parentIdx.internalPointer()
            resourceItem = modelIndex.internalPointer()
            self.beginRemoveRows(parentIdx, modelIndex.row(),  modelIndex.row())
            parentItem.removeChild(resourceItem)
            self.endRemoveRows()

        if saveRequired:
            self.saveSettings()

    def checkReferencesAdded(self):
        """
        Checks the current contents of the scene against the model to find any new references that have been added
        and then creates new items in the model to represent those.
        """
        # Find all namespaces currently represented in in the model
        existingNamespaces = set()
        for baseSceneItem in self.baseSceneSectionItem.childItems:
            existingNamespaces.add(baseSceneItem.namespace)

        usedNamespaces = set()
        for sceneSplitItem in self.splitPartsItem.childItems:
            for resourceItem in sceneSplitItem.childItems:
                usedNamespaces.add(resourceItem.namespace)

        # Get all relevant resource namespaces in the current scene
        allRefNamespaces = self._sceneSplitSettings.getAllSplittableNamespaces()

        # Find the difference to get any namespaces that are in the scene but don't have an item in the model yet
        newlyAddedNamespaces = allRefNamespaces - existingNamespaces

        # Add items for any new namespaces
        if len(newlyAddedNamespaces) > 0:
            self.baseSceneSectionItem.appendResourceItemsFromNamespaces(newlyAddedNamespaces)

        # update enabled one
        unusedNamespaces = allRefNamespaces - usedNamespaces
        self.baseSceneSectionItem.updateResourceItemsFromNamespaces(usedNamespaces, unusedNamespaces)

    def registerCallbacks(self):
        """Add callbacks that allow the model to stay in sync with changes in the scene"""
        if hasattr(self.refManager, "CALLBACKS"):  # Ref system callbacks not yet implemented on GTA5
            self.refManager.CALLBACKS.CREATE.connect(self.checkReferencesAdded)
            self.refManager.CALLBACKS.UPDATE.connect(self.checkReferencesChanged)
            self.refManager.CALLBACKS.DELETE.connect(self.checkReferencesRemoved)

    def removeCallbacks(self):
        """Remove all callbacks"""
        if hasattr(self.refManager, "CALLBACKS"):  # Ref system callbacks not yet implemented on GTA5
            self.refManager.CALLBACKS.CREATE.disconnect(self.checkReferencesAdded)
            self.refManager.CALLBACKS.UPDATE.disconnect(self.checkReferencesChanged)
            self.refManager.CALLBACKS.DELETE.disconnect(self.checkReferencesRemoved)

    def headerData(self, section, orientation=QtCore.Qt.Vertical, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface."""
        if role == QtCore.Qt.DisplayRole and section < len(self.HEADERS):
            return self.HEADERS[section]

    def columnCount(self, parent):
        """Reimplemented from QAbstractItemModel interface."""
        if parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return len(self.HEADERS)

    def moveItems(self, parentIndex, indices, mimeType, row=None, copy=False):
        """Reimplemented from DragDropModel interface."""
        # sort and reverse indices when copying to prevent index crashing issues
        indices.sort(reverse=True)

        if parentIndex.isValid() and isinstance(parentIndex.internalPointer(), modelItem.SplitSceneItem):
            copy = True

        dataChanged = super(SceneSplitSettingsModel, self).moveItems(parentIndex, indices, mimeType, row=row, copy=copy)
        if dataChanged:
            self.saveSettings()

        return dataChanged

    def moveItem(self, index, row, parentIndex, mimeType=None):
        """
        Reimplemented from DragDropModel interface.
        Handle items moved within the same parent
        """
        return False

    def copyItem(self, index, row, parentIndex, mimeType=None):
        """Handle copied items"""
        if not index.isValid() or not parentIndex.isValid():
            return False

        itemToCopy = index.internalPointer()
        if not isinstance(itemToCopy, modelItem.ResourceItem):
            return False

        newParentItem = parentIndex.internalPointer()
        if not isinstance(newParentItem, modelItem.SplitSceneItem):
            return False

        # check for duplicates
        for item in newParentItem.childItems:
            if item.namespace == itemToCopy.namespace:
                return False

        # only copy from BaseSceneItem and SplitSceneItem
        oldParentItem = index.parent().internalPointer()
        isOldSplitScene = isinstance(oldParentItem, modelItem.SplitSceneItem)
        isOldBaseScene = isinstance(oldParentItem, modelItem.BaseSceneItem)
        if not any([isOldSplitScene, isOldBaseScene]):
            return False

        # make new
        copiedItem = modelItem.ResourceItem(itemToCopy.namespace)

        targetRow = row
        if targetRow < 0:
            targetRow = newParentItem.childCount()

        # append new item
        self.beginInsertRows(parentIndex, targetRow, targetRow)
        newParentItem.appendChild(copiedItem)
        self.endInsertRows()

        # if we try to copy from split to split, move it
        if isOldSplitScene:
            self.beginRemoveRows(index.parent(), index.row(), index.row())
            oldParentItem.removeChild(itemToCopy)
            self.endRemoveRows()

        self.checkReferencesChanged()
        self.modelRefreshed.emit()

        return True

    def dropItem(self, index, row, parentIndex, mimeType=None):
        """Handle dropped items"""
        if not index.isValid() or not parentIndex.isValid():
            return False

        oldParentIndex = index.parent()
        if not oldParentIndex.isValid():
            oldParentItem = self.rootItem
        else:
            oldParentItem = oldParentIndex.internalPointer()

        oldItem = index.internalPointer()
        newParentItem = parentIndex.internalPointer()

        isBaseScene = isinstance(newParentItem, modelItem.BaseSceneItem)
        isSplitScene = isinstance(newParentItem, modelItem.SplitSceneItem)
        isOldParentSplitScene = isinstance(oldParentItem, modelItem.SplitSceneItem)
        # only let you drop on these types
        if not any([isBaseScene, isSplitScene]):
            return False

        sourceRow = index.row()
        targetRow = row
        if targetRow < 0:
            targetRow = newParentItem.childCount()

        # Append child if new parent is scene split
        if isSplitScene:
            self.beginInsertRows(parentIndex, sourceRow, targetRow)
            newParentItem.appendChild(oldItem, targetRow)
            self.endInsertRows()

        # Remove child when newParent is a BaseSceneItem or oldParent is SplitSceneItem
        if isBaseScene or isOldParentSplitScene:
            self.beginRemoveRows(oldParentIndex, sourceRow, sourceRow)
            oldParentItem.removeChild(oldItem)
            self.endRemoveRows()

        # check removal on vertical splits
        if isOldParentSplitScene:
            self.checkVerticalSplitsRemoved(oldParentItem.getSuffix(), oldItem.namespace)

        self.checkReferencesChanged()

        return True

    def _addSceneSectionItems(self, sceneSectionItems):
        """Appends the given scene section items to the root of the model"""
        if not sceneSectionItems:
            return

        for item in sceneSectionItems:
            self.splitPartsItem.appendChild(item)

    def loadSettings(self):
        """Load or reload all data in the model based on the current scene"""
        self.setupModelData(self.rootItem)

    def setupModelData(self, rootItem):
        """Sets up the model data based on the current scene"""
        # Load settings
        self.settingsAboutToBeLoaded.emit()
        self._sceneSplitSettings.loadFromFile()
        self.settingsLoaded.emit()

        self.refreshModel()

    def refreshModel(self):
        """Re-creates all model items based on the current split settings data"""
        self.clear()

        self.masterSplitItem = modelItem.MasterFileItem()
        self.splitPartsItem = modelItem.SplitPartsItem()

        self.beginInsertRows(QtCore.QModelIndex(), 0, 1)
        self.rootItem.appendChild(self.masterSplitItem)
        self.endInsertRows()

        self.splitPartsItem.setParent(self.rootItem)

        # Create model items wrapping the settings objects
        self.baseSceneSectionItem = modelItem.BaseSceneItem(self._sceneSplitSettings)
        self.masterSplitItem.appendChild(self.baseSceneSectionItem)

        sceneSectionItems = []
        for splitSection in self._sceneSplitSettings.splitSections:
            splitSectionItem = modelItem.SplitSceneItem(splitSection)
            sceneSectionItems.append(splitSectionItem)

        self._addSceneSectionItems(sceneSectionItems)

        # Populate base scene items and ensure any old namespaces that
        # no longer exist in the scene are pruned from the split items
        self.checkReferencesChanged()

        self.modelRefreshed.emit()

    def _makeSuffixUnique(self, suffix=None):
        """
        Returns a unique suffix which is not yet used by any items currently in the model.

        Keyword Args:
            suffix (str): The suffix to make unique. If not specified, a new suffix will be chosen
                from a list of defaults.

        Returns:
            str: Unique variant of the suffix - or the original if it was already unique.
        """
        allExistingSuffixes = frozenset([childItem.getSuffix() for childItem in self.splitPartsItem.childItems])

        if not suffix:
            for defaultSuffix in self.DEFAULT_SUFFIXES:
                if defaultSuffix not in allExistingSuffixes:
                    suffix = defaultSuffix
                    break
            if not suffix:
                # All default suffixes are already taken, fall back to appending a number
                suffix = self.DEFAULT_SUFFIXES[0]

        if suffix not in allExistingSuffixes:
            # Early out - already unique
            return suffix

        numberToAppend = 1
        padding = 1
        origSuffix = suffix

        # If the suffix already ends with a number then start searching for a unique variant from that value
        tailNumbers = re.search(r"(\d+)$", origSuffix)
        if tailNumbers:
            padding = len(tailNumbers.group(0))
            numberToAppend = int(tailNumbers.group(0)) + 1
            origSuffix = suffix.rstrip(string.digits)

        # Loop until we find a number suffix that's not already taken
        while suffix in allExistingSuffixes:
            suffix = "{}{}".format(origSuffix, str(numberToAppend).zfill(padding))
            numberToAppend += 1

        return suffix

    def data(self, modelIndex, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface."""
        if not modelIndex.isValid():
            return None

        if role == QtCore.Qt.DisplayRole:
            if modelIndex.column() == const.SceneSplitColumns.FRAME_RANGE:
                splitItem = modelIndex.internalPointer()
                if isinstance(splitItem, (modelItem.SplitSceneItem, modelItem.BaseSceneItem)):
                    return splitItem.getFrameRange()

        return super(SceneSplitSettingsModel, self).data(modelIndex, role)

    def setData(self, modelIndex, value, role=QtCore.Qt.EditRole):
        """Reimplemented from BaseModel interface. Called by the view when the user edits a value."""
        if not modelIndex.isValid():
            return False

        if role == QtCore.Qt.EditRole:

            if modelIndex.column() == const.SceneSplitColumns.NAME:
                splitItem = modelIndex.internalPointer()
                if isinstance(splitItem, modelItem.SplitSceneItem) and splitItem.getSuffix() != value:
                    # Ensure the suffix is unique before we set it
                    value = self._makeSuffixUnique(value)

            elif modelIndex.column() == const.SceneSplitColumns.FRAME_RANGE:
                splitItem = modelIndex.internalPointer()
                if isinstance(splitItem, (modelItem.SplitSceneItem, modelItem.BaseSceneItem)):
                    splitItem.setFrameRange(value)
                    self.saveSettings()
                    return True

        # Call base method to set the data
        dataChanged = super(SceneSplitSettingsModel, self).setData(modelIndex, value, role)

        if dataChanged:
            # Save any changes
            self.saveSettings()

        return dataChanged

    def mergeSplits(self, modelIndexList, verticalMerge=False):
        """Merges split items

        Args:
            modelIndexList (list of QtCore.QModelIndex): Model indexes to merge together
            verticalMerge (bool): If True will only merge vertical splits
        """
        # Items are row-based so filter out additional column indexes to avoid attempting to delete the same item twice
        modelIndexList = [idx for idx in modelIndexList if idx.isValid() and idx.column() == 0]

        if verticalMerge:
            # filter to vertical splits
            modelIndexList = [idx for idx in modelIndexList if self.isVerticalSplit(idx)]

        if len(modelIndexList) < 2:
            return

        # grab parent to merge into
        parentModelIndex = modelIndexList.pop()
        parentItem = parentModelIndex.internalPointer()

        if not verticalMerge:
            # add all vertical splits contained in a horizontal into index
            modelIndexList = self.getAllSplits(modelIndexList, excludeIndexes=[parentModelIndex])

        # get model items and get range to merge
        childItems = [modelIndex.internalPointer() for modelIndex in modelIndexList]
        allRanges = [childItem.getFrameRange() for childItem in childItems]
        allRanges.append(parentItem.getFrameRange())

        # set parent range
        startFrames, endFrames = zip(*allRanges)
        frameRange = [min(startFrames), max(endFrames)]
        parentItem.setFrameRange(frameRange)

        # get all the children, use set to remove duplicates
        allChildren = set()
        for splitItem in childItems:
            for resourceItem in splitItem.childItems:
                allChildren.add(resourceItem.namespace)

        parentChildren = set()
        for resourceItem in parentItem.childItems:
            parentChildren.add(resourceItem.namespace)

        # set children merge
        addedChildren = allChildren - parentChildren
        parentItem.appendResourceItemsFromNamespaces(addedChildren, updateInternalData=True)

        # remove items
        self.removeSceneSplits(modelIndexList)

        self.saveSettings()

    def isVerticalSplit(self, modelIndex):
        """Checks if modelIndex is a vertical split item

        Args:
            modelIndex (QtCore.QModelIndex): ModelIndex to check

        Returns:
            bool: True or False if is vertical split
        """
        splitItem = modelIndex.internalPointer()
        splitName = splitItem.getSuffix()

        for item in self.splitPartsItem.childItems:
            if item is not splitItem and item.getSuffix() == splitName:
                return True

        return False

    def getAllSplits(self, modelIndexList, excludeIndexes=None):
        """Get all vertical splits related to any modelindex in list

        Args:
            modelIndexList (list of QtCore.QModelIndex): ModelIndexes to search
            excludeIndexes (list of QtCore.QModelIndex): Items to exclude

        Returns:
            list of QtCore.QModelIndex: Extended ModelIndex list with any splits found
        """
        excludeIndexes = excludeIndexes if excludeIndexes is not None else []

        for modelIndex in modelIndexList:
            splitItem = modelIndex.internalPointer()
            for idx, item in enumerate(self.splitPartsItem.childItems):
                if item.getSuffix() != splitItem.getSuffix() or splitItem == item:
                    continue

                foundModelIndex = self.getModelIndexByRow(idx)
                if foundModelIndex not in modelIndexList and not any(idx == index.row() for index in excludeIndexes):
                    modelIndexList.append(foundModelIndex)

        return modelIndexList

    def createNewVerticalSplit(self, modelIndex, frame=None):
        """Adds a new vertical split item to the model

        Args:
            modelIndex (QtCore.QModelIndex): The scene split modelIndex to make a vertical split of
            frame (int): frame to split at
        """
        splitItem = modelIndex.internalPointer()
        if not isinstance(splitItem, modelItem.SplitSceneItem):
            return

        # insert after
        insertRow = modelIndex.row() + 1

        # use same suffix
        splitSection = settings.SceneSplitSectionSettings()
        splitSection.splitSuffix = splitItem.getSuffix()

        self._sceneSplitSettings.splitSections.insert(insertRow, splitSection)
        splitItem = modelItem.SplitSceneItem(splitSection)

        self._splitRanges(splitItem, frame)

        self._addSceneSectionItems([splitItem])
        self.checkReferencesChanged()

        self.saveSettings()

    def _splitRanges(self, splitItem, frame=None):
        """Splits all time frame ranges based on current frame

        Args:
            splitItem (modelItem.SceneSplitItem): The vertical split to split
            frame (int): frame to split at
        """
        # get suffix
        suffix = splitItem.getSuffix()

        # get current frame
        frameSplit = frame if frame else util.getCurrentFrame()

        # get scene frame range
        startSceneFrame, endSceneFrame = Time.getSceneFrameRange()
        startRange, endRange = startSceneFrame, endSceneFrame

        # get split items
        allSplitItems = sorted(self.splitPartsItem.childItems)
        # init loop variables
        lastFrameRange = allSplitItems[0].getFrameRange()

        # get first and last suffix indexes
        firstIndex = lastIndex = None
        for index, splitPart in enumerate(allSplitItems):
            if splitPart.getSuffix() == suffix:
                if not firstIndex:
                    firstIndex = index
                lastIndex = index

        foundSplitRange = False
        for index, splitPart in enumerate(allSplitItems):

            # if not same suffix skip
            if not splitPart.getSuffix() == suffix:
                continue

            # get frame range of current split
            startFrame, endFrame = splitPart.getFrameRange()

            # check if in range
            if endFrame >= frameSplit >= startFrame:
                if startFrame == frameSplit:
                    startRange, endRange = startFrame, frameSplit
                    splitPart.setFrameRange([startFrame + 1, endFrame])
                elif endFrame == frameSplit:
                    startRange, endRange = endFrame, frameSplit
                    splitPart.setFrameRange([startFrame, frameSplit - 1])
                else:
                    startRange, endRange = frameSplit, endFrame
                    splitPart.setFrameRange([startFrame, frameSplit - 1])
                foundSplitRange = True

            # check if in-between range
            elif lastFrameRange[1] < frameSplit < startFrame:
                startRange, endRange = lastFrameRange[1] + 1, startFrame - 1
                foundSplitRange = True

            # check if after range
            elif index == lastIndex and frameSplit > endFrame:
                startRange, endRange = endFrame + 1, endSceneFrame
                foundSplitRange = True

            # check if before range
            elif index == firstIndex and frameSplit < startFrame:
                startRange, endRange = startSceneFrame, startFrame - 1
                foundSplitRange = True

            if foundSplitRange:
                break

            lastFrameRange = [startFrame, endFrame]

        # set range to new split values
        splitItem.setFrameRange([startRange, endRange])

    def createNewSceneSplit(self, suffix="", namespaces=None):
        """Adds a new scene split item to the model

        Args:
            suffix (str, optional): Suffix of split
        """
        splitSection = settings.SceneSplitSectionSettings()
        suffix = self._makeSuffixUnique(suffix)
        splitSection.splitSuffix = suffix

        self._sceneSplitSettings.splitSections.append(splitSection)
        splitItem = modelItem.SplitSceneItem(splitSection)
        self._addSceneSectionItems([splitItem])

        if namespaces is not None:
            for split in self.splitPartsItem.childItems:
                for child in split.childItems:
                    if child.namespace in namespaces:
                        namespaces.remove(child.namespace)

            splitItem.appendResourceItemsFromNamespaces(namespaces, updateInternalData=True)
            self.checkReferencesChanged()

        self.saveSettings()

    def removeSceneSplits(self, modelIndexList):
        """Removes scene split items matching the given model indices from the model

        Args:
            modelIndexList (list of QtCore.QModelIndex): Model indices to remove
        """
        # Items are row-based so filter out additional column indexes to avoid attempting to delete the same item twice
        modelIndexList = [idx for idx in modelIndexList if idx.isValid() and idx.column() == 0]
        # Delete in reverse row order to avoid an access violation crash where the view reads from a deleted item
        modelIndexList.sort(key=lambda modelIdx: modelIdx.row(), reverse=True)

        saveRequired = False
        for modelIndex in modelIndexList:
            if not modelIndex.isValid():
                continue

            sourceItem = modelIndex.internalPointer()
            isSceneSplit = isinstance(sourceItem, modelItem.SplitSceneItem)
            isResourceItem = isinstance(sourceItem, modelItem.ResourceItem)
            if not any([isSceneSplit, isResourceItem]):
                # Ignore any selected items that are not scene splits
                # (base scene item and resource items cannot be deleted)
                continue

            if isSceneSplit:
                parentIndex = modelIndex.parent()
                rowIndex = modelIndex.row()
                self.beginRemoveRows(parentIndex, rowIndex, rowIndex)
                self.splitPartsItem.removeChild(sourceItem)
                self.endRemoveRows()

                # Remove the split section object from the settings
                self._sceneSplitSettings.splitSections.remove(sourceItem.getSectionSettings())

            elif isResourceItem:
                splitSceneIndex = modelIndex.parent()
                splitScene = splitSceneIndex.internalPointer()
                if isinstance(splitScene, modelItem.SplitSceneItem):
                    # make sure any other splits with same suffix also removed same item
                    self.checkVerticalSplitsRemoved(splitScene.getSuffix(), sourceItem.namespace)

            saveRequired = True

        self.checkReferencesChanged()

        if saveRequired:
            self.saveSettings()

    def getModelIndexByRow(self, rowIndex, columnIndex=0, childIndex=None):
        """Get first column modelIndex by row

        Args:
            rowIndex (int): Row to get
            columnIndex (int): Column to get
            childIndex (int): Child of row to get model index of
        """
        childItem = self.splitPartsItem.childItems[rowIndex]
        modelIndex = self.createIndex(rowIndex, columnIndex, childItem)

        if childIndex is not None:
            return self.index(childIndex, columnIndex, modelIndex)

        return modelIndex

    def addSplitResource(self, modelIndex, data):
        """Updates a split item based on index

        Args:
            modelIndex (QCore.QModelIndex): Model index to update
            data (str): Value to set
        """
        splitItem = modelIndex.internalPointer()
        for split in self.splitPartsItem.childItems:
            for child in split.childItems:
                if child.namespace == data and not self.isVerticalSplit(modelIndex):
                    return

        splitItem.appendResourceItemsFromNamespaces([data], updateInternalData=True)
        self.checkReferencesChanged()
        self.saveSettings()
        self.modelRefreshed.emit()

    def getSettingsFilePath(self):
        """Returns the path to the split settings file"""
        return self._sceneSplitSettings.getSplitSettingsFilePath()

    def saveSettings(self):
        """Requests that the current split settings data be saved to disk"""
        self.requestSaveSettings.emit(self._sceneSplitSettings)

    def getSettings(self):
        """
        Returns a reference to the internal scene split settings object owned by this model.

        Returns:
            settings.SceneSplitSettings: The settings object holding the split settings data exposed by this model.
        """
        return self._sceneSplitSettings

    def supportedDropActions(self):
        """Reimplemented from QAbstractItemModel interface."""
        return QtCore.Qt.MoveAction

    def supportedDragActions(self):
        """Reimplemented from QAbstractItemModel interface."""
        return QtCore.Qt.MoveAction


class SplitFoldersModel(QtCore.QAbstractListModel):
    """Model for presenting the list of split folders within the split settings that this model references."""

    requestSaveSettings = QtCore.Signal(object)

    def __init__(self, sceneSplitSettings, parent=None):
        """
        Initialise the SplitFoldersModel instance.

        Args:
            sceneSplitSettings (SceneSplitSettings): The settings object that the list of split
                folders should be presented from.
            parent (QObject): The parent object for this model.
        """
        super(SplitFoldersModel, self).__init__(parent)

        if not isinstance(sceneSplitSettings, settings.SceneSplitSettings):
            raise TypeError("sceneSplitSettings parameter must be of type SceneSplitSettings")

        self._sceneSplitSettings = sceneSplitSettings

    def rowCount(self, parent=QtCore.QModelIndex()):
        """Reimplemented from QAbstractItemModel interface."""
        return self._sceneSplitSettings.splitFolderCount

    def data(self, modelIndex, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface."""
        if not modelIndex.isValid():
            return None

        if role == QtCore.Qt.DisplayRole:
            return self._sceneSplitSettings.getSplitFolderNameByIndex(modelIndex.row())

    def addFolder(self, name=None):
        """
        Adds a new split settings folder to the model and settings data.

        Args:
            name (str optional): The name for the new settings model (if None, a default name will be generated)
        """
        self._sceneSplitSettings.addNewSplitFolder(name)

        self.saveSettings()

    def removeFolder(self, folderIndex):
        """
        Deletes all split settings data relating to a split folder in the model.

        Args:
            folderIndex (int): The index of the folder that should be deleted.
        """
        self._sceneSplitSettings.removeSplitFolder(folderIndex)

        self.saveSettings()

    def saveSettings(self):
        """Requests that the current split settings data be saved to disk"""
        self.requestSaveSettings.emit(self._sceneSplitSettings)
