import os

from PySide import QtGui

from RS import Config


class SceneSplitColumns(object):
    """Enumeration of split scene settings model column indices"""
    NAME = 0
    FRAME_RANGE = 1
    TYPE = 2
    BUG_ID = 3
    BUG_STATUS = 4


class SceneSplitRows(object):
    """Enumeration of split scene settings model row indices"""
    MASTER_FILE_ROW = 0
    SPLIT_PARTS_ROW = 1


class Icons(object):
    """Icons used by the split scene settings editor"""
    BASE_FBX = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "base_fbx_16.png"))
    SPLIT_FBX = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "split_fbx_16.png"))
    RESOURCE = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "resource_8.png"))
    RESOURCE_CHECKED = QtGui.QIcon(
        os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "resource_checked_8.png"))
    RESOURCE_UNCHECKED = QtGui.QIcon(
        os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "resource_unchecked_8.png"))
    REFRESH = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "refresh_white_16.png"))
    ADD = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "add_white_16.png"))
    REMOVE = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "remove_white_16.png"))
    SPLIT_HORIZONTAL = QtGui.QIcon(
        os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "split_horizontal_32.png"))
    SPLIT_VERTICAL = QtGui.QIcon(
        os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "split_vertical_32.png"))
    JOIN_HORIZONTAL = QtGui.QIcon(
        os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "join_horizontal_32.png"))
    JOIN_VERTICAL = QtGui.QIcon(
        os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "join_vertical_32.png"))
    TRASH = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "trashcan_white_32.png"))
    SAVE_SETTINGS = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "SceneSplitEditor", "savesettings_64.png"))


class MismatchDialogResult(object):
    """Results for mismatch dialog"""
    ACCEPTED = 1
    REJECTED = 0
