from PySide import QtCore, QtGui

from RS.Utils.Scene import Time
from RS.Tools.SceneSplitEditor import modelItem, const


class RangeSpinBox(QtGui.QSpinBox):
    SPIN_BOX_BG_COLOR = (24, 24, 24)
    MIN_WIDTH = 50

    def __init__(self, parent=None):
        super(RangeSpinBox, self).__init__(parent)

        sceneFrameRange = Time.getSceneFrameRange()

        self.setMinimumWidth(self.MIN_WIDTH)
        self.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.setAlignment(QtCore.Qt.AlignRight)
        self.setSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Minimum)

        self.setRange(*sceneFrameRange)


class FrameRangeEditor(QtGui.QWidget):
    START_LABEL = "From:"
    END_LABEL = "to:"
    SPINBOX_BG_COLOR = (24, 24, 24)

    def __init__(self, item, parent=None):
        """
        Args:
            item (QtGui.QAbstractItemModel, optional): Model item from model index
            parent (QtGui.QWidget, optional): Parent of editor
        """
        super(FrameRangeEditor, self).__init__(parent)

        # set background color of spinbox
        self.setStyleSheet("QSpinBox {{ background-color: rgb({}, {}, {}); }}".format(*self.SPINBOX_BG_COLOR))

        # create layout
        self.hLayout = QtGui.QHBoxLayout()
        self.hLayout.setContentsMargins(0, 0, 0, 0)

        # add labels and spin boxes
        self.startLabel = QtGui.QLabel(self.START_LABEL)
        self.endLabel = QtGui.QLabel(self.END_LABEL)
        self.startRange = RangeSpinBox()
        self.endRange = RangeSpinBox()

        self.hLayout.addWidget(self.startLabel)
        self.hLayout.addWidget(self.startRange)
        self.hLayout.addWidget(self.endLabel)
        self.hLayout.addWidget(self.endRange)

        # read frame range from item
        frameRange = item.getFrameRange()
        self.setFrameRange(frameRange)

        # base scene shouldn't be editable
        if isinstance(item, modelItem.BaseSceneItem):
            self.startRange.setEnabled(False)
            self.endRange.setEnabled(False)

        self.setLayout(self.hLayout)

    def setFrameRange(self, frameRange):
        """Sets the frame range

        Args:
            frameRange (list of int): The frame range to set
        """
        self.startRange.setValue(frameRange[0])
        self.endRange.setValue(frameRange[1])

    def getFrameRange(self):
        """Gets the frame range

        Returns:
             list of int: the frame range on the spinboxes
        """
        return [self.startRange.value(), self.endRange.value()]

    def validateRange(self, frameRange=None):
        """Method that validates the range on start and end spinboxes

        Args:
            frameRange (list): Start and end frame range values
        Returns:
            list of int: Frame range validated
        """
        frameRange = frameRange if frameRange else self.getFrameRange()

        sceneStartFrame, sceneEndFrame = Time.getSceneFrameRange()

        # clamp min and max
        startFrameClamped = max(sceneStartFrame, min(sceneEndFrame, frameRange[0]))
        endFrameClamped = min(sceneEndFrame, max(sceneStartFrame, frameRange[1]))

        if startFrameClamped > endFrameClamped:
            startFrameClamped = endFrameClamped

        if frameRange[0] != startFrameClamped:
            frameRange[0] = startFrameClamped

        if frameRange[1] != endFrameClamped:
            frameRange[1] = endFrameClamped

        self.setFrameRange(frameRange)

        return frameRange


class BaseDelegateItem(QtGui.QStyledItemDelegate):
    SIZE_HINT = (0, 0)
    SEPARATOR_COLOR = (70, 70, 70)
    SEPARATOR_OUTLINE_COLOR = (39, 39, 39)
    SEPARATOR_X_OFFSET = 20

    def paint(self, painter, option, modelIndex):
        """Overridden method to create a rectangle over row

        Args:
            painter (QtGui.QPainter): Painter object
            option (QtGui.QStyleOptionViewItem): Parameters used to draw item
            modelIndex (QtCore.QModelIndex): Index for model data
        """
        if not modelIndex.isValid():
            return super(BaseDelegateItem, self).paint(painter, option, modelIndex)

        item = modelIndex.internalPointer()
        if (isinstance(item, modelItem.FileItem)
                and modelIndex.row() in [const.SceneSplitRows.MASTER_FILE_ROW, const.SceneSplitRows.SPLIT_PARTS_ROW]):

            color = QtGui.QColor(*self.SEPARATOR_COLOR)
            painter.setBrush(QtGui.QBrush(color, QtCore.Qt.SolidPattern))
            painter.setPen(color)

            # draw rectangle
            topLeft = option.rect.topLeft()
            topRight = option.rect.topRight()
            bottomLeft = option.rect.bottomLeft()
            bottomRight = option.rect.bottomRight()
            width = topRight.x() - topLeft.x() + self.SEPARATOR_X_OFFSET
            height = bottomLeft.y() - topLeft.y()
            painter.drawRect(topLeft.x() - self.SEPARATOR_X_OFFSET, topLeft.y(), width, height)

            # draw outline
            outlineColor = QtGui.QColor(*self.SEPARATOR_OUTLINE_COLOR)
            painter.setPen(outlineColor)
            painter.drawLine(topLeft.x() - self.SEPARATOR_X_OFFSET, topLeft.y(), topRight.x(), topRight.y())
            painter.drawLine(bottomLeft.x() - self.SEPARATOR_X_OFFSET, bottomLeft.y(), bottomRight.x(), bottomRight.y())

            if modelIndex.column() == const.SceneSplitColumns.NAME:
                painter.drawLine(topLeft.x() - self.SEPARATOR_X_OFFSET, topLeft.y(),
                                 bottomLeft.x() - self.SEPARATOR_X_OFFSET, bottomLeft.y())
            elif modelIndex.column() == const.SceneSplitColumns.BUG_STATUS:
                painter.drawLine(topRight.x(), topRight.y(),
                                 bottomRight.x(), bottomRight.y())

        return super(BaseDelegateItem, self).paint(painter, option, modelIndex)

    def updateEditorGeometry(self, editor, option, modelIndex):
        """Updates the editor for the item specified by index according to the style option given

        Args:
            editor (QtGui.QWidget): editor widget
            option (QtGui.QStyleOptionViewItem): Parameters used to draw item
            modelIndex (QtCore.QModelIndex): Index for model data
        """
        editor.setGeometry(option.rect)

    def sizeHint(self, option, index):
        """Size hint to delegate

        Args:
            option (QtGui.QStyleOptionViewItem): Parameters used to draw item
            index (QtCore.QModelIndex): Index for model data
        Returns:
            QtCore.QSize: Size hint
        """
        return QtCore.QSize(*self.SIZE_HINT)


class FrameRangeDelegate(BaseDelegateItem):
    """Frame range delegate item representing a frame range"""
    SIZE_HINT = (10, 15)

    def createEditor(self, parent, option, modelIndex):
        """Creates the editor widget based on index

        Args:
            parent (QtGui.QWidget): Parent widget
            option (QtGui.QStyleOptionViewItem): Parameters used to draw
            modelIndex (QtCore.QModelIndex): Index for model data
        Returns:
            QtGui.QWidget: Editor widget
        """
        if not modelIndex.isValid():
            return False

        item = modelIndex.internalPointer()
        editor = FrameRangeEditor(item, parent=parent)

        # get setEditorData to callback
        editor.startRange.editingFinished.connect(self.commitEditor)
        editor.endRange.editingFinished.connect(self.commitEditor)

        return editor

    def setEditorData(self, editor, modelIndex):
        """
        Sets editor data

        Args:
            editor (QtGui.QWidget): Editor widget
            modelIndex (QtCore.QModelIndex): Index for model data
        """
        if not modelIndex.isValid() or not editor:
            return

        # validate the editor range
        frameRange = modelIndex.data()
        editor.setFrameRange(frameRange)

    def commitEditor(self):
        """Triggers commitData callback so setEditorData triggers"""
        editor = self.sender()
        self.commitData.emit(editor.parent())

    def setModelData(self, editor, model, modelIndex):
        """
        Sets Model data

        Args:
            editor (QtGui.QWidget): Editor widget
            model (QtGui.QAbstractItemModel): Item model
            modelIndex (QtCore.QModelIndex): Index for model data
        """
        if not modelIndex.isValid() or not editor:
            return

        item = modelIndex.internalPointer()
        if isinstance(item, modelItem.SplitSceneItem):
            # set range on model item
            frameRange = editor.validateRange()
            model.setData(modelIndex, frameRange)


class SeparatorItemDelegate(BaseDelegateItem):
    """Separator delegate to separate master file and split parts"""
    SIZE_HINT = (50, 20)

    def __init__(self, parent=None):
        super(SeparatorItemDelegate, self).__init__(parent)

    def sizeHint(self, option, modelIndex):
        """Size hint to delegate

        Args:
            option (QtGui.QStyleOptionViewItem): Parameters used to draw item
            modelIndex (QtCore.QModelIndex): Index for model data
        Returns:
            QtCore.QSize: Size hint
        """
        item = modelIndex.data(QtCore.Qt.UserRole)
        if (modelIndex.row() in [const.SceneSplitRows.MASTER_FILE_ROW, const.SceneSplitRows.SPLIT_PARTS_ROW] and
                isinstance(item, modelItem.FileItem)):
            return QtCore.QSize(*self.SIZE_HINT)

        return super(SeparatorItemDelegate, self).sizeHint(option, modelIndex)
