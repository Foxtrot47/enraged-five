import os
import string

from PySide import QtCore, QtGui

from RS import Globals
from RS.PyCoreQt.Models import baseModelItem
from RS.Core.SceneSplitter import settings, util
from RS.Tools.SceneSplitEditor import const


class ResourceItem(baseModelItem.BaseModelItem):
    """Model item representing a resource within the current scene that can be assigned to splits by drag/drop"""
    DISABLED_COLOR = (150, 150, 150)

    def __init__(self, namespace, parent=None):
        super(ResourceItem, self).__init__(parent)
        self._namespace = namespace
        self._enabled = True

    def flags(self, modelIndex):
        """Reimplemented from QAbstractItemModel interface"""
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled

    def columnCount(self, parent=None):
        """Reimplemented from QAbstractItemModel interface"""
        return 2

    def data(self, modelIndex, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface"""
        if not modelIndex.isValid():
            return None

        if role == QtCore.Qt.DisplayRole:
            if modelIndex.column() == const.SceneSplitColumns.NAME:
                return self._namespace
            if modelIndex.column() == const.SceneSplitColumns.TYPE:
                return "Resource"
        elif role == QtCore.Qt.DecorationRole:
            if modelIndex.column() == const.SceneSplitColumns.NAME:
                parentItem = modelIndex.parent().internalPointer()
                if isinstance(parentItem, BaseSceneItem):
                    if self._enabled:
                        return const.Icons.RESOURCE_CHECKED
                    else:
                        return const.Icons.RESOURCE_UNCHECKED
                elif isinstance(parentItem, SplitSceneItem):
                    return const.Icons.RESOURCE

                return None

        elif role == QtCore.Qt.ForegroundRole:
            if modelIndex.column() == const.SceneSplitColumns.NAME:
                if not self._enabled:
                    return QtGui.QColor(*self.DISABLED_COLOR)

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface"""
        return False

    def hasChildren(self, parent=QtCore.QModelIndex()):
        """Reimplemented from QAbstractItemModel interface"""
        return False

    @property
    def enabled(self):
        """The namespace belonging to the asset represented by this item."""
        return self._enabled

    @enabled.setter
    def enabled(self, value):
        """The namespace belonging to the asset represented by this item."""
        self._enabled = bool(value)

    @property
    def namespace(self):
        """The namespace belonging to the asset represented by this item."""
        return self._namespace


class AbstractSceneSectionItem(baseModelItem.BaseModelItem):
    """Abstract base class for items representing FBX files within the split settings model"""

    def __init__(self, parent):
        super(AbstractSceneSectionItem, self).__init__(parent)

        self.createResourceItems()

    def flags(self, modelIndex):
        """Reimplemented from QAbstractItemModel interface"""
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDropEnabled

    def columnCount(self, parent=None):
        """Reimplemented from QAbstractItemModel interface"""
        return 5

    def data(self, modelIndex, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface"""
        if not modelIndex.isValid():
            return None

        if role == QtCore.Qt.DisplayRole:
            if modelIndex.column() == const.SceneSplitColumns.NAME:
                return self.getFilename()
            elif modelIndex.column() == const.SceneSplitColumns.TYPE:
                return self.getType()
            elif modelIndex.column() == const.SceneSplitColumns.BUG_ID:
                return self.getBugId()
            elif modelIndex.column() == const.SceneSplitColumns.BUG_STATUS:
                return self.getBugStatus()
        elif role == QtCore.Qt.DecorationRole:
            if modelIndex.column() == const.SceneSplitColumns.NAME:
                return self.getIcon()

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """Reimplemented from QAbstractItemModel interface"""
        return False

    def appendResourceItemsFromNamespaces(self, namespaceList, updateInternalData=False):
        """
        Creates a child resource item from each namespace in the list

        Args:
            namespaceList (list of str): List of namespaces to create resource items from.
            updateInternalData (bool): Whether to trigger update or not
        """
        for namespace in namespaceList:
            if any(namespace == item.namespace for item in self.childItems):
                continue

            item = ResourceItem(namespace)
            self.appendChild(item, updateInternalData=updateInternalData)

    def updateResourceItemsFromNamespaces(self, usedNamespaces, unusedNamepaces):
        """
        Updates model data on resource items

        Args:
            usedNamespaces (list of str): List of namespaces that are used
            unusedNamepaces (list of str): List of namespaces that are unused
        """
        for resourceItem in self.childItems:
            for namespace in usedNamespaces:
                if resourceItem.namespace == namespace:
                    resourceItem.enabled = False
                    break

            for namespace in unusedNamepaces:
                if resourceItem.namespace == namespace:
                    resourceItem.enabled = True
                    break

    def createResourceItems(self):
        """Creates the resource items owned by this scene"""
        pass

    def getIcon(self):
        """Returns the icon for this item."""
        return None

    def getSuffix(self):
        """Returns the suffix for this scene"""
        return None

    def getFrameRange(self):
        """Returns the framerange for this item"""
        return None

    def getFilename(self):
        """Returns the filename for this scene."""
        fileName = os.path.basename(os.path.splitext(Globals.Application.FBXFileName)[0])
        if not fileName:
            fileName = "<Unsaved FBX>"

        suffix = self.getSuffix()
        if not suffix:
            return "{}.fbx".format(fileName)

        return "{}_{}.fbx".format(fileName, suffix)

    def getType(self):
        """Returns the type name for this item"""
        return None

    def getBugId(self):
        """Returns the bug ID for this scene."""
        return None

    def getBugStatus(self):
        """Returns the bug status for the bug belonging to this scene."""
        bugId = self.getBugId()
        if not bugId:
            return None
        # TODO: Request status from Bugstar - depends on Bugstar library upgrade url:bugstar:5881421
        return None

    def clearChildren(self, updateInternalData=True):
        """Reimplemented from BaseModelItem interface"""
        toRemove = self.childItems[:]
        for item in toRemove:
            self.removeChild(item, updateInternalData=updateInternalData)

    def removeChild(self, item, updateInternalData=True):
        """Reimplemented from BaseModelItem interface"""
        super(AbstractSceneSectionItem, self).removeChild(item)

    def appendChild(self, item, index=-1, updateInternalData=True):
        """Reimplemented from BaseModelItem interface"""
        super(AbstractSceneSectionItem, self).appendChild(item, index=index)


class FileItem(AbstractSceneSectionItem):
    TITLE = ""

    def __init__(self, parent=None):
        super(FileItem, self).__init__(parent)

    def data(self, modelIndex, role=QtCore.Qt.DisplayRole):

        if not modelIndex.isValid():
            return None

        if role == QtCore.Qt.DisplayRole:
            if modelIndex.column() == const.SceneSplitColumns.NAME:
                return self.TITLE

    def flags(self, index):

        if not index.isValid():
            return QtCore.Qt.ItemIsEnabled

        childItem = index.internalPointer()
        if childItem is not self and childItem is not None:
            return childItem.flags(index)

        return QtCore.Qt.ItemIsEnabled

    def columnCount(self, parent=None):
        return 5


class SplitPartsItem(FileItem):
    """Model item representing the split parts section"""
    TITLE = "Split Parts"


class MasterFileItem(FileItem):
    """Model item representing the master file section"""
    TITLE = "Master file"


class BaseSceneItem(AbstractSceneSectionItem):
    """Model item representing the "base" fbx scene in a split settings model"""

    def __init__(self, splitSettings, parent=None):
        self._splitSettings = splitSettings
        super(BaseSceneItem, self).__init__(parent)

    def getIcon(self):
        """Reimplemented from AbstractSceneSectionItem interface"""
        return const.Icons.BASE_FBX

    def getType(self):
        """Reimplemented from AbstractSceneSectionItem interface"""
        return "Base FBX"

    def getBugId(self):
        """Reimplemented from AbstractSceneSectionItem interface"""
        return self._splitSettings.passBugId

    def getFrameRange(self):
        return self._splitSettings.splitFrameRange


class SplitSceneItem(AbstractSceneSectionItem):
    """Model item representing a split fbx scene in a split settings model"""
    EDITABLE_COLUMNS = [const.SceneSplitColumns.NAME,
                        const.SceneSplitColumns.BUG_ID]
    VALID_SUFFIX_CHARACTERS = frozenset("{}{} _-".format(string.ascii_letters, string.digits))

    def __init__(self, splitSectionSettings=None, parent=None):
        if splitSectionSettings is None:
            self._splitSection = settings.SceneSplitSectionSettings()
        else:
            self._splitSection = splitSectionSettings
        super(SplitSceneItem, self).__init__(parent)

    def createResourceItems(self):
        """Reimplemented from AbstractSceneSectionItem interface"""
        self._splitSection.resourceNamespaces.sort()
        self.appendResourceItemsFromNamespaces(self._splitSection.resourceNamespaces, updateInternalData=False)

    def flags(self, modelIndex):
        """Reimplemented from QAbstractItemModel interface"""
        if not modelIndex.isValid():
            return QtCore.Qt.NoItemFlags
        flags = super(SplitSceneItem, self).flags(modelIndex)
        if modelIndex.column() in self.EDITABLE_COLUMNS:
            flags = flags | QtCore.Qt.ItemIsEditable
        return flags

    def data(self, modelIndex, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface"""
        if role == QtCore.Qt.EditRole:
            if modelIndex.column() == const.SceneSplitColumns.NAME:
                # Only edit the suffix part of the name
                return self.getSuffix()

        return super(SplitSceneItem, self).data(modelIndex, role)

    def setData(self, modelIndex, value, role=QtCore.Qt.EditRole):
        """Reimplemented from QAbstractItemModel interface"""
        if not modelIndex.isValid():
            return False

        if role == QtCore.Qt.EditRole:
            if modelIndex.column() == const.SceneSplitColumns.NAME:
                retValue = self.setSuffix(value)
                return retValue

        return super(SplitSceneItem, self).setData(modelIndex, value, role)

    def getSectionSettings(self):
        """
        Returns the settings object represented by this item.

        Returns:
            SplitSectionSettings: The settings object for the split scene represented by this item.
        """
        return self._splitSection

    @classmethod
    def _makeValidFilename(cls, inputString):
        """Strips characters from the string that would be invalid if used as a filename"""
        if not isinstance(inputString, basestring):
            raise TypeError("Filename must be a string type.")

        # Strip invalid characters from start/end
        outputString = inputString.strip().rstrip(". \r\n\t")
        outputString = "".join(char for char in outputString if char in cls.VALID_SUFFIX_CHARACTERS)
        if not outputString:
            # Invalid if empty after strip (e.g. input did not contain any valid file name characters)
            raise ValueError("Unable to convert input string to a valid file name: \"{}\"".format(inputString))

        return outputString

    def getFilename(self):
        """Returns the filename for this scene."""
        fileName = os.path.splitext(super(SplitSceneItem, self).getFilename())[0]
        frameRange = self.getFrameRange()

        return "{}_{}-{}.fbx".format(fileName, frameRange[0], frameRange[1])

    def getIcon(self):
        """Reimplemented from AbstractSceneSectionItem interface"""
        return const.Icons.SPLIT_FBX

    def setSuffix(self, value):
        """Reimplemented from AbstractSceneSectionItem interface"""
        if not value:
            return False

        try:
            value = self._makeValidFilename(value)
        except ValueError:
            return False

        self._splitSection.splitSuffix = value
        return True

    def getSuffix(self):
        """Reimplemented from AbstractSceneSectionItem interface"""
        return self._splitSection.splitSuffix

    def getType(self):
        """Reimplemented from AbstractSceneSectionItem interface"""
        return "Split FBX"

    def getBugId(self):
        """Reimplemented from AbstractSceneSectionItem interface"""
        return self._splitSection.splitSectionBugId

    def getFrameRange(self):
        return self._splitSection.splitFrameRange

    def setFrameRange(self, frameRange):
        self._splitSection.splitFrameRange = frameRange

    def removeChild(self, item, updateInternalData=True):
        """
        Reimplemented from BaseModelItem interface

        Keyword Args:
            updateInternalData (bool): If True the internal settings objects will be updated
                to mirror the changes in the model.
        """
        super(AbstractSceneSectionItem, self).removeChild(item)

        if updateInternalData and isinstance(item, ResourceItem):
            self._splitSection.resourceNamespaces.remove(item.namespace)

    def appendChild(self, item, index=-1, updateInternalData=True):
        """
        Reimplemented from BaseModelItem interface

        Keyword Args:
            updateInternalData (bool): If True the internal settings objects will be updated
                to mirror the changes in the model.
        """
        super(AbstractSceneSectionItem, self).appendChild(item, index=index)

        if updateInternalData and isinstance(item, ResourceItem):
            if index == -1:
                self._splitSection.resourceNamespaces.append(item.namespace)
            else:
                self._splitSection.resourceNamespaces.insert(index, item.namespace)
