from RS.Tools.MeshCuller import MeshCullerDialog

def Run(show=True):
    tool = MeshCullerDialog.MeshCullingUI()

    if show:
        tool.show()
    return tool
