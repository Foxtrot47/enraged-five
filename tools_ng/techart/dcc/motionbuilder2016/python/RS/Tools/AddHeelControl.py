from pyfbsdk import *

import RS.Config

# Location of High Heel FBX file containig models and constraint for control...
HIGH_HEEL_CONTROL_MODEL_FILE = "{0}\\etc\\config\\characters\\Asset_HeelHeightSliderControl_Model.fbx".format( RS.Config.Tool.Path.TechArt )

def Run():
    global HIGH_HEEL_CONTROL_MODEL_FILE

    # Every single character to be inspecting for heel height master control system...
    characters = FBSystem().Scene.Characters
    for character in characters:
        
        print ("CHARACTER:\t\t\t" + character.LongName)
        characterNamespace = character.LongName.split(":")[0]
        print "\tcurrent namespace:\t\t" + characterNamespace
        
        #Check for existance of high heel masters control current character
        heelControlLongName = characterNamespace + ":RECT_HeelHeight"
        heelControlModel = FBFindModelByName(heelControlLongName)
        if heelControlModel:
            #Control does exists, no proceeding in due course
            print ("\tFound heel control:\t\t" + heelControlModel.LongName)
            
                    
        else:
            
            #Control missing: control must create from scene for this character
            print ("\tUnable to find heel control:\t" + heelControlLongName)
            print ("\tCreating New Heel control:\t" + heelControlLongName)
            
            # Find the current character namespace
            fullMoverName =  characterNamespace + ":mover"
            moverModel = FBFindModelByName(fullMoverName)
            
            if moverModel:        
                #import heel control item from FBX source  
                print ("\tFound character Mover:\t" + moverModel.LongName)
                FBApplication().FileAppend( HIGH_HEEL_CONTROL_MODEL_FILE , False )
                
    
                
                # Prefix everything with a namespace
                # heel slider control...
                newHeelControlModel = FBFindModelByLabelName("RECT_HeelHeight")          
                newHeelControlModel.LongName = characterNamespace + ":RECT_HeelHeight"  
                for childModel in newHeelControlModel.Children:
                    childModel.LongName = characterNamespace + ":" + childModel.Name
                    
                #constraints...
                constraints = FBSystem().Scene.Constraints
                for con in constraints:
                    if con.LongName == "HeelHeight":
                        con.LongName = characterNamespace + ":" + con.Name 
                        
                #heel proxy box models...
                #EO_R_Foot
                #EO_R_Proxy
                rightFootProxyModel = FBFindModelByLabelName("EO_R_Proxy")          
                rightFootProxyModel.LongName = characterNamespace + ":EO_R_Proxy"  
                leftFootProxyModel = FBFindModelByLabelName("EO_L_Proxy")          
                leftFootProxyModel.LongName = characterNamespace + ":EO_L_Proxy"  
                
                
                
                
                #attach controls and proxies to the character...    
                newHeelControlModel.Parent = moverModel
                newHeelControlModel.Translation.Data = FBVector3d(-0.5, 0, -0.8)
            
                rightFootModel = FBFindModelByName(characterNamespace + ":SKEL_R_Foot")
                rightFootProxyModel.Parent = rightFootModel
                rightFootProxyModel.Translation.Data = FBVector3d(0.13, -0.12, 0)
                
                leftFootModel = FBFindModelByName(characterNamespace + ":SKEL_L_Foot")
                leftFootProxyModel.Parent = leftFootModel
                leftFootProxyModel.Translation.Data = FBVector3d(0.13, -0.12, 0)
                
                
                
                
                #Add all this objects to an group of heel heights...
                # Creating Group One, which contains one cube
                heelGroup = FBGroup ("High Heels")
                heelGroup.LongName = characterNamespace + ":" + heelGroup.Name
                # Adding the object to the group
                heelGroup.ConnectSrc( rightFootProxyModel ) 
                heelGroup.ConnectSrc( leftFootProxyModel ) 
                
                # Group under parent character group
                groups = FBSystem().Scene.Groups
                for group in groups:
                    print group.LongName
                    if group.LongName == characterNamespace + ":" + characterNamespace:
                            group.ConnectSrc(heelGroup)
                            break
                
                # Setting the attributes
                heelGroup.Show = False
                heelGroup.Pickable = False
                heelGroup.Transformable = False
                
    
    
            else:
                print "Could not find mover: " + fullMoverName
        print "\n"   # Next character

