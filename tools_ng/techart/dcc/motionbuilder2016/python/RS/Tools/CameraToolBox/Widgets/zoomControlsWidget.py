# Widget for the Lens and Zoom FOV spinner
from functools import partial

from PySide import QtCore, QtGui

from RS.Utils.Scene import Property as RsProperty
from RS.Tools.CameraToolBox.PyCoreQt.Widgets import checkboxButton
from RS.Tools.CameraToolBox import const
from RS.Tools.CameraToolBox.Widgets import cameraLensDial, cameraSpinNDial, cameraIntSpinNDial
from RS.Tools.CameraToolBox.PyCore.Decorators import noRecursive


class ZoomControlsWidget(QtGui.QWidget):
    """
    This widget contains the lens and zoom FOV spinner
    One Signal:
        CameraDataChanged - Emmited when the camera data is changed, emits the camera object
                            the index value thats changed and the new value
    """
    CameraDataChanged = QtCore.Signal(object, object, object)
    
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(ZoomControlsWidget, self).__init__(parent=parent)
        self._activeCamera = None
        self._propertyMonitor = RsProperty.PropertyMonitor()
        self.SetupUi()

    def SetupUi(self):
        """
        Intenal Method
        
        Setups up the UI and all the connections.
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()

        # Create Widgets
        lensWidget = QtGui.QWidget()
        lensLabel = QtGui.QLabel("Lens")
        self._lensFinder = cameraLensDial.CameraLensDial()

        # Generate the spin widgets - Slightly weird as its moddled after the camera controls setup
        name, cameraControlIndex, minValue, maxValue = "Zoom", const.CameraControlPropertiesIndex.zoom, 0.0, 180.0
        layout = QtGui.QVBoxLayout()
        self._spinWidget = QtGui.QWidget()

        self._zoomDial = cameraSpinNDial.CameraSpinNDial(const.fovDefaultValue, minValue, maxValue)
        self._zoomButton = QtGui.QPushButton(name)
        
        layout.addWidget(self._zoomButton)
        layout.addWidget(self._zoomDial)

        self._spinWidget.setLayout(layout)

        self._zoomDial.ValueChanged.connect(partial(self._handleSpinner, self._zoomDial, cameraControlIndex))
        self._zoomButton.pressed.connect(partial(self._handleDialButton, cameraControlIndex))

        # Set Properties
        lensLabel.setAlignment(QtCore.Qt.AlignHCenter)
        self.setMaximumWidth(200)

        # Assign Layouts
        mainLayout.addWidget(lensLabel)
        mainLayout.addWidget(self._lensFinder)
        mainLayout.addWidget(self._spinWidget)

        # Set Layouts
        self.setLayout(mainLayout)

        # Connect Signals
        self._lensFinder.ValueChanged.connect(partial(self._handleLensChange, const.CameraControlPropertiesIndex.lens))
        self.setEnabled(False)
    
    @noRecursive.noRecursive
    def _handleLensChange(self, prop, value):
        """
        Internal Method
        
        Handle the lens changing, update the property in the MoBu Camera
        
        Prop (str) The property name to edit
        value (str) The new Value to set
        """
        cameaAttra = self._activeCamera.PropertyList.Find(prop)
        cameaAttra.Data = cameaAttra.GetEnumStringList().IndexOf(str(value))
        self.CameraDataChanged.emit(self._activeCamera, prop, str(value))
        self._spinWidget.setEnabled("zoom" in value.lower())
        if self._spinWidget.isEnabled() is False:
            self._zoomDial.setValue(const.fovDefaultValue)

    @noRecursive.noRecursive
    def _handleSpinner(self, ui, prop, value):
        """
        Internal Method
        
        Handle a spinner value change, update the property in the MoBu Camera
        
        Prop (str) The property name to edit
        value (float) The new value to set
        """
        cameraAttra = self._activeCamera.PropertyList.Find(prop)
        if type(cameraAttra.Data) is int:
            value = int(value)
        
        cameraAttra.Data = value
        self.CameraDataChanged.emit(self._activeCamera, prop, value)
        ui.setValue(cameraAttra.Data)

    def _handleDialButton(self, prop):
        """
        Internal Method
        
        Select the FCurve property for the active camera
        """
        if self._activeCamera is None:
            return
        
        # Kristine Added - Should we not deselecting all the other properties, if not remove three lines again.
        for propAll in self._activeCamera.PropertyList:
            if propAll.IsAnimatable ():
                propAll.SetFocus(False)

        fProp = self._activeCamera.PropertyList.Find(prop)
        if (fProp):
            fProp.SetFocus(True) # Need to use this method to actually select the property in MotionBuilder

    def SetActiveCamera(self, camera):
        """
        Set the active camera to the given camera.
        This will populate the UI controls
        
        Takes a FBCamera Object
        """
        self._propertyMonitor.Clear()
        self._propertyMonitor.Deactivate()
        
        lens = camera.PropertyList.Find(const.CameraControlPropertiesIndex.lens)

        if lens  is None:
            self.setEnabled(False)
            self._activeCamera = None
            return
        self._activeCamera = camera
        self.setEnabled(True)
        
        fProp = camera.PropertyList.Find(const.CameraControlPropertiesIndex.zoom)
        self._zoomDial.setValue(fProp.Data)
        self._propertyMonitor.Add(fProp, self._handleMobuSpinnerPropertyUpdate, extraArgs={"uiElement": self._zoomDial, "prop": const.CameraControlPropertiesIndex.zoom})

        self._propertyMonitor.Add(lens, self._handleMobuLensPropertyUpdate, extraArgs={"uiElement": self._lensFinder, "prop": const.CameraControlPropertiesIndex.lens})
        self._lensFinder.CurrentLens = lens.AsString()
        
        # activate callback
        self._propertyMonitor.Activate()

    @noRecursive.noRecursive
    def _handleMobuSpinnerPropertyUpdate(self, targetProperty, previousValue, uiElement=None, prop=None):
        if uiElement is None:
            return
        uiElement.setValue(targetProperty.Data)
        self.CameraDataChanged.emit(self._activeCamera, prop, targetProperty.Data)

    @noRecursive.noRecursive
    def _handleMobuLensPropertyUpdate(self, targetProperty, previousValue, uiElement=None, prop=None):
        if uiElement is None:
            return
        
        self._lensFinder.CurrentLens = targetProperty.AsString()
        self.CameraDataChanged.emit(self._activeCamera, prop, targetProperty.AsString())

if __name__ == "__main__":
    win = ZoomControlsWidget()
    win.show()