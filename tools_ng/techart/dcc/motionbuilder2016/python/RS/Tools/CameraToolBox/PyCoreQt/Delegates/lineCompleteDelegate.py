import re

from PySide import QtCore, QtGui


class RegexCompleter(QtGui.QCompleter):
    def __init__(self, items=None, parent=None):
        super(RegexCompleter, self).__init__(parent)
        self.sourceModel = None

        if items is not None:
            model = QtGui.QStringListModel()
            model.setStringList(sorted(items))
            self.setModel(model)

    def setModel(self, model):
        self.sourceModel = model
        super(RegexCompleter, self).setModel(self.sourceModel)

    def setFilterRegExp(self, text):
        # Create the proxy model for filtering
        text = re.sub('[A-Z]+', lambda m: m.group(0).lower(), text)
        proxyModel = QtGui.QSortFilterProxyModel()
        proxyModel.setFilterRegExp(text)
        proxyModel.setSourceModel(self.sourceModel)
        super(RegexCompleter, self).setModel(proxyModel)

    def splitPath(self, text):
        self.setFilterRegExp(str(text))
        return ""


class LineCompleteDelegate(QtGui.QItemDelegate):
    '''
    Creates a line edit delgate with an auto complete, items are set to contain a string only

    args:   List - list of strings
    '''
    def __init__(self, items, parent=None):
        super(LineCompleteDelegate, self).__init__(parent=parent)
        self._items = items
        self._items.sort()

    def createEditor(self, parent, option, index):
        '''
        Creates QCombobox and sets the items - just sets a string
        '''
        editor = QtGui.QLineEdit(parent)
        editor.setCompleter(RegexCompleter(self._items))
        return editor

    def setEditorData(self, editor, index):
        '''
        Gets the value of the current item and sets it to the text entry
        '''
        editor.setText(index.model().data(index, QtCore.Qt.DisplayRole))

    def setModelData(self, editor, model, index):
        '''
        Takes the current text value and sets that back to the model
        '''
        model.setData(index, editor.text(), QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        '''
        Sets the combobox's position
        '''
        editor.setGeometry(option.rect)

