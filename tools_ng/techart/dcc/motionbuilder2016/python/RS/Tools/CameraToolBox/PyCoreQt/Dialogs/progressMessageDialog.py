"""
Contains the ProgressMessageDialog class.
"""
from PySide import QtGui, QtCore


class ProgressMessageDialog(QtGui.QDialog):
    """
    Generic dialog for displaying a progress message while waiting for a blocking process to complete.
    """

    def __init__(self, title, message, parent=None):
        super(ProgressMessageDialog, self).__init__(parent=parent)
        self._isClosed = False
        self._message = message
        self._title = title
        self.setupUi()

    def setupUi(self):
        # Widgets
        self._label = QtGui.QLabel(self._message)

        # Configure widgets
        self.setWindowTitle(self._title)
        self.setWindowModality(QtCore.Qt.NonModal)

        # Layouts
        mainLayout = QtGui.QHBoxLayout()
        mainLayout.addWidget(self._label)
        self.setLayout(mainLayout)

    # TODO: revisit the repaint workaround to see if we can remove it.
    def show(self):
        super(ProgressMessageDialog, self).show()

        # Workaround to force the QLabel to draw.
        for _ in xrange(5):
            self.repaint()
            self._label.repaint()  # In some instances, repainting the dialog is not enough on show.
            QtCore.QCoreApplication.processEvents()

    # TODO: revisit the repaint workaround to see if we can remove it.
    def setMessage(self, message):
        self._label.setText(message)

        # Workaround to force the QLabel to draw.
        self.repaint()
        QtCore.QCoreApplication.processEvents()

    def close(self):
        if not self._isClosed:
            super(ProgressMessageDialog, self).close()
            self._isClosed = True


if __name__ == '__main__':
    import sys
    import time

    app = QtGui.QApplication(sys.argv)

    # wid = QtGui.QWidget()
    # wid.show()  # Parent widget has to be shown and not hidden. Might be an override for this.
    # dialog = ProgressMessageDialog("Tool Name", "Processing something...", parent=wid)

    dialog = ProgressMessageDialog("Tool Name", "Processing something...")
    dialog.show()
    time.sleep(2)
    dialog.setMessage("Almost done...")
    time.sleep(2)
    dialog.hide()

    sys.exit(app.exec_())