import time


def timeIt(func):
    """Prints the time it took for the decorated function to execute

    Args:
        func (function): function to time

    Returns:
        results from the function that was decorated
    """

    def wrapper(*args, **kwargs):
        start = time.time()
        try:
            return func(*args, **kwargs)
        finally:
            print time.time() - start

    return wrapper
