"""
Deprecated Decorator
"""


def deprecated(msg):
    """
    Decorator. 

    Example:

        @deprecated("Use newMethod instead")
        def oldMethod(a):
            return a + 1
            
        def newMethod(a):
            return a + 2

    """
    def depreacted_dec(func):
        def func_wrapper(*args, **kwargs):
            print "DEPRECATED:: Method '{0}' has been Deprecated. {1}".format(func.__name__, msg)
            return func(*args, **kwargs)
        return func_wrapper
    return depreacted_dec
