import os
import sys
import math

from PySide import QtCore, QtGui

from RS import Config


class CustomIntDial(QtGui.QWidget):
    """
    Spinner Widget to allow for a better graphical interface to the dial
    """

    valueChanged = QtCore.Signal(float)
    
    def __init__(self, defaultValue=0, minimumValue=0, maximumValue=1.0, numMiniLights=20, parent=None):
        super(CustomIntDial, self).__init__(parent=parent)
        
        self.__dialBg = QtGui.QImage(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera","dial","dial_dark.png")))
        self.__dialKnob = QtGui.QImage(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera","dial","dial_knob.png")))
        self.__dialMiniLight = QtGui.QImage(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera","dial","dial_dark_mini_light.png")))
        self.__dialMiniLightDisabled = QtGui.QImage(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera","dial","dial_dark_mini_light_Disabled.png")))

        # track if left mouse button is pressed
        self.__leftPressed = False   
        
        # scale factor for minilights when adjustign the window
        self.__miniLightScaleFactor = 0.4
        
        # radius from widget center of where to place the mini lights
        self.__miniLightRadius = 0.75 
        
        # number of lights we want to generate
        self.__numMiniLights = numMiniLights
        
        # min & max values for the dial
        self.MinimumValue = minimumValue
        self.MaximumValue = maximumValue

        # current value
        self._value = defaultValue
        
    def minimumSizeHint(self):
        return QtCore.QSize(50,50)

    @property
    def Value(self):
        return int(self._value)
        
    @Value.setter
    def Value(self, newValue):
        if self._value == newValue:
            return
        self._value = newValue
        self.repaint()
        self.valueChanged.emit(self.Value)

    def setValue(self, newValue):
        self.Value = newValue
        
    def setMaximum(self, newValue):
        self.MaximumValue = newValue
        if self._value > newValue:
            self._value = newValue
        self.repaint()
        
    def setMinimum(self, newValue):
        self.MinimumValue = newValue
        if self._value < newValue:
            self._value = newValue
        self.repaint

    def UpdateCurrentValue(self, pt):
        # finds the current value based on where the mouse pointer is
        world = QtCore.QPoint(self.width() * 0.5, self.height() * 0.5)
        
        # world = center of widget(0,0)
        x = pt.x() - world.x()
        y = pt.y() - world.y()
        
        # calculates an angle based on the x,y positions
        # 'math.atan2(y, x)' calculates angle in radians
        # '(180.0 / math.pi)' converts to degrees
        # result is in degrees
        theta = math.atan2(y, x) *(180.0 / math.pi)
        if theta < 0:
            theta = 360 + theta
            
        if theta > 360:
            theta = 0

        # converting the angle to give us a value between min / max range
        prevValue = self._value
        
        difference = self.MaximumValue-self.MinimumValue
        value = (theta / 360.0) *(difference) + self.MinimumValue
        delta = prevValue - value
        if abs(delta) >= abs(difference) - ((20.0/100) * difference):
            if delta > 0:
                value = self.MaximumValue
            else:
                value = self.MinimumValue
        self.Value = round(value)

    def GetCurrentValueInDegrees(self):
        normalizedVal = (float(self.Value) - self.MinimumValue) /(self.MaximumValue - self.MinimumValue)
        return normalizedVal * 360.0
    
    def mousePressEvent(self, event):
        
        # gettingt he position of where you have clicked the left mouse button
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            self.UpdateCurrentValue(event.pos())
            
            # tracking if the button has been clicked 
            # we have to do this ourself as 'QtCore.Qt.MouseButton.LeftButton' doesnt get recognised in the mouse move event
            self.__leftPressed = True
        
        # getting the built-in functionality of the mouse press events
        QtGui.QWidget.mousePressEvent(self, event)
        
    def mouseMoveEvent(self, event):
        if self.__leftPressed:
            self.UpdateCurrentValue(event.pos())
            
            # force a repaint event to happen(reruns 'paintEvent' def each time)
            self.update()
        
        # getting the built-in functionality of the mouse move events
        QtGui.QWidget.mouseMoveEvent(self, event)
        
    def mouseReleaseEvent(self, event):
        
        # tracking turned off when you release the left mouse button
        self.__leftPressed = False
        
        # getting the built-in functionality of the mouse release events
        QtGui.QWidget.mouseReleaseEvent(self, event) 

    def paintEvent(self, event):
        
        painter = QtGui.QPainter()
    
        painter.begin(self)
        
        # getting center point of the widget
        widgetWidthCenter = self.width() * 0.5
        widgetHeightCenter = self.height() * 0.5
        
        # scaling the images to proportionally fit with the window
        if self.height() <= self.width():
            
            # scaling the image to the height with a smooth transform
            imgBg = self.__dialBg.scaledToHeight(self.height(), mode = QtCore.Qt.TransformationMode.SmoothTransformation)
            imgKnob = self.__dialKnob.scaledToHeight(self.height(), mode = QtCore.Qt.TransformationMode.SmoothTransformation)
            
            # scaling the radius with the window for the lights
            # 0.75 is a constant
            # /100 normalises back down to be between 0 & 1(0 is the center, 1 is the top)
            radius = (self.__miniLightRadius * self.height()) / 100.0
            
            # scales the mini light images to proportionally fit with the window
            miniLightScaleFactor = (self.__miniLightScaleFactor * self.height()) / 100.0
            
        else:
            imgBg = self.__dialBg.scaledToWidth(self.width(), mode = QtCore.Qt.TransformationMode.SmoothTransformation)
            imgKnob = self.__dialKnob.scaledToWidth(self.width(), mode = QtCore.Qt.TransformationMode.SmoothTransformation)
            
            radius = (self.__miniLightRadius * self.width()) / 100.0
            miniLightScaleFactor = (self.__miniLightScaleFactor * self.width()) / 100.0

        # start our drawing in the center of the widget
        painter.translate(widgetWidthCenter, widgetHeightCenter)
        #painter.rotate(-90) 
        
        # img
        # get the image's width and height
        imageWidth = imgBg.width()
        imageHeight = imgBg.height()

        # move the image to be int he center of the widget.  
        # the pivot point is at the top left of the image, so it draws offset - the pivot point of all images always start at the top left of an image
        imageWidthOffset = -(imageWidth * 0.5)
        imageHeightOffset = -(imageHeight * 0.5)
        
        # draw the image onto the widget
        painter.drawImage(imageWidthOffset, imageHeightOffset, imgBg)
        
        # Mini lights
        imgMiniLight = self.__dialMiniLight
        imgMiniLightDisabled = self.__dialMiniLightDisabled
        
        # scaling the mini lights
        imgMiniLight = imgMiniLight.scaled(imgMiniLight.width() * miniLightScaleFactor, imgMiniLight.height() * miniLightScaleFactor, mode = QtCore.Qt.TransformationMode.SmoothTransformation)
        imgMiniLightDisabled = imgMiniLightDisabled.scaled(imgMiniLightDisabled.width() * miniLightScaleFactor, imgMiniLightDisabled.height() * miniLightScaleFactor, mode = QtCore.Qt.TransformationMode.SmoothTransformation)
        
        # image's width & height
        miniKnobWidth = imgMiniLight.width()
        miniKnobHeight = imgMiniLight.height()
                
        # space in degrees between each light
        # we convert to an int as we cannot step with a float
        step = int(360 / self.__numMiniLights)
        
        # current value in degrees
        currentVal = self.GetCurrentValueInDegrees()
        
        # calculating where the x & y are at 
        # we can only find the position on the circle in degrees, but to get the x, y we need to convert the degree result to radians
        # we need the result in x, y to then draw the image in pixel space
        # expand out with radius
        for angle in range(0, 359, step):
            x = radius * math.degrees(math.cos(math.radians(angle)))
            y = radius * math.degrees(math.sin(math.radians(angle)))

            # mini lights disabled by default
            currentLight = imgMiniLightDisabled
            
            # if the current value is greater than the current angle of the loop, then draw the mini light on
            if currentVal >= angle and currentVal != 0:
                currentLight = imgMiniLight
                
            # draw the image
            # take away the width and height halved to center the image in the space  
            painter.drawImage(QtCore.QPoint(x -(miniKnobWidth * 0.5), y -(miniKnobHeight * 0.5)), currentLight)
            
        # imgKnob
        imageWidth = imgKnob.width()
        imageHeight = imgKnob.height()

        # get the center of the image 
        imageWidthOffset = -(imageWidth * 0.50)
        imageHeightOffset = -(imageHeight * 0.50) 
        
        # this allows us to rotate the image
        # value is the knob position
        # value's default is from 0 - 100
        # we are working in 360deg so we need to scale it to 360 so the dial can spin around full circle
        painter.rotate(currentVal)
        
        # draw the image
        painter.drawImage(QtCore.QPoint(imageWidthOffset, imageHeightOffset), imgKnob)

        painter.end()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    
    def handleValueChanged(newValue):
        print newValue
    
    spinner = CustomIntDial(0.5, 0.0, 0.5)
    spinner.valueChanged.connect(handleValueChanged)
    spinner.show()
    sys.exit(app.exec_())
