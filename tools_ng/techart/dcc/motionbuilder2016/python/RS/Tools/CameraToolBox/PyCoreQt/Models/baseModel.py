__DoNotReload__ = True  # Module is NOT safe to be dynamciy reloaded

from PySide import QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class BaseModel(QtCore.QAbstractItemModel):
    """
    Base Model class to cover all the boiler plate code that QtCode requires
    """
    def __init__(self, parent=None):
        super(BaseModel, self).__init__(parent=parent)
        self.reset()

    def reset(self):
        self.modelAboutToBeReset.emit()
        super(BaseModel, self).reset()
        self.rootItem = None
        self.refresh()
        self.modelReset.emit()

    @property
    def childItems(self):
        return self.children()

    def getHeadings(self):
        return [""]

    def refresh(self):
        self.rootItem = textModelItem.TextModelItem(self.getHeadings(), parent=self)
        self.setupModelData(self.rootItem)

    def clear(self):
        children = self.rootItem.childItems
        self.beginRemoveRows(QtCore.QModelIndex(), 0, len(children))
        self.rootItem.clearChildren()
        for child in children:
            child.parentItem = None
        self.endRemoveRows()

    def columnCount(self, parent):
        if parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return self.rootItem.columnCount()

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid():
            return None
        item = index.internalPointer()
        if item == self:
            return None

        if item is None:
            item = self.rootItem.child(index.row())

        return item.data(index, role)

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        if not index.isValid():
            return False
        item = index.internalPointer()

        return item.setData(index, value, role)

    def flags(self, index):
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        return self.rootItem.flags(index)

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.rootItem.data(self.createIndex(0, section))

        return None

    def canFetchMore(self, parent):
        if not parent.isValid():
            return self.rootItem.fetchMore(parent) or False
        return parent.internalPointer().canFetchMore(parent) or False

    def fetchMore(self, parent):
        if not parent.isValid():
            return self.rootItem.fetchMore(parent)
        return parent.internalPointer().fetchMore(parent)

    def index(self, row, column, parent=QtCore.QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        if parentItem is None:
            parentItem = self.rootItem

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def parent(self, index=QtCore.QModelIndex()):
        if not index.isValid():
            return QtCore.QModelIndex()

        childItem = index.internalPointer()
        if childItem is None:
            return QtCore.QModelIndex()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QtCore.QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent=QtCore.QModelIndex()):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer() or self.rootItem

        return parentItem.childCount()

    def setupModelData(self, parent):
        raise NotImplementedError("Reimplement this!")

    def supportedDragActions(self):
        """ Items can be moved and copied """
        return super(BaseModel, self).supportedDragActions()

    def supportedDropActions(self):
        """ Items can be moved and copied """
        return super(BaseModel, self).supportedDropActions()

    def mimeTypes(self):
        """
        The MimeType for the encoded data.

        This method should be reimplemented to allow for other mime types to be accepted by the model
        """
        raise NotImplementedError("Reimplement this!")

    def mimeData(self, indices):
        """
        Encode serialized data from the item at the given index into a QMimeData object.

        This method is called by Qt when a drag operation on model items start from the model the item belongs to.
        This method should be reimplemented to achieve desired behavior when data is grabbed
        """
        raise NotImplementedError("Reimplement this!")

    def dropMimeData(self, mimedata, action, row, column, parentIndex):
        """
        Drops dragged mime data into a model

        This method is called by the model where the item is being dropped to.

        Arguments:
            mimedata (QtGui.QMimeData): mime data being dropped
            action (QtCore.Qt.DropAction): drop action being performed
            row (int): row where the data is being dropped to
            column (int): column where the data is being dropped to
            parentIndex (QtCore.QModelIndex): model index of the item where the data is being dropped to

        Return:
            True
        """
        raise NotImplementedError("Reimplement this!")

    def moveItems(self, parentIndex, indicies, row=None, copy=False):
        """
        Moves an items within the tree model.

        This method is meant to be used within dropMimeData method

        Arguments:
            parentIndex (QtCore.QModelIndex): the parent model index
            items (DataModelItem): DataModelItem items to move to a new parent
            row (int): row to insert items at
            copy (boolean): should the items not be removed from their original
            drop (boolean): should the items be added under the new parent

        """
        row = row if row is not None else -1
        dataChanged = False
        for index in indicies:
            # This is to stop items from being parented to themselves
            same = parentIndex == index
            grandParentIndex = parentIndex.parent()

            while grandParentIndex.isValid() and not same:
                same = grandParentIndex == index
                grandParentIndex = grandParentIndex.parent()

            if same:
                continue

            # Check that this model has an index at the row and column of the index being moved
            # and if the two indexes are the same.
            # This is to check if the index being moved is from another model.
            indexRow = index.row()
            hasIndex = self.hasIndex(index.row(), index.column(), parentIndex)
            isParent = self.index(index.row(), index.column(), parentIndex) == index

            if hasIndex and isParent:
                # Moving to position that will leave the item in the same place seems to crash the tool
                if row not in (indexRow, indexRow + 1):
                    self.moveItem(index, row, parentIndex)

            elif copy:
                # Copy the item
                self.copyItem(index, row, parentIndex)

            else:
                self.dropItem(index, row, parentIndex)
            dataChanged = True
        return dataChanged

    def copyItem(self, index, row, parentIndex):
        """
        Logic for when an item is dropped on another item but the original item should not be deleted/removed

        This method needs to be reimplemented in inherited classes

        Arguments:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped
        """
        raise NotImplementedError("Reimplement this!")

    def dropItem(self, index, row, parentIndex):
        """
        Logic for when an item is dropped on top of another item

        This method needs to be reimplemented in inherited classes

        Arguments:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped
        """

        raise NotImplementedError("Reimplement this!")

    def moveItem(self, index, row, parentIndex):
        """
        Logic for when an item is dropped on its parent to shift its position in the view hierarchy

        This method needs to be reimplemented in inherited classes

        Arguments:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped
        """
        raise NotImplementedError("Reimplement this!")

    def _itemFromIndex(self, index):
        """ Returns the item instance from a QModelIndex. """
        return index.internalPointer() if index.isValid() else self.rootItem

    def _indexFromItem(self, item):
        """ Returns the QModelIndex associated with an item. """
        parentItem = item.parent()
        if parentItem is None:
            return QtCore.QModelIndex()

        return self.createIndex(item.row(), 0, item)
