from PySide import QtCore, QtGui


class CheckboxButon(QtGui.QPushButton):
    """
    CheckboxButton class to allow buttons to be toggleable
    """
    def __init__(self, text='', checked=False, downColour=None, parent=None):
        """
        Constructor
        """
        self._colour = downColour or QtGui.QColor(0, 255, 127)
        super(CheckboxButon, self).__init__(text, parent=parent)
        self.setCheckable(True)
        self.toggled.connect(self.setDown)
        self.setChecked(checked)

    def setColour(self, newColour):
        """
        Set the colour for when the push button is pressed down
        
        args:
            newColour (QColor): The new colour
        """
        self._colour = newColour
        
    def getColour(self):
        """
        Get the current colour for when the push button is pressed down
        
        Returns:
            QColor of the current colour
        """
        return self._colour

    def setDown(self, value):
        """
        Re-Implemented
        
        Sets if the button is down or not.
        Not the same as checked, this is just the down state
        """
        super(CheckboxButon, self).setDown(value)
        styleSheet = " "
        if value:
            styleSheet = """
            QPushButton {
                background-color: rgb(%s, %s, %s);
            }
            """ % (self._colour.red(), self._colour.green(), self._colour.blue())
        self.setStyleSheet(styleSheet)


def _handleStateChanged(val):
    print 'Button is now %s' % val

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    win = CheckboxButon('Hello World', True)

    win.toggled.connect(_handleStateChanged)
    win.show()
    sys.exit(app.exec_())
