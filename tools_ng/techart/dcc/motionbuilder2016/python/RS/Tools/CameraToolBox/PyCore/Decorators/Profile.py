"""
Profiles a function
"""

import os
import pstats
import cProfile
import tempfile
import functools
import pythonidelib


class Profile(object):
    """ Decorator that profiles the execution of the decorated function """
    name = "name"  # sort by the name of the module
    file = "file"  # sort by the name of the file
    time = "time"  # sort by the time it took to run one call to this function
    calls = "calls"  # sort by the number of times the function was called
    primitiveCalls = "pcalls"  # sort by primitive call count
    cumulativeTime = "cumtime"  # sort by total time it took to run all calls to this function
    line = "line"  # sort by line number
    nameFileLine = "nfl"  # sort by name , filename and line number
    standard = "stdname"  # sort by standard name

    def __init__(self, path=None, sortBy='cumulative', print_=True):
        """
        Constructor

        Arguments:
            path (string): path to save output too, default is None
            sortBy (string): order to sort profile stats by
            print_ (boolean): print the results to the console, default is False
        """
        self._path = path or os.path.join(tempfile.tempdir, r"profileDump.prof")
        self._sortBy = sortBy
        self._print = print_

    def __call__(self, func):
        """
         Overrides built-in method
         Arguments:
             func (function): decorated function
        """
        @functools.wraps(func)
        def inner(*args, **kwargs):
            with self:
                result = func(*args, **kwargs)
            return result
        return inner

    def __enter__(self):
        """
        Enable
        """
        self.prof = cProfile.Profile()
        self.prof.enable()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.prof.disable()
        # create file
        self.prof.dump_stats(self._path)
        stats = pstats.Stats(self._path)
        stats.strip_dirs()
        stats.sort_stats(self._sortBy)
        if self._print:
            stats.print_stats()
            pythonidelib.FlushOutput()
