"""
This module provides a Widget to display messages on screen, in a non interactable and unintrusive way.
It can be useful to prompt the user with some information after a button has been pressed, or an operation has completed.

Usage:

import time
from RS.Tools.CameraToolBox.PyCoreQt.Dialogs import popupWidget

message = "This is a Popup Message!"
pop = popupWidget.TimedPopupWidget(text = message, prefixType=0)
time.sleep(2)

message = "This is a Popup Error!"
pop = popupWidget.TimedPopupWidget(text = message, prefixType=2)
time.sleep(2)

message = "<font color='orange'><i>This is a <\font><font color='pink'>Popup Message with HTML!<\font><\i>"
pop = popupWidget.TimedPopupWidget(text = message)

"""

__author__ = "francesco.torelli"

from PySide import QtCore, QtGui
from RS.Tools.UI import Application


class PrefixType(object):
    """
    Store information regarding the type of prefix.
    Based on this type, a prefix might be added to the popup message.
    """

    NOPREFIX = -1
    INFO = 0
    WARNING = 1
    ERROR = 2


class TimedPopupManager(object):
    """
    A manager class to manage existing popups.
    Each popup is added to the manager during its __init__, so that when it's shown, the manager can rearrange
    existing popups to better fit on screen and not overlap with each other.
    """

    _instances = []

    @classmethod
    def append(cls, instance):
        """
        Append a popup instance to the _instances list, and connect the popup signals related to showing/closing
        the popup to the relevant functions.
        Args:
            instance (TimedPopupWidget): the popup to append.
        """
        cls._instances.append(instance)
        instance.closed.connect(cls.remove)
        instance.shown.connect(cls.updateStack)

    @classmethod
    def remove(cls, instance):
        """
        Removes an instance from the _instances list.
        Args:
            instance (TimedPopupWidget): the popup to remove.
        """
        if instance in cls._instances:
            cls._instances.remove(instance)

    @classmethod
    def updateStack(cls, newPopup):
        """
        Cycle through all the existing popup instances, and update each popup geometry
        so that it does not overlap with any other visible popup.
        """
        offset = 10
        newRect = newPopup.geometry()
        for popup in cls._instances:
            # we only check against popup which are visible
            if popup != newPopup and popup.isVisible():
                rect = popup.geometry()

                # if the new popup intersects with this existing one, move the existing popup out of the way
                if newRect.intersects(rect):
                    x = rect.x()
                    y = newRect.y() + newRect.height() + offset
                    rect.moveTo(QtCore.QPoint(x, y))

                    # update the geometry of this popup
                    popup.setGeometry(rect)
                    # recurse to make sure after moving this popup, it is not overlapping
                    # with any of the other popups.
                    cls.updateStack(popup)


class TimedPopupWidget(QtGui.QDialog):
    """
    Create a transparent Popup to print a message on screen!

    Known issues: FadeIn and FadeOut won't work as expected if you display your widget while code is still running,
    due to the QT event loop not being updated.
    """

    DEFAULT_BACKGROUND = QtGui.QColor(30, 30, 30, 200)
    DEFAULT_FOREGROUND = QtGui.QColor(230, 230, 230, 180)
    DEFAULT_SIZE = 18

    PREFIX_INFO = {
        PrefixType.INFO: ("INFO: ", QtGui.QColor(QtCore.Qt.green)),
        PrefixType.WARNING: ("WARNING: ", QtGui.QColor(QtCore.Qt.yellow)),
        PrefixType.ERROR: ("ERROR: ", QtGui.QColor(QtCore.Qt.red)),
    }

    shown = QtCore.Signal(object)
    closed = QtCore.Signal(object)

    def __init__(
        self,
        parent=None,
        text="",
        timer=2000,
        fadeIn=0,
        fadeOut=0,
        pos=None,
        prefixType=PrefixType.NOPREFIX,
        safeToProcessEvents=True,
        show=True,
    ):
        """
        Initialize the widget and show it if required.

        Args:
            parent (QWidget): parent of the widget. This should usually be MoBu main window.
            text (str): the text to be displayed in the Popup
            timer (float): how long the Popup should linger on screen before being destroyed (in milliseconds)
            fadeIn (float): how long the fadeIn animation should last (in milliseconds)
            fadeOut (float): how long the fadeOut animation should last (in milliseconds)
            pos (QPos): Where the top left corner of the popup should appear. If not specified, it will use the center
                        of the screen.
            prefixType (PrefixType): What prefix should be used for this popup (INFO, WARNING, etc..)
            safeToProcessEvents (bool): Should it try to process QT Event Loop?
            show (bool): If the widget should be shown or not right after initialization.
        """

        parent = parent or Application.GetMainWindow()
        super(TimedPopupWidget, self).__init__(parent=parent)

        # append the current instance to the PopupManager, so that it's position on screen can be managed.
        TimedPopupManager.append(self)

        # make sure the widget is transparent to mouse events and its deleted on close
        self.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents, True)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)

        # make sure the window is transparent and stays always on top
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)
        self.setWindowFlags(
            self.windowFlags() | QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.FramelessWindowHint | QtCore.Qt.ToolTip
        )

        text = text or "Message"
        self._prefixType = prefixType
        self._safeToProcessEvents = safeToProcessEvents

        # setup our timers
        self.setTimer(timer)
        self.setFadeIn(fadeIn)
        self.setFadeOut(fadeOut)

        self._document = QtGui.QTextDocument()

        # set the colors
        p = self.palette()
        p.setColor(self.backgroundRole(), self.DEFAULT_BACKGROUND)
        p.setColor(self.foregroundRole(), self.DEFAULT_FOREGROUND)
        self.setPalette(p)

        # get the message input and setup a default font
        font = self.font()
        font.setPixelSize(self.DEFAULT_SIZE)
        font.setBold(True)
        self.setFont(font)

        # set the origin position
        self.position = pos

        # set the message in the widget
        self.setText(text)

        self.effect = QtGui.QGraphicsOpacityEffect()
        self.effect.setOpacity(1)
        self.setGraphicsEffect(self.effect)
        self.animation = QtCore.QPropertyAnimation(self.effect, "opacity", parent=self)

        self._shown = True

        if show:
            self.show()

    def setText(self, text):
        """
        Set the text to be to be displayed by the popup,
        and update the the size of the popup based on the HTML document.

        Args:
            text (str): the text to be displayed in the popup
        """
        self._text = text
        self.setHTMLDocument(text)

    def text(self):
        """
        Return the text to be displayed as a message
        """
        return self._text

    def prefixText(self):
        return self._prefixText

    def setPrefixType(self, prefixType=-1):
        """
        Change the prefixType

        Args:
            prefixType (PrefixType): The type of prefix to be used (INFO, WARNING, etc)
        """
        self._prefixType = prefixType
        self.setHTMLDocument(self._text)

    def setPrefixText(self):
        """
        Define what kind of prefix to use for this message, if any has to be displayed.
        """

        self._prefixText, self._prefixColor = self.PREFIX_INFO.get(self._prefixType, ("", None))

        if self._prefixText:
            self._prefixRich = "<font color='{0}'>{1}</font>".format(self._prefixColor.name(), self._prefixText)
        else:
            self._prefixRich = ""

    def setHTMLDocument(self, text):
        """
        Create a QTextDocument to read our text in HTML format, so that we can support HTML inputs
        and also prefix some extra colored information.
        Args:
            text (str): the text to be used in the HTML document

        """

        self._document = QtGui.QTextDocument()
        self.setPrefixText()

        self._document.setDefaultFont(self.font())

        # make sure the text is encapsulated in the correct HTML color.
        # Also, convert any python new line \n in a HTML new line.
        foregroundColor = self.palette().color(self.foregroundRole()).name()
        richText = "<font color='{0}'>{1}</font>".format(foregroundColor, "<br />".join(text.split("\n")))

        self._richText = self._prefixRich + richText
        self._document.setHtml(self._richText)

        # set the alignment to be central.
        block = self._document.lastBlock().blockFormat()
        block.setAlignment(QtCore.Qt.AlignCenter)
        cursor = QtGui.QTextCursor(self._document)
        cursor.setBlockFormat(block)

        self.setSizeFromFont()

    def setSizeFromFont(self):
        """
        Update the size of the popup using the size of the QTextDocument to calculate width and height
        """

        size = self._document.size()

        # set the fixed size of our widget
        self.setFixedSize(QtCore.QSize(size.width() + 30, size.height() + 20))

        # update the position of the widget, based on input. Either the Passed QPos, or the center of the active Screen.
        dialogGeometry = self.geometry()
        if self.position:
            dialogGeometry.moveTo(self.position)
        else:
            dialogGeometry.moveCenter(QtGui.QApplication.desktop().availableGeometry().center())
        self.setGeometry(dialogGeometry)

    def setFont(self, *args):
        """
        Reimplement the setFont method, so when the font is changed we can also update our QTextDocument
        """
        result = super(TimedPopupWidget, self).setFont(*args)
        self._document.setDefaultFont(self.font())
        return result

    def timer(self):
        """
        Returns:
            float: the amount of time the widget will stay on screen (in milliseconds)
        """
        return self._timer

    def setTimer(self, val):
        """
        Args:
            val ( float ): set how long the widget should be displayed on screen (in milliseconds)
        """
        self._timer = val

    def fadeIn(self):
        """
        Returns:
            float: the amount of time the fadeIn animation will last (in milliseconds)
        """
        return self._fadeIn

    def setFadeIn(self, val):
        """
        Args:
            val (float ): set how long the fadeIn animation will last (in milliseconds)
        """
        self._fadeIn = val if val < self.timer() else 0

    def fadeOut(self):
        """
        Returns:
            float: the amount of time the fadeOut animation will last (in milliseconds)
        """
        return self._fadeOut

    def setFadeOut(self, val):
        """
        Args:
            val (float ): set how long the fadeOut animation will last (in seconds)
        """
        remainingTime = self.timer() - self.fadeIn()
        self._fadeOut = val if val < remainingTime else remainingTime

    def paintEvent(self, event):
        """
        Override paintEvent to display our text the way we want it.
        """

        # get current window size
        rect = self.rect()
        painter = QtGui.QPainter()
        painter.begin(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing, True)

        # draw popup
        background = self.palette().color(self.backgroundRole())
        painter.setPen(background)
        painter.setBrush(background)

        painter.drawRoundedRect(rect, 15, 15)

        # draw contents of popup
        self._document.setTextWidth(rect.width())
        painter.translate(0, rect.y() + (rect.height() - self._document.size().height()) / 2)
        self._document.drawContents(painter, rect)

        painter.end()

    def show(self, *args, **kwargs):
        """
        Override the Show method, so that once the widget is shown, it can fade in/out.
        """
        # setup a timer to fadeOut and close the widget
        if self._shown:

            self._shown = False

            self.shown.emit(self)

            timeout = self._timer - self._fadeOut
            QtCore.QTimer.singleShot(timeout, self.createFadeOut)

            # need to process events twice, to make sure the widgets are properly shown/hidden while python runs.
            if self._safeToProcessEvents:
                QtGui.QApplication.instance().processEvents(QtCore.QEventLoop.ExcludeUserInputEvents)

            # show the widget
            super(TimedPopupWidget, self).show()
            self.createFadeIn()

            if self._safeToProcessEvents:
                QtGui.QApplication.instance().processEvents(QtCore.QEventLoop.ExcludeUserInputEvents)

        else:
            super(TimedPopupWidget, self).show()

    def createFadeIn(self):
        """
        Create an animation to animate the Widget opacity from 0 to 1 when it is shown.
        The length of animation is controller by _fadeIn
        """
        if self._fadeIn:
            self.animation.setDuration(self._fadeIn)
            self.animation.setStartValue(0)
            self.animation.setEndValue(1)
            self.animation.setEasingCurve(QtCore.QEasingCurve.InBack)
            self.animation.start()

    def createFadeOut(self):
        """
        Create an animation to animate the Widget opacity from 1 to 0 before it is hidden.
        The length of animation is controller by _fadeOut
        """
        self.animation.stop()
        if self._fadeOut < 1:
            self.close()
            return
        self.animation.setDuration(self._fadeOut)
        self.animation.setStartValue(1)
        self.animation.setEndValue(0)
        self.animation.setEasingCurve(QtCore.QEasingCurve.OutBack)
        self.animation.start(QtCore.QPropertyAnimation.DeleteWhenStopped)
        self.animation.finished.connect(self.close)

    def close(self, *args, **kwargs):
        """
        Override the Close method, to process QEvents to make sure it closes even when a script is still running
        """
        self.closed.emit(self)
        super(TimedPopupWidget, self).close(*args, **kwargs)
        if self._safeToProcessEvents:
            QtGui.QApplication.instance().processEvents(QtCore.QEventLoop.ExcludeUserInputEvents)
