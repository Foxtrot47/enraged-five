"""
UI Control to allow users to click and drag to get a numerical value
"""
import math
from PySide import QtGui


class DragableSpinBox(QtGui.QSpinBox):
    """
    Interger spin box control
    """
    def __init__(self, parent=None):
        self._eventHandlerDict  = {
                         QtGui.QMouseEvent.MouseButtonPress: self.mousePressEvent,
                         QtGui.QMouseEvent.MouseButtonRelease: self.mouseReleaseEvent,
                         QtGui.QMouseEvent.MouseMove: self.mouseMoveEvent,
        }
        
        super(DragableSpinBox, self).__init__(parent=parent)
        lineEdit = self.findChild(QtGui.QLineEdit, "qt_spinbox_lineedit")
        lineEdit.installEventFilter(self)
        
        self.setMouseTracking(True)
        self._isDown = False
        self._lastPos = None
        self._initalPos = None

    def eventFilter(self, target, event):
        for eventType, eventMethod in self._eventHandlerDict.iteritems():
            if event.type() == eventType:
                eventMethod(event)
        return False

    def mousePressEvent(self, event):
        self._isDown = True
        self._lastPos = event.pos()
        self._initalPos = event.pos()
        super(DragableSpinBox, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self._isDown = False
        self._lastPos = None
        self._initalPos = None
        super(DragableSpinBox, self).mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        if self._isDown is False:
            return
        currentPoint = event.pos()
        delta = math.sqrt(
                     math.pow((self._lastPos.x() - currentPoint.x()), 2) +
                     math.pow((self._lastPos.y() - currentPoint.y()), 2)
                    )
        if self._initalPos.y() < currentPoint.y():
            delta *= -1
        self.setValue(self.value() + delta)
        self._lastPos = currentPoint
        
        
if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
        
    from PySide import QtCore
    spinner = QtGui.QSlider()#DragableSpinBox()
    spinner.setStyleSheet("""
        QSlider::groove:horizontal {
            border: 1px solid #999999;
            height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */
            background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);
            margin: 2px 0;
        }
        
        QSlider::handle:horizontal {
            background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);
            border: 1px solid #5c5c5c;
            width: 18px;
            margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */
            border-radius: 3px;
        }
    """)
    spinner.setMaximum(5000.0)
    spinner.setMinimum(0.0)
    spinner.setOrientation(QtCore.Qt.Horizontal)
    spinner.show()
    sys.exit(app.exec_())

