import functools
from PySide import QtCore, QtGui


class SpinBoxDelegate(QtGui.QItemDelegate):

    valueChanged = QtCore.Signal(QtCore.QModelIndex, object)

    def __init__(self, parent=None):
        super(SpinBoxDelegate, self).__init__(parent=parent)
        self._min = 0
        self._max = 99

    def createEditor(self, parent, option, index):
        '''
        Creates Qspinbox and sets the min and max values (int)
        '''        
        editor = QtGui.QSpinBox(parent)
        editor.setMinimum(self._min)
        editor.setMaximum(self._max)
        editor.valueChanged.connect(functools.partial(self.valueChanged.emit, index))
        return editor

    def setEditorData(self, spinBox, index):
        '''
        Reads the value from the Model Data and sets the current value (int) of the spinbox
        
        Model Data -> ComboBox
        '''
        value = index.model().data(index, QtCore.Qt.EditRole)
        spinBox.setValue(value)

    def setModelData(self, spinBox, model, index):
        '''
        Reads the current value (int) of the spinbox and pushes that information to the Model Data
        
        ComboBox -> Model SetData
        '''
        spinBox.interpretText()
        value = spinBox.value()

        model.setData(index, value, QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        editor.setGeometry(option.rect)

    def min(self):
        ''' Minimum value for the spinbox '''
        return self._min

    def setMin(self, value):
        '''
        Sets the minimum value for the spinbox
        
        Arguments:
            value (int): value to set as the minimum for the spinbox
        '''
        self._min = value

    def max(self):
        ''' Maximum value for the spinbox '''
        return self._max

    def setMax(self, value):
        '''
        Sets the maximum value for the spinbox
    
        Arguments:
            value (int): value to set as the maximum for the spinbox
        '''
        self._max = value