"""
Resizeable message box for displaying lengthy messages
"""
from PySide import QtGui, QtCore

from RS.Tools.Animation.Anim2Fbx.Widgets import CollapsableWidget


class MessageBox(QtGui.QWidget):

    _defaultComment = "Add message to the user here"
    Information = QtGui.QStyle.SP_MessageBoxInformation
    Warning = QtGui.QStyle.SP_MessageBoxWarning

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
        """
        super(MessageBox, self).__init__(parent=parent)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setWindowFlags(QtCore.Qt.Dialog)

        self._set = False
        self._width = 0
        self._previousHeight = None

        layout = QtGui.QVBoxLayout()
        textLayout = QtGui.QVBoxLayout()

        self._descriptionLabel = QtGui.QLabel()
        self._errorTextEdit = QtGui.QTextEdit()
        self._commentTextEdit = QtGui.QTextEdit()
        self._errorTextEdit.setReadOnly(True)

        self._commentTextEdit.setText(self._defaultComment)
        self._commentTextEdit.setMinimumHeight(50)
        self.icon = QtGui.QLabel()
        self.setIcon(self.Information)
        self._button = QtGui.QPushButton("Ok")
        self._button.setIconSize(QtCore.QSize(289, 69))
        self._button.setFixedHeight(25)
        self._button.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        self._button.pressed.connect(self.close)

        collapsableWidget = CollapsableWidget.CollapsableWidget()
        collapsableWidget.setLength(50)
        collapsableWidget.addWidget(self._errorTextEdit)
        collapsableWidget.preclicked.connect(self.storePreviousSize)
        collapsableWidget.clicked.connect(self.showError)
        collapsableWidget.setText("Show Details")
        collapsableWidget.setClickedText("Hide Details")
        self._collapsableWidget = collapsableWidget

        horizontalLayout = QtGui.QHBoxLayout()
        horizontalLayout.addWidget(self.icon)
        horizontalLayout.addWidget(self._descriptionLabel, 1)
        horizontalLayout.setSpacing(10)
        horizontalLayout.setContentsMargins(10, 5, 10, 5)

        layout.addLayout(horizontalLayout, 0)
        layout.addWidget(collapsableWidget, 0)
        layout.addWidget(self._commentTextEdit, 2)
        layout.addWidget(self._button)

        textLayout.setSpacing(4)
        textLayout.setContentsMargins(4, 4, 4, 0)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self._commentTextEdit.setVisible(False)
        self.setLayout(layout)
        self.setWindowTitle("Rockstar - Tools Error")
        self.setStyleSheet("QLabel {"
                           "font-size: 14px; "
                           "qproperty-alignment: AlignCenter;"
                           "}"
                           "QPushButton {"
                           "font: bold 12px;"
                           "qproperty-alignment: AlignCenter;"
                           "}")

    def showError(self, visible):
        """
        Resizes the window so that the full error is shown

        Arguments:
            visible (boolean): is the error visible
        """
        if not visible:
            self.resize(self.width(), self._previousHeight)

    def storePreviousSize(self, visible):
        """
        Stores the height of the widget before the error was visible

        Arguments:
            visible (boolean): is the error visible
        """
        if not visible:
            self._previousHeight = self.height()

    def description(self):
        """ The description of the dialog """
        return self._descriptionLabel.text()

    def setDescripion(self, description):
        """
        Sets the description for the dialog

        Arguments:
            description (string): the description to set
        """
        self._descriptionLabel.setText(description)

    def buttonText(self):
        """ Label of the button """
        return self._button.text()

    def setButtonText(self, text):
        """
        Sets the text of the button
        Arguments:
            text (string): string to set as the label of the button
        """
        self._button.setText(text)

    def text(self):
        """ Error being shown to the user  """
        self._errorTextEdit.text()

    def setText(self, text):
        """
        Sets the text/error as html to show to the user

        Arguments:
            text (string): text/html that contains the error to show to the user
        """
        self._errorTextEdit.insertPlainText(text)
        if not text:
            self._collapsableWidget.setVisible(False)

    def comment(self):
        """ Comment from the comment field """
        return self._commentTextEdit.toPlainText()

    def setComment(self, text):
        """
        Sets a comment on the comment field

        Arguments:
            text (string): comment to set on the comment field
        """
        self._commentTextEdit.setText(text)

    def setIcon(self, iconType):
        """
        Sets the icon for the message box

        Arguments:
            iconType (QtGui.QStyle.StandardPixmap): icon type to use
        """
        if isinstance(iconType, QtGui.QStyle.StandardPixmap):
            style = QtGui.QApplication.instance().style()
            icon = style.standardIcon(iconType)
            self.icon.setPixmap(icon.pixmap(50, 50))

    def isCommentVisible(self):
        """
        Is the comment edit box visible

        Return:
            Bool
        """
        return self._commentTextEdit.isVisible()

    def setCommentVisible(self, value):
        """
        Sets the visibility of the comment edit box

        Arguments:
            value (bool): visibility of the comment edit box
        """
        self._commentTextEdit.setVisible(value)

    @classmethod
    def information(cls, parent=None, title="Information",
                  description="Information",  text="", buttonText="Ok",
                  suppressComment=True):
        """
        Show the message box with the information icon

        Arguments:
            parent (QtGui.QWidget): parent widget
            title (string): title for the message box
            description(string): text to show the user
            text (string): text to show the user when the user expands the "show detail" medal
            buttonText (string): text to display for the ok cancel
            suppressComment (boolean): hide the comment box where users can write feedback

        Return:
            tuple(boolean, string)
        """

        dialog = MessageBox(parent)
        dialog.setWindowTitle(title)
        dialog.setDescripion(description)
        dialog.setText(text)
        dialog.setButtonText(buttonText)
        dialog.setCommentVisible(not suppressComment)
        # 40 is roughly the size of the comment text widget
        dialog.resize(382, 110 + (40 * int(not suppressComment)))
        dialog.show()
        # if you don't store the window instance somewhere that persists outside the scope of the function block
        # then python's garbage collection deletes the instance & by extension the window
        cls._dialog = dialog
        return True, dialog.comment()

    @classmethod
    def warning(cls, parent=None, title="Warning",
                description="Warning",  text="", buttonText="Ok",
                suppressComment=True):
        """
        Show the message box with the information icon

        Arguments:
            parent (QtGui.QWidget): parent widget
            title (string): title for the message box
            description(string): text to show the user
            text (string): text to show the user when the user expands the "show detail" medal
            buttonText (string): text to display for the ok cancel
            suppressComment (boolean): hide the comment box where users can write feedback

        Return:
            tuple(boolean, string)
        """

        dialog = MessageBox(parent)
        dialog.setIcon(MessageBox.Warning)
        dialog.setWindowTitle(title)
        dialog.setDescripion(description)
        dialog.setText(text)
        dialog.setButtonText(buttonText)
        dialog.setCommentVisible(not suppressComment)
        # 40 is roughly the size of the comment text widget
        dialog.resize(382, 110 + (40 * int(not suppressComment)))
        dialog.show()
        # if you don't store the window instance somewhere that persists outside the scope of the function block
        # then python's garbage collection deletes the instance & by extension the window
        cls._dialog = dialog
        return True, dialog.comment()