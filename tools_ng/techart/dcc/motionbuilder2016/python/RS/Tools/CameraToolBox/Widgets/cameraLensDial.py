"""
CameraLens Dial Widget
"""
from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox import const


class CameraLensDial(QtGui.QWidget):
    """
    Spinner Widget to allow for a better graphical interface to the dial, like a camera lens

    One Signal:
        ValueChanged - Emmited when the current spinner value changes, emits a string
    """

    ValueChanged = QtCore.Signal(str)

    def __init__(self, defaultLens=None, lensTypes=None, parent=None):
        """
        Constructor

        defaultLens (str) The default item to select
        lensTypes (list of str) A list of items to represent the items in the dial
        """
        super(CameraLensDial, self).__init__(parent=parent)

        self._lensTypes = lensTypes or const.lensTypes

        self._spinnerOffset = 0
        self._spacing = 75

        self.__leftPressed = False

        # current value
        self._currentLens = defaultLens
        self._lastPosition = None

        if defaultLens is not None:
            self.CurrentLens = defaultLens

        self.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding))

    @property
    def CurrentLens(self):
        """
        Returns the the current Lens as a string

        Example:
            derp = CameraLensDial('Test', ['Test', 'Test1', 'Test2'])
            print derp.CurrentLens
        """
        return self._lensTypes[self.GetIndex()]

    @CurrentLens.setter
    def CurrentLens(self, newValue):
        """
        Sets the current lens in the dial. Takes a string

        Example:
            derp = CameraLensDial('Test', ['Test', 'Test1', 'Test2'])
            derp.CurrentLens = 'Test2'

        """
        try:
            lensIdx = self._lensTypes.index(newValue)
        except ValueError:
            raise ValueError("{0} is not a valid lense time".format(newValue))
        self._SnapTo(lensIdx * -1)
        self.repaint()
        self.ValueChanged.emit(self.CurrentLens)

    def _UpdateSpinner(self, position):
        """
        Internal Method

        Update the spinner based of the XY position. Position expects a QPoint
        """
        posY = position.y()
        if self._lastPosition is None:
            self._lastPosition = posY

        delta = self._lastPosition - posY
        self._lastPosition = posY
        newSpinnerOffset = self._spinnerOffset
        maxValue = self._spacing * (len(self._lensTypes) - 1) * -1

        if self._spinnerOffset - delta <= maxValue:
            newSpinnerOffset = maxValue

        elif self._spinnerOffset - delta >= 0:
            newSpinnerOffset = 0
        else:
            newSpinnerOffset -= delta

        self._spinnerOffset = newSpinnerOffset

    def _SnapTo(self, index):
        """
        Internal Method

        Snap the current render offset to the item index, taking into account the spacing
        """
        self._spinnerOffset = index * self._spacing
        self.repaint()
        self.ValueChanged.emit(self.CurrentLens)

    def _handleSnapTo(self):
        """
        Internal Method

        Handle the snap part of the mouse release, to snap to the cloest item
        """
        self._SnapTo(self._internalGetIndex())

    def _internalGetIndex(self):
        """
        Internal Method

        Get the current item index, by using the render offset and the spacing
        Returns a negative number
        """
        return round(self._spinnerOffset / float(self._spacing))

    def GetIndex(self):
        """
        Get the current selected item's index
        """
        return abs(int(self._internalGetIndex()))

    def mousePressEvent(self, event):
        """
        ReImplemented from Qt
        Mouse Press Event
        """
        # gettingt he position of where you have clicked the left mouse button
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            self._UpdateSpinner(event.pos())

            # tracking if the button has been clicked
            # we have to do this ourself as 'QtCore.Qt.MouseButton.LeftButton' doesnt get recognised in the mouse move event
            self.__leftPressed = True

        # getting the built-in functionality of the mouse press events
        QtGui.QWidget.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        """
        ReImplemented from Qt
        Mouse Move Event
        """
        if self.__leftPressed:
            self._UpdateSpinner(event.pos())

            # force a repaint event to happen ( reruns 'paintEvent' def each time )
            self.update()

        # getting the built-in functionality of the mouse move events
        QtGui.QWidget.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        """
        ReImplemented from Qt
        Mouse Release Event
        """
        # tracking turned off when you release the left mouse button
        self.__leftPressed = False
        self._lastPosition = None
        self._handleSnapTo()
        # getting the built-in functionality of the mouse release events
        QtGui.QWidget.mouseReleaseEvent(self, event)

    def paintEvent(self, event):
        """
        ReImplemented from Qt
        Paint Event

        This is where we do all the painting for the widget, the text, the offsets
        and the dot
        """
        painter = QtGui.QPainter()
        painter.begin(self)

        middle = self.height() * 0.5
        spliiterSize = 30
        splitter = self.width() - spliiterSize

        blackPen = QtGui.QPen(QtCore.Qt.black)
        blackBrush = QtGui.QBrush(QtCore.Qt.black)
        whitePen = QtGui.QPen(QtCore.Qt.white)
        whiteBrush = QtGui.QBrush(QtCore.Qt.white)
        grayPen = QtGui.QPen(QtCore.Qt.darkGray)
        grayBrush = QtGui.QBrush(QtCore.Qt.darkGray)

        painter.setPen(blackPen)
        painter.setBrush(blackBrush)
        painter.drawRect(QtCore.QRectF(0, 0, self.width(), self.height()))

        # Paint Splitter Line
        painter.setPen(grayPen)
        painter.drawLine(splitter, 0, splitter, self.height())

        # Paint Middle Dot
        if self.isEnabled():
            painter.setPen(whitePen)
            painter.setBrush(whiteBrush)
        else:
            painter.setPen(grayPen)
            painter.setBrush(grayBrush)

        painter.drawEllipse(QtCore.QRectF(splitter + 10, middle - 5, 10, 10))

        # Paint the numbers
        lensScrollerWidth = (self.width() - spliiterSize)
        painter.setFont(QtGui.QFont('Decorative', 12))
        for idx in xrange(len(self._lensTypes)):
            lens = self._lensTypes[idx]

            textRect = QtCore.QRect(0, (idx * self._spacing) + self._spinnerOffset + middle - 10, lensScrollerWidth, 20)
            painter.drawText(textRect, QtCore.Qt.AlignCenter, lens)
        painter.end()

    def wheelEvent(self, event):
        """
        ReImplemented from Qt
        Mouse Wheel Event
        """
        if not self.isEnabled():
            return

        if event.delta() < 0:
            newIdx = self.GetIndex() + 1
        else:
            newIdx = self.GetIndex() - 1

        # Checks to ensure we're not going over or under max, min
        lensCount = len(self._lensTypes) - 1
        if newIdx > lensCount:
            newIdx = lensCount
        elif newIdx < 0:
            newIdx = 0

        lensIdx = newIdx
        self._SnapTo(lensIdx * -1)
        self.repaint()
        self.ValueChanged.emit(self.CurrentLens)


if __name__ == "__main__":
    def handleValueChanged(newValue):
        print newValue

    spinner = CameraLensDial("75mm")
    spinner.ValueChanged.connect(handleValueChanged)
    spinner.show()
