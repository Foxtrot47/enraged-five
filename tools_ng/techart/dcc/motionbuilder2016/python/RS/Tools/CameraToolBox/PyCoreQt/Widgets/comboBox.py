"""
Contains the ComboBox wigdet.
"""
from PySide import QtCore, QtGui


class ComboBox(QtGui.QComboBox):
    """
    Subclass of QComboBox with clicked signal added. 

    This is a better approach than adding an eventfilter, as it covers all 
    the triggerable events and is cleaner to use.
    """
    clicked = QtCore.Signal()

    def showPopup(self):
        self.clicked.emit()
        super(ComboBox, self).showPopup()