"""
UI Control to allow users to click and drag to get a numerical value
"""
import math
from PySide import QtGui, QtCore


class DragableDoubleSpinBox(QtGui.QDoubleSpinBox):
    """
    Float spin box control
    """
    def __init__(self, parent=None, increment=None, useDoubleClick=False):
        self._eventHandlerDict  = {
            QtCore.QEvent.MouseButtonDblClick: self.mouseDoubleClickEvent,
            QtGui.QMouseEvent.MouseButtonPress: self.mousePressEvent,
            QtGui.QMouseEvent.MouseButtonRelease: self.mouseReleaseEvent,
            QtGui.QMouseEvent.MouseMove: self.mouseMoveEvent,
        }
        
        super(DragableDoubleSpinBox, self).__init__(parent=parent)
        lineEdit = self.findChild(QtGui.QLineEdit, "qt_spinbox_lineedit")
        lineEdit.installEventFilter(self)
        
        self.setMouseTracking(True)
        self._isDown = False
        self._lastPos = None
        self._initalPos = None
        self._useDoubleClick = useDoubleClick
        self._increment = increment if increment and isinstance(increment, float) else 1.0
        self.setSingleStep(self._increment)

    def eventFilter(self, target, event):
        for eventType, eventMethod in self._eventHandlerDict.iteritems():
            if event.type() == eventType:
                eventMethod(event)
        return False

    def mouseDoubleClickEvent(self, event):
        if self._useDoubleClick:
            self._isDown = True
            self._lastPos = event.pos()
            self._initalPos = event.pos()
        super(DragableDoubleSpinBox, self).mouseDoubleClickEvent(event)

    def mousePressEvent(self, event):
        self._isDown = True
        self._lastPos = event.pos()
        self._initalPos = event.pos()
        super(DragableDoubleSpinBox, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self._isDown = False
        self._lastPos = None
        self._initalPos = None
        super(DragableDoubleSpinBox, self).mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        if self._isDown is False:
            return

        self.findChild(QtGui.QLineEdit, "qt_spinbox_lineedit").deselect()

        currentPoint = event.pos()

        xDif = currentPoint.x() - self._lastPos.x()
        yDif = -(currentPoint.y() - self._lastPos.y())
        greaterDif = xDif if abs(xDif) > abs(yDif) else yDif

        delta = self._increment if greaterDif > 0 else (self._increment * -1)

        self.setValue(self.value() + delta)
        self._lastPos = currentPoint


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    
    spinner = DragableDoubleSpinBox()
    spinner.show()
    sys.exit(app.exec_())