"""
Widget that displays a list of the current takes in the scene
"""
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS.Core.Scene.models import takeModelTypes


class TakeSelectionView(QtGui.QTreeView):
    """
    Widget that displays a list of the current takes in the scene so user
    """
    selectionChangedSignal = QtCore.Signal(object)

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget

        """
        super(TakeSelectionView, self).__init__(parent=parent)

        model = takeModelTypes.TakeModel()
        model.enableCheckBox(False)
        self.setModel(model)
        self.setSelectionMode(self.ExtendedSelection)

        currentTakeIndex = [model.index(index, 0) for index in xrange(model.rowCount())
                            if model.index(index, 0).data(QtCore.Qt.UserRole) == mobu.FBSystem().CurrentTake]

        self.selectionModel().select(currentTakeIndex[0], QtGui.QItemSelectionModel.Select)

    def selectedTakes(self):
        """
        The takes selected through the view
        """
        return [item.data(QtCore.Qt.UserRole)
                for range in self.selectionModel().selection()
                for item in range.indexes()]

    def selectionChanged(self, selected, deselected):
        """
        emits the selection changed signal with the currently selected takes on the widget

        Arguments:
            selected (QtGui.QItemSelection): the newly selected items
            deselected (QtGui.QItemSelection): the deselected items
        """
        super(TakeSelectionView, self).selectionChanged(selected, deselected)
        self.selectionChangedSignal.emit(self.selectedTakes())

    def exec_(self, title="Select Takes"):
        """
        Shows the tree view as a dialog that returns the selected takes from the view

        Arguments:
            title (string): title for the window

        Return:
            list; list of the selected takes
        """
        dialog = QtGui.QDialog()
        dialog.setWindowTitle(title)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self)
        okButton = QtGui.QPushButton("Select")
        cancelButton = QtGui.QPushButton("Cancel")

        okButton.pressed.connect(dialog.accept)
        cancelButton.pressed.connect(dialog.reject)

        horizontalLayout = QtGui.QHBoxLayout()
        horizontalLayout.addWidget(okButton)
        horizontalLayout.addWidget(cancelButton)
        horizontalLayout.setSpacing(0)

        layout.addLayout(horizontalLayout)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        dialog.setLayout(layout)

        dialog.exec_()

        if dialog.result():
            return self.selectedTakes()
        return []

    @staticmethod
    def dialog(title="Select Takes"):
        """
        Creates a takes tree view as a dialog that returns the selected takes

        Arguments:
            title (string): title for the window

        Return:
            list; list of the selected takes
        """
        tree = TakeSelectionView()
        return tree.exec_(title=title)
