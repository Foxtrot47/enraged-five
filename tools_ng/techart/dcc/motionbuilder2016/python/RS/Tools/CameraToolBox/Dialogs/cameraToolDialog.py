"""
CameraToolDialog putting the CameraToolWidget into a dialog and adding actions and menu buttons
"""
import os

import unbind
from PySide import QtCore, QtGui

import pyfbsdk as mobu
from RS import Config
from RS.Tools.CameraToolBox import const
from RS.Tools.CameraToolBox.Widgets import cameraControlsWidget, shakeControlsWidget, \
                                           sceneCameraExplorer, depthOfFieldControlsWidget, takeSelectionView
from RS.Tools.CameraToolBox.Dialogs import createCameraDialog, copyTakeSelectorDialog
from RS import Globals
from RS.Globals import System as RsSystem
from RS.Globals import Scene as RsScene
from RS.Globals import Callbacks as RsCallbacks
from RS.Core.Camera import LibGTA5, Lib as CamLib
from RS.Core.Camera import CameraLocker, SaveCameras, Switcher, CaptureRender, CamUtils, MarkerTools
from RS.Core.Camera.KeyTools import camKeyTasks
from RS.Tools import UI
from RS.Tools.UI import Application
# from RS.Tools.RemoteCamera import RemoteCamera
from RS.Utils import Scene
from RS.Utils.Scene import Component
from RS.Tools.CameraToolBox.PyCore.Decorators import noRecursive


class CameraToolDialog(UI.QtMainWindowBase):
    """
    The CameraToolDialog wrappes the CameraToolWidget into a dialog and adding actions and menu buttons
    """

    CameraSelected = QtCore.Signal(object)

    def __init__(self, parent=None):
        """
        Constructor
        """
        self.__isDeleted = False
        self._toolName = "CamBox"
        self._settings = QtCore.QSettings("RockstarSettings", "CameraToolBoxWidget")
        self._autoSwitchToCamera = False
        self._autoChangeingSelection = False
        self._autoShowCameraPlanes = False
        self._docWidgets = {}
        self._activeCamera = None
        super(CameraToolDialog, self).__init__(title=self._toolName, dockable=True)
        self.SetupUi()
        self.setStyleSheet("font: 10pt;")
        self.dockWidget().closeEvent = self.closeEvent
        
    def showEvent(self, event):
        """
        ReImplement
        Show event, where we read the qsettings to get the last set settings
        """
        RsCallbacks.OnFileExit.Add(self._handleUnregisterCallback)
        RsCallbacks.OnChange.Add(self._handleCameraCallback)
        
        self._dock = self.dockWidget()
        self._dock.resize(QtCore.QSize(730, 400))

        self._dock.restoreGeometry(self._settings.value("geometry"));
        self.restoreState(self._settings.value("windowState"));

        self._autoSwitchToCamera = self._strToBool(self._settings.value("autoSwitchToCamera", "True"))
        self._autoShowCameraPlanes = self._strToBool(self._settings.value("autoShowCameraPlanes", "False"))
        self._autoChangeingSelection = self._strToBool(self._settings.value("keepInSync", "True"))

        try:
            self.ac_enableAutoSwitchingViewToCamera.setChecked(self._autoSwitchToCamera)
            self.ac_enableAutoShowCameraPlanes.setChecked(self._autoShowCameraPlanes)
            self.ac_keepInSync.setChecked(self._autoChangeingSelection)
        except:
            pass

        camLockObj = CameraLocker.LockManager()
        camLockObj.setAllCamLocks(False)
        # CameraCleaner.FixCamNames()
        
        vcams = CamUtils.GetUndupedVcams()
        if vcams:
            msgTxt = "{} unduped vcams detected - dupe these and remove set cams?".format(len(vcams))
            result = QtGui.QMessageBox.warning(self, "Warning", msgTxt, QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
            if result == QtGui.QMessageBox.Ok:
                CamUtils.DupeRtcsToRsCams()


        gameControl = self._docWidgets.get("Game Controls")
        if gameControl is not None:
            gameControl.raise_()

    def _strToBool(self, inputString):
        """
        Helper method to convert a string into a bool value.

        Example:
            _strToBool('True') # Return True
            _strToBool('true') # Return True
            _strToBool('False') # Return False
            _strToBool('false') # Return False
            _strToBool('derp') # Will Raise
        """
        if inputString.lower() == 'true':
            return True
        elif inputString.lower() == 'false':
            return False
        raise ValueError("Value {0} cannot be converted to bool".format(inputString))

    def closeEvent(self, event=None):
        """
        ReImplement
        Hide event, where we save the qsettings
        """
        self.__isDeleted = True
        self._settings.setValue("geometry", self._dock.saveGeometry());
        self._settings.setValue("windowState", self.saveState());
        self._settings.setValue("autoSwitchToCamera", self._autoSwitchToCamera)
        self._settings.setValue("autoShowCameraPlanes", self._autoShowCameraPlanes)
        self._settings.setValue("keepInSync", self._autoChangeingSelection)
        self._activateCameraSwitcherCallback(False)
        self._handleUnregisterCallback()
        super(CameraToolDialog, self).closeEvent(event)

    def _SetupActions(self):
        """
        Internal Method

        Setup all the actions, with the connections and icons
        """
        iconPath = os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera", "icons"))

        # Camera
        self.ac_createCamera = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_new.png")), 'Create Camera', self)
        self.ac_createCamera.triggered.connect(self._handleActionCreateCamera)
        self.ac_createExportCamera = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_newexport")), 'Create Export Camera', self)
        self.ac_createExportCamera.triggered.connect(self._handleActionCreateExportCamera)
        self.ac_updateAllCameras = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_update.png")), 'Update All Cameras', self)
        self.ac_updateAllCameras.triggered.connect(self._handleActionUpdateCameras)
        self.ac_unlockCameras = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_unlock.png")), 'Unlock Cameras', self)
        self.ac_unlockCameras.triggered.connect(self._handleActionUnlockCameras)
        self.ac_saveCameras = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_save.png")), 'Save Cameras', self)
        self.ac_saveCameras .triggered.connect(self._handleActionSaveCameras)
        self.ac_duplicateSelectedCameras = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_duplicate.png")), 'Duplicate Selected Camera', self)
        self.ac_duplicateSelectedCameras.triggered.connect(self._handleActionDuplicateCameras)
        self.ac_deleteSelectedCameras = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_delete.png")), 'Delete Selected Cameras', self)
        self.ac_deleteSelectedCameras.triggered.connect(self._handleActionDeleteCameras)
        self.ac_cleanCameras = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_clean.png")), 'Clean All Cameras', self)
        self.ac_cleanCameras.triggered.connect(self._handleActionCleanCameras)
        self.ac_lockCameras = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_lock.png")), 'Lock Cameras', self)
        self.ac_lockCameras.triggered.connect(self._handleActionLockCameras)
        self.ac_sendCaptures = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "send_captures.png")), 'Send Captures', self)
        self.ac_sendCaptures.triggered.connect(self._handleActionSendCaptures)
        self.ac_loadCameras = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "load_cameras.png")), 'Load Cameras', self)
        self.ac_loadCameras.triggered.connect(self._handleActionLoadCameras)
        self.ac_removeNonSwitcherCameras = QtGui.QAction('Remove Non-Switcher Cameras', self)
        self.ac_removeNonSwitcherCameras.triggered.connect(self._handleActionRemoveNonSwitcherCameras)

        # Switcher
        self.ac_selectSwitcher = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_switcherselect.png")), 'Select Switcher', self)
        self.ac_selectSwitcher.triggered.connect(self._handleActionSelectSwitcher)
        self.ac_plotSwitcherToExportCamera = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_plot.png")), 'Plot Switcher to Export Camera', self)
        self.ac_plotSwitcherToExportCamera.triggered.connect(self._handleActionPlotToCamera)
        self.ac_plotSwitcherToAllTakesExportCamera = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_plotalltakes.png")), 'Plot Switcher to All Takes Export Camera', self)
        self.ac_plotSwitcherToAllTakesExportCamera.triggered.connect(self._handleActionPlotToCameraAllTakes)
        self.ac_plotSwitcherToSelectedTakesExportCamera = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_plotselectedtakes.png")), 'Plot Switcher to Selected Takes Export Camera', self)
        self.ac_plotSwitcherToSelectedTakesExportCamera.triggered.connect(self._handleActionPlotToCameraSelectedTakes)
        self.ac_copySwitcher = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "switcher_duplicate.png")), 'Copy Camera To New Take', self)
        self.ac_copySwitcher.triggered.connect(self._handleActionCopySwitcherToNewTake)

        # DOF
        self.ac_hideAllPlanes = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "plane_hideall.png")), 'Hide All Planes', self)
        self.ac_hideAllPlanes.triggered.connect(self._handleActionHideAllPlanes)
        self.ac_showAllPlanes = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "plane_showall.png")), 'Show All Planes', self)
        self.ac_showAllPlanes.triggered.connect(self._handleActionShowAllPlanes)
        self.ac_hideSelectedPlanes = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "plane_hideselected.png")), 'Hide Selected Planes', self)
        self.ac_hideSelectedPlanes.triggered.connect(self._handleActionHideSelectedPlanes)
        self.ac_showSelectedPlanes = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "plane_showselected.png")), 'Show Selected Planes', self)
        self.ac_showSelectedPlanes.triggered.connect(self._handleActionShowSelectedPlanes)
        self.ac_unlockPlanes = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "plane_unlock.png")), 'Unlock Planes', self)
        self.ac_unlockPlanes.triggered.connect(self._handleActionUnlockPlanes)

        # Settings
        self.ac_enableAutoSwitchingViewToCamera = QtGui.QAction('Enable Auto-Switch View to Camera', self, checkable=True)
        self.ac_enableAutoSwitchingViewToCamera.triggered.connect(self._handleActionAutoSwitchToCamera)
        self.ac_enableAutoShowCameraPlanes = QtGui.QAction('Enable Auto-Show Camera Planes', self, checkable=True)
        self.ac_enableAutoShowCameraPlanes.triggered.connect(self._handleActionAutoShowCameraPlanes)
        self.ac_keepInSync = QtGui.QAction('Keep in Sync', self, checkable=True)
        self.ac_keepInSync.triggered.connect(self._handleActionInSync)
        self.ac_labelsInToolsBar = QtGui.QAction("Show Toolbar Labels", self, checkable=True)
        self.ac_labelsInToolsBar.triggered.connect(self._handleActionLabelsInToolsBar)

        # Misc
        self.ac_dupeRtcs = QtGui.QAction('Dupe RTCs to RS Cams', self)
        self.ac_dupeRtcs.triggered.connect(self._handleActionDupeRtcs)
        self.ac_showOnlyCameras = QtGui.QAction(QtGui.QIcon(os.path.join(iconPath, "camera_onlyshow.png")), 'Show Only Cameras (Debug)', self)
        self.ac_showOnlyCameras.triggered.connect(self._handleActionShowOnlyCameras)

        ac_togglePlay = QtGui.QAction(self)
        ac_togglePlay.setShortcut(QtGui.QKeySequence('Space'))
        ac_togglePlay.triggered.connect(self._handleToggleTimelinePlay)
        self.addAction(ac_togglePlay)

        self.ac_enableAutoSwitchingViewToCamera.setChecked(self._autoSwitchToCamera)
        self.ac_enableAutoShowCameraPlanes.setChecked(self._autoShowCameraPlanes)
        self.ac_keepInSync.setChecked(self._autoChangeingSelection)

    def keyPressEvent(self, event):
        """
        ReImplement

        Catach the Key events and push them to the mobu app as keyboard presses
        """
        QtGui.QApplication.sendEvent(Application.GetMainWindow(), event)
        Application.GetMainWindow().keyPressEvent(event)

    def _SetupMenu(self):
        """
        Internal Method

        Setup the menu's using the actions which were created
        """
        menubar = QtGui.QMenuBar()
        self.layout().setMenuBar(menubar)
        cameraMenu = menubar.addMenu('Camera')
        switcherMenu = menubar.addMenu('Switcher')
        dofMenu = menubar.addMenu('DoF')
        settingsMenu = menubar.addMenu('Settings')
        windowsMenu = menubar.addMenu('Windows')
        miscMenu = menubar.addMenu('Misc')

        self._addActionsToMenu(cameraMenu,
            [
            self.ac_createCamera, self.ac_createExportCamera, self.ac_duplicateSelectedCameras,
            None, self.ac_updateAllCameras, self.ac_removeNonSwitcherCameras, self.ac_cleanCameras,
            None, self.ac_loadCameras, self.ac_sendCaptures, self.ac_lockCameras,
            self.ac_saveCameras, self.ac_unlockCameras, None, self.ac_deleteSelectedCameras
            ]
        )

        self._addActionsToMenu(switcherMenu,
            [
             self.ac_selectSwitcher, self.ac_copySwitcher, None, self.ac_plotSwitcherToExportCamera,
            self.ac_plotSwitcherToAllTakesExportCamera, self.ac_plotSwitcherToSelectedTakesExportCamera
            ]
        )

        self._addActionsToMenu(dofMenu,
            [
             self.ac_hideAllPlanes, self.ac_showAllPlanes, None, self.ac_hideSelectedPlanes,
            self.ac_showSelectedPlanes, None, self.ac_unlockPlanes
            ]
        )

        self._addActionsToMenu(settingsMenu,
            [
             self.ac_enableAutoSwitchingViewToCamera, self.ac_enableAutoShowCameraPlanes,
             self.ac_keepInSync, self.ac_labelsInToolsBar
            ]
        )

        self._addActionsToMenu(miscMenu,
            [
             self.ac_dupeRtcs,
             self.ac_showOnlyCameras
            ]
        )

        for widget in self._docWidgets.values():
            windowsMenu.addAction(widget.toggleViewAction())

    def _SetupMenuBar(self):
        """
        Internal Method

        Setup the ToolBar using the actions which were created
        """

        self._toolBar = self.addToolBar('ToolBar')
        self._toolBar.setFloatable(True)
        self._toolBar.setMovable(True)
        self._toolBar.setIconSize(QtCore.QSize(35, 35))
        self._toolBar.setAllowedAreas(QtCore.Qt.AllToolBarAreas)

        self._addActionsToMenu(self._toolBar,
            [
            self.ac_createCamera, self.ac_createExportCamera, self.ac_deleteSelectedCameras, None,
            self.ac_cleanCameras, self.ac_updateAllCameras, self.ac_plotSwitcherToExportCamera, None,
            self.ac_loadCameras, self.ac_saveCameras, self.ac_sendCaptures, None,
            self.ac_selectSwitcher, self.ac_plotSwitcherToAllTakesExportCamera,
            self.ac_plotSwitcherToSelectedTakesExportCamera, None,
            self.ac_showAllPlanes, self.ac_hideAllPlanes, self.ac_showSelectedPlanes, None,

            ]
        )

    def _addActionsToMenu(self, menuToAdd, actions):
        """
        Internal Method

        Helper method to add a list of actions to a menu
        if an action in the list of actions is None, then a separator is added
        """
        for action in actions:
            if action is None:
                menuToAdd.addSeparator()
                continue
            menuToAdd.addAction(action)

    def SetupUi(self):
        """
        Internal Method

        Setup the UI, setting up the connections
        """
        # If we set up the dialogs now, and hook up the signals, we only have to pay once
        self._createCameraDialog = createCameraDialog.CreateCameraDialog()
        self._createCameraDialog.CameraCreated.connect(self._handleCreateCamera)
        self._copyTakeDialog = copyTakeSelectorDialog.CopyTakeSelectorDialog()
        self._copyTakeDialog.TakeSelected.connect(self._handleCopyTake)

        banner = UI.BannerWidget(self._toolName, helpUrl=const.helpWikiAddress,)
        banner.setMaximumHeight(30)

        self._setupUiTabs()
        self._SetupActions()
        self._SetupMenuBar()
        self._SetupMenu()

        self._autoSwitchToCamera = self.ac_enableAutoSwitchingViewToCamera.isCheckable()
        self._autoShowCameraPlanes = self.ac_enableAutoShowCameraPlanes.isCheckable()
        self._autoChangeingSelection = self.ac_keepInSync.isCheckable()

    def _setupUiTabs(self):
        self._cameraList = sceneCameraExplorer.SceneCameraExplorer()
        self.__cameraDocWidget = QtGui.QDockWidget("Camera Explore")
        self.__cameraDocWidget.setWidget(self._cameraList)
        self._docWidgets["Camera Explore"] = self.__cameraDocWidget
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.__cameraDocWidget)
        
        # TODO: Removed these old cambox functions - aren't used, and error on RDR cams
        # self._cameraControls = cameraControlsWidget.CameraControlsWidget()
        # cameraControlsDockWidget = QtGui.QDockWidget("Camera Controls")
        # cameraControlsDockWidget.setWidget(self._cameraControls)
        # self._docWidgets["Camera Controls"] = cameraControlsDockWidget

        # self._dofControls = depthOfFieldControlsWidget.DepthOfFieldControlsWidget()
        # self.__dofDockWidget = QtGui.QDockWidget("Depth of Field")
        # self.__dofDockWidget.setWidget(self._dofControls)
        # self._docWidgets["Depth of Field"] = self.__dofDockWidget

        # self._shakeControls = shakeControlsWidget.ShakeControlsWidget()
        # self.__shakeDockWidget = QtGui.QDockWidget("Shake Pass")
        # self.__shakeDockWidget.setWidget(self._shakeControls)
        # self._docWidgets["Shake Pass"] = self.__shakeDockWidget

        # self._gameControlsWidget = RemoteCamera.SetupWidget()
        # self.__gameControlsDockWidget = QtGui.QDockWidget("Game Controls")
        # self.__gameControlsDockWidget.setWidget(self._gameControlsWidget)
        # self._docWidgets["Game Controls"] = self.__gameControlsDockWidget

        # self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.__dofDockWidget)
        # self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.__shakeDockWidget)
        # self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.__gameControlsDockWidget)
        # self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.__cameraDocWidget)

        # self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, cameraControlsDockWidget)
        # self.tabifyDockWidget(self.__dofDockWidget, self.__shakeDockWidget)
        # self.tabifyDockWidget(self.__dofDockWidget, self.__gameControlsDockWidget)

        for widget in self._docWidgets.values():
            widget.setObjectName(widget.windowTitle())

        self.setDockOptions(QtGui.QMainWindow.VerticalTabs | QtGui.QMainWindow.AllowTabbedDocks |
                            QtGui.QMainWindow.AllowNestedDocks | QtGui.QMainWindow.AnimatedDocks)
        self._cameraList.CameraSelected.connect(self._handleCameraSelected)
        self._cameraList.CameraSwitcherCheckBoxChecked.connect(self._activateCameraSwitcherCallback)
        self.CameraSelected.connect(self._handleCameraSelected)
        # self._cameraControls.CameraDataChanged.connect(self._handleCameraDataChanged)
        # self._dofControls.CameraDataChanged.connect(self._handleCameraDataChanged)

    @noRecursive.noRecursive
    def _handleCameraSelected(self, camera):
        """
        Internal Method

        Handle the user selecting a different camera in the UI
        Camera is an mobu.FBCamera object
        """
        for comp in [comp for comp in mobu.FBSystem().Scene.Components if comp is not None and comp.Selected]:
                comp.Selected = False

        if camera is None:
            return

        # Add check to enable constraint for camera planes in the scene - 1331929
        for idx in xrange(camera.GetSrcCount()):
            if isinstance(camera.GetSrc(idx), mobu.FBConstraintRelation):
                if camera.GetSrc(idx).Active is False:
                    camera.GetSrc(idx).Active = True
        camera.Selected = True
        if self._autoSwitchToCamera:
            # CamLib.camUpdate.rs_switchViewtoCamera(camera)
            Globals.Scene.Renderer.SetCameraInPane(camera, 0)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, False, True)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(self._autoShowCameraPlanes, True, True)

        self.SelectCamera(camera)

    @noRecursive.noRecursive
    def _handleCameraCallback(self, obj, evt):
        """
        callback for Camera selection url:bugstar:1815189
        """
        if self.__isDeleted:
            return
        
        # Dont run when Sync is turned off or events where a camera is the not object
        if self._autoChangeingSelection and isinstance(evt.Component, mobu.FBCamera):
            if evt.Type == mobu.FBSceneChangeType.kFBSceneChangeSelect:
                cameraSelected = evt.Component
                if self._activeCamera is not cameraSelected:
                    self.SelectCamera(cameraSelected)
            elif evt.Type == mobu.FBSceneChangeType.kFBSceneChangeChangeName:
                self.RefreshCameraList()
            elif evt.Type == mobu.FBSceneChangeType.kFBSceneChangeRenameUnique:
                self.RefreshCameraList()
            elif evt.Type == mobu.FBSceneChangeType.kFBSceneChangeRemoveChild:
                self.RefreshCameraList()

    def _handleUnregisterCallback(self, control=None, event=None):
        RsCallbacks.OnChange.Remove(self._handleCameraCallback)
        RsCallbacks.OnFileExit.Remove(self._handleUnregisterCallback)

    # Camera Actions
    def _handleActionCreateCamera(self):
        """
        Internal Method

        Action Handler to show the create camera dialog
        """
        self._createCameraDialog.show()

    def _handleActionCreateExportCamera(self):
        """
        Internal Method

        Action Handler to create an export camera
        """
        # CamLib.camUpdate.CreateExportCamera()
        CamLib.MobuCamera.CreateExportCamera()
        self.RefreshCameraList()

    def _handleActionUpdateCameras(self):
        """
        Internal Method

        Action Handler to Update all the cameras
        """
        LibGTA5.camUpdate.rs_updateCameras()
        self.RefreshCameraList()
    
    def _handleActionDupeRtcs(self):
        """
        Internal Method

        Action Handler to dupe RTC cams to RS cams
        """
        CamUtils.DupeRtcsToRsCams()

    def _handleActionShowOnlyCameras(self):
        """
        Internal Method

        Action Handler to show only cameras (debug)
        """
        LibGTA5.camUpdate.showOnlyCameras()

    def _handleActionUnlockCameras(self):
        """
        Internal Method

        Action Handler to unlock cameras
        """
        camLockObj = CameraLocker.LockManager()
        camLockObj.setAllCamLocks(False)

    def _handleActionDeleteCameras(self):
        """
        Internal Method

        Action Handler to delete the currently selected camera
        """
        cameraNode = self.GetSelectedCamera()
        if cameraNode is None:
            return
        # CamLib.camUpdate.DeleteCamera(cameraNode, RemoveCam=True)
        rsCam = CamLib.MobuCamera(cameraNode)
        rsCam.DeleteCamera()
        self.RefreshCameraList()

    def _handleActionCleanCameras(self):
        """
        Internal Method

        Action Handler to Clean all the cameras in the scene
        """
        # CameraCleaner.FixCamNames()
        self.RefreshCameraList()

    def _handleActionSaveCameras(self):
        """
        Internal Method

        Action Handler to lock and save the cameras
        """
        SaveCameras.Run()

    def _handleActionLockCameras(self):
        """
        Internal Method

        Action Handler to lock the cameras
        """
        camLockObj = CameraLocker.LockManager()
        camLockObj.setAllCamLocks(True)

    def _handleActionSendCaptures(self):
        """
        Internal Method

        Action Handler to send captures
        """
        SaveCameras.Run()
        CaptureRender.QueueCaptureRender()

    def _handleActionLoadCameras(self):
        """
        Internal Method

        Action Handler to Load the cameras
        """
        camKeyTasks.LoadCamsInterface()
        self.RefreshCameraList()

    def _handleActionDuplicateCameras(self):
        """
        Internal Method

        Action Handler to duplicate the selected camera
        """
        selectedCamera = self.GetSelectedCamera()
        if selectedCamera is None:
            msgTxt = "No camera selected"
            QtGui.QMessageBox.critical(self, "Error", msgTxt, QtGui.QMessageBox.Ok)
            return
        newCamera = CamUtils.DupeCamera(selectedCamera)
        msgTxt = "{0} duped to new {1}".format(selectedCamera.Name, newCamera.Name)
        QtGui.QMessageBox.information(self, "Cam Duped", msgTxt, QtGui.QMessageBox.Ok)

    # Switcher Actions
    def _handleActionSelectSwitcher(self):
        """
        Internal Method

        Action Handler to select the camera switcher
        """
        # switcher = CamLib.camUpdate.getSwitcherCamera()
        switcher = CamLib.Manager.GetCameraSwitcher()
        self.SelectCamera(switcher)
        switcher.Selected = True

    def _handleActionPlotToCamera(self):
        """
        Internal Method

        Action Handler to plot the switcher on the current take
        """
        # Plot.PlotSwitcherToExportCamera(RsSystem.CurrentTake)
        CamUtils.PlotSwitcherToExportCamera(RsSystem.CurrentTake)

    def _handleActionPlotToCameraSelectedTakes(self):
        """
        Internal Method

        Action Handler to plot the switcher on the selected takes
        """
        takes = takeSelectionView.TakeSelectionView.dialog()
        if takes:
            # Plot.PlotSwitcherToExportCamera(takes)
            CamUtils.PlotSwitcherToExportCamera(takes)

    def _handleActionPlotToCameraAllTakes(self):
        """
        Internal Method

        Action Handler to plot the switcher on all takes
        """
        result = QtGui.QMessageBox.warning(
            self,
            "Warning",
            "This will plot the camera switcher to all {0} takes!".format(len(RsScene.Takes)),
            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel
        )

        if result != QtGui.QMessageBox.Ok:
            return
        # Plot.PlotSwitcherToExportCamera(RsScene.Takes)
        CamUtils.PlotSwitcherToExportCamera(RsScene.Takes)

    def _handleActionCopySwitcherToNewTake(self):
        """
        Internal Method

        Action Handler to copy switcher to new take
        """
        self._copyTakeDialog.show()

    def _handleActionRemoveNonSwitcherCameras(self):
        """
        Internal Method

        Action Handler to delete all non system cameras not in the switcher
        """
        # CamLib.camUpdate.rs_RemoveNonSwitcherCameras()
        CamLib.Manager.DeleteNonSwitcherCams()
        self.RefreshCameraList()

    # DoF Actions
    def _handleActionHideAllPlanes(self):
        """
        Internal Method

        Action Handler to hide all planes
        """
        # CamLib.camUpdate.rs_ShowHideCameraPlanes(False, False, False)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

    def _handleActionShowAllPlanes(self):
        """
        Internal Method

        Action Handler to show all planes
        """
        # CamLib.camUpdate.rs_ShowHideCameraPlanes(True, False, False)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(True, False, False)

    def _handleActionHideSelectedPlanes(self):
        """
        Internal Method

        Action Handler to hide the selected planes
        """
        # CamLib.camUpdate.rs_ShowHideCameraPlanes(False, True, False)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(False, True, False)

    def _handleActionShowSelectedPlanes(self):
        """
        Internal Method

        Action Handler to show the selected planes
        """
        # CamLib.camUpdate.rs_ShowHideCameraPlanes(True, True, False)
        LibGTA5.camUpdate.rs_ShowHideCameraPlanes(True, True, False)

    def _handleActionUnlockPlanes(self):
        """
        Internal Method

        Action Handler to unlock planes
        """
        # CamLib.camUpdate.setPlanesLock(False)
        LibGTA5.camUpdate.setPlanesLock(False)

    # Settings Actions
    def _handleActionAutoSwitchToCamera(self):
        """
        Internal Method

        Action Handler to turn the auto switch to camera on and off
        """
        self._autoSwitchToCamera = self.ac_enableAutoSwitchingViewToCamera.isChecked()

    def _handleActionAutoShowCameraPlanes(self):
        """
        Internal Method

        Action Handler to turn the auto show camera planes on and off
        """
        self._autoShowCameraPlanes = self.ac_enableAutoShowCameraPlanes.isChecked()

    def _handleActionInSync(self):
        """
        Internal Method

        Action Handler to turn the Sync on and off
        """
        self._autoChangeingSelection = self.ac_keepInSync.isChecked()

    def _handleActionLabelsInToolsBar(self):
        """
        Internal Method

        Action Handler to show/hide the labels on the toolbar
        """
        setting = QtCore.Qt.ToolButtonIconOnly
        if self.ac_labelsInToolsBar.isChecked():
            setting = QtCore.Qt.ToolButtonTextUnderIcon

        self._toolBar.setToolButtonStyle(setting)

    def _handleCopyTake(self, newTake):
        """
        Handle the actual copy take from the UI
        """
        currentTake = RsSystem.CurrentTake
        msg = "This will copy switcher data from {0} to {1}".format(currentTake.Name, newTake.Name)
        result = QtGui.QMessageBox.warning(
                    self,
                    "Warning",
                     msg,
                    QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel
                )

        if result != QtGui.QMessageBox.Ok:
            return

        Scene.Model.CopyAnimationBetweenTakes(
                                              Switcher,
                                              currentTake,
                                              [newTake],
                                              [Scene.Property.SwitcherIndex]
                                              )

    def _handleCreateCamera(self, lensSize, amount=1):
        """
        Handle the actual camera creation from the UI
        """
        for idx in xrange(amount):
            # newCamera = CamLib.camUpdate.CreateCustomCamera()
            newCamera = CamLib.MobuCamera.CreateRsCamera().camera

            cameaAttra = newCamera.PropertyList.Find(const.CameraControlPropertiesIndex.lens)
            cameaAttra.Data = cameaAttra.GetEnumStringList().IndexOf(str(lensSize))
        self.RefreshCameraList()

    def _handleToggleTimelinePlay(self):
        """
        Internal Method

        Play or stop the timeline contorl
        """
        playa = mobu.FBPlayerControl()
        if playa.IsPlaying:
            playa.Stop()
        else:
            playa.Play()

    def RefreshCameraList(self):
        """
        Force a refresh of the camera's in the camera explorers list of cameras
        """
        self._cameraList.Refresh()

    def GetSelectedCamera(self):
        """
        Get the currently selected camera, returns as None if nothing is selected
        or an mobu.FBCamera object
        """
        return self._cameraList.GetSelectedCamera()

    @noRecursive.noRecursive
    def SelectCamera(self, cameraToSelect):
        """
        Select the input camera from the list of cameras, and update the UI accordingly
        Takes a mobu.FBCamera Object
        """
        if self._activeCamera is cameraToSelect:
            return
        
        if isinstance(cameraToSelect, tuple(unbind.unbound_wrapper.values())):
            cameraToSelect = None
        
        if cameraToSelect.GetObjectStatus(mobu.FBObjectStatus.kFBStatusOwnedByUndo):
            cameraToSelect = None
        
        # TODO: Disabled below code to prevent cambox from hanging on RDR cams
        # if self._dofControls.ActiveCamera() is not cameraToSelect:
            # self._dofControls.SetActiveCamera(cameraToSelect)
        # if self._shakeControls.ActiveCamera() is not cameraToSelect:
            # self._shakeControls.SetActiveCamera(cameraToSelect)
        if self._cameraList.GetSelectedCamera() is not cameraToSelect:
            self._cameraList.SelectCamera(cameraToSelect)
        # if self._cameraControls.ActiveCamera() is not cameraToSelect:
            # self._cameraControls.SetActiveCamera(cameraToSelect)
            
        self._activeCamera = cameraToSelect
        
    @noRecursive.noRecursive
    def _handleCameraDataChanged(self, camera, prop, value):
        """
        Internal Method

        To re-emit any camera data changes
        """
        self._cameraList.UpdateCameraData()

    def _activateCameraSwitcherCallback(self, toggle):
        """
        Activate or remove callback for selecting the current camera the switcher is on

        Arguments:
            toggle (boolean): add or remove callback
        """
        if toggle:
            Globals.System.SuspendMessageBoxes = True
            Globals.EvaluateManager.UseGPUDeformation = True
            Globals.EvaluateManager.ParallelEvaluation = True
            Globals.System.SuspendMessageBoxes = False

            RsCallbacks.OnSynchronizationEvent.Add(self._selectCurrentSwitcherCamera)
        else:
            RsCallbacks.OnSynchronizationEvent.Remove(self._selectCurrentSwitcherCamera)

    def _selectCurrentSwitcherCamera(self, source, event):
        """
        Triggers the Select Current Switcher Camera method

        Arguments:
            source ():
            event:

        Returns:

        """
        CamUtils.SelectCurrentSwitcherCamera(MarkerTools.FocusMarker())

if __name__ == "__main__":
    win = CameraToolDialog()
    win.show()
