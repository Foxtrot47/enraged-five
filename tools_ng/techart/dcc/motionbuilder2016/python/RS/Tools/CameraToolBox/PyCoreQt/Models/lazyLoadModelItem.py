__DoNotReload__ = True # Module is NOT safe to be dynamciy reloaded


from PySide import QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModelItem


class LoadingModelItem(baseModelItem.BaseModelItem):
    def __init__(self, parent=None):
        super(LoadingModelItem, self).__init__(parent=parent)
        self._fetchedMore = False
    
    def canFetchMore(self, parent):
        if not parent.isValid():
            return False
        if self._fetchedMore is False:
            return parent.internalPointer().canFetchMore(parent) or False
        return False
    
    def fetchMore(self, parent):
        if not parent.isValid():
            return False
        self._fetchedMore = True
        return parent.internalPointer().fetchMore(parent)

    def hasChildren(self, parent):
        if self.canFetchMore(parent):
            return True
        return self.rowCount() > 0

    def child(self, row):
        if len(self.childItems) == 0:
            if self.canFetchMore(self.createIndex(0,0)) is True:
                self.fetchMore(self.createIndex(0,0)) 
        return self.childItems[row]

    def childCount(self, index=QtCore.QModelIndex()):
        if self.canFetchMore(index) is True:
            return 1
        return len(self.childItems)