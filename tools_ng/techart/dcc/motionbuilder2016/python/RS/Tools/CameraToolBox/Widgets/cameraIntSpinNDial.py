# Camera int Spin'N Dial widget to combine the custom dial and a spinner box
from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.PyCoreQt.Widgets import customIntDial


class CameraIntSpinNDial(QtGui.QWidget):
    """
    Camera Spin'N Dial widget to combine the custom dial and a spinner box
    
    One Signal:
        ValueChanged - If the value of the spinner or the dial changes, 
                       this signal will emit with the new value
    """
    ValueChanged = QtCore.Signal(float)
    
    def __init__(self, value=0, minimumValue=0, maximumValue=100, parent=None):
        """
        Constructor
        
        value (int) The starting value
        minimumValue (int) the minimum value the spinner can go to
        maximumValue (int) the maximum value the spinner can go to
        """
        super(CameraIntSpinNDial, self).__init__(parent=parent)
        self.SetupUi(value, minimumValue, maximumValue)
        self.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding))
        
    def SetupUi(self, value, minValue, maxValue):
        """
        Internal Method
        
        Setup the UI and the connections
        
        value (int) The starting value
        minValue (int) the minimum value the spinner can go to
        maxValue (int) the maximum value the spinner can go to
        """
        mainLayout = QtGui.QGridLayout()
        
        self._spinnerBox = QtGui.QSpinBox()
        self._dial = customIntDial.CustomIntDial(value, minValue, maxValue)
        
        self._spinnerBox.setMaximum(maxValue)
        self._spinnerBox.setMinimum(minValue)
        self._spinnerBox.setValue(value)
        self._spinnerBox.setMaximumWidth(80)
        
        self._dial.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding))
                
        mainLayout.addWidget(self._dial, 0, 0, 1, 3)
        mainLayout.addWidget(self._spinnerBox, 1, 1)

        self._spinnerBox.valueChanged.connect(self._handleSpinnerChange)
        self._dial.valueChanged.connect(self._handleDialChange)
        
        self.setLayout(mainLayout)
       
    def setValue(self, newValue):
        """
        Set the spinner's Value
        """
        self._dial.setValue(newValue)
        
    def value(self):
        """
        Get the spinner's Value
        """
        return self._dial.Value
    
    def setMaximum(self, newValue):
        """
        Set the Maximum value the spinners can go to
        """
        self._spinnerBox.setMaximum(newValue)
        self._dial.setMaximum(newValue)

    def setMinimum(self, newValue):
        """
        Set the Minimum value the spinners can go to
        """
        self._spinnerBox.setMinimum(newValue)
        self._dial.setMinimum(newValue)
        
    def minimum(self):
        """
        Get the Minimum value the spinners can go to
        """
        return self._dial.MinimumValue
    
    def maximum(self):
        """
        Get the Maximum value the spinners can go to
        """
        return self._dial.MaximumValue

    def _handleDialChange(self, newValue):
        """
        Internal Method
        
        Handle the dial change, so update the spinner
        """
        self._spinnerBox.setValue(newValue)
        self.ValueChanged.emit(newValue)
    
    def _handleSpinnerChange(self, newValue):
        """
        Internal Method
        
        Handle the spinner change, so update the dial
        """
        self._dial.setValue(newValue)
        self.ValueChanged.emit(newValue)


if __name__ == "__main__":
    win = CameraIntSpinNDial()
    win.show()