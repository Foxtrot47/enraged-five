from PySide import QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class MimeData(QtCore.QMimeData):
    """ Mime Data for sharing model indexes """
    def __init__(self):
        """ Constructor """

        super(MimeData, self).__init__()
        self._ModelIndexes = []
        self._copy = False
        self._customColor = None

    def indicies(self):
        """ The stored QModelIndexes """
        return self._ModelIndexes

    def setIndicies(self, indicies):
        """
        Stores the indicies within the class

        Arguments:
            indicies (list(PySide.QtGui.QModelIndex)): QModelIndexes to store

        """
        self._ModelIndexes = indicies

    def copy(self):
        return self._copy

    def setCopy(self, value):
        self._copy = value

    def customColor(self):
        return self._customColor

    def setCustomColor(self, value):
        self._customColor = value


class DragDropModel(baseModel.BaseModel):

    def __init__(self, parent=None):
        self._copy = False
        self._customColor = None
        self._mimeType = "text/plain"
        self._acceptableMimeTypes = ('text/plain',)
        self._itemRole = QtCore.Qt.UserRole + 42
        self._headers = []
        super(DragDropModel, self).__init__(parent=parent)

    def addHeader(self, header, position=None):
        if not position:
            position = len(self._headers)
        self._headers[position:position] = [header]
        self.refresh()

    def removeHeader(self, header):
        self._headers.pop(header)

    def getHeaders(self):
        return self._headers

    def flags(self, index):
        """
        flags that tell the view how to handle the items

        Arguments:
            index (QtCore.QModelIndex): model index for the current item

        """
        # Invalid indices (open space in the view) are also drop enabled, so you can drop items onto the top level.
        if not index.isValid():
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsDropEnabled

        # For items to be drag and drop enabled they need to return the
        # QtCore.Qt.ItemIsDropEnabled | QtCore.Qt.ItemIsDragEnabled flags

        return self._itemFromIndex(index).flags(index)

    def supportedDragActions(self):
        """ Items can be moved and copied """
        return QtCore.Qt.MoveAction | QtCore.Qt.CopyAction

    def supportedDropActions(self):
        """ Items can be moved and copied """
        return QtCore.Qt.MoveAction | QtCore.Qt.CopyAction

    def mimeType(self):
        return self._mimeType

    def setMimeType(self, value):
        self._mimeType = value

    def mimeTypes(self):
        """ The Mime Types that the model accepts when mime data dropped on it. """
        return self._acceptableMimeTypes

    def setMimeTypes(self, mimeTypes):
        """
        Sets the Mime Types that the model accepts when mime data dropped on it

        Arguments:
            mimeTypes:
        """
        self._acceptableMimeTypes = mimeTypes

    def mimeData(self, indices):
        """ Encode serialized data from the item at the given index into a QMimeData object. """
        data = ''

        mimedata = MimeData()
        mimedata.setData(self._mimeType, data)
        mimedata.setIndicies([index for index in indices if index.column() == 0])
        mimedata.setCopy(self._copy)
        return mimedata

    def copy(self):
        """ Dragged & Dropped items should be copied rather than moved"""
        return self._copy

    def setCopy(self, value):
        self._copy = value

    def dropMimeData(self, mimedata, action, row, column, parentIndex):
        """
        Handles the dropping of an item onto the model.

        De-serializes the data into a TreeItem instance and inserts it into the model.
        """

        # Get parent from the parent index

        row = row if (row, column) != (-1, -1) else -1
        self.moveItems(parentIndex, mimedata.indicies(), mimedata.formats(), copy=mimedata.copy(), row=row)
        return True

    def moveItems(self, parentIndex, indicies, mimeType, row=None, copy=False):
        """
        Moves an items within the tree model.

        This method is meant to be used within the  dropMimeData method

        Arguments:
            parentIndex (QtCore.QModelIndex): the parent model index
            indicies (list): list of QModelIndex that have been dragged to a new parent
            mimeType (string): the mimetype for the QModelIndexes
            row (int): row to copy/drop item too
            copy (boolean): should the items not be removed from their original parents

        Return:
            boolean
        """
        row = row if row is not None else -1
        dataChanged = False
        for index in indicies:
            # This is to stop items from being parented to themselves
            same = parentIndex == index
            grandParentIndex = parentIndex.parent()

            while grandParentIndex.isValid() and not same:
                same = grandParentIndex == index
                grandParentIndex = grandParentIndex.parent()

            if same:
                continue

            # Check that this model has an index at the row and column of the index being moved
            # and if the two indexes are the same.
            # This is to check if the index being moved is from another model.
            indexRow = index.row()
            hasIndex = self.hasIndex(index.row(), index.column(), parentIndex)
            isParent = self.index(index.row(), index.column(), parentIndex) == index

            if hasIndex and isParent:
                # Moving to position that will leave the item in the same place seems to crash the tool
                if row not in (indexRow, indexRow + 1):
                    self.moveItem(index, row, parentIndex, mimeType)

            elif copy:
                # Copy the item
                self.copyItem(index, row, parentIndex, mimeType)

            else:
                self.dropItem(index, row, parentIndex, mimeType)
            dataChanged = True
        return dataChanged

    def copyItem(self, index, row, parentIndex, mimeType):
        """
        Logic for when an item is dropped on another item but the original item should not be deleted/removed

        This method needs to be reimplemented in inherited classes

        Arguments:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped
        """
        raise NotImplementedError("Reimplement this!")

    def dropItem(self, index, row, parentIndex, mimeType):
        """
        Logic for when an item is dropped on top of another item

        This method needs to be reimplemented in inherited classes

        Arguments:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped
        """

        raise NotImplementedError("Reimplement this!")

    def moveItem(self, index, row, parentIndex, mimeType):
        """
        Logic for when an item is dropped on its parent to shift its position in the view hierarchy

        This method needs to be reimplemented in inherited classes

        Arguments:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped
        """
        raise NotImplementedError("Reimplement this!")

    def headerData(self, index, orientation=QtCore.Qt.Vertical, role=QtCore.Qt.DisplayRole):
        """
        Header information for the view

        Arguments:
            index (QtGui.QModelIndex): index being queried
            orientation (QtCore.Qt.Orientation): orientation of the header, vertical or horizontal
            role (QtCore.Qt.Role): role requested by the view

        Return:
            object
        """
        if role == QtCore.Qt.DisplayRole and len(self._headers) > index:
            return self._headers[index]