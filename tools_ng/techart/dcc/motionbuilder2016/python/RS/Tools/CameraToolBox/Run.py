from RS.Tools.CameraToolBox.Dialogs import cameraToolDialog


def Run( show = True ):
    toolsDialog = cameraToolDialog.CameraToolDialog()
    
    if show:
        toolsDialog.show()

    return toolsDialog

