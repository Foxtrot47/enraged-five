import clr
clr.AddReference("System")
clr.AddReference("RSG.Rag.Clients")

from System.ServiceModel import CommunicationState
from RSG.Rag.Clients import RagClientFactory

#TODO: Move to global Area, non DCC



class Gameconnection(object):
    def __init__(self):
        """

        :return:
        """
        self._gameConnection = None
        self._isConnected = False
        self._CommunicationState = CommunicationState


    @property
    def IsConnected(self):
        """
        Return Status of Game Connection
        :return:
        """

        return self._gameConnection is not None

    @property
    def CommunicationState(self):
        """
        Return thr System Service model commuciaiton state

        :return: Enum COmmuication State
        """
        return self._CommunicationState


    def EnsureConnection(self):
        """
        Return our Game Connection if there is one
        If Rag is running and the Game is running then return the Game connecttion
        else return None
        :return:
        """

        if self.IsConnected:
            return True
        else:
            try:
                connectionList = RagClientFactory.GetGameConnections()
                conn = [f for f in connectionList if f.DefaultConnection]
                self._gameConnection = conn[0]
            except:
                self._gameConnection = None
        return self.IsConnected


    def initClient(self):
        """
        Initialize our Client,
        We do a try catch to check if the client fails, if so we mark the Service channel as faulted

        :return: RSG.ClientFactory or None
        """
        client = None
        if self.IsConnected:
            client = RagClientFactory.CreateWidgetClient(self._gameConnection)
        return client


    def Disconnect(self):
        """

        :return:
        """
        if not self.IsConnected:
            return
        try:
            RagClientFactory.Close()
        except:
            pass
        self._gameConnection = None


    def Dispose(self):
        """
        Disconnect Implementation
        :return:
        """
        self.Disconnect()






