import os
import re
import time
import math
import ctypes

import clr
import numpy
import pyfbsdk as mobu

from RS import Config, Globals
from RS.Utils import Math, MathGameUtils
from RS.Utils import Exception as RSException
from RS.Core.Camera import RangeTools
from RS.Tools.RemoteCamera.Models import GameConnection, RagCommands
reload(RagCommands)


clr.AddReference("RSG.Base")
from RSG.Base import Math as RageMath
from RS.Core.Export import RexExport


widget = RagCommands.widget

MOTIONBUILDER_UNIT_SCALE = 0.01

# # PURPOSE:    conversions between radians and degrees
RtoD   =  (180.0/math.pi)
RtoDx2 =  (360.0/math.pi)
DtoR   =  (math.pi/180.0)
Dx2toR =  (math.pi/360.0)


class MissingGameError(Exception):
    pass


class RemoteConnectionModel(object):

    def __init__(self, remoteSignal):
        """
        Constructor
        
        Arguments
            remoteSignal (QtGui.QObject): a qobject with that emits signals back to the UI if the game connection fails
        
        """
        self.remoteSignal = remoteSignal

        self._client = None
        self._LoadedAnimSceneName = None

        self.mConnection = GameConnection.Gameconnection()
        self.ENABLED = False
        self.GAME_MODE = False
        self.USESWITCHER = True
        self.USEFULLRANGE = False
        self.LOADRPF = True
        self.DISABLEDOF = False
        self.ENABLEDBLENDOUT = True
        self.ENABLEDOFOVERLAY = False
        self.ENABLELETTERBOX = True
        self.ENABLEGRIDOVERLAY = False
        self.ENABLEDOFSUMMARY = False
        self.cutsceneName = None

        self.OFFSET_X = 0
        self.OFFSET_Y = 0
        self.OFFSET_Z = 0
        self.OFFSET_RX = 0 # Rotation
        self.OFFSET_RY = 0 # Rotation
        self.OFFSET_RZ = 0 # Rotation
        self.FRAME_OFFSET = 0
        self.lastFrame = 0
        self.FRAME_END = 100000
        self.CurrentCamera = None
        self.Lastcamera = None
        self.Fov = 37.0
        self.Dof = -1
        self.CamMatrix = mobu.FBMatrix()

        self.CUTSCENE_MODE = False
        self.ENABLE_FRAME_UPDATE = False

        self.discardedFramesOffsets = []

        self.mergeDict = {}
        self._vectors = {
                   "x": [1, 0, 0],
                   "y": [0, 1, 0],
                   "z": [0, 0, 1]}
        self._arePerpendicular = {
                            "xy": False,
                            "xz": False,
                            "yz": False}
        # Initialize the cutscene export if its a cutscene
        RexExport.InitializeCutscene()

    """
        Public Methods
    """

    @property
    def client(self):
        if self._client is None:
            self.getClient()
        return self._client

    def Connect(self):
        """
        Create our connection
        """
        if not self.mConnection.IsConnected:

            if self.mConnection.EnsureConnection():
                self.mConnection.initClient()
                self.intiRequiredWidgets()
                self.setWidgetBankReferences()
                self.Unregister()
                self.Register()
                # Read in any offsets from the FBX file
                #self.ReadOffsets()
                # set our offset for the range start for playback
                self.ReadStartRange()
                # Read the end range if any
                self.ReadEndRange()
            else:
                self.remoteSignal.wcf_disconnect(False)

        # Get Latest Merge Dict
        fbxPath = mobu.FBApplication().FBXFileName
        self.mergeDict = RangeTools.GetMergeDict(fbxPath)


    def Disconnect(self):
        """ Disconnect connection """
        # Clear previously stored cameras
        self.clearsCameras()
        # Disable any optimizations
        self.enableOptimizations(False)
        # Unregister our callbacks
        self.Unregister()
        # If we still have an connection to the game, close our widgets and close our connection
        if self.mConnection.IsConnected:
            self.CloseRequiredWidgets()
            self.mConnection.Disconnect()
            self.mConnection.Dispose()


    """
        Private Methods
    """
    def _runClientSafeCmd(self, ragCmd, *args):
        """
        Will handle any exceptions if the communication channel fails. ie. game Crash

        Arguments:
            ragCmd (method): Rag Widget cmd to run
            args (list): list of arguments to pass to the me

        Return:
            Widget Value or None
        """

        try:
            # debug
            #print ragCmd, args

            return ragCmd(*args)
        except Exception as e:
            pass
            # Handle the exception.
            # TODO: We might need to close the connection and re-initialize though Game may have Crashes out
        finally:
            # Check if the connection has faulted, if so then close the client channel
            if self.client.State == self.mConnection.CommunicationState.Faulted:
                self.remoteSignal.wcf_disconnect(False)
                self.Unregister()
                if hasattr(self.client, "Abort"):
                    self.client.Abort()

                self.mConnection.Disconnect()
                self.mConnection.Dispose()

    def _isUsingPreviewData(self):
        """ Are we loading up preview data, if so set the UI to using Full Range Support """
        useFullRange = False
        previewFolder = os.path.join(Config.Project.DefaultBranch.Path.Build, 'preview')
        if os.path.exists(previewFolder):
            for filename in os.listdir(previewFolder):
                if filename.lower().startswith(self.cutsceneName.lower()):
                    useFullRange = True
        self.remoteSignal.fullRangeStateChanged(useFullRange)


    def clearsCameras(self):
        """ clears the internally stored cameras """
        self.CurrentCamera = None
        self.Lastcamera = None


    def getClient(self):
        """ get an instance our our client to send commands to """
        try:
            self._client = self.mConnection.initClient()
        except:
            self._client = None
        finally:
            if self._client is None:
                raise MissingGameError("A connection to the game could not be established.\n"
                                       "Please make sure that game is running before connecting again")
        return True




    def disableDOF(self, value):
        """
        Toggle DOF on or Off

        Arguments:
            value (boolean): whether dof should be enabled
        """
        self.DISABLEDOF = value


    def getSelectedCamera(self):
        """
        Gets the selected camera from Motion Builder
        """
        self.CurrentCamera = Globals.CameraSwitcher.CurrentCamera
        self.CurrentCamera.Selected = True


    def setSwitcherCamera(self):
        """
        Sets the switcher camera
        """
        # deselect any cameras we have currently
        self.ResetSelection()
        self.getSelectedCamera()


    def enableSwitcher(self, value):
        """ Enables """
        self.USESWITCHER = value


    def enableFullRange(self, value):
        """
        enables full range
        Arguments:
            value (boolean): whether enable Full Rane Load should be enabled
        """
        self.USEFULLRANGE = value
        self.ReadStartRange()
        self.ReadEndRange()


    def enableRPFLoad(self, value):
        """
        enable RPF Load

        Arguments:
            value (boolean): whether enable RPF Load should be enabled
        """
        self.LOADRPF = value


    def enableOptimizations(self, value):
        """
        Enables the Parallel

        Arguments:
            value (boolean): whether the parallel evaluation option should be on or off
        """
        Globals.EvaluateManager.ParallelEvaluation = value


    def showInterface(self, value):
        """
        Toggle the game interface

        Arguments:
            value (boolean): whether the game interface should be on or off
        """

        if self.client:
            if self._runClientSafeCmd(self.client.WidgetExists, widget.widgetBankHideTrackEvents):
                self._runClientSafeCmd(self.client.WriteBoolWidget, widget.widgetBankHideTrackEvents, not value)
                self._runClientSafeCmd(self.client.WriteBoolWidget, widget.widgetBankHideInterface,  value)


    def _toggleAutoBlendout(self, value):
        """
        Private method that toggles the auto blendout behavior in the game

        Arguments:
            value (boolean): whether the autoblend should be on or off
        """

        if self.client:
            if self._runClientSafeCmd(self.client.WidgetExists, widget.widgetToggleBlendout):
                self._runClientSafeCmd( self.client.WriteBoolWidget, widget.widgetToggleBlendout,  value)


    def toggleAutomaticBlendOut(self, value):
        """
        Toggle the animscene blendout

        Arguments:
            value (boolean): whether the blend out should be on or off
        """
        self.ENABLEDBLENDOUT = value
        self._toggleAutoBlendout(self.ENABLEDBLENDOUT)


    def toggleDOFOverlay(self, value):
        """
            Toggle DOF Overlay

        Arguments:
            value: (boolean)
        """

        self.ENABLEDOFOVERLAY = value
        self._toggleDOFOverlay()


    def _toggleDOFOverlay(self):
        """
        Private method that toggles the DOF Overlay in the game

        Arguments:
            value (boolean): whether the dof Overlay should be on or off
        """
        if self.client:
            if self._runClientSafeCmd(self.client.WidgetExists, widget.widgetDOFOverlay):
                self._runClientSafeCmd( self.client.WriteBoolWidget, widget.widgetDOFOverlay,  self.ENABLEDOFOVERLAY)


    def toggleDOFSummary(self, value):
        """

        :param value:
        :return:
        """
        self.ENABLEDOFSUMMARY = value
        self._toggleDOFSummary()


    def _toggleDOFSummary(self):
        """
            Private function to toggle DOF summary on/off
        Returns:

        """
        if self.client:
            if self._runClientSafeCmd(self.client.WidgetExists, widget.widgetDOFSummary):
                self._runClientSafeCmd( self.client.WriteBoolWidget, widget.widgetDOFSummary, self.ENABLEDOFSUMMARY)


    def toggleLetterbox(self, value):
        """  This will toggle Letterbox mode  """
        self.ENABLELETTERBOX = value
        self._toggleLetterbox()


    def _toggleLetterbox(self):
        """ Private function to Toggle Letterbox mode """
        if self.client:
            if self._runClientSafeCmd(self.client.WidgetExists, widget.widgetToggleLetterbox):
                self._runClientSafeCmd(self.client.WriteBoolWidget, widget.widgetToggleLetterbox, self.ENABLELETTERBOX)
            if self._runClientSafeCmd(self.client.WidgetExists, widget.widgetLetterboxSolid):
                self._runClientSafeCmd(self.client.WriteBoolWidget, widget.widgetLetterboxSolid, self.ENABLELETTERBOX)

    def toggleGridOverlay(self, value):
        """ THis this"""
        self.ENABLEGRIDOVERLAY = value
        self._toggleGridOverlay()

    def _toggleGridOverlay(self):
        """ Private function to Toggle Grid Overlay """
        if self.client:
            if self._runClientSafeCmd(self.client.WidgetExists, widget.widgetGridThirdLines):
                self._runClientSafeCmd(self.client.WriteBoolWidget, widget.widgetGridThirdLines, self.ENABLEGRIDOVERLAY)
            if self._runClientSafeCmd(self.client.WidgetExists, widget.widgetGridCenterLines):
                self._runClientSafeCmd(self.client.WriteBoolWidget, widget.widgetGridCenterLines, self.ENABLEGRIDOVERLAY)


    def setWidgetBankReferences(self):
        """ This will set references to the widgets banks we need to Read """

        self.client.AddBankReference("Anim Scenes")
        self.client.AddBankReference("Camera")
        self.client.AddBankReference("Renderer")
        self.client.AddBankReference("Marketing Tools")
        self.client.AddBankReference("Camera/Renderer/CamMtx")
        self.client.AddBankReference("Cascade Shadows")
        self.client.AddBankReference("Cascade Shadows/Entity Tracker")


    def _getAnimSceneList(self):
        """
            Read what is the currently loaded Animscene
        Returns:

        """
        if self.client:
            if self._runClientSafeCmd(self.client.WidgetExists, widget.CutsceneSceneList):
                self._LoadedAnimSceneName = self._runClientSafeCmd(self.client.ReadStringWidget, widget.CutsceneSceneList)



    def getShotListPairs(self):
        """ Returns a list of any discarded frame ranges and there offset """
        self.discardedFramesOffsets = []
        trackDictionary = {iTrack.Name: iTrack for iTrack in Globals.Story.RootEditFolder.Tracks}
        trackList = trackDictionary.keys()
        trackList.sort()
        offset = 0
        for iTrack in trackList:
            iTrack = trackDictionary[iTrack]
            if iTrack.Name.endswith('_EST'):
                for iClip in iTrack.Clips:

                    clip_in = int(iClip.MarkIn.GetTimeString().replace("*", ""))
                    clip_out = int(iClip.MarkOut.GetTimeString().replace("*", ""))

                    # only add a new ranage if there is a gap between the last range
                    items = len(self.discardedFramesOffsets)
                    if items > 0:

                        if self.discardedFramesOffsets[items-1][1] == clip_in:
                            self.discardedFramesOffsets[items-1][1] = clip_out
                        else:
                            offset = offset + (clip_in - self.discardedFramesOffsets[items-1][1])
                            self.discardedFramesOffsets.append([clip_in,clip_out, offset])
                    else:
                        self.discardedFramesOffsets.append([clip_in,clip_out, 0])


    def intiRequiredWidgets(self):
        """ Enable any widgets we need to expand (Mightnot need this) """
        # Open up our Camera widget
        if self.getClient():


            if self._runClientSafeCmd(self.client.WidgetExists,  widget.widgetCamera):
                self._runClientSafeCmd(self.client.PressButton,  widget.widgetCamera)
                self.client.SendSyncCommand(0)

            # Need to check if this is opened or not
            if not self._runClientSafeCmd(self.client.WidgetExists,  widget.CutsceneSceneList):
                self._runClientSafeCmd(self.client.PressButton, widget.CutsceneBankToggle)
                self.client.SendSyncCommand(0)

            # cehck if renderer is open
            if not self._runClientSafeCmd(self.client.WidgetExists,  widget.widgetOverdraw):
                self._runClientSafeCmd(self.client.PressButton, widget.widgetBankRenderer)
                self.client.SendSyncCommand(0)

            # This is a sub bank widget
            #NOTE: NOT AVALIABLE IN GTA
            '''
            if not self._runClientSafeCmd(self.client.WidgetExists,  widget.widgetDOFOverlay):
                self._runClientSafeCmd(self.client.PressButton, widget.widgetBankPostFx)
                self.client.SendSyncCommand(0)
            '''



    def CloseRequiredWidgets(self):
        """ Reset any widgets we needed enabled for remote settings """
        self.toggleDOFOverlay(False)
        self.toggleDOFSummary(False)
        self.toggleLetterbox(False)
        self.toggleGridOverlay(False)

        # GTA ONLY
        if self._runClientSafeCmd(self.client.WidgetExists, widget.CutsceneSceneList):

            # Sets the DOF Value to negative -1, essentially disabling our DOF override
            self.WriteFocus(-1)
            self._runClientSafeCmd( self.client.WriteBoolWidget, widget.CameraOverrideUsingMatrixWidgetName, False)
            #self._runClientSafeCmd( self.client.WriteBoolWidget, widget.widgetOverrideFOV, False )  # Turn off FOV override
            if self.CUTSCENE_MODE:
                self._runClientSafeCmd(self.client.PressButton, widget.CutsceneStopSelectedCutScene)




    def SynchCutscenePlayback(self):
        """ Play back our cutscene when uses clicks sync to Animscane or cutscene. """
        if self.client:
            self.getShotListPairs()

            self.cutsceneName = self.ReadFBXExportName()

            # Check if we are using Preview Data
            self._isUsingPreviewData()

            # Check Merge Dict for Matched Merge
            matchedMerges = [mergeName for mergeName, partList in self.mergeDict.iteritems()
                             if self.cutsceneName in partList and partList.index(self.cutsceneName) == len(partList) - 1]
            if matchedMerges:
                self.cutsceneName = matchedMerges[0]


            self.ENABLE_FRAME_UPDATE = True

            if self.getClient():

                if self._runClientSafeCmd( self.client.WidgetExists, widget.CutsceneSceneList):
                    # clear any search filter in the serach filter text box
                    self._runClientSafeCmd( self.client.WriteStringWidget, widget.CutsceneSceneList, '')
                    self._runClientSafeCmd( self.client.SendSyncCommand, 0)
                    # Select our animscene in the select animscene in the dropdown list
                    self._runClientSafeCmd( self.client.WriteStringWidget, widget.CutsceneSceneList, self.cutsceneName)
                    self._runClientSafeCmd( self.client.SendSyncCommand, 0)

                    # Need to check here to see if the cutscene exits: We read the selected animscene in the combo list
                    if not self.CUTSCENE_MODE:
                        # Enable our camera to override the camera maxtrix
                        # GTA
                        self._runClientSafeCmd(self.client.WriteBoolWidget, widget.CameraOverrideUsingMatrixWidgetName, True)
                        # RDR
                        # self._runClientSafeCmd(self.client.WriteBoolWidget, widget.widgetBankAnimSceneOverrideMatrix,True)
                        #self._runClientSafeCmd(self.client.WriteBoolWidget, widget.widgetBankHideTrackEvents, True)
                        self.ValidateCutsceneOffset()
                        self._toggleLetterbox()
                        self.ReadAnimOffsets()      # Might not need to do this if reading from fbx file or animscene ???
                        return True

                    else:
                        _validCutscene = self._runClientSafeCmd( self.client.ReadStringWidget, widget.CutsceneSceneList).lower()

                        if self.cutsceneName == _validCutscene:
                            # Press Load cutscene!!!
                            if Config.Project.Name.upper() == 'RDR3':
                                # load in the cutscenes
                                if self.LOADRPF:
                                    self._runClientSafeCmd( self.client.PressButton, widget.CutsceneSceneStartButtonRpf )
                                else:
                                    self._runClientSafeCmd( self.client.PressButton,  widget.CutsceneSceneStartButton )
                            else:
                                self._runClientSafeCmd( self.client.PressButton, widget.CutsceneStopSelectedCutScene )
                            self._runClientSafeCmd( self.client.SendSyncCommand, 0)

                            time.sleep(2)
                            # Enable our camera to override the camera maxtrix
                            self._runClientSafeCmd( self.client.WriteBoolWidget, widget.CameraOverrideUsingMatrixWidgetName, True)

                            if not self.ValidateAnimsceneLoad():
                                print 'not fail'
                                msg = "Cutscene '{}' does not Exist in your build, either export or sync to newer build if available".format(self.cutsceneName)
                                self.remoteSignal.wcf_disconnect(False, msg)
                            else:
                                #self._runClientSafeCmd( self.client.WriteBoolWidget, widget.widgetBankHideTrackEvents, True )
                                self.ValidateCutsceneOffset()

                                # Run any other widget settings
                                self._toggleAutoBlendout(self.ENABLEDBLENDOUT)
                                self._toggleLetterbox()
                        else:
                            print "fail!"
                            msg= "Animscene '{}' does not Exist in your build, either export or sync to newer build if available".format(self.cutsceneName)
                            self.remoteSignal.wcf_disconnect(False, msg)
                        return True
                else:
                    return False
            return False

    def CloseAnimScene(self):
        """ Closes the Anim scene Rag Widget """
        self.CUTSCENE_MODE = False
        if self._runClientSafeCmd( self.client.WidgetExists, widget.CutsceneSceneStartButton ):
            # We use the close ANim Scene widget as if you toggle the Anim scenes widget it will hide the player
            self._runClientSafeCmd( self.client.PressButton, widget.CutsceneStopSelectedCutScene )

    def SetupWorldForCutscene(self):
        """ Presses the Setup for world cutscene button """
        if self._runClientSafeCmd( self.client.WidgetExists, widget.widgetSetupWorldForCutscene ):
            # Setup world for Cutscene
            self._runClientSafeCmd( self.client.PressButton, widget.widgetSetupWorldForCutscene)
            self._runClientSafeCmd( self.client.SendSyncCommand, 0)

    def SyncAnimScene(self):
        """ Reads the Anim Scene offsets """
        if self.client:
            self.CUTSCENE_MODE = False
            self.lastFrame = 0
            return self.ReadAnimOffsets()

    def SyncGameOffsets(self):
        """ Reads the Free camera offsets """
        if self.client:
            self.CUTSCENE_MODE = False
            self.readGameFreeCameraOffsets()

    def SyncGameCutsceneOffsets(self):
        """ Set up Game camera to use Cutscene Offsets """
        if self.client:
            self.CUTSCENE_MODE = False
            self.ReadFBXOffsets()


    def startStopCutscene(self):
        """ Starts or stops the cutscene, the widget is an on or off mode """
        return self.SynchCutscenePlayback()


    def startAnimScene(self):
        """ Syncs the anim scene"""
        return self.SyncAnimScene()

    """
        Read Methods
    """

    def ValidateAnimsceneLoad(self):
        """ Need to be aware that some widegts may get disabled by code! """
        if not self.CUTSCENE_MODE:
            return True

        # GTA ONLY
        return True
        '''
        if not self._runClientSafeCmd( self.client.WidgetExists, widget.CutsceneFrameCaptureWidgetName ):
            return False
        else:
            return True
        '''

    def ValidateCutsceneOffset(self):
        """ Validates the cutscene offsets """
        isValid = True
        self.ReadFBXOffsets()
        if self.getClient():
            if self._runClientSafeCmd(self.client.WidgetExists, widget.CutsceneSceneOrientationWidgetName):
                _offset = self._runClientSafeCmd( self.client.ReadVector3Widget, widget.CutsceneSceneOrientationWidgetName )
                if abs(_offset.X-self.OFFSET_X) < 5.1:
                    isValid = False
                if abs(_offset.Y-self.OFFSET_Y) < 5.1:
                    isValid = False
                if abs(_offset.Z-self.OFFSET_Z) < 5.1:
                    isValid = False
        return isValid

    def IsCutscenePlaying(self):
        """ Is the cutscene playing """
        if self.getClient():
            if self._runClientSafeCmd( self.client.WidgetExists, widget.CutsceneSceneOrientationWidgetName ):
                _offset = self._runClientSafeCmd( self.client.ReadVector3Widget, widget.CutsceneSceneOrientationWidgetName )
                if _offset.X == 0.0:
                    return False
                else:
                    return True
            return False
        return False

    # Private
    def ReadFBXOffsets(self):
        """
            Read any offsets from the Mobu File.
            Might be better to read from Game
        """
        if self.CUTSCENE_MODE:
            self.OFFSET_X = RexExport.GetCutsceneOffsetsX()
            self.OFFSET_Y = RexExport.GetCutsceneOffsetsY()
            self.OFFSET_Z = RexExport.GetCutsceneOffsetsZ()
            self.OFFSET_RZ = RexExport.GetCutsceneOrientation()

            if self.OFFSET_X == -1.0 and self.OFFSET_Y == -1.0 and self.OFFSET_Z == -1.0:

                # There's no valid cutfile/setup then lets get the position fo rGame
                message = ("Cutscene does not have any shots set up in the Cutscene Exporter\n"
                           "You will need to import a Part file to get the location")
                self.remoteSignal.wcf_disconnect(False,
                                                 message)
                raise RSException.SilentError(message)



    def readCutsceneOffsets(self):
        """ Read any offset setup from any init cutscenes """
        if self.getClient():
            if self._runClientSafeCmd( self.client.WidgetExists, widget.CutsceneSceneOrientationWidgetName ):
                _offset = self._runClientSafeCmd( self.client.ReadVector3Widget, widget.CutsceneSceneOrientationWidgetName )
                _rotation = self._runClientSafeCmd( self.client.ReadVector3Widget, widget.widgetSceneOffsetRotation )

            self.OFFSET_X = _offset.X
            self.OFFSET_Y = _offset.Y
            self.OFFSET_Z = _offset.Z

            self.OFFSET_RX =_rotation.X
            self.OFFSET_RY =_rotation.Y
            self.OFFSET_RZ =_rotation.Z

    def readGameFreeCameraOffsets(self):
        """ Reads the Free CAmera Offset Values """

        if self.getClient():
            self.OFFSET_X = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetFreeCameraPX )
            self.OFFSET_Y = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetFreeCameraPY )
            self.OFFSET_Z = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetFreeCameraPZ )


    def ReadAnimOffsets(self):
        """ Reads the anim offsets """
        if self.getClient():
            # INGAME ONLY
            if not self.CUTSCENE_MODE:
                self._getAnimSceneList()
                if self._LoadedAnimSceneName:
                    animScenePositionWidgetPath = "Anim Scenes/Current Anim Scene/Scene Details/{}/sceneOrigin/position [current]".format(
                        self._LoadedAnimSceneName)
                    animSceneorientationWidgetPath = "Anim Scenes/Current Anim Scene/Scene Details/{}/sceneOrigin/orientation [current]".format(
                        self._LoadedAnimSceneName)

                    if self._runClientSafeCmd(self.client.WidgetExists, animScenePositionWidgetPath):
                        _offsetPos = self._runClientSafeCmd(self.client.ReadVector3Widget,
                                                            animScenePositionWidgetPath)
                        _offsetRot = self._runClientSafeCmd(self.client.ReadVector3Widget,
                                                            animSceneorientationWidgetPath)

                        self.OFFSET_X = _offsetPos.X
                        self.OFFSET_Y = _offsetPos.Y
                        self.OFFSET_Z = _offsetPos.Z #+ -0.005           # THis is very odd as there is a small offset!!!!

                        self.OFFSET_RX = _offsetRot.X
                        self.OFFSET_RY = _offsetRot.Y
                        self.OFFSET_RZ = _offsetRot.Z
                        return True
                    else:
                        return False
            else:
                if self._runClientSafeCmd( self.client.WidgetExists, widget.widgetBankAnimScenePosition):
                    _offsetPos = self._runClientSafeCmd( self.client.ReadVector3Widget, widget.widgetBankAnimScenePosition )
                    _offsetRot = self._runClientSafeCmd( self.client.ReadVector3Widget, widget.widgetBankAnimSceneRotation )

                    self.OFFSET_X = _offsetPos.X
                    self.OFFSET_Y = _offsetPos.Y
                    self.OFFSET_Z = _offsetPos.Z

                    self.OFFSET_RX =_offsetRot.X
                    self.OFFSET_RY =_offsetRot.Y
                    self.OFFSET_RZ =_offsetRot.Z
                    return True
                else:
                    return False
        return False

    def EnableFreeCamera(self, bFreeCam):
        """
        This will Enable or Disable the Freecamera Mode

        Arguments:
            bFreeCam (boolean): should free cam be enabled
        """
        if self.getClient():
            if self._runClientSafeCmd( self.client.WidgetExists, widget.widgetCameraActivemode ):
                if not bFreeCam:
                    print "set to Free Camera"
                    # Read the current Position and Rotation Values
                    PosX = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraDX )
                    PosY = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraDY )
                    PosZ = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraDZ )

                    self._runClientSafeCmd( self.client.WriteStringWidget, widget.widgetCameraActivemode, "Free")

                    if self.getClient():
                        self._runClientSafeCmd( self.client.WriteFloatWidget, widget.widgetFreeCameraPX, PosX)
                        self._runClientSafeCmd( self.client.WriteFloatWidget, widget.widgetFreeCameraPY, PosY)
                        self._runClientSafeCmd( self.client.WriteFloatWidget, widget.widgetFreeCameraPZ, PosZ)
                else:
                    print "Disabled Free Camera"
                    self._runClientSafeCmd( self.client.WriteStringWidget, widget.widgetCameraActivemode, "None")

            if self._runClientSafeCmd( self.client.WidgetExists, widget.widgetDebugPad ):
                self._runClientSafeCmd( self.client.WriteBoolWidget, widget.widgetDebugPad, bFreeCam)

    def ReadFBXExportName(self):
        """
        Reads the Exporter's export Name in the Rag Widget

        Return:
            string
        """
        self.CUTSCENE_MODE = False
        if 'cutscene' in Globals.Application.FBXFileName.lower():
            self.CUTSCENE_MODE = True

        ctypes.cdll.rexMBRage.GetCutsceneSceneName_Py.restype = ctypes.c_char_p
        return ctypes.cdll.rexMBRage.GetCutsceneSceneName_Py().lower()


    def ReadStartRange(self):
        """ Reads the first shot and its Range """
        if self.USEFULLRANGE:
            self.FRAME_OFFSET = 0
        else:
            self.FRAME_OFFSET = -ctypes.cdll.rexMBRage.GetCutsceneShotStartRange_Py( 0 )


    def ReadEndRange(self):
        """ Reads the end range from Motion Builder """
        self.FRAME_END = int(Globals.System.CurrentTake.LocalTimeSpan.GetStop().GetTimeString().replace('*',''))


    def ReadMobuCamera(self):
        """ Read a Mobu Camera Matrix """
        self.CurrentCamera.GetCameraMatrix(self.CamMatrix,  mobu.FBCameraMatrixType.kFBModelView)


    def ReadMobuFocus(self):
        """  Read our Camera Focus Marker Property on the current camera """
        _dof = self.CurrentCamera.PropertyList.Find('FOCUS (cm)')
        if _dof:
            self.Dof = _dof


    def ReadGameCameraFov(self):
        """ Read our Camera FOV """
        # Turn Off override
        if self.getClient():
            self._runClientSafeCmd(self.client.WriteBoolWidget, widget.widgetOverrideFOV, False)
            self.Fov = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetInputFov )


    def ReadGameCamera(self):
        """ Read the Game Camera Matrix """
        # Get our Camera Matrix
        lFinalMatrix = mobu.FBMatrix()
        if self.getClient():
            aX = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraAX )
            aY = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraAY )
            aZ = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraAZ )

            bX = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraBX )
            bY = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraBY )
            bZ = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraBZ )

            cX = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraCX )
            cY = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraCY )
            cZ = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraCZ )

            dX = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraDX )
            dY = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraDY )
            dZ = self._runClientSafeCmd( self.client.ReadFloatWidget, widget.widgetCameraDZ )

            # Record the values to our local Game matrix
            self.CamMatrix[0] = aX
            self.CamMatrix[1] = aY
            self.CamMatrix[2] = aZ

            self.CamMatrix[4] = bX
            self.CamMatrix[5] = bY
            self.CamMatrix[6] = bZ

            self.CamMatrix[8] = cX
            self.CamMatrix[9] = cY
            self.CamMatrix[10] = cZ

            self.CamMatrix[12] = 0
            self.CamMatrix[13] = 0
            self.CamMatrix[14] = 0

            # add in any offset Rotations from the game ie. Cutscene offsets or Anim scene Offsets
            lAdjustmentMatrixA = mobu.FBMatrix()
            lVector3d = mobu.FBVector3d(self.OFFSET_RY ,self.OFFSET_RX , -self.OFFSET_RZ)
            mobu.FBRotationToMatrix(lAdjustmentMatrixA, lVector3d, mobu.FBRotationOrder.kFBXZY)

            # Multiply these Matricies togather to get Final Result

            mobu.FBMatrixMult(lFinalMatrix, lAdjustmentMatrixA, self.CamMatrix)

            # Add back our Positin offsets
            lFinalMatrix[12] = (dX - self.OFFSET_X) * 100
            lFinalMatrix[13] = (dZ - self.OFFSET_Z) * 100
            lFinalMatrix[14] = ((dY - self.OFFSET_Y) * 100) * -1

        return lFinalMatrix

    """
        Write Methods
    """


    def WriteCameraToAnimscene(self):
        """ Writes the values of the Motionbuilder camera to the current cutscene ingame """
        camMatrix = mobu.FBMatrix()
        self.CurrentCamera.GetMatrix(camMatrix)

        # Rotate our Scene around any offset Rotation
        orbitMatrix = mobu.FBMatrix()
        offsetRotation = mobu.FBVector3d(0, self.OFFSET_RZ, 0)
        mobu.FBRotationToMatrix(orbitMatrix, offsetRotation)

        # Rotate our Camera 90 Degrees to match game View
        offsetMatrix = mobu.FBMatrix()
        gameRotation = mobu.FBVector3d(0, 0, -90)
        mobu.FBRotationToMatrix(offsetMatrix, gameRotation)

        roll = 0
        rollProperty = self.CurrentCamera.PropertyList.Find("Roll")
        if rollProperty is not None:
            roll = rollProperty.Data

        # Apply the roll of the camera
        rollMatrix = mobu.FBMatrix()
        rollRotation = mobu.FBVector3d(0, roll, 0)
        mobu.FBRotationToMatrix(rollMatrix, rollRotation)

        # Multiply all all Matrix's together. Order is Import!!!
        resultMatrix = orbitMatrix * camMatrix * offsetMatrix * rollMatrix

        # Flip our Matrix and swap the Y, Z's around

        self._vectors["x"] = [resultMatrix[8],    resultMatrix[10]*-1, resultMatrix[9]*-1]
        self._vectors["y"] = [resultMatrix[4],    resultMatrix[6]*-1,  resultMatrix[5]]
        self._vectors["z"] = [resultMatrix[0]*-1, resultMatrix[2],     resultMatrix[1]*-1]

        # Ensure the updated matrix is orthonormal
        self._arePerpendicular["xy"] = False,
        self._arePerpendicular["xz"] = False,
        self._arePerpendicular["yz"] = False

        for axisAB in self._arePerpendicular:



            angle = Math.AngleBetweenVectors(self._vectors[axisAB[0]], self._vectors[axisAB[1]])
            # Sometimes numpy returns 90.0 != 90.0, so we are going to use numpy.isclose instead
            self._arePerpendicular[axisAB] = numpy.isclose(angle, 90, 0.0000000000000000000000000000000000000000000001)

        if not all(self._arePerpendicular.itervalues()):
            for vectorsAB, isPerpendicular in self._arePerpendicular.iteritems():
                if isPerpendicular:
                    axis = re.sub("|".join(vectorsAB), "", "xyz")
                    vectorA = Math.Normalize(*self._vectors[vectorsAB[0]])
                    vectorB = Math.Normalize(*self._vectors[vectorsAB[1]])
                    # Get a perpendicular vector from vectors a & b
                    vectorC = Math.CrossProduct(vectorA, vectorB)
                    # Make sure the solved vector is facing the same direction as the original
                    if Math.DotProduct(vectorC, self._vectors[axis]) < 0 and Math.AngleBetweenVectors(self._vectors[axis], vectorC) > 145:
                        vectorC = [value * -1 for value in vectorC]
                    self._vectors[axis] = vectorC
                    break

        lGameMatrix = mobu.FBMatrix([self._vectors["x"][0], self._vectors["x"][1], self._vectors["x"][2], 0,
                                     self._vectors["y"][0], self._vectors["y"][1], self._vectors["y"][2], 0,
                                     self._vectors["z"][0], self._vectors["z"][1], self._vectors["z"][2], 0,
                                     0, 0, 0, 1])

        lGameMatrix[12] = (resultMatrix[12] * MOTIONBUILDER_UNIT_SCALE) + self.OFFSET_X
        lGameMatrix[13] = (resultMatrix[14] * MOTIONBUILDER_UNIT_SCALE) *-1 + self.OFFSET_Y
        lGameMatrix[14] = (resultMatrix[13] * MOTIONBUILDER_UNIT_SCALE) + self.OFFSET_Z

        #lFinalGameMatrix = self.convertMobuCameraToIngame(self.CurrentCamera)
        lFinalGameMatrix = lGameMatrix

        # Convert over to our Rage Matrix by Tranposing the Matrix
        vecA = RageMath.Vector3f(lFinalGameMatrix[0],   lFinalGameMatrix[1],    lFinalGameMatrix[2])
        vecB = RageMath.Vector3f(lFinalGameMatrix[4],   lFinalGameMatrix[5],    lFinalGameMatrix[6])
        vecC = RageMath.Vector3f(lFinalGameMatrix[8],   lFinalGameMatrix[9],    lFinalGameMatrix[10])
        vecD = RageMath.Vector3f(lFinalGameMatrix[12],   lFinalGameMatrix[13],    lFinalGameMatrix[14])
        final_matrix = RageMath.Matrix34f(vecA, vecB, vecC, vecD)

        if self.getClient():
            if Config.Project.Name.upper() == 'RDR3':
                self._runClientSafeCmd(self.client.WriteMatrix34Widget, widget.widgetBankWriteCameraMatrix, final_matrix)
            else:
                self._runClientSafeCmd(self.client.WriteMatrix34Widget, widget.CameraMatrixWidgetName, final_matrix)


    def convertMobuCameraToIngame(self, camera):
        """
        Converts the camera translation and rotation values from motion builder to ingame.
        Currently this method does not work properly

        Arguments:
            camera (pyfbsdk.FBCamera): camera to get matrix from

        Return:
            pyfbsdk.FBMatrix
        """
        # Get the camera quaternion and convert it to a transform matrix

        cameraMatrix = mobu.FBMatrix()
        camera.GetMatrix(cameraMatrix)

        if self.CUTSCENE_MODE:
            # Adds a scene offset to the matrix
            cameraMatrix = mobu.FBMatrix(Math.AngleToMatrix(self.OFFSET_RZ, "y")) * cameraMatrix

        # Store the offset translation and set the current translation to world
        translation = [cameraMatrix[12], cameraMatrix[13], cameraMatrix[14]]
        cameraMatrix[12], cameraMatrix[13], cameraMatrix[14] = 0, 0, 0

        # Add roll offset and additional offsets
        cameraMatrix = cameraMatrix * mobu.FBMatrix(Math.AngleToMatrix(camera.Roll, "x"))
        cameraMatrix = cameraMatrix * mobu.FBMatrix(Math.AngleToMatrix(self.test_x, "x")) * mobu.FBMatrix(Math.AngleToMatrix(self.test_y, "y")) * mobu.FBMatrix(Math.AngleToMatrix(self.test_z, "z"))

        # Rotate the camera so Z is facing up
        zupIdentityMatrix = mobu.FBMatrix([-1, 0, 0, 0,
                                           0, 0, 1, 0,
                                           0, 1, 0, 0,
                                           0, 0, 0, 1])
        zupIdentityMatrix.Inverse()
        cameraMatrix = zupIdentityMatrix * cameraMatrix

        # Add back the original translation to the rig
        cameraMatrix[12] = translation[0] * MOTIONBUILDER_UNIT_SCALE
        cameraMatrix[13] = translation[2] * MOTIONBUILDER_UNIT_SCALE * -1
        cameraMatrix[14] = translation[1] * MOTIONBUILDER_UNIT_SCALE

        if self.CUTSCENE_MODE:
            cameraMatrix[12] += self.OFFSET_X
            cameraMatrix[13] += self.OFFSET_Y
            cameraMatrix[14] += self.OFFSET_Z

        return cameraMatrix

    def WriteFovToGame(self):
        """ Write the FOV to game """
        if self.getClient():
            if self._runClientSafeCmd( self.client.WidgetExists, widget.widgetOverrideFOV):
                self._runClientSafeCmd( self.client.WriteBoolWidget, widget.widgetOverrideFOV, True)
                self._runClientSafeCmd( self.client.WriteFloatWidget, widget.widgetOverridenFOV, self.CurrentCamera.FieldOfView)

    def ReadWriteMobuFocus(self):
        """ Write Mobu DOF to Game """
        dofValue = -1
        if not self.DISABLEDOF:
            self.ReadMobuFocus()
            dofValue = self.Dof * MOTIONBUILDER_UNIT_SCALE

        self.WriteFocus(dofValue)

    def WriteFocus(self, value):
        """
        Sets the DOF Value ingame

        Args:
            value (float): dof value
        """
        if self.getClient():
            if self._runClientSafeCmd(self.client.WidgetExists, widget.widgetDOF):
                self._runClientSafeCmd(self.client.WriteFloatWidget, widget.widgetDOF, value)

    def WriteCameraToMobu(self):
        """ Write Camera Data to Mobu """
        # Convert our Game Matrix to EulerMatrix
        lFinalMatrix = MathGameUtils.convertGameToMobu( self.CamMatrix )

        # Set the matrix and update scene
        self.CurrentCamera.SetMatrix( lFinalMatrix )
        self.CurrentCamera.FieldOfView = self.Fov

    def ResetSelection(self):
        """ De-Select any models we have selected """
        selectedModels = mobu.FBModelList()
        mobu.FBGetSelectedModels( selectedModels, None, True )
        for model in selectedModels:
            model.Selected = False

    def UpdateWidgets(self):
        '''
            Called on frame update
        :return:
        '''
        return




    def UpdateFrame(self):
        """ Update the Game frame number if its a cutscene """
        if self.ENABLE_FRAME_UPDATE:

            CurrentFrame = Globals.System.LocalTime.GetFrame()
            if self.CUTSCENE_MODE:
                if CurrentFrame < self.FRAME_END:
                    if len(self.discardedFramesOffsets) > 0 and not self.USEFULLRANGE:
                        for i in range(0, len(self.discardedFramesOffsets)):
                            if CurrentFrame >= self.discardedFramesOffsets[i][0] and CurrentFrame <= self.discardedFramesOffsets[i][1]:
                                shot_offset = self.discardedFramesOffsets[i][2]
                                total_offset = CurrentFrame + self.FRAME_OFFSET - shot_offset
                                if hasattr(self.client, "WriteIntWidget"):
                                    if self.getClient():
                                        self._runClientSafeCmd( self.client.WriteIntWidget, widget.CutsceneCurrentFrameWidgetName, total_offset )

                    # Has no shot Ranges so lets use Full Range
                    else:
                        total_offset = CurrentFrame + self.FRAME_OFFSET
                        if total_offset < 0:
                            print "warning, NO est set up it looks like!!!"
                        if self.getClient():
                            self._runClientSafeCmd(self.client.WriteIntWidget, widget.CutsceneCurrentFrameWidgetName, total_offset)

            else:
                # For updating anim Sync scenes. Pretty crappy
                if CurrentFrame < self.FRAME_END:
                    self._runClientSafeCmd(self.client.WriteIntWidget, widget.CutsceneCurrentFrameWidgetName,
                                           CurrentFrame)


                '''
                
                if CurrentFrame > self.lastFrame:
                    if self.getClient():
                        self._runClientSafeCmd(self.client.WriteIntWidget, widget.CutsceneCurrentFrameWidgetName,
                                               CurrentFrame)
                        
                        #self._runClientSafeCmd( self.client.PressVCRButton, widget.widgetBankAnimScenePlayback,3 )
                    self.lastFrame = CurrentFrame
                elif CurrentFrame < self.lastFrame:
                    if self.getClient():
                        self._runClientSafeCmd( self.client.PressVCRButton, widget.widgetBankAnimScenePlayback,4 )
                    self.lastFrame = CurrentFrame
                '''

    """
        MB Callbacks
    """

    def SendCameraUpdatesToMobu(self):
        """ Wrapper callback methods to read the Game camera and Update Mobu's Camera """
        self.ReadGameCameraFov()
        self.CamMatrix = self.ReadGameCamera()
        self.WriteCameraToMobu()


    def SendCameraUpdatesToGame(self):
        """ Wrapper Callback methods to read Mobu Camera and Update Game Camera """

        if self.USESWITCHER:
            self.CurrentCamera = Globals.CameraSwitcher.CurrentCamera
            if self.Lastcamera != self.CurrentCamera:
                # delesect old camera
                if self.Lastcamera:
                    self.Lastcamera.Selected = False
                # new camera == last camera
                self.Lastcamera = self.CurrentCamera
                # select new camera
                self.CurrentCamera.Selected = True
        # DOF
        self.ReadWriteMobuFocus()
        # FOV
        self.WriteFovToGame()
        self.WriteCameraToAnimscene()
        self.UpdateFrame()
        self.UpdateWidgets()


    # Check if Camera is selected
    def OnConnectionStateNotify( self, control, event ):
        """ Sets the current camera """
        if event.Action ==  mobu.FBConnectionAction.kFBSelect:

            if event.Plug.ClassName() == 'FBCamera':
                self.CurrentCamera = event.Plug

            if event.Plug.ClassName() == ' FBCameraSwitcher':
                self.CurrentCamera = event.Plug.CurrentCamera

    def OnSyncUpdateCameraPosition( self, control, event):
        """ Update our camera in game or in Mobu """
        try: 
            if self.CurrentCamera:
                if self.GAME_MODE:
                    self.SendCameraUpdatesToMobu()
                else:
                    self.SendCameraUpdatesToGame()

        except MissingGameError as exception:
            self.Disconnect()
            self.remoteSignal.wcf_disconnect(False, exception.message)

    def OnFileOpenCompleted(self, control, event):
        """ Disconect any connection """
        self.Disconnect()

    """
    Register Callbacks
    """

    def Register(self):
        """ Registers the callbacks """
        Globals.Callbacks.OnSynchronizationEvent.Add(self.OnSyncUpdateCameraPosition)
        Globals.Callbacks.OnConnectionStateNotify.Add(self.OnConnectionStateNotify)
        Globals.Callbacks.OnFileOpenCompleted.Add(self.OnFileOpenCompleted)
        Globals.Callbacks.OnFileNew.Add(self.OnFileOpenCompleted)
        Globals.Callbacks.OnFileExit.Add(self.Unregister)

    def Unregister(self, control=None, event=None):
        """ Unregisters the callbacks """

        try:
            Globals.Callbacks.OnSynchronizationEvent.Remove(self.OnSyncUpdateCameraPosition)
            Globals.Callbacks.OnConnectionStateNotify.Remove(self.OnConnectionStateNotify)
            Globals.Callbacks.OnFileOpenCompleted.Remove(self.OnFileOpenCompleted)
            Globals.Callbacks.OnFileNew.Remove(self.OnFileOpenCompleted)
            Globals.Callbacks.OnFileExit.Remove(self.Unregister)

        except Exception as err:
            print err.message
