from xml.etree import ElementTree
import RS

class atdict(dict):
    __getattr__ = dict.__getitem__


cfgSettings = 'remote_console_settings.xml'

# Load in our config settings, this is shared between the tools Config
_configPath = "{0}\\config\\anim\\{1}".format(RS.Config.Tool.Path.Config, cfgSettings)

_cmds = dict()
root = ElementTree.parse(_configPath).getroot()
for child in root:
    for item in child.getchildren():
        _cmds[item.get('key')] = item.text




# These are values not in the config file.

_cmds['widgetCameraOverrideCam']            = 'Cut Scene Debg/Current Scene/Camera Editing/Camera Overrides'
_cmds['widgetSceneOffsetRotation']          = 'Cut Scene Debug/Current Scene/Scene Properties/Scene Heading'

_cmds['widgetSetupWorldCutscene']           = 'Cut Scene Debug/Setup world for cutscene'

_cmds['widgetSearchCutscene']               = 'Anim Scenes/Load scene/Scene name selector: /Enter search text:'

_cmds['widgetCamera']                       = 'Camera/Create camera widgets'
_cmds['widgetOverrideFOV']                  = 'Camera/Frame propagator/Override FOV'
_cmds['widgetOverridenFOV']                 = 'Camera/Frame propagator/Overridden FOV'

_cmds['widgetCameraAX']                     = 'Camera/Renderer/CamMtx/WorldMtx a X'
_cmds['widgetCameraAY']                     = 'Camera/Renderer/CamMtx/WorldMtx a Y'
_cmds['widgetCameraAZ']                     = 'Camera/Renderer/CamMtx/WorldMtx a Z'

_cmds['widgetCameraBX']                     = 'Camera/Renderer/CamMtx/WorldMtx b X'
_cmds['widgetCameraBY']                     = 'Camera/Renderer/CamMtx/WorldMtx b Y'
_cmds['widgetCameraBZ']                     = 'Camera/Renderer/CamMtx/WorldMtx b Z'

_cmds['widgetCameraCX']                     = 'Camera/Renderer/CamMtx/WorldMtx c X'
_cmds['widgetCameraCY']                     = 'Camera/Renderer/CamMtx/WorldMtx c Y'
_cmds['widgetCameraCZ']                     = 'Camera/Renderer/CamMtx/WorldMtx c Z'

_cmds['widgetCameraDX']                     = 'Camera/Renderer/CamMtx/WorldMtx d X'
_cmds['widgetCameraDY']                     = 'Camera/Renderer/CamMtx/WorldMtx d Y'
_cmds['widgetCameraDZ']                     = 'Camera/Renderer/CamMtx/WorldMtx d Z'

_cmds['widgetFreeCameraPX']                 = 'Camera/Free camera/Position X'
_cmds['widgetFreeCameraPY']                 = 'Camera/Free camera/Position Y'
_cmds['widgetFreeCameraPZ']                 = 'Camera/Free camera/Position Z'

_cmds['widgetFreeCameraRX']                 = 'Camera/Free camera/Orientation X'
_cmds['widgetFreeCameraRY']                 = 'Camera/Free camera/Orientation Y'
_cmds['widgetFreeCameraRZ']                 = 'Camera/Free camera/Orientation Z'

_cmds['widgetInputFov']                     = 'Cascade Shadows/Entity Tracker/Input FOV'

_cmds['widgetBankRefCamMtrx']               = 'Camera/Renderer/CamMtx'
_cmds['widgetBankRefEntityTracker']         = 'Cascade Shadows/Entity Tracker'
_cmds['widgetBankRefCutsceneDebug']         = 'Cut Scene Debug'
_cmds['widgetBankAnimScene']                = 'Anim Scenes'
_cmds['widgetBankRenderer']                 = 'Renderer/Create Renderer widgets'
_cmds['widgetBankPostFx']                   = "Renderer/Post FX/Create PostFX widgets"

_cmds['CutsceneSceneStartButtonRpf']        = 'Anim Scenes/Load scene/Load scene from rpf'
_cmds['widgetBankAnimSelectAnimScene']      = 'Anim Scenes/Load scene/Scene name selector: /Select animscene'

_cmds['widgetBankAnimScenePosition']        = 'Anim Scenes/Anim scenes/Scene details/sceneOrigin/position'
_cmds['widgetBankAnimSceneRotation']        = 'Anim Scenes/Anim scenes/Scene details/sceneOrigin/orientation'
_cmds['widgetBankAnimScenePlayback']        = 'Anim Scenes/Anim scenes/Playback'

_cmds['widgetBankAnimSceneOverrideMatrix']  = 'Anim Scenes/Remote Console/Camera Editing/Override Cam Using Matrix'
_cmds['widgetBankWriteCameraMatrix']        = 'Anim Scenes/Remote Console/Camera Editing/Cam Matrix'
_cmds['widgetBankHideTrackEvents']          = 'Anim Scenes/Editor Settings/General/Hide interface'

_cmds['widgetBankHideInterface']            = 'Anim Scenes/Editor Settings/General/Hide events tracks'


_cmds['widgetSetupWorldForCutscene']        = 'Anim Scenes/Load scene/Setup world for cutscene anim scene'

# Dropdwon Menu
_cmds['widgetCameraActivemode']             = 'Camera/Debug director/Active mode'

_cmds['widgetDebugPad']                     = 'Input/Pad/Debug pad'

# NEW DOF EDITOR SETTINGS
_cmds['widgetDOF']                          = 'Anim Scenes/Remote Console/Camera Editing/Dof Focus Distance'
_cmds['widgetLensModel']                    = 'Anim Scenes/Remote Console/Camera Editing/Lens Model'

# Blendout
_cmds['widgetToggleBlendout']               = 'Anim Scenes/Editor Settings/Cameras/Prevent camera from blending out.'

# DOF Overlay
_cmds['widgetDOFOverlay']                   = 'Marketing Tools/Depth Of Field [NEW]/Draw DOF Overlay'
_cmds['widgetDOFSummary']                   = 'Marketing Tools/Depth Of Field [NEW]/Draw DOF Summary'

# GRID Overlay
_cmds['widgetGridThirdLines']               = 'Camera/Debug rendering/Render third lines'
_cmds['widgetGridCenterLines']              = 'Camera/Debug rendering/Render center lines'

# Display Letter Box
_cmds['widgetToggleLetterbox']              = 'Marketing Tools/Cinematic Controls/Display Letter Box'
_cmds['widgetLetterboxSolid']               = 'Anim Scenes/Editor Settings/General/Make LetterBox solid in-editor(will hide some of the editor UI)'

_cmds['widgetOverdraw']                     = 'Renderer/Overdraw/Render Overdraw'




widget = atdict(_cmds)