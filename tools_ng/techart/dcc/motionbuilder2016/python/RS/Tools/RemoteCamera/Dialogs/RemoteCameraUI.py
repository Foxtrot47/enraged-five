__DoNotReload__ = True # Module is NOT safe to be dynamciy reloaded

import os
import time
import functools

from PySide import QtGui, QtCore

from RS import Config, Globals
from RS.Tools.RemoteCamera.Models import RemoteModel
from RS.Core.Automation.FrameCapture import ExportDataTools, FileTools, CapturePaths

reload(RemoteModel)


class DisplayError(object):
    """
    Brings up an error bug for custom errors from the Remote Model class

    Arguments:
        func (function):

    Returns:
        function
    """
    _depth = 0

    def __call__(self, func):
        """
         Overrides built-in method
         Arguments:
             func (function): decorated function
        """
        @functools.wraps(func)
        def inner(*args, **kwargs):
            with self:
                self._instance = args[0]
                return func(*args, **kwargs)
        return inner

    def __init__(self):
        self._instance = None

    def __enter__(self):
        """
        Overrides built-in method
        Raises the depth count
        """
        DisplayError._depth += 1

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Overrides built-in method

        Handles exceptions if they bubble up all the way to the top most depth level.
        """
        DisplayError._depth -= 1
        if exc_type == RemoteModel.MissingGameError and DisplayError._depth <= 0:
            QtGui.QMessageBox.critical(self._instance, "Missing Game Error", exc_val.message)
            return True # Stop the error from raising by returning True

class RemoteSignal(QtCore.QObject):
    """
        Simple class to handle any remote signal calls back to the UI if the game connection
        fails for whatever reason
    """

    disconnected = QtCore.Signal(bool, str)
    fullRangeState = QtCore.Signal(bool)

    def __init__(self):
        # Initialize the PunchingBag as a QObject
        QtCore.QObject.__init__(self)

    def wcf_disconnect(self, state, msg=None):
        self.disconnected.emit(state, msg)

    def fullRangeStateChanged(self, state):
        self.fullRangeState.emit(state)

    def disconnectSignal(self):
        """ Removes methods attached to the signals """
        try:
            self.disconnected.disconnect()
            self.fullRangeState.disconnect()
        except RuntimeError:
            pass # If this somehow gets called when the signals have no methods attached to them it errors out

remoteSignal = RemoteSignal()
con = RemoteModel.RemoteConnectionModel(remoteSignal)


class SetupWidget(QtGui.QWidget):
    def __init__(self, parent=None, f=0):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
            f (QtCore.Qt.WindowFlags): flags to use for the widget
        """

        self.connectionStates = {
            True:   "Disconnect",
            False: "Connect"
            }


        remoteSignal.disconnected.connect(self._setDisconnectButton)
        remoteSignal.fullRangeState.connect(self._setEventFullRange)


        super(SetupWidget, self).__init__(parent=parent)
        layout = QtGui.QVBoxLayout()

        # ICONS
        self.iconMouse = QtGui.QIcon(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "Remote", "camera_Mouse.png")))
        self.iconController = QtGui.QIcon(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "Remote", "camera_Controller.png")))

        # Toggle Connect Button
        self.connectButton = QtGui.QPushButton(self.connectionStates[False])
        self.connectButton.setStyleSheet("QPushButton {  font-size: 12px;  }")
        self.connectButton.setCheckable(True)
        self.connectButton.setMaximumHeight(20)
        self.connectButton.clicked.connect(self._eventConnect)
        self.connectButton.setToolTip(QtGui.QApplication.translate("Form", "Connect to the Game\n"
                                                                           "Once connected you can select cameras in motionbuilder\n"
                                                                           "to update in the game and vice versa unless your Joe then\n"
                                                                           "everything will be broken anyway", None, QtGui.QApplication.UnicodeUTF8))
        layout.addWidget(self.connectButton, 0, 0)

        groupBox = QtGui.QGroupBox("Cutscene Only")
        vbox = QtGui.QVBoxLayout()
        # Play Cutscene
        self.playCutsceneButton = QtGui.QPushButton('Sync to Animscene')
        self.playCutsceneButton.setToolTip(QtGui.QApplication.translate("Form", "This will play the cutscene/Animscene and sync the camera\n"
                                                                                "and playabck with moitonbuilder timeline", None, QtGui.QApplication.UnicodeUTF8))
        self.playCutsceneButton.setMinimumHeight(40)
        self.playCutsceneButton.clicked.connect(self._eventPlayCutscene)

        # Sync Cutscene RPFs
        self.previewCutsceneButton = QtGui.QPushButton('Sync Cutscene Preview RPF')
        self.previewCutsceneButton.setToolTip(QtGui.QApplication.translate("Form", "This copy the available preview RPFs of the cutscene and copy them to the game\n"
                                                                                ,None, QtGui.QApplication.UnicodeUTF8))
        self.previewCutsceneButton.clicked.connect(self._eventPreviewCutscene)
        self.previewCutsceneButton.setMinimumHeight(20)

        self.setupWorldForCutsceneButton = QtGui.QPushButton('Set Up World For Cutscene')
        self.setupWorldForCutsceneButton.setToolTip(QtGui.QApplication.translate("Form", "Sets up the world for cutscene\n"
                                                                         "", None, QtGui.QApplication.UnicodeUTF8))
        self.setupWorldForCutsceneButton.setMinimumHeight(20)

        #self.closeCutsceneButton.setEnabled(False)
        self.setupWorldForCutsceneButton.clicked.connect(self._eventSetupWorldForCutscene)

        # TRack Camera Switcher
        self.trackSwitcher = QtGui.QCheckBox("Track Camera Switcher")
        self.trackSwitcher.setToolTip(QtGui.QApplication.translate("Form", "This will use the current active camera switcher to detmeine\n"
                                                                           "which cameras to show in game", None, QtGui.QApplication.UnicodeUTF8))
        self.trackSwitcher.setChecked(True)
        # print dir(self.trackSwitcher)
        self.trackSwitcher.stateChanged.connect(lambda *args:self._eventUseCameraSwitcher())

        # ENable FUll Range Tracking
        self.fullRange = QtGui.QCheckBox("Full Range Support")
        self.fullRange.setToolTip(QtGui.QApplication.translate("Form", "This will use the Full Cutscene Range\n"
                                                                           "to playback the scene", None, QtGui.QApplication.UnicodeUTF8))
        self.fullRange.setChecked(False)
        # print dir(self.trackSwitcher)
        self.fullRange.stateChanged.connect(lambda *args:self._eventFullRange())

        # Load from RPF
        self.rpfLoad = QtGui.QCheckBox("Load RPF instead of from Disk")
        self.rpfLoad.setToolTip(QtGui.QApplication.translate("Form", "This will Load the RPF cutscenes e\n"
                                                                           "and not the disk version", None, QtGui.QApplication.UnicodeUTF8))
        self.rpfLoad.setChecked(True)
        self.rpfLoad.stateChanged.connect(lambda *args:self._eventLoadFromRPF())

        # Show Game Time LIne
        self.showInterface = QtGui.QCheckBox("Show Game Interface")
        self.showInterface.setToolTip(QtGui.QApplication.translate("Form", "This will Toggle showing and hiding the interface\n"
                                                                           "and will hide the tracks", None, QtGui.QApplication.UnicodeUTF8))
        self.showInterface.setChecked(False)
        self.showInterface.stateChanged.connect(lambda *args:self._eventShowInterface())


        # Disable MB Optimizations
        self.toggleDof = QtGui.QCheckBox("Disable DOF")
        self.toggleDof.setToolTip(QtGui.QApplication.translate("Form", "This will Enable/Disable the DOF IN RAG\n"
                                                                            "", None, QtGui.QApplication.UnicodeUTF8))
        self.toggleDof.setChecked(False)
        self.toggleDof.stateChanged.connect(lambda *args:self._eventDOF())

        # Disable Auto Blendout
        self.toggleBlendout = QtGui.QCheckBox("Disable AutoBlendOut")
        self.toggleBlendout.setToolTip(QtGui.QApplication.translate("Form", "This will Enable/Disable the automatic blendout from playing\n"
                                                                            "at the end of an cutscene", None, QtGui.QApplication.UnicodeUTF8))
        self.toggleBlendout.setEnabled(False)
        self.toggleBlendout.setChecked(True)
        self.toggleBlendout.stateChanged.connect(lambda *args:self._eventBlendOut())

        # Toggle DOF Overlay
        self.toggleDOFOverlay = QtGui.QCheckBox("Show DOF Overlay")
        self.toggleDOFOverlay.setEnabled(False)
        self.toggleDOFOverlay.setToolTip(QtGui.QApplication.translate("Form", "This will Enable/Disable the dof overlay\n"
                                                                            "under renderer/postfx/DofController", None, QtGui.QApplication.UnicodeUTF8))
        self.toggleDOFOverlay.setChecked(False)
        self.toggleDOFOverlay.stateChanged.connect(lambda *args:self._eventDOFOverlay())

        # DOF Summary
        self.toggleDOFSummary = QtGui.QCheckBox("Show DOF Summary")
        self.toggleDOFSummary.setEnabled(False)
        self.toggleDOFSummary.setToolTip(QtGui.QApplication.translate("Form", "This will Enable/Disable the dof summary\n"
                                                                            "under Marketing Tools/Depth Of Field [NEW]/Draw DOF Summary", None, QtGui.QApplication.UnicodeUTF8))
        self.toggleDOFSummary.setChecked(False)
        self.toggleDOFSummary.stateChanged.connect(lambda *args:self._eventDOFSummary())

        # Toggle Black Bars Overlay
        self.toggleLetterBoxOverlay = QtGui.QCheckBox("Show LetterBox")
        self.toggleLetterBoxOverlay.setToolTip(QtGui.QApplication.translate("Form", "This will Enable/Disable the black bars overlay\n"
                                                                            "under Marketing Tools/Cinematic Controls/Display Letter Box", None, QtGui.QApplication.UnicodeUTF8))
        self.toggleLetterBoxOverlay.setChecked(False)
        self.toggleLetterBoxOverlay.setEnabled(False)
        self.toggleLetterBoxOverlay.stateChanged.connect(lambda *args:self._eventBlackBarsOverlay())

        # Toggle Grid VIew Overlay
        self.toggleGridOverlay = QtGui.QCheckBox("Show Grid")
        self.toggleGridOverlay.setToolTip(QtGui.QApplication.translate("Form", "This will Enable/Disable the grid overlay\n"
                                                 "under Marketing Tools/Cinematic Controls/Display Letter Box", None,
                                         QtGui.QApplication.UnicodeUTF8))
        self.toggleGridOverlay.setChecked(False)
        self.toggleGridOverlay.stateChanged.connect(lambda *args: self._eventGridOverlay())

        # Adding the buttons and checkboxes to a widget so they don't overlap when resizing
        buttonHolder = QtGui.QWidget()
        buttonHolder.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        buttonHolder.setMinimumHeight(100)
        buttonLayout = QtGui.QVBoxLayout()
        buttonLayout.addWidget(self.playCutsceneButton)
        buttonLayout.addWidget(self.setupWorldForCutsceneButton)
        buttonLayout.addWidget(self.previewCutsceneButton)
        buttonLayout.setContentsMargins(0, 0, 0, 0)
        buttonLayout.setSpacing(2)
        buttonHolder.setLayout(buttonLayout)

        checkboxHolder = QtGui.QWidget()
        checkboxLayout = QtGui.QVBoxLayout()

        checkboxLayout.addWidget(self.trackSwitcher)
        checkboxLayout.addWidget(self.fullRange)
        checkboxLayout.addWidget(self.rpfLoad)
        checkboxLayout.addWidget(self.showInterface)
        checkboxLayout.addWidget(self.toggleDof)
        checkboxLayout.addWidget(self.toggleDOFOverlay)
        checkboxLayout.addWidget(self.toggleDOFSummary)
        checkboxLayout.addWidget(self.toggleLetterBoxOverlay)
        checkboxLayout.addWidget(self.toggleBlendout)
        checkboxLayout.addWidget(self.toggleGridOverlay)
        checkboxHolder.setLayout(checkboxLayout)
        checkboxLayout.setContentsMargins(0, 0, 0, 0)

        vbox.addWidget(buttonHolder, 0, QtCore.Qt.AlignTop)
        vbox.addWidget(checkboxHolder, 1)
        vbox.setSpacing(0)

        groupBox.setLayout(vbox)
        layout.addWidget(groupBox)

        # Create our offset Spinners
        self.spinOffsetX = QtGui.QDoubleSpinBox()
        self.spinOffsetX.setStyleSheet("QDoubleSpinBox {  font-size: 16px;  }")
        # self.spinOffsetX.setStyleSheet("QDoubleSpinBox { color: rgb(50, 50, 50); font-size: 11px; background-color: rgba(255, 188, 20, 50); }")
        self.spinOffsetX.setMaximum(10000)
        self.spinOffsetX.setMinimum(-10000)
        self.spinOffsetX.setFixedWidth(100)
        self.spinOffsetX.setDecimals(5)
        self.spinOffsetX.valueChanged.connect(self._eventOffsetChanged)

        self.spinOffsetY = QtGui.QDoubleSpinBox()
        self.spinOffsetY.setStyleSheet("QDoubleSpinBox {  font-size: 16px;  }")
        self.spinOffsetY.setMaximum(10000)
        self.spinOffsetY.setMinimum(-10000)
        self.spinOffsetY.setFixedWidth(100)
        self.spinOffsetY.setDecimals(5)
        self.spinOffsetY.valueChanged.connect(self._eventOffsetChanged)

        self.spinOffsetZ = QtGui.QDoubleSpinBox()
        self.spinOffsetZ.setStyleSheet("QDoubleSpinBox {  font-size: 16px;  }")
        self.spinOffsetZ.setMaximum(10000)
        self.spinOffsetZ.setMinimum(-10000)
        self.spinOffsetZ.setFixedWidth(100)
        self.spinOffsetZ.setDecimals(5)
        self.spinOffsetZ.valueChanged.connect(self._eventOffsetChanged)

        groupBox = QtGui.QGroupBox("Read Offsets")
        # self.groupBox.setStyleSheet("QLabel { color: rgb(50, 50, 50); font-size: 11px; background-color: rgba(188, 188, 188, 50); border: 1px solid rgba(188, 188, 188, 250); }")

        self.playAnimSceneeButton = QtGui.QPushButton('Use Loaded Anim Scene Offsets')

        self.playAnimSceneeButton.clicked.connect(self._eventPlayAnimScene)
        self.playAnimSceneeButton.setToolTip(QtGui.QApplication.translate("Form", "This will read the current Anim scene that is playing offset", None, QtGui.QApplication.UnicodeUTF8))

        self.synsGameButton = QtGui.QPushButton('Use Player Game Camera Offsets')

        self.synsGameButton.clicked.connect(self._eventSyncGameOffsets)
        self.synsGameButton.setToolTip(QtGui.QApplication.translate("Form", "[Be in Free Camera Mode] This will read the current players Camera positions offsets", None, QtGui.QApplication.UnicodeUTF8))

        # TODO: BUG ned to read Z offset as well if there is one
        self.useCutsceneOffsetsButton = QtGui.QPushButton('Use FBX Cutscene Offsets')

        self.useCutsceneOffsetsButton.clicked.connect(self._eventCutsceneGameOffsets)
        self.useCutsceneOffsetsButton.setToolTip(QtGui.QApplication.translate("Form", "This will read the current Loaded FBX Scene offsets", None, QtGui.QApplication.UnicodeUTF8))

        hbox = QtGui.QVBoxLayout()
        hbox.addWidget(self.playAnimSceneeButton)
        hbox.addWidget(self.synsGameButton)
        hbox.addWidget(self.useCutsceneOffsetsButton)

        groupBox.setLayout(hbox)
        layout.addWidget(groupBox)

        groupBox = QtGui.QGroupBox("Global")
        # Create a groupbox to hold our spinners
        horizontalGroupBox = QtGui.QGroupBox('Scene Location Offset (X, Y, Z)')
        horizontalGroupBox.setAlignment(QtCore.Qt.AlignCenter)
        # Add a horizontal layout to contain our spinners
        QHBoxLayout = QtGui.QHBoxLayout()
        QHBoxLayout.addWidget(self.spinOffsetX)
        QHBoxLayout.addWidget(self.spinOffsetY)
        QHBoxLayout.addWidget(self.spinOffsetZ)

        # add the horizontal layout to our groupbox

        horizontalGroupBox.setLayout(QHBoxLayout)
        # horizontalGroupBox.setLayout(QHBoxLayout2)

        # Add to our mian layour
        layout.addWidget(horizontalGroupBox)

        horizontalGroupBox = QtGui.QGroupBox('Scene Rotation Offset (X, Y, Z)')
        horizontalGroupBox.setAlignment(QtCore.Qt.AlignCenter)
        QHBoxLayout2 = QtGui.QHBoxLayout()

        self.spinOffsetRX = QtGui.QDoubleSpinBox()
        self.spinOffsetRX.setStyleSheet("QDoubleSpinBox {  font-size: 16px;  }")
        self.spinOffsetRX.setFixedWidth(100)
        self.spinOffsetRX.setMaximum(10000)
        self.spinOffsetRX.setMinimum(-10000)
        self.spinOffsetRX.setDecimals(5)
        self.spinOffsetRX.valueChanged.connect(self._eventOffsetChanged)

        self.spinOffsetRY = QtGui.QDoubleSpinBox()
        self.spinOffsetRY.setStyleSheet("QDoubleSpinBox {  font-size: 16px;  }")
        self.spinOffsetRY.setFixedWidth(100)
        self.spinOffsetRY.setMaximum(10000)
        self.spinOffsetRY.setMinimum(-10000)
        self.spinOffsetRY.setDecimals(5)
        self.spinOffsetRY.valueChanged.connect(self._eventOffsetChanged)

        self.spinOffsetRZ = QtGui.QDoubleSpinBox()
        self.spinOffsetRZ.setStyleSheet("QDoubleSpinBox {  font-size: 16px;  }")
        self.spinOffsetRZ.setFixedWidth(100)
        self.spinOffsetRZ.setMaximum(10000)
        self.spinOffsetRZ.setMinimum(-10000)
        self.spinOffsetRZ.setDecimals(5)
        self.spinOffsetRZ.valueChanged.connect(self._eventOffsetChanged)

        QHBoxLayout2.addWidget(self.spinOffsetRX)
        QHBoxLayout2.addWidget(self.spinOffsetRY)
        QHBoxLayout2.addWidget(self.spinOffsetRZ)
        horizontalGroupBox.setLayout(QHBoxLayout2)
        layout.addWidget(horizontalGroupBox)

        # Label
        label = QtGui.QLabel("Camera Mode In Control")
        label.setAlignment(QtCore.Qt.AlignCenter)
        layout.addWidget(label)

        # Toggle Mode button
        self.modeButton = QtGui.QPushButton('Game')
        self.modeButton.setCheckable(True)
        self.modeButton.setChecked(True)
        self.modeButton.clicked.connect(self._eventModeChanged)
        self.modeButton.setIcon(self.iconController)
        self.modeButton.setIconSize(QtCore.QSize(84, 55))
        self.modeButton.setMinimumSize((QtCore.QSize(84, 55)))
        self.modeButton.setToolTip(QtGui.QApplication.translate("Form", "This will swap between motionbuilder contolling the game camera\n"
                                                                        "or the game's free camera controlling Moitonbuilder's Camera", None, QtGui.QApplication.UnicodeUTF8))
        layout.addWidget(self.modeButton, 0, 0)
        #self.setMinimumSize(100,700)
        # Layout

        self.setLayout(layout)

        # Disable all our buttons
        self.modeButton.setEnabled(False)
        self.spinOffsetX.setEnabled(False)
        self.spinOffsetY.setEnabled(False)
        self.spinOffsetZ.setEnabled(False)
        self.spinOffsetRX.setEnabled(False)
        self.spinOffsetRY.setEnabled(False)
        self.spinOffsetRZ.setEnabled(False)
        self.playCutsceneButton.setEnabled(False)
        self.trackSwitcher.setEnabled(False)
        self.playAnimSceneeButton.setEnabled(False)
        self.synsGameButton.setEnabled(False)
        self.setupWorldForCutsceneButton.setEnabled(False)
        self.showInterface.setEnabled(False)

        layout.setContentsMargins(2, 2, 2, 2)
        print "finih"

    def closeEvent(self, event):
        """
        Overrides built-in event

        Arguments:
            event (QtCore.QEvent): event being invoked by the UI
        """
        remoteSignal.disconnectSignal()
        con.Disconnect()

    def setUIOffsets(self):
        '''

        :return:
        '''
        # read back our values into the offsets, we need to disconnec telse the valuse don;t update :(
        self.spinOffsetX.valueChanged.disconnect()
        self.spinOffsetY.valueChanged.disconnect()
        self.spinOffsetZ.valueChanged.disconnect()
        self.spinOffsetRZ.valueChanged.disconnect()

        self.spinOffsetX.setValue(con.OFFSET_X)
        self.spinOffsetY.setValue(con.OFFSET_Y)
        self.spinOffsetZ.setValue(con.OFFSET_Z)
        self.spinOffsetRZ.setValue(con.OFFSET_RZ)

        self.spinOffsetX.valueChanged.connect(self._eventOffsetChanged)
        self.spinOffsetY.valueChanged.connect(self._eventOffsetChanged)
        self.spinOffsetZ.valueChanged.connect(self._eventOffsetChanged)
        self.spinOffsetRZ.valueChanged.connect(self._eventOffsetChanged)


    def _enableButtons(self, buttonEnabled):
        """
        """
        self.modeButton.setEnabled(buttonEnabled)
        self.spinOffsetX.setEnabled(buttonEnabled)
        self.spinOffsetY.setEnabled(buttonEnabled)
        self.spinOffsetZ.setEnabled(buttonEnabled)
        self.spinOffsetRX.setEnabled(buttonEnabled)
        self.spinOffsetRY.setEnabled(buttonEnabled)
        self.spinOffsetRZ.setEnabled(buttonEnabled)
        self.playCutsceneButton.setEnabled(buttonEnabled)
        self.trackSwitcher.setEnabled(buttonEnabled)
        self.playAnimSceneeButton.setEnabled(buttonEnabled)
        self.synsGameButton.setEnabled(buttonEnabled)
        self.showInterface.setEnabled(False) # NOt supported for GTA
        self.setupWorldForCutsceneButton.setEnabled(buttonEnabled)



    # EVENTS
    @DisplayError()
    def _eventConnect(self):
        '''
        CONNECT Button Event

        :return:
        '''
        isConnect = self.connectButton.isChecked()

        self._enableButtons(isConnect)
        try:
            self.connectButton.setText(self.connectionStates[isConnect])

            if isConnect:
                con.Connect()
                self._eventEnableUserSettings()
            else:
                con.Disconnect()
        except RemoteModel.MissingGameError:
            self._setDisconnectButton()
            raise

    def _setDisconnectButton(self, state=False, msg=None):
        """
            Set Connection Button State

            :return: None
        """

        self.connectButton.setChecked(state)
        self._enableButtons(state)

        if not state and msg:
            QtGui.QMessageBox.critical(None, "Connection Error", msg)

        self.connectButton.setText(self.connectionStates[state])

    @DisplayError()
    def _eventEnableUserSettings(self):
        """ Triggers all the events that set the user settings on the connection """
        self._eventUseCameraSwitcher()
        self._eventFullRange()
        self._eventLoadFromRPF()
        self._eventShowInterface()
        self._eventDOF()
        self._eventDOFOverlay()
        self._eventBlackBarsOverlay()
        self._eventBlendOut()

    @DisplayError()
    def _eventUseCameraSwitcher(self):
        """
        Sets if the current camera on the camera switcher should be used for sending camera position & rotation
        to the game
        """

        con.enableSwitcher(self.trackSwitcher.isChecked())

    @DisplayError()
    def _eventFullRange(self):
        """ Enable Full Range Syncing """
        # QPrint(self.fullRange.isChecked()) #,
        con.enableFullRange(self.fullRange.isChecked())

    @DisplayError()
    def _setEventFullRange(self, state):
        """
        Set Full Range State

        Arguments:
            state (bool): should the game use the full range
        """
        self.fullRange.setChecked(state)
        con.enableFullRange(self.fullRange.isChecked())


    @DisplayError()
    def _eventLoadFromRPF(self):
        """ Enable Full Range Syncing """
        con.enableRPFLoad(self.rpfLoad.isChecked())

    @DisplayError()
    def _eventShowInterface(self):
        """ Show/Hide ingame animscene Interface """
        con.showInterface(self.showInterface.isChecked())

    @DisplayError()
    def _eventDOF(self):
        """ Disable/ Enable Depth Of Field in the game"""
        con.disableDOF(self.toggleDof.isChecked())

    @DisplayError()
    def _eventDOFOverlay(self):
        """ Enables the DOF Overlay in the game """
        con.toggleDOFOverlay(self.toggleDOFOverlay.isChecked())

    @DisplayError()
    def _eventDOFSummary(self):
        """ Adds the DOF Summary Overlay to the game """
        con.toggleDOFSummary(self.toggleDOFSummary.isChecked())

    @DisplayError()
    def _eventBlackBarsOverlay(self):
        """ Adds the black bars to the game """
        con.toggleLetterbox(self.toggleLetterBoxOverlay.isChecked())

    @DisplayError()
    def _eventGridOverlay(self):
        """ Adds a grid to the game for framing purposes"""
        con.toggleGridOverlay(self.toggleGridOverlay.isChecked())


    @DisplayError()
    def _eventBlendOut(self):
        """ Disable/Enable automatic blendout """
        con.toggleAutomaticBlendOut(self.toggleBlendout.isChecked())

    @DisplayError()
    def _eventSetupWorldForCutscene(self):
        """ Enable the World for cutscene setup """
        con.SetupWorldForCutscene()

    @DisplayError()
    def _eventPlayAnimScene(self):
        '''

        :return:
        '''
        if con.startAnimScene():
            self.setUIOffsets()
            self.modeButton.setChecked(True)
            self.modeButton.setText("Mobu")
            con.GAME_MODE = False
        else:
            QtGui.QMessageBox.warning(self, "Error!!!", "No Editable Anim Scene has been Loaded!")

    @DisplayError()
    def _eventPlayCutscene(self):
        '''

        :return:
        '''
        if con.startStopCutscene():
            self.setUIOffsets()

            # Set to Mobu mode
            self.modeButton.setChecked(True)
            self.modeButton.setText("Mobu")
            con.GAME_MODE = False
            con.setSwitcherCamera()

    @DisplayError()
    def _eventPreviewCutscene(self):
        message = "This is an empty fbx\nNothing was synced."
        if Globals.Application.FBXFileName:
            fbxPath = Globals.Application.FBXFileName
            capPaths = CapturePaths.FastPath(fbxPath)
            capDataRoot = capPaths.capDataRoot
            if not os.path.exists(capDataRoot):
                drivepath, _ = os.path.splitdrive(capDataRoot)
                message = ("Could not access {directory}\n" 
                           "Re-enable the {drive} drive on your computer and try again.\n"
                           "If you continue to lack access to the directory after enabling the {drive} drive, "
                           "contact IT.").format(directory=capDataRoot, drive=drivepath)
            else:
                path = os.path.join(capDataRoot, "PreviewFiles")
                timestamp = FileTools.GetTimeStamp(path)
                if timestamp:
                    if QtGui.QMessageBox.information(self, "Sync Preview Files",
                                                            "Preview Files were last updated on {}\n"
                                                            "Do you wish to continue with the sync ?".format(
                                                            time.asctime(time.localtime(timestamp))),
                                                            QtGui.QMessageBox.Yes|QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
                        success, message = ExportDataTools.SyncCaptureData(fbxPath)
                        if success:
                            isGameRunning = con.mConnection.IsConnected
                            if not isGameRunning:
                                try:
                                    # Check if the game is running
                                    con.mConnection.EnsureConnection()
                                    con.mConnection.Disconnect()
                                    isGameRunning = True
                                except:
                                    isGameRunning = False

                            if isGameRunning:
                                message = "{}\nPlease restart your build for the changes to take effect.".format(message)
                    else:
                        message = "Sync cancelled by user"
                else:
                    message = "Could not find preview files to sync.\nSend this scene for a capture and sync the data after the capture is completed."
        QtGui.QMessageBox.information(self, "Remote Connection", message)

    @DisplayError()
    def _eventCloseCutscene(self):
        """
        CLOSE ANIM SCENE EDITOR
        """

        con.CloseAnimScene()


    def _eventSyncGameOffsets(self):
        '''

        :return:
        '''
        # REad our Camera Offsets from game
        con.SyncGameOffsets()
        # Set our UI to the OFFSETS
        self.setUIOffsets()

    def _eventCutsceneGameOffsets(self):
        """
        """
        con.SyncGameCutsceneOffsets()
        self.setUIOffsets()



    def _eventModeChanged(self):
        '''
        Chaage which direction we drive our, either MObu-?Game or vice versa
        :return:
        '''
        if self.modeButton.isChecked():
            icon = self.iconMouse
            self.modeButton.setText("Mobu")
        else:
            icon = self.iconController
            self.modeButton.setText("Game")


        con.GAME_MODE = not self.modeButton.isChecked()
        #print "Game_Mode = ", con.GAME_MODE
        #con.EnableFreeCamera(not self.modeButton.isChecked())


        self.modeButton.setIcon(icon)

    def _eventOffsetChanged(self):
        '''

        :return:
        '''
        con.OFFSET_X = self.spinOffsetX.value()
        con.OFFSET_Y = self.spinOffsetY.value()
        con.OFFSET_Z = self.spinOffsetZ.value()
        con.OFFSET_RX = self.spinOffsetRX.value()
        con.OFFSET_RY = self.spinOffsetRY.value()
        con.OFFSET_RZ = self.spinOffsetRZ.value()



'''
    Test code  for right click mouse increase
'''
class Wrap_Spinner(QtGui.QDoubleSpinBox):
    def __init__(self, minVal=0, maxVal=100000, default=0):
        super(Wrap_Spinner, self).__init__()
        self.drag_origin = None
        self.setAccelerated(True)
        self.setRange(minVal, maxVal)
        self.setValue(default)

    def get_is_dragging(self):
        # are we the widget that is also the active mouseGrabber?
        return self.mouseGrabber() == self

    ### Dragging Handling Methods ################################################
    def do_drag_start(self):
        # Record position
        # Grab mouse
        self.drag_origin = QtGui.QCursor().pos()
        self.grabMouse()

    def do_drag_update(self):
        # Transpose the motion into values as a delta off of the recorded click position
        curPos = QtGui.QCursor().pos()
        offsetVal = self.drag_origin.y() - curPos.y()
        self.setValue(offsetVal)

    def do_drag_end(self):
        self.releaseMouse()
        # Restore position
        # Reset drag origin value
        self.drag_origin = None

    ### Mouse Override Methods ################################################
    def mousePressEvent(self, event):
        if QtCore.Qt.LeftButton:

            self.do_drag_start()
        elif self.get_is_dragging() and QtCore.Qt.RightButton:
            # Cancel the drag
            self.do_drag_end()
        else:
            super(Wrap_Spinner, self).mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        if self.get_is_dragging():
            self.do_drag_update()
        else:
            super(Wrap_Spinner, self).mouseReleaseEvent(event)

    def mouseReleaseEvent(self, event):
        if self.get_is_dragging() and QtCore.Qt.LeftButton:

            self.do_drag_end()
        else:
            super(Wrap_Spinner, self).mouseReleaseEvent(event)
