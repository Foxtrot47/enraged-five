'''
Create or Edit SyncedScenes (GTA's AnimScene equiv).

Simon Papp RSGLDS Summer 2019
'''

import pyfbsdk as mobu
import os
import re
import xml.etree.ElementTree as ET
from xml.dom import minidom
import xlrd

from PySide import QtGui, QtCore
import RS.Tools.UI
from RS import Config, Globals, Perforce

from threading import Thread

import webbrowser
import RS.Config
import RS.Utils.UserPreferences
from RS.Tools.UI import Application

uiDirectory = os.path.join(RS.Config.Script.Path.Root, "RS\\Tools\\UI\\CreateSyncedScene\\QT\\")

RS.Tools.UI.CompileUi( uiDirectory + "CreateSyncedScene.ui",
                    uiDirectory + "CreateSyncedScene_compiled.py" )

RS.Tools.UI.CompileUi( uiDirectory + "WIDGET\\Dialogue_InfoScreen.ui",
                    uiDirectory + "WIDGET\\Dialogue_InfoScreen_compiled.py" )

import RS.Tools.UI.CreateSyncedScene.QT.CreateSyncedScene_compiled as CreateSyncedSceneCompiled
import RS.Tools.UI.CreateSyncedScene.QT.WIDGET.Dialogue_InfoScreen_compiled as Dialogue_InfoScreenCompiled

'''
# option to align text in the centre of the dropdownlist
class AlignDelegate(QtGui.QStyledItemDelegate):
    def initStyleOption(self, option, index):
        super(AlignDelegate, self).initStyleOption(option, index)
        option.displayAlignment = QtCore.Qt.AlignCenter
'''


# store position/size of infoscreen
class InfoScreen(object):
    location = [1300, 350]
    width = 518
    height = 268
    textEdit_Text = ""
    active = False


# Info Dialogue
class Dialogue_InfoScreen(RS.Tools.UI.QtMainWindowBase, Dialogue_InfoScreenCompiled.Ui_Dialog_InfoScreen):
    # create a signal slot so we can emit a closed signal back to the MainWindow
    #sendClosedloadinPosesSignal = QtCore.Signal()
    # main code for the load poses dialog
    def __init__(self):
        RS.Tools.UI.QtMainWindowBase.__init__(self, None, title = 'Info Screen')

        #self.setWindowFlags( QtCore.Qt.Window | QtCore.Qt.FramelessWindowHint | QtCore.Qt.Tool )
        self.setWindowFlags( QtCore.Qt.Window | QtCore.Qt.Tool )
        #self.setWindowFlags( QtCore.Qt.Window | QtCore.Qt.WindowStaysOnTopHint )
        
        self.setupUi(self)
        
        self.resize(InfoScreen.width, InfoScreen.height)
        self.move(InfoScreen.location[0], InfoScreen.location[1])
          
    def resizeEvent(self, event):  
        InfoScreen.width = self.frameGeometry().width()-16 # seems to need a fudge factor?
        InfoScreen.height = self.frameGeometry().height()-40 # seems to need a fudge factor?         
   
    def moveEvent(self, e):
        #print "Dialog Moved: ", self.pos()
        InfoScreen.location[0] = self.x()
        InfoScreen.location[1] = self.y()
        
    def closeEvent(self, e):
        #print "Dialog Closed: ", self.pos()
        InfoScreen.active = False

                
# a class to hold all the transforms for each syncedScene
class SyncedSceneTransforms(object): 
    def __init__(self):
        self.data = []
        
# we only need one instance of the SyncedSceneTransforms class, as we are appending new lists that get added as we go
xmlLists = SyncedSceneTransforms()


# class object to hold the character list as we need to pass this to the thread and back
class CharacterListHolder(object):
    def __init__(self):
        self.data = []
        
CharacterList = CharacterListHolder()


# option to chose the type of xml (enum type object)
class XmlType(object):
    REXMB = 1
    SYNCED_SCENE = 2
    META = 3


# new thread to handle grabbing all of the characters from all of the packs (first run will cache all the availiable characters)
# every other time is just a case of checking for updates
class Thread(QtCore.QThread):
    # hook up the text signal
    sendTextUpdate = QtCore.Signal(str)

    
    def __init__(self, extraContentPeds):
        super(Thread, self).__init__()
        self.extraContentPeds = extraContentPeds
        self.toolDirectory = os.path.join(RS.Config.Script.Path.Root, 'RS', 'Tools', 'CreateSyncedScene')


    def fallBackList(self, pedMetaFileName):
        self.sendTextUpdate.emit(
                '\n ... Failed to grab characters from: \n' + pedMetaFileName +
                '\n\nYou should re-open motionbuilder and check your perforce connection.' +
                '\n... Falling back to basic list.' +
                '   Note: Some of the characters in your scene might not be available in the editor.')

        fallbackCharacterList = os.path.join(self.toolDirectory, "CharacterList.dat")

        if os.path.isfile(fallbackCharacterList):
            with open(fallbackCharacterList, "r") as f:
                for line in f:
                    line = line.rstrip('\n')
                    if line not in CharacterList.data:
                        CharacterList.data.append(line)
                f.close
        

    def run(self):
        self.sendTextUpdate.emit('\n\n ...Character List update started.')

        cachedMetaPaths = os.path.join(self.toolDirectory, 'CachedMetaPaths.dat')
        cachedInvalidMetaPaths = os.path.join(self.toolDirectory, "CachedInvalidMetaPaths.dat")
        
        oldExtraContentPeds = []
        invalidMetaPaths = []
        
        if os.path.isfile(cachedMetaPaths):
            with open(cachedMetaPaths, "r") as f:
                for line in f:
                    line = line.rstrip('\n')
                    oldExtraContentPeds.append(line)
                f.close
                
        if os.path.isfile(cachedInvalidMetaPaths):
            with open(cachedInvalidMetaPaths, "r") as f:
                for line in f:
                    line = line.rstrip('\n')
                    invalidMetaPaths.append(line)
                f.close
        
        # find all the possible depot locations where extra content peds might exist
        p4depot = '//gta5_dlc/mpPacks/'
        
        # grabs all the mpPack parent folder names, so we have a list of all the packs, need this for first run, and to find new packs as they are added
        depotPaths = Perforce.Run('dirs', ['-i', '{}*'.format(p4depot)])
        
        p4Paths = [os.path.basename(str(record[key])) for record in depotPaths.Records for key in record.get_Fields().Keys]
        
        if not p4Paths:
            self.sendTextUpdate.emit(
                '\nError finding any mpPacks, are we on the right Workspace?\nPlease check your Perforce Workspace mappings, and Perforce connection.')

            self.fallBackList('All mpPacks')

            return
        
        p4Paths.pop(0)# first entry is not a valid mpPack

        # search through the locations for peds.meta data
        for p4Path in p4Paths:
            oldExtraCharacterPath = ('{0}{1}{2}'.format(p4depot, p4Path, "/build/dev_ng/common/data/peds.meta")).lower()
            foundIn = [x for x in oldExtraContentPeds, self.extraContentPeds, invalidMetaPaths if oldExtraCharacterPath in x]# search the various lists to see if the potential mpPack has already been listed somewhere
            if not foundIn:
                PerforceFileData = Perforce.Files([oldExtraCharacterPath]) # test to see if this is a valid meta
                if len([record[key] for record in PerforceFileData.Records for key in record.get_Fields().Keys]) > 0:# if the peds.meta path is invalid, we add it to the invalid list
                    for item in PerforceFileData:      
                        depotFile = item['depotFile']
                        if depotFile:
                            currentDepotFile = [] # if the depotFile exists, we'll add it to a temporary list to extract the data
                            currentDepotFile.append(depotFile.encode("utf-8"))
                            if currentDepotFile[0]:
                                oldExtraContentPeds.append(currentDepotFile[0].lower())
                else:
                    invalidMetaPaths.append(oldExtraCharacterPath.lower())
        
        # save out (cache) the oldExtraContentPeds list
        with open(cachedMetaPaths, "w") as f:
            for oldExtraContentPed in oldExtraContentPeds:
                f.write(str(oldExtraContentPed) + '\n')
            f.close

        # save out (cache) the cachedInvalidMetaPaths list
        with open(cachedInvalidMetaPaths, "w") as f:
            for invalidMetaPath in invalidMetaPaths:
                f.write(str(invalidMetaPath) + '\n')
            f.close
        
        syncedPedsMeta = ""
        
        for oldExtraContentPed in oldExtraContentPeds:
            
            PerforceFileData = Perforce.Run("fstat", [oldExtraContentPed])# get perforce stats on our pose file
            
            for item in PerforceFileData:      

                haveRev = item['haveRev']
                headRev = item['headRev']
                if not haveRev:# if there is no local file, we need to grab it
                    Perforce.Sync([oldExtraContentPed])
                    syncedPedsMeta = ('{0} {1} {2} {3}'.format(syncedPedsMeta, '\n', oldExtraContentPed, '- grabbed'))

                elif haveRev != headRev:# old version, lets grab that
                    Perforce.Sync([oldExtraContentPed])
                    syncedPedsMeta = ('{0} {1} {2} {3}'.format(syncedPedsMeta, '\n', oldExtraContentPed, ' - old rev, got latest'))

            pedMetaFileName = oldExtraContentPed
            pedMetaFileName = pedMetaFileName.replace("//", "X:\\")
            pedMetaFileName = pedMetaFileName.replace("/", "\\")

            # add new characters to the characterList
            if os.path.isfile(pedMetaFileName):
                tree = ET.parse(pedMetaFileName)
                root = tree.getroot()

                item = root.findall(".//InitDatas/Item/Name")
                for child in item:
                    if child.tag == "Name":
                        pedName = child.text
                        if not pedName in CharacterList.data:
                            CharacterList.data.append(pedName)
            else:
                self.fallBackList(pedMetaFileName)

                return

        CharacterList.data.sort()

        self.sendTextUpdate.emit('\n ... Synced peds.meta(s): ' + syncedPedsMeta + '\n\n ...Character List update complete.')

        
# main code
class MainWindow(RS.Tools.UI.QtMainWindowBase, CreateSyncedSceneCompiled.Ui_MainWindow):
    def __init__(self):
        # main window settings
        RS.Tools.UI.QtMainWindowBase.__init__(self, None, title = 'Create Synced Scene')

        self.setupUi(self)
        
        self.splitter.setSizes([500, 500, 700]) # set the split sizes

        # change selection behaviour of characterAndProp widget to allow multiple selects
        self.listWidget_charactersAndProps.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

        self.pushButton_newFolderName.clicked.connect(self.newFolderNameCallback)
        self.pushButton_loadSyncedScene.clicked.connect(self.loadSyncedSceneCallback)
        self.pushButton_saveSyncedScene.clicked.connect(self.saveSyncedSceneCallback)
        self.pushButton_newSyncedScene.clicked.connect(self.newSyncedSceneCallback)
        
        self.pushButton_loadSyncedSceneFolder.clicked.connect(self.loadSyncedSceneFolderCallback)
        self.pushButton_saveSyncedSceneFolder.clicked.connect(self.saveSyncedSceneFolderCallback)
        
        self.pushButton_rexMBFilename.clicked.connect(self.rexMBFilenameCallback)
        
        self.pushButton_populateOutputList.clicked.connect(self.populateOutputListCallback)
        
        # connects the QTabWidget.currentChanged() signal with its respective slot
        self.tabWidget_outputList.currentChanged.connect(self.updateTransform)
        
        self.pushButton_populateOutputList.clicked.connect(self.listWidget_charactersAndProps.clearSelection)
        self.pushButton_populateOutputList.clicked.connect(self.listWidget_animations.clearSelection)
        self.pushButton_deleteSelectedItem.clicked.connect(self.deleteSelectedItemCallback)
        self.pushButton_extractTransform.clicked.connect(self.extractTransformCallback)
                
        self.label_headerIcon.setPixmap( uiDirectory + "CreateSyncedScene_logo.png")
        self.label_headerText.setPixmap( uiDirectory + "CreateSyncedScene_text.png")
        
        icon = QtGui.QPixmap( uiDirectory + "CreateSyncedScene_help.png")
        self.pushButton_headerIcon_Help.setIcon(icon)
        self.pushButton_headerIcon_Help.clicked.connect( self.help )
        
        self.pushButton_close_tab.clicked.connect(self.removeTab)
        
        self.doubleSpinBox_headingValue.valueChanged.connect(self.updateTransformList)
        self.doubleSpinBox_pitchValue.valueChanged.connect(self.updateTransformList)
        self.doubleSpinBox_rollValue.valueChanged.connect(self.updateTransformList)
        self.doubleSpinBox_transXValue.valueChanged.connect(self.updateTransformList)
        self.doubleSpinBox_transYValue.valueChanged.connect(self.updateTransformList)
        self.doubleSpinBox_transZValue.valueChanged.connect(self.updateTransformList) 
        
        self.lineEdit_newFolderNameValue.setText("X:/gta5/assets_ng/anim/synced_scenes/".encode("utf-8"))

        self.populateCharacterList()

       
    def dialogueOpenInfoScreen( self, text ):
        greyColor = QtGui.QColor(128, 128, 128)
        greenColor = QtGui.QColor(0, 255, 0)
        
        if not InfoScreen.active:
            InfoScreen.textEdit_Text = "" 
        
        dialogue = Dialogue_InfoScreen()
        
        if not InfoScreen.active:
            dialogue.show() 
            InfoScreen.active = True
        
        dialogue.textEdit_infoScreen.setTextColor(greyColor)
        dialogue.textEdit_infoScreen.append(InfoScreen.textEdit_Text) 

        dialogue.textEdit_infoScreen.setTextColor(greenColor)
        dialogue.textEdit_infoScreen.append(text)
        
        dialogue.textEdit_infoScreen.moveCursor(QtGui.QTextCursor.End)
        QtGui.QApplication.processEvents()
        
        InfoScreen.textEdit_Text = dialogue.textEdit_infoScreen.toPlainText() + "\n____"


    def startThread(self):
        self.workerThread = Thread(self.extraContentPeds)# pass the list of extraContentPeds which we collected in the populateCharacterList method (deault peds)
        
        # connect a signal to the background thread to update the UI when were done
        self.workerThread.sendTextUpdate.connect(self.collectSignal)
        
        self.workerThread.start()


    # method to pass data to the infoscreen
    def collectSignal(self, text=""):
        self.dialogueOpenInfoScreen(text)
        

    def populateCharacterList(self):
        basePeds = os.path.join(Config.Project.Path.Root, 'assets_ng', 'export', 'data', 'peds.pso.meta')
        commandline = os.path.join(Config.Project.Path.Root, 'titleupdate', 'dev_ng', 'commandline.txt')
        
        self.extraContentPeds = []
        
        with open(commandline) as f:
            commandlineContents = f.readlines()
            
        for line in commandlineContents:
            if line[0][0] != "#" and "-extracontent" in line:
                split = re.findall(r"[\w '\']+", line)
                for mpPack in split:
                    if "mp" in mpPack and mpPack != 'mpPacks':
                        extraContent = os.path.join("x:\\gta5_dlc\\mpPacks", mpPack, "build\\dev_ng\\Common\\data\\peds.meta")
                        self.extraContentPeds.append(extraContent)
                    
        depotLocations = [basePeds]
        
        for extraContentPed in self.extraContentPeds:
            depotLocations.append(extraContentPed.lower())
         
        CharacterList.data = []
        
        for loc in depotLocations:
            if not self.perforceFileCheck( loc, False ):
                value = depotLocations.index(loc)
                del depotLocations[value]
 
        for loc in depotLocations:
            self.parseXML( XmlType.META, loc, "" )
            
        CharacterList.data.sort()

        locations = '\n'.join(depotLocations)
        
        self.dialogueOpenInfoScreen( "\nThese are the default characters that are being loaded for use in the Output List character dropdown:"
                                    + "\n\n" + locations
                                    + "\n\nNote that characters that are available in the Reference Editor may not be available in the "
                                    + "Sync Scene widget in RAG."
                                    + "\n\n...checking for additional characters from older mpPacks (we'll do this in the background). "
                                    + "\nPLEASE WAIT FOR THIS TO COMPLETE! before adding characters to your Sync Scenes.")

        self.startThread()


    def updateTransformList( self ):
        tabIndex = self.tabWidget_outputList.currentIndex()
        tabName = self.tabWidget_outputList.tabText(tabIndex)
        for item in xmlLists.data:
            if tabName == item[0]:

                item[1] = self.doubleSpinBox_pitchValue.text().encode("utf-8")
                item[2] = self.doubleSpinBox_rollValue.text().encode("utf-8")
                item[3] = self.doubleSpinBox_headingValue.text().encode("utf-8")
                
                item[4] = self.doubleSpinBox_transXValue.text().encode("utf-8")
                item[5] = self.doubleSpinBox_transYValue.text().encode("utf-8")
                item[6] = self.doubleSpinBox_transZValue.text().encode("utf-8")


    @QtCore.Slot(int)
    def updateTransform( self, index ):
        filename = self.tabWidget_outputList.tabText(index)

        for item in xmlLists.data:
            if filename == item[0]:
                self.lineEdit_syncedSceneFilenameValue.setText(filename)
                
                # block all QDoubleSpinBox signals as these are sending signals whenever they get updated
                doubleSpiners = self.findChildren(QtGui.QDoubleSpinBox)
                for doubleSpiner in doubleSpiners:
                    doubleSpiner.blockSignals(True)
                
                self.doubleSpinBox_pitchValue.setValue(float(item[1]))
                self.doubleSpinBox_rollValue.setValue(float(item[2]))
                self.doubleSpinBox_headingValue.setValue(float(item[3]))
                
                self.doubleSpinBox_transXValue.setValue(float(item[4]))
                self.doubleSpinBox_transYValue.setValue(float(item[5]))
                self.doubleSpinBox_transZValue.setValue(float(item[6]))
                
                # re-activate QDoubleSpinBox signals as these are also used when editing manually in the ui
                for doubleSpiner in doubleSpiners:
                    doubleSpiner.blockSignals(False)


    def makeDir( self, dirpath ):
        # test that the folder path isn't doing anything wierd
        if not dirpath.endswith("/"):
            dirpath = dirpath + "/"
            self.lineEdit_newFolderNameValue.setText(dirpath.encode("utf-8"))

        if not "X:/gta5/assets_ng/anim/synced_scenes/" in dirpath:            
            self.dialogueOpenInfoScreen( "\nLooks like there is an issue with your folder path?."
                                        + "\nPlease check your save folder location starts with 'X:/gta5/assets_ng/anim/synced_scenes/'."
                                        + "\nIf it does, make sure that there is a '/' after 'synced_scenes'.")
            return
            
        fileCheck = dirpath.split("synced_scenes/")[1]            
        fileCheck = fileCheck.replace("/","")

        if len(os.path.splitext(fileCheck)[0]) >= 65:
            self.dialogueOpenInfoScreen( "\nLooks like your Synced Scene filename and dict combined exceeds the limit of 64 characters.\nPlease make it shorter and try again." )
            return False
        
        if not os.path.isdir(dirpath):
            os.makedirs(dirpath)
            return True
        else:
            return True


    def diloguePopup( self, Directory, dirpath ):
        # a simplified shared method to create the direcotry or file open dialogue
        if not self.makeDir( dirpath ):# check to see if the synced scene output folder exists
            return
        
        popup = QtGui.QFileDialog()
        popup.setOption(QtGui.QFileDialog.ShowDirsOnly)
        popup.setDirectory(dirpath)
        if Directory:
            popup.setFileMode(QtGui.QFileDialog.Directory)
            selected_directory = QtGui.QFileDialog.getExistingDirectory(None, "Choose the Folder Path", "" )
            selected_directory = selected_directory.replace(os.sep, '/')
            return selected_directory
        else:
            popup.setViewMode(QtGui.QFileDialog.List)
            selected_directory = QtGui.QFileDialog.getOpenFileName(None, "Choose an Existing Synced Scene", "", "XML files (*.xml)")
            fileinfo = selected_directory[0]
            return fileinfo


    def newFolderNameCallback( self ):
        # popup for user to select new export folder
        dirPath = self.lineEdit_newFolderNameValue.text().encode("utf-8")

        if os.path.isdir(dirPath):
            selected_directory = self.diloguePopup(True, dirPath)

            if str(selected_directory) != "":
                self.lineEdit_newFolderNameValue.setText(QtGui.QApplication.translate("MainWindow", str(selected_directory), None, QtGui.QApplication.UnicodeUTF8))
        else:
            self.lineEdit_newFolderNameValue.setText('X:/gta5/assets_ng/anim/synced_scenes/')
            self.lineEdit_newFolderNameValue.clearFocus()


    def loadSyncedSceneCallback( self ):
        # popup for user to select an existing synced scene
        dirpath = self.lineEdit_newFolderNameValue.text().encode("utf-8")

        if not dirpath.endswith("/"):
            dirpath = dirpath + "/"
            self.lineEdit_newFolderNameValue.setText(dirpath.encode("utf-8"))

        if self.lineEdit_syncedSceneFilenameValue.text() == "":
            fileinfo = self.diloguePopup( False, dirpath )
            if not fileinfo:
                self.lineEdit_syncedSceneFilenameValue.setText(QtGui.QApplication.translate("MainWindow", "", None, QtGui.QApplication.UnicodeUTF8))
                return
            else:
                dirpath, filename = os.path.split(fileinfo)
                self.lineEdit_syncedSceneFilenameValue.setText(filename.encode("utf-8"))
                self.lineEdit_newFolderNameValue.setText(dirpath.encode("utf-8"))
                self.parseXML( XmlType.SYNCED_SCENE, dirpath, filename )       
        else:
            datafile = os.path.join( dirpath, self.lineEdit_syncedSceneFilenameValue.text().encode("utf-8") )
            if not os.path.isfile(datafile): #does this file exist? hmmmm
                fileinfo = self.diloguePopup( False, dirpath )
                if not fileinfo:
                    self.lineEdit_syncedSceneFilenameValue.setText(QtGui.QApplication.translate("MainWindow", "", None, QtGui.QApplication.UnicodeUTF8))
                    return
                dirpath, filename = os.path.split(fileinfo)
                self.lineEdit_syncedSceneFilenameValue.setText(filename.encode("utf-8"))
                self.lineEdit_newFolderNameValue.setText(dirpath.encode("utf-8"))
            self.parseXML( XmlType.SYNCED_SCENE, dirpath, self.lineEdit_syncedSceneFilenameValue.text().encode("utf-8") )


    def saveSyncedSceneCallback( self ):
        tabIndex = self.tabWidget_outputList.currentIndex() 
        if self.tabWidget_outputList.tabText(tabIndex) == "":
            self.dialogueOpenInfoScreen( "\nThere is no data to save?\nPlease check and try again." )
            return       
        
        dirpath = self.lineEdit_newFolderNameValue.text().encode("utf-8") 
        filename = self.lineEdit_syncedSceneFilenameValue.text().encode("utf-8")
        
        if filename == "":
            self.dialogueOpenInfoScreen( "\nThere is no filename?\nPlease check and try again." )
            return

        if os.path.splitext(filename)[1] != ".xml":
            filename = filename + ".xml"
            self.lineEdit_syncedSceneFilenameValue.setText(filename)
        
        self.makeDir( dirpath )# check to see if the synced scene output folder exists
  
        self.saveSyncedScenes( dirpath, filename )

        
    def saveSyncedScenes( self, dirpath, filename ):
        # save the output list to a SyncedScene.xml in pretty formatting :)
        datafile = os.path.join(dirpath , filename)

        sceneName = os.path.splitext(filename)[0]
        
        locationsData = ["LocationName", "LocationRotation", "LocationOrigin"]
        syncedSceneEntityData = ["Type", "ModelName", "AnimDict", "AnimName", "AttachedEntities", "AttachedInfo"]
        attachedInfoData = ["Offset", "Rot", "ParentBone", "ChildBone", "flags"]
        
        root = ET.Element("fwSyncedSceneEntityManager")
        SceneInfo = ET.SubElement(root, "SceneInfo")

        ET.SubElement( SceneInfo,  "SceneName" ).text = sceneName
        
        Audio = ET.SubElement(SceneInfo, "Audio") # this is where we could add audio info
        
        Locations = ET.SubElement(SceneInfo, "Locations")
        Item = ET.SubElement(Locations, "Item")

        for item in locationsData:
            if item == "LocationRotation":
                ET.SubElement( Item, item, z = self.doubleSpinBox_headingValue.text().encode("utf-8"),
                                           x = self.doubleSpinBox_pitchValue.text().encode("utf-8"),
                                           y = self.doubleSpinBox_rollValue.text().encode("utf-8") )
            elif item == "LocationOrigin":
                ET.SubElement( Item, item, x = self.doubleSpinBox_transXValue.text().encode("utf-8"),
                                           y = self.doubleSpinBox_transYValue.text().encode("utf-8"),
                                           z = self.doubleSpinBox_transZValue.text().encode("utf-8") )      
            else:
                ET.SubElement( Item, item )
            
        SyncedSceneEntites = ET.SubElement(root, "SyncedSceneEntites")
        Item = ET.SubElement(SyncedSceneEntites, "Item") # start off the individual sub items ( create an <Item indent)

        # read selected item from output list
        tabIndex = self.tabWidget_outputList.currentIndex()              
        tableWidgetName = self.tabWidget_outputList.widget(tabIndex)

        # read in the data from the Output List
        model = tableWidgetName.model()
        index_list = [] 
        for row in range(model.rowCount()):
            index_list.append(row)
        
        if index_list:
            for index in range(len(index_list)):
                
                '''
                # we need to figure out what the cell type is in the tablewidget in order to read its contents, as a combobox is handled differently
                # couple of different ways to do that
                print 'QTableWidgetItem', isinstance((tableWidgetName.cellWidget(index_list[index], 0)), QtGui.QTableWidgetItem)
                print 'QComboBox', isinstance((tableWidgetName.cellWidget(index_list[index], 0)), QtGui.QComboBox)
                print (tableWidgetName.cellWidget(index_list[index], 0).__class__.__name__)# prints the type if its a cellwidget, otherwise its a !NoneType
                '''
                
                item_1 = tableWidgetName.item(index_list[index], 1) # stating what item_1 is here, to simplyfy code slightly
                
                for item in syncedSceneEntityData:
                    if item == "Type":
                        if isinstance((tableWidgetName.cellWidget(index_list[index], 0)), QtGui.QComboBox): # this is the character dropdown
                            item_0 = tableWidgetName.cellWidget(index_list[index], 0)
                            pal = item_0.palette()
                            if pal.base().color().getRgb() == (128, 128, 128, 255):
                                ET.SubElement( Item, item, value = "3" )                            
                            if pal.base().color().getRgb() == (255, 0, 0, 255):
                                ET.SubElement( Item, item, value = "2" )
                            if pal.base().color().getRgb() == (0, 255, 0, 255):
                                ET.SubElement( Item, item, value = "1" )
                            if pal.base().color().getRgb() == (0, 191, 255, 255):
                                ET.SubElement( Item, item, value = "0" )     
                        else:
                            item_0 = tableWidgetName.item(index_list[index], 0) # if its not a dropdown, we access the cell differently ('item' rather than 'cellWidget')
                            if item_0.background().color().getRgb() == (128, 128, 128, 255):
                                ET.SubElement( Item, item, value = "3" )
                            if item_0.background().color().getRgb() == (255, 0, 0, 255):
                                ET.SubElement( Item, item, value = "2" )
                            if item_0.background().color().getRgb() == (0, 255, 0, 255):
                                ET.SubElement( Item, item, value = "1" )
                            if item_0.background().color().getRgb() == (0, 191, 255, 255):
                                ET.SubElement( Item, item, value = "0" )                

                    elif item == "ModelName":
                        modelName = None
                        if isinstance((tableWidgetName.cellWidget(index_list[index], 0)), QtGui.QComboBox): # this is the character dropdown
                            item_0 = tableWidgetName.cellWidget(index_list[index], 0)
                            modelName = item_0.currentText()
                            ET.SubElement( Item, item ).text = item_0.currentText().encode("utf-8").split("\\")[0]
                        else:
                            item_0 = tableWidgetName.item(index_list[index], 0)
                            modelName = item_0.text()
                            ET.SubElement( Item, item ).text = item_0.text().encode("utf-8").split("\\")[0]
                        if modelName == '':
                            self.dialogueOpenInfoScreen( "\nSave file error."
                                                        + "\nThere is no model data in line: " + str(index+1)
                                                        + "\nPlease check and try again." )
                            return                            

                    elif item == "AnimDict":
                        ET.SubElement( Item, item ).text = item_1.text().encode("utf-8").split("\\")[0]

                    elif item == "AnimName":
                        # is there anim data? if not we want to catch that
                        try:
                            item_1.text().encode("utf-8").split("\\")[1]
                        except:
                            self.dialogueOpenInfoScreen( "\nSave file error."
                                                        + "\nThere is no animation data in line: " + str(index+1)
                                                        + "\nPlease check and try again." )
                            return
                        else:
                            ET.SubElement( Item, item ).text = item_1.text().encode("utf-8").split("\\")[1]

                    elif item == "AttachedEntities":
                        ET.SubElement( Item, item )

                    elif item == "AttachedInfo":
                        AttachedInfo = ET.SubElement(Item, "AttachedInfo")
                        for subItem in attachedInfoData:
                            if subItem == "Offset":
                                ET.SubElement( AttachedInfo, subItem, x = "0.0", y = "0.0", z = "0.0")
                            elif subItem == "Rot":
                                ET.SubElement( AttachedInfo, subItem, x = "0.0", y = "0.0", z = "0.0")               
                            elif subItem == "ParentBone":
                                ET.SubElement( AttachedInfo, subItem, value = "-1" )      
                            elif subItem == "ChildBone":
                                ET.SubElement( AttachedInfo, subItem, value = "-1" ) 
                            elif subItem == "flags":
                                ET.SubElement( AttachedInfo, subItem, value = "0" )
                        Item = ET.SubElement(SyncedSceneEntites, "Item") # cap off the individual sub items ( create an /Item> indent)

        xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ", encoding='UTF-8')
        
        if self.checkBox_perforce.isChecked():
            self.perforceFileCheck( datafile, True )

        try:
            with open(datafile, "w") as f:
                f.write(str(xmlstr.decode('UTF-8')))
                f.close()
                if InfoScreen.active: # see if the infoscreen is active, otherwise this could be slightly annoying
                    self.dialogueOpenInfoScreen("\nSave Successful.")
                    
        except Exception, e:
            self.dialogueOpenInfoScreen("\nSave file error: " + str(e) + "\n\nIs this file checked out?"
                                + "\nPlease check and try again.")


    def loadSyncedSceneFolderCallback( self ):
        # popup for user to load all synced scenes that live in a folder
        dirPath = self.lineEdit_syncedSceneFolderValue.text()
        
        if '*' in dirPath:# url:bugstar:6414873 - [techart][create or edit sync scene] don't allow wildcards
            self.dialogueOpenInfoScreen("\nPlease don't use wildcards.")
            self.lineEdit_syncedSceneFolderValue.setText('X:/gta5/assets_ng/anim/synced_scenes/')
            self.lineEdit_newFolderNameValue.setText('X:/gta5/assets_ng/anim/synced_scenes/')
            self.lineEdit_newFolderNameValue.clearFocus()
            return
        
        selected_directory = self.diloguePopup( True, dirPath )

        if os.path.isdir(str(selected_directory)):
            self.lineEdit_syncedSceneFolderValue.setText(QtGui.QApplication.translate("MainWindow", str(selected_directory), None, QtGui.QApplication.UnicodeUTF8))
            
            dirPath = self.lineEdit_syncedSceneFolderValue.text()# make sure dirpath is updated. fix for - url:bugstar:6011128 - Create or Edit Synced Scene - error

            xmlList = os.listdir(str(selected_directory))
            
            for item in xmlList:
                filename, file_extension = os.path.splitext(item)
                if file_extension == ".xml" or file_extension == ".XML":
                    
                    self.parseXML( XmlType.SYNCED_SCENE, dirPath, filename + ".xml" )

            self.lineEdit_newFolderNameValue.setText(dirPath)
            self.lineEdit_newFolderNameValue.clearFocus()


    def saveSyncedSceneFolderCallback( self ):
        # iterate through the open output tabs saving the data
        dirpath = self.lineEdit_syncedSceneFolderValue.text() 
        
        self.makeDir( dirpath )# check to see if the synced scene output folder exists

        tabCount = self.tabWidget_outputList.count()
        
        for index in range(tabCount):
            filename = self.tabWidget_outputList.tabText(index)
            if filename != "":
                self.tabWidget_outputList.setCurrentIndex(index)    
                self.saveSyncedScenes( dirpath, filename )


    def rexMBFilenameCallback( self ):
        # popup for user to select a rexmb file
        workingFilename = RS.Globals.Application.FBXFileName
        path, filename = os.path.split(workingFilename)
        if self.lineEdit_rexMBFilenameValue.text() != "":
            path, filename = os.path.split(self.lineEdit_rexMBFilenameValue.text().encode("utf-8"))
            dirpath = path
        else:
            dirpath = path
        popup = QtGui.QFileDialog()
        popup.setViewMode(QtGui.QFileDialog.List)
        popup.setOption(QtGui.QFileDialog.ShowDirsOnly)
        popup.setDirectory(dirpath)
        selected_directory = QtGui.QFileDialog.getOpenFileName(None, "Choose an Export Markup File", "", "rexMB files (*.rexmb)")
        if selected_directory[0] != "":
            self.lineEdit_rexMBFilenameValue.setText(QtGui.QApplication.translate("MainWindow", str(selected_directory[0]), None, QtGui.QApplication.UnicodeUTF8))

            path, filename = os.path.split(self.lineEdit_rexMBFilenameValue.text().encode("utf-8"))
            self.parseXML( XmlType.REXMB, path, filename )


    def removeTab( self ):
        index = self.tabWidget_outputList.currentIndex()
        filename = self.tabWidget_outputList.tabText(index)

        self.tabWidget_outputList.removeTab(index)
        
        # tidy up the xmlLists.data list
        for item in xmlLists.data:
            if filename in item:
                index = xmlLists.data.index(item)
                del xmlLists.data[index]
                
        # if all tabs have been closed, lets create a label to fill that gap
        if self.tabWidget_outputList.count() == 0:
            self.createBlankTab()

                
    def createBlankTab( self ):
        self.label_output_notinuse = QtGui.QLabel(self.tab_1)
        self.label_output_notinuse.setObjectName("label_output_notinuse")
        self.gridLayout_5.addWidget(self.label_output_notinuse, 0, 0, 1, 1)
        self.tabWidget_outputList.addTab(self.tab_1, "")
        self.label_output_notinuse.setText(QtGui.QApplication.translate("MainWindow",
            "<html><head/><body><p align=\"center\">Output currently not in use. Open a synced scene, or create a new one.</p></body></html>",
            None, QtGui.QApplication.UnicodeUTF8))


    def createTab( self, count, filename ):
        tableWidgetName = str("self.tableWidget_outputList_") + filename
        tableWidgetName = QtGui.QTableWidget()
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(tableWidgetName.sizePolicy().hasHeightForWidth())
        tableWidgetName.setSizePolicy(sizePolicy)
        tableWidgetName.setSelectionBehavior(QtGui.QAbstractItemView.SelectColumns)
        tableWidgetName.setShowGrid(True)
        tableWidgetName.setObjectName(str(tableWidgetName))
        tableWidgetName.setColumnCount(2)
        tableWidgetName.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        tableWidgetName.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        tableWidgetName.setHorizontalHeaderItem(1, item)
        tableWidgetName.horizontalHeader().setCascadingSectionResizes(False)
        tableWidgetName.horizontalHeader().setDefaultSectionSize(200)
        tableWidgetName.horizontalHeader().setHighlightSections(False)
        tableWidgetName.horizontalHeader().setMinimumSectionSize(200)
        tableWidgetName.horizontalHeader().setStretchLastSection(False)
        tableWidgetName.verticalHeader().setVisible(False)
        tableWidgetName.verticalHeader().setDefaultSectionSize(30)
        tableWidgetName.verticalHeader().setHighlightSections(True)
        tableWidgetName.verticalHeader().setMinimumSectionSize(30)
        tableWidgetName.verticalHeader().setStretchLastSection(False)
        self.tabWidget_outputList.addTab(tableWidgetName, filename)

        header = tableWidgetName.horizontalHeader()
        header.setResizeMode(1, QtGui.QHeaderView.Stretch)
        tableWidgetName.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows) 
        
        tableWidgetName.horizontalHeaderItem(0).setText(QtGui.QApplication.translate("MainWindow", "Character or Prop", None, QtGui.QApplication.UnicodeUTF8))
        tableWidgetName.horizontalHeaderItem(1).setText(QtGui.QApplication.translate("MainWindow", "Animation", None, QtGui.QApplication.UnicodeUTF8))
        
        self.tabWidget_outputList.setCurrentIndex(count)
        self.tabWidget_outputList.setMovable(True)
        
        if self.tabWidget_outputList.tabText(0) == "":
            self.tabWidget_outputList.removeTab(0)
        
        self.doubleSpinBox_pitchValue.setValue(0)
        self.doubleSpinBox_rollValue.setValue(0)
        self.doubleSpinBox_headingValue.setValue(0)
        
        self.doubleSpinBox_transXValue.setValue(0)
        self.doubleSpinBox_transYValue.setValue(0)
        self.doubleSpinBox_transZValue.setValue(0)  
        
        # cretate an entry in our xmlLists.data 
        list = [filename.encode("utf-8"), 0, 0, 0, 0, 0, 0]
        
        xmlLists.data.append(list)
        
        return tableWidgetName


    def newSyncedSceneCallback( self ):
        count = self.tabWidget_outputList.count()
                
        if self.lineEdit_syncedSceneFilenameValue.text() != "":
            
            dirpath, filename = os.path.split(self.lineEdit_syncedSceneFilenameValue.text().encode("utf-8"))
            fileExtension = os.path.splitext(filename)[1]
            
            if fileExtension != ".xml":
                filename = self.lineEdit_syncedSceneFilenameValue.text() + ".xml"
                self.lineEdit_syncedSceneFilenameValue.setText(filename)
            
            for i in range(count):
                if filename == self.tabWidget_outputList.tabText(i):
                    #print "found ",str(self.tabWidget_outputList.tabText(i))
                    self.tabWidget_outputList.setCurrentIndex(i)
                    return

            self.createTab( count, filename )

            
    def parseXML( self, xmlType, dirpath, filename ):
        if xmlType == XmlType.REXMB: # parse an rexmb file

            # clear out listWidget_charactersAndProps if it is populated
            itemList = [self.listWidget_charactersAndProps.item(i) for i in range(self.listWidget_charactersAndProps.count())]
            for item in range(len(itemList)):
                self.listWidget_charactersAndProps.takeItem(self.listWidget_charactersAndProps.row(itemList[item])) 
                
            # clear out listWidget_animations if it is populated
            itemList = [self.listWidget_animations.item(i) for i in range(self.listWidget_animations.count())]
            for item in range(len(itemList)):
                self.listWidget_animations.takeItem(self.listWidget_animations.row(itemList[item]))
             
            datafile = os.path.join( dirpath, filename )
            
            charAndPropList = []
            subItemsList = []
            animList = []
            
            if os.path.isfile(datafile): #does this file exist? hmmmm
                tree = ET.parse(datafile)
                root = tree.getroot()

                # grab the default output path
                outputPath = None
                
                for outputPath in root.iter("outputPath"):
                    if outputPath.text != None:
                        
                        if outputPath.text.startswith("x") or outputPath.text.startswith("X"):               
                            origOutputPath = outputPath.text
                            
                            if not "export_mb" in origOutputPath:
                                self.dialogueOpenInfoScreen( "\nThe rexmb default output path is not an export_mb location?"
                                                            + "\nPlease check and try again.")
                                return
                            else:                   
                                origOutputPath = origOutputPath.split('export_mb\\')
                                origOutputPath[1] = origOutputPath[1].replace('\\', '')
                                
                                outputPath = origOutputPath[1]
                        
                        elif outputPath.text.startswith("//"):
                            
                            self.dialogueOpenInfoScreen( "\nThe rexmb default output path is pointing to the depot?"
                                                        + "\nThis is not supported in the exporter."
                                                        + "\nPlease check and try again.")
                            return                           
                    else:
                        self.dialogueOpenInfoScreen( "\nThe rexmb default output path looks odd?\nPlease check and try again." )
                        return
                        
                # grab the Characters and Props
                modelName = None
                modelType = None
                animPrefix = None
                animSuffix = None
                modelSubString = None
                
                for item in root.findall(".//*[@type='RexCharAnimData']"):
                    for child in item:
                       
                        if child.tag == "inputModelNames":
                            inputModelNames = child[0].text
                            inputModelNamesNamespace = inputModelNames.split( ':' )
                            if len(inputModelNamesNamespace) == 2:
                                modelName = inputModelNamesNamespace[0]
                                modelSubString = inputModelNamesNamespace[1]
                                #print modelSubString
                            else:
                                modelName = inputModelNames
                            modelName = modelName.split("^")[0]
                            subItemsList.append(modelName)
                            #print modelName

                        if child.tag == "charCtrlFile":  
                            modelType = child.text
                            if "Veh" in modelType:
                                modelType = "grey"
                            elif "Prop" in modelType: 
                                modelType = "lime"
                            elif "Camera" in modelType:
                                modelType = "red"
                            else:
                                modelType = "deepskyblue"
                            subItemsList.append(modelType)
                        
                        if child.tag == "animPrefix":
                            animPrefix = child.text
                            subItemsList.append(animPrefix)
                           
                        if child.tag == "animSuffix":
                            animSuffix = child.text
                            subItemsList.append(animSuffix)
                            
                        if child.tag == "altOutputPath":
                            altOutputPath = child.text
                            if not '(none specified)' in altOutputPath:
                                altOutputPath = altOutputPath.split('export_mb\\')
                                altOutputPath[1] = altOutputPath[1].replace('\\', '')
                                subItemsList.append(altOutputPath[1])
                            else:
                                subItemsList.append(None)

                # grab the animations data from the rexmb file
                for items in root.findall(".//*[@type='RexMbAnimSegmentData']"):
                    takeNameOverride = None
                    altOutputPaths = None
                    
                    for child in items:

                        if child.tag == "takeName":
                            takeName = child.text
                            animList.append(takeName)
                            
                        if child.tag == "takeNameOverride":
                            if not child.text == None:
                                takeNameOverride = child.text
                            animList.append(takeNameOverride)
                            
                        if child.tag == "altOutputPaths":
                            if child.text == None:
                                animList.append(outputPath)
                            else:
                                if len(child) > 1: # need to see if there is more than one altOutputPath, if there is, we need to get them all
                                    count = 0 # first append can just have the first item from the altOutputPath list, all others need to have the takeName and takeNameOverride appened before the altOutputPaths
                                    for i in range(len(child)):
                                        if count == 0: # simplest way to grab the first item from the altOutputPath list
                                            altOutputPaths = child[i].text
                                            altOutputPaths = altOutputPaths.split('export_mb\\')
                                            altOutputPaths[1] = altOutputPaths[1].replace('\\', '')
                                            animList.append(altOutputPaths[1])
                                            
                                            count += 1
                                        else:
                                            altOutputPaths = child[i].text
                                            altOutputPaths = altOutputPaths.split('export_mb\\')
                                            altOutputPaths[1] = altOutputPaths[1].replace('\\', '')
                                            
                                            animList.append(takeName)
                                            animList.append(takeNameOverride)
                                            animList.append(altOutputPaths[1])
                                            
                                            count += 1      
                                else:
                                    altOutputPaths = child[0].text
                                    
                                    altOutputPaths = altOutputPaths.split('export_mb\\')
                                    altOutputPaths[1] = altOutputPaths[1].replace('\\', '')
                                    animList.append(altOutputPaths[1])
                                    
            # group the charAndPropList into sub lists, with [modelName, modelType, prefix, suffix, altOutputPaths] as the sub items
            N = 5
            charAndPropList = [subItemsList[n:n+N] for n in range(0, len(subItemsList), N)]

            # group the list animList sub lists, with [takeName, takeNameOverride, altOutputPaths] as the sub items
            N = 3
            groupedAnimList = [animList[n:n+N] for n in range(0, len(animList), N)]   
            
            # populate the listWidget_charactersAndProps
            for i in range(len(charAndPropList)):
                
                prefixsuffix = ""

                if not charAndPropList[i][1] == None and not charAndPropList[i][2] == None:
                    prefixsuffix = "( Prefix&Suffix = " + charAndPropList[i][1] + "&" + charAndPropList[i][2] + " )  "
                else:
                    if not charAndPropList[i][1] == None:
                        prefixsuffix = "( Prefix = " + charAndPropList[i][1] + " )  "

                    if not charAndPropList[i][2] == None:
                        prefixsuffix = "( Suffix = " + charAndPropList[i][2] + " )  "
                
                if not charAndPropList[i][4] == None: 
                    fullString = str(prefixsuffix + charAndPropList[i][0]) + " -> absolute output path used -> " + str(charAndPropList[i][4])
                else:
                    fullString = prefixsuffix + charAndPropList[i][0]
                self.listWidget_charactersAndProps.addItem(fullString)
                self.listWidget_charactersAndProps.item(i).setBackground( QtGui.QColor(charAndPropList[i][3]) )
                self.listWidget_charactersAndProps.item(i).setForeground( QtGui.QColor(0,0,0) )
                  
            for i in range(len(groupedAnimList)):
                
                fullAnimName = ""

                if groupedAnimList[i][1] == None:
                    fullAnimName = groupedAnimList[i][2] + "\\" + groupedAnimList[i][0]
                    self.listWidget_animations.addItem(fullAnimName)
                else:
                    fullAnimName = groupedAnimList[i][2] + "\\" + groupedAnimList[i][1]

                    # grabbing the exported anim name before it was overridden
                    origionalAnimName = "    (original anim name was: " + groupedAnimList[i][0] + ")"
                    
                    # some extra html style formatting to have two colours on same line (to denote the old name and new)
                    # technically it adds the text as a QLabel set within another widget
                    widgitItem = QtGui.QListWidgetItem() 
                    widget = QtGui.QWidget()
                    widgetText = QtGui.QLabel( fullAnimName + '<span style="color:#707070;">' + origionalAnimName + '</span>')
                    widgetText.setObjectName('label' + str(i))
                    widgetLayout = QtGui.QHBoxLayout()
                    widgetLayout.addWidget(widgetText)
                    widgetLayout.setSizeConstraint(QtGui.QLayout.SetFixedSize)
                    widgetLayout.setContentsMargins(0, 0, 0, 0)# otherwise it'll be offset slightly
                    widget.setLayout(widgetLayout)      
                    self.listWidget_animations.addItem(widgitItem)
                    widgitItem.setSizeHint(widget.sizeHint()) 
                    self.listWidget_animations.setItemWidget(widgitItem, widget)
                    index = self.listWidget_animations.count()

            '''
            # example how to read the line
            for i in range(self.listWidget_animations.count()):
                if self.listWidget_animations.item(i).text():
                    print self.listWidget_animations.item(i).text()
                else:
                    print i
                    data = self.listWidget_animations.findChild(QtGui.QLabel, 'label' + str(i))
                    print data.text() 
            '''
        
        if xmlType == XmlType.SYNCED_SCENE: # parse a synced scene file
        
            datafile = os.path.join( dirpath, filename )

            # remove the defualt blank tab if its there
            if self.tabWidget_outputList.tabText(0) == "":
                self.tabWidget_outputList.removeTab(0)
        
            count = self.tabWidget_outputList.count()

            # if we're asked to open a file that already exists as a tab then we just jump to the tab and make it selected
            for i in range(count):
                if filename == self.tabWidget_outputList.tabText(i):
                    self.tabWidget_outputList.setCurrentIndex(i)
                    return

            tableWidgetName = self.createTab( count, filename )

            # clear out tableWidget_outputList if it is populated # hand to keep just in case...
            tableWidgetName.setRowCount(0) 

            parsedSyncedSceneEntityList = []
            syncedSceneEntityList = []
            
            try:
                tree = ET.parse(datafile)
            except Exception, e:
                # this is a error caused by a problem synced scene, lets report that and get back to business
                self.dialogueOpenInfoScreen( "\n.xml parse error: " + str(e)
                                    + "\n\nIn scene:"
                                    + "\n" + datafile.replace("/","\\")
                                    + "\n\nThis is probably a corrupted synced scene."
                                    + "\nPlease check in notepad for anything odd, and try again." )
                
                # clear out the corrupted filename from the UI
                self.lineEdit_syncedSceneFilenameValue.setText("")
                
                # a tab would have been generated, lets delete that out                
                index = self.tabWidget_outputList.currentIndex()
                self.tabWidget_outputList.removeTab(index)
                
                # if all tabs have been closed, lets create a label to fill that gap
                if self.tabWidget_outputList.count() == 0:
                    self.createBlankTab()
                return
            
            root = tree.getroot()
            
            LocationRotation = root.findall(".//LocationRotation")
            if 'x' in LocationRotation[0].attrib:
                pitch = float(LocationRotation[0].attrib['x'])
                self.doubleSpinBox_pitchValue.setValue(float(LocationRotation[0].attrib['x']))
            if 'y' in LocationRotation[0].attrib:
                roll = float(LocationRotation[0].attrib['y'])
                self.doubleSpinBox_rollValue.setValue(float(LocationRotation[0].attrib['y']))
            if 'z' in LocationRotation[0].attrib:
                heading = float(LocationRotation[0].attrib['z'])
                self.doubleSpinBox_headingValue.setValue(float(LocationRotation[0].attrib['z']))
                LocationOrigin = root.findall(".//LocationOrigin")
            if 'x' in LocationOrigin[0].attrib:
                transX = float(LocationOrigin[0].attrib['x'])
                self.doubleSpinBox_transXValue.setValue(float(LocationOrigin[0].attrib['x']))
            if 'y' in LocationOrigin[0].attrib:
                transY = float(LocationOrigin[0].attrib['y'])
                self.doubleSpinBox_transYValue.setValue(float(LocationOrigin[0].attrib['y']))
            if 'z' in LocationOrigin[0].attrib:
                transZ = float(LocationOrigin[0].attrib['z'])
                self.doubleSpinBox_transZValue.setValue(float(LocationOrigin[0].attrib['z']))
            
            # update the xmlLists.data entry (which we created when we created the new tab) with our newly parsed scene
            for item in xmlLists.data:
                if filename in item:
                    item[0] = filename
                    item[1] = pitch
                    item[2] = roll
                    item[3] = heading
                    item[4] = transX
                    item[5] = transY
                    item[6] = transZ

            ModelType = None
            ModelName = None
            AnimDict = None
            AnimName = None
            for item in root.findall(".//SyncedSceneEntites/Item"):
                for child in item:
                    if child.tag == "Type":
                        ModelType = child.attrib['value']
                        parsedSyncedSceneEntityList.append(ModelType)
                    
                    if child.tag == "ModelName":
                        ModelName = child.text
                        parsedSyncedSceneEntityList.append(ModelName)
                         
                    if child.tag == "AnimDict":
                        AnimDict = child.text
                        parsedSyncedSceneEntityList.append(AnimDict)
                       
                    if child.tag == "AnimName":
                        AnimName = child.text
                        parsedSyncedSceneEntityList.append(AnimName)
        
            # group parsedSyncedSceneEntityList into sub lists, with Type, ModelName, AnimDict, AnimName as the sub items
            N = 4
            syncedSceneEntityList = [parsedSyncedSceneEntityList[n:n+N] for n in range(0, len(parsedSyncedSceneEntityList), N)]
            
            # add data to the tableWidget_outputList
            for i in range(len(syncedSceneEntityList)):
                
                # create a blank row
                rowPosition = tableWidgetName.rowCount()# gets the last row
                tableWidgetName.insertRow(rowPosition)
                
                # add data to it
                if syncedSceneEntityList[i][1]:     
                    tableWidgetName.setItem(rowPosition , 0, QtGui.QTableWidgetItem( syncedSceneEntityList[i][1] ))
                else:
                    tableWidgetName.setItem(rowPosition , 0, QtGui.QTableWidgetItem( "null" ))
                    self.dialogueOpenInfoScreen( "\nError loading Character or prop from synced scene .xml\nPlease check and try again." )
                    
                if syncedSceneEntityList[i][2] and syncedSceneEntityList[i][3]:
                    tableWidgetName.setItem(rowPosition , 1, QtGui.QTableWidgetItem( syncedSceneEntityList[i][2] + "\\" + syncedSceneEntityList[i][3] )) 
                else:
                    tableWidgetName.setItem(rowPosition , 1, QtGui.QTableWidgetItem( "null" + "\\" + "null" ))
                    self.dialogueOpenInfoScreen( "\nError loading dict location or anim name from synced scene .xml\nPlease check and try again." ) 
                
                if syncedSceneEntityList[i][0] == "3":
                    tableWidgetName.item(rowPosition, 0).setBackground( QtGui.QColor("grey") )
                elif syncedSceneEntityList[i][0] == "2":
                    tableWidgetName.item(rowPosition, 0).setBackground( QtGui.QColor("red") )
                elif syncedSceneEntityList[i][0] == "1":
                    tableWidgetName.item(rowPosition, 0).setBackground( QtGui.QColor("lime") )
                elif syncedSceneEntityList[i][0] == "0":
                    #tableWidgetName.item(rowPosition, 0).setBackground( QtGui.QColor("deepskyblue") )
                    combo = QtGui.QComboBox()
                    
                    pal = self.setPallete( combo )

                    if syncedSceneEntityList[i][1] in CharacterList.data:
                        index = CharacterList.data.index(syncedSceneEntityList[i][1])
                    else:
                        characterListLower = [item.lower() for item in CharacterList.data]

                        if syncedSceneEntityList[i][1].lower() in characterListLower:
                            index = characterListLower.index(syncedSceneEntityList[i][1].lower())
                        else:
                            self.dialogueOpenInfoScreen( '\n"' + str(syncedSceneEntityList[i][1]) + '"' +
                                                         " is not in the available character list?\nDefaulting to index 0.\nPlease check and try again." )
                            index = 0
                        
                    for t in CharacterList.data:
                        '''
                        #code to align the list items in the center of the drop down
                        delegate = AlignDelegate(combo)
                        combo.setItemDelegate(delegate)
                        '''
                        combo.setEditable(True)
                        # bit of a workaround to center text and make it non editable
                        ledit = combo.lineEdit()
                        ledit.setAlignment(QtCore.Qt.AlignCenter)
                        ledit.setReadOnly(True)
 
                        combo.addItem(t)
                        combo.setPalette(pal)
                        combo.setCurrentIndex( index )
                        #combo.SizeAdjustPolicy(AdjustToContents)
                        #combo.view(int(len(self.characterDepotFile)/3))

                    tableWidgetName.setCellWidget(rowPosition,0,combo)

                tableWidgetName.item(rowPosition, 0).setForeground(QtGui.QColor(0,0,0))
                tableWidgetName.item(rowPosition, 0).setTextAlignment(QtCore.Qt.AlignCenter)
        
        if xmlType == XmlType.META: # parse a ped meta file
            
            tree = ET.parse(dirpath)
            root = tree.getroot()
            
            item = root.findall(".//InitDatas/Item/Name")
            for child in item:
                if child.tag == "Name":
                    pedName = child.text
                    CharacterList.data.append(pedName)


    def setPallete( self, combo ):
        pal = combo.palette()
        pal.setColor(QtGui.QPalette.Button, QtGui.QColor("deepskyblue"))
        #pal.setColor(QtGui.QPalette.Button, QtGui.QColor("dodgerblue"))
        pal.setColor(QtGui.QPalette.ButtonText, QtGui.QColor("black"))
        #pal.setColor(QtGui.QPalette.ButtonText, QtGui.QColor("blue"))
        pal.setColor(QtGui.QPalette.Highlight, QtGui.QColor("black"))
        pal.setColor(QtGui.QPalette.Base, QtGui.QColor("deepskyblue")) 
        pal.setColor(QtGui.QPalette.Text, QtGui.QColor("black"))
        pal.setColor(QtGui.QPalette.Background, QtGui.QColor("deepskyblue"))
        
        return pal

                
    def deleteSelectedItemCallback( self ):
        # delete selected item from output list
        tabIndex = self.tabWidget_outputList.currentIndex()
        #tabName = self.tabWidget_outputList.tabText(tabIndex)                
        tableWidgetName = self.tabWidget_outputList.widget(tabIndex)
                
        index_list = []
        if self.tabWidget_outputList.tabText(tabIndex) != "":
            for model_index in tableWidgetName.selectionModel().selectedRows():
                index = QtCore.QPersistentModelIndex(model_index)
                index_list.append(index)
            
            for index in index_list:                 
                tableWidgetName.removeRow(index.row())

                
    def populateOutputListCallback( self ):
        # grab data from the listwidgets
        charAndPropData = None
        animData = None
        colourData = None
        
        if len(self.listWidget_charactersAndProps.selectedItems()) >= 1 and len(self.listWidget_animations.selectedItems()) == 1:
            charAndPropData = [item.text().encode("utf-8") for item in self.listWidget_charactersAndProps.selectedItems()] 
            colourData = [item.background().color().getRgb() for item in self.listWidget_charactersAndProps.selectedItems()] 
                                                   
            for charAndPropItem in range(len(charAndPropData)):

                if 'absolute output path used' in charAndPropData[charAndPropItem]:
                    animData = []
                    animData.append(charAndPropData[charAndPropItem].split("-> absolute output path used -> ")[1])
                    baseAnim = [item.text().encode("utf-8") for item in self.listWidget_animations.selectedItems()]
                    # check to see if we used a QLabel to populate the QListwidget (if we had a anim name override)
                    if baseAnim[0] != '':
                        animData.append(baseAnim[0].split("\\")[1])
                    else:
                        i = [x.row() for x in  self.listWidget_animations.selectedIndexes()]
                        data = self.listWidget_animations.findChild(QtGui.QLabel, 'label' + str(i[0]))
                        data = data.text().split("<span")[0]
                        animData.append(data.split("\\")[1])
                else:
                    animData = [item.text().encode("utf-8") for item in self.listWidget_animations.selectedItems()]
                    # check to see if we used a QLabel to populate the QListwidget (if we had a anim name override)
                    if animData[0] != '':
                        animData = animData[0].split("\\")
                    else:
                        i = [x.row() for x in  self.listWidget_animations.selectedIndexes()]
                        data = self.listWidget_animations.findChild(QtGui.QLabel, 'label' + str(i[0]))
                        data = data.text().split("<span")[0]
                        animData = data.split("\\")
         
                # break up data to be more useful
                if not '=' in charAndPropData[charAndPropItem]:
                    charData = charAndPropData[charAndPropItem]
                    if 'absolute output path used' in charData:
                        charData = charData.split(" -> absolute output path used -> ")[0]
                    usePrefix = 'NoPrefix'
                else:
                    charData = charAndPropData[charAndPropItem].split(")  ")[1]
                    if 'absolute output path used' in charData:
                        charData = charData.split(" -> absolute output path used -> ")[0]

                    prefixSuffixData = charAndPropData[charAndPropItem].split(" )")[0]

                    if '&' in prefixSuffixData:
                        usePrefixSuffix = True
                        usePrefix = False
                    elif 'Prefix' in prefixSuffixData:
                        usePrefix = True
                        usePrefixSuffix = False
                    else:
                        usePrefix = False
                        usePrefixSuffix = False

                    prefixSuffixData = prefixSuffixData.split("= ")[1]

                # add data to the tableWidget_outputList
                tabIndex = self.tabWidget_outputList.currentIndex()
                tableWidgetName = self.tabWidget_outputList.widget(tabIndex)

                if self.tabWidget_outputList.tabText(tabIndex) == "":
                    self.dialogueOpenInfoScreen("\nThere is not an open Synced Scene Output List to send data to?\nPlease check and try again.")
                    return

                # create a blank row
                rowPosition = tableWidgetName.rowCount()# gets the last row
                tableWidgetName.insertRow(rowPosition)

                # add data to it
                tableWidgetName.setItem(rowPosition , 0, QtGui.QTableWidgetItem( charData ))
                if colourData[charAndPropItem] == (128, 128, 128, 255):
                    tableWidgetName.item(rowPosition, 0).setBackground( QtGui.QColor("grey") )
                elif colourData[charAndPropItem] == (255, 0, 0, 255):
                    tableWidgetName.item(rowPosition, 0).setBackground( QtGui.QColor("red") )
                elif colourData[charAndPropItem] == (0, 255, 0, 255):
                    tableWidgetName.item(rowPosition, 0).setBackground( QtGui.QColor("lime") )
                elif colourData[charAndPropItem] == (0, 191, 255, 255):
                    combo = QtGui.QComboBox()

                    pal = self.setPallete(combo)

                    if charData in CharacterList.data:
                        index = CharacterList.data.index(charData)
                    else:
                        characterListLower = [item.lower() for item in CharacterList.data]

                        if charData.lower() in characterListLower:
                            index = characterListLower.index(charData.lower())
                        else:
                            self.dialogueOpenInfoScreen( '\n"' + str(charData) + '"' +
                                                         " is not in the available character list?\nDefaulting to index 0.\nPlease check and try again." )
                            index = 0

                    for t in CharacterList.data:
                        '''
                        #code to align the list items in the center of the drop down
                        delegate = AlignDelegate(combo)
                        combo.setItemDelegate(delegate)
                        '''
                        combo.setEditable(True)
                        # bit of a workaround to center text and make it non editable
                        ledit = combo.lineEdit()
                        ledit.setAlignment(QtCore.Qt.AlignCenter)
                        ledit.setReadOnly(True)
                        combo.addItem(t)
                        combo.setPalette(pal)
                        combo.setCurrentIndex(index)

                    tableWidgetName.setCellWidget(rowPosition,0,combo)

                tableWidgetName.item(rowPosition, 0).setForeground(QtGui.QColor(0,0,0))
                tableWidgetName.item(rowPosition, 0).setTextAlignment(QtCore.Qt.AlignCenter)

                if usePrefix == 'NoPrefix':
                    tableWidgetName.setItem(rowPosition, 1, QtGui.QTableWidgetItem( animData[0] + "\\" + animData[1]))
                    tableWidgetName.scrollToBottom()
                else:
                    if usePrefixSuffix:
                        prefixSuffixText = prefixSuffixData.split("&")
                        tableWidgetName.setItem(rowPosition, 1, QtGui.QTableWidgetItem(
                            animData[0] + "\\" + prefixSuffixText[0] + animData[1] + prefixSuffixText[1]))
                        tableWidgetName.scrollToBottom()
                    elif usePrefix:
                        tableWidgetName.setItem(rowPosition, 1, QtGui.QTableWidgetItem( animData[0] + "\\" + prefixSuffixData + animData[1]))
                        tableWidgetName.scrollToBottom()
                    else:
                        tableWidgetName.setItem(rowPosition, 1, QtGui.QTableWidgetItem(
                            animData[0] + "\\" + animData[1] + prefixSuffixData))
                        tableWidgetName.scrollToBottom()

  
    def extractTransformCallback( self ):
        self.pushButton_extractTransform.setText("working, please wait...")
        QtGui.QApplication.processEvents()
        # work out the transform from the asset_tracker.xls
        
        # the location of the asset_tracker.xls file 
        loc = ("x:\gta5\Docs\production\Animation\Asset_Tracker.xls")
        
        if not self.perforceFileCheck( loc, False ):
            path, fileName = os.path.split(loc)
            self.dialogueOpenInfoScreen( "\ndepotFile: " + fileName
                                            + "\npath: " + path
                                            + "\n\nEither does not exist, or is not mapped to current client view."
                                            + "\n\nWe need the " + fileName + " to extrapolate the point origin info."
                                            + "\nPlease check and try again." )
            return
          
        # To open Workbook 
        wb = xlrd.open_workbook(loc) 
        sheet = wb.sheet_by_index(0) 
        
        setName = None
        usePointOrig = True
        
        setNameList = []
        
        for lComp in mobu.FBSystem().Scene.Components:
            if re.search("SetRoot", lComp.Name, re.IGNORECASE): # regexp search ftw # make it case insensitive
                if mobu.FBFindModelByLabelName(str(lComp.LongName)): # just weed out the models
                    setName = lComp.LongName.split(":")[0]
                    if not setName in setNameList:
                        setNameList.append(setName)
                        
                    #print setName
                    if lComp.LongName.split(":")[0] == "SetRoot":
                        self.dialogueOpenInfoScreen( "\nThe 'Set' was not brought in through the Refrence Editor."
                                                    + "\nThis way of merging in the set is not supported."
                                                    + "\nYou'll need to delete this set, then Reference it in."
                                                    + "\nOnce that is complete, please try again." )
                        self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
                        return  
        
        if len(setNameList) > 1:
            self.dialogueOpenInfoScreen( "\nYou have more than one 'Set' in this scene."
                                        + "\nThat means we can't reliably exptrapolate the point origin."
                                        + "\nPlease remove one, and then try again." )
            self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
            return                    

        if setName == None:
            self.dialogueOpenInfoScreen( "\nThere does not seem to be a valid 'Set' in this scene"
                                        + "\nPlease check and try again." )
            self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
            return
        else:   
            for i in range(sheet.nrows):
                if sheet.cell_value(i, 0) == setName:
                    pointOrig_X = sheet.cell_value(i, 5)
                    pointOrig_Y = sheet.cell_value(i, 6)
                    pointOrig_Z = sheet.cell_value(i, 7)
                    pointOrig_rotZ = sheet.cell_value(i, 8)

                    '''
                    # useful debug info - can be deleted?
                    #print "trans: x: " + str(sheet.cell_value(i, 5)) + " y: " + str(sheet.cell_value(i, 6)) + " z: " + str(sheet.cell_value(i, 7)) + " rot: " + str(sheet.cell_value(i, 8))
                    
                    text = ("set: " + str(sheet.cell_value(i, 0)) + "\nrow: " + str(i+1)
                            + "\ntrans: x: " + str(sheet.cell_value(i, 5)) + " y: " + str(sheet.cell_value(i, 6)) + " z: " + str(sheet.cell_value(i, 7))
                            + "\nrot: " + str(sheet.cell_value(i, 8)))
                                            
                    dialogue = Dialogue_InfoScreen()
                    QtGui.QApplication.processEvents()
                    dialogue.show()
                    dialogue.textEdit_infoScreen.setText( text )
                        
                    dialogue.textEdit_infoScreen.moveCursor(QtGui.QTextCursor.End)
                    QtGui.QApplication.processEvents()     
                    '''              

                    usePointOrig = False

        # if there is no point origin info, we should tell the user, then drop out
        if usePointOrig:
            self.dialogueOpenInfoScreen( "\nThe scene 'Set': " + setName + " Does not exist in the doc." 
                        + "\nPlease send a bug to Animation Resources."
                        + "\nIt is not safe to continue." )
            self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
            return
        
        if not mobu.FBFindModelByLabelName(str("exportOrigin_Mover:Dummy01")):
            self.dialogueOpenInfoScreen( "\nThere is no 'exportOrigin_Mover' in this scene."
                        + "\nThis is used to provide an offset when extrapolating the point origin." 
                        + "\nPlease add one and try again." ) 
            self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
            return

        lStartFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        
        offsetOriginRoot = mobu.FBFindModelByLabelName(str("exportOrigin_Mover:Dummy01"))          
        offsetOriginMover = mobu.FBFindModelByLabelName(str("exportOrigin_Mover:mover"))
        
        setRoot = offsetOriginRoot # we can just use the default location of the offset origin to find the centre of the world, this will always be at the centre of the scene
        
        # in order to be 100% accurate with the scene position, we need to do an adjustment if the set was rotated in relation to the game world. We do that with this code.
        # we create a simple parent/child setup and rotate the parent to the game world, then find the new position of the child after that.

        # just do a quick check on the rotations of the exportOrigin, if we aligned to a scene node, this also can break things
        setRootPos = mobu.FBVector3d()
        setRoot.GetVector(setRootPos, mobu.FBModelTransformationType.kModelTranslation, True)
        setRootRot = mobu.FBVector3d()
        setRoot.GetVector(setRootRot, mobu.FBModelTransformationType.kModelRotation, True)        
        
        sceneRoot = offsetOriginMover
        sceneRootRot = mobu.FBVector3d()
        sceneRoot.GetVector(sceneRootRot, mobu.FBModelTransformationType.kModelRotation, True)
        
        pos = [0,0,0]
        rot = [-90,0,0]
        
        # check the scene root is where it should be
        for index, (first, second) in enumerate(zip(setRootPos, pos)):
            if first != second:       
                self.dialogueOpenInfoScreen( "\nThe root of the 'exportOrigin_Mover' has been moved."
                            + "\nPlease move it back to (global) 0,0,0 and try again." ) 
                self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
                return
        for index, (first, second) in enumerate(zip(setRootRot, rot)):
            if first != second:       
                self.dialogueOpenInfoScreen( "\nThe root of the 'exportOrigin_Mover' has been rotated."
                            + "\nPlease rotate it back to (global) -90,0,0 and try again." ) 
                self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
                return

        # check the exportOrigin
        originErrorA = False
        originErrorB = False
        
        # the rotations can be one of two values on x and z if y has changed (globally)
        if int(sceneRootRot[0]) != 90 or int(sceneRootRot[2]) != -180:
            originErrorA = True              
        if int(sceneRootRot[0]) != -90 or int(sceneRootRot[2]) != 0:
            originErrorB = True
        else:
            originErrorA = False

        if originErrorA or originErrorB:    
            self.dialogueOpenInfoScreen( "\nThe 'exportOrigin_Mover:mover' has got some rotations on the x and y axis."
                        + "\nPlease check this in-game. If this does not look right, put (local) x and y back to 0." )
        # end of offset origin checks
           
        Marker_Root = mobu.FBModelMarker("Marker_Root")
        
        Marker_Root.Show = True
        Marker_Root.Scaling = mobu.FBVector3d(100, 100, 100)
        Marker_Root.Size = 15.00
        Marker_Root.PropertyList.Find('Look').Data = 2
        Marker_Root.PropertyList.Find('ShowLabel').Data = True
        
        Marker_Child = mobu.FBModelMarker("Marker_Child")
        Marker_Child.Show = True
        Marker_Child.Scaling = mobu.FBVector3d(100, 100, 100)
        Marker_Child.Size = 15.00
        Marker_Child.PropertyList.Find('Look').Data = 2
        Marker_Child.PropertyList.Find('ShowLabel').Data = True

        Marker_Child.Parent = Marker_Root
        
        Marker_Root.Translation = setRootPos
        Marker_Root.Rotation = setRootRot
  
        ParentModel = offsetOriginMover
        ChildModel = Marker_Child 
        myManager = mobu.FBConstraintManager()               # creating parent constraint
        myCons = myManager.TypeCreateConstraint("Parent/Child")

        myCons.Name = (ParentModel.Name + " => " + ChildModel.Name)   

        myCons.ReferenceAdd(0, ChildModel)                 
        myCons.ReferenceAdd(1, ParentModel)             
      
        myCons.Active = True 
        
        Marker_Child.Translation.SetAnimated(True) # make sure these are animatatable
        Marker_Child.Rotation.SetAnimated(True) # make sure these are animatatable
       
        # plot to the object, as it might be constrained!
        self.plotAnimToObj( Marker_Child )
        
        '''
        # sometimes there still isn't a key on the objects
        # insertkeys code, don't delete for now, but it doesn't work as I hoped
        for curve in range(3):
            fcurve = Marker_Child.Translation.GetAnimationNode().Nodes[curve].FCurve
            fcurve.KeyInsert(mobu.FBTime(0,0,0,lStartFrame))
        
        for curve in range(3):
            fcurve = Marker_Child.Rotation.GetAnimationNode().Nodes[curve].FCurve
            fcurve.KeyInsert(mobu.FBTime(0,0,0,lStartFrame))  
        '''      
        
        RS.Globals.Scene.Evaluate() # got to add an evaluate and pause in there, just breaks otherwise
        mobu.FBSleep(20)
        
        myCons.FBDelete()
        
        RS.Globals.Scene.Evaluate() # got to add an evaluate and pause in there, just breaks otherwise
        mobu.FBSleep(20)

        Marker_Root.Rotation = mobu.FBVector3d(setRootRot[0], pointOrig_rotZ, setRootRot[2])

        RS.Globals.Scene.Evaluate() # got to add an evaluate and pause in there, just breaks otherwise
        mobu.FBSleep(20)
        
        scenePos = mobu.FBVector3d()
        Marker_Child.GetVector(scenePos, mobu.FBModelTransformationType.kModelTranslation, True)
        
        sceneRot = mobu.FBVector3d()
        Marker_Child.GetVector(sceneRot, mobu.FBModelTransformationType.kModelRotation, True)
        
        if setRootPos[0] != 0 or setRootPos[1] != 0 or setRootPos[2] != 0:
            self.dialogueOpenInfoScreen( "\nThe 'Set' has been relocated in the scene, this feature is no longer supported."
                                            + "\nPlease reload the 'Set', and adjust your scene accordingly." )
            self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
            return
        else:        
            finalX = pointOrig_X + (scenePos[0]/100)
            finalY = pointOrig_Y - (scenePos[2]/100)
            finalZ = pointOrig_Z + (scenePos[1]/100)

        # got to use the local Z axis (and we need to clear the animation then re-add a key as the values can exceed +-180 degreee's otherwise
        #offsetOriginMover.Selected = True        
        
        for track in offsetOriginMover.AnimationNode.Nodes:
            if 'Lcl Rotation' in track.Name:
                for axis in track.Nodes:
                    if axis.FCurve:
                        axis.FCurve.EditClear()        
        
        RS.Globals.Scene.Evaluate() # got to add an evaluate and pause in there, just breaks otherwise
        mobu.FBSleep(20)  
        
        '''
        offsetOriginMover.Rotation.SetAnimated(True) # make sure these are animatable
        fcurve = offsetOriginMover.Rotation.GetAnimationNode().Nodes[2].FCurve
        fcurve.KeyInsert(mobu.FBTime(0,0,0,lStartFrame))      
        '''
        
        # plot to the object, as it might be constrained!
        self.plotAnimToObj( offsetOriginMover )   
         
        ZKeyValue = None
                       
        for track in offsetOriginMover.AnimationNode.Nodes:
            if 'Lcl Rotation' in track.Name:
                for axis in track.Nodes:
                    if 'Z' in axis.Name:
                        for lKey in axis.FCurve.Keys:
                            ZKeyValue = lKey.Value        
   
        if ZKeyValue != None:
            heading = ZKeyValue + pointOrig_rotZ
        else:
            self.dialogueOpenInfoScreen( "\nThere was an issue adding a key to the exporOrigin_Mover:mover"
                                        + "\nPlease select this object and add a key manually.")
            self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
            return
        
        if heading > 180: # slider in the synced scene tool runs from -180 to + 180, so we got to do some maths to fix our heading
            heading = - heading + 180
        
        self.doubleSpinBox_transXValue.setValue(finalX)
        self.doubleSpinBox_transYValue.setValue(finalY)
        self.doubleSpinBox_transZValue.setValue(finalZ)
        
        self.doubleSpinBox_headingValue.setValue(heading)
        
        Marker_Root.FBDelete()
        Marker_Child.FBDelete()
        
        self.updateTransformList()
        
        self.pushButton_extractTransform.setText("Extrapolate Scene Origin using Point Origin Info")
        QtGui.QApplication.processEvents()


    def perforceFileCheck( self, p4File, syncedScene, pedMetaSearch=False ):
        #p4File = p4File.replace( "//depot/", "x:/" )
        #p4File = p4File.replace( "//", "x:/" )
        #p4File = p4File.replace( "/", "\\" )
        #p4File = p4File.replace( "@", "%40")# replace the iffy characters that perforce uses for other stuff
        #p4File = p4File.replace( "#", "%23")# replace the iffy characters that perforce uses for other stuff
        #p4File = p4File.strip()# remove dead-spaces at front and end of string
   
        currentDepotFile = []
        filename = os.path.basename(p4File)
        dirPath = os.path.dirname(p4File)
        
        p4Path = p4File

        try:
            PerforceFileData = Perforce.Run("fstat", [p4Path])# get perforce stats on our pose file
        except Exception, e:
            self.dialogueOpenInfoScreen( "\nPerforce error: " + str(e) )
            return False    

        for item in PerforceFileData: 
            depotFile = item['depotFile']
            currentDepotFile.append(depotFile.encode("utf-8"))
            
            haveRev = item['haveRev']
            if not haveRev:# if there is no local file, we need to handle that
                currentDepotFile.append("0")
            else:
                currentDepotFile.append(haveRev.encode("utf-8"))
            
            headRev = item['headRev']
            if not headRev:# if this is a new file, we need to handle that
                currentDepotFile.append("0")
            else:
                currentDepotFile.append(headRev.encode("utf-8")) 
                
 
        if syncedScene:
            if not currentDepotFile:
                Perforce.Add(p4File)
            else:
                Perforce.Sync(currentDepotFile[0])
                Perforce.Edit(currentDepotFile[0])
            return

        if not currentDepotFile:
            path, fileName = os.path.split(p4File)
            if fileName == "peds.meta":
                self.dialogueOpenInfoScreen( "\nMissing depotFile: " + fileName
                                            + "\npath: " + path
                                            + "\n\nEither does not exist, or is not mapped to the current client view."
                                            + "\n\nThere may be no pack specific peds yet for a pack you listed in your 'commandline.txt'."
                                            + "\nThis could be why this error was reported."
                                            + "\nYou should expect this to change once more work has been done on the pack."
                                            + "\nIf the path looks correct, its probably safe to ignore this error." )
            else:
                self.dialogueOpenInfoScreen( "\nMissing depotFile: " + fileName
                                            + "\npath: " + path
                                            + "\n\nEither does not exist, or is not mapped to the current client view." )          
            return False
        else:
            if currentDepotFile[1] != currentDepotFile[2]:
                path, fileName = os.path.split(str(currentDepotFile[0]))
                self.dialogueOpenInfoScreen( "\ndepotFile: " + fileName
                                            + "\npath: " + path
                                            + " - is not latest version.\n(Perforce Version " 
                                            + str(currentDepotFile[1]) + "/" + str(currentDepotFile[2]) + ")"   
                                            + "\n      Grabbed latest version for you.")
                Perforce.Sync(currentDepotFile[0])
            return True


    def plotAnimToObj( self, Obj ):
        Obj.Translation.SetAnimated(True) # make sure these are animatatable
        Obj.Rotation.SetAnimated(True) # make sure these are animatatable
        Obj.Selected = True
        plottoMarker = mobu.FBPlotOptions()
        plottoMarker.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterGimbleKiller
        plottoMarker.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
        plottoMarker.PlotOnFrame = True
        plottoMarker.UseConstantKeyReducer = True
        plottoMarker.ConstantKeyReducerKeepOneKey  = True
        mobu.FBSystem().CurrentTake.PlotTakeOnSelected(plottoMarker)   
        Obj.Selected = False


    def help( self ):
        webbrowser.open('https://hub.rockstargames.com/display/ANIM/Create+or+Edit+Synced+Scenes')


def Run():
    form = MainWindow()
    form.setWindowTitle('Create or Edit Synced Scene')
    form.show()