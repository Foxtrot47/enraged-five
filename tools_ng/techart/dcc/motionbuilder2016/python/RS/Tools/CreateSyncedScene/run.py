from RS.Tools.CreateSyncedScene.Widgets import CreateSyncedScene


def Run( show = True ):
    toolsDialog = CreateSyncedScene.MainWindow()
    
    if show:
        toolsDialog.show()

    return toolsDialog