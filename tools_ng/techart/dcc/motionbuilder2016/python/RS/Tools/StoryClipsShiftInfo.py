"""
Running the script

import RS.Tools.StoryClipsShiftInfo
RS.Tools.StoryClipsShiftInfo.Run()
"""
import pyfbsdk as mobu
from PySide import QtGui, QtCore

from RS import Globals
from RS.Tools import UI


class TrackInfoKeys(object): 
    CLIPS = "Clips"
    CHARACTER_NAME = "characterName"
    
    
class ClipInfoKeys(object):
    START = "start"
    STOP = "stop"
    MARK_START = "markStart"
    MARK_STOP = "markStop"
    SPEED = "speed"


class StoryClipInfo(QtGui.QWidget):
    def __init__(self, parent=None):
        super(StoryClipInfo, self).__init__(parent)
        self.setupUI()

        self.trackInfo = StoryClipInfo.getStoryTrackInfo()

        displayString = self.formatDataToString(self.trackInfo)
        self.mainText.setPlainText(displayString)

    def setupUI(self):
        """
        Builds the simple GUI with the play text to show the results so that the artists can select and copy the data.
        """
        layout = QtGui.QVBoxLayout()

        mainWidget = QtGui.QWidget()

        self.mainText = QtGui.QPlainTextEdit()
        self.mainText.setReadOnly(True)
        self.mainText.setWordWrapMode(QtGui.QTextOption.NoWrap)

        layout.addWidget(self.mainText)

        mainWidget.setLayout(layout)
        self.setLayout(layout)

    @staticmethod
    def getStoryTrackInfo():
        """
        Gets all the clips from the selected Story Track and which character is it controling.

        Returns:
            Dictionary with the info as such:
                {"TrackName":
                    {"characterName": "First namespace of the character"}
                    {"Clips":
                        { "clipName":
                            { "start" : ###,
                            "stop" : ###,
                            "markStart" : ###,
                            "markStop" : ### }
                        }
                    }
                }
        """


        selectedTracks = [comp for comp in Globals.Scene.Components if isinstance(comp, mobu.FBStoryTrack) and
                                                                                comp.Selected is True]
        #Be able to get the track info from the other props and not just characters.
        trackInfo = {}
        for track in selectedTracks:
            trackName = track.LongName
            if track.Character:
                trackName = track.Character.LongName.split(":")[0]
            elif len(track.Components):
                trackName = track.Components[0].GetAffectedObjects()[0].LongName.split(":")[0]

            trackInfo[track.Name] = {TrackInfoKeys.CLIPS: {clip.Name:{ClipInfoKeys.START:clip.Start.GetFrame(),
                                                                     ClipInfoKeys.STOP:clip.Stop.GetFrame(),
                                                                     ClipInfoKeys.MARK_START:clip.MarkIn.GetFrame(),
                                                                     ClipInfoKeys.MARK_STOP:clip.MarkOut.GetFrame(),
                                                                     ClipInfoKeys.SPEED:clip.Speed}
                                                                    for clip in track.Clips},
                                     TrackInfoKeys.CHARACTER_NAME: trackName}

        return trackInfo

    @classmethod
    def formatDataToString(self, storyTrackInfo):
        """
        Gets the clip shift frame data and generates the string that shows info for each clip as such:
        CLIP: clip name \t characterName \t Frames: startFrame-endFrame \t shifted to: markStart-markStop
        Args:
            storyTrackInfo (dict): Dictionary with the information of each track on the selected story.
                                        {"TrackName":
                                            {"characterName": "First namespace of the character"}
                                            {"Clips":
                                                { "clipName":
                                                    {"start" : ###,
                                                    "stop" : ###,
                                                    "markStart" : ###,
                                                    "markStop" : ### }
                                                }
                                            }
                                        }
        """

        txt = []    # Text to display with the result from the selection.
        for track in storyTrackInfo:
            txt.append("Track {} data: \n\n".format(track))

            # Create a dictionary to be able to sort the output by start frame instead of clip name.
            sortClipsByFrame = {}
            sortClipsByFrame = {clipInfo[ClipInfoKeys.START]:(clipName, clipInfo)
                                for clipName, clipInfo in storyTrackInfo[track][TrackInfoKeys.CLIPS].iteritems()}

            # Write the output in the frame order.
            for frame in sorted(sortClipsByFrame.keys()):
                clipName, clipInfo = sortClipsByFrame[frame]
                # outputing only the clips that have the time shift, either the start/end frame have shifted or the speed.
                # When the speed changes, seams like the start/end frame are not updated.
                if clipInfo[ClipInfoKeys.MARK_START] != clipInfo[ClipInfoKeys.START] and\
                    clipInfo[ClipInfoKeys.MARK_STOP] != clipInfo[ClipInfoKeys.STOP] or clipInfo[ClipInfoKeys.SPEED] != 1.0:
                    txt.append(("CLIP: {clipName} \t{char}\t\tFrames: {markStart}-{markStop} "
                            "\tshifted to: {startFrame}-{endFrame}\t\t{speed}".format(clipName=clipName,
                                                                      startFrame=clipInfo[ClipInfoKeys.START],
                                                                      endFrame=clipInfo[ClipInfoKeys.STOP],
                                                                      markStart=clipInfo[ClipInfoKeys.MARK_START],
                                                                      markStop=clipInfo[ClipInfoKeys.MARK_STOP],
                                                                      char=storyTrackInfo[track][TrackInfoKeys.CHARACTER_NAME],
                                                                      speed="x{}".format(clipInfo[ClipInfoKeys.SPEED]) if clipInfo[ClipInfoKeys.SPEED] != 1.0 else "")))

            txt.append("\n")
        return "\n".join(txt)


@UI.Run("Story clip shift info", size=(1024, 768), dialog=True)
def Run(show=None):
    dlg = StoryClipInfo()
    if dlg.trackInfo == {}:
        lbl = QtGui.QLabel("There is no Story tracks selected.")
        lbl.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        return lbl
    return dlg

