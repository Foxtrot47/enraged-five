class PerforceError(Exception):
    """Standard P4 Error"""


class InvalidStream(Exception):
    """The given stream does not exist"""


class AuthenticationError(Exception):
    """Authentication failed"""


class PerforceDisabled(Exception):
    """Perforce is disabled"""


class InvalidClient(Exception):
    """The given workspace doesn't exist"""


class PathNotInPerforce(Exception):
    """Raise when the path does not exist in Perforce."""

    def __init__(self, path, server):
        self.message = "Path is not on '{}': '{}'".format(server, path)


# These are Mobu specific
class P4ConnectionNotAttempted(Exception):
    """Raise when a connection has not yet been attempted."""


class P4PathFormattingError(Exception):
    """Raise when a path has been incorrectly formatted for the specified command."""
