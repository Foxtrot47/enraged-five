"""Helper functions for interfacing with Perforce through our .NET RSG.SourceControl.Perforce library."""
import os
import types
import datetime
import functools
import re
import stat

from RS import Config
from RS.Utils import UserPreferences
from RS._Perforce import serversTemp as servers, constTemp as const, exceptions
from RS.PyCoreQt.Threading import worker


# Classes to mimic the old API
class FileState(object):

    def __init__(self, fileState):
        self._fileState = fileState
        self.Changelist = self._fileState.changelist
        self.ClientFilename = self._fileState.clientFilename
        self.DepotFilename = self._fileState.depotFilename
        self.HeadAction = self._fileState.headAction
        self.HaveRevision = int(self._fileState.haveRevision)
        self.HeadChange = int(self._fileState.headChange)
        self.HeadRevision = int(self._fileState.headRevision)
        self.HeadChangelistTime = self._fileState.headChangelistTime
        self.IsMapped = self._fileState.isMapped
        self.OpenAction = self._fileState.openAction
        self.OtherLocked = self._fileState.otherLocked

    @property
    def IsShelved(self):
        return self._fileState.isShelved

    def getChangelist(self):
        return self.Changelist

    def getClientFilename(self):
        return self.ClientFilename

    def getDepotFilename(self):
        return self.DepotFilename

    def getHaveRevision(self):
        return self.HaveRevision

    def getHeadAction(self):
        return self.HeadAction

    def getHeadRevision(self):
        return self.HeadRevision

    def getHeadChange(self):
        return self.HeadChange

    def getHeadChangelistTime(self):
        return self.HeadChangelistTime

    def getIsMapped(self):
        return self.IsMapped

    def getIsShelved(self):
        return self.IsShelved

    def getOpenAction(self):
        return self.OpenAction

    def getOtherLocked(self):
        return self.OtherLocked


class P4RecordSet(object):

    def __init__(self, fileStates):
        self._fileStates = fileStates

    def __contains__(self, item):
        return item in self._fileStates

    def __iter__(self):
        for item in self._fileStates:
            yield item

    def __getitem__(self, key):
        return self._fileStates[key]

    @property
    def Records(self):
        return self._fileStates


class P4Settings(object):
    """Settings referenced by TechArt tools to modify error handling and running p4 commands."""
    raiseErrors = False
    ignoreErrors = False

    p4EnabledByUser = UserPreferences.__Readini__(
        "menu",
        "Perforce Integration",
        notFoundValue=True,
        default_value=True
    )
    p4EnabledByTools = os.environ.get("PERFORCE_ENABLED", "true").lower() == "true"


def Enabled():
    """Checks global enabled state for perforce operations. All tools should respect this."""
    return P4Settings.p4EnabledByTools and P4Settings.p4EnabledByUser


P4Exception = exceptions.PerforceError
P4PathFormattingError = exceptions.P4PathFormattingError


class EnableExceptions(object):
    """Context manager for raising p4 exceptions"""

    def __init__(self):
        self._state = None

    def __enter__(self):
        """Method called by the with statement"""
        self._state = P4Settings.raiseErrors
        P4Settings.raiseErrors = True

    def __exit__(self, exceptionType, value, traceback):
        """Method called when the with statement ends.
        It also catches error values within the with block

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (str): error string
            traceback (traceback): the full traceback of the error
        """
        P4Settings.raiseErrors = self._state


def SetEnabledUserPreference(enable):
    """Change the user's perforce integration setting and update the client state to match."""
    UserPreferences.__Saveini__("menu", "Perforce Integration", enable)
    P4Settings.p4EnabledByUser = enable

    # Update all connections.
    gameDepot = servers.gameDepot
    mocapDepot = servers.mocapDepot
    if enable is True:
        gameDepot.reset()
        mocapDepot.reset()
    else:
        gameDepot.disposeClient()
        mocapDepot.disposeClient()


def IsLatest(fileStateObj):
    """Checks if version of the file is latest

    Args:
        fileStateObj (FileState): FileState object used to gather P4 data from file

    Returns:
        bool
    """
    return fileStateObj.HaveRevision == fileStateObj.HeadRevision


def IsOpenForEdit(fileStateObj):
    """Checks if file is open for edit

    Args:
        fileStateObj (FileState): FileState object used to gather P4 data from file

    Returns:
        bool
    """
    return fileStateObj.OpenAction == "edit"


def IsOpenForAdd(fileStateObj):
    """Checks if file is open for add

    Args:
        fileStateObj (FileState): FileState object used to gather P4 data from file

    Returns:
        bool
    """
    return fileStateObj.OpenAction == "add"


def CanOpenForEdit(fileStateObj):
    """Checks if file is locked/can be opened for edit

    Args:
        fileStateObj (FileState): FileState object used to gather P4 data from file

    Returns:
        bool
    """
    return not fileStateObj.OtherLocked


def FixFilePath(filename, invert=False):
    """Replaces characters not recognized by P4 with their escapes.

    Args:
        filename (str): path to file
        invert (bool): True or False

    Returns:
        str: The modified file path.
    """
    mappings = (("%", "%25", "%25|%23|%40"), ("@", "%40", "%40"))
    if invert is True:
        mappings = (("%25", "%", "%"), ("%40", "@", "@"))

    for character, escape, regex in mappings:
        if not re.search(regex, filename) and character in filename:
            filename = filename.replace(character, escape)

    # Not sure if this is needed
    return str(r"{}".format(filename))


def FixFilePaths(filenames):
    """Some file paths contain @, we need to convert it %

    Args:
        filenames (list of str): path to files

    Returns:
        list of str
    """
    filenames = filenames or []
    if not isinstance(filenames, (list, tuple)):
        filenames = [filenames]
    return [FixFilePath(filename) for filename in filenames]


def SwitchPathStyle(path):
    """Detect and switch between a Perforce depot path or a Windows filesystem path and vice versa.

    Args:
        path (str): The depot path or windows filesystem path.

    Returns:
        str
    """
    if os.path.splitdrive(path)[0]:
        _, path = os.path.splitdrive(path)[0]
        return "/{}{}".format(["", "depot/"]["gta5" in path.lower()], path.replace("\\", "/"))
    else:
        return r"x:{}".format(path.replace("/", "\\")[1:])


def EnsureFileWritable(filePath):
    """Sets the file to be writable on disk.

    Args:
        filePath (str): Local path to a file on disk
    """
    if os.path.exists(filePath):
        if not os.access(filePath, os.W_OK):
            os.chmod(filePath, stat.S_IWRITE)


# Is this needed?
# _AssertFileState = servers.assertFileState
@property
def _AssertFileState():
    print "_AssertFileState - Bad Import"

def p4DisabledReturnValue(returnValue):
    """Decorator that will return a preset object if P4Settings.p4EnabledByUser is set to False.

    Args:
        returnValue (object): The object to return; passing a list, set, or dict tells the decorator
            to create a new, empty instance of that class.
    """
    mutableTypes = (list, set, dict)

    def callable(func):

        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            if not Enabled():
                if isinstance(returnValue, mutableTypes):
                    # Create a new instance to avoid modifying the return value for the next call.
                    # Believe it or not, this is faster than calling copy.copy().
                    return type(returnValue)()
                return returnValue
            return func(*args, **kwargs)

        return wrapped

    return callable


def DisposeConnection():
    """Destroys the current perforce client instance and cleans up references."""
    servers.gameDepot.P4.disposeClient()


# Keeping this version around to avoid issues; could import from servers instead
def AssertConnectionEnabled(func):
    """
    Decorator used to throw an exception if the wrapped method is called while perforce integration is disabled.
    Use to ensure methods that would potentially cause problems if run while perforce integration is
    disabled cannot be called in that case.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if not Enabled():
            msg = ('Calling {}.{} is not allowed while Perforce integration is disabled.'
                   .format(func.__module__, func.__name__))
            raise P4Exception(msg)
        return func(*args, **kwargs)
    return wrapper


def Reconnect():
    """
    Disconnects and connects the perforce instance. This is to resolve network issues where
    the connection is invalid. This can be called after a long running process to ensure
    the connection did not time out.
    """
    servers.gameDepot.P4.reconnect()


def User():
    """Gets the name of the user using perforce

    Returns:
        str
    """
    return servers.gameDepot.P4.user()


def Client():
    """Gets the name of the client using perforce

    Returns:
        str
    """
    return servers.gameDepot.P4.client()


def Port():
    """Gets the port used by perforce

    Returns:
        str
    """
    return servers.gameDepot.P4.server()


def Host():
    """Gets the host for perforce

    Returns:
        str
    """
    return servers.gameDepot.P4.host()


def CreateConfig(directory, client=None, port=None, user=None):
    """Creates a .p4config file in the supplied directory.

    Args:
        directory (string): The directory to create the .p4config file.
        client (string): The client workspace to set.
        port (string): The server and port to use.  Format would be: 'rsgsanp4p01:1999'
        user (string): The username to use.
    """
    if not os.path.isdir(directory):
        os.makedirs(directory)

    if not port:
        port = Port()
    if not client:
        client = Client()
    if not user:
        user = User()

    try:
        p4config = open('{0}\\.p4config'.format(directory), 'w')
        p4config.write('P4CLIENT={0}\n'.format(client))
        p4config.write('P4PORT={0}\n'.format(port))
        p4config.write('P4USER={0}\n'.format(user))
    except IOError, err:
        print 'Could not create the .p4config file due to the following error!\n', err
    finally:
        p4config.close()


def DoesFileExist(filePath, includeAdd=True):
    """Checks to see if the supplied filePath exists in Perforce.
    Also checks if the file has been marked for add locally

    Args:
        filePath (string): The filePath to check.
        includeAdd (bool): if True, a file in Mark for Add will be considered as existing in p4. If False, it won't.

    Returns:
        True or False
    """
    inValidHeadActionsForFileExistence = [0, 3, 6]  # Unknown/Delete/MoveAndDelete
    fileStateObj = GetFileState(filePath)
    if (fileStateObj is not None) and (fileStateObj.HeadAction not in inValidHeadActionsForFileExistence):
        return True
    if includeAdd and IsOpenForAdd(fileStateObj):
        return True
    return False


def Where(depotPaths):
    """Calls 'p4 where' with the depot paths as the input.

    Notes:
        * All 'illegal' characters are replaced with their escapes prior to passing on to 'p4 where'.
        * All input paths will have any trialing "/" characters removed.

    Args:
        depotPaths (list of str): The input depot paths.

    Returns:
        list of dict: A list of the p4 dict returned by 'p4 where'.
    """
    return servers.gameDepot.P4.where(depotPaths)


def GetLocalPaths(depotPaths):
    """Convenience wrapper around passing depot paths to 'p4 where' to get the local path under the current workspace.

    Works on file and directory paths.

    Args:
        depotPaths (list of str): The input depot paths.

    Returns:
        list of str: The local paths.
    """
    return servers.gameDepot.P4.getLocalPaths(depotPaths)


def GetClientRoot():
    """Get the client's root directory for the current workspace.

    Returns:
        str
    """
    return servers.gameDepot.P4.clientRoot()


def GetFileState(filenames):
    """Runs fstat on the supplied filenames.

    Args:
        filenames (list[string, ect.]): The file names to test.

    Returns:
        FileState object or a list of FileState objects.
    """
    if type(filenames) not in (types.TupleType, types.ListType):
        filenames = [filenames]

    fileStates = [FileState(fs) for fs in servers.gameDepot.P4.fileStates(filenames)]

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def GetFileInfo(localFilePath, revision, key):
    """Gets information about the file from Perforce based on the revision number and key

    Args:
        localFilePath (string): local perforce file path to test
        revision (int): revision you want to check
        key (string): the type of information you wish to return. See the FileInfoTypes enum for
                      acceptable values.

    Returns:
        key result - could be int/string etc.
    """
    p4File = "{}#{}".format(localFilePath, revision)
    data = servers.gameDepot.P4.run("filelog", ["-m", 1, p4File])
    if len(data) == 0:
        raise ValueError("Unable to find '{}'".format(p4File))
    return data[0][key][0]


@p4DisabledReturnValue([])
def GetAllMarkedDeleteDepotFiles(startingDirectory):
    """Gets ALL the files marked for delete within the supplied directory. This includes files on ALL clients.

    Example format for startingDirectory: "//americas/art/dev/animation/resources/...fbx"

    Args:
        startingDirectory (str): directory to inspect

    Returns:
        list[str]:  A list of depot file names.
    """
    args = ["-a", startingDirectory]
    openedFiles = servers.gameDepot.P4.run(const.Commands.OPENED, args)

    fileList = []
    for fileData in openedFiles:
        if fileData["action"] == "delete":
            fileList.append(fileData["depotFile"])

    return fileList


@p4DisabledReturnValue([])
def GetAllMarkedAddFiles(startingDirectory):
    """Gets all the files marked for Add within the supplied directory

    Returns :
        dict{"clientFileName", "depotFileName"}
    """
    # args = ["-a", startingDirectory]
    args = ["{}/*".format(startingDirectory)]
    openedFiles = servers.gameDepot.P4.run(const.Commands.OPENED, args)

    fileObjList = []
    for fileData in openedFiles:
        if fileData["action"] == "add":
            fileObj = {"clientFilePath": fileData["clientFile"], "depotFilePath": fileData["depotFile"]}
            fileObjList.append(fileObj)

    return fileObjList


def Add(filenames, changelistNum=None, force=False, fileType=None):
    """Adds the supplied filenames to Perforce.

    Note:
        This method should NOT escape symbols in the file paths (@, etc).
        The P4 Add command accepts characters normally ignored by the other P4 commands.
        Replacing the characters with their escapes would add a file to perforce with the escapes in
        the filename instead of the intended character.

    Warning:
        If a file has already been added, it will not get added to the changelist and NO error
        will be raised or message printed. This is a little odd considering that the command will
        print a warning message if run in Power Shell.

    Args:
        filenames (string): The filenames to add.
        changelistNum (int): Add files to the specified changelist.
        force (boolean): Indicates that the file should be force added.
        fileType (string): The file type that we want to add the file as.
                           Should be string that matches one of the following list of fle types;
                           http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html

    Returns:
        A single or list of RSG.SourceControl.Perforce.FileState objects that were added.
    """
    if type(filenames) not in (types.TupleType, types.ListType):
        filenames = [filenames]

    fileStates = servers.gameDepot.P4.add(filenames, changelist=changelistNum, force=force, fileType=fileType)

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def AddOrEditFiles(filenames, changelistNum=None, fileType=None, force=False):
    """Adds or Opens for edit the supplied filenames.

    Args:
        filenames (list[string, etc.]): The filenames to open for add or edit.
        changelistNum (int): Added files or Open for edit the files under this changelist.
        fileType: The file type that we want to add the file as. Should be string that matches
                  one of the following list of fle types;
                  http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html
        force (bool): Indicates that the file should be force added.

    Returns:
        A single or list of RS.PyP4.FileState objects that were opened for edit or added.
    """
    vals = []
    filesAdded = Add(filenames, changelistNum=changelistNum, force=force, fileType=fileType)
    filesEdded = Edit(filenames, changelistNum=changelistNum, fileType=fileType)
    for record in [filesAdded, filesEdded]:
        if isinstance(record, list):
            vals.extend(record)
        else:
            vals.append(record)

    return vals


def Edit(filenames, changelistNum=None, fileType=None):
    """Opens for edit the supplied filenames.

    Args:
        filenames (list[string, etc.]): The filenames to open for edit.
        changelistNum (int): Open for edit the files under this changelist.
        fileType: The file type that we want to add the file as. Should be string that matches
                  one of the following list of fle types;
                  http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html

    Returns:
        A single or list of RSG.SourceControl.Perforce.FileState objects that were opened for edit.
    """
    if isinstance(filenames, basestring):
        filenames = [filenames]

    fileStates = servers.gameDepot.P4.edit(filenames, changelist=changelistNum, fileType=fileType)

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def Sync(filenames, force=False, revision=None):
    """Syncs the supplied filename(s).

    Args:
        filenames (list[string, etc.]): The filename(s) to sync.
        force (boolean): Whether or not to force the sync.
        revision (int or list, optional): Revision to sync to; Default: None.

    Returns:
        A single or list of RSG.SourceControl.Perforce.FileState objects that were synced.
    """
    if filenames == "":
        # This should raise, but the old API didnt
        return None
    
    
    fileStates = servers.gameDepot.P4.sync(filenames, force=force, revision=revision)

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def GetFilesInChangelist(changelistNum):
    """Query all files in a changelist.

    Args:
        changelistNum (int): The changelist number to query.

    Returns:
        List of files found in the changelist.
    """
    return servers.gameDepot.P4.describe(changelistNum).get("depotFile", [])


@AssertConnectionEnabled
def GetDescription(changelistNumber):
    """Gets the description from the given changelist. The results are returned in a normal python dictionary

    Args:
        changelistNumber (int): changelist number

    Returns:
        dictionary
    """
    return servers.gameDepot.P4.describe(changelistNumber)


def ConvertRecordSetToDictionary(recordSet):
    """Convert P4RecordSet object to a single dictionary.

    Args:
        recordSet (P4RecordSet): P4RecordSet object.

    Returns:
        dict
    """
    return recordSet.Records[0]


def DeleteChangelist(changelistNum, deleteShelvedFiles=True):
    """Delete a changelist.

    Args:
        changelistNum(int): The changelist number to delete.
        deleteShelvedFiles(boolean): Delete shelved files in the changelist.

    Returns:
        P4API.P4RecordSet object or None.
    """
    return servers.gameDepot.P4.deleteChangelist(changelistNum, deleteShelvedFiles=deleteShelvedFiles)


def DeleteShelvedFiles(changelistNum):
    """Delete all shelved files in a changelist.

    Args:
        changelistNum (int): The changelist number to delete.

    Returns:
        P4API.P4RecordSet object or None.
    """
    return servers.gameDepot.P4.deleteShelve(changelistNum)


@AssertConnectionEnabled
def ShelveChangelist(changelistNum):
    """Shelve all files in a changelist.

    Args:
        changelistNum (int): The changelist number.

    Returns:
        P4API.P4RecordSet object.
    """
    return servers.gameDepot.shelveChangelist(changelistNum)


@AssertConnectionEnabled
def Shelve(filenames, changelistNum=None):
    """Shelve a list of files.

    Args:
        filenames (list[string, etc.]): The filenames to shelve.
        changelistNum (int): The changelist number.

    Returns:
        P4API.P4RecordSet object.
    """
    return servers.gameDepot.P4.shelve(filenames, [], changelistNum=changelistNum)


def Revert(filenames):
    """Revert a list of files.

    Args:
        filenames (list[string, etc.]): The filenames to revert.

    Returns:
        P4API.P4RecordSet object or None.
    """
    return servers.gameDepot.revert(filenames)


def Submit(changelistNum):
    """Submit a changelist.

    Args:
        changelistNum (int): The changelist number to submit.

    Returns:
        P4API.P4RecordSet object or None.
    """
    return servers.gameDepot.submit(changelistNum)


@AssertConnectionEnabled
def Run(p4Cmd, args=None):
    """Native P4 Run command.

    Args:
        p4Cmd (str): valid p4 cmd
        args (list[str, etc.]):  list of paths and/or perforce arguments

    Returns:
        P4 Record or error message
    """
    results = servers.gameDepot.P4.run(p4Cmd, args=args)
    if len(results) == 0:
        return []
    return P4RecordSet(results)


def Files(paths, excludeDeletedFiles=True):
    """Wraps the files perforce command

    Args:
        paths (list of str): list of file paths to pass to the files command
        excludeDeletedFiles (bool): exclude files marked as deleted on depot

    Returns:
        RecordSet or list
    """
    args = []
    if excludeDeletedFiles:
        args = ["-e"]
    args.extend(paths)
    return Run(const.Commands.FILES, args)


@p4DisabledReturnValue([])
@AssertConnectionEnabled
def Dirs(paths):
    """Wraps the dirs perforce command.

    Notes:
        * Tools expect this method to fail silently when they pass in incorrectly formatted paths.

    Args:
        paths (list of str): list of file paths to pass to the dirs command

    Returns:
        RecordSet or list
    """
    # Must replicate P4Connection.dirs without the extra validation in order to maintain api compatibility.
    return P4RecordSet(servers.gameDepot.P4.run(const.Commands.DIRS, paths))


def Changes(paths):
    """Wraps the dirs perforce command

    Args:
        paths (list): list of file paths to pass to the changes command

    Returns:
        RecordSet or list
    """
    return servers.gameDepot.run(const.Commands.CHANGES, paths)


def ConvertPerforceSecondsToDate(seconds):
    """Converts the seconds returned by perforce to a date and time

    Args:
        seconds (int): number of seconds grabbed from the perforce server

    Returns:
        string
    """
    return str(datetime.datetime(1970, 1, 1) + datetime.timedelta(0, int(seconds)))


def Connected():
    """Can a connection to Perforce be made?

    Returns:
        bool: True if a connection can be made; False if not.
    """
    if not Enabled():
        return False

    return servers.gameDepot.isConnectionAttempted()


# TODO Scope to a specific p4 server
class SyncOperation(object):
    """Perforce p4.exe command line"""
    def __init__(self, fileList, start=False, force=False, revisionList=None):
        """Constructor"""

        self.p4root = Config.Project.Path.Root
        self.fileList = FixFilePaths(fileList)
        self.revisionList = revisionList if revisionList and len(revisionList) == len(self.fileList) else None
        self.force = force
        self.task = None

        if start:
            self.Start()

    def Start(self):
        """Sync Operation"""
        if not Enabled():
            return

        # This for loop integrates changes from CL 30145404 that was not included in Geoff's update
        # ToDo: This needs to be changed in order to fix: url:bugstar:7021925 - Reference Editor Not updating Assets
        # ToDo: Sync method has a revision arg that can be utilized instead of adding revisions to the filepath string
        for idx, filepath in enumerate(self.fileList):
            if self.revisionList:
                newFilepath = "{}#{}".format(filepath, self.revisionList[idx])
                self.fileList[idx] = newFilepath

        self.task = worker.Worker.submit(Sync, self.fileList, force=self.force)

    def WaitForCompletion(self):
        """Wait for process to finish"""
        if not self.task:
            return

        self.task.result()


def GetChangelists(changelistType=None, user=None, client=None):
    """List local changelists.

    Returns:

        P4API.P4RecordSet or None.
    """
    return servers.gameDepot.getChangelists(changelistType=changelistType, user=user, client=client)


def GetLocalPendingChangelists():
    """List local, pending changelists.

    Returns:
        P4API.P4RecordSet or None.
    """
    return servers.gameDepot.getLocalPendingChangelists()
