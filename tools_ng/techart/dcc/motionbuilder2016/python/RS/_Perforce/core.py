"""Custom wrapper around the python perforce interface using the python P4 API.

Official Documentation:
https://www.perforce.com/perforce/doc.current/manuals/p4script/03_python.html

The following terms are used interchangeably through out the official documentation and this module:
    client & workspace - client will be used in the code to describe workspaces
    server & port - server will be used in the code to describe ports
"""
import os
import re
import sys
import uuid
import time
import base64
import socket
import datetime
import collections
import getpass

import P4

from RS._Perforce import constTemp as const, exceptions, utils
from RS._Perforce.decorators import path
from PySide import QtGui, QtCore


class PasswordPrompt(QtGui.QDialog):
    """QDialog popup for inputting a password for logging into Perforce"""
    def __init__(self, parent=None):
        super(PasswordPrompt, self).__init__(parent=parent)

        # Set window title and flags
        self.setWindowTitle("Password Entry")
        self.setWindowFlags(QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowStaysOnTopHint)

        # Set instance variable for password to None by default
        self.password = None

        # Create and set the layout for the dialog window
        layout = QtGui.QVBoxLayout()
        self.setLayout(layout)

        # Create label telling user to input their password
        label = QtGui.QLabel("Enter your Perforce login password to connect to the server.")

        # Create line edit for inputting the password and set echo mode to password to hide any input
        self.passwordInput = QtGui.QLineEdit()
        self.passwordInput.setEchoMode(QtGui.QLineEdit.Password)

        # Create OK button to submit the password
        self.okButton = QtGui.QPushButton("OK")
        self.okButton.setDefault(True)

        # Add everything to the layout
        layout.addWidget(label)
        layout.addWidget(self.passwordInput)
        layout.addWidget(self.okButton)

        # Connect function for grabbing the password (and closing the prompt) to the OK button
        self.okButton.clicked.connect(self.getPassword)

    def getPassword(self):
        """Gets the inputted password and closes the prompt window"""
        # The password gets encoded to properly pass to the authentication process
        self.password = base64.b64encode(self.passwordInput.text()) or None
        self.close()


class P4Connect(object):
    """ContextManager that manages connections to the P4 Server.

    If a connection is already active and another command is ran, then a new connection will not be made.
    """

    def __init__(self, p4):
        """Constructor.

        Args:
            p4 (P4.P4): a P4 adapter instance
        """
        self._p4 = p4
        if not self._p4.connected():
            self._p4.connect()

    def __enter__(self):
        """Overrides internal method."""
        if not self._p4.connected():
            self._p4.connect()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._p4.connected():
            self._p4.disconnect()


class RSP4(object):
    """Custom wrapper for the P4 module."""
    Pool = {}
    Queue = {}

    MessagePrompt = None
    PasswordPrompt = None

    _internalDisable = True

    def __init__(self, server, cwd=None):
        """Constructor.

        Args:
            server (str): server to connect to

            cwd(string, optional): path to the directory owned by a perforce client/workspace
        """
        self.p4 = P4.P4()
        self.p4.port = server  # sets server
        self.p4.host = socket.gethostname()  # sets machine name #Modified
        self.p4.user = ""  # this must be set via the account

        self._server = server
        # The P4 adaptor class wont' accept a null value, so we set a null internally to track that a client hasn't
        # been explicitly set by us.
        self._client = None
        self._account = None
        self._settings = None
        self._raiseErrors = True
        self.p4.exception_level = 1

        self._promptUser = False

        # Second value should be a linux default location
        # A client/workspace should be dynamically created if it doesn't exist
        self.setCwd(cwd or os.environ.get("RS_PROJROOT", ""))
        self.setProg('Python TA - "{}"'.format(sys.executable))

        self._isCaseSensitive = False
        self._validateServer = False
        self.connectionAttempts = 1

    @property
    def caseSensitiveUsername(self):
        """Internal method for getting the correct casing of the name to use.

        Returns:
            str
        """
        name = getpass.getuser()  # self.account.name  # Modified
        if self._isCaseSensitive:
            name = name.lower()
        return name

    @property
    def account(self):
        """Account to authenticate against the server.

        Defaults to the current user logged into the machine.

        Returns:
            rockstar.core.accounts.AbstractAccount
        """
        return self._account or getpass.getuser()  # accounts.CurrentUser() # Modified

    @account.setter
    def account(self, value):
        """Sets the account to use for authenticating against the server.

        Args:
            value (rockstar.core.account.AbstractAccount): account to use for authenticating against the server
        """
        self._account = value
        self.p4.user = self.caseSensitiveUsername  # set the name with the correct casing

    @property
    def isCaseSensitive(self):
        """Is the server case sensitive.

        Returns:
            bool
        """
        return self._isCaseSensitive

    @isCaseSensitive.setter
    def isCaseSensitive(self, value):
        """Sets if the server is case sensitive.

        Args:
            value (bool): is the server case sensitive
        """
        self._isCaseSensitive = value
        self.p4.user = self.caseSensitiveUsername

    def rebuild(self):
        """Destroy and rebuild the P4.P4 instance."""
        user = self.caseSensitiveUsername  # Grab the name from the account set
        host = socket.gethostname()  # Get the host based on the current machine #Modified
        port = self._server  # Use the internally stored server
        client = self.client()  # Current workspace set for the connection
        cwd = self.cwd()  # The root directory that the workspace  looks at
        title = self.prog()  # Name of the connection for server side debugging

        del self.p4
        self.p4 = P4.P4()
        self.p4.exception_level = 1

        self.p4.user = user
        self.p4.host = host
        self.p4.port = port

        self.setClient(client)
        self.setCwd(cwd)
        self.setProg(title)

    @property
    def settings(self):
        """Dictionary that holds information about P4 connections.

        Returns:
            dict
        """
        return {}

    def defaultClient(self):
        """The default client/workspace to use for the current server.

        A user may have multiple clients associated with a server. This returns the one that has been associated as the
        default client for that server

        Returns:
            str
        """
        return self.settings.get("defaultClient", None)

    def setDefaultClient(self, client):
        """Sets the default workspace that should be used by this user in the current server.

        Args:
            client (str): workspace to set as the default
        """
        self.settings["defaultClient"] = client

    def setPassword(self, value):
        """Sets the password that P4 will use to login into server.

        Args:
             value (str): base64 encoded password
        """
        try:
            self.p4.password = base64.b64decode(value)
        except:
            raise ValueError("Password is not encoded in base64")

    def authenticate(self, password=None, username=None, connectionAttempts=None):
        """Authenticates the credentials against the server for access.

        Args:
            password (str): password to use for authenticating against perforce
            username (str): name of the user for authenticating against perforce.
                            This argument is unused by this method but required by the inherited class.
            connectionAttempts (int): number of attempts to connect to perforce
        """
        if password:
            self.setPassword(password)
            self.run("login", input=base64.b64decode(password or ""), connectionAttempts=connectionAttempts)
        else:
            # Try to login using the existing p4 ticket
            self.run("login", ["-s"], connectionAttempts=connectionAttempts)
        return True

    def login(self, server=None, connectionAttempts=None):
        """Login into perforce with the ticket.

        Same as P4 login -s

        Args:
            server (string, optional): name of the server to logging to
            connectionAttempts (int, optional): ammount of attempts the p4 object should try to log in.
        """
        if server:
            self.setServer(server)

        try:
            try:
                # Make the first attempt to connect using the p4 ticket
                success = self.authenticate(connectionAttempts=connectionAttempts)
            except exceptions.AuthenticationError:
                # Second attempt asks for the user to input their password for connecting
                prompt = PasswordPrompt()
                prompt.exec_()
                password = prompt.password
                success = self.authenticate(password=password, connectionAttempts=connectionAttempts)
        except exceptions.PerforceError:
            ### subsequent attempts to login use the standard login logic.
            ### success = super(RSP4, self).login(counter=9, connectionAttempts=connectionAttempts)
            # ToDo: Added as fix to - url:bugstar:7383021 - [Pure Python Perforce] Code cleanup/refactor
            # The super login doesn't work so forcing this as False. Leaving Geoff's old code for now to be aware.
            success = False

        if not success:
            raise exceptions.AuthenticationError(
                "The P4 ticket/session for user {} on {} has expired. "
                "Could not log back in with the provided password(s)".format(self.user(), self.server())
            )

        # if valid password was used, store it for future use.
        # We skip resetting the password again as P4 assigns a number to the password field after performing p4 login
        return True

    def logout(self):
        """Clears out the P4 ticket."""
        self.run("logout")

    def exceptionLevel(self):
        """The exception level of the P4 module as to what type of errors should be exposed.

        Returns:
            int
        """
        return self.p4.exception_level

    def setExceptionLevel(self, level):
        """Sets the p4 exception level as to what errors and warnings it should raise.

        Args:
            level (int): the level that determines which errors should be raised
        """
        self.p4.exception_level = level

    # TODO: After the removal of the warnings module, this property no longer works url:bugstar:6336444
    def raiseErrors(self):
        """Are P4 errors being raised.

        Returns:
            bool
        """
        return self._raiseErrors

    def setRaiseErrors(self, value):
        """Sets whether P4 errors are being raised.

        Args:
            value (bool): should errors be raised
        """
        self._raiseErrors = value

    def promptUser(self):
        """Should the user be prompted with a dialog when login fails.

        Returns:
            bool
        """
        return self._promptUser

    def setPromptUser(self, value):
        """Sets if the user should be prompted to input their password.

        Args:
            value (bool): should the user be prompted for password if a login fails with the current password.
        """
        self._promptUser = value

    @classmethod
    def disabled(cls):
        """Is perforce disabled.

        Returns:
            bool
        """
        return os.environ.get("PERFORCE_DISABLED", False) and cls._internalDisable

    @classmethod
    def setDisabled(cls, value):
        """Set if perforce should be disabled.

        Args:
            value (bool): should perforce be disabled.
        """
        cls._internalDisabled = value

    def createConfig(self, directory, client=None, server=None, user=None):
        """Creates a P4 Config file

        Args:
            directory (string): path of the directory

            client (string, optional): the perforce client/workspace to set
            server (string, optional): the perforce server/port to set
            user (string, optional): the user to set
        """
        if not os.path.isdir(directory):
            os.makedirs(directory)

        try:
            p4config = open("{0}\\.p4config".format(directory), "w")
            p4config.write("P4CLIENT = {0}\n".format(client or self.client()))
            p4config.write("P4PORT = {0}\n".format(server or self.server()))
            p4config.write("P4USER = {0}\n".format(user or self.user()))
        except IOError, err:
            print("Could not create the .p4config file due to the following error!\n", err)

        finally:
            p4config.close()

    def connect(self):
        """Establish a connection with the P4 Server

        Note:
            A workspace isn't required for establishing a connection but is needed for running some commands

        Returns:
            rockstar.core.perforce.core.P4Connect
        """
        if not self.isConnected():
            self.p4.port = self._server  # sets server
            self.p4.host = socket.gethostname()  # sets machine name # Modified
            self.p4.user = self.caseSensitiveUsername  # sets the user to connect as

        return P4Connect(self.p4)

    def disconnect(self):
        """Break a connection with the P4 Server.

        Returns:
            bool
        """
        if not self.disabled() and self.isConnected():
            return self.p4.disconnect()

    def reconnect(self):
        """Disconnect the current connection with the P4 Server and start a new one."""
        self.disconnect()
        self.connect()

    def isConnected(self):
        """Is there a connection with the P4 Server.

        Returns:
            bool
        """
        if self.disabled():
            return False
        return self.p4.connected()

    def canConnect(self):
        """Attempts to connect to the server and returns if it succeeded.

        Returns:
            bool
        """
        if self.disabled():
            return False
        elif self.isConnected():
            return True

        connected = False
        try:
            self.connect()
            connected = self.isConnected()
            self.disconnect()
        except exceptions.PerforceError:
            pass

        return connected

    def apiLevel(self):
        """P4 Api version.

        Returns:
            str
        """
        while self.isConnected():
            return self.p4.api_level

    def user(self):
        """The current P4 username.

        Notes:
            This value is determined by the account set for this connection

        Returns:
            str
        """
        return self.p4.user

    def client(self):
        """The current client/workspace.

        Returns:
            str
        """
        return self.p4.client

    def setClient(self, value):
        """Sets the current client/workspace to use for this connection.

        Args:
            value (str): name of a client/workspace to use.
        """
        self.p4.client = value
        self._client = value

    def isClientSet(self):
        """Has a client been set properly.

        Returns:
            bool
        """
        if self._client:
            return self._client == self.p4.client
        return False

    def server(self):
        """The current port number being used.

        Notes:
            This is declared at the initialization of the class

        Returns:
            str
        """
        return self.p4.port

    def setServer(self, value):
        """Sets the current server/port to use for this connection.

        Args:
            value (str): the server/port number to use.
        """
        self.p4.port = value
        self._server = value

    def host(self):
        """The current host.

        Notes:
            This is determined by the current machine name.

        Returns:
            str
        """
        return self.p4.host

    def attrs(self):
        """Perforce attributes.

        Returns:
            list of str
        """
        attrs = []

        info = self.run("info")
        for key in info[0]:
            attrs.append("{0} = {1}".format(key, info[0][key]))

        return attrs

    def streams(self):
        """number of available streams.

        Returns:
            int
        """
        return self.run("streams")

    def caseFolding(self):
        """Please see P4 docs.

        Returns:
            int
        """
        return self.p4.case_folding

    def charSet(self):
        """Please see P4 docs.

        Returns:
            str
        """
        return self.p4.charset

    def cwd(self):
        """The current root directory for the client/workspace.

        Returns:
            str
        """
        return self.p4.cwd

    def setCwd(self, value):
        """Sets the current root directory for the client/workspace.

        Args:
            value (str): path to the directory on disk.
        """
        # cwd rejects unicode
        self.p4.cwd = str(value)

    def handler(self):
        """An output handler for messages, errors and warnings."""
        return self.p4.handler

    def setHandler(self, value):
        """Sets the output handler for messages, errors and warnings.

        Args:
            value (object): an object that can receive str data, such as a I/O object.
        """
        self.p4.handler = value

    def maxLockTime(self):
        """Limit the amount of time (in milliseconds) spent during data scans to prevent the server from locking tables for too long.

        Returns:
            int
        """
        return self.p4.maxlocktime

    def maxResults(self):
        """Limit the number of results Perforce permits for subsequent commands.

        Returns:
            int
        """
        return self.p4.maxresults

    def maxScanRows(self):
        """Limit the number of database records Perforce scans for subsequent commands.

        Returns:
            int
        """
        return self.p4.maxscanrows

    def logger(self):
        """The logger used by the P4 instance.

        Returns:
            logging.logger
        """
        return self.p4.logger

    def serverLevel(self):
        """Returns the current Perforce server level.

        Each iteration of the Perforce server is given a level number.
        As part of the initial communication this value is passed between the client application and the Perforce server.
        This value is used to determine the communication that the Perforce server will understand.
        All subsequent requests can therefore be tailored to meet the requirements of this server level.

        Returns:
            int
        """
        return self.p4.server_level

    def ticketFile(self):
        """Contains the location of the P4TICKETS file.

        Returns:
            str
        """
        return self.p4.ticket_file

    def version(self):
        """The current version of the P4 server.

        Returns:
            str
        """
        return self.p4.version

    def input(self):
        """Input to process when using a command that accepts data that can't be processed by its flags.

        Notes:
            Once a command is ran that processes this data, this value is emptied and set to None.

        Returns:
            dict
        """
        return self.p4.input

    def setInput(self, value):
        """
        Set the data to process by a command that accepts input data.

        Args:
            value (dict): data to process
        """
        self.p4.input = value

    def warnings(self):
        """The last warnings raised by a perforce command.

        Returns:
            list
        """
        return self.p4.warnings

    def errors(self):
        """The last errors raised by a perforce command.

        Returns:
            list
        """
        return self.p4.errors

    def clientSpec(self):
        """The specs of the current client/workspace.

        Returns:
            dict
        """
        return self.run(const.Commands.CLIENT, ("-o",)).pop(0)

    def clientRoot(self):
        """Get p4v's workspace root.

        Notes:
            * The client root is not the same thing as the p4 current working directory (self.p4.cwd).

        Returns:
            str: The workspace root as noted in p4v.
        """
        clientSpec = self.clientSpec()
        return clientSpec.get("Root", "")

    def prog(self):
        """The name of the program invoking P4 calls.

        Returns:
            str
        """
        return self.p4.prog

    def setProg(self, value):
        """Sets the name of the progam invoking perforce commands.

        Args:
            value (str): name of the program being invoked.
        """
        self.p4.prog = value

    def env(self):
        """Environments on Perforce."""
        return self.p4.env

    def clients(self, unloaded=False, filters=None):
        """This will return the p4 hosts client name. There is a possibility a machine can have multiple clients so will
        return a list.

        Warning:
            If unloaded=True, we cannot do the hostname check since the "Host" key:value pair is not in the p4 dict. It
            is possible that this will return clients/workspaces from other machines that start with the same hostname.
            E.g. nycu-vega, nycu-vega2 - "nycu-vega" will return clients/workspaces for "nycu-vega" and "nycu-vega2"

        Keyword Args:
            unloaded (bool): If True, add the -U flag to only return unloaded clients.
            filters (list of str): filter to use for limiting the output of the clients command.
                                   By default this is the hostname.
         Return:
            List of dict
        """
        results = {}
        hostname = socket.gethostname()
        filters = filters or [hostname]

        for filt in filters:
            parameters = []
            hostnameRegex = re.compile("", re.I)  # regex search that will always match

            if unloaded:
                parameters.append("-U")

            if filt is not None:
                parameters.extend(["-e", "*{}*".format(filt)])
                hostnameRegex = re.compile(filt, re.I)

            clients = self.run(const.Commands.CLIENTS, parameters)

            for client in clients:
                hostnameMatch = hostnameRegex.search(client["client"])
                if (unloaded and hostnameMatch) or (hostnameMatch and client.get("Host", None) == hostname):
                    accessed = results.setdefault(client["Access"], [])
                    if client not in accessed:
                        accessed.append(client)

        return [client for key in sorted(results, key=int) for client in results[key]]

    @staticmethod
    def toDateTime(p4TimeValue):
        """Converts the P4 Int time to an Datetime.

        Args:
            p4TimeValue (int): int value representing time from P4

        Return:
            datetime.datetime
        """
        return datetime.datetime.utcfromtimestamp(int(p4TimeValue))

    @staticmethod
    def toHours(seconds):
        """converts seconds to hours.

        Args:
            seconds (int): seconds to convert to hours

        Returns:
            str
        """
        return str(datetime.timedelta(seconds=int(seconds)))

    def reset(self):
        """creates a new perforce instance with the values of the old instance."""
        self.disconnect()
        p4 = P4.P4()
        p4.port = self.server()
        p4.client = self.client()
        p4.user = self.user()
        p4.host = self.host()
        self.p4 = p4

    def run(self, cmd, args=None, silent=False, input=None, connectionAttempts=None, suppressPasswordError=False):
        """Runs perforce command line.

        Args:
            cmd (str): Perforce command to run

            args (list, optional): List of arguments that perforce command accepts.
            silent (bool, optional): Only valid for commands that have the input & output command. It suppresses P4 from
                              opening a text editor when running commands.
            input (dict, optional): Directory for expanding or editing on the output received from the command being ran
                                before it is executed by perforce.
            connectionAttempts (int): Number of attempts to re-try connecting to Perforce.
            suppressPasswordError (bool): should the password error be suppressed

        Returns:
            list of dict
        """
        args = args or []
        if self.disabled():
            raise exceptions.PerforceDisabled(
                "The '{}' command was not executed as perforce has been disabled".format(cmd)
            )

        # Queues up command to avoid running perforce commands at the same time in multiple threads
        connectionID = uuid.uuid1()
        queue = self.Queue.setdefault(self.p4, [])
        queue.append(connectionID)

        while True:
            if queue[0] == connectionID:
                break

        with self.connect():
            result = []
            try:
                if input is not None:
                    self.p4.input = input
                if not silent:
                    result = self.p4.run(cmd, *args)
                else:
                    result = self.p4.run(cmd, "-o", *args)[0]
                    result.update(input or {})
                    self.p4.input = result
                    self.p4.run(cmd, "-i")

            except Exception as exception:

                passwordFailure = (
                    "Perforce password (P4PASSWD) invalid or unset" in exception.value
                    or "Your session has expired" in exception.value
                )
                passwordFailure = passwordFailure and not suppressPasswordError
                queue.remove(connectionID)
                message = str(exception)
                if (not self.p4.connected() or passwordFailure) and connectionAttempts != 0:
                    if connectionAttempts is None:
                        connectionAttempts = self.connectionAttempts
                    if passwordFailure:
                        self.login(connectionAttempts=connectionAttempts - 1)
                    result = self.run(cmd, args, silent, input, connectionAttempts - 1)
                    if not result:
                        raise exceptions.PerforceError(message)
                elif "Authentication failed" in message:
                    raise exceptions.AuthenticationError(message)
                else:
                    raise exceptions.PerforceError(message)
            finally:
                if connectionID in queue:
                    queue.remove(connectionID)
            return result

    def add(self, filenames, changelist=None, force=False, fileType=None):
        """Adds a files to a changelist.

        Args:
            filenames (list): list of filenames to open for add

            changelist (int, optional): changelist number to add files to
            force (boolean, optional): force the file to be added
            fileType (string, optional): add the files as the specified file type

        Returns:
            list of dict: dictionaries of files that have been successfully opened for add
        """
        if not isinstance(filenames, (tuple, list)):
            filenames = [filenames]

        args = list(filenames)

        if fileType:
            args[0:0] = ["-t", fileType]

        if force:
            args[0:0] = ["-f"]

        if changelist:
            args[0:0] = ["-c", str(changelist)]

        return [result for result in self.run(const.Commands.ADD, args) if isinstance(result, dict)]

    @path.cleanPaths
    def edit(self, filenames, changelist=None, fileType=None):
        """Opens the files for edit.

        Args:
            filenames (list): list of filenames to open for edit

            changelist (int, optional): changelist number to add files to
            fileType (str, optional): add the files as the specified filetype

        Returns:
            list of dict: dictionaries of files that have been successfully opened for edit
        """
        arguments = []
        if not isinstance(filenames, (tuple, list)):
            filenames = [filenames]

        if fileType:
            arguments[0:0] = ["-t", fileType]

        if changelist is not None:
            arguments.extend(["-c", str(changelist)])
        results = []
        for filename in filenames:
            # For the edit command, all arguments must be before the filenames
            results.extend(
                [result for result in self.run(const.Commands.EDIT, arguments + [filename]) if isinstance(result, dict)]
            )
        return results

    # TODO: Move to the server module
    @path.cleanPaths
    def editFolder(self, folder, changelist=None, fileType=None):
        """Opens all the files in folder for edit

        Args:
            folder (str): folder path

            changelist (int, optional): changelist number to add files to
            fileType (str, optional): add the files as the specified file type

        Returns:
            list of dict
        """
        folder = utils.convertToDirectory(folder)
        return self.run(const.Commands.EDIT, folder, changelist=changelist, fileType=fileType)

    def getRevisionForFiles(self, changelist):
        """Gets each CL file revision using the one submitted in the CL.

        Args:
            changelist (int): the changelist from where the files are fetched.

        Returns:
            list of dict
        """
        args = ["-f", "@{0},@{0}".format(changelist)]
        return self.run(const.Commands.SYNC, args)

    @path.cleanPaths
    def delete(self, filenames, changelist=None, keepFiles=False, preview=False, removeUnsyncedFiles=False):
        """Marks files for delete.

        Args:
            filenames (list): list of filenames to mark for deletion

            changelist (int, optional): changelist number to add deleted files to
            keepFiles (bool, optional): keep files on disk but still mark for delete
            preview (bool, optional): returns the expected results of the perforce operation without performing it
            removeUnsyncedFiles (bool, optional): if a file is missing on disk due to not being synced, then it is marke for delete.
                                        When using this arg, the files should be passed in using their depot paths.
        Returns:
            list of dict: dictionaries of files that have been successfully marked for deletion
        """
        if not isinstance(filenames, (tuple, list)):
            filenames = [filenames]

        args = list(filenames)

        if keepFiles:
            args[0:0] = ["-k"]

        if preview:
            args[0:0] = ["-n"]

        if removeUnsyncedFiles:
            args[0:0] = ["-v"]

        if changelist:
            args[0:0] = ["-c", str(changelist)]

        return [result for result in self.run(const.Commands.DELETE, args) if isinstance(result, dict)]

    @path.cleanPaths
    def integrate(self, fromToFilesDict, changelist=None):
        """Integrates files from a path to another using fromToFilesDict.
        Integrate command docs: https://www.perforce.com/manuals/v18.1/cmdref/Content/CmdRef/p4_integrate.html

        Args:
            fromToFilesDict (dict<string, string>): dictionary of fromFile toFile path pairs.

            changelist (int, optional): changelist number to add deleted files to

        Returns:
            list of dict
        """
        args = []

        if changelist:
            args = ["-c", changelist]

        results = []
        for filePath, newFilePath in fromToFilesDict.iteritems():
            results.extend(self.run(const.Commands.INTEGRATE, args + [filePath, newFilePath]))
        return results

    def move(self, filePathsDict, changelist=None, force=False):
        """Moves/renames files from their location to newLocation

        Args:
            filenamesDict (dict): dictionary of <oldFilename : newFilename> key, value pairs.

            changelist (int, optional): changelist number to add moved files to
            force (bool): force the operation

        Returns:
            list of dict
        """
        args = []

        if changelist:
            args = ["-c", changelist]
        if force:
            args.append("-f")

        results = []
        for filePath, newFilePath in filePathsDict.iteritems():
            # We use + instead of append or extend to create a new list in place
            results.extend(self.run(const.Commands.MOVE, args + [filePath, newFilePath]))
        return results

    def sync(
        self, filenames=None, force=False, revision=None, preview=False, parallel=False, threads=6, syncSubFolders=False
    ):
        """Syncs the list of files to the latest revision unless otherwise specified.

        Args:
            filenames (collections.Iterable, optional): List of filenames to open for sync, i.e. a list, tuple, set, etc. of strings; Default: None.
            force (bool, optional): Force sync files; Default: False.
            revision (int or list, optional): Revision to sync to; Default: None.
            preview (bool, optional): Returns the expected results of the perforce operation without performing it; Default: False.
            parallel (bool, optional): runs sync in parallel if set on True.
            threads (int, optional): number of threads used in parallel sync.
            syncSubFolders (bool, optional): if true it will append "..." to the folder path and sync everything in that folder and subfolders.

        Returns:
            list of dict
        """
        if filenames is None:
            filenames = []
        elif not isinstance(filenames, collections.Iterable) or isinstance(filenames, basestring):
            filenames = [filenames]

        results = []
        processedFileNames = []
        args = []

        for idx, filename in enumerate(filenames):
            if revision:
                if isinstance(revision, int):
                    filename = "{}#{}".format(filename, revision)
                elif isinstance(revision, list):
                    filename = "{}#{}".format(filename, revision[idx])
            if syncSubFolders:
                filename = "{}...".format(filename)
            processedFileNames.append(filename)

            if force:
                args = ["-f"]
            else:
                args = ["-s", "-q"]
            if preview:
                args[0:0] = ["-n"]
            if parallel:
                args.append("--parallel=threads={}".format(threads))

        if processedFileNames:
            try:
                results.extend(self.run("sync", args + [processedFileNames]))

            except exceptions.PerforceError as error:
                errorMessage = str(error)
                if "can't clobber writable file" in errorMessage.lower():
                    path = errorMessage.split("Can't clobber writable file")[-1].strip()
                    results.extend([{"error": errorMessage, "path": path}])
                if "cannot create a file" in errorMessage.lower():
                    results.extend([{"error": errorMessage, "path": filename}])
                else:
                    print(errorMessage)
        return results

    def shelve(
        self, pathsToAddEdit, pathsToRemove, changelist=None, description=None, client=None, match=False, sync=False
    ):
        """Shelves the given files.

        If no changelist is given a new one will be created.

        If a description is provided and match is set to True then if a changelist with a matching description already exists,
        it will be used instead of creating a new one.

        Descriptions with # in them will only match the part of the description before the hashtag/pound sign

        Args:
            pathsToAddEdit (list): list of files to shelve and need marked as add/edit.
            pathsToRemove (list): list of files to shelve and need marked as remove.

            changelist (int, optional): existing changelist number to use
            description (string, optional): description for the changelist
            client (string, optional): client to find existing changelists in
            match (boolean, optional): find existing changelists
            sync (boolean, optional): update the files to their latest revision without changing their content

        Return:
            str(number of the changelist)
        """
        if match and changelist is None and description is not None:
            changelists = self.run(const.Commands.CHANGES, ["-l", "-c", client or self.client(), "-s", "pending"])
            for changelist in changelists:
                # Remove everything after the commonly used "#review" portion of a comment.
                if description.lower().strip().startswith(changelist["desc"].lower().strip().split("#")[0]):
                    changelist = changelist.get("change")
                    self.deleteShelve(changelist)
                    break
                changelist = None

        if changelist is None:
            changelist = self.changelist(description or "Automatically Generated Shelf By Tech Art")

        if sync:
            # Have P4 update the local files to latest revision without changing their content
            self.run(const.Commands.SYNC, ["-k"] + pathsToAddEdit + pathsToRemove)

        # Move the files to the changelist
        self.moveToChangelist(changelist, pathsToAddEdit, pathsToRemove)

        if pathsToAddEdit or pathsToRemove:
            if pathsToAddEdit:
                self.run(const.Commands.EDIT, ["-k", "-c", changelist, pathsToAddEdit])
            self.run(const.Commands.SHELVE, ["-c", changelist])

        return changelist

    def unshelve(self, shelvedChangelist, changelist=None, filenames=None, force=False, stream=None):
        """Unshelves files from a changelist.

        Args:
            shelvedChangelist (int): changelist to unshelve files from

            changelist (int, optional): changelist to add unshelved files to, defaults to the changelist where the files are being unshelved from
            filenames (list, optional): list of files from the shelve changelist to unshelve. By default all files are unshelved.
            force (bool, optional): force unshelving of files even if they already exist
            stream (str, optional): stream to unshelve files to.

        Returns:
            list of dict
        """
        args = ["-s", shelvedChangelist]

        if force:
            args[0:0] = ["-f"]

        if changelist:
            args.extend(["-c", changelist])

        if stream:
            args.extend(["-S", stream])

        if filenames:
            args.extend(filenames)

        return self.run(const.Commands.UNSHELVE, args)

    def deleteShelve(self, changelist):
        """Deletes the shelf attached to a changelist

        Args:
            changelist (string): changelist whose shelf should be deleted

        Return:
            bool
        """
        try:
            # Attempt to delete the shelve of the changelist
            # Errors out when there are no shelved files or the changelist doesn't exist
            self.run(const.Commands.SHELVE, ["-d", "-c", changelist])
            return True
        except:
            return False

    def submit(self, changelist):
        """submits files.

        Args:
            changelist (int): changelist to submit to the server

        Returns:
            list of dict
        """
        return self.run(const.Commands.SUBMIT, ["-c", changelist])

    def where(self, path):
        """Show where a particular file is located, as determined by the client view.

        Args:
            path (str): The depot or filesystem path.

        Returns:
            list of dict
        """
        return self.run(const.Commands.WHERE, (path,))

    def files(self, path, excludeDeletedFiles=True):
        """Gets all the files that live in the given path on perforce.

        Args:
            path (str): path to check for files

            excludeDeletedFiles (bool, optional): do not include files that have been deleted from perforce
        """
        args = [path]
        if excludeDeletedFiles:
            args = ["-e", path]

        files = self.run(const.Commands.FILES, args)
        return files

    # TODO: Move to server class
    def filesInFolderTree(self, path, excludeDeletedFiles=True):
        """Gets all the files that live in the given path on perforce and also in subfolders tree.

        Args:
            path (str): path to check for files

            excludeDeletedFiles (bool, optional): do not include files that have been deleted from perforce

        Returns:
            list of dict
        """
        path = utils.convertToDirectory(path)
        return self.files(path, excludeDeletedFiles=excludeDeletedFiles)

    def describe(self, changelist, force=False, original=False, short=False, shelved=False):
        """Get a list of the files that are shelved in the changelist no matter if they will be successfully unshelved or not.

        Args:
            changelist (int): number of the changelist which information we need.

            force (bool, optional): Force the display od description for restricted changelists. (requires Admin permission)
            original (bool, optional): If a changelist was renumbered on submit, and you know only the original changelist number
            short (bool, optional): Display a shortened output that excludes the files' diffs
            shelved (bool, optional): Display files shelved for the specified changelist, including diffs of those files against
                            their previous depot revision.

        Returns:
            list of dict
        """
        args = []
        if force:
            args.append("-f")
        if original:
            args.extend("-O")
        if short:
            args.append("-s")
        if shelved:
            args.append("-S")
        args.append(changelist)

        return self.run(const.Commands.DESCRIBE, args)

    def dirs(self, path):
        """Gets all the directories that live in the given path on perforce

        Args:
            path (str): path to check for directories

        Returns:
            list of dict
        """
        return self.run(const.Commands.DIRS, [path])

    def revert(
        self,
        filenames,
        changelist=None,
        unchanged=False,
        delete=False,
    ):
        """Revert files

        Args:
            filenames (list): list of filenames to revert

            changelist (int, optional): changelist with files to revert
            unchanged (bool, optional): only revert unchanged files
            delete (bool, optional): delete files locally that are marked for add

        Returns:
            list of dict
        """
        args = []

        if unchanged:
            args.append("-a")
        if delete:
            args.append("-w")
        if changelist:
            args.extend(["-c", str(changelist)])

        if not isinstance(filenames, (tuple, list)):
            filenames = [filenames]
        args.extend([utils.removeSpecialCharacters(filename) for filename in filenames])

        return self.run(const.Commands.REVERT, args)

    def revertUnchanged(self, changelist=None):
        """Reverts unchanged files in the list

        Args:
            changelist (int, optional): changelist to revert unchanged files in
        """
        args = ["-a"]
        if changelist:
            args.extend(["-c", str(changelist)])
        return self.run(const.Commands.REVERT, args)

    def reconcile(self, files, changelist):
        """Reconcile an list of files/folder and add/edit to an changelist.

        Args:
            files (string): files to add/edit to a changelist
            changelist (int): number of the changelist

        Returns:
            list of dict
        """
        return self.run(const.Commands.RECONCILE, ["-f", "-a", "-e", "-d", "-c", changelist, files])

    def changelist(self, description, files=None):
        """Creates a new changelist with the given description.

        Args:
            description (string): description for the new changelist

            files (list, optional): list of file paths to add to the changelist

        Return:
            int
        """
        self.run(const.Commands.CHANGE, silent=True, input={"Description": description, "Files": files or []})
        changelistNumber = self.run(const.Commands.CHANGES, ["-c", self.client(), "-s", "pending", "-m", 1])[0][
            "change"
        ]
        return int(changelistNumber)

    def deleteChangelist(self, changelist, revertFiles=False):
        """Deletes the given changelist.
        To delete a changelist with files in it, set revertFiles to True.

        Args:
            changelist (int): changelist to delete

            revertFiles (bool, optional): reverts files in the CL before deleting it, if there are.

        Returns:
            list of dict
        """
        if revertFiles:
            self.run(const.Commands.REVERT, ["-a", "-c", changelist])

        self.run(const.Commands.CHANGE, ["-d", changelist])

    def changelists(self, status=None, user=None, client=None):
        """List changelists.

        Args:
            status(str, optional):  Options: "pending", "submitted", "shelved".
            user(str, optional): user who owns the changelists
            client(str, optional): the workspace that has the changelists to query

        Returns:
            list of dict
        """
        if user is None:
            user = self.user()
        if client is None:
            client = self.client()

        args = ["-u", user, "-c", client, "-l"]
        if status is not None:
            args.extend(["-s", status])

        return self.run(const.Commands.CHANGELISTS, args)

    # TODO: Move to server as it is a more complex operation
    def moveToChangelist(self, changelist, pathsToAddEdit=None, pathsToRemove=None):
        """Moves the given file paths to be opened for add, edit or delete in the given changelist

        Args:
            changelist (int): changelist to move files to

            pathsToAddEdit (list, optional): list of file paths to open for add or edit in the given changelist
            pathsToRemove (list, optional): list of file paths to open for delete in the given changelist
        """
        commandArgs = ["-c", changelist]
        pathsToAddEdit = pathsToAddEdit or []
        pathsToRemove = pathsToRemove or []

        if pathsToAddEdit:
            addEditCommandArgs = commandArgs + pathsToAddEdit
            self.run(const.Commands.ADD, addEditCommandArgs)
            self.run(const.Commands.EDIT, addEditCommandArgs)
            self.run(const.Commands.REOPEN, addEditCommandArgs)

        if pathsToRemove:
            self.run(const.Commands.REVERT, pathsToRemove)
            self.run(const.Commands.DELETE, commandArgs + pathsToRemove)

    def changePassword(self, user, oldPassword, newPassword):
        """Change the password of a user.

        Args:
            user (string): perforce username
            oldPassword (string): old password
            newPassword (string): new password
        """
        if not user:
            user = user()

        self.p4.user = user
        self.run(const.Commands.PASSWD, ("-O", oldPassword, "-P", newPassword))

    def fetch(self, cmd, value=None):
        """Gets data without syncing files.

        Args:
            cmd (string): command to run

            value (str): addtional filters for the call

        Returns:
            list of dict
        """
        args = ["-o"]
        if value is not None:
            args.append(value)
        return self.run(cmd, args)[0]

    def fetchChange(self, changeNum=None):
        return self.fetch(const.Commands.CHANGE, changeNum)

    def fetchLabel(self, labelName):
        return self.fetch(const.Commands.LABEL, labelName)

    def fetchClient(self, clientName):
        return self.fetch(const.Commands.CLIENT, clientName)

    # TODO: deprecated, use perforce.utils.convertPath
    def convertPath(self, path):
        """Converts depot path to a local path & vice versa

        Args:
            path (string): path to convert

        Returns:
            str
        """
        return utils.convertPath(path)

    @path.cleanPaths
    def fileState(self, path, flags=None, filter=None):
        """Returns the result of the filestat with default values for missing information

        Args:
            filepath (string): path to the file to get perforce information from

            flags (list, optional): list of str parameters to request through the fstat command.
            filter (str, optional): filter to apply, e.g. '^headAction = delete' (everything but what's deleted on head rev.)

        Returns:
            rockstar.core.perforce.core.FileState
        """
        fileState = None

        args = []
        if flags is not None:
            args.append("-T")
            args.append(", ".join(flags))

        if filter is not None:
            args.append("-F")
            args.append(filter)
        args.append(path)

        try:
            newResult = self.run(const.Commands.FSTAT, args)
            if newResult != []:
                fileState = newResult[0]
        except exceptions.PerforceError as error:
            print((str(error)))
        return FileState(fileState)

    @path.cleanPaths
    def fileStates(self, paths, flags=None, filter=None):
        """Returns the result of the filestat for multiple files.

        Args:
            paths (list): list of paths to the files to get perforce information from

            flags (list, optional): list of str parameters to request through the fstat command.
            filter (str, optional): filter to apply, e.g. '^headAction = delete' (everything but what's deleted on head rev.)

        Return:
            list
        """
        args = []
        if flags is not None:
            args.append("-T")
            args.append(", ".join(flags))
        if filter is not None:
            args.append("-F")
            args.append(filter)
        args.append(paths)

        fileStates = []
        for fileState in self.run(const.Commands.FSTAT, args):
            fileStates.append(FileState(fileState))
        return fileStates

    # TODO: Move to server class
    @path.cleanPaths
    def folderFileStates(self, folderPath, flags=None, filter=None, iterative=False):
        """Returns the filestates for all the files in a folder.

        Args:
            folderPath (str): a path to the folder.

            flags (list, optional): list of str parameters to request through the fstat command.
            filter (str, optional): filter to apply, e.g. '^headAction = delete' (everything but what's deleted on head rev.)
            iterative (bool, optional): determines if the fstat command should look for all the subfolders too.

        Return:
            list of FileStates
        """
        folderPath = utils.convertToDirectory(folderPath, iterative=iterative)
        return self.fileStates([folderPath], flags=flags, filter=filter)

    def switchDirectory(self, cwd):
        """Switch the current directory being used by the client.

        Args:
            cwd (string): path to the directory to switch to
        """
        self.setCwd(cwd)

    def switchServer(self, server, name=None, excludeStreams=False, defaultClient=None):
        """Switches P4 Servers.

        Args:
            server (string): port name

            name (string, optional): name of the client/workspace to switch to
            excludeStreams (boolean, optional): exclude clients/workspaces that are associated with a stream
            defaultClient (str, optional): default client to switch to

        Returns:
            bool
        """
        # Run the switch logic the first time to ensure that the P4 connection is set properly and pointing to a
        # valid client workspace
        if self.server() == server and self._validateServer:
            return False

        currentServer = self.server()
        self.setServer(server)
        client = self.getLastAccessedClient(name, excludeStreams)
        if not client:
            self.setServer(currentServer)
            return False

        self.setClient(str(client))
        return True

    def getLastAccessedClient(self, names=None, excludeStreams=True, create=False):
        """Gets the last accessed client.

        If there is no last accessed client, it will attempt to get the default client.

        Args:
            names (list, optional): list of valid names to look for.
            excludeStreams (bool, optional): exclude stream clients
            create (bool, optional): create the client if it can't be found.
                            This will only create the first client from the names list

        Returns:
            str or None
        """
        lastAccessedClient = None
        accessOrder = {}
        names = names or []
        filters = list(names)
        host = socket.gethostname()

        # Add the default client to the list of workspace names to search for
        defaultClient = self.defaultClient()
        if defaultClient and defaultClient not in names:
            names.append(defaultClient)
            filters.append(defaultClient)

        if host not in filters:
            filters.append(host)

        # Go through the currently loaded workspaces to verify the client is currently loaded
        # The list of workspaces are returned in the order of most recently used.
        for client in self.clients(unloaded=False, filters=filters):
            if excludeStreams and client.get("Stream", None):
                continue
            name = client["client"]
            accessOrder.setdefault(self.toDateTime(client["Access"]), []).append(name)
            if name in names:
                lastAccessedClient = name
                break

        # Go through the unloaded clients to see if the clients we are looking for are currently unloaded
        if len(accessOrder) == 0:
            unloadedClients = self.clients(unloaded=True, filters=filters)
            if unloadedClients:
                for unloadedClient in reversed(unloadedClients):
                    if unloadedClient["Owner"] == self.user():
                        name = unloadedClient["client"]
                        if self.promptUser() and not self._reloadClientPrompt(name):
                            return None
                        # reload client to very
                        reloadedClient = self.reloadClient(name)
                        # Make sure the client is valid for the host and then add to access order.
                        if reloadedClient["Host"] == host:
                            accessOrder.setdefault(self.toDateTime(reloadedClient["Access"]), []).append(name)
                            continue

            if len(accessOrder) == 0:
                if not create:
                    # Exit/Raise Error as no valid workspaces have been found
                    server = self.server()
                    warning = (
                        "Unable to find a client/workspace mapping for server: (Server {server}).\n"
                        "Please correct this by reloading an unloaded client/workspace that belongs to this machine"
                        "or create a new client/workspace.\n"
                        "The client/workspace should start with the host name ({host})"
                    ).format(server=server, host=socket.gethostname())  # Modified
                    raise exceptions.InvalidClient(warning)

                # Create the workspace, the mappings still need to be set outside this function
                lastAccessedClient = names[0]
                self.run(const.Commands.CLIENT, [lastAccessedClient], silent=True)

        if len(accessOrder) >= 1 and lastAccessedClient is None:
            # Get the last accessed client
            escape = False
            for accessIndex, key in enumerate(reversed(sorted(accessOrder.keys()))):
                for clientIndex, client in enumerate(accessOrder[key]):
                    if not accessIndex and not clientIndex:
                        # store the last accessed client incase we can't find the default client
                        lastAccessedClient = client

                    if client in names:
                        # Use the default client as we found a matching workspace
                        lastAccessedClient = client
                        break
                if escape:
                    break

        # Store the last accessed client as the default client if one is not set
        if lastAccessedClient and not defaultClient:
            self.setDefaultClient(lastAccessedClient)
        return lastAccessedClient

    def _reloadClientPrompt(self, client):
        """Prompts the user to select if they want to reload the workspace or not

        Args:
            client (string): name of the workspace

        Returns:
            bool
        """
        qApplication = QtGui.QApplication.instance()
        if self.promptUser() and qApplication:

            messageBox = QtGui.QMessageBox()
            messageBox.setWindowTitle("Perforce Warning")
            messageBox.setText(
                "A valid workspace is required to perform P4 operations.\n"
                "A potential unloaded workspace '{}' has been found.\n"
                "Should this workspace be reloaded and used?\n".format(client)
            )
            noButton = messageBox.addButton("No", QtGui.QMessageBox.NoRole)

            # On Linux, the accept button is always set last so we use a different role to make them appear in a consistent order between platforms
           # if config.user.system.isLinux():
            #    messageBox.addButton("Yes (Recommended)", QtGui.QMessageBox.ApplyRole)
            # else:
            messageBox.addButton("Yes (Recommended)", QtGui.QMessageBox.YesRole)

            # try:
            #     # Attempt to set our custom P4 icon if it is available
            #     from rockstar.coreQt import const
            #
            #     icon = const.Icons.perforce()
            #     messageBox.setWindowIcon(icon)
            #     messageBox.setIconPixmap(icon.pixmap(50, 50))
            # except:
            #     messageBox.setIcon(QtGui.QMessageBox.Warning)

            messageBox.exec_()
            if messageBox.clickedButton() == noButton:
                QtGui.QMessageBox.critical(
                    None,
                    "Perforce Error",
                    "Please contact Tech Art to help set up your P4 environment.\n"
                    "Perforce commands for server {} wil not work until a workspace is setup.".format(self.server()),
                )
                return False
        return True

    def reloadClient(self, name, streamName=None):
        """Runs the "p4 reload" command on the specified client.

        Warning:
            Will error if the current user is not the owner.

        Args:
            name (str): The client name as listed in the P4 client dict.

            streamName (str, optional): The name of the stream to query for the specified client.

        Returns:
            dict: p4 client dict
        """
        # Reload the inactive client
        self.run(const.Commands.RELOAD, ("-c", name))

        # Poll p4 until the unloaded client is available again
        counter = 5
        while counter:
            if streamName:
                clients = self.streamClients(streamName)
            else:
                clients = self.clients()
            clients_ = [client_ for client_ in clients if client_.get("Host") == socket.gethostname()]
            if len(clients_):
                break
            counter -= 1
            time.sleep(1)

        print(("Reloaded client: {!r}".format(name)))

        return clients_[0]

    def unloadClient(self, name):
        """Unloads the given workspace/client

        Args:
            name (str): name of the workspace/client

        Returns:
            list of dict
        """
        return self.run(const.Commands.UNLOAD, ("-c", name))

    # TODO: replace with a raises instead of returning None url:bugstar:6204381
    def switchStreams(self, streamName, rootPath, clients=None, sync=True):
        """Switches between streams.

        This also changes the current client/workspace.

        Args:
            streamName(string): name of the stream to switch to

            rootPath(string, optional): the root path for the stream
            clients (list, optional): list of clients associated with that stream
            sync (bool, optional): sync

        Returns:
            None (if a valid client could not be loaded) or dict (a loaded p4 client dict)
        """
        if clients is None:
            clients = self.streamClients(streamName) + self.streamClients(streamName, unloaded=True)
        client = None
        newClient = False

        currentHost = socket.gethostname()
        unloadableClients = []
        for client_ in clients:
            client = client_["client"]
            clientHost = client_.get("Host", None)
            clientOwner = client_.get("Owner")
            isLoaded = client_.get("IsUnloaded", "0") == "0"

            if isLoaded and clientHost == currentHost:
                rootPath = client_["Root"]
                break
            elif not isLoaded and clientOwner == self.user():
                reloadedClient = self.reloadClient(client, streamName=streamName)
                rootPath = reloadedClient["Root"]
                break
            elif not isLoaded:
                unloadableClients.append(client_)
            client = None

        if client is None and not unloadableClients:
            newClient = True
            # Create new client for that stream
            client = "{}_{}".format(socket.gethostname(), streamName)
            stream = self.stream(streamName)
            streamPath = stream["Stream"]
            path = streamPath
            if stream.get("Type", None) == "virtual":
                path = stream["baseParent"]
        elif client is None:
            raise exceptions.InvalidClient("Could not load valid client:\n{}".format(self.server))

        # The directory must be set first so the correct "root" is set when creating a new client
        self.setCwd(rootPath)

        if newClient is True:
            # create client when it doesn't exist
            self.run(const.Commands.CLIENT, ("-S", streamPath, client), silent=True)

        self.setClient(client)

        if sync:
            self.sync(os.path.join(rootPath, "..."))

        return client

    def streamClients(self, streamName, unloaded=False):
        """The clients associated with a stream.

        Args:
            streamName(string): name of the stream to switch to

            unloaded(bool, optional): return unloaded clients

        Returns:
            dict: A loaded or unloaded p4 client dict (depending on the value of the unloaded param)
        """
        stream = self.stream(streamName)

        if stream is None:
            raise exceptions.InvalidStream("Stream {} does not exist".format(streamName))

        streamPath = stream["Stream"]
        parameters = ["-E", "{}_*".format(self.host()), "-S", streamPath]
        if unloaded:
            parameters.append("-U")
        return self.run(const.Commands.CLIENTS, parameters)

    def stream(self, name):
        """Get a stream by its name.

        Args:
            name (string): name of the stream

        Returns:
            dict
        """
        for stream_ in self.streams():
            if stream_.get("Name", None) == name:
                return stream_


class FileState(object):
    """Wraps around the P4 fstat results and returns its objects in a more accessible manner.

    For Reference:
        http://www.perforce.com/perforce/doc.current/manuals/cmdref/fstat.html
    """

    ACTIONS = const.ActionTypes.ACTIONS

    def __init__(self, fileState):
        """Constructor.

        Args:
            fileState (dictionary): results from the fstat perforce command
        """
        fileState = fileState or {}
        self.clientFilename = fileState.get("clientFile", "")
        self.depotFilename = fileState.get("depotFile", "")
        self.isMapped = "isMapped" in fileState
        self.isShelved = "shelved" in fileState
        self.isResolved = "resolved" in fileState
        self.isUnResolved = "unresolved" in fileState

        self.headActionString = fileState.get("headAction", "unknown")
        self.headAction = self.ACTIONS.index(self.headActionString)
        self.headChange = fileState.get("headChange", "")
        self.headRevision = fileState.get("headRev", 0)
        self.headChangelistTime = fileState.get("headTime", "")
        self.headModifiedTime = fileState.get("headModTime", "")
        self.headFileType = fileState.get("headType", "")

        self.haveAction = fileState.get("action", "")
        self.haveRevision = fileState.get("haveRev", 0)
        self.openActionString = fileState.get("action", "unknown")
        self.openAction = self.ACTIONS.index(self.openActionString)
        self.openFileType = fileState.get("type", "")
        self.changelist = fileState.get("change", 0)
        self.otherOpen = "otherOpen" in fileState
        self.otherLocked = "otherLocked" in fileState or ("+l" in self.headActionString and self.OtherOpen)
        self.otherOpenUsers = fileState.get("otherLock", []) or fileState.get("otherOpen", [])

        self.locked = "ourLock" in fileState
        self.isLatestRevision = (
            self.haveRevision == self.headRevision and self.headRevision != 0
        ) or "add" in self.haveAction
        self.isLocal = "add" in self.haveAction
        self.isMarkedForMoveAdd = self.haveAction == "move/add"
        self.exists = bool(fileState and self.headActionString != "delete")
        self.isDeleted = self.headActionString == "delete"
