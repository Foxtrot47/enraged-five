"""
Automated tests for the servers.py module.
"""
import unittest
import socket
import os

from RS._Perforce import servers, const
from RS import Config, Globals


class P4EnabledBase(unittest.TestCase):
    """
    Base class to set up/tear down P4 settings with P4 enabled.
    """
    raiseErrors = None
    suspendMessageBoxes = None
    enabledUserPreference = None

    @classmethod
    def setUpClass(cls):
        super(P4EnabledBase, cls).setUpClass()
        # Store existing settings.
        cls.raiseErrors = servers.P4Settings.raiseErrors
        cls.suspendMessageBoxes = Globals.System.SuspendMessageBoxes
        cls.enabledUserPreference = servers.Enabled()
        # Suppress gui pop-ups.
        servers.P4Settings.raiseErrors = True
        Globals.System.SuspendMessageBoxes = True
        # Enable P4.
        servers.SetEnabledUserPreference(True)

    @classmethod
    def tearDownClass(cls):
        super(P4EnabledBase, cls).tearDownClass()
        # Restore settings.
        servers.P4Settings.raiseErrors = cls.raiseErrors
        Globals.System.SuspendMessageBoxes = cls.suspendMessageBoxes
        servers.SetEnabledUserPreference(cls.enabledUserPreference)


class P4DisabledBase(unittest.TestCase):
    """
    Base class to set up/tear down P4 settings with P4 disabled.
    """
    raiseErrors = None
    suspendMessageBoxes = None
    enabledUserPreference = None

    @classmethod
    def setUpClass(cls):
        # Store existing settings.
        cls.raiseErrors = servers.P4Settings.raiseErrors
        cls.suspendMessageBoxes = Globals.System.SuspendMessageBoxes
        cls.enabledUserPreference = servers.Enabled()
        # Suppress gui pop-ups.
        servers.P4Settings.raiseErrors = True
        Globals.System.SuspendMessageBoxes = True
        # Disable P4.
        servers.SetEnabledUserPreference(False)

    @classmethod
    def tearDownClass(cls):
        # Restore settings.
        servers.P4Settings.raiseErrors = cls.raiseErrors
        Globals.System.SuspendMessageBoxes = cls.suspendMessageBoxes
        servers.SetEnabledUserPreference(cls.enabledUserPreference)


class TestSimpleP4Enabled(P4EnabledBase):
    """
    MVP tests to make sure the very basics are working.
    """
    def test_01_P4IsPopulated(self):
        self.assertIsNone(servers.gameDepot._P4)
        self.assertIsNone(servers.mocapDepot._P4)

    def test_02_user(self):
        with servers.gameDepot as con:
            self.assertEqual(con.user(), Config.User.Name)
        with servers.mocapDepot as con:
            self.assertEqual(con.user(), Config.User.Name)

    def test_02_client(self):
        with servers.gameDepot as con:
            self.assertIsInstance(con.client(), basestring)
            self.assertIsNot(con.client(), "")
        with servers.mocapDepot as con:
            self.assertIsInstance(con.client(), basestring)
            self.assertIsNot(con.client(), "")

    def test_02_port(self):
        with servers.gameDepot as con:
            self.assertEqual(con.port(), Config.User.P4ServerDefault)
        with servers.mocapDepot as con:
            self.assertEqual(con.port(), Config.User.P4ServerMocap)

    def test_02_host(self):
        with servers.gameDepot as con:
            self.assertEqual(con.host(), socket.gethostname())
        with servers.mocapDepot as con:
            self.assertEqual(con.host(), socket.gethostname())

    def test_03_isConnectionAttempted(self):
        self.assertEqual(servers.gameDepot.isConnectionAttempted(), False)
        with servers.gameDepot as con:
            self.assertTrue(con.isConnectionAttempted())

        self.assertEqual(servers.mocapDepot.isConnectionAttempted(), False)
        with servers.mocapDepot as con:
            self.assertTrue(con.isConnectionAttempted())

    def test_04_run(self):
        # Uses the clientRoot method as a quick check that run is working.
        for depot in [servers.gameDepot, servers.mocapDepot]:
            self.assertRaises(servers.P4ConnectionNotAttempted, callable=depot.clientRoot)

        for depot in [servers.gameDepot, servers.mocapDepot]:
            with depot as con:
                self.assertTrue(os.path.isdir(con.clientRoot()))


class TestSimpleP4Disabled(P4DisabledBase):
    """
    Re-run all tests with p4 disabled.
    """
    def test_01_P4IsPopulated(self):
        self.assertIsNone(servers.gameDepot._P4)
        self.assertIsNone(servers.mocapDepot._P4)

    def test_02_user(self):
        with servers.gameDepot as con:
            self.assertRaises(servers.P4Exception, callable=con.user)
        with servers.mocapDepot as con:
            self.assertRaises(servers.P4Exception, callable=con.user)

    def test_02_client(self):
        with servers.gameDepot as con:
            self.assertRaises(servers.P4Exception, callable=con.client)
        with servers.mocapDepot as con:
            self.assertRaises(servers.P4Exception, callable=con.client)

    def test_02_port(self):
        with servers.gameDepot as con:
            self.assertRaises(servers.P4Exception, callable=con.port)
        with servers.mocapDepot as con:
            self.assertRaises(servers.P4Exception, callable=con.port)

    def test_02_host(self):
        with servers.gameDepot as con:
            self.assertRaises(servers.P4Exception, callable=con.host)
        with servers.mocapDepot as con:
            self.assertRaises(servers.P4Exception, callable=con.host)

    def test_03_isConnectionAttempted(self):
        self.assertFalse(servers.gameDepot.isConnectionAttempted())
        self.assertFalse(servers.mocapDepot.isConnectionAttempted())

        with servers.gameDepot as con:
            self.assertFalse(con.isConnectionAttempted())
        with servers.mocapDepot as con:
            self.assertFalse(con.isConnectionAttempted())

    def test_04_run(self):
        # uses the clientRoot method as a quick check that run is working.
        for depot in [servers.gameDepot, servers.mocapDepot]:
            self.assertRaises(servers.P4ConnectionNotAttempted, callable=depot.clientRoot)

        for depot in [servers.gameDepot, servers.mocapDepot]:
            with depot as con:  # It should hit the assertP4Enabled check first.
                self.assertRaises(servers.P4Exception, callable=con.clientRoot)


class TestAdvancedP4Enabled(P4EnabledBase):
    """
    Test the methods that should only be used in rarer use cases.

    Notes on P4Connection methods without explicit tests:
        * reset: this gets called in servers.SetEnabledUserPreference; if this is broken few
            or none of the tests will work correctly and you'll know it straight away.
        * disposeClient: this gets called in servers.P4Connection.reset, so same story there.
    """
    def test_nestedContextManagers(self):
        for depot in [servers.gameDepot, servers.mocapDepot]:
            with depot as con:
                self.assertTrue(os.path.isdir(con.clientRoot()))
                with depot as con:
                    self.assertTrue(os.path.isdir(con.clientRoot()))
                    with depot as con:
                        self.assertTrue(os.path.isdir(con.clientRoot()))


class TestAdvancedP4Disabled(P4DisabledBase):
    """
    Test the methods that should only be used in rarer use cases.

    Notes on P4Connection methods without explicit tests:
        * reset: this gets called in servers.SetEnabledUserPreference; if this is broken few
            or none of the tests will work correctly and you'll know it straight away.
        * disposeClient: this gets called in servers.P4Connection.reset, so same story there.
    """
    def test_nestedContextManagers(self):
        for depot in [servers.gameDepot, servers.mocapDepot]:
            with depot as con:
                self.assertIsNone(con.clientRoot())
                with depot as con:
                    self.assertIsNone(con.clientRoot())
                    with depot as con:
                        self.assertIsNone(con.clientRoot())

    def test_p4DisabledReturnValue(self):
        # Make user p4DisabledReturnValue is returning a new instance of mutable datatypes to
        #  prevent users from modifying the value for the next call.
        with servers.gameDepot as con:
            result = con.where(["//fake/path.txt"])
            result.append("test")
        with servers.gameDepot as con:
            newResult = con.where(["//fake/path.txt"])
        self.assertEqual(newResult, [])


class TestRunMethodsP4Enabled(P4EnabledBase):
    """
    Tests all the P4Connection methods that call run or FileState under the hood and do not
    add, edit, or remove a file or changelist.

    This test case assumes TestBasicsP4Enabled is working correctly as it needs those methods to
    generate the expected test data.
    """
    # TODO this isnt standardized yet
    GAMEDEPOT_DEPOT_PATH = "//wildwest/data/stub.txt"
    GAMEDEPOT_DEPOT_PATHS = [
        "//wildwest/data/automatedTests/motionbuilder/perforce/subdir1/empty.txt",
        "//wildwest/data/automatedTests/motionbuilder/perforce/subdir2/empty.txt",
    ]
    GAMEDEPOT_FILESYSTEM_PATH = u'X:\\wildwest\\data\\stub.txt'
    GAMEDEPOT_DEPOT_DIR = "//wildwest/data/automatedTests/motionbuilder/perforce/*"
    GAMEDEPOT_FILESYSTEM_DIR = "X:\\wildwest\\data\\automatedTests\\motionbuilder\\perforce\\*"
    MOCAPDEPOT_DEPOT_PATH = "//depot/projects/global_assets/placeholder.txt"
    MOCAPDEPOT_FILESYSTEM_PATH = u"X:\\GC\\projects\\global_assets\\placeholder.txt"

    def test_clients(self):
        for depot in [servers.gameDepot, servers.mocapDepot]:
            with depot as con:
                clients = con.clients()
                self.assertIsInstance(clients, list)
                self.assertTrue(len(clients) > 0)

    def test_userHostClients(self):
        for depot in [servers.gameDepot, servers.mocapDepot]:
            with depot as con:
                clients = con.userHostClients()
                self.assertIsInstance(clients, list)
                self.assertTrue(len(clients) > 0)

    def test_clientRoot(self):
        for depot in [servers.gameDepot, servers.mocapDepot]:
            with depot as con:
                self.assertTrue(os.path.isdir(con.clientRoot()))

    def test_where(self):
        # Technically this tests several other methods in order to be able to test P4Connection.where:
        #     servers.ConversionsMixin.convertP4RecordSetToDict
        #     P4Connection.client
        with servers.gameDepot as con:
            result = con.where([self.GAMEDEPOT_DEPOT_PATH])
            resultAsDict = servers.ConversionsMixin.convertP4RecordSetToDict(result)
            # at this point, client() should work correctly.
            client = con.client()
            expectedResult = {
                u'clientFile': u'//{}/wildwest/data/stub.txt'.format(client),
                u'depotFile': u'//wildwest/data/stub.txt',
                u'path': u'X:\\wildwest\\data\\stub.txt'
            }
            self.assertEqual(resultAsDict, expectedResult)
        with servers.mocapDepot as con:
            result = con.where([self.MOCAPDEPOT_DEPOT_PATH])
            resultAsDict = servers.ConversionsMixin.convertP4RecordSetToDict(result)
            # at this point in testing, we know client() and clientRoot() should work correctly.
            client = con.client()
            expectedResult = {
                u'clientFile': u'//{}/projects/global_assets/placeholder.txt'.format(client),
                u'depotFile': u'//depot/projects/global_assets/placeholder.txt',
                u'path': u'{}\\projects\\global_assets\\placeholder.txt'.format(con.clientRoot())
            }
            self.assertEqual(resultAsDict, expectedResult)

    def test_fileState(self):
        with servers.gameDepot as con:
            actualResult = con.fileState(self.GAMEDEPOT_DEPOT_PATH)
            self.assertEqual(actualResult.ClientFilename, self.GAMEDEPOT_FILESYSTEM_PATH)

        with servers.mocapDepot as con:
            actualResult = con.fileState(self.MOCAPDEPOT_DEPOT_PATH)
            self.assertEqual(actualResult.ClientFilename, self.MOCAPDEPOT_FILESYSTEM_PATH)

    def test_fileStates(self):
        with servers.gameDepot as con:
            actualResult = con.fileStates(self.GAMEDEPOT_DEPOT_PATHS)
            self.assertIsInstance(actualResult, list)
            self.assertEqual(actualResult[0].DepotFilename, self.GAMEDEPOT_DEPOT_PATHS[0])

    def test_doesFileExist(self):
        with servers.gameDepot as con:
            actualResult = con.doesFileExist(self.GAMEDEPOT_DEPOT_PATH)
            self.assertTrue(actualResult)

        with servers.mocapDepot as con:
            actualResult = con.doesFileExist(self.MOCAPDEPOT_DEPOT_PATH)
            self.assertTrue(actualResult)

    def test_getLocalPaths(self):
        with servers.gameDepot as con:
            actualResult = con.getLocalPaths([self.GAMEDEPOT_DEPOT_PATH])
            self.assertEqual(actualResult, [self.GAMEDEPOT_FILESYSTEM_PATH])

        with servers.mocapDepot as con:
            actualResult = con.getLocalPaths([self.MOCAPDEPOT_DEPOT_PATH])
            self.assertEqual(actualResult, [self.MOCAPDEPOT_FILESYSTEM_PATH])

    def test_getFileInfo(self):
        with servers.gameDepot as con:
            actualResult = con.getFileInfo(self.GAMEDEPOT_FILESYSTEM_PATH, 1, const.FileInfoTypes.DESC)
            expectedResult = u'Integration change.\nBranch current Wildwest to new wildwest Depot\n'
            self.assertEqual(actualResult, expectedResult)

    def test_sync(self):
        with servers.gameDepot as con:
            # Force a FileState obj to be returned later by first making sure the file is at rev 0.
            con.sync([self.GAMEDEPOT_DEPOT_PATH], force=True, revision=0)
            self.assertFalse(os.path.exists(self.GAMEDEPOT_FILESYSTEM_PATH))

            actualResult = con.sync([self.GAMEDEPOT_DEPOT_PATH])
            self.assertIsInstance(actualResult, list)
            fileStateObj = actualResult[0]
            self.assertIsInstance(fileStateObj, servers.FileState)
            self.assertTrue(os.path.exists(self.GAMEDEPOT_FILESYSTEM_PATH))

        with servers.mocapDepot as con:
            # Force a FileState obj to be returned later by first making sure the file is at rev 0.
            con.sync([self.MOCAPDEPOT_DEPOT_PATH], force=True, revision=0)
            self.assertFalse(os.path.exists(self.MOCAPDEPOT_FILESYSTEM_PATH))

            actualResult = con.sync([self.MOCAPDEPOT_DEPOT_PATH])
            self.assertIsInstance(actualResult, list)
            fileStateObj = actualResult[0]
            self.assertIsInstance(fileStateObj, servers.FileState)
            self.assertTrue(os.path.exists(self.MOCAPDEPOT_FILESYSTEM_PATH))

    def test_files(self):
        with servers.gameDepot as con:
            result = con.files([self.GAMEDEPOT_DEPOT_PATH])
            self.assertEqual(result.__class__.__name__, "P4RecordSet")
            p4RecordSetAsList = servers.ConversionsMixin.convertP4RecordSetToList(result)
            self.assertTrue(len(p4RecordSetAsList) == 1)
            self.assertEqual(p4RecordSetAsList[0].get("depotFile"), self.GAMEDEPOT_DEPOT_PATH)

        with servers.mocapDepot as con:
            result = con.files([self.MOCAPDEPOT_DEPOT_PATH])
            self.assertEqual(result.__class__.__name__, "P4RecordSet")
            p4RecordSetAsList = servers.ConversionsMixin.convertP4RecordSetToList(result)
            self.assertTrue(len(p4RecordSetAsList) == 1)
            self.assertEqual(p4RecordSetAsList[0].get("depotFile"), self.MOCAPDEPOT_DEPOT_PATH)

    def test_dirs(self):
        with servers.gameDepot as con:
            expectedP4RecordList = [
                {u'dir': u'//wildwest/data/automatedTests/motionbuilder/perforce/subdir1'},
                {u'dir': u'//wildwest/data/automatedTests/motionbuilder/perforce/subdir2'}
            ]

            result = con.dirs([self.GAMEDEPOT_DEPOT_DIR])
            actualP4RecordList = servers.ConversionsMixin.convertP4RecordSetToList(result)
            self.assertEqual(actualP4RecordList, expectedP4RecordList)

            result = con.dirs([self.GAMEDEPOT_FILESYSTEM_DIR])
            actualP4RecordList = servers.ConversionsMixin.convertP4RecordSetToList(result)
            self.assertEqual(actualP4RecordList, expectedP4RecordList)

            self.assertRaises(servers.P4PathFormattingError, con.dirs, [self.GAMEDEPOT_DEPOT_DIR[:-2]])
            self.assertRaises(servers.P4PathFormattingError, con.dirs, [self.GAMEDEPOT_DEPOT_DIR[:-1]])
            self.assertRaises(servers.P4PathFormattingError, con.dirs, [self.GAMEDEPOT_FILESYSTEM_DIR[:-2]])
            self.assertRaises(servers.P4PathFormattingError, con.dirs, [self.GAMEDEPOT_FILESYSTEM_DIR[:-1]])
            self.assertRaises(servers.P4Exception, con.dirs, [self.GAMEDEPOT_DEPOT_DIR.replace("/*", "/.../*")])

    def test_getUserData(self):
        keys = (u'Update', u'Access', u'User', u'FullName', u'Type', u'Email',)
        with servers.gameDepot as con:
            actualResult = con.getUserData(con.user())
            self.assertEqual(sorted(actualResult.keys()), sorted(keys))
            self.assertEqual(actualResult.get("User"), con.user())

        with servers.mocapDepot as con:
            actualResult = con.getUserData(con.user())
            self.assertEqual(sorted(actualResult.keys()), sorted(keys))
            self.assertEqual(actualResult.get("User"), con.user())


class TestRunMethodsP4Disabled(P4DisabledBase):
    """
    Tests all the P4Connection methods that call run or FileState under the hood and do not
    add, edit, or remove a file or changelist.

    This test case assumes TestBasicsP4Enabled is working correctly as it needs those methods to
    generate the expected test data.
    """
    GAMEDEPOT_DEPOT_PATH = "//wildwest/data/stub.txt"
    GAMEDEPOT_FILESYSTEM_PATH = u'X:\\wildwest\\data\\stub.txt'
    GAMEDEPOT_DEPOT_DIR = "//wildwest/data/automatedTests/motionbuilder/perforce/*"
    MOCAPDEPOT_DEPOT_PATH = "//depot/projects/global_assets/placeholder.txt"
    MOCAPDEPOT_FILESYSTEM_PATH = u"X:\\GC\\projects\\global_assets\\placeholder.txt"

    def test_clients(self):
        self.assertRaises(servers.P4ConnectionNotAttempted, servers.gameDepot.clients)

        with servers.gameDepot as con:
            self.assertRaises(servers.P4Exception, con.clients)

    def test_userHostClients(self):
        actualResult = servers.gameDepot.userHostClients()
        self.assertEqual(actualResult, [])

        with servers.gameDepot as con:
            actualResult = con.userHostClients()
            self.assertEqual(actualResult, [])

    def test_clientRoot(self):
        actualResult = servers.gameDepot.clientRoot()
        self.assertEqual(actualResult, None)

        with servers.gameDepot as con:
            actualResult = con.clientRoot()
            self.assertEqual(actualResult, None)

    def test_where(self):
        # Technically this tests several other methods in order to be able to test P4Connection.where:
        #     servers.ConversionsMixin.convertP4RecordSetToDict
        #     P4Connection.client
        with servers.gameDepot as con:
            actualResult = con.where([self.GAMEDEPOT_DEPOT_PATH])
            self.assertEqual(actualResult, [])

    def test_fileState(self):
        actualResult = servers.gameDepot.fileState(self.GAMEDEPOT_DEPOT_PATH)
        self.assertEqual(actualResult, None)

        with servers.gameDepot as con:
            actualResult = con.fileState(self.GAMEDEPOT_DEPOT_PATH)
            self.assertEqual(actualResult, None)

    def test_fileStates(self):
        actualResult = servers.gameDepot.fileStates(self.GAMEDEPOT_DEPOT_PATH)
        self.assertEqual(actualResult, [])

        with servers.gameDepot as con:
            actualResult = con.fileStates(self.GAMEDEPOT_DEPOT_PATH)
            self.assertEqual(actualResult, [])

    def test_doesFileExist(self):
        with servers.gameDepot as con:
            self.assertRaises(servers.P4Exception, con.doesFileExist, self.GAMEDEPOT_DEPOT_PATH)

    def test_getLocalPaths(self):
        with servers.gameDepot as con:
            actualResult = con.getLocalPaths([self.GAMEDEPOT_DEPOT_PATH])
            self.assertEqual(actualResult, [])

    def test_getFileInfo(self):
        with servers.gameDepot as con:
            actualResult = con.getFileInfo(self.GAMEDEPOT_FILESYSTEM_PATH, 1, const.FileInfoTypes.DESC)
            expectedResult = None
            self.assertEqual(actualResult, expectedResult)

    def test_sync(self):
        with servers.gameDepot as con:
            actualResult = con.sync([self.GAMEDEPOT_DEPOT_PATH])
            self.assertEqual(actualResult, [])

    def test_files(self):
        with servers.gameDepot as con:
            actualResult = con.files([self.GAMEDEPOT_DEPOT_PATH])
            self.assertEqual(actualResult, [])

    def test_dirs(self):
        with servers.gameDepot as con:
            actualResult = con.dirs([self.GAMEDEPOT_DEPOT_DIR])
            self.assertEqual(actualResult, [])

            actualResult = con.dirs([self.GAMEDEPOT_DEPOT_DIR[:-2]])
            self.assertEqual(actualResult, [])

    def test_getUserData(self):
        self.assertRaises(servers.P4ConnectionNotAttempted, servers.gameDepot.getUserData, "fakeUserName")

        with servers.gameDepot as con:
            self.assertRaises(servers.P4Exception, con.getUserData, "fakeUserName")

# TODO finish these up - manually testing for now via dev_servers.py
# class TestFileOperationsP4Enabled(P4EnabledBase):
#     """
#     Tests all the P4Connection methods that add, edit, or remove a file or changelist.
#
#     This test case assumes TestBasicsP4Enabled is working correctly as it needs those methods to
#     generate the expected test data.
#     """
#     def test_getAllMarkedDeleteDepotFiles(self):
#         self.fail()
#
#     def test_getAllMarkedAddFiles(self):
#         self.fail()
#
#     def test_add(self):
#         self.fail()
#
#     def test_edit(self):
#         self.fail()
#
#     def test_addOrEditFiles(self):
#         self.fail()
#
#     def test_getChangelist(self):
#         self.fail()
#
#     def test_createChangelist(self):
#         self.fail()
#
#     def test_getFilesInChangelist(self):
#         self.fail()
#
#     def test_getDescription(self):
#         self.fail()
#
#     def test_deleteChangelist(self):
#         self.fail()
#
#     def test_revertChangelist(self):
#         self.fail()
#
#     def test_deleteShelvedFiles(self):
#         self.fail()
#
#     def test_shelveChangelist(self):
#         self.fail()
#
#     def test_shelve(self):
#         self.fail()
#
#     def test_revert(self):
#         self.fail()
#
#     def test_submit(self):
#         self.fail()
#
#     def test_changes(self):
#         self.fail()
#
#     def test_getChangelists(self):
#         self.fail()
#
#     def test_getLocalPendingChangelists(self):
#         self.fail()


if __name__ == '__builtin__':  # The "main" clause required for use in MotionBuilder.
    # Run tests as needed.

    # Warning: If you try to run these tests using nose in the same way, the setUpClass and
    # tearDownClass methods will each get called twice for some reason. Use the ./runner.py
    # script to avoid this as nose.TestLoader().loadTestsFromModule() loads correctly.
    testLoader = unittest.TestLoader()
    suite = unittest.TestSuite()
    tests = (
        TestSimpleP4Enabled,
        TestSimpleP4Disabled,
        TestAdvancedP4Enabled,
        TestAdvancedP4Disabled,
        TestRunMethodsP4Enabled,
        TestRunMethodsP4Disabled,
    )
    for test in tests:
        tests = testLoader.loadTestsFromTestCase(test)
        suite.addTests(tests)
    runner = unittest.TextTestRunner()
    result = runner.run(suite)
