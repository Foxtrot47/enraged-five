"""
Automated tests for the Perforce.py module.
"""
import unittest
import socket
import os

from RS import Perforce, Config, Globals
from RS._Perforce import servers, const


class P4EnabledBase(unittest.TestCase):
    """
    Base class to set up/tear down P4 settings with P4 enabled.
    """
    raiseErrors = None
    suspendMessageBoxes = None
    enabledUserPreference = None

    @classmethod
    def setUpClass(cls):
        super(P4EnabledBase, cls).setUpClass()
        # Store existing settings.
        cls.raiseErrors = Perforce.P4Settings.raiseErrors
        cls.suspendMessageBoxes = Globals.System.SuspendMessageBoxes
        cls.enabledUserPreference = Perforce.Enabled()
        # Suppress gui pop-ups.
        Perforce.P4Settings.raiseErrors = True
        Globals.System.SuspendMessageBoxes = True
        # Enable P4.
        Perforce.SetEnabledUserPreference(True)

    @classmethod
    def tearDownClass(cls):
        super(P4EnabledBase, cls).tearDownClass()
        # Restore settings.
        Perforce.P4Settings.raiseErrors = cls.raiseErrors
        Globals.System.SuspendMessageBoxes = cls.suspendMessageBoxes
        Perforce.SetEnabledUserPreference(cls.enabledUserPreference)


class P4DisabledBase(unittest.TestCase):
    """
    Base class to set up/tear down P4 settings with P4 disabled.
    """
    raiseErrors = None
    suspendMessageBoxes = None
    enabledUserPreference = None

    @classmethod
    def setUpClass(cls):
        # Store existing settings.
        cls.raiseErrors = Perforce.P4Settings.raiseErrors
        cls.suspendMessageBoxes = Globals.System.SuspendMessageBoxes
        cls.enabledUserPreference = Perforce.Enabled()
        # Suppress gui pop-ups.
        Perforce.P4Settings.raiseErrors = True
        Globals.System.SuspendMessageBoxes = True
        # Disable P4.
        Perforce.SetEnabledUserPreference(False)

    @classmethod
    def tearDownClass(cls):
        # Restore settings.
        Perforce.P4Settings.raiseErrors = cls.raiseErrors
        Globals.System.SuspendMessageBoxes = cls.suspendMessageBoxes
        Perforce.SetEnabledUserPreference(cls.enabledUserPreference)


class TestSimpleP4Enabled(P4EnabledBase):
    """
    MVP tests to make sure the very basics are working.
    """
    def test_02_User(self):
        self.assertEqual(Perforce.User(), Config.User.Name)

    def test_02_Client(self):
        self.assertIsInstance(Perforce.Client(), basestring)
        self.assertIsNot(Perforce.Client(), "")

    def test_02_Port(self):
        self.assertEqual(Perforce.Port(), Config.User.P4ServerDefault)

    def test_02_Host(self):
        self.assertEqual(Perforce.Host(), socket.gethostname())

    def test_05_Run(self):
        # Uses the clientRoot method as a quick check that run is working.
        self.assertTrue(os.path.isdir(Perforce.GetClientRoot()))


class TestSimpleP4Disabled(P4DisabledBase):
    """
    MVP tests to make sure the very basics are working.
    """
    def test_02_User(self):
        self.assertRaises(Perforce.P4Exception, callable=Perforce.User)

    def test_02_Client(self):
        self.assertRaises(Perforce.P4Exception, callable=Perforce.Client)

    def test_02_Port(self):
        self.assertRaises(Perforce.P4Exception, callable=Perforce.Port)

    def test_02_Host(self):
        self.assertRaises(Perforce.P4Exception, callable=Perforce.Host)

    def test_05_Run(self):
        # Uses the clientRoot method as a quick check that run is working.
        self.assertIsNone(Perforce.GetClientRoot())


class TestRunMethodsP4Enabled(P4EnabledBase):
    """
    Tests all the P4Connection methods that call run or FileState under the hood and do not
    add, edit, or remove a file or changelist.

    This test case assumes TestBasicsP4Enabled is working correctly as it needs those methods to
    generate the expected test data.
    """
    GAMEDEPOT_DEPOT_PATH = "//wildwest/data/stub.txt"
    GAMEDEPOT_DEPOT_PATHS = [
        "//wildwest/data/automatedTests/motionbuilder/perforce/subdir1/empty.txt",
        "//wildwest/data/automatedTests/motionbuilder/perforce/subdir2/empty.txt",
    ]
    GAMEDEPOT_FILESYSTEM_PATH = u'X:\\wildwest\\data\\stub.txt'
    GAMEDEPOT_DEPOT_DIR = "//wildwest/data/automatedTests/motionbuilder/perforce/*"
    GAMEDEPOT_FILESYSTEM_DIR = "X:\\wildwest\\data\\automatedTests\\motionbuilder\\perforce\\*"

    def test_GetClientRoot(self):
        self.assertTrue(os.path.isdir(Perforce.GetClientRoot()))

    def test_Where(self):
        result = Perforce.Where([self.GAMEDEPOT_DEPOT_PATH])
        resultAsDict = servers.ConversionsMixin.convertP4RecordSetToDict(result)
        client = Perforce.Client()
        expectedResult = {
            u'clientFile': u'//{}/wildwest/data/stub.txt'.format(client),
            u'depotFile': u'//wildwest/data/stub.txt',
            u'path': u'X:\\wildwest\\data\\stub.txt'
        }
        self.assertEqual(resultAsDict, expectedResult)

    def test_GetFileState(self):
        actualResult = Perforce.GetFileState(self.GAMEDEPOT_DEPOT_PATHS)
        self.assertIsInstance(actualResult, list)
        self.assertEqual(actualResult[0].DepotFilename, self.GAMEDEPOT_DEPOT_PATHS[0])

    def test_DoesFileExist(self):
        actualResult = Perforce.DoesFileExist(self.GAMEDEPOT_DEPOT_PATH)
        self.assertTrue(actualResult)

    def test_GetLocalPaths(self):
        actualResult = Perforce.GetLocalPaths([self.GAMEDEPOT_DEPOT_PATH])
        self.assertEqual(actualResult, [self.GAMEDEPOT_FILESYSTEM_PATH])

    def test_GetFileInfo(self):
        actualResult = Perforce.GetFileInfo(self.GAMEDEPOT_FILESYSTEM_PATH, 1, const.FileInfoTypes.DESC)
        expectedResult = u'Integration change.\nBranch current Wildwest to new wildwest Depot\n'
        self.assertEqual(actualResult, expectedResult)

    def test_Sync(self):
        # Force a FileState obj to be returned later by first making sure the file is at rev 0.
        Perforce.Sync([self.GAMEDEPOT_DEPOT_PATH], force=True, revision=0)
        self.assertFalse(os.path.exists(self.GAMEDEPOT_FILESYSTEM_PATH))

        actualResult = Perforce.Sync([self.GAMEDEPOT_DEPOT_PATH])
        self.assertIsInstance(actualResult, servers.FileState)
        self.assertTrue(os.path.exists(self.GAMEDEPOT_FILESYSTEM_PATH))

    def test_Files(self):
        result = Perforce.Files([self.GAMEDEPOT_DEPOT_PATH])
        self.assertEqual(result.__class__.__name__, "P4RecordSet")
        p4RecordSetAsList = servers.ConversionsMixin.convertP4RecordSetToList(result)
        self.assertTrue(len(p4RecordSetAsList) == 1)
        self.assertEqual(p4RecordSetAsList[0].get("depotFile"), self.GAMEDEPOT_DEPOT_PATH)

    def test_Dirs(self):
        expectedP4RecordList = [
            {u'dir': u'//wildwest/data/automatedTests/motionbuilder/perforce/subdir1'},
            {u'dir': u'//wildwest/data/automatedTests/motionbuilder/perforce/subdir2'}
        ]

        result = Perforce.Dirs([self.GAMEDEPOT_DEPOT_DIR])
        actualP4RecordList = servers.ConversionsMixin.convertP4RecordSetToList(result)
        self.assertEqual(actualP4RecordList, expectedP4RecordList)

        result = Perforce.Dirs([self.GAMEDEPOT_DEPOT_DIR[:-1]])
        actualP4RecordList = servers.ConversionsMixin.convertP4RecordSetToList(result)
        self.assertEqual(actualP4RecordList, [])

        result = Perforce.Dirs([self.GAMEDEPOT_DEPOT_DIR[:-2]])
        actualP4RecordList = servers.ConversionsMixin.convertP4RecordSetToList(result)
        self.assertEqual(actualP4RecordList, [{u'dir': u'//wildwest/data/automatedTests/motionbuilder/perforce'}])

        result = Perforce.Dirs([self.GAMEDEPOT_FILESYSTEM_DIR])
        actualP4RecordList = servers.ConversionsMixin.convertP4RecordSetToList(result)
        self.assertEqual(actualP4RecordList, expectedP4RecordList)

        result = Perforce.Dirs([self.GAMEDEPOT_FILESYSTEM_DIR[:-1]])
        actualP4RecordList = servers.ConversionsMixin.convertP4RecordSetToList(result)
        self.assertEqual(actualP4RecordList, [])

        result = Perforce.Dirs([self.GAMEDEPOT_FILESYSTEM_DIR[:-2]])
        actualP4RecordList = servers.ConversionsMixin.convertP4RecordSetToList(result)
        self.assertEqual(actualP4RecordList, [{u'dir': u'//wildwest/data/automatedTests/motionbuilder/perforce'}])

        self.assertRaises(
            servers.P4Exception,
            Perforce.Dirs,
            ["C:\\notARealPath"]
        )

    def test_GetUserData(self):
        keys = (u'Update', u'Access', u'User', u'FullName', u'Type', u'Email',)
        actualResult = Perforce.GetUserData(Perforce.User())
        self.assertEqual(sorted(actualResult.keys()), sorted(keys))
        self.assertEqual(actualResult.get("User"), Perforce.User())


class TestRunMethodsP4Disabled(P4DisabledBase):
    """
    Tests all the P4Connection methods that call run or FileState under the hood and do not
    add, edit, or remove a file or changelist.

    This test case assumes TestBasicsP4Enabled is working correctly as it needs those methods to
    generate the expected test data.
    """
    GAMEDEPOT_DEPOT_PATH = "//wildwest/data/stub.txt"
    GAMEDEPOT_DEPOT_PATHS = [
        "//wildwest/data/automatedTests/motionbuilder/perforce/subdir1/empty.txt",
        "//wildwest/data/automatedTests/motionbuilder/perforce/subdir2/empty.txt",
    ]
    GAMEDEPOT_FILESYSTEM_PATH = u'X:\\wildwest\\data\\stub.txt'
    GAMEDEPOT_DEPOT_DIR = "//wildwest/data/automatedTests/motionbuilder/perforce/*"
    GAMEDEPOT_FILESYSTEM_DIR = "X:\\wildwest\\data\\automatedTests\\motionbuilder\\perforce\\*"

    def test_GetClientRoot(self):
        self.assertIsNone(Perforce.GetClientRoot())

    def test_Where(self):
        self.assertEqual(Perforce.Where([self.GAMEDEPOT_DEPOT_PATH]), [])

    def test_GetFileState(self):
        self.assertEqual(Perforce.GetFileState(self.GAMEDEPOT_DEPOT_PATHS), [])

    def test_DoesFileExist(self):
        self.assertRaises(
            servers.P4Exception,
            Perforce.DoesFileExist,
            [self.GAMEDEPOT_DEPOT_PATH]
        )

    def test_GetLocalPaths(self):
        self.assertEqual(Perforce.GetLocalPaths([self.GAMEDEPOT_DEPOT_PATH]), [])

    def test_GetFileInfo(self):
        actualResult = Perforce.GetFileInfo(self.GAMEDEPOT_FILESYSTEM_PATH, 1, const.FileInfoTypes.DESC)
        self.assertEqual(actualResult, None)

    def test_Sync(self):
        self.assertEqual(Perforce.Sync([self.GAMEDEPOT_DEPOT_PATH]), [])

    def test_Files(self):
        self.assertEqual(Perforce.Files([self.GAMEDEPOT_DEPOT_PATH]), [])

    def test_Dirs(self):
        result = Perforce.Dirs([self.GAMEDEPOT_DEPOT_DIR])
        self.assertEqual(result, [])

        result = Perforce.Dirs([self.GAMEDEPOT_DEPOT_DIR[:-1]])
        self.assertEqual(result, [])

        result = Perforce.Dirs([self.GAMEDEPOT_DEPOT_DIR[:-2]])
        self.assertEqual(result, [])

        result = Perforce.Dirs([self.GAMEDEPOT_FILESYSTEM_DIR])
        self.assertEqual(result, [])

        result = Perforce.Dirs([self.GAMEDEPOT_FILESYSTEM_DIR[:-1]])
        self.assertEqual(result, [])

        result = Perforce.Dirs([self.GAMEDEPOT_FILESYSTEM_DIR[:-2]])
        self.assertEqual(result, [])

        self.assertEqual(Perforce.Dirs(["notARealPath"]), [])

    def test_GetUserData(self):
        self.assertRaises(servers.P4Exception, Perforce.GetUserData, "fakeUserName")


if __name__ == '__builtin__':  # The "main" clause required for use in MotionBuilder.
    # Run tests as needed.

    # Warning: If you try to run these tests using nose in the same way, the setUpClass and
    # tearDownClass methods will each get called twice for some reason. Use the ./runner.py
    # script to avoid this as nose.TestLoader().loadTestsFromModule() loads correctly.
    testLoader = unittest.TestLoader()
    suite = unittest.TestSuite()
    tests = (
        TestSimpleP4Enabled,
        TestSimpleP4Disabled,
        TestRunMethodsP4Enabled,
        TestRunMethodsP4Disabled,
    )
    for test in tests:
        tests = testLoader.loadTestsFromTestCase(test)
        suite.addTests(tests)
    runner = unittest.TextTestRunner()
    result = runner.run(suite)
