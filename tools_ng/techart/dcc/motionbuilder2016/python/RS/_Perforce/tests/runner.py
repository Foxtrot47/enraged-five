"""
Runs all the perforce unittests via nose.
"""
if __name__ == '__builtin__':  # The "main" clause required for use in MotionBuilder.
    import nose

    from RS._Perforce.tests import test_servers

    testLoader = nose.loader.TestLoader()
    testSuite = testLoader.loadTestsFromModule(test_servers)
    nose.run(suite=testSuite)
