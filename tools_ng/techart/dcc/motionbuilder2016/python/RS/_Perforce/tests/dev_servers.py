"""
Snippets of code to manually test the server.py module.
"""
import datetime
import pathlib

from RS._Perforce import servers, const


class Paths(object):
    GAMEDEPOT_DEPOT_PATH = "//wildwest/data/stub.txt"
    GAMEDEPOT_DEPOT_PATHS = [
        "//wildwest/data/automatedTests/motionbuilder/perforce/subdir1/empty.txt",
        "//wildwest/data/automatedTests/motionbuilder/perforce/subdir2/empty.txt",
    ]
    GAMEDEPOT_FILESYSTEM_PATH = u'X:\\wildwest\\data\\stub.txt'
    GAMEDEPOT_DEPOT_DIR = "//wildwest/data/automatedTests/motionbuilder/perforce/*"
    MOCAPDEPOT_DEPOT_PATH = "//depot/projects/global_assets/placeholder.txt"
    MOCAPDEPOT_FILESYSTEM_PATH = u"X:\\GC\\projects\\global_assets\\placeholder.txt"


fileDir = "//wildwest/dcc/motionbuilder/python/RS/_Perforce/..."
changelist = 25982377
with servers.gameDepot as con:
    #print "- getAllMarkedDeleteDepotFiles", con.getAllMarkedDeleteDepotFiles(fileDir) # works
    print "- getAllMarkedAddFiles", con.getAllMarkedAddFiles(fileDir)  # Not returning everything
    #print "- getChangelist", con.getChangelist(changelist) # works
    #print "- getFilesInChangelist", con.getFilesInChangelist(changelist)
    #print "- getDescription", con.getDescription(changelist)) # works
    #print "- changes", servers.ConversionsMixin.convertRecordSetToList(con.changes(["//wildwest/dcc/motionbuilder/python/RS/_Perforce/windows.py"])) # works
    #print "- getChangelists", con.getChangelists(user=con.user()) # works
    #print servers.ConversionsMixin.convertRecordSetToList(con.getChangelists(changelistType=const.ChangelistTypes.PENDING, user=con.user(), client=con.client()))  # works
    #print "- getLocalPendingChangelists", con.getLocalPendingChangelists() # works

    # changelist = con.createChangelist("testing add/remove") # works
    # changelistNum = int(changelist.Number)

    # path = pathlib.Path("X:\\wildwest\\data\\automatedTests\\motionbuilder\\perforce\\test_add.txt")
    # path.touch()
    # print "- add", con.add([str(path)], changelistNum)  # works
    # print "- addOrEditFiles", con.addOrEditFiles(Paths.GAMEDEPOT_DEPOT_PATHS, changelistNum=changelistNum) # also tests con.edit() # works

    # print "- shelve", con.shelve([str(path)], changelistNum=changelistNum)  # works
    # print "- revert", con.revert([str(path)])  # works

    # print "- shelveChangelist", con.shelveChangelist(changelistNum) # works
    # print "- revertChangelist", con.revertChangelist(changelistNum) # works

    # print "- deleteShelvedFiles", con.deleteShelvedFiles(changelistNum)  # works
    # print "- deleteChangelist", con.deleteChangelist(changelistNum)  # works

    # print "- submit", con.submit(changelistNum)  # works
