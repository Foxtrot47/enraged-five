"""
Description:
    Helper classes and functions for interfacing with Perforce.
    This module wraps our .NET RSG.SourceControl.Perforce library which is exposed via the clr module.

Automated tests can be found in RS._Perforce.tests.test_servers.py. Keep these updated!
"""
import re
import os
import stat
import types
import socket
import functools
import traceback
import inspect

import clr

from RS import Config, Globals
from RS.Utils import UserPreferences
from RS.Tools.CameraToolBox.PyCore.Metaclasses import singleton
from RS.Tools.UI import Application

from PySide import QtGui

from RS._Perforce import const

clr.AddReference("RSG.SourceControl.Perforce")
clr.AddReference("RSG.Base")
from RSG.Base import Logging
from RSG.SourceControl.Perforce import P4
from RSG.SourceControl.Perforce import FileState
from RSG.SourceControl.Perforce import FileAction
from RSG.SourceControl.Perforce import Changelist


class P4Settings(object):
    """
    Settings referenced by TechArt tools to modify error handling and running p4 commands.
    """

    raiseErrors = False
    ignoreErrors = False
    p4EnabledByUser = UserPreferences.__Readini__(
        "menu", "Perforce Integration", notFoundValue=True, default_value=True
    )
    p4EnabledByTools = os.environ.get("PERFORCE_ENABLED", "true").lower() == "true"


class P4Exception(Exception):
    """
    Raise when there is a P4 related error.
    """


class P4ConnectionNotAttempted(P4Exception):
    """
    Raise when a connection has not yet been attempted.
    """


class P4PathFormattingError(P4Exception):
    """
    Raise when a path has been incorrectly formatted for the specified command.
    """


class EnableExceptions(object):
    """
    Context manager for raising p4 exceptions
    """

    def __init__(self):
        self._state = None

    def __enter__(self):
        """
        method called by the with statement
        """
        self._state = P4Settings.raiseErrors
        P4Settings.raiseErrors = True

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (str): error string
            traceback (traceback): the full traceback of the error
        """
        P4Settings.raiseErrors = self._state


def Enabled():
    """
    Checks global enabled state for perforce operations. All tools should respect this.
    """
    return P4Settings.p4EnabledByTools and P4Settings.p4EnabledByUser


def SetEnabledUserPreference(enable):
    """
    Change the user's perforce integration setting and update the client state to match.
    """
    UserPreferences.__Saveini__("menu", "Perforce Integration", enable)
    P4Settings.p4EnabledByUser = enable

    # Update all connections.
    gameDepot = P4ConnectionGame()
    mocapDepot = P4ConnectionMocap()
    if enable is True:
        gameDepot.reset()
        mocapDepot.reset()
    else:
        gameDepot.disposeClient()
        mocapDepot.disposeClient()


def assertP4Enabled(func):
    """
    Decorator used to throw an exception if the wrapped method is called while perforce integration is disabled.
    Use to ensure methods that would potentially cause problems if run while perforce integration is
    disabled cannot be called in that case.
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if not Enabled():
            try:
                isMethod = inspect.getargspec(func)[0][0] == "self"
            except Exception:  # Keeping broad, because this has to work no matter what.
                isMethod = False
            if isMethod:
                fullName = "{}.{}.{}".format(func.__module__, args[0].__class__.__name__, func.__name__)
            else:
                fullName = "{}.{}".format(func.__module__, func.__name__)

            msg = "Calling {} is not allowed while Perforce integration is disabled.".format(fullName)
            raise P4Exception(msg)
        return func(*args, **kwargs)

    return wrapper


# Perforce Path Utility Methods ########################################################################################


def FixFilePath(filename, invert=False):
    """
    Replaces characters not recognized by P4 with their escapes.

    Args:
        filename (str): path to file

    Returns:
        str: The modified file path.
    """
    mappings = (("%", "%25", "%25|%23|%40"),  ("#", "%23", "%23"), ("@", "%40", "%40"))
    if invert is True:
        mappings = (("%25", "%", "%"), ("%23", "#", "#"), ("%40", "@", "@"))

    for character, escape, regex in mappings:
        if not re.search(regex, filename) and character in filename:
            filename = filename.replace(character, escape)

    # Not sure if this is needed
    return str(r"{}".format(filename))


def FixFilePaths(filenames):
    """
    Some file paths contain @, we need to convert it %

    Args:
        filenames (list of str): path to files

    Returns:
        list of str
    """
    filenames = filenames or []
    if not isinstance(filenames, (list, tuple)):
        filenames = [filenames]
    return [FixFilePath(filename) for filename in filenames]


def SwitchPathStyle(path):
    """
    Detect and switch between a Perforce depot path or a Windows filesystem path and vice versa.

    Args:
        path (str): The depot path or windows filesystem path.

    Returns:
        str
    """
    if os.path.splitdrive(path)[0]:
        _, path = os.path.splitdrive(path)[0]
        return "/{}{}".format(["", "depot/"]["gta5" in path.lower()], path.replace("\\", "/"))
    else:
        return r"x:{}".format(path.replace("/", "\\")[1:])


def EnsureFileWritable(filePath):
    """
    Sets the file to be writable on disk.

    Args:
        filePath (str): Local path to a file on disk
    """
    if os.path.exists(filePath):
        if not os.access(filePath, os.W_OK):
            os.chmod(filePath, stat.S_IWRITE)


# FileState Helpers ####################################################################################################


def assertFileState(fileStateObj):
    """
    Asserts if the supplied object isn't of type RSG.SourceControl.Perforce.FileState

    Args:
        fileStateObj (RSG.SourceControl.Perforce.FileState): The RSG.SourceControl.Perforce.FileState object to check.
    """
    assert isinstance(
        fileStateObj, FileState
    ), "Incorrect type: Must supply a RSG.SourceControl.Perforce.FileState object!"


def IsLatest(fileStateObj):
    """
    Checks to see if the supplied RSG.SourceControl.Perforce.FileState object is the latest revision.

    Args:
        fileStateObj (RSG.SourceControl.Perforce.FileState): The RSG.SourceControl.Perforce.FileState object to check.

    Returns:
        bool
    """
    assertFileState(fileStateObj)
    return fileStateObj.HaveRevision == fileStateObj.HeadRevision


def IsOpenForEdit(fileStateObj):
    """
    Checks whether or not the supplied file is opened for edit by the current user.

    Args:
        fileStateObj (RSG.SourceControl.Perforce.FileState): The RSG.SourceControl.Perforce.FileState object to check.

    Returns:
        bool
    """
    assertFileState(fileStateObj)
    return fileStateObj.OpenAction == FileAction.Edit


def IsOpenForAdd(fileStateObj):
    """
    Checks whether or not the supplied file is opened for edit by the current user.

    Args:
        fileStateObj (RSG.SourceControl.Perforce.FileState): The RSG.SourceControl.Perforce.FileState object to check.

    Returns:
        bool
    """
    assertFileState(fileStateObj)
    return fileStateObj.OpenAction == FileAction.Add


def CanOpenForEdit(fileStateObj):
    """
    Tests whether or not a file can be opened for edit by the current user.

    Args:
        fileStateObj (RSG.SourceControl.Perforce.FileState): The RSG.SourceControl.Perforce.FileState object to check.

    Returns:
        bool
    """
    assertFileState(fileStateObj)
    return not fileStateObj.OtherLocked


# Connection ###########################################################################################################


class ConversionsMixin(object):
    """
    Mixins for converting various P4 C# objects to Python standard data types.
    """

    @staticmethod
    def convertP4RecordToDict(record):
        """
        Convert C# P4API.P4Record objects to dict.

        Examples:
            >>> with servers.gameDepot as con:
            ...     recordSet = con.files(["//wildwest/data/stub.txt"])
            ...     record = list(recordSet.Records)[0]
            ...     servers.ConversionsMixin.convertP4RecordToDict(p4Record)
            {u'rev': u'1',
             u'time': u'1336574713',
             u'action': u'branch',
             u'type': u'text',
             u'depotFile': u'//wildwest/data/stub.txt', u'change': u'2596348'}

        Args:
            record (P4API.P4Record): P4API.P4Record C# object.

        Returns:
            dict
        """
        return {key: record[key] for key in record.Fields.Keys}

    @classmethod
    def convertP4RecordSetToList(cls, recordSet):
        """
        Convert C# P4API.P4RecordSet object to list of dictionaries that represent the P4Records.

        Args:
            recordSet (P4API.P4RecordSet): P4API.P4RecordSet C# object.

        Returns:
            list of dict
        """
        return [cls.convertP4RecordToDict(record) for record in recordSet.Records]

    @staticmethod
    def convertP4RecordSetToDict(recordSet):
        """
        Convert C# P4API.P4RecordSet object to a single dictionary.

        Warnings:
            * Reduces a P4API.P4RecordSet with a single P4Record to a dict; DO NOT USE on a
            P4RecordSet that may have multiple P4Records - use convertP4RecordSetToList instead.

        Args:
            recordSet (P4API.P4RecordSet): P4API.P4RecordSet C# object.

        Returns:
            dict
        """
        return {key: record[key] for record in recordSet.Records for key in record.Fields.Keys}


def assertConnectionAttempted(func, *args):
    """
    Confirm that a P4.Connect has been called.

    Args:
        func (method): method that is being decorated, it is automatically passed into this method.
        *args: For catching extra arguments passed into the decorator

    Raises:
        P4ConnectionNotAttempted: If a connection has not yet been attempted via the P4 instance.

    Returns:
        function
    """
    # First wrapper is to grab arguments passed into the decorator
    @functools.wraps(func)
    def wrapper(instance=None, *args, **kwargs):
        if not instance.isConnectionAttempted():
            raise P4ConnectionNotAttempted(
                "The P4.Connect() method must be called before running "
                "'P4Connection.{}'; make sure to run all p4 calls within "
                "the context manager!".format(func.__name__)
            )
        return func(instance, *args, **kwargs)

    return wrapper


def p4DisabledReturnValue(returnValue):
    """
    Decorator that will return a preset object if P4Settings.p4EnabledByUser is set to False.

    Args:
        returnValue (object): The object to return; passing a list, set, or dict tells the decorator
            to create a new, empty instance of that class.
    """
    mutableTypes = (list, set, dict)

    def callable(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            if not Enabled():
                if isinstance(returnValue, mutableTypes):
                    # Create a new instance to avoid modifying the return value for the next call.
                    # Believe it or not, this is faster than calling copy.copy().
                    return type(returnValue)()
                return returnValue
            return func(*args, **kwargs)

        return wrapped

    return callable


class P4Connection(ConversionsMixin, object):
    """
    Context manager than handles initialising, opening, and closing the connection to Perforce.

    See Also:
        X:\3rdParty\dev\cli\P4.Net\src\P4API\P4Connection.cs

    Notes:
        * Any method with the "if Enabled" check should not have the checkConnection decorator.
        * Purposefully removed the isInitialised method.
        * Most commands are designed to be run via the context manager approach to simplify the
            code and testing.
    """

    def __init__(self, server, displayName, client=None, cwd=None):
        """
        Connect to server, giving the option to choose which workspace to use if multiple exist

        Warnings:
            * The cwd param must be set if you intend to use a .p4config file.

        Args:
            server (str): The Host and port name to connect to.
            displayName (str): A nice name for the connection which will be displayed to the user in the
                              event of an error.

        Keyword Args:
            client (str): The workspace to pass to the P4 instance.
            cwd (str): The current working directory.
        """
        self._server = server
        self._displayName = displayName
        self._client = client
        self._cwd = cwd

        self._connectionAttempted = False
        self._openContextManagersCount = 0
        self._P4 = None

    def _isClientInitialised(self):
        """
        Quick way to confirm that the P4 client object was created using the stored args.

        Returns:
            bool: True if initialised; False if not.
        """
        return self._P4 is not None

    def _initClient(self):
        """
        Initialises a new instance of the perforce client (the P4 class).
        """
        if self._isClientInitialised() or not Enabled():
            return

        try:
            self._P4 = P4()
        except Exception as error:
            self._P4 = P4(Logging.LogFactory.ApplicationLog)

        if self._server is not None:
            self._P4.Port = self._server
        if self._client is not None:
            self._P4.Client = self._client
        if self._cwd is not None:
            self._P4.CWD = self._cwd

    def isConnectionAttempted(self):
        """
        Used by decorator.

        Returns:
            bool
        """
        return self._connectionAttempted

    def disposeClient(self):
        """
        Destroys and cleans up internal references to the current perforce client instance.
        """
        if not self._isClientInitialised():
            return
        self._P4.Dispose()
        self._P4 = None  # Force garbage collection while ensuring var is available to access later.
        self._connectionAttempted = False

    def reset(self):
        """
        Fully resets the perforce client internal state by destroying the current instance and
        initialising a new one.

        Explicitly avoids initialising if the class wasn't already initialised, as this prevents
        triggering errors trying to call the 'p4 clients' command on the mocap depot when a user
        doesn't have permissions for it.
        """
        if self._isClientInitialised():
            self.disposeClient()
            self._initClient()

    def _connect(self):
        """
        Attempt to make a connection.
        """
        self._P4.Connect()
        self._connectionAttempted = True

    def _disconnect(self):
        """
        Attempt to disconnect.
        """
        if not self._connectionAttempted:
            return

        cwd = self._P4.CWD
        self._P4.Disconnect()
        # CWD setting is forgotten during Disconnect() so restore it ready for next connection (url:bugstar:6226352)
        self._P4.CWD = cwd
        self._connectionAttempted = False

    def reconnect(self):
        """
        Disconnects and connects the perforce instance. This is to resolve network issues where
        the connection is invalid. This can be called after a long running process to ensure
        the connection did not time out.

        Notes:
            * Only used in X:\wildwest\dcc\motionbuilder\python\RS\Core\Automation2\__init__.py
        """
        reopenConnection = False
        if self._connectionAttempted:
            self._disconnect()
            reopenConnection = True

        self._initClient()

        if reopenConnection:
            # Only call connect if the connection was already open at the beginning of this function.
            # Otherwise this could leave a dangling connection.
            self._connect()

    def __enter__(self):
        """
        Called when context manager context is entered. Opens the connection to perforce.

        Returns:
            P4Connection instance
        """
        self._openContextManagersCount += 1

        if not self._isClientInitialised():
            self._initClient()

        # Note the methods are responsible for implementing handling for when p4 disabled.

        if not self._connectionAttempted and Enabled():
            self._connect()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Called when context manager context is exited. Closes the connection to perforce.

        Notes:
            * We can get multiple enters before any exits, so we must check and decrement the
            counter on each exit in order to only then close the connection on the last exit.
        """
        self._openContextManagersCount -= 1
        if self._openContextManagersCount != 0:
            # Don't disconnect if this instance didn't open the connection (e.g. when nested within another context)
            return

        self._disconnect()

    @assertP4Enabled
    def user(self):
        """
        Gets the name of the user using perforce.

        Prefer using this method to get the p4 user name over getting the user from the Config or
        the environment, as the Perforce user name might not be the same as the ActiveDirectory name
        or the login username if tools are being used by an outsource company. All three names are
        the same within Rockstar.

        Returns:
            str
        """
        if not self._isClientInitialised():
            self._initClient()

        return self._P4.User

    @assertP4Enabled
    def client(self):
        """
        Gets the name of the client using Perforce

        Return:
            str
        """
        if not self._isClientInitialised():
            self._initClient()

        return self._P4.Client

    @assertP4Enabled
    def port(self):
        """
        Gets the port used by Perforce

        Return:
            str
        """
        if not self._isClientInitialised():
            self._initClient()

        return self._P4.Port

    @assertP4Enabled
    def host(self):
        """
        Gets the host for Perforce

        Return:
            str
        """
        if not self._isClientInitialised():
            self._initClient()

        return self._P4.Host

    @assertP4Enabled
    @assertConnectionAttempted
    def run(self, p4Cmd, args=None):
        """
        Native P4 Run command.

        Args:
            p4Cmd (str): valid p4 cmd
            args (list[str]):  list of paths and/or perforce arguments

        Returns:
            P4 Record or error message
        """
        args = args or []
        records = self._P4.Run(p4Cmd, False, args)

        if P4Settings.ignoreErrors is True:
            return records
        elif records.HasErrors() and not P4Settings.raiseErrors and not Globals.System.SuspendMessageBoxes:
            # This shouldn't be here, but to stop other tools from breaking, this is a lesser of 2 evils.
            QtGui.QMessageBox.critical(
                Application.GetMainWindow(), "P4 Error", "P4 returned the following errors:\n{0}".format(records.ErrorMessage)
            )
            return records
        elif records.HasErrors() and P4Settings.raiseErrors:
            raise P4Exception(records.ErrorMessage)
        else:
            return records

    @assertConnectionAttempted
    def clients(self, userFilter=None, nameFilter=None):
        """
        Gets a list of clients for this machine based on the name of the clients

        See Also:
            https://www.perforce.com/manuals/v15.1/cmdref/p4_clients.html

        Keyword Args:
            userFilter (bool): Limit the output to workspaces owned by the currents user using the "-u" option.
            nameFilter (str): Limit the output to clients whose name matches the filter pattern.

        Returns:
            list[dict]
        """
        args = []
        if userFilter is not None:
            args.extend(["-u", userFilter])
        if nameFilter is not None:
            args.extend(["-e", nameFilter])
        p4Records = [client for client in self.run(const.Commands.CLIENTS, args=args)]
        return [self.convertP4RecordToDict(p4Record) for p4Record in p4Records]

    @p4DisabledReturnValue([])
    def userHostClients(self):
        """
        Get a list of all the current depot's clients for this machine which are owned by the user.

        Returns:
            list[str]
        """
        user = self.user()
        host = socket.gethostname()

        # Using the global G option so we get back a python dictionary which is easier to work with
        results = self.run(const.Commands.CLIENTS, ["-u", user])

        results = [val for val in results if val["Host"] == host]
        if len(results) == 0:
            results = self.run(const.Commands.CLIENTS, ["-e", "*{0}*".format(host)])

        return [result["client"] for result in results]

    @p4DisabledReturnValue(None)
    def clientRoot(self):
        """
        Get the client's root directory for the current workspace.

        Returns:
            str
        """
        clientDetails = self.run(const.Commands.CLIENT, args=["-o"])
        return clientDetails[0]["Root"]

    @p4DisabledReturnValue([])
    def where(self, depotPaths):
        """
        Calls 'p4 where' with the depot paths as the input.

        Notes:
            * All 'illegal' characters are replaced with their escapes prior to passing on to 'p4 where'.
            * All input paths will have any trialing "/" characters removed.

        Args:
            depotPaths (list of str): The input depot paths.

        Returns:
            list[dict]: A list of the p4 dict returned by 'p4 where'.
        """
        fixedPaths = [FixFilePath(depotPath.rstrip("/")) for depotPath in depotPaths]
        return self.run(const.Commands.WHERE, args=fixedPaths)

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def fileState(self, filePath):
        """
        Run fstat on a file.

        Optimised for single file usage.

        Args:
            filePath (str): Local filesystem path or depot path to the file.

        Returns:
            RSG.SourceControl.Perforce.FileState
        """
        return FileState(self._P4, FixFilePath(filePath))

    @p4DisabledReturnValue([])
    @assertConnectionAttempted
    def fileStates(self, filenames):
        """
        Run fstat on a list of files.

        Optimised to only run a single p4 query for multiple files.

        Args:
            filenames (list[str]): The file names to test.

        Returns:
            list[RSG.SourceControl.Perforce.FileState]
        """
        if not filenames:
            return []

        # Fixing file paths
        filenames = FixFilePaths(filenames)
        fileStates = list(FileState.Create(self._P4, filenames))

        return fileStates

    @assertP4Enabled
    def doesFileExist(self, filePath):
        """
        Checks to see if the supplied filePath exists in Perforce.
        Also checks if the file has been marked for add locally

        Args:
            filePath (str): The filePath to check.

        Returns:
            bool
        """
        inValidHeadActionsForFileExistance = [0, 3, 6]  # Unknown/Delete/MoveAndDelete

        fileStateObj = self.fileState(filePath)
        if (fileStateObj is not None) and (fileStateObj.HeadAction not in inValidHeadActionsForFileExistance):
            return True
        if IsOpenForAdd(fileStateObj):
            return True
        return False

    @p4DisabledReturnValue([])
    def getLocalPaths(self, depotPaths):
        """
        Convenience wrapper around passing depot paths to 'p4 where' to get the local path under the current workspace.

        Works on file and directory paths.

        Args:
            depotPaths (list[str]): The input depot paths.

        Returns:
            list[str]: The local paths.
        """
        return [result["path"] for result in self.where(depotPaths)]

    @p4DisabledReturnValue(None)
    def getFileInfo(self, localFilePath, revision, key):
        """
        Gets information about the file from Perforce based on the revision number and key

        Args:
            localFilePath (str): local perforce file path to test
            revision (int): revision you want to check
            key (str): the type of information you wish to return. See the const.FileInfoTypes for acceptable values.

        Returns:
            key result - could be int/string etc.
        """
        result = None
        filePathState = self.fileState(localFilePath)

        if filePathState:
            depotFile = filePathState.DepotFilename
            cmdArgs = ["{}#{}".format(depotFile, revision)]

            if key == const.FileInfoTypes.DESC:
                # Return the full changelist description rather than the default truncated form
                cmdArgs.insert(0, "-l")

            recordSet = self.run(const.Commands.CHANGES, cmdArgs)

            if recordSet.Records.Length != 0 and recordSet.Records[0].Fields.ContainsKey(key):
                result = recordSet.Records[0].Fields.get_Item(key)

        return result

    @p4DisabledReturnValue([])
    def getAllMarkedDeleteDepotFiles(self, startingDirectory):
        """
        Gets ALL the files marked for delete within the supplied directory.

        Args:
            startingDirectory (str): directory to inspect

        Returns:
            list[str]: A list of depot file names.
        """
        args = ["-a", startingDirectory]
        p4Files = self.run(const.Commands.OPENED, args)

        fileList = [
            record[key]
            for record in p4Files.Records
            for key in record.get_Fields().Keys
            if key == "depotFile" and record["action"] == "delete"
        ]

        return fileList

    @p4DisabledReturnValue([])
    def getAllMarkedAddFiles(self, startingDirectory):
        """
        Gets all the files marked for Add in the supplied directory

        Returns:
            dict[str: str]: clientFileName & depotFileName
        """
        fileObjs = []
        args = ["{}/*".format(startingDirectory)]
        p4Result = self.run(const.Commands.OPENED, args)
        for p4Record in p4Result.Records:
            if p4Record["action"] == "add":
                fileObj = {"depotFilePath": p4Record["depotFile"]}
                recSet = self.where([fileObj["depotFilePath"]])
                recSetDict = self.convertP4RecordSetToDict(recSet)
                fileObj["clientFilePath"] = recSetDict["clientFile"]
                fileObjs.append(fileObj)

        return fileObjs

    @p4DisabledReturnValue([])
    def add(self, filenames, changelistNum=None, force=False, fileType=None):
        """
        Adds the supplied filenames to Perforce.

        Warnings:
            * _Perforce.windows.Add accepts a str or list - this method only accepts a list.
            * If a file has already been added, it will not get added to the changelist and NO error
                will be raised or message printed. This is a little odd considering that the command will
                print a warning message if run in Powershell.

        Notes:
            * This method should NOT escape symbols in the file paths (@, etc).
            * The P4 Add command accepts characters normally ignored by the other P4 commands.
            * Replacing the characters with their escapes would add a file to perforce with the escapes in
                the filename instead of the intended character.

        Args:
            filenames (list[str]): The filenames to add.

        Keyword Args:
            changelistNum (int): Add files to the specified changelist.
                Default: None - will add to the Default changelist.
            force (boolean): Indicates that the file should be force added.
            fileType (str): The file type that we want to add the file as. Should be string that
                matches one of the following list of fle types;
                http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html

        Returns:
            list[RSG.SourceControl.Perforce.FileState]: The FileState objects that were added.
        """
        # IMPORTANT: p4 add should NOT escape symbols in the file paths (@, etc).
        # The P4 Add command accepts characters normally ignored by the other P4 commands.
        # Replacing the characters with their escapes would add a file to perforce with the escapes in
        # the filename instead of the intended character.

        args = [filename for filename in filenames]

        if fileType:
            args[0:0] = ["-t", fileType]
        if force:
            args[0:0] = ["-f"]
        if changelistNum:
            args[0:0] = ["-c", str(changelistNum)]

        recordSet = self.run(const.Commands.ADD, args)
        fileStates = list(FileState.Create(self._P4, recordSet))

        return fileStates

    def edit(self, filenames, changelistNum=None, fileType=None, forceWritable=True):
        """
        Opens for edit the supplied filenames.

        Warnings:
            _Perforce.windows.Edit accepts a str or list - this method only accepts a list.

        Args:
            filenames (list[str]): The filenames to open for edit.

        Keyword Args:
            changelistNum (int): Open for edit the files under this changelist.
            fileType (str): The file type that we want to add the file as. Should be string that matches
                one of the following list of fle types;
                http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html
            forceWritable (bool): If True, the file will have write permissions opened on it.

        Returns:
            list[RSG.SourceControl.Perforce.FileState]: FileState objects that were opened for edit.
        """
        fileStates = []
        if not Enabled():
            if forceWritable:
                for filename in filenames:
                    EnsureFileWritable(filename)
            return fileStates

        p4Filenames = FixFilePaths(filenames)

        # Get the filenames
        args = [filename for filename in p4Filenames]

        # Add file type flag to the front of the args list
        if fileType:
            args[0:0] = ["-t", fileType]

        # Add changelist flag to the front of the args list
        if changelistNum:
            args[0:0] = ["-c", str(changelistNum)]

        recordSet = self.run(const.Commands.EDIT, args)
        fileStates = list(FileState.Create(self._P4, recordSet))

        return fileStates

    def addOrEditFiles(self, filenames, changelistNum=None, fileType=None, force=False):
        """
        Adds or Opens for edit the supplied filenames.

        Args:

            filenames (list[str]): The filenames to open for add or edit.

        Keyword Args:
            changelistNum (int):
                Added files or Open for edit the files under this changelist.
            fileType:
                The file type that we want to add the file as. Should be string that matches
                one of the following list of fle types;
                http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html
            force (bool):
                Indicates that the file should be force added.

        Returns:
            list[RS.PyP4.FileState]: FileState objects that were opened for edit or added.
        """
        vals = []
        filesAdded = self.add(filenames, changelistNum=changelistNum, force=force, fileType=fileType)
        filesEdited = self.edit(filenames, changelistNum=changelistNum, fileType=fileType)
        for record in [filesAdded, filesEdited]:
            vals.extend(record)
        return vals

    @p4DisabledReturnValue([])
    def sync(self, filenames, force=False, revision=None):
        """
        Syncs the supplied filename(s).

        Args:
            filenames (list[str]): The filenames to sync.

        Keyword Args:
            force (bool): Whether or not to force the sync.

        Returns:
            list[RSG.SourceControl.Perforce.FileState]: FileState objects that were synced.
        """
        if not filenames:
            # Guard against accidental depot root sync
            return []

        filenames = FixFilePaths(filenames)

        args = []
        if force:
            args.append("-f")
        for filename in filenames:
            if revision is not None:
                filename = "{}#{}".format(filename, revision)
            args.append(filename)

        recordSet = self.run(const.Commands.SYNC, args)
        return list(FileState.Create(self._P4, recordSet))

    @p4DisabledReturnValue(None)
    def getChangelist(self, changelistNumber):
        """
        Query a changelist.

        Args:
            changelistNumber (int): The changelist number to query.

        Returns:
            RSG.SourceControl.Perforce.Changelist or None if not found.
        """
        changelist = Changelist.Create(self._P4, [changelistNumber])
        if changelist.Length == 1:
            return changelist[0]
        return None

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def createChangelist(self, description, force=True):
        """
        Creates a new changelist.

        Args:
            description (str): The changelist description.
            force (bool): When True, force the creation of the changelist, even if a changelist with the same
                             description already exists.
                             When False, return an existing changelist with the same description if possible..

        Returns:
            RSG.SourceControl.Perforce.Changelist object.
        """
        if not force:
            recordSet = self.getLocalPendingChangelists(longOutput=True)
            changes = self.convertP4RecordSetToList(recordSet)
            for change in changes:
                if description == change.get("desc", "").strip():
                    return self.getChangelist(change["change"])
        changelist = self._P4.CreatePendingChangelist(description)
        return self.getChangelist(changelist.Number)

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def getFilesInChangelist(self, changelistNumber):
        """
        Query all files in a changelist.

        Args:
            changelistNumber (int): The changelist number to query.

        Returns:
            List of files found in the changelist.
        """
        changelist = self.getChangelist(changelistNumber)
        if changelist:
            return list(changelist.Files)
        return None

    @assertConnectionAttempted
    def getDescription(self, changelistNumber):
        """
        Wrapper for the 'p4 describe' command.

        "p4 describe displays the details of one or more changelists. For each changelist, the output
        includes the changelist number, the changelist creator, the client workspace name, the date
        the changelist was created, and the changelist description."

        Args:
            changelistNumber (int): changelist number

        Returns:
            dict
        """
        results = self.run(const.Commands.DESCRIBE, [str(changelistNumber)])
        return self.convertP4RecordSetToDict(results)

    @assertConnectionAttempted
    def getUserData(self, user):
        """
        Gets information on the user based on the perforce user name.

        Args:
            user (str): perforce username to get data from

        Return:
            dict
        """
        results = self.run(const.Commands.USERS, [user])
        return self.convertP4RecordSetToDict(results)

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def deleteChangelist(self, changelistNum, deleteShelvedFiles=True):
        """
        Delete a changelist.

        Args:
            changelistNum(int): The changelist number to delete.

        Keyword Args:
            deleteShelvedFiles(boolean): Delete shelved files in the changelist.

        Returns:
            P4API.P4RecordSet object or None.
        """
        if deleteShelvedFiles:
            self.deleteShelvedFiles(changelistNum)

        return self.run(const.Commands.CHANGE, ["-d", str(changelistNum)])

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def revertChangelist(self, changelistNum, deleteShelvedFiles=False):
        """
        Revert all files in a changelist.

        Args:
            changelistNum (int): The changelist number to delete.

        Keyword Args:
            deleteShelvedFiles (boolean): Delete any shelved files in the changelist.

        Returns:
            P4API.P4RecordSet object or None.
        """
        if deleteShelvedFiles:
            self.deleteShelvedFiles(changelistNum)

        return self.run(const.Commands.REVERT, ["-c", str(changelistNum), "//..."])

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def deleteShelvedFiles(self, changelistNum):
        """
        Delete all shelved files in a changelist.

        Args:
            changelistNum (int): The changelist number to delete.

        Returns:
            P4API.P4RecordSet object or None.
        """
        return self.run(const.Commands.SHELVE, ["-c", str(changelistNum), "-d"])

    @assertConnectionAttempted
    def shelveChangelist(self, changelistNum):
        """
        Shelve all files in a changelist.

        Args:
            changelistNum (int): The changelist number.

        Returns:
            P4API.P4RecordSet object.
        """
        return self.run(const.Commands.SHELVE, ["-c", str(changelistNum), "-f"])

    @assertConnectionAttempted
    def shelve(self, filenames, changelistNum=None):
        """
        Shelve a list of files.

        Args:
            filenames (list[str]): The filenames to shelve.

        Keyword Args:
            changelistNum (int): The changelist number.

        Returns:
            P4API.P4RecordSet object.
        """
        if type(filenames) not in (types.TupleType, types.ListType):
            filenames = [filenames]

        args = [filename for filename in filenames]
        if changelistNum:
            args[0:0] = ["-c", str(changelistNum), "-f"]

        return self.run(const.Commands.SHELVE, args)

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def revert(self, filenames):
        """
        Revert a list of files.

        Args:
            filenames (list[str]): The filenames to revert.

        Returns:
            P4API.P4RecordSet object or None.
        """
        filenames = FixFilePaths(filenames)

        args = [filename for filename in filenames]

        return self.run(const.Commands.REVERT, args)

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def submit(self, changelistNum):
        """
        Submit a changelist.

        Args:
            changelistNum (int): The changelist number to submit.

        Returns:
            P4API.P4RecordSet object or None.
        """
        return self.run(const.Commands.SUBMIT, ["-c", str(changelistNum)])

    @p4DisabledReturnValue([])
    @assertConnectionAttempted
    def files(self, paths, excludeDeletedFiles=True):
        """
        Provide information about files in the depot without accessing their contents.

        Wraps the files perforce command:
        "This command lists each file that matches the file patterns provided as arguments. If a
        revision specifier is given, the files are described at the given revision.
        ...
        Unlike most Perforce commands, p4 files reports on any file in the depot; it is not limited
        to only those files that are visible through the client view. If a file pattern on the
        command line is given in client syntax, only files in the client workspace are shown."

        Args:
            paths (list): list of file paths to pass to the files command
            excludeDeletedFiles (bool): exclude files marked as deleted, purged, or archived

        Return:
            RecordSet or list
        """
        args = []
        if excludeDeletedFiles:
            args = ["-e"]
        args.extend(paths)
        return self.run(const.Commands.FILES, args)

    @p4DisabledReturnValue([])
    @assertConnectionAttempted
    def dirs(self, paths):
        """
        List the immediate subdirectories of specified depot directories.

        Wraps the dirs perforce command:
        "Use p4 dirs to find the immediate subdirectories of any depot directories provided as
        arguments. Any directory argument must be provided in depot or local syntax and must
        end with the * wildcard.
        ...
        By default, only subdirectories that contain at least one undeleted file will be returned.
        ...
        The "..." wildcard is not supported by p4 dirs.
        "

        Args:
            paths (list of str): list of directory paths formatted as "x:\dirname\*".

        Return:
            RecordSet or list
        """
        for path in paths:
            if not path.endswith("/*") and not path.endswith("\\*"):
                raise P4PathFormattingError(
                    "The 'p4 dirs' command requires all paths end with '/*' (depot) or '\\*' (local): '{}'".format(path)
                )
        return self.run(const.Commands.DIRS, paths)

    @p4DisabledReturnValue([])
    @assertConnectionAttempted
    def changes(self, paths):
        """
        Wraps the 'p4 changes' command with filtering to passing file paths.

        "Use p4 changes to view a list of submitted and pending changelists.
        ...
        (The default changelist is never listed.)
        ...
        If you provide file patterns as arguments, the changelists listed are those that affect
        files matching the patterns, whether submitted or pending."

        Args:
            paths (list): list of file paths to pass to the changes command

        Return:
            P4API.P4RecordSet or list
        """
        return self.run(const.Commands.CHANGES, paths)

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def getChangelists(self, changelistType=None, user=None, client=None, longOutput=False):
        """
        Wraps the 'p4 changelists' command with filtering based on type, user, client.

        "The command p4 changelists is an alias for p4 changes."

        Args:
            changelistType (str): The changelist type - choose option in const.ChangelistTypes.
                Default: None (no filtering based on changelist type).
            user (str): The P4 username.
                Default: None (no filtering based on user).
            client (str): The P4 client/workspace name.
                Default: None (no filtering based on client).
            longOutput(bool): list long output, with the full text of each changelist description.
                Default: False

        Returns:
            P4API.P4RecordSet or None.
        """
        args = []
        if changelistType is not None:
            args.extend(["-s", changelistType])
        if user is not None:
            args.extend(["-u", user])
        if client is not None:
            args.extend(["-c", client])
        if longOutput:
            args.append("-l")
        return self.run(const.Commands.CHANGELISTS, args)

    @p4DisabledReturnValue(None)
    @assertConnectionAttempted
    def getLocalPendingChangelists(self, longOutput=False):
        """
        List local, pending changelists.

        Args:
            longOutput(bool): list long output, with the full text of each changelist description.

        Returns:
            P4API.P4RecordSet or None.
        """
        # Safer to use P4 username just in case it differs from a system or ActiveDirectory username.
        user = self.user()
        client = self.client()
        return self.getChangelists(const.ChangelistTypes.PENDING, user, client, longOutput)


class P4ConnectionGame(P4Connection):
    """
    Setup and manage a connection to the Perforce server for the game data.

    Uses the singleton pattern (less obvious as to what's happening, but avoids a lot of overhead.)
    """

    __metaclass__ = singleton.Singleton

    def __init__(self, client=None, cwd=None):
        """
        Connect to server, giving the option to choose which workspace to use if multiple exist

        Notes:
            * Once you've run the tools installer, you're guaranteed to have a correct P4 config.

        Keyword Args:
            client (str):
            cwd (str): The current working directory; Default: If None, defaults to using Config.Project.Path.Root.
        """
        server = Config.User.P4ServerDefault
        displayName = const.DepotNames.GAME
        cwd = cwd or Config.Project.Path.Root  # Safe for use once the tools installer as been run.
        super(P4ConnectionGame, self).__init__(server, displayName, client=client, cwd=cwd)


class P4ConnectionMocap(P4Connection):
    """
    Setup and manage a connection to the Perforce server for the mocap data.

    Uses the singleton pattern (less obvious as to what's happening, but avoids a lot of overhead.)
    """

    __metaclass__ = singleton.Singleton

    def __init__(self, client=None, cwd=None):
        """
        Connect to server, giving the option to choose which workspace to use if multiple exist

        Keyword Args:
            client (str):
            cwd (str): The current working directory; Default: None
        """
        # ToDo: Get PerforceMocapServer (P4ServerMocap) added to the RSG.Configuration.Implementation GTA5 assembly
        # url:bugstar:7286108 - [Motionbuilder] [Config] Update the config.py for GTA5 to use RSG.Configuration 
        # for the User class and ensure that it gets P4ServerMocap
        # server = Config.User.P4ServerMocap
        
        # This is a temporary fix to ensure that P4 can connect to 1777
        server = "rsgperforce:1777"
        
        displayName = const.DepotNames.MOCAP
        super(P4ConnectionMocap, self).__init__(server, displayName, client=client, cwd=cwd)

    def _initClient(self):
        """
        Reimplemented from P4Connection to allow for querying and setting of the correct client.
        """
        super(P4ConnectionMocap, self)._initClient()

        # Get the default client and apply it.
        if self._client is None and Enabled() is True:
            defaultClient = self.getDefaultClient()
            self._client = defaultClient
            self._P4.Client = defaultClient

    def getDefaultClient(self):
        """
        Get the default workspace for the mocap depot.

        Designed to be run outside the context manager.

        Returns:
            str
        """
        try:
            self._connect()
            clients = self.userHostClients()
            if len(clients) == 1:
                return clients[0]
            elif len(clients) > 1:
                # Prefer client that matches the hostname
                for client in clients:
                    if client == self.host():
                        return client
                # otherwise just grab the first one in the list.
                return clients[0]
            else:
                return None  # No matches.
        except Exception as error:
            return None
        finally:
            self._disconnect()


class P4ConnectionDev(P4Connection):
    """
    Setup and manage a connection to the Perforce server for development tasks.

    Uses the singleton pattern (less obvious as to what's happening, but avoids a lot of overhead.)

    Warnings:
        * Only use this under the user: svcrsgnyctechart
        * Only instantiate and use as needed for testing - NOT FOR PRODUCTION USAGE.
        * The VM hosting this server has VERY limited storage space - use small or empty files!
    """

    __metaclass__ = singleton.Singleton

    def __init__(self, client=None, cwd=None):
        """
        Connect to server, giving the option to choose which workspace to use if multiple exist

        Keyword Args:
            client (str):
            cwd (str): The current working directory. Default: If None, defaults to using Config.Project.Path.Root.
        """
        server = const.Servers.DEVELOPMENT
        displayName = const.DepotNames.DEVELOPMENT
        cwd = cwd or Config.Project.Path.Root
        super(P4ConnectionGame, self).__init__(server, displayName, client=client, cwd=cwd)

    def defaultClient(self):
        """
        The default client to set for this server

        Returns:
            list
        """
        return "{}_UnitTest".format(socket.gethostname())


# Aliasing the connection singletons for more concise syntax (these are all context managers as well)
gameDepot = P4ConnectionGame()
mocapDepot = P4ConnectionMocap()
