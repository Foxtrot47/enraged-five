"""
Description:
    Helper functions for interfacing with Perforce through our .NET RSG.SourceControl.Perforce library.
"""
__DoNotReload__ = True  # Module is NOT safe to be dynamically reloaded

import os
import types
import datetime
import subprocess
import functools

from RS import Config
from RS._Perforce import servers, const
# Imports required to maintain API
from RS._Perforce.servers import (
    # CLR imports
    FileState,
    FileAction,
    Changelist,
    # Constants
    P4Settings,
    # Exceptions
    P4Exception,
    P4PathFormattingError,
    # Functions
    EnableExceptions,
    Enabled,
    SetEnabledUserPreference,
    FixFilePath,
    FixFilePaths,
    SwitchPathStyle,
    EnsureFileWritable,
    IsLatest,
    IsOpenForEdit,
    IsOpenForAdd,
    CanOpenForEdit,
)
_AssertFileState = servers.assertFileState


def DisposeConnection(control=None, event=None):
    """
    Destroys the current perforce client instance and cleans up references.
    """
    servers.gameDepot.disposeClient()


# Keeping this version around to avoid issues; could import from servers instead
def AssertConnectionEnabled(func):
    """
    Decorator used to throw an exception if the wrapped method is called while perforce integration is disabled.
    Use to ensure methods that would potentially cause problems if run while perforce integration is
    disabled cannot be called in that case.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if not Enabled():
            msg = ('Calling {}.{} is not allowed while Perforce integration is disabled.'
                   .format(func.__module__, func.__name__))
            raise P4Exception(msg)
        return func(*args, **kwargs)
    return wrapper


def Reconnect():
    """
    Disconnects and connects the perforce instance. This is to resolve network issues where
    the connection is invalid. This can be called after a long running process to ensure
    the connection did not time out.
    """
    with servers.gameDepot as depot:
        depot.reconnect()


def User():
    """
    Gets the name of the user using perforce

    Returns:
        str
    """
    with servers.gameDepot as depot:
        return depot.user()


def Client():
    """
    Gets the name of the client using perforce

    Returns:
        str
    """
    with servers.gameDepot as depot:
        return depot.client()


def Port():
    """
    Gets the port used by perforce

    Return:
        str
    """
    with servers.gameDepot as depot:
        return depot.port()


def Host():
    """
    Gets the host for perforce

    Return:
        str
    """
    with servers.gameDepot as depot:
        return depot.host()


def CreateConfig(directory, client=None, port=None, user=None):
    """
    Creates a .p4config file in the supplied directory.

    Arguments:

        directory (string): The directory to create the .p4config file.

    Keyword Arguments:

        client (string): The client workspace to set.
        port (string): The server and port to use.  Format would be: 'rsgsanp4p01:1999'
        user (string): The username to use.
    """
    if not os.path.isdir(directory):
        os.makedirs(directory)

    if not port:
        with servers.gameDepot as depot:
            port = depot.port()
    if not client:
        with servers.gameDepot as depot:
            client = depot.client()
    if not user:
        with servers.gameDepot as depot:
            user = depot.user()

    try:
        p4config = open('{0}\\.p4config'.format(directory), 'w')
        p4config.write('P4CLIENT={0}\n'.format(client))
        p4config.write('P4PORT={0}\n'.format(port))
        p4config.write('P4USER={0}\n'.format(user))

    except IOError, err:
        print 'Could not create the .p4config file due to the following error!\n', err

    finally:
        p4config.close()


def DoesFileExist(filePath):
    """
    Checks to see if the supplied filePath exists in Perforce.
    Also checks if the file has been marked for add locally

    Arguments:

        filePath (string): The filePath to check.

    Returns:

        True or False
    """
    with servers.gameDepot as depot:
        return depot.doesFileExist(filePath)


def Where(depotPaths):
    """
    Calls 'p4 where' with the depot paths as the input.

    Notes:
        * All 'illegal' characters are replaced with their escapes prior to passing on to 'p4 where'.
        * All input paths will have any trialing "/" characters removed.

    Arguments:
        depotPaths (list of str): The input depot paths.

    Returns:
        list of dict: A list of the p4 dict returned by 'p4 where'.
    """
    with servers.gameDepot as depot:
        return depot.where(depotPaths)


def GetLocalPaths(depotPaths):
    """
    Convenience wrapper around passing depot paths to 'p4 where' to get the local path under the current workspace.

    Works on file and directory paths.

    Arguments:
        depotPaths (list of str): The input depot paths.

    Returns:
        list of str: The local paths.
    """
    with servers.gameDepot as depot:
        return depot.getLocalPaths(depotPaths)


def GetClientRoot():
    """
    Get the client's root directory for the current workspace.

    Returns:
        str
    """
    with servers.gameDepot as depot:
        return depot.clientRoot()


def GetFileState(filenames):
    """
    Runs fstat on the supplied filenames.

    Arguments:

        filenames (list[string, ect.]): The file names to test.

    Returns:

        A list of RSG.SourceControl.Perforce.FileState objects.
    """
    if type(filenames) not in (types.TupleType, types.ListType):
        filenames = [filenames]

    with servers.gameDepot as depot:
        fileStates = depot.fileStates(filenames)

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def GetFileInfo(localFilePath, revision, key):
    """
    Gets information about the file from Perforce based on the revision number and key

    Arguments:

        localFilePath (string): local perforce file path to test
        revision (int): revision you want to check
        key (string): the type of information you wish to return. See the FileInfoTypes enum for
                      acceptable values.

    Returns:

        key result - could be int/string etc.
    """
    with servers.gameDepot as depot:
        return depot.getFileInfo(localFilePath, revision, key)


def GetAllMarkedDeleteDepotFiles(startingDirectory):
    """
    Gets ALL the files marked for delete within the supplied directory. This includes fi

    Arguments:

        startingDirectory (string): directory to inspect
        lResourceDirectory = "//depot/gta5/art/animation/resources/...fbx"

    Returns:
        list[string, etc.];  A list of depot file names.
    """
    with servers.gameDepot as depot:
        return depot.getAllMarkedDeleteDepotFiles(startingDirectory)


def GetAllMarkedAddFiles(startingDirectory):
    """
    Gets all the files marked for Add in the supplied directory

    Returns :

        A dict with clientFileName & depotFileName

    """
    with servers.gameDepot as depot:
        return depot.getAllMarkedAddFiles(startingDirectory)


def Add(filenames, changelistNum=None, force=False, fileType=None):
    """
    Adds the supplied filenames to Perforce.

    Note:

        This method should NOT escape symbols in the file paths (@, etc).
        The P4 Add command accepts characters normally ignored by the other P4 commands.
        Replacing the characters with their escapes would add a file to perforce with the escapes in
        the filename instead of the intended character.

    Warning:

        If a file has already been added, it will not get added to the changelist and NO error
        will be raised or message printed. This is a little odd considering that the command will
        print a warning message if run in Powershell.

    Arguments:

        filenames (string): The filenames to add.

    Keyword Arguments:

        changelistNum (int):
            Add files to the specified changelist.
        force (boolean):
            Indicates that the file should be force added.
        fileType (string):
            The file type that we want to add the file as. Should be string that matches
            one of the following list of fle types;
            http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html

    Returns:

        A single or list of RSG.SourceControl.Perforce.FileState objects that were added.
    """
    if type(filenames) not in (types.TupleType, types.ListType):
        filenames = [filenames]

    with servers.gameDepot as depot:
        fileStates = depot.add(filenames, changelistNum=changelistNum, force=force, fileType=fileType)

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def AddOrEditFiles(filenames, changelistNum=None, fileType=None, force=False):
    """
    Adds or Opens for edit the supplied filenames.

    Arguments:

        filenames (list[string, etc.]): The filenames to open for add or edit.

    Keyword Arguments:

        changelistNum (int):
            Added files or Open for edit the files under this changelist.
        fileType:
            The file type that we want to add the file as. Should be string that matches
            one of the following list of fle types;
            http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html
        force (bool):
            Indicates that the file should be force added.

    Returns:

        A single or list of RS.PyP4.FileState objects that were opened for edit or added.
    """
    vals = []
    filesAdded = Add(filenames, changelistNum=changelistNum, force=force, fileType=fileType)
    filesEdded = Edit(filenames, changelistNum=changelistNum, fileType=fileType)
    for record in [filesAdded, filesEdded]:
        if isinstance(record, list):
            vals.extend(record)
        else:
            vals.append(record)

    return vals


def Edit(filenames, changelistNum=None, fileType=None, forceWritable=True):
    """
    Opens for edit the supplied filenames.

    Arguments:

        filenames (list[string, etc.]): The filenames to open for edit.

    Keyword Arguments:

        changelistNum (int):
            Open for edit the files under this changelist.
        fileType:
            The file type that we want to add the file as. Should be string that matches
            one of the following list of fle types;
            http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html
        forceWritable (bool):
            If True, the file will have write permissions opened on it.

    Returns:

        A single or list of RSG.SourceControl.Perforce.FileState objects that were opened for edit.
    """
    if isinstance(filenames, basestring):
        filenames = [filenames]

    with servers.gameDepot as depot:
        fileStates = depot.edit(filenames, changelistNum=changelistNum, fileType=fileType, forceWritable=forceWritable)

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def Sync(filenames, force=False, revision=None):
    """
    Syncs the supplied filename(s).

    Arguments:

        filenames (list[string, etc.]): The filename(s) to sync.

    Keyword Arguments:

        force (boolean): Whether or not to force the sync.

    Returns:

        A single or list of RSG.SourceControl.Perforce.FileState objects that were synced.
    """
    with servers.gameDepot as depot:
        fileStates = depot.sync(filenames, force=force, revision=revision)

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def GetChangelist(changelistNum):
    """
    Query a changelist.

    Arguments:

        changelistNum(int): The changelist number to query.

    Returns:

        RSG.SourceControl.Perforce.Changelist object.  None if not found.
    """
    with servers.gameDepot as depot:
        return depot.getChangelist(changelistNum)


def CreateChangelist(description):
    """
    Creates a new changelist.

    Arguments:

        description (string): The changelist desription.

    Returns:

        RSG.SourceControl.Perforce.Changelist object.
    """
    with servers.gameDepot as depot:
        return depot.createChangelist(description)


def GetFilesInChangelist(changelistNum):
    """
    Query all files in a changelist.

    Arguments:

        changelistNum (int): The changelist number to query.

    Returns:

        List of files found in the changelist.
    """
    with servers.gameDepot as depot:
        return depot.getFilesInChangelist(changelistNum)


@AssertConnectionEnabled
def GetDescription(changelistNumber):
    """
    Gets the description from the given changelist. The results are returned in a normal python dictionary

    Arguments:
        changelistNumber (int): changelist number

    Returns:
        dictionary
    """
    with servers.gameDepot as depot:
        return depot.getDescription(changelistNumber)


@AssertConnectionEnabled
def GetUserData(user):
    """
    Gets information on the user based on the perforce user name. The results are returned in a normal python dictionary

    Arguments:
        user (string): perforce username to get data from

    Return:
        dictionary
    """
    with servers.gameDepot as depot:
        return depot.getUserData(user)


def ConvertRecordSetToDictionary(recordSet):
    """
    Convert C# P4API.P4RecordSet object to a single dictionary.

    Warnings:
        * Reduces a P4API.P4RecordSet with a single P4Record to a dict; DO NOT USE on a
        P4RecordSet that may have multiple P4Records - use servers.ConversionsMixin.convertP4RecordSetToList instead.

    Args:
        recordSet (P4API.P4RecordSet): P4API.P4RecordSet C# object.

    Returns:
        dict
    """
    return servers.ConversionsMixin.convertP4RecordSetToDict(recordSet)


def DeleteChangelist(changelistNum, deleteShelvedFiles=True):
    """
    Delete a changelist.

    Arguments:

        changelistNum(int): The changelist number to delete.

    Keyword Arguments:

        deleteShelvedFiles(boolean): Delete shelved files in the changelist.

    Returns:

        P4API.P4RecordSet object or None.
    """
    with servers.gameDepot as depot:
        return depot.deleteChangelist(changelistNum, deleteShelvedFiles=deleteShelvedFiles)


def RevertChangelist(changelistNum, deleteShelvedFiles=False):
    """
    Revert all files in a changelist.

    Arguments:

        changelistNum (int): The changelist number to delete.

    Keyword Arguments:

        deleteShelvedFiles (boolean): Delete any shelved files in the changelist.

    Returns:

        P4API.P4RecordSet object or None.
    """
    with servers.gameDepot as depot:
        return depot.revertChangelist(changelistNum, deleteShelvedFiles=deleteShelvedFiles)


def DeleteShelvedFiles(changelistNum):
    """
    Delete all shelved files in a changelist.

    Arguments:

        changelistNum (int): The changelist number to delete.

    Returns:

        P4API.P4RecordSet object or None.
    """
    with servers.gameDepot as depot:
        return depot.deleteShelvedFiles(changelistNum)


@AssertConnectionEnabled
def ShelveChangelist(changelistNum):
    """
    Shelve all files in a changelist.

    Arguments:

        changelistNum (int): The changelist number.

    Returns:

        P4API.P4RecordSet object.
    """
    with servers.gameDepot as depot:
        return depot.shelveChangelist(changelistNum)


@AssertConnectionEnabled
def Shelve(filenames, changelistNum=None):
    """
    Shelve a list of files.

    Arguments:

        filenames (list[string, etc.]): The filenames to shelve.

    Keyword Arguments:

        changelistNum (int): The changelist number.

    Returns:

        P4API.P4RecordSet object.
    """
    with servers.gameDepot as depot:
        return depot.shelve(filenames, changelistNum=changelistNum)


def Revert(filenames):
    """
    Revert a list of files.

    Arguments:

        filenames (list[string, etc.]): The filenames to revert.

    Returns:

        P4API.P4RecordSet object or None.
    """
    with servers.gameDepot as depot:
        return depot.revert(filenames)


def Submit(changelistNum):
    """
    Submit a changelist.

    Arguments:

        changelistNum (int): The changelist number to submit.

    Returns:

        P4API.P4RecordSet object or None.
    """
    with servers.gameDepot as depot:
        return depot.submit(changelistNum)


@AssertConnectionEnabled
def Run(p4Cmd, args=None):
    """
    Native P4 Run command.

    Arguments:

        p4Cmd (str): valid p4 cmd
        args (list[str, etc.]):  list of paths and/or perforce arguments

    Returns:

        P4 Record or error message
    """
    with servers.gameDepot as depot:
        return depot.run(p4Cmd, args=args)


def Files(paths, excludeDeletedFiles=True):
    """
    Wraps the files perforce command

    Arguments:
        paths (list of str): list of file paths to pass to the files command
        excludeDeletedFiles (bool): exclude files marked as deleted on depot

    Return:
        RecordSet or list
    """
    with servers.gameDepot as depot:
        return depot.files(paths, excludeDeletedFiles=excludeDeletedFiles)


@servers.p4DisabledReturnValue([])
@AssertConnectionEnabled
def Dirs(paths):
    """
    Wraps the dirs perforce command.

    Notes:
        * Tools expect this method to fail silently when they pass in incorrectly formatted paths.

    Arguments:
        paths (list of str): list of file paths to pass to the dirs command

    Return:
        RecordSet or list
    """
    with servers.gameDepot as depot:
        # Must replicate P4Connection.dirs without the extra validation in order to maintain api compatibility.
        return depot.run(const.Commands.DIRS, paths)


def Changes(paths):
    """
    Wraps the dirs perforce command

    Arguments:
        paths (list): list of file paths to pass to the changes command

    Return:
        RecordSet or list
    """
    with servers.gameDepot as depot:
        return depot.changes(paths)


def ConvertPerforceSecondsToDate(seconds):
    """
    Converts the seconds returned by perforce to a date and time

    Arguments:
        seconds (int): number of seconds grabbed from the perforce server

    Return:
        string
    """
    return str(datetime.datetime(1970, 1, 1) + datetime.timedelta(0, int(seconds)))


def Connected():
    """
    Can a connection to Perforce be made?

    Returns:
        bool: True if a connection can be made; False if not.
    """
    if not Enabled():
        return False

    with servers.gameDepot as depot:
        return depot.isConnectionAttempted()


# TODO Scope to a specific p4 server
class SyncOperation(object):
    """
    Perforce p4.exe command line
    """
    def __init__(self, fileList, start=False, force=False, revisionList=None):
        """
        Constructor
        """

        self.p4root = Config.Project.Path.Root
        self.fileList = FixFilePaths(fileList)
        self.revisionList = revisionList if revisionList and len(revisionList) == len(self.fileList) else None
        self.force = force
        self.proc = None

        if start:
            self.Start()

    def Start(self):
        """
        Sync Operation
        """
        if not Enabled():
            return

        # send a request for a sync to perforce
        fileListString = ""
        for i in range(len(self.fileList)):
            if self.revisionList:
                fileListString = fileListString + ("\"{0}#{1}\" ").format(self.fileList[i], self.revisionList[i])
            else:
                fileListString = fileListString + ("\"{0}\" ").format(self.fileList[i])

        if self.force:
            commandText = ('p4 sync -f {0} ').format(fileListString)
        else:
            commandText = ('p4 sync {0} ').format(fileListString)

        # Remember the original working directory
        originaDir = Config.Project.Path.Root

        # Change it to the current working one so that we can use the p4 command
        os.chdir(self.p4root)
        try:
            self.proc = subprocess.Popen(commandText, shell=True, stdout=subprocess.PIPE)
        except:
            self.proc = subprocess.Popen(commandText, shell=True, stdin=subprocess.PIPE, stderr=subprocess.PIPE,
                                         stdout=subprocess.PIPE)
            self.proc.stdin.close()
            self.proc.stderr.close()
        finally:
            # Change it back to the original directory
            os.chdir(originaDir)

    def WaitForCompletion(self):
        """
        Wait for process to finish
        """
        if not self.proc:
            return

        self.proc.wait()


def GetChangelists(changelistType=None, user=None, client=None):
    """
    List local changelists.

    Returns:

        P4API.P4RecordSet or None.
    """
    with servers.gameDepot as depot:
        return depot.getChangelists(changelistType=changelistType, user=user, client=client)


def GetLocalPendingChangelists():
    """
    List local, pending changelists.

    Returns:

        P4API.P4RecordSet or None.
    """
    with servers.gameDepot as depot:
        return depot.getLocalPendingChangelists()
