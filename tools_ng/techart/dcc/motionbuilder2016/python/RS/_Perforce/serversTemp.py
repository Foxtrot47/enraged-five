"""Classes for running commands on specific perforce servers/depots."""
import os
import socket

from collections import OrderedDict
from RS._Perforce import constTemp as const, core, exceptions
from RS._Perforce.decorators import path


class Singleton(type):
    """Singleton Class Object"""
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class BaseServer(object):
    """The base class for holding a the information for connecting to a server as well as utility methods for P4."""

    NAME = None
    IS_CASE_SENSITIVE = False

    def __init__(self):
        """Constructor."""
        self.instance = core.RSP4(self.NAME)
        self.instance.isCaseSensitive = self.IS_CASE_SENSITIVE

    @property
    def P4(self):
        """The P4 connection instance.

        Also connects to P4 and grabs the default or last client used by the machine.
        A client ,a.k.a workspace, is required for performing P4 actions that involves accessing a file or directory.

        Returns:
            rockstar.core.perforce.core.Core
        """
        if not self.instance.isConnected():
            self.instance.connect()  # connect to the server
            if not self.instance.isClientSet():
                client = self.instance.getLastAccessedClient(self.defaultClients())  # get a valid workspace
                self.instance.setClient(client)  # set this workspace for the connection
        return self.instance

    @property
    def account(self):
        """Account that is being used to authenticate against the server.

        Returns:
            rockstar.core.accounts.AbstractAccount
        """
        return self.instance.account

    def promptUser(self):
        """Returns if the user should be prompted with an UI option when certain errors occur.

        This wrapper is so you don't have to use the P4 property, which runs P4 commands to connect to the server.

        Returns:
            bool
        """
        return self.instance.promptUser()

    def setPromptUser(self, value):
        """Sets if the user should be prompted with UI options when certain errors occur.

        This wrapper is so you don't have to use the P4 property, which runs P4 commands to connect to the server.

        Args:
            value (bool): should password and unloaded workspaces prompt the user for feedback.
        """
        self.instance.setPromptUser(value)

    def toDateTime(self, p4TimeValue):
        """Converts the P4 Int time to an Datetime.

        This wrapper is so you don't have to use the P4 property, which runs P4 commands to connect to the server.

        Args:
            p4TimeValue (int): int value representing time from P4

        Return:
            datetime.datetime
        """
        return self.instance.toDateTime(p4TimeValue)

    def toHours(self, seconds):
        """converts seconds to hours.

        This wrapper is so you don't have to use the P4 property, which runs P4 commands to connect to the server.

        Args:
            seconds (int): seconds to convert to hours
        """
        return self.instance.toHours(seconds)

    def exists(self, filePath, includeDeleted=False):
        """Test if the file path exists on the server.

        Args:
            filePath (str): Path to file.

            includeDeleted (bool, optional): If True, a deleted file will count as an existing file.

        Returns:
            bool: True if exists, False if not.
        """
        try:
            fileState = self.P4.fileState(filePath)
            if includeDeleted:
                return fileState.isDeleted  # If the file was never in p4, it will return False.
            return fileState.exists
        except Exception:
            return False

    def dirExists(self, directoryPath):
        """Test if the directory path exists on the server.

        Args:
            directoryPath (str): Path to directory - can be client or depot path.

        Returns:
            bool: True if exists, False if not.
        """
        # The where command will raise an error if a "null directory" is passed in; the paths listed in the P4.dirs result
        # have trailing slashes stripped, so we can't match a directoryPath with a trailing slash to that list.
        directoryPath = directoryPath.rstrip("/")

        # Get a list of depot paths from in the input directory.
        depotPaths = [directoryPath]
        if not directoryPath.startswith("//"):
            # P4 where will give back one result per client mapping. Since we can't know which one we need, query with
            # all the returned depot paths. The correct dir will append valid paths to the results list and the incorrect
            # one(s) won't raise an error, nor append invalid paths to the results list.
            results = self.P4.where(directoryPath)
            depotPaths = [p4Object.get("depotFile") for p4Object in results]

        # Use the dirs command on the parent dir to get a list of sub-dirs and see if the input dir exists in that list.
        queryPaths = ["{}/*".format(os.path.dirname(depotPath)) for depotPath in depotPaths]
        results = self.P4.run(const.Commands.DIRS, queryPaths)
        for depotPath in depotPaths:
            for entry in results:
                if depotPath == entry.get("dir"):
                    return True
        return False

    def doesExist(self, filename):
        """Deprecated Method; use exists instead."""
        return self.exists(filename)

    @path.cleanPaths
    def isLatest(self, path):
        """Is the file the latest available version from the server.

        Args:
            path (str): path to file

        Returns:
            bool
        """
        filestate = self.P4.fileState(path)
        return filestate.isLatestRevision

    @path.cleanPaths
    def isReadOnly(self, path):
        """Is this file on disk read-only.

        Args:
            path (str): path that is on disk to check if it is read only or not.
        Returns:
            bool
        """
        return not os.access(path, os.W_OK)

    @path.cleanPaths
    def isLocal(self, path):
        """Checks if a file is local to the workspace. Could be marked for add or not.

        Args:
            filePath (str): a file path.

        Returns:
            bool
        """
        p4FilePath = self.P4.convertPath(path)
        fileState = self.P4.fileState(p4FilePath, flags=["action", "depotFile"])
        return fileState.isLocal

    def isOpenForEdit(self, filename):
        """Is the file open for editing.

        Args:
            filename (str): path to file

        Returns:
            bool
        """
        result = self.P4.fileState(filename)
        return result.openAction != const.ActionTypes.UNKNOWN

    def isCheckedOutByOther(self, filename):
        """Is the the file checked out by someone else.

        Notes:
            When someone has the file checked out, the function returns a tuple whose first value is true and the second
            a list of all the other users that have it checked out.

        Args:
            filename (str): path to file

        Returns:
            bool or tuple
        """
        filestate = self.P4.fileState(filename)
        numOpened = filestate.otherOpen or 0
        if numOpened > 0:
            openBy = filestate.otherOpen or "<unknown>"
            otherUser = openBy.split("@")[0]
            return True, otherUser
        return False

    def isMarkedForDelete(self, filename):
        """Is the the file marked for delete.

        Args:
            filename (str): path to file

        Returns:
            bool
        """
        filestate = self.P4.fileState(filename)
        return filestate.openAction in (const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE)

    def isMarkedForAdd(self, filename):
        """Is the the file marked for add.

        Args:
            filename (str): path to file

        Returns:
            bool
        """
        filestate = self.P4.fileState(filename)
        return filestate.openAction == const.ActionTypes.ADD

    def addOrEditFiles(self, filenames, changelist=None, fileType=None, force=False):
        """opens files for add or edit if they aren't already opened.

        Args:
            filenames (list): list of file paths to add or edit.

            changelist (int, optional): The changelist number.
            fileType (str, optional): add the files as the specified filetype.
            force (bool, optional): if the operation must be forced.

        Returns:
            list of dict: dictionaries of files that have been successfully opened for add and/or edit
        """
        files = self.P4.add(filenames, changelist=changelist, force=force, fileType=fileType)
        files.extend(self.P4.edit(filenames, changelist=changelist, fileType=fileType))
        return filter(None, files)

    @path.cleanPaths
    def moveFiles(self, paths, changelist=None):
        """Moves every file in depotFilesPath.
        
        Args:
            paths (dict): dictionary made of pairs <oldPath, newPath>.

            changelist (int, optional): The changelist number.

        Returns:
            dict: move result.
        """
        return self.P4.move(paths, changelist=changelist)

    @path.cleanPaths
    def getOutOfSyncFilesInFolder(self, path, orderedResult=False):
        """Checks all the files in a depot folder and returns the ones who are out of sync.

        Args:
            path (str): a depot path.

            orderedResult (bool): returns the results in the order that they were returned by P4

        Returns:
            dict or OrderedDict: a dictionary containing pairs of <depotPath, state>.
        """
        if orderedResult:
            outOfSyncFiles = OrderedDict()
        else:
            outOfSyncFiles = {}
        p4FolderPath = "{}/*".format(path)
        folderFilesState = self.P4.folderFileStates(
            p4FolderPath, flags=["-T", "depotFile, haveRev, headRev, headAction"], filter="^headAction = delete"
        )
        for state in folderFilesState:
            haveRev = state.get("haveRev")
            headRev = state.get("headRev")
            if haveRev < headRev and state.get("headAction") != "delete":
                outOfSyncFiles[state.get("depotFile")] = state
        return outOfSyncFiles

    def getLocalPendingChangelists(self):
        """Gets the local pending changelists.

        Returns:
            list
        """
        return self.P4.changelists(status=const.ChangelistTypes.PENDING)

    def filesInChangelist(self, changelist, excludeByAction=None):
        """Gets list of files in the changelist.

        When used on an unsubmitted changelist, it returns the current contents of the changelist

        Excluding files marked for delete & move delete on a unsubmitted changelist effectively returns a list of files
        on disk that are in that changelist.

        Args:
            changelist (str): number of the changelist

            excludeByAction (list, optional): list of perforce actions to used to filter out files by from the list

        Return:
            list
        """
        paths = []
        excludeByAction = excludeByAction or []
        results = self.P4.describe(changelist)
        if results:
            p4Dict = results[0]
            paths = [
                path
                for path, action in zip(p4Dict.get("depotFile", []), p4Dict.get("action", []))
                if action not in excludeByAction
            ]
        return paths

    def shelvedFiles(self, changelist, all=False):
        """Get a dictionary of the files that are in a shelved changelist and the p4 action being performed on them.

        Args:
            changelist (str): changelist number to get shelved files from

            all (bool, optional): If we want to get all the files in changelist even the ones that need to be reverted before unshelving

        Return:
            dict
        """
        if all:
            p4Dicts = self.P4.describe(changelist, short=True, shelved=True)

            for p4Dict in p4Dicts:
                # Add this to the dict comprehension.
                if "depotFile" and "action" in p4Dict:
                    return {p4Dict["depotFile"][idx]: p4Dict["action"][idx] for idx in xrange(len(p4Dict["depotFile"]))}

        else:
            return {
                p4Dict["depotFile"]: p4Dict["action"]
                for p4Dict in self.P4.run(const.Commands.UNSHELVE, ["-n", "-s", changelist])
                if isinstance(p4Dict, dict)
            }

    def removeFromWorkspace(self, filenames):
        """Removes files from the workspace by syncing them to revision 0.

        Args:
            filenames (list): list of filenames to remove from the local disk
        """
        self.P4.revert(filenames)
        self.P4.sync(filenames, revision=0, force=True)

    # TODO: Replace with server.P4.clients
    def getClients(self):
        """Gets a list of clients for this machine based on the name of the clients.

        Returns:
            list
        """
        return [
            client for client in self.P4.run(const.Commands.CLIENTS, ["-e", "*{}_*".format(socket.gethostname())])  # Modified to remove rockstar.config
        ]

    def unshelveFiles(self, changelist, force=False):
        """Unshelves the changelist to the new changelist in the current workspace.

        The number returned is the changelist number that was unshelved.

        Args:
            changelist (int): number of changelist to unshelve the files from

            force (bool, optinal): Force the unshelve, by reverting the files that are in conflict

        Returns:
            int
        """
        shelvedFiles = self.shelvedFiles(changelist, all=True)
        unshelveChangeList = self.P4.changelist('Unshelved from pending changelist "{}"'.format(changelist), None)

        if force:
            self.P4.revert(shelvedFiles.keys())
        self.P4.unshelve(changelist, changelist=unshelveChangeList, filenames=shelvedFiles.keys(), force=force)

        return unshelveChangeList

    def revertChangelist(self, changelist, force=True):
        """Reverts all the files that are in the changelist.

        If the files were set to be added to the workspace,
        then removes them from the file system and deletes the changelist.

        Args:
            changelist (int): number of the changelist that was unshelved

            force (bool, optional): Forces the removal of the files from the system if files were marked for add.

        Returns:
            dict
        """
        changelistFiles = self.filesInChangelist(changelist)
        success = self.P4.revert(
            changelistFiles,
            changelist=changelist,
            unchanged=False,
            delete=force,
        )
        if success:
            self.P4.deleteShelve(changelist)
            self.P4.deleteChangelist(changelist)
            return True

    def fileState(self, path):
        """Returns the filestate of a given path.

        Args:
            path (str): path to the file whose perforce stats should be queried.
        Returns:
            rockstar.core.perforce.core.FileState
        """
        return self.P4.fileState(path)

    def getAvailableProjects(self):
        """Virtual Method.

        Get the projects available to the current user based on which directories they have access
        to under techart
        """
        raise NotImplementedError

    def defaultClients(self):
        """The default clients to user/set for this server.

        Returns:
            list
        """
        return []

    def getClientPath(self, depotPath):
        """Get the exact filesystem path of the file.

        We can also use .where() for this, but it can require additional filtering if the user has multiple mappings
        for the same directories in their workspace.

        Args:
            depotPath (str): Path to the file in p4.

        Raises:
            exceptions.PathNotInPerforce: If the depot path cannot be found in the depot.

        Returns:
            str: Local path on disk.
        """
        fileState = self.fileState(depotPath)
        if fileState.exists is False:
            raise exceptions.PathNotInPerforce(depotPath, self.P4.server())
        return fileState.clientFilename

    def getDepotPath(self, clientPath, checkFileExists=True):
        """Get the depot path of the file.

        Warnings:
            Only designed for file paths, NOT folder paths.

        Args:
            clientPath (str): Path to the file on disk.
            checkFileExists (bool, optional): If True, will check if the path exists on the server. Default is True.

        Returns:
            str: Depot path in p4.
        """
        if checkFileExists:
            fileState = self.fileState(clientPath)
            if fileState.exists is False:
                raise exceptions.PathNotInPerforce(clientPath, self.P4.server())
            return fileState.depotFilename
        else:
            # Technically, this will work on folder paths.
            return self.P4.where(clientPath)[0]["depotFile"]


class Game(BaseServer):
    """Class for connecting to the game server."""

    __metaclass__ = Singleton
    NAME = const.Servers.GAME

    def getAvailableProjects(self):
        """Overrides inherited function.

        Get the projects available to the current user based on which directories they have access to under techart.
        """
        # os.path.split does not work as expected when the slashes being split don't match the expected separators
        # for the os.
        directories = self.P4.run(const.Commands.DIRS, ["//techart/*"])
        return [directory["dir"].rsplit("/")[-1] for directory in directories if "development" not in directory["dir"]]

    def defaultClients(self):
        """Overrides inherited function.

        The default clients to look for when accessing this server.
        """
#         if config.user.system.isLinux():  # Modified to remove rockstar.config
#             return ["{}_north".format(config.user.system.host)]
        return []


class Mocap(BaseServer):
    """Class for accessing the development server."""

    __metaclass__ = Singleton
    NAME = const.Servers.MOCAP
    IS_CASE_SENSITIVE = True

    def defaultClients(self):
        """The default client to set for this server.

        Returns:
            list
        """
#         if config.user.system.isLinux():  # Modified to remove rockstar.config
#             return [config.user.system.host]
        return []


class Development(BaseServer):
    """Class for accessing the development server.

    Notes:
        This server has limited access, thus the default account for accessing it is the basic techart service account.
        Please use the unittests to create the workspace for using with this server and access it in P4V via this account.
    """

    __metaclass__ = Singleton
    NAME = const.Servers.DEVELOPMENT

    def __init__(self):
        """Constructor."""
        super(Development, self).__init__()
#         self.instance.account = accounts.TechArt()  # Modified to remove rockstar.account

    def defaultClients(self):
        """The default client to set for this server.

        Returns:
            list
        """
        return ["{}_UnitTest".format(socket.gethostname())]  # Modified to remove rockstar.config


def getByName(name):
    """Gets the server based on the name.

    Args:
        name (str): name of the server to get

    Returns:
        rockstar.core.perforce.servers.BaseServer
    """
    for cls in BaseServer.__subclasses__():
        if cls.NAME == name:
            return cls()
    return None


def canSwitchToGameServer():
    """Can the current account set for accessing the game server connect to it."""
    return Game().P4.canConnect()


def canSwitchToMocapServer():
    """Can the current account set for accessing the mocap server connect to it."""
    return Mocap().P4.canConnect()


def canSwitchToDevelopmentServer():
    """Can the current account set for accessing the development server connect to it."""
    return Development().P4.canConnect()


# Singletons
mocapDepot = Mocap()
gameDepot = Game()
developmentDepot = Development()
