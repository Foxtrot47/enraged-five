import unittest

import yaml

from RS import ProjectData


def loadYaml(path):
    """Loads yaml file's data

    Args:
        path (str): Yaml path to load

    Returns:
        dict: Yaml parsed file contents
    """
    with open(path, "r") as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            data = None
            print(exc)
    return data


def getConfigKey(value, config):
    """Tries getting key from config and grabbing default if not project specific

    Args:
        value (str): Value to query
        config (dict): Dictionary to query value

    Returns:
        dict: Key value
    """
    PROJECT_NAME = ProjectData.ProjectDataManager().KEY

    data = config.get(PROJECT_NAME, {}).get(value, None)
    if not data:
        data = config.get("default", {}).get(value, None)
    return data


def getSuite(module, functionName, className=None):
    """Get a test suite from the given module with UnitTests

    Args:
        module (module): module to search for unittest classes
        functionName (str): name of the function to load
        className (str, optional): name of the class to load. Loads all classes if None is passed

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()
    if className:
        _class = getattr(module, className)
        suite.addTest(_class(functionName))
    else:
        for content in dir(module):
            if content.startswith("Test_"):
                _class = getattr(module, content)
                suite.addTest(_class(functionName))
    return suite
