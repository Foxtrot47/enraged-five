"""
Module for working with Rex exporter commands.

This module allows us to talk with the exporter to set various options and export anim/cutscenes

Python hooks are defined in the below c++ code:
X:\gta5\src\dev_ng\rage\suite\tools\dcc\motionbuilder\rexMBRage\entry.cpp

Also see the related Rockstar Hub page below:
https://hub.rockstargames.com/pages/viewpage.action?spaceKey=RSGTOOLS&title=Cutscene+Exporter+Python+Hooks

Usage:
from RS.Core.Export import *
RexExport.[command]

Author: Mark Harrison-Ball <mark.harrison-Ball@rockstargames.com>
"""

import pyfbsdk as mobu
from ctypes import *

class RexExport(object):
    """
    We Open and close the Exporter so that it gets initialized when the module is first imported.
    """
    def __init__( self ):
        self.InitializeCutscene()

    def ShowToolByName(self, name):
        mobu.ShowToolByName(name)
        mobu.CloseToolByName(name)

    # [2015 HACK]
    def InitializeCutscene( self ):
        """
        Initialize our Cutscene Exporter
        """
        if not self.IsCutsceneExporterInitialized(  ) :
            self.ShowToolByName("Rex Rage Cut-Scene Export")

    #Python wrappers matching Rex names

    def IsCutsceneExporterInitialized( self ):
        """
        Checks if the static instance of the cutscene exporter is valid. (Exporter needs to be opened once)
        Returns (int)
        """
        return (cdll.rexMBRage.IsCutsceneExporterInitialized_Py() != 0)

    def CutsceneExporterInitialize( self ):
        """
        Initialize our Exporter
        """
        derp = mobu.FBSystem()
        if derp.Version == 15000.0:
            return self.InitializeCutscene()

        return (cdll.rexMBRage.CutsceneExporterInitialize_Py() != 0)

    def GetCutsceneSceneName( self ):
        """
        Gets the current scene name
        Returns (string)
        """
        cdll.rexMBRage.GetCutsceneSceneName_Py.restype = c_char_p
        return cdll.rexMBRage.GetCutsceneSceneName_Py()

    def GetCutsceneExportPath( self ):
        """
        Gets the current scene's export directory
        Returns (string)
        """
        cdll.rexMBRage.GetCutsceneExportPath_Py.restype = c_char_p
        return cdll.rexMBRage.GetCutsceneExportPath_Py()

    def GetCutsceneFaceExportPath(self):
        """
        Gets the current scene's export face directory
        Returns (string)
        """
        cdll.rexMBRage.GetCutsceneFaceExportPath_Py.restype = c_char_p
        return cdll.rexMBRage.GetCutsceneFaceExportPath_Py()

    def GetNumberCutsceneShots( self ):
        """
        Gets number of shots in exporter
        Return (int)
        """
        return cdll.rexMBRage.GetNumberCutsceneShots_Py()

    def GetCutsceneShotName( self, index ):
        """
        Gets shot name by index
        Arguments:
            index: Int of shot to get name from
        Returns (string)
        """
        return c_char_p(cdll.rexMBRage.GetCutsceneShotName_Py( index )).value

    def GetCutsceneShotStartRange( self, index ):
        """
        Gets shot Start range by index
        Arguments:
            index: Int of shot to get start range from
        Returns (int)
        """
        return cdll.rexMBRage.GetCutsceneShotStartRange_Py( index )

    def GetCutsceneShotEndRange( self, index ):
        """
        Gets shot end range by index
        Arguments:
            index: Int of shot to get end range from
        Returns (int)
        """
        return cdll.rexMBRage.GetCutsceneShotEndRange_Py( index )

    def SetCutsceneShotStartRange( self, index, start_range ):
        """
        Sets shot start range by index
        Arguments:
            index: Int of shot to set start range from
            start_range: Int of start range to set
        """
        cdll.rexMBRage.SetCutsceneShotStartRange_Py( index, start_range)

    def SetCutsceneShotEndRange( self, index, end_range ):
        """
        Sets shot end range by index
        Arguments:
            index: Int of shot to set end range from
            end_range: Int of end range to set
        """
        cdll.rexMBRage.SetCutsceneShotEndRange_Py( index, end_range)

    def SetCutsceneStoryMode( self, enable_Storymode ):
        """
        Sets story mode status on all shots
        Arguments:
            enable_Storymode: Bool - true to enable, false to disable
        """
        cdll.rexMBRage.SetCutsceneStoryMode_Py( enable_Storymode )

    def SetCutsceneShotUseCatchupCamera(self, enable_catchupcam):
        """
        Sets catch_cam

        Arguments:
        """
        cdll.rexMBRage.SetCutsceneShotCatchupCamera_Py( 0, enable_catchupcam )

    def SetCutsceneShotFadeInCutscene(self, index, enabled, duration=0.8):
        """
        Sets Fade in

        Arguments:
        """
        cdll.rexMBRage.SetCutsceneShotFadeInCutscene_Py(index, enabled, c_float(duration))

    def SetCutsceneShotFadeOutCutscene(self, index, enabled, duration=0.8):
        """
        Sets Fade out

        Arguments:
        """
        cdll.rexMBRage.SetCutsceneShotFadeOutCutscene_Py(index, enabled, c_float(duration))

    def SetCutsceneShotDisableDOFLastCut( self, index, disable_DOF):
        """
        Sets shot disable dof status
    
        Arguments:
        """

        cdll.rexMBRage.SetCutsceneShotDisableDOF_Py(index, disable_DOF)

    def SetCutsceneShotEnableDOFFirstCut( self, index, enable_DOF):
        """
        Sets shot enable dof on 1st cut status.
    
        Argumnets:
    
        """

        return cdll.rexMBRage.SetCutsceneShotEnableDOFFirstCut_Py(index, enable_DOF)

    # Below function doesn't work...
    def SetCutsceneShotEnableDOFSecondCut(self, index, enabled):
        """
        :param index: int
        :param enabled: boolean
        :return: None
        """
        return cdll.rexMBRage.SetCutsceneShotEnableDOFSecondCut_Py(index, enabled)




    def SetCutsceneShotExportStatus( self, index, status):
        """
        Sets the shots export status.

        Argumnets:

        (Shot Index)
        (Enable Export)
        """
        cdll.rexMBRage.SetCutsceneShotExportStatus_Py(index, status)



    def ClearCutsceneShots( self ):
        """
        Clear all cutscene shots
        """
        cdll.rexMBRage.ClearCutsceneShots_Py()


    def AddCutsceneShot( self, name ):
        """
        Adds new shot with specified name.

        Arguments:
        (Name of Sot)
        """
        cdll.rexMBRage.AddCutsceneShot_Py(name)

    def SetCutsceneConcatMode(self, enabled=True ):
        """
        Enable/Disable Concat mode

        Arguments:
        Enable

        """

        return (cdll.rexMBRage.SetCutsceneConcatMode_Py(enabled) !=0)



    def SetCutsceneShotCatchupCamera(self, index, enabled=True):
        """
        Enable the Catchup camera on a shot.

        Arguments:

        (Shot Index)
        (Enable Catchup Camera)
        """
        return cdll.rexMBRage.SetCutsceneShotCatchupCamera_Py(index, enabled)


    def SetCutsceneShotBlendOut(self, index, enabled=True, duration=30, offset=0):
        """
        Enable cutscene blendouts per shot
        Arguments:

        (Shot Index)
        (Enable Blendout Camera)
        (Duration of Blendout)
        (Offset duration)
        """
        return cdll.rexMBRage.SetCutsceneShotBlendOut_Py(index, enabled, duration, offset)


    def SetCutsceneShotFadeOutGame(self, index, enabled, duration=0.8):
        """
        Enabled Cutscene Fade out per shot
        Arguments:

        (Shot Index)
        (Enable Fade out Camera)
        (Duration of Fade)

        """
        return cdll.rexMBRage.SetCutsceneShotFadeOutGame_Py(index, enabled, c_float(duration))

    def SetCutsceneShotFadeInGame(self, index, enabled, duration=0.8):
        """
        Enabled Cutscene Fade In  per shot
        Arguments:

        (Shot Index)
        (Enable Fade In Camera)
        (Duration of Fade)

        """
        return cdll.rexMBRage.SetCutsceneShotFadeInGame_Py(index, enabled, c_float(duration))

    def SetCutsceneShotSectioning(self, index, option):
        """
        Set Cutscene Shot sectioning
        Arguments:

        (Shot Index)
        (Option Index 0-3)

        """

        return cdll.rexMBRage.SetCutsceneShotSectioning_Py(index, option)

    def SetCutsceneShotSectioningDuration(self, index, duration=4.0):
        """
        Set SHot Section duration
        Arguments:

        (Shot Index)
        (Duration)

        """

        return cdll.rexMBRage.SetCutsceneShotSectioningDuration_Py(index, c_float(duration))



    def ExportActiveCutscene(self, p4_Intergration=True, buildzip=True):
        """
        Export active cutscene

        Arguments:
        Enable p4 integration
        build zip
        """

        return cdll.rexMBRage.ExportActiveCutscene_Py( p4_Intergration , buildzip )

    def ExportCameraAnim(self, p4_Intergration=True, buildzip=True ):
        """
        Export only Camera

        Arguments:
        Enable p4 integration
        build zip
        """

        return cdll.rexMBRage.ExportCameraAnim_Py( p4_Intergration , buildzip )

    def BuildActiveCutscene(self, p4_Intergration=True, preview=True ):
        """
        Build Active Cutscene

        Arguments:
        Enable p4 integration
        Enable preview export

        """
        return cdll.rexMBRage.BuildActiveCutscene_Py( p4_Intergration, preview )


    def ExportBuildActiveCutscene( self, p4_Intergration=True, preview=False ):
        """
        Export the cutscne using the currently active settings

        Arguments:
        Enable p4 intergration
        Enable preview export
        """
        return  cdll.rexMBRage.ExportBuildActiveCutscene_Py( p4_Intergration, preview )

    def ExportBuildCutscene( self, cutPart, export_cutfile=True, p4_Intergration=True, preview=False ):
        """
        ExportBuildCutscene. THis will export the current information in the cutpart file and not any local chnages

        Arguments:
        CutPart File Path: If file does not exist the export will fail
        Export CutFile
        Enable p4 Intergration
        Build Preview
        """
        #print 'Expoting Cutscene'
        return (cdll.rexMBRage.ExportBuildCutscene_Py(cutPart, True, export_cutfile, p4_Intergration, preview) != 0)

    def GetNumberCutsceneObjects(self):
        """
        Import the module in will initalize the Exporter, though if a new cutscene has been loaded then we need to re-initalize with the show tool winodw command
        """
        return cdll.rexMBRage.GetNumberCutsceneObjects_Py()

    def GetCutsceneObjectName(self, index):
        """
        :param index: int
        :return: string
        """
        cdll.rexMBRage.GetCutsceneObjectName_Py.restype = c_char_p
        return c_char_p(cdll.rexMBRage.GetCutsceneObjectName_Py(index)).value

    def SetCutsceneObjectHandle(self, index, name="Handle"):
        """
        set the name of the Cutscene
        :param index: int
        :param name: string
        :return: None
        """
        cdll.rexMBRage.SetCutsceneObjectHandle_Py(index, name)

    def GetCutsceneObjectHandle(self, index):
        """

        :param index: int
        :return: string
        """
        cdll.rexMBRage.GetCutsceneObjectHandle_Py.restype = c_char_p
        return c_char_p(cdll.rexMBRage.GetCutsceneObjectHandle_Py(index)).value

    def SetCutsceneOffsets(self, x , y, z, orientation):
        """
        sets coordinates for the cutscene offsets
        :param x: float
        :param y: float
        :param z: float
        :param orientation: float
        :return: list
        """
        cdll.rexMBRage.SetCutsceneOffsets_Py(c_float(x), c_float(y), c_float(z), c_float(orientation))

    def GetCutsceneOffsetsX(self):
        """
        :return: float
        """
        cdll.rexMBRage.GetCutsceneOffsetsX_Py.restype = c_float
        return cdll.rexMBRage.GetCutsceneOffsetsX_Py()

    def GetCutsceneOffsetsY(self):
        """
        :return: float
        """
        cdll.rexMBRage.GetCutsceneOffsetsY_Py.restype = c_float
        return cdll.rexMBRage.GetCutsceneOffsetsY_Py()

    def GetCutsceneOffsetsZ(self):
        """
        :return: float
        """
        cdll.rexMBRage.GetCutsceneOffsetsZ_Py.restype = c_float
        return cdll.rexMBRage.GetCutsceneOffsetsZ_Py()

    def GetCutsceneOrientation(self, *args, **kwargs):
        """
        gets the orientation from the cutscene
        :return float
        """
        cdll.rexMBRage.GetCutsceneOrientation_Py.restype = c_float
        return cdll.rexMBRage.GetCutsceneOrientation_Py()

    def Export(self, *args, **kwargs):

        """
        exports
        :return: Boolean
        """

        return cdll.rexMBRage.Export_Py()

    def AddAdditionalModelEntry(self, target_model, model_name, track_name, attribute_name, *args, **kwargs):

        """
        :param target_model: string
        :param model_name: string
        :param track_name: string
        :param attribute_name: string
        """
        #Might need to convert all parameters into c_char_p pointers
        return cdll.rexMBRage.AddAdditionalModelEntry_Py(target_model, model_name, track_name, attribute_name)

    def GetExportPath(*args, **kwargs):
        """
        gets the export path
        :return: string
        """

        return c_char_p(cdll.rexMBRage.GetExportPath_Py()).value

    def RenameClip(self, new_name, *args, **kwargs):

        """
        renames the clip
        :return: string
        """
        #parameter might need to be converted to c_char_p pointer
        return cdll.rexMBRage.RenameClip_Py(new_name)

    def SetExportPath(self, export_path, *args, **kwargs):

        """
        sets the export path
        :param export_path: string
            path to export file to
        :return: None
        """

        return cdll.rexMBRage.SetExportPath_Py(export_path)

    def SetControlFile(self, control_file, *args, **kwargs):
        """
        sets the control file
        :param control_file: string
        :return: None
        """

        return cdll.rexMBRage.SetControlFile_Py(control_file)

    def DeriveAndSetExportPath(*args, **kwargs):

        """
        Finds and sets the export path
        :return: None
        """

        return cdll.rexMBRage.DeriveAndSetExportPath_Py()

    def SetNameSpace(self, index, new_namespace, *args, **kwargs):

        """
        sets the namespace
        :param index: int
        :param new_namespace: string
            the new name for the namespace
        :return: Boolean
        """

        return cdll.rexMBRage.SetNameSpace_Py(index, new_namespace)

    def GetModelCount(*args, **kwargs):

        """
        gets the number of models
        :return: Int
        """

        return cdll.rexMBRage.GetModelCount_Py()

    def InitializeAnimation( self ):
        """
        Initialize our Cutscene Exporter
        """
        if not self.IsAnimationExporterInitialized():
            self.ShowToolByName("Rex Rage Animation Export")

    def IsAnimationExporterInitialized(*args, **kwargs):

        """
        checks if the animation exporter has been initialized
        :returns: Boolean
        """

        return cdll.rexMBRage.IsAnimationExporterInitialized_Py()

    def EditRootEntry(self, root_model, model_name, skeleton_file, model_type, spec_file, facial_attribute_file, *args, **kwargs):
        """
        changes the root entry

        :param root_model: string
        :param model_name: string
        :param skeleton_file: string
        :param model_type: string
        :param spec_file: string
        :param facial_attribute_file: string

        :return Boolean ?
        """

        return cdll.rexMBRage.EditRootEntry_Py(root_model, model_name, skeleton_file, model_type, spec_file, facial_attribute_file)
        
    def SetExportModel(self, root_model, value, *args, **kwargs):
        """
        changes the root entry

        :param root_model: string
        :param value: Boolean

        :return Boolean ?
        """

        return cdll.rexMBRage.SetExportModel_Py(root_model, value)
        
    def SetMergeFacial(self, root_model, value, *args, **kwargs):
        """
        changes the root entry

        :param root_model: string
        :param value: Boolean

        :return Boolean ?
        """

        return cdll.rexMBRage.SetMergeFacial_Py(root_model, value)
        
    def SetAltOutputPath(self, root_model, alt_output_path, *args, **kwargs):
        """
        changes the root entry

        :param root_model: string
        :param alt_output_path: string

        :return Boolean ?
        """

        return cdll.rexMBRage.SetAltOutputPath_Py(root_model, alt_output_path)

    def PopulateTakesExportInfo(*args, **kwargs):

        """
        checks if Populate takes export info
        :return: Boolean
        """

        return cdll.rexMBRage.PopulateTakesExportInfo_Py()

    def SetTakeExportSetting(self, take_name, set_export, *args, **kwargs):

        """
        :param take_name: string
        :param set_export: boolean
        :return: boolean
        """

        return cdll.rexMBRage.SetTakeExportSetting_Py(take_name, set_export)

    def SetTakeAdditiveSetting(self, take_name, set_additive, *args, **kwargs):

        """
        :param take_name: string
        :param set_additive: boolean
        :return: boolean
        """

        return cdll.rexMBRage.SetTakeAdditiveSetting_Py(take_name, set_additive)

    def SetTakeControlFile(self, take_name, control_file, *args, **kwargs):
        """
        :param take_name: string
        :param control_file: string
        :return: boolean
        """

        return cdll.rexMBRage.SetTakeControlFile_Py(take_name, control_file)

    def SetTakeCompressionFile(self, take_name, compression_file,*args, **kwargs):

        """
        :param take_name: string
        :param compression_file: string
        :return: boolean
        """

        return cdll.rexMBRage.SetTakeCompressionFile_Py(take_name, compression_file)

    def AddTakeOutputPath(self, take_name, output_file,*args, **kwargs):

        """
        :param take_name: string
        :param output_file: string
        :return: boolean
        """

        return cdll.rexMBRage.AddTakeOutputPath_Py(take_name, output_file)

    def GetModelName(self, index, *args, **kwargs):

        """
        gets the name of the model based on the index passed

        :param index: int
        :return: string
        """

        return c_char_p(cdll.rexMBRage.GetModelName_Py(index)).value

    def GetModelMover(self, index, *args, **kwargs):

        """
        gets the name of the model mover based on the index passed
        :param index: int
        :return: string
        """

        return c_char_p(cdll.rexMBRage.GetModelMover_Py(index)).value

    def GetModelSpecFile(self, index, *args, **kwargs):

        """
        gets the name of the model specs file based on the index passed
        :param index: int
        :return: string
        """

        return c_char_p(cdll.rexMBRage.GetModelSpecFile_Py(index)).value

    def GetModelType(self, index, *args, **kwargs):

        """
        gets the model type based on the index passed
        :param index: int
        :return: string
        """

        return c_char_p(cdll.rexMBRage.GetModelType_Py(index)).value

    def GetModelSkeleton(self, index, *args, **kwargs):

        """
        gets the name of the model skeleton based on the index passed
        :param index: int
        :return: string
        """

        return c_char_p(cdll.rexMBRage.GetModelSkeleton_Py(index)).value

    def GetModelFacialAttribute(self, index, *args, **kwargs):

        """
        gets the name of the model facial attributes based on the index passed
        :param index: int
        :return: string
        """

        return c_char_p(cdll.rexMBRage.GetModelFacialAttribute_Py(index)).value

    def SetLightTransformInMotionbuilderSpace(self, name, x, y, z, quatx, quaty, quatz, quatw,*args, **kwargs):

        """
        sets location of the light transforms in motion builder space

        :param x: float
        :param y: float
        :param z: float
        :param quatx: float
        :param quaty: float
        :param quatz: float
        :param quatw: float

        :return: boolean
        """

        return cdll.rexMBRage.SetLightTransformInMotionbuilderSpace_Py(name, x, y, z, quatx, quaty, quatz, quatw)
    
    def ImportCutFile(self, cutFilePath):

        """
        imports cutfile from supplied path
        
        :param cutFilePath: string
        
        :return: boolean
        """

        return cdll.rexMBRage.ImportCutFile_Py(cutFilePath)
    
    def ImportCutPart(self, cutPartPath):

        """
        imports cutpart from supplied path
        
        :param cutPartPath: string
        
        :return: boolean
        """

        return cdll.rexMBRage.ImportCutPart_Py(cutPartPath)
    
    def AddCutsceneCamera(self, cameraName, addFade=False):

        """
        adds a camera to the exporter, and optionally it's screenfade
        
        :param cameraName: string
        :param addFade: boolean
        
        :return: boolean
        """

        return cdll.rexMBRage.AddCutsceneCamera_Py(cameraName, addFade)
    
    def RemoveCutsceneObject(self, objectName):

        """
        Removes an object from the exporter
        
        :param objectName: string
        
        :return: boolean
        """

        return cdll.rexMBRage.RemoveCutsceneObject_Py(objectName)
    
    def RemoveAllCutsceneObjects(self):
        """
        Removes all objects from the exporter
        Returns (bool)
        """
        return cdll.rexMBRage.RemoveCutsceneAllObjects_Py()

    def AutoPopulateCutscene(self):
        """
        Searches through FBX for audio, characters, props, weapons,
        and vehicles then adds those items to the exporter automatically.
        RDR ONLY - this command is not supported for GTA yet
        """
        return cdll.rexMBRage.AutoPopulateCutsceneData_Py()

    def AddCutsceneAudio(self, audioName):
        """
        Adds audio to the cutscene exporter
        Arguments:
            characterRoot - String of audio's filename WITHOUT any extensions (no .C.wav)
        Returns (bool) - True for success, False for failure
        """
        return cdll.rexMBRage.AddCutsceneAudio_Py(audioName)

    def AddCutsceneCharacter(self, characterRoot):
        """
        Adds a character model to the cutscene exporter
        Arguments:
            characterRoot - String of model's skel root LongName (example: "Player_Zero:SKEL_ROOT")
        Returns (bool) - True for success, False for failure
        """
        return bool(cdll.rexMBRage.AddCutscenePed_Py(characterRoot))

    def AddCutsceneFace(self, characterRoot, faceRoot=""):
        """
        Adds a character face to the cutscene exporter.
        Arguments:
            characterRoot - String of model's skel root LongName (example: "Player_Zero:SKEL_ROOT")
            faceRoot - String of the face root. If not provided, the tool will attempt to find it automatically.
        Returns (bool) - True for successful add, False for failure
        """
        results = cdll.rexMBRage.AddCutsceneFace_Py(characterRoot, faceRoot)
        if results != 0:
            return True
        else:
            return False

    def AddCutsceneProp(self, propRoot):
        """
        Adds a prop model to the cutscene exporter
        Arguments:
            propRoot - String of prop's model root (example: "p_doorMausoleum01x:Root_p_doorMausoleum01x")
        Returns (bool) - True for success, False for failure
        """
        return bool(cdll.rexMBRage.AddCutsceneProp_Py(propRoot))

    def AddCutsceneWeapon(self, weaponRoot):
        """
        Adds a weapon model to the cutscene exporter
        Arguments:
            weaponRoot - String of weapons's gun root (example: "W_PI_APPistol:Gun_Root")
        Returns (bool) - True for success, False for failure
        """
        return bool(cdll.rexMBRage.AddCutsceneWeapon_Py(weaponRoot))

    def AddCutsceneVehicle(self, vehicleRoot):
        """
        Adds a vehicle model to the cutscene exporter
        Arguments:
            vehicleRoot - String of vehicle's chassis root (example: "wagon02x:chassis")
        Returns (bool) - True for success, False for failure
        """
        return bool(cdll.rexMBRage.AddCutsceneVehicle_Py(vehicleRoot))

RexExport = RexExport()

#fbmenu = FBMenuManager()    
#menu = fbmenu.GetMenu("Open Reality/Tools/Rex Rage Cut-Scene Export")


