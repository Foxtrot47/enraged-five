'''

 Script Path: RS/Core/Reference/SaveSceneReferences.py

 Written And Maintained By: Kathryn Bodey

 Created: 11.06.2014

 Description: Saves a list of References from a file.  User can then use the Ref System's 'batch create' 
 			  function to read the list back in and rebuild the same references. 

'''

from pyfbsdk import *
import RS.Globals


def SaveSceneReferences( control, event ):
	referenceScene = None
	refNullList = []
	pathList = []

	# find ref null
	for component in RS.Globals.gComponents:
		if component.LongName == 'REFERENCE:Scene' and component.ClassName() == 'FBModelNull':    
			referenceScene = component

	# get references
	if referenceScene != None:
		RS.Utils.Scene.GetChildren( referenceScene, refNullList, "", False )

		# find properties and add refs to a dict 'RefName:RefPath'
		for i in refNullList:
			assetTypeProperty = i.PropertyList.Find( "rs_Asset_Type" )
			refPathProperty = i.PropertyList.Find( "Reference Path" )
	
			if assetTypeProperty and refPathProperty:
				if assetTypeProperty.Data == "3Lateral":
					None
				else:
					pathList.append( refPathProperty.Data )
	
		# save dict to a txt file. seperating entries with '^' 
		currentFilePath = FBApplication().FBXFileName
		split = currentFilePath.lower().split(".fbx") 
		txtPath = split[0] + '.txt'
	
		try:
			txtFile = open( txtPath, 'w' )
			for i in pathList:
				txtFile.write( i )
				txtFile.write( '\n' )
				
				# user feedback
			FBMessageBox( 'R*', 'References list saved as:\n{0}'.format( txtPath ), 'Ok' )
	
		except IOError, err:
			print err
			
		finally:
			if txtPath:
				txtFile.close() 
	else:
		# user feedback
		FBMessageBox( 'R*', 'References do not exist in scene.', 'Ok' )