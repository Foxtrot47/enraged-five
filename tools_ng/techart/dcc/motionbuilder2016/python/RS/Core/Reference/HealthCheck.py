###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_RefSysHealthCheck.py
## Written And Maintained By: Kathryn Bodey
## Contributors: Kristine Middlemiss
## Description: This tool checks the 'health' of the scene.
##              - Set is rotated/scaled correctly
##              - TimeMode is set to 30FPS
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *

import RS.Globals as glo
import RS.Utils.Scene
import RS.Core.Reference.Manager

lReferenceNullList = []

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Fix for Rotation/Scale issue - url:bugstar:694263
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_RotationScale_Check():

    lSetRootList = []
    for iComponent in glo.gComponents:
        if iComponent.Name == 'SetRoot' and iComponent.ClassName() == 'FBModelNull':
            lSetRootList.append(iComponent)
    
    if len(lSetRootList) > 0:
    
        for lSetRoot in lSetRootList:
            
            if lSetRoot.ClassName() == 'FBModelNull':
                
                RS.Utils.Scene.DeSelectAll()
                lSetRoot.Selected = True
                FBSystem().CurrentTake.ClearAllProperties(True)
                FBSystem().Scene.Evaluate()
                
                lSetRoot.PropertyList.Find("Enable Rotation DOF").Data = False
                lSetRoot.PropertyList.Find("Lcl Rotation").SetAnimated(True)
                FBSystem().Scene.Evaluate()
                lSetRoot.PropertyList.Find("Lcl Scaling").SetAnimated(True)
                lLclScale = lSetRoot.Scaling.Data
                                    
                lGrabProp = None
                lGrabProp = lSetRoot.PropertyList.Find("Asset_Setup")
                
                if lGrabProp != None:
                    
                    if lGrabProp.Data == "RAG obj":
    
                        lVector = FBVector3d()
                        lSetRoot.GetVector(lVector, FBModelTransformationType.kModelRotation, True)  
                
                        if str(lVector[0]) != '0':
                                                
                            l0Nodes = lSetRoot.PropertyList.Find("Rotation (Lcl)").GetAnimationNode().Nodes[0]        
                            l1Nodes = lSetRoot.PropertyList.Find("Rotation (Lcl)").GetAnimationNode().Nodes[1]
                            l2Nodes = lSetRoot.PropertyList.Find("Rotation (Lcl)").GetAnimationNode().Nodes[2] 
                               
                            lSetRoot.SetVector(FBVector3d(0, lVector[1], 0), FBModelTransformationType.kModelRotation, True)
                            FBSystem().Scene.Evaluate() 
                        
                        if str(lLclScale) != 'FBVector3d(1, 1, 1)':
                            l0ScaleNodes = lSetRoot.PropertyList.Find("Scaling (Lcl)").GetAnimationNode().Nodes[0]        
                            l1ScaleNodes = lSetRoot.PropertyList.Find("Scaling (Lcl)").GetAnimationNode().Nodes[1]
                            l2ScaleNodes = lSetRoot.PropertyList.Find("Scaling (Lcl)").GetAnimationNode().Nodes[2] 
                            
                            lSetRoot.Scaling = FBVector3d(1, 1, 1)            
                            FBSystem().Scene.Evaluate()
    
                    else:  
                                          
                        lVector = FBVector3d()
                        lSetRoot.GetVector(lVector,  FBModelTransformationType.kModelRotation, True)  
                
                        if str(lVector[0]) != '-90.0':
                                                
                            l0Nodes = lSetRoot.PropertyList.Find("Rotation (Lcl)").GetAnimationNode().Nodes[0]        
                            l1Nodes = lSetRoot.PropertyList.Find("Rotation (Lcl)").GetAnimationNode().Nodes[1]
                            l2Nodes = lSetRoot.PropertyList.Find("Rotation (Lcl)").GetAnimationNode().Nodes[2] 
                                        
                            lSetRoot.SetVector(FBVector3d(-90, lVector[1], 0),  FBModelTransformationType.kModelRotation, True)
                            FBSystem().Scene.Evaluate() 
                        
    
                        
                        if str(lLclScale) != 'FBVector3d(100, 100, 100)':
                            l0ScaleNodes = lSetRoot.PropertyList.Find("Scaling (Lcl)").GetAnimationNode().Nodes[0]        
                            l1ScaleNodes = lSetRoot.PropertyList.Find("Scaling (Lcl)").GetAnimationNode().Nodes[1]
                            l2ScaleNodes = lSetRoot.PropertyList.Find("Scaling (Lcl)").GetAnimationNode().Nodes[2] 
                            
                            lSetRoot.Scaling = FBVector3d(100, 100, 100)            
                            FBSystem().Scene.Evaluate()
                            
                lSetRoot.Selected = False
        
        if len(lSetRootList) == 2:
            FBMessageBox('Health Check','There are 2 Environments in this scene.\nYou may find you need to re-align the INT and EXT back together again.\n\nSee Wiki Page:\nhttps://devstar.rockstargames.com/wiki/index.php/Cutscene_Environment_Int/Ext_Matching','Ok')
                

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Check for correct TimeMode (FPS) in scene. url:bugstar:912017
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_TimeMode_Check():

    lCurrentTimeMode = FBPlayerControl().GetTransportFps()
    lTimeMode30 = FBTimeMode.kFBTimeMode30Frames
    
    if lCurrentTimeMode != lTimeMode30:
        FBPlayerControl().SetTransportFps(lTimeMode30)
    else:
        None

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Delete all the empty Folders, as they are annoying and cause clutter
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_DeleteFolders_Check():
    for folder in FBSystem().Scene.Folders:
        for item in folder.Items:
            if isinstance(item,FBCharacter):
                folder.Items.remove(item)

    lFolderList = []
    for lFolder in glo.gFolders:    
        if len(lFolder.Items) == 0:
            lFolderList.append(lFolder)
            
    map( FBComponent.FBDelete, lFolderList )
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Option to delete empty Anim Layers. Bug 974642
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################    
def KeyCount( pNode, lNoKeys ):
    if pNode.FCurve:
        if pNode.KeyCount > 0:
            return True
    else:
        # Then we are dealing with a parent node. Let's look at it children nodes.
        for lNode in pNode.Nodes:
            # Recursively call ourselves to deal with sub-nodes.
            HasKey = KeyCount( lNode, lNoKeys )
            if HasKey:
                return True            
    return False

def rs_DeleteAnimLayer_Check():     

    for lTake in FBSystem().Scene.Takes:
        lDeleteLayer = []
        lnumAnimLayers = lTake.GetLayerCount()    
        for lanimLayer in range( 0, lnumAnimLayers ):
            
            lNoKeys = True
            llayer = lTake.GetLayer( lanimLayer )
    
            # You can never delete the BaseAnimation layer, so no point in working on it
            if llayer.Name != "BaseAnimation":
                lTake.SetCurrentLayer(lanimLayer)
                for lComp in FBSystem().Scene.Components:
                    if (lComp.ClassName() == "FBModel") or (lComp.ClassName() == "FBCamera"):                 
                        try:
                            lAnimationNodesIN = lComp.AnimationNodeInGet().Nodes
                            for lAnimationNode in lAnimationNodesIN:
                                HasKey = KeyCount(lAnimationNode, lNoKeys)
                                if HasKey == True:
                                    lNoKeys = False
                                    break                   
                        except:
                            pass
    
                if lNoKeys == True:
                    lDeleteLayer.append(llayer)
                    
        for delLayer in lDeleteLayer:
            print "Deleting Animation Layer: " + delLayer.Name + " from take: " + lTake.Name 
            delLayer.FBDelete()   

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Disable all Rotation DOFS
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################    

def rs_DisableRotDOF_Check():  
    for iRoot in glo.gRoot.Children:
        # Finds the objects that have Dummy01 Nodes
        if iRoot.Name == "Dummy01":
            if ":" in iRoot.LongName:        
                lNamespace = iRoot.LongName.partition(":")[0]            
                if " " in lNamespace:        
                    lNamespace = lNamespace.partition(" ")[0]
                # add feature that disables all rotation dofs in props & weapons
                lDummy = FBFindModelByLabelName(lNamespace + ":Dummy01")
                if lDummy:
                    lAssetTypeProp = lDummy.PropertyList.Find("Asset_Type")
                    if lAssetTypeProp:
                        if lAssetTypeProp.Data == 'Props Skinned' or lAssetTypeProp.Data == 'Props' or lAssetTypeProp.Data == 'Props Weapon' or lAssetTypeProp.Data == 'Vehicles':
                            for lChild in lDummy.Children:
                                if  lChild.LongName == lNamespace + ":mover":
                                    lMover = FBFindModelByLabelName(lChild.LongName)
                                    if lMover:
                                        lRotDOFProp = lMover.PropertyList.Find("Enable Rotation DOF")        
                                        if lRotDOFProp:
                                            if lRotDOFProp.Data == True:
                                                lRotDOFProp.Data = False 

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Deletes all the HIK 4.5 Solvers that are in the Navigator under Solvers, they are not used in the game and were introduced by a Hot Fix
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################    

def rs_DeleteHIKSolver_Check():
    lHIKSolverList = []
    
    for lSolve in FBSystem().Scene.ConstraintSolvers:
        if lSolve.Name.startswith("HIK 4.5 Solver"):
            lHIKSolverList.append(lSolve)
    
    for i in lHIKSolverList:
        i.FBDelete()            
        
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This deletes the facial bones that are problematic after you strip and sync/merge, it will delete  FaceRoot_Head_000_R and FaceRoot_Head_001_R, then you can strip and sync/merge again
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################    

def findChildren(Model):    
    for child in Model.Children:
        if len (child.Children) < 1:
            child.FBDelete()  
        else:
            findChildren(child)    


def rs_DeleteFacialBone_Check():

    lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
    
    # From the root, get all the children which are the registered reference namespaces in the scene
    if lReferenceNull:
        RS.Utils.Scene.GetChildren(lReferenceNull, lReferenceNullList, "", False)
        #lReferenceNullList.extend(eva.gChildList)
        #del eva.gChildList[:]    
   
    if len(lReferenceNullList) >=1:
        for iRef in lReferenceNullList:
            lSkelRoot = FBFindModelByLabelName(iRef.Name + ":FaceRoot_Head_000_R")            
            if lSkelRoot:
                for i in range(10):
                    findChildren(lSkelRoot)            

            lSkelRoot1 = FBFindModelByLabelName(iRef.Name + ":FaceRoot_Head_001_R")  
            if lSkelRoot1:
                for i in range(10):
                    findChildren(lSkelRoot1)


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Delete all Unused Control Rigs. url:bugstar:1166107
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  


def rs_DeleteUnusedControlRigs_Check( popup=True ):
    
    #Create empty Dictionary
    lControlSetDict = {}
    
    #Find Control Sets
    for iControlRig in glo.gControlSets:
        
        #Counter
        lFound = False
        
        #Set the Dictionary with Key & Value info (modelName : Bool, modelPointer)
        lControlSetDict[ iControlRig.LongName ] = ( False, iControlRig )
            
        #Find Control Set Connections
        for iDst in range( iControlRig.GetDstCount() ):
            
            #Find if the ClassName of the Connection is FBCharacter
            if iControlRig.GetDst( iDst ).ClassName() == 'FBCharacter':
               
                #Change Counter if found
                lFound = True
            
            #Update Dictionary if found and reset the Counter
            if lFound == True:
                lControlSetDict[ iControlRig.LongName ] = ( True, iControlRig )
                lFound == False            
    
    #Get list of unused control rigs
    lFalseRigs = [ data[ 1 ] for modelName, data in lControlSetDict.iteritems() if not data[ 0 ] ]
    
    #Create list string for user feedback
    lFalseNames = [ modelName for modelName, data in lControlSetDict.iteritems() if not data[ 0 ] ]
    lFeedbackString = '\n'.join( lFalseNames )
    
    print str(lFalseNames)
    
    #Check the list has data in it
    if len(lFalseRigs) > 0:
        
        #Iterate through list and delete the unused rigs
        for iRig in lFalseRigs:
            iRig.FBDelete()      
        
        if popup == True:
            #User Feedback
            FBMessageBox('Deleted Control Rigs', 'The following control rigs have been deleted:\n\n' + lFeedbackString, 'ok')


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Delete all Duplicate & Unused Character Extensions. url:bugstar:1166107 
##              Also Renaming left over Extensions to match the Character Node Names
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
def rs_FixBadCharExt():

    #Create the Character Extension Dictionary: [ CharacterLongName ] = ( CharacterHandle, ExtensionList )
    def CharacterExtensionDict(pDict):
        
        if len(glo.gCharacters) > 0:
            
            for iCharacter in glo.gCharacters:
                
                lTempExtList = []
                
                for iSrc in range(iCharacter.GetSrcCount()):
                    
                    if iCharacter.GetSrc(iSrc).ClassName() == 'FBCharacterExtension':
                        
                        lExt = iCharacter.GetSrc(iSrc)
                        lTempExtList.append(lExt)
                    
                pDict[ iCharacter.LongName ] = ( iCharacter, lTempExtList )
                    
                lTempExtList = [] 
        
        return pDict
    
    
    #Iterate through Extension List to Remove any unused extensions
    lEmptyExtList = []
    for iExt in glo.gCharacterExtensions:
        if iExt.GetSrcCount() == 0:
            lEmptyExtList.append(iExt)
    
    if len(lEmptyExtList) > 0:
        for i in lEmptyExtList:
            i.FBDelete()
    
    
    #Generate Dictionary & iterate through to Remove any duplicate extensions
    lCharExtDict = {}
    
    for key, value in CharacterExtensionDict(lCharExtDict).iteritems():
        
        lExtLen = len(value[1])      
        
        for iExt in range(0, lExtLen-1):
            value[1][iExt].FBDelete()
       
        
    #Regenerate a fresh Dictionary List, iterate through and rename extensions to match the character name
    lCharExtDict = {}
    
    for key, value in CharacterExtensionDict(lCharExtDict).iteritems():
        if len(value[1]) > 0:
            for i in range(len(value[1])):
                lSplitName = value[1][i].LongName.split(':')
                value[1][i].LongName = value[0].Name + ":" + lSplitName[-1]


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Delete all 'headCam' & 'headLight' objects. url:bugstar:1199003 
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
def rs_DeleteHeadCamLightObjects():

    #Empty list for objects to delete
    lRemoveList = []
    
    for iComponent in glo.gComponents:
        
        #find headCam objects, make sure they are FBCamera. 
        if iComponent.Name == "headCam":
            if iComponent.ClassName() == 'FBCamera':
                #Add to lRemoveList
                lRemoveList.append(iComponent)
        
        #find headLight objects, make sure they are FBLight.
        if iComponent.Name == "headLight":
            if iComponent.ClassName() == 'FBLight':
                #Add to lRemoveList
                lRemoveList.append(iComponent)
    
    #iterate through list and delete each object from scene
    for i in lRemoveList:
        try:
            i.FBDelete()
        except:
            pass

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Checks for Broken High Heels
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
def rs_FixHighHeel_Check():
    lNamespacesWithProblematicHighHeels = []
    lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
    
    # From the root, get all the children which are the registered reference namespaces in the scene
    if lReferenceNull:
        RS.Utils.Scene.GetChildren(lReferenceNull, lReferenceNullList, "", False)
        #lReferenceNullList.extend(eva.gChildList)
        #del eva.gChildList[:]    
   
    if len(lReferenceNullList) >=1:
        for iRef in lReferenceNullList:
            if iRef.Name != "exportOrigin_Mover":
                lProp = iRef.PropertyList.Find('rs_Asset_Type')
                if lProp:
                    if lProp.Data == 'Characters':
                        
                        #################################
                        ## Item A - RECT_HeelHeight Model
                        #################################
                        # Check one - existence of model
                        lRECT_HeelHeight = FBFindModelByLabelName(iRef.Name + ":RECT_HeelHeight")  
                        lMover = FBFindModelByLabelName(iRef.Name + ":mover")  
                        if lRECT_HeelHeight and lMover:
    
                            # Check two - parentage of model
                            if lRECT_HeelHeight.Parent != lMover:                            
                                print "For " + iRef.Name + " RECT_HeelHeight is not parented to the mover"
                                lNamespacesWithProblematicHighHeels.append(iRef.Name) # High Heel Problem
            
                            # Check three - TRS of model                    
                            # This is the proper translation FBVector3d(-0.5, 0, -0.8)
                            lTrans = lRECT_HeelHeight.Translation.Data                        
                            if str(lTrans) != 'FBVector3d(-0.5, 0, -0.8)':
                                lRECT_HeelHeight.Translation.Data = FBVector3d(-0.5, 0, -0.8) # Fixed Heel Problem
    
                            # This is the proper rotation FBVector3d(0, 0, 180)
                            lRot = lRECT_HeelHeight.Rotation.Data                        
                            if str(lRot) != 'FBVector3d(0, 0, 180)':
                                lRECT_HeelHeight.Rotation.Data = FBVector3d(0, 0, 180) # Fixed Heel Problem
    
                            # This is the proper scaling FBVector3d(0.2, 0.2, 0.2)
                            lScale = lRECT_HeelHeight.Scaling.Data                        
                            if str(lScale) != 'FBVector3d(0.2, 0.2, 0.2)':                       
                                lRECT_HeelHeight.Scaling.Data = FBVector3d(0.2, 0.2, 0.2) # Fixed Heel Problem
                            
                            # Check four - Rotation DOF of model 
                            lRotationActive = lRECT_HeelHeight.PropertyList.Find("RotationActive")
                            if lRotationActive:
                                lRotationActiveValue = lRotationActive.Data  
                                if lRotationActiveValue == False:
                                    lRotationActive.Data = True # Fixed Heel Problem
                                    
                            lEnableRotationDOF = lRECT_HeelHeight.PropertyList.Find("Enable Rotation DOF")
                            if lEnableRotationDOF:
                                lEnableRotationDOFValue = lEnableRotationDOF.Data  
                                if lEnableRotationDOFValue == False:
                                    lEnableRotationDOF.Data = True # Fixed Heel Problem                                                 
                        else:
                            print "For " + iRef.Name + " RECT_HeelHeight is not found, no high heel"
                            lNamespacesWithProblematicHighHeels.append(iRef.Name) # High Heel Problem
            
                        ############################
                        ## Item B - HeelHeight Model
                        ############################
                                     
                        # Check one - existence of model
                        lHeelHeight = FBFindModelByLabelName(iRef.Name + ":HeelHeight")  
                        if lRECT_HeelHeight and lHeelHeight:
            
                            # Check two - parentage of model
                            if lHeelHeight.Parent != lRECT_HeelHeight:
                                print "For " + iRef.Name + " HeelHeight is not parented to the mover"                            
                                lNamespacesWithProblematicHighHeels.append(iRef.Name) # High Heel Problem
    
                            # Check three - TRS of model                    
                            # This is the proper translation FBVector3d(0, 1, 0)
                            lTransHH = lHeelHeight.Translation.Data                        
                            if str(lTransHH) != 'FBVector3d(0, 1, 0)':
                                lHeelHeight.Translation.Data = FBVector3d(0, 1, 0) # Fixed Heel Problem
    
                            # This is the proper rotation FBVector3d(0, 0, 0)
                            lRotHH = lHeelHeight.PropertyList.Find("Lcl Rotation").Data                
                            if str(lRotHH) != 'FBVector3d(0, 0, 0)':
                                lHeelHeight.PropertyList.Find("Lcl Rotation").Data  = FBVector3d(0, 0, 0) # Fixed Heel Problem                         
    
                            # This is the proper scaling FBVector3d(1, 1, 1)
                            lScaleHH = lHeelHeight.Scaling.Data                        
                            if str(lScaleHH) != 'FBVector3d(1, 1, 1)': 
                                lHeelHeight.Scaling.Data = FBVector3d(1, 1, 1) # Fixed Heel Problem                                   
    
                            # Check four - Rotation DOF of model 
                            lTranslationActive = lHeelHeight.PropertyList.Find("TranslationActive")
                            if lTranslationActive:
                                lTranslationActiveValue = lTranslationActive.Data  
                                if lTranslationActiveValue == False:
                                    lTranslationActive.Data = True # Fixed Heel Problem
                                    
                            lEnableTranslationDOF = lHeelHeight.PropertyList.Find("Enable Translation DOF")
                            if lEnableTranslationDOF:
                                lEnableTranslationDOFValue = lEnableTranslationDOF.Data  
                                if lEnableTranslationDOFValue == False:
                                   lEnableTranslationDOF.Data = True # Fixed Heel Problem

                        else:                            
                            print "For " + iRef.Name + " HeelHeight is not found, no high heel"
                            lNamespacesWithProblematicHighHeels.append(iRef.Name) # High Heel Problem
            
                        #################################
                        ## Item C - HeelHeight Constraint
                        #################################    
                        
                        for lCon in FBSystem().Scene.Constraints:
                            if lCon.LongName == iRef.Name + ":HeelHeight":
                                if lCon.Is( FBConstraintRelation_TypeInfo() ):
                                    # Check five - Not enough boxes in the relation constraint
                                    if len(lCon.Boxes) < 3:
                                        print "For " + iRef.Name + " the HeelHeight Relation contraint does not have the right about of boxes"
                                        lNamespacesWithProblematicHighHeels.append(iRef.Name) # High Heel Problem    
    
                                    # Check six - Ensure the constraint is active
                                    if lCon.Active == False:
                                        lCon.Active = True # Fixed Heel Problem
                                else:
                                    print "For " + iRef.Name + " the HeelHeight Relation contraint was not found"
                                    lNamespacesWithProblematicHighHeels.append(iRef.Name) # High Heel Problem        

        lNamespacesWithProblematicHighHeels = list(set(lNamespacesWithProblematicHighHeels))       
        lNamespacesWithProblematicHighHeelsString = ""
        if len(lNamespacesWithProblematicHighHeels) > 0:
            for item in lNamespacesWithProblematicHighHeels:
                lNamespacesWithProblematicHighHeelsString = lNamespacesWithProblematicHighHeelsString + item + "\n"
                    
            FBMessageBox('Warning', 'The high heels for these characters cannot be fixed without using the reference system to strip/merge:\n\n {0}'.format(lNamespacesWithProblematicHighHeelsString), "Ok")
        else:
            FBMessageBox('Complete', 'The high heels have been checked and fixed where needed, everything is good!', "Ok")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Deletes Unused Textures and Videos.
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                  

def rs_DeleteTextureVideoClip():
    # There are all these Floating Textures and Videos, let's delete them.
    lFloatingTextures = []
    for lMat in FBSystem().Scene.Textures: 
    
        lConnectedModel = False
        for i in range(lMat.GetDstCount()):
     
            if lMat.GetDst(i).Name == lMat.Name or lMat.GetDst(i).Name == "Scene" or lMat.GetDst(i).Name.startswith('Textures'):
                pass
            else:
                lConnectedModel = True   
       
        if lConnectedModel == False:
            print "Deleting " + lMat.Name
            lFloatingTextures.append(lMat)
            
    map( FBComponent.FBDelete, lFloatingTextures ) 
    
    
    lFloatingVideoClips = []
    for lMat in FBSystem().Scene.VideoClips: 
        # This is a weird one, if the GetDst(i).Name = to lMat.Name it means it's has a Texture parent but this is not the case Textures...
        lConnectedModel = False
        for i in range(lMat.GetDstCount()):                          
            print lMat.Name + " belongs to " +  lMat.GetDst(i).Name 
            if lMat.GetDst(i).Name == "Scene" or lMat.GetDst(i).Name.startswith('Videos'):
                pass
            else:
                lConnectedModel = True 
            
        if lConnectedModel == False:
            print "Deleting " + lMat.Name   
            lFloatingVideoClips.append(lMat)             
           
    map( FBComponent.FBDelete, lFloatingVideoClips )     

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Deletes Unused Namespaces
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################    

def rs_DeleteUnusedNamespace_Check():

    lDeleteNamespaces = []
    
    for lNameSpace in FBSystem().Scene.Namespaces:
        lDeleteNamespaces.append(lNameSpace)
    
    map( FBComponent.FBDelete, lDeleteNamespaces )
