'''
Reference Editor Source Control Utiliets

'''

import RS
import RS.Core.Reference.RefPaths as RefPath

lMarkedFileList = []


'''
   Get all marked files for Delete in p4. This should only be called once to keep p4 actitviety down.
   
   TODO: THIs assume all resource files are refernced in the resoure directory.... of course not!
  
'''
def __getMarkedforDeleteFiles():
    global lMarkedFileList
    lMarkedFileList = RS.Perforce.GetAllMarkedDeleteDepotFiles(RefPath.lResourceDirectory)
    print lMarkedFileList
    

'''
   Validate if file is marked for delete or not
'''
def IsMarkedForDelete( pReferenceFilePath ):    
    safeFilename = (pReferenceFilePath.replace('\\','/')).replace('X:','/')
    if safeFilename in lMarkedFileList:
        return True
    else:
        return False
    

'''
    This will replace invalid characters in filenames for use with p4. the @ and # are typical examples of these characters
    
    TODO: This is better to be in the mian RS. Perforce script but needs a bit of refacotring
'''
def ToSafeP4Path( filename ):
    if not type(filename) is str:return filename
    safeP4Filename = filename.replace('@','%40')
    return safeP4Filename

   
# get a list of marked for delete files in perofrce
__getMarkedforDeleteFiles()