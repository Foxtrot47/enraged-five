'''
Module to manage the loading and saving of assets in a cutscene.

'''

# FBX Python SDK Modules
from fbx import *
import FbxCommon

# MotionBuilder Python SDK Modules
from pyfbsdk import *

import os
import sys
import glob
import types
import ctypes

import RS.Globals
import RS.Utils.Widgets
import RS.Core.Reference.Manager
import RS.Core.Reference.New
import RS.Utils.Profiler
import RS.Utils.FBXSDK as fbxHelper
import RS.Utils.ControlSet

class RsCutsceneOptions( object ):
    def __init__( self ):
        self.newFile = True
        self.saveAll = False


class RsCutsceneAsset( object ):
    '''
    Wrapper for a cutscene asset.
    '''
    def __init__( self, assetFilename ):
        self.__assetFilename = assetFilename
        self.__assetName = os.path.basename( assetFilename ).split( '.' )[ 0 ]
        
    
    # Properties #
    
    @property
    def isLoaded( self ):
        mgr = RS.Core.Reference.Manager.RsReferenceManager()
        mgr.collectSceneReferences()
        
        return self.__assetName in mgr.references
        
    @property
    def filename( self ):
        return self.__assetFilename
    
    @property
    def name( self ):
        return self.__assetName
    
    
    # Methods #
    
    def select( self ):
            
        # Deselect everything else first.
        for component in FBSystem().Scene.Components:
            component.Selected = False
            
        # Select everything for this asset.
        FBSystem().Scene.NamespaceSelectContent(self.__assetName, True)
        FBSystem().Scene.NamespaceSelectContent('{0}_Ctrl'.format( self.__assetName ), True)        
        
        # Select reference null as well.
        refSceneNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        refSceneNull.Selected = True
        
        refNull = FBFindModelByLabelName( 'RS_Null:{0}'.format( self.__assetName ) )
        refNull.Selected = True
            
    def unload( self ):
        if self.isLoaded:
            self.select()
            
            refSceneNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
            refSceneNull.Selected = False
            
            # Delete selection.
        
        return False
    
    def load( self):
        if os.path.isfile( self.__assetFilename ):
            if not self.isLoaded:                                                  
                return self.__assetFilename
            else:
                print 'Cutscene asset already loaded: {0}'.format( self.__assetName )
            
        return False
    
    def save( self ):
        self.select()
        
        options = FBFbxOptions( False )
        options.SaveSelectedModelsOnly = True
        
        if FBApplication().FileSave( self.__assetFilename, options ):
            print 'Saved cutscene asset: {0}'.format( self.__assetName )
            
            return True
        
        return False
        

class RsCutsceneManager( object ):
    def __init__( self, cutsceneFilename ):
        if not str( cutsceneFilename ).endswith( '.cutscene' ):
            assert 0, 'Did not choose an appropriate cutscene file!'
        
        self.__cutsceneName = os.path.basename( cutsceneFilename ).split( '.' )[ 0 ]
        self.__cutsceneRootDir = os.path.dirname( cutsceneFilename )
        self.__cutsceneAssetsDir = '{0}\\assets'.format( self.__cutsceneRootDir )
        self.__localSaveFilename = '{0}\\_local_{1}.fbx'.format( self.__cutsceneRootDir, self.__cutsceneName )
        
        self.__assets = {}

    # Properties #
    
    @property
    def localSaveFilename( self ):
        return self.__localSaveFilename
    
    @property
    def name( self ):
        return self.__cutsceneName
    
    @property
    def rootDir( self ):
        return self.__cutsceneRootDir
    
    @property
    def assetsDir( self ):
        return self.__cutsceneAssetsDir
    
    
    # Methods
    def getAssetsFromDisk( self ):
        assetFiles = glob.glob( '{0}\\*.fbx'.format( self.__cutsceneAssetsDir ) )
        
        for assetFile in assetFiles:
            assetName = os.path.basename( assetFile ).split( '.' )[ 0 ]
            self.__assets[ assetName ] = RsCutsceneAsset( assetFile )
                
        return self.__assets
    
    def getAssetsFromScene( self ):
        assets = {}
        diskAssets = self.getAssetsFromDisk()
            
        mgr = RS.Core.Reference.Manager.RsReferenceManager()
        mgr.collectSceneReferences()
        
        for refName, ref in mgr.references.iteritems():
            if refName in diskAssets:
                assets[ refName ] = diskAssets[ refName ]
                
        return assets
    
    # Uses FBX Python SDK
    def loadAssetOffline( self, assetName, sdkManager, refContainer, profileRoot = None):
        if assetName in self.__assets:
            asset = self.__assets[ assetName ]
            
            if asset:
                
                filePath = asset.load()
                
                # Create a new scene so it can be populated by the first file.
                resourceContainer = FbxCommon.FbxScene.Create(sdkManager,"resourceContainer") 
                lResult = FbxCommon.LoadScene(sdkManager, resourceContainer, filePath)
                if lResult == False:
                    print("\n\nAn error occurred while creating the scene...\n")
                    sdkManager.Destroy()
                    sys.exit(1)                 
                
                # Move Scene
                fbxHelper.moveAllSceneObjects(refContainer, resourceContainer)
                
                # Move Connections
                fbxHelper.moveAllConnections(refContainer, resourceContainer)
                
                print 'Cutscene loaded: {0}'.format( assetName )
                
                # Destroy - this needs to be done every file iteration, so that the memory that is used is released
                resourceContainer.Destroy()
                return True
            
        return False
    
    # Uses MotionBuilder Python SDK 
    def loadAssetOnline( self, assetName, profileRoot = None):
        if assetName in self.__assets:
            asset = self.__assets[ assetName ]
            
            if asset:                
                
                RS.Utils.Profiler.start( 'Entire Online Merge (one asset)  - Merge & Save:'.format(assetName), profileRoot )
                filePath = asset.load()
                FBApplication().FileMerge(filePath) 
                FBApplication().FileSave(self.localSaveFilename)
                RS.Utils.Profiler.stop()
                return True
            
        return False
        
        
    def loadAssets( self, assetNamesList ):
        if type( assetNamesList ) != types.ListType and type( assetNamesList ) != types.TupleType:
            assetNamesList = [ assetNamesList ]
                
        #There is only one file do not need offline merge
        if len(assetNamesList) == 1:            
            profileRoot = RS.Utils.Profiler.start( 'Entire Online Merge (one asset) - Including Merge & Save & Bookkeeping. ')
            self.loadAssetOnline(assetNamesList[0], profileRoot)            
            RS.Utils.Profiler.stop()            
            
        # More then one item selected, need to do offline merge
        else:        
            profileRoot = RS.Utils.Profiler.start( 'Entire Offline Merge - Including Save & Open. ')
            RS.Utils.Profiler.start( 'Entire Offline Merge - Excluding Save & Open. ', profileRoot)    
            
            # Prepare the FBX SDK and the referenceContainer.
            (sdkManager, refContainer) = FbxCommon.InitializeSdkObjects()                        
            
            RS.Utils.Profiler.start( 'First Asset No Merge Required - {0}'.format(assetNamesList[0] ), profileRoot )            
            if assetNamesList[0] in self.__assets:
                 asset = self.__assets[ assetNamesList[0] ]      
                 if asset:
                    filePath = asset.load()   
                    lResult = FbxCommon.LoadScene(sdkManager, refContainer, filePath)
                    print 'Cutscene loaded: {0}'.format( assetNamesList[0] )
                    if lResult == False:
                        print("\n\nAn error occurred while creating the scene...\n")
                        sdkManager.Destroy()
                        sys.exit(1)                    
                    
            assetNamesList.pop(0)
            
            RS.Utils.Profiler.stop()
            
            for assetName in assetNamesList:
                RS.Utils.Profiler.start( 'Individual Merge - {0}'.format(assetName), profileRoot )
                self.loadAssetOffline( assetName, sdkManager, refContainer)
                RS.Utils.Profiler.stop()
                
            RS.Utils.Profiler.stop()
            
            # File Save Option
##            RS.Utils.Profiler.start( 'Saving to Disk', profileRoot )
##            lResult = FbxCommon.SaveScene(sdkManager, refContainer, self.localSaveFilename)  
##            if lResult == False:
##                print("\n\nAn error occurred while saving the scene...\n")
##                sdkManager.Destroy()
##                sys.exit(1)  
##
##            refContainer.Destroy()
##            sdkManager.Destroy()                  
##            
##            RS.Utils.Profiler.stop()   
##             
##            RS.Utils.Profiler.start( 'Opening File in MotionBuilder', profileRoot ) 
##                    
##            # - ##################################################################### - #            
##            # Testing Code to see I can speed up the File Open            
##            #lOptions = FBFbxOptions(True)
##            #lOptions.CacheSize = 104857600            
##            #FBApplication().FileOpen(self.localSaveFilename, False, lOptions)   
##            # - ##################################################################### - #
##            
##            FBApplication().FileOpen(self.localSaveFilename)  


            buff = ctypes.create_string_buffer(refContainer.read())
            FBApplication().FileOpen(addressof(buff), sizeof(buff)) # You can open this file from memory as many times as you want by executing this line.
            RS.Utils.Profiler.stop()  
            RS.Utils.Profiler.stop() 
            
        return True
            
    def saveAsset( self, assetName ):
        if assetName in self.__assets:
            asset = self.__assets[ assetName ]
            
            if asset:
                return asset.save()
            
        return False
    
    def saveAssets( self, assetNamesList ):
        if type( assetNamesList ) != types.ListType and type( assetNamesList ) != types.TupleType:
            assetNamesList = [ assetNamesList ]        

        for assetName in assetNamesList:
            self.saveAsset( assetName )
            
    def saveCutsceneLocally( self ):
        FBApplication().FileSave( self.localSaveFilename )
        

def convertSceneToAssembler(cutsceneFilenPath):
    '''
    Converts an existing cutscene to the new Referencing System 2.0 staging idea thing.
    
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''
    mgr = RS.Core.Reference.Manager.RsReferenceManager()
    mgr.collectSceneReferences()
    
    if mgr.references:
        assets = mgr.references.keys()
        assets.sort()
        
        dlg = RS.Utils.Widgets.RsVerticalListDialog( 'Choose Assets to Save Out', size = [ 300, 300 ], choices = assets, multiChoice = True )
        
        if dlg.value:
                        
            # Select each asset chosen from the list.
            for assetName in dlg.value:
                
                FBBeginChangeAllModels()
                
                # Deselect/select as needed
                character = None
                for component in FBSystem().Scene.Components:
                    if component.OwnerNamespace != None:
                        if( component.OwnerNamespace.Name == assetName and component.OwnerNamespace.OwnerNamespace == None ):
                            component.Selected = True
                            if( type(component) == FBCharacter ):
                                character = component
                        else: component.Selected = False
                    else: component.Selected = False                            
                
                #Select character models
                if character != None:
                    character.SelectModels( True, True, True, True )
                
                FBEndChangeAllModels()
                
                # Select reference null as well.
                refSceneNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
                
                if refSceneNull:
                    refSceneNull.Selected = True
                
                refNull = FBFindModelByLabelName( 'RS_Null:{0}'.format( assetName ) )
                
                if refNull:
                    refNull.Selected = True            
            
                # Save to cutscene assets directory.
                options = FBFbxOptions( False )
                options.SaveSelectedModelsOnly = True
                        
                if not FBApplication().FileSave( '{0}\\assets\\{1}.fbx'.format( cutsceneFilenPath, assetName ), options ):
                    FBMessageBox( 'Rockstar', 'An error occurred while trying to save the asset "{0}"!'.format( assetName ), 'OK' )
    
def loadCutscene( cutsceneFilename, options = RsCutsceneOptions() ):
    '''
    Open an existing cutscene.
    '''
    cutsceneManager = RS.Core.Reference.New.getCutsceneManager( cutsceneFilename )
    
    if cutsceneManager:
        assets = cutsceneManager.getAssetsFromDisk()
        assetsList = assets.keys()
        assetsList.sort()
        
        dlg = RS.Utils.Widgets.RsVerticalListDialog( 'Choose Cutscene Assets to Load', size = [ 300, 300 ], choices = assetsList, multiChoice = True ) 
        
        if dlg.value:
            if options:
                if options.newFile:
                    FBApplication().FileNew()
                
            cutsceneManager.loadAssets( dlg.value )            
            return True
    
    return False
     
def saveCutscene():
    '''
    Saves all assets in the currently loaded cutscene.
    '''
    cutsceneManager = RS.Core.Reference.New.getCutsceneManager()
    
    if cutsceneManager:
        assets = cutsceneManager.getAssetsFromScene()
        
        cutsceneManager.saveAssets( assets.keys() )
        cutsceneManager.saveCutsceneLocally()
        return True
    
    else:
        FBMessageBox( 'Rockstar', 'There is no cutscene currently loaded to save!', 'OK' )

    return False

def saveCutsceneAs( cutsceneFilename ):
    cutsceneManager = RS.Core.Reference.New.getCutsceneManager()
    
    if cutsceneManager:
        assets = cutsceneManager.getAssetsFromScene()
        assetsList = assets.keys()
        assetsList.sort()
        
        # Pop-up list box for user to choose which assets to save out.
        dlg = RS.Utils.Widgets.RsVerticalListDialog( 'Choose Cutscene Assets to Save', size = [ 300, 300 ], choices = assetsList, multiChoice = True )
            
        if dlg.value:
            
            saveDialog = FBFilePopup()
            saveDialog.Caption = "Save Cutscene As..."
            saveDialog.Style = FBFilePopupStyle.kFBFilePopupSave
            saveDialog.Filter = "*.cutscene" 
            
            saveResult = saveDialog.Execute()
            
            if saveResult:
                cutsceneFilename = '{0}\\{1}'.format( saveDialog.Path, saveDialog.FileName )
                
                # Set cutscene manager to point at new cutscene filename.
                cutsceneManager = RS.Core.Reference.New.getCutsceneManager( cutsceneFilename )
            
                return True
    
    return False
