'''
Reference Editor Paths

Put all paths here to keep things nice and tidy
'''
import RS.Config

lResourceDirectory = "{0}/art/animation/resources/...fbx".format( RS.Config.Project.Path.Root ) 


lRefSysCorePath =  r"{0}\RS\Core\Reference\Lib.py".format( RS.Config.Script.Path.Root )
lRefSysManagerPath = r"{0}\RS\Core\Reference\Manager.py".format( RS.Config.Script.Path.Root )
lRefSysUIPath = r"{0}\RS\Tools\UI\ReferenceEditor.py".format( RS.Config.Script.Path.Root )  


lRefAnimSource = '{0}\\art\\animation\\cutscene\\'.format( RS.Config.Project.Path.Root )

lRefAnimResources = "{0}\\art\\animation\\resources".format( RS.Config.Project.Path.Root )


	