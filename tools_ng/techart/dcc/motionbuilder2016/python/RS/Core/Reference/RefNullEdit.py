###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_RefSys_RefNullEdit.py
## Written And Maintained By: Kristine Middlemiss & Kathryn Bodey
## Contributors: 
## Description: Functions to replace Namespaces in the scene including the asset path.
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Config
import RS.Tools.UI.ReferenceEditor

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: ListCallback
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def ListCallback(control, event):   
    RSNull = FBFindModelByLabelName("RS_Null:" + control.Items[control.ItemIndex])
    if RSNull:      
        Namespace = RSNull.PropertyList.Find("Namespace").Data
        Path = RSNull.PropertyList.Find("Reference Path").Data
        control.editNamespace.Text = Namespace
        control.editPath.Text = Path

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: FillPath
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def FillPath(control, event):    
    lFp = FBFilePopup()
    lFp.Caption = "FBFilePopup example: Select a file"
    lFp.Style = FBFilePopupStyle.kFBFilePopupOpen
    lFp.Filter = "*"
    lFp.Path = r"{0}\art\animation\resources".format( RS.Config.Project.Path.Root )
    lRes = lFp.Execute()
    if lRes:
        control.editPath.Text = lFp.Path + "\\" + lFp.FileName
        filePartition = lFp.FileName.partition(".")
        control.editNamespace.Text = filePartition[0]
        
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: CommitChange
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
def NamespaceReplace(pSceneType, pOldName, pNewName):
    for component in pSceneType:            
        lNamespace = component.LongName.split(":") # Need to pull out the namespace to make sure we only get ones that match exactly
        if lNamespace[0] == pOldName:
            component.LongName = pNewName + ":" + component.Name   
       
#  Update Path Noctice For Future Coders: The FBKeyingGroups don't get changed on this code, so if you were to query them, you would see they don't match the path name, however as soon at the
#  user updates the reference using the reference system, then the new FBKeyingGroups will come in with the new reference, so you don't have to worry about it. They really shouldn't be working
#  with the file without updating the reference, because it's this half file, geometry from old path, reference tags from new path.
def CommitChange(control, event):
    
    if control.editNamespace.Text == "" or control.editPath.Text == "":
        FBMessageBox( "Error", "Please Fill in a Namespace and/or a Path And Try Again", "OK" )
    else:
        cl = FBComponentList()
        FBFindObjectsByName (control.controls.Items[control.controls.ItemIndex], cl, False, True)
        RSNull = cl[0]
        lScene = FBSystem().Scene

        if control.editNamespace.Text.endswith(RSNull.Name):
            FBMessageBox("Naming Error", "Please rename your new namespace so the new word is the end of the old namespace or make it completely new namespace from the original.", "Ok") 
            return
        
        # Code Logic for Namespace Replace
        if control.editNamespace.Text != "":               
            for component in lScene.Components:
                component.ProcessObjectNamespace (FBNamespaceAction.kFBReplaceNamespace, RSNull.Name, control.editNamespace.Text, False)  
                
            for component in lScene.Components:
                if not component.Is(FBNamespace.TypeInfo):
                    if component.Name == "Decklink Video Capture" or component.Name == "Blackmagic WDM Capture":
                        pass
                    else:
                        lNamespace = component.LongName.split(":") # Need to pull out the namespace to make sure we only get ones that match exactly    
                        if lNamespace[0] == RSNull.Name:
                            component.LongName = control.editNamespace.Text + ":" + component.Name                                  
                
            NamespaceReplace(lScene.Groups, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Characters, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.CharacterExtensions, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Materials, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Folders, RSNull.Name, control.editNamespace.Text)                        
            NamespaceReplace(lScene.Takes, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Cameras, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Textures, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Materials, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Shaders, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Deformers, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Devices, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Constraints, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Lights, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.AudioClips, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.VideoClips, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.MotionClips, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Notes, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Poses, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.ObjectPoses, RSNull.Name, control.editNamespace.Text)       
            NamespaceReplace(lScene.Actors, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.ActorFaces, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.MarkerSets, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.ControlSets, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.CharacterFaces, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.CharacterPoses, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.UserObjects, RSNull.Name, control.editNamespace.Text)        
            NamespaceReplace(lScene.Sets, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.Handles, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.ConstraintSolvers, RSNull.Name, control.editNamespace.Text)            
            NamespaceReplace(lScene.PhysicalProperties, RSNull.Name, control.editNamespace.Text)        

            # Need to rememeber the original namespace
            OrigNamespace = RSNull.Name
            # Update the UI with new new namespace
            RSNull.Name = control.editNamespace.Text
            
            #Update RS_Null properties with the correct information
            lPropNameSpace = RSNull.PropertyList.Find("Namespace")
            if lPropNameSpace:
                lPropNameSpace.Data = control.editNamespace.Text            
                
        # Code Logic for Path Replace        
        if control.editPath.Text != "":
            OrigPathObj = RSNull.PropertyList.Find("Reference Path")
            if OrigPathObj:
                OrigPath = OrigPathObj.Data           
                
            #Update RS_Null properties with the correct information
            lPropRefPath = RSNull.PropertyList.Find("Reference Path")
            if lPropRefPath:
                lPropRefPath.Data = control.editPath.Text
        
            # Need to dissect old path to get the file name minus .FBX.
            lOldFileName = None
            
            lPathParts = OrigPath.split("\\")
            for lPart in lPathParts:
                if '.FBX' in lPart or '.fbx' in lPart:
                    lOldFileName = lPart.split('.')
                    break  
                
            # Need to for the property UDP3DSMAX later in the code.
            if lOldFileName != None:
                lOldFileNameUpper = lOldFileName[0].upper()             
        
            # Need to dissect old path to get the file name minus .FBX.
            lNewFileName = None
            lNew = control.editPath.Text 
        
            lNewPathParts = lNew.split("\\")
            for lPart in lNewPathParts:
                if '.FBX' in lPart or '.fbx' in lPart:
                    lNewFileName = lPart.split('.')
                    break                 
        
            # Need to for the property UDP3DSMAX later in the code.
            if lNewFileName != None:
                lNewFileNameUpper = lNewFileName[0].upper() 
                
            #for component in lScene.Components:
                #if not component.Is(FBKeyingGroup.TypeInfo):                
            
        # This is done when resetting the Namespace and the Path. 
        for lComp in lScene.Components:
            lNamespace = lComp.LongName.split(":")
            
            if lNamespace[0] == RSNull.Name: 
                # Replace the Object names to match the path name.
                # If Path Changes, you need to update the mesh name and the Group name.
                if lComp.Name == lOldFileName[0]:
                    lComp.Name = lNewFileName[0]                        
                if lComp.Name == lOldFileName[0] + "_Control":
                    lComp.Name = lNewFileName[0] + "_Control"     
                for lProp in lComp.PropertyList:                        
                        # This is needed to match the new path
                    if lProp.Name == "Asset_Path":
                        #Replaces Path with new one or old one, we will do it either way.
                        lProp.Data = control.editPath.Text                            
                        # Update the MAXPATH file
                    # If Path changes, you need to change the property Dummy01::UDP3DSMAX Value to the new PATH.
                    if lProp.Name == "UDP3DSMAX":
                        l3DSMAXFILEName = None
                        l3DSMAX = lProp.Data 
                        
                        l3DSMAXUpper = l3DSMAX.upper()                            
                        l3dsParts = l3DSMAXUpper.split("\\")                            
                        
                        lReassemble3DSData = ""
                        
                        # Dissecting the value of the property so I can replace with path name
                        for lP in l3dsParts:
                            if '.MAX' in lP:
                                l3dsFileName = lP.split('.')  
                                lReassemble3DSData = lReassemble3DSData + '\\' + lNewFileNameUpper + "." + l3dsFileName[1]
                            elif lP == lOldFileNameUpper:
                                lReassemble3DSData = lReassemble3DSData + '\\' + lNewFileNameUpper
                            else:
                                # Don't need the backslash if this is the first item in the string
                                if lReassemble3DSData == "":
                                    lReassemble3DSData = lP
                                else:
                                    lReassemble3DSData = lReassemble3DSData + '\\' + lP   
                        lProp.Data = lReassemble3DSData                
                    
                    # This is needed to match the new namespace
                    if lProp.Name == "rs_Type":
                        # Dissecting the values of the property so I can replace with the new name
                        lReassembleData = ""
                        try:                                
                            lDataValue = lProp.Data.split(":")  
                            for lItem in lDataValue:                                    
                                if lItem == OrigNamespace:                                     
                                    lReassembleData = lReassembleData + ":" + RSNull.Name
                                else:
                                    # Don't need the colon if this is the first item in the string
                                    if lReassembleData == "":
                                        lReassembleData = lItem
                                    else:
                                        lReassembleData = lReassembleData + ":" + lItem                                     
                            lProp.Data = lReassembleData  
                        except:
                            pass
        
        # Need to manually remove the namespace from the Navigator > Namespaces, even though it's not used anymore.        
        for lNamespace in lScene.Namespaces:
            if lNamespace.Name == OrigNamespace:
                lNamespace.FBDelete()        
        
        # Refresh UI
        RS.Tools.UI.ReferenceEditor.Run()        