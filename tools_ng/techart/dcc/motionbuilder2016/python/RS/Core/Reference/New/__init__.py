
__CUTSCENE_MANAGER = None

def getCutsceneManager( cutsceneFilename = None ):
    global __CUTSCENE_MANAGER
    
    if __CUTSCENE_MANAGER and cutsceneFilename == None:
        return __CUTSCENE_MANAGER
    
    elif cutsceneFilename != None:
        import RS.Core.Reference.New.Scene
        
        __CUTSCENE_MANAGER = RS.Core.Reference.New.Scene.RsCutsceneManager( cutsceneFilename )
        return __CUTSCENE_MANAGER
    
    else:
        return None
    