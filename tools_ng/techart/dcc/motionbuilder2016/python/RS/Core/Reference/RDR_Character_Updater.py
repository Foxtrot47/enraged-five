'''
Script Path: RS/Core/Reference/RDR_Character_Updater.py

Description: For fully updating a self.Character - new one brought in and animation copied across from old version.
'''

import pyfbsdk as  mobu
import sys

from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character

#reload(sys.modules[Manager.__module__])

# Set player control to start frame
mobu.FBPlayerControl().GotoStart()

# Create manager and load data
manager = Manager()

# Update just characters. Pass True to force update.
manager.UpdateReferences(manager.GetReferenceListByType(Character), True)

# Show log
#manager.ShowLog()