'''

 Script Path: RS/Core/Reference/GSPairing.py

 Written And Maintained By: Kathryn Bodey

 Created: 16.10.2014

 Description: Classes for use in Ref System GS Pairing Tab

'''

from pyfbsdk import *

import RS.Globals
import RS.Utils


class ActorAnimManager:

	def __init__( self ):
		self.ActorAnimList = []
		self.PropertyReferenceList = []
		self.CharacterNameList = []
		self.ReferenceScene = None
		
		# get reference null
		for component in RS.Globals.gComponents:
			if component.LongName == 'REFERENCE:Scene' and component.ClassName() == 'FBModelNull':    
				self.ReferenceScene = component		
		
		if self.ReferenceScene:		
		
			# populate character list
			refNullList = []
			RS.Utils.Scene.GetChildren( self.ReferenceScene, refNullList, "", False )
			if len( refNullList ) > 0:
				for i in refNullList:
					typeProperty = i.PropertyList.Find( 'rs_Asset_Type' )
					if typeProperty:
						if typeProperty.Data == 'Characters':
							self.CharacterNameList.append( i.Name )	
							
			# populate reference propertylist
			self.PopulateReferenceProperty()			
				
			# populate actor anim list
			for i in RS.Globals.Components:
				actorDataProperty = i.PropertyList.Find( 'GiantAnim' )
				if 'SKEL_ROOT' in i.Name and actorDataProperty:
					foundProperty = False
					# check if a reference property for this anm pre-exists
					for prop in self.PropertyReferenceList:
						if actorDataProperty.Data == prop.AnimReferenceProperty.Name:
							self.ActorAnimList.append( ActorAnim( actorDataProperty.Data, prop.CharacterReferenceData ) )
							foundProperty = True
					# property doesnt pre-exist
					if foundProperty == False:
						self.ActorAnimList.append( ActorAnim( actorDataProperty.Data, "None" ) )


	def PopulateReferenceProperty( self ):
		self.PropertyReferenceList = []		
		
		for i in self.ReferenceScene.PropertyList:
			if '.anm' in i.Name:
				# check character still exists in scene
				found = False
				for char in self.CharacterNameList:
					if i.Data == char:
						self.PropertyReferenceList.append( ReferenceProperty( i, i.Data ) )
						found = True
				if found == False:
					self.PropertyReferenceList.append( ReferenceProperty( i, "None" ) )

				
	def GetUnpairedCharacterNames( self ):
		unpairedCharacterNameList = list( self.CharacterNameList )
		for i in self.ActorAnimList:
			if i.CharacterName != "None":
				unpairedCharacterNameList.remove( i.CharacterName )
		return unpairedCharacterNameList


class ActorAnim:
	
	def __init__( self, AnimName, CharacterName ):
		self.AnimName = AnimName
		self.CharacterName = CharacterName


class ReferenceProperty:
	
	def __init__( self, AnimReferenceProperty, CharacterReferenceData ):
		self.AnimReferenceProperty = AnimReferenceProperty
		self.CharacterReferenceData = CharacterReferenceData	


class ManagedMenuSpreadCell:
	
	def __init__( self, OwnerSpread, CellIndex, RowIndex ):
		self.OwnerSpread = OwnerSpread
		self.CellIndex = CellIndex
		self.RowIndex = RowIndex
		self._internalCellValue = ""
		
	def SetCellValues( self, CellValueList ):
		self._internalCellValue = ""
		for i in CellValueList:
			self._internalCellValue += ( i + "~" )
		self.OwnerSpread.SetCellValue( self.RowIndex, self.CellIndex, self._internalCellValue )
	
	def GetCurrentCellValue( self ):
		index = self.OwnerSpread.GetCellValue( self.RowIndex, self.CellIndex )
		internalCellValueSplitList = self._internalCellValue.split( "~" )
		return internalCellValueSplitList[index]
	
	def SetCurrentCellValue( self, CellValue ):
		internalCellValueSplitList = self._internalCellValue.split( "~" )
		for i in internalCellValueSplitList:
			if i == CellValue:
				self.OwnerSpread.SetCellValue( self.RowIndex, self.CellIndex, internalCellValueSplitList.index( i ) )
		
	
	
			
		