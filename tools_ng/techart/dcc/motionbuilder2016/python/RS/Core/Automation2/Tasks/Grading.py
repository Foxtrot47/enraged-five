'''
Author: Ross George

Description: 
    Processes a scene for grading information.
'''
import shutil
import os
import math
import time
import random
import shutil
import hashlib
import colorsys
import uuid
import subprocess
import re
import zipfile

from pyfbsdk import *
import fbx


import RS.Core.Automation2
import RS.Core.Metadata as Metadata
import RS.Config
import RS.Globals
import RS.Perforce as Perforce
import RS.Utils.Scene
import RS.Utils.Scene.Model
import RS.Utils.Scene.Skin
import RS.Utils.Logging.Universal
import RS.Utils.Color
import RS.Utils.Instrument
import RS.Utils.Math

from System import DateTime
from System.Xml import XmlTextReader
from System.Xml import XmlDocument
import datetime
import time

#We don't use techart path here because we want the project techart path not WW
GRADING_CONFIG_FILE_PATH = os.path.join( RS.Config.Tool.Path.Root, r"techart\etc\config\grading\config.xml" )
GRADING_IMAGE_PROCESSOR = os.path.join( RS.Config.Tool.Path.Root, r"techart\bin\GradingImageProcessor\GradingImageProcessor.exe" )

class GradingTask( RS.Core.Automation2.AutomationTaskBase ):

    def __init__( self, ownerJob, taskMetaStruct, taskID ):
        RS.Core.Automation2.AutomationTaskBase.__init__( self, ownerJob, taskMetaStruct, taskID )
        
        self._sourceCutFilePath = taskMetaStruct.SourceCutFilePath
        
        #Hack to change to non-master version. Not sure what the difference is between these
        #Ideally this would be done at a higher level but don't want to pull thread too far
        if self._sourceCutFilePath.endswith("data_master.cutxml"):
            self._sourceCutFilePath = self._sourceCutFilePath.replace("data_master.cutxml", "data.cutxml")
        
        self._sourceFbxFilePath = taskMetaStruct.SourceFbxFilePath
        self._projectName = taskMetaStruct.ProjectName
        self._missionName = taskMetaStruct.MissionName
        self._partName = taskMetaStruct.PartName
    
    def _preProcessFbx( self ):
            
        self._log.LogMessage( "Initializing FBXSDK." )
        sdkManager = fbx.FbxManager.Create()
        if not sdkManager:
            raise Exception( "Failed to initialize FBXSDK." )
        ios = fbx.FbxIOSettings.Create(sdkManager, fbx.IOSROOT)
        sdkManager.SetIOSettings(ios)
        scene = fbx.FbxScene.Create(sdkManager, "")
        
        self._log.LogMessage( "Mounting source.fbx file." )
        importer = fbx.FbxImporter.Create( sdkManager, "" )    
        result = importer.Initialize( self._fbxFilePath, -1, sdkManager.GetIOSettings())
        if not result:
            raise Exception( "Failed to mount source.fbx with FBXSDK." )
        importer.Import(scene)
        importer.Destroy()       

        self._log.LogMessage( "Processsing geometry." )
        
        for nodeIndex in range( scene.GetNodeCount() ):
            node = scene.GetNode(nodeIndex)

            geometry = node.GetGeometry()
            if isinstance( geometry, fbx.FbxMesh ):
                layer = geometry.GetLayer(0)
                material = fbx.FbxLayerElementMaterial.Create( geometry, "material" )
                material.SetMappingMode( fbx.FbxLayerElement.eByPolygon )
                material.SetReferenceMode( fbx.FbxLayerElement.eDirect )
                material.GetIndexArray().SetCount( geometry.GetPolygonCount() )      
                for i in range( geometry.GetPolygonCount() ):
                    material.GetIndexArray().SetAt(i, 0)                  
                layer.SetMaterials( material )
    
        self._log.LogMessage( "Saving out processed source.fbx." )
        exporter = fbx.FbxExporter.Create(sdkManager, "")
        fileFormat = -1
        if fileFormat < 0 or fileFormat >= sdkManager.GetIOPluginRegistry().GetWriterFormatCount():
            fileFormat = sdkManager.GetIOPluginRegistry().GetNativeWriterFormat()
            formatCount = sdkManager.GetIOPluginRegistry().GetWriterFormatCount()
            for formatIndex in range(formatCount):
                if sdkManager.GetIOPluginRegistry().WriterIsFBX(formatIndex):
                    writerFormatDescription = sdkManager.GetIOPluginRegistry().GetWriterFormatDescription(formatIndex)
                    if "FBX binary" in writerFormatDescription:
                        fileFormat = formatIndex
    
        ios = fbx.FbxIOSettings.Create(sdkManager, fbx.IOSROOT)
        ios.SetBoolProp(fbx.EXP_FBX_COMPRESS_ARRAYS, True)
        ios.SetIntProp(fbx.EXP_FBX_COMPRESS_MINSIZE, 1024) #1 Megabyte
        ios.SetIntProp(fbx.EXP_FBX_COMPRESS_LEVEL, 1)
    
        if exporter.Initialize( self._fbxFilePath, fileFormat, ios ):
            exporter.Export(scene)
            exporter.Destroy()
        else:
            raise Exception( "Failed to save out source.fbx with FBXSDK." )        

    def _createMaterial( self, MaterialName, MaterialColor ):
        material = FBMaterial( MaterialName )
        material.Emissive = MaterialColor
        material.EmissiveFactor = 1.0
        material.DiffuseFactor = 0.0
        material.SpecularFactor = 0.0
        material.AmbientFactor = 0.0
        return material    

    def _preProcessComponent( self, Component ):
        
        if issubclass( type( Component ), FBModel ):
            #Check if we are dealing with head accessories or hair
            if re.search( "HEAD.*HAT|HEAD.*HELMET|HEAD.*CAP|HEAD.*BERET|HEAD.*BEANIE|HAIR", Component.FullName, re.IGNORECASE ):
                Component.Show = False              
        
    def _getEntityTypeAndStreamingName( self, cutObjectElement ):
            
            cutObjectType = cutObjectElement.GetAttribute("type")
            
            #See if this matches any of our entity types
            for entityType in self._configFile.EntityTypes:
                if re.search( entityType.CutType, cutObjectType, re.IGNORECASE ):
                    #Extract the streaming name
                    entityStreamingName = cutObjectElement.SelectSingleNode("cName").InnerText.split(':')[0]
                    entityStreamingName = entityStreamingName.split('^')[0]
                    
                    #Use this to match the type 
                    if re.search( entityType.Pattern, entityStreamingName, re.IGNORECASE ):
                        return entityType, entityStreamingName
                        
            #We don't have any entity types that match the passed cutObjectElement so return None
            return None, None
    
    def Process( self, log, directory, changelist ):
        '''Process the grading job.'''
        
        #Record a handle on the log
        self._log = log
        
        #Sync and read grading config file
        self._log.LogMessage( "Syncing grading config file - {0}.".format( GRADING_CONFIG_FILE_PATH))
        Perforce.Sync( GRADING_CONFIG_FILE_PATH )
        
        self._log.LogMessage( "Parsing grading config file." )
        self._configFile = Metadata.ParseMetaFile( GRADING_CONFIG_FILE_PATH )
        
        #Get the project we are targetting - start with the core project.
        import RS.Config
        targetProject = RS.Config.Project
        if targetProject.Name.upper() != self._projectName.upper():
            if RS.Config.DLCProjects.has_key(self._projectName):
                targetProject = RS.Config.DLCProjects[self._projectName]
            else:
                raise Exception("Couldn't resolve the project: {0}".format(self._projectName) )
        
        #Sync the cutfile and copy it to the job directory
        self._log.LogMessage( "Syncing .cut file - {0}.".format(self._sourceCutFilePath) )
        Perforce.Sync( self._sourceCutFilePath )
        cutFileState = Perforce.GetFileState( self._sourceCutFilePath )
        if cutFileState == [] or cutFileState == None or cutFileState.HeadRevision != cutFileState.HaveRevision:
            raise Exception( "Failed to sync to the cut file" ) 
        self._cutFilePath = os.path.join( directory, "source.cut" )
        shutil.copyfile( self._sourceCutFilePath, self._cutFilePath )
        
        #Sync the fbx to the designated revision and copy it to the job directory
        self._log.LogMessage( "Syncing .fbx file - {0}.".format(self._sourceFbxFilePath) )
        Perforce.Sync( self._sourceFbxFilePath )
        fbxFileState = Perforce.GetFileState( self._sourceFbxFilePath )
        if fbxFileState == [] or fbxFileState == None or fbxFileState.HeadRevision != fbxFileState.HaveRevision:
            raise Exception( "Failed to sync to the fbx file" )        
        self._fbxFilePath = os.path.join( directory, "source.fbx" )
        shutil.copyfile( self._sourceFbxFilePath, self._fbxFilePath )
        
        #Parse the cut xml        
        #now use xml parser rather than the metadata system because metadata system 
        #doesn't play nice with the cutxmls
        self._log.LogMessage( "Parsing .cut file.".format(self._sourceCutFilePath) )
        cutXmlReader = XmlTextReader( self._cutFilePath )
        self._cutFile = XmlDocument()        
        self._cutFile.Load( cutXmlReader )        

        #Check that the source cut has some shot information
        concatDataListElement = self._cutFile.DocumentElement.SelectSingleNode("concatDataList")
        if concatDataListElement == None:
            raise Exception("No <concatDataList> element in the cut xml.")
        if concatDataListElement.ChildNodes.Count < 1:
            raise Exception("No shot information in the cut file.")
        
        #Check that the source cut has some cut object information
        cutsceneObjectsElement = self._cutFile.DocumentElement.SelectSingleNode( "pCutsceneObjects" )
        if cutsceneObjectsElement == None:
            raise Exception("No <pCutsceneObjects> element in the cut xml.")
        
        #Preprocess the fbx scene
        self._log.LogMessage( "Preprocessing source.fbx file." )
        self._preProcessFbx()
        
        #Open the fbx scene
        self._log.LogMessage( "Opening source.fbx file." )
        FBApplication().FileOpen( self._fbxFilePath )
        
        #Fix faces (jaw error)
        import RS.Core.Face.Lib
        RS.Core.Face.Lib.FixFaceInCutscene(True)
        
        #Preprocess all the components
        self._log.LogMessage("Preprocessing components" )
        for component in RS.Globals.Scene.Components:
            self._preProcessComponent( component )

        #Start our tracking dictionary
        trackedEntityManager = TrackedEntityManager()
                    
        #For each object in the cutscene
        self._log.LogMessage( r"Processing cut rage objects" )
        for cutObjectElement in cutsceneObjectsElement.ChildNodes:
            
            cutObjectID = int( cutObjectElement.SelectSingleNode( "iObjectId" ).GetAttribute("value") )
            self._log.LogMessage( r"Analyzing cut rage object. ID: {0} Type: {1}".format( cutObjectID, cutObjectElement.GetAttribute("type") ) )
    
            #Get entity type
            entityType, entityStreamingName = self._getEntityTypeAndStreamingName( cutObjectElement )
            if entityType == None:
                self._log.LogMessage( r"Ignoring cut rage object - unknown/supported type" )
                continue
            else:
                self._log.LogMessage( r"Processing entity as: {0}".format( entityType.Name ) )
                self._log.LogMessage( r"Entity name: {0}".format( entityStreamingName ) )
            
            #Get the root model. If we failed to find the root model then continue to next object. This is bad.
            entityRootModelName = str( cutObjectElement.SelectSingleNode("cName").InnerText )
            entityRootModel = FBFindModelByLabelName( entityRootModelName ) 
            if entityRootModel == None: 
                self._log.LogWarning( r"Failed to find root model: {0}".format( entityRootModelName ) )
                continue
            else:
                self._log.LogMessage( r"Obtained root model: {0}".format( entityRootModelName ) )
 
            #Get mover model
            moverModel = entityRootModel.Parent
            if 'mover' not in moverModel.Name.lower():
                self._log.LogWarning( r"Failed to find mover model." )
                continue
            else:
                self._log.LogMessage( r"Obtained mover model: {0}".format( moverModel.LongName ) )
            
            #Setup our tracked entity and add the mover to start with
            trackedEntity = trackedEntityManager.Create( entityStreamingName, entityType, entityRootModel )
            trackedEntity.SetMover( moverModel )
     
            #Get all child elements of the root model (likely be the bone models)
            childModels = list()
            RS.Utils.Scene.GetChildren( entityRootModel, childModels )
            
            rigidModels = []
            if trackedEntity.GetType().BoneExclusionPattern != None:
                for childModel in childModels:
                    
                    #If it's a proper model object then this is likely a rigid model (like a wheel)
                    if type(childModel) == FBModel:
                        rigidModels.append( childModel )
                        #Also add the model's parent as this is where the animation is found in the case of the wheels.
                        rigidModels.append( childModel.Parent ) 
                    
                    #See if the bone should be hidden - mostly windows on vehicles
                    if re.search( trackedEntity.GetType().BoneExclusionPattern, 
                                  childModel.Name, 
                                  re.IGNORECASE ):
                        self._log.LogMessage( r"Excluding bone [{0}] with pattern: {1}.".format( childModel.Name, trackedEntity.GetType().BoneExclusionPattern ) )
                        childModel.Scaling.Data = FBVector3d(0.0001,0.0001,0.0001)
                
            #Get the skin models that these models act as bones for (if any)
            skinModels = RS.Utils.Scene.Model.GetSkinModels( childModels )  
            
            #If we have some skinned models then we are dealing with a skinned object (majority of objects)
            if len( skinModels ) > 0:
                
                self._log.LogMessage( r"Processing as skinned entity." )
                
                #For each rigid model
                for rigidModel in rigidModels:
                    self._log.LogMessage( r"Processing rigid model: {0}.".format( rigidModel.Name ) )                    
                    trackedEntity.AddModel( rigidModel )
                    
                #For each skinned model
                for skinModel in skinModels:
                    self._log.LogMessage( r"Processing skinned model: {0}.".format( skinModel.Name ) )                    
                    
                    #Check the model to see if it should be excluded by pattern against name
                    if trackedEntity.GetType().ModelExclusionPattern != None:
                        if re.search( trackedEntity.GetType().ModelExclusionPattern, skinModel.Name, re.IGNORECASE ) != None:
                            self._log.LogMessage( r"Excluding model with pattern: {0}.".format( trackedEntity.GetType().ModelExclusionPattern ) )
                            skinModel.Show = False
                            continue
            
                    #If model is visible at this point then we should process it
                    if RS.Utils.Scene.Model.IsModelVisible( skinModel ):
                        trackedEntity.AddModel( skinModel )
                    else:
                        self._log.LogMessage( r"Model is not visible so ignoring." )
            
            #Otherwise we are dealing with a simple 'non-skinned' entity
            else:
                self._log.LogMessage( r"Processing as simple (flat) entity." )
                trackedEntity.AddModel( entityRootModel )
                
            #Check we actually have some models (this can happen if all of an object's models are hidden)
            if( trackedEntity.GetModelCount() == 0 ):
                trackedEntityManager.Remove( trackedEntity )
                self._log.LogWarning( r"Entity has no visible\valid models so ignoring" )
            else:
                self._log.LogMessage( r"Finished processing entity. Entity is composed of [{0}] model(s) and has [{1}] controller(s)".format( trackedEntity.GetModelCount(), trackedEntity.GetControllerCount() ) )                        
        
        if trackedEntityManager.GetEntityCount() == 0:
            raise Exception("No entities were found or visible so there is nothing to grade. Aborting.")
        
        self._log.LogMessage( r"Setting up scene for render" )
        
        #Now we have our objects collected we will gather all the models that relate to 
        #them so we can hide everything else
        modelList = trackedEntityManager.GetModels()
        
        # Need to also gather and process the set models
        setRootModels = list(component for component in RS.Globals.Components if component.Name == "SetRoot" and type(component) == FBModelNull)
        for setRootModel in setRootModels:
            setRootModelChildren = list()
            RS.Utils.Scene.GetChildren(setRootModel, setRootModelChildren)
            for child in setRootModelChildren:
                if type(child) == FBModel:
                    model = child
                    if re.search("DOOR|CYLINDER", model.Name, re.IGNORECASE):
                        model.Show = False
                    else:
                        #Fix for url:bugstar:6090297 - Removing model.Show, so that unwanted set models dont show up in the grading
                        colorValue = random.uniform(0.75, 0.95)
                        color = FBColor(colorValue, colorValue, colorValue)
                        newMaterial = self._createMaterial(model.FullName, color)
                        model.Materials.removeAll()
                        model.Materials.append(newMaterial)
                        modelList.append(model)
        
        #Hide everything that is not on this list (not the static set or exporting objects.)
        for component in RS.Globals.Components:
            if type( component ) == FBModel:
                component.Shaders.removeAll()
                component.ShadingMode = FBModelShadingMode.kFBModelShadingHard
                if component not in modelList:
                    component.Show = False
                
        #Now we start outputting the data. First get the export camera and validate it.
        exportCamera = FBFindModelByLabelName( 'ExportCamera' )
        if( type( exportCamera ) != FBCamera ):
            self._log.LogWarning("Missing export camera so falling back to camera switcher.")
            RS.Globals.System.Renderer.UseCameraSwitcher = True
            cameraProcessList = RS.Globals.Scene.Cameras #Process all cameras
        else:
            cameraProcessList = [exportCamera]
            #Make sure we are not on camera switcher (important as this overrides 'CurrentCamera')
            RS.Globals.System.Renderer.UseCameraSwitcher = False
            RS.Globals.System.Renderer.CurrentCamera = exportCamera
            
        for camera in cameraProcessList:             
            #Setup resolution and render options
            #Unlock all properties
            for prop in camera.PropertyList:
                if prop.AllowsLocking ():
                    prop.SetLocked(False)
                    
            camera.ResolutionMode = FBCameraResolutionMode.kFBResolutionCustom
            camera.ResolutionWidth = self._configFile.RenderWidth
            camera.ResolutionHeight = self._configFile.RenderHeight
            camera.ViewShowAxis = False
            camera.ViewShowGrid = False
            camera.BackGroundColor = FBColor(0.5,0.5,0.5)
            camera.FrameColor = FBColor(0.1,0.1,0.1)
            camera.UseFrameColor = True            

        #This helps with the poping in about out of objects (mobu optimization)
        RS.Globals.System.Renderer.FrustumCulling = False
        
        #Set up our render options
        grabber = FBVideoGrabber()
        renderOptions = grabber.GetOptions()
        renderOptions.CameraResolution = FBCameraResolutionMode.kFBResolutionCustom
        renderOptions.ViewingMode = FBVideoRenderViewingMode.FBViewingModeModelsOnly
        renderOptions.ShowSafeArea = False
        renderOptions.ShowTimeCode = False
        renderOptions.ShowCameraLabel = False
        renderOptions.AntiAliasing = False
        renderOptions.RenderAudio = False
        grabber.SetOptions(renderOptions)
        
        #This is scrapy but will out one file that kick starts the renderer.
        frameTime = FBTime()
        frameTime.SetFrame(1)
        renderOptions.OutputFileName = "X:\\#####.tif"
        renderOptions.TimeSpan = FBTimeSpan( frameTime, frameTime )
        RS.Globals.Player.Goto( frameTime )
        RS.Globals.App.FileRender( renderOptions )
        os.remove("X:\\00001.tif")

        #Get handle on discardFrameList element
        discardFrameListElement = self._cutFile.DocumentElement.SelectSingleNode("discardFrameList")
        
        #Output all our shot data for the part
        for concatDataElement in concatDataListElement.ChildNodes:
            
            shotName = concatDataElement.SelectSingleNode("cSceneName").InnerText             
            shotStartFrame = int( concatDataElement.SelectSingleNode("iRangeStart").GetAttribute( "value" ) )
            shotEndFrame = int( concatDataElement.SelectSingleNode("iRangeEnd").GetAttribute( "value" ) )
            shotFrameCount = shotEndFrame - shotStartFrame;
            shotDataDirectory = os.path.join( directory, shotName )
            
            #This will be replaced if we find a discard element for the shot
            shotRange = xrange( shotStartFrame, shotEndFrame ) 
            
            #Get discardFrameList element for this shot
            if discardFrameListElement != None:
                for discardFrameElement in discardFrameListElement.ChildNodes:
                
                    #Get shot name and see if it matches
                    discardFrameShotName = discardFrameElement.SelectSingleNode("cSceneName").InnerText
                    if discardFrameShotName != shotName:
                        continue
                    
                    shotRange = []                    
                    
                    #Parse the frame numbers as ints and check we have even number dor pairing
                    discardFramesUnparsed = discardFrameElement.SelectSingleNode("frames").InnerText
                    discardFrames = [int(unparsedNumber) for unparsedNumber in discardFramesUnparsed.split()]
                    if len(discardFrames) % 2 != 0:
                        break # not an even number so cannot pair
                    
                    #Add our start and 'None' as our end frame - we will use this to flag that we are on the last pair
                    frameIndexesUnpaired = list(discardFrames)
                    frameIndexesUnpaired.insert(0, shotStartFrame) #Start frame
                    frameIndexesUnpaired.append(None) #End frame flag
                    
                    #Put the frames into tuple pairs
                    frameIndexesPaired = [(frameIndexesUnpaired[index],frameIndexesUnpaired[index+1]) for index in xrange(0, len(frameIndexesUnpaired), 2)]
                    
                    #Build up a new shotRange from these pairs
                    for frameIndexPair in frameIndexesPaired:
                        
                        #If this is the last pair we will do something special
                        if frameIndexPair[1] == None:
                            
                            #Figure out frame length for this final pair
                            finalPairFrameLength = shotFrameCount - len(shotRange)
                            
                            #Create new tuple pair using existing start frame and our calculated final frame length
                            #+1 because it works that way HB said so!
                            frameIndexPair = ( frameIndexPair[0], frameIndexPair[0] + finalPairFrameLength + 1 )
                            
                        self._log.LogMessage( "Adding frame range: [{0} - {1}].".format(frameIndexPair[0], frameIndexPair[1]) )
                        for frame in xrange(frameIndexPair[0], frameIndexPair[1]):
                            shotRange.append(frame)
                
            self._log.LogMessage( "Outputting data for shot: [{0}].".format(shotName) )                
            self._log.LogMessage( "Frames count: [{0}].".format(len(shotRange) ) )

            #Keeps track of the files that need to be ziped
            filePathListForZipping = []
            
            shotInfo = trackedEntityManager.GetShotInfo()
            shotInfo.DataJobID = self._ownerJob.ID
            shotInfo.DataTimeStamp = datetime.datetime.now().strftime("%d/%m/%Y %I:%M:%S %p")
            shotInfo.SourceCutFileRevision = cutFileState.HaveRevision
            shotInfo.SourceCutFilePath = self._sourceCutFilePath
            shotInfo.SourceFbxFileRevision = fbxFileState.HaveRevision
            shotInfo.SourceFbxFilePath = self._sourceFbxFilePath       
            shotInfo.FrameCount = shotEndFrame - shotStartFrame
            shotInfoFilePath = os.path.join( shotDataDirectory, "shot.data" )        
            shotInfo.SetFilePath( shotInfoFilePath )        
            shotInfo.Save()        
            
            filePathListForZipping.append(shotInfoFilePath)
            
            frameTime = FBTime()       
            frameIndex = 0            
            for sourceFrameIndex in shotRange:
                
                #Output a message every ten frames which we use to register if MoBu has hung
                if( frameIndex % 10 == 0 ):
                    self._log.LogMessage("Processing frame: {0} ({1}).".format( frameIndex, sourceFrameIndex ))
        
                #Move to correct frame
                frameTime.SetFrame( sourceFrameIndex )
                RS.Globals.Player.Goto( frameTime )	
                
                #Process our objects and output or frame info data
                frameInfo = trackedEntityManager.GetFrameInfo()
                frameInfo.FrameIndex = frameIndex
                frameInfo.SourceFrameIndex = sourceFrameIndex
                frameInfoFileName = "{0:0>5d}.frame.data".format( sourceFrameIndex )
                frameInfoFilePath = os.path.join( shotDataDirectory, frameInfoFileName )
                frameInfo.SetFilePath( frameInfoFilePath ) 
                frameInfo.Save()
                
                #Get the name and path
                frameImageFileName = "{0:0>5d}.tif".format( sourceFrameIndex )
                frameImageFilePath = os.path.join( shotDataDirectory, frameImageFileName )                    
               
                #Using snapshot
                frameImage = grabber.RenderSnapshot()
                frameImage.WriteToTif( str(frameImageFilePath), "", True )
                if frameImage.Width != self._configFile.RenderWidth and \
                   frameImage.Height != self._configFile.RenderHeight:
                    raise Exception("Output image size is not correct. Likely viewing through "
                                    "perspective or one of the other default cameras. Switcher "
                                    "or export camera is effectively broken.")
                                
                #Send image off for post processing
                startupinfo = subprocess.STARTUPINFO()
                startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW            
                subprocess.Popen( [GRADING_IMAGE_PROCESSOR, frameImageFilePath, frameInfoFilePath, shotInfoFilePath, GRADING_CONFIG_FILE_PATH ], startupinfo=startupinfo )
                
                filePathListForZipping.append(frameInfoFilePath)
                filePathListForZipping.append(frameImageFilePath)
                
                frameIndex += 1
        
            self._log.LogMessage( "Finished outputting data for shot: [{0}].".format(shotName) )
            self._log.LogMessage( "Checking out zip for [{0}].".format(shotName) )
            
            shotZipFileDirectory = self._configFile.DataRootPath.replace( '$(assets)', targetProject.Path.Assets )
            if not os.path.exists(shotZipFileDirectory):
                os.makedirs(shotZipFileDirectory)            

            shotZipFileName = "{0} [{1}].zip".format(self._partName.upper(), shotName.upper())
            shotZipFilePath = os.path.join( shotZipFileDirectory, shotZipFileName )
            if not RS.Perforce.DoesFileExist( shotZipFilePath ):
                open(shotZipFilePath, 'w').close()
                RS.Perforce.Add( shotZipFilePath, 
                                 changelistNum = changelist, 
                                 fileType='binary' ) 
            else:
                RS.Perforce.Sync( shotZipFilePath )
                RS.Perforce.Edit( shotZipFilePath, changelist )
                shotZipFileState = RS.Perforce.GetFileState(shotZipFilePath)
                if not RS.Perforce.IsLatest( shotZipFileState ) or \
                   not RS.Perforce.IsOpenForEdit( shotZipFileState ) or \
                   not shotZipFileState.Changelist == changelist:
                    raise Exception("Failed to checkout zip file into the designated changelist - {0}".format(shotZipFilePath))
            
            self._log.LogMessage( "Zipping and verifying data for [{0}].".format(shotName) )
            zipSuccessful = False
            zipAttempt = 0            
            while not zipSuccessful:
                with zipfile.ZipFile(shotZipFilePath, "w", zipfile.ZIP_DEFLATED) as shotZipFile:
                    for filePath in filePathListForZipping:
                        fileName = os.path.basename( filePath )
                        shotZipFile.write( filePath, fileName )
                        
                zipSuccessful = True
                with zipfile.ZipFile(shotZipFilePath, "r", zipfile.ZIP_DEFLATED) as shotZipFile:
                    for shotZipEntryName in shotZipFile.namelist():
                        shotZipEntryInfo = shotZipFile.getinfo( shotZipEntryName )
                        if shotZipEntryInfo.CRC == 0:
                            zipSuccessful = False
                            break
                        
                if not zipSuccessful:
                    if zipAttempt >= 5:
                        raise Exception("After 5 attempts failed to create a valid zip.")
                    else:
                        self._log.LogMessage( "Failed to zip data - attempting again." )
                    
                        

class TrackedEntityManager:
    '''A manager that keeps a track of all the entities within a cutscene.'''
    
    def __init__( self ):
        self._entityList = []
        self._entityControllerDict = {}

        colorHueStep = 1.0 / 360
        self._availableColors = list()
        for i in range( 0, 360 ):
            colorRGB = colorsys.hsv_to_rgb( colorHueStep * i, 1.0, 1.0 )
            self._availableColors.append( FBColor( colorRGB[0], colorRGB[1], colorRGB[2]  ) )
    
    def GetEntityCount( self ):
        return self._entityList.count
    
    def GetNextColor( self ):
        chosenColor = random.choice( self._availableColors )
        self._availableColors.remove( chosenColor )
        return chosenColor              

    def Create( self, EntityName, EntityType, EntityRootModel  ):
        newTrackedEntity = TrackedEntity( self, EntityName, EntityType, EntityRootModel )
        self._entityList.append( newTrackedEntity )
        return newTrackedEntity
    
    def Remove( self, TrackedEntityToRemove ):
        self._entityList.remove( TrackedEntityToRemove )
        
    def _getController( self, Model ):
        if self._entityControllerDict.has_key( Model ):
            return self._entityControllerDict[ Model ]
        else:
            newController = TrackedEntityController( Model )
            self._entityControllerDict[ Model ] = newController
            return newController
    
    def GetModels( self ):
        modelList = []
        for trackedEntity in self._entityList:
            modelList.extend( trackedEntity.GetModels() )
        return modelList
    
    def GetFrameInfo( self ):
        
        #Evaluate on this frame
        self.Evaluate()
        
        #Output our frame info
        frameInfo = Metadata.CreateMetaFile( "GradingFrameInfo" )        

        #Get our camera
        camera = RS.Globals.System.Renderer.CurrentCamera
        cameraMatrix = FBMatrix()
        camera.GetMatrix( cameraMatrix )
        
        cameraPosition = FBVector4d()
        FBMatrixToTranslation( cameraPosition, cameraMatrix )        
        frameInfo.CameraPosition.X = cameraPosition[0]
        frameInfo.CameraPosition.Y = cameraPosition[1]
        frameInfo.CameraPosition.Z = cameraPosition[2]
        
        cameraRotation = FBVector3d()
        FBMatrixToRotation( cameraRotation, cameraMatrix, FBRotationOrder.kFBXYZ )  
        frameInfo.CameraRotation.X = cameraRotation[0]
        frameInfo.CameraRotation.Y = cameraRotation[1]
        frameInfo.CameraRotation.Z = cameraRotation[2]        
        frameInfo.CameraFOV = camera.FieldOfView.Data
        
        for trackedEntity in self._entityList:

            frameInfoEntity = frameInfo.Entities.Create()
            frameInfoEntity.ID = str( trackedEntity.GetID() )
            
            trackedEntityRootTranslation = trackedEntity.GetRootTranslation()
            frameInfoEntity.RootPosition.X = trackedEntityRootTranslation[0]          
            frameInfoEntity.RootPosition.Y = trackedEntityRootTranslation[1]
            frameInfoEntity.RootPosition.Z = trackedEntityRootTranslation[2]
            
            trackedEntityRootRotation = trackedEntity.GetRootRotation()
            frameInfoEntity.RootRotation.X = trackedEntityRootRotation[0]          
            frameInfoEntity.RootRotation.Y = trackedEntityRootRotation[1]
            frameInfoEntity.RootRotation.Z = trackedEntityRootRotation[2]            
            
            trackedEntityAverageTranslation = trackedEntity.GetAverageGlobalTranslation()
            frameInfoEntity.Position.X = trackedEntityAverageTranslation[0]          
            frameInfoEntity.Position.Y = trackedEntityAverageTranslation[1]
            frameInfoEntity.Position.Z = trackedEntityAverageTranslation[2]            
                        
            for part in trackedEntity.GetParts():
                if part.HasControllers():
                    partInfo = frameInfoEntity.Parts.Create()
                    partInfo.Name = part.GetName()
                    
                    partAverageGlobalTranslation = part.GetAverageGlobalTranslation()
                    partInfo.Position.X = partAverageGlobalTranslation[0]          
                    partInfo.Position.Y = partAverageGlobalTranslation[1]
                    partInfo.Position.Z = partAverageGlobalTranslation[2]
                         
                    partInfo.ControllerRotationMovement = part.GetRotationMovementValues()
                    partInfo.ControllerPositionMovement = part.GetPositionMovementValues()
            
        return frameInfo
    
    def GetShotInfo( self ):
        
        #Reset any previous evaluation
        self.Reset()        
        
        returnShotInfo = Metadata.CreateMetaFile( "GradingShotInfo" )
        for trackedEntity in self._entityList:
            shotInfoEntity = returnShotInfo.Entities.Create()
            shotInfoEntity.ID = str( trackedEntity.GetID() )
            shotInfoEntity.Name = str( trackedEntity.GetName() )
            shotInfoEntity.Type = str( trackedEntity.GetType().Name )
            shotInfoEntity.ModelCount = trackedEntity.GetModelCount()
            shotInfoEntity.ControllerCount = trackedEntity.GetControllerCount()            
            for model in trackedEntity.GetModels():
                modelName = shotInfoEntity.Models.Create()
                modelName.Set( self._processModelName(model.Name) )
            for part in trackedEntity.GetParts():
                partInfo = shotInfoEntity.Parts.Create()
                partInfo.Name = part.GetName()
                partInfo.Color = part.GetColorHex()
                for controller in part.GetControllers():
                    controllerName = partInfo.Controllers.Create()
                    controllerName.Set( self._processModelName( controller.GetModel().Name ) )
        return returnShotInfo
    
    def _processModelName( self, modelName ):
        '''
        Takes a string (model name) and processes it to remove certain characters and replace them with a '_'
        '''
        returnModelName = ""
        for char in modelName:
            if char.isalpha() or char.isdigit() or char in ["_", " "]:
                returnModelName += char
            else:
                returnModelName += "_"
        return returnModelName
    
    def Evaluate( self ):
        for controller in self._entityControllerDict.values():
            controller.Evaluate()
    
    def Reset(self):
        for controller in self._entityControllerDict.values():
            controller.Reset()        
            
class TrackedEntity:
    '''Tracked entity within a cutscene.'''
    
    def __init__( self, Manager, Name, Type, RootModel ):
        
        self._manager = Manager
        
        self._id = uuid.uuid1()
        self._name = Name
        self._type = Type
        
        self._controllerSet = TrackedEntityControllerSet()
        self._modelSet = set()      
        self._rootController = self._resolveController( RootModel )
        
        self._partList = list()
        for partType in self._type.Parts:
            self._partList.append( TrackedEntityPart( self, partType, self._manager.GetNextColor() ) )

    def GetID( self ):
        return self._id
    
    def GetName( self ):
        return self._name

    def GetType( self ):
        return self._type
    
    def GetRootTranslation( self ): 
        return self._rootController.GetGlobalTranslation()
    
    def GetRootRotation( self ): 
        return self._rootController.GetGlobalRotation()
    
    def GetDefaultPart( self ):
        return self._partList[-1] #Last part     
    
    def SetMover( self, MoverModel ):
        moverController = self._resolveController( MoverModel ) 
        self.GetDefaultPart().AddController( moverController )
        
    def _processBoneModel( self, BoneModel ):

        for part in self._partList:
            if part.GetRegex().search( BoneModel.Name ):
                boneController = self._resolveController( BoneModel ) 
                part.AddController( boneController )                
                return part
            
        raise Exception("Failed to match the passed bone to a part. Entity should have a part with "
                        "a regex of '.' - this is the default part. Error in the config.")        
               
    def _resolveController( self, Model ):
        controller = self._manager._getController( Model )
        self._controllerSet.add( controller )
        return controller
    
    def AddModel( self, Model ):
        
        #Add the model to our model set (mainly for debug purposes)        
        self._modelSet.add( Model ) 
        
        if Model.SkeletonDeformable:
            
            #Create vertex weight table
            vertexWeightTable = RS.Utils.Scene.Skin.VertexWeightTable( Model )
    
            #Get the model's mesh and start editing
            mesh = Model.Geometry
            mesh.GeometryBegin()
            
            #Get material index array (so we can start changing the IDs to match parts)            
            materialIndexArray = mesh.GetMaterialIndexArray()
            for polygonIndex in range( 0, mesh.PolygonCount() ):
                
                #Gather up the verts that form this polygon
                vertexIndexList = []
                for polygonVertexIndex in range( 0, mesh.PolygonVertexCount( polygonIndex ) ):
                    vertexIndexList.append( mesh.PolygonVertexIndex( polygonIndex, polygonVertexIndex ) )
                    
                #Gather up weights used by polygon
                vertexWeightList = []
                for vertexIndex in vertexIndexList:
                    vertexWeightList.append( vertexWeightTable.GetVertexWeight(vertexIndex) )
                
                #Gather up culminitive weight bone pairings
                valueDict = dict()
                for vertexWeight in vertexWeightList:
                    for entry in vertexWeight.EntryList:
                        part = self._processBoneModel( entry.Model ) 
                        if not part in valueDict:
                            valueDict[part] = 0.0
                        valueDict[part] += entry.Value
                
                #Get strongest part across the polygon
                strongestPart = None
                strongestPartValue = 0.0
                for part in valueDict:
                    
                    #Check if this part is set to is face (override any other parts)
                    if part.GetIsFace():
                        strongestPart = part
                        strongestPartValue = valueDict[part]
                        break
                    
                    #See if this part is stronger than others    
                    if strongestPart == None:
                        strongestPart = part
                        strongestPartValue = valueDict[part]
                    else:
                        if valueDict[part] > strongestPartValue:
                            strongestPart = part
                            strongestPartValue = valueDict[part]
                            
                #If we failed to find a part (because the vertex is not weighted to anything) then 
                #use the default part
                if strongestPart == None: 
                    strongestPart = self.GetDefaultPart()
                    
                #Get part material
                strongestPartMaterial = strongestPart.GetMaterial()
                if strongestPartMaterial not in Model.Materials:
                    Model.Materials.append( strongestPartMaterial )
                
                #Get the index of the material
                for index in range( len( Model.Materials ) ):
                    material = Model.Materials[index]
                    if material == strongestPartMaterial:
                        strongestPartMaterialIndex = index
                        break
                
                #Set the poly material id to the matching part material id
                materialIndexArray[polygonIndex] = strongestPartMaterialIndex
            
            #Set the material index array and stop editing the mesh    
            mesh.SetMaterialIndexArray(materialIndexArray)    
            mesh.GeometryEnd()
        else:
            part = self._processBoneModel( Model )
            Model.Materials.removeAll()
            Model.Materials.append( part.GetMaterial() )
            
    def GetAverageGlobalTranslation( self ): 
        return self._controllerSet.GetTransformAverage( GLOBAL_TRANSLATION ) 
    
    def GetPartCount( self ): 
        return len( self._partList )
    
    def GetModelCount( self ): 
        return len( self._modelSet )
    
    def GetControllerCount( self ): 
        return len( self._controllerSet )
    
    def GetParts( self ): 
        return self._partList
    
    def GetModels( self ):
        return self._modelSet        
    
    def GetControllers( self ):
        return self._controllerSet    
    
class TrackedEntityPart:
    '''A part of a tracked entity. Arm, Leg etc..'''
    
    def __init__( self, Parent, Type, Color ):

        self._parent = Parent
        self._partType = Type
        self._name = Type.Name
        self._fullName = "{0}_{1}".format( self._parent.GetName(), self._name)
        self._regex = re.compile( Type.Pattern, re.IGNORECASE )
        self._color = Color
        self._controllerSet = TrackedEntityControllerSet()
        self._isFace = Type.IsFace        
        
        self._material = FBMaterial( self._fullName )
        self._material.Emissive = self._color
        self._material.EmissiveFactor = 1.0
        self._material.DiffuseFactor = 0.0
        self._material.SpecularFactor = 0.0
        self._material.AmbientFactor = 0.0
        
    def AddController( self, Controller ): 
        self._controllerSet.add( Controller )
    
    def HasControllers( self ):
        return len(self._controllerSet) > 0
    
    def GetControllers( self ):
        return self._controllerSet 
    
    def GetAverageGlobalTranslation( self ): 
        return self._controllerSet.GetTransformAverage( GLOBAL_TRANSLATION ) 
    
    def GetRotationMovementValues( self ):
        return self._controllerSet.GetRotationMovementValues() 

    def GetPositionMovementValues( self ):
        return self._controllerSet.GetPositionMovementValues() 

    def GetMaterial( self ): 
        return self._material
    
    def GetRegex( self ):
        return self._regex
    
    def GetName(self):
        return self._name
    
    def GetIsFace(self):
        return self._isFace
    
    def GetColor(self):
        return self._color

    def GetColorHex(self):
        return RS.Utils.Color.GetHexCode( self._color )
    
            
class TrackedEntityController:
    '''A controller on a tracked entity part. Essentially a bone'''
    def __init__( self, Model ):
        
        self._model = Model

        self._previousGlobalTranslationVector = None
        self._currentGlobalTranslationVector = None
        
        self._previousLocalRotationMatrix = None
        self._currentLocalRotationMatrix = None        
    
    def GetGlobalTranslation( self ): 
        currentGlobalTranslationVector = FBVector3d()
        self._model.GetVector( currentGlobalTranslationVector, FBModelTransformationType.kModelTranslation )
        return currentGlobalTranslationVector
    
    def GetGlobalRotation( self ): 
        currentGlobalRotationVector = FBVector3d()
        self._model.GetVector( currentGlobalRotationVector, FBModelTransformationType.kModelRotation )
        return currentGlobalRotationVector
    
    def GetPositionMovement( self ):
        if self._previousGlobalTranslationVector != None:
            return math.sqrt( pow((self._previousGlobalTranslationVector[0]-self._currentGlobalTranslationVector[0]),2) + 
                              pow((self._previousGlobalTranslationVector[1]-self._currentGlobalTranslationVector[1]),2) + 
                              pow((self._previousGlobalTranslationVector[2]-self._currentGlobalTranslationVector[2]),2))        
        else:
            return 0.0
        
    def GetRotationMovement( self ):
        if self._previousLocalRotationMatrix != None:
                
                currentXAxis = FBVector3d(  self._currentLocalRotationMatrix[0], 
                                            self._currentLocalRotationMatrix[1], 
                                            self._currentLocalRotationMatrix[2])
                currentYAxis = FBVector3d(  self._currentLocalRotationMatrix[4], 
                                            self._currentLocalRotationMatrix[5], 
                                            self._currentLocalRotationMatrix[6])
                currentZAxis = FBVector3d(  self._currentLocalRotationMatrix[8], 
                                            self._currentLocalRotationMatrix[9], 
                                            self._currentLocalRotationMatrix[10])

                previousXAxis = FBVector3d( self._previousLocalRotationMatrix[0], 
                                            self._previousLocalRotationMatrix[1], 
                                            self._previousLocalRotationMatrix[2])
                previousYAxis = FBVector3d( self._previousLocalRotationMatrix[4], 
                                            self._previousLocalRotationMatrix[5], 
                                            self._previousLocalRotationMatrix[6])
                previousZAxis = FBVector3d( self._previousLocalRotationMatrix[8], 
                                            self._previousLocalRotationMatrix[9], 
                                            self._previousLocalRotationMatrix[10])
                
                xAxisChannge =  RS.Utils.Math.AngleBetweenVectors( currentXAxis, previousXAxis )
                yAxisChannge =  RS.Utils.Math.AngleBetweenVectors( currentYAxis, previousYAxis )
                zAxisChannge =  RS.Utils.Math.AngleBetweenVectors( currentZAxis, previousZAxis )
                
                return max([xAxisChannge, yAxisChannge, zAxisChannge])
        else:
            return 0.0
            
    def GetModel(self):
        return self._model
    
    def Reset(self):
        self._previousGlobalTranslationVector = None
        self._currentGlobalTranslationVector = None
        
        self._previousLocalRotationMatrix = None
        self._currentLocalRotationMatrix = None          

    def Evaluate(self):
        
        self._previousGlobalTranslationVector = self._currentGlobalTranslationVector
        self._currentGlobalTranslationVector = FBVector3d()
        self._model.GetVector( self._currentGlobalTranslationVector, FBModelTransformationType.kModelTranslation )
        
        self._previousLocalRotationMatrix = self._currentLocalRotationMatrix
        self._currentLocalRotationMatrix = FBMatrix()
        self._model.GetMatrix( self._currentLocalRotationMatrix, FBModelTransformationType.kModelRotation, False )
        
            
GLOBAL_TRANSLATION = "GetGlobalTranslation"
GLOBAL_ROTATION = "GetGlobalRotation"
LOCAL_TRANSLATION = "GetLocalTranslation"
LOCAL_ROTATION = "GetLocalRotation"

class TrackedEntityControllerSet (set):
    
    def GetTransformAverage( self, TargetTransformElement ):
        
        X = Y = Z = 0

        for controller in self:
            controllerFunc = getattr( controller, TargetTransformElement )
            controllerVector = controllerFunc()
            X += controllerVector[0]
            Y += controllerVector[1]
            Z += controllerVector[2]
         
        return FBVector3d( X / len(self),
                           Y / len(self),
                           Z / len(self))
 
    def GetPositionMovementValues( self ):
        '''
        #Returns a formatted string with all the position (global translation) based movement values.
        '''        
        movementString = ""

        for controller in self:
            controllerMovement = controller.GetPositionMovement()
            movementString += "{0:.2f}, ".format( controllerMovement )
        
        #return string with the space and comma removed from the end
        return movementString[:-2]

    def GetRotationMovementValues( self ):
        '''
        #Returns a formatted string with all the rotation based movement values.
        '''
        movementString = ""

        for controller in self:
            controllerMovement = controller.GetRotationMovement()
            movementString += "{0:.2f}, ".format( controllerMovement )
        
        #return string with the space and comma removed from the end
        return movementString[:-2]