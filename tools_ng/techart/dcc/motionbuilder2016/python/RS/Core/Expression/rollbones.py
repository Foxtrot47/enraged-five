import ast
from collections import OrderedDict
import os
import re
from xml.etree.cElementTree import parse as xmlParser
import pyfbsdk as mobu
import RS.Utils.Creation as cre

from RS.Utils.Scene import RelationshipConstraintManager as relationshipManager
#exampleFile: X:\gta5\art\peds\Story_Characters\CS_AmandaTownley
#MH_L_FingerBulge00 MH_L_Finger00


from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS.Tools import UI


class ExpressionAttributeMapping(object):
    NODE_ATTRIBUTES = ('value',
                       'channel',
                       'isVector')

    ROTATION_SUFFIX = 'Rotation'.lower()

    TRANSLATION_SUFFIX = 'Position'.lower()

    SCALING_SUFFIX = 'Scale'.lower()

    CHANNELS_MATCH = '(X_|Y_|Z_)'.lower()

    CHANNELS = ('X_'.lower(),
                'Y_'.lower(),
                'Z_'.lower())

    POSITION_MATCH = r'{0}{1}'.format(CHANNELS_MATCH,
                                      TRANSLATION_SUFFIX)

    ROTATION_MATCH = r'{0}{1}'.format(CHANNELS_MATCH,
                                      ROTATION_SUFFIX)

    SCALING_MATCH = r'{0}{1}'.format(CHANNELS_MATCH,
                                     SCALING_SUFFIX)

    MOBU_PROPERTY = ('Translation (Lcl)',
                     'Rotation (Lcl)',
                     'Scaling (Lcl)')

    BOX_PROPERTY = ('Lcl Translation',
                    'Lcl Rotation',
                    'Lcl Scaling')

    BOX_CHANNELS = ('X', 'Y', 'Z')

    IS_ANGULAR_VALUE = (False,
                        True,
                        False)

    def __init__(self,
                 inputProperty):
        self.xmlProperty = str(inputProperty)

        self.value = str(inputProperty)

        self.boxValue = str(inputProperty)

        self.channelOutput = None

        self.channel = None

        self.isVector = False

        self.isAngularData = False

        self.collect()

    def collect(self):
        for index, pattern in enumerate([self.POSITION_MATCH,
                                         self.ROTATION_MATCH,
                                         self.SCALING_MATCH]):
            if not re.match(pattern,
                            self.xmlProperty.lower()):
                continue

            self.value = self.MOBU_PROPERTY[index]

            self.boxValue = self.BOX_PROPERTY[index]

            channelData = re.match(self.CHANNELS_MATCH, self.xmlProperty.lower()).group()

            self.channel = self.CHANNELS.index(channelData.strip())

            self.channelOutput = self.BOX_CHANNELS[self.channel]

            self.isAngularData = self.IS_ANGULAR_VALUE[index]

            self.isVector = True

    def __repr__(self):
        reportData = ['\n\t\t<{0}>'.format(self.__class__.__name__)]

        for attribute in self.NODE_ATTRIBUTES:
            currentValue = getattr(self,
                                   attribute)

            reportData.append('\n\t\t\t{0}:{1}'.format(attribute,
                                                       currentValue))

        return ''.join(reportData)


class RelationConstraintNode(object):
    INPUT_A_ATTRIBUTE = 'a'

    INPUT_B_ATTRIBUTE = 'b'

    BOX_WIDTH = 80

    CATEGORY = "Number"

    NODE_TYPE = "Add (a + b)"

    def __init__(self,
                 astNode,
                 pprintSpace=1):
            self.astNode = astNode

            self.pprintSpace = pprintSpace

            self.relationBox = None

            self.category = None

            self.nodeType = None

            self.offset = self.astNode.col_offset

    def getCategory(self):
        self.category = str(self.CATEGORY)

        self.nodeType = str(self.NODE_TYPE)

    def build(self,
              constraintUtils):
        self.getCategory()

        self.relationBox = constraintUtils.AddFunctionBox(self.category,
                                                          self.nodeType)

        self.relationBox.SetBoxPosition(self.offset*-self.BOX_WIDTH, 0)

    def findAnimationNode(self,
                          inputComponent,
                          attributeName):
        for node in inputComponent.Nodes:
            if node.Name == attributeName:
                return node

        return None

    def setStaticValue(self,
                       boxNode,
                       attribute,
                       value):
        inPlugs = self.findAnimationNode(boxNode.GetBox().AnimationNodeInGet(),
                                         attribute)

        if not inPlugs:
            return

        if not isinstance(value, list):
            outputValue = [value]

            inPlugs.WriteData(outputValue)

        else:
            inPlugs.WriteData(value)

    def connect(self,
                constraintUtils,
                nodePoolArray,
                IoDictionary):
        pass

    def __repr__(self):
        reportData = ['\n{1}<{0}>'.format(self.__class__.__name__,
                                          '\t'*self.pprintSpace)]

        reportData.append('\n{2}{0}:{1}'.format('astNode',
                                                self.astNode.__dict__,
                                                '\t'*(self.pprintSpace+1)))

        return ''.join(reportData)


class ExpressionClamp(RelationConstraintNode):
    COMPARE_BOX = ['Number', 'IF Cond Then A Else B']

    MAX_BOX = ['Number', "Is Greater (a > b)"]

    MIN_BOX = ['Number', "Is Less (a < b)"]

    VALUE_BOX = ['Number', "Add (a + b)"]

    def __init__(self,
                 clampType):
        self.clampType = clampType

        self.relationBox = None

        self.conditionBox = None

        self.inputAbox = None

        self.inputBbox = None

    def build(self,
              constraintUtils):
        self.relationBox = constraintUtils.AddFunctionBox(self.COMPARE_BOX[0],
                                                          self.COMPARE_BOX[1])

        self.relationBox.SetName('{}_relationBox1'.format(self.clampType))

        if self.clampType == 'min':
            self.conditionBox = constraintUtils.AddFunctionBox(self.MIN_BOX[0],
                                                               self.MIN_BOX[1])
        else:
            self.conditionBox = constraintUtils.AddFunctionBox(self.MAX_BOX[0],
                                                               self.MAX_BOX[1])

        self.conditionBox.SetName('{}_conditionBox1'.format(self.clampType))

        self.inputAbox = constraintUtils.AddFunctionBox(self.VALUE_BOX[0],
                                                        self.VALUE_BOX[1])

        self.inputBbox = constraintUtils.AddFunctionBox(self.VALUE_BOX[0],
                                                        self.VALUE_BOX[1])

        self.inputAbox.SetName('{}_inputAbox1'.format(self.clampType))

        self.inputBbox.SetName('{}_inputBbox1'.format(self.clampType))

        constraintUtils.ConnectBoxes(self.inputAbox,
                                     'Result',
                                     self.relationBox,
                                     'a')

        constraintUtils.ConnectBoxes(self.inputBbox,
                                     'Result',
                                     self.relationBox,
                                     'b')

        constraintUtils.ConnectBoxes(self.conditionBox,
                                     'Result',
                                     self.relationBox,
                                     'Cond')

        constraintUtils.ConnectBoxes(self.inputAbox,
                                     'Result',
                                     self.conditionBox,
                                     'a')

        constraintUtils.ConnectBoxes(self.inputBbox,
                                     'Result',
                                     self.conditionBox,
                                     'b')


class ExpressionLogicNode(RelationConstraintNode):
    FLOW_OPERATION = ('CONDITION',
                      'degToRad')

    CUSTOM_FUNCTION_NAMES = ('min',
                             'max')

    CUSTOM_FUNCTIONS = ('buildMinBox',
                        'buildMaxBox')

    FLOW_OPERATION_BOX = (['Number', 'IF Cond Then A Else B'],
                          ['Converters', 'Deg To Rad'])

    def __init__(self,
                 astNode,
                 pprintSpace=1):
        super(ExpressionLogicNode, self).__init__(astNode,
                                                  pprintSpace=pprintSpace)

        self.arguments = self.astNode.args[:]

        self.function = self.astNode.func.id

        self.customBoxData = None

    def getCategory(self):
        classification = self.FLOW_OPERATION_BOX[self.FLOW_OPERATION.index(self.function)]

        self.category = classification[0]

        self.nodeType = classification[1]

    def build(self,
              constraintUtils):
        if self.function in self.FLOW_OPERATION:
            super(ExpressionLogicNode, self).build(constraintUtils)

        if self.function in self.CUSTOM_FUNCTION_NAMES:
            functionType = self.CUSTOM_FUNCTIONS[self.CUSTOM_FUNCTION_NAMES.index(self.function)]

            invokeCall = getattr(self,
                                 functionType)

            invokeCall(constraintUtils)

            self.relationBox = self.customBoxData.relationBox

    def buildMinBox(self,
                    constraintUtils):
        self.customBoxData = ExpressionClamp(self.function)

        self.customBoxData.build(constraintUtils)

    def buildMaxBox(self,
                    constraintUtils):
        self.customBoxData = ExpressionClamp(self.function)

        self.customBoxData.build(constraintUtils)

    def connect(self,
                constraintUtils,
                nodePoolArray,
                IoDictionary):
        if self.function == 'CONDITION':
            for index, attribute in enumerate(['Cond',
                                               self.INPUT_A_ATTRIBUTE ,
                                               self.INPUT_B_ATTRIBUTE]):
                if not nodePoolArray[self.arguments[index]].relationBox:
                    print 'Please build:'
                    print self.arguments[index]

                    continue

                constraintUtils.ConnectBoxes(nodePoolArray[self.arguments[index]].relationBox,
                                             'Result',
                                             self.relationBox,
                                             attribute)

        if self.function == 'degToRad':
            constraintUtils.ConnectBoxes(nodePoolArray[self.arguments[0]].relationBox,
                                         'Result',
                                         self.relationBox,
                                         self.INPUT_A_ATTRIBUTE)

        if self.function in self.CUSTOM_FUNCTION_NAMES:
            constraintUtils.ConnectBoxes(nodePoolArray[self.arguments[0]].relationBox,
                                         'Result',
                                         self.customBoxData.inputAbox,
                                         self.INPUT_A_ATTRIBUTE)

            constraintUtils.ConnectBoxes(nodePoolArray[self.arguments[1]].relationBox,
                                         'Result',
                                         self.customBoxData.inputBbox,
                                         self.INPUT_A_ATTRIBUTE)

    def __repr__(self):
        reportData = ['\n{1}<{0}>'.format(self.__class__.__name__,
                                          '\t'*self.pprintSpace)]

        reportData.append('\n{2}{0}:{1}'.format('function',
                                                self.function,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('offset',
                                                self.offset,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{1}{0}:'.format('arguments',
                                             '\t'*(self.pprintSpace+1)))

        for node in self.arguments:
            reportData.append('\n{1}{0}'.format(node,
                                                '\t'*(self.pprintSpace+2)))

        return ''.join(reportData)


class ExpressionOperationNode(RelationConstraintNode):
    MATH_OPERATION = ('Mult',
                      'Div',
                      'Add',
                      'Sub')

    MATH_OPERATION_BOX = (['Number', 'Multiply (a x b)'],
                          ['Number', 'Divide (a/b)'],
                          ['Number', 'Add (a + b)'],
                          ['Number', 'Subtract (a - b)'])

    def __init__(self,
                 astNode,
                 pprintSpace=1):
        super(ExpressionOperationNode, self).__init__(astNode,
                                                      pprintSpace=pprintSpace)

        self.operation = self.astNode.op.__class__.__name__

        self.left = self.astNode.left

        self.right = self.astNode.right

    def getCategory(self):
        classification = self.MATH_OPERATION_BOX[self.MATH_OPERATION.index(self.operation)]

        self.category = classification[0]

        self.nodeType = classification[1]

    def connect(self,
                constraintUtils,
                nodePoolArray,
                IoDictionary):
        constraintUtils.ConnectBoxes(nodePoolArray[self.left].relationBox,
                                     'Result',
                                     self.relationBox,
                                     self.INPUT_A_ATTRIBUTE)

        constraintUtils.ConnectBoxes(nodePoolArray[self.right].relationBox,
                                     'Result',
                                     self.relationBox,
                                     self.INPUT_B_ATTRIBUTE)

    def __repr__(self):
        reportData = ['\n{1}<{0}>'.format(self.__class__.__name__,
                                          '\t'*self.pprintSpace)]

        reportData.append('\n{2}{0}:{1}'.format('operation',
                                                self.operation,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('left',
                                                self.astNode.left,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('right',
                                                self.astNode.right,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('offset',
                                                self.offset,
                                                '\t'*(self.pprintSpace+1)))

        return ''.join(reportData)


class ExpressionCompareNode(RelationConstraintNode):
    COMPARE_OPERATION = ('Gt',
                         'GtE',
                         'Eq',
                         'Lt',
                         'LtE')

    COMPARE_OPERATION_BOX = (['Number', "Is Greater (a > b)"],
                             ['Number', "Is Greater or Equal (a >= b)"],
                             ['Number', "Is Identical (a == b)"],
                             ['Number', "Is Less (a < b)"],
                             ['Number', "Is Less or Equal (a <= b)"])

    def __init__(self,
                 astNode,
                 pprintSpace=1):
        super(ExpressionCompareNode, self).__init__(astNode,
                                                    pprintSpace=pprintSpace)

        self.operation = self.astNode.ops[0].__class__.__name__

        self.left = self.astNode.left

        self.right = self.astNode.comparators[0]

    def getCategory(self):
        classification = self.COMPARE_OPERATION_BOX[self.COMPARE_OPERATION.index(self.operation)]

        self.category = classification[0]

        self.nodeType = classification[1]

    def connect(self,
                constraintUtils,
                nodePoolArray,
                IoDictionary):
        constraintUtils.ConnectBoxes(nodePoolArray[self.left].relationBox,
                                     'Result',
                                     self.relationBox,
                                     self.INPUT_A_ATTRIBUTE)

        constraintUtils.ConnectBoxes(nodePoolArray[self.right].relationBox,
                                     'Result',
                                     self.relationBox,
                                     self.INPUT_B_ATTRIBUTE)

    def __repr__(self):
        reportData = ['\n{1}<{0}>'.format(self.__class__.__name__,
                                          '\t'*self.pprintSpace)]

        reportData.append('\n{2}{0}:{1}'.format('operation',
                                                self.operation,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('left',
                                                self.left,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('right',
                                                self.right,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('offset',
                                                self.offset,
                                                '\t'*(self.pprintSpace+1)))

        return ''.join(reportData)


class ExpressionNegateNode(RelationConstraintNode):
    CATEGORY = "Number"

    NODE_TYPE = "Multiply (a x b)"

    REVERSE_VALUE = -1.0

    def __init__(self,
                 astNode,
                 pprintSpace=1):
        super(ExpressionNegateNode, self).__init__(astNode,
                                                   pprintSpace=pprintSpace)

        self.negateData = self.astNode.operand

    def writeDefaultValue(self):
        self.setStaticValue(self.relationBox,
                            self.INPUT_B_ATTRIBUTE,
                            self.REVERSE_VALUE)

    def build(self,
              constraintUtils):
        super(ExpressionNegateNode, self).build(constraintUtils)

        self.writeDefaultValue()

    def connect(self,
                constraintUtils,
                nodePoolArray,
                IoDictionary):
        if not nodePoolArray[self.negateData].relationBox:
            print 'Please build:'
            print nodePoolArray[self.negateData]

            return

        constraintUtils.ConnectBoxes(nodePoolArray[self.negateData].relationBox,
                                     'Result',
                                     self.relationBox,
                                     self.INPUT_A_ATTRIBUTE)

    def __repr__(self):
        reportData = ['\n{1}<{0}>'.format(self.__class__.__name__,
                                          '\t'*self.pprintSpace)]

        reportData.append('\n{2}{0}:{1}'.format('negateData',
                                                self.negateData,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('offset',
                                                self.offset,
                                                '\t'*(self.pprintSpace+1)))

        return ''.join(reportData)


class ExpressionNumericValueNode(RelationConstraintNode):
    def __init__(self,
                 astNode,
                 pprintSpace=1):
        super(ExpressionNumericValueNode, self).__init__(astNode,
                                                         pprintSpace=pprintSpace)

        self.value = self.astNode.n

    def __repr__(self):
        reportData = ['\n{1}<{0}>'.format(self.__class__.__name__,
                                          '\t'*self.pprintSpace)]

        reportData.append('\n{2}{0}:{1}'.format('value',
                                                self.value,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('offset',
                                                self.offset,
                                                '\t'*(self.pprintSpace+1)))

        return ''.join(reportData)

    def writeDefaultValue(self):
        self.setStaticValue(self.relationBox,
                            self.INPUT_A_ATTRIBUTE,
                            self.value)

    def build(self,
              constraintUtils):
        super(ExpressionNumericValueNode, self).build(constraintUtils)

        self.writeDefaultValue()


class ExpressionVariableNode(RelationConstraintNode):
    def __init__(self,
                 astNode,
                 pprintSpace=1):
        super(ExpressionVariableNode, self).__init__(astNode,
                                                     pprintSpace=pprintSpace)

        self.variable = self.astNode.id

        self.value = 0.0

    def writeDefaultValue(self):
        self.setStaticValue(self.relationBox,
                            self.INPUT_A_ATTRIBUTE,
                            self.value)

    def addInputRelativeSpace(self,
                              attributeData,
                              constraintUtils):
        self.attributeChannel = constraintUtils.AddFunctionBox("Converters",
                                                               "Vector To Number")

        self.spaceChannel = constraintUtils.AddFunctionBox("Vector",
                                                           "Subtract (V1 - V2)")


        self.setStaticValue(self.spaceChannel,
                            'V2',
                            [0, 0, 0])

        constraintUtils.ConnectBoxes(attributeData.relationBox,
                                     attributeData.mapUtils.boxValue,
                                     self.spaceChannel,
                                     'V1')

        constraintUtils.ConnectBoxes(self.spaceChannel,
                                     'Result',
                                     self.attributeChannel,
                                     'V')

        if not attributeData.mapUtils.isAngularData:
            constraintUtils.ConnectBoxes(self.attributeChannel,
                                         attributeData.mapUtils.channelOutput,
                                         self.relationBox,
                                         self.INPUT_A_ATTRIBUTE)

        converter = constraintUtils.AddFunctionBox("Converters",
                                                   'Deg To Rad')

        constraintUtils.ConnectBoxes(self.attributeChannel,
                                     attributeData.mapUtils.channelOutput,
                                     converter,
                                     'a')

        constraintUtils.ConnectBoxes(converter,
                                     'Result',
                                     self.relationBox,
                                     self.INPUT_A_ATTRIBUTE)

    def connect(self,
                constraintUtils,
                nodePoolArray,
                IoDictionary):
        attributeData = IoDictionary[self.variable]

        if not attributeData.mapUtils.isVector:
            constraintUtils.ConnectBoxes(attributeData.relationBox,
                                         attributeData.mapUtils.value,
                                         self.relationBox,
                                         self.INPUT_A_ATTRIBUTE)

        else:
            self.addInputRelativeSpace(attributeData,
                                       constraintUtils)

    def __repr__(self):
        reportData = ['\n{1}<{0}>'.format(self.__class__.__name__,
                                          '\t'*self.pprintSpace)]

        reportData.append('\n{2}{0}:{1}'.format('variable',
                                                self.variable,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('offset',
                                                self.offset,
                                                '\t'*(self.pprintSpace+1)))

        reportData.append('\n{2}{0}:{1}'.format('relationBox',
                                                self.relationBox,
                                                '\t'*(self.pprintSpace+1)))

        return ''.join(reportData)


class ExpressionParser(object):
    START_GROUP = '('

    END_GROUP = ')'

    DEFAULT_OFFSET = 0

    FLOW_PATTERN = ('min',
                    'max',
                    'degToRad',
                    'CONDITION')

    NODE_ATTRIBUTES = ('scriptFlows',
                       'mathOperations',
                       'comparisons',
                       'negateArray',
                       'variables',
                       'podValues')

    def __init__(self,
                 pprintSpace=2):
        self.expression = ''

        self.expressionComponents = []

        self.variables = []

        self.scriptFlows = []

        self.mathOperations = []

        self.negateArray = []

        self.podValues = []

        self.comparisons = []

        self.mainEvaluation = None

        self.pprintSpace = pprintSpace

        self.nodePool = {}

        self.constraintUtils = None

        self.outputChannel = None

    def consolidateExpression(self):
        self.expression = self.expression.replace('if', 'CONDITION')

        self.expression = self.expression.replace('min (', 'min(')

        self.expression = self.expression.replace('max (', 'max(')

        self.expression = self.expression.replace('if (', 'if(')

        self.expression = self.expression.replace('degToRad (', 'degToRad(')

    def reset(self):
        self.mainEvaluation = None

        self.expressionComponents = []

        self.variables = []

        self.scriptFlows = []

        self.mathOperations = []

        self.negateArray = []

        self.podValues = []

        self.comparisons = []

        self.nodePool = {}

        self.constraintUtils = None

    def initialize(self):
        self.consolidateExpression()

        self.reset()

        expressionTree = ast.parse(self.expression)

        visitor = ast.walk(expressionTree)

        self.expressionComponents = [node
                                     for node in visitor
                                     if all([not isinstance(node, ast.Module),
                                             not isinstance(node, ast.Load)])]

    def collectComponents(self):
        self.mainEvaluation = [node
                               for node in self.expressionComponents
                               if isinstance(node, ast.Expr)][0].value

        self.scriptFlows = [ExpressionLogicNode(node,
                                                pprintSpace=self.pprintSpace+2)
                            for node in self.expressionComponents
                            if isinstance(node, ast.Call)]

        self.mathOperations = [ExpressionOperationNode(node,
                                                       pprintSpace=self.pprintSpace+2)
                               for node in self.expressionComponents
                               if isinstance(node, ast.BinOp)]

        self.negateArray = [ExpressionNegateNode(node,
                                                 pprintSpace=self.pprintSpace+2)
                            for node in self.expressionComponents
                            if isinstance(node, ast.UnaryOp)]

        self.podValues = [ExpressionNumericValueNode(node,
                                                     pprintSpace=self.pprintSpace+2)
                          for node in self.expressionComponents
                          if isinstance(node, ast.Num)]

        variables = [node
                     for node in self.expressionComponents
                     if isinstance(node, ast.Name)]

        self.variables = [ExpressionVariableNode(node,
                                                 pprintSpace=self.pprintSpace+2)
                          for node in variables
                          if node.id not in self.FLOW_PATTERN]

        self.comparisons = [ExpressionCompareNode(node,
                                                  pprintSpace=self.pprintSpace+2)
                            for node in self.expressionComponents
                            if isinstance(node, ast.Compare)]

        nodePoolArray = []

        for attribute in self.NODE_ATTRIBUTES:
            currentArray = getattr(self,
                                   attribute)

            if not currentArray:
                continue

            nodePoolArray.extend(currentArray)

        self.nodePool = {node.astNode:node
                         for node in nodePoolArray}

    def parse(self):
        self.initialize()

        self.collectComponents()

    def build(self,
              IoDictionary,
              constraintUtils=None):
        if not constraintUtils:
            self.constraintUtils = relationshipManager.RelationShipConstraintManager()

        else:
            self.constraintUtils = constraintUtils

        for node in self.nodePool.values():
            node.build(self.constraintUtils)

        for node in self.nodePool.values():
            node.connect(self.constraintUtils,
                         self.nodePool,
                         IoDictionary)

    def getOuputNode(self):
        self.outputChannel = self.nodePool[self.mainEvaluation].relationBox

    def __repr__(self):
        reportData = ['\n{1}<{0}>'.format(self.__class__.__name__,
                                          '\t'*self.pprintSpace)]

        for attribute in self.NODE_ATTRIBUTES:
            currentArray = getattr(self,
                                   attribute)

            if not currentArray:
                continue

            reportData.append('\n{1}{0}:'.format(attribute,
                                                 '\t'*(self.pprintSpace+1)))

            for item in currentArray:
                reportData.append(repr(item))

        return ''.join(reportData)


class RollExpressionDriver(object):
    ANCHOR_ATTRIBUTES = ('scalarConts',
                         'scalarNames',
                         'scalarObjects')

    NODE_ATTRIBUTES = ('driverProperty',
                       'driverVariableName',
                       'driverNode',
                       'driverSceneNode',
                       'mapUtils')

    def __init__(self,
                 xmlAnchor):
        self.ouputAttribute = None

        self.driverProperty = None

        self.driverNode = None

        self.driverSceneNode = None

        self.mobuAttribute = None

        self.mapUtils = None

        self.isValid = False

        self.relationBox = None

        self.collect(xmlAnchor)

    def getProperty(self):
        if self.driverSceneNode is None:
            return

        self.mapUtils = ExpressionAttributeMapping(self.driverProperty)

        self.mobuAttribute = self.driverSceneNode.PropertyList.Find(self.mapUtils.value)

        if self.mobuAttribute is None:
            print 'Could not find property {} on {}'.format(self.driverProperty,
                                                            self.driverNode)

            return

        self.mobuAttribute.SetAnimated(True)

    def getNode(self):
        self.driverSceneNode = mobu.FBFindModelByLabelName(self.driverNode)

        self.getProperty()

    def collect(self,
                xmlAnchor):
        for index, attribute in enumerate(self.ANCHOR_ATTRIBUTES):
            currentValue = xmlAnchor.attrib[attribute]

            setattr(self,
                    self.NODE_ATTRIBUTES[index],
                    currentValue)

        self.ouputAttribute = self.driverProperty

    def __repr__(self):
        reportData = ['\n\t<{0}>'.format(self.__class__.__name__)]

        for attribute in self.NODE_ATTRIBUTES:
            currentValue = getattr(self,
                                   attribute)

            reportData.append('\n\t\t{0}:{1}'.format(attribute,
                                                     currentValue))

        reportData.append('\n\t\t{0}:{1}'.format('relationBox',
                                                 self.relationBox))

        return ''.join(reportData)


class CustomAttributeData(object):
    ANCHOR_ATTRIBUTES = ('attrMin',
                         'attrMax',
                         'attrName',
                         'attrDefault')

    def __init__(self,
                 xmlAnchor):
        self.objName = None

        self.attrMin = None

        self.attrMax = None

        self.attrName = None

        self.attrDefault = None

        self.mobuNode = None

        self.collect(xmlAnchor)

    def collect(self,
                xmlAnchor):
        self.objName = xmlAnchor.attrib['objName']

        rollOutAnchor = xmlAnchor.find('rolloutParams')

        for attribute in self.ANCHOR_ATTRIBUTES:
            currentValue = rollOutAnchor.attrib[attribute]

            setattr(self,
                    attribute,
                    currentValue)

    def addAttributes(self):
        self.mobuNode = mobu.FBFindModelByLabelName(self.objName)

        if not self.mobuNode:
            return

        attributeType = mobu.FBPropertyType.kFBPT_double

        rollProperty = self.mobuNode.PropertyList.Find('weight')

        if rollProperty is None:
            rollProperty = self.mobuNode.PropertyCreate(self.attrName,
                                                        attributeType,
                                                        "",
                                                        True,
                                                        True,
                                                        None)

        rollProperty.SetMax(float(self.attrMax))

        rollProperty.SetMin(float(self.attrMin))

        rollProperty.SetAnimated(True)

        rollProperty.Data = float(self.attrDefault)


class RollBoneData(object):
    ANCHOR_ATTRIBUTES = ('expCont',
                         'expObj',
                         'expString')

    NODE_ATTRIBUTES = ('drivenProperty',
                       'drivenNode',
                       'expression',
                       'drivenSceneNode',
                       'mapUtils')

    ROLL_PREFIX = ('RB_',
                   'MH_',
                   'SM_')

    def __init__(self,
                 xmlAnchor,
                 expression):
        self.drivenProperty = None

        self.drivenNode = None

        self.driverArray = []

        self.expression = expression

        self.isRollBone = False

        self.drivenSceneNode = None

        self.mobuAttribute = None

        self.isValid = False

        self.mapUtils = None

        self.relationBox = None

        self.IoDictionary = {}

        self.expressionUtils = ExpressionParser()

        self.collect(xmlAnchor)

    def collect(self,
                xmlAnchor):
        for index, attribute in enumerate(self.ANCHOR_ATTRIBUTES):
            currentValue = xmlAnchor.attrib[attribute]

            setattr(self,
                    self.NODE_ATTRIBUTES[index],
                    currentValue)

        self.ouputAttribute = self.drivenProperty

        for name in  self.ROLL_PREFIX:
            if self.drivenNode.upper().startswith(name):
                self.isRollBone = True

                break

        if self.isRollBone is False:
            return

        driverItems = xmlAnchor.findall('.//{}'.format('driver'))

        self.driverArray = [RollExpressionDriver(item)
                            for item in driverItems]

    def validateComponents(self):
        self.isValid = False

        nodeArray = [item.driverSceneNode
                     for item in self.driverArray]

        propertyArray = [item.mobuAttribute
                         for item in self.driverArray]

        nodeArray.extend(propertyArray)

        nodeArray.append(self.drivenSceneNode)

        nodeArray.append(self.mobuAttribute)

        if None in nodeArray:
            return

        self.isValid = True

    def getProperty(self):
        if self.drivenSceneNode is None:
            return

        self.mapUtils = ExpressionAttributeMapping(self.drivenProperty)

        self.mobuAttribute = self.drivenSceneNode.PropertyList.Find(self.mapUtils.value)

        if self.mobuAttribute is None:
            print 'Could not find property {} on {}'.format(self.mapUtils.value,
                                                            self.drivenNode)

            return

        if 'scal' in self.mobuAttribute.Name.lower():
            self.mobuAttribute = None

            return

        self.mobuAttribute.SetAnimated(True)

    def getNodes(self):
        self.drivenSceneNode = mobu.FBFindModelByLabelName(self.drivenNode)

        self.getProperty()

        for component in self.driverArray:
            component.getNode()

    def build(self):
        self.constraintUtils = relationshipManager.RelationShipConstraintManager()

        self.constraintUtils.SetName('{}-{}'.format(self.drivenNode,
                                                    self.drivenProperty))

        self.relationBox = self.constraintUtils.AddReceiverBox(self.drivenSceneNode)

        if self.mapUtils.isVector:
            self.relationBox.SetGlobalTransforms(False)

        for item in self.driverArray:
            item.relationBox = self.constraintUtils.AddSenderBox(item.driverSceneNode)

            self.IoDictionary[item.driverVariableName] = item

            if not item.mapUtils.isVector:
                continue

            item.relationBox.SetGlobalTransforms(False)

        self.expressionUtils.expression = self.expression

        self.expressionUtils.parse()

        self.expressionUtils.build(self.IoDictionary,
                                   constraintUtils=self.constraintUtils)

        self.expressionUtils.getOuputNode()

        self.connectOutput()

    def connectOutput(self):
        if self.mapUtils.isVector:
            self.outputChannel = self.constraintUtils.AddFunctionBox("Converters",
                                                                     "Number To Vector")

            relativeSpaceOutput = self.constraintUtils.AddFunctionBox("Vector",
                                                                      "Add (V1 + V2)")

            drivenRelationBox = self.constraintUtils.AddSenderBox(self.drivenSceneNode)

            drivenRelationBox.SetGlobalTransforms(False)

            #rotation attribute needs to be converted back to radians
            if not self.mapUtils.isAngularData:
                self.constraintUtils.ConnectBoxes(self.expressionUtils.outputChannel,
                                                  'Result',
                                                  self.outputChannel,
                                                  self.mapUtils.channelOutput)

                self.constraintUtils.ConnectBoxes(self.outputChannel,
                                                  'Result',
                                                  relativeSpaceOutput,
                                                  'V1')

            else:
                converter = self.constraintUtils.AddFunctionBox("Converters",
                                                                'Rad To Deg')

                self.constraintUtils.ConnectBoxes(self.expressionUtils.outputChannel,
                                                  'Result',
                                                  converter,
                                                  'a')

                self.constraintUtils.ConnectBoxes(converter,
                                                  'Result',
                                                  self.outputChannel,
                                                  self.mapUtils.channelOutput)


            self.constraintUtils.ConnectBoxes(self.outputChannel,
                                              'Result',
                                              relativeSpaceOutput,
                                              'V1')


            self.constraintUtils.ConnectBoxes(drivenRelationBox,
                                              self.mapUtils.boxValue,
                                              relativeSpaceOutput,
                                              'V2')

            self.constraintUtils.ConnectBoxes(relativeSpaceOutput,
                                              'Result',
                                              self.relationBox,
                                              self.mapUtils.boxValue)

        else:
            #TODO
            #connect value to float attribute
            self.constraintUtils.SetActive(True)

            return

        self.constraintUtils.SetActive(True)

    def __repr__(self):
        reportData = ['\n<{0}>'.format(self.__class__.__name__)]

        for attribute in self.NODE_ATTRIBUTES:
            currentValue = getattr(self,
                                   attribute)

            reportData.append('\n\t{0}:{1}'.format(attribute,
                                                   currentValue))

        reportData.append('\n\t{0}:{1}'.format('isRollBone',
                                                 self.isRollBone))

        reportData.append('\n\tdriverArray:')

        for driverData in self.driverArray:
            reportData.append(repr(driverData))

        reportData.append(repr(self.expressionUtils))

        return ''.join(reportData)


class RollBoneManager(object):
    ROOT_TAG = 'drivers'

    DRIVEN_TAG = 'driven'

    ATTRIBUTE_TAG = 'cAtt'

    def __init__(self):
        self.definition = None

        self.customAttributes = []

        self.rollComponents = []

        self.sceneComponents = []

    def collect(self):
        if not os.path.exists(self.definition):
            return

        with open(self.definition, "r") as inputfile:
            inputTree = xmlParser(inputfile)

            rootElement = inputTree.getroot()

            if str(rootElement.tag) != self.ROOT_TAG:
                return

            attributeItems = rootElement.findall('.//{}'.format(self.ATTRIBUTE_TAG))

            self.customAttributes = [CustomAttributeData(item)
                                     for item in attributeItems]

            rollItems = rootElement.findall('.//{}'.format(self.DRIVEN_TAG))

            rollComponents = [RollBoneData(item,
                                           self.definition)
                              for item in rollItems]

            self.rollComponents = [node
                                   for node in rollComponents
                                   if node.isRollBone]

    def process(self):
        for node in self.customAttributes:
            node.addAttributes()

        for node in self.rollComponents:
            node.getNodes()

            node.validateComponents()

        self.sceneComponents = [node
                                for node in self.rollComponents
                                if node.isValid]

    def build(self):
        self.collect()

        self.process()

        sceneData = OrderedDict()
        for node in self.sceneComponents:
            sceneKey = '{}-{}'.format(node.drivenNode,
                                      node.drivenProperty)
            sceneData[sceneKey] = node

        for node in sceneData.values():
            node.build()
        
        expressionList = []
        for key in sceneData.keys():
            for constraint in mobu.FBSystem().Scene.Constraints:
                if constraint.Name == key:
                    expressionList.append(constraint)
                    
        lExpressionsFolderStr = "Expressions"
        cre.rs_CreateConstraintFolderAddComponents(expressionList, lExpressionsFolderStr)

class RollBoneConstraints():

    def createRelationConstraints(self, sourceXml):
        if not sourceXml.strip():
            return

        sourceXml = sourceXml.strip()

        if not os.path.exists(sourceXml):
            return

        xmlUtils = RollBoneManager()

        xmlUtils.definition = sourceXml

        xmlUtils.build()