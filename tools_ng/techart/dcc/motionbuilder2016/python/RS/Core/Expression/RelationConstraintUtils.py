from pyfbsdk import *
import RS.Globals as glo
import os
import sys
import string
from xml.dom import minidom
import time

    

#==========================================================================
#===  Controller parent class to hold all shared variables
#==========================================================================
class cBaseController:
    def __init__(self):
        self.name = ""
        self.box = None
        self.uid = 00000
        self.entityID = 0
        self.pos = (True, 0,0)
        self.nodeType = ""
        self.globalSpace = False
        self.inputInfo = []
        self.outputInfo = []




#==========================================================================
#===  Parent class for the manager contains all of the private functions
#==========================================================================        
class cBaseFunctions:
    def __init__(self):
        pass
        
    def stringToBool(self, stringToConvert):
        if stringToConvert.lower() == "true":
            return True
        else:
            return False
            
    
    def FindAnimationNode(self, pParent, pName):
        lResult = None
        for lNode in pParent.Nodes:
            if lNode.Name == pName:
                lResult = lNode
                break
        return lResult
    
    
    
    def XConnectBox(self, nodeOut,nodeOutPos, nodeIn, nodeInPos):
        mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
        mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )
        if mynodeOut and mynodeIn:
            FBConnect(mynodeOut, mynodeIn)

#==========================================================================
#===  AddVector class to hold any addvectors that are needed
#==========================================================================
class cAddVector(object, cBaseController):
    def __init__(self):
        cBaseController.__init__(self)
        
        #== Connection info, [[senderController, senderAttribute, inputIndex]]
        self.inputInfo = [None, None]
        self.outputInfo = []
        
        #== [[[self, connectionIN]],[[self, connectionOUT]]] 
        self.connections = []
        


#==========================================================================
#===  VectorToNumber class to hold any vector to number boxes that 
#===   are needed.
#==========================================================================
class cVectorToNumber(object, cBaseController):
    def __init__(self):
        cBaseController.__init__(self)
        
        self.inputInfo = None
        self.outputInfo = []
        
        #== [[[self, connectionIN]],[[self, connectionOUT]]] 
        self.connections = []
        
        
                

#==========================================================================        
#===  Controller class, this holds the information for any of the sender 
#===   controllers
#==========================================================================
class cSenderNode(object, cBaseController):
    def __init__(self):
        cBaseController.__init__(self)
        
        self.modelName = ""
        self.controllerType = None
        
        #== Connection info, [[senderAttribute, targetController, targetAttribute]]
        self.outputInfo = []
        
        #== [[[self, connectionIN]],[[self, connectionOUT]]] 
        self.connections = []
        

#==========================================================================
#=== Controller class to hold all of the receiving node information
#==========================================================================
class cReceiverNode(object, cBaseController):
    def __init__(self):
        cBaseController.__init__(self)
        
        self.modelName = ""
        self.controllerType = None
        
        #== Connection info, [[senderController, senderAttribute, targetAttribute]
        self.inputInfo = []
        
        #== [[[self, connectionIN]],[[self, connectionOUT]]] 
        self.connections = []
        

#==========================================================================
#=== Controller class to hold the plugin information
#==========================================================================
class cPluginNode(object, cBaseController):
    def __init__(self):
        cBaseController.__init__(self)
        self.connections = []
        self.outputInfo = []
        #extends baseController no need for vars yet...
        

#==========================================================================
#===  Holds all of the class nodes for manipulating the relation constraint
#==========================================================================
class cRelationManager(object, cBaseFunctions):
    def __init__(self, relationName, pluginMBBoxName):
        cBaseFunctions.__init__(self)
        self.name = ""
        self.characterName = ""
        self.entityIDs = {}
        self.entities = []
        self.XMLPath = ""
        self.relationInstName = relationName
        self.relationInst = None 
        self.boxes = {}
        self.senderInstances = []
        self.receiverInstances = []
        self.addVectorInstances = []
        self.pluginName = pluginMBBoxName
        self.pluginInstance = None
        self.constraintXMLPath = ""
        self.allControllers = []

    def setEntityID(self, idNum, entityName):
        self.entityID[idNum] = entityName
     
     
    #=================
    #  Test if the string is a number (INT)
    #=================   
    def stringIsNumber(self, testString):
        try:
            int(testString)
        except:
            return False
            
        return True    
        
    #=================
    #  Get the entity id of a namespace
    #=================
    def getEntityID(self, nameSpace):
        IDkey = str(nameSpace.lower())
        if IDkey in self.entityIDs.keys():
            print "FOUND ENTITY ID: '{0}'  ID:{1}".format(IDkey, self.entityIDs[IDkey])
            return self.entityIDs[IDkey]
            
        else:
            newEntityID = len(self.entityIDs.keys())
            self.entityIDs[IDkey] = newEntityID
            print "NEW ENTITY ID: '{0}'  ID:{1}".format(IDkey, newEntityID)
            return newEntityID
            

    #=================
    #  Find a controller based on the uid
    #================= 
    def findNodeByUID(self, uidRef):
        foundController = None
        for i in range(0, len(self.allControllers)):
            if int(uidRef) == int(self.allControllers[i].uid):
                foundController = self.allControllers[i]
                
        return foundController
        
    #=================
    #  Remove spaces from a string
    #=================
    def formatBoxName(self, objName):
        exceptions = ["V1", "V2"]
        if objName not in exceptions:
            lastSpace = objName.rfind(" ")
            lastPhrase = objName[lastSpace:]
            newString = objName
            if lastSpace != -1:
                if self.stringIsNumber(lastPhrase):
            
                    newString = objName[:lastSpace]
                          
            newString = newString.replace(self.characterName, "")   
            return newString
        else:
            return objName
            
            
    #=================
    #  Select a node in the relation constraint by name
    #=================
    def getNodeByNameInRelation(self, searchString):
        constraint = self.relationInst
        selected = None
           
        for cBox in constraint.Boxes:
            
            if searchString.lower() in cBox.Name.lower():
                if cBox.ClassName() == "FBModelPlaceHolder":
                   
                    selected = cBox.Model.Name
                else:
                    selected = cBox.Name                 


        if selected != None:
            return selected
        else:
            print "Nothing Selected."
            return ""
    
    #=================
    #  Return all of the connections in and out of a box
    #=================
    def getConnections(self, boxNode, inOutBool, debugBool=0):
        connections = []
        
        #== 0 = IN - receiver
        if inOutBool == 0:
            animNodes = boxNode.AnimationNodeInGet().Nodes
            for animNode in animNodes:
                for i in range(0, animNode.GetSrcCount()):
                    animSource = animNode.GetSrc(i)
                    if animSource != None:
                        
                        if self.relationInstName.lower() not in animSource.GetOwner().Name.lower():
                            #== receiver anim node, sender anim node
                            print animNode.UserName
                            connections.append([animNode, animSource])
                        
       
       #== 1 = OUT - sender
        elif inOutBool == 1:
            animNodes = boxNode.AnimationNodeOutGet().Nodes
            for animNode in animNodes:
                for i in range(0, animNode.GetDstCount()):
                    
                    animDest = animNode.GetDst(i)
                    if animDest != None:
                        if self.relationInstName.lower() not in animDest.GetOwner().Name.lower():
                            #== [sender anim node, receiver anim node]
                            connections.append([animNode, animDest]) 
                            
                        else:
                            
                            print "BAD CONN:", self.relationInstName.lower(), animDest.GetOwner().Name.lower()
        return connections                        
                        
    
    #=================
    #  Get all of the output info any node that has outputs
    #=================  
    def gatherOutputConnections(self, controller):
        outputCons = []
        for conOut in controller.connections[1]:
            if conOut[0].GetOwner().ClassName() == "FBModelPlaceHolder":
                conName = conOut[0].GetOwner().Model.Name
            else:
                conName = conOut[0].GetOwner().Name
            
            targetName = self.getNodeByNameInRelation(conOut[1].GetOwner().Name)
            #== If we are connecting to the plugin...
            if "3Lateral" in targetName:
                targetName = "#plugin#"
                
            targetName = self.formatBoxName( targetName)
            
            targetAttribute = self.formatBoxName(conOut[1].UserName)
            outputCons.append([conOut[0], targetName, targetAttribute])
            
        return outputCons
        

    def formatInputConnections(self, controller):
        print ""
    
    #=================
    # Connect a node, this assumes all the appropriate data has been supplied
    #=================
    def connectBox(self, controller):
        if controller.box == None:
            print "Bad Node:{0}".format(controller.name)
            return 0
            
        if controller.nodeType == "sender" or controller.nodeType == "addVector" or controller.nodeType == "plugin":
            #print "NODE:", controller.name, controller.nodeType, controller.box
            for output in controller.outputInfo:
                print output
                print controller.box.AnimationNodeOutGet()
                
                target = self.findNodeByUID(int(output[-1]))
                print "Target:", output[-1], target.name
                print "TARGET:", target.box, target.nodeType
                
                
                if controller.nodeType == "sender" or controller.nodeType == "plugin":
                    try:
                        outputAnimNode = self.FindAnimationNode(controller.box.AnimationNodeOutGet(), output[0] )
                        targetAnimNode = self.FindAnimationNode(target.box.AnimationNodeInGet(), output[2])
                    except:
                        print "Could not find connections: {0}".format(target.name)
                    
                    
                if controller.nodeType == "addVector":
                    outputAnimNode = self.FindAnimationNode(controller.box.AnimationNodeOutGet(), "Result" )
                    targetAnimNode = self.FindAnimationNode(target.box.AnimationNodeInGet(), output[1])
                
                
                if outputAnimNode != None and targetAnimNode != None:

                    con = FBConnect(outputAnimNode, targetAnimNode)

                else:

                    print "Bad Node"
                
                print ""
                           
                
                
        elif controller.nodeType == "receiver":
            print "R"
        #elif controller.nodeType == "addVector":
        #    print "A"
        else:
            print "NONE"
            
        
    #=================
    # Connects all boxes that are in the constraint
    #=================
    def connectAllBoxes(self):
        tStart = time.clock()
        for i in range(0, len(self.allControllers)):
            self.connectBox(self.allControllers[i])
        tEnd = time.clock()
        print "ALL CONNECTIONS TOOK:", (tEnd - tStart), "seconds."
       
    #=================
    #  Find what kind of box we are dealing with
    #=================    
    def getBoxType(self, boxNode, connections):
        #print boxNode.Name
        functionNames = ["add (v1 + v2)"]

        #print 
        boxType = None
        
        #== IN - receiver
        if len(connections[0]) > 0 and len(connections[1]) == 0:
            boxType = "receiver"
                    
        #== OUT - sender
        elif len(connections[0]) == 0 and len(connections[1]) > 0:
            boxType = "sender"
            
        
        #== BOTH
        elif len(connections[0]) > 0 and len(connections[1]) > 0:
            if "add (v1 + v2)" in boxNode.Name.lower():
                boxType = "addVector"
            else:
                boxType = "function"
            
        

        
        if "3Lateral" in boxNode.Name:
            boxType = "plugin"

        
        #== Make sure we dont have a function that has broken connections...
        
        for i in range(0, len(functionNames)):
            if functionNames[i].lower() in boxNode.Name.lower(): 
                if i == 0:
                    boxType = "addVector"
                else:
                    boxType = "function"
        
        
        if boxType == None:
            boxType = "object"
            
        print "NODETYPE: ", boxType            
        return boxType    



    
    #=================
    #  Find the plugin node in the relation constraint
    #=================
    def getPluginNode(self):
        found = False
        for box in self.relationInst.Boxes:   
            if "3Lateral" in box.Name:
                # think we have found the plugin node!
                self.pluginInstance = cPluginNode()
                self.pluginInstance.name = box.Name
                self.pluginInstance.box = box
                
                self.pluginName = self.pluginInstance.name
                self.pluginInstance.uid = 0
                self.pluginInstance.pos = self.relationInst.GetBoxPosition(box)
                
                self.pluginInstance.connections = [self.getConnections(box, 0), self.getConnections(box, 1)]
                self.pluginInstance.outputInfo = self.gatherOutputConnections(self.pluginInstance)
                self.pluginInstance.nodeType = self.getBoxType(box, self.pluginInstance.connections)
                
                self.boxes[self.pluginInstance.uid] = self.pluginInstance.box
                self.allControllers.append(self.pluginInstance)
                print "Found the plugin:", box.Name, self.pluginInstance.nodeType
                found = True
        
        if found == False:
            print "Could not find plugin"
            
        return found
        
    #=================
    #  Map the constraint that is present in the scene
    #=================
    def mapRelationConstraint(self, conName):
        tStart = time.clock()
        fBoxTypes = ['Add (V1 + V2)', 'Vector to Number']
        uidIndex = 00001
        self.name = conName
        
        for con in glo.gConstraints:
            if conName == con.Name:
                self.relationInstName = con.Name
                self.relationInst = con
                
        if self.relationInstName != "":
            
            if self.getPluginNode() == False:
                print "CANT FIND PLUGIN!"
            
            #print "NUM BOXES:", len(self.relationInst.Boxes)
            #for box in self.relationInst.Boxes:
                #print "BOX TYPE:", box.ClassName()
                
                
            for box in self.relationInst.Boxes:
                
                #== Lets get all of the connections, IN and OUT
                boxConnection = [self.getConnections(box, 0), self.getConnections(box, 1)]
                boxType = self.getBoxType(box, boxConnection)
                boxNamespace = str(box.Box.LongName.split(":")[0])
                print "============  NAMESPACE: {0},  FULLNAME: '{1}'".format(boxNamespace, box.Box.LongName)
                
                boxEntityID = self.getEntityID(boxNamespace)
                
                #== if the box has no connections, lets still map it.  We want a 1 to 1 match.
                if boxConnection == [[],[]]:
                    if boxType == "function":
                        if "Vector To Number" in box.Name:
                            print "VEC TO NUM"
                        
                        elif "Number To Vector" in box.Name:
                            print "NUM TO VEC"
                            
                    elif boxType == "addVector":
                                 
                        #== lets create a vector add box
                        
                        
                        
                        self.boxes[uidIndex] = box
                        addVecInst = cAddVector()
                        addVecInst.name = box.Name
                        addVecInst.box = box
                        addVecInst.uid = uidIndex
                        addVecInst.entityID = boxEntityID
                        addVecInst.nodeType = boxType
                        addVecInst.connections = boxConnection
                        addVecInst.pos = self.relationInst.GetBoxPosition(box)
                        uidIndex += 1
                        self.addVectorInstances.append(addVecInst) 
                        
                        
                        
                #== Ok we have some connections lets handle them!                               
                else:
                                                
                    
                    #==  If it is a function... TODO Adding other function types
                    if boxType == "function":
                        if "Vector To Number" in box.Name:
                            print "VEC TO NUM"
                        
                        elif "Number To Vector" in box.Name:
                            print "NUM TO VEC"
                            
                    elif boxType == "plugin":
                        self.pluginName = box.Name
                        self.pluginBox = box
                        
                                
                    #== If it is a addVector, lets set it up right...        
                    elif boxType == "addVector":        
                        #== lets create a vector add box
                        
                        self.boxes[uidIndex] = box
                        addVecInst = cAddVector()
                        addVecInst.name = box.Name
                        addVecInst.box = box
                        addVecInst.pos = self.relationInst.GetBoxPosition(box)
                        addVecInst.uid = uidIndex
                        addVecInst.entityID = boxEntityID
                        addVecInst.nodeType = boxType
                        addVecInst.connections = boxConnection
                        uidIndex += 1
         
                        #== IF there are incomming connections...
                        if boxConnection[0] !=[]:
                            
                            
                                
                            for conIndex in range(0, len(boxConnection[0])):
                            
                                inputName = boxConnection[0][conIndex][1].GetOwner().Name
                                
                                
                                #== If we are connecting to the plugin...
                                if "3Lateral" in inputName:
                                    inputName = "#plugin#"
                                    
                                inputName = self.formatBoxName(inputName)
                                inputAttribute = boxConnection[0][conIndex][1].UserName
                                if inputName == None: inputName = "None"
                                if inputAttribute == None: inputAttribute = "None"
                            
                                # setting up the addvector instance with the correct format of input
                                addVecInst.inputInfo[conIndex] = [inputName, inputAttribute]
                                
                                
                            if len(boxConnection[0]) == 1:
                                addVecInst.inputInfo[1] = ["#userVector#", "None"]
                                
                                
                        else:
                            print "NO INPUTS!"
                                            
                        #== IF there are outgoing connections...
                        if boxConnection[1] !=[]:
                            
                            for conIndex in range(0, len(boxConnection[1])):
                                
                                outputName = self.getNodeByNameInRelation(boxConnection[1][conIndex][1].GetOwner().Name)
                                outputAttribute = boxConnection[1][conIndex][1].UserName
                                
                                
                                #== If we are connecting to the plugin...
                                if "3Lateral" in outputName:
                                    outputName = "#plugin#"
                                    
                                if outputName == None: outputName = "None"
                                if outputAttribute == None: outputAttribute = "None"
                                
                                addVecInst.outputInfo.append([outputName, outputAttribute])
                        else:
                            print "NO OUTS!", addVecInst.name
                            outs = self.getConnections(box, 1, 1)
                            print outs
                            for each in outs:
                                for e in each:
                                    print e.GetOwner().Name
                                
                            print "========="
                        
                       
                            
                        #== Add the addVector to the manager!
                        self.addVectorInstances.append(addVecInst)  
                        self.allControllers.append(addVecInst)
                                                     
                                                         
                        
                        
                    #== IF we have only outputs it is a sender... 
                    elif boxType == "sender":
                        
                        #== make sure we do not have any function nodes...
                        if "Add (V1 + V2)" not in box.Name:                            
                            print "Sender:", box.Name
                            senderInst = cSenderNode()
                            
                            if box.ClassName() == "FBModelPlaceHolder":
                                senderInst.name = box.Model.Name
                                senderInst.globalSpace = box.UseGlobalTransforms
                            else:
                                senderInst.name = box.Name
                                
                            senderInst.controllerType = None
                            senderInst.box = box
                            senderInst.pos = self.relationInst.GetBoxPosition(box)
                            senderInst.nodeType = boxType
                            senderInst.entityID = boxEntityID
                            self.boxes[uidIndex] = box
                            senderInst.uid = uidIndex
                            
                            uidIndex += 1
                            senderInst.connections = boxConnection
                            
                            #== Connection info, [[senderAttribute, targetController, targetAttribute]]
                            '''
                            for conOut in boxConnection[1]:
                                

                                conName = conOut[0].GetOwner().Model.Name
                                
                                targetName = self.getNodeByNameInRelation(conOut[1].GetOwner().Name)
                                #== If we are connecting to the plugin...
                                if "3Lateral" in targetName:
                                    targetName = "#plugin#"
                                
                                targetName = self.formatBoxName( targetName)
                                
                                targetAttribute = self.formatBoxName(conOut[1].UserName)
                                senderInst.outputInfo.append([conOut[0], targetName, targetAttribute])
                            '''
                            senderInst.outputInfo = self.gatherOutputConnections(senderInst)  
                            
                            self.senderInstances.append(senderInst)
                            self.allControllers.append(senderInst)
                    
                    #== IF we have only inputs it is a receiver... 
                    elif boxType == "receiver": 
                                                   
                        print "Receiver:", box.Name
                        receiverInst = cReceiverNode()
                        if box.ClassName() == "FBModelPlaceHolder":
                            #print "model:", box.Model.Name
                            receiverInst.name =  box.Model.Name
                            receiverInst.globalSpace = box.UseGlobalTransforms
                           
                        else:
                            print "Nope..."
                            receiverInst.name = box.Name
                            
                            
                        receiverInst.controllerType = None
                        receiverInst.box = box
                        self.boxes[uidIndex] = box
                        receiverInst.pos = self.relationInst.GetBoxPosition(box)
                        receiverInst.uid = uidIndex
                        receiverInst.entityID = boxEntityID
                        receiverInst.nodeType = boxType
                        uidIndex += 1
                        receiverInst.connections = boxConnection
                        
                        
                        for conIndex in range(0, len(boxConnection[0])):
                            
                                  
                            tempName = self.getNodeByNameInRelation(boxConnection[0][conIndex][1].GetOwner().Name)
                            #== If we are connecting to the plugin...
                            if "3Lateral" in tempName:
                                tempName = "#plugin#"
                                
                            inputName = self.formatBoxName(tempName)
                            inputAttribute = boxConnection[0][conIndex][1].UserName
                            
                            receiverInst.inputInfo.append([boxConnection[0][conIndex][0].UserName, inputName, inputAttribute])
                        
                        
                        self.receiverInstances.append(receiverInst)
                        self.allControllers.append(receiverInst)
                        
            #self.allControllers = self.senderInstances + self.receiverInstances + self.addVectorInstances + [self.pluginInstance]
            self.mapBoxConnectionUID()              
        else:
            print "Could not find relation constraint." 
    
        tEnd = time.clock()
        print "== MAPPING THE CONSTRAINT TOOK:", (tEnd - tStart), "SECONDS."

    #=================
    # Find all of the objects connected uid's for the final step of mapping the constraint
    #=================
    def mapBoxConnectionUID(self):
        tStart = time.clock()
        #for conKey in self.boxes.keys():
            #print conKey, self.boxes[conKey]

        for controller in self.allControllers:
            
            foundUIDBool = 0
            
            if controller.nodeType != "receiver":    
                for outputNode in range(0, len(controller.connections[1])):
                    for boxKey in self.boxes.keys():
                        if controller.connections[1][outputNode][1].GetOwner() == self.boxes[boxKey]:
                            
                            print "SENDER FOUND ID:", boxKey
                            foundUIDBool = 1
                            controller.outputInfo[outputNode].append(boxKey)
                             
                            
                if foundUIDBool == 0:
                    
                    print "Could not find UID for output:", controller.name, controller.connections[1]
                
                
            if controller.nodeType == "receiver" or controller.nodeType == "addVector":
                for inputNode in range(0, len(controller.connections[0])):
                    for boxKey in self.boxes.keys():
                        if controller.connections[0][inputNode][1].GetOwner() == self.boxes[boxKey]:
                            
                            #print "FOUND ID:", boxKey
                            foundUIDBool = 1
                            controller.inputInfo[inputNode].append(boxKey)
                if foundUIDBool == 0:
                    print "Could not find UID for input:", controller.name, controller.connections[0]
                
        tEnd = time.clock()
        print "MAPPING UIDS TOOK:", (tEnd - tStart), "SECONDS."       
        '''
        print "PLUGIN:", self.pluginInstance
        if self.pluginInstance != None:
            if len(self.pluginInstance.connections) > 1:
                if len(self.pluginInstance.connections[1]) > 0:
                    for conIndex in range(0, len(self.pluginInstance.connections[1])):
                        foundUIDBool = 0
                        if len(self.pluginInstance.connections[1][conIndex]) > 0:
                            targetBox = self.pluginInstance.connections[1][conIndex][1].GetOwner()
                            for targController in self.allControllers:
                                if targetBox == targController.box:
                                    self.pluginInstance.outputInfo[conIndex].append(targController.uid)
                                    print "targetAttr: ", self.pluginInstance.outputInfo[conIndex]
                                    print "Found:", self.pluginInstance.name, targetBox.Name, targController.uid
                                    foundUIDBool = 1
                                    
                            if foundUIDBool == 0:
                                print "ERROR: Could not find UID for sender:", self.pluginInstance.name, self.pluginInstance.outputInfo


        for sender in self.senderInstances:
            print "WORKING ON THIS SENDER:", sender.name
            if len(sender.connections) > 1:
                if len(sender.connections[1]) > 0:
                    for conIndex in range(0, len(sender.connections[1])):
                        foundUIDBool = 0
                        if len(sender.connections[1][conIndex]) > 0 and len(sender.connections[1][conIndex]) < 3:
                            
                            targetBox = sender.connections[1][conIndex][1].GetOwner()
                            for targController in self.allControllers:
                                
                                if targController != None:
                                    if targetBox == targController.box:
                                        sender.outputInfo[conIndex].append(targController.uid)
                                        print "targetAttr: ", sender.outputInfo[conIndex]
                                        print "Found:", sender.name, targetBox.Name, targController.uid
                                        foundUIDBool = 1
                            
                            if foundUIDBool == 0:
                                print "Could Not find connection.. testing plugin..."
                                if self.pluginInstance != None:
                                    if targetBox == self.pluginInstance.box:
                                        print "Plug:", self.pluginInstance
                                        sender.outputInfo[conIndex].append(self.pluginInstance.uid)
                                        print "targetAttr: ", sender.outputInfo[conIndex]
                                        print "Found:", sender.name, self.pluginInstance.name, self.pluginInstance.uid
                                        foundUIDBool = 1
                            
                                    
                        if foundUIDBool == 0:
                            print "ERROR: Could not find UID for receiver:", sender.name, sender.outputInfo
        
        for addVector in self.addVectorInstances:
            if len(addVector.connections[0]) > 0:
                for conIndex in range(0, len(addVector.connections[0])):
                    foundUIDBool = 0
                    inputBox = addVector.connections[0][conIndex][1].GetOwner()
                    print "--> ", inputBox.Name
                    for targetController in self.allControllers:
                        if targetController != None:
                            if inputBox == targetController.box:
                                addVector.inputInfo[conIndex].append(targetController.uid)
                                foundUIDBool = 1
        
                        
                        if foundUIDBool == 0:
                            print "Could Not find connection.. testing plugin..."
                            if self.pluginInstance != None:
                                if inputBox == self.pluginInstance.box:
                                    addVector.inputInfo[conIndex].append(self.pluginInstance.uid)
                                    print "targetAttr: ", addVector.inputInfo[conIndex]
                                    print "Found:", addVector.name, self.pluginInstance.name, self.pluginInstance.uid
                                    foundUIDBool = 1
                        
                                   
                    if foundUIDBool == 0:
                        print "ERROR: Could not find UID for add IN:", addVector.name, addVector.inputInfo
        
            if len(addVector.connections) > 1:
                if len(addVector.connections[1]) > 0:
                    for conIndex in range(0, len(addVector.connections[1])):
                        foundUIDBool = 0
                        if len(addVector.connections[1][conIndex]) > 0:
                            targetBox = addVector.connections[1][conIndex][1].GetOwner()
                            for targController in self.allControllers:
                                
                                if targController != None:
                                    if targetBox == targController.box:
                                        addVector.outputInfo[conIndex].append(targController.uid)
                                        print "targetAttr: ", addVector.outputInfo[conIndex]
                                        print "Found:", addVector.name, targetBox.Name, targController.uid
                                        foundUIDBool = 1
                                  
                                if foundUIDBool == 0:
                                    if self.pluginInstance != None:
                                        print "Could Not find connection.. testing plugin..."
                                        if targetBox == self.pluginInstance.box:
                                            addVector.outputInfo[conIndex].append(self.pluginInstance.uid)
                                            print "targetAttr: ", addVector.outputInfo[conIndex]
                                            print "Found:", addVector.name, self.pluginInstance.name, self.pluginInstance.uid
                                            foundUIDBool = 1
                                           
                        if foundUIDBool == 0:
                            print "ERROR: Could not find UID add OUT:", addVector.name, addVector.outputInfo   

    
        '''
        
    #=================
    #  Export the specified relation constraint to XML
    #=================
    def exportConstraintXML(self, fullXMLPath):
        self.XMLPath = fullXMLPath
        header = '<?xml version="1.0" encoding="utf-8"?>\n'
        blockStart = '<attributeMapping>\n'
        blockEnd = '</attributeMapping>\n'
        
        xmlLines = []
        if self.pluginInstance != None:
            lineInput = (str(self.pluginInstance.uid), self.pluginInstance.nodeType, str(self.pluginInstance.pos[1]), str(self.pluginInstance.pos[2]))
            xmlLines.append("\t<plugin name='#plugin#' uid='{0}' type='{1}' posx='{2}' posy='{3}'>\n".format(lineInput))
            
            for i in range(0, len(self.pluginInstance.outputInfo)):
                xmlLines.append("\t\t<attributeOut name='" + self.pluginInstance.outputInfo[i][0].UserName + "' type='vector'>\n")             
                xmlLines.append("\t\t\t<targetAttribute parentController='" + self.pluginInstance.outputInfo[i][1] + "' name='" + self.pluginInstance.outputInfo[i][2] + "' uid='" + str(self.pluginInstance.outputInfo[i][3]) + "'></targetAttribute>\n")                                  
                xmlLines.append("\t\t</attributeOut>\n")
            xmlLines.append("\t</plugin>\n\n")
            
        #== Sender block
        for sender in self.senderInstances:
            if sender.controllerType == None:
                sender.controllerType = 'controller'
            xmlLines.append("\t<senderBox name='" + self.formatBoxName(sender.name) + "' globalSpace='" + str(sender.globalSpace) + "' type='" + sender.nodeType + "' uid='" + str(sender.uid) + "' entityid='" + str(sender.entityID) + "' posx='" + str(sender.pos[1]) + "' posy='" + str(sender.pos[2]) + "'>\n")
            
            for i in range(0, len(sender.outputInfo)):
                xmlLines.append("\t\t<attributeOut name='" + sender.outputInfo[i][0].UserName + "' type='vector'>\n")
                #print sender.name, sender.outputInfo[i]
                xmlLines.append("\t\t\t<targetAttribute parentController='" + sender.outputInfo[i][1] + "' name='" + sender.outputInfo[i][2] + "' uid='" + str(sender.outputInfo[i][3]) + "'></targetAttribute>\n")
                xmlLines.append("\t\t</attributeOut>\n")
            xmlLines.append("\t</senderBox>\n\n")
    
        #== Receiver block
        for rec in self.receiverInstances:
            if rec.controllerType == None:
                rec.controllerType = 'controller'
            xmlLines.append("\t<receiverBox name='" + self.formatBoxName(rec.name) + "' globalSpace='" + str(rec.globalSpace) + "' type='" + rec.nodeType + "' uid='" + str(rec.uid) + "' entityid='" + str(rec.entityID) + "' posx='" + str(rec.pos[1]) + "' posy='" + str(rec.pos[2]) + "'>\n")
            
            
            #== TODO ADD UID FOR INCOMMING CONNECTION
            for inputNode in rec.inputInfo:
                xmlLines.append("\t\t<attributeIn name='" + inputNode[0] + "' uid='" + str(inputNode[1]) + "' type='vector'>\n")
                xmlLines.append("\t\t\t<senderAttribute parentController='" + inputNode[1] + "' name='" + inputNode[2] + "'></senderAttribute>\n")
                xmlLines.append("\t\t</attributeIn>\n")
            xmlLines.append("\t</receiverBox>\n\n")
        
        #== AddVector block
        for addVec in self.addVectorInstances:
            #print "ADDVECTOR:", addVec.name, addVec.inputInfo
            #print addVec.outputInfo
            xmlLines.append("\t<addVector name='" + self.formatBoxName(addVec.name) + "' uid='" + str(addVec.uid) + "' entityid='" + str(rec.entityID) + "' type='" + addVec.nodeType + "' posx='" + str(addVec.pos[1]) + "' posy='" + str(addVec.pos[2]) + "'>\n")
            for i in range(0, len(addVec.inputInfo)):
                if addVec.inputInfo[i] != None:
                    if addVec.inputInfo[i][0] != "#userVector#":
                        xmlLines.append("\t\t<vectorIn parentController='" + addVec.inputInfo[i][0] +"' attributeOut='" + addVec.inputInfo[i][1] + "' type='vector' inputIndex='" + str(i) + "' uid='" + str(addVec.inputInfo[i][2]) + "'></vectorIn>\n")
                    else:
                        xmlLines.append("\t\t<vectorIn parentController='" + addVec.inputInfo[i][0] +"' attributeOut='" + addVec.inputInfo[i][1] + "' type='vector' inputIndex='" + str(i) + "' uid='" + "None" + "'></vectorIn>\n")                        
              
            for i in range(0, len(addVec.outputInfo)):
                if addVec.outputInfo[i] != None:
                    xmlLines.append("\t\t<targetAttribute parentController='" + addVec.outputInfo[i][0] + "' name='" + addVec.outputInfo[i][1] + "' uid='" + str(addVec.outputInfo[i][2]) + "'></targetAttribute>\n")
            
            xmlLines.append("\t</addVector>\n\n")    
                
        xmlFile = open(fullXMLPath, "w")
        xmlFile.write(header)
        xmlFile.write(blockStart)
        for line in xmlLines:
            xmlFile.write(line)
        xmlFile.write(blockEnd)
        xmlFile.close()
        print "Created XML: ", fullXMLPath
        
        
    #=================
    #  Load the XML and populate the lists
    #=================
    def loadConstraintXML(self, fullXMLPath):
        
        self.XMLPath = fullXMLPath
        if os.path.exists(fullXMLPath):
            domParse = minidom.parse(fullXMLPath)
        
            #== load the plugin info
            for node in domParse.getElementsByTagName('plugin'):
                #self.getPluginNode()
                self.pluginInstance = cPluginNode()
                self.pluginInstance.uid = int(node.getAttribute('uid'))
                self.pluginInstance.nodeType = node.getAttribute('type')
                self.pluginInstance.pos = (True, int(node.getAttribute('posx')), int(node.getAttribute('posy')))
                
                for attrOut in node.getElementsByTagName('attributeOut'):
                    
                    attrName = str(attrOut.getAttribute('name'))
                    for target in attrOut.getElementsByTagName('targetAttribute'):
                        targetName = str(target.getAttribute('parentController'))
                        targetAttr = str(target.getAttribute('name'))
                        targetUID = str(target.getAttribute('uid'))
                        self.pluginInstance.outputInfo.append([attrName, targetName, targetAttr, targetUID])
                        self.allControllers.append(self.pluginInstance)
                
            #== Load the senders
            for sender in domParse.getElementsByTagName('senderBox'):
                senderInst = cSenderNode()
                senderInst.nodeType = "sender"
                senderInst.name = str(sender.getAttribute('name'))
                senderInst.pos = (True, int(sender.getAttribute('posx')), int(sender.getAttribute('posy')))
                senderInst.nodeType = str(sender.getAttribute('type'))
                senderInst.uid = str(sender.getAttribute('uid'))
                senderInst.entityID = int(sender.getAttribute('entityid'))
                senderInst.globalSpace = self.stringToBool(sender.getAttribute('globalSpace'))
                
                for attrOut in sender.getElementsByTagName('attributeOut'):
                    
                    attrName = str(attrOut.getAttribute('name'))
                    for target in attrOut.getElementsByTagName('targetAttribute'):
                        targetName = str(target.getAttribute('parentController'))
                        targetAttr = str(target.getAttribute('name'))
                        targetUID = str(target.getAttribute('uid'))
                        senderInst.outputInfo.append([attrName, targetName, targetAttr, targetUID])
        
                self.senderInstances.append(senderInst)
                self.allControllers.append(senderInst)
                
                
            #== Load the receivers
            for rec in domParse.getElementsByTagName('receiverBox'):
                
                recInst = cReceiverNode()
                recInst.nodeType = "receiver"
                recInst.name = str(rec.getAttribute('name'))  
                recInst.nodeType = str(rec.getAttribute('type'))
                recInst.uid = str(rec.getAttribute('uid'))
                recInst.entityID = int(rec.getAttribute('entityid'))
                recInst.pos = (True, int(rec.getAttribute('posx')), int(rec.getAttribute('posy')))
                recInst.globalSpace = self.stringToBool(rec.getAttribute('globalSpace'))
               
                for attrIn in rec.getElementsByTagName('attributeIn'):
                    
                    attrName = str(attrIn.getAttribute('name'))
                    for sender in attrIn.getElementsByTagName('senderAttribute'):
                        senderName = str(sender.getAttribute('parentController'))
                        senderAttr = str(sender.getAttribute('name'))
                        senderUID = str(sender.getAttribute('uid'))
                        recInst.inputInfo.append([attrName, senderName, senderAttr]) #, senderUID])
                self.receiverInstances.append(recInst)
                self.allControllers.append(recInst)


            #== Load the add vectors...
            for add in domParse.getElementsByTagName('addVector'):
                addVecInst = cAddVector()
                addVecInst.nodeType = "addVector"
                addVecInst.name = str(add.getAttribute('name'))  
                addVecInst.controllerType = str(add.getAttribute('type'))
                addVecInst.uid = str(add.getAttribute('uid'))
                addVecInst.entityID = int(add.getAttribute('entityID'))
                addVecInst.pos = (True, int(add.getAttribute('posx')), int(add.getAttribute('posy')))
                
                for attrIn in add.getElementsByTagName('vectorIn'):
                                    
                    senderName = str(attrIn.getAttribute('parentController'))
                    senderAttr = str(attrIn.getAttribute('attributeOut'))
                    senderUID = str(attrIn.getAttribute('uid'))
                    
                    inputIndex = int(attrIn.getAttribute('inputIndex'))                    
                    addVecInst.inputInfo[inputIndex] = [senderName, senderAttr, senderUID]
                    
                    
                
                for attrOut in add.getElementsByTagName('targetAttribute'):

                    
                    targetName = str(attrOut.getAttribute('parentController'))
                    targetAttr = str(attrOut.getAttribute('name'))
                    targetUID = str(attrOut.getAttribute('uid'))
                    addVecInst.outputInfo.append([targetName, targetAttr, targetUID])
                
                self.addVectorInstances.append(addVecInst)
                self.allControllers.append(addVecInst)
                
            #self.allControllers = self.senderInstances + self.receiverInstances + self.addVectorInstances + [self.pluginInstance]
            
            
            
                                    
        else:
            print "Could not find", fullXMLPath, ", please check the file path."




    #=================
    # Create the constraint in motionbuilder
    #=================            
    def constructConstraint(self):
        #print "BUILD THIS BIZZY"
        tStart = time.clock()
        self.relationInst = FBConstraintRelation(self.relationInstName)
        self.relationInst.Active = True
        
        if self.pluginInstance != None:
            self.pluginInstance.box = self.relationInst.CreateFunctionBox('GTAV', (self.pluginName))
            self.boxes[self.pluginInstance.uid] = self.pluginInstance.box
            
            self.relationInst.SetBoxPosition(self.pluginInstance.box, self.pluginInstance.pos[1], self.pluginInstance.pos[2])
            self.allControllers.append(self.pluginInstance)
        
        
        #== Lets start by creating all of the senders, and hooking them up if we can...
        for sender in self.senderInstances:
            
            #== IF any of the nodes need to point to the plugin, lets point it to the correct name!
            if sender.name == "#plugin#":
                sender.name = self.pluginName
            
            if len(sender.outputInfo) > 0:
                if sender.outputInfo[0] == "#plugin#":
                    sender.name = self.pluginName
                
                
            #== lets find the in scene model
            if sender.entityID != -1:
                senderModel = FBFindModelByLabelName("{0}:{1}".format(self.entities[int(sender.entityID)], sender.name))
            else:
                senderModel = FBFindModelByLabelName("{0}:{1}".format(self.characterName, sender.name))
                
            if senderModel:
                
                #== lets see if we've created this box already...
                if sender.uid in self.boxes.keys():
                    
                    sender.box = self.boxes[sender.uid]
                        
                #== if we have not, lets add it to the constraint and the box dict...
                else:
                    
                    self.boxes[sender.uid] = self.relationInst.SetAsSource(senderModel)
                    sender.box = self.boxes[sender.uid]
                    sender.box.UseGlobalTransforms = sender.globalSpace
                    self.relationInst.SetBoxPosition(sender.box, sender.pos[1], sender.pos[2])
                   
          
            else:
                print "Could not sender find model:", sender.name

           
        
        
        for receiver in self.receiverInstances:
            
            #== IF any of the nodes need to point to the plugin, lets point it to the correct name!
            if receiver.name == "#plugin#":
                receiver.name = self.pluginName
            
            if len(receiver.inputInfo) > 0:
                if receiver.inputInfo[0] == "#plugin#":
                    receiver.name = self.pluginName
                
                
            #== lets find the in scene model
            if receiver.entityID != -1:
                receiverModel = FBFindModelByLabelName("{0}:{1}".format(self.entities[receiver.entityID], receiver.name))
            else:
                receiverModel = FBFindModelByLabelName("{0}:{1}".format(self.characterName, receiver.name))
            if receiverModel:
                
                #== lets see if we've created this box already...
                if receiver.uid in self.boxes.keys():
                    
                    receiver.box = self.boxes[receiver.uid]
                        
                #== if we have not, lets add it to the constraint and the box dict...
                else:
                    
                    self.boxes[receiver.uid] = self.relationInst.ConstrainObject(receiverModel)
                    receiver.box = self.boxes[receiver.uid]
                    #print receiver.box.UseGlobalTransforms, receiver.globalSpace
                    receiver.box.UseGlobalTransforms = receiver.globalSpace
                    self.relationInst.SetBoxPosition(receiver.box, receiver.pos[1], receiver.pos[2])
                    
          
            else:
                print "Could not receiver find model:", receiver.name
                
                
        
        
        for addVector in self.addVectorInstances:
            
            #== IF any of the nodes need to point to the plugin, lets point it to the correct name!
            if addVector.name == "#plugin#":
                addVector.name = self.pluginName
            
            if len(addVector.outputInfo) > 0:
                if addVector.outputInfo[0] == "#plugin#":
                    addVector.name = self.pluginName
                
               
                            
            #== lets see if we've created this box already...
            if addVector.uid in self.boxes.keys():
                #print "ITS IN HERE"
                #print self.boxes[addVector.uid].Name
                addVector.box = self.boxes[addVector.uid]
                    
            #== if we have not, lets add it to the constraint and the box dict...
            else:
                #print "Adding Add Vector"
                self.boxes[addVector.uid] = self.relationInst.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                addVector.box = self.boxes[addVector.uid]
                
                self.relationInst.SetBoxPosition(addVector.box, addVector.pos[1], addVector.pos[2])
                       

        #== Everything has been made, now lets connect it!
        self.connectAllBoxes() 
        tEnd = time.clock()
        print "BUILDING THE CONSTRAINT TOOK:", (tEnd - tStart), "SECONDS."

    #=================
    # Print all of the constraint lists
    #=================    
    def debugPrintConstraint(self, loadingBool):
        if loadingBool:
            print "NO LOADING DEBUG YET"
        else:
            
            for control in self.senderInstances:
                print "Name: ", control.name, " Type:", control.controllerType
                for target in control.outputInfo:
                    print "\tOUT:", (control.name + "." + target[0].UserName + "-> " + (target[1] + "." + target[2]) + "\t"), "IN: " + target[1] + "." + target[2]
            
                print "\n" 
            
            for addVec in self.addVectorInstances:
                print "Name: ", addVec.name
                print "\tINPUT0:", addVec.inputInfo[0]
                print "\tINPUT1:", addVec.inputInfo[1]
                print "\tOUTPUT:", addVec.outputInfo
                print "\n"


        print "Num Of Senders:", len(self.senderInstances)
        print "Num Of Receivers:", len(self.receiverInstances)
        print "Num Of Add Vectors:", len(self.addVectorInstances)
        
        
                
                                
                            
        
#==============    COMMON Functions     ============== 
#=====================================================
def FindAnimationNode(pParent, pName):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult


def ConnectBox(nodeOut,nodeOutPos, nodeIn, nodeInPos):
    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
    mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )
    if mynodeOut and mynodeIn:
        FBConnect(mynodeOut, mynodeIn)



                
#=====================================================        
#=====================================================


def saveXML(xmlPath):

    FBSystem().Scene.Evaluate()
    rManager = cRelationManager("", "")
    rManager.characterName = "TransChar"
    #rManager.mapRelationConstraint('Dr_friedlander 3Lateral 1')
    rManager.mapRelationConstraint('Head_Delta')
    #rManager.debugPrintConstraint(0)
    rManager.exportConstraintXML(xmlPath)
    

def loadXML(xmlPath):
    tStart = time.clock()
    FBSystem().Scene.Evaluate()
    #rManager = cRelationManager("", "")
    rManager = cRelationManager("TESTREL", "")
    rManager.pluginName = "temp"
    rManager.characterName = "NEW_Player_Zero 2"
    rManager.entities.append("NEW_Player_Zero 2")
    rManager.entities.append("Player_Zero")
    rManager.loadConstraintXML(myXMLPATH)
    rManager.constructConstraint()
    #print rManager.allControllers
                
    rManager.debugPrintConstraint(1)
    tEnd = time.clock()
    print "TOTAL TIME:", (tEnd - tStart)

#mappingXMLPath = 
#myXMLPATH = "C:\\test\\myXML2.xml"

#saveXML(myXMLPATH)
#loadXML(myXMLPATH)                      

      