class IncrediBuildBuildError(Exception):
    pass


class IncrediBuildTaskError(Exception):
    pass


class IncrediBuildUnknownError(Exception):
    pass
