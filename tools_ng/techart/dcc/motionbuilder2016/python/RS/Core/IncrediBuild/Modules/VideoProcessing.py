import os
import uuid
import tempfile

from RS import Config


BLANK_FOOTAGE_IMAGE = os.path.join(os.path.dirname(__file__), "blankFootage.png")
FOOTAGE_NOT_FOUND_IMAGE = os.path.join(os.path.dirname(__file__), "footageNotFound.png")


def _addFfMpeg(prject, remote=True):
    ffMpegDict = {
                  True: "ffmpeg Remote",
                  False: "ffmpeg Local"
    }

    ffmpegFilePath = os.path.join(Config.Tool.Path.Bin, "video", "ffmpeg",
                                     Config.AppConfig.FFMPEG, "ffmpeg.exe")

    nameOfTool = ffMpegDict[remote]
    tool = prject.getToolByName(nameOfTool)

    if tool is None:
        tool = prject.addNewTool(nameOfTool, ffmpegFilePath, remote)
    return tool


def _sourceVideo(filePath):
    if filePath is None:
        return BLANK_FOOTAGE_IMAGE
    if os.path.isfile(filePath):
        return filePath
    return FOOTAGE_NOT_FOUND_IMAGE


def mosaicTask(upperLeftVideoPath, upperRightVideoPath, lowerLeftVideoPath, lowerRightVideoPath,
               outputPath, incredibuildProject, taskGroup=None, outputSize=(640, 480), runRemote=True):
    taskName = "Create Mosaic {0} ({1})".format(os.path.basename(outputPath), str(uuid.uuid1()))
    vids = []
    for vid in [upperLeftVideoPath, upperRightVideoPath, lowerLeftVideoPath, lowerRightVideoPath]:
        vids.append(_sourceVideo(vid))

    tool = _addFfMpeg(incredibuildProject, runRemote)

    arg = "-i {0} -i {1} -i {2} -i {3} -filter_complex \"nullsrc=size={4}x{5} [base]; [0:v] " \
            "setpts=PTS-STARTPTS, scale={6}x{7} [upperleft]; [1:v] setpts=PTS-STARTPTS, scale={6}" \
            "x{7} [upperright]; [2:v] setpts=PTS-STARTPTS, scale={6}x{7} [lowerleft]; [3:v]" \
            "setpts=PTS-STARTPTS, scale={6}x{7} [lowerright]; [base][upperleft] overlay=1:shortest=1 [tmp1]; "\
            "[tmp1][upperright] overlay=1:x={6} [tmp2]; [tmp2][lowerleft] overlay=1:y={7} [tmp3]; "\
            "[tmp3][lowerright] overlay=1:x={6}:y={7}\" -c:v ffv1 -y {8}".format(
                                                                                 vids[0],
                                                                                  vids[1],
                                                                                  vids[2],
                                                                                  vids[3],
                                                                                  outputSize[0],
                                                                                  outputSize[1],
                                                                                  int(outputSize[0] / 2),
                                                                                  int(outputSize[1] / 2),
                                                                                  outputPath
                                                                                 )

    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            arg,
                            taskName,
                            skipIfProjectFailed=False,
        )
    else:
        return taskGroup.addNewTask(
                            arg,
                            taskName,
                            name=taskName,
                            tool=tool,
                            skipIfProjectFailed=False,
        )


def extractVideoTask(inputVideo, outputPath, incredibuildProject, taskGroup=None, dependsOn=None,
                     runRemote=True, startTimecode=None, endTimecode=None):
    taskName = "Split Video {0} ({1})".format(os.path.basename(inputVideo), str(uuid.uuid1()))
    tool = _addFfMpeg(incredibuildProject, runRemote)

    addationalArgs = ""
    if startTimecode is not None:
        addationalArgs = "{0} -ss {1}".format(addationalArgs, startTimecode)

    if endTimecode is not None:
        addationalArgs = "{0} -t {1}".format(addationalArgs, endTimecode)

    arg = "-y -i \"{0}\" {1} -map 0:1 -c:v copy \"{2}\"".format(inputVideo, addationalArgs, outputPath)

    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            arg,
                            taskName,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )
    else:
        return taskGroup.addNewTask(
                            arg,
                            taskName,
                            name=taskName,
                            tool=tool,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )


def compressTask(inputVideo, outputPath, incredibuildProject, taskGroup=None, dependsOn=None,
                     runRemote=True):
    taskName = "Compress Video {0} ({1})".format(os.path.basename(inputVideo), str(uuid.uuid1()))
    tool = _addFfMpeg(incredibuildProject, runRemote)

    arg = "-y -i \"{0}\" -pix_fmt yuv420p -f mov -vcodec libx264 -b:a 16000k \"{1}\"".format(
                                                                                             inputVideo,
                                                                                             outputPath
                                                                                             )
    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            arg,
                            taskName,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )
    else:
        return taskGroup.addNewTask(
                            arg,
                            taskName,
                            name=taskName,
                            tool=tool,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )


def concatTask(inputPaths, outputPath, incredibuildProject, taskGroup=None, dependsOn=None,
                     runRemote=True):
    taskName = "Concat {0} ({1})".format(os.path.basename(outputPath), str(uuid.uuid1()))
    tool = _addFfMpeg(incredibuildProject, runRemote)

    # We have to write a list of files to concat to a file for ffmpeg
    concatFilePath = os.path.join(tempfile.gettempdir(), "ffmpegConcat", "{0}".format(str(uuid.uuid1())))
    if not os.path.exists(os.path.dirname(concatFilePath)):
        os.makedirs(os.path.dirname(concatFilePath))
    
    handler = open(concatFilePath, "w")
    for inputPath in inputPaths:
        handler.write("file '{0}'\n".format(inputPath))
    handler.close()
    
    arg = "-y -f concat -i {0} -c copy \"{1}\"".format(concatFilePath, outputPath)

    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            arg,
                            taskName,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )
    else:
        return taskGroup.addNewTask(
                            arg,
                            taskName,
                            name=taskName,
                            tool=tool,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )


def mergeTask(inputVideo, inputAudio, outputPath, incredibuildProject, taskGroup=None, dependsOn=None,
                     runRemote=True):
    taskName = "Merge Video and Audio {0} ({1})".format(os.path.basename(inputVideo), str(uuid.uuid1()))
    tool = _addFfMpeg(incredibuildProject, runRemote)

    arg = "-y -i \"{0}\" -i \"{1}\" -c copy \"{2}\"".format(inputAudio, inputVideo, outputPath)

    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            arg,
                            taskName,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )
    else:
        return taskGroup.addNewTask(
                            arg,
                            taskName,
                            name=taskName,
                            tool=tool,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )


def extractAudioTask(inputVideo, outputPath, incredibuildProject, taskGroup=None, dependsOn=None,
                     runRemote=True):
    tool = _addFfMpeg(incredibuildProject, runRemote)
    taskName = "Extract Audio {0} ({1})".format(os.path.basename(inputVideo), str(uuid.uuid1()))

    arg = "-y -i \"{0}\" -map 0:2 -map 0:0 -c:v copy -map_metadata 0 -c copy -c:a copy -sn \"{1}\"".format(
                                                                                                           inputVideo,
                                                                                                           outputPath
                                                                                                        )

    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            arg,
                            taskName,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )
    else:
        return taskGroup.addNewTask(
                            arg,
                            taskName,
                            name=taskName,
                            tool=tool,
                            skipIfProjectFailed=False,
                            dependsOn=dependsOn,
        )
