'''
Module for working with Rex exporter commands.

This module allows us to talk with the exporter to set various options and export anim/cutscenes

Usage:
from RS.Core.Export import *
RexExport.[command]

Author: Mark Harrison-Ball <mark.harrison-Ball@rockstargames.com>
'''

import pyfbsdk
from ctypes import *

class RexExport:
  '''
     We Open and close the Exporter so that it gets initialized when the module is first imported.     
  '''
  def __init__( self ):
    self.InitializeCutscene()
  
  '''
     Initialize our Cutscene Exporter
  '''    
  def InitializeCutscene( object ):    
    if not object.IsCutsceneExporterInitialized(  ) :
      ''''''
      pyfbsdk.ShowToolByName("Rex Rage Cut-Scene Export")
      pyfbsdk.CloseToolByName("Rex Rage Cut-Scene Export")    

  '''
     Checks if the static instance of the cutscene exporter is valid. (Exporter needs to be opened once) 
     Returns (int)
  '''
  def IsCutsceneExporterInitialized( object ):
    return (cdll.rexMBRage.IsCutsceneExporterInitialized_Py() != 0)
  
  '''
     Gets the current scene name 
     Returns (string)
  '''	
  def GetCutsceneSceneName( object ):
    cdll.rexMBRage.GetCutsceneExportPath_Py.restype = c_char_p
    return cdll.rexMBRage.GetCutsceneSceneName_Py()
  
  '''
     Gets the current scene name 
     Returns (string)
  '''	
  def GetCutsceneExportPath( object ):
    cdll.rexMBRage.GetCutsceneExportPath_Py.restype = c_char_p
    return cdll.rexMBRage.GetCutsceneExportPath_Py()
  
  '''
     Number of shots 
     Return (int)
  '''
  def GetNumberCutsceneShots( object ):
    return cdll.rexMBRage.GetNumberCutsceneShots_Py()
  
  '''
     Gets shot name by index. 
     
     Arguments:
  '''
  def GetCutsceneShotName( object, index ):
    return c_char_p(cdll.rexMBRage.GetCutsceneShotName_Py( index )).value
  
  '''
     Gets shot Start range by index. 
     
     Arguments:
  '''
  def GetCutsceneShotStartRange( object, index ):
    return cdll.rexMBRage.GetCutsceneShotStartRange_Py( index )
  
  '''
     Gets shot end range by index. 
     
     Arguments:
  '''	
  def GetCutsceneShotEndRange( object, index ):
    return cdll.rexMBRage.GetCutsceneShotEndRange_Py( index )
  
  '''
     Sets shot start range by index. 
     
     Arguments:
  '''	
  def SetCutsceneShotStartRange( object, index, start_range ):
    cdll.rexMBRage.SetCutsceneShotStartRange_Py( index, start_range)	
  
  '''
     Sets shot end range by index. 
     
     Arguments:
  '''	
  def SetCutsceneShotEndRange( object, index, end_range ):
    cdll.rexMBRage.SetCutsceneShotEndRange_Py( index, end_range)
	  
  '''
     Sets story mode status. 
    
     Arguments:
  '''	
  def SetCutsceneStoryMode( object, enable_Storymode ):
    cdll.rexMBRage.SetCutsceneStoryMode_Py( enable_Storymode )		
    
  '''
     Sets shot disable dof status
    
     Arguments:
  '''
  def SetCutsceneShotDisableDOF( object, disable_DOF ):
    cdll.rexMBRage.SetCutsceneShotDisableDOF_Py( disable_DOF)
   
  '''
     Sets shot enable dof on 1st cut status. 
     
     Argumnets:
  
  ''' 
  def SetCutsceneShotEnableDOFFirstCut( object, enable_DOF):
    cdll.rexMBRage.SetCutsceneShotEnableDOFFirstCut_Py( enable_DOF)
    
  
  '''
     Sets the shots export status. 
     
     Argumnets:
     
          (Shot Index)
          (Enable Export)
  ''' 
  def SetCutsceneShotExportStatus( object, index, status):
    cdll.rexMBRage.SetCutsceneShotExportStatus_Py(index, status) 
    
  '''
     Adds new shot with specified name. 
     
     Arguments:
           (Name of Sot) 
  ''' 
  def AddCutsceneShot( object, name ):
    cdll.rexMBRage.AddCutsceneShot_Py(name) 
    
  
  '''
     Export the cutscne using the currently active settings
  
     Arguments:
     Enable p4 intergration
     Enable preview export
  '''
  def ExportBuildActiveCutscene( object, p4_Intergration=True, preview=False ):
    return  cdll.rexMBRage.ExportBuildActiveCutscene_Py( p4_Intergration, preview )
    
    
    
  '''
     ExportBuildCutscene. THis will export the current information in the cutpart file and not any local chnages
     
     Arguments:
     CutPart File Path: If file does not exist the export will fail
     Export CutFile
     Enable p4 Intergration
     Build Preview
  
  '''
  def ExportBuildCutscene( object, cutPart, export_cutfile=True, p4_Intergration=True, preview=False ):
    print 'Expoting Cutscene'
    return (cdll.rexMBRage.ExportBuildCutscene_Py(cutPart, True, export_cutfile, p4_Intergration, preview) != 0)
    
  '''
      Import the module in will initalize the Exporter, though if a new cutscene has been loaded then we need to re-initalize with the show tool winodw command 
  '''  
  def _init_():    
    ShowToolByName("Rex Rage Cut-Scene Export")
  
  
    

RexExport = RexExport( ) 
	
	
	
#fbmenu = FBMenuManager()	
#menu = fbmenu.GetMenu("Open Reality/Tools/Rex Rage Cut-Scene Export")
