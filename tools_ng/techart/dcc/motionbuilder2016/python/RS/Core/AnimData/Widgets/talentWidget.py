import functools

from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData.Models import talentModel
from RS.Core.AnimData._internal import contexts
from RS.Core.AnimData.Views import abstractComboView, columnFilterView


class AbstractTalentWidgets(QtCore.QObject):
    """
    Abstract Talent Variation Widget mixin

    has Three signals:
        contextSelected (Context): The currently selected context
        talentSelected (Talent): The currently selected Talent context
    """
    contextSelected = QtCore.Signal(object)
    talentSelected = QtCore.Signal(object)

    def __init__(self):
        super(AbstractTalentWidgets, self).__init__()

    def setContext(self, context):
        """
        VIRTUAL
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Talent or Talent
        """
        raise NotImplementedError("This method needs to be implemented in subclasses")

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        return:
            a list of context objects which are selected
        """
        return [index.data(QtCore.Qt.UserRole) for index in self.selectedIndexes()]

    def getSelectedTalents(self):
        """
        get all the currently selected Talent Variations

        return:
            a list of all the selected Talent Variations
        """
        returnVals = []
        for index in self.selectedIndexes():
            val = index.data(QtCore.Qt.UserRole)
            if isinstance(val, contexts.Talent):
                returnVals.append(val)
        return returnVals

    def _handleContextClick(self, index):
        """
        Internal Method

        Handle the selection change, to re-emit when a context object or trial is selected

        args:
            index (QModelIndex): The model index which is selected
        """
        val = index.data(QtCore.Qt.UserRole)

        if val is not None:
            self.contextSelected.emit(val)

        if isinstance(val, contexts.Talent):
            self.talentSelected.emit(val)


class TalentTreeView(QtGui.QTreeView, AbstractTalentWidgets):
    """
    Context Tree View for the AnimData
    """
    def __init__(self, parent=None):
        super(TalentTreeView, self).__init__(parent=parent)

        # Set To Project By Default
        project = Context.animData.getCurrentProject()

        # Model stuff
        mainModel = talentModel.TalentModel(project)
        self.setModel(mainModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findContextIndex(context)

        # Set Selection
        selection = self.selectionModel()
        selection.select(contextIdx, QtGui.QItemSelectionModel.Select)
        self.setSelectionModel(selection)

        # Expand to selection
        self.expandChildren(contextIdx)

    def expandChildren(self, index):
        """
        Method to expand the view to the given index

        args:
            index (QModelIndex): Model index to expand to
        """
        if not index.isValid():
            return
        self.expandChildren(index.parent())

        self.setExpanded(index, True)


class TalentColumnView(columnFilterView.ColumModelView, AbstractTalentWidgets):
    """
    Context Column View for the AnimData
    """

    def __init__(self, parent=None):
        super(TalentColumnView, self).__init__(parent=parent)

        # Set To Project By Default
        project = Context.animData.getCurrentProject()

        # Model stuff
        mainModel = talentModel.TalentModel(project)
        self.setModel(mainModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        if context is None:
            return

        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)

    def getSelectedProject(self):
        projs = self.getSelectedColumnsIndexes()[0]
        if len(projs) > 0:
            proj = projs[0].data(QtCore.Qt.UserRole)
            if isinstance(proj, contexts.Project):
                return proj
        return None

    def projectColumnVisibility(self, state):
        self.setColumnVisibility(0, state)


class TalentComboxBoxView(abstractComboView.ContextComboxBoxView, AbstractTalentWidgets):
    """
    Context Combo Box for the AnimData

    has Three signals:
        contextSelected (Context): The currently selected context
        talentSelected (Talent): The currently selected Talent context
        talentSelected (Talent): The currently selected Talent Variation context
    """
    def __init__(self, parent=None):
        super(TalentComboxBoxView, self).__init__(parent=parent)

    def model(self):
        """
        Virtual Method
        """
        # Set To Project By Default
        project = Context.animData.getCurrentProject()
        return talentModel.TalentModel(project)

    def _boxDef(self):
        """
        Internal Method

        Virtual Method
        """
        return [
               ("Project:", 0),
               ("Talent:", 1),
               ("Variation:", 2)
               ]

    def _handleContextClick(self, index):
        """
        Re-implemented
        """
        pass

    def itemSelected(self, boxIndex):
        """
        Method for when any box is selected
        """
        item = self.boxes[boxIndex].itemData(self.boxes[boxIndex].currentIndex(), QtCore.Qt.UserRole)
        self.contextSelected.emit(item)

        if isinstance(item, contexts.Talent):
            self.talentSelected.emit(item)

    def childItemSelected(self):
        """
        Method for when the bottom most box is selected
        """
        self.talentSelected.emit(self.boxes[-1].itemData(self.boxes[-1].currentIndex(), QtCore.Qt.UserRole))

    def getCurrentContext(self):
        return self.getSelectedContexts()[0]

    def getCurrentTrial(self):
        trials = self.getSelectedTrials()
        if len(trials) == 1:
            return trials[0]
        return None

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        return:
            a list of context objects which are selected
        """
        contextBox = self._getDeepestEditedBox()
        return [contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)]

    def getSelectedTalents(self):
        """
        get all the currently selected Talent Variations

        return:
            a list of all the selected Talent Variations
        """
        contextBox = self._getDeepestEditedBox()
        val = contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)
        if isinstance(val, contexts.Talent):
            return [val]
        return []

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)


if __name__ == "__main__":
    def _handleSelect(item):
        print item

    def _handleButton(*args, **kwargs):
        print win.getSelectedContexts()

    import sys
    app = QtGui.QApplication(sys.argv)
    wid = QtGui.QWidget()
    lay = QtGui.QVBoxLayout()
    # win = QtGui.QColumnView()
    # win = TalentTreeView()
    win = TalentColumnView()
    # win = TalentComboxBoxView()
    but = QtGui.QPushButton("Test")
    lay.addWidget(win)
    lay.addWidget(but)
    wid.setLayout(lay)
    but.pressed.connect(_handleButton)
    win.talentSelected.connect(_handleSelect)

    wid.show()
    sys.exit(app.exec_())
