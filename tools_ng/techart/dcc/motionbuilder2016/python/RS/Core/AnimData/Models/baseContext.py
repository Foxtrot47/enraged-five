"""
Base Context Model for the lazy loaded contexts
"""
import os

from PySide import QtGui, QtCore

from RS import Config
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModelItem, lazyLoadModelItem
from RS.Core.AnimData import Const
from RS.Core.AnimData import Context as animContext
from RS.Core.AnimData._internal import contexts


class ContextBaseItem(lazyLoadModelItem.LoadingModelItem):
    """
    Base Item Model Type

    This represents an actual context
    """
    def __init__(self, context, parent=None, rootItem=None, addLazyLoader=True):
        """
        Constructor
        """
        super(ContextBaseItem, self).__init__(parent=parent)
        self._context = context
        self._rootItem = rootItem
        self._canFetchMore = True

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()
        if role in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):
            if column == 0:
                return self._context.name

        elif role == QtCore.Qt.UserRole:
            return self._context

        elif role == QtCore.Qt.UserRole + 1:
            return self

    def context(self):
        """
        Get the current Context of the item

        return:
                The Context item which this item represents
        """
        return self._context

    def findContextIndex(self, context, index):
        """
        Get the current Context of the item

        args:
                context (AnimData Context): The AnimData context item to find
                index (QModelIndex): The parent model index to base the child index off

        return:
                The index of the found context
        """
        modelIdx = QtCore.QModelIndex()
        if self.canFetchMore(modelIdx):
            self.fetchMore(modelIdx)

        for idx, child in enumerate(self.childItems):
            if child.data(modelIdx, QtCore.Qt.UserRole) == context:
                return index.child(idx, 0)
        return modelIdx

    def canFetchMore(self, parent):
        return self._canFetchMore

    def fetchMore(self, parent):
        if self.canFetchMore(parent) is True:
            children = self._populateChildren()
            for child in children:
                self.appendChild(child)
            self._canFetchMore = False

    def _populateChildren(self):
        raise NotImplementedError("Populate Children should be redefined in inheirted classes")

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class BaseTrialsItem(ContextBaseItem):

    UNKNOWN_ICON = None
    BODYROM_ICON = None
    BODYSCALE_ICON = None
    CUTSCENE_ICON = None
    FACEROM_ICON = None
    HEADSCAN_ICON = None
    IGCS_ICON = None
    INGAME_ICON = None

    REWORK_ICON = None

    PRIORITY1_ICON = None
    PRIORITY2_ICON = None
    PRIORITY3_ICON = None
    PRIORITY4_ICON = None
    PRIORITY5_ICON = None

    __cachedIcons = {}

    def __init__(self, context, parent=None, rootItem=None):
        super(BaseTrialsItem, self).__init__(context, parent=parent, rootItem=rootItem, addLazyLoader=False)
        self._isSelect = context.isSelected()
        self._priority = animContext.settingsManager.consts.Priorities.NOT_PRIORITIZED
        self._pipeline = context.pipelineStage()
        if isinstance(context, contexts.MotionTrialBase):
            self._priority = context.priority()
        rework = context.pipelineStageRework()

        self._icon = self._getIcon(context, self._priority, self._pipeline, rework=rework)

    @classmethod
    def _getIcon(cls, context, priority, pipelineStage, rework=False):
        cachedName = "{0}-{1}-{2}".format(context.captureType.name, str(priority.name), str(pipelineStage))
        icon = cls.__cachedIcons.get(cachedName)
        if icon is not None:
            return icon

        iconHeight = 20
        typeIcon = cls.getTypeIconDict().get(context.captureType, cls.UNKNOWN_ICON)
        priorityIcon = None
        if priority is not None:
            priorityIcon = cls.getPriorityIconDict().get(priority)
        pipelineIcon = cls.getPipelineIconDict().get(pipelineStage)

        reworkIcon = None
        if rework is True:
            reworkIcon = cls.REWORK_ICON

        paintBuff = 10
        priorityIconWidth = 0
        if priorityIcon is None:
            priorityIconWidth = typeIcon.width() + paintBuff
        else:
            priorityIconWidth = priorityIcon.width() + paintBuff

        pipelineIconWidth = 0
        if pipelineIcon is None:
            pipelineIconWidth = typeIcon.width() + paintBuff
        else:
            pipelineIconWidth = pipelineIcon.width() + paintBuff

        reworkIconWidth = 0
        if reworkIcon is None:
            reworkIconWidth = typeIcon.width() + paintBuff
        else:
            reworkIconWidth = reworkIcon.width() + paintBuff

        finalWidth = typeIcon.width() + priorityIconWidth + pipelineIconWidth + reworkIconWidth + paintBuff
        combinedIcon = QtGui.QPixmap(finalWidth, typeIcon.height())
        combinedIcon.fill(QtCore.Qt.transparent)
        painter = QtGui.QPainter(combinedIcon)

        iconsCombined = 0
        leftCorner = 0
        for icon in [priorityIcon, typeIcon, pipelineIcon, reworkIcon]:
            if icon is None:
                leftCorner += typeIcon.width() + paintBuff
            else:
                painter.drawPixmap(leftCorner, 0, icon)
                leftCorner += icon.width() + paintBuff
            iconsCombined += 1

        del painter

        combinedIcon = combinedIcon.scaledToWidth(iconHeight * iconsCombined, QtCore.Qt.SmoothTransformation)
        cls.__cachedIcons[cachedName] = combinedIcon
        return combinedIcon

    @classmethod
    def getTypeIconDict(cls):
        if cls.UNKNOWN_ICON is None:
            cls._setupIcons()

        return {
                animContext.settingsManager.consts.CaptureTypes.BODY_ROM: cls.BODYROM_ICON,
                animContext.settingsManager.consts.CaptureTypes.BODY_SCALE: cls.BODYSCALE_ICON,
                animContext.settingsManager.consts.CaptureTypes.FACE_ROM: cls.FACEROM_ICON,
                animContext.settingsManager.consts.CaptureTypes.HEAD_SCAN: cls.HEADSCAN_ICON,
                animContext.settingsManager.consts.FeatureCategories.CUTSCENE: cls.CUTSCENE_ICON,
                animContext.settingsManager.consts.FeatureCategories.INGAME: cls.INGAME_ICON,
                }

    @classmethod
    def getPriorityIconDict(cls):
        if cls.UNKNOWN_ICON is None:
            cls._setupIcons()

        return {
                animContext.settingsManager.consts.Priorities.PRIORITY_1: cls.PRIORITY1_ICON,
                animContext.settingsManager.consts.Priorities.PRIORITY_2: cls.PRIORITY2_ICON,
                animContext.settingsManager.consts.Priorities.PRIORITY_3: cls.PRIORITY3_ICON,
                animContext.settingsManager.consts.Priorities.PRIORITY_4: cls.PRIORITY4_ICON,
                animContext.settingsManager.consts.Priorities.PRIORITY_5: cls.PRIORITY5_ICON,
                }

    @classmethod
    def getPipelineIconDict(cls):
        if cls.UNKNOWN_ICON is None:
            cls._setupIcons()

        return {
                Const.PipelineStages.CAPTURED: cls.PIPELINE_CAPTURE_ICON,
                Const.PipelineStages.PROCESSED: cls.PIPELINE_PROCESSED_ICON,
                Const.PipelineStages.TRACKED: cls.PIPELINE_TRACKED_ICON,
                Const.PipelineStages.REFINED: cls.PIPELINE_REFINED_ICON,
                Const.PipelineStages.CHAR_MAPPED: cls.PIPELINE_CHARMAPPED_ICON,
                Const.PipelineStages.IMPORTED_INTO_MOBU: cls.PIPELINE_MOBU_ICON,
                Const.PipelineStages.TRIAL_FINISHED: cls.PIPELINE_FINISHED_ICON,
                Const.PipelineStages.ADJUSTED: cls.PIPELINE_ADJUSTED_ICON,
                Const.PipelineStages.ROM_VERIFIED: cls.PIPELINE_VERFIED_ICON,
                Const.PipelineStages.CHECKED_INTO_PERFORCE: cls.PIPELINE_CHECKEDINTOP4_ICON,
                }

    @classmethod
    def _setupIcons(cls):
        rootPath = os.path.abspath(os.path.join(Config.Script.Path.ToolImages, "animData", "icons"))
        cls.UNKNOWN_ICON = QtGui.QPixmap(os.path.join(rootPath, "unknown.png"))
        cls.BODYROM_ICON = QtGui.QPixmap(os.path.join(rootPath, "bodyRom.png"))
        cls.BODYSCALE_ICON = QtGui.QPixmap(os.path.join(rootPath, "bodyScale.png"))
        cls.CUTSCENE_ICON = QtGui.QPixmap(os.path.join(rootPath, "cutscene.png"))
        cls.FACEROM_ICON = QtGui.QPixmap(os.path.join(rootPath, "faceRom.png"))
        cls.HEADSCAN_ICON = QtGui.QPixmap(os.path.join(rootPath, "headScan.png"))
        cls.IGCS_ICON = QtGui.QPixmap(os.path.join(rootPath, "igCS.png"))
        cls.INGAME_ICON = QtGui.QPixmap(os.path.join(rootPath, "ingame.png"))

        cls.REWORK_ICON = QtGui.QPixmap(os.path.join(rootPath, "rework.png"))

        cls.PRIORITY1_ICON = QtGui.QPixmap(os.path.join(rootPath, "priority_1.png"))
        cls.PRIORITY2_ICON = QtGui.QPixmap(os.path.join(rootPath, "priority_2.png"))
        cls.PRIORITY3_ICON = QtGui.QPixmap(os.path.join(rootPath, "priority_3.png"))
        cls.PRIORITY4_ICON = QtGui.QPixmap(os.path.join(rootPath, "priority_4.png"))
        cls.PRIORITY5_ICON = QtGui.QPixmap(os.path.join(rootPath, "priority_5.png"))

        cls.PIPELINE_CAPTURE_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_Captured.png"))
        cls.PIPELINE_TRACKED_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_Tracked.png"))
        cls.PIPELINE_CHARMAPPED_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_CharMapped.png"))
        cls.PIPELINE_PROCESSED_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_Processed.png"))
        cls.PIPELINE_REFINED_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_Refined.png"))
        cls.PIPELINE_MOBU_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_Mobu.png"))
        cls.PIPELINE_FINISHED_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_Finished.png"))
        cls.PIPELINE_ADJUSTED_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_Adjusted.png"))
        cls.PIPELINE_VERFIED_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_Verfied.png"))
        cls.PIPELINE_CHECKEDINTOP4_ICON = QtGui.QPixmap(os.path.join(rootPath, "pipeline_CheckedIntoP4.png"))

    def isSelected(self):
        return self._isSelect

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()
        if role == QtCore.Qt.BackgroundColorRole:
            if self._isSelect is True:
                return Const.ModelColours.SELECT
            else:
                return Const.ModelColours.NONSELECT
        elif role == QtCore.Qt.DecorationRole:
            if column == 0:
                return self._icon
        elif role == QtCore.Qt.SizeHint:
            return self._icon.size()

        return super(BaseTrialsItem, self).data(index, role)

    def _populateChildren(self):
        return []

    def canFetchMore(self, parent):
        return False

    def rowCount(self, parent=None):
        return 0

    def hasChildren(self, parent=None):
        return False
