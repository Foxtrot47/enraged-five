from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.PyCore.Decorators import memorize
from RS.Core.AnimData import Context


class NoteWidgetBase(QtGui.QFrame):

    noteDeleted = QtCore.Signal(object)
    noteEdited = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(NoteWidgetBase, self).__init__(parent=parent)
        self.setupUi()

    def setupUi(self):
        """
        Setup the Note
        """
        # Create Layouts
        mainLayout = QtGui.QGridLayout()
        editLayout = QtGui.QHBoxLayout()

        # Create Widgets
        self._createdByCombo = QtGui.QComboBox()
        self._createdOnLabel = QtGui.QLabel("<No Date>")
        self._modifiedOnLabel = QtGui.QLabel("<No Date>")
        self._roleCombo = QtGui.QComboBox()
        self._editNoteButton = QtGui.QPushButton("Edit Note")
        self._commentTextEditor = QtGui.QPlainTextEdit()
        self._editOptions = QtGui.QWidget()

        self._deleteNoteButton = QtGui.QPushButton("Delete Note")
        self._saveNoteButton = QtGui.QPushButton("Save Changes")
        self._cancelNoteButton = QtGui.QPushButton("Cancel Changes")

        # Configure Widgets
        for item in Context.settingsManager.getProductionRoles():
            self._roleCombo.addItem(item.name, item)
        self._roleCombo.setEnabled(False)
        self._createdByCombo.setEnabled(False)

        # Add to layouts
        mainSpacer = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        mainLayout.addWidget(QtGui.QLabel("Created by:"), 0, 0)
        mainLayout.addWidget(self._createdByCombo, 0, 1)
        mainLayout.addWidget(QtGui.QLabel("Created on:"), 1, 0)
        mainLayout.addWidget(self._createdOnLabel, 1, 1)

        mainLayout.addWidget(QtGui.QLabel("Modified on:"), 0, 2)
        mainLayout.addWidget(self._modifiedOnLabel, 0, 3)
        mainLayout.addWidget(QtGui.QLabel("Role:"), 1, 2)
        mainLayout.addWidget(self._roleCombo, 1, 3)

        mainLayout.addItem(mainSpacer, 0, 4, 1, 2)
        mainLayout.addWidget(self._editNoteButton, 0, 5, 2, 1)

        mainLayout.addWidget(self._commentTextEditor, 2, 0, 1, 6)
        mainLayout.addWidget(self._editOptions, 3, 0, 1, 6)

        editSpacer = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        editLayout.addWidget(self._deleteNoteButton)
        editLayout.addItem(editSpacer)
        editLayout.addWidget(self._cancelNoteButton)
        editLayout.addWidget(self._saveNoteButton)

        # Edit Properties
        editLayout.setContentsMargins(0, 0, 0, 0)

        # Set Layouts
        self._editOptions.setLayout(editLayout)
        self.setLayout(mainLayout)

        # Setup Connections
        self._editNoteButton.pressed.connect(self.toggleEditMode)
        self._deleteNoteButton.pressed.connect(self._deleteNote)
        self._saveNoteButton.pressed.connect(self._saveEdits)
        self._cancelNoteButton.pressed.connect(self._cancelEdits)

        self.setFrameShape(QtGui.QFrame.Box)

        self.editMode(False)
        self._populateComment()

    @memorize.memoized
    @staticmethod
    def __populateUsernamesFromProject(project):
        return {tal.name:tal for tal in project.getAllStaff()}

    def _populateUsernames(self, project):
        self._createdByCombo.clear()
        if self._context is not None:
            data = self.__populateUsernamesFromProject(project)
            names = data.keys()
            names.sort()
            for name in names:
                self._createdByCombo.addItem(name, data[name])
        self._createdByCombo.setEnabled(False)

    def _getUsernameIndex(self, userName):
        for idx in xrange(self._createdByCombo.count()):
            if self._createdByCombo.itemData(idx, QtCore.Qt.UserRole) == userName:
                return idx
        return -1

    def _getRoleIndex(self, role):
        for idx in xrange(self._roleCombo.count()):
            if self._roleCombo.itemData(idx, QtCore.Qt.UserRole) == role:
                return idx
        return -1

    def _populateComment(self):
        """
        Virtual Method
        """
        pass

    def _cancelEdits(self):
        """
        Virtual Method
        """
        self.editMode(False)

    def _saveEdits(self):
        """
        Virtual Method
        """
        self.editMode(False)
        self.noteEdited.emit(self)

    def _deleteNote(self):
        """
        Virtual Method
        """
        self.noteDeleted.emit(self)

    def isEditing(self):
        return self._editOptions.isVisible() is True

    def editMode(self, val):
        if val is True:
            self._editOptions.show()
            self._commentTextEditor.setReadOnly(False)
        else:
            self._commentTextEditor.setReadOnly(True)
            self._editOptions.hide()

    def toggleEditMode(self):
        if self._editOptions.isVisible() is True:
            self._cancelEdits()
            self.editMode(False)
        else:
            self.editMode(True)


class NewNoteWidget(NoteWidgetBase):

    newNoteAdded = QtCore.Signal()

    def __init__(self, trialContext, parent=None):
        self._context = trialContext
        super(NewNoteWidget, self).__init__(parent=parent)
        self._deleteNoteButton.hide()
        self._editNoteButton.hide()

        self._cancelNoteButton.setText("Cancel")
        self._saveNoteButton.setText("Add Note")

    def _populateComment(self):
        """
        Virtual Method
        """
        self._populateUsernames(self._context.project())
        self._createdByCombo.setEnabled(True)
        self._roleCombo.setEnabled(True)
        self._createdOnLabel.setText("N/A")
        self._modifiedOnLabel.setText("N/A")

        self.editMode(True)

    def _cancelEdits(self):
        """
        Virtual Method
        """
        self.close()

    def _saveEdits(self):
        """
        Virtual Method
        """

        tal = self._createdByCombo.itemData(self._createdByCombo.currentIndex(), QtCore.Qt.UserRole)
        role = self._roleCombo.itemData(self._roleCombo.currentIndex(), QtCore.Qt.UserRole)
        try:
            self._context.addNewTrialNote(self._commentTextEditor.toPlainText(), role, tal)
        except Exception, error:
            QtGui.QMessageBox.critical(self, "Add Note", "Unable to add note with following error:{}".format(error.message))
            return
        self.newNoteAdded.emit()
        super(NewNoteWidget, self)._saveEdits()
        self.close()

    def _deleteNote(self):
        """
        Virtual Method
        """
        import pdb; pdb.set_trace()
        self.close()


class NoteWidget(NoteWidgetBase):
    def __init__(self, motionTrialNote, parent=None):
        self._context = motionTrialNote
        super(NoteWidget, self).__init__(parent=parent)

    def updateNote(self, note):
        self._context = note
        self._populateComment()

    def _populateComment(self):
        """
        Virtual Method
        """
        self._populateUsernames(self._context.project())
        self._createdByCombo.setCurrentIndex(self._getUsernameIndex(self._context.creator()))
        self._createdOnLabel.setText(self._context.creationTime())
        self._modifiedOnLabel.setText(self._context.lastModifiedTime() or "N/A")
        self._commentTextEditor.setPlainText(self._context.comment())
        self._roleCombo.setCurrentIndex(self._getRoleIndex(self._context.role()))

        self._editNoteButton.setEnabled(True)

    def _cancelEdits(self):
        """
        Virtual Method
        """
        super(NoteWidget, self)._cancelEdits()
        self._populateComment()

    def _saveEdits(self):
        """
        Virtual Method
        """
        super(NoteWidget, self)._saveEdits()
        self._context.editComment(self._commentTextEditor.toPlainText())

    def _deleteNote(self):
        """
        Virtual Method
        """
        self._context.deleteNote()
        super(NoteWidget, self)._deleteNote()
        self.close()


if __name__ == "__main__":
    import sys
    from RS.Core.AnimData import Context
    from RS.Core.AnimData._internal import services

    app = QtGui.QApplication(sys.argv)

    # Setup the services for debug and get a trial
    proj = Context.animData.getProjectByName("Redemption2")
    shoots = proj.getAllShoots()
    sessions = shoots[0].getAllSessions()
    trials = sessions[1].getAllTrials()
    trial = trials[0]
    notes = trial.getTrialNotes()
    wid = NewNoteWidget(trial)
    #wid = NoteWidget(notes[0])
    wid.show()
    sys.exit(app.exec_())
