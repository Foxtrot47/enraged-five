"""
Runs all the animdata tests via nose.
"""
import os


def getTestsDir():
    """
    Get the directory where the Perforce tests are located.

    Returns:
        str
    """
    from RS.Core.AnimData.Tests import test_fileManager  # Defer this import until run, as it is not required for production.
    return os.path.dirname(test_fileManager.__file__)


def runTests(testsDir=None):
    """
    Run all tests in a directory via Nose.

    Args:
        testsDir (str): The path to the directory where tests are located.
    """
    import nose  # Defer this import until run, as it is not required for production.

    testLoader = nose.loader.TestLoader()
    testSuite = testLoader.discover(testsDir)
    nose.run(suite=testSuite)


if __name__ == '__builtin__':  # The "main" clause required for running from the Python Editor in MotionBuilder.
    testsDir = getTestsDir()
    runTests(testsDir)
