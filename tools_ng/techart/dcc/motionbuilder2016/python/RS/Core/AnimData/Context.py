"""
Anim Data Module for dealing with Mocap Contexts and services such as watchstar

More info: https://hub.rockstargames.com/display/RSGTECHART/AnimData
"""
import datetime
import os
import platform
import socket

from RS.Core.AnimData._internal import contexts, services, stages, settings, settingsConst
from RS import ProjectData
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize
from RS.Tools.CameraToolBox.PyCore.Metaclasses import singleton


class AnimData(contexts._contextBase):
    """
    Singleton class to create and hold the services that the child classes will use
    """
    __metaclass__ = singleton.Singleton

    def __init__(self, service=None):
        """
        Constructor
        """
        service = service or services.Services()
        super(AnimData, self).__init__(service)
        self.setServices(service)
        service._animDataContext = self
        service._animDataSettingsManager = SettingsManager()

    def setServices(self, services):
        """
        Set the services to use

        args:
            services (Services): The services to use
        """
        self._services = services

    @memorize.memoized
    def _getAllLocations(self):
        """
        Internal Method

        Get all the shooting locations

        return:
            returns a list of Location classes
        """
        data = self.getServices().watchstar.executeCommand("tools.LocationSearch",
                                                           [self.getServices().getUserName()])
        if data is None:
            return []
        return [stages.Location(self.getServices(), locData) for locData in data.get("locations")]

    def getAllLocations(self, includeLocal=False, includeOffsite=False, includeNonActive=False):
        """
        Get all the shooting locations

        kwargs:
            includeLocal (bool): if the local location/stage should be included
            includeOffsite (bool): if the offsite locations should be included
            includeNonActive (bool): if non-active locations should be included

        return:
            returns a list of Location classes
        """
        returnLocs = self._getAllLocations()

        if includeOffsite is False:
            returnLocs = [loc for loc in returnLocs if loc.offsite() is False]

        if includeNonActive is False:
            returnLocs = [loc for loc in returnLocs if loc.locationActive() is True]

        if includeLocal is True:
            returnLocs.append(stages.LocalLocation(self.getServices()))

        return returnLocs

    @memorize.memoized
    def getStageFromID(self, stageId, includeLocal=False, includeOffsite=False, includeNonActive=True):
        """Get the stage from its ID

        Returns:
            stage (Stage): The stage with the given ID
            includeLocal (bool): Search through Local Location stages
            includeOffsite (bool): Search through Offsite stages
            includeNonActive (bool): Search through Non-Active stages
        """
        if stageId is None:
            return None
        stageId = int(stageId)
        allStages = {}
        for loc in self.getAllLocations(includeLocal, includeOffsite, includeNonActive):
            allStages.update({stg.stageId: stg for stg in loc.getStages()})
        return allStages.get(stageId)

    @memorize.memoized
    def getLocationFromID(self, locId):
        """
        Get the location from its ID

        return:
            location (Location): The location with the given ID
        """
        for loc in self.getAllLocations():
            if loc.locationID == locId:
                return loc
        return None

    def getCurrentSessionByStage(self, stage):
        """
        Get the current Session for the given stage

        args:
            stage (Stage): The stage to get the current session for the given stage

        return:
            Session object that is being used at that stage
        """
        trial = self.getCurrentTrialByStage(stage)
        if trial is None:
            return None
        return trial.session()

    def getCurrentTrialByStage(self, stage):
        """
        Get the current Trial on a given stage

        args:
            stage (Stage): The Stage to get the current trial for

        return:
            Trial object that is the active trial on that stage
        """
        data = self.getServices().watchstar.executeCommand("tools.CaptureSearchByCurrentDayStage",
                                                           [
                                                               self.getServices().getUserName(),
                                                               stage.stageId
                                                           ])
        if data is None:
            return None
        vals = self._handleTrialCreation(data, None, None, None)
        if not vals:
            return None
        return vals[0]

    def getRawTrialByID(self, trialId):
        """
        Get a trial by its ID, does not cache!!

        return:
            Trial object that is associated with the trial ID
        """
        data = self.getServices().watchstar.executeCommand("tools.CaptureSearchByID",
                                                           [
                                                               self.getServices().getUserName(),
                                                               trialId
                                                           ])
        if data is None:
            return None
        vals = self._handleTrialCreation(data, None, None, None)
        if not vals:
            return None
        return vals[0]

    @memorize.memoizeWithExpiry(30)
    def getTrialByID(self, trialId):
        """
        Get a trial by its ID

        return:
            Trial object that is associated with the trial ID
        """
        return self.getRawTrialByID(trialId)

    @memorize.memoized
    def getShootByID(self, shootID):
        """
        Get a shoot in its ID

        return:
            None or a single shoot (Shoots) shoot
        """
        data = self._services.watchstar.executeCommand("tools.ShootSearchByID",
                                                       [
                                                           self._services.getUserName(),
                                                           shootID
                                                       ])
        if data is None:
            return None
        returnedShoots = self._handleShootsCreation(data, None)
        if len(returnedShoots) != 1:
            return None
        return returnedShoots[0]

    @memorize.memoized
    def getProjectByID(self, projectID):
        """
        Get a project by its ID

        return:
            Project object that is associated with the project ID
        """
        data = self.getServices().watchstar.executeCommand("tools.AnyProjectSearchByProject",
                                                           [
                                                               self.getServices().getUserName(),
                                                               projectID
                                                           ])
        if data is None:
            return None
        projs = self._handleProjectCreation(data)
        if len(projs) > 0:
            return projs[0]
        return None

    @memorize.memoized
    def getProjectsByTalentID(self, talentID):
        """
        Get a projects by a talent ID

        return:
            Projects object that have variations for the given talent ID
        """
        data = self.getServices().watchstar.executeCommand("tools.AnyProjectSearchByTalent",
                                                           [
                                                               self.getServices().getUserName(),
                                                               talentID
                                                           ])
        if data is None:
            return None
        projs = self._handleProjectCreation(data)
        if len(projs) > 0:
            return projs
        return None

    @memorize.memoized
    def getHelmetCams(self):
        """
        Get all the HelmetCams.

        return:
            A list of HelmetCams items
        """
        data = self.getServices().watchstar.executeCommand("tools.HelmetCamSearch",
                                                           [
                                                               self.getServices().getUserName(),
                                                           ])
        if data is None:
            return []
        return self._handleHelmetCamCreation(data)

    @memorize.memoized
    def getHelmetCamCalibrationByID(self, helmetCamCalibrationId):
        """
        Get the HelmetCamCalibration that matches the provided ID.

        Args:
            helmetCamCalibrationId (int): The ID of the desired HelmetCamCalibration.

        Returns:
            HelmetCamCalibration object|None
        """
        data = self.getServices().watchstar.executeCommand("tools.HelmetCamCalibrationSearchByID",
                                                           [
                                                               self.getServices().getUserName(),
                                                               helmetCamCalibrationId,
                                                           ])
        if data is None:
            return None
        return self._handleHelmetCamCalibrationCreation(data)[0]

    @memorize.memoized
    def getHelmetCamRevisionByID(self, helmetCamRevisionId):
        """
        Get the HelmetCamCalibration that matches the provided ID.

        Args:
            helmetCamRevisionId (int): The ID of the desired HelmetCamRevisionId.

        Returns:
            contexts.HelmetCamRevision | None
        """
        data = self.getServices().watchstar.executeCommand("tools.HelmetCamRevisionSearchByID",
                                                           [
                                                               self.getServices().getUserName(),
                                                               helmetCamRevisionId,
                                                           ])
        if data is None:
            return None
        # This is a little bit of an awkward implementation, but better than parsing the data dict outside the handler.
        helmetCams = self._handleHelmetCamCreation(data)
        return helmetCams[0].revisions()[0]

    @memorize.memoized
    def getStageCalibrationByID(self, stageCalibrationId):
        """
        Get the StageCalibration that matches the provided ID.

        Args:
            stageCalibrationId (int): The ID of the desired StageCalibration.

        Returns:
            StageCalibration object|None
        """
        data = self.getServices().watchstar.executeCommand("tools.StageCalibrationSearchByID",
                                                           [
                                                               self.getServices().getUserName(),
                                                               stageCalibrationId,
                                                           ])
        if data is None:
            return None
        return self._handleStageCalibrationCreation(data)[0]

    @memorize.memoized
    def getCurrentLocation(self):
        """
        Get the current Location of the computer based off its HostName

        return:
            A Location object of that studio or None if unknown studio
        """
        hostName = socket.gethostname().lower()
        for location in self.getAllLocations():
            locHostName = location.hostName()
            if hostName.startswith(locHostName) and locHostName != "":
                return location
        return None

    def getLocationSessions(self, location=None, date=None):
        """
        Get the sessions for today at the given location, or a given date

        kwargs:
            location (Location): The location to find the sessions for
            date (DateTime): The Date to find the sessions for

        returns:
            list of Session objects
        """

        date = date or datetime.datetime.now()
        location = location or self.getCurrentLocation()
        if location is None:
            raise ValueError("No locaiton specified and unable to retrieve current location")

        kwargs = [
            "@LocationID={}".format(location.id),
            "@SessionDate='{}'".format(date.strftime(self.DATE_FORMAT)),
        ]
        data = self.getServices().watchstar.executeCommand("tools.ShootSessionSearchByLocationDate",
                                                           [self.getServices().getUserName()],
                                                           kwargList=kwargs
                                                           )
        if data is None:
            return None
        return self._handleSessionCreation(data, None, None)

    @memorize.memoized
    def getCurrentProject(self):
        """
        Get the current project based off the local environment settings

        return:
            A Project class that represents the project, or None if not found
        """
        # When outside of a project the project name should be an empty string
        return self.getProjectByName(ProjectData.data.GetAnimDataProjectName())

    @memorize.memoized
    def getAllProjects(self):
        """
        Get all the projects that the user has access to

        return:
            returns a list of Project classes for the different projects
        """
        data = self.getServices().watchstar.executeCommand("tools.AnyProjectSearchAll", [self.getServices().getUserName()])
        if data is None:
            return []
        projs = self._handleProjectCreation(data)
        if not projs:
            return []
        projs.sort()
        return projs

    @memorize.memoized
    def getProjectByName(self, projectName, allowCodenames=True):
        """
        Get a certain project by its name

        Warnings:
            To enable the use of the "RS_USEPROJCODENAMES" environment variable switch for stage facing tools, this
            method MUST use the projectName() method over the name property.

        args:
            projectName (str): The project name

        kwargs:
            allowCodenames (bool): If project codenames should be checked for find the project

        return:
            A Project class that represents the project, or None if not found
        """
        if projectName is not None:
            projs = self.getAllProjects()
            for proj in projs:
                if projectName.lower() == proj.projectName().lower():
                    return proj
                if allowCodenames is True and projectName.lower() == proj.codeName().lower():
                    return proj
        return None


class SettingsManager(contexts._contextBase):
    """
    Single class for managing Enum setting values from Watchstar
    """
    __metaclass__ = singleton.Singleton

    def __init__(self, service=None):
        """
        constructor
        """
        super(SettingsManager, self).__init__(service)
        self._settingConsts = settingsConst.SettingsConst(self)

    @property
    def consts(self):
        return self._settingConsts

    def setServices(self, services):
        """
        Set the services to use

        args:
            services (Services): The services to use
        """
        self._services = services

    def getServices(self):
        """
        Get the services with the current context

        return:
            (AnimData.services.Services): The services for communicating with databases
        """
        return self._services or AnimData().getServices()

    @memorize.memoized
    def getVideoSettings(self, settingID=None):
        """
        Get all the video settings, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A VideoSettings list of single item
        """
        settingsData = []
        if len(settingsData) == 0:
            data = [(1, "ntsc", "ntsc"),
                    (2, "pal", "pal"),
                    (3, "1080p30", "hp30"),
                    (4, "1080p60", "hp60"),
                    (5, "720p30", "hp30"),
                    (6, "720p60", "hp60"),
                    (7, "720p25", "hp25"),
                    ]
            for num, name, code in data:
                newDict = {"name": name, "id": num, "blackMagicCodeName": code}
                settingsData.append(settings.VideoSetting(newDict))

        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getCaptureTypes(self, settingID=None):
        """
        Get all the capture types, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A CaptureType list of single item
        """
        settingsData = self._getCaptureTypes()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getCaptureTypesByName(self, name):
        """
        Get a capture types by its name

        args:
            name (str): The settings name to get

        return:
            A CaptureType item
        """
        for setting in self.getCaptureTypes():
            if setting.name == name:
                return setting
        return None

    @memorize.memoized
    def getPipelineSteps(self, settingID=None):
        """
        Get all the Pipeline Step settings, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A PipelineStep list of single item
        """
        settingsData = self._getPipelineSteps()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    def _getSettingsLogic(self, settingsList, targetClass):
        """
        Internal Method

        Factory method for creating the list of setting objects.

        args:
            settingsList (list of dict): A list of settings dictionaries from Watchstar.
            targetClass (class): Class type to instantiate from a settings dictionary.

        return:
            list: A list of targetClass instances
        """
        if settingsList is None:
            return []
        return [targetClass(settings) for settings in settingsList]

    def _runLookupCommand(self, command):
        """
        Internal Method

        Runs the given command and adds the username to the args, and runs upto 10 times to makes
        sure data is returned before erroring out

        args:
            command (str): The string command to run

        return:
            dict of returned data
        """
        userName = self.getServices().getUserName()
        for _ in xrange(10):
            data = self.getServices().watchstar.executeCommand(command, [userName])
            if data is not None:
                return data
        raise ValueError("Error running look up command from Watchstar. "
                         "Command '{}' for user '{}'".format(command, userName))

    @memorize.memoized
    def _getCaptureTypes(self):
        """
        Internal Method

        Get the capture Types settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupCaptureTypes")
        return self._getSettingsLogic(data.get("captureTypes"), settings.CaptureTypes)

    @memorize.memoized
    def _getPipelineSteps(self):
        """
        Internal Method

        Get the pipeline steps settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupPipelineSteps")
        return self._getSettingsLogic(data.get("pipelineSteps"), settings.PipelineStep)

    @memorize.memoized
    def _getLatticeStatus(self):
        """
        Internal Method

        Get the Lattice Status settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupLatticeStatus")
        return self._getSettingsLogic(data.get("latticeStatuses"), settings.LatticeStatus)

    @memorize.memoized
    def _getVariations(self):
        """
        Internal Method

        Get the Variations settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupVariations")
        return self._getSettingsLogic(data.get("variations"), settings.Variations)

    @memorize.memoized
    def _getFilePathTableNames(self):
        """
        Internal Method

        Get the Path Type settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupFilePathTableNames")
        return self._getSettingsLogic(data.get("filePathTableNames"), settings.FilePathTableNames)

    @memorize.memoized
    def _getFilePathTypes(self):
        """
        Internal Method

        Get the Path Type settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.PathTypeSearch")
        return self._getSettingsLogic(data.get("pathTypes"), settings.FilePathTypes)

    @memorize.memoized
    def _getFaceAudioPack(self):
        """
        Internal Method

        Get the AudioFaceDevice settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.AudioFaceDeviceSearch")
        return self._getSettingsLogic(data.get("audioFaceDevices"), settings.FaceAudioPack)

    @memorize.memoized
    def _getUsers(self, showAssignmentUsersOnly=True):
        """
        Internal Method

        Get the User settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self.getServices().watchstar.executeCommand("tools.LookupUsers",
                                                           [
                                                               self.getServices().getUserName(),
                                                               showAssignmentUsersOnly
                                                           ])
        return self._getSettingsLogic(data.get("users"), settings.User)

    @memorize.memoized
    def _getUserAssignmentTasks(self):
        """
        Internal Method
        Get the User Assignment Tasks settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupUserAssignmentTasks")
        return self._getSettingsLogic(data.get("userAssignmentTasks"), settings.UserAssignmentTasks)

    @memorize.memoized
    def _getPriority(self):
        """
        Internal Method

        Get the Priority settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupPriority")
        return self._getSettingsLogic(data.get("priorities"), settings.Priority)

    @memorize.memoized
    def _getFeatureTrialType(self):
        """
        Internal Method

        Get the Feature Trial Type settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator
        """
        data = self._runLookupCommand("tools.LookupFeatureTrialContentTypes")
        return self._getSettingsLogic(data.get("featureTrialContentTypes"), settings.FeatureTrialType)

    @memorize.memoized
    def _getAssetTypes(self):
        """
        Internal Method

        Get the Character Sub Type settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.Asset3DTypeSearch")
        return self._getSettingsLogic(data.get("asset3DTypes"), settings.AssetType)

    @memorize.memoized
    def _getAssetClasses(self):
        """
        Internal Method

        Get the Asset settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupAssetClass")
        return self._getSettingsLogic(data.get("assetClasses"), settings.AssetClass)

    @memorize.memoized
    def _getFeatureCategories(self):
        """
        Internal Method

        Get the Feature Types settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupFeatureCategories")
        return self._getSettingsLogic(data.get("featureCategories"), settings.FeatureCategory)

    @memorize.memoized
    def _getFeatureTypes(self):
        """
        Internal Method

        Get the Feature Types settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.FeatureTypeSearch")
        return self._getSettingsLogic(data.get("featureTypes"), settings.FeatureType)

    @memorize.memoized
    def _getProductionRoles(self):
        """
        Internal Method

        Get the Production Role settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.ProductionRoleSearch")
        return self._getSettingsLogic(data.get("productionRoles"), settings.ProductionRole)

    @memorize.memoized
    def _getMarkerSets(self):
        """
        Internal Method

        Get the Markerset settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.MarkersetSearch")
        return self._getSettingsLogic(data.get("markersets"), settings.MarkerSet)

    @memorize.memoized
    def _getCameraTypes(self):
        """
        Internal Method

        Get the Markerset settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.CameraTypeSearch")
        return self._getSettingsLogic(data.get("cameraTypes"), settings.CameraType)

    @memorize.memoized
    def _getMocapPropCategories(self):
        """
        Internal Method

        Get the MocapPropCategory settings list from the database. Could be expensive if called a lot so the result
        is cached via the memoize decorator. Might make this a time limited memoize method
        """
        data = self._runLookupCommand("tools.LookupStagePropCategories")
        return self._getSettingsLogic(data.get("stagePropCategories"), settings.MocapPropCategory)

    @memorize.memoized
    def getFilePathTableNames(self, settingID=None):
        """
        Get all the file path types, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A FilePathTypes list of single item
        """
        settingsData = self._getFilePathTableNames()
        if settingID is None:
            return settingsData
        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getFilePathTypes(self, settingID=None):
        """
        Get all the file path types, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A FilePathTypes list of single item
        """
        settingsData = self._getFilePathTypes()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getFilePathTypeByName(self, name):
        """
        Get a File Path types by its name

        kwargs:
            name (str): The settings name to get

        return:
            A FilePathType item
        """
        for setting in self.getFilePathTypes():
            if setting.name == name:
                return setting
        return None

    @memorize.memoized
    def getLatticeStatus(self, settingID=None):
        """
        Get all the Lattice Status, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A VideoType list of single item
        """
        settingsData = self._getLatticeStatus()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getVariations(self, settingID=None):
        """
        Get all the Variations, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A VideoType list of single item
        """
        settingsData = self._getVariations()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getUsers(self, settingID=None, showAssignmentUsersOnly=True):
        """
        Get all the Users, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A User list of single item
        """
        settingsData = self._getUsers(showAssignmentUsersOnly=showAssignmentUsersOnly)
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getUserAssignmentTasks(self, settingID=None):
        """
        Get all the UserAssignmentTasks, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A UserAssignmentTasks list of single item
        """
        settingsData = self._getUserAssignmentTasks()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getFaceAudioPack(self, settingID=None):
        """
        Get all the FaceAudioPack, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A FaceAudioPack list of single item
        """
        settingsData = self._getFaceAudioPack()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getFaceAudioPackByName(self, name):
        """
        Get a FaceAudioPack by its name

        kwargs:
            name (str): The settings name to get

        return:
            A FaceAudioPack item
        """
        for setting in self.getFaceAudioPack():
            if setting.name == name:
                return setting
        return None

    @memorize.memoized
    def getFeatureTrialType(self, settingID=None):
        """
        Get all the Feature Trial Type, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A Feature Trial Type list of single item
        """
        settingsData = self._getFeatureTrialType()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getFeatureTrialTypeByName(self, name):
        """
        Get a Feature Trial Type by its name

        args:
            name (str): The settings name to get

        return:
            A Feature Trial Type item
        """
        for setting in self.getFeatureTrialType():
            if setting.name == name:
                return setting
        return None

    @memorize.memoized
    def getPriority(self, settingID=None):
        """
        Get all the Priority, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A Priority list of single item
        """
        settingsData = self._getPriority()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getPriorityByName(self, name):
        """
        Get a Priority by its name

        args:
            name (str): The settings name to get

        return:
            A Priority item
        """
        for setting in self.getPriority():
            if setting.name == name:
                return setting
        return None

    @memorize.memoized
    def getAssetType(self, settingID=None):
        """
        Get all the Asset3dTypes, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            A AssetType list of single item
        """
        settingsData = self._getAssetTypes()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getAssetTypesFromAssetClass(self, assetClass):
        return [setting for setting in self.getAssetType() if setting.assetClassId() == assetClass.settingsID]

    @memorize.memoized
    def getAssetClasses(self, settingID=None):
        """
        Get all the Asset3dClasses, or a specific settings ID if provided.

        note:
            Asset3d and AssetScript search procs both use these class IDs.

        kwargs:
            settingID (int): The settings Index to get

        return:
            A AssetClass list of single item
        """
        settingsData = self._getAssetClasses()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getAssetClassByName(self, name):
        """
        Get an AssetClass by its name.

        kwargs:
            name (str): The settings name to get

        return:
            A AssetClass item
        """
        for setting in self.getAssetClasses():
            if setting.name == name:
                return setting
        return None

    @memorize.memoized
    def getFeatureCategories(self, settingID=None):
        """
        Get all the feature categories settings, or a specific settings ID if provided.

        kwargs:
            settingID (int): The settings Index to get.

        return:
            list of settings.FeatureCategory or single settings.FeatureCategory object
        """
        settingsData = self._getFeatureCategories()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getFeatureTypes(self, settingID=None):
        """
        Get all the feature type settings, or a specific settings ID if provided.

        kwargs:
            settingID (int): The settings Index to get.

        return:
            list of settings.FeatureType or single settings.FeatureType object
        """
        settingsData = self._getFeatureTypes()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getFeatureCategoryByName(self, name):
        """
        Get a Feature Category by its name

        kwargs:
            name (str): The settings name to get

        return:
            A FeatureCategory item
        """
        for setting in self.getFeatureCategories():
            if setting.name == name:
                return setting
        return None

    def getProductionRoles(self, settingID=None):
        """
        Get all the production role settings, or a specific settings ID if provided.

        kwargs:
            settingID (int): The settings Index to get.

        return:
            list of settings.ProductionRole or single settings.ProductionRole object
        """
        settingsData = self._getProductionRoles()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getMarkerSets(self, settingID=None):
        """
        Get all markersets, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            list of settings.MarkerSet: if no id provided | settings.MarkerSet: if id matches | None: if no id matches
        """
        settingsData = self._getMarkerSets()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getCameraTypes(self, settingID=None):
        """
        Get all camera types, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            list of settings.CameraType: if no id provided | settings.CameraType: if id matches | None: if no id matches
        """
        settingsData = self._getCameraTypes()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getMocapPropCategories(self, settingID=None):
        """
        Get all mocap prop categories, or a specific settings ID if provided

        kwargs:
            settingID (int): The settings Index to get

        return:
            list of settings.MocapPropCategory: if no id provided | settings.MocapPropCategory: if id matches | None: if no id matches
        """
        settingsData = self._getMocapPropCategories()
        if settingID is None:
            return settingsData

        for setting in settingsData:
            if setting.settingsID == settingID:
                return setting
        return None

    @memorize.memoized
    def getMocapPropCategoryByName(self, name):
        """
        Get a Mocap Prop Category by its name

        kwargs:
            name (str): The settings name to get

        return:
            A MocapPropCategory item
        """
        for setting in self.getMocapPropCategories():
            if setting.name == name:
                return setting
        return None


def setServices(services):
    """
    Set the services used by AnimData

    args:
        services (Services): The services to use
    """
    AnimData().setServices(services)
    SettingsManager().setServices(services)
    services._animDataContext = AnimData()
    services._animDataSettingsManager = SettingsManager()


animData = AnimData()
settingsManager = SettingsManager()
