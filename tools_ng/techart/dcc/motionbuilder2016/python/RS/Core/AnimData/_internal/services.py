"""
Services Module for AnimData which deals with the connection to the different databases

Warning:
    DO NOT import RS.Utils.Logging.Universal here. This module is called outside MoBu and the MoBu
    specific Universal class will trigger various issues if run in a standard Python interpreter.
"""
import uuid
import time
import logging
import getpass
import json  # We will need to build ujson against Mobu Python to be able to use it here.

from RS.Core import DatabaseConnection
from RS.Core.Database import ActiveDirectory
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize


# There is a Universal logger instance in MoBu that captures stdout. These log messages will go there.
LOG = logging.getLogger("AnimData")


class WatchstarDefaultsProduction(object):
    """
    Defaults for the production instance of Watchstar.
    """
    DRIVER = "{SQL Server}"
    SERVER = "watchstar-sql"
    DATABASE = "watchstar_prod"


class WatchstarDefaultsTest(object):
    """
    Defaults for the test instance of Watchstar.
    """
    DRIVER = "{SQL Server}"
    SERVER = "watchstar-sql-test"
    DATABASE = "watchstar_test"


class WatchstarDefaultsDev(object):
    """
    Defaults for the dev instance of Watchstar.
    """
    DRIVER = "{SQL Server}"
    SERVER = "watchstar-sql-dev"
    DATABASE = "watchstar_dev"


class _watchstarConnection(object):
    """
    Internal wrapper for the Watchstar connection.

    Attributes:
         WATCHSTAR_DEFAULTS (class): The defaults to use for the SQL connection.
    """
    _RETRYABLE_ERRORS = (
        "[FreeTDS][SQL Server]Communication link failure (0) (SQLExecDirectW)",
        "[08S01] [FreeTDS][SQL Server]Write to the server failed (20006) (SQLExecDirectW)",
        "[08S01] [FreeTDS][SQL Server]Read from the server failed (20004) (SQLExecDirectW)",
        "[42000] [FreeTDS][SQL Server]SHUTDOWN is in progress. (6005) (SQLExecDirectW)",
        "[42000] [FreeTDS][SQL Server]Transaction (Process ID",
        "[FreeTDS][SQL Server]Read from the server failed (20004) (SQLExecDirectW)",
        "[HYT00] [FreeTDS][SQL Server]Timeout expired (0) (SQLExecDirectW)",
    )

    __CONNECTION_QUEUE = []

    _PERMISSIONS_ERROR = "[SQL Server]THIS USER DOES NOT HAVE ACCESS TO THIS PROJECT (50000) (SQLExecDirectW)"
    _CAPTURE_DOES_NOT_EXIST_ERROR = "[FreeTDS][SQL Server]THIS CAPTURE DOES NOT EXIST IN WATCHSTAR (50000) (SQLExecDirectW)"
    WATCHSTAR_DEFAULTS = WatchstarDefaultsProduction

    def __init__(self, driver=None, server=None, database=None):
        """
        Constructor
        """
        self._dbConnection = None

        self._driver = driver or self.WATCHSTAR_DEFAULTS.DRIVER
        self._server = server or self.WATCHSTAR_DEFAULTS.SERVER
        self._database = database or self.WATCHSTAR_DEFAULTS.DATABASE

        if self._server is not WatchstarDefaultsProduction.SERVER:
            print "Non-Production Watchstar '{}' in use!".format(self._server)

        self._lastReturnValue = None
        self._lastSentValue = None

        if not self._userHasWatchstarPermissions():
            LOG.warning("User does not have permissions for Watchstar!")

        # Mobu only vars
        self._hasTriedToConnect = False
        self._hasConnected = False

    def _createConnection(self):
        self._dbConnection = DatabaseConnection.DatabaseConnection(self._driver, self._server, self._database, raiseErrors=True)

    def getDriver(self):
        return self._driver

    def getServer(self):
        return self._server

    def getDatabase(self):
        return self._database

    def getLastSentValue(self):
        """
        Get the last sent command as a string. Useful for debugging

        return:
            string of the last command sent to the database
        """
        return self._lastSentValue

    def getLastRawReturnValue(self):
        """
        Get the last returned value as a raw string. Useful for debugging

        return:
            string of the last return result from the database
        """
        return self._lastReturnValue

    @memorize.memoized
    def _userHasWatchstarPermissions(self):
        """
        Check if the user is in any of the required Active Directory groups for Watchstar.

        return:
            bool: True if in one of the required groups, False if not or error accessing ActiveDirectory.
        """
        try:
            wsGroups = ["S_RSGLIC_Watchstar_Production", "S_RSGLIC_Watchstar_Animators",]
            return ActiveDirectory.DirectoryServices().isUserInGroups(wsGroups)
        except Exception as error:
            # Added to handle errors from RDU where it is difficult to test new changes.
            try:
                LOG.critical(error.message)
            except Exception as error:
                print error.message
            return False

    def executeCommand(self, toolName, argList=None, expectingResult=True, kwargList=None):
        """
        Run the Watchstar proc on the database.

        args:
            toolName (str): Watchstar proc to run.

        kwargs:
            argList (list of str): Args to add to the command
            expectingResult (bool): If the command is expecting a return result
            kwargList (list): List of keyword args which will defined. eg. ['@sampleTest=2']

        return:
            dict: result of the converted json str.
        """
        if not self._userHasWatchstarPermissions():
            LOG.critical("User does not have permissions for Watchstar!")
            return None

        if self._hasTriedToConnect is True and self._hasConnected is False:
            LOG.critical("Last attempt to connect failed, not trying again!")
            return None

        self._hasTriedToConnect = True
        if self._dbConnection is None:
            self._createConnection()

        if self._dbConnection is None:
            LOG.critical("Unable to connect to the database!")
            return None

        connectionID = uuid.uuid1()
        self.__CONNECTION_QUEUE.append(connectionID)

        while True:
            if self.__CONNECTION_QUEUE[0] == connectionID:
                break
            print "WAITING"
            time.sleep(0.5)

        args = ""
        if argList is not None:
            args = ", ".join(["'{0}'".format(arg) for arg in argList])

        kwargs = ""
        if kwargList is not None:
            kwargs = ", ".join([arg for arg in kwargList])

        argsStr = ",".join([item for item in [args, kwargs] if item != ""])

        query = "exec {0} {1}".format(toolName, argsStr)
        self._lastSentValue = query
        retryCount = 30
        for idx in xrange(retryCount):
            result = None
            try:
                result = self._dbConnection.ExecuteQuery(query, expectingResult)
                self._hasConnected = True
                break
            except Exception, error:
                if idx == retryCount - 1:
                    raise

                retrying = False
                cleanedMsg = error.message.replace("\'", "")
                for message in self._RETRYABLE_ERRORS:
                    if message in cleanedMsg:
                        LOG.warning("Connection Error ({0}). Retrying...".format(idx))
                        retrying = True
                        time.sleep(0.5)
                        break

                if retrying is False:
                    LOG.critical("Critical  Connection Error. Unable to retry. Aborting!")
                    LOG.critical("Command run: {}".format(self._lastSentValue))
                    raise

                # Send a 'close' signal to the database so we can re-create the connection properly
                csr = self._dbConnection.cursor()
                csr.close()
                del csr
                self._dbConnection.close()
                self._createConnection()
            finally:
                # Deal with the race condition where two threads can try to remove the same uuid at the same time.
                try:
                    self.__CONNECTION_QUEUE.remove(connectionID)
                except ValueError, error:
                    if error.message != "list.remove(x): x not in list":
                        raise

        self._lastReturnValue = result

        if result is None:
            return None
        if result == []:
            return None
        if result[0][0] is None:
            return None

        try:
            jsonFormattedStr  = str(result[0][0]).decode("ascii", "ignore").encode("utf8", "ignore")
        except:
            jsonFormattedStr = result[0][0]
        # Hack - occasionally the JSON str is missing the last character, which has been a "}".
        if jsonFormattedStr[-1] != "}":
            jsonFormattedStr = "{}}}".format(jsonFormattedStr)
        data = json.loads(jsonFormattedStr)
        return data


class Services(object):
    """
    Services class to wrap up multiple connection types to allow for easier adding of new sources
    """
    def __init__(self, watchstarConnection=None):
        """
        Constructor
        """
        self._animDataContext = None
        self._animDataSettingsManager = None
        self._watchstarConnection = watchstarConnection or _watchstarConnection()
        self._username = getpass.getuser()

        # TODO: Remove hotfix once permissions are resolved for the svcrsgnycvmx service account.
        # [HOTFIX START]
        # Change the username to prevent errors in automation jobs.
        if self._username.lower() == "svcrsgnycvmx":
            self._username = "svcrsglicshoot"
        # [HOTFIX END]

    def getUserName(self):
        """
        Get the user name as a string

        return:
            The user name as a string
        """
        return self._username

    @property
    def watchstar(self):
        return self._watchstarConnection

    @property
    def animDataContext(self):
        return self._animDataContext

    @property
    def animDataSettingsManager(self):
        return self._animDataSettingsManager

