from PySide import QtGui, QtCore

from RS.Core.AnimData.Widgets import contextWidget, stageContextWidget


class StageTrialContextWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(StageTrialContextWidget, self).__init__(parent=parent)
        self.setupUi()

    def setupUi(self):
        # create layouts
        mainLayout = QtGui.QVBoxLayout()
        contextLayout = QtGui.QHBoxLayout()

        # create widgets
        self._stageWidget = stageContextWidget.StageContextWidget()
        self._contextWidget = contextWidget.ContextComboxBoxView()

        # assign widget to layout
        mainLayout.addWidget(self._stageWidget)
        mainLayout.addWidget(self._contextWidget)

        # assign layout to widget
        self.setLayout(mainLayout)

        # setup connections

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    wid = StageTrialContextWidget()

    wid.show()
    sys.exit(app.exec_())
