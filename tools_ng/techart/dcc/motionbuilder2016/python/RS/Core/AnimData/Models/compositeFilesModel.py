"""
Motion Trials Selects Model
 - Seemed easier than trying to hack it with QTableWidgetItem's
"""
from PySide import QtGui, QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem


class CompositeFilesModelItem(baseModelItem.BaseModelItem):
    """
    This represents a single File
    """

    def __init__(self, fileObj, parent=None):
        """
        Constructor
        """
        super(CompositeFilesModelItem, self).__init__(parent=parent)
        self._fileObj = fileObj

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._fileObj.name
            elif column == 1:
                return self._fileObj.fileType().name
            elif column == 2:
                return self._fileObj.filePath()
            elif column == 3:
                return self._fileObj.thumbnailPath()
            elif column == 4:
                return self._fileObj.fileOnMocapServer()
        elif role == QtCore.Qt.UserRole:
            return self._fileObj

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class AssociatedCompositeFilesModel(baseModel.BaseModel):
    """
    Base Model Type
    """

    def __init__(self, comp=None, parent=None):
        self._comp = comp
        self._filters = []
        super(AssociatedCompositeFilesModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Name", "Type", "Path", "Thumbnail", "MocapP4"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def setCompositeContext(self, comp):
        self._comp = comp
        self.reset()

    def compositeContext(self):
        return self._comp

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._comp is None:
            return

        for fileObj in self._comp.getAllFiles():
            parent.appendChild(CompositeFilesModelItem(fileObj, parent=parent))


if __name__ == "__main__":
    import sys
    
    from RS.Core.AnimData import Context, Const

    app = QtGui.QApplication(sys.argv)
    comp = Context.animData.getProjectByName("Redemption2").getCompositesByRegex("_T01$")[0]
    mainModel = AssociatedCompositeFilesModel(comp)

    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()

    sys.exit(app.exec_())
