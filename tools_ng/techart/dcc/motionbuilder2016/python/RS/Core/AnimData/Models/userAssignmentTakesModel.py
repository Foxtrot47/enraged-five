"""
Models for dealing with contexts
"""
from PySide import QtGui, QtCore

from RS.Core.AnimData.Models import baseContext
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class UserAssignmentTakesBaseTrialsItem(baseContext.BaseTrialsItem):
    
    def __init__(self, context, parent=None, rootItem=None):
        super(UserAssignmentTakesBaseTrialsItem, self).__init__(context, parent=parent, rootItem=rootItem)
        user = self._context.assignmentUser()
        if user is not None:
            self._assignedUser = user.name
        else:
            self._assignedUser = "No User"
            
        task = self._context.assignmentTask()
        if task is not None:
            self._assignedTask = task.name
        else:
            self._assignedTask = "No Task"
    
    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()
        if role == QtCore.Qt.DisplayRole:
            if column == 1:
                return self._assignedUser
            elif column == 2:
                return self._assignedTask
        return super(UserAssignmentTakesBaseTrialsItem, self).data(index, role)

    
class UserAssignmentTakesDataModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, project=None, user=None, task=None, parent=None):
        self._project = project
        self._user = user
        self._task = task
        super(UserAssignmentTakesDataModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Context", "User", "Task"]

    def setProject(self, newProject):
        self._project = newProject
        self.reset()

    def project(self):
        return self._project

    def setUser(self, user):
        self._user = user
        self.reset()

    def setTask(self, task):
        self._task = task
        self.reset()

    def user(self):
        return self._user

    def task(self):
        return self._task

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int number of columns in the model
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._project is None:
            return
        for trial in self._project.getTrialsByUserAssignment(self._user, self._task):
            parent.appendChild(UserAssignmentTakesBaseTrialsItem(trial, parent=parent, rootItem=self))

    def findContextIndex(self, context):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.Context): The context to find

        returns:
            QModelIndex of the location
        """
        return QtCore.QModelIndex()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    from RS.Core.AnimData import Context

    prj = Context.animData.getProjectByName("Redemption2")
    users = Context.settingsManager.getUsers()
    user = users[-1]  # If the user for this index has no tasks, pick another index.
    tasks = Context.settingsManager.getUserAssignmentTasks()

    mainModel = UserAssignmentTakesDataModel(prj)
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()

    win.setModel(mainModel)
    mainModel.setUser(user)
    # mainModel.setTask(tasks[4])
    win.show()
    sys.exit(app.exec_())
