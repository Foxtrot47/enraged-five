from PySide import QtCore, QtGui

from RS.Core.AnimData.Widgets import contextWidget, trialNotesScroller


class OnSetMocapNotesTool(QtGui.QWidget):
    def __init__(self, parent=None):
        super(OnSetMocapNotesTool, self).__init__(parent=parent)
        self.setupUi()

    def setupUi(self):
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()

        # Create Widgets
        self._contextView = contextWidget.ContextComboxBoxView()
        self._notesView = trialNotesScroller.NotesScroller()

        # Assign Widgets to Layouts
        mainLayout.addWidget(self._contextView)
        mainLayout.addWidget(self._notesView)

        # Assign Layouts to Widgets
        self.setLayout(mainLayout)

        # Setup Connections
        self._contextView.trialSelected.connect(self._handleNewTrialSelected)

    def _handleNewTrialSelected(self, newTrial):
        self._notesView.setContext(newTrial)


class MobuOnSetMocapNotesTool(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MobuOnSetMocapNotesTool, self).__init__(parent=parent)
        self.setupUi()

    def setupUi(self):
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()
        notesLayout = QtGui.QVBoxLayout()

        # Create Widgets
        self._contextView = contextWidget.ContextComboxBoxView()
        self._notesView = trialNotesScroller.NotesScroller()
        refreshButton = QtGui.QPushButton("Refresh Notes")

        # Assign Widgets to Layouts
        notesLayout.addWidget(refreshButton)
        notesLayout.addWidget(self._notesView)

        mainLayout.addWidget(self._contextView)
        mainLayout.addLayout(notesLayout)

        # Assign Layouts to Widgets
        self.setLayout(mainLayout)

        # Setup Connections
        self._contextView.trialSelected.connect(self._handleNewTrialSelected)
        refreshButton.pressed.connect(self._handleRefresh)

    def _handleRefresh(self):
        self._notesView.checkForUpdates()

    def _handleNewTrialSelected(self, newTrial):
        self._notesView.setContext(newTrial)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    wid = OnSetMocapNotesTool()
    wid.show()
    sys.exit(app.exec_())
