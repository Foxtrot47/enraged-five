import functools

from PySide import QtGui, QtCore


class ContextComboxBoxView(QtGui.QWidget):
    """
    Abstract Combo Box View

    Will handle the combo boxes updating based off the model.
    To Subclass, the following classes need to be re-implemeneted:
        model - Return the model to use in the View
        _boxDef - Return a list of tuples to describe the combo box names and their model row indexs

    Item Selection hook methods are:
        itemSelected - for when any item is selected
        childItemSelected - for the bottom most item is selected
    """
    def __init__(self, parent=None):
        super(ContextComboxBoxView, self).__init__(parent=parent)

        # Model stuff
        self._mainModel = self.model()
        self._emptyModel = QtGui.QStandardItemModel()
        self._lastEdited = 0

        layout = QtGui.QGridLayout()

        # Build the combos and set their model rows
        boxDef = self._boxDef()
        self._columnTitleBoxes = []
        self.boxes = []
        for idx in xrange(len(boxDef)):
            name, modelIndex = boxDef[idx]
            box = QtGui.QComboBox()
            self.boxes.append(box)
            if idx == 0:
                box.setModel(self._mainModel)
                box.setRootModelIndex(QtCore.QModelIndex())
                box.setCurrentIndex(-1)
            box.activated.connect(functools.partial(self._handleCurentIndexChange, idx))

            columnLabel = QtGui.QLabel(name)
            self._columnTitleBoxes.append(columnLabel)
            layout.addWidget(columnLabel, 0, idx)
            layout.addWidget(box, 1, idx)

        self.setLayout(layout)

    def model(self):
        """
        Virtual Method

        Must Be ReImplemented

        Return the model in the inherited sub-class
        """
        raise NotImplementedError()

    def showColumnTitles(self, state):
        for label in self._columnTitleBoxes:
            label.setVisible(state)

    def _boxDef(self):
        """
        Internal Method

        Virtual Method

        Must Be ReImplemented

        Return a list of tuples for the box name, and the model row index it represents

        example:
            return [
                    ("Location:", 0),
                    ("Stage:", 1),
                    ]
        """
        raise NotImplementedError()

    def _handleCurentIndexChange(self, boxIdx, idx):
        """
        Internal Method

        Handle a combo box change, update the next combo box and set the model and index
        """
        self._lastEdited = boxIdx
        self.itemSelected(boxIdx)
        currentBox = self.boxes[boxIdx]
        if boxIdx == len(self.boxes) - 1:
            self.childItemSelected()
            return

        for boxNum in xrange(len(self.boxes)):
            if boxNum > boxIdx:
                self.boxes[boxNum].setModel(self._emptyModel)

        # build up the indexs
        idx = QtCore.QModelIndex()
        for boxNum in xrange(len(self.boxes)):
            if boxNum >= boxIdx + 1:
                break
            idx = self._mainModel.index(self.boxes[boxNum].currentIndex(), 0, idx)
        
        if currentBox.model().canFetchMore(idx) is True:
            currentBox.model().fetchMore(idx)

        childBox = self.boxes[boxIdx + 1]
        childBox.setModel(currentBox.model())
        childBox.setRootModelIndex(idx)
        childBox.setCurrentIndex(-1)
        childBox.setDuplicatesEnabled(True)

    def _getDeepestEditedBox(self):
        """
        Internal Method

        Get the deepest edited box
        """
        return self.boxes[self._lastEdited]

    def itemSelected(self, boxIndex):
        """
        Method for when any box is selected
        """
        pass

    def childItemSelected(self):
        """
        Method for when the bottom most box is selected
        """
        pass

    def hideBox(self, index):
        """
        hide a given box, and its title, given its index

        args:
            index (int): The index to hide the box and its label
        """
        self._columnTitleBoxes[index].setVisible(False)
        self.boxes[index].setVisible(False)

    def showBox(self, index):
        """
        show a given box, and its title, given its index

        args:
            index (int): The index to show the box and its label
        """
        self._columnTitleBoxes[index].setVisible(True)
        self.boxes[index].setVisible(True)

    def _setParentContext(self, index):
        """
        Internal Method

        Set parent context,helpful to set the parents context allowing for a bottom up setting approach

        args:
            index (QModelIndex): Index to set
        """
        if not index.isValid():
            return 0

        col = self._setParentContext(index.parent())
        self.boxes[col].setCurrentIndex(index.row())

        self._handleCurentIndexChange(col, None)
        self._handleCurentIndexChange(col, None)
        return col + 1
