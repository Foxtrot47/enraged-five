"""
Module with all custom exceptions used in the AnimData sub-package.
"""
class WatchstarPermissionsError(Exception):
    """
    Raised when the user does not have adequate Active Directory permissions.
    """
    pass


class CaptureDoesNotExistError(Exception):
    """
    Raised when the capture id used in a query does not exist in Watchstar.
    """
    pass
