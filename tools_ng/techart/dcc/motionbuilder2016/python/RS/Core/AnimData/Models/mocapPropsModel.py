from PySide import QtGui, QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem
from RS.Core.AnimData import Context


class MocapPropModelItem(baseModelItem.BaseModelItem):
    """
    This represents a single MocapProp
    """
    def __init__(self, mocapProp, parent=None):
        """
        Constructor
        """
        super(MocapPropModelItem, self).__init__(parent=parent)
        self._mocapProp = mocapProp

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._mocapProp.name

        elif role == QtCore.Qt.UserRole:
            return self._mocapProp

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class MocapPropsModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, project, parent=None):
        self._project = project
        super(MocapPropsModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Name"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        for mocapProp in context.animData.getMocapProps(self._project):
            parent.appendChild(MocapPropModelItem(mocapProp, parent=parent))

    def findContextIndex(self, con):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.Context): The context to find

        returns:
            QModelIndex of the location
        """
        if con is None:
            return QtCore.QModelIndex()

        idxs = self.match(self.createIndex(0, 0), 0, con.name, 1, QtCore.Qt.MatchExactly)
        if len(idxs) == 0:
            return QtCore.QModelIndex()
        idxs = idxs[0]

        return idxs


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)
    proj = Context.animData.getProjectByName("Redemption2")
    mainModel = MocapPropsModel(proj)
#     win = QtGui.QComboBox()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()

    sys.exit(app.exec_())
