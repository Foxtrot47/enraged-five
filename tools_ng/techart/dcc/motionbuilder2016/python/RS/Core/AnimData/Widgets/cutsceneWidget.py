from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData.Models import cutsceneModel, proxyFilterModels
from RS.Core.AnimData._internal import contexts
from RS.Core.AnimData.Views import abstractComboView, columnFilterView
from RS.Core.AnimData.Widgets import contextWidget


class CutsceneTreeView(QtGui.QTreeView, contextWidget.AbstractContextWidgets):
    """
    Context Tree View for the Cutscene Model
    """
    def __init__(self, parent=None):
        super(CutsceneTreeView, self).__init__(parent=parent)

        # Model stuff
        self._mainModel = cutsceneModel.CutscenesModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(self._mainModel)
        self.setModel(self._proxyModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None, completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes, selectesOnly=selectesOnly, sortAssending=sortAssending, completeSessions=completeSessions, filterPipeline=filterPipeline)

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findContextIndex(context)

        # Set Selection
        selection = self.selectionModel()
        selection.select(contextIdx, QtGui.QItemSelectionModel.Select)
        self.setSelectionModel(selection)

        # Expand to selection
        self.expandChildren(contextIdx)

    def expandChildren(self, index):
        """
        Method to expand the view to the given index

        args:
            index (QModelIndex): Model index to expand to
        """
        if not index.isValid():
            return
        self.expandChildren(index.parent())

        self.setExpanded(index, True)


class CutsceneColumnView(columnFilterView.ColumModelView, contextWidget.AbstractContextWidgets):
    """
    Context Column View for the Cutscene Model
    """

    def __init__(self, parent=None):
        super(CutsceneColumnView, self).__init__(parent=parent)

        # Model stuff
        self._mainModel = cutsceneModel.CutscenesModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(self._mainModel)
        self.setModel(self._proxyModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

        self.setContext(Context.animData.getCurrentProject())

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None, completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes, selectesOnly=selectesOnly, sortAssending=sortAssending, completeSessions=completeSessions, filterPipeline=filterPipeline)

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        if context is None:
                return
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)

    def expandChildren(self, index):
        """
        Method to expand the view to the given index

        args:
            index (QModelIndex): Model index to expand to
        """
        if not index.isValid():
            return
        self.expandChildren(index.parent())

        self.setExpanded(index, True)

    def getSelectedProject(self):
        projs = self.getSelectedColumnsIndexes()[0]
        if len(projs) > 0:
            proj = projs[0].data(QtCore.Qt.UserRole)
            if isinstance(proj, contexts.Project):
                return proj
        return None

    def projectColumnVisibility(self, state):
        self.setColumnVisibility(0, state)


class CutsceneComboxBoxView(abstractComboView.ContextComboxBoxView, contextWidget.AbstractContextWidgets):
    """
    Context Combo Box for the Cutscene Model

    has two signals:
        contextSelected (Context): The currently selected context
        trialSelected (_trialBase): The currently selected Trial context
    """
    def model(self):
        """
        Virtual Method
        """
        self._mainModel = cutsceneModel.CutscenesModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(self._mainModel)
        return self._proxyModel

    def _boxDef(self):
        """
        Internal Method

        Virtual Method
        """
        return [
               ("Project:", 0),
               ("Cutscene:", 1),
               ("Trial:", 2),
               ]

    def _handleContextClick(self, index):
        """
        Re-implemented
        """
        pass

    def itemSelected(self, boxIndex):
        """
        Method for when any box is selected
        """
        self.contextSelected.emit(self.boxes[boxIndex].itemData(self.boxes[boxIndex].currentIndex(), QtCore.Qt.UserRole))

    def childItemSelected(self):
        """
        Method for when the bottom most box is selected
        """
        self.trialSelected.emit(self.boxes[-1].itemData(self.boxes[-1].currentIndex(), QtCore.Qt.UserRole))

    def getCurrentContext(self):
        return self.getSelectedContexts()[0]

    def getCurrentTrial(self):
        trials = self.getSelectedTrials()
        if len(trials) == 1:
            return trials[0]
        return None

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        return:
            a list of context objects which are selected
        """
        contextBox = self._getDeepestEditedBox()
        return [contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)]

    def getSelectedTrials(self):
        """
        get all the currently selected trial contexts

        return:
            a list of all the selected trial contexts
        """
        contextBox = self._getDeepestEditedBox()
        val = contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)
        if isinstance(val, contexts._trialBase):
            return [val]
        return []

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None, completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes, selectesOnly=selectesOnly, sortAssending=sortAssending, completeSessions=completeSessions, filterPipeline=filterPipeline)

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)

    def expandChildren(self, index):
        """
        Method to expand the view to the given index

        args:
            index (QModelIndex): Model index to expand to
        """
        if not index.isValid():
            return
        self.expandChildren(index.parent())

        self.setExpanded(index, True)


if __name__ == "__main__":
    def _handleSelect(item):
        print item

    def _handleButton(*args, **kwargs):
        print win.getSelectedContexts()

    def _handleContextSelect(item):
        print item

    import sys
    app = QtGui.QApplication(sys.argv)
    wid = QtGui.QWidget()
    lay = QtGui.QVBoxLayout()
#     win = CutsceneTreeView()
    win = CutsceneColumnView()
#    win = CutsceneComboxBoxView()
    but = QtGui.QPushButton("Test")
    lay.addWidget(win)
    lay.addWidget(but)
    wid.setLayout(lay)
    but.pressed.connect(_handleButton)
    win.trialSelected.connect(_handleSelect)

    wid.show()
    sys.exit(app.exec_())
