from PySide import QtCore, QtGui

from RS.Core.AnimData.Widgets import trialNotesWidget


class NotesScroller(QtGui.QWidget):

    noteAdded = QtCore.Signal()
    noteUpdated = QtCore.Signal()
    noteRemoved = QtCore.Signal()

    def __init__(self, context=None, parent=None):
        super(NotesScroller, self).__init__(parent=parent)
        self._notesDict = {}
        self.setupUi()
        self.setContext(context)

    def setupUi(self):
        """
        Setup the Note
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()

        # Create Widgets
        scroll = QtGui.QScrollArea()
        scrollView = QtGui.QWidget()
        self._scrollLayout = QtGui.QVBoxLayout(scrollView)
        self._contextPathLabel = QtGui.QLabel("CONTEXT NAME GOES HERE!")
        self._addNewNoteButton = QtGui.QPushButton("Add New Note")

        # Configure Widgets
        scroll.setWidget(scrollView)
        scroll.setWidgetResizable(True)
        scroll.setFrameShape(QtGui.QFrame.NoFrame)

        # Assign Widgets to Layouts
        mainLayout.addWidget(self._contextPathLabel)
        mainLayout.addWidget(scroll)
        mainLayout.addWidget(self._addNewNoteButton)
        self._contextPathLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

        # Assign Layouts to Widgets
        self.setLayout(mainLayout)

        # Set Connections
        self._addNewNoteButton.pressed.connect(self._handleNewNote)

    def noteCount(self):
        return len(self._notesDict.values())

    def _handleNewNote(self):
        self._newNote = trialNotesWidget.NewNoteWidget(self._context)
        self._newNote.newNoteAdded.connect(self.checkForUpdates)
        self._newNote.setWindowModality(QtCore.Qt.ApplicationModal)
        self._newNote.show()

    def setContext(self, newContext):
        self._context = newContext
        self._clearAllNotes()

        if self._context is None:
            self._contextPathLabel.setText("No Trial Given")
            self._addNewNoteButton.setEnabled(False)
            return

        self._addNewNoteButton.setEnabled(True)
        self._contextPathLabel.setText("Notes for '{0}'".format(self._context.name))
        self._populateNotes()

    def checkForUpdates(self):
        if self._context is None:
            return
        self._populateNotes()

    def _populateNotes(self):
        if self._context is None:
            return
        notesToBeRemoved = []
        allNotes = self._context.getTrialNotes(True)
        for note in self._notesDict.iterkeys():
            if note not in allNotes:
                notesToBeRemoved.append(note)

        for note in notesToBeRemoved:
            widget = self._notesDict.pop(note)
            self._scrollLayout.removeWidget(widget)
            self.noteRemoved.emit()

        for note in allNotes:
            if not self._notesDict.has_key(note):
                
                newNoteWidget = trialNotesWidget.NoteWidget(note, parent=self)
                newNoteWidget.noteDeleted.connect(self._handleNoteDeleted)
                newNoteWidget.show()
                self._scrollLayout.insertWidget(0, newNoteWidget)
                self._notesDict[note] = newNoteWidget
                
                self.noteAdded.emit()
            else:
                noteWidget = self._notesDict[note]
                if not noteWidget.isEditing():
                    noteWidget.updateNote(note)
                    self.noteUpdated.emit()

    def _clearAllNotes(self):
        for key, value in self._notesDict.iteritems():
            self._scrollLayout.removeWidget(value)
            value.deleteLater()
        self._notesDict = {}

    def _handleNoteDeleted(self, noteWidget):
        for key, value in self._notesDict.iteritems():
            if value == noteWidget:
                self._scrollLayout.removeWidget(noteWidget)
                noteWidget.deleteLater()
                self._notesDict.pop(key)
                return


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    from RS.Core.AnimData import Context

    trial = Context.animData.getTrialByID(38227)
    wid = NotesScroller(trial)
    # wid = NoteWidget(notes[0])
    wid.show()
    sys.exit(app.exec_())
