"""
Motion Trials Selects Model
 - Seemed easier than trying to hack it with QTableWidgetItem's
"""
from PySide import QtGui, QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem
from RS.Core.AnimData import Const


class SelectsModelItem(baseModelItem.BaseModelItem):
    """
    This represents a single select
    """
    def __init__(self, select, parent=None):
        """
        Constructor
        """
        super(SelectsModelItem, self).__init__(parent=parent)
        self._select = select
        self._selectNote = (self._select.notes() or "").replace("\r", "    ")

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._select.actionNumber()
            elif column == 1:
                return self._select.hasMocap()
            elif column == 2:
                return self._select.hasAudio()
            elif column == 3:
                return self._select.hasVideo()
            elif column == 4:
                return self._select.timecode()
            elif column == 5:
                return self._selectNote

        elif role == QtCore.Qt.UserRole:
            return self._select

        elif role == QtCore.Qt.BackgroundColorRole:
            if column == 1:
                if self._select.hasMocap() is True:
                    return const.ModelColours.SELECT
                else:
                    return const.ModelColours.NONSELECT
            elif column == 2:
                if self._select.hasAudio() is True:
                    return const.ModelColours.SELECT
                else:
                    return const.ModelColours.NONSELECT
            elif column == 3:
                if self._select.hasVideo() is True:
                    return const.ModelColours.SELECT
                else:
                    return const.ModelColours.NONSELECT

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class SelectsModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, trial=None, parent=None):
        self._trial = trial
        super(SelectsModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Action Number", "Mocap", "Audio", "Video", "Timecode", "Notes"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def setTrialContext(self, trial):
        self._trial = trial
        self.reset()

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._trial is None:
            return

        for select in self._trial.getSelects():
            parent.appendChild(SelectsModelItem(select, parent=parent))


if __name__ == "__main__":
    import sys
    from RS.Core.AnimData import Context
    app = QtGui.QApplication(sys.argv)

    take = Context.animData.getTrialByID(125504)
    print take.name

    mainModel = SelectsModel(take)
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
    sys.exit(app.exec_())
