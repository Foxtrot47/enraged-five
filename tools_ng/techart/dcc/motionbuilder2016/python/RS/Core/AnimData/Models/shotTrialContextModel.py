"""
Models for dealing with shot contexts
"""
from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData._internal import contexts as animContexts
from RS.Core.AnimData.Models import baseContext
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, lazyLoadModelItem


class ContextProjectShotItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return [ContextShootsItem(shoot, self, rootItem=self._rootItem) for shoot in self._context.getAllShoots()]


class ContextShootsItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return [ContextShotsItem(day, self, rootItem=self._rootItem) for day in self._context.getAllShots()]


class ContextShotsItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return [baseContext.BaseTrialsItem(trial, self, rootItem=self._rootItem) for trial in self._context.getAllTrials()]


class AnimDataShotTrialModel(baseModel.BaseModel):
    """
    Base Shot Model Type
    """

    def __init__(self, initialProject=None, parent=None):
        self._initialProject = initialProject
        super(AnimDataShotTrialModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Context"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int number of columns in the model
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._initialProject is None:
            for prj in Context.animData.getAllProjects():
                parent.appendChild(ContextProjectShotItem(prj, parent=parent, rootItem=self))
        else:
            parent.appendChild(ContextProjectShotItem(self._initialProject, parent=parent, rootItem=self))

    def findContextIndex(self, context):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.Context): The context to find

        returns:
            QModelIndex of the location
        """
        if context is None:
            return QtCore.QModelIndex()

        sameType = False
        # Project
        if isinstance(context, animContexts.Project):
            projName = context.name
            sameType = True
        else:
            projName = context.project().name

        projIdx = self.match(self.createIndex(0, 0), 0, projName, 1, QtCore.Qt.MatchExactly)
        if len(projIdx) == 0:
            return QtCore.QModelIndex()
        projIdx = projIdx[0]
        if sameType:
            return projIdx
        proj = self.data(projIdx, QtCore.Qt.UserRole + 1)

        # Shoot
        if isinstance(context, animContexts.Shoots):
            con = context
            sameType = True
        else:
            con = context.shoot()
        shootIdx = proj.findContextIndex(con, projIdx)
        if shootIdx is None:
            return QtCore.QModelIndex()

        if sameType:
            return shootIdx
        shoot = self.data(shootIdx, QtCore.Qt.UserRole + 1)

        # Shot
        shotIdx = shoot.findContextIndex(context, shootIdx)
        if shotIdx is None:
            return QtCore.QModelIndex()

        return shotIdx


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)
    mainModel = AnimDataShotTrialModel()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
    sys.exit(app.exec_())
