"""
Module for all the different context types, how they all fit together

Warnings:
    This module has been ported from the TechArt depot. Make changes there first and then integrate them here.
"""
import json

from PySide import QtCore

from RS.Tools.CameraToolBox.PyCore.Metaclasses import singleton
from RS.Core.AnimData._internal import contexts
# Import/monkey patch names to match those in the Techart depot; this simplifies porting updates from there.
from RS.Core.AnimData import Const as const
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize as memoize
memoize.memoize = memoize.memoized


class _locationBase(contexts._dataDictContextBase):
    """
    Base Class for all Location Context
    """
    def __init__(self, services, dataDict):
        """
        Constructor

        Args:
            services (animData.services.Services): The services for communicating with databases
        """
        super(_locationBase, self).__init__(services, dataDict)

        self._additionalData = {}

        self._getAdditionalData()

    def _getAdditionalData(self):
        addData = self._dataDict.get("additionalData")
        if addData not in ["", None]:
            try:
                self._additionalData = json.loads(addData)
            except ValueError as error:
                pass  # handling for invalid json str.

    def additionalData(self):
        return self._additionalData

    def setAdditionalData(self, key, value):
        raise NotImplementedError()

    def removeAdditionalData(self, key):
        """
        Remove an additional Data key and its value.

        Args:
            key (str): the name of the value to remove
        """
        raise NotImplementedError()

    def _handleStageCreation(self, stagesFromWatchstar, location):
        """
        Handles creating the Stages from the returned watchstar data

        Args:
            stagesFromWatchstar (list[dict]): The json dict from the watchstar database
            location (stages.Location): The parent location which the stage lives under

        Returns:
            list[stages.Stage]: list of stages (Stages) objects
        """
        return [Stage(location, self.getServices(), data) for data in stagesFromWatchstar]

    def __hash__(self):
        """
        Hash the object
        """
        return self.id.__hash__()


class Location(_locationBase):
    """
    Context object for a Location
    """

    def __init__(self, services, dataDict):
        """
        Constructor

        Args:
            services (animData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(Location, self).__init__(services, dataDict)
        self._locationName = dataDict.get("displayName")

    @property
    def locationID(self):
        """
        get the code ID of the location

        Returns:
            str: location code ID
        """
        return self.id

    @property
    def name(self):
        """
        get the name of the location

        Returns:
            str: location name
        """
        return self._locationName

    def city(self):
        """
        Get the name of the city that the location is in

        Returns:
            str: City Name
        """
        return self._dataDict.get("city")

    def offsite(self):
        """
        Get if the location is offsite or not

        Returns:
            bool: True if the location is offsite
        """

        return self._dataDict.get("isOffsite")

    def locationAbbreviation(self):
        """
        Get the location abbreviation

        Returns:
            str: location abbreviation
        """
        return self._dataDict.get("abbrev")

    def hostName(self):
        """
        Get the hostname of the location

        Returns:
            str: value for the hostname
        """
        return self._dataDict.get("hostName", "")

    def locationActive(self):
        """
        Get if the location is active or not

        Returns:
            bool: True if the location is still active
        """
        return self._dataDict.get("isActive", False)

    def previsFolderPath(self):
        """
        Get the perforce folder path for the locations previz folder location

        Returns:
            str: p4 path name
        """
        return self._dataDict.get("previzFolderPath", "")

    @memoize.memoize
    def getStages(self):
        """
        Get all the stages at this location

        Returns:
            list of Stage
        """
        data = self.getServices().watchstar.executeCommand("tools.LocationStageSearchByLocation",
                                                           [
                                                               self.getServices().getUserName(),
                                                               self.locationID,
                                                           ]
                                                           )
        if data is None:
            return []
        return self._handleStageCreation(data.get("stages"), self)

    def updateLocation(self):
        """
        Update the location's data in the database.
        """
        kwargs = ["@AdditionalData='{0}'".format(json.dumps(self._additionalData))]
        data = self._services.watchstar.executeCommand("toolswritepub.UpdateLocation",
                                                       [
                                                           self._services.getUserName(),
                                                           self.id,
                                                       ],
                                                       kwargList=kwargs,
                                                       )
        if data is None:
            return []
        location = [Location(self.getServices(), locData) for locData in data.get("locations")][0]
        self._dataDict = location._dataDict

    def setAdditionalData(self, key, value):
        """
        Set an additional data key and its value

        Notes:
            Both the key and value must be pickleable.

        Args:
            key (str): the name of the value to set
            value (object): The object to set
        """
        self._additionalData[key] = value
        self.updateLocation()

    def removeAdditionalData(self, key):
        """
        Remove an additional data key and its value.

        Args:
            key (str): the name of the value to remove
        """
        self._additionalData.pop(key)
        self.updateLocation()

    def getCalibrations(self, date=None):
        """
        Get calibrations for the location.

        Keyword Args:
            date (datetime.datetime): The date of the calibration.

        Returns:
            list[contexts.StageCalibration]
        """
        kwargs = ["@FacilityLocationID='{0}'".format(self.id)]
        if date:
            kwargs.append("@StageCalibrationDate='{0}'".format(date.strftime(const.DatetimeFormatters.DATE)))
        data = self._services.watchstar.executeCommand("tools.StageCalibrationSearchByLocationStage",
                                                       [
                                                           self._services.getUserName(),
                                                       ],
                                                       kwargList=kwargs,
                                                       )
        if data is None:
            return []
        return self._handleStageCalibrationCreation(data)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for '{}'".format(self.__class__.__name__, self._locationName)


class Stage(_locationBase):
    """
    Context object for a stage
    """

    def __init__(self, location, services, dataDict):
        """
        Constructor

        Args:
            location (AnimData.contexts.Location): The location this stage is in
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(Stage, self).__init__(services, dataDict)

        self._stageName = dataDict.get("name")
        self._locationName = location.name
        self._location = location

    @property
    def stageId(self):
        """
        get the code ID of the location

        Returns:
            str: location code ID
        """
        return self.id

    @property
    def name(self):
        """
        get the name of the stage

        Returns:
            str: stage name
        """
        return self._stageName

    def location(self):
        """
        Get the location for this stage

        Returns:
            animData.contexts.Location: the location for the stage
        """
        return self._location

    def previsFolderPath(self):
        """
        Get the perforce folder path for the locations previz folder location

        Returns:
            str: p4 path name
        """
        return self._location.previsFolderPath()

    def stageActive(self):
        """
        Get if the stage is active or not

        Returns:
            bool: True if the stage is still active
        """
        return self._dataDict.get("isActive", False)

    def captureWorkstationIp(self):
        """
        Get the Capture Machine IP for the current stage

        Returns:
            str
        """
        return self._dataDict.get("captureWorkstationIP", "000.000.000.000")

    def updateStage(self):
        """
        Update the stage's data in the database.
        """
        kwargs = ["@AdditionalData='{0}'".format(json.dumps(self._additionalData))]
        data = self._services.watchstar.executeCommand("toolswritepub.UpdateLocationStage",
                                                       [
                                                           self._services.getUserName(),
                                                           self.id,
                                                       ],
                                                       kwargList=kwargs,
                                                       )
        if data is None:
            return []
        stage = self._handleStageCreation(data.get("stages"), self)[0]
        self._dataDict = stage._dataDict

    def setAdditionalData(self, key, value):
        """
        Set an additional data key and its value

        Args:
            key (str): the name of the value to set
            value (object): The object to set

        Notes:
            The value must be pickleable
        """
        self._additionalData[key] = value
        self.updateStage()

    def removeAdditionalData(self, key):
        """
        Remove an additional data key and its value.

        Args:
            key (str): the name of the value to remove
        """
        self._additionalData.pop(key)
        self.updateStage()

    def addCalibration(self, name, date, notes=None):
        """
        Adds a new calibration for the stage.

        Args:
            name (str): Name of the calibration.
            date (datetime.datetime): The date of the calibration in UTC.

        Keyword Args:
            notes (str): Notes for the calibration.

        Returns:
            contexts.StageCalibration: The new calibration object.
        """
        dateName = date.strftime(const.DatetimeFormatters.TIMESTAMP_MINS)
        kwargs = [
            "@StageCalibrationName='{0}'".format(name),
            "@StageCalibrationDate='{0}'".format(dateName),
        ]
        if notes is not None:
            cleanTxt = self._formatText(notes)
            kwargs.append("@Notes='{0}'".format(cleanTxt))

        data = self._services.watchstar.executeCommand("toolswritepub.AddStageCalibration",
                                                       [
                                                           self._services.getUserName(),
                                                           self.id,
                                                       ],
                                                       kwargList=kwargs,
                                                       )
        if data is None:
            return []

        return self._handleStageCalibrationCreation(data)[0]

    def getCalibrations(self, date=None):
        """
        Get calibrations for the stage.

        Keyword Args:
            date (datetime.datetime): The date of the calibration.

        Returns:
            list[contexts.StageCalibration]
        """
        kwargs = ["@LocationStageID='{0}'".format(self.id)]
        if date:
            kwargs.append("@StageCalibrationDate='{0}'".format(date.strftime(const.DatetimeFormatters.DATE)))
        data = self._services.watchstar.executeCommand("tools.StageCalibrationSearchByLocationStage",
                                                       [
                                                           self._services.getUserName(),
                                                       ],
                                                       kwargList=kwargs,
                                                       )
        if data is None:
            return []
        return self._handleStageCalibrationCreation(data)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for '{}' at '{}'".format(self.__class__.__name__, self._stageName, self._locationName)


class LocalLocation(Location):
    _LOCAL_STAGE_ID = -91

    __metaclass__ = singleton.Singleton

    def __init__(self, services):
        """
        Context object for a local Location
        """
        self._settings = QtCore.QSettings("RockstarSettings", "AnimData")
        dataDict = {
            "id": -90,
            "abbrev": "Lcl",
            "city": "everywhere",
            "displayName": "Local Location",
            "hostName": "",
            "isActive": False,
            "isOffsite": False,
            "previzFolderPath": "",
        }
        super(LocalLocation, self).__init__(services, dataDict)

        self._localStage = self._getLocalStage()
        self._storedStages = self._getLocalStoredStages()

    def _getLocalStage(self):
        """
        Get the local hostname as a stage

        Returns:
            stages.Stage: The stage with the local IP of the host
        """
        localIp = "192.168.0.0"  # Config.User has no equivalent to config.user.system.ipaddress
        dataDict = {
            "id": self._LOCAL_STAGE_ID,
            "captureWorkstationIP": localIp,
            "name": "Local Stage ({})".format(localIp),
        }
        return Stage(self, self.getServices(), dataDict)

    def _readStoredStages(self):
        data = self._settings.value("CustomLocationStages", []) or []
        if not isinstance(data, list):
            data = [data]
        return data

    def _saveStoredStages(self):
        ips = [stg.captureWorkstationIp() for stg in self._storedStages]
        self._settings.setValue("CustomLocationStages", ips)

    def _clearStoredStages(self):
        self._settings.setValue("CustomLocationStages", [])

    def _getLocalStoredStages(self):
        """
        get all the custom stages at this location

        Returns:
            list[stages.Stage]: list of the custom stages (Stage) under the location
        """
        allStoredStages = []
        for idx, ip in enumerate(self._readStoredStages()):
            dataDict = {
                "id": self._LOCAL_STAGE_ID - idx,
                "captureWorkstationIP": ip,
                "name": "Local Stage ({})".format(ip),
            }
            allStoredStages.append(Stage(self, self.getServices(), dataDict))
        return allStoredStages

    def addCustomStage(self, ipAddress):
        """
        Add a new stage to the local location
        """
        currentIdx = min([stg.stageId for stg in self._storedStages] or [self._LOCAL_STAGE_ID])
        dataDict = {
            "id": currentIdx - 1,
            "captureWorkstationIP": ipAddress,
            "name": "Local Stage ({})".format(ipAddress),
        }
        self._storedStages.append(Stage(self, self.getServices(), dataDict))
        self._saveStoredStages()

    def clearCustomStages(self):
        """
        Clear all the custom stages
        """
        self._clearStoredStages()

    def getStages(self):
        """
        Get all the stages at this location

        Returns:
            list[Stage]: list of stages (Stage) under the location
        """
        return [self._localStage] + self._storedStages
