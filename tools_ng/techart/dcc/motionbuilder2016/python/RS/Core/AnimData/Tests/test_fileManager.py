"""
Unittests for utils.fileManager

Warnings:
    This module has been ported from the TechArt depot. Make changes there first and then integrate them here.
"""
import unittest

# Import/monkey patch names to match those in the Techart depot; this simplifies porting updates from there.
from RS.Core.AnimData import Context as context
from RS.Core.AnimData import fileManager


class TestFileManager(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Use setUpClass over setUp to reduce the number of calls to Watchstar and increase performance.
        cls.motionTrialWithPpf = context.animData.getTrialByID(131435)  # mocap development/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1
        cls.motionTrialWithoutPpf = context.animData.getTrialByID(131442)  # mocap development/000797_01_SD_CB3test_310519
        cls.romWithTrialPpf = context.animData.getTrialByID(131086)  # mocap development/smcla_190522am.scott_mclaughlin.rom
        cls.romWithoutTrialPpf = context.animData.getTrialByID(131418)  # mocap development/jharr_190530pm2.justin_harrison.rom
        cls.project = context.animData.getProjectByName("Mocap Development")
        cls.mocapPropVersion = cls.project.getMocapPropVersionByID(947)  # mocap development/GizmoRed_PR001_V01
        cls.mocapPropVersion2 = cls.project.getMocapPropVersionByID(1677)  # mocap development/SlateActionClock_PR004_V01
        cls.gameCharacter = cls.project.getAsset3dByID(13148)  # mocap development/Male_Skeleton.fbx

    def test_getPrjForTrial(self):
        expectedResult = "//depot/projects/development/capture/takes/2019_0531_Session0551/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1.prj"
        self.assertEqual(expectedResult, fileManager.getPrjForTrial(self.motionTrialWithPpf))

    def test_getPrjForMocapPropVersion(self):
        expectedResult = "//depot/projects/development/capture/props/GizmoRed/PR001/GizmoRed_PR001_V01.prj"
        self.assertEqual(expectedResult, fileManager.getPrjForMocapPropVersion(self.mocapPropVersion))

    def test_getAnmFileForMocapPropVersion(self):
        expectedResult = "//depot/projects/development/anim/target/props/GizmoRed.anm"
        self.assertEqual(expectedResult, fileManager.getAnmFileForMocapPropVersion(self.mocapPropVersion))

    def test_getCmFileForMocapPropVersion(self):
        expectedResult = "//depot/projects/development/charmap/props/GizmoRed.cm"
        self.assertEqual(expectedResult, fileManager.getCmFileForMocapPropVersion(self.mocapPropVersion))

    def test_getSubjectsFolderForMocapPropVersion(self):
        expectedResult = "//depot/projects/development/subject/GizmoRed_PR001_V01"
        self.assertEqual(expectedResult, fileManager.getSubjectsFolderForMocapPropVersion(self.mocapPropVersion))

    def test_getAnmForTrial(self):
        expectedResult = "//depot/projects/development/capture/takes/2019_0531_Session0551/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1.rsrefine.anm"
        self.assertEqual(expectedResult, fileManager.getAnmForTrial(self.motionTrialWithPpf))

    def test_getMasterAnmForTrial(self):
        expectedResult = "//depot/projects/development/capture/takes/2019_0531_Session0551/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1/charmapped/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1_Master.anm"
        self.assertEqual(expectedResult, fileManager.getMasterAnmForTrial(self.motionTrialWithPpf))

    def test_getTalentPrjsForTrial(self):
        expectedResult = ['//depot/projects/development/capture/talent/2019_0531_Session0552/ddumm_190531pm.donny_dummy.rom.prj']
        self.assertEqual(expectedResult, fileManager.getTalentPrjsForTrial(self.motionTrialWithPpf))

    def test_getDataFolderForTrial(self):
        expectedResult = "//depot/projects/development/capture/takes/2019_0531_Session0551/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1"
        self.assertEqual(expectedResult, fileManager.getDataFolderForTrial(self.motionTrialWithPpf))

    def test_getProblemSetupFileForTrial(self):
        expectedResult = "//depot/projects/development/capture/takes/2019_0531_Session0551/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1.psf"
        self.assertEqual(expectedResult, fileManager.getProblemSetupFileForTrial(self.motionTrialWithPpf))

    def test_getSubjectsFolderForTrial(self):
        expectedResult = "//depot/projects/development/subject/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1"
        self.assertEqual(expectedResult, fileManager.getSubjectsFolderForTrial(self.motionTrialWithPpf))

    def test_getSubjectsFolderForMocapPropVersion(self):
        expectedResult = "//depot/projects/development/subject/GizmoRed_PR001_V01"
        self.assertEqual(expectedResult, fileManager.getSubjectsFolderForMocapPropVersion(self.mocapPropVersion))

    def test__getPpfFileForNonMotionTrial(self):
        expectedResult = "//depot/projects/development/subject/smcla_190522am/smcla_190522am.ppf"
        self.assertEqual(expectedResult, fileManager._getPpfFileForNonMotionTrial(self.romWithTrialPpf))

        expectedResult = "//depot/projects/development/subject/jharr_190530pm2/jharr_190530pm2.ppf"
        self.assertEqual(expectedResult, fileManager._getPpfFileForNonMotionTrial(self.romWithoutTrialPpf))

    def test__getPpfFileForMotionTrial(self):
        expectedResult = "//depot/projects/development/capture/takes/2019_0531_Session0551/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1.ppf"
        self.assertEqual(expectedResult, fileManager._getPpfFileForMotionTrial(self.motionTrialWithPpf))

        expectedResult = "//depot/projects/development/capture/takes/2019_0531_Session0553/000797_01_SD_CB3test_310519/000797_01_SD_CB3test_310519.ppf"
        self.assertEqual(expectedResult, fileManager._getPpfFileForMotionTrial(self.motionTrialWithoutPpf))

    def test_getPpfFileForTrial(self):
        expectedResult = "//depot/projects/development/subject/smcla_190522am/smcla_190522am.ppf"
        self.assertEqual(expectedResult, fileManager.getPpfFileForTrial(self.romWithTrialPpf))

        expectedResult = "//depot/projects/development/subject/jharr_190530pm2/jharr_190530pm2.ppf"
        self.assertEqual(expectedResult, fileManager.getPpfFileForTrial(self.romWithoutTrialPpf))

        expectedResult = "//depot/projects/development/capture/takes/2019_0531_Session0551/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1/000795_02_BE_CAPTUREBOSS3_WEST_TEST_P1.ppf"
        self.assertEqual(expectedResult, fileManager.getPpfFileForTrial(self.motionTrialWithPpf))

        expectedResult = "//depot/projects/development/capture/takes/2019_0531_Session0553/000797_01_SD_CB3test_310519/000797_01_SD_CB3test_310519.ppf"
        self.assertEqual(expectedResult, fileManager.getPpfFileForTrial(self.motionTrialWithoutPpf))

    def test_getPpfFileForMocapPropVersion(self):
        # test on a prop version with multiple ppfs - one in the subjects folder and one in a capture folder.
        expectedResult = "//depot/projects/development/subject/SlateActionClock_PR004_V01/SlateActionClock_PR004_V01.ppf"
        self.assertEqual(expectedResult, fileManager.getPpfFileForMocapPropVersion(self.mocapPropVersion2))

    def test_generatePpfFilePathFromPrj(self):
        expectedResult = "//depot/projects/bob/subject/aahr_120612am/aahr_120612am.ppf"
        prjDepotPath = "//depot/projects/bob/capture/talent/2012_0612_Day083/aahr_120612am.abe_ahmed.scale.prj"
        self.assertEqual(expectedResult, fileManager.generatePpfFilePathFromPrj(prjDepotPath))

        prjLocalPath = "/media/giant_capture_data/bob/capture/talent/2012_0612_Day083/aahr_120612am.abe_ahmed.scale.prj"
        expectedResult = "/media/giant_capture_data/bob/subject/aahr_120612am/aahr_120612am.ppf"
        self.assertEqual(expectedResult, fileManager.generatePpfFilePathFromPrj(prjLocalPath))

    def test_generateBdfFilePathFromPrj(self):
        expectedResult = "//depot/projects/bob/subject/aahr_120612am/aahr_120612am.bdf"
        prjDepotPath = "//depot/projects/bob/capture/talent/2012_0612_Day083/aahr_120612am.abe_ahmed.scale.prj"
        self.assertEqual(expectedResult, fileManager.generateBdfFilePathFromPrj(prjDepotPath))

        prjLocalPath = "/media/giant_capture_data/bob/capture/talent/2012_0612_Day083/aahr_120612am.abe_ahmed.scale.prj"
        expectedResult = "/media/giant_capture_data/bob/subject/aahr_120612am/aahr_120612am.bdf"
        self.assertEqual(expectedResult, fileManager.generateBdfFilePathFromPrj(prjLocalPath))

        # make sure that other occurances of ppf wont accidentally get replaced.
        prjLocalPath = "/media/giant_capture_data/bob/capture/talent/2012_0612_Day083/ppf_120612am.abe_ahmed.ppf.prj"
        expectedResult = "/media/giant_capture_data/bob/subject/ppf_120612am/ppf_120612am.bdf"
        self.assertEqual(expectedResult, fileManager.generateBdfFilePathFromPrj(prjLocalPath))

    def test_getTargetAnimForGameAsset(self):
        expectedResult = "//depot/projects/development/anim/target/characters/Male_Skeleton_01_Skel_gs.anm"
        self.assertEqual(expectedResult, fileManager.getTargetAnmForGameAsset(self.gameCharacter))
