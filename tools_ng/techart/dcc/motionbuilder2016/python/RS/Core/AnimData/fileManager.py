"""
Utility methods for getting various file and folder depot paths from anim data contexts.

Make sure to update and run the unittests in rockstar/mocapBoss/tests/utils/test_fileManager.py when making changes.

Warnings:
    This module has been ported from the TechArt depot. Make changes there first and then integrate them here.
"""
import os
import re

# Import/monkey patch names to match those in the Techart depot; this simplifies porting updates from there.
from RS.Core.AnimData import Context as animContext
from RS.Core.AnimData._internal import contexts
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize as memoize
memoize.memoize = memoize.memoized


class _giantFileType(object):
    def __init__(self, friendlyName, fileExt):
        self._friendlyName = friendlyName
        self._fileExt = fileExt

    @property
    def friendlyName(self):
        return self._friendlyName

    @property
    def fileExt(self):
        return self._fileExt


class GiantFileTypes(object):
    ANM = _giantFileType("Animation File", ".anm")
    BMO = _giantFileType("Body Motion File", ".bmo")
    BDF = _giantFileType("Body Definition File", ".dbf")
    CAL = _giantFileType("Camera Calibration File", ".cal")
    CM = _giantFileType("Character Map File", ".cm")
    CPJ = _giantFileType("Charmapper Project File", ".cpj")
    DAT = _giantFileType("Data File", ".dat")
    DOF = _giantFileType("Degrees of Freedom File", ".dof")
    FBX = _giantFileType("Filmbox File", ".fbx")
    GID = _giantFileType("Glob ID File", ".gid")
    GLOBS = _giantFileType("Raw Glob File", ".globs")
    IDS = _giantFileType("Raw ID File", ".ids")
    IMU = _giantFileType("Inertial Measurement Unit", ".imu")
    LMT = _giantFileType("Body Limit File", ".lmt")
    MDL = _giantFileType("Model File", ".mdl")
    PDF = _giantFileType("Problem Definition File", ".pdf")
    PPF = _giantFileType("Pattern File", ".ppf")
    PRJ = _giantFileType("Project File", ".prj")
    PSF = _giantFileType("Problem Setup File", ".psf")
    PSP = _giantFileType("Problem Specification File", ".psp")
    RAW = _giantFileType("Raw Capture Data", ".raw")
    RNG = _giantFileType("Range File", ".rng")
    RTPARAMS = _giantFileType("Realtime Parameters File", ".rtparams")
    SCH = _giantFileType("Search Parameters File", ".sch")
    SDF = _giantFileType("Scaling Definition File", ".sdf")
    STR = _giantFileType("Strength File", ".str")


def getPrjForTrial(trial):
    """
    Get the depot path of the project (prj) for the given trial.

    Will generate it if its not in Watchstar.

    Examples:
        >>> trial = context.animData.getTrialByID(129774)
        >>> fileManager.getPrjForTrial(trial)
        '//depot/projects/development/capture/takes/2019_0315_Session0462/000634_08_BE_CAPTUREBOSSTEST.prj'

    Args:
        trial (AnimData.Trial): AnimData trial context to get the prj path for.

    Returns:
        str: depot path.
    """
    # Try getting it from the watchstar associated files
    for fileObj in trial.getAllFiles():
        if fileObj.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_PRJ:
            fileObjPath = fileObj.rawFilePath()
            if fileObjPath not in ("", None):
                return fileObjPath
    # Otherwise, just generate it
    trialName = trial.getFileName()
    sessionName = trial.session().getSessionFolderName()
    capType = "takes"
    if isinstance(trial, contexts.NonMotionTrialBase):
        capType = "talent"
    folderPath = "/".join([trial.project().getMocapDepotRootPath(), "capture", capType, sessionName])
    return "{0}/{1}.prj".format(folderPath, trialName)


def getPrjForMocapPropVersion(mocapPropVersion):
    """
    Get the depot path of the project (prj) for the given mocap prop version.

    Will generate it if its not in Watchstar.

    Examples:
        >>> project = context.animData.getProjectByName("Development")
        >>> mocapPropVersion = project.getMocapPropVersionByID(889)
        >>> getPrjForMocapPropVersion(mocapPropVersion)
        '//depot/projects/development/capture/props/PropToy02/PR001/PropToy02_PR001_V01.prj'

    Args:
        mocapPropVersion (contexts.MocapPropVersion): AnimData MocapPropVersion context to get the prj path for.

    Returns:
        str: depot path
    """
    # Try getting it from the Watchstar associated files
    for fileObj in mocapPropVersion.getAllFiles():
        if fileObj.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_PRJ:
            fileObjPath = fileObj.rawFilePath()
            if fileObjPath not in ("", None):
                return fileObjPath
    # Otherwise, just generate it
    propName = mocapPropVersion.prop().name
    propVarName = mocapPropVersion.propVariation().name
    folderPath = "/".join([mocapPropVersion.project().getMocapDepotRootPath(), "capture", "props", propName, propVarName])
    return "{0}/{1}.prj".format(folderPath, mocapPropVersion.name)


def getAnmFileForMocapPropVersion(mocapPropVersion):
    """
    Get the depot path of the target anim (anm) for the given mocap prop version.

    Will generate it if its not in Watchstar.

    Examples:
        >>> project = context.animData.getProjectByName("Development")
        >>> mocapPropVersion = project.getMocapPropVersionByID(947)
        >>> getAnmFileForMocapPropVersion(mocapPropVersion)
        '//depot/projects/development/anim/target/props/GizmoRed.anm'

    Args:
        mocapPropVersion (contexts.MocapPropVersion): AnimData MocapPropVersion context to get the anm path for.

    Returns:
        str: depot path.
    """
    # Try getting it from the Watchstar associated files
    prop = mocapPropVersion.prop()
    for fileObj in prop.getAllFiles():
        if fileObj.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_ANM:
            fileObjPath = fileObj.rawFilePath()
            if fileObjPath not in ("", None):
                return fileObjPath
    # Otherwise, just generate it
    folderPath = "/".join([mocapPropVersion.project().getMocapDepotRootPath(), "anim", "target", "props"])
    return "{0}/{1}.anm".format(folderPath, prop.name)


def getCmFileForMocapPropVersion(mocapPropVersion):
    """
    Get the depot path of the charmap (cm) for the given mocap prop version.

    Will generate it if its not in Watchstar.

    Examples:
        >>> project = context.animData.getProjectByName("Development")
        >>> mocapPropVersion = project.getMocapPropVersionByID(947)
        >>> getCmFileForMocapPropVersion(mocapPropVersion)
        '//depot/projects/development/charmap/props/GizmoRed.cm'

    Args:
        mocapPropVersion (contexts.MocapPropVersion): AnimData MocapPropVersion context to get the anm path for.

    Returns:
        str: depot path
    """
    # Try getting it from the Watchstar associated files
    prop = mocapPropVersion.prop()
    for fileObj in prop.getAllFiles():
        if fileObj.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_CHARACTERMAP:
            fileObjPath = fileObj.rawFilePath()
            if fileObjPath not in ("", None):
                return fileObjPath
    # Otherwise, just generate it
    folderPath = "/".join([mocapPropVersion.project().getMocapDepotRootPath(), "charmap", "props"])
    return "{0}/{1}.cm".format(folderPath, prop.name)


def getSclForTrial(trial):
    """
    Get the depot path of the scaling file (.scl) for the given trial.

    Will generate it if its not in Watchstar.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the anm path for.

    Returns:
        str: depot path
    """
    # Try getting it from the watchstar associated files
    for fileObj in trial.getAllFiles(pathType=animContext.settingsManager.consts.FilePathTypes.GIANT_SCL):
        fileObjPath = fileObj.rawFilePath()
        sclFileName = "{}.scl".format(trial.getFileName())
        if fileObjPath not in ("", None) and fileObjPath.endswith(sclFileName):
            return fileObjPath
    # Otherwise, just generate it
    trialName = trial.getFileName()
    sessionName = trial.session().getSessionFolderName()
    capType = "takes"
    if isinstance(trial, contexts.NonMotionTrialBase):
        capType = "talent"
    folderPath = "/".join([trial.project().getMocapDepotRootPath(), "capture", capType, sessionName, trialName])
    return "{0}/{1}.scl".format(folderPath, trialName)


def getMdlForTrial(trial):
    """
    Get the depot path of the model file (.mdl) for the given trial.

    Will generate it if its not in Watchstar.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the anm path for.

    Returns:
        str: depot path
    """
    # Try getting it from the watchstar associated files
    for fileObj in trial.getAllFiles(pathType=animContext.settingsManager.consts.FilePathTypes.GIANT_MDL):
        fileObjPath = fileObj.rawFilePath()
        mdlFileName = "{}.mdl".format(trial.getFileName())
        if fileObjPath not in ("", None) and fileObjPath.endswith(mdlFileName):  # Filter out .target.mdl
            return fileObjPath
    # Otherwise, just generate it
    trialName = trial.getFileName()
    sessionName = trial.session().getSessionFolderName()
    capType = "takes"
    if isinstance(trial, contexts.NonMotionTrialBase):
        capType = "talent"
    folderPath = "/".join([trial.project().getMocapDepotRootPath(), "capture", capType, sessionName, trialName])
    return "{0}/{1}.mdl".format(folderPath, trialName)


def getDofForTrial(trial):
    """
    Get the depot path of the graphic mapping file (.dof) for the given trial.

    Will generate it if its not in Watchstar.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the anm path for.

    Returns:
        str: depot path
    """
    # Try getting it from the watchstar associated files
    for fileObj in trial.getAllFiles(pathType=animContext.settingsManager.consts.FilePathTypes.GIANT_MDL):
        fileObjPath = fileObj.rawFilePath()
        dofFileName = "{}.dof".format(trial.getFileName())
        if fileObjPath not in ("", None) and fileObjPath.endswith(dofFileName):
            return fileObjPath
    # Otherwise, just generate it
    trialName = trial.getFileName()
    sessionName = trial.session().getSessionFolderName()
    capType = "takes"
    if isinstance(trial, contexts.NonMotionTrialBase):
        capType = "talent"
    folderPath = "/".join([trial.project().getMocapDepotRootPath(), "capture", capType, sessionName, trialName])
    return "{0}/{1}.dof".format(folderPath, trialName)


def getRsrefineFbxForTrial(trial):
    """
    Get the depot path of the refine fbx for the given trial.

    Will generate it if its not in Watchstar.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the anm path for.

    Returns:
        str: depot path
    """
    # Try getting it from the watchstar associated files
    for fileObj in trial.getAllFiles():
        if fileObj.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_REFINE_FBX:
            fileObjPath = fileObj.rawFilePath()
            if fileObjPath not in ("", None):
                return fileObjPath
    # Otherwise, just generate it
    trialName = trial.getFileName()
    sessionName = trial.session().getSessionFolderName()
    capType = "takes"
    if isinstance(trial, contexts.NonMotionTrialBase):
        capType = "talent"
    folderPath = "/".join([trial.project().getMocapDepotRootPath(), "capture", capType, sessionName])
    return "{0}/{1}.rsrefine.fbx".format(folderPath, trialName)


def getAnmForTrial(trial):
    """
    Get the depot path of the refine anim (anm) for the given trial.

    Will generate it if its not in Watchstar.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the anm path for.

    Returns:
        str: depot path
    """
    # Try getting it from the watchstar associated files
    for fileObj in trial.getAllFiles():
        if fileObj.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_REFINEDATA:
            fileObjPath = fileObj.rawFilePath()
            if fileObjPath not in ("", None):
                return fileObjPath
    # Otherwise, just generate it
    trialName = trial.getFileName()
    sessionName = trial.session().getSessionFolderName()
    capType = "takes"
    if isinstance(trial, contexts.NonMotionTrialBase):
        capType = "talent"
    folderPath = "/".join([trial.project().getMocapDepotRootPath(), "capture", capType, sessionName])
    return "{0}/{1}.rsrefine.anm".format(folderPath, trialName)


def getMasterAnmForTrial(trial):
    """
    Get the depot path of the master anim (anm) for the folder of the trial.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the master anm path for.

    Returns:
        str: depot path.
    """
    trialName = None
    # Try getting it from the watchstar associated files
    for fileObj in trial.getAllFiles():
        if fileObj.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_REFINEDATA:
            fileObjPath = fileObj.rawFilePath()
            if fileObjPath not in ("", None):
                trialName = os.path.basename(fileObjPath).rsplit(".", 2)[0]

    trialName = trialName or trial.getFileName()
    sessionName = trial.session().getSessionFolderName()
    capType = "takes"
    if isinstance(trial, contexts.NonMotionTrialBase):
        capType = "talent"
    folderPath = "/".join([trial.project().getMocapDepotRootPath(), "capture", capType, sessionName])
    return "{0}/{1}/charmapped/{1}_Master.anm".format(folderPath, trialName)


def getTalentPrjsForTrial(trial):
    """
    Get a list of the depot paths for the projects (prj) on all talent in the given trial.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the talent prj path for.

    Returns:
        list of str: the talent prj depot paths for the given context.
    """
    if isinstance(trial, contexts.NonMotionTrialBase):
        return []
    prjs = []
    for char in trial.getCharacters():
        rom = char.performerROM()  # This triggers a w* query - call it as few times as possible.
        if rom is not None:
            prj = getPrjForTrial(rom)
            prjs.append(prj)
    return prjs


def getDataFolderForTrial(trial):
    """
    Get the depot path of the folder for the given trial.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the folder path for.

    Returns:
        str: depot path.
    """
    prjPath = getPrjForTrial(trial)
    return os.path.splitext(prjPath)[0]


def getProblemSetupFileForTrial(trial):
    """
    Get the depot path of the problem setup file (psf) for the given trial.

    Will generate it if its not in Watchstar.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the PSF path for

    Returns:
        str: depot path.
    """
    # Try getting it from the watchstar associated files
    for fileObj in trial.getAllFiles():
        if fileObj.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_PSF:
            fileObjPath = fileObj.rawFilePath()
            if fileObjPath not in ("", None):
                return fileObjPath

    # Otherwise, just generate it
    trialName = trial.getFileName()
    sessionName = trial.session().getSessionFolderName()
    capType = "takes"
    if isinstance(trial, contexts.NonMotionTrialBase):
        capType = "talent"
    folderPath = "/".join([trial.project().getMocapDepotRootPath(), "capture", capType, sessionName])
    return "{0}/{1}/{1}.psf".format(folderPath, trialName)


@memoize.memoize
def getSubjectsFolderForTrial(trial):
    """
    Get the depot path of the trial's subject folder.

    Will generate it if its not in Watchstar.

    Notes:
        This will have the .ppf and .bdf files (sometimes also the .sdf).

    Args:
        trial (AnimData.Trial): AnimData trial context to get the subjects dir for.

    Returns:
        str: depot path.
    """
    # Example: //depot/projects/bob/subject/cmill_180108p
    subDir = trial.name.split(".", 1)[0]
    return "/".join([trial.project().getMocapDepotRootPath(), "subject", subDir])


@memoize.memoize
def getSubjectsFolderForMocapPropVersion(mocapPropVersion):
    """
    Get the depot path of the prop version's subject folder; contains the .ppf and .bdf files (sometimes also the .sdf).

    Will generate it if its not in Watchstar.

    Args:
        mocapPropVersion (AnimData.MocapPropVersion): AnimData MocapPropVersion context to get the subjects dir for.

    Returns:
        str: depot path.
    """
    # Example: //depot/projects/bob/subject/GizmoRed_PR001_V01
    subDir = mocapPropVersion.name
    return "/".join([mocapPropVersion.project().getMocapDepotRootPath(), "subject", subDir])


def _getPpfFileForNonMotionTrial(trial):
    """
    Get the depot path of the pattern (ppf) for the given trial; Needs to return the ppf in the subjects folder.

    Will generate it if its not in Watchstar.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the PPF path for.

    Returns:
        str: depot path.
    """
    # Try getting it from the watchstar associated files
    allFiles = trial.getAllFiles()
    depotPaths = [aFile.filePath() for aFile in allFiles if aFile.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_PPF]
    # Extra filtering for the ROMs that have ppf files on the trial for both the subjects and capture folders.
    depotDir = getSubjectsFolderForTrial(trial)
    depotPaths = [depotPath for depotPath in depotPaths if depotPath.startswith(depotDir)]
    if depotPaths:
        return depotPaths[0]

    # Otherwise, just generate it.
    return "{0}/{1}.ppf".format(depotDir, os.path.basename(depotDir))


def _getPpfFileForMotionTrial(trial):
    """
    Get the depot path of the pattern (ppf) for the given trial; Needs to return the ppf in the takes folder.

    Will generate it if its not in Watchstar.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the PPF path for.

    Returns:
        str: depot path.
    """
    # Try getting it from the watchstar associated files
    allFiles = trial.getAllFiles()
    depotPaths = [aFile.filePath() for aFile in allFiles if aFile.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_PPF]
    takesDir = "/".join([trial.project().getMocapDepotRootPath(), "capture", "takes"])
    depotPaths = [depotPath for depotPath in depotPaths if depotPath.startswith(takesDir)]
    if depotPaths:
        return depotPaths[0]

    # Otherwise, just generate it.
    return "/".join([takesDir, trial.session().getSessionFolderName(), trial.name, "{}.ppf".format(trial.name)])


def getPpfFileForTrial(trial):
    """
    Get the depot path of the pattern (ppf) for the given trial.

    Will generate it if its not in Watchstar.

    Args:
        trial (AnimData.Trial): AnimData trial context to get the PPF path for.

    Returns:
        str: depot path.
    """
    if isinstance(trial, contexts.NonMotionTrialBase):
        return _getPpfFileForNonMotionTrial(trial)
    elif isinstance(trial, contexts.MotionTrialBase):
        return _getPpfFileForMotionTrial(trial)
    raise ValueError("Unsupported trial type: {}".format(type(trial)))


def getPpfFileForMocapPropVersion(mocapPropVersion):
    """
    Get the depot path of the pattern (ppf) for the given mocap prop version.

    Will generate it if its not in Watchstar.

    These files should always be referenced from the subjects folder.

    Args:
        mocapPropVersion (contexts.MocapPropVersion): AnimData MocapPropVersion context to get the PPF path for.

    Returns:
        str: depot path.
    """
    # Try getting it from the watchstar associated files
    allFiles = mocapPropVersion.getAllFiles()
    depotPaths = [romfile.filePath() for romfile in allFiles if romfile.fileType() == animContext.settingsManager.consts.FilePathTypes.GIANT_PPF]

    depotDir = getSubjectsFolderForMocapPropVersion(mocapPropVersion)
    depotPaths = [depotPath for depotPath in depotPaths if depotPath.startswith(depotDir)]
    if depotPaths:
        return depotPaths[0]

    # Otherwise, just generate it
    depotDir = getSubjectsFolderForMocapPropVersion(mocapPropVersion)
    return "{0}/{1}.ppf".format(depotDir, mocapPropVersion.name)


def generatePpfFilePathFromPrj(prjPath):
    """
    Generate the local or depot path of the .ppf matching the provided .prj.

    Warnings:
        Supports only Unix or Perforce depot path formatting.

    Args:
        prjPath (str): Local or depot path of the .prj.

    Returns:
        str: Expected local or depot path of the .ppf matching the .prj.
    """
    splitChar = "/"
    if "\\" in prjPath:
        splitChar = "\\"
    projectDir, _ = prjPath.split("{0}capture{0}".format(splitChar))
    baseName = os.path.splitext(os.path.basename(prjPath))[0]
    assetName = baseName.split(".")[0]
    return splitChar.join([projectDir, "subject", assetName, "{0}.ppf".format(assetName)])


def generateBdfFilePathFromPrj(prjPath):
    """
    Generate the local or depot path of the .bdf matching the provided .prj.

    Warnings:
        Supports only Unix or Perforce depot path formatting.

    Args:
        prjPath (str): Local or depot path of the .prj.

    Returns:
        str: Expected local or depot path of the .bdf matching the .prj.
    """
    ppfPath = generatePpfFilePathFromPrj(prjPath)
    return re.sub(".ppf$", ".bdf", ppfPath)


def getTargetAnmForGameAsset(gameAsset):
    """
    Get the target anm file for the specified Game Asset.

    Args:
        gameAsset (contexts.AssetGameBase): A game asset context, e.g. GameCharacter, GameProp, GameAnimal, etc.

    Returns:
        str: Depot path for the target anm file.
    """
    anmDepotPath = None
    mocapAsset = gameAsset.mocapAsset()
    if mocapAsset:
        anmDepotPath = mocapAsset.gsAnm()
    gsSubstitute = gameAsset.gsSubstitute()
    if gsSubstitute:
        gsSubstituteMocapAsset = gsSubstitute.mocapAsset()
        if gsSubstituteMocapAsset:
            gsSubAnmDepotPath = gsSubstituteMocapAsset.gsAnm()
            if gsSubAnmDepotPath:
                anmDepotPath = gsSubAnmDepotPath
    return anmDepotPath


if __name__ == '__builtin__':
    # This module has full unittests in rockstar/mocapBoss/tests/utils/test_fileManager.py; use them for testing, too.
    trial = animContext.animData.getTrialByID(131086)  # mocap development/smcla_190522am.scott_mclaughlin.rom
    print getRsrefineFbxForTrial(trial)
