"""
Models for dealing with Talents
"""
from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData._internal import contexts as animContexts
from RS.Core.AnimData.Models import baseContext
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, lazyLoadModelItem


class ContextTalentItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return []

    def rowCount(self, parent=None):
        return 0

    def hasChildren(self, parent=None):
        return False

    def canFetchMore(self, parent):
        return False


class TalentModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, project, parent=None):
        self._project = project
        super(TalentModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Context"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int number of columns in the model
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        for talent in self._project.getAllTalent():
            parent.appendChild(ContextTalentItem(talent, parent=parent, rootItem=self))

    def findContextIndex(self, context):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.context): The context to find

        returns:
            QModelIndex of the location
        """
        if context is None:
            return QtCore.QModelIndex()
        return QtCore.QModelIndex()


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)

    rdrPrjId = 69367
    project = Context.animData.getProjectByID(rdrPrjId)
    mainModel = TalentModel(project)
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
    sys.exit(app.exec_())
