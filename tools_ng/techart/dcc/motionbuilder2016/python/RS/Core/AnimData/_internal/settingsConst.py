"""
Warnings:
    This module has been ported from the TechArt depot. Make changes there first and then integrate them here.
"""
class _baseSettingsConst(object):
    def __init__(self, settingsManager):
        self._settingsManager = settingsManager


class _assetClasses(_baseSettingsConst):
    @property
    def ANIMAL(self):
        return self._settingsManager.getAssetClassByName("Animal")

    @property
    def CHARACTER(self):
        return self._settingsManager.getAssetClassByName("Character")

    @property
    def ENVIRONMENT(self):
        return self._settingsManager.getAssetClassByName("Environment")

    @property
    def PROP(self):
        return self._settingsManager.getAssetClassByName("Prop")

    @property
    def VEHICLE(self):
        return self._settingsManager.getAssetClassByName("Vehicle")

    @property
    def HELPER(self):
        return self._settingsManager.getAssetClassByName("Helper")

    def __iter__(self):
        settings = (
            self.CHARACTER,
            self.PROP,
            self.ANIMAL,
            self.VEHICLE,
            self.ENVIRONMENT,
            self.HELPER,
        )
        for setting in settings:
            yield setting


class _featureCategories(_baseSettingsConst):
    @property
    def CUTSCENE(self):
        return self._settingsManager.getFeatureCategoryByName("Cutscene")

    @property
    def INGAME(self):
        return self._settingsManager.getFeatureCategoryByName("In-Game")

    @property
    def ASSET(self):
        return self._settingsManager.getFeatureCategoryByName("3D Asset")

    @property
    def GENERIC(self):
        return self._settingsManager.getFeatureCategoryByName("Generic")

    def __iter__(self):
        settings = (
            self.CUTSCENE,
            self.INGAME,
            self.ASSET,
            self.GENERIC,
        )
        for setting in settings:
            yield setting


class _captureTypes(_baseSettingsConst):
    @property
    def TRIAL(self):
        return self._settingsManager.getCaptureTypesByName("Trial - Trial")

    @property
    def BODY_ROM(self):
        return self._settingsManager.getCaptureTypesByName("Init - Body ROM")

    @property
    def FACE_ROM(self):
        return self._settingsManager.getCaptureTypesByName("Init - Face ROM")

    @property
    def BODY_SCALE(self):
        return self._settingsManager.getCaptureTypesByName("Init - Body Scale")

    @property
    def HEAD_SCAN(self):
        return self._settingsManager.getCaptureTypesByName("Init - Head Scan")

    def __iter__(self):
        settings = (
            self.TRIAL,
            self.BODY_ROM,
            self.FACE_ROM,
            self.BODY_SCALE,
            self.HEAD_SCAN,
        )
        for setting in settings:
            yield setting


class _priorities(_baseSettingsConst):
    @property
    def PRIORITY_1(self):
        return self._settingsManager.getPriorityByName("1 (Highest)")

    @property
    def PRIORITY_2(self):
        return self._settingsManager.getPriorityByName("2")

    @property
    def PRIORITY_3(self):
        return self._settingsManager.getPriorityByName("3")

    @property
    def PRIORITY_4(self):
        return self._settingsManager.getPriorityByName("4")

    @property
    def PRIORITY_5(self):
        return self._settingsManager.getPriorityByName("5 (Lowest)")

    @property
    def NOT_PRIORITIZED(self):
        return self._settingsManager.getPriorityByName("Not Prioritized")

    def __iter__(self):
        settings = (
            self.PRIORITY_1,
            self.PRIORITY_2,
            self.PRIORITY_3,
            self.PRIORITY_4,
            self.PRIORITY_5,
            self.NOT_PRIORITIZED,
        )
        for setting in settings:
            yield setting


class _featureTrialContextTypes(_baseSettingsConst):
    @property
    def AI(self):
        return self._settingsManager.getFeatureTrialTypeByName("AI")

    @property
    def MI(self):
        return self._settingsManager.getFeatureTrialTypeByName("MI")

    @property
    def IG(self):
        return self._settingsManager.getFeatureTrialTypeByName("IG")

    @property
    def CS(self):
        return self._settingsManager.getFeatureTrialTypeByName("CS")

    def __iter__(self):
        settings = (
            self.AI,
            self.MI,
            self.IG,
            self.CS,
        )
        for setting in settings:
            yield setting


class _filePathTypes(_baseSettingsConst):
    @property
    def REF_VIDEO(self):
        return self._settingsManager.getFilePathTypeByName("Stage Video")

    @property
    def FACE_VIDEO(self):
        return self._settingsManager.getFilePathTypeByName("Face Video")

    @property
    def TALENT_VARIATION_IMAGE(self):
        return self._settingsManager.getFilePathTypeByName("Talent Variation Image")

    @property
    def FBX_SCENE(self):
        return self._settingsManager.getFilePathTypeByName("FBX Scene")

    @property
    def ANM_MOCAP_ASSET(self):
        return self._settingsManager.getFilePathTypeByName("ANM Mocap Asset")

    @property
    def FBX_MOCAP_ASSET(self):
        return self._settingsManager.getFilePathTypeByName("FBX Mocap Asset")

    @property
    def GIANT_PRJ(self):
        return self._settingsManager.getFilePathTypeByName("Giant PRJ")

    @property
    def GIANT_SCL(self):
        return self._settingsManager.getFilePathTypeByName("Giant SCL")

    @property
    def GIANT_CHARACTERMAP(self):
        return self._settingsManager.getFilePathTypeByName("Giant CM")

    @property
    def GIANT_CPJ(self):
        return self._settingsManager.getFilePathTypeByName("Giant CPJ")

    @property
    def GIANT_CAL(self):
        return self._settingsManager.getFilePathTypeByName("Giant CAL")

    @property
    def GIANT_ANM(self):
        return self._settingsManager.getFilePathTypeByName("Giant ANM")

    @property
    def GIANT_BMO(self):
        return self._settingsManager.getFilePathTypeByName("Giant BMO")

    @property
    def GIANT_GID(self):
        return self._settingsManager.getFilePathTypeByName("Giant GID")

    @property
    def GIANT_PSF(self):
        return self._settingsManager.getFilePathTypeByName("Giant PSF")

    @property
    def GIANT_RAW(self):
        return self._settingsManager.getFilePathTypeByName("Giant RAW")

    @property
    def GIANT_DOF(self):
        return self._settingsManager.getFilePathTypeByName("Giant DOF")

    @property
    def GIANT_RTPARAMS(self):
        return self._settingsManager.getFilePathTypeByName("Giant RTPARAMS")

    @property
    def GIANT_MDL(self):
        return self._settingsManager.getFilePathTypeByName("Giant MDL")

    @property
    def GIANT_SCH(self):
        return self._settingsManager.getFilePathTypeByName("Giant SCH")

    @property
    def GIANT_SDF(self):
        return self._settingsManager.getFilePathTypeByName("Giant SDF")

    @property
    def GIANT_LMT(self):
        return self._settingsManager.getFilePathTypeByName("Giant LMT")

    @property
    def GIANT_PDF(self):
        return self._settingsManager.getFilePathTypeByName("Giant PDF")

    @property
    def GIANT_BDF(self):
        return self._settingsManager.getFilePathTypeByName("Giant BDF")

    @property
    def GIANT_PPF(self):
        return self._settingsManager.getFilePathTypeByName("Giant PPF")

    @property
    def GIANT_PSP(self):
        return self._settingsManager.getFilePathTypeByName("Giant PSP")

    @property
    def FBX_AUDIO(self):
        return self._settingsManager.getFilePathTypeByName("FBX Audio")

    @property
    def GIANT_REFINEDATA(self):
        return self._settingsManager.getFilePathTypeByName("Giant Refine ANM")

    @property
    def GIANT_REFINE_FBX(self):
        return self._settingsManager.getFilePathTypeByName("Giant Refine FBX")

    @property
    def GIANT_DEFORMER(self):
        return self._settingsManager.getFilePathTypeByName("Giant Lattice FFD")

    @property
    def EST_AUDIO(self):
        return self._settingsManager.getFilePathTypeByName("EST FBX Audio")

    @property
    def SINGLE_CAMERA_CALIBRATION(self):
        return self._settingsManager.getFilePathTypeByName("Single Cam Cal")

    @property
    def MULTI_CAMERA_CALIBRATION(self):
        return self._settingsManager.getFilePathTypeByName("Multi Cam Cal")

    @property
    def HELMET_CAMERA_CALIBRATION(self):
        return self._settingsManager.getFilePathTypeByName("Helmet Cam Cal")

    @property
    def HELMET_CAMERA_VIDEO(self):
        return self._settingsManager.getFilePathTypeByName("Helmet Cam Video")

    def __iter__(self):
        settings = (
            self.REF_VIDEO,
            self.FACE_VIDEO,
            self.TALENT_VARIATION_IMAGE,
            self.FBX_SCENE,
            self.ANM_MOCAP_ASSET,
            self.FBX_MOCAP_ASSET,
            self.GIANT_PRJ,
            self.GIANT_SCL,
            self.GIANT_CHARACTERMAP,
            self.GIANT_CPJ,
            self.GIANT_CAL,
            self.GIANT_ANM,
            self.GIANT_BMO,
            self.GIANT_GID,
            self.GIANT_PSF,
            self.GIANT_RAW,
            self.GIANT_RTPARAMS,
            self.GIANT_MDL,
            self.GIANT_SCH,
            self.GIANT_SDF,
            self.GIANT_LMT,
            self.GIANT_PDF,
            self.GIANT_DOF,
            self.GIANT_BDF,
            self.GIANT_PPF,
            self.GIANT_PSP,
            self.FBX_AUDIO,
            self.GIANT_REFINEDATA,
            self.GIANT_REFINE_FBX,
            self.GIANT_DEFORMER,
            self.EST_AUDIO,
            self.SINGLE_CAMERA_CALIBRATION,
            self.MULTI_CAMERA_CALIBRATION,
            self.HELMET_CAMERA_CALIBRATION,
            self.HELMET_CAMERA_VIDEO,
        )
        for setting in settings:
            yield setting

    def getAllFilePathTypes(self):
        """
        Get a list of the file path settings.

        Returns:
            list
        """
        return list(self)


class _mocapPropCategories(_baseSettingsConst):
    @property
    def CAMERA(self):
        return self._settingsManager.getMocapPropCategoryByName("Camera")

    @property
    def DEFAULT(self):
        return self._settingsManager.getMocapPropCategoryByName("Default")

    @property
    def GIZMO(self):
        return self._settingsManager.getMocapPropCategoryByName("Gizmo")

    @property
    def LASER(self):
        return self._settingsManager.getMocapPropCategoryByName("Laser")

    @property
    def PROP_CLONE(self):
        return self._settingsManager.getMocapPropCategoryByName("PropClone")

    @property
    def PROP_TOY(self):
        return self._settingsManager.getMocapPropCategoryByName("PropToy")

    @property
    def SLATE(self):
        return self._settingsManager.getMocapPropCategoryByName("Slate")

    def __iter__(self):
        settings = (
            self.CAMERA,
            self.DEFAULT,
            self.GIZMO,
            self.LASER,
            self.PROP_CLONE,
            self.PROP_TOY,
            self.SLATE,
        )
        for setting in settings:
            yield setting


class SettingsConst(_baseSettingsConst):
    def __init__(self, settingsManager):
        super(SettingsConst, self).__init__(settingsManager)
        self._assetClasses = _assetClasses(settingsManager)
        self._featureCategories = _featureCategories(settingsManager)
        self._captureTypes = _captureTypes(settingsManager)
        self._priorities = _priorities(settingsManager)
        self._filePathTypes = _filePathTypes(settingsManager)
        self._featureTrialContextTypes = _featureTrialContextTypes(settingsManager)
        self._mocapPropCategories = _mocapPropCategories(settingsManager)

    @property
    def AssetClasses(self):
        return self._assetClasses

    @property
    def FeatureCategories(self):
        return self._featureCategories

    @property
    def CaptureTypes(self):
        return self._captureTypes

    @property
    def Priorities(self):
        return self._priorities

    @property
    def FilePathTypes(self):
        return self._filePathTypes

    @property
    def MocapPropCategories(self):
        return self._mocapPropCategories

    @property
    def FeatureTrialTypes(self):
        return self._featureTrialContextTypes
