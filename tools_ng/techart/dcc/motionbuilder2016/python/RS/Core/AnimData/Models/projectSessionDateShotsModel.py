"""
Models for dealing with contexts
"""
from PySide import QtGui, QtCore

from RS.Core.AnimData import Context, Const
from RS.Core.AnimData.Models import baseContext
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class ContextProjectItem(baseContext.ContextBaseItem):
    def __init__(self, context, sessions, parent=None, rootItem=None):
        """
        Constructor
        
        args:
            context (Project): The conext for the item, in this case , the project
            sessions (list of Session): The sessions to put under this item
        
        kwargs:
            parent (baseModelItem): The parent of this item
            rootItem (baseModelItem): The root item of the model
        """
        self._sessions = sessions
        super(ContextProjectItem, self).__init__(context, parent=parent, rootItem=rootItem, addLazyLoader=False)

    def _populateChildren(self):
        return [ContextSessionsItem(session, self, rootItem=self._rootItem) for session in self._sessions]


class ContextSessionsItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return [ContextShotsItem(shot, self, rootItem=self._rootItem) for shot in self._context.getAllShots()]


class ContextShotsItem(baseContext.ContextBaseItem):
    def __init__(self, context, parent=None, rootItem=None):
        super(ContextShotsItem, self).__init__(context, parent=parent, rootItem=rootItem, addLazyLoader=False)

    def _populateChildren(self):
        return []

    def rowCount(self, parent=None):
        return 0

    def hasChildren(self, parent=None):
        return False

    def canFetchMore(self, parent):
        return False


class ProjectSessionDateShotsModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, date=None, location=None, parent=None):
        """
        Constructor
        
        kwargs:
            location (Location): The location to find the sessions for
            date (datetime.datetime): The Date to find the sessions for
        """
        self._date = date
        self._location = location
        super(ProjectSessionDateShotsModel, self).__init__(parent=parent)

    def date(self):
        """
        Get the current date
        
        returns:
            datetime.datetime 
        """
        return self._date

    def location(self):
        """
        get the current location
        
        returns:
            Location
        """
        return self._location

    def setDate(self, date):
        """
        Set the date to search for
        
        args:
            date (datetime.datetime): The date
        """
        self._date = date
        self.reset()

    def setLocation(self, location):
        """
        Set the location to search for
        
        args:
            location (Location): The location
        """
        self._location = location
        self.reset()
        
    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Context"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int of the number of columns in the model
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._date is None or self._location is None:
            return
        
        sessions = Context.animData.getLocationSessions(self._location, self._date)

        projectDict = {}
        for session in sessions:
            curSessions = projectDict.get(session.project(), [])
            curSessions.append(session)
            projectDict[session.project()] = curSessions
        
        # Use that new dict to make the model
        for prj, sessions in projectDict.iteritems():
            parent.appendChild(ContextProjectItem(prj, sessions, parent=parent, rootItem=self))
