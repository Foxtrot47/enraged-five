from PySide import QtGui, QtCore

from RS.Core.AnimData._internal import contexts as animContexts


class AnimDataTrialsModelFilterModel(QtGui.QSortFilterProxyModel):
    """
    Proxy Filter Model
    """
    def __init__(self, parent=None):
        self._hideNonSelects = False
        self._completeSessions = False
        self._typesToShow = ()
        self._pipelineStepsToShow = ()
        super(AnimDataTrialsModelFilterModel, self).__init__(parent=parent)

    def filterAcceptsRow(self, row, parent=QtCore.QModelIndex()):
        """
        Internal Method

        """
        if self.sourceModel() is None:
            return False

        index = self.sourceModel().index(row, 0, parent)
        con = self.sourceModel().data(index, QtCore.Qt.UserRole)
        if isinstance(con, (animContexts.Session, animContexts.Shoots)):
            if self._completeSessions:
                return not con.isDelivered()
        elif isinstance(con, animContexts._trialBase):
            if self._hideNonSelects:
                if con.isSelected() is not True:
                    return False
            if len(self._typesToShow) == 0:
                return True

            if con.captureType not in self._typesToShow:
                return False

            if len(self._pipelineStepsToShow) == 0:
                return True

            if con.pipelineStage() not in self._pipelineStepsToShow:
                return False
        return True

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None, completeSessions=None, filterPipeline=None):
        if selectesOnly is not None:
            self._hideNonSelects = selectesOnly

        if takeTypes is not None:
            self._typesToShow = tuple(takeTypes)

        if sortAssending is not None:
            if sortAssending is True:
                self.sort(0, QtCore.Qt.AscendingOrder)
            else:
                self.sort(0, QtCore.Qt.DescendingOrder)

        if completeSessions is not None:
            self._completeSessions = completeSessions

        if filterPipeline is not None:
            self._pipelineStepsToShow = filterPipeline

        self.invalidate()

    def hideNonSelected(self, val):
        """
        Set if the Non Selects are shown or hidden

        args:
            val (bool): if selects are shown or not
        """
        self._hideNonSelects = val
        self.invalidate()

    def setShownTypes(self, shownTypes):
        """
        Set the types of trials to show

        args:
            shownTypes (list of _trialBase): list of trial types to show
        """
        self._typesToShow = tuple(shownTypes)
        self.invalidate()

    def setShownType(self, shownType):
        """
        Set the type of trials to show

        args:
            shownType (_trialBase): trials types to show
        """
        self._typesToShow = (shownType)
        self.invalidate()

    def showAllTypes(self):
        """
        show all the types of trials
        """
        self._typesToShow = ()
        self.invalidate()

    def setShownPipelineStages(self, shownPipelineStages):
        """
        Set the types of Pipeline Stages to show

        args:
            shownPipelineStages (list of string): list of name of the pipeline stages to show
        """
        self._pipelineStepsToShow = tuple(shownPipelineStages)
        self.invalidate()

    def setShownPipelineStage(self, shownPipelineStage):
        """
        Set the type of Pipeline Stages to show

        args:
            shownPipelineStage (string): name of the pipeline stages to show
        """
        self._pipelineStepsToShow = (shownPipelineStage)
        self.invalidate()

    def showAllPipelineStages(self):
        """
        show all the Pipeline Stages
        """
        self._pipelineStepsToShow = ()
        self.invalidate()

    def canFetchMore(self, index):
        return self.sourceModel().canFetchMore(self.mapToSource(index)) or False

    def fetchMore(self, index):
        return self.sourceModel().fetchMore(self.mapToSource(index)) or False

    def findContextIndex(self, context):
        return self.mapFromSource(self.sourceModel().findContextIndex(context))

    def setRootModelIndex(self, newIndex):
        self.sourceModel().setRootModelIndex(self.mapToSource(newIndex))
        self.invalidate()

    def reset(self):
        self.sourceModel().reset()
        super(AnimDataTrialsModelFilterModel, self).reset()


class AnimDataFilesModelFilterModel(QtGui.QSortFilterProxyModel):
    """
    Proxy Filter Model
    """
    def __init__(self, parent=None):
        self._hideFilesWithNoPath = False
        self._hideFilesWithNoPath = True
        self._typesToShow = ()

        super(AnimDataFilesModelFilterModel, self).__init__(parent=parent)

    def filterAcceptsRow(self, row, parent=QtCore.QModelIndex()):
        """
        Internal Method

        """
        if self.sourceModel() is None:
            return False

        index = self.sourceModel().index(row, 0, parent)
        con = self.sourceModel().data(index, QtCore.Qt.UserRole)

        if len(self._typesToShow) != 0:
            if not con.fileType() in self._typesToShow:
                return False

        if con.rawFilePath() == None or con.rawFilePath() == "None":
            return False
        return True

    def lessThan(self, left, right):
        leftCon = self.sourceModel().data(left, QtCore.Qt.UserRole)
        rightCon = self.sourceModel().data(right, QtCore.Qt.UserRole)
        if leftCon.thumbnailPath() == '':
            if rightCon.thumbnailPath() != '':
                return False
        if rightCon.thumbnailPath() == '':
            return True
        return leftCon.name < rightCon.name

    def hideFilesWithNoPath(self, val):
        """
        Set if the files with no path are shown or hidden

        args:
            val (bool): if selects are shown or not
        """
        self._hideFilesWithNoPath = val
        self.invalidate()

    def setFileTypeFilters(self, shownTypes):
        self._typesToShow = tuple(shownTypes)
        self.invalidate()

    def setFileTypeFilter(self, shownType):
        self._typesToShow = (shownType)
        self.invalidate()

    def clearFileTypeFilters(self):
        """
        show all the types of files
        """
        self._typesToShow = ()
        self.invalidate()

    def canFetchMore(self, index):
        return self.sourceModel().canFetchMore(self.mapToSource(index)) or False

    def fetchMore(self, index):
        return self.sourceModel().fetchMore(self.mapToSource(index)) or False

    def findContextIndex(self, context):
        return self.mapFromSource(self.sourceModel().findContextIndex(context))

    def setRootModelIndex(self, newIndex):
        self.sourceModel().setRootModelIndex(self.mapToSource(newIndex))
        self.invalidate()


class AnimDataPropsModelFilterModel(QtGui.QSortFilterProxyModel):
    """
    Proxy Filter Model
    """
    def __init__(self, parent=None):
        self._hideNonPerformanceProps = True
        super(AnimDataPropsModelFilterModel, self).__init__(parent=parent)
        self.setDynamicSortFilter(True)
        self.sort(0)

    def filterAcceptsRow(self, row, parent=QtCore.QModelIndex()):
        """
        Internal Method

        """
        if self.sourceModel() is None:
            return False

        index = self.sourceModel().index(row, 0, parent)
        con = self.sourceModel().data(index, QtCore.Qt.UserRole)

        if isinstance(con, animContexts.MocapProp):
            if self._hideNonPerformanceProps is True:
                if con.isPerformanceProp() is False:
                    return False
        return True

    def setFilterOptions(self, hideNonPerformanceProps=None):
        if hideNonPerformanceProps is not None:
            self._hideNonPerformanceProps = hideNonPerformanceProps

        self.invalidate()

    def lessThan(self, left, right):
        leftCon = self.sourceModel().data(left, QtCore.Qt.DisplayRole)
        rightCon = self.sourceModel().data(right, QtCore.Qt.DisplayRole)
        return leftCon < rightCon

    def hideNonPerformanceProps(self, val):
        """
        Set if the files with no path are shown or hidden

        args:
            val (bool): if selects are shown or not
        """
        self._hideNonPerformanceProps = val
        self.invalidate()

    def canFetchMore(self, index):
        return self.sourceModel().canFetchMore(self.mapToSource(index)) or False

    def fetchMore(self, index):
        return self.sourceModel().fetchMore(self.mapToSource(index)) or False

    def findContextIndex(self, context):
        return self.mapFromSource(self.sourceModel().findContextIndex(context))

    def setRootModelIndex(self, newIndex):
        self.sourceModel().setRootModelIndex(self.mapToSource(newIndex))
        self.invalidate()
