from PySide import QtGui, QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem
from RS.Core.AnimData import Context as animContexts


class GamePropModelItem(baseModelItem.BaseModelItem):
    """
    This represents a single Game Prop
    """
    def __init__(self, gameProp, parent=None):
        """
        Constructor
        """
        super(GamePropModelItem, self).__init__(parent=parent)
        self._gameProp = gameProp

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._gameProp.name
            elif column == 1:
                return self._gameProp.path()
            elif column == 2:
                return self._gameProp.category()
            elif column == 3:
                return self._gameProp.isActive()

        elif role == QtCore.Qt.UserRole:
            return self._gameProp

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class GamePropsModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, project=None, parent=None):
        self._project = project
        super(GamePropsModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Name", "Path", "Prop Category", "isActive"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def setProject(self, project):
        self._project = project
        self.reset()

    def project(self):
        return self._project

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._project is None:
            return

        for gameProp in self._project.getGameProps():
            parent.appendChild(GamePropModelItem(gameProp, parent=parent))

    def findContextIndex(self, context):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.Context): The context to find

        returns:
            QModelIndex of the location
        """
        if context is None:
            return QtCore.QModelIndex()

        idxs = self.match(self.createIndex(0, 0), 0, context.name, 1, QtCore.Qt.MatchExactly)
        if len(idxs) == 0:
            return QtCore.QModelIndex()
        idxs = idxs[0]

        return idxs


if __name__ == "__main__":
    import sys

    from RS.Core.AnimData import Context, Const

    app = QtGui.QApplication(sys.argv)
    proj = Context.animData.getProjectByName("Redemption2")
    mainModel = GamePropsModel(proj)
#     win = QtGui.QComboBox()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()

    sys.exit(app.exec_())
