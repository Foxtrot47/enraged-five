"""
Models for dealing with contexts
"""
from PySide import QtGui, QtCore

from RS.Core.AnimData.Models import baseContext
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class UserCheckoutTakesDataModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, project=None, user=None, parent=None):
        self._project = project
        self._user = user
        super(UserCheckoutTakesDataModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Context"]

    def setProject(self, newProject):
        self._project = newProject
        self.reset()

    def project(self):
        return self._project

    def setUser(self, user):
        self._user = user
        self.reset()

    def user(self):
        return self._user

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int number of columns in the model
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._project is None or self._user is None:
            return
        for trial in self._project.getTrialsByUserCheckout(self._user):
            parent.appendChild(baseContext.BaseTrialsItem(trial, parent=parent, rootItem=self))

    def findContextIndex(self, context):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.Context): The context to find

        returns:
            QModelIndex of the location
        """
        return QtCore.QModelIndex()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    from RS.Core.AnimData import Context

    prj = Context.animData.getProjectByName("Redemption2")
    users = Context.settingsManager.getUsers()
    
    mainModel = UserCheckoutTakesDataModel(prj)
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    mainModel.setUser(users[3])
    win.show()
    sys.exit(app.exec_())
