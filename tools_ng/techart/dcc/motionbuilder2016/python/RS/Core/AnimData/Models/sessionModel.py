"""
Models for dealing with contexts
"""
from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData._internal import contexts as animContexts
from RS.Core.AnimData.Models import baseContext
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, lazyLoadModelItem


class ContextProjectItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return [ContextShootsItem(shoot, self, rootItem=self._rootItem) for shoot in self._context.getAllShoots()]


class ContextShootsItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return [ContextSessionsItem(day, self, rootItem=self._rootItem) for day in self._context.getAllSessions()]


class ContextSessionsItem(baseContext.ContextBaseItem):
    def __init__(self, context, parent=None, rootItem=None):
        super(ContextSessionsItem, self).__init__(context, parent=parent, rootItem=rootItem, addLazyLoader=False)

    def _populateChildren(self):
        return []

    def rowCount(self, parent=None):
        return 0

    def hasChildren(self, parent=None):
        return False

    def canFetchMore(self, parent):
        return False


class ShootSessionsModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, initalProject=None, parent=None):
        self._initalProject = initalProject
        super(ShootSessionsModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Context"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int number of columns in the model
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._initalProject is None:
            for prj in Context.animData.getAllProjects():
                parent.appendChild(ContextProjectItem(prj, parent=parent, rootItem=self))
        else:
            parent.appendChild(ContextProjectItem(self._initalProject, parent=parent, rootItem=self))

    def findContextIndex(self, context):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.Context): The context to find

        returns:
            QModelIndex of the location
        """
        if context is None:
            return QtCore.QModelIndex()

        sameType = False
        # Project
        if isinstance(context, animContexts.Project):
            projName = context.name
            sameType = True
        else:
            projName = context.project().name

        projIdx = self.match(self.createIndex(0, 0), 0, projName, 1, QtCore.Qt.MatchExactly)
        if len(projIdx) == 0:
            return QtCore.QModelIndex()
        projIdx = projIdx[0]
        if sameType:
            return projIdx
        proj = self.data(projIdx, QtCore.Qt.UserRole + 1)

        # Shoot
        if isinstance(context, animContexts.Shoots):
            con = context
            sameType = True
        else:
            con = context.shoot()
        shootIdx = proj.findContextIndex(con, projIdx)
        if shootIdx is None:
            return QtCore.QModelIndex()

        if sameType:
            return shootIdx
        shoot = self.data(shootIdx, QtCore.Qt.UserRole + 1)

        # Capture Day
        if isinstance(context, animContexts.Session):
            con = context
            sameType = True
        else:
            con = context.session()
        sessionIdx = shoot.findContextIndex(con, shootIdx)
        if sessionIdx is None:
            return QtCore.QModelIndex()
        return sessionIdx


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    mainModel = ShootSessionsModel()
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
    sys.exit(app.exec_())
