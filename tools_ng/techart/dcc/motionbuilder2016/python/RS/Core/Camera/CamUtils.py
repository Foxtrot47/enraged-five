"""
Description:
    Generic Mobu functions used frequently with camera and automation scripts.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import time
import pyfbsdk as mobu

from RS.Utils.Scene import Plug
from RS import Globals, Config, Perforce as P4
from RS.Utils import Scene
from RS.Tools.UI import Application
from RS.Core.Animation import Lib as AnimLib
from RS.Core.Camera import Lib as CamLib, CameraLocker, ClipTools, LensTools
CAMERA_SELECTION = {}


def DeleteInvalidCams():
    """
    Deletes any invalid cams and cam folders from the FBX (like 3Lateral face cams).
    """
    invalidCamNames = ["rcam", "laser", "gizmo", "build", "3lat"]
    cameraList = list(mobu.FBSystem().Scene.Cameras[7:])
    for camera in cameraList:
        for invalidName in invalidCamNames:
            if camera.LongName.lower().startswith(invalidName):
                if ":" not in camera.LongName:
                    CamLib.MobuCamera(camera).DeleteCamera()
                else:
                    camComps = Scene.Component.GetComponents(Namespace=camera.LongName.split(":")[0])
                    Scene.Component.Delete(camComps, deleteNamespaces=True)
                break
    invalidFolderNames = ["buildcameras"]
    camFolders = [folder for folder in mobu.FBSystem().Scene.Folders if folder.Name.lower() in invalidFolderNames]
    for folder in camFolders:
        folder.FBDelete()


def ShowOnlyCameras():
    """
    Debug code used to check broken cams in scenes more easily.
    """
    lSceneList = mobu.FBModelList()
    mobu.FBGetSelectedModels(lSceneList, None, False)
    mobu.FBGetSelectedModels(lSceneList, None, True)
    for iScene in lSceneList:
        if isinstance(iScene, mobu.FBModel):
            iScene.Show = False
        if isinstance(iScene, mobu.FBCamera):
            iScene.Show = True


def RemoveStarsFromSwitcherKeys():
    """
    Looks for any switcher keys with stars (that are between frames
    and marked with an asterick) and moves them to exact frames instead.
    """
    switcherNode = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
    switcherKeys = switcherNode.FCurve.Keys
    for eachKey in switcherKeys:
        if eachKey.Time.GetTimeString().endswith("*"):
            fixedTime = eachKey.Time.GetFrame()
            eachKey.Time = mobu.FBTime(0, 0, 0, fixedTime)


def OverrideSwitcherKeys(camName):
    # Get Cam Index
    camNameList = [cam.Name for cam in mobu.FBSystem().Scene.Cameras[7:]]
    camIndex = float(camNameList.index(camName)) + 1.0

    # Select Base Layer
    mobu.FBSystem().CurrentTake.SetCurrentLayer(0)

    # Replace Existing Keys
    mbSwitcherNode = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
    for eachKey in mbSwitcherNode.FCurve.Keys:
        eachKey.Value = camIndex


def SetupExportCam():
    """
    Cleans the switcher and sets up the export camera based on various conditions.

    We create an export camera if one doesn't exist, plot if cams and keys are found,
    key/plot the first cam if some cams are found (but no keys), or just move the
    export cam to the producer cam position if there's no keys or cams at all.
    """
    # Create Switcher Node, Keys, and Camera List
    switcherNode = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
    switcherKeys = switcherNode.FCurve.Keys

    # Disabled reusing old export cam - ensures it's rebuilt correctly

    # Unlock All Cams
    camLockObj = CameraLocker.LockManager()
    camLockObj.setAllCamLocks(False)

    # SWITCHER KEYS FOUND - Clean switcher and plot
    if switcherKeys:
        RemoveStarsFromSwitcherKeys()
        GoToFrameNumber(-1)
        expCam = PlotSwitcherToExportCamera(mobu.FBSystem().CurrentTake, Silent=True)

    # NO KEYS - wipe expCam keys and move it to the producer perspective
    else:
        # Force New Export Cam
        expCam = CamLib.MobuCamera.CreateExportCamera().camera

        # Get Producer and KeyTime
        prodCam = mobu.FBSystem().Scene.Cameras[0]
        keyTime = mobu.FBTime()
        keyTime.SetFrame(0)

        # Set Export Cam to Animated
        expCam.Translation.SetAnimated(True)
        expCam.Rotation.SetAnimated(True)
        
        # Erase Any Old Keys
        for propNode in expCam.AnimationNode.Nodes:
            if propNode.FCurve:
                propNode.FCurve.EditClear()
            for axisNode in propNode.Nodes:
                axisNode.FCurve.EditClear()

        # Key Export Cam At Producer Cam's Position
        expCam.Translation.GetAnimationNode().KeyAdd(keyTime, list(prodCam.Translation))
        expCam.Rotation.GetAnimationNode().KeyAdd(keyTime, list(prodCam.Rotation))

        # Key FOV and Lens - if lens prop found
        expCam.PropertyList.Find("Field of View").Data = 37.0
        # Disabled forcing lensProp.Data = 2 - shouldn't need it.

    return expCam


def GetThumbnailFrame():
    """
    Uses the switcher keys to determine an ideal thumbnail frame.
    Returns:
        An int of an ideal thumbnail frame.
    """
    # Remove Any Switcher Stars
    RemoveStarsFromSwitcherKeys()

    # Get Switcher Frames
    switcherIndexProp = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index')
    switcherKeys = switcherIndexProp.GetAnimationNode().FCurve.Keys
    switcherFrames = [eachKey.Time.GetFrame() for eachKey in switcherKeys]

    # Empty Switcher - Add a frame at 0 if there's no keys in the switcher
    if len(switcherFrames) == 0:
        switcherFrames.append(0)

    # Create Basic Cut List - from keys within our hard ranges
    endFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
    cutList = [frame for frame in switcherFrames if 0 <= frame < endFrame]

    # Adjust First Cut - Negative keys need an extra cut at 0.
    if switcherFrames[0] < 0:
        cutList = [0] + cutList
    # Otherwise we just force first key to 0.
    else:
        cutList[0] = 0

    # Pick Frame Based on Number Cuts
    if len(cutList) >= 3:
        thumbnailFrame = cutList[2]
    elif len(cutList) == 2:
        thumbnailFrame = cutList[1]
    elif int(endFrame) > 600:
        thumbnailFrame = 600
    else:
        thumbnailFrame = 0

    return thumbnailFrame


def GoToFrameNumber(frameNumber):
    """
    Moves Mobu's player to the given frameNumber.
    Arguments:
        frameNumber: An int of the frame number to move to.
    """
    newPosition = mobu.FBTime()
    newPosition.SetFrame(int(frameNumber))
    mobu.FBPlayerControl().Goto(newPosition)


def GrabCutsceneZip():
    """
    Detects the current FBX's zip path and syncs via perforce. Useful during exports.
    """
    # Verify Cutscene FBX
    fbxPath = mobu.FBApplication().FBXFileName
    if fbxPath and "!!scenes" in fbxPath.lower():

        # Get Default Zip Folder
        fbxName = os.path.splitext(os.path.split(fbxPath)[1])[0]
        zipName = "{}.icd.zip".format(fbxName.lower())
        zipRoot = os.path.join(Config.Project.Path.Export, "anim", "cutscene")

        # Detect Specific Zip Folder
        perforceZips = [record["depotFile"] for record in P4.Run("files", ["-e", "{}....icd.zip".format(zipRoot)])]
        matchedZips = [zipPath for zipPath in perforceZips if zipPath.lower().endswith(zipName)]

        # Sync Zip Folder - force sync matched zips or normal sync all zips
        if matchedZips:
            zipFolder = os.path.split(matchedZips[0])[0]
            P4.Sync("{}/...zip".format(zipFolder), force=True)
        else:
            P4.Sync("{}/...zip".format(zipRoot))


def HideOldCameraPlanes():
    """
    Hides old planes from all cameras in an FBX.
    """
    planeNames = ["ScreenFade", "FarPlane", "NearPlane",
                  "DOF_Strength_FarPlane", "DOF_Strength_NearPlane"]
    camList = [cam for cam in mobu.FBSystem().Scene.Cameras if not cam.SystemCamera]
    for eachCam in camList:
        for eachChild in list(eachCam.Children):
            for planeName in planeNames:
                if planeName in eachChild.Name:
                    eachChild.Show = False
                    eachChild.PropertyList.Find("Visibility").Data = False
                    break


def BadCompCleanup():
    """
    Removes broken components found in many older FBX files that cause errors on export.
    Mainly used avoid time consuming re-refrencing during captures and cam pass exports.
    """
    badComps = []
    badCompNames = ["es_brimrighttip", "lookat_nub"]
    for comp in Globals.Scene.Components:
        for badName in badCompNames:
            if badName in comp.Name.lower():
                badComps.append(comp)
    [comp.FBDelete() for comp in badComps]


def SelectCurrentSwitcherCamera(focusMarker=None):
    """
    Selects the current switcher camera and sets the focus to the properties of the previously selected camera.
    When the last previously selected camera can't be found, it just assumses the last camera selected was the
    current camera.
    """
    if not Globals.CameraSwitcher.CurrentCamera.Selected:
        currentCamera = Globals.CameraSwitcher.CurrentCamera
        previousCamera = CAMERA_SELECTION.setdefault("previousCamera", currentCamera)

        if not Globals.Player.IsPlaying:
            if Plug.IsDestroyed(previousCamera):
                CAMERA_SELECTION["previousCamera"] = Globals.CameraSwitcher.CurrentCamera
                return

            properties = ("Translation (Lcl)", "Rotation (Lcl)", "Field Of View", "Roll", "Lens", "FOCUS (cm)")

            isMarkerActive = focusMarker is not None and focusMarker.ClickerActive
            previousFocus = []

            for fbproperty in properties:
                try:
                    previousProperty = previousCamera.PropertyList.Find(fbproperty)
                    if isMarkerActive and fbproperty != properties[-1]:
                        previousFocused = False
                    else:
                        previousFocused = previousProperty.IsFocused()
                    previousProperty.SetFocus(False)
                    previousFocus.append(previousFocused)
                except:
                    pass

            [setattr(camera, "Selected", False) for camera in Globals.Cameras if camera.Selected]
            currentCamera.Selected = True
            CAMERA_SELECTION["previousCamera"] = currentCamera

            # There is a motion builder bug that if a property is in focus BUT the property is not visible via the
            # UI then it won't return that it is in Focus even if it is.
            # Here we set the focus to the properties of the current switcher camera after selecting it to make sure
            # they are toggled properly

            for fbproperty, previousFocused in zip(properties, previousFocus):
                try:
                    currentProperty = currentCamera.PropertyList.Find(fbproperty)
                    currentFocused = currentProperty.IsFocused()
                    if currentFocused != previousFocused:
                        currentProperty.SetFocus(previousFocused)
                except:
                    pass


def FixCutsceneHardRanges():
    """
    Sets our hard range start to 0 and removes partial * frames from the hard end range.
    """
    endString = Globals.System.CurrentTake.LocalTimeSpan.GetStop().GetTimeString()
    if endString.endswith('*'):
        endString = endString[:-1]
    fixedTimeSpan = mobu.FBTimeSpan(mobu.FBTime(0, 0, 0, 0), mobu.FBTime(0, 0, 0, int(endString)))
    Globals.System.CurrentTake.LocalTimeSpan = fixedTimeSpan


def RenderFramesForAvid(framePath):
    """
    Renders out an image sequence for Avid at the framePath.
    Arguments:
        framePath (string): the path to render the frames to (without frame numbers).
    """
    # Setup Ranges
    Globals.System.Scene.Renderer.UseCameraSwitcher = True

    # Set Render Options
    renderOptions = mobu.FBVideoGrabber().GetOptions()
    Globals.Player.SnapMode = mobu.FBTransportSnapMode.kFBTransportSnapModeSnapOnFrames
    renderOptions.CameraResolution = renderOptions.CameraResolution.kFBResolutionHD
    renderOptions.RenderAudio = False
    renderOptions.ShowTimeCode = True
    renderOptions.ShowSafeArea = False
    renderOptions.ShowCameraLabel = False
    renderOptions.AntiAliasing = False
    renderOptions.TimeSpan = mobu.FBTimeSpan(mobu.FBTime(0, 0, 0, 0), mobu.FBTime(0, 0, 0, 0))
    renderOptions.TimeSteps = mobu.FBTime(0, 0, 0, 1)
    renderOptions.OutputFileName = framePath

    # Render Scene
    Globals.Application.FileRender(renderOptions)


def ShowOnlyCharactersForAdr():
    """
    Hides all objects in a scene except for the characters.
    Works fine most of the time, but needs tweaks, as some stuff still gets through.
    """
    # Get Valid Groups
    validGroups = []
    for character in Globals.Characters:
        rootModel = character.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
        if rootModel:
            charGroups = Scene.GetConnections(rootModel, incoming=False, outgoing=True,
                                              filterByType=(mobu.FBGroup,), exactType=False)
            if charGroups:
                charGroup = charGroups[0]
                while len(charGroup.Parents) > 1:
                    charGroup = charGroup.Parents[-1]
                validGroups.append(charGroup)

    # Hide All Groups Not in Valid Groups
    for group in Globals.Scene.Groups:
        if len(group.Parents) == 1 and group not in validGroups:
            group.Show = False

    # Hide Mover Groups - because they're annoying
    for group in Globals.Scene.Groups:
        if group.Name.lower().endswith("mover"):
            for validGroup in validGroups:
                if validGroup in group.Parents:
                    group.Show = False
                    break


def DetectDofPass(camera):
    invalidFocusList = [150.0, 0.0]
    focusProp = camera.PropertyList.Find("FOCUS (cm)")
    if focusProp is not None:
        # Check Current Value - return true if valid
        if focusProp.Data not in invalidFocusList:
            return True
        focusNode = focusProp.GetAnimationNode()
        if focusNode:
            focusKeys = focusNode.FCurve.Keys
            # Check Key Values - a slow check, but needed for rare cases
            for key in focusKeys:
                if key.Value not in invalidFocusList:
                    return True
    return False


def DetectDofPassComplete():
    """
    Detects if Dof Pass complete, potentially to be used during captures.
    Returns:
        (bool) - True if over half of the switcher cams have non-default focus values.
    """
    switcherCams = CamLib.Manager.GetSwitcherCameras()
    for cam in switcherCams:
        dofFound = DetectDofPass(cam.camera)
        if dofFound:
            return True
    return False


def StepSwitcherKeys(forward=True):
    """
    Moves the player to the next switcher key or previous switcher key.
    Keyword Arguments:
        forward (bool): True for next key, false for previous
    """
    switcher = CamLib.Manager.GetCameraSwitcher()
    switcherKeys = switcher.PropertyList.Find('Camera Index').GetAnimationNode().FCurve.Keys
    currentFrame = Globals.System.LocalTime.GetFrame()
    for index, key in enumerate(switcherKeys):
        keyFrame = key.Time.GetFrame()
        if forward:
            if keyFrame > currentFrame:
                Globals.Player.Goto(mobu.FBTime(0, 0, 0, keyFrame))
                break
        elif keyFrame < currentFrame:
            nextFrame = switcherKeys[index+1].Time.GetFrame() if index + 1 != len(switcherKeys) else None
            if nextFrame is None or nextFrame >= currentFrame:
                Globals.Player.Goto(mobu.FBTime(0, 0, 0, keyFrame))
                break


def ToggleSwitcherShortcuts(enabled=True):
    """
    Toggles the keyboard shortcuts for stepping through switcher keys.
    Keyword Arguments:
        enabled (bool): True to add shortcut keys, False to remove them
    """
    stepForwardCombo = "alt+Right"
    stepBackCombo = "alt+Left"
    if enabled:
        Application.AddShortCut(stepForwardCombo, lambda *args: StepSwitcherKeys(True))
        Application.AddShortCut(stepBackCombo, lambda *args: StepSwitcherKeys(False))
    else:
        Application.RemoveShortCut(stepForwardCombo)
        Application.RemoveShortCut(stepBackCombo)


def ResetCameraInterest():
    rsCam = CamLib.MobuCamera(mobu.FBSystem().Scene.Renderer.CurrentCamera)
    rsCam.SetInterestDistance()


def ConstrainCameraForPlot(sourceCam, destCam, propNames):
    """
    Creates a constraint from a sourceCam to a destCam using the specified propNames.
    Arguments:
        sourceCam (mobu.FBCamera): the parent camera to constraint from
        destCam (mobu.FBCamera): the child camera to constrain
        propNames (list of strings): the names of the properties to attach
    Returns:
        camPlotConst (mobu.FBConstraintRelation): the newly created constraint
    """
    camPlotConst = mobu.FBConstraintRelation("CamPlot")
    camSourceBox = camPlotConst.SetAsSource(sourceCam)
    camDestBox = camPlotConst.ConstrainObject(destCam)
    camPlotConst.SetBoxPosition(camDestBox, 700, 20)
    lensList = [lens.FOV for lens in LensTools.Manager.GetLensDict().itervalues()]

    for propName in propNames:
        # Set Props To Animated
        sourceProp = sourceCam.PropertyList.Find(propName)
        if sourceProp and sourceProp.IsAnimated() is False:
            sourceProp.SetAnimated(True)

        # Get Src and Dest Nodes - removing any Leading "Lcl" Text
        propName = propName.split("Lcl ")[1] if propName.startswith("Lcl ") else propName
        srcNode = [node for node in camSourceBox.AnimationNodeOutGet().Nodes if node.Name == propName]
        destNode = [node for node in camDestBox.AnimationNodeInGet().Nodes if node.Name == propName]

        # Lens Missing
        if propName == "Lens" and not srcNode:
            # Get FOV Values
            fovKeys = [key for key in sourceCam.FieldOfView.GetAnimationNode().FCurve.Keys]
            fovValues = set(key.Value for key in fovKeys)
            # Single FOV - detect nearest lens
            if len(fovValues) == 1:
                srcFov = sourceCam.FieldOfView
                nearestIndex = LensTools.GetNearestLensIndex(srcFov)
                lensValue = nearestIndex if abs(srcFov - lensList[nearestIndex]) < 1.0 else len(lensList) - 1
            # Multiple FOVs - use zoom lens
            else:
                lensValue = len(lensList) - 1

            integerBox = camPlotConst.CreateFunctionBox("Number", "Integer")
            camPlotConst.SetBoxPosition(integerBox, 700, 300)
            integerBox.AnimationNodeInGet().Nodes[0].WriteData([lensValue])
            srcNode = [integerBox.AnimationNodeOutGet().Nodes[0]]

        # Connect Valid Nodes
        if srcNode and destNode:
            mobu.FBConnect(srcNode[0], destNode[0])
    camPlotConst.Active = True
    Globals.Scene.Evaluate()
    return camPlotConst


def GetVcamDevice(vcam):
    """ Gets the device for a virtual cameral """
    if vcam.Parent:
        for device in mobu.FBSystem().Scene.Devices:
            if "vcam" in device.Name.lower() and device.ModelBindingRoot == vcam.Parent:
                return device


def GetUndupedVcams():
    """ Gets the virtual cameras used on set """
    vcamsToDupe = []
    vcams = [cam for cam in mobu.FBSystem().Scene.Cameras if cam.PropertyList.Find("TcHours")]
    dupedCams = [cam for cam in mobu.FBSystem().Scene.Cameras if cam.Translation.GetAnimationNode()
                 and cam.Translation.GetAnimationNode().KeyCount > 30]
    # Snap to Frame Start - ensures cam values compared correctly
    frameNumber = mobu.FBSystem().LocalTime.GetFrame()
    framePosition = mobu.FBTime()
    framePosition.SetFrame(frameNumber)
    mobu.FBPlayerControl().Goto(framePosition)
    # Loop Vcams
    for vcam in vcams:
        # Check Vcam Valid - has device with keys
        device = GetVcamDevice(vcam)
        if not device:
            continue
        mainNode = device.AnimationNodeOutGet()
        if not mainNode:
            continue
        transNodes = [node for node in mainNode.Nodes if node.Name == "Tran"]
        # todo: while playing back during recording, the keycount is always zero?
        if not transNodes or transNodes[0].KeyCount == 0:
            continue

        # Check Vcam For Dupes - in case we've duped already
        dupeFound = False
        vcamMatrix = mobu.FBMatrix()
        vcam.GetMatrix(vcamMatrix)
        for dupeCam in dupedCams:
            dupeMatrix = mobu.FBMatrix()
            dupeCam.GetMatrix(dupeMatrix)
            distance = abs(vcamMatrix[12] - dupeMatrix[12]) +\
                       abs(vcamMatrix[13] - dupeMatrix[13]) +\
                       abs(vcamMatrix[14] - dupeMatrix[14])
            if distance < 0.001:
                dupeFound = True
                break

        # No Dupes Found - add to unduped vcam list
        if dupeFound is False:
            vcamsToDupe.append(vcam)
    return vcamsToDupe


def DupeRtcsToRsCams():
    # TODO: The plot here technically lowers vcam fidelity to 30fps - copy animation might be better?
    """
    Searches for any realtime cameras (recorded on set) and plots their data over to new RS cameras.
    """
    # Get Switcher Node
    switcherNode = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()

    # Loop through VCams
    dupecamNumber = None
    vcams = GetUndupedVcams()
    for vcam in vcams:
        # Turn off Device
        device = GetVcamDevice(vcam)
        device.Live = False
        device.RecordMode = False

        # todo: name camera to match cam's frame color, set duped cam frame to match?
        dupeCam = CamLib.MobuCamera.CreateRsCamera().camera
        plotProps = ["Lcl Translation", "Lcl Rotation", "FieldOfView", "Lens", "Roll"]

        # Create Plot Constraint, Plot, and Remove
        camPlotConst = ConstrainCameraForPlot(vcam, dupeCam, plotProps)
        AnimLib.PlotProperties([dupeCam], properties=plotProps, children=False)
        camPlotConst.FBDelete()

        # todo: delete old vcam components? mocap supposedly handling this during cleanup
        # vcamComps = Scene.Component.GetComponents(Namespace=vcam.Name)
        # Scene.Component.Delete(vcamComps, deleteNamespaces=True)

        # Replace Keys in Switcher
        camList = [cam for cam in mobu.FBSystem().Scene.Cameras if cam.SystemCamera is False]
        vcamNumber = camList.index(vcam) + 1
        dupecamNumber = camList.index(dupeCam) + 1
        for key in switcherNode.FCurve.Keys:
            if key.Value == vcamNumber:
                key.Value = dupecamNumber

    # Check for Empty Switcher - and key it with last dupedCam
    if dupecamNumber and len(switcherNode.FCurve.Keys) == 0:
        switcherNode.KeyAdd(mobu.FBTime(0, 0, 0, 0), dupecamNumber)


def GetSwitcherRangeDict(startFrame, endFrame):
    """
    Gets a dictionary of cameras (keys) and switcher ranges (values) within a given start/end frame.
    Arguments:
        startFrame (int): the 'master' start range
        endFrame (int): the 'master' end range
    Returns:
        rangeDict (dict): mobu cameras as keys and a list of sub ranges (start/end frame ints) as values
                          Example: rangeDict[mobu.FBCamera] = [(100, 200), (500, 600)]
    """
    rangeDict = {}
    camList = [cam for cam in Globals.Cameras if not cam.SystemCamera]
    switcherProp = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index')
    switcherCurve = switcherProp.GetAnimationNode().FCurve
    keyCount = len(switcherCurve.Keys)
    if camList and keyCount:
        for keyIndex, key in enumerate(switcherCurve.Keys):
            # Get Key Start - or default to startFrame
            keyStart = key.Time.GetFrame()
            if keyStart < startFrame:
                keyStart = startFrame
            # Get Key End - or default to endFrame
            keyEnd = endFrame
            if keyIndex + 1 < keyCount:
                nextKey = switcherCurve.Keys[keyIndex + 1].Time.GetFrame()
                if nextKey < endFrame:
                    keyEnd = nextKey - 1
            # Skip Keys Outside our Start/End Frames
            if keyEnd < startFrame or keyStart > endFrame:
                continue
            # Get Camera for Ranges and
            switcherIndex = int(switcherCurve.Evaluate(mobu.FBTime(0, 0, 0, keyStart))) - 1
            camera = camList[switcherIndex]
            if camera in rangeDict.iterkeys():
                rangeDict[camera].append((keyStart, keyEnd))
            else:
                rangeDict[camera] = [(keyStart, keyEnd)]
    return rangeDict


def GetCamPropNodes(camera, propName):
    """
    Gets all animation nodes for a given property name on a camera.
    Arguments:
        camera (mobu.FBCamera): the camera to search for nodes on
        propName (string): the name of the property to look for
    Returns:
        nodeList (list of mobu.FBAnimationNode objects): any found anim nodes
    """
    nodeList = []
    camProp = camera.PropertyList.Find(propName)
    if camProp:
        propNode = camProp.GetAnimationNode()
        nodeList = [subNode for subNode in propNode.Nodes] if len(propNode.Nodes) > 0 else [propNode]
    return nodeList


def PlotSwitcherToExportCamera(TakeList, Silent=False):
    """
    Plots cameras in the switcher to the export camera for the given takes.
    Arguments:
        TakeList (list of mobu.FBTake objects): the takes to plot the export camera on
        Silent (boolean): suppress the results dialog box if true
    """
    start = time.time()
    if not isinstance(TakeList, (list, tuple, mobu.FBPropertyListTake)):
        TakeList = [TakeList]
    plotCount = 0
    resultMessages = []

    # Set Prop Names
    propNames = ["Lcl Translation", "Lcl Rotation", "FieldOfView", "Roll", "Lens", "FOCUS (cm)"]

    # Get Former Mute Sate and Frame Number
    formerStoryMuteState = Globals.Story.Mute
    formerFrameNumber = Globals.System.LocalTime.GetFrame()

    # Force Selection, Story Mute, and Base Layer
    Scene.DeSelectAll()
    Globals.Story.Mute = True
    Globals.System.CurrentTake.SetCurrentLayer(0)

    # Mute Override Layers - fix for rare bug where override layers break cam plot
    layerCount = Globals.System.CurrentTake.GetLayerCount()
    layerModeAdditive = mobu.FBLayerMode.kFBLayerModeAdditive
    for i in xrange(layerCount):
        layer = Globals.System.CurrentTake.GetLayer(i)
        if layer.LayerMode != layerModeAdditive and not layer.Mute:
            layer.Mute = True

    # Disabled reusing old export cam - ensures it's rebuilt correctly

    # Force New Export Cam - and add extra props for GTA
    exportCam = CamLib.MobuCamera.CreateExportCamera()
    isGtaCamera = exportCam.isGtaCamera
    if isGtaCamera:
        propNames.extend(["Near Outer Override", "Near Inner Override", "Far Inner Override", "Far Outer Override"])
    exportCam = exportCam.camera

    # Setup Plot Cam
    plotCam = mobu.FBFindModelByLabelName("PlotCam")
    if isinstance(plotCam, mobu.FBCamera):
        CamLib.MobuCamera(plotCam).DeleteCamera()
    plotCam = CamLib.MobuCamera.CreateRsCamera(specificName="PlotCam").camera

    # Set Export/Plot Cam Interpolation
    for cam in [plotCam, exportCam]:
        for propName in propNames:
            propNode = cam.PropertyList.Find(propName).GetAnimationNode()
            propNode.DefaultInterpolation = mobu.FBInterpolation.kFBInterpolationLinear

    # Update Switcher Cams Before Plot - to ensure latest data goes into game
    switcherProp = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index')
    [cam.SetupCamera() for cam in CamLib.Manager.GetSwitcherCameras() if cam.RequiresUpdate()]

    # MAIN TAKE LOOP
    for take in TakeList:
        Globals.System.CurrentTake = take
        frameRange = (take.LocalTimeSpan.GetStart().GetFrame(), take.LocalTimeSpan.GetStop().GetFrame() + 1)
        switcherCurve = switcherProp.GetAnimationNode().FCurve
        switcherKeys = switcherCurve.Keys

        # Skip Takes with Empty Switchers
        if len(switcherKeys) == 0:
            resultMessages.append("Skipping Take [{0}] - no keys found in camera switcher.\n".format(take.Name))
            continue

        # Clean Switcher Keys
        ClipTools.CleanSwitcherKeys()

        # Clear Export Cam's Old Keys
        for prop in exportCam.PropertyList:
            if prop.IsAnimatable():
                propNode = prop.GetAnimationNode()
                if propNode:
                    for layerIndex in xrange(layerCount):
                        Globals.System.CurrentTake.SetCurrentLayer(layerIndex)
                        if propNode.KeyCount:
                            # Skip GTA5 Overlays - to preserve rare overlay keys
                            if isGtaCamera and "overlay" in prop.Name.lower():
                                continue
                            keyedNodes = propNode.Nodes if propNode.Nodes else [propNode]
                            [node.FCurve.EditClear() for node in keyedNodes]
        Globals.System.CurrentTake.SetCurrentLayer(0)

        # Loop Through Switcher Cams
        rangeDict = GetSwitcherRangeDict(frameRange[0], frameRange[1])
        for sourceCam, editRanges in rangeDict.iteritems():

            # Create Cam Plot Constraint
            camPlotConst = ConstrainCameraForPlot(sourceCam, plotCam, propNames)
            dofFound = DetectDofPass(sourceCam)

            # Loop Through Edit Ranges
            for editRange in editRanges:
                rangeStart = mobu.FBTime(0, 0, 0, editRange[0])
                rangeEnd = mobu.FBTime(0, 0, 0, editRange[1])

                # Plot Down PlotCam
                AnimLib.PlotProperties([plotCam], properties=propNames, children=False, useConstantKeyReducer=False,
                                       rotationFilter=mobu.FBRotationFilter.kFBRotationFilterNone)

                # Override Focus - to zero if dof pass not found
                if not dofFound:
                    plotFocusKeys = plotCam.PropertyList.Find("FOCUS (cm)").GetAnimationNode().FCurve.Keys
                    [setattr(key, "Value", 0.0) for key in plotFocusKeys]

                # Copy Plot Cam Keys to Export Cam
                for propName in propNames:
                    plotCamNodes = GetCamPropNodes(plotCam, propName)
                    exportCamNodes = GetCamPropNodes(exportCam, propName)
                    for nodeIndex, exportCamNode in enumerate(exportCamNodes):
                        plotCamNode = plotCamNodes[nodeIndex]
                        exportCamNode.FCurve.KeyReplaceBy(plotCamNode.FCurve, rangeStart, rangeEnd)

            # Delete Constraint - might could just rebuild it?
            camPlotConst.FBDelete()

        # GTA Dof Plane Plot
        if isGtaCamera:
            PlotGta5Planes()

        # Take Plotted
        plotCount += 1
        resultMessages.append("Take [{}] successfully plotted".format(take.Name))

    # Delete Plot Cam
    CamLib.MobuCamera(plotCam).DeleteCamera()

    # Reset Story Mute and Frame Number
    Globals.Story.Mute = formerStoryMuteState
    Globals.Player.Goto(mobu.FBTime(0, 0, 0, formerFrameNumber))
    Globals.Scene.Evaluate()

    # Show Results Message Box - only if Silent is false
    if not Silent:
        totalTime = time.time() - start
        resultMessages[0:0] = ["Plot Time: {0:02}:{1:02}\n\n".format(int(totalTime / 60), int(totalTime % 60))]
        resultText = "Plotted {} out of {} takes\n\n{}".format(plotCount, len(TakeList), "".join(resultMessages))
        mobu.FBMessageBox("Cam Data Loader", resultText, "Okay")

    return exportCam


def PlotGta5Planes():
    exportCam = mobu.FBFindModelByLabelName("ExportCamera")
    if exportCam:
        planeModels = [child for child in exportCam.Children if "Plane" in child.Name]
        if planeModels:
            mobu.FBSystem().Scene.Evaluate()
            take = mobu.FBSystem().CurrentTake
            frameRange = (take.LocalTimeSpan.GetStart().GetFrame(), take.LocalTimeSpan.GetStop().GetFrame() + 1)
            AnimLib.PlotTimeRange(planeModels, frameRange=frameRange)


def PlotCamsToSpaceBall():
    """
    Plots switcher cams to any movements recorded on the spaceball device.
    """
    # Get SpaceBall Device
    foundDevices = [device for device in mobu.FBSystem().Scene.Devices if device.Name == "SpaceBall"]
    if not foundDevices:
        return
    spaceBall = foundDevices[0]

    # Get SpaceBall Translation / Rotation Nodes
    mainNode = spaceBall.AnimationNodeOutGet()
    transNode = [node for node in mainNode.Nodes if node.Name == "Manipulator Camera Translation"][0].Nodes
    rotNode = [node for node in mainNode.Nodes if node.Name == "Manipulator Camera Rotation"][0].Nodes

    # Loop Through Switcher Cams
    ClipTools.CleanSwitcherKeys()
    take = Globals.System.CurrentTake
    frameRange = (0, take.LocalTimeSpan.GetStop().GetFrame() + 1)
    rangeDict = GetSwitcherRangeDict(frameRange[0], frameRange[1])
    for sourceCam, editRanges in rangeDict.iteritems():

        sourceCam.Translation.Key()

        # Loop Through Edit Ranges
        for editRange in editRanges:
            rangeStart = mobu.FBTime(0, 0, 0, editRange[0])
            rangeEnd = mobu.FBTime(0, 0, 0, editRange[1])

            # Copy Keys From SpaceBall to Cams
            for propName in ["Lcl Translation", "Lcl Rotation"]:
                camNodes = GetCamPropNodes(sourceCam, propName)
                ballNodes = transNode if "Translation" in propName else rotNode
                for nodeIndex, camNode in enumerate(camNodes):
                    ballNode = ballNodes[nodeIndex]
                    camNode.FCurve.KeyReplaceBy(ballNode.FCurve, rangeStart, rangeEnd)

                    # Hacky Workaround - remove first 2 keys to prevent hitching after switching cams
                    if len(camNode.FCurve.Keys) > 2:
                        camNode.FCurve.KeyDeleteByIndexRange(0, 2)


def SimplifyCamKeys(interval=50, startFrame=None, endFrame=None):
    """
    Work in progress tool to help camera guys edit vcam keys.
    """
    curvesToClean = []
    camera = Globals.CameraSwitcher.CurrentCamera
    cleanPropNames = ['Translation (Lcl)', 'Rotation (Lcl)', 'Field Of View']
    for propName in cleanPropNames:
        validProp = camera.PropertyList.Find(propName)
        if validProp:
            parentNode = validProp.GetAnimationNode()
            childCount = len(parentNode.Nodes)
            if childCount:
                for childIndex in xrange(childCount):
                    curvesToClean.append(parentNode.Nodes[childIndex].FCurve)
            else:
                curvesToClean.append(parentNode.FCurve)
    curvesToClean = [curve for curve in curvesToClean if len(curve.Keys) > 1]  # up this to 50?

    for curve in curvesToClean:
        startFrame = startFrame or curve.Keys[0].Time.GetFrame()
        endFrame = endFrame or curve.Keys[len(curve.Keys) - 1].Time.GetFrame()
        currentFrame = startFrame
        while currentFrame < endFrame:
            eraseStart = mobu.FBTime(0, 0, 0, currentFrame + 1)
            eraseStop = mobu.FBTime(0, 0, 0, currentFrame + interval - 1)
            curve.KeyDeleteByTimeRange(eraseStart, eraseStop)
            currentFrame += interval


def DupeCamera(srcCamera):
    """ Copies srcCamera to a duped newCamera. """
    newCamera = CamLib.MobuCamera.CreateRsCamera().camera
    matchItem = Scene.Component.ComponentMatch(srcCamera, newCamera)
    Scene.Component.CopyAllTakes([matchItem])
    return newCamera
