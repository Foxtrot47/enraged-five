"""
Description:
    Main module for creating, updating, and manipulating RS cameras in Mobu.
    Rewritten to streamline the old bloated Lib and add tweaks for our new camera needs.
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""
import os
import re
import pyfbsdk as mobu

from RS import Globals
from RS import Config
from RS.Core.Camera import LensTools, LibGTA5
from RS.Utils import Math
from RS.Utils.Scene import Component as CompTools
from RS.Core.Automation.FrameCapture import CapturePaths


class Manager(object):
    """
    Class for larger camera managing methods that affect multiple cameras.
    """

    @staticmethod
    def GetRsCameras():
        """
        Gets all detected RS cameras in a scene and returns them as MobuCamera objects.
        Returns:
            camList (list): of MobuCamera objects.
        """
        camList = [MobuCamera(camera) for camera in Globals.Cameras]
        camList = [camera for camera in camList if camera.isRsCamera]
        return camList

    @staticmethod
    def GetCameraSwitcher():
        """
        Returns the Mobu camera switcher object.
        """
        return Globals.CameraSwitcher

    @classmethod
    def GetSwitcherCameras(cls):
        """
        Gets a MobuCamera object for every camera found in the switcher.
        Returns:
            (list): of MobuCamera objects.
        """
        Globals.System.CurrentTake.SetCurrentLayer(0)
        camList = [camera for camera in Globals.Cameras
                   if not camera.SystemCamera]

        switcherCams = []
        switcherKeys = cls.GetCameraSwitcher().PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys
        numberOfCameras = len(camList)
        for key in switcherKeys:
            if key.Value > numberOfCameras:
                return []
            camera = camList[int(key.Value) - 1]
            if camera not in switcherCams:
                switcherCams.append(camera)
        return [MobuCamera(camera) for camera in switcherCams]

    @classmethod
    def GetCurrentCameras(cls):
        """
        Gets all the non-default cameras in the scene that are currently being shown in the viewport
        """
        for count in xrange(Globals.Scene.Renderer.GetPaneCount()):
            camera = Globals.Scene.Renderer.GetCameraInPane(count)
            if camera.SystemCamera:
                continue
            yield MobuCamera(camera)

    @classmethod
    def DeleteSwitcherKeys(cls):
        """
        Removes all keys from the camera switcher.
        """
        Globals.System.CurrentTake.SetCurrentLayer(0)
        switcherCurve = cls.GetCameraSwitcher().PropertyList.Find("Camera Index").GetAnimationNode().FCurve
        switcherCurve.EditClear()

    @classmethod
    def DeleteNonSwitcherCams(cls):
        """
        Removes all cameras not found in the camera switcher.
        """
        sceneCams = [cam for cam in Globals.Cameras if not cam.SystemCamera]
        switcherCams = [cam.camera for cam in cls.GetSwitcherCameras()]
        [MobuCamera(cam).DeleteCamera() for cam in sceneCams if cam not in switcherCams and cam.Name != "ExportCamera"
         and not cam.LongName.startswith("MC_")]

    @staticmethod
    def DeleteOldCameraParts():
        """
        Deletes old camera components (but not cam properties) from every camera in the scene.
        Keyword Arguments:
            removeMarkers (bool): if True we also remove old focus marker components.
        """
        macroRelations, parentRelations, camMaterials, camMarkers, camModels = [], [], [], [], []
        for comp in Globals.Components:
            if isinstance(comp, mobu.FBConstraintRelation):
                if re.search("((near|far)_(outer|inner)_plane)|hyperfocal|lens_coc_focal_length", comp.LongName, re.I):
                    if comp not in macroRelations:
                        macroRelations.append(comp)
                        for parent in comp.Parents:
                            if parent not in parentRelations and isinstance(parent, mobu.FBConstraintRelation):
                                parentRelations.append(parent)
            elif isinstance(comp, mobu.FBMaterial):
                if re.search("(screenfade|transparency_planes|farplane|nearplane)", comp.LongName, re.I):
                    if comp not in camMaterials:
                        camMaterials.append(comp)
            elif isinstance(comp, mobu.FBModelMarker):
                if re.search("focus[ _]*marker", comp.LongName, re.I):
                    if comp not in camMarkers:
                        camMarkers.append(comp)
            elif isinstance(comp, mobu.FBModel):
                if re.search("(screenfade|transparency_planes|farplane|nearplane)", comp.LongName, re.I):
                    if comp not in camModels:
                        camModels.append(comp)
        deleteList = parentRelations + macroRelations + camMaterials + camMarkers + camModels

        # TODO: Hacky fix to prevent gta export camera components from being deleted
        deleteList = [comp for comp in deleteList if "exportcamera" not in comp.Name.lower()]
        
        CompTools.Delete(deleteList)
        [comp.FBDelete() for comp in Globals.Components
         if isinstance(comp, mobu.FBFolder) and len(comp.Items) == 0]

    @classmethod
    def UpdateCameras(cls):
        """
        Updates all RS cameras in a scene to the new format.
        """
        # TODO: Disabled clean up here for GTA compatibility - don't think we need this now?
        # cls.DeleteOldCameraParts()
        mobuCams = [cam for cam in cls.GetRsCameras()]
        [cam.SetupCamera() for cam in mobuCams if cam.RequiresUpdate()]
        [cam.SetupFrameMask() for cam in mobuCams]
        Globals.Scene.Evaluate()


class MobuCamera(object):
    """
    Wrapper class for managing individual FBCamera objects and setting up components for our RS cameras.
    """
    _lensPropName = "Lens"
    _focusPropName = "FOCUS (cm)"
    _lensDict = LensTools.Manager.GetLensDict()

    def __init__(self, camera):
        """
        Constructor, requires a Mobu FBCamera object.
        """
        if not isinstance(camera, mobu.FBCamera):
            raise ValueError("MobuCamera initialized without a Mobu FBCamera object.")
        self._camera = camera
        self.Name = camera.Name
        self.LongName = camera.LongName
        self._constraint = None

    @property
    def camera(self):
        """
        Returns the actual FBCamera object if needed outside this class.
        """
        return self._camera

    @property
    def constraint(self):
        """
        Returns the actual FBRelationConstraint object if needed outside this class.
        """
        return self._constraint

    @property
    def isRsCamera(self):
        """
        Detects if a camera is a Rockstar formatted camera.
        """
        if self._camera.SystemCamera or self._camera.PropertyList.Find(self._lensPropName) is None:
            return False
        return True

    @property
    def isGtaCamera(self):
        """
        Detects if a camera is a GTA formatted camera.
        """
        if self._camera.PropertyList.Find("CoC") is not None:
            return True
        return False

    @property
    def dollyMode(self):
        """ Sets the lock state for Translation Y, Rotation X and Rotation Z """
        try:
            # 1 is for the y axis
            return self._camera.PropertyList.Find("Lcl Translation").IsMemberLocked(1)
        except:
            # For cases where the camera has been deleted from the scene
            return False

    @dollyMode.setter
    def dollyMode(self, value):
        """
        Removes any tilt in the camera.
        Does not take into account offsets caused by the roll attribute.

        Arguments:
            value (bool): lock Translation Y, Rotation X and Rotation Z
        """
        matrix = mobu.FBMatrix()
        self._camera.GetMatrix(matrix)
        x = [matrix[0], matrix[1], matrix[2]]
        y = [0, 1, 0]
        currentZ = [matrix[0], matrix[1], matrix[2]]
        # Get a Z Vector Perpendicular to the X where why is looking up
        z = Math.CrossProduct(x, y)

        if Math.AngleBetweenVectors(currentZ, z) > 90:
            z = [-1 * value for value in  z]
        # Solve for a new X vector that is orthonormal to the new y & z axis
        x = Math.CrossProduct(y, z)

        matrix[0], matrix[1], matrix[2] = x
        matrix[4], matrix[5], matrix[6] = y
        matrix[8], matrix[9], matrix[10] = z

        # Do vector math to resolve a flat orientation
        self._camera.SetMatrix(matrix)

    @classmethod
    def CreateRsCamera(cls, specificName=None):
        """
        Creates a new RS camera and sets up all its options and parts.
        Keyword Arguments:
            specificName (string): the name to use for this new cam (deletes conflicting cam if found).
        Returns:
            newCam (MobuCamera): the newly created cam as a MobuCamera object.
        """
        # Detect Project
        fbxPath = mobu.FBApplication().FBXFileName
        capPaths = CapturePaths.FastPath(fbxPath)
        project = capPaths.project

        # Set Camera Name - by detecting number or by using specific name
        if not specificName:
            camNumber = cls._getNewCamNumber()
            camName = "RS_Camera_{}".format(camNumber)
            if project == "GTA5":
                camName = "GTA5_Camera_{}".format(camNumber)
        else:
            camName = specificName
            # Force GTA Name for GTA Cams
            if project == "GTA5" and camName.lower().startswith("rs_camera_"):
                camName = "GTA5_" + camName[3:]
            matchedCamera = mobu.FBFindModelByLabelName(specificName)
            if matchedCamera:
                # Conflicting Camera Found - delete unless on GTA5 and valid
                oldCam = MobuCamera(matchedCamera)
                if project == "GTA5" and camName == "ExportCamera" and oldCam.isGtaCamera:
                    return oldCam
                oldCam.DeleteCamera()

        # Select Base Layer - if not already selected
        if Globals.System.CurrentTake.GetCurrentLayer() > 0:
            Globals.System.CurrentTake.SetCurrentLayer(0)

        # GTA Logic
        if project == "GTA5":
            CamLibGTA5 = LibGTA5._camFunctions()
            if camName == "ExportCamera":
                mbCam = CamLibGTA5._CreateBaseCamera(ExportCamera=True)
            else:
                mbCam = CamLibGTA5._CreateBaseCamera(specificName=camName)
            mbCam.PropertyList.Find("Roll").SetAnimated(True)
            newCam = MobuCamera(mbCam)

        else:
            # Create Camera - and set options
            newCam = MobuCamera(mobu.FBCamera(camName))
            newCam.SetupCamera()
            newCam._setNewCamOptions()

        return newCam

    @classmethod
    def CreateExportCamera(cls):
        """
        Creates a new export camera (removes old export camera if found).
        Returns:
            expCam (MobuCamera): the newly created export cam as a MobuCamera object.
        """
        expCam = cls.CreateRsCamera(specificName="ExportCamera")
        expCam._camera.FrameColor = mobu.FBColor(1, 0, 0)
        return expCam

    def SwitchViewToCamera(self):
        """
        Switches the current viewer window to this MobuCamera.
        """
        Globals.Scene.Renderer.SetCameraInPane(self._camera, 0)

    def DeleteCamera(self):
        """
        Deletes this MobuCamera and all of it's parts from the scene.
        """
        # Detect Project
        fbxPath = mobu.FBApplication().FBXFileName
        capPaths = CapturePaths.FastPath(fbxPath)
        project = capPaths.project

        # GTA Cam Delete
        if project == "GTA5":
            CamUpGTA5 = LibGTA5._camUpdate()
            CamUpGTA5.DeleteCamera(self.camera, RemoveCam=False)

        # RS Cam Delete
        else:
            # TODO: I want to add the group component delete here (from CamUtils.DeleteInvalidCams)
            # just not sure if that would possibly break things (moving cutscene)? Need to ask Geoff Smith
            # Delete Focus Relation
            relationName = "{}_FocusRelation".format(self._camera.Name)
            focusRelation = [const for const in Globals.Constraints if const.Name == relationName]
            if focusRelation:
                focusRelation[0].Active = False
                focusRelation[0].FBDelete()

            # Delete Focus Marker
            markerName = "{}_FocusMarker".format(self._camera.Name)
            focusMarker = mobu.FBFindModelByLabelName(markerName)
            if type(focusMarker) == mobu.FBModelMarker:
                focusMarker.FBDelete()
        self._camera.FBDelete()

    def RequiresUpdate(self):
        """
        Various checks to determine if this camera needs an update.
        Returns:
            (bool): True if an update is needed, False otherwise
        """
        # Skip Detection for Non RS Cameras
        if self.isRsCamera is False:
            return False

        # Skip GTA Cams - for now, maybe hook up the old LibGTA5 eventually?
        if self.isGtaCamera:
            return False

        # TODO: Temporarily disabled scale check, as something is altering them
        # Validate Scale
        # if self._camera.Scaling.Data != mobu.FBVector3d(1, 1, 1):
        #     return True

        # Validate Props
        oldProps = self._getInvalidCameraProps()
        if oldProps:
            return True
        lensProp = self._camera.PropertyList.Find(self._lensPropName)
        focusProp = self._camera.PropertyList.Find(self._focusPropName)
        if not lensProp or not focusProp:
            return True
        lensEnum = lensProp.GetEnumStringList(True)
        oldLensNames = [lensEnum[index] for index in xrange(len(lensEnum))]
        # lensAttr = "MetaName" if "exportcamera" in self.Name.lower() else "Name"
        # newLensNames = [getattr(self._lensDict[index], lensAttr) for index in sorted(self._lensDict.iterkeys())]
        newLensNames = [self._lensDict[index].MetaName for index in sorted(self._lensDict.iterkeys())]
        if oldLensNames != newLensNames:
            return True

        # Validate Marker
        markerName = "{}_FocusMarker".format(self._camera.Name)
        validMarkers = [child for child in self._camera.Children
                        if type(child) == mobu.FBModelMarker and child.Name == markerName]
        if not validMarkers or self._validateFocusMarker(validMarkers[0]) == False:
            return True

        # Validate Relation
        relationName = "{}_CamRelation".format(self._camera.Name)
        validRelations = [const for const in Globals.Constraints
                          if self._camera in const.Parents and const.Name == relationName]
        if not validRelations or self._validateCamRelation(validRelations[0]) == False:
            return True

        # Validate Groups and Visibility
        camGroups = [group for group in Globals.Groups if group.Contains(self._camera)]
        camSets = [mbSet for mbSet in Globals.Sets if mbSet.Contains(self._camera)]
        if camGroups or camSets or self._camera.IsVisible is False:
            return True

        return False

    def SetupCamera(self):
        """
        Main method for verifying and setting up RS camera options and parts.
        """
        self._updateOldName()
        self._setDefaultCamOptions()
        self._setupLensProp()
        self._setupFocusProp()
        self._setupFocusMarker()
        self._setupCamRelation()
        self._deleteOldCameraProps()
        self._removeCamFromGroups()

    @classmethod
    def _getNewCamNumber(cls):
        """
        Internal method - detects RS cameras and returns the lowest available number.
        Returns:
            newNumber (int): the lowest available RS camera number.
        """
        newNumber = 1
        camNumbers = []
        for camera in Manager.GetRsCameras():
            nameMatch = re.match("^(?:gta5|r[s\*][ _]*)(?:camera)[ _]*(\d+)", camera.Name, re.I)
            if nameMatch:
                camNumbers.append(int(nameMatch.group(1)))
        while newNumber in camNumbers:
            newNumber += 1
        return newNumber

    def _updateOldName(self):
        """
        Internal method - converts old camera names to the new format.
        """
        # Remove Namespaces from Cameras
        if ":" in self._camera.LongName:
            self._camera.LongName = "::{0}".format(self._camera.Name)

        # Update Old Camera Names
        nameMatch = re.match("^(?:gta5|r\*)(?:camera)[ _]*(\d+)*[ _]*(.*)", self._camera.Name, re.I)
        if nameMatch:
            # newName = "RS_Camera_{}".format(int(nameMatch.group(1)))
            camNumber = nameMatch.group(1) if nameMatch.group(1) else self._getNewCamNumber()
            extraText = "_{}".format(nameMatch.group(2).replace(" ", "_")) if nameMatch.group(2) else ""
            newName = "RS_Camera_{0}{1}".format(camNumber, extraText)

            # Blocking Name Found - get new cam number instead
            blockingCamera = mobu.FBFindModelByLabelName(newName)
            if blockingCamera:
                camNumber = self._getNewCamNumber()
                newName = "RS_Camera_{0}{1}".format(camNumber, extraText)
            self._camera.Name = newName

        # Update Object's Name Property
        self.Name = self._camera.Name
        self.LongName = self._camera.LongName

    def _getInvalidCameraProps(self):
        """
        Internal method - detects any invalid camera properties.
        """
        validPropNames = (self._lensPropName, self._focusPropName)
        propHideFlag = mobu.FBPropertyFlag.kFBPropertyFlagHideProperty
        invalidProps = [prop for prop in self._camera.PropertyList if prop.IsUserProperty() and
                        prop.GetPropertyFlag(propHideFlag) == False and prop.Name not in validPropNames]
        return invalidProps

    def _deleteOldCameraProps(self):
        """
        Internal method - removes old camera properties from this camera.
        """
        oldProps = self._getInvalidCameraProps()
        [self._camera.PropertyRemove(prop) for prop in oldProps]

    def _removeCamFromGroups(self):
        """
        Internal method - removes this camera from any groups or sets that might be overriding its visibility.
        """
        [group.DisconnectSrc(self._camera) for group in Globals.Groups if group.Contains(self._camera)]
        [mbSet.DisconnectSrc(self._camera) for mbSet in Globals.Sets if mbSet.Contains(self._camera)]

    def _setDefaultCamOptions(self):
        """
        Internal method - sets the default camera options used by all RS cameras.
        """
        self._camera.Show = True
        self._camera.Visibility = True
        self._camera.VisibilityInheritance = False
        self._camera.ResolutionMode = mobu.FBCameraResolutionMode.kFBResolutionCustom
        self._camera.FrameSizeMode = mobu.FBCameraFrameSizeMode.kFBFrameSizeFixedRatio
        self._camera.ResolutionWidth = 1.77777
        self._camera.PixelAspectRatio = 1.0
        self._camera.UseFrameColor = True
        self._camera.SafeAreaMode = mobu.FBCameraSafeAreaMode.kFBSafeAreaSquare
        self._camera.ViewDisplaySafeArea = False
        self._camera.BackGroundImageFit = False
        self._camera.BackGroundPlaneDistance = 100
        self._camera.ForeGroundMaterialThreshold = 50
        self._camera.ForeGroundPlaneDistanceMode = mobu.FBCameraDistanceMode.kFBDistModeAbsoluteFromCamera
        self._camera.ViewForeGroundPlaneMode = mobu.FBCameraViewPlaneMode.kFBViewPlaneDisabled
        self._camera.FilmBackType = mobu.FBCameraFilmBackType.kFBFilmBackCustom
        self._camera.ViewShowGrid = False
        self._camera.FieldOfView.SetAnimated(True)
        self._camera.Roll.SetAnimated(True)
        self._camera.NearPlaneDistance = 10.0
        self._camera.FarPlaneDistance = 999999.99
        scaleNodes = self._camera.Scaling.GetAnimationNode()
        if scaleNodes and scaleNodes.KeyCount:
            [node.FCurve.EditClear() for node in scaleNodes.Nodes]
        self._camera.Scaling = mobu.FBVector3d(1, 1, 1)
        self._camera.Scaling.SetAnimated(False)

    def _setNewCamOptions(self):
        """
        Internal method - sets extra options needed when creating a new RS camera.
        """
        self._camera.FieldOfView = 37.0
        self._camera.FrameColor = mobu.FBColor(0, 0, 0)
        self._camera.QuaternionInterpolate = True
        currentCamera = mobu.FBSystem().Renderer.GetCameraInPane(0)
        if currentCamera:
            self._camera.Translation.Data = mobu.FBVector3d(currentCamera.Translation)
            self._camera.Rotation.Data = mobu.FBVector3d(currentCamera.Rotation)
            self.SetupFrameMask()

    def _setupLensProp(self):
        """
        Internal method - creates new lens prop or validates old lens prop.
        """
        lensProp = self._camera.PropertyList.Find(self._lensPropName)
        # lensAttr = "MetaName" if "exportcamera" in self.Name.lower() else "Name"
        # newLensNames = [getattr(self._lensDict[index], lensAttr) for index in sorted(self._lensDict.iterkeys())]
        newLensNames = [self._lensDict[index].MetaName for index in sorted(self._lensDict.iterkeys())]
        if not lensProp:
            # Create New Lens Prop
            lensProp = self._camera.PropertyCreate(self._lensPropName, mobu.FBPropertyType.kFBPT_enum,
                                                   "Enum", True, True, None)
            lensEnum = lensProp.GetEnumStringList(True)
            [lensEnum.Add(lensName) for lensName in newLensNames]
            lensProp.NotifyEnumStringListChanged()
            lensProp.Data = len(self._lensDict) - 1
            lensProp.SetAnimated(True)

        else:
            # Get Old Name and Fov
            lensEnum = lensProp.GetEnumStringList(True)
            oldName = lensEnum.GetAt(lensProp)
            oldFov = self._camera.PropertyList.Find("FieldOfView").Data

            # Update Lens Names
            oldLensNames = [lensEnum[index] for index in xrange(len(lensEnum))]
            if oldLensNames != newLensNames:
                for index in xrange(len(newLensNames)):
                    if index > len(lensEnum) - 1:
                        lensEnum.Add(newLensNames[index])
                    elif lensEnum[index] != newLensNames[index]:
                        lensEnum[index] = newLensNames[index]
                while len(lensEnum) > len(newLensNames):
                    lensEnum.Remove(lensEnum[-1])

            # Update Value and Keys
            lensProp.SetAnimated(True)
            lensProp.GetAnimationNode().FCurve.EditClear()
            if "zoom" in oldName.lower():
                lensProp.Data = len(lensEnum) - 1
            else:
                newIndex = LensTools.GetNearestLensIndex(oldFov)
                lensProp.Data = newIndex

    def _setupFocusProp(self):
        """
        Internal method - creates new lens prop if one isn't found.
        """
        focusProp = self._camera.PropertyList.Find(self._focusPropName)
        if not focusProp:
            focusProp = self._camera.PropertyList.Find("FOCUS")
            if focusProp:
                focusProp.Name = self._focusPropName
            else:
                focusProp = self._camera.PropertyCreate(self._focusPropName, mobu.FBPropertyType.kFBPT_double,
                                                        "Number", True, True, None)
                focusProp.Data = 150.0
        focusProp.SetAnimated(True)

    def _deleteBlockerPart(self, partType, validName):
        """
        Internal method - checks if a part (we're trying to create elsewhere) already exists, and deletes it if found.
        Arguments:
            partType (mobu part type): the type of Mobu part we're checking.
            validName (string): the name of the part we're checking.
        """
        blocker = None
        if partType in [mobu.FBModelMarker, mobu.FBModel, mobu.FBModelNull]:
            blocker = mobu.FBFindModelByLabelName(validName)
        elif partType == mobu.FBConstraintRelation:
            blockers = [const for const in Globals.Constraints if const.Name == validName]
            blocker = blockers[0] if blockers else None
        if blocker:
            blocker.FBDelete()

    def _cleanOldParts(self, oldParts, validName):
        """
        Internal method - keeps (or renames) one valid old camera part and deletes any extras or dupes.
        Arguments:
            oldParts (list): of mobu camera parts (cannot be empty list).
            validName (string): the valid name to search for / rename to (cannot be empty string).
        Returns:
            validPart (Mobu component): the valid part we intend to keep
        """
        validParts = [part for part in oldParts if part.Name == validName]
        if validParts:
            validPart = validParts[0]
        else:
            validPart = oldParts[0]
            self._deleteBlockerPart(type(validPart), validName)
            validPart.Name = validName
        oldParts.remove(validPart)
        for extraPart in oldParts:
            if type(extraPart) == mobu.FBConstraintRelation:
                extraPart.Active = False
            extraPart.FBDelete()
        return validPart

    @staticmethod
    def _validateFocusMarker(focusMarker):
        """
        Internal method - checks that focus marker properties are valid (free of legacy cam issue)
        """
        if any((focusMarker.Size < 500.0, focusMarker.Scaling.Data != mobu.FBVector3d(1, 1, 1),
                focusMarker.Color != mobu.FBColor(0, 1, 0))):
            return False
        return True

    def _setupFocusMarker(self):
        """
        Internal method - fixes up any old focus markers or creates a new one.
        """
        # DetectMarkers
        markerName = "{}_FocusMarker".format(self._camera.LongName)
        oldMarkers = [child for child in self._camera.Children
                      if type(child) == mobu.FBModelMarker and re.search("focus[ _]*marker", child.Name, re.I)]
        if oldMarkers:
            # Setup Old Marker - updating name and props if needed
            focusMarker = self._cleanOldParts(oldMarkers, markerName)
            validMarker = self._validateFocusMarker(focusMarker)
            if not validMarker:
                focusMarker.Size = 500.0
                focusMarker.Color = mobu.FBColor(0, 1, 0)
                focusMarker.Scaling = mobu.FBVector3d(1, 1, 1)

        else:
            # Create New Marker - set default options
            self._deleteBlockerPart(mobu.FBModelMarker, markerName)
            focusMarker = mobu.FBModelMarker(markerName)
            focusMarker.Size = 500.0
            focusMarker.Color = mobu.FBColor(0, 1, 0)
            focusMarker.Look = mobu.FBMarkerLook.kFBMarkerLookHardCross

        # Attach Marker to Cam
        focusMarker.Parent = self._camera

    def _validateCamRelation(self, camRelation):
        """
        Checks a camera relation's lens values to see if they need updating.
        Arguments:
            camRelation (Mobu relation const): the relation to check lens values in.
        Returns:
            (bool): True for a valid relation, False otherwise
        """
        # Check for Lens Curve Box
        oldLensBox = [box for box in camRelation.Boxes if "FCurve Number (%)" in box.Name]
        if not oldLensBox:
            return False

        # Validate Curve Keys - the the count and value are within tolerance
        oldLensKeys = oldLensBox[0].AnimationNodeOutGet().Nodes[0].FCurve.Keys
        oldLensValues = [key.Value for key in oldLensKeys if key.Value != 0.0]
        newLensValues = [self._lensDict[index].FOV for index in sorted(self._lensDict.iterkeys())]
        newLensValues = [fovValue for fovValue in newLensValues if fovValue != 0.0]
        if len(oldLensValues) != len(newLensValues):
            return False
        for index, value in enumerate(newLensValues):
            if abs(oldLensValues[index] - newLensValues[index]) > 0.001:
                return False
        return True

    def _setupCamRelation(self):
        """
        Internal method - validates any old cam relations or creates a new one.
        Note: Requires focusMarker, focusProp, and lensProp to have been setup previously.
        """
        marker = [child for child in self._camera.Children if "FocusMarker" in child.Name][0]
        relationName = "{}_CamRelation".format(self._camera.LongName)

        # Detect Existing Relations - updating name and removing extras
        searchText = ".*(cam|camera (\d+ )*)relation"
        oldRelations = [const for const in Globals.Constraints
                        if self._camera in const.Parents and re.match(searchText, const.LongName, re.I)]
        if oldRelations:
            oldRelation = self._cleanOldParts(oldRelations, relationName)
            validRelation = self._validateCamRelation(oldRelation)
            if validRelation:
                self._constraint = oldRelation
                return

        # Create New Relation - and box components
        self._deleteBlockerPart(mobu.FBConstraintRelation, relationName)
        camRelation = mobu.FBConstraintRelation(relationName)
        camSourceBox = camRelation.SetAsSource(self._camera)
        convertBox = camRelation.CreateFunctionBox("Converters", "Number to Vector")
        markerBox = camRelation.ConstrainObject(marker)
        fcurveBox = camRelation.CreateFunctionBox("Other", "FCurve Number (%)")
        checkNumberBox = camRelation.CreateFunctionBox("Number", "Is Identical (a == b)")
        lensConditionBox = camRelation.CreateFunctionBox("Number", "IF Cond Then A Else B")
        camDestBox = camRelation.ConstrainObject(self._camera)

        # Position Boxes (to prevent overlapping)
        camRelation.SetBoxPosition(camSourceBox, 20, 120)
        camRelation.SetBoxPosition(convertBox, 330, 25)
        camRelation.SetBoxPosition(markerBox, 850, 10)
        camRelation.SetBoxPosition(fcurveBox, 285, 164)
        camRelation.SetBoxPosition(checkNumberBox, 320, 235)
        camRelation.SetBoxPosition(lensConditionBox, 630, 120)
        camRelation.SetBoxPosition(camDestBox, 900, 120)

        # Set Data for Marker and Check Number Boxes
        markerBox.UseGlobalTransforms = False
        # checkNumberBox.AnimationNodeInGet().Nodes[1].WriteData([6.0])

        # Set Data for FCurve Box
        fcurveValueNode = fcurveBox.AnimationNodeOutGet().Nodes[0]
        lensCurve = fcurveValueNode.FCurve
        lensCurve.EditBegin()
        lensCurve.EditClear()

        # Create Curve Data From Lens List
        lensCurveData = {index: lens.FOV for index, lens in self._lensDict.iteritems()}
        lensCurveData[100] = 0.0
        for frame, value in lensCurveData.iteritems():
            lensCurve.KeyAdd(mobu.FBTime(0, 0, 0, frame), value)
            key = lensCurve.Keys[-1]
            key.Interpolation = mobu.FBInterpolation.kFBInterpolationLinear
        lensCurve.EditEnd()
        zoomLensIndex = len(self._lensDict) - 1
        checkNumberBox.AnimationNodeInGet().Nodes[1].WriteData([zoomLensIndex])

        # Connect Focus Nodes
        xVectorNode = convertBox.AnimationNodeInGet().Nodes[0]
        vectorNode = convertBox.AnimationNodeOutGet().Nodes[0]
        focusSrcNode = [node for node in camSourceBox.AnimationNodeOutGet().Nodes if node.Name == self._focusPropName][
            0]
        markerTransNode = [node for node in markerBox.AnimationNodeInGet().Nodes if node.Name == "Lcl Translation"][0]
        mobu.FBConnect(focusSrcNode, xVectorNode)
        mobu.FBConnect(vectorNode, markerTransNode)

        # Connect Lens Nodes
        fcurvePositionNode = fcurveBox.AnimationNodeInGet().Nodes[0]
        checkNumberANode = checkNumberBox.AnimationNodeInGet().Nodes[0]
        checkNumberResultNode = checkNumberBox.AnimationNodeOutGet().Nodes[0]
        lensConditionBoolNode = lensConditionBox.AnimationNodeInGet().Nodes[0]
        lensConditionANode = lensConditionBox.AnimationNodeInGet().Nodes[1]
        lensConditionBNode = lensConditionBox.AnimationNodeInGet().Nodes[2]
        lensConditionResultNode = lensConditionBox.AnimationNodeOutGet().Nodes[0]
        fovSrcNode = [node for node in camSourceBox.AnimationNodeOutGet().Nodes if node.Name == "FieldOfView"][0]
        lensSrcNode = [node for node in camSourceBox.AnimationNodeOutGet().Nodes if node.Name == self._lensPropName][0]
        fovDestNode = [node for node in camDestBox.AnimationNodeInGet().Nodes if node.Name == "FieldOfView"][0]
        mobu.FBConnect(lensSrcNode, fcurvePositionNode)
        mobu.FBConnect(fovSrcNode, lensConditionANode)
        mobu.FBConnect(lensSrcNode, checkNumberANode)
        mobu.FBConnect(checkNumberResultNode, lensConditionBoolNode)
        mobu.FBConnect(fcurveValueNode, lensConditionBNode)
        mobu.FBConnect(lensConditionResultNode, fovDestNode)

        # Activate Constraint
        camRelation.Active = True
        self._constraint = camRelation

    def SetInterestDistance(self, distance=100.0):
        """
        Moves the camera's interest point to a set distance. Most the time we don't
        want to use this - using the current camera's interest point usually works better.
        Arguments:
            distance (float): the distance to the interest point (in centimeters).
        """
        # Create Interest Vector
        camMatrix = mobu.FBMatrix()
        self._camera.GetMatrix(camMatrix)
        directionVector = mobu.FBVector3d(camMatrix[0], camMatrix[1], camMatrix[2])
        positionVector = mobu.FBVector3d(camMatrix[12], camMatrix[13], camMatrix[14])
        intVector = directionVector * distance + positionVector

        # Create Interest Cube and Set to Cam
        intCube = mobu.FBModelCube("CamIntCube")
        intCube.Translation = intVector
        self._camera.Interest = intCube

        # Update Current Cam, Render Frame, Restore Previous Cam
        renderer = Globals.System.Renderer
        usingSwitcher = renderer.IsCameraSwitcherInPane(0)
        previousCam = renderer.GetCameraInPane(0)
        renderer.SetCameraInPane(self._camera, 0)
        renderer.Render()
        if usingSwitcher:
            renderer.SetCameraSwitcherInPane(0, True)
        else:
            renderer.SetCameraInPane(previousCam, 0)

        # Delete Interest Cube
        intCube.FBDelete()

    def SetupFrameMask(self):
        # Early GTA Exit - only need frame mask in RDR3
        project = os.environ["RS_PROJECT"].lower()
        fbxPath = mobu.FBApplication().FBXFileName.lower()
        pathParts = fbxPath.split(os.path.sep)
        if "gta5_dlc" in pathParts:
            # todo: might need to remove invalid masks here at some point?
            return

        # Set Mask Name and Path
        maskName = "CamFrameMask"
        maskRoot = os.path.join(Config.Script.Path.ToolImages, "camera", "masks")
        imageName = "camFrame235.png"
        if (len(pathParts) > 1 and pathParts[1] == "americas") or (len(pathParts) < 2 and project == "americas"):
            imageName = "camFrame200.png"
        maskImagePath = os.path.join(maskRoot, imageName)

        # Get Texture - use existing or create new
        matchedTextures = [tex for tex in mobu.FBSystem().Scene.Textures if tex.Name == maskName]
        maskTexture = mobu.FBTexture(maskName) if not matchedTextures else matchedTextures[0]

        # Get Image - use existing or create new
        matchedImages = [img for img in mobu.FBSystem().Scene.VideoClips if img.Name.lower().startswith("camframe")]
        maskImage = mobu.FBVideoClipImage(maskName) if not matchedImages else matchedImages[0]

        # Set Texture and Image Options
        maskImage.Name = maskName
        maskImage.Filename = maskImagePath
        maskTexture.Video = maskImage

        # Set Camera Options
        self._camera.ForeGroundTexture = maskTexture
        self._camera.ForeGroundAlpha = 0.75
        self._camera.ViewDisplaySafeArea = False
        self._camera.ViewForeGroundPlaneMode = mobu.FBCameraViewPlaneMode.kFBViewPlaneDisabled

        # Cleanup Old / Invalid Components
        invalidComps = [comp for comp in maskTexture.Components if comp.Name != maskName]
        [maskTexture.DisconnectSrc(comp) for comp in invalidComps]
        deleteList = []
        [deleteList.append(tex) for tex in mobu.FBSystem().Scene.Textures if tex.Name.lower().startswith("camframe")]
        [deleteList.append(img) for img in mobu.FBSystem().Scene.VideoClips if img.Name.lower().startswith("camframe")]
        [comp.FBDelete() for comp in deleteList if comp not in [maskTexture, maskImage]]


def ShowGtaPlanes(show=False):
    currentCam = mobu.FBSystem().Renderer.GetCameraInPane(0)
    camList = [cam for cam in mobu.FBSystem().Scene.Cameras if cam.SystemCamera == False]
    for cam in camList:
        for camChild in cam.Children:
            if cam == currentCam and 'screenfade' not in camChild.Name.lower():
                camChild.PropertyList.Find("Visibility").Data = show
                camChild.Show = show
            else:
                camChild.PropertyList.Find("Visibility").Data = False
                camChild.Show = False
