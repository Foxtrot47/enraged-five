"""

Shake Cam Lib V 1.0
---------------

Revision History
----------------
This is a copy of Shaker.py but will work natively without requireing UI and can run on FBCamera Itemss

"""
from pyfbsdk import *

import os

# New modules to support handling takes/aniamtions/nodes
import RS.Core
import RS.Utils.Scene.Model
import RS.Utils.Scene.AnimationNode
import RS.Utils.Scene.Property
import RS.Utils.Scene.Take

import RS.Globals as glo

import RS.Perforce
import RS.Config


class FrameDefInfo:
    """
        Frame Info Def
        We now will record all translation data too
        Changing this data will mean updating the parser to cgheck what export data is their
    """
    frame = 0,
    TransX = 0,
    TransY = 0,
    TransZ = 0,
    RotX = 0,
    RotY = 0,
    RotZ = 0,
    FOV = 0


class ShakerInterFaceMap(object):
    def __init__(self):
        self.transX = self.transY = self.transZ = False
        self.rotX = self.rotY = self.rotZ = False
        self.fov = False

        self.Clear()

    def Clear(self):
        self.transX = False
        self.transY = False
        self.transZ = False

        self.rotX = False
        self.rotY = False
        self.rotZ = False

        self.fov = False


class HandShake:
    """
        TODO: Set up shake data and parse in XML to define descrtiption, lenght, name etc
              Add beter UI visualerizer
              Add A shit load of penguins
    """
    def __init__(self):
        """
            Constructor
        """
        self.path = os.path.abspath("{0}/etc/CameraData/".format(RS.Config.Tool.Path.TechArt))
        self.CameraShake = []
        self.Light = "Light Handheld"
        self.Medium = "MEdium Handheld" # Not sure if spelling mistake or not, either way its REALLLLLLLLLY annoying!
        self.Heavy = "Heavy Handheld"

        self.CameraRange = dict()

        self.lExportCamera = FBFindModelByLabelName("ExportCamera")
        self.lSwitcherCamera = None

        self.gCamKeys = []

        self.ShakeLayer = None

        # Used to store our frame start and end Ranges
        self.FrameStart = None
        self.FrameEnd = None

        # Used to disbale local Transforms
        self.ExportLocal = True

        self.lCameraList = []
        self.Shakephase = 1 # USed for phase start

        self.lSystem = FBSystem()

        # Default load light Shake, 1st nam ein our list

        self.readShakeFile(os.path.join(self.path, "Light.csf"))
        self._propertiesMapping = ShakerInterFaceMap()
    """
    Private
    """
    def __setTakebyName(self, takename):
        """
            Go through our takes, if we find out take name then set it to our name
        """
        for lTakeIdx in range(len(self.lSystem.Scene.Takes)):
            if self.lSystem.Scene.Takes[lTakeIdx].Name == takename:
                self.lSystem.CurrentTake = self.lSystem.Scene.Takes[lTakeIdx]
                return()

        FBMessageBox("Information", "Failed to find a take which matches the selected camera name (the expected setup).\nUsing the current take as source for animation.", "OK")

    def __returnCamLength(self, cam):
        """
            WHats this do I hear you ask, well it checks all the translation fcurves and
            will return the largest number so we only export the len of the camera animation
            Also need to go through each layer as we don't know which layer has the aniamtion
            Might in future check all vlaus are same len as this migth cause mismatch issues
        """
        maxlen = 0
        for idx in range(self.lSystem.CurrentTake.GetLayerCount()): # Go through each layer
            self.lSystem.CurrentTake.SetCurrentLayer(idx)
            for axis in range(3):
                AnimNode = cam.Rotation.GetAnimationNode()
                if AnimNode:
                    thisLen = AnimNode.Nodes[axis].FCurve
                    if len(thisLen.Keys) > maxlen:
                        maxlen = len(thisLen.Keys)
                AnimNode = cam.Translation.GetAnimationNode()
                if AnimNode:
                    thisLen = cam.Translation.GetAnimationNode().Nodes[axis].FCurve
                    if len(thisLen.Keys) > maxlen:
                        maxlen = len(thisLen.Keys)
        return maxlen

    def __ExportCamShake(self, camera):
        """
            Get the camera
            Check the Take
            Make sure camera is at 0,0,0 and export data
            read data out and write to csf file,
            Cam if exists has already been checked earlier
        """
        # Set our take name if we have one to the camera name
        if not RS.Utils.Scene.Take.SetCurrentTakeByName(camera.Name):
            FBMessageBox("Information", "Failed to find a take which matches the selected camera name (the expected setup).\nUsing the current take as source for animation.", "OK")

        # Record camera information
        self.RecordSelectedCamera(camera)

    def __DeleteSelectedShake(self, camera):
        if camera != None:
            if self.FindLayer("ShakeCam"):
                self.SetLayer("ShakeCam")
                # Get the Cameras Fcurves and delete the shit out of them
                for axis in range(3):
                    camera.Rotation.GetAnimationNode().Nodes[axis].FCurve.EditClear()
                    camera.Translation.GetAnimationNode().Nodes[axis].FCurve.EditClear()
                camera.PropertyList.Find("Field Of View").GetAnimationNode().FCurve.EditClear()


    def run(self):
        self.main()

    @staticmethod
    def _saltToken(token, index):
        """
            Method will get the index from the array or return 0, but will not error
        """
        try:
            return token[index]
        except IndexError:
            return 0

    """
    Public
    """
    def readShakeFile(self, pFilename):
        """
            Load up and read in out costume Shake Data
            Maybe at one point we could seralize this, but whats the point it will piss everyone off
        """
        self.CameraShake = []
        if not os.path.exists(pFilename):
            return
        f = open(pFilename, 'r')
        for line in iter(f):
            FrameInfo = FrameDefInfo()
            tokens = line.split(',')
            FrameInfo.frame = int(self._saltToken(tokens, 0))
            if len(tokens) == 5:
                FrameInfo.TransX = 0.0
                FrameInfo.TransY = 0.0
                FrameInfo.TransZ = float(self._saltToken(tokens, 1))
                FrameInfo.RotX = float(self._saltToken(tokens, 2))
                FrameInfo.RotY = float(self._saltToken(tokens, 3))
                FrameInfo.RotZ = float(self._saltToken(tokens, 4))

            else:
                FrameInfo.TransX = float(self._saltToken(tokens, 1))
                FrameInfo.TransY = float(self._saltToken(tokens, 2))
                FrameInfo.TransZ = float(self._saltToken(tokens, 3))
                FrameInfo.RotX = float(self._saltToken(tokens, 4))
                FrameInfo.RotY = float(self._saltToken(tokens, 5))
                FrameInfo.RotZ = float(self._saltToken(tokens, 6))
                FrameInfo.FOV = float(self._saltToken(tokens, 7))
            self.CameraShake.append(FrameInfo)
        f.close()
        return

    def writeShakeFile(self, pFilename, rootRelativePath=True):
        """
            Write out our Shake Data, pretty straight forward
        """
        # Make File Writable incase its not
        newFilePath = os.path.join(self.path, pFilename)

        # Check if the path is root relative from the camera data folder or if its a file on disk
        if not rootRelativePath:
            newFilePath = pFilename

        if os.path.exists(newFilePath):
            os.chmod(newFilePath, stat.S_IWRITE)

        print 'Saving Shake file to :{0}.csf'.format(newFilePath)

        f = open("{0}.csf".format(newFilePath), 'w')
        for fDef in self.CameraShake:
            f.write(str(fDef.frame) + ","
                    + str(fDef.TransX) + ","
                    + str(fDef.TransY) + ","
                    + str(fDef.TransZ) + ","
                    + str(fDef.RotX) + ","
                    + str(fDef.RotY) + ","
                    + str(fDef.RotZ) + ","
                    + str(fDef.FOV) + "\n")
        f.close()

    def blendAnimationCurves(self, pCamera):
        """
            So this will automactially make the camera loop: SCARY!!
            We take start and end and take the difference and divide by 2
            Not used yet
        """
        fstart_TransX = pCamera.Translation.GetAnimationNode().Nodes[0].FCurve.Evaluate(FBTime(0, 0, 0, (0)))
        fEnd_TransX = pCamera.Translation.GetAnimationNode().Nodes[0].FCurve.Evaluate(FBTime(0, 0, 0, (self.FrameEnd)))
        diff = fstart_TransX - fEnd_TransX
        diff = diff / 2 # gets the offset

        return

    def ReadCamShakeKeys(self, pCamera):
        """
        RECORDING ONLY
        read our camera data into a instaced class 'FrameInfo'
        GLOBAL MATRIX!!!

        Function Author: Ross.George@RockstarLondon.com

        C++ void FBGetGlobalMatrix        (FBMatrix &pMatrix, FBMatrix pMatrixParent, FBMatrix pLocalMatrix);

        """
        if not pCamera:
            return
        print 'Processing Cam data'
        self.CameraShake = []

        # Set the base layer
        RS.Core.System.CurrentTake.SetCurrentLayer(0)

        # Get time range on the current take for the animation nodes we are interested in
        lCameraTimeRange = RS.Utils.Scene.Model.GetTimeRange(pCamera, [ RS.Core.System.CurrentTake ], None,
                                                  [ RS.Utils.Scene.Property.Translation,
                                                    RS.Utils.Scene.Property.Rotation,
                                                    RS.Utils.Scene.Property.FOV ])

        # Check the range we have been returned has some valid results
        if not lCameraTimeRange.Valid():
            print 'Invalid Time Range'
            return()

        # Set time to first frame of the given camera
        RS.Core.Player.Goto(lCameraTimeRange.Start)

        # Get initial FOV data
        lCameraFOVInitialValue = pCamera.FieldOfView.Data

        lNullParent = None
        # Transform the camera into world space looking down the Z axis
        if(self.ExportLocal):
            # Setup an inverse matrix
            lCameraInverseMatrix = FBMatrix()
            pCamera.GetMatrix(lCameraInverseMatrix)
            lCameraInverseMatrix.Inverse()

            # Create a null object and align it to the origin
            lNullParent = FBModelNull("ShakeCameraExportParent")
            lNullParent.SetMatrix(FBMatrix())
            RS.Core.System.Scene.Evaluate()

            # Parent the camera to the null and then pass in the cameras inverse
            # matrix to bring it into the centre of the world
            pCamera.Parent = lNullParent
            lNullParent.SetMatrix(lCameraInverseMatrix)
            RS.Core.System.Scene.Evaluate()

        # Now we step through each frame and export the data
        for iFrame in range(lCameraTimeRange.GetStartFrame(),
                             lCameraTimeRange.GetEndFrame()):

            # To keep the data as raw as possible we want to record everything even though we may not use it all
            FrameInfo = FrameDefInfo()
            FrameInfo.frame = iFrame

            lFrameTime = FBTime(0, 0, 0, iFrame)

            # Move to the given frame
            RS.Core.Player.Goto(lFrameTime)

            # Get the current frame matrix
            lCameraCurrentFrameMatrix = FBMatrix()
            pCamera.GetMatrix(lCameraCurrentFrameMatrix)

            # Extract rotation components from the matrix and write out our FrameInfo
            lRotationVector = FBVector3d()
            FBMatrixToRotation(lRotationVector, lCameraCurrentFrameMatrix)

            FrameInfo.RotX = lRotationVector[0]
            FrameInfo.RotY = lRotationVector[1]
            FrameInfo.RotZ = lRotationVector[2]

            # Extract translation components from the matrix and write out our FrameInfo
            lTranslationVector = FBVector4d()
            FBMatrixToTranslation(lTranslationVector, lCameraCurrentFrameMatrix)

            FrameInfo.TransX = lTranslationVector[0]
            FrameInfo.TransY = lTranslationVector[1]
            FrameInfo.TransZ = lTranslationVector[2]

            # Extract FOV data
            FrameInfo.FOV = pCamera.FieldOfView - lCameraFOVInitialValue

            self.CameraShake.append(FrameInfo)

        if(lNullParent):
            # Delete the null to revert the camera back to where it was before.
            lNullParent.FBDelete()

    def RecordSelectedCamera(self, pCamera):
        self.ReadCamShakeKeys(pCamera)

        (lRes, lShakeName) = FBMessageBoxGetUserValue("Save Camera", "Enter Shake Name for Camera: ", pCamera.Name, FBPopupInputType.kFBPopupString, "OK", "Cancel")

        if lRes and lShakeName != "":
            self.writeShakeFile(lShakeName)

        return

    def delLayer(self, layerName):
        lTake = FBSystem().CurrentTake
        lCurrentLayer = lTake.GetCurrentLayer()
        lLayerNumber = lTake.GetLayerCount()

        for iNumber in range(lLayerNumber):
            lLayer = lTake.GetLayer(iNumber)
            if lLayer.Name == layerName:
                # print "found layer %s " % lLayer.Name
                lTake.RemoveLayer(iNumber)
                return True
        return False


    def SetLayer(self, layerName=None):
        if not (self.FindLayer(layerName)):
            lSystem = FBSystem()
            lSystem.CurrentTake.CreateNewLayer()
            lCount = lSystem.CurrentTake.GetLayerCount()
            lSystem.CurrentTake.GetLayer(lCount - 1).Name = layerName
            lSystem.CurrentTake.SetCurrentLayer(lCount - 1)

            print "No %s Layer found, creating a new one"

    def FindLayer(self, layerName):
        lTake = FBSystem().CurrentTake
        lCurrentLayer = lTake.GetCurrentLayer()
        lLayerNumber = lTake.GetLayerCount()

        for iNumber in range(lLayerNumber):
            lLayer = lTake.GetLayer(iNumber)
            if lLayer.Name == layerName:
                lTake.SetCurrentLayer(iNumber)
                # print "Set to layer %s" % (layerName)
                return True

        return False

    def GetCameraList(self):
        self.lCameraList = []
        for iCamera in glo.gCameras:
            if not "Producer" in iCamera.Name:
                if not "ExportCamera" in iCamera.Name:
                    self.lCameraList.append(iCamera)

    def GetPropertiesMapping(self):
        return self._propertiesMapping or ShakerInterFaceMap()

    def SetPropertiesMapping(self, newValue):
            self._propertiesMapping = newValue

    def plotShakeLayer(self):
        propertiesMapping = self.GetPropertiesMapping()
        for lCamera in self.lCameraList :

            # Get time range on the current take for the animation nodes we are interested in
            lCameraTimeRange = RS.Utils.Scene.Take.GetLocalTimeRange(RS.Core.System.CurrentTake)

            # Check the properies we want to animate all exsist and
            lTranslationProperty = lCamera.PropertyList.Find(RS.Utils.Scene.Property.Translation)
            lRotationProperty = lCamera.PropertyList.Find(RS.Utils.Scene.Property.Rotation)
            lFOVProperty = lCamera.PropertyList.Find(RS.Utils.Scene.Property.FOV)

            # Check everything we need is set to animate
            lTranslationProperty.SetAnimated(True)
            lRotationProperty.SetAnimated(True)
            lFOVProperty.SetAnimated(True)

            # Get our target animation nodes
            lTranslationNode = lTranslationProperty.GetAnimationNode()
            lRotationNode = lRotationProperty.GetAnimationNode()
            lFOVNode = lFOVProperty.GetAnimationNode()

            # Clear all the old keys we don't need
            RS.Utils.Scene.AnimationNode.DeleteKeys(lTranslationNode, True)
            RS.Utils.Scene.AnimationNode.DeleteKeys(lRotationNode, True)
            RS.Utils.Scene.AnimationNode.DeleteKeys(lFOVNode, True)

            lCamShakeIndex = int(len(self.CameraShake) * (self.Shakephase / 100)) # start Frame

            # Now we step through each frame and export the data
            for iFrame in range(lCameraTimeRange.GetStartFrame(),
                                 lCameraTimeRange.GetEndFrame()):
                lKeyTime = FBTime(0, 0, 0, iFrame)

                if lCamShakeIndex > len(self.CameraShake) - 1:
                    lCamShakeIndex = 0 # We cycle through the shake camera
                    print "reset cam index to 0"

                # ROTATION#
                if(propertiesMapping.rotX):
                    lRotationNode.Nodes[0].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].RotX)
                if(propertiesMapping.rotY):
                    lRotationNode.Nodes[1].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].RotY)
                if(propertiesMapping.rotZ):
                    lRotationNode.Nodes[2].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].RotZ)

                # TRANSLATION
                if(propertiesMapping.transX):
                    lTranslationNode.Nodes[0].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].TransX)
                if(propertiesMapping.transY):
                    lTranslationNode.Nodes[1].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].TransY)
                if(propertiesMapping.transZ):
                    lTranslationNode.Nodes[2].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].TransZ)

                # FOV (We work on a Additive layer so we need to add the diff)
                if(propertiesMapping.fov):
                    lFOVNode.KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].FOV)

                # Move to next Value
                lCamShakeIndex += 1

                # Break while we work on the translation logic
                continue

                # Move to the frame so we get accurate matrix information
                RS.Core.Player.Goto(lKeyTime)
                RS.Core.System.Scene.Evaluate()

                # Now get the camera's matrix at this frame
                lCameraFrameMatrix = FBMatrix()
                lCamera.GetMatrix(lCameraFrameMatrix)

                # Create a matrix for the amount we need to offset the camera
                lCameraOffsetTranslationMatrix = FBMatrix()
                FBTranslationToMatrix(lCameraOffsetTranslationMatrix, FBVector4d(self.CameraShake[lCamShakeIndex].TransX,
                                                                                   self.CameraShake[lCamShakeIndex].TransY,
                                                                                   self.CameraShake[lCamShakeIndex].TransZ, 1))
                # Get the pretranslated matrix
                lCameraPretranslatedFrameMatrix = FBMatrix()
                FBMatrixMult(lCameraPretranslatedFrameMatrix, lCameraFrameMatrix, lCameraOffsetTranslationMatrix)

                # Now we subtract the original matrix by the final
                lCameraLocalMatrix = lCameraPretranslatedFrameMatrix - lCameraFrameMatrix

                # Extract the translation
                lTranslationVector = FBVector4d()
                FBMatrixToTranslation(lTranslationVector, lCameraLocalMatrix)

                # Finally we key this data
                lTranslationNode.Nodes[0].KeyAdd(lKeyTime, lTranslationVector[0])
                lTranslationNode.Nodes[1].KeyAdd(lKeyTime, lTranslationVector[1])
                lTranslationNode.Nodes[2].KeyAdd(lKeyTime, lTranslationVector[2])

                # FOV (We work on a Additive layer so we need to add the diff)
                lFOVNode.KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].FOV)

                # Move to next Value
                lCamShakeIndex += 1

                RS.Core.Player.SnapMode = FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames

                # Get End Frame.
                RS.Core.Player.GotoStart()

    """
    Public Methods
    """
    def plotShakeToCameras(self, camera=None):
        # Resolve our shake layer and set it as active
        lShakeLayer = RS.Utils.Scene.Layer.ResolveLayer ("ShakeCam")
        RS.Utils.Scene.Layer.SetLayerActive(RS.Core.System.CurrentTake, lShakeLayer)

        if camera != None:
            self.lCameraList = [camera]
            self.plotShakeLayer()
        else:
            self.GetCameraList()
            self.plotShakeLayer()

        return

    def LoadShakeFile(self, shakeName, rootRelativePath=True):
        shakePath = os.path.join(self.path, shakeName)
        if not rootRelativePath:
            shakePath = shakeName
        self.readShakeFile(shakePath)
        if len(self.CameraShake) > 0:
            print "Loaded %s file: " % (shakePath)
        else:
            print "Failed to Load %s " % (shakePath)
        return

    def DeleteShake(self):
        lResult = FBMessageBox("Warning" , "This will remove ALL camera Shake Data" , "Cancel", "Continue")
        if lResult == 2:
            self.delLayer("ShakeCam")
        return

    def AddShake(self):
        lResult = FBMessageBox("Warning" , "This will add Shake Data to ALL cameraa" , "Cancel", "Continue")
        if lResult == 2:
            if len(self.CameraShake) > 0:
                self.delLayer("ShakeCam")
                self.SetLayer("ShakeCam")
                self.plotShakeToCameras()
            else:
                print "No Shake Loaded"
        return

    def AddShakeSelected(self, camera):
        if len(self.CameraShake) > 0:
            if not self.FindLayer("ShakeCam"):
                self.SetLayer("ShakeCam")
            self.plotShakeToCameras(camera)
        else:
            print "No Shake Loaded"
        return

    def DeleteShakeSelected(self, camera):
        if camera != None:
            self.__DeleteSelectedShake(camera)
            return

    def UpdateShakeWeight(self, shakeweight):
        self.Shakephase = shakeweight
        return

    def ExportCamShake(self, camera):
        if camera != None:
            self.__ExportCamShake(camera)
            return

    def DisableLocalExportTransforms(self, exportlocal=True):
        self.ExportLocal = exportlocal

    def main(self):
        """
            Not used
        """
        self.delLayer("ShakeCam")
        self.SetLayer("ShakeCam")

        self.plotCameras()


HandShake = HandShake()
