"""
Description:
    A lower level function for creating a camInfoObj instance from the currently opened FBX.
    Assumes higher level setup steps have already happened, via camKeyTasks or otherwise.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import time
from pyfbsdk import *
import RS.Config
from RS.Utils.Scene import DeSelectAll
from RS.Core.Automation.FrameCapture import CapturePaths
import camKeyDef


def CreateCamInfoFromFbx(saveExpCam=False):
    """
    Creates a camInfoObj from the currently open FBX.
    Assumes the FBX has been validated, cameras are unlocked, and keyless properties are keyed.
    Returns:
        An instance of the camInfo object containing camera data.
    """
    savedPropertiesList = ['Translation (Lcl)', 'Rotation (Lcl)', 'Roll', 'Field Of View', 'Lens', 'FOCUS (cm)']
    tangentFlagProps = ["Interpolation", "TangentBreak", "TangentClampMode", "TangentConstantMode", "TangentMode"]
    exportCamNames = ["ExportCamera", "TransferCamera"]

    # Save Extra Properties - GTA cams only
    capPaths = CapturePaths.FastPath(FBApplication().FBXFileName)
    if capPaths.project == "GTA5":
        savedPropertiesList += ['Near Outer Override', 'Near Inner Override', 'Far Inner Override',
                                'Far Outer Override', 'CoC Override']

    # Create New CamInfo Object
    camInfoObj = camKeyDef.CamInfo()

    # Make Basic Info Object
    camInfoObj.FbxPath = FBApplication().FBXFileName.replace("\\", "/")
    camInfoObj.UserEmail = RS.Config.User.Email.lower()
    camInfoObj.CreationTime = str(time.time())

    # Save ShakeWeight - Just a single global value for now, might expand later
    shakeLayer = FBSystem().CurrentTake.GetLayerByName("ShakeCam")
    if shakeLayer:
        camInfoObj.ShakeWeight = shakeLayer.Weight

    # Create Basic MB Objects
    lSystem = FBSystem()
    lScene = lSystem.Scene
    
    # Force 30FPS - just while saving
    previousFps = FBPlayerControl().GetTransportFps()
    FBPlayerControl().SetTransportFps(FBTimeMode.kFBTimeMode30Frames)

    # Create Constant Key Filter
    keyFilter = FBFilterManager().CreateFilter("Constant Key Reducer")
    endFrame = FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
    keyFilter.PropertyList[0].Data = FBTime(0, 0, 0, 0)
    keyFilter.PropertyList[1].Data = FBTime(0, 0, 0, endFrame)
    keyFilter.PropertyList[2].Data = True

    # Deselect All And Set Base Layer
    DeSelectAll()
    lSystem.CurrentTake.SetCurrentLayer(0)

    # Create LayerList
    mbTake = lSystem.CurrentTake
    layerList = [i for i in xrange(mbTake.GetLayerCount()) if i is 0 or "cam" in mbTake.GetLayer(i).Name.lower()]

    # Create CameraList - skipping any invalid names
    cameraList = [cam for cam in lScene.Cameras if cam.SystemCamera is False]

    # Create Cam Switcher Objects
    switcherCamIndexes = []
    lSwitcherKeys = FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode().FCurve.Keys
    previousKey = camKeyDef.KeyItem()

    # Get Cam Rig Dict
    camRigDict = GetCamRigs()

    # LOOP THROUGH CAM SWITCHER
    for i in range(len(lSwitcherKeys)):
        eachKey = lSwitcherKeys[i]

        # Get Key Values - skipping invalid cams
        keyTime = eachKey.Time.GetTimeString().replace("*", "")
        keyIndex = int(eachKey.Value) - 1
        keyedCam = cameraList[keyIndex]
        if keyedCam.PropertyList.Find("Lens") is None:
            continue
        keyValue = str(keyedCam.Name)

        # Add switcher index to indexList
        if keyIndex not in switcherCamIndexes:
            switcherCamIndexes.append(keyIndex)

        # Create New Key - only if value is different from the last key
        # or there's only one switcher cam (for export/transfer cam mode).
        if keyValue != previousKey.Value or len(switcherCamIndexes) == 1:
            newKey = camKeyDef.KeyItem()
            newKey.Time = keyTime
            newKey.Value = keyValue
            camInfoObj.SwitcherKeyList.append(newKey)

            previousKey = newKey

    # LOOP THROUGH CAMERAS
    for eachCam in cameraList:

        # Skip invalid cams - all RS cams should have Lens
        if eachCam.PropertyList.Find("Lens") is None:
            continue
        # Skip Export Cam - unless allowed via arg
        if saveExpCam is False and "exportcamera" in eachCam.Name.lower():
            continue

        # Create Cam Data Object
        camObj = camKeyDef.ParentItem()
        camObj.Type = "Camera"
        camObj.Name = eachCam.Name
        camObj.Quaternion = eachCam.QuaternionInterpolate
        exportCam = camObj.Name in exportCamNames

        # Save Cam Rig Name - if detected
        if eachCam.Name in camRigDict.iterkeys():
            rigName = camRigDict[eachCam.Name]
            camObj.CamRig = rigName

        # LOOP THROUGH CAM PROPERTIES
        for eachProp in savedPropertiesList:
            currentCamProp = eachCam.PropertyList.Find(eachProp)

            # Filter out Missing Properties
            if currentCamProp is None:
                continue

            # Force Animated Props
            if not currentCamProp.IsAnimated():
                currentCamProp.SetAnimated(True)
            parentAnimNode = currentCamProp.GetAnimationNode()

            # Create Property Node List
            propNameList = []
            propNodeList = []

            # Add Single Node for Standard Properties
            if (eachProp != 'Translation (Lcl)') and (eachProp != 'Rotation (Lcl)'):
                propNameList.append(eachProp)
                propNodeList.append(parentAnimNode)

            # Add 3 Nodes for Translation Rotation Properties
            else:
                childNodes = parentAnimNode.Nodes
                for g in range(len(childNodes)):
                    baseAnimNode = childNodes[g]
                    propName = "".join([eachProp, 'XYZ'[g]])
                    propNameList.append(propName)
                    propNodeList.append(baseAnimNode)

            # Loop Through Property Nodes
            for propIndex in range(len(propNameList)):
                currentPropName = propNameList[propIndex]
                currentAnimNode = propNodeList[propIndex]
                propObjExists = False

                # LOOP THROUGH LAYERS
                for i in layerList:
                    lSystem.CurrentTake.SetCurrentLayer(i)
                    if currentAnimNode.FCurve.Keys:

                        # Run Constant Key Filter
                        if not exportCam:
                            keyFilter.Apply(parentAnimNode, True)

                        # Create Prop Object - only once we know it has keys
                        if not propObjExists:
                            propObj = camKeyDef.ParentItem()
                            propObj.Type = "Property"
                            propObj.Name = currentPropName
                            propObjExists = True

                        # Create Layer Object
                        layerObj = camKeyDef.ParentItem()
                        layerObj.Type = "Layer"
                        layerObj.Name = lSystem.CurrentTake.GetLayer(i).Name
                        layerKeys = currentAnimNode.FCurve.Keys

                        # Create Initial Key
                        currentKey = camKeyDef.KeyItem()
                        currentKey.LeftDerivative = 0.0
                        currentKey.RightDerivative = 0.0
                        currentKey.LeftTangentWeight = 0.3333333432674408
                        currentKey.RightTangentWeight = 0.3333333432674408
                        nextKey = layerKeys[0]

                        # LOOP THROUGH KEYS
                        for keyIndex in xrange(len(layerKeys)):
                            mbKey = nextKey
                            nextIndex = keyIndex + 1 if keyIndex < len(layerKeys) - 1 else keyIndex
                            nextKey = layerKeys[nextIndex]

                            # TODO: Removed below newKeyNeeded check - rarely it skips valid keys
                            # newKeyNeeded = (exportCam or mbKey.Value != currentKey.Value or
                            #                 mbKey.RightBezierTangent != nextKey.LeftBezierTangent or
                            #                 (int(currentKey.TangentFlag[-1]) == 0 and len(layerObj.ChildList) > 1 and
                            #                  layerObj.ChildList[-2].Value != mbKey.Value))
                            # # Skip Obvious Dupes - if values and tangents are identical
                            # if newKeyNeeded:

                            # Create New Key - and set time (so it isn't compared like other key props)
                            newKey = camKeyDef.KeyItem()
                            newKey.Time = mbKey.Time.GetSecondDouble()

                            # Filter Properties to Check
                            keyProps = newKey.Order[1:]  # Filter out time
                            if mbKey.TangentBreak is False:  # Filter out right tangents (unless tangent break)
                                keyProps = keyProps[:-2]
                            if int(mbKey.TangentMode) in [0, 4, 5]:  # Filter out all tangents (if using Auto modes)
                                keyProps = keyProps[:2]

                            # Loop Through Key Props
                            for keyPropName in keyProps:

                                # Get New Value
                                if keyPropName == "TangentFlag":
                                    newValue = "".join([str(getattr(mbKey, flag).numerator) for flag in tangentFlagProps])
                                else:
                                    newValue = getattr(mbKey, keyPropName)

                                # Changed Property - we update our new & current key and set newKeyNeeded to True
                                previousValue = getattr(currentKey, keyPropName)
                                if newValue != previousValue or exportCam:
                                    setattr(newKey, keyPropName, newValue)
                                    setattr(currentKey, keyPropName, newValue)

                            # Add New Key to ChildList
                            layerObj.ChildList.append(newKey)

                        # Add Layer to LayerList
                        propObj.ChildList.append(layerObj)

                # Add Property to PropList - only if keys were found
                if propObjExists:
                    camObj.ChildList.append(propObj)

        # Add Camera to CamList
        camInfoObj.CamList.append(camObj)

    # Select Base Layer
    lSystem.CurrentTake.SetCurrentLayer(0)
    
    # Restore FPS
    FBPlayerControl().SetTransportFps(previousFps)

    return camInfoObj


def GetCamRigs():
    camRigDict = {}
    cameraList = [cam for cam in FBSystem().Scene.Cameras if cam.SystemCamera is False]
    # Check Constraints
    for const in FBSystem().Scene.Constraints:
        if const.LongName.lower().startswith("mc") and const.ReferenceGet(0) in cameraList:
            camName = const.ReferenceGet(0).Name
            rigName = "C:{0}".format(const.LongName.split(":")[0])
            camRigDict[camName] = rigName
    # Check Parenting
    for camera in cameraList:
        if camera.Parent and camera.Parent.LongName.lower().startswith("mc_"):
            camName = camera.Name
            rigName = "P:{0}".format(camera.Parent.LongName.split(":")[0])
            camRigDict[camName] = rigName
    return camRigDict
