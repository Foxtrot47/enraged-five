"""
Description:
    Functions for reading and writing camKey objects to and from files.
    Also a conversion script to convert old formatted key XMLs to the new format.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import cPickle
from xml.etree import cElementTree as ET
from xml.dom import minidom
import camKeyDef


def SerializeCamInfo(camInfoObj, picklePath):
    """
    Writes out a camInfoObj to a pickle binary at the picklePath.
    Arguments:
        camInfoObj: An instance of the camInfo object containing camera data.
        picklePath: A string containing the local output path of the pickle file.
    """

    pickleFile = open(picklePath, 'wb')
    cPickle.dump(camInfoObj, pickleFile, protocol=2)
    pickleFile.close()


def DeserializeCamInfo(picklePath):
    """
    Creates a camInfoObj from a pickle binary at the picklePath.
    Arguments:
        picklePath: A string containing the local path of the pickle file to load.
    Returns:
        A new instance of the camInfoObj deserialized from the input file.
    """

    pickleFile = open(picklePath, 'rb')
    camInfoObj = cPickle.load(pickleFile)
    pickleFile.close()

    return camInfoObj


def WriteCamInfoXml(camInfoObj, xmlPath):
    """
    Writes out a camInfoObj to an xml file at the xmlPath.
    Arguments:
        camInfoObj: An instance of the camInfo object containing camera data.
        xmlPath: A string containing the local path to save the xml file to.
    """
    # Create Node Tree
    camInfoTree = CreateCamInfoXmlTree(camInfoObj)

    # Convert Tree to Formatted Xml String
    xmlString = ET.tostring(camInfoTree, 'utf-8')
    xmlString = minidom.parseString(xmlString).toprettyxml(indent="\t")

    # Remove Extra Lines - caused by minidom's dumb .toprettyxml call
    xmlLines = xmlString.split(">")
    for i in range(len(xmlLines)):
        if xmlLines[i].count("\n") == 2:
            xmlLines[i] = xmlLines[i].replace("\t", "").replace("\n", "")
    xmlString = ">".join(xmlLines)

    # Write Out xmlString
    # check if directory exists url:bugstar:5300313
    splitPath = xmlPath.split('/')
    disk = "{}\\".format(splitPath[0])
    directoryList = splitPath[1:-1]
    directoryList.insert(0,disk)
    directory = os.path.join(*directoryList)

    if not os.path.isdir(directory):
        os.makedirs(directory)

    # write out file
    xmlFile = open(xmlPath, "w")
    xmlFile.write(xmlString)
    xmlFile.close()


def CreateCamInfoXmlTree(currentObject, parent=None):
    """
    Creates an xml element tree and appends data recursively.
    Arguments:
        currentObject: An instance of a camKey object.
    Keyword Arguments:
        parent: The parent camKey objects of the current object
    Returns:
        An xml tree element containing the currentObject's data in xml format.
    """

    # Detect Element Name
    currentName = str(currentObject.__class__).split(".")[-1]
    if currentName.endswith("'>"):
        currentName = currentName[:-2]

    # Create New Element
    if parent is None:
        currentElement = ET.Element(currentName)
    else:
        currentElement = ET.SubElement(parent, currentName)

    # Loop Through Object Attributes
    for attributeName in currentObject.Order:
        attributeValue = getattr(currentObject, attributeName, None)
        if attributeValue is None:
            continue

        # Set Type as Attribute
        elif attributeName == "Type":
            attributes = currentElement.attrib
            attributes["Type"] = attributeValue

        # Create Simple Elements - basic attributes
        elif type(attributeValue) != list:
            subElement = ET.SubElement(currentElement, attributeName)
            if type(attributeValue) == float:
                subElement.text = repr(attributeValue)
            else:
                subElement.text = str(attributeValue)

        # Create SubElements - list attributes
        else:
            if len(attributeValue) == 0:
                continue
            subElement = ET.SubElement(currentElement, attributeName)
            for childObject in attributeValue:
                CreateCamInfoXmlTree(childObject, subElement)

    return currentElement


def CreateCamInfoObjFromXmlPath(xmlPath):
    """
    Creates a camInfoObj from an xml path.
    Also detects files in the old key xml format and updates them before processing.
    Arguments:
        xmlPath: A string containing the xml local path to load from.
    Returns:
        An instance of the camInfo object containing camera data.
    """

    tree = ET.parse(xmlPath)
    rootNode = tree.getroot()
    rootObject = camKeyDef.CamInfo()

    # Detect Old Xml File - and update node to the new format
    if rootNode.tag == "cameraDetails":
        rootNode = UpdateOldXmlToNew(rootNode)

    # Create New camInfoObj
    camInfoObj = SetAttributesFromXmlNode(rootNode, rootObject)

    return camInfoObj


def SetAttributesFromXmlNode(rootNode, rootObject):
    """
    Sets an objects attributes from an xml node recursively.
    Arguments:
        rootNode: The current xml node to read attributes from.
        rootObject: The current camKey object to write elements to.
    Returns:
        The current camKey object with all matched attributes and children set.
    """

    # Set Matched Xml Attributes
    for xmlAttribName in rootNode.attrib.keys():
        if xmlAttribName in rootObject.Order:
            setattr(rootObject, xmlAttribName, rootNode.attrib[xmlAttribName])

    # Set Matched Children Attributes
    for currentNode in rootNode.getchildren():
        matchedAttributeName = [objectAttribute for objectAttribute in rootObject.Order if
                                objectAttribute.lower() == currentNode.tag.lower()]
        if len(matchedAttributeName) > 0:
            attributeName = matchedAttributeName[0]
            attributeValue = getattr(rootObject, matchedAttributeName[0])

            if type(attributeValue) != list:
                nodeText = currentNode.text.strip()
                if nodeText.lower() == "true":
                    setattr(rootObject, attributeName, True)
                elif nodeText.lower() == "false":
                    setattr(rootObject, attributeName, False)
                else:
                    setattr(rootObject, attributeName, nodeText)

            else:
                childObjList = []
                for childNode in currentNode.getchildren():
                    matchedObjectName = [objName for objName in dir(camKeyDef) if
                                         objName.lower() == childNode.tag.lower()]
                    if len(matchedObjectName) > 0:
                        childObjectName = matchedObjectName[0]
                        childObj = getattr(camKeyDef, childObjectName)()
                        SetAttributesFromXmlNode(childNode, childObj)
                        childObjList.append(childObj)
                setattr(rootObject, attributeName, childObjList)

    return rootObject


def UpdateOldXmlToNew(rootNode):
    """
    Updates old xml files to the new format. A bit shit - I've changed the xml format a few times now
    and this has just been tweaked each time, rather than doing a comprehensive rewrite.  Works for now though.
    Arguments:
        rootNode: An xml node containing data in the old format.
    Returns:
        An xml node containing data in the new format.
    """

    renameDict = {"cameraDetails": "CamInfo",
                  "switcherKeys": "SwitcherKeyList",
                  "createdTime": "CreationTime",
                  "camName": "Name",
                  "propName": "Name",
                  "layerName": "Name"}
    extraTagsToRemove = ["fbxInfo"]
    extraTagsToAdd = {"camItem": "CamList",
                      "propItem": "PropertyList",
                      "layerItem": "LayerList",
                      "keyItem": "KeyList"}
    skipExtraTags = ["SwitcherKeyList"]

    tagsRenamedParent = ["camItem", "propItem", "layerItem"]
    tagsRenamedChildList = ["PropertyList", "LayerList", "KeyList"]

    for parent in rootNode.getiterator():
        nodesToDelete = []

        # Rename Old Nodes to New Names
        if parent.tag in renameDict.keys():
            parent.tag = renameDict[parent.tag]

        # Remove Extra Tags
        for child in parent:
            if child.tag in extraTagsToRemove:
                for i in range(len(list(child))):
                    parent.insert(i, child[i])
                parent.remove(child)

        # Add Addition List Tags - if not in the skipExtraTags list
            if child.tag in extraTagsToAdd.keys() and parent.tag != extraTagsToAdd[child.tag]:
                if parent.tag not in skipExtraTags:
                    listNode = parent.find(extraTagsToAdd[child.tag])
                    if listNode is None:
                        listNode = ET.SubElement(parent, extraTagsToAdd[child.tag])
                    listNode.append(child)
                    nodesToDelete.append(child)
        for eachNode in nodesToDelete:
            parent.remove(eachNode)

        # Rename Parent Items
        if parent.tag in tagsRenamedParent:
            index = tagsRenamedParent.index(parent.tag)
            parent.tag = "ParentItem"
            if index == 0:
                parent.attrib["Type"] = "Camera"
            elif index == 1:
                parent.attrib["Type"] = "Property"
            else:
                parent.attrib["Type"] = "Layer"

        # Rename ChildList Items
        if parent.tag in tagsRenamedChildList:
            parent.tag = "ChildList"

    return rootNode