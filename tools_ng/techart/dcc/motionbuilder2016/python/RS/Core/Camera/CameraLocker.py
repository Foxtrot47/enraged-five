from pyfbsdk import *
from xml.dom import minidom
import RS.Config
from RS.Core.Automation.FrameCapture import CapturePaths


class LockManager():
    def __init__(self):
        self.cameraList = []
        self.lockedCount = 0
        self.unlockedCount = 0

        self.excludedCamNames = ['producer perspective', 'producer front', 'producer back',
                                 'producer right', 'producer left', 'producer top', 'producer bottom',
                                 '3lateral', 'gizmo']
        self.camPropNames = ["Translation (Lcl)", "Rotation (Lcl)", "Roll", "Field Of View", "Lens", "FOCUS (cm)"]
        self.gta5PropNames = ["CoC Override", "Near Outer Override", "Near Inner Override", "Far Inner Override",
                              "Far Outer Override"]

        self.createCamList()
        self.detectLockCounts()

        # self.setAllCamLocks() #Pass nothing or True for lock, False for unlock
        # self.keyAllKeylessCams()

    def createCamList(self):
        """ Creates a list of all camera objects in the FBX,
        unless their names contain strings found in the excludedCamNames list. """

        # Get MB Objects
        lSystem = FBSystem()
        lScene = lSystem.Scene

        # Go Through Cameras
        for camera in lScene.Cameras:

            # Check Cam Name for All Invalid Names
            validCam = True
            for eachInvalidName in self.excludedCamNames:
                if eachInvalidName in camera.Name.lower():
                    validCam = False
                    break

            # Add Valid Cams To Cam List
            if validCam == True:
                self.cameraList.append(camera)

    def detectLockCounts(self):
        """ Counts the total number of locked and unlocked properties from the cameras in our cameraList. """

        # Reset Previous Counts
        self.lockedCount = 0
        self.unlockedCount = 0

        # Loop Through Each Camera
        for camera in self.cameraList:
            for eachProp in camera.PropertyList:

                # Count Each Property's Lock Status
                if eachProp.AllowsLocking():
                    if eachProp.IsLocked() == True:
                        self.lockedCount += 1
                    else:
                        self.unlockedCount += 1

        # Count Switcher Index Lock Status
        lCameraIndexProp = FBCameraSwitcher().PropertyList.Find("Camera Index")
        if lCameraIndexProp.IsLocked() == True:
            self.lockedCount += 1
        else:
            self.unlockedCount += 1

    def setAllCamLocks(self, locked=True):
        """ Sets the lock status of all properties on all cameras from the cameraList.
        Passing nothing or True will lock them, False will unlock. """

        # Loop Through Cams
        for camera in self.cameraList:
            # Only lock RS Cameras
            if locked and camera.PropertyList.Find("Lens") is None:
                continue
            for eachProp in camera.PropertyList:
                if eachProp.AllowsLocking():
                    # 2D Magnifier Props - always unlock and turn off
                    if "2DMagnifier" in eachProp.Name.replace(" ", ""):
                        eachProp.SetLocked(False)
                        if eachProp.Name == "Use2DMagnifierZoom":
                            eachProp.Data = False
                    # Regular Props - set lock
                    else:
                        eachProp.SetLocked(locked)

        # Set Lock Status On Switcher
        lCameraIndexProp = FBCameraSwitcher().PropertyList.Find("Camera Index")
        lCameraIndexProp.SetLocked(locked)

    def unlockOtherCams(self):
        for camera in self.cameraList:
            if camera.PropertyList.Find("Lens") is not None:
                continue
            for eachProp in camera.PropertyList:
                if eachProp.AllowsLocking() and eachProp.IsLocked():
                    eachProp.SetLocked(False)

    def getUserLayer(self):
        """ Gets the user's selected layer number,
        or simply uses the max if multiple are selected."""

        # Get MB Objects
        lSystem = FBSystem()
        layerCount = lSystem.CurrentTake.GetLayerCount()

        # Create List of Selected Layers
        selectedLayers = []
        for i in range(layerCount):
            layerSelected = lSystem.CurrentTake.GetLayer(i).Selected
            if layerSelected == True:
                selectedLayers.append(i)

        # Return Max Layers - or default to 0 if errors occur
        try:
            userLayer = max(selectedLayers)
        except ValueError:
            userLayer = 0
        return userLayer

    def keyAllKeylessCams(self):
        """ Keys any 'keyless' switcher camera found with a default key at frame 0.
        We only key properties specified in our propsToKey list (and FOV if using the zoom lens). """

        # Get Prop Dict - adding extras for GTA5 only
        capPaths = CapturePaths.FastPath(FBApplication().FBXFileName)
        if capPaths.project == "GTA5":
            self.camPropNames.extend(self.gta5PropNames)

        # Detect Any Unlocked Props - and unlock all if found.
        self.detectLockCounts()
        if self.lockedCount != 0:
            self.setAllCamLocks(False)

        # Get User Layer - so we can restore later
        userLayer = self.getUserLayer()

        # Get MB Objects & Set Base Layer
        lSystem = FBSystem()
        lSystem.CurrentTake.SetCurrentLayer(0)

        # Set Default Keyframe Time
        keyTime = FBTime()
        keyTime.SetFrame(0)

        # Loop Through Each Layer
        layerCount = lSystem.CurrentTake.GetLayerCount()
        for layerIndex in xrange(layerCount):
            lSystem.CurrentTake.SetCurrentLayer(layerIndex)

            # Loop Through Each Camera
            for camera in self.cameraList:
                # Loop Through Each Prop
                for propName in self.camPropNames:
                    camProp = camera.PropertyList.Find(propName)

                    # Skip Missing Props
                    if camProp is None:
                        continue

                    # Force Animated Props
                    if not camProp.IsAnimated():
                        camProp.SetAnimated(True)

                    # Get Nodes to Key
                    propNodes = []
                    if type(camProp) == FBPropertyAnimatableVector3d:
                        for childNode in camProp.GetAnimationNode().Nodes:
                            propNodes.append(childNode)
                    else:
                        propNodes.append(camProp.GetAnimationNode())

                    # Loop Through Nodes - keying if no keys and value is not default
                    for propNode in propNodes:
                        if propNode.KeyCount == 0:
                            if layerIndex == 0 or propNode.FCurve.Evaluate(keyTime) != 0.0:
                                camProp.KeyAt(keyTime)

        # Restore Previous User Layer
        lSystem.CurrentTake.SetCurrentLayer(userLayer)


class AutoLock():
    """ A lock reminder script setup as a callback on saving an FBX.
    Only runs for cam team members when unlocked cams are detected. """

    def __init__(self):
        self.validUserXmlPath = RS.Config.Tool.Path.TechArt + "\\etc\\config\\camera\\camTeam_config.xml"

        self.main()

    def createValidUserListFromXml(self):
        validUserList = []
        xmlFile = minidom.parse(self.validUserXmlPath)
        for node in xmlFile.getElementsByTagName("camTeamEmail"):
            tagValue = (str(node.firstChild.nodeValue)).lower()
            validUserList.append(tagValue)
        xmlFile.unlink()
        return validUserList

    def validateUser(self):
        try:
            userEmail = RS.Config.User.Email.lower()
        except:
            return False
        try:
            camTeamEmails = self.createValidUserListFromXml()
        except:
            return False
        if userEmail not in camTeamEmails:
            return False
        else:
            return True

    def main(self):
        # Import camKeyTasks - a bit shit, but prevents circular reference
        import RS.Core.Camera.SaveCameras as SaveCameras

        # Validate User First
        userStatus = self.validateUser()
        if userStatus:

            # Detected Unlocked Cams
            camLockObj = LockManager()
            if camLockObj.unlockedCount != 0:

                # Ask For Lock Save Choice
                messageText = "Unlocked cams detected. Lock or save cam data before saving the FBX?"
                userChoice = FBMessageBox("Cam Data Alert", messageText,
                                          "Lock Cameras", "Lock & Save Cams", "Save FBX Only")

                # Save Key Data - if user chooses
                if userChoice == 2:
                    SaveCameras.Run()

                # Lock Cams - if user chooses
                if userChoice in [1, 2]:
                    camLockObj.setAllCamLocks(True)


def ForceRenderAudioRate():
    """ Sets render audio rate to 48K instead of the default 44.1K, preventing sync issues. """
    renderOptions = FBVideoGrabber().GetOptions()
    renderOptions.AudioRenderFormat = 2113538
    FBVideoGrabber().SetOptions(renderOptions)


def Run():
    """ If triggered via the RS menu, simply detect any locked cams and automatically unlock them. """
    _camLockObj_ = LockManager()
    if _camLockObj_.lockedCount != 0:
        _camLockObj_.setAllCamLocks(False)