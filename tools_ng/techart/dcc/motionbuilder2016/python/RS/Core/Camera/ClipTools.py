import os
import pyfbsdk as mobu
from RS import Globals, Config
from RS.Core.Automation.FrameCapture import CapturePaths
from RS.Core.Camera import RangeTools

# Janky Import - to show message boxes in both Mobu versions
try:
    from PySide2 import QtCore, QtGui, QtWidgets
except ImportError:
    from PySide import QtCore, QtGui as QtWidgets


def SetHardRanges(endFrame):
    """
    Sets the FBX's hardranges based on a given endFrame.
    Arguments:
        endFrame: An int of the desired endFrame.
    """
    # todo: Force 30fps temporarily - until our ranges are fully compatible with 60fps
    previousFps = mobu.FBPlayerControl().GetTransportFps()
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)

    # Set Ranges in Empty FBX
    fixedTimeSpan = mobu.FBTimeSpan(mobu.FBTime(0, 0, 0, 0), mobu.FBTime(0, 0, 0, int(endFrame)))
    mobu.FBSystem().CurrentTake.LocalTimeSpan = fixedTimeSpan

    # Restore Previous Fps
    mobu.FBPlayerControl().SetTransportFps(previousFps)


def SetSoftRanges(startFrame, endFrame):
    mobu.FBPlayerControl().ZoomWindowStart = mobu.FBTime(0, 0, 0, startFrame)
    mobu.FBPlayerControl().ZoomWindowStop = mobu.FBTime(0, 0, 0, endFrame)


def CleanSwitcherKeys():
    # Get Switcher Keys - or exit early if none found
    # TODO: Investigate Globals Switcher Bug - below line crashes mobu when using Multi Camera Viewer
    # switcherCurve = Globals.CameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve
    switcherCurve = mobu.FBCameraSwitcher().PropertyList.Find("Camera Index").GetAnimationNode().FCurve
    switcherKeys = switcherCurve.Keys
    if len(switcherKeys) == 0:
        return

    # Get CamList and ClipEndFrame
    camList = [cam for cam in Globals.Cameras if not cam.SystemCamera]
    clipFrames = [clip.MarkOut.GetFrame() for clip in GetAutoClips()]
    clipEndFrame = max(clipFrames) if clipFrames else None

    # Delete Invalid Switcher Keys
    invalidKeys = [i for i, key in enumerate(switcherKeys) if int(key.Value) > len(camList)]
    invalidKeys.reverse()
    [switcherCurve.KeyDelete(index, index) for index in invalidKeys]

    # Delete Dupe Switcher Keys
    dupeKeys = [i for i, key in enumerate(switcherKeys) if i and switcherKeys[i - 1].Value == key.Value]
    dupeKeys.reverse()
    [switcherCurve.KeyDelete(index, index) for index in dupeKeys]

    # Cleanup Switcher Keys
    for keyIndex, key in enumerate(switcherKeys):
        key.TangentMode = mobu.FBTangentMode.kFBTangentModeAuto
        key.Interpolation = mobu.FBInterpolation.kFBInterpolationConstant
        key.TangentConstantMode = mobu.FBTangentConstantMode.kFBTangentConstantModeNormal
        # Force First Key to Zero - only if it's > 0
        if keyIndex == 0 and key.Time.GetFrame() > 0.0:
            key.Time = mobu.FBTime(0, 0, 0, 0)
            continue
        # Force Keys to Exact Frame - rarely keys can be between frames
        if float(key.Time.Get()) % 1539538600.0 != 0.0:
            keyFrame = key.Time.GetFrame()
            key.Time = mobu.FBTime(0, 0, 0, keyFrame)
        # Clip EndFrame Fix - increases last switcher key by a frame to prevent interpolation
        if key.Time.GetFrame() == clipEndFrame:
            key.Time = mobu.FBTime(0, 0, 0, clipEndFrame + 1)


def CheckForRangeUpdate():
    rangeUsers = ["blake.buck", "jack.davidson", "albert.carbillas", "sheraz.afzal", "duke.edge",
                  "phil.sokoloff", "jordan.hwang", "cole.krug", "nathan.hunt", "steven.latta",
                  "duggy.duignan", "jesus.cobo", "luke.howard", "alex.rooney", "andrew.albon",
                  "jonathan.sullivan", "geoffrey.fermin", "darren.iddon", "justin.granger",
                  "steve.mears"]
    userName = Config.User.Email.split("@")[0].lower()
    if userName in rangeUsers:
        fbxPath = mobu.FBApplication().FBXFileName.lower()
        RangeTools.UpdateRangesPickle(fbxPath)


def LoadClips():
    fbxPath = mobu.FBApplication().FBXFileName.lower()
    if not fbxPath:
        return "Error: Ranges not found.\n\nFbx is blank, or not yet saved."
    cutsceneFolder = os.path.sep.join(["animation", "cutscene"])
    cinematicFolder = os.path.sep.join(["animation", "cinematic"])
    ingameFolder = os.path.sep.join(["animation", "ingame"])
    if cutsceneFolder in fbxPath or cinematicFolder in fbxPath:
        loadStatus = LoadCutsceneClips()
        return "Cutscene clips reloaded." if loadStatus is True else loadStatus
    elif ingameFolder in fbxPath:
        LoadIngameClips()
        return "Ingame clip update ran."
    else:
        return "Error: Ranges not found.\n\nFbx saved outside of a cutscene/ingame root folder."


def GetAutoClips():
    autoClips = []
    estTracks = list(mobu.FBStory().RootEditFolder.Tracks)
    [[autoClips.append(clip) for clip in track.Clips if clip.Name.lower().startswith("autoclip")] for track in estTracks]
    return autoClips


def DeleteAutoClips():
    shotClips = []
    shotTracks = list(Globals.Story.RootEditFolder.Tracks)
    shotComps = [comp for comp in mobu.FBSystem().Scene.Components
                 if comp.Name.startswith("Shot ") and [prop for prop in comp.PropertyList if prop.Name == "Camera"]]
    [[shotClips.append(clip) for clip in track.Clips] for track in shotTracks]
    [comp.FBDelete() for comp in shotComps]
    [clip.FBDelete() for clip in shotClips]
    [track.FBDelete() for track in shotTracks]


def LoadCutsceneClips():
    # Force 30FPS - during clip creation for now
    previousFps = mobu.FBPlayerControl().GetTransportFps()
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)
    
    # Get Cams and Switcher
    CleanSwitcherKeys()
    camList = [cam for cam in Globals.Cameras if not cam.SystemCamera]
    switcherKeys = list(Globals.CameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys)
    if not switcherKeys:
        return "Error: No switcher keys found."

    # Detect Fbx Name
    fbxPath = mobu.FBApplication().FBXFileName.replace("\\", "/")
    fbxName = os.path.splitext(os.path.split(fbxPath)[1])[0].upper()
    if not RangeTools.ValidateFbxName(fbxName):
        return "Error: Invalid fbx."

    # Get Cap Paths
    capPaths = CapturePaths.FastPath(fbxPath)

    # Get Range Dict
    rangeDict = RangeTools.GetRangesPickle(fbxPath)
    # rangeDict = RangeTools.GetRangesExcel(fbxPath)
    if not rangeDict:
        return "Error: Range data couldn't be loaded."

    # Get Tracks - or error if none found
    fbxTracks = RangeTools.GetTracksForFbx(fbxName, rangeDict)
    if not fbxTracks:
        return "Error: No ranges found for this FBX."

    # Check Range Dict for Fbx Name - or try to use name from previous autoclips
    startClips = GetAutoClips()

    # Setup Indexes
    clipIndex = 0
    if startClips:
        clipIndex = max([int(clip.Name.lower().split("autoclip")[1].split("_")[0]) for clip in startClips]) + 1
    clipEnd = 0

    # Get Old Clip Dict - keys: cameras, values: list of tuples(frame, number)
    oldClipDict = {}
    for clip in startClips:
        clipCamera = clip.ShotCamera
        if clipCamera:
            if clipCamera not in oldClipDict.iterkeys():
                oldClipDict[clipCamera] = []
            clipFrame = clip.Start.GetFrame()
            clipNumber = int(clip.Name.lower().split("autoclip")[1].split("_")[0])
            clipTuple = (clipFrame, clipNumber)
            oldClipDict[clipCamera].append(clipTuple)
    # Sort Old Clip Dict - by (start) frame from tuple list
    for clipCamera, clipTuples in oldClipDict.iteritems():
        oldClipDict[clipCamera] = sorted(clipTuples, key=lambda clipTuple: clipTuple[0])

    # Delete Old Clips and Disable Locked Shot
    DeleteAutoClips()
    Globals.Story.LockedShot = False

    # TODO: New simple edit for cinematic exporter - might want to split from older cutscene exporter logic soon?
    useSimpleEdit = True if capPaths.project == "AMERICAS" and "dev/animation/cinematic" in fbxPath.lower() else False

    # Main Loop - Creates tracks and clips
    trackClips = []
    for trackItem in fbxTracks:

        # Detect or Create Shot Track
        trackName = "EditTrack" if useSimpleEdit else "Shot_{0}_EST".format(trackItem.index + 1)
        matchedTrack = [track for track in Globals.Story.RootEditFolder.Tracks if track.Name == trackName]
        if matchedTrack:
            shotTrack = matchedTrack[0]
        else:
            shotTrack = mobu.FBStoryTrack(mobu.FBStoryTrackType.kFBStoryTrackShot, Globals.Story.RootEditFolder)
            shotTrack.Name = trackName

        # Range Loop - reset valid clips each track
        trackClips = trackClips if useSimpleEdit else []
        for rangeItem in trackItem.ranges:
            rangeStart = rangeItem.start
            rangeEnd = rangeItem.end

            # Switcher Loop - get clip info for cameras in the switcher within this range
            for keyIndex, key in enumerate(switcherKeys):
                clipStart = key.Time.GetFrame() if key.Time.GetFrame() > rangeStart else rangeStart
                clipEnd = switcherKeys[keyIndex+1].Time.GetFrame() if keyIndex+1 < len(switcherKeys) else rangeEnd
                clipEnd = rangeEnd + 1 if rangeEnd <= clipEnd else clipEnd
                if clipStart < rangeEnd and clipEnd > rangeStart:

                    # Valid Clip Range Found - get our story frame, camera, and clip name
                    storyFrame = trackClips[-1].Stop.GetFrame() if trackClips else rangeStart
                    if useSimpleEdit and not trackClips:
                        storyFrame = 0
                    camera = camList[int(key.Value - 1)]
                    if camera in oldClipDict.iterkeys() and oldClipDict[camera]:
                        clipName = "AutoClip{0}_{1}".format(oldClipDict[camera][0][1], fbxName)
                        oldClipDict[camera].pop(0)
                    else:
                        clipName = "AutoClip{0}_{1}".format(clipIndex, fbxName)
                        clipIndex += 1

                    # Create New Clip
                    newClip = mobu.FBStoryClip(camera, shotTrack, mobu.FBTime(0, 0, 0, clipStart))
                    newClip.Name = clipName
                    newClip.Start = mobu.FBTime(0, 0, 0, storyFrame)
                    newClip.Stop = mobu.FBTime(0, 0, 0, storyFrame + clipEnd - clipStart)
                    newClip.ShotActionStart = mobu.FBTime(0, 0, 0, clipStart)
                    newClip.ShotActionStop = mobu.FBTime(0, 0, 0, clipEnd)
                    trackClips.append(newClip)

    # TODO - Below range check should look at all clips, not just the last one we made
    # Check Hard End Range - update if our last clip end range is > the hard end range
    hardEndRange = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
    if clipEnd > hardEndRange:
        SetHardRanges(clipEnd)

    # GTA5 Time Shift - might need error handling?
    if capPaths.project == "GTA5":
        ShiftGTA5Events()
    
    # Restore FPS
    mobu.FBPlayerControl().SetTransportFps(previousFps)

    # Success - return true
    return True


def CreateFrameXml():
    # Check For Ranges Update
    CheckForRangeUpdate()

    # Get Switcher Dict - forcing 30fps for now
    previousFps = mobu.FBPlayerControl().GetTransportFps()
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)
    switcherDict = GetSwitcherData()
    mobu.FBPlayerControl().SetTransportFps(previousFps)

    # Write Frame Xml - and show result message
    fbxPath = mobu.FBApplication().FBXFileName
    rangeStatus = RangeTools.WriteRangeXml(fbxPath, switcherDict)
    successTxt = "{0}Frame xml created.\n\n(Added to your pending changelist)".format(" "*5)
    msgTxt = successTxt if rangeStatus is True else rangeStatus
    errorState = False if rangeStatus is True else True
    ShowMessage(msgTxt, "Create Frame Xml", errorState)


def LoadIngameClips():
    # Force 30FPS - during clip creation for now
    previousFps = mobu.FBPlayerControl().GetTransportFps()
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)

    # TODO: This quick rewrite I did ages ago is still hardcoded to RDR! No error handling!
    fbxPath = mobu.FBApplication().FBXFileName.replace("\\", "/")
    fbxName = os.path.splitext(os.path.split(fbxPath)[1])[0].upper()
    rangeDict = RangeTools.GetIngameRanges(fbxPath)

    DeleteAutoClips()
    Globals.Story.LockedShot = False
    takeNames = [take.Name.upper() for take in mobu.FBSystem().Scene.Takes]
    camList = [cam for cam in Globals.Cameras if not cam.SystemCamera]

    clipEnd = 0
    clipIndex = 0
    validTracks = []
    for scene in rangeDict.itervalues():
        for track in scene.tracks:
            if track.name.split(":")[0] == fbxName:
                validTracks.append(track)

    # Main Loop - Creates tracks and clips
    for trackItem in validTracks:
        # Set Current Take
        takeName = trackItem.name.split(":")[1]
        if takeName not in takeNames:
            # TODO: Maybe warn user here if take not found?
            continue
        takeIndex = takeNames.index(takeName)
        mobu.FBSystem().CurrentTake = mobu.FBSystem().Scene.Takes[takeIndex]

        # Setup Switcher
        CleanSwitcherKeys()
        switcherKeys = list(Globals.CameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys)
        if not switcherKeys:
            continue

        # Detect or Create Shot Track
        trackName = "{0}_EST".format(takeName)
        matchedTrack = [track for track in Globals.Story.RootEditFolder.Tracks if track.Name == trackName]
        if matchedTrack:
            shotTrack = matchedTrack[0]
        else:
            shotTrack = mobu.FBStoryTrack(mobu.FBStoryTrackType.kFBStoryTrackShot, Globals.Story.RootEditFolder)
            shotTrack.Name = trackName

        # Range Loop - reset valid clips each track
        trackClips = []
        for rangeItem in trackItem.ranges:
            rangeStart = rangeItem.start
            rangeEnd = rangeItem.end

            # Switcher Loop - get clip info for cameras in the switcher within this range
            for keyIndex, key in enumerate(switcherKeys):
                clipStart = key.Time.GetFrame() if key.Time.GetFrame() > rangeStart else rangeStart
                clipEnd = switcherKeys[keyIndex + 1].Time.GetFrame() if keyIndex + 1 < len(switcherKeys) else rangeEnd
                clipEnd = rangeEnd + 1 if rangeEnd <= clipEnd else clipEnd
                if clipStart < rangeEnd and clipEnd > rangeStart:

                    # Valid Clip Range Found - get our story frame, camera, and clip name
                    storyFrame = trackClips[-1].Stop.GetFrame() if trackClips else rangeStart
                    camera = camList[int(key.Value - 1)]
                    # if camera in oldClipDict.iterkeys() and oldClipDict[camera]:
                    #     clipName = "AutoClip{0}_{1}".format(oldClipDict[camera][0][1], fbxName)
                    #     oldClipDict[camera].pop(0)
                    # else:
                    clipName = "AutoClip{0}_{1}".format(clipIndex, fbxName)
                    clipIndex += 1

                    # Create New Clip
                    newClip = mobu.FBStoryClip(camera, shotTrack, mobu.FBTime(0, 0, 0, clipStart))
                    newClip.Name = clipName
                    newClip.Start = mobu.FBTime(0, 0, 0, storyFrame)
                    newClip.Stop = mobu.FBTime(0, 0, 0, storyFrame + clipEnd - clipStart)
                    newClip.ShotActionStart = mobu.FBTime(0, 0, 0, clipStart)
                    newClip.ShotActionStop = mobu.FBTime(0, 0, 0, clipEnd)
                    trackClips.append(newClip)

    # TODO - Below range check should look at all clips, not just the last one we made
    # Check Hard End Range - update if our last clip end range is > the hard end range
    hardEndRange = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
    if clipEnd > hardEndRange:
        SetHardRanges(clipEnd)
    
    # Restore FPS
    mobu.FBPlayerControl().SetTransportFps(previousFps)


def StepThroughClips(forward=True):
    clipFrames = [clip.ShotActionStart.GetFrame() for clip in GetAutoClips()]
    clipFrames = sorted(set(clipFrames))
    clipFrames = clipFrames[::-1] if not forward else clipFrames
    currentFrame = Globals.System.LocalTime.GetFrame()
    for clipFrame in clipFrames:
        if (forward and currentFrame < clipFrame) or (not forward and currentFrame > clipFrame):
            Globals.Player.Goto(mobu.FBTime(0, 0, 0, clipFrame))
            break


def SoftFrameClips():
    # Get Selected Clips - or use the one active on the the current frame
    autoClips = GetAutoClips()
    if not autoClips:
        return
    selectedClips = [clip for clip in autoClips if clip.Selected]
    if not selectedClips:
        currentFrame = Globals.System.LocalTime.GetFrame()
        for clip in autoClips:
            if clip.ShotActionStart.GetFrame() <= currentFrame < clip.ShotActionStop.GetFrame():
                selectedClips.append(clip)

    # Get Min and Max from Selected Clips
    if selectedClips:
        clipMin = selectedClips[0].ShotActionStart.GetFrame()
        clipMax = selectedClips[0].ShotActionStop.GetFrame()
        if len(selectedClips) > 1:
            for clip in selectedClips[1:]:
                newMin = clip.ShotActionStart.GetFrame()
                newMax = clip.ShotActionStop.GetFrame()
                clipMin = newMin if newMin < clipMin else clipMin
                clipMax = newMax if newMax > clipMax else clipMax
        SetSoftRanges(clipMin, clipMax)


def GetSwitcherData():
    switcherData = {}
    camList = [cam for cam in Globals.Cameras if not cam.SystemCamera]
    switcherKeys = list(Globals.CameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys)
    for key in switcherKeys:
        frame = key.Time.GetFrame()
        camera = camList[int(key.Value - 1)].Name
        switcherData[frame] = camera
    return switcherData


def ShiftGTA5Events():
    """Finds the current start frame in the data.cutpart, finds any ped var or blocking bound events,
    looks at the current EST track in Story, compares the start frame against the last export, then
    moves the ped vars or blocking bounds to match the new start frame"""
    
    # Get track start of shot 1
    estTrackOneStart = None
    validTracks = [track for track in mobu.FBStory().RootEditFolder.Tracks if "shot_1" in track.Name.lower()]
    if validTracks:
        validClips = [clip for clip in validTracks[0].Clips if clip.Name.lower().startswith("autoclip")]
        if validClips:
            estTrackOneStart = validClips[0].Start.GetFrame()
    if not estTrackOneStart:
        return
    
    # Find dataXmlPath
    fbxPath = mobu.FBApplication().FBXFileName.lower()
    capPaths = CapturePaths.FastPath(fbxPath)
    dataXmlPath = os.path.join(capPaths.cutsPath, "data.cutxml")

    # Get Object Ids and Shot Starts
    objectIds = RangeTools.GetGTA5ObjectIds(dataXmlPath)
    shotStarts = RangeTools.GetGTA5ShotStarts(dataXmlPath)
    if not objectIds or not shotStarts:
        return

    # Find the value of new start, minus old start, divided by 30
    dataShotOneStart = shotStarts[0]
    shotChangeDiff = estTrackOneStart - dataShotOneStart
    timeShift = None
    if shotChangeDiff != 0:
        timeShift = round((float(shotChangeDiff) / 30), 6)
        if timeShift < 0:
            # Not sure why, but anything that needs to be shifted backwards is always a frame out. Adding one more frame.
            timeShift = timeShift - float(0.033333)
    if timeShift == None:
        return

    # Update dataXml
    RangeTools.UpdateGTA5DataXml(dataXmlPath, objectIds, timeShift)

    # Reload dataXml - and a janky import here, would rather avoid importing RexExport across the board
    from RS.Core.Export.Export import RexExport
    RexExport.ImportCutFile(dataXmlPath)


def ShowMessage(msgTxt, title, error=False):
    # Janky Import - to ensure Application calls only happen when UI required
    from RS.Tools.UI import Application

    # Create Message Box
    msgBox = QtWidgets.QMessageBox()
    boxIcon = QtWidgets.QMessageBox.Critical if error else QtWidgets.QMessageBox.Information
    msgBox.setWindowTitle(title)
    msgBox.setText(msgTxt)
    msgBox.setIcon(boxIcon)

    # Position Message Box
    mainWindow = Application.GetMainWindow()
    mainPos = mainWindow.pos()
    size = mainWindow.size()
    boxPos = QtCore.QPoint(mainPos.x() + (size.width() / 2), mainPos.y() + (size.height() / 2))
    msgBox.move(boxPos)
    msgBox.exec_()


def Run():
    # Load Clips
    msgTxt = LoadClips()
    errorState = True if msgTxt.lower().startswith("error") else False
    ShowMessage(msgTxt, "Update Clips", errorState)
