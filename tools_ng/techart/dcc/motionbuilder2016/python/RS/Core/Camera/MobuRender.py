import os
import shutil
import pyfbsdk as mobu

from RS import Globals, Config
from RS.Core.Automation.FrameCapture import VideoTools
from RS.Core.Scene.widgets import ComponentView
from RS.Tools.UI import Run
import RS.Utils.UserPreferences as userPref

# Janky Pyside Import - allows same code on all projects
try:
    from PySide2 import QtCore, QtGui, QtWidgets
    qtProxyModel = QtCore.QSortFilterProxyModel
    qIcon = QtGui.QIcon
except ImportError:
    from PySide import QtCore, QtGui as QtWidgets
    qtProxyModel = QtWidgets.QSortFilterProxyModel
    qIcon = QtWidgets.QIcon

ICONSROOT = Config.Script.Path.ToolImages
FOLDERICON = os.path.join(ICONSROOT, "ReferenceEditor", "Folder.png")


def ShowOtherHuds(show=True):
    currentCam = mobu.FBSystem().Renderer.GetCameraInPane(0)
    for camHud in currentCam.HUDs:
        for hudElement in camHud.Elements:
            if "FbxNameText" not in hudElement.Name:
                hudElement.Show = show


def AddFbxNameHudItems():
    DeleteFbxNameHudItems()
    fbxPath = mobu.FBApplication().FBXFileName
    fbxName = os.path.splitext(os.path.split(fbxPath)[1])[0]

    currentCam = mobu.FBSystem().Renderer.GetCameraInPane(0)
    camHuds = list(currentCam.HUDs)
    if not camHuds:
        currentHud = mobu.FBHUD("FbxNameHud")
        mobu.FBSystem().Scene.ConnectSrc(currentHud)
    else:
        currentHud = camHuds[0]

    hudText = mobu.FBHUDTextElement("FbxNameText")
    hudText.Content = fbxName
    hudText.Height = 7
    hudText.X = 1
    hudText.Y = 7
    currentHud.ConnectSrc(hudText)
    currentCam.ConnectSrc(currentHud)

    # Hide Other Huds
    ShowOtherHuds(show=False)


def DeleteFbxNameHudItems():
    deleteHuds = []
    deleteElements = []
    currentCam = mobu.FBSystem().Renderer.GetCameraInPane(0)
    for camHud in currentCam.HUDs:
        if camHud.Name.startswith("FbxNameHud"):
            deleteHuds.append(camHud)
        for hudElement in camHud.Elements:
            if hudElement.Name.startswith("FbxNameText"):
                deleteElements.append(hudElement)
    deleteList = deleteHuds + deleteElements
    for deleteItem in deleteList:
        deleteItem.FBDelete()

    # Show Other Huds
    ShowOtherHuds(show=True)


def MobuRenderCore(videoPath, fbxPath):
    # Delete Previous Render
    if os.path.exists(videoPath):
        try:
            os.unlink(videoPath)
        except:
            videoName = os.path.split(videoPath)[-1]
            return "Error: Video locked - close any applications using {0}".format(videoName)

    # Setup Frame Folder and Path
    frameFolder = "x:\\renderTemp"
    if os.path.exists(frameFolder):
        shutil.rmtree(frameFolder)
    os.makedirs(frameFolder)
    framePath = "x:\\renderTemp\\frame-.jpg"

    # Set Render Options
    renderOptions = mobu.FBVideoGrabber().GetOptions()
    renderOptions.CameraResolution = renderOptions.CameraResolution.kFBResolutionHD
    renderOptions.RenderAudio = True
    renderOptions.AudioRenderFormat = 2113538  # Code for 48K
    renderOptions.ShowTimeCode = True
    renderOptions.ShowSafeArea = False
    renderOptions.ShowCameraLabel = False
    renderOptions.AntiAliasing = False
    renderOptions.TimeSpan = mobu.FBTimeSpan(mobu.FBTime(0, 0, 0, 0), mobu.FBTime(0, 0, 0, 0))
    renderOptions.TimeSteps = mobu.FBTime(0, 0, 0, 1)
    renderOptions.OutputFileName = framePath

    # Force 30 FPS
    previousFps = mobu.FBPlayerControl().GetTransportFps()
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)

    # Add Fbx Name Hud
    AddFbxNameHudItems()
    # todo: may need tweaks for switcher render

    # Render Frames
    Globals.Application.FileRender(renderOptions)

    # todo: can we set the box back to .mov path afterwards?
    # renderOptions.OutputFileName = videoPath

    # Delete Fbx Name Hud
    DeleteFbxNameHudItems()

    # Get Frame Path and Cam Name
    firstFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
    frameString = VideoTools.AddZeroesToNumber(firstFrame, 4)
    framePath = "x:\\renderTemp\\frame-{0}.jpg".format(frameString)
    camName = os.path.splitext(os.path.split(videoPath)[1])[0].split("-")[-1]

    # Convert Frames to Video - via ffmpeg
    VideoTools.CreateRdrAvidVideo(fbxPath, framePath, videoPath, noteText=camName)

    # Delete Temp Frames
    if os.path.exists(frameFolder):
        shutil.rmtree(frameFolder)

    # Restore FPS
    mobu.FBPlayerControl().SetTransportFps(previousFps)

    # Verify Video Exists - or show error
    if os.path.exists(videoPath) is False:
        return "Error: Video conversion failed. Contact Blake Buck in Techart."

    # Render Success
    return True


class FileProxyModel(qtProxyModel):
    """
    ProxyModel to allow users to see files in the file dialog, but only select folders.
    """
    def __init__(self, parent=None):
        super(FileProxyModel, self).__init__(parent=parent)

    def flags(self, index):
        index = self.mapToSource(index)
        if not self.sourceModel().isDir(index):
            return QtCore.Qt.ItemIsEnabled
        return self.sourceModel().flags(index)


class MobuRenderMenu(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(MobuRenderMenu, self).__init__(parent=parent)
        self.renderFolder = self.GetRenderFolder()
        self.renderFolderText = QtWidgets.QLabel(self.renderFolder)
        self.camViewer = ComponentView.ComponentView(mobu.FBCamera)
        self.useTakeName = False
        self.SetupUi()

    def GetRenderFolder(self):
        defaultPath = "x:\\"
        savedPath = userPref.__Readini__("renderPaths", "avidrender", notFoundValue=None)
        if savedPath:
            return savedPath
        return defaultPath

    def BrowseFolderPressed(self):
        fileDialog = QtWidgets.QFileDialog()
        fileProxy = FileProxyModel()
        fileDialog.setProxyModel(fileProxy)
        fileDialog.setWindowTitle("Select Render Folder")

        widgetCenter = self.window().frameGeometry().center()
        frameOffset = (self.window().frameGeometry().y() - self.window().geometry().y()) / 2
        fileDialog.move(widgetCenter.x() - fileDialog.width() / 2,
                        widgetCenter.y() - fileDialog.height() / 2 + frameOffset)

        fileDialog.setFileMode(QtWidgets.QFileDialog.Directory)
        fileDialog.setDirectory(self.renderFolder)
        if fileDialog.exec_():
            self.renderFolder = fileDialog.selectedFiles()[0]
            self.renderFolderText.setText(self.renderFolder)
        del fileDialog

    def RenderPressed(self):
        # todo: validation checks - maybe gray out render if no cams or fbx path?
        cams = self.camViewer.selectedComponents()
        if not cams:
            return
        fbxPath = mobu.FBApplication().FBXFileName
        if not fbxPath:
            return
        userPref.__Saveini__("renderPaths", "avidrender", self.renderFolder)

        # Get Name Start - either fbx or take name
        nameStart = os.path.splitext(os.path.split(fbxPath)[1])[0]
        if self.useTakeName:
            nameStart = mobu.FBSystem().CurrentTake.Name

        # Get Render Pane Info
        renderer = Globals.System.Renderer
        if Config.Project.Name.lower() == "gta5":
            # GTA5 Force 1 Pane - as GetSelectedPaneIndex not available in Mobu 2016
            renderer.SetPaneCount(1)
            paneIndex = 0
        else:
            paneIndex = renderer.GetSelectedPaneIndex()
        paneCam = renderer.GetCameraInPane(paneIndex)

        for cam in cams:
            videoName = "{0}-{1}.mov".format(nameStart, cam.Name)
            videoPath = os.path.join(self.renderFolder, videoName)
            renderer.SetCameraInPane(cam, paneIndex)
            mobu.FBSystem().Scene.Evaluate()
            result = MobuRenderCore(videoPath, fbxPath)
            # todo: needs error handling here - add popup message

        # Restore Pane Cam
        renderer.SetCameraInPane(paneCam, paneIndex)

    def TimeEstimate(self):
        endFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
        totalFrames = endFrame - mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        minutes = 0
        seconds = totalFrames / 7.5
        while seconds > 60:
            minutes += 1
            seconds -= 60
        if minutes:
            return "{0} min {1} sec".format(minutes, int(seconds))
        else:
            return "{0} sec".format(int(seconds))

    def SetupUi(self):
        mainLayout = QtWidgets.QVBoxLayout()
        self.setLayout(mainLayout)

        self.camViewer.setSelectionMode(self.camViewer.MultiSelection)
        self.camViewer.setRootIsDecorated(False)
        self.camViewer.setHeader("Cameras")
        mainLayout.addWidget(self.camViewer)

        # Take Name Checkbox
        useTakeNameCheckbox = QtWidgets.QCheckBox("Use Take Name (instead of fbx name)")
        fbxPath = mobu.FBApplication().FBXFileName.replace("\\", "/").lower()
        if "/ingame/" in fbxPath:
            self.useTakeName = True
            useTakeNameCheckbox.toggle()
        useTakeNameCheckbox.stateChanged.connect(self.UseTakeNamePressed)
        mainLayout.addWidget(useTakeNameCheckbox)

        # Setup Output Folder Box
        renderFolderLayout = QtWidgets.QHBoxLayout()
        folderLabelText = QtWidgets.QLabel("Render Folder:")
        self.renderFolderText.setFrameStyle(QtWidgets.QFrame.Panel | QtWidgets.QFrame.Sunken)
        self.renderFolderText.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        renderFolderLayout.addWidget(folderLabelText)
        renderFolderLayout.addWidget(self.renderFolderText, stretch=1)

        # Browse Button
        browseFolderButton = QtWidgets.QPushButton()
        browseFolderIcon = qIcon(FOLDERICON)
        browseFolderButton.setIcon(browseFolderIcon)
        browseFolderButton.setMaximumWidth(30)
        browseFolderButton.clicked.connect(self.BrowseFolderPressed)
        renderFolderLayout.addWidget(browseFolderButton)
        mainLayout.addLayout(renderFolderLayout)

        # Render Button
        renderButton = QtWidgets.QPushButton("Render")
        renderButton.clicked.connect(self.RenderPressed)
        mainLayout.addWidget(renderButton)

        # Time Estimate
        timeString = self.TimeEstimate()
        timeText = QtWidgets.QLabel("Estimate: {0} per cam".format(timeString))
        timeText.setAlignment(QtCore.Qt.AlignCenter)
        mainLayout.addWidget(timeText)

    def UseTakeNamePressed(self, stateInt):
        self.useTakeName = False if stateInt is 0 else True


@Run("MultiCam Render", url="https://hub.rockstargames.com/display/ANIM/MultiCam+Render+Tool")
def Run():
    wid = MobuRenderMenu()
    wid.resize(540, 600)
    return wid
