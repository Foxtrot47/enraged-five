import os
from datetime import datetime
from xml.dom import minidom
import pyfbsdk as mobu

import RS.Config
import RS.Perforce
from RS.Core.Camera import CamUtils, CamValidationTools, SaveCameras
from RS.Core.Camera.KeyTools import camKeySaver
from RS.Core.Automation.FrameCapture import CaptureModel, CapturePaths, FileTools, JobDbTools

from RS.Core.Scene.widgets import ComponentView
from RS.Core.Scene.models.modelItems.ComponentModelItem import ComponentItem

# Janky Pyside Import - allows same code on all projects
try:
    from PySide2 import QtCore, QtGui, QtWidgets
    qtProxyModel = QtCore.QSortFilterProxyModel
except ImportError:
    from PySide import QtCore, QtGui as QtWidgets
    qtProxyModel = QtWidgets.QSortFilterProxyModel


class CamProxyModel(qtProxyModel):
    """
    ProxyModel to allow users to see files in the file dialog, but only select folders.
    """
    def __init__(self, parent=None):
        super(CamProxyModel, self).__init__(parent=parent)

    def filterAcceptsRow(self, row, parent=None):
        # index = self.index(row, 0, parent)
        index = self.sourceModel().index(row, 0, parent)
        cam = index.data(ComponentItem.Component)
        if cam.Name == "ExportCamera":
            return False
        return True


class CamSelector(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(CamSelector, self).__init__(parent=parent)
        self.camViewer = ComponentView.ComponentView(mobu.FBCamera)
        self.camViewer.setFilterModel(CamProxyModel())
        self.selection = None
        self.SetupUi()

    def SetupUi(self):
        mainLayout = QtWidgets.QVBoxLayout()
        # mainLayout.setSpacing(30)
        self.setLayout(mainLayout)

        switcherCapButton = QtWidgets.QPushButton("Switcher Capture")
        switcherCapButton.setMinimumHeight(50)
        switcherCapButton.clicked.connect(self.SwitcherPressed)
        mainLayout.addWidget(switcherCapButton)

        lineDivider = QtWidgets.QFrame()
        lineDivider.setFrameShape(QtWidgets.QFrame.HLine)
        lineDivider.setFrameShadow(QtWidgets.QFrame.Sunken)
        mainLayout.addWidget(lineDivider)

        camSelectLayout = QtWidgets.QHBoxLayout()
        camSelectLayout.setSpacing(10)
        # camSelectLayout.setMargin(0)
        mainLayout.addLayout(camSelectLayout)

        self.camViewer.setSelectionMode(self.camViewer.MultiSelection)
        self.camViewer.setRootIsDecorated(False)
        self.camViewer.setHeader("Cameras")
        camSelectLayout.addWidget(self.camViewer)

        camCapButton = QtWidgets.QPushButton("Camera\nCapture")
        camCapButton.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        camCapButton.clicked.connect(self.CamCapPressed)
        camSelectLayout.addWidget(camCapButton)

    def SwitcherPressed(self):
        self.selection = "SWITCHER"
        self.close()

    def CamCapPressed(self):
        cams = self.camViewer.selectedComponents()
        if not cams:
            return
        self.selection = [cam.Name for cam in cams]
        self.close()


def CheckCamRigs():
    camRigNames = []
    camRigDict = camKeySaver.GetCamRigs()
    for camName, rigName in camRigDict.iteritems():
        rigType = "constrained" if rigName.startswith("C:") else "parented"
        rigText = "{0} : {1} ({2})".format(camName, rigName.split(":")[1], rigType)
        camRigNames.append(rigText)
    return camRigNames


def QueueCaptureRender():
    # Get Job Paths and Data
    fbxPath = mobu.FBApplication().FBXFileName
    fbxName = os.path.splitext(os.path.basename(fbxPath))[0].upper()
    capPaths = CapturePaths.FastPath(fbxPath)
    projectName = capPaths.project
    capDataRoot = capPaths.capDataRoot
    dataCutXmlPath = os.path.join(capDataRoot, "CutFiles", "data.cutxml")
    if capPaths.project == "AMERICAS":
        # todo: ideally we'd check the .rexcin file here, but we need .ias as well - which we weren't saving before
        # dataCutXmlPath = "{0}\\CutFiles\\{1}.rexcin".format(capDataRoot, capPaths.fbxName.upper())
        dataCutXmlPath = "{0}\\IasFiles\\{1}.ias".format(capDataRoot, capPaths.fbxName)
    logFolder = os.path.join(capDataRoot, "LogFiles")
    jobXmlPath = os.path.join(capPaths.expJobQueue, "{}.xml".format(fbxName))
    userEmail = RS.Config.User.Email

    # Check Production Drive Access
    if not os.path.exists(capPaths.expJobQueue):
        mobu.FBMessageBox("Capture Render",
                          "Error: The job folder on the production drive could not be accessed.", "Okay")
        return "ERROR"

    # Detect Valid FBX
    fbxStatus = CamValidationTools.CheckFbxPath(fbxPath)
    if fbxStatus != "VALID":
        messageText = "Invalid FBX: " + fbxStatus
        mobu.FBMessageBox("Capture Render", messageText, "Okay")
        return "ERROR"

    # Detect Old EndFrame
    oldEndFrame = None
    if os.path.exists(logFolder):
        jobXmls = [os.path.join(logFolder, log) for log in os.listdir(logFolder) if log.lower().endswith(".xml")]
        if jobXmls:
            latestLog = max(jobXmls, key=os.path.getmtime)
            oldCap = CaptureModel.MobuCaptureModel(latestLog)
            oldEndFrame = getattr(oldCap, "endFrame", None)
            oldEndFrame = None if oldEndFrame.lower() == "none" else oldEndFrame

    # Fix Invalid FPS
    currentFps = mobu.FBPlayerControl().GetTransportFps()
    if currentFps not in [mobu.FBTimeMode.kFBTimeMode30Frames, mobu.FBTimeMode.kFBTimeMode60Frames]:
        mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)

    # Check for EndFrame Change
    endFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
    if mobu.FBPlayerControl().GetTransportFps() == mobu.FBTimeMode.kFBTimeMode60Frames:
        endFrame /= 2
    endFrame = str(endFrame)
    endFrameUpdated = False
    if oldEndFrame and (int(oldEndFrame) != int(endFrame)):
        msgTxt = "Endrange has changed. Use previous range ({0}) or new range ({1})?".format(oldEndFrame, endFrame)
        msgTxt += "\n\nWarning: Changing endrange breaks batch import, requires Full capture.\n"
        userChoice = mobu.FBMessageBox("Capture Sender", msgTxt, "Keep Old Endrange", "Use New Endrange")
        if userChoice == 1:
            endFrame = oldEndFrame
        elif userChoice == 2:
            endFrameUpdated = True

    # Check for Cam Rigs
    captureType = "FULL"
    camRigs = CheckCamRigs()
    if camRigs:
        camRigs = camRigs[:10] if len(camRigs) > 10 else camRigs
        boxMessage = "Cameras are attached to a moving cutscene rig.\n\n"
        boxMessage += "    {0}\n\n".format("\n    ".join(camRigs))
        boxMessage += "  Would you like to capture using the cam rig?"
        userChoice = mobu.FBMessageBox("Capture Sender", boxMessage,
                                       "CAM RIG", "NORMAL", "CANCEL")
        if userChoice == 1:
            boxMessage = ("Cam Rig captures require the FBX to be saved submitted to P4 manually.\n\n"
                          "        Have you saved and finished submitting your latest changes?")
            hasSaved = mobu.FBMessageBox("Capture Sender", boxMessage, "YES", "NO")
            if hasSaved == 1:
                captureType = "SIMPLE"
            else:
                return
        elif userChoice == 3:
            return

    # Check for Fast Capture
    if captureType == "FULL" and endFrameUpdated is False and os.path.exists(dataCutXmlPath):
        lastCaptureTime = datetime.fromtimestamp(os.path.getctime(dataCutXmlPath)).strftime('%m/%d/%Y %H:%M:%S')
        userChoice = mobu.FBMessageBox("Capture Sender", "               SELECT CAPTURE TYPE\n\n" +
                                       "Full Capture - Latests cams & animations\n" +
                                       "Fast Capture - Latest cams only\n\n" +
                                       "Last full capture on " + lastCaptureTime,
                                       "FULL CAP", "FAST CAP", "CANCEL")
        if userChoice == 2:
            captureType = "FAST"
        elif userChoice == 3:
            return

    # Create Job Dict - to send data to WriteJobXml
    jobDict = {"xmlPath": jobXmlPath, "projectName": projectName, "fbxName": fbxName, "fbxPath": fbxPath,
               "endFrame": endFrame, "captureType": captureType, "userEmail": userEmail, "jobId": None, "camName": None}

    # Cam Selector Popup
    wid = CamSelector()
    wid.exec_()
    if wid.selection is None:
        # Canel - if user closes window
        return
    if wid.selection == "SWITCHER":
        # Switcher Selected
        WriteJobXml(jobDict)
        msgTxt = "Switcher capture queued."
    elif type(wid.selection) == list:
        # Camera Capture Selected
        for camName in wid.selection:
            # TODO: figure out a consistent way of cleaning these up, detect correctly during captures?
            # camName = camName.lower().replace("_", "")
            # camName = camName[:-4] if camName.endswith("_cam") else camName

            # Update Dict and Write JobXml
            jobXmlPath = os.path.join(capPaths.expJobQueue, "{0}-{1}.xml".format(fbxName, camName.lower()))
            jobDict["xmlPath"] = jobXmlPath
            jobDict["camName"] = camName
            WriteJobXml(jobDict)
        msgTxt = "{} camera captures queued.".format(len(wid.selection))

    # Success Popup
    mobu.FBMessageBox("Capture Render", msgTxt, "OK")
    return "SUCCESS"


def WriteJobXml(jobDict):
    # Read Dict
    fbxPath = jobDict["fbxPath"]
    jobXmlPath = jobDict["xmlPath"]
    jobName = os.path.splitext(os.path.split(jobXmlPath)[1])[0]
    userEmail = jobDict["userEmail"]
    userName = userEmail.lower().split("@")[0].replace(".", " ").title()

    # Delete Existing Queue Xml
    FileTools.DeletePath(jobXmlPath)

    # Create Job on Database - and only save jobId if job was created
    jobId = None
    try:
        jobId = JobDbTools.CreateDbJob(jobName, userName, fbxPath)
        jobId = str(jobId)
    except:
        print "Warning: Problem connecting to database. Skipping user-side job creation."
    jobDict["jobId"] = jobId

    # Create Job Xml
    doc = minidom.Document()
    fbxInfo = doc.createElement("fbxInfo")
    doc.appendChild(fbxInfo)
    tagNames = ["projectName", "fbxName", "fbxPath", "endFrame", "captureType", "userEmail", "jobId", "camName"]
    for tagName in tagNames:
        tagValue = jobDict[tagName]
        if tagValue:
            tagNode = doc.createElement(tagName)
            textNode = doc.createTextNode(tagValue)
            tagNode.appendChild(textNode)
            fbxInfo.appendChild(tagNode)

    # Write Out Formatted Xml
    docPretty = doc.toprettyxml(indent="    ", encoding="utf-8")
    xmlFile = open(jobXmlPath, "w")
    xmlFile.write(docPretty)
    xmlFile.close()


def Run():
    camDataSaved = SaveCameras.Run()
    if camDataSaved:
        QueueCaptureRender()
