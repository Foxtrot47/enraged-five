###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_CameraDOFTransferMP3.py
## Written And Maintained By: David Bailey
## Contributors: -
## Description: This tool transfers DOF animation between cameras
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
from pyfbsdk import *
from pyfbsdk_additions import *
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gFromCamera = None
gToCamera = None

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: UI
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_SelectCameras( pControl, pEvent):
    
    global gFromCamera
    global gToCamera
    
    if pControl.Name == "region0":
        gFromCamera = pControl.Items[pControl.ItemIndex]
    else:
        gToCamera = pControl.Items[pControl.ItemIndex]

def rs_TransferDOFs( pControl, pEvent):
    
    global gFromCamera
    global gToCamera
    
    if gFromCamera != None and gToCamera != None:
        
        lFromCamera = FBFindModelByLabelName(gFromCamera)
        lToCamera = FBFindModelByLabelName(gToCamera)
        
        if lFromCamera and lToCamera:
            
            FBSystem().CurrentTake.SetCurrentLayer(0)
            
            for i in range(FBSystem().CurrentTake.GetLayerCount()):
                
                FBSystem().CurrentTake.SetCurrentLayer(i)
            
                if lFromCamera.PropertyList.Find("DOF Expand").IsAnimated() == True:  
                    
                    for iKey in lFromCamera.PropertyList.Find("DOF Expand").GetAnimationNode().FCurve.Keys:
                        
                        lToCamera.PropertyList.Find("DOF Expand").GetAnimationNode().KeyAdd(iKey.Time, iKey.Value)
                              
                if lFromCamera.PropertyList.Find("DOF Multiplier").IsAnimated() == True:
                    
                    for iKey in lFromCamera.PropertyList.Find("DOF Multiplier").GetAnimationNode().FCurve.Keys:
                        
                        lToCamera.PropertyList.Find("DOF Multiplier").GetAnimationNode().KeyAdd(iKey.Time, iKey.Value)
                    
                if lFromCamera.PropertyList.Find("DOF Strength").IsAnimated() == True:
                    
                    for iKey in lFromCamera.PropertyList.Find("DOF Strength").GetAnimationNode().FCurve.Keys:
                        
                        lToCamera.PropertyList.Find("DOF Strength").GetAnimationNode().KeyAdd(iKey.Time, iKey.Value)
                    
                if lFromCamera.PropertyList.Find("DOF Strength Override").IsAnimated() == True:
                    
                    for iKey in lFromCamera.PropertyList.Find("DOF Strength Override").GetAnimationNode().FCurve.Keys:
                        
                        lToCamera.PropertyList.Find("DOF Strength Override").GetAnimationNode().KeyAdd(iKey.Time, iKey.Value)
            
        else:  
           FBMessageBox( "Transfer DOF's Error", "Please Check The Cameras Exist In Scene", "Ok" ) 
        
    else:
        FBMessageBox( "Transfer DOF's Error", "Please Select A 'From' Camera (Top)\n       and a 'To' Camera (Bottom)", "Ok" )

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: UI
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_PopulateDOFTransferTool( pMainLyt):
    
    lX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
    lY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
    lW = FBAddRegionParam(200,FBAttachType.kFBAttachNone,"")
    lH = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
    pMainLyt.AddRegion("main","main", lX, lY, lW, lH)
    
    lVBoxLayout = VBoxLayout()
    pMainLyt.SetControl("main",lVBoxLayout)
    
    global gControls
    gControls = [FBList(), FBList()]
    for i in gControls:
        i.OnChange.Add(rs_SelectCameras)
        for iCamera in FBSystem().Scene.Cameras:
            if not iCamera.SystemCamera:
                lCamName = iCamera.Name
                i.Items.append(lCamName)
    
    gControls[0].Style = FBListStyle.kFBDropDownList
    lVBoxLayout.Add(gControls[0], 25)
    gControls[0].Selected(-1, True)
    
    gControls[1].Style = FBListStyle.kFBDropDownList
    lVBoxLayout.Add(gControls[1], 25)
    gControls[1].Selected(-1, True)
    
    lTransferButton = FBButton()
    lTransferButton.Caption = "-------------- Transfer DOF's --------------"
    lTransferButton.Justify = FBTextJustify.kFBTextJustifyLeft
    lVBoxLayout.Add(lTransferButton,30)
    lTransferButton.Justify = FBTextJustify.kFBTextJustifyCenter
    lTransferButton.OnClick.Add(rs_TransferDOFs)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: UI
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_DOFTransferTool():    

    lDOFTransferToolLayout = CreateUniqueTool("Transfer DOF's")    
    rs_PopulateDOFTransferTool(lDOFTransferToolLayout)  
    
    lDOFTransferToolLayout.StartSizeX = 218
    lDOFTransferToolLayout.StartSizeY = 130
      
    ShowTool(lDOFTransferToolLayout)

def Run():
    rs_DOFTransferTool()