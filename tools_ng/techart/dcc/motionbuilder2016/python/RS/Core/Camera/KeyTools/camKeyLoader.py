"""
Description:
    Lower level functions for loading data from a camInfoObj instance into the currently opened FBX.
    Assumes higher level setup steps have already happened, via camKeyTasks or otherwise.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""


import pyfbsdk as mobu

from RS import Globals
from RS.Core.Camera import Lib as CamLib, LibGTA5
from RS.Core.Automation.FrameCapture import CapturePaths
import camKeyDef


def LoadCamsIntoFbx(camInfoObj, loadExpCam=False):
    """
    Creates new cameras in the current FBX from the camList attribute of a camInfoObj instance.
    Arguments:
        camInfoObj: An instance of the camInfo object containing camera data.
    """
    # Get Project
    fbxPath = camInfoObj.FbxPath.replace("/", "\\")
    capPaths = CapturePaths.FastPath(fbxPath)
    project = capPaths.project

    # Create Layer List
    mbLayerList = []
    mbLayerCount = Globals.System.CurrentTake.GetLayerCount()
    for i in range(mbLayerCount):
        mbLayer = Globals.System.CurrentTake.GetLayer(i)
        mbLayer.Lock = False
        mbLayerList.append(mbLayer.Name)
    
    # Force 30 FPS
    previousFps = mobu.FBPlayerControl().GetTransportFps()
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)

    # CAMERA LOOP
    for eachCam in camInfoObj.CamList:

        # Skip Export Cam - unless allowed via arg
        if loadExpCam is False and "exportcamera" in eachCam.Name.lower():
            continue

        # Create Camera
        if eachCam.Name == "ExportCamera":
            mbCamObj = CamLib.MobuCamera.CreateExportCamera().camera
        else:
            mbCamObj = CamLib.MobuCamera.CreateRsCamera(specificName=eachCam.Name).camera

        # Detect Renamed Cam - and update camInfoObj
        if mbCamObj.Name != eachCam.Name:
            switcherKeys = [key for key in camInfoObj.SwitcherKeyList if key.Value == eachCam.Name]
            for key in switcherKeys:
                key.Value = mbCamObj.Name
            eachCam.Name = mbCamObj.Name

        # Set Cam's Quaternion Property
        if eachCam.Quaternion is None:
            mbCamObj.QuaternionInterpolate = False
        else:
            mbCamObj.QuaternionInterpolate = eachCam.Quaternion

        # PROPERTY LOOP
        for eachProp in eachCam.ChildList:

            if ("Translation (Lcl)" in eachProp.Name) or ("Rotation (Lcl)" in eachProp.Name):
                parentProp = mbCamObj.PropertyList.Find(eachProp.Name[:-1]).GetAnimationNode()
                axisNumber = 'XYZ'.index(eachProp.Name[-1:])
                mbCamPropNode = parentProp.Nodes[axisNumber]
            else:
                mbCamProp = mbCamObj.PropertyList.Find(eachProp.Name)
                if mbCamProp is None:
                    continue
                mbCamProp.SetAnimated(True)
                mbCamPropNode = mbCamProp.GetAnimationNode()

            # LAYER LOOP
            for eachLayer in eachProp.ChildList:
                if eachLayer.Name not in mbLayerList:
                    Globals.System.CurrentTake.CreateNewLayer()
                    newLayer = Globals.System.CurrentTake.GetLayer(len(mbLayerList))
                    newLayer.Name = eachLayer.Name
                    mbLayerList.append(newLayer.Name)

                layerIndex = Globals.System.CurrentTake.GetLayerByName(eachLayer.Name).GetLayerIndex()
                Globals.System.CurrentTake.SetCurrentLayer(layerIndex)
                currentKey = camKeyDef.KeyItem()

                # MAIN KEY LOOP - Sets time, value, and tangent modes
                for eachKey in eachLayer.ChildList:
                    # Update CurrentKey Object
                    for currentPropName in eachKey.Order:
                        currentPropValue = getattr(eachKey, currentPropName)
                        if currentPropValue is not None:
                            setattr(currentKey, currentPropName, currentPropValue)

                    # Create New Key
                    keyTime = mobu.FBTime()
                    keyTime.SetSecondDouble(float(currentKey.Time))
                    keyValue = float(currentKey.Value)
                    mbCamPropNode.KeyAdd(keyTime, keyValue)
                    mbKey = mbCamPropNode.FCurve.Keys[-1]

                    # Set Tangent Modes
                    tangentFlag = currentKey.TangentFlag
                    mbKey.Interpolation = mbKey.Interpolation.values[int(tangentFlag[:1])]
                    mbKey.TangentBreak = bool(int(tangentFlag[1:2]))
                    mbKey.TangentMode = mbKey.TangentMode.values[int(tangentFlag[-1:])]
                    mbKey.TangentConstantMode = mbKey.TangentConstantMode.values[int(tangentFlag[3:4])]
                    mbKey.TangentClampMode = mbKey.TangentClampMode.values[int(tangentFlag[2:3])]

                # Reset Current Key
                currentKey = camKeyDef.KeyItem()

                # SECOND KEY LOOP - Sets LeftRight tangent values
                for keyIndex, eachKey in enumerate(eachLayer.ChildList):
                    mbKey = mbCamPropNode.FCurve.Keys[keyIndex]
                    for propName in eachKey.Order[3:]:
                        propValue = getattr(eachKey, propName)
                        if propValue is None:
                            propValue = getattr(currentKey, propName)
                        else:
                            setattr(currentKey, propName, float(propValue))
                        if propValue is not None:
                            # Use Right Props for First Key - as they don't have left tangents
                            if keyIndex == 0 and "Left" in propName:
                                propName = propName.replace("Left", "Right")
                            setattr(mbKey, propName, float(propValue))

    # Set Shake Weight - if one exists
    if camInfoObj.ShakeWeight:
        shakeLayer = Globals.System.CurrentTake.GetLayerByName("ShakeCam")
        if shakeLayer is None:
            Globals.System.CurrentTake.CreateNewLayer()
            shakeLayer = Globals.System.CurrentTake.GetLayer(mbLayerCount)
            shakeLayer.Name = "ShakeCam"
            mbLayerCount += 1
        Globals.System.CurrentTake.SetCurrentLayer(shakeLayer.GetLayerIndex())
        shakeLayer.Weight = float(camInfoObj.ShakeWeight)

    # Select the Base Layer
    Globals.System.CurrentTake.SetCurrentLayer(0)
    
    # Restore FPS
    mobu.FBPlayerControl().SetTransportFps(previousFps)


def LoadSwitcherIntoFbx(camInfoObj):
    """
    Restores the switcher key data from the SwitcherKeyList attribute of a camInfoObj instance.
    Arguments:
        camInfoObj: An instance of the camInfo object containing switcher data.
    """
    # Select the Base Layer
    Globals.System.CurrentTake.SetCurrentLayer(0)
    
    # Force 30 FPS
    previousFps = mobu.FBPlayerControl().GetTransportFps()
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)
    
    # Create mbSwitcherNode and cameraNameList
    mbSwitcherNode = Globals.CameraSwitcher.PropertyList.Find('Camera Index').GetAnimationNode()
    cameraNameList = []
    for camera in Globals.System.Scene.Cameras:
        cameraNameList.append(camera.Name)
    cameraNameList = cameraNameList[7:]
    
    # Switcher Loop
    for keyIndex, keyData in enumerate(camInfoObj.SwitcherKeyList):

        # Set Key Time and Value - and force first key time to 0
        keyTime = int(keyData.Time)
        if keyIndex == 0 and keyTime > 0:
            keyTime = 0
        keyValue = float(cameraNameList.index(keyData.Value)) + 1.0
        
        # Create Key
        mbSwitcherNode.KeyAdd(mobu.FBTime(0, 0, 0, keyTime), keyValue)
        currentKey = mbSwitcherNode.FCurve.Keys[-1]
        
        # Set TangentOptions
        currentKey.Interpolation = currentKey.Interpolation.values[0]
        currentKey.TangentMode = currentKey.TangentMode.values[0]
    
    # Restore FPS
    mobu.FBPlayerControl().SetTransportFps(previousFps)


def LoadCamRigs(camInfoObj):
    """
    Restores any detected cam rigs from a camInfoObject.
    Arguments:
        camInfoObj (camKeyDef.CamInfo) - object instance containing the cam data to load
    """
    for camObj in camInfoObj.CamList:
        if camObj.CamRig:
            matchedCam = mobu.FBFindModelByLabelName(camObj.Name)
            # Parenting Logic
            if camObj.CamRig.startswith("P:"):
                rigRootName = "{0}:MASTER_CAMERA".format(camObj.CamRig.split(":")[1])
                matchedRig = mobu.FBFindModelByLabelName(rigRootName)
                if matchedCam and matchedRig:
                    cameraNull = [child for child in matchedRig.Children if "camera" in child.Name.lower()]
                    if cameraNull:
                        matchedCam.Parent = cameraNull[0]
            # Constraint Logic
            elif camObj.CamRig.startswith("C:"):
                rigConstName = "{0}:{1}_CONSTRAINT".format(camObj.CamRig.split(":")[1], camObj.Name)
                matchedConsts = [const for const in mobu.FBSystem().Scene.Constraints if const.LongName == rigConstName]
                if matchedCam and matchedConsts:
                    child = matchedConsts[0].ReferenceGet(0, 0)
                    if child:
                        matchedConsts[0].ReferenceRemove(0, child)
                    matchedConsts[0].ReferenceAdd(0, matchedCam)
                    matchedConsts[0].Active = True
