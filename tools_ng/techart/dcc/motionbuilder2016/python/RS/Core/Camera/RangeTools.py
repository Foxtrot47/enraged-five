import os
import xlrd
import subprocess
import cPickle
from xml.dom import minidom
from xml.etree import cElementTree as ET
from RS.Core.Automation.FrameCapture import CapturePaths, FileTools


class CutsceneItem(object):
    def __init__(self, sceneName):
        self.name = sceneName
        self.tracks = []

    def __eq__(self, other):
        return True if self.__dict__ == other.__dict__ else False


class TrackItem(object):
    def __init__(self, fbxName, trackIndex):
        self.name = fbxName
        self.index = trackIndex
        self.ranges = []

        self.mergeParts = []

    def __eq__(self, other):
        return True if self.__dict__ == other.__dict__ else False


class RangeItem(object):
    def __init__(self, startFrame, endFrame):
        self.start = startFrame
        self.end = endFrame

    def __eq__(self, other):
        return True if self.__dict__ == other.__dict__ else False


def ValidateSceneName(sceneName):
    if not sceneName or len(sceneName) < 5 or "_" not in sceneName:
        return False
    for character in sceneName:
        if character.isalnum() is False and character not in ("_", "-"):
            return False
    return True


def ValidateFbxName(fbxName):
    if ValidateSceneName(fbxName) is False:
        return False
    tagNames = ("multistart", "fadein", "fadeout", "blendout", "blackframes", "blankshot")
    if fbxName.lower() in tagNames:
        return False
    return True


def RunTerminalCommand(cmdTxt):
    capPaths = CapturePaths.FastPath()
    cmdProc = subprocess.Popen(cmdTxt, shell=True, cwd=capPaths.projectRoot,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    cmdOut, cmdErr = cmdProc.communicate()
    return cmdOut, cmdErr


def CheckPathCmdLine(filePath):
    cmdTxt = "p4 files -e {0}".format(filePath)
    cmdOut, cmdErr = RunTerminalCommand(cmdTxt)
    if " - no such file(s)." in cmdErr:
        return False
    if " change " in cmdOut:
        return True


def SyncPathCmdLine(filePath, force=False):
    cmdTxt = "p4 sync "
    if force:
        cmdTxt += "-f "
    cmdTxt += filePath
    if not os.path.splitext(filePath)[1]:
        cmdTxt += "\\..."
    cmdOut, cmdErr = RunTerminalCommand(cmdTxt)
    if " - updating " in cmdOut.lower() or " - added " in cmdOut.lower() or " up-to-date." in cmdErr.lower():
        return True
    else:
        return False


def EditPathCmdLine(filePath, clNumber=None):
    cmdTxt = "p4 edit {0}".format(filePath)
    if clNumber:
        cmdTxt = "p4 edit -c {0} {1}".format(clNumber, filePath)
    cmdOut, cmdErr = RunTerminalCommand(cmdTxt)
    if not cmdErr:
        return True


def AddPathCmdLine(filePath, clNumber=None):
    if not os.path.exists(filePath):
        cmdTxt = "copy NUL {0}".format(filePath)
        RunTerminalCommand(cmdTxt)
    cmdTxt = "p4 add {0}".format(filePath)
    if clNumber:
        cmdTxt = "p4 add -c {0} {1}".format(clNumber, filePath)
    RunTerminalCommand(cmdTxt)


def SubmitPathCmdLine(filePath, subTxt):
    cmdTxt = "p4 submit -d \"{0}\" {1}".format(subTxt, filePath)
    cmdOut, cmdErr = RunTerminalCommand(cmdTxt)
    if not cmdErr:
        return True


def RevertPathCmdLine(filePath):
    cmdTxt = "p4 revert {0}".format(filePath)
    cmdOut, cmdErr = RunTerminalCommand(cmdTxt)
    if cmdOut or "not opened on this client" in cmdErr:
        return True


def GetRangePaths(fbxPath):
    capPaths = CapturePaths.FastPath(fbxPath)
    rangePathXml = CapturePaths.FastPath().rangePathXml  # detecting by project, not specific fbx
    syncStatus = SyncPathCmdLine(rangePathXml)
    if syncStatus is not True:
        return
    try:
        tree = ET.parse(rangePathXml)
    except:
        return
    rootNode = tree.getroot()
    projects = rootNode.findall("project")
    for project in projects:
        packs = project.findall("pack")
        for pack in packs:
            packRoot = pack.find("root").attrib["path"].replace("/", "\\")
            if packRoot.lower() == capPaths.packRoot.lower():
                excelPath = pack.find("excel").attrib["path"]
                picklePath = pack.find("pickle").attrib["path"]
                return excelPath, picklePath


def GetRangesExcel(fbxPath):
    rangePaths = GetRangePaths(fbxPath)
    if not rangePaths:
        return
    xlsPath = rangePaths[0]
    trackerSynced = SyncPathCmdLine(xlsPath)
    if not trackerSynced or not os.path.exists(xlsPath):
        return

    validColumnNames = ("cutscene name", "fbx name", "range in", "range out", "duration", "total duration", "notes")

    # Get Merge Dict - slightly reworked as {mainPart:mergeParts}
    mergeDict = {}
    for partList in GetMergeDict(fbxPath).itervalues():
        mainPart = partList[-1].upper()
        mergeParts = [partName.upper() for partName in partList[:-1]]
        mergeDict[mainPart] = mergeParts

    workBook = xlrd.open_workbook(xlsPath, on_demand=False)
    rangeDict = {}
    for sheetIndex in range(workBook.nsheets):
        sheet = workBook.sheet_by_index(sheetIndex)

        # Create Column Dict from Valid Columns
        columnDict, extraCount = {}, 0
        if sheet.nrows < 3 or sheet.ncols < 3:
            continue
        for columnIndex, columnValue in enumerate(sheet.row_values(0)):
            columnName = str(columnValue).lower()
            if columnName in validColumnNames:
                if columnName in validColumnNames[:4]:
                    columnDict[columnName] = columnIndex
            elif columnName:
                extraCount += 1

        # Create Scene Dict from Valid Sheets
        if len(columnDict) == 4 and extraCount < 3:
            sceneName = ""
            previousFbx = ""
            shotIndex = 0
            for row in xrange(1, sheet.nrows):
                rowName = str(sheet.cell_value(row, columnDict["cutscene name"])).strip().upper()
                sceneName = rowName if ValidateSceneName(rowName) else sceneName
                fbxName = str(sheet.cell_value(row, columnDict["fbx name"])).strip().upper()
                rangeIn = str(sheet.cell_value(row, columnDict["range in"])).strip()
                rangeOut = str(sheet.cell_value(row, columnDict["range out"])).strip()

                if sceneName and ValidateFbxName(fbxName) and rangeIn and rangeOut:
                    if sceneName not in rangeDict.iterkeys():
                        rangeDict[sceneName] = CutsceneItem(sceneName)
                        shotIndex = 0
                    elif fbxName != previousFbx:
                        shotIndex += 1
                    sceneItem = rangeDict[sceneName]
                    rangeItem = RangeItem(int(float(rangeIn)), int(float(rangeOut)))
                    if not sceneItem.tracks or sceneItem.tracks[-1].index != shotIndex:
                        sceneItem.tracks.append(TrackItem(fbxName, shotIndex))
                        # Add Any Merge Parts
                        if fbxName in mergeDict.iterkeys():
                            sceneItem.tracks[-1].mergeParts = mergeDict[fbxName]
                    sceneItem.tracks[-1].ranges.append(rangeItem)

                previousFbx = fbxName

    return rangeDict


def GetRangesPickle(fbxPath):
    picklePath = ""
    rangePaths = GetRangePaths(fbxPath)
    if rangePaths:
        picklePath = rangePaths[1]
        SyncPathCmdLine(picklePath)
        # TODO: Error handling for bad sync / already checked out?
    if picklePath and os.path.exists(picklePath):
        pickleFile = open(picklePath, 'rb')
        rangeDict = cPickle.load(pickleFile)
        pickleFile.close()
        return rangeDict


def UpdateRangesPickle(fbxPath):
    rangePaths = GetRangePaths(fbxPath)
    if rangePaths:
        picklePath = rangePaths[1]
        rangeDict = GetRangesExcel(fbxPath)
        oldRangeDict = GetRangesPickle(fbxPath)

        if rangeDict and oldRangeDict and rangeDict != oldRangeDict:
            EditPathCmdLine(picklePath)
            pickleFile = open(picklePath, 'wb')
            cPickle.dump(rangeDict, pickleFile, protocol=2)
            pickleFile.close()
            SubmitPathCmdLine(picklePath, "Ranges updated.")


def GetScenesForFbx(fbxName, rangeDict):
    sceneList = []
    fbxName = fbxName.lower()
    for sceneName in sorted(rangeDict.iterkeys()):
        scene = rangeDict[sceneName]
        for track in scene.tracks:
            if track.name.lower() == fbxName or fbxName in [mergePart.lower() for mergePart in track.mergeParts]:
                sceneList.append(scene)
                break
    return sceneList


def GetTracksForFbx(fbxName, rangeDict):
    rangeTracks = []
    fbxName = fbxName.lower()
    for sceneName in sorted(rangeDict.iterkeys()):
        scene = rangeDict[sceneName]
        for track in scene.tracks:
            if track.name.lower() == fbxName or fbxName in [mergePart.lower() for mergePart in track.mergeParts]:
                track.index = len(rangeTracks)
                rangeTracks.append(track)
    return rangeTracks


def CreateConcatList(fbxPath, clNumber=None):
    # Get Range Dict and Merge Dict
    capPaths = CapturePaths.FastPath(fbxPath)
    fbxName = capPaths.fbxName
    rangeDict = GetRangesPickle(fbxPath)
    if not rangeDict:
        return "Range Path Error: Pickle path not found for this fbx."
    mergeDict = GetMergeDict(fbxPath)

    # Get Scene List
    sceneList = GetScenesForFbx(fbxName, rangeDict)
    if not sceneList:
        return "Concat Error: FBX could not be found in camera tracking doc - {0}".format(fbxName)

    # Override First - as merge names and multiscene fbx indexs need to be updated
    shotDict = {}
    for scene in sceneList:
        for track in scene.tracks:
            # Merge Override - concats use merge name instead of part names
            mergeMatch = [mergeName for mergeName in mergeDict.iterkeys() if
                          track.name.lower() in mergeDict[mergeName]]
            if mergeMatch:
                track.name = mergeMatch[0]
            # Mutli Scene Fbx Override
            if track.name not in shotDict.iterkeys():
                shotDict[track.name] = 0
            else:
                shotDict[track.name] += 1
                track.index = shotDict[track.name]

    # Scene Loop - as a single FBX can rarely be in more than one cutscene
    for scene in sceneList:
        # Set Paths and Defaults
        xmlPath = "{0}\\{1}.concatlist".format(capPaths.cutListRoot, scene.name)
        cutsPrefix = "$(assets)\\cuts\\"
        scenePathText = "{0}{1}".format(cutsPrefix, scene.name)
        sceneAudioText = "{0}.C.wav".format(scene.name)
        sectionMethod = "1"
        sectionDuration = "4"

        # Check For Existing Concatlist
        checkStatus = CheckPathCmdLine(xmlPath)
        if checkStatus is False:
            # Add New Concatlist to P4
            AddPathCmdLine(xmlPath, clNumber=clNumber)
            # TODO: New CL number fix above doesn't work...
        else:
            # Sync and Read Existing Concatlist
            SyncPathCmdLine(xmlPath)
            try:
                rootNode = ET.parse(xmlPath).getroot()
            except ET.ParseError:
                return "Concat Error: Existing concatlist could not be read - {0}".format(xmlPath)

            # Get Old Shots, Path, and Audio Text
            oldShotValues = []
            oldShotNodes = rootNode.findall("parts/Item/filename")
            for shotNode in oldShotNodes:
                shotText = shotNode.text.lower()
                oldShotValues.append(shotText)
            oldPathText = rootNode.find("path").text if rootNode.find("path") is not None else None
            oldAudioText = rootNode.find("audio").text if rootNode.find("audio") is not None else None

            # Compare Old Data to New Data
            newShotValues = []
            for track in scene.tracks:
                shotName = "shot_{0}".format(track.index + 1)
                shotText = "{0}{1}\\{2}.cutxml".format(cutsPrefix, track.name.lower(), shotName)
                newShotValues.append(shotText)
            if oldShotValues == newShotValues and oldPathText == scenePathText and oldAudioText == sceneAudioText:
                # Old Concatlist Validated - exit early, no update required
                continue

            # Get Old Section Method and Duration if possible
            methodNode = rootNode.find("sectionMethodIndex[@value]")
            durationNode = rootNode.find("sectionTimeDuration[@value]")
            sectionMethod = methodNode.attrib["value"] if methodNode is not None else sectionMethod
            sectionDuration = durationNode.attrib["value"] if durationNode is not None else sectionDuration

            # Checkout Concatlist for Update
            EditPathCmdLine(xmlPath, clNumber=clNumber)

        # Write New / Updated Concatlist
        rootNode = ET.Element("RexRageCutscenePartFile")
        partsNode = ET.SubElement(rootNode, "parts")
        for track in scene.tracks:
            itemNode = ET.SubElement(partsNode, "Item")
            itemNode.attrib["type"] = "RexRageCutscenePart"
            nameNode = ET.SubElement(itemNode, "name")
            fileNameNode = ET.SubElement(itemNode, "filename")

            # shotNumber = len(
            #     [trk for trk in scene.tracks if trk.name == track.name and trk.index < track.index]) + 1
            shotNumber = track.index + 1
            shotName = "Shot_{0}".format(shotNumber)
            nameNode.text = shotName
            fileNameNode.text = "{0}{1}\\{2}.cutxml".format(cutsPrefix, track.name.upper(), shotName)

        pathNode = ET.SubElement(rootNode, "path")
        pathNode.text = scenePathText
        audioNode = ET.SubElement(rootNode, "audio")
        audioNode.text = sceneAudioText
        sectionMethodNode = ET.SubElement(rootNode, "sectionMethodIndex")
        sectionMethodNode.attrib["value"] = sectionMethod
        sectionDurationNode = ET.SubElement(rootNode, "sectionTimeDuration")
        sectionDurationNode.attrib["value"] = sectionDuration

        xmlString = ET.tostring(rootNode)
        xmlString = minidom.parseString(xmlString).toprettyxml(indent="  ", encoding="UTF-8")
        xmlPath = os.path.join(capPaths.cutListRoot, "{0}.concatlist".format(scene.name))
        FileTools.CreateParentFolders(xmlPath)
        xmlFile = open(xmlPath, "w")
        xmlFile.write(xmlString)
        xmlFile.close()

    # All Concats Updated
    return True


def GetMergeDict(fbxPath):
    """
    Searches through mergelist files and creates a merge dict of the data.
    Returns:
        mergeDict (dict): Made of merge names (keys) paired to a list of it's part names (values).
    """
    capPaths = CapturePaths.FastPath(fbxPath)
    mergeListRoot = capPaths.mergeListRoot
    mergeDict = {}
    SyncPathCmdLine(mergeListRoot)
    if os.path.exists(mergeListRoot):
        for root, dirs, files in os.walk(mergeListRoot):
            for filename in files:
                xmlPath = os.path.join(root, filename)
                try:
                    rootNode = ET.parse(xmlPath).getroot()
                except ET.ParseError:
                    continue
                pathNode = rootNode.find("path")
                partNames = [os.path.split(node.text)[1].lower() for node in rootNode.findall(".//parts/Item/filename")]
                if pathNode is not None and len(partNames) > 0:
                    mergeName = os.path.split(pathNode.text)[1].lower()
                    mergeDict[mergeName] = partNames
    return mergeDict


def CheckForMergeFbx(fbxPath):
    """
    Searches through mergelists for a matched merge, so that main cams are used for layers.
    Arguments:
        fbxPath (string): the fbx to search for a matched merge with
    Returns:
        fbxPath (string) of the matched merge's main part, or None if no matched merge found.
    """
    fbxRoot, fbxName = os.path.split(fbxPath)
    fbxName = os.path.splitext(fbxName)[0].lower()
    # todo: need better error handling, crashes on a freshly setup capture machine
    mergeDict = GetMergeDict(fbxPath)
    matchedMerges = [partList[-1] for partList in mergeDict.itervalues() if fbxName in partList]
    if matchedMerges:
        return os.path.join(fbxRoot, "{0}.fbx".format(matchedMerges[0]))


def GetIngameRanges(fbxPath):
    rangeDict = {}

    # Get Xls Path - or return empty dict if not found
    capPaths = CapturePaths.FastPath(fbxPath)
    if capPaths.project == "RDR3":
        xlsPath = "x:\\rdr3\\docs\\Production\\RDR3_IG_CAMERA_TRACKING.xlsm"
    elif capPaths.project == "AMERICAS":
        xlsPath = "x:\\americas\\docs\\Production\\Performance_Capture\\AMERICAS_IG_TRACKER.xlsm"
    else:
        return rangeDict

    # Sync Xls - and verify it exists
    trackerSynced = SyncPathCmdLine(xlsPath)
    if not trackerSynced or not os.path.exists(xlsPath):
        return rangeDict

    validColumnNames = ("scene", "fbx name", "take", "range in", "range out", "duration", "total duration")

    workBook = xlrd.open_workbook(xlsPath, on_demand=False)
    for sheetIndex in range(workBook.nsheets):
        sheet = workBook.sheet_by_index(sheetIndex)
        if sheet.nrows == 0:
            continue

        # Create Column Dict from Valid Columns
        columnDict, extraCount = {}, 0
        for columnIndex, columnValue in enumerate(sheet.row_values(0)):
            columnName = str(columnValue).lower()
            if columnName in validColumnNames:
                if columnName in validColumnNames[:5]:
                    columnDict[columnName] = columnIndex
            elif columnName:
                extraCount += 1

        # Create Scene Dict from Valid Sheets
        if len(columnDict) == 5 and extraCount < 3:
            sceneName = ""
            for row in xrange(1, sheet.nrows):
                rowName = str(sheet.cell_value(row, columnDict["scene"])).strip().upper()
                sceneName = rowName if rowName else sceneName
                fbxName = str(sheet.cell_value(row, columnDict["fbx name"])).strip().upper()
                takeName = str(sheet.cell_value(row, columnDict["take"])).strip().upper()
                rangeIn = str(sheet.cell_value(row, columnDict["range in"])).strip()
                rangeOut = str(sheet.cell_value(row, columnDict["range out"])).strip()

                if sceneName and fbxName and takeName and rangeIn and rangeOut:
                    if sceneName not in rangeDict.iterkeys():
                        rangeDict[sceneName] = CutsceneItem(sceneName)
                    sceneItem = rangeDict[sceneName]

                    trackName = "{0}:{1}".format(fbxName, takeName)
                    matchedTrack = [track for track in sceneItem.tracks if track.name == trackName]
                    if matchedTrack:
                        trackItem = matchedTrack[0]
                    else:
                        shotIndex = len(sceneItem.tracks)
                        trackItem = TrackItem(trackName, shotIndex)
                        sceneItem.tracks.append(trackItem)

                    rangeItem = RangeItem(int(float(rangeIn)), int(float(rangeOut)))
                    trackItem.ranges.append(rangeItem)

    return rangeDict


def CreateRangeXmlNode():
    """ Creates a the basic node structure of the range xml, just without ranges yet. """
    # Create Basic Node/Loop Elements
    mainNode = ET.Element("ShotDef")
    sceneEditsNode = ET.SubElement(mainNode, "SceneEdits")
    cameraEditNode = ET.SubElement(sceneEditsNode, "Item")
    cameraEditNode.attrib["type"] = "CameraEdit"
    cameraEditNameNode = ET.SubElement(cameraEditNode, "Name")
    cameraEditNameNode.text = "ExportCamera"
    editsNode = ET.SubElement(cameraEditNode, "Edits")

    # Extra Nodes At End - don't think these even get used?
    priorityNode = ET.SubElement(cameraEditNode, "HighPriorityCamera")
    priorityNode.attrib["value"] = "false"
    alternateNode = ET.SubElement(cameraEditNode, "IsAlternateTrack")
    alternateNode.attrib["value"] = "false"
    optionalNode = ET.SubElement(cameraEditNode, "IsOptionalTrack")
    optionalNode.attrib["value"] = "false"
    return mainNode, editsNode


def WriteRangeXml(fbxPath, switcherDict):
    # Check Project, FbxPath, SwitcherDict
    capPaths = CapturePaths.FastPath(fbxPath)
    fbxName = capPaths.fbxName
    if capPaths.project != "AMERICAS":
        return "Error: Frame Xml are only supported on Americas, not {0}".format(capPaths.project)
    if not fbxPath:
        return "Error: Ranges not found.\n\nFbx is blank, or not yet saved."
    if not switcherDict:
        return "Error: No cameras in switcher, ranges cannot be created."

    # Check RangeDict and FbxTracks
    rangeDict = GetRangesPickle(fbxPath)
    if not rangeDict:
        return "Error: Range path/pickle not found for this project or dlc pack."
    fbxTracks = GetTracksForFbx(fbxName, rangeDict)
    if not fbxTracks:
        return "Error: No ranges found for this fbx."

    # Get Nodes and Switcher Frames
    mainNode, editsNode = CreateRangeXmlNode()
    switcherFrames = sorted(switcherDict.iterkeys())

    # Loop Through Ranges
    for track in fbxTracks:
        editNode = ET.SubElement(editsNode, "Item")
        editNode.attrib["type"] = "Edit"
        editNameNode = ET.SubElement(editNode, "Name")
        trackName = "Shot_{}_EST".format(track.index + 1)
        editNameNode.text = trackName
        rangesNode = ET.SubElement(editNode, "Ranges")
        for range in track.ranges:
            rangeStart = range.start
            rangeEnd = range.end
            # todo: below bit is complicated, but works. need to ask mike about frame numbering
            for keyIndex, frame in enumerate(switcherFrames):
                clipStart = frame if frame > rangeStart else rangeStart
                clipEnd = switcherFrames[keyIndex + 1] if keyIndex + 1 < len(switcherFrames) else rangeEnd
                clipEnd = rangeEnd + 1 if rangeEnd <= clipEnd else clipEnd
                if clipStart < rangeEnd and clipEnd > rangeStart:
                    rangeNode = ET.SubElement(rangesNode, "Item")
                    rangeNode.attrib["type"] = "CameraRange"
                    startNode = ET.SubElement(rangeNode, "RangeStart")
                    startNode.attrib["value"] = str(clipStart * 2)
                    endNode = ET.SubElement(rangeNode, "RangeEnd")
                    endNode.attrib["value"] = str(clipEnd * 2)
                    cameraNode = ET.SubElement(rangeNode, "CameraName")
                    cameraNode.text = switcherDict[frame]

    # Checkout / Add Xml
    xmlPath = os.path.join(capPaths.assetsRoot, "anim", "cinematic", fbxName.upper(), "{}.fr".format(fbxName.upper()))
    checkStatus = CheckPathCmdLine(xmlPath)
    if checkStatus is False:
        # Add New Concatlist to P4
        AddPathCmdLine(xmlPath)
    else:
        SyncPathCmdLine(xmlPath)
        EditPathCmdLine(xmlPath)

    # Write Out Updated Xml - return true on success
    SaveCleanXml(mainNode, xmlPath)
    return True


def CaptureRangeXml(fbxPath, camName, endFrame):
    """ Overrides a range xml for our capture (normally just a single full range export camera). """
    # Get Xml Path
    capPaths = CapturePaths.FastPath(fbxPath)
    fbxName = capPaths.fbxName
    xmlPath = os.path.join(capPaths.assetsRoot, "anim", "cinematic", fbxName.upper(), "{}.fr".format(fbxName.upper()))

    # Get Main and Edit Nodes
    mainNode, editsNode = CreateRangeXmlNode()

    # Create Single Edit Track
    editNode = ET.SubElement(editsNode, "Item")
    editNode.attrib["type"] = "Edit"
    editNameNode = ET.SubElement(editNode, "Name")
    editNameNode.text = "Shot_1_EST"
    rangesNode = ET.SubElement(editNode, "Ranges")

    # Create Single Range
    rangeNode = ET.SubElement(rangesNode, "Item")
    rangeNode.attrib["type"] = "CameraRange"
    startNode = ET.SubElement(rangeNode, "RangeStart")
    startNode.attrib["value"] = "0"
    endNode = ET.SubElement(rangeNode, "RangeEnd")
    endNode.attrib["value"] = str(endFrame)
    cameraNode = ET.SubElement(rangeNode, "CameraName")
    cameraNode.text = camName

    # Write Out Xml
    FileTools.MakePathWriteable(xmlPath)
    SaveCleanXml(mainNode, xmlPath)


def SaveCleanXml(xmlNode, xmlPath):
    xmlString = ET.tostring(xmlNode, 'utf-8')
    xmlString = minidom.parseString(xmlString).toprettyxml(indent="  ")

    # Remove Extra Lines - caused by minidom's dumb .toprettyxml call
    xmlLines = xmlString.split(">")
    for i in range(len(xmlLines)):
        if xmlLines[i].count("\n") == 2:
            xmlLines[i] = xmlLines[i].replace("\t", "").replace("\n", "")
    xmlString = ">".join(xmlLines)

    # write out file
    FileTools.CreateParentFolders(xmlPath)
    xmlFile = open(xmlPath, "w")
    xmlFile.write(xmlString)
    xmlFile.close()


def GetGTA5ObjectIds(dataXmlPath):
    # Sync DataXml - for now skipping the revert
    # RevertPathCmdLine(dataXmlPath)
    SyncPathCmdLine(dataXmlPath)
    if not os.path.exists(dataXmlPath):
        return

    # Get the IDs of any characters and blocking bounds
    shiftedTypes = ["rage__cutfPedModelObject", "rage__cutfBlockingBoundsObject", "rage__cutfHiddenModelObject"]
    objectIds = {}
    matchedEvents = 0

    # Store the current start frame from the data.cutpart
    tree = ET.parse(dataXmlPath)
    root = tree.getroot()
    for dataManager in root:
        # Store the ID and name of the blocking bound and character objects
        if "pCutsceneObjects" in dataManager.tag:
            for item in dataManager:
                itemType = item.get('type')
                if itemType in shiftedTypes:
                    if not itemType == "rage__cutfPedModelObject":
                        matchedEvents += 1
                    objectId = (item.find('iObjectId').get('value'))
                    objectName = (item.find('cName')).text.split(":")[0]
                    objectIds[objectId] = objectName
        if "pCutsceneEventArgsList" in dataManager.tag:
            for item in dataManager:
                if item.get('type') == "rage__cutfObjectVariationEventArgs":
                    matchedEvents += 1

    if matchedEvents > 0:
        return objectIds


def GetGTA5ShotStarts(dataXmlPath):
    # Assumes we've already synced dataXml in GetGTA5ObjectIds above
    shotStarts = []
    tree = ET.parse(dataXmlPath)
    root = tree.getroot()
    for dataManager in root:
        if dataManager.tag == "concatDataList":
            for item in dataManager:
                startString = item.find('iRangeStart').get('value')
                try:
                    shotStarts.append(float(startString))
                except ValueError:
                    continue
    return shotStarts


def UpdateGTA5DataXml(dataXmlPath, objectIds, timeShift):
    # Now we'll edit the daata.cutxml to shift the events
    EditPathCmdLine(dataXmlPath)
    tree = ET.parse(dataXmlPath)
    root = tree.getroot()

    for dataManager in root:
        # Find and adjust any ped variation and blocking bound event times
        if "pCutsceneEventList" in dataManager.tag:
            for item in dataManager:
                itemId = (item.find('iObjectId').get('value'))
                itemFrame = (item.find('fTime').get('value'))
                # If the current time is at the first frame of the exported scene, move it to the new start
                if itemId in objectIds.keys() and float(itemFrame) < 0.300000:
                    [element.set("value", str(timeShift)) for element in item if element.tag == "fTime"]

    # Write the new frames into the data.cutxml file
    tree.write(dataXmlPath)
