"""
Description:
    High level functions for creating, converting, submitting, and loading camera key data within MotionBuilder.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

from pyfbsdk import *
import RS.Config
import RS.Perforce as P4
from RS.Core.Camera import Lib as CamLib
from RS.Core.Camera import CamUtils, CameraLocker, CamValidationTools, ClipTools
from RS.Core.Automation.FrameCapture import FileTools
from RS.Core.Camera import RangeTools

from RS.Core.Camera.KeyTools import *
import camKeyParser
import camKeySaver
import camKeyLoader
import camKeyPerforce
import camKeyDef


def GetLocalCamXmlPath():
    fbxPath = FBApplication().FBXFileName
    p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(fbxPath)
    localXmlPath = camKeyPerforce.convertPerforcePathToLocal(p4XmlPath)
    return localXmlPath


def ValidateBeforeSaveCams():
    """
    Separate module to check our current user, FBX, and p4 connection are valid before saving.
    Also handles error messages for user so the SaveAndSubmitFbxCams module doesn't have to.
    Returns:
        A bool - true if all checks are valid, false if any fail.
    """
    # Detect Valid FBX
    fbxPath = FBApplication().FBXFileName
    fbxStatus = CamValidationTools.CheckFbxPath(fbxPath)
    if fbxStatus != "VALID":
        messageText = "Invalid FBX: " + fbxStatus
        FBMessageBox("Save Cam Data", messageText, "Okay")
        return False

    # Detect Cams in Switcher
    switcherCams = CamLib.Manager.GetSwitcherCameras()
    if not switcherCams:
        messageText = "ERROR: No cameras in switcher. Cam data not submitted."
        FBMessageBox("Save Cam Data", messageText, "Damn.")
        return False

    # Detect Old Cams
    if "gta5_dlc" not in fbxPath.lower():
        oldCams = [cam.Name for cam in switcherCams if cam.RequiresUpdate()]
        if oldCams:
            messageText = ("ERROR: {0} camera(s) in switcher need updating.\n\n"
                           "{1}Update cameras before saving.".format(len(oldCams), " "*12))
            FBMessageBox("Save Cam Data", messageText, "Okay")
            return False

    # Detect Valid Perforce Connection
    if not P4.Connected():
        messageText = "ERROR: Couldn't connect to perforce. Cam data not submitted."
        FBMessageBox("Save Cam Data", messageText, "Damn.")
        return False

    # All Checks Valid
    return True


def SaveAndSubmitFbxCams():
    # Validate Before Save
    saveCamsValid = ValidateBeforeSaveCams()
    if not saveCamsValid:
        return False

    # TODO: Get rid of key cam layers here and handle in saver
    camLocker = CameraLocker.LockManager()
    camLocker.keyAllKeylessCams()

    # Create CamInfo
    camData = camKeySaver.CreateCamInfoFromFbx(saveExpCam=False)
    p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(camData.FbxPath)
    localXmlPath = camKeyPerforce.convertPerforcePathToLocal(p4XmlPath)

    # Use P4.Add - for new cam xmls
    if P4.DoesFileExist(p4XmlPath) is False:
        camKeyParser.WriteCamInfoXml(camData, localXmlPath)
        P4.Add(localXmlPath)
    # Or P4.Edit - after syncing and verifying updated cams
    else:
        P4.Sync(p4XmlPath, force=True)
        oldData = camKeyParser.CreateCamInfoObjFromXmlPath(localXmlPath)
        if camData == oldData:
            FBMessageBox("Save Cams", "Cams identical to latest xml, skipping submit.", "Okay")
            return True
        openXml = P4.Edit(p4XmlPath)
        if not openXml:
            FBMessageBox("Save Cams", "Error: Cam data xml could not be checked out.", "Okay")
            return False
        camKeyParser.WriteCamInfoXml(camData, localXmlPath)

    # Submit Xml - via RangeTools for now
    subTxt = "Automated submission after camera change detected."
    RangeTools.SubmitPathCmdLine(localXmlPath, subTxt)
    FBMessageBox("Save Cams", "Cam data saved.", "Okay")
    return True


def ForceSubmitCamData():
    """
    Submits cam data locally (without using the server) without validation checks.
    Useful for the LayoutTools where we don't have access to the server.
    """
    # Unlock and Key Keyless Cams
    # TODO: Get rid of keyless cams here and handle in saver
    camLockObj = CameraLocker.LockManager()
    camLockObj.keyAllKeylessCams()

    # Get Local Xml Path
    localXmlPath = GetLocalCamXmlPath()

    # Verify Folders
    FileTools.CreateParentFolders(localXmlPath)
    FileTools.MakePathWriteable(localXmlPath)

    # Create camInfoObj and Write Out Xml
    camInfoObj = camKeySaver.CreateCamInfoFromFbx(saveExpCam=False)
    camKeyParser.WriteCamInfoXml(camInfoObj, localXmlPath)


def DeleteAndLoadCamsFromXmlPath(xmlPath):
    """
    Mid level function for deleting and restoring key data from an xml path.
    Used in other scripts (like the crapExporter), but not intended for end users.
    """
    # Save Autoclip Dict
    clipDict = {clip: clip.ShotCamera.Name for clip in ClipTools.GetAutoClips()}
    # TODO: Split the load cams ui into a different file, and do the warning about empty clips there
    # emptyClips = [clip for clip in CamUtils.GetAutoClips() if clip.ShotCamera is None]

    # Unlock Cameras
    _camLockObj_ = CameraLocker.LockManager()
    if _camLockObj_.lockedCount != 0:
        _camLockObj_.setAllCamLocks(False)

    # Delete Old Cam Data
    CamLib.Manager.DeleteSwitcherKeys()
    # TODO: Disabled cleanup here for GTA compatibility
    # CamLib.Manager.DeleteOldCameraParts()
    CamLib.Manager.DeleteNonSwitcherCams()

    # Create camInfoObj from Xml
    xmlInfoObj = camKeyParser.CreateCamInfoObjFromXmlPath(xmlPath)

    # Load Cams, Switcher, and Constraints
    camKeyLoader.LoadCamsIntoFbx(xmlInfoObj, loadExpCam=False)
    camKeyLoader.LoadSwitcherIntoFbx(xmlInfoObj)
    camKeyLoader.LoadCamRigs(xmlInfoObj)

    # Restore Autoclip Cams - quick fix if matching name found
    for clip, camName in clipDict.iteritems():
        if clip.ShotCamera is None:
            matchedCam = [cam for cam in FBSystem().Scene.Cameras if cam.Name == camName]
            if matchedCam:
                clip.ShotCamera = matchedCam[0]

    return "SUCCESS"


def LoadCamsInterface():
    """
    High level UI function that allows users to delete and restore cams via a key xml.
    The user can either grab the latest perforce xml, or load a custom one via an MB file popup.
    """

    # Simple Message Box Interface
    messageText = "".join([" "*10, "Do you want to restore camera data from the most recent xml backup?",
                           "\t\n\n", " "*25, "WARNING: This will delete the current camera data."])
    restoreCamsChoice = FBMessageBox("Cam Data Loader", messageText, "Restore Latest", "Custom Load", "Cancel")
    results = None

    # Get Fbx Path
    fbxPath = FBApplication().FBXFileName.replace("\\", "/")

    # LOAD LATEST XML
    if restoreCamsChoice == 1:

        # Validate Fbx Path
        if ("/" not in fbxPath):
            results = "Invalid FBX."
        else:

            # Check for Merge FBX
            mergeCams = RangeTools.CheckForMergeFbx(fbxPath)
            if mergeCams:
                fbxPath = mergeCams

            # Detect Xml and Sync
            p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(fbxPath)
            P4.Sync(p4XmlPath, force=True)

            # Validate Xml
            localXmlPath = camKeyPerforce.convertPerforcePathToLocal(p4XmlPath)
            if not os.path.exists(localXmlPath):
                results = "Cam data xml not found, or couldn't be synced from P4."
            else:

                # Delete and Load Cam Data From Xml
                results = DeleteAndLoadCamsFromXmlPath(localXmlPath)

    # LOAD CUSTOM XML
    elif restoreCamsChoice == 2:

        # Detect Xml Root - using fbx path in case from a different project
        xmlRoot = os.path.join(RS.Config.Project.Path.Art, "animation", "resources", "cameras")
        if "/" in fbxPath:
            p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(fbxPath)
            localXmlPath = camKeyPerforce.convertPerforcePathToLocal(p4XmlPath)
            xmlRoot = os.path.split(localXmlPath)[0].replace("/", "\\")

        # Create MB FilePopup
        lFp = FBFilePopup()
        lFp.Caption = "FBFilePopup example: Select a file"
        lFp.Style = FBFilePopupStyle.kFBFilePopupOpen
        lFp.Filter = "*.xml"
        lFp.Path = xmlRoot
        lRes = lFp.Execute()
        if (lRes):

            # Delete and Load Cam Data From Xml
            results = DeleteAndLoadCamsFromXmlPath(lFp.FullFilename)

    # OUTPUT
    if results == "SUCCESS":

        # Move To Frame 0
        # weird bug after creating cams where keys
        # don't update until the frame is altered
        thumbnailTime = FBTime()
        thumbnailTime.SetFrame(-1)
        FBPlayerControl().Goto(thumbnailTime)
        thumbnailTime.SetFrame(0)
        FBPlayerControl().Goto(thumbnailTime)

        # Broken Autoclips Remain - run the full SetupAutoClips
        brokenClips = [clip for clip in ClipTools.GetAutoClips() if clip.ShotCamera is None]
        if not brokenClips:
            successText = "The cam data was successfully restored."
            FBMessageBox("Cam Data Loader", successText, "Okay")
        else:
            # TODO: Add Ingame support here? LoadClips()?
            ClipTools.LoadCutsceneClips()
            msgTxt = "Cameras loaded, and ran EST to fix broken autoclips."
            FBMessageBox("Cam Data Loader", msgTxt, "Thanks!")

    elif results != None:
        errorText = 'Error: ' + results
        FBMessageBox("Cam Data Loader", errorText, "Okay")


def LoadKeyXmlIntoEmptyFbx(fbxPath, camName=None):
    """
    Detects the key xml path from a given fbxPath and loads it
    into an FBX via the DeleteAndLoadCamsFromXmlPath function.
    Arguments:
        fbxPath: A string of the local FBX path.
    """
    # Detect Merge - and use main cams for layer files
    mergeCams = RangeTools.CheckForMergeFbx(fbxPath)
    if mergeCams:
        fbxPath = mergeCams

    # Detect Xml and Sync
    p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(fbxPath)
    localXmlPath = camKeyPerforce.convertPerforcePathToLocal(p4XmlPath)

    # Sync Latest Cam Data Xml - or return false on fail
    syncedFiles = P4.Sync(p4XmlPath, force=True)
    if not syncedFiles:
        return False

    # Load Single Cam
    if camName:
        camLoaded = LoadSingleCam(localXmlPath, camName)
        if not camLoaded:
            print "Error: camKeyTasks load single cam failed."
            return False
    # Load All Cams
    else:
        DeleteAndLoadCamsFromXmlPath(localXmlPath)

    return True


def LoadSingleCam(xmlPath, camName):
    # Unlock Cameras
    camLocker = CameraLocker.LockManager()
    camLocker.setAllCamLocks(False)

    # Delete Old Cam Data
    CamLib.Manager.DeleteSwitcherKeys()
    CamLib.Manager.DeleteNonSwitcherCams()

    # Check for Cam - if name not found return false
    camInfo = camKeyParser.CreateCamInfoObjFromXmlPath(xmlPath)
    matchedCam = [cam for cam in camInfo.CamList if cam.Name.lower() == camName.lower()]
    if not matchedCam:
        return False

    # Strip CamInfo - to one cam, one switcher key
    camInfo.CamList = [matchedCam[0]]
    switcherKey = camKeyDef.KeyItem()
    switcherKey.Time = 0
    switcherKey.Value = camName
    camInfo.SwitcherKeyList = [switcherKey]

    # Load Cams, Switcher, and Constraints
    camKeyLoader.LoadCamsIntoFbx(camInfo, loadExpCam=True)
    camKeyLoader.LoadSwitcherIntoFbx(camInfo)
    camKeyLoader.LoadCamRigs(camInfo)

    return True


def CreateTransferCam():
    """
    Sets up the export camera in an empty FBX, renames it TransferCamera,
    rekeys the switcher with it, and returns a transCamData key object.
    Returns:
        A camKeyInfo object of the camera data to transfer.
    """
    # Setup Export Cam
    expCam = CamUtils.SetupExportCam()

    # Override Switcher Keys with TransferCamera
    CamUtils.OverrideSwitcherKeys(expCam.Name)

    # Create transCamData
    transCamData = camKeySaver.CreateCamInfoFromFbx(saveExpCam=True)

    return transCamData


def LoadTransferCam(transCamData):
    """
    Loads a transCamData key object into a full FBX,
    then rekeys the switcher, and sets up export camera.
    Arguments:
        transCamData: A camKeyInfo object of the transferred cam data.
    """
    # Unlock Cams
    camLockObj = CameraLocker.LockManager()
    camLockObj.setAllCamLocks(False)

    # Delete Old Switcher and Export Camera
    CamLib.Manager.DeleteSwitcherKeys()
    oldExportCam = [cam for cam in FBSystem().Scene.Cameras if cam.Name == "ExportCamera"]
    if oldExportCam:
        expCam = CamLib.MobuCamera(oldExportCam[0])
        expCam.DeleteCamera()

    # Load expCamObj into Full Fbx
    camKeyLoader.LoadCamsIntoFbx(transCamData, loadExpCam=True)
    camKeyLoader.LoadSwitcherIntoFbx(transCamData)


def CheckCamXmlExists(fbxPath):
    p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(fbxPath)
    return P4.DoesFileExist(p4XmlPath)
