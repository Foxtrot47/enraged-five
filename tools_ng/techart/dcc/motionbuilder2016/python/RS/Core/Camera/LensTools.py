"""
Description:
    Module for working with lenses and reading data from the game's lens meta file.
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""
import math


class Lens(object):
    """
    Simple object for storing data on each camera lens.
    """
    def __init__(self):
        self.MobuIndex = None
        self.Name = None
        self.MetaName = None
        self.FocalLength = None
        self.FOV = None


class Manager(object):
    """
    Class reading data from the lens meta (or hardcoded backup) and returning it as a dict.
    This should probably be setup as a singleton down the road, so we only run the GetLensDict code once.
    """
    # _lensMetaPath = os.path.join(Config.Project.Path.Export, "camera", "generic", "default_lens_models.pso.meta")
    # _backupLensList = ["16MM_LENS_MODEL", "25MM_LENS_MODEL", "35MM_LENS_MODEL", "50MM_LENS_MODEL",
    #                    "75MM_LENS_MODEL", "120MM_LENS_MODEL", "6MM_120MM_ZOOM_LENS_MODEL"]
    # _oldLensList = (("16mm", 69.97731), ("25mm", 47.84784), ("35mm", 36.99415), ("50mm", 25.24476),
    #                 ("75mm", 17.09556), ("120mm", 9.99896), ("16-120mm zoom", 0.0))

    _lensMmList = [10, 12, 14, 16, 18, 21, 25, 27, 35, 40, 50, 65, 75, 100, 125, 150]

    @staticmethod
    def _createLens(lensName, focalLength, mobuIndex):
        """
        Internal method - creates a lens object using a given lensName, focalLength, and mobuIndex.
        """
        lensObj = Lens()
        lensObj.Name = lensName
        lensObj.MetaName = lensName.upper() + "_LENS_MODEL"
        if "zoom" in lensName.lower():
            lensObj.MetaName = "6MM_120MM_ZOOM_LENS_MODEL"
        lensObj.MobuIndex = int(mobuIndex)
        lensObj.FocalLength = float(focalLength)
        lensObj.FOV = 0.0
        if "zoom" not in lensName.lower():
            lensObj.FOV = 2.0 * math.degrees(math.atan((24.0 / lensObj.FocalLength) * 0.5))
        return lensObj

    # @classmethod
    # def _parseLensMeta(cls):
    #     """
    #     Internal method - creates a lens dict by parsing the game's lens meta file.
    #     """
    #     lensDict = {}
    #     if os.path.exists(cls._lensMetaPath):
    #         tree = ET.parse(cls._lensMetaPath)
    #         lensItems = [node for node in tree.getroot().findall('MetadataList/Item[@type="camLensModelMetadata"]')]
    #         for lensNode in lensItems:
    #             nodeName = lensNode.find("Name")
    #             nodeName = nodeName.text.upper() if nodeName is not None else None
    #             nodeFocal = lensNode.find("DefaultFocalLength[@value]")
    #             nodeFocal = nodeFocal.attrib["value"] if nodeFocal is not None else None
    #             nodeIndex = lensNode.find("MotionBuilderIndex[@value]")
    #             nodeIndex = int(nodeIndex.attrib["value"]) if nodeIndex is not None else None
    #             if nodeName and nodeFocal and nodeIndex is not None and nodeIndex > -1:
    #                 lensObj = cls._createLens(nodeName.upper(), float(nodeFocal), nodeIndex)
    #                 lensDict[lensObj.MobuIndex] = lensObj
    #     return lensDict

    @classmethod
    def GetLensDict(cls):
        """
        Creates a lensDict using the meta file or via a hard-coded backup.
        """
        # TODO: Don't think we need any of this xml stuff anymore?
        # Create Lens Dict from Meta
        # try:
        #     Perforce.Sync(cls._lensMetaPath)
        #     lensDict = cls._parseLensMeta()
        # except:
        #     lensDict = {}
        #     print "Lens Warning: Meta parsing failed, using hardcoded backup."

        # Hard Coded Backup - if xml detection or parsing fails
        lensDict = {}
        if not lensDict:
            for lensIndex, lensMm in enumerate(cls._lensMmList):
                lensObj = cls._createLens("{}mm".format(lensMm), float(lensMm), lensIndex)
                lensDict[lensObj.MobuIndex] = lensObj
            lensObj = cls._createLens("Zoom", 0.0, len(cls._lensMmList))
            lensDict[lensObj.MobuIndex] = lensObj
        return lensDict


def GetNearestLensIndex(fovFloat):
    lensDict = Manager.GetLensDict()
    newFovs = [lensObj.FOV for lensObj in lensDict.itervalues()][:-1]  # skip last zoom lens, 0.0 not real lens
    nearestFov = newFovs[min(range(len(newFovs)), key=lambda i: abs(newFovs[i] - fovFloat))]
    newIndex = [lens.MobuIndex for lens in lensDict.itervalues() if lens.FOV == nearestFov][0]
    return newIndex
