"""
Description:
    Module for adding the KeyTools directory to sys.path so imports can be made locally.
    This is needed to run processes on the server (which doesn't have global modules).

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""


import os
import sys

directory = os.path.split(__file__)[0]
if directory not in sys.path or directory != sys.path[0]:
    sys.path = [directory] + sys.path