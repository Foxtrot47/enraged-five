"""
Hotkeys for the new camera team numpads.
A bit hacky, but we're still iterating on the key setup with Steve.
"""

import re
import pyfbsdk as mobu

from RS import Globals
from RS.Core.Camera import CamUtils
from RS.Core.Camera import SaveCameras
from RS.Core.Camera import Lib as CamLib
from RS.Tools.CameraToolBox.Run import Run as CamBoxRun
from RS.Utils.Scene import Model


def OpenCamBox():
    CamBoxRun()


def OpenCamBoxTurbo():
    TogglePickable()
    ToggleMovers()
    OpenCamBox()


def SaveCams():
    SaveCameras.Run()


def NewCam25mm():
    newCam = CamLib.MobuCamera.CreateRsCamera()
    newCam.SetInterestDistance()
    lensName = CamLib.MobuCamera._lensPropName
    lensProp = newCam.camera.PropertyList.Find(lensName)
    lensProp.Data = 1


def NewCam35mm():
    newCam = CamLib.MobuCamera.CreateRsCamera()
    newCam.SetInterestDistance()
    lensName = CamLib.MobuCamera._lensPropName
    lensProp = newCam.camera.PropertyList.Find(lensName)
    lensProp.Data = 2


def NewCam50mm():
    newCam = CamLib.MobuCamera.CreateRsCamera()
    newCam.SetInterestDistance()
    lensName = CamLib.MobuCamera._lensPropName
    lensProp = newCam.camera.PropertyList.Find(lensName)
    lensProp.Data = 3


def NewCam75mm():
    newCam = CamLib.MobuCamera.CreateRsCamera()
    newCam.SetInterestDistance()
    lensName = CamLib.MobuCamera._lensPropName
    lensProp = newCam.camera.PropertyList.Find(lensName)
    lensProp.Data = 4


def ResetCamInterest():
    CamUtils.ResetCameraInterest()


def LevelCamera():
    value = None
    for camera in CamLib.Manager.GetCurrentCameras():
        if value is None:
            value = not camera.dollyMode
        camera.dollyMode = value


def TogglePickable():
    if not Model.PickableManager.Registered:
        Model.PickableManager.register()
    value = not Model.PickableManager.Pickable()
    Model.PickableManager.setPickable(value)


def ToggleMovers():
    show = None
    for component in Globals.Models:
        if re.search("mover|facing_direction|upperfixupdirection|independentmoverdirection|facingdirection",
                     component.Name, re.I) and hasattr(component, "Show") and not isinstance(component, mobu.FBCamera):
            if show is None:
                show = component.Show - 1
            component.Show = show

