import re
import pyfbsdk as mobu
import RS.Globals
from RS.Core.Animation import Lib as AnimLib
from RS.Core.Camera import Lib as CamLib
from RS.Tools.UI import Application
from RS.Utils.Scene.Model import PickableManager

# Janky Pyside Import - allows same code on all projects
try:
    from PySide2 import QtCore, QtWidgets
except ImportError:
    from PySide import QtCore, QtGui as QtWidgets


class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class ViewerOverride(QtWidgets.QWidget):
    clicked = QtCore.Signal(object)
    scrolled = QtCore.Signal(object)

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.MouseButtonPress:
            self.clicked.emit((event.pos().x(), event.pos().y()))
            # return True  # Locks Camera Movement
        elif event.type() == QtCore.QEvent.Wheel:
            self.scrolled.emit(event.delta())
            return True
        return False


class FocusMarker(object):
    __metaclass__ = Singleton

    def __init__(self):
        self._viewerFilter = ViewerOverride()
        self._viewerWidget = self._getViewerWidget()
        self._viewerFilter.clicked.connect(self._handleClick)
        self._viewerFilter.scrolled.connect(self._handleScroll)
        self._selectedComp = None
        self.ClickerActive = False

    @staticmethod
    def _deleteMarkerHudItems():
        """
        Deletes all marker hud elements from the current camera.
        """
        deleteHuds = []
        deleteElements = []
        for camHud in mobu.FBSystem().Scene.HUDs:
            if camHud.Name.startswith("CamMarkerHud"):
                deleteHuds.append(camHud)
            for hudElement in camHud.Elements:
                if hudElement not in deleteElements and hudElement.Name.startswith("CamMarkerHud"):
                    deleteElements.append(hudElement)
        deleteList = deleteElements + deleteHuds
        [hudItem.FBDelete() for hudItem in deleteList]

    @staticmethod
    def _addMarkerHudItems():
        """
        Adds help text to camera hud when using cam marker click tools.
        """
        # Create HudText Item - to attach to each cam's hud
        hudData = {"Content": "Focus Mode", "X": 0, "Y": 45, "Height": 10,
                   "Color": mobu.FBColorAndAlpha(0, 1, 0, 1),
                   "Justification": mobu.FBHUDElementHAlignment.kFBHUDCenter,
                   "HorizontalDock": mobu.FBHUDElementHAlignment.kFBHUDCenter,
                   "VerticalDock": mobu.FBHUDElementVAlignment.kFBHUDVCenter}
        hudText = mobu.FBHUDTextElement("CamMarkerHudText")
        for attrName, attrValue in hudData.iteritems():
            setattr(hudText, attrName, attrValue)

        # Loop Through Cameras - and attach hudText to existing hud or use a new one
        for camera in RS.Globals.Cameras:
            camHuds = list(camera.HUDs)
            if camHuds:
                clickHud = camHuds[0]
            else:
                clickHuds = [hud for hud in mobu.FBSystem().Scene.HUDs if hud.Name == "CamMarkerHud"]
                clickHud = clickHuds[0] if clickHuds else mobu.FBHUD("CamMarkerHud")
                camera.ConnectSrc(clickHud)
            clickHud.ConnectSrc(hudText)

    @staticmethod
    def _getHitDict(mouseTuple):
        hitDict = {}
        modelList = mobu.FBModelList()
        mobuRender = mobu.FBSystem().Renderer
        camera = mobuRender.GetCameraInPane(0)

        # Create Hit Box to Check Positions in Frustum
        hitBox = mobu.FBFindModelByLabelName("ClickHitBox")
        if not hitBox:
            hitBox = mobu.FBModelCube("ClickHitBox")
        hitBox.Show = True

        # Search Through Models in Cam Frustum
        mobuRender.GetDisplayableGeometryInCameraFrustum(modelList, camera)
        for model in modelList:
            invalidModel = (not model.Geometry or isinstance(model, mobu.FBCamera) or model.Name.lower() == "mover" or
                            [mat for mat in model.Materials if "stagematerial" in mat.Name.lower()])
            if invalidModel:
                continue

            # Raycast From Camera To Model
            hitPosition = mobu.FBVector3d()
            hitNormal = mobu.FBVector3d()
            hitDetected = model.RayCast(camera, mouseTuple[0], mouseTuple[1], hitPosition, hitNormal)
            if hitDetected:

                # Move Hit Box to Hit Position & Raycast Again - add to dict if specific hit position valid
                hitBox.Translation.Data = hitPosition
                mobuRender.Render()
                hitBox.RayCast(camera, mouseTuple[0], mouseTuple[1], hitPosition, hitNormal)
                hitFound = mobuRender.IsModelInsideCameraFrustum(hitBox, camera)
                if hitFound:
                    hitDict[model] = hitPosition

        hitBox.Show = False
        return hitDict

    @staticmethod
    def _getNearestHitVector(hitDict):
        camera = mobu.FBSystem().Renderer.GetCameraInPane(0)
        smallestLength = min([(vector - camera.Translation.Data).Length() for vector in hitDict.itervalues()])
        nearestVector = [vector for vector in hitDict.itervalues()
                         if (vector - camera.Translation.Data).Length() == smallestLength][0]
        return nearestVector

    @staticmethod
    def _moveFocusMarker(positionVector):
        for camera in RS.Globals.Cameras:
            focusProp = camera.PropertyList.Find("FOCUS (cm)")
            if not focusProp:
                continue
            camMatrix = mobu.FBMatrix()
            camera.GetMatrix(camMatrix)
            positionMatrix = mobu.FBMatrix()
            positionMatrix[12], positionMatrix[13], positionMatrix[14] = positionVector[0], positionVector[1], positionVector[2]
            camMatrix.Inverse()
            resultMatrix = camMatrix * positionMatrix
            focusProp = camera.PropertyList.Find("FOCUS (cm)")
            focusProp.Data = abs(resultMatrix[12])

    @staticmethod
    def _getViewerWidget():
        mobuWindow = Application.GetMainWindow()
        viewerWindow = Application.GetChildrenByWindowTitle(mobuWindow, "Viewer")[0]
        viewerParent = [child for child in viewerWindow.children() if isinstance(child, QtWidgets.QWidget)][2]
        viewerWidget = viewerParent.children()[0].children()[5]
        return viewerWidget

    def _handleClick(self, mouseTuple):
        # Disable GPU Deformation - this causes model detection to fail in self._getHitDict
        if RS.Globals.EvaluateManager.UseGPUDeformation:
            RS.Globals.EvaluateManager.UseGPUDeformation = False
            RS.Globals.Scene.Evaluate()
        hitDict = self._getHitDict(mouseTuple)
        if hitDict:
            hitVector = self._getNearestHitVector(hitDict)
            if hitVector:
                self._moveFocusMarker(hitVector)
                self.ShowActiveMarker()

    def _handleScroll(self, scrollInt):
        scrollFloat = float(scrollInt / 10)
        camera = mobu.FBSystem().Renderer.GetCameraInPane(0)
        for camChild in camera.Children:
            if "focus" in camChild.Name.lower():
                markerMatrix = mobu.FBMatrix()
                mobu.FBSystem().Scene.Evaluate()
                camChild.GetMatrix(markerMatrix, mobu.FBModelTransformationType.kModelTransformation, False)
                markerMatrix[12] += scrollFloat
                if markerMatrix[12] < 10.0:
                    markerMatrix[12] = 10.0
                cameraMatrix = mobu.FBMatrix()
                camera.GetMatrix(cameraMatrix)
                globalMatrix = cameraMatrix * markerMatrix
                globalVector = [globalMatrix[12], globalMatrix[13], globalMatrix[14]]
                self._moveFocusMarker(globalVector)
                break

    @staticmethod
    def _getMarkers():
        markers = []
        for comp in mobu.FBSystem().Scene.Components:
            if isinstance(comp, mobu.FBModelMarker):
                if re.search("focus[ _]*marker", comp.LongName, re.I):
                    markers.append(comp)
        return markers

    @classmethod
    def SetupViewer(cls):
        # Force Viewer to Normal Picking Mode
        viewOptions = mobu.FBSystem().Scene.Renderer.GetViewingOptions()
        viewOptions.PickingMode = mobu.FBPickingMode.kFBPickingModeStandard

        # Force Hide Camera Geometry
        for camera in RS.Globals.Cameras:
            camera.Visibility = True
            camera.Show = False
            camera.NearPlaneDistance = 10.0
            camera.FarPlaneDistance = 999999.99

    @classmethod
    def ShowActiveMarker(cls):
        camera = mobu.FBSystem().Renderer.GetCameraInPane(0)
        for marker in cls._getMarkers():
            showBool = True if marker in camera.Children else False
            if marker.Show != showBool or marker.Visibility != showBool:
                marker.Show = showBool
                marker.Visibility = showBool

    @classmethod
    def HideMarkers(cls):
        for marker in cls._getMarkers():
            if marker.Show or marker.Visibility:
                marker.Visibility = False
                marker.Show = False

    @classmethod
    def SetFocusKeyGroup(cls):
        rsCams = CamLib.Manager.GetRsCameras()
        if rsCams:
            focusProp = rsCams[0].camera.PropertyList.Find("FOCUS (cm)")
            focusKeyGroup = mobu.FBKeyingGroup("CamFocus", mobu.FBKeyingGroupType.kFBKeyingGroupGlobal)
            focusKeyGroup.AddProperty(focusProp)
            focusKeyGroup.SetActive(True)
            focusKeyGroup.SetEnabled(True)

    @classmethod
    def RemoveFocusKeyGroup(cls):
        [group.FBDelete() for group in list(RS.Globals.Scene.KeyingGroups) if "camfocus" in group.Name.lower()]

    def EnableClickMarker(self):
        PickableManager.setPickable(False)
        self.SetupViewer()
        self._deleteMarkerHudItems()
        self._addMarkerHudItems()
        self.ShowActiveMarker()
        self._viewerWidget.installEventFilter(self._viewerFilter)
        self.SetFocusKeyGroup()
        self.ClickerActive = True

    def DisableClickMarker(self):
        self._deleteMarkerHudItems()
        hitBox = mobu.FBFindModelByLabelName("ClickHitBox")
        if isinstance(hitBox, mobu.FBModelCube):
            hitBox.FBDelete()
        self._viewerWidget.removeEventFilter(self._viewerFilter)
        self.HideMarkers()
        self.RemoveFocusKeyGroup()
        if self.ClickerActive:
            PickableManager.restoreState(PickableManager.Opened)
            self.ClickerActive = False


class SubjectMarker(object):
    """
    Simple class for creating, removing, and plotting the new subject marker.
    """

    _subjectMarkerName = "RS_Camera_SubjectMarker"

    @staticmethod
    def _switchViewerToXray():
        viewOptions = mobu.FBSystem().Scene.Renderer.GetViewingOptions()
        viewOptions.PickingMode = mobu.FBPickingMode.kFBPickingModeXRay

    @classmethod
    def _createSubjectMarker(cls):
        subjectMarker = mobu.FBModelMarker(cls._subjectMarkerName)
        subjectMarker.Look = mobu.FBMarkerLook.kFBMarkerLookSphere
        subjectMarker.Color = mobu.FBColor(1, 0, 0)
        subjectMarker.Scaling = mobu.FBVector3d(10.0, 10.0, 10.0)
        subjectMarker.Size = 100.0
        return subjectMarker

    @classmethod
    def _getSubjectMarker(cls):
        """
        Internal method - gets an existing subject marker or creates a new one.
        Returns:
            subjectMarker (FBModelMarker): the found or created marker.
        """
        subjectMarker = mobu.FBFindModelByLabelName(cls._subjectMarkerName)
        if not subjectMarker:
            subjectMarker = cls._createSubjectMarker()
        subjectMarker.Translation.SetAnimated(True)
        subjectMarker.Rotation.SetAnimated(True)
        subjectMarker.Show = True
        return subjectMarker

    @staticmethod
    def _moveMarkerToCurrentCam(subjectMarker):
        offsetDistance = 100.0
        camMatrix = mobu.FBMatrix()
        camera = mobu.FBSystem().Renderer.GetCameraInPane(0)
        camera.GetMatrix(camMatrix)

        directionVector = mobu.FBVector3d(camMatrix[0], camMatrix[1], camMatrix[2])
        positionVector = mobu.FBVector3d(camMatrix[12], camMatrix[13], camMatrix[14])
        markerVector = directionVector * offsetDistance + positionVector
        subjectMarker.Translation.Data = markerVector
        subjectMarker.Rotation.Data = camera.Rotation.Data

    @staticmethod
    def _plotMarkerToModel(subjectMarker, sourceModel):
        """
        Internal method - clears marker keys and plots its translation to the sourceModel.
        Arguments:
            subjectMarker (FBModelMarker): the marker to use for plotting.
            sourceModel (FBModel): the model or object we want to plot the marker to.
        """
        RS.Globals.System.CurrentTake.SetCurrentLayer(0)
        AnimLib.ClearAnimation(subjectMarker, allProperties=True)
        AnimLib.TransferMotion(subjectMarker, sourceModel)

    @classmethod
    def PlotSelectedModel(cls):
        """
        Plots the subject marker to the first currently selected model.
        """
        cls._switchViewerToXray()
        subjectMarker = cls._getSubjectMarker()
        selectedModels = mobu.FBModelList()
        mobu.FBGetSelectedModels(selectedModels, None, True)
        if selectedModels:
            cls._plotMarkerToModel(subjectMarker, selectedModels[0])
        else:
            cls._moveMarkerToCurrentCam(subjectMarker)
