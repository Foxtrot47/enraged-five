"""
Unittests for testing MocapCommands and core Virtual Production commands
"""
import os
import unittest
import tempfile

import pyfbsdk as mobu

from RS import Globals
from RS.Utils import ContextManagers
from RS.Core.Mocap import MocapCommands
from RS.Core.VirtualProduction import tpvcLib
from RS.Core.VirtualProduction.UnitTest import base


def Run():
    """Run unittests from this module"""
    unittest.TextTestRunner(verbosity=0).run(suite())


def suite():
    """A suite to hold all the tests from this module

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()

    for functionName in dir(Test_Commands):
        if functionName.startswith("test_") and not functionName.endswith(("_Scene", "_Referencing")):
            suite.addTest(Test_Commands(functionName))
    return suite


class Test_Commands(base.VirtualProductionBase):
    """Command Tests"""

    def test_SaveLoadRange(self):
        """Testing saving/loading range"""

        tempPath = os.path.join(tempfile.gettempdir(), "rtc_ranges.txt")

        MocapCommands.RTCRangeSave(outputPath=tempPath)
        self.assertTrue(os.path.exists(tempPath))
        MocapCommands.RTCRangeLoad(inputPath=tempPath)

        os.remove(tempPath)

    def test_SaveSelectedCamera(self):
        """Testing saving camera"""
        camera = mobu.FBCamera("test")
        camera.Selected = True

        with ContextManagers.SuspendMessages():
            MocapCommands.SaveSelectedCameras()

        Globals.Application.FileNew()

    def test_Toybox(self):
        """Testing creating toybox"""
        MocapCommands.CreateToybox()
        self.assertExists("ToyBox")
        Globals.Application.FileNew()

    def test_ZeroOutScene(self):
        """Test zero out scene"""
        MocapCommands.ZeroOutScene(10, showMessage=False)

    def test_SetupCamera(self):
        """Tests setting up a camera"""
        camera = mobu.FBCamera("test")

        tpvcLib.SetupTpvcDeviceCamera._setupCameraOptions(camera)
        tpvcLib.SetupTpvcDeviceCamera._setupCameraLens(camera)
        self.assertIsNotNone(camera.PropertyList.Find(tpvcLib.SetupTpvcDeviceCamera._CAMERA_LENS_NAME))

        tpvcLib.SetupTpvcDeviceCamera._setupHud(camera)
        self.assertTrue(any(comp for comp in Globals.Components if comp.Name == "test_Hud"))

        Globals.Application.FileNew()
