import unittest

from RS.Core.VirtualProduction.UnitTest import ui, commands


def getTestSuites():
    """Gathers suites together to test

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()
    suite.addTest(ui.suite())
    suite.addTest(commands.suite())
    return suite


def Run():
    """Runs unittests"""
    unittest.TextTestRunner(verbosity=2).run(getTestSuites())


if __name__ == "__main__":
    Run()
