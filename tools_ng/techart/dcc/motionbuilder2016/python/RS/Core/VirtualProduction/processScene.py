import time
import logging

import pyfbsdk as mobu

from RS import Globals
from RS import Perforce
from RS.Core import Mocap
from RS.Core.Camera import CamUtils
from RS.Core.AnimData import Context
from RS.Core.Mocap import MocapCommands
from RS.Utils import Namespace, Scene
from RS.Utils.Scene import Devices
from RS.Core.ReferenceSystem import Manager
# The following line purposely differs from wildwest - the Unknown type is not supported in GTA5.
from RS.Core.ReferenceSystem.Types import MocapGsSkeleton, MocapBasePreviz, MocapBuildAsset, MocapNonEssential
from RS.Core.VirtualProduction import tpvcLib


class NoWatchstarTrialForCurrentTake(Exception):
    """Exception if there is no Watchstar for the current trial"""


class NoGsSkeletonFound(Exception):
    """Exception if there is no GS Skeleton found"""


class CamerasNotBakedError(Exception):
    """Exception if there is an error while trying to bake and remove the vCams."""


class PrevizSaveReferenceError(Exception):
    """Raised in PostProcessPrevizSave if there is an problem creating a reference or accessing information from it."""


class _ProcessCharacterInfo(object):
    """Parent object that holds all the related objects required to fully represent
    the asset when processing the scene.
    """

    def __init__(self, watchstar, gsSkelRef, gsSkelChar, gameSkelRef, gameSkelChar):
        """
        Args:
            watchstar (AnimData.Character): A watchstar character (actor->char mapping)
            gsSkelRef (RS.Core.ReferenceSystem.Types.MocapGsSkeleton): The GS character in the ref system
            gsSkelChar (FBCharacter): The mobu character for the GS
            gameSkelRef (RS.Core.ReferenceSystem.Types.CharacterBase.CharacterBase):
                The game character in the ref system
            gameSkelChar (FBCharacter): The mobu character for the game asset
        """
        super(_ProcessCharacterInfo, self).__init__()
        self.watchstar = watchstar
        self.gsSkelRef = gsSkelRef
        self.gsSkelChar = gsSkelChar
        self.gameSkelRef = gameSkelRef
        self.gameSkelChar = gameSkelChar

    def __str__(self):
        watchstarText = "None"
        gsSkelRefText = "None"
        gsSkelCharText = "None"
        gameSkelRefText = "None"
        gameSkelCharText = "None"

        if self.watchstar is not None:
            watchstarText = str(self.watchstar)

        if self.gsSkelRef is not None:
            gsSkelRefText = str(self.gsSkelRef.Namespace)

        if self.gsSkelChar is not None:
            gsSkelCharText = str(self.gsSkelChar.FullName)

        if self.gameSkelRef is not None:
            gameSkelRefText = str(self.gameSkelRef.Namespace)

        if self.gameSkelChar is not None:
            gameSkelCharText = str(self.gameSkelChar.FullName)

        return "Watchstar: {} | GS Skel Ref: {} | GS Skel Char: {} | Game Skel Ref: {} | Game Skel Char: {}".format(
            watchstarText, gsSkelRefText, gsSkelCharText, gameSkelRefText, gameSkelCharText
        )


class ProcessScene(object):
    """Class to hold all the logic of the Process Scene

    The public interface to call is process().

    Examples:
        >>> ProcessScene.process()
    """

    @staticmethod
    def _getTrial():
        """Get the Watchstar trial for the current take in mobu

        Returns:
            contexts.Trial
        """
        trialID = Manager.Manager().TrialID
        if trialID in [-1, 0, None]:
            raise NoWatchstarTrialForCurrentTake("No Watchstar Trial for the current Motion Builder take found")
        return Context.animData.getTrialByID(trialID)

    @staticmethod
    def _deleteDevices():
        """Deletes all devices except the SpaceBall."""
        # Delete Vcam Devices
        tpvcLib.DeleteVcamDevices()

        # Delete all the devices and the assets in the Navigator related to those devices.
        # The SpaceBall is left in by request of the previz team.
        spaceBallDevices = Devices.GetAllDeviceByType("FBDeviceSpaceBall")
        MocapCommands.DeleteDevices(ignoredDevices=spaceBallDevices)

    @staticmethod
    def _bakeCams():
        """Bakes out VCams to RS cams.

        Returns:
            bool: If the process was successful or not.
        """
        undupedVCams = CamUtils.GetUndupedVcams()
        if not undupedVCams:  # No VCams - exit early
            return True

        # Dupe and check VCams to dupe again
        CamUtils.DupeRtcsToRsCams()
        undupedVCams = CamUtils.GetUndupedVcams()
        if undupedVCams:  # Error: VCams still remain unduped
            raise CamerasNotBakedError("VCams were not all baked in the FBX scene.")
        return True

    @staticmethod
    def _getGsSkelForCharacter(character, skelNames):
        """Find the GS Skeleton for a Watchstar character

        Args:
            character (AnimData.Character): The char->actor mapping to find a GS for
            skelNames (list[str]): List of mobu character name strings to search through

        Returns:
            str or None: name of the skeleton to use for that character, if found, otherwise None
        """
        talent = character.talent()
        if talent:
            characterFirstName = talent.firstName().replace(" ", "").lower()
            characterLastName = talent.lastName().replace(" ", "").lower()
            if characterFirstName in skelNames:
                return characterFirstName

            charName = characterFirstName
            for lastNameChar in characterLastName:
                charName = "{}{}".format(charName, lastNameChar)
                if charName in skelNames:
                    return charName

        return None

    @classmethod
    def process(cls, updateWatchstar=True):
        """Process the scene.

        "Clean-up tasks" checklist:
            * Confirm/get the the Watchstar trial of the current take.
            * Bake all VCams and then delete all devices except the SpaceBall.
            * Remove all references of the following types:
                * MocapBasePreviz
                * MocapBuildAsset
                * MocapNonEssential
            * Gather what GS's are missing from the trial.
            * Check what GS->characters are missing and load them in.
            * Check what GS->characters are wrong, and load the correct assets in.
            * Link the GS's to the characters.
            * Update Watchstar
                * Update the namespace that is being used by each GS skeleton in MotionBuilder.
                * Add the task result notes to the Talent and Staff section of the trial.

        Args:
            updateWatchstar (bool): True to update Watchstar, False to skip (for testing, etc).

        Returns:
            list[str]: lines that make up a data log/report
        """
        reportData = []
        exceptionRaised = False  # Any exceptions are raised at the end so as much processes as possible.
        start = time.time()

        # Get the Trial and Characters
        trial = cls._getTrial()
        charData = cls._collectAllData(trial)

        # Clean up devices.
        try:
            cls._bakeCams()
            reportData.append("VCams were baked successfully.")
        except Exception as error:
            reportData.append("[ERROR] Error while baking VCams. Please bake and delete the VCams manually.")
            exceptionRaised = True

        try:
            reportData.append("VCams were deleted successfully.")
            cls._deleteDevices()
        except Exception as error:
            reportData.append("[ERROR] Error while deleting VCams. Please delete them manually.")
            exceptionRaised = True

        # Clean up the file
        try:
            rmTypes = (
                MocapBasePreviz.MocapBasePreviz,
                MocapBuildAsset.MocapBuildAsset,
				MocapNonEssential.MocapNonEssential,
            )
            rmRefs = [ref for ref in Manager.Manager().GetReferenceListAll() if isinstance(ref, rmTypes)]
            if len(rmRefs) > 0:
                reportData.append(
                    "Cleaned up and removed references: {}\n".format(", ".join([ref.Name for ref in rmRefs]))
                )
                Manager.Manager().DeleteReferences(rmRefs)
        except Exception as error:
            reportData.append(
                "[ERROR] Error while deleting extra assets. Extra previz or build assets could be left in the scene."
            )
            exceptionRaised = True

        # Validate the data...
        for char in charData:
            updatedChar = False
            reportData.append("Processing: {}".format(char.watchstar))
            # Check for GS
            try:
                if char.gsSkelRef is None or char.gsSkelChar is None:
                    reportData.append("Missing GS Skel for Character: {}".format(char.watchstar))
                    updatedChar = True

                # This needs to be local, not p4
                gameCharacterPath = char.watchstar.gameChar().fbxGameAssetPath()
                Perforce.Sync(gameCharacterPath)
                fileInfo = Perforce.GetFileState(gameCharacterPath)
                localGameCharacterPath = fileInfo.get_ClientFilename()
                if char.gameSkelRef and char.gameSkelRef.Path.lower() != localGameCharacterPath.lower():
                    reportData.append(
                        "Found incorrect Game Character for {}.\nRemoving {}".format(char.watchstar, char.gameSkelRef.Path)
                    )
                    Manager.Manager().DeleteReferences(char.gameSkelRef)
                    char.gameSkelChar = None
                    updatedChar = True

                if char.gameSkelRef is None or char.gameSkelChar is None:
                    reportData.append(
                        "Found missing Game Character for {}.\nLoading {}".format(char.watchstar, localGameCharacterPath)
                    )
                    gameSkelRef, gameSkelChar = cls._addCharacter(char, localGameCharacterPath)
                    char.gameSkelRef = gameSkelRef
                    char.gameSkelChar = gameSkelChar
                    updatedChar = True
            except PrevizSaveReferenceError as error:
                reportData.append(
                    "[ERROR] Error while checking scene data for {}:\n{}\n"
                    "\nCheck that the correct assets are loaded into the scene for this character.".format(
                        char, str(error)
                    )
                )
                # Don't raise anything for this, as it is a common error raised and may not be an issue.
                exceptionRaised = False
            except Exception as error:
                reportData.append(
                    "[ERROR] Error while checking scene data for {}\n"
                    "\nCheck that the correct assets are loaded into the scene for this character.".format(char)
                )
                exceptionRaised = True

            # TODO enable or remove once Mocap Post confirms this check is needed...
            # Fix the thumb orientation
            # cls._fixThumbOrientationOnCharacter(char.gameSkelChar)
            # reportData.append("Fixing thumb orientation for '{}".format(char.gameSkelRef.Namespace))

            # Update Watchstar (character only)
            try:
                if updateWatchstar and char.gameSkelRef is not None:
                    char.watchstar.updateCharacter()
                    reportData.append("Set Namespace of {} to {}".format(char.watchstar, char.gameSkelRef.Namespace))
                if updatedChar == False:
                    reportData.append("No updates for this character.")
                reportData.append("")  # Appends whitespace between each character.
            except Exception as error:
                reportData.append(
                    "[ERROR] Error while updating character information into Watchstar for: {}."
                    "Please check that correct character assets are in the trial.".format(char)
                )
                exceptionRaised = True

        end = time.time()
        reportData.append("Took %.2f seconds to process file" % (end - start))

        # Update Watchstar (Talent and Staff Notes for the whole trial)
        try:
            if updateWatchstar:
                role = Context.settingsManager.getProductionRoles(26)  # Get the "Tools Automation" staff role.
                talent = trial.project().getTalentByName("MocapTools PostProcessPrevizSave")
                if not role or not talent:
                    reportData.append(
                        "Could not add report to Watchstar! Check that the MocapTools Automation talent exists."
                    )
                else:
                    automationNote = [note for note in trial.getTrialNotes() if note.creator().name == talent.name]
                    watchstarReport = "\n".join(reportData)
                    if len(automationNote) > 0:
                        automationNote[0].editComment(watchstarReport)
                    else:
                        trial.addNewTrialNote(watchstarReport, role=role, talentProject=talent)
        except Exception as error:
            reportData.append("[ERROR] Error while updating the report to Watchstar.")
            exceptionRaised = True

        # Display the report data.
        logging.info("\n".join(reportData))

        # Raise the exception after everything has processed so as much cleanup occurs as possible.
        if exceptionRaised:
            raise
        return reportData

    @classmethod
    def _fixThumbOrientationOnCharacter(cls, character):
        """Fix the Thumb orientation on a given character

        Args:
            character (pyfbsdk.Character)
        """
        # Finger Rotation Values
        rotationValuesIG = {
            "SKEL_R_Finger00": mobu.FBVector3d(10.344, 15.851, 30.603),
            "SKEL_R_Finger01": mobu.FBVector3d(-0.404659, -1.43456, -16.5556),
            "SKEL_R_Finger02": mobu.FBVector3d(-0.0504775, -1.16117, -13.269),
            "SKEL_L_Finger00": mobu.FBVector3d(-10.3441, -15.8509, 30.6029),
            "SKEL_L_Finger01": mobu.FBVector3d(0.4047, 1.43454, -16.5556),
            "SKEL_L_Finger02": mobu.FBVector3d(0.0501915, 1.16085, -13.2691),
        }

        rotationValuesCS = {
            "SKEL_R_Finger00": mobu.FBVector3d(90, 0, 0),
            "SKEL_R_Finger01": mobu.FBVector3d(0, 0, 0),
            "SKEL_R_Finger02": mobu.FBVector3d(0, 0, 0),
            "SKEL_L_Finger00": mobu.FBVector3d(-90, 0, 0),
            "SKEL_L_Finger01": mobu.FBVector3d(0, 0, 0),
            "SKEL_L_Finger02": mobu.FBVector3d(0, 0, 0),
        }

        namespace = Namespace.GetNamespace(character)

        # Find Mover Constraint
        moverConstraint = None
        for constraint in Globals.Constraints:
            if constraint.LongName == "{0}:mover_toybox_Control".format(namespace):
                moverConstraint = constraint
                break

        if moverConstraint is not None:
            # Store initial state and prepare
            moverState = moverConstraint.Active
            moverConstraint.Active = False
            currentInputChar = character.InputCharacter
            currentInputType = character.InputType
            character.SetCharacterizeOff()
            Globals.Scene.Evaluate()

            # Determine the finger state
            fingerArray = rotationValuesCS
            fingerCheck = Scene.FindModelByName("{0}:{1}".format(namespace, "SKEL_R_Finger00"), True)
            if 89.9 < fingerCheck.Rotation[0] < 90.1:
                fingerArray = rotationValuesIG

            # Set to match the other state
            for fingerName in fingerArray:
                finger = Scene.FindModelByName("{0}:{1}".format(namespace, fingerName), True)
                finger.Rotation = fingerArray[fingerName]
                Globals.Scene.Evaluate()

            # Post Preparation
            character.SetCharacterizeOn(True)
            character.InputCharacter = currentInputChar
            character.InputType = currentInputType
            if currentInputType == mobu.FBCharacterInputType.kFBCharacterInputCharacter:
                character.ActiveInput = True
            moverConstraint.Active = moverState
            Globals.Scene.Evaluate()

    @classmethod
    def _addCharacter(cls, character, correctAssetToLoad):
        """Add a new character to the scene, given its _ProcessCharacterInfo

        Args:
            character(_ProcessCharacterInfo): The character info to use
            correctAssetToLoad (str): The local path of the asset to load in

        Returns:
            tuple[RS.Core.ReferenceSystem.Types.Character, FBCharacter]: ref type and FBCharacter of the swapped asset
        """
        newAssets = Manager.Manager().CreateReferences(correctAssetToLoad)
        if not newAssets or len(newAssets) != 1:
            raise PrevizSaveReferenceError("Unable to create reference to '{}'".format(correctAssetToLoad))
        gameSkelRef = newAssets[0]
        gameSkelChar = cls._getMobuCharacterFromReference(gameSkelRef)
        if not gameSkelChar:
            raise PrevizSaveReferenceError("Unable to get Mobu Character from Reference: '{}'".format(gameSkelRef))
        if character.gsSkelChar:
            gameSkelChar.InputCharacter = character.gsSkelChar
        gameSkelChar.InputType = mobu.FBCharacterInputType.kFBCharacterInputCharacter
        gameSkelChar.ActiveInput = True
        return gameSkelRef, gameSkelChar

    @classmethod
    def _swapCharacter(cls, character, correctAssetToLoad):
        """Swaps a current character to the correct character in the scene, given its _ProcessCharacterInfo

        Args:
            character(_ProcessCharacterInfo): The character info to use
            correctAssetToLoad (str): The local path of the asset to load in

        Returns:
            tuple[RS.Core.ReferenceSystem.Types.Character, FBCharacter]: ref type and FBCharacter of the swapped asset
        """
        gameSkelRef = Manager.Manager().SwapReference(character.gameSkelRef, correctAssetToLoad)
        gameSkelChar = cls._getMobuCharacterFromReference(gameSkelRef)
        gameSkelChar.InputCharacter = character.gsSkelChar
        gameSkelChar.InputType = mobu.FBCharacterInputType.kFBCharacterInputCharacter
        gameSkelChar.ActiveInput = True
        return gameSkelRef, gameSkelChar

    @classmethod
    def _collectAllData(cls, trial):
        """Using a Watchstar Trial, look at the scene and compile the current state of
        Watchstar<->GS Assets<->Game Assets in the scene to get an idea of whats missing and what is correct.

        Args:
            trial (AnimData.Trial): The trial to use to gather info

        Returns:
            list[_ProcessCharacterInfo]: Fully populated _ProcessCharacterInfo classes
        """
        watchstarCharacters = trial.getCharacters()

        # Match up the GS, Reference and Watchstar Characters into loads of dicts
        refTypes = (MocapGsSkeleton.MocapGsSkeleton)  # This line purposely differs from wildwest - the Unknown type is not supported in GTA5.
        gsSkeletons = [ref for ref in Manager.Manager().GetReferenceListAll() if isinstance(ref, refTypes)]

        gsSkelDict = {skel.Namespace.lower().split(":")[0]: skel for skel in gsSkeletons}
        gsSkelNames = gsSkelDict.keys()

        watchstarCharToGsRef = {}
        refGsToMobuChars = {}
        mobuGsCharsToMobuGameChar = {}
        mobuGameCharToRefGameChar = {}

        for char in watchstarCharacters:
            gs = cls._getGsSkelForCharacter(char, gsSkelNames)
            if gs is not None:
                watchstarCharToGsRef[char] = gsSkelDict[gs]

        # Match up GS and their game characters
        for skel in gsSkeletons:
            refGsToMobuChars[skel] = cls._getMobuCharacterFromReference(skel)

        # Get the Mobu Game Characters being driven by Mobu GS
        for mobuCharacter in Globals.Characters:
            mobuGsChar = mobuCharacter.InputCharacter
            if mobuGsChar is not None:
                mobuGsCharsToMobuGameChar[mobuGsChar] = mobuCharacter

        # Match up the mobu game characters with the reference system
        for mobuGameChar in mobuGsCharsToMobuGameChar.itervalues():
            refGameChar = Manager.Manager().GetReferenceByNamespace(Namespace.GetNamespace(mobuGameChar))

            if refGameChar is not None:
                mobuGameCharToRefGameChar[mobuGameChar] = refGameChar

        # Now put it all together
        data = []
        for char in watchstarCharacters:
            watchstar = char
            gsSkelRef = watchstarCharToGsRef.get(watchstar)
            gsSkelChar = refGsToMobuChars.get(gsSkelRef)
            gameSkelChar = mobuGsCharsToMobuGameChar.get(gsSkelChar)
            gameSkelRef = mobuGameCharToRefGameChar.get(gameSkelChar)
            item = _ProcessCharacterInfo(watchstar, gsSkelRef, gsSkelChar, gameSkelRef, gameSkelChar)
            data.append(item)
        return data

    @staticmethod
    def _getMobuCharacterFromReference(gsReference):
        """Gets the FBCharacter components associated with the gsReference.

        By default it gets the character component associated with this reference object.

        Args:
            gsReference (ReferenceSystem.Types.MocapGsSkeleton): reference object to the mocap GS skeleton

        Returns:
            pyfbsdk.FBCharacter
        """
        namespace = Namespace.GetFBNamespace(gsReference.Namespace)

        # Get a handle on our source character component - note we have defended against no
        # characters being present earlier in the update process
        characters = Namespace.GetContentsByType(namespace, mobu.FBCharacter, True)
        if len(characters) == 1:
            return characters[0]
        return None


def Run(updateWatchstar=True):
    """Run PostProcessPrevizSave.

    Args:
        updateWatchstar (bool): If True, the Staff Notes and Roles section will be updated with the results.

    Returns:
        str: Information about how the process ran
    """
    reportData = ProcessScene.process(updateWatchstar=updateWatchstar)
    return reportData
