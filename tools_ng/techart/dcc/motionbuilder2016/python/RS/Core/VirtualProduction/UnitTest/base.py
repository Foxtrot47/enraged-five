import pyfbsdk as mobu
from PySide import QtGui

from RS import Perforce, Globals
from RS.Core.ReferenceSystem.UnitTest import base
from RS.Tools.VirtualProduction.widgets import virtualProductionWidget


class VirtualProductionBase(base.Base):
    """
    Base class for the Virtual Production tests
    """
    def __init__(self, *args, **kwargs):
        super(VirtualProductionBase, self).__init__(*args, **kwargs)
        self.ui = None

    def setUp(self):
        """Clears the scene before running the test"""
        Globals.Application.FileNew()

    def tearDown(self):
        """Clears the scene after running the test"""
        self.assertClose()
        Globals.Application.FileNew()

    def assertClose(self):
        """Close the toolbox"""
        if not self.ui:
            return

        self.ui.close()
        self.assertEquals(False, self.ui.isVisible())
        self.ui = None

    def assertOpen(self):
        """Opens the toolbox"""
        if not self.ui:
            self.ui = virtualProductionWidget.VirtualProductionDialog()

        if not self.ui.isVisible():
            self.ui.show()
        QtGui.qApp.processEvents()  # wait to show ui
        self.assertEquals(True, self.ui.isVisible())

    def assertTabs(self):
        """Switches indices of tabs"""
        for window in self.ui._virtualProductionWidget._docWidgets.itervalues():
            window.raise_()
            QtGui.qApp.processEvents()

    def assertRefresh(self):
        """Press the refresh button"""
        self.ui._virtualProductionWidget.refreshUI(rebuildStageSpecificModels=True)

    def assertExists(self, name):
        """
        Args:
            name (str): Name of model to assert
        """
        self.assertIsNotNone(mobu.FBFindModelByLabelName(name))
