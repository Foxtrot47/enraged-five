"""Classes for creating and managing the TPVC device.

TODO background info on the device

TODO examples
"""
import os

import pyfbsdk as mobu

from RS import Globals, ProjectData, Perforce
from RS.Core.Camera import Lib as CamLib
from RS.Utils import IO, Namespace, Scene
from RS.Utils.Scene import Huds, Devices, RelationshipConstraintManager as rcm


class Lens(object):
    """Simple object for storing data on each camera lens."""

    def __init__(self, mobuIndex, fov, name):
        self.mobuIndex = mobuIndex
        self.name = name
        self.fov = fov


class TpvcDevice(object):
    """TPVC device manager."""
    # todo: rewrite this terrible class
    _TPVC_DEVICE_CLASS = "TPVcam"

    def __init__(self, device=None, color="Red"):
        """Initialize class.

        Args:
            device (FBDevice): The FB Device to wrap
        """
        if device is None:
            device = self._addTpvcDevice()
        self._device = device
        self.color = color

    @staticmethod
    def _addTpvcDevice():
        """Add a new new device

        Returns:
            FBDevice for the newly created device
        """
        device = mobu.FBCreateObject("Browsing/Templates/Devices", "TP Vcam", "TP Vcam")
        Globals.Scene.Devices.append(device)
        return device

    def device(self):
        """Get the Mobu FBDevice

        Returns:
            FBDevice
        """
        return self._device

    def name(self):
        """Get the Name of the device

        Returns:
            string of the name
        """
        return self._device.Name

    def getOrCreateBoundCamera(self):
        """Get or Create a bound camera for the device

        Returns:
            FBCamera that is bound to the device
        """
        cam = self.boundCamera()
        if cam is None:
            self._device.ModelBindingCreate()
            cam = self.boundCamera()
        return cam

    def boundCamera(self):
        """Get the bound camera for the device, or None if there isnt one

        Returns:
            FBCamera that is bound to the device, or None
        """
        root = self._device.ModelBindingRoot
        if root is None:
            return None
        cameras = [model for model in root.Children if isinstance(model, mobu.FBCamera)]
        if len(cameras) == 0:
            return None
        return cameras[0]

    @classmethod
    def getAllTpvcDevices(cls):
        """Get all the TPVC Deivces in the scene

        Returns:
            list of TpvcDevice
        """
        return [TpvcDevice(dev) for dev in Devices.GetAllDeviceByType(cls._TPVC_DEVICE_CLASS)]


class SetupTpvcDeviceCamera(object):
    """Class for setting up a TPVC Camera Device."""
    # todo: rewrite this terrible class

    _CAMERA_LENS_NAME = "Lens"
    _CAMERA_LENSES = None
    FOV_TO_MM_MAPPINGS = [
        # WVCam Fov, Lens MM
        (6.477, 10),
        (7.772, 12),
        (9.068, 14),
        (10.363, 16),
        (11.659, 18),
        (13.602, 21),
        (16.192, 25),
        (17.490, 27),
        (22.670, 35),
        (25.910, 40),
        (32.379, 50),
        (42.100, 65),
        (48.580, 75),
        (64.770, 100),
        (80.960, 125),
        (97.150, 150),
    ]

    @classmethod
    def getVirtualCameraLenses(cls):
        """Get the Lens objects... TODO

        Returns:
            list of tpvcLib.Lens
        """
        if cls._CAMERA_LENSES is None:
            lens = []
            for idx, data in enumerate(cls.FOV_TO_MM_MAPPINGS):
                wvcamfov, lensMm = data
                lens.append(Lens(idx, wvcamfov, "{} mm".format(lensMm)))
            cls._CAMERA_LENSES = lens
        return cls._CAMERA_LENSES

    @classmethod
    def setup(cls, device):
        """Set up a given device for previz, with a camera, constraint and HUD

        Args:
            device (TpvcDevice): The device to setup
        """
        camera = device.getOrCreateBoundCamera()
        cls._setupCameraOptions(camera)
        cls._setupCameraLens(camera)
        cls._setupRelationshipConstraint(device, camera)
        cls._setupHud(camera, color=device.color)

    @classmethod
    def setMocapIP(cls, device, ipTuple):
        """ Sets IP address and port for mocap shooting. """
        ipProp = [prop for prop in device.device().PropertyList if prop.Name == "IP Address"]
        portProp = [prop for prop in device.device().PropertyList if prop.Name == "Port"]
        if ipProp and portProp:
            ipProp[0].Data = ipTuple[0]
            portProp[0].Data = ipTuple[1]

    @classmethod
    def _setupRelationshipConstraint(cls, device, camera):
        """Setup the constraint that reformats the lens label from FOV to MM for the hud.

        Args:
            device (TpvcDevice): The device to drive the constraint
            camera (FBCamera): The camera to setup the options on
        """
        constraint = rcm.RelationShipConstraintManager(name="{}:LensHudConstraint".format(device.device().LongName))
        camSrcBox = constraint.AddSenderBox(camera)

        curveBox = constraint.AddFunctionBox("Other", "FCurve Number (%)")
        curveBox.SetBoxPosition(500, 100)
        constraint.ConnectBoxes(camSrcBox, "FocalLength", curveBox, "Position %")

        fcurveValueNode = curveBox.GetBox().AnimationNodeOutGet().Nodes[0]
        lensCurve = fcurveValueNode.FCurve
        lensCurve.EditBegin()
        lensCurve.EditClear()

        # Set the curve in the curveBox
        lensCurveData = {lens.mobuIndex: lens.fov for lens in cls.getVirtualCameraLenses()}
        lensCurve.KeyAdd(mobu.FBTime(0, 0, 0, 0), 0)
        for frame, value in lensCurveData.iteritems():
            lensCurve.KeyAdd(mobu.FBTime(0, 0, 0, int(value)), frame)
        lensCurve.KeyAdd(mobu.FBTime(0, 0, 0, 100), len(lensCurveData) - 1)

        # Set the keys to be stepped
        for key in lensCurve.Keys:
            key.Bias = 4.09
            key.LeftBezierTangent = frame
            key.RightBezierTangent = frame
            key.LeftTangentWeight = 0.3
            key.RightTangentWeight = 0.3
            key.TangentBreak = False
            key.TangentClampMode = mobu.FBTangentClampMode.kFBTangentClampModeNone
            key.TangentConstantMode = mobu.FBTangentConstantMode.kFBTangentConstantModeNormal
            key.TangentMode = mobu.FBTangentMode.kFBTangentModeUser
            key.Interpolation = mobu.FBInterpolation.kFBInterpolationConstant

        lensCurve.EditEnd()

        intBox = constraint.AddFunctionBox("Number", "Integer")
        intBox.SetBoxPosition(1200, 100)
        constraint.ConnectBoxes(curveBox, "Value", intBox, "a")

        camDestBox = constraint.AddReceiverBox(camera)
        camDestBox.SetBoxPosition(1500, 100)
        constraint.ConnectBoxes(intBox, "Result", camDestBox, cls._CAMERA_LENS_NAME)
        constraint.SetActive(True)

    @classmethod
    def _setupCameraOptions(cls, camera):
        """Setup the camera options

        Args:
            camera (FBCamera): The camera to setup the options on
        """
        # Set Options
        camera.ViewOpticalCenter = False
        camera.FrameColor = mobu.FBColor(0.0, 0.0, 0.0)
        camera.BackGroundColor = mobu.FBColor(0.16, 0.16, 0.16)
        camera.ResolutionMode = mobu.FBCameraResolutionMode.kFBResolutionHD
        camera.PixelAspectRatio = 1.0
        camera.ApertureMode = mobu.FBCameraApertureMode.kFBApertureVertical
        camera.FilmSizeWidth = 0.816
        camera.FilmSizeHeight = 0.612
        camera.ViewShowTimeCode = False

        # Add Frame Mask
        mobuCamera = CamLib.MobuCamera(camera)
        mobuCamera.SetupFrameMask()

        # Remove BackGround Texture
        # TODO: below code is clunky because calling cam.BackGroundTexture = None crashes Mobu?
        if camera.BackGroundTexture:
            dstCount = camera.BackGroundTexture.GetDstCount()
            for dstIndex in xrange(dstCount):
                dstItem = camera.BackGroundTexture.GetDst(dstIndex)
                if dstItem.GetOwner() == camera and dstItem.Name == "Background Texture":
                    camera.BackGroundTexture.DisconnectDstAt(dstIndex)
                    break

    @classmethod
    def _setupCameraLens(cls, camera):
        """Setup the Lens for the given Camera

        Args:
            camera (FBCamera): The camera to setup the Lens on
        """
        lensProp = camera.PropertyCreate(cls._CAMERA_LENS_NAME, mobu.FBPropertyType.kFBPT_enum, "Enum", True, True, None)
        lensProp.SetAnimated(True)
        mobu.FBSystem().Scene.Evaluate()
        lensEnum = lensProp.GetEnumStringList(True)
        for lens in cls.getVirtualCameraLenses():
            lensEnum.Add(lens.name)
        lensProp.NotifyEnumStringListChanged()
        lensProp.SetAnimated(True)
        mobu.FBSystem().Scene.Evaluate()

    @classmethod
    def _setupHud(cls, camera, color="Red"):
        camName = camera.LongName.split(":")[0]
        hudName = "{}_Hud".format(camName)
        hudColorName = "{}_HudColor".format(camName)
        hudLensName = "{}_HudLens".format(camName)
        hudFrameName = "{}_HudFrame".format(camName)

        vcamHud = mobu.FBHUD(hudName)
        mobu.FBSystem().Scene.ConnectSrc(vcamHud)
        camera.ConnectSrc(vcamHud)

        # Color Box
        hudColorBox = mobu.FBHUDRectElement(hudColorName)
        vcamHud.ConnectSrc(hudColorBox)
        hudColorBox.Justification = mobu.FBHUDElementHAlignment.kFBHUDCenter
        hudColorBox.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDCenter
        hudColorBox.ScaleUniformly = False
        hudColorBox.X = 0
        hudColorBox.Y = 5.5
        hudColorBox.Width = 10
        hudColorBox.Height = 4.5

        # Set Color - based on cam name
        vcamColorDict = {
            "Red": mobu.FBColorAndAlpha(1.0, 0.0, 0.0, 0.5),
            "Yellow": mobu.FBColorAndAlpha(1.0, 1.0, 0.0, 0.5),
            "Blue": mobu.FBColorAndAlpha(0.0, 0.0, 1.0, 0.5),
            "Orange": mobu.FBColorAndAlpha(1.0, 0.5, 0.0, 0.5),
        }
        unknownCamColor = mobu.FBColorAndAlpha(1.0, 1.0, 1.0, 1.5)
        hudColorBox.Color = vcamColorDict.get(color, unknownCamColor)

        # Lens Text
        hudLensText = mobu.FBHUDTextElement(hudLensName)
        vcamHud.ConnectSrc(hudLensText)
        camLensProp = camera.PropertyList.Find("Lens")
        hudLensText.PropertyAddReferenceProperty(camLensProp)
        hudLensText.BackgroundColor = mobu.FBColorAndAlpha(0.0, 0.0, 0.0, 0.0)
        hudLensText.Content = "%s"
        hudLensText.Height = 7
        hudLensText.X = 2
        hudLensText.Y = 4

        # Frame Text
        hudFrameText = mobu.FBHUDTextElement(hudFrameName)
        vcamHud.ConnectSrc(hudFrameText)
        hudFrameProp = vcamHud.PropertyList.Find("ActionTime")
        hudFrameText.PropertyAddReferenceProperty(hudFrameProp)
        hudFrameText.BackgroundColor = mobu.FBColorAndAlpha(0.0, 0.0, 0.0, 0.0)
        hudFrameText.Content = "%s"
        hudFrameText.Height = 7
        hudFrameText.Justification = mobu.FBHUDElementHAlignment.kFBHUDCenter
        hudFrameText.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDCenter
        hudFrameText.Y = 4


def GetVcamHud(vcamDevice):
    camera = vcamDevice.boundCamera()
    if camera:
        validHud = [hud for hud in camera.HUDs if hud.Name.lower().startswith("tpvc")]
        if validHud:
            return validHud[0]


def UpdateHudAlpha(vcamDevice, alpha):
    vcamHud = GetVcamHud(vcamDevice)
    if vcamHud:
        hudColor = [hud for hud in vcamHud.Elements if "hudcolor" in hud.Name.lower()]
        if hudColor:
            colorList = list(hudColor[0].Color)
            colorList[3] = alpha
            hudColor[0].Color = mobu.FBColorAndAlpha(colorList)


def DeleteVcamHud(vcamDevice):
    vcamHud = GetVcamHud(vcamDevice)
    if vcamHud:
        deleteList = list(vcamHud.Elements)
        deleteList.append(vcamHud)
        [comp.FBDelete() for comp in deleteList]


def DeleteVcamDevices():
    # Disable Devices - otherwise Mobu Crashes
    vcamDevices = TpvcDevice.getAllTpvcDevices()
    for vcamDevice in vcamDevices:
        vcamDevice.device().Live = False
        vcamDevice.device().Online = False
        vcamDevice.device().RecordMode = False
        if mobu.FBPlayerControl().IsRecording is True:
            mobu.FBPlayerControl().Record(True)
        # Delete Hud
        DeleteVcamHud(vcamDevice)

    # todo: clean up this messy namespace stuff, god
    # There are multiple namespaces for the TPVCams. Try to check all of them.
    tpvCamObjects = Scene.FindObjectsByNamespace("TPVC*", namespaceOnly=True)
    tpvCamObjects.extend(Scene.FindObjectsByNamespace("TP V*", namespaceOnly=True))
    # Delete all the devices and the assets in the Navigator related to those devices.
    namespaces = []
    for obj in tpvCamObjects:
        namespaces.append(Namespace.GetFBNamespace(obj.LongName.split(":")[0]))
    for namespace in set(namespaces):
        # print namespace.Name
        if namespace:
            Namespace.Delete(namespace)

    # Delete Vcam Devices
    for vcamDevice in vcamDevices:
        vcamDevice.device().FBDelete()
