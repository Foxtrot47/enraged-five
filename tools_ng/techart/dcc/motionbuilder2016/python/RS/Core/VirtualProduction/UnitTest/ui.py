"""
Unittests for testing basic ui operations
"""
import unittest

from RS.Core.VirtualProduction.UnitTest import base


def Run():
    """Run unittests from this module"""
    unittest.TextTestRunner(verbosity=0).run(suite())


def suite():
    """A suite to hold all the tests from this module

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()

    for functionName in dir(Test_UI):
        if functionName.startswith("test_") and not functionName.endswith(("_Scene", "_Referencing")):
            suite.addTest(Test_UI(functionName))
    return suite


class Test_UI(base.VirtualProductionBase):
    """UI Tests"""

    def test_Close(self):
        """Testing closing the ui"""
        self.assertOpen()
        self.assertClose()

    def test_Open(self):
        """Testing opening the ui"""
        self.assertOpen()

    def test_Tabs(self):
        """Testing selecting tabs"""
        self.assertOpen()
        self.assertTabs()

    def test_Refresh(self):
        """Testing refresh of ui"""
        self.assertOpen()
        self.assertRefresh()
