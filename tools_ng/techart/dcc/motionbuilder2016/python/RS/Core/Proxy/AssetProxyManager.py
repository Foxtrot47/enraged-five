__author__ = 'mharrison-ball'

'''
This to to work with swapping out assets in cutscenes for test purposes

FIles to be changed:

* Data.cutxml
* Data_master.cutxml
* Rename the anim. files
* Extract Data and rename clips
    x:\gta5\tools_ng\bin\anim\cliprename.exe

'''

# x:\gta5\tools_ng\bin\anim\cliprename.exe

import RS.Config
import os, subprocess, shutil, stat, re
import RS, RS.Utils.ZipUtils as ziputils, RS.Utils.ClipFile as clipfile

import xml.etree.cElementTree as xml
from glob import glob
from xml.etree.ElementTree import ElementTree
from RS.Utils.AssetConversion import convert_asset

reload(clipfile)
reload(ziputils)


CLIPRENAME_EXE = "{0}/anim/cliprename.exe".format( RS.Config.Tool.Path.Bin)




class proxy:
    def __init__(self, Cutpath):
        self.cutpath = Cutpath

        # Need to chnage to recursive look for xml files
        self.xml_data_paths = ["{0}//data.cutxml".format(self.cutpath),
                               "{0}//data_master.cutxml".format(self.cutpath),
                               "{0}//Shot_1.cutxml".format(self.cutpath),
                               "{0}//Shot_1//data.cutxml".format(self.cutpath),
                               "{0}//Shot_1//data_stream.cutxml".format(self.cutpath)]


        self.asset_name = None
        self.asset_lookup = None

    # prviate
    def _rename_cutxml(self, xml_file):

        xml_tree = ElementTree()
        xml_tree = xml.parse(xml_file)
        xml_element = xml_tree.getroot()

        '''
        Replace
        '''
        #cName
        #StreamingName
        #bFoundFaceAnimation -> true
        #bFaceAndBodyAreMerged  -> true

        '''
        Remove
        '''
        #cFaceExportCtrlSpecFile
        #faceAnimationNodeName
        #faceAttributesFilename


        #for child in xml_element:
        #    print child.tag

        val = xml_element.findall('./pCutsceneObjects/')
        for child in val:
            if child.attrib['type'] == 'rage__cutfPedModelObject':
                for elm in child.findall('./cName'):
                    if elm.text:
                         elm.text = elm.text.replace(self.asset_lookup, self.asset_name)

                for elm in child.findall('./StreamingName'):

                    if elm.text:
                         elm.text = elm.text.replace(self.asset_lookup, self.asset_name) #self.asset_name

                for elm in child.findall('./faceAnimationNodeName'):
                    if elm.text:
                        elm.text = ''

                for elm in child.findall('./faceAttributesFilename'):
                    if elm.text:
                        elm.text = ''

                for elm in child.findall('./cFaceExportCtrlSpecFile'):
                    if elm.text:
                        elm.text = ''

                for elm in child.findall('./bFoundFaceAnimation'):
                    #if elm:
                    elm.attrib['value'] = 'false'

                for elm in child.findall('./bOverrideFaceAnimation'):
                    #if elm:
                    elm.attrib['value'] = 'true'

        xml_tree.write(xml_file)
        return

    '''
        Description: Will look through folder recursively and update the xml data
    '''
    def _reanme_xmls(self):
        cutxml_list = [os.path.join(dp, f) for dp, dn, filenames in os.walk(self.cutpath) for f in filenames if os.path.splitext(f)[1] == '.cutxml']
        for xml_data in cutxml_list:
            self._rename_cutxml(xml_data)

    '''
        Description: Will rename the anims file found with the new anim files
    '''
    def _rename_anim(self):
        for filename in os.listdir(self.cutpath):
            if filename.startswith(self.asset_lookup):
                new_filename = filename.replace(self.asset_lookup, self.asset_name)
                os.rename(os.path.join(self.cutpath, filename), os.path.join(self.cutpath, new_filename))
        return

    def _rename_clips(self):
        return_code = subprocess.call("{0} -dir={1}".format(CLIPRENAME_EXE, self.cutpath), shell=True)
        return return_code

    # move to an deiffernt module
    def _rebuild_data(self):
        return

    # check if the fbx file is from a dlc or core project
    def _get_export_rootpath(self):
        export_rootpath = RS.Config.Project.Path.Export

        for dlcname in RS.Config.DLCProjects:
            if dlcname in self.cutpath:
                export_rootpath = RS.Config.DLCProjects[dlcname].Path.Export
                break
        return export_rootpath



    def _get_mission_name(self):
        cut_file = glob("{0}/*.clip".format(self.cutpath))
        if len(cut_file) > 0:

            test = clipfile.ClipFile(cut_file[0])
            test_property = test.GetProperty('FBXFile_DO_NOT_RESOURCE')
            test_attr = test_property.GetAttribute('FBXFile_DO_NOT_RESOURCE')

            matches = re.search('\w+.!!scenes\\\\(\w+.)\\\\', test_attr.GetValue())
            if matches and len(matches.groups()) > 0:
                return matches.group(1)
        return False


    def _get_mission_zip_path(self):
        export_path = "{0}/anim/cutscene/{1}".format(self._get_export_rootpath(), self._get_mission_name())
        return export_path


    def _addto_zip(self):
        head, tail = os.path.split(self.cutpath)
        zip_file = "{0}.icd".format( os.path.join(self._get_mission_zip_path(),tail))
        ziputils.zipAssets(self.cutpath,  zip_file)
        return zip_file


    def _remove_readonly(self, fn, path, excinfo):
        try:
            os.chmod(fn, path, stat.S_IWRITE)
            fn(path)
        except Exception as exc:
            print "Skipped:", path, "because:\n", exc
        return

    def make_writeable(self):
        shutil.rmtree(self.cutpath, onerror=self._remove_readonly)


    # Public methods

    def replace_asset(self, old_name, new_name, build=True):
        self.asset_name = new_name
        self.asset_lookup = old_name

        self._reanme_xmls()
        self._rename_anim()
        self._rename_clips()

        if build:
            convert_asset(self._addto_zip())

        return

#cutpath = "X:\\gta5_dlc\\mpPacks\\mpHeist\\assets_ng\\cuts\\MPH_NAR_FIN_EXT_P4_L1_P3"
#cutpath = r"x:\gta5_dlc\mpPacks\mpHeist\assets_ng\cuts\MPH_NAR_FIN_INT_P1_T04"
#_proxy = proxy(cutpath)

#print _proxy._get_mission_zip_path()
#_proxy._addto_zip()

#_proxy.replace_asset("MP_M_Freemode_01","ig_bride")
#_proxy.make_writeable()


#_proxy._rename_clips()
#_proxy.replaceAsset("test")



