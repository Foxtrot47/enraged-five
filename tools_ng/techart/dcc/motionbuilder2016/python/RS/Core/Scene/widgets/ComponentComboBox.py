"""
ComboBox that offers a list of components currently in the scene
"""
from PySide import QtGui

import pyfbsdk as mobu

from RS import Globals
from RS.Core.Scene.models import ComponentModel, SceneMonitor, ComponentsMonitor


class ComponentComboBox(QtGui.QComboBox):
    """
    ComboBox that offers a list of components currently in the scene
    """
    def __init__(self, componentType=mobu.FBComponent, parent=None):
        """
        Constructor

        Arguments:
            componentType (pyfbsdk.FBComponet): pyfbsdk component class to filter components by
            parent (QtGui.QWidget): parent qwidget

        """
        super(ComponentComboBox, self).__init__(parent=parent)

        self._enabled = True
        self._previousComponentCount = len(Globals.Components)
        self.componentType = componentType

        self._model = ComponentModel.ComponentModel(componentType)
        self._model.setShowLongName(True)

        self.filter = QtGui.QSortFilterProxyModel()
        self.filter.setSourceModel(self._model)
        self.setModel(self.filter)

        monitor = SceneMonitor.SceneMonitor()
        monitor.connect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Attach)
        monitor.connect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Detach)
        monitor.connect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.FileOpenCompleted)

    def updateModel(self, event=None, countChanged=False):
        """
        Updates the model so it contains the current components of the scene

        Arguments:
            event (pyfbsdk.FBEvent): Motion Builder event that triggered this method
            countChanged (boolean): the number of the components being monitored in the scene has changed

        """
        if not countChanged:
            return

        # Call to text is just to trigger error that the combobox has been destroyed to avoid
        # repopulating the model if it has been closed.
        self.currentText()

        # Update model
        self._model.reset()
        self.filter.setSourceModel(self._model)

        # Set combobox to index 0
        # If you don't force this, the box will be blank and the user will have to manually select the combobox to set an active character
        self.setCurrentIndex(0)

    def closeEvent(self, event):
        """
        Overrides built-in method.
        Removes callbacks when UI is closed

        Arguments:
            event (QtGui.QEvent): the event being triggered by Qt

        """
        monitor = SceneMonitor.SceneMonitor()
        monitor.disconnect(self.componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Attach)
        monitor.disconnect(self.componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Destroy)
        monitor.disconnect(self.componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.FileOpenCompleted)
