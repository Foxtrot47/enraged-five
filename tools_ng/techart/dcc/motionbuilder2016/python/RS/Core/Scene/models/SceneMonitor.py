"""
Class for adding a Motion Builedr Callbacks that are tied to an UI that should only be called when items are
created or deleted in the scene. All methods are stored in standard python lists and they get picked up and removed
properly by python's Garbage Collection so mobu doesn't hang once the few mobu callbacks that are setup are removed.\

Example:
import pyfbsdk as mobu
from RS.Core.Scene.models import SceneMonitor

def PrintNewCameraCreated(event, count):
    print event.Component.LongName, " has been created"

SCENE_MONITOR = SceneMonitor.SceneMonitor()

# Add callback when a new camera is created
SCENE_MONITOR.camera.attached.connect(PrintNewCameraCreated)

# Create Camera
mobu.FBCamera("My New Camera")

# Remove callback when a new camera is created
SCENE_MONITOR.camera.attached.disconnect(PrintNewCameraCreated)
"""
import sys

import pyfbsdk as mobu

from RS import Globals
from RS.Tools.CameraToolBox.PyCore.Decorators import Singleton
from RS.Core.Scene.models import ComponentsMonitor


def toggle(func):
    """
    Toggles the scene monitor off before executing a function that may cause slowdowns and/or errors with the monitor
    on
    Arguments:
        func (function): decorated function

    Return:
        function
    """
    def wrap(*args, **kwargs):
        sceneMonitor = SceneMonitor()
        previouslyEnabled = sceneMonitor.isEnabled()
        sceneMonitor.disableCallback()
        try:
            return func(*args, **kwargs)
        except:
           exception = sys.exc_info()
           raise exception[0], exception[1], exception[2]
        finally:
            if previouslyEnabled:
                sceneMonitor.enableCallback()
    return wrap


@Singleton.Singleton
class SceneMonitor(object):
    """
    A Singleton for managing SceneChange instances that trigger when a component in motion builder has been
    created or deleted.
    """

    __ENABLED = False

    def __init__(self):
        """
        Constructor
        """
        self._cache = {}
        self._monitors = {}
        self._callbacksAdded = False

    def __getattr__(self, item):
        """
        Overrides built in method.
        If a property name has a matching pyfbsdk.FBComponent type, create or return a ComponentsMonitor object.

        Arguments:
            item (string): name of the property being requested of the object

        Return:
            object
        """
        if item in ("_cache", "_monitors", "_enabled"):
            return object.__getattribute__(self, item)

        componentType = getattr(mobu, "FB{}{}".format(item[0].upper(), item[1:]), None)
        componentMonitor = self._getComponentMonitor(componentType)
        if componentMonitor:
            return componentMonitor
        raise AttributeError, "'SceneMonitor' object has no attribute {}".format(item)

    def __setattr__(self, key, value):
        """
        Overrides built in method.
        If a property name has a matching pyfbsdk.FBComponent type, don't override the value.

        Arguments:
            key (string): name of the property being requested of the object
            value (object): value to set for the property

        """
        if key not in ("_cache", "_monitors", "_enabled"):

            componentType = self._cache.get(key, None)
            if not componentType:
                componentType = getattr(mobu, "FB{}".format(key.capitalize()), None)

            if componentType and isinstance(componentType, mobu.FBComponent):
                return

        object.__setattr__(self, key, value)

    def _getComponentMonitor(self, componentType, force=True):
        """
        gets the component monitor that is associated with the given component type

        componentType (pyfbsdk.FBComponent): type of component to get monitor from
        force (boolean): return/create a monitor if no monitor exists for the given component type

        Return:
            RS.Core.Scene.models.ComponentsMonitor.ComponentsMonitor()
        """
        componentMonitor = self._monitors.get(componentType, None)
        if componentMonitor is None and force:
            componentMonitor = ComponentsMonitor.ComponentsMonitor(componentType)
            self._monitors[componentType] = componentMonitor

        if not self._callbacksAdded:
            self.addMotionBuilderCallbacks()

        return componentMonitor

    def enableCallback(self, control=None, event=None):
        """
        Enables the callback to execute.

        This method does not add the callbacks to Motion  Builder, use the addMotionBuilderCallbacks instead.

        Arguments:
            control (object): Motion Builder object calling this method
            event (pyfbsdk.FBEvent): Motion Builder event that triggered this method

        """
        self.__ENABLED = True
        for monitor in self._monitors.itervalues():
            monitor.setEnabled(True)

    def disableCallback(self, control=None, event=None):
        """
        Disables the callback from executing.

        This method does not remove the callbacks from Motion Builder, use the removeMotionBuilderCallbacks instead.

        Arguments:
            control (object): Motion Builder object calling this method
            event (pyfbsdk.FBEvent): Motion Builder event that triggered this method

        """
        self.__ENABLED = False
        for monitor in self._monitors.itervalues():
            monitor.setEnabled(False)

    def isEnabled(self):
        """
        Is the Scene Monitor enabled

        Return:
            boolean
        """
        return self.__ENABLED

    def connect(self, componentType, func, eventType=None):
        """
        Adds a function that is connected to a monitor that is watching over components of the given type

        Arguments:
            componentType (mobu.FBComponent): type of the component
            func (func): method to un-attach to monitor
            eventType (mobu.FBEventName): the type of event attach function too.
        """
        componentMonitor = self._getComponentMonitor(componentType)
        if componentMonitor:
            componentMonitor.connect(func, eventType)

    def disconnect(self, componentType, func, eventType=None):
        """
        Removes a function that is connected to a monitor that is watching over components of the given type

        Arguments:
            componentType (mobu.FBComponent): type of the component
            func (func): method to un-attach to monitor
            eventType (mobu.FBEventName): the type of event attach function too.
        """
        componentMonitor = self._monitors.get(componentType, None)
        if componentMonitor is None:
            return False

        disconnected = componentMonitor.disconnect(func, eventType)
        if not componentMonitor.isActive():
            self._monitors.pop(componentType)
        if self._callbacksAdded and not self._monitors:
            self.removeMotionBuilderCallbacks()
        return disconnected

    def disconnectAll(self):
        """
        Removes all the callbacks on the ComponentsMonitor
        """
        for monitor in self._monitors.itervalues():
            monitor.disconnect()

    def addMotionBuilderCallbacks(self):
        """ Adds motion builder callback so whenever a selection is made the widget characters selection is updated """
        if self._callbacksAdded:
            return False
        # Disables tool from updating when a new file is being loaded.
        Globals.Callbacks.OnFileNew.Add(self.disableCallback)
        Globals.Callbacks.OnFileOpen.Add(self.disableCallback)
        Globals.Callbacks.OnFileMerge.Add(self.disableCallback)

        # Reactivates automatic update of the tool when the new files are finished loading
        Globals.Callbacks.OnFileNewCompleted.Add(self.enableCallback)
        Globals.Callbacks.OnFileOpenCompleted.Add(self.enableCallback)

        Globals.Callbacks.OnFileExit.Add(self.removeMotionBuilderCallbacks)
        self._callbacksAdded = True
        return True

    def removeMotionBuilderCallbacks(self, control=None, event=None):
        """
        Removes motion builder callback so whenever a selection is made the widget characters selection is updated
        """
        if not self._callbacksAdded:
            return False

        self._callbacksAdded = False

        self.disconnectAll()

        # Disables tool from updating when a new file is being loaded.
        Globals.Callbacks.OnFileNew.Remove(self.disableCallback)
        Globals.Callbacks.OnFileOpen.Remove(self.disableCallback)
        Globals.Callbacks.OnFileMerge.Remove(self.disableCallback)

        # # Reactivates automatic update of the tool when the new files are finished loading
        Globals.Callbacks.OnFileNewCompleted.Remove(self.enableCallback)
        Globals.Callbacks.OnFileOpenCompleted.Remove(self.enableCallback)

        Globals.Callbacks.OnFileExit.Remove(self.removeMotionBuilderCallbacks)
        return True
