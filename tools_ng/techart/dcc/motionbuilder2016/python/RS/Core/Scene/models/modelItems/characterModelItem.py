from PySide import QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class CharacterTextModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    def __init__(self, characterName, characterObject, parent=None):
        super(CharacterTextModelItem, self).__init__([], parent=parent)
        self._characterName = characterName
        self._characterObject = characterObject
        self._checked = False

    def SetCheckedStateOn(self):
        '''
        Sets check state to False for item
        '''
        self._checked = True

    def SetCheckedStateOff(self):
        '''
        Sets check state to False for item
        '''
        self._checked = False

    def GetCheckedState(self):
        '''
        returns current Check state of item
        '''
        return self._checked

    def GetCharacterNamespace(self):
        '''
        returns character namespace for item
        '''
        return self._characterName

    def GetCharacterObject(self):
        '''
        returns character node for item
        '''
        return self._characterObject

    def columnCount(self):
        '''
        returns the number of columns in view
        '''
        return self._columnNumber

    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()

        if role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
            if column == 0:
                return self._characterName

        if role == QtCore.Qt.UserRole:
            return self._characterObject

        if role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if self._checked:
                    return QtCore.Qt.CheckState.Checked
                else:
                    return QtCore.Qt.CheckState.Unchecked
        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        model data updated based off of user interaction with the view
        '''
        column = index.column()
        if role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if value == QtCore.Qt.CheckState.Checked:
                    self._checked = True
                else:
                    self._checked = False
                return True
        return False

