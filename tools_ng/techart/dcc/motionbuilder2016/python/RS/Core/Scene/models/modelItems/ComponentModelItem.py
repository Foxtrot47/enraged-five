"""
Custom Model for getting a list of components in Motion Builder
"""
import os
import re

from PySide import QtCore, QtGui

from RS import Config
from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize


class ComponentItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    Name = QtCore.Qt.DisplayRole
    LongName = QtCore.Qt.ToolTipRole
    Type = QtCore.Qt.UserRole
    Selected = QtCore.Qt.UserRole + 1
    Namespace = QtCore.Qt.UserRole + 2
    Component = QtCore.Qt.UserRole + 3
    ShowShortName = QtCore.Qt.UserRole + 4
    ItemRole = QtCore.Qt.UserRole + 5

    def __init__(self, component, parent=None):
        """
        Constructor

        Arguments:
            component (pyfbsdk.FBCompenent): component to access data from
            parent (ComponentItem): parent item

        """
        super(ComponentItem, self).__init__([], parent=parent)
        self._component = component
        self._showShortName = True
        self._roles = {}

    def columnCount(self):
        """
        returns the number of columns in view
        """
        return self._columnNumber

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        model data generated for view display

        Arguments:
            index (QtGui.QModelIndex): modelIndex to get information from
            role (QtCore.Qt.QRole): role that is being requested by the data

        """
        if isinstance(index, QtCore.QModelIndex) and not index.isValid():
            return None

        # Check that component has not been deleted from the scene
        if self._component.__class__.__name__.endswith("_Unbound"):
            self._component = None

        if role == self.ShowShortName:
            return self._showShortName

        if role == self.ItemRole:
            return self

        if self._component is not None:
            if self._showShortName and role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
                return self._component.Name

            elif role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole, QtCore.Qt.ToolTipRole]:
                return self._component.LongName

            elif role == QtCore.Qt.DecorationRole:
                # Removes FB at the start of the class name only if the name starts with FB
                componentType = re.sub("^FB", "", self._component.__class__.__name__).lower()
                path = os.path.join(Config.Script.Path.ToolImages,
                                    "MotionBuilder", "{}.png".format(componentType))
                if not os.path.exists(path):
                    path = os.path.join(Config.Script.Path.ToolImages, "MotionBuilder", "model.png")
                return self._getIcon(path)

            elif role == self.Type:
                return self._component.__class__

            elif role == self.Selected:
                return self._component.Selected

            elif role == self.Namespace:
                return self._component.LongName.split(":", 1)[0]

            elif role == self.Component:
                return self._component

        return self._roles.get(role, None)

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """
        model data updated based off of user interaction with the view

        Arguments:
            index (QtGui.QModelIndex): modelIndex to get information from
            value (object): value to set
            role (QtCore.Qt.QRole): role that is being requested by the data

        """
        if role == self.ShowShortName:
            self._showShortName = value

        else:
            self._roles[role] = value

    def flags(self, index):
        return QtCore.Qt.ItemIsDropEnabled|QtCore.Qt.ItemIsDragEnabled|QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEnabled

    @memorize.memoized
    def _getIcon(self, path):
        return QtGui.QIcon(path)
