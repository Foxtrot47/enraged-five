"""
Line Edit that offers autocompletion of names of components currently in the scene
"""
from PySide import QtGui

import pyfbsdk as mobu

from RS.Core.Scene.models import ComponentModel, SceneMonitor, ComponentsMonitor


class ComponentLineEdit(QtGui.QLineEdit):
    """
    Line Edit that offers autocompletion of names of components currently in the scene
    """
    def __init__(self, componentType=mobu.FBComponent, parent=None):
        """
        Constructor

        Arguments:
            componentType (pyfbsdk.FBComponet): pyfbsdk component class to filter components offered for autocompletion
            parent (QtGui.QWidget): parent qwidget

        """
        super(ComponentLineEdit, self).__init__(parent=parent)

        self._enabled = True
        self._previousComponentCount = len(mobu.FBSystem().Scene.Components)
        self.componentType = componentType

        self._model = ComponentModel.ComponentModel()
        self._model.setShowLongName(True)
        self._completer = QtGui.QCompleter()
        self._completer.setModel(self._model)
        self.setCompleter(self._completer)

        monitor = SceneMonitor.SceneMonitor()
        monitor.connect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Attach)
        monitor.connect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Detach)
        monitor.connect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.FileOpenCompleted)

    def updateModel(self, event=None, countChanged=False):
        """
        Updates the model so it contains the current components of the scene

        Arguments:
            event (pyfbsdk.FBEvent): Motion Builder event that triggered this method
            countChanged (boolean): the number of the components being monitored in the scene has changed

        """
        if not countChanged:
            return

        # Call to text is just to trigger error that the TreeView has been destroyed to avoid
        # repopulating the model if the view has been closed.
        self.text()

        self._model.reset()

    def closeEvent(self, event):
        """
        Overrides built-in method.
        Removes callbacks when UI is closed

        Arguments:
            event (QtGui.QEvent): the event being triggered by Qt

        """
        monitor = SceneMonitor.SceneMonitor()
        monitor.disconnect(self.componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Attach)
        monitor.disconnect(self.componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Destroy)
        monitor.disconnect(self.componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.FileOpenCompleted)