'''
This is the core logic for searching for objects within a scene based on a pattern of the object(s) names or long names.
'''

#PySide module
from PySide import QtGui

#RS modules
from RS import Globals


def getObjsByPattern(rootModel, pattern, foundObjs=None):
	'''
    Recursively checks all objects in the scene for what matches the passed in pattern, then returns a list of the matches.

    :param rootModel: <FBScene.RootModel>
    :param pattern: <str> The string pattern to search against
    :return: objs <list> All objects that meet the pattern criteria
    '''

	if foundObjs == None:
		foundObjs = []

	if rootModel and pattern:
		pattern = pattern.lower()
		for child in rootModel.Children:
			childName = child.Name.lower()
			childLongName = child.LongName.lower()
			if childName.find(pattern) != -1 or childLongName.find(pattern) != -1:
				foundObjs.append(child)

			if child:
				getObjsByPattern(child, pattern, foundObjs=foundObjs)

	return foundObjs


def userInput(parent):
	'''
    Message box prompting user to input a tag for the desired object(s) the
    user is searching for.

    :return: None
    '''

	if parent:
		(pattern, result) = QtGui.QInputDialog.getText(None, 'Select by Tag',
                                                              'Select All Objects by Name(s) Containing:\n- search is not case sesitive -')

		if result:
			getObjsByPattern(Globals.Scene.RootModel, pattern)
			print pattern

			del(Globals.Scene)

