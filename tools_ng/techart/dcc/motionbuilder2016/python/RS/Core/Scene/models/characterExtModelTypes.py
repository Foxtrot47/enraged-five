from PySide import QtCore

from RS.Core.Scene.models.modelItems import characterExtModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, textModelItem


class CharacterExtFromCharacterModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    def __init__(self, character=None, parent=None):
        self._checkable = False
        self._character = character
        super(CharacterExtFromCharacterModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ['Character Extension Name',]

    def setCheckable(self, val):
        """
        Set if the model is checkable or not
        
        args: val (bool)
        """
        self._checkable = val
        
    def checkable(self):
        """
        Gets if the model is checkable or not
        
        Returns:
            bool
        """
        return self._checkable

    def setCharacter(self, character):
        self._character = character
        self.reset()

    def _getAllComponentsFromCharacter(self):
        allComps = {}
        for ext in self._character.CharacterExtensions:
            for comp in ext.Components:
                allComps[comp.Name] = comp
        return allComps.values()

    def checkAllItems(self):
        """
        Check all the items in the model
        """
        for child in self.rootItem.children():
            idx = self.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Checked
            self.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self.dataChanged.emit(index0, index1)

    def uncheckAllItems(self):
        """
        Uncheck all the items in the model
        """
        for child in self.rootItem.children():
            idx = self.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Unchecked
            self.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self.dataChanged.emit(index0, index1)

    def invertCheckedState(self):
        """
        Invert the checked state of all items in the model
        """
        for child in self.rootItem.children():
            idx = self.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Checked
            if self.data(index0, QtCore.Qt.CheckStateRole) == QtCore.Qt.CheckState.Checked:
                dataValue = QtCore.Qt.CheckState.Unchecked
            self.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self.dataChanged.emit(index0, index1)

    def getCheckedItems(self):
        """
        Get all the checked items in the model as a list
        """
        checkedItems = []
        for child in self.rootItem.children():
            idx = self.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            if self.data(index0, QtCore.Qt.CheckStateRole) == QtCore.Qt.CheckState.Checked:
                checkedItems.append(child)
        return checkedItems

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        if self._character is None:
            parent.appendChild(
                        textModelItem.TextModelItem(["No Character Selected"], parent=parent)
                        )
            return
        comps = self._getAllComponentsFromCharacter()
        # iterate through list and create dataItem for treeview
        for comp in comps:
            # append tree view with item
            parent.appendChild(characterExtModelItem.ComponentTextModelItem(comp, parent=parent))

    def flags(self, index):
        '''
        flags added to determine column properties
        
        args: index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        
        if self._character is None:
            return QtCore.Qt.NoItemFlags
        
        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        
        if self._checkable:
            flags |= QtCore.Qt.ItemIsUserCheckable
        return flags
