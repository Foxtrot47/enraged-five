class TransformIDs(object):
    TRANSLATION_X = 1
    TRANSLATION_Y = 2
    TRANSLATION_Z = 3

    ROTATION_X = 4
    ROTATION_Y = 5
    ROTATION_Z = 6


class Transforms(object):
    TRANSLATION = "Translation"
    ROTATION = "Rotation"



class Axis(object):
    X = "x"
    Y = "y"
    Z = "z"


ModelDOFs = {
    TransformIDs.TRANSLATION_X:(Transforms.TRANSLATION, Axis.X),
    TransformIDs.TRANSLATION_Y:(Transforms.TRANSLATION, Axis.Y),
    TransformIDs.TRANSLATION_Z:(Transforms.TRANSLATION, Axis.Z),
    TransformIDs.ROTATION_X:(Transforms.ROTATION, Axis.X),
    TransformIDs.ROTATION_Y:(Transforms.ROTATION, Axis.Y),
    TransformIDs.ROTATION_Z:(Transforms.ROTATION, Axis.Z),
}
