from RS.Core.rsgMath.dataStructures import point3
from RS.Core.PinPoint.dataStructures import scene, model, animatedBodies


class Project(object):
    def __init__(self):
        super(Project, self).__init__()
        self._addationalData = {}
        self._problemSpec = None
        self._problemBodies = None
        self._problemVars = None
        self._calibration = None
        self._scaleDefinition = None
        self._rawData = None
        self._globlIds = None
        self._bodyDefinition = None
        self._limits = None
        self._searchParams = None
        self._gfxModel = None
        self._gfxScaling = None
        self._pattern = None
        self._motion = None
        self._realPoints = None 

    def problemBodies(self):
        return self._problemBodies

    def problemVars(self):
        return self._problemVars

    def setProblemVars(self, vars):
        self._problemVars = vars

    def setProblemBodies(self, bodies):
        self._problemBodies = bodies

    def calibartion(self):
        return self._calibration

    def setCalibration(self, calibration):
        self._calibration = calibration
        
    def motion(self):
        return self._motion

    def setMotion(self, motion):
        self._motion = motion
        
    def gfxModel(self):
        return self._gfxModel

    def setGfxModel(self, model):
        self._gfxModel = model
        
    def addationalData(self, key):
        return self._addationalData[key]
        
    def addationalDataKeys(self):
        return self._addationalData.keys()
    
    def setAddationalData(self, key, data):
        self._addationalData[key] = data

    def realPoints(self):
        return self._realPoints
    
    def setRealPoints(self, points):
        self._realPoints = points
        
    def generateSceneFromProblem(self):
        newScene = scene.Scene()
        # Build the models and make a dict (to variable) for it
        animBodies = []
        for body in self._problemBodies:
            animBodies.append(animatedBodies.AnimatedBodies(body, newScene))

        frameCount = self.motion().frameCount()
        newScene.setFrameCount(frameCount)

        for idx in xrange(frameCount):
            frameData = self.motion().getFrame(idx)
            newScene.setTimecode(frameData.timecode(), idx)
            for animBody in animBodies:
                trs = point3.Point3(*[frameData.varValue(var) if var != -1 else 0.0 for var in animBody.body().translationVars()])
                rot = point3.Point3(*[frameData.varValue(var) if var != -1 else 0.0 for var in animBody.body().rotationVars()])

                animBody.setTranslation(trs, idx)
                animBody.setRotation(rot, idx)
        return newScene
