from RS.Core.PinPoint.dataStructures import mapping
from RS.Core.GiantIO.fileHandlers import graphicalMappingDof
from RS.Core.GiantIO.utils import giantPaths
from RS.Core.PinPoint import consts


class MappingIO(object):

    @classmethod
    def getMappingFromFile(cls, filePath, giantDataDir=None, bodyIdOffset=0, systemVarIdOffset=0):
        dof = graphicalMappingDof.GraphicalMappingDof(filePath)
        mappings = []
        for incl in dof.getIncludedBodies():
            varOffset = incl.getSystemVariableIdOffset()
            bodyOffset = incl.getBodyIdOffset()
            mappings.extend(MappingIO.getMappingFromFile(
                                               giantPaths.toFileSystemPath(incl.getFileName(), giantDataDir),
                                               giantDataDir=giantDataDir,
                                               bodyIdOffset=bodyOffset,
                                               systemVarIdOffset=varOffset
                                               )
                            )
        for body in dof.getBodies():
            newMappings = mapping.Mapping(body.getBodyId() + bodyIdOffset)
            transformOrder = body.getTransformOrder()
            newMappings.setTransformOrder(transformOrder)

            for coord in [consts.TransformIDs.TRANSLATION_X, consts.TransformIDs.TRANSLATION_Y, consts.TransformIDs.TRANSLATION_Z,
                          consts.TransformIDs.ROTATION_X, consts.TransformIDs.ROTATION_Y, consts.TransformIDs.ROTATION_Z]:
                transformVar = body.getTransformVar(coord)
                if transformVar == 0:
                    continue
                elif transformVar == -1:
                    # Convert the GiantIO Linear Equation to PinPoints with var offsets
                    giantEq = body.getTransformEquation(coord)
                    eqs = []
                    for co in giantEq.coefficients():
                        eqs.append(mapping.Equation(co.coefficient, co.variable + systemVarIdOffset))

                    pinPointEq = mapping.LinearCombinationEquation(giantEq.coefficientConst(), eqs)
                    newMappings.setTransformEquation(coord, pinPointEq)
                else:
                    newMappings.setTransformVar(coord, transformVar + systemVarIdOffset)
            mappings.append(newMappings)

        return mappings


if __name__ == "__main__":
    # giantDataDir = r"x:\GC\projects\bob"
    # filePath = r"x:\GC\depot\projects\bob\dof\combined\SD_SlateWArm_Gizmo_9_12_9_Gizmo_9_12_9_GizmoPurple_SD_Ext_rsF_03.8_boots.dof"
    giantDataDir = r"x:\GC\projects\bob_dlc"
    # filePath = r"x:\GC\projects\bob_dlc\capture\takes\2020_0129_Session0228\000787_03_TO_WEAPONS_LONGARMS_ELEPHANT_STEP_BACK\000787_03_TO_WEAPONS_LONGARMS_ELEPHANT_STEP_BACK.dof"

    filePath = r"X:\GC\projects\bob_dlc\dof\rsM_03.2.dof"
    for dof in MappingIO.getMappingFromFile(filePath, giantDataDir=giantDataDir):
        print dof.variableCount(), dof.bodyID(), dof._transformVars
