from RS.Core.rsgMath.dataStructures import point3
from RS.Core.PinPoint.dataStructures import baseSceneObject


class AnimatedModel(baseSceneObject.BaseSceneObject):
    def __init__(self, model, scene):
        super(AnimatedModel, self).__init__(scene)
        self._model = model
        self._translation = {}
        self._rotation = {}

    def model(self):
        return self._model

    def translation(self, frameIdx=None, includeOffset=False):
        trans = self._translation.get(frameIdx or self._scene.currentFrame(), point3.Point3(0, 0, 0))
        if includeOffset is True:
            trans += self._model.translation()
        return trans

    def rotation(self, frameIdx=None, includeOffset=False):
        rot = self._rotation.get(frameIdx or self._scene.currentFrame(), point3.Point3(0, 0, 0))
        if includeOffset is True:
            rot += self._model.rotation()
        return rot

    def setTranslation(self, data, frameIdx=None):
        self._translation[frameIdx or self._scene.currentFrame()] = data

    def setRotation(self, data, frameIdx=None):
        self._rotation[frameIdx or self._scene.currentFrame()] = data

    def name(self):
        return self._model.name()

    def longName(self):
        return self._model.longName()

    def suffix(self):
        return self._model.suffix()

    def modelID(self):
        return self._model.modelID()

    def __str__(self):
        return "{} '{}'".format(self.__class__.__name__, self.longName())

    def __repr__(self):
        return "<{} at {}>".format(self.__str__(), hex(id(self)))
