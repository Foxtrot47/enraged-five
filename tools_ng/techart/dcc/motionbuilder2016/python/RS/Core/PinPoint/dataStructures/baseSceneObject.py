class BaseSceneObject(object):
    def __init__(self, scene):
        super(BaseSceneObject, self).__init__()
        self._scene = scene
        self._currentFrame = 0
        self._scene.addSceneObject(self)
        
    def name(self):
        return "<NO NAME>"