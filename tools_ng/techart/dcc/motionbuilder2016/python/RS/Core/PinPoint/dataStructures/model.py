from RS.Core.rsgMath.dataStructures import point3, axis


class Model(object):

    def __init__(self, name, modelID, suffix=None):
        super(Model, self).__init__()
        self._name = name
        self._suffix = suffix
        self._id = modelID
        self._parent = None
        self._children = []
        self._verts = []
        self._polygons = []
        self._translation = point3.Point3(0,0,0)
        self._rotation = point3.Point3(0,0,0)
        self._rotationOrder = [axis.Axis.X, axis.Axis.Y, axis.Axis.Z]
    
    def name(self):
        return self._name
    
    def setName(self, value):
        self._name = value
    
    def modelID(self):
        return self._id
    
    def setRotationOrder(self, item1, item2, item3):
        self._rotationOrder = [item1, item2, item3]
        
    def setRotation(self, value):
        self._rotation = value

    def rotationOrder(self):
        return self._rotationOrder
        
    def rotation(self):
        return self._rotation

    def setTranslation(self, value):
        self._translation = value
        
    def translation(self):
        return self._translation

    def vertexCount(self):
        return len(self._verts)

    def setVertices(self, value):
        self._verts = value
        
    def vertices(self):
        return self._verts
        
    def setPolygons(self, value):
        self._polygons = value
        
    def polygons(self):
        return self._polygons

    def polygonCount(self):
        return len(self._polygons)

    def parent(self):
        return self._parent

    def setParent(self, value):
        self._parent = value
        
    def addChild(self, value):
        self._children.append(value)
    
    def removeChild(self, value):
        self._children.remove(value)
    
    def children(self):
        return self._children 

    def suffix(self):
        return self._suffix

    def setSuffix(self, newSuffix):
        self._suffix = newSuffix

    def longName(self):
        return "".join([self._name, self._suffix or ""])

    def __str__(self):
        return "{} '{}'".format(self.__class__.__name__, self.longName())

    def __repr__(self):
        return "<{} at {}>".format(self.__str__(), hex(id(self)))

