import collections

import numpy

from RS.Core.PinPoint.dataStructures import scene, animatedModel
from RS.Core.PinPoint import consts


class Animation(object):
    def __init__(self):
        super(Animation, self).__init__()
        self._model = None
        self._mapping = None
        self._motion = None
        self._scaling = None

    def model(self):
        return self._model

    def setModel(self, model):
        self._model = model

    def mapping(self):
        return self._mapping

    def setMapping(self, mapping):
        self._mapping = mapping

    def motion(self):
        return self._motion

    def setMotion(self, motion):
        self._motion = motion

    def scaling(self):
        return self._scaling

    def setScaling(self, scaling):
        self._scaling = scaling

    def generateScene(self):
        """
        Generate the scene.

        Returns:
            scene.Scene
        """
        newScene = scene.Scene()

        # Build the models and make a dict (to variable) for it
        modelIdsToAnimModels = {}
        for model in self.model():
            animModel = animatedModel.AnimatedModel(model, newScene)
            modelIdsToAnimModels[model.modelID()] = animModel

        mappingDict = collections.OrderedDict()
        transVars = []
        for dof in self.mapping():
            model = modelIdsToAnimModels[dof.bodyID()]
            # for transform in dof.transformOrder():
            for transform in (1, 2, 3, 4, 5, 6):
                transVar = dof.transformVar(transform)
                transEq = None
                if transVar == 0:  # We must skip 0 indexed items; 0 means the axis isn't listed in the bmo.
                    continue
                elif transVar == -1:
                    transEq = dof.transformEquation(transform)
                mappingDict[(consts.ModelDOFs[transform], model)] = transVar, transEq
                transVars.append(transVar)

        frameCount = self.motion().frameCount()
        newScene.setFrameCount(frameCount)
        for idx in xrange(frameCount):
            frameData = self.motion().getFrame(idx)
            for modelInfo, mappingData in mappingDict.iteritems():
                # Check if its an Equation that needs to be solved
                mappingIdx, transEq = mappingData
                if transEq is not None:
                    # Solve the Equation
                    eqVars = {var:frameData.varValue(var - 1) for var in transEq.getVars()}
                    value = transEq.solve(eqVars)

                else:
                    #Otherwise just get the var needed
                    value = frameData.varValue(mappingIdx - 1)  # pinpoint uses 0 based indexes.
                transformType, axis = modelInfo[0]
                animModel = modelInfo[1]

                if transformType == consts.Transforms.TRANSLATION:
                    currentValue = animModel.translation(idx)
                elif transformType == consts.Transforms.ROTATION:
                    currentValue = animModel.rotation(idx)
                    value = numpy.rad2deg(value)  # Giant stores rotation as radians, PinPoint uses degrees.
                else:
                    continue

                if axis == consts.Axis.X:
                    currentValue.x = value
                elif axis == consts.Axis.Y:
                    currentValue.y = value
                elif axis == consts.Axis.Z:
                    currentValue.z = value

                if transformType == consts.Transforms.TRANSLATION:
                    animModel.setTranslation(currentValue, idx)
                elif transformType == consts.Transforms.ROTATION:
                    animModel.setRotation(currentValue, idx)

        return newScene
