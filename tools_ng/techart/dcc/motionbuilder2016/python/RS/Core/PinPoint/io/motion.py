import os

from RS.Core.PinPoint.dataStructures import motion
from RS.Core.GiantIO.fileHandlers import systemVarsBmo


class MotionIO(object):
    @classmethod
    def getMotionFromFile(cls, filePath):
        newMotion = motion.Motion()
        motionFile = systemVarsBmo.SystemVarsBmo(filePath)

        for frameData in motionFile.getFrames():
            # BMO indexs are are 1, we need them at 0
            frameValues = list(frameData.getRawVars())
            frameTc = frameData.getTC()
            frameNumber = frameData.getFrame() - 1
            newMotion.addNewFrame(frameNumber, frameValues, timecode=frameTc)
        return newMotion


if __name__ == "__main__":
    filePath = r"x:\GC\depot\projects\bob\capture\talent\2018_0227_Session2078\cmill_180227am.craig_miller.rom\cmill_180227am.craig_miller.rom.bmo"
    myMotion = MotionIO.getMotionFromFile(filePath)
    print myMotion.frameCount()
    import pdb; pdb.set_trace()
