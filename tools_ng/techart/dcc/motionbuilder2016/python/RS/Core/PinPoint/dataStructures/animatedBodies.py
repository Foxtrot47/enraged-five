import collections

from RS.Core.rsgMath.dataStructures import point3
from RS.Core.PinPoint.dataStructures import baseSceneObject


class AnimatedBodies(baseSceneObject.BaseSceneObject):
    def __init__(self, body, scene):
        super(AnimatedBodies, self).__init__(scene)
        self._body = body
        self._translation = collections.OrderedDict()
        self._rotation = collections.OrderedDict()

    def body(self):
        return self._body

    def translation(self, frameIdx=None):
        trans = self._translation.get(frameIdx or self._scene.currentFrame(), point3.Point3(0, 0, 0))
        return trans

    def rotation(self, frameIdx=None):
        rot = self._rotation.get(frameIdx or self._scene.currentFrame(), point3.Point3(0, 0, 0))
        return rot

    def allTranslation(self):
        return self._translation

    def allRotations(self):
        return self._rotation

    def setTranslation(self, data, frameIdx=None):
        self._translation[frameIdx or self._scene.currentFrame()] = data

    def setRotation(self, data, frameIdx=None):
        self._rotation[frameIdx or self._scene.currentFrame()] = data

    def name(self):
        return self._body.bodyName()
