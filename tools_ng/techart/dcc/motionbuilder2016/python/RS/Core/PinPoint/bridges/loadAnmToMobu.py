import sys
rdrDevPath = r"X:\techart\development\python"
if rdrDevPath not in sys.path:
    sys.path.append(rdrDevPath)

import numpy
import pyfbsdk as mobu

from RS.Core.PinPoint.io import animation
reload(animation)


def _buildModel(model):
    bodyName = model.name()
    if model.polygonCount() > 0:
        newMesh = mobu.FBMesh("{}_geo".format(bodyName))
        newMesh.GeometryBegin()
        newMesh.VertexInit(model.vertexCount(), False, False)
        for vert in model.vertices():
            newMesh.VertexAdd(vert.x, vert.y, vert.z)

        for face in model.polygons():
            newMesh.PolygonBegin()
            newMesh.PolygonVertexAdd(face[0])
            newMesh.PolygonVertexAdd(face[1])
            newMesh.PolygonVertexAdd(face[2])
            newMesh.PolygonEnd()

        # Compute mesh vertex normal with Counter Clock-Wise order
        newMesh.ComputeVertexNormals(True)
        newMesh.GeometryEnd()
    else:
        newMesh = mobu.FBMesh("{}_geo".format(bodyName))

    newModel = mobu.FBModelCube(bodyName)
    newModel.Geometry = newMesh
    newModel.Visible = True
    newModel.Show = True
    newModel.Translation = mobu.FBVector3d(model.translation().x, model.translation().y, model.translation().z)

    return newModel


def buildSkeletonNode(model):
    """
    Create a skeleton node for the provided .mdl body.

    Args:
        model ():

    Returns:
        pyfbsdk.FBModelSkeleton
    """
    name = model.name()
    newSkelNode = mobu.FBModelSkeleton(name)

    # Apply MoBu's Skeleton node settings to match mergefbx output.
    newSkelNode.Show = True
    newSkelNode.Color = mobu.FBColor(0.8, 0.8, 0.8)
    newSkelNode.VisibilityInheritance = False

    # Reduce size to make the smaller scale skeleton easier to view.
    newSkelNode.Size = 20

    # Set rot order
    newSkelNode.PropertyList.Find("Enable Rotation DOF").Data = True
    rotOrderMappings = {
        "XYZ": mobu.FBModelRotationOrder.kFBEulerXYZ,
        "XZY": mobu.FBModelRotationOrder.kFBEulerXZY,
        "YZX": mobu.FBModelRotationOrder.kFBEulerYZX,
        "YXZ": mobu.FBModelRotationOrder.kFBEulerYXZ,
        "ZXY": mobu.FBModelRotationOrder.kFBEulerZXY,
        "ZYX": mobu.FBModelRotationOrder.kFBEulerZYX,
    }
    newSkelNode.RotationOrder = rotOrderMappings["".join(model.rotationOrder())]

    return newSkelNode


def findMobuObject(name):
    """
    This function finds an object (not just a model) that matches the name exactly.

    Args:
        name (str): The name of the MotionBuilder object.

    Returns:
        mobu.FB?
    """
    matches = mobu.FBComponentList()
    includeNamespace = True
    searchModelsOnly = False
    mobu.FBFindObjectsByName(name, matches, includeNamespace, searchModelsOnly)

    searchTypes = (mobu.FBModelSkeleton, mobu.FBModelRoot, mobu.FBModelNull)

    mobuModels = [model for model in list(matches) if model.Name == name and isinstance(model, searchTypes)]
    if len(mobuModels) > 1:
        raise ValueError("Found more than one match: {}".format(name))
    elif len(mobuModels) == 0:
        raise ValueError("Found 0 matches for: {}!".format(name))
    return mobuModels[0]


def importAnm(filePath, giantDataDir, createSkeleton=True, applyMotion=True):
    """
    Import animation data from a Giant .anm.

    Downsamples from 60 fps to 30 fps.

    Args:
        filePath ():
        giantDataDir (str):

    Keyword Args:
        createSkeleton (bool):
    """
    scaleFactor = 2.54

    myAnm = animation.AnimationIO.getAnimationFromFile(filePath, giantDataDir=giantDataDir)
    scene = myAnm.generateScene()

    # Get or create the models and make a dict (to variable) for it
    animModelsToMobuModels = {}
    modelsToMobuModels = {}
    mocapOffsetNode = None
    skelRootNodeOffsets = {}

    for animModel in scene.sceneObjects():
        if createSkeleton:  # this ordering only works if the list of models is in order from the root downwards.
            mobuModel = buildSkeletonNode(animModel.model())
            animModelParent = animModel.model().parent()

            if animModelParent is not None:
                mobuModel.Parent = modelsToMobuModels[animModelParent]

            mobuModel.PropertyList.Find("Rotation Pivot").Data = mobu.FBVector3d(0, 0, 0)
            mobuModel.PropertyList.Find("Rotation Offset").Data = mobu.FBVector3d(0, 0, 0)
            mobuModel.PropertyList.Find("Scaling Pivot").Data = mobu.FBVector3d(0, 0, 0)

            mobuModel.Translation = mobu.FBVector3d(
                # TODO add the scale factor back later after debugging is complete.
                # animModel.model().translation().x * scaleFactor,
                # animModel.model().translation().y * scaleFactor,
                # animModel.model().translation().z * scaleFactor,
                animModel.model().translation().x,
                animModel.model().translation().y,
                animModel.model().translation().z,
            )
            mobuModel.Rotation = mobu.FBVector3d(
                 numpy.rad2deg(animModel.model().rotation().x),
                 numpy.rad2deg(animModel.model().rotation().y),
                 numpy.rad2deg(animModel.model().rotation().z),
            )

        else:
            animModelName = animModel.model().name()
            try:
                mobuModel = findMobuObject(animModelName)
                isAssetRootNode = False
                if animModelName.endswith(":SKEL_ROOT"):  # Characters
                    isAssetRootNode = True
                elif mobuModel.Parent and mobuModel.Parent.Name.lower().endswith("_offset"):  # Non-characters (props)
                    isAssetRootNode = True
                if isAssetRootNode:
                    tx, ty, tz = mobuModel.Translation
                    skelRootNodeOffsets[animModel] = (tx * -1, ty * -1, tz * -1)
            except:
                print "Unable to find {}, skipping it".format(animModelName)
                mobuModel = None

        modelsToMobuModels[animModel.model()] = mobuModel
        animModelsToMobuModels[animModel] = mobuModel

    if applyMotion:
        frameCount = scene.frameCount()
        # frameCount = 500

        # Set player controls
        startFrame = mobu.FBTime(0, 0, 0, 0)
        endFrame = mobu.FBTime(0, 0, 0, frameCount)
        playerControls = mobu.FBPlayerControl()
        playerControls.ZoomWindowStart = mobu.FBTime(startFrame)
        playerControls.ZoomWindowStop = mobu.FBTime(endFrame)
        playerControls.SetTransportFps(mobu.FBTimeMode.kFBTimeMode60Frames)

        # Apply motion to the mobu models
        # frameNumbers = range(frameCount)[0::2]  # Start on fr 0 to match anim2fbx; downsample from 60 to 30 fps.
        frameNumbers = xrange(frameCount)
        for anmFrNum in frameNumbers:
            # mbFrNum = anmFrNum","2  # Remap 60s fps anm frame to be the next 30 fps mobu frame. i.e. 0:0, 2:1, 4:2, etc.
            mbFrNum = anmFrNum
            curFrame = mobu.FBTime(0, 0, 0, mbFrNum)
            playerControls.Goto(curFrame)
            scene.setFrame(anmFrNum)
            for animModel in scene.sceneObjects():
                trans = animModel.translation(frameIdx=anmFrNum, includeOffset=True)
                # transX = trans.x * scaleFactor
                # transY = trans.y * scaleFactor
                # transZ = trans.z * scaleFactor
                transX = trans.x
                transY = trans.y
                transZ = trans.z

                # if createSkeleton is False and animModel in skelRootNodeOffsets.keys():  # Remove the baked in mocap offset.
                #     offsets = skelRootNodeOffsets[animModel]
                #     if animModel.model().name().lower().endswith(":SKEL_ROOT"):  # Character logic...
                #         #transX = transX + offsets[0]
                #         transY = transY + offsets[1]
                #         #transZ = transZ + offsets[2]
                #     else:  # assume its a prop
                #         transX = transX + offsets[0]
                #         transY = transY + offsets[1]
                #         transZ = transZ + offsets[2]
                mobuModel = animModelsToMobuModels[animModel]
                if mobuModel is not None:
                    mobuModel.Translation = mobu.FBVector3d(transX, transY, transZ)
                    mobuModel.Translation.Key()

                    rot = animModel.rotation(frameIdx=anmFrNum, includeOffset=True)
                    mobuModel.Rotation = mobu.FBVector3d(rot.x, rot.y, rot.z)
                    mobuModel.Rotation.Key()

                    if anmFrNum in (1, 2, 3, 4, 5) and animModel.model().name() == "torso_1":
                        print("fr {}: TRANS {}({}),{}({}),{}({}) ROT {},{},{}".format(
                            anmFrNum, transX, trans.x, transY, trans.y, transZ, trans.z, rot.x, rot.y, rot.z
                        ))

        playerControls.SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)

        # Workaround for getting the last take to commit
        curTake = mobu.FBSystem().CurrentTake
        curTake.LocalTimeSpan = mobu.FBTimeSpan(startFrame, endFrame)


if __name__ == '__builtin__':
    import os
    # Sync the test data with TakeBoss
    giantDataDir = os.path.join(r"x:\\", "GC", "projects", "bob_dlc")
    filePath = os.path.join(giantDataDir, "capture", "takes", "2020_0129_Session0228", "000787_03_TO_WEAPONS_LONGARMS_ELEPHANT_STEP_BACK.rsrefine.anm")
    filePath = os.path.join(giantDataDir, "capture", "takes", "2020_0129_Session0228", "000787_03_TO_WEAPONS_LONGARMS_ELEPHANT_STEP_BACK.rsrefine.anm")
    filePath = os.path.join(giantDataDir, "capture", "takes", "2020_0129_Session0228", "000787_03_TO_WEAPONS_LONGARMS_ELEPHANT_STEP_BACK", "charmapped", "000787_03_TO_WEAPONS_LONGARMS_ELEPHANT_STEP_BACK_IG_GenStoryMale_Alt_Skel_gs_StephenSl_MALEA.anm")
    filePath = os.path.join(giantDataDir, "capture", "takes", "2020_0129_Session0228", "000787_03_TO_WEAPONS_LONGARMS_ELEPHANT_STEP_BACK", "charmapped", "000787_03_TO_WEAPONS_LONGARMS_ELEPHANT_STEP_BACK_Gun_Replica_Repeater_01_.anm")
    filePath = os.path.join(giantDataDir, "capture", "takes", "2020_0219_Session0230", "000792_01_ED_MP_EMOTE_ACTION_PSYCHO", "charmapped", "000792_01_ED_MP_EMOTE_ACTION_PSYCHO_MP_F_Player_01_Skel_gs_JenHa.anm")
    filePath = os.path.join(giantDataDir, "capture", "takes", "2020_0219_Session0230", "000792_01_ED_MP_EMOTE_ACTION_PSYCHO", "charmapped", "000792_01_ED_MP_EMOTE_ACTION_PSYCHO_SlateClapper.anm")

    importAnm(str(filePath), giantDataDir=str(giantDataDir), createSkeleton=True, applyMotion=True)
