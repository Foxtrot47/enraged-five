class MotionFrame(object):
    """
    Class to represent a Motion Frame
    """
    def __init__(self, varValues, timecode=None):
        super(MotionFrame, self).__init__()
        self._varValues = varValues
        self._timecode = timecode

    def timecode(self):
        return self._timecode

    def setTimecode(self, value):
        self._timecode = value

    def varValues(self):
        return self._varValues

    def varValue(self, index):
        return self._varValues[index]

    def setVarValues(self, values):
        self._varValues = values

    def setVarValue(self, index, value):
        self._varValues[index] = value

    def varCount(self):
        return len(self._varValues)


class Motion(object):
    """
    Class to represent Motion
    """
    def __init__(self):
        super(Motion, self).__init__()
        self._frames = []

    def addNewFrame(self, frameNumber, varValues, timecode=None):
        self._frames.append(MotionFrame(varValues, timecode=timecode))

    def addFrame(self, frameNumber, frame):
        self._frames[frameNumber] = frame

    def getFrame(self, frameNumber):
        return self._frames[frameNumber]

    def frameCount(self):
        return len(self._frames)
