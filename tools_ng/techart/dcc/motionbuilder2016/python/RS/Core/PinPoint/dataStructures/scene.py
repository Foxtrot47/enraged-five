class Scene(object):
    def __init__(self):
        super(Scene, self).__init__()
        self._currentFrame = 0
        self._sceneObjects = []
        self._frameCount = 0
        self._timecodes = {}

    def frameCount(self):
        return self._frameCount

    def timecode(self, frame):
        return self._timecodes.get(frame)

    def setTimecode(self, timecode, frame):
        self._timecodes[frame] = timecode

    def setFrameCount(self, frameCount):
        self._frameCount = frameCount

    def addSceneObject(self, obj):
        self._sceneObjects.append(obj)

    def setFrame(self, idx):
        self._currentFrame = idx

    def currentFrame(self):
        return self._currentFrame

    def sceneObjects(self):
        return self._sceneObjects

    def getSceneObjectByName(self, name, includeSuffix=True):
        lowerName = name.lower()
        for item in self._sceneObjects:
            itemName = (item.longName() if includeSuffix is True else item.name()).lower()
            if itemName == lowerName:
                return item
        return None

    def getAllSceneObjectByName(self, name, includeSuffix=True, exact=True):
        returnList = []
        lowerName = name.lower()
        for item in self._sceneObjects:
            itemName = (item.longName() if includeSuffix is True else item.name()).lower()
            if (exact is True and itemName == lowerName) or \
                (exact is False and lowerName in itemName):
                returnList.append(item)
        return returnList

    def getAllSuffixes(self):
        sufs = set()
        for item in self._sceneObjects:
            sufs.add(item.suffix())
        return list(sufs)

    def getAllSceneObjectsInSuffix(self, suffix):
        returnList = []
        for item in self._sceneObjects:
            if item.suffix() == suffix:
                returnList.append(item)
        return returnList
