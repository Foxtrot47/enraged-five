# pinPoint


# *** WARNING ***

This module was ported ad hoc from rockstar.pinPoint in the TA depot specifically for use in the Virtual Production Toolbox.
Therefore, it is possible that some features will not work in this codebase!

This will be needed until pinPoint becomes a pip installable dependency for use in MotionBuilder.

This package was also ported over with RS.Core.GiantIO, so the same issues may apply there. Some parts of PinPoint may
rely on rsgMath, which has not been ported yet.

Additionally, some portions of the code differ from the TA depot versions in order to reduce import issues. We
removed some runners in the code because imports from the TA depot are not allowed here.

# *** END WARNING ***


## Description

A hub page is available for pinPoint: https://hub.gametools.dev/display/RSGTECHART/PinPoint

> A Cross Platform Python based model for reading Raw Mocap Files directly from Giant.

## Dependencies

To check the dependencies, run `packageScanner` pointed at this directory.

## Content Rules

Modules available for import are detailed in `rockstar\README.md` under the `Imposed Dependency Chart` section.
