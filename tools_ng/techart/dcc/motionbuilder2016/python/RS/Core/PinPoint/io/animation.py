import os

from RS.Core.GiantIO.utils import giantPaths
from RS.Core.PinPoint.dataStructures import animation
from RS.Core.PinPoint.io import model, motion, mapping
from RS.Core.GiantIO.fileHandlers import animationAnm


class AnimationKeys(object):
    MODEL = "MODEL"
    MAPPING = "MAPPING"
    MOTION = "MOTION"
    SCALING = "SCALING"


class AnimationIO(object):

    @classmethod
    def getAnimationFromFile(cls, filePath, giantDataDir=None):
        newAnimation = animation.Animation()
        anm = animationAnm.AnimationAnm(filePath)
        for stubb in anm.getStubbs():
            stubbName = stubb.stubbName
            value = stubb.value

            if stubbName == AnimationKeys.MODEL:
                filePath = giantPaths.toFileSystemPath(value, giantDataDir)
                print "mdl", filePath
                mdlData = model.ModelsIO.getModelsFromFile(filePath, giantDataDir=giantDataDir)
                newAnimation.setModel(mdlData)

            elif stubbName == AnimationKeys.MAPPING:
                filePath = giantPaths.toFileSystemPath(value, giantDataDir)
                print "dof", filePath
                dofData = mapping.MappingIO.getMappingFromFile(filePath, giantDataDir=giantDataDir)
                newAnimation.setMapping(dofData)

            elif stubbName == AnimationKeys.MOTION:
                filePath = giantPaths.toFileSystemPath(value, giantDataDir)
                print "bmo", filePath
                bmoData = motion.MotionIO.getMotionFromFile(filePath)
                newAnimation.setMotion(bmoData)

            # elif stubbName == AnimationKeys.SCALING:
            #     # import pdb; pdb.set_trace()
            #     # TODO read in the scaling data
            #     mdlData = model.ModelsIO.getModelsFromFile(giantPaths.toFileSystemPath(value, giantDataDir))
            #     newAnimation.setModel(mdlData)

        return newAnimation
