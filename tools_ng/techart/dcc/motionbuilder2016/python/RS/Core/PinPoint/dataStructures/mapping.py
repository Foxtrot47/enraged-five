class Mapping(object):
    def __init__(self, bodyId):
        super(Mapping, self).__init__()
        self._bodyId = bodyId
        self._transformOrder = []
        self._transformVars = {}
        self._transformEqs = {}

    def bodyID(self):
        return self._bodyId
    
    def setBodyID(self, value):
        self._bodyId = value

    def setTransformOrder(self, value):
        self._transformOrder = value

    def transformOrder(self):
        return self._transformOrder

    def variableCount(self):
        return len(self._transformOrder)

    def transformVar(self, coords):
        """
        Get the transform variable number for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to get the variable for

        return:
            int of the variable number, or -1 if none is set
        """
        return self._transformVars.get(coords, 0)

    def setTransformVar(self, coords, value):
        """
        set the transform variable number for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to set the variable for
            value(int): the variable number for the body coordinate
        """
        self._transformVars[coords] = value

    def setTransformEquation(self, coords, equation):
        """
        set the transform equation for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to set the variable for
            equation(LinearCombinationEquation): the equation for the body coordinate
        """
        self._transformEqs[coords] = equation
        self.setTransformVar(coords, -1)

    def transformEquation(self, coords):
        """
        get the transform equation for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to set the variable for

        returns:
            LinearCombinationEquation for the body coordinate
        """
        return self._transformEqs.get(coords, None)


class Equation(object):
    """
    Represents an equation
    """

    def __init__(self, coefficientValue, systemVariable):
        """
        args:
             coefficientValue (float): The  coefficient Value
             systemVariable (int): The system variable for the equation
        """
        super(Equation, self).__init__()
        self.coefficient = coefficientValue
        self.variable = systemVariable


class LinearCombinationEquation(object):
    """
    Represents and solves a limear combination equation
    """

    def __init__(self, coefficientConst, coefficients):
        """
        Constructor

        args:
            coefficientConst (float): The coefficient constant value
            coefficients (list of Equation): List of Equation that make up the equation
        """
        super(LinearCombinationEquation, self).__init__()
        self._coefficients = coefficients
        self._coefficientConst = coefficientConst

    def getVars(self):
        """
        Get all the system vars needed to solve this equation

        returns:
            list of ints
        """
        return [co.variable for co in self._coefficients]

    def coefficientConst(self):
        """
        Get the coefficient Constant

        returns:
            float coefficient Const
        """
        return self._coefficientConst

    def coefficients(self):
        """
        Get the coefficients

        returns:
            list of Equation
        """
        return self._coefficients

    def solve(self, varDict):
        """
        solve the equation

        args:
            varDict (dict of int:floats): dict where the keys are the vars needed and values are the float values

        returns:
            float value
        """
        return sum([co.coefficient * varDict[co.variable] for co in self._coefficients]) + self._coefficientConst
