class ProblemVariable(object):
    """
    Class to represent a variable
    """
    def __init__(self, index, description):
        super(ProblemVariable, self).__init__()
        """
        Constructor

        args:
            index (int): The variable index
            description (str): The description of the variable
        """
        self._index = index
        self._description = description

    def description(self):
        """
        Get the description of the variable

        return:
            String description of the variable
        """
        return self._description

    def setDescription(self, value):
        """
        set the file name to include

        args:
            value(str): the description of the variable
        """
        self._description = value

    def index(self):
        """
        Get the variable index

        return:
            int of the variable
        """
        return self._index

    def setIndex(self, value):
        """
        set the variable index

        args:
            value(int): the index of the variable
        """
        self._index = value


class Body(object):
    """
    Represent a Body object
    """
    def __init__(self, bodyName, parentName, suffix, graphicsType, bodyType):
        """
        Constructor

        args:
            bodyName(str): the name of the body
            parentName(str): the name of the parent body
            graphicsType(int): the graphics type
            bodyType(int): the type of body

        """
        super(Body, self).__init__()
        self._bodyName = bodyName
        self._parentName = parentName
        self._suffix = suffix
        self._graphicsType = graphicsType
        self._bodyType = bodyType

        self._translationVars = []
        self._rotationVars = []

    def setBodyName(self, value):
        """
        The name of the body

        args:
            value(str): The name of the body
        """
        self._bodyName = value

    def bodyName(self):
        """
        Get the name of the body

        return:
            str of the body's name
        """
        return self._bodyName

    def fullName(self):
        """
        Get the full name of the body
        """
        return "{}{}".format(self._bodyName, self._suffix or "")

    def bodyType(self):
        """
        Get the type of the body

        return:
            int of the body's type
        """
        return self._bodyType

    def setBodyType(self, value):
        """
        Set the type of the body

        args:
            value(int): The body's type
        """
        self._bodyType = value

    def graphicsType(self):
        """
        Get the graphics type of the body

        return:
            int of the body's graphics type
        """
        return self._graphicsType

    def setGraphicsType(self, value):
        """
        Set the graphics type of the body

        args:
            value(int): The body's graphics type
        """
        self._graphicsType = value

    def setParentName(self, value):
        """
        The name of the body's parent

        args:
            value(str): The name of the bodies parent
        """
        self._parentName = value

    def parentName(self):
        """
        Get the name of the parent body

        return:
            str of the body's parent name
        """
        return self._parentName

    def setTranslationVars(self, trsVars):
        self._translationVars = trsVars

    def setRotationVars(self, rotVars):
        self._rotationVars = rotVars

    def translationVars(self):
        return self._translationVars

    def rotationVars(self):
        return self._rotationVars


class RealPoint(object):
    """
    Represent a RealPoint object
    """
    def __init__(self, markerID, predType, weight, body):
        """
        Constructor

        args:
            markerID(int): The ID of the marker
            predType(str): The type of the marker
            weight(int): The Marker Weight
            body(Body): The Body its driving

        """
        super(RealPoint, self).__init__()
        self._markerID = markerID
        self._predType = predType
        self._weight = weight
        self._body = body

    def markerID(self):
        return self._markerID

    def predType(self):
        return self._predType

    def weight(self):
        return self._weight

    def body(self):
        return self._body
