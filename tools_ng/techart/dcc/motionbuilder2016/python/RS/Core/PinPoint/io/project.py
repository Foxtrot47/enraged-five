import os

from RS.Core.PinPoint.dataStructures import project
from RS.Core.PinPoint.io import motion, problem
from RS.Core.GiantIO.fileHandlers import project as giantPrj
from RS.Core.GiantIO.utils import giantPaths


class ProjectFileKeys(object):
    PROBLEM_SPEC = "PROBLEM_SPEC"
    PROBLEM_DEF = "PROBLEM_DEF"
    CALIBRATION = "CALIBRATION"
    SCALE_DEF = "SCALE_DEF"
    RAW_DATA = "RAW_DATA"
    GLOB_IDS = "GLOB_IDS"
    BODY_DEF = "BODY_DEF"
    LIMITS = "LIMITS"
    SEARCH_PARAMS = "SEARCH_PARAMS"
    GFX_MODEL = "GFX_MODEL"
    GFX_SCALING = "GFX_SCALING"
    PATTERN = "PATTERN"
    MOTION = "MOTION"


class ProjectIO(object):

    @classmethod
    def getProjectFromFile(cls, filePath, giantDataDir=None):
        newProject = project.Project()
        prj = giantPrj.GiantPrj(filePath)
        for key in prj.getKeys():
            if key == ProjectFileKeys.PROBLEM_DEF:
                filePath = giantPaths.toFileSystemPath(prj.getValue(ProjectFileKeys.PROBLEM_DEF), giantDataDir)
                bodies, _vars, realPoints = problem.ProblemIO.getProblemFromFile(filePath, giantDataDir)
                newProject.setProblemVars(_vars)
                newProject.setProblemBodies(bodies)
                newProject.setRealPoints(realPoints)

            elif key == ProjectFileKeys.MOTION:
                filePath = giantPaths.toFileSystemPath(prj.getValue(ProjectFileKeys.MOTION), giantDataDir)
                motionData = motion.MotionIO.getMotionFromFile(filePath)
                newProject.setMotion(motionData)

            else:
                newProject.setAddationalData(key, prj.getValue(key))
        return newProject

