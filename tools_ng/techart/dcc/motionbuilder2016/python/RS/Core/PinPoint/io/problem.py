import os

from RS.Core.PinPoint.dataStructures import problem
from RS.Core.GiantIO.fileHandlers import problemDefinition
from RS.Core.GiantIO.utils import giantPaths
from RS.Core.PinPoint import consts


class ProblemIO(object):

    @classmethod
    def _internalReadWithOffset(cls, filePath, giantDataDir=None, bodyIdOffset=0, bodySuffix=None, realMarkerOffset=0):
        bodies = []
        vars = []
        realPoints = []
        newProb = problemDefinition.ProblemDefinition(filePath)

        for var in newProb.vars():
            vars.append(problem.ProblemVariable(var.index() + bodyIdOffset - 1, var.description()))
        bodyLookup = {}
        for body in newProb.bodies():
            newBody = problem.Body(body.bodyName(), body.parentName(), bodySuffix, body.graphicsType(), body.bodyType())
            bodyLookup[body.bodyName()] = newBody
            trsVars = {}
            rotVars = {}

            for coord in [consts.TransformIDs.TRANSLATION_X, consts.TransformIDs.TRANSLATION_Y, consts.TransformIDs.TRANSLATION_Z,
                          consts.TransformIDs.ROTATION_X, consts.TransformIDs.ROTATION_Y, consts.TransformIDs.ROTATION_Z]:
                transformVar = body.getTransformVar(coord)
                if transformVar != -1:
                    # Giant indexs start at 1
                    transformVar = transformVar + bodyIdOffset - 1

                if coord == consts.TransformIDs.TRANSLATION_X:
                    trsVars[consts.TransformIDs.TRANSLATION_X] = transformVar
                elif coord == consts.TransformIDs.TRANSLATION_Y:
                    trsVars[consts.TransformIDs.TRANSLATION_Y] = transformVar
                elif coord == consts.TransformIDs.TRANSLATION_Z:
                    trsVars[consts.TransformIDs.TRANSLATION_Z] = transformVar

                elif coord == consts.TransformIDs.ROTATION_X:
                    rotVars[consts.TransformIDs.ROTATION_X] = transformVar
                elif coord == consts.TransformIDs.ROTATION_Y:
                    rotVars[consts.TransformIDs.ROTATION_Y] = transformVar
                elif coord == consts.TransformIDs.ROTATION_Z:
                    rotVars[consts.TransformIDs.ROTATION_Z] = transformVar

            newBody.setTranslationVars([trsVars[consts.TransformIDs.TRANSLATION_X],
                                        trsVars[consts.TransformIDs.TRANSLATION_Y],
                                        trsVars[consts.TransformIDs.TRANSLATION_Z]])
            newBody.setRotationVars([rotVars[consts.TransformIDs.ROTATION_X],
                                        rotVars[consts.TransformIDs.ROTATION_Y],
                                        rotVars[consts.TransformIDs.ROTATION_Z]])
            bodies.append(newBody)

        for markerID, markerType, markerWeight, markerBody in newProb.realPoints():
            realPoints.append(problem.RealPoint(markerID + realMarkerOffset, markerType, markerWeight, bodyLookup[markerBody]))

        # NOTE: Some pdf files do not have any included body stubbs, such as those in the probspec folder.
        # Even though this section is recursive, it will not continue to call itself when it gets to these pdf files.
        for includedProblem in newProb.includedBodies():
            newFilePath = giantPaths.toFileSystemPath(includedProblem.getFileName(), giantDataDir)
            _bodies, _vars, _realPoints = cls._internalReadWithOffset(
                newFilePath,
                giantDataDir,
                bodyIdOffset=len(vars),
                bodySuffix=includedProblem.getBodyNameSuffix(),
                realMarkerOffset=len(realPoints)
            )
            bodies.extend(_bodies)
            vars.extend(_vars)
            realPoints.extend(_realPoints)

        return bodies, vars, realPoints

    @classmethod
    def getProblemFromFile(cls, filePath, giantDataDir=None):
        return cls._internalReadWithOffset(filePath, giantDataDir=giantDataDir)


if __name__ == "__main__":
    giantDataDir = "x:\projects\development"
    filePath = r"x:\projects\development\probspec\characters\rsH_04a_fingers\rsH_04a_fingers.pdf"
    bodies, vars, realPoints = ProblemIO.getProblemFromFile(filePath, giantDataDir=giantDataDir)
    for point in realPoints:
        print "{} : {}".format(point.markerID(), point.body().bodyName())
    for var in vars:
        print "{}:{}".format(var.index(), var.description())
