import os
import collections

from RS.Core.rsgMath.dataStructures import point3
from RS.Core.PinPoint.dataStructures import model
from RS.Core.GiantIO.fileHandlers import modelMdl
from RS.Core.GiantIO.utils import giantPaths


class ModelsIO(object):
    @classmethod
    def getModelsFromFile(cls, filePath, giantDataDir=None, idOffset=0, bodySuffix=None):
        mdl = modelMdl.ModelMdl(filePath)
        models = []

        for incl in mdl.getIncludedModels():
            fileName = incl.getFileName()
            inclBodySuffix = incl.getBodyNameSuffix()
            if inclBodySuffix == "_a":
                inclBodySuffix = None

            filePath = giantPaths.toFileSystemPath(fileName, giantDataDir)
            models.extend(cls.getModelsFromFile(filePath, giantDataDir, idOffset=incl.getBodyIdOffset(), bodySuffix=inclBodySuffix))

        vertsPack = {}
        parentData = []
        bodies = collections.OrderedDict()  # maintaining order here eliminates need to zero out transforms later.

        for bodyStubb in mdl.getAllByStubbName("BODY"):
            name = bodyStubb.getByStubbName("NAME").value
            bodyId = bodyStubb.getByStubbName("ID").value + idOffset
            parentName = bodyStubb.getByStubbName("PARENT_NAME").value
            newModel = model.Model(name, bodyId, bodySuffix)
            bodies[name] = newModel

            if parentName == "GLOBAL":
                parentName = None
            else:
                parentData.append((newModel, parentName))

            coordSysStubb = bodyStubb.getByStubbName("COORDINATE_SYSTEM")
            xPos, yPos, zPos = [float(val) for val in coordSysStubb.getByStubbName("TRANSLATION").value.split(" ")]
            newModel.setTranslation(point3.Point3(xPos, yPos, zPos))

            value1, value2, value3, key1, key2, key3 = coordSysStubb.getByStubbName("ROTATION").value.split(" ")
            newModel.setRotation(point3.Point3(value1, value2, value3))
            newModel.setRotationOrder(key1, key2, key3)

        for vertStubb in mdl.getAllByStubbName("VERTICES"):
            name = vertStubb.getByStubbName("NAME").value
            coords = []
            for rawCoord in vertStubb.getByStubbName("COORDINATES").value:
                xPos, yPos, zPos = [float(val) for val in rawCoord.split(" ")]
                coords.append(point3.Point3(xPos, yPos, zPos))
            vertsPack[name] = coords

        for geoStubb in mdl.getAllByStubbName("GEOMETRY"):
            bodyName = geoStubb.getByStubbName("BODY").value
            body = bodies[bodyName]
            vertsStubb = geoStubb.getByStubbName("VERTICES")
            if vertsStubb is not None:
                verts = vertsPack[vertsStubb.value]
                body.setVertices(verts)
                for matStubb in geoStubb.getAllByStubbName("MATERIAL_GROUP"):
                    polyStubb = matStubb.getByStubbName("TEXTURED_POLYGONS")
                    if polyStubb is None:
                        continue
                    polyValues = []
                    for values in polyStubb.value:
                        polyValues.append([int(val.split("/")[0])-1 for val in values.split(" ")])
                    body.setPolygons(polyValues)

        for child, parentName in parentData:
            parent = bodies[parentName]
            child.setParent(parent)
            parent.addChild(child)

        return bodies.values() + models


if __name__ == "__main__":
    giantDataDir = r"x:\GC\projects\bob"
    filePath = os.path.join(
        giantDataDir,
        "model",
        "combined",
        "SD_SlateWArm_Gizmo_9_12_9_Gizmo_9_12_9_GizmoPurple_SD_Ext_rsF_03b.mdl",
    )
    # filePath = r"x:\GC\depot\projects\bob\model/SD_SlateWArm/SD_SlateWArm.mdl"
    # mdlDict = {mdl.modelID(): mdl.name() for mdl in ModelsIO.getModelsFromFile(filePath, giantDataDir=giantDataDir)}
    # import pprint; pprint.pprint(mdlDict)

    filePath = r"x:\GC\projects\bob_dlc\model\IG_GenStoryMale_Alt_Skel_gs.mdl"
    for mdl in ModelsIO.getModelsFromFile(filePath, giantDataDir=giantDataDir):
        print mdl.name(), mdl.rotationOrder()

