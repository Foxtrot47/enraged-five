"""
Description:
    Methods for connecting to Databases.

Authors:
    Mark Harrison-Ball <mark.harrison-ball@rockstargames.com>
    David Vega <david.vega@rockstargames.com>

Example:

import  RS.Core.DatabaseConnection as db
import RS.Utils.Logging.Universal as universal

gLog = universal.UniversalLog( "Motionbuilder_EST", False, True)

# Method #1
database = db.DatabaseConnection('{SQL Server}','10.1.16.147\RSGNYCSQL1','gradingStats', gLog)
database.ExecuteQuery("SELECT * FROM AvidEdits")

# All instances have the same data, so you can just call the class again
# to use the last server you connected to

# Method #2
db.DatabaseConnection('{SQL Server}','10.1.16.147\RSGNYCSQL1','gradingStats', gLog)
db.DatabaseConnection().ExecuteQuery("SELECT * FROM AvidEdits")

# Stored Procedures Method #1
     Parameter Values need to be in correct order defined by the procedure
database = db.DatabaseConnection('{SQL Server}','10.1.16.147\RSGNYCSQL1','CaptureRenders', None)
results = database.ExecuteQuery("exec GetCaptureStatus_RDR 2,0")

# Stored Procedures Method #2
    Parameter Values need to be in correct order defined by the procedure
database = db.DatabaseConnection('{SQL Server}','10.1.16.147\RSGNYCSQL1','CaptureRenders', None)
results = database.ExecuteQuery("{CALL GetCaptureStatus_RDR('2','0')}")

# Legacy
db.SetConnection('{SQL Server}','10.1.16.147\RSGNYCSQL1','gradingStats')
db.ReturnQuery("SELECT * FROM AvidEdits")
"""
import re
import sys
import platform
import traceback
import threading
from functools import wraps

LINUX_CONST = "Linux"
WINDOWS_CONST = "Windows"


# Try to import pydobc, if it fails then use the slower but pure python implementation
try:
    import pyodbc

except ImportError:
    import pypyodbc as pyodbc


WARNING = 'There is no connection to the database.'

# ----------- Exceptions ----------


class ConnectionError(Exception):
    """Raised if we can not connect to DB"""

# -----------Decorators ----------


def RaiseErrors(databases, state=True):
    """
    Decorator for temporarily raising errors on a database.
    This is an utility function for forcing errors to raise or be suppressed regardless of the current raise error
    state of the database connection.

    Arguments:
        databases (list): databases to raise errors on
        state (boolean): Should errors be raised or not
    """
    # Allow decorator to accept a single database connection
    if not isinstance(databases, (list, tuple)):
        databases = [databases]
    # Remove Nones
    databases = [database for database in databases if database is not None]

    def funcWrap(func):
        def wrap(*args, **kwargs):
            raiseErrors = []
            for database in databases:
                raiseErrors.append(database.RaiseErrors())
                database.SetRaiseErrors(state)
            try:
                return func(*args, **kwargs)
            except:
                exceptionType, exceptionValue, traceback = sys.exc_info()
                raise exceptionType, exceptionValue, traceback
            finally:
                [database.SetRaiseErrors(raiseError) for database, raiseError in zip(databases, raiseErrors)]
        return wrap
    return funcWrap


def ConnectionCheck(func, *parameters):
    """
    decorator for the database class, to checks if there is a connection to the database

    Arguments: 
        func (method): method that is being decorated, it is automatically passed into this method.
        warning (string): The warning message that you want to print out when the connection fails.
                          default warning is : "There is no connection to the database."
        *parameters: anything
            For catching extra arguments passed into the decorator
    
    Return: 
        results from func
    """

    # First wrapper is to grab arguments passed into the decorator
    @wraps(func)
    def wrapper(database_connection=None, *args, **kwargs):
        # Call function if there is a connection to the database

        if not database_connection.IsConnected:
            database_connection.Connect(force=True)

        if database_connection.Connection is not None:
            return func(database_connection, *args, **kwargs)
        else:
            return []

    return wrapper


def Enabled(isEnabled=True):
    """
    runs the decorated function if isEnabled is true

    Arguments:
        isEnabled (boolean): if this method is enabled

    Return:
        method or None
    """
    def another_wrapper(func, *parameters, **keyword_parameters):
        def wrapper(*args, **kwargs):
            if isEnabled:
                return func(*args, **kwargs)
        return wrapper
    return another_wrapper

# ----------- Classes ----------


class DatabaseConnection(object):
    """
    Methods for connecting to a database
    """

    driver = ''
    server = ''
    database = ''
    log = None
    timeout = 60

    warning = 'There is no connection to the database'

    __connection_format = ('Driver={driver};Server={server};Database={database};'
                           'Initial Catalog={database};Integrated Security={security};Connection Timeout={timeout}')
    __connection_format_Linux = ('Driver={driver};Servername={server};Database={database};'
                                 'Initial Catalog={database};Integrated Security={security};'
                                 'Connection Timeout={timeout}')
    __cache = {}

    def __new__(cls, driver, server, database, log=None, force=False, security=True, username=None,
                 password=None, timeout=timeout, raiseErrors=False, cache=True):
        """
        Overrides built-in method

        Caches the connection string so the same connection instance is returned if that connection string is
        passed in again.

        Arguments:
            driver (string): name of the driver
            server (string): name of the server
            database (string): name of the database
            log (ULog): Universal Log
            force (boolean): force the connection
            security (bool): use integrated security
            username (string): the username to use to connect to the database
            password (string): password for the username being used
            timeout (int): time to wait before a connection should time out
            cache (bool): should this connection be cached. Default is True

        Return:
            DatabaseConnection
        """

        connectionString = cls._ConnectionString(driver=driver, server=server, database=database, security=security,
                                                 username=username, timeout=timeout, password=password)
        instance = cls.__cache.get(connectionString, None)
        if instance is None or cache is False:
            instance = object.__new__(cls)
        return instance

    def __init__(self, driver="", server="", database="", log=None, force=False, security=True, username=None,
                 password=None, timeout=timeout, raiseErrors=False, cache=True):
        """
        Attempts to connect to the database based on the parameters provided

        Arguments:
            driver (string): name of the driver
            server (string): name of the server
            database (string): name of the database
            log (ULog): Universal Log
            force (boolean): force the connection
            security (bool): use integrated security
            username (string): the username to use to connect to the database
            password (string): password for the username being used
            timeout (int): time to wait before a connection should time out
            cache (bool): should this connection be cached. Default is True
        """

        # Account for the DB Instance
        if platform.system() == LINUX_CONST:
                parts = server.split("\\")
                if len(parts) == 2:
                        server = parts[1]

        connectionString = self._ConnectionString(driver=driver, server=server, database=database, security=security,
                                                  username=username, timeout=timeout, password=password)

        if self.__class__.__cache.get(connectionString, None) and cache:
            return

        # Set values if they are passed as keyword arguments
        self.__connection = None
        self._raiseErrors = raiseErrors
        self._warn = True
        self.driver = driver
        self.server = server
        self.database = database
        self.log = log
        self.security = security
        self.username = username
        self.password = password
        if timeout:
            self.timeout = timeout

        if cache:
            self.__class__.__cache.setdefault(connectionString, self)

    def __str__(self):
        return "{driver}:{server}:{database}".format(
            driver=self.driver,
            server=self.server,
            database=self.database)

    # ----------- Connection Methods ----------
    @classmethod
    def _ConnectionString(cls, driver, server, database, security, timeout, username, password):
        """
        String that returns the driver name, server and database the current DatabaseConnection instance is
        connected to.

        Return:
            String
        """

        connectionFormat = cls.__connection_format
        if platform.system() == LINUX_CONST:
            connectionFormat = cls.__connection_format_Linux
            username = "ROCKSTAR\\svcrsgnycvmx"
            password = "B0bthecapture!"

        connectionString = connectionFormat.format(
            driver=driver,
            server=server,
            database=database,
            security=security,
            timeout=timeout
        )
        if username and password:
            connectionString = "{};UID={};PWD={}".format(connectionString, username, password)
        return connectionString

    @property
    def ConnectionString(self):
        """
        String that returns the driver name, server and database the current DatabaseConnection instance is
        connected to.

        Return:
            String
        """
        return self._ConnectionString(driver=self.driver, server=self.server, database=self.database,
                                      security=self.security, timeout=self.timeout, username=self.username,
                                      password=self.password)

    @property
    def IsConnected(self):
        """ returns true or false if we are connected to the database """
        try:
            cursor = self.__connection.cursor()
            # Test ping, when a connection is idle for too long it may fail
            cursor.execute("SELECT 1")
            cursor.commit()
            cursor.close()
            # Read somewhere that explicitly deleting cursor should be done
            del cursor
            return True
        except Exception, e:
            return False

    @property
    def Connection(self):
        """ The pydobc connection instance """
        return self.__connection

    def Connect(self, force=True, timeout=4):
        """
        connects to the database

        Arguments: 
            force (boolean): tries to connect to the database even if there is a connection
        
        Returns: 
            cursor
        """
        if not self.IsConnected or force:

            self.__connection = None
            log_type = 'LogMessage'
            context = "Message"
            error = None

            # Attempt to connect to database
            try:
                self._errorClass = None
                self._errorString = ""

                def connect():
                    try:
                        self.__connection = pyodbc.connect(self.ConnectionString)
                        self.__connection.timeout = self.timeout
                    except Exception as e:
                        self._errorClass = e.__class__
                        self._errorString = traceback.format_exc()

                connectThread = threading.Thread(target=connect)
                connectThread.start()
                connectThread.join(timeout)

                if connectThread.isAlive():
                    raise ConnectionError("Connection Timed Out")

                elif self._errorClass:
                    raise self._errorClass(self._errorString)

                else:
                    log_message = "Connected to {0}:{1} successfully".format(self.server, self.database)

            except pyodbc.ProgrammingError as e:
                log_type, log_message, context = 'LogError', e.args[0], "Database Programming Error"
                error = e

            except pyodbc.Error as e:
                log_type, log_message, context = 'LogError', e.args[0], "Database Connection Error"
                self.__class__.timeout = 1
                error = e

            except ConnectionError as e:
                log_type, log_message, context = 'LogError', e.args[0], "Database Time Out Error"
                error = e

            except Exception as e:
                log_type, log_message, context = 'LogError', e.args[0], "Logic Error"
                self.__class__.timeout = 1
                error = e

            finally:
                # log results from connection if log is available
                if self.log:
                    getattr(self.log, log_type)(log_message, context)

                if self._raiseErrors and error:
                    raise ConnectionError(log_message)

        return self.__connection

    # ----------- SQL Methods ----------

    def ExecuteQuery(self, query, fetchall=True):
        """
        performs the sql query provided on the current database

        Arguments:
            query (string): an sql query to perform on the database
        
        Return: 
            string or None
        """
        return self.ExecuteQueries([query], fetchall=fetchall)

    # WARNING: From our custom cut of Motionbuilder 2016 onwards, threading was fixed and the 
    # TimeLimitedThread decorator was added to this module and applied to this method. DO NOT add 
    # it here, as this branch of code is run in Motionbuilder 2014 and will raise a RuntimeError.
    @ConnectionCheck
    def ExecuteQueries(self, queries, fetchall=True):
        """
        performs multiple sql queries within the same transaction.

        Arguments:
            query (string): an sql query to perform on the database

        Return:
            string or None
        """
        result = []
        error = None

        cursor = self.Cursor()

        for index, query in enumerate(queries):
            try:
                if platform.system() == LINUX_CONST:
                    query = re.compile(re.escape("'null'"), re.IGNORECASE).sub("NULL", query)
                cursor.execute(query)

            except pyodbc.Error, e:
                if self.log:
                    self.log.LogError(e.args[1], "Database Connection Error")
                elif self._warn:
                    print "pyodbc", traceback.format_exc()
                error = e
            except Exception, e:
                if self.log:
                    self.log.LogError(e.args[1], "Python Error")
                elif self._warn:
                    print "exception", traceback.format_exc()
                error = e

            if self._raiseErrors and error:
                cursor.close()
                raise ConnectionError(traceback.format_exc())

            # Added support for running stored procedure and also made case insensitive
            try:
                result.extend([row for row in cursor])
            except Exception, error:
                pass
            cursor.commit()

        return result

    @property
    @ConnectionCheck
    def Cursor(self):
        """ Returns a cursor object and starts a new transaction """
        return self.__connection.cursor
    
    @ConnectionCheck
    def Close(self):
        """ Closes the current connection """
        return self.__connection.close()
    
    @property
    @ConnectionCheck
    def Commit(self):
        """ Commits current transaction """
        return self.__connection.commit

    @ConnectionCheck
    def Rollback(self):
        """ NOT YET IMPLEMENTED
        Rolls back current transaction """
        pass

    def RaiseErrors(self):
        """
        Is this connection raising errors.
        By default this module silently passes errors than raising them

        Return:
            boolean
        """
        return self._raiseErrors

    def SetRaiseErrors(self, value):
        """
        Set connection to raise errors it encounters

        Arguments:
            value (boolean): set connection to raise errors (true) or not (false)
        """
        self._raiseErrors = value

    def Warn(self):
        """
        Is this connection printing out errors.
        By default this module prints out any exception it encounters

        Return:
            boolean
        """
        return self._warn

    def SetWarn(self, value):
        """
        Set connection to print errors it encounters

        Arguments:
            value (boolean): set connection to print errors (true) or not (false)
        """
        self._warn = value

    @property
    def Server(self):
        return self.server

    @property
    def Database(self):
        return self.database

    @property
    def Driver(self):
        return self.driver

    # ----------- Frequently Used SQL Queries ----------

    def ColumnNames(self, tableName):
        """
        Returns a list of the names of all the columns of the table

        Arguments:
            table_name (string): name of the table that you want to query

        Return:
            list
        """

        query = "SELECT * FROM information_schema.columns WHERE table_name='{0}'".format(tableName)
        return [each[3] for each in self.ExecuteQuery(query)]

    # ----------- Deprecated methods ----------

    def sql_command(self, sql_string):
        """
        DEPRECATED: use ExecuteQuery
        """
        return self.ExecuteQuery(query=sql_string)

    def connect(self, force=False):
        """ DEPRECATED: use Connect"""
        return self.Connect(force=force)

    @property
    def is_connected(self):
        """ DEPRECATED: use IsConnected"""
        return self.IsConnected

    @property
    def connection_string(self):
        """ DEPRECATED: Use ConnectionString """
        return self.ConnectionString

    @property
    def cursor(self):
        """ DEPRECATED: Use Cursor """
        return self.Cursor

    @property
    def commit(self):
        """ DEPRECATED: Use Commit"""
        return self.Commit
