from pyfbsdk import *
import RS.Globals
import RS.Utils.Scene
import time
from RS.Utils.Scene import Constraint
from RS.Tools.UI.Face import AmbientCSBGroupSetup

#Search and destroy all 3Lateral Alpha Shaders to prevent crashing.
def deleteAlphaShaders() :
    deleteAlphas = FBComponentList()
    FBFindObjectsByName("3LateralAlphaShader*", deleteAlphas, False, False)
    for each in deleteAlphas :
        if each :
            print ("Deleting: " + each.LongName)
            each.FBDelete()

# Returns List of all selected models
def getSelection():
    sl = FBModelList()
    FBGetSelectedModels(sl)
    return sl

# Deselects all selected objects
def deselectAll():
    #cl = FBModelList()
    #FBGetSelectedModels(cl)
    cl = FBSystem().Scene.Components
    for each in cl :
        each.Selected = False

# Function -- Zero Controls
def zeroSelectedControls() :
    sl = FBModelList()
    FBGetSelectedModels(sl)
    zeroControls(sl)

def zeroControls(pCtrls) :
    for iCtrls in pCtrls :
        iCtrls.Translation.Data = FBVector3d(0,0,0)
        iCtrls.Rotation.Data = FBVector3d(0,0,0)
        FBPlayerControl().Key()
        print "Zero'd out: " + iCtrls.Name


# Show/Hide Face Controls
def showHideFaceCtrls(lNamespace) :

    visibleFlag = FBFindModelByLabelName(lNamespace + "lipCorner_R_CTRL")
    visibleBool = visibleFlag.Visibility.Data

    lControls = FBModelList()
    for iCtrls in RS.Globals.gOnFaceControls :
        lControls.append( FBFindModelByLabelName( lNamespace + iCtrls ) )

    if visibleBool == True :
        print "Toggling OFF:"
        for item in lControls :
            item.Visibility = False
    else :
        print "Toggling ON:"
        for item in lControls :
            item.Visibility = True


def selectFacialCtrls(lNamespace) :

    deselectAll()

    # Get Namespace
    if lNamespace == "FACIAL_facialRoot" :
        lNS = ""
    else :
        lNS = ( (lNamespace.split(":",1))[0] + ":" )

    lCtrls_Face = FBModelList()
    for item in RS.Globals.gBrowCtrls :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Face.append( lName )
    for item in RS.Globals.gEyeCtrls :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Face.append( lName )
    for item in RS.Globals.gMouthCtrls :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Face.append( lName )

    for item in RS.Globals.gBlushCtrl :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Face.append( lName )

    for each in lCtrls_Face :
        each.Selected = True

# Make Group and add controls
def make3LGroups(lNamespace) :

    # Get Namespace
    if lNamespace == "FACIAL_facialRoot" :
        lNS = ""
    else :
        lNS = ( (lNamespace.split(":",1))[0] + ":" )

    # Gather GUI Elements

    lCtrls_Brows = FBModelList()
    lCtrls_Eyes = FBModelList()
    lCtrls_Mouth = FBModelList()
    for item in RS.Globals.gBrowCtrls :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Brows.append( lName )
    for item in RS.Globals.gEyeCtrls :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Eyes.append( lName )
    for item in RS.Globals.gMouthCtrls :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Mouth.append( lName )

    for item in RS.Globals.gBlushCtrl :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Mouth.append( lName )

    lFrames_Name = RS.Globals.gCAFaceAttrGui
    lFrames = FBModelList()
    for item in lFrames_Name :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lFrames.append( lName )


    # Accessing the scene components to view all the groups
    lScene = FBSystem().Scene
    lAllGroups = lScene.Groups

    # Gather Names
    name3L = lNS + "3Lateral"
    name3L_ctrls = lNS + "3L_Controls"
    name3L_frames = lNS + "3L_Frames"
    name3L_brows = lNS + "Brows"
    name3L_eyes = lNS + "Eyes"
    name3L_mouth = lNS + "Mouth"

    # Quick test
    quickTest = False
    for group in lAllGroups:
        if group.LongName == name3L :
            quickTest = True

    if quickTest :
        # Remove Groups
        for group in lAllGroups:
            if group.LongName == name3L_ctrls :
                group.FBDelete()
        for group in lAllGroups:
            if group.LongName == name3L_frames :
                group.FBDelete()
        for group in lAllGroups:
            if group.LongName == name3L_brows :
                group.FBDelete()
        for group in lAllGroups:
            if group.LongName == name3L_eyes :
                group.FBDelete()
        for group in lAllGroups:
            if group.LongName == name3L_mouth :
                group.FBDelete()

        RS.Globals.gScene.Evaluate()

        # Create Groups
        lGrp_ctrls = FBGroup (name3L_ctrls)
        lGrp_Frames = FBGroup (name3L_frames)
        lGrp_brows = FBGroup (name3L_brows)
        lGrp_eyes = FBGroup (name3L_eyes)
        lGrp_mouth = FBGroup (name3L_mouth)


        # Parent Groups
        for group in lAllGroups:
            if group.LongName == name3L_ctrls:
                # Remove existing group members
                while len(group.Items) != 0 :
                    group.Items.pop()

                group.Items.append(lGrp_brows)
                group.Items.append(lGrp_eyes)
                group.Items.append(lGrp_mouth)

            if group.LongName == name3L:

                # Remove existing group members
                while len(group.Items) != 0 :
                    group.Items.pop()

                group.Items.append(lGrp_ctrls)
                group.Items.append(lGrp_Frames)

        for each in lCtrls_Brows :
            lGrp_brows.ConnectSrc(each)
        for each in lCtrls_Eyes :
            lGrp_eyes.ConnectSrc(each)
        for each in lCtrls_Mouth :
            lGrp_mouth.ConnectSrc(each)
        for each in lFrames :
            lGrp_Frames.ConnectSrc(each)

        lGrp_ctrls.Show = True
        lGrp_ctrls.Pickable = True
        lGrp_ctrls.Transformable = True

        lGrp_Frames.Show = True
        lGrp_Frames.Pickable = False
        lGrp_Frames.Transformable = True
    else:
        FBMessageBox( "Error", "Invalid Character Selected\nOr this character doesn't have a 3Lateral Group", "OK" )

# Make Group and add controls
def makeAmbientGroups(lNamespace) :

    # Get Namespace
    if lNamespace != "" :
        lNS = lNamespace + ":"
    else :
        lNS = ""

    ambParent = FBFindModelByLabelName(lNS + "Ambient_UI")
    faceFXParent = FBFindModelByLabelName(lNS + "FaceFX")

    amb_Ctrls = []
    amb_Frames = []

    # Recursively gather Ambient Information
    def getAmbientInfo(currentObj) :
        if "CTRL" in currentObj.Name :
            amb_Ctrls.append(currentObj)
        elif "TEXT" in currentObj.Name :
            amb_Frames.append(currentObj)
        elif "RECT" in currentObj.Name :
            amb_Frames.append(currentObj)
        elif "Ambient_UI" in currentObj.Name :
            amb_Frames.append(currentObj)
        elif "FaceFX" in currentObj.Name :
            amb_Frames.append(currentObj)
        else :
            amb_Ctrls.append(currentObj)

        # Recurse
        for child in currentObj.Children:
            getAmbientInfo(child)

    if ambParent :
        getAmbientInfo(ambParent)
    if faceFXParent :
        getAmbientInfo(faceFXParent)

    if (len(amb_Ctrls) + len(amb_Frames)) != 0 :
        # Accessing the scene components to view all the groups
        lScene = FBSystem().Scene
        lAllGroups = lScene.Groups

        # Gather Names
        nameAmb = lNS + "Ambient_Facial"
        nameAmb_ctrls = lNS + "Amb_Controls"
        nameAmb_frames = lNS + "Amb_Frames"

        # Quick test
        quickTest = False
        for group in lAllGroups:
            if group.LongName == nameAmb :
                quickTest = True

        if quickTest:
            # Remove Groups
            for group in lAllGroups:
                if group.LongName == nameAmb_ctrls :
                    group.FBDelete()
            for group in lAllGroups:
                if group.LongName == nameAmb_frames :
                    group.FBDelete()

            RS.Globals.gScene.Evaluate()

            # Create Groups
            lGrp_ctrls = FBGroup (nameAmb_ctrls)
            lGrp_frames = FBGroup (nameAmb_frames)




            # Parent Groups
            for group in lAllGroups:
                if group.LongName == nameAmb:
                    # Remove existing group members
                    while len(group.Items) != 0 :
                        group.Items.pop()

                    group.Items.append(lGrp_ctrls)
                    group.Items.append(lGrp_frames)

            for each in amb_Ctrls :
                lGrp_ctrls.ConnectSrc(each)
            for each in amb_Frames :
                lGrp_frames.ConnectSrc(each)

            lGrp_ctrls.Show = True
            lGrp_ctrls.Pickable = True
            lGrp_ctrls.Transformable = True

            lGrp_frames.Show = True
            lGrp_frames.Pickable = False
            lGrp_frames.Transformable = True


        else:
            FBMessageBox( "Error", "Invalid Character Selected\nOr this character doesn't have a 3Lateral Group", "OK" )



    else :
        print ("No Ambient_UI detected for character: " + lNamespace)



    '''
    # Gather GUI Elements

    lCtrls_Brows = FBModelList()
    lCtrls_Eyes = FBModelList()
    lCtrls_Mouth = FBModelList()
    for item in RS.Globals.gBrowCtrls :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Brows.append( lName )
    for item in RS.Globals.gEyeCtrls :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Eyes.append( lName )
    for item in RS.Globals.gMouthCtrls :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lCtrls_Mouth.append( lName )
    lFrames_Name = RS.Globals.gCAFaceAttrGui
    lFrames = FBModelList()
    for item in lFrames_Name :
        lName = FBFindModelByLabelName( lNS + item )
        if lName :
            lFrames.append( lName )


    # Accessing the scene components to view all the groups
    lScene = FBSystem().Scene
    lAllGroups = lScene.Groups

    # Gather Names
    name3L = lNS + "3Lateral"
    name3L_ctrls = lNS + "3L_Controls"
    name3L_frames = lNS + "3L_Frames"
    name3L_brows = lNS + "Brows"
    name3L_eyes = lNS + "Eyes"
    name3L_mouth = lNS + "Mouth"

    # Quick test
    quickTest = False
    for group in lAllGroups:
        if group.LongName == name3L :
            quickTest = True

    if quickTest :
        # Remove Groups
        for group in lAllGroups:
            if group.LongName == name3L_ctrls :
                group.FBDelete()
        for group in lAllGroups:
            if group.LongName == name3L_frames :
                group.FBDelete()
        for group in lAllGroups:
            if group.LongName == name3L_brows :
                group.FBDelete()
        for group in lAllGroups:
            if group.LongName == name3L_eyes :
                group.FBDelete()
        for group in lAllGroups:
            if group.LongName == name3L_mouth :
                group.FBDelete()

        RS.Globals.gScene.Evaluate()

        # Create Groups
        lGrp_ctrls = FBGroup (name3L_ctrls)
        lGrp_Frames = FBGroup (name3L_frames)
        lGrp_brows = FBGroup (name3L_brows)
        lGrp_eyes = FBGroup (name3L_eyes)
        lGrp_mouth = FBGroup (name3L_mouth)


        # Parent Groups
        for group in lAllGroups:
            if group.LongName == name3L_ctrls:
                # Remove existing group members
                while len(group.Items) != 0 :
                    group.Items.pop()

                group.Items.append(lGrp_brows)
                group.Items.append(lGrp_eyes)
                group.Items.append(lGrp_mouth)

            if group.LongName == name3L:

                # Remove existing group members
                while len(group.Items) != 0 :
                    group.Items.pop()

                group.Items.append(lGrp_ctrls)
                group.Items.append(lGrp_Frames)

        for each in lCtrls_Brows :
            lGrp_brows.ConnectSrc(each)
        for each in lCtrls_Eyes :
            lGrp_eyes.ConnectSrc(each)
        for each in lCtrls_Mouth :
            lGrp_mouth.ConnectSrc(each)
        for each in lFrames :
            lGrp_Frames.ConnectSrc(each)

        lGrp_ctrls.Show = True
        lGrp_ctrls.Pickable = True
        lGrp_ctrls.Transformable = True

        lGrp_Frames.Show = True
        lGrp_Frames.Pickable = False
        lGrp_Frames.Transformable = True
    else:
        FBMessageBox( "Error", "Invalid Character Selected\nOr this character doesn't have a 3Lateral Group", "OK" )
    '''


# Function -- Delete ALL HeadCams and HeadLights
def deleteAllConstrainedStuff() :

    searchForCams = FBComponentList()
    FBFindObjectsByName("headCam", searchForCams, False, True)
    for each in searchForCams :
        each.FBDelete()

    searchForLights = FBComponentList()
    FBFindObjectsByName("headLight", searchForLights, False, True)
    for each in searchForLights :
        each.FBDelete()

# Create camera constrained to the head
def createConstrainedHeadCam(camName,camParent) :

    # If Camera exists, delete it.
    searchForCam = FBFindModelByLabelName(camName + ":headCam")
    if searchForCam != None :
        searchForCam.FBDelete()

    # Create camera camName
    lCamera1 = FBCamera(camName + ":headCam")
    lCamera1.Show = True

    # Move Camera into place
    lCamera1.Translation = FBVector3d(0,0.5,0)
    lCamera1.Rotation.Data = FBVector3d(0,0,-90)

    # Parent camName to camParent (Head)
    camParent.ConnectSrc( lCamera1 )


    # If Light exists, delete it
    searchForLight = FBFindModelByLabelName(camName + ":headLight")
    if searchForLight != None:
        searchForLight.FBDelete()

    # Create light lightName
    lLight1 = FBLight(camName + ":headLight")
    lLight1.LightType = FBLightType.kFBLightTypePoint
    lLight1.Show = True

    # Move Camera into place
    lLight1.Translation = FBVector3d(0,0.5,0)
    lLight1.Rotation.Data = FBVector3d(0,0,0)

    # Parent camName to camParent (Head)
    camParent.ConnectSrc( lLight1 )


# Function -- Find by name
def findHeadByNamespace(argName) :
    cl = FBComponentList()
    FBFindObjectsByName( "SKEL_Head", cl, False, True)
    if len(cl) == 0 :
        print "No proper facial rigs in the scene."
    else :
        tempCounter = 0
        for each in cl :
            if (argName + ":SKEL_Head") == each.LongName :
                return each
            else :
                tempCounter += 1

        if tempCounter == len(cl) :
            print "No proper facial rigs in the scene."


#List of controllers to look for
def getFaceParents(Namespace) :

    if Namespace == "" :
        colonTF = ""
    else :
        colonTF = ":"

    theList = FBComponentList()

    FBFindObjectsByName( Namespace + colonTF + "eyelidUpper_R_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidUpperInner_R_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidUpperOuter_R_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidLower_R_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidLowerInner_R_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidLowerOuter_R_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidLower_L_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidLowerInner_L_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidLowerOuter_L_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidUpper_L_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidUpperInner_L_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "eyelidUpperOuter_L_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "upperLip_R_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "lipCorner_L_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "upperLip_L_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "upperLip_C_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "lipCorner_R_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "jaw_C_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "lowerLip_R_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "lowerLip_C_OFF", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "lowerLip_L_OFF", theList, True, True )


    return theList


# Performs scale on the Translation Vector of all objects in given list. sV = scale amount.  multordiv, True = Multiply, False = Divide
def scaleThis (theList, sV, multordiv) :
    if multordiv :
        for each in theList :
            each.Translation.Data = (each.Translation.Data * sV)
    else :
        for each in theList :
            tempTranslationData = (each.Translation.Data / sV)
            each.Translation.Data = tempTranslationData


# Toggles through different scale settings for Michael's face GUI
def fixMichaelGUIScale() :

    # find the Current take
    lSystem = FBSystem()
    lPlayer = FBPlayerControl()

    # Set the endTime
    lStartSave = lSystem.CurrentTake.LocalTimeSpan.GetStart().GetFrame(True)
    lEndSave = lSystem.CurrentTake.LocalTimeSpan.GetStop().GetFrame(True)

    lSetTimeSpan = FBTimeSpan(FBTime(0,0,0,0),FBTime(0,0,0,lEndSave) )
    lSystem.CurrentTake.LocalTimeSpan = lSetTimeSpan

    FBKeyControl().AutoKey = True
    lPlayer.GotoStart()

    # Zero Controls
    zeroControls(getAllControls("Player_Zero"))



    theObjects = FBComponentList()
    FBFindObjectsByName("Player_Zero:upperLip_C_OFF", theObjects, True, True)

    for each in theObjects:
        if each.Translation.Data[1] > -0.041 :
            scaleThis(getFaceParents("Player_Zero"), 1.05, True)

        else :
            scaleThis(getFaceParents("Player_Zero"), 1.05, False)


    #lSetTimeSpan = FBTimeSpan(FBTime(0,0,0,lStartSave),FBTime(0,0,0,lEndSave) )
    #lSystem.CurrentTake.LocalTimeSpan = lSetTimeSpan



def createParentConstraint( pSrcModel, pDstModel, pName, pMoveToSource=True, pActive=True, pLock=False ):
    """
    creates a parent constraint between source and destination model.

    """

    CM = FBConstraintManager()

    # Clear existing instances of constraint
    lScene = FBSystem().Scene
    for each in lScene.Constraints :
        if each.LongName == pName :
            each.FBDelete()


    if ( pSrcModel == None ) or ( pDstModel == None ):
        return

    c = None
    lProp = None
    lRot = FBVector3d()
    lPos = FBVector3d()

    # 3 is index in the constraint manager list for parent constraint
    c = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
    if ( c ):

        c.Name = pName
        c.ReferenceAdd(0,pDstModel)
        c.ReferenceAdd(1,pSrcModel)


        # copy the transform from source to destination if specified
        if not pMoveToSource:
            pSrcModel.GetVector ( lRot, FBModelTransformationType.kModelRotation, True )
            pSrcModel.GetVector ( lPos, FBModelTransformationType.kModelTranslation, True )

        # set the active state
        c.Active = pActive

        if not pMoveToSource:
            pDstModel.SetVector ( lRot, FBModelTransformationType.kModelRotation, True )
            pDstModel.SetVector ( lPos, FBModelTransformationType.kModelTranslation, True )

        FBSystem().Scene.Evaluate()


        # set lock attribute
        lProp = c.PropertyList.Find( 'Lock' )
        if lProp:
            lProp.Data = pLock

    del ( CM, lProp, lRot, lPos )

    return c



def fixParentConstraint_Michael() :

    theNamespace = "Player_Zero"

    if theNamespace == "" :
        colonTF = ""
    else :
        colonTF = ":"

    theParent = theNamespace + colonTF + "FACIAL_facialRoot"
    theChild = theNamespace + colonTF + "facialRoot_C_OFF"
    theName = theNamespace + colonTF + "3LateralfacialRoot_Parent"

    theList = FBComponentList()
    FBFindObjectsByName(theParent, theList, True, True)
    FBFindObjectsByName(theChild, theList, True, True)

    if len(theList) == 2 :
        lSrc = theList[0]
        lDst = theList[1]
        createParentConstraint( lSrc, lDst, theName, pMoveToSource=False )
    else :
        None


def copyOnFaceCtrlData(sourceNS, targetNS) :

    gFacialOff = (
        "eyelidUpper_R_OFF",
        "eyelidUpperInner_R_OFF",
        "eyelidUpperOuter_R_OFF",
        "eyelidLower_R_OFF",
        "eyelidLowerInner_R_OFF",
        "eyelidLowerOuter_R_OFF",
        "eyelidLower_L_OFF",
        "eyelidLowerInner_L_OFF",
        "eyelidLowerOuter_L_OFF",
        "eyelidUpper_L_OFF",
        "eyelidUpperInner_L_OFF",
        "eyelidUpperOuter_L_OFF",
        "upperLip_R_OFF",
        "lipCorner_L_OFF",
        "upperLip_L_OFF",
        "upperLip_C_OFF",
        "lipCorner_R_OFF",
        "jaw_C_OFF",
        "lowerLip_R_OFF",
        "lowerLip_C_OFF",
        "lowerLip_L_OFF",
        "facialRoot_C_OFF",
        "faceControls_OFF"
    )

    targetCtrls = FBComponentList()
    for iCtrls in gFacialOff :
        FBFindObjectsByName(targetNS + iCtrls, targetCtrls, True, True)

    for iCtrls in targetCtrls :
        sourceCtrl = FBFindModelByLabelName(sourceNS + iCtrls.Name)
        iCtrls.Translation.Data = sourceCtrl.Translation.Data
        iCtrls.Rotation.Data = sourceCtrl.Rotation.Data
        iCtrls.Scaling.Data = sourceCtrl.Scaling.Data



#List of controllers to look for
def getAllControls(Namespace=None) :

    if Namespace == None :
        charNamespace = FBMessageBoxGetUserValue( "Select all controllers for...", "Please enter Character Namespace", "Player_Zero", FBPopupInputType.kFBPopupString, "Select", "Cancel" )

        # If "OK" is clicked
        if charNamespace[0] == 1 :
            Namespace = charNamespace[1]

        # If "Cancel" is clicked
        if charNamespace[0] == 2 :
            return []

    # Manage the colon
    if Namespace == "" :
        colonTF = ""
    else :
        colonTF = ":"


    theList = FBComponentList()

    FBFindObjectsByName( Namespace + colonTF + "*_CTRL", theList, True, True )
    FBFindObjectsByName( Namespace + colonTF + "CIRC_*", theList, True, True )


    return theList



def selectChildren (parentModel, selectParent = True):
    # Get model children
    children = parentModel.Children

    # Check if any children exist
    if (len (children) != 0):
        # Loop through the children
        for child in children:
            # Select the child
            child.Selected = True

            # Get any children
            selectChildren (child)

    # Select parent
    parentModel.Selected = selectParent

    # Return status
    return True

def exportAmbient(lNamespace) :

    lOptions = FBFbxOptions(False)
    lOptions.SaveSelectedModelsOnly = True

    ambUI = FBFindModelByLabelName(lNamespace + ":Ambient_UI")
    if ambUI:

        deselectAll()
        selectChildren(ambUI)

        fileName = FBApplication().FBXFileName
        fileName = fileName.replace(".FBX","_ANIM.FBX")
        fileName = fileName.replace(".fbx","_ANIM.FBX")
        if not FBApplication().FileSave(fileName, lOptions):
            print 'Problem saving the file: FileSave()'

    else:
        print "no object found."

def destroyFacialStuff(lNamespace):
    lObjList = (
        "FACIAL_facialRoot",
        "faceControls_OFF",
        "FacialAttrGUI"
    )

    lDelete = []

    #Delete Geo
    lGeoNode = FBFindModelByLabelName(lNamespace + ":" + lNamespace)
    if lGeoNode:

        for each in lGeoNode.Children:
            if each.Name.lower().startswith("head"):
                lDelete.append(each)
            if each.Name.lower().startswith("teef"):
                lDelete.append(each)



    for each in lObjList:
        lRigList = []
        lObj = FBFindModelByLabelName( lNamespace + ":" + each )
        if lObj:
            RS.Utils.Scene.GetChildren( lObj, lRigList )
        if len(lRigList) > 0:
            for item in lRigList:
                lDelete.append( item )



    for each in lDelete:
        each.FBDelete()

def fixFacialGroups( lNamespace ):

    lAllGroups = RS.Globals.gScene.Groups
    lGroupMain = []
    lGroupFace = []

    if lNamespace != None:
        for lGroup in lAllGroups:
            if lGroup.LongName.startswith( "{0}:".format( lNamespace ) ):
                lGroupMain.append( lGroup )

        for lGroup in lAllGroups:
            if lGroup.LongName.startswith( "{0}:".format( lNamespace ) ):
                lGroupFace.append( lGroup )
    else:
        for lGroup in lAllGroups:
            lGroupMain.append( lGroup )

        for lGroup in lAllGroups:
            lGroupFace.append( lGroup )

        # Use filename as main group node search
        fileName = FBApplication().FBXFileName
        fileNameSplit = fileName.split("\\")
        fileName = fileNameSplit[ len( fileNameSplit ) - 1 ]
        lNamespace = fileName.split(".")[0]

    facialGroupFix_Setup( lGroupMain, lGroupFace, lNamespace )

def facialGroupFix_Setup( lGroupMain, lGroupFace, lNamespace ):
       
    for lGroup in lGroupMain:
        if lGroup.Name.lower() == "facial":
            lGroup.Show = False
            lGroup.Pickable = False
            lGroup.Transformable = False
            lGroup.Show = True
            lGroup.Pickable = True
            lGroup.Transformable = True
        if lGroup.Name == lNamespace:
            lGroup.Pickable = False

    for lGroup in lGroupFace:
        if lGroup.Name.lower() == "maingui":
            lGroup.Pickable = False
            lGroup.Transformable = False
        if lGroup.Name.lower() == "eyegui":
            lGroup.Pickable = False
            lGroup.Transformable = False
        if "elements" in lGroup.Name.lower():
            lGroup.Pickable = False
            lGroup.Transformable = False
        if lGroup.Name.lower() == "facial - global eyes":
            lGroup.Show = False
        if lGroup.Name.lower() == "eyes":
            lGroup.Show = False
    AmbientCSBGroupSetup.Run(lNamespace) 