#CODE: ANIMATION_KEY_CLAMPER
#PROJECT: GTAV
#FUNCTIONALITY: CLAMPS THE KEYS FOR FACIAL CONTROLS
#               BEYOND +1 AND -1 FOR IN-SCENE 
#               CHARACTERS CHOSEN BY THE USER
#AUTHOR: SUPRATIM CHAUDHURY
#DATE: 14-DEC-2021

from PySide import QtGui
import pyfbsdk as mobu
import pythonidelib
import csv
import os
import RS.Core.Face.KeyClamper.ctrlList as cl
reload(cl)

lApp = mobu.FBApplication()
lSys = mobu.FBSystem()

#FIND NAMESPACES
def findNamespaces():
    stringAmbient = "*{0}*".format(cl.searchStringForAmbientNamespaces)
    compList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(stringAmbient, compList, True, True)
    nsListAmbient = findNS(compList)

    stringCSB = "*{0}*".format(cl.searchStringForCSBNamespaces)
    compList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(stringCSB, compList, True, True)
    nsListCSB = findNS(compList)
    
    nsList = nsListAmbient + nsListCSB
    return nsList

def findNS(compList):
    nsList = []
    for comp in compList:
        if (":" in comp.LongName):
            namespace = ((comp.LongName).split(":"))[0]
            if (namespace not in nsList):
                nsList.append(namespace)
    return nsList

#PRESERVING CURVE BY SETTING THE BOUNDING KEYS
def curvePreserve(cFCurve,startFrame,endFrame,clampValueMax,clampValueMin):
    if startFrame!=endFrame:  
        for lFrame in range(startFrame,endFrame+1): #[len(frameList)-1] will hold the last frameNumber
            if(lFrame < endFrame):
                lTimeCurrent = mobu.FBTime(0,0,0,lFrame)
                lValueCurrent = cFCurve.Evaluate(lTimeCurrent)
                lTimeNext = mobu.FBTime(0,0,0,lFrame+1)
                lValueNext = cFCurve.Evaluate(lTimeNext)
                
                #CLAMPING TO clampValueMax
                if lValueNext > clampValueMax: #Initial bounding key while clamping to clampValueMax
                    if lValueCurrent <= clampValueMax:
                        cFCurve.KeyAdd(mobu.FBTime(0, 0, 0, lFrame),lValueCurrent)
                if lValueNext <= clampValueMax: #Final bounding key while clamping to clampValueMax
                    if lValueCurrent > clampValueMax:
                        cFCurve.KeyAdd(mobu.FBTime(0, 0, 0, lFrame+1),lValueNext)
            
                #CLAMPING TO clampValueMin
                if lValueNext < clampValueMin: #Initial bounding key while clamping to clampValueMin
                    if lValueCurrent >= clampValueMin:
                        cFCurve.KeyAdd(mobu.FBTime(0, 0, 0, lFrame),lValueCurrent)
                if lValueNext >= clampValueMin: #Final bounding key while clamping to clampValueMin
                    if lValueCurrent < clampValueMin:
                        cFCurve.KeyAdd(mobu.FBTime(0, 0, 0, lFrame+1),lValueNext)
            else:
                calculateForSingleKey(cFCurve,lFrame,clampValueMax,clampValueMin)
    else:
        calculateForSingleKey(cFCurve,startFrame,clampValueMax,clampValueMin)
        
#CLAMPING KEYS ON A SINGLE-KEYED CURVE OR FOR THE LAST KEY
def calculateForSingleKey(cFCurve,frame,clampValueMax,clampValueMin):
    lTimeCurrent = mobu.FBTime(0,0,0,frame)
    lValueCurrent = cFCurve.Evaluate(lTimeCurrent)
    if lValueCurrent > clampValueMax:
            cFCurve.KeyAdd(mobu.FBTime(0, 0, 0, frame),clampValueMax)
    if lValueCurrent < clampValueMin:
            cFCurve.KeyAdd(mobu.FBTime(0, 0, 0, frame),clampValueMin)

#PRESERVING TANGENTS FOR THE BOUNDING KEYS TO AVERT ABNORMAL INTERPOLATION
def tangentPreserve(cFCurve,keyFrameNumberList):
    for key in cFCurve.Keys:
        if key.Time.GetFrame not in keyFrameNumberList:
            key.TangentMode = mobu.FBTangentMode.kFBTangentModeClampProgressive

#DELETING REDUNDANT KEYS
def deleteKeysAtFrame(cFCurve,frameNumber):
    timeStamp = mobu.FBTime(0,0,0,frameNumber)
    cFCurve.KeyDelete(timeStamp,timeStamp)           

#WORKING ON EACH CONTROLLER
def processController(nameSpace,ctrlName):
    cStr = "{0}:{1}".format(nameSpace,ctrlName)
    cName = mobu.FBFindModelByLabelName(cStr)
    
    clampValueMax = [cName.PropertyList.Find('TranslationMax')[0],cName.PropertyList.Find('TranslationMax')[1]]
    clampValueMin = [cName.PropertyList.Find('TranslationMin')[0],cName.PropertyList.Find('TranslationMin')[1]]
    
    cNode = cName.Translation.GetAnimationNode()
    if cNode:
        for axis in range(0,2):
            if (clampValueMax[axis] != clampValueMin[axis]):
                cFCurve = cNode.Nodes[axis].FCurve
                cKeyList = cFCurve.Keys
                keyFrameNumberList = []
                
                for key in cKeyList:
                    if(key.Time.GetFrame() not in keyFrameNumberList):
                        keyFrameNumberList.append(key.Time.GetFrame())
                
                if (keyFrameNumberList != []):
                    startFrame = keyFrameNumberList[0]
                    endFrame = keyFrameNumberList[len(keyFrameNumberList)-1]

                    curvePreserve(cFCurve,startFrame,endFrame,clampValueMax[axis],clampValueMin[axis])
                    tangentPreserve(cFCurve,keyFrameNumberList)
                    
                    cKeyListFinal = cFCurve.Keys
                    framesToDelete = []
                    
                    for key in cKeyListFinal:   
                        if (key.Value > clampValueMax[axis]):
                            framesToDelete.append(key.Time.GetFrame())
                        if (key.Value < clampValueMin[axis]):
                            framesToDelete.append(key.Time.GetFrame())

                    for frameNumber in framesToDelete:
                        deleteKeysAtFrame(cFCurve,frameNumber)
                            
#MAIN
def main(nsList):
    for ns in nsList:
            if('CSB' in ns):
                for ctrl in cl.ctrlListCSB:
                    processController(ns,ctrl)
            elif('CS' in ns):
                for ctrl in cl.ctrlListCS:
                    processController(ns,ctrl)
            else:
                for ctrl in cl.ctrlListAmbient:
                    processController(ns,ctrl)
                      
#UI BEGINS
class keyClamperDialog(QtGui.QWidget):
    TOOL_NAME = "Keys Clamping Tool"
    
    def __init__(self, parent=None):
        
        super(keyClamperDialog, self).__init__(parent=parent)
        
        self.setWindowTitle("Keys Clamping Tool")
        
        mainLayout = QtGui.QVBoxLayout()
 
        nsFinderBtn = QtGui.QPushButton("Execute On All Namespaces")
        nsFinderBtn.clicked.connect(self._namespaceFinderButton)

        choiceLabel1 = QtGui.QLabel("Select Namespace From Scene")
 
        self.nsComboBox = QtGui.QComboBox()
        self.nsComboBox.addItems(findNamespaces())

        nsComboExecuteBtn = QtGui.QPushButton("Execute On Selected Namespace")
        nsComboExecuteBtn.clicked.connect(self._comboBoxSelected)

        mainLayout.addWidget(choiceLabel1)
        mainLayout.addWidget(self.nsComboBox)
        mainLayout.addWidget(nsComboExecuteBtn)
        mainLayout.addWidget(nsFinderBtn)        
        self.setLayout(mainLayout)
        
    def _namespaceFinderButton(self):
        nsList = findNamespaces()
        main(nsList)

    def _comboBoxSelected(self):
        selectedNamespace = self.nsComboBox.currentText()
        nsList = [selectedNamespace]
        main(nsList)
        
newUI = keyClamperDialog()
newUI.show()

#END OF CODE