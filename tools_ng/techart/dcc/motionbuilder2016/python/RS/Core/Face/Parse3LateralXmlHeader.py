from pyfbsdk import *
import RS.Utils.Collections
from xml.dom import minidom
import os

import RS.Config

def TestMessageBox( pVal, pType ):

    lRes = FBMessageBoxGetUserValue( "Enter Character Name" , "Name: ", pVal, pType, "Ok" )

    if lRes[0]:
        return lRes[1]



def rs_FindLongestNameInArray(pArray):
    
    lInputLength = 0
    for iInput in pArray:
        if len(iInput) > lInputLength:
            lInputLength = len(iInput)
            
    return lInputLength

def rs_AddHeader(pVectorType, pInputOutput):

    gFileArray.append("\n" + 
                      "//###############################################################################################################\n" +
                      "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" + 
                      "//##\n" +
                      "//## Description: " + pVectorType + " " + pInputOutput + "\n" +
                      "//##\n" +
                      "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" + 
                      "//###############################################################################################################"
                      "\n")

def rs_ParseVectors(pArray, pVectorType, pInputOutput):
    
    global gFileArray
    global gIter
    
    lDuplicateArray = []
    lInputVector3d = []
    
    lAnimationNodeType = None
    lPartition = None
    lVectorType = None
    lUnderScore = ""
    
    if pInputOutput == "Input":
        lPartition = "_"
        lUnderScore = "_"
        lAnimationNodeType = "AnimationNodeInCreate"
        if pVectorType == "Position":
            lVectorType = "Pos"
        elif pVectorType == "Rotation":
            lVectorType = "Rot"
        elif pVectorType == "Scale":
            lVectorType = "Scale"
    else:
        lPartition = "-"
        lUnderScore = ""
        lAnimationNodeType = "AnimationNodeOutCreate"
        if pVectorType == "Position":
            lVectorType = "_Position"
        elif pVectorType == "Rotation":
            lVectorType = "_Rotation"
        elif pVectorType == "Scale":
            lVectorType = "_Scale"
        
    rs_AddHeader(pVectorType, pInputOutput)
    
    lInputLength = rs_FindLongestNameInArray(pArray)
    
    lPartition1 = ""
    
    for i in range(len(pArray)):
        
        if (lPartition + "X_") in pArray[i]:
            lPartition1 =  lPartition + "X_"
        
        elif (lPartition + "Y_") in pArray[i]:
            lPartition1 =  lPartition + "Y_"
        
        elif (lPartition + "Z_") in pArray[i]:
            lPartition1 =  lPartition + "Z_"
        
        if not pArray[i].rpartition(lPartition1)[0] in lDuplicateArray:
            lDuplicateArray.append(pArray[i].rpartition(lPartition1)[0])
        
    for iDuplicate in lDuplicateArray:  
        lTemporaryArray = []
        for iElement in pArray:
            if iElement.startswith(iDuplicate + "-"):
                
                lTemporaryArray.append(iElement.rpartition(lPartition)[2])
        
        gIter = gIter + 1
            
        if pInputOutput == "Input":
            gInputVector3d.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType)
        else:
            gOutputVector3d.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType)
        
        lCode = "    HFBAnimationNode    m" + iDuplicate + "_XYZ" + lUnderScore + lVectorType + ";"
        for i in range((lInputLength - len(iDuplicate))):
            lCode = lCode + " "
        lCode = lCode + "//!< " + pInputOutput + " node: " + iDuplicate + "'s XYZ " + lVectorType
        gFileArray.append(lCode)

def rs_ParseCustom(pArray, pVectorType, pInputOutput):
    
    global gIter
    
    rs_AddHeader(pVectorType, pInputOutput)
    lInputLength = rs_FindLongestNameInArray(pArray)
    
    for iElement in pArray:
        
        
        if pInputOutput == "Input":
            gInputDoubleArray.append(iElement)
        else:
            gOutputDoubleArray.append(iElement)
        
        lCode = "    HFBAnimationNode    m" + iElement + ";"
        for i in range(((lInputLength + 8) - len(iElement))):
            lCode = lCode + " "
        lCode = lCode + "//!< " + pInputOutput + " node: " + iElement 
        gFileArray.append(lCode)       



def rs_ParseExpressionXML(pXmlFileName):
    
    global gInputArray
    global gExpressionObjectArray
    global gExpressionArray
    
    gExpression = ""
    
    lXmlFile = minidom.parse(pXmlFileName)
    for iNode in lXmlFile.childNodes:
        gExpression = ""
        for iNode1 in iNode.childNodes:
            if iNode1.nodeType != iNode1.TEXT_NODE: 
                if iNode1.nodeName == "driven":
                    
                    lExpressionObj = (iNode1.attributes["expObj"].value + "-" + iNode1.attributes["expCont"].value)
                    
                    #if not lExpressionObj in gExpressionObjectArray:
                    gExpressionObjectArray.append(iNode1.attributes["expObj"].value.replace("'", "") + "-" + iNode1.attributes["expCont"].value)
                    gExpression = iNode1.attributes["expString"].value
                    #else:
                    #gExpression = gExpression + " + " + iNode1.attributes["expString"].value
                    gExpressionArray.append(gExpression)
                    
                    for iNode2 in iNode1.childNodes:
                        if iNode2.nodeType != iNode2.TEXT_NODE: 
                            if iNode2.nodeName == "driver":

                                gInputArray.append(iNode2.attributes["scalarObjects"].value + "_" + iNode2.attributes["scalarConts"].value)                                  
                     
    gInputArray = RS.Utils.Collections.RemoveDuplicatesFromList(gInputArray)   
    gInputArray = sorted(gInputArray)             
                         

def rs_SortInputsType(pArray, pInputOutput):
      
    lPosArray = []
    lRotArray = []
    lScaleArray = []
    lCustomArray = []
    
    for iInput in pArray:
        if "position" in iInput.lower():
            lPosArray.append(iInput)
        elif "rotation" in iInput.lower():
            lRotArray.append(iInput)
        elif "scale" in iInput.lower():
            lScaleArray.append(iInput)
        else:
            lCustomArray.append(iInput)
            
    rs_ParseVectors(lPosArray, "Position", pInputOutput)
    
    rs_ParseVectors(lRotArray, "Rotation", pInputOutput)
    
    rs_ParseVectors(lScaleArray, "Scale", pInputOutput)
    
    rs_ParseCustom(lCustomArray, "Custom", pInputOutput)


def Run():
    global gProjRoot
    global gFileName
    global gInputArray
    global gExpressionObjectArray 
    global gExpressionArray
    global gFileArray
    global gInputVector3d
    global gOutputVector3d
    global gInputDoubleArray
    global gOutputDoubleArray
    global gIter     
    global CharacterName  
    
    CharacterName = "IM_GTAV"
    
    gProjRoot = RS.Config.Project.Path.Root
    
    lFilePopup = FBFilePopup();
    lFilePopup.Filter = '*.xml'
    lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen
    lFilePopup.Path = gProjRoot + "\\art"
    
    gFileName = ""
    
    if lFilePopup.Execute():
        gFileName = lFilePopup.FullFilename
        
    gInputArray = []
    gExpressionObjectArray = []
    gExpressionArray = []
    gFileArray = []
    gInputVector3d = []
    gOutputVector3d = []
    gInputDoubleArray = []
    gOutputDoubleArray = []
    gIter = 0    


    print '#ifndef __ORBOX_' + CharacterName + '__BOX_H__'
    print '#define __ORBOX_' + CharacterName + '__BOX_H__'
    print ''
    print '//###############################################################################################################' 
    print '//##'
    print '//##     Rockstar North(R) Open Reality(R) rs_Rig_In_A_Box'
    print '//##     Written By: David Bailey'
    print '//##'
    print '//##     (C) 2011 Rockstar North, Inc. and/or its licensors'
    print '//##     All rights reserved.'
    print '//##'
    print '//##     Rockstar North, Inc., reserves the right to revise and improve its products as it sees fit.'
    print '//##'
    print '//##     This code has been generated by the rs_Rig_In_A_Box.py XML Parser, DO NOT edit this code'
    print '//##'
    print '//###############################################################################################################'
    print ''
    print '/** \\file orbox_' + CharacterName + '_box.h'
    print '*/'
    print ''
    print '//--- SDK include'
    print '#include <fbsdk/fbsdk.h>'
    print ''
    print '//--- Registration defines'
    print '#define ORBOX' + CharacterName + '__CLASSNAME    ORBox_' + CharacterName + ''
    print '#define ORBOX' + CharacterName + '__CLASSSTR    "ORBox_' + CharacterName + '"'
    print ''
    print '/**    Template for FBBox class.'
    print '*/'
    print 'class ORBox_' + CharacterName + ' : public FBBox'
    print '{'
    print '    //--- box declaration.'
    print '    FBBoxDeclare( ORBox_' + CharacterName + ', FBBox );'
    print ''
    print 'public:'
    print '    //! creation function.'
    print '    virtual bool FBCreate();'
    print ''
    print '    //! destruction function.'
    print '    virtual void FBDestroy();'
    print ''
    print '    //! Overloaded FBBox real-time evaluation function.'
    print '    virtual bool AnimationNodeNotify(HFBAnimationNode pAnimationNode,HFBEvaluateInfo pEvaluateInfo);'
    print ''
    print '    //! FBX Storage function'
    print '    virtual bool FbxStore( HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );'
    print ''
    print '    //! FBX Retrieval function'
    print '    virtual bool FbxRetrieve(HFBFbxObject pFbxObject, kFbxObjectStore pStoreWhat );'
    print ''
    print 'private:'
    print ''    


    rs_ParseExpressionXML(gFileName)
    rs_SortInputsType(gInputArray, "Input")
    rs_SortInputsType(gExpressionObjectArray, "Output")
    for iCode in gFileArray:
        print iCode 
                
    print ''                
    print'};'
    print''
    print'#endif /* __ORBOX_IM_GTAV_H__ */'        
