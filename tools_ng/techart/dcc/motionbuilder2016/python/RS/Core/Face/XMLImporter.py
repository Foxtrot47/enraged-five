from pyfbsdk import *

import RS.Config
import RS.Core.Face.Lib

import os
import shutil
import subprocess
import time
import xml.etree.ElementTree

class dxyz_obj( object ):
    #
    # Object to store all info needed to import 1 take of animation.
    #
    def __init__ ( self ):
        Name = None
        AnimFile = None
        AudioFile = None
        VideoFile = None
        ImgSeqFile = None
        ImgSeqFile_Source = None
        ImgSeqName = None
        
        
def prepareFaceCtrls():
    #
    # Clear all keys, and resets all facial controllers to their proper values
    #
    
    # CTRLs we don't want to zero out (ie. gui elements).
    zeroFilterList = [ "logic", "_loc", "gui" ]
    
    # CTRLs we want to set translation to (0,1,0)
    oneFilterList = [ "ctrl_rig_logicswitch" ]
    
    # Find all controllers
    cl = FBComponentList()
    FBFindObjectsByName( "CTRL_*", cl, False, True )
    
    for ctrl in cl:
        # Set these controls to 0. (CTRLs not in zeroFilterList)
        if len( [ x for x in zeroFilterList if x in ctrl.Name.lower() ] ) == 0:
            ctrl.Translation.Data = FBVector3d( 0,0,0 )
        
        # Set these controls to 1.
        if len( [ x for x in oneFilterList if x in ctrl.Name.lower() ] ) > 0:
            ctrl.Translation.Data = FBVector3d( 0,1,0 )
        
        # Set Animated & clear all keys
        ctrl.Translation.SetAnimated( True )
        ctrl.Rotation.SetAnimated( True )
        lTrans = ctrl.Translation.GetAnimationNode().Nodes
        for lTAxis in lTrans:
            lTAxis.FCurve.EditClear()
        lRot = ctrl.Rotation.GetAnimationNode().Nodes
        for lRAxis in lRot:
            lRAxis.FCurve.EditClear()
            

def setupVisibleMoviePlanes():
    #
    # Sets up movieplane visibility against take name, such that each take has 1 visible movieplane.
    #
    cl = FBComponentList()
    FBFindObjectsByName( "*_imgseq", cl, False, True )
    for take in FBSystem().Scene.Takes:
        FBSystem().CurrentTake = take
        for mp in cl:
            mp.Visibility.SetAnimated( True )
            lCurve = mp.Visibility.GetAnimationNode().FCurve
            if mp.Name.split("_imgseq")[0] ==  take.Name:
                lCurve.KeyAdd(FBTime(0), 1)
            else:
                lCurve.KeyAdd(FBTime(0), 0)
                

def popupFolderPath( lCaption ) :
    #
    # Browse for folder
    #
    lRes = None
    lFp = FBFolderPopup()
    lFp.Caption = lCaption
    lRes = lFp.Execute()
    if lRes:
        return lFp.Path
        
        
def popupFilePath( lCaption, lExtensions ) :
    #
    # Browse for file
    #
    lRes = None
    lFp = FBFilePopup()
    lFp.Caption = lCaption
    lFp.Filter = lExtensions
    lRes = lFp.Execute()
    if lRes:
        return os.path.join( lFp.Path, lFp.FileName )
        

def getCtrl( lName ):
    #
    # Input: Component name
    # Return: Component Object in Scene
    #
    cl = FBComponentList()
    FBFindObjectsByName( lName, cl, False, True )
    if len(cl) == 1:
        return cl[0]
    return None
        

def findFilePath( lName, lExt, lPath ):
    #
    # Input: Name, File Extension and Folderpath
    # Return: Filepath
    #
    for root, dirs, files in os.walk( lPath ):
        for name in files:
            if lName == name.split(".")[0] and lExt in name.lower():
                return os.path.join( root, name )
                
                
def findAnimFolder( lVideoPath ):
    #
    # Input: *.PerfProj Filepath
    # Return: RS_ANIM or RS_anim folder path
    #
    lRootFolder = os.path.join( lVideoPath, "RS_Dynmx", "AnimData" )
    if os.path.exists( lRootFolder ):
        return lRootFolder
    return None
    
    
def findAudioRoot( lPerfFile ):
    #
    # Input: *PerfProj Filepath
    # Return: RS_Audio folder path
    #
    lRoot = os.path.join( os.path.dirname( lPerfFile ), os.path.basename( lPerfFile ).split(".")[0] )
    lAudioRoot = os.path.join( lRoot, "RS_Audio" )
    if not os.path.exists( lAudioRoot ):
        os.makedirs( lAudioRoot )
    return lAudioRoot
    
    
def findJpgs( lName, lPerfFile ):
    #
    # Input: *.PerfProj filepath and name of file.
    # Return: the first jpg in Image Sequence
    #
    lJpgRoot = os.path.join( lPerfFile.split(".")[0], "SESSIONS", lName, "FRAMES", "Camera" )
    lJpgSeq = findFilePath( "00000000", ".jpg", lJpgRoot )
    if lJpgSeq:
        return lJpgSeq
    return None


def compressJpg( lSrc, lTrg ):
    lProc = r'{0}\bin\video\ffmpeg.exe -i'.format( RS.Config.Tool.Path.Root )
    subprocess.call( lProc + " " + lSrc + " -q:v 20 -vf scale=-1:720 " + lTrg, shell=True )
    

def copyJpgs( lSourceFolder, lTargetFolder ):
    #
    # Copy Files from lSourceFolder to lTargetFolder
    # Now copies with compression settings.  Saves more than 90% filesize. 6/9/2017 TPhan
    # Return filepath to first jpg in the sequence
    #
    
    if not os.path.exists( lTargetFolder ):
        os.makedirs( lTargetFolder )
    
    for lFile in os.listdir( lSourceFolder ):
        lSrc = os.path.join( lSourceFolder, lFile )
        lTrg = os.path.join( lTargetFolder, lFile )
        
        shutil.copyfile( lSrc, lTrg )
        #compressJpg( lSrc, lTrg )
        
    return os.path.join( lTargetFolder, "00000000.jpg" )
    

def setKey( lCtrl, lTrans, lAxis, lTime, lValue, lTan=[0,0,0,0] ):
    #
    # Sets key.
    # WIP - Still needs to set tangents.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #
    if lCtrl:
        if lTrans == 0:
            lCtrl.Translation.GetAnimationNode().Nodes[lAxis].FCurve.KeyAdd( FBTime( 0,0,0,lTime ), lValue )
        elif lTrans == 1:
            lCtrl.Rotation.GetAnimationNode().Nodes[lAxis].FCurve.KeyAdd( FBTime( 0,0,0,lTime ), lValue )
        elif lTrans == 2:
            lCtrl.Scaling.GetAnimationNode().Nodes[lAxis].FCurve.KeyAdd( FBTime( 0,0,0,lTime ), lValue )
    
    
def importAnimFile( lAnimFile, lOffset ):
    #
    # Input: Anim XML File, and offset
    # Function reads xml, and creates keys on matching controllers
    #
    
    # Filter: Only apply animation keys for these channels.
    # Options are: tx, ty, tz, rx, ry, rz, sx, sy, sz
    transformFilter = [ "tx", "ty" ]
    
    # Table to convert Transform type to an index
    transConvert = {"t":0, "r":1, "s":2, "x":0, "y":1, "z":2}
    
    # Initialize Frame ranges
    firstFrame = 0
    lastFrame = 1
    
    # Parse XML
    tree = xml.etree.ElementTree.parse( lAnimFile )
    root = tree.getroot()
    for entity in root:
        lCtrl = getCtrl( entity.attrib["name"] )
        if lCtrl:
            for curve in entity:
                lTemp = curve.attrib["name"].split(".")[ len( curve.attrib["name"].split(".") ) - 1 ]
                if lTemp in transformFilter:
                    for keyframe in curve:
                        if int( keyframe.attrib["index"] ) < firstFrame:
                            firstFrame = int( keyframe.attrib["index"] )
                        if int( keyframe.attrib["index"] ) > lastFrame:
                            lastFrame = int( keyframe.attrib["index"] )
                        
                        # Define parameters for Key
                        lTrans = int( transConvert[ lTemp[0] ] )
                        lAxis = int( transConvert[ lTemp[1] ] )
                        lTime = int( keyframe.attrib["index"] ) + lOffset
                        lValue = float( keyframe.attrib["value"] )
                        lTangents = [ float( keyframe.attrib["inTanX"] ), float( keyframe.attrib["inTanY"] ), float( keyframe.attrib["outTanX"] ), float( keyframe.attrib["outTanY"] ) ]
                        
                        # Set Key
                        setKey( lCtrl, lTrans, lAxis, lTime, lValue, lTangents )
    
    # Set Frame Range
    FBSystem().CurrentTake.LocalTimeSpan = FBTimeSpan( FBTime( 0,0,0,0 ), FBTime( 0,0,0, ( lastFrame + lOffset ) ) )
    

def extractAudio( lName, lVideoRoot, lAudioRoot ) :
    #
    # Input: Name, Source Folder, Target Folder
    # Return: Extracts audio and returns Audio FilePath
    #
    
    # Get video file
    inputFile = findFilePath( lName, ".mov", lVideoRoot )
    
    # Extract Audio File
    if not os.path.exists( lAudioRoot ):
        os.makedirs( lAudioRoot )
    if inputFile:
        if ".mov" in inputFile:
            outputFile = inputFile.replace( os.path.dirname( inputFile ), lAudioRoot )
            outputFile = outputFile.replace( ".mov", ".wav" )
            if not os.path.isfile( outputFile ):
                lProc = r'{0}\bin\video\ffmpeg.exe'.format( RS.Config.Tool.Path.Root )
                subprocess.Popen( [lProc, '-i', inputFile, outputFile, '-y'], shell=True )
    
            # Return audio filepath
            return outputFile
    return None
            
def importAudio( lAudioFile, lOffset ):
    #
    # Import Audio File.  Audio should only play in active take.
    #
    if os.path.isfile( lAudioFile ):
        lAudio = FBAudioClip( lAudioFile )
        lAudio.DstIn = FBTime( 0,0,0,lOffset )
        
def importAudio_All( lFbxFile, lOffset ):
    #
    # Import all wavs that match takes in the fbx.
    #def setupAudio( lOffset=0 ):
    
    # Remove all audio
    RS.Core.Face.XMLImporter.removeAllAudio()
    
    # Search for all audio files from 3 potential locations.
    lWav_List = []
    #lFilePath = FBApplication().FBXFileName
    lFilePath = lFbxFile
    lAssetRoot = os.path.join( lFilePath.split(".")[0], "" )
    lAssetRoot2 = os.path.join( os.path.dirname( lFilePath ), "fwr" )
    
    if os.path.isdir( lAssetRoot ):
        for lName in os.listdir( lAssetRoot ):
            lTakeRoot = os.path.join( lAssetRoot, lName )
            
            # If it finds the audio folder, grab all wav files from there
            if lName == "RS_Audio":
                if os.path.isdir( lTakeRoot ):
                    for lWavName in os.listdir( lTakeRoot ):
                        lWav = os.path.join( lTakeRoot, lWavName )
                        if os.path.isfile( lWav ):
                            lWav_List.append( lWav )
            # if it's a wav, add it.
            elif os.path.isfile( lTakeRoot ):
                if lTakeRoot.lower().endswith(".wav"):
                    lWav_List.append( lTakeRoot )
    
    if os.path.isdir( lAssetRoot2 ):
        for lName in os.listdir( lAssetRoot2 ):
            lTakeRoot = os.path.join( lAssetRoot2, lName )
            if os.path.isfile( lTakeRoot ):
                if lTakeRoot.lower().endswith( ".wav" ):
                    lWav_List.append( lTakeRoot )
    
    # Search for all TAKE names
    lTakeNames = []
    for lTake in FBSystem().Scene.Takes:
        lTakeNames.append( lTake.Name )
    
    # Import all audio if there's a matching TAKE name
    for lWav in lWav_List:
        if os.path.basename( lWav ).rsplit(".", 1 )[0] in lTakeNames:
            importAudio( lWav, lOffset )
    
    # Match audio with take name
    for take in FBSystem().Scene.Takes:
        for audioFile in FBSystem().Scene.AudioClips:
            if audioFile.Name.split(".")[0] == take.Name:
                audioFile.CurrentTake = take


def createMoviePlane( lName, lFile, lOffset ):
    #
    # Create Model, Texture, Video and Material
    #
    
    # Find SKEL_Head for movieplane parenting
    lSkelHead = None
    lSkelHeadList = FBComponentList()
    FBFindObjectsByName( "SKEL_Head", lSkelHeadList, False, True )
    if len( lSkelHeadList ) > 0:
        lSkelHead = lSkelHeadList[0]
        
    # Create Movie Plane
    lPlane = FBModelPlane( lName )
    lPlane.Show = True
    lPlane.Parent = lSkelHead
    lPlane.Translation.Data = FBVector3d( 0,.05,-.17 )
    lPlane.Rotation.Data = FBVector3d( 0,-90,0 )
    lPlane.Scaling.Data = FBVector3d( .0009,.002,.0015 )
    
    # Create Texture, Video and Material
    lTexture = FBTexture( lFile )
    lTexture.Name = lName
    lVideo = lTexture.Video
    lVideo.Name = lName
    lVideo.ImageSequence = True
    lVideo.PropertyList.Find( "OffSet" ).Data = FBTime( 0,0,0,lOffset )
    lMaterial = FBMaterial( lName )
    lMaterial.SetTexture( lTexture, FBMaterialTextureType.kFBMaterialTextureDiffuse )
    
    # Apply Material to Plane
    lPlane.Materials.append( lMaterial )
    

def removeAllAudio( ):
    #
    # Removes all audio from scene
    #
    lDelete = [ lAudio for lAudio in FBSystem().Scene.AudioClips ]
    for lDel in lDelete:
        lDel.FBDelete()
        

def removeAllJpgs ():
    #
    # Remove certain components with the name "imgseq" in the name.
    #
    deleteOrder = [
        "Model",
        "Material",
        "Texture",
        "Video"
        ]
    allComp = FBComponentList()
    FBFindObjectsByName( "*imgseq*", allComp, False, False )
    deleteMe = []
    for compType in deleteOrder:
        for comp in allComp:
            if comp.FullName.startswith( compType ):
                deleteMe.append( comp )
    for item in deleteMe:
        item.FBDelete()
    
    
def setupSingleFile( lAnimFile, lPerfFile, lVideoPath, lOffset ):
    #
    # This sets up the current take on the current character.
    #
    
    lTakeName = os.path.basename( lAnimFile ).split(".")[0]
    lAudioPath = findAudioRoot( lPerfFile )
    lAudioFile = extractAudio( lTakeName, lVideoPath, lAudioPath )
    lImgSeqFile = findJpgs( lTakeName, lPerfFile )
    
    #time.sleep( 1 ) # Gives time for file to write before importing audio below.
    
    prepareFaceCtrls()
    importAnimFile( lAnimFile, lOffset )
    FBSystem().CurrentTake.Name = lTakeName
    removeAllAudio()
    importAudio( lAudioFile, lOffset )
    removeAllJpgs()
    createMoviePlane( lTakeName + "_imgseq", lImgSeqFile, lOffset )
    print "Process Complete."
    

def setupFolderFiles( lSaveName, lPerfFile, lVideoPath, lTargetPath, lCharPath, lMaxTakes, lOffset ) :
    #
    # Grabs all anims and sets them all up in 1 or more fbx files.
    #
    
    # Ask if you want to overwrite existing files?
    lOverwrite = FBMessageBox( "Setup Files", "\nOverwrite existing files?\n\nYes: Process ALL Files\nNo: Process only the MISSING fbx files.", "Yes", "No", "Cancel" )
    if lOverwrite in [1,2]:
        
        # Setup Anim Object for each xml found in RS_Anim folder
        lAnimList = []
        lAnimFolder = findAnimFolder( lVideoPath )
        if os.path.exists( lAnimFolder ):
            for lName in os.listdir( lAnimFolder ):
                if os.path.isfile( os.path.join( lAnimFolder, lName ) ):
                    if ".xml" in lName:
                        lObj = dxyz_obj()
                        lObj.Name = lName.split(".")[0]
                        lObj.AnimFile = os.path.join( lAnimFolder, lName )
                        lObj.VideoFile = findFilePath( lName.split(".")[0], ".mov", lVideoPath )
                        lObj.ImgSeqName = lObj.Name + "_imgseq"
                        lObj.ImgSeqFile_Source = findJpgs( lObj.Name, lPerfFile )
                        
                        # Add valid Anim Object to Anim List for processing.
                        if lObj.ImgSeqFile_Source:
                            if lObj.VideoFile:
                                lAnimList.append( lObj )
                            else:
                                print "Missing Video File for {0}".format( lName )
                        else:
                            print "Missing SESSIONS Folder for {0}".format( lName )
        
        # Process each Anim Object
        lFBXNum = 0
        lFBXTakeNum = 1
        for lObj in lAnimList:
            lFBXName = "{0}_{1}".format( lSaveName, str( lFBXNum ).zfill( 2 ) )
            lFBXFile = os.path.join( lTargetPath, ( lFBXName + ".fbx" ) )
            lTargetFolder = os.path.join( lTargetPath, lFBXName )
            if not os.path.exists( lTargetFolder ):
                os.makedirs( lTargetFolder )
            
            # Do not overwrite existing fbx files.
            # Good for reprocessing if there was an issue. If you want new fbx file, delete it locally.
            if lOverwrite == 2:
                if os.path.isfile( lFBXFile ):
                    if lFBXTakeNum % lMaxTakes == 0 or lFBXTakeNum == len( lAnimList ):
                        lFBXTakeNum += 1
                        lFBXNum += 1
                        continue
                    else:
                        lFBXTakeNum += 1
                        continue
            
            # Extract Audio (Done early so that the file has time to write to disk)
            lObj.lAudioFile = extractAudio( lObj.Name, lVideoPath, lTargetFolder )
            #time.sleep(0.25)  # Script still runs too fast and occasionally skips audio files.  Stalling...
            
            # Copy Jpg Sequence from SESSIONS folder to Target Folder
            lObj.ImgSeqFile = copyJpgs( os.path.dirname( lObj.ImgSeqFile_Source ), os.path.join( lTargetFolder, lObj.Name ) )
            
            # Setup fresh scene
            if ( lFBXTakeNum - 1 ) % lMaxTakes == 0:
                RS.Core.Face.Lib.SetupFacialFile( lCharPath, syncTxt = True )
                
                delTakes = [ lTake for lTake in FBSystem().Scene.Takes ]
                for lTake in delTakes:
                    lTake.FBDelete()
            
            # Setup Takes
            if FBSystem().CurrentTake.Name == "Take 001":
                FBSystem().CurrentTake.Name = lObj.Name
            else:
                FBSystem().CurrentTake.CopyTake( lObj.Name )
            
            # Import
            prepareFaceCtrls()
            importAnimFile( lObj.AnimFile, lOffset )
            createMoviePlane( lObj.ImgSeqName, lObj.ImgSeqFile, lOffset )
            
            # Post-Prep
            if lFBXTakeNum % lMaxTakes == 0 or lFBXTakeNum == len( lAnimList ):
                setupVisibleMoviePlanes()
                RS.Core.Face.Lib.addDelay_All()
                RS.Core.Face.Lib.reduceAllEyeAnimation()
                #FBApplication().FileSave( lFBXFile )
                importAudio_All( lFBXFile, lOffset )
                FBApplication().FileSave( lFBXFile )
                lFBXNum += 1
                lFBXTakeNum += 1
                
            else:
                lFBXTakeNum += 1
        
        print "Process Complete."
    else:
        print "User Cancelled."
