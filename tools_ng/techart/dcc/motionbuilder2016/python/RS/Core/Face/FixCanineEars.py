import pyfbsdk as mobu

lSystem = mobu.FBSystem()

def getNamespace(lComp):
    lNamespace = None
    if ":" in lComp.LongName:
        lNamespace = lComp.LongName.split(":")[0]
    return lNamespace

def findAnimationNode(pParent, pName):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

def findHeadRoot(lNamespace):
    if lNamespace:
        headRoot = mobu.FBFindModelByLabelName("{0}:SKEL_Head".format(lNamespace))
    else:
        headRoot = mobu.FBFindModelByLabelName("SKEL_Head")
    return headRoot

def findEarRoot(lNamespace):
    if lNamespace:
        earRoot = mobu.FBFindModelByLabelName("{0}:CTRL_Ear_Root".format(lNamespace))
    else:
        earRoot = mobu.FBFindModelByLabelName("CTRL_Ear_Root")
    return earRoot
    
def findEarCtrls(lNamespace):
    earCtrls = mobu.FBComponentList()
    if lNamespace:
        mobu.FBFindObjectsByName("{0}:CTRL_*_Ear".format(lNamespace), earCtrls, True, True)
    else:
        mobu.FBFindObjectsByName("CTRL_*_Ear", earCtrls, True, True)
    return earCtrls

def findEarBones(lNamespace):
    earBones = mobu.FBComponentList()
    if lNamespace:
        mobu.FBFindObjectsByName("{0}:FB_*_Ear_000".format(lNamespace), earBones, True, True)
    else:
        mobu.FBFindObjectsByName("FB_*_Ear_000", earBones, True, True)
    return earBones

def fixCanineConstraint(lConstraint, boxName):
    """
    Identifies and fixes the broken relation constraint for canine ears.
    """
    vectorBoxOut = None
    boneBoxIn = None
    
    # Cycle through matching boxes in relation constraint
    for each in (x for x in lConstraint.Boxes if boxName in x.Name):
        
        # If the rotation node has 1 out connection, follow it to get the math box "Result" node
        if each.GetInConnector(1).GetSrcCount() == 1:
            outCount = each.AnimationNodeOutGet().Nodes[1].GetDstCount()
            if outCount > 0:
                vectorBox = each.AnimationNodeOutGet().Nodes[1].GetDst(0).GetOwner()
                vectorBoxOut = findAnimationNode(vectorBox.AnimationNodeOutGet(), 'Result')
                
        # If the target box has no connections in rotation, get the node.
        if each.GetInConnector(1).GetSrcCount() == 0:
            boneBoxIn = findAnimationNode(each.AnimationNodeInGet(), 'Lcl Rotation')
    
    # Do the thing
    if vectorBoxOut and boneBoxIn:
        print "Fixing Relation Constraint for: {0}!".format(boxName)
        mobu.FBConnect(vectorBoxOut, boneBoxIn)
        
        
def resetEarControllers(lConstraint):
    """
    Fixes the transforms/offsets for the ear controls.
    """
    print "Resetting Ear Controllers"
    lNamespace = getNamespace(lConstraint)
    headRoot = findHeadRoot(lNamespace)
    earControls = findEarCtrls(lNamespace)
    earBones = findEarBones(lNamespace)
    
    # zeroes out most of the controls.  Some values are hard-coded, but generally works.
    for ctrl in earControls:
        ctrl.Translation.Data = mobu.FBVector3d(0,0,0)
        ctrl.Rotation.Data = mobu.FBVector3d(0,0,0)
        ctrl.PropertyList.Find("GeometricTranslation").Data = mobu.FBVector3d(0,0,0)
        ctrl.PropertyList.Find("GeometricRotation").Data = mobu.FBVector3d(0,-90,0)
        ctrl.PropertyList.Find("GeometricScaling").Data = mobu.FBVector3d(10,10,1)
        ctrl.PropertyList.Find("RotationActive").Data = True
        ctrl.PropertyList.Find("TranslationActive").Data = True
        ctrl.PropertyList.Find("TranslationMinX").Data = True
        ctrl.PropertyList.Find("TranslationMinY").Data = True
        ctrl.PropertyList.Find("TranslationMinZ").Data = True
        ctrl.PropertyList.Find("TranslationMaxX").Data = True
        ctrl.PropertyList.Find("TranslationMaxY").Data = True
        ctrl.PropertyList.Find("TranslationMaxZ").Data = True
        ctrl.Show = True
        
        # There seems to be a couple different setups, this ensures we account for both.
        if len(earBones) == 2:
            if "_L_" in earBones[0].Name:
                leftEar = earBones[0]
                rightEar = earBones[1]
            else:
                leftEar = earBones[1]
                rightEar = earBones[0]
            if "_R_" in ctrl.Parent.Name or "_L_" in ctrl.Parent.Name:
                ctrl.PropertyList.Find("PreRotation").Data = mobu.FBVector3d(0,0,0)
                ctrl.PropertyList.Find("TranslationMin").Data = mobu.FBVector3d(0,0,0)
                ctrl.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(0,0,0)
            elif "_L_" in ctrl.Name:
                ctrl.PropertyList.Find("PreRotation").Data = mobu.FBVector3d(0,-105,0)
                ctrl.PropertyList.Find("TranslationMin").Data = leftEar.Translation.Data
                ctrl.PropertyList.Find("TranslationMax").Data = leftEar.Translation.Data
            elif "_R_" in ctrl.Name:
                ctrl.PropertyList.Find("PreRotation").Data = mobu.FBVector3d(0,-105,0)
                ctrl.PropertyList.Find("TranslationMin").Data = rightEar.Translation.Data
                ctrl.PropertyList.Find("TranslationMax").Data = rightEar.Translation.Data
        
def addParentConstraint(lConstraint):
    """
    Creates parent constraint for the controls to follow the head
    """
    print "Resetting Parent Constraint"
    lNamespace = getNamespace(lConstraint)
    if lNamespace:
        constrName = "{0}:faceCtrlRoot_Parent/Child".format(lNamespace)
    else:
        constrName = "faceCtrlRoot_Parent/Child"
    earRoot = findEarRoot(lNamespace)
    headRoot = findHeadRoot(lNamespace)
    
    if earRoot and headRoot:
        # Remove existing parent constraints.
        for constraint in lSystem.Scene.Constraints:
            if constraint.LongName == constrName:
                constraint.FBDelete()
        
        # Create parent constraint
        parentConstraint = mobu.FBConstraintManager().TypeCreateConstraint('Parent/Child')
        parentConstraint.LongName = constrName
        parentConstraint.ReferenceAdd(0,earRoot)
        parentConstraint.ReferenceAdd(1,headRoot)
        parentConstraint.Active = True
        
        # Tuck into the same parent folder as the relation constraint
        for i in range(lConstraint.GetDstCount()):
            if lConstraint.GetDst(i).ClassName() == "FBFolder":
                parentFolder = lConstraint.GetDst(i)
                parentFolder.ConnectSrc(parentConstraint)
    
def Run():
    for constraint in lSystem.Scene.Constraints:
        if constraint.Name == "Ambient_canine 3Lateral":
            fixCanineConstraint(constraint, "FB_R_Ear")
            fixCanineConstraint(constraint, "FB_L_Ear")
            resetEarControllers(constraint)
            addParentConstraint(constraint)
            