#CODE: FACE_FINDER
#PROJECT: GTAV
#FUNCTIONALITY: PROVIDES TABULAR INFO OF FACE FILES AND THE
#               CORRESPONDING CHARACTERS ON WHICH THOSE ARE IMPLEMENTED
#AUTHOR: SUPRATIM CHAUDHURY
#DATE: 01-FEB-2022
#NOTE: BASED ON THE ORIGINAL FACE FINDER TOOL CREATED BY SANDEEP SIVA PRASAD, 2017

from PySide import QtGui,QtCore
import os
import sys
import fnmatch
import subprocess
import pyfbsdk as mobu 
from RS import Perforce
from RS.Utils import Scene
from xml.dom import minidom
from RS import Config
from pyfbsdk_additions import *
from time import gmtime, strftime
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types import Face
import RS.Perforce as p4
import xml.etree.ElementTree as ET
   
'''
def findDetails(namespace):
    checkString = ("*{0}*".format(namespace))
    compList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(checkString, compList, True, True)
    for each in compList:
        print each.Name
        print each.PropertyList.Find('Reference Path').Data
        print each.PropertyList.Find('Namespace').Data
        print each.PropertyList.Find('P4_Version').Data
        print each.PropertyList.Find('Target_Namespace').Data
        print each.PropertyList.Find('Take_Name').Data
'''

def findP4Version(namespace):
    checkString = ("*{0}*".format(namespace))
    compList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(checkString, compList, True, True)
    p4Version =  compList[0].PropertyList.Find('P4_Version').Data
    return p4Version
    
def faceRefsMethod():
    manager = Manager()
    simpleInfo = mobu.FBStringList()
    faceRefs = manager.GetReferenceListByType(Face.Face)
    return faceRefs
    

class baseDialog(QtGui.QWidget):
    TOOL_NAME = "Face Finder"
    
    def __init__(self, parent=None):
        super(baseDialog, self).__init__(parent=parent)        
        self.setWindowTitle("Face Finder")
        
        mainLayout = QtGui.QVBoxLayout()
        grid = QtGui.QGridLayout()
        
        faceRefs = faceRefsMethod()
        count = len(faceRefs)
        rigName=[]
        charName=[]
        defaultIndex = 0

        for faceRefsIndex in range(0,count+1):
            if (faceRefsIndex==0):
                grid.addWidget(QtGui.QLabel("      Face-Rig Name"),faceRefsIndex,1)
                grid.addWidget(QtGui.QLabel(""),faceRefsIndex,1)
                grid.addWidget(QtGui.QLabel("Character Name"),faceRefsIndex,3)
            else:
                i = faceRefsIndex-1
                rigName.append(faceRefs[i].Namespace)
                charName.append(faceRefs[i].TargetNamespace)
                
                radiobutton = QtGui.QRadioButton(rigName[i])
                #if(i==0):
                    #radiobutton.setChecked(True)
                radiobutton.index = i
                radiobutton.toggled.connect(self.radioBtnClicked)
                
                grid.addWidget(radiobutton,faceRefsIndex,1)
                grid.addWidget(QtGui.QLabel(""),faceRefsIndex,2)
                grid.addWidget(QtGui.QLabel(charName[i]),faceRefsIndex,3)
        
        self.moreInfoText = QtGui.QTextEdit()
        self.moreInfoText.setReadOnly(True)
        
        self.moreInfoLabel = QtGui.QPushButton("Check details")
        self.moreInfoLabel.setToolTip("Click any radio button above to find details below")
        self.moreInfoLabel.setEnabled(False)
        mainLayout.addLayout(grid)
        mainLayout.addWidget(self.moreInfoLabel)
        mainLayout.addWidget(self.moreInfoText)
        self.setLayout(mainLayout)
        
    
    def radioBtnClicked(self):
        faceRefs = faceRefsMethod()
        rigName = []

        for each in faceRefs:
            rigName.append(each.Namespace)
        
        radioButton = self.sender()
        index = radioButton.index
        if radioButton.isChecked():    
            print("Rig Name is {0}".format(rigName[index]))
            
            p4Version = findP4Version(rigName[index])
                
            text = ""
            text = text + "{0}\n\n".format(str(faceRefs[index].Namespace))
            text = text + "Last User: {0}\n\n".format(str(Perforce.GetFileInfo(faceRefs[index].Path, 'head', 'user')))
            localPath = str(faceRefs[index].Path)
            p4Path = (localPath.replace("\\","/")).replace("X:/","//")
            text = text + "Local Filepath: {0}\n\n".format(localPath)
            text = text + "Perforce Filepath: {0}\n\n".format(p4Path)
            text = text + "P4 Version: {0}\n\n".format(p4Version)
            text = text + "Take Name: {0}\n\n".format(str(faceRefs[index].TakeName))
            
            self.moreInfoText.setText(text)
            

newUI = baseDialog()
newUI.show()