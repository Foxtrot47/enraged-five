###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_RigInABox_GenCode.py
## Written And Maintained By: David Bailey
## Contributors: -
## Description: Generates C++ Code For Expression Plug-Ins
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
from pyfbsdk import *
import RS.Utils.Collections
from xml.dom import minidom
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gFileName = "{0}\\art\\peds\\Ped_Models\\Z_Z_GTAVHeadTestv15\\3LateralData\\RockStar_GTAV_masterRig_v15_jointsSDK_001.xml".format( RS.Config.Project.Path.Root )
gInputArray = []
gExpressionObjectArray = []
gExpressionArray = []
gFileArray = []
gInputVector3d = []
gOutputVector3d = []
gInputDoubleArray = []
gOutputDoubleArray = []
gZeroValueArray = []
gIter = 0

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression XML
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_FindLongestNameInArray(pArray):
    
    lInputLength = 0
    for iInput in pArray:
        if len(iInput) > lInputLength:
            lInputLength = len(iInput)
            
    return lInputLength

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AddHeader(pVectorType, pInputOutput):

    gFileArray.append("\n" + 
                      "//###############################################################################################################\n" +
                      "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" + 
                      "//##\n" +
                      "//## Description: " + pVectorType + " " + pInputOutput + "\n" +
                      "//##\n" +
                      "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" + 
                      "//###############################################################################################################"
                      "\n")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ParseVectors(pArray, pVectorType, pInputOutput):
    
    global gFileArray
    global gIter
    
    lDuplicateArray = []
    lInputVector3d = []
    
    lAnimationNodeType = None
    lPartition = None
    lVectorType = None
    lUnderScore = ""
    
    if pInputOutput == "Input":
        lPartition = "_"
        lUnderScore = "_"
        lAnimationNodeType = "AnimationNodeInCreate"
        if pVectorType == "Position":
            lVectorType = "Pos"
        elif pVectorType == "Rotation":
            lVectorType = "Rot"
        elif pVectorType == "Scale":
            lVectorType = "Scale"
    else:
        lPartition = "-"
        lUnderScore = ""
        lAnimationNodeType = "AnimationNodeOutCreate"
        if pVectorType == "Position":
            lVectorType = "_Position"
        elif pVectorType == "Rotation":
            lVectorType = "_Rotation"
        elif pVectorType == "Scale":
            lVectorType = "_Scale"
        
    rs_AddHeader(pVectorType, pInputOutput)
    
    lInputLength = rs_FindLongestNameInArray(pArray)
    
    for i in range(len(pArray)):
        if not pArray[i].rpartition(lPartition)[0] in lDuplicateArray:
            lDuplicateArray.append(pArray[i].rpartition(lPartition)[0])
       
    for iDuplicate in lDuplicateArray:  
        lTemporaryArray = []
        for iElement in pArray:
            if iElement.startswith(iDuplicate + "-"):
                
                lTemporaryArray.append(iElement.rpartition(lPartition)[2])
        
        gIter = gIter + 1
            
        if pInputOutput == "Input":
            gInputVector3d.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType)
        else:
            gOutputVector3d.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType)
        
        lCode = '    m' + iDuplicate + '_XYZ' + lUnderScore + lVectorType
        for i in range(((lInputLength) - len(iDuplicate))):
            lCode = lCode + " "
        lCode = lCode + ' = ' + lAnimationNodeType + ' ( ' + str(gIter) + ', "' + iDuplicate + '_XYZ' + lUnderScore + lVectorType + '", ANIMATIONNODE_TYPE_VECTOR );'
        gFileArray.append(lCode)
        gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[0] = 0;")
        gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[1] = 0;")
        gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[2] = 0;")
##        print iDuplicate + '_XYZ' + lUnderScore + lVectorType
##        print lTemporaryArray
##        if len(lTemporaryArray) == 2:
##        
##            if ("X" + lVectorType) and ("Y" + lVectorType) in lTemporaryArray:
##                
##                if pInputOutput == "Output":
##                
##                    gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[2] = 0;")
##        
##            elif ("X" + lVectorType) and ("Z" + lVectorType) in lTemporaryArray:
##                
##                if pInputOutput == "Output":
##                
##                    gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[1] = 0;")
##        
##            elif ("Y" + lVectorType) and ("Z" + lVectorType) in lTemporaryArray:
##                
##                if pInputOutput == "Output":
##                
##                    gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[0] = 0;")
##        
##        elif len(lTemporaryArray) == 1:
##        
##            if ("X" + lVectorType) in lTemporaryArray:
##                
##                if pInputOutput == "Output":
##                
##                    gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[1] = 0;")
##                    gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[2] = 0;")
##
##            elif ("Y" + lVectorType) in lTemporaryArray:
##                
##                if pInputOutput == "Output":
##                    
##                    gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[0] = 0;")
##                    gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[2] = 0;")
##        
##            elif ("Z" + lVectorType) in lTemporaryArray:
##                    
##                if pInputOutput == "Output":    
##                    
##                    gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[0] = 0;")
##                    gZeroValueArray.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType + "[1] = 0;")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression XML
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ParseCustom(pArray, pVectorType, pInputOutput):
    
    global gIter
    
    rs_AddHeader(pVectorType, pInputOutput)
    lInputLength = rs_FindLongestNameInArray(pArray)
    
    lAnimationNodeType = None
    if pInputOutput == "Input":
        lAnimationNodeType = "AnimationNodeInCreate"
    else:
        lAnimationNodeType = "AnimationNodeOutCreate"
    
    for iElement in pArray:
        
        gIter = gIter + 1
        
        if pInputOutput == "Input":
            gInputDoubleArray.append(iElement)
        else:
            gOutputDoubleArray.append(iElement)
        
        lCode = "    m" + iElement 
        for i in range(((lInputLength + 9) - len(iElement))):
            lCode = lCode + " "
        lCode = lCode + ' = ' + lAnimationNodeType + ' ( ' + str(gIter) + ', "' + iElement + '", ANIMATIONNODE_TYPE_NUMBER );'
        gFileArray.append(lCode)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression XML
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ParseExpressionXML(pXmlFileName):
    
    global gInputArray
    global gExpressionObjectArray
    global gExpressionArray
    
    gExpression = ""
    
    lXmlFile = minidom.parse(pXmlFileName)
    for iNode in lXmlFile.childNodes:
        gExpression = ""
        for iNode1 in iNode.childNodes:
            if iNode1.nodeType != iNode1.TEXT_NODE: 
                if iNode1.nodeName == "driven":
                    
                    lExpressionObj = (iNode1.attributes["expObj"].value + "-" + iNode1.attributes["expCont"].value)
                    
                    #if not lExpressionObj in gExpressionObjectArray:
                    gExpressionObjectArray.append(iNode1.attributes["expObj"].value + "-" + iNode1.attributes["expCont"].value)
                    gExpression = iNode1.attributes["expString"].value
                    #else:
                    #gExpression = gExpression + " + " + iNode1.attributes["expString"].value
                    gExpressionArray.append(gExpression)
                    
                    for iNode2 in iNode1.childNodes:
                        if iNode2.nodeType != iNode2.TEXT_NODE: 
                            if iNode2.nodeName == "driver":
                                if not iNode2.attributes["scalarNames"].value.startswith("FACIAL"):
                                    gInputArray.append(iNode2.attributes["scalarNames"].value) 
                     
    gInputArray = RS.Utils.Collections.RemoveDuplicatesFromList(gInputArray)   
    gInputArray = sorted(gInputArray)         

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression XML
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ParseVariables():
    gFileArray.append("\n" + 
                      "int lStatus[" + str(len(gInputVector3d) + len(gInputDoubleArray)) + "];\n" +
                      "\n")
    for i in range(len(gInputVector3d)):
        if i == 0:
            gFileArray.append("double " + gInputVector3d[i] + "[3],")
        else:
           gFileArray.append("       " + gInputVector3d[i] + "[3],")
    for i in range(len(gOutputVector3d)):
           gFileArray.append("       " + gOutputVector3d[i] + "[3],")
    gFileArray.append(";\n")
    for i in range(len(gInputDoubleArray)):
        if i == 0:
            gFileArray.append("double " + gInputDoubleArray[i] + ",")
        else:
            gFileArray.append("       " + gInputDoubleArray[i] + ",")
    for i in range(len(gOutputDoubleArray)):
            gFileArray.append("       " + gOutputDoubleArray[i] + ",")
    gFileArray.append(";\n")
    gFileArray.append("\n")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression XML
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_lStatus():
    
    lTotal = 0
    
    for i in range(len(gInputVector3d)):
        
        gFileArray.append("lStatus[" + str(i) + "] = m" + gInputVector3d[i] + " -> ReadData( " + gInputVector3d[i] + ", pEvaluateInfo );")
        lTotal = i
        
    for i in range(len(gInputDoubleArray)):
        
        gFileArray.append("lStatus[" + str(i + (lTotal + 1)) + "] = m" + gInputDoubleArray[i] + " -> ReadData( &" + gInputDoubleArray[i] + ", pEvaluateInfo );")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression XML
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_FacialExpressions():
    
    rs_AddHeader("Facial", "Expressions")
    
    global gExpressionObjectArray
    global gExpressionArray
    
    tempExpObjArray = []
    tempExpressionArray = []
    
    tempExpObjArray = RS.Utils.Collections.RemoveDuplicatesFromList(gExpressionObjectArray) 
    
    for i in range(len(tempExpObjArray)):
        expression =  ""
        for j in range(len(gExpressionObjectArray)):
            if tempExpObjArray[i] == gExpressionObjectArray[j]:
                if expression == "":
                    expression = gExpressionArray[j]
                else:
                    expression = expression + " + " + gExpressionArray[j]
        tempExpressionArray.append(expression)

##    for iZeroValue in gZeroValueArray:
##        print iZeroValue
##        gFileArray.append(iZeroValue)
    
    allConds = []
    
    for i in range(len(tempExpObjArray)):
        lController = tempExpObjArray[i].partition("-")
        rs_AddHeader(lController[0], lController[2])
          
        temp = tempExpressionArray[i].split(" + ")
        temp.sort()
        
        lCondVarArray = []
        lCondArray = []
        lIfArray = []
        
        ifCount = 0
        
        for tmp in temp:
            if "if" in tmp:
                ifCount = ifCount + 1
                tmp = tmp.replace(" ", "", len(temp))
                ifTemp = tmp.split(",")
                lCond = ""
                
                variable = ""
                if "XPos" in ifTemp[0]:
                    variable = ifTemp[0].replace("XPos", "XYZ_Pos[0]", len(ifTemp[0]))
                elif "YPos" in ifTemp[0]:
                    variable = ifTemp[0].replace("YPos", "XYZ_Pos[1]", len(ifTemp[0]))
                elif "ZPos" in ifTemp[0]:
                    variable = ifTemp[0].replace("ZPos", "XYZ_Pos[2]", len(ifTemp[0]))
                elif "XRot" in ifTemp[0]:
                    variable = ifTemp[0].replace("XRot", "XYZ_Rot[0]", len(ifTemp[0]))
                elif "YRot" in ifTemp[0]:
                    variable = ifTemp[0].replace("YRot", "XYZ_Rot[1]", len(ifTemp[0]))
                elif "ZRot" in ifTemp[0]:
                    variable = ifTemp[0].replace("ZRot", "XYZ_Rot[2]", len(ifTemp[0]))
                elif "XScale" in ifTemp[0]:
                    variable = ifTemp[0].replace("XScale", "XYZ_Scale[0]", len(ifTemp[0]))
                elif "YScale" in ifTemp[0]:
                    variable = ifTemp[0].replace("YScale", "XYZ_Scale[1]", len(ifTemp[0]))
                elif "ZScale" in ifTemp[0]:
                    variable = ifTemp[0].replace("ZScale", "XYZ_Scale[2]", len(ifTemp[0]))
                else:
                    variable = ifTemp[0]
                
                
                
                if "<" in ifTemp[0]:
                    condVar = ifTemp[0][4:ifTemp[0].index("<")]
                    allConds.append(condVar)
                    lCondVarArray.append("float " + condVar + "_Cond" + str(allConds.count(condVar))+ " = 0;")
                    lCond = condVar + "_Cond" + str(allConds.count(condVar))
                    lCondArray.append(lCond)
                elif ">" in ifTemp[0]:
                    condVar = ifTemp[0][4:ifTemp[0].index(">")]
                    allConds.append(condVar)
                    lCondVarArray.append("float " + condVar + "_Cond" + str(allConds.count(condVar)) + " = 0;") 
                    lCond = condVar + "_Cond" + str(allConds.count(condVar))
                    lCondArray.append(lCond)
                lIfArray.append("\n" + variable[1:len(variable)] + ")\n" +
                                "{")
                
                variable = ""
                if "XPos" in ifTemp[1]:
                    variable = ifTemp[1].replace("XPos", "XYZ_Pos[0]", len(ifTemp[1]))
                elif "YPos" in ifTemp[1]:
                    variable = ifTemp[1].replace("YPos", "XYZ_Pos[1]", len(ifTemp[1]))
                elif "ZPos" in ifTemp[1]:
                    variable = ifTemp[1].replace("ZPos", "XYZ_Pos[2]", len(ifTemp[1]))
                elif "XRot" in ifTemp[1]:
                    variable = ifTemp[1].replace("XRot", "XYZ_Rot[0]", len(ifTemp[1]))
                elif "YRot" in ifTemp[1]:
                    variable = ifTemp[1].replace("YRot", "XYZ_Rot[1]", len(ifTemp[1]))
                elif "ZRot" in ifTemp[1]:
                    variable = ifTemp[1].replace("ZRot", "XYZ_Rot[2]", len(ifTemp[1]))
                elif "XScale" in ifTemp[1]:
                    variable = ifTemp[1].replace("XScale", "XYZ_Scale[0]", len(ifTemp[1]))
                elif "YScale" in ifTemp[1]:
                    variable = ifTemp[1].replace("YScale", "XYZ_Scale[1]", len(ifTemp[1]))
                elif "ZScale" in ifTemp[1]:
                    variable = ifTemp[1].replace("ZScale", "XYZ_Scale[2]", len(ifTemp[1]))
                else:
                    variable = ifTemp[1]
                
                lIfArray.append("    " + lCond + " = " + variable + ";\n" +
                                "}else{")
                
                variable = ""
                if "XPos" in ifTemp[2][0:len(ifTemp[2]) - 2]:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2].replace("XPos", "XYZ_Pos[0]", len(ifTemp[2][0:len(ifTemp[2]) - 2]))
                elif "YPos" in ifTemp[2][0:len(ifTemp[2]) - 2]:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2].replace("YPos", "XYZ_Pos[1]", len(ifTemp[2][0:len(ifTemp[2]) - 2]))
                elif "ZPos" in ifTemp[2][0:len(ifTemp[2]) - 2]:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2].replace("ZPos", "XYZ_Pos[2]", len(ifTemp[2][0:len(ifTemp[2]) - 2]))
                elif "XRot" in ifTemp[2][0:len(ifTemp[2]) - 2]:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2].replace("XRot", "XYZ_Rot[0]", len(ifTemp[2][0:len(ifTemp[2]) - 2]))
                elif "YRot" in ifTemp[2][0:len(ifTemp[2]) - 2]:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2].replace("YRot", "XYZ_Rot[1]", len(ifTemp[2][0:len(ifTemp[2]) - 2]))
                elif "ZRot" in ifTemp[2][0:len(ifTemp[2]) - 2]:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2].replace("ZRot", "XYZ_Rot[2]", len(ifTemp[2][0:len(ifTemp[2]) - 2])) 
                elif "XScale" in ifTemp[2][0:len(ifTemp[2]) - 2]:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2].replace("XScale", "XYZ_Scale[0]", len(ifTemp[2][0:len(ifTemp[2]) - 2]))
                elif "YScale" in ifTemp[2][0:len(ifTemp[2]) - 2]:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2].replace("YScale", "XYZ_Scale[1]", len(ifTemp[2][0:len(ifTemp[2]) - 2]))
                elif "ZScale" in ifTemp[2][0:len(ifTemp[2]) - 2]:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2].replace("ZScale", "XYZ_Scale[2]", len(ifTemp[2][0:len(ifTemp[2]) - 2]))
                else:
                    variable = ifTemp[2][0:len(ifTemp[2]) - 2]
                
                lIfArray.append("    " + lCond + " = " + variable + ";\n" +
                                "}\n")
        
        for iCond in lCondVarArray:
            gFileArray.append(iCond)
        for iIf in lIfArray:
            gFileArray.append(iIf)
         
        expObjSplit = tempExpObjArray[i].partition("-")
        vecorType = expObjSplit[2].partition("_")
        expObj = ""
        
        if "X" in expObjSplit[2]:
            expObj = expObjSplit[0] + "_XYZ_" + vecorType[2] + "[0]"
        elif "Y" in expObjSplit[2]:
            expObj = expObjSplit[0] + "_XYZ_" + vecorType[2] + "[1]"
        elif "Z" in expObjSplit[2]: 
            expObj = expObjSplit[0] + "_XYZ_" + vecorType[2] + "[2]"
        
        gFileArray.append(expObj + " = \n")
        
        isRotation = False
        
        if vecorType[2] == "Rotation":
            isRotation = True
            gFileArray.append("    (")
        
        for i in range(len(lCondArray)):
            if (len(temp) - ifCount) > 0:
                gFileArray.append("    " + lCondArray[i] + " + ")
            elif i != len(lCondArray) - 1:
                gFileArray.append("    " + lCondArray[i] + " + ")
            else:
                if isRotation == True:
                    gFileArray.append("    " + lCondArray[i] + ") *  57.2957795; ")
                else:
                    gFileArray.append("    " + lCondArray[i] + "; ")
                    
        for i in range(len(temp)):
            variable = ""
            if not "if" in temp[i]:
                if "XPos" in temp[i]:
                    variable = temp[i].replace("XPos", "XYZ_Pos[0]", len(temp[i]))
                elif "YPos" in temp[i]:
                    variable = temp[i].replace("YPos", "XYZ_Pos[1]", len(temp[i]))
                elif "ZPos" in temp[i]:
                    variable = temp[i].replace("ZPos", "XYZ_Pos[2]", len(temp[i]))
                elif "XRot" in temp[i]:
                    variable = temp[i].replace("XRot", "XYZ_Rot[0]", len(temp[i]))
                elif "YRot" in temp[i]:
                    variable = temp[i].replace("YRot", "XYZ_Rot[1]", len(temp[i]))
                elif "ZRot" in temp[i]:
                    variable = temp[i].replace("ZRot", "XYZ_Rot[2]", len(temp[i]))
                elif "XScale" in temp[i]:
                    variable = temp[i].replace("XScale", "XYZ_Scale[0]", len(temp[i]))
                elif "YScale" in temp[i]:
                    variable = temp[i].replace("YScale", "XYZ_Scale[1]", len(temp[i]))
                elif "ZScale" in temp[i]:
                    variable = temp[i].replace("ZScale", "XYZ_Scale[2]", len(temp[i]))
                else:
                    variable = temp[i]
                if i != (len(temp) - ifCount) - 1:
                    gFileArray.append("    " + variable + " + ")
                else:
                    if isRotation == True:
                        gFileArray.append("    " + variable + ") *  57.2957795; ")
                    else:
                        gFileArray.append("    " + variable + ";")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression XML
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_FacialExpressionsOutput():
    
    rs_AddHeader("Write", "Data")
    
    lDuplicateArray = []
    
    ltempArray = RS.Utils.Collections.RemoveDuplicatesFromList(gExpressionObjectArray) 
    
    for i in range(len(ltempArray)):
        if not (ltempArray[i].rpartition("-")[0] + " " + ltempArray[i].rpartition("_")[2]) in lDuplicateArray:
            lDuplicateArray.append(ltempArray[i].rpartition("-")[0] + " " + ltempArray[i].rpartition("_")[2])
    
    for iDuplicate in lDuplicateArray:  
        for iElement in gExpressionObjectArray:
            if iElement.startswith(iDuplicate.rpartition(" ")[0] + "-"):
##                expObjSplit = iElement.partition("-")
##                vecorType = expObjSplit[2].partition("_")
                expObj = iDuplicate.rpartition(" ")[0] + "_XYZ_" + iDuplicate.rpartition(" ")[2]
            
        gFileArray.append("m" + expObj + " -> WriteData( &" + expObj + "[0], pEvaluateInfo );")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression XML
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_SortInputsType(pArray, pInputOutput):
      
    lPosArray = []
    lRotArray = []
    lScaleArray = []
    lCustomArray = []
    
    for iInput in pArray:
        if "Pos" in iInput:
            lPosArray.append(iInput)
        elif "Rot" in iInput:
            lRotArray.append(iInput)
        elif "Sca" in iInput:
            lScaleArray.append(iInput)
        else:
            lCustomArray.append(iInput)
            
    rs_ParseVectors(lPosArray, "Position", pInputOutput)
    
    rs_ParseVectors(lRotArray, "Rotation", pInputOutput)
    
    rs_ParseVectors(lScaleArray, "Scale", pInputOutput)
    
    rs_ParseCustom(lCustomArray, "Custom", pInputOutput)
    
def Run():
    rs_ParseExpressionXML(gFileName)
    ##rs_SortInputsType(gInputArray, "Input")
    ##rs_SortInputsType(gExpressionObjectArray, "Output")
    ##rs_ParseVariables()
    ##rs_lStatus()
    ##rs_FacialExpressions()
    rs_FacialExpressionsOutput()
    for iCode in gFileArray:
        print iCode
            
    

