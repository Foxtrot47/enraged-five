import pyfbsdk as mobu

def getAllChildren(lRoot, lList, exclude=None):
    if lRoot not in lList:
        lList.append(lRoot)
    for each in lRoot.Children:
        if each.ClassName() == "FBModel":
            if each.Name != exclude:
                if each not in lList:
                    lList.append(each)
                lList = getAllChildren(each, lList)
    return lList
    
def positionFFXCtrl(ffxRoot, lName, lPos):
    for child in ffxRoot.Children:
        if lName in child.Name:
            child.Translation.Data = mobu.FBVector3d(lPos[0], lPos[1], lPos[2])

def positionFFX(lRoot, ambientRoot, targetPosition, showSetting):
    
    lRoot.Parent = ambientRoot
    lRoot.Translation.Data = targetPosition
    positionFFXCtrl(lRoot, "IH", (0.25, -.1, 0.0))
    positionFFXCtrl(lRoot, "tBack", (-.35, -.3, 0.0))
    positionFFXCtrl(lRoot, "tTeeth", (-.25, -.3, 0.0))
    positionFFXCtrl(lRoot, "tRoof", (-.15, -.3, 0.0))
    positionFFXCtrl(lRoot, "Brow_Up_L", (-.35, -.5, 0.0))
    positionFFXCtrl(lRoot, "Brow_Up_R", (-.25, -.5, 0.0))
    positionFFXCtrl(lRoot, "Brows_Down", (-.15, -.5, 0.0))
    positionFFXCtrl(lRoot, "Squint", (0.0, -.4, 0.0))
    
    for each in getAllChildren(ambientRoot, [], exclude=lRoot.Name):
        each.Show = showSetting

def getLowest(lList):
    lowestPos = mobu.FBVector3d(0,10000,0)
    for each in lList:
        if each.Translation.Data[1] < lowestPos[1]:
            lowestPos = each.Translation.Data
    return lowestPos

def getPosSetting(lRoot):
    
    lRootPosZero = mobu.FBVector3d(0.0, -4., 0.0)
    lRootPosRight = mobu.FBVector3d(13.0, -4., 0.0)
    lRootPosLeft = mobu.FBVector3d(-24.0, -4., 0.0)
    
    if lRoot.Translation.Data == lRootPosRight:
        return lRootPosLeft, True
    elif lRoot.Translation.Data == lRootPosLeft:
        return lRootPosZero, False
    else:
        return lRootPosRight, True
    

def run():
    
    ambUI = "Ambient_UI"
    ffxUI = "FaceFX"
    
    ambientRootList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(ambUI, ambientRootList, False, True)
    
    nextPos = getLowest(ambientRootList)
    for ambRoot in ambientRootList:
        ambRoot.Translation.Data = nextPos
        nextPos = nextPos + mobu.FBVector3d(0., 40.0, 0.)
    
    ffxRootList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(ffxUI, ffxRootList, False, True)
    
    posSetting = None
    showSetting = True
    for ffxRoot in ffxRootList:
        if posSetting == None:
            posSetting, showSetting = getPosSetting(ffxRoot)
            
        if ":" in ffxRoot.LongName:
            lNS = ffxRoot.LongName.split(":")[0]
            ambientRootName = "{0}:{1}".format(lNS, ambUI)
        else:
            ambientRootName = ambUI
        ambientRoot = mobu.FBFindModelByLabelName(ambientRootName)
        if ambientRoot:
            positionFFX(ffxRoot, ambientRoot, posSetting, showSetting)