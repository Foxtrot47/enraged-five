from pyfbsdk import *

import os
import xml.etree.cElementTree

import RS.Perforce
import RS.Config


PRE_ROTATION_DIR = '{0}\\art\\animation\\resources\\PreRotation'.format( RS.Config.Project.Path.Root )


def getTabs( tabLevel ):
    tabStr = ''
    
    for i in range( 0, tabLevel ):
        tabStr += '\t'
        
    return tabStr

def recurceExportPreRotation( currentBone, fstream, tabLevel ):
    '''
    Description:
        Recursively write out the pre-rotation for each bone in the hierarchy to the xml file stream.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''
    
    for child in currentBone.Children:
        x, y, z = child.PreRotation
        fstream.write( '{0}<bone name="{1}" preRotation="{2} {3} {4}" />\n'.format( getTabs( tabLevel ), child.Name, x, y, z ) )
        recurceExportPreRotation( child, fstream, tabLevel )
      
def exportPreRotation( fbxFilename, rootBone = 'FACIAL_facialRoot' ):
    '''
    Description:
        Exports pre-rotations for the skeleton hierarchy under the supplied root bone to an xml file.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''
    
    global PRE_ROTATION_DIR
    
    if rootBone == None:
        FBMessageBox( 'Rockstar', 'A root bone name must be supplied in order to export the pre-rotations!', 'OK' )
        return False
    
    # Didn't supply a fbx filename, so use the current scene.
    if fbxFilename == None:
        fbxFilename = FBApplication().FBXFileName
        
    # Supplied a fbx filename, so open that scene.
    else:
        RS.Perforce.Sync( fbxFilename )

        if not os.path.isfile( fbxFilename ):
            fbxFilename = None
            
    if fbxFilename:
        FBApplication().FileOpen( fbxFilename )
        
        theRootBone = FBFindModelByName( rootBone )
        
        if theRootBone:
        
            # Find the pre-rotation file for the fbx filename supplied.
            filename = str( os.path.basename( fbxFilename ) ).split( '.' )[ 0 ]
        
            if not os.path.isdir( PRE_ROTATION_DIR ):
                os.mkdir( preRotDir )
            
            preRotFile = '{0}\\{1}.prerot'.format( PRE_ROTATION_DIR, filename )
            
            if not RS.Perforce.DoesFileExist( preRotFile ):
                RS.Perforce.add( preRotFile )
                
            if ( RS.Perforce.Edit( preRotFile ) ):
                fstream = open( preRotFile, 'w' )
                fstream.write( '<?xml version="1.0" encoding="utf-8"?>\n' )
                fstream.write( '<bones>\n' )
                
                recurceExportPreRotation( theRootBone, fstream, 1 )
                
                fstream.write( '</bones>\n' )
                fstream.close()
                
                return True
            
            else:
                FBMessageBox( 'Rockstar', 'Could not export pre-rotations because the file cannot be checked out!\n\n{0}'.format( fbxFilename ), 'OK' )
        else:
            result = FBMessageBox( 'Rockstar', 'Could not find the supplied root bone name ({0})!  Cannot export pre-rotations for:\n\n{1}\n\nMaybe this file does not have a facial rig setup?'.format( rootBone, fbxFilename ), 'Continue', 'Cancel' )
            
            if result == 1:
                return True
    else:
        FBMessageBox( 'Rockstar', 'Cannot export pre-rotations because the file does not exist.\n\n{0}'.format( fbxFilename ), 'OK' )
        
    return False
        
def applyPreRotation( charNamespace ):
    '''
    Description:
        Imports and applies the pre-rotations to skeleton bones who match the supplied character namespace.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''
    
    global PRE_ROTATION_DIR
    
    preRotFile = '{0}\\{1}.prerot'.format( PRE_ROTATION_DIR, charNamespace )

    if not RS.Perforce.DoesFileExist( preRotFile ):
        FBMessageBox( 'Rockstar', 'There is no pre-rotation xml file for character ({0})!  Please export one.\n\nAborting pre-rotation application for this character.'.format( charNamespace ), 'OK' )
        
    else:
        RS.Perforce.Sync( preRotFile )
        
        doc = xml.etree.cElementTree.parse( preRotFile )
        root = doc.getroot()
        
        if root:
            bones = root.findall( 'bone' )
            
            for bone in bones:
                boneName = bone.get( 'name' )
                x, y, z = str( bone.get( 'preRotation' ) ).split( ' ' )
                preRot = FBVector3d( float( x ), float( y ), float( z ) )
                
                sceneBone = FBFindModelByName( '{0}:{1}'.format( charNamespace, boneName ) )
                
                if sceneBone:
                    lclRotation = sceneBone.PropertyList.Find( 'Lcl Rotation' )
                    rotationActive = sceneBone.PropertyList.Find( 'RotationActive' )
                    preRotation = sceneBone.PropertyList.Find( 'PreRotation' )
                    
                    if lclRotation and rotationActive and preRotation:
                        if lclRotation.GetAnimationNode():
                            animNodes = lclRotation.GetAnimationNode().Nodes
                            
                            for axisId in range( 0, len( animNodes ) ):
                                animNodes[ axisId ].FCurve.EditClear()
                            
                        lclRotation.Data = FBVector3d( 0, 0 ,0 )
                        rotationActive.Data = True
                        preRotation.Data = preRot
                        
                    else:
                        print 'An unknown error occurred while attempting to query the pre-rotation properties for ({0})!'.format( sceneBone.Name )
                    
                else:
                    print 'Could not apply the pre-rotation to bone ({0}:{1}) because it could not be found!'.format( charNamespace, boneName )
                
def batchExportPreRotation( fileList, boneName = 'FACIAL_facialRoot' ):
    '''
    Description:
        Batch export pre-rotations for the fbx filenames supplied.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''
    
    RS.Perforce.Sync( fileList )
    
    result = True
    idx = 0
    
    while result:
        if idx <= ( len( fileList ) - 1 ):
            fbxFilename = fileList[ idx ]
            result = exportPreRotation( fbxFilename )
            idx += 1
            
        else:
            result = False
    

preRotFileList = ( '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_AmandaTownley.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Andreas.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_ArmySmith.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Ashley.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_BallasOG.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Barry.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Beverly.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Brad.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_BradCadaver.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Brucie.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_ChengSr.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_ChrisFormage.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Clay.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Dale.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_DaveNorton.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Debra.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Denise.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Devin.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Dom.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_DrFriedlander.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_DrFriedlander_Comparison.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_DrFriedlander_Comparison1.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_FBIsuit_01.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_FBIsuit_01_VisemeTest.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Fabien.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Female_Proxy.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Floyd.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Guadalope.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Gurk.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Hunter.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Ira.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_JAY_Norris.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Janet.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_JewelAss.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_JimmyBoston.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_JimmyDiSanto.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_JoeMinuteMan.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_JohnnyKlebitz.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Josef.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Josh.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_LamarDavis.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Lazlow.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_LemonFem_01.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_LemonFem_02.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_LemonMale_01.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_LesterCrest.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_LifeInvad_01.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_MRK.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_MRSA.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_MRSB.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_MRS_Thornhill.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Magenta.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Male_Proxy.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Manuel.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Marnie.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_MartinMadrazo.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_MaryAnn.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Michelle.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Milton.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Natalia.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_NervousRon.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Nigel.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Old_Man1A.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Old_Man2.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Omega.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Paper.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Patricia.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_PornStar.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Priest.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_ProlSec.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_ProlSec_02.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_RussianDrunk.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Sasq.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_SextonDaniels.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_SiemonYetarian.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Solomon.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_SteveHains.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Stretch.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Tanisha.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_TaoCheng.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_TaosTranslator.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_TennisCoach.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Terry.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Tom.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_TomEpsilon.FBX'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_TracyDiSanto.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Wade.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_Zimbor.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\Models\\cutscene\\Player_One.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\Models\\cutscene\\Player_Zero.fbx'.format( RS.Config.Project.Path.Root ),
                    '{0}\\art\\animation\\resources\\characters\\Models\\cutscene\\Player_Two.fbx'.format( RS.Config.Project.Path.Root ) )