from RS.Core.Face.FaceFinder import FaceFinder
reload (FaceFinder)
from RS.Tools import UI

@UI.Run(title=FaceFinder.baseDialog.TOOL_NAME, dockable=True)

def Run():
    return FaceFinder.baseDialog()