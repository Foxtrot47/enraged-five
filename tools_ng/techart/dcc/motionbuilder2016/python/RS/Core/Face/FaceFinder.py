#CODE: FACE_FINDER
#PROJECT: GTAV
#FUNCTIONALITY: PROVIDES TABULAR INFO OF FACE FILES AND THE
#               CORRESPONDING CHARACTERS ON WHICH THOSE ARE IMPLEMENTED
#AUTHOR: SANDEEP SIVA PRASAD, 2017
#NOTE: ORIGINAL REVISITED AND TRUNCATED BY: SUPRATIM CHAUDHURY, 12-JAN-2022


import os
import sys
import fnmatch
import subprocess
import pyfbsdk as mobu 
from RS import Perforce
from RS.Utils import Scene
from xml.dom import minidom
from RS import Config, Globals
from pyfbsdk_additions import *
from time import gmtime, strftime
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types import Face
import RS.Perforce as p4
import xml.etree.ElementTree as ET


class SelectionButtonWindow(object):
    
    def __init__(self):       
        self.selectionButtons = []
        self.CreateTool()
    #For FaceFinder        


    def FaceFinder(self, control, event):
        faceFinderTool = 0
        
            
        manager = Manager.Manager()        
        moreInfoBtn = mobu.FBButton()       

        
        def BtnMoreInformation(control, event):
            faceFinder = FBCreateUniqueTool("Face Finder1")
            faceFinder.StartSizeX = 500
            faceFinder.StartSizeY = 300
            PopulateLayout1(faceFinder)      
            mobu.ShowTool(faceFinder)
                   
        def PopulateLayout(mainLyt):
            mAX = 0
            #create Spread        
            memo = mobu.FBMemo()
            lyt = FBHBoxLayout()
            x = mobu.FBAddRegionParam(0,mobu.FBAttachType.kFBAttachLeft,"")
            y = mobu.FBAddRegionParam(25,mobu.FBAttachType.kFBAttachTop,"")
            w = mobu.FBAddRegionParam(0,mobu.FBAttachType.kFBAttachRight,"")
            h = mobu.FBAddRegionParam(0,mobu.FBAttachType.kFBAttachBottom,"")           
            mainLyt.AddRegion("memo","memo", x, y, w, h)            
            mainLyt.SetControl("memo",memo)            
            x = mobu.FBAddRegionParam(0,mobu.FBAttachType.kFBAttachNone,"")
            y = mobu.FBAddRegionParam(0,mobu.FBAttachType.kFBAttachNone,"")
            w = mobu.FBAddRegionParam(100,mobu.FBAttachType.kFBAttachNone,"")
            h = mobu.FBAddRegionParam(25,mobu.FBAttachType.kFBAttachNone,"")
            mainLyt.AddRegion("moreInfoBtn","moreInfoBtn", x, y, w, h)
            mainLyt.SetControl("moreInfoBtn", moreInfoBtn)
            moreInfoBtn.Visible = True
            moreInfoBtn.Enabled = True
            moreInfoBtn.Caption = "More Information"
            moreInfoBtn.OnClick.Add(BtnMoreInformation)
            
            #For Simple Information           
            simpleInfo = mobu.FBStringList()
            faceRefs = manager.GetReferenceListByType(Face.Face)
            simpleInfo.Add("Face File Name           *          Implemented Character Name")
            simpleInfo.Add ("---------------------------------------------------------------------")
            for faceRef in faceRefs:
                Takes = faceRef.TakeName
                simpleInfo.Add(('{0}    >>    {1}\n').format(str(faceRef.Namespace),str(faceRef.TargetNamespace)))
                curlen = len(('{0}    >>    ').format(str(faceRef.Namespace),str(faceRef.TargetNamespace)))
                
                if(curlen>=mAX):
                    mAX=curlen
                              
            memo.SetStrings(simpleInfo)
                         
            return mAX*10
            
        #For More Information
        def PopulateLayout1(mainLyt):
            mAX = 0
            #create Spread
            memo = mobu.FBMemo()            
            x = mobu.FBAddRegionParam(0,mobu.FBAttachType.kFBAttachLeft,"")
            y = mobu.FBAddRegionParam(0,mobu.FBAttachType.kFBAttachTop,"")
            w = mobu.FBAddRegionParam(100,mobu.FBAttachType.kFBAttachRight,"")
            h = mobu.FBAddRegionParam(25,mobu.FBAttachType.kFBAttachBottom,"")          
            mainLyt.AddRegion("memo","memo", x, y, w, h)            
            mainLyt.SetControl("memo",memo)
            #add info to layout         
            detail_info = mobu.FBStringList()
            faceRefs = manager.GetReferenceListByType(Face.Face)
            
            detail_info.Add("Face File Name    *    Implemented Character Name    *    Last user    *    Filepath    *    Facefile Takes")
            detail_info.Add ("-------------------------------------------------------------------------------------------------------------------- ")
            for faceRef in faceRefs:
                path = faceRef.Path
                user = Perforce.GetFileInfo(faceRef.Path, 'head', 'user')
                takes = faceRef.TakeName
                details = Perforce.GetUserData(str(user))
                detail_info.Add(('{0}    >>    {1}    >>    {2}    >>    {3}    >>    {4}    >>    {5}\n').format(str(faceRef.Namespace),str(faceRef.TargetNamespace),str(user),str(path),str(takes),str(details)))
                curlen = len(('{0}    >>    {1}').format(str(faceRef.Namespace),str(faceRef.TargetNamespace)))
                
                if(curlen>=mAX):
                    mAX=curlen 
                              
            memo.SetStrings(detail_info)            

            return mAX*7 
                   
        def CreateTool():    
            # Tool creation will serve as the hub for all other controls
            faceFinderTool = FBCreateUniqueTool("Face Finder")
            faceFinderTool.StartSizeX = PopulateLayout(faceFinderTool)
            faceFinderTool.StartSizeY = 250
            PopulateLayout(faceFinderTool)      
            mobu.ShowTool(faceFinderTool)        
            
        CreateTool()

   
    # For UI      
    def PopulateLayout (self):        
        x = mobu.FBAddRegionParam(5,mobu.FBAttachType.kFBAttachLeft,"")
        y = mobu.FBAddRegionParam(5,mobu.FBAttachType.kFBAttachTop,"")
        w = mobu.FBAddRegionParam(-5,mobu.FBAttachType.kFBAttachRight,"")
        h = mobu.FBAddRegionParam(-5,mobu.FBAttachType.kFBAttachBottom,"")
        self.toolWin.AddRegion("main","main",x,y,w,h)            
        grid = FBGridLayout ()
        self.toolWin.SetControl("main",grid)       
   
        btnFaceFinder = mobu.FBButton()
        btnFaceFinder.Caption = "Face Finder"
        grid.Add(btnFaceFinder,0,0)
        btnFaceFinder.OnClick.Add(self.FaceFinder)  
        
                  
    def CreateTool(self):     
        self.toolWin = FBCreateUniqueTool("Face Finder")
        self.toolWin.StartSizeX = 150
        self.toolWin.StartSizeY = 100
        self.PopulateLayout()
        mobu.ShowTool(self.toolWin)
        
           
# Instantiate the window class
myWindow = SelectionButtonWindow()
