from pyfbsdk import *
import RS.Globals as glo

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

g3LateralRig = ["FACIAL_underChin","FACIAL_L_underChin","FACIAL_chin",
                "FACIAL_chinSkinBottom","FACIAL_L_chinSkinBottom","FACIAL_R_chinSkinBottom","FACIAL_tongueA",
                "FACIAL_tongueB","FACIAL_tongueC","FACIAL_tongueD","FACIAL_tongueE","FACIAL_L_tongueE",
                "FACIAL_R_tongueE","FACIAL_L_tongueD","FACIAL_R_tongueD","FACIAL_L_tongueC","FACIAL_R_tongueC",
                "FACIAL_L_tongueB","FACIAL_R_tongueB","FACIAL_L_tongueA","FACIAL_R_tongueA","FACIAL_chinSkinTop",
                "FACIAL_L_chinSkinTop","FACIAL_chinSkinMid","FACIAL_L_chinSkinMid","FACIAL_L_chinSide",
                "FACIAL_R_chinSkinMid","FACIAL_R_chinSkinTop","FACIAL_R_chinSide","FACIAL_R_underChin",
                "FACIAL_L_lipLowerSDK","FACIAL_L_lipLowerAnalog","FACIAL_L_lipLowerThicknessV",
                "FACIAL_L_lipLowerThicknessH","FACIAL_lipLowerSDK","FACIAL_lipLowerAnalog","FACIAL_lipLowerThicknessV",
                "FACIAL_lipLowerThicknessH","FACIAL_R_lipLowerSDK","FACIAL_R_lipLowerAnalog","FACIAL_R_lipLowerThicknessV",
                "FACIAL_R_lipLowerThicknessH","FACIAL_nose","FACIAL_L_nostril","FACIAL_L_nostrilThickness",
                "FACIAL_noseLower","FACIAL_L_noseLowerThickness","FACIAL_R_noseLowerThickness","FACIAL_noseTip",
                "FACIAL_R_nostril","FACIAL_R_nostrilThickness","FACIAL_noseUpper","FACIAL_L_noseUpper","FACIAL_noseBridge",
                "FACIAL_L_nasolabialFurrow","FACIAL_L_nasolabialBulge","FACIAL_L_cheekLower","FACIAL_L_cheekLowerBulge1",
                "FACIAL_L_cheekLowerBulge2","FACIAL_L_cheekInner","FACIAL_L_cheekOuter","FACIAL_L_eyesackLower",
                "FACIAL_L_eyeball","FACIAL_L_eyelidLower","FACIAL_L_eyelidLowerOuterSDK","FACIAL_L_eyelidLowerOuterAnalog",
                "FACIAL_L_eyelashLowerOuter","FACIAL_L_eyelidLowerInnerSDK","FACIAL_L_eyelidLowerInnerAnalog",
                "FACIAL_L_eyelashLowerInner","FACIAL_L_eyelidUpper","FACIAL_L_eyelidUpperOuterSDK","FACIAL_L_eyelidUpperOuterAnalog",
                "FACIAL_L_eyelashUpperOuter","FACIAL_L_eyelidUpperInnerSDK","FACIAL_L_eyelidUpperInnerAnalog",
                "FACIAL_L_eyelashUpperInner","FACIAL_L_eyesackUpperOuterBulge","FACIAL_L_eyesackUpperInnerBulge",
                "FACIAL_L_eyesackUpperOuterFurrow","FACIAL_L_eyesackUpperInnerFurrow","FACIAL_forehead","FACIAL_L_foreheadInner",
                "FACIAL_L_foreheadInnerBulge","FACIAL_L_foreheadOuter","FACIAL_skull","FACIAL_foreheadUpper",
                "FACIAL_L_foreheadUpperInner","FACIAL_L_foreheadUpperOuter","FACIAL_R_foreheadUpperInner",
                "FACIAL_R_foreheadUpperOuter","FACIAL_L_temple","FACIAL_L_ear","FACIAL_L_earLower","FACIAL_L_masseter",
                "FACIAL_L_jawRecess","FACIAL_L_cheekOuterSkin","FACIAL_R_cheekLower","FACIAL_R_cheekLowerBulge1",
                "FACIAL_R_cheekLowerBulge2","FACIAL_R_masseter","FACIAL_R_jawRecess","FACIAL_R_ear","FACIAL_R_earLower",
                "FACIAL_R_eyesackLower","FACIAL_R_nasolabialBulge","FACIAL_R_cheekOuter","FACIAL_R_cheekInner",
                "FACIAL_R_noseUpper","FACIAL_R_foreheadInner","FACIAL_R_foreheadInnerBulge","FACIAL_R_foreheadOuter",
                "FACIAL_R_cheekOuterSkin","FACIAL_R_eyesackUpperInnerFurrow","FACIAL_R_eyesackUpperOuterFurrow",
                "FACIAL_R_eyesackUpperInnerBulge","FACIAL_R_eyesackUpperOuterBulge","FACIAL_R_nasolabialFurrow",
                "FACIAL_R_temple","FACIAL_R_eyeball","FACIAL_R_eyelidUpper","FACIAL_R_eyelidUpperOuterSDK",
                "FACIAL_R_eyelidUpperOuterAnalog","FACIAL_R_eyelashUpperOuter","FACIAL_R_eyelidUpperInnerSDK",
                "FACIAL_R_eyelidUpperInnerAnalog","FACIAL_R_eyelashUpperInner","FACIAL_R_eyelidLower","FACIAL_R_eyelidLowerOuterSDK",
                "FACIAL_R_eyelidLowerOuterAnalog","FACIAL_R_eyelashLowerOuter","FACIAL_R_eyelidLowerInnerSDK",
                "FACIAL_R_eyelidLowerInnerAnalog","FACIAL_R_eyelashLowerInner","FACIAL_L_lipUpperSDK","FACIAL_L_lipUpperAnalog",
                "FACIAL_L_lipUpperThicknessH","FACIAL_L_lipUpperThicknessV","FACIAL_lipUpperSDK","FACIAL_lipUpperAnalog",
                "FACIAL_lipUpperThicknessH","FACIAL_lipUpperThicknessV","FACIAL_L_lipCornerSDK","FACIAL_L_lipCornerAnalog",
                "FACIAL_L_lipCornerThicknessUpper","FACIAL_L_lipCornerThicknessLower","FACIAL_R_lipUpperSDK","FACIAL_R_lipUpperAnalog",
                "FACIAL_R_lipUpperThicknessH","FACIAL_R_lipUpperThicknessV","FACIAL_R_lipCornerSDK","FACIAL_R_lipCornerAnalog",
                "FACIAL_R_lipCornerThicknessUpper","FACIAL_R_lipCornerThicknessLower","FACIAL_jaw_Null",
                "lowerLip_L_OFF","lowerLip_C_OFF","lowerLip_R_OFF",
                "lipCorner_R_OFF","upperLip_C_OFF","upperLip_L_OFF","lipCorner_L_OFF","upperLip_R_OFF",
                "eyelidUpper_L_OFF","eyelidUpperOuter_L_OFF","eyelidUpperInner_L_OFF","eyelidLower_L_OFF",
                "eyelidLowerOuter_L_OFF","eyelidLowerInner_L_OFF","eyelidLower_R_OFF","eyelidLowerOuter_R_OFF",
                "eyelidLowerInner_R_OFF","eyelidUpper_R_OFF","eyelidUpperOuter_R_OFF","eyelidUpperInner_R_OFF"]
                
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_ConstraintsFolder
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def rs_ZeroRotations():
                
    lCheck = False
    for i in range(0,10) :
        lModel_temp = FBFindModelByLabelName(g3LateralRig[i])
        if lModel_temp:
            if lModel_temp.PropertyList.Find("RotationActive").Data:
                if lModel_temp.PropertyList.Find("PreRotation").Data != FBVector3d(0,0,0):
                    lCheck = True
    
    if lCheck :
        print "This already has pre-rotation."
    else :
        for iJoint in g3LateralRig:
            
            lModel = FBFindModelByLabelName(iJoint)
            
            if lModel:
                
                lRotation = lModel.PropertyList.Find("Lcl Rotation").Data
                lModel.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,0)
                lModel.PropertyList.Find("RotationActive").Data = True
                lModel.PropertyList.Find("PreRotation").Data = lRotation
                glo.gScene.Evaluate()
                
                