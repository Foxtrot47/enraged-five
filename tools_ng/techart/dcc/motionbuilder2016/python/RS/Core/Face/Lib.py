'''
Library module for face related tools and functions.

*Author:*
    * Jason Hayes <jason.hayes@rockstarsandiego.com>
'''

from pyfbsdk import *
from ctypes import *

import os
import glob
import zipfile
import tempfile
import shutil
import stat
import math
import re

from PySide import QtGui

from RS.Core.Face import AnimImporter
from RS.Core.Face import FixFaceScale
from RS.Core.Face import NamespaceFN
from RS.Core.Face import Toolbox as faceToolbox

from RS import Config, Globals, Perforce
from RS.Core.Video import videoManager
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types.Face import Face
from RS.Core.ReferenceSystem.Types.Character import Character

# # Constants ##
MOVIE_PLANE_OBJ_NAME = 'MoviePlane'
MOVIE_PLANE_MAT_NAME = 'MoviePlaneMaterial'


def unzipfwr(fwrFileName, extractPath=None):
    '''
    Unzips a .fwr file.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''

    if os.path.isfile(fwrFileName) and str(fwrFileName).endswith('.fwr'):
        fwrName = os.path.basename(str(fwrFileName).split('.')[ 0 ])

        # No extract path supplied, so make it relative to where the file currently lives.
        if extractPath == None:
            extractPath = '{0}\\{1}'.format(os.path.dirname(fwrFileName), fwrName)

        fwrZipObj = zipfile.ZipFile(fwrFileName)

        for zipItem in fwrZipObj.namelist():
            if zipItem.endswith('/'):
                try:
                    os.makedirs('{0}\\{1}'.format(extractPath, zipItem))

                except WindowsError:
                    pass

            else:
                fwrZipObj.extract(zipItem, extractPath)

        return True

    else:
        print 'Did not find a .fwr file to extract!'
        return False


def updateVideoInFwr(fwrFileName, newVideoPath, rotate=False, force=False):
    """
    Update the video frames and sound in a fwr file.

    args:
        fwrFileName (str): The file path to the fwr to update
        newVideoPath (str): The file path to the video to use to update the fwr with

    kwargs:
        rotate (bool): If the video should be rotated or not
        force (bool): If the opertation should go ahead if the fwr is read only
    """
    # Check file is in a good state
    if not os.path.isfile(fwrFileName):
        raise ValueError("fwr file '{0}' does not exit".format(fwrFileName))
    if not os.path.isfile(newVideoPath):
        raise ValueError("video file '{0}' does not exit".format(newVideoPath))

    fileStat = os.stat(fwrFileName)
    if not fileStat[0] & stat.S_IWRITE:
        if not force:
            raise ValueError("fwr file '{0}' cannot be updated as it is read only".format(fwrFileName))
        os.chmod(fwrFileName, stat.S_IWRITE)

    # Make copy of zip and copy everything apart from the video folder over
    tempDir = tempfile.mkdtemp()
    tempZip = os.path.join(tempDir, "tempFwr.zip")

    fwrZipObjRead = zipfile.ZipFile(fwrFileName, "r")
    fwrZipObjWrite = zipfile.ZipFile(tempZip, "w")

    baseName = None
    # Delete Everything under video/
    for zipItem in fwrZipObjRead.namelist():
        buff = fwrZipObjRead.read(zipItem)
        regex = re.match("^video/(.*).wav$", zipItem, re.I)
        if regex is not None:
            baseName = regex.groups(0)[0]
        if not zipItem.startswith("video/"):
            fwrZipObjWrite.writestr(zipItem, buff)
    fwrZipObjRead.close()

    if baseName is None:
        baseName, _ = os.path.splitext(os.path.basename(fwrFileName))

    # Convert Video to image seq/audio
    wavFilePath = os.path.join(tempDir, "{0}.wav".format(baseName))
    videoManager.VideoManager.extractAudioFromVideo(newVideoPath, wavFilePath)
    fwrZipObjWrite.write(wavFilePath, "video/{0}".format(os.path.basename(wavFilePath)))

    framesFilePath = os.path.join(tempDir, "frames")
    os.mkdir(framesFilePath)

    addationFlags = []
    if rotate is True:
        addationFlags.extend(["-vf", "transpose=1"])

    videoManager.VideoManager.createImageSequenceFromVideo(newVideoPath, os.path.join(framesFilePath, "{0}.%04d.jpg".format(baseName)), compression=addationFlags)

    imageSeqFolderName = "video/{0}_frames".format(baseName)
    for fileName in os.listdir(framesFilePath):
        fwrZipObjWrite.write(os.path.join(framesFilePath, fileName), "{0}/{1}".format(imageSeqFolderName, fileName))

    # Replace the orginal file and clean up the temp dir
    fwrZipObjWrite.close()
    shutil.copyfile(tempZip, fwrFileName)
    shutil.rmtree(tempDir)


def getFaceMoviePlane(namespace):
    '''
    Finds and returns the face movie plane for the supplied namespace.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Arguments:*
        * ``namespace`` The namespace of the character.

    *Returns:*
        * ``FBModel`` The movie plane object.
    '''

    global MOVIE_PLANE_OBJ_NAME

    return FBFindModelByLabelName('{0}:{1}'.format(namespace, MOVIE_PLANE_OBJ_NAME))


def removeFaceMoviePlane(namespace):
    '''
    Remove an existing movie plane and materials for a character.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Arguments:*
        * ``namespace`` The namespace of the character.

    *Returns:*
        * ``None``
    '''

    oldMoviePlane = getFaceMoviePlane(namespace)

    if oldMoviePlane:
        for mat in oldMoviePlane.Materials:
            tex = mat.GetTexture()
            tex.FBDelete()
            mat.FBDelete()

        oldMoviePlane.FBDelete()


def setFaceMoviePlaneVisibility(namespace, state):
    '''
    Set the visibility of the face movie plane for the supplied namespace.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Arguments:*
        * ``namespace`` The namespace of the character.
        * ``state`` Visibility state boolean.

    *Returns:*
        * ``None``
    '''

    moviePlane = getFaceMoviePlane(namespace)

    if moviePlane:
        moviePlane.Visibility = state


def offsetFaceMoviePlane(namespace, offsetAmount):
    '''
    Offset the face movie plane for the supplied namespace.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Arguments:*
        * ``namespace`` The namespace of the character.
        * ``offsetAmount`` The amount to offset the movie plane.

    *Returns:*
        * ``None``
    '''

    moviePlane = getFaceMoviePlane(namespace)

    if moviePlane:
        raise NotImplementedError, 'This function is not yet working.'


def scaleFaceMoviePlane(namespace, scaleX, scaleY):
    '''
    Scale the face movie plane for the supplied namespace.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Arguments:*
        * ``namespace`` The namespace of the character.
        * ``scaleX`` Height to scale.
        * ``scaleY`` Width to scale.

    *Returns:*
        * ``None``
    '''

    moviePlane = getFaceMoviePlane(namespace)

    if moviePlane:
        raise NotImplementedError, 'This function is not yet working.'


def createFaceMoviePlane(namespace, scaleFactor=0.175, offset=-0.25):
    '''
    Creates a face movie plane and aligns it for the supplied namespace.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Arguments:*
        * ``namespace`` The namespace of the character.

    *Keyword Arguments:*
        * ``scaleFactor`` Scale of the movie plane.
        * ``offset`` How far to offset the movie plane.

    *Returns:*
        * ``None``
    '''
    global MOVIE_PLANE_OBJ_NAME
    global MOVIE_PLANE_MAT_NAME

    moviePlaneModel = FBFindModelByLabelName('{0}:{1}'.format(namespace, MOVIE_PLANE_OBJ_NAME))
    if moviePlaneModel:
        moviePlaneModel.FBDelete()

    facialRoot = None
    facialRootNameList = ["FACIAL_facialRoot", "FACIAL_C_FacialRoot"]
    for fRoot in facialRootNameList:
        facialRoot = FBFindModelByLabelName('{0}:{1}'.format(namespace, fRoot))
        if facialRoot is not None:
            break
        # It should be safe at this point to create the face movie plane.
    if facialRoot:
        print facialRoot.Name
        # Create new movie plane.
        moviePlane = FBModelPlane('{0}:{1}'.format(namespace, MOVIE_PLANE_OBJ_NAME))
        moviePlane.Show = True
        # Assign movie plane images.
        moviePlane.Scaling = FBVector3d(0.10, 0.15, 0.15)
        moviePlane.Rotation = FBVector3d(90, 0, 0)
        moviePlane.PropertyList.Find('Lcl Translation').Data = FBVector3d(-25, 170, 0)
        moviePlane.Parent = facialRoot


def getRootFaceGUIControls(charNamespace):
    '''
    Get the root nulls for the face gui controls.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Arguments:*
        * ``charNamespace`` The namespace of the character.

    *Returns:*
        * ``List`` A list of all the root face controls.
    '''

    faceControls_OFF = FBFindModelByLabelName('{0}:faceControls_OFF'.format(charNamespace))
    facialAttrGui = FBFindModelByLabelName('{0}:FacialAttrGUI'.format(charNamespace))
    textNormals = FBFindModelByLabelName('{0}:TEXT_Normals'.format(charNamespace))

    return (faceControls_OFF, facialAttrGui, textNormals)


def createFaceGUIControlsCamera(charNamespace):
    '''
    Creates a camera for the face gui controls for the supplied character namespace.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Arguments:*
        * ``charNamespace`` The namespace of the character.

    *Returns:*
        * ``None``
    '''

    faceControlsCam = FBFindModelByLabelName(charNamespace + ':FaceControlsCam')

    if faceControlsCam:
        faceControlsCam.FBDelete()

    faceControlsCam = FBCamera(charNamespace + ':FaceControlsCam')
    faceControlsCam.Type = FBCameraType.kFBCameraTypeOrthogonal
    faceControlsCam.PropertyList.Find('Near Plane').Data = 0.0
    faceControlsCam.Use2DMagnifier = True
    faceControlsCam.MagnifierZoom = 915.0
    faceControlsCam.MagnifierPosX = -1.75
    faceControlsCam.MagnifierPosY = -0.35

    if faceControlsCam:

        # Get face control roots
        faceControls_OFF, facialAttrGui, textNormals = getRootFaceGUIControls(charNamespace)

        # Move camera into place
        faceControls_OFF_pos = faceControls_OFF.PropertyList.Find('Lcl Translation').Data

        faceControlsCam.Rotation.Data = FBVector3d(0, 90, 0)
        faceControlsCam.Translation = faceControls_OFF_pos - FBVector3d(-12.5, -5, -10)

    else:
        FBMessageBox('Rockstar', 'Could not create a face controls camera!', 'OK')


def moveAllFaceGUIControlsOffscreen(x=0.0, y=-10000.0, z=0.0, spaceBetweenControls=50.0):
    '''
    Moves all of the GUI controls for face rigs off-screen and spaced evenly apart.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Keyword Arguments:*
        * ``x`` The starting world x position.
        * ``y`` The starting world y position.
        * ``z`` The starting world z position.
        * ``spaceBetweenControls`` The amount of space between each set of face controls.

    *Returns:*
        * ``None``
    '''

    # get face references
    manager = Manager.Manager()
    characterReferenceList = manager.GetReferenceListByType(Character)

    if len(characterReferenceList) == 0:
        QtGui.QMessageBox.information('Face Reference',
                                      'Could not move face controls because there are no referenced characters in the scene!',
                                      QtGui.QMessageBox.Ok)
        return

    faceControlsWidth = 50.0
    # Iterate over each character reference.
    for charRef in characterReferenceList:
        faceControls_OFF, facialAttrGui, textNormals = getRootFaceGUIControls(charRef.Namespace)

        if all([faceControls_OFF, facialAttrGui, textNormals]):
                # Delete any keys on each root.
            if facialAttrGui.AnimationNode.FCurve:
                facialAttrGui.AnimationNode.FCurve.EditClear()

            if faceControls_OFF.AnimationNode.FCurve:
                faceControls_OFF.AnimationNode.FCurve.EditClear()

            if textNormals.AnimationNode.FCurve:
                textNormals.AnimationNode.FCurve.EditClear()

            # Record current positions, so that we can offset them from their new world position.
            faceControls_OFF_pos = faceControls_OFF.PropertyList.Find('Lcl Translation').Data
            facialAttrGui_pos = facialAttrGui.PropertyList.Find('Lcl Translation').Data
            textNormals_pos = textNormals.PropertyList.Find('Lcl Translation').Data

            # Virtual world position for the face controls.
            newFaceControlWorldPos = FBVector3d(x, y, z)

            # Re-position each gui controls root.
            faceControls_OFF.PropertyList.Find('Lcl Translation').Data = newFaceControlWorldPos
            facialAttrGui.PropertyList.Find('Lcl Translation').Data = newFaceControlWorldPos + (facialAttrGui_pos - faceControls_OFF_pos)
            textNormals.PropertyList.Find('Lcl Translation').Data = newFaceControlWorldPos + (textNormals_pos - faceControls_OFF_pos)

            # Create a camera for the face controls.
            createFaceGUIControlsCamera(charRef.Namespace)

            FBSystem().Scene.Evaluate()

            x += faceControlsWidth + spaceBetweenControls


def collectAllAmbientFacialRigRoots():
    '''
    Finds and returns the root model nulls for ambient facial rigs in the scene.

    *Author:*
        * Jason Hayes <jason.hayes@rockstarsandiego.com>

    *Arguments:*
        * ``None``

    *Returns:*
        * ``FBComponentList``  All of the *:Ambient_UI controls.
    '''

    modelList = FBComponentList()

    FBFindObjectsByName('*:Ambient_UI', modelList, True, True)
    return modelList


def FixFaceInCutscene(quiet=False):
    '''
    Attempts to fix broken facial due to constraints, in cutscenes.

    *Author:*
        * Thanh Phan <thanh.phan@rockstarsandiego.com>

    *Arguments:*
        *

    *Keyword Arguments:*
        *

    *Returns:*
        * ``None``
    '''

    lSystem = FBSystem()
    lPlayerControl = FBPlayerControl()


    # Gather Scene Time Info

    CURRENT_LAYER = lSystem.CurrentTake.GetCurrentLayer()
    lSystem.CurrentTake.SetCurrentLayer(0)
    START_FRAME = lPlayerControl.LoopStart.GetFrame()
    CURRENT_FRAME = lSystem.LocalTime.GetFrame()

    START_TIME = FBTime(0, 0, 0, START_FRAME)
    CURRENT_TIME = FBTime(0, 0, 0, CURRENT_FRAME)
    ZERO_TIME = FBTime(0, 0, 0, 0)

    # Set time and cursor to frame 0.
    lPlayerControl.LoopStart = ZERO_TIME
    lPlayerControl.Goto(ZERO_TIME)

    #------ Do the work.
    lControllerList = (Globals.gMouthCtrls + Globals.gBrowCtrls + Globals.gEyeCtrls + Globals.gBlushCtrl)
    lAllControls = FBComponentList()

    for each in lControllerList:
        FBFindObjectsByName(each, lAllControls, False, True)

    for each in lAllControls :
        each.Translation.Data = FBVector3d(0, 0, 0)
        each.Translation.Key()
        each.Rotation.Data = FBVector3d(0, 0, 0)
        each.Rotation.Key()
        FBSystem().Scene.Evaluate() # Needed to add a refresh after each or the faces weren't updating on FileOpen Callback
    #------

    #
    # Stupid hack to fix random non-finite jaw values
    #
    #

    ml = FBComponentList()
    FBFindObjectsByName("FACIAL_jaw", ml, False, True)
    for jawNode in ml:
        print jawNode.LongName
        for i in [0, 1, 2]:
            if jawNode.Translation.Data[i] > 999999999999999999999:
                jawNode.Translation.Data = FBVector3d(1.86865e-008, 0.015, -0.015)
    #
    #
    # END of Stupid hack
    #

    # Set time and cursor back to original frames.
    lPlayerControl.Goto(CURRENT_TIME)
    lPlayerControl.LoopStart = START_TIME

    lSystem.CurrentTake.SetCurrentLayer(CURRENT_LAYER)

    if not quiet:
        FBMessageBox("Fix Face In Cutscene", "Done!", "OK")

    return None

def SetKeyValue(pTime, pValue, pAnimationNode):
    if len(pAnimationNode.Nodes) == 0:
        pAnimationNode.KeyAdd(pTime, pValue)
    else:
        for lNode in pAnimationNode.Nodes:
            SetKeyValue(pTime, pValue, lNode)

def SetKey(pTime, pAnimationNode):
    if len(pAnimationNode.Nodes) == 0:
        if pAnimationNode.FCurve != None:
            pAnimationNode.FCurve.KeyInsert(pTime)
        else:
            print "Unable to insert key for node."
    else:
        for lNode in pAnimationNode.Nodes:
            SetKey(pTime, lNode)

def headLean (lean_offset=0):
    '''
    Adjusts the neck to a more natural position, from that of stance pose.
    '''

    neckRoot = ["SKEL_Neck0"]
    neckBones = ["SKEL_Neck1", "SKEL_Neck2", "SKEL_Head"]

    boneList1 = FBComponentList()
    boneList2 = FBComponentList()

    for boneName in neckRoot:
        FBFindObjectsByName(boneName, boneList1, False, True)
    for boneName in neckBones:
        FBFindObjectsByName(boneName, boneList2, False, True)

    # ---------------
    # Error catching:
    #
    if len(boneList1) > 1:
        FBMessageBox(
            "Error",
            "The neck lean fix will not work if there is more than one character in the scene.\nCould not perform head lean fix.",
            "OK")
        return
    elif len(boneList1) < 1:
        FBMessageBox(
            "Error",
            "Could not find correct character rig for neck lean fix.",
            "OK")
        return
    if len(boneList2) != 3:
        FBMessageBox(
            "Error",
            "Could not find correct character rig for neck lean fix.",
            "OK")
        return
    #
    #
    # ----------------

    init_rot = (3 * (boneList2[0].Rotation.Data[2])) + (boneList1[0].Rotation.Data[2])
    lean_value = (-1 * init_rot) + (lean_offset)
    neckRot1 = FBVector3d(0, 0, ((init_rot) + (lean_value)))
    neckRot2 = FBVector3d(0, 0, (-1 * lean_value / 3))

    for bone in boneList1:
        bone.Rotation.Data = neckRot1
        SetKey(FBTime(0), bone.AnimationNode)

    for bone in boneList2:
        bone.Rotation.Data = neckRot2
        SetKey(FBTime(0), bone.AnimationNode)


def SetupFacialFile(faceFile=None, syncTxt=True, ulog=None):
    '''
    Sets up facial file for facial animator to begin working.
    Prompts user to select a character rig, and the tool performs series of actions.

    *Author:*
        * Thanh Phan <thanh.phan@rockstarsandiego.com>

    *Arguments:*
        *

    *Keyword Arguments:*
        *

    *Returns:*
        * ``None``
    '''

    if ulog:
        ulog.LogMessage("Start SetupFacialFile");
    # Create the popup and set necessary initial values.
    gProjRoot = Config.Project.Path.Root
    lFilePopup = FBFilePopup()
    lFilePopup.Caption = "Select a Character"
    lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen

    # BUG: If we do not set the filter, we will have an exception.
    lFilePopup.Filter = "*.fbx"

    # Set the default path.
    lFilePopup.Path = str(gProjRoot + "\\art\\animation\\resources\\characters\\Models")
    # Get the GUI to show.
    lFilePopupGUI = None
    if faceFile == None:
        lFilePopupGUI = lFilePopup.Execute()
    else:
        lFilePopupGUI = True

    if lFilePopupGUI:
        gProjUrl = "//" + gProjRoot.split("\\")[1]

        lName = None
        if faceFile == None:
            lName = lFilePopup.FileName.split(".")[0]
            faceFile = lFilePopup.FullFilename
        else:
            lName = faceFile.split("\\")[len(faceFile.split("\\")) - 1].split(".")[0]


        Globals.App.FileNew()

    if faceFile is not None:

        if ulog:
            ulog.LogMessage("Creating references");
        manager = Manager.Manager()

        filePath = str(os.path.normpath(faceFile))
        referenceList = manager.CreateReferences(filePath)

        if ulog:
            ulog.LogMessage("Reloading textures");
        manager.ReloadTextures(referenceList)

        for char in Globals.Characters:
            char.ActiveInput = False

        for expr in Globals.Constraints:
            if "_scale" in expr.Name.lower():
                expr.Active = False

        if ulog:
            ulog.LogMessage("fixFacialGroups");

        faceToolbox.fixFacialGroups(lName)

        # AddFacewareDevice( lFilePopup.FileName )
        try:
            lTC = FBCreateObject("Browsing/Templates/Devices", "Faceware Retargeter", "Faceware Retargeter")
        except RuntimeError:
            lTC = None
            if ulog:
                ulog.LogMessage("Faceware Retargeter not found. Skipping FW device...")

        if lTC:
            Globals.gScene.Devices.append(lTC)
            lTC.Online = True

        if ulog:
            ulog.LogMessage("headLean");

        # Adjust Head Lean
        headLean()

        namespace = referenceList[0].Namespace
        createFaceMoviePlane(namespace)

        if ulog:
            ulog.LogMessage("Cameras")

        # Adjust Camera
        for each in FBSystem().Scene.Cameras:
            if "Perspective" in each.Name:
                each.Translation.Data = FBVector3d(13, 162, 100)
            each.Rotation.Data = FBVector3d(0, 90, 0)
            each.PropertyList.Find("Nearplane").Data = 0.01
            each.PropertyList.Find("FieldOfView").Data = 30
            FBSystem().Scene.Evaluate()

        if ulog:
            ulog.LogMessage("Set Animatable for all CTRLs")
        # Set Animatable for all CTRLs
        cl = FBComponentList()
        FBFindObjectsByName("CTRL_*", cl, False, True)
        if cl:
            for ctrl in cl:
                ctrl.Translation.SetAnimated(True)
            ctrl.Rotation.SetAnimated(True)

        if ulog:
            ulog.LogMessage("fixAllScale")
        FixFaceScale.fixAllScale()

    if ulog:
        ulog.LogMessage("End SetupFacialFile")

    return None


def switch_CTL_CTRL ():

    for each in FBSystem().Scene.Components:
        if isinstance(each, FBModel):
            if "CTRL" in each.Name:
                each.Name = each.Name.replace("CTRL", "CTL")
            elif "CTL" in each.Name:
                each.Name = each.Name.replace("CTL", "CTRL")


def switch_Raise_Lift ():

    ml = FBComponentList()
    FBFindObjectsByName("FRM_chinRaise", ml, False, True)

    if len(ml) != 0:
        print "Raise > Lift"

        cl = FBComponentList()
        FBFindObjectsByName("*chinRaise*", cl, False, True)
        FBFindObjectsByName("*ChinRaise*", cl, False, True)
        for each in cl:
            each.Name = each.Name.replace("chinRaise", "chinLift")
            each.Name = each.Name.replace("ChinRaise", "ChinLift")
            print "{0} was renamed.".format(each.Name)

        lookAtCtrls = FBComponentList()
        FBFindObjectsByName("LOC_L_AimTarget", lookAtCtrls, False, True)
        FBFindObjectsByName("LOC_R_AimTarget", lookAtCtrls, False, True)
        for each in lookAtCtrls:
            each.Name = each.Name.replace("AimTarget", "lookAt")
            each.Name = each.Name.replace("LOC", "CTRL")

        FBMessageBox("v11", "Rig converted to v12.\nChinRaise was renamed to ChinLift.", "OK")

    else:

        ml = FBComponentList()
        FBFindObjectsByName("FRM_chinLift", ml, False, True)

        if len(ml) != 0:
            print "Lift > Raise"

            cl = FBComponentList()
            FBFindObjectsByName("*chinLift*", cl, False, True)
            FBFindObjectsByName("*ChinLift*", cl, False, True)
            for each in cl:
                each.Name = each.Name.replace("chinLift", "chinRaise")
                each.Name = each.Name.replace("ChinLift", "ChinRaise")
                print "{0} was renamed.".format(each.Name)

            lookAtCtrls = FBComponentList()
            FBFindObjectsByName("CTRL_L_lookAt", lookAtCtrls, False, True)
            FBFindObjectsByName("CTRL_R_lookAt", lookAtCtrls, False, True)
            for each in lookAtCtrls:
                each.Name = each.Name.replace("lookAt", "AimTarget")
                each.Name = each.Name.replace("CTRL", "LOC")

            FBMessageBox("v12", "Rig converted to v11.\nChinLift was renamed to ChinRaise.", "OK")

def clearFacialPreRotations(lNamespace):
    cl = FBComponentList()
    FBFindObjectsByName("{0}:FACIAL_*".format(lNamespace), cl, True, True)

    for each in cl:
        each.PreRotation = FBVector3d(0, 0, 0)


def selectUFCNamespace(lNamespace):

    dll_Character = NamespaceFN.validateNamespace(lNamespace)

    try:
        print "Found {0}, {1}".format(lNamespace, dll_Character)
        for dev in Globals.Devices:
            if dev.LongName.split(":")[0] == lNamespace:
                if dev.ClassName() == "RSExprSolverDevice":
                    # The order in witch we call the device functions is essential,
                    # if we don't respect it pop-up errors regarding hash and bone mismatch appear
                    exec 'cdll.MBExprSolver.SetNamespace_Py("{0}")'.format(lNamespace)
                    exec 'cdll.MBExprSolver.SetOnline_Py( True )'
                    exec 'cdll.MBExprSolver.SetActiveDevice_Py("{0}")'.format(dev.LongName)
                    # Function seems to break face with the New Ref system- doesn't seem needed in old ref system either
                    # clearFacialPreRotations( lNamespace )
                else:
                    exec 'cdll.UFCRig_{0}_{1}_x64.SetNamespace_Py("{2}")'.format(dll_Character, Config.Script.TargetBuild, lNamespace)
                    exec 'cdll.UFCRig_{0}_{1}_x64.SetOnline_Py(True)'.format(dll_Character, Config.Script.TargetBuild, lNamespace)
                    exec 'cdll.UFCRig_{0}_{1}_x64.SetLive_Py(True)'.format(dll_Character, Config.Script.TargetBuild, lNamespace)
                    exec 'cdll.UFCRig_{0}_{1}_x64.SetLive_Py(True)'.format(dll_Character, Config.Script.TargetBuild, lNamespace)
    except:
        pass

# This reconnects the UFC Relation Constraint boxes that break when a namespace is added during referencing.
def connectUFCRelationBoxes():

    # Look for all relation constraints in scene
    for relation in [c for c in FBSystem().Scene.Constraints if c.Is(FBConstraintRelation_TypeInfo()) ]:

        # Match this relation constraint
        if relation.Name == "setup_relationConstraint" or relation.Name == "CTRL_lookAt_WorldParentSwitch_Relation":

            # Init the input and output vars
            head_in = None
            head_out = None
            lookAt_in = None
            lookAt_out = None

            # Find all the relation Boxes we need
            addBoxes = [ box for box in relation.Boxes if "Add (a + b)" in box.Name ]
            multBoxes = [ box for box in relation.Boxes if "Multiply (a x b)" in box.Name ]
            targetBoxes = [ box for box in relation.Boxes if "GRP_C_lookAt_parentConstraint" in box.Name ]

            # Find the outputs
            for addBox in addBoxes:
                if addBox.AnimationNodeOutGet().Nodes[0].GetDstCount() == 0:
                    lookAt_out = addBox.AnimationNodeOutGet().Nodes[0]

            for multBox in multBoxes:
                if multBox.AnimationNodeOutGet().Nodes[0].GetDstCount() == 0:
                    head_out = multBox.AnimationNodeOutGet().Nodes[0]

            # Find the inputs
            for targetBox in targetBoxes:
                for node in targetBox.AnimationNodeInGet().Nodes:
                    if "head" in node.Name:
                        head_in = node
                    elif "OH_LookAtRoot" in node.Name:
                        lookAt_in = node

            # Connect !
            if head_in != None and head_out != None:
                FBConnect(head_out, head_in)

            if lookAt_in != None and lookAt_out != None:
                FBConnect(lookAt_out, lookAt_in)

            print "Connections reset for {0}".format(relation.LongName.split(":")[0])


def fixFacial_UFC(char):
    if char:
        connectUFCRelationBoxes()
        for device in Globals.Devices:
            if char.Namespace in device.LongName:
                selectUFCNamespace(char.Namespace)
                break

def fixFace_UFC(lNamespace):
    if lNamespace:
        connectUFCRelationBoxes()
        for device in Globals.Devices:
            if lNamespace in device.LongName:
                selectUFCNamespace(lNamespace)
                break



def removeFacialStuff(lNamespace):

    deleteList = FBComponentList()

    if lNamespace:

        faceBoneRoots = [
            "FACIAL_C_FacialRoot",
            "FACIAL_C_NeckRoot",
            "FACIAL_facialRoot"
        ]
        faceConstraints = [
            "3lateral",
            "viseme",
            "_lookat",
            "_eyedriver",
            "_convergencegui",
            "setup_relationconstraint"
        ]

        # Remove all child face bones
        for faceRootName in faceBoneRoots:
            faceRoot = FBFindModelByLabelName("{0}:{1}".format(lNamespace, faceRootName))
            if faceRoot:
                faceBones = findAllChildren(faceRoot, nodeList=[], includeParent=True)
                if len(faceBones) > 0:
                    for bone in faceBones:
                        if "FACIAL_" in bone.Name:
                            if bone not in deleteList:
                                deleteList.append(bone)

        # Remove all face constraints
        for constraint in Globals.Constraints:
            if constraint.LongName.split(":")[0] == lNamespace:
                for constrName in faceConstraints:
                    if constrName in constraint.Name.lower():
                        if constraint not in deleteList:
                            deleteList.append(constraint)

        # Remove all face devices
        for dev in Globals.Devices:
            if dev.LongName.split(":")[0] == lNamespace:
                if dev not in deleteList:
                    deleteList.append(dev)

        for delItem in deleteList:
            delItem.FBDelete()

def setSamplingRate_UFC (sampleRate, dll_Character):
    try:
        exec 'cdll.UFCRig_{0}_{1}_x64.SetRate_Py({2})'.format(dll_Character, Config.Script.TargetBuild, sampleRate)
    except:
        pass


def setAllSamplingRate_UFC (sampleRate):

    manager = Manager.Manager()

    characters = manager.GetReferenceListByType(Character)

    for each in characters:
        lName = NamespaceFN.validateNamespace(each.Namespace)
        setSamplingRate_UFC(sampleRate, lName)

def setSingleSamplingRate_UFC (sampleRate, lNamespace):
    lName = NamespaceFN.validateNamespace(lNamespace)
    setSamplingRate_UFC(sampleRate, lName)

def tagAnalogs (rootNode):

    # Figure out Namespace
    rootPrefix = ""
    if len(rootNode.LongName.split(":")) > 1:
        rootPrefix = "{0}:".format(rootNode.LongName.split(":")[0])

    # Trans AND Rot
    for ctrl in Globals.gUFCFacial_AnalogCtrls_TR:
        foundCtrl = FBFindModelByLabelName( "{0}{1}".format(rootPrefix, ctrl) )
        if foundCtrl:
            cdll.rexmbrage.AddAdditionalModelEntry_Py("{0}".format(rootNode.LongName),
                                                      "{0}{1}".format(rootPrefix, ctrl),
                                                      "facialRotation", "{0}".format(ctrl))

    # Trans Only
    for ctrl in Globals.gUFCFacial_AnalogCtrls_T:
        foundCtrl = FBFindModelByLabelName( "{0}{1}".format(rootPrefix, ctrl) )
        if foundCtrl:
            cdll.rexmbrage.AddAdditionalModelEntry_Py("{0}".format(rootNode.LongName),
                                                      "{0}{1}".format(rootPrefix, ctrl),
                                                      "facialTranslation", "{0}".format(ctrl))


def tagAllAnalogs(rootNodeName):

    allRigs = FBComponentList()
    FBFindObjectsByName("*{0}".format(rootNodeName), allRigs, True, True)
    for rootNode in allRigs:
        tagAnalogs(rootNode)


def tagEarAnalogs(rootNodeName):

    allRigs = FBComponentList()
    FBFindObjectsByName("*{0}".format(rootNodeName), allRigs, True, True)
    for rootNode in allRigs:

        # Figure out Namespace
        rootPrefix = ""
        if len(rootNode.LongName.split(":")) > 1:
            rootPrefix = "{0}:".format(rootNode.LongName.split(":")[0])

        # Trans AND Rot
        for ctrl in Globals.gUFCFacial_AnalogCtrls_Ears_TR:
            foundCtrl = FBFindModelByLabelName( "{0}{1}".format(rootPrefix, ctrl) )
            if foundCtrl:
                cdll.rexmbrage.AddAdditionalModelEntry_Py("{0}".format(rootNode.LongName),
                                                          "{0}{1}".format(rootPrefix, ctrl),
                                                          "facialRotation", "{0}".format(ctrl))


def tagSmallAnimalFace (rootNode):
    smallAnimal_List = ["CTRL_C_jaw",
                        "CTRL_R_mouth_upperLipRaise",
                        "CTRL_gils"]

    smallAnimal_SpecList = ["CREATURES_FISH_FullBody_Spec.xml"]

    # Figure out Namespace
    rootPrefix = ""
    if len(rootNode.LongName.split(":")) > 1:
        rootPrefix = "{0}:".format(rootNode.LongName.split(":")[0])

    # Trans Only
    for ctrl in smallAnimal_List:
        foundCtrl = FBFindModelByLabelName( "{0}{1}".format(rootPrefix, ctrl) )
        if foundCtrl:
            cdll.rexmbrage.AddAdditionalModelEntry_Py("{0}".format(rootNode.LongName),
                                                      "{0}{1}".format(rootPrefix, ctrl),
                                                      "facialTranslation", "{0}".format(ctrl))


def tagAllSmallAnimalFace(rootNodeName):
    allRigs = FBComponentList()
    FBFindObjectsByName("*{0}".format(rootNodeName), allRigs, True, True)
    for rootNode in allRigs:
        tagSmallAnimalFace(rootNode)


def tagSmallAnimalFace_Selected():
    ml = FBModelList()
    FBGetSelectedModels(ml)

    if len(ml) == 1:
        lNamespace = ml[0].LongName.split(":")[0]
        foundCtrl = FBFindModelByLabelName("{0}:{1}".format( lNamespace, "SKEL_ROOT"))
        if foundCtrl:
            tagSmallAnimalFace(foundCtrl)


def removeAllFacialTags(rootNodeName):
    pass

def shiftKeys(time, node):
    fcurve = node.FCurve
    if fcurve:
        if len(fcurve.Keys) > 0 :
            for key in fcurve.Keys:
                key.Time += time

    for child in node.Nodes:
        shiftKeys(time, child)


def shiftAllKeys(amount):
    allCtrls = FBComponentList()
    FBFindObjectsByName("CTRL_*", allCtrls, False, True)
    for ctrl in allCtrls:
        shiftKeys(FBTime(0, 0, 0, amount), ctrl.AnimationNode)


def copyTakeInfo():

    startingTake = FBSystem().CurrentTake

    openDialog = FBFilePopup()
    openDialog.Caption = "Choose an FBX to pull take information from..."
    openDialog.Style = FBFilePopupStyle.kFBFilePopupOpen
    openDialog.Filter = "*.fbx"

    openResult = openDialog.Execute()

    if openResult:
        fbxFilename = '{0}\\{1}'.format(openDialog.Path, openDialog.FileName)

        currentTakeNames = []
        for each in FBSystem().Scene.Takes:
            currentTakeNames.append(each.Name)
            print each.ClassName()

        faceAnimFileObj = AnimImporter.fbxGetAnimationFromFile(fbxFilename)
        if faceAnimFileObj:
            for takeName in faceAnimFileObj.takes:
                # print take
                lStartFrame = FBTime(0, 0, 0, faceAnimFileObj.takes[ takeName ].startFrame)
                lEndFrame = FBTime(0, 0, 0, faceAnimFileObj.takes[ takeName ].endFrame)
                # Create Take
                if takeName not in currentTakeNames:
                    print "Creating new take:   {0}".format(takeName)
                    newTake = FBSystem().CurrentTake.CopyTake(takeName)
                else:
                    print "Take already exists: {0}".format(takeName)

                # set Frame Range
                for take in FBSystem().Scene.Takes:
                    if take.Name == takeName:
                        print "Setting frame range: {0}".format(takeName)
                        take.LocalTimeSpan = FBTimeSpan(lStartFrame, lEndFrame)

    FBSystem().CurrentTake = startingTake
    FBMessageBox('Copy Take Info', "All takes from target fbx now exist in this scene.\nAny new takes created, has animation copied from this current take.\nAny existing takes have had their frame ranges set to match the target fbx.", 'OK')


#
#
# 3L Test Player Setup Scripts
#
#

def setupCams():
    for cam in Globals.Cameras:
        if "Perspective" in cam.Name:

            for prop in cam.PropertyList:
                if "field" in prop.Name.lower():
                    print prop.Name

            cam.PropertyList.Find("NearPlane").Data = .01
            cam.PropertyList.Find("FieldOfView").Data = 30

def addToGroup (lList, lName, lShow=True, lPick=True, lTrans=True) :
    if len(lList) > 0:
        lFolder = None

        for grp in Globals.Groups:
            if grp.Name == lName :
                lFolder = grp

        if lFolder == None :
            if ":" in lList[0].LongName:
                lNamespace = lList[0].LongName.split(":")[0]
                lName = "{0}:{1}".format(lNamespace, lName)
            lFolder = FBGroup(lName)

        for item in lList:
            if isinstance(item, FBModel):
                lFolder.ConnectSrc(item)

        lFolder.Show = lShow
        lFolder.Pickable = lPick
        lFolder.Transformable = lTrans

def findItems (lStrList, searchNamespace=True, searchModelsOnly=True) :
    cl = FBComponentList()
    for item in lStrList:
        lStr = "*{0}*".format(item)
        FBFindObjectsByName(lStr, cl, searchNamespace, searchModelsOnly)
    return cl


def findAllChildren(rootNode, nodeList=[], includeParent=True):
    if rootNode != None:
        if includeParent:
            nodeList.append(rootNode)

        for each in rootNode.Children:
            nodeList.append(each)

            findAllChildren(each, nodeList, False)
    return nodeList


# Find all facial components
def setupGroups_Master () :
    guiList = []
    lChildren = findAllChildren(FBFindModelByLabelName("headGui"))
    guiList = guiList + lChildren
    lChildren = findAllChildren(FBFindModelByLabelName("FACIAL_C_NeckRoot"))
    guiList = guiList + lChildren
    lChildren = findAllChildren(FBFindModelByLabelName("FACIAL_C_FacialRoot"))
    guiList = guiList + lChildren

    # Clear facial components from all groups


    allGroups = FBSystem().Scene.Groups
    for group in allGroups:
        removeItems = []
        for item in group.Items:
            if item in guiList:
                removeItems.append(item)
        for delItem in removeItems:
            group.Items.remove(delItem)

    # Define all facial groups

    lApp = FBApplication().FBXFileName
    lRootGroup_Name = lApp.split("\\")[len(lApp.split("\\")) - 1].split(".")[0]

    lRootGroup = None
    lFacialRootGroup_Name = "Facial"

    lFacialGroup_Name = "Facial - Main"
    lCtrlGroup_Name = "Controllers"
    lGuiGroup_Name = "GUI Elements"

    lFacialGroupEyes_Name = "Facial - Global Eyes"
    lCtrlGroupEyes_Name = "Controllers - Global Eyes"
    lGuiGroupEyes_Name = "GUI Elements - Global Eyes"

    lBonesGroup_Name = "Facial Bones"
    lGeoGroup_Name = "Geo"
    lGeoHairGroup_Name = "Geo - Facial Hair"
    lOtherGroup_Name = "Other (Unsorted)"

    # Remove all facial Groups (and find the root groups)

    groupsToDel = []
    for group in allGroups:
        if group.Name == lFacialGroup_Name:
            groupsToDel.append(group)
        if group.Name == lCtrlGroup_Name:
            groupsToDel.append(group)
        if group.Name == lGuiGroup_Name:
            groupsToDel.append(group)
        if group.Name == lOtherGroup_Name:
            groupsToDel.append(group)
        if group.Name == lBonesGroup_Name:
            groupsToDel.append(group)
        if group.Name == lGeoGroup_Name:
            groupsToDel.append(group)
        if group.Name == lGeoHairGroup_Name:
            groupsToDel.append(group)
        if group.Name == lFacialRootGroup_Name:
            groupsToDel.append(group)
        if group.Name == lFacialGroupEyes_Name:
            groupsToDel.append(group)
        if group.Name == lCtrlGroupEyes_Name:
            groupsToDel.append(group)
        if group.Name == lGuiGroupEyes_Name:
            groupsToDel.append(group)

        if group.Name == lRootGroup_Name:
            lRootGroup = group

    for item in groupsToDel:
        item.FBDelete()

    # Create all facial Groups

    lFacialRootGroup = FBGroup(lFacialRootGroup_Name)

    lFacialGroup = FBGroup(lFacialGroup_Name)
    lCtrlGroup = FBGroup(lCtrlGroup_Name)
    lGuiGroup = FBGroup(lGuiGroup_Name)

    lFacialGroupEyes = FBGroup(lFacialGroupEyes_Name)
    lCtrlGroupEyes = FBGroup(lCtrlGroupEyes_Name)
    lGuiGroupEyes = FBGroup(lGuiGroupEyes_Name)

    lBonesGroup = FBGroup(lBonesGroup_Name)
    lGeoGroup = FBGroup(lGeoGroup_Name)
    lGeoHairGroup = FBGroup(lGeoHairGroup_Name)
    lOtherGroup = FBGroup(lOtherGroup_Name)

    # Parent the groups
    lFacialRootGroup.ConnectSrc(lFacialGroup)
    lFacialGroup.ConnectSrc(lCtrlGroup)
    lFacialGroup.ConnectSrc(lGuiGroup)
    lFacialRootGroup.ConnectSrc(lFacialGroupEyes)
    lFacialGroupEyes.ConnectSrc(lCtrlGroupEyes)
    lFacialGroupEyes.ConnectSrc(lGuiGroupEyes)
    if lRootGroup != None:
        lRootGroup.ConnectSrc(lFacialRootGroup)
        lRootGroup.ConnectSrc(lBonesGroup)
        lRootGroup.ConnectSrc(lGeoGroup)
        lRootGroup.ConnectSrc(lGeoHairGroup)
        lRootGroup.ConnectSrc(lOtherGroup)

    # Add components to group

    for each in guiList:

        if "mesh" == each.Name.split("_")[ len(each.Name.split("_")) - 1 ]:
            if each not in lGeoGroup.Items:
                lGeoGroup.Items.append(each)
        elif "CTRL_faceGUI" == each.Name:
            if each not in lGuiGroup.Items:
                lGuiGroup.Items.append(each)
        elif "CTRL_rig_Logic" == each.Name:
            if each not in lGuiGroup.Items:
                lGuiGroup.Items.append(each)
        elif "CTRL_R_neck_stretch_loc" == each.Name or "CTRL_L_neck_stretch_loc" == each.Name:
            if each not in lOtherGroup.Items:
                lOtherGroup.Items.append(each)
        elif "ctrl_l_lookat" == each.Name.lower() or "ctrl_r_lookat" == each.Name.lower() :
            if each not in lCtrlGroupEyes.Items:
                lCtrlGroupEyes.Items.append(each)
        elif "CTRL_convergenceSwitch" == each.Name or "ctrl_c_lookat" == each.Name.lower() :
            if each not in lCtrlGroupEyes.Items:
                lCtrlGroupEyes.Items.append(each)
        elif "CTRL" == each.Name.split("_")[0]:
            if each not in lCtrlGroup.Items:
                lCtrlGroup.Items.append(each)
        elif "FRM_convergenceGUI" == each.Name or "FRM_convergenceSwitch" == each.Name:
            if each not in lGuiGroupEyes.Items:
                lGuiGroupEyes.Items.append(each)
        elif "FRM" == each.Name.split("_")[0]:
            if each not in lGuiGroup.Items:
                lGuiGroup.Items.append(each)
        elif "TEXT_convergence" == each.Name:
            if each not in lGuiGroupEyes.Items:
                lGuiGroupEyes.Items.append(each)
        elif "TEXT" == each.Name.split("_")[0]:
            if each not in lGuiGroup.Items:
                lGuiGroup.Items.append(each)
        elif "FACIAL" == each.Name.split("_")[0]:
            if each not in lBonesGroup.Items:
                lBonesGroup.Items.append(each)
        else:
            if each not in lOtherGroup.Items:
                lOtherGroup.Items.append(each)
                print each.ClassName()

    # Initialize Group Settings
    lFacialRootGroup.Pickable = False
    lFacialRootGroup.Show = False
    lFacialRootGroup.Transformable = False
    lFacialRootGroup.Pickable = True
    lFacialRootGroup.Show = True
    lFacialRootGroup.Transformable = True

    # Setup Group Settings
    lGuiGroup.Pickable = False
    lGuiGroup.Transformable = False
    lGuiGroupEyes.Pickable = False
    lGuiGroupEyes.Transformable = False
    lFacialGroupEyes.Show = False
    lBonesGroup.Pickable = False
    lBonesGroup.Show = False
    lBonesGroup.Transformable = False
    lGeoGroup.Pickable = False
    lGeoGroup.Transformable = False
    lGeoHairGroup.Show = False
    lGeoHairGroup.Pickable = False
    lGeoHairGroup.Transformable = False
    lOtherGroup.Pickable = False
    lOtherGroup.Show = False
    lOtherGroup.Transformable = False

    print "Done."



def setupGroups_Master_OLD() :
    theList = findItems([ "CTRL", "CTL", "Controller" ])
    addToGroup(theList, "Controllers", True, True, True)

    theList = findItems([ "GRP", "FRM", "TEXT" ])
    addToGroup(theList, "GUI Objects", True, False, False)

    theList = findItems([ "eyeCap_mesh" ])
    addToGroup(theList, "MISC", False, False, False)

    theList = findItems([ "beard_mesh", "browsLash_mesh" ])
    addToGroup(theList, "Facial Hair", False, False, False)

    theList = findItems([ "eyes_mesh", "hair_mesh", "head_mesh", "innerMouth_mesh" ])
    addToGroup(theList, "Geo", True, False, False)

def setupGroups_Wolf() :
    theList = findItems([ "CTRL", "CTL", "Controller" ])
    addToGroup(theList, "Controllers", True, True, True)

    theList = findItems([ "GRP", "FRM", "TEXT" ])
    addToGroup(theList, "GUI Objects", True, False, False)

    theList = findItems([  ])
    addToGroup(theList, "MISC", False, False, False)

    theList = findItems([  ])
    addToGroup(theList, "Facial Hair", False, False, False)

    theList = findItems([ "head_mesh", "innerMouth_mesh" ])
    addToGroup(theList, "Geo", True, False, False)


# ---
#
# --- 3L Test Player Setup Scripts End
#
# ---



#
# Scripts for reducing eye animation to within 10%
#
#
#
def findEyeControl():
    cl = FBComponentList()
    FBFindObjectsByName("CTRL_*_eye", cl, False, True)
    for eyeCtrl in cl:
        print "Found eye control: {0}".format(eyeCtrl.LongName)
        maxValue = checkEyeControlScale(eyeCtrl)
        if maxValue > .1:
            reduceAnimation(eyeCtrl.Translation.GetAnimationNode(), maxValue)

def checkEyeControlScale(eyeCtrl):
    lMaxValue = 0
    eyeCtrl.Translation.SetAnimated(True)

    if hasattr(eyeCtrl.Translation, 'Nodes'):
        for key in eyeCtrl.Translation.GetAnimationNode().Nodes[0].FCurve.Keys:
            if abs(key.Value) > lMaxValue:
                lMaxValue = abs(key.Value)
        for key in eyeCtrl.Translation.GetAnimationNode().Nodes[1].FCurve.Keys:
            if abs(key.Value) > lMaxValue:
                lMaxValue = abs(key.Value)
        return lMaxValue

def reduceAnimation(eyeAnimNode, maxValue):
    shrinkFactor = .1 / maxValue
    for key in eyeAnimNode.Nodes[0].FCurve.Keys:
        # key.Value = key.Value * 0.1
        key.Value = key.Value * shrinkFactor

    for key in eyeAnimNode.Nodes[1].FCurve.Keys:
        # key.Value = key.Value * 0.1
        key.Value = key.Value * shrinkFactor

def reduceAllEyeAnimation():
    for take in FBSystem().Scene.Takes:
        FBSystem().CurrentTake = take
        findEyeControl()
#
#
#
# END -- Scripts for reducing eye animation to within 10%
#



#  This creates a single facial movieplane for each take.
def createMoviePlanesPerTake():

    # Loop through take names to generate movieplanes.
    for take in FBSystem().Scene.Takes:
        moviePlane_Name = "{0}{1}".format("MoviePlane_", take.Name)

        # If plane exists, assin, else make new one.
        lPlane = None
        foundMoviePlane = FBFindModelByLabelName(moviePlane_Name)
        if not foundMoviePlane:
            lPlane = FBModelPlane(moviePlane_Name)
        else:
            lPlane = foundMoviePlane

        # Re-position relative to SKEL_HEAD bone
        headBone = FBComponentList()
        FBFindObjectsByName("SKEL_Head", headBone, False, True)
        if headBone[0]:
            lPlane.Parent = headBone[0]
            lPlane.Translation.Data = FBVector3d(.01, 0, -.15)
            lPlane.Parent = None
            lPlane.Rotation.Data = FBVector3d(90, 0, 0)
            lPlane.Scaling.Data = FBVector3d(.07, .12, .11)

        else:
            print "No head bone found."


    # Look for all movieplanes
    allMoviePlanes = FBComponentList()
    FBFindObjectsByName("MoviePlane_*", allMoviePlanes, False, True)

    # Loop through each take and set visibility settings for the movieplanes
    for take in FBSystem().Scene.Takes:
        FBSystem().CurrentTake = take

        for mp in allMoviePlanes:
            mp.Visibility.SetAnimated(True)
            lCurve = mp.Visibility.GetAnimationNode().FCurve

            # If take name matches movieplane name, show it, else hide.
            if mp.Name.replace("MoviePlane_", "") == take.Name:
                mp.Show = True
                mp.Visibility = True
                lCurve.KeyAdd(FBTime(0), 1)
            else:
                mp.Show = True
                mp.Visibility = False
                lCurve.KeyAdd(FBTime(0), 0)



def reParentCustomGUI (  ):

    lApp = FBApplication()
    # Find Custom GUI
    cl = FBComponentList()
    FBFindObjectsByName( "CTRL_faceCustomizationGUI", cl, False, True )

    # If Found, find the head node, and share the parent.
    if len(cl) == 1:
        lParent = cl[0].Parent
        while lParent.Name != "head":
            if lParent.Parent != None:
                lParent = lParent.Parent
            else:
                break
        if lParent.Parent != None:
            if lParent.Name == "head":
                cl[0].Parent = lParent.Parent
                return "Re-parented Custom GUI"

    # Report Issues
    elif len(cl) > 1:
        return "Found too many custom GUIs?!"
    elif len(cl) == 0:
        return "No custom GUI."
    else:
        return "Already fixed.  GUI Parent = {0}".format( cl[0].Parent.Name )


# Recursively return list of all children
def addAllChildren( node, deleteList ):
    if node not in deleteList:
        deleteList.append( node )
    for child in node.Children:
        deleteList = addAllChildren( child, deleteList )
    return deleteList

# Delete dupe facial rigs.  Character namespace optional, otherwise, fixes all.
def deleteDupeFacialRigs( char = None ):

    # Gather old facial rigs
    oldRoots = FBComponentList()
    if char == None:
        FBFindObjectsByName( "FACIAL_FacialRoot", oldRoots, False, True )
        FBFindObjectsByName( "FACIAL_facialRoot", oldRoots, False, True )
    else:
        FBFindObjectsByName( char + ":FACIAL_FacialRoot", oldRoots, True, True )
        FBFindObjectsByName( char + ":FACIAL_facialRoot", oldRoots, True, True )

    # If a newer rig exists, add old rig to trash bin
    deleteMe = []
    for oldRoot in oldRoots:
        for b in oldRoot.Parent.Children:
            if b.Name.lower() == "facial_c_facialroot":
                deleteMe = deleteMe + addAllChildren( oldRoot, [] )

    # Take out the trash
    deleteByType = [
        "FBModelSkeleton",
        "FBModelNull",
        "FBModel"  ]
    for item in deleteMe:
        if item.ClassName() in deleteByType:
            #print "Deleting... {0}".format( item.LongName )
            item.FBDelete()


def getTalentFromFaceVideoPath(project, videoPath):
    """
    Get the TalentVariation from a video path given a project to search in

    args:
        project (AnimData._contexts.Project): The project to search
        videoPath (str): The input video path to find the Talent from

    return:
        a AnimData._context.TalentVariation object for the actor who the data was recorded for
    """
    # Copy Name
    trialName = os.path.splitext(os.path.basename(videoPath))[0]
    vidName = str(trialName)

    # Remove all script characters we can
    for char in project.getAllScriptCharacters():
        vidName = re.sub("_{0}$".format(char.characterName().replace(" ", "_")), "", vidName, flags=re.I)

    # Find the Trial name from the video name, no decent way of doing it, could be cached
    trialParts = vidName.split("_")
    trial = None
    for idx in xrange(len(trialParts)):
        testTrialName = "_".join(trialParts[:len(trialParts) - idx])
        trials = project.getTrialsByRegex("^{0}$".format(testTrialName))
        if len(trials) == 1:
            trial = trials[0]
            break

    if trial is None:
        raise ValueError("Unable to find trial for {0}".format(videoPath))

    # Use this to clean the 'nice names' to how they are stored in filenames
    textMappingDict = {
        " ": "_",
        "(":"",
        ")":"",
    }

    # get the video char name by removing the trial name from the video name
    videoCharName = trialName.replace("{0}_".format(trial.name), "")

    # go through each character and try to match it up with file suffix
    for char in trial.getCharacters():
        charName = char.scriptChar().characterName()
        for oldChar, newChar in textMappingDict.iteritems():
            charName = charName.replace(oldChar, newChar)
        if videoCharName == charName:
            # only way to get the talent variation at the moment
            return char.performerROM().bodyScale().talentVariation()
    raise ValueError("Unable to find character for {0} on trial {1}".format())


# Adds Pre/Post Delay to suppled Controller name.
def addDelay_Ctrl( lCtrlName, lFrame, lDelayFactor, minDelayFrames, maxDelayFrames, maxCtrlValue ):

    # Find Controller
    cl = FBComponentList()
    FBFindObjectsByName( lCtrlName, cl, False, True )
    if len( cl ) == 1:
        if lCtrlName == "CTRL_C_jaw":
            print lCtrlName
        lCtrl = cl[0]

        # Goto Current Start/End frame
        pc = FBPlayerControl()
        lTime = FBTime( 0,0,0,lFrame )
        pc.Goto( lTime )
        lValue = lCtrl.Translation[1]

        # If ctrl value exceeds ctrl max, add Delay.
        #if lValue > maxCtrlValue: - Removed.  TESTING always adds delay to controller.

        #Sets Key at current frame range start/end.
        lCtrl.Translation.GetAnimationNode().Nodes[1].FCurve.KeyAdd( lTime, lValue )

        # Clear keys before/after current frame range start/end.
        if lDelayFactor == -1:
            lCtrl.Translation.GetAnimationNode().Nodes[1].FCurve.KeyDeleteByTimeRange( FBTime.MinusInfinity, FBTime( 0,0,0, (lFrame - 1) ) )
        elif lDelayFactor == 1:
            lCtrl.Translation.GetAnimationNode().Nodes[1].FCurve.KeyDeleteByTimeRange( FBTime( 0,0,0, (lFrame + 1) ), FBTime.Infinity )

        # This is the magic formula to determine amount of delay frames needed.  ranges from min to max.
        # lDelayFactor (-1 or 1) allows this to work for pre or post delay without multiple formulas.
        lDelayFrames = int( lValue * lDelayFactor * (maxDelayFrames - minDelayFrames) ) + ( minDelayFrames * lDelayFactor )

        # Set keys for delay frames.
        # Does not completely 0 out the controller, but will scale from 0 to the max Ctrl Value set above.
        lDelayTime = FBTime( 0,0,0,(lFrame + lDelayFrames) )
        lDelayValue = lValue * maxCtrlValue
        lCtrl.Translation.GetAnimationNode().Nodes[1].FCurve.KeyAdd( lDelayTime, lDelayValue )

        # Returns current delay range for pre/post
        return (lFrame + lDelayFrames)
    return lFrame

def addPreDelay_Ctrl( lCtrlName, lStartFrame_Old, minDelayFrames, maxDelayFrames, maxCtrlValue ):
    # Settings for "Pre" Delay
    lStartFrame = FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
    lStartFrame_New = addDelay_Ctrl( lCtrlName, lStartFrame, -1, minDelayFrames, maxDelayFrames, maxCtrlValue )
    # To see if the take needs more frames, this returns the lower of either the current or the control range.
    return min([ lStartFrame_New, lStartFrame_Old ])

def addPostDelay_Ctrl( lCtrlName, lEndFrame_Old, minDelayFrames, maxDelayFrames, maxCtrlValue ):
    # Settings for "Post" Delay
    lEndFrame = FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
    lEndFrame_New = addDelay_Ctrl( lCtrlName, lEndFrame, 1, minDelayFrames, maxDelayFrames, maxCtrlValue )
    # To see if the take needs more frames, this returns the higher of either the current or the control range.
    return max([ lEndFrame_New, lEndFrame_Old ])

def addDelay_Current():
    # Sets up Pre/Post delay for current take for list of controllers names.

    # Initialize Range
    lStartFrame_New = FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
    lEndFrame_New = FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()

    # Specs
    lCtrlList = [
        "CTRL_C_jaw",
        "CTRL_L_mouth_funnelU",
        "CTRL_L_mouth_funnelD",
        "CTRL_R_mouth_funnelU",
        "CTRL_R_mouth_funnelD",
        "CTRL_L_mouth_puckerU",
        "CTRL_L_mouth_puckerD",
        "CTRL_R_mouth_puckerU",
        "CTRL_R_mouth_puckerD",
        "CTRL_L_mouth_lipsTogetherU",
        "CTRL_L_mouth_lipsTogetherD",
        "CTRL_R_mouth_lipsTogetherU",
        "CTRL_R_mouth_lipsTogetherD",
        "CTRL_L_mouth_lowerLipDepress",
        "CTRL_R_mouth_lowerLipDepress",
        "CTRL_L_mouth_upperLipRaise",
        "CTRL_R_mouth_upperLipRaise"
    ]

    minDelayFrames = 2
    maxDelayFrames = 7
    maxCtrlValue = 0.1

    # Setup Pre/Post Delay for each controller in list.  Saves updated range max/min
    for lCtrlName in lCtrlList:
        lStartFrame_New = addPreDelay_Ctrl( lCtrlName, lStartFrame_New, minDelayFrames, maxDelayFrames, maxCtrlValue )
        lEndFrame_New = addPostDelay_Ctrl( lCtrlName, lEndFrame_New, minDelayFrames, maxDelayFrames, maxCtrlValue )


    # Different list, different specs.
    lCtrlList_2 = [
        "CTRL_L_eye_blink",
        "CTRL_R_eye_blink"
    ]

    minDelayFrames = 2
    maxDelayFrames = 4
    maxCtrlValue = 0.1

    # Setup Pre/Post Delay for each controller in list.  Saves updated range max/min
    for lCtrlName in lCtrlList_2:
        lStartFrame_New = addPreDelay_Ctrl( lCtrlName, lStartFrame_New, minDelayFrames, maxDelayFrames, maxCtrlValue )
        lEndFrame_New = addPostDelay_Ctrl( lCtrlName, lEndFrame_New, minDelayFrames, maxDelayFrames, maxCtrlValue )

    # Apply New Range to include Delay to accomodate max/min controller
    FBSystem().CurrentTake.LocalTimeSpan = FBTimeSpan( FBTime( 0,0,0, lStartFrame_New ), FBTime( 0,0,0, lEndFrame_New ) )

# Loops through each take and applies Pre/Post Delay
def addDelay_All():
    for take in FBSystem().Scene.Takes:
        FBSystem().CurrentTake = take

        # Setup Pre/Post Delay for one take.
        lStartFrame = FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        if lStartFrame == 0:
            addDelay_Current()

def resetCtrl( lCtrl_Name, lCtrl_Value ):
    #
    # Clear all keys, and resets all facial controllers to provided value
    #

    ctrlList = FBComponentList()
    FBFindObjectsByName( lCtrl_Name, ctrlList, False, True )

    for lCtrl in ctrlList:

        # Set Animated & clear all keys
        lCtrl.Translation.SetAnimated( True )
        lCtrl.Rotation.SetAnimated( True )

        lCtrlT = lCtrl.Translation.GetAnimationNode().Nodes
        for lTAxis in lCtrlT:
            lTAxis.FCurve.EditClear()

        lCtrlR = lCtrl.Rotation.GetAnimationNode().Nodes
        for lRAxis in lCtrlR:
            lRAxis.FCurve.EditClear()

        lCtrl.Translation.Data = FBVector3d( 0, lCtrl_Value, 0 )
        lCtrl.Rotation.Data = FBVector3d( 0,0,0 )

def fixTweakerAnimation():

    tweakerList = [
        'CTRL_R_mouth_lipUpDownD',
        'CTRL_L_mouth_lipUpDownD',
        'CTRL_R_mouth_lipUpDownU',
        'CTRL_L_mouth_lipUpDownU',
        'CTRL_R_eye_relax',
        'CTRL_R_eye_squeeze',
        'CTRL_L_eye_relax',
        'CTRL_L_eye_squeeze',
        'CTRL_R_nose_nasolabialDeepen',
        'CTRL_L_nose_nasolabialDeepen',
        'CTRL_R_mouth_corner',
        'CTRL_L_mouth_corner',
        'CTRL_L_mouth_thicknessD',
        'CTRL_R_mouth_thicknessD',
        'CTRL_R_mouth_thicknessU',
        'CTRL_L_mouth_thicknessU',
        'CTRL_L_mouth_lipsRollD',
        'CTRL_R_mouth_lipsRollD',
        'CTRL_R_mouth_lipsRollU',
        'CTRL_L_mouth_lipsRollU',
        'CTRL_R_mouth_cornerShapnessU',
        'CTRL_L_mouth_cornerShapnessU',
        'CTRL_L_mouth_cornerShapnessD',
        'CTRL_R_mouth_cornerShapnessD',
        'CTRL_R_mouth_pushPullD',
        'CTRL_L_mouth_pushPullD',
        'CTRL_R_mouth_pushPullU',
        'CTRL_L_mouth_pushPullU'
    ]

    for lCtrl_Name in tweakerList:
        resetCtrl( lCtrl_Name, 0 )

def fixTweakerAnimation_AllTakes():
    for lTake in FBSystem().Scene.Takes:
        FBSystem().CurrentTake = lTake
        fixTweakerAnimation()