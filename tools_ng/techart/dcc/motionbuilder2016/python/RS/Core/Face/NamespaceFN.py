'''
Name Validation Module, to cover inconsistent naming conventions.
*Author:*
    * Thanh Phan <thanh.phan@rockstarsandiego.com>
'''


from pyfbsdk import *

def validateNamespace ( lName ):
# This is to determine plugin name.
    
    lName_validated = lName
    
    if "Player_Zero" in lName:
        lName_validated = "Player"
    elif "Player_One" in lName:
        lName_validated = "Player"
    elif "Player_Two" in lName:
        lName_validated = "Player"
    elif "Horse" in lName:
        lName_validated = "Horse"
    elif "Wolf" in lName:
        lName_validated = "Wolf"
    elif "Player_Three" in lName:
        lName_validated = "JohnMarston"
    elif "Thomas" in lName:
        lName_validated = "ThomasDowne"
    elif "Mud2BigGuy" in lName:
        lName_validated = "BigDude"
    elif "MasterB" in lName:
        lName_validated = "MasterB"
    else:
        lName_validated = lName.partition(" ")[0].partition("^")[0].partition("_")[2].partition("_")[0]
    
    return lName_validated