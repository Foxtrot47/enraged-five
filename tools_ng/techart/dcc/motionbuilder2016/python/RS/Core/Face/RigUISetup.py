#===========================================================================
#       Edited by: Kyle Hansen, kyle.hansen@rockstarsandiego.com
#       Notes:
#
#           Added support for using an XML to drive the attribute relation constraint.
#               This insures that we have the correct controllers driving
#               correct attributes.  
#===========================================================================

from pyfbsdk import *
import RS.Globals as glo
import os
import sys
from xml.dom import minidom
import RS.Core.Face.Toolbox as face
import RS.Core.Face.ZeroTransforms as lzt
import RS.Utils.Path 

reload(face)


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_ConstraintsFolder
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def rs_IK_ConstraintsFolder():
    
    lFolder = None
    
    for iFolder in glo.gFolders:
        if iFolder.Name == 'Constraints 1':
            lFolder = iFolder
    
    if lFolder == None:
        lPlaceholder = FBConstraintRelation('Remove_Me')
        lFolder = FBFolder("Constraints 1", lPlaceholder)   
        lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Folders"
        
        FBSystem().Scene.Evaluate()
        
        lPlaceholder.FBDelete()  
            
    return lFolder
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_ShadersFolder
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def rs_ShadersFolder():
    
    lFolder = None
    
    for iFolder in glo.gFolders:
        if iFolder.Name == 'Shaders':
            lFolder = iFolder
    
    if lFolder == None:
        lPlaceholder = FBShader('Remove_Me')
        lFolder = FBFolder("Shaders ", lPlaceholder)   
        lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Folders"
        
        FBSystem().Scene.Evaluate()
        
        lPlaceholder.FBDelete()  
            
    return lFolder

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_3LateralUISetUp
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   


    
#===  Slider controller class, this holds the information for the sliders connections
class cSlideController:
    def __init__(self):
        
        self.name = ""
        self.controllerType = None
        
        #== Connection info, [outboundAttribute, targetController, targetAttribute]
        self.targetAttributes = []



def FindAnimationNode(pParent, pName):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult



def ConnectBox(nodeOut,nodeOutPos, nodeIn, nodeInPos):
    ret = False
    
    #print "NODES", (nodeOut.Name + "." + nodeOutPos), " -->", (nodeIn.Name + "." + nodeInPos)
    
    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
    mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )
    
    if mynodeOut and mynodeIn:
        ret = FBConnect(mynodeOut, mynodeIn)
      
    return ret


#==========================================================
#  Returns controller class objects whos types match the passed in string
#       controllers = list of controller class objects
#       cType       = searchString for the type
#       nameBool    = bool to return names or objects
#==========================================================
def getControllersByType(controllers, cType, nameBool=0):
    foundControllers = []
    for controller in controllers:
        if controller.controllerType == cType:
            if nameBool:
                foundControllers.append(controller.name)
            else:
                foundControllers.append(controller)
    return foundControllers

def rs_3LateralUISetUp():

    #=== XML mappings path
    
    mappingXMLPath = '{0}/etc/config/characters/faceAttributeXMLMapping.xml'.format( RS.Config.Tool.Path.TechArt )
    if os.path.exists(mappingXMLPath):
        '''
        EXAMPLE XML FILE TEMPLATE
        
        <?xml version="1.0" encoding="utf-8"?>
        <attributeMapping xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Version="1">
            <controllerOut name='CIRC_scream' type='1dir' >
                <attributeOut name='X'>
                    <targetAttribute parentController='mouth_CTRL' name='scream_CA'></targetAttribute>
                </attributeOut>
            </controllerOut>
        </attributeMapping>
        '''
        
        
        domParse = minidom.parse(mappingXMLPath)
        controllers = []
        
        for node in domParse.getElementsByTagName('controllerOut'):
            contInst = cSlideController()
            contInst.name = str(node.getAttribute('name'))
            contInst.controllerType = str(node.getAttribute('type'))
            
            for childNodeOut in node.getElementsByTagName('attributeOut'):
                attributeOutName = str(childNodeOut.getAttribute('name'))
                
                for childNodeAttribute in childNodeOut.getElementsByTagName('targetAttribute'):
                    
                    targetCont = str(childNodeAttribute.attributes['parentController'].value)
        
                    targetAttr = str(childNodeAttribute.getAttribute('name'))
                    contInst.targetAttributes.append([attributeOutName, targetCont , targetAttr])
                    
            controllers.append(contInst)
    
        #== IF we have some controllers then we can work!                
        if len(controllers) > 0:
            
            # Set the pre-rotations.
            lzt.rs_ZeroRotations()
        
            constraintBoxes = []
            lRelConstraint = FBConstraintRelation('3LateralCustomAttributes')
            lTag = lRelConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Constraints"   
            lConFolder = rs_IK_ConstraintsFolder()
            lConFolder.Items.append(lRelConstraint)
            lRelConstraint.Active = True
            
            # Parent constraint for on-face GUI
            lParentFace = FBFindModelByLabelName("FACIAL_facialRoot")
            lChildFace = FBFindModelByLabelName("facialRoot_C_OFF")
            if lChildFace:
                if lParentFace:
                    lOnFaceConstraint = face.createParentConstraint( lParentFace, lChildFace, "3LateralfacialRoot_Parent", pMoveToSource=False )
                    lTag2 = lOnFaceConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                    lTag2.Data = "rs_Constraints"
                    lConFolder.Items.append(lOnFaceConstraint)
            
            i = 0       
            for controller in controllers:
                outputModel = FBFindModelByLabelName(controller.name)
                print controller.name, "--->", outputModel.Name
                if outputModel:               
                    for targetAttribute in controller.targetAttributes:
                                             
                        targetModel = FBFindModelByLabelName(targetAttribute[1])   # get the model in the scene, this attribute is going to link with.
                        if targetModel:
                            print targetAttribute[1], "---->", targetModel.Name
                            lConstrainedObject = None
                            for relBoxIndex in range(0, len(constraintBoxes)):
                                #== if the object exists, lets use that one
                                if targetModel.Name in constraintBoxes[relBoxIndex].Name:
                                    lConstrainedObject = constraintBoxes[relBoxIndex]
                                    
                            #== IF the object has not been created, create it.
                            if lConstrainedObject == None:
                                
                                lConstrainedObject = lRelConstraint.ConstrainObject(targetModel)
                                                                
                                constraintBoxes.append(lConstrainedObject) #== add it to our library of contraint boxes for use on the next controller
                                
                                lConstrainedObject.UseGlobalTransforms = False
                                lRelConstraint.SetBoxPosition(lConstrainedObject, 1000, i * 200)
                                
                            if lConstrainedObject == None:
                                print "We lost the constrained object!!!"
                                
                                   
                            iProp = targetModel.PropertyList.Find(targetAttribute[2])
                            
                            if iProp:
                                try:
                                    iProp.SetAnimated(True)
                                except:
                                    iProp.SetAnimated = True
                            
                                if iProp.IsUserProperty():
                                    
                                    if str(type(iProp)) == "<class 'pyfbsdk.FBPropertyAnimatableDouble'>":
                                        iProp.SetMin(0)
                                        iProp.SetMax(1)
                                
                                
                            
                                lConverterBox = lRelConstraint.CreateFunctionBox('Converters', 'Vector To Number')
                                lRelConstraint.SetBoxPosition(lConverterBox, 500, i * 200)
                                
                                
                                lSourceObject = lRelConstraint.SetAsSource(outputModel)
                                lSourceObject.UseGlobalTransforms = False
                                lRelConstraint.SetBoxPosition(lSourceObject, 0, i * 200)
    
                                con1 = ConnectBox(lSourceObject, 'Lcl Translation', lConverterBox, 'V')
                                con2 = ConnectBox(lConverterBox, targetAttribute[0], lConstrainedObject, iProp.Name)
                                
                                if con1 == False:
                                    print "Error: Connections were not valid:", (lSourceObject.Name + ".Lcl Translation"), (lConverterBox.Name + ".V")
                                if con2 == False:
                                    print "Error: Connections were not valid:", (lConverterBox.Name + "." + targetAttribute[0]), (lConstrainedObject.Name + "." + iProp.Name)
                                i = i + 1
    
            
            #=== Setup some lists of controllers to perform functions on them
            gCAFaceAttrGui = ["FacialAttrGUI","Tongue","RECT_press","TEXT_press","RECT_rollIn","TEXT_rollIn",
                              "RECT_narrowWide","TEXT_narrowWide","JAW","RECT_clench","TEXT_clench","RECT_backFwd",
                              "TEXT_backFwd","NOSE","RECT_nasolabialFurrowL","TEXT_nasolabialFurrowL","RECT_nasolabialFurrowR",
                              "TEXT_nasolabialFurrowR","MOUTH","RECT_smileR","RECT_smileL","TEXT_smileL","RECT_openSmileR",
                              "TEXT_openSmileR","RECT_openSmileL","TEXT_openSmileL","RECT_frownR","TEXT_frownR","RECT_frownL",
                              "TEXT_frownL","RECT_scream","TEXT_scream","RECT_lipsNarrowWideR","TEXT_lipsNarrowWideR","RECT_lipsNarrowWideL",
                              "TEXT_lipsNarrowWideL","RECT_lipsStretchOpenR","TEXT_lipsStretchOpenR","RECT_lipsStretchOpenL",
                              "TEXT_lipsStretchOpenL","RECT_chinWrinkle","TEXT_chinWrinkle","RECT_chinRaiseUpper","TEXT_chinRaiseUpper",
                              "RECT_chinRaiseLower","TEXT_chinRaiseLower","RECT_closeOuterR","TEXT_closeOuterR","RECT_closeOuterL",
                              "TEXT_closeOuterL","RECT_puckerR","TEXT_puckerR","RECT_puckerL","TEXT_puckerL","RECT_oh","TEXT_oh",
                              "RECT_funnelUR","TEXT_funnelUR","RECT_funnelDR","TEXT_funnelDR","RECT_mouthSuckUR","TEXT_mouthSuckUR",
                              "RECT_mouthSuckDR","TEXT_mouthSuckDR","RECT_pressR","TEXT_pressR","RECT_pressL","TEXT_pressL","RECT_dimpleR",
                              "TEXT_dimpleR","RECT_dimpleL","TEXT_dimpleL","RECT_suckPuffR","TEXT_suckPuffR","RECT_suckPuffL","TEXT_suckPuffL",
                              "RECT_lipBite","TEXT_lipBite","RECT_funnelUL","TEXT_funnelUL","RECT_funnelDL","TEXT_funnelDL","RECT_funnelDL001",
                              "TEXT_mouthSuckUL","RECT_mouthSuckDL","TEXT_mouthSuckDL","Eyes","RECT_blinkL","TEXT_blinkL","RECT_squeezeR",
                              "TEXT_squeezeR","RECT_squeezeL","TEXT_squeezeL","RECT_lipsNarrowWideR001","TEXT_blinkR","RECT_openCloseUR001",
                              "TEXT_openCloseDR","RECT_squintInnerUR","TEXT_squintInnerUR","RECT_squintInnerDR","TEXT_squintInnerDR",
                              "RECT_openCloseUR","TEXT_openCloseUR","RECT_openCloseDL","TEXT_openCloseDL","RECT_squintInnerUL","TEXT_squintInnerUL",
                              "RECT_squintInnerDL","TEXT_squintInnerDL","RECT_openCloseUL","TEXT_openCloseUL","LIPS","RECT_thinThick_C",
                              "RECT_thinThick_D","RECT_stickyLips_E","RECT_thinThick_B","RECT_thinThick_F","RECT_thinThick_H","RECT_thinThick_G",
                              "RECT_stickyLips_A",
                              "faceControls_FRAME","mouth_FRAME","mouth_TEXT","jaw_FRAME","jaw_TEXT","tongue_TEXT","tongueMove_FRAME",
                              "tongueRoll_FRAME","tongueInOut_FRAME","outerBrow_R_FRAME","innerBrow_L_FRAME","outerBrow_L_FRAME","brows_TEXT",
                              "innerBrow_R_FRAME","eye_R_FRAME","eye_L_FRAME","eyes_TEXT","eye_C_FRAME","nose_L_FRAME","nose_R_FRAME",
                              "nose_TEXT","cheek_TEXT","cheek_L_FRAME","cheek_R_FRAME"]
            
            #== This one is for a controller type set in the XML...
            gCAOneDSliders = getControllersByType(controllers, "1dir", 1)
            
            #== This one is for a controller type set in the XML...
            gCATwoDSliders = getControllersByType(controllers, "2dir", 1)
            
            qSquareSliders = ["mouth_CTRL","tongueMove_CTRL","tongueRoll_CTRL","eye_C_CTRL","eye_L_CTRL","eye_R_CTRL","nose_L_CTRL","nose_R_CTRL"]
            
            qPosNegSliders = ["tongueInOut_CTRL","outerBrow_R_CTRL","outerBrow_L_CTRL"]
            
            qPosSliders =    ["cheek_L_CTRL", "cheek_R_CTRL"]
            
            g3WaySliders =   ["jaw_CTRL","innerBrow_L_CTRL","innerBrow_R_CTRL"]                
            
            #lets lock the gui!
            for iGui in gCAFaceAttrGui:
                
                iGuiModel = FBFindModelByLabelName(iGui)
                
                if iGuiModel:
            
                    iGuiModel.Pickable = False
                    iGuiModel.Transformable = False   
                     
##            #Create Shader, add to folder and apply to controls
##            lControlShader = FBShaderLighted( '3LateralAlphaShader')
##            lShadFolder = rs_ShadersFolder()
##            lShadFolder.Items.append(lControlShader)
##            lControlShader.Transparency = FBAlphaSource.kFBAlphaSourceTransluscentAlpha
##            
##            lFaceControlList = []
##            
##            for iChild in glo.gComponents:
##                if iChild.Name.endswith("_CTRL"):
##                    lFaceControlList.append(iChild)
##                if iChild.Name.startswith("CIRC_"):
##                    lFaceControlList.append(iChild)
##                if iChild.Name.startswith("RECT_"):
##                    lFaceControlList.append(iChild)
##                                        
##            for iControl in lFaceControlList:
##                if iControl.ClassName() == 'FBModel':
##                    iControl.Shaders.append(lControlShader)
##                    iControl.ShadingMode = FBModelShadingMode.kFBModelShadingAll
####                    FBSystem().Scene.Evaluate()
                
            #=== Lets start processing the lists of controllers, making their settings match our rig        
            for iCirc in gCAOneDSliders:
                
                iCircModel = FBFindModelByLabelName(iCirc)
                
                if iCircModel:
                    iCircModel.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0.01) 
                    iCircModel.PropertyList.Find("TranslationActive").Data = True
                    
                    iCircModel.PropertyList.Find("TranslationMinX").Data = True
                    iCircModel.PropertyList.Find("TranslationMinY").Data = True
                    iCircModel.PropertyList.Find("TranslationMinZ").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxX").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxY").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxZ").Data = True 
                    
                    iCircModel.PropertyList.Find("TranslationMin").Data = FBVector3d(0,0,0.01)
                    iCircModel.PropertyList.Find("TranslationMax").Data = FBVector3d(1,0,0.01)
                    
                    #lControlShader.ReplaceAll(iCircModel)
                    
                    glo.gScene.Evaluate()  
                    
            for iCirc in gCATwoDSliders:
                
                iCircModel = FBFindModelByLabelName(iCirc)
                if iCircModel == None:
                    print "Could not find Model: " + iCirc
                    
                if iCircModel:
                    print "Working on: " + iCirc
                    iCircModel.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0.01) 
                    iCircModel.PropertyList.Find("TranslationActive").Data = True
                    
                    iCircModel.PropertyList.Find("TranslationMinX").Data = True
                    iCircModel.PropertyList.Find("TranslationMinY").Data = True
                    iCircModel.PropertyList.Find("TranslationMinZ").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxX").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxY").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxZ").Data = True 
                    
                    iCircModel.PropertyList.Find("TranslationMin").Data = FBVector3d(-1,0,0.01)
                    iCircModel.PropertyList.Find("TranslationMax").Data = FBVector3d(1,0,0.01)  
                    
                    #lControlShader.ReplaceAll(iCircModel) 
                    
                    glo.gScene.Evaluate()    
                    
            for iCirc in qSquareSliders:
                
                iCircModel = FBFindModelByLabelName(iCirc)
                
                if iCircModel:
                    iCircModel.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0.01) 
                    iCircModel.PropertyList.Find("TranslationActive").Data = True
                    
                    iCircModel.PropertyList.Find("TranslationMinX").Data = True
                    iCircModel.PropertyList.Find("TranslationMinY").Data = True
                    iCircModel.PropertyList.Find("TranslationMinZ").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxX").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxY").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxZ").Data = True 
                    
                    iCircModel.PropertyList.Find("TranslationMin").Data = FBVector3d(-1,-1,0.01)
                    iCircModel.PropertyList.Find("TranslationMax").Data = FBVector3d(1,1,0.01)  
                    
                    #lControlShader.ReplaceAll(iCircModel) 
                    
                    glo.gScene.Evaluate()   
                    
                    
            for iCirc in qPosNegSliders:
                
                iCircModel = FBFindModelByLabelName(iCirc)
                
                if iCircModel:
                    iCircModel.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0.01) 
                    iCircModel.PropertyList.Find("TranslationActive").Data = True
                    
                    iCircModel.PropertyList.Find("TranslationMinX").Data = True
                    iCircModel.PropertyList.Find("TranslationMinY").Data = True
                    iCircModel.PropertyList.Find("TranslationMinZ").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxX").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxY").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxZ").Data = True 
                    
                    iCircModel.PropertyList.Find("TranslationMin").Data = FBVector3d(0,-1,0.01)
                    iCircModel.PropertyList.Find("TranslationMax").Data = FBVector3d(0,1,0.01)  
                    
                    #lControlShader.ReplaceAll(iCircModel) 
                    
                    glo.gScene.Evaluate()  
                    
            for iCirc in qPosSliders:
                
                iCircModel = FBFindModelByLabelName(iCirc)
                
                if iCircModel:
                    iCircModel.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0.01) 
                    iCircModel.PropertyList.Find("TranslationActive").Data = True
                    
                    iCircModel.PropertyList.Find("TranslationMinX").Data = True
                    iCircModel.PropertyList.Find("TranslationMinY").Data = True
                    iCircModel.PropertyList.Find("TranslationMinZ").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxX").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxY").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxZ").Data = True 
                    
                    iCircModel.PropertyList.Find("TranslationMin").Data = FBVector3d(0,0,0.01)
                    iCircModel.PropertyList.Find("TranslationMax").Data = FBVector3d(0,1,0.01)  
                    
                    #lControlShader.ReplaceAll(iCircModel) 
                    
                    glo.gScene.Evaluate()  
                    
            for iCirc in g3WaySliders:
                
                iCircModel = FBFindModelByLabelName(iCirc)
                
                if iCircModel:
                    iCircModel.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0.01) 
                    iCircModel.PropertyList.Find("TranslationActive").Data = True
                    
                    iCircModel.PropertyList.Find("TranslationMinX").Data = True
                    iCircModel.PropertyList.Find("TranslationMinY").Data = True
                    iCircModel.PropertyList.Find("TranslationMinZ").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxX").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxY").Data = True
                    iCircModel.PropertyList.Find("TranslationMaxZ").Data = True 
                    
                    if "jaw" in iCirc:
                    
                        iCircModel.PropertyList.Find("TranslationMin").Data = FBVector3d(-1,-1,0.01)
                        iCircModel.PropertyList.Find("TranslationMax").Data = FBVector3d(1,0,0.01)  
                        
                    elif "_L_" in iCirc:
                    
                        iCircModel.PropertyList.Find("TranslationMin").Data = FBVector3d(-1,-1,0.01)
                        iCircModel.PropertyList.Find("TranslationMax").Data = FBVector3d(0,1,0.01)  
                
                    elif "_R_" in iCirc:
                    
                        iCircModel.PropertyList.Find("TranslationMin").Data = FBVector3d(-1,-1,0.01)
                        iCircModel.PropertyList.Find("TranslationMax").Data = FBVector3d(0,1,0.01)  
                    #lControlShader.ReplaceAll(iCircModel) 
                    
                    glo.gScene.Evaluate()  
            
            
            
            FacialAttrGUI = FBFindModelByLabelName("FacialAttrGUI")
            
            l3LateralCACameraParent = FBCreateObject( "Browsing/Templates/Elements", "Null", "3LateralCACameraParent" )
            l3LateralCACameraParent.Parent = FacialAttrGUI
            l3LateralCACameraParent.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0.25)
            glo.gScene.Evaluate()
            l3LateralCACameraParent.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,90,0)
            glo.gScene.Evaluate()
            l3LateralCACameraParent.Show = False
            l3LateralCACameraParent.Pickable = False
            
            l3LateralCACamera = FBCamera("3LateralCACamera")
            l3LateralCACamera.Parent = l3LateralCACameraParent
            l3LateralCACamera.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
            glo.gScene.Evaluate()
            l3LateralCACamera.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,0)
            glo.gScene.Evaluate()
            
            FacialAttrGUI = FBFindModelByLabelName("faceControls_OFF")
            
            l3LateralCACameraParent = FBCreateObject( "Browsing/Templates/Elements", "Null", "3LateralUICameraParent" )
            l3LateralCACameraParent.Parent = FacialAttrGUI
            l3LateralCACameraParent.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,-20,2)
            glo.gScene.Evaluate()
            l3LateralCACameraParent.PropertyList.Find("Lcl Rotation").Data = FBVector3d(90,0,90)
            glo.gScene.Evaluate()
            l3LateralCACameraParent.Show = False
            l3LateralCACameraParent.Pickable = False
            
            l3LateralCACamera = FBCamera("3LateralUICamera")
            l3LateralCACamera.Parent = l3LateralCACameraParent
            l3LateralCACamera.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
            glo.gScene.Evaluate()
            l3LateralCACamera.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,0)
            glo.gScene.Evaluate()
            
        else:
            print "Error: Could not find controllers based on the XML: ", mappingXMLPath
            
            
        return True
    
    else:
        print "Error: Cannot find file:", mappingXMLPath
        return False
    
