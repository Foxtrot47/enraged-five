from RS.Core.Face.KeyClamper import keyClamper
reload (keyClamper)
from RS.Tools import UI

@UI.Run(title=keyClamper.keyClamperDialog.TOOL_NAME, dockable=True)
def Run():
    return keyClamper.keyClamperDialog()
