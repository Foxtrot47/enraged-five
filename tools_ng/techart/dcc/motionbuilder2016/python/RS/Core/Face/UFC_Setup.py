from pyfbsdk import *
from ctypes import *
from xml.dom import minidom

import RS.Config
import RS.Globals
import RS.Perforce as p4
import RS.Utils.Scene.Component
from RS.Utils.Scene import Constraint
import os

def PrintProperty( pModel ):
    lPropMgr = pModel.PropertyList
    for lProp in lPropMgr:
        print lProp.GetName()

def findAllChildren(rootNode, nodeList = None, includeParent = True):
    nodeList = nodeList or []
    if includeParent:
        nodeList.append( rootNode )
    for each in rootNode.Children:
        nodeList.append( each )
        
        findAllChildren( each, nodeList, False )
    return nodeList
        
def get3LFaceRig( lNamespace ):
    startPath = RS.Config.Project.Path.Root + "\\art\\peds\\3LateralSetup\\Mobu_Rigs\\"
    p4.Sync( startPath + "..." )
    
    lFilePopup = FBFilePopup();
    lFilePopup.Filter = '*.fbx'
    lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen
    lFilePopup.Path = startPath
    
    if lFilePopup.Execute():
        fullFileName = lFilePopup.FullFilename
        lOptions = FBFbxOptions( True )
        lOptions.NamespaceList = lNamespace
        lOptions.CamerasAnimation = False
        lOptions.BaseCameras = False
        lOptions.CurrentCameraSettings = False
        lOptions.CameraSwitcherSettings = False
        
        RS.Globals.Application.FileMerge( fullFileName, False, lOptions )
        
        
def remove3LFaceRig( lNamespace ):
    compList = FBComponentList()
    compList = RS.Utils.Scene.Component.GetComponents( lNamespace )
    for i in range(5):
        tempComponents = RS.Utils.Scene.Component.GetComponents( lNamespace + " " + str(i) )
        for j in tempComponents:
            compList.append(j)
            
    FBSystem().Scene.Evaluate()
    
    for i in compList:
        i.FBDelete()
    
    FBMessageBox( "Sucess", "OK", "OK" )
    

def setupUFCConstraints(  ):
    # create constraints
    
    objList = [
        "CTRL_C_lookAt",
        "CTRL_L_LookAt",
        "CTRL_R_LookAt",
        "GRP_C_lookAt",
        "GRP_L_lookAtOffset",
        "GRP_R_lookAtOffset",
        "GRP_convergenceGUI",
        "LOC_L_eyeDriver",
        "LOC_L_eyeLookAtUp",
        "LOC_L_LookAtDriver",
        "LOC_L_LookAtDriverOffset",
        "LOC_L_LookAtFollow",
        "LOC_L_LookAtTarget",
        "LOC_R_LookAtDriver",
        "LOC_R_LookAtTarget",
        "LOC_R_eyeDriver",
        "LOC_R_eyeLookAtUp",
        "LOC_R_LookAtFollow",
        "LOC_R_LookAtDriverOffset",
        "OH_LookAtRoot"
        ]
    
    transList = []
    rotList = []
    
    for each in objList:
        obj = FBFindModelByLabelName( each )
        if obj != None:
            transList.append( obj.Translation.Data )
            rotList.append( obj.Rotation.Data )
    
    constMgr = FBConstraintManager()
    
    constList = FBComponentList()
    
    a = Constraint.CreateConstraint(Constraint.ROTATION)
    a.LongName = "LOC_R_eyeDriver_orientConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("LOC_R_eyeDriver", objs)
    FBFindObjectsByName("LOC_R_LookAtDriverOffset", objs)
    a.ReferenceAdd(0,objs[0])
    a.ReferenceAdd(1,objs[1])
    a.Active = True
    constList.append(a)
    
    a = Constraint.CreateConstraint(Constraint.ROTATION)
    a.LongName = "LOC_L_eyeDriver_orientConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("LOC_L_eyeDriver", objs)
    FBFindObjectsByName("LOC_L_LookAtDriverOffset", objs)
    a.ReferenceAdd(0,objs[0])
    a.ReferenceAdd(1,objs[1])
    a.Active = True
    constList.append(a)
    
    
    a = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
    a.LongName = "GRP_convergenceGUI_parentConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("GRP_convergenceGUI", objs)
    FBFindObjectsByName("CTRL_C_lookAt", objs)
    a.ReferenceAdd(0,objs[0])
    a.ReferenceAdd(1,objs[1])
    a.Active = True
    constList.append(a)
    
    a = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
    a.LongName = "GRP_C_lookAt_parentConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("GRP_C_lookAt", objs)
    FBFindObjectsByName("head", objs)
    FBFindObjectsByName("OH_LookAtRoot", objs)
    a.ReferenceAdd(0,objs[0])
    a.ReferenceAdd(1,objs[1])
    a.ReferenceAdd(1,objs[2])
    a.PropertyList.Find('head.Weight').SetAnimated(True)
    a.PropertyList.Find('OH_LookAtRoot.Weight').SetAnimated(True)
    a.Active = True
    constList.append(a)
    
    a = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
    a.LongName = "GRP_L_lookAtOffset_parentConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("GRP_L_lookAtOffset", objs)
    FBFindObjectsByName("LOC_L_LookAtFollow", objs)
    a.ReferenceAdd(0,objs[0])
    a.ReferenceAdd(1,objs[1])
    a.Active = True
    constList.append(a)
    
    a = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
    a.LongName = "GRP_R_lookAtOffset_parentConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("GRP_R_lookAtOffset", objs)
    FBFindObjectsByName("LOC_R_LookAtFollow", objs)
    a.ReferenceAdd(0,objs[0])
    a.ReferenceAdd(1,objs[1])
    a.Active = True
    constList.append(a)
    
    a = Constraint.CreateConstraint(Constraint.AIM)
    a.LongName = "LOC_L_LookAtDriver_aimConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("LOC_L_LookAtDriver", objs)
    a.ReferenceAdd(0,objs[0])
    objs = FBComponentList()
    FBFindObjectsByName("LOC_L_LookAtTarget", objs)
    a.ReferenceAdd(1,objs[0])
    objs = FBComponentList()
    FBFindObjectsByName("LOC_L_eyeLookAtUp", objs)
    a.PropertyList.Find('WorldUpType').Data = 1
    z = a.PropertyList.Find('World Up Object')
    z.append(objs[0])
    a.Active = True
    constList.append(a)
    
    a = Constraint.CreateConstraint(Constraint.AIM)
    a.LongName = "LOC_R_LookAtDriver_aimConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("LOC_R_LookAtDriver", objs)
    a.ReferenceAdd(0,objs[0])
    objs = FBComponentList()
    FBFindObjectsByName("LOC_R_LookAtTarget", objs)
    a.ReferenceAdd(1,objs[0])
    objs = FBComponentList()
    FBFindObjectsByName("LOC_R_eyeLookAtUp", objs)
    a.PropertyList.Find('WorldUpType').Data = 1
    z = a.PropertyList.Find('World Up Object')
    z.append(objs[0])
    a.Active = True
    constList.append(a)
    
    a = Constraint.CreateConstraint(Constraint.AIM)
    a.LongName = "LOC_L_LookAtDriverOffset_aimConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("LOC_L_LookAtDriverOffset", objs)
    a.ReferenceAdd(0,objs[0])
    objs = FBComponentList()
    FBFindObjectsByName("CTRL_L_lookAt", objs)
    FBFindObjectsByName("CTRL_L_LookAt", objs)
    a.ReferenceAdd(1,objs[0])
    objs = FBComponentList()
    FBFindObjectsByName("LOC_L_eyeLookAtUp", objs)
    a.PropertyList.Find('WorldUpType').Data = 1
    z = a.PropertyList.Find('World Up Object')
    z.append(objs[0])
    a.Active = True
    constList.append(a)
    
    a = Constraint.CreateConstraint(Constraint.AIM)
    a.LongName = "LOC_R_LookAtDriverOffset_aimConstraint1"
    objs = FBComponentList()
    FBFindObjectsByName("LOC_R_LookAtDriverOffset", objs)
    a.ReferenceAdd(0,objs[0])
    objs = FBComponentList()
    FBFindObjectsByName("CTRL_R_lookAt", objs)
    FBFindObjectsByName("CTRL_R_LookAt", objs)
    a.ReferenceAdd(1,objs[0])
    objs = FBComponentList()
    FBFindObjectsByName("LOC_R_eyeLookAtUp", objs)
    a.PropertyList.Find('WorldUpType').Data = 1
    z = a.PropertyList.Find('World Up Object')
    z.append(objs[0])
    a.Active = True
    constList.append(a)

    for each in objList:
        obj = FBFindModelByLabelName( each )
        if obj != None:
            i = objList.index( each )
            obj.Translation.Data = transList[i]
            obj.Rotation.Data = rotList[i]
    
    # constants
    EYESETUP_HOR_RANGE = 40
    EYESETUP_VERT_RANGE = 30
    CONVERGENCE_L_HOR_COR = 0.032553
    CONVERGENCE_L_VERT_COR = 0.000000000408935
    CONVERGENCE_R_HOR_COR = -0.0330271
    CONVERGENCE_R_VERT_COR = -0.00205613
    
    objs = FBComponentList()
    
    # scale down locators
    FBFindObjectsByName("LOC_*", objs)
    for obj in objs:
        if not obj.LongName.endswith("1"):
            if obj.ClassName() == "FBModelSkeleton":
                obj.PropertyList.Find("Size").Data = 1.0
                obj.PropertyList.Find("Show").Data = False
    objs = FBComponentList()
    FBFindObjectsByName("LOC_L_LookAtTarget", objs)
    for obj in objs:
        if obj.ClassName() == "FBModelSkeleton":
            obj.PropertyList.Find("Size").Data = 1.0
            obj.PropertyList.Find("Show").Data = False
    objs = FBComponentList()
    FBFindObjectsByName("LOC_R_LookAtTarget", objs)
    for obj in objs:
        if obj.ClassName() == "FBModelSkeleton":
            obj.PropertyList.Find("Size").Data = 1.0
            obj.PropertyList.Find("Show").Data = False
    
    # update frames selection
    objs = FBComponentList()
    FBFindObjectsByName("FRM_*", objs)
    for obj in objs:
        if obj.ClassName() == "FBModelSkeleton":
            obj.PropertyList.Find("Transformable").Data = False
            obj.PropertyList.Find("Pickable").Data = False
        
    objs = FBComponentList()
    FBFindObjectsByName("CTRL_*_mouth_lipsPressD", objs)
    for obj in objs:
        if obj.ClassName() == "FBModelSkeleton":
            obj.PropertyList.Find("Transformable").Data = False
            obj.PropertyList.Find("Pickable").Data = False
    
    objs = FBComponentList()
    FBFindObjectsByName("CTRL_*_neck_stretch_loc", objs)
    for obj in objs:
        if obj.ClassName() == "FBModelSkeleton":
            obj.PropertyList.Find("Transformable").Data = False
            obj.PropertyList.Find("Pickable").Data = False
        
    # update text selection
    objs = FBComponentList()
    FBFindObjectsByName("TEXT_*", objs)
    for obj in objs:
        if obj.ClassName() == "FBModelSkeleton":
            obj.PropertyList.Find("Transformable").Data = False
            obj.PropertyList.Find("Pickable").Data = False
        
    # create setup_relationConstraint
    setupRelConst = Constraint.CreateConstraint(Constraint.RELATION)
    setupRelConst.LongName = "setup_relationConstraint"
    
    constList.append( setupRelConst )
    
    objs = FBComponentList()
    FBFindObjectsByName("GRP_C_lookAt_parentConstraint1", objs)
    
    if "animal" not in FBApplication().FBXFileName.lower():
        # neck stretch setup
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_L_mouth_stretch", objs)
        ctlLMouthStr = setupRelConst.SetAsSource(objs[0])
        setupRelConst.SetBoxPosition(ctlLMouthStr, 0, 0)
        ctlLMouthStr.UseGlobalTransforms = False
        vec_ctlLMouthStr = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
        setupRelConst.SetBoxPosition(vec_ctlLMouthStr, 300, 0)
        FBConnect(ctlLMouthStr.AnimationNodeOutGet().Nodes[0], vec_ctlLMouthStr.AnimationNodeInGet().Nodes[0])
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_C_neck_stretch", objs)
        ctlCNeckStr = setupRelConst.SetAsSource(objs[0])
        setupRelConst.SetBoxPosition(ctlCNeckStr, 0, 100)
        ctlCNeckStr.UseGlobalTransforms = False
        vec_ctlCNeckStr = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
        setupRelConst.SetBoxPosition(vec_ctlCNeckStr, 300, 100)
        FBConnect(ctlCNeckStr.AnimationNodeOutGet().Nodes[0], vec_ctlCNeckStr.AnimationNodeInGet().Nodes[0])
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_R_mouth_stretch", objs)
        ctlRMouthStr = setupRelConst.SetAsSource(objs[0])
        setupRelConst.SetBoxPosition(ctlRMouthStr, 0, 200)
        ctlRMouthStr.UseGlobalTransforms = False
        vec_ctlRMouthStr = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
        setupRelConst.SetBoxPosition(vec_ctlRMouthStr, 300, 200)
        FBConnect(ctlRMouthStr.AnimationNodeOutGet().Nodes[0], vec_ctlRMouthStr.AnimationNodeInGet().Nodes[0])
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_L_neck_stretch", objs)
        ctlLNeckStr = setupRelConst.SetAsSource(objs[0])
        setupRelConst.SetBoxPosition(ctlLNeckStr, 0, 300)
        ctlLNeckStr.UseGlobalTransforms = False
        vec_ctlLNeckStr = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
        setupRelConst.SetBoxPosition(vec_ctlLNeckStr, 300, 300)
        FBConnect(ctlLNeckStr.AnimationNodeOutGet().Nodes[0], vec_ctlLNeckStr.AnimationNodeInGet().Nodes[0])
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_R_neck_stretch", objs)
        ctlRNeckStr = setupRelConst.SetAsSource(objs[0])
        setupRelConst.SetBoxPosition(ctlRNeckStr, 0, 400)
        ctlRNeckStr.UseGlobalTransforms = False
        vec_ctlRNeckStr = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
        setupRelConst.SetBoxPosition(vec_ctlRNeckStr, 300, 400)
        FBConnect(ctlRNeckStr.AnimationNodeOutGet().Nodes[0], vec_ctlRNeckStr.AnimationNodeInGet().Nodes[0])
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_L_neck_stretch_loc", objs)
        ctlLNeckStrLoc = setupRelConst.ConstrainObject(objs[0])
        setupRelConst.SetBoxPosition(ctlLNeckStrLoc, 1600, 300)
        ctlLNeckStrLoc.UseGlobalTransforms = False
        vec_ctlLNeckStrLoc = setupRelConst.CreateFunctionBox("Converters", "Number To Vector")
        setupRelConst.SetBoxPosition(vec_ctlLNeckStrLoc, 1300, 300)
        FBConnect(vec_ctlLNeckStrLoc.AnimationNodeOutGet().Nodes[0], ctlLNeckStrLoc.AnimationNodeInGet().Nodes[0])
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_R_neck_stretch_loc", objs)
        ctlRNeckStrLoc = setupRelConst.ConstrainObject(objs[0])
        setupRelConst.SetBoxPosition(ctlRNeckStrLoc, 1600, 400)
        ctlRNeckStrLoc.UseGlobalTransforms = False
        vec_ctlRNeckStrLoc = setupRelConst.CreateFunctionBox("Converters", "Number To Vector")
        setupRelConst.SetBoxPosition(vec_ctlRNeckStrLoc, 1300, 400)
        FBConnect(vec_ctlRNeckStrLoc.AnimationNodeOutGet().Nodes[0], ctlRNeckStrLoc.AnimationNodeInGet().Nodes[0])
        
        mult1 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
        setupRelConst.SetBoxPosition(mult1, 600, 50)
        FBConnect(vec_ctlLMouthStr.AnimationNodeOutGet().Nodes[1], mult1.AnimationNodeInGet().Nodes[0])
        FBConnect(vec_ctlCNeckStr.AnimationNodeOutGet().Nodes[1], mult1.AnimationNodeInGet().Nodes[1])
        
        mult2 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
        setupRelConst.SetBoxPosition(mult2, 600, 150)
        FBConnect(vec_ctlRMouthStr.AnimationNodeOutGet().Nodes[1], mult2.AnimationNodeInGet().Nodes[0])
        FBConnect(vec_ctlCNeckStr.AnimationNodeOutGet().Nodes[1], mult2.AnimationNodeInGet().Nodes[1])
        
        add1 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
        setupRelConst.SetBoxPosition(add1, 1000, 300)
        FBConnect(mult1.AnimationNodeOutGet().Nodes[0], add1.AnimationNodeInGet().Nodes[0])
        FBConnect(vec_ctlLNeckStr.AnimationNodeOutGet().Nodes[1], add1.AnimationNodeInGet().Nodes[1])
        
        add2 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
        setupRelConst.SetBoxPosition(add2, 1000, 400)
        FBConnect(mult2.AnimationNodeOutGet().Nodes[0], add2.AnimationNodeInGet().Nodes[0])
        FBConnect(vec_ctlRNeckStr.AnimationNodeOutGet().Nodes[1], add2.AnimationNodeInGet().Nodes[1])
        
        FBConnect(add1.AnimationNodeOutGet().Nodes[0], vec_ctlLNeckStrLoc.AnimationNodeInGet().Nodes[1])
        FBConnect(add2.AnimationNodeOutGet().Nodes[0], vec_ctlRNeckStrLoc.AnimationNodeInGet().Nodes[1])
        
        
        # mouth lips press setup
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_L_mouth_lipsPressU", objs)
        in1 = setupRelConst.SetAsSource(objs[0])
        setupRelConst.SetBoxPosition(in1, 0, 600)
        in1.UseGlobalTransforms = False
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_R_mouth_lipsPressU", objs)
        in2 = setupRelConst.SetAsSource(objs[0])
        setupRelConst.SetBoxPosition(in2, 0, 700)
        in2.UseGlobalTransforms = False
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_L_mouth_lipsPressD", objs)
        out1 = setupRelConst.ConstrainObject(objs[0])
        setupRelConst.SetBoxPosition(out1, 400, 600)
        out1.UseGlobalTransforms = False
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_R_mouth_lipsPressD", objs)
        out2 = setupRelConst.ConstrainObject(objs[0])
        setupRelConst.SetBoxPosition(out2, 400, 700)
        out2.UseGlobalTransforms = False
        
        FBConnect(in1.AnimationNodeOutGet().Nodes[0], out1.AnimationNodeInGet().Nodes[0])
        FBConnect(in2.AnimationNodeOutGet().Nodes[0], out2.AnimationNodeInGet().Nodes[0])
        
        # eyes squeeze setup
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_L_eye_squeeze", objs)
        in1 = setupRelConst.SetAsSource(objs[0])
        setupRelConst.SetBoxPosition(in1, 0, 1000)
        in1.UseGlobalTransforms = False
        
        objs = FBComponentList()
        FBFindObjectsByName("CTRL_R_eye_squeeze", objs)
        in2 = setupRelConst.SetAsSource(objs[0])
        setupRelConst.SetBoxPosition(in2, 0, 1100)
        in2.UseGlobalTransforms = False
        
        objs = FBComponentList()
        FBFindObjectsByName("FRM_L_eye_border", objs)
        out1 = setupRelConst.ConstrainObject(objs[0])
        setupRelConst.SetBoxPosition(out1, 1600, 1000)
        out1.UseGlobalTransforms = False
        
        objs = FBComponentList()
        FBFindObjectsByName("FRM_R_eye_border", objs)
        out2 = setupRelConst.ConstrainObject(objs[0])
        setupRelConst.SetBoxPosition(out2, 1600, 1100)
        out2.UseGlobalTransforms = False
        
        vec1 = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
        setupRelConst.SetBoxPosition(vec1, 300, 1000)
        FBConnect(in1.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[0])
        
        vec2 = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
        setupRelConst.SetBoxPosition(vec2, 300, 1100)
        FBConnect(in2.AnimationNodeOutGet().Nodes[0], vec2.AnimationNodeInGet().Nodes[0])
        
        mult1 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
        setupRelConst.SetBoxPosition(mult1, 600, 1000)
        FBConnect(vec1.AnimationNodeOutGet().Nodes[1], mult1.AnimationNodeInGet().Nodes[0])
        mult1.AnimationNodeInGet().Nodes[1].WriteData([-0.7], None)
        
        mult2 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
        setupRelConst.SetBoxPosition(mult2, 600, 1100)
        FBConnect(vec2.AnimationNodeOutGet().Nodes[1], mult2.AnimationNodeInGet().Nodes[0])
        mult2.AnimationNodeInGet().Nodes[1].WriteData([-0.7], None)
        
        add1 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
        setupRelConst.SetBoxPosition(add1, 1000, 1000)
        FBConnect(mult1.AnimationNodeOutGet().Nodes[0], add1.AnimationNodeInGet().Nodes[1])
        add1.AnimationNodeInGet().Nodes[0].WriteData([1.0], None)
        
        add2 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
        setupRelConst.SetBoxPosition(add2, 1000, 1100)
        FBConnect(mult2.AnimationNodeOutGet().Nodes[0], add2.AnimationNodeInGet().Nodes[1])
        add2.AnimationNodeInGet().Nodes[0].WriteData([1.0], None)
        
        vec1 = setupRelConst.CreateFunctionBox("Converters", "Number To Vector")
        setupRelConst.SetBoxPosition(vec1, 1300, 1000)
        FBConnect(add1.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[0])
        FBConnect(add1.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[1])
        FBConnect(add1.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[2])
        
        vec2 = setupRelConst.CreateFunctionBox("Converters", "Number To Vector")
        setupRelConst.SetBoxPosition(vec2, 1300, 1100)
        FBConnect(add2.AnimationNodeOutGet().Nodes[0], vec2.AnimationNodeInGet().Nodes[0])
        FBConnect(add2.AnimationNodeOutGet().Nodes[0], vec2.AnimationNodeInGet().Nodes[1])
        FBConnect(add2.AnimationNodeOutGet().Nodes[0], vec2.AnimationNodeInGet().Nodes[2])
        
        FBConnect(vec1.AnimationNodeOutGet().Nodes[0], out1.AnimationNodeInGet().Nodes[2])
        FBConnect(vec2.AnimationNodeOutGet().Nodes[0], out2.AnimationNodeInGet().Nodes[2])
    
    # create eyes setup
    objs = FBComponentList()
    FBFindObjectsByName("CTRL_L_eye", objs)
    in1 = setupRelConst.SetAsSource(objs[0])
    setupRelConst.SetBoxPosition(in1, 0, 1300)
    in1.UseGlobalTransforms = False
    
    objs = FBComponentList()
    FBFindObjectsByName("CTRL_R_eye", objs)
    in2 = setupRelConst.SetAsSource(objs[0])
    setupRelConst.SetBoxPosition(in2, 0, 1500)
    in2.UseGlobalTransforms = False
    
    objs = FBComponentList()
    FBFindObjectsByName("CTRL_C_eye", objs)
    in3 = setupRelConst.SetAsSource(objs[0])
    setupRelConst.SetBoxPosition(in3, 0, 1400)
    in3.UseGlobalTransforms = False
    
    objs = FBComponentList()
    FBFindObjectsByName("LOC_L_UIDriver", objs)
    out1 = setupRelConst.ConstrainObject(objs[0])
    setupRelConst.SetBoxPosition(out1, 1600, 1300)
    out1.UseGlobalTransforms = False
    
    objs = FBComponentList()
    FBFindObjectsByName("LOC_R_UIDriver", objs)
    out2 = setupRelConst.ConstrainObject(objs[0])
    setupRelConst.SetBoxPosition(out2, 1600, 1500)
    out2.UseGlobalTransforms = False
    
    vec1 = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
    setupRelConst.SetBoxPosition(vec1, 300, 1300)
    FBConnect(in1.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[0])
    
    vec2 = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
    setupRelConst.SetBoxPosition(vec2, 300, 1500)
    FBConnect(in2.AnimationNodeOutGet().Nodes[0], vec2.AnimationNodeInGet().Nodes[0])
    
    vec3 = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
    setupRelConst.SetBoxPosition(vec3, 300, 1400)
    FBConnect(in3.AnimationNodeOutGet().Nodes[0], vec3.AnimationNodeInGet().Nodes[0])
    
    add1 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
    setupRelConst.SetBoxPosition(add1, 600, 1300)
    FBConnect(vec1.AnimationNodeOutGet().Nodes[0], add1.AnimationNodeInGet().Nodes[0])
    FBConnect(vec3.AnimationNodeOutGet().Nodes[0], add1.AnimationNodeInGet().Nodes[1])
    
    add2 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
    setupRelConst.SetBoxPosition(add2, 600, 1350)
    FBConnect(vec1.AnimationNodeOutGet().Nodes[1], add2.AnimationNodeInGet().Nodes[0])
    FBConnect(vec3.AnimationNodeOutGet().Nodes[1], add2.AnimationNodeInGet().Nodes[1])
    
    add3 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
    setupRelConst.SetBoxPosition(add3, 600, 1500)
    FBConnect(vec2.AnimationNodeOutGet().Nodes[0], add3.AnimationNodeInGet().Nodes[0])
    FBConnect(vec3.AnimationNodeOutGet().Nodes[0], add3.AnimationNodeInGet().Nodes[1])
    
    add4 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
    setupRelConst.SetBoxPosition(add4, 600, 1550)
    FBConnect(vec2.AnimationNodeOutGet().Nodes[1], add4.AnimationNodeInGet().Nodes[0])
    FBConnect(vec3.AnimationNodeOutGet().Nodes[1], add4.AnimationNodeInGet().Nodes[1])
    
    mult1 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult1, 1000, 1300)
    FBConnect(add1.AnimationNodeOutGet().Nodes[0], mult1.AnimationNodeInGet().Nodes[0])
    mult1.AnimationNodeInGet().Nodes[1].WriteData([EYESETUP_HOR_RANGE], None)
    
    mult2 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult2, 1000, 1350)
    FBConnect(add2.AnimationNodeOutGet().Nodes[0], mult2.AnimationNodeInGet().Nodes[0])
    mult2.AnimationNodeInGet().Nodes[1].WriteData([-EYESETUP_VERT_RANGE], None)
    
    mult3 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult3, 1000, 1500)
    FBConnect(add3.AnimationNodeOutGet().Nodes[0], mult3.AnimationNodeInGet().Nodes[0])
    mult3.AnimationNodeInGet().Nodes[1].WriteData([EYESETUP_HOR_RANGE], None)
    
    mult4 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult4, 1000, 1550)
    FBConnect(add4.AnimationNodeOutGet().Nodes[0], mult4.AnimationNodeInGet().Nodes[0])
    mult4.AnimationNodeInGet().Nodes[1].WriteData([-EYESETUP_VERT_RANGE], None)
    
    vec1 = setupRelConst.CreateFunctionBox("Converters", "Number To Vector")
    setupRelConst.SetBoxPosition(vec1, 1300, 1300)
    FBConnect(mult1.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[1])
    FBConnect(mult2.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[0])
    
    vec2 = setupRelConst.CreateFunctionBox("Converters", "Number To Vector")
    setupRelConst.SetBoxPosition(vec2, 1300, 1500)
    FBConnect(mult3.AnimationNodeOutGet().Nodes[0], vec2.AnimationNodeInGet().Nodes[1])
    FBConnect(mult4.AnimationNodeOutGet().Nodes[0], vec2.AnimationNodeInGet().Nodes[0])
    
    FBConnect(vec1.AnimationNodeOutGet().Nodes[0], out1.AnimationNodeInGet().Nodes[1])
    FBConnect(vec2.AnimationNodeOutGet().Nodes[0], out2.AnimationNodeInGet().Nodes[1])
    
    # create convergence setup
    objs = FBComponentList()
    FBFindObjectsByName("CTRL_convergenceSwitch", objs)
    in1 = setupRelConst.SetAsSource(objs[0])
    setupRelConst.SetBoxPosition(in1, 0, 1800)
    in1.UseGlobalTransforms = False
    
    objs = FBComponentList()
    FBFindObjectsByName("LOC_L_LookAtTarget", objs)
    out1 = setupRelConst.ConstrainObject(objs[0])
    setupRelConst.SetBoxPosition(out1, 1900, 1800)
    out1.UseGlobalTransforms = False
    
    objs = FBComponentList()
    FBFindObjectsByName("LOC_R_LookAtTarget", objs)
    out2 = setupRelConst.ConstrainObject(objs[0])
    setupRelConst.SetBoxPosition(out2, 1900, 1900)
    out2.UseGlobalTransforms = False
    
    vec1 = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
    setupRelConst.SetBoxPosition(vec1, 300, 1800)
    FBConnect(in1.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[0])
    
    mult1 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult1, 600, 1800)
    FBConnect(vec1.AnimationNodeOutGet().Nodes[1], mult1.AnimationNodeInGet().Nodes[0])
    mult1.AnimationNodeInGet().Nodes[1].WriteData([-1.0], None)
    
    add1 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
    setupRelConst.SetBoxPosition(add1, 900, 1800)
    FBConnect(mult1.AnimationNodeOutGet().Nodes[0], add1.AnimationNodeInGet().Nodes[1])
    add1.AnimationNodeInGet().Nodes[0].WriteData([1.0], None)
    
    mult1 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult1, 1200, 1800)
    FBConnect(add1.AnimationNodeOutGet().Nodes[0], mult1.AnimationNodeInGet().Nodes[0])
    mult1.AnimationNodeInGet().Nodes[1].WriteData([CONVERGENCE_L_HOR_COR], None)
    
    mult2 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult2, 1200, 1850)
    FBConnect(add1.AnimationNodeOutGet().Nodes[0], mult2.AnimationNodeInGet().Nodes[0])
    mult2.AnimationNodeInGet().Nodes[1].WriteData([CONVERGENCE_L_VERT_COR], None)
    
    mult3 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult3, 1200, 1900)
    FBConnect(add1.AnimationNodeOutGet().Nodes[0], mult3.AnimationNodeInGet().Nodes[0])
    mult3.AnimationNodeInGet().Nodes[1].WriteData([CONVERGENCE_R_HOR_COR], None)
    
    mult4 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult4, 1200, 1950)
    FBConnect(add1.AnimationNodeOutGet().Nodes[0], mult4.AnimationNodeInGet().Nodes[0])
    mult4.AnimationNodeInGet().Nodes[1].WriteData([CONVERGENCE_R_VERT_COR], None)
    
    vec1 = setupRelConst.CreateFunctionBox("Converters", "Number To Vector")
    setupRelConst.SetBoxPosition(vec1, 1600, 1800)
    FBConnect(mult1.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[0])
    FBConnect(mult2.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[1])
    vec1.AnimationNodeInGet().Nodes[2].WriteData([0.0], None)
    
    vec2 = setupRelConst.CreateFunctionBox("Converters", "Number To Vector")
    setupRelConst.SetBoxPosition(vec2, 1600, 1900)
    FBConnect(mult3.AnimationNodeOutGet().Nodes[0], vec2.AnimationNodeInGet().Nodes[0])
    FBConnect(mult4.AnimationNodeOutGet().Nodes[0], vec2.AnimationNodeInGet().Nodes[1])
    vec2.AnimationNodeInGet().Nodes[2].WriteData([0.0], None)
    
    FBConnect(vec1.AnimationNodeOutGet().Nodes[0], out1.AnimationNodeInGet().Nodes[0])
    FBConnect(vec2.AnimationNodeOutGet().Nodes[0], out2.AnimationNodeInGet().Nodes[0])
    
    FBSystem().Scene.Evaluate()
    FBSystem().Scene.Evaluate()
    
    # create look at parent switch
    objs = FBComponentList()
    FBFindObjectsByName("CTRL_lookAt_WorldParentSwitch", objs)
    in1 = setupRelConst.SetAsSource(objs[0])
    setupRelConst.SetBoxPosition(in1, 0, 2100)
    in1.UseGlobalTransforms = False
    
    objs = FBComponentList()
    FBFindObjectsByName("GRP_C_lookAt_parentConstraint1", objs)
    theObj = None
    for each in objs:
        if each.ClassName() == "FBConstraint":
            theObj = each
    out1 = setupRelConst.ConstrainObject(theObj)
    setupRelConst.SetBoxPosition(out1, 1500, 2100)
    out1.UseGlobalTransforms = False
    
    vec1 = setupRelConst.CreateFunctionBox("Converters", "Vector To Number")
    setupRelConst.SetBoxPosition(vec1, 300, 2100)
    FBConnect(in1.AnimationNodeOutGet().Nodes[0], vec1.AnimationNodeInGet().Nodes[0])
    
    mult1 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult1, 900, 2100)
    FBConnect(vec1.AnimationNodeOutGet().Nodes[1], mult1.AnimationNodeInGet().Nodes[0])
    mult1.AnimationNodeInGet().Nodes[1].WriteData([100.0], None)
    
    mult2 = setupRelConst.CreateFunctionBox("Number", "Multiply (a x b)")
    setupRelConst.SetBoxPosition(mult2, 600, 2200)
    FBConnect(vec1.AnimationNodeOutGet().Nodes[1], mult2.AnimationNodeInGet().Nodes[0])
    mult2.AnimationNodeInGet().Nodes[1].WriteData([-100.0], None)
    
    add1 = setupRelConst.CreateFunctionBox("Number", "Add (a + b)")
    setupRelConst.SetBoxPosition(add1, 900, 2200)
    FBConnect(mult2.AnimationNodeOutGet().Nodes[0], add1.AnimationNodeInGet().Nodes[1])
    add1.AnimationNodeInGet().Nodes[0].WriteData([100.0], None)
    
    FBConnect(mult1.AnimationNodeOutGet().Nodes[0], out1.AnimationNodeInGet().Nodes[0])
    FBConnect(add1.AnimationNodeOutGet().Nodes[0], out1.AnimationNodeInGet().Nodes[1])
    
    #Setup Folder
    constFolder = None
    
    for folder in FBSystem().Scene.Folders:
        if folder.Name == "3Lateral":
            constFolder = folder
    
    for each in constList:
        if constFolder == None:
            constFolder = FBFolder( "3Lateral", each )
        else:
            constFolder.Items.append( each )
    



def rs_CSScaleHead():
    
    scaleRoot = FBFindModelByLabelName("SKEL_Head")
    scaleFactor = 1
                        
    charName = os.path.basename( FBApplication().FBXFileName ).split(".")[0]
    scaleValue = rs_getScaleFromPedsMeta( charName )
    
    scaleVal = scaleFactor * scaleValue
    scaleRoot.Scaling.Data = FBVector3d( scaleVal, scaleVal, scaleVal )
    
    FBMessageBox("Character Head Scale", "Character's head has now been scaled to {0}".format( scaleValue ), "Ok")

def setupUFCDOFS():
    # Sync and merge temp objects
    lPath = RS.Config.Project.Path.Root + "\\art\\peds\\3LateralSetup\\Mobu_Rigs\\AllRigs.fbx"

    p4.Sync( lPath )
    if os.path.isfile( lPath ):
        FBApplication().FileMerge( lPath )
        
        # Find temp objects
        facialRoot = "FACIAL_C_FacialRoot"
        tempObjs = FBComponentList()
        FBFindObjectsByName( facialRoot, tempObjs, False, True )
        
        # Store temp objects in array to remove later
        removeThese = []
        for obj in tempObjs:
            if len( obj.LongName.split(":") ) > 1:
                tempNamespace = obj.LongName.split(":")[0]
                removeThese.append( tempNamespace )
                    
        # Find and spec out all CTRL DOFs
        ctrlList = FBComponentList()
        FBFindObjectsByName( "CTRL_*", ctrlList, True, True )
        
        ctrlPropList = [
            "TranslationActive",
            "TranslationMin",
            "TranslationMax",
            "TranslationMinX",
            "TranslationMinY",
            "TranslationMinZ",
            "TranslationMaxX",
            "TranslationMaxY",
            "TranslationMaxZ"
            ]
        
        # Copy DOFs from any matching temp objects
        for ctrl in ctrlList :
            if len( ctrlList ) > 100:
                lNS = "Biped"
                ctrl_ref = FBFindModelByLabelName( lNS + ":" + ctrl.Name )
                ctrl_new = ctrl
                
                if ctrl_ref and ctrl_new:
                    for prop in ctrlPropList:
                        ctrl_new.PropertyList.Find( prop ).Data = ctrl_ref.PropertyList.Find( prop ).Data
            else:
                for lNS in removeThese:
                    ctrl_ref = FBFindModelByLabelName( lNS + ":" + ctrl.Name )
                    ctrl_new = ctrl
                    
                    if ctrl_ref and ctrl_new:
                        for prop in ctrlPropList:
                            ctrl_new.PropertyList.Find( prop ).Data = ctrl_ref.PropertyList.Find( prop ).Data
                        
        # Look for all temporary merged assets, and remove.
        for lNS in removeThese:
            FBSystem().Scene.NamespaceDeleteContent(lNS, FBPlugModificationFlag.kFBPlugAllContent, True)                    
    
    else:
        print "Files not found, could not merge temp objects"
    
def setupSceneForUFC():
    
                        
    # Other setup
    ## Setup Camera
    for cam in FBSystem().Scene.Cameras:
        if cam.Name == "Producer Perspective":
            for each in cam.PropertyList:
                cam.PropertyList.Find("NearPlane").Data = 0.01
                cam.PropertyList.Find("FieldOfView").Data = 50
                
    ## STRIP LAYERS
    while FBSystem().CurrentTake.GetLayerCount() > 1:
        FBSystem().CurrentTake.GetLayer(1).FBDelete()
        
    ## SNAP ON FRAMES
    FBPlayerControl().SnapMode = FBTransportSnapMode.kFBTransportSnapModeSnapOnFrames        

def setupUFCPreRotation( lNamespace ):
    
        
    # Scale Geo Up
    headGuiRoot = FBFindModelByLabelName( lNamespace + ":head" )
    charName = os.path.basename( FBApplication().FBXFileName ).split(".")[0]
    scaleFactor = 100
    scaleValue = scaleFactor * rs_getScaleFromPedsMeta( charName )
    headGuiRoot.Scaling.Data = FBVector3d( scaleValue, scaleValue, scaleValue )
                
    # Find and update all Rotations and Pre Rotations for Facial Bones
    objectList = FBComponentList()
    FBFindObjectsByName( lNamespace + ":FACIAL_*", objectList, True, True )
    otherObjectList = [
        "LOC_C_facialRoot",
        "OH_LookAtRoot"
    ]
    
    for obj_ref in objectList:
        obj_new = FBFindModelByLabelName( obj_ref.Name )
        
        obj_new.Rotation.Data = obj_ref.Rotation.Data
        obj_new.PreRotation = obj_ref.PreRotation
        #obj_new.Translation.Data = obj_ref.Translation.Data
        if obj_ref.RotationActive:
            obj_new.RotationActive = True
            
        obj_new.Selected = True
        
    for rootName in otherObjectList :
        rootNode = FBFindModelByLabelName( rootName )
        rootArray = findAllChildren( rootNode )
        
        for each in rootArray:
            
            obj_ref = FBFindModelByLabelName( lNamespace + ":" + each.Name )
            obj_new = each
            
            if obj_ref and obj_new:
                
                obj_new.Rotation.Data = obj_ref.Rotation.Data
                obj_new.PreRotation = obj_ref.PreRotation
                #obj_new.Translation.Data = obj_ref.Translation.Data
                obj_new.Selected = True
    
    # Copy the Global Controller transforms
    
    a = FBFindModelByLabelName("GRP_convergenceGUI")
    a.Selected = True
    
    for obj in findAllChildren(a):
        b = FBFindModelByLabelName( lNamespace + ":" + obj.Name )
        if b!= None:
            obj.Translation.Data = b.Translation.Data
            obj.Rotation.Data = b.Rotation.Data
            obj.Scaling.Data = b.Scaling.Data
            obj.isHidden = False
    
    
def setupUFCFacialGroups():
    # Find all facial components
    guiList = []
    lChildren = []
    lNodes = ["head", "FACIAL_C_NeckRoot", "FACIAL_C_FacialRoot" ]

    for lName in lNodes:
        findObj = FBFindModelByLabelName(lName)
        if findObj != None:
            guiList.extend(findAllChildren(findObj))
            
            
    if len( guiList ) > 0:
    
        # Clear facial components from all groups
        allGroups = FBSystem().Scene.Groups
        for group in allGroups:
            removeItems = []
            for item in group.Items:
                if item in guiList:
                    removeItems.append( item )
            for delItem in removeItems:
                group.Items.remove( delItem )
    
        # Define all facial groups
        lApp = FBApplication().FBXFileName
        lRootGroup_Name = lApp.split("\\")[len(lApp.split("\\")) - 1].split(".")[0]
    
        lRootGroup = None
        lFacialRootGroup_Name = "Facial"
    
        lFacialGroup_Name = "Main"
        lCtrlGroup_Name = "mainControls"
        lGuiGroup_Name = "mainGUI"
    
        lFacialGroupEyes_Name = "Eyes"
        lCtrlGroupEyes_Name = "eyeControls"
        lGuiGroupEyes_Name = "eyeGUI"
    
        lBonesGroup_Name = "Facial Bones"
        lOtherGroup_Name = "Other (Unsorted)"
    
        # Remove all facial Groups (and find the root groups)
        groupsToDel = []
        for group in allGroups:
            if group.Name == lFacialGroup_Name:
                groupsToDel.append( group )
            if group.Name == lCtrlGroup_Name:
                groupsToDel.append( group )
            if group.Name == lGuiGroup_Name:
                groupsToDel.append( group )
            if group.Name == lOtherGroup_Name:
                groupsToDel.append( group )
            if group.Name == lBonesGroup_Name:
                groupsToDel.append( group )
            if group.Name == lFacialRootGroup_Name:
                groupsToDel.append( group )
            if group.Name == lFacialGroupEyes_Name:
                groupsToDel.append( group )
            if group.Name == lCtrlGroupEyes_Name:
                groupsToDel.append( group )
            if group.Name == lGuiGroupEyes_Name:
                groupsToDel.append( group )
                
            if group.Name == lRootGroup_Name:
                lRootGroup = group
                
        for item in groupsToDel:
            item.FBDelete()
        
        # Create all facial Groups
        
        lFacialRootGroup = FBGroup( lFacialRootGroup_Name )
        
        lFacialGroup = FBGroup( lFacialGroup_Name )
        lCtrlGroup = FBGroup( lCtrlGroup_Name )
        lGuiGroup = FBGroup( lGuiGroup_Name )
        
        lFacialGroupEyes = FBGroup( lFacialGroupEyes_Name )
        lCtrlGroupEyes = FBGroup( lCtrlGroupEyes_Name )
        lGuiGroupEyes = FBGroup( lGuiGroupEyes_Name )
        
        lBonesGroup = FBGroup( lBonesGroup_Name )
        lOtherGroup = FBGroup( lOtherGroup_Name )
        
        # Parent the groups
        lFacialRootGroup.ConnectSrc( lFacialGroup )
        lFacialGroup.ConnectSrc( lCtrlGroup )
        lFacialGroup.ConnectSrc( lGuiGroup )
        lFacialRootGroup.ConnectSrc( lFacialGroupEyes )
        lFacialGroupEyes.ConnectSrc( lCtrlGroupEyes )
        lFacialGroupEyes.ConnectSrc( lGuiGroupEyes )
        if lRootGroup != None:
            lRootGroup.ConnectSrc( lFacialRootGroup )
            lRootGroup.ConnectSrc( lBonesGroup )
            lRootGroup.ConnectSrc( lOtherGroup )
    
        # Add components to group
        for each in guiList:
            if "CTRL_faceGUI" == each.Name:
                if each not in lGuiGroup.Items:
                    lGuiGroup.Items.append( each )
            elif "CTRL_rig_Logic" == each.Name:
                if each not in lGuiGroup.Items:
                    lGuiGroup.Items.append( each )
            elif "CTRL_R_neck_stretch_loc" == each.Name or "CTRL_L_neck_stretch_loc" == each.Name:
                if each not in lOtherGroup.Items:
                    lOtherGroup.Items.append( each )
            elif "ctrl_l_lookat" == each.Name.lower() or "ctrl_r_lookat" == each.Name.lower() :
                if each not in lCtrlGroupEyes.Items:
                    lCtrlGroupEyes.Items.append( each )
            elif "CTRL_convergenceSwitch" == each.Name or "ctrl_c_lookat" == each.Name.lower() :
                if each not in lCtrlGroupEyes.Items:
                    lCtrlGroupEyes.Items.append( each )
            elif "CTRL" == each.Name.split("_")[0]:
                if each not in lCtrlGroup.Items:
                    lCtrlGroup.Items.append( each )
            elif "FRM_convergenceGUI" == each.Name or "FRM_convergenceSwitch" == each.Name:
                if each not in lGuiGroupEyes.Items:
                    lGuiGroupEyes.Items.append( each )
            elif "FRM" == each.Name.split("_")[0]:
                if each not in lGuiGroup.Items:
                    lGuiGroup.Items.append( each )
            elif "TEXT_convergence" == each.Name:
                if each not in lGuiGroupEyes.Items:
                    lGuiGroupEyes.Items.append( each )
            elif "TEXT" == each.Name.split("_")[0]:
                if each not in lGuiGroup.Items:
                    lGuiGroup.Items.append( each )
            elif "FACIAL" == each.Name.split("_")[0]:
                if each not in lBonesGroup.Items:
                    lBonesGroup.Items.append( each )
            else:
                if each not in lOtherGroup.Items:
                    lOtherGroup.Items.append( each )
    
        # Initialize Group Settings
        lFacialRootGroup.Pickable = False
        lFacialRootGroup.Show = False
        lFacialRootGroup.Transformable = False
        lFacialRootGroup.Pickable = True
        lFacialRootGroup.Show = True
        lFacialRootGroup.Transformable = True
        
        # Setup Group Settings
        lGuiGroup.Pickable = False
        lGuiGroup.Transformable = False
        lGuiGroupEyes.Pickable = False
        lGuiGroupEyes.Transformable = False
        lBonesGroup.Pickable = False
        lBonesGroup.Show = False
        lBonesGroup.Transformable = False
        lOtherGroup.Pickable = False
        lOtherGroup.Show = False
        lOtherGroup.Transformable = False


def rs_getScaleFromPedsMeta( charName ):
    
    scaleValue = 1.0    
    gProjRoot = RS.Config.Project.Path.Root
    pXmlFileName = gProjRoot + "\\assets\\export\\data\\peds.pso.meta"
    p4.Sync( pXmlFileName )
    
    if(os.path.exists(pXmlFileName)):
        charName = os.path.basename( FBApplication().FBXFileName ).split(".")[0]
        lXmlFile = minidom.parse( pXmlFileName )
        rootData = lXmlFile.getElementsByTagName("InitDatas")
        charData =  rootData[0].getElementsByTagName("Name")
        scaleData = rootData[0].getElementsByTagName("CutsceneScale")
        for char in charData:
            if char.childNodes[0].data == charName:
                for scale in scaleData:
                    if char.parentNode == scale.parentNode:
                        scaleValue = float( scale.getAttribute("value") )
                        
    else:
        print "{0} file does not exist locally.".format( pXmlFileName )
                        
    return float( scaleValue )


def findExpressionXML( exprFolder ):
    p4.Sync( exprFolder + "..." )
    if os.path.exists( exprFolder ):
        fileList = os.listdir( exprFolder )
        for each in fileList:
            if each.startswith( "RigFATXML_" ):
                if each.endswith( ".xml" ):
                    return each
    return None


def createDevice():
    # Clear all expression solver devices
    remove = []
    for device in FBSystem().Scene.Devices:
        if device.ClassName() == "RSExprSolverDevice":
            remove.append( device )
    for device in remove:
        device.FBDelete()
    
    # Create device
    exprSolver = FBCreateObject( "Browsing/Templates/Devices","RS Expression Solver", "RS Expression Solver" )
    if exprSolver:
        FBSystem().Scene.Devices.append(exprSolver)
        #exprSolver.Online = True
    
    ### SETUP Device
    missingSetting = False
    
    # Determine whether facial rig needs to be scaled
    setScale = True    
    if "cutscene" in os.path.dirname( FBApplication().FBXFileName ):
        setScale = True
    else:
        setScale = False
    
    # Set Char name, and change char name to CS_ if it's IG_
    charName = os.path.basename( FBApplication().FBXFileName ).split(".")[0]
    if charName.startswith("IG_"):
        charName = charName.replace("IG_", "CS_")
        setScale = False
    
    # Set folder name, where skel file and expression files are stored.
    skelFolder = "x:/rdr3/assets/anim/expressions/{0}_FACE/".format( charName )
    exprFolder = "x:/rdr3/assets/anim/expressions/{0}_FACE/".format( charName )
    
    # Set the skel file
    skelFile = skelFolder + "skeleton.skel"
    p4.Sync( skelFile )
    if os.path.exists( skelFile ):
        cdll.MBExprSolver.SetSkel_Py( skelFile )
    else:
        print "SKEL file does not exist locally:"
        print "  {0}".format( skelFile )
        missingSetting = True
    
    # Set the BoneList
    #cdll.MBExprSolver.SetBonelist_Py("X:/rdr3/assets/metadata/characters/skeleton/masterlists/character.bonelist")
    
    # Clear and set the expression file
    cdll.MBExprSolver.ClearExprs_Py()
    exprFile = findExpressionXML( exprFolder )
    print exprFile
    if exprFile != None:
        exprFile = exprFolder + exprFile
        cdll.MBExprSolver.AddExpr_Py( exprFile )
    else:
        print "XML file does not exist locally:"
        missingSetting = True
    
    # Set the Scaling
    if setScale:
        charScale = rs_getScaleFromPedsMeta( charName )
        cdll.MBExprSolver.SetScale_Py(c_double( charScale ) )
    else:
        print "This character does not need scale."
        
        
    # Activate Device
    if not missingSetting:
        cdll.MBExprSolver.SetOnline_Py()
        FBMessageBox("Expression Solver", "Sucess!  Expression Solver has been added.", "Ok")
    else:
        print "Missing a setting or file. Removing all devices"
        remove = []
        for device in FBSystem().Scene.Devices:
            if device.ClassName() == "RSExprSolverDevice":
                remove.append( device )
        for device in remove:
            device.FBDelete()
        FBMessageBox("Expression Solver", "Failed...  Expression solver could not be added.", "Ok")