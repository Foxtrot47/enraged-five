'''

 Script Path: RS/Core/Face/FacewareLive.py

 Written And Maintained By: Kristine Middlemiss & Kathryn Bodey

 Created: 19.06.14

 Description: modules to load and delete faceware assets for animation transfer.  
              Referenced in FacewareToolbox.py
              url:bugstar:1822499

Definitions: __init__, CheckFaceWareExists, MergeFBXOptions, MergeHeadFile, MergeConstraintsFile, CreateReceiverBox,
			 SetupTranferConstraint, SetupParentConstraint, FacewareOrganization, AddDevice, WriteCharacterXML


'''


from pyfbsdk import *

import RS.Config
import RS.Utils.Scene
import RS.Globals
import RS.Perforce
from RS.Utils.Scene import Constraint

import os
from xml.etree import ElementTree as et


class FacewareConstraint():


        def __init__(self):

                self.Namespace = None
                self.ParentConst = None
                self.TransferConstraint = None
                self.FaceWareComponents = []
                self.FaceWareConstraintComponents = []
                self.FacewareHead = None  
                self.FacewareScale = None
                self.FacewareDict = {} # [character]:(FaceMergeBool, ConstraintMergeBool, DeviceBool )
                self.FacewareXMLPath = 'x:\\virtualproduction\\previz\\globalAssets\\facewareXML\\projects\\bob\\'

        def CheckFaceWareExists( self, character ):

                # refresh class variables
                self.__init__()

                # get self.Namespace
                splitName = character.LongName.split(':')
                self.Namespace = splitName[0]

                # search if faceware for this character already exists
                facewareHeadJnt = RS.Utils.Scene.FindModelByName( self.Namespace + '_Head_Jnt' )

                if facewareHeadJnt:

                        # faceware head exists.
                        # look for faceware constraint
                        foundConstraint = False
                        for i in RS.Globals.Constraints:

                                if i.Name == self.Namespace + '_FACEWARE_Transfer':
                                        self.FacewareDict[ self.Namespace ] = ( False, False )
                                        foundConstraint = True
                                        #print character.LongName + ': found head & constraint'


                        if foundConstraint == False:
                                self.FacewareDict[ self.Namespace ] = ( False, True )  

                                # get variables for head and scale
                                facewareHead = RS.Utils.Scene.FindModelByName( self.Namespace + '_Head' )
                                if facewareHead:
                                        self.FacewareHead = facewareHead
                                facewareScale = RS.Utils.Scene.FindModelByName( self.Namespace + '_Scale' )
                                if facewareScale:
                                        self.FacewareScale = facewareScale
                                #print character.LongName + ': found head. Missing constraint'


                else:
                        # faceware and constraint do not exist
                        self.FacewareDict[ self.Namespace ] = ( True, True )
                        #print character.LongName + ': Missing head. Missing constraint'


                # check if device exists for character
                foundDevice = False
                for i in RS.Globals.Devices:
                        if i.Name == self.Namespace+ '_Faceware Live Client Device':
                                foundDevice = True        

                for key, value in self.FacewareDict.iteritems():
                        if foundDevice == False:
                                self.FacewareDict[key] = value[0], value[1], True
                        else:
                                self.FacewareDict[key] = value[0], value[1], False

                return self.FacewareDict


        def MergeFBXOptions( self ):
                options = FBFbxOptions( True ) # True for Load, not Save 
                options.NamespaceList = "PlaceholderNamespace" # placeholder self.Namespace so we can find merged-in items, this will be removed once we find them again        
                options.BaseCameras = False
                options.CameraSwitcherSettings = False
                options.CurrentCameraSettings = False
                options.GlobalLightingSettings = False
                options.TransportSettings = False    
                return options


        def MergeHeadFile( self ):
                # get latest and merge Faceware_Transfer_Constraint.fbx
                transferPath = '{0}/art/animation/resources/mocap/FACEWARE/Faceware_Victor_MotionBuilder/Victor_Live_MoBu.fbx'.format( RS.Config.Project.Path.Root ) 
                RS.Perforce.Sync( transferPath )

                options = self.MergeFBXOptions()

                app = FBApplication()

                if os.path.exists( transferPath ):
                        app.FileMerge( transferPath, False, options )

                        # find newly merged faceware items and add to a list
                        for i in RS.Globals.gComponents:
                                if i.LongName.startswith( "PlaceholderNamespace" ):
                                        if i.ClassName() in [ 'FBModelNull', 'FBModel', 'FBModelSkeleton', 'FBMaterial', 'FBTexture', 'FBVideoClipImage' ]:
                                                self.FaceWareComponents.append( i )

                        for i in self.FaceWareComponents:

                                facewareProperty = i.PropertyCreate('Faceware Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                                facewareProperty.Data = 'victor head'

                                if i.Name.endswith( '_Head' ) and i.ClassName() == 'FBModel':
                                        self.FacewareHead = i

                                if '_Scale' in i.Name and i.ClassName() == 'FBModelNull':
                                        self.FacewareScale = i

                                # remove placeholder self.Namespace
                                i.ProcessObjectNamespace( FBNamespaceAction.kFBRemoveAllNamespace, '', '' )

                                # add new name
                                split = i.Name.split( 'Victor_' )
                                i.Name = self.Namespace + '_' + split[-1]
                else:
                        FBMessageBox( 'R* Error', 'Unable to find file:\n\n{0}'.format( transferPath ), 'Ok' )

        def MergeConstraintFile( self ):
                # get latest and merge Faceware_Transfer_Constraint.fbx
                transferPath = '{0}/art/animation/resources/mocap/FACEWARE/Faceware_Victor_MotionBuilder/Faceware_Transfer_Constraint.fbx'.format( RS.Config.Project.Path.Root ) 
                RS.Perforce.Sync( transferPath )

                options = self.MergeFBXOptions()  
                options.Constraints = FBElementAction.kFBElementActionMerge # Except for the actor and its animation
                #options.SetAll( FBElementAction.kFBElementActionDiscard, False ) # tell that by default we won't save anything    

                app = FBApplication()

                if os.path.exists( transferPath ):
                        app.FileMerge( transferPath, False, options )

                        # find newly merged faceware items and add to a list
                        for i in RS.Globals.gComponents:
                                if i.LongName.startswith( "PlaceholderNamespace" ):
                                        if i.ClassName() in [ 'FBModelNull', 'FBModel', 'FBModelSkeleton', 'FBMaterial', 'FBTexture', 'FBVideoClipImage', 'FBConstraintRelation', 'FBBox' ]:
                                                self.FaceWareConstraintComponents.append( i )

                        for i in self.FaceWareConstraintComponents:

                                if i.Is( FBConstraintRelation_TypeInfo() ):

                                        self.TransferConstraint = i

                                facewareProperty = i.PropertyCreate('Faceware Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                                facewareProperty.Data = 'faceware constraint'                          

                                # remove placeholder namespace
                                i.ProcessObjectNamespace( FBNamespaceAction.kFBRemoveAllNamespace, '', '' )

                                # add new name
                                i.Name = self.Namespace + '_' + i.Name

                if self.TransferConstraint:
                        self.SetupTranferConstraint()
                        self.SetupParentConstraint()
                        self.FacewareOrganization()
                else:
                        FBMessageBox( 'R* Error', 'Unable to find file:\n\n{0}'.format( transferPath ), 'Ok' )

        def SetupUFCConstraint( self ):
                self.TransferConstraint = FBConstraintRelation( self.Namespace + '_FACEWARE_Transfer')
                facewareProperty = self.TransferConstraint.PropertyCreate('Faceware Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                facewareProperty.Data = 'faceware constraint'
                self.TransferConstraint.Active = True

                if self.FacewareHead:
                        # need to set the head blendShape properties animatable
                        for prop in self.FacewareHead.PropertyList:
                                if prop.Name.endswith( "Shape" ):
                                        if not prop.IsAnimated():
                                                prop.SetAnimated( True )
                # create head sender
                headSender = None
                headSender = self.CreateSenderBox(self.Namespace + '_Head', 0, 0)

                if headSender:
                        # create function boxes
                        lBrowDownDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 0, writeDataA=None, writeDataB=100, writeDataC=None)
                        lBrowDownAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 0)
                        lBrowDownNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 0)
                        rBrowDownDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 100, writeDataA=None, writeDataB=100, writeDataC=None)
                        rBrowDownAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 100)
                        rBrowDownNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 100)
                        browRaiseInDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 200, writeDataA=None, writeDataB=100, writeDataC=None)
                        browRaiseInAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 200)
                        browRaiseInNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 200)
                        rbrowRaiseOutDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 300, writeDataA=None, writeDataB=100, writeDataC=None)
                        rbrowRaiseOutAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 300)
                        rbrowRaiseOutNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 300)
                        lbrowRaiseOutDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 400, writeDataA=None, writeDataB=100, writeDataC=None)
                        lbrowRaiseOutAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 400)
                        lbrowRaiseOutNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 400)
                        browLateralDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 500, writeDataA=None, writeDataB=100, writeDataC=None)
                        browLateralAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 500)
                        browLateralNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 500)
                        rEyeBlinkDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 600, writeDataA=None, writeDataB=100, writeDataC=None)
                        rEyeBlinkDiv2 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 800, 600, writeDataA=None, writeDataB=-100, writeDataC=None)
                        rEyeBlinkAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 1100, 600)
                        rEyeBlinkNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1400, 600)
                        lEyeBlinkDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 700, writeDataA=None, writeDataB=100, writeDataC=None)
                        lEyeBlinkDiv2 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 800, 700, writeDataA=None, writeDataB=-100, writeDataC=None)
                        lEyeBlinkAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 1100, 700)
                        lEyeBlinkNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector',1400, 700)
                        cEyeDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 800, writeDataA=None, writeDataB=-100, writeDataC=None)
                        cEyeDiv2 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 800, 800, writeDataA=None, writeDataB=100, writeDataC=None)
                        cEyeDiv3 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 1100, 800, writeDataA=None, writeDataB=-100, writeDataC=None)
                        cEyeDiv4 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 1400, 800, writeDataA=None, writeDataB=100, writeDataC=None)
                        cEyeAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 1700, 800)
                        cEyeAdd2 = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 2000, 800)
                        cEyeNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 2300, 800)
                        rMouthCornerDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 900, writeDataA=None, writeDataB=-100, writeDataC=None)
                        rMouthCornerDiv2 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 800, 900, writeDataA=None, writeDataB=100, writeDataC=None)
                        rMouthCornerAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 1100, 900)
                        rMouthCornerNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1400, 900)
                        lMouthCornerDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1000, writeDataA=None, writeDataB=-100, writeDataC=None)
                        lMouthCornerDiv2 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 800, 1000, writeDataA=None, writeDataB=100, writeDataC=None)
                        lMouthCornerAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 1100, 1000)
                        lMouthCornerNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1400, 1000)
                        cMouthDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1100, writeDataA=None, writeDataB=-100, writeDataC=None)
                        cMouthDiv2 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 800,1100, writeDataA=None, writeDataB=100, writeDataC=None)
                        cMouthDiv3 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 1100, 1100, writeDataA=None, writeDataB=-100, writeDataC=None)
                        cMouthDiv4 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 1400, 1100, writeDataA=None, writeDataB=100, writeDataC=None)
                        cMouthAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 1700, 1100)
                        cMouthAdd2 = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 2000, 1100)
                        cMouthNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 2300, 1100)
                        cJawDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1200, writeDataA=None, writeDataB=100, writeDataC=None)
                        cJawDiv2 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 800, 1200, writeDataA=None, writeDataB=-50, writeDataC=None)
                        cJawDiv3 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 1100, 1200, writeDataA=None, writeDataB=50, writeDataC=None)
                        cJawAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 1400, 1200)
                        cJawNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1700, 1200)
                        noseDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1300, writeDataA=None, writeDataB=100, writeDataC=None)
                        noseDiv2 = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 800, 1300, writeDataA=None, writeDataB=-100, writeDataC=None)
                        noseAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 1100, 1300)
                        noseNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1400, 1300)
                        lFunnelDDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1400, writeDataA=None, writeDataB=100, writeDataC=None)
                        lFunnelDAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 1400)
                        lFunnelDNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 1400)
                        lFunnelUDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1500, writeDataA=None, writeDataB=100, writeDataC=None)
                        lFunnelUAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 1500)
                        lFunnelUNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 1500)
                        rFunnelDDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1600, writeDataA=None, writeDataB=100, writeDataC=None)
                        rFunnelDAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 1600)
                        rFunnelDNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 1600)
                        rFunnelUDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1700, writeDataA=None, writeDataB=100, writeDataC=None)
                        rFunnelUAdd = self.NumberBox(self.TransferConstraint, 'Add (a + b)', 800, 1700)
                        rFunnelUNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 1100, 1700)
                        mouthPuckerDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1800, writeDataA=None, writeDataB=100, writeDataC=None)
                        mouthPuckerNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 800, 1800)
                        lipBiteDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 1900, writeDataA=None, writeDataB=100, writeDataC=None)
                        lipBiteNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 800, 1900)
                        lMouthOpenSmileDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 2000, writeDataA=None, writeDataB=100, writeDataC=None)
                        lMouthOpenSmileNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 800, 2000)
                        rMouthOpenSmileDiv = self.NumberBox(self.TransferConstraint, 'Divide (a/b)', 500, 2100, writeDataA=None, writeDataB=100, writeDataC=None)
                        rMouthOpenSmileNumVec = self.ConverterBox(self.TransferConstraint, 'Number to Vector', 800, 2100)

                        # create receiver boxes
                        rBrowRaiseOutReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_brow_raiseOut', 2600, 300)
                        rBrowRaiseInReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_brow_raiseIn', 2900, 200)
                        lBrowRaiseOutReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_brow_raiseOut', 2600, 400)
                        lBrowRaiseInReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_brow_raiseIn', 2600, 200)
                        rBrowDownReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_brow_down', 2600, 100)
                        lBrowDownReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_brow_down', 2600, 0)
                        rBrowLateralReciever = self.CreateReceiverBox(self.Namespace + ':CTRL_R_brow_lateral', 2600, 500)
                        lBrowLateralReciever = self.CreateReceiverBox(self.Namespace + ':CTRL_L_brow_lateral', 2900, 500)
                        lEyeBlinkReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_eye_blink', 2600, 700)
                        rEyeBlinkReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_eye_blink', 2600, 600)
                        cEyeReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_C_eye', 2600, 800)
                        lMouthFunnelUReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_mouth_funnelU', 2600, 1500)
                        lMouthFunnelDReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_mouth_funnelD', 2600, 1400)
                        rMouthFunnelUReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_mouth_funnelU', 2600, 1700)
                        rMouthFunnelDReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_mouth_funnelD', 2600, 1600)
                        lMouthCornerReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_mouth_corner', 2600, 1000)
                        rMouthCornerReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_mouth_corner', 2600, 900)
                        cMouthReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_C_mouth', 2600, 1100)
                        cJawReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_C_jaw', 2600, 1200)
                        rMouthPuckerDReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_mouth_puckerD', 2600, 1800)
                        rMouthPuckerUReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_mouth_puckerU', 2900, 1800)
                        lMouthPuckerDReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_mouth_puckerD', 3200, 1800)
                        lMouthPuckerUReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_mouth_puckerU', 3500, 1800)
                        lMouthOpenSmileReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_mouth_openSmile', 2600, 2000)
                        rMouthOpenSmileReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_mouth_openSmile', 2600, 2000)
                        rMouthLipBiteDReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_mouth_lipBiteD', 2600, 1900)
                        rMouthLipBiteUReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_mouth_lipBiteU', 2900, 1900)
                        lMouthLipBiteDReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_mouth_lipBiteU', 3200, 1900)
                        lMouthLipBiteUReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_mouth_lipBiteD', 3500, 1900)
                        lNoseReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_L_nose', 2600, 1300)
                        rNoseReceiver = self.CreateReceiverBox(self.Namespace + ':CTRL_R_nose', 2900, 1400)

                        # set up connections
                        if rBrowLateralReciever and lBrowLateralReciever:
                                RS.Utils.Scene.ConnectBox(headSender, 'Brows_Down_CenterShape', browLateralDiv, 'a')
                                RS.Utils.Scene.ConnectBox(browLateralDiv, 'Result', browLateralAdd, 'a')
                                RS.Utils.Scene.ConnectBox(browLateralAdd, 'Result', browLateralNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(browLateralNumVec, 'Result', rBrowLateralReciever, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(browLateralNumVec, 'Result', lBrowLateralReciever, 'Lcl Translation')
                        if lBrowDownReceiver and rBrowDownReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Brows_Down_LeftShape', lBrowDownDiv, 'a')
                                RS.Utils.Scene.ConnectBox(lBrowDownDiv, 'Result', lBrowDownAdd, 'a')
                                RS.Utils.Scene.ConnectBox(lBrowDownAdd, 'Result', lBrowDownNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(lBrowDownNumVec, 'Result', lBrowDownReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(headSender, 'Brows_Down_RightShape', rBrowDownDiv, 'a')
                                RS.Utils.Scene.ConnectBox(rBrowDownDiv, 'Result', rBrowDownAdd, 'a')
                                RS.Utils.Scene.ConnectBox(rBrowDownAdd, 'Result', rBrowDownNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(rBrowDownNumVec, 'Result', rBrowDownReceiver, 'Lcl Translation')
                        if rBrowRaiseInReceiver and lBrowRaiseInReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Brows_Up_CenterShape', browRaiseInDiv, 'a')
                                RS.Utils.Scene.ConnectBox(browRaiseInDiv, 'Result', browRaiseInAdd, 'a')
                                RS.Utils.Scene.ConnectBox(browRaiseInAdd, 'Result', browRaiseInNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(browRaiseInNumVec, 'Result', lBrowRaiseInReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(browRaiseInNumVec, 'Result', rBrowRaiseInReceiver, 'Lcl Translation')
                        if lBrowRaiseOutReceiver and rBrowRaiseOutReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Brows_Up_LeftShape', lbrowRaiseOutDiv, 'a')
                                RS.Utils.Scene.ConnectBox(lbrowRaiseOutDiv, 'Result', lbrowRaiseOutAdd, 'a')
                                RS.Utils.Scene.ConnectBox(lbrowRaiseOutAdd, 'Result', lbrowRaiseOutNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(lbrowRaiseOutNumVec, 'Result', lBrowRaiseOutReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(headSender, 'Brows_Up_RightShape', rbrowRaiseOutDiv, 'a')
                                RS.Utils.Scene.ConnectBox(rbrowRaiseOutDiv, 'Result', rbrowRaiseOutAdd, 'a')
                                RS.Utils.Scene.ConnectBox(rbrowRaiseOutAdd, 'Result', rbrowRaiseOutNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(rbrowRaiseOutNumVec, 'Result', rBrowRaiseOutReceiver, 'Lcl Translation')
                        if lEyeBlinkReceiver and rEyeBlinkReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Eyes_Blink_LeftShape', lEyeBlinkDiv, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Eyes_Wide_LeftShape', lEyeBlinkDiv2, 'a')
                                RS.Utils.Scene.ConnectBox(lEyeBlinkDiv, 'Result', lEyeBlinkAdd, 'a')
                                RS.Utils.Scene.ConnectBox(lEyeBlinkDiv2, 'Result', lEyeBlinkAdd, 'b')
                                RS.Utils.Scene.ConnectBox(lEyeBlinkAdd, 'Result', lEyeBlinkNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(lEyeBlinkNumVec, 'Result', lEyeBlinkReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(headSender, 'Eyes_Blink_RightShape', rEyeBlinkDiv, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Eyes_Wide_RightShape', rEyeBlinkDiv2, 'a')
                                RS.Utils.Scene.ConnectBox(rEyeBlinkDiv, 'Result', rEyeBlinkAdd, 'a')
                                RS.Utils.Scene.ConnectBox(rEyeBlinkDiv2, 'Result', rEyeBlinkAdd, 'b')
                                RS.Utils.Scene.ConnectBox(rEyeBlinkAdd, 'Result', rEyeBlinkNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(rEyeBlinkNumVec, 'Result', rEyeBlinkReceiver, 'Lcl Translation')
                        if cEyeReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Eyes_Look_DownShape', cEyeDiv, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Eyes_Look_LeftShape', cEyeDiv2, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Eyes_Look_RightShape', cEyeDiv3, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Eyes_Look_UpShape', cEyeDiv4, 'a')
                                RS.Utils.Scene.ConnectBox(cEyeDiv, 'Result', cEyeAdd2, 'b')
                                RS.Utils.Scene.ConnectBox(cEyeDiv2, 'Result', cEyeAdd, 'a')
                                RS.Utils.Scene.ConnectBox(cEyeDiv3, 'Result', cEyeAdd, 'b')
                                RS.Utils.Scene.ConnectBox(cEyeDiv4, 'Result', cEyeAdd2, 'a')
                                RS.Utils.Scene.ConnectBox(cEyeAdd, 'Result', cEyeNumVec, 'X')
                                RS.Utils.Scene.ConnectBox(cEyeAdd2, 'Result', cEyeNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(cEyeNumVec, 'Result', cEyeReceiver, 'Lcl Translation')
                        if cJawReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Jaw_LeftShape', cJawDiv2, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Jaw_RightShape', cJawDiv3, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Jaw_OpenShape', cJawDiv, 'a')
                                RS.Utils.Scene.ConnectBox(cJawDiv2, 'Result', cJawAdd, 'a')
                                RS.Utils.Scene.ConnectBox(cJawDiv3, 'Result', cJawAdd, 'b')
                                RS.Utils.Scene.ConnectBox(cJawDiv, 'Result', cJawNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(cJawAdd, 'Result', cJawNumVec, 'X')
                                RS.Utils.Scene.ConnectBox(cJawNumVec, 'Result', cJawReceiver, 'Lcl Translation')
                        if lMouthFunnelDReceiver and rMouthFunnelDReceiver and lMouthFunnelUReceiver and lMouthFunnelDReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Lower_Lip_Down_LeftShape', lFunnelDDiv, 'a')
                                RS.Utils.Scene.ConnectBox(lFunnelDDiv, 'Result', lFunnelDAdd, 'a')
                                RS.Utils.Scene.ConnectBox(lFunnelDAdd, 'Result', lFunnelDNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(lFunnelDNumVec, 'Result', lMouthFunnelDReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(headSender, 'Lower_Lip_Down_RightShape', rFunnelDDiv, 'a')
                                RS.Utils.Scene.ConnectBox(rFunnelDDiv, 'Result', rFunnelDAdd, 'a')
                                RS.Utils.Scene.ConnectBox(rFunnelDAdd, 'Result', rFunnelDNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(rFunnelDNumVec, 'Result', rMouthFunnelDReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(headSender, 'Upper_Lip_Up_LeftShape', lFunnelUDiv, 'a')
                                RS.Utils.Scene.ConnectBox(lFunnelUDiv, 'Result', lFunnelUAdd, 'a')
                                RS.Utils.Scene.ConnectBox(lFunnelUAdd, 'Result', lFunnelUNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(lFunnelUNumVec, 'Result', lMouthFunnelUReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(headSender, 'Upper_Lip_Up_RightShape', rFunnelUDiv, 'a')
                                RS.Utils.Scene.ConnectBox(rFunnelUDiv, 'Result', rFunnelUAdd, 'a')
                                RS.Utils.Scene.ConnectBox(rFunnelUAdd, 'Result', rFunnelUNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(rFunnelUNumVec, 'Result', rMouthFunnelUReceiver, 'Lcl Translation')
                        if lMouthCornerReceiver and rMouthCornerReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_Narrow_LeftShape', lMouthCornerDiv, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_Stretch_LeftShape', lMouthCornerDiv2, 'a')
                                RS.Utils.Scene.ConnectBox(lMouthCornerDiv, 'Result', lMouthCornerAdd, 'a')
                                RS.Utils.Scene.ConnectBox(lMouthCornerDiv2, 'Result', lMouthCornerAdd, 'b')
                                RS.Utils.Scene.ConnectBox(lMouthCornerAdd, 'Result', lMouthCornerNumVec, 'X')
                                RS.Utils.Scene.ConnectBox(lMouthCornerNumVec, 'Result', lMouthCornerReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_Narrow_RightShape', rMouthCornerDiv, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_Stretch_RightShape', rMouthCornerDiv2, 'a')
                                RS.Utils.Scene.ConnectBox(rMouthCornerDiv, 'Result', rMouthCornerAdd, 'a')
                                RS.Utils.Scene.ConnectBox(rMouthCornerDiv2, 'Result', rMouthCornerAdd, 'b')
                                RS.Utils.Scene.ConnectBox(rMouthCornerAdd, 'Result', rMouthCornerNumVec, 'X')
                                RS.Utils.Scene.ConnectBox(rMouthCornerNumVec, 'Result', rMouthCornerReceiver, 'Lcl Translation')
                        if cMouthReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_DownShape', cMouthDiv, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_Scrunch_LeftShape', cMouthDiv2, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_Scrunch_RightShape', cMouthDiv3, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_UpShape', cMouthDiv4, 'a')
                                RS.Utils.Scene.ConnectBox(cMouthDiv, 'Result', cMouthAdd2, 'a')
                                RS.Utils.Scene.ConnectBox(cMouthDiv2, 'Result', cMouthAdd, 'b')
                                RS.Utils.Scene.ConnectBox(cMouthDiv3, 'Result', cMouthAdd, 'a')
                                RS.Utils.Scene.ConnectBox(cMouthDiv4, 'Result', cMouthAdd2, 'b')
                                RS.Utils.Scene.ConnectBox(cMouthAdd, 'Result', cMouthNumVec, 'X')
                                RS.Utils.Scene.ConnectBox(cMouthAdd2, 'Result', cMouthNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(cMouthNumVec, 'Result', cMouthReceiver, 'Lcl Translation')
                        if rMouthLipBiteDReceiver and rMouthLipBiteUReceiver and lMouthLipBiteDReceiver and lMouthLipBiteUReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_Phoneme_FVShape', lipBiteDiv, 'a')
                                RS.Utils.Scene.ConnectBox(lipBiteDiv, 'Result', lipBiteNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(lipBiteNumVec, 'Result', rMouthLipBiteDReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(lipBiteNumVec, 'Result', rMouthLipBiteUReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(lipBiteNumVec, 'Result', lMouthLipBiteDReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(lipBiteNumVec, 'Result', lMouthLipBiteUReceiver, 'Lcl Translation')
                        if rMouthPuckerDReceiver and rMouthPuckerUReceiver and lMouthPuckerDReceiver and lMouthPuckerUReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Mouth_Phoneme_OOShape', mouthPuckerDiv, 'a')
                                RS.Utils.Scene.ConnectBox(mouthPuckerDiv, 'Result', mouthPuckerNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(mouthPuckerNumVec, 'Result', rMouthPuckerDReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(mouthPuckerNumVec, 'Result', rMouthPuckerUReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(mouthPuckerNumVec, 'Result', lMouthPuckerDReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(mouthPuckerNumVec, 'Result', lMouthPuckerUReceiver, 'Lcl Translation')
                        if lNoseReceiver and rNoseReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Nostril_FlareShape', noseDiv, 'a')
                                RS.Utils.Scene.ConnectBox(headSender, 'Nostril_NarrowShape', noseDiv2, 'a')
                                RS.Utils.Scene.ConnectBox(noseDiv, 'Result', noseAdd, 'a')
                                RS.Utils.Scene.ConnectBox(noseDiv2, 'Result', noseAdd, 'b')
                                RS.Utils.Scene.ConnectBox(noseAdd, 'Result', noseNumVec, 'X')
                                RS.Utils.Scene.ConnectBox(noseNumVec, 'Result', lNoseReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(noseNumVec, 'Result', rNoseReceiver, 'Lcl Translation')
                        if lMouthOpenSmileReceiver and rMouthOpenSmileReceiver:
                                RS.Utils.Scene.ConnectBox(headSender, 'Smile_LeftShape', lMouthOpenSmileDiv, 'a')
                                RS.Utils.Scene.ConnectBox(lMouthOpenSmileDiv, 'Result', lMouthOpenSmileNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(lMouthOpenSmileNumVec, 'Result', lMouthOpenSmileReceiver, 'Lcl Translation')
                                RS.Utils.Scene.ConnectBox(headSender, 'Smile_RightShape', rMouthOpenSmileDiv, 'a')
                                RS.Utils.Scene.ConnectBox(rMouthOpenSmileDiv, 'Result', rMouthOpenSmileNumVec, 'Y')
                                RS.Utils.Scene.ConnectBox(rMouthOpenSmileNumVec, 'Result', rMouthOpenSmileReceiver, 'Lcl Translation')

                self.SetupParentConstraint()
                self.FacewareOrganization()

        def CreateReceiverBox( self, objectName, posX, posY):
                receiverBox = None
                obj = FBFindModelByLabelName( objectName )
                if obj:
                        receiverBox = self.TransferConstraint.ConstrainObject( obj )
                        self.TransferConstraint.SetBoxPosition( receiverBox, posX, posY ) 
                        receiverBox.UseGlobalTransforms = False
                else:
                        print "Could not add the object '{0}' to the '{1}_FACEWARE_Transfer' because it was not found in the scene.".format( objectName, self.Namespace )
                        #FBMessageBox( "Tranfer Constraint Setup Error", "Could not add the object '{0}' to the '{1}_FACEWARE_Transfer' because it was not found in the scene.".format( objectName, self.Namespace ), "OK" )
                return receiverBox


        def CreateSenderBox( self, objectName, posX, posY ):
                senderBox = None
                obj = FBFindModelByLabelName( objectName )
                if obj:
                        senderBox = self.TransferConstraint.SetAsSource( obj )
                        self.TransferConstraint.SetBoxPosition( senderBox, posX, posY ) 
                        senderBox.UseGlobalTransforms = False
                else:
                        print "Could not add the object '{0}' to the '{1}_FACEWARE_Transfer' because it was not found in the scene.".format( objectName, self.Namespace )
                        #FBMessageBox( "Tranfer Constraint Setup Error", "Could not add the object '{0}' to the '{1}_FACEWARE_Transfer' because it was not found in the scene.".format( objectName, self.Namespace ), "OK" )
                return senderBox

        def ConverterBox( self, relation, converterType, xPos, yPos, writeDataX=None, writeDataY=None, writeDataZ=None ):
                converterBox = relation.CreateFunctionBox( 'Converters', converterType )
                relation.SetBoxPosition( converterBox ,xPos ,yPos )
                if writeDataX != None:
                        dataX = RS.Utils.Scene.FindAnimationNode( converterBox.AnimationNodeInGet(),'X' )
                        dataX.WriteData( [writeDataX] )
                if writeDataY != None:
                        dataY = RS.Utils.Scene.FindAnimationNode( converterBox.AnimationNodeInGet(),'Y' )
                        dataY.WriteData( [writeDataY] )
                if writeDataZ != None:
                        dataZ = RS.Utils.Scene.FindAnimationNode( converterBox.AnimationNodeInGet(),'Z' )
                        dataZ.WriteData( [writeDataZ] )
                return converterBox

        def NumberBox( self, relation, numberType, xPos, yPos, writeDataA=None, writeDataB=None, writeDataC=None ):
                numberBox = relation.CreateFunctionBox( 'Number', numberType )
                relation.SetBoxPosition( numberBox, xPos, yPos ) 
                if writeDataA != None:
                        dataA = RS.Utils.Scene.FindAnimationNode( numberBox.AnimationNodeInGet(),'a' )
                        dataA.WriteData( [writeDataA] )   
                if writeDataB != None:
                        dataB = RS.Utils.Scene.FindAnimationNode( numberBox.AnimationNodeInGet(),'b' )
                        dataB.WriteData( [writeDataB] )  
                if writeDataC != None:
                        dataC = RS.Utils.Scene.FindAnimationNode( numberBox.AnimationNodeInGet(),'c' )
                        dataC.WriteData( [writeDataC] ) 
                return numberBox

        def OtherBox( relation, otherType, xPos, yPos ):
                otherBox = relation.CreateFunctionBox( 'Other', otherType )
                relation.SetBoxPosition( otherBox, xPos, yPos ) 
                return otherBox

        def FindBox( self, BoxName ):
                for box in self.TransferConstraint.Boxes:
                        if box.Name == BoxName:
                                return box
                return None

        def SetupTranferConstraint( self ):
                if self.FacewareHead:
                        # need to set the head blendShape properties animatable
                        for lProp in self.FacewareHead.PropertyList:
                                if lProp.Name.endswith( "Shape" ):
                                        if not lProp.IsAnimated():
                                                lProp.SetAnimated( True )

                        # insert the object in the constraint
                        lHeadBox = self.TransferConstraint.SetAsSource( self.FacewareHead )
                        self.TransferConstraint.SetBoxPosition(  lHeadBox, 0, -1000 )

                        # create receivers
                        lCIRC_blinkR = self.CreateReceiverBox(self.Namespace + ":CIRC_blinkR", 1600, -1150)
                        lCIRC_blinkL = self.CreateReceiverBox(self.Namespace + ":CIRC_blinkL", 1600, -1000)
                        leye_C_CTRL = self.CreateReceiverBox(self.Namespace + ":eye_C_CTRL", 1600, -850)
                        louterBrow_R_CTRL = self.CreateReceiverBox(self.Namespace + ":outerBrow_R_CTRL", 1600, -380)
                        louterBrow_L_CTRL = self.CreateReceiverBox(self.Namespace + ":outerBrow_L_CTRL", 1600, -150)
                        linnerBrow_R_CTRL = self.CreateReceiverBox(self.Namespace + ":innerBrow_R_CTRL", 1600, 50)
                        linnerBrow_L_CTRL = self.CreateReceiverBox(self.Namespace + ":innerBrow_L_CTRL", 1600, 300)
                        lnose_R_CTRL = self.CreateReceiverBox(self.Namespace + ":nose_R_CTRL", 1450, 600)
                        lnose_L_CTRL = self.CreateReceiverBox(self.Namespace + ":nose_L_CTRL", 1450, 700)
                        ljaw_CTRL = self.CreateReceiverBox(self.Namespace + ":jaw_CTRL", 2400, 850)
                        lCIRC_lipsNarrowWideL = self.CreateReceiverBox(self.Namespace + ":CIRC_lipsNarrowWideL", 2100, 1180)
                        lCIRC_lipsNarrowWideR = self.CreateReceiverBox(self.Namespace + ":CIRC_lipsNarrowWideR", 2100, 1280)
                        lCIRC_funnelDL = self.CreateReceiverBox(self.Namespace + ":CIRC_funnelDL", 2100, 1380)
                        lCIRC_funnelDR = self.CreateReceiverBox(self.Namespace + ":CIRC_funnelDR", 2100, 1480)
                        lCIRC_funnelUR = self.CreateReceiverBox(self.Namespace + ":CIRC_funnelUR", 2100, 1580)
                        lCIRC_funnelUL = self.CreateReceiverBox(self.Namespace + ":CIRC_funnelUL", 2100, 1680)
                        lCIRC_lipBite = self.CreateReceiverBox(self.Namespace + ":CIRC_lipBite", 1100, 1280)
                        lCIRC_frownL = self.CreateReceiverBox(self.Namespace + ":CIRC_frownL", 1100, 1380)
                        lCIRC_frownR = self.CreateReceiverBox(self.Namespace + ":CIRC_frownR", 1100, 1480)
                        lCIRC_oh = self.CreateReceiverBox(self.Namespace + ":CIRC_oh", 1100, 1580)
                        lCIRC_smileL = self.CreateReceiverBox(self.Namespace + ":CIRC_smileL", 1100, 1680)
                        lCIRC_smileR = self.CreateReceiverBox(self.Namespace + ":CIRC_smileR", 1100, 1780)
                        lmouth_CTRL = self.CreateReceiverBox(self.Namespace + ":mouth_CTRL", 3000, -100)

                        # set up sender connections
                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 6' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Eyes_Blink_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 7' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Eyes_Wide_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 4' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Eyes_Blink_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 5' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Eyes_Wide_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b)' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Eyes_Look_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 1' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Eyes_Look_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 2' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Eyes_Look_UpShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 3' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Eyes_Look_DownShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 8' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Brows_Up_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 9' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Brows_Down_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 10' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Brows_Up_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 11' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Brows_Down_LeftShape', receiverObj, 'a' ) 

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 13' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Brows_Squeeze_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 14' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Brows_Up_CenterShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 15' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Brows_Down_CenterShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 12' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Brows_Squeeze_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 20' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_DownShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 19' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_UpShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 21' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_Stretch_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 22' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_Stretch_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 37' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_Scrunch_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 38' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_Scrunch_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 36' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Nostril_NarrowShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 35' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Nostril_FlareShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 28' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_Phoneme_FVShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 26' )
                        if receiverObj:
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Frown_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 27' )
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Frown_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 23' )                
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_Phoneme_OOShape', receiverObj, 'a' )   

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 24' )                
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Smile_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 25' )                
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Smile_RightShape', receiverObj, 'a' ) 

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 16' )
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Jaw_OpenShape', receiverObj, 'a' )   

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 18' )                
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Jaw_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 17' )                
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Jaw_RightShape', receiverObj, 'a' ) 

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 34' )
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_Narrow_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 33' )                
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Mouth_Narrow_RightShape', receiverObj, 'a' ) 

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 29' )                
                        if receiverObj:                        
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Lower_Lip_Down_LeftShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 32' )                
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Lower_Lip_Down_RightShape', receiverObj, 'a' )   

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 31' )
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Upper_Lip_Up_RightShape', receiverObj, 'a' )

                        receiverObj = self.FindBox ( self.Namespace + '_FaceTransfer_Divide (a/b) 30' )                
                        if receiverObj:                                                
                                RS.Utils.Scene.ConnectBox( lHeadBox, 'Upper_Lip_Up_LeftShape', receiverObj, 'a' )

                        # set up receiver connections
                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 2' )                
                        if lCIRC_blinkR and senderObj:
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_blinkR, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 1' )                                    
                        if lCIRC_blinkL and senderObj:                    
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_blinkL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector')                
                        if leye_C_CTRL and senderObj:
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', leye_C_CTRL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 3')                
                        if louterBrow_R_CTRL and senderObj:                    
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', louterBrow_R_CTRL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 4')                
                        if louterBrow_L_CTRL and senderObj: 
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', louterBrow_L_CTRL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 7')  
                        if linnerBrow_L_CTRL and senderObj:               
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', linnerBrow_L_CTRL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 8') 
                        if linnerBrow_R_CTRL and senderObj:               
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', linnerBrow_R_CTRL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 6')
                        if lmouth_CTRL and senderObj:                 
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lmouth_CTRL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 21') 
                        if lnose_R_CTRL and lnose_L_CTRL and senderObj:               
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lnose_R_CTRL, 'Lcl Translation' )
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lnose_L_CTRL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 14')
                        if lCIRC_lipBite and senderObj: 
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_lipBite, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 12') 
                        if lCIRC_frownL and senderObj:               
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_frownL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 13')
                        if lCIRC_frownR and senderObj:               
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_frownR, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 9') 
                        if lCIRC_oh and senderObj:             
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_oh, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 11') 
                        if lCIRC_smileL and senderObj:               
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_smileL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 10') 
                        if lCIRC_smileR and senderObj: 

                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_smileR, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 5') 
                        if ljaw_CTRL and senderObj:              
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', ljaw_CTRL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 20')
                        if lCIRC_lipsNarrowWideL and senderObj:                 
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_lipsNarrowWideL, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 19') 
                        if lCIRC_lipsNarrowWideR and senderObj:               
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_lipsNarrowWideR, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 15') 
                        if lCIRC_funnelDL and senderObj:               
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_funnelDL, 'Lcl Translation' )                                                                                                                                                                

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 18')
                        if lCIRC_funnelDR and senderObj:                 
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_funnelDR, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 17') 
                        if lCIRC_funnelUR and senderObj:              
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_funnelUR, 'Lcl Translation' )

                        senderObj = self.FindBox ( self.Namespace + '_FaceTransfer_Number to Vector 16')  
                        if lCIRC_funnelUL and senderObj:        
                                RS.Utils.Scene.ConnectBox( senderObj, 'Result', lCIRC_funnelUL, 'Lcl Translation' )      

                else:
                        print "Could not add the object '{0}_Head' to the '{0}_FACEWARE_Transfer' because it was not found in the scene.".format( self.Namespace)
                        FBMessageBox("Tranfer Constraint Setup Error", "Could not add the object '{0}_Head' to the '{0}_FACEWARE_Transfer' because it was not found in the scene.".format( self.Namespace), "OK" )



        def SetupParentConstraint( self ):

                # need to know which project user is in for specific neck setups
                project = str( RS.Config.Project.Name ).lower()

                if self.Namespace:

                        self.ParentConst = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
                        self.ParentConst.Name = self.Namespace + "_FACEWARE_Parent"

                        facewareProperty = self.ParentConst.PropertyCreate('Faceware Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                        facewareProperty.Data = 'faceware constraint'               

                        # get neck specfic to user's current project
                        skelNeck = None

                        if project == 'gta5':
                                skelNeck = FBFindModelByLabelName(self.Namespace + ":SKEL_Neck_1")

                        if project == 'rdr3':
                                skelNeck = FBFindModelByLabelName(self.Namespace + ":SKEL_Neck0")


                        if self.FacewareScale:
                                self.ParentConst.ReferenceAdd ( 0, self.FacewareScale )

                        else:
                                print "Could not add the object '{0}_Scale' to the '{0}_FACEWARE_Parent' because it was not found in the scene.".format( self.Namespace)
                                FBMessageBox("Parent Constraint Setup Error", "Could not add the object '{0}_Scale' to the '{0}_FACEWARE_Parent' because it was not found in the scene.".format(self.Namespace), "OK" )

                        if skelNeck:
                                self.ParentConst.ReferenceAdd ( 1, skelNeck )

                                # setting up the offset for the head on the constraint.
                                for i in self.ParentConst.PropertyList:
                                        if i.Name == self.Namespace + ":{0}.Offset T".format( skelNeck.Name ):
                                                i.Data = FBVector3d(0.10, -0.05, -0.25)
                                        if i.Name == self.Namespace + ":{0}.Offset R".format( skelNeck.Name ):
                                                if project == 'gta5':
                                                        i.Data = FBVector3d( 0.05, -90.0, -120.0 )   
                                                if project == 'rdr3':
                                                        i.Data = FBVector3d( 0.05, -90.0, -100.0 )

                        else:
                                print "Could not add the Neck object to the '{0}_FACEWARE_Parent' because it was not found in the scene.".format( self.Namespace )
                                FBMessageBox("Parent Constraint Setup Error", "Could not add the Neck object to the '{0}_FACEWARE_Parent' because it was not found in the scene.".format(self.Namespace), "OK" )

                        self.ParentConst.Active = True
                        self.ParentConst.Lock = True         


        def FacewareOrganization( self ):

                if self.Namespace:
                        folder = FBFolder( self.Namespace + "_FACEWARE_Constraints", self.TransferConstraint)   
                        lCons = FBSystem().Scene.Constraints

                        # add the parent constraint to the constraint Folder
                        folder.Items.append( self.ParentConst )           
                        tag = folder.PropertyCreate( 'rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None )
                        tag.Data = "rs_Folders" 
                        facewareProperty = folder.PropertyCreate('Faceware Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                        facewareProperty.Data = 'faceware folder' 

                        FBSystem().Scene.Evaluate()  

                        # organise groups! #####
                        mainGroup = None			

                        for i in RS.Globals.Groups:
                                if i.Name == 'Faceware_All':
                                        mainGroup = i

                        if mainGroup:
                                characterGroup = FBGroup( self.Namespace + '_Faceware' )
                                facewareProperty = characterGroup.PropertyCreate('Faceware Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                                facewareProperty.Data = 'faceware group' 				

                                mainGroup.ConnectSrc( characterGroup )

                                for i in self.FaceWareComponents:
                                        characterGroup.ConnectSrc( i )
                        else:
                                mainGroup = FBGroup( 'Faceware_All' )
                                facewareProperty = mainGroup.PropertyCreate('Faceware Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                                facewareProperty.Data = 'faceware group' 

                                characterGroup = FBGroup( self.Namespace + '_Faceware' )
                                facewareProperty = characterGroup.PropertyCreate('Faceware Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                                facewareProperty.Data = 'faceware group'

                                mainGroup.ConnectSrc( characterGroup )	
                                for i in self.FaceWareComponents:
                                        characterGroup.ConnectSrc( i )				


        def AddDevice( self ): 

                # add device        
                facewareDevice = FBCreateObject( "Browsing/Templates/Devices", "Faceware Live Client Device", self.Namespace + "_Faceware Live Client Device" )    
                if facewareDevice:
                        FBSystem().Scene.Devices.append( facewareDevice )
                        facewareProperty = facewareDevice.PropertyCreate('Faceware Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                        facewareProperty.Data = 'faceware device'  

                # write xml if needed
                xmlPath = self.FacewareXMLPath + 'Faceware_{0}_MotionBuilder.xml'.format( self.Namespace )
                RS.Perforce.Sync( xmlPath )

                if os.path.exists( xmlPath ): 
                        None
                else:
                        self.WriteCharacterXML()


        def WriteCharacterXML( self ): 

                templatePath = self.FacewareXMLPath + 'Faceware_CharacterName_MotionBuilder.xml'
                RS.Perforce.Sync( templatePath )

                newPath = self.FacewareXMLPath + 'Faceware_{0}_MotionBuilder.xml'.format( self.Namespace )

                if os.path.exists( templatePath ):
                        doc = et.parse( templatePath )
                        root = doc.getroot()

                        seachStr = 'CharacterName'

                        # get all the elements
                        elements = root.findall('.//*')

                        # filter them for the searchStr
                        matches = [item for item in elements if seachStr in item.text]

                        # each item in matches is a reference to an element in the doc 
                        for i in matches:
                                i.text = i.text.replace( 'CharacterName', self.Namespace )

                        # write to new file
                        doc.write( newPath )

                        # mark new file for add in P4
                        RS.Perforce.Add( newPath )

                else:
                        FBMessageBox( 'R* Error', 'Unable to find file:\n\n{0}'.format( templatePath ), 'Ok' )


        def TimeShift( self, value ):

                plotShiftList = []

                facewareProperty = None

                #  time shift and scale filter   
                filterManager = FBFilterManager()
                timeShiftScaleFilter = filterManager.CreateFilter('Time Shift And Scale')

                # Set the Shift property         
                propertyShift = timeShiftScaleFilter.PropertyList.Find( 'Shift' )
                propertyShift.Data = FBTime( 0, 0, 0, ( int ( value ) ) )

                # plot, then apply filter to all rigs
                for i in RS.Globals.Components:
                        facewareProperty = i.PropertyList.Find( 'Faceware Component' )
                        if facewareProperty != None and i.Name.endswith( '_Head' ):
                                plotShiftList.append( i )

                # deselect all first
                RS.Utils.Scene.DeSelectAll()

                # plot
                if plotShiftList:	
                        for i in plotShiftList:

                                i.Selected = True				

                                plotPeriod = FBTime(0, 0, 0, 1)
                                FBSystem().CurrentTake.PlotTakeOnSelected( plotPeriod ) 

                                i.Selected = False		

                # time shift		
                if plotShiftList:	
                        for i in plotShiftList:

                                for prop in i.PropertyList:
                                        if prop and prop.IsAnimatable() and prop.IsAnimated():
                                                timeShiftScaleFilter.Apply( prop.GetAnimationNode(), True ) 

                timeShiftScaleFilter.FBDelete()
                FBSystem().Scene.Evaluate()   


        def SwapRig( self, characterSource, characterTarget ):

                sourcePropertyList = []
                targetPropertyList = []
                deleteSourceFacewareList = []

                # get heads
                sourceHead = RS.Utils.Scene.FindModelByName( str( characterSource ) + '_Head' )
                targetHead = RS.Utils.Scene.FindModelByName( str( characterTarget ) + '_Head' )


                if sourceHead and targetHead:

                        for i in sourceHead.PropertyList:
                                if i and i.IsAnimatable() and i.IsAnimated():
                                        sourcePropertyList.append( i )

                        for sourceProperty in sourcePropertyList:
                                for i in targetHead.PropertyList:
                                        if sourceProperty.Name == i.Name:
                                                i.SetAnimated( True )
                                                targetPropertyList.append( i )

                        # plot animation from source to target
                        for i in range( len ( sourcePropertyList ) ):
                                RS.Utils.Scene.AnimationNode.CopyData( sourcePropertyList[i].GetAnimationNode(), targetPropertyList[i].GetAnimationNode(), Recursive=True)	

                        # delete source faceware stuff
                        for i in RS.Globals.Components:
                                facewareProperty = i.PropertyList.Find( 'Faceware Component' )
                                if facewareProperty and i.Name.startswith( characterSource + '_' ):
                                        deleteSourceFacewareList.append( i )

                        for i in deleteSourceFacewareList:
                                #try:
                                i.FBDelete()
                                #except:
                                #pass

                        # user feedback
                        FBMessageBox( 'R*', 'Copied animation from [{0}] to [{1}].\n\nFaceware for [{0}] has now been deleted from the scene.'.format( characterSource, characterTarget ), 'Ok' )


                else:
                        FBMessageBox( 'R*', 'Unable to find [{0}] and/or [{1}] model in scene.\n\nEnsure they are both present before trying again.'.format( characterSource + '_Head', characterTarget + '_Head' ), 'Ok' )