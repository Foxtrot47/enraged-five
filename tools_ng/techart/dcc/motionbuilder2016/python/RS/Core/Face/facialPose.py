'''
Pose Tool

Description:  Based on the 3dsmax Pose2Pose tool, allows the storing, loading and blending of poses.
'''
import re
import os
from xml.dom import minidom

import pyfbsdk as mobu

from RS.Utils import Playblast
from RS import Perforce


class RsPoseNode(object):
    '''
    Represents a node in the current scene, but does not contain a direct pointer to the scene object.
    Call getSceneNode() to get a pointer to the real object in the scene.
    '''
    def __init__(self, nodeName=None):
        # The name of the scene node.
        self.name = nodeName or None

        # Saved pose data.
        self.position = None
        self.rotation = None
        self.scale = None

        # Snapshot current node transform.
        self._snapshotPosition = None
        self._snapshotRotation = None
        self._snapshotScale = None

    def _interpolateVectors(self, vecA, vecB, time):
        '''
        Performs a linear interpolation between two vectors and a time constant.

        Pissed that I can't do multiplication on an FBVector3d object, which makes this way more code
        than is needed.
        '''
        aX, aY, aZ = vecA.GetList()
        bX, bY, bZ = vecB.GetList()

        x = bX * (1.0 - time) + aX * time
        y = bY * (1.0 - time) + aY * time
        z = bZ * (1.0 - time) + aZ * time
        return mobu.FBVector3d(x, y, z)

    def snapshot(self):
        '''
        Creates a snapshot of the current node transform.
        '''
        sceneNode = self.getSceneNode()

        if sceneNode:
            self._snapshotPosition = mobu.FBVector3d(sceneNode.Translation.Data)
            self._snapshotRotation = mobu.FBVector3d(sceneNode.Rotation.Data)
            self._snapshotScale = mobu.FBVector3d(sceneNode.Scaling.Data)

        else:
            assert 0, "Could not locate object ({0}) in the scene!  The pose tool data has somehow gotten out of sync, or the character is not compatible with this file.".format(self.name)

    def blend(self, pct):
        '''
        Blends the current node to the stored pose transform, based on the supplied percentage. The
        percentage should be normalized down to 0 - 1.
        '''
        sceneNode = self.getSceneNode()

        if sceneNode:
            sceneNode.Translation = self._interpolateVectors(self.position, self._snapshotPosition or self.position, pct)
            sceneNode.Rotation = self._interpolateVectors(self.rotation, self._snapshotRotation or self.rotation, pct)
            sceneNode.Scaling = self._interpolateVectors(self.scale, self._snapshotScale or self.scale, pct)

    def key(self):
        '''
        Sets a key for the pose at the current frame time.
        '''
        sceneNode = self.getSceneNode()

        if sceneNode:
            sceneNode.Translation.Key()
            sceneNode.Rotation.Key()
            sceneNode.Scaling.Key()

    def getSceneNode(self):
        '''
        Returns the real scene node, if found.
        '''
        modelName = self.name
        model = mobu.FBFindModelByLabelName(modelName)
        
        if model is None:
            if modelName .startswith("CS_"):
                modelName = re.sub("(^CS_|^IG_)", "IG_", modelName)
                model = mobu.FBFindModelByLabelName(modelName)
            elif modelName .startswith("IG_"):
                modelName = re.sub("(^CS_|^IG_)", "CS_", modelName)
                model = mobu.FBFindModelByLabelName(modelName)
        return model

    def save(self, baseDoc):
        '''
        Creates an xml representation of the object as a string and returns it.
        ''' #
        nodeItem = baseDoc.createElement("node")
        nodeItem.setAttribute("name", self.name)

        xPos, yPos, zPos = self.position.GetList()
        nodePos = baseDoc.createElement("position")
        nodePos.appendChild(baseDoc.createTextNode("{0} {1} {2}".format(xPos, yPos, zPos)))
        nodeItem.appendChild(nodePos)

        xRot, yRot, zRot = self.rotation.GetList()
        nodeRot = baseDoc.createElement("rotation")
        nodeRot.appendChild(baseDoc.createTextNode("{0} {1} {2}".format(xRot, yRot, zRot)))
        nodeItem.appendChild(nodeRot)

        xScale, yScale, zScale = self.scale.GetList()
        nodeScl = baseDoc.createElement("scale")
        nodeScl.appendChild(baseDoc.createTextNode("{0} {1} {2}".format(xScale, yScale, zScale)))
        nodeItem.appendChild(nodeScl)

        return nodeItem


class RsPose(object):
    '''
    Represents a stored pose.
    '''
    def __init__(self):
        # The name of the pose.
        self.name = None
        self.thumbNailPath = None

        # The RsPoseNode objects that represent the pose.
        self.nodes = []

    def save(self, baseDoc):
        '''
        Creates an xml representation of the pose as a string and returns it.
        '''
        poseItem = baseDoc.createElement("pose")
        poseItem.setAttribute("name", self.name)
        poseItem.setAttribute("thumbnail", self.thumbNailPath)

        nodeItems = baseDoc.createElement("nodes")

        for node in self.nodes:
            nodeItems.appendChild(node.save(baseDoc))

        poseItem.appendChild(nodeItems)
        return poseItem


class RsPoseManager(object):
    '''
    Static interface for managing the poses.
    '''
    def __init__(self):
        # Store a version number in case of xml format changes.
        self.version = 1.0

        # Track if the poses need to be saved.
        self._dirty = False

        # Dictionary of all the loaded poses.
        # key: The pose name.
        # value: RsPose object.
        self._poses = {}

        # File extension for the saved pose files.
        self.fileExtension = 'p2px'

        # Store the last loaded pose file.
        self.lastPoseFile = None
        self.namespace = None
        self.thumbnailFolderPath = None

    @property
    def poses(self):
        return self._poses

    def getModelSelection(self):
        '''
        Return the current model selection.
        '''
        modelList = mobu.FBModelList()
        mobu.FBGetSelectedModels(modelList)

        return [item for item in modelList]

    def selectPoseControls(self, poseName):
        if poseName in self._poses:
            pose = self._poses[poseName]

            # Clear current selection.
            modelList = mobu.FBModelList()
            mobu.FBGetSelectedModels(modelList, None, True)

            for model in modelList:
                model.Selected = False

            # Now select just the nodes for this pose.
            for node in pose.nodes:
                obj = node.getSceneNode()

                if obj:
                    obj.Selected = True

    def clearAll(self):
        '''
        Clear all poses.
        '''
        self._dirty = True
        self._poses = {}

    def snapshot(self, poseName):
        '''
        Snapshot deltas for the supplied pose.
        '''
        if poseName in self._poses:
            pose = self._poses[poseName]

            for node in pose.nodes:
                node.snapshot()

    def blendPose(self, poseName, pct, key=True):
        '''
        Blends a pose using the supplied percentage.
        '''
        if poseName in self._poses:
            pose = self._poses[poseName]

            for node in pose.nodes:
                node.blend(pct)

                if key:
                    node.key()

    def keyPose(self, poseName):
        '''
        Sets an animation key for a pose.
        '''
        if poseName in self._poses:
            pose = self._poses[poseName]

            for node in pose.nodes:
                node.key()

    def savePoses(self, filename):
        '''
        Save the current set of poses to file.
        '''
        fileHandle = open(filename, 'w')
        implementation = minidom.getDOMImplementation()
        baseDoc = implementation.createDocument(None, "poses", None)
        baseDocElement = baseDoc.documentElement

        baseDocElement.setAttribute("version", str(self.version))
        baseDocElement.setAttribute("namespace", self.namespace)

        for poseName, pose in self._poses.iteritems():
            baseDocElement.appendChild(pose.save(baseDoc))

        fileHandle.write(baseDoc.toprettyxml())
        fileHandle.close()

        self._dirty = False

    def loadLastPoses(self):
        if self.lastPoseFile != None:
            if os.path.exists(self.lastPoseFile):
                self.loadPoses(self.lastPoseFile)

    def loadPoses(self, filename):
        '''
        Load a set of poses from file.
        '''
        if not os.path.exists(filename):
            raise ValueError("The post file ({0}) does not exist!".format(filename))

        self._poses = {}

        xmldoc = minidom.parse(filename)
        root = xmldoc.getElementsByTagName("poses")

        if len(root) == 0:
            raise ValueError("Not a valid Pose file")
        root = root[0]
        self.namespace = str(root.getAttribute("namespace"))
        # self.version = root.get('version')
        xmlPoses = [node for node in root.childNodes if not isinstance(node, minidom.Text)
                                                 and node.tagName == "pose"]

        for xmlPose in xmlPoses:
            pose = RsPose()
            pose.name = str(xmlPose.getAttribute('name'))
            pose.thumbNailPath = str(xmlPose.getAttribute('thumbnail'))

            xmlNode = [node for node in xmlPose.childNodes if not isinstance(node, minidom.Text)
                                                            and node.tagName == "nodes"]
            for poseNode in xmlNode:
                xmlNodes = [node for node in poseNode.childNodes if not isinstance(node, minidom.Text)
                                                                    and node.tagName == "node"]

                for xmlNode in xmlNodes:
                    poseNode = RsPoseNode()

                    poseNode.name = str(xmlNode.getAttribute('name'))
                    dataDict = {node.tagName:node.childNodes[0].nodeValue for node in xmlNode.childNodes
                                if not isinstance(node, minidom.Text)}

                    # Get and assign transform information.
                    xPos, yPos, zPos = dataDict['position'].split(' ')
                    xRot, yRot, zRot = dataDict['rotation'].split(' ')
                    xScale, yScale, zScale = dataDict['scale'].split(' ')

                    poseNode.position = mobu.FBVector3d(float(xPos), float(yPos), float(zPos))
                    poseNode.rotation = mobu.FBVector3d(float(xRot), float(yRot), float(zRot))
                    poseNode.scale = mobu.FBVector3d(float(xScale), float(yScale), float(zScale))

                    pose.nodes.append(poseNode)

            self._poses[pose.name] = pose

        self.lastPoseFile = filename

    def renamePose(self, oldPoseName, newPoseName):
        '''
        Rename an existing pose.
        '''
        if oldPoseName not in self._poses:
            raise ValueError("The supplied pose name ({0}) does not exist!".format(oldPoseName))
        self._dirty = True

        pose = self._poses[oldPoseName]
        pose.name = newPoseName

        self._poses.pop(oldPoseName)
        self._poses[newPoseName] = pose

    def deletePose(self, poseName):
        '''
        Delete an existing pose.
        '''
        if poseName not in self._poses:
            raise ValueError("The supplied pose name ({0}) does not exist!".format(poseName))
        self._dirty = True
        self._poses.pop(poseName)

    def updatePoseThumbnail(self, poseName):
        """
        Update the thumbnail on an existing pose

        args:
            poseName (str): The pose name
        """
        pose = self._poses.get(poseName)
        if pose is None:
            raise ValueError("The supplied pose name ({0}) does not exist!".format(poseName))
        self._dirty = True
        pose.thumbNailPath = self.generateThumbnail(poseName)

    def updatePose(self, poseName):
        '''
        Update an existing pose to use a different set of nodes.
        '''
        if poseName not in self._poses:
            raise ValueError("The supplied pose name ({0}) does not exist!".format(poseName))
        self._dirty = True

        selection = self.getModelSelection()
        pose = self._poses[poseName]
        pose.nodes = []

        for obj in selection:
            node = RsPoseNode()
            node.name = obj.LongName
            node.position = mobu.FBVector3d(obj.Translation.Data)
            node.rotation = mobu.FBVector3d(obj.Rotation)
            node.scale = mobu.FBVector3d(obj.Scaling)

            pose.nodes.append(node)

            lNamespace = obj.LongName.partition(":")
            if lNamespace[1] != "":
                self.namespace = lNamespace[0]
            else:
                self.namespace = "None"

        self._poses[poseName] = pose

    def createNewPose(self, poseName):
        '''
        Create a new pose based on the current selection.
        '''
        selection = self.getModelSelection()

        if len(selection) == 0:
            raise ValueError("No objects selected!")

        self._dirty = True

        pose = RsPose()
        pose.name = poseName

        for obj in selection:
            node = RsPoseNode()
            node.name = obj.LongName
            node.position = mobu.FBVector3d(obj.Translation.Data)
            node.rotation = mobu.FBVector3d(obj.Rotation)
            node.scale = mobu.FBVector3d(obj.Scaling)

            pose.nodes.append(node)

            lNamespace = obj.LongName.partition(":")
            if lNamespace[1] != "":
                self.namespace = lNamespace[0]
            else:
                self.namespace = "None"

        self._poses[poseName] = pose
        pose.thumbNailPath = self.generateThumbnail(poseName)

        return pose

    def generateThumbnail(self, nameOfPose):
        """
        Generate the thumbnail for the given pose under the current character thumbnail folder path
        Will also add the the thumbnail to default changelist in p4

        args:
            nameOfPose (str): The name of the pose

        return:
            the path to the pose
        """
        if self.thumbnailFolderPath is None:
            return None

        if not os.path.isdir(self.thumbnailFolderPath):
            os.makedirs(self.thumbnailFolderPath)

        renderPath = os.path.join(self.thumbnailFolderPath, "{0}.png".format(nameOfPose))
        currentTime = mobu.FBSystem().LocalTime
        Playblast.PlaybastFrame(renderPath, int(currentTime.GetFrame()), 300, 300)
        Perforce.Add(renderPath)
        return renderPath


poses = RsPoseManager()
