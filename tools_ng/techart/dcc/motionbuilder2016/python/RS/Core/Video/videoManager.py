"""
Module to wrap FFMPEG

This is a cross platform library

Example:

    from RS.Core.Video import videoManager
    print videoManager.VideoManager.getVideoLength(r"X:\Work\testFile.mov")
    print videoManager.VideoManager.getVideoSize(r"X:\Work\testFile.mov")
    print videoManager.VideoManager.getVideoFps(r"X:\Work\testFile.mov")
    print videoManager.VideoManager.getVideoTimecode(r"X:\Work\testFile.mov")

"""
import os
import re
import subprocess
import platform


class VideoManager(object):
    """
    Video Manager class to handle and wrap FFMPEG
    """
    _FFMPEG_FILE_PATH = None
    # TODO: This needs to be config driven
    # Linux Op
    if platform.system() == "Linux":
        _FFMPEG_FILE_PATH = os.path.join(os.getenv("HOME"), "bin", "ffmpeg.exe")

    # Windows Op
    elif platform.system() == "Windows":
        from RS import Config
        _FFMPEG_FILE_PATH = os.path.join(Config.Tool.Path.Bin, "video", "ffmpeg",
                                     Config.AppConfig.FFMPEG, "ffmpeg.exe")

    @classmethod
    def getFFMPEGApp(cls):
        """
        Class Method

        Get the file path to the FFMPEG application

        Return:
            string path to FFMPEG
        """
        return cls._FFMPEG_FILE_PATH

    @classmethod
    def _runFfmpeg(cls, commandsToRun):
        """
        Intenral Method

        Run FFMPEG with the given command

        Args:
            commandsToRun (list of strings): The args to run as a list of strings

        Returns:
            tuple of strings of the Output and Error from FFMPEG
        """
        command = [cls._FFMPEG_FILE_PATH]
        command.extend(commandsToRun)
        proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdOutput, stdError = proc.communicate()
        return stdOutput, stdError

    @classmethod
    def getVideoLength(cls, filePath):
        """
        Class Method

        Get the length of the given video as a SMPTE String "HH:MM:SS.FF"

        Args:
            filePath (str): the input video file path

        Return:
            a string of the video as a SMPTE string
        """
        if not os.path.exists(filePath):
            return None

        _, output = cls._runFfmpeg(["-i", filePath])
        resultStringRegex = re.compile("Duration: \d\d:\d\d:\d\d.\d\d")
        results = resultStringRegex.findall(output)
        if len(results) != 1:
            return None
        return results[0].replace("Duration: ", "")

    @classmethod
    def getVideoLengthAsTuple(cls, filePath):
        """
        Class Method

        Get the length of the given video as a tuple of SMPTE (HH,MM,SS,FF)

        Args:
            filePath (str): the input video file path

        Return:
            a tuple of int of the SMPTE length
        """
        if not os.path.exists(filePath):
            return None

        _, output = cls._runFfmpeg(["-i", filePath])
        resultStringRegex = re.compile("Duration: \d\d:\d\d:\d\d.\d\d")
        results = resultStringRegex.findall(output)
        if len(results) != 1:
            return None
        endString = results[0].replace("Duration: ", "")
        hours, minutes, secondsAndFrames = endString.split(":", 3)
        seconds, frames = secondsAndFrames.split(".", 2)
        return int(hours), int(minutes), int(seconds), int(frames)

    @classmethod
    def getVideoSize(cls, filePath):
        """
        Class Method

        Get the size of the video frames

        Args:
            filePath (str): the input video file path

        Return:
            an tuple of int for Height and Width of the video
        """
        if not os.path.exists(filePath):
            return None

        _, output = cls._runFfmpeg(["-i", filePath])
        resultStringRegex = re.compile("[1-9][\d]+x[1-9][\d]+")
        results = resultStringRegex.findall(output)
        if len(results) != 1:
            return None
        height, width = results[0].split("x")
        return int(height), int(width)

    @classmethod
    def getVideoFps(cls, filePath):
        """
        Class Method

        Get the video frames per second of the given video file

        Args:
            filePath (str): the input video file path

        Return:
            a string of the video FPS
        """
        if not os.path.exists(filePath):
            return None

        _, output = cls._runFfmpeg(["-i", filePath])
        resultStringRegex = re.compile("[\d]+ fps")
        results = resultStringRegex.findall(output)
        if len(results) != 1:
            return None
        return results[0].replace(" fps", "")

    @classmethod
    def getVideoTimecode(cls, filePath):
        """
        Class Method

        Get the start timecode of the given video as a SMPTE String "HH:MM:SS.FF"

        Args:
            filePath (str): the input video file path

        Return:
            a string of the video start timecode as a SMPTE string
        """
        if not os.path.exists(filePath):
            return None

        _, output = cls._runFfmpeg(["-i", filePath])
        resultStringRegex = re.compile("\d\d:\d\d:\d\d:\d\d")
        results = list(set(resultStringRegex.findall(output)))
        if len(results) != 1:
            return None
        return results[0]


    @classmethod
    def extractAudioFromVideo(cls, inputFilePath, outputFilePath):
        """
        Class Method

        Extract out the audio file from an given input video

        Args:
            inputFilePath (str): the input video file path
            outputFilePath (str): the output audio file path

        """
        if not os.path.exists(inputFilePath):
            return None
        args = ["-y", "-async", "0"]

        args.append("-i")
        args.append(inputFilePath)

        args.extend(["-vn", "-acodec", "copy", outputFilePath])
        cls._runFfmpeg(args)

    @classmethod
    def createVideoFromImageSequence(
                                     cls,
                                     imagePath,
                                     outputPath,
                                     framerate=None,
                                     startNumber=None,
                                     codec=None,
                                     compression=None):
        """
        Class Method

        Create a video from the given image sequence

        example:
            createVideoFromImageSequence("X:\\test_%03d.png", "X:\\test.mov")

        Args:
            imagePath (str): the input image sequence file path
            outputPath (str): the output video file path

        Kwargs:
            framerate (int): The video framerate
            startNumber (int): The start frame number
            codec (str): The string codec
            compression (list of strings): List of flags and values to add to the ffmpeg command
        """
        args = [
                "-start_number_range", "8",
                "-pattern_type", "sequence",
                "-y",
                "-async", "0"
                ]
        if startNumber is not None:
            args.append("-start_number")
            args.append(str(startNumber))

        if framerate is not None:
            args.append("-framerate")
            args.append(str(framerate))

        args.append("-i")
        args.append(imagePath)

        if codec is not None:
            args.append("-vcodec")
            args.append(codec)

        if compression is not None:
            args.extend(compression)

        args.append(outputPath)
        cls._runFfmpeg(args)

    @classmethod
    def createImageSequenceFromVideo(
                                     cls,
                                     inputPath,
                                     outputPath,
                                     framerate=None,
                                     codec=None,
                                     compression=None):
        """
        Class Method

        Create a video from the given image sequence

        example:
            createVideoFromImageSequence("X:\\test.mov", "X:\\test_%03d.png")

        Args:
            inputPath (str): the input video file path
            outputPath (str): the output location to extract the frames to

        Kwargs:
            framerate (int): The video framerate
            codec (str): The string codec
            compression (list of strings): List of flags and values to add to the ffmpeg command
        """
        args = [
                "-y",
                "-async", "0",
                ]
        args.append("-i")
        args.append(inputPath)


        if framerate is not None:
            args.append("-framerate")
            args.append(str(framerate))

        if codec is not None:
            args.append("-vcodec")
            args.append(codec)

        if compression is not None:
            args.extend(compression)

        args.extend([
                     "-start_number", 
                     "0",
                     outputPath
                    ])
        cls._runFfmpeg(args)

    @classmethod
    def compressVideo(
                           cls,
                           inputPath,
                           outputPath,
                           audioCodec=None,
                           videoCodec=None,
                           compression=None
                           ):
        """
        Class Method

        Compress a video file

        Args:
            imagePath (str): the input video file path
            outputPath (str): the output video file path

        Kwargs:
            audioCodec (str): The string codec for the audio stream
            videoCodec (str): The string codec for the video stream
            compression (list of strings): List of flags and values to add to the ffmpeg command
        """
        args = ["-y"]
        args.append("-i")
        args.append(inputPath)

        if audioCodec is not None:
            args.append("-acodec")
            args.append(audioCodec)

        if videoCodec is not None:
            args.append("-vcodec")
            args.append(videoCodec)

        if compression is not None:
            args.extend(compression)

        args.append(outputPath)
        cls._runFfmpeg(args)

    @classmethod
    def mergeVideoAndAudio(
                           cls,
                           videoPath,
                           audioPath,
                           outputPath,
                           audioCodec=None,
                           videoCodec=None,
                           compression=None
                           ):
        """
        Class Method

        Merge a video and audio files

        Args:
            imagePath (str): the input video file path
            audioPath (str): the input audio file path
            outputPath (str): the output video file path

        Kwargs:
            audioCodec (str): The string codec for the audio stream
            videoCodec (str): The string codec for the video stream
            compression (list of strings): List of flags and values to add to the ffmpeg command
        """
        args = ["-y"]

        args.append("-i")
        args.append(videoPath)

        args.append("-i")
        args.append(audioPath)

        if audioCodec is not None:
            args.append("-acodec")
            args.append(audioCodec)

        if videoCodec is not None:
            args.append("-vcodec")
            args.append(videoCodec)

        if compression is not None:
            args.extend(compression)

        args.append(outputPath)
        cls._runFfmpeg(args)
