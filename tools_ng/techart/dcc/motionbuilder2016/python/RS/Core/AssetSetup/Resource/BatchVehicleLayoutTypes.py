
from pyfbsdk import *

import RS.Utils
import RS.Globals

import RS.Core.AssetSetup.Vehicles as VehicleCore

'''
batch script to apply layout types and steering offsets to all vehicles.
url:bugstar:2029360
'''

def Run():
	
	lFileName = RS.Utils.Path.GetBaseNameNoExtension( FBApplication().FBXFileName ) 	
	
	# add layout type data as a custom property on chassis url:bugstar:2029360
	layoutData = VehicleCore.GetLayoutType( lFileName )
	lExportable = RS.Utils.Scene.FindModelByName( 'chassis' )
	
	if lExportable:	
	
		if layoutData:
			tag = lExportable.PropertyCreate('layouttype', FBPropertyType.kFBPT_charptr, "", False, True, None)
			tag.Data = str( layoutData )
		else:
			print 'unable to find layout type data in meta file, vehicle name may be incorrect.'  
			
		# add new null to represent the steering wheel offset url:bugstar:2029360
		if layoutData:
			steeringOffset = VehicleCore.GetSteeringWheelOffset( str( layoutData ) )
			if steeringOffset:
				
				print 'Vehicle: ', lFileName
				print 'Layout Type: ', layoutData
				print 'Steering Offset: ', steeringOffset
				print
				
				steeringOffsetNull = FBModelNull( 'SteeringOffset_' + str( layoutData ) )	
				
				frontDoorSideSeat = None
				for i in RS.Globals.Components:
					if i.Name == 'seat_dside_f':
						frontDoorSideSeat = i
						
				if frontDoorSideSeat:
					steeringOffsetNull.Parent = frontDoorSideSeat
					steeringOffsetNull.SetVector( FBVector3d( steeringOffset ), FBModelTransformationType.kModelTranslation, False )
					tag = steeringOffsetNull.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
					tag.Data = "rs_Mesh"
					
					# save file
					FBApplication().FileSave( FBApplication().FBXFileName )					
					
				else:
					print 'Vehicle: ', lFileName
					print 'unable to find seat node'
					print
			
			else:
				print 'Vehicle: ', lFileName
				print 'unable to find steering offset in meta'
				print
		else:
			print 'Vehicle: ', lFileName
			print 'unable to find layout data in meta'
			print
				
	else:
		print 'Vehicle: ', lFileName
		print 'unable to find chassis'	
		print
	
Run()