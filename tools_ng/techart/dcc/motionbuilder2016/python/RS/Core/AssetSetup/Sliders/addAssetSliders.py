import pyfbsdk as mobu
from RS.Utils import Scene
from RS.Core.AssetSetup.Sliders import utils as sliderUtils
from RS.Core.AssetSetup.Sliders import components as sliderElements

from RS.Utils import ContextManagers 


def Build(inputComponents,
          sliderGroupName,
          createExtensionPrefix=None,
          yGridOffset=0.0,
          deletePreviousComponents=False,
          overrideRange=[0.0, 1.0],
          connectSliders=True):
    '''
        inputComponents (list of string)
    '''

    sliderBuilder =  sliderUtils.Builder()

    sliders = []
    sliderGroup = None
    defaulFloatValue = 0.0

    dummyRoot = Scene.FindModelByName(sliderBuilder.DUMMY_GROUP)
    sliderData = []

    for assetIndex, assetName in enumerate(inputComponents):
        componentUtils = sliderUtils.Slider()
        componentUtils.collectFromScene(assetName)

        slider, sliderGroup = componentUtils.validateAsset()

        applyOffset = False

        if slider is None:
            slider = sliderElements.sliderShapeBuilder(assetName)
            slider.create()
            applyOffset = True

        componentUtils.collectFromScene(assetName)

        if slider and deletePreviousComponents is True:
            componentUtils.rectangleOutline.FBDelete()
            componentUtils.textShape.FBDelete()
            componentUtils.animationControl.FBDelete()

            mobu.FBSystem().Scene.Evaluate()

            slider = sliderElements.sliderShapeBuilder(assetName)
            slider.create()
            applyOffset = True

            componentUtils.collectFromScene(assetName)

        offsetVectorValue = (assetIndex+1)*sliderBuilder.SLIDER_DEFAULT_OFFSET

        if applyOffset == True:
            componentUtils.rectangleOutline.Translation.Data = mobu.FBVector3d(offsetVectorValue,
                                                                               yGridOffset,
                                                                               defaulFloatValue)

        for component in [componentUtils.animationControl,
                          componentUtils.rectangleOutline,
                          componentUtils.textShape]:
            sliderGroup = sliderBuilder.createSliderGroup(component, 
                                                          sliderGroupName=sliderGroupName)

        if dummyRoot is not None:
            componentUtils.rectangleOutline.Parent = dummyRoot

        if createExtensionPrefix is not None:
            componentUtils.createCharacterExtension(createExtensionPrefix)

        sliderData.append(componentUtils)
        sliders.append(componentUtils.animationControl)

    if connectSliders is False:
        return [sliderData, 
                sliderGroup]

    sliderBuilder.connectSlider(sliders, 
                                sliderGroup,
                                overrideRange=overrideRange)

    return [sliderData, 
            sliderGroup]


def Create(inputComponents,
           yGridOffset=0.0,
           overrideRange=[0.0, 1.0]):
    sliderBuilder =  sliderUtils.Builder()

    sliderGroup = None
    defaulFloatValue = 0.0

    sliderData = []

    for assetIndex, assetName in enumerate(inputComponents):
        componentUtils = sliderUtils.Slider()

        applyOffset = True

        slider = sliderElements.sliderShapeBuilder(assetName)

        slider.create()

        componentUtils.rectangleOutline = slider.rectangleOutline
        componentUtils.textShape = slider.textShape
        componentUtils.animationControl = slider.animationControl

        offsetVectorValue = (assetIndex+1)*sliderBuilder.SLIDER_DEFAULT_OFFSET

        if applyOffset == True:
            componentUtils.rectangleOutline.Translation.Data = mobu.FBVector3d(offsetVectorValue,
                                                                               yGridOffset,
                                                                               defaulFloatValue)

        sliderData.append(componentUtils)

    return sliderData 


def BuildJoysticks(inputComponents,
                   sliderGroupName,
                   createExtensionPrefix=None,
                   yGridOffset=0.0,
                   deletePreviousComponents=False):

    sliderBuilder =  sliderUtils.Builder()

    sliderGroup = None
    defaulFloatValue = 0.0

    dummyRoot = Scene.FindModelByName(sliderBuilder.DUMMY_GROUP)

    if deletePreviousComponents is True:
        with ContextManagers.SceneExpressionsDisabled():
            for assetIndex, assetName in enumerate(inputComponents):
                componentUtils = sliderUtils.Slider()
                componentUtils.collectFromScene('{}_XY'.format(assetName))

                if deletePreviousComponents is True:
                    if componentUtils.rectangleOutline:
                        previousComponents = []

                        for data in componentUtils.rectangleOutline.Children:
                            previousComponents.append(data)
                            previousComponents.extend(list(data.Children))

                        for data in previousComponents:
                            data.FBDelete()

                        componentUtils.rectangleOutline.FBDelete()

                    mobu.FBSystem().Scene.Evaluate()

    for assetIndex, assetName in enumerate(inputComponents):
        componentUtils = sliderUtils.Slider()
        componentUtils.collectFromScene('{}_XY'.format(assetName))

        joystick, sliderGroup = componentUtils.validateAsset()

        if joystick is None:
            joystick = sliderElements.joystickShapeBuilder(suffix='{}_XY'.format(assetName),
                                                           alternateCaption='{}_XYZ'.format(assetName))
            joystick.create()

            slider = sliderElements.sliderShapeBuilder('{}_Z'.format(assetName))
            slider.create()
            slider.rectangleOutline.Translation = mobu.FBVector3d(8, 0, 0)
            slider.textShape.FBDelete()
            slider.rectangleOutline.Parent = joystick.rectangleOutline

            slider.animationControl.PropertyList.Find('TranslationActive').Data = True
            slider.animationControl.PropertyList.Find('TranslationMax').Data = mobu.FBVector3d(0, 1, 0)
            slider.animationControl.PropertyList.Find('TranslationMin').Data = mobu.FBVector3d(0, -1, 0)

            for attribute in ('TranslationMinX',
                              'TranslationMinY',
                              'TranslationMinZ',
                              'TranslationMaxX',
                              'TranslationMaxY',
                              'TranslationMaxZ'):
                slider.animationControl.PropertyList.Find(attribute).Data = True

        componentUtils.collectFromScene('{}_XY'.format(assetName))

        offsetVectorValue = (assetIndex+1)*sliderBuilder.SLIDER_DEFAULT_OFFSET
        
        componentUtils.rectangleOutline.Translation.Data = mobu.FBVector3d(offsetVectorValue,
                                                                           yGridOffset,
                                                                           defaulFloatValue)

        for component in [componentUtils.animationControl,
                          componentUtils.rectangleOutline,
                          componentUtils.textShape]:
            sliderGroup = sliderBuilder.createSliderGroup(component, 
                                                          sliderGroupName=sliderGroupName)

        if dummyRoot is not None:
            componentUtils.rectangleOutline.Parent = dummyRoot

        if createExtensionPrefix is not None:
            componentUtils.createCharacterExtension(createExtensionPrefix)


def ChangeCaption(sliderName,
                  overrideCaptionName):
    componentUtils = sliderUtils.Slider()
    componentUtils.collectFromScene(sliderName)
    slider, sliderGroup = componentUtils.validateAsset()
    
    if slider is None:
        return

    captionUtils = sliderElements.SliderCaption()
    captionMesh = captionUtils.createCaption(overrideCaptionName)

    componentUtils.textShape.Geometry = captionMesh.Geometry
    componentUtils.textShape.GeometricTranslation = mobu.FBVector3d(captionUtils.shapeWidth*-0.5,
                                                                    0,
                                                                    0)

    captionMesh.FBDelete()


def AddBloodFlow():
    defaultValue = 0.0

    captionUtils = sliderElements.SliderCaption()
    captionMesh = captionUtils.createCaption('Contextual_BloodFlow')

    captionMesh.GeometricTranslation = mobu.FBVector3d(captionUtils.shapeWidth*-0.5,
                                                       defaultValue,
                                                       defaultValue)


    sliderData, sliderGroup = Build(['CTRL_Blush',
                                     'CTRL_Anger',
                                     'CTRL_Cold'],
                                     'Sliders',
                                     yGridOffset=0.0)

    horizontalOffset = [-2.53, -0.28, 1.95]
    for index, data in enumerate(sliderData):
        data.rectangleOutline.Parent = captionMesh
        value = data.rectangleOutline.Translation.Data
        value[0] = horizontalOffset[index]
        value[1] = -1.49

        data.rectangleOutline.Translation.Data = value
        data.rectangleOutline.Scaling.Data = mobu.FBVector3d(1.0, 1.0, 1.0)
        data.rectangleOutline.Name = data.rectangleOutline.Name.replace('CTRL_', '')

        data.textShape.Name = data.textShape.Name.replace('CTRL_', '')

    captionMesh.PropertyList.Find('Scaling (lcl)').Data = mobu.FBVector3d(5.0, 5.0, 5.0)

    captionMesh.PropertyList.Find('Translation (lcl)').Data  = mobu.FBVector3d(63.11, 163.53, 0.0)

    dummyRoot = Scene.FindModelByName(sliderUtils.Builder.DUMMY_GROUP)
    captionMesh.Name = 'TEXT_BloodFlow'
    
    if dummyRoot is not None:
        captionMesh.Parent = dummyRoot

        captionMesh.PropertyList.Find('Scaling (lcl)').Data = mobu.FBVector3d(0.05, 0.05, 0.05)

        captionMesh.Translation.Data = mobu.FBVector3d(0.63, 0.0, 1.64)


def AddRagDoll():
    PurgeRagDoll()

    sliderData, sliderGroup = Build(['RagdollSlider'],
                                     'Sliders',
                                     yGridOffset=26.0)

    ragDoll = sliderData[0]
    ragDoll.animationControl.Name = 'Slider_RagdollSlider'
    ragDoll.rectangleOutline.Name = 'RECT_RagdollSlider'
    ragDoll.textShape = 'TEXT_RagdollSlider'


def AddHeelHeight():
    Build(['HeelHeight',
           'Spine_Pin', 
           'Root_Pin',
           'L_Foot_Pin',
           'R_Foot_Pin',
           'L_Knee_Pin',
           'R_Knee_Pin',
           'L_Hand_Pin',
           'R_Hand_Pin'],
           'HeelHeight',
           yGridOffset=-10.0)


def CreateHeelHeight():
    Create(['HeelHeight',
            'Spine_Pin', 
            'Root_Pin',
            'L_Foot_Pin',
            'R_Foot_Pin',
            'L_Knee_Pin',
            'R_Knee_Pin',
            'L_Hand_Pin',
            'R_Hand_Pin'],
            yGridOffset=-10.0)


def AddHairScale():
    hairRoot = mobu.FBModelNull('HairSystem')
    hairRoot.Show = True

    sliderData = Create(['HS_UpDown',
                         'HS_FrontBack',
                         'HS_LeftRight',
                         'HS_HatOn'],
                         overrideRange=[-2.0, 2.0])

    captionArray = ['up_down',
                    'front_back',
                    'left_right',
                    'hat_on']

    horizontalOffset = [-12, 0.0, 12, 24]
    for index, data in enumerate(sliderData):
        data.rectangleOutline.Parent = hairRoot
        value = data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data
        value[0] = horizontalOffset[index]

        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data = value

        captionUtils = sliderElements.SliderCaption()
        captionMesh = captionUtils.createCaption(captionArray[index])

        data.textShape.Geometry = captionMesh.Geometry
        data.textShape.GeometricTranslation = mobu.FBVector3d(captionUtils.shapeWidth*-0.5,
                                                              0,
                                                              0)

        captionMesh.FBDelete()
        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').SetLocked(True)

        data.setSliderLimits([-2.0, 2.0])

    sliderData2 = Create(['HS_RotX',
                          'HS_RotY',
                          'HS_RotZ'],
                          overrideRange=[-2.0, 2.0])

    captionArray = ['rot_X',
                    'rot_Y',
                    'rot_Z']

    for index, data in enumerate(sliderData2):
        data.rectangleOutline.Parent = hairRoot

        value = mobu.FBVector3d(horizontalOffset[index],
                                -18.0,
                                0.0)

        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data = value

        captionUtils = sliderElements.SliderCaption()
        captionMesh = captionUtils.createCaption(captionArray[index])

        data.textShape.Geometry = captionMesh.Geometry
        data.textShape.GeometricTranslation = mobu.FBVector3d(captionUtils.shapeWidth*-0.5,
                                                              0,
                                                              0)

        captionMesh.FBDelete()
        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').SetLocked(True)
        data.setSliderLimits([-2.0, 2.0])

    sliderData2[1].animationControl.Name = 'HS_RotationY'

    hairRoot.Translation = mobu.FBVector3d(98, 199.71, 0.0)
    hairRoot.PropertyList.Find('Look').Data = 0
    hairRoot.PropertyList.Find('Size').Data = 0.0

    sliderBuilder =  sliderUtils.Builder()
    sliderBuilder.createSliderGroup(hairRoot, 
                                    sliderGroupName="Expr Driven Sliders")

    for sliderComponent in sliderData2:
        for component in [sliderComponent.animationControl,
                          sliderComponent.rectangleOutline,
                          sliderComponent.textShape]:
                sliderGroup = sliderBuilder.createSliderGroup(component, 
                                                              sliderGroupName="Expr Driven Sliders")

    for sliderComponent in sliderData:
        for component in [sliderComponent.animationControl,
                          sliderComponent.rectangleOutline,
                          sliderComponent.textShape]:
                sliderGroup = sliderBuilder.createSliderGroup(component, 
                                                              sliderGroupName="Expr Driven Sliders")


def AddTagHairScale():
    hairRoot = mobu.FBModelNull('TagHairSystem')
    hairRoot.Show = True

    sliderData = Create(['HS_UpDown_TAG',
                         'HS_FrontBack_TAG',
                         'HS_LeftRight_TAG'],
                         overrideRange=[-2.0, 2.0])

    captionArray = ['up_down_tag',
                    'front_back_tag',
                    'left_right_tag']

    horizontalOffset = [-12, 0.0, 12, 24]
    for index, data in enumerate(sliderData):
        data.rectangleOutline.Parent = hairRoot
        value = data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data
        value[0] = horizontalOffset[index]

        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data = value

        captionUtils = sliderElements.SliderCaption()
        captionMesh = captionUtils.createCaption(captionArray[index])

        data.textShape.Geometry = captionMesh.Geometry
        data.textShape.GeometricTranslation = mobu.FBVector3d(captionUtils.shapeWidth*-0.5,
                                                              0,
                                                              0)

        captionMesh.FBDelete()
        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').SetLocked(True)

        data.setSliderLimits([-2.0, 2.0])

    sliderData2 = Create(['HS_RotX_TAG',
                          'HS_RotationY_TAG',
                          'HS_RotZ_TAG'],
                          overrideRange=[-2.0, 2.0])

    captionArray = ['rot_X_tag',
                    'rot_Y_tag',
                    'rot_Z_tag']

    for index, data in enumerate(sliderData2):
        data.rectangleOutline.Parent = hairRoot

        value = mobu.FBVector3d(horizontalOffset[index],
                                -18.0,
                                0.0)

        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data = value

        captionUtils = sliderElements.SliderCaption()
        captionMesh = captionUtils.createCaption(captionArray[index])

        data.textShape.Geometry = captionMesh.Geometry
        data.textShape.GeometricTranslation = mobu.FBVector3d(captionUtils.shapeWidth*-0.5,
                                                              0,
                                                              0)

        captionMesh.FBDelete()
        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').SetLocked(True)
        data.setSliderLimits([-2.0, 2.0])


    hairRoot.Translation = mobu.FBVector3d(150, 199.71, 0.0)
    hairRoot.PropertyList.Find('Look').Data = 0
    hairRoot.PropertyList.Find('Size').Data = 0.0

    sliderBuilder =  sliderUtils.Builder()
    sliderBuilder.createSliderGroup(hairRoot, 
                                    sliderGroupName="Expr Driven Sliders")

    for sliderComponent in sliderData:
        for component in [sliderComponent.animationControl,
                          sliderComponent.rectangleOutline,
                          sliderComponent.textShape]:
                sliderGroup = sliderBuilder.createSliderGroup(component, 
                                                              sliderGroupName="Expr Driven Sliders")

    for sliderComponent in sliderData2:
        for component in [sliderComponent.animationControl,
                          sliderComponent.rectangleOutline,
                          sliderComponent.textShape]:
                sliderGroup = sliderBuilder.createSliderGroup(component, 
                                                              sliderGroupName="Expr Driven Sliders")


def AddOpenJacket():
    sliderData = Create(['L_OpenJacket',
                         'R_OpenJacket'],
                        yGridOffset=-230.0)


def AddSatchel():
    sliderData = Create(['PH_Satchel',
                         'SatchelSwitch'])

    captionArray = ['Slider_PH_Satchel',
                    'Slider_SatchelSwitch']

    xOffsetArray = [72.64, 87.00]
    for index, data in enumerate(sliderData):
        value = mobu.FBVector3d(xOffsetArray[index],
                                106.44,
                                0.0)

        data.rectangleOutline.Parent = None
        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data = value

        data.animationControl.Name = captionArray[index]
        data.setSliderLimits()

    sliderData2 = Create(['Satchel_UpperBody',
                          'Satchel_LowBody',
                          'Satchel_Shoulder'])

    captionArray = ['Slider_Satchel_UpperBody',
                    'Slider_Satchel_LowBody',
                    'Slider_Satchel_Shoulder']

    xOffsetArray = [63.36, 79.36, 95.36]
    for index, data in enumerate(sliderData2):
        value = mobu.FBVector3d(xOffsetArray[index],
                                88.47,
                                0.0)

        data.rectangleOutline.Parent = None
        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data = value

        data.animationControl.Name = captionArray[index]
        data.setSliderLimits()


def AddDressControllers():
    sliderData = Create(['Dress_Lift',
                         'Dress_OnHorse'])

    captionUtils = sliderElements.SliderCaption()
    captionMesh = captionUtils.createCaption('Dress_Controllers')

    shiftOffset = [[0.0, -0.1],
                   [-0.15, -0.1]]

    captionMesh.GeometricTranslation = mobu.FBVector3d(captionUtils.shapeWidth*-0.5*5.0,
                                                       captionUtils.shapeHeight*-0.5*5.0,
                                                       0)

    captionMesh.GeometricScaling = mobu.FBVector3d(5.0,
                                                   5.0,
                                                   0.0)

    captionMesh.Name = 'TEXT_Dress_Controllers'

    for index, data in enumerate(sliderData):
        data.rectangleOutline.Parent = captionMesh

        value = mobu.FBVector3d(shiftOffset[index][0]*100.0,
                                shiftOffset[index][1]*100.0,
                                0.0)

        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data = value
        data.setSliderLimits()

    dressRoot = mobu.FBModelNull('Dress_Controllers')
    dressRoot.Show = True

    dressRoot.PropertyList.Find('Look').Data = 0
    dressRoot.PropertyList.Find('Size').Data = 0.0

    captionMesh.Parent = dressRoot
    dressRoot.Translation = mobu.FBVector3d(220.67, 105.71, 0.0)


def AddHairScaleSimple():
    sliderData = Create(['HairScale',
                         'HairCrown'],
                         overrideRange=[-2.0, 2.0])

    xOffsetArray = [93.73, 102.03]
    for index, data in enumerate(sliderData):
        value = mobu.FBVector3d(xOffsetArray[index],
                                220.74,
                                0.0)

        data.rectangleOutline.Parent = None
        data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data = value

        data.setSliderLimits([-2.0, 2.0])


def AddHands():
    handRoot = mobu.FBModelNull('Hand_Controllers')
    handRoot.Show = True

    handRoot.PropertyList.Find('Look').Data = 0
    handRoot.PropertyList.Find('Size').Data = 0.0

    captionUtils = sliderElements.SliderCaption()
    captionMesh = captionUtils.createCaption('Hand_Controllers')
    captionMesh.GeometricTranslation = mobu.FBVector3d(captionUtils.shapeWidth*-0.5*5.0,
                                                       captionUtils.shapeHeight*-0.5*5.0,
                                                       0)

    captionMesh.GeometricScaling = mobu.FBVector3d(5.0,
                                                   5.0,
                                                   0.0)

    captionMesh.Name = 'TEXT_Hand_Controllers'
    captionMesh.Parent = handRoot
    captionMesh.Translation = mobu.FBVector3d(0.0, 0.3, 0.0)

    horizontalOffset = [-20, -7, 7, 20]
    verticalOffset = [-8, -23, -38, -52]

    controlArrayName = [['Hand_R_Tendon1',
                         'Hand_R_Knuckle1',
                         'Hand_L_Knuckle1',
                         'Hand_L_Tendon1'],
                        ['Hand_R_Tendon2',
                         'Hand_R_Knuckle2',
                         'Hand_L_Knuckle2',
                         'Hand_L_Tendon2'],
                        ['Hand_R_Tendon3',
                         'Hand_R_Knuckle3',
                         'Hand_L_Knuckle3',
                         'Hand_L_Tendon3'],
                        ['Hand_R_Tendon4',
                         'Hand_R_Knuckle4',
                         'Hand_L_Knuckle4',
                         'Hand_L_Tendon4']]

    for offsetIndex,controls in enumerate(controlArrayName):
        sliderData = Create(controls,
                            overrideRange=[-1.0, 1.0])

        for index, data in enumerate(sliderData):
            data.rectangleOutline.Parent = captionMesh

            value = mobu.FBVector3d(horizontalOffset[index], 
                                    verticalOffset[offsetIndex], 
                                    0)

            data.rectangleOutline.PropertyList.Find('Translation (Lcl)').Data = value

    handRoot.Translation = mobu.FBVector3d(132.96, 105.71, 0.0)


def PurgeRagDoll():
    ragDollRoots = ['RECT_RagdollSlider',
                    'RECT_Ragdoll']

    for ragDollName in ragDollRoots:
        root = Scene.FindModelByName(ragDollName)

        ragdollComponents = []
        if root is None:
            continue

        Scene.GetChildren(root, 
                          ragdollComponents)

        for node in ragdollComponents:
            node.FBDelete()


def PurgePhObjects(sliderRootName):
    sliderRoot = mobu.FBFindModelByLabelName(sliderRootName)

    if sliderRoot is None:
        return

    sliderNodes = []

    Scene.GetChildren(sliderRoot,
                      sliderNodes)

    for node in set(sliderNodes):
        node.FBDelete()


def AddPhSpaceSliders():
    sliderPrefixArray = ['LObject', 
                         'RObject']

    sliderLabels = ['LObjectSpacePHSlider',
                    'RObjectSpacePHSlider']

    for label in sliderLabels:
        PurgePhObjects('RECT_{}'.format(label))

    #bug [4384840]
    sliderData, sliderGroup = Build(sliderLabels,
                                    'Sliders',
                                    yGridOffset=-215.0,
                                    createExtensionPrefix='ObjectSpacePHSliders',
                                    overrideRange=[0.0, 5.0])

    #bug [4384840]
    captionArray = ['O_self',
                    '1_Skel_root',
                    '2_same_hand',
                    '3_cigarette',
                    '4_Skel_Head',
                    '5_opposite_hand']

    heightOffset = [0.0,
                    1.0,
                    2.0,
                    3.0,
                    4.0,
                    5.0]

    defaultValue = 0.0

    for prefixIndex,slider in enumerate(sliderData):
        for index, overrideCaptionName in enumerate(captionArray):
            captionUtils = sliderElements.SliderCaption()

            captionMesh = captionUtils.createCaption('{}_{}'.format(sliderPrefixArray[prefixIndex],
                                                                    overrideCaptionName))

            captionMesh.GeometricTranslation = mobu.FBVector3d(captionUtils.shapeWidth*-1.0,
                                                               defaultValue,
                                                               defaultValue)

            captionMesh.Parent = slider.rectangleOutline

            captionMesh.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(-0.50, 
                                                                                      heightOffset[index], 
                                                                                      defaultValue)

            captionMesh.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d(0, 0, 0)

            slider.rectangleOutline.GeometricTranslation = mobu.FBVector3d(defaultValue,
                                                                           2.5,
                                                                           defaultValue)

            slider.rectangleOutline.GeometricScaling = mobu.FBVector3d(1.0, 1.0, 2.5)

            captionMesh.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(0.3, 0.3, 0.3)

            slider.textShape.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d(defaultValue, 
                                                                                           -1.0, 
                                                                                           defaultValue)
