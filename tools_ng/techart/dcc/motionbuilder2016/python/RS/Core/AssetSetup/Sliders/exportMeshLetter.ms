

struct mobuSliderShapeSet
(
	templateAnchor = "mobuSliderShapeSets",
	meshShapeArray = #()
)



struct mobuSliderStringSet
(
	templateAnchor = "mobuSliderLetterSets",
	meshLetterArray = #()
)



struct mobuSliderStringData
(
	stringName = "",

	faceList = #(),
	vertexPosition = #(),

	meshNode = undefined,
	
	fn getGeometry deleteNode:true=
	(
		if meshNode == undefined do
		(
			return false
		)
		
		local faceCount = getNumFaces this.meshNode		
		this.faceList = for faceIndex = 1 to faceCount collect (getFace this.meshNode faceIndex)
		
		local vertexCount = getNumVerts this.meshNode	
		this.vertexPosition = for vertexIndex = 1 to vertexCount collect (getVert this.meshNode vertexIndex)
		
		if deleteNode == false do
		(
			return false
		)

		delete this.meshNode 
		this.meshNode = undefined
		
		return true
	)
)



struct sliderComponentXmlIO
(
	xmlTargetFile = undefined,
	
	fn capitalizeAttribute inputName =
	(
		local firstLetter = ( toUpper inputName[1] )
		local outputName = replace inputName 1 1 firstLetter

		return outputName
	),

	public fn PrepareWriter  =
	(
		/*build a format class so we indent out output. makes it readable */
		local xmlFormatter = dotnetobject "System.Xml.XmlWriterSettings"
		xmlFormatter.IndentChars = "\t"
		xmlFormatter.Indent = true
	 
		/*Instantiate our xml writer class */
		local XMLWriter = dotnetclass "System.Xml.XmlWriter"
		
		fileExists = doesFileExist xmlTargetFile
		if fileExists == false then
		(
			xmlTargetDirectory = RSMakeSafeSlashes(getFilenamePath xmlTargetFile)

			directoryExists = doesFileExist xmlTargetDirectory
			if directoryExists == false then
			(
				makeDir xmlTargetDirectory all:true
			)
		)

		XMLWriter = XMLWriter.Create xmlTargetFile xmlFormatter
		
		return XMLWriter
	),

	public fn ReleaseWriter XmlWriter =
	(
		XmlWriter.WriteEndDocument()
		XmlWriter.Close()
	),
	
	public fn WritePropertyArray XmlWriter propertyName inputArray anchorName:"Node"=
	(
		XmlWriter.WriteStartElement(propertyName as string) 

		for nodeData in inputArray do
		(
			if (isStruct nodeData) == true do 
			(
				local structType = (classOf nodeData) as string				
				structType = substituteString  (filterString structType "(")[1] "#Struct:" ""
				
				XmlWriter.WriteStartElement(structType as string) 
				this.WriteObjectProperty XmlWriter nodeData 
				XmlWriter.WriteEndElement() 

				continue
			)

			XmlWriter.WriteStartElement(anchorName as string) 

			if (isValidNode nodeData )== false then
			(
				XmlWriter.WriteValue(nodeData as string) 
			) else (
				XmlWriter.WriteValue(nodeData.name as string) 
			)
			XmlWriter.WriteEndElement()
		)
		XmlWriter.WriteEndElement() 	
	),

	public fn WriteObjectProperty XmlWriter inputObject=
	(
		prefixList = getPropNames inputObject 
		for propertyIndex = 1 to prefixList.count do 
		(
			writtenValue = getProperty inputObject prefixList[propertyIndex] 
			if writtenValue == undefined do
			(
				continue
			)
			
			if classof writtenValue == MAXScriptFunction do
			(
				continue
			)
			
			if isStruct  writtenValue do
			(
				continue
			)

			if classof writtenValue == array do
			(
				this.WritePropertyArray XmlWriter (prefixList[propertyIndex] as string)  writtenValue
				continue
			)

			XmlWriter.WriteStartElement(prefixList[propertyIndex] as string) 
			XmlWriter.WriteValue(writtenValue as string) 
			XmlWriter.WriteEndElement() 		
		)
	),
	
	public fn SaveTemplate inputComponentStructure  =
	(
		XMLWriter = PrepareWriter()
	
		try
		(
			XmlWriter.WriteStartDocument()
			XmlWriter.WriteStartElement(inputComponentStructure.templateAnchor) 

			WriteObjectProperty XmlWriter inputComponentStructure 
		) catch (
			XmlWriter.WriteEndElement() 	
			XMLWriter.Close()
			
			format "*** % ***\n" (getCurrentException())
			return false
		)
		XmlWriter.WriteEndElement() 	

		ReleaseWriter XMLWriter
	)
)



struct alphabetSampler
(
	lowerCapLetters = #(	
									"a","b", "c", "d",
									"e","f", "g", "h",
									"i","j", "k", "l",
									"m","n", "o", "p",
									"k","r", "s", "t",
									"u","v", "w", "x",
									"y","z" 	
								),

	upperCapLetters = #(	
									"A","B", "C", "D",
									"E","F", "G", "H",
									"I","J", "K", "L",
									"M","N", "O", "P",
									"K","R", "S", "T",
									"U","V", "W", "X",
									"Y","Z" 	
								),

	signLetters = #(	"_", "-", "1", "2", "3", "4", "5", "6", "7", "8", "9" ),

	fn turnToMesh inputNode=
	(
		--addModifier inputNode (Edit_Spline())
		addModifier inputNode (Edit_Poly())

		collapseStack  inputNode
		convertToMesh  inputNode

		return OK
	),

	fn createMesh inputLetter:"a"=
	(
		meshLetter = text size:1 kerning:0 leading:0 pos:[0,0,0] isSelected:off text:inputLetter name:(inputLetter+"_mesh1")
		this.turnToMesh meshLetter

		return meshLetter
	),
	
	fn createLetterSet =
	(
		max create mode
		local letterSet = mobuSliderStringSet()

		local letterArray= #()
		join letterArray this.lowerCapLetters
		join letterArray this.upperCapLetters
		join letterArray this.signLetters

		for letter in letterArray do
		(
			local outputData = mobuSliderStringData()
			outputData.meshNode= this.createMesh inputLetter:letter
			outputData.stringName = letter	

			outputData.getGeometry()
			append letterSet.meshLetterArray outputData
		)

		xmlUtils = sliderComponentXmlIO()
		local templateFilePath = RSMakeSafeSlashes( getFilenamePath(getThisScriptFilename()) + "sliderStringData.xml")
		xmlUtils.xmlTargetFile =templateFilePath
		xmlUtils.SaveTemplate letterSet

		format "Succesfully export %\n" templateFilePath
	),

	fn createShapeSet shapeSuffix:"sliderShapeData.xml"=
	(
		max create mode
		local letterSet = mobuSliderStringSet()

		local letterArray= selection as array

		for shapeNode in letterArray do
		(
			local outputData = mobuSliderStringData()
			this.turnToMesh shapeNode
			outputData.meshNode= shapeNode
			outputData.stringName = shapeNode.name	

			outputData.getGeometry  deleteNode:false
			append letterSet.meshLetterArray outputData
		)

		xmlUtils = sliderComponentXmlIO()
		local templateFilePath = RSMakeSafeSlashes( getFilenamePath(getThisScriptFilename()) + shapeSuffix)
		xmlUtils.xmlTargetFile =templateFilePath
		xmlUtils.SaveTemplate letterSet

		format "Succesfully export %\n" templateFilePath
	)
)

fn text_export_letters =
(
	alpbUtils = alphabetSampler()
	alpbUtils.createLetterSet()

	OK
)


fn text_export_controlShapes shapeSuffix:"sliderShapeData.xml"=
(
	--selct your shape mesh and go
	alpbUtils = alphabetSampler()
	alpbUtils.createShapeSet shapeSuffix:shapeSuffix
	
	OK
)

text_export_controlShapes shapeSuffix:"joystickShapeData.xml"