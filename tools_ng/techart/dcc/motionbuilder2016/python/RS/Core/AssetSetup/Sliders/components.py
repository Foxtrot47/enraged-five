from xml.etree.cElementTree import parse as xmlParser
import inspect
import os


import pyfbsdk as mobu


class SliderLetter(object):
    NODE_PREFIXES = ['stringName', 'faceList', 'vertexPosition']

    NODE_ATTRIBUTES = ['stringName', 'faceList', 'vertexPosition']

    def __init__(self):
        self.stringName = ""
        self.faceList = []
        self.vertexPosition = []
        self.vertexArray = []
        self.limits = []

        self.width = 0.0
        self.height = 0.0
        self.depth = 0.0

        self.vertexCount = 0
        self.faceCount = 0

        self.shiftPivot = 0.0

    def getValues(self, inputXmlText, isFloat=True):
        inputData = inputXmlText.replace('[', '')
        inputData = inputData.replace(']', '')

        outputData = inputData.split(',')
        outputValues = []

        if isFloat is True:
            for data in outputData:
                outputValues.append(float(data))
        else:
            for data in outputData:
                outputValues.append(int(data)-1)

        return outputValues

    def getDimensions(self):
        self.width = self.limits[1][0] - self.limits[0][0]
        self.height = self.limits[1][1] - self.limits[0][1]
        self.depth = self.limits[1][2] - self.limits[0][2]

    def computeBoundingBox(self):
        minPointX = 100000.0
        minPointY = 100000.0
        minPointZ = 100000.0

        maxPointX = -100000.0
        maxPointY = -100000.0
        maxPointZ = -100000.0

        for pointPosition in  self.vertexPosition:
            if pointPosition[0] < minPointX :
                minPointX = pointPosition[0]

            if pointPosition[1] < minPointY :
                minPointY = pointPosition[1]

            if pointPosition[2] < minPointZ :
                minPointZ = pointPosition[2]

            if pointPosition[0] > maxPointX :
                maxPointX = pointPosition[0]

            if pointPosition[1] > maxPointY :
                maxPointY = pointPosition[1]

            if pointPosition[2] > maxPointZ :
                maxPointZ = pointPosition[2]

        minPoint = mobu.FBVector4d(minPointX, minPointY, minPointZ, 1.0)
        maxPoint = mobu.FBVector4d(maxPointX, maxPointY, maxPointZ, 1.0)

        self.limits = [minPoint, maxPoint]
        self.getDimensions()

        self.vertexCount = len(self.vertexPosition)
        self.faceCount = len(self.faceList)

    def offsetPivot(self, preserveHeight=False):
        self.vertexArray = []
        for vertexPosition in self.vertexPosition:
            sideValue = -self.limits[0][0]
            heightValue = -self.limits[0][1]
            if preserveHeight is True:
                heightValue = 0.0

            shiftVertexPosition = [vertexPosition[0] + sideValue,
                                   vertexPosition[1] + heightValue,
                                   vertexPosition[2]]

            self.vertexArray.extend(shiftVertexPosition)


class SliderCaption(object):
    currentDirectory = os.path.dirname(__file__)

    STRING_XML_DATA = os.path.join(currentDirectory, 
                                   'sliderStringData.xml')

    STRING_XML_DATA = STRING_XML_DATA.replace('\\', '/')

    STRING_SET_ANCHOR = 'mobuSliderStringData'

    LETTER_ANCHOR = 'meshLetterArray'

    ARRAY_ANCHOR = 'Node'

    MESH_PREFIX = 'sliderTextMesh'

    SLIDER_UDP_ATTRIBUTE = 'UDP3DSMAX'

    SLIDER_META_DATA = 'knownObjectType = joystickText'

    def __init__(self):
        self.sliderLetters = {}
        self.shapeWidth = 0.0
        self.shapeHeight = 0.0

    def readStringSet(self, preserveHeight=False):
        inputfile = open(self.STRING_XML_DATA, "r")
        inputTree = xmlParser(inputfile)
        rootElement = inputTree.getroot()

        meshArray = rootElement.find(self.LETTER_ANCHOR)
        stringSetArray = meshArray.findall(self.STRING_SET_ANCHOR)

        for stringSet in stringSetArray:
            textData = SliderLetter()

            for attribute in textData.NODE_PREFIXES:
                textXmlAttribute = stringSet.find(attribute)

                if isinstance(getattr(textData, attribute), list):
                    meshData = []

                    isFloat = True
                    if attribute == 'faceList':
                        isFloat = False
        
                    for inputData in textXmlAttribute.findall(self.ARRAY_ANCHOR):
                        inputVector = textData.getValues(inputData.text,
                                                         isFloat=isFloat)

                        meshData.append(inputVector)

                    setattr(textData, attribute, meshData)
                else:                    
                    setattr(textData, attribute, textXmlAttribute.text)

            textData.computeBoundingBox()
            textData.offsetPivot(preserveHeight=preserveHeight)
            self.sliderLetters[textData.stringName] = textData

    def offsetMeshPivot(self,
                        vertexArray):
        minPointX = 100000.0
        minPointY = 100000.0
        minPointZ = 100000.0

        maxPointX = -100000.0
        maxPointY = -100000.0
        maxPointZ = -100000.0

        vertexPosition = []

        for index in  xrange(0,len(vertexArray),3):
            pointPosition = [vertexArray[index],
                             vertexArray[index+1], 
                             vertexArray[index+2]]

            vertexPosition.append(pointPosition)

            if pointPosition[0] < minPointX :
                minPointX = pointPosition[0]

            if pointPosition[1] < minPointY :
                minPointY = pointPosition[1]

            if pointPosition[2] < minPointZ :
                minPointZ = pointPosition[2]

            if pointPosition[0] > maxPointX :
                maxPointX = pointPosition[0]

            if pointPosition[1] > maxPointY :
                maxPointY = pointPosition[1]

            if pointPosition[2] > maxPointZ :
                maxPointZ = pointPosition[2]

        minPoint = mobu.FBVector4d(minPointX, minPointY, minPointZ, 1.0)
        maxPoint = mobu.FBVector4d(maxPointX, maxPointY, maxPointZ, 1.0)

        limits = [minPoint, maxPoint]
        width = limits[1][0] - limits[0][0]
        height = limits[1][1] - limits[0][1]
        depth = limits[1][2] - limits[0][2]

        shiftArray = []
        for index, vertexData in enumerate(vertexPosition):
            shiftVertexPosition = [vertexData[0] - width*0.5,
                                   vertexData[1] - height*0.25,
                                   vertexData[2]  ]

            shiftArray.extend(shiftVertexPosition)

        return shiftArray

    def createMesh(self, 
                   inputSliderLetter, 
                   nodeOrientation=None,
                   letterOffset=mobu.FBVector3d(0, 0, 0),
                   targetName=None,
                   useTriangleStrip=False,
                   offsetPivot=False):
        vertexCount = len(inputSliderLetter.vertexPosition)
        sliderTextMesh = mobu.FBMesh(self.MESH_PREFIX)

        sliderTextMesh.GeometryBegin()
        sliderTextMesh.VertexArrayInit(vertexCount, 
                              False, 
                              mobu.FBGeometryArrayID.kFBGeometryArrayID_Point)


        if offsetPivot is True:
            inputSliderLetter.vertexArray = self.offsetMeshPivot(inputSliderLetter.vertexArray) 

        vertexArray = []
        vertexArray.extend(inputSliderLetter.vertexArray)

        sliderTextMesh.SetPositionsArray(vertexArray)

        if useTriangleStrip is False:
            for face in inputSliderLetter.faceList:
                sliderTextMesh.TriangleListAdd(3, face)
        else:
            triangleStrip = range(vertexCount)
            triangleStrip.append(vertexCount-1)
            triangleStrip.append(0)
            triangleStrip.append(0)
            sliderTextMesh.TriangleStripAdd(vertexCount+3 , triangleStrip, 0)

        sliderTextMesh.GeometryEnd()

        sliderModel = mobu.FBModelCube(inputSliderLetter.stringName)
        sliderModel.Geometry = sliderTextMesh

        sliderModel.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
        sliderModel.Visible = True
        sliderModel.Show = True

        sliderModel.Translation.Data = letterOffset

        if nodeOrientation is not None:
            sliderModel.GeometricRotation = nodeOrientation

        if targetName is not None:
            sliderModel.Name = targetName

        rootProperty = sliderModel.PropertyCreate(self.SLIDER_UDP_ATTRIBUTE, 
                                                  mobu.FBPropertyType.kFBPT_charptr, 
                                                  'String', 
                                                  False, 
                                                  True,
                                                  None)

        rootProperty.Data = self.SLIDER_META_DATA

        return sliderModel

    def createWord(self, 
                   inputSliderLetter):
        sliderTextMesh = mobu.FBMesh(self.MESH_PREFIX)

        sliderTextMesh.GeometryBegin()
        sliderTextMesh.VertexArrayInit(inputSliderLetter.vertexCount, 
                              False, 
                              mobu.FBGeometryArrayID.kFBGeometryArrayID_Point)

        vertexArray = []
        vertexArray.extend(inputSliderLetter.vertexArray)

        sliderTextMesh.SetPositionsArray(vertexArray)

        for face in inputSliderLetter.faceList:
            sliderTextMesh.TriangleListAdd(len(face), face)

        sliderTextMesh.GeometryEnd()

        sliderModel = mobu.FBModelCube(inputSliderLetter.stringName)
        sliderModel.Geometry = sliderTextMesh

        sliderModel.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
        sliderModel.Visible = True
        sliderModel.Show = True

        rootProperty = sliderModel.PropertyCreate(self.SLIDER_UDP_ATTRIBUTE, 
                                                  mobu.FBPropertyType.kFBPT_charptr, 
                                                  'String', 
                                                  False, 
                                                  True,
                                                  None)

        rootProperty.Data = self.SLIDER_META_DATA

        return sliderModel

    def createCaption(self, 
                      captionText, 
                      alternateCaption=None,
                      offset=0.07):
        self.readStringSet()

        currentWidth = 0.0
        offsetId = 0
        wordMesh = SliderLetter()

        inputCaption = str(captionText)
        if alternateCaption != None:
            inputCaption = str(alternateCaption)

        for letter in inputCaption:
            currentLetter = self.sliderLetters[letter]

            xValueIndex = 0
            for vertexPosition in currentLetter.vertexArray:
                if xValueIndex == 0:
                    wordMesh.vertexArray.append(vertexPosition+currentWidth)
                else:
                    wordMesh.vertexArray.append(vertexPosition)

                xValueIndex += 1
                if xValueIndex > 2:
                    xValueIndex = 0

            for faceData in currentLetter.faceList:
                offsetfaceList = [(faceIndex+offsetId) for faceIndex in faceData]
                wordMesh.faceList.append(offsetfaceList)

            offsetId += currentLetter.vertexCount
            currentWidth += currentLetter.width + offset

        wordMesh.vertexCount += offsetId
        wordMesh.stringName = "TEXT_{0}".format(captionText)

        captionMesh = self.createWord(wordMesh)
        self.shapeWidth = currentWidth
        self.shapeHeight = wordMesh.height        

        return captionMesh 


class SliderManipulator(SliderCaption):
    currentDirectory = os.path.dirname(__file__)

    STRING_XML_DATA = os.path.join(currentDirectory, 
                                   'sliderShapeData.xml')

    STRING_XML_DATA = STRING_XML_DATA.replace('\\', '/')

    COMPONENTS = ('RECT', 'CTRL')

    NODE_ATTRIBUTES = ('rectangleOutline', 'animationControl')

    def __init__(self):
        super(SliderManipulator, self).__init__()

        self.rectangleOutline = None
        self.animationControl = None

    def createCaption(self):
        return

    def create(self,
               suffix, 
               useTriangleStrip=True,
               nodeOrientation=mobu.FBVector3d(90, 90, 0),
               offsetPivot=False):
        self.readStringSet(preserveHeight=True)
        self.shapeWidth = self.sliderLetters[self.COMPONENTS[0]].depth

        componentArray = []

        typeValues = ['knownObjectType = joystickSurround',
                      'translateTrackType=TRACK_GENERIC_CONTROL\n\nknownObjectType = joystick']

        for shapeIndex, shape in enumerate(self.COMPONENTS):
            currentShape = self.sliderLetters[shape]
            targetName = "{0}_{1}".format(currentShape.stringName,
                                          suffix)
            if currentShape.stringName == 'CTRL':
                targetName = "{0}".format(suffix)

            shapeMesh = self.createMesh(currentShape,
                                        useTriangleStrip=useTriangleStrip,
                                        targetName=targetName,
                                        nodeOrientation=nodeOrientation,
                                        offsetPivot=offsetPivot)

            setattr(self, 
                    self.NODE_ATTRIBUTES[shapeIndex],
                    shapeMesh)

            shapeProperty = shapeMesh.PropertyList.Find(self.SLIDER_UDP_ATTRIBUTE) 
            shapeProperty.Data = typeValues[shapeIndex]


class JoystickManipulator(SliderManipulator):
    currentDirectory = os.path.dirname(__file__)

    STRING_XML_DATA = os.path.join(currentDirectory, 
                                   'joystickShapeData.xml')

    STRING_XML_DATA = STRING_XML_DATA.replace('\\', '/')

    def __init__(self):
        super(JoystickManipulator, self).__init__()


class sliderTextSettings(object):
    def __init__(self):
        self.shapeWidth = 0.0
        self.shapeHeight = 0.0

    def setData(self,
                captionUtils,
                manipulatorUtils):
        self.rotation = mobu.FBVector3d(0, 90, 0)
        self.scaling = mobu.FBVector3d(0.4, 0.40, 0.40)
        self.textOffset = mobu.FBVector3d(-0.5*captionUtils.shapeWidth, 0, 0)

        textHeight = -0.5*manipulatorUtils.shapeWidth 
        textHeight -= 0.475
        self.textPosition = mobu.FBVector3d(0, textHeight, 0)
        self.outlineScale = mobu.FBVector3d(5, 5, 5)
        self.outlineRotation = mobu.FBVector3d(0, 0.0, 0)


class sliderShapeBuilder(object):
    def __init__(self, 
                 suffix="", 
                 alternateCaption=None):
        self.rectangleOutline = None
        self.textShape = None
        self.animationControl = None

        self.suffix = suffix
        self.textSettings = sliderTextSettings()
        self.alternateCaption = str(suffix)

        if alternateCaption is not None:
            self.alternateCaption = alternateCaption

    def _buildComponents(self,
                         manipulatorUtils,
                         captionUtils,
                         useTriangleStrip=True,
                         nodeOrientation=mobu.FBVector3d(90, 90, 0),
                         offsetPivot=False):
        manipulatorUtils.create(self.suffix,
                                useTriangleStrip=useTriangleStrip,
                                nodeOrientation=nodeOrientation,
                                offsetPivot=offsetPivot)

        self.textShape = captionUtils.createCaption(self.alternateCaption)
        self.rectangleOutline = manipulatorUtils.rectangleOutline
        self.animationControl = manipulatorUtils.animationControl

        self.textSettings.setData(captionUtils,
                                  manipulatorUtils) 

        self.animationControl.Parent = self.rectangleOutline
        self.textShape.Parent = self.rectangleOutline

        self.textShape.Scaling = self.textSettings.scaling

        self.textShape.GeometricTranslation = self.textSettings.textOffset
        self.textShape.Translation = self.textSettings.textPosition

        self.rectangleOutline.Scaling = self.textSettings.outlineScale
        self.rectangleOutline.Rotation = self.textSettings.outlineRotation

    def create(self):
        manipulatorUtils = SliderManipulator()
        captionUtils = SliderCaption()

        self._buildComponents(manipulatorUtils,
                              captionUtils)


class joystickShapeBuilder(sliderShapeBuilder):
    def __init__(self, 
                 suffix="", 
                 alternateCaption=None):
        self.alternateCaption = alternateCaption

        super(joystickShapeBuilder, self).__init__(suffix=suffix,
                                                   alternateCaption=alternateCaption)

    def create(self,
               useTriangleStrip=False,
               nodeOrientation=mobu.FBVector3d(90, 0, 90),
               offsetPivot=True):
        manipulatorUtils = JoystickManipulator()
        captionUtils = SliderCaption()

        self._buildComponents(manipulatorUtils,
                              captionUtils,
                              useTriangleStrip=useTriangleStrip,
                              nodeOrientation=nodeOrientation,
                              offsetPivot=True)

        self.animationControl.PropertyList.Find('TranslationActive').Data = True
        self.animationControl.PropertyList.Find('TranslationMax').Data = mobu.FBVector3d(1, 1, 0)
        self.animationControl.PropertyList.Find('TranslationMin').Data = mobu.FBVector3d(-1, -1, 0)

        for attribute in ('TranslationMinX',
                          'TranslationMinY',
                          'TranslationMinZ',
                          'TranslationMaxX',
                          'TranslationMaxY',
                          'TranslationMaxZ'):
            self.animationControl.PropertyList.Find(attribute).Data = True