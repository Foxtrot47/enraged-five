unaryOperators      = ["kNegate", "kAbs", "kAcos", "kAsin", "kATan", "kCeil", "kCos", "kCosH", "kDToR", "kExp", "kLn",
                       "kFloor", "kLog", "kRToD", "kSin", "kSinH", "kSqrt", "kTan", "kTanH", "kCosD", "kSinD", "kTanD",
                       "kAcosD", "kAsinD", "kAtanD"]
binaryOperators     = ["kAssign", "kAdd", "kSubtract", "kMultiply", "kDivide", "kGreaterThan", "kGreaterThanEqualTo",
                       "kLessThan", "kLessThanEqualTo", "kEqualTo", "kNotEqualTo", "kIfThen", "kMax", "kMin", "kMod", 
                       "kPow"]
ternaryOperators    = ["kIfThenElse", "kClamp"]

class Bone:
    
    def __init__(self):
    
        self.name = None
        
        self.aimLst = []
        
        self.xTransLst = []
        self.yTransLst = []
        self.zTransLst = []
        
        self.xRotLst = []
        self.yRotLst = []
        self.zRotLst = []
        
        self.xScaleLst = []
        self.yScaleLst = []
        self.zScaleLst = []
    
def ProcessExpression( node, targetControlName, axis, controllerName ):

    control     = ProcessControlBox( node, axis )
    return control

def ProcessEntry( node, position, axis ):

    #""" Unary Operators - Single Value Input """
    if node.__class__.__name__ == "MetaStruct[rage__ExprNodeUnaryOperator]":
        control = ProcessControlBox( node, axis )
        return control
    
    #""" Binary Operators - Two Value Inputs """      
    elif node.__class__.__name__ == "MetaStruct[rage__ExprNodeBinaryOperator]":
        control = ProcessControlBox( node, axis )
        return control
        
    #""" Turnary Operators - Three Value Inputs """          
    elif node.__class__.__name__ == "MetaStruct[rage__ExprNodeTernaryOperator]":
        control = ProcessControlBox( node, axis )
        return control
    
    #""" Constants - Single Value """  
    elif node.__class__.__name__ == "MetaStruct[rage__ExprNodeConstant]":
        return node.value
    
    #""" Name Attr Pair - Model Attribute Input """  
    elif node.__class__.__name__ == "MetaStruct[rage__ExprNodeNameAttrPair]":

        nodeName = node.name
        attrName = node.attr
        
        transformLst = ["translateX", "translateY", "translateZ", "rotateX", "rotateY", "rotateZ", "scaleX", "scaleY", "scaleZ"]
        
        if attrName in transformLst:
            return nodeName + "#" + attrName
        else:
            index = nodeName.find(attrName)
            modelName = nodeName[:(index-1)]
            return modelName + "#" + attrName

def ProcessControlBox( node, axis ):
    
    if node.opType in unaryOperators:

        pChildControl = None
        unaryLst = []
        unaryLst.append( node.opType )
        
        try:
            pChild = node.pChild
            pChildControl = ProcessEntry( pChild, "pChild", axis ) 
            unaryLst.append( pChildControl ) 
        except:
            None
            
        return unaryLst
        
    elif str(node.opType) in binaryOperators: 
        
        pLeftControl = None
        pRightControl = None
        binaryLst = []
        binaryLst.append( node.opType )
        
        try:
            pLeft = node.pLeft
            pLeftControl = ProcessEntry( pLeft, "pLeft", axis ) 
            binaryLst.append( pLeftControl ) 
        except:
            None
            
        try:
            pRight = node.pRight
            pRightControl = ProcessEntry( pRight, "pRight", axis ) 
            binaryLst.append( pRightControl )  
        except:
            None
    
        return binaryLst
        
    elif node.opType in ternaryOperators:
        
        pLeftControl = None
        pMiddleControl = None
        pRightControl = None
        ternaryLst = []
        ternaryLst.append( node.opType )
        
        try:
            pLeft = node.pLeft
            pLeftControl = ProcessEntry( pLeft, "pLeft", axis ) 
            ternaryLst.append( pLeftControl ) 
        except:
            None
        
        try:
            pMiddle = node.pMiddle
            pMiddleControl = ProcessEntry( pMiddle, "pMiddle", axis ) 
            ternaryLst.append( pMiddleControl )  
        except:
            None
           
        try:
            pRight = node.pRight
            pRightControl = ProcessEntry( pRight, "pRight", axis ) 
            ternaryLst.append( pRightControl )  
        except:
            None
    
        return ternaryLst

def recursiveConcatinate( array ):
     
    if len(array) > 1:

        addArray = []
        addArray.append('kAdd')
        addArray.append(array[0])
        addArray.append(array[1])
        array[0] = addArray
        array.remove(array[1])
        
        recursiveConcatinate( array )
        
    return array

def populateBoneLst(metaFile, concatinateExpressions):
    
    boneLst = []
    index = 0
    
    for bone in metaFile.boneLibrary:
        
        newBone = Bone()
        newBone.name = bone.id
        for transform in bone.transform:
            
            xTrans  = []
            xRot    = []
            xScale  = []
            
            yTrans  = []
            yRot    = []
            yScale  = []
            
            zTrans  = []
            zRot    = []
            zScale  = []
            
            boneTransform = transform.id 
            control = metaFile.controllerLibrary[index]
            targetControlName = control.id
            controllerName = targetControlName
            for input in control.inputs:
    
                if boneTransform == "translate":
                    
                    try:
                        pXController = input.pController.pXController 
                        if pXController.__class__.__name__ == "MetaStruct[rage__AeExpressionController]":   
                            try:
                                pExprTreeRoot = pXController.pExprTree.pRoot
                                control1 = ProcessExpression( pExprTreeRoot, targetControlName, "X", controllerName )
                                xTrans.append( control1 )
                                
                            except:
                                None  
                        newBone.xTransLst = xTrans
                    except:
                        None
                        
                    try:
                        pYController = input.pController.pYController 
                        if pYController.__class__.__name__ == "MetaStruct[rage__AeExpressionController]":   
                            try:
                                pExprTreeRoot = pYController.pExprTree.pRoot
                                control1 = ProcessExpression( pExprTreeRoot, targetControlName, "X", controllerName )
                                yTrans.append( control1 )
                                
                            except:
                                None  
                        newBone.yTransLst = yTrans
                    except:
                        None
                        
                    try:
                        pZController = input.pController.pZController 
                        if pZController.__class__.__name__ == "MetaStruct[rage__AeExpressionController]":   
                            try:
                                pExprTreeRoot = pZController.pExprTree.pRoot
                                control1 = ProcessExpression( pExprTreeRoot, targetControlName, "X", controllerName )
                                zTrans.append( control1 )
                                
                            except:
                                None  
                        newBone.zTransLst = zTrans
                    except:
                        None
                    
    
                elif boneTransform == "rotate":
                    
                    try:
                        pXController = input.pController.pXController        
                        if pXController.__class__.__name__ == "MetaStruct[rage__AeExpressionController]":   
                            try:                            
                                pExprTreeRoot = pXController.pExprTree.pRoot
                                control1 = ProcessExpression( pExprTreeRoot, targetControlName, "X", controllerName )
                                xRot.append( control1 )
                            except:
                                None
                        newBone.xRotLst = xRot
                    except:
                        None
                        
                    try:
                        pYController = input.pController.pYController        
                        if pYController.__class__.__name__ == "MetaStruct[rage__AeExpressionController]":   
                            try:                            
                                pExprTreeRoot = pYController.pExprTree.pRoot
                                control1 = ProcessExpression( pExprTreeRoot, targetControlName, "X", controllerName )
                                yRot.append( control1 )
                            except:
                                None
                        newBone.yRotLst = yRot
                    except:
                        None
                        
                    try:
                        pZController = input.pController.pZController        
                        if pZController.__class__.__name__ == "MetaStruct[rage__AeExpressionController]":   
                            try:                            
                                pExprTreeRoot = pZController.pExprTree.pRoot
                                control1 = ProcessExpression( pExprTreeRoot, targetControlName, "X", controllerName )
                                zRot.append( control1 )
                            except:
                                None
                        newBone.zRotLst = zRot
                    except:
                        None
                        
                    
                    try:
                        pBlendController = input.pController.pBlendController
                        newBone.aimLst.append( pBlendController.inputs[0].pController.inputs[0].driverId ) 
                        
                        pBoneOSController = input.pController.pBoneOSController
                        newBone.aimLst.append( pBoneOSController.inputs[0].driverId )   
                        
                        pUpnodeOSController = input.pController.pUpnodeOSController
                        newBone.aimLst.append( pUpnodeOSController.inputs[0].driverId )   
                        
                        newBone.aimLst.append( input.pController.boneAxis )  
                        newBone.aimLst.append( input.pController.boneUpAxis )
                        newBone.aimLst.append( input.pController.upnodeUpAxis )
                    except:
                        None
                
                ###############################
                    
                elif boneTransform == "scale":
                    
                    try:
                        pXController = input.pController.pXController  
                        if pXController.__class__.__name__ == "MetaStruct[rage__AeExpressionController]":   
                            try:         
                                pExprTreeRoot = pXController.pExprTree.pRoot
                                control1 = ProcessExpression( pExprTreeRoot, targetControlName, "X", controllerName )
                                xScale.append( control1 )
                            except:
                                None
                        newBone.xScaleLst = xScale
                    except:
                        None
                        
                    try:
                        pYController = input.pController.pYController  
                        if pYController.__class__.__name__ == "MetaStruct[rage__AeExpressionController]":   
                            try:         
                                pExprTreeRoot = pYController.pExprTree.pRoot
                                control1 = ProcessExpression( pExprTreeRoot, targetControlName, "X", controllerName )
                                yScale.append( control1 )
                            except:
                                None
                        newBone.yScaleLst = yScale
                    except:
                        None
                        
                    try:
                        pZController = input.pController.pZController  
                        if pZController.__class__.__name__ == "MetaStruct[rage__AeExpressionController]":   
                            try:         
                                pExprTreeRoot = pZController.pExprTree.pRoot
                                control1 = ProcessExpression( pExprTreeRoot, targetControlName, "X", controllerName )
                                zScale.append( control1 )
                            except:
                                None
                        newBone.zScaleLst = zScale
                    except:
                        None
            
            if concatinateExpressions == True:
                newBone.xTransLst = recursiveConcatinate( newBone.xTransLst )
                newBone.yTransLst = recursiveConcatinate( newBone.yTransLst )
                newBone.zTransLst = recursiveConcatinate( newBone.zTransLst )
                
                newBone.xRotLst = recursiveConcatinate( newBone.xRotLst )
                newBone.yRotLst = recursiveConcatinate( newBone.yRotLst )
                newBone.zRotLst = recursiveConcatinate( newBone.zRotLst )
                
                newBone.xScaleLst = recursiveConcatinate( newBone.xScaleLst )
                newBone.yScaleLst = recursiveConcatinate( newBone.yScaleLst )
                newBone.zScaleLst = recursiveConcatinate( newBone.zScaleLst )
        
            index += 1
            
        boneLst.append(newBone)
            
    return boneLst
