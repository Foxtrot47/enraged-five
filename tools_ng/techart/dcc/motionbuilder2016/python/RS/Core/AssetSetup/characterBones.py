from pyfbsdk import *
from PySide import QtGui
from RS.Utils import Scene
import mobuExtender

def findBoneClashes(namespace = ""):
    """
    Finds the Hash ID for each object under the character's skel root,
    Adds all the IDs to a dictionary, then pops up a message for the user if any
    clashes are detected.
    """
    characterList = []
    boneList = []
    boneHashes = {}
    boneClashes = 0
    
    feedbackMessageString = "Bone Hash ID Clashes found.\n"
    
    # Detect the skelroot and add everything beneath it to the bonelist
    skelRoot = FBFindModelByLabelName(namespace + "SKEL_ROOT")
    if skelRoot:
        Scene.GetChildren(skelRoot, characterList)
    
    # Filter out anything that isn't an FBModelNull or FBModelSkeleton (controls)
    [boneList.append(component) for component in characterList if isinstance(component, (FBModelSkeleton, FBModelNull))]
    
    for bone in boneList:
        # Detect the hash ID at Hash16
        boneId = mobuExtender.atHash16U(bone.Name)
        # If the bone ID is already in the boneHashes dictionary, we'll add a string to the feedback message with the bones that clash, and the ID of those bones
        if boneId in boneHashes:
            boneClashes += 1
            boneString = "\n" + str(boneHashes[boneId].LongName) + " clashes with " + str(bone.LongName) + "\n@ Hash ID " + str(boneId)
            feedbackMessageString = feedbackMessageString + boneString
        else:
            # Otherwise, add the bone ID to the list of hashes we're comparing against later
            boneHashes[boneId] = bone
    
    # Add the end of the feedback string, telling Resource what to do next
    feedbackMessageString = feedbackMessageString + "\n\nRemove any bones you know should not be in the FBX.\
    \nSpeak to the rigging team if you're in doubt on how to proceed."
    
    if boneClashes > 0:
        QtGui.QMessageBox.information(
                                    None,
                                    "Bone Hash Clash Detection",
                                    feedbackMessageString,
                                    QtGui.QMessageBox.Ok
                                )