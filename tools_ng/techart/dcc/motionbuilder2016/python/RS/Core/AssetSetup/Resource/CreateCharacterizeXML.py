from PySide import QtGui

import pyfbsdk as mobu

import os

from RS import Config, Globals, Perforce
from RS.Utils import Path, Scene
from RS.Core import Metadata


def GetCharacter():
                characterList = Globals.Characters
                if len (characterList) > 0:
                                character = characterList[0]
                                return character
                return None

def CreateSkeletonFBXSetupMetaFile():
                '''
                create the xml file for the character

                xml contains:
                bone scene object
                characterization slot
                bone's Rotation
                bone's Translation
                bone's Scaling
                color - based of whether it a a left, right or center bone

                returns: FBCharacter
                '''
                skelRootModelNull = Scene.FindModelByName('SKEL_ROOT')
                fileName = Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName).lower()

                # make sure the user has the latest .psc definitions file
                definitionsFilePath = os.path.join(Config.Project.Path.Root,
                                                   'assets',
                                                   'metadata',
                                                   'definitions',
                                                   'tools',
                                                   'motionbuilder',
                                                   'skeletonFBXSetup.psc')
                Perforce.Sync(definitionsFilePath)

                # get character and error check
                character = GetCharacter()
                if character == None:
                                QtGui.QMessageBox.warning(
                                                None,
                                                "Warning",
                                                "There is no character node present in the file./n/nEnsure you have hit the 'Prep Skeleton for XML' Button\nbefore doing this step.",
                                                QtGui.QMessageBox.Ok
                                )
                                return

                # get characterize type property and data
                characterTypeProperty = character.PropertyList.Find("Character Type")
                characterTypeData = 0
                if characterTypeProperty:
                                characterTypeData = characterTypeProperty.Data

                # get psc file and error check
                if not os.path.exists(definitionsFilePath):
                                QtGui.QMessageBox.warning(
                                                None,
                                                "Warning",
                                                "Unable to find .psc definitions file.\n\nCheck you have the following file:\n\n{0}".format(definitionsFilePath),
                                                QtGui.QMessageBox.Ok
                                )
                                return

                # check characterization
                if not character.GetCharacterize():
                                QtGui.QMessageBox.warning(
                                                None,
                                                "Warning",
                                                "Character is not characterized - check all of the Base Slots have been\nfilled and that you have checked the characterize button On.",
                                                QtGui.QMessageBox.Ok
                                )
                                return

                # get slot and bone linked
                characterizeDict = {}
                for prop in character.PropertyList:
                                if prop.Name.endswith('Link'):
                                                for idx in range(prop.GetSrcCount()):
                                                                slotBone = prop.GetSrc(idx)
                                                                characterizeDict[slotBone] = prop.Name
                                                                continue

                # create a dict of all of the SKEL nulls
                skelList = []
                skeletonDict = {}
                Scene.GetChildren(skelRootModelNull, skelList, "", True)

                for item in skelList:
                                if not item.Name.startswith('SKEL_'):
                                                skelList.remove(item)

                for item in skelList:
                                slotBone = ''
                                for key, value in characterizeDict.iteritems():
                                                if item == key:
                                                                slotBone = value
                                                                break
                                # get color
                                if '_L_' in item.Name:
                                                # green
                                                color = mobu.FBVector3d(0.4, 0.7, 0)
                                elif '_R_' in item.Name:
                                                # orange
                                                color = mobu.FBVector3d(0.9, 0.4, 0)
                                else:
                                                # blue
                                                color = mobu.FBVector3d(0, 0.2, 0.65)

                                skeletonDict[item] = slotBone, item.Rotation, item.Translation, item.Scaling, color

                # create meta file
                metaFile = Metadata.CreateMetaFile("SkeletonSetupFile");
                metaFile.ModelName = fileName
                metaFile.CharacterizeType = str(characterTypeData)

                for key, value in skeletonDict.iteritems():
                                metaItem = metaFile.SkeletonProperties.Create()
                                metaItem.SKELName = key.Name
                                metaItem.CharacterizeSlot = value[0]
                                metaItem.SKELLclRot.X = value[1][0]
                                metaItem.SKELLclRot.Y = value[1][1]
                                metaItem.SKELLclRot.Z = value[1][2]
                                metaItem.SKELLclTrns.X = value[2][0]
                                metaItem.SKELLclTrns.Y = value[2][1]
                                metaItem.SKELLclTrns.Z = value[2][2]
                                metaItem.SKELLclScale.X = value[3][0]
                                metaItem.SKELLclScale.Y = value[3][1]
                                metaItem.SKELLclScale.Z = value[3][2]
                                metaItem.SKELColor.X = value[4][0]
                                metaItem.SKELColor.Y = value[4][1]
                                metaItem.SKELColor.Z = value[4][2]

                metaPath = os.path.join(Config.Project.Path.Root,
                                        'art',
                                        'animation',
                                        'resources',
                                        'characters',
                                        'skeletonSetupXML',
                                        fileName + '.xml')

                if os.path.exists(metaPath):
                                # check out the file in P4
                                Perforce.Edit(metaPath)

                metaFile.SetFilePath(metaPath)
                metaFile.Save()

                # mark xml for add in p4
                Perforce.Add(metaPath)

CreateSkeletonFBXSetupMetaFile()