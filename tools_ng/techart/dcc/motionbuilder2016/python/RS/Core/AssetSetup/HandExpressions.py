from pyfbsdk import *
import RS.Utils.Scene.AnimationNode
import RS.Utils.Scene.Take
from ctypes import *
from pyfbsdk_additions import *
import os

def rsta_ConnectBoxes(sender, senderAttr, receiver, receiverAttr):
    lSourceOut = FindAnimationNode( sender.AnimationNodeOutGet(), senderAttr )
    lDest = FindAnimationNode(receiver.AnimationNodeInGet(), receiverAttr)
    FBConnect(lSourceOut, lDest)

def FindAnimationNode(pParent, pName):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            ##print "Found Node:" + lNode.Name
            break
        ##else:
            ##print "Not Needed/Found Node:" + lNode.Name
    return lResult


##################################
## MH_L/R_Finger_11 constraints
##################################

## STILL UNDECIDED ABOUT POSITION
lMH_L_10_PosX_Con = FBConstraintRelation('MH_L_Finger10-XPosition')

def rs_ScaleFinger10 (ConName, ConReceiver, ConSender, ConSender01, Angle01, Angle02, Axis, Factor):
    lMH_L_10_ScaX_Con = FBConstraintRelation(ConName) 
    senderObj = lMH_L_10_ScaX_Con.SetAsSource(ConSender)
    senderObj.UseGlobalTransforms = False
    receiverObj = lMH_L_10_ScaX_Con.ConstrainObject(ConReceiver)
    receiverObj.UseGlobalTransforms = False
    lMH_L_10_ScaX_Con.SetBoxPosition(receiverObj, 3200, 500)
    
    # Finger 10
    SubtractV1V2 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V2, 350, -300) 
    
    VectorToNumber = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber, 750, -300) 
    
    DegToRad = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad, 1050, -300) 
    
    Multiply = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply, 1450, -300) 
    
    Add = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Add (a + b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add, 1750, -300)
    
    # Finger 11
    SubtractV1V201 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V201, 350, 300) 
    
    VectorToNumber01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber01, 750, 300) 
    
    DegToRad01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad01, 1050, 300) 
    
    Multiply01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply01, 1450, 300) 
    
    senderObj01 = lMH_L_10_ScaX_Con.SetAsSource(ConSender01)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj01, 0, 300) 
    senderObj01.UseGlobalTransforms = False
    
    ##Adding Fingers
    Add01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Add (a + b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add01, 2050, 0)
    
    VectorToNumber02 = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Number to Vector")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber02, 2350, 0) 
    
    Add02 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Add (V1 + V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add02, 2650, 0)
    
    senderObj02 = lMH_L_10_ScaX_Con.SetAsSource(ConReceiver)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj02, 2000, 300)
    senderObj02.UseGlobalTransforms = False
    
    ##Connecting Boxes
    rsta_ConnectBoxes(senderObj, "Lcl Rotation", SubtractV1V2, "V1")
    rsta_ConnectBoxes(SubtractV1V2, "Result", VectorToNumber, "V")
    SubtractV1V2_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V2.AnimationNodeInGet(),'V2')
    SubtractV1V2_B.WriteData(Angle01)
    rsta_ConnectBoxes(VectorToNumber, "Z", DegToRad, "a")
    rsta_ConnectBoxes(DegToRad, "Result", Multiply, "a")
    rsta_ConnectBoxes(Multiply, "Result", Add, "a")
    Multiply_B = RS.Utils.Scene.FindAnimationNode(Multiply.AnimationNodeInGet(),'b')
    Multiply_B.WriteData([Factor])
    rsta_ConnectBoxes(Add, "Result", Add01, "a")
    Add_B = RS.Utils.Scene.FindAnimationNode(Add.AnimationNodeInGet(),'b')
    Add_B.WriteData([1])
    
    rsta_ConnectBoxes(senderObj01, "Lcl Rotation", SubtractV1V201, "V1")
    SubtractV1V201_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V201.AnimationNodeInGet(),'V2')
    SubtractV1V201_B.WriteData(Angle02)
    rsta_ConnectBoxes(SubtractV1V201, "Result", VectorToNumber01, "V")
    ## Important Axis
    rsta_ConnectBoxes(VectorToNumber01, "Z", DegToRad01, "a")
    rsta_ConnectBoxes(DegToRad01, "Result", Multiply01, "a")
    Multiply01_B = RS.Utils.Scene.FindAnimationNode(Multiply01.AnimationNodeInGet(),'b')
    Multiply01_B.WriteData([Factor])
    rsta_ConnectBoxes(Multiply01, "Result", Add01, "b")
    
    rsta_ConnectBoxes(Add01, "Result", VectorToNumber02, Axis)
    rsta_ConnectBoxes(VectorToNumber02, "Result", Add02, "V1")
    if Axis!="X":
        rsta_ConnectBoxes(senderObj02, "Lcl Scaling", Add02, "V2")
    
    rsta_ConnectBoxes(Add02, "Result", receiverObj, "Lcl Scaling")
    lMH_L_10_ScaX_Con.Active = True

### Function for the Bulge Bones
def rs_ScaleBulge (ConName, ConReceiver, ConSender, Angle01, Axis, Factor):
    lMH_L_10_ScaX_Con = FBConstraintRelation(ConName) 
    senderObj = lMH_L_10_ScaX_Con.SetAsSource(ConSender)
    senderObj.UseGlobalTransforms = False
    receiverObj = lMH_L_10_ScaX_Con.ConstrainObject(ConReceiver)
    receiverObj.UseGlobalTransforms = False
    lMH_L_10_ScaX_Con.SetBoxPosition(receiverObj, 3200, 500)
    

    SubtractV1V2 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V2, 350, -300) 
    
    VectorToNumber = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber, 750, -300) 
    
    DegToRad = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad, 1050, -300) 
    
    Multiply = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply, 1450, -300) 
    
    Add = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Add (a + b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add, 1750, -300)
    
    NumberToVector = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Number to Vector")
    lMH_L_10_ScaX_Con.SetBoxPosition(NumberToVector, 2050, -300)
    
    Max = lMH_L_10_ScaX_Con.CreateFunctionBox("My Macros", "max(x,y)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Max, 2350, -300)
    
    
    ##Adding Fingers
    Add01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Add (V1 + V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add01, 2650, 0)
      
    senderObj02 = lMH_L_10_ScaX_Con.SetAsSource(ConReceiver)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj02, 2950, 300)
    senderObj02.UseGlobalTransforms = False
    
    ##Connecting Boxes
    rsta_ConnectBoxes(senderObj, "Lcl Rotation", SubtractV1V2, "V1")
    rsta_ConnectBoxes(SubtractV1V2, "Result", VectorToNumber, "V")
    SubtractV1V2_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V2.AnimationNodeInGet(),'V2')
    SubtractV1V2_B.WriteData(Angle01)
    rsta_ConnectBoxes(VectorToNumber, "Z", DegToRad, "a")
    rsta_ConnectBoxes(DegToRad, "Result", Multiply, "a")
    Multiply_B = RS.Utils.Scene.FindAnimationNode(Multiply.AnimationNodeInGet(),'b')
    Multiply_B.WriteData([Factor])
    rsta_ConnectBoxes(Multiply, "Result", Add, "a")
    Add_B = RS.Utils.Scene.FindAnimationNode(Add.AnimationNodeInGet(),'b')
    Add_B.WriteData([1])
    rsta_ConnectBoxes(Add, "Result", Max, "MacroInput0")
    Max_B = RS.Utils.Scene.FindAnimationNode(Max.AnimationNodeInGet(),'MacroInput1')
    Max_B.WriteData([1])
    rsta_ConnectBoxes(Max, "MacroOutput0", NumberToVector, Axis)
    rsta_ConnectBoxes(NumberToVector, "Result", Add01, "V1")
    if Axis!="X":
        rsta_ConnectBoxes(senderObj02, "Lcl Scaling", Add01, "V2")
    
    rsta_ConnectBoxes(Add01, "Result", receiverObj, "Lcl Scaling")
    lMH_L_10_ScaX_Con.Active = True

def rs_FingerTop (ConName, ConReceiver, ConSender, Angle01, Axis, Factor):
    lMH_L_10_ScaX_Con = FBConstraintRelation(ConName) 
    senderObj = lMH_L_10_ScaX_Con.SetAsSource(ConSender)
    senderObj.UseGlobalTransforms = False
    receiverObj = lMH_L_10_ScaX_Con.ConstrainObject(ConReceiver)
    receiverObj.UseGlobalTransforms = False
    lMH_L_10_ScaX_Con.SetBoxPosition(receiverObj, 3200, 500)
    
    # Finger 10
    SubtractV1V2 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V2, 350, -300) 
    
    VectorToNumber = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber, 750, -300) 
    
    DegToRad = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad, 1050, -300) 
    
    Multiply = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply, 1450, -300) 
    
    # Finger 11
    SubtractV1V201 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V201, 350, 300) 
    
    VectorToNumber01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber01, 750, 300) 
    
    DegToRad01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad01, 1050, 300) 
    
    Multiply01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply01, 1450, 300) 
    
    senderObj01 = lMH_L_10_ScaX_Con.SetAsSource(ConSender)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj01, 0, 300) 
    senderObj01.UseGlobalTransforms = False
    
    ##Adding Fingers
    SubtractV1V202 = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Add (a + b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V202, 1700, 0)

    Add01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Add (a + b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add01, 2050, 0)
    
    Max = lMH_L_10_ScaX_Con.CreateFunctionBox("My Macros", "max(x,y)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Max, 2350, 0)   
    
    VectorToNumber02 = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Number to Vector")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber02, 2650, 0) 
    
    Add02 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Add (V1 + V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add02, 2950, 0)
    
    senderObj02 = lMH_L_10_ScaX_Con.SetAsSource(ConReceiver)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj02, 3100, 0)
    senderObj02.UseGlobalTransforms = False
    
    ##Connecting Boxes
    rsta_ConnectBoxes(senderObj, "Lcl Rotation", SubtractV1V2, "V1")
    rsta_ConnectBoxes(SubtractV1V2, "Result", VectorToNumber, "V")
    SubtractV1V2_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V2.AnimationNodeInGet(),'V2')
    SubtractV1V2_B.WriteData(Angle01)
    rsta_ConnectBoxes(VectorToNumber, "Y", DegToRad, "a")
    rsta_ConnectBoxes(DegToRad, "Result", Multiply, "a")
    rsta_ConnectBoxes(Multiply, "Result", SubtractV1V202, "a")
    Multiply_B = RS.Utils.Scene.FindAnimationNode(Multiply.AnimationNodeInGet(),'b')
    Multiply_B.WriteData([Factor])
    
    rsta_ConnectBoxes(senderObj01, "Lcl Rotation", SubtractV1V201, "V1")
    SubtractV1V201_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V201.AnimationNodeInGet(),'V2')
    SubtractV1V201_B.WriteData(Angle01)
    rsta_ConnectBoxes(SubtractV1V201, "Result", VectorToNumber01, "V")
    ## Important Axis
    rsta_ConnectBoxes(VectorToNumber01, "Z", DegToRad01, "a")
    rsta_ConnectBoxes(DegToRad01, "Result", Multiply01, "a")
    Multiply01_B = RS.Utils.Scene.FindAnimationNode(Multiply01.AnimationNodeInGet(),'b')
    Multiply01_B.WriteData([Factor])
    rsta_ConnectBoxes(Multiply01, "Result", SubtractV1V202, "b")
    

    rsta_ConnectBoxes(SubtractV1V202, "Result", Add01, "b")
    Add01_B = RS.Utils.Scene.FindAnimationNode(Add01.AnimationNodeInGet(),'a')
    Add01_B.WriteData([1])        
    rsta_ConnectBoxes(Add01, "Result", Max, "MacroInput0")
    Max_B = RS.Utils.Scene.FindAnimationNode(Max.AnimationNodeInGet(),'MacroInput1')
    Max_B.WriteData([1])      
    rsta_ConnectBoxes(Max, "MacroOutput0", VectorToNumber02, Axis)
          
    rsta_ConnectBoxes(VectorToNumber02, "Result", Add02, "V1")
    if Axis!="X":   
        rsta_ConnectBoxes(senderObj02, "Lcl Scaling", Add02, "V2")
    
    rsta_ConnectBoxes(Add02, "Result", receiverObj, "Lcl Scaling")
    lMH_L_10_ScaX_Con.Active = True

### Function for the HAND SIDE
def rs_HandSide (ConName, ConReceiver, ConSender, Angle01, Axis, Factor, Factor01):
    lMH_L_10_ScaX_Con = FBConstraintRelation(ConName) 
    senderObj = lMH_L_10_ScaX_Con.SetAsSource(ConSender)
    senderObj.UseGlobalTransforms = False
    receiverObj = lMH_L_10_ScaX_Con.ConstrainObject(ConReceiver)
    receiverObj.UseGlobalTransforms = False
    lMH_L_10_ScaX_Con.SetBoxPosition(receiverObj, 3500, 0)
    

    SubtractV1V2 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V2, 350, -300) 
    
    VectorToNumber = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber, 750, -300) 
    
    DegToRad = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad, 1050, -300) 
    
    Multiply = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply, 1450, -300) 
    
    SubtractV1V201 = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Subtract (a - b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V201, 1750, -300)
    
   
    Max = lMH_L_10_ScaX_Con.CreateFunctionBox("My Macros", "max(x,y)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Max, 2050, -300)

    Min = lMH_L_10_ScaX_Con.CreateFunctionBox("My Macros", "min(x,y)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Min, 2350, -300)
    
    NumberToVector = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Number to Vector")
    lMH_L_10_ScaX_Con.SetBoxPosition(NumberToVector, 2650, -300)
    
    ##Adding Fingers
    Add01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Add (V1 + V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add01, 2950, 0)
      
    senderObj02 = lMH_L_10_ScaX_Con.SetAsSource(ConReceiver)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj02, 3250, 300)
    senderObj02.UseGlobalTransforms = False
    
    ##Connecting Boxes
    rsta_ConnectBoxes(senderObj, "Lcl Rotation", SubtractV1V2, "V1")
    rsta_ConnectBoxes(SubtractV1V2, "Result", VectorToNumber, "V")
    SubtractV1V2_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V2.AnimationNodeInGet(),'V2')
    SubtractV1V2_B.WriteData(Angle01)
    rsta_ConnectBoxes(VectorToNumber, "Y", DegToRad, "a")
    rsta_ConnectBoxes(DegToRad, "Result", Multiply, "a")
    Multiply_B = RS.Utils.Scene.FindAnimationNode(Multiply.AnimationNodeInGet(),'b')
    Multiply_B.WriteData([-1])
    rsta_ConnectBoxes(Multiply, "Result", Max, "MacroInput0")
    #Add_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V201.AnimationNodeInGet(),'b')
    #Add_B.WriteData([1])
    #rsta_ConnectBoxes(SubtractV1V201, "Result", Max, "MacroInput0")
    Max_B = RS.Utils.Scene.FindAnimationNode(Max.AnimationNodeInGet(),'MacroInput1')
    Max_B.WriteData([Factor])

    rsta_ConnectBoxes(Max, "MacroOutput0", Min, "MacroInput0")
    Min_B = RS.Utils.Scene.FindAnimationNode(Min.AnimationNodeInGet(),'MacroInput1')
    Min_B.WriteData([Factor01])
    
    rsta_ConnectBoxes(Min, "MacroOutput0", NumberToVector, Axis)
    rsta_ConnectBoxes(NumberToVector, "Result", Add01, "V1")
    if Axis!="X":
        rsta_ConnectBoxes(senderObj02, "Lcl Scaling", Add01, "V2")
    
    rsta_ConnectBoxes(Add01, "Result", receiverObj, "Lcl Scaling")
    lMH_L_10_ScaX_Con.Active = True

def rs_X_PosFinger10 (ConName, ConReceiver, ConSender, Angle01, Axis, Factor):
    lMH_L_10_ScaX_Con = FBConstraintRelation(ConName) 
    senderObj = lMH_L_10_ScaX_Con.SetAsSource(ConSender)
    senderObj.UseGlobalTransforms = False
    receiverObj = lMH_L_10_ScaX_Con.ConstrainObject(ConReceiver)
    receiverObj.UseGlobalTransforms = False
    lMH_L_10_ScaX_Con.SetBoxPosition(receiverObj, 3200, 0)
    

    SubtractV1V2 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V2, 350, -300) 
    
    VectorToNumber = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber, 750, -300) 
    
    DegToRad = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad, 1050, -300) 
    
    Multiply = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply, 1450, -300) 
       
    NumberToVector = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Number to Vector")
    lMH_L_10_ScaX_Con.SetBoxPosition(NumberToVector, 2050, -300)  
    
    ##Adding Fingers
    Add01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Add (V1 + V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add01, 2650, 0)
      
    senderObj02 = lMH_L_10_ScaX_Con.SetAsSource(ConReceiver)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj02, 2050, 0)
    senderObj02.UseGlobalTransforms = False
    
    ##Connecting Boxes
    rsta_ConnectBoxes(senderObj, "Lcl Rotation", SubtractV1V2, "V1")
    rsta_ConnectBoxes(SubtractV1V2, "Result", VectorToNumber, "V")
    SubtractV1V2_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V2.AnimationNodeInGet(),'V2')
    SubtractV1V2_B.WriteData(Angle01)
    rsta_ConnectBoxes(VectorToNumber, "Y", DegToRad, "a")
    rsta_ConnectBoxes(DegToRad, "Result", Multiply, "a")
    Multiply_B = RS.Utils.Scene.FindAnimationNode(Multiply.AnimationNodeInGet(),'b')
    Multiply_B.WriteData([Factor])
    rsta_ConnectBoxes(Multiply, "Result", NumberToVector, Axis)
    rsta_ConnectBoxes(NumberToVector, "Result", Add01, "V1")
    rsta_ConnectBoxes(senderObj02, "Lcl Scaling", Add01, "V2")
    
    rsta_ConnectBoxes(Add01, "Result", receiverObj, "Lcl Scaling")
    lMH_L_10_ScaX_Con.Active = True

def rs_X_PosHandSide (ConName, ConReceiver, ConSender, Angle01, Axis, Factor, Factor01):
    lMH_L_10_ScaX_Con = FBConstraintRelation(ConName) 
    senderObj = lMH_L_10_ScaX_Con.SetAsSource(ConSender)
    senderObj.UseGlobalTransforms = False
    receiverObj = lMH_L_10_ScaX_Con.ConstrainObject(ConReceiver)
    receiverObj.UseGlobalTransforms = False
    lMH_L_10_ScaX_Con.SetBoxPosition(receiverObj, 3500, 0)
    

    SubtractV1V2 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V2, 350, -300) 
    
    VectorToNumber = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber, 750, -300) 
    
    DegToRad = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad, 1050, -300) 
    
    Multiply = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply, 1450, -300) 
       
    Max = lMH_L_10_ScaX_Con.CreateFunctionBox("My Macros", "max(x,y)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Max, 2050, -300)

    Min = lMH_L_10_ScaX_Con.CreateFunctionBox("My Macros", "min(x,y)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Min, 2350, -300)
    
    NumberToVector = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Number to Vector")
    lMH_L_10_ScaX_Con.SetBoxPosition(NumberToVector, 2650, -300)
    
    ##Adding Fingers
    Add01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Add (V1 + V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add01, 2950, 0)
      
    senderObj02 = lMH_L_10_ScaX_Con.SetAsSource(ConReceiver)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj02, 3250, 300)
    senderObj02.UseGlobalTransforms = False
    
    ##Connecting Boxes
    rsta_ConnectBoxes(senderObj, "Lcl Rotation", SubtractV1V2, "V1")
    rsta_ConnectBoxes(SubtractV1V2, "Result", VectorToNumber, "V")
    SubtractV1V2_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V2.AnimationNodeInGet(),'V2')
    SubtractV1V2_B.WriteData(Angle01)
    rsta_ConnectBoxes(VectorToNumber, "Y", DegToRad, "a")
    rsta_ConnectBoxes(DegToRad, "Result", Multiply, "a")
    Multiply_B = RS.Utils.Scene.FindAnimationNode(Multiply.AnimationNodeInGet(),'b')
    Multiply_B.WriteData([-0.01])
    rsta_ConnectBoxes(Multiply, "Result", Max, "MacroInput0")
    Max_B = RS.Utils.Scene.FindAnimationNode(Max.AnimationNodeInGet(),'MacroInput1')
    Max_B.WriteData([Factor])

    rsta_ConnectBoxes(Max, "MacroOutput0", Min, "MacroInput0")
    Min_B = RS.Utils.Scene.FindAnimationNode(Min.AnimationNodeInGet(),'MacroInput1')
    Min_B.WriteData([Factor01])
    
    rsta_ConnectBoxes(Min, "MacroOutput0", NumberToVector, Axis)
    rsta_ConnectBoxes(NumberToVector, "Result", Add01, "V1")
    rsta_ConnectBoxes(senderObj02, "Lcl Translation", Add01, "V2")
    
    rsta_ConnectBoxes(Add01, "Result", receiverObj, "Lcl Translation")
    lMH_L_10_ScaX_Con.Active = True
    

def rs_XPosFinger00 (ConName, ConReceiver, ConSender, ConSender01, Angle01, Angle02, Axis, Factor):
    lMH_L_10_ScaX_Con = FBConstraintRelation(ConName) 
    senderObj = lMH_L_10_ScaX_Con.SetAsSource(ConSender)
    senderObj.UseGlobalTransforms = False
    receiverObj = lMH_L_10_ScaX_Con.ConstrainObject(ConReceiver)
    receiverObj.UseGlobalTransforms = False
    lMH_L_10_ScaX_Con.SetBoxPosition(receiverObj, 3200, 500)
    
    # Finger 10
    SubtractV1V2 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V2, 350, -300)
    
    VectorToNumber = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber, 750, -300) 
    
    DegToRad = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad, 1050, -300) 
    
    Multiply = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply, 1450, -300) 
    
    
    # Finger 11
    SubtractV1V201 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(SubtractV1V201, 350, 300) 
    
    VectorToNumber01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Vector to Number")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber01, 750, 300) 
    
    DegToRad01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Deg To Rad")
    lMH_L_10_ScaX_Con.SetBoxPosition(DegToRad01, 1050, 300) 
    
    Multiply01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Multiply (a x b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Multiply01, 1450, 300) 
    
    senderObj01 = lMH_L_10_ScaX_Con.SetAsSource(ConSender01)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj01, 0, 300) 
    senderObj01.UseGlobalTransforms = False
    
    ##Adding Fingers
    Add01 = lMH_L_10_ScaX_Con.CreateFunctionBox("Number", "Add (a + b)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add01, 2050, 0)
    
    VectorToNumber02 = lMH_L_10_ScaX_Con.CreateFunctionBox("Converters", "Number to Vector")
    lMH_L_10_ScaX_Con.SetBoxPosition(VectorToNumber02, 2350, 0) 
    
    Add02 = lMH_L_10_ScaX_Con.CreateFunctionBox("Vector", "Add (V1 + V2)")
    lMH_L_10_ScaX_Con.SetBoxPosition(Add02, 2650, 0)
    
    senderObj02 = lMH_L_10_ScaX_Con.SetAsSource(ConReceiver)
    lMH_L_10_ScaX_Con.SetBoxPosition(senderObj02, 2000, 300)
    senderObj02.UseGlobalTransforms = False
    
    ##Connecting Boxes
    rsta_ConnectBoxes(senderObj, "Lcl Rotation", SubtractV1V2, "V1")
    rsta_ConnectBoxes(SubtractV1V2, "Result", VectorToNumber, "V")
    SubtractV1V2_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V2.AnimationNodeInGet(),'V2')
    SubtractV1V2_B.WriteData(Angle01)
    rsta_ConnectBoxes(VectorToNumber, "Z", DegToRad, "a")
    rsta_ConnectBoxes(DegToRad, "Result", Multiply, "a")
    rsta_ConnectBoxes(Multiply, "Result", Add01, "a")
    Multiply_B = RS.Utils.Scene.FindAnimationNode(Multiply.AnimationNodeInGet(),'b')
    Multiply_B.WriteData([Factor])
        
    rsta_ConnectBoxes(senderObj01, "Lcl Rotation", SubtractV1V201, "V1")
    SubtractV1V201_B = RS.Utils.Scene.FindAnimationNode(SubtractV1V201.AnimationNodeInGet(),'V2')
    SubtractV1V201_B.WriteData(Angle02)
    rsta_ConnectBoxes(SubtractV1V201, "Result", VectorToNumber01, "V")
    ## Important Axis
    rsta_ConnectBoxes(VectorToNumber01, "Z", DegToRad01, "a")
    rsta_ConnectBoxes(DegToRad01, "Result", Multiply01, "a")
    Multiply01_B = RS.Utils.Scene.FindAnimationNode(Multiply01.AnimationNodeInGet(),'b')
    Multiply01_B.WriteData([Factor])
    rsta_ConnectBoxes(Multiply01, "Result", Add01, "b")
    
    rsta_ConnectBoxes(Add01, "Result", VectorToNumber02, Axis)
    rsta_ConnectBoxes(VectorToNumber02, "Result", Add02, "V1")
    rsta_ConnectBoxes(senderObj02, "Lcl Translation", Add02, "V2")
    
    rsta_ConnectBoxes(Add02, "Result", receiverObj, "Lcl Translation")

    lMH_L_10_ScaX_Con.Active = True


def initialiseHandDeformerSetup():
    
    lHandDeformers = []
    ## Driver
    lSKEL_L_Finger00 = RS.Utils.Scene.FindModelByName('SKEL_L_Finger00')
    lSKEL_L_Finger01 = RS.Utils.Scene.FindModelByName('SKEL_L_Finger01')
    lSKEL_L_Finger02 = RS.Utils.Scene.FindModelByName('SKEL_L_Finger02')
    lSKEL_L_Finger10 = RS.Utils.Scene.FindModelByName('SKEL_L_Finger10')
    lSKEL_L_Finger11 = RS.Utils.Scene.FindModelByName('SKEL_L_Finger11')
    lSKEL_L_Finger40 = RS.Utils.Scene.FindModelByName('SKEL_L_Finger40')
    lSKEL_L_Hand = RS.Utils.Scene.FindModelByName('SKEL_L_Hand')
    
    lSKEL_R_Finger00 = RS.Utils.Scene.FindModelByName('SKEL_R_Finger00')
    lSKEL_R_Finger01 = RS.Utils.Scene.FindModelByName('SKEL_R_Finger01')
    lSKEL_R_Finger02 = RS.Utils.Scene.FindModelByName('SKEL_R_Finger02')
    lSKEL_R_Finger10 = RS.Utils.Scene.FindModelByName('SKEL_R_Finger10')
    lSKEL_R_Finger11 = RS.Utils.Scene.FindModelByName('SKEL_R_Finger11')
    lSKEL_R_Finger40 = RS.Utils.Scene.FindModelByName('SKEL_R_Finger40')
    lSKEL_R_Hand = RS.Utils.Scene.FindModelByName('SKEL_R_Hand')
    
    ##Driven
    lMH_L_FingerBulge00 = RS.Utils.Scene.FindModelByName('MH_L_FingerBulge00')
    lMH_L_FingerTop00 = RS.Utils.Scene.FindModelByName('MH_L_FingerTop00')
    lMH_L_HandSide = RS.Utils.Scene.FindModelByName('MH_L_HandSide')
    lMH_L_Finger00 = RS.Utils.Scene.FindModelByName('MH_L_Finger00')
    lMH_L_Finger10 = RS.Utils.Scene.FindModelByName('MH_L_Finger10')
    
    lMH_R_FingerBulge00 = RS.Utils.Scene.FindModelByName('MH_R_FingerBulge00')
    lMH_R_FingerTop00 = RS.Utils.Scene.FindModelByName('MH_R_FingerTop00')
    lMH_R_HandSide = RS.Utils.Scene.FindModelByName('MH_R_HandSide')
    lMH_R_Finger00 = RS.Utils.Scene.FindModelByName('MH_R_Finger00')
    lMH_R_Finger10 = RS.Utils.Scene.FindModelByName('MH_R_Finger10')
    
    lHandDeformers = [lSKEL_L_Finger00,lSKEL_L_Finger01,lSKEL_L_Finger02,lSKEL_L_Finger10,lSKEL_L_Finger11,lSKEL_L_Finger40,lSKEL_L_Hand,
                      lSKEL_R_Finger00,lSKEL_R_Finger01,lSKEL_R_Finger02,lSKEL_R_Finger10,lSKEL_R_Finger11,lSKEL_R_Finger40,lSKEL_R_Hand,
                      lMH_L_FingerBulge00,lMH_L_FingerTop00,lMH_L_HandSide,lMH_L_Finger00,lMH_L_Finger10,
                      lMH_R_FingerBulge00,lMH_R_FingerTop00,lMH_R_HandSide,lMH_R_Finger00,lMH_R_Finger10]
    
    for deformer in lHandDeformers:
        if (deformer == None):
            print "Missing hand deformers"
            return False
    
    ##MH_L_Finger10
    rs_ScaleFinger10("MH_L_Finger10-X_scale", lMH_L_Finger10, lSKEL_L_Finger10, lSKEL_L_Finger11, [-90,0,0],[0,0,0],"X", 0.10)
    rs_ScaleFinger10("MH_L_Finger10-Y_scale", lMH_L_Finger10, lSKEL_L_Finger10, lSKEL_L_Finger11, [-90,0,0],[0,0,0],"Y", -0.30)
    rs_ScaleFinger10("MH_L_Finger10-Z_scale", lMH_L_Finger10, lSKEL_L_Finger10, lSKEL_L_Finger11, [-90,0,0],[0,0,0],"Z", -0.30)
    
    ##MH_L_Finger00
    rs_ScaleFinger10("MH_L_Finger00-X_scale", lMH_L_Finger00, lSKEL_L_Finger01, lSKEL_L_Finger02, [0.4,1.43,-16.56],[0.05,1.16,-13.27],"X", 0.10)
    rs_ScaleFinger10("MH_L_Finger00-Y_scale", lMH_L_Finger00, lSKEL_L_Finger01, lSKEL_L_Finger02, [0.4,1.43,-16.56],[0.05,1.16,-13.27],"Y", -0.30)
    rs_ScaleFinger10("MH_L_Finger00-Z_scale", lMH_L_Finger00, lSKEL_L_Finger01, lSKEL_L_Finger02, [0.4,1.43,-16.56],[0.05,1.16,-13.27],"Z", -0.30)
    
    
    ##MH_R_Finger10
    rs_ScaleFinger10("MH_R_Finger10-X_scale", lMH_R_Finger10, lSKEL_R_Finger10, lSKEL_R_Finger11, [-90,0,0],[0,0,0],"X", -0.10)
    rs_ScaleFinger10("MH_R_Finger10-Y_scale", lMH_R_Finger10, lSKEL_R_Finger10, lSKEL_R_Finger11, [-90,0,0],[0,0,0],"Y", 0.30)
    rs_ScaleFinger10("MH_R_Finger10-Z_scale", lMH_R_Finger10, lSKEL_R_Finger10, lSKEL_R_Finger11, [-90,0,0],[0,0,0],"Z", 0.30)
    
    ##MH_R_Finger00
    rs_ScaleFinger10("MH_R_Finger00-X_scale", lMH_R_Finger00, lSKEL_R_Finger01, lSKEL_R_Finger02, [-0.4,-1.43,-16.56],[-0.05,-1.16,-13.27],"X", 0.10)
    rs_ScaleFinger10("MH_R_Finger00-Y_scale", lMH_R_Finger00, lSKEL_R_Finger01, lSKEL_R_Finger02, [-0.4,-1.43,-16.56],[-0.05,-1.16,-13.27],"Y", -0.30)
    rs_ScaleFinger10("MH_R_Finger00-Z_scale", lMH_R_Finger00, lSKEL_R_Finger01, lSKEL_R_Finger02, [-0.4,-1.43,-16.56],[-0.05,-1.16,-13.27],"Z", -0.40)
    
    ##MH_L_Bulge00
    rs_ScaleBulge("MH_L_FingerBulge00-X_scale", lMH_L_FingerBulge00, lSKEL_L_Finger00, [-10.34,-15.85,30.60], "X", -1.5)
    rs_ScaleBulge("MH_L_FingerBulge00-Y_scale", lMH_L_FingerBulge00, lSKEL_L_Finger00, [-10.34,-15.85,30.60], "Y", -1.5)
    rs_ScaleBulge("MH_L_FingerBulge00-Z_scale", lMH_L_FingerBulge00, lSKEL_L_Finger00, [-10.34,-15.85,30.60], "Z", -1.5)
    
    ##MH_R_Bulge00
    rs_ScaleBulge("MH_R_FingerBulge00-X_scale", lMH_R_FingerBulge00, lSKEL_R_Finger00, [10.34,15.85,30.60], "X", -1.5)
    rs_ScaleBulge("MH_R_FingerBulge00-Y_scale", lMH_R_FingerBulge00, lSKEL_R_Finger00, [10.34,15.85,30.60], "Y", -1.5)
    rs_ScaleBulge("MH_R_FingerBulge00-Z_scale", lMH_R_FingerBulge00, lSKEL_R_Finger00, [10.34,15.85,30.60], "Z", -1.5)
    
    ##MH_L_FingerTop00
    rs_FingerTop("MH_L_FingerTop00-X_scale", lMH_L_FingerTop00, lSKEL_L_Finger00, [-10.34,-15.85,30.60], "X", -1.5)
    rs_FingerTop("MH_L_FingerTop00-Y_scale", lMH_L_FingerTop00, lSKEL_L_Finger00, [-10.34,-15.85,30.60], "Y", -1.5)
    rs_FingerTop("MH_L_FingerTop00-Z_scale", lMH_L_FingerTop00, lSKEL_L_Finger00, [-10.34,-15.85,30.60], "Z", -1.5)
    
    ##MH_R_FingerTop00
    rs_FingerTop("MH_R_FingerTop00-X_scale", lMH_R_FingerTop00, lSKEL_R_Finger00, [10.34,15.85,30.60], "X", -1.5)
    rs_FingerTop("MH_R_FingerTop00-Y_scale", lMH_R_FingerTop00, lSKEL_R_Finger00, [10.34,15.85,30.60], "Y", -1.5)
    rs_FingerTop("MH_R_FingerTop00-Z_scale", lMH_R_FingerTop00, lSKEL_R_Finger00, [10.34,15.85,30.60], "Z", -1.5)
    
    ##MH_L_HandSide
    rs_HandSide("MH_L_HandSide-X_scale", lMH_L_HandSide, lSKEL_L_Finger40, [-90,0,0], "X", 1, 1.23)
    rs_HandSide("MH_L_HandSide-Y_scale", lMH_L_HandSide, lSKEL_L_Finger40, [-90,0,0], "Y", 1, 1.23)
    
    ##MH_R_HandSide
    rs_HandSide("MH_R_HandSide-X_scale", lMH_R_HandSide, lSKEL_R_Finger40, [-90,0,0], "X", 1, 1.23)
    rs_HandSide("MH_R_HandSide-Y_scale", lMH_R_HandSide, lSKEL_R_Finger40, [-90,0,0], "Y", 1, 1.23)
    
    
    ##-----------------------------------------------------------------------------------------------
    ##Position Constraints
    
    ##MH_L_Finger00
    rs_XPosFinger00("MH_L_Finger00-X_Position", lMH_L_Finger00, lSKEL_L_Finger01, lSKEL_L_Finger02,[0.4,1.43,-16.56],[0.05,1.16,-13.27],"X", 0.002)
    
    ##MH_R_Finger00
    rs_XPosFinger00("MH_R_Finger00-X_Position", lMH_R_Finger00, lSKEL_R_Finger01, lSKEL_R_Finger02,[-0.4,-1.43,-16.56],[-0.05,-1.16,-13.27],"X", 0.002)
    
    
    
    ##MH_L_Finger10
    rs_X_PosFinger10("MH_L_Finger10-X_Position", lMH_L_Finger10, lSKEL_L_Finger10, [-90,0,0], "X", 0.002)
    
    ##MH_R_Finger10
    rs_X_PosFinger10("MH_R_Finger10-X_Position", lMH_R_Finger10, lSKEL_R_Finger10, [-90,0,0], "X", 0.002)
    
    
    
    ##MH_L_HandSide
    rs_X_PosHandSide("MH_L_HandSide-Y_Position", lMH_L_HandSide, lSKEL_L_Finger40, [-90,0,0], "Y", -0.01, 0)
    
    ##MH_R_HandSide
    rs_X_PosHandSide("MH_R_HandSide-Y_Position", lMH_R_HandSide, lSKEL_R_Finger40, [-90,0,0], "Y", -0.01, 0)

    print "Hand deformers setup complete"

initialiseHandDeformerSetup()



























