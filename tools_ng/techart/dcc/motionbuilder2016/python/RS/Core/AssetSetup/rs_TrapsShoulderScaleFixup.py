from pyfbsdk import *

import RS.Core.AssetSetup.Lib as ass
import RS.Globals as glo
import RS.Utils.Creation as cre
import RS.Core.Reference.SceneConfigXML as xml
import RS.Core.Face.RigUISetup as lat
import RS.Core.Face.ConnectRigInABox as concs
import RS.Core.Face.ConnectRigInABoxAmbient as conig
import RS.Core.Character.IngameCharacterIKControls as ikcon
import RS.Core.Character.Setup.HighHeel as heel
##import RS.Core.AssetSetup.Characters as chars
import RS.Core.Face.RelationConstraints as relationCon

import RS.Utils.Path
import RS.Utils.Scene

def RSTA_querySender(ownerNode, boxNode):
    connections = []
    
    animNodes = boxNode.AnimationNodeOutGet().Nodes
    for animNode in animNodes:
        for i in range(0, animNode.GetDstCount()):
            
            animDest = animNode.GetDst(i)
            if animDest != None:
                ##print "Owner name: "+animDest.GetOwner().Name
                if ownerNode.Name.lower() not in animDest.GetOwner().Name.lower():
                    #== [sender anim node, receiver anim node]
                   ## connections.append([animNode, animDest]) 
                    connections.append([animNode, animDest.GetOwner()]) 
                    
##                else:
                    ##print "BAD CONN:", ownerNode.Name.lower(), animDest.GetOwner().Name.lower()
    return connections                       
 
def FindAnimationNode( pParent, pName ):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult 
                 
                    
def rs_ShoulderScaleFixup(side):

    trapSupport = None
    upperArm = None
    trapName = None
    if side == "_R_":
        trapSupport = RS.Utils.Scene.FindModelByName("Trap_R_Support")
        upperArm = RS.Utils.Scene.FindModelByName("SKEL_R_UpperArm")
        trapName = "MH_R_Trap"
    else:
        trapSupport = RS.Utils.Scene.FindModelByName("Trap_L_Support")
        upperArm = RS.Utils.Scene.FindModelByName("SKEL_L_UpperArm")
        trapName = "MH_L_Trap"

    if trapSupport and upperArm:
        #print "Found Trap setup for "+side+"..."
        
        for i in RS.Globals.gConstraints:
            iName = i.Name
            lNameLength = len(iName)
            lThisNameLower = iName.lower()
                
            if iName.find(trapName) != -1:

                #print "Found trapNode "+iName
                    
                #print "Found the following boxes:"
                
                for b in i.Boxes:
                    bName = b.Name
                    if bName.find(trapName) != -1:
                                    
                       # print "FOUND "+b.Name
                        senderData = RSTA_querySender(i, b)
                        ##print "SenderData: "+str(senderData)
                        
                        senderCount = len(senderData)
                        ##print "SenderData count: "+str(senderCount)
                        
                        if senderCount != 0:

                           # print "animNode: "+senderData[0][0].Name
                           # print "animDest: "+senderData[0][1].Name                            
                            
                            if senderData[0][0].Name == "Lcl Scaling":
                               # print "Valid sender box is "+b.Name
                       
                                senderBox = b
                                receiverBox = senderData[0][1]
                               # print "Receiver: "+receiverBox.Name
                                ##OK NOW WE HAVE FOUND THE SENDER BOX WE CAN ACTUALLY DO WHAT WE NEED TO!
                                ## we disconnect this senderBox from receiverBoxx, make the new nodes then reconnect everything
                                senderOut = FindAnimationNode( senderBox.AnimationNodeOutGet(), 'Lcl Scaling' )
                                receiverIn = FindAnimationNode( receiverBox.AnimationNodeInGet(), 'V2' )
                                FBDisconnect(senderOut, receiverIn)
                                
                                lNumberToVectorBox = i.CreateFunctionBox( 'Converters', 'Vector to Number' )
                                i.SetBoxPosition( lNumberToVectorBox, 200, (200 * 25) )                                
                                lNumberToVectorOutX = FindAnimationNode( lNumberToVectorBox.AnimationNodeOutGet(), 'X' )
                                lNumberToVectorOutY = FindAnimationNode( lNumberToVectorBox.AnimationNodeOutGet(), 'Y' )
                                lNumberToVectorOutZ = FindAnimationNode( lNumberToVectorBox.AnimationNodeOutGet(), 'Z' )

                                lNumberToVectorIn = FindAnimationNode( lNumberToVectorBox.AnimationNodeInGet(), 'V' )

                                FBConnect(senderOut, lNumberToVectorIn)
                                
                                lVectorToNumberBox = i.CreateFunctionBox( 'Converters', 'Number to Vector' )                                
                                i.SetBoxPosition( lVectorToNumberBox, 470, (200 * 25) ) 
                                lVectorToNumberBoxInX = FindAnimationNode( lVectorToNumberBox.AnimationNodeInGet(), 'X' )
                                lVectorToNumberBoxInY = FindAnimationNode( lVectorToNumberBox.AnimationNodeInGet(), 'Y' )
                                lVectorToNumberBoxInZ = FindAnimationNode( lVectorToNumberBox.AnimationNodeInGet(), 'Z' )
                                lVectorToNumberBoxOut = FindAnimationNode( lVectorToNumberBox.AnimationNodeOutGet(), 'Result' )
##                                print dir(lVectorToNumberBox)
                                ##print dir(lVectorToNumberBox.PropertyList)
                                
                                FBConnect( lNumberToVectorOutY, lVectorToNumberBoxInY)
                                FBConnect( lNumberToVectorOutZ, lVectorToNumberBoxInZ)
                                
                                FBConnect( lVectorToNumberBoxOut, receiverIn)
                                print "Shoulder fixup applied to "+side
                        print "---------------------------"

                    
                    
##rs_ShoulderScaleFixup("_R_")
##rs_ShoulderScaleFixup("_L_")