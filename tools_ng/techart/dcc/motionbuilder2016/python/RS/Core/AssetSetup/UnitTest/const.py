import os

import scandir


class ConfigPathDict(object):
    """Yaml config paths for unittests"""

    RIG_DATA_CONFIG_PATH = os.path.join(os.path.dirname(__file__), "configs", "rigData.yaml")


class FileNamesDict(object):
    """File names for storing data"""

    CONTROL_RIG_NAME = "controlrig.xml"
    SKELETON_NAME = "skeleton.xml"
    ORGANIZATION_NAME = "organization.xml"


class TestPathDict(object):
    """Project based asset path"""

    ASSET_DIR = os.path.join(os.path.dirname(__file__), "Fbx")
    ASSET_PATHS = {
        "blueJay": os.path.join(ASSET_DIR, "A_C_BlueJay_01.fbx"),
        "heron": os.path.join(ASSET_DIR, "A_C_Heron_01.FBX"),
        "chicken": os.path.join(ASSET_DIR, "A_C_Prairiechicken_01.fbx"),
        "shark": os.path.join(ASSET_DIR, "A_C_SharkHammerhead_01.FBX"),
        "turkey": os.path.join(ASSET_DIR, "A_C_Turkey_01.fbx"),
    }
