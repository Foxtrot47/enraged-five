''' 
Kathryn Adams 13.11.14

Cecelio's custom rowboat rig

This will be automatically run from the Asset Setups vehicle setup button.  
- If script finds the vehicle is called 'rowboat' this script will trigger.

NOTE:-
Animator will be required to add L & R Oar Root Controls to a P/C Constraint to hook it up completely in a scene. 
Animator Setups:
1. Reference in rowboat
2. Reference in 2x Oars
3. Select 'P_boat_oar_01_S_Root_Control' and add to the Parent/Child Constraint 'l_oar_root_parent_con', as a child
4. Select 'P_boat_oar_01_S_Root_Control^1' and add to the Parent/Child Constraint 'r_oar_root_parent_con', as a child
5. Snap both constraints on


'''

from pyfbsdk import *

import RS.Utils
import RS.Globals


def Rowboat_Oar_Custom_Rig_Setup():
    # rowboat bones needed
    rowboat_mover = RS.Utils.Scene.FindModelByName( "mover" )
    r_oar_par_bone = RS.Utils.Scene.FindModelByName( "R_Oar_Par" )
    l_oar_par_bone = RS.Utils.Scene.FindModelByName( "L_Oar_Par" )
    r_rowlock_ik_att_bone = RS.Utils.Scene.FindModelByName( "R_RowLock_IK_att" )
    l_rowlock_ik_att_bone = RS.Utils.Scene.FindModelByName( "L_RowLock_IK_att" )
    r_rowlock_att_bone = RS.Utils.Scene.FindModelByName( "R_RowLock_att" )
    l_rowlock_att_bone = RS.Utils.Scene.FindModelByName( "L_RowLock_att" )
    
    
    # create rig elements
    rowboat_mover_marker = FBModelMarker( "rowboat_mover_Marker" )
    rowboat_mover_marker.Color = FBColor( 0, 0, 1 )
    rowboat_mover_marker.Size = 400
    rowboat_mover_marker.Show = True
    
    l_oar_ik_effector_null = FBModelNull( "L_Oar_IK_Effector" )
    r_oar_ik_effector_null = FBModelNull( "R_Oar_IK_Effector" )
    
    r_oar_first_joint_marker = FBModelMarker( "R_Oar_First_Joint" )
    r_oar_first_joint_marker.Look = FBMarkerLook.kFBMarkerLookHardCross
    r_oar_first_joint_marker.Color = FBColor( 0, 0, 1 )
    r_oar_first_joint_marker.Size = 200
    r_oar_first_joint_marker.Show = True
    r_oar_last_joint_marker = FBModelNull( "R_Oar_Last_Joint" )
    r_oar_ik_polevector_marker = FBModelMarker( "R_Oar_IK_PoleVector" )
    r_oar_ik_polevector_marker.Look = FBMarkerLook.kFBMarkerLookSphere
    r_oar_ik_polevector_marker.Size = 200
    r_oar_copier_null = FBModelNull( "R_Oar_Copier" )
    
    l_oar_first_joint_marker = FBModelMarker( "L_Oar_First_Joint" )
    l_oar_first_joint_marker.Look = FBMarkerLook.kFBMarkerLookHardCross 
    l_oar_first_joint_marker.Color = FBColor( 0, 0, 1 )
    l_oar_first_joint_marker.Size = 200
    l_oar_first_joint_marker.Show = True
    l_oar_last_joint_marker = FBModelNull( "L_Oar_Last_Joint" )
    l_oar_ik_polevector_marker = FBModelMarker( "L_Oar_IK_PoleVector" )
    l_oar_ik_polevector_marker.Look = FBMarkerLook.kFBMarkerLookSphere
    l_oar_ik_polevector_marker.Size = 200
    l_oar_copier_null = FBModelNull( "L_Oar_Copier" )
    
    
    # set element transforms
    vector = FBVector3d()
    rowboat_mover.GetVector( vector, FBModelTransformationType.kModelTranslation, True )
    rowboat_mover_marker.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    rowboat_mover.GetVector( vector, FBModelTransformationType.kModelRotation, True )
    rowboat_mover_marker.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    
    r_rowlock_ik_att_bone.GetVector( vector, FBModelTransformationType.kModelTranslation, True )
    r_oar_ik_effector_null.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    r_oar_last_joint_marker.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    r_rowlock_ik_att_bone.GetVector( vector, FBModelTransformationType.kModelRotation, True )
    vector = FBVector3d( vector[0], vector[1], -180 )
    r_oar_ik_effector_null.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    r_oar_last_joint_marker.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    
    l_rowlock_ik_att_bone.GetVector( vector, FBModelTransformationType.kModelTranslation, True )
    l_oar_ik_effector_null.SetVector( ( vector ), FBModelTransformationType.kModelTranslation, True )
    l_oar_last_joint_marker.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    l_rowlock_ik_att_bone.GetVector( vector, FBModelTransformationType.kModelRotation, True )
    l_oar_ik_effector_null.SetVector( ( vector ), FBModelTransformationType.kModelRotation, True )
    l_oar_last_joint_marker.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    
    l_oar_par_bone.GetVector( vector, FBModelTransformationType.kModelTranslation, True )
    r_oar_first_joint_marker.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    r_oar_copier_null.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    l_oar_par_bone.GetVector( vector, FBModelTransformationType.kModelRotation, True )
    r_oar_first_joint_marker.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    r_oar_copier_null.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    
    r_oar_par_bone.GetVector( vector, FBModelTransformationType.kModelTranslation, True )
    l_oar_first_joint_marker.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    l_oar_copier_null.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    r_oar_par_bone.GetVector( vector, FBModelTransformationType.kModelRotation, True )
    l_oar_first_joint_marker.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    l_oar_copier_null.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    
    vector = FBVector3d( 67.73, 107.50, 55.40 )
    r_oar_ik_polevector_marker.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    vector = FBVector3d( -67.73 , 107.50, 55.40 )
    l_oar_ik_polevector_marker.SetVector( vector, FBModelTransformationType.kModelTranslation, True )
    vector = FBVector3d( -90, 0, 0 )
    r_oar_ik_polevector_marker.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    vector = FBVector3d( -90, 0, 180 )
    l_oar_ik_polevector_marker.SetVector( vector, FBModelTransformationType.kModelRotation, True )
    
    
    # parent the rig together
    r_oar_first_joint_marker.Parent = rowboat_mover_marker
    l_oar_first_joint_marker.Parent = rowboat_mover_marker
    r_oar_last_joint_marker.Parent = r_oar_first_joint_marker
    l_oar_last_joint_marker.Parent = l_oar_first_joint_marker
    l_oar_ik_polevector_marker.Parent = l_oar_first_joint_marker
    r_oar_ik_polevector_marker.Parent = r_oar_first_joint_marker
    l_oar_copier_null.Parent = l_oar_first_joint_marker
    r_oar_copier_null.Parent = r_oar_first_joint_marker
    
    
    # create constraints
    l_oar_root_parent_con = FBConstraintManager().TypeCreateConstraint( 3 )
    l_oar_root_parent_con.Name = ( "l_oar_root_parent" )
    l_oar_root_parent_con.ReferenceAdd ( 1, l_rowlock_ik_att_bone )
    
    r_oar_root_parent_con = FBConstraintManager().TypeCreateConstraint( 3 )
    r_oar_root_parent_con.Name = ( "r_oar_root_parent" )
    r_oar_root_parent_con.ReferenceAdd ( 1, r_rowlock_ik_att_bone )
    
    l_oar_master_parent_con = FBConstraintManager().TypeCreateConstraint( 3 )
    l_oar_master_parent_con.Name = ( "l_oar_master_parent" )
    l_oar_master_parent_con.ReferenceAdd ( 0, l_rowlock_ik_att_bone )
    l_oar_master_parent_con.ReferenceAdd ( 1, l_oar_copier_null )
    l_oar_master_parent_con.Snap()
    
    r_oar_master_parent_con = FBConstraintManager().TypeCreateConstraint( 3 )
    r_oar_master_parent_con.Name = ( "r_oar_master_parent" )
    r_oar_master_parent_con.ReferenceAdd ( 0, r_rowlock_ik_att_bone )
    r_oar_master_parent_con.ReferenceAdd ( 1, r_oar_copier_null )
    r_oar_master_parent_con.Snap()
    
    l_oar_chain_ik_con = FBConstraintManager().TypeCreateConstraint( 13 )
    l_oar_chain_ik_con.Name = ( "l_oar_chain_ik" )
    l_oar_chain_ik_con.ReferenceAdd ( 0, l_oar_first_joint_marker )
    l_oar_chain_ik_con.ReferenceAdd ( 1, l_oar_last_joint_marker )
    l_oar_chain_ik_con.ReferenceAdd ( 2, l_oar_ik_effector_null )
    l_oar_chain_ik_con.ReferenceAdd ( 4, l_oar_ik_polevector_marker )
    l_oar_chain_ik_con.Snap()
    
    r_oar_chain_ik_con = FBConstraintManager().TypeCreateConstraint( 13 )
    r_oar_chain_ik_con.Name = ( "r_oar_chain_ik" )
    r_oar_chain_ik_con.ReferenceAdd ( 0, r_oar_first_joint_marker )
    r_oar_chain_ik_con.ReferenceAdd ( 1, r_oar_last_joint_marker )
    r_oar_chain_ik_con.ReferenceAdd ( 2, r_oar_ik_effector_null )
    r_oar_chain_ik_con.ReferenceAdd ( 4, r_oar_ik_polevector_marker )
    r_oar_chain_ik_con.Snap()
    
    l_rowhole_aim_con = FBConstraintManager().TypeCreateConstraint( 0 )
    l_rowhole_aim_con.Name = ( "l_rowhole_aim" )
    l_rowhole_aim_con.ReferenceAdd ( 0, l_rowlock_att_bone )
    l_rowhole_aim_con.ReferenceAdd ( 1, l_oar_first_joint_marker )
    l_rowhole_aim_con.Snap()
    
    r_rowhole_aim_con = FBConstraintManager().TypeCreateConstraint( 0 )
    r_rowhole_aim_con.Name = ( "r_rowhole_aim" )
    r_rowhole_aim_con.ReferenceAdd ( 0, r_rowlock_att_bone )
    r_rowhole_aim_con.ReferenceAdd ( 1, r_oar_first_joint_marker )
    affect_x_r = r_rowhole_aim_con.PropertyList.Find( 'AffectX' )
    affect_z_r = r_rowhole_aim_con.PropertyList.Find( 'AffectZ' )
    r_rowhole_aim_con.Snap()

    rowhole_offset_relation_con = FBConstraintRelation( 'rowhole_offset_relation' )

    l_rowlock_att_bone_sender = rowhole_offset_relation_con.SetAsSource( l_rowlock_att_bone )
    l_rowlock_att_bone_sender.UseGlobalTransforms = False 
    rowhole_offset_relation_con.SetBoxPosition( l_rowlock_att_bone_sender, 0, 0 )
    
    vector_to_number_box = rowhole_offset_relation_con.CreateFunctionBox( 'Converters', 'Vector to Number' )
    rowhole_offset_relation_con.SetBoxPosition( vector_to_number_box, 300, 0 )
    
    number_to_vector_box = rowhole_offset_relation_con.CreateFunctionBox( 'Converters', 'Number to Vector' )
    number_to_vector_box_x = RS.Utils.Scene.FindAnimationNode( number_to_vector_box.AnimationNodeInGet(),'X' )
    number_to_vector_box_x.WriteData( [0] )
    number_to_vector_box_y = RS.Utils.Scene.FindAnimationNode( number_to_vector_box.AnimationNodeInGet(),'Y' )
    number_to_vector_box_y.WriteData( [0] )    
    rowhole_offset_relation_con.SetBoxPosition( number_to_vector_box, 600, 0 )
    
    l_rowlock_att_bone_receiver = rowhole_offset_relation_con.ConstrainObject( l_rowlock_att_bone )
    l_rowlock_att_bone_receiver.UseGlobalTransforms = False 
    rowhole_offset_relation_con.SetBoxPosition( l_rowlock_att_bone_receiver, 900, 0 )

    RS.Utils.Scene.ConnectBox( l_rowlock_att_bone_sender, 'Lcl Rotation', vector_to_number_box, 'V' )
    RS.Utils.Scene.ConnectBox( vector_to_number_box, 'Z', number_to_vector_box, 'Z' )
    RS.Utils.Scene.ConnectBox( number_to_vector_box, 'Result', l_rowlock_att_bone_receiver, 'Lcl Rotation' )



    r_rowlock_att_bone_sender = rowhole_offset_relation_con.SetAsSource( r_rowlock_att_bone )
    r_rowlock_att_bone_sender.UseGlobalTransforms = False 
    rowhole_offset_relation_con.SetBoxPosition( r_rowlock_att_bone_sender, 0, 300 )

    vector_to_number_box_2 = rowhole_offset_relation_con.CreateFunctionBox('Converters', 'Vector to Number')
    rowhole_offset_relation_con.SetBoxPosition( vector_to_number_box_2, 300, 300 )

    number_to_vector_box_2 = rowhole_offset_relation_con.CreateFunctionBox( 'Converters', 'Number to Vector' )
    number_to_vector_box_2_x = RS.Utils.Scene.FindAnimationNode( number_to_vector_box_2.AnimationNodeInGet(),'X' )
    number_to_vector_box_2_x.WriteData( [0] )
    number_to_vector_box_2_y = RS.Utils.Scene.FindAnimationNode( number_to_vector_box_2.AnimationNodeInGet(),'Y' )
    number_to_vector_box_2_y.WriteData( [0] )   
    rowhole_offset_relation_con.SetBoxPosition( number_to_vector_box_2, 600, 300 )
    
    r_rowlock_att_bone_receiver = rowhole_offset_relation_con.ConstrainObject( r_rowlock_att_bone )
    r_rowlock_att_bone_receiver.UseGlobalTransforms = False 
    rowhole_offset_relation_con.SetBoxPosition( r_rowlock_att_bone_receiver, 900, 300 )

    RS.Utils.Scene.ConnectBox( r_rowlock_att_bone_sender, 'Lcl Rotation', vector_to_number_box_2, 'V' )
    RS.Utils.Scene.ConnectBox( vector_to_number_box_2, 'Z', number_to_vector_box_2, 'Z' )
    RS.Utils.Scene.ConnectBox( number_to_vector_box_2, 'Result', r_rowlock_att_bone_receiver, 'Lcl Rotation' )

    rowhole_offset_relation_con.Active = True


    # object lists
    constraint_object_list = [ l_oar_root_parent_con,
                             r_oar_root_parent_con,
                             l_oar_master_parent_con,
                             r_oar_master_parent_con,
                             l_oar_chain_ik_con,
                             r_oar_chain_ik_con,
                             l_rowhole_aim_con,
                             r_rowhole_aim_con,
                             rowhole_offset_relation_con
                             ]

    oar_rig_object_list = [ rowboat_mover_marker,
                   l_oar_ik_effector_null,
                   r_oar_ik_effector_null,
                   r_oar_first_joint_marker,
                   r_oar_last_joint_marker,
                   r_oar_ik_polevector_marker,
                   r_oar_copier_null,
                   l_oar_first_joint_marker,
                   l_oar_last_joint_marker,
                   l_oar_ik_polevector_marker,
                   l_oar_copier_null
                   ]


    # organise constraints   
    oar_rowboat_constraint_folder = FBFolder( "rowboat_custom_rig_constraints", constraint_object_list[0] )
    for i in range( len( constraint_object_list ) ):
        if i != 0:
            oar_rowboat_constraint_folder.ConnectSrc( constraint_object_list[i] )


    # add an rowboat_oar_rig custom property to all new objects
    for rig in oar_rig_object_list:
        tag = rig.PropertyCreate( 'custom_techart_rig', FBPropertyType.kFBPT_charptr, "", False, True, None )
        tag.Data = 'rowboat_oar_rig'
    
    for constraint in constraint_object_list:
        tag = constraint.PropertyCreate( 'custom_techart_rig', FBPropertyType.kFBPT_charptr, "", False, True, None )
        tag.Data = 'rowboat_oar_rig'
            
    tag = oar_rowboat_constraint_folder.PropertyCreate( 'custom_techart_rig', FBPropertyType.kFBPT_charptr, "", False, True, None )
    tag.Data = 'rowboat_oar_rig'