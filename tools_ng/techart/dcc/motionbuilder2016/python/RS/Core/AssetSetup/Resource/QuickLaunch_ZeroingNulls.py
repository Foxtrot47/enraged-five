"""
Scales down the nulls to zero
"""
from RS.Core.AssetSetup import Animals_2


def Run():
    """
    Scales down the nulls to zero
    """
    # temporary quick setup for zeroing null scaling
    modelSetup = Animals_2.ModelSetup()
    characterize = modelSetup.ScaleDownNulls()