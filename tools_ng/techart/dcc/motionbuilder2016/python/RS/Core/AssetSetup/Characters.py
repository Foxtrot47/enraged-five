from pyfbsdk import *

from PySide import QtCore, QtGui

import os

import xml.dom.minidom as minidom

import RS.Core.AssetSetup.Lib as ass
import RS.Globals as glo
import RS.Utils.Creation as cre
import RS.Core.Reference.SceneConfigXML as xml
import RS.Core.Face.RigUISetup as lat
import RS.Core.Face.ConnectRigInABox as concs
import RS.Core.Face.ConnectRigInABoxAmbient as conig
import RS.Core.Face.UFC_Setup as ufcsetup
import RS.Core.Character.IngameCharacterIKControls as ikcon
import RS.Core.Character.Setup.HighHeel as heel
from RS.Utils import Path
import RS.Utils.Scene as Scene
from RS.Utils.Scene import Character
from RS import ProjectData
from RS import Config, Perforce
from RS.Core.AssetSetup import ExpressionSolver as solv


class CharacterSolver():
    def __init__( self ):
        # mb character solver
        self.CharacterSolver = 2


def rs_IK_ConstraintsFolder():
    # creates the constraints folder
    constraintsFolder = None
    for folder in glo.gFolders:
        if folder.Name == 'Constraints 1':
            constraintsFolder = folder
    if constraintsFolder == None:
        placeholderConst = FBConstraintRelation('Remove_Me')
        constraintsFolder = FBFolder("Constraints 1", placeholderConst)
        tag = constraintsFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        tag.Data = "rs_Folders"
        FBSystem().Scene.Evaluate()
        placeholderConst.FBDelete()
    return constraintsFolder


def rs_AssetSetupCharToyboxMoverControl():
    # setup mover and toybox relation - for use in rs_CSCharacterSetup & rs_IGCharacterSetup
    dummy = ass.rs_AssetSetupVariables()[0]
    mover = ass.rs_AssetSetupVariables()[1]
    geo = ass.rs_AssetSetupVariables()[2]
    skelRoot = mover.Children[0]

    toyBoxMarker = FBCreateObject("Browsing/Templates/Elements", "Marker", "ToyBox Marker")
    toyBoxMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
    toyBoxMarker.Size = 500
    toyBoxMarker.Color = FBColor(1, 0, 1)
    toyBoxMarker.Parent = dummy
    toyBoxMarker.Translation.Data = mover.Translation.Data
    toyBoxMarker.Rotation.Data = mover.Rotation.Data
    tag = toyBoxMarker.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    tag.Data = "rs_Contacts"

    mover.Selected = True
    mover.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    mover.Selected = False
    mover.PropertyList.Find("Visibility").Data = True
    moverMarker = FBModelMarker("mover_Control")
    moverMarker.Show = True
    moverMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
    moverMarker.PropertyList.Find("Color RGB").Data = FBColor(0,1,0)
    vector = FBVector3d()
    mover.GetVector(vector, FBModelTransformationType.kModelRotation, True)
    moverMarker.SetVector(vector, FBModelTransformationType.kModelRotation, True)
    mover.GetVector(vector, FBModelTransformationType.kModelTranslation, True)
    moverMarker.SetVector(vector, FBModelTransformationType.kModelTranslation, True)
    FBSystem().Scene.Evaluate()
    moverMarker.Parent = skelRoot

    relationConst = FBConstraintRelation('mover_toybox_Control')
    moverMarkerSender = relationConst.SetAsSource(moverMarker)
    relationConst.SetBoxPosition(moverMarkerSender, 0, 0)
    toyBoxMarkerReceive = relationConst.ConstrainObject(toyBoxMarker)
    relationConst.SetBoxPosition(toyBoxMarkerReceive, 300, 0)
    toyBoxMarkerSender = relationConst.SetAsSource(toyBoxMarker)
    relationConst.SetBoxPosition(toyBoxMarkerSender, 0, 150)
    toyBoxMarkerSender.UseGlobalTransforms = False
    moverReceive = relationConst.ConstrainObject(mover)
    relationConst.SetBoxPosition(moverReceive, 350, 150)
    moverReceive.UseGlobalTransforms = False
    Scene.ConnectBox(moverMarkerSender, 'Translation', toyBoxMarkerReceive, 'Translation')
    Scene.ConnectBox(moverMarkerSender, 'Rotation', toyBoxMarkerReceive, 'Rotation')
    Scene.ConnectBox(toyBoxMarkerSender, 'Lcl Translation', moverReceive, 'Lcl Translation')
    Scene.ConnectBox(toyBoxMarkerSender, 'Lcl Rotation', moverReceive, 'Lcl Rotation')
    relationConst.Active = False
    return relationConst


def rs_AssetSetupFacingDirConstraint():
    if not ProjectData.data.GetConfig("CharSetupFacingDirection"):
        # project does not need this setup
        return

    # constraint OH_UpperFixup and OH_IndependentMover to OH_FacingDir
    ikFacingDirection = Scene.FindModelByName('OH_FacingDirection')
    ohUpperFixupDirection = Scene.FindModelByName('OH_UpperFixupDirection')
    ohIndependentMoverDirection = Scene.FindModelByName('OH_IndependentMoverDirection')

    if ikFacingDirection and ohUpperFixupDirection and ohIndependentMoverDirection:

        upperFixupConstraint = FBConstraintManager().TypeCreateConstraint(10)
        upperFixupConstraint.Name = "OH_UpperFixup_Constraint"
        constFolder = rs_IK_ConstraintsFolder()
        constFolder.Items.append(upperFixupConstraint)
        upperFixupConstraint.ReferenceAdd (0,ohUpperFixupDirection)
        upperFixupConstraint.ReferenceAdd (1,ikFacingDirection)
        upperFixupConstraint.Snap()
        independentMoverConstraint = FBConstraintManager().TypeCreateConstraint(10)
        independentMoverConstraint.Name = "OH_IndependentMover_Constraint"
        constFolder.Items.append(independentMoverConstraint)
        independentMoverConstraint.ReferenceAdd (0,ohIndependentMoverDirection)
        independentMoverConstraint.ReferenceAdd (1,ikFacingDirection)
        independentMoverConstraint.Snap()


def rs_PrepJointLooperCharacterSetup(pModel, pX, pY, pZ,pColor, pBool):
    # prep joints loop - for use in rs_PrepJointsCharacterSetup
    vector = FBVector3d()
    colorValue = pModel.PropertyList.Find('Color RGB')
    pModel.SetVector(FBVector3d(pX, pY, pZ), FBModelTransformationType.kModelRotation, pBool)
    FBSystem().Scene.Evaluate()
    colorValue.Data = pColor


def rs_SpinePrepJointLooperCharacterSetup(pModel, pColor, pBool):
    if not ProjectData.data.GetConfig("CharSetupPrepSpine"):
        # project does not need this setup
        return
    # prep spine joints loop
    vector = FBVector3d()
    colorValue = pModel.PropertyList.Find('Color RGB')
    FBSystem().Scene.Evaluate()
    colorValue.Data = pColor


def rs_PrepFeetJointsCharacterSetup(pModel, pCSSetup=True):
    # prep joints - for use in rs_AssetSetupCSCharacters
    lThighRot = Scene.FindModelByName(glo.gSkelArray[46]).PropertyList.Find("Lcl Rotation").Data
    rThighRot = Scene.FindModelByName(glo.gSkelArray[47]).PropertyList.Find("Lcl Rotation").Data
    lCalfRot = Scene.FindModelByName(glo.gSkelArray[49]).PropertyList.Find("Lcl Rotation").Data
    rCalfRot = Scene.FindModelByName(glo.gSkelArray[53]).PropertyList.Find("Lcl Rotation").Data
    lThighPlusCalf = lThighRot[2] + lCalfRot[2]
    rThighPlusCalf = rThighRot[2] + rCalfRot[2]

    lFootRot = Scene.FindModelByName(glo.gSkelArray[50]).PropertyList.Find("Lcl Rotation").Data
    rFootRot = Scene.FindModelByName(glo.gSkelArray[54]).PropertyList.Find("Lcl Rotation").Data
    if pModel.Name == glo.gSkelArray[50]:
        if lThighPlusCalf > 0:
            rs_PrepJointLooperCharacterSetup(pModel, 0,0,(lFootRot[2] - lThighPlusCalf), glo.gGreen, False)
            FBSystem().Scene.Evaluate()
        else:
            rs_PrepJointLooperCharacterSetup(pModel, 0,0,(lFootRot[2] + lThighPlusCalf), glo.gGreen, False)
            FBSystem().Scene.Evaluate()
    if pModel.Name == glo.gSkelArray[54]:
        if rThighPlusCalf > 0:
            rs_PrepJointLooperCharacterSetup(pModel, 0,0,(rFootRot[2] - rThighPlusCalf), glo.gOrange, False)
            FBSystem().Scene.Evaluate()
        else:
            rs_PrepJointLooperCharacterSetup(pModel, 0,0,(rFootRot[2] + rThighPlusCalf), glo.gOrange, False)
            FBSystem().Scene.Evaluate()
    if pModel.Name == glo.gSkelArray[52]:
        colorValue = pModel.PropertyList.Find('Color RGB').Data = glo.gGreen
    if pModel.Name == glo.gSkelArray[56]:
        colorValue = pModel.PropertyList.Find('Color RGB').Data = glo.gOrange

def rs_PrepJointsCharacterSetup(pModel, pFemaleSetup, pCSSetup=True):
    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    sizeValue = pModel.PropertyList.Find('Size')
    if sizeValue:
        sizeValue.Data = 0.85

    if ProjectData.data.GetConfig("CharSetupNeckZeroTrns"):
        # this is to make sure the Neck0 has no translation at all which causes problems to the control rig
        lNeck0 = Scene.FindModelByName(glo.gSkelArray[66])
        if lNeck0:
            lNeck0.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)

    # run the projdata scripts so it picks up the correct project for the setup
    xRot, yRot, zRot, color, useGlobal, size, prepDef = ProjectData.data.PrepJointsCharacterSetup(pModel, pFemaleSetup, fileName, pCSSetup)
    if size is not None:
        sizeValue.Data = size

    if prepDef is 0:
        # joint prep
        rs_PrepJointLooperCharacterSetup(pModel, xRot, yRot, zRot, FBColor(*color), useGlobal)
    elif prepDef is 1:
        # spine prep
        rs_SpinePrepJointLooperCharacterSetup(pModel, FBColor(*color), useGlobal)
    elif prepDef is 2:
        # color prep
        pModel.PropertyList.Find('Color RGB').Data = FBColor(*color)

    FBSystem().Scene.Evaluate()


def rs_FootHandContactCharacterSetup(pCharacter):
    #Foot/hand Contact Setup - for use in rs_AssetSetupCSCharacters
    lCalf = Scene.FindModelByName(glo.gSkelArray[49])
    rCalf = Scene.FindModelByName(glo.gSkelArray[53])

    globePlane = FBModelPlane("Global_Contact")
    globePlane.Show = True
    globePlane.Translation = FBVector3d(-0.2,0,6)
    globePlane.Rotation = FBVector3d(0,-0,0)
    globePlane.Scaling = FBVector3d(0.56,0.39,0.20)

    rHandPlane = FBModelPlane("R_Hand_Contact")
    rHandPlane.Show = True
    rHandPlane.Translation = FBVector3d(-38.99,0,5)
    rHandPlane.Rotation = FBVector3d(0,-0,0)
    rHandPlane.Scaling = FBVector3d(0.15,0.16,0.08)
    lHandPlane = FBModelPlane("L_Hand_Contact")
    lHandPlane.Show = True
    lHandPlane.Translation = FBVector3d(39.18,0,5)
    lHandPlane.Rotation = FBVector3d(0,-0,0)
    lHandPlane.Scaling = FBVector3d(0.15,0.16,0.08)

    rFootPlane = FBModelPlane("R_Foot_Contact")
    rFootPlane.Show = True
    rFootPlane.Translation = FBVector3d(0,0,6.32)
    Scene.AlignTranslation(rFootPlane, Scene.FindModelByName(glo.gSkelArray[53]), True, False, False)
    rFootPlane.Rotation = FBVector3d(0,-0,0)
    rFootPlane.Scaling = FBVector3d(0.08,0.15,0.18)
    lFootPlane = FBModelPlane("L_Foot_Contact")
    lFootPlane.Show = True
    lFootPlane.Translation = FBVector3d(0,0,6.32)
    Scene.AlignTranslation(lFootPlane, Scene.FindModelByName(glo.gSkelArray[49]), True, False, False)
    lFootPlane.Rotation = FBVector3d(0,-0,0)
    lFootPlane.Scaling = FBVector3d(0.08,0.15,0.18)

    globalPlaneMat = FBMaterial('Global_Contact_Mat')
    globalPlaneMat.Emissive = FBColor(0, 0, 0)
    globalPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    globalPlaneMat.Diffuse = FBColor(0.15, 0.15, 0.15)
    globalPlaneMat.ConnectDst(globePlane)
    lHandPlaneMat = FBMaterial('L_Hand_Contact_Mat')
    lHandPlaneMat.Emissive = FBColor(0, 0, 0)
    lHandPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    lHandPlaneMat.Diffuse = FBColor(0.4,0.7,0)
    lHandPlaneMat.ConnectDst(lHandPlane)
    rHandPlaneMat = FBMaterial('R_Hand_Contact_Mat')
    rHandPlaneMat.Emissive = FBColor(0, 0, 0)
    rHandPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    rHandPlaneMat.Diffuse = FBColor(0.9,0.4,0)
    rHandPlaneMat.ConnectDst(rHandPlane)
    lFootPlaneMat = FBMaterial('L_Foot_Contact_Mat')
    lFootPlaneMat.Emissive = FBColor(0, 0, 0)
    lFootPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    lFootPlaneMat.Diffuse = FBColor(0.4,0.7,0)
    lFootPlaneMat.ConnectDst(lFootPlane)
    rFootPlaneMat = FBMaterial('R_Foot_Contact_Mat')
    rFootPlaneMat.Emissive = FBColor(0, 0, 0)
    rFootPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    rFootPlaneMat.Diffuse = FBColor(0.9,0.4,0)
    rFootPlaneMat.ConnectDst(rFootPlane)

    wireShader = FBShaderManager().CreateShader("WireShader")
    wireShader.PropertyList.Find("Line Width").Data = 3.23
    wireShader.Name = ("Contact_WireShader")

    globePlane.Shaders.append(wireShader)
    rHandPlane.Shaders.append(wireShader)
    lHandPlane.Shaders.append(wireShader)
    rFootPlane.Shaders.append(wireShader)
    lFootPlane.Shaders.append(wireShader)

    globePlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    globePlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    rHandPlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    rHandPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    lHandPlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lHandPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    rFootPlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    rFootPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    lFootPlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lFootPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll

    rHandPlane.Parent = globePlane
    lHandPlane.Parent = globePlane
    rFootPlane.Parent = globePlane
    lFootPlane.Parent = globePlane
    globePlane.Parent = Scene.FindModelByName("Dummy01")

    #re-set rotations for foot planes. Seems to need this in MB14!
    rFootPlane.Rotation = FBVector3d(0,0,0)
    lFootPlane.Rotation = FBVector3d(0,0,0)

def rs_SetHingeDOF():
    # setting Degrees of Freedom for Hinges
    hingeList =[Scene.FindModelByName(glo.gSkelArray[14]),
                Scene.FindModelByName(glo.gSkelArray[15]),
                Scene.FindModelByName(glo.gSkelArray[17]),
                Scene.FindModelByName(glo.gSkelArray[18]),
                Scene.FindModelByName(glo.gSkelArray[20]),
                Scene.FindModelByName(glo.gSkelArray[21]),
                Scene.FindModelByName(glo.gSkelArray[23]),
                Scene.FindModelByName(glo.gSkelArray[24]),
                Scene.FindModelByName(glo.gSkelArray[26]),
                Scene.FindModelByName(glo.gSkelArray[27]),
                Scene.FindModelByName(glo.gSkelArray[32]),
                Scene.FindModelByName(glo.gSkelArray[33]),
                Scene.FindModelByName(glo.gSkelArray[35]),
                Scene.FindModelByName(glo.gSkelArray[36]),
                Scene.FindModelByName(glo.gSkelArray[38]),
                Scene.FindModelByName(glo.gSkelArray[39]),
                Scene.FindModelByName(glo.gSkelArray[41]),
                Scene.FindModelByName(glo.gSkelArray[42]),
                Scene.FindModelByName(glo.gSkelArray[44]),
                Scene.FindModelByName(glo.gSkelArray[45]),
                Scene.FindModelByName(glo.gSkelArray[12]),
                Scene.FindModelByName(glo.gSkelArray[30]),
                Scene.FindModelByName(glo.gSkelArray[49]),
                Scene.FindModelByName(glo.gSkelArray[53])]
    for joint in hingeList:
        if joint:
            joint.PropertyList.Find("Enable Rotation DOF").Data = True
            joint.PropertyList.Find("RotationMinX").Data = True
            joint.PropertyList.Find("RotationMinY").Data = True
            joint.PropertyList.Find("RotationMin").Data = FBVector3d(-2,-2,0)
            joint.PropertyList.Find("RotationMaxX").Data = True
            joint.PropertyList.Find("RotationMaxY").Data = True
            joint.PropertyList.Find("RotationMax").Data = FBVector3d(2,2,0)


def rs_CharacterSetupTidy():
    jointList = []
    contactList = []

    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    dummy = Scene.FindModelByName("Dummy01")
    mover = Scene.FindModelByName("mover")
    exportable = Scene.FindModelByName("SKEL_ROOT")
    contactMain = Scene.FindModelByName("Global_Contact")
    geo = Scene.FindModelByName(fileName)
    Scene.GetChildren(contactMain, contactList)
    Scene.GetChildren(exportable, jointList)

    # eyebrow setup url:bugstar:2198726
    facialHairGeoList = []
    glassesList = []
    eyeBrowString = 'mis2_'
    beardString = 'berd_'
    hairString = 'hair_'
    glassesString = 'p_ey'
    for component in glo.Components:
        if eyeBrowString in component.Name.lower():
            facialHairGeoList.append(component)
        if beardString in component.Name.lower():
            facialHairGeoList.append(component)
        if hairString in component.Name.lower():
            facialHairGeoList.append(component)
        if glassesString in component.Name.lower():
            glassesList.append(component)
    if facialHairGeoList:
        lightedShader = FBShaderLighted('facialHair_Lighted')
        lightedShader.Transparency = FBAlphaSource.kFBAlphaSourceMatteAlpha
        for geo in facialHairGeoList:
            if geo.ClassName() == "FBModel":
                lightedShader.Append(geo)
                geo.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    if glassesList:
        glassesShader = FBShaderLighted('glasses_Lighted')
        glassesShader.Transparency = FBAlphaSource.kFBAlphaSourceAccurateAlpha 
        for geo in glassesList:
            if geo.ClassName() == "FBModel":
                glassesShader.Append(geo)
                geo.ShadingMode = FBModelShadingMode.kFBModelShadingAll

    # folders
    materials = FBSystem().Scene.Materials
    materialFolderStr = "Materials"
    textures = FBSystem().Scene.Textures
    texturesFolderStr = "Textures"
    constraints = FBSystem().Scene.Constraints
    constraintFolderStr = "Constraints"
    shaders = FBSystem().Scene.Shaders
    shaderFolderStr = "Shaders"
    videoClips = FBSystem().Scene.VideoClips
    if len(videoClips) > 0:
        videoFolder = FBFolder ("Videos", videoClips[0])
        for video in videoClips:
            videoFolder.ConnectSrc(video)
    cre.rs_CreateFolderAddComponents(materials, materialFolderStr)
    cre.rs_CreateFolderAddComponents(textures, texturesFolderStr)
    cre.rs_CreateConstraintFolderAddComponents(constraints, constraintFolderStr)
    cre.rs_CreateFolderAddComponents(shaders, shaderFolderStr)

    # default materials
    for material in materials:
        if material:
            if "_Contact" in material.Name:
                None
            else:
                material.Ambient = FBColor(0,0,0)
                material.Diffuse = FBColor(1,1,1)

    # parent facial
    faceControls = Scene.FindModelByName("faceControls_OFF")
    faceScaleOffset = Scene.FindModelByName("RECT_ScaleOffset")
    faceScaleOffsetCtrl = Scene.FindModelByName("ScaleOffset")
    faceAttrGUI = Scene.FindModelByName("FacialAttrGUI")

    if faceScaleOffset:
        faceScaleOffset.Parent = None
    if faceScaleOffsetCtrl:
        if faceScaleOffsetCtrl.Translation.GetAnimationNode():
            animNodes = faceScaleOffsetCtrl.Translation.GetAnimationNode().Nodes
            for node in animNodes:
                node.FCurve.EditClear()
        faceScaleOffsetCtrl.Translation.Data = FBVector3d(0,0,0)
    if faceAttrGUI and faceControls:
        if faceAttrGUI.Translation.GetAnimationNode():
            if not ProjectData.data.GetConfig("CharSetupFaceGUI"):
                animNodes = faceScaleOffsetCtrl.Translation.GetAnimationNode().Nodes
            else:
                animNodes = faceAttrGUI.Translation.GetAnimationNode().Nodes
            for node in animNodes:
                node.FCurve.EditClear()
        if ProjectData.data.GetConfig("CharSetupFaceTempParent"):
            tempParent = faceControls.Parent
            faceControls.Parent = None
            newPos = faceControls.Translation.Data + FBVector3d(21,6,0)
            faceControls.Parent = tempParent
            faceAttrGUI.Translation.Data = newPos
        else:
            newPos = faceControls.Translation.Data + FBVector3d(21,6,0)
            faceAttrGUI.Translation.Data = newPos

    #facial lists
    faceText = Scene.FindModelByName("TEXT_Normals")
    faceText2 = Scene.FindModelByName("Text001")
    facialTextList = []
    if faceText:
        Scene.GetChildren(faceText, facialTextList)
    if faceText2:
        Scene.GetChildren(faceText2, facialTextList)

    faceFacial = Scene.FindModelByName("Facial_UI")
    faceAttr = Scene.FindModelByName("FacialAttrGUI")
    facial3LateralList = []
    if faceFacial:
        Scene.GetChildren(faceFacial, facial3LateralList)
    if faceControls:
        Scene.GetChildren(faceControls, facial3LateralList)
    if faceAttr:
        Scene.GetChildren(faceAttr, facial3LateralList)

    ambientUI = Scene.FindModelByName("Ambient_UI")
    faceFX = Scene.FindModelByName("FaceFX")
    facialAmbientList = []
    if ambientUI:
        Scene.GetChildren(ambientUI, facialAmbientList)
    if faceFX:
        Scene.GetChildren(faceFX, facialAmbientList)

    # groups
    mainGroup = FBGroup(fileName)
    if len(facial3LateralList) > 0:
        threeLateralGroup = FBGroup("3Lateral")
    else:
        threeLateralGroup = None
    if len(facialAmbientList) > 0:
        ambientFacialGroup = FBGroup("Ambient_Facial")
    else:
        ambientFacialGroup = None
    dummyGroup = FBGroup("Dummy01")
    moverGroup = FBGroup("mover")
    jointGroup = FBGroup("Joints")
    contactsGroup = FBGroup("Contacts")
    phGroup = FBGroup("PH_Nulls")
    heelsGroup = None

    if len(facialTextList) > 0:
        for textItem in facialTextList:
            if textItem:
                tag = textItem.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = "rs_3Lateral"
                textItem.Show = False
                textItem.Pickable = False
    if len(facial3LateralList) > 0:
        for threeLateralItem in facial3LateralList:
            if threeLateralItem:
                tag = threeLateralItem.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = "rs_3Lateral"
                threeLateralGroup.ConnectSrc(threeLateralItem)
    if len(facialAmbientList) > 0:
        for ambientItem in facialAmbientList:
            if ambientItem:
                tag = ambientItem.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = "rs_3Lateral"
                ambientFacialGroup.ConnectSrc(ambientItem)

    dummyGroup.ConnectSrc(dummy)
    moverGroup.ConnectSrc(mover)
    for joint in jointList:
        jointGroup.ConnectSrc(joint)
    for contact in contactList:
        contactsGroup.ConnectSrc(contact)

    PH_List = FBComponentList()
    FBFindObjectsByName( "PH_*", PH_List, False, True )
    for each in PH_List:
        each.PropertyList.Find( 'Look' ).Data = 1
        each.PropertyList.Find( 'Size' ).Data = 2
        phGroup.ConnectSrc( each )

    heelsList = []
    heels = None
    heelProxyGroup = None
    heels = Scene.FindModelByName("RECT_HeelHeight")
    if heels != None:
        heelsGroup = FBGroup("HighHeel_Slider")
        heelsGroup.Show = False
        heelsGroup.Pickable = True
        heelsGroup.Transformable = True
        heelProxyGroup = FBGroup("HighHeel_Proxies")
        heelProxyGroup.Show = False
        heelProxyGroup.Pickable = False
        heelProxyGroup.Transformable = False
        Scene.GetChildren(heels, heelsList)
    if len( heelsList ) > 0:
        for heel in heelsList:
            tag = heel.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = "rs_Contacts"
            heelsGroup.ConnectSrc(heel)

    eoRProxy = Scene.FindModelByName("EO_R_Proxy")
    if eoRProxy:
        tag = eoRProxy.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        tag.Data = "rs_Contacts"
        if heelProxyGroup != None:
            heelProxyGroup.ConnectSrc(eoRProxy)
    eoLProxy = Scene.FindModelByName("EO_L_Proxy")
    if eoLProxy:
        tag = eoLProxy.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        tag.Data = "rs_Contacts"
        if heelProxyGroup != None:
            heelProxyGroup.ConnectSrc(eoLProxy)

    if threeLateralGroup != None:
        mainGroup.ConnectSrc(threeLateralGroup)
    if ambientFacialGroup != None:
        mainGroup.ConnectSrc(ambientFacialGroup)
    mainGroup.ConnectSrc(dummyGroup)
    mainGroup.ConnectSrc(moverGroup)
    mainGroup.ConnectSrc(jointGroup)
    mainGroup.ConnectSrc(contactsGroup)
    jointGroup.ConnectSrc(phGroup)
    if heelsGroup:
        mainGroup.ConnectSrc(heelsGroup)
        mainGroup.ConnectSrc(heelProxyGroup)
    for group in glo.gGroups:
        if group.Name == 'GeoVariations':
            mainGroup.ConnectSrc(group)
            group.Transformable = False
            group.Pickable = False
            group.Show = True

    if threeLateralGroup != None:
        threeLateralGroup.Transformable = True
        threeLateralGroup.Pickable = True
        threeLateralGroup.Show = False
    if ambientFacialGroup != None:
        ambientFacialGroup.Transformable = True
        ambientFacialGroup.Pickable = True
        ambientFacialGroup.Show = False
    dummyGroup.Show = False
    dummyGroup.Pickable = False
    dummyGroup.Transformable = False
    jointGroup.Pickable = False
    jointGroup.Transformable = False
    contactsGroup.Pickable = False
    contactsGroup.Transformable = False
    moverGroup.Show = False
    moverGroup.Pickable = False
    moverGroup.Transformable = False

    #safe bone group
    safeBoneGroup = FBGroup("SafeBones")
    tag = safeBoneGroup .PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    tag.Data = "rs_Groups"

    boneStringList = ProjectData.data.SafeBoneList()
    for boneString in boneStringList:
        boneModel = Scene.FindModelByName(boneString)
        if boneModel:
            safeBoneGroup.ConnectSrc(boneModel)

    # expressions group
    expressionsList = []
    for component in glo.gComponents:
        if component.Name.startswith('MH_'):
            expressionsList.append(component)
        if component.Name.startswith('RB_'):
            expressionsList.append(component)
        if component.Name.startswith('SM_'):
            expressionsList.append(component)
        if component.Name.startswith('SPR_'):
            expressionsList.append(component)
    expressionsGroup = FBGroup("Expressions")
    tag = expressionsGroup .PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    tag.Data = "rs_Groups"
    for expression in expressionsList:
        if expression:
            expressionsGroup.ConnectSrc(expression)
        else:
            None

    # delete setup items
    ass.rs_AssetSetupDeleteItems()


def rs_CharacterExtension(character):
    # character extensions
    boneList = ProjectData.data.CharacterExtensionBoneStringList()

    placeholder = FBCharacterExtension('Remove_Me')
    folder = FBFolder("character_extensions", placeholder)
    rsProperty = folder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    rsProperty.Data = "rs_Folders"

    characterExtensionList = []
    for bone in boneList:
        model = Scene.FindModelByName(bone)
        if model:
            characterExt = FBCharacterExtension(model.Name)
            if model.Name == 'mover':
                characterExt.Label = 'mover_prop_extension'
            else:
                characterExt.Label = model.Name
            character.AddCharacterExtension(characterExt)
            FBConnect(model, characterExt)
            characterExtensionList.append(characterExt)
            folder.Items.append(characterExt)
            rsProperty = characterExt.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            rsProperty.Data = "rs_Extensions"
    
    # Add an extension for extra bones if present
    extraDict = {}
    extraString = "xtra_"
    for component in glo.Components:
        componentName = component.Name.lower()
        if componentName.startswith(extraString):
            if isinstance(component, FBModelNull):
                # Our extension name will be whatever comes after "xtra_"
                extensionName = componentName.split(extraString)[1].split("_")[0]
                print extensionName
                if extensionName not in extraDict.keys():
                    extraDict[extensionName] = [component]
                if component not in extraDict[extensionName]:
                    extraDict[extensionName].append(component)
    
    if len(extraDict.keys()) > 0:
        for extensionName in extraDict.keys():
            extraExtension = FBCharacterExtension(extensionName)
            character.AddCharacterExtension(extraExtension)
            characterExtensionList.append(extraExtension)
            folder.Items.append(extraExtension)
            rsProperty = extraExtension.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            rsProperty.Data = "rs_Extensions"
            
            for extensionModel in extraDict[extensionName]:
                FBConnect(extensionModel, extraExtension)

    for folder in glo.gFolders:
        if folder.Name == 'Constraints 1':
            for item in folder.Items:
                if isinstance(item, FBCharacter):
                    folder.Items.remove(item)
    
    placeholder.FBDelete()
    FBSystem().Scene.Evaluate()
    return characterExtensionList


def rs_FixShoulders():
    # Dave Bailey's Traps Shoulder Fix Def url:bugstar:754532
    trapRSupport = Scene.FindModelByName("Trap_R_Support")
    upperArmR    = Scene.FindModelByName("SKEL_R_UpperArm")
    trapLSupport = Scene.FindModelByName("Trap_L_Support")
    upperArmL    = Scene.FindModelByName("SKEL_L_UpperArm")

    if trapRSupport and upperArmR:
        constraintManager = FBConstraintManager()
        aimConstraint = constraintManager.TypeCreateConstraint(0)
        aimConstraint.Name = "Trap_R_Support_Aim"
        aimConstraint.ReferenceAdd(0,trapRSupport)
        aimConstraint.ReferenceAdd(1,upperArmR)
        aimConstraint.PropertyList.Find("AimVector").Data = FBVector3d(1,0,0)
        glo.gScene.Evaluate()
        aimConstraint.PropertyList.Find("UpVector").Data  = FBVector3d(0,0,1)
        glo.gScene.Evaluate()
        aimConstraint.PropertyList.Find("WorldUpType").Data = 0
        glo.gScene.Evaluate()
        aimConstraint.PropertyList.Find("WorldUpVector").Data = FBVector3d(0,1,0)
        glo.gScene.Evaluate()
        aimConstraint.Active = True
        constFolder = rs_IK_ConstraintsFolder()
        constFolder.Items.append(aimConstraint)

    if trapLSupport and upperArmL:
        constraintManager = FBConstraintManager()
        aimConstraint = constraintManager.TypeCreateConstraint(0)
        aimConstraint.Name = "Trap_L_Support_Aim"
        aimConstraint.ReferenceAdd(0,trapLSupport)
        aimConstraint.ReferenceAdd(1,upperArmL)
        aimConstraint.PropertyList.Find("AimVector").Data = FBVector3d(1,0,0)
        glo.gScene.Evaluate()
        aimConstraint.PropertyList.Find("UpVector").Data  = FBVector3d(0,0,-1)
        glo.gScene.Evaluate()
        aimConstraint.PropertyList.Find("WorldUpType").Data = 0
        glo.gScene.Evaluate()
        aimConstraint.PropertyList.Find("WorldUpVector").Data = FBVector3d(0,1,0)
        glo.gScene.Evaluate()
        aimConstraint.Active = True
        constFolder = rs_IK_ConstraintsFolder()
        constFolder.Items.append(aimConstraint)


class BoneRotation:
    # Save rotation and name of the bones - This function is used to store the Max pose
    name = ''
    rotation = []

def rs_SaveBoneRotation(pBone,pBoneRotationList):
    tempBone = BoneRotation()
    tempBone.name=''
    tempBone.rotation=[]
    tempBone.name = pBone.Name
    tempBone.rotation.append (pBone.Rotation[0])
    tempBone.rotation.append (pBone.Rotation[1])
    tempBone.rotation.append (pBone.Rotation[2])
    pBoneRotationList.append (tempBone)


def rs_LoadBoneRotation(pBoneList):
    # Load rotation of bones - This function is used to Load the stored max rotation of each bone
    # back into the skeleton on MaxFigPose take
    for bone in pBoneList:
        ModelFound = FBFindModelByLabelName(bone.name)
        ModelFound.Rotation.SetAnimated(True)
        CreateAnimation(ModelFound.Rotation.GetAnimationNode(),bone.rotation)


def CreateAnimation(pNode, pRotation):
    if pNode.Nodes:
        lXFCurve = pNode.Nodes[0].FCurve
        lXFCurve.KeyAdd(FBTime.Zero, pRotation[0])
        lYFCurve = pNode.Nodes[1].FCurve
        lYFCurve.KeyAdd(FBTime.Zero, pRotation[1])
        lZFCurve = pNode.Nodes[2].FCurve
        lZFCurve.KeyAdd(FBTime.Zero, pRotation[2])


def rs_PrepSetupCharacters(pCSSetup=True):
    # Prepping Character Setup
    skelList = []
    lodList = []
    lodParent = None
    lodList2 = []
    lodParent2 = None
    boneRotationList = []
    sceneList = FBModelList()
    FBGetSelectedModels (sceneList, None, False)
    FBGetSelectedModels (sceneList, None, True)
    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    dummy = ass.rs_AssetSetupVariables()[0]
    mover = ass.rs_AssetSetupVariables()[1]
    geo = ass.rs_AssetSetupVariables()[2]
    skelRoot = mover.Children[0]

    # dummy property
    typeTag = dummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    typeTag.Data = "Character"

    # delete all animation
    FBSystem().CurrentTake.ClearAllProperties(False)

    # delete lods
    for sceneItem in sceneList:
        if '-l2' in sceneItem.Name.lower():
            lodParent2 = sceneItem
            break
    for sceneItem in sceneList:
        if '-l' in sceneItem.Name.lower() and '-l2' not in sceneItem.Name.lower():
            lodParent = sceneItem
            break
    for sceneItem in sceneList:
        if 'misc_' in sceneItem.Name.lower():
            sceneItem.Parent = geo
    if lodParent != None:
        Scene.GetChildren(lodParent, lodList)
    if lodParent2 != None:
        Scene.GetChildren(lodParent2, lodList2)
        for lLOD in lodList:
            lLOD.FBDelete()
        for lLOD2 in lodList2:
            lLOD2.FBDelete()

    # set pre-rotation on dummy
    dummy.PropertyList.Find("Pre Rotation").Data = FBVector3d(0,0,0)

    # adjust rotation of dummy and mover
    FBDisconnect(mover, dummy)
    dummy.Rotation = FBVector3d(-90,0,0)
    FBConnect(mover, dummy)
    mover.Rotation = FBVector3d(0,0,0)
    geo.Parent = dummy
    FBSystem().Scene.Evaluate()

    # create takes for max and t-poses
    FBSystem().Scene.Takes[0].Name = 'MaxFigPose'
    FBSystem().Scene.Takes[0].CopyTake ('MBStancePose')
    FBSystem().CurrentTake = FBSystem().Scene.Takes[1]

    Scene.GetChildren(mover, skelList, "", False)
    pFemaleSetup = None
    pFemaleSetup = Scene.FindModelByName("SPR_L_Breast")

    # setup feet
    for skel in skelList:
        rs_PrepFeetJointsCharacterSetup(skel, pCSSetup)
        FBSystem().Scene.Evaluate()

    # t-pose character
    for skel in skelList:
        # save max pose, before t-posing. this will be applied to the maxfig pose take
        rs_SaveBoneRotation(skel, boneRotationList)
        # apply rotation to the bones for T-Pose
        rs_PrepJointsCharacterSetup(skel, pFemaleSetup, pCSSetup)
        FBSystem().Scene.Evaluate()

    # create character
    character = FBCharacter(fileName)

    # New for MB14 - it defaults to 'none', so you have to force it to select the char to be active
    character.Selected = True

    # foot and hand contacts setup
    rs_FootHandContactCharacterSetup(character)
    FBSystem().Scene.Evaluate()

    # assign joints
    Scene.AssignJoints(character)

    # set solver type
    SolverClass = CharacterSolver()
    character.PropertyList.Find("CharacterSolverSelector").Data = SolverClass.CharacterSolver

    # set characterize On
    character.SetCharacterizeOn(True)

    # load the pre-saved max pose onto the maxfigpose take
    FBSystem().CurrentTake = FBSystem().Scene.Takes[0]
    character.ActiveInput = False
    rs_LoadBoneRotation(boneRotationList)

    # default the current take to the t-pose
    FBSystem().CurrentTake = FBSystem().Scene.Takes[1]
    character.ActiveInput = True
    FBSystem().Scene.Evaluate()

def ReturnMaxProperties():
    '''
    Gather property data from the max custom properties

    Returns:
        String, String : MAXFILE (path tot he max file)
                         REVISION (revision of the max file used in the fbx)
    '''
    # go through hierarchy and find the max properties
    maxProperty = None
    for component in glo.Components:
        maxProperty = component.PropertyList.Find('UDP3DSMAX')
        if maxProperty:
            break
    if maxProperty == None:
        return None, None

    maxRevisionResult = None
    maxPathResult = None
    dataList = maxProperty.Data.split('\n')
    for data in dataList:
        if 'REVISION' in data:
            maxRevision = data.replace('\r', '')
            maxRevisionResult = maxRevision.split(' ')[-1]
        elif 'filename' in data:
            maxPath = data.replace('\r', '')
            maxPathResult = maxPath.split('=')[-1]
            
    print maxPathResult
    if maxRevisionResult and maxPathResult:
        return  maxRevisionResult, maxPathResult
    else:
        return None, None


def CheckMaxRevision():
    '''
    Compares the current Perforce max file revision with the one used by the resourcer.
    Will notify resourcer if their max asset is out of date.

    Returns:
        String : 'Cancel' if the user has selected the Cancel button.
    '''
    currentMaxRevision, maxPath = ReturnMaxProperties()

    if currentMaxRevision == None and maxPath == None:
        return

    # get latest max Perforce revision
    pathFileState = Perforce.GetFileState(maxPath)
    if isinstance(pathFileState, list):
        return

    headRevision = pathFileState.HeadRevision

    if int(currentMaxRevision) == int(headRevision):
        return
    else:
        result = QtGui.QMessageBox.warning(
                        None,
                        "Warning",
                        "You are using revision #{0} of this asset's max file.\n\nThere is a newer version in perforce #{1}.\n\nHit Cancel to stop the setup process of this asset,\nor OK to continue.".format(int(currentMaxRevision), int(headRevision)),
                        QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel
                                )
        if result != QtGui.QMessageBox.Cancel:
            return
        return 'Cancel'


def OHPelvisSetup():
    if not ProjectData.data.GetConfig("CharSetupPelvisSetup"):
        # project does not need this setup
        return
    # oh_pelvis setup
    ohPelvis = Scene.FindModelByName('OH_Pelvis')
    pelvis = Scene.FindModelByName('SKEL_Pelvis')
    mover = Scene.FindModelByName('mover')
    dummy = Scene.FindModelByName('Dummy01')
    skelRoot = Scene.FindModelByName('SKEL_ROOT')
    if not ohPelvis:
        ohPelvis = FBModelNull('OH_Pelvis')
        skelRootTrns = FBVector3d()
        skelRootRot = FBVector3d()
        skelRoot.GetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
        skelRoot.GetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
        ohPelvis.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
        ohPelvis.SetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
        ohPelvis.Parent = skelRoot
        ohPelvis.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling)

    if not ohPelvis and pelvis and mover and dummy and skelRoot:
        return

    # create nulls for hierarchy
    ohPelvisNull = FBModelNull("OH_Pelvis_Control")
    moverTracerNull = FBModelNull("Mover_Tracer")
    offsetParentNull =FBModelNull("Offset_Parent_Node")
    offsetNull = FBModelNull("Offset_Node")
    rootTracerNull = FBModelNull("Root_Tracer")
    zeroNull = FBModelNull("Zero_Node")

    # parent and position new nulls
    pelvisTrns = FBVector3d()
    pelvisRot = FBVector3d()
    pelvis.GetVector(pelvisTrns, FBModelTransformationType.kModelTranslation, True)
    pelvis.GetVector(pelvisRot, FBModelTransformationType.kModelRotation, True)
    skelRootTrns = FBVector3d()
    skelRootRot = FBVector3d()
    skelRoot.GetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    skelRoot.GetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
    moverTrns = FBVector3d()
    moverRot = FBVector3d()
    mover.GetVector(moverTrns, FBModelTransformationType.kModelTranslation, True)
    mover.GetVector(moverRot, FBModelTransformationType.kModelRotation, True)
    ohPelvisNull.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    ohPelvisNull.SetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
    ohPelvisNull.Parent = dummy
    moverTracerNull.SetVector(moverTrns, FBModelTransformationType.kModelTranslation, True)
    moverTracerNull.SetVector(moverRot, FBModelTransformationType.kModelRotation, True)
    moverTracerNull.Parent = ohPelvisNull
    offsetParentNull.SetVector(moverTrns, FBModelTransformationType.kModelTranslation, True)
    offsetParentNull.SetVector(moverRot, FBModelTransformationType.kModelRotation, True)
    offsetParentNull.Parent = moverTracerNull
    offsetNull.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    offsetNull.SetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
    offsetNull.Parent = offsetParentNull
    rootTracerNull.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    rootTracerNull.SetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
    rootTracerNull.Parent = ohPelvisNull
    zeroNull.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    zeroNull.SetVector(FBVector3d(-90,0,0), FBModelTransformationType.kModelRotation, True)
    zeroNull.Parent = rootTracerNull

    # create constraints
    moverTracerConstraint = FBConstraintManager().TypeCreateConstraint(3)
    moverTracerConstraint.Name = 'moverTracer_parentChild'
    moverTracerConstraint.ReferenceAdd (0, moverTracerNull)
    moverTracerConstraint.ReferenceAdd (1, mover)
    moverTracerConstraint.Snap()

    pelvisOffsetConstraint = FBConstraintManager().TypeCreateConstraint(3)
    pelvisOffsetConstraint.Name = 'pelvisOffset_parentChild'
    pelvisOffsetConstraint.ReferenceAdd (0, offsetNull)
    pelvisOffsetConstraint.ReferenceAdd (1, zeroNull)
    trnsOffsetProperty = pelvisOffsetConstraint.PropertyList.Find('Zero_Node.Offset T')
    rotOffsetProperty = pelvisOffsetConstraint.PropertyList.Find('Zero_Node.Offset R')
    scaleOffsetProperty = pelvisOffsetConstraint.PropertyList.Find('Zero_Node.Offset S')
    if trnsOffsetProperty and rotOffsetProperty and scaleOffsetProperty:
        trnsOffsetProperty.Data = FBVector3d(0,0,0)
        rotOffsetProperty.Data = FBVector3d(0,0,0)
        scaleOffsetProperty.Data = FBVector3d(1,1,1)
    pelvisOffsetConstraint.Snap()
    pelvisOffsetConstraint.Lock = True

    rootTracerConstraint = FBConstraintManager().TypeCreateConstraint(3)
    rootTracerConstraint.Name = 'rootTracer_parentChild'
    rootTracerConstraint.ReferenceAdd (0, rootTracerNull)
    rootTracerConstraint.ReferenceAdd (1, skelRoot)
    rootTracerConstraint.Snap()

    pelvisExportConstraint = FBConstraintRelation('OH_Pelvis_Export')
    pelvisExportSender = pelvisExportConstraint.SetAsSource(offsetNull)
    pelvisExportConstraint.SetBoxPosition(pelvisExportSender, 0, 0)
    pelvisExportSender.UseGlobalTransforms = False
    pelvisExportReceiver = pelvisExportConstraint.ConstrainObject(ohPelvis)
    pelvisExportConstraint.SetBoxPosition(pelvisExportReceiver, 300, 0)
    pelvisExportReceiver.UseGlobalTransforms = False
    Scene.ConnectBox(pelvisExportSender, 'Lcl Translation', pelvisExportReceiver, 'Lcl Translation')
    Scene.ConnectBox(pelvisExportSender, 'Lcl Rotation', pelvisExportReceiver, 'Lcl Rotation')
    pelvisExportConstraint.Active = False

    pelvisLockConstraint = FBConstraintRelation('OH_Pelvis_Lock')
    pelvisLockSender = pelvisLockConstraint.SetAsSource(skelRoot)
    pelvisLockConstraint. SetBoxPosition(pelvisLockSender, 0, 0)
    pelvisLockReceiver = pelvisLockConstraint.ConstrainObject(ohPelvis)
    pelvisLockConstraint.SetBoxPosition(pelvisLockReceiver, 300, 0)
    Scene.ConnectBox(pelvisLockSender, 'Translation', pelvisLockReceiver, 'Translation')
    Scene.ConnectBox(pelvisLockSender, 'Rotation', pelvisLockReceiver, 'Rotation')
    pelvisLockConstraint.Active = True

    ohPelvis.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    ohPelvisNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    moverTracerNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    offsetParentNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    offsetNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    rootTracerNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    zeroNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)

    ohPelvis.Translation.GetAnimationNode().Nodes[0].FCurve.EditClear()
    ohPelvis.Translation.GetAnimationNode().Nodes[1].FCurve.EditClear()
    ohPelvis.Translation.GetAnimationNode().Nodes[2].FCurve.EditClear()
    ohPelvis.Rotation.GetAnimationNode().Nodes[0].FCurve.EditClear()
    ohPelvis.Rotation.GetAnimationNode().Nodes[1].FCurve.EditClear()
    ohPelvis.Rotation.GetAnimationNode().Nodes[2].FCurve.EditClear()

    # add OH Constraints into a folder
    ohConstraintFolder = None
    constraintFolder = None
    for folder in glo.Folders:
        if folder.Name == 'OH_Constraints':
            ohConstraintFolder = folder
        if folder.Name == 'Constraints 1':
            constraintFolder = folder
    if ohConstraintFolder:
        folder.Items.append(moverTracerConstraint)
        folder.Items.append(pelvisOffsetConstraint)
        folder.Items.append(rootTracerConstraint)
        folder.Items.append(pelvisExportConstraint)
        folder.Items.append(pelvisLockConstraint)
        if constraintFolder:
            try:
                constraintFolder.Items.append(ohConstraintFolder)
            except:
                pass
    else:
        placeholder = FBCharacterExtension('Remove_Me')
        ohConstraintFolder = FBFolder("OH_Constraints", placeholder)
        ohConstraintFolder.Items.append(moverTracerConstraint)
        ohConstraintFolder.Items.append(pelvisOffsetConstraint)
        ohConstraintFolder.Items.append(rootTracerConstraint)
        ohConstraintFolder.Items.append(pelvisExportConstraint)
        ohConstraintFolder.Items.append(pelvisLockConstraint)
        placeholder.FBDelete()
        if constraintFolder:
            try:
                constraintFolder.Items.append(ohConstraintFolder)
            except:
                pass


def rs_AssetSetupFacialGroups():
    # Accessing the scene components to view all the groups
    scene = FBSystem().Scene
    allGroups = scene.Groups
    for group in allGroups:

        # Do the 3Lateral
        if group.Name == "3Lateral":
            lateralGUI = Scene.FindModelByName("faceControls_OFF")
            if lateralGUI:

                # Gather Objects
                lCtrls_Brows = FBModelList()
                lCtrls_Eyes = FBModelList()
                lCtrls_Mouth = FBModelList()
                lFrames = FBModelList()
                for item in glo.gBrowCtrls :
                    lName = Scene.FindModelByName( item )
                    if lName :
                        lCtrls_Brows.append( lName )
                for item in glo.gEyeCtrls :
                    lName = Scene.FindModelByName( item )
                    if lName :
                        lCtrls_Eyes.append( lName )
                for item in glo.gMouthCtrls :
                    lName = Scene.FindModelByName( item )
                    if lName :
                        lCtrls_Mouth.append( lName )
                for item in glo.gCAFaceAttrGui :
                    lName = Scene.FindModelByName( item )
                    if lName :
                        lFrames.append( lName )

                # Gather Names
                name3L_ctrls = "3L_Controls"
                name3L_frames = "3L_Frames"
                name3L_brows = "Brows"
                name3L_eyes = "Eyes"
                name3L_mouth = "Mouth"

                # Remove Groups and their members
                for group in allGroups:
                    if group.LongName == name3L_ctrls :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in allGroups:
                    if group.LongName == name3L_frames :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in allGroups:
                    if group.LongName == name3L_brows :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in allGroups:
                    if group.LongName == name3L_eyes :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in allGroups:
                    if group.LongName == name3L_mouth :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                while len(group.Items) != 0 :
                    group.Items.pop()

                # Create Groups
                lGrp_ctrls = FBGroup (name3L_ctrls)
                lGrp_frames = FBGroup (name3L_frames)
                lGrp_brows = FBGroup (name3L_brows)
                lGrp_eyes = FBGroup (name3L_eyes)
                lGrp_mouth = FBGroup (name3L_mouth)

                # Parent Groups
                lGrp_ctrls.Items.append(lGrp_brows)
                lGrp_ctrls.Items.append(lGrp_eyes)
                lGrp_ctrls.Items.append(lGrp_mouth)
                group.Items.append(lGrp_ctrls)
                group.Items.append(lGrp_frames)
                for each in lCtrls_Brows :
                    lGrp_brows.ConnectSrc(each)
                for each in lCtrls_Eyes :
                    lGrp_eyes.ConnectSrc(each)
                for each in lCtrls_Mouth :
                    lGrp_mouth.ConnectSrc(each)
                for each in lFrames :
                    lGrp_frames.ConnectSrc(each)
                lGrp_ctrls.Show = False
                lGrp_ctrls.Pickable = True
                lGrp_ctrls.Transformable = True
                lGrp_frames.Show = False
                lGrp_frames.Pickable = False
                lGrp_frames.Transformable = True
                glo.gScene.Evaluate()

        # Do the Ambient
        if group.Name == "Ambient_Facial":

            ambientGUI = Scene.FindModelByName("Ambient_UI")
            faceFXGUI = Scene.FindModelByName("FaceFX")
            if ambientGUI:

                # Object Containers
                amb_Ctrls = []
                amb_Frames = []

                # Recursively gather Ambient Information
                def getAmbientInfo(currentObj) :
                    if "CTRL" in currentObj.Name :
                        amb_Ctrls.append(currentObj)
                    elif "TEXT" in currentObj.Name :
                        amb_Frames.append(currentObj)
                    elif "RECT" in currentObj.Name :
                        amb_Frames.append(currentObj)
                    elif "Ambient_UI" in currentObj.Name :
                        amb_Frames.append(currentObj)
                    elif "FaceFX" in currentObj.Name :
                        amb_Frames.append(currentObj)
                    else :
                        amb_Ctrls.append(currentObj)

                    # Recurse
                    for child in currentObj.Children:
                        getAmbientInfo(child)
                getAmbientInfo(ambientGUI)
                if faceFXGUI :
                    getAmbientInfo(faceFXGUI)

                # Gather Names
                nameAmb_ctrls = "Amb_Controls"
                nameAmb_frames = "Amb_Frames"

                # Remove Groups
                for group in allGroups:
                    if group.LongName == nameAmb_ctrls :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in allGroups:
                    if group.LongName == nameAmb_frames :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                while len(group.Items) != 0 :
                    group.Items.pop()

                # Create Groups
                lGrp_ctrls = FBGroup (nameAmb_ctrls)
                lGrp_frames = FBGroup (nameAmb_frames)

                # Parent Groups
                group.Items.append(lGrp_ctrls)
                group.Items.append(lGrp_frames)
                for each in amb_Ctrls :
                    lGrp_ctrls.ConnectSrc(each)
                for each in amb_Frames :
                    lGrp_frames.ConnectSrc(each)
                lGrp_ctrls.Show = False
                lGrp_ctrls.Pickable = True
                lGrp_ctrls.Transformable = True
                lGrp_frames.Show = False
                lGrp_frames.Pickable = False
                lGrp_frames.Transformable = True
                glo.gScene.Evaluate()

    # Reset group selection and transformable settings, seems buggy.
    for etc in glo.Groups:
        if etc.Name == "Brows":
            etc.Pickable = False
            etc.Transformable = False
            etc.Pickable = True
            etc.Transformable = True
        if etc.Name == "Eyes":
            etc.Pickable = False
            etc.Transformable = False
            etc.Pickable = True
            etc.Transformable = True
        if etc.Name == "Mouth":
            etc.Pickable = False
            etc.Transformable = False
            etc.Pickable = True
            etc.Transformable = True
        if etc.Name == "3L_Frames":
            etc.Pickable = False
            etc.Transformable = True


def MoverCameraCreate(mover):
    # onyl for ingame setup
    moverCam = FBCamera("MoverCamera")
    moverCamInterest = FBModelNull("MoverCamera_Interest")
    moverCam.Interest = moverCamInterest
    moverCam.Show = True
    moverCamInterest.Show = True
    moverCamConstraint = FBConstraintManager().TypeCreateConstraint(3)
    moverCamConstraint.Name = "CameraMover_Constraint"
    moverCamConstraint.ReferenceAdd (0,moverCam)
    moverCamConstraint.ReferenceAdd (1,mover)
    moverCamConstraint.Snap()
    moverCamInterestConstraint = FBConstraintManager().TypeCreateConstraint(3)
    moverCamInterestConstraint.Name = "CameraMoverInterest_Constraint"
    moverCamInterestConstraint.ReferenceAdd (0,moverCamInterest)
    moverCamInterestConstraint.ReferenceAdd (1,mover)
    moverCamInterestConstraint.Snap()
    moverCam.Translation = FBVector3d(-319.68,100,0)
    vector = FBVector3d()
    mover.GetVector(vector, FBModelTransformationType.kModelTranslation, True)
    moverCamInterest.SetVector(vector, FBModelTransformationType.kModelTranslation, True)
    tag = moverCamInterest.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    tag.Data = "rs_Cameras"
    cameraGroup = FBGroup("Camera")
    cameraGroup.ConnectSrc(moverCam)
    cameraGroup.Show = False
    cameraGroup.Pickable = False
    cameraGroup.Transformable = False
    FBSystem().Scene.Evaluate()
    return cameraGroup


def ShadowPlaneCreate(geo):
    # only for ingame setup
    shadowList = []
    shadowLight = FBLight("ShadowLight")
    shadowLight.Show = True
    shadowLight.LightType = FBLightType.kFBLightTypeInfinite
    shadowLight.Intensity = 15.38
    shadowLight.Translation = FBVector3d(46.04,268.12,19.48)
    shadowLight.Rotation = FBVector3d(29.32, 47.13, 2.46)
    shadowLight.Scaling = FBVector3d(0,0,0)
    additionalLight = FBLight("AdditionalLight1")
    additionalLight.Show = True
    additionalLight.LightType = FBLightType.kFBLightTypeInfinite
    additionalLight.Intensity = 116.92
    additionalLight.Translation = FBVector3d(-3.02, 205.57,-131.06)
    additionalLight.Rotation = FBVector3d(-115.49, 64.44, 105.69)
    additionalLight.Scaling = FBVector3d(0,0,0)
    additionalLight2 = FBLight("AdditionalLight2")
    additionalLight2.Show = True
    additionalLight2.LightType = FBLightType.kFBLightTypeInfinite
    additionalLight2.Intensity = 52.31
    additionalLight2.Translation = FBVector3d(70.11, 218.59, -99.49)
    additionalLight2.Rotation = FBVector3d(-23.38, 38, 112.86)
    additionalLight2.Scaling = FBVector3d(0,0,0)
    additionalLight3 = FBLight("AdditionalLight3")
    additionalLight3.Show = True
    additionalLight3.LightType = FBLightType.kFBLightTypeInfinite
    additionalLight3.Intensity = 89.23
    additionalLight3.Translation = FBVector3d(-7.42, 242.38, -89.97)
    additionalLight3.Rotation = FBVector3d(44.88, 40.25, -92.65)
    additionalLight3.Scaling = FBVector3d(0,0,0)

    shadowShader = FBShaderManager().CreateShader("ShadowLiveShader")
    shadowShader.Name = "Live Shadow"
    shadowShader.ShadowType = FBShadowType.kFBShadowTypeShadowOpaquePlanar
    shadowShader.PropertyList.Find("Shadow Intensity").Data = 80
    shadowShader.Lights.append(shadowLight)

    Scene.GetChildren(geo, shadowList)

    for shadowItem in shadowList:
        shadowShader.Models.append(shadowItem)

    shadowPlane = FBModelPlane("ShadowPlane")
    shadowPlane.Show = True
    shadowPlane.Scaling = FBVector3d(2.85,2.85,2.85)
    shadowPlane.Translation = FBVector3d(0,0,0)
    planeTexture = FBTexture("ShadowPlaneTexture")
    planeTexture.ConnectDst(shadowPlane)
    shadowPlane.Shaders.append(shadowShader)
    shadowPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll

    # custom properties
    tag = shadowPlane .PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    tag.Data = "rs_Contacts"

    # groups
    mainShadowGroup = FBGroup("Shadow")
    lightGroup = FBGroup ("Lights")
    planeGroup = FBGroup ("Plane")
    lightGroup.ConnectSrc(shadowLight)
    lightGroup.ConnectSrc(additionalLight)
    lightGroup.ConnectSrc(additionalLight2)
    lightGroup.ConnectSrc(additionalLight3)
    planeGroup.ConnectSrc(shadowPlane)
    mainShadowGroup.ConnectSrc(lightGroup)
    mainShadowGroup.ConnectSrc(planeGroup)
    mainShadowGroup.Pickable = False
    mainShadowGroup.Transformable = False
    return mainShadowGroup


def CharacterPropertySetup(character):
    character.PropertyList.Find('LeftLegRollMode').Data = True
    character.PropertyList.Find('LeftLegRoll').Data = 100
    character.PropertyList.Find('RightLegRollMode').Data = True
    character.PropertyList.Find('RightLegRoll').Data = 100
    character.PropertyList.Find('LeftForeArmRollMode').Data = True
    character.PropertyList.Find('LeftForeArmRoll').Data = 100
    character.PropertyList.Find('RightForeArmRollMode').Data = True
    character.PropertyList.Find('RightForeArmRoll').Data = 100
    character.PropertyList.Find("Feet Floor Contact").Data = False
    character.PropertyList.Find("Toes Floor Contact").Data = False
    character.PropertyList.Find("Hands Floor Contact").Data = False
    character.PropertyList.Find("Fingers Floor Contact").Data = False
    character.PropertyList.Find("Automatic Finger Base").Data = True
    character.PropertyList.Find("Hands Contact Type").Data = 2
    character.PropertyList.Find("Hips Level Mode").Data = False
    character.PropertyList.Find("Feet Spacing Mode").Data = False
    character.PropertyList.Find("Ankle Height Compensation Mode").Data = False
    character.PropertyList.Find("Match Source").Data = True
    character.PropertyList.Find("Left Hand Thumb Tip").Data = 0.10
    character.PropertyList.Find("Left Hand Index Tip").Data = 0.10
    character.PropertyList.Find("Left Hand Middle Tip").Data = 0.10
    character.PropertyList.Find("Left Hand Ring Tip").Data = 0.10
    character.PropertyList.Find("Left Hand Pinky Tip").Data = 0.10
    character.PropertyList.Find("Left Hand Extra Finger Tip").Data = 0.10
    character.PropertyList.Find("Right Hand Thumb Tip").Data =  0.10
    character.PropertyList.Find("Right Hand Index Tip").Data = 0.10
    character.PropertyList.Find("Right Hand Middle Tip").Data = 0.10
    character.PropertyList.Find("Right Hand Ring Tip").Data = 0.10
    character.PropertyList.Find("Right Hand Pinky Tip").Data = 0.10
    character.PropertyList.Find("Right Hand Extra Finger Tip").Data = 0.10
    character.PropertyList.Find("Automatic Finger Base").Data = True
    character.PropertyList.Find("Hands Contact Type").Data = 2
    FBSystem().Scene.Evaluate()


def detectFaceExpressions(character, skelRoot, devBranch="assets_ng"):
    import xml.etree.ElementTree as elementTree
    from xml.etree import cElementTree as xml
    from RS.Core.Automation.FrameCapture import CapturePaths
    
    exprSetName = None
    dlcBranch = None
    maxProperty = None
    foundExprList = []
    packOrders = {}
    
    fbxPath = glo.Application.FBXFileName.lower()
    capPaths = CapturePaths.FastPath(fbxPath)
    projectRoot = capPaths.projectRoot
    
    # Find DLC Pack for the current file
    for component in glo.Components:
        maxProperty = component.PropertyList.Find('UDP3DSMAX')
        if maxProperty:
            break
    maxPathResult = None
    dataList = maxProperty.Data.split('\n')
    for data in dataList:
        if 'maxfile' in data.lower():
            maxPath = data.replace('\r', '')
            maxPathResult = maxPath.split('=')[-1]
        elif 'filename' in data.lower():
            maxPath = data.replace('\r', '')
            maxPathResult = maxPath.split('=')[-1]
    
    if maxPathResult is not None:
        dlcBranch = maxPathResult.lower().split("\\art")[0]
    
    # Find peds.meta for the DLC pack
    if dlcBranch is None:
        return
    if not "dlc" in dlcBranch:
        exprSetName = character
    else:
        pedMeta = os.path.join(dlcBranch,"build","dev_ng","common","data","peds.meta")
    
        # Sync it
        if Perforce.DoesFileExist(pedMeta) == True:
            Perforce.Sync(pedMeta, force=True)
        
        # Open peds.meta & find entry for the character name
        if os.path.exists(pedMeta):
            tree = elementTree.parse(pedMeta)
            root = tree.getroot()
            for dataManager in root:
                if dataManager.tag == 'InitDatas':
                    for dataSet in dataManager:
                        for item in dataSet:
                            if item.tag == 'Name':
                                if item.text == character:
                                    # Find the expression dictionary
                                    exprSetField = dataSet.find('ExpressionDictionaryName')
                                    if exprSetField is not None:
                                        exprSetName = exprSetField.text
                                        print "EXPRESSION SET NAME: " + exprSetName
    
    if exprSetName is None:
        return
    
    # Search patchpacks, mppacks, then assets_ng for the expression dictionary for the character
    # Trawl base game expressions
    baseExprPath = os.path.join(projectRoot, (devBranch + "\\"))
    baseSearchTerm = os.path.join("anim", "expressions", exprSetName, "Components", "Head_000_R.expr")
    editedBaseTerm = baseSearchTerm.replace("\\","/")
    
    baseArgList = ["-e", "{0}...{1}".format(baseExprPath, editedBaseTerm)]
    baseFiles = Perforce.Run("files", args=baseArgList)
    
    for record in baseFiles.Records:
        try:
            recordText = str(record["depotFile"])
        except:
            recordText = ""
        foundExprList.append(recordText)
    
    # Trawl dlc packs
    dlcPacks = os.path.join("x:\\gta5_dlc", "mpPacks")
    dlcSearchTerm = os.path.join(devBranch, "anim", "expressions", exprSetName, "Components", "Head_000_R.expr")
    editedDlcTerm = dlcSearchTerm.replace("\\","/")
    
    dlcFiles = ["-e", "{0}...{1}".format(dlcPacks, editedDlcTerm)]
    dlcFiles = Perforce.Run("files", args=dlcFiles)
    
    for record in dlcFiles.Records:
        try:
            recordText = str(record["depotFile"])
        except:
            recordText = ""
        foundExprList.append(recordText)
    
    # Find the pack order for the found paths
    for exprItem in foundExprList:
        if "gta5_dlc" in exprItem:
            dlcBranch = exprItem.split(devBranch)[0].split("mpPacks")[1]
            dlcBranch = dlcBranch.replace("/","")
            buildXml = os.path.join(dlcPacks, dlcBranch, "build", "dev_ng", "setup2.xml")
            Perforce.Sync(buildXml, force=True)
            if os.path.exists(buildXml):
                tree = elementTree.parse(buildXml)
                root = tree.getroot()
                for dataManager in root:
                    if dataManager.tag == 'order':
                        packOrders[exprItem] = dataManager.get('value')
        else:
            packOrders[exprItem] = 1
    
    # compare the pack order
    exprValue = 0
    usedExpr = None
    for item in packOrders:
        if packOrders[item] > exprValue:
            exprValue = packOrders[item]
            usedExpr = item
    
    if usedExpr is None:
        return
    
    # Create properties on the skelroot
    exprNameProp = skelRoot.PropertyCreate('facialExprName', FBPropertyType.kFBPT_charptr, "", False, True, None)
    exprNameProp.Data = exprSetName
    exprPathProp = skelRoot.PropertyCreate('facialExprath', FBPropertyType.kFBPT_charptr, "", False, True, None)
    exprPathProp.Data = str(usedExpr)


def IGAndCSFinalSetups(character):
    '''
    Start Timeline at Zero
    Setup Facial Groups
    Rotation Order for Upper Arms
    Setup Finger Limits
    Plot Animation to Skeleton
    Master Null Creation
    Snap and Play on Frames
    Scale Down Nulls
    Set Hair Scale to 1
    Unparent Face Controls
    Set Default Material Properties
    Character Property Setup
    Setup Facial Groups
    '''
    # start timeline at 0
    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    # setup facial groups
    rs_AssetSetupFacialGroups()

    # setup facial groups
    if ProjectData.data.GetConfig("CharSetupUFCSetup"):
        ufcsetup.setupUFCFacialGroups()

    # Change the rotation order of the Upper Arms
    if ProjectData.data.GetConfig("CharSetupArmRotation"):
        lBoneNames = ('SKEL_R_UpperArm', 'SKEL_L_UpperArm')
        lBoneObjects = []
        for lBoneName in lBoneNames:
            lObj = FBFindModelByLabelName(lBoneName)
            if lObj: lBoneObjects.append( lObj )
        for lBone in lBoneObjects:
            lBone.PropertyList.Find( 'RotationActive' ).Data = True
            lBone.PropertyList.Find( 'RotationOrder' ).Data = 3
        FBSystem().Scene.Evaluate()

    # setup finger limits
    rs_SetHingeDOF()

    # plot animation to the skeleton
    if ProjectData.data.GetConfig("CharSetupPlot"):
        plotWhere = 'skeleton'
        if plotWhere == 'skeleton':
            plotWhere = FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton
        elif plotWhere == 'rig':
            plotWhere = FBCharacterPlotWhere.kFBCharacterPlotOnControlRig
        else:
            plotWhere = FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton
        plotOptions = FBPlotOptions()
        plotOptions.PlotAllTakes = False
        plotOptions.PlotOnFrame = True
        plotOptions.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
        plotOptions.UseConstantKeyReducer = False
        plotOptions.ConstantKeyReducerKeepOneKey = False
        plotOptions.PlotTranslationOnRootOnly = True
        for char in FBSystem().Scene.Characters:
            char.PlotAnimation(plotWhere, plotOptions)

    # create a master null and parent everything under it
    if ProjectData.data.GetConfig("CharSetupMasterDummy"):
        fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        lSceneParentNull = cre.rs_CreateNullElement(fileName,0,0,0)
        lSceneRootNodes = FBComponentList()
        for model in FBSystem().Scene.RootModel.Children:
            lSceneRootNodes.append (model)
        for model in lSceneRootNodes:
            if model.Name != lSceneParentNull.Name:
                model.Parent = lSceneParentNull
        FBSystem().Scene.Evaluate()

    # snap and play on frames
    FBPlayerControl().SnapMode = FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames

    # scale down nulls
    ass.ScaleDownNulls()

    # set hair scale to 1
    lHairScale = Scene.FindModelByName("HairScale")
    if lHairScale:
        lHairScale.Translation.Data = FBVector3d(0, -1, 0)

    # adjust default materials
    for iMat in glo.gMaterials:
        if iMat:
            if "_Contact" in iMat.Name:
                None
            else:
                iMat.Ambient = FBColor(1,1,1)

    # sets up character properties
    CharacterPropertySetup(character)

    # create gesture safe area
    if ProjectData.data.GetConfig("CharSetupGestureSafeArea"):
        gestureSafe = FBCreateObject( "Browsing/Templates/Elements/Primitives", "Cylinder", "Cylinder" )
        gestureSafe.Name =("GestureSafeArea")
        gestureSafe.Show = True
        gestureSafe.Translation = FBVector3d(1.87,100,91.19)
        gestureSafe.Rotation = FBVector3d(0,0,180)
        gestureSafe.Scaling = FBVector3d(16,10,16)
        gestureSafe.Selected = True
        gestureSafe.ShadingMode = FBModelShadingMode.kFBModelShadingWire
        moverModel = Scene.FindModelByName( "mover" )
        if moverModel:
            gestureSafe.Parent = moverModel
            gestureSafe.Show = False

    # refresh
    FBSystem().Scene.Evaluate()


def rs_AssetSetupCSCharacters(pControl, pEvent):
    # check max revision
    result = CheckMaxRevision()
    if result != None:
        return

    # tidy face nodes
    if ProjectData.data.GetConfig("CharSetupGTAFaceFix"):
        faceControls = Scene.FindModelByName("faceControls_OFF")
        faceRoot = Scene.FindModelByName("facialRoot_C_OFF")
        if faceControls and faceRoot:
            faceRoot.Parent = faceControls
            fix3LShaders()
            FBSystem().Scene.Evaluate()

    # strip layers
    while FBSystem().CurrentTake.GetLayerCount() > 1:
        FBSystem().CurrentTake.GetLayer(1).FBDelete()

    # prep character into t-pose and characterise
    rs_PrepSetupCharacters(True)

    # get variables
    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    dummy = ass.rs_AssetSetupVariables()[0]
    mover = ass.rs_AssetSetupVariables()[1]
    geo = ass.rs_AssetSetupVariables()[2]
    skelRoot = mover.Children[0]

    # character
    character = glo.gCharacters[0]

    # toybox setup
    moverConstraint = rs_AssetSetupCharToyboxMoverControl()
    moverConstraint.Active = True

    # set mover properties - DOFs
    mover.PropertyList.Find("Enable Rotation DOF").Data = True
    mover.PropertyList.Find("RotationMinX").Data = True
    mover.PropertyList.Find("RotationMinY").Data = True
    mover.PropertyList.Find("RotationMaxX").Data = True
    mover.PropertyList.Find("RotationMaxY").Data = True

    # set pelvis properties
    lPelvisSkel = Scene.FindModelByName('SKEL_Pelvis')
    lPelvisSkel.PropertyList.Find("Enable Selection").Data = False
    lPelvisSkel.PropertyList.Find("Enable Transformation").Data = False

    # rotate mover marker
    character.InputType.kFBCharacterInputStance
    character.ActiveInput = True
    moverMarker = Scene.FindModelByName("mover_Control")
    moverMarker.PropertyList.Find("Lcl Rotation").Data = FBVector3d(90, 0, 0)
    FBSystem().Scene.Evaluate()
    character.ActiveInput = False
    character.ActiveInput = True

    # set up custom proeprties for ref edtior - not sure if needed for new ref editor?
    ass.rs_AssetSetupCustomProperties()

    # fix shoulders
    rs_FixShoulders()
    FBSystem().Scene.Evaluate()

    # parent the high heels to the hierarchy and set it to 0
    heelRoot = None
    heelRoot = Scene.FindModelByName("RECT_HeelHeight")
    if heelRoot != None:
        heelRoot.Parent = mover
        heelHeight = None
        heelHeight = Scene.FindModelByName("HeelHeight")
        if heelHeight != None:
            heelHeight.PropertyList.Find("Lcl Translation").SetAnimated(True)
            heelHeight.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
    FBSystem().Scene.Evaluate()

    # set up character extensions
    rs_CharacterExtension(character)
    FBSystem().Scene.Evaluate()

    # final setups used in both cs and ig
    IGAndCSFinalSetups(character)

    # tidy scene - groups, folders etc
    rs_CharacterSetupTidy()
    
    # Add toe relation constraint
    toeRelation = AddToeRelation()
    
    # Flip the mover (temp fix)
    rotateCSMover()
    
    # Detect face expressions
    detectFaceExpressions(fileName, skelRoot)


def rs_AssetSetupIGCharacters(pControl, pEvent):
    # get max revision info
    result = CheckMaxRevision()
    if result != None:
        return

    # tidy face nodes
    if ProjectData.data.GetConfig("CharSetupGTAFaceFix"):
        faceControls = Scene.FindModelByName("faceControls_OFF")
        faceRoot = Scene.FindModelByName("facialRoot_C_OFF")
        head = Scene.FindModelByName("SKEL_Head")
        if faceControls and head and faceRoot:
            faceControls.Parent = head
            faceRoot.Parent = faceControls
            FBSystem().Scene.Evaluate()

    # prep character into t-pose and characterise
    rs_PrepSetupCharacters(False)

    # heels setup
    heel.rs_HeelsSetup()

    # get variables
    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    dummy = ass.rs_AssetSetupVariables()[0]
    mover = ass.rs_AssetSetupVariables()[1]
    geo = ass.rs_AssetSetupVariables()[2]
    skelRoot = mover.Children[0]

    # character
    character = glo.gCharacters[0]

    # setup toybox
    moverConstraint = rs_AssetSetupCharToyboxMoverControl()
    moverConstraint.Active = False
    mover.Show = True

    # character set to stance pose
    character.InputType.kFBCharacterInputStance
    character.ActiveInput = True

    # some head selection/transform properties
    head = Scene.FindModelByName('SKEL_Head')
    if head:
        head.PropertyList.Find("Enable Selection").Data = False
        head.PropertyList.Find("Enable Transformation").Data = False

    # create movercam
    cameraGroup = MoverCameraCreate(mover)

    # create lighting rig and shadow setup - return the group
    mainShadowGroup = ShadowPlaneCreate(geo)

    # tidy scene - groups, folders etc
    rs_CharacterSetupTidy()

    # add safebone and expressions to main groups
    characterGroup = None
    safeBoneGroup = None
    expressionsGroup = None
    for group in glo.gGroups:
        if group.Name.lower() == fileName.lower():
            characterGroup = group
        if group.Name.lower() == 'safebones':
            safeBoneGroup = group
        if group.Name.lower() == 'expressions':
            expressionsGroup = group
    if characterGroup:
        if safeBoneGroup:
            characterGroup.ConnectSrc(safeBoneGroup)
        if expressionsGroup:
            characterGroup.ConnectSrc(expressionsGroup)

    # additional group tidy
    for group in glo.gGroups:
        if group.Name == fileName:
            mainGroup = group
        if group.Name == 'Contacts':
            contactGroup = group
        if group.Name == 'mover':
            moverGroup = group
        if group.Name == 'Joints':
            jointsGroup = group
    if mainShadowGroup:
        mainGroup.ConnectSrc(mainShadowGroup)
    if cameraGroup:
        mainGroup.ConnectSrc(cameraGroup)
    moverGroup.Show = False
    moverGroup.Pickable = True
    moverGroup.Transformable = True
    contactGroup.Show = False
    jointsGroup.Show = True

    # sets up IK bones
    boneStringList = ProjectData.data.CharacterIKPHBoneList()

    for boneString in boneStringList:
        iPHIK = Scene.FindModelByName(boneString)
        if iPHIK:
            lookProperty = iPHIK.PropertyList.Find('Look')
            if lookProperty:
                lookProperty.Data = 1
            sizeProperty = iPHIK.PropertyList.Find('Size')
            if sizeProperty:
                sizeProperty.Data = 3.0
            if boneString in ['IK_L_Hand', 'IK_R_Hand', 'PH_L_Hand', 'PH_R_Hand', 'mover']:
                iPHIK.PropertyList.Find("Enable Transformation").Data = True

    # rotate mover marker on 'ig_' chars - not sure why
    moverMarker = Scene.FindModelByName("mover_Control")
    if fileName.startswith("IG_"):
        moverMarker.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0, 0, 0)
        FBSystem().Scene.Evaluate()
    moverMarker.PropertyList.Find("Enable Transformation").Data = True

    # rename geo parent
    geo.Name = "Geometry"

    # fix shoulders
    rs_FixShoulders()
    FBSystem().Scene.Evaluate()

    # facing direction and lookat cones setup (upperfixup, independant mover & facing direction)
    ikcon.rs_FacingDirectionArrow()
    ikcon.rs_HeadLookAt()
    rs_AssetSetupFacingDirConstraint()
    FBSystem().Scene.Evaluate()

    # generate character extensions
    characterExtensionList = rs_CharacterExtension(character)
    for characterExtension in characterExtensionList:
        characterExtension.PlotAllowed = FBPlotAllowed.kFBPlotAllowed_Skeleton

    # get status of heel & IK setups - reassures the resource team these setups are complete
    # - they were essential to be set up correctly for gtaV
    heelBool =  heel.heelsCheckList
    faceDirectionBool = ikcon.faceDirectionCheckList
    lookAtBool = ikcon.lookAtCheckList

    # OH Pelvis setup
    OHPelvisSetup()

    # setup expression solver - implement when approved (see Kat). In for debug only.
    #Character.SetupExpressionSolver(character)

    # final setups used in both cs and ig
    IGAndCSFinalSetups(character)
    
    # Add toe relation constraint
    toeRelation = AddToeRelation()
    
    # Detect face expressions
    detectFaceExpressions(fileName, skelRoot)


'''
SHOULDNT NEED THE BELOW EXPRESSION SETUPS AFTER NEW EXPRESSION SYSTEM IS IN PLACE url:bugstar:2449964
'''
def onFacePositionFix():
    # facial hookups
    themBones = [
        "FACIAL_L_lipCornerSDK",
        "FACIAL_L_lipUpperSDK",
        "FACIAL_lipUpperSDK",
        "FACIAL_R_lipUpperSDK",
        "FACIAL_R_lipCornerSDK",
        "FACIAL_L_lipLowerSDK",
        "FACIAL_lipLowerSDK",
        "FACIAL_R_lipLowerSDK",
        "FACIAL_L_eyelidUpperOuterSDK",
        "FACIAL_L_eyelidUpperInnerSDK",
        "FACIAL_L_eyelidLowerOuterSDK",
        "FACIAL_L_eyelidLowerInnerSDK",
        "FACIAL_R_eyelidUpperOuterSDK",
        "FACIAL_R_eyelidUpperInnerSDK",
        "FACIAL_R_eyelidLowerOuterSDK",
        "FACIAL_R_eyelidLowerInnerSDK",
        "FACIAL_jaw" ]

    themControllers = [
        "lipCorner_L_OFF",
        "upperLip_L_OFF",
        "upperLip_C_OFF",
        "upperLip_R_OFF",
        "lipCorner_R_OFF",
        "lowerLip_L_OFF",
        "lowerLip_C_OFF",
        "lowerLip_R_OFF",
        "eyelidUpperOuter_L_OFF",
        "eyelidUpperInner_L_OFF",
        "eyelidLowerOuter_L_OFF",
        "eyelidLowerInner_L_OFF",
        "eyelidUpperOuter_R_OFF",
        "eyelidUpperInner_R_OFF",
        "eyelidLowerOuter_R_OFF",
        "eyelidLowerInner_R_OFF",
        "jaw_C_OFF" ]

    i = 0
    while i != len( themBones ):
        theBone = FBFindModelByLabelName( themBones[i] )
        theController = FBFindModelByLabelName( themControllers[i] )

        if ( theBone != None ) and ( theController != None ):
            glo.gScene.Evaluate()
            translationValue = theBone.Translation.Data
            theController.Translation.Data = translationValue
            glo.gScene.Evaluate()
        i+=1


def rs_CSFacialSetup( pControl, pEvent ):
    # Gather & Delete Facial Constraints and Camera Objects
    deleteMe = []
    searchItems = [ "3Lateral", "Viseme Relation" ]

    for constraint in glo.gScene.Constraints:
        for each in searchItems:
            if each in constraint.Name:
                deleteMe.append( constraint )

    deleteMe = list( set( deleteMe ) )
    for each in deleteMe:
        each.FBDelete()

    deleteObjects = FBComponentList()
    FBFindObjectsByName("3Lateral*Camera*", deleteObjects, True, False)
    for each in deleteObjects:
        each.FBDelete()

    # Odds and ends
    # onFacePositionFix()

    rectOffset = None
    rectOffset = Scene.FindModelByName( "RECT_ScaleOffset" )
    if rectOffset != None:
        rectOffset.Parent = None

    glo.gScene.Evaluate()
    # Odds and ends - Done

    check = lat.rs_3LateralUISetUp()
    if check:
        onFacePositionFix()
        solverAssets = solv.detectSolverAssets()
        if solverAssets is not None:
            solv.facialSolverSetup(solverAssets)
        else:
            concs.rs_ConnectRigInABox()
            concs.rs_ConnectRigInABox1()
        conig.rs_SetUpAmbientUI()

        glo.gScene.Evaluate()
        fileName = Path.GetBaseNameNoExtension( FBApplication().FBXFileName )


def rs_IGFacialSetup( pControl, pEvent ):
    conig.rs_FacialPreRotation()
    conig.rs_ConnectAmbientFaceRig()
    conig.rs_SetUpAmbientUI()

    glo.gScene.Evaluate()

    rectOffset = None
    rectOffset = Scene.FindModelByName("RECT_ScaleOffset")
    if rectOffset != None:
        rectOffset.Parent = None

    lFileName = Path.GetBaseNameNoExtension(glo.App.FBXFileName)


def rs_getScaleFromPedsMeta( charName ):
    scaleValue = 1.0
    gProjRoot = RS.Config.Project.Path.Root
    pXmlFileName = gProjRoot + "\\assets\\export\\data\\peds.pso.meta"
    if(os.path.exists(pXmlFileName)):
        Perforce.Sync( pXmlFileName )
        charName = os.path.basename( FBApplication().FBXFileName ).split(".")[0]
        lXmlFile = minidom.parse( pXmlFileName )
        rootData = lXmlFile.getElementsByTagName("InitDatas")
        charData =  rootData[0].getElementsByTagName("Name")
        scaleData = rootData[0].getElementsByTagName("CutsceneScale")
        for char in charData:
            if char.childNodes[0].data == charName:
                for scale in scaleData:
                    if char.parentNode == scale.parentNode:
                        scaleValue = float( scale.getAttribute("value") )
    return float( scaleValue )


def thighRollFix ( relConstrName, oldThighName, newThighName ):
# Custom Thigh Roll Fix after applying standard expressions.
    for relation in FBSystem().Scene.Constraints:
        if relation.Name == relConstrName:

            # Gather boxes
            oldThighBoxes = []
            inputPlug = None
            multBox = None
            for box in relation.Boxes:
                if "Add (V1 + V2)" in box.Name:
                    for plug in box.AnimationNodeInGet().Nodes:
                        if plug.Name == "V2":
                            inputPlug = plug
                if oldThighName in box.Name:
                    oldThighBoxes.append( box )
                if "Multiply (a x b)" in box.Name:
                    multBox = box

            # Create new connection box
            newThighBox_obj = FBFindModelByLabelName( newThighName )
            newThighBox = relation.SetAsSource( newThighBox_obj )
            newThighBox.UseGlobalTransforms = False
            relation.SetBoxPosition( newThighBox, 0, 500 )

            # Disconnect invalid connection
            for box in oldThighBoxes:
                for outputPlug in box.AnimationNodeOutGet().Nodes:
                    if outputPlug.Name == "Lcl Rotation":
                        if inputPlug != None:
                            FBDisconnect( outputPlug, inputPlug )

            # Connect new connection
            for outputPlug in newThighBox.AnimationNodeOutGet().Nodes:
                if outputPlug.Name == "Lcl Rotation":
                    if inputPlug != None:
                        FBConnect( outputPlug, inputPlug )

            # Update multiply box Value
            for plug in multBox.AnimationNodeInGet().Nodes:
                if plug.Name == "b":
                    plug.WriteData( [-.5], None )


def fixupExpressions( pControl, pEvent ):
    deactivateConstraints = ["MH_Hair_Scale-X_scale",
                            "MH_Hair_Scale-Y_scale",
                            "MH_Hair_Scale-Z_scale",
                            "MH_Hair_Crown-X_Position",
                            "MH_Hair_Crown-X_scale",
                            "MH_Hair_Crown-Y_scale",
                            "MH_Hair_Crown-Z_scale",]

    removeConstraints = ["RB_L_ThighRoll-Y_rotation",
                        "RB_L_ThighRoll-Z_rotation",
                        "RB_R_ThighRoll-Y_rotation",
                        "RB_R_ThighRoll-Z_rotation",
                        "RB_L_ForeArmRoll-Y_rotation",
                        "RB_L_ForeArmRoll-Z_rotation",
                        "RB_L_ArmRoll-Y_rotation",
                        "RB_L_ArmRoll-Z_rotation",
                        "RB_R_ForeArmRoll-Y_rotation",
                        "RB_R_ForeArmRoll-Z_rotation",
                        "RB_R_ArmRoll-Y_rotation",
                        "RB_R_ArmRoll-Z_rotation",
                        "SKEL_ROOT-X_Position",
                        "SKEL_ROOT-Y_Position",
                        "SKEL_ROOT-Z_Position"]
    
    deleteMe = FBComponentList()
    for constr in FBSystem().Scene.Constraints:
        if constr.Name in deactivateConstraints:
            constr.Active = False
        if constr.Name in removeConstraints:
            deleteMe.append(constr)
        
    for item in deleteMe:
        item.FBDelete()

    thighRollFix( "RB_L_ThighRoll-X_rotation", "RB_L_ThighRoll", "SKEL_L_Thigh" )
    thighRollFix( "RB_R_ThighRoll-X_rotation", "RB_R_ThighRoll", "SKEL_R_Thigh" )


def AddToeRelation():
    """
    Adds a relation constraint so the EO toe rotataions are driven by the SKEL
    bones. This allows toe rotation on heeled characters when driven by GS skel
    or Control Rig.
    """
    # Find the EO and SKEL toe bones
    leftToeSkel = FBFindModelByLabelName("SKEL_L_Toe0")
    rightToeSkel = FBFindModelByLabelName("SKEL_R_Toe0")
    leftToeEO = FBFindModelByLabelName("EO_L_Toe")
    rightToeEO = FBFindModelByLabelName("EO_R_Toe")
    
    if not leftToeEO or not rightToeEO:
        return
    
    # Set Pre-Rotation DOF Values to match the SKEL_ toe 
    leftToeRotation = leftToeSkel.Rotation.Data
    rightToeRotation = rightToeSkel.Rotation.Data
    leftEORotation = leftToeEO.Rotation.Data
    rightEORotation = rightToeEO.Rotation.Data
    
    leftPreRotation = leftEORotation-leftToeRotation
    rightPreRotation = rightEORotation-rightToeRotation
    
    leftToeEO.PropertyList.Find("Enable Rotation DOF").Data = True
    rightToeEO.PropertyList.Find("Enable Rotation DOF").Data = True
    
    leftToeEO.PropertyList.Find("Pre Rotation").Data = FBVector3d(leftPreRotation)
    rightToeEO.PropertyList.Find("Pre Rotation").Data = FBVector3d(rightPreRotation)
    
    # Create new Toe Relation Constraint
    relationConst = FBConstraintRelation('Toe_Relation')
    
    # Add the connection boxes to drive the EO rotations by the SKEL ones
    leftToeSender = relationConst.SetAsSource(leftToeSkel)
    relationConst.SetBoxPosition(leftToeSender, 0, 0)
    leftToeReceive = relationConst.ConstrainObject(leftToeEO)
    relationConst.SetBoxPosition(leftToeReceive, 300, 0)
    rightToeSender = relationConst.SetAsSource(rightToeSkel)
    relationConst.SetBoxPosition(rightToeSender, 0, 150)
    rightToeReceive = relationConst.ConstrainObject(rightToeEO)
    relationConst.SetBoxPosition(rightToeReceive, 350, 150)
    leftToeSender.UseGlobalTransforms = False
    leftToeReceive.UseGlobalTransforms = False
    rightToeSender.UseGlobalTransforms = False
    rightToeReceive.UseGlobalTransforms = False
    Scene.ConnectBox(leftToeSender, 'Lcl Rotation', leftToeReceive, 'Lcl Rotation')
    Scene.ConnectBox(rightToeSender, 'Lcl Rotation', rightToeReceive, 'Lcl Rotation')
    relationConst.Active = True
    # Add the new constraint into the constraints folder
    for folder in glo.Folders:
        if folder.Name == 'Constraints 1':
            constraintFolder = folder
            constraintFolder.Items.append(relationConst)
    return relationConst
    
def rotateCSMover():
    """
    Deactivates characterisation, recharacterizes, then sets the mover rotation
    back to what it was previously.
    Seems to fix some thumb solver issues the Giant team reported, but is a temp
    solution. See url:bugstar:5772449
    """
    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    character = None
    for sceneCharacter in glo.Characters:
        if sceneCharacter.Name == fileName:
            character = sceneCharacter
    mover = FBFindModelByLabelName("mover")
    currentRotation = mover.Rotation.Data
    # Disable characterization
    character.SetCharacterizeOff()
    #Re-enable chracterization
    character.SetCharacterizeOn(True)
    # Restore mover's rotation
    mover.Rotation.Data = currentRotation

def fix3LShaders():
    """
    Fixes texture paths for the 3Lateral shaders by merging in the fix file
    """
    # Initiate empty lists
    faceObjects = []
    blankControllers = []
    
    # Find facial controller roots
    faceControlRoot = FBFindModelByLabelName("faceControls_OFF")
    faceControlRoot2 = FBFindModelByLabelName("facialRoot_C_OFF")
    
    if not faceControlRoot:
        return
    
    # Populate face Control Objects list with all children from the control roots
    Scene.GetChildren(faceControlRoot, faceObjects)
    if faceControlRoot2:
        Scene.GetChildren(faceControlRoot2, faceObjects)

    if len(faceObjects) == 0:
        return
    
    faceControllers = [controller for controller in faceObjects if "_CTRL" in controller.Name]
    for controller in faceControllers:
        if len(controller.Materials) == 0:
            blankControllers.append(controller)
    
    if len(blankControllers) == 0:
        return
    
    Perforce.Sync("x:\\gta5\\art\\peds\\3LateralSetup\\gui_texture_fix.fbx")
    fixedFile = "x:\\gta5\\art\\peds\\3LateralSetup\\gui_texture_fix.fbx"
    
    FbxMergeOptions = FBFbxOptions(False)
    FbxMergeOptions.SetAll(FBElementAction.kFBElementActionDiscard, False)
    FbxMergeOptions.Materials = FBElementAction.kFBElementActionMerge
    FbxMergeOptions.Textures= FBElementAction.kFBElementActionMerge
    FbxMergeOptions.Video = FBElementAction.kFBElementActionMerge
    FBApplication().FileMerge(fixedFile, True, FbxMergeOptions)