from pyfbsdk import *

import os

import RS.Globals as glo
import RS.Perforce
import RS.Config

def Run():
    if "ingame" in glo.gApp.FBXFileName:
    
        if "Player_Zero" in glo.gApp.FBXFileName:
            
            lHand = FBFindModelByLabelName("hand_000_r")
            
            for i in range(lHand.GetSrcCount()):
                
                lSource = lHand.GetSrc(i)
                
                if lSource.ClassName() == "FBMaterial":
                    
                    lPath = "{0}/art/peds/Ped_Parts/MotionBuilder_Textures/HAND_DIFF_000_A_WHI_Arrow.tga".format( RS.Config.Project.Path.Root )
                    
                    RS.Perforce.Sync( lPath )
                    
                    lTexture = lSource.GetTexture()
                    print lTexture.Video.Filename
                    lTexture.Video.Filename = lPath
                    
        elif "A_M_Y_Skater_01" in glo.gApp.FBXFileName:
            
            lHand = FBFindModelByLabelName("Hand_001_R")
            
            for i in range(lHand.GetSrcCount()):
                
                lSource = lHand.GetSrc(i)
                
                if lSource.ClassName() == "FBMaterial":
                    
                    lPath = "{0}/art/peds/Ped_Parts/MotionBuilder_Textures/hand_diff_001_a_whi_Arrow.tga".format( RS.Config.Project.Path.Root )
                    
                    RS.Perforce.Sync( lPath )
                    
                    lTexture = lSource.GetTexture()
                    print lTexture.Video.Filename
                    lTexture.Video.Filename = lPath
            
            

        
        
        



