'''

 Script Path: RS/Core/AssetSetup/VehicleRig.py

 Written And Maintained By: Kathryn Bodey

 Created: 01 April 2014

 Description: Suspension for Car Rig

'''

from pyfbsdk import *

import RS.Utils.Scene
import RS.Utils.Scene.Model
reload(RS.Utils.Scene.Model)
import RS.Globals
import RS.Perforce
from RS.Utils import Path
from RS.Utils import Scene


import os

class RotationDOFSpec:
        def __init__( self, XMin, XMax, YMin, YMax, ZMin, ZMax ):
                self.XMin = XMin
                self.XMax = XMax
                self.YMin = YMin
                self.YMax = YMax
                self.ZMin = ZMin
                self.ZMax = ZMax

DOF_SPEC_DICTIONARY = dict()
DOF_SPEC_DICTIONARY[ 'benson' ]		= RotationDOFSpec(0.0, 120, 0.0, 0.0, 0.0, 0.0)
DOF_SPEC_DICTIONARY[ 'armordillo2' ]	= RotationDOFSpec(0.0, 0.0, 0.0, 0.0, -90.0, 0.0)
DOF_SPEC_DICTIONARY[ 'romero' ]		= RotationDOFSpec(0.0, 0.0, 0.0, 0.0, -90.0, 0.0)
DOF_SPEC_DICTIONARY[ 'technical' ]	= RotationDOFSpec(0.0, 150.0, 0.0, 0.0, 0.0, 0.0)
DOF_SPEC_DICTIONARY[ 'bobcatxl' ]	= RotationDOFSpec(0.0, 90.0, 0.0, 0.0, 0.0, 0.0)

#Sets up vehicles with 4 wheels (with different configurations of doors/boots etc...)
def Setup4Wheel( ):

        #Requirements:
                #Vehicle wheels should be sitting on zero plane
                #Bone for each wheel that follows naming 'wheel_xx'
                #Bone for 'chassis'
        #Optional:
                #Bones for 'door_dside_r', 'door_dside_f',  'door_pside_r', 'door_pside_f', 'boot'

        if FBMessageBox("Message", "This will now attempt to create a rig for a 4 wheeled vehicle.\n\nEnsure that you have the vehicle .fbx open, the vehicle has only 4 wheels and all old rigs have been deleted.\n\nWould you like to continue?", "Yes", "No", None, 1 ) > 1:
                return

        try:
                #Check we have bones the we need
                for boneModelName in ["chassis", "wheel_lf", "wheel_lr", "wheel_rr", "wheel_rf"]:
                        boneModel = FBFindModelByLabelName( boneModelName )
                        if boneModel == None:
                                raise Exception("Missing important bone: {0}".format(boneModelName))

                #Check that we have the rig
                rigFilePath = "{0}\\animation\\resources\\vehicles\\rigs\\4_Wheel.fbx".format(RS.Config.Project.Path.Art)
                RS.Perforce.Sync(rigFilePath)

                if not os.path.exists(rigFilePath):
                        raise Exception("Missing rig file: {0}".format(rigFilePath))

                #Merge in rig
                mergeOptions = FBFbxOptions(True)
                mergeOptions.UpdateRecentFiles = False
                RS.Globals.Application.FileMerge( rigFilePath, False, mergeOptions )

                # if the vehicle has vertical opening doors, we need to reset the rotation locks
                fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName).lower()

                verticleGullWingDoorVehicles = ['osiris']
                for nameString in verticleGullWingDoorVehicles:
                        if fileName.lower() == nameString:
                                doorDSide = Scene.FindModelByName('door_dside_f_handle')
                                doorPSide = Scene.FindModelByName('door_pside_f_handle')
                                if doorDSide and doorPSide:
                                        doorDSide.Rotation.SetMemberLocked( 0 , True )
                                        doorDSide.Rotation.SetMemberLocked( 1 , False )
                                        doorDSide.Rotation.SetMemberLocked( 2 , True )
                                        doorDSide.PropertyList.Find("RotationMin").Data = FBVector3d(0,0,0)
                                        doorDSide.PropertyList.Find("RotationMax").Data = FBVector3d(0,90,0)
                                        doorPSide.Rotation.SetMemberLocked( 0 , True ) 
                                        doorPSide.Rotation.SetMemberLocked( 1 , False )
                                        doorPSide.Rotation.SetMemberLocked( 2 , True )
                                        doorPSide.PropertyList.Find("RotationMin").Data = FBVector3d(0,-90,0)
                                        doorPSide.PropertyList.Find("RotationMax").Data = FBVector3d(0,0,0)

                verticleDoorVehicles = ['t20']
                for nameString in verticleDoorVehicles:
                        if fileName.lower() == nameString:
                                doorDSide = Scene.FindModelByName('door_dside_f_handle')
                                doorPSide = Scene.FindModelByName('door_pside_f_handle')
                                if doorDSide and doorPSide:
                                        doorDSide.Rotation.SetMemberLocked( 0 , True )
                                        doorDSide.Rotation.SetMemberLocked( 1 , True )
                                        doorDSide.Rotation.SetMemberLocked( 2 , False )
                                        doorDSide.PropertyList.Find("RotationMin").Data = FBVector3d(0,0,0)
                                        doorDSide.PropertyList.Find("RotationMax").Data = FBVector3d(0,0,90)
                                        doorPSide.Rotation.SetMemberLocked( 0 , True ) 
                                        doorPSide.Rotation.SetMemberLocked( 1 , True )
                                        doorPSide.Rotation.SetMemberLocked( 2 , False )
                                        doorPSide.PropertyList.Find("RotationMin").Data = FBVector3d(0,0,-90)
                                        doorPSide.PropertyList.Find("RotationMax").Data = FBVector3d(0,0,0)

                #Get our chassis bone model
                chassisBoneModel = FBFindModelByLabelName("chassis")

                #Create our connection relation
                connectRelation = FBConstraintRelation("connect_relation")

                #Align root to chassis
                rootModel = FBFindModelByLabelName("rig")
                RS.Utils.Scene.Align( rootModel, chassisBoneModel, True, True, True, False, False, False )
                RS.Globals.Scene.Evaluate()

                #Get our Dummy01 model
                dummy01Model = FBFindModelByLabelName("Dummy01")

                #Parent root model to Dummy01
                rootModel.Parent = dummy01Model

                #For each of our wheels
                wheelRadius = 0
                for wheelSuffix in ["lf","lr","rr","rf"]:

                        #Get the wheel name and bone
                        wheelName = "wheel_{0}".format(wheelSuffix)
                        wheelBoneModel = FBFindModelByLabelName(wheelName)

                        #Translate the wheel root model to the centre of the wheel bone
                        wheelRootModelName = "{0}_root".format(wheelSuffix)
                        wheelRootModel = FBFindModelByLabelName(wheelRootModelName)
                        RS.Utils.Scene.Align( wheelRootModel, wheelBoneModel, True, True, True, False, False, False )
                        RS.Globals.Scene.Evaluate()
                        RS.Utils.Scene.Model.FreezeTransform(wheelRootModel)

                        #Move the suspension plane to be inline with zero on Y
                        wheelSuspensionPlaneModelName = "{0}_suspension_plane_root".format(wheelSuffix)
                        wheelSuspensionPlaneModel = FBFindModelByLabelName(wheelSuspensionPlaneModelName)
                        RS.Utils.Scene.Model.SetGlobalTranslation( wheelSuspensionPlaneModel, None, 0, None )
                        RS.Globals.Scene.Evaluate()
                        RS.Utils.Scene.Model.FreezeTransform(wheelSuspensionPlaneModel)		

                        #Calculate wheel radius from the distance suspension plane is to wheel bone
                        wheelRadius += RS.Utils.Scene.Model.Distance( wheelSuspensionPlaneModel, wheelBoneModel )

                        #Move the suspension aim to be inline with the wheel bone on Y
                        wheelSuspensionAimModelName = "{0}_suspension_aim".format(wheelSuffix)
                        wheelSuspensionAimModel = FBFindModelByLabelName(wheelSuspensionAimModelName)		
                        RS.Utils.Scene.Align( wheelSuspensionAimModel, wheelBoneModel, False, True, False, False, False, False )
                        RS.Globals.Scene.Evaluate()
                        RS.Utils.Scene.Model.FreezeTransform(wheelSuspensionAimModel)

                        #Move the chassis manipulate up to be inline with the chassis bone on Y
                        wheelChassisManipulateModelName = "{0}_chassis_manipulate_root".format(wheelSuffix)
                        wheelChassisManipulateModel = FBFindModelByLabelName(wheelChassisManipulateModelName)
                        RS.Utils.Scene.Align( wheelChassisManipulateModel, chassisBoneModel, False, True, False, False, False, False )
                        RS.Globals.Scene.Evaluate()
                        RS.Utils.Scene.Model.FreezeTransform(wheelChassisManipulateModel)

                        #Get wheel override model		
                        wheelOverrideModelName = "{0}_override".format(wheelSuffix)
                        wheelOverrideModel = FBFindModelByLabelName(wheelOverrideModelName)				

                        #Add and hook up in our connectRelation
                        wheelOverrideModelBoxSource = connectRelation.SetAsSource(wheelOverrideModel)
                        wheelOverrideModelBoxSource.UseGlobalTransforms = True

                        wheelBoneModelBoxTarget = connectRelation.ConstrainObject(wheelBoneModel)
                        wheelBoneModelBoxTarget.UseGlobalTransforms = True

                        RS.Utils.Scene.ConnectBox( wheelOverrideModelBoxSource, 'Rotation', wheelBoneModelBoxTarget, 'Rotation' )				
                        RS.Utils.Scene.ConnectBox( wheelOverrideModelBoxSource, 'Translation', wheelBoneModelBoxTarget, 'Translation' )

                #Input wheel circumference into the autorotation constraint
                wheelRadius = wheelRadius / 4
                wheelCircumference = ( wheelRadius * 2 ) * 3.14159265359
                wheelOverrideConstraint = RS.Utils.Scene.GetComponentByNameAndType( FBConstraint , "wheel_autorotation_relation" )
                targetBox = None
                for box in wheelOverrideConstraint.Boxes:
                        if box.Name.find("wheel_circumference") != -1:
                                targetBox = box
                                break
                targetNode = targetBox.AnimationNodeInGet().Nodes[1]
                targetNode.WriteData([wheelCircumference])

                #Align various controls
                for controlBonePairings in [
                        ("chassis", "chassis_control"),
                        ("door_dside_r", "door_dside_r_control"),
                        ("door_dside_f", "door_dside_f_control"),
                        ("door_pside_r", "door_pside_r_control"),
                        ("door_pside_f", "door_pside_f_control"),
                        ("boot", "boot_control")
                        ]:

                        targetBoneName = controlBonePairings[0]	
                        targetBoneModel = FBFindModelByLabelName( targetBoneName )

                        targetControlName = controlBonePairings[1]
                        targetControlModel = FBFindModelByLabelName( targetControlName )

                        if targetBoneModel != None and targetControlModel != None :

                                RS.Utils.Scene.Align( targetControlModel, targetBoneModel, True, True, True, True, True, True )
                                RS.Globals.Scene.Evaluate()

                                RS.Utils.Scene.Model.FreezeTransform( targetControlModel )
                                RS.Globals.Scene.Evaluate()

                #Hook up the boot, door and chassis controls
                for controlBonePairings	in [	("chassis", "chassis_control"), 
                                                           ("door_dside_r", "door_dside_r_handle"),
                                                           ("door_dside_f", "door_dside_f_handle"),
                                                           ("door_pside_r", "door_pside_r_handle"),
                                                           ("door_pside_f", "door_pside_f_handle"),
                                                           ("boot", "boot_handle"),
                                                           ("mover", "mover_control")]:

                        targetBoneName = controlBonePairings[0]
                        targetBoneModel = FBFindModelByLabelName( targetBoneName )

                        targetControlName = controlBonePairings[1]
                        targetControlModel = FBFindModelByLabelName( targetControlName )

                        if targetBoneModel != None and targetControlModel != None : 

                                targetBoneModelBoxTarget = connectRelation.ConstrainObject(targetBoneModel)
                                targetBoneModelBoxTarget.UseGlobalTransforms = True

                                targetControlModelBoxSource = connectRelation.SetAsSource(targetControlModel)
                                targetControlModelBoxSource.UseGlobalTransforms = True

                                RS.Utils.Scene.ConnectBox( targetControlModelBoxSource, 'Rotation', targetBoneModelBoxTarget, 'Rotation' )				
                                RS.Utils.Scene.ConnectBox( targetControlModelBoxSource, 'Translation', targetBoneModelBoxTarget, 'Translation' )			

                #Freeze scaling on all objects that are part of the rig (defensive measure that stops MoBu doing some weird shit when aligning/constraining to other objects)
                rigModels = list()
                RS.Utils.Scene.GetChildren( rootModel, rigModels )
                for rigModel in rigModels:
                        RS.Utils.Scene.Model.FreezeScaling( rigModel )

                #These are controls from the old rig setup and need removing	
                oldChassisControl = RS.Utils.Scene.GetComponentByNameAndType( FBModelMarker , "chassis_Control" )
                oldMoverControl = RS.Utils.Scene.GetComponentByNameAndType( FBModelMarker , "mover_Control" )
                oldSuspensionConstraint = RS.Utils.Scene.GetComponentByNameAndType( FBConstraintRelation , "Marker_Suspension_Relation" )
                oldConstraintFolder = RS.Utils.Scene.GetComponentByNameAndType( FBFolder , "Constraints 1" )
                for component in [oldChassisControl, oldMoverControl, oldSuspensionConstraint, oldConstraintFolder]:
                        if component != None:
                                component.FBDelete()

                # Special rotations for boot
                carName = RS.Utils.Path.GetBaseNameNoExtension( FBApplication().FBXFileName )

                if DOF_SPEC_DICTIONARY.has_key( carName.lower() ):
                        bootHandle = RS.Utils.Scene.FindModelByName( 'boot_handle' ) # vans with 2 boot doors will have a different name. need to adjust this when we come to that.
                        if bootHandle:
                                dofSpec = DOF_SPEC_DICTIONARY[ carName.lower() ]
                                bootHandle.Rotation.SetLocked( False ) # need to find how to unlock specific axis, rather than all of them
                                bootHandle.RotationMax = FBVector3d(dofSpec.XMax, dofSpec.YMax, dofSpec.ZMax)
                                bootHandle.RotationMin = FBVector3d(dofSpec.XMin, dofSpec.YMin, dofSpec.ZMin)

                #Activate rig
                connectRelation.Active = True

        except Exception as e:
                FBMessageBox( "Error processing vehicle", e.message, "Ok")

def Setup4WheelTrigger( Control, Event ):
        Setup4Wheel( )