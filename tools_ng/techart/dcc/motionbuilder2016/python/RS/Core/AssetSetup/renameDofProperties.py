from pyfbsdk import *
import RS.Utils.Scene.AnimationNode
import RS.Globals as glo
import RS.Perforce
import RS.Utils.Scene.Take
from ctypes import *
from pyfbsdk_additions import *
import os

def appendIfUnique(array, nodeToAdd):
    if nodeToAdd not in array:
        array.append(nodeToAdd)
        ##print "Adding "+nodeToAdd.Name+" to array"
    else:
        print "NOT Adding "+nodeToAdd.Name+" to array as its already there"
    return array


def get_all_children(root, array):
    for c in root.Children:
        ##array.append(c)
        array = appendIfUnique(array, c)
        get_all_children(c, array)
    return array

def getNodeByName(useNameSpace, name, nameSpace, modelsOnly):
    returnNode = None

    cl = FBComponentList()
    searchString = name
    if nameSpace != None:
        searchString = nameSpace+":"+name
        
    FBFindObjectsByName(searchString, cl, useNameSpace, modelsOnly)     
    
    ##print "Number of matches: "+str(len(cl))
    if len(cl) == 1:
        returnNode = cl[0]
        ##print "Found "+cl[0].LongName
    else:
        if len(cl) == 0:
            print "Couldn't find "+nameSpace+":"+name
        else:
            print "Found multiple matches for "+name
        
    return returnNode
    
lNameSpace = "A_M_Y_Skater_01"
    
attrNode = getNodeByName(True, "FirstPersonCamPivotAttrs", lNameSpace, True)    

for property in attrNode.PropertyList:
    # The following name can be used to obtain a property in FBComponent.PropertyList.Find(<name>)
    if property.Name == "DOF_MaxX":
        print property.Name
        property.Name = "FirstPersonCamPivotAttrs_DOF_MaxX"
    if property.Name == "DOF_MaxY":
        print property.Name
        property.Name = "FirstPersonCamPivotAttrs_DOF_MaxY"    
    if property.Name == "DOF_MaxZ":
        print property.Name
        property.Name = "FirstPersonCamPivotAttrs_DOF_MaxZ"        
    if property.Name == "DOF_MinX":
        print property.Name
        property.Name = "FirstPersonCamPivotAttrs_DOF_MinX"
    if property.Name == "DOF_MinY":
        print property.Name
        property.Name = "FirstPersonCamPivotAttrs_DOF_MinY"
    if property.Name == "DOF_MinZ":
        print property.Name
        property.Name = "FirstPersonCamPivotAttrs_DOF_MinZ"