from pyfbsdk import *
from pyfbsdk_additions import *

from ctypes import *
import os

import RS.Utils.Scene.AnimationNode
import RS.Globals as glo
import RS.Perforce
import RS.Utils.Scene.Take
import RS.Config



'''
GENERIC DEFS
'''
def rs_CharacterExtension(lCharacter):
    lExtensionCreated = False
    lCHLHand = RS.Utils.Scene.FindModelByName('CH_L_Hand')
    lCHRHand = RS.Utils.Scene.FindModelByName('CH_R_Hand')  

    lExtList = [lCHLHand, lCHRHand]

    lCharExtensions = lCharacter.CharacterExtensions
    for lExtension in lCharExtensions:
        if(lExtension.Name=='Char_Extention'):
            FBConnect(lCHLHand,lExtension)
            FBConnect(lCHRHand,lExtension)
            lExtensionCreated = True
    if lExtensionCreated == False:
        FBConnect(lCHLHand,lCharExtensions[0])
        FBConnect(lCHRHand,lCharExtensions[0])        

    FBSystem().Scene.Evaluate()
    return lCharExtensions


def appendIfUnique(array, nodeToAdd):
    if nodeToAdd not in array:
        array.append(nodeToAdd)
    else:
        print "NOT Adding "+nodeToAdd.LongName+" to array as its already there"
    return array


def createSelectionHandle(followObj, handleName):
    myHandle = FBHandle(handleName)

    propManager = myHandle.PropertyList

    ##printProperties(myHandle)

    myHandle.Show = True
    myHandle.HandlerShow3DModel = True
    myHandle.Handler3DModelLook = 2
    myHandle.Handler3DColor = FBColor(200,0,0)
    myHandle.Follow.append(followObj)
    myHandle.Manipulate.append(followObj)

    return myHandle


'''
CUSTOM DEFS
'''

def addAttributesToExporter(lNameSpace):
    if lNameSpace:
        customAttributeList = [
            "FirstPersonCamPivotAttrs_DOF_MaxX",
            "FirstPersonCamPivotAttrs_DOF_MaxY",
            "FirstPersonCamPivotAttrs_DOF_MaxZ",
            "FirstPersonCamPivotAttrs_DOF_MinX",
            "FirstPersonCamPivotAttrs_DOF_MinY",
            "FirstPersonCamPivotAttrs_DOF_MinZ",
            "FirstPersonCamPivotAttrs_DOF_FOV"
        ]

        attrNode = RS.Utils.Scene.FindModelByName( lNameSpace + ":FirstPersonCamPivotAttrs", True)
        skelRoot = RS.Utils.Scene.FindModelByName( lNameSpace + ":SKEL_ROOT", True)

        for attribute in customAttributeList:
            cdll.rexMBRage.AddAdditionalModelEntry_Py( skelRoot.LongName, attrNode.LongName, "kTrackCameraLimit", attribute) 
    
    else:
        print "no namespace found for function [addAttributesToExporter]"


def driveGameJointByRig(lNameSpace):
    if lNameSpace:
        # this will actually drive the FirstPersonCamPivotMoverSpace by the cameraHelper
        sourceObj = None
        targetObj = None
        targetCamPivotObj = None

        sourceObj = RS.Utils.Scene.FindModelByName( lNameSpace + ":coneMaster", True)
        cameraHelperObj = RS.Utils.Scene.FindModelByName( lNameSpace + ":cameraHelper", True)   
        targetObj = RS.Utils.Scene.FindModelByName( lNameSpace + ":FirstPersonCamPivot_MoverSpace", True) 
        targetCamPivotObj = RS.Utils.Scene.FindModelByName( lNameSpace + ":FirstPersonCamPivot", True)                            

        rigFollowCon = FBConstraintManager().TypeCreateConstraint(3)
        rigFollowCon.LongName = lNameSpace+":"+"RigToJoint"

        rigFollowCon.ReferenceAdd(0,targetObj) ##target
        rigFollowCon.ReferenceAdd(1,cameraHelperObj) ##source

        # Align the MoverSpace to the cameraHelper
        tVect = FBVector3d()
        rVect = FBVector3d()
        cameraHelperObj.GetVector(rVect, FBModelTransformationType.kModelRotation, True)
        cameraHelperObj.GetVector(tVect, FBModelTransformationType.kModelTranslation, True)   
        targetObj.SetVector(rVect, FBModelTransformationType.kModelRotation, True)   
        targetObj.SetVector(tVect, FBModelTransformationType.kModelTranslation, True)   

        rigFollowCon.Active = True
        rigFollowCon.Lock = True

        # NOW WE NEED TO SETUP THE RELATION BETWEEN MOVERSPACE AND FIRSTPERSONCAMPIVOT 
        MoverToRoot = None
        MoverToRoot = FBConstraintRelation(lNameSpace+":"+'MoverToRoot_CamPivot')

        # Create all the boxes
        SenderObj = MoverToRoot.SetAsSource(targetObj)
        SenderObj.UseGlobalTransforms = False
        MoverToRoot.SetBoxPosition(SenderObj, -200, 400)       

        receiverObj = MoverToRoot.ConstrainObject(targetCamPivotObj)
        receiverObj.UseGlobalTransforms = False
        MoverToRoot.SetBoxPosition(receiverObj, 3200, 500)   

        ##Now connect all the boxes                      
        RS.Utils.Scene.ConnectBox(SenderObj, "Lcl Rotation", receiverObj, "Lcl Rotation")    
        ##now turn on the constraint
        MoverToRoot.Active = True  

        # now add a ahandle to the coneMaster
        coneRootNodeHandle = createSelectionHandle(sourceObj, lNameSpace+":"+"cameraPivotHandle")      
    else:
        print "no namespace found for function [driveGameJointByRig]"
        
        
# Creates a group for the cone master and makes the handle visible
def createGroupCamMaster(lNameSpace):
    if lNameSpace:
        #Create Groups
        lMainGroup = None
        for iGroup in glo.Groups:
            if iGroup.Name == lNameSpace:
                lMainGroup = iGroup

        if lMainGroup:
            # Group for the CAM Master
            lCameraGroup = FBGroup(lNameSpace+":FPS_Camera_Control")
            coneMasterNode = RS.Utils.Scene.FindModelByName( lNameSpace + ":coneMaster", True)  
            # Group for the preview CAMERA
            lCameraPreviewGroup = FBGroup(lNameSpace+":FPS_Camera_Preview")
            cameraPreviewNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":ConeGeo", True)
            FPSCamNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":FPS_Cam", True)
            FPSCamPreviewNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":FPS_Preview_Cam", True)                                    

            lMainGroup.ConnectSrc(lCameraGroup)
            lCameraGroup.ConnectSrc(coneMasterNode)
            lCameraGroup.Show = True

            lMainGroup.ConnectSrc(lCameraPreviewGroup)
            lCameraPreviewGroup.ConnectSrc(cameraPreviewNode)
            lCameraPreviewGroup.ConnectSrc(FPSCamNode)
            lCameraPreviewGroup.ConnectSrc(FPSCamPreviewNode)
            lCameraPreviewGroup.Show = False
    else:
        print "no namespace found for function [createGroupCamMaster]"
        
        
def linkDofAttrs(conName, sourceObj, targetObjs, lNameSpace):
    if lNameSpace:
        attrCon = None
        attrCon = FBConstraintRelation(lNameSpace+":"+conName) 
        print "created attrCon:"+attrCon.LongName
    
    
        attrSender = attrCon.SetAsSource(sourceObj)
    
        custAttrs = [
            "FirstPersonCamPivotAttrs_DOF_MaxX",
            "FirstPersonCamPivotAttrs_DOF_MaxY",
            "FirstPersonCamPivotAttrs_DOF_MaxZ",
            "FirstPersonCamPivotAttrs_DOF_MinX",
            "FirstPersonCamPivotAttrs_DOF_MinY",
            "FirstPersonCamPivotAttrs_DOF_MinZ",
            "FirstPersonCamPivotAttrs_DOF_FOV"
        ]
    
        for targetObj in targetObjs:
    
            attrReceiver = attrCon.ConstrainObject(targetObj)
            attrCon.SetBoxPosition( attrReceiver, 600, 100 )            
    
            for attrName in custAttrs:    
                RS.Utils.Scene.ConnectBox(attrSender, attrName, attrReceiver, attrName)            
    
        attrCon.Active = True

    else:
        print "no namespace found for function [linkDofAttrs]"
        
        
def configureControlSystem(lNameSpace):
    if lNameSpace:
        # first off make a handle for the ConeRoot node
        coneRootNode = None
        coneRootNodeHandle = None
        coneRootNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":ConeRoot", True) 

        # now what I need to do is make a new null which is a child of moverNode & translated 
        # to moverNode position but rotated to match skelHeadZero
        moverNode = None
        moverZero = None
        moverNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":mover", True)
           
        moverZero = FBModelNull(lNameSpace+":"+"moverZero")

        moverZero.Parent = moverNode
        FBSystem().Scene.Evaluate()

        tVec = FBVector3d()
        if moverNode:
            moverNode.GetVector(tVec, FBModelTransformationType.kModelTranslation, True)    
            moverZero.SetVector(tVec, FBModelTransformationType.kModelTranslation, True)   

        rVec = FBVector3d(90,0,180) ##this is because the animations are fucking stupid and once they are in motion they go in the opposite dir to the mover
        moverZero.SetVector(rVec, FBModelTransformationType.kModelRotation, False)   

        FBSystem().Scene.Evaluate()     

        # now need to make a root for the coneRootNode
        coneZero = None
        coneMaster = None
        skelHeadZero = None
        cameraOffsetNode = None
        cameraHelperNode = None
        chLHelperNode = None
        chRHelperNode = None
        pivotNodeName = "FirstPersonCamPivot"
        coneFollowNode = None 
        coneZero = FBModelNull(lNameSpace+":coneZero")
        coneMaster = FBModelNull(lNameSpace+":coneMaster")
        skelHeadZero = FBModelNull(lNameSpace+":skelHeadZero")
        cameraOffsetNode = FBModelNull(lNameSpace+":cameraOffset")
        cameraHelperNode = FBModelNull(lNameSpace+":cameraHelper")
        chLHelperNode = FBModelNull(lNameSpace+":CH_L_Hand")
        chRHelperNode = FBModelNull(lNameSpace+":CH_R_Hand")        
        coneFollowNode = FBModelNull(lNameSpace+":coneFollow")      
        pivotNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":" + pivotNodeName, True)
        lHand = RS.Utils.Scene.FindModelByName(lNameSpace + ":SKEL_L_Hand", True)
        rHand = RS.Utils.Scene.FindModelByName(lNameSpace + ":SKEL_R_Hand", True)
        skelHead = RS.Utils.Scene.FindModelByName(lNameSpace + ":SKEL_Head", True)


        tVect = FBVector3d()
        rVect = FBVector3d()
        pivotNode.GetVector(tVect, FBModelTransformationType.kModelTranslation, True)  
        pivotNode.GetVector(rVect, FBModelTransformationType.kModelRotation, True)      
        coneZero.SetVector(tVect, FBModelTransformationType.kModelTranslation, True)  
        coneMaster.SetVector(tVect, FBModelTransformationType.kModelTranslation, True)  

        skelHeadZero.Parent = skelHead

        FBSystem().Scene.Evaluate()

        rVect = FBVector3d()
        tVect = FBVector3d()
        coneZero.GetVector(rVect, FBModelTransformationType.kModelRotation, True)
        coneZero.GetVector(tVect, FBModelTransformationType.kModelTranslation, True)

        skelNeck = RS.Utils.Scene.FindModelByName(lNameSpace + ":SKEL_Neck_1", True)
        tVect = FBVector3d()
        rVect = FBVector3d()
        skelNeck.GetVector(rVect, FBModelTransformationType.kModelRotation, True)
        skelNeck.GetVector(tVect, FBModelTransformationType.kModelTranslation, True)

        coneFollowNode.SetVector(rVect, FBModelTransformationType.kModelRotation, True)
        coneFollowNode.SetVector(tVect, FBModelTransformationType.kModelTranslation, True)

        FBSystem().Scene.Evaluate()

        rVect = FBVector3d(0,0,0)
        skelHeadZero.SetVector(rVect, FBModelTransformationType.kModelRotation, True)

        tVect = FBVector3d(0,0,0)
        skelHeadZero.SetVector(tVect, FBModelTransformationType.kModelTranslation, False)

        FBSystem().Scene.Evaluate()

        print "ConeRootNode: "+coneRootNode.LongName
        coneMaster.Parent = coneZero ##had top add this due to setting transforms on coneRootNode directly dont seem to work



        # THIS SETS UP THE CAMERA OFFSET AND THE CH HELPERS
        cameraOffsetNode.Parent = coneMaster
        coneRootNode.Parent = cameraOffsetNode    
        cameraOffsetNode.SetVector(rVect, FBModelTransformationType.kModelRotation, False)
        cameraOffsetNode.SetVector(rVect, FBModelTransformationType.kModelTranslation, False)


        FBSystem().Scene.Evaluate()


        # add properties to coneMaster
        custAttrs = [
            "FirstPersonCamPivotAttrs_DOF_MaxX",
            "FirstPersonCamPivotAttrs_DOF_MaxY",
            "FirstPersonCamPivotAttrs_DOF_MaxZ",
            "FirstPersonCamPivotAttrs_DOF_MinX",
            "FirstPersonCamPivotAttrs_DOF_MinY",
            "FirstPersonCamPivotAttrs_DOF_MinZ",
            "FirstPersonCamPivotAttrs_DOF_FOV",
            "RotationDominance",
            "OffsetX",
            "OffsetY",
            "OffsetZ"
        ]
        for attrName in custAttrs:   
            prop = coneMaster.PropertyCreate( attrName, FBPropertyType.kFBPT_float, 'Number', True, True, None )                      
            if prop == None:
                print "NoneObj:"+attrName+","+str(FBPropertyType.kFBPT_float)
            else:
                print prop.GetName()+prop.GetPropertyTypeName()    

            lProp = coneMaster.PropertyList.Find(attrName)

            if attrName[29:33] == "MaxX":
                print attrName[29:32]
                lProp.SetMin(0)
                lProp.SetMax(180)
                lProp.Data = 45
                lProp.SetAnimated(True)
            else:
                if attrName[29:33] == "MinX":
                    lProp.SetMin(-180)
                    lProp.SetMax(0)
                    lProp.Data = -45
                    lProp.SetAnimated(True)           
                else:
                    if attrName[29:33] == "MaxY":
                        lProp.SetMin(0)
                        lProp.SetMax(180)
                        lProp.Data = 60
                        lProp.SetAnimated(True)            
                    else:
                        if attrName[29:33] == "MinY":
                            lProp.SetMin(-180)
                            lProp.SetMax(0)
                            lProp.Data = -60
                            lProp.SetAnimated(True)           
                        else:
                            if attrName == "RotationDominance":
                                lProp.SetMin(-1)
                                lProp.SetMax(1)
                                lProp.Data = 1
                                lProp.SetAnimated(True)
                            else:
                                if attrName[29:32] == "FOV":
                                    lProp.SetMin(0)
                                    lProp.SetMax(100)
                                    lProp.Data = 45
                                    lProp.SetAnimated(True)
                                else:
                                    lProp.SetMin(-50)
                                    lProp.SetMax(50)
                                    if attrName == "OffsetX":
                                        lProp.Data = 0
                                        lProp.SetAnimated(True) 
                                    else:
                                        if attrName == "OffsetY" or attrName == "OffsetZ":
                                            lProp.Data = 5
                                            lProp.SetAnimated(True) 
                                        else:
                                            lProp.Data = 0
                                            lProp.SetAnimated(True)                                           



        # now we need to offset the rotation of the coneZero Node
        rVect = FBVector3d(0,0,0)
        coneZero.SetVector(rVect, FBModelTransformationType.kModelRotation, False)          
        FBSystem().Scene.Evaluate()

        coneZero.Parent = coneFollowNode
        FBSystem().Scene.Evaluate()    

        # now we need to ParentConstrain the coneFollowNode node to the skelNeck
        rigFollowCon = FBConstraintManager().TypeCreateConstraint(3)
        rigFollowCon.LongName = lNameSpace+":"+"camToJoystick"
        rigFollowCon.ReferenceAdd(0,coneFollowNode)
        rigFollowCon.ReferenceAdd(1,skelNeck)
        rigFollowCon.Snap()


        #now we need to relation constrain the attrs together (because having the attrs on the pivot node inside the hierarchy means they dont export
        conName = "DOF_Attrs_Connections"
        sourceObj = coneMaster
        coneRoot = None
        pivotAttrs = None
        coneRoot = RS.Utils.Scene.FindModelByName(lNameSpace + ":ConeRoot", True)
        pivotAttrs = RS.Utils.Scene.FindModelByName(lNameSpace + ":FirstPersonCamPivotAttrs", True)

        targetObjs = [
            pivotAttrs,
            pivotNode,
            coneRoot
        ]

        linkDofAttrs(conName, sourceObj, targetObjs, lNameSpace)

        pivotAttrs.Parent = coneMaster

    # NOW WE need to setup the offsets for the cameraOffset Node
        camOffsetCon = None

        CamPivotNode = RS.Utils.Scene.FindModelByName( lNameSpace + ":FirstPersonCamPivot", True) 

        camOffsetCon = FBConstraintRelation(lNameSpace+":"+'camOffset')

        camOffsetSender = camOffsetCon.SetAsSource(coneMaster)
        camOffsetSender.UseGlobalTransforms = False
        camOffsetCon.SetBoxPosition(camOffsetSender, -200, 400)
        multX = camOffsetCon.CreateFunctionBox("Number", "Multiply (a x b)")
        multX_B = RS.Utils.Scene.FindAnimationNode(multX.AnimationNodeInGet(),'b')
        multX_B.WriteData([-1])

        numToVec = camOffsetCon.CreateFunctionBox( 'Converters', 'Number to Vector' )
        camOffsetCon.SetBoxPosition(numToVec, 2750, 650 )          

        receiverObj = camOffsetCon.ConstrainObject(cameraOffsetNode)
        receiverObj.UseGlobalTransforms = False
        camOffsetCon.SetBoxPosition(receiverObj, 3200, 500)   

        RS.Utils.Scene.ConnectBox(camOffsetSender, "OffsetX", multX, "a")
        RS.Utils.Scene.ConnectBox(multX, "Result", numToVec, "X")
        RS.Utils.Scene.ConnectBox(camOffsetSender, "OffsetY", numToVec, "Z")
        RS.Utils.Scene.ConnectBox(camOffsetSender, "OffsetZ", numToVec, "Y")                

        RS.Utils.Scene.ConnectBox(numToVec, "Result", receiverObj, "Lcl Translation")    
        camOffsetCon.Active = True    

        # WE ALSO CREATE THE CONSTRAINT TO DRIVE THE CAMPIVOT TRANSLATION USING CAMERAOFFSET
        OffsetToCamPivot = None

        OffsetToCamPivot = FBConstraintRelation(lNameSpace+':Offset_CamPivot')
        ConeMasterNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":coneMaster", True)

        SenderObj = OffsetToCamPivot.SetAsSource(ConeMasterNode)
        SenderObj.UseGlobalTransforms = False
        OffsetToCamPivot.SetBoxPosition(SenderObj, -200, 400)       

        receiverObj = OffsetToCamPivot.ConstrainObject(CamPivotNode)
        receiverObj.UseGlobalTransforms = False
        OffsetToCamPivot.SetBoxPosition(receiverObj, 3200, 500)   

        ##VectToNumber =  OffsetToCamPivot.CreateFunctionBox('Converters', 'Vector to Number')
        ##OffsetToCamPivot.SetBoxPosition(VectToNumber, 2750, 650 )

        multX = OffsetToCamPivot.CreateFunctionBox("Number", "Multiply (a x b)")
        OffsetToCamPivot.SetBoxPosition(multX, 1400, 250 ) 

        multY = OffsetToCamPivot.CreateFunctionBox("Number", "Multiply (a x b)")
        OffsetToCamPivot.SetBoxPosition(multY, 1400, 250 ) 

        multZ = OffsetToCamPivot.CreateFunctionBox("Number", "Multiply (a x b)")
        OffsetToCamPivot.SetBoxPosition(multZ, 1400, 250 )     

        numToVec = OffsetToCamPivot.CreateFunctionBox( 'Converters', 'Number to Vector' )
        OffsetToCamPivot.SetBoxPosition(numToVec, 2750, 650 ) 

        RS.Utils.Scene.ConnectBox(SenderObj, "OffsetX", multX, "a")
        multX_B = RS.Utils.Scene.FindAnimationNode(multX.AnimationNodeInGet(),'b')
        multX_B.WriteData([0.01]) 
        RS.Utils.Scene.ConnectBox(multX, "Result", numToVec, "X")

        RS.Utils.Scene.ConnectBox(SenderObj, "OffsetY", multY, "a")
        multY_B = RS.Utils.Scene.FindAnimationNode(multY.AnimationNodeInGet(),'b')
        multY_B.WriteData([0.01])    
        RS.Utils.Scene.ConnectBox(multY, "Result", numToVec, "Y")

        RS.Utils.Scene.ConnectBox(SenderObj, "OffsetZ", multZ, "a")
        multZ_B = RS.Utils.Scene.FindAnimationNode(multZ.AnimationNodeInGet(),'b')
        multZ_B.WriteData([0.01])    
        RS.Utils.Scene.ConnectBox(multZ, "Result", numToVec, "Z")


        RS.Utils.Scene.ConnectBox(numToVec, "Result", receiverObj, "Lcl Translation")    
        ##now turn on the constraint
        OffsetToCamPivot.Active = True  

        # NOW WE NEED TO CREATE A PARENT/CHILD CONSTRAINT BETWEEN THE CH_L/R_HAND TO THE CAMERAHELPER
        cameraHelperNode.Rotation.Data = cameraOffsetNode.Rotation.Data + FBVector3d(90,0,180)
        lVector = FBVector3d()
        cameraOffsetNode.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
        cameraHelperNode.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
        cameraHelperNode.Parent = cameraOffsetNode

        # Now parent the CH_L_Hand and CH_R_Hand to the cameraHelper node
        chLHelperNode.Parent = lHand   
        chRHelperNode.Parent = rHand

        chLHandCon = FBConstraintManager().TypeCreateConstraint(3)
        chRHandCon = FBConstraintManager().TypeCreateConstraint(3)
        if lNameSpace != None:
            chLHandCon.LongName = (lNameSpace+":"+"CameraHelper_L_Constraint")
            chRHandCon.LongName = (lNameSpace+":"+"CameraHelper_R_Constraint")
        else:
            chLHandCon.LongName = ("CameraHelper_L_Constraint")
            chRHandCon.LongName = ("CameraHelper_R_Constraint")
        chLHandCon.ReferenceAdd (0,chLHelperNode)
        chRHandCon.ReferenceAdd (0,chRHelperNode)

        chLHandCon.ReferenceAdd (1,cameraHelperNode)
        chRHandCon.ReferenceAdd (1,cameraHelperNode)             

        chLHandCon.Active = True
        chRHandCon.Active = True

        chLHandCon.Lock = True
        chRHandCon.Lock = True       

        FBSystem().Scene.Evaluate()

        #now we need to add a constraint to constrain the coneZero between the skelHeadZero rotation and the mover rotation.
        #WHEN THE ATTRS IS 0 THEN MOVER HAS DOMINANCE
        #WHEN THE ATTRS IS 100 THN CAMZER HAS DOMINANCE (WHICH SHOULD BE A CHILD OF HEAD
        rotConstA = FBConstraintManager().TypeCreateConstraint(10)
        rotConstA.LongName = (lNameSpace+":"+"CamZero_Rot_Dominance")
        rotConstA.ReferenceAdd (0,coneZero)
        rotConstA.ReferenceAdd (1,skelHeadZero)            
        rotConstA.Active = True
        rotConstA.Lock = True             

        rotConstB = FBConstraintManager().TypeCreateConstraint(10)
        rotConstB.LongName = (lNameSpace+":"+"Mover_Rot_Dominance")
        rotConstB.ReferenceAdd (0,coneZero)
        rotConstB.ReferenceAdd (1,moverZero)      
        rotConstB.Active = True
        rotConstB.Lock = True 

        rotDominanceProperty = coneMaster.PropertyList.Find("RotationDominance")

        for prop in rotConstA.PropertyList:
            if prop.Name == "Weight":
                prop.SetAnimated(True)

        for prop in rotConstB.PropertyList:
            if prop.Name == "Weight":
                prop.SetAnimated(True)

        # now we will build the actual relation constraint
        weightRelationCon = None
        weightRelationCon = FBConstraintRelation(lNameSpace+":"+'Dominance')     

        weightSender = weightRelationCon.SetAsSource(coneMaster)
        weightSender.UseGlobalTransforms = False    
        weightRelationCon.SetBoxPosition(weightRelationCon, 0, 150)    

        addA = weightRelationCon.CreateFunctionBox("Number", "Add (a + b)")
        weightRelationCon.SetBoxPosition(addA, 1000, 50 )     

        subA = weightRelationCon.CreateFunctionBox("Number", "Subtract (a - b)")
        weightRelationCon.SetBoxPosition(subA, 1000, 400 )     

        multA = weightRelationCon.CreateFunctionBox("Number", "Multiply (a x b)")
        weightRelationCon.SetBoxPosition(multA, 1400, 250 )     

        multB = weightRelationCon.CreateFunctionBox("Number", "Multiply (a x b)")
        weightRelationCon.SetBoxPosition(multB, 1400, 500 )     

        multC = weightRelationCon.CreateFunctionBox("Number", "Multiply (a x b)")
        weightRelationCon.SetBoxPosition(multC, 2000, 550 )     

        receiverA = weightRelationCon.ConstrainObject(rotConstA)
        weightRelationCon.SetBoxPosition(receiverA, 1800, 150)       

        receiverB = weightRelationCon.ConstrainObject(rotConstB)
        weightRelationCon.SetBoxPosition(receiverB, 2400, 500)   

        RS.Utils.Scene.ConnectBox(weightSender, "RotationDominance", addA, "a")   
        lAddA_B = RS.Utils.Scene.FindAnimationNode(addA.AnimationNodeInGet(),'b')
        lAddA_B.WriteData([1])                       
        RS.Utils.Scene.ConnectBox(addA, "Result", multA, "a")                

        RS.Utils.Scene.ConnectBox(weightSender, "RotationDominance", subA, "a")            
        subA_b = RS.Utils.Scene.FindAnimationNode(subA.AnimationNodeInGet(),'b')
        subA_b.WriteData([1])                    
        RS.Utils.Scene.ConnectBox(subA, "Result", multB, "a")   

        multA_b = RS.Utils.Scene.FindAnimationNode(multA.AnimationNodeInGet(),'b')
        multA_b.WriteData([50])
        RS.Utils.Scene.ConnectBox(multA, "Result", receiverA, "Weight")

        multB_b = RS.Utils.Scene.FindAnimationNode(multB.AnimationNodeInGet(),'b')
        multB_b.WriteData([-1])
        RS.Utils.Scene.ConnectBox(multB, "Result", multC, "a")

        multC_b = RS.Utils.Scene.FindAnimationNode(multC.AnimationNodeInGet(),'b')
        multC_b.WriteData([50])
        RS.Utils.Scene.ConnectBox(multC, "Result", receiverB, "Weight")

        weightRelationCon.Active = True   

    else:
        print "no namespace found for function [configureControlSystem]"
        
        
def enableDofProperties(obj):

    propList = [
        "DOF_MinX",
        "DOF_MinY",
        "DOF_MinZ",       
        "DOF_MaxX",    
        "DOF_MaxY",
        "DOF_MaxZ",
        "DOF_FOV"
                ]
    propManager = obj.PropertyList

    for propName in propList:
        dofProp = propManager.Find(propName)

        if dofProp != None:
            dofProp.Name = "FirstPersonCamPivotAttrs_"+propName
            dofProp.SetAnimated(True)


def createRuntimeNodes(lNameSpace):
    if lNameSpace:

        pivotNodeName = "FirstPersonCamPivot"
    
        pivotNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":" + pivotNodeName, True)
    
        if pivotNode:
            print 'FPS exists'
        else:
            fbxFilePath = "x:\\gta5\\art\\ng\\animation\\resources\\characters\\Models\\ingame\\firstPersonCamData.FBX"
            
            if os.path.exists(fbxFilePath):            
                RS.Perforce.Sync(fbxFilePath)
        
                '''add file merge options here'''
                options = FBFbxOptions( True, fbxFilePath )    
                options.BaseCameras = False
                options.NamespaceList = "PlaceholderNamespace"
                options.CameraSwitcherSettings = False
                options.CurrentCameraSettings = False
                options.GlobalLightingSettings = False
                options.TransportSettings = False     
        
                # Disable merging of all import file takes 
                for takeIndex in range( 0, options.GetTakeCount() ):
                    options.SetTakeSelect( takeIndex, False )    
            
                FBApplication().FileMerge( fbxFilePath, False, options )
        
                FBSystem().Scene.Evaluate()   
                
                mergeItemList = []
                # find newly merged fps items and add to a list
                for i in RS.Globals.gComponents:
                    if i.LongName.startswith( "PlaceholderNamespace" ):
                        if i.ClassName() in [ 'FBModelNull', 'FBModel', 'FBModelSkeleton', 'FBMaterial', 'FBTexture', 'FBVideoClipImage', 'FBCharacter', 'FBGroup', 'FBMesh', 'FBModelRoot', 'FBDevice', 'FBNote', 'FBStoryTrack', 'FBConstraintRelation', 'FBConstraint', 'FBFolder', 'FBSet', 'FBShader', 'FBHandle', 'FBCamera' ]:
                            mergeItemList.append( i )            
                
                if mergeItemList > 0:
                    for item in mergeItemList:
                        # remove placeholder namespace
                        item.ProcessObjectNamespace( FBNamespaceAction.kFBRemoveAllNamespace, '', '' )
            
                        # add new name
                        item.LongName = lNameSpace + ':' + item.Name
        
                # now we need to orient a load of the shit we merged in.
                # FirstPersonCam_Pivot -> Aligned/Parented to SKEL_ROOT
                fpsCamPivotNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":FirstPersonCamPivot", True) 
                skelRoot = RS.Utils.Scene.FindModelByName(lNameSpace + ":SKEL_ROOT", True)  
                fpsCamPivotNode.Parent = skelRoot
        
                rVect = FBVector3d(180,180,-180)
                fpsCamPivotNode.SetVector(rVect, FBModelTransformationType.kModelRotation, False) ##False flag make it set in local space
                enableDofProperties(fpsCamPivotNode)
        
                # FirstPersonCam_Pivot_MoverSpace -> Aligned/Parented to Mover
                fpsCamPivotMoverSpaceNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":FirstPersonCamPivot_MoverSpace", True) 
                mover = RS.Utils.Scene.FindModelByName(lNameSpace + ":mover", True)  
        
                fpsCamPivotMoverSpaceNode.Parent = mover
        
                rVect = FBVector3d(180,180,-180)
                enableDofProperties(fpsCamPivotMoverSpaceNode)       
        
                # Properties to the ConeRoot
                coneRootNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":ConeRoot", True) 
                enableDofProperties(coneRootNode)
                FirstPersonCamPivotAttrsNode = RS.Utils.Scene.FindModelByName(lNameSpace + ":FirstPersonCamPivotAttrs", True) 
                enableDofProperties(FirstPersonCamPivotAttrsNode) 
            
            else:
                FBMessageBox( 'R* Error', 'Unable to find file:\n\n{0}'.format( fbxFilePath ), 'Ok' )

    else:
        print "no namespace found for function [createRuntimeNodes]"
        
        
def removeV2FPSRig(lNameSpace):
    if lNameSpace:

        oldComponentListV2 = [
            # objects
            "ConeGeo",
            "coneMaster",
            "FirstPersonCamPivot",
            "FirstPersonCamPivot_MoverSpace",
            "coneZero",
            "ConeRoot",
            "moverZero",
            "skelHeadZero",
            "cameraOffset",
            "cameraHelper",
            "coneFollow",
            "FPS_Cam",
            "Preview_Handle",
            "FPS_Preview_Cam",
            # constraints
            "DOFCone_Relation",
            "CameraLock",
            "camToJoystick",
            "DOF_Attrs_Connections",
            "camOffset",
            "limit_firstPersonCamPivot",
            "CamZero_Rot_Dominance",
            "Mover_Rot_Dominance",
            "Dominance",
            "RigToJoint",
            "Limits_Preview_Cam",
            "FOV_Relation",
            "Relation" ,
            "Offset_CamPivot",
            "CameraHelper_L_Constraint",
            "CameraHelper_R_Constraint",
            "MoverToRoot_CamPivot"
                            ]
    
        deleteListV2 = []
    
        for component in RS.Globals.Components:
            for item in oldComponentListV2:
                if component.LongName == lNameSpace + ':' + item:
                    deleteListV2.append( component )
                    
        for i in deleteListV2:
            try:
                i.FBDelete()
            except:
                pass



def removeOldFirstPersonSetup( lNameSpace ):

    if lNameSpace:
        print "Attempting to remove existing First Person camera setup for "+lNameSpace
    
        oldComponentList = [
            # objects
            "ConeMid",
            "coneFollow",
            "FirstPersonCone",
            "FirstPersonCamPivot",
            "FirstPersonCamPivot_MoverSpace",
            "CamRoot",
            "firstPersonCamZero",
            "cameraPivotCam",
            "cameraPivotHandle",
            "FirstPersonCone",
            "RECT_MoverWeighting",
            "TEXT_MoverWeighting",
            "CTRL_MoverWeighting",
            "moverZero",
            "ConeMaxY",
            "Helper_00_MaxY",
            "Helper_01_MaxY",
            "ConeMinY",
            "Helper_00_MinY",
            "Helper_01_MinY",        
            "ConeMinX",
            "Helper_00_MinX",
            "Helper_01_MinX",    
            "ConeMaxX",
            "Helper_00_MaxX",
            "Helper_01_MaxX",
            "ConeMaxZ",
            "ConeMinZ",    
            "RectParent",
            "FirstPersonCamPivotAttrs",
            "CamRoot",
            "PivotParent",
            # constraints
            "ConeFOV",
            "Cone_MinX_Relation",
            "Cone_MinY_Relation",
            "Cone_MaxX_Relation",
            "Cone_MaxY_Relation",
            "limit_firstPersonCamPivot",
            "CamZero_Rot_Dominance",
            "Mover_Rot_Dominance",
            "Dominance",
            "LimitJs",
            "CameraLock",
            "camToJoystick",
            "DOFAttrs",
            "camRootToNeckConstraint",
            "DEOF_Relation",
            "Relation"
                        ]    
    
        deleteList = []
    
        for component in RS.Globals.Components:
            for item in oldComponentList:
                if component.LongName == lNameSpace + ':' + item:
                    deleteList.append( component )
                    
        for i in deleteList:
            try:
                i.FBDelete()
            except:
                pass
        
        
def initialiseFPS_Cam_Setup( characterList ):
    # ASK IF THE USER WANTS TO SAVE THE FILE
    # MERGING CAMERA FROM ANOTHER SCENE CAN WIPE UNSAVED CHANGES TO THE EXPORTER MARKUP.
    result = FBMessageBox("Save File", "Do you want to save the file? \nThis script can wipe unsaved changes to the markup", "Yes", "No", "Cancel")

    if result == 1:
        if FBApplication().FileSave(FBApplication().FBXFileName) == False:
            return False       
    else:
        if result == 3:
            return False

    for i in RS.Globals.Characters:
        for characterName in characterList:
            print i.LongName, characterName
            if i.LongName == characterName:
                character = i
                splitName = character.LongName.split(":")
                lNameSpace = splitName[0]        
                print 'Namespace is: ', lNameSpace
            
                # Checks if the character is already added to the exporter.
                # Select selected character DUMMY_01
                characterDummy = RS.Utils.Scene.FindModelByName(lNameSpace + ":Dummy01", True)
        
                # Puts the character in stance mode
                originalInputType = character.InputType
                character.InputType = FBCharacterInputType.kFBCharacterInputStance
                character.ActiveInput = True 
                
                # delete old rigs
                removeOldFirstPersonSetup( lNameSpace )                   

                # now try and run the removal of any preconfigured v2 rigs
                removeV2FPSRig( lNameSpace )
        
                if createRuntimeNodes(lNameSpace) == False:
                    return False
        
                configureControlSystem(lNameSpace)
        
                createGroupCamMaster(lNameSpace)
        
                driveGameJointByRig(lNameSpace)
        
                # Add CH bones to the Character Extensions
                rs_CharacterExtension(character)
        
                # need to also configure the attributes in the exporter
                addAttributesToExporter(lNameSpace)
        
                # Parent the rig to mover
                mover = FBFindModelByLabelName(lNameSpace + ':mover')
                for rigComponentName in [':coneFollow',':ConeGeo']:
                    rigComponent = FBFindModelByLabelName(lNameSpace + rigComponentName)
                    if rigComponent:
                        rigComponent.Parent = mover
        
                # Puts the character back in Control Rig mode
                character.InputType = originalInputType
                character.ActiveInput = True