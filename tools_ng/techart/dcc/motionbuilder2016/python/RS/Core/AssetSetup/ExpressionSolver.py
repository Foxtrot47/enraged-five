
import pyfbsdk as mobu
import os
from RS import Config, Globals, Perforce
from RS.Utils import Path
from RS.Core.Expression import expressionSolver as expr
from PyExprSolver import RSExprSolver


def detectSolverAssets():
    # Detects the solver expression files for the current character
    boneList = None
    skeletonFile = None
    expressionsList = []
    solverData = None
    
    solverAssetsPath = os.path.join(
        Config.Project.Path.Root,
        "art",
        "animation",
        "resources",
        "characters",
        "solver_assets")
    
    Perforce.Sync(["-f", os.path.join(solverAssetsPath, "...")])
    
    fileName = Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName)
    
    characterSolverPath = os.path.join(solverAssetsPath, fileName)
    
    if os.path.exists(characterSolverPath):
        expressionsPath = os.path.join(characterSolverPath, "expressions")
        if os.path.exists(expressionsPath):
            for file in os.listdir(expressionsPath):
                if file.endswith(".expr"):
                    expressionFile = (os.path.join(expressionsPath, file))
                    if expressionFile:
                        expressionsList.append(expressionFile)
        sharedExpressionsPath = os.path.join(solverAssetsPath, "shared", "expressions")
        if os.path.exists(sharedExpressionsPath):
            for file in os.listdir(sharedExpressionsPath):
                if file.endswith(".expr"):
                    expressionFile = (os.path.join(sharedExpressionsPath, file))
                    if expressionFile:
                        expressionsList.append(expressionFile)
        skeletonPath = os.path.join(characterSolverPath, "skeleton")
        if os.path.exists(skeletonPath):
            for file in os.listdir(skeletonPath):
                if file.endswith(".skelset"):
                    skeletonFile = (os.path.join(skeletonPath, file))
        boneListPath = os.path.join(solverAssetsPath, "masterlists")
        for file in os.listdir(boneListPath):
            if file.endswith(".bonelist"):
                boneList = os.path.join(boneListPath, file)
        if skeletonFile is not None:
            solverData = [boneList,
                          skeletonFile,
                          expressionsList]
    
    return solverData

def facialSolverSetup(solverAssets):
    fileName = Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName)
    
    constraint = mobu.FBCreateObject("Browsing/Templates/Constraints", "RS Expression Solver", "RS Expression Solver")
    constraint.LongName = "{0}_ExprSolver".format(fileName)
    #constraintList.append(constraint)
    Globals.Constraints.append(constraint)

    # Create the solver in Mobu.
    solver = RSExprSolver(constraint.LongName)
    
    # set scale to Constraint properties
    solver.SetScale(1.0)
   
    solver.SetSkel(str(solverAssets[1]))
    
    for exprFile in solverAssets[2]:
        solver.AddExpressionFile(str(exprFile))
    solver.Reset()