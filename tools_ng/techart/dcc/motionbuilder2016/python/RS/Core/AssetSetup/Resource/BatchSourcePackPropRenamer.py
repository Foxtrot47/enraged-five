"""
Renamer for props that need the pack prefix  - For Batching
"""

import pyfbsdk as mobu

import RS.Globals


def ValidComponentToRename(component):
    """
    Returns if a component is valid

    Arguments:
        component (FBComponent): component to check if it should be renamed
    """
    refProperty = component.PropertyList.Find('rs_Type')
    if refProperty:
        if refProperty.Data in ['rs_Bone', 'rs_Mesh', 'rs_Contacts']:
            if 'mover_Control' in component.Name or 'ToyBox Marker' in component.Name:
                return False
    return True


def Run():
    """ Renames props that need the pack prefix and adds the fbx path to the dummy components"""
    packList = ['agt_']

    application = mobu.FBApplication()
    currentFilePath = application.FBXFileName    
    
    # find appropriate nulls to rename
    for component in RS.Globals.gComponents:
        if ValidComponentToRename(component):
            # rename component
            component.Name = component.Name.split(packList[0])[-1]

        # update asset path in Dummy node
        if component.Name == 'Dummy01' and component.PropertyList.Find('Asset_Path'):
            component.PropertyList.Find('Asset_Path').Data = currentFilePath

    mobu.FBApplication().FileSave(currentFilePath)