from RS.Tools import UI
import os
import pyfbsdk as mobu
from pyfbsdk_additions import *
import RS.Config
from PySide import QtCore, QtGui
from RS import Globals, Utils

ICONSROOT = RS.Config.Script.Path.ToolImages
FILEICON = os.path.join(ICONSROOT, "ReferenceEditor", "Folder.png")
MAXROOT = os.path.join(RS.Config.Project.Path.Art, "animation", "resources", "sets", "Max_Files")

def EntitySets():
    '''
    Opens the Entity Sets UI.
    '''
    tool = EntitySetsUI()
    tool.show()

class EntitySetsUI(UI.QtMainWindowBase):
    global entitySetGroups
    entitySetGroups = {}
    
    def __init__(self):

        super(EntitySetsUI, self).__init__(None, title='Entity Sets', size=(300, 100), dockable=False, store=True)
        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QGridLayout()
        mainWidget = QtGui.QWidget()
        self.selFileGroupBox = QtGui.QGroupBox("File Selection")
        self.selectFileLayout = QtGui.QGridLayout(self.selFileGroupBox)
        self.selSetGroupBox = QtGui.QGroupBox("Select Groups to Create:")
        self.selectSetsLayout = QtGui.QGridLayout(self.selSetGroupBox)
        
        # Select file section
        self.filePath = MAXROOT
        self.selectedFileText = QtGui.QLabel(self.filePath)
        self.selectedFileText.setFrameStyle(QtGui.QFrame.Panel | QtGui.QFrame.Sunken)
        self.selectedFileText.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        self.selectedFileText.setEnabled(False)
        
        self.selectFileButton = QtGui.QPushButton()
        self.fileIcon = QtGui.QIcon(FILEICON)
        self.selectFileButton.setIcon(self.fileIcon)
        self.selectFileButton.setMaximumWidth(30)
        self.selectFileButton.clicked.connect(self.selectFile)
        
        # Select entity sets section
        self.groupSelectWidget = QtGui.QListView()
        self.groupSelectWidget.setAutoFillBackground(True)
        colorPalette = self.groupSelectWidget.palette()
        colorPalette.setColor(self.groupSelectWidget.backgroundRole(), QtCore.Qt.white)
        self.groupSelectWidget.setPalette(colorPalette)
        self.groupListModel = QtGui.QStandardItemModel()
        self.groupSelectWidget.setModel(self.groupListModel)
        self.selectAllButton = QtGui.QPushButton("Select All")
        self.clearAllButton = QtGui.QPushButton("Clear All")
        self.selectAllButton.clicked.connect(self.selectAll)
        self.clearAllButton.clicked.connect(self.clearAll)
        
        # Create button
        self.createGroupsButton = QtGui.QPushButton("Create Groups")
        self.createGroupsButton.clicked.connect(self.setupSelectedGroups)
        
        # Populate sublayouts
        self.selectFileLayout.addWidget(self.selectedFileText, 0, 0, 1, 1, stretch=1)
        self.selectFileLayout.addWidget(self.selectFileButton, 0, 2, 1, 1)
        self.selectSetsLayout.addWidget(self.groupSelectWidget)
        self.selectSetsLayout.addWidget(self.selectAllButton)
        self.selectSetsLayout.addWidget(self.clearAllButton)
        
        # Populate main layout
        mainLayout.addWidget(self.selFileGroupBox, 1, 0, 1, 1)
        mainLayout.addWidget(self.selSetGroupBox, 2, 0, 1, 1)
        mainLayout.addWidget(self.createGroupsButton, 3, 0, 1, 1)
        
        mainWidget.setLayout(mainLayout)
        self.setCentralWidget(mainWidget)
        
        # Disable select sets and create until a file is selected
        self.selSetGroupBox.setEnabled(False)
        self.createGroupsButton.setEnabled(False)
    
    def selectAll(self):
        for index in xrange(self.groupListModel.rowCount()):
            currentIndex = self.groupListModel.index(index, 0)
            currentItem = self.groupListModel.itemFromIndex(currentIndex)
            if currentItem.checkState() == QtCore.Qt.CheckState.Unchecked:
                currentItem.setCheckState(QtCore.Qt.CheckState.Checked)
    
    def clearAll(self):
        for index in xrange(self.groupListModel.rowCount()):
            currentIndex = self.groupListModel.index(index, 0)
            currentItem = self.groupListModel.itemFromIndex(currentIndex)
            if currentItem.checkState() == QtCore.Qt.CheckState.Checked:
                currentItem.setCheckState(QtCore.Qt.CheckState.Unchecked)
    
    def selectFile(self):
        for index in xrange(self.groupListModel.rowCount()):
            self.groupListModel.removeRows(index, self.groupListModel.rowCount())
        global entitySetGroups
        app = mobu.FBApplication()
        entitySetDataPopup = mobu.FBFilePopup()
        #Set default path to look in
        entitySetDataPopup.Path = r"X:\gta5\art\animation\resources\sets\Max_Files"
        entitySetDataPopup.Filter = '*.txt'
        entitySetDataPopup.Style = mobu.FBFilePopupStyle.kFBFilePopupOpen
        
        entitySetsToIgnore = ["0",
                              "geometry",
                              "props",
                              "milo_proxy",
                              "not_exported",
                              "edgeblends",
                              "rsrefs"
                              ]
        
        # If we don't cancel the file read, we'll look for the locations
        if entitySetDataPopup.Execute():
            entitySetGroups = {}
            filePath = entitySetDataPopup.FullFilename
            self.selectedFileText.setText(filePath)
            self.selectedFileText.setEnabled(True)
            entitySetData = open(filePath)
            for line in entitySetData:
                if "ENTITY SET START>> " in line:
                    entitySetName = line.split("ENTITY SET START>> ")[1]
                    entitySetName = entitySetName.split(" <<ENTITY SET END")[0]
                    if entitySetName.lower() not in entitySetsToIgnore:
                        if entitySetName not in entitySetGroups:
                            entitySetGroups[entitySetName] = []
                else:
                    entitySetGroup = line.split(">>")[0]
                    entitySetGroup = entitySetGroup.split("<<")[1]
                    objectName = line.split(">>")[1]
                    objectName = objectName.split("\n")[0]
                    if entitySetGroup in entitySetGroups.keys():
                        entitySetGroups[entitySetGroup].append(objectName)
            self.selSetGroupBox.setEnabled(True)
            self.createGroupsButton.setEnabled(True)
            for group in entitySetGroups.keys():
                item = QtGui.QStandardItem(group)
                item.setSizeHint(QtCore.QSize(100, 20))
                item.setCheckable(True)
                item.setCheckState(QtCore.Qt.CheckState.Checked)
                self.groupListModel.appendRow(item)
        # Otherwise we'll pop up a message to click away
        else:
            mobu.FBMessageBox("Warning:", "Selection canceled!", "OK")
    
    def setupSelectedGroups(self):
        global entitySetGroups
        groups = Globals.Groups
        groupNames = []
        for group in groups:
            groupNames.append(group.Name)
        setRootGroup = None
        entitySetGroup = None
        for group in groups:
            if "SetRoot" in group.Name:
                setRootGroup = group
            if "Entity Sets" in group.Name:
                entitySetGroup = group
        
        selectedGroups = []
        groupsToCreate = []
        
        for index in xrange(self.groupListModel.rowCount()):
            currentIndex = self.groupListModel.index(index, 0)
            currentItem = self.groupListModel.itemFromIndex(currentIndex)
            if currentItem.checkState() == QtCore.Qt.CheckState.Checked:
                selectedGroups.append(currentItem.text())
        
        for group in entitySetGroups.keys():
            if group in selectedGroups:
                groupsToCreate.append(group)
        
        if setRootGroup is not None:
            if len(groupsToCreate) > 0:
                if entitySetGroup is None:
                    entitySetGroup = mobu.FBGroup("Entity Sets")
                setRootGroup.ConnectSrc(entitySetGroup)
                for group in groupsToCreate:
                    if group in groupNames:
                        for existingGroup in groups:
                            if group in existingGroup.Name:
                                newGroup = existingGroup
                    else:
                        newGroup = mobu.FBGroup(group)
                    entitySetGroup.ConnectSrc(newGroup)
                    for obj in entitySetGroups[group]:
                        objModel = mobu.FBFindModelByLabelName(obj)
                        if objModel:
                            newGroup.ConnectSrc(objModel)
                mobu.FBMessageBox("Entity Set Creation", "Completed Successfully!", "OK")
        else:
            mobu.FBMessageBox("Entity Set Creation", "NO SETROOT FOUND!", "OK")


def rs_setupEntitySets(pControl, pEvent):
    EntitySets()