from RS import Globals

# update extension label
for extension in Globals.CharacterExtensions:
    if extension.Name == 'mover':
        extension.Label = 'mover_prop_extension'
    else:
        extension.Label = extension.Name

# save file
filePath = Globals.Application.FBXFileName
Globals.Application.FileSave(filePath)