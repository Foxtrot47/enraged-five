###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_AnimateControlRig.py
## Written And Maintained By: David Bailey
## Contributors:
## Description: Animates Animal Control Rig
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Globals as glo
import os

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gFKRig = ["HipsEffector",
          "LeftUpLeg",
          "LeftLeg",
          "LeftFoot",
          "LeftToeBase",
          "RightUpLeg",
          "RightLeg",
          "RightFoot",
          "RightToeBase",
          "Spine",
          "Spine1",
          "Spine2",
          "Spine3",
          "LeftShoulder",
          "LeftArm",
          "LeftForeArm",
          "LeftHand",
          "LeftFingerBase",
          "RightShoulder",
          "RightArm",
          "RightForeArm",
          "RightHand",
          "RightFingerBase",
          "Neck",
          "Neck1",
          "Neck2",
          "Neck3",
          "Head",
          "gs:CTRLRIG_Tail_01",
          "gs:CTRLRIG_Tail_02",
          "gs:CTRLRIG_Tail_03",
          "gs:CTRLRIG_Tail_04",
          "gs:CTRLRIG_Tail_05"]

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AnimateRotation( pModel, pNode, pOffset, pLength ):
   
   lAttr = pModel.PropertyList.Find( "Lcl Rotation" )
   if not lAttr.IsAnimated():
      lAttr.SetAnimated( True )
   
   lNode = pModel.Rotation.GetAnimationNode().Nodes[pNode]
   
   lValue = lNode.FCurve.Evaluate(FBTime(0,0,0,0))
   
   lNode.KeyAdd(FBTime(0,0,0,( 0             + pOffset)),  lValue)
   lNode.KeyAdd(FBTime(0,0,0,((pLength / 2)  + pOffset)),  lValue + 90)
   lNode.KeyAdd(FBTime(0,0,0,( pLength       + pOffset)),  lValue)
   glo.gScene.Evaluate()

###############################################################################################################   
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AnimateTranslation( pModel, pNode, pOffset, pLength ):
   
   lAttr = pModel.PropertyList.Find( "Lcl Rotation" )
   if not lAttr.IsAnimated():
      lAttr.SetAnimated( True )
   
   lNode = pModel.Translation.GetAnimationNode().Nodes[pNode]
   
   lValue = lNode.FCurve.Evaluate(FBTime(0,0,0,0))
   
   lNode.KeyAdd(FBTime(0,0,0,( 0             + pOffset)),  lValue)
   lNode.KeyAdd(FBTime(0,0,0,((pLength / 2)  + pOffset)),  lValue + 25)
   lNode.KeyAdd(FBTime(0,0,0,( pLength       + pOffset)),  lValue)
   glo.gScene.Evaluate()

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_GenerateROM( pArray, pLength, pCharacter ):

    if pCharacter:
        for iCharacter in glo.gCharacters:
            if iCharacter.Name == os.path.basename(glo.gApp.FBXFileName).partition(".")[0]:
                for iControlSet in glo.gControlSets:
                    iControlSet.FBDelete()
                iCharacter.CreateControlRig(True)
                iCharacter.InputType = FBCharacterInputType.kFBCharacterInputMarkerSet
                iCharacter.ActiveInput = True   

    if pArray:
        
        lPlayerControl = FBPlayerControl()
        
        lLengthOfAnimation = (len(pArray) * pLength) * 3 + (pLength * 3)
    
        lPlayerControl.LoopStop = FBTime(0,0,0,lLengthOfAnimation)
        
        lOffset = 0
        
        for iControl in pArray:
            
            lControl = FBFindModelByLabelName(iControl)
            
            if iControl == "HipsEffector":
                
                for i in range(3):
                
                    rs_AnimateTranslation( lControl, i, lOffset, pLength )
                    
                    lOffset = lOffset + pLength
            
            for i in range(3):
                
                rs_AnimateRotation( lControl, i, lOffset, pLength )
                
                lOffset = lOffset + pLength
    else:
        print "The control list is empty, this did not work"     

           
def Run():
   rs_GenerateROM( gFKRig, 20, True )
