"""
Methods for reading data from xmls in the etc\config\character\skeleton\ directories
"""
from pyfbsdk import *

import os
import re
import sys
from functools import wraps
import xml.etree.cElementTree as cElementTree

from RS import Globals
import RS.Utils.Scene
from RS.Utils.Scene import RelationshipConstraintManager
from RS.Core.Animation import Lib
from RS.Utils.Scene.Component import SetProperties
from RS.Utils.Scene import Constraint


"""
Utility Methods
"""


def CreateName(name, namespace):
    """
    Combines the given name and namespace into a single long name
    """
    if namespace and not namespace.endswith(":"):
        namespace = "{}:".format(namespace)
    if not name.startswith(namespace):
        name = "{}{}".format(namespace, name)
    return name


def ConvertStringToValue(name):
    """
    Gets the value of the variable that is named the same as the string
    passed into this function.

    Currently doesn't support python methods/classes that need to use any kind of argument what so ever.
    ex. passing "sys.paths" returns the value of sys.path


    Arguments:
        name (string): the name of the variable/method/class whose value you want to get

    Return:
        value of the variable/method/class whose variable name matches the argument 'name'
    """

    value = None
    exec("value = {}".format(name))
    return value

"""
Decorator for the Organize Methods
"""


def ConvertStringToValueDecorator(func):
    """
    Wraps the ConvertStringToValue method that gets the value of the variable that is named the same as the string
    passed into this function.

    Currently doesn't support python methods/classes that need to use any kind of argument what so ever.
    ex. passing "sys.paths" returns the value of sys.path

    Arguments:
        name = string; the name of the variable/method/class whose value you want to get

    Return:
        value of the variable/method/class whose variable name matches the argument 'name'
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        # Get the first parameter that isn't self from the decorated method
        counter = 0
        firstParameterName = "self"
        while firstParameterName == "self":
            firstParameterName = func.func_code.co_varnames[counter]
            counter += 1

        # If the first string has {}, execute the string as python code to get its value
        string_ = kwargs.get(firstParameterName, "")
        isPythonVariable = ""
        if isinstance(string_, basestring):
            # Checks if the string is in curly brackets ei. {}
            isPythonVariable = re.search("(?<={)[0-9a-z.,\(\)_+'\"\{\} ]+(?=})", string_, re.I)

        if isPythonVariable:
            # Converts python code from the string into a value
            string_ = ConvertStringToValue(isPythonVariable.group())

        # Pass the value into the decorated method
        kwargs[firstParameterName] = string_
        return func(*args, **kwargs)
    return wrapper


"""
Organize class
"""


class Organize(object):
    """
    This class reads the contents of an xml and generates motion builder components based on the data inside
    """
    def __init__(self, path="", parent=None, create=False, inheritProperties=False, namespace="", name="", removeByTypes=()):
        """
        Constructor

        Arguments:
            path (string): path to an xml file
            parent (FBComponent): parent of this component, default is None
            create (boolean): create this component if it doesn't exist
            inheritProperties (boolean): should element pass their attributes to their children.
            namespace (string): namespace to use
            name (string): name to replace component names that begin with a #
            removeByTypes(list): list of component types that should have components deleted from the scene if they
                            weren't modified or created by this class
        """
        self._namespace = {"__default__": namespace}
        self._name = name
        self._constraints = []
        self.components = {}
        self.deleteComponents = []
        self.missingComponents = []
        self.File(path=path, parent=parent, create=create, inheritProperties=inheritProperties, namespace=namespace)

        for component in self.deleteComponents:
            component.FBDelete()
        for componentType in removeByTypes:
            components = Globals.ComponentLists.get(componentType, None)
            if components is None:
                components = [component for component in Globals.Components if isinstance(component, componentType)]
            else:
                components = list(components)
            for component in components:
                if component not in self.components.get(componentType, ()):
                    component.FBDelete()

    def ConvertAttributeValues(self, attribDictionary, removeUnderscores=True, namespace=""):
        """
        Converts the text value from a dictionary into an appropriate data type if possible. Intended
        to be used on the attributes of xml elements.

        Arguments:
            attribDictionary (dictionary): dictionary whose string values need to be converted into other
                                data types
            removeUnderscores (boolean): replace underscores with spaces
            namespace (string): namespace being used in the current motion builder scene

        Return:
            dictionary
        """

        for eachKey, eachValue in attribDictionary.items():
            if isinstance(eachValue, basestring):
                attribDictionary[eachKey] = self.ConvertStringToData(eachValue, namespace=namespace)

        return attribDictionary

    def ConvertStringToData(self, value, namespace=""):
        """
        Converts a string to an appropriate data type

        Arguments:
            value (string): string value to convert into another data type
            namespace (string): namespace to add to the value if applicable

        Return:
            object
        """
        if namespace and not namespace.endswith(":"):
            namespace = "{}:".format(namespace)

        isPythonCode = re.compile("(?<={)[0-9a-z.\(\)_+'\"\{\}, ]+(?=})", re.I).search
        isFloat = RS.Utils.Math.IsFloat

        if (value.startswith("FB") and "|" not in value) or re.search("(?<=\[)[0-9,. ]+(?=])", value):
            value = re.sub("(?<=[\(,])[0]+(?=[1-9])", "", value)
            exec("value={}".format(value))

        elif "||" in value:
            # Can't use dictionary comprehension here because we are using exec in ConvertString
            # The generator must be calling it and we run into a syntax error for trying to call two exec within
            # the same block of code
            _value = value
            value = {}
            for index, eachValue in enumerate(_value.split("||")):
                value[index] = self.ConvertStringToData(eachValue, namespace=namespace)

        elif value.strip().startswith("$") and "]" in value:

            results = re.search("(?<=\[)[a-z0-9_^: ]+(?=\])", value, re.I)

            name = value.strip().split("]")[-1]
            namespace = self._namespace.get(results.group(), namespace)

            value = FBFindModelByLabelName("{}{}".format(namespace, name))
            value = FBFindModelByLabelName(name) if not value else value

        elif value.strip().startswith("$"):

            name = value.strip()[1:]
            value = FBFindModelByLabelName("{}{}".format(namespace, name))

        elif value.strip() in ["True", "False"]:
            value = value.strip() == "True"

        elif value.strip().isdigit():
            value = int(value)

        elif isFloat(value):
            value = float(value)

        elif isPythonCode(value):
            exec("value = {}".format(isPythonCode(value).group()))

        elif "," in value and value.startswith("(") and value.endswith(")"):
            value = [self.ConvertStringToData(each.strip(), namespace=namespace) for each in value[1:-1].split(",")]

        return value

    def GetProperties(self, element, namespace=""):
        """
        Converts a Properties element into a dictionary

        Arguments:
            element (xml.etree.elementTree.Element): element that has data about the properties of a component
            namespace (namespace): namespace to look for components under
        """
        properties = {}
        sources = {}
        references = {}
        propertiesElement = element.find("Properties") or []

        for propertyElement in propertiesElement:
            name = propertyElement.attrib.get("Name", "").strip()
            if propertyElement.attrib.get("Reference"):
                name, propertyName = name.split(".", 1)
                references.setdefault(name, []).append(propertyName)
                continue
            data = propertyElement.find("Data")
            data = data.text or ""
            properties[name] = self.ConvertStringToData(data.strip(), namespace=namespace)

            _sources = propertyElement.findall("Source")
            for source in _sources:
                sources.setdefault(name, [])
                isComponent = source.attrib["IsComponent"] == "True"
                sourceText = source.text.strip() if not isComponent else "${}".format(source.text.strip())
                
                sources[name].append(self.ConvertStringToData(sourceText, namespace=namespace))

        return properties, sources, references

    def SetSources(self, component, **sources):
        """
        Connects sources/incoming connections to the properties of the component

        Arguments:
            component (pyfbsdk.FBComponent):
            sources (dictionary): keys are the name of the property and the values are a list of FBComponents or
                                  FBProperties to connect to the property from the component

        """
        if component is None:
            return

        for property in component.PropertyList:
            if property.Name in sources:
                for source in sources[property.Name]:
                    property.ConnectSrc(source)
                    FBSystem().Scene.Evaluate()
                    
    def Parse(self, element, parent=None, create=False, inheritProperties=True, namespace="", **properties):
        """
        recursively generates groups and parents objects based on the xml
        element provided

        Arguments:
            element (xml.etree.cElementTree.Element): element generated from an xml file using elementTree
            parent (FBComponent): parent for the components generated by the top elements of the xml file,
                                  default is None
            create (boolean): create this component if it doesn't exist
            namespace (string): namespace to use
            inheritProperties (boolean): should element pass their attributes to their children.
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.
        Example:
            # Show = 'True' is the same as FBModel.Show = True
            # AddReference = (0, FBModel) is the same as FBConstraint.AddReference(0, FBModel)

        Return:
            pyfbsdk.FBComponent
        """

        elementType = element.tag

        if not inheritProperties:
            properties = {"inheritProperties": inheritProperties}

        _properties, sources, references = self.GetProperties(element, namespace=namespace)
        properties.update(element.attrib)
        properties.update(_properties)
        properties = self.ConvertAttributeValues(properties, namespace=namespace)

        currentModule = sys.modules[__name__]

        FBType = getattr(currentModule, "FB{}".format(elementType), None)

        isModel = "FBModel" in FBType.__class__.__name__
        defaultMethod = [self.Component, self.Model][isModel]
        organizeMethod = getattr(self, elementType, defaultMethod)

        if organizeMethod == self.Component and FBType is None:

            def organizeMethod(**_):
                return None

        elif organizeMethod == self.Constraint:
            self._constraints.append(element)

        properties["FBType"] = FBType
        properties["name"] = properties.pop("Name", properties.get("name", FBType.__class__.__name__[2:]))

        text = element.text or ""
        if organizeMethod == self.Python:
            properties["code"] = text

        elif organizeMethod in [self.Constraint, self.Shader, self.Folder]:
            properties["type"] = text.strip()
            properties["string_"] = text

        elif organizeMethod in [self.File, self.Template, self.ConstraintRelation]:
            properties["path"] = text
            properties["string_"] = text
            if organizeMethod == self.ConstraintRelation:
                properties["name"] = os.path.splitext(os.path.basename(text))[0]
        elif organizeMethod in [self.CharacterExtension]:
            properties["extensions"] = text.split(",")
            properties["string_"] = text
            properties["references"] = references
        else:
            properties["string_"] = text

        properties["parent"] = parent
        if isinstance(parent, (FBGroup, FBFolder)):
            properties["parent"] = None
            properties["ConnectDst"] = parent
        properties.setdefault("create", create)
        properties["namespace"] = namespace

        currentComponent = organizeMethod(**properties)

        self.SetSources(currentComponent, **sources)
        properties["parent"] = currentComponent
        for eachSubElement in element:
            if eachSubElement.tag in "Properties":
                continue
            self.Parse(element=eachSubElement, **properties)

        return currentComponent

    @ConvertStringToValueDecorator
    def File(self, path, parent=None, create=False, namespace="", inheritProperties=True, **properties):
        """
        Takes the path passed to the xml file and starts to recursively apply the data from it

        Arguments:
            path (string): path to an xml file
            parent (FBComponent): parent for the components generated by the top elements of the xml file,
                                  default is None
            create (boolean): create this component if it doesn't exist
            namespace (string): namespace to use
            inheritProperties (boolean): should element pass their attributes to their children.
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.
        Return:
            pyfbsdk.FBComponent
        """
        if self._name:
            with open(path, "r") as _:
                text = _.read()
            regex = ""
            results = re.findall("#[a-zA-Z0-9_]+", text)
            for result in list(set(results)):
                if not regex or len(regex) > len(result):
                    regex = result
            text = text.replace(regex, self._name)
            element = cElementTree.fromstring(text)
        else:

            xmlTree = cElementTree.parse(path)
            element = xmlTree.getroot()
        return self.Parse(element=element, parent=parent, create=create, inheritProperties=inheritProperties,
                          namespace=namespace, **properties)

    @ConvertStringToValueDecorator
    def Component(self, name, parent=None, create=False, FBType=FBComponent, namespace="", **properties):
        """
        Creates a FBComponent or any object that derives from FBComponent. The type of the object created by this method
        is determined by the FBType parameter.
        All keyword parameters are treated as properties to be set on each child.

        Arguments:
            name (string): name of the FBModel
            parent (FBComponent): parent of this component, default is None
            create (boolean): create this component if it doesn't exist
            FBType (FBComponent): the type of component that you want to
                    create/organize
            namespace (string): namespace to use
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.
        Return:
            FBComponent
        """

        if namespace and not namespace.endswith(':'):
            namespace = "{}:".format(namespace)

        if not isinstance(name, FBComponent):
            component = [each for each in
                         RS.Utils.Scene.GetComponentsByNamePatternAndType(FBType, "{}{}".format(namespace, name))]
        else:
            component = [name]

        if FBType is FBModel:
            FBType = FBModelNull

        if not component and create:
            component = [FBType("{}{}".format(namespace, name))]

        elif not component:
            self.missingComponents.append("{}{}".format(namespace, name))
            return

        component = component[0]
        if parent:
            component.ConnectDst(parent)

        if component not in self.components.setdefault(FBType, []):
            self.components[FBType].append(component)

        properties.pop("string_", None)
        properties.pop("inheritProperties", None)

        SetProperties(component, force=create, **properties)
        return component

    def Group(self, name, parent, namespace="", **properties):
        """
        Creates a FBGroup

        Arguments:
            name (string): name of the FBGroup
            parent (FBComponent): parent of the FBGroup, default is None
            namespace (string): namespace to use
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.
        Return:
            pyfbsdk.FBGroup
        """
        properties["create"] = True
        component = self.Component(name=name, parent=parent, namespace=namespace, **properties)
        return component

    @ConvertStringToValueDecorator
    def Folder(self, type, parent=None, create=False, excludeByTypes="", namespace="", **properties):
        """
        Creates a FBFolder

        Arguments:
            type (string): under which type should the Folder should be created.
            parent (FBComponent): parent of this component, default is None
            create (boolean): create this component if it doesn't exist
            excludeByTypes (string): type of objects that you do not want in a folder. Certain types
                            of objects such as FBCharacter seem to add other types of objects into
                            the folder as well. To include multiple types to be excluded separate the
                            types with a '|'.
                            ex. 'FBCharacter|FBMaterial'
            namespace (string): namespace to use
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.

        Return:
            pyfbsdk.FBFolder
        """

        folders = {folder.LongName: folder for folder in FBSystem().Scene.Components if isinstance(folder, FBFolder)}
        name = CreateName(properties.get("name", ""), namespace)
        folder = folders.get(name, None)

        if parent:
            properties["ConnectDst"] = parent
        if not folder:
            folder = RS.Utils.Creation.rs_CreateFolder(name, type)
        SetProperties(folder, **properties)
        if folder not in self.components.setdefault(FBFolder, []):
            self.components[FBFolder].append(folder)
        return folder

    def Model(self, name, parent=None, create=False, namespace="", **properties):
        """
        Attempts to find a model and parents it to the new parent. It also creates/sets any properties passed
        as keyword arguments on this model.

        Arguments:
            name (string): name of the FBModel
            parent (FBComponent): parent of this component, default is None
            create (boolean): create this component if it doesn't exist
            namespace (string): namespace to use
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.
        Return:
            pyfbsdk.FBModel
        """

        properties.setdefault("FBType", FBModel)

        if "FBModel" not in properties["FBType"].__name__:
            properties["FBType"] = FBModel

        model = self.Component(name=name, parent=parent, create=create, namespace=namespace, **properties)

        if model.__class__ is not properties["FBType"] and model is not None:
            # When the class is not the same as the one passed in, we assume that a new null was created and we
            # set the size of it to 1 to avoid 
            model.Size = 1

        if model and parent.__class__.__name__ == "FBGroup":
            children = []
            if properties.pop("allChildren", False):
                RS.Utils.Scene.GetChildren(model, children)

            model.ConnectDst(parent)
            [SetProperties(each, **properties) for each in filter(None, children)]

        return model

    def ModelPath3D(self, name, parent=None, create=False, namespace="", **properties):
        """
        Creates a FBModelPath3D()

        Arguments:
            type (string): type of FBShader to create
            parent (FBComponent): parent of this component, default is None
            create (boolean): create this component if it doesn't exist
            namespace (string): namespace to use
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.
        Return:
            pyfbsdk.FBModelPath3D
        """
        points = {propertyName: properties.pop(propertyName) for propertyName in properties.keys()
                  if propertyName.startswith("Point_")}
        model = self.Model(name, parent=parent, create=create, namespace=namespace, **properties)

        for index, propertyName in enumerate(sorted(points.keys())):
            position = FBVector4d(*points[propertyName])
            if index > 1:
                model.Segment_PathKeyAdd(index, position)
            else:
                model.PropertyList.Find(propertyName).Data = position
        return model

    def ModelMarker(self, name, parent=None, create=False, namespace="", **properties):
        """
        Creates a FBModelMarker()

        Arguments:
            type (string): type of FBShader to create
            parent (FBComponent): parent of this component, default is None
            create (boolean): create this component if it doesn't exist
            namespace (string): namespace to use
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.
        Return:
            pyfbsdk.FBModelMarker
        """
        model = self.Model(name, parent=parent, create=create, namespace=namespace, **properties)
        geometryOffsets = {"GeometricTranslation": properties.get("GeometricTranslation", None),
                           "GeometricRotation": properties.get("GeometricRotation", None),
                           "GeometricScaling": properties.get("GeometricScaling", None)}
        if all([model] + geometryOffsets.values()):
            SetProperties(model, **geometryOffsets)

        return model

    def Shader(self, type, parent=None, create=True, namespace="", **properties):
        """
        Creates a FBShader()
        Arguments:
            type (string): type of FBShader to create
            parent (FBComponent): parent of this component, default is None
            create (boolean): create this component if it doesn't exist
            namespace (string): namespace to use
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.
        Return:
            pyfbsdk.FBShader
        """
        shader = FBShaderManager().CreateShader(type)
        properties["name"] = shader
        return self.Component(parent=parent, create=create, namespace=namespace, **properties)

    def Character(self, name, parent=None, create=True, namespace="", **properties):
        """
        Creates a FBCharacter()

        Arguments:
            name (string): name of the component
            parent (FBComponent): parent of this component, default is None
            create (boolean): create this component if it doesn't exist
            namespace (string): namespace to use
            **properties (dictionary): properties to set on this component. The key is the name of the property and
                                       the value is the value.
        Return:
            pyfbsdk.FBCharacter
        """
        createControlRig = properties.pop("CreateControlRig", False)
        setCharecterize = properties.pop("SetCharacterizeOn", False)
        useBiped = properties.pop("Posture", 0) == 0
        for character_ in Globals.Characters:
            if character_.LongName.startswith(namespace) and character_.Name == name and Lib.HasControlRig(character_):
                createControlRig, setCharecterize = False, False
                break

        # Setting the values of certain properties switches their mode to user, which may not be the intended mode
        # mode properties should be set after the first set of properties they affect are set.

        modeProperties = {property: properties.pop(property)
                          for property in properties.keys() if property.endswith("Mode")}

        # Link properties refer to FBModel components in the scene, so their values need to be converted to the
        # fbmodel they represent
        linkProperties = {}
        for key, value in properties.items():
            if key.endswith("Link") and value:
                if isinstance(value, dict):
                    linkProperties[key] = {index: FBFindModelByLabelName(value_) for index, value_ in value.iteritems()}
                    properties.pop(key)
                else:
                    linkProperties[key] = FBFindModelByLabelName(properties.pop(key))

        character = self.Component(name=name, parent=parent, create=create, namespace=namespace,
                                   FBType=properties.pop("FBType"))

        # 'Refresh' the characterization so the control rig is created properly
        if setCharecterize:
            character.SetCharacterizeOff()
            SetProperties(character, **properties)
            SetProperties(character, **linkProperties)
            # Set mode properties incase they were changed while setting the other properties they get affected by
            SetProperties(character, **modeProperties)
            character.SetCharacterizeOn(useBiped)
        else:
            SetProperties(character, **properties)
            SetProperties(character, **linkProperties)
            # Set mode properties incase they were changed while setting the other properties they get affected by
            SetProperties(character, **modeProperties)

        if createControlRig:
            character.CreateControlRig(True)
            Globals.Scene.Evaluate()
        referenceNull = Lib.GetControlRigReferenceNull(character)

        if referenceNull is not None:
            # Discover the namespace for the control rig
            self._namespace[name] = "".join(["{}:".format(namePart)
                                             for namePart in referenceNull.LongName.split(":")[:-1]])
        Globals.Scene.Evaluate()
        return character

    @ConvertStringToValueDecorator
    def CharacterExtension(self, extensions, parent=None, folder=None, namespace="", references=None, **properties):
        """
        Creates a character extension for the model whose name is passed if it exists.

        Arguments:
            extensions (list[strings, etc.]): name of the components to be added to this extension
            parent (FBComponent): parent of the component, default is None
            folder (FBFolder): folder to add
            namespace (string): namespace to use
            references (list): list of valid references for this character extension. reference properties that are
                               not part of this list are removed from the character extension.
            **properties (dictionary): properties to set on the component. The key is the name of the property and
                                       the value is the value.
        Return:
            pyfbsdk.FBCharacterExtension
        """
        if hasattr(Globals.System, "SuspendMessageBoxes"):
            Globals.System.SuspendMessageBoxes = True

        name = properties.get("name", "Extension")
        longName = CreateName(name, namespace)
        characterExtensions = [extension for extension in Globals.CharacterExtensions
                               if longName == extension.LongName]

        if not characterExtensions:
            characterExtension = FBCharacterExtension(longName)
            characterExtension.Label = longName

            if not isinstance(parent, FBCharacter):
                parent = Globals.Characters[0]

            parent.AddCharacterExtension(characterExtension)
        else:
            characterExtension = characterExtensions[0]

        # Add objects to the extension

        for index, extension in enumerate(extensions):

            extension = extension.strip()
            if isinstance(extension, basestring) and namespace:
                extension = FBFindModelByLabelName("{}:{}".format(namespace, extension))
            elif isinstance(extension, basestring):
                extension = FBFindModelByLabelName(extension)

            if isinstance(extension, basestring) or extension is None:
                continue

            FBConnect(extension, characterExtension)
            characterExtension.AddObjectProperties(extension)

        # Adds namespace to properties that reflect a component in the scene
        for property, value in properties.items():
            if "." in property and namespace:
                properties.pop(property, None)
                properties["{}:{}".format(namespace, property)] = value
        SetProperties(characterExtension, **properties)
        # remove referenced properties that were not part of the parsed xml
        if references:
            for property in list(characterExtension.PropertyList):
                if property.IsReferenceProperty():
                    referenceName, propertyName = property.Name.split(".", 1)
                    hasNumber = re.search(" [0-9]+$", property.Name)
                    if hasNumber:
                        propertyName = propertyName[:len(hasNumber.group()) * -1].strip()
                    referenceName = referenceName.split(":")[-1]
                    validProperties = references.get(referenceName, [])

                    if propertyName not in validProperties:
                        characterExtension.PropertyRemove(property)
        if folder:
            folder.Items.append(characterExtension)

        if hasattr(Globals.System, "SuspendMessageBoxes"):
            Globals.System.SuspendMessageBoxes = False

        if characterExtension not in self.components.setdefault(FBCharacterExtension, []):
            self.components[FBCharacterExtension].append(characterExtension)

        return characterExtension

    def Constraint(self, type, parent, namespace="", **properties):
        """
        Creates an FBConstraint

        Arguments:
            type (string): type of constraint that you want to create
            parent (FBComponent): or object that inherits FBComponent; parent for this new component
            namespace (string): namespace to use if applicable
            **properties: You can pass the name of a property you want to create/edit as a keyword
                  parameter. Any created/found motion builder object found will have
                  these values set/created.

        Return:
            pyfbsdk.FBConstraint
        """
        constraintTypes = {
                        "Aim"               :  Constraint.AIM,
                        "Expression"        :  Constraint.EXPRESSION,
                        "Expressions"       :  Constraint.EXPRESSION,
                        "SplineIk"          :  Constraint.SPLINE_IK,
                        "Multi Referential" :  Constraint.MULTI_REFERENTIAL,
                        "Parent/Child"      :  Constraint.PARENT_CHILD,
                        "Parent-Child"      :  Constraint.PARENT_CHILD,
                        "Path"              :  Constraint.PATH,
                        "Path Constraint"   :  Constraint.PATH,
                        "Position"          :  Constraint.POSITION,
                        "Range"             :  Constraint.RANGE,
                        "Relation"          :  Constraint.RELATION,
                        "Rigid Body"        :  Constraint.RIGID_BODY,
                        "3 Points"          :  Constraint.THREE_POINT,
                        "Rotation"          : Constraint.ROTATION,
                        "Rotation From Rotations": Constraint.ROTATION,
                        "Scale"             : Constraint.SCALE,
                        "Mapping"           : Constraint.MAPPING,
                        "Chain IK"          : Constraint.CHAIN_IK,
                        "RSExprSolverConstraint": Constraint.EXPRESSION}

        name = properties.get("name", "Constraint")
        if namespace:
            name = "{}:{}".format(namespace, name)

        constraint = filter(lambda each: each.LongName.strip() == name.strip(), RS.Globals.Constraints)

        if not constraint:
            constraint = [Constraint.CreateConstraint(constraintTypes[type])]
            constraint[0].LongName = name

        # The Constraint Properties use the name of the components for the offset values
        # so if the name of the offset properties are not the same as the currently constrained components
        # they will not be set.

        offsetProperties = {}
        #TODO: Test against the IkSpline constraint setup
        #The curve should be connected first and then the joints
        referenceValues = properties.get("ReferenceAdd", None)
        if referenceValues:
            if not isinstance(referenceValues, dict):
                # The parsing logic will create a list instead of a dict if the constraints only have one connection
                referenceValues = {referenceValues[0]: referenceValues}
            # Get a list of the objects that are part of the constraint
            components = [values[-1] for values in referenceValues.itervalues()
                          if isinstance(values[-1], FBComponent)]
            if isinstance(properties.get("Source (Parent)", None), dict):
                components.extend([values for values in properties["Source (Parent)"].values()])
            components = list(set(components))
            # filter properties that only affect the components that are part of the constraint
            for key, value in properties.items():
                for component in components:
                    if "." in key and component.Name.strip() in key and key.startswith(component.LongName):
                        _, propertyName = key.split(".", 1)
                        offsetProperties["{}.{}".format(component.LongName, propertyName)] = value

        SetProperties(constraint[0], **properties)
        if offsetProperties:
            SetProperties(constraint[0], create=True,  **offsetProperties)

        # FBConstraints do not have a Parent variable, so we are creating one to make sure the xml works
        # properly when a child of this object tries to access the parent of it.

        constraint[0].Parent = parent

        return constraint[0]

    def ConstraintRelation(self, path, parent=None, namespace="", **properties):
        """
        Creates a relationship constraint

        Arguments:
            path (string): path to an Motion Builder object template
            parent (FBComponent): parent for the components generated by the top elements of the xml file,
                                  default is None
            namespace (string): namespace to use
            **properties (dictionary): properties to set on the component. The key is the name of the property and
                                       the value is the value.

        Return:
            pyfbsdk.FBConstraintRelation
        """

        constraint = None
        for _constraint in FBSystem().Scene.Constraints:
            if _constraint.__class__ == FBConstraintRelation:
                if namespace in _constraint.LongName and properties.get("name", "") == _constraint.Name:
                    constraint = _constraint
                    break

        if os.path.exists(path) and not constraint:
            manager = RelationshipConstraintManager.RelationShipConstraintManager()
            if properties.get("name"):
                manager.SetName(properties.get("name"))
            constraint = manager.GetConstraint()
            try:
                manager.LoadConstraintFromFile(path, namespace=namespace or None)
            except:
                self.missingComponents.append(constraint.Name)
                self.deleteComponents.append(constraint)
            properties["name"] = constraint

        self.Component(**properties)
        return constraint

    def Template(self, path, parent=None, create=False, namespace="", **properties):
        """
        Creates an object from a path to the Browser/Template window in Motion Builder

        Arguments:
            path (string): path to an Motion Builder object template
            parent (FBComponent): parent for the components generated by the top elements of the xml file,
                                  default is None
            create (boolean): create the component if it doesn't exist
            namespace (string): namespace to use
            **properties (dictionary): properties to set on the component. The key is the name of the property and
                                       the value is the value.

        Return:
            pyfbsdk.FBComponent
        """

        path, objectType = os.path.split(path)
        name = properties.get("Name", objectType)
        if namespace:
            name = "{}:{}".format(namespace, name)

        component = FBFindModelByLabelName(name)

        if not component and create:
            component = FBCreateObject(path, objectType, objectType)

        elif not component:
            return
        properties["name"] = component
        properties["FBType"] = FBModel
        return self.Component(parent=parent, create=create, **properties)

    def Python(self, code, parent=None, namespace="", *args,  **properties):
        """
        Takes python code and executes it
        Arguments:
            code: string; python code
            parent (FBComponent): parent for the components generated by the top elements of the xml file,
                                  default is None
            namespace (string): namespace of the FBCharacter
            **properties (dictionary): properties to set on the character. The key is the name of the property and
                                       the value is the value.
        Return:
            None
        """
        # Resolve spacing from the xml file
        # python is space sensitive and needs to be indented properly so the code doesn't fail
        spaces = 0
        if namespace:
            namespace = "{}:".format(namespace)

        for eachLine in code.splitlines():
            if eachLine.strip():
                spaces = len(eachLine) - len(eachLine.lstrip())
                break
        code = code.replace(" " * spaces, "")

        # Get values from Python
        variables = re.findall("(?<={)[0-9a-z.\[\]()_]+(?=})", code, re.M | re.I)
        variable_values = [ConvertStringToValue(eachVariable) for eachVariable in variables]

        # Build python code
        pythonCodeFormat = re.sub("(?<={)[0-9a-z.](?=})", "", code)
        pythonCode = pythonCodeFormat.format(*variable_values)

        # Run Python Code
        exec pythonCode
