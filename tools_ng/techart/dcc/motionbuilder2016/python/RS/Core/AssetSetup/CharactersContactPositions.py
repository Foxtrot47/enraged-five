import pyfbsdk as mobu
from PySide import QtGui

from os import *
import glob
import os

from RS import Perforce, Config



# contactPosition directory
logRootPath = os.path.join("{0}".format(Config.Project.Path.Art),
                            "animation",
                            "resources",
                            "characters",
                            "contactPositions")

propList = []
propDataList = []


# Definition to gather property data. Used in rs_SaveContactPostion
def rs_PropertyInformationtoLists(prop):
    propData = prop.Data
    propList.append(prop.Name)
    propDataList.append(propData)


# Definition to load property data. Used in rs_LoadContactPostion
def rs_PropertyLoad(dict, character):
    for propName, propData in dict.iteritems():
        propNameProperty = character.PropertyList.Find( propName )
        if propNameProperty:
            propNameProperty.Data = float(propData)


# Definition to SAVE contacts into a text doc
def rs_SaveContactPostion(character, saveName):
    rs_PropertyInformationtoLists(character.PropertyList.Find("Hand Height"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Hand Back"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Hand Middle"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Hand Front"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Hand In Side"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Hand Out Side"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Foot Height"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Foot Back"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Foot Middle"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Foot Front"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Foot In Side"))
    rs_PropertyInformationtoLists(character.PropertyList.Find("Foot Out Side"))

    propertyDict = dict(zip(propList, propDataList))

    logList = []

    # check directory exists, if not, create it!
    if not os.path.isdir(logRootPath):
        os.makedirs(logRootPath)

    for propName, propData in propertyDict.iteritems():
        dictString = ("'" + propName + "'" + ":" + "'%s" %propData + "'" +'\n')
        logList.append(dictString)

    filePath = os.path.join(logRootPath,
                            '{0}.txt'.format(saveName))

    Perforce.Sync(filePath)

    if os.path.exists(filePath):
        Perforce.Edit(filePath)
    else:
        Perforce.Add(filePath)

    sep = ''
    rosetta = sep.join(logList)
    try:
        myFile1 = open(filePath, O_CREAT)
        myFile = open(filePath, O_WRONLY)
        write (myFile, rosetta)
        close (myFile)
        close (myFile1)
        mobu.FBMessageBox("Success!",
                          "\nContact positions have saved.\n" + filePath + '\n\nRemember:  Check the text doc into P4.',
                          "Ok")
    except OSError as err:
        if "permission denied" in str(err).lower():
            QtGui.QMessageBox.warning(None,
                                      "Write Error",
                                      "Unable to open file as it is read only.\n\n{}".format(str(err)),
                                      QtGui.QMessageBox.Ok)

# Definition to LOAD contacts into a text doc
def rs_LoadContactPostion(character,
                          saveName,
                          popUpMessage=True):

    filename = os.path.join(logRootPath,
                             "{0}.txt".format(saveName))

    if not path.isfile(filename):
        Perforce.Sync(filename)

    if path.isfile(filename):
        myFile = file(filename ,"r")
        lTextLines = myFile.readlines()
        propNameList = []
        propDataList = []
        for iLine in lTextLines:

            lineSplit = iLine.split(":")

            propNameSplit1 = lineSplit[0]
            propNameSplit2 = propNameSplit1.split("'")
            if len(propNameSplit2) >1:
                propName = propNameSplit2[1]
                propNameList.append(propName)
            if len(lineSplit) >1:
                propDataSplit1 = lineSplit[1]
                lPropDataSplit2 = propDataSplit1.split("'")
                propData = lPropDataSplit2[1]
                propDataList.append(propData)

        loadPropDict = dict(zip(propNameList, propDataList))

        rs_PropertyLoad(loadPropDict, character)

        if popUpMessage:
            mobu.FBMessageBox("Success!", "Properties for Foot and Hand Contact Positions\nhave loaded on to " + character.Name + ".", "Ok")
        print "File Loaded: " + filename

    else:
        reportText = 'The file does not exist! Cannot load contact positions.\n\n{0}'.format(filename)
        if popUpMessage:
            mobu.FBMessageBox('Rockstar', reportText, 'OK')

def validateScene():
    if len(mobu.FBSystem().Scene.Characters) == 0:
        mobu.FBMessageBox("Error", "No characters exist in the scene. Please open a scene containing a character in order to use the Contact Position tools.", "Ok")
        return False
    return True

# Find what type of character is open, and run script to SAVE
def rs_SaveContactPostionLaunch(control=None, event=None):
    if not validateScene():
        return
    character = mobu.FBSystem().Scene.Characters[0]
    mobu.FBPlayerControl().Goto(mobu.FBTime(0,0,0, 0))
    saveName = character.Name
    rs_SaveContactPostion(character, saveName)


# Description: Find what type of character is open, and run script to LOAD
def rs_LoadContactPostionLaunch(control=None, event=None):
    if not validateScene():
        return False
    character = mobu.FBSystem().Scene.Characters[0]
    mobu.FBPlayerControl().Goto(mobu.FBTime(0,0,0, 0))
    saveName = character.Name
    rs_LoadContactPostion(character, saveName, popUpMessage=False)
    return True

