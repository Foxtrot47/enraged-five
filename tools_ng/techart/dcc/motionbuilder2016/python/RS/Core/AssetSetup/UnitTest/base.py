import os
import unittest

import pyfbsdk as mobu
import scandir

from RS import Globals, Perforce
from RS.Utils import Scene, ContextManagers
from RS.Unittests import utils
from RS.Core.Rigging import core as riggingCore
from RS.Core.AssetSetup import Characters
from RS.Core.AssetSetup.UnitTest import const
from RS.Core.AssetSetup.Parser import Organize
from RS.Core.AssetSetup.Serializer import Skeleton, ControlRig, Organization


class Base(unittest.TestCase):
    """Base class for the asset setup unit Tests"""

    def __init__(self, *args, **kwargs):
        super(Base, self).__init__(*args, **kwargs)

        # get data
        self.config = utils.loadYaml(const.ConfigPathDict.RIG_DATA_CONFIG_PATH)
        self.testData = utils.getConfigKey(None, self.config)
        self.rootName = utils.getConfigKey("rootName", self.config)

    def setUp(self):
        """Clears the scene before running the test"""

        # Need to run scene evaluate twice to make sure that Motion Builder realizes that a new scene has been opened
        # Not running them was causing false positives in the unit tests when ran in succession
        Globals.Scene.Evaluate()
        Globals.Application.FileNew()
        Globals.Scene.Evaluate()

    def tearDown(self):
        """Clears the scene after running the test"""

        Globals.Application.FileNew()

    def assertSetupAsset(self, buildIG=True, definitionName="Player_Zero", outfit="META_OUTFIT_WARM_WEATHER"):
        """Imports max asset and sets it up before asset setup is run

        Args:
            buildIG (bool): Build ingame or cutscene rig
            definitionName (str, optional): Name of the character definition
            outfit (str, optional): Name of outfix to use
        """
        # use metaped to build rig pre setup to test
        rigSettings = riggingCore.dependency.BuildRigSettings(buildIG=buildIG,
                                                              includeWearable=False,
                                                              useLegacySetup=True,
                                                              overrideBoneFromPreviousSkelSet=True,
                                                              checkOutInReviewFolder=False,
                                                              exposeOutfit=True,
                                                              definitionName=definitionName,
                                                              useOverrideDefinition=False,
                                                              outfit=outfit,
                                                              useOverrideName=False,
                                                              buildingPhase=0)

        builder = riggingCore.AssetBuilder(rigSettings)
        builder.build(rigSettings)

    def assertCutsceneCharCreate(self):
        """Create cutscene character"""
        with ContextManagers.SuspendMessages():
            Characters.rs_AssetSetupCSCharacters(buildFromMetapedToFbx=True, popUpMessage=False)

    def assertIngameCharCreate(self):
        """Create ingame character"""
        with ContextManagers.SuspendMessages():
            Characters.rs_AssetSetupIGCharacters(buildFromMetapedToFbx=True, popUpMessage=False)

    def assertStore(self, path):
        """Stores the rig information into xmls

        Args:
            path (str): Path of directory to store
        """
        # Ensure the data directory exists and that existing
        if not os.path.exists(path):
            os.makedirs(path)

        paths = [childPath.path for childPath in scandir.scandir(path)]

        Perforce.Edit(paths, forceWritable=True)

        self.assertStoreSkeleton(path)
        self.assertStoreControlRig(path)
        self.assertStoreOrganization(path)

        Perforce.Revert(paths)

    def assertStoreSkeleton(self, path):
        """Stores the skeleton data

        Args:
            path (str): Path of directory to store the skeleton
        """
        model = mobu.FBFindModelByLabelName(self.rootName)
        self.assertIsNotNone(model)
        serializer = Skeleton.Serializer(model)
        serializer.Serialize()
        serializer.Write(os.path.join(path, const.FileNamesDict.SKELETON_NAME))

    def assertStoreControlRig(self, path):
        """Stores the control rig data

        Args:
            path (str): Path of directory to store the skeleton
        """
        serializer = ControlRig.Serializer()
        if serializer.hasRig():
            serializer.Serialize()
            serializer.Write(os.path.join(path, const.FileNamesDict.CONTROL_RIG_NAME))

    def assertStoreOrganization(self, path):
        """Stores the organization data

        Args:
            path (str): Path of directory to store the skeleton
        """
        serializer = Organization.Serializer()
        serializer.Serialize()
        serializer.Write(os.path.join(path, const.FileNamesDict.ORGANIZATION_NAME))

    def assertP4Sync(self, path):
        """Syncs the provided file from P4

        Args:
            path (str): Path to sync
        """
        Perforce.Sync(path, force=True)
        self.assertTrue(os.path.exists(path))

    def assertRoot(self):
        """Checks that there is a root skeleton component"""
        model = mobu.FBFindModelByLabelName(self.rootName)
        self.assertIsNotNone(model)
        return model

    def assertRestore(self, path, marginOfError=0.0):
        """Checks that the rig is rebuilt properly

        Args:
            marginOfError (float, optional): The acceptable margin of error between pre-rig and post-rig skeleton values
                                    This is due to the rebuilt rigs having small value differences due to how the rig
                                    evaluates a bone position. Usually the difference is so small that it is negligible.
        """
        self.assertEqual(len(Globals.Characters), 1)

        character = Globals.Characters[0]
        namespace = character.Name.split(":")[0] if ":" in character.Name else ""

        model = self.assertRoot()

        self.assertRestoreSkeleton(namespace, path)
        skeletonValues = {}
        for child in Scene.GetChildrenGenerator(model):
            skeletonValues[child] = [
                list(child.PropertyList.Find(propertyName).Data) for propertyName in ("Lcl Translation", "Lcl Rotation")
            ]

        self.assertRestoreControlRig(namespace, character, path)
        self.assertRestoreOrganization(namespace, path)

        # Validate skeleton has changed drastically
        self.assertSkeleton(skeletonValues, marginOfError=marginOfError)

    def assertRestoreSkeleton(self, namespace, path):
        """Tests the creation of a reference

        Args:
            namespace (str): Namespace to check
            path (str): Path to the reference
        """
        newPath = os.path.join(path, const.FileNamesDict.SKELETON_NAME)
        Organize.Organize(newPath, namespace=namespace, create=False)

    def assertRestoreControlRig(self, namespace, character, path):
        """Tests the creation of a reference

        Args:
            namespace (str): Namespace to check
            character (mobu.FBCharacter): Character to check
            path (str): Path to the reference
        """
        newPath = os.path.join(path, const.FileNamesDict.CONTROL_RIG_NAME)
        Organize.Organize(
            newPath, namespace=namespace, create=True, name=character.Name, removeByTypes=(mobu.FBCharacterExtension,)
        )

    def assertRestoreOrganization(self, namespace, path):
        """Tests the creation of a reference

        Arguments:
            path (str): Path to the reference
        """
        newPath = os.path.join(path, const.FileNamesDict.ORGANIZATION_NAME)
        Organize.Organize(newPath, namespace=namespace, create=False)

    def assertSkeleton(self, skeletonValues, marginOfError=0.0):
        """Asserts the current values of the skeleton hierarchy against the provided values at an earlier state within
           the same MotionBuilder session

        Args:
            skeletonValues (dict): translation and rotation values for each skeleton bone.
                                         Keys are FBModelSkeleton and the value is a list containing the values of
                                         the translation and rotation as lists.
            marginOfError (float, optional): Tolerance of match
        """
        bonesToValidate = self.testData.get("boneToValidate", None)
        if not bonesToValidate:
            model = self.assertRoot()
            bones = Scene.GetChildrenGenerator(model)
        else:
            bones = (mobu.FBFindModelByLabelName(boneName) for boneName in bonesToValidate)

        for bone in bones:
            propertyValues = skeletonValues[bone]
            for value1, propertyName in zip(propertyValues, ("Lcl Translation", "Lcl Rotation")):
                value2 = list(bone.PropertyList.Find(propertyName).Data)
                self.assertVectorsAlmostEquals(value1, value2, marginOfError=marginOfError)

    def assertVectorsAlmostEquals(self, vector1, vector2, marginOfError=0.0):
        """Checks the values of two vectors of equal length almost equal each other.

        Args:
            vector1 (list): vector to check against vector2
            vector2 (list): vector to check against vector1
            marginOfError (float, optional): the difference allowed between two values
        """
        for value1, value2 in zip(vector1, vector2):
            self.assertLessEqual(abs(value1 - value2), marginOfError)
