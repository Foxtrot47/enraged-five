###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_SelectionSetsToGroups.py
## Written And Maintained By: David Bailey
## Contributors: -
## Description: Creates Groups From SelectionSets.txt 
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
from pyfbsdk import *
import os
import RS.Config
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
gProjRoot = RS.Config.Project.Path.Root
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Creates Groups From SelectionSets.txt 
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
def rs_SelectionSetsToGroups():
    
    lFilePopup = FBFilePopup();
    lFilePopup.Filter = '*.txt'
    lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen
    lFilePopup.Path = gProjRoot + "\\art\\peds\\Story_Characters"
    
    lFilePath = ""
    
    if lFilePopup.Execute():
        lFilePath = lFilePopup.FullFilename
    else:
        return

    lSelectionSets = open(lFilePath, 'r')
    
    for iLine in lSelectionSets:
        
        lLineArray = []
        lLine = iLine[2:(len(line) - 2)]
        lLine = lLine.replace("#(", "", len(lLine))
        lLine = lLine.replace(")", "", len(lLine))
        lLine = lLine.replace(" ", "", len(lLine))
        lLine = lLine.replace("\"", "", len(lLine))
        lLinePartition = lLine.partition(",")
        lLineArray.append(lLinePartition[0])
        lLineArray.append(lLinePartition[2].partition(",")[0])
        lLineArray.append(lLinePartition[2].partition(",")[2].split(","))
        
        lGroup = FBGroup(lLineArray[1])
        for iModelName in lLineArray[2]:
            lModel = FBFindModelByLabelName(iModelName)
            if lModel:
                lGroup.Items.append(lModel)
