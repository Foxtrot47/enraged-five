
from pyfbsdk import *
import RS.Globals



def CleanGroups():

	emptyGroupList = []
	foundEmptyGroup = False

	for i in RS.Globals.Groups:
		if i.Items:

			if len( i.Items ) == 1 and i.Items[0].ClassName() == 'FBBox':
				emptyGroupList.append( i )   
				foundEmptyGroup = True
				
		else:
			emptyGroupList.append( i )   
			foundEmptyGroup = True			


	for i in emptyGroupList:

		try:
			i.FBDelete()
		except:
			pass

	if foundEmptyGroup == True:
		CleanGroups() 

CleanGroups()