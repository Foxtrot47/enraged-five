from pyfbsdk import *
import RS.Utils.Scene.AnimationNode
import RS.Globals as glo
import RS.Perforce
import RS.Utils.Scene.Take
from ctypes import *
from pyfbsdk_additions import *
import os


SCENE = FBSystem().Scene

##def addToList(self, str_to_add):
##    if str_to_add not in self.list_of_strings:
##        self.list_of_strings.append(str_to_add)

def findStanceTake():
    cont = False

    allTakes = FBSystem().Scene.Takes    
    for lTakeIdx in range( len( allTakes)):
        if allTakes[lTakeIdx].Name == "Stance":
            print "Found Stance take. Index:"+str(lTakeIdx)
##            cont = lTakeIdx
##            return cont
            
            return lTakeIdx + 1 ##ADDING ONE AS IF ITS 0 THEN MY SCRIPT FUCKS UP
##    if cont == False:
##        print "Couldn't find Stane take"                
##    return cont

def printProperties(obj):
    for prop in obj.PropertyList:
        print prop.Name


def rsta_ConnectBoxes(sender, senderAttr, receiver, receiverAttr):
    lSourceOut = FindAnimationNode( sender.AnimationNodeOutGet(), senderAttr )
    lDest = FindAnimationNode(receiver.AnimationNodeInGet(), receiverAttr)
    FBConnect(lSourceOut, lDest)

def findBoxAttr(box, attrName):
    returnVal = None
    for a in box.AnimationNodeOutGet().Nodes:
        if a.Name == attrName:
            print "Found attr: "+a.Name
            returnVal = a
        else:
            print a.Name+" != "+attrName
    return returnVal

def FindAnimationNode(pParent, pName):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            ##print "Found Node:" + lNode.Name
            break
        ##else:
            ##print "Not Needed/Found Node:" + lNode.Name
    return lResult

def appendIfUnique(array, nodeToAdd):
    if nodeToAdd not in array:
        array.append(nodeToAdd)
        ##print "Adding "+nodeToAdd.Name+" to array"
    else:
        print "NOT Adding "+nodeToAdd.Name+" to array as its already there"
    return array


def get_all_children(root, array):
    for c in root.Children:
        ##array.append(c)
        array = appendIfUnique(array, c)
        get_all_children(c, array)
    return array

def getNodeByName(useNameSpace, name, nameSpace, modelsOnly):
    returnNode = None

    cl = FBComponentList()
    searchString = name
    if nameSpace != None:
        searchString = nameSpace+":"+name
        
    FBFindObjectsByName(searchString, cl, useNameSpace, modelsOnly)     
    
    ##print "Number of matches: "+str(len(cl))
    if len(cl) == 1:
        returnNode = cl[0]
        ##print "Found "+cl[0].LongName
    else:
        if len(cl) == 0:
            print "Couldn't find "+nameSpace+":"+name
        else:
            print "Found multiple matches for "+name
        
    return returnNode

def createSelectionHandle(followObj, handleName):
    
    myHandle = FBHandle(handleName)
    
    propManager = myHandle.PropertyList
    
    ##printProperties(myHandle)
    
    myHandle.Show = True
    myHandle.HandlerShow3DModel = True
    myHandle.Handler3DModelLook = 2
    myHandle.Handler3DColor = FBColor(200,0,0)
    myHandle.Follow.append(followObj)
    myHandle.Manipulate.append(followObj)

    return myHandle

def rs_CreateFirstPersonRig(checkTake, lNameSpace):
    
    ##need to store the initial take and frame range as merging in the bits and pieces nafs this up.
    ## we will set the take and frame range back to these values at the end
    lCameraTimeRange = RS.Utils.Scene.Take.GetLocalTimeRange( FBSystem().CurrentTake )
    initialFrameRangeStart = lCameraTimeRange.Start
    initialFrameRangeEnd = lCameraTimeRange.End

    lSystem = FBSystem()    
    currTake = lSystem.CurrentTake.Name
     
    print "current Take: "+str(currTake)
    
    allTakes = FBSystem().Scene.Takes    
    initialTake = None
    for lTakeIdx in range( len( allTakes)):
        if allTakes[lTakeIdx].Name == currTake:
            initialTake = lTakeIdx    
    
    ## Need to ensure we have a stance take first
    foundStance = False
    if checkTake != False:
        print "Searching for Stance Take..."
        foundStance = findStanceTake()
    
##    if foundStance != False:
        ##ok we found the take so we need to make it the active take
    if foundStance == False:
        ##print "no stance take found..."
        FBMessageBox('WARNING!', 'Could not find a take named Stance. We cannot continue', 'OK')
    else:
        if len(allTakes) > 1:
            lSystem.CurrentTake = lSystem.Scene.Takes[(foundStance - 1)]

        
        ##now we will parse the scene for all the objects with lNamespace - we only want to operate on these
        validObjects = RS.Utils.Scene.FindObjectsByNamespace(lNameSpace)
        
        ##first of we'll merge in the camera cone with adjustable fov
        fbxFilePath = "x:\\gta5\\art\\peds\\firstPersonCamSetup\\firstPersonCone.FBX"
        RS.Perforce.Sync( fbxFilePath)
        FBApplication().FileMerge(fbxFilePath)
        
        FBSystem().Scene.Evaluate()
        allNodes = []
        allNodes = get_all_children(SCENE.RootModel, allNodes)        
        
        lNameSpaceB = "CONESTUFF_"
        
        coneMid = getNodeByName(True, "ConeMid", lNameSpaceB, True)
        coneMid.LongName =  lNameSpace+":"+coneMid.Name
        coneGeo = getNodeByName( True,"FirstPersonCone", lNameSpaceB, True)
        coneGeo.LongName = lNameSpace+":"+coneGeo.Name

        cameraPivot = getNodeByName(True, "FirstPersonCamPivot", lNameSpaceB, True)
        cameraPivot.LongName = lNameSpace+":"+cameraPivot.Name
        

        
        coneMaxX = getNodeByName(True, "ConeMaxX", lNameSpaceB, True)
        coneMaxX.LongName = lNameSpace+":"+coneMaxX.Name
        coneMinX = getNodeByName(True, "ConeMinX", lNameSpaceB, True)
        coneMinX.LongName = lNameSpace+":"+coneMinX.Name
        coneMaxZ = getNodeByName(True, "ConeMaxZ", lNameSpaceB, True)
        coneMaxZ.LongName = lNameSpace+":"+coneMaxZ.Name
        coneMinZ = getNodeByName(True, "ConeMinZ", lNameSpaceB, True)
        coneMinZ.LongName = lNameSpace+":"+coneMinZ.Name  

        constraints = FBSystem().Scene.Constraints
        for con in constraints:
            if con.LongName == "ConeFOV":
                con.LongName = lNameSpace+":"+con.LongName
                print "Found "+con.LongName        
                              
        
        camRoot = getNodeByName(True, "CamRoot", lNameSpaceB, True)
           
        cameraPivot.Show = True
        cameraPivot.Size = 600
        
        ##need to try and enable the attrs on the receiver for some reason
        propManager = cameraPivot.PropertyList
        dofProp = propManager.Find("DOF_MinX")
        dofProp.SetAnimated(True)
        dofProp = propManager.Find("DOF_MinY")
        dofProp.SetAnimated(True)        
        dofProp = propManager.Find("DOF_MinZ")
        dofProp.SetAnimated(True)
        dofProp = propManager.Find("DOF_MaxX")
        dofProp.SetAnimated(True)
        dofProp = propManager.Find("DOF_MaxY")
        dofProp.SetAnimated(True)
        dofProp = propManager.Find("DOF_MaxZ")
        dofProp.SetAnimated(True)          
        
        cameraZero = getNodeByName(True, "firstPersonCamZero", lNameSpaceB, True)
        if cameraZero == None:
            cameraZero = FBModelNull(lNameSpace+":"+"firstPersonCamZero") 
            print   "Had to generate new "+cameraZero.LongName
        else:
            cameraZero.LongName = lNameSpace+":"+cameraZero.Name
        
        skelHead = getNodeByName(True, "SKEL_Head", lNameSpace, False)
        
        skelNeck = getNodeByName(True, "SKEL_Neck_1", lNameSpace, False)
        
        skelRoot = getNodeByName(True, "SKEL_ROOT", lNameSpace, False)
    
        moverNode = getNodeByName(True, "mover", lNameSpace, True)       
        
    
        cameraPivotCam = FBCamera(lNameSpace+":"+"cameraPivotCam")
        cameraPivotCam.Show = True
  
    
    
        print "coneMid:       "+coneMid.LongName+" class:"+coneMid.ClassName()
        print "coneGeo:       "+coneGeo.LongName+" class:"+coneGeo.ClassName()
        print "cameraPivot:   "+cameraPivot.LongName+" class:"+cameraPivot.ClassName()
        print "camRoot:       "+camRoot.LongName+" class:"+camRoot.ClassName()
        print "cameraZero:    "+cameraZero.LongName+" class:"+cameraZero.ClassName()
        print "skelHead:      "+skelHead.LongName+" class:"+skelHead.ClassName()
        print "skelNeck:      "+skelNeck.LongName+" class:"+skelNeck.ClassName()
        print "moverNode:     "+moverNode.LongName+" class:"+moverNode.ClassName()
        print "cameraPivotCam:"+cameraPivotCam.LongName+" class:"+cameraPivotCam.ClassName()
##        print "camPivotHandle:"+camPivotHandle.LongName+" class:"+camPivotHandle.ClassName()
    
    
    
        lVector = FBVector3d()
        skelHead.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
    
        camRoot.Parent = skelRoot
        
        ##now we need to parent constrain the camRoot to the skelNeck
        camRootConstraint = FBConstraintManager().TypeCreateConstraint(3)
        camRootConstraint.Name = lNameSpace+":"+"camRootToNeckConstraint"
        camRootConstraint.ReferenceAdd(0,camRoot)
        camRootConstraint.ReferenceAdd(1,skelNeck)
        camRootConstraint.Active = True     
    
    
        cameraZero.Parent = skelNeck
        
        camRoot.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)   
        rVector = FBVector3d(0,0,0)        
        camRoot.SetVector(rVector, FBModelTransformationType.kModelRotation, False)       
    
        FBSystem().Scene.Evaluate()
             
        cameraPivot.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)       
        rVector = FBVector3d(-90,0,0)    

        cameraPivot.SetVector(rVector, FBModelTransformationType.kModelRotation, True)           
        
        propManager = cameraPivot.PropertyList    
        propManager.Find("Enable Rotation DOF").Data = True 
        propManager.Find("Pre Rotation").Data = FBVector3d(180,0,0)    
        propManager.Find("Post Rotation").Data = FBVector3d(0,-90,0)     
        
        FBSystem().Scene.Evaluate()
        
        cameraPivot.Parent = camRoot       
        
        FBSystem().Scene.Evaluate()
        
        ##******
        
        lVector = FBVector3d()        
        cameraPivot.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)    
        cameraPivotCam.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)  
        
        FBSystem().Scene.Evaluate()
        cameraPivotCam.Parent = cameraPivot         
        
        rVector = FBVector3d(90,180,90)
        cameraPivotCam.SetVector(rVector, FBModelTransformationType.kModelRotation, False)                    
        
        cameraZero.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)   
                            
        coneGeo.ShadingMode = FBModelShadingMode.kFBModelShadingWire
        
        tVec = FBVector3d(0,0,0)
        cameraPivot.SetVector(tVec, FBModelTransformationType.kModelTranslation, False) 
        rVec = FBVector3d(0,0,0)
        cameraPivot.SetVector(tVec, FBModelTransformationType.kModelRotation, False) 
        FBSystem().Scene.Evaluate()
        
        rVector = FBVector3d(0,0,0)
        cameraZero.SetVector(rVector, FBModelTransformationType.kModelRotation, False)
        
    
        ##now what I need to do is make a new null which is a child of moverNode & translated 
        ##to moverNode position but rotated to match cameraZero
        moverZero = FBModelNull(lNameSpace+":"+"moverZero")       
        moverZero.Parent = moverNode
        FBSystem().Scene.Evaluate()
        
        rVec = FBVector3d(0,0,180)        
        tVec = FBVector3d()
        moverNode.GetVector(tVec, FBModelTransformationType.kModelTranslation, True)    
        moverZero.SetVector(tVec, FBModelTransformationType.kModelTranslation, True)   
        moverZero.SetVector(rVec, FBModelTransformationType.kModelRotation, False)   
        
        FBSystem().Scene.Evaluate()

#######################################################################################################
    
        propManager = cameraZero.PropertyList    
        propManager.Find("Enable Transformation").Data = False

        #Custom Property Functions for DOFS
    
        propManager = coneMid.PropertyList
        propManager.Find("Enable Transformation").Data = False   
        propManager = coneGeo.PropertyList
        propManager.Find("Enable Transformation").Data = False   
        
              
        camPivotHandle = createSelectionHandle(cameraPivot, lNameSpace+":"+"cameraPivotHandle")                            

#######################################################################################################
##                              NOW DO THE CONSTRAINTS                  ###############################
#######################################################################################################
    
    
        ##now we need to connect the DOF property on the cameraCone node to the limitMax property via a relation constraint
        lRelationCon = FBConstraintRelation(lNameSpace+":"+'limit_firstPersonCamPivot')
    
        ##Create all the boxes
        
        firstPersonCamPivotSender = lRelationCon.SetAsSource(cameraPivot)
        firstPersonCamPivotSender.UseGlobalTransforms = False
        lRelationCon.SetBoxPosition(firstPersonCamPivotSender, -200, 400)
            
        lVectToNum = lRelationCon.CreateFunctionBox( 'Converters', 'Vector to Number' )
        lRelationCon.SetBoxPosition( lVectToNum, 300, 550 )    
        
        isgA = lRelationCon.CreateFunctionBox("Number", "Is Greater or Equal (a >= b)")
        lRelationCon.SetBoxPosition( isgA, 1100, 300)
        
        islA = lRelationCon.CreateFunctionBox("Number", "Is Less or Equal (a <= b)")
        lRelationCon.SetBoxPosition( islA, 1600, 50)  
        
        ifA = lRelationCon.CreateFunctionBox("Number", "IF Cond Then A Else B")
        lRelationCon.SetBoxPosition( ifA, 1600, 200 )  
        
        ifB = lRelationCon.CreateFunctionBox("Number", "IF Cond Then A Else B")
        lRelationCon.SetBoxPosition(ifB, 2200, 150 )  
        
        numToVec = lRelationCon.CreateFunctionBox( 'Converters', 'Number to Vector' )
        lRelationCon.SetBoxPosition(numToVec, 2750, 650 )    
        
        islB = lRelationCon.CreateFunctionBox("Number", "Is Less or Equal (a <= b)")
        lRelationCon.SetBoxPosition( islB, 1100, 1000 ) 
        
        isgB = lRelationCon.CreateFunctionBox("Number", "Is Greater or Equal (a >= b)")
        lRelationCon.SetBoxPosition( isgB, 1100, 850)
        
        ifC = lRelationCon.CreateFunctionBox("Number", "IF Cond Then A Else B")
        lRelationCon.SetBoxPosition( ifC, 1600, 850 )  
        
        ifD = lRelationCon.CreateFunctionBox("Number", "IF Cond Then A Else B")
        lRelationCon.SetBoxPosition( ifD, 2200, 850)  
        
        isgC = lRelationCon.CreateFunctionBox("Number", "Is Greater or Equal (a >= b)")
        lRelationCon.SetBoxPosition( isgC, 1100, 500)
        
        islC = lRelationCon.CreateFunctionBox("Number", "Is Less or Equal (a <= b)")
        lRelationCon.SetBoxPosition( islC, 1600, 650 )  
        
        ifE =  lRelationCon.CreateFunctionBox("Number", "IF Cond Then A Else B")
        lRelationCon.SetBoxPosition( ifE, 1600, 450 )        

        ifF =  lRelationCon.CreateFunctionBox("Number", "IF Cond Then A Else B")
        lRelationCon.SetBoxPosition( ifF, 2200, 600 )         
        
        receiverObj = lRelationCon.ConstrainObject(cameraPivot)
        receiverObj.UseGlobalTransforms = False
        lRelationCon.SetBoxPosition(receiverObj, 3200, 500)   
                  
        ##Now connect all the boxes          

        rsta_ConnectBoxes(firstPersonCamPivotSender, "Lcl Rotation", lVectToNum, "V")
        
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MinX", isgA, "b")
        rsta_ConnectBoxes(lVectToNum, "X", isgA, "a")    
              
        rsta_ConnectBoxes(lVectToNum, "X", islA, "a")     
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MaxX", islA, "b")     
     
        rsta_ConnectBoxes(lVectToNum, "X", ifA, "a") 
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MinX", ifA, "b")     
        rsta_ConnectBoxes(isgA, "Result", ifA, "Cond")   
        
        rsta_ConnectBoxes(ifA, "Result", ifB, "a")  
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MaxX", ifB, "b")          
        rsta_ConnectBoxes(islA, "Result", ifB, "Cond")          
            
        rsta_ConnectBoxes(lVectToNum, "Z", islB, "a")    
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MaxZ", islB, "b")    #orig max
        
        rsta_ConnectBoxes(lVectToNum, "Z", isgB, "a")         
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MinZ", isgB, "b")    #orig min
            
        rsta_ConnectBoxes(lVectToNum, "Z", ifC, "a")     
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MinZ", ifC, "b")       #orig min
        rsta_ConnectBoxes(isgB, "Result", ifC, "Cond")      
    
        rsta_ConnectBoxes(ifC, "Result", ifD, "a")  
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MaxZ", ifD, "b")  #orig max
        rsta_ConnectBoxes(islB, "Result", ifD, "Cond")  
           
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MaxY", islC, "b")
        rsta_ConnectBoxes(lVectToNum, "Y", islC, "a")
        
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MinY", isgC, "b")
        rsta_ConnectBoxes(lVectToNum, "Y", isgC, "a")        
        
        rsta_ConnectBoxes(lVectToNum, "Y", ifE, "a")        
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MinY", ifE, "b")
        rsta_ConnectBoxes(isgC, "Result", ifE, "Cond")
        
        rsta_ConnectBoxes(ifE, "Result", ifF, "a")
        rsta_ConnectBoxes(firstPersonCamPivotSender, "DOF_MaxY", ifF, "b")
        rsta_ConnectBoxes(islC, "Result", ifF, "Cond")
        
        rsta_ConnectBoxes(ifB, "Result", numToVec, "X")  
        rsta_ConnectBoxes(ifF, "Result", numToVec, "Y")          
        rsta_ConnectBoxes(ifD, "Result", numToVec, "Z")          
        
        rsta_ConnectBoxes(numToVec, "Result", receiverObj, "Lcl Rotation")    
    
        ##now turn on the constraint
        lRelationCon.Active = True
    
#######    *******************
        rVec = FBVector3d(0,-90,0)        
        moverZero.SetVector(rVec, FBModelTransformationType.kModelRotation, False)    
    
        ##now we need to add a constraint to constrain the camRoot between the cameraZero rotation and the mover rotation.
        rotConstA = FBConstraintManager().TypeCreateConstraint(10)
        rotConstA.Name = (lNameSpace+":"+"CamZero_Rot_Dominance")
        rotConstA.ReferenceAdd (0,camRoot)
        rotConstA.ReferenceAdd (1,cameraZero)            
        rotConstA.Active = True
        rotConstA.Lock = True             
    

        rotConstB = FBConstraintManager().TypeCreateConstraint(10)
        rotConstB.Name = (lNameSpace+":"+"Mover_Rot_Dominance")
        rotConstB.ReferenceAdd (0,camRoot)
        rotConstB.ReferenceAdd (1,moverZero)      
        rotConstB.Active = True
        rotConstB.Lock = True                   
                   
        
        ##now we need to merge in a joystick for allowing user to slide the weight between the two constraints.
##        js_fbxFilePath = "X:\\gta5\\art\\peds\\firstPersonCamSetup\\joystick.FBX"
##        RS.Perforce.Sync(js_fbxFilePath)
##        FBApplication().FileMerge(js_fbxFilePath)
    
        FBSystem().Scene.Evaluate()
        allNodes = []
        allNodes = get_all_children(SCENE.RootModel, allNodes)           
    
        js_Ctrl = getNodeByName(True, "CTRL_MoverWeighting", lNameSpaceB, True)
        js_Ctrl.LongName = lNameSpace+":"+js_Ctrl.Name
        
        tVector = FBVector3d(0,1,0)
        js_Ctrl.SetVector(tVector, FBModelTransformationType.kModelTranslation, False)   
        js_Rect = getNodeByName(True, "RECT_MoverWeighting", lNameSpaceB, True)
        js_Rect.LongName = lNameSpace+":"+js_Rect.Name
        js_Text = getNodeByName(True, "TEXT_MoverWeighting", lNameSpaceB, True)
        js_Text.LongName = lNameSpace+":"+js_Text.Name
        
        js_RectParent = FBModelNull(lNameSpace+":"+"RectParent") 
        FBSystem().Scene.Evaluate()          
        
        ##now we need to parent constrain the js_Rect object to the cameraPivotCam
        # Create the gun root control parent constraint
        camConstraint = FBConstraintManager().TypeCreateConstraint(3)
        camConstraint.Name = lNameSpace+":"+"camToJoystick"
        camConstraint.ReferenceAdd(0,js_RectParent)
        camConstraint.ReferenceAdd(1,cameraPivotCam)
        camConstraint.Active = True 
        
        recVec = FBVector3d(-5,164.55,9)
        js_RectParent.SetVector(recVec, FBModelTransformationType.kModelTranslation, False)                
        recVec = FBVector3d(0,0,0)
        js_RectParent.SetVector(recVec, FBModelTransformationType.kModelRotation, True)  
        js_Rect.Parent = js_RectParent
        FBSystem().Scene.Evaluate()   
                
        ##we may need to offset this node a bit   

        rRotVec = FBVector3d(90,0,-180)
        js_Rect.SetVector(rRotVec, FBModelTransformationType.kModelRotation, False)  
                
        scaleVector = FBVector3d(3,3,3)   
        js_Rect.SetVector(scaleVector, FBModelTransformationType.kModelScaling, True)  
        FBSystem().Scene.Evaluate()   

        recVec = FBVector3d(0,0,0)
        js_Rect.SetVector(recVec, FBModelTransformationType.kModelTranslation, False)
        FBSystem().Scene.Evaluate()      
        
        ##god knows why but i have to set these twice.
        recVec = FBVector3d(0,0,0)
        js_RectParent.SetVector(recVec, FBModelTransformationType.kModelRotation, True)  
        recVec = FBVector3d(-5,164.55,9)
        js_RectParent.SetVector(recVec, FBModelTransformationType.kModelTranslation, False)                        
        ##now we need to activate the animatable params on the weights of the constraints so they can actually be driven    
        for prop in rotConstA.PropertyList:
            if prop.Name == "Weight":
                ##print "Found Weight!"
                prop.SetAnimated(True)
        
        for prop in rotConstB.PropertyList:
            if prop.Name == "Weight":
                ##print "Found Weight!"
                prop.SetAnimated(True)
    
        ##now we will build the actual relation constraint
        weightRelationCon = FBConstraintRelation(lNameSpace+":"+'Dominance')
        
        weightSender = weightRelationCon.SetAsSource(js_Ctrl)
        weightSender.UseGlobalTransforms = False    
        weightRelationCon.SetBoxPosition(weightRelationCon, 0, 150)    
        
        lVectToNum = weightRelationCon.CreateFunctionBox( 'Converters', 'Vector to Number' )
        weightRelationCon.SetBoxPosition( lVectToNum, 600, 150 )        

        
        addA = weightRelationCon.CreateFunctionBox("Number", "Add (a + b)")
        weightRelationCon.SetBoxPosition(addA, 1000, 50 )     

        subA = weightRelationCon.CreateFunctionBox("Number", "Subtract (a - b)")
        weightRelationCon.SetBoxPosition(subA, 1000, 400 )     
        
        multA = weightRelationCon.CreateFunctionBox("Number", "Multiply (a x b)")
        weightRelationCon.SetBoxPosition(multA, 1400, 250 )     

        multB = weightRelationCon.CreateFunctionBox("Number", "Multiply (a x b)")
        weightRelationCon.SetBoxPosition(multB, 1400, 500 )     

        multC = weightRelationCon.CreateFunctionBox("Number", "Multiply (a x b)")
        weightRelationCon.SetBoxPosition(multC, 2000, 550 )     

        
        receiverA = weightRelationCon.ConstrainObject(rotConstA)
        weightRelationCon.SetBoxPosition(receiverA, 1800, 150)       
    
        receiverB = weightRelationCon.ConstrainObject(rotConstB)
        weightRelationCon.SetBoxPosition(receiverB, 2400, 500)   
        
            
        ##now connect all the boxes
        rsta_ConnectBoxes(weightSender, "Lcl Translation", lVectToNum, "V")    
        rsta_ConnectBoxes(lVectToNum, "Y", addA, "a")   
        lAddA_B = RS.Utils.Scene.FindAnimationNode(addA.AnimationNodeInGet(),'b')
        lAddA_B.WriteData([1])                       
        rsta_ConnectBoxes(addA, "Result", multA, "a")                
        
        rsta_ConnectBoxes(lVectToNum, "Y", subA, "a")            
        subA_b = RS.Utils.Scene.FindAnimationNode(subA.AnimationNodeInGet(),'b')
        subA_b.WriteData([1])                    
        rsta_ConnectBoxes(subA, "Result", multB, "a")   
        
        multA_b = RS.Utils.Scene.FindAnimationNode(multA.AnimationNodeInGet(),'b')
        multA_b.WriteData([50])
        rsta_ConnectBoxes(multA, "Result", receiverA, "Weight")
        
        multB_b = RS.Utils.Scene.FindAnimationNode(multB.AnimationNodeInGet(),'b')
        multB_b.WriteData([-1])
        rsta_ConnectBoxes(multB, "Result", multC, "a")

        multC_b = RS.Utils.Scene.FindAnimationNode(multC.AnimationNodeInGet(),'b')
        multC_b.WriteData([50])
        rsta_ConnectBoxes(multC, "Result", receiverB, "Weight")
                
        ##now turn on the constraint
        weightRelationCon.Active = True             
    
        ##now clamp the joysticks movement
        weightLimitCon = FBConstraintRelation(lNameSpace+":"+'LimitJs')
        
        weightSender = weightLimitCon.SetAsSource(js_Ctrl)
        weightSender.UseGlobalTransforms = False    
        weightLimitCon.SetBoxPosition(weightRelationCon, -250, -250)    
        
        lVectToNum = weightLimitCon.CreateFunctionBox( 'Converters', 'Vector to Number' )
        weightLimitCon.SetBoxPosition( lVectToNum, 50, 0 )   
        
        isless = weightLimitCon.CreateFunctionBox("Number", "Is Less or Equal (a <= b)")
        weightLimitCon.SetBoxPosition( isless, 400, 500 )      
        
        isGreater = weightLimitCon.CreateFunctionBox("Number", "Is Greater or Equal (a >= b)")
        weightLimitCon.SetBoxPosition(isGreater, 300, 300)    
        
        ifA = weightLimitCon.CreateFunctionBox("Number", "IF Cond Then A Else B")
        weightLimitCon.SetBoxPosition( ifA, 500, -250 )      
        
        ifB = weightLimitCon.CreateFunctionBox("Number", "IF Cond Then A Else B")
        weightLimitCon.SetBoxPosition( ifB, 500, -250 )          
        
        numToVec = weightLimitCon.CreateFunctionBox( 'Converters', 'Number to Vector' )
        weightLimitCon.SetBoxPosition(numToVec, 1200, 0 )    
    
        weightReceiver = weightLimitCon.ConstrainObject(js_Ctrl)
        weightReceiver.UseGlobalTransforms = False        
        weightLimitCon.SetBoxPosition(weightReceiver, 500, 250)
            
        ##now connect all the boxes
        rsta_ConnectBoxes(weightSender, "Lcl Translation", lVectToNum, "V")    
        
        rsta_ConnectBoxes(lVectToNum, "Y", isless, "a")   
        maxVal = RS.Utils.Scene.FindAnimationNode(isless.AnimationNodeInGet(),'b')
        maxVal.WriteData([1])                                
        
        rsta_ConnectBoxes(lVectToNum, "Y", isGreater, "a")            
        minVal = RS.Utils.Scene.FindAnimationNode(isGreater.AnimationNodeInGet(),'b')
        minVal.WriteData([-1])                                    
        
        rsta_ConnectBoxes(isless, "Result", ifA, "Cond")       
        rsta_ConnectBoxes(ifB, "Result", ifA, "a")
        maxVal = RS.Utils.Scene.FindAnimationNode(ifA.AnimationNodeInGet(),'b')
        maxVal.WriteData([1])                                
        
        rsta_ConnectBoxes(lVectToNum, "Y", ifB, "a")
        minVal = RS.Utils.Scene.FindAnimationNode(ifB.AnimationNodeInGet(),'b')
        minVal.WriteData([-1])                          
        rsta_ConnectBoxes(isGreater, "Result", ifB, "Cond")
        
        rsta_ConnectBoxes(ifA, "Result", numToVec, "Y")
        rsta_ConnectBoxes(numToVec, "Result", weightReceiver, "Lcl Translation")
           
        ##now turn on the constraint     
        weightLimitCon.Active = True          

        ##Now we need to lock the cameraPivotCam so it cannot be accidentally moved when using the dolly controls
        
        ##now clamp the joysticks movement
        cameraLockCon = FBConstraintRelation(lNameSpace+":"+'CameraLock')   
    
        camReceiver = cameraLockCon.ConstrainObject(cameraPivotCam)
        camReceiver.UseGlobalTransforms = False        
        cameraLockCon.SetBoxPosition(camReceiver, 0, 0)  
        
        rotVal = RS.Utils.Scene.FindAnimationNode(camReceiver.AnimationNodeInGet(),'Lcl Rotation')
        rotVal.WriteData([90,0,-90])                   

        sclVal = RS.Utils.Scene.FindAnimationNode(camReceiver.AnimationNodeInGet(),'Lcl Scaling')
        sclVal.WriteData([0.01,0.01,0.01])     
        
        traVal = RS.Utils.Scene.FindAnimationNode(camReceiver.AnimationNodeInGet(),'Lcl Translation')
        traVal.WriteData([0,0,0])  
        
        cameraLockCon.Active = True            
        
        ##now we need to relation constrain the attrs together (because having the attrs on the pivot node inside the hierarchy means they dont export
        attrCon = FBConstraintRelation(lNameSpace+":"+'DOFAttrs')              
        
        attrSender = attrCon.SetAsSource(cameraPivot)
        
        attrRcvr = getNodeByName(True, "FirstPersonCamPivotAttrs", lNameSpaceB, True)
        attrRcvr.LongName = (lNameSpaceB+":"+attrRcvr.Name)
        
        attrReceiver = attrCon.ConstrainObject(attrRcvr)
        attrCon.SetBoxPosition( attrReceiver, 600, 100 )   
             
            
        
        ##now connect the attrs 
        rsta_ConnectBoxes(attrSender, "DOF_MinX", attrReceiver, "DOF_MinX")    
        rsta_ConnectBoxes(attrSender, "DOF_MinY", attrReceiver, "DOF_MinY")
        rsta_ConnectBoxes(attrSender, "DOF_MinZ", attrReceiver, "DOF_MinZ")
        rsta_ConnectBoxes(attrSender, "DOF_MaxX", attrReceiver, "DOF_MaxX")
        rsta_ConnectBoxes(attrSender, "DOF_MaxY", attrReceiver, "DOF_MaxY")
        rsta_ConnectBoxes(attrSender, "DOF_MaxZ", attrReceiver, "DOF_MaxZ")
        
        attrCon.Active = True
        
        #############            
        
        FBSystem().Scene.Evaluate()              
        
        FBSystem().Scene.Evaluate()
        
        ##now set frame range and back to initial take and ranges queried at start 
        
        lSystem.CurrentTake = lSystem.Scene.Takes[initialTake]
        
        FBPlayerControl().LoopStart = initialFrameRangeStart
        FBPlayerControl().LoopStop = initialFrameRangeEnd
        
        ##for some god known reason if i dont set the translation here back to 0 the cam gets offset
        tVec = FBVector3d(0,0,0)
        cameraPivot.SetVector(tVec, FBModelTransformationType.kModelTranslation, False) 
        FBSystem().Scene.Evaluate()   
        
        for lComp in FBSystem().Scene.Components:
            # This function is a recursive function that will go through the whole hierarchy to add or replace the prefix
            lComp.ProcessNamespaceHierarchy (FBNamespaceAction.kFBReplaceNamespace, lNameSpaceB, lNameSpace, False) 
            
        for lTakeIdx in range( len( allTakes)):
            if allTakes[lTakeIdx].Name == currTake:
                lSystem.CurrentTake = lSystem.Scene.Takes[lTakeIdx]        



def transferAnim(sourceObj, targetObj):

    propWeWantToTransfer = [
    "DOF_MaxX",
    "DOF_MaxY",
    "DOF_MaxZ",
    "DOF_MinX",
    "DOF_MinY",
    "DOF_MinZ",
    "Lcl Translation",
    "Lcl Rotation"    
    ]
    
    for SourceProperty in sourceObj.PropertyList:
        for TargetProperty in targetObj.PropertyList:
            if TargetProperty.Name == SourceProperty.Name:
                for propName in propWeWantToTransfer:
                    if propName == TargetProperty.Name:
                        if issubclass( type(SourceProperty), FBPropertyAnimatable) and SourceProperty.IsAnimated():
                            ##print ("Property Name on "+targetObj.LongName+": "+TargetProperty.Name)
                            if RS.Utils.Scene.Property.HasKeys( SourceProperty ):
                                RS.Utils.Scene.Property.CopyAnimationData( SourceProperty, TargetProperty )
                                print "Transferred Animation on "+SourceProperty.Name+" from "+sourceObj.LongName+" to "+targetObj.LongName 
                            ##raise Exception("No keys found on "+SourceProperty.Name+" on "+sourceObj.LongName)
                            else:
                                tmp = 1
                                ##print ("No keys found on "+SourceProperty.Name+" on "+sourceObj.LongName)
                        else:
                            if RS.Utils.Scene.Property.IsSupportedCopyType(TargetProperty):
        
                                "Poperty.Name: "+TargetProperty.Name+" type:"+str(type(TargetProperty))
                                TargetProperty.Data = SourceProperty.Data
                                print "Copied value for "+SourceProperty.Name+" from "+sourceObj.LongName+" to "+targetObj.LongName                    
                            else:
                                print "Couldn't copy "+SourceProperty.Name+" as it was read only."
                    else:
                        temp = 1
    

def removeExistingFirstPersonSetup(obj, lNameSpace):
    
    print "Attempting to remove existing First Person camera setup for "+lNameSpace
    
    itemsToRemove = [
    "ConeMid",
    "FirstPersonCone",
    "FirstPersonCamPivot",
    "CamRoot",
    "firstPersonCamZero",
    "cameraPivotCam",
    "cameraPivotHandle",
    "FirstPersonCone",
    "RECT_MoverWeighting",
    "TEXT_MoverWeighting",
    "CTRL_MoverWeighting",
    "moverZero",
    "ConeMaxZ",
    "ConeMinZ",    
    "ConeMinX",
    "ConeMaxX",
    "RectParent",
    "FirstPersonCamPivotAttrs"
    ]
    
    constraintsToRemove = [
    "ConeFOV",
    "limit_firstPersonCamPivot",
    "CamZero_Rot_Dominance",
    "Mover_Rot_Dominance",
    "Dominance",
    "LimitJs",
    "CameraLock",
    "camToJoystick",
    "DOFAttrs",
    "camRootToNeckConstraint",
    "DEOF_Relation"
    ]    
    
    foundItems = []
    foundConstraints = []
    
    constraints = FBSystem().Scene.Constraints
        
    if lNameSpace != None:
        print "Searching using namespace: "+lNameSpace
        ##search objects
        for item in itemsToRemove:
            if item == "cameraPivotHandle":
                thisNode = getNodeByName(True, item, lNameSpace, False)
                if thisNode != None:
                    foundItems.append(thisNode)                
                    print "Found HANDLE "+item+": "+thisNode.LongName+" class:"+thisNode.ClassName()
                else:
                    print "Couldn't find "+item                
            else:
                thisNode = getNodeByName(True, item, lNameSpace, True)
                if thisNode != None:
                    foundItems.append(thisNode)                
                    print "Found OBJECT "+item+": "+thisNode.LongName+" class:"+thisNode.ClassName()
                else:
                    print "Couldn't find "+item
        ##now search for constraints
        for conToRemove in constraintsToRemove:
            for con in constraints:
                if con.LongName == lNameSpace+":"+conToRemove:
                    foundConstraints.append(con) 
                    print "Found "+con.LongName+" constrraint"
    else:
        print "Searching without namespace"
        ##search for object things
        for item in itemsToRemove:
            if item == "cameraPivotHandle":
                thisNode = getNodeByName(True, item, lNameSpace, False)
                if thisNode != None:
                    foundItems.append(thisNode)                
                    print "Found HANDLE "+item+": "+thisNode.LongName+" class:"+thisNode.ClassName()
                else:
                    print "Couldn't find "+item                
            else:            
                thisNode = getNodeByName(False, item, None, True)
                if thisNode != None:
                    foundItems.append(thisNode)                
                    print "Found OBJECT "+item+": "+thisNode.LongName+" class:"+thisNode.ClassName()
                else:
                    print "Couldn't find "+item            
        ##now search constraints
        for conToRemove in constraintsToRemove:
            for con in constraints:
                if con.LongName == conToRemove:
                    foundConstraints.append(con)
                    print "Found "+con.LongName+" constrraint"

    delItems = []
    for i in foundItems:
        print "Changing "+i.LongName+" to "+"FOR_REMOVAL_:"+i.LongName
        i.LongName = "FOR_REMOVAL_:"+i.LongName
        
        delItems.append(i)
        
    for i in foundConstraints:
        print "Changing "+i.LongName+" to "+"FOR_REMOVAL_:"+i.LongName                
        i.LongName = "FOR_REMOVAL_:"+i.LongName
        delItems.append(i)
    return delItems
        
def initialiseFirstPerson(obj):        
       
    if obj != None: 
        fndTake = False
        fndTake = findStanceTake()
##        if fndTake != False:
        if fndTake == False:
            print "fndTake: "+str((fndTake - 1))
            ##FBMessageBox('WARNING!', 'Could not find a take named Stance. We cannot continue', 'OK')
        else:
            print "fndTake: "+str((fndTake - 1))            
            lNameSpace = ""
            if obj.OwnerNamespace != None:
                lNameSpace = obj.OwnerNamespace.Name
                print ("Using namespace "+lNameSpace)            
            else:
                obj.Name+" has no namespace"
            
            removeOldFPRig = 1
            existingNodes = []
            
            if removeOldFPRig == 1:
                print 'yes'
                existingNodes = removeExistingFirstPersonSetup(obj, lNameSpace)
                
            else:
                print 'no'     
            
            onlyApplyIfStancePoseTakeFound = True ##this is a debug param - set to false when working on a test scene. 
            
            rs_CreateFirstPersonRig(onlyApplyIfStancePoseTakeFound, lNameSpace)     
            
            print "Rig done"
        
            lSystem = FBSystem()      
            allTakes = FBSystem().Scene.Takes            
            
            contIn = True
    
            if len(allTakes) > 1:
                for thisTake in range( len(allTakes)):  
                    lSystem.CurrentTake = lSystem.Scene.Takes[thisTake]                 
                    print "Take index "+str(thisTake)+"------------------------------------------------------------\n" 
                    
                    for iLayerIndex in range( 0 , lSystem.CurrentTake.GetLayerCount() ):    
                        lCurrentLayer = lSystem.CurrentTake.GetLayer( iLayerIndex )
                        if len(existingNodes) > 0:
                            
                            for i in existingNodes:
                               ## print ("Trying to find matches for "+i.Name+" type:"+str(type(i)))
                                
                                removeNameSpace = "FOR_REMOVAL_:"
                                sourceObj  = i
                                endInd = len(i.LongName)
                                tgtName = i.LongName[13:endInd]
                                endInd = len(tgtName)
                                nameSpaceLength = len(lNameSpace)
                                tgtName = tgtName[(nameSpaceLength+1):endInd]
                                ##print "tgtName:"+tgtName
                                
                                targetObj = getNodeByName (True, tgtName, lNameSpace, True)
                
                                ## NEED TO DO THIS FOR EACH TAKE
                                
                                if sourceObj != None:
                                    if targetObj != None:
                                        print ("Transferring from "+sourceObj.LongName+" to "+targetObj.LongName)
                            
                                        transferAnim(sourceObj, targetObj)                        
                                        
                                        
                                    else:
                                        print "Couldn't find "+tgtName+" targetObject"
                                       ## contIn = False
                                    
                                else:
                                    print "Couldn't find "+removeNameSpace+i.LongName+" sourceObject"
    
            ###################################################################################
            ##DELETE THE OLD CRAP NOW
            ###################################################################################
        
            if contIn == True:    
                for i in reversed(existingNodes):
                    try:
                        thisNode = i
                        print "Deleting item "+thisNode.LongName
                        thisNode.FBDelete()
                    except:
                        pass
                        
            ############################
            ### NOW WE'LL ADD THESE ATTRS INTO THE ADD MODEL WINDOW
            ############################   
            custAttrs = [
            "FirstPersonCamPivotAttrs_DOF_MaxX",
            "FirstPersonCamPivotAttrs_DOF_MaxY",
            "FirstPersonCamPivotAttrs_DOF_MaxZ",
            "FirstPersonCamPivotAttrs_DOF_MinX",
            "FirstPersonCamPivotAttrs_DOF_MinY",
            "FirstPersonCamPivotAttrs_DOF_MinZ"
            ]
                    
            attrNode = getNodeByName(True, "FirstPersonCamPivotAttrs", lNameSpace, True)
            skelRoot = getNodeByName(True, "SKEL_ROOT", lNameSpace, True) 
            
            for catt in custAttrs:
                cdll.rexMBRage.AddAdditionalModelEntry_Py( skelRoot.LongName, attrNode.LongName, "facialControl", catt) 
                print "Added attribute "+catt+" on "+skelRoot.LongName
       
            ##now we need to rename the actual custom properties to these
            for property in attrNode.PropertyList:
                # The following name can be used to obtain a property in FBComponent.PropertyList.Find(<name>)
                if property.Name == "DOF_MaxX":
                    print property.Name
                    property.Name = "FirstPersonCamPivotAttrs_DOF_MaxX"
                if property.Name == "DOF_MaxY":
                    print property.Name
                    property.Name = "FirstPersonCamPivotAttrs_DOF_MaxY"    
                if property.Name == "DOF_MaxZ":
                    print property.Name
                    property.Name = "FirstPersonCamPivotAttrs_DOF_MaxZ"        
                if property.Name == "DOF_MinX":
                    print property.Name
                    property.Name = "FirstPersonCamPivotAttrs_DOF_MinX"
                if property.Name == "DOF_MinY":
                    print property.Name
                    property.Name = "FirstPersonCamPivotAttrs_DOF_MinY"
                if property.Name == "DOF_MinZ":
                    print property.Name
                    property.Name = "FirstPersonCamPivotAttrs_DOF_MinZ"    
##        else:
##            FBMessageBox('WARNING!', 'Could not find a take named Stance. We cannot continue', 'OK')            
            
    else:
        FBMessageBox("ERROR!", "Please pick the Dummy boject of the character you wish to add FP rig to.", "OK")



def startUpScript():
    
    lModelList = FBModelList()
        
    # Get the selected models.
    FBGetSelectedModels( lModelList )
    
    if len( lModelList) == 1:
        ##FBMessageBox("Message", "You picked something", "OK", None, None)
        obj = lModelList[0]
        if obj.Name == "Dummy01":
            print "Running on "+obj.Name  
            allTakes = FBSystem().Scene.Takes   
            lSystem = FBSystem()    
            currTake = lSystem.CurrentTake.Name
            
            
            initialiseFirstPerson(obj)
            
            initialiseFirstPerson(obj)
            
            allTakes = FBSystem().Scene.Takes   
            
            for lTakeIdx in range( len( allTakes)):
                if allTakes[lTakeIdx].Name == currTake:
                    lSystem.CurrentTake = lSystem.Scene.Takes[lTakeIdx]
            
        else:
            print "Please select your Dummy01"
            FBMessageBox("ERROR!", "Please select your Dummy01", "OK")
    else:
        FBMessageBox("ERROR!", "Please pick the Dummy object of the character you wish to add First Person rig to.", "OK")
        print "Nothing picked. Please pick the Dummy for the character you need"


startUpScript()

##startUpScript()