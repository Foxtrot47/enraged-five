import os, re, math
from zipfile import ZipFile
import pyfbsdk as mobu
from RS import Config, Globals, Perforce
from RS._Perforce import servers
from RS.Core.Animation.DataManager import Parser
from RS.Utils import Path


class SkeletonObject(Parser.AbstractObject):
    """
    Opens .skel file and stores data into a class instance in order to have an easy interface to
    access the data with.
    """
    Root = None
    BoneList = []

    def ParseData(self):
        """
        Reads the .skel file and stores the data into the newly generated class instance
        """
        # Clear data
        self.BoneList = []

        split_expression = re.compile("[\t|:| ]+")

        self.Children = []
        current_bone = self

        for each_line in self.Data:
            # each_line = each_line.replace("\t", "")
            values = split_expression.split(each_line)
            values = filter(None, values)

            if "bone" in values[0]:
                values[0] = values[0].replace("bone", "")

            if "{" in values:
                bone = BoneObject()
                bone.Number = int(values[0])
                bone.Name = values[1]

                bone.Parent = current_bone
                current_bone.Children.append(bone)
                self.BoneList.append(bone)

                current_bone = bone
                continue
            
            elif "euler" in values:
                current_bone.orientation = (values[1], values[2], values[3])
            
            elif "offset" in values:
                current_bone.offset = (values[1], values[2], values[3])

            elif "id" in values:
                bone.Index = int(values[-1])

            elif "}" in values:
                current_bone = current_bone.Parent
                continue

            if isinstance(current_bone, SkeletonObject):
                setattr(self, values[0], values[-1])
                continue

            if re.search("(trans|rot)[XYZ]", each_line):
                current_bone.attribute = values
                continue

            setattr(self, values[0], [float(each) for each in  values[1:]])

        self.Root = self.Children[0]
        delattr(self, "Children")

    @property
    def Id(self):
        """
        The id of the skeleton

        Returns:
            int
        """
        return self.Index

    @property
    def BoneIds(self):
        """
        A list with the id of every bone

        Returns:
            list[int, etc.]
        """
        return [each_bone.Index for each_bone in self.BoneList]

    def GetBoneByName(self, name):
        """
        Returns the BoneObject whose Name variable matches the provided name

        Arguments:
            name (string): name of the bone whose BoneObject you want to retrieve

        Return:
            Bone()
        """
        results = filter(lambda eachTrack: eachTrack.Name == name, self.BoneList)
        if results:
            return results[0]

    def GetBoneById(self, id):
        """
        returns the BoneObject() instance whose id variable matches the provided id

        Arguments:
            id (int): id of the bone whose BoneObject() instance you want to retrieve

        Return:
            Bone Instance()
        """
        results = filter(lambda eachTrack: eachTrack.Index == id, self.BoneList)
        if results:
            return results[0]

    def BoneExists(self, id_name):
        """
        Checks if this SkeletonObject() instance has a BoneObject() instance that matches the id or name provided

        Arguments:
            id_name (int): float or string; the name or id number of the bone who you want to check

        Return:
            Boolean
        """
        if isinstance(id_name, basestring):
            return self.GetBoneByName(id_name) is not None

        return self.GetBoneById(id_name) is not None


class BoneObject(object):
    """
    Stores information about a bone from the .skel file
    """
    def __init__(self):
        """
        Constructor
        """
        self.Name = ""
        self.Index = 0
        self.Parent = None
        self.Children = []


def extractVehicleSkel(vehicleName):
    """
    Extracts the vehicle skel file
    """
    skelFilePath = None
    
    project = Config.Project.Name.lower()

    assetsSplit = Config.Project.Path.Assets.split("\\")
    for split in assetsSplit:
        if "assets" in split:
            if Config.Project.Path.Assets.split(split)[1] is not None:
                assetsType = split + Config.Project.Path.Assets.split(split)[1]
            else:
                assetsType = split
    
    if project == "gta5":
        vehicleDirName = "vehicles_packed"
    else:
        vehicleDirName = "vehicles"
    
    # Prompt the user to select the origin max file so we can find the export zip
    app = mobu.FBApplication()
    vehMaxPopup = mobu.FBFilePopup()
    vehMaxPopup.Caption = "Select the Vehicle's Max File"
    #Set default path to look in
    vehMaxPopup.Path = os.path.join(Config.Project.Branch.Path.Art)
    vehMaxPopup.Filter = '*.max'
    vehMaxPopup.Style = mobu.FBFilePopupStyle.kFBFilePopupOpen
    
    # If we don't cancel the file read, we'll look for the locations
    if not vehMaxPopup.Execute():
        mobu.FBMessageBox("Warning:", "Selection canceled!", "OK")
        return skelFilePath
    
    filePath = vehMaxPopup.FullFilename
    projectDir = filePath.lower().split("\\art\\")[0]
    
    # Def
    zipFolderPath = os.path.join(projectDir, assetsType, "export", "levels", project, vehicleDirName)
    
    # Search for the vehicle's zip
    vehicleZip = None
    fileName = (vehicleName + ".veh.zip")
    fileRecord = Perforce.Run("files", args=[os.path.join(zipFolderPath, "...{}".format(fileName))])
    p4RecordSetAsList = servers.ConversionsMixin.convertP4RecordSetToList(fileRecord)
    if p4RecordSetAsList:
        # Get file path
        result = Perforce.Where([p4RecordSetAsList[0]["depotFile"]])
        p4RecordSetAsList = servers.ConversionsMixin.convertP4RecordSetToList(result)
        vehicleZip = p4RecordSetAsList[0]["path"]
        Perforce.Sync(vehicleZip, force=True)

    # Sync it
    if vehicleZip is not None and not os.path.exists(vehicleZip):
        mobu.FBMessageBox("Warning:", "Selection canceled!", "OK")
    
    # Definet the output path
    skelOutputFolder = os.path.join(Config.Project.Branch.Path.Art, "animation", "resources", "vehicles", "skel_files")
    skelOutputPath = os.path.join(skelOutputFolder, vehicleName)
    
    with ZipFile(vehicleZip, 'r') as zipObj:
        # Get a list of all archived file names from the zip
        listOfFileNames = zipObj.namelist()
        # Iterate over the file names
        for fileName in listOfFileNames:
            # Check filename endswith csv
            if fileName.endswith('.skel') and "0/" in fileName:
                # Extract a single file from zip
                skelFilePath = os.path.join(skelOutputPath, fileName)
                if Perforce.DoesFileExist(skelFilePath):
                    Perforce.Sync(skelFilePath)
                    Perforce.Edit(skelFilePath)
                else:
                    Perforce.Add(skelFilePath)
                zipObj.extract(fileName, skelOutputPath)
    
    return skelFilePath


def setBoneLimits(skeletonFile):
    """
    Sets the limits on vehicle bones to match the limits in its skel file
    """
    
    if not os.path.exists(skeletonFile):
        mobu.FBMessageBox("Warning:", "Couldn't find the skeleton file!", "OK")
        return
    
    skeletonData = SkeletonObject(skeletonFile)
    bones = skeletonData.BoneIds
    
    for bone in bones:
        boneName = skeletonData.GetBoneById(bone).Name
        attributes = skeletonData.GetBoneById(bone).attribute
        skelOrient = skeletonData.GetBoneById(bone).orientation
        skelOffset = skeletonData.GetBoneById(bone).offset
        sceneBone = mobu.FBFindModelByLabelName(boneName)
        
        # Skip if the bone is not found
        if not sceneBone:
            continue
        
        # Get orientation from the skel file
        orientation = mobu.FBVector3d(0,0,0)
        if skelOrient:
            xDegrees = math.degrees(float(skelOrient[0]))
            yDegrees = math.degrees(float(skelOrient[1]))
            zDegrees = math.degrees(float(skelOrient[2]))
        
        orientation = mobu.FBVector3d(xDegrees, yDegrees, zDegrees)
        
        offset = mobu.FBVector3d(0,0,0)
        if skelOffset:
            xTrans = float(skelOffset[0])
            yTrans = float(skelOffset[1])
            zTrans = float(skelOffset[2])
        
        offset = mobu.FBVector3d(xTrans, yTrans, zTrans)
        
        # Set the bone orientation and translation to match the skel file
        sceneBone.Rotation.Data = orientation
        sceneBone.Translation.Data = offset
        mobu.FBSystem().Scene.Evaluate()
        
        # Lock scale
        sceneBone.Scaling.SetLocked(True)
        
        # Create a new list for our objects to be restricted, hold the bone
        objectsToRestrict = [sceneBone]
        
        # Search for the bone controller
        boneController = mobu.FBFindModelByLabelName(boneName + "_handle")
        
        if boneController:
            # Add the controller to our list
            objectsToRestrict.append(boneController)
            boneController.Rotation.Data = orientation
            boneController.Translation.Data = offset
            mobu.FBSystem().Scene.Evaluate()
            # Lock scale
            boneController.Scaling.SetLocked(True)
        
        # Unlock rotation if the properties are found for it
        if  "rotX" in attributes or "rotY" in attributes or "rotZ" in attributes:
            sceneBone.Rotation.SetLocked(False)
            if boneController:
                boneController.Rotation.SetLocked(False)
        
        # Unlock translation if the properties are found for it
        if "transX" in attributes or "transY" in attributes or "transZ" in attributes:
            sceneBone.Translation.SetLocked(False)
            if boneController:
                boneController.Translation.SetLocked(False)
        
        # Store new variables for the limits
        minXRot = 0.0
        minYRot = 0.0
        minZRot = 0.0
        maxXRot = 0.0
        maxYRot = 0.0
        maxZRot = 0.0
        minXTrans = 0.0
        minYTrans = 0.0
        minZTrans = 0.0
        maxXTrans = 0.0
        maxYTrans = 0.0
        maxZTrans = 0.0
        
        rotationLimit = False
        translationLimit = False
        
        # Iterate through out attribute list to find the limits
        for i in range((len(attributes))-3):
            if attributes[i] == "transX" and attributes[i+1] == "limit":
                minXTrans = float(attributes[i+2])
                maxXTrans = float(attributes[i+3])
                translationLimit = True
            if attributes[i] == "transY" and attributes[i+1] == "limit":
                minYTrans = float(attributes[i+2])
                maxYTrans = float(attributes[i+3])
                translationLimit = True
            if attributes[i] == "transZ" and attributes[i+1] == "limit":
                minZTrans = float(attributes[i+2])
                maxZTrans = float(attributes[i+3])
                translationLimit = True
            if attributes[i] == "rotX" and attributes[i+1] == "limit":
                minXRot =  math.degrees(float(attributes[i+2]))
                maxXRot =  math.degrees(float(attributes[i+3]))
                rotationLimit = True
            if attributes[i] == "rotY" and attributes[i+1] == "limit":
                minYRot =  math.degrees(float(attributes[i+2]))
                maxYRot =  math.degrees(float(attributes[i+3]))
                rotationLimit = True
            if attributes[i] == "rotZ" and attributes[i+1] == "limit":
                minZRot =  math.degrees(float(attributes[i+2]))
                maxZRot =  math.degrees(float(attributes[i+3]))
                rotationLimit = True
        
        # Store our limits as vectors
        minRotLimits = mobu.FBVector3d(minXRot, minYRot, minZRot)
        maxRotLimits = mobu.FBVector3d(maxXRot, maxYRot, maxZRot)
        minTransLimits = mobu.FBVector3d(minXTrans, minYTrans, minZTrans)
        maxTransLimits = mobu.FBVector3d(maxXTrans, maxYTrans, maxZTrans)
        
        # Apply limits to the initial orientation/offset
        minRotVector = orientation + minRotLimits
        maxRotVector = orientation + maxRotLimits
        minTransBone = offset + minTransLimits
        maxTransBone = offset + maxTransLimits
        minTransControl = offset + (minTransLimits * 100)
        maxTransControl = offset + (maxTransLimits * 100)
        
        if rotationLimit == True:
            # Apply rotation limits
            for object in objectsToRestrict:
                rotDof = object.PropertyList.Find("Enable Rotation DOF")
                rotDof.Data = True
                object.PropertyList.Find("RotationMinX").Data = True
                object.PropertyList.Find("RotationMaxX").Data = True
                object.PropertyList.Find("RotationMinY").Data = True
                object.PropertyList.Find("RotationMaxY").Data = True
                object.PropertyList.Find("RotationMinZ").Data = True
                object.PropertyList.Find("RotationMaxZ").Data = True
                object.PropertyList.Find("RotationMin").Data = mobu.FBVector3d(minRotVector)
                object.PropertyList.Find("RotationMax").Data = mobu.FBVector3d(maxRotVector)
                
                # Lock the ones that have limits of 0
                if minXRot == 0.0 and maxXRot == 0.0:
                    object.Rotation.SetMemberLocked(0, True)
                if minYRot == 0.0 and maxYRot == 0.0:
                    object.Rotation.SetMemberLocked(1, True)
                if minZRot == 0.0 and maxZRot == 0.0:
                    object.Rotation.SetMemberLocked(2, True)
            
            # Adjust rotation order
            if minXRot != 0.0 and orientation[0] == 0.0 or maxXRot != 0.0 and orientation[0] == 0.0:
                sceneBone.PropertyList.Find( 'RotationOrder' ).Data = 1 
            elif minYRot != 0.0 and orientation[1] == 0.0 or maxYRot != 0.0 and orientation[1] == 0.0:
                sceneBone.PropertyList.Find( 'RotationOrder' ).Data = 1
            elif minZRot != 0.0 and orientation[2] == 0.0 or maxZRot != 0.0 and orientation[2] == 0.0:
                sceneBone.PropertyList.Find( 'RotationOrder' ).Data = 4
        
        if translationLimit == True:
            # Apply translation limits
            for object in objectsToRestrict:
                # Need to multiple controller vectors by 100 to allow for scaling differences
                transDof = object.PropertyList.Find("Enable Translation DOF")
                transDof.Data = True
                
                object.PropertyList.Find("TranslationMinX").Data = True
                object.PropertyList.Find("TranslationMaxX").Data = True
                object.PropertyList.Find("TranslationMinY").Data = True
                object.PropertyList.Find("TranslationMaxY").Data = True
                object.PropertyList.Find("TranslationMinZ").Data = True
                object.PropertyList.Find("TranslationMaxZ").Data = True
                
                if not object == boneController:
                    object.PropertyList.Find("TranslationMin").Data = mobu.FBVector3d(minTransBone)
                    object.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(maxTransBone)
                else:
                    object.PropertyList.Find("TranslationMin").Data = mobu.FBVector3d(minTransControl)
                    object.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(maxTransControl)
                
                # Unlock the ones that have limits of 0
                if minXTrans == 0.0 and maxXTrans == 0.0:
                    object.Translation.SetMemberLocked(0, True)
                if minYTrans == 0.0 and maxYTrans == 0.0:
                    object.Translation.SetMemberLocked(1, True)
                if minZTrans == 0.0 and maxZTrans == 0.0:
                    object.Translation.SetMemberLocked(2, True)
    
    # Unshow the chassis bone, making it easier to work with vehicles
    chassisBone = mobu.FBFindModelByLabelName("chassis")
    if chassisBone:
        chassisBone.Show = False
    
    mobu.FBMessageBox("DOF Setup", "DOF Setup complete, check over the vehicle", "OK")


def SetupVehicleDofs(control, event):
    # Get the file name, this should always match the vehicle name
    currentFile = Path.GetBaseNameNoExtension(Globals.Application.FBXFileName)
    
    skelFile = extractVehicleSkel(currentFile)
    
    if skelFile == None:
       return "Selection Canceled"
   
    setBoneLimits(skelFile)