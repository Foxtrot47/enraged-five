import sys
import unittest

import pyfbsdk as mobu

from RS import Globals, Perforce
from RS.Unittests import utils
from RS.Core.AssetSetup import Props, Vehicles
from RS.Core.AssetSetup.UnitTest import base, const
from RS.Utils import ContextManagers


def Run(className=None):
    """Run unittests from this module

    Args:
        className (str, optional): Name of class
    """
    unittest.TextTestRunner(verbosity=0).run(suite(className))


def suite(className=None):
    """A suite to hold all the tests from this module

    Arguments:
        className (str, optional): the name of the class whose test should be ran. Runs all the tests if None.
    Return:
        unittest.TestSuite
    """
    module = sys.modules[__name__]
    return utils.getSuite(module, "test_Data", className)


class Test_BlueJay(base.Base):
    """Blue Jay rig was crashing due to setting the link properties while the characterization was still on
    url:bugstar:3944690
    """

    def test_Data(self):
        """Testing blue jay asset"""
        sourcePath = self.testData.get("blueJayPath", None)
        if not sourcePath:
            return

        self.assertP4Sync(sourcePath)

        # open and save data
        Globals.Application.FileOpen(sourcePath)
        sourceDir = self.testData.get("blueJayData", None)
        self.assertStore(sourceDir)

        # sync restore path
        restorePath = const.TestPathDict.ASSET_PATHS.get("blueJay", None)
        self.assertP4Sync(restorePath)

        # open and restore Data
        Globals.Application.FileOpen(restorePath)
        self.assertRestore(sourceDir, marginOfError=0.1)


class Test_CharacterCutscene(base.Base):
    """Create a cutscene character"""

    def test_Data(self):
        """Testing creation of cutscene"""
        Globals.Application.FileNew()
        definitionName = self.testData.get("charDefinitionName", None)
        outfitName = self.testData.get("charOutfitName", None)
        self.assertSetupAsset(buildIG=False, definitionName=definitionName, outfit=outfitName)
        self.assertCutsceneCharCreate()


class Test_CharacterIngame(base.Base):
    """Create a ingame character"""

    def test_Data(self):
        """Testing creation of ingame"""
        Globals.Application.FileNew()
        definitionName = self.testData.get("charDefinitionName", None)
        outfitName = self.testData.get("charOutfitName", None)
        self.assertSetupAsset(buildIG=True, definitionName=definitionName, outfit=outfitName)
        self.assertIngameCharCreate()


class Test_Chicken(base.Base):
    """Chicken was coming in borked due to not setting all the properties of the FBCharacter before creating the rig"""

    def test_Data(self):
        """Testing chicken asset"""
        sourcePath = self.testData.get("chickenPath", None)
        if not sourcePath:
            return

        self.assertP4Sync(sourcePath)

        # open and save data
        Globals.Application.FileOpen(sourcePath)
        sourceDir = self.testData.get("chickenData", None)
        self.assertStore(sourceDir)

        # sync restore path
        restorePath = const.TestPathDict.ASSET_PATHS.get("chicken", None)
        self.assertP4Sync(restorePath)

        # open and restore Data
        Globals.Application.FileOpen(restorePath)
        self.assertRestore(sourceDir, marginOfError=0.1)


class Test_Heron(base.Base):
    """The heron mover was setup incorrectly in the source file
    The heron torso was flipping 180 degrees when rebuilt
    """

    def test_Data(self):
        """Testing heron asset"""
        sourcePath = self.testData.get("heronPath", None)
        if not sourcePath:
            return

        self.assertP4Sync(sourcePath)

        # open and save data
        Globals.Application.FileOpen(sourcePath)
        sourceDir = self.testData.get("heronData", None)
        self.assertStore(sourceDir)

        # sync restore path
        restorePath = const.TestPathDict.ASSET_PATHS.get("heron", None)
        self.assertP4Sync(restorePath)

        # open and restore Data
        Globals.Application.FileOpen(restorePath)
        self.assertRestore(sourceDir, marginOfError=0.1)


class Test_Prop(base.Base):
    """Create a prop"""

    def test_Data(self):
        """Testing creation of prop"""
        sourcePath = self.testData.get("propPath", None)
        if not sourcePath:
            return

        self.assertP4Sync(sourcePath)

        # open file and setup prop
        Globals.Application.FileOpen(sourcePath)
        with ContextManagers.SuspendMessages():
            Props.AssetSetupProps(messagePopup=False)

        sourceName = self.testData.get("propControl", "")
        self.assertIsNotNone(mobu.FBFindModelByLabelName(sourceName))


class Test_Shark(base.Base):
    """SharkHammerhead rig head was flipping up when recreated
    url:bugstar:3975955
    """

    def test_Data(self):
        """Testing shark asset"""
        sourcePath = self.testData.get("sharkPath", None)
        if not sourcePath:
            return

        self.assertP4Sync(sourcePath)

        # open and save data
        Globals.Application.FileOpen(sourcePath)
        sourceDir = self.testData.get("sharkData", None)
        self.assertStore(sourceDir)

        # sync restore path
        restorePath = const.TestPathDict.ASSET_PATHS.get("shark", None)
        self.assertP4Sync(restorePath)

        # open and restore Data
        Globals.Application.FileOpen(restorePath)
        self.assertRestore(sourceDir, marginOfError=0.15)


class Test_Turkey(base.Base):
    """Turkey was coming in borked due to not setting all the properties of the FBCharacter before creating the rig"""

    def test_Data(self):
        """Testing turkey asset"""
        sourcePath = self.testData.get("turkeyPath", None)
        if not sourcePath:
            return

        self.assertP4Sync(sourcePath)

        # open and save data
        Globals.Application.FileOpen(sourcePath)
        sourceDir = self.testData.get("turkeyData", None)
        self.assertStore(sourceDir)

        # sync restore path
        restorePath = const.TestPathDict.ASSET_PATHS.get("turkey", None)
        self.assertP4Sync(restorePath)

        # open and restore Data
        Globals.Application.FileOpen(restorePath)
        self.assertRestore(sourceDir, marginOfError=0.1)


class Test_Vehicle(base.Base):
    """Create a vehicle"""

    def test_Data(self):
        """Testing creation of vehicle"""
        sourcePath = self.testData.get("vehiclePath", None)
        if not sourcePath:
            return

        self.assertP4Sync(sourcePath)

        # open file and setup prop
        Globals.Application.FileOpen(sourcePath)
        with ContextManagers.SuspendMessages():
            Vehicles.rs_AssetSetupVehicles()

        sourceName = self.testData.get("vehicleControl", "")
        self.assertIsNotNone(mobu.FBFindModelByLabelName(sourceName))


class Test_Weapon(base.Base):
    """Create a weapon"""

    def test_Data(self):
        """Testing creation of weapon"""
        sourcePath = self.testData.get("weaponPath", None)
        if not sourcePath:
            return

        self.assertP4Sync(sourcePath)

        # open file and setup prop
        Globals.Application.FileOpen(sourcePath)
        with ContextManagers.SuspendMessages():
            Props.AssetSetupProps(messagePopup=False)

        sourceName = self.testData.get("weaponControl", "")
        self.assertIsNotNone(mobu.FBFindModelByLabelName(sourceName))
