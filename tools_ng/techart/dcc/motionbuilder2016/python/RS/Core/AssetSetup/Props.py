###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_AssetSetupProps
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Function to set up props - ingame, cutscene, skinned, weapons
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import re
import os
from RS import Config, Perforce
from RS.Utils import Path, Scene
from RS.Utils.Scene import RelationshipConstraintManager as rcm
import RS.Core.AssetSetup.Lib as ass
import RS.Globals as glo



#####################################################################################
# Reset Pivot Offset - MB14 bug workaround (logged with autodesk) url:bugstar:1713249
#####################################################################################

def ResetPivotOffset( parent ):
    
    markerList = []

    Scene.GetChildren( parent, markerList, "", True )

    for marker in markerList:
	pivotRotProp = marker.PropertyList.Find( 'RotationPivot' )
	pivotScaleProp = marker.PropertyList.Find( 'ScalingPivot' )
	offsetRotProp = marker.PropertyList.Find( 'RotationOffset' )
	offsetScaleProp = marker.PropertyList.Find( 'ScalingOffset' )

	if pivotRotProp:
	    pivotRotProp.Data = FBVector3d( 0,0,0 )

	if pivotScaleProp:
	    pivotScaleProp.Data = FBVector3d( 0,0,0 )        

	if offsetRotProp:
	    offsetRotProp.Data = FBVector3d( 0,0,0 )

	if offsetScaleProp:
	    offsetScaleProp.Data = FBVector3d( 0,0,0 )

	FBSystem().Scene.Evaluate()    


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Setup mover and toybox relation - for use in rs_CSCharacterSetup & rs_IGCharacterSetup
##              This was only added in Jan/13 so needs to be adjusted if it was used at the beginning of the production 
##              Currently only BoxStar Tool calls this function, not anywhere else in this module - KM
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupPropToyboxMoverControl(lNameSpace, dummy, mover, moverControl):

    # Check for ToyBox Marker, however most files should have this at this point.
    lFound = False
    for toyBoxMarker in dummy.Children:
	if toyBoxMarker.Name == "ToyBox Marker":
	    lFound = True

    if lFound == False:

	toyBoxMarker = FBCreateObject("Browsing/Templates/Elements", "Marker", "ToyBox Marker")
	toyBoxMarker.Look = FBMarkerLook.kFBMarkerLookHardCross  
	toyBoxMarker.Size = 500
	toyBoxMarker.Color = FBColor(1, 0, 1)
	toyBoxMarker.Parent = dummy
	toyBoxMarker.Translation.Data = mover.Translation.Data
	toyBoxMarker.Rotation.Data = mover.Rotation.Data
	tag = toyBoxMarker.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
	tag.Data = "rs_Contacts"

    # Creating the mover_toybox_Control Relation Constraint
    relationCon = FBConstraintRelation(lNameSpace+':mover_toybox_Control') # Why is this call slow..
    relationCon.Active = False 

    # Add senders/receivers to the Relation Constraint
    moverMarkerSender = relationCon.SetAsSource(moverControl)
    toyboxMarkerReceive = relationCon.ConstrainObject(toyBoxMarker)
    toyBoxMarkerSender = relationCon.SetAsSource(toyBoxMarker)
    moverReceive = relationCon.ConstrainObject(mover)

    # Position the boxes and set the properties    
    relationCon.SetBoxPosition(moverMarkerSender, 0, 0)
    relationCon.SetBoxPosition(toyboxMarkerReceive, 300, 0)    
    relationCon.SetBoxPosition(toyBoxMarkerSender, 0, 150)    
    relationCon.SetBoxPosition(moverReceive, 350, 150)
    toyBoxMarkerSender.UseGlobalTransforms = False
    moverReceive.UseGlobalTransforms = False

    # Make the property connections
    Scene.ConnectBox(moverMarkerSender, 'Translation', toyboxMarkerReceive, 'Translation')
    Scene.ConnectBox(moverMarkerSender, 'Rotation', toyboxMarkerReceive, 'Rotation')   
    Scene.ConnectBox(toyBoxMarkerSender, 'Lcl Translation', moverReceive, 'Lcl Translation')
    Scene.ConnectBox(toyBoxMarkerSender, 'Lcl Rotation', moverReceive, 'Lcl Rotation')

    return relationCon


def setupExportScale():
    '''
    Some props allow for scaling e.g. liquid in a glass. We need to create constraints to allow the
    user to manipulate the scale of certain objects.
    Only objects with the property exportScale will be set up for scaling.
    url:bugstar:2593730
    '''
    # get a list of models with scale properties
    scaleList = []
    modelList = [component for component in glo.Components if isinstance(component, FBModel)]

    for model in modelList:
	maxProperty = model.PropertyList.Find('UDP3DSMAX')
	if maxProperty:
	    dataList = maxProperty.Data.replace("\r","\n").split("\n")
	    lowerList = []
	    for data in dataList:
		removedSpace = data.replace(" ","").lower()
		lowerList.append(removedSpace)
	    if "exportscale=true" in lowerList:
                scaleList.append(model)

    if len(scaleList) == 0:
        return

    # create constraint
    scaleConstraint = rcm.RelationShipConstraintManager(name="Scale_Relation_Constraint")

    y = 0
    for model in scaleList:
        controlModel = Scene.FindModelByName(model.Name + "_Control")
        if controlModel is None:
            continue
        # create boxes
        senderBox = scaleConstraint.AddSenderBox(controlModel)
        scaleBox = scaleConstraint.AddFunctionBox('Vector', 'Scale (a x V)')
        receiverBox = scaleConstraint.AddReceiverBox(model)

        # add box properties
        scaleBox.SetReceiverValueByName("Number", 100)
        senderBox.SetBoxPosition(0, y)
        scaleBox.SetBoxPosition(500, y)
        receiverBox.SetBoxPosition(900, y)

        # connect boxes
        scaleConstraint.ConnectBoxes(senderBox, 'Rotation', receiverBox, 'Rotation')
        scaleConstraint.ConnectBoxes(senderBox, 'Translation', receiverBox, 'Translation')
        scaleConstraint.ConnectBoxes(senderBox, 'Scaling', scaleBox, 'Vector')
        scaleConstraint.ConnectBoxes(scaleBox, 'Result', receiverBox, 'Scaling')

        y += 200

    scaleConstraint.SetActive(True)

    return scaleConstraint

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Marker Setup 
##                         - Setup markers for each null (Not including Dummy)
##                         - Create Marker, align to null, create constraint
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupCreateMarkers():

    sceneList = FBModelList()
    FBGetSelectedModels (sceneList, None, False)
    FBGetSelectedModels (sceneList, None, True)

    nullList = []
    markerList = []
    spawnPointList = []
    vfxList = []

    dummy = ass.rs_AssetSetupVariables()[0]
    mover = ass.rs_AssetSetupVariables()[1]
    geo = ass.rs_AssetSetupVariables()[2]
    exportable = mover.Children[0]

    nullRelConst = FBConstraintRelation("Bone_Relation_Constraint")
    nullRelConst.Active = True

    Scene.GetChildren(mover, nullList, "", False)

    x = 0
    y = 0
    for null in nullList:

	marker = FBModelMarker(null.Name + "_Control")
	markerList.append(marker)
	
	if "_vfx" in null.Name.lower():
	    vfxList.append(marker)

	marker.Show = True
	marker.Look = FBMarkerLook.kFBMarkerLookSphere
	
	marker.PropertyList.Find("Size").Data = 100

	lVector = FBVector3d()
	null.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
	marker.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
	null.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
	marker.SetVector(lVector, FBModelTransformationType.kModelRotation, True)
	FBSystem().Scene.Evaluate()

	sender = nullRelConst.SetAsSource(marker)
	receiver = nullRelConst.ConstrainObject(null)

	if nullRelConst.SetBoxPosition(sender, 0, x):
	    x = x + 150
	else:
	    nullRelConst.SetBoxPosition(sender, 0, x)

	if nullRelConst.SetBoxPosition(receiver, 400, y):
	    y = y + 150
	else:
	    propConst.SetBoxPosition(receiver, 400, y)                    

	Scene.ConnectBox(sender, 'Rotation', receiver, 'Rotation')
	Scene.ConnectBox(sender, 'Translation', receiver, 'Translation')

	FBSystem().Scene.Evaluate()

    moverRelConst = FBConstraintRelation("mover_Relation_Constraint")
    #FBSystem().Scene.Constraints.append(moverRelConst)
    moverRelConst.Active = True

    moverMarker = FBModelMarker(mover.Name + "_Control")

    moverMarker.Show = True
    moverMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
    moverMarker.PropertyList.Find("Size").Data = 140

    moverVector = FBVector3d()
    mover.GetVector(moverVector, FBModelTransformationType.kModelTranslation, True)
    moverMarker.SetVector(moverVector, FBModelTransformationType.kModelTranslation, True)
    mover.GetVector(moverVector, FBModelTransformationType.kModelRotation, True)
    moverMarker.SetVector(moverVector, FBModelTransformationType.kModelRotation, True)
    FBSystem().Scene.Evaluate()

    moverSender = moverRelConst.SetAsSource(moverMarker)
    moverReceiver = moverRelConst.ConstrainObject(mover)

    moverRelConst.SetBoxPosition(moverSender, 0, 0)
    moverRelConst.SetBoxPosition(moverReceiver, 400, 0)                    

    Scene.ConnectBox(moverSender, 'Rotation', moverReceiver, 'Rotation')
    Scene.ConnectBox(moverSender, 'Translation', moverReceiver, 'Translation')   

    multiDimensionalArray = []

    for null in nullList:
	ChildrenArray = []
	ChildrenArray.append(FBFindModelByLabelName(null.Name + "_Control"))
	for child in null.Children:
	    ChildrenArray.append(FBFindModelByLabelName(child.Name + "_Control"))
	multiDimensionalArray.append(ChildrenArray)

    for i in range(len(multiDimensionalArray)):
	if len(multiDimensionalArray[i]) > 0:
	    for j in range(len(multiDimensionalArray[i]) - 1):
		multiDimensionalArray[i][j + 1].Parent = multiDimensionalArray[i][0]

    exportableMarker = FBFindModelByLabelName(exportable.Name + "_Control")

    moverMarker.Parent = exportableMarker
    exportableMarker.Parent = dummy

    moverMarker.PropertyList.Find("Enable Transformation").Data = False
    moverMarker.PropertyList.Find("Enable Selection").Data = False

    for sceneItem in sceneList:
	if "spawnpoint" in sceneItem.Name.lower():
	    spawnPointList.append(sceneItem)

    for spawnPoint in spawnPointList:
    
	spawnControl = FBModelMarker(spawnPoint.Name + "_visual")
	spawnControl.Show = True
	spawnControl.Look = FBMarkerLook.kFBMarkerLookSphere
	spawnControl.PropertyList.Find("Size").Data = 150
	spawnControl.PropertyList.Find("Color RGB").Data = FBColor(1,1,0)
	spawnControl.PropertyList.Find("Enable Transformation").Data = False            
    
	vector = FBVector3d()
	spawnPoint.GetVector(vector, FBModelTransformationType.kModelTranslation, True)
	spawnControl.SetVector(vector, FBModelTransformationType.kModelTranslation, True)
	spawnPoint.GetVector(vector, FBModelTransformationType.kModelRotation, True)
	spawnControl.SetVector(vector, FBModelTransformationType.kModelRotation, True)
    
	FBSystem().Scene.Evaluate()
    
	spawnControl.Parent = geo
    
    # Remove controllers for VFX emitters
    for vfxControl in vfxList:
        if vfxControl:
            try:
                vfxControl.FBDelete()
            except:
                None
	

def getHierarchyMaxDepth(rootNode, startDepth=0):
    maxDepth = startDepth
    for child in rootNode.Children:
	if isinstance(child,FBModelMarker):
	    branchDepth = getHierarchyMaxDepth(child, startDepth + 1)
	    maxDepth = max(maxDepth, branchDepth)
    return maxDepth

def setMarkerGradient(rootNode, maxDepth, maxSat, minSat, branchDepth=0):
    # Set the marker gradient by finding the maximum depth of the hierarchy chain, setting the top to max, bototm to min, and everything else inbetween    
    if "main_" in rootNode.Name.lower():
	branchDepth -= 1
	maxDepth -= 1
    branchChildren = []
    for child in rootNode.Children:
	if isinstance(child,FBModelMarker):
	    branchChildren.append(child)
    if len(branchChildren) > 0:
	branchDepth += 1
	for childObject in branchChildren:
	    if maxDepth == 0:
		saturation = 1
	    else:
		saturation = (((maxSat-minSat)/(maxDepth)) * (maxDepth-branchDepth)) + minSat
	    # Set the red saturation value
	    # Pink Option
	    childObject.PropertyList.Find("Color RGB").Data = FBColor(1,(1-saturation),(1-saturation))
	    # Grey-red Option
	    #childObject.PropertyList.Find("Color RGB").Data = FBColor(saturation,0,0)
	    setMarkerGradient(childObject, maxDepth, maxSat, minSat, branchDepth)

def rs_AssetSetupMarkerColors(dummy):
    rootControl = None
    
    for child in dummy.Children:
	if "_control" in child.Name.lower():
	    if isinstance(child, FBModelMarker):
		rootControl = child
    
    # Define the max and min saturation of markers
    maxSaturation = 1.0
    minSaturation = 0.25
    
    for child in rootControl.Children:
	maxDepth = getHierarchyMaxDepth(child)
	setMarkerGradient(child, maxDepth, maxSaturation, minSaturation)

    # Make root marker blue and scale it up
    rootControl.PropertyList.Find("Color RGB").Data = FBColor(0,0,1)
    rootControl.PropertyList.Find("Size").Data = 110
    #rootControl.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    
    # Mover marker should be green
    moverMarker = FBFindModelByLabelName("mover_Control")
    if moverMarker:
	moverMarker.PropertyList.Find("Color RGB").Data = FBColor(0,1,0)
    


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Lock Markers
##                         - Sets trans, rot, scale to locked depending on properties/names of markers
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupLockMarkers(dummy):
    
    objectList = []
    boneList = []
    controllerList = []
    
    Scene.GetChildren(dummy, objectList, "", False)
    
    [boneList.append(object) for object in objectList if not object.Name.lower().endswith("_control")]    
    [controllerList.append(object) for object in objectList if object.Name.lower().endswith("_control")]
    
    # Find the controller based on the bone list, so we can iterate with the bone's custom properties
    for bone in boneList:
	controllerName = bone.Name.lower() + "_control"
	controller = None
	for ctrl in controllerList:
	    if ctrl.Name.lower() == controllerName:
		controller = ctrl
	if not controller:
	    continue
	
	# If the dummy doesn't have "exportScale = true" in the UDP3DSMAX property, lock scaling
	if dummy in controller.Parents:
	    maxProperty = dummy.PropertyList.Find('UDP3DSMAX')
	    if maxProperty:
		dataList = maxProperty.Data.replace("\r","\n").split("\n")
		lowerList = []
		for data in dataList:
		    removedSpace = data.replace(" ","").lower()
		    lowerList.append(removedSpace)
		if not "exportscale=true" in lowerList:
		    controller.Scaling.SetLocked(True)
	# Locking scaling on the mover control
	elif "mover_control" in controllerName:
	    controller.Scaling.SetLocked(True)
	# Lock and hide the "main" controller completely, we don't want people moving this
	elif "main_" in controllerName:
	    controller.Translation.SetLocked(True)
	    controller.Rotation.SetLocked(True)
	    controller.Scaling.SetLocked(True)
	    controller.Show = False
	    controller.PropertyList.Find("Color RGB").Data = FBColor(0.5,0.5,0.5)
	# Lock spawnpoint controls
	elif "spawnpoint" in controllerName:
	    controller.Translation.SetLocked(True)
	    controller.Rotation.SetLocked(True)
	    controller.Scaling.SetLocked(True)
	# For everything else, if the bone's max property doesn't have exportTrans or exportScale, lock those properties
	else:
	    maxProperty = bone.PropertyList.Find('UDP3DSMAX')
	    if maxProperty:
		dataList = maxProperty.Data.replace("\r","\n").split("\n")
		lowerList = []
		for data in dataList:
		    removedSpace = data.replace(" ","").lower()
		    lowerList.append(removedSpace)
		if not "exporttrans=true" in lowerList:
		    controller.Translation.SetLocked(True)
		if not "exportscale=true" in lowerList:
		    controller.Scaling.SetLocked(True)
	    else:
		controller.Translation.SetLocked(True)
		controller.Scaling.SetLocked(True)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Tidy Scene
##                         - Remove Unwanted Nulls, Create Folders, Create groups
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupTidyScene():

    controlNullList = []
    exportNullList = []
    deleteList = []

    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    mover = ass.rs_AssetSetupVariables()[1]
    geo = ass.rs_AssetSetupVariables()[2]
    exportable = mover.Children[0]

    ass.rs_AssetSetupDeleteItems()

    controlGroup = FBGroup("Controls")
    plotGroup = FBGroup("Plot Group")
    geoGroup = FBGroup("Geo")

    mainGroup = FBGroup(fileName)
    mainGroup.ConnectSrc(controlGroup)
    mainGroup.ConnectSrc(plotGroup)
    mainGroup.ConnectSrc(geoGroup)

    controlRoot = FBFindModelByLabelName(exportable.Name + "_Control")

    Scene.GetChildren(controlRoot, controlNullList)
    #controlNullList.extend(eva.gChildList)
    #del eva.gChildList[:]

    Scene.GetChildren(mover, exportNullList)   
    #exportNullList.extend(eva.gChildList)
    #del eva.gChildList[:]

    for child in controlNullList:
	controlGroup.ConnectSrc(child)

    for child in exportNullList:
	plotGroup.ConnectSrc(child)

    geoGroup.ConnectSrc(geo)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: Render Target Check - adds a custom property onto the mesh if a render target exists on it
##				url:bugstar:1299744
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def RenderTargetCheck():
    mesh = None
    for component in glo.Components:
	rsType = component.PropertyList.Find( "rs_Type" )
	if rsType:
	    if rsType.Data == 'rs_Mesh':
		mesh = component

    materialList = []
    if mesh:
	if mesh.GetSrcCount() != 0:
	    for i in range( mesh.GetSrcCount() ):
		sourceObject = mesh.GetSrc( i )
		if sourceObject.ClassName() == 'FBMaterial':
		    materialList.append( sourceObject )

	renderTargetList = []
	if materialList:
	    for material in materialList:
		texture = material.GetTexture()
		if texture:
		    video = texture.Video
		    if video:
			if 'script_rt' in video.Filename:

			    split = video.Filename.split( '\\' )
			    rtNameExtensionSplit = split[-1].split( '.' )
			    rtName = rtNameExtensionSplit[0]

			    rtPathProperty = mesh.PropertyCreate( 'RenderTargetPath', FBPropertyType.kFBPT_charptr, "", False, True, None )
			    rtPathProperty.Data = video.Filename
			    rtPathProperty.SetLocked( True )

			    rtNameProperty = mesh.PropertyCreate( 'RenderTargetName', FBPropertyType.kFBPT_charptr, "", False, True, None )
			    rtNameProperty.Data = rtName
			    rtNameProperty.SetLocked( True )


def rs_PropPerforceCheck(filePath):
    # Detect whether the file exists in Perforce. If it does, edit it, if it doesn't add it
    perforceRun = False
    
    # Ensure the file is on the X drive
    directories = filePath.split("\\")
    if directories[0].lower() == "x:":
	Perforce.Sync(filePath, force=True)
	try:
	    Perforce.AddOrEditFiles(filePath, force=True, fileType = "binary+lm")
	except Perforce.P4Exception:
	    Perforce.EnsureFileWritable(filePath)
	
	perforceRun = True
    return perforceRun


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Prop Setup 
##                         - Prop Setup Functions:
##                           Delete animation, Delete unwanted nulls, Get Exportable Null Variable, 
##                           Hide mover, Parent geo to dummy, Brighten Materials
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupProps(pControl, pEvent):
    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    fileNamePrefix = fileName[:2].lower()
    
    #Detect whether we're looking at a weapon (no prefix) or another prop
    weaponType = False
    
    gripR = FBFindModelByLabelName("Gun_GripR")
    if gripR:
	weaponType = True
    elif fileName[-5:-1] == "_mag" and fileNamePrefix == "w_":
	weaponType = True
    
    # Added catch for weapon shells, which we've never been able to set up due to missing hierarchy objects url:bugstar:6695852
    if fileName[-6:].lower() == "_shell" and fileNamePrefix == "w_":
	weaponType = True
	
	mover = None
	dummy = None
	dummyChildren = []
	
	# Create a Mover object if none exist
	for comp in glo.Components:
	    if "mover" in comp.Name.lower():
		mover = comp
	    elif "dummy" in comp.Name.lower():
		dummy = comp
	if mover == None:
	    mover = FBModelNull("Mover")
	    if dummy:
		mover.Translation = FBVector3d(dummy.Translation)
		FBSystem().Scene.Evaluate()
		# Reparent the dummy children beneath the new mover object
		for child in dummy.Children:
		    dummyChildren.append(child)
		mover.Parent = dummy
		for child in dummyChildren:
		    child.Parent = mover
    
    # Detect the pack prefix & add to the file name on non-weapon prop FBXs
    prefix = None
    if weaponType != True:
	prefix = ass.rs_DetectDLCPrefix()
	if prefix:
	    if not fileName.lower().startswith(prefix):
		fileName = prefix + fileName
    
    # Perform Perforce operations before running further setup
    filePath = os.path.dirname(FBApplication().FBXFileName) + '\\' + fileName + '.fbx'
    
    perforceRun = rs_PropPerforceCheck(filePath)
    
    saveOptions = FBFbxOptions(False)
    saveOptions.UseASCIIFormat = False
    
    FBApplication().FileSave(filePath, saveOptions)
    
    FBSystem().CurrentTake.ClearAllProperties(False)
    
    if len(ass.rs_AssetSetupVariables()) == 3:
	ass.rs_AssetSetupDeleteItems()

	FBSystem().Scene.Evaluate()

	dummy = ass.rs_AssetSetupVariables()[0]
	mover = ass.rs_AssetSetupVariables()[1]
	geo = ass.rs_AssetSetupVariables()[2]
	
	gripR = FBFindModelByLabelName("Gun_GripR")
	gripL = FBFindModelByLabelName("Gun_GripL")
	exportable = mover.Children[0]

	dummy.Translation = FBVector3d(0,0,0)

	FBSystem().Scene.Evaluate()

	mover.Selected = True
	mover.ShadingMode = FBModelShadingMode.kFBModelShadingWire
	mover.Selected = False
	mover.PropertyList.Find("Visibility").Data = True

	FBSystem().Scene.Evaluate()

	if geo.Parent == None:
	    geo.Parent = dummy

	FBSystem().Scene.Evaluate()

	dummy.PropertyList.Find("Enable Transformation").Data = False
	dummy.PropertyList.Find("Enable Selection").Data = False
	mover.PropertyList.Find("Enable Transformation").Data = False
	mover.PropertyList.Find("Enable Selection").Data = False

	FBSystem().Scene.Evaluate()

	rs_AssetSetupCreateMarkers()

	FBSystem().Scene.Evaluate()

	rs_AssetSetupTidyScene()

	FBSystem().Scene.Evaluate()

	if gripR:
	    rootControl = FBFindModelByLabelName(exportable.Name + "_Control")
	    gripRControl = FBFindModelByLabelName(gripR.Name + "_Control")           
    
	    gripRControl.Parent = None
	    rootControl.Parent = None
    
	    FBSystem().Scene.Evaluate()
    
	    gripRControl.Parent = dummy
	    rootControl.Parent = gripRControl
    
	    FBSystem().Scene.Evaluate()
    
	    if gripL:
    
		gripLControl = FBFindModelByLabelName(gripL.Name + "_Control")

		gripLControl.Parent = None

		FBSystem().Scene.Evaluate()

		if fileName == "W_SG_BullpupShotgun" or fileName == "W_SG_PumpShotgun" or fileName == "W_SG_SawnOff"  :
		    gripLControl.Parent = FBFindModelByLabelName("Gun_Cock1_Control")
		else:
		    gripLControl.Parent = gripRControl

		FBSystem().Scene.Evaluate()
    
	    typeTag = dummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
	    typeTag.Data = "Props Weapon"
    
	    FBSystem().Scene.Evaluate()
    
	    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

	else:
	    if 'root' in exportable.Name.lower():
		typeTag = dummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
		typeTag.Data = "Props Skinned"
	    else:
		typeTag = dummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
		typeTag.Data = "Props"

	    FBPlayerControl().LoopStart = FBTime(0,0,0,0)
	
	FBSystem().Scene.Evaluate()
	
	rs_AssetSetupMarkerColors(dummy)
	
	rs_AssetSetupLockMarkers(dummy)

	ass.rs_CreateToyBoxMarker()
	
	ass.rs_CleanupMaterials()

	ass.rs_AssetSetupFolderTidy()

	ass.rs_AssetSetupCustomProperties()  

	# checks for render targets in the mesh materials, adds as a custom property url:bugstar:1299744
	RenderTargetCheck()

	# scale down nulls
	ass.ScaleDownNulls()
        
        # setup scale on nulls if required
        scaleConstraint = setupExportScale()
	
	if prefix:
	    geoName = prefix + geo.Name
	else:
	    geoName = geo.Name
	
	if perforceRun == True:
	    FBMessageBox("Complete", geoName + " has now been set up and added to the default Perforce CL. \nCheck it over, SAVE, then submit if everything's good.", "Ok")
	else:
	    FBMessageBox("Complete", geoName + " has now been set up, but the file could not be added to Perforce. \nCheck it over, SAVE, then add to the default CL if everything's good.", "Ok")


    else:
	print "An Error occured in the 'rs_AssetSetupVariables' step."