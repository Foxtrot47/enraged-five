"""
Setups our muscle system to the rigs
"""
from RS.Core.AssetSetup import Animals_2


def Run():
    """ temporary quick setup for muscle setup (only for characters already setup """
    modelSetup = Animals_2.ModelSetup()
    modelSetup.SetupMuscleConstraints()