###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: AssetSetupEnvironments
## Written And Maintained By: Kathryn Bodey
## Contributors: Darren Iddon
## Description: 2 functions to set up environments - 1 for 3ds setup & one for obj setup
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import os
import csv
from RS.Utils import Path, Scene, Creation
from RS.Utils import Exception as RSException
from RS import Config, ProjectData, Globals, Perforce
from RS.Core.AssetSetup import Lib
from PySide import QtGui

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Strip Existing Materials
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################


def dict_add_or_append(dictionary, key, value):
    # Checks whether a key is in a dictionary, adds the value to that key if so. If not, adds a new key with the passed value
    if key not in dictionary.keys():
        dictionary[key] = [value]
    if value not in dictionary[key]:
        dictionary[key].append(value)

def getGroupHierarchy(group, groupHierarchy):
    if len(group.Parents)>1:
        groupHierarchy.append(group.Parents[1].Name)
        getGroupHierarchy(group.Parents[1], groupHierarchy)
    return groupHierarchy

def detectObjectNames(passedObjects):
    # Removes defined prefixes, indices and _rsref from object names to aid categorisation
    
    definedPrefixes = ["ceiling",
                       "door",
                       "dressing",
                       "environment",
                       "floors",
                       "ground",
                       "ipls",
                       "props",
                       "prop",
                       "seating",
                       "vegetation",
                       "wall",
                       "water"]
    
    objectNames = {}
    
    for object in passedObjects:
        # So long as the object isn't a group, we'll add it to the setGroups dictionary, along with its prefix and group hierarchy
        if not isinstance(object, FBGroup):
            prefix = None
            prefixToRemove = None
            objectName = object.Name.lower()
            # Detect the object prefix, checked against the defined prefixes above, and remove it from the name we save to CSV
            if "_" in objectName:
                objectSplit = objectName.split("_")
                index = 0
                for split in objectSplit:
                    # We need to detect numbers after the prefix and remove them from the object name, depending how the rename was done in Max previously
                    if len(objectSplit)-2 > index:
                        numeralPostPrefix = objectSplit[(index+1)]
                        namingAfterNumeral = objectSplit[(index+2)]
                        if split in definedPrefixes and numeralPostPrefix.isdigit() and not namingAfterNumeral.isdigit():
                            prefixToRemove = split + "_" + numeralPostPrefix
                            prefix = split
                            break
                        elif split in definedPrefixes and not namingAfterNumeral.isdigit():
                            prefixToRemove = split
                            prefix = split
                            break
                        elif split in definedPrefixes:
                            prefixToRemove = split
                            prefix = split
                            break
                    elif split in definedPrefixes:
                        prefixToRemove = split
                        prefix = split
                        break
                    index +=1
                if prefixToRemove != None and (prefixToRemove + "_") in objectName:
                    objectName = objectName.split(prefixToRemove + "_", 1)[1]
            
            # Remove instance suffixes
            if "_index" in objectName:
                objectName = objectName.split("_index")[0]
            elif "_rsref" in objectName:
                objectName = objectName.split("_rsref")[0]
        
            objectNames[object] = objectName, prefix
    
    return objectNames


def saveSetCategories(fileName, force=False):
    # Saves out the set categories to a .csv file to read on further conversions both in MB and Max
    project = str( Config.Project.Name ).lower()
    if project == 'gta5':
        outputPath = os.path.join( Config.CoreProject.Path.Root , 'art', 'animation', 'resources', 'sets', 'objectCategories')
    else:
        outputPath = os.path.join(Config.Project.Branch.Path.Art, 'animation', 'resources', 'sets', 'objectCategories')
    
    if not os.path.exists(outputPath):
        os.mkdir(outputPath)
    
    outputFilePath = os.path.join(outputPath, (fileName.lower() + "_categories.csv"))
    
    Perforce.Sync(["-f", outputFilePath])
    
    setGroups = {}
    
    for group in Globals.Groups:
        if len(group.Parents)>1:
            groupHierarchy = []
            getGroupHierarchy(group, groupHierarchy)
            groupHierarchy.reverse()
            groupHierarchy.append(group.Name)
            
            sceneObjectNames = detectObjectNames(group.Items)
            for object in sceneObjectNames.keys():
                objectName = sceneObjectNames[object][0]
                prefix = sceneObjectNames[object][1]
                if prefix != None:
                    groupString = ""
                    groupString += prefix + ","
                    for thing in groupHierarchy:
                        groupString += ">>" + thing
                    dict_add_or_append(setGroups, objectName, groupString )
                else:
                    groupString = ","
                    for thing in groupHierarchy:
                        groupString += ">>" + thing
                    dict_add_or_append(setGroups, objectName, groupString )
    
    # Check the existing .csv file. If we're not force updating, any objects we'd categorised previous that aren't present in the new FBX will still be stored for future conversions
    if force == False:
        if os.path.exists(outputFilePath):
            with open(outputFilePath, "r") as outputFile:
                reader = csv.reader(outputFile)
                next(reader, None)  # skip the headers
                for row in reader:
                    # row[1] is the model name, row[0] the group name
                    if row[0] not in setGroups:
                        dict_add_or_append(setGroups, row[0], (row[1] + "," + row[2]))
    
    # Add to Perforce
    if Perforce.DoesFileExist(outputFilePath) == True:
        Perforce.Edit(outputFilePath)
    else:
        Perforce.Add(outputFilePath)
    
    # Write the CSV
    with open(outputFilePath, "w") as outputFile:
        writer = csv.DictWriter(outputFile, fieldnames = ["Object Name", "Category", "Group Structure"])
        writer.writeheader()
        for object in setGroups.keys():
            if not object == "":
                writer.writerows({'Object Name':object, 'Category':(groupStructure.split(",")[0]), 'Group Structure':groupStructure.split(",")[1]} for groupStructure in setGroups[object])
    
    FBMessageBox("Category Setup", "Object categories have been saved to: \n" + outputFilePath +"\n\nThis file has been checked out in Perforce.\nPlease submit this file along with the set FBX.", "Ok")
    
def restoreSetCategories(fileName):
    project = str( Config.Project.Name ).lower()
    if project == 'gta5':
        inputPath = os.path.join( Config.CoreProject.Path.Root , 'art', 'animation', 'resources', 'sets', 'objectCategories')
    else:
        inputPath = os.path.join(Config.Project.Branch.Path.Art, 'animation', 'resources', 'sets', 'objectCategories')
    
    inputFilePath = os.path.join(inputPath, (fileName.lower() + "_categories.csv"))
    
    Perforce.Sync(inputFilePath)
    
    # Store all the scene object names, without their prefixes
    sceneObjectNames = detectObjectNames(Globals.Components)
    
    sceneGroups = []
    for group in Globals.Groups:
        sceneGroups.append(group.Name)
    
    # Check the existing .csv file, 
    if os.path.exists(inputFilePath):
        with open(inputFilePath, "r") as inputFile:
            reader = csv.reader(inputFile)
            next(reader, None)  # skip the headers
            for row in reader:
                csvGroups = list(row[2].split(">>"))
                objectGroupName = csvGroups[-1]
                matchedObjects = []
                objectName = row[0]
                if not objectName == "":
                    [matchedObjects.append(component) for component in sceneObjectNames.keys() if objectName == sceneObjectNames[component][0]]
                for index in range(len(csvGroups)):
                    csvGroup = csvGroups[index]
                    parentCsvGroup = csvGroups[index-1]
                    if not csvGroup == "" and not parentCsvGroup == "":
                        if not csvGroup in sceneGroups:
                            newGroup = FBGroup(csvGroup)
                            sceneGroups.append(csvGroup)
                        if not parentCsvGroup in sceneGroups:
                            parentGroup = FBGroup(parentCsvGroup)
                            sceneGroups.append(parentCsvGroup)
                        matchedGroup = None
                        matchedParent = None
                        for sceneGroup in Globals.Groups:
                            if sceneGroup.Name == objectGroupName:
                                matchedGroup = sceneGroup
                                for matchedObject in matchedObjects:
                                    sceneGroup.ConnectSrc(matchedObject)
                            elif sceneGroup.Name == csvGroup:
                                matchedGroup = sceneGroup
                            if sceneGroup.Name == parentCsvGroup:
                                matchedParent = sceneGroup
                        if not matchedParent == None and not matchedGroup == None:
                            matchedParent.ConnectSrc(matchedGroup)

def StripExistingMaterials():
    '''
    remove existing materials
    '''
    setList = []

    setRoot = getSetRoot()

    Scene.GetChildren(setRoot, setList, "", False)
    
    removeList = [material for material in Globals.Materials] + [texture for texture in Globals.Textures]
    
    for component in removeList:
        [setItem.DisconnectSrc(component) for setItem in setList]
    [comp.FBDelete() for comp in removeList if comp.Name != "DefaultMaterial"]

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Environment Setup from 3dsMax
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def CreateMaterialsEnvironmentSetup():
    '''
    Create new materials
    '''
    
    matWall = FBMaterial('Wall')
    matGround = FBMaterial('Ground')
    matDressing = FBMaterial('Dressing')
    matProps = FBMaterial('Prop')
    matDes = FBMaterial('DES')
    matPed = FBMaterial('Ped')
    matMirror = FBMaterial('Mirror')
    matDoor = FBMaterial('Door')
    matWater = FBMaterial('Water')
    matVegetation = FBMaterial('Vegetation')

    matWall.Diffuse = FBColor(0.33, 0.8, 0.67)
    matGround.Diffuse = FBColor(0.02, 0.26, 0.44)
    matDressing.Ambient = FBColor(0.86, 0.65, 0.2)
    matDressing.Diffuse = FBColor(0.93, 0.8, 0.23)
    matProps.Ambient = FBColor(0.71, 0.37, 0.2)
    matProps.Diffuse = FBColor(0.97, 0.37, 0)
    matDes.Diffuse = FBColor(1, 0, 1)
    matPed.Diffuse = FBColor(1, 0.67, 1)
    matMirror.Diffuse = FBColor(0.7, 0, 0.8)
    matDoor.Diffuse = FBColor(0.27, 0, 0)
    matWater.Ambient = FBColor(0.2, 0.2, 0.2)
    matWater.Diffuse = FBColor(0, 0.40, 0)
    matWater.DiffuseFactor = 0.70
    matVegetation.Ambient = FBColor(0, 0.3, 0)
    matVegetation.Diffuse = FBColor(0.1, 0.3, 0)
    matVegetation.Shininess = 5
    matVegetation.DiffuseFactor = 0.70

    trans = FBShaderLighted('Transparency')
    FBConnect(matWall, trans)
    FBConnect(matWater, trans)
    matWall.DisplayMode = FBModelShadingMode.kFBModelShadingAll
    trans.Transparency = (FBAlphaSource.kFBAlphaSourceAccurateAlpha)
    trans.Alpha = 0.462
    cartoonShad = FBShaderManager().CreateShader("FlatCartoonShader")
    cartoonShad.PropertyList.Find("All Edges").Data = True
    reflectShad = FBCreateObject("Browsing/Templates/Shading Elements/Shaders", "Reflection", "Reflection")
    reflectShad.Name = "Reflect_Shader"
    reflectStrength = reflectShad.PropertyList.Find("ReflexionFactor")
    reflectStrength.Data = 40
    Globals.Shaders.append(reflectShad)

    materialStr = "Materials"
    shaderStr = "Shaders"

    matAction = matWall

    Creation.rs_CreateFolderAddComponents(Globals.Materials, materialStr)
    Creation.rs_CreateFolderAddComponents(Globals.Shaders, shaderStr)

def SetupGroupsEnvironmentSetup():
    '''
    Creates groups for the objects in the scene to be arranged into
    '''
    setList = []

    FBSystem().CurrentTake.ClearAllProperties(False)
    
    setRoot = getSetRoot()

    Scene.GetChildren(setRoot, setList, "", False)

    typeTag = setRoot.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    typeTag.Data = "Environment"

    setRoot.Show = False

    for camera in Globals.Cameras:
        if camera.Name == "Producer Perspective":
            camera.PropertyList.Find("Far Plane").Data = 400000

    # Set up the main SetRoot group
    mainGrp = FBGroup(setRoot.Name)
    
    # Assign each object in each object type list to their respective groups
    def assignGroups(objectType):
        objectList = []
        for null in setList:
            if objectType == "DES":
                objectType = "DES_"
            nullName = ProjectData.data.GetNullName(null)
            if objectType.lower() in nullName.lower():
                # Add the object to the objectlist if the prefix name is in the object name
                objectList.append(null)
        # Only create the new group if there are any objects of its type in the scene
        if len(objectList) > 0:
            objectGroup = FBGroup(objectType)
            # Attach the new object group as a child of the main SetRoot group
            mainGrp.ConnectSrc(objectGroup)
        for sceneObject in objectList:
            # If an object is already grouped, detach it so each object is only in one group
            if len(sceneObject.Parents) > 2:
                sceneObject.Parents[2].DisconnectSrc(sceneObject)
            # Connect the object to the new group
            objectGroup.ConnectSrc(sceneObject)
        # Hide the ceiling group
        if objectType == "Ceiling" and len(objectList) > 0:
            objectGroup.Show = False
    
    groupsToCreate = ["Ceiling",
                      "Prop",
                      "Dressing",
                      "Wall",
                      "Wall_Dressing",
                      "Ground",
                      "Door",
                      "DES",
                      "Water",
                      "Mirror",]
    
    for group in groupsToCreate:
        assignGroups(group)
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Materials Apply
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def ApplyMaterialsEnvironmentSetup():
    """
    Apply the materials and shaders created earlier to the objects in each group
    """
    # Define the shader variables
    for shader in Globals.Scene.Shaders:
        if shader.Name == "Transparency":
            trans = shader
        if shader.Name == "FlatCartoonShader":
            cartoonShad = shader
        if shader.Name == "Reflect_Shader":
            reflectShad = shader
    
    # Assign each object in each object type list to their respective groups
    def assignMaterials(objectType):
        groupMaterial = None
        objectGroup = None
        for group in Globals.Groups:
            if group.Name == objectType:
                objectGroup = group
        for material in Globals.Materials:
            if objectType == "Wall_Dressing":
                materialName = "Dressing"
                if materialName == material.Name:
                    groupMaterial = material
            elif material.Name == objectType:
                groupMaterial = material
        if objectGroup:
            for item in objectGroup.Items:
                if item.Name != "BaseAnimation":
                    if isinstance(item, FBModel):
                        if len(item.Materials) == 0:
                            item.Materials.append(groupMaterial)
                        if objectType in transparentGroups:
                            item.Shaders.append(trans)
                            item.ShadingMode = FBModelShadingMode.kFBModelShadingAll
                        if objectType in cartoonGroups:
                            Globals.Scene.Evaluate()
                            item.Shaders.append(cartoonShad)
                        if objectType in reflectGroups:
                            item.Shaders.append(reflectShad)
                            item.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    
    groupsToAssign = ["Ceiling",
                      "Mirror",
                      "Wall_Dressing",
                      "Wall",
                      "Dressing",
                      "DES",
                      "Prop",
                      "Ground",
                      "Door",]
    
    transparentGroups = ["Wall",
                         "Wall_Dressing",
                         "Water"]
    
    cartoonGroups = ["Ground"]
    
    reflectGroups = ["Mirror"]
    
    for group in groupsToAssign:
        assignMaterials(group)

    Globals.Scene.Evaluate()

def getSetRoot():
    setRoot = Scene.FindModelByName('SetRoot')
    
    if setRoot == None:
        sceneList = FBModelList()
        FBGetSelectedModels (sceneList, None, False)
        FBGetSelectedModels (sceneList, None, True)
        for sceneItem in sceneList:
            if "_root" in sceneItem.Name.lower():
                setRoot = sceneItem
                setRoot.Name = "SetRoot"

    if setRoot is None:
        raise RSException.PopupError("SetRoot Missing", "This file is missing a 'SetRoot' null.\n\nSetup aborted.")
    return setRoot

def setupCheck(setRoot):
    '''
    check the setup hasnt already been run
    '''
    assetTypeProperty = setRoot.PropertyList.Find('Asset_Type')
    if assetTypeProperty:
        raise RSException.PopupError("Setup Already Done", "This environment has already been through setup.\n\nAborting script.")
    
    # check the setup hasnt been run, and if it has and got to this point, there were obviously
    # issues with that setup as it should have had the property 'Asset_Type', checked for in the
    # above lines
    for material in Globals.Materials:
        if material.Name == 'Prop':
            raise RSException.PopupError("Setup Already Done",
                                      "This environment has already been through setup, although seems to be missing properties.\n\
                                      It is advised that you set up this environment from scratch again.\n\nAborting script.")
    
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - 3dsMax Environment Setup
##                         - Setus up materials, groups, custom properties & applies materials.
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def maxEnvironmentSetup(pControl, pEvent):
    '''
    Sets up materials, groups, custom properties & applies materials.
    '''
    setRoot = getSetRoot()

    setupCheck(setRoot)
    
    StripExistingMaterials()
    
    CreateMaterialsEnvironmentSetup()
    
    SetupGroupsEnvironmentSetup()

    ApplyMaterialsEnvironmentSetup()

    Lib.rs_AssetSetupCustomProperties()
    
    typeTag = setRoot.PropertyCreate('Asset_Setup', FBPropertyType.kFBPT_charptr, "", False, True, None)
    typeTag.Data = "3dsMax"

    # get parent and children in a list and disable transforms
    setList = []
    Scene.GetChildren(setRoot, setList, '', True)
    for null in setList:
        enableTrnsProp = null.PropertyList.Find('Enable Transformation')
        if enableTrnsProp:
            enableTrnsProp.Data = False

    # scale down nulls
    Lib.ScaleDownNulls()

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)
    
    # Restore any set category hierarchy from previous conversions
    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    restoreSetCategories(fileName)
    
    QtGui.QMessageBox.information(None, "3ds Setup", fileName + " has now been setup.")