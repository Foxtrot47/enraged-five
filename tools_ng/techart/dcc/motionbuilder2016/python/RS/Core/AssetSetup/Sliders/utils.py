import inspect
import os
import uuid

from xml.etree.cElementTree import parse as xmlParser

import pyfbsdk as mobu

from RS.Utils import Scene
from RS.Utils.Scene import RelationshipConstraintManager as rcm

from RS import Perforce

from RS.Core.AssetSetup.Sliders import components as sliderElements

from PyExprSolver import RSExprSolver

from RS.Utils import Path

from RS import Globals


class Slider(object):
    NODE_PREFIXES = ['RECT_', 'TEXT_', '']#'CTRL_'

    NODE_ATTRIBUTES = ['rectangleOutline', 'textShape', 'animationControl']

    SLIDER_GROUP = 'Sliders'

    def __init__(self):
        self.rectangleOutline = None
        self.textShape = None
        self.animationControl = None

    def setSliderLimits(self,
                        overrideRange=[0.0, 1.0]):
        dofSetting = self.animationControl.PropertyList.Find('Enable Translation DOF')
        dofSetting.Data = True
        
        for attribute in ['TranslationMinX',
                          'TranslationMaxX',
                          'TranslationMinZ',
                          'TranslationMaxZ',
                          'TranslationMinY',
                          'TranslationMaxY']:
            self.animationControl.PropertyList.Find(attribute).Data = True

        self.animationControl.PropertyList.Find('TranslationMin').Data = mobu.FBVector3d(0.0, overrideRange[0], 0.0)
        self.animationControl.PropertyList.Find('TranslationMax').Data = mobu.FBVector3d(0.0, overrideRange[1], 0.0)

    def matchAssetPattern(self, assetName, controlPrefix):
        targetNodeList = mobu.FBComponentList()
        validNode = None

        assetPattern = '{prefix}{asset}'
        assetPattern = assetPattern.format(prefix=controlPrefix, 
                                           asset=assetName)

        mobu.FBFindObjectsByName(assetPattern,
                                 targetNodeList,
                                 False,
                                 True)

        if len(targetNodeList) > 0:
            validNode = targetNodeList[0]

        return validNode

    def collectFromScene(self, 
                         assetName,
                         inputNamespace=None):
        for attributeIndex, attribute in enumerate(self.NODE_PREFIXES):
            validNode = None

            assetPattern = '{prefix}{asset}'
            assetPattern = assetPattern.format(prefix=attribute, 
                                               asset=assetName)

            if inputNamespace is None:
                validNode = Scene.FindModelByName(assetPattern)
            else:
                sampleName = '{0}:{1}'.format(inputNamespace,
                                              assetPattern)

                sliderNodeList = mobu.FBComponentList()
                mobu.FBFindObjectsByName(sampleName,
                                         sliderNodeList,
                                         True,
                                         True)

                if len(sliderNodeList)>0:
                    validNode = sliderNodeList[0]
                else:
                    validNode = None

            if validNode is None:
                targetNode = None
            else:
                targetNode = validNode

            setattr(self, self.NODE_ATTRIBUTES[attributeIndex], targetNode)

    def createSliderGroup(self, sliderControl):
        sliderGroup = None

        for group in mobu.FBSystem().Scene.Groups:
            if group.Name == self.SLIDER_GROUP:
                sliderGroup = group
        
        if sliderGroup is None:
            sliderGroup = mobu.FBGroup(self.SLIDER_GROUP)

        sliderGroup.ConnectSrc(sliderControl)

        return sliderGroup

    def createCharacterExtension(self, targetExtension):
        character = mobu.FBApplication().CurrentCharacter
        if character is None:
            return

        characterExt = None
        for characterExtension in mobu.FBSystem().Scene.CharacterExtensions:
            if characterExtension.Label == targetExtension:
                characterExt = characterExtension

        if characterExt is None:
            characterExt = mobu.FBCharacterExtension(targetExtension)
            characterExt.Label = targetExtension

            rsProperty = characterExt.PropertyCreate('rs_Type', mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)
            rsProperty.Data = "rs_Extensions"

        folder = None
        for currentfolder in Globals.gFolders:
            if currentfolder.Name == "character_extensions":
                folder = currentfolder

        if folder is not None:
            folder.ConnectSrc(characterExt)

        character.AddCharacterExtension(characterExt)

        mobu.FBConnect(self.animationControl, characterExt)

        return characterExt

    def validateAsset(self):
        sliderComponentIsMissing = False

        for attributeIndex, attribute in enumerate(self.NODE_PREFIXES):
            component = getattr(self, self.NODE_ATTRIBUTES[attributeIndex])

            if component is None:
                sliderComponentIsMissing = True
                continue

            sliderGroup = self.createSliderGroup(component)

        if sliderComponentIsMissing:
            return None, None

        sliderControls = [self.animationControl]

        return sliderControls, sliderGroup

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)
        reportData += '\n\t{0}'.format('<Components:>')

        for attribute in self.NODE_ATTRIBUTES:
            currentAttribute = getattr(self, attribute)
            if currentAttribute is None:
                reportData += '\n\t\t{0}:None'.format(attribute)
            else:
                reportData += '\n\t\t{0}:{1}'.format(attribute,
                                                     currentAttribute.LongName)

        return reportData


class Builder(object):
    SLIDER_GROUP = 'Sliders'
    DUMMY_GROUP = 'Dummy01'
    RIG_CONSTRAINT_GROUP = "Constraints 1"

    EXPRESSION_ANCHOR = 'object'
    GENERIC_CONTROL_SUFFIX = 'genericControl'
    EXPRESSION_SOLVER_NAME = 'RSExprSolverConstraint'

    TRACK_ATTRIBUTE = 'track'

    EXPRESSION_SLIDER_ID_ATTRIBUTE = 'id'

    SLIDER_DEFAULT_OFFSET = 30.0

    def getExpressionSolverConstraint(self):
        sceneConstraints = [component for component in mobu.FBSystem().Scene.Constraints]

        for constraint in sceneConstraints:
            if constraint.ClassName() == self.EXPRESSION_SOLVER_NAME:
                return constraint

        return None

    def collectExpressionFiles(self, currentSolver):
        solver = RSExprSolver(currentSolver.LongName)
        if solver is None:
            print 'Could not find Expression solver constraint'

        expressionCount = solver.GetExpressionFileCount()

        expressionFiles = []
        for expressionFileIndex in xrange(expressionCount):
            expressionFile = solver.GetExpressionFile(expressionFileIndex)
            expressionFile = expressionFile.replace('\\', '/')
            expressionFile = expressionFile.replace('.expr', '.xml')
            expressionFiles.append(expressionFile)

        return expressionFiles

    def collectGenericControlsFromXml(self, expressionFile):
        if not os.path.exists(expressionFile):
            return None

        inputfile = open(expressionFile, "r")
        inputTree = xmlParser(inputfile)

        rootElement = inputTree.getroot()

        xmlObjectArray = rootElement.findall(self.EXPRESSION_ANCHOR)
        genericControls = []

        for objectNode in xmlObjectArray:
            if self.TRACK_ATTRIBUTE not in objectNode.keys():
                continue
            
            if str(objectNode.get(self.TRACK_ATTRIBUTE)) != self.GENERIC_CONTROL_SUFFIX:
                continue

            control = str(objectNode.get(self.EXPRESSION_SLIDER_ID_ATTRIBUTE))
            control = control.replace('CTRL_', '')

            genericControls.append(control)

        return genericControls

    def collectGenericControls(self, expressionFileArray):
        if len(expressionFileArray) == 0:
            return None

        genericControls = []
        for expressionFile in expressionFileArray:
            controls = self.collectGenericControlsFromXml(expressionFile)
            genericControls.extend(controls)

        return genericControls

    def deletePreviousSliders(self, genericControls):
        currentSceneNodeList = mobu.FBComponentList()
        mobu.FBFindObjectsByName('*',
                                 currentSceneNodeList,
                                 True,
                                 False)

        sliderControls = []
        sliderRoot = None

        for component in currentSceneNodeList:
            #'Slider_RagdollSlider'
            joyStickAttribute = component.PropertyList.Find('UDP3DSMAX')

            if joyStickAttribute is None:
                if component.Name.startswith('RECT'):
                    sliderControls.append(component)

                continue

            if 'joystick' not in joyStickAttribute.Data:
                continue

            if 'joystickSurround' not in joyStickAttribute.Data:
                continue

            sliderControls.append(component)

        #Now collect previous slider
        deleteArray = []
        previousSliders = []

        for component in sliderControls:
            skipControl = False
            for assetName in genericControls:
                controlName = assetName.lower()
                controlName = controlName.replace('slider_', '')
                controlName = controlName.replace('_slider', '')
            
                if controlName.lower() in str(component.Name).lower():
                    previousSliders.append([component, assetName])
                    skipControl = True

                    continue

            if skipControl is True:
                continue

            excludeSlider = True
            for controlName in genericControls:
                if controlName in component.Name:
                    excludeSlider = False

            if excludeSlider is True:
                continue

            deleteArray.append(component)

            for child in component.Children:
                deleteArray.append(child)

        deleteArray = list(set(deleteArray))
        for component in deleteArray:
            if component is None:
                continue

            if not isinstance(component, mobu.FBModel):
                continue

            self.removePreviousRelationConstraint(component)
            component.FBDelete()

        return previousSliders

    def createGenericControls(self):
        currentSolver = self.getExpressionSolverConstraint()
        if currentSolver is None:
            print 'Could not find expression solver in this scene'
            return

        expressionFiles = self.collectExpressionFiles(currentSolver)

        genericControlsPrefixArray = self.collectGenericControls(expressionFiles)

        if genericControlsPrefixArray is None:
            print 'Could not find generic control to create from expression xml file'
            return

        if len(genericControlsPrefixArray) == 0:
            print 'Could not find generic control to create from expression xml file'
            return

        sliders = []
        sliderGroup = None

        dummyRoot = Scene.FindModelByName(self.DUMMY_GROUP)

        for assetIndex, assetName in enumerate(genericControlsPrefixArray):
            pinningUtils = Slider()
            pinningUtils.collectFromScene(assetName)

            slider, sliderGroup =  pinningUtils.validateAsset()

            if slider is None:
                slider = sliderElements.sliderShapeBuilder(assetName)
                slider.create()

            offsetVectorValue = (assetIndex+1)*self.SLIDER_DEFAULT_OFFSET
            pinningUtils.collectFromScene(assetName)
            pinningUtils.rectangleOutline.Translation.Data = mobu.FBVector3d(offsetVectorValue,
                                                                             -10.0,
                                                                             0.0)

            for component in [pinningUtils.animationControl,
                              pinningUtils.rectangleOutline,
                              pinningUtils.textShape]:
                sliderGroup = self.createSliderGroup(component)

            sliders.append(pinningUtils.animationControl)

            if dummyRoot is not None:
                pinningUtils.rectangleOutline.Parent = dummyRoot

        if len(sliders) == 0:
            print 'No valid slider to create - Aborting'
            return

        if sliderGroup is None:
            print 'No valid slider to create - Aborting'
            return

        self.connectSlider(sliders, sliderGroup)

    def setSliderLimits(self, sliderControl, overrideRange=[0.0, 1.0]):
        dofSetting = sliderControl.PropertyList.Find('Enable Translation DOF')
        dofSetting.Data = True
        
        for attribute in ['TranslationMinX',
                          'TranslationMaxX',
                          'TranslationMinZ',
                          'TranslationMaxZ',
                          'TranslationMinY',
                          'TranslationMaxY']:
            sliderControl.PropertyList.Find(attribute).Data = True

        sliderControl.PropertyList.Find('TranslationMin').Data = mobu.FBVector3d(0.0, overrideRange[0], 0.0)
        sliderControl.PropertyList.Find('TranslationMax').Data = mobu.FBVector3d(0.0, overrideRange[1], 0.0)

    def createSliderGroup(self, 
                          sliderControl, 
                          sliderGroupName=None):
        if sliderControl is None:
            return None
        if sliderGroupName == None:
            sliderGroupName = str(self.SLIDER_GROUP)

        sliderGroup = None
        dummyGroup = None

        for group in mobu.FBSystem().Scene.Groups:
            if group.Name == sliderGroupName:
                sliderGroup = group

            if group.Name == self.DUMMY_GROUP:
                dummyGroup = group
        
        if sliderGroup is None:
            sliderGroup = mobu.FBGroup(sliderGroupName)

        if dummyGroup is not None:
            dummyGroup.GetDst(1).ConnectSrc(sliderGroup)

        sliderGroup.ConnectSrc(sliderControl)

        return sliderGroup

    def importSliderControls(self, assetName):
        toolPath = os.path.dirname(inspect.getfile(self.createSliderGroup))
        sliderAssetFile = os.path.join(toolPath, assetName)
        sliderGroup = None

        if not os.path.exists(sliderAssetFile):
            Perforce.Sync(sliderAssetFile, force=True)

        mergeOptions = mobu.FBFbxOptions(True, sliderAssetFile)
        mergeOptions.SetAll(mobu.FBElementAction.kFBElementActionDiscard , False)

        mergeOptions.Bones = mobu.FBElementAction.kFBElementActionAppend
        mergeOptions.Characters = mobu.FBElementAction.kFBElementActionAppend
        mergeOptions.Models = mobu.FBElementAction.kFBElementActionAppend
        mergeOptions.Solvers = mobu.FBElementAction.kFBElementActionAppend

        uuidValue = uuid.uuid1()
        namespacePrefix = 'sliderAsset_{0}'.format(uuidValue)
        mergeOptions.NamespaceList = namespacePrefix

        mergeOptions.EmbedMedia = False 
        mergeOptions.UpdateRecentFiles = False

        mobu.FBApplication().FileAppend(sliderAssetFile, False, mergeOptions)

        sceneNamespace = mobu.FBSystem().Scene.Namespaces
        sliderControls = []
        sliderRoot = None

        namespaceOperation = mobu.FBNamespaceAction.kFBRemoveAllNamespace
        sliderNames = []
        sliderComponents = []

        for namespace in sceneNamespace:
            if not namespacePrefix in namespace.LongName: 
                continue

            sliderComponents = [namespace.GetContent(index) 
                                for index in xrange(namespace.GetContentCount())]

            for component in sliderComponents:
                if component.Parent == None:
                    sliderRoot = component
                    sliderNames.append(sliderRoot.Name)

                #'Slider_RagdollSlider'
                joyStickAttribute = component.PropertyList.Find('UDP3DSMAX')

                if joyStickAttribute is None:
                    continue
                
                if 'joystick' in joyStickAttribute.Data and \
                    'TRACK_GENERIC_CONTROL' not in joyStickAttribute.Data:
                    continue

                sliderControls.append(component)
                sliderNames.append(component.Name)

        dummyRoot = Scene.FindModelByName(self.DUMMY_GROUP)

        #Now collect previous slider
        deleteArray = []

        sceneComponents = [component for component in mobu.FBSystem().Scene.Components]

        for component in sceneComponents:
            if component in sliderComponents:
                continue

            if component.Name in sliderNames:
                if isinstance(component, mobu.FBModelPlaceHolder):
                    continue

                joyStickAttribute = component.PropertyList.Find('UDP3DSMAX')

                if joyStickAttribute is None:
                    continue

                isvalidControl = False
                if 'joystick' in joyStickAttribute.Data or \
                    'TRACK_GENERIC_CONTROL' in joyStickAttribute.Data:
                    isvalidControl = True

                if isvalidControl is False:
                    continue

                deleteArray.append(component)

                if component.Parent is None:
                    continue

                if dummyRoot is not None:
                    if component.Parent == dummyRoot:
                        continue

                joyStickAttribute = component.Parent.PropertyList.Find('UDP3DSMAX')

                if joyStickAttribute is None:
                    continue

                deleteArray.append(component.Parent)

                if sliderRoot is not None:
                    if component.Name == sliderRoot.Name:
                        continue

                for child in component.Parent.Children:
                    deleteArray.append(child)

        #Delete Components
        deleteArray = list(set(deleteArray))
        for component in deleteArray:
            if component is None:
                continue

            if not isinstance(component, mobu.FBModel):
                continue

            self.removePreviousRelationConstraint(component)
            component.FBDelete()

        namespaceToRemove = None
        #Remove namespace prefix
        for namespace in sceneNamespace:
            if not namespacePrefix in namespace.LongName: 
                continue

            namespaceToRemove = namespace

        for component in sliderComponents:
            sliderGroup = self.createSliderGroup(component)
            component.ProcessNamespaceHierarchy(namespaceOperation,
                                                namespacePrefix)

        if dummyRoot is not None:
            if sliderRoot is not None:
                sliderRoot.Parent = dummyRoot

        if namespaceToRemove is not None:
            namespaceToRemove.FBDelete()

        return sliderControls, sliderGroup

    def removePreviousRelationConstraint(self, sliderControl):
        for sourceIndex in range(sliderControl.GetSrcCount()-1):
            relationConstraint = sliderControl.GetSrc(sourceIndex)

            if relationConstraint is None:
                continue

            if isinstance(relationConstraint, mobu.FBConstraintRelation) is False:
                continue

            print '\tremovePreviousRelationConstraint --> sliderControl', sliderControl.Name
            relationConstraint.FBDelete()

    def setupSliderAttribute(self, 
                             sliderControl,
                             sliderAttributeName,
                             sliderConstraintPrefix,
                             overrideRange=[0.0, 1.0]):
        '''
         the ragdoll slider enables springs on the bones
         e.g. when an animator turns the slider on in game the limbs go wobbly
        '''
        # create slider property
        attributeType = mobu.FBPropertyType.kFBPT_double
        ragdollProperty = sliderControl.PropertyList.Find(sliderAttributeName)

        self.setSliderLimits(sliderControl,
                             overrideRange=overrideRange)

        if ragdollProperty is None:
            ragdollProperty = sliderControl.PropertyCreate(sliderAttributeName,
                                                            attributeType,
                                                            "",
                                                            True,
                                                            True,
                                                            None)
            ragdollProperty.SetMax(1.0)
            ragdollProperty.SetMin(0.0)
            ragdollProperty.SetAnimated(True)

        self.removePreviousRelationConstraint(sliderControl)

        # create relation constraint
        relationConstraintName = '{}_Relation'.format(sliderConstraintPrefix)
        relationConstraint = rcm.RelationShipConstraintManager(name=relationConstraintName)
        relationConstraint.SetActive(True)

        # Add senders/receivers to the Relation Constraint
        sender = relationConstraint.AddSenderBox(sliderControl)
        sender.SetBoxPosition(0, 0)
        sender.SetGlobalTransforms(False)

        vectorToNumber = relationConstraint.AddFunctionBox("Converters",
                                                           "Vector to Number")

        vectorToNumber.SetBoxPosition(300, 0)

        receiver = relationConstraint.AddReceiverBox(sliderControl)
        receiver.SetBoxPosition(600, 0)
        receiver.SetGlobalTransforms(False)

        weightAttribute = sliderControl.PropertyList.Find(sliderAttributeName)
        if weightAttribute is None:
            weightAttribute = sliderControl.PropertyCreate(sliderAttributeName,
                                                           mobu.FBPropertyType.kFBPT_double,
                                                           True,
                                                           True,
                                                           None)

        weightAttribute.SetAnimated(True)

        relationConstraint.ConnectBoxes(sender, 
                                        'Lcl Translation', 
                                        vectorToNumber, 'V')

        relationConstraint.ConnectBoxes(vectorToNumber,
                                        'Y', 
                                        receiver,
                                        sliderAttributeName)

        rootConstraintFolder = None
        for folder in mobu.FBSystem().Scene.Components:
            if not isinstance(folder, mobu.FBFolder):
                continue

            if folder.Name == self.RIG_CONSTRAINT_GROUP:
                rootConstraintFolder = folder

        if rootConstraintFolder is not None:
            rootConstraintFolder.ConnectSrc(relationConstraint.GetConstraint())

    def connectSlider(self, 
                      sliders, 
                      sliderGroup, 
                      useSuffix=False,
                      overrideRange=[0.0, 1.0]):
        for slider in sliders:
            sliderAttributeName = slider.Name

            if useSuffix is True:
                sliderPrefix = slider.Name.replace('CTRL_', '')

                suffix = 'Slider'
                if sliderPrefix.endswith(suffix):
                    sliderPrefix = sliderPrefix.replace('slider', suffix)
                    suffix = ''

                sliderAttributeName = '{0}{1}'.format(sliderPrefix.title(),
                                                      suffix)

                sliderAttributeName = sliderAttributeName.replace('slider', 'Slider')

            sliderConstraintPrefix = slider.Name.lower()

            self.setupSliderAttribute(slider,
                                      sliderAttributeName,
                                      sliderConstraintPrefix,
                                      overrideRange=overrideRange)

    def createSliders(self, assetName, useSuffix=False):
        sliders, sliderGroup = self.importSliderControls(assetName)

        self.connectSlider(sliders, 
                           sliderGroup,
                           useSuffix=useSuffix)

        return sliderGroup