import pyfbsdk as mobu

import os

from RS import Globals
from RS.Utils import Scene
from RS.Core.AssetSetup import Characters


# delete existing rigs due to incorrect setup url:bugstar:2474689
deleteList = []

modelList = ["OH_Pelvis_Control",
            "Mover_Tracer",
            "Offset_Parent_Node",
            "Offset_Node",
            "Root_Tracer",
            "Zero_Node"]

constraintList = ['moverTracer_parentChild',
                  'pelvisOffset_parentChild',
                  'rootTracer_parentChild',
                  'OH_Pelvis_Export',
                  'OH_Pelvis_Lock']


for model in modelList:
    sceneModel = Scene.FindModelByName(model)
    if sceneModel:
        deleteList.append(sceneModel)

for constraint in constraintList:
    for component in Globals.Components:
        if component.Name == constraint and isinstance(component, (mobu.FBConstraint, mobu.FBConstraintRelation)):
            deleteList.append(component)

for delete in deleteList:
    delete.FBDelete()

# run setup
Characters.OHPelvisSetup()
# save file
filePath = Globals.Application.FBXFileName
Globals.Application.FileSave(filePath)