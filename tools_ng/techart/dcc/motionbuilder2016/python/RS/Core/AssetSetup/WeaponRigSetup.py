from pyfbsdk import *


import RS.Utils.Scene
import RS.Globals
import os
from xml.dom import minidom

def GetModelTransform( bModel ):
    trans = FBVector3d()
    bModel.GetVector( trans, FBModelTransformationType.kModelTranslation )
    rot = FBVector3d()
    bModel.GetVector( rot, FBModelTransformationType.kModelRotation )
    
    return trans, rot

def GetModelsChildren( bModel, allChildren=True):
    children = bModel.Children
    retChildren = []
    for child in children:
        retChildren.append(child)
        
        if allChildren:
            retChildren += GetModelsChildren(child)
        
    return retChildren


class ControllerMapTemplateEntry:
    def __init__(self):
        self.__nodeName = ""
        self.__snapName = ""
        
        
    def SetNodeName(self, name):
        self.__nodeName = name
        
    def SetSnapName(self, name):
        self.__snapName = name
        
    
    def GetNodeName(self):
        return self.__nodeName
        
    def GetSnapName(self):
        return self.__snapName
        
            

class ControllerMapTemplate:
    def __init__(self):
        self.controllerMaps = {}
        
    def LoadControllerSettings(self, settingsXmlFilePath):
        
        if os.path.exists(settingsXmlFilePath):
            domParse = minidom.parse(settingsXmlFilePath)
            xmlControlElements = domParse.getElementsByTagName('control')
            
            for controlElement in xmlControlElements:
                
                newControllerMap = ControllerMapTemplateEntry()
                for attr in controlElement.attributes.keys():
                    if attr == "name":
                        newControllerMap.SetNodeName(controlElement.attributes[attr].value)
                        
                    if attr == "snap":
                        newControllerMap.SetSnapName(controlElement.attributes[attr].value)
                    
                if newControllerMap.GetNodeName() != "":
                    self.controllerMaps[newControllerMap.GetNodeName()] = newControllerMap
                    
                else:
                    print "Could not find nodes name, skipping node!"
                    

class WeaponRigController:
    def __init__(self, component, controllerName):
        print "-->", controllerName
        self.__name = controllerName
        self.__component = component
        self.__constraints = {}
        self.__active = False

    def GetActive(self):
        return self.__active

    def GetName(self):
        return self.__name

    def SetActive(self, boolState):
        if isinstance(boolState, bool):
            self.__active = boolState
            
        elif isinstance(boolState, int):
            if boolState > 0:
                self.__active = True
            else:
                self.__active = False
                
        

    def AddConstraint(self, constraint):
        if constraint.Name not in self.__constraints.keys():
            self.__constraints[constraint.Name] = constraint

        
    def CreateConstraints(self, rigRootNode, weaponDummy):
        lMgr = FBConstraintManager()
    
        print "--->", type(self.__name)
        driverConstraint = lMgr.TypeCreateConstraint( 3 )
        print "Const:", driverConstraint.Name, driverConstraint
        driverConstraint.Name = str(self.__name) + "_driver"
        driverConstraint.ReferenceAdd(0,rigRootNode)
        driverConstraint.ReferenceAdd(1,self.__component)
        self.__constraints['driver'] = driverConstraint
        
        drivenConstraint = lMgr.TypeCreateConstraint( 3 )
        drivenConstraint.Name = str(self.__name) + "_driven"
        drivenConstraint.ReferenceAdd(0,self.__component)
        drivenConstraint.ReferenceAdd(1,rigRootNode)
        self.__constraints['driven'] = drivenConstraint
        
        self.__component.Parent = weaponDummy
        
        
    def ToggleActive(self, active=True):
        self.SetActive(active)
        for constraint in self.__constraints.keys():
            if "gun_root" not in constraint and "gun_main" not in constraint:
                if self.__active == True:
                    if "driver" in constraint:
                        self.__constraints[constraint].Snap()
                    else:
                        self.__constraints[constraint].Active = False
                else:
                    if "driver" in constraint:
                        self.__constraints[constraint].Active = False
                    else:
                        self.__constraints[constraint].Snap()
            else:
                print "RIGROOT:{0}".format(constraint.Name)
                self.__constraints[constraint].Snap()

class WeaponRig:
    def __init__(self, weaponComponent):
        self.__name = ""
        self.__weaponComponent = weaponComponent
        self.__weaponName = weaponComponent.Name
        self.__weaponNamespace = weaponComponent.LongName
        self.__rigControllers = {}
        self.__rigConstraints = {}
        self.__rigRootController = None
        self.__rigRootComponent = None
        self.__childRigNodes = {}
        self.__weaponRigNodes = GetModelsChildren(self.__weaponComponent)
        print "Finding model.."
        self.FindWeaponModel()

    def SetName(self, name):
        self.__name = name
        
    def GetName(self):
        return self.__name

    def GetRigControllers(self):
        return self.__rigControllers

    def SetRigRootComponent(self, component):
        self.__rigRootComponent = component

    def FindWeaponModel(self):
        for component in self.__weaponRigNodes:
            
            if "w_" == component.Name.lower()[0:2] and "weaponrigroot" not in component.Name.lower():
                print "Assuming this is the model:", component.Name
                self.__weaponName = component.LongName

    def DeActivateAllControllers(self):
        for controller in self.__rigControllers.keys():
            print "---->", controller
            print "==--->", self.__rigControllers[controller]
            self.__rigControllers[controller.lower()].ToggleActive(active = False)
        
    def ActivateController(self, controllerName):
        for control in self.__rigControllers.keys():
            print "CONTROLKEY:", control
        if controllerName.lower() in self.__rigControllers.keys():
            self.__rigControllers[controllerName.lower()].ToggleActive(active=True)
            
        elif "gun_root" in controllerName.lower() or "gun_main" in controllerName.lower():
            self.__rigRootController.ToggleActive(active=True)


    def FindRigNodes(self):
        #Finds the two necessary controls in the existing gun rig

        for component in self.__weaponRigNodes:
            if "gun_root_" in component.Name.lower():
                #Finds the gun root control, not the joint.
                self.__childRigNodes['gun_root'] = component
                
            if "gun_main_bone_control" in component.LongName.lower():
                self.__childRigNodes['gun_main'] = component
        

    def PopulateRigFromRigNode(self, rigNode, rigTemplate):
        scene = FBSystem().Scene     
        
        for constraint in scene.Constraints:
            
            constrainedProperty = constraint.PropertyList.Find('Constrained object (Child)')
            parentProperty = constraint.PropertyList.Find('Source (Parent)')
            

            
            if parentProperty != None and constrainedProperty != None:
                constrainedProperty = constrainedProperty.GetDst(0)
                parentProperty = parentProperty.GetSrc(0)
                
                if parentProperty == self.__rigRootComponent:
                    #If it is a weaponrig constraint then use the weapon rig, otherwise use the other component
                    if "gun_root" in constrainedProperty.Name.lower() or "gun_main" in constrainedProperty.Name.lower():
                        if self.__rigRootController == None:
                            self.__rigRootController = WeaponRigController(parentProperty, parentProperty.Name)
                        self.__rigRootController.AddConstraint(constraint)
                        
                    else:
                        #We are not a weaponrig node constraint...
                        if constrainedProperty.Name.lower() not in self.__rigControllers.keys():
                            self.__rigControllers[constrainedProperty.Name.lower()] = WeaponRigController(constrainedProperty, constrainedProperty.Name)

                        self.__rigControllers[constrainedProperty.Name.lower()].AddConstraint(constraint)
                    
                else:

                    if parentProperty.Name.lower() not in self.__rigControllers.keys():
                            self.__rigControllers[parentProperty.Name.lower()] = WeaponRigController(parentProperty, parentProperty.Name)

                    self.__rigControllers[parentProperty.Name.lower()].AddConstraint(constraint)

        '''        
        #Try to find the correct controller name from the control template
        groupedConstraints = {}

        for constraint in rigConstraints.keys():
            controllerName = rigConstraints[constraint].Name
            for templateEntry in rigTemplate.controllerMaps.keys():
                if rigTemplate.controllerMaps[templateEntry].GetNodeName() in controllerName:
                    
                    #If the entry has been added to the grouped constraints, append...
                    if rigTemplate.controllerMaps[templateEntry].GetNodeName() in groupedConstraints.keys():
                        
                        groupedConstraints[rigTemplate.controllerMaps[templateEntry].GetNodeName()].append([constraint,rigConstraints[constraint]] )
                    else:
                        groupedConstraints[rigTemplate.controllerMaps[templateEntry].GetNodeName()] = [[constraint,rigConstraints[constraint]]]


        for key in groupedConstraints.keys():
                        
            # Setup the rig controller with the                             
            newControllerInst = WeaponRigController(groupedConstraints[key][0][1], key) #Use the second entry in any index as they will be the same
            
            for constraint in groupedConstraints[key]:
                newControllerInst.AddConstraint(constraint[0])
            self.__rigControllers[controllerName] = newControllerInst    
        
        for controlKey in self.__rigControllers.keys():
            
            print self.__rigControllers[controlKey].GetName()
            #print control.__component
            #print control.__constraints
    
        '''
        
        
    def SetupRig(self, rigTemplate):
        
        self.FindRigNodes()
        
        #Setup the rig root controller
        print self.__childRigNodes.keys()
        rootTrans, rootRot = GetModelTransform(self.__childRigNodes['gun_root'])
        self.__rigRootComponent = FBModelNull(self.__weaponName + "_WeaponRigRoot")
        self.__rigRootComponent.SetVector( rootTrans,  FBModelTransformationType.kModelTranslation )
        self.__rigRootComponent.SetVector( rootRot,  FBModelTransformationType.kModelRotation )
        
        
        lMgr = FBConstraintManager()
    
        # Unparent the gun root and gun main 
        self.__childRigNodes['gun_root'].Parent = None
        self.__childRigNodes['gun_main'].Parent = None
        
        # Create the gun root control parent constraint
        driverConstraint = lMgr.TypeCreateConstraint( 3 )
        driverConstraint.Name = self.__rigRootComponent.Name + "_" + self.__childRigNodes['gun_root'].Name
        driverConstraint.ReferenceAdd(0,self.__childRigNodes['gun_root'])
        driverConstraint.ReferenceAdd(1,self.__rigRootComponent)
        self.__rigConstraints['rigroot_gun_root'] = driverConstraint
        
        # Create the main gun control parent constraint
        driverConstraint = lMgr.TypeCreateConstraint( 3 )
        driverConstraint.Name = self.__rigRootComponent.Name + "_" + self.__childRigNodes['gun_main'].Name
        driverConstraint.ReferenceAdd(0,self.__childRigNodes['gun_main'])
        driverConstraint.ReferenceAdd(1,self.__rigRootComponent)
        self.__rigConstraints['rigroot_gun_main'] = driverConstraint
        
        self.__rigRootComponent.Parent = self.__weaponComponent
        self.__rigRootController = WeaponRigController(self.__rigRootComponent, self.__rigRootComponent.LongName)
        self.__rigRootController.AddConstraint(self.__rigConstraints['rigroot_gun_root'])
        self.__rigRootController.AddConstraint(self.__rigConstraints['rigroot_gun_main'])
        
        for component in self.__weaponRigNodes:

            if component.LongName.lower() != self.__rigRootController.GetName().lower():
                #Finds the weapon rig nodes based on the template
                for templateEntry in rigTemplate.controllerMaps.keys():
                    if rigTemplate.controllerMaps[templateEntry].GetNodeName() in component.LongName.lower():
                        
                        print rigTemplate.controllerMaps[templateEntry].GetNodeName()
                        newControllerInst = WeaponRigController(component, rigTemplate.controllerMaps[templateEntry].GetNodeName())
                        newControllerInst.CreateConstraints(self.__rigRootComponent, self.__weaponComponent)
                        self.__rigControllers[rigTemplate.controllerMaps[templateEntry].GetNodeName()] = newControllerInst

                
            
        
        
        
class WeaponRigManager:
    def __init__(self):
        self.__weaponRigs = {}
        self.__rigTemplateXmlPath = r"X:\wildwest\dcc\motionbuilder2014\python\etc\weaponControlSettings.xml"
        self.__rigTemplate = ControllerMapTemplate()
        self.__rigTemplate.LoadControllerSettings(self.__rigTemplateXmlPath)

    def GetWeaponRigs(self):
       return self.__weaponRigs
        
    def SetRigTemplate(self, filePath):
        self.__rigTemplateXmlPath = filePath
        self.__rigTemplate.LoadControllerSettings(self.__rigTemplateXmlPath)
        
    def ActivateController(self, rigName, controllerName):
        self.__weaponRigs[rigName].DeActivateAllControllers()
        print rigName, controllerName
        self.__weaponRigs[rigName].ActivateController(controllerName)
        
    def SetupRigOnComponent(self, component):
        if component.LongName in self.__weaponRigs:
            print "Rig is already setup..."
        else:
            self.__weaponRigs[str(component.LongName)] = WeaponRig(component)
            
            self.__weaponRigs[str(component.LongName)].SetupRig(self.__rigTemplate)
        
    def DetectRigsInScene(self):
        sceneModelList = FBModelList()
        FBGetSelectedModels (sceneModelList, None, False)
        FBGetSelectedModels (sceneModelList, None, True)
        
        for component in sceneModelList:

            if "weaponrigroot" in component.Name.lower():
                print "Found a rig node:", component.LongName
                self.__weaponRigs[str(component.LongName)] = WeaponRig(component.Parent)
                self.__weaponRigs[str(component.LongName)].SetName(str(component.LongName))
                self.__weaponRigs[str(component.LongName)].SetRigRootComponent(component)
                self.__weaponRigs[str(component.LongName)].PopulateRigFromRigNode(component, self.__rigTemplate)

'''
def WeaponSnapUtilUI():
    mainUI = 

sceneModelList = FBModelList()
FBGetSelectedModels (sceneModelList, None, False)
FBGetSelectedModels (sceneModelList, None, True)



dummyObj = None
weaponRigInst = None
templateXmlPath = r"X:\wildwest\dcc\motionbuilder2014\python\etc\weaponControlSettings.xml"
for component in sceneModelList:
    if 'dummy' in component.Name.lower():
        dummyObj = component
        
if dummyObj != None:
    weaponRigInst = WeaponRig( dummyObj )


    if weaponRigInst != None:
        rigTemplate = ControllerMapTemplate()
        rigTemplate.LoadControllerSettings(templateXmlPath)
        weaponRigInst.SetupRig(rigTemplate)
'''
rigMgr = WeaponRigManager()
rigMgr.DetectRigsInScene()
rigs = rigMgr.GetWeaponRigs()
for rig in rigs.keys():
    print rigs[rig].GetRigControllers()
    
    
