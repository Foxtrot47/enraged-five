"""
EST Trackering DOc

Purpose: Read Tracking file from perforce, then populuate the rangeList class with the info 

Author: Mark.Harrison-ball@rockstargames.com
Reviewer: HB


"""

from pyfbsdk import *
import RS
import os.path
from RS.Core.EST.rangeListDef import *
import RS.Core.EST.estLayer as Layer
import RS.Perforce as p4


from collections import namedtuple

import xlrd
reload(Layer)


# Might put this in the EST Paths 
lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

IGNORE_SHEETS = ['main tracking','stats','ranger','data-polish','data_facial','unapprovedmeta','data', 'tracker', 'cam pass']
VALID_COLUMNAMES = ['Cutscene Name','FBX NAME','RANGE IN','RANGE OUT']
COLUMN_INDEX = namedtuple('COLUMN_VALUES', ['Cutscene_Name','FBX_NAME','RANGE_IN','RANGE_OUT'])
#DEFAULT_HEADER_INDEXS = None

Debug = True

# go through the sheets
# record the index and the ranges.
# if index is not sequential then its a new take

"""
Search for

Argument1:      Excel File to open
Argument2:      FBXname (no extenstion)
"""
def _openWB( lxlsFilename, lfbxSceneName ):
    workbook = xlrd.open_workbook( lxlsFilename, on_demand=False )
    
    _rangeEditList = RangeEditList()
    
    _rangeEdit = None
    _shotDef = None
    
    for sheet_index in range(workbook.nsheets):
    
        sheet = workbook.sheet_by_index(sheet_index)
        
        if sheet.name.lower() not in IGNORE_SHEETS:

            DEFAULT_HEADER_INDEXS =_setValidColHeaders(sheet)
            bIsRange = False # Track if we found our ranges
            lastIndex = None
            cutsceneName = ""

            if not DEFAULT_HEADER_INDEXS: continue

            for row in range(1, sheet.nrows):
                #try:
                    #Get the name of the cutscene
                    currentCutsceneName = sheet.cell_value(row, DEFAULT_HEADER_INDEXS.Cutscene_Name)

                    if currentCutsceneName:
                        cutsceneName = currentCutsceneName

                    fbxName = sheet.cell_value(row, DEFAULT_HEADER_INDEXS.FBX_NAME).lower()


                    correctScene = cutsceneName.lower() in lfbxSceneName.lower()
                    correctFBXFile = (lfbxSceneName.lower() == fbxName.lower())

                    if correctFBXFile:

                        bIsRange = True 
                        if (row-1) != lastIndex:
                            if _shotDef != None:
                                _rangeEditList.Shots.append(_shotDef)    

                            _shotDef = ShotDef()
                            
                        _rangeEdit = RangeEdit()
                    
                        # Add our ranges
                        _rangeEdit.RangeIn  = int(sheet.cell_value(row, DEFAULT_HEADER_INDEXS.RANGE_IN))
                        _rangeEdit.RangeOut = int(sheet.cell_value(row, DEFAULT_HEADER_INDEXS.RANGE_OUT))+1 #
                        _shotDef.Ranges.append(_rangeEdit)
                        #print fbxName, _rangeEdit.RangeIn, _rangeEdit.RangeOut
                        lastIndex = row

                #except Exception,e:
                #    print "EST ", e
            if _shotDef != None:
                _rangeEditList.Shots.append(_shotDef)

            if bIsRange:
                break

    
    # DEBUG
    if Debug:
        for idx, shot in enumerate(_rangeEditList.Shots):
            for rng in shot.Ranges: 
                print ("Shot:{3}\tRange:{1} - {2}\tCamera:{0}".format(rng.CameraName, rng.RangeIn, rng.RangeOut, idx ) )   
    
    if len(_rangeEditList.Shots) == 0:
        _rangeEditList = None

    return _rangeEditList
    
    
# Function to get header indexs from our lookup, incase someone decides to move cell rows around
def _setValidColHeaders(sheet):
    #global DEFAULT_HEADER_INDEXS

    validCols = []
    for columnName in VALID_COLUMNAMES:

        if columnName in sheet.row_values(0):
            validCols.append(sheet.row_values(0).index(columnName))

    if validCols:
        validCols = COLUMN_INDEX(*validCols)

    return validCols

"""
    Public Entry point

    Arg1:   Excel File to read from
    Arg2:   Fbxscene name (no extension)
    Arg3:   glog
"""
def GetRangeEdits( lxlsFilename, lfbxSceneName, gLog = None ):

    lfbxSceneName = Layer.getMasterFile(lfbxSceneName)

    if gLog:
        gLog.LogMessage("MasterFBX file: {0}".format(lfbxSceneName))
        gLog.LogMessage("Syncing perforce file {0}".format(lxlsFilename))

    p4.Sync(lxlsFilename)
    if lfbxSceneName == '':
        return None
    else:
        if gLog:
            gLog.LogMessage("Reading Ranges from {0}".format(lxlsFilename))

        if not os.path.isfile(lxlsFilename):
            gLog.LogError("Error: Seems {0} is not mapped to your workspase".format(lxlsFilename))
            return None


        return _openWB( lxlsFilename, lfbxSceneName )
    
        

    
  


#lFilename = 'X:\\rdr3\\docs\\Production\\RDR3_CAMERA_TRACKING.xlsm'
#lFilename = 'X:\\gta5\\docs\\production\\GTAV_CAMERA_TRACKING.xlsm'

#lscenename = 'RBHS_INT_P1_take6'
#GetRangeEdits( lFilename, lscenename )
#_openWB(lFilename, lscenename)