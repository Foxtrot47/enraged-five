"""
Generate EST metadata from FBX file

Eventaully Read EST data from Database
EST data is created from Avid, EDL files are generated and used to create the EST.
THe EDL is timecode in/out Data. THis should be added automatically to the database (Watchstar/CUtscene Stats) and the cutscene generated from the databse.

*Issues
* No databse Connection?
** Maybe not a problem as EST is created externally to an XML file that is added to perforce


*Faster Process
-Create Shot Track
-Plot switcher to SHot Track ---
_Run Edits

"""
import os
import pyfbsdk
import xml.etree.cElementTree    as xml


import RS.Utils.Logging.Universal
import RS.Core.EST.estTrimEdit   as trimEdit
import RS.Core.EST.estTrackerDoc as estTracker
import RS.Core.EST.rangeListDef  as rangeListDef
import RS.Core.EST.estSaveMeta   as estSaveMeta
import RS.Core.EST.estStoryUtils as estStoryUtils
import RS.Core.EST.estPaths as estPaths
import RS.Config
import RS.Perforce as p4

import RS.Tools.UI.Commands as commands
from RS.Core.Camera import ClipTools

import RS.Utils.Bugstar
Bugstar = RS.Utils.Bugstar.Bugstar()

# Override our gLog
# gLog = RS.Utils.Logging.Universal.UniversalLog( "Motionbuilder_EST", False, True)

reload(estPaths)
#reload(estRangeEdits)
reload(RS.Core.EST.rangeListDef)
#reload(RS.Core.EST.estSaveMeta)
#reload(trimEdit)
reload(RS.Core.EST.estStoryUtils)
#reload(estWatchstar)
reload(estTracker)

DEBUG = True



'''
Updated now so will read the tracking doc paths from an xml
X:\[project]\etc\config\camera\estTrackingDoc.xml

'''
def getTrackingDocPath(filename):
    """
    For now will loop the xml and do a project look up before checking if any DLC look up
    """

    xml_path = estPaths.getTrackingConfigPath()
    xml_tree = xml.parse(xml_path)
    xml_element = xml_tree.getroot()


    trackingDocPath = None
    for project in xml_element.findall("./projects/project"):
        if project.attrib['name'].lower()== RS.Config.Project.Name.lower():
            for each in project.findall("./dlcMapping"):
                if each.attrib['name'].lower() in filename.lower():
                    trackingDocPath = each.attrib['cutscene']

            if trackingDocPath == None:
                trackingDocPath = project.attrib['cutscene']
    return trackingDocPath


def get_emails(team='camera'):
    """
    Gets the email list of who should be notified of EST changes

    returns: list
    list of all the emails from the camTeam xml file
    """
    #Using XML as that seems to be what we are all using
    xml_path = os.path.join(RS.Config.Tool.Path.TechArt, "etc", "config", "camera", "camTeam_config.xml")

    xml_tree = xml.parse(xml_path)
    xml_element = xml_tree.getroot()
    xml_tree = xml.ElementTree(xml_element)

    return [each.text for each in xml_element[1].find(team)]


def get_bug_report(report, fix="lighting", xml_file=""):
    return """
        Ranges have been changed, please fix {fix} ASAP:
        {xml}

        {report}
        """.format(fix=fix, xml=xml, report=report)


def CreateBug(owner, qaOwner, filename, revision, content, email_list=None):
    """
    Creates a bug about EST Range change

    :param owner: string
        name or email of the user who is generating this bug

    :param content: string
        description of the bug

    :param email_list: list
        list of the emails of everyone that should be cc'd on the bug

    :return: None
    """
    #Add support for the camera team
    team = {
        "Default Cutscene Lighting" :"lighting" ,
        "Default Animation Resource":"animation"
            }

    if not email_list:
        email_list = get_emails(team.get(owner, "animation"))

    if owner in email_list:
        email_list.remove(owner)

    #Create Bugstar Ticket
    if DEBUG:
        emailList=["mark.harrison-ball@rockstargames.com"]

    Bugstar.create_bug('[Range Change] {filename}'.format(filename=filename), content,
                       owner=owner, qaOwner=qaOwner, category="A", ccList=emailList,
                       tags=["range change", "Revision:{}".format(revision)])


def getEditRanges(tracking_doc_path, scene_name, gLog):
    """
    gets the edit ranges from the tracking doc, database or watchstar

    :param tracking_doc_path: string
        path to the tracking doc
    :param scene_name:
        name of the scene that is being checked
    :param gLog:
        the uLog instance were we are keeping track of changes

    :return: string ?
    """
    edit_ranges = None


    #CHOOSE ONE OF THE FOLLOWING: GetAvidEdlRanges, GetTrackingDocRanges, GetWatchstarRanges
    edit_ranges = estTracker.GetRangeEdits(tracking_doc_path, scene_name, gLog)

    '''Disabled Database ranges for now'''
    #edit_ranges = estRangeEdits.GetAvidEdlRanges(gLog)
    if edit_ranges:
        _logRanges(edit_ranges, "Found edit ranges")

    return edit_ranges


def generateClips(_exportRanges):
    """
    Generate Autoclips from Ranges

    This can Crash Motion builder if you manually delete the shot track ... Fucking Crap
    """
    estStoryUtils.delShotTracks()
    estStoryUtils.lStory.LockedShot = True
    estStoryUtils.createClips(_exportRanges, gLog)
    estStoryUtils.lStory.LockedShot = False

    estStoryUtils.moveClips()



def useHardRanges():
    """
    Set Hard ranges as no cameras in scene
    """

    _rangeEditList = rangeListDef.RangeEditList()
    _shotDef = rangeListDef.ShotDef()
    _shotDef.ShotIndex = 0

    _rangeEdit = rangeListDef.RangeEdit()
    lStartFrame = 0
    lEndHardFrame = int(estStoryUtils.lSystem.CurrentTake.LocalTimeSpan.GetStop().GetTimeString().replace('*', ''))

    _rangeEdit.RangeIn = lStartFrame
    _rangeEdit.RangeOut = lEndHardFrame
    _rangeEdit.CameraName = "rockStarCamera"
    _shotDef.Ranges.append(_rangeEdit)

    _rangeEditList.Shots.append(_shotDef)

    return _rangeEditList


def _logRanges(ranges, log_message="New export cutscene ranges..."):
    """
    Log our new export Ranges
    """

    gLog.LogMessage(log_message)

    for idx, shot in enumerate(ranges.Shots):
        for rng in shot.Ranges:
            log = "Shot:{3}\tRange:{1} - {2}\t\tCamera: {0}".format(rng.CameraName, rng.RangeIn, rng.RangeOut, idx)
            gLog.LogWarning(log)
            if DEBUG:
                print log


def genShotTrack(silent=False):
    ClipTools.CheckForRangeUpdate()
    ClipTools.LoadClips()


def genShotTrackOld(silent=False):

    """
    Generates the shot track

    Keyword Arguments:

        silent: Whether or not open the Ulogger output

    """

    # Get our scene Name
    fullSceneName = pyfbsdk.FBApplication().FBXFileName
    lSceneName = RS.Utils.Path.GetBaseNameNoExtension(fullSceneName)

    # init our logger
    global gLog
    gLog = RS.Utils.Logging.Universal.UniversalLog("cutscene/process/export_EST_{}".format(lSceneName), False, True)

    # SetTracking Doc location if used       
    trackingDocPath = getTrackingDocPath(fullSceneName)

    if not os.path.isfile(trackingDocPath):
        #Add Perforce Sync here
        p4.Sync(trackingDocPath, force=True)


    if lSceneName == "":
        gLog.LogError("Error: This is not a valid cutscene file, Aborting!!")
        gLog.Show(modal=False)
        return

    # define our Meta Utility
    metaUtils = estSaveMeta.ESTmeta(fullSceneName, gLog)

    # Make sure we are working on the base Anim Layer (BUG 2401598)
    estStoryUtils.setToBaseLayer()

    # Clean up dup switcher keys
    estStoryUtils.cleanUpSwitcherKeys()

    # 1. Get our camera ranges from the switcher which we want to export
    _switcherRanges = estStoryUtils.getSwitcherInfo()

    # 2. Get current Story Ranges from our scene if any
    _storyRanges = estStoryUtils.getStoryRanges()

    # 3. Get Database Ranges / Watchstar  / Tracker DOc
    # So for GTA we will use the old tracking doc, for BOb we use the database and eventually watchstar.
    _editRanges = getEditRanges(trackingDocPath, lSceneName, gLog)

    # 4. If we have Database ranges then use them
    if _editRanges:

        #Log Information
        edit_ranges_log = "Found Avid Edit Ranges for {0} shot(s)".format(len(_editRanges.Shots))
        _logRanges(_editRanges, edit_ranges_log)

        # If have no switcher Ranges then we can not make the edit.. ERROR
        _exportRanges = trimEdit.TrimEdit(_switcherRanges, _editRanges)
        generateClips(_exportRanges)

    # 5. If we have no Database/Watchstar but had existing ranges then use our current ranges. No Change
    # gLog.LogWarning("Found no valid Avid Ranges!")
    elif _storyRanges.hasRanges():
        gLog.LogMessage("Using Existing Story Ranges for export!") 
        _exportRanges = _storyRanges

    elif _switcherRanges.hasRanges():
        gLog.LogMessage("Using Existing Switcher Ranges for export!") 
        _exportRanges = _switcherRanges

        # Do we want to generate clips if there are none? Question
        generateClips(_exportRanges)

    else:
        gLog.LogWarning("Found no Camera Switcher Ranges")
        # 6. USe the Hard Ranges
        gLog.LogMessage("Using Hard Ranges for export!") 
        _exportRanges = useHardRanges()

    _logRanges(_exportRanges)

    gLog.LogMessage("Does a EST meta file exists: {}".format(metaUtils.fileExists()))

    if _exportRanges != _storyRanges:
        gLog.LogMessage("EST Changes: {}".format( _exportRanges.report()))

    #Generate bug if export ranges and story range do not match
    if RS.Config.User.Email in get_emails('camera') and _exportRanges != _storyRanges:
        #Add check to see if scene is approved for 1st or 2nd Pass
        #Bugs should only be added after an approval
        #This should tie in to Watchstar/Bugstar to check for cutscene status and if the previous bug has bene resolved

        #This dialog box is only a temporary solution
        create_bug = pyfbsdk.FBMessageBox("Create Bug", "EST track has been changed from the previous version,\n\
                Do you want to create a bug for it?", "Bug It! ", "Bug Off")

        if create_bug == 1:
            #Creating bugs takes a while, the user will probably have to wait 3-5 seconds while the bugs are created
            CreateBug("Default Anim Resource" , RS.Config.User.Email, lSceneName, _exportRanges.Revision,
                      get_bug_report(_exportRanges.report(), "DD/LOD/BB", ""))
            CreateBug("Default Cutscene Lighting"   , RS.Config.User.Email, lSceneName, _exportRanges.Revision,
                      get_bug_report(_exportRanges.report(), "lighting", ""))
            CreateBug("Geoffery Fermin"             , RS.Config.User.Email, lSceneName, _exportRanges.Revision,
                      get_bug_report(_exportRanges.report(), "blend-out", ""))

    if RS.Config.Project.Name != 'GTA5' and _exportRanges:
            # Write out our Meta file with our Export Ranges
            gLog.LogMessage("Export Ranges has been disabled for now.")
            #metaUtils.exportRanges(_exportRanges)

    elif RS.Config.Project.Name != 'GTA5' and not _exportRanges:
            gLog.LogError("Fatal Error!!, No Valid Export Ranges!!!!!")

    if not silent:
        gLog.Show(modal=False)

#genShotTrack()