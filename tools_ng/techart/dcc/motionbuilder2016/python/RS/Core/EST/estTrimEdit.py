from RS.Core.EST.rangeListDef import *



"""
    Return a updated Range object
"""
def returnRange( rangeIn, rangeOut):
    newRange = RangeEdit()
    newRange.RangeIn = rangeIn
    newRange.RangeOut = rangeOut
    return newRange
    

"""
    Compare 
"""
def _compareEachRange( editRange, _sceneRanges ):
    trimmedRanges = []
    
    # There is only one shotDef in the SceneRange
    for scnRange in _sceneRanges.Shots[0].Ranges:
        trimmedRange = None
        if scnRange.RangeIn >= editRange.RangeIn and scnRange.RangeOut <= editRange.RangeOut:
            #print ":{0} - {1}     =   {0} - {1}".format(scnRange.RangeIn, scnRange.RangeOut)  
            trimmedRange = returnRange(scnRange.RangeIn, scnRange.RangeOut)                
        elif scnRange.RangeOut > editRange.RangeIn and scnRange.RangeOut <= editRange.RangeOut:
            #print ":{0} - {1}     =   {2} - {1}".format(scnRange.RangeIn, scnRange.RangeOut, editRange.RangeIn ) 
            trimmedRange = returnRange(editRange.RangeIn, scnRange.RangeOut)
        elif scnRange.RangeIn >= editRange.RangeIn  and scnRange.RangeIn  < editRange.RangeOut:                                
            #print ":{0} - {1}     =   {0} - {2}".format(scnRange.RangeIn, scnRange.RangeOut, editRange.RangeOut ) 
            trimmedRange = returnRange(scnRange.RangeIn, editRange.RangeOut)
        elif scnRange.RangeIn < editRange.RangeIn and scnRange.RangeOut > editRange.RangeOut:
            #print ":{0} - {1}     =   {2} - {3}".format(scnRange.RangeIn, scnRange.RangeOut, editRange.RangeIn, editRange.RangeOut ) 
            trimmedRange = returnRange(editRange.RangeIn, editRange.RangeOut)
        else:
            trimmedRange = None 
            #print "remove:{0} - {1}     =   {2} - {3}".format(scnRange.RangeIn, scnRange.RangeOut, editRange.RangeIn, editRange.RangeOut )  
    
        if trimmedRange:
            # Add our camera name
            trimmedRange.CameraName = scnRange.CameraName
            trimmedRanges.append( trimmedRange )
                
    return trimmedRanges
             
            

def _trimEdit( _sceneRanges, _editRanges ):
    _exportRangeList = RangeEditList()    
    
    # go through each range edit while constrcting a new RangeEdit List
    for editShot in _editRanges.Shots:
        _exportShotDef = ShotDef()
        rangeList = []
        for editRange in editShot.Ranges:
            _exportRanges = _compareEachRange(editRange, _sceneRanges)
            if _exportRanges:
                rangeList.extend(_exportRanges)

        # Add valid Ranges for each shot Track  
        if rangeList:
            _exportShotDef.Ranges = rangeList
            _exportRangeList.Shots.append(_exportShotDef)
            
    return _exportRangeList
            
    


def TrimEdit( _exportRanges, _editRanges ):
    return _trimEdit( _exportRanges, _editRanges )
    
    
