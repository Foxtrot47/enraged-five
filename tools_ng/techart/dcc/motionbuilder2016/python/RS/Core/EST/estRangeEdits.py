"""
    Container Object to hold the Range Edits
    
    Author: Mark Harrison-Ball <mark.harrison-ball@rockstargames.com>
    
    This will generate a standard EST metaObject from any cameras in the switcher
    This EST metafile is then modified from any EDITS generated from avid via a database
    
    Notes: 
            Need to make this external and not reliant on MB
            
"""
from pyfbsdk import *
import RS

# import RS.Utils.CacheLib as cache
from RS.Core import DatabaseConnection
# import RS.Core.EST.estMetaUpdate as MetaUpdate
# reload(cache)

# reload(MetaUpdate)
# We need to reference the Class in a separate file so we can unpickle the object correctly
import RS.Core.EST.rangeListDef
from RS.Core.EST.rangeListDef import *

"""
 Input: Database Table
"""


class ProcessAvidRanges():
    def __init__(self, rows):
        """ FINDS RANGES FROM XML FILE AND CONVERTS THEM TO RANGELISTDEF FORMAT """
        # CREATE CLIP DICTIONARY FROM XML
        self.rows = rows
        self.ShotIndex = 0
        self.rangeList = []
        self.trackList = []
        self.editList = None
        
        self.main()
    
    def saveRangesToShotTrack(self):
        """ SAVES CURRENT RANGES INTO A SHOT TRACK """
        shotTrack = ShotDef()
        shotTrack.Ranges = self.rangeList
        shotTrack.ShotIndex = self.ShotIndex
        self.ShotIndex = self.ShotIndex + 1
        
        self.trackList.append(shotTrack)
        
    def saveTracksToEditList(self):
        """ SAVES CURRENT SHOT TRACK IN THE EDIT LIST OBJECT """
        self.editList = RangeEditList()
        self.editList.Shots = self.trackList
    
    def main(self):
        """ MAIN FUNCTION FOR RANGE AND TRACK CREATION """
        previousRangeType = None
        previousRangeOut = None
        
        fadeInStart = 0
        tripleException = False
        tripleCounter = 2
        
        maxRows = len(self.rows)
        # CREATE AN EDITRANGE FOR EVERY CLIP IN OUR XML DICTIONARY
        for i in range(len(self.rows)):
            nextType = "NONE"
            nextNextType = "NONE"
            
            # If we've just done a tripleException, we skip the next 2 clips,
            # then forget it ever happend (so clip and track logic contiunes normally)
            if (tripleException == True):
                if tripleCounter != 0:
                    tripleCounter = tripleCounter - 1
                    continue
                else:
                    tripleException == False
                    continue
            
            # SET OUR CURRENT RANGE TYPE AND CREATE EDITRANGE
            rangeType = self.rows[i][2]
            # If the rangetype is invalid (for export data), we skip that range
            if (rangeType == "LEADIN") or (rangeType == "LEADOUT") or (rangeType == "INGAME"):
                continue
            
            # If this isn't the last range, we check the next type (in case it's a blendout/leadout)
            if i+1 < maxRows:
                nextType = self.rows[i+1][2]
            # And the type after that (for the below TRIPLE EXCEPTION)
            if i+2 < maxRows:
                nextNextType = self.rows[i+2][2]
            
            # Set this range's start frame
            editRange = RangeEdit()
            editRange.RangeIn = int(self.rows[i][0])
            
            # Basic Filtering for Fadein / Fadeout Clips
            if (rangeType == "BLACK"):
                fadeInStart = editRange.RangeIn
                continue
            
            if (rangeType == "FADEIN"):
                # ================
                # TRIPLE EXCEPTION (in case fadein, range, and blendout are all one cam shot)
                # ================
                if (nextType == "RANGE") and (nextNextType == "FADEOUT"):
                    print 'Triple exception found!'
                    editRange.RangeOut = int(self.rows[i+2][1]) + 1
                    rangeType = "RANGE"
                    
                    self.rangeList.append(editRange)
                    previousRangeType = rangeType
                    previousRangeOut = editRange.RangeOut
                    
                    tripleException = True
                    continue
                else:
                    # REGULAR FADEIN BEHAVIOR
                    if fadeInStart == 0:
                        fadeInStart = editRange.RangeIn
                    continue
            
            # Skips a fadeout range (after the previous clip is updated to fadeout endframe)
            if (rangeType == "FADEOUT"):
                # MULTITRACK FADE CHECK - If we're going from a fadeout to a fadein or black range (in the same fbx),
                # we need to create a new shot track.  Shouldn't happen often, but just covering everything.
                if (nextType == "FADEIN" or nextType == "BLACK"):
                    self.saveRangesToShotTrack()
                    self.rangeList = []
                continue
            
            # Check for pre-existing fadein start range
            if fadeInStart != 0:
                editRange.RangeIn = fadeInStart
                fadeInStart = 0
            
            # FADEOUT ENDRANGE BEHAVIOUR - If next clip is a fadeout, we adjust the current range and skip the next one
            if nextType == "FADEOUT":
                editRange.RangeOut = int(self.rows[i+1][1]) + 1
            else:
                # REGULAR ENDRANGE BEHAVIOUR
                editRange.RangeOut = int(self.rows[i][1]) + 1
            
            # IF WE'RE CHANGING TO OR FROM A REGULAR RANGE TO A MULTISTART/LEADIN...
            newTrackTest = (((rangeType == 'MULTISTART') and previousRangeType == "RANGE")) or (
                ((previousRangeType == 'MULTISTART') and rangeType != previousRangeType)) or (
                editRange.RangeIn < previousRangeOut)
            if newTrackTest == True:
                # WE SAVE OUT THE CURRENT SHOT TRACK, AND RESET THE RANGELIST FOR A NEW TRACK
                self.saveRangesToShotTrack()
                self.rangeList = []
            # THEN ADD THIS EDITRANGE TO OUR CURRENT RANGELIST EITHER WAY, AND SET PREVIOUS RANGE TYPE TO THIS EDITRANGE
            self.rangeList.append(editRange)
            previousRangeType = rangeType
            previousRangeOut = editRange.RangeOut
        
        # ONCE ALL CLIPS ARE DONE, WE SAVE THE CURRENT TRACK
        self.saveRangesToShotTrack()
        self.saveTracksToEditList()

def __buildTrackingDocEdit__(dt):
    _rangeEditList = RangeEditList()
    _shotDef = ShotDef()
    shotIndex = 0
    
    for row in dt:
        
        if row.ShotIndex > shotIndex:
            _rangeEditList.Shots.append(_shotDef)  
            
            shotIndex = row.ShotIndex
            # define a new shotDEf
            _shotDef = ShotDef()
            _shotDef.ShotIndex = shotIndex
            
        
        _rangeEdit = RangeEdit()
        _rangeEdit.RangeIn = row.RangeIn
        _rangeEdit.RangeOut = (row.RangeOut + 1) # We keep the frame
        _shotDef.Ranges.append(_rangeEdit) 
        
        
    
    _rangeEditList.Shots.append(_shotDef)         
            

        
    #_rangeEditList.ShotTracks.append(_shotDef)     

    return _rangeEditList
    

# Future Fucntionaility
def __ReadEditsFromLocalCache__():    
    myObject = cacheLib.readFromCacheFile()
    return myObject

# Future Fucntionaility
def __WriteEditsToLocalCache__(objectDef):
    cacheLib.writeToCacheFile(objectDef)
    return

# Future Fucntionaility
def __RequireEditUpdate__(CurrRev):
    requireUpdate = True
    CachedEdit = __ReadEditsFromLocalCache__()
    if CachedEdit:
        if CurrRev == CachedEdit.Revision:
            requireUpdate = True
    return requireUpdate
    
def __UpdateMeta__():
    return
    


## Constants ##
DB_DRIVER       = '{SQL Server}'
DB_SERVER       = 'NYCW-MHB-SQL' # Temp Servername
DB_DATABASE     = 'gradingStats'
 

lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
# Set the users local Cache Path 
#cacheLib = cache.CacheLib("EstCache")

#Public
def GetTrackingDocRanges(gLog = None):
    if gLog:
        gLog.LogMessage("Reading Ranges from Database Server {0}".format(DB_SERVER))
    # Set our Database Connection
    DB = DatabaseConnection.DatabaseConnection('{SQL Server}','NYCW-MHB-SQL','gradingStats', gLog)
    ESTobj = None
    if not DB.IsConnected():
        gLog.LogWarning("Unable to conenct to Database, using existing camera Switcher Ranges!")
    else:
        if lFileName:
            rows = DB.ExecuteQuery("SELECT * FROM FBX WHERE FBXName='{0}'".format(lFileName))
            if rows:
                if gLog:
                    gLog.LogMessage("Reading ranges from Database")
                # If we have data then lets check the modified date
                fbxID = rows[0].FBXID # We will alwasy get a EDIT ID!!!           
                rangeData = DB.ReturnQuery("SELECT * FROM [ExcelRanges] WHERE FBXID={0}".format(fbxID))
      
                ESTobj = __buildTrackingDocEdit__(rangeData)

    return ESTobj


def GetAvidEdlRanges(gLog = None):
    if gLog:
        gLog.LogMessage("Reading Ranges from Database Server {0}".format(DB_SERVER))
    # Set our Database Connection
    DB.SetConnection('{SQL Server}','NYCW-MHB-SQL','gradingStats', gLog)
    ESTobj = None
    if not DB.IsConnected():
        gLog.LogWarning("Unable to conenct to Database, using existing camera Switcher Ranges!")
    else:
        if lFileName:
            rows = DB.ReturnQuery("SELECT RangeIn, RangeOut, RangeType FROM [AvidShots] WHERE SourceName ='{0}'".format(lFileName))
            if rows:
                print rows
                if gLog:
                    gLog.LogMessage("Reading ranges from Database")
                
                _ProcessObj_ = ProcessAvidRanges(rows)
                ESTobj = _ProcessObj_.editList

    return ESTobj

def GetWatchstarRanges(gLog = None):
    if gLog:
        gLog.LogMessage("Reading Ranges from Watchstar XML")
    _RangeDataObj_ = MakeClipsFromXml()
    clipDictionary = _RangeDataObj_.clipDictionary
    rows = []
    
    for clipName, dataObj in sorted(clipDictionary.items()):
        rangeType = str(dataObj.rangeType)
        rangeType = filter(lambda x: x!=' ', rangeType)
        RangeIn = int(dataObj.startFrame)
        RangeOut = int(dataObj.endFrame)
        
        rangeEntry = [RangeIn, RangeOut, rangeType]
        rows.append(rangeEntry)
    
    _ProcessObj_ = ProcessAvidRanges(rows)
    ESTobj = _ProcessObj_.editList
    
    return ESTobj