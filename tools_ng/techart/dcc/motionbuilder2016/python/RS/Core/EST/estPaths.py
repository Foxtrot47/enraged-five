"""
    EST Path Utils
"""
import RS, os


def getStrandName(filename):
    '''
    Get StrandName from the cutscene. The strand Name should be the child folder of teh '!!scenes' Directory
    
    Arguments:
        filename: Full FBX Filename 
    '''
    strandName = None
    tokens = filename.lower().replace('/','\\').split('\\')
    if '!!scenes' in tokens:
        i = tokens.index( '!!scenes' )
        strandName = tokens[i+1]
    return strandName
    


def getAssetsPath():
    '''
    Return our (Safe) Asset Meta Data Path we save the Meta file objects to
    :return:
    '''
    #lPath = "{0}/animation/cutscenes".format(RS.Config.Project.Path.Metadata).replace('\\','/')
    lPath = "{0}/anim/anim_scenes/cutscene".format(RS.Config.Project.Path.Export).replace('\\','/')
    return lPath
    


def returnShotMetaPath(filename):
    '''
    Return Asset Edit Meta File

    Arguments:
        filename: Full FBX Filename
    '''
    lFileName = RS.Utils.Path.GetBaseNameNoExtension(filename)
    lPath = "{0}/{1}/{2}.xml".format(getAssetsPath(), getStrandName(filename), lFileName)
    return lPath


def getTrackingConfigPath():
    '''
    Return tracking path config file

    :return:
    '''
    trackingConfigPath = os.path.join(RS.Config.Tool.Path.Root,"techart", "etc", "config", "camera", "estTrackingDoc.xml")
    return trackingConfigPath
    
