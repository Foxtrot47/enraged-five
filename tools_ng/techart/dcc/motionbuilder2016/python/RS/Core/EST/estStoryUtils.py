
"""
Functions to work with the stroy track and clips.
Requires using the RangeEdit class to collect Data

"""
from pyfbsdk import *
import RS
import RS.Globals as glo
from RS.Core.EST.rangeListDef import *

lStory = FBStory() 
lSystem = FBSystem()
lCameraSwitcher = FBCameraSwitcher()  
lFolder = lStory.RootEditFolder      
lTrackContainer = lStory.RootEditFolder.Tracks


def isValidTime(fbTime):
    """
    Check if time is negative
    :param fbTime:
    :return:
    """
    return fbTime >= 0

def setToBaseLayer():
    """
    Set the selected Layer to teh Base Animation Layer
    :return:
    """
    lSystem.CurrentTake.SetCurrentLayer(0)


def delShotTracks():
    """
    Go through the shot tracks and delete any existing tracks
    :return:
    """
    deleteList = []
    for iTrack in lTrackContainer:
        # Deselect Clips Before Delete - otherwise Mobu crashes
        for clip in iTrack.Clips:
            clip.Selected = False
        deleteList.append(iTrack)
    map( FBComponent.FBDelete, deleteList )


  

def getStoryRanges():
    """
    Get the current clips from the Story Tracks
    :return:
    """
    # instance of our RangeEdit class to hold rnage info
    _rangeEditList = RangeEditList()
    index = 0

    #Sort the tracks based on naming rather than their current order in the outliner
    trackDictionary = {iTrack.Name:iTrack for iTrack in lTrackContainer}
    trackList = trackDictionary.keys()
    trackList.sort()

    for iTrack in trackList:
        iTrack = trackDictionary[iTrack]
        if iTrack.Name.endswith('_EST'):            
            _shotDef = ShotDef()
            _shotDef.ShotIndex = index   
            index+=1
            for iClip in iTrack.Clips:            
                _rangeEdit = RangeEdit()
                # Some clips may not have cameras which means its fubar. MB bug
                if iClip.ShotCamera != None:
                    _rangeEdit.CameraName = iClip.ShotCamera.Name                    
                else:
                    _rangeEdit.CameraName = 'None'
                    
                _rangeEdit.RangeIn = int(iClip.MarkIn.GetTimeString().replace("*", ""))
                _rangeEdit.RangeOut = int(iClip.MarkOut.GetTimeString().replace("*", ""))
                _shotDef.Ranges.append(_rangeEdit)    
            _rangeEditList.Shots.append(_shotDef) 
    return _rangeEditList            

def moveClips():
    """
    Move our clips so bunched up
    :return:
    """
    for iTrack in lTrackContainer:
        if iTrack.Name.endswith('_EST'):
            for i in range(len(iTrack.Clips)):
                if i > 0:        
                    Number = int(iTrack.Clips[i - 1].Stop.GetTimeString().replace("*", ""))
                    iTrack.Clips[i].MoveTo(FBTime(0,0,0,Number), False)




def createClips(_editRanges, gLog = None):
    """
    Generate our Autoclips for use with lighting, this in the future will not be importanr.
    :param _editRanges:     RangeEdit Object
    :param gLog:
    :return:
    """

    lfilePath = RS.Utils.Path.GetBaseNameNoExtension( FBApplication().FBXFileName ) 
    _clipIndex = 0
    # go through each range edit while constrcting a new RangeEdit List
    for idx, shot in enumerate(_editRanges.Shots): 
        
        gTrack = FBStoryTrack(FBStoryTrackType.kFBStoryTrackShot, lFolder)
        gTrack.Name = "Shot_{0}_EST".format(idx+1); 

        for iClip, editRange in enumerate(shot.Ranges):  
            lCam = FBFindModelByLabelName(editRange.CameraName)
            # We can only create a clip if we have a camera
            if lCam:
                lClipStart = FBTime(0,0,0,editRange.RangeIn) 
                lClip = FBStoryClip(lCam, gTrack, lClipStart)
                lClip.Stop = FBTime(0,0,0,editRange.RangeOut)
                lClip.Name = "AutoClip{0}_{1}".format(_clipIndex, lfilePath )
                _clipIndex += 1
                
            else:
                erroMsg = "'No camera named: [{0}] found, Aborting Story Clip generation else MB will Crash!!!".format(editRange.CameraName)
                print erroMsg
                if (gLog):
                    gLog.LogError(erroMsg)
                
                break



def getSwitcherInfo():
    """
    # Read camera ranges from switcher
    :return:
    """
    # instance of our RangeEdit class to hold rnage info
    _rangeEditList = RangeEditList()
    _shotDef = ShotDef()
    _shotDef.ShotIndex = 0
    
    lEndHardFrame = int(lSystem.CurrentTake.LocalTimeSpan.GetStop().GetTimeString().replace('*',''))
    lSwitcherAnimNode = lCameraSwitcher.AnimationNodeInGet()
    if (lSwitcherAnimNode):
        lAnimNode = RS.Utils.Scene.FindAnimationNode( lSwitcherAnimNode, "Camera Index" ) 
        if lAnimNode: # There should be a animnode but this is moitonbuidelr and will fucking fuck you up!!
            for i in range(lAnimNode.KeyCount):
                lStartFrame = int(lAnimNode.FCurve.Keys[i].Time.GetTimeString().replace('*',''))
                # If we are starting at before 0 then set to 0
                if not isValidTime(lStartFrame):
                    lStartFrame = 0 
                lEndFrame = lEndHardFrame 
                if i < lAnimNode.KeyCount-1:                
                    lEndFrame = int(lAnimNode.FCurve.Keys[i+1].Time.GetTimeString().replace('*',''))
      
                # if the end frame is before 0 then ignore the camera
                if isValidTime(lEndFrame):
    
                    # collect range Data
                    lCam = glo.gCameras[int(lAnimNode.FCurve.Keys[i].Value + 6)]  # that number scares me!
                 
                    _rangeEdit = RangeEdit()
                                      
                    _rangeEdit.CameraName = lCam.LongName
                    _rangeEdit.RangeIn = lStartFrame
                    _rangeEdit.RangeOut = lEndFrame
                    _shotDef.Ranges.append(_rangeEdit)     
                           
    _rangeEditList.Shots.append(_shotDef) 
    return _rangeEditList


def cleanUpSwitcherKeys():
    """
    Mimics the Clean-up button in the switcher
    Remove duplicate keys in the camera switcher
    :return:
    """
    _fCurve = lCameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve
    _switcherKeys = _fCurve.Keys
    _deleteList = []
    # collect dup keys
    for i in range(len(_switcherKeys)-1):
        if _switcherKeys[i].Value == _switcherKeys[i+1].Value:
            _deleteList.append(_switcherKeys[i+1].Time)
            
    # Deleting keys by time range is much safer and does not require recursive process because of index change
    for i in _deleteList:
        _fCurve.KeyDeleteByTimeRange(i,i)    
    
    ''''''
    