"""
EST Watchstar

Purpose: Read XML file from perforce, then populudate the rangeList class witht he info 

Author: Blake Buck 
Reviewer: HB


"""
import os
import sys
import urllib
import RS.Utils.Path
from RS.Core.EST.rangeListDef import *
from xml.dom import minidom
from pyfbsdk import *


"""
Public Function
"""
class RangeClip():
    def __init__(self, name, rangeType, sortNum, fbxPath, startFrame, endFrame, transitionType, transitionFrames, notes):
        """ CREATES OBJECTS FOR EACH RANGE CLIP IN AN XML FILE """
        self.name = name
        self.rangeType = rangeType
        self.sortNum = sortNum
        self.fbxPath = fbxPath
        self.startFrame = startFrame
        self.endFrame = endFrame
        self.transitionType = transitionType
        self.transitionFrames = transitionFrames
        self.notes = notes

class MakeClipsFromXml():
    def __init__(self):
        """ MAKES RANGE CLIP OBJECTS FROM RANGE XML FILE. """
        self.seenError = False
        self.fbxName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        self.fbxName = self.fbxName.upper()
        
        self.cutsceneName = self.getCutsceneName()
        if self.cutsceneName == "":
            print "Stopping script."
        else:
            #LOCAL DEBUG OVERRIDE WITH MORE REALISTIC DATA
            self.cutsceneXmlDir = 'http://watchstar/data-exports/13/cutscene/'
            #self.cutsceneXmlDir = 'X:\\rdr3\\tools\\techart\\dcc\\motionbuilder2014\\python\\RS\\Core\\EST\\fakeWatchstarPath\\' 
            
            self.xmlPath = self.cutsceneXmlDir + self.cutsceneName + '.XML'
            self.xmlPage = urllib.urlopen(self.xmlPath) #ENABLE / DISABLE THIS LINE
            try:
                self.xmlFile = minidom.parse(self.xmlPage) #ALSO SWAP PATH/PAGE
            except IOError:
                self.seenError = True
                print 'ERROR: NO LOCAL XML FILE FOUND FOR ' + self.fbxName
                print 'Expected xml path: ' + self.xmlPath
            else:
                self.validateInput()
                
                self.clipDictionary = {}
                self.rangeClips = self.xmlFile.getElementsByTagName('range')
                
                for clipNum in range(len(self.rangeClips)):
                    fbxPath = self.findTagValues(clipNum, 'fbxScene')
                    fbxNameLen = len(self.fbxName)
                    trimmedPath = fbxPath[(-fbxNameLen-4):-4] #ALSO CHANGE 5/4
                    trimmedPath = trimmedPath.upper()
        
                    #VERIFY THAT THE CURRENT CLIP IS FOR THIS FBX
                    if trimmedPath != self.fbxName:
                        continue
                    
                    name = 'Clip ' + str(clipNum + 1)
                    rangeType = self.findTagValues(clipNum, 'rangeType')
                    sortNum = self.findTagValues(clipNum, 'rangeSort')
                    startFrame = self.findTagValues(clipNum, 'startFrame')
                    endFrame = self.findTagValues(clipNum, 'endFrame')
                    transitionType = self.findTagValues(clipNum, 'transitionType')
                    transitionFrames = self.findTagValues(clipNum, 'transitionFrames')
                    notes = self.findTagValues(clipNum, 'rangeNotes')
        
                    _clipObj_ = RangeClip(name, rangeType, sortNum, fbxPath, startFrame,
                                        endFrame, transitionType, transitionFrames, notes)
                    
                    self.clipDictionary[_clipObj_.name] = _clipObj_
                    
                if self.clipDictionary == {}:
                    self.seenError = True
                    print "ERROR: No clips matching this FBX found in the XML file."
        
    def getCutsceneName(self):
        # GET FBX PATH AND TRIM TO AFTER THE !!SCENES FOLDER
        lApp = FBApplication()
        fbxPath = lApp.FBXFileName
        
        # DETECT IF THIS FBX IS A VALID ALT FILE
        altFile = False
        fbxFilename = os.path.split(fbxPath)[-1]
        if "ALT" in fbxFilename:
            altFile == True
        
        # BUSTED FILENAME FOR DEBUG TESTING
        #fbxPath = "X:\rdr3\art\animation\cutscene\!!scenes\UTP2\UTP2_INDT\UTP2_INT_P1\UTP2_INT_P1_T09\UTP2_INt_P1_T09.fbx"
        
        if "!!scenes" not in fbxPath:
            print "ERROR: Scene located in an invalid directory."
            print "This script only supports FBXs found here: //rdr3/art/animation/cutscene/!!scenes/"
            self.seenError = True
            return ""
        else:
            scenesLoc = fbxPath.find("!!scenes") + 9
            fbxPath = fbxPath[scenesLoc:]
            
            fbxPath = fbxPath.upper()
            slashCount = fbxPath.count('\\') - 1
            finalString = ""
            
            # THEN LOOK THROUGH EACH FOLDER NAME TO SEE IF IT MATCHES OUR CRITERIA
            for eachSlash in range(slashCount):
                firstSlashLoc = fbxPath.index('\\') + 1
                fbxPath = fbxPath[firstSlashLoc:]
                secondSlashLoc = fbxPath.index('\\')
                possibleName = fbxPath[:secondSlashLoc]
                
                # ALT FOLDER DETECTION
                # If this Fbx is a valid ALT, and this current directory ends with ALT, we trim off the ALT and continue filtering.
                if altFile == True:
                    if possibleName[-3:] == "ALT":
                        possibleName == possibleName[:-4]
                
                # WE LOOK FOR NAMES ENDING WITH INT, EXT, MCS_#, and MCS_##
                if (possibleName[-3:] == "INT" or
                    possibleName[-3:] == "EXT" or
                    possibleName[-5:-2] == "MCS" or
                    possibleName[-6:-3] == "MCS" or
                    possibleName[-5:-2] == "RSC" or
                    possibleName[-6:-3] == "RSC"):
                        finalString = possibleName
                        break
            
            if finalString == "":
                # IF WE STILL FIND NOTHING WE RAISE AN ERROR AND STOP
                print 'ERROR: NO VALID CUTSCENE NAME FOUND'
                self.seenError = True
            
            # OR RETURN THE MATCHED CUTSCENE NAME
            print 'Cutscene Name: ' + finalString
            return finalString
    
    def validateInput(self):
        """ CHECKS THAT WE FIND A VALID XML FILE. """
        badPathCheck = self.xmlFile.getElementsByTagName('title')
        if badPathCheck != []:
            print '\nERROR: NO WEB XML FILE FOUND FOR ' + self.fbxName
            print 'Expected xml path: ' + self.xmlPath
            self.seenError = True
        else:
            print 'XML file loaded successfully.\n'
    
    def findTagValues(self, clipNum, tagName):
        """ FINDS TAG VALUES OF A GIVEN XML TAG. """
        xmlTag = self.rangeClips[clipNum].getElementsByTagName(tagName)
        for value in xmlTag:
            if value == None:
                return 'None'
            else:
                try:
                    tagValue = value.firstChild.nodeValue
                except AttributeError:
                    return 'None'
                return tagValue.upper()
