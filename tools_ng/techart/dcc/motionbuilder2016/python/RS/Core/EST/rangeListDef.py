"""
Basic Def class to hold Range Edits for the Cutscene
Needs to be seperate so we can pickle and unpickle the Object
"""


class RangeEditList(object):

    def __init__(self):
        self.Scene = None
        self.Revision = None
        self.Modified = None
        self.User = None
        self.Shots = list() # Store a list of ShotDefs   
        self.Differences = []

    def __eq__(self, other, *args):
        """
        overrides ==
        Checks if the content of the two objects are the same, returns false if the content is different for the
        ranges of each shot or if the two objects aren't from the same class. This also fills the difference
        variable with information on what is the difference between the two objects.

        :param other: object instance
            instance of any object, intended to be another RangeEditList instance

        :return: boolean
        """
        self.Differences = []

        match_flag = True
        added_removed = ['added to', 'removed from']
        earlier_later = ['earlier', 'later']

        #Check that incoming object is from the same class
        if not isinstance(other, RangeEditList):
            return False

        #Check if shot list size is the same
        if self.numberOfShots != other.numberOfShots:

            self.Differences.append("{} Shots have been {} Scene {}\n".format(
                self.numberOfShots - other.numberOfShots,
                added_removed[self.numberOfShots < other.numberOfShots],
                self.Scene))

            return False

        for self_shot_def, other_shot_def in zip(self.Shots, other.Shots):

            #Check that the number of ranges is the same
            if not self_shot_def.numberOfRanges == other_shot_def.numberOfRanges:

                self.Differences.append("{} Ranges have been {} Shot {} in Scene {}\n".format(
                    self_shot_def.numberOfRanges - other_shot_def.numberOfRanges,
                    added_removed[self_shot_def.numberOfRanges < other_shot_def.numberOfRanges],
                    self_shot_def.ShotIndex, self.Scene))

                match_flag = False
                continue

            range_number = 0
            for self_range, other_range in zip(self_shot_def.Ranges, other_shot_def.Ranges):

                #Check that the range values are the same
                if not [self_range.RangeIn, self_range.RangeOut] == [other_range.RangeIn, other_range.RangeOut]:

                    range_in_difference = self_range.RangeIn - other_range.RangeOut
                    range_out_difference = self_range.RangeOut - self_range.RangeOut

                    if range_in_difference:
                        self.Differences.append("Range starts {} frames {} in Range {} from Shot {} in Scene {}\n".format(
                            abs(range_in_difference), earlier_later[range_in_difference > 0], range_number, 
                            self_shot_def.ShotIndex, self.Scene)
                        )

                    if range_out_difference:
                        self.Differences.append("Range starts {} frames {} in Range {} from Shot {} in Scene {}\n".format(
                            abs(range_out_difference), earlier_later[range_out_difference > 0], range_number, 
                            self_shot_def.ShotIndex, self.Scene)
                        )

                    match_flag = False
                range_number += 1

        other.Differences = self.Differences
        return match_flag

    def __ne__(self, other, *args):
        """
        overrides !=
        Checks if the content of the two objects are not the same, returns True if the content is different for the
        ranges of each shot or if the two objects aren't from the same class

        :param other: object instance
            instance of any object, intended to be another RangeEditList instance

        :return: boolean
        """

        return self.__eq__(other) is False

    @property
    def numberOfShots(self):
        return len(self.Shots)

    def hasRanges(self):
        if len(self.Shots) == 0:
            return False
        else:
            return (len(self.Shots[0].Ranges) > 0)

    def report(self):
        """
        Generates a report based on the difference between two different files
        :return: string
        """
        return "".join(self.Differences)

class ShotDef(object):
    def __init__(self):
        self.ShotIndex = 0
        self.Ranges = list() # Store a list of RangeEdits

    @property
    def numberOfRanges(self):
        return len(self.Ranges)

class RangeEdit(object):
    CameraName = "",
    RangeIn = None
    RangeOut = None

    

#test = RangeEditList()
#print test.rangeList[0]