__author__ = 'mharrison-ball'

# Function to determine if FBX file is a layer file and abstract the master fbx file to lookup
import RS
import re
import RS.Core.Metadata as Metautils
import os

'''

Look up our meta file match to see if the layer file has a match

Arguments:

    Arg1: Fbx filename no extension
    
Returns:
    
    Returns the master fbxname or the same name again if not found

'''
def __returnMasterFile( fbxFileName ):
    layerMetaFile = os.path.join(RS.Config.Tool.Path.Root,"techart","etc","EST","ESTMergeLists.meta")
    if os.path.exists(layerMetaFile):
        ConfigLayersMetafile = RS.Core.Metadata.ParseMetaFile( layerMetaFile  )

        masterFile = fbxFileName
        for layerList in ConfigLayersMetafile.Root.Cutscenes:
            for i in layerList.LayerMerges:
                if i.lower() == fbxFileName.lower():
                    masterFile = layerList.CutsceneName
                    break
                
    return masterFile
    
'''

Determine if the file is a layer File and if so get the master File

Arguments:


    Arg1: fbxfilename no extension
    
Returns:
    
    Returns the master fbxname or the same name again if not found

'''
def getMasterFile( fbxFileName ):
    masterFile = fbxFileName
    result = re.match('(\w+.p[0-9]_t[0-9]+)(_l[0-9])', fbxFileName, re.IGNORECASE)

    if result != None:
        masterFile = result.groups(0)[0]
    else:
        masterFile = __returnMasterFile( fbxFileName )

    return masterFile
    
    


                


