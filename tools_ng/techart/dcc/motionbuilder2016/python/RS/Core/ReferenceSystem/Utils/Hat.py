import os
import pyfbsdk as mobu
from RS.Utils.Scene import Component
from RS.Utils import Scene


class HatExportBindPoseState(object):
    LOCK_PROPERTY = 'Export Bind Pose'

    def __init__(self,
                 inputNode):
        self.name = inputNode.Name

        self.lock = inputNode.PropertyList.Find(self.LOCK_PROPERTY).Data

    def transfert(self,
                  inputNamespace):
        currentNode = Scene.FindModelByName('{0}:{1}'.format(inputNamespace, self.name),
                                            includeNamespace=True)

        if currentNode is None:
            return

        lockProperty = currentNode.PropertyList.Find(self.LOCK_PROPERTY)
        if lockProperty is None:
            return

        lockProperty.SetLocked(False)
        lockProperty.Data = self.lock


class HatUtils(object):
    LOCK_PROPERTY = 'Export Bind Pose'

    EXTENSION_ARRAY = ('PH_Hat_loc',
                       'CP_brimBack',
                       'CP_brimFront',
                       'CP_brimLeft',
                       'CP_brimRight')

    def __init__(self,
                 inputNamespace):
        self.Namespace = inputNamespace

        self.hatComponents = []

    def cleanSourceCharacter(self):
        self.hatComponents = []

        hatLoc = Scene.FindModelByName('{0}:Hat_Loc'.format(self.Namespace),
                                       includeNamespace=True)

        rigComponents = []
        if hatLoc is not None:
            Scene.GetChildren(hatLoc,
                              rigComponents,
                              "",
                              True)

        self.hatComponents = [HatExportBindPoseState(node)
                              for node in rigComponents
                              if node.PropertyList.Find(self.LOCK_PROPERTY) is not None]

        Component.Delete(rigComponents)

        #Delete hat manipulator rig components except the animated markers
        preservedHatComponents = ('Zro_Hat_loc',
                                  'Zro_brimBack',
                                  'Zro_brimFront',
                                  'Zro_brimLeft',
                                  'Zro_brimRight',
                                  'RH_Hat_loc',
                                  'RZ_Hat_loc')

        hatNodes = []
        for node in preservedHatComponents:
            hatNodeName = '{0}:{1}'.format(self.Namespace,
                                           node)

            strapComponentArray = mobu.FBComponentList()
            mobu.FBFindObjectsByName(hatNodeName,
                                     strapComponentArray,
                                     True,
                                     True)

            outputList = list(strapComponentArray)

            if not outputList:
                continue

            hatNodes.append(outputList[0])

        Component.Delete(hatNodes)

    def transferExportBindposeValue(self):
        for node in self.hatComponents:
            node.transfert(self.Namespace)

        self.hatComponents = []