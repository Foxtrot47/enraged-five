import os

import pyfbsdk as mobu

from RS import Config, Globals
from RS.Core import ReferenceSystem
from RS.Utils import Namespace
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex


class MocapReferencePose(MocapComplex):
    """
    Represents a mocap reference pose reference in a scene
    """
    studio = ()
    PropertyTypeName = "MocapReferencePose"
    FormattedTypeName = "Mocap Reference Pose"
    TypePathList = [os.path.join(Config.Project.Path.Root, "art", "animation", "resources", "mocap", "REF_POSES")]
    FileType = "fbx"

    def _create(self, filestate=None, importOptions=None):
        """
        This function is run when a reference pose is created

        Args:
            importOptions (FaceAnimDataImportOptions, optional): face import options.
        """

        tempPath = self.StripSourceFileNamespace()
        # run the gs create
        if os.path.exists(tempPath):
            # merge options
            mergeOptions = ReferenceSystem.CreateBasicMergeOptions(tempPath)
            mergeOptions.NamespaceList = self.Namespace

            # merge
            Globals.Application.FileMerge(tempPath, False, mergeOptions)

            # add ref pose custom property to group

            refPoseMainGroup = None
            namespace = Globals.Scene.NamespaceGet(self.Namespace)
            for refPoseComponent in Namespace.GetContents(namespace):
                if isinstance(refPoseComponent, mobu.FBGroup):
                    refPoseMainGroup = refPoseComponent

                refPoseProperty = refPoseComponent.PropertyCreate('Ref Pose Component', mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)
                if refPoseProperty is None:
                    print "Unable to create refPoseProperty for {}".format(refPoseComponent)
                else:
                    refPoseProperty.Data = None

            if refPoseMainGroup is not None:
                mainGroupProperty = refPoseMainGroup.PropertyCreate("Ref Pose Group Plot", mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)
                mainGroupProperty.Data = None

            # update p4 version
            self.UpdateReferenceP4Version()

            # delete temp path
            os.remove(tempPath)
