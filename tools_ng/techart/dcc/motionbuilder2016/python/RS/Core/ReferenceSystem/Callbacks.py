import traceback
from RS import Globals
from RS.Core.ReferenceSystem import const


class ReferenceCallback(object):
    """
    Signal to emit so other tools can know when specific reference functions have finished running
    """
    __cache__ = {}
    __externalEnabled = True

    def __new__(cls, referenceOperation):
        """
        If caches the callback so the same instance of the callback is returned

        Arguments:
            referenceOperation (string): reference operation
        """
        instance = cls.__cache__.get(referenceOperation, None)
        if instance is None:
            instance = object.__new__(cls)
        return instance

    def __init__(self, referenceOperation):
        """
        Constructor

        Arguments:
            referenceOperation (string): reference operation
        """

        instance = self.__class__.__cache__.get(referenceOperation, None)
        if instance is not None:
            return

        self.__class__.__cache__[referenceOperation] = self
        self._enabled = True
        self._functions = []
        self._referenceOperation = referenceOperation

    def emit(self):
        """
        emits the signal to run the function
        """
        if not self._enabled or not self.__externalEnabled:
            return

        for func in self._functions:
            try:
                func()
            except Exception as ex:
                tbMsg = traceback.format_exc([ex.__class__, ex.message, ex])
                msg = "Exception from the \"{}\" ReferenceSystem callback:\n{}".format(self._referenceOperation, tbMsg)
                Globals.Ulog.LogWarning(msg)
    
    def connect(self, func):
        """
        add function to list
        """
        # skip dulicates if already in list
        if func in self._functions:
            return
        self._functions.append(func)

    def disconnect(self, func):
        """
        remove function to list
        """
        # check function is in list
        if func in self._functions:
            self._functions.remove(func)

    def enable(self, value):
        """
        allow the function to run for that one operation
        """
        self._enabled = value

    @classmethod
    def enableAll(cls, value):
        """
        class level - allow the functions to run for all operations
        """
        cls.__externalEnabled = value


class ReferenceCallbacks(object):
    """
    Callbacks to run
    """
    CREATE = ReferenceCallback(const.ReferenceOperations.CREATE)
    UPDATE = ReferenceCallback(const.ReferenceOperations.UPDATE)
    DELETE = ReferenceCallback(const.ReferenceOperations.DELETE)
