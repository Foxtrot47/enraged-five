'''
Usage:
    This contains logic to help interaction with PROP references
Author:
    Ross George
    Bianca Rudareanu
'''
import os

from RS.Core.ReferenceSystem import Decorators
from RS.Core.ReferenceSystem.Types.Complex import Complex


class Prop(Complex) :
    '''
    Represents a prop reference in a scene
    ''' 
    PropertyTypeName    = 'Props'
    FormattedTypeName   = 'Prop'
    TypePathList        = [os.path.join('art', 'animation', 'resources', 'props'),
                           os.path.join('art', 'dev','animation', 'resources', 'props'),
                           ]
    FileType            = 'fbx'

    def __init__(self, modelNull, log):
        '''
        Call base class constructor
        '''
        super(Prop, self).__init__(modelNull, log)

    @Decorators.Logger("Prop - Update")
    def _update(self, force=False, filestate=None):
        '''
        This function gets run only when a prop is created. Calls the base class and then
        fixes the groups.
        '''
        self._log.LogMessage("Strip Prop before update")
        self._strip()

        super(Prop, self)._update(filestate=filestate)
