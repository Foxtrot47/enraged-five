import os
import pyfbsdk as mobu

from RS import Perforce
from RS.Utils import Scene
from RS.Core.ReferenceSystem import Exceptions
from RS.Core.ReferenceSystem.Types.Complex import Complex
from RS.Core.ReferenceSystem.Decorators import Logger


class RayFire(Complex):
    '''
    Represents a rayfire reference in a scene
    '''
    PropertyTypeName    = 'RayFire'
    FormattedTypeName   = 'RayFire'
    TypePathList        = [os.path.join('art', 'animation', 'resources', 'rayfire'),
                           os.path.join('art', 'dev','animation', 'resources', 'rayfire'),
                           ]
    FileType            = 'fbx'

    @Logger("Rayfire - Create")
    def _create(self, filestate=None, clipStartFrame=None, importOptions=None):
        '''
        This function gets run only when a statedAnimation is created. Calls the base create class and then
        additional methods we wish to run specifically when created a statedAnimation reference.
        '''
        self._log.LogMessage("Creating rayfire reference.")
        super(RayFire, self)._create(filestate=filestate)

        if '_animation.fbx' in self.Path.lower():
            raise Exceptions.IncorrectRayfireFile()

        self.createStoryTrack(clipStartFrame)

    @Logger("Rayfire - Update")
    def _update(self, force=False, filestate=None, clipStartFrame=None):
        '''
        This function gets run only when a rayfire is created. Calls the base create class and then
        additional methods we wish to run specifically when created a rayfire reference.
        '''
        self._log.LogMessage("Updating rayfire reference.")

        # get rayfire clip from story, get its start frame and save in a variable
        trackClipDict = self.rayfireGetTrackClipDict()

        startFrame = None
        if trackClipDict is not None:
            if len(trackClipDict) > 0 and trackClipDict[trackClipDict.keys()[0]] is not None:
                startFrame = self.getRayfireClipStartFrame(trackClipDict[trackClipDict.keys()[0]])
            # remove old track and clip to bring in fresh again
            self.removeOldClipAndTrack(trackClipDict)

        # get reference path and save in a variable
        referencePath = self.Path

        # update
        super(RayFire, self)._update(force=force, filestate=filestate)

        # add the story track and clip back in again
        self.createStoryTrack(startFrame)

    def rayfireGetTrackClipDict(self):
        """
        return:
            dict[FBStoryTrack] = FBStroyClip
        """
        trackClipDict = {}
        # get anim file path
        baseName = os.path.basename(self.Path)
        baseNameSplit = baseName.split(".")
        dirName = os.path.dirname(self.Path)

        animFilePath = os.path.join('{0}'.format(dirName),
                                    '{0}_animation.{1}'.format(baseNameSplit[0], baseNameSplit[1]))

        # get a list of story tracks
        storyTrackList = list(mobu.FBStory().RootFolder.Tracks)
        for storyFolder in mobu.FBStory().RootFolder.Childs:
            for storyComponent in storyFolder.Components:
                if isinstance(storyComponent, mobu.FBStoryTrack):
                    storyTrackList.append(storyComponent)

        # find the rayfire clip
        for storyTrack in storyTrackList:
            for clip in storyTrack.Clips:
                animPathProperty = clip.ClipAnimationPath
                if animPathProperty == animFilePath:
                    trackClipDict[storyTrack] = clip
                    return trackClipDict
            if storyTrack.Name == self.Namespace:
                trackClipDict[storyTrack] = None

        return trackClipDict

    def getRayfireClipStartFrame(self, clip):
        """
        arg: FBStoryClip()
        returns: int
        """
        return clip.Start.GetFrame()

    def removeOldClipAndTrack(self, trackClipDict):
        """
        arg: FBStoryClip
        delete the clip and track from the current rayfire
        """
        for key, value in trackClipDict.iteritems():
            # delete clip first
            if value is not None:
                value.FBDelete()
            # delete track
            key.FBDelete()

    def setRayfireClipStartFrame(self, clip, frame):
        """
        args:
            FBStoryClip()
            int

        moves the clip to the given start frame
        """
        clip.MoveTo(mobu.FBTime(0 ,0 ,0 , frame), True)

    def createStoryTrack(self, clipStartFrame=None):
        '''
        This method sets up the story track for the scene, adding the rayfire nulls for the track
        content.
        '''
        self._log.LogMessage("Creating rayfire story track.")
        # find SA dummy01
        dummyNode = Scene.FindModelByName('{0}:Dummy01'.format(self.Namespace), True)
        if dummyNode is None:
            raise Exceptions.RayfireMissingDummy()

        # get children
        statedAnimHierarchyList = []
        Scene.GetChildren(dummyNode, statedAnimHierarchyList, "", False)

        # create story folder, if it doesnt exist
        rayfireFolder = None
        for storyFolder in mobu.FBStory().RootFolder.Childs:
            if storyFolder.Name == "Rayfire":
                rayfireFolder = storyFolder
            break

        if rayfireFolder is None:
            rayfireFolder = mobu.FBStoryFolder()
            rayfireFolder.Label = "Rayfire"

        # create track
        track = mobu.FBStoryTrack(mobu.FBStoryTrackType.kFBStoryTrackAnimation, rayfireFolder)
        track.Name = self.Namespace

        # add content to track
        trackContentProperty = track.PropertyList.Find("Track Content")
        for item in statedAnimHierarchyList:
            trackContentProperty.ConnectSrc(item)

        # add animation clip
        self.addAnimationClip(track, clipStartFrame=clipStartFrame)

    def addAnimationClip(self, track, clipStartFrame=None):
        '''
        This method imports in the animation clip associated with the rayfire just created.
        '''
        self._log.LogMessage("Importing rayfire animation clip to story track.")
        baseName = os.path.basename(self.Path)
        baseNameSplit = baseName.split(".")
        dirName = os.path.dirname(self.Path)

        animFilePath = os.path.join('{0}'.format(dirName),
                                    '{0}_animation.{1}'.format(baseNameSplit[0], baseNameSplit[1]))

        Perforce.Sync(animFilePath)

        if not os.path.exists(animFilePath):
            raise Exceptions.RayFireAnimFileNotFound()

        # add the anim clip
        clip = mobu.FBStoryClip(animFilePath, track, mobu.FBTime(0, 0, 0 ,0))

        # shift the clip start time if required
        if clipStartFrame is not None:
            clip.MoveTo(mobu.FBTime(0, 0, 0, clipStartFrame), True)
