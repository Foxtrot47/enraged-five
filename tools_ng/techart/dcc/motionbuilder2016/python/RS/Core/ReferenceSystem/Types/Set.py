'''
Usage:
    This contains logic to help interaction with SET references
Author:
    Ross George
    Bianca Rudareanu
'''
import os
import re

from pyfbsdk import *

import RS.Globals as Globals
import RS.Core.ReferenceSystem as Core
import RS.Utils.Namespace
from RS.Core.ReferenceSystem.Types.Base import Base
from RS.Utils.Scene import Component
from RS.Core.ReferenceSystem.Types.Complex import Complex
from RS.Core.ReferenceSystem.Decorators import Logger
import RS.Config as Config


class Set(Complex):
    '''
    Represents a set reference in a scene
    '''

    PropertyTypeName    = 'Sets'
    FormattedTypeName   = 'Set'
    TypePathList        = [os.path.join('art', 'animation', 'resources', 'Sets'),
                           os.path.join('art', 'dev','animation', 'resources', 'Sets'),
                           ]
    FileType            = 'fbx'

    def _update(self, force=False, filestate=None):
        '''
        Updates the set reference with latest data. Force does nothing here currently.
        '''
        self._strip()

        #Setup load options
        mergeOptions = Core.CreateBasicMergeOptions( self.Path )

        mergeOptions.NamespaceList = self.Namespace

        RS.Globals.Application.FileMerge( self.Path, False, mergeOptions )

        #Update Reference P4 Version
        filestate = filestate or RS.Perforce.GetFileState( self.Path )
        self._p4VersionProperty.Data = str( filestate.HaveRevision )

    @Logger("Rayfire - Reset")
    def ResetEnvironment( self ):
        '''
        Fixes Rotation/Scale issue that some sets
        '''
        self._log.LogMessage("Resetting the environment Roation and Scale.")

        setRoot = FBFindModelByLabelName(self.Namespace + ":SetRoot")

        setRoot.PropertyList.Find("Enable Rotation DOF").Data = False
        setRoot.Rotation.SetAnimated(True)
        setRoot.Scaling.SetAnimated(True)

        grabProp = setRoot.PropertyList.Find("Asset_Setup")

        if grabProp != None:
            vector = FBVector3d()
            setRoot.GetVector(vector, FBModelTransformationType.kModelRotation, True)

            if grabProp.Data == "RAG obj":
                setRoot.SetVector(FBVector3d(0, vector[1], 0), FBModelTransformationType.kModelRotation, True)
                setRoot.Scaling = FBVector3d(1, 1, 1)
            else:
                setRoot.SetVector(FBVector3d(-90, vector[1], 0),  FBModelTransformationType.kModelRotation, True)
                setRoot.Scaling = FBVector3d(100, 100, 100)

        numberOfSets = []
        for component in Globals.gComponents:
            if component.Name == 'SetRoot' and component.ClassName() == 'FBModelNull':
                numberOfSets.append(component)

        if len(numberOfSets) == 2:
            FBMessageBox('There are 2 Environments in this scene.\nYou may find you need to re-align the INT and EXT back together again.\n\nSee Wiki Page:\nhttps://devstar.rockstargames.com/wiki/index.php/Cutscene_Environment_Int/Ext_Matching','Ok')


    def _proxySetup ( self ):
        '''
        Turns the current reference into a proxy
        '''

    def _strip( self ):
        '''
        Preforms a full strip of the reference.
        This will likely be overloaded by each of the inherited classes.
        '''

        #Delete visual models
        for visualModel in list(self.GetVisualModelList()) :
            visualModel.FBDelete()

    def GetVisualModelList( self ):
        '''
        Returns a list of all the geometry elements that are currently being used as skin for the vehicle
        '''
        models = RS.Utils.Scene.Component.GetComponents( Namespace = self.Namespace,
                                          TypeList = [FBModel],
                                          DerivedTypes = False,
                                          ExcludeRegex = '(?i)text|rect|contact|ctrl|frm' )

        visualModelList = list()
        for model in models:
            if type( model.Geometry ) == FBMesh:
                visualModelList.append(model)

        return visualModelList