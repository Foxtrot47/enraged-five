import unittest

from RS import Globals
from RS.Utils import ContextManagers
from RS.Core.ReferenceSystem import Manager


# TODO: Add Logging
class Base(unittest.TestCase):

    Manager = Manager.Manager()
    Manager.RaiseErrors = True
    Manager.Silent = True

    def setUp(self):
        """Overrides inherited method

        Clears the scene before running the test
        """
        Globals.Application.FileNew()

    def tearDown(self):
        """Overrides inherited method

        Clears the scene after running the test
        """
        pass

    def test_Scene(self):
        """Scene to assert"""
        raise Exception("Re-implement")

    def test_Referencing(self):
        """Reference to assert"""
        raise Exception("Re-implement")

    def assertCreate(self, path):
        """test_s the creation of a reference

        Args:
            path (string): path to the reference

        Returns:
            RS.Core.ReferenceSystem.Types.Base: Created asset
        """
        with ContextManagers.SuspendMessages():
            return self.Manager.CreateReferences(path)[0]

    def assertMinorUpdate(self, reference):
        """test_s the update of a reference

        Args:
            reference (RS.Core.ReferenceSystem.Types.Base): referenece to rename
        """
        with ContextManagers.SuspendMessages():
            self.Manager.UpdateReferences(reference)
            self.assertTempExists()

    def assertMajorUpdate(self, reference):
        """test_s the update of a reference

        Args:
            reference (RS.Core.ReferenceSystem.Types.Base): referenece to rename
        """
        with ContextManagers.SuspendMessages():
            self.Manager.UpdateReferences(reference, force=True)
            self.assertTempExists()

    def assertRename(self, reference, newName):
        """test_s the renaming of a reference

        Args:
            reference (RS.Core.ReferenceSystem.Types.Base): reference to rename
            newName (str): new reference name
        """
        with ContextManagers.SuspendMessages():
            oldName = reference.Namespace
            self.Manager.ChangeNamespace(reference, newName)
            self.assertTempExists()
            self.assertTrue(Globals.Scene.NamespaceExist(newName), msg="Namespace was not renamed to the new namespace")
            self.assertFalse(Globals.Scene.NamespaceExist(oldName), msg="The old namespace still exists in the scene")

    def assertSwap(self, reference, path):
        """test_s the swapping of a reference

        Args:
            reference (RS.Core.ReferenceSystem.Types.Base): Reference to rename
            path (str): path to the reference
        """
        with ContextManagers.SuspendMessages():
            self.Manager.SwapReference(reference, path)
            self.assertTempExists()

    def assertDelete(self, reference):
        """test_s the deletion of a reference

        Args:
            reference (RS.Core.ReferenceSystem.Types.Base): reference to delete
        """
        with ContextManagers.SuspendMessages():
            name = reference.Namespace
            self.Manager.DeleteReferences(reference)
            self.assertFalse(Globals.Scene.NamespaceExist(name), msg="namespace was not deleted")

    def assertTempExists(self):
        """Checks if the temp namespace exists"""
        for namespace in Globals.Namespaces:
            self.assertFalse("__temp__" in namespace.Name, msg="__temp__ namespace found in the scene")
