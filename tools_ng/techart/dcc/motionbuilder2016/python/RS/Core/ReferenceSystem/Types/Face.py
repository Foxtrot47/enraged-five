'''
Usage:
    This contains logic to help interaction with FACE references
Author:
    Ross George
    Bianca Rudareanu
'''
import os
import re

import pyfbsdk as mobu

import RS.Core.ReferenceSystem as Core
import RS.Config as Config
import RS.Perforce as Perforce
import RS.Core.Face.AnimImporter
import RS.Core.Face.FixFaceScale

from RS.Core.ReferenceSystem import Exceptions
from RS.Core.ReferenceSystem.Types.Complex import Complex
from RS.Core.ReferenceSystem.Decorators import Logger


class Face(Complex):
    '''
    Represents a face reference in a scene
    '''

    PropertyTypeName = '3Lateral'
    FormattedTypeName = 'Face'
    TypePathList        = [os.path.join('art', 'animation', 'cutscene', 'facial'),
                           os.path.join('art', 'dev','animation', 'cutscene', 'facial'),
                            '\\face\\',
                           ]
    FileType = 'fbx'

    def __init__(self, modelNull, log):
        '''
        Initialises the face reference with the passed model null. Will call the base initializor as well.
        '''

        super(Face, self).__init__(modelNull, log)

        # Specific null properties for face
        self._targetNamespace = modelNull.PropertyList.Find(Core.STR_TARGET_NAMESPACE)
        self._takeName = modelNull.PropertyList.Find(Core.STR_TAKE_NAME)
        self._importOption = modelNull.PropertyList.Find(Core.STR_IMPORT_OPTION)
        self._createMissingTakes = modelNull.PropertyList.Find(Core.STR_CREATE_MISSING)

    @property
    def ImportOption(self):
        '''
        Gets the import option for the reference.
        '''
        return self._importOption.Data

    @property
    def TakeName(self):
        '''
        Gets the take name for the reference.
        '''
        return self._takeName.Data

    @property
    def CreateMissingTakes(self):
        '''
        Value that indicates if missing takes should be created.
        '''
        return self._createMissingTakes.Data

    @property
    def TargetNamespace(self):
        '''
        Value that indicates if missing takes should be created.
        '''
        if not self._targetNamespace:
            self._targetNamespace = self._modelNull.PropertyCreate(
                Core.STR_TARGET_NAMESPACE, mobu.FBPropertyType.kFBPT_charptr, 'String', False, True, None)
            self._targetNamespace.Data = ""
        return self._targetNamespace.Data

    def SetTargetNamespace(self, newTargetNamespace):
        """
        Arg:
        newTargetNamespace - String

        Set the Target_Namespace property's data
        """
        self._targetNamespace.Data = newTargetNamespace

    @Logger("Face - Create")
    def _create(self, filestate=None, importOptions=None):
        '''
        Creates the 3lateral face asset
        '''
        from RS.Core.Face import AnimImporterDialog

        # Update Reference P4 Version of character
        if os.path.exists(self.Path):
            filestate = filestate or Perforce.GetFileState(self.Path)
            if filestate == []:
                self._p4VersionProperty.Data = '0'
            else:
                self._p4VersionProperty.Data = str(filestate.HaveRevision)
        # Specific null properties for face
        self._targetNamespace = self._modelNull.PropertyCreate(
            Core.STR_TARGET_NAMESPACE, mobu.FBPropertyType.kFBPT_charptr, 'String', False, True, None)
        self._takeName = self._modelNull.PropertyCreate(
            Core.STR_TAKE_NAME, mobu.FBPropertyType.kFBPT_charptr, 'String', False, True, None)
        self._importOption = self._modelNull.PropertyCreate(
            Core.STR_IMPORT_OPTION, mobu.FBPropertyType.kFBPT_charptr, 'String', False, True, None)
        self._createMissingTakes = self._modelNull.PropertyCreate(
            Core.STR_CREATE_MISSING, mobu.FBPropertyType.kFBPT_bool, 'Bool', True, True, None)

        if not AnimImporterDialog.show(self.Path):
            raise Exception("Cannot open Anim Importer Dialog")

        if AnimImporterDialog.animIporterDialogClosed:
            raise Exceptions.NotAnError("Face Create Aborted")

        self._log.LogMessage("Creating face reference.")
        importOptions = AnimImporterDialog.getImportOptions()

        # Set the import option
        self._importOption.Data = importOptions.importOnto

        # Set the take name
        self._takeName.Data = importOptions.currentTakeName

        # Set the create missing takes property
        self._createMissingTakes.Data = importOptions.createMissingTakes

        # Set the taget namespace
        self._targetNamespace.Data = importOptions.targetNamespace

        # Fix Face Scale on all Expression Solver Devices. -- Thanh Phan
        # 10/2/2015
        self._log.LogMessage("Fixing face scale")
        RS.Core.Face.FixFaceScale.fixAllScale()

    @Logger("Face - Create")
    def _update(self, quiet=True, filestate=None):
        '''
        Updates the face reference with latest data. Force does nothing here currently.
        '''

        # TO DO:: perforce check to see if the face animation fbx file has new
        # animation
        if not os.path.exists(self.Path):
            self._log.LogError("Missing refrence File")

            raise Exception("Missing reference file - should have been sync'ed by this point")

        faceAnimFileObj = RS.Core.Face.AnimImporter.fbxGetAnimationFromFile(
            self.Path)

        if faceAnimFileObj:
            self._log.LogMessage("Updating face reference")
            importOptions = RS.Core.Face.AnimImporter.RsImportOptions()

            # Setup import options.
            importOptions.clear()

            for takeName, takeObj in faceAnimFileObj.takes.iteritems():
                for layerName, layerObj in takeObj.animationLayers.iteritems():
                    importOptions.addAnimationLayer(takeName, layerName)

            for characterName in faceAnimFileObj.characters:
                importOptions.characters.append(characterName)

            # Get the Reference Null to help fill import options
            importOptions.importOnto = self.ImportOption

            importOptions.currentTakeName = self.TakeName
            importOptions.createMissingTakes = self.CreateMissingTakes

            # Ipmport option added recently, must add data to it if empty
            if self.TargetNamespace == '':
                self._targetNamespace.Data = faceAnimFileObj.characters[0]

            importOptions.targetNamespace = self.TargetNamespace

            # Need to show animation import error if it occurs
            faceAnimSuccess = RS.Core.Face.AnimImporter.importFaceAnimation(
                faceAnimFileObj, importOptions, quiet=False)

            # Update Reference P4 Version
            if not isinstance(filestate, list):
                self._p4VersionProperty.Data = str(filestate.HaveRevision)

            # Fix Face Scale on all Expression Solver Devices. -- Thanh Phan
            # 10/2/2015
            self._log.LogMessage("Fixing face scale")
            RS.Core.Face.FixFaceScale.fixAllScale()

            if not faceAnimSuccess:
                raise Exception("Face asset hasn't been updated")

    def GetFaceBones(self):
        '''
        Returns a list of all facial bones of the current Character
        '''

        faceBonestList = []
        namespaceComponentList = mobu.FBComponentList()
        RS.Globals.Scene.NamespaceGetContentList(
            namespaceComponentList, self.Namespace, mobu.FBPlugModificationFlag.kFBPlugAllContent, True, mobu.FBModel.TypeInfo, False)

        for component in namespaceComponentList:

            prop = component.PropertyList.Find("rs_Type")
            if type(component) == mobu.FBModel and prop:
                if "rs_Mesh"in prop.Data:
                    if component.Name.find("FACIAL") != -1:
                        faceBonestList.append(component)

        return faceBonestList

    def GetFaceControls(self):
        '''
        Returns a list of the facial controls of the current Character
        '''
        faceControlsList = []

        lFaceControls = RS.Utils.Scene.FindModelByName("faceControls_OFF")
        lFaceAttrGUI = RS.Utils.Scene.FindModelByName("FacialAttrGUI")

        if lFaceFacial:
            RS.Utils.Scene.GetChildren(lFaceFacial, faceControlsList)
        if lFaceControls:
            RS.Utils.Scene.GetChildren(lFaceAttrGUI, faceControlsList)

        return faceControlsList

    def DeleteHeadCamLightObjects(self):
        '''
        Deletes headCam and headLight objects
        '''
        deleteList = RS.Utils.Scene.Component.GetComponents(Namespace="",
                                                            TypeList=[
                                                                mobu.FBCamera, mobu.FBLight],
                                                            DerivedTypes=False,
                                                            IncludeRegex='(?i)headCam|headLight')

        map(mobu.FBComponent.FBDelete, deleteList)
