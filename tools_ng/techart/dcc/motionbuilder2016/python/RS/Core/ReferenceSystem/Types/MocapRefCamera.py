import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex


class MocapRefCamera(MocapComplex):
    """
    Represents a mocap virtual camera reference in a scene
    """

    studio = ()
    PropertyTypeName = "Mocap RefCamera"
    FormattedTypeName = "Mocap RefCamera"
    TypePathList = (
        os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "North", "RefCams"),
        os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "NYC", "RefCams"),
        os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "SD", "RefCams"),
        os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "Tor", "RefCams"),
    )
    FileType = "fbx"

    def __init__(self, modelNull, log):
        """
        Call base class constructor
        """
        super(MocapRefCamera, self).__init__(modelNull, log)
