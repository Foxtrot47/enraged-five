from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Types.MocapBasePreviz import MocapBasePreviz
from RS.Core.ReferenceSystem.Types.MocapBuildAsset import MocapBuildAsset


class MocapNonEssential(MocapComplex):
    """
    A master category which holds subcategories
    NonEssential is a category for the users to know they can delete these assets at the end of
    mocap, for cleanup
    """

    PropertyTypeName = "MocapNonEssential"
    FormattedTypeName = "Mocap Non Essential"
    TypePathList = []
    FileType = ""
    SubCats = [MocapBasePreviz, MocapBuildAsset]
