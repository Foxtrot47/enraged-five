"""
Settings used by the reference editor to determine its behavior

Updating the version number cheat sheet:
    Order - Major.Minor.Patch

        Major - Change the public API of system, where existing code might no longer function or work due to the changes introduced
        Minor - Feature Additions
        Patch - Fix an issue where the public API has not changed
"""
Debug = False
Silent = False
IgnoreProject = False
RaiseErrors = False
Optimize = True
Version = "2.5.3"
