import math
import pyfbsdk as mobu
from RS import Globals
from RS.Utils.Scene import Plot
from RS.Core.Animation import Lib as AnimLib
from RS.Utils import Scene
from RS.Utils.Scene import Component, Constraint


NECK_BODY_NODE_IDS = [mobu.FBBodyNodeId.kFBNeckNodeId,
                      mobu.FBBodyNodeId.kFBNeck1NodeId,
                      mobu.FBBodyNodeId.kFBNeck2NodeId,
                      mobu.FBBodyNodeId.kFBHeadNodeId]


THUMB_BODY_NODE_IDS = [mobu.FBBodyNodeId.kFBLeftThumbANodeId,
                       mobu.FBBodyNodeId.kFBLeftThumbBNodeId,
                       mobu.FBBodyNodeId.kFBLeftThumbCNodeId,
                       mobu.FBBodyNodeId.kFBRightThumbANodeId,
                       mobu.FBBodyNodeId.kFBRightThumbBNodeId,
                       mobu.FBBodyNodeId.kFBRightThumbCNodeId]


DEFAULT_ANGLE_TOLERANCE = 0.1


class StancePoseConversionError(Exception):
    """Raised when a stance pose conversion operation is not possible due to external conditions."""
    pass


def quaternionDot(q1, q2):
    """Returns the dot product of two quaternions"""
    return q1[0] * q2[0] + q1[1] * q2[1] + q1[2] * q2[2] + q1[3] * q2[3]


def rotationsAreEquivalent(firstRot, secondRot, tolerance=DEFAULT_ANGLE_TOLERANCE,
                           firstRotationOrder=mobu.FBRotationOrder.kFBXYZ,
                           secondRotationOrder=mobu.FBRotationOrder.kFBXYZ):
    """
    Checks whether two rotations would result in an equivalent orientation within a given angular tolerance.
    
    Args:
        firstRot (mobu.FBVector3d): First euler rotation vector to compare
        secondRot (mobu.FBVector3d): Second euler rotation vector to compare
        tolerance (float): The angular threshold (in degrees) within which to consider the two rotations equivalent.
        firstRotationOrder (mobu.FBRotationOrder): The rotation order for the first rotation input.
        secondRotationOrder (mobu.FBRotationOrder): The rotation order for the second rotation input.
    
    Returns:
        bool: True if the rotations are within the angular tolerance of one another or False if not.
    """
    firstQuat = mobu.FBVector4d()
    mobu.FBRotationToQuaternion(firstQuat, firstRot, firstRotationOrder)
    secondQuat = mobu.FBVector4d()
    mobu.FBRotationToQuaternion(secondQuat, secondRot, secondRotationOrder)

    # Using dot product as simplified test for equivalence.
    # Standard method would be [Q1^-1 * Q2 -> angle axis -> angle] but this way
    # avoids having to write some of those operations since mobu doesn't provide them
    dot = quaternionDot(firstQuat, secondQuat)
    dot = min(1.0, max(dot, -1.0))  # Clamp to ensure rounding errors do not result in an invalid value for use in acos
    angleBetween = math.degrees(math.acos(dot) * 2.0)

    if angleBetween > tolerance:
        return False
    return True


def stancePoseRotationsMatchForBodyNodeIds(firstCharacter, secondCharacter, bodyNodeIds,
                                           tolerance=DEFAULT_ANGLE_TOLERANCE):
    """
    Checks whether the stance poses are equivalent on the two given characters for the given body node IDs.
    
    Args:
        firstCharacter (mobu.FBCharacter): The first character to compare stance pose from.
        secondCharacter (mobu.FBCharacter): The second character to compare stance pose from.
        bodyNodeIds (list of mobu.FBBodyNodeId): The list of body node IDs to compare from each character.
        tolerance (float): The angular threshold within which to consider rotations as equivalent.

    Returns:
        bool: True if the stance poses are equivalent or False if not.
    """
    firstStanceRotOffset = mobu.FBVector3d()
    secondStanceRotOffset = mobu.FBVector3d()
    for bodyNodeId in bodyNodeIds:
        firstCharacter.GetROffset(bodyNodeId, firstStanceRotOffset)
        secondCharacter.GetROffset(bodyNodeId, secondStanceRotOffset)

        if not rotationsAreEquivalent(firstStanceRotOffset, secondStanceRotOffset, tolerance=tolerance):
            return False
    return True


def thumbStancePosesMatch(firstCharacter, secondCharacter, tolerance=DEFAULT_ANGLE_TOLERANCE):
    """
    Checks whether the stance poses on the thumb bones for the two given characters match.
    
    Args:
        firstCharacter (mobu.FBCharacter): The first character to compare stance pose from.
        secondCharacter (mobu.FBCharacter): The second character to compare stance pose from.
        tolerance (float): The angular threshold within which to consider rotations as equivalent.

    Returns:
        bool: True if the stance poses are equivalent or False if not.
    """
    return stancePoseRotationsMatchForBodyNodeIds(firstCharacter, secondCharacter,
                                                  THUMB_BODY_NODE_IDS, tolerance=tolerance)


def neckStancePosesMatch(firstCharacter, secondCharacter, tolerance=DEFAULT_ANGLE_TOLERANCE):
    """
    Checks whether the stance poses on the neck bones for the two given characters match.
    
    Args:
        firstCharacter (mobu.FBCharacter): The first character to compare stance pose from.
        secondCharacter (mobu.FBCharacter): The second character to compare stance pose from.
        tolerance (float): The angular threshold within which to consider rotations as equivalent.

    Returns:
        bool: True if the stance poses are equivalent or False if not.
    """
    return stancePoseRotationsMatchForBodyNodeIds(firstCharacter, secondCharacter,
                                                  NECK_BODY_NODE_IDS, tolerance=tolerance)


def stancePosesMatch(firstCharacter, secondCharacter, tolerance=DEFAULT_ANGLE_TOLERANCE):
    """
    Checks whether the stance poses for the two given characters match.

    Args:
        firstCharacter (mobu.FBCharacter): The first character to compare stance pose from.
        secondCharacter (mobu.FBCharacter): The second character to compare stance pose from.
        tolerance (float): The angular threshold within which to consider rotations as equivalent.

    Returns:
        bool: True if the stance poses are equivalent or False if not.
    """
    return thumbStancePosesMatch(firstCharacter, secondCharacter, tolerance) and \
           neckStancePosesMatch(firstCharacter, secondCharacter, tolerance)


def plotWithStancePoseFixes(sourceChar, targetChar, plotToTargetControlRig=True, allTakes=True):
    """
    Plots animation from one character to another and applies fixes to compensate for stance pose
    differences between the two.
    
    Args:
        sourceChar (mobu.FBCharacter): The character to plot animation from.
        targetChar (mobu.FBCharacter): The character to plot animation to.
        plotToTargetControlRig (bool): If set to True the animation will be plotted back to the control
            rig of the target character - if it has one.
        allTakes (bool): If true the animation will be plotted on all takes.
    """
    if not isinstance(sourceChar, mobu.FBCharacter):
        raise TypeError("sourceChar must be of type FBCharacter, got: {}".format(type(sourceChar)))
    if not isinstance(targetChar, mobu.FBCharacter):
        raise TypeError("targetChar must be of type FBCharacter, got: {}".format(type(targetChar)))
    
    hadActiveControlRigInput = AnimLib.HasControlRig(targetChar)
    
    # Set the source char as input ready for plotting
    targetChar.InputCharacter = sourceChar
    targetChar.InputType = mobu.FBCharacterInputType.kFBCharacterInputCharacter
    targetChar.ActiveInput = True

    # Do normal plot from source char to target char
    Plot.PlotOnCharacterSkeleton(targetChar, allTakesBool=allTakes)
    
    # Reset character inputs
    targetChar.ActiveInput = False

    thumbPlotFixRequired = not thumbStancePosesMatch(sourceChar, targetChar)
    neckPlotFixRequired = not neckStancePosesMatch(sourceChar, targetChar)

    if thumbPlotFixRequired or neckPlotFixRequired:
        sourceBones = []
        targetBones = []
        if thumbPlotFixRequired:
            sourceThumbBones = [sourceChar.GetModel(bodyNodeId) for bodyNodeId in THUMB_BODY_NODE_IDS]
            targetThumbBones = [targetChar.GetModel(bodyNodeId) for bodyNodeId in THUMB_BODY_NODE_IDS]
            if None in sourceThumbBones:
                raise StancePoseConversionError("Not all required source thumb bones were found for character [{}]."
                                                .format(sourceChar.LongName))
            if None in targetThumbBones:
                raise StancePoseConversionError("Not all required target thumb bones were found for character [{}]."
                                                .format(targetChar.LongName))
            sourceBones.extend(sourceThumbBones)
            targetBones.extend(targetThumbBones)
        
        if neckPlotFixRequired:
            sourceNeckBones = [sourceChar.GetModel(bodyNodeId) for bodyNodeId in NECK_BODY_NODE_IDS]
            targetNeckBones = [targetChar.GetModel(bodyNodeId) for bodyNodeId in NECK_BODY_NODE_IDS]
            
            if None in sourceNeckBones:
                raise StancePoseConversionError("Not all required source neck bones were found for character [{}]."
                                                .format(sourceChar.LongName))
            if None in targetNeckBones:
                raise StancePoseConversionError("Not all required target neck bones were found for character [{}]."
                                                .format(targetChar.LongName))
            sourceBones.extend(sourceNeckBones)
            targetBones.extend(targetNeckBones)
    
        tempConstraints = []
        Scene.DeSelectAll()
        for sourceBone, targetBone in zip(sourceBones, targetBones):
            # Create rotation constraint
            orientConstraint = Constraint.CreateConstraint(Constraint.ROTATION)
            orientConstraint.Name = '{}_RotationPlot'.format(targetBone.Name)
            orientConstraint.ReferenceAdd(0, targetBone)
            orientConstraint.ReferenceAdd(1, sourceBone)
            orientConstraint.Active = True
            tempConstraints.append(orientConstraint)
    
            # Select the target bone and focus the property for plotting
            targetBone.Selected = True
            targetBone.Rotation.SetFocus(True)
    
        if allTakes:
            targetTakes = [take for take in Globals.Scene.Takes]
        else:
            targetTakes = [Globals.System.CurrentTake]
    
        # Remember the active take
        origTake = Globals.System.CurrentTake
        try:
            for currentTake in targetTakes:
                Globals.System.CurrentTake = currentTake
                
                # Create a temporary animation layer
                currentTake.CreateNewLayer()
                offsetAnimLayerIndex = currentTake.GetLayerCount() - 1
                offsetAnimLayer = currentTake.GetLayer(offsetAnimLayerIndex)
                offsetAnimLayer.LayerMode = mobu.FBLayerMode.kFBLayerModeOverride
                offsetAnimLayer.Name = "StancePoseConversionPlot"
                currentTake.SetCurrentLayer(offsetAnimLayerIndex)
            
                # Plot constraint result to layer
                plotOptions = AnimLib.PlotOptions(useConstantKeyReducer=False)
                currentTake.PlotTakeOnSelectedProperties(plotOptions)
            
                # Merge layer result
                currentTake.MergeLayers(mobu.FBAnimationLayerMergeOptions.kFBAnimLayerMerge_AllLayers_SelectedProperties,
                                        False,
                                        mobu.FBMergeLayerMode.kFBMergeLayerModeAutomatic,
                                        False)
                offsetAnimLayer.FBDelete()
        finally:
            # Clear selection and property focus
            for targetBone in targetBones:
                targetBone.Rotation.SetFocus(False)
                targetBone.Selected = False
        
            # Clean up constraints
            Component.Delete(tempConstraints)
            
            # Restore the original active take
            Globals.System.CurrentTake = origTake
    
    if hadActiveControlRigInput and plotToTargetControlRig:
        Plot.PlotOnCharacterCtrlRig(targetChar, allTakesBool=allTakes)
        targetChar.InputType = mobu.FBCharacterInputType.kFBCharacterInputMarkerSet
        targetChar.ActiveInput = True
