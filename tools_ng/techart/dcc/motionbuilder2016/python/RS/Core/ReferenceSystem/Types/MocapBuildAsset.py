import os

from RS import Config
from RS.Core.ReferenceSystem.Decorators import Logger
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex


class MocapBuildAsset(MocapComplex):
    """
    Represents a mocap build asset reference in a scene
    """

    studio = ()
    PropertyTypeName = "MocapBuildAsset"
    FormattedTypeName = "Mocap Build Asset"
    TypePathList = [
        os.path.join(Config.VirtualProduction.Path.Previz, "Global", "PreBuildAssets"),
    ]
    FileType = "fbx"

    def __init__(self, modelNull, log):
        """
        Call base class constructor
        """
        super(MocapBuildAsset, self).__init__(modelNull, log)

    @Logger("Build Asset - Create")
    def _create(self, filestate=None, importOptions=None):
        """
        This function gets run only when a build asset is created. Calls the base class and then
        fixes the groups.

        Args:
            importOptions (FaceAnimDataImportOptions, optional): face import options.
        """
        super(MocapBuildAsset, self)._create(filestate=filestate, importOptions=importOptions)

        # update groups
        self._log.LogMessage("Updating build asset groups into the previz groups")
        self.UpdateGroups()
