"""
Tests scenes with known reference system related problems
"""
import os
import sys
import unittest

from RS import Perforce, Globals
from RS.Unittests import utils
from RS.Core.ReferenceSystem.UnitTest import base, const


def Run(className=None):
    """Run unittests from this module

    Args:
        testName (string): the name of the class whose test should be ran. Runs all the tests if None.
    """
    unittest.TextTestRunner(verbosity=0).run(suite(className))


def suite(className=None):
    """A suite to hold all the tests from this module

    Args:
        testName (string): the name of the class whose test should be ran. Runs all the tests if None.
    Returns:
        unittest.TestSuite
    """
    module = sys.modules[__name__]
    return utils.getSuite(module, "test_Scene", className)


class SceneBase(base.Base):

    KEY = None

    def test_Scene(self):
        """Base testing of scene assets

        This method grabs the KEY attribute set to check the yaml config for data like the path, revision etc.
        If the value isn't set, it will grab the key via the name of the class. ex class test_Prop = Prop
        By default this method will create, rename, update, and delete the asset. If there is a swapPath in the config
        it will do a swap as well.

        Returns:
            bool: If successful or not
        """
        # get key, use class name if not set
        self.KEY = self.KEY if self.KEY else self.__class__.__name__.split("_")[-1]

        # get data
        scenesConfig = utils.loadYaml(const.ConfigPathDict.SCENES_CONFIG_PATH)
        self.assetData = utils.getConfigKey(self.KEY, scenesConfig)

        if not self.assetData:
            print("No config key found for {} to test scene".format(self.KEY))
            return False

        self.path = self.assetData.get("path", "")
        self.revision = self.assetData.get("revision", None)
        self.swapPath = self.assetData.get("swapPath", "")
        self.swapRevision = self.assetData.get("swapRevision", None)
        self.references = self.assetData.get("references", "")

        # sync file
        Perforce.Sync(self.path, force=True, revision=self.revision)
        if not self.path or not os.path.exists(self.path):
            self.fail(
                "{} filepath doesn't exist! Manually sync this file or check the scenes.yaml config".format(self.path)
            )

        # open scene
        Globals.Application.FileOpen(self.path)

        return True


class Test_Case001(SceneBase):
    """
    Swapping Player Zero in to his summer outfit causes a crash
    """

    def test_Scene(self):
        """Testing swapping player zero crash"""
        validScene = super(Test_Case001, self).test_Scene()

        if not validScene:
            return

        # get reference in scene
        reference = self.Manager.GetReferenceByNamespace(self.references[0])

        # sync swap
        Perforce.Sync(self.swapPath, force=True, revision=self.revision)
        self.assertSwap(reference, self.swapPath)


class Test_Case002(SceneBase):
    """
    Updating Player Zero causes a crash
    """

    def test_Scene(self):
        """Testing updating player zero crash"""
        validScene = super(Test_Case002, self).test_Scene()

        if not validScene:
            return

        reference = self.Manager.GetReferenceByNamespace(self.references[0])

        self.assertMinorUpdate(reference)
        self.assertMajorUpdate(reference)


class Test_Case003(SceneBase):
    """
    Deleting duplicate horses causes a crash,  bug:3647929
    """

    def test_Scene(self):
        """Testing duplicate horses causing a crash"""
        validScene = super(Test_Case003, self).test_Scene()

        if not validScene:
            return

        for referenceName in self.references:
            reference = self.Manager.GetReferenceByNamespace(referenceName)
            self.assertDelete(reference)
