import pyfbsdk as mobu

from PySide import QtGui, QtCore

import os
import re
import time
from ctypes import cdll
from functools import wraps

from RS import Globals, Perforce, Config
from RS.Utils import Collections, ContextManagers, Path, Namespace, Scene
from RS.Utils.Scene import Component, Cleanup
from RS.Utils.Logging import Universal
from RS.Tools.UI.MovingCutscene.Model import Components
from RS.Tools.UI import Application
import RS.Core.ReferenceSystem as Core
from RS.Core.ReferenceSystem import Decorators, Exceptions, Settings
from RS.Utils import Exception as RSException

from RS.Core.ReferenceSystem.Types.Character import Character
from RS.Core.ReferenceSystem.Types.Face import Face
from RS.Core.ReferenceSystem.Types.Audio import Audio
from RS.Core.ReferenceSystem.Types.Prop import Prop
from RS.Core.ReferenceSystem.Types.Set import Set
from RS.Core.ReferenceSystem.Types.AmbientFace import AmbientFace
from RS.Core.ReferenceSystem.Types.RayFire import RayFire
from RS.Core.ReferenceSystem.Types.Vehicle import Vehicle
from RS.Core.ReferenceSystem.Types.Complex import Complex
from RS.Core.ReferenceSystem.Types.Unknown import Unknown
from RS.Core.ReferenceSystem.Types.MocapEssential import MocapEssential
from RS.Core.ReferenceSystem.Types.MocapNonEssential import MocapNonEssential
from RS.Core.ReferenceSystem.Types.MocapBasePreviz import MocapBasePreviz
from RS.Core.ReferenceSystem.Types.MocapBlockingModel import MocapBlockingModel
from RS.Core.ReferenceSystem.Types.MocapVirtualCamera import MocapVirtualCamera
from RS.Core.ReferenceSystem.Types.MocapRefCamera import MocapRefCamera
from RS.Core.ReferenceSystem.Types.MocapGsSkeleton import MocapGsSkeleton
from RS.Core.ReferenceSystem.Types.MocapReviewTrial import MocapReviewTrial
from RS.Core.ReferenceSystem.Types.MocapPropToy import MocapPropToy
from RS.Core.ReferenceSystem.Types.MocapSlate import MocapSlate
from RS.Core.ReferenceSystem.Types.MocapStage import MocapStage
from RS.Core.ReferenceSystem.Types.MocapHUD import MocapHUD
from RS.Core.ReferenceSystem.Types.MocapReferencePose import MocapReferencePose
from RS.Core.ReferenceSystem.Types.MocapBuildAsset import MocapBuildAsset
from RS.Core.ReferenceSystem.Types.CharacterHelper import CharacterHelper
from RS.Core.ReferenceSystem.Types.CharacterCutscene import CharacterCutscene
from RS.Core.ReferenceSystem.Types.CharacterIngame import CharacterIngame
from RS.Core.ReferenceSystem.Types.CharacterAnimal import CharacterAnimal

def Deprecated(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        # TODO: Add code that updates a table on the database everytime deprecated methods are used.
        return func(*args, **kwargs)

    return wrapper


class Singleton(type):
    _instance = None
    def __call__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instance


class QtSingleton(type(QtCore.QObject), Singleton):
    """
    A metaclass for creating singletons which inherit from PySide classes
    (avoids metaclass conflict).
    """

    pass


class Manager(QtCore.QObject):
    """
    Manages what references exist in the current scene.

    Signals:
        referenceAdded (list of References): List of references which were added
        referenceUpdated (list of References): List of references which were updated
        referenceRemoved (list of Str): List of namespaces which were deleted
    """

    __metaclass__ = QtSingleton

    referenceAdded = QtCore.Signal(object)
    referenceUpdated = QtCore.Signal(object)
    referenceRemoved = QtCore.Signal(object)

    def __init__(self):
        """
        Initialise the manager.
        """
        super(Manager, self).__init__()
        self._referenceDictionary = {}
        self._rootNull = None
        self._p4SyncStatus = True
        self._active = False
        self.trialIDDict = {}

        # Setup our log
        self._log = Universal.UniversalLog("{0}_reference_system".format(os.getpid()))
        # Don't want to output to console for now
        self._log.ConsoleOutput = False

    def disconnect(self, *args, **kwargs):
        """Disconnect callback"""
        value = super(Manager, self).disconnect(*args, **kwargs)

        signalArg = args[1]
        if isinstance(signalArg, str):
            signalName = re.search("\d+(?P<signalArg>[a-zA-Z0-9]+)\(", signalArg).group(1)
        else:
            signalName = "Signal not found. May be stored in a different arg!"

        if value is False:
            self._log.LogError("[Reference System Manager] Unable to disconnect signal '{}'".format(signalName))

        return True

    @Decorators.Logger("Manager - Checking RS Null Namespace")
    def _fixReferenceNullNamespace(self):
        """
        on rare occassions, reference nulls will have a namespace 'RS Null 1', we need to check for
        this and fix it if it occurs.
        """
        if self._rootNull is None:
            return

        # get child list
        refNullList = []
        Scene.GetChildren(self._rootNull, refNullList, "", False)

        for refNull in refNullList:
            splitRSNullName = refNull.LongName.split(":", 1)
            if splitRSNullName[0] != "RS_Null":
                refNull.LongName = "RS_Null:{0}".format(splitRSNullName[-1])
                self._log.LogMessage("Fixing RS Null Namespace for: {0}".format(refNull.LongName))

    @Decorators.Logger("Manager - Parsing Reference")
    def _parseReference(self, modelNull):
        """
        Attempts to parse a reference from the passed model null
        """
        # Match the type by NAME from the property
        typeNameProperty = modelNull.PropertyList.Find(Core.STR_REFERENCE_PATH)
        if typeNameProperty is None:
            message = "Custom properties are missing. Unable to find path file for [{0}].\n\n For the reference editor to work you will need to find and delete this null , or manually add in the required custom properties.".format(
                modelNull.LongName
            )
            raise RSException.PopupError("Broken Reference Null", message)

        typeName = typeNameProperty.Data
        matchingType = self._resolveTypeByPath(typeName)

        # Verify this matches the type derived from PATH
        filePath = modelNull.PropertyList.Find(Core.STR_REFERENCE_PATH).Data
        filePathType = self._resolveTypeByPath(filePath)
        if matchingType != filePathType:
            raise Exception(
                "The type [{0}] isn't consistent with the location of the file [{1}].".format(
                    typeName.FormattedTypeName, filePath
                )
            )

        # get model name, some may contain additional namespaces
        orgName = modelNull.LongName
        rsNullNamespace, modelName = modelNull.LongName.split(":", 1)
        nameSpaceProperty = modelNull.PropertyList.Find(Core.STR_NAMESPACE)
        if nameSpaceProperty:
            if modelName != nameSpaceProperty.Data:
                # Check if that namespace is already in use
                if self._usingNamespace(nameSpaceProperty.Data) is True:
                    raise Exception(
                        "Namespace '{}' is already in use, unable to update '{}'".format(
                            nameSpaceProperty.Data, modelNull.LongName
                        )
                    )

                msg = "Ref System Null miss matching for namespace, expecting {}, got {}. Updating".format(
                    modelName, nameSpaceProperty.Data
                )
                self._log.LogMessage(msg)
                newName = "{}:{}".format(rsNullNamespace, nameSpaceProperty.Data)
                modelNull.LongName = newName
                if modelNull.LongName != newName:
                    raise Exception("Unable to rename '{}' to '{}'".format(orgName, newName))
                modelName = nameSpaceProperty.Data

        # Create a new instance of it by passing in the FBModelNull
        loadedReference = matchingType(modelNull, self._log)

        # Validate the reference, will raise if not valid with error
        loadedReference.Validate()

        # check to see if the name of the null is the same as the its namespace property
        if self._checkNameConsistency(modelName, loadedReference.Namespace):
            raise Exception(
                "Model null's name [{}] does not match its namespace [{}] property.".format(
                    modelNull.LongName, loadedReference.Namespace
                )
            )

        # Make sure the namespace isn't in use already
        if self._usingNamespace(loadedReference.Namespace) and not self.Silent:
            QtGui.QMessageBox.warning(
                Application.GetMainWindow(),
                "Namespace warning",
                "There are multiple references with the same namespace:\n\n[{0}]\n\nYou should resolve this so there is only one.".format(
                    loadedReference.Namespace
                ),
            )

        # Add it to our dictionary and return it
        self._referenceDictionary[loadedReference.Namespace] = loadedReference

        return loadedReference

    def _getScriptSetupVersionFromAsset(self, namespace):
        """
        Look for the reference's script setup version, so we can add the property to the RS Null
        """
        scriptVersion = "1"
        componentList = [
            component
            for component in Globals.Components
            if Component.GetNamespace(component) == namespace
            if isinstance(component, mobu.FBModel)
        ]

        for component in componentList:
            scriptVersionProperty = component.PropertyList.Find(Core.STR_SETUP_VERSION)
            if scriptVersionProperty is not None:
                scriptVersion = scriptVersionProperty.Data
                break
        return scriptVersion

    def _saveExportSettings(self):
        """
        Temporary function that will save the root null to an empty file.Its purpose is to force
        save/create the userObjects related to the animation export (saving the settings of the exporter).
        When we merge or load a file the markup that was stored (if any) is loaded. So any changes that
        have been made by the user will be lost. We must preform a save operation to force save the markup.
        """

        self._log.LogMessage("Saving export settings.")

        # Deselect all objects - ToDo - this should be changed to use the 'with' statement for
        # the begin change all models.
        mobu.FBBeginChangeAllModels()
        for component in Globals.Components:
            component.Selected = False
        mobu.FBEndChangeAllModels()

        self._rootNull.Selected = True

        tempFbxFilePath = os.path.join(Config.Tool.Path.Temp, "markup_temp.fbx")

        saveOptions = mobu.FBFbxOptions(True, tempFbxFilePath)
        saveOptions.SetAll(mobu.FBElementAction.kFBElementActionDiscard, False)

        saveOptions.SaveSelectedModelsOnly = True
        saveOptions.UpdateRecentFiles = False

        Globals.Application.FileSave(tempFbxFilePath, saveOptions)

        self._rootNull.Selected = False

    def _checkNameConsistency(self, nullName, namespace):
        """
        Checks to see if the name of the reference null is the same as it's namespace property
        """
        # make the check case insensitive
        if str(nullName).lower() == str(namespace).lower():
            # check if there is a casing mismatch and log a warning if one is found
            if str(nullName) != str(namespace):
                self._log.LogWarning(
                    "Model null's Casing [{0}] does not match its namespace [{1}] property casing".format(
                        nullName, namespace
                    )
                )
            return False
        else:
            return True

    def _usingNamespace(self, targetNamespace):
        """
        Returns true if the passed namespace is in use.
        """
        for key in self._referenceDictionary.iterkeys():
            if all([key, targetNamespace]):
                if key.lower() == targetNamespace.lower():
                    return True
        return False

    @Decorators.Logger("Manager - Resolving Namespace")
    def _resolveNamespace(self, namespace, includeIteration=True):
        """
        Returns the correct namespace for the passed file path. Will derive this from the filename
        for the passed reference file path. It will add ^1 ^2 etc... for each subsequent reference.

        Arguments:
            namespace (string): namespace to resolve

        Return:
            string
        """
        self._log.LogMessage("Resolving the Reference Namespace")

        # Remove the outfit name from the namespace, which uses a '-' as the separator.
        namespace = namespace.split("-")[0]

        if includeIteration is not True:
            return namespace

        baseNamespace = namespace
        iteration = 1
        while iteration < 50:
            if not self._usingNamespace(namespace):
                return namespace
            namespace = "{0}^{1}".format(baseNamespace, str(iteration))
            iteration += 1

        raise Exception("Detected over 50 references which are using the derived namespace!")

    @Decorators.Logger("Manager - Resolving Type by Path")
    def _resolveTypeByPath(self, filePath, useCoreProjectPath=False):
        """
        Returns the type from the passed file path. Will raise an exception if the file is not
        rooted in one of the designated resource directories.
        """
        projectRootPath = Config.Project.Path.Root.replace("\\", "\\\\")

        # compile regex expression to see if a string starts with the one of the project paths for speed
        expression = r"{}|{}|{}".format(
            projectRootPath, Config.VirtualProduction.Path.Previz.replace("\\", "\\\\"), r"x:\\projects\\"
        )
        if Config.CoreProject.Path.Root != Config.Project.Path.Root:
            expression = "{}|{}".format(expression, Config.CoreProject.Path.Root.replace("\\", "\\\\"))

        inProjectRegex = re.compile(expression, re.I)

        for referenceType in self.GetReferenceTypeList():
            for typePath in referenceType.TypePathList:
                if typePath.lower() in filePath.lower():
                    if self.IgnoreProject or inProjectRegex.match(filePath):
                        if type(referenceType.FileType) is tuple:
                            for fileType in referenceType.FileType:
                                if re.search(fileType, filePath, re.I):
                                    return referenceType
                        else:
                            if re.search(referenceType.FileType, filePath, re.I):
                                return referenceType
        return Unknown

    @Decorators.Logger("Manager - Resolving Type by Name")
    def _resolveTypeByName(self, typeName):
        """
        Returns the type from the passed passed string - from a ModelNull's property.
        """
        self._log.LogMessage("Resolving the Reference Type by its Name")
        for referenceType in self.GetReferenceTypeList():
            if referenceType.PropertyTypeName.lower() == typeName.lower():
                return referenceType

        raise Exception("Unrecognised type [{0}].".format(typeName))

    def _returnMainCategoryType(self, referenceType):
        """
        get the main category for a reference. If already a main cateogry, it will just return the
        same type.

        Returns:
        String(referenceType)
        """
        for reference in self.GetReferenceHierarchyList():
            for subCat in reference.SubCats or []:
                if referenceType == subCat:
                    return reference
        return referenceType

    def _checkReference(self, reference):
        """
        Checks if given reference is a valid reference in the manager

        Arguments:
            reference (string): name of the reference
        """

        if not reference or reference.Namespace not in self._referenceDictionary:
            raise Exceptions.MissingReference()

    @Decorators.IsListEmpty()
    def _processReferences(self, referenceList):
        """
        Checks to see if the references are valid. i.e are reference still in the scene and are
        in the reference dictionary
        """

        if type(referenceList) != list:
            referenceList = [referenceList]

        currentList = list(self.GetReferenceListAll())

        for ref in referenceList:
            referenceFound = False
            for currentRef in currentList:
                if ref.Namespace.lower() == currentRef.Namespace.lower():
                    referenceFound = True
                    break

            if referenceFound is True:
                continue
            else:
                raise Exceptions.MissingReference()

        return referenceList

    def _checkGeometryName(self, namespace):
        """
        For swapping from CS to IG, we need to make sure the geo's name is 'Geometry', and the top
        parent null doesn't have extra numbers in the name.
        The CS setups have the geo named after the character and IG setups have it named 'Geometry',
        which is causing issues when character swapping between CS and IG assets.
        url:bugstar:2914543
        """
        # get Name (namespace may have '^#', which we will need to remove)
        splitNamespace = namespace.split("^")
        name = splitNamespace[0]

        # find 'Geometry' null
        geometry = Scene.FindModelByName("Geometry")

        # if geo node is found we dont need to take any further action
        if geometry is not None:
            return

        # if geometry is not found, we look for the double name - e.g. Player_Zero:Player_Zero
        geometry = Scene.FindModelByName("{0}:{1}".format(namespace, name), True)

        # if we cant find geo at all, we dont need to take any further action
        if geometry is None:
            return

        # rename to 'Geometry'
        geometry.Name = "Geometry"

        # find top parent null, it will have a ' 1' at the end of the name, due to clashing with the
        # old geo name
        topParentNull = Scene.FindModelByName("{0}:{1} 1".format(namespace, name), True)

        # if top node cannot be found, we dont need to take any further action
        if topParentNull is None:
            return

        # remove extra ' 1 ' in the name
        topParentNull.Name = name

    def _getRootNull(self, createIfNotFound=False):
        """
        Gets the root reference null of the scene

        kwargs:
            createIfNotFound (bool): Should the reference Null be created if its not present in the scene
        """
        null = mobu.FBFindModelByLabelName("REFERENCE:Scene")
        if createIfNotFound and null is None:
            null = mobu.FBModelNull("REFERENCE:Scene")
        return null

    def rootChanged(self):
        """ Has the root null changed """
        rootNull = mobu.FBFindModelByLabelName("REFERENCE:Scene")
        return rootNull != self._rootNull

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Reloading data")
    def _reload(self, force=False):
        """
        Reloads the manager. Only if needed - so if the rootNull has changed. Which indicates that
        a new scene been loaded.
        """
        # get root null
        rootNull = mobu.FBFindModelByLabelName("REFERENCE:Scene")

        if rootNull != self._rootNull or force:
            self._log.LogMessage("RELOAD: Scene has changed so performing a reload.")

            # check for multiple instances
            referenceNullList = [
                component
                for component in Globals.Components
                if re.search("(^reference|scene$)", component.LongName, re.I)
                if isinstance(component, mobu.FBModelNull)
            ]

            if len(referenceNullList) > 1 and not self.Silent:
                QtGui.QMessageBox.warning(
                    Application.GetMainWindow(),
                    "Reference:Scene Error",
                    "There are [{0}] instances of the 'REFERENCE:Scene' null.\n\n "
                    "There should only be ONE.\n\n"
                    "Please correct this in your scene, or you will encounter issues.".format(len(referenceNullList)),
                )

            # Unload before we attempt to load anything
            self._referenceDictionary = {}

            # Record the null we are working with
            self._rootNull = rootNull

            # check for abnormailities in RS Null namespace (e.g. 'RS Null 1')
            self._log.LogMessage("Checking for RS_Null namespace abnormailities.")
            self._fixReferenceNullNamespace()

            # If we don't have root null then there is nothing to load
            if self._rootNull is None:
                self._log.LogWarning("No reference root null - no data to load.")
            else:
                self._log.LogMessage(Globals.Application.FBXFileName)
                # For each child (should be a list of FBModelNull objects) of
                # the root parse
                errors = []
                for modelNull in self._rootNull.Children:
                    try:
                        reference = self._parseReference(modelNull)

                        self._log.LogMessage(
                            "Successfully parsed [{0}] as [{1}]".format(
                                reference.Namespace, type(reference).FormattedTypeName
                            )
                        )
                        
                    except Exception as error:
                        self._log.LogError("Error parsing - {0}".format(modelNull.LongName))
                        self._log.LogError(error.message)
                        errors.append(error)
                
                if errors:
                    if len(errors) == 1:
                        raise errors[0]
                    
                    maxDisplayCount = 10
                    displayCount = len(errors)
                    
                    if displayCount > maxDisplayCount:
                        # Show the log to the user instead of allowing an infinite size popup
                        self.ShowLog(modal=False)
                        displayCount = maxDisplayCount
                        
                    combinedMessage = "<br><br>".join([str(error) for error in errors[:displayCount]])
                    raise Exceptions.MultipleReferenceErrors(combinedMessage)

    @Decorators.Logger("Manager - Clean scene")
    def CleanScene(self):
        """
        Cleans up the scene. Deletes things that are not connected to anything and fixes various problems.
        """

        self._log.LogMessage("Start cleaning the scene...")

        self._log.LogMessage("clean scene: clean unused components...")
        delete = Cleanup.GetUnusedComponentsList()
        self._log.LogMessage("clean scene: clean old facial rigs...")
        delete.extend(Cleanup.GetOldFacialRigs())
        self._log.LogMessage("clean scene: delete multicameraviewerdata & unusedbindposedata...")
        if "Mocap Workstation" not in Config.Script.Profile:
            delete.extend(Cleanup.GetMultiCameraViewerDataList())
        for namespace in Globals.Namespaces:
            delete.extend(Cleanup.GetUnusedBindPoseList(namespace))
        Component.Delete(delete, deleteNamespaces=True)

        self._log.LogMessage("End cleaning the scene.")

    def _cleanScene(self):
        """
        Deprecated, please use CleanScene instead
        """
        self.CleanScene()

    def SetConsoleOutputState(self, value):
        """
        Sets the manager log to output to the console.
        """
        self._log.ConsoleOutput = value

    def ShowLog(self, modal=True):
        """
        Shows the log.
        """
        self._log.Show(modal=modal)

    def GetReferenceListByType(self, targetType, force=False):
        """
        Returns a list of references of the passed type.
        """
        self._reload(force)

        if targetType.SubCats is None or len(targetType.SubCats) == 0:
            return list(reference for reference in self._referenceDictionary.values() if type(reference) == targetType)

        referenceTypeList = []
        for cat in targetType.SubCats:
            for reference in self._referenceDictionary.values():
                if type(reference) == cat:
                    referenceTypeList.append(reference)

        return referenceTypeList

    def IsReferenced(self, targetNamespace):
        """
        Check to see if a passed namespace has a reference.

        Returns:
            bool
        """
        self._reload()

        if self._usingNamespace(targetNamespace):
            return True
        else:
            return False

    def GetReferenceListAll(self, forceReload=False):
        """
        Returns a list of all references in the scene.
        """
        self._reload(force=forceReload)

        return self._referenceDictionary.values()

    def GetReferenceByNamespace(self, targetNamespace):
        """
        Returns a reference that matches the passed namespace.
        """
        self._reload()

        if self._usingNamespace(targetNamespace):
            for key, value in self._referenceDictionary.iteritems():
                if key.lower() == targetNamespace.lower():
                    return value
        else:
            self._log.LogError("No reference with the passed namespace")
            return None

    def GetNamespaceList(self, targetNamespace):
        """
        Returns all namespaces that are in use within the scene.
        """
        self._reload()
        return self._referenceDictionary.keys()

    def GetReferenceTypeList(self):
        """
        Returns a list of all the reference types, including top categories and sub categories.
        """
        return [
            AmbientFace,
            Audio,
            Character,
            CharacterIngame,
            CharacterCutscene,
            CharacterHelper,
            CharacterAnimal,
            Face,
            Prop,
            RayFire,
            Set,
            Vehicle,
            MocapEssential,
            MocapBlockingModel,
            MocapBuildAsset,
            MocapVirtualCamera,
            MocapRefCamera,
            MocapHUD,
            MocapGsSkeleton,
            MocapPropToy,
            MocapSlate,
            MocapStage,
            MocapReferencePose,
            MocapNonEssential,
            MocapBasePreviz,
            MocapReviewTrial,
            Unknown,
        ]

    def GetReferenceHierarchyList(self):
        """
        Returns a list of all the top reference type categories.
        Does not include sub catergories.
        """
        return [
            AmbientFace,
            Audio,
            Character,
            Face,
            Prop,
            RayFire,
            Set,
            Vehicle,
            MocapEssential,
            MocapNonEssential,
            MocapReviewTrial,
            Unknown,
        ]

    def GetTypeFromPath(self, path):
        """
        Get the reference type of an fbx by its path.

        Arguments:
            path (string): path to the fbx file

        Return:
            RS.Core.ReferenceSystem.Types.Base.Base or None
        """
        try:
            return self._resolveTypeByPath(path)
        except Exception:
            pass

    def MakeReferenceFromPath(self, namespace, path):
        """
        Turn an existing unreferenced asset into a referenced asset

        args:
        namespace - string needed to create reference modelnull
        path - path to asset - where we can determine the type of reference
        """
        # is type known
        if not os.path.exists(path):
            return

        if not isinstance(namespace, str):
            return

        # get type from path
        referenceType = self._resolveTypeByPath(path)
        if referenceType is Unknown:
            raise Exceptions.UnrecognisedReferenceDirectory(path)

        if self._rootNull is None:
            self._rootNull = mobu.FBModelNull("REFERENCE:Scene")

        # Create our modelNull and Link it up to the root null and set the
        # asset type correctly
        modelNull = mobu.FBModelNull("RS_Null:{0}".format(namespace))
        modelNull.Parent = self._rootNull

        # Create our properties.
        modelNull.PropertyCreate(
            Core.STR_REFERENCE_PATH, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None
        )
        modelNull.PropertyCreate(Core.STR_NAMESPACE, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
        modelNull.PropertyCreate(Core.STR_RS_ASSET_TYPE, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
        modelNull.PropertyCreate(Core.STR_UPDATE, mobu.FBPropertyType.kFBPT_bool, "Bool", True, True, None)
        modelNull.PropertyCreate(Core.STR_P4_VERSION, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
        modelNull.PropertyCreate(Core.STR_PROXY_ASSET, mobu.FBPropertyType.kFBPT_bool, "Bool", False, True, None)

        modelNull.PropertyList.Find(Core.STR_RS_ASSET_TYPE).Data = referenceType.PropertyTypeName
        modelNull.PropertyList.Find(Core.STR_NAMESPACE).Data = namespace
        modelNull.PropertyList.Find(Core.STR_REFERENCE_PATH).Data = os.path.normpath(path)
        # We set this to zero to start with which will be used to indicate
        # a 'create' operation
        modelNull.PropertyList.Find(Core.STR_UPDATE).Data = 0
        modelNull.PropertyList.Find(Core.STR_PROXY_ASSET).Data = 0

        # Pass it to the parse reference function which handle the rest
        # of the creation and adding to list of references
        newReference = referenceType(modelNull, self._log)

        self._referenceDictionary[newReference.Namespace] = newReference

        return newReference

    @Decorators.Logger("Manager - Creating Reference Snapshot")
    def CreateReferenceSnapshot(self, reference):
        currentFrame = Globals.System.LocalTime.GetFrame()

        # Create our modelNull and Link it up to the root null and set the asset type correctly
        snapshotNamespace = "Snapshot:{}:frame_{}".format(reference.Namespace.replace(":", "_"), currentFrame)

        baseNamespace = snapshotNamespace
        iteration = 1
        while iteration < 50:
            componentList = mobu.FBComponentList()
            mobu.FBFindObjectsByName(snapshotNamespace, componentList, True, True)
            if len(componentList) == 0:
                break

            snapshotNamespace = "{0}^{1}".format(baseNamespace, str(iteration))
            iteration += 1

        modelNull = mobu.FBModelNull("RS_Null:{}".format(snapshotNamespace))
        modelNull.Parent = self._rootNull
        snapshotNull = mobu.FBModelNull(snapshotNamespace)

        modelNull.PropertyCreate("RefNamespace", mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
        modelNull.PropertyCreate("SnapshotNamespace", mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
        modelNull.PropertyCreate("RefType", mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
        modelNull.PropertyCreate("RefName", mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
        modelNull.PropertyList.Find("RefType").Data = "Snapshot"
        modelNull.PropertyList.Find("RefName").Data = "Snapshot of '{}' on frame {}".format(
            reference.Name, currentFrame
        )
        modelNull.PropertyList.Find("RefNamespace").Data = reference.Namespace
        modelNull.PropertyList.Find("SnapshotNamespace").Data = snapshotNull.LongName
        modelNull.Parent = reference.ModelNull

        for item in reference.CreateSnapshot():
            item.LongName = "{}:{}".format(snapshotNull.LongName, item.Name)
            item.Parent = snapshotNull

        # emit the callback signal for other widgets to use
        self.referenceAdded.emit([reference])
        self._log.LogMessage("Added Snapshot of '{}' on frame {}".format(reference.Name, currentFrame))

    @Decorators.Logger("Manager - Removing Reference Snapshot(s)")
    def DeleteReferenceSnapshots(self, reference, snapshots=None):
        deletedSnapshots = []
        for snapshot in snapshots or reference.GetSnapshots():
            # Get the Snapshot frame root
            componentList = mobu.FBComponentList()
            snapshotRootName = snapshot.PropertyList.Find("SnapshotNamespace").Data
            mobu.FBFindObjectsByName(snapshotRootName, componentList, True, True)
            deletedSnapshots.append(snapshotRootName)
            if len(componentList) != 1:
                raise ValueError("Unable to find Snapshot root '{}'".format(snapshotRootName))

            Namespace.Delete([snapshotRootName], True, [snapshot, componentList[0]])

            self._log.LogMessage("Delete Snapshot of '{}' from {}".format(snapshotRootName, reference.Name))

        # emit the callback signal for other widgets to use
        self.referenceRemoved.emit(deletedSnapshots)

    @Decorators.Active
    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Creating References")
    @Decorators.ReferenceMergeTransaction()
    @Decorators.IsListEmpty()
    @Decorators.CleanScene
    def CreateReferences(self, filePathList, useCoreProjectPathToResolveType=False, importOptions=None):
        """
        Creates a new reference from the passed filePathList
        """
        self._reload()

        # Animation Exporter plugin loads UI values from the exported clips on disk. Disable this on
        # a reference update.
        if self.PluginsAvailable:
            cdll.rexMBRage.SetLoadClipDataIntoUI_Py(False)

        p4Sync = self.PerforceSync

        if type(filePathList) != list:
            filePathList = [filePathList]

        # Fix possible issue with the path
        filePathList = [os.path.normpath(str(path)) for path in filePathList]

        # Start our sync operation on the first path
        if p4Sync is True:
            currentSyncOperation = Perforce.SyncOperation(Collections.Peek(filePathList), start=True)

        # List to store new references created
        newReferenceList = []
        while Collections.HasItems(filePathList):
            # Get the current path
            filePath = Collections.Pop(filePathList)

            # Wait until the sync operation for this reference is complete.
            if p4Sync is True:
                currentSyncOperation.WaitForCompletion()

            # If there is another reference on the list then queue the sync
            # operation
            if Collections.HasItems(filePathList) and p4Sync is True:
                currentSyncOperation = Perforce.SyncOperation(Collections.Peek(filePathList), start=True)
            # Setup our log context
            self._log.LogMessage("Starting creation of reference [{0}].".format(filePath))

            referenceType = self._resolveTypeByPath(filePath, useCoreProjectPathToResolveType)
            if referenceType is Unknown:
                raise Exceptions.UnrecognisedReferenceDirectory(filePath)

            self._log.LogMessage("Reference type detected as [{0}].".format(referenceType.FormattedTypeName))

            # If we don't have a root null we will have to create one at this
            # point
            if self._rootNull == None:
                self._log.LogMessage("Creating new root null object as this is fresh/empty scene.")
                self._rootNull = mobu.FBModelNull("REFERENCE:Scene")

            # Create our model null and associated properties
            self._log.LogMessage("Creating new reference null object and setting up properties.")

            # Get our namespace
            fileName = Path.GetBaseNameNoExtension(filePath)

            # Remove double extension
            if "." in fileName:
                fileName = Path.GetBaseNameNoExtension(fileName)

            namespace = self._resolveNamespace(fileName)
            self._log.LogMessage("Namespace derived as [{0}].".format(namespace))

            # Create our modelNull and Link it up to the root null and set the
            # asset type correctly
            modelNull = mobu.FBModelNull("RS_Null:{0}".format(namespace))
            modelNull.Parent = self._rootNull

            # Create our properties.
            modelNull.PropertyCreate(
                Core.STR_REFERENCE_PATH, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None
            )
            modelNull.PropertyCreate(Core.STR_NAMESPACE, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None)
            modelNull.PropertyCreate(
                Core.STR_RS_ASSET_TYPE, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None
            )
            modelNull.PropertyCreate(Core.STR_UPDATE, mobu.FBPropertyType.kFBPT_bool, "Bool", True, True, None)
            modelNull.PropertyCreate(
                Core.STR_P4_VERSION, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None
            )
            modelNull.PropertyCreate(Core.STR_PROXY_ASSET, mobu.FBPropertyType.kFBPT_bool, "Bool", False, True, None)
            modelNull.PropertyCreate(
                Core.STR_SETUP_VERSION, mobu.FBPropertyType.kFBPT_charptr, "String", False, True, None
            )

            modelNull.PropertyList.Find(Core.STR_RS_ASSET_TYPE).Data = referenceType.PropertyTypeName
            modelNull.PropertyList.Find(Core.STR_NAMESPACE).Data = namespace
            modelNull.PropertyList.Find(Core.STR_REFERENCE_PATH).Data = os.path.normpath(filePath)
            # We set this to zero to start with which will be used to indicate
            # a 'create' operation
            modelNull.PropertyList.Find(Core.STR_UPDATE).Data = 0
            modelNull.PropertyList.Find(Core.STR_PROXY_ASSET).Data = 0

            try:
                # Pass it to the parse reference function which handle the rest
                # of the creation and adding to list of references
                newReference = referenceType(modelNull, self._log)

                # Saving the UserObjects- export Rage- saves changes made to
                # the settings
                self._saveExportSettings()

                # Get the visualmodel list from under the reference namespace and check if the namespace of the model
                # is corresponds to the namespace of the textures
                newReference.CheckNamespaceConsistency()

                # If this was successful then create the new reference
                newReference._create(importOptions=importOptions)

                # set script version property data
                setupScriptVersion = self._getScriptSetupVersionFromAsset(namespace)
                modelNull.PropertyList.Find(Core.STR_SETUP_VERSION).Data = setupScriptVersion

                filestate = Perforce.GetFileState(filePath)
                if filestate:
                    modelNull.PropertyList.Find(Core.STR_P4_VERSION, mobu.FBPropertyType.kFBPT_charptr).Data = str(
                        filestate.HaveRevision
                    )

                # Log that we created the reference correctly and return the
                # reference
                self._log.LogMessage("Successfully created new reference: {0}.".format(newReference.Namespace))

                newReferenceList.append(newReference)
                self._referenceDictionary[newReference.Namespace] = newReference

            except Exception as e:

                # log error and clean up the null and anything merged in so the scene
                # should be back to the state it was previously.
                deleteComponentList = []
                if modelNull is not None:
                    deleteComponentList.append(modelNull)
                if namespace is not None:
                    mergedComponentList = [
                        component for component in Globals.Components if Component.GetNamespace(component) == namespace
                    ]
                    deleteComponentList.extend(mergedComponentList)

                Component.Delete(deleteComponentList)
                self._log.LogError("Creation aborted - {0}".format(e.message))
                raise

            finally:
                if self.PluginsAvailable:
                    cdll.rexMBRage.SetLoadClipDataIntoUI_Py(True)

        # emit the callback signal for other widgets to use
        self.referenceAdded.emit(newReferenceList)
        return newReferenceList

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Swapping References")
    @Decorators.Optimize
    @Decorators.SuspendDynamicEvaluation()
    @Decorators.CleanScene
    def SwapReference(self, reference, theNewPath):
        """
        Swaps one reference out for to a new file path.
        """
        self._log.LogMessage("Begin Swap Reference...")

        # double check if the reference passed is a valid reference
        self._checkReference(reference)

        # create a variable to hold the original namespace
        originalNamespace = reference.Namespace

        # strip namespace from any poses found url:bugstar:4494564
        Namespace.RemoveNamespaceByType(reference.Namespace, componentType=mobu.FBCharacterPose)

        # normalise the new file path
        newPath = os.path.normpath(str(theNewPath))

        # get the file name with no extension
        fileName = Path.GetBaseNameNoExtension(newPath)
        if "-" in fileName:
            fileName = fileName.split("-")[0]

        # don't iterate on the namespace, when resolving, if the original namespace matches the
        # new filename
        if originalNamespace.lower() == fileName.lower():
            includeIterationBool = False
        else:
            includeIterationBool = True

        # resolve newNamespace
        newNamespace = self._resolveNamespace(fileName, includeIterationBool)

        # get reference type
        referenceType = self._resolveTypeByPath(newPath)
        # get main category for newRef
        newRefType = self._returnMainCategoryType(referenceType)
        # get main category for originalRef
        existingRefType = self._returnMainCategoryType(type(reference))

        # check if the categories match, we dont want to swap if not.
        if newRefType != existingRefType:
            self._log.LogError("Error can't swap references as they are different types.")
            return

        # check if character ref and if so, get face ref, if attached
        faceReference = self.CheckTargetFaceReference(reference)

        # rename geo and top parent null of CS version
        self._checkGeometryName(reference.Namespace)

        # Change path and version - push through as a major update
        reference._referencePathProperty.Data = newPath
        reference._p4VersionProperty.Data = "0"

        # change the name if the new and old references are different
        if newNamespace.lower() != originalNamespace.lower():
            reference._namespaceProperty.Data = newNamespace
            self.ChangeNamespace(reference, newNamespace)

        # update reference to complete the swap
        if self.PerforceSync:
            self._log.LogMessage("Synching latest version of [{}].".format(theNewPath))
            Perforce.Sync(theNewPath)

        # update reference
        reference._update(True)

        with ContextManagers.MergeTransactionActive():
            # update asset type (in case of character swaps between ig and cs)
            assetTypeProperty = reference.ModelNull.PropertyList.Find("rs_Asset_Type")
            if assetTypeProperty is not None:
                propertyTypeName = self._resolveTypeByPath(theNewPath).PropertyTypeName
                if propertyTypeName != assetTypeProperty.Data:
                    assetTypeProperty.Data = propertyTypeName
                    # reload dictionary so it has the new correct information
                    self._reload(force=True)
                    # refind the reference so it has the correct one to return
                    reference = self.GetReferenceByNamespace(newNamespace)

            # force update face reference and update the Target_Namespace property
            if faceReference is not None:
                # sync face file
                Perforce.Sync(faceReference.Path)
                # get file state
                filestate = Perforce.GetFileState(faceReference.Path)
                # need to update face reference, to ensure it is latest with latest updated char
                faceReference._update(quiet=True, filestate=filestate)
                self._log.LogMessage("Successfully updated face reference: [{0}]".format(faceReference.Namespace))
                if newNamespace != originalNamespace:
                    faceReference.SetTargetNamespace(newNamespace)
                    self._log.LogMessage(
                        "Successfully updated face reference ([{0}]) "
                        "Target_Namespace property to [{1}]".format(faceReference.Namespace, newNamespace)
                    )

            self._log.LogMessage("Swap Reference end.")

        return reference

    @Decorators.ToggleExpressionActiveStatus()
    @Decorators.ExceptionMessage
    @Decorators.ReferenceEditTransaction()
    @Decorators.Logger("Manager - Change Namespace")
    def ChangeNamespace(self, reference, newNamespace, includeIteration=False):
        """
        Changes the namespace of a single reference

        Arguments:
            reference (RS.Core.ReferenceSystem.Types.Base): object that represents a reference in the scene.
                                                            Also accepts strings for operations outside the reference
                                                            editor.
            newNamespace (string): new namespace to assign to the given reference
        """
        self._log.LogMessage("Begin Namespace Change...")
        if isinstance(reference, basestring):
            reference = self.GetReferenceByNamespace(reference)
        self._checkReference(reference)

        # resolve newNamespace - maintain namespace, dont include iteration
        newNamespace = self._resolveNamespace(newNamespace, includeIteration)

        oldNamespace = reference.Namespace

        # check if character ref and if so, get face ref, if attached
        faceReference = self.CheckTargetFaceReference(reference)

        # this is on run for specific reference types - if it doesnt fall under a specific type it will pass
        reference.ChangeLabelName(oldNamespace, newNamespace)

        # rename - includes rename of reference null & properties
        Namespace.Merge(newNamespace, [oldNamespace])
        Namespace.Rename(oldNamespace, newNamespace)
        reference.Namespace = newNamespace

        self._referenceDictionary.pop(oldNamespace, None)
        self._referenceDictionary[newNamespace] = reference

        # check handles for duplicated namespace
        reference.FixHandleDoubleNamespace()
        self._log.LogMessage("Namespace Change end.")

        # update the Target_Namespace property
        if faceReference is not None:
            faceReference.SetTargetNamespace(newNamespace)
            self._log.LogMessage(
                "Successfully updated face reference ([{0}]) Target_Namespace property to [{1}]".format(
                    faceReference.Namespace, newNamespace
                )
            )
        return newNamespace

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Change Path")
    def ChangePath(self, reference, newPath):
        """
        Changes the path of a single reference

        Arguments:
            reference (RS.Core.ReferenceSystem.Types.Base): object that represents a reference in the scene.
                                                            Also accepts strings for operations outside the reference
                                                            editor.
            newPath (string): new path to assign to the given reference
        """
        self._log.LogMessage("Begin Path Change...")

        # remove internal unused bind poses before making any changes, as they can cause crashes
        # url:bugstar:4455340
        unusedBindPoseList = []
        for namespace in Globals.Namespaces:
            unusedBindPoseList.extend(Cleanup.GetUnusedBindPoseList(namespace))
        Component.Delete(unusedBindPoseList)

        self._checkReference(reference)

        # rename path property
        pathProperty = reference.ModelNull.PropertyList.Find(Core.STR_REFERENCE_PATH)
        if pathProperty is not None:
            pathProperty.Data = newPath

        # update asset
        self.UpdateReferences(reference)

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Duplicating References")
    @Decorators.IsListEmpty()
    @Decorators.CleanScene
    def DuplicateReferences(self, referenceList):
        """
        Duplicates the passed in reference.
        """
        self._log.LogMessage("Begin Duplicating Reference...")
        referenceList = self._processReferences(referenceList)

        filePathList = [ref.Path for ref in list(referenceList)]

        self.CreateReferences(filePathList)
        self._log.LogMessage("Successfully created a duplicate reference.")

    @Decorators.Active
    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Deleting References")
    @Decorators.IsListEmpty()
    @Decorators.ToggleConstraintActiveStatus()
    @Decorators.ReferenceEditTransaction()
    def DeleteReferences(self, referenceList):
        """
        Deletes the passed in references.
        """
        referenceList = self._processReferences(referenceList)

        self._log.LogMessage("Deleting [{0}] reference(s)".format(len(referenceList)))
        deletedReferenceNamespaces = []

        # For reference in referenceList
        for reference in referenceList:

            # save the namespace
            refNamespace = reference.Namespace
            deletedReferenceNamespaces.append(refNamespace)

            # Call delete on reference (removes namespace from scene)
            # Delete the null object
            reference._delete()

            # Remove from dictionary
            self._referenceDictionary.pop(refNamespace, None)
            self._log.LogMessage("Successfully deleted reference: {0}".format(refNamespace))

        # emit the callback signal for other widgets to use
        self.referenceRemoved.emit(refNamespace)

    @Decorators.Active
    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Updating References")
    @Decorators.Optimize
    @Decorators.IsListEmpty()
    @Decorators.ToggleConstraintActiveStatus()
    @Decorators.SuspendDynamicEvaluation()
    @Decorators.CleanScene
    def UpdateReferences(self, referenceList, force=False, updateToLatest=True):
        """
        Updates the passed references. If the user passes force then an update will be
        preformed regardless of version number. In the case of characters this will also force a full
        update of the character even if the version is not marked as 'Major'.

        Arguments:
            referenceList (list): list of RS.Core.ReferenceSystem.Types.Base objects that represent a reference in the
                                  scene.
            force (bool): force the update even if the character is already the latest revision.
            updateToLatest (bool): Update each asset to latest version, otherwise update to version set
        """
        # Animation Exporter plugin loads UI values from the exported clips on disk. Disable this on
        # a reference update.
        if self.PluginsAvailable:
            cdll.rexMBRage.SetLoadClipDataIntoUI_Py(False)

        p4Sync = self.PerforceSync

        referenceList = self._processReferences(referenceList)

        if len(referenceList) == 0:
            return

        # Get moving cutscene objects associated with the references about to be updated
        movingCutscene = {}
        for reference in referenceList:
            for marker in Components.MovingCutscene.getMarkersByNamespace(reference):
                component = marker.component()
                movingCutscene[marker.constraint()] = component

        # Setup our log context
        self._log.LogMessage("Updating {0} reference(s) - Force:{1}".format(len(referenceList), str(force).upper()))

        previousVersionsList = [reference.CurrentVersion for reference in referenceList]
        if updateToLatest:
            for reference in referenceList:
                reference._setCurrentVersion(reference.LatestVersion)

        # Start our sync operation on the first reference.
        if p4Sync is True:
            # add current version to sync path
            currentSyncOperation = Perforce.SyncOperation(
                Collections.Peek(referenceList).Path, revisionList=[referenceList[-1].CurrentVersion], start=True)

        while Collections.HasItems(referenceList):
            # Get the current reference
            currentReference = Collections.Pop(referenceList)
            previousVersion = Collections.Pop(previousVersionsList)

            # check if the asset is tagged as RS_EXPORT_IGNORE
            hasExportIgnore = self.HasExportIgnore(currentReference)

            self._log.LogMessage("Updating reference - {0}".format(currentReference.Namespace))

            # Wait until the sync operation for this reference is complete.
            if p4Sync is True:
                currentSyncOperation.WaitForCompletion()

            # If there is another reference on the list then queue the sync
            # operation
            if Collections.HasItems(referenceList) and p4Sync is True:
                # add current version to sync path
                currentSyncOperation = Perforce.SyncOperation(Collections.Peek(referenceList).Path,
                                                              revisionList=[referenceList[-1].CurrentVersion],
                                                              start=True)

            # If we are not forcing check if the revision we have is the
            # same as the one we are currently on
            fileState = Perforce.GetFileState(currentReference.Path)
            if fileState == []:
                self._log.LogWarning("File {0} not in P4 - skipping reference.".format(currentReference.Path))
                continue

            if not force and fileState.HeadRevision == previousVersion:
                self._log.LogMessage("Skipping reference {0} as it is up to date.".format(currentReference.Namespace))
                self._log.LogMessage(
                    "Use the 'Force' option if you would like to push through the update and 'refresh' the reference."
                )
                continue

            else:
                # check if character ref and if so, get face ref, if attached
                faceReference = self.CheckTargetFaceReference(currentReference)
                # UserObject data is lost if we do not save before we merge
                # in other files
                self._saveExportSettings()

                currentReference._update(force, filestate=fileState)
                self._log.LogMessage("Successfully updated reference: [{0}]".format(currentReference.Namespace))

                if force is True and faceReference is not None:
                    # sync face file
                    Perforce.Sync(faceReference.Path)
                    # get file state
                    filestate = Perforce.GetFileState(faceReference.Path)
                    # need to update face reference, to ensure it is latest with latest updated char
                    faceReference._update(quiet=True, filestate=filestate)
                    self._log.LogMessage("Successfully updated face reference: [{0}]".format(faceReference.Namespace))

            # reapply RS_EXPORT_IGNORE tag if required
            if hasExportIgnore == True:
                self.SetExportIgnore(currentReference)

        for constraint, name in movingCutscene.iteritems():
            component = mobu.FBFindModelByLabelName(name)
            if not component:
                continue
            constraint.ReferenceAdd(0, component)

        if self.PluginsAvailable:
            cdll.rexMBRage.SetLoadClipDataIntoUI_Py(True)

        # emit the callback signal for other widgets to use
        self.referenceUpdated.emit(referenceList)

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Stripping References")
    @Decorators.ReferenceEditTransaction()
    @Decorators.IsListEmpty()
    @Decorators.CleanScene
    def StripReferences(self, referenceList):
        """
        Strips the passed in references of their non essential components.
        """
        referenceList = self._processReferences(referenceList)

        # Setup our log context
        self._log.LogMessage("Stripping [{0}] reference(s)".format(len(referenceList)))

        for reference in referenceList:
            try:
                self._log.LogMessage(
                    "Stripping reference [{0}] as [{1}]".format(reference.Namespace, type(reference).FormattedTypeName)
                )
                startTime = time.time()

                reference._strip()

                reference._setCurrentVersion(0)
                endTime = time.time()

                self._log.LogMessage(
                    "Successfully stripped reference. Took {0:.2f} seconds".format(endTime - startTime)
                )
            except Exception as e:
                self._log.LogError("Error stripping reference - {0}.".format(str(e)))

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Reloading Textures")
    @Decorators.IsListEmpty()
    def ReloadTextures(self, referenceList, force=False):
        """
        Syncs and calls a texture reload on all the references in the passed list.
        """
        p4Sync = self.PerforceSync
        if p4Sync is False:
            self._log.LogWarning("P4 Syncing is turned off so textures will not be synced")
            return

        referenceList = self._processReferences(referenceList)
        complexReferenceList = [reference for reference in referenceList if isinstance(reference, Complex)]

        if len(complexReferenceList) == 0:
            self._log.LogWarning("No valid references to reload texures on.")
            return
        else:
            self._log.LogMessage(
                "Syncing and reloading textures for {0} reference(s)".format(len(complexReferenceList))
            )

        # Gather up a list of unique texture paths
        textureFilePathSet = set()
        for complexReference in complexReferenceList:

            # Collects the file paths of the textures
            namespace = Globals.Scene.NamespaceGet(complexReference.Namespace)
            # Moving from TGA to Tifs, this code checks for both types of
            # texture extension
            for textureComponent in Namespace.GetContentsByType(namespace, mobu.FBTexture):

                textureFilePath = None

                # Draw filepath from the video
                if textureComponent.Video is not None:
                    textureFilePath = textureComponent.Video.Filename

                # Draw filepath from the backup property - this gets added during strip
                elif textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH) is not None:
                    textureFilePath = textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH).Data

                if textureFilePath is not None:
                    if "." not in textureFilePath:
                        self._log.LogWarning("Texture file path is invalid: '{0}'".format(textureFilePath))
                    else:
                        textureFilePathSet.add(textureFilePath)

                        # Also add a .tiff - new version of textures that we want to move to
                        textureDirectoryPath, textureFileName = os.path.split(textureFilePath)
                        textureFileName, textureFileExtension = os.path.splitext(textureFileName)
                        if textureFileExtension == ".tga":
                            textureFilePathSet.add(
                                os.path.join(textureDirectoryPath, "{0}.tif".format(textureFileName))
                            )

        # Exit script if no textures have been found
        if len(textureFilePathSet) == 0:
            self._log.LogMessage("No texture filepaths found to sync/reload")
        else:
            # Pass this to P4 in one big batch for syncing
            self._log.LogMessage("Syncing {0} texture file(s)".format(len(textureFilePathSet)))
            Perforce.Sync(list(textureFilePathSet), force)

        # Now reload the textures
        self._log.LogMessage("Starting refresh of textures...")
        for complexReference in complexReferenceList:
            complexReference._reloadTextures()

        self._log.LogMessage("Finished refresh of textures.")

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Stripping Textures")
    @Decorators.IsListEmpty()
    @Decorators.CleanScene
    def StripTextures(self, referenceList):
        """
        Actually strips the textures off the models for each of the passed in references.
        """
        referenceList = self._processReferences(referenceList)
        complexReferenceList = [reference for reference in referenceList if isinstance(reference, Complex)]

        # Strip textures
        for complexReference in complexReferenceList:
            self._log.LogMessage("Starting strip of textures for Reference: {0}...".format(complexReference))
            complexReference._stripTextures()
            self._log.LogMessage("Finished strip of textures.")

    def CheckTargetFaceReference(self, reference):
        """
        Checking if there is a face reference attached to the passed character reference

        Arguments:
            reference (RS.Core.ReferenceSystem.Types.Character.Character): Character Reference

        Return:
            Face Reference (RS.Core.ReferenceSystem.Types.Face.Face)
        """
        # check if the reference is a character
        refType = self._resolveTypeByPath(reference.Path)
        refMainCat = self._returnMainCategoryType(refType)
        if refMainCat is not Character:
            return

        # check for face references and their target namespace for a match
        faceRefList = self.GetReferenceListByType(Face)

        faceReferenceList = []

        for faceRef in faceRefList:
            # get all face references which are attached to the referenced character
            if faceRef.TargetNamespace == reference.Namespace:
                faceReferenceList.append(faceRef)

        if len(faceReferenceList) == 0:
            # no face references are attached to this character
            return

        if len(faceReferenceList) > 1:
            # too many face references are attached to this character
            self._log.LogWarning(
                "There are too many Face References attached to one Character Reference.\n "
                "Aborting Face Update. Please clean scene of excess Face References."
            )
            return

        return faceReferenceList[0]

    def SaveSceneReferences(self):
        referenceList = self.GetReferenceListAll()

        if len(referenceList) == 0:
            return

        pathList = []
        # find properties and add refs to a dict 'RefName:RefPath'
        for refNull in referenceList:
            refPathProperty = refNull.Path
            assetTypeProperty = refNull.PropertyTypeName

            if all([assetTypeProperty, refPathProperty]) and assetTypeProperty not in ["3Lateral", "Face"]:
                pathList.append(refPathProperty)

        # save dict to a txt file
        fileNameSplit = Globals.Application.FBXFileName.split(".")
        txtPath = "{0}.txt".format(fileNameSplit[0])
        txtFile = open(txtPath, "w")
        for path in pathList:
            txtFile.write(path)
            txtFile.write("\n")
        txtFile.close()
        return txtPath

    @property
    def PerforceSync(self):
        """
        Check if the user has P4 syncing turned on or off
        Returns:
            Boolean (True = On, False = Off)
        """
        return self._p4SyncStatus

    @PerforceSync.setter
    @Decorators.ExceptionMessage
    @Decorators.Logger("PerforceSync - Setting P4 Sync Value (On/Off)")
    def PerforceSync(self, value):
        """
        sets P4 syncing to be turned on, or off, depending on the arg value passed
        """
        self._log.LogMessage("Setting P4 Sync Value to {0}".format(value))
        self._p4SyncStatus = value

    @property
    def IgnoreProject(self):
        """ Is the reference editor ignoring projects """
        return Settings.IgnoreProject

    @IgnoreProject.setter
    def IgnoreProject(self, value):
        """ Set the reference editor to ignore projects """
        Settings.IgnoreProject = value

    @property
    def Silent(self):
        """ Is the reference editor not bringing up warnings"""
        return Globals.System.SuspendMessageBoxes or Settings.Silent

    @Silent.setter
    def Silent(self, value):
        """ Set the reference editor to not bringing up warnings"""
        Settings.Silent = value

    @property
    def Optimize(self):
        """ Use the merge & prevent ui update transactions when using the update operation """
        return Settings.Optimize

    @Optimize.setter
    def Optimize(self, value):
        """ Set to use the merge & prevent ui update transactions when using the update operation """
        Settings.Optimize = value

    @property
    def Debug(self):
        """
        Is the references editor in debug mode
        """
        return Settings.Debug

    @Debug.setter
    def Debug(self, value):
        """
        Set the reference editor to debug mode
        """
        Settings.Debug = value

    @property
    def RaiseErrors(self):
        """ Raises all errors even if we are expecting them """
        return Settings.RaiseErrors

    @RaiseErrors.setter
    def RaiseErrors(self, value):
        """ Set whether we want to raise all errors """
        Settings.RaiseErrors = value

    @property
    def TrialID(self):
        """
        Check the current value of the trial id
        Returns:
            Int
        """
        # get the trialid value from the dict cache, if it exists
        trialID = self.trialIDDict.get(Globals.System.CurrentTake)
        if trialID is not None:
            return int(trialID)

        rootNull = self._getRootNull(createIfNotFound=True)

        # get trialid property, or create it new if it doesnt exist
        trialIDProperty = rootNull.PropertyList.Find(Core.STR_TRIAL_ID)
        if trialIDProperty is None:
            trialIDProperty = rootNull.PropertyCreate(
                Core.STR_TRIAL_ID, mobu.FBPropertyType.kFBPT_int, "Number", True, True, None
            )
            trialIDProperty.SetLocked(True)

        if trialIDProperty.IsAnimated():
            zeroFrame = mobu.FBTime(0)
            # get key property value at frame 0
            trialIDProperty.SetAnimated(True)
            fCurveKeys = trialIDProperty.GetAnimationNode().FCurve.Keys
            if len(fCurveKeys) == 0:
                # The property might not have been set correctly, re-apply the Trial ID to make sure its correctly applied
                trialIDValue = trialIDProperty.Data
                self.TrialID = trialIDValue
                return int(trialIDValue)

            for key in fCurveKeys:
                if key.Time == zeroFrame:
                    return int(key.Value)
        return 0

    @TrialID.setter
    @Decorators.ExceptionMessage
    @Decorators.Logger("Setting Trial Value (int)")
    def TrialID(self, value):
        """
        sets value, depending on the arg value passed
        """
        self._log.LogMessage("Setting Trial ID Value to {0}".format(value))

        rootNull = self._getRootNull(createIfNotFound=True)

        # get trialid property, or create it if it doesnt exist
        trialIDProperty = rootNull.PropertyList.Find(Core.STR_TRIAL_ID)
        if trialIDProperty is None:
            trialIDProperty = rootNull.PropertyCreate(
                Core.STR_TRIAL_ID, mobu.FBPropertyType.kFBPT_int, "Number", True, True, None
            )

        # unlock property
        trialIDProperty.SetLocked(False)

        # set trial id property
        trialIDProperty.Data = value

        # get fcurve
        trialIDProperty.SetAnimated(True)
        fCurve = trialIDProperty.GetAnimationNode().FCurve
        # clear keys
        fCurve.EditClear()
        # set key property at frame 0
        fCurve.KeyAdd(mobu.FBTime(0, 0, 0, 0), value)
        # lock property
        trialIDProperty.SetLocked(True)

        # update dictionary
        self.trialIDDict[mobu.FBSystem().CurrentTake] = value

    # DEPRECATED METHODS
    @Deprecated
    def SetDebug(self, value):
        """
        Set the reference editor to debug mode

        Arguments:
            value (bool): to activate debug mode True or not False
        """
        Settings.Debug = value

    @Deprecated
    def SetSilent(self, value):
        """
        Sets the manager to be in silent mode - prevents any message boxes appearing.

        Arguments:
            value (bool): to ignore message boxes (True) or not (False)
        """
        Settings.Silent = value

    @Deprecated
    def SetIgnoreProject(self, value):
        """
        Sets whether the manager should ignore the current project when looking for references in the scene

        Arguments:
            value (bool): to ignore the project (True) or not (False)
        """
        Settings.Settings.IgnoreProject = value

    @property
    def PluginsAvailable(self):
        """ Are plugins available to be used """
        return False

    def findRsExportIgnoreProperty(self, reference):
        """

        :return: custom property if found, returns None if not
        """
        return reference.Mover.PropertyList.Find("RS_EXPORT_IGNORE")

    def SetExportIgnore(self, reference):
        """
        Removes original material, textures and videos from an asset, replacing with a yellow
        material. Also adds a custom property to the mover. This is all to indicate the asset should
        not be exported - tools also have support for this, looking for the custom property we add.
        url:bugstar:4859311
        """
        # check if mover exists
        if reference.Mover is None:
            return

        # check if export material exists already
        resultList = filter(
            lambda component: component.LongName == "{0}:RS_EXPORT_IGNORE_Material".format(reference.Namespace),
            Globals.Materials,
        )
        # filter only needs to return 1 item in the list, which will be the material we are looking for
        materialFound = len(resultList) > 0

        # check if 'RS_EXPORT_IGNORE' property exists already, if not, create it
        if self.findRsExportIgnoreProperty(reference) is None:
            # create property
            reference.Mover.PropertyCreate("RS_EXPORT_IGNORE", mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)

        # create material if it was not found
        if materialFound == False:
            # get fbnamespace and its contents
            fbNamespace = Namespace.GetFBNamespace(reference.Namespace)

            # delete existing materials/textures/video
            materialList = Namespace.GetContentsByType(fbNamespace, objectType=mobu.FBMaterial, exactType=True)
            textureList = Namespace.GetContentsByType(fbNamespace, objectType=mobu.FBTexture, exactType=True)
            videoList = Namespace.GetContentsByType(fbNamespace, objectType=mobu.FBVideoClip, exactType=False)

            materialList.extend(textureList)
            materialList.extend(videoList)

            for material in materialList:
                material.FBDelete()

            # find folder for materials
            matFolder = 0
            for folder in Globals.Folders:
                if folder.LongName == "{0}:Materials".format(reference.Namespace):
                    matFolder = folder
                    break

            # create new materials and apply to all multi-regions of the object
            geoObjectList = []
            modelList = Namespace.GetContentsByType(fbNamespace, objectType=mobu.FBModel, exactType=False)
            for model in modelList:
                if isinstance(model, mobu.FBModel) and (
                    model.Name == "Geometry" or model.Name == reference.Namespace.split("^")[0] or "_skin" in model.Name
                ):
                    geoObjectList.append(model)
                    if len(model.Children) > 0:
                        geoObjectList.extend(model.Children)

            for geoObject in geoObjectList:
                # get geometry objectridiculous
                geometry = geoObject.Geometry
                if geometry:
                    # get number of regions
                    geometryRegionList = set(geometry.GetMaterialIndexArray())
                    numberRegions = 1
                    if len(geometryRegionList) > 0:
                        numberRegions = max(geometryRegionList) + 1

                    # create 1 material per region
                    for idx in xrange(numberRegions):
                        ignoreMaterial = mobu.FBMaterial("{0}:RS_EXPORT_IGNORE_Material_0".format(reference.Namespace))
                        ignoreMaterial.Diffuse = mobu.FBColor(1, 1, 0)
                        ignoreMaterial.Emissive = mobu.FBColor(0.2, 0.2, 0.2)
                        geoObject.Materials.append(ignoreMaterial)

                        # add material to a folder
                        if matFolder is not None:
                            folder.ConnectSrc(ignoreMaterial)

    def RemoveExportIgnore(self, reference):
        """
        removes export property & reloads asset to set it back to its original exportable state
        :returns: boolean
        """
        # check if mover exists
        if reference.Mover is None:
            return False

        # check if 'RS_EXPORT_IGNORE' property exists and remove it
        ignoreProperty = self.findRsExportIgnoreProperty(reference)
        if ignoreProperty is None:
            return False

        reference.Mover.PropertyRemove(ignoreProperty)

        return True

    def HasExportIgnore(self, reference):
        """
        check if the asset is tagged for RS_EXPORT_IGNORE
        :returns: boolean
        """
        # check if mover exists, if not run the mover grab function
        if reference.Mover is None:
            return False

        # check if 'RS_EXPORT_IGNORE' property exists
        if self.findRsExportIgnoreProperty(reference) is not None:
            return True

        return False

    def IsActive(self):
        """
        Is a reference system operation currently being processed

        Returns:
            bool
        """
        return self._active
