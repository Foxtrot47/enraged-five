import pyfbsdk as mobu

from PySide import QtGui

import fbx
import FbxCommon

from RS import Globals
from RS.Utils import Scene
from RS.Utils.Scene import Component, Constraint
from RS.Core.ReferenceSystem.Types.Complex import Complex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapComplex(Complex):
    """
    Sub-class of Complex
    Contains additional methods required by mocap types, but not other types.
    """

    @Logger("Mocap Complex - Align to Active Stage")
    def AlignReferenceRootToActiveStage(self):
        """
        Gets the top most parent node of the FBModels associated with the reference.
        Aligns these nodes with the active stage.
        If there is no active stage, there is no change.
        """
        self._log.LogMessage("Aligning root of reference to active stage.")
        rootModelList = []
        activeStage = None
        # get namespace to search for models
        namespace = self.Namespace
        # make a list of root models that match the reference namespace
        sceneRoot = mobu.FBSystem().Scene.RootModel
        for child in sceneRoot.Children:
            childNamespace = child.LongName.split(":")[0]
            stagesProperty = child.PropertyList.Find("Active Stage")
            if childNamespace == namespace:
                # get matching root objects to reference namespace
                rootModelList.append(child)
            elif stagesProperty is not None:
                # get active stage
                if stagesProperty.Data == True:
                    activeStage = child

        # if there is no active stage, no need to move the assets
        if activeStage is None:
            return

        # align top nodes to active stage
        for model in rootModelList:
            Scene.AlignTranslation(model, activeStage, True, True, True)

    @Logger("Mocap Complex - Change Label Name")
    def ChangeLabelName(self, oldNamespace, newNamespace):
        """
        Reimplemented
        We need to make sure the handle Label property also changes with the namespace
        """
        self._log.LogMessage("Changing label name of a handle")
        handleComponents = [handle for handle in Globals.Handles if Component.GetNamespace(handle) == oldNamespace]
        for handle in handleComponents:
            handle.PropertyList.Find("Label").Data = newNamespace

    @Logger("Mocap Complex - Apply Color")
    def ApplyColor(self, userColor):
        """
        changes the color for all relevant components for a selected reference
        """
        self._log.LogMessage("Changing texture color.")
        # get color
        if userColor is not None:
            # get stage object
            namespace = self._modelNull.Name
            modelList = Component.GetComponents(namespace, TypeList=[mobu.FBModel], DerivedTypes=False)
            if len(modelList) > 0:
                for model in modelList:
                    # get stage material
                    if len(model.Materials) > 0:
                        # apply color
                        model.Materials[0].Diffuse = userColor

            # get handle
            handleList = Component.GetComponents(namespace, TypeList=[mobu.FBHandle], DerivedTypes=False)
            if len(handleList) > 0:
                handleList[0].PropertyList.Find("2D Display Color").Data = userColor

    @Logger("Mocap Complex - Update Groups")
    def UpdateGroups(self):
        """
        After a mocap reference creation we want to make sure the asset is assigned to the correct
        group
        """
        self._log.LogMessage("Updating mocap groups.")
        # reference's namespace
        referenceNamespace = self._modelNull.Name
        # get FBGroup components related to the reference
        referenceGroupList = [
            component
            for component in Globals.Components
            if component.LongName.startswith(referenceNamespace) and isinstance(component, mobu.FBGroup)
        ]
        # get the main mocap groups
        mainMocapGroupList = [
            component
            for component in Globals.Components
            if component.LongName == "MOCAP" and isinstance(component, mobu.FBGroup)
        ]
        snapAllGroupList = [
            component
            for component in Globals.Components
            if component.LongName == "SNAP ALL" and isinstance(component, mobu.FBGroup)
        ]

        # get main mocap group and check if the same groups in the selected reference groups already
        # exist in the main mocap group
        deleteGroupList = []
        if len(mainMocapGroupList) == 1:
            mainMocapGroup = mainMocapGroupList[0]
            # go through reference groups and match to an existing mocap group
            for referenceGroup in referenceGroupList:
                matchedRefGroup = None
                for mocapGroup in mainMocapGroup.Items:
                    if isinstance(mocapGroup, mobu.FBGroup):
                        # if they have the same name they are a match
                        if mocapGroup.Name == referenceGroup.Name:
                            matchedRefGroup = mocapGroup
                            break
                # if it is a match we want to copy the items from the ref group to the mocap group
                if matchedRefGroup is not None:
                    for refItem in referenceGroup.Items:
                        if not isinstance(refItem, mobu.FBGroup):
                            matchedRefGroup.ConnectSrc(refItem)
                    # delete group as it is no longer needed
                    referenceGroup.FBDelete()
                else:
                    # add unique group to the main mocap group
                    mainMocapGroup.ConnectSrc(referenceGroup)
                    # add property
                    tag = referenceGroup.PropertyCreate(
                        "Mocap Group Cleanup", mobu.FBPropertyType.kFBPT_charptr, "", False, True, None
                    )
                    tag.Data = referenceGroup.Name
                    # strip namespace
                    referenceGroup.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBRemoveAllNamespace, "", "")
            # update snap all group with the same items as in snap mocap group
            # (previz user will update the snap all group manually as they go, which is why there
            # are two groups with the same items to begin with)
            snapMocapGroupList = [
                component
                for component in Globals.Components
                if component.LongName == "SNAP MOCAP" and isinstance(component, mobu.FBGroup)
            ]
            if len(snapAllGroupList) == 1 and len(snapMocapGroupList) == 1:
                snapAllGroup = snapAllGroupList[0]
                snapMocapGroup = snapMocapGroupList[0]
                for item in snapMocapGroup.Items:
                    if not isinstance(item, mobu.FBGroup):
                        snapAllGroup.ConnectSrc(item)
        else:
            # create the main mocap group as it doesnt exist
            mainMocapGroup = mobu.FBGroup("MOCAP")
            # also need to create a few other groups
            moveAllGroup = mobu.FBGroup("MOVE ALL")
            snapAllGroup = mobu.FBGroup("SNAP ALL")
            charactersGroup = mobu.FBGroup("CHARACTERS")
            # add property tag
            for group in [moveAllGroup, snapAllGroup, charactersGroup]:
                tag = group.PropertyCreate(
                    "Mocap Group Cleanup", mobu.FBPropertyType.kFBPT_charptr, "", False, True, None
                )
                tag.Data = group.Name
            # connect groups
            moveAllGroup.ConnectSrc(snapAllGroup)
            snapAllGroup.ConnectSrc(charactersGroup)

            # assign reference groups to the main mocap group
            for referenceGroup in referenceGroupList:
                mainMocapGroup.ConnectSrc(referenceGroup)
                # add property tag
                tag = referenceGroup.PropertyCreate(
                    "Mocap Group Cleanup", mobu.FBPropertyType.kFBPT_charptr, "", False, True, None
                )
                tag.Data = referenceGroup.Name
                # strip namespace
                referenceGroup.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBRemoveAllNamespace, "", "")
            # update snap all group with the same items as in snap mocap group
            # (previz user will update the snap all group manually as they go, which is why there
            # are two groups with the same items to begin with)
            snapMocapGroupList = [
                component
                for component in Globals.Components
                if component.LongName == "SNAP MOCAP" and isinstance(component, mobu.FBGroup)
            ]

            if len(snapMocapGroupList) == 1:
                snapMocapGroup = snapMocapGroupList[0]
                for item in snapMocapGroup.Items:
                    if not isinstance(item, mobu.FBGroup):
                        snapAllGroup.ConnectSrc(item)

        # update/create constraints
        self.SnapMocapConstraints()

    @Logger("Mocap Complex - Snap Mocap Constraints")
    def SnapMocapConstraints(self):
        """
        Once the SNAP MOCAP Group has been update with 'Update Groups' method, we want to ensure the
        items all have constraints and are matched to the current active stage.

        url:bugstar:2599374
        """
        self._log.LogMessage("Creating 'snap mocap' constraints")

        # get active stage
        activeStage = self.GetActiveStage()
        if activeStage is None:
            return

        # get snap mocap group
        snapMocapGroupList = [
            component
            for component in Globals.Components
            if component.LongName == "SNAP MOCAP" and isinstance(component, mobu.FBGroup)
        ]
        if len(snapMocapGroupList) != 1:
            return

        snapMocapGroup = snapMocapGroupList[0]
        for item in snapMocapGroup.Items:
            # look for activestage constraint
            itemList = [
                item.GetSrc(srcIdx)
                for srcIdx in range(item.GetSrcCount())
                if isinstance(item.GetSrc(srcIdx), mobu.FBConstraint)
                and item.GetSrc(srcIdx).LongName == "{0}:ActiveStage_ParentChild".format(Component.GetNamespace(item))
            ]

            # constraint found
            if len(itemList) == 1:
                # get active stage
                parentChildConstraint = itemList[0]
                # add currently active stage to parent slot
                oldParentObject = parentChildConstraint.ReferenceGet(1)
                parentChildConstraint.ReferenceRemove(1, oldParentObject)
                parentChildConstraint.ReferenceAdd(1, activeStage)
                # align items to active stage
                Scene.Align(item, activeStage, True, True, True, True, True, True)

            # constraint not found
            else:
                # create constraint
                parentChildConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
                parentChildConstraint.Name = "{0}:ActiveStage_ParentChild".format(Component.GetNamespace(item))
                parentChildConstraint.ReferenceAdd(0, item)
                parentChildConstraint.ReferenceAdd(1, activeStage)
                Scene.Align(item, activeStage, True, True, True, True, True, True)
                parentChildConstraint.Snap()

                # add to folder
                activeStageFolderList = [
                    folder for folder in Globals.Folders if folder.Name == "BasePreviz_ActiveStage"
                ]
                if len(activeStageFolderList) == 1:
                    activeStageFolder = activeStageFolderList[0]
                    activeStageFolder.ConnectSrc(parentChildConstraint)
                else:
                    activeStageFolder = mobu.FBFolder("BasePreviz_ActiveStage", parentChildConstraint)

    @Logger("Mocap Complex - Get Active Stage")
    def GetActiveStage(self):
        """
        Returns:
            FBModel - Currently active stage
        """
        self._log.LogMessage("Get active stage.")

        stageList = [
            model
            for model in Globals.Scene.RootModel.Children
            if model.PropertyList.Find("Active Stage") and model.PropertyList.Find("Active Stage").Data is True
        ]
        return None if len(stageList) != 1 else stageList[0]

    def sdkSetName(self, node):
        """
        if a node has a LongName in the source file, it will be replaced with just the name,
        no namespace. Using fbxsdk.

        Args:
        node: FBModel
        """
        nodeName = node.GetName()
        split = nodeName.split(":")
        node.SetName(split[-1])

    def StripSourceFileNamespace(self):
        """
        checks the source file for any namespaces present, and replaces it with just the name.
        Used before a merge takes place.
        """
        # temp path
        tempPath = "x:\\toDelete.fbx"

        # First we will look in the fbx file
        sdkManager, scene = FbxCommon.InitializeSdkObjects()
        try:
            self._log.LogMessage("Analyzing resource FBX file for any occurance of a namespace existing already.")

            FbxCommon.LoadScene(sdkManager, scene, self.Path)

            for idx in range(scene.GetNodeCount()):
                node = scene.GetNode(idx)
                self.sdkSetName(node)
            for idx in range(scene.GetCharacterCount()):
                character = scene.GetCharacter(idx)
                self.sdkSetName(character)
            for idx in range(scene.GetMaterialCount()):
                material = scene.GetMaterial(idx)
                self.sdkSetName(material)
            for idx in range(scene.GetTextureCount()):
                texture = scene.GetTexture(idx)
                self.sdkSetName(texture)

            # save file in a temp location
            FbxCommon.SaveScene(sdkManager, scene, tempPath)
        except:
            raise
        finally:
            # Makes sure we clean up
            sdkManager.Destroy()

        return tempPath
