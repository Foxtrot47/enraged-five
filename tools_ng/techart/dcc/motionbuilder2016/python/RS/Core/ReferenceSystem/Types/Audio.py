'''
Usage:
    This contains logic to help interaction with AUDIO references
Author:
    Ross George
    Bianca Rudareanu
'''
import os

import unbind

import pyfbsdk as mobu

from RS.Core.ReferenceSystem.Types.Complex import Complex
from RS.Core.ReferenceSystem.Decorators import Logger
from RS import Config, Globals, Perforce
from RS.Utils import Namespace, Scene
from RS.Utils.Scene import Component


class Audio(Complex):
    '''
    Represents a prop reference in a scene
    '''
    PropertyTypeName    = 'Audio'
    FormattedTypeName   = 'Audio'
    TypePathList        = [os.path.join('art', 'animation', 'resources', 'audio'),
                           os.path.join('art', 'dev','animation', 'resources', 'audio'),
                           os.path.join(Config.VirtualProduction.Path.Previz, "Global", "Audio"),
                           os.path.join('audio', 'dev', 'assets', 'waves'), # url:bugstar:3855272
                           os.path.join('art', 'animation', 'ingame', 'Renders') #url:bugstar:3676014
                           ]
    FileType            = 'wav'

    def __init__(self, modelNull, log):
        '''
        Initialises the CHARACTER reference with the passed model null. Will call the base initializor as well.
        '''
        # Call base class constructor
        super(Audio, self).__init__(modelNull, log)
        self.audio = None

    def _create(self, filestate=None, importOptions=None):
        '''
        Creates the audio asset
        '''
        self._update(filestate=filestate)

    @Logger("Audio - Updating")
    def _update(self, force=True, filestate=None):
        '''
        Updates the prop reference with latest data. Force does nothing here currently.
        '''
        self._log.LogMessage("Updating Audio: {0}".format(self.Namespace))

        audioPath = self.Path

        if os.path.exists(audioPath):
            audioDeleteList = []
            # check if audio exists already, delete it if it does, so we can add it back in
            namespace = Globals.Scene.NamespaceGet(self.Namespace)

            for audio in Namespace.GetContentsByType(namespace, mobu.FBAudioClip):
                audioDeleteList.append(audio)

            # remove excess audio linked to the same path as the ref audio
            for audioClip in Globals.AudioClips:
                if audioClip.Path == audioPath:
                    audioDeleteList.append(audioClip)

            for audioDelete in audioDeleteList:
                try:
                    audioDelete.FBDelete()
                except unbind.UnboundWrapperError:
                    # audio has already been removed
                    continue

            # occasionally mobu will hold a reference to the audio, even after deleted, which will
            # prevent us to update it. We need to force a refresh (merge in a blank file) to remove
            # this reference and allow us to sync to it in perforce
            Scene.ForceFullSceneRefresh()

            # get latest on audio file
            Perforce.Sync(audioPath)

            # add audio back in
            audioClip = mobu.FBAudioClip(audioPath)
            audioClip.LongName = '{0}:{1}'.format(self.Namespace, audioClip.Name)
            audioClip.DstIn = mobu.FBTime(0, 0, 0, 0)
            audioClip.PropertyList.Find("AccessMode").Data = 1

        filestate = filestate or Perforce.GetFileState(audioPath)
        # Update Reference P4 Version
        if not isinstance(filestate, list):
            self._p4VersionProperty.Data = str(filestate.HeadRevision)

    def getAudioClip(self):
        """ return audioclip for passed reference """
        if self.audio is None or Component.IsDestroyed(self.audio):
            namespace = Globals.Scene.NamespaceGet(self.Namespace)
            for audio in Namespace.GetContentsByType(namespace, mobu.FBAudioClip):
                self.audio = audio
                return self.audio
            self.audio = None
        return self.audio
