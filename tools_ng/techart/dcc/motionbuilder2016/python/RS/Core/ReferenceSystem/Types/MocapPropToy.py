import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapPropToy(MocapComplex):
    """
    Represents a mocap proptoy reference in a scene
    """
    studio = ()
    PropertyTypeName = "MocapPropToy"
    FormattedTypeName = "Mocap PropToy"
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz, "Global", "PropToys"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Global", "Props"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Global", "PropClones"),]
    FileType = "fbx"

    @Logger("Mocap Prop Toy - Create")
    def _create(self, filestate=None, importOptions=None):
        """
        This function gets run only when a proptoy is created
        """
        super(MocapPropToy, self)._create(filestate=filestate, importOptions=None)

        # align assets to active stage
        self._log.LogMessage("Aligning proptoy to active stage.")
        self.AlignReferenceRootToActiveStage()

        if self.Path.startswith(os.path.join(Config.VirtualProduction.Path.Previz, "Global", "Lasers")):
            self.UpdateGroups()
