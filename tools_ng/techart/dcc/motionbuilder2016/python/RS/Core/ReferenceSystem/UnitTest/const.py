import os


class ConfigPathDict(object):
    """Yaml config paths for unittests"""

    REFERENCES_CONFIG_PATH = os.path.join(os.path.dirname(__file__), "configs", "references.yaml")
    SCENES_CONFIG_PATH = os.path.join(os.path.dirname(__file__), "configs", "scenes.yaml")
