import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex


class MocapVirtualCamera(MocapComplex):
    """
    Represents a mocap virtual camera reference in a scene
    """

    studio = ()
    PropertyTypeName = "Mocap VirtualCamera"
    FormattedTypeName = "Mocap VirtualCamera"
    TypePathList = (
        os.path.join(Config.VirtualProduction.Path.Previz, "Global", "VirutalCameras"),
        os.path.join(Config.VirtualProduction.Path.Previz, "Global", "VirtualCameras"),  # in case typo gets fixed
    )
    FileType = "fbx"

    def __init__(self, modelNull, log):
        """
        Call base class constructor
        """
        super(MocapVirtualCamera, self).__init__(modelNull, log)
