import os

from RS.Core.ReferenceSystem.Types.Complex import Complex


class CharacterHelper(Complex):
    '''
    Represents a character helper reference in a scene
    '''
    studio = ()
    PropertyTypeName = 'CharacterHelper'
    FormattedTypeName = 'Character Helper'
    TypePathList = [os.path.join('art', 'animation', 'resources', 'characters', 'characterHelpers'),
                    os.path.join('art', 'dev', 'animation', 'resources', 'characters', 'characterHelpers'),
                    ]
    FileType = 'fbx'