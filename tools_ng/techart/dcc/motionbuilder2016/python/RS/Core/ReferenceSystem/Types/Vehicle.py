
'''
Usage:
    This contains logic to help interaction with VEHICLE references
Author:
    Ross George
    Bianca Rudareanu
'''
import os

from RS.Core.ReferenceSystem.Types.Complex import Complex


class Vehicle( Complex ) :
    '''
    Represents a vehicle reference in a scene
    ''' 

    PropertyTypeName    = 'Vehicles'
    FormattedTypeName   = 'Vehicle'
    TypePathList        = [os.path.join('art', 'animation', 'resources', 'vehicles'),
                           os.path.join('art', 'dev','animation', 'resources', 'vehicles'),
                           ]
    FileType            = 'fbx'

