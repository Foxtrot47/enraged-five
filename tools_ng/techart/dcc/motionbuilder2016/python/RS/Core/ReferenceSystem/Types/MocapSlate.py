import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapSlate(MocapComplex):
    """
    Represents a mocap slate reference in a scene
    """
    studio = ()
    PropertyTypeName = "MocapSlate"
    FormattedTypeName = "Mocap Slate"
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz, "Global", "Slate"),]
    FileType = "fbx"

    @Logger("Mocap Slate - Create")
    def _create(self, filestate=None, importOptions=None):
        """
        This function is run when a slate is created

        Args:
            importOptions (FaceAnimDataImportOptions, optional): face import options.
        """
        super(MocapSlate, self)._create(filestate=None)

        # update groups
        self._log.LogMessage("Updating base previz groups with new slate reference created.")
        self.UpdateGroups()
