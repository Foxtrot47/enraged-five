import pyfbsdk as mobu

from RS import Globals
from RS.Utils import Scene
from RS.Utils import Path
from RS.Utils import Namespace
from RS.Utils.Scene import RelationshipConstraintManager as relationConstraintManager


class StrapComponents(object):
    PRESERVE_NAME_SPACE = 'weaponstrap_restore'

    NODE_ATTRIBUTES = ('nodePropertyName',
                       'rigComponents')

    EXCLUDE_SET = ('dummy',
                   'ConstraintDistribution',
                   #'chainComponents',
                   'ikSplineRoot',
                   'strapRelationConstraint',
                   'strapConstraintFolder',
                   'handleConstraints',
                   'serialConstraint')

    def __init__(self,
                 property,
                 strapNamespace):
        self.restoreNamespace = None

        self.nodePropertyName = property.Name

        self.parentAnchors = []

        self.rigComponents = self.collectComponents(property,
                                                    strapNamespace)

    def collectComponents(self,
                          property,
                          strapNamespace):
        if property.Name in self.EXCLUDE_SET:
            return []

        groupMembersCount = property.GetSrcCount()
        if groupMembersCount == 0:
            return []

        groupData = []

        for index in xrange(groupMembersCount):
            currentNode = property.GetSrc(index)

            groupData.append(currentNode.FullName)

            namespaceObject = currentNode.OwnerNamespace
            if namespaceObject is None:
                continue

            if str(namespaceObject.Name) != strapNamespace:
                continue

            currentNode.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBReplaceNamespace, 
                                               strapNamespace, 
                                               self.PRESERVE_NAME_SPACE)

            self.restoreNamespace = currentNode.OwnerNamespace.Name

            groupData[-1] = currentNode.FullName

            if isinstance(currentNode, mobu.FBModel):
                self.parentAnchors.append(currentNode.Parent.FullName)

        return groupData

    def connectMetaData(self,
                        inputObject,
                        objectProperty):
        inputObject.ConnectDst(objectProperty)

    def restore(self,
                inputNode,
                strapFolder,
                strapNamespace):
        targetProperty = inputNode.PropertyList.Find(self.nodePropertyName)

        if targetProperty is None:
            targetProperty = inputNode.PropertyCreate(self.nodePropertyName, 
                                                      mobu.FBPropertyType.kFBPT_object, 
                                                      'Object', False, True, None)

        for index, nodeName in enumerate(self.rigComponents):
            targetNode = mobu.FBFindObjectByFullName(nodeName)

            if targetNode is None:
                print 'Missing', nodeName
                continue

            self.connectMetaData(targetNode,
                                 targetProperty)

            if self.restoreNamespace is not None:
                targetNode.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBReplaceNamespace, 
                                                  self.restoreNamespace,
                                                  strapNamespace)

            if isinstance(targetNode, mobu.FBConstraint):
                strapFolder.Items.append(targetNode)

            elif isinstance(targetNode, mobu.FBModel):
                if not self.parentAnchors:
                    continue

                if index > len(self.parentAnchors)-1:
                    continue

                targetParent = mobu.FBFindObjectByFullName(self.parentAnchors[index])

                if targetNode is None:
                    continue

                targetNode.Parent = targetParent

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)

        for attribute in self.NODE_ATTRIBUTES:
            currentAttribute = getattr(self, attribute)

            if isinstance(currentAttribute, str):
                reportData += '\n\t\t{0}:{1}'.format(attribute,
                                                     currentAttribute)

            if isinstance(currentAttribute, list):
                reportData += '\n\t\t{0}'.format(attribute)

                for node in currentAttribute:
                    reportData += '\n\t\t\t{0}'.format(node)

        return reportData


class StrapChainBlender(object):
    WEIGHT_PROPERTY_SUFFIX = 'Weight'

    ANCHOR_CONSTRAINT_TYPE = 'anchorConstraint'

    DRIVEN_NODE_INDEX = 0

    PARENT_NODE_INDEX = 1

    HORIZONTAL_OFFSET = 500

    VERTICAL_OFFSET = 100

    SHIFT_COMPONENT_OFFSET = 600

    SCALE_PREFIX = '{}_Scale1'

    OUTPUT_PREFIX = '{}_Output1'

    INTERPOLATE_PREFIX = '{}_Blend1'

    WEIGHT1_PREFIX = '{}_WeightSourceA'

    WEIGHT2_PREFIX = '{}_WeightSourceB'

    WEAPON_PROPERTY = 'Body_Weapon_DOF_{0:02d}'

    STRAP_POINT_PREFIX = 'SH_BodyStrap'

    ALTERNATE_SUFFIX = 'Alt1'

    def __init__(self,
                 strapRigRoot,
                 inputCharacter):
        self.character = inputCharacter

        self.attachConstraints = []

        self.attachDrivers = []

        self.strapAnchors = []

        self.splineIkRigRoot = None

        self.constraintDistribution = None

        self.strapRelationConstraint = None

        self.strapRigRoot = strapRigRoot

        self.relationConstraint = None

        self.constraintPrefix = ''

        self.blendDriver = None

        self.targetConstraint = None

        self.useAlternatePoint = False

        self.increment = 0

        self.collect()

    def getPropsNamespace(self,
                          inputDummy):
        currentNamespace = Namespace.GetNamespace(inputDummy)

        if len(currentNamespace)==0:
            return None

        return currentNamespace

    def sortSuffixAsset(self,
                        inputArray):
        assetCollection = {}
        for node in inputArray:
            suffixKeys = node.Name.split('_')
            suffixKey = None

            for suffix in suffixKeys:
                if not suffix.isdigit():
                    continue
                suffixKey = str(suffix)

            assetCollection[int(suffixKey)] = node

        sortedNodesKeys = sorted(assetCollection.keys())
        outputAssets = [assetCollection[node] for node in sortedNodesKeys]

        return outputAssets

    def collectFromScene(self,
                         namespacePrefix=None):
        self.namespacePrefix = str(namespacePrefix)

        targetNodeList = mobu.FBComponentList()
        sampleName = str(self.STRAP_POINT_PREFIX)

        if namespacePrefix is None:
            mobu.FBFindObjectsByName('{0}*'.format(sampleName),
                                     targetNodeList,
                                     False,
                                     True)
        else:
            sampleName = '{0}:{1}*'.format(namespacePrefix,
                                           sampleName)

            mobu.FBFindObjectsByName(sampleName,
                                     targetNodeList,
                                     True,
                                     True)

        if len(targetNodeList) == 0:
            self.strapAnchors = []

            return False

        if self.useAlternatePoint is False:
            self.strapAnchors = [node for node in targetNodeList if not node.Name.endswith(self.ALTERNATE_SUFFIX)]
        else:
            self.strapAnchors = [node for node in targetNodeList if node.Name.endswith(self.ALTERNATE_SUFFIX)]

        self.strapAnchors = self.sortSuffixAsset(self.strapAnchors)

        return True

    def getConstraintDistribution(self):
        controlLink = self.strapRigRoot.PropertyList.Find('ConstraintDistribution')

        self.constraintDistribution = controlLink.GetSrc(0)

    def getChainContraint(self):
        controlLink = self.strapRigRoot.PropertyList.Find('strapRelationConstraint')

        self.strapRelationConstraint = controlLink.GetSrc(0)

    def extractAttachConstraints(self):
        controlLink = self.splineIkRigRoot.PropertyList.Find(self.ANCHOR_CONSTRAINT_TYPE)

        if controlLink is None:
            return

        groupMembersCount = controlLink.GetSrcCount()
        if groupMembersCount == 0:
            return

        for memberIndex in range(groupMembersCount):
            member = controlLink.GetSrc(memberIndex)

            self.attachConstraints.append(member)

    def extractAttachDrivers(self):
        controlLink = self.strapRigRoot.PropertyList.Find('chainComponents')

        if controlLink is None:
            return

        groupMembersCount = controlLink.GetSrcCount()
        if groupMembersCount == 0:
            return

        for memberIndex in range(groupMembersCount):
            member = controlLink.GetSrc(memberIndex)

            self.attachDrivers.append(member)

    def collect(self):
        ikProperty = self.strapRigRoot.PropertyList.Find('ikSplineRoot')
        self.splineIkRigRoot = ikProperty.GetSrc(0)

        self.extractAttachConstraints()

        self.extractAttachDrivers()

        self.useAlternatePoint = self.strapRigRoot.PropertyList.Find('useAlternatePoint').Data

        characterNamespace = self.getPropsNamespace(self.character)

        self.collectFromScene(namespacePrefix=characterNamespace)

        if not self.attachConstraints:
            return

        if not self.attachDrivers:
            return

        if len(self.attachConstraints) != len(self.attachDrivers):
            return

        self.getConstraintDistribution()

        self.getChainContraint()

    def layoutBlendComponents(self):
        self.interpolateNode = self.relationConstraint.AddFunctionBox("Rotation",
                                                                      "Interpolate")

        self.interpolateNode.SetBoxPosition(self.HORIZONTAL_OFFSET, 
                                            self.increment*\
                                            self.SHIFT_COMPONENT_OFFSET)

        self.interpolateNode.GetBox().Name = self.INTERPOLATE_PREFIX.format(self.constraintPrefix)

    def createInputSource(self,
                          targetAttribute,
                          targetAttribute2,
                          offsetPosition,
                          targetName,
                          inputConnection1,
                          inputConnection2):
        sender1 = self.relationConstraint.AddSenderBox(self.blendDriver)

        self.relationConstraint.ConnectBoxes(sender1, 
                                             inputConnection1,
                                             self.interpolateNode, 
                                             targetAttribute)

        self.relationConstraint.ConnectBoxes(sender1, 
                                             inputConnection2,
                                             self.interpolateNode, 
                                             targetAttribute2)

        sender1.SetBoxPosition(-600, 
                               offsetPosition[1])

    def prepareInputComponents(self):
        self.createInputSource('Ra',
                               'Rb',
                               [0, 
                                self.increment*\
                                self.SHIFT_COMPONENT_OFFSET+\
                                int(self.VERTICAL_OFFSET*-1.5)],
                                self.WEIGHT1_PREFIX.format(self.constraintPrefix),
                                'sourceA_Setting1',
                                'sourceA_Setting2')

    def setupOutputComponents(self):
        self.scaleNode = self.relationConstraint.AddFunctionBox("Vector",
                                                                "Scale (a x V)")

        inPlugs = self.findAnimationNode(self.scaleNode.GetBox().AnimationNodeInGet(),
                                         'Number')

        inPlugs.WriteData([100.0])

        self.scaleNode.SetBoxPosition(self.HORIZONTAL_OFFSET*2, 
                                      self.increment*\
                                      self.SHIFT_COMPONENT_OFFSET)

        self.outputNode = self.relationConstraint.AddFunctionBox("Converters",
                                                                 "Vector To Number")

        self.outputNode.SetBoxPosition(self.HORIZONTAL_OFFSET*3, 
                                       self.increment*\
                                       self.SHIFT_COMPONENT_OFFSET)

        self.relationConstraint.ConnectBoxes(self.interpolateNode, 
                                             'Result', 
                                             self.scaleNode, 
                                             'Vector')

        self.relationConstraint.ConnectBoxes(self.scaleNode, 
                                             'Result', 
                                             self.outputNode, 
                                             'V')

        self.scaleNode.GetBox().Name = self.SCALE_PREFIX.format(self.constraintPrefix)

        self.outputNode.GetBox().Name = self.OUTPUT_PREFIX.format(self.constraintPrefix)

    def connectIoBlocks(self):
        receiver = self.relationConstraint.AddReceiverBox(self.targetConstraint)

        ikAnchor = self.targetConstraint.ReferenceGet(1, 0)

        baseAnchor = self.targetConstraint.ReferenceGet(1, 1)

        for property in self.targetConstraint.PropertyList:
            if property.Name == '{}.Weight'.format(ikAnchor.LongName):
                ikAnchorWeightProperty = property
                ikAnchorWeightProperty.SetAnimated(True)
            elif property.Name == '{}.Weight'.format(baseAnchor.LongName):
                baseAnchorWeightProperty = property
                baseAnchorWeightProperty.SetAnimated(True)

        self.relationConstraint.ConnectBoxes(self.outputNode, 
                                             'X', 
                                             receiver, 
                                             ikAnchorWeightProperty.Name)

        self.relationConstraint.ConnectBoxes(self.outputNode, 
                                             'Y', 
                                             receiver, 
                                             baseAnchorWeightProperty.Name)

        receiver.SetBoxPosition(self.HORIZONTAL_OFFSET*4, 
                                self.increment*\
                                self.SHIFT_COMPONENT_OFFSET)

    def connectWeightDriver(self):
        sender2 = self.relationConstraint.AddSenderBox(self.constraintDistribution)
        propertyName = self.WEAPON_PROPERTY.format(self.strapPointIndex-1)
        senderProperty = self.constraintDistribution.PropertyList.Find(propertyName)
        senderProperty.SetAnimated(True)

        self.relationConstraint.ConnectBoxes(sender2, 
                                             propertyName, 
                                             self.interpolateNode, 
                                             'c')

        offsetPosition = [0, int(self.increment*self.SHIFT_COMPONENT_OFFSET+self.VERTICAL_OFFSET*-0.5)]

        sender2.SetBoxPosition(offsetPosition[0], 
                               offsetPosition[1])
               
    def findAnimationNode(self, 
                          inputComponent, 
                          attributeName):
        for node in inputComponent.Nodes:
            if node.Name == attributeName:
                return node

        return None

    def buildNetWork(self):
        self.relationConstraint = relationConstraintManager.RelationShipConstraintManager(constraint=self.strapRelationConstraint)

        self.relationConstraint.SetActive(True)

        self.increment = 0
        for index, constraint in enumerate(self.attachConstraints):
            self.constraintPrefix = constraint.Name

            self.blendDriver = self.attachDrivers[index]

            self.targetConstraint = constraint

            print 'Restoring', self.constraintPrefix, 'with', self.strapAnchors[index].LongName

            constraint.ReferenceAdd(self.PARENT_NODE_INDEX,
                                    self.strapAnchors[index])

            Globals.Scene.Evaluate()

            self.strapPointIndex = index+1

            self.layoutBlendComponents()

            self.prepareInputComponents()

            self.setupOutputComponents()

            self.connectIoBlocks()

            self.connectWeightDriver()

            self.increment += 1


class WeaponStrapProp(object):
    DUMMY_NAME = 'Dummy01'

    MOVER_NAME = 'mover'

    BASE_RIFFLES_POINTS = ('PH_RifleInverted',
                           'PH_RifleInverted_Alt1')

    DISTRIBUTION_DRIVER_NAME = 'ConstraintDistribution'

    BACK_DISTRIBUTION_DRIVER_NAME = 'ConstraintDistributionBack'

    STRAP_GROUP_NAME = 'strapInterpolationDrivers1'

    STRAP_POINT_PREFIX = 'SH_BodyStrap'

    ALTERNATE_SUFFIX = 'Alt1'

    LEGAL_STRAP_ASSETS = ('w_leftshoulder_strap',
                          'w_repeater_strap')

    BONE_ROOT_NAME = 'AAPStrap'

    NODE_NAMES = (DUMMY_NAME,
                  MOVER_NAME,
                  STRAP_GROUP_NAME)

    SOURCE_NODE_ATTRIBUTES = ('dummy',
                              'mover',
                              'strapRigRoot')

    OBJECT_SET = mobu.FBPropertyType.kFBPT_object

    def __init__(self,
                 path,
                 namespaceName):
        self.Path = path

        self.Namespace = namespaceName

        self.assetName = Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName)

        self.splineIkRigRoot = None

        self.strapRigRoot = None

        self.dummy = None

        self.mover = None

        self.isWeaponStrap = False

        self.metadata = []

        self.bindPose = []

        self.collect()

        self.validate()

        self.storeMetaData()

    def getStrapMetadata(self, 
                         strapRigRoot,
                         attributeName,
                         collectArray=False):
        metaData = strapRigRoot.PropertyList.Find(attributeName)

        if metaData is None:
            return None

        groupMembersCount = metaData.GetSrcCount()
        if groupMembersCount == 0:
            return None

        if collectArray is False:
            return metaData.GetSrc(0)
        else:
            groupData = []
            for index in xrange(groupMembersCount):
                groupData.append(metaData.GetSrc(index))

            return groupData

    def storeMetaData(self):
        if self.isWeaponStrap is False:
            return

        self.metadata = [StrapComponents(attribute, self.Namespace)
                         for attribute in self.strapRigRoot.PropertyList
                         if str(attribute.GetPropertyType()) == str(self.OBJECT_SET)]

        self.bindPose = [(attribute.Name, attribute.Data)
                         for attribute in self.strapRigRoot.PropertyList
                         if attribute.GetPropertyType() == mobu.FBPropertyType.kFBPT_charptr and \
                         attribute.IsUserProperty() == True]       

    def validate(self):
        self.isWeaponStrap = False

        if not all([self.strapRigRoot]):
            return False

        self.isWeaponStrap = True

        return True

    def collect(self):
        for index, nodeName in enumerate(self.NODE_NAMES):
            currentNode = self.getPart(nodeName,
                                       self.Namespace)

            setattr(self,
                    self.SOURCE_NODE_ATTRIBUTES[index],
                    currentNode)

    def getPart(self,
                namePattern,
                inputNameSpace):
        sampleName = '{0}:{1}'.format(inputNameSpace,
                                      namePattern)

        components = mobu.FBComponentList()
        mobu.FBFindObjectsByName(sampleName,
                                 components,
                                 True,
                                 True)

        components = list(components)

        if not components:
            return None

        return components[0]

    def restore(self):
        self.collect()

        strapFolder = self.strapRigRoot.PropertyList.Find('strapConstraintFolder').GetSrc(0)

        for objectSetProperty in self.metadata:
            objectSetProperty.restore(self.strapRigRoot,
                                      strapFolder,
                                      self.Namespace)

        self.restoreBaseRoot()

        self.restoreAAStrapConstraint()

        self.restoreStrapRelationConstraint()

        self.restoreBaseRoot()

        self.restoreBindPoseValues()

    def restoreBindPoseValues(self):
        for pose in self.bindPose:
            poseProperty = self.strapRigRoot.PropertyList.Find(pose[0])

            if poseProperty is not None:
                continue

            targetProperty = self.strapRigRoot.PropertyCreate(pose[0], 
                                                              mobu.FBPropertyType.kFBPT_charptr, 
                                                              'String', False, True, None)

            targetProperty.Data = pose[1]

            targetProperty.SetLocked(True)

    def restoreAAStrapConstraint(self):
        baseRootProperty= self.strapRigRoot.PropertyList.Find('baseRootConstraint')
        if baseRootProperty is None:
            return

        if baseRootProperty.GetSrcCount() == 0:
            return

        baseRootConstraint = baseRootProperty.GetSrc(0)
        if baseRootConstraint is None:
            return

        boneRoot = self.getPart(self.BONE_ROOT_NAME,
                                self.Namespace)

        if boneRoot is None:
            return

        sourceDriver = baseRootConstraint.ReferenceGet(0, 0)
        if sourceDriver is not None:
            return

        baseRootConstraint.ReferenceAdd(0, boneRoot)

    def restoreStrapRelationConstraint(self):
        targetCharacterProperty = self.strapRigRoot.PropertyList.Find('targetCharacter')

        if targetCharacterProperty.GetSrcCount() == 0 :
            return

        inputCharacter = targetCharacterProperty.GetSrc(0)
 
        strapRelationConstraintProperty = self.strapRigRoot.PropertyList.Find('strapRelationConstraint')

        if strapRelationConstraintProperty.GetSrcCount() == 0 :
            return

        propsNamespace = self.strapRigRoot.OwnerNamespace.Name

        chainUtils = StrapChainBlender(self.strapRigRoot,
                                       inputCharacter)

        chainUtils.buildNetWork()

        self.restoreSliderConnections(inputCharacter,
                                      propsNamespace)

    def setupSliderAttribute(self,
                             propsNamespace):
        self.strapWeightNode = self.getStrapMetadata(self.strapRigRoot,
                                                     'ConstraintDistribution')

        if self.strapWeightNode is None:
            return

        relationConstraintName = '{}_Relation'.format(self.DISTRIBUTION_DRIVER_NAME)

        relationConstraint = relationConstraintManager.RelationShipConstraintManager(name=relationConstraintName)

        relationConstraint.SetActive(True)

        sender = relationConstraint.AddSenderBox(self.distributionSlider)

        receiver = relationConstraint.AddReceiverBox(self.strapWeightNode)

        useAlternatePoint = self.strapRigRoot.PropertyList.Find('useAlternatePoint').Data

        senderPropertyName = str(self.DISTRIBUTION_DRIVER_NAME)

        if useAlternatePoint is False:
            senderPropertyName = str(self.BACK_DISTRIBUTION_DRIVER_NAME)

        relationConstraint.ConnectBoxes(sender, 
                                        senderPropertyName, 
                                        receiver,
                                        self.DISTRIBUTION_DRIVER_NAME)

        strapConstraintFolder = self.getStrapMetadata(self.strapRigRoot,
                                                      'strapConstraintFolder')

        sliderConstraint = relationConstraint.GetConstraint()
        sliderConstraint.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBConcatNamespace, 
                                                propsNamespace)

        if strapConstraintFolder:
            strapConstraintFolder.Items.append(sliderConstraint)

    def connectMetaData(self,
                        inputObject,
                        objectSetName):
        metaRootProperty = self.strapRigRoot.PropertyList.Find(objectSetName)

        if metaRootProperty is None:
            return

        inputObject.ConnectDst(metaRootProperty)

    def restoreBaseRoot(self):
        characters = self.getStrapMetadata(self.strapRigRoot,
                                           'targetCharacter',
                                           collectArray=True)

        if not characters:
            return

        inputCharacter = characters[0]
        characterNamespace = inputCharacter.OwnerNamespace.Name

        useAlternatePoint = self.strapRigRoot.PropertyList.Find('useAlternatePoint').Data
        rifflePoint = self.BASE_RIFFLES_POINTS[0]
        if useAlternatePoint is True:
            rifflePoint = self.BASE_RIFFLES_POINTS[1]

        contactPointList = mobu.FBComponentList()
        sampleName = '{0}:{1}'.format(characterNamespace,
                                      rifflePoint)

        mobu.FBFindObjectsByName(sampleName,
                                 contactPointList,
                                 True,
                                 True)

        contactPointList = list(contactPointList)
        if not contactPointList:
            return 

        self.connectMetaData(contactPointList[0],
                             'baseRoot')

    def restoreSliderConnections(self,
                                 inputCharacter,
                                 propsNamespace):
        characterNamespace = inputCharacter.OwnerNamespace.Name

        sliderList = mobu.FBComponentList()
        useAlternatePoint = self.strapRigRoot.PropertyList.Find('useAlternatePoint').Data

        sliderName = str(self.DISTRIBUTION_DRIVER_NAME)

        if useAlternatePoint is False:
            sliderName = str(self.BACK_DISTRIBUTION_DRIVER_NAME)

        sampleName = '{0}:{1}'.format(characterNamespace,
                                      sliderName)

        mobu.FBFindObjectsByName(sampleName,
                                 sliderList,
                                 True,
                                 True)

        sliderList = list(sliderList)
        if sliderList:
            self.distributionSlider = sliderList[0]

            self.connectMetaData(self.distributionSlider,
                                 'targetCharacterSlider')

            self.setupSliderAttribute(propsNamespace)

    def __repr__(self):
        reportData = '<{0}>'.format(self.__class__.__name__)

        for attribute in self.SOURCE_NODE_ATTRIBUTES:
            currentAttribute = getattr(self, attribute)

            if currentAttribute is None:
                reportData += '\n\t{0}:None'.format(attribute)
            else:
                reportData += '\n\t{0}:{1}'.format(attribute,
                                                   currentAttribute.LongName)

        for node in self.metadata:
            reportData += '\n\t{0}'.format(node)

        return reportData