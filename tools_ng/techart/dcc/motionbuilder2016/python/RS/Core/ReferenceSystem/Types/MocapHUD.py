import os

import pyfbsdk as mobu

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger
from RS.Utils import Namespace


class MocapHUD(MocapComplex):
    """
    Represents a mocap slate reference in a scene
    """
    studio = ()
    PropertyTypeName = "MocapHUD"
    FormattedTypeName = "Mocap HUD"
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "North", "BaseAssets"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "SD", "BaseAssets"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "TOR", "BaseAssets"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "NYC", "BaseAssets"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "Offsite", "BaseAssets"),
                    ]

    FileType = "fbx"

    @Logger("Mocap HUD - Create")
    def _create(self, filestate=None, importOptions=None):
        """
        This function is run when a HUD is created

        Args:
            importOptions (FaceAnimDataImportOptions, optional): face import options.
        """
        super(MocapHUD, self)._create(filestate=None)

        # update groups
        self._log.LogMessage("Updating base previz groups with new slate reference created.")
        self.UpdateGroups()

    def getHUDElements(self):
        """
        Get all the HUD elements in the reference

        return:
            list of FBHUD objects
        """
        return Namespace.GetContentsByType(self.GetFBNamespace(), mobu.FBHUD)
