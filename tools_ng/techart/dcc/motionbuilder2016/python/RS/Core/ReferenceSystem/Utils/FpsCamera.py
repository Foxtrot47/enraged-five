import os
from xml.etree.cElementTree import parse as xmlParser


import pyfbsdk as mobu
from RS.Utils import Namespace
from RS import ProjectData
from RS import Globals
from RS import Config


class FpsCameraRigComponent(object):
    VECTOR_ATTRIBUTES = ('PostRotation',
                         'Rotation',
                         'scale')

    VECTOR_PROPERTIES = ('PostRotation',
                         'Rotation (Lcl)',
                         'Scaling (Lcl)')

    ATTRIBUTES = ('control',
                  'enableDof')

    def __init__(self,
                 inputAnchor):
        self.control = None

        self.PostRotation = None

        self.Rotation = None

        self.enableDof = None

        self.scale = None

        self.collect(inputAnchor)

    def unpackArray(self,
                    currentAnchor,
                    attribute):
        arrayValue = str(currentAnchor.attrib[attribute]).strip().replace('[', '').replace(']', '')

        vectorData = [float(item.strip())
                      for item in arrayValue.split(',')]

        return vectorData

    def collect(self,
                inputAnchor):
        self.control = str(inputAnchor.attrib['control'])

        self.enableDof = bool(inputAnchor.attrib['enableDof'])

        for attribute in self.VECTOR_ATTRIBUTES:
            setattr(self,
                    attribute,
                    self.unpackArray(inputAnchor,
                                     attribute))

    def resetControl(self,
                     namespace):
        model = mobu.FBFindModelByLabelName('{}:{}'.format(namespace,
                                                           self.control))

        if not model:
            return

        for index, attribute in enumerate(self.VECTOR_ATTRIBUTES):
            currentData = getattr(self,
                                  attribute)

            property = model.PropertyList.Find(self.VECTOR_PROPERTIES[index])

            if property is None:
                continue

            property.SetLocked(False)

            property.Data = mobu.FBVector3d(currentData[0],
                                            currentData[1],
                                            currentData[2])

            property.SetLocked(True)


class FpsCameraSettings(object):
    CURRENT_PROJECT = Config.Project.Path.Root.replace('\\', '/')

    SETTING_FILE = os.path.join(CURRENT_PROJECT,
                                'tools',
                                'release',
                                'techart',
                                'etc',
                                'config',
                                'motionbuilder',
                                'fpsCamera',
                                'defaultCameraLimits.xml')

    SETTING_FILE = SETTING_FILE.replace('\\', '/')

    CAMERA_ATTRIBUTE = ('PostRotation_Offset',
                        'Target_Offset1',
                        'Target_Offset2',
                        'Camera_Axis_Orient')

    def __init__(self):
        self.PostRotation_Offset = [180.0, 0.0, 90.0]

        self.Target_Offset1 = [180.0, 0.0, 90.0]

        self.Target_Offset2 = [90.0, 0.0, 90.0]

        self.Camera_Axis_Orient = [-90.0, -90.0, 0.0]

        self.components = []

    def unpackArray(self,
                    currentAnchor):
        arrayValue = str(currentAnchor.text).strip().replace('[', '').replace(']', '')

        vectorData = [float(item.strip())
                      for item in arrayValue.split(',')]

        return vectorData

    def collect(self):
        if not os.path.exists(self.SETTING_FILE):
            return

        with open(self.SETTING_FILE, "r") as inputfile:
            inputTree = xmlParser(inputfile)

            rootElement = inputTree.getroot()

            for attribute in self.CAMERA_ATTRIBUTE:
                currentAnchor = rootElement.find(attribute)

                if currentAnchor is None:
                    continue

                setattr(self,
                        attribute,
                        self.unpackArray(currentAnchor))

            rigComponentsAnchor = rootElement.find('rigComponents')

            if rigComponentsAnchor is None:
                return

            itemArray = rigComponentsAnchor.findall('item')

            self.components = [FpsCameraRigComponent(item)
                               for item in itemArray]


class FpsCameraUtils(object):
    GYROSCOPE_IO = ('DeformedMesh',
                    'FpsCamera',
                    'FpsCameraPreview',
                    'ParentAnchor',
                    'OrientTarget1',
                    'OrientTarget2',
                    'CameraPivot',
                    'PreviewHandle',
                    'ConeOffset',
                    'ConeFollow',
                    'ExportPropertyDriver',
                    'Left_CH_Helper',
                    'Right_CH_Helper',
                    'ExportPivot',
                    'ScaleRoot',
                    'SKEL_Root',
                    'MOVER',
                    'ExportAttributeTarget',
                    'WorldMover',
                    'Aim Target',
                    'Up Target',
                    'Cone Master Zero')

    LEGAL_GYROSCOPE_TYPE = ('headGyroscope',
                            'fpsHeadGyroscope',
                            'fpsCameraConstraint')

    def __init__(self):
        self.xmlUtils = FpsCameraSettings()

    def writeConstraintFactorySettings(self,
                                       headGyroscope):
        for attribute in self.xmlUtils.CAMERA_ATTRIBUTE:
            propertyName = attribute.replace('_', ' ')

            property = headGyroscope.PropertyList.Find(propertyName)

            if property is None:
                continue

            vectorData = getattr(self.xmlUtils,
                                 attribute)

            property.Data = mobu.FBVector3d(vectorData[0],
                                            vectorData[1],
                                            vectorData[2])

        if not self.xmlUtils.components:
            return

        namespace = headGyroscope.OwnerNamespace

        if not namespace:
            return

        for item in self.xmlUtils.components:
            item.resetControl(namespace.Name)

        Globals.Scene.Evaluate()

    def ExtractFpsCameraConstraints(self,
                                    inputCharacter):
        characterNameSpace = Namespace.GetNamespace(inputCharacter)

        if characterNameSpace is None:
            return []

        if not characterNameSpace.strip():
            return []

        headGyroscopes = []
        for constraint in mobu.FBSystem().Scene.Constraints:
            if constraint.ClassName() not in self.LEGAL_GYROSCOPE_TYPE:
                continue
            
            if constraint.OwnerNamespace is None:
                continue

            if constraint.OwnerNamespace.Name.lower().strip() != characterNameSpace.lower().strip():
                continue

            headGyroscopes.append(constraint)

        return headGyroscopes

    def restoreConstraintState(self,
                               headGyroscope):
        coneMaster = headGyroscope.ReferenceGet(self.GYROSCOPE_IO.index('CameraPivot'), 0)

        if coneMaster is None:
            return

        headGyroscope.Lock = False

        disconnectChannel = ('ExportPropertyDriver', 
                             'CameraPivot')

        #in order to refresh constraint we have to disconnect/connect the conemaster
        for attribute in disconnectChannel:
            headGyroscope.ReferenceRemove(self.GYROSCOPE_IO.index(attribute),
                                          coneMaster)

        Globals.Scene.Evaluate()

        for attribute in disconnectChannel:
            headGyroscope.ReferenceAdd(self.GYROSCOPE_IO.index(attribute),
                                       coneMaster)

        Globals.Scene.Evaluate()
                          
    def refreshConstraintConnections(self,
                                     inputCharacter,
                                     allTakes=True):
        """
        Parameters:
            inputCharacter (FBCharacter): reference character with a namesapce used to
                                          refresh fpsCamera constraint.
            allTakes (bool): If set to True the operation will reset the fps rig's
                             internal non-animatable values to their defaults on all takes.
                             If False it will only affect the current take. Default is True.
        """
        headGyroscopes = self.ExtractFpsCameraConstraints(inputCharacter)
        
        if not headGyroscopes:
            return None

        self.xmlUtils.collect()

        for headGyroscope in headGyroscopes:
            self.restoreConstraintState(headGyroscope)
        
        if allTakes:
            currentTake = Globals.System.CurrentTake
            for take in Globals.Scene.Takes:
                Globals.System.CurrentTake = take
                
                for headGyroscope in headGyroscopes:
                    self.writeConstraintFactorySettings(headGyroscope)
            Globals.System.CurrentTake = currentTake
        else:
            for headGyroscope in headGyroscopes:
                self.writeConstraintFactorySettings(headGyroscope)
