"""
This contains logic to help interaction with references from unknown locations that made it into the scene.
These references are usually from previous reference systems, manually made reference nulls and legacy fbx files.
"""

from RS.Core.ReferenceSystem.Types.Base import Base


class Unknown(Base):
    '''
    Represents a prop reference in a scene
    '''
    PropertyTypeName    = 'Unknown'
    FormattedTypeName   = 'Unknown'
    TypePathList        = []
    FileType            = 'fbx'

    def _create(self, filestate=None, importOptions=None):
        """
        Args:
            importOptions (FaceAnimDataImportOptions, optional): face import options.
        """
        pass
