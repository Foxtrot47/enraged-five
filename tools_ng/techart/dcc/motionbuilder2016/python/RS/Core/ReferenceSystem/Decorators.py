import os
import sys
import pstats
import cProfile
import tempfile
from functools import wraps

import pyfbsdk as mobu
from PySide import QtGui

from RS import Globals
from RS.Utils import Instrument, ContextManagers
from RS.Utils.Scene import Component
from RS.Utils.Logging import Universal

from RS.Core.ReferenceSystem import Exceptions, Settings
from RS.Tools.CameraToolBox.PyCore.Decorators import Profile

# list of exception classes
KNOWN_ERROR_TYPES = (Exceptions.FilePathDoesNotExist,
                     Exceptions.UnrecognisedReferenceDirectory,
                     Exceptions.CharacterComponentMissing,
                     Exceptions.MultipleCharacterComponents,
                     Exceptions.MissingReference,
                     Exceptions.UnsupportedConstraint,
                     Exceptions.UnicodeError,
                     Exceptions.StatedAnimationAnimFileNotFound,
                     Exceptions.IncorrectRayfireFile,
                     Exceptions.StatedAnimationMissingDummy,
                     Exceptions.UnboundReference,
                     Exceptions.MultipleReferenceErrors)


class IsListEmpty(object):
    '''
    Decorator that checks if the second argument passed to the decorated function is an empty list
    '''
    def __init__(self):
        self._log = Universal.UniversalLog("{0}_reference_system".format(os.getpid()))

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if len(args) >1:
                # get list of selected references
                targetList = args[1]
                if isinstance(targetList, list):
                    # print log warning if no references have been selected
                    if len(targetList) == 0:
                        self._log.LogWarning("Selected reference list is empty.")
                elif targetList:
                    return func(*args, **kwargs)
                return func(*args, **kwargs)
            return func(*args, **kwargs)
        return wrapper


class Profiler(Profile.Profile):
    """
    Decorator that profiles the execution of the decorated function
    Adds a 20 second overhead to the decorated function
    """

    def __enter__(self):
        """
        Instruments the pyfbsdk module so it gets picked up by python's Profile module
        """
        Instrument.InstrumentModule(mobu)
        return super(Profiler, self).__enter__()


def ExceptionMessage(func):
    """
    This decorator will show a message box with the exception is a
    """
    @wraps(func)
    def wrapper(*args, **kwargs):

        manager = args[0]
        try:
            return func(*args, **kwargs)
        except Exception as exception:
            import pythonidelib;print exception ;pythonidelib.FlushOutput()
            # is this exception a known error
            isExpectedError = False
            if type(exception) in KNOWN_ERROR_TYPES:
                isExpectedError = True

            # if exception is known, launch custom error popup message
            if not manager.Silent and not isinstance(exception, Exceptions.NotAnError):
                QtGui.QMessageBox.warning(None,
                                          "Reference Editor Warning",
                                          "{}\n\n{}".format(str(exception),
                                                            "Speak to Tech Art if you are unable to diagnose the issue."))

            # exception is not known
            if manager.RaiseErrors or not isExpectedError:
                exceptionType, exceptionValue, traceback = sys.exc_info()
                raise exceptionType, exceptionValue, traceback
    return wrapper


class Logger(object):
    '''
    A decorator that will act as log wrapper around the RS.Logging system.
    It takes optional arguments: context, debug and log( if used outside the Reference System you need to pass as an argument the RS.Universal.Logging)
    It raises full traceback of the occuring errors
    '''
    def __init__(self, context=None, state=True):
        self.Context = context
        self._state = state

    def __call__(self, func):

        @wraps(func)
        def wrapper(*args, **kwargs):

            manager = args[0]

            # set the progresswindowoutput property and run the log UI
            manager._log.ProgressWindowOutput = self._state

            # add log string to a list
            manager._log.PushContext(self.Context)

            try:
                return func(*args, **kwargs)

            except Exception as e:
                if not hasattr( e, '_handled' ):
                    manager._log.LogError( e.message )
                    e._handled = True
                raise

            finally:
                if self.Context:
                    manager._log.PopContext()

        return wrapper


class Progress(object):
    '''
    '''
    def __init__(self, context=None):
        self.Context = context

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            manager = args[0]
            if manager._log.ProgressWindowOutput:
                manager._log.ProgressWindowStart()
            return func(*args, **kwargs)
        return wrapper


def CleanScene(func):
    '''
    Decorator that rememebers scene settings and pushes them back. It will also call the
    Manager._cleanScene before and after running the target function. Bit of a strange design
    but will look at improving this at a later date. Also now handles turning of devices as well.
    '''
    @wraps(func)
    def wrapper(*args, **kwargs):

        manager = args[0]
        activeFps = Globals.Player.GetTransportFps()

        # Remember information
        startTime = Globals.Player.LoopStart
        endTime = Globals.Player.LoopStop

        # Record current Take.
        oldTake = Globals.System.CurrentTake

        # Run it before the function
        manager._log.LogMessage("Preforming a pre pass of scene clean up.")
        manager._cleanScene()

        result = func(*args, **kwargs)

        #Also run it after - clean up anything that might have been left as a result of an update
        manager._log.LogMessage("Preforming a post pass of scene clean up.")
        manager._cleanScene()

        if oldTake != Globals.System.CurrentTake:
            Globals.System.CurrentTake = oldTake

        # Fixup time range
        Globals.Player.LoopStart = startTime
        Globals.Player.LoopStop = endTime

        # Go back to base layer
        Globals.System.CurrentTake.SetCurrentLayer(0)

        # Set FPS 
        Globals.Player.SetTransportFps(activeFps)

        return result

    return wrapper


class DeactivateFileOpenCallbacks(object):
    '''
    A decorator that will temporarily deactivate any callbacks on FileOpen during a function's
    running.If we are passed 'False' as our state the decorator will do nothing (simply call the wrapped function directly)
    '''
    def __init__(self, State):
        self._state = State

    def __call__(self, targetFunction):
        def inner(*args, **kwargs):
            if not self._state:
                return targetFunction(*args, **kwargs)
            with ContextManagers.SuspendCallbacks():
                return targetFunction(*args, **kwargs)
        return inner


class SuspendDynamicEvaluation(object):
    """
    A decorator that turns off callbacks, scene parallelism and expressions to keep the scene
    stable while merging in a file to be reference and editing that scene.

    Does not include MergeTransactionActive()
    """
    Count = 0

    def __init__(self, flush=False):
        """
        Constructor

        Arguments:
            flush (bool): flush all events at the end of the transaction
        """
        self._flush = flush

    def __call__(self, func):
        """
        Overrides built-in method

        Arguments:
            func (function): the decorated function

        Return:
            func
        """
        @wraps(func)
        def wrapper(*args, **kwargs):
            SuspendDynamicEvaluation.Count += 1
            with ContextManagers.SuspendCallbacks(), ContextManagers.SuspendSceneParallelism(), \
                 ContextManagers.SceneExpressionsDisabled():
                value = func(*args, **kwargs)
                if self._flush:
                    Globals.Application.FlushEventQueue()
            SuspendDynamicEvaluation.Count -= 1
            return value
        return wrapper


class ReferenceMergeTransaction(object):
    """
    A decorator that turns off callbacks, scene parallelism, expressions and internal mobu event calls
    to keep the scene stable while merging in a file to be reference and editing that scene.
    """
    Count = 0

    def __init__(self, flush=False):
        """
        Constructor

        Arguments:
            flush (bool): flush all events at the end of the transaction
        """
        self._flush = flush

    def __call__(self, func):
        """
        Overrides built-in method

        Arguments:
            func (function): the decorated function

        Return:
            func
        """
        @wraps(func)
        def wrapper(*args, **kwargs):
            ReferenceMergeTransaction.Count += 1
            with ContextManagers.SuspendCallbacks(), ContextManagers.SuspendSceneParallelism(), \
                 ContextManagers.SceneExpressionsDisabled(), ContextManagers.MergeTransactionActive():
                value = func(*args, **kwargs)
                if self._flush:
                    Globals.Application.FlushEventQueue()
            ReferenceMergeTransaction.Count -= 1
            return value
        return wrapper


class ReferenceEditTransaction(object):
    """
    A decorator that turns off callbacks, scene parallelism, expressions and internal mobu event calls
    to keep the scene stable while editing that scene. This is should be used in cases where a merge isn't happening
    but namespaces and/or their content are being deleted.
    """
    Count = 0

    def __init__(self, flush=False):
        """
        Constructor

        Arguments:
            flush (bool): flush all events at the end of the transaction
        """
        self._flush = flush

    def __call__(self, func):
        """
        Overrides built-in method

        Arguments:
            func (function): the decorated function

        Return:
            func
        """
        @wraps(func)
        def wrapper(*args, **kwargs):
            ReferenceEditTransaction.Count += 1
            with ContextManagers.SuspendCallbacks(), ContextManagers.SuspendSceneParallelism(), \
                 ContextManagers.SceneExpressionsDisabled(), ContextManagers.PreventUIUpdateActive():
                value = func(*args, **kwargs)
                if self._flush:
                    Globals.Application.FlushEventQueue()
            ReferenceEditTransaction.Count -= 1
            return value
        return wrapper


class ToggleExpressionActiveStatus(object):
    """
    A decorator that turns off expression constraints, runs a function, then turns them back on
    """
    def __init__(self):
        """
        Constructor
        """
        self._log = Universal.UniversalLog("{0}_reference_system".format(os.getpid()))

    def __call__(self, func):
        """
        Overrides built-in method

        Arguments:
            func (function): the decorated function

        Return:
            func
        """
        @wraps(func)
        @Universal.Logger(self._log, "Toggle Expression Constraints")
        def wrapper(*args, **kwargs):
            # deactivate expression constraints
            self._log.LogMessage("Deactivate all RSExprSolverConstraints...")
            for constraint in Globals.Constraints:
                if constraint.ClassName() == "RSExprSolverConstraint":
                    constraint.Active = False
            self._log.LogMessage("RSExprSolverConstraints deactivated.")

            # run the main function
            results = func(*args, **kwargs)

            # activate expression constraints
            self._log.LogMessage("Activate all RSExprSolverConstraints...")
            for constraint in Globals.Constraints:
                if constraint.ClassName() == "RSExprSolverConstraint":
                    constraint.Active = True
            self._log.LogMessage("RSExprSolverConstraints activated.")

            return results
        return wrapper


def Optimize(func):
    """ Sets the Merge & Prevent UI Transaction context managers to respect the Optimization flag of the Reference System Manager  """
    @wraps(func)
    def wrapper(*args, **kwargs):

        optimize = ContextManagers.AbstractTransaction.enabled()
        ContextManagers.AbstractTransaction.setEnabled(Settings.Optimize)
        try:
            results = func(*args, **kwargs)
        except:
            raise
        finally:
            ContextManagers.AbstractTransaction.setEnabled(optimize)
        return results

    return wrapper


class ToggleConstraintActiveStatus(object):
    """
    We want to switch off constraints before doing an update, and switch them back on when it
    has completed.
    """
    def __init__(self):
        """
        Constructor
        """
        self._log = Universal.UniversalLog("{0}_reference_system".format(os.getpid()))

    def __call__(self, func):
        """
        Overrides built-in method

        Arguments:
            func (function): the decorated function

        Return:
            func
        """
        @wraps(func)
        @Universal.Logger(self._log, "Toggle Constraints")
        def wrapper(*args, **kwargs):

            referenceList = args[1]

            if not isinstance(referenceList, list):
                referenceList = [referenceList]

            for reference in referenceList:
                referenceNamespace = reference.Namespace

                constraintDict = {}

                constraintList = [constraint for constraint in Globals.Constraints
                                  if Component.GetNamespace(constraint) == referenceNamespace]

                # deactivate constraints and record their original state in a dict
                self._log.LogMessage("Deactivating Constraints for [{0}]...".format(referenceNamespace))

                for constraint in constraintList:
                    constraintDict[constraint.LongName] = constraint.Active
                    constraint.Active = False
                    Globals.Scene.Evaluate()
                self._log.LogMessage("Finished deactivating Constraints.")

                # run the main function
                results = func(*args, **kwargs)

                # Eval to ensure all objects are updated before any constraints are activated
                # Fixes issues such as (url:bugstar:3925745)
                Globals.Scene.Evaluate()

                # re-activate constraints
                self._log.LogMessage("Activating Constraints for [{0}]...".format(referenceNamespace))
                # get a dict of the current constraints
                currentConstraintDict = {constraint.LongName.lower():constraint for constraint in Globals.Constraints}
                # set active value
                for key, value in constraintDict.iteritems():
                    constraint = currentConstraintDict.get(key.lower())
                    if constraint is None:
                        continue
                    constraint.Active = value
                    Globals.Scene.Evaluate()
                self._log.LogMessage("Finished activating Constraints.")

                return results

        return wrapper


def Active(func):
    """
    Sets the active state for the reference system

    Args:
        func (function): function being wrapped

    Returns:
        func
    """
    @wraps(func)
    def wrapper(instance, *args, **kwargs):
        try:
            update = instance.IsActive() != True
            instance._active = True
            return func(instance, *args, **kwargs)
        except:
            raise
        finally:
            if update:
                instance._active = False
    return wrapper
