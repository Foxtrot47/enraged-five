import os

import pyfbsdk as mobu

from PySide import QtGui

from RS import Config, Globals, Perforce
from RS.Utils import Scene
from RS.Utils.Scene import Component
from RS.Tools.VirtualProduction import const
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapStage(MocapComplex):
    """
    Represents a mocap stage reference in a scene
    """
    studio = ()
    PropertyTypeName = "MocapStage"
    FormattedTypeName = "Mocap Stage"
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "North"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "NYC"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "SD"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "Tor"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "Offsite"),
                    ]
    FileType = "fbx"

    @Logger("Mocap Stage - Make Active")
    def MakeActive(self):
        """
        Make selected reference Active (checking on the FBModel"s Active property)
        Turn off all other Active Stage properties, if they"re checked on
        """
        self._log.LogMessage("Makeing Stage reference Active: {0}".format(self._modelNull.Name))

        sceneRoot = mobu.FBSystem().Scene.RootModel
        for child in sceneRoot.Children:
            childNamespace = child.LongName.split(":")[0]
            stagesProperty = child.PropertyList.Find("Active Stage")
            if stagesProperty is not None:
                if childNamespace == self._modelNull.Name:
                    stagesProperty.Data = True
                    self.SnapMocapConstraints()
                else:
                    stagesProperty.Data = False

    @Logger("Mocap Stage - Create")
    def _create(self, filestate=None, importOptions=None):
        """
        This function gets run only when a character is created. Calls the base class and then
        fixes the UFC device.

        Args:
            importOptions (FaceAnimDataImportOptions, optional): face import options.
        """
        super(MocapStage, self)._create(filestate=filestate)

        # update handle namespace
        self._log.LogMessage("Checking and fixing double namespaces in handles.")
        self.FixHandleDoubleNamespace()
