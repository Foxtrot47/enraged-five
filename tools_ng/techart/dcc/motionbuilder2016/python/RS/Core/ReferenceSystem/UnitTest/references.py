"""
Unit test for each individual type of reference
"""
import os
import sys
import unittest

from RS import Perforce
from RS.Unittests import utils
from RS.Core.ReferenceSystem.UnitTest import base, const


def Run(className=None):
    """Run unittests from this module"""
    unittest.TextTestRunner(verbosity=0).run(suite(className))


def suite(className=None):
    """A suite to hold all the tests from this module

    Args:
        className (str): the name of the class whose test should be ran. Runs all the tests if None.
    Returns:
        unittest.TestSuite
    """
    module = sys.modules[__name__]
    return utils.getSuite(module, "test_Referencing", className)


class ReferenceBase(base.Base):

    KEY = None

    def test_Referencing(self):
        """Base testing of reference assets

        This method grabs the KEY attribute set to check the yaml config for data like the path, revision etc.
        If the value isn't set, it will grab the key via the name of the class. ex class test_Prop = Prop
        By default this method will create, rename, update, and delete the asset. If there is a swapPath in the config
        it will do a swap as well.

        Return:
            bool: If successful or not
        """
        # get key, use class name if not set
        self.KEY = self.KEY if self.KEY else self.__class__.__name__.split("_")[-1]

        # get data
        referencesConfig = utils.loadYaml(const.ConfigPathDict.REFERENCES_CONFIG_PATH)
        self.assetData = utils.getConfigKey(self.KEY, referencesConfig)

        if not self.assetData:
            print("No config key found for {} to test asset".format(self.KEY))
            return False

        self.path = self.assetData.get("path", "")
        self.revision = self.assetData.get("revision", None)
        self.swapPath = self.assetData.get("swapPath", "")
        self.swapRevision = self.assetData.get("swapRevision", None)
        self.rename = self.assetData.get("rename", "defaultRename")

        # sync file
        Perforce.Sync(self.path, force=True, revision=self.revision)
        if not self.path or not os.path.exists(self.path):
            self.fail(
                "{} filepath doesn't exist! Manually sync this file or check the references.yaml config".format(
                    self.path
                )
            )

        # reference tests
        reference = self.assertCreate(self.path)
        self.assertRename(reference, self.rename)
        self.assertMinorUpdate(reference)
        self.assertMajorUpdate(reference)

        # sync other revision to swap if needed
        if self.swapPath:
            Perforce.Sync(self.swapPath, force=True, revision=self.swapRevision)
            self.assertSwap(reference, self.swapPath)

        self.assertDelete(reference)
        return True


class Test_Audio(ReferenceBase):
    """Tests referencing in audio"""

    def test_Referencing(self):
        """Testing Audio reference"""
        super(Test_Audio, self).test_Referencing()


class Test_CharacterAnimal(ReferenceBase):
    """Tests referencing in animal rigs"""

    def test_Referencing(self):
        """Testing Animal reference"""
        super(Test_CharacterAnimal, self).test_Referencing()


class Test_CharacterCutscene(ReferenceBase):
    """Tests referencing in character cutscene rigs"""

    def test_Referencing(self):
        """Testing CharacterCutscene reference"""
        super(Test_CharacterCutscene, self).test_Referencing()


class Test_CharacterHelper(ReferenceBase):
    """Tests referencing in character helper rigs"""

    def test_Referencing(self):
        """Testing CharacterHelper reference"""
        super(Test_CharacterHelper, self).test_Referencing()


class Test_CharacterIngame(ReferenceBase):
    """Tests referencing in character ingame rigs"""

    def test_Referencing(self):
        """Testing CharacterIngame reference"""
        super(Test_CharacterIngame, self).test_Referencing()


class Test_Face(ReferenceBase):
    """Tests referencing in Face"""

    def test_Referencing(self):
        """Testing Face reference"""
        # TODO: Needs some custom logic as character reference is also required and the reference logic needs
        # TODO: Logic also needs to be decoupled from the UI it brings up
        pass


class Test_MocapBasePrevis(ReferenceBase):
    """Tests referencing in mocap base previs assets"""

    def test_Referencing(self):
        """Testing MocapBasePrevis reference"""
        super(Test_MocapBasePrevis, self).test_Referencing()


class Test_MocapBlockingModel(ReferenceBase):
    """Tests referencing in mocap blocking model assets"""

    def test_Referencing(self):
        """Testing MocapBlockingModel reference"""
        super(Test_MocapBlockingModel, self).test_Referencing()


class Test_MocapBuildAsset(ReferenceBase):
    """Tests referencing in mocap build assets"""

    def test_Referencing(self):
        """Testing MocapBuildAsset reference"""
        super(Test_MocapBuildAsset, self).test_Referencing()


class Test_MocapGsSkeleton(ReferenceBase):
    """Tests referencing in mocap gs skeletons"""

    def test_Referencing(self):
        """Testing MocapGsSkeleton reference"""
        super(Test_MocapGsSkeleton, self).test_Referencing()


class Test_MocapHUD(ReferenceBase):
    """Tests referencing in mocap gs skeletons"""

    def test_Referencing(self):
        """Testing MocapHUD reference"""
        super(Test_MocapHUD, self).test_Referencing()


class Test_MocapPropToy(ReferenceBase):
    """Tests referencing in mocap prop toys """

    def test_Referencing(self):
        """Testing MocapPropToy reference"""
        super(Test_MocapPropToy, self).test_Referencing()


class Test_MocapRefCamera(ReferenceBase):
    """Tests referencing in mocap ref camera"""

    def test_Referencing(self):
        """Testing MocapRefCamera reference"""
        super(Test_MocapRefCamera, self).test_Referencing()


class Test_MocapReferencePose(ReferenceBase):
    """Tests referencing in mocap reference pose"""

    def test_Referencing(self):
        """Testing MocapReferencePose reference"""
        super(Test_MocapReferencePose, self).test_Referencing()


class Test_MocapSlate(ReferenceBase):
    """Tests referencing in mocap slates"""

    def test_Referencing(self):
        """Testing MocapSlate reference"""
        super(Test_MocapSlate, self).test_Referencing()


class Test_MocapVirtualCamera(ReferenceBase):
    """Tests referencing in mocap virtual camera"""

    def test_Referencing(self):
        """Testing MocapVirtualCamera reference"""
        super(Test_MocapVirtualCamera, self).test_Referencing()


class Test_Set(ReferenceBase):
    """Tests referencing in sets"""

    def test_Referencing(self):
        """Testing Set reference"""
        super(Test_Set, self).test_Referencing()


class Test_StatedAnimation(ReferenceBase):
    """Tests referencing in statedAnimation/destruction"""

    def test_Referencing(self):
        """Testing StatedAnimation reference"""
        super(Test_StatedAnimation, self).test_Referencing()


class Test_Vehicle(ReferenceBase):
    """Tests referencing in vehicle"""

    def test_Referencing(self):
        """Testing Vehicle reference"""
        super(Test_Vehicle, self).test_Referencing()
