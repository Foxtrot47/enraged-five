import os

import pyfbsdk as mobu

from RS import Config, Globals
from RS import ProjectData
from RS.Core import ReferenceSystem
from RS.Utils import Scene
from RS.Utils.Scene import Character
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class classProperty(object):
    """
    Decorator to make a function a class level property
    """
    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        return self.fget(owner_cls)


class MocapGsSkeleton(MocapComplex):
    """
    Represents a mocap gs Skeleton reference in a scene
    """
    studio = ()
    PropertyTypeName = "MocapGsSkeleton"
    FormattedTypeName = "Mocap gs Skeleton"
    FileType = "fbx"

    @classProperty
    def TypePathList(cls):
        """
        This is a class property so that paths dont get baked into the class and are generated when needed
        """
        return ProjectData.data.GetAllGsSkeletonPaths()

    @Logger("Mocap GS Skeleton - Delete")
    def _delete(self):
        """
        This function is run when a gs skel is deleted
        """
        # remove the double namespaces in handle objects
        self._log.LogMessage("Checking and fixing double namespaces in handles.")
        self.FixHandleDoubleNamespace()

        # run the rest of the delete
        super(MocapGsSkeleton, self)._delete()

    def _create(self, filestate=None, importOptions=None):
        """
        This function is run when a gs skel is created
        """
        tempPath = self.StripSourceFileNamespace()
        # run the gs create
        if os.path.exists(tempPath):

            # merge options
            mergeOptions = ReferenceSystem.CreateBasicMergeOptions(tempPath)
            mergeOptions.NamespaceList = self.Namespace

            # merge
            Globals.Application.FileMerge(tempPath, False, mergeOptions)

            # refresh
            Globals.Scene.Evaluate()

            # update p4 version
            self.UpdateReferenceP4Version()

            # delete temp path
            if os.path.exists(tempPath):
                os.remove(tempPath)

            # add top gs null to CHARACTERS group
            mainMocapGroupList = [component for component in Globals.Components if component.LongName == "MOVE ALL" and isinstance(component, mobu.FBGroup)]

            if len(mainMocapGroupList) == 0:
                # update groups
                self._log.LogMessage("Updating camera groups into the previz groups")
                self.UpdateGroups()

            charMocapGroupList = [component for component in Globals.Components if component.LongName == "CHARACTERS" and isinstance(component, mobu.FBGroup)]

            if len(charMocapGroupList) == 0:
                return
            characterGroup = charMocapGroupList[0]

            # get skel_root to then find the top null
            skelRootList = [component for component in Globals.Components if component.LongName == ("{0}:SKEL_ROOT".format(self.Namespace)) and isinstance(component, mobu.FBModel)]
            if len(skelRootList) == 0:
                return
            skelRoot = skelRootList[0]

            gsParentNull = Scene.GetParent(skelRoot)

            if gsParentNull is None:
                return

            characterGroup.ConnectSrc(gsParentNull)

    def GetMobuCharacter(self):
        """
        Gets the FBCharacter components associated with the namespace.
        By default it gets the character component associated with this reference object.

        Return:
            pyfbsdk.FBCharacter
        """
        return self._getCharacterComponent()

    def _snapshotAssetsInReference(self):
        """Get a list of all the items in the Reference to Snapshot as FBModels

        Returns:
            list of FBModels
        """
        modelList = mobu.FBModelList()
        for obj in Character.GetComponentsFromCharacter(self.GetMobuCharacter()):
            if isinstance(obj, mobu.FBModelSkeleton):
                obj.GetSkinModelList(modelList)
        return [item for item in modelList]
