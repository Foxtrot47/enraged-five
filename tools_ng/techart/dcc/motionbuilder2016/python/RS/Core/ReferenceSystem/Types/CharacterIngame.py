import os

from RS.Core.Animation import Lib
from RS.Core.ReferenceSystem.Types.CharacterComplex import CharacterComplex
from RS.Utils import Namespace


class CharacterIngame(CharacterComplex):
    '''
    Represents an ingame character reference in a scene
    '''
    studio = ()
    PropertyTypeName = 'CharacterIngame'
    FormattedTypeName = 'Ingame Character'
    TypePathList = [os.path.join('art', 'animation', 'resources', 'characters', 'Models', 'ingame'),
                    os.path.join('art', 'dev', 'animation', 'resources', 'characters', 'Models', 'ingame')
                    ]
    FileType = 'fbx'

    def _create(self, filestate=None, importOptions=None):
        super(CharacterIngame, self)._create(filestate, importOptions)
        namespace = Namespace.GetNamespace(self.GetMobuCharacter())
        if namespace:
            Lib.ThumbJointOrientationToggle(namespace, overrideCS=True)
