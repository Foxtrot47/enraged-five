from RS.Core.ReferenceSystem.Types.CharacterComplex import CharacterComplex
from RS.Core.ReferenceSystem.Types.CharacterHelper import CharacterHelper
from RS.Core.ReferenceSystem.Types.CharacterCutscene import CharacterCutscene
from RS.Core.ReferenceSystem.Types.CharacterIngame import CharacterIngame
from RS.Core.ReferenceSystem.Types.CharacterAnimal import CharacterAnimal


class Character(CharacterComplex):
    '''
    Represents a character reference in a scene
    '''
    PropertyTypeName = 'Characters'
    FormattedTypeName = 'Character'
    TypePathList = []
    FileType = ''
    SubCats = [CharacterHelper, CharacterCutscene, CharacterIngame, CharacterAnimal]
