""" 
Usage:
    This contains logic for the BASE reference which is the abstract parent class for all other reference types
Author:
    Ross George
    Bianca Rudareanu
"""

import pyfbsdk as mobu

import os
import re
import unbind

from RS import Globals, Perforce
from RS.Utils import Namespace
from RS.Utils.Scene import Component, Groups
from RS.Core.Geometry import Snapshot
from RS.Core import ReferenceSystem
from RS.Core.ReferenceSystem import Exceptions
from RS.Core.ReferenceSystem.Decorators import Logger


class Base(object):
    '''
    Represents a reference in a scene. Abstract class that doesn't get initialised directly.
    '''
    SubCats = None

    def __init__(self, modelNull, log):
        '''
        Update reference data from an existing model null object.
        '''
        self._modelNull = modelNull
        self._log = log

        self._referencePathProperty = modelNull.PropertyList.Find(
            ReferenceSystem.STR_REFERENCE_PATH)
        self._namespaceProperty = modelNull.PropertyList.Find(
            ReferenceSystem.STR_NAMESPACE)
        self._updateProperty = modelNull.PropertyList.Find(
            ReferenceSystem.STR_UPDATE)
        self._p4VersionProperty = modelNull.PropertyList.Find(
            ReferenceSystem.STR_P4_VERSION)
        self._assetTypeProperty = modelNull.PropertyList.Find(
            ReferenceSystem.STR_RS_ASSET_TYPE)
        self._proxyAssetProperty = modelNull.PropertyList.Find(
            ReferenceSystem.STR_PROXY_ASSET)
        self._setupVersionProperty = modelNull.PropertyList.Find(
            ReferenceSystem.STR_SETUP_VERSION)
        self._latestVersion = None

    @Logger("Base - Creating Reference")
    def _create(self, filestate=None, importOptions=None):
        '''This is the function is called when a reference is created. The logic is slightly different when creating to
        when updating.

        Args:
            filestate (RSG.SourceControl.Perforce.FileState, optional): The P4 filestate.
            importOptions (FaceAnimDataImportOptions, optional): Face import options.
        '''
        mergeOptions = ReferenceSystem.CreateBasicMergeOptions(self.Path)
        mergeOptions.NamespaceList = self.Namespace

        self._log.LogMessage("Start merge of reference: {0}".format(self.Namespace))
        Globals.Application.FileMerge(self.Path, False, mergeOptions)
        self._log.LogMessage("Merge reference end.")

        # Update Reference P4 Version
        self.UpdateReferenceP4Version(fileState=filestate)

    def CreateSnapshot(self):
        """
        Snapshot the Reference Asset
        """
        return Snapshot.Snapshot.shapshotModels(self.GetReferenceModels())

    def GetSnapshots(self):
        snaps = []
        for child in self.ModelNull.Children:
            if child.PropertyList.Find("RefType").Data == "Snapshot":
                snaps.append(child)
        return snaps

    def GetReferenceModels(self):
        """
        Get a list of all the items in the Reference as FBModels

        returns:
            list of FBModels
        """
        contentsList = Namespace.GetContents(self.GetFBNamespace())
        return [comp for comp in contentsList if isinstance(comp, mobu.FBModel)]

    def GetFBNamespace(self):
        """
        Get the FB Namespace of the reference item

        returns:
            FBNamespace object
        """
        return Namespace.GetFBNamespace(self.Namespace)

    def _update(self, force=False, filestate=None):
        '''
        Preforms an update of the reference. This should be called via the manager
        as it will queue the sync commands and wrap the operation in some MergeTransaction calls.
        This is will likely be overloaded by each of the inherited class.
        '''

    def _delete(self):
        """
        Deletes the reference. This should be called via the manager
        as it will also delete the null and wrap multiple delete calls for efficency.
        """
        Namespace.Delete(self.Namespace, deleteNamespaces=True, additionalComponents=(self._modelNull,))

    @property
    def ModelNull(self):
        return self._modelNull

    @property
    def Mover(self):
        """
        cache mover object
        """
        # get fbnamespace and its contents
        fbNamespace = Namespace.GetFBNamespace(self.Namespace)
        contentsList = Namespace.GetContents(fbNamespace)

        # grab necessary components for setup
        for component in contentsList:
            # find mover
            if isinstance(component, mobu.FBModel):
                if component.Name == 'mover':
                    return component
        return

    @property
    def Name(self):
        '''
        Gets the name of the reference.
        '''
        return os.path.splitext(os.path.basename(self.Path))[0]

    @property
    def Path(self):
        '''
        Gets the path for the reference.
        '''
        return self._referencePathProperty.Data

    @Path.setter
    def Path(self, newPath):
        """
        Sets the path for the reference.
        """
        self._referencePathProperty.Data = newPath

    @property
    def Namespace(self):
        '''
        Gets the namespace for the reference.
        '''
        try:
            return self._modelNull.LongName.split(":", 1)[-1]
        except unbind.UnboundWrapperError:
            raise Exceptions.UnboundReference()

    @Namespace.setter
    def Namespace(self, namespace):
        self._namespaceProperty.Data = namespace
        if not namespace.startswith("RS_Null:"):
            namespace = "RS_Null:{}".format(namespace)
        self._modelNull.LongName = namespace

    @property
    def UpdateState(self):
        '''
        Gets the current update state
        '''
        return self._updateProperty.Data

    @property
    def SetupScriptVersion(self):
        '''
        Gets the namespace for the reference.
        '''
        if self._setupVersionProperty is None:
            return "1"

        # Set to 1 if data is empty or contains non-number values
        if not self._setupVersionProperty.Data or re.search("[^0-9]", self._setupVersionProperty.Data):
            self._setSetupScriptVersion("1")

        return self._setupVersionProperty.Data

    def _setSetupScriptVersion(self, value):
        '''
        Sets the CurrentVersion property on the modelNull.
        '''
        if self._setupVersionProperty is not None:
            self._setupVersionProperty.Data = str(value)

    @property
    def CurrentVersion(self):
        '''
        Gets the version num ber that is currently referenced in the scene.
        '''
        return int(self._p4VersionProperty.Data)

    def _setCurrentVersion(self, value):
        '''
        Sets the CurrentVersion property on the modelNull.
        '''
        self._p4VersionProperty.Data = str(value)

    def _setPath(self, value):
        '''
        Sets the Path property on the modelNull.
        '''
        self._referencePathProperty.Data = str(value)

    def ChangeLabelName(self, oldNamespace, newNamespace):
        """
        Virtual Method - need to be reimplemented in sub-classes when needed.
        For updating a Handle's Label string
        """
        pass

    def SetLatestVersionToNone(self):
        """
        set latestversion to None to force a check of perforce
        """
        self._latestVersion = None

    @property
    def LatestVersion(self):
        '''
        Gets the version number that is latest in perforce.
        '''
        if self._latestVersion is None:
            self._latestVersion = 0
            fileState = Perforce.GetFileState(self._referencePathProperty.Data)
            if fileState:
                self._latestVersion = int(fileState.HeadRevision)

        return self._latestVersion

    def Validate(self):
        '''
        Validates the reference (make sure path to the asset is the same with the project and other things)
        '''
        # ToDo implement logic here.

    def IsDestroyed(self):
        """ Has the reference been destroyed """
        return Component.IsDestroyed(self._modelNull)

    @property
    def IsProxy(self):
        '''
        Returns weather or not the asset is a proxy
        '''
        if not self._proxyAssetProperty:
            self._proxyAssetProperty = self._modelNull.PropertyCreate(
                ReferenceSystem.STR_PROXY_ASSET, mobu.FBPropertyType.kFBPT_bool, 'Bool', False, True, None)
            self._proxyAssetProperty.Data = 0
        return self._proxyAssetProperty.Data

    @IsProxy.setter
    def IsProxy(self, value):
        '''
        Turns the current asset into a proxy
        '''
        self._proxyAssetProperty.Data = value
        if value:
            self._proxySetup()

    def GetVisualModelList(self):
        '''
        Returns a list of all the geometry elements that are currently being used as skin for the vehicle
        '''
        namespace = Namespace.GetFBNamespace(self.Namespace)
        if not namespace:
            return []

        regex = re.compile('(?i)mover|text|rect|contact|ctrl|frm')
        # we are unable to pass FBMesh in GetContentsByType as FBMesh is the child of the FBModel
        # and doesnt seem to have a namespace, so wouldn't be included in the contents
        # polygoncount to 6 to avoid random objects the player has that isnt geo.
        return [model for model in Namespace.GetContentsByType(namespace, objectType=mobu.FBModel, exactType=False)
                if not regex.search(model.Name) and model.Geometry and type(model.Geometry) == mobu.FBMesh
                and model.Geometry.PolygonCount() > 6]

    def HideVisualModels(self):
        '''
        Hides all the skin models of the reference
        '''
        for visualModel in self.GetVisualModelList():
            visualModel.Show = False

    def UnhideVisualModels(self):
        '''
        Unhides all the skin models of the reference
        '''
        for visualModel in self.GetVisualModelList():
            visualModel.Show = True

    def SelectVisualModels(self):
        '''
        selects all of the skin models of the reference
        '''
        for visualModel in self.GetVisualModelList():
            visualModel.Selected = True

    def GetFileState(self):
        return Perforce.GetFileState(self.Path)

    def UpdateReferenceP4Version(self, fileState=None):
        '''
        Update Reference P4 Version of character
        Note: at the moment assets coming from GC's server cannot be found as we are unable to
        switch servers just yet. This is a temp fix until we can start switching.
        '''
        # Update Reference P4 Version of character
        version = self._p4VersionProperty.Data or '0'
        if os.path.exists(self.Path):
            fileState = fileState or Perforce.GetFileState(self.Path)
            if fileState:
                self._p4VersionProperty.Data = str(fileState.HaveRevision)
        self._p4VersionProperty.Data = version

    def getGroup(self):
        """
        Get the group for this reference item.

        Return:
             FBGroups for the reference asset
        """
        groups = Groups.GetGroupsFromNamespace(self.Namespace)
        if len(groups) > 0:
            return groups[0]
        return None
