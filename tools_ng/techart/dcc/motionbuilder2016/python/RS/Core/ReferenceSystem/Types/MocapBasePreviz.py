import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapBasePreviz(MocapComplex):
    """
    Represents a mocap base previz asset reference in a scene
    """
    studio = ()
    PropertyTypeName = "MocapBasePreviz"
    FormattedTypeName = "Mocap BasePreviz"
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "North", "BaseAssets"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "NYC", "BaseAssets"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "SD", "BaseAssets"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Stages", "Tor", "BaseAssets"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Global", "Lasers"),
                    os.path.join(Config.VirtualProduction.Path.Previz, "Global", "Gizmos"),]
    FileType = "fbx"

    def __init__(self, modelNull, log):
        """
        Call base class constructor
        """
        super(MocapBasePreviz, self).__init__(modelNull, log)

    @Logger("Mocap Base Previz - Create")
    def _create(self, filestate=None, importOptions=None):
        """
        This function gets run only when a character is created. Calls the base class and then
        fixes the UFC device.

        Args:
            importOptions (FaceAnimDataImportOptions, optional): face import options.
        """
        super(MocapBasePreviz, self)._create(filestate=filestate, importOptions=importOptions)

        # update groups
        self._log.LogMessage("Updating base previz groups with new base previz reference created.")
        self.UpdateGroups()
