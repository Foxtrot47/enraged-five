import os

from RS.Core.ReferenceSystem.Types.CharacterComplex import CharacterComplex


class CharacterAnimal(CharacterComplex):
    '''
    Represents an animal reference in a scene
    '''
    studio = ()
    PropertyTypeName = 'CharacterAnimal'
    FormattedTypeName = 'Animal'
    TypePathList = [os.path.join('art', 'animation', 'resources', 'characters', 'Models', 'Animals'),
                    os.path.join('art', 'dev', 'animation', 'resources', 'characters', 'Models', 'Animals')]
    FileType = 'fbx'
