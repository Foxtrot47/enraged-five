import os
from RS.Core.Animation import Lib
from RS.Utils import Namespace
from RS.Core.ReferenceSystem.Types.CharacterComplex import CharacterComplex


class CharacterCutscene(CharacterComplex):
    '''
    Represents a cutscene character reference in a scene
    '''
    studio = ()
    PropertyTypeName = 'CharacterCutscene'
    FormattedTypeName = 'Cutscene Character'
    TypePathList = [os.path.join('art', 'animation', 'resources', 'characters', 'Models', 'cutscene'),
                    os.path.join('art', 'dev', 'animation', 'resources', 'characters', 'Models', 'cutscene')
                    ]
    FileType = 'fbx'

    def _create(self, filestate=None, importOptions=None):
        super(CharacterCutscene, self)._create(filestate, importOptions)
        namespace = Namespace.GetNamespace(self.GetMobuCharacter())
        if namespace:
            Lib.ThumbJointOrientationToggle(namespace, overrideCS=True)