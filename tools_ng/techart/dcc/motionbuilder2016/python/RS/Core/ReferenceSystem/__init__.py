"""
Usage:
    Some core functions and constants related to the reference system

    Updating the version number cheat sheet:
        Order - Major.Minor.Patch

        Major - Change the public API of system, where existing code might no longer function or work due to the changes introduced
        Minor - Feature Additions
        Patch - Fix an issue where the public API has not changed
"""

__version__ = "2.1.0"

import pyfbsdk as mobu

#ModelNull core FBProperty names
STR_NAMESPACE = 'Namespace'
STR_RS_TYPE = 'rs_Type'
STR_RS_ASSET_TYPE = 'rs_Asset_Type'
STR_UPDATE = 'Update'
STR_P4_VERSION = 'P4_Version'
STR_BUG_NUMBERS = 'Bug Numbers'
STR_REFERENCE_PATH = 'Reference Path'
STR_UDP3DSMAX = 'UDP3DSMAX'
STR_FACE_REFERENCE = 'faceReference'
STR_TAKE_NAME = 'Take_Name'
STR_TARGET_NAMESPACE = 'Target_Namespace'
STR_IMPORT_OPTION = 'Import_Option'
STR_CREATE_MISSING = 'Create_Missing_Takes'
STR_OUTFIT = 'Outfit'
STR_PROXY_ASSET = 'ProxyAsset'
STR_VIDEO_PATH = 'Video Path'
STR_SETUP_VERSION = 'Setup Script Version'
STR_TRIAL_ID = 'Trial ID'

STATIC_COMPONENTS = ["GRP_R_ShoulderTwist", "GRP_L_ShoulderTwist"]
STATIC_PROPERTIES = [STR_UDP3DSMAX,  # UPD3DSMAX data should be maintained from import [2701439]
                     "Camera Limits",
                     "AspectW", "AspectH"
                     "Size",  # ignore size properties due to new hat rig url:bugstar:4508641,
                     # ignore bind pose default values url:bugstar:4926266
                     "Bind Pose Translation", "Bind Pose Rotation",
                     STR_SETUP_VERSION]
STATIC_PROPERTIES_ON_COMPONENTS = {"FPS_Cam": ["PreRotation"],
                                   "FPS_Cam_Preview": ["PreRotation"],
                                   # Maintain new ref values for CollisionKeys/AnimscriptList [url:bugstar:6435806]
                                   "Dummy01":  ["CollisionKeys", "ANIMSCRIPTLIST", "ANIMSCRIPTSET"]}


def CreateBasicMergeOptions(FilePath):

    #Setup merge options
    mergeOptions = mobu.FBFbxOptions(True, FilePath)
    mergeOptions.SetAll(mobu.FBElementAction.kFBElementActionMerge, False)
    for takeIndex in range(mergeOptions.GetTakeCount()):
        mergeOptions.SetTakeSelect(takeIndex, False)

    # For Bug 731622 turn story merge OFF when sync/merging files
    mergeOptions.Story  = mobu.FBElementAction.kFBElementActionDiscard
    mergeOptions.Lights = mobu.FBElementAction.kFBElementActionDiscard

    mergeOptions.BaseCameras = False #1195174
    mergeOptions.CameraSwitcherSettings = False
    mergeOptions.CurrentCameraSettings = False
    mergeOptions.CamerasAnimation = False
    mergeOptions.GlobalLightingSettings = False
    mergeOptions.ConstraintsAnimation = True #1854762
    mergeOptions.UpdateRecentFiles = False #This will stop the merged file getting added to the recent file list
    mergeOptions.CacheSize = 1572864000 #Set this to maximum (1500MB) as it does invariably help but never hinders
    mergeOptions.SetPropertyStaticIfPossible = False #2536974
    mergeOptions.EmbedMedia = False #2557110

    return mergeOptions
