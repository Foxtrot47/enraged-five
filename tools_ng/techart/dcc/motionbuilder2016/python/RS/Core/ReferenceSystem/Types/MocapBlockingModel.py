import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapBlockingModel(MocapComplex):
    """
    Represents a mocap blockingmodel reference in a scene
    """
    studio = ()
    PropertyTypeName = "MocapBlockingModel"
    FormattedTypeName = "Mocap BlockingModel"
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz, "Global", "BlockingModels")]
    FileType = "fbx"

    def __init__(self, modelNull, log):
        """
        Initialises the MOCAPBLOCKINGMODEL reference with the passed model null.
        Will call the base initializor as well.
        """
        # call base class constructor
        super(MocapBlockingModel, self).__init__(modelNull, log)

    @Logger("Mocap Blocking Model - Create")
    def _create(self, filestate=None, importOptions=None):
        """This function gets run only when a blocking model is created

        Args:
            filestate (RSG.SourceControl.Perforce.FileState, optional): The P4 filestate
            importOptions (FaceAnimDataImportOptions, optional): Face import options not used by water sim asset
        """
        super(MocapBlockingModel, self)._create()

        # align assets to active stage
        self._log.LogMessage("Aligning new blocking model to active stage.")
        self.AlignReferenceRootToActiveStage()
