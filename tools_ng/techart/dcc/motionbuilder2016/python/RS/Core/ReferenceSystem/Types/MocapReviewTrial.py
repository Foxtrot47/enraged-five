import os

import pyfbsdk as mobu

from RS import Globals, ProjectData
from RS.Core import ReferenceSystem
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger
from RS.Utils.Scene import Component, Character


# TODO refactor into a more generic location and import here.
class classProperty(object):
    """Decorator to make a function a class level property"""

    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        return self.fget(owner_cls)


class MocapReviewTrial(MocapComplex):
    """Represents a mocap trial after it has been refined, but before charmapping or motion editing has occurred."""

    PropertyTypeName = "MocapReviewTrial"
    FormattedTypeName = "Mocap Review Trial"
    FileType = "rsrefine.fbx"
    StoryFolderName = "MocapReviewTrial"

    @classProperty
    def TypePathList(cls):
        """This is a class property so that paths dont get baked into the class and are generated when needed"""
        return ProjectData.data.GetAllMocapReviewTrialPaths()

    @classmethod
    def GetStoryFolder(cls):
        """Find and return the story folder for all MocapReviewTrial assets if it exists.

        Returns:
            pyfbsdk.FBStoryFolder or None
        """
        for storyFolder in mobu.FBStory().RootFolder.Childs:
            if storyFolder.Name == cls.StoryFolderName:
                return storyFolder
        return None

    def GetStoryTrack(self, folder=None):
        """Find and return the story track for the asset if it exists.

        Args:
            folder (pyfbsdk.FBStoryFolder, optional): If provided, defines the folder to search within.

        Returns:
            pyfbsdk.FBStoryTrack or None
        """
        folder = folder or self.GetStoryFolder()
        tracks = list(folder.Tracks)
        for track in tracks:
            if track.Name == self.Namespace:
                return track
        return None

    def GetStoryClip(self, track=None):
        """Find and return the story clip for the asset if it exists.

        Args:
            track (pyfbsdk.FBStoryTrack, optional): If provided, defines the track to search within.

        Returns:
            pyfbsdk.FBStoryClip or None

        Warnings:
            * This is not designed to handle more than one clip per track.
        """
        track = track or self.GetStoryTrack()
        for clip in track.Clips:
            if clip.ClipAnimationPath == self.Path:
                return clip
        return None

    def _create(self, filestate=None, importOptions=None):
        """This function is run when the asset is created.

        Reimplemented from RS.Core.ReferenceSystem.Types.Base.

        Args:
            filestate (RSG.SourceControl.Perforce.FileState, optional): The P4 filestate.
            importOptions (FaceAnimDataImportOptions, optional): face import options.
        """
        initialTake = Globals.System.CurrentTake
        tempPath = self.StripSourceFileNamespace()
        if os.path.exists(tempPath):
            # Merge options.
            mergeOptions = ReferenceSystem.CreateBasicMergeOptions(tempPath)

            # Revise some options that make the trial import incorrectly
            mergeOptions.SetAll(mobu.FBElementAction.kFBElementActionMerge, True)
            for takeIndex in xrange(mergeOptions.GetTakeCount()):
                mergeOptions.SetTakeSelect(takeIndex, True)

            mergeOptions.NamespaceList = self.Namespace

            # Merge.
            Globals.Application.FileMerge(tempPath, False, mergeOptions)

            # Refresh.
            Globals.Scene.Evaluate()

            # Update p4 version.
            self.UpdateReferenceP4Version()

            # Delete temp path.
            if os.path.exists(tempPath):
                os.remove(tempPath)

        self.LoadAnimationIntoStory()

        # Create a null to group all of the refine items
        topLevelNull = mobu.FBModelNull(":".join([self.Namespace, self.Name]))
        refineGroup = mobu.FBGroup(topLevelNull.LongName)
        refineGroup.ConnectSrc(topLevelNull)
        filteredItems = []
        for child in self.GetReferenceModels():
            refineGroup.ConnectSrc(child)
            topItem = Character.GetTopLevelItem(child)
            if topItem not in filteredItems and topItem.Name != self.Name:
                filteredItems.append(topItem)
        for item in filteredItems:
            parentNull = mobu.FBModelNull("_".join([item.LongName, "null"]))
            item.Parent = parentNull
            parentNull.Parent = topLevelNull
            refineGroup.ConnectSrc(parentNull)

        # Delete imported take, restore original
        for take in Globals.Scene.Takes:
            if take.Name == self.Namespace and take != initialTake:
                take.FBDelete()
                Globals.System.CurrentTake = initialTake
                break

    @Logger("MocapReviewTrial - Delete")
    def _delete(self):
        """This function is run when the asset is deleted.

        Reimplemented from RS.Core.ReferenceSystem.Types.Base.
        """
        # Remove the double namespaces in handle objects.
        self._log.LogMessage("Checking and fixing double namespaces in handles.")
        self.FixHandleDoubleNamespace()

        # Remove story objects.
        mocapReviewFolder = self.GetStoryFolder()
        storyTrack = None
        storyClip = None
        if mocapReviewFolder:
            if len(list(mocapReviewFolder.Tracks)) > 1:  # Don't remove the folder if other trials have been referenced.
                mocapReviewFolder = None
            storyTrack = self.GetStoryTrack(folder=mocapReviewFolder)
            if storyTrack:
                storyClip = self.GetStoryClip(storyTrack)

        storyObjects = [item for item in (mocapReviewFolder, storyTrack, storyClip) if item]
        if storyObjects:
            Component.Delete(storyObjects, deleteNamespaces=False)

        # Run the rest of the delete.
        super(MocapReviewTrial, self)._delete()

    def LoadAnimationIntoStory(self, clipStartFrame=None):
        """Load the referenced FBX.

        Args:
            clipStartFrame (int, optional): If provided, set the clip start, else default to frame 0.

        Returns:
            tuple of pyfbsdk.FBStoryFolder, pyfbsdk.FBStoryTrack, pyfbsdk.FBStoryClip
        """
        self._log.LogMessage("Configuring story for MocapReviewTrial.")

        # Make sure Story is active.
        mobu.FBStory().Mute = False

        # Create story folder, if it doesnt exist.
        mocapReviewFolder = self.GetStoryFolder()
        if mocapReviewFolder is None:
            mocapReviewFolder = mobu.FBStoryFolder()
            mocapReviewFolder.Label = self.StoryFolderName

        # Create track and clip.
        newStoryTrack = mobu.FBStoryTrack(mobu.FBStoryTrackType.kFBStoryTrackAnimation, mocapReviewFolder)
        newStoryTrack.Name = self.Namespace
        models = self.GetReferenceModels()
        trackContentProperty = newStoryTrack.PropertyList.Find("Track Content")
        for item in models:
            trackContentProperty.ConnectSrc(item)

        animFilesystemPath = self.Path  # The referenced file has both the assets and the animation.
        newStoryClip = mobu.FBStoryClip(animFilesystemPath, newStoryTrack, mobu.FBTime(0, 0, 0, 0))
        if clipStartFrame is not None:
            newStoryClip.MoveTo(mobu.FBTime(0, 0, 0, clipStartFrame), True)

        return (mocapReviewFolder, newStoryTrack, newStoryClip)
