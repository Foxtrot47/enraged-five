from RS.Utils import Exception as RSException


class NotAnError(RSException.SilentError):
    """
    Raises if not really an error, but aborting silently for whatever reason
    """
    pass


class UnrecognisedReferenceDirectory(RSException.SilentError):
    """
    Raises if the user attempts to import a reference from an unknown directory

    Doesn't send an error, but will generate a popup notifying the user of the issue.

    Formatted in HTML format to support hub links for error code
    """
    def __init__(self, filePath):
        self.filePath = filePath

    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB101'>MB101</a></b><br>" \
               "<br>Unable to determine the type of reference:" \
               "<br>{0}" \
               "<br><br><b>The file you are attempting to add is not from a known Reference Directory.</b>" \
               "<br><br>Click error code for more info, else Bug Tech Art if you wish for this directory to be known to the Ref Editor." \
               "<br><br>A Reference has not been added to the scene.".format(self.filePath.lower())


class FilePathDoesNotExist(RSException.SilentError):
    """
    Raises if the user attempts to import a reference from an unknown directory
    """
    def __init__(self, filePath):
        self.filePath = filePath

    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB102'>MB102</a></b><br>" \
               "<b>The reference function you have run has been aborted - this reference path cannot be found:</b><br>{0}<br><br>Please check for the following potential reasons:" \
               "<br>-File has been deleted from Perforce<br>-You do not have access to the directory/project<br>-File is having sync issues - may need a force sync.".format(self.filePath)


class CharacterComponentMissing(RSException.SilentError):
    """
    Raises if the user attempts to run a function on a character reference where the namespace is
    missing from the character component - either it has a different name or doesn't exist at all.
    """
    def __init__(self, namespace=None, *args):
        super(RSException.SilentError, self).__init__(*args)
        self._namespace = namespace
    
    def __str__(self):
        namespaceInfo = ""
        if self._namespace:
            namespaceInfo = " \"{}\"".format(self._namespace)
        return ("<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB103'>MB103</a></b><br>" \
                "<b>There is no character component in the namespace{}.</b><br><br>This is a sign that the character is in a broken state and it is not possible to continue with update - please check over your scene."
                .format(namespaceInfo))


class MultipleCharacterComponents(RSException.SilentError):
    """
    Raises if the user attempts to run a function on a character reference where the namespace is
    present in mulitple character components - there should only be one.
    Example:
    Player_Zero:Player_Zero
    Player_Zero:player_zero
    """
    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB104'>MB104</a></b><br>" \
               "<b>There are too many Character components with the same namespace, unable to determine which one to run this function on.</b><br><br>Please check over your scene as something is wrong with the namespaces, most likely a casing issue."


class MissingReference(RSException.SilentError):
    """
    Raises if the user attempts to run a function and a reference cannot be found with the passed
    namespace.
    """
    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB105'>MB105</a></b><br>" \
               "<b>There are no reference with the passed namespace.</b><br><br>Please check over your scene for potential issues with the reference nulls or namespaces."


class UnsupportedConstraint(RSException.SilentError):
    """
    Raises if the user attempts to run a function on a reference with a constraint we are unable to
    support (found in major updates where we have to copy anims from one character to another).
    """
    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB106'>MB106</a></b><br>" \
               "<b>The reference you are trying to update has an unsupported constraint(s).</b><br><br>Supported Constraints include:<br>- Parent/Child<br>- Rotation\- Position<br>- Scale<br><br>Unable to proceed with Update."


class UnicodeError(RSException.SilentError):
    """
    Raises if there is a unicode encode error in a namespace.
    e.g. an extended dash "-" is recognised as "u'\u2013'", instead of a dash
    (small dashes are fine)
    UnicodeEncodeError: 'ascii' codec can't encode character u'\u2013' in position X: ordinal not in
    range(X)
    """
    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB107'>MB107</a></b><br>" \
               "<b>The reference you are trying to use contains a unicode in the string.</b><br><br>Unicode characters can be difficult to spot.  It is most likely found in the character" \
               "<br>componenet name. <br><br>Unable to proceed with process."


class StatedAnimationAnimFileNotFound(RSException.SilentError):
    """
    Raises if the statedAnimation added does not have a corresponding '_animation.fbx' version to bring in.
    """
    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB108'>MB108</a></b><br>" \
               "<b>The animation file you are trying to bring in does not exist.</b><br><br>Please ensure the Stated Anim has been run through the setup with Resource team" \
               "<br>correctly, this should have automatically generated a '[StatedAnimationname]_animation.fbx'file for you.<br><br>Unable to proceed with process."


class IncorrectRayfireFile(RSException.SilentError):
    """
    Raises if the Rayfire File added is the inncorect file, user has tried to add int he '_animation.fbx'
    version of the file.
    """
    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB109'>MB109</a></b><br>" \
               "<b?You are attempting to bring in the incorrect RayFire file.</b><br>You need to select the version of the RayFire without the '_animation.fbx' prefix in the file's basename." \
               "<br><br>Unable to proceed with process."


class StatedAnimationMissingDummy(RSException.SilentError):
    """
    Raises if the reference added does not have a Dummy01 node
    """
    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB110'>MB110</a></b><br>" \
               "<b>The statedAnimation file you're attempting to bring in is missing a Dummy01 node.</b><br>This is required for full setup in the scene with the Story Tracks." \
               "<br><br>Please check with Resource Team to have this asset set up correctly.<br><br>Unable to proceed with process."


class UnboundReference(RSException.SilentError):
    """
    Raises if the reference has been deleted from the scene
    """
    def __str__(self):
        return "<b>Error Code: <a href='https://hub.rockstargames.com/display/ANIM/Error+Codes#ErrorCodes-ErrorMB111'>MB111</a></b><br>" \
               "<b>Reference has become unbound</b><br>which means it has already been removed from the scene.<br><br>Aborting functon."


class MultipleReferenceErrors(RSException.SilentError):
    """
    Raised if multiple errors are thrown (e.g. within a loop) and then are reported to the user later.
    """
    pass
