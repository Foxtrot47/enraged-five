import os

import pyfbsdk as mobu

from PySide import QtGui

import RS.Core.ReferenceSystem as Core

from RS import Globals, Perforce
from RS.Utils import Namespace, Scene, ContextManagers
from RS.Utils.Scene import Component, Constraint, Plug
from RS.Core.ReferenceSystem import Exceptions
from RS.Core.ReferenceSystem import Decorators
from RS.Core.ReferenceSystem.Types.Base import Base


class Complex(Base):

    def __init__(self, modelNull, log):
        '''
        Constructor. Will call the base initializor as well.
        '''
        # Call base class constructor
        super(Complex, self).__init__(modelNull, log)

        # List of supported constraint types when updating character using _majorUpdate
        self._supportedConstraints = [Constraint.PARENT_CHILD,
                                      Constraint.ROTATION,
                                      Constraint.POSITION,
                                      Constraint.SCALE]

    @Decorators.Logger("Complex - Update")
    def _update(self, force=False, filestate=None):
        """ Updates the reference with latest data. Force does nothing here currently. """

        # Setup load options
        mergeOptions = Core.CreateBasicMergeOptions(self.Path)

        # Create a holding/temporary namespace
        tempNamespace = "__temp__"
        mergeOptions.NamespaceList = tempNamespace

        self._log.LogMessage("Merging file: {0}".format(self.Path))

        Globals.Application.FileMerge(self.Path, False, mergeOptions)

        self._log.LogMessage("Copying Existing Animation")

        Namespace.CopyData(self.Namespace, tempNamespace)

        Namespace.CopyAnimationData(self.Namespace, tempNamespace)

        # Now we will go through each component and if the source component is driving something else in a
        # constraint we will make sure it is swapped in the target component appropriately
        componentMatchList = Namespace.GetComponentMatchList(self.Namespace, tempNamespace, [mobu.FBModelSkeleton,
                                                                                             mobu.FBModel,
                                                                                             mobu.FBModelMarker,
                                                                                             mobu.FBModelRoot,
                                                                                             mobu.FBModelNull])
        for componentMatch in componentMatchList:
            for constraint in Globals.Constraints:
                # swap aim constraints
                if Constraint.IsConstraintType(constraint, [Constraint.AIM]):
                        Constraint.SwapAimConstraintModels(constraint,
                                                           componentMatch.SourceComponent,
                                                           componentMatch.TargetComponent)
            for constraint in Plug.GetDstPlugList(componentMatch.SourceComponent, mobu.FBConstraint):
                if Constraint.IsConstraintType(constraint, [Constraint.PARENT_CHILD,
                                                            Constraint.ROTATION,
                                                            Constraint.POSITION,
                                                            Constraint.SCALE]):
                    Constraint.SwapSourceModel(constraint,
                                               componentMatch.SourceComponent,
                                               componentMatch.TargetComponent)

        Namespace.Delete(self.Namespace, deleteNamespaces=True)
        Namespace.Rename(tempNamespace, self.Namespace)

        # Update Reference P4 Version
        fileState = filestate or Perforce.GetFileState(self.Path)
        if not isinstance(fileState, list):
            self._p4VersionProperty.Data = str(fileState.HaveRevision)

        if self.IsProxy:
            self._proxySetup()

    @Decorators.Logger("Complex - Stripping Reference")
    def _strip(self):
        '''
        Preforms a full strip of the reference.
        This will likely be overloaded by each of the inherited classes.
        '''

        # Delete visual models
        for visualModel in list(self.GetVisualModelList()):
            visualModel.FBDelete()

    @Decorators.Logger("Complex - Reloading Textures")
    def _reloadTextures(self):
        '''
        Pushes a refresh to all textures on the reference. Called after the manager has sync'ed some
        textures the user didn't have or was out of date on. Manager will batch calls for efficency
        with syncing on p4.
        '''
        # get video folder
        videoFolder = None
        for folder in Globals.Folders:
            if folder.LongName == "{0}:Videos".format(self.Namespace):
                videoFolder = folder
                break

        self._log.LogMessage("Reloading textures for reference: {0}".format(self.Namespace))
        namespace = Globals.Scene.NamespaceGet(self.Namespace)
        for textureComponent in Namespace.GetContentsByType(namespace, mobu.FBTexture):

            # Fist we check to see if the texture already has a video attached
            textureFilePath = None
            if textureComponent.Video is not None:
                textureFilePath = textureComponent.Video.Filename
                textureComponent.Video.FBDelete()

            # If it doesn't we need to create one drawing information from backup property
            elif textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH) is not None:
                textureFilePath = textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH).Data

            if textureFilePath is not None:
                #Check for tif version first - the more prefered option
                if os.path.exists(textureFilePath.replace('tga', 'tif')):
                    textureFilePath = textureFilePath.replace('tga', 'tif')
                if os.path.exists(textureFilePath):
                    if textureComponent.Video is None:
                        textureComponent.Video = mobu.FBVideoClipImage('{0}:{1}_video'.format(self.Namespace, textureComponent.Name))
                        if videoFolder:
                            textureComponent.Video.ConnectDst(videoFolder)

                    textureComponent.Video.Filename = textureFilePath
                    Globals.Scene.Evaluate()

    @Decorators.Logger("Complex - Strip Textures")
    def _stripTextures(self):
        '''
        Strips (deletes) textures from all the models for this reference. Called through
        the manager so calls to multiple refrences can be batched for efficency.
        '''
        self._log.LogMessage("Stripping textures for reference: {0}".format(self.Namespace))
        namespace = Globals.Scene.NamespaceGet(self.Namespace)

        for textureComponent in Namespace.GetContentsByType(namespace, mobu.FBTexture):
            if textureComponent.Video is not None:
                # Remember the texture name- this gests lost after we delete
                # the video and MOBU gaves it a generic name
                name = textureComponent.Name

                # Check to see if it has already a path property
                videoPathProperty = textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH)
                if videoPathProperty is None:
                    videoPathProperty = textureComponent.PropertyCreate(Core.STR_VIDEO_PATH,
                                                                        mobu.FBPropertyType.kFBPT_charptr,
                                                                        "String", False, True, None)
                videoPathProperty.Data = str(textureComponent.Video.Filename)

                #Remove the video
                textureComponent.Video = None

                # Rename it for consistency
                textureComponent.Name = name

    @Decorators.Logger("Complex - Proxy Setup")
    def _proxySetup(self):
        '''
        Turns the current reference into a proxy - a place holder asset colored pink in the scene
        '''
        self._log.LogMessage("Proxy setup for reference: {0}".format(self.Namespace))

        # strip textures and materials from asset
        textureList = Component.GetComponents(Namespace=self.Namespace,
                                              TypeList=[mobu.FBTexture,
                                                        mobu.FBMaterial,
                                                        mobu.FBVideo],
                                              DerivedTypes=False)

        for item in textureList:
            item.FBDelete()

        # iterate through list
        for model in self.GetVisualModelList():
            # get number of regions in mesh
            geometry = model.Geometry
            if geometry:
                geometryRegionList = set(geometry.GetMaterialIndexArray())
                numberRegions = 1
                if len(geometryRegionList) > 0:
                    numberRegions = max(geometryRegionList) + 1
                # create 1 material per region
                materialList = []
                for idx in range(numberRegions):
                    material = mobu.FBMaterial('{0}:ProxyMat_0'.format(model.Name))
                    material.Ambient = mobu.FBColor(1, 0, 1)
                    material.Diffuse = mobu.FBColor(1, 0, 1)

                    tag = material.PropertyCreate(
                        'ProxyAssetMaterial', mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)
                    tag.Data = model.Name

                    materialList.append(material)

                # apply materials to each region
                for material in materialList:
                    if material not in model.Materials:
                        model.Materials.append(material)

                # Create mat folder
                placeholder = mobu.FBMaterial('Remove_Me')
                folder = mobu.FBFolder("{0}:ProxyMaterials".format(model.Name), placeholder)
                placeholder.FBDelete()

                # Add materials to a folder
                for component in Globals.Components:
                    proxyMatProperty = component.PropertyList.Find(
                        'ProxyAssetMaterial')
                    if proxyMatProperty:
                        folder.ConnectSrc(component)

    @Decorators.Logger("Complex - Check Namespace")
    def CheckNamespaceConsistency(self):
        '''
        Sometimes models and textures obtain a different namespace to the reference(mobu bug perhaps)
        '''
        self._log.LogMessage("Checking Namespace for reference: {0}".format(self.Namespace))
        for texture in Scene.Model.GetTextureList(self.GetVisualModelList()):
            if Component.GetNamespace(texture) != self.Namespace:
                Component.SetNamespace(texture, self.Namespace)

    @Decorators.Logger("Complex - Fix Handle Double Namespace")
    def FixHandleDoubleNamespace(self):
        '''
        the handles have double namespaces in an autodesk error.  They are causing crashes when
        deleting them and then attempting to save. So we need to scan for double namespaces before
        removing the asset from the scene.
        '''
        self._log.LogMessage("Checking for Duplicated Namespaces on Handles")

        for handle in Globals.Handles:
            namespace = Component.GetNamespace(handle)
            if namespace is None:
                continue
            namespaceList = namespace.split(":")
            # check if the handle has multiple namespace of the same name
            if len(namespaceList) > 1 and namespaceList[0] == namespaceList[1]:
                handleNamespace = "{0}:{1}".format(namespaceList[0], namespaceList[1])
                handle.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBReplaceNamespace, handleNamespace, namespaceList[0], False)
                # delete the namespace related to the double handle namespace

                for namespace in Globals.Namespaces:
                    if namespace.LongName == handleNamespace:
                        namespace.FBDelete()

    def DisableRotDOF(self):
        '''
        Disable all Rotation DOFS
        '''
        if self.GetMover():
            dummyProp = self._modelNull.PropertyList.Find("Asset_Type")
            if dummyProp:
                rotDOFProp = self.GetMover().PropertyList.Find("Enable Rotation DOF")
                if rotDOFProp:
                    rotDOFProp.Data = False

    def GetMover(self):
        '''
        Retrieves the mover
        '''
        mover = mobu.FBFindModelByLabelName('{0}:mover'.format(self.Namespace))
        if mover == None:
            raise Exception("Missing mover model.")

        return mover

    def _getCharacterComponent(self):
        '''
        Gets the mobu.FBCharacter component for the character reference - should be 1 and only 1.
        '''
        if self.FormattedTypeName not in ['Cutscene Character', 'Ingame Character', 'Mocap gs Skeleton']:
            return

        characterComponents = [component for component in Globals.Characters
                               if Component.GetNamespace(component) == self.Namespace ]
        if len(characterComponents) > 1:
            raise Exceptions.MultipleCharacterComponents()
        elif len(characterComponents) < 1:
            raise Exceptions.CharacterComponentMissing(namespace=self.Namespace)
        else:
            return characterComponents[0]

    def PlotReferenceCurrentTake(self, allTakes=False):
        '''
        plots reference, not used for characters - character complex contains plotting for those
        '''
        if self.FormattedTypeName in ['Cutscene Character', 'Ingame Character', 'Mocap gs Skeleton']:
            return

        # deselect all
        Scene.DeSelectAll()

        # get related reference items
        componentList = [component for component in Globals.Components
                         if Component.GetNamespace(component) == self.Namespace
                         and isinstance(component, mobu.FBModel)]

        # select items
        for item in componentList:
            item.Selected = True

        # plot selected
        Scene.Plot.PlotCurrentTakeonSelected()

        # deselect all
        Scene.DeSelectAll()

    def PlotReferenceAllTakes(self, allTakes=False):
        '''
        plots reference, not used for characters - character complex contains plotting for those
        '''
        if self.FormattedTypeName in ['Cutscene Character', 'Ingame Character', 'Mocap gs Skeleton']:
            return
        system = mobu.FBSystem()
        # get original take
        currentTake = system.CurrentTake

        # deselect all
        Scene.DeSelectAll()

        for take in Globals.Takes:
            system.CurrentTake = take
            # get related reference items
            componentList = [component for component in Globals.Components
                             if Component.GetNamespace(component) == self.Namespace
                             and isinstance(component, mobu.FBModel)]

            # select items
            for item in componentList:
                item.Selected = True

            # plot selected
            Scene.Plot.PlotCurrentTakeonSelected()

        # deselect all
        Scene.DeSelectAll()

        # set back to original current take
        system.CurrentTake = currentTake

    def SetupPlotNull(self):
        """
        a feature for dealing with replacing multiple references in a scene, where a null is created
        and plotted to the control of the selected reference
        url:bugstar:4120474
        """

        referenceTypeControlDict = {"Ingame Character":"mover",
                                    "Cutscene Character":"mover",
                                    "Mocap gs Skeleton":"SKEL_ROOT",
                                    "Prop":"_control",
                                    "Vehicle":"chassis_control",
                                    }

        # get reference type
        refType = self.FormattedTypeName
        refNamespace = self.Namespace
        refName = self.Namespace.split("^")

        # get reference's main control
        if refType in referenceTypeControlDict.keys():
            propControl = None
            if refType == "Prop":
                # the reference is a prop. Different types of props will have differently names
                #'main controls'. We want to find the dummy node, then check the direct children for
                # the main control.
                for model in Globals.Scene.RootModel.Children:
                    if str(model.LongName.lower()) == str("{0}:dummy01".format(refNamespace.lower())):
                        dummy = model
                        for child in dummy.Children:
                            if referenceTypeControlDict[refType] in child.Name.lower():
                                propControl = child
                                break
                        break
            else:
                # reference is a character or vehicle, in which case, there is only one
                # possibility for the control
                for model in Globals.Models:
                    if model.LongName.lower() == "{0}:{1}".format(refNamespace.lower(), referenceTypeControlDict[refType]):
                        propControl = model
                        break

            if propControl is None:
                # unable to find control
                QtGui.QMessageBox.warning(None,
                                          "Reference Editor",
                                          "Unable to find the Control null for {0}".format(refNamespace),
                                          QtGui.QMessageBox.Ok)
                return

            # create null
            plotNull = mobu.FBModelNull("Plotted_Null:{0}".format(refNamespace))
            plotNull.Show = True

            # create parent/child constraint
            parentChildConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
            parentChildConstraint.Name = "{0}:plotted_null_PC".format(refNamespace)
            parentChildConstraint.ReferenceAdd (0, plotNull)
            parentChildConstraint.ReferenceAdd (1, propControl)
            parentChildConstraint.Active = True

            # plot null
            Scene.DeSelectAll()
            plotNull.Selected = True
            plotOptions = mobu.FBPlotOptions()
            plotOptions.PlotOnFrame = True
            plotOptions.UseConstantKeyReducer = True
            plotOptions.ConstantKeyReducerKeepOneKey = True
            plotOptions.PlotAllTakes = True
            plotOptions.PlotTranslationOnRootOnly = True
            plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll
            plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
            currentTake = mobu.FBSystem().CurrentTake
            currentTake.PlotTakeOnSelected(plotOptions)

            # delete constraint
            parentChildConstraint.FBDelete()

    def Validate(self):
        '''
        Validates the reference (make sure path to the asset is the same with the project and other things)
        '''
        self._getCharacterComponent()
        return True
