import pyfbsdk as mobu

from RS import Globals
from RS.Utils import Scene
from RS.Utils import Namespace


class ObjectSetData(object):
    def __init__(self,
                 targetProperty):
        self.targetModelFullName = targetProperty.GetOwner().FullName

        self.targetPropertyName = targetProperty.Name

        self.objectArray = [node.FullName
                            for node in targetProperty]

    def connectMetaData(self,
                        inputObject,
                        objectProperty):
        if inputObject in objectProperty:
            return

        inputObject.ConnectDst(objectProperty)

    def restoreConnection(self):
        targetNode = mobu.FBFindObjectByFullName(self.targetModelFullName)
        if targetNode is None:
            return

        targetProperty = targetNode.PropertyList.Find(self.targetPropertyName)
        if targetProperty is None:
            return

        for name in self.objectArray:
            node = mobu.FBFindObjectByFullName(self.targetModelFullName)

            if node is None:
                continue

            self.connectMetaData(node,
                                 targetProperty)


def StoreObjectSetConnections(inputNamespace):
    legalPropTypes = ('Prop_Capture')

    inputNodes = mobu.FBComponentList()
    mobu.FBFindObjectsByName('{0}:*'.format(inputNamespace),
                             inputNodes,
                             True,
                             True)

    inputNodes = list(inputNodes)
    if not inputNodes:
        return []

    props = [node
             for node in inputNodes
             if isinstance(node.PropertyList.Find('Asset_Type'),
                                                  mobu.FBPropertyString)]

    objsetSetValues = []

    for currentNode in props:
        assetProperty = currentNode.PropertyList.Find('Asset_Type')
        if assetProperty is None:
            continue

        if not isinstance(assetProperty,
                          mobu.FBPropertyString):
            continue

        if assetProperty.Data not in legalPropTypes:
            continue

        objsetSetValues.append(ObjectSetData(currentNode))

    return objsetSetValues