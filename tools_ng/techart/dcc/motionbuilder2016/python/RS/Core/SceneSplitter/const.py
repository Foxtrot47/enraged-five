import fbx

import pyfbsdk as mobu

POSES_FOLDER = "Poses"
SPLIT_POSE_FOLDER = "Scene Splitter"


class FrameRangeSettings(object):
    """Settings for frame range splitting"""

    MINIMUM_VERTICAL_SPLIT_RANGE = 100


class FbxToMobuEnums(object):
    """Fbx values matching mobu's enumerations"""

    FBX_INTERPOLATIONS = {
        fbx.FbxAnimCurveDef.eInterpolationConstant: mobu.FBInterpolation.kFBInterpolationConstant,
        fbx.FbxAnimCurveDef.eInterpolationLinear: mobu.FBInterpolation.kFBInterpolationLinear,
        fbx.FbxAnimCurveDef.eInterpolationCubic: mobu.FBInterpolation.kFBInterpolationCubic,
    }
    FBX_TANGENT_MODES = {
        fbx.FbxAnimCurveDef.eTangentAuto: mobu.FBTangentMode.kFBTangentModeAuto,
        fbx.FbxAnimCurveDef.eTangentTCB: mobu.FBTangentMode.kFBTangentModeTCB,
        fbx.FbxAnimCurveDef.eTangentUser: mobu.FBTangentMode.kFBTangentModeUser,
        fbx.FbxAnimCurveDef.eTangentUser | fbx.FbxAnimCurveDef.eTangentAuto: mobu.FBTangentMode.kFBTangentModeUser,
        fbx.FbxAnimCurveDef.eTangentGenericBreak: mobu.FBTangentMode.kFBTangentModeBreak,
        fbx.FbxAnimCurveDef.eTangentBreak: mobu.FBTangentMode.kFBTangentModeBreak,
        fbx.FbxAnimCurveDef.eTangentAutoBreak: mobu.FBTangentMode.kFBTangentModeBreak,
        fbx.FbxAnimCurveDef.eTangentGenericClamp: mobu.FBTangentMode.kFBTangentModeClampProgressive,
        fbx.FbxAnimCurveDef.eTangentGenericTimeIndependent: mobu.FBTangentMode.kFBTangentModeTimeIndependent,
        fbx.FbxAnimCurveDef.eTangentGenericClampProgressive: mobu.FBTangentMode.kFBTangentModeClampProgressive,
    }
    FBX_TANGENT_CONSTANTS = {
        fbx.FbxAnimCurveDef.eConstantStandard: mobu.FBTangentConstantMode.kFBTangentConstantModeNormal,
        fbx.FbxAnimCurveDef.eConstantNext: mobu.FBTangentConstantMode.kFBTangentConstantModeNext,
    }
    FBX_BLEND_MODES = {
        fbx.FbxAnimLayer.eBlendAdditive: mobu.FBLayerMode.kFBLayerModeAdditive,
        fbx.FbxAnimLayer.eBlendOverride: mobu.FBLayerMode.kFBLayerModeOverride,
        fbx.FbxAnimLayer.eBlendOverridePassthrough: mobu.FBLayerMode.kFBLayerModeOverridePassthrough,
    }

    FBX_TIME_MODES = {
        mobu.FBTimeMode.kFBTimeModeDefault: fbx.FbxTime.eDefaultMode,
        mobu.FBTimeMode.kFBTimeMode120Frames: fbx.FbxTime.eFrames120,
        mobu.FBTimeMode.kFBTimeMode100Frames: fbx.FbxTime.eFrames100,
        mobu.FBTimeMode.kFBTimeMode60Frames: fbx.FbxTime.eFrames60,
        mobu.FBTimeMode.kFBTimeMode50Frames: fbx.FbxTime.eFrames50,
        mobu.FBTimeMode.kFBTimeMode48Frames: fbx.FbxTime.eFrames48,
        mobu.FBTimeMode.kFBTimeMode30Frames: fbx.FbxTime.eFrames30,
        mobu.FBTimeMode.kFBTimeMode2997Frames_Drop: fbx.FbxTime.eNTSCDropFrame,
        mobu.FBTimeMode.kFBTimeMode2997Frames: fbx.FbxTime.eNTSCFullFrame,
        mobu.FBTimeMode.kFBTimeMode25Frames: fbx.FbxTime.ePAL,
        mobu.FBTimeMode.kFBTimeMode24Frames: fbx.FbxTime.eFrames24,
        mobu.FBTimeMode.kFBTimeMode1000Frames: fbx.FbxTime.eFrames1000,
        mobu.FBTimeMode.kFBTimeMode23976Frames: fbx.FbxTime.eFilmFullFrame,
        mobu.FBTimeMode.kFBTimeModeCustom: fbx.FbxTime.eCustom,
        mobu.FBTimeMode.kFBTimeMode96Frames: fbx.FbxTime.eFrames96,
        mobu.FBTimeMode.kFBTimeMode72Frames: fbx.FbxTime.eFrames72,
        mobu.FBTimeMode.kFBTimeMode5994Frames: fbx.FbxTime.eFrames59dot94,
    }
