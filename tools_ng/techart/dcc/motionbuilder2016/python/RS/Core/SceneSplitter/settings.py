import json
import os
import string
import re

from RS import Globals
from RS.Utils.Scene import Time
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types import Character, Prop, Vehicle
from RS.Core.SceneSplitter import exceptions, workflow, util


class SceneSplitSectionSettings(object):
    """
    Represents the settings data related to one "Split Scene" (e.g. which namespaces it has ownership of)
    """
    SPLIT_SUFFIX_PROPERTY_KEY = "SplitSuffix"
    SPLIT_BUG_ID_PROPERTY_KEY = "SplitSectionBugId"
    RESOURCE_NAMESPACES_PROPERTY_KEY = "ResourceNamespaces"
    SPLIT_FRAME_RANGE_PROPERTY_KEY = "FrameRange"

    def __init__(self, sectionData=None):
        """
        Initialise the `SceneSplitSectionSettings` instance.
        
        Args:
            sectionData (:obj:`dict`, optional): Dictionary of existing settings data for this split.
        """
        if isinstance(sectionData, dict):
            self._sectionData = sectionData
        else:
            self._sectionData = {}

    @property
    def splitSuffix(self):
        """str: The suffix for this split"""
        return self._sectionData.get(self.SPLIT_SUFFIX_PROPERTY_KEY, "")

    @splitSuffix.setter
    def splitSuffix(self, value):
        self._sectionData[self.SPLIT_SUFFIX_PROPERTY_KEY] = str(value)

    @property
    def splitSectionBugId(self):
        """int: The bugstar bug ID for this split"""
        return self._sectionData.get(self.SPLIT_BUG_ID_PROPERTY_KEY, "")

    @splitSectionBugId.setter
    def splitSectionBugId(self, value):
        self._sectionData[self.SPLIT_BUG_ID_PROPERTY_KEY] = int(value)
    
    @property
    def resourceNamespaces(self):
        """list of str: List of the namespaces owned by this split"""
        return self._sectionData.setdefault(self.RESOURCE_NAMESPACES_PROPERTY_KEY, [])

    @resourceNamespaces.setter
    def resourceNamespaces(self, resourceNamespacs):
        """Set resource namespaces

        Args:
            resourceNamespacs
        """
        self._sectionData[self.RESOURCE_NAMESPACES_PROPERTY_KEY] = list(resourceNamespacs)

    @property
    def splitFrameRange(self):
        """list of int: List of the frame ranges owned by this split"""
        return self._sectionData.setdefault(self.SPLIT_FRAME_RANGE_PROPERTY_KEY, Time.getSceneFrameRange())

    @splitFrameRange.setter
    def splitFrameRange(self, value):
        self._sectionData[self.SPLIT_FRAME_RANGE_PROPERTY_KEY] = value

    def getDataDict(self):
        """Returns the settings data for this split as a dictionary (e.g. for serialisation)"""
        return self._sectionData
    
    def getOutputFileName(self, baseFilePath):
        """
        Args:
            baseFilePath (str): The full file path to the base fbx for this split.
        
        Returns:
             str: The output path for this split.
        """
        fileName = os.path.basename(os.path.splitext(baseFilePath)[0])
        if not fileName:
            return None

        suffix = self.splitSuffix
        if not suffix:
            return None

        return "{}_{}_{}-{}.fbx".format(fileName, suffix, self.splitFrameRange[0], self.splitFrameRange[1])


class SplitFolderSettings(object):
    """
    Represents the settings relating to a particular split "set". Split sets are each output to a separate sub-folder.
    """
    SPLIT_FOLDER_NAME_PROPERTY_KEY = "SplitFolderName"
    PASS_BUG_ID_PROPERTY_KEY = "PassBugId"
    SPLIT_SECTIONS_PROPERTY_KEY = "SplitSections"
    SPLIT_FRAME_RANGE_PROPERTY_KEY = "FrameRange"
    SPLIT_SUFFIX_PROPERTY_KEY = "SplitSuffix"

    def __init__(self, splitFolderData=None):
        if isinstance(splitFolderData, dict):
            self._splitFolderData = splitFolderData
        else:
            self._splitFolderData = {}

        splitSectionsData = self._splitFolderData.pop(self.SPLIT_SECTIONS_PROPERTY_KEY, [])
        # sort by suffix then frame range
        splitSectionsData.sort(
            key=lambda sectionData: (sectionData.get(self.SPLIT_SUFFIX_PROPERTY_KEY),
                                     sectionData.get(self.SPLIT_FRAME_RANGE_PROPERTY_KEY))
        )
        self._splitSections = [SceneSplitSectionSettings(sectionData) for sectionData in splitSectionsData]

    @property
    def splitFolderName(self):
        """str: The name of the split folder"""
        return self._splitFolderData.get(self.SPLIT_FOLDER_NAME_PROPERTY_KEY)

    @splitFolderName.setter
    def splitFolderName(self, value):
        self._splitFolderData[self.SPLIT_FOLDER_NAME_PROPERTY_KEY] = value
    
    @property
    def passBugId(self):
        """int: The 'pass' (parent) bug relating to all splits within the folder."""
        return self._splitFolderData.get(self.PASS_BUG_ID_PROPERTY_KEY)
    
    @passBugId.setter
    def passBugId(self, value):
        self._splitFolderData[self.PASS_BUG_ID_PROPERTY_KEY] = int(value)

    @property
    def splitFrameRange(self):
        """list of int: frame range for the scene"""
        return Time.getSceneFrameRange()

    @splitFrameRange.setter
    def splitFrameRange(self, value):
        self._splitFolderData[self.SPLIT_FRAME_RANGE_PROPERTY_KEY] = list(value)

    @property
    def splitSections(self):
        """list of SplitSectionSettings: The settings for the splits that this folder contains."""
        return self._splitSections
    
    def getDataDict(self):
        """Returns the settings data for this folder as a dictionary (e.g. for serialisation)"""
        # ensure split range is added
        self.splitFrameRange

        # Shallow copy to allow appending the dictionary-ified split sections without modifying internal data
        splitSetData = self._splitFolderData.copy()
        splitSectionsData = [splitSection.getDataDict() for splitSection in self._splitSections]
        splitSetData[self.SPLIT_SECTIONS_PROPERTY_KEY] = splitSectionsData
        return splitSetData


class SceneSplitSettings(object):
    """
    Represents the settings data defining how an FBX scene should be split and merged.
    """
    FORMAT_VERSION = 2
    DATA_FORMAT_VERSION_PROPERTY_KEY = "FormatVersion"
    SPLIT_FOLDERS_PROPERTY_KEY = "SplitFolders"
    ACTIVE_SPLIT_FOLDER_INDEX_KEY = "ActiveSplitFolderIndex"
    SPLIT_SETTINGS_FILE_EXT = ".splitsettings.json"

    def __init__(self, activeWorkflow=None):
        """
        Initialises the SceneSplitSettings instance.
        
        Args:
            activeWorkflow (workflow.SplitWorkflowBase): The current workflow to use for naming conventions etc.
        """
        self._refManager = Manager.Manager()
        
        if not isinstance(activeWorkflow, workflow.SplitWorkflowBase):
            activeWorkflow = workflow.SplitWorkflowBase()
        self.activeWorkflow = activeWorkflow

        self._splitFolders = None
        self._activeSplitFolderIndex = None
        self.reset()
    
    @classmethod
    def newFromFile(cls, activeWorkflow=None):
        """
        Static constructor: Creates a new instance and loads any existing settings data for the current scene.
        
        Args:
            activeWorkflow (workflow.SplitWorkflowBase): The current workflow to use for naming conventions etc.
        
        Returns:
            SceneSplitSettings: New instance of this class.
        """
        inst = cls(activeWorkflow=activeWorkflow)
        
        # Initial load of any existing data
        inst.loadFromFile()

        return inst
    
    def _fillMinDefaultFolders(self):
        """
        Creates the default split folders according to the current workflow's naming conventions.
        """
        curFolderCount = self.splitFolderCount
        minFolderCount = self.activeWorkflow.getMinFolderCount()
        if curFolderCount >= minFolderCount:
            return
        
        for splitFolderIdx in xrange(curFolderCount, minFolderCount):
            folderName = self.activeWorkflow.generateDefaultFolderName(splitFolderIdx)
            self.addNewSplitFolder(folderName)
    
    def reset(self):
        """Clear all split configuration back to its initial blank state."""
        self._splitFolders = []
        self._activeSplitFolderIndex = 0
        
        self._fillMinDefaultFolders()

    @property
    def splitFrameRange(self):
        """list of int: frame range for the scene"""
        return self._splitFolders[self._activeSplitFolderIndex].splitFrameRange

    @property
    def passBugId(self):
        """int: The 'pass' bug ID for the active splits folder"""
        if not self._splitFolders:
            return None
        
        return self._splitFolders[self._activeSplitFolderIndex].passBugId

    @property
    def splitSections(self):
        """list of SplitSectionSettings: The settings for the splits that the active split folder contains."""
        if not self._splitFolders:
            return []
        
        return self._splitFolders[self._activeSplitFolderIndex].splitSections
    
    def _ensureSplitFolderNameIsUnique(self, name):
        """
        Ensures that the split folder name is unique.
        
        Args:
            name (str): The initial name that should be made unique.
        """
        if not name:
            raise ValueError("The name parameter must be a non-empty string")
        
        existingNames = frozenset(self.getAllSplitFolderNames())
        
        if name not in existingNames:
            return name

        padding = 1
        numberToAppend = 1
        origName = name

        # If the suffix already ends with a number then start searching for a unique variant from that value
        tailNumbers = re.search(r"(\d+)$", origName)
        if tailNumbers:
            padding = len(tailNumbers.group(0))
            numberToAppend = int(tailNumbers.group(0)) + 1
            # Strip existing trailing digits so we can replace them
            origName = origName.rstrip(string.digits)

        # Loop until we find a number suffix that's not already taken
        while name in existingNames:
            name = "{}{}".format(origName, str(numberToAppend).zfill(padding))
            numberToAppend += 1
        
        return name
    
    def addNewSplitFolder(self, name=None):
        """
        Adds a new split folder to the settings data.
        
        Args:
            name (:obj: `str`, optional): The name for the new split folder. If None, a new name will be generated.
        """
        if not name:
            nextIdx = self.splitFolderCount
            name = self.activeWorkflow.generateDefaultFolderName(nextIdx)
        
        splitFolderName = self._ensureSplitFolderNameIsUnique(name)

        newSplitFolder = SplitFolderSettings()
        newSplitFolder.splitFolderName = splitFolderName
        self._splitFolders.append(newSplitFolder)
    
    def renameSplitFolder(self, newName, splitFolderIndex=None):
        """
        Renames a split folder.
        
        Args:
            newName (str): The new name for the folder.
            splitFolderIndex (:obj: `int`, optional): The index of the folder to rename. If None, the active folder
                index will be renamed.
        """
        if splitFolderIndex is None:
            splitFolderIndex = self._activeSplitFolderIndex
        
        if newName == self._splitFolders[splitFolderIndex].splitFolderName:
            return
        
        self._splitFolders[splitFolderIndex].splitFolderName = self._ensureSplitFolderNameIsUnique(newName)

    def removeSplitFolder(self, splitFolderIndex=None):
        """
        Removes all settings related to a split folder.
        
        Args:
            splitFolderIndex (:obj: `int`, optional): The index of the folder to remove. If None, the active folder
                index will be removed.
        """
        if splitFolderIndex is None:
            splitFolderIndex = self._activeSplitFolderIndex

        self._splitFolders.pop(splitFolderIndex)
    
    def getSplitFolderNameByIndex(self, folderIndex):
        """Returns the name of the split folder with the given index"""
        return self._splitFolders[folderIndex].splitFolderName
    
    def getAllSplitFolderNames(self):
        """Returns a list of all the split folder names contained within the settings data."""
        return [splitSet.splitFolderName for splitSet in self._splitFolders]
    
    @property
    def splitFolderCount(self):
        """int: Number of split folders"""
        return len(self._splitFolders)
    
    @property
    def activeSplitFolderIndex(self):
        """int: Index of the active split folder."""
        return self._activeSplitFolderIndex

    @activeSplitFolderIndex.setter
    def activeSplitFolderIndex(self, newIndex):
        highestValidIndex = self.splitFolderCount - 1
        if newIndex < 0 or newIndex > highestValidIndex:
            msg = "Split folder index of {} is invalid (valid range is from 0 to {})".format(newIndex,
                                                                                             highestValidIndex)
            raise IndexError(msg)

        self._activeSplitFolderIndex = newIndex
    
    @property
    def activeSplitFolderName(self):
        """str: The name of the active split folder"""
        if not self._splitFolders:
            return None
        
        return self._splitFolders[self._activeSplitFolderIndex].splitFolderName
    
    @classmethod
    def getSplitSettingsFilePath(cls):
        """Returns the path to the split settings file for the current scene"""
        baseFbxFilePath = Globals.Application.FBXFileName
        if not baseFbxFilePath:
            return None
        splitSettingsFilePath = os.path.splitext(baseFbxFilePath)[0] + cls.SPLIT_SETTINGS_FILE_EXT
        return splitSettingsFilePath

    def getSplitFileOutputFolder(self, baseFilePath):
        """
        Returns the path to the directory that split fbx files will be output to.
        
        Args:
            baseFilePath (str): Base fbx that the splits output folder should be found for.
        
        Returns:
            str: Path to splits output folder.
        """
        if not baseFilePath:
            return None
        
        currentDir = os.path.dirname(baseFilePath)
        
        return os.path.join(currentDir, self.activeSplitFolderName)

    def getAllSplitFileOutputPaths(self, baseFilePath):
        """
        Returns a list of fbx output paths - one for each of the splits that have been set up in the current data.
        
        Args:
            baseFilePath: Base fbx that the split output paths should be relative to.

        Returns:
            list of str: List of all split output fbx paths.

        """
        outputPaths = []
        outputFolder = self.getSplitFileOutputFolder(baseFilePath)
        if not outputFolder:
            return outputPaths
        
        for sectionSettings in self.splitSections:
            if not sectionSettings.resourceNamespaces:
                # Ignore splits that don't contain any references
                continue

            splitFbxFileName = sectionSettings.getOutputFileName(baseFilePath)
            if not splitFbxFileName:
                continue
            outputPaths.append(os.path.join(outputFolder, splitFbxFileName))
        
        return outputPaths

    def getReferenceFromNamespace(self, namespace):
        """Gets a reference by namespace

        Args:
            namespace (str): Name of the namespace

        Returns:
            ReferenceSystem.Types: Reference asset
        """
        return self._refManager.GetReferenceByNamespace(namespace)

    def getAllSceneNamespaces(self):
        """Returns a frozenset containing of all reference namespaces in the current scene"""
        return frozenset(self._refManager.GetNamespaceList(None))

    def getAllSplittableNamespaces(self):
        """Return namespaces containing assets that are relevant to animators."""
        characterNamespaces = self.getNamespacesWithReferenceType(Character.Character)
        propNamespaces = self.getNamespacesWithReferenceType(Prop.Prop)
        vehicleNamespaces = self.getNamespacesWithReferenceType(Vehicle.Vehicle)
        return characterNamespaces | propNamespaces | vehicleNamespaces

    def getNamespacesWithReferenceType(self, refType):
        """
        Returns a set of all namespaces in the current scene which contain a reference of the given type
        
        Args:
            refType (ReferenceSystem.Types.Base.Base): The reference type to be searched for.
        
        Return:
            frozenset: Set containing namespaces with references of the given type.
        """
        matchingReferences = self._refManager.GetReferenceListByType(refType)
        matchingReferences.sort(key=lambda ref: ref.Namespace)
        return frozenset([ref.Namespace for ref in matchingReferences])

    def _serialize(self, pretty=False):
        """Serialise the current settings objects to a json string"""
        # Gather section data as dicts to serialise
        splitFolders = []
        for splitFolder in self._splitFolders:
            splitFolders.append(splitFolder.getDataDict())

        # Build settings dict
        settingsData = {self.DATA_FORMAT_VERSION_PROPERTY_KEY: self.FORMAT_VERSION,
                        self.ACTIVE_SPLIT_FOLDER_INDEX_KEY: self.activeSplitFolderIndex,
                        self.SPLIT_FOLDERS_PROPERTY_KEY: splitFolders}

        if pretty:
            return json.dumps(settingsData, indent=4, sort_keys=True)
        else:
            return json.dumps(settingsData)
    
    def _deserialize(self, rawData):
        """
        Initialise settings objects from the json string data
        
        Args:
            rawData (str): A string containing serialised settings data in json format.
        """
        try:
            settingsDataDict = json.loads(rawData)
        except Exception as ex:
            raise exceptions.SceneSplitSettingsIOError("Failed to load settings from json: \"{}\"".format(ex))

        dataVersion = settingsDataDict.get(self.DATA_FORMAT_VERSION_PROPERTY_KEY, 1)
        if dataVersion > self.FORMAT_VERSION:
            msg = "Data format version ({}) is higher than loader version ({})".format(dataVersion, self.FORMAT_VERSION)
            raise exceptions.SceneSplitSettingsIOError(msg)

        splitFoldersDataList = settingsDataDict.get(self.SPLIT_FOLDERS_PROPERTY_KEY)
        if splitFoldersDataList:
            self._splitFolders = []
            for splitFolderData in splitFoldersDataList:
                self._splitFolders.append(SplitFolderSettings(splitFolderData))

            self._fillMinDefaultFolders()
        
            # Set the active folder to match what was last saved
            splitFolderIndexData = settingsDataDict.get(self.ACTIVE_SPLIT_FOLDER_INDEX_KEY, 0)
            self.activeSplitFolderIndex = min(self.splitFolderCount - 1, splitFolderIndexData)

    def writeToFile(self, overwrite=True):
        """
        Serialise and write all settings data to a file on disk.
        
        Args:
            overwrite (bool): If True the operation will attempt to overwrite any existing file.
                Otherwise an exception will be raised if the file already exists.
                
        Raises:
            exceptions.SceneSplitSettingsIOError: Raised if writing the settings to disk fails.
        """
        splitSettingsFilePath = self.getSplitSettingsFilePath()
        if not splitSettingsFilePath:
            return
        
        if not overwrite and os.path.isfile(splitSettingsFilePath):
            raise exceptions.SceneSplitSettingsIOError("Unable to overwrite existing settings file")
        
        # Save settings to file
        rawData = self._serialize(pretty=True)
        if not rawData:
            return

        try:
            with open(splitSettingsFilePath, "w") as settingsFile:
                settingsFile.write(rawData)
        except IOError as ex:
            raise exceptions.SceneSplitSettingsIOError("Failed to write split settings to file: {}".format(ex))

    def loadFromFile(self):
        """
        Loads settings data relating to the current Mobu scene from a file on disk.
        If the file does not exist the settings will be emptied.
        If the file exists but contains invalid data an exception will be raised.

        Raises:
            exceptions.SceneSplitSettingsIOError: Raised if an error occurs while attempting to load the data.
        """
        # clear any existing data back to defaults
        self.reset()
        
        # Load settings from file
        splitSettingsFilePath = self.getSplitSettingsFilePath()
        if not splitSettingsFilePath or not os.path.isfile(splitSettingsFilePath):
            # Return cleanly if the file doesn't exist - no settings have been created yet
            return

        with open(splitSettingsFilePath, "r") as settingsFile:
            rawData = settingsFile.read()
        
        if rawData:
            self._deserialize(rawData)
