import re


class SplitWorkflowBase(object):
    """
    Base for workflow specific settings and naming conventions
    """
    BASE_FOLDER_PREFIX = "Splits_"

    def __init__(self):
        self._workflowName = "Default"
        self._defaultSplitFolderNameRegex = re.compile(r"^{}\d+$".format(re.escape(self.BASE_FOLDER_PREFIX)))
        self._minFolderCount = 1

    def getWorkflowName(self):
        """Returns the name of this workflow e.g. for display in UI"""
        return self._workflowName

    def getDefaultSplitFolderNameRegex(self):
        """
        Returns a compiled regular expression that will match split folder names generated inline
        with this workflow's naming conventions.
        
        Returns:
            re.RegexObject: The compiled regular expression object.
        """
        return self._defaultSplitFolderNameRegex

    def getMinFolderCount(self):
        """
        Returns the minimum number of split folders that should be generated while using this workflow.
        Returns:
            int: The minimum number of folders that should be created.
        """
        return self._minFolderCount

    def generateDefaultFolderName(self, splitFolderIndex):
        """
        Generates a default name for the split folder at the given index.
        
        Args:
            splitFolderIndex (int): The index of the folder that a name should be generated for.

        Returns:
            str: The generated default name for the folder at the given index.
        """
        number = splitFolderIndex + 1
        return "{}{}".format(self.BASE_FOLDER_PREFIX, number)


class CinematicPassesSplitWorkflow(SplitWorkflowBase):
    """
    Settings and naming conventions specific to the Cinematic Passes split workflow
    """
    NUMBER_SUFFIXES = {1: "st", 2: "nd", 3: "rd"}
    CINEMATIC_PASS_FOLDER_SUFFIX = "pass_Split"

    def __init__(self):
        self._workflowName = "Cinematic Passes"
        pattern = r"^[1-9][0-9]*\w\w{}$".format(re.escape(self.CINEMATIC_PASS_FOLDER_SUFFIX))
        self._defaultSplitFolderNameRegex = re.compile(pattern)
        self._minFolderCount = 2

    def generateDefaultFolderName(self, splitFolderIndex):
        """
        Generates a default name for the split folder at the given index.

        Args:
            splitFolderIndex (int): The index of the folder that a name should be generated for.

        Returns:
            str: The generated default name for the folder at the given index.
        """
        number = splitFolderIndex + 2  # Pass folders must start from "2nd" apparently...
        return "{}{}{}".format(number, self.NUMBER_SUFFIXES.get(number, "th"), self.CINEMATIC_PASS_FOLDER_SUFFIX)
