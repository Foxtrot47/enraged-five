import re
import os
import fbx

import pyfbsdk as mobu

import collections
from RS import ProjectData, Globals
from RS.Utils import Namespace, ContextManagers
from RS.Utils.Scene import Model, Time
from RS.Core.Animation import Lib as AnimLib
from RS.Core.ReferenceSystem.Types import Prop, Vehicle
from RS.Core.ReferenceSystem import Manager, Types
from RS.Core.SceneSplitter import exceptions, const
from RS.Core.Fbx import fbxProcessor


keyTangent = collections.namedtuple(
    "keyTangent", ["key", "leftDerivative", "rightDerivative", "leftTangent", "rightTangent"]
)


def _getControlsSearcher(namespace):
    refManager = Manager.Manager()
    reference = refManager.GetReferenceByNamespace(namespace)
    if not reference:
        return None

    rootControlName = None
    stopRecursionPattern = None
    excludePattern = None
    forceIncludePattern = None
    disableFilterPattern = None

    if isinstance(reference, ProjectData.data.CharacterReferenceBaseType()):
        (
            rootControlName,
            stopRecursionPattern,
            excludePattern,
            forceIncludePattern,
            disableFilterPattern,
        ) = ProjectData.data.CharacterAnimationControlPatterns(reference.Name)
    elif isinstance(reference, Prop.Prop):
        (
            rootControlName,
            stopRecursionPattern,
            excludePattern,
            forceIncludePattern,
            disableFilterPattern,
        ) = ProjectData.data.PropAnimationControlPatterns()
    elif isinstance(reference, Vehicle.Vehicle):
        (
            rootControlName,
            stopRecursionPattern,
            excludePattern,
            forceIncludePattern,
            disableFilterPattern,
        ) = ProjectData.data.VehicleAnimationControlPatterns()

    if rootControlName is None:
        return None

    controlsSearcher = Model.HierarchyRegexSearcher(
        rootControlName,
        excludePattern=excludePattern,
        forceIncludePattern=forceIncludePattern,
        stopRecursionPattern=stopRecursionPattern,
        disableFilterPattern=disableFilterPattern,
    )
    return controlsSearcher


def getAnimControls(namespace):
    """Gets the anim controls relative to the namespace

    Args:
        namespace (str): The namespace to search for animation controls within

    Returns:
        list: The list of controls
    """
    controlsSearcher = _getControlsSearcher(namespace)
    if controlsSearcher is None:
        return []

    return [control for control in controlsSearcher.iterMatchingObjects(namespace)]


class SceneSplitFbxSdkProcessor(fbxProcessor.BaseFbxProcessorContext):
    """
    Class used to load FBX scenes using the FBXSDK and perform Scene Split related operations on them.
    """

    LOG_CONTEXT = "SplitSceneProcessor"
    EXCEPTION_TYPE = exceptions.SceneSplitToolError
    MARKUP_MATERIAL_NAME = "SplitScene_InactiveAsset_Material"

    def _setDefaultIoSettings(self):
        super(SceneSplitFbxSdkProcessor, self)._setDefaultIoSettings()
        ioSettings = self.getIoSettings()
        ioSettings.SetBoolProp(fbx.EXP_FBX_MATERIAL, True)
        ioSettings.SetBoolProp(fbx.EXP_FBX_TEXTURE, True)
        ioSettings.SetBoolProp(fbx.EXP_FBX_SHAPE, True)
        ioSettings.SetBoolProp(fbx.EXP_FBX_GOBO, True)
        ioSettings.SetBoolProp(fbx.EXP_FBX_ANIMATION, True)
        ioSettings.SetBoolProp(fbx.EXP_FBX_GLOBAL_SETTINGS, True)

    @staticmethod
    def _getNamespaceFromNode(fbxObject):
        """
        Returns the namespace for the given object.

        Args:
            fbxObject (fbx.FbxObject): Object to find the namespace from.

        Returns:
            str: The namespace for the given object. An empty string will be returned if the object has no namespace.
        """
        name = fbxObject.GetName()
        if name and ":" in name:
            return name.rsplit(":", 1)[0]
        return ""

    def sceneContainsCharactersWithActiveRigs(self, targetNamespaces=None):
        """
        Returns True if there is at least one character with a ControlRig as its active input in the loaded scene.

        Keyword Args:
            targetNamespaces (list of str): If specified, only the namespaces in this list will be checked.
                Otherwise all namespaces in the scene will be checked.
        """
        if targetNamespaces is not None and not isinstance(targetNamespaces, list):
            raise TypeError("targetNamespaces must be a list or None")

        self._logDebug("Checking for active control rigs in: {}".format(os.path.basename(self.fbxFilePath)))

        for charIdx in xrange(self.fbxScene.GetCharacterCount()):
            character = self.fbxScene.GetCharacter(charIdx)

            if targetNamespaces is not None:
                characterNamespace = self._getNamespaceFromNode(character)
                if characterNamespace not in targetNamespaces:
                    # Character does not belong to one of the target namespaces
                    continue

            inputIsRig = character.GetInputType() == fbx.FbxCharacter.eInputMarkerSet
            if not inputIsRig:
                # Character's input type is not a rig
                continue

            activeSourceProp = character.FindProperty("Active")  # Determines whether the input is active in Mobu
            if activeSourceProp.IsValid():
                activeSourceProp = fbx.FbxPropertyBool1(activeSourceProp)
                if activeSourceProp.IsValid() and activeSourceProp.Get() is False:
                    # Input is not active
                    continue

            self._logDebug("Character with active control rig found: {}".format(character.GetName()))
            return True

        self._logDebug("No characters with active control rigs found")
        return False

    def _findNamespacesBelongingToAssetType(self, assetTypeName, targetNamespaces=None):
        """
        Returns a list of namespaces in the scene that are owned by an asset type matching the given type name.

        Args:
            assetTypeName (str): The rs_Asset_Type name that should be searched for.
            targetNamespaces (list of str): If specified, only the namespaces in this list will be checked.
                Otherwise all namespaces in the scene will be checked.

        Returns:
            list of str: List of namespaced owned by assets of the given type.
        """
        rootNode = self.fbxScene.GetRootNode()
        foundNamespaces = set()

        rsRefNullRoot = None
        for idx in xrange(rootNode.GetChildCount()):
            rootChild = rootNode.GetChild(idx)
            if rootChild.GetName() == "REFERENCE:Scene":
                rsRefNullRoot = rootChild
                break

        if rsRefNullRoot is None:
            self._logWarning("No RS Ref Null could not be found in the scene.")
            return foundNamespaces

        refPrefix = "RS_Null:"
        for idx in xrange(rsRefNullRoot.GetChildCount()):
            refNull = rsRefNullRoot.GetChild(idx)
            refNullLongName = refNull.GetName()

            if not refNullLongName.startswith(refPrefix):
                # Skip objects that aren't ref nulls
                continue

            refNullName = refNullLongName[len(refPrefix) :]
            if targetNamespaces is not None and refNullName not in targetNamespaces:
                # Skip reference if it doesn't belong to one of the target namespaces
                continue

            refTypeProperty = refNull.FindProperty("rs_Asset_Type")
            if not refTypeProperty.IsValid():
                # Missing asset type property
                continue

            refTypeProperty = fbx.FbxPropertyString(refTypeProperty)
            if not refTypeProperty.IsValid():
                # Asset property is an invalid type
                continue

            if refTypeProperty.Get() != assetTypeName:
                # ref is not the specified type
                continue

            foundNamespaces.add(refNullName)

        return foundNamespaces

    def sceneContainsRefsRequiringPlot(self, targetNamespaces=None, typeString=None):
        """Checks the loaded fbx for references by type and returns true if it finds any that require plotting.

        Args:
            targetNamespaces (list of str, optional ): If specified, only the namespaces in this list will be checked.
                Otherwise all namespaces in the scene will be checked.
            typeString (str): Asset type string to check. Defaults to Props.
        Returns:
            bool: True if props requiring plotting are found or False if not.
        """
        if targetNamespaces is not None and not isinstance(targetNamespaces, list):
            raise TypeError("targetNamespaces must be a list or None")

        self._logDebug("Checking for props requiring plot in: {}".format(os.path.basename(self.fbxFilePath)))

        typeString = typeString or "Props"
        foundNamespaces = self._findNamespacesBelongingToAssetType(typeString, targetNamespaces=targetNamespaces)

        if not foundNamespaces:
            return False

        # TODO: Might be possible to optimise out some unnecessary plotting by checking the
        #  hierarchy for constraints to external objects not belonging to the prop itself?

        return True

    def _findOrCreateMarkupMaterial(self):
        """
        Finds the markup material by name if it already exists in the loaded scene or creates it if not.

        Returns:
            fbx.FbxSurfaceMaterial: The markup material.
        """
        markupMaterial = None
        for materialIdx in xrange(self.fbxScene.GetMaterialCount()):
            fbxMaterial = self.fbxScene.GetMaterial(materialIdx)
            if fbxMaterial.GetName() == self.MARKUP_MATERIAL_NAME:
                self._logDebug("Found markup material")
                markupMaterial = fbxMaterial

        if markupMaterial is None:
            self._logDebug("Create markup material")
            markupMaterial = fbx.FbxSurfacePhong.Create(self.fbxScene, self.MARKUP_MATERIAL_NAME)

        colBlack = fbx.FbxDouble3(0.0, 0.0, 0.0)
        colRed = fbx.FbxDouble3(1.0, 0.0, 0.0)
        colDarkGrey = fbx.FbxDouble3(0.2, 0.2, 0.2)
        markupMaterial.Emissive.Set(colBlack)
        markupMaterial.Diffuse.Set(colRed)
        markupMaterial.Ambient.Set(colDarkGrey)
        markupMaterial.Specular.Set(colDarkGrey)
        markupMaterial.Shininess.Set(5.0)

        return markupMaterial

    def assignMarkupMaterial(self, targetNamespaces):
        """
        Assigns a coloured markup material to all meshes belonging to the given target namespaces.

        Args:
            targetNamespaces (list of str): List of namespaces to find meshes within.
        """

        if not isinstance(targetNamespaces, list):
            raise TypeError("targetNamespaces must be a list")

        markupMaterial = self._findOrCreateMarkupMaterial()

        self._logMessage("Assigning markup material to namespaces: [{}]".format(", ".join(targetNamespaces)))
        for geoIdx in xrange(self.fbxScene.GetGeometryCount()):
            fbxGeo = self.fbxScene.GetGeometry(geoIdx)
            if not isinstance(fbxGeo, fbx.FbxMesh):
                continue

            fbxNode = fbxGeo.GetNode()
            namespace = self._getNamespaceFromNode(fbxNode)

            if namespace not in targetNamespaces:
                continue

            matCount = fbxNode.GetMaterialCount()
            if matCount == 0:
                # Leave it alone if no materials assigned - e.g. geo for sliders, mover, rig controls, etc.
                continue

            layer = fbxGeo.GetLayer(0)
            if layer is None:
                continue

            matLayerElement = layer.GetMaterials()
            if matLayerElement is None:
                continue

            if matLayerElement.GetReferenceMode() != fbx.FbxLayerElement.eIndexToDirect:
                matLayerElement.SetReferenceMode(fbx.FbxLayerElement.eIndexToDirect)

            # Ensure all polygons of the mesh are mapped to a single material (material index 0 attached to the FBXNode)
            if matLayerElement.GetMappingMode() != fbx.FbxLayerElement.eAllSame:
                matLayerElement.SetMappingMode(fbx.FbxLayerElement.eAllSame)
                matLayerElement.GetIndexArray().SetCount(1)
                matLayerElement.GetIndexArray().SetAt(0, 0)

            # Disconnect all existing materials
            fbxNode.RemoveAllMaterials()

            # Attach the markup material
            fbxNode.AddMaterial(markupMaterial)

    @staticmethod
    def _iterSrcObjectsWithClassId(startObject, classId, exactType=False, reverse=False):
        """
        Generator that yields objects connected via source connections from the given
        startObject and which match the given class ID.

        Args:
            startObject (fbx.FbxObject): The object to check for connections from.
            classId (fbx.FbxClassId): Connected objects matching this class ID will be returned.
            exactType (bool): If True only exact classId will be returned, if False, subclasses will also be returned.
            reverse (bool): Iterate in reverse order - this must be used if deleting any of the returned objects.

        Yields:
            fbx.FbxObject: Next connected object matching the given classId
        """
        if exactType:
            criteria = fbx.FbxCriteria.ObjectTypeStrict(classId)
        else:
            criteria = fbx.FbxCriteria.ObjectType(classId)

        idxIterator = xrange(startObject.GetSrcObjectCount(criteria))
        if reverse:
            idxIterator = reversed(idxIterator)

        for srcObjIdx in idxIterator:
            yield startObject.GetSrcObject(criteria, srcObjIdx)

    @staticmethod
    def _iterDstObjectsWithClassId(startObject, classId, exactType=False, reverse=False):
        """
        Generator that yields objects connected via destination connections from the given
        startObject and which match the given class ID.

        Args:
            startObject (fbx.FbxObject): The object to check for connections from.
            classId (fbx.FbxClassId): Connected objects matching this class ID will be returned.
            exactType (bool): If True only exact classId will be returned, if False, subclasses will also be returned.
            reverse (bool): Iterate in reverse order - this must be used if deleting any of the returned objects.

        Yields:
            fbx.FbxObject: Next connected object matching the given classId
        """
        if exactType:
            criteria = fbx.FbxCriteria.ObjectTypeStrict(classId)
        else:
            criteria = fbx.FbxCriteria.ObjectType(classId)

        idxIterator = xrange(startObject.GetDstObjectCount(criteria))
        if reverse:
            idxIterator = reversed(idxIterator)

        for dstObjIdx in idxIterator:
            yield startObject.GetDstObject(criteria, dstObjIdx)

    def _iterControlSetPlugsInScene(self, reverse=False):
        """
        Generator that yields all of the FbxControlSetPlug objects in the loaded fbx scene.

        Args:
            reverse (bool): Iterate in reverse order - this must be used if deleting any of the returned objects.

        Yields:
            fbx.FbxControlSetPlug: The next control set plug in the scene.
        """
        for controlSetPlug in self._iterSrcObjectsWithClassId(
            self.fbxScene, fbx.FbxControlSetPlug.ClassId, reverse=reverse
        ):
            yield controlSetPlug

    def _getControlSetPlugForCharacter(self, fbxCharacter):
        """
        Finds the FbxControlSetPlug that is connected to given character (if any).

        Args:
            fbxCharacter (fbx.FbCharacter): The character to find the connected control set plug from.
        """
        for controlSetPlug in self._iterControlSetPlugsInScene():
            for dstCharacter in self._iterDstObjectsWithClassId(controlSetPlug, fbx.FbxCharacter.ClassId):
                if dstCharacter == fbxCharacter:
                    return controlSetPlug
        return None

    def deleteUnusedShadingComponents(self, targetNamespaces):
        """
        Deletes shading (e.g. materials, shaders) components that don't have at least an indirect connection to a model.

        Args:
            targetNamespaces (list of str): List of namespaces to delete unused shading component from.
        """
        if not isinstance(targetNamespaces, list):
            raise TypeError("targetNamespaces must be a list")

        self._logMessage("Deleting unused shading objects in namespaces: [{}]".format(", ".join(targetNamespaces)))
        deletedCount = 0
        for shadingType, validDstType in (
            (fbx.FbxSurfaceMaterial, fbx.FbxNode),
            (fbx.FbxTexture, fbx.FbxSurfaceMaterial),
            (fbx.FbxVideo, fbx.FbxTexture),
        ):
            for fbxShadingObj in self._iterSrcObjectsWithClassId(self.fbxScene, shadingType.ClassId, reverse=True):
                try:
                    namespace = self._getNamespaceFromNode(fbxShadingObj)
                except UnicodeDecodeError:
                    # Skip shading objects with names that the fbxsdk fails to read (url:bugstar:6143734)
                    continue

                if namespace not in targetNamespaces:
                    # Skip objects not in target namespaces
                    continue

                expectedDstTypeCriteria = fbx.FbxCriteria.ObjectType(validDstType.ClassId)
                if fbxShadingObj.GetDstObjectCount(expectedDstTypeCriteria) > 0:
                    # Don't delete shading object if it's connected to something useful
                    continue

                # Delete
                fbxShadingObj.Destroy()
                deletedCount += 1
        self._logMessage("Deleted {} shading objects".format(deletedCount))

    @staticmethod
    def _isMobuCharacterExtension(fbxObject):
        """
        Checks whether the object passed is a Motionbuilder character extension object

        Args:
            fbxObject (fbx.FbxObject): The FbxObject to be tested

        Returns:
            bool: True if the object passed is a motionbuilder character extension object, False if not
        """
        mobuTypeProp = fbxObject.FindProperty("MoBuSubTypeName")
        if mobuTypeProp.IsValid() and fbx.FbxPropertyString(mobuTypeProp).Get() == "CharacterExtension":
            return True
        return False

    @classmethod
    def _getCharacterExtensionsForCharacter(cls, fbxCharacter):
        """
        Finds all character extension objects connected to the given FbxCharacter

        Args:
            fbxCharacter (fbx.FbxCharacter): The character to find extensions for.

        Returns:
            list of fbx.FbxObject: List of character extensions.
        """
        genericObjectCriteria = fbx.FbxCriteria.ObjectType(fbx.FbxObject.ClassId)
        attachedCharExtensions = []
        for srcIdx in xrange(fbxCharacter.GetSrcObjectCount(genericObjectCriteria)):
            fbxObj = fbxCharacter.GetSrcObject(genericObjectCriteria, srcIdx)
            if cls._isMobuCharacterExtension(fbxObj):
                attachedCharExtensions.append(fbxObj)

        return attachedCharExtensions

    def deleteCharacterisationAndRigs(self, targetNamespaces, skipGsSkels=False):
        """Deletes all character and rig related components within the given namespaces from the current fbxsdk scene

        Args:
            targetNamespaces (list of str): List of namespaces that should have character and rig components deleted.
            skipGsSkels (bool): If True, characterization will be preserved on GS Skel references.
        """
        if not isinstance(targetNamespaces, list):
            raise TypeError("targetNamespaces must be a list")

        self._logMessage(
            "Deleting character and rig components in namespaces: [{}]".format(", ".join(targetNamespaces))
        )

        preserveCharacterizationNamespaces = set()
        if skipGsSkels:
            # Remove namespaces containing a GS skeleton from those that will have characterization stripped
            gsSkelAssetTypeName = "MocapGsSkeleton"
            namespacesWithGsSkel = self._findNamespacesBelongingToAssetType(gsSkelAssetTypeName)
            preserveCharacterizationNamespaces |= namespacesWithGsSkel

        deletedCount = 0
        for charIdx in reversed(xrange(self.fbxScene.GetCharacterCount())):

            character = self.fbxScene.GetCharacter(charIdx)

            characterNamespace = self._getNamespaceFromNode(character)
            if characterNamespace not in targetNamespaces:
                continue

            # Deactivate control rig input on the character
            if character.GetInputType() == fbx.FbxCharacter.eInputMarkerSet:
                character.SetInput(fbx.FbxCharacter.eInputActor, None)

            controlSet = character.GetControlSet()
            if controlSet:
                # Delete FK controls
                for charNodeId in xrange(fbx.FbxCharacter.eNodeIdCount):
                    controlLink = fbx.FbxControlSetLink()
                    success = controlSet.GetControlSetLink(charNodeId, controlLink)
                    if not success:
                        continue
                    # print "delete FK control: {}".format(controlLink.mNode.GetName())
                    self.fbxScene.DisconnectSrcObject(controlLink.mNode)
                    controlLink.mNode.Destroy()
                    deletedCount += 1

                # Delete IK effectors
                for effectorId in xrange(fbx.FbxEffector.eNodeIdCount):
                    effector = fbx.FbxEffector()
                    success = controlSet.GetEffector(effectorId, effector)
                    if not success:
                        continue
                    # print "delete IK effector: {}".format(effector.mNode.GetName())
                    self.fbxScene.DisconnectSrcObject(effector.mNode)
                    effector.mNode.Destroy()
                    deletedCount += 1

                controlSetPlug = self._getControlSetPlugForCharacter(character)
                if controlSetPlug:
                    self._logDebug("Destroy control set plug: {}".format(controlSetPlug.GetName()))
                    self.fbxScene.DisconnectSrcObject(controlSetPlug)
                    controlSetPlug.Destroy()
                    deletedCount += 1

            if characterNamespace in preserveCharacterizationNamespaces:
                self._logDebug("Preserving characterization in namespace: {}".format(characterNamespace))
                continue

            for charExtension in self._getCharacterExtensionsForCharacter(character):
                self._logDebug("Destroy character extension: {}".format(charExtension.GetName()))
                charExtension.Destroy()
                deletedCount += 1

            self._logDebug("Destroy character: {}".format(character.GetName()))
            self.fbxScene.DestroyCharacter(charIdx)
            deletedCount += 1

        self._logMessage("Deleted {} character and rig related objects".format(deletedCount))

    def _getFbxRange(self, frameRange):
        """Creates an FbxTimeSpan

        Args:
            frameRange (list of int): Start and end frame range to set

        Returns:
            fbx.FbxTimeSpan: The time span object
        """
        sceneTimeMode = mobu.FBPlayerControl().GetTransportFps()
        timeMode = const.FbxToMobuEnums.FBX_TIME_MODES.get(sceneTimeMode, fbx.FbxTime.eDefaultMode)

        # set a timespan
        animStart = fbx.FbxTime()
        animStart.SetGlobalTimeMode(timeMode)
        animStart.SetFrame(frameRange[0])
        animEnd = fbx.FbxTime()
        animEnd.SetGlobalTimeMode(timeMode)
        animEnd.SetFrame(frameRange[1])

        return fbx.FbxTimeSpan(animStart, animEnd)

    def mergeAnimation(self, frameRange, allControls):
        """Sets output of fbx to put in frame range

        Args:
            frameRange (list of int): Start and end frame range to set
            allControls (list of mobu.FBModel): Controls to merge animation onto
        """
        animRange = self._getFbxRange(frameRange)
        startTime = mobu.FBTime(0, 0, 0, frameRange[0], 0)
        endTime = mobu.FBTime(0, 0, 0, frameRange[1], 0)

        # root node and stack
        rootNode = self.fbxScene.GetRootNode()
        stack = self.fbxScene.GetCurrentAnimationStack()

        # get take
        currentTake = Globals.System.CurrentTake
        currentLayerIndex = currentTake.GetCurrentLayer()

        for layerIndex in xrange(stack.GetSrcObjectCount()):
            # get layer
            layer = stack.GetSrcObject(layerIndex)
            layerName = str(layer.GetName().split(":")[-1])
            layerMode = const.FbxToMobuEnums.FBX_BLEND_MODES.get(layer.EBlendMode())

            currentLayer = currentTake.GetLayerByName(layerName)
            # create layer if it doesn't exist
            if not currentLayer:
                currentTake.CreateNewLayer()
                currentLayer = currentTake.GetLayer(layerIndex)
                currentLayer.Name = layerName

            # find index as it may not match
            index = layerIndex
            for index in xrange(currentTake.GetLayerCount()):
                if layerName == currentTake.GetLayer(index).Name:
                    break

            # copy layer values / modes
            currentTake.SetCurrentLayer(index)
            currentLayer.LayerMode = layerMode
            currentLayer.Lock = False  # set layer to false first
            currentLayer.Mute = layer.Mute.Get()
            currentLayer.Solo = layer.Solo.Get()
            currentLayer.Weight = layer.Weight.Get()

            for control in allControls:
                # find control matching scene control
                matchingControl = rootNode.FindChild(control.LongName, True, False)
                if not matchingControl:
                    continue

                # delete any keys on control before merging on layer
                Time.deleteByTimeRange(control, startTime, endTime)

                # find properties recursively
                srcProperty = matchingControl.GetFirstProperty()
                while srcProperty.IsValid():
                    srcAnimNode = srcProperty.GetCurveNode(layer)
                    srcPropertyName = srcProperty.GetName()

                    # don't do anything if not animated
                    if not srcAnimNode or not srcAnimNode.IsAnimated():
                        srcProperty = matchingControl.GetNextProperty(srcProperty)
                        continue

                    # get property match
                    destProperty = next((item for item in control.PropertyList if item.Name == srcPropertyName), None)
                    if not destProperty:
                        continue

                    destProperty.SetAnimated(True)
                    destAnimNode = destProperty.GetAnimationNode()

                    for channel in xrange(srcAnimNode.GetChannelsCount()):
                        srcAnimCurve = srcAnimNode.GetCurve(channel)

                        try:
                            destAnimCurve = destAnimNode.Nodes[channel].FCurve
                        except IndexError:
                            continue

                        if not srcAnimCurve or not destAnimCurve:
                            continue

                        addedKeys = []
                        for keyIndex in xrange(srcAnimCurve.KeyGetCount()):
                            keyTime = srcAnimCurve.KeyGetTime(keyIndex)

                            if not animRange.IsInside(keyTime) and not keyTime == animRange.GetStop():
                                continue

                            newKeyIndex = destAnimCurve.KeyAdd(
                                mobu.FBTime(0, 0, 0, keyTime.GetFrameCount(), 0), srcAnimCurve.KeyGetValue(keyIndex)
                            )
                            newKey = destAnimCurve.Keys[newKeyIndex]

                            # set interp
                            newKey.Interpolation = const.FbxToMobuEnums.FBX_INTERPOLATIONS[
                                srcAnimCurve.KeyGetInterpolation(keyIndex)
                            ]
                            # set tangent
                            newKey.TangentMode = const.FbxToMobuEnums.FBX_TANGENT_MODES[
                                srcAnimCurve.KeyGetTangentMode(keyIndex)
                            ]

                            # not using TCB mode just set to break
                            if newKey.TangentMode == mobu.FBTangentMode.kFBTangentModeTCB:
                                newKey.TangentMode = mobu.FBTangentMode.kFBTangentModeBreak

                            # set key constant
                            newKey.TangentConstantMode = const.FbxToMobuEnums.FBX_TANGENT_CONSTANTS[
                                srcAnimCurve.KeyGetConstantMode(keyIndex)
                            ]

                            # add key to set later
                            addedKeys.append(
                                keyTangent(
                                    key=newKey,
                                    leftDerivative=srcAnimCurve.KeyGetLeftDerivative(keyIndex),
                                    rightDerivative=srcAnimCurve.KeyGetRightDerivative(keyIndex),
                                    leftTangent=srcAnimCurve.KeyGetLeftTangentWeight(keyIndex),
                                    rightTangent=srcAnimCurve.KeyGetRightTangentWeight(keyIndex),
                                )
                            )

                        # set tangent weights after adding keys
                        for addedKey in addedKeys:
                            addedKey.key.LeftDerivative = addedKey.leftDerivative
                            addedKey.key.RightDerivative = addedKey.rightDerivative

                            if not AnimLib.tangentIsDefaultWeight(addedKey.leftTangent):
                                addedKey.key.LeftTangentWeight = addedKey.leftTangent

                            if not AnimLib.tangentIsDefaultWeight(addedKey.rightTangent):
                                addedKey.key.RightTangentWeight = addedKey.rightTangent

                    srcProperty = matchingControl.GetNextProperty(srcProperty)

            currentLayer.Lock = layer.Lock.Get()

        currentTake.SetCurrentLayer(currentLayerIndex)

    def allDescendants(self, fbxNode):
        """Gets all descendants of an FbxNode recursively

        Args:
            fbxNode (fbx.FbxNode): Node to get children

        Returns:
            list of fbx.FbxNode: All child fbx objects
        """
        children = []
        for childIdx in xrange(fbxNode.GetChildCount()):
            child = fbxNode.GetChild(childIdx)
            if child:
                children.append(child)
                children.extend(self.allDescendants(child))

        return children

    def setOutputRange(self, frameRange):
        """Sets output of fbx to put in frame range

        Args:
            frameRange (list of int): Start and end frame range to set
        """
        animRange = self._getFbxRange(frameRange)

        # get current anim stack that stores all layers
        stack = self.fbxScene.GetCurrentAnimationStack()
        rootNode = self.fbxScene.GetRootNode()

        # get all nodes in scene
        allNodes = self.allDescendants(rootNode)

        for index in xrange(stack.GetSrcObjectCount()):
            # get layer
            layer = stack.GetSrcObject(index)
            # get all items from layer
            for node in allNodes:
                srcProperty = node.GetFirstProperty()
                while srcProperty.IsValid():
                    srcAnimNode = srcProperty.GetCurveNode(layer)

                    if not srcAnimNode or not srcAnimNode.IsAnimated():
                        srcProperty = node.GetNextProperty(srcProperty)
                        continue

                    for channel in xrange(srcAnimNode.GetChannelsCount()):
                        srcAnimCurve = srcAnimNode.GetCurve(channel)
                        if not srcAnimCurve:
                            continue

                        # don't delete single keys
                        keyCount = srcAnimCurve.KeyGetCount()
                        if keyCount <= 1:
                            continue

                        for keyIndex in reversed(xrange(keyCount)):
                            keyTime = srcAnimCurve.KeyGetTime(keyIndex)
                            if not animRange.IsInside(keyTime) and not keyTime == animRange.GetStop():
                                srcAnimCurve.KeyRemove(keyIndex)

                    srcProperty = node.GetNextProperty(srcProperty)

        # set time range
        stack.LocalStart.Set(animRange.GetStart())
        stack.LocalStop.Set(animRange.GetStop())

    def findAllReferencesByType(self, refTypes=None):
        """Finds all references in this scene

        Args:
            refTypes (list of ReferenceTypes): Types to filter for

        Returns:
             collections.defaultdict(set): Assets by type with namespaces
        """
        refMatch = re.compile(r"(^reference|scene$)", re.I)
        rootNode = self.fbxScene.GetRootNode()
        refDict = collections.defaultdict(set)

        # find reference null
        refNull = None
        for idx in xrange(rootNode.GetChildCount()):
            node = rootNode.GetChild(idx)
            if refMatch.search(node.GetName()):
                refNull = node
                break

        if not refNull:
            self._logWarning("No RS Ref Null could not be found in the scene.")
            return refDict

        # search through reference null children
        for idx in xrange(refNull.GetChildCount()):
            # only get nulls
            childNull = refNull.GetChild(idx)
            if not childNull.GetTypeName() == "Null":
                continue

            # check for properties
            refNamespaceProperty = childNull.FindProperty("Namespace")
            refTypeProperty = childNull.FindProperty("rs_Asset_Type")

            if not refNamespaceProperty.IsValid() or not refTypeProperty.IsValid():
                continue

            refNamespace = fbx.FbxPropertyString(refNamespaceProperty)
            refType = fbx.FbxPropertyString(refTypeProperty)

            if not refNamespace.IsValid() or not refType.IsValid():
                continue

            # append asset type if in reference types
            refTypeName = str(refType.Get())
            if not hasattr(Types, refTypeName):
                continue

            if refTypes is not None and not any(
                isinstance(getattr(Types, refTypeName), refType) for refType in refTypes
            ):
                continue

            refDict[refTypeName].add(str(refNamespace.Get()))

        return refDict


def iterCharactersWithActiveControlRigs(targetNamespaces=None):
    """
    Generator that returns all characters with active control rigs in the current Mobu scene.

    Keyword Args:
        targetNamespaces (list of str): If specified then only characters within one of the given namespaces will be
            considered. If None then all characters in the scene will be considered.

    Yields:
        mobu.FBCharacter: The next character found with an active control rig
    """
    if targetNamespaces is not None and not isinstance(targetNamespaces, list):
        raise TypeError("targetNamespaces must be a list or None")

    refManager = Manager.Manager()
    if targetNamespaces is None:
        targetNamespaces = refManager.GetNamespaceList(None)

    for namespace in targetNamespaces:
        reference = refManager.GetReferenceByNamespace(namespace)
        if reference is None or not isinstance(reference, ProjectData.data.CharacterReferenceBaseType()):
            continue

        fbNamespace = Namespace.GetFBNamespace(str(namespace))
        charactersInNs = Namespace.GetContentsByType(fbNamespace, mobu.FBCharacter, True)

        for character in charactersInNs:
            if not character.ActiveInput or character.InputType is not AnimLib.InputTypes.Rig:
                continue
            if AnimLib.HasControlRig(character):
                # Character has active control rig
                yield character


def iterRefsWithActiveControlRigs(targetNamespaces=None, referenceType=None):
    """
    Generator that returns all props that require plotting the current Mobu scene.

    Args:
        targetNamespaces (list of str, optional): If specified then only props within one of the given namespaces will
            be considered. If None then all props in the scene will be considered.
        referenceType (RS.Core.ReferenceSystem.RefTypes.Base, optional): reference type to search, default is Prop.Prop
    Yields:
        RS.Core.ReferenceSystem.RefTypes.Base: The next reference based on type found in the scene that requires plotting.
    """
    if targetNamespaces is not None and not isinstance(targetNamespaces, list):
        raise TypeError("targetNamespaces must be a list or None")

    referenceType = referenceType or Prop.Prop

    refManager = Manager.Manager()
    if targetNamespaces is None:
        targetNamespaces = refManager.GetNamespaceList(None)

    for namespace in targetNamespaces:
        reference = refManager.GetReferenceByNamespace(namespace)
        if reference is None or not isinstance(reference, referenceType):
            continue

        # TODO: Might be possible to optimise out some unnecessary plotting by checking the prop's
        #  hierarchy for constraints to external objects not belonging to the prop itself?

        yield reference


def isPlotRequired(targetNamespaces=None, checkCharacters=True, checkProps=False, checkVehicles=False):
    """Checks whether any plotting is required in the current Mobu scene.

    Args:
        targetNamespaces (list of str): Optional list of namespaces to check. If not specified, all
            namespaces will be checked
        checkCharacters (bool, optional): If True, character references will be checked to determine if any need plotted
        checkProps (bool, optional): If True, prop references will be checked to determine if any need plotted
        checkVehicles (bool, optional): If True, vehicle references will be checked to determine if any need plotted

    Returns:
        bool: True if plotting is required or False if not
    """
    plotIsRequired = False

    if checkCharacters:
        for _ in iterCharactersWithActiveControlRigs(targetNamespaces):
            # Plot is required for at least one character in the target namespaces
            plotIsRequired = True
            break

    if not plotIsRequired and checkProps:
        for _ in iterRefsWithActiveControlRigs(targetNamespaces):
            # Plot is required for at least one prop in the target namespaces
            plotIsRequired = True
            break

    if not plotIsRequired and checkVehicles:
        for _ in iterRefsWithActiveControlRigs(targetNamespaces, referenceType=Vehicle.Vehicle):
            # Plot is required for at least one prop in the target namespaces
            plotIsRequired = True
            break

    return plotIsRequired


def plotAllActiveControlRigs(targetNamespaces=None, plotCharacters=True, plotProps=False, plotVehicles=False):
    """
    Find any characters with active control rigs in the current Mobu scene and plot them to skeleton.

    Args:
        targetNamespaces (list of str): Optional list of namespaces to check. If not specified, all
            namespaces will be checked.
        plotCharacters (bool, optional): If True, characters with active control rigs will be plotted.
        plotProps (bool, optional): If True, props that require it will be plotted.
        plotVehicles (bool, optional): If True, vehicles that require it will be plotted.
    """
    if plotCharacters:
        for character in iterCharactersWithActiveControlRigs(targetNamespaces):
            AnimLib.Plot(
                character,
                "skeleton",
                AllTakes=True,
                UseConstantKeyReducer=False,
                PlotLockedProperties=True,
                RotationFilter=mobu.FBRotationFilter.kFBRotationFilterUnroll,
                FrameRate=Globals.Player.GetTransportFps(),
            )
            character.ActiveInput = False

    if plotProps:
        Model.UnselectAll()
        with ContextManagers.UnlockLayers():
            for prop in iterRefsWithActiveControlRigs(targetNamespaces):
                controlsSearcher = _getControlsSearcher(prop.Namespace)
                if controlsSearcher is None:
                    continue

                controlsList = [ctrl for ctrl in controlsSearcher.iterMatchingObjects(prop.Namespace)]
                AnimLib.PlotObjects(
                    controlsList, children=False, allTakes=True, useConstantKeyReducer=False, plotLockedProperties=True
                )

    if plotVehicles:
        Model.UnselectAll()
        with ContextManagers.UnlockLayers():
            for vehicle in iterRefsWithActiveControlRigs(targetNamespaces, referenceType=Vehicle.Vehicle):
                controlsSearcher = _getControlsSearcher(vehicle.Namespace)
                if controlsSearcher is None:
                    continue

                controlsList = [ctrl for ctrl in controlsSearcher.iterMatchingObjects(vehicle.Namespace)]
                AnimLib.PlotObjects(
                    controlsList, children=False, allTakes=True, useConstantKeyReducer=False, plotLockedProperties=True
                )


def plotRequiredInFbx(
    fbxFilePath,
    targetNamespaces=None,
    ulog=None,
    checkCharacters=True,
    checkProps=False,
    checkVehicles=False,
    verbose=False,
):
    """Uses the FBXSDK to load the given file and check whether any characters have active control rigs

    Args:
        fbxFilePath (str): The path to the fbx file to load
        targetNamespaces (list of str, optional):  list of namespaces to check. If not specified, all
            namespaces will be checked
        ulog (UniversalLog, optional): Universal log to write messages to while processing
        checkCharacters (bool, optional): If True, character references will be checked to determine if any need plotted
        checkProps (bool, optional): If True, prop references will be checked to determine if any need plotted
        checkVehicles (bool, optional): If True, vehicle references will be checked to determine if any need plotted
        verbose (bool): If True, more detailed log messages will be included

    Returns:
        bool: True if plot is required or False if not.
    """
    if targetNamespaces is not None and not isinstance(targetNamespaces, list):
        raise TypeError("targetNamespaces must be a list or None")

    if not checkCharacters and not checkProps and not checkVehicles:
        return False

    with SceneSplitFbxSdkProcessor(ulog=ulog, verbose=verbose) as splitSceneProcessor:
        ioSettings = splitSceneProcessor.getIoSettings()
        ioSettings.SetBoolProp(fbx.EXP_FBX_MATERIAL, False)
        ioSettings.SetBoolProp(fbx.EXP_FBX_TEXTURE, False)
        ioSettings.SetBoolProp(fbx.EXP_FBX_EMBEDDED, False)
        ioSettings.SetBoolProp(fbx.EXP_FBX_SHAPE, False)
        ioSettings.SetBoolProp(fbx.EXP_FBX_GOBO, False)
        ioSettings.SetBoolProp(fbx.EXP_FBX_ANIMATION, False)
        ioSettings.SetBoolProp(fbx.EXP_FBX_GLOBAL_SETTINGS, True)

        splitSceneProcessor.loadFbx(fbxFilePath)

        if checkCharacters and splitSceneProcessor.sceneContainsCharactersWithActiveRigs(targetNamespaces):
            return True
        if checkProps and splitSceneProcessor.sceneContainsRefsRequiringPlot(targetNamespaces):
            return True
        if checkVehicles and splitSceneProcessor.sceneContainsRefsRequiringPlot(
            targetNamespaces, typeString="Vehicles"
        ):
            return True

    return False


def getCurrentFrame():
    """Gets the current frame

    Returns:
        int: The current frame
    """
    return Globals.System.LocalTime.GetFrame()
