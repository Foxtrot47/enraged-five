import os
import operator
import itertools
import collections

import fbx
import pyfbsdk as mobu
import scandir

from RS import Globals
from RS.Utils.Scene import Time
from RS.Core.SceneSplitter import exceptions, settings, util, const
from RS.Tools.SceneSplitEditor import assetMismatchDialog
from RS.Tools.SceneSplitEditor import const as uiConst


class ValidationResults(object):
    LOG_CONTEXT = "SplitSceneValidation"

    def __init__(self, raiseExceptionOnError=False, ulog=None):
        """
        Initialise the validation results object.

        Args:
            raiseExceptionOnError (bool): If True an exception will be thrown as soon as an error message is added
            ulog (UniversalLog): Optional log to add relevant messages to
        """
        self._raiseExceptionOnError = raiseExceptionOnError
        self._ulog = ulog
        self._warnings = []
        self._errors = []
        self._remapping = {}

    def warnings(self):
        """Generator that yields each of the warning messages held"""
        for msg in self._warnings:
            yield msg

    def errors(self):
        """Generator that yields each of the error messages held"""
        for msg in self._errors:
            yield msg

    def remapping(self):
        """Get the remapping dictionary

        Returns:
            dict: Remapping dictionary
        """
        return self._remapping

    def hasWarnings(self):
        """Returns True if the results hold any warnings or False if not"""
        return self.warningCount() > 0

    def hasErrors(self):
        """Returns True if the results hold any errors or False if not"""
        return self.errorCount() > 0

    def hasRemapping(self):
        """Returns True if the results hold any remapping or False if not"""
        return len(self._remapping) > 0

    def warningCount(self):
        """Returns the number of warnings"""
        return len(self._warnings)

    def errorCount(self):
        """Returns the number of errors"""
        return len(self._errors)

    def addWarning(self, message):
        """
        Adds a warning message to the validation results.

        Args:
            message (str): The warning message to add.
        """
        self._warnings.append(str(message))
        if self._ulog:
            self._ulog.LogWarning(message, self.LOG_CONTEXT)

    def addError(self, message):
        """
        Adds an error message to the validation results.
        If raiseExceptionOnError was true on construction this will also raise a SceneSplitUserError containing
        the same message.

        Args:
            message (str): The error message to add.
        """
        self._errors.append(str(message))

        if self._ulog:
            self._ulog.LogError(message, self.LOG_CONTEXT)

        if self._raiseExceptionOnError:
            raise exceptions.SceneSplitUserError(message)

    def addRemapping(self, remapDict=None):
        """Sets remapping dictionary

        Args:
            remapDict (dict): Remap dictionary of scene
        """
        self._remapping = remapDict


def commonSceneValidation(results):
    """
    Performs basic validation to ensure that the current scene is in a valid state for split/merge operations

    Args:
        results (ValidationResults): The object that will be used to store the validation results.

    Returns:
        ValidationResults: Results of the validation.
    """
    if not isinstance(results, ValidationResults):
        raise TypeError("results parameter must be of type ValidationResults")

    # Check that the current scene has a valid filename (i.e. it has been saved to disk)
    fbxFileName = Globals.Application.FBXFileName
    if not fbxFileName:
        results.addError("The FBX scene must be saved before running scene split operations.")
        return results

    return results


def settingsValidation(sceneSplitSettings, results, selectedSplitSections=None):
    """
    Performs validation on the scene split settings to ensure they are in a valid state for split/merge operations

    Args:
        sceneSplitSettings (settings.SceneSplitSettings): The split settings object for the current scene.
        results (ValidationResults): The object that will be used to store the validation results.
        selectedSplitSections (list of settings.SceneSplitSectionSettings): Optional list of split sections to validate.

    Returns:
        ValidationResults: Results of the validation.
    """
    if not isinstance(sceneSplitSettings, settings.SceneSplitSettings):
        raise TypeError("sceneSplitSettings must be of type settings.SceneSplitSettings")
    if not isinstance(results, ValidationResults):
        raise TypeError("results parameter must be of type ValidationResults")

    fbxFileName = Globals.Application.FBXFileName

    # Check that the operation is being run from the base scene and not one of the split scenes
    parentFolderName = os.path.basename(os.path.dirname(fbxFileName))
    if sceneSplitSettings.activeWorkflow.getDefaultSplitFolderNameRegex().search(parentFolderName) is not None:
        results.addError(
            "Scene split/merge operations should be run from the base FBX file. "
            "The current scene appears to be a split file."
        )
        return results

    # Check that at least one split exists
    if len(sceneSplitSettings.splitSections) <= 0:
        results.addError("No scene splits have been set up in the current scene.")
        return results

    selectedStr = ""
    if selectedSplitSections is None:
        # Validate all if no selection subset passed in
        filteredSplitSections = sceneSplitSettings.splitSections
    elif len(selectedSplitSections) == 0:
        # Empty selection list passed in
        results.addError("No splits selected.")
        return results
    else:
        filteredSplitSections = selectedSplitSections
        selectedStr = "selected "

    # Ignore empty splits
    validSplitSections = []
    for split in filteredSplitSections:
        if len(split.resourceNamespaces) == 0:
            results.addWarning(
                'Scene split with suffix "{}" contains no references and will be ignored.'.format(split.splitSuffix)
            )
            continue
        validSplitSections.append(split)

    if len(validSplitSections) == 0:
        # If all (selected) splits are empty then escalate to error
        msg = "All {0}splits are empty. Resources must be added to at least one {0}split.".format(selectedStr)
        results.addError(msg)
        return results

    # Warn if a split contains no resource items (split would be empty/skipped)
    for split in reversed(validSplitSections):
        for namespace in split.resourceNamespaces:
            # Warn if the namespace does not contain the expected skeleton root object "Dummy01"
            expectedSkeletonRootName = "{}:Dummy01".format(namespace)
            skeletonRoot = mobu.FBFindModelByLabelName(expectedSkeletonRootName)
            if not skeletonRoot:
                results.addWarning(
                    'Skeleton root missing: "{}". '
                    "Animation merge will ignore this namespace.".format(expectedSkeletonRootName)
                )

    if len(validSplitSections) == 0:
        msg = "All {0}splits are invalid. See previous warnings for details.".format(selectedStr)
        results.addError(msg)

    return results


def splitRangeValidation(sceneSplitSettings, results):
    """Performs validation on the scene splits frame ranges and assets

    Args:
        sceneSplitSettings (settings.SceneSplitSettings): The split settings object for the current scene.
        results (ValidationResults): The object that will be used to store the validation results.

    Returns:
        ValidationResults: Results of the validation.
    """
    baseFbxFilePath = Globals.Application.FBXFileName
    sceneFrameRange = Time.getSceneFrameRange()
    allSceneNamespaces = sceneSplitSettings.getAllSplittableNamespaces()

    # get overlapping assets
    allResources = []
    allFrameRanges = []
    for splitSection in sceneSplitSettings.splitSections:
        currentResources = set(splitSection.resourceNamespaces)
        currentFrameRange = splitSection.splitFrameRange

        # check if have common elements (not disjoint)
        for index, resources in enumerate(allResources):
            if currentResources.isdisjoint(resources):
                continue

            # current split based on index
            currentSplit = sceneSplitSettings.splitSections[index]
            # get frame range based on index
            lastFrameRange = currentSplit.splitFrameRange

            # check for overlap
            overlapFrames = max(
                0, min(currentFrameRange[1], lastFrameRange[1]) - max(currentFrameRange[0], lastFrameRange[0]) + 1
            )

            # if frames overlap add and error with assets, frame range, and split sections
            if overlapFrames:
                # join the intersection resources into a string that will work
                matchingResources = "\n          ".join(currentResources.intersection(resources))
                results.addError(
                    "{0} overlapping frames between ({1}-{2}):\t\t\n"
                    "          {3}\n"
                    "          {4}\n"
                    "      Found on asset(s):\n"
                    "          {5}\n".format(
                        overlapFrames,
                        max(currentFrameRange[0], lastFrameRange[0]),  # overlap min
                        min(currentFrameRange[1], lastFrameRange[1]),  # overlap max
                        currentSplit.getOutputFileName(baseFbxFilePath),
                        splitSection.getOutputFileName(baseFbxFilePath),
                        matchingResources,
                    )
                )

        allResources.append(currentResources)
        allFrameRanges.append(currentFrameRange)

    # check for unused assets
    currentNamespaces = set().union(*allResources)
    unusedNamespaces = allSceneNamespaces.difference(currentNamespaces)

    if unusedNamespaces:
        unusedNamespacesStr = "\n          ".join(sorted(unusedNamespaces))
        results.addWarning(
            "Are you sure you don't want to include these assets?\n" "          {0}\n".format(unusedNamespacesStr)
        )

    # check for frame minimums
    frameMinimums = []
    for index, frameRange in enumerate(allFrameRanges):
        if frameRange[1] - frameRange[0] <= const.FrameRangeSettings.MINIMUM_VERTICAL_SPLIT_RANGE:
            fileName = sceneSplitSettings.splitSections[index].getOutputFileName(baseFbxFilePath)
            frameMinimums.append("{0} {1}-{2}".format(fileName, frameRange[0], frameRange[1]))

    if frameMinimums:
        results.addWarning(
            "Are you sure you want a frame range this short?\n"
            "          {0}\n".format("\n          ".join(frameMinimums))
        )

    # check unused frameranges
    allFrames = set()
    for frameRange in allFrameRanges:
        allFrames.update(set(range(frameRange[0], frameRange[1] + 1)))
    sceneFrames = set(range(sceneFrameRange[0], sceneFrameRange[1] + 1))

    # check difference between scene range and frames found
    frames = sorted(sceneFrames.difference(allFrames))

    if not frames:
        return results

    # group missing sequential ranges
    missingRanges = []
    for key, group in itertools.groupby(enumerate(frames), lambda frameRange: frameRange[0] - frameRange[1]):
        ranges = list(map(operator.itemgetter(1), group))
        missingRanges.append([ranges[0], ranges[-1]])

    missingRangesStr = "\n          ".join(["{0}-{1}".format(*missingRange) for missingRange in missingRanges])

    results.addWarning(
        "Are you sure you don't require these frame ranges?\n" "          {0}\n".format(missingRangesStr)
    )

    return results


def fpsValidation(sceneSplitSettings, results):
    """Performs validation to ensure merged files have the same fps

    Args:
        sceneSplitSettings (settings.SceneSplitSettings): The split settings object for the current scene.
        results (ValidationResults): The object that will be used to store the validation results.

    Returns:
        ValidationResults: Results of the validation.
    """
    sceneFps = Time.getSceneFps()
    baseFbxFilePath = Globals.Application.FBXFileName
    splitOutputDir = sceneSplitSettings.getSplitFileOutputFolder(baseFbxFilePath)

    mismatchedFiles = []
    for splitSection in sceneSplitSettings.splitSections:
        outputName = splitSection.getOutputFileName(baseFbxFilePath)
        splitFilePath = os.path.join(splitOutputDir, outputName)

        with util.SceneSplitFbxSdkProcessor(ulog=results._ulog, verbose=False) as splitSceneProcessor:
            splitSceneProcessor.loadFbx(splitFilePath)

            timeMode = splitSceneProcessor.fbxScene.GetGlobalSettings().GetTimeMode()
            fileFps = fbx.FbxTime.GetFrameRate(timeMode)
            if fileFps != sceneFps:
                mismatchedFiles.append("{} ({} fps)".format(outputName, fileFps))

    if mismatchedFiles:
        mismatchFilesStr = "\n          ".join([mismatch for mismatch in mismatchedFiles])
        results.addError(
            "Mismatching frame rates between current file ({} fps) and these split scenes:\n"
            "          {}\n".format(sceneFps, mismatchFilesStr)
        )

    return results


def assetMismatchValidation(sceneSplitSettings, results):
    """Performs validation to fix asset mismatches based on the master file

    If a mismatch is found the user will be presented with a ui to fix mismatches

    Args:
        sceneSplitSettings (settings.SceneSplitSettings): The split settings object for the current scene.
        results (ValidationResults): The object that will be used to store the validation results.

    Returns:
        ValidationResults: Results of the validation.
    """
    allNamespaces = sceneSplitSettings.getAllSplittableNamespaces()

    splitNamespaces = set()
    for splitSection in sceneSplitSettings.splitSections:
        splitNamespaces.update(splitSection.resourceNamespaces)

    # check for if namespaces in scene namespaces haven't differed
    if not splitNamespaces.issubset(allNamespaces):
        unmatchedSplitNamespaces = splitNamespaces.difference(allNamespaces)
        availableAssets = allNamespaces.difference(splitNamespaces)

        # make dialog
        dialog = assetMismatchDialog.AssetMismatchDialog(
            sourceAssets=list(unmatchedSplitNamespaces), destAssets=list(availableAssets)
        )
        # run with exec and wait for result
        result = dialog.exec_()
        if result == uiConst.MismatchDialogResult.ACCEPTED:
            # get mappings if result
            results.addRemapping(dialog.getMappings())
        else:
            results.addError("Mismatched assets in scene.")

    return results


def preSplitValidation(sceneSplitSettings, results):
    """
    Performs validation to ensure the current state is valid for running split operations.

    Args:
        sceneSplitSettings (settings.SceneSplitSettings): The split settings object for the current scene.
        results (ValidationResults): The object that will be used to store the validation results.

    Returns:
        ValidationResults: Results of the validation.
    """
    if not isinstance(results, ValidationResults):
        raise TypeError("results parameter must be of type ValidationResults")

    results = commonSceneValidation(results)
    if results.hasErrors():
        return results

    results = settingsValidation(sceneSplitSettings, results)
    if results.hasErrors():
        return results

    results = splitRangeValidation(sceneSplitSettings, results)
    if results.hasErrors():
        return results

    # TODO: Validate that the split settings have a base scene bug ID set
    #  (depends on bugstar library upgrade url:bugstar:5881421)

    baseFbxFilePath = Globals.Application.FBXFileName
    outputFolder = sceneSplitSettings.getSplitFileOutputFolder(baseFbxFilePath)

    # Warn if any of the output split files already exist on disk
    for sectionSettings in sceneSplitSettings.splitSections:
        if not sectionSettings.resourceNamespaces:
            # Ignore splits that don't contain any references
            continue

        splitSceneFilePath = sectionSettings.getOutputFileName(baseFbxFilePath)
        if not splitSceneFilePath:
            continue

        fileName = os.path.basename(os.path.splitext(baseFbxFilePath)[0])
        matchPath = os.path.join(outputFolder, "{}_{}_".format(fileName, sectionSettings.splitSuffix))

        if not os.path.isdir(outputFolder):
            continue

        for childPath in scandir.scandir(outputFolder):
            if matchPath in childPath.path:
                results.addWarning(
                    "You have already split this scene\n"
                    '          Existing file may be overwritten: "{}"'.format(os.path.basename(childPath.path))
                )

    return results


def preMergeValidation(sceneSplitSettings, results, selectedSplitSections=None):
    """
    Performs validation to ensure the current state is valid for running merge operations.

    Args:
        sceneSplitSettings (settings.SceneSplitSettings): The split settings object for the current scene.
        results (ValidationResults): The object that will be used to store the validation results.
        selectedSplitSections (list of settings.SceneSplitSectionSettings): Optional list of split sections to validate.

    Returns:
        ValidationResults: Results of the validation.
    """
    if not isinstance(results, ValidationResults):
        raise TypeError("results parameter must be of type ValidationResults")

    results = commonSceneValidation(results)
    if results.hasErrors():
        return results

    results = settingsValidation(sceneSplitSettings, results, selectedSplitSections=selectedSplitSections)
    if results.hasErrors():
        return results

    results = splitRangeValidation(sceneSplitSettings, results)
    if results.hasErrors():
        return results

    results = fpsValidation(sceneSplitSettings, results)
    if results.hasErrors():
        return results

    results = assetMismatchValidation(sceneSplitSettings, results)
    if results.hasErrors():
        return results

    baseFbxFilePath = Globals.Application.FBXFileName
    splitOutputDir = sceneSplitSettings.getSplitFileOutputFolder(baseFbxFilePath)

    if selectedSplitSections is None:
        # Validate all if no selection subset passed in
        filteredSplitSections = sceneSplitSettings.splitSections
    elif len(selectedSplitSections) == 0:
        results.addError("No splits selected for merging.")
        return results
    else:
        filteredSplitSections = selectedSplitSections

    # Check that the scenes to be merged exist on disk
    allFilesMissing = True
    for splitSection in filteredSplitSections:
        splitSceneFilePath = os.path.join(splitOutputDir, splitSection.getOutputFileName(baseFbxFilePath))
        if os.path.exists(splitSceneFilePath) and os.path.isfile(splitSceneFilePath):
            allFilesMissing = False
        else:
            results.addWarning('Split file is missing: "{}"'.format(splitSceneFilePath))

    # Upgrade to an error if ALL expected files are missing
    if allFilesMissing:
        results.addError("None of the expected split FBX files exist on disk. See warnings for details.")
        return results

    return results
