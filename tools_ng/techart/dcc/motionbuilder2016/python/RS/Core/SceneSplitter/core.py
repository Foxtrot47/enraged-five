import os
import uuid
import itertools
import tempfile

import pyfbsdk as mobu

from RS.Utils.Logging import Universal
from RS import Globals, Perforce
from RS.Core.Animation import Lib as AnimLib
from RS.Core.ReferenceSystem import Manager
from RS.Core.ReferenceSystem.Types import Set, CharacterComplex, Vehicle, Prop
from RS.Utils import ContextManagers
from RS.Core.SceneSplitter import exceptions, validation, util, const


SPLIT_SCENE_LOG = Universal.UniversalLog("SceneSplitter_{}".format(os.getpid()), appendToFile=False, consoleOutput=False)


def splitScene(
    sceneSplitSettings,
    selectedSplitSections=None,
    ulog=None,
    plotBaseSceneActiveCharacterRigs=True,
    plotBaseSceneProps=False,
    plotBaseSceneVehicles=False,
    preserveGsSkelCharacterization=False,
    verbose=False,
):
    """
    Creates the "split" scenes from the base fbx and performs relevant markup and optimisation operations on them.

    Args:
        sceneSplitSettings (dict): The split settings for the current base scene.
        selectedSplitSections (list of SplitSectionSettings): List of settings for each split to be processed.
        ulog (UniversalLog.UniversalLog): Optional log to use during the split operation.
        plotBaseSceneActiveCharacterRigs (bool): If True, any active control rigs in the base scene will be
            plotted to skeleton before creating the split scenes from it.
        plotBaseSceneProps (bool): If True, all props in the base scene will be plotted before split
        plotBaseSceneVehicles (bool): If true all vehicles in the base scene will be plotted before split
        preserveGsSkelCharacterization (bool): If True, characterization will be preserved on all GS Skeletons.
        verbose (bool): If True, more verbose messages will be logged during the split operation.
    """
    if ulog is None:
        ulog = SPLIT_SCENE_LOG
    LOG_CONTEXT = "SplitScene"

    ulog.LogMessage("Performing pre-split validation", LOG_CONTEXT)
    validationResults = validation.ValidationResults(raiseExceptionOnError=True, ulog=ulog)
    validation.preSplitValidation(sceneSplitSettings, validationResults)

    baseFbxFilePath = Globals.Application.FBXFileName
    ulog.LogMessage("Starting scene split from base fbx: [{}]".format(baseFbxFilePath), LOG_CONTEXT)

    # TODO: Create new bugs for the split scenes if they don't already have them
    #  (depends on bugstar library upgrade url:bugstar:5881421)

    # Create a changelist
    p4ChangelistNum = None
    if Perforce.Enabled():
        p4Changelist = Perforce.CreateChangelist("FBX Scene Splitter")
        if p4Changelist:
            p4ChangelistNum = p4Changelist.Number

    try:
        # Try to mark the settings file for add/edit in perforce
        splitSettingsFile = sceneSplitSettings.getSplitSettingsFilePath()
        if splitSettingsFile and os.path.isfile(splitSettingsFile):
            try:
                Perforce.AddOrEditFiles(splitSettingsFile, changelistNum=p4ChangelistNum)
            except Perforce.P4Exception:
                pass

        if plotBaseSceneActiveCharacterRigs or plotBaseSceneProps or plotBaseSceneVehicles:
            # Plot any characters with active control rigs in the current scene to skeleton
            ulog.LogMessage("Checking if the base scene requires pre-split plot", LOG_CONTEXT)

            if util.isPlotRequired(
                checkCharacters=plotBaseSceneActiveCharacterRigs,
                checkProps=plotBaseSceneProps,
                checkVehicles=plotBaseSceneVehicles,
            ):
                ulog.LogWarning("Base scene contains references that require plotting", LOG_CONTEXT)
                util.plotAllActiveControlRigs(
                    plotCharacters=plotBaseSceneActiveCharacterRigs,
                    plotProps=plotBaseSceneProps,
                    plotVehicles=plotBaseSceneVehicles,
                )

                with ContextManagers.SuspendMessages():
                    # Ensure the current scene is writable and add/edit in perforce
                    try:
                        Perforce.AddOrEditFiles(baseFbxFilePath, changelistNum=p4ChangelistNum, fileType="binary+lm")
                    except Perforce.P4Exception:
                        Perforce.EnsureFileWritable(baseFbxFilePath)

                    # Save the current scene to disk
                    ulog.LogMessage("Saving base scene to disk", LOG_CONTEXT)
                    Globals.Application.FileSave(baseFbxFilePath)

        splitOutputDir = sceneSplitSettings.getSplitFileOutputFolder(baseFbxFilePath)
        if not os.path.exists(splitOutputDir) or not os.path.isdir(splitOutputDir):
            ulog.LogMessage("Creating split output directory: {}".format(splitOutputDir), LOG_CONTEXT)
            os.makedirs(splitOutputDir)

        ulog.LogMessage("Running scene split", LOG_CONTEXT)

        setNamespaces = sceneSplitSettings.getNamespacesWithReferenceType(Set.Set)

        toSplit = selectedSplitSections or sceneSplitSettings.splitSections

        with util.SceneSplitFbxSdkProcessor(
            ulog=ulog, verbose=verbose
        ) as splitSceneProcessor, ContextManagers.SuspendCallbacks():
            for splitSection in toSplit:
                splitFileBaseName = splitSection.getOutputFileName(baseFbxFilePath)

                ulog.LogMessage("Creating split scene", splitFileBaseName)

                splitFilePath = os.path.join(splitOutputDir, splitFileBaseName)
                splitSceneProcessor.loadFbx(baseFbxFilePath)

                namespacesOwnedByCurrentSplit = frozenset(splitSection.resourceNamespaces)
                inactiveNamespaces = sceneSplitSettings.getAllSceneNamespaces() - namespacesOwnedByCurrentSplit
                namespacesForMarkup = list(inactiveNamespaces - setNamespaces)  # Don't alter materials on set geo
                inactiveNamespaces = list(inactiveNamespaces)

                ulog.LogMessage("Cleaning inactive character components", splitFileBaseName)
                splitSceneProcessor.deleteCharacterisationAndRigs(
                    namespacesForMarkup, skipGsSkels=preserveGsSkelCharacterization
                )

                ulog.LogMessage("Assigning markup materials", splitFileBaseName)
                splitSceneProcessor.assignMarkupMaterial(namespacesForMarkup)

                ulog.LogMessage("Cleaning unused shading components", splitFileBaseName)
                splitSceneProcessor.deleteUnusedShadingComponents(list(inactiveNamespaces))

                ulog.LogMessage("Setting frame range on split", splitFileBaseName)
                splitSceneProcessor.setOutputRange(splitSection.splitFrameRange)

                try:
                    Perforce.AddOrEditFiles(splitFilePath, changelistNum=p4ChangelistNum, fileType="binary+lm")
                except Perforce.P4Exception:
                    # Handle paths outside of p4 depot root
                    Perforce.EnsureFileWritable(splitFilePath)

                ulog.LogMessage("Saving scene", splitFileBaseName)
                splitSceneProcessor.saveFbx(splitFilePath)

    finally:
        # Clean up p4 changelist if it's empty
        if p4ChangelistNum and not Perforce.GetFilesInChangelist(p4ChangelistNum):
            ulog.LogDebug("Deleting empty changelist CL:{}".format(p4ChangelistNum), LOG_CONTEXT)
            Perforce.DeleteChangelist(p4ChangelistNum, deleteShelvedFiles=False)

    ulog.LogMessage("Split scene complete", LOG_CONTEXT)


def _handlePreMergePlottingInSplitScenes(
    sceneSplitSettings,
    selectedSplitSections,
    ulog,
    plotActiveCharacterControlRigs=True,
    plotProps=False,
    plotVehicles=False,
    verbose=False,
):
    """Checks each split scene fbx using the FBXSDK to determine if it contains any relevant active control rigs.
    If any are found then the scene will be opened in Mobu and the rig animation will be plotted to skeleton.

    Args:
        sceneSplitSettings (SceneSplitSettings): The split settings for the current base scene.
        selectedSplitSections (list of SplitSectionSettings): List of settings for each split to be processed.
        ulog (UniversalLog): The log to use during the operation.
        plotActiveCharacterControlRigs (bool, optional): plot characters
        plotProps (bool, optional): plot props
        plotVehicles (bool, optional): plot vehicles
        verbose (bool, optional): If True, the messages logged during the operation will be more detailed.
    """
    if not any((plotActiveCharacterControlRigs, plotProps, plotVehicles)):
        # No plotting required
        return

    LOG_CONTEXT = "PreMergePlot"
    ulog.LogMessage("Checking if any split scenes require pre-merge plot", LOG_CONTEXT)

    baseFbxFilePath = Globals.Application.FBXFileName
    splitOutputDir = sceneSplitSettings.getSplitFileOutputFolder(baseFbxFilePath)

    if selectedSplitSections is None:
        # Validate all if no selection subset passed in
        filteredSplitSections = sceneSplitSettings.splitSections
    else:
        filteredSplitSections = selectedSplitSections

    splitSectionsRequiringPlot = []
    for splitSection in filteredSplitSections:
        splitFileBaseName = splitSection.getOutputFileName(baseFbxFilePath)
        splitFilePath = str(os.path.join(splitOutputDir, splitFileBaseName))
        if not os.path.exists(splitFilePath) or not os.path.isfile(splitFilePath):
            continue
        targetNamespaces = list(splitSection.resourceNamespaces)
        if not targetNamespaces:
            # Skip empty splits
            continue

        # Load a minimal set of data from the file with fbxsdk to check if plot is required
        ulog.LogMessage("Checking if pre-merge plot is required", splitFileBaseName)
        if not util.plotRequiredInFbx(
            splitFilePath,
            targetNamespaces,
            ulog=ulog,
            checkCharacters=plotActiveCharacterControlRigs,
            checkProps=plotProps,
            checkVehicles=plotVehicles,
            verbose=verbose,
        ):
            continue

        splitSectionsRequiringPlot.append(splitSection)

    if not splitSectionsRequiringPlot:
        # Skip pre-plot if none require it
        ulog.LogMessage("No split scenes require pre-merge plot", LOG_CONTEXT)
        return

    prePlotFileNames = [splitSection.getOutputFileName(baseFbxFilePath) for splitSection in splitSectionsRequiringPlot]
    ulog.LogWarning("Found split scene(s) that require plotting: {}".format(", ".join(prePlotFileNames)), LOG_CONTEXT)

    # Create a changelist
    p4ChangelistNum = None
    if Perforce.Enabled():
        ulog.LogDebug("Creating a changelist for pre-merge plot edits", LOG_CONTEXT)
        p4Changelist = Perforce.CreateChangelist("FBX Scene Splitter - Pre-merge plotting has been run on these scenes")
        if p4Changelist:
            p4ChangelistNum = p4Changelist.Number
            ulog.LogDebug("Changelist created: CL:{}".format(p4ChangelistNum), LOG_CONTEXT)

    ulog.LogMessage("Starting pre-merge plot", LOG_CONTEXT)
    requireRestoreOriginalScene = False
    try:
        for splitSection in splitSectionsRequiringPlot:
            splitFileBaseName = splitSection.getOutputFileName(baseFbxFilePath)

            splitFilePath = str(os.path.join(splitOutputDir, splitFileBaseName))

            # Open the file in
            ulog.LogMessage("Opening scene for plotting: {}".format(splitFileBaseName), LOG_CONTEXT)
            Globals.Application.FileOpen(splitFilePath)
            requireRestoreOriginalScene = True

            if plotActiveCharacterControlRigs or plotProps or plotVehicles:
                # Plot any active control rigs that belong to this split
                plotTypes = []
                if plotActiveCharacterControlRigs:
                    plotTypes.append("characters")
                if plotProps:
                    plotTypes.append("props")
                if plotVehicles:
                    plotTypes.append("vehicles")

                plotTypesMsg = " and ".join(plotTypes)

                ulog.LogMessage("Plotting {} in: {}".format(plotTypesMsg, splitFileBaseName), LOG_CONTEXT)
                util.plotAllActiveControlRigs(
                    list(splitSection.resourceNamespaces),
                    plotCharacters=plotActiveCharacterControlRigs,
                    plotProps=plotProps,
                    plotVehicles=plotVehicles,
                )

            with ContextManagers.SuspendMessages():
                # Check out/make writable
                try:
                    Perforce.AddOrEditFiles(splitFilePath, changelistNum=p4ChangelistNum, fileType="binary+lm")
                except Perforce.P4Exception:
                    # Handle files outside of p4 workspace
                    Perforce.EnsureFileWritable(splitFilePath)

                # Save
                ulog.LogMessage("Saving: {}".format(splitFileBaseName), LOG_CONTEXT)
                Globals.Application.FileSave(splitFilePath)
    finally:
        # Clean up p4 changelist if it's empty
        if p4ChangelistNum and not Perforce.GetFilesInChangelist(p4ChangelistNum):
            ulog.LogDebug("Deleting empty changelist CL:{}".format(p4ChangelistNum), LOG_CONTEXT)
            Perforce.DeleteChangelist(p4ChangelistNum, deleteShelvedFiles=False)

        if requireRestoreOriginalScene:
            # Reopen the original base scene
            ulog.LogMessage("Reopening base scene", LOG_CONTEXT)
            Globals.Application.FileOpen(baseFbxFilePath)

    ulog.LogMessage("Pre-merge plot complete", LOG_CONTEXT)


def _handlePreMergeAssetRemapping(sceneSplitSettings, selectedSplitSections, remappingDict, ulog, verbose=False):
    """Remaps assets in each scene split

    Args:
        sceneSplitSettings (SceneSplitSettings): The split settings for the current base scene.
        selectedSplitSections (list of SplitSectionSettings): List of settings for each split to be processed.
        remappingDict (dict): Namespaces to remap
        ulog (UniversalLog): The log to use during the operation.
        verbose (bool, optional): If True, the messages logged during the operation will be more detailed.
    """
    LOG_CONTEXT = "PreMergeAssetMismatch"
    ulog.LogMessage("Pre-merge fixing mismatched assets", LOG_CONTEXT)

    baseFbxFilePath = Globals.Application.FBXFileName
    splitOutputDir = sceneSplitSettings.getSplitFileOutputFolder(baseFbxFilePath)
    assetFilters = (CharacterComplex.CharacterComplex, Prop.Prop, Vehicle.Vehicle)

    preMismatchFileNames = [splitSection.getOutputFileName(baseFbxFilePath) for splitSection in selectedSplitSections]
    ulog.LogWarning("Found split scene(s) that require fixes: {}".format(", ".join(preMismatchFileNames)), LOG_CONTEXT)

    # Create a changelist
    p4ChangelistNum = None
    if Perforce.Enabled():
        ulog.LogDebug("Creating a changelist for pre-merge asset mismatch edits", LOG_CONTEXT)
        p4Changelist = Perforce.CreateChangelist(
            "FBX Scene Splitter - Pre-merge asset remapping has been run on these scenes"
        )
        if p4Changelist:
            p4ChangelistNum = p4Changelist.Number
            ulog.LogDebug("Changelist created: CL:{}".format(p4ChangelistNum), LOG_CONTEXT)

    ulog.LogMessage("Starting pre-merge asset remap", LOG_CONTEXT)
    requireRestoreOriginalScene = False
    try:
        refManager = Manager.Manager()

        for namespace, otherNamespace in remappingDict.iteritems():
            reference = refManager.GetReferenceByNamespace(namespace)
            remappingDict[namespace] = {"namespace": otherNamespace, "path": reference.Path}

        for idx, splitSection in enumerate(selectedSplitSections):

            splitFileBaseName = splitSection.getOutputFileName(baseFbxFilePath)
            splitFilePath = str(os.path.join(splitOutputDir, splitFileBaseName))

            # get index
            splitIndex = None
            for orgSplitIndex, orgSplitSection in enumerate(sceneSplitSettings.splitSections):
                if orgSplitSection.getOutputFileName(baseFbxFilePath) == splitFileBaseName:
                    splitIndex = orgSplitIndex
                    break

            # Open the file in
            ulog.LogMessage("Opening scene for asset mismatch: {}".format(splitFileBaseName), LOG_CONTEXT)
            Globals.Application.FileOpen(splitFilePath)
            requireRestoreOriginalScene = True

            targetNamespaces = refManager.GetNamespaceList()

            # go through each namespace
            for namespace, assetDict in remappingDict.iteritems():
                if assetDict["namespace"] not in targetNamespaces:
                    continue

                # get reference
                reference = refManager.GetReferenceByNamespace(assetDict["namespace"])
                if not reference or not isinstance(reference, assetFilters):
                    continue

                # swap namespace
                refManager.ChangeNamespace(reference, str(namespace), includeIteration=False)
                ulog.LogDebug(
                    "Changed Namespace: {} to {}".format(assetDict["namespace"], namespace),
                    LOG_CONTEXT,
                )

                # swap path reference
                previousPath = reference.Path
                if not previousPath == assetDict["path"]:
                    refManager.SwapReference(reference, str(assetDict["path"]))
                    ulog.LogDebug("Swapped referenced: {} to {}".format(previousPath, assetDict["path"]), LOG_CONTEXT)

            with ContextManagers.SuspendMessages():
                # Check out/make writable
                try:
                    Perforce.AddOrEditFiles(splitFilePath, changelistNum=p4ChangelistNum, fileType="binary+lm")
                except Perforce.P4Exception:
                    # Handle files outside of p4 workspace
                    Perforce.EnsureFileWritable(splitFilePath)

                # Save
                ulog.LogMessage("Saving: {}".format(splitFileBaseName), LOG_CONTEXT)
                Globals.Application.FileSave(splitFilePath)

    finally:
        # Clean up p4 changelist if it's empty
        if p4ChangelistNum and not Perforce.GetFilesInChangelist(p4ChangelistNum):
            ulog.LogDebug("Deleting empty changelist CL:{}".format(p4ChangelistNum), LOG_CONTEXT)
            Perforce.DeleteChangelist(p4ChangelistNum, deleteShelvedFiles=False)

        if requireRestoreOriginalScene:
            # Reopen the original base scene
            ulog.LogMessage("Reopening base scene", LOG_CONTEXT)
            Globals.Application.FileOpen(baseFbxFilePath)

    ulog.LogMessage("Pre-merge asset mismatch complete", LOG_CONTEXT)


def mergeSplits(
    sceneSplitSettings,
    selectedSplitSections=None,
    ulog=None,
    prePlotAnyActiveControlRigs=False,
    prePlotProps=False,
    prePlotVehicles=False,
    verbose=False,
    createPosesVerticalSplit=False,
):
    """
    Merges animation from namespaces owned by split FBX files back into the base scene.

    Args:
        sceneSplitSettings (SceneSplitSettings): Scene split settings dict
        selectedSplitSections (list of SplitSectionSettings): List of split sections that should be processed.
            If None, animation from all splits will be merged.
        ulog (UniversalLog): The ulog to use for messages during the merge operation.
        prePlotAnyActiveControlRigs (bool): If True,  animation on active control rigs inside the split scenes
            will be plotted to skeleton before the animation is merged back to the base scene.
        prePlotProps (bool): If True, all prop animation in split scenes will be plotted before merging.
        prePlotVehicles (bool): If True, all vehicle animation in split scenes will be plotted.
        verbose (bool): If True, the messages logged during the merge operation will be more detailed.
        createPosesVerticalSplit (bool): If True, all vertical splits will get poses created for each
                                         asset inbetween splits
    """
    if ulog is None:
        ulog = SPLIT_SCENE_LOG
    LOG_CONTEXT = "MergeSplits"

    ulog.LogMessage("Performing pre-merge validation", LOG_CONTEXT)
    validationResults = validation.ValidationResults(raiseExceptionOnError=True, ulog=ulog)
    validation.preMergeValidation(sceneSplitSettings, validationResults, selectedSplitSections=selectedSplitSections)

    # Load split settings for the current scene
    baseFbxFilePath = Globals.Application.FBXFileName

    if selectedSplitSections is None:
        # Merge all if no selection subset passed in
        filteredSplitSections = sceneSplitSettings.splitSections
    else:
        filteredSplitSections = selectedSplitSections

    # Ignore empty splits
    filteredSplitSections = [splitSection for splitSection in filteredSplitSections if splitSection.resourceNamespaces]

    selectedSplitSuffixesStr = ", ".join([splitSection.splitSuffix for splitSection in filteredSplitSections])
    msg = "Merging splits [{}] to base fbx: {}".format(selectedSplitSuffixesStr, baseFbxFilePath)
    ulog.LogMessage(msg, LOG_CONTEXT)

    splitOutputDir = sceneSplitSettings.getSplitFileOutputFolder(baseFbxFilePath)

    # Check if we need to replace assets
    if validationResults.hasRemapping():
        # open files and remap
        _handlePreMergeAssetRemapping(
            sceneSplitSettings, filteredSplitSections, validationResults.remapping(), ulog, verbose=verbose
        )

        # remap data
        fileNameOutputs = [
            sceneSplitSection.getOutputFileName(baseFbxFilePath)
            for sceneSplitSection in sceneSplitSettings.splitSections
        ]
        for newNamespace, assetDict in validationResults.remapping().iteritems():
            # update filtered section namespace
            for idx, splitSection in enumerate(filteredSplitSections):
                # remap namespace if exists
                resourceNamespaces = splitSection.resourceNamespaces
                oldIndex = resourceNamespaces.index(assetDict["namespace"])
                if oldIndex != -1:
                    resourceNamespaces[oldIndex] = newNamespace
                    splitSection.resourceNamespaces = resourceNamespaces
                else:
                    continue

                # remap scene settings
                sceneIndex = fileNameOutputs.index(splitSection.getOutputFileName(baseFbxFilePath))
                if sceneIndex != -1:
                    splitScene = sceneSplitSettings.splitSections[sceneIndex]
                    splitOldIndex = splitScene.resourceNamespaces.index(assetDict["namespace"])
                    if splitOldIndex != -1:
                        resourceNamespaces[splitOldIndex] = newNamespace
                        splitScene.resourceNamespaces = resourceNamespaces

            sceneSplitSettings.writeToFile()

    # Check for active control rigs and perform pre-plot in scenes that require it
    if prePlotAnyActiveControlRigs or prePlotProps or prePlotVehicles:
        _handlePreMergePlottingInSplitScenes(
            sceneSplitSettings,
            filteredSplitSections,
            ulog,
            plotActiveCharacterControlRigs=prePlotAnyActiveControlRigs,
            plotProps=prePlotProps,
            plotVehicles=prePlotVehicles,
            verbose=verbose,
        )

    # Loop through splits merging animation
    ulog.LogMessage("Starting animation merge", LOG_CONTEXT)
    animationMerged = False
    fullFrameRange = sceneSplitSettings.splitFrameRange
    allSuffixes = [splitSection.splitSuffix for splitSection in filteredSplitSections]

    with util.SceneSplitFbxSdkProcessor(
        ulog=ulog, verbose=verbose
    ) as splitSceneProcessor, ContextManagers.SuspendCallbacks():
        for splitSection in filteredSplitSections:
            splitFileBaseName = splitSection.getOutputFileName(baseFbxFilePath)

            animImportValid = False
            allControlsList = []
            for namespace in splitSection.resourceNamespaces:
                animControls = util.getAnimControls(namespace)
                allControlsList.append(animControls)

                if animControls:
                    animImportValid = True

            allControls = list(itertools.chain(*allControlsList))

            if not animImportValid:
                msg = "Skipping animation merge - failed to find any related animation controls: {}".format(
                    splitFileBaseName
                )
                ulog.LogWarning(msg, LOG_CONTEXT)
                continue

            # Deactivate any control rig inputs so that the imported skeletal animation will be visible
            for character in util.iterCharactersWithActiveControlRigs(splitSection.resourceNamespaces):
                character.ActiveInput = False

            # load fbx and read animation data
            splitFilePath = os.path.join(splitOutputDir, splitFileBaseName)
            splitSceneProcessor.loadFbx(splitFilePath)

            ulog.LogMessage("Merging animation from: {}".format(splitFileBaseName), LOG_CONTEXT)
            # Merge animation onto selected objects
            splitFrameRange = splitSection.splitFrameRange
            splitSceneProcessor.mergeAnimation(splitFrameRange, allControls)
            animationMerged = True

            if (
                not createPosesVerticalSplit
                or allSuffixes.count(splitSection.splitSuffix) < 2  # check if it's vertical split
                or all(splitFrameRange[idx] == fullFrameRange[idx] for idx in xrange(2))
            ):
                continue

            # get frames for poses
            poseFrames = [splitFrameRange[idx] for idx in xrange(2) if splitFrameRange[idx] != fullFrameRange[idx]]

            # get pose folder
            poseFolder = None
            for comp in Globals.Folders:
                if const.POSES_FOLDER == comp.Name:
                    poseFolder = comp
                    break

            sceneSplitterPoseFolder = createPoseFolder(const.SPLIT_POSE_FOLDER, poseFolder)
            currentSplitPoseFolder = createPoseFolder(
                "{} Poses".format(splitFileBaseName.split(".fbx")[0]), sceneSplitterPoseFolder
            )

            ulog.LogMessage("Adding poses for splits...", LOG_CONTEXT)

            # create poses
            refManager = Manager.Manager()
            for poseFrame in poseFrames:
                Globals.Player.Goto(mobu.FBTime(0, 0, 0, poseFrame))
                Globals.Scene.Evaluate()
                for idx, namespace in enumerate(splitSection.resourceNamespaces):
                    reference = refManager.GetReferenceByNamespace(namespace)
                    poseName = "_".join([str(namespace), str(poseFrame)])

                    if isinstance(reference, CharacterComplex.CharacterComplex):
                        characterComponent = reference.Component()
                        pose = AnimLib.AddPoseToPoseControl(poseName, characterComponent)
                    else:
                        pose = AnimLib.AddPoseObjectToPoseControl(poseName, components=allControlsList[idx])

                    currentSplitPoseFolder.ConnectSrc(pose)

    if not animationMerged:
        raise exceptions.SceneSplitToolError("No animation was merged. See previous warnings for details.")

    ulog.LogMessage("Merging splits completed successfully", LOG_CONTEXT)


def getStoryESTData():
    """Gathers Story data from root

    Returns:
        dict: Dictionary of story data
    """
    storyData = {}

    for track in Globals.Story.RootEditFolder.Tracks:
        if track.Name.endswith("_EST"):
            # gather clip data
            clipData = []
            for clip in track.Clips:
                clipData.append(
                    {
                        "shotCamera": clip.ShotCamera.Name,
                        "name": clip.Name,
                        "markIn": clip.MarkIn,
                        "markOut": clip.MarkOut,
                        "loop": clip.Loop,
                        "autoLoop": clip.AutoLoop,
                        "loopTranslation": clip.LoopTranslation,
                        "timewarpEnabled": clip.TimeWarpEnabled,
                        "timewarpReverse": clip.TimeWarpReverse,
                        "timewarpInterpolatorType": clip.TimeWarpInterpolatorType,
                        "color": clip.Color,
                        "offset": clip.Offset,
                        "preBlend": clip.PreBlend,
                        "postBlend": clip.PostBlend,
                        "speed": clip.Speed,
                        "start": clip.Start,
                        "stop": clip.Stop,
                        "shotActionStart": clip.ShotActionStart,
                        "shotActionStop": clip.ShotActionStop,
                    }
                )

            # create dict entry with track data
            storyData[track.Name] = {
                "clips": clipData,
                "solo": track.Solo,
                "backPlate": track.ShowBackplate,
                "frontPlate": track.ShowFrontplate,
                "mute": track.Mute,
            }

    return storyData


def setStoryESTData(storyData):
    """Sets story data in scene

    Args:
        storyData (dict): data from getStoryESTData
    """
    sceneTracks = [track for track in Globals.Story.RootEditFolder.Tracks if track.Name.endswith("_EST")]

    for trackName, trackData in storyData.iteritems():
        storyTrack = None
        for track in sceneTracks:
            if track.Name == trackName:
                storyTrack = track
                # remove all clips
                for clip in reversed(storyTrack.Clips):
                    clip.FBDelete()
                break

        if storyTrack is None:
            storyTrack = mobu.FBStoryTrack(mobu.FBStoryTrackType.kFBStoryTrackShot, Globals.Story.RootEditFolder)
            storyTrack.Name = trackName

        storyTrack.Solo = trackData["solo"]
        storyTrack.Mute = trackData["mute"]
        storyTrack.ShowBackplate = trackData["backPlate"]
        storyTrack.ShowFrontplate = trackData["frontPlate"]

        for clipData in trackData["clips"]:
            # create clip
            cameraNode = mobu.FBFindModelByLabelName(clipData["shotCamera"])
            clip = mobu.FBStoryClip(cameraNode, storyTrack, clipData["start"])
            clip.Name = clipData["name"]
            clip.MarkIn = clipData["markIn"]
            clip.MarkOut = clipData["markOut"]
            clip.Loop = clipData["loop"]
            clip.AutoLoop = clipData["autoLoop"]
            clip.LoopTranslation = clipData["loopTranslation"]
            clip.TimeWarpEnabled = clipData["timewarpEnabled"]
            clip.TimeWarpInterpolatorType = clipData["timewarpInterpolatorType"]
            clip.TimeWarpReverse = clipData["timewarpReverse"]
            clip.Color = clipData["color"]
            clip.Offset = clipData["offset"]
            clip.PreBlend = clipData["preBlend"]
            clip.PostBlend = clipData["postBlend"]
            clip.Start = clipData["start"]
            clip.Stop = clipData["stop"]
            clip.Speed = clipData["speed"]
            clip.ShotActionStart = clipData["shotActionStart"]
            clip.ShotActionStop = clipData["shotActionStop"]


def getCameraData(cameras):
    """Gather camera data based on clip cameras

    Args:
        cameras (list): Camera names to get data on

    Returns:
        dict: camera data
    """
    cameraData = {}
    for camera in cameras:
        cameraNode = mobu.FBFindModelByLabelName(camera)
        cameraData[camera] = AnimLib.getAnimData(cameraNode, allProperties=True, allLayers=True)

    return cameraData


def exportCameraData(cameras, filePath=None):
    """Gather camera data based on clip cameras

    Args:
        cameras (list): Camera names to get data on
        filePath (str): Where to save the file

    Returns:
        (str): Filepath used
    """
    if filePath is None:
        filePath = os.path.join(tempfile.gettempdir(), "{}.fbx".format(uuid.uuid4()))

    for comp in Globals.Components:
        comp.Selected = False

    for camera in cameras:
        cameraNode = mobu.FBFindModelByLabelName(camera)
        if cameraNode:
            cameraNode.Selected = True

    # set merge options
    saveOptions = mobu.FBFbxOptions(False)
    saveOptions.SetAll(mobu.FBElementAction.kFBElementActionDiscard, False)
    saveOptions.Models = mobu.FBElementAction.kFBElementActionSave
    saveOptions.Cameras = mobu.FBElementAction.kFBElementActionSave
    saveOptions.CamerasAnimation = True
    saveOptions.SaveSelectedModelsOnly = True

    Globals.Application.FileSave(filePath, saveOptions)

    return filePath


def importCameraData(filePath, frameRange=None):
    """Import camera data based on fbx merge

    Args:
        filePath (str): File to merge
        frameRange (list of 2 int): Frame range to cull
    """
    timeSpan = Globals.System.CurrentTake.LocalTimeSpan
    fixedTimeSpan = mobu.FBTimeSpan(timeSpan.GetStart(), timeSpan.GetStop())
    mergeOptions = mobu.FBFbxOptions(True, filePath)
    mergeOptions.SetAll(mobu.FBElementAction.kFBElementActionDiscard, False)
    mergeOptions.Models = mobu.FBElementAction.kFBElementActionMerge
    mergeOptions.Cameras = mobu.FBElementAction.kFBElementActionMerge
    mergeOptions.CamerasAnimation = True
    result = Globals.Application.FileMerge(filePath, False, mergeOptions)

    if frameRange is not None and result:
        for cameraNode in Globals.Cameras:
            AnimLib.deleteKeysByRange(cameraNode, frameRange, allLayers=True)

    Globals.System.CurrentTake.LocalTimeSpan = fixedTimeSpan


def updateCameras(sceneSplitSettings, ulog=None, verbose=False):
    """Updates all cameras data in split scenes based on base scene

    Args:
        sceneSplitSettings (dict): The split settings for the current base scene.
        ulog (UniversalLog.UniversalLog): Optional log to use during the split operation.
        verbose (bool): If True, more verbose messages will be logged during the split operation.
    """
    if ulog is None:
        ulog = SPLIT_SCENE_LOG
    LOG_CONTEXT = "UpdateCamera"

    p4ChangelistNum = None
    if Perforce.Enabled():
        p4Changelist = Perforce.CreateChangelist("FBX Scene Splitter")
        if p4Changelist:
            p4ChangelistNum = p4Changelist.Number

    # get story info
    storyData = getStoryESTData()
    cameraNames = []
    for storyName, data in storyData.iteritems():
        for clip in data["clips"]:
            cameraName = clip["shotCamera"]
            if cameraName not in cameraNames:
                cameraNames.append(cameraName)

    # grab export camera if exists
    exportCamera = mobu.FBFindModelByLabelName("ExportCamera")
    if exportCamera:
        cameraNames.append(exportCamera.Name)

    # grab camera switcher data
    switcherData = AnimLib.getAnimData(Globals.CameraSwitcher, properties=["Camera Index"], allLayers=True)

    baseFbxFilePath = Globals.Application.FBXFileName

    # export camera data
    cameraFbxPath = baseFbxFilePath.replace(".fbx", "_cameraExport_{}.fbx".format(uuid.uuid4()))
    exportCameraData(cameraNames, filePath=cameraFbxPath)

    ulog.LogMessage("Starting camera update from base fbx: [{}]".format(baseFbxFilePath), LOG_CONTEXT)
    splitOutputDir = sceneSplitSettings.getSplitFileOutputFolder(baseFbxFilePath)

    requireRestoreOriginalScene = False
    try:
        # go through each split
        for splitSection in sceneSplitSettings.splitSections:
            splitFileBaseName = splitSection.getOutputFileName(baseFbxFilePath)

            ulog.LogMessage("Opening scene for camera update: {}".format(splitFileBaseName), LOG_CONTEXT)

            splitFilePath = str(os.path.join(splitOutputDir, splitFileBaseName))

            # open file to import data
            Globals.Application.FileOpen(splitFilePath)
            requireRestoreOriginalScene = True

            # merge cameras in
            importCameraData(cameraFbxPath, frameRange=splitSection.splitFrameRange)
            ulog.LogMessage("Imported camera data: {}".format(splitFileBaseName), LOG_CONTEXT)

            # update switcher data, grabbing new instance so it's unique to the scene
            AnimLib.setAnimData(mobu.FBCameraSwitcher(), switcherData)

            # update story/tracks
            setStoryESTData(storyData)
            ulog.LogMessage("Story data updated: {}".format(splitFileBaseName), LOG_CONTEXT)

            with ContextManagers.SuspendMessages():
                # Check out/make writable
                try:
                    Perforce.AddOrEditFiles(splitFilePath, changelistNum=p4ChangelistNum, fileType="binary+lm")
                except Perforce.P4Exception:
                    # Handle files outside of p4 workspace
                    Perforce.EnsureFileWritable(splitFilePath)

                # Save
                ulog.LogMessage("Saving: {}".format(splitFileBaseName), LOG_CONTEXT)
                Globals.Application.FileSave(splitFilePath)

    finally:
        # Clean up p4 changelist if it's empty
        if p4ChangelistNum and not Perforce.GetFilesInChangelist(p4ChangelistNum):
            ulog.LogDebug("Deleting empty changelist CL:{}".format(p4ChangelistNum), LOG_CONTEXT)
            Perforce.DeleteChangelist(p4ChangelistNum, deleteShelvedFiles=False)

        if requireRestoreOriginalScene:
            # delete camera fbx path
            os.remove(cameraFbxPath)
            # Reopen the original base scene
            ulog.LogMessage("Reopening base scene", LOG_CONTEXT)
            Globals.Application.FileOpen(baseFbxFilePath)

    ulog.LogMessage("Camera update complete", LOG_CONTEXT)


def createPoseFolder(poseFolderName, parentFolder):
    """Creates a pose folder

    Args:
        poseFolderName (str): Name of pose folder
        parentFolder (mobu.FBFolder): Parent folder component

    Returns:
        mobu.FBFolder: Folder either found or created
    """
    # get pose folder if exists
    poseFolder = None
    for idx in xrange(parentFolder.GetSrcCount()):
        childFolder = parentFolder.GetSrc(idx)
        if poseFolderName == childFolder.Name:
            poseFolder = childFolder
            break

    # if it can't be found, create it
    if poseFolder is None:
        poseFolder = mobu.FBFolder(poseFolderName, parentFolder)

    return poseFolder
