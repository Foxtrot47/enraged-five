class SceneSplitException(Exception):
    """Base exception type used by SceneSplitter"""
    pass


class SceneSplitUserError(SceneSplitException):
    """Exception type used when an error occurs due to setup controlled by the user or a user action"""
    pass


class SceneSplitToolError(SceneSplitException):
    """Exception type used when the tool fails in a way that is unrecoverable (i.e. not fixable by the user)"""
    pass


class SceneSplitSettingsIOError(SceneSplitToolError):
    """Exception type used when an error occurs while writing/reading settings to/from the scene"""
    pass
