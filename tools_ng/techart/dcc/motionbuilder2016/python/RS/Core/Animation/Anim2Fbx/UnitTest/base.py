import os
import unittest

import pyfbsdk as mobu

from RS import Perforce, Globals
from RS.Unittests import utils
from RS.Core.ReferenceSystem.UnitTest import base
from RS.Core.Animation.Anim2Fbx.UnitTest import const


class Base(base.Base):
    """
    Base class for the Anim2Fbx UnitTests, it inherits the base UnitTest class from the Reference System Unit Tests
    so it can reference in characters to apply the animation to.
    """

    KEY = None

    def setUp(self):
        """Clears the scene before running the test"""
        Globals.Application.FileNew()

    def tearDown(self):
        """Clears the scene after running the test"""
        for path in self.paths:
            try:
                os.remove(path)
            except WindowsError as err:
                print(err)

    def test_Animation(self):
        """Test importing of animation"""
        self.KEY = self.KEY if self.KEY else self.__class__.__name__.split("_")[-1]

        self.paths = []

        animConfig = utils.loadYaml(const.ConfigPathDict.ANIM_CONFIG_PATH)
        self.animData = utils.getConfigKey(self.KEY, animConfig)

        if not self.animData:
            print("No config key found for {} to test asset".format(self.KEY))
            return False

        self.animPath = self.animData.get("animPath", None)
        self.animRevision = self.animData.get("animRevision", None)

        self.skeletonPath = self.animData.get("skeletonPath", None)
        self.skeletonRevision = self.animData.get("skelRevision", None)

        self.referencePath = self.animData.get("referencePath", None)
        self.namespaces = self.animData.get("namespaces", None)

        if not os.path.exists(self.animPath):
            self.paths.append(self.animPath)

        Perforce.Sync(self.animPath, force=True, revision=self.animRevision)
        if not self.animPath or not os.path.exists(self.animPath):
            self.fail(
                "{} animation path doesn't exist! Manually sync this file or check the animations.yaml config".format(
                    self.animPath
                )
            )

        if not os.path.exists(self.skeletonPath):
            self.paths.append(self.skeletonPath)

        Perforce.Sync(self.skeletonPath, force=True, revision=self.skeletonRevision)
        if not self.animPath or not os.path.exists(self.animPath):
            self.fail(
                "{} skeleton path doesn't exist! Manually sync this file or check the animations.yaml config".format(
                    self.skeletonPath
                )
            )

        return True

    def assertAnimation(self, componentName):
        """Check if the component has any keyframes

        Arguments:
            componentName (str): Name of component to check
        """
        camera = mobu.FBFindModelByLabelName(componentName)
        self.assertNotEquals(camera.AnimationNode.Nodes[0].KeyCount, 0)

    def assertExists(self, name):
        """
        Arguments:
            name (str): Name of model to assert
        """
        self.assertIsNotNone(mobu.FBFindModelByLabelName(name))
