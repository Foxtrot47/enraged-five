import os


class ConfigPathDict(object):
    """Yaml config paths for unittests"""
    ANIM_CONFIG_PATH = os.path.join(os.path.dirname(__file__), "configs", "animations.yaml")
