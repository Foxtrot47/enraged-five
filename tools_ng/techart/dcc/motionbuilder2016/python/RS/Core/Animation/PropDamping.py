'''

 Script Path: RS\Core\Animation\PropDamping.py

 Written And Maintained By: Kathryn Bodey

 Created: 13.08.14

 Description: props set up in such a way to get a delayed time offset between each bone 
              to get snake/tail-like motion which could be used for ropes/straps/chains 
              as well as possible other props

'''

from pyfbsdk import *

import RS.Globals  
import RS.Utils.Scene


def ReferencePropList():

	# find props in scene via reference
	referenceScene = None
	refNullList = []
	propList = []

	for component in RS.Globals.gComponents:
		if component.LongName == 'REFERENCE:Scene' and component.ClassName() == 'FBModelNull':    
			referenceScene = component

	if referenceScene != None:
		RS.Utils.Scene.GetChildren( referenceScene, refNullList, "", False )

		if len( refNullList ) > 0:
			for asset in refNullList:
				if asset.PropertyList.Find( 'rs_Asset_Type' ):
					typeProperty = asset.PropertyList.Find( 'rs_Asset_Type' )
					if typeProperty.Data == 'Props':
						propList.append( asset )

		return propList

def CreateConstraint( selectedProp, userNewDampValue, userEditDampValue, newBool ):
		
	prop = RS.Utils.Scene.FindModelByName( selectedProp + ':Dummy01', True )

	if prop:		
		# get main control
		mainControl = None
		multiProp = None

		propName = selectedProp		

		if '^' in propName:
			splitName = propName.split( '^' )
			propName = splitName[0]
			multiProp = splitName[-1]


		if RS.Utils.Scene.FindModelByName( propName + ':' + propName + '_Control', True ):
			mainControl = RS.Utils.Scene.FindModelByName( propName + ':' + propName + '_Control', True )
		elif RS.Utils.Scene.FindModelByName( propName + ':' + 'Gun_GripR_Control', True ):
			mainControl = RS.Utils.Scene.FindModelByName( propName + ':' + 'Gun_GripR_Control', True )
		elif RS.Utils.Scene.FindModelByName( propName + ':' 'Root_' + propName + '_Control', True ):
			mainControl = RS.Utils.Scene.FindModelByName( propName + ':' 'Root_' + propName + '_Control', True )
		elif RS.Utils.Scene.FindModelByName( propName + ':' + propName + '_Root_Control', True ):
			mainControl = RS.Utils.Scene.FindModelByName( propName + ':' + propName + '_Root_Control', True )		

		if multiProp:
			propName = selectedProp

		if mainControl:
			
			if newBool == True:			
			
				# get list of controls
				controlList = []
				RS.Utils.Scene.GetChildren( mainControl, controlList, "", True )
	
				# create a relation constraint
				relationConstraint = FBConstraintRelation( propName + '_DampingRelation')
				relationConstraintProperty = relationConstraint.PropertyCreate('Damping Relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
				relationConstraintProperty.Data = propName
				dampingValueProperty = relationConstraint.PropertyCreate('Damping Value', FBPropertyType.kFBPT_charptr, "", False, True, None)
				dampingValueProperty.Data = str( userNewDampValue )
	
				integer = relationConstraint.CreateFunctionBox( 'Number', "Integer")
				relationConstraint.SetBoxPosition( integer, -300, 0 )				
				integera = RS.Utils.Scene.FindAnimationNode( integer.AnimationNodeInGet(),'a' )
				integera.WriteData( [userNewDampValue] )	
	
				y = 0
				for i in controlList:
					
					if i.Name == 'mover_Control': 
						None
					
					else:
						sender = relationConstraint.SetAsSource( i )
						relationConstraint.SetBoxPosition( sender, 0, y )	
			
						receiver = relationConstraint.ConstrainObject( i )
						relationConstraint.SetBoxPosition( receiver, 1000, y )
			
						damping = relationConstraint.CreateFunctionBox( 'Other', "Damping (3D)")
						relationConstraint.SetBoxPosition( damping, 600, y - 50 )		
			
						damping2 = relationConstraint.CreateFunctionBox( 'Other', "Damping (3D)")
						relationConstraint.SetBoxPosition( damping2, 600, y + 50 )
						
						RS.Utils.Scene.ConnectBox( sender, 'Rotation', damping, 'P' )
						RS.Utils.Scene.ConnectBox( damping, 'Result', receiver, 'Rotation' )
						RS.Utils.Scene.ConnectBox( sender, 'Translation', damping2, 'P' )
						RS.Utils.Scene.ConnectBox( damping2, 'Result', receiver, 'Translation' )
						RS.Utils.Scene.ConnectBox( integer, 'Result', damping, 'Damping Factor' )
						RS.Utils.Scene.ConnectBox( integer, 'Result', damping2, 'Damping Factor' )
						
						y = y + 300
										
				return relationConstraint			
			
			else: 
				propDampingRelation = None
				integer = None
				
				for i in RS.Globals.Constraints:
					dampingProperty = i.PropertyList.Find( 'Damping Relation' )
					dampingValueProperty = i.PropertyList.Find( 'Damping Value' )
					
					if dampingProperty and dampingValueProperty:
						if dampingProperty.Data == propName:
							propDampingRelation = i

				if propDampingRelation:
					
					for box in propDampingRelation.Boxes:
						if 'Integer' in box.Name:
							integer = box

				if integer:
					integera = RS.Utils.Scene.FindAnimationNode( integer.AnimationNodeInGet(),'a' )
					integera.WriteData( [userEditDampValue] )	
					
					dampingValueProperty.Data = str( userEditDampValue )
					
				return propDampingRelation
		
		else:
			FBMessageBox( 'R* Error', 'Unable to find the main Control null of prop {0}.'. format( selectedProp ), 'Ok' )
			
			return None
		
	else:
		FBMessageBox( 'R* Error', 'Unable to find prop {0}, in the scene.'. format( selectedProp ), 'Ok' )
		
		return None