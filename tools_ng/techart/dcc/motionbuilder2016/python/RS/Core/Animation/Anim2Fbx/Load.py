"""
This module contains code specific for the AnimToFbx Motion Builder tool but that is independent from the QT UI used

The main purpose of this module is to separate the functionality of the tool from the UI components.
"""

import os
import re
from functools import wraps
from collections import OrderedDict

import pyfbsdk as mobu

from PySide import QtGui

from RS import Perforce
from RS.Utils import Path
from RS.Core.Animation import Lib
from RS.Core.Animation.Anim2Fbx import AnimTrack
from RS.Core.Animation.DataManager.Skeleton import SkeletonObject


def ImportCamera(filepath):
    """
    Creates a new camera in the scene that matches the animation from the .anim file with camera
    keyframe data

    Arguments:
        filepath (string): path to the anim file

    """
    Lib.CopyTake(filepath)
    cameraData = AnimTrack.AnimObject(path=filepath)
    cameraData.ImportCamera()
    SetTakeRange(0, cameraData.Duration)
    return True


def Plot(path, namespace, skeletonPath="", startFrame=0, poseFrame=None, degreesOfFreedomOn=True, setTimeRange=True):
    """
    Plots the animation data found in an anim file to the components provided by the file in the provided namespace.

    Arguments:
        path (string): path to the .anim file with the animation data to plot
        namespace (string): namespace to look under for the components to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        startFrame (int): frame on which to start the animation
        poseFrame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        setTimeRange (boolean): updates the frame range to match the length of the animation
    Returns:
        AnimTrack instance
    """

    bones = []
    createPose = type(poseFrame) in (int, float)

    # We check if skeleton path has a value before running os.path.exists
    # os.path.exists errors out if you pass None or anything other than a string
    skeletonData = None
    if skeletonPath and os.path.exists(skeletonPath):
        skeletonData = SkeletonObject(skeletonPath)
        bones = skeletonData.BoneIds

    # animationPath = animationPath.replace("%40", "@")
    AnimData = AnimTrack.AnimObject(path=path, SkeletonObject=skeletonData, SkeletonPath=skeletonPath)
    
    if poseFrame and poseFrame < 0:
        # negative numbers access poses from the end of the animation
        # for example -1 would resolve to the last frame of animation
        poseFrame = AnimData.Frames + poseFrame

    if createPose:
        AnimData.ApplyPose(namespace, bones=bones, frame=poseFrame, startFrame=startFrame,
                           degreesOfFreedomOn=degreesOfFreedomOn)
        result = poseFrame

    elif not createPose and setTimeRange:
        SetTakeRange(startFrame, AnimData.Duration)
        result = AnimData.ApplyAnimation(namespace, bones=bones, startFrame=startFrame,
                                         degreesOfFreedomOn=degreesOfFreedomOn)
    else:
        result = AnimData.ApplyAnimation(namespace, bones=bones, startFrame=startFrame,
                                         degreesOfFreedomOn=degreesOfFreedomOn)
    Lib.SetPlayerControl()

    return result


def ResolvePath(path):
    """
    Takes a path and resolves the Perforce path and windows path for the given path

    Arguments:
        path (string): path to file or directory, can be a perforce or local path

    Return:
        tuple(string(Perforce Path), string(Windows Path)]

    """
    state = Perforce.GetFileState(path)

    if not state:
        path = path.replace("%40", "@")
        syncPath = path.replace("@", "%40").replace("\\", "/")
        if re.match("[A-Za-z]:", syncPath):
            syncPath = "/{}".format(syncPath[2:])

        if re.match("//", path):
            path = "x:{}".format(path[1:].replace("/", "\\"))

    else:
        path = state.ClientFilename.replace("%40", "@")
        syncPath = state.DepotFilename

    return syncPath, path


def DoesPathExist(paths, sync=True, extensions=(".anim",)):
    """
    Checks if a path exists, if they don't then we try to sync with perforce to retrieve the files.

    Arguments:
        paths = list[string, string, etc.] ; paths to check if they exist
        sync (boolean): if we should sync with perforce

    Return:
        Boolean
    """
    # Check to see if a path was passed in
    if isinstance(paths, basestring):
        paths = [paths]

    for path in paths:

        if not path:
            return False

        if type(path) is unicode:
            path = str(path)

        # if not path.lower().startswith("x"):
        #     path = os.path.abspath(path.encode("string-escape"))\

        syncPath, path = ResolvePath(path)

        isFile = any([path.endswith(extension) for extension in extensions])
        # Sync file

        if (not os.path.exists(path) and sync) or sync:
            # try:
                if isFile:
                    Perforce.Sync(syncPath, force=True)
                else:
                    for extension in extensions:
                        Perforce.Run("sync", ["{}...{}".format(syncPath, extension)])

            # except Exception, e:
            #     print "Error encounter when trying to Sync File:\n", e

        # Check if we have the file locally
        if not os.path.exists(path):
            QtGui.QMessageBox.warning(None, "Warning", ('{}\n was not found on local drive.\n'
                                                        'Try syncing to perforce depot').format(path))
            return False
    return True


def CheckPathExist(func):
    """
    Checks if a path exists, if they don't then we try to sync with perforce to retrieve the files.

    This decorator does intercept the incoming parameters of the methods it decorates. It expects the decorated
    methods first argument to be a path to a file/directory and adds support for the sync keyword even if the
    decorated function doesn't support it. The decorated function must be able to support keyword arguments.
    """

    @wraps(func)
    def wrapper(path, *args, **kwargs):
        sync = kwargs.get("sync", True)

        if not DoesPathExist(path, sync):
            return

        isList = isinstance(path, list)
        if not isList:
            path = [path]

        paths = OrderedDict()
        for eachPath in path:
            _, eachPath = ResolvePath(eachPath)

            correctedPath = eachPath
            if "^" in eachPath:
                correctedPath = eachPath.replace("^", "_")
                os.rename(eachPath, correctedPath)

            paths[eachPath] = correctedPath

        results = func([paths.values()[0], paths.values()][isList], *args, **kwargs)

        # rename files that had the "^" removed
        [os.rename(corrected, original) for original, corrected in paths.iteritems() if original != corrected]

        return results
    return wrapper


@CheckPathExist
def LoadFile(filePath, skeletonPath="", startFrame=0, poseFrame=None, useCurrentTake=False,
             componentType=mobu.FBCharacter, namespaces=[], sync=True, storePose=False, warning=True,
             degreesOfFreedomOn=True):
    """
    Applies the animation from the provided file to the currently selected character.

    This method differs from the Plot() method as this method creates a new take or completely overrides
    all animation on the current take and replaces it with the data from the provided file. The Plot() method
    will only override animation data that lies on the same frame as the data from the provided file. Any subsequent
    keyframe information is kept on the rig. The LoadFile() method will clear all animation data before applying
    the new keyframe data.

    Arguments:
        filePath (string): path to the .anim file with the animation data to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        startFrame (int): frame on which to start the animation
        poseFrame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
        componentType (FBComponent): type of component to apply animation on, defaults to mobu.FBCharacter
        namespaces (list[string, etc.]); list of namespaces to look for objects to apply animation to in.
        sync (boolean): sync anim files from Perforce before importing animations. The sync parameter is used by the
                        CheckPathExist decorator
        storePose (boolean): add pose to pose control when poseFrame is a valid number

    """

    take = Lib.CopyTake(filePath, useCurrentTake, warning=warning)

    if componentType is mobu.FBCamera:
        return ImportCamera(filePath)

    characterActiveStates = [(character, character.Active) for namespace in namespaces
                             for character in mobu.FBSystem().Scene.Characters
                             if character.LongName.startswith("{}:".format(namespace))]

    results = [Plot(path=filePath, namespace=namespace, skeletonPath=skeletonPath, startFrame=startFrame,
                    poseFrame=poseFrame, degreesOfFreedomOn=degreesOfFreedomOn) for namespace in namespaces]

    if poseFrame is not None and storePose and componentType == mobu.FBCharacter:
        character = characterActiveStates[0][0] if characterActiveStates else None

        # Interface to use the transport controls
        lPlayer = mobu.FBPlayerControl()
        # Go to the next frame.
        lPlayer.Goto(mobu.FBTime(0, 0, 0, 1))
        # Run Scene.Evaluate to make sure all bones update to teh correct position
        mobu.FBSystem().Scene.Evaluate()
        poseName = "FRAME_{0:03d}_of_ANIM_{1}".format(results[-1], Path.GetBaseNameNoExtension(filePath))
        # Add Pose
        Lib.AddPoseToPoseControl(poseName, pCharacter=character)
        # Go back to frame zero
        lPlayer.Goto(mobu.FBTime(0, 0, 0, 0))
        # Delete take
        take.FBDelete()
        [setattr(character, "Active", active) for character, active in characterActiveStates]
    return any([result is not None for result in results])


def LoadFiles(filePaths, startFrame=0, poseFrame=None, useCurrentTake=False,
              componentType=mobu.FBCharacter, namespaces=[], skeletonPath="", sync=True, storePose=False, warning=True,
              degreesOfFreedomOn=True):
    """
    Applies the animation from the provided files to the currently selected character.

    Arguments:
        filePath (string): path to the .anim file with the animation data to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        startFrame (int): frame on which to start the animation
        poseFrame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
        componentType (FBComponent): type of component to apply animation on, defaults to mobu.FBCharacter
        namespaces (list[string, etc.]); list of namespaces to look for objects to apply animation to in.
        sync (boolean): sync anim files from Perforce before importing animations
        storePose (boolean): add pose to pose control when poseFrame is a valid number
    """
    for filePath in filePaths:
        if not LoadFile(filePath, startFrame=startFrame, poseFrame=poseFrame, useCurrentTake=useCurrentTake,
                        componentType=componentType, namespaces=namespaces, skeletonPath=skeletonPath, sync=sync,
                        storePose=storePose, warning=warning, degreesOfFreedomOn=degreesOfFreedomOn):
            print "{} failed to load".format(filePath)
            return
    return True


@CheckPathExist
def LoadFolder(folderPath, startFrame=0, poseFrame=None, useCurrentTake=False,
               componentType=mobu.FBCharacter, namespaces=[], skeletonPath="", sync=True, storePose=False,
               warning=True, degreesOfFreedomOn=True):
    """
    Applies the animation from the files in the provided folder to the currently selected character.

    Arguments:
        filePath (string): path to the .anim file with the animation data to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        startFrame (int): frame on which to start the animation
        poseFrame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
        componentType (FBComponent): type of component to apply animation on, defaults to mobu.FBCharacter
        namespaces (list[string, etc.]); list of namespaces to look for objects to apply animation to in.
        sync (boolean): sync anim files from Perforce before importing animations
        storePose (boolean): add pose to pose control when poseFrame is a valid number
    """
    files = [os.path.join(path, eachFile) for path, folders, files in os.walk(folderPath) for eachFile in files
             if os.path.isfile(os.path.join(path, eachFile)) and eachFile.endswith(".anim")]
    return LoadFiles(files, startFrame=startFrame, poseFrame=poseFrame, useCurrentTake=useCurrentTake,
                     componentType=componentType, namespaces=namespaces, skeletonPath=skeletonPath, sync=sync,
                     storePose=storePose, warning=warning, degreesOfFreedomOn=degreesOfFreedomOn)


def LoadFolders(folderPaths, startFrame=0, poseFrame=None, useCurrentTake=False,
                componentType=mobu.FBCharacter, namespaces=[], skeletonPath="", sync=True, storePose=False,
                warning=True, degreesOfFreedomOn=True):
    """
    Applies the animation from the files in the provided folder to the currently selected character.

    Arguments:
        filePath (string): path to the .anim file with the animation data to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        startFrame (int): frame on which to start the animation
        poseFrame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
        componentType (FBComponent): type of component to apply animation on, defaults to mobu.FBCharacter
        namespaces (list[string, etc.]); list of namespaces to look for objects to apply animation to in.
        sync (boolean): sync anim files from Perforce before importing animations
        storePose (boolean): add pose to pose control when poseFrame is a valid number
    """
    for folderPath in folderPaths:
        LoadFolder(folderPath, startFrame=startFrame, poseFrame=poseFrame, useCurrentTake=useCurrentTake,
                   componentType=componentType, namespaces=namespaces, skeletonPath=skeletonPath, sync=sync,
                   storePose=storePose, warning=warning, degreesOfFreedomOn=degreesOfFreedomOn)


def Load(paths, skeletonPath="", startFrame=0, poseFrame=None, useCurrentTake=False,
         componentType=mobu.FBCharacter, namespaces=[], sync=True, storePose=False, warning=True,
         degreesOfFreedomOn=True):
    """
    Applies the animation from the files in the provided list to the currently selected character.

    Arguments:
        filePath (string): path to the .anim file with the animation data to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        startFrame (int): frame on which to start the animation
        poseFrame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
        componentType (FBComponent): type of component to apply animation on, defaults to mobu.FBCharacter
        namespaces (list[string, etc.]); list of namespaces to look for objects to apply animation to in.
        sync (boolean): sync anim files from Perforce before importing animations
        storePose (boolean): add pose to pose control when poseFrame is a valid number

    Return:
        tuple; ([list of folder paths],  [list of file paths])
    """
    # Separate files from folders
    animFolders, animFiles = GetValidAnimPaths(paths=paths)

    if animFolders:
        LoadFolders(animFolders, namespaces=namespaces, skeletonPath=skeletonPath,
                    useCurrentTake=useCurrentTake, sync=sync, componentType=componentType,
                    startFrame=startFrame, poseFrame=poseFrame, storePose=storePose, warning=warning,
                    degreesOfFreedomOn=degreesOfFreedomOn)
        
    if animFiles:
        LoadFiles(animFiles, namespaces=namespaces, skeletonPath=skeletonPath,
                  useCurrentTake=useCurrentTake, sync=sync, componentType=componentType,
                  startFrame=startFrame, poseFrame=poseFrame, storePose=storePose, warning=warning,
                  degreesOfFreedomOn=degreesOfFreedomOn)
    return animFolders, animFiles


def GetValidAnimPaths(paths):
    """
    Prunes the passed list of anim files and folders of duplicate paths to avoid importing the same anim twice

    Arguments:
        paths (list[string, etc.]): path of anim files and folders

    Return:
        list[strings, etc.], list[strings, etc.]

        a list of the anim folders and another of animfiles

    """
    # Separate files from folders
    animFiles = []
    animFolders = []

    for path in paths:
        if path.endswith(".anim"):
            animFiles.append(path)
        else:
            animFolders.append(path)

    if animFolders:
        # Prune file list so that files that are within folders that we are importing do not import multiple times
        animFiles = [animFile for animFile in animFiles for animFolder in animFolders if animFolder not in animFile]
        animFiles = list(set(animFiles))
        animFiles.sort()

    return animFolders, animFiles


def SetTakeRange(startFrame, duration):
    """
    Adjusts the range of the current take

    Arguments:
        startFrame (int): most animations start at frame 0, but can have an offset
        duration (int): duration (in frames) of the current animation track
    """
    # apply range here (startFrame, duration + startFrame)
    lStartFrame = mobu.FBTime(0, 0, 0, startFrame)
    lEndFrame = mobu.FBTime(0, 0, 0, duration+startFrame-1)

    lPlayerControls = mobu.FBPlayerControl()
    lPlayerControls.LoopStart = lStartFrame
    lPlayerControls.ZoomWindowStart = lStartFrame
    lPlayerControls.LoopStop = lEndFrame
    lPlayerControls.ZoomWindowStop = lEndFrame
    lPlayerControls.Goto(lStartFrame)

    # Workaround for getting the last take to commit
    curTake = mobu.FBSystem().CurrentTake
    curTake.LocalTimeSpan = mobu.FBTimeSpan(lStartFrame, lEndFrame)