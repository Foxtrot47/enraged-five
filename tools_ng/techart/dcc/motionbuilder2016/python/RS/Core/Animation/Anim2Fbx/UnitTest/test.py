import unittest

from RS.Core.Animation.Anim2Fbx.UnitTest import animations


def getTestSuites():
    """Gathers suites together to test

    Returns:
        unittest.TestSuite
    """
    suite = unittest.TestSuite()
    suite.addTest(animations.suite())
    return suite


def Run():
    """Runs unittests"""
    unittest.TextTestRunner(verbosity=2).run(getTestSuites())


if __name__ == "__main__":
    Run()
