from RS.Core.Mocap.Giant import PrepCharacter
from RS.Utils import Scene

import pyfbsdk as mobu

def ResetRollbones():
    '''
    Finds the Rollbones in the scene via the constraints, selects them and then clears the animation.
    '''
    # deselect all
    Scene.DeSelectAll()

    # get list of rollbones and select them
    rollBoneDict = PrepCharacter.GetAllRollBoneDrives()
    for rollBoneContainer in rollBoneDict.itervalues():
        rollBone = rollBoneContainer.rollBone
        if rollBone:
            rollBone.Selected = True
            translation = rollBone.PropertyList.Find('Lcl Translation')
            rotation = rollBone.PropertyList.Find('Lcl Rotation')
            scaling = rollBone.PropertyList.Find('Lcl Scaling')
            translation.Selected = True
            rotation.Selected = True
            scaling.Selected =- True

    # clear animation on selected
    mobu.FBSystem().CurrentTake.ClearAllProperties(True)

    # deselect all
    Scene.DeSelectAll()

ResetRollbones()