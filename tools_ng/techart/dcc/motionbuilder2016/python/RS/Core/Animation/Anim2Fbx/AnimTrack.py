import os.path

import RS.Globals
import RS.Config
from RS.Core.Animation.DataManager import AnimTrack
from RS.Core.Animation.Anim2Fbx import Apply
# Static variables

FACIAL_CONTROL_ID = {control_id: control_name for control_name, control_id
                     in RS.Globals.gAmbientControllerIds.items()}
XYZ= {"x": 0,
      "y": 1,
      "z": 2}

# Anim Files for testing purposes
DEBUG_BODY_FILE = os.path.join(RS.Config.Project.Path.Art,'anim\export_mb\COMBAT@\BULLET_REACTIONS\BEHIND_1.anim')
DEBUG_FACE_FILE = os.path.join(RS.Config.Project.Path.Art,'anim\export_mb\FACIALS@\GEN_FEMALE@\BASE\BlowKiss_1.anim')
DEBUG_SKELETON_FILE = os.path.join(RS.Config.Tool.Path.Root, 'etc\\config\\anim\\skeletons\\player.skel')

# Classes


class AnimObject(AnimTrack.AnimObject):
    """
    Inherits AnimTrack.AnimObject and expands it's functionality by adding Motion Builder specific code
    """

    def __init__(self, path, **keywordArguments):
        """
        Parses an anim file and stores it's information through attributes to make accessing the
        information easier

        Arguments:
            animation_data_path = string; path to an .anim or .anim.txt file
        """
        path = os.path.abspath(path)
        if not os.path.isfile(path):
            path = ''
        super(AnimObject, self).__init__(path, **keywordArguments)

    def ApplyAnimation(self, namespace, bones=[], startFrame=0, degreesOfFreedomOn=True):
        """
        Applies the stored animation data to the currently selected Character in Motion Builder
        Arguments:
            namespace (string): namespace of the bones that the animation is being applied to
            bones (list[string, etc.]): specific bones to apply the animation to
            startFrame (int): The start frame for the animation, by default it is 0
        """
        trackList = getattr(self, "TrackList", [])
        if bones:
            trackList = self.GetPoses(bones=bones)

        return Apply.MapToSkeleton(namespace, trackList, startFrame, degreesOfFreedomOn=degreesOfFreedomOn)

    def ApplyPose(self, namespace, frame, bones=[], startFrame=0, degreesOfFreedomOn=True):
        """
        Applies a pose from the stored animation data to the currently selected Character in Motion Builder

        Arguments:
            namespace (string): namespace of the bones that the animation is being applied to
            frame (int): frame of the animation to apply
            bones (list[string, etc.]): specific bones to apply the animation to
            startFrame (int): The start frame for the animation, by default it is 0
        """
        trackList = self.GetPoses(frame, bones)
        return Apply.MapToSkeleton(namespace, trackList, startFrame, True, degreesOfFreedomOn=degreesOfFreedomOn)

    def ImportCamera(self, name=None):
        """
        Creates a new camera and applies the saved animation to it
        """
        CameraTrack = filter(None, [self.GetTrackByName(each_property) for each_property in
                                    ("cameraTranslation", "cameraRotation", "cameraFOV")])
        return Apply.MapToCamera(CameraTrack, name=None)