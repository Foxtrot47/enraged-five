"""
Unit test for importing animation from anim clips into motion builder
"""
import os
import sys
import unittest

from scandir import scandir

from RS import Perforce, Globals
from RS.Unittests import utils
from RS.Utils import ContextManagers
from RS.Core.Animation.Anim2Fbx import Load
from RS.Core.Animation.Anim2Fbx.UnitTest import base
from RS.Tools.Animation.Anim2Fbx.Widgets import ToolBox


def Run(className=None):
    """Run unittests from this module"""
    unittest.TextTestRunner(verbosity=0).run(suite(className))


def suite(className=None):
    """A suite to hold all the tests from this module

    Args:
        className (str): The name of the class whose test should be ran. Runs all the tests if None.
    Returns:
        unittest.TestSuite
    """
    module = sys.modules[__name__]
    return utils.getSuite(module, "test_Animation", className)


class Test_AnimFolder(base.Base):
    def test_Animation(self):
        """Testing a folder import and checking takes were imported properly"""
        if not super(Test_AnimFolder, self).test_Animation():
            return

        if not os.path.exists(self.referencePath):
            self.paths.append(self.referencePath)

        # sync reference asset
        Perforce.Sync(self.referencePath, force=True)
        if not self.referencePath or not os.path.exists(self.referencePath):
            self.fail(
                "{} reference path doesn't exist! Manually sync this file or check the animations.yaml config".format(
                    self.referencePath
                )
            )

        # Create a reference so we can apply animation to it
        reference = self.assertCreate(self.referencePath)
        animPath = self.animPath.split("\\...")[0]

        # get takes before import and make a list
        defaultTakes = [take.Name for take in Globals.Scene.Takes]
        defaultTakes.extend(
            [
                path.name.split(".")[0]
                for path in scandir(animPath)
                if path.name.endswith(".anim")
            ]
        )

        with ContextManagers.SuspendMessages():
            Load.Load([animPath], namespaces=self.namespaces)

        self.assertEqual([take.Name for take in Globals.Scene.Takes], defaultTakes)
        self.assertDelete(reference)


class Test_AnimSource(base.Base):
    def test_Animation(self):
        """Testing that anim was made with same skeleton as target"""
        if not super(Test_AnimSource, self).test_Animation():
            return

        animTargetMismatch = Load.ValidateAnimSource(self.animPath, self.namespaces[0])
        self.assertEqual(animTargetMismatch, (False, "female.skel", "biped.skel"))


class Test_Cameras(base.Base):
    def test_Animation(self):
        """Test animation imports on a camera"""
        if not super(Test_Cameras, self).test_Animation():
            return

        Load.ImportCamera(self.animPath)

        self.assertExists("ImportedCamera")
        self.assertAnimation("ImportedCamera")


class Test_Character(base.Base):
    def test_Animation(self):
        """Testing that animation imports on a character"""
        if not super(Test_Character, self).test_Animation():
            return

        if not os.path.exists(self.referencePath):
            self.paths.append(self.referencePath)

        # sync reference asset
        Perforce.Sync(self.referencePath, force=True)
        if not self.referencePath or not os.path.exists(self.referencePath):
            self.fail(
                "{} reference path doesn't exist! Manually sync this file or check the animations.yaml config".format(
                    self.referencePath
                )
            )

        # Create a reference so we can apply animation to it
        namespace = self.assertCreate(self.referencePath)

        # Load animation and check
        with ContextManagers.SuspendMessages():
            Load.Load([self.animPath], namespaces=self.namespaces)

        self.assertAnimation(":".join([self.namespaces[0], "SKEL_ROOT"]))
        self.assertDelete(namespace)


class Test_Face(base.Base):
    def test_Animation(self):
        """Testing that the toolbox finds face files from an anim path"""
        if not super(Test_Face, self).test_Animation():
            return

        if not os.path.exists(self.referencePath):
            self.paths.append(self.referencePath)

        # sync face file as it's needed to be able to find it
        Perforce.Sync(self.referencePath, force=True)
        if not self.referencePath or not os.path.exists(self.referencePath):
            self.fail(
                "{} face path doesn't exist! Manually sync this file or check the animations.yaml config".format(
                    self.referencePath
                )
            )

        facePath = ToolBox.ToolBox().FindFaceFile(self.animPath)

        self.assertIsNot(facePath, None)
