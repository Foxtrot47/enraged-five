"""
Library module for animation related tools and functions.

Author:
    * Jason Hayes <jason.hayes@rockstarsandiego.com>
    * Thanh Phan <thanh.phan@rockstarsandiego.com>
    * David Vega <david.vega@rockstargames.com>
"""

import os
import re
import shutil
import filecmp
import tempfile
import subprocess
import collections
from xml.etree import cElementTree as xml

from PySide import QtGui

import pyfbsdk as mobu

from RS import Config, Globals, Perforce, ProjectData
from RS.Utils import Path, Scene, Namespace, Strings, ZipUtils, ContextManagers
from RS.Utils.Scene import Component, Constraint, RelationshipConstraintManager as rcm, Property

PLOT_TYPE = {
    "rig": mobu.FBCharacterPlotWhere.kFBCharacterPlotOnControlRig,
    "skeleton": mobu.FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton,
}


class InputTypes(object):
    """
    Stores the input type constants from the pyfbsdk lib
    """

    Character = mobu.FBCharacterInputType.kFBCharacterInputCharacter
    Rig = mobu.FBCharacterInputType.kFBCharacterInputMarkerSet
    Stance = mobu.FBCharacterInputType.kFBCharacterInputStance

    _FindDictionary = {
        mobu.FBCharacterInputType.kFBCharacterInputCharacter: "Character",
        mobu.FBCharacterInputType.kFBCharacterInputMarkerSet: "Rig",
        mobu.FBCharacterInputType.kFBCharacterInputStance: "Stance",
    }

    @classmethod
    def GetNameFromType(cls, characterInputType):
        """
        Returns the name of the constant in this class that corresponds with the passed pyfbsdk FBCharacterInputType
        constant

        Arguments:
            characterInputType (mobu.FBCharacterInputType): motion builder character input type

        Return:
            string
        """
        return cls._FindDictionary.get(characterInputType, None)


def ThumbJointOrientationToggle(lNamespace, overrideCS=False):
    """
    Orients the thumb joints

    Arguments:
        lNamespace (str): namespace for the character whose thumbs you want to edit
        overrideCS (bool): If true, use the cutscene rotation values regardless
    """

    # Finger Rotation Values
    rotationValues_IG = {
        "SKEL_R_Finger00": mobu.FBVector3d(10.344, 15.851, 30.603),
        "SKEL_R_Finger01": mobu.FBVector3d(-0.404659, -1.43456, -16.5556),
        "SKEL_R_Finger02": mobu.FBVector3d(-0.0504775, -1.16117, -13.269),
        "SKEL_L_Finger00": mobu.FBVector3d(-10.3441, -15.8509, 30.6029),
        "SKEL_L_Finger01": mobu.FBVector3d(0.4047, 1.43454, -16.5556),
        "SKEL_L_Finger02": mobu.FBVector3d(0.0501915, 1.16085, -13.2691),
    }

    rotationValues_CS = {
        "SKEL_R_Finger00": mobu.FBVector3d(90, 0, 0),
        "SKEL_R_Finger01": mobu.FBVector3d(0, 0, 0),
        "SKEL_R_Finger02": mobu.FBVector3d(0, 0, 0),
        "SKEL_L_Finger00": mobu.FBVector3d(-90, 0, 0),
        "SKEL_L_Finger01": mobu.FBVector3d(0, 0, 0),
        "SKEL_L_Finger02": mobu.FBVector3d(0, 0, 0),
    }

    # Find Mover Constraint
    lMoverConstraint = None
    for each in Globals.gConstraints:
        if each.LongName == "{0}:mover_toybox_Control".format(lNamespace):
            lMoverConstraint = each

    # Find Character
    lCharacter = None
    for each in Globals.gCharacters:
        if each.LongName.split(":")[0] == lNamespace:
            lCharacter = each

    if lMoverConstraint is not None and lCharacter is not None:

        # Store initial state and prepare
        lMoverState = lMoverConstraint.Active
        lMoverConstraint.Active = False
        lCharacter.SetCharacterizeOff()
        Globals.gScene.Evaluate()

        # Determine the finger state
        lArray = rotationValues_CS
        lFingerCheck = mobu.FBFindModelByLabelName("{0}:{1}".format(lNamespace, "SKEL_R_Finger00"))
        if not overrideCS and 89.9 < lFingerCheck.Rotation[0] < 90.1:
            lArray = rotationValues_IG

        # Set to match the other state
        for each in lArray:
            lFinger = mobu.FBFindModelByLabelName("{0}:{1}".format(lNamespace, each))
            lFinger.Rotation = lArray[each]
            Globals.gScene.Evaluate()

        # Post Preparation
        lCharacter.SetCharacterizeOn(True)
        lMoverConstraint.Active = lMoverState
        Globals.gScene.Evaluate()


def scaleKey(keyframe, matrix, scaleFactorX=1, scaleFactorY=1, framerate=30.0):
    """
    Scales a key based on the given 2d matrix obtained from the getFCurveMatrix method
    Currently only scales from left to right.

    Args:
        keyframe (mobu.FBFCurveKey): key to scale
        matrix (list): matrix to scale
        scaleFactorX (float): factor to scale the x (time) by
        scaleFactorY (float): factor to scale the y (key value) by
    """
    frame = keyframe.Time.GetSecondDouble() * framerate
    # Create matrix with the frame and value offset
    fbmatrix = mobu.FBMatrix()
    fbmatrix[12], fbmatrix[13] = frame - matrix[4], keyframe.Value - matrix[5]

    # Create the scale matrix
    scaleMatrix = mobu.FBMatrix()
    scaleMatrix[0] = scaleFactorX or 1
    scaleMatrix[5] = scaleFactorY or 1

    # Scale the matrix
    scaledMatrix = scaleMatrix * fbmatrix

    # Remove offsets
    frame = scaledMatrix[12] + matrix[4]
    value = scaledMatrix[13] + matrix[5]

    fbtime = mobu.FBTime()
    fbtime.SetTimeString(str(frame))

    keyframe.Time = fbtime
    keyframe.value = value


def getFCurveMatrix(keys):
    """
    Gets the Bounding Box / 2D Matrix representation of a set of keys

    Args:
        keys (list): list of FBFCurveKeys

    Returns:
        list
    """
    valueMin = None
    valueMax = None
    timeMin = None
    timeMax = None
    for key in keys:
        value, frame = key.Value, key.Time.GetFrame()
        if None in (valueMin, valueMax):
            valueMin, valueMax = value, value
        elif value < valueMin:
            valueMin = value
        elif value > valueMax:
            valueMax = value

        if None in (timeMin, timeMax):
            timeMin, timeMax = frame, frame
        elif frame < timeMin:
            timeMin = frame
        elif frame > timeMax:
            timeMax = frame

    return [timeMax - timeMin, 0, 0, valueMax - valueMin, timeMin, valueMin]


def CopyPropertyValues(source, destinations, properties=["Translation (Lcl)", "Rotation (Lcl)"]):

    if not isinstance(destinations, (tuple, list)):
        destinations = [destinations]

    propertyValues = {
        propertyName: source.PropertyList.Find(propertyName).Data
        for propertyName in properties
        if source.PropertyList.Find(propertyName) is not None
    }
    [Component.SetProperties(destination, **propertyValues) for destination in destinations]


def CopyAnimation(
    source,
    destinations,
    properties=("Translation (Lcl)", "Rotation (Lcl)"),
    transfer=False,
    clear=False,
    allLayers=False,
    allTakes=False,
):
    """
    Copies animation from the source component to the destination components

    Arguments:
        source (FBComponent): component to copy animation from
        destinations  (list[FBComponent, etc.]): components to copy animation to
        properties (list[string, etc.]): name of the properties whose animation should be copied
        transfer (bool): if the animation from the source component should be deleted after copying it to the
                          destination objects
        clear (bool): clear the keys on the destination property before copying the keys. If the source has no keys
                         then keys on the destination will be cleared.
        allLayers (bool): copy keys all the layers and not just the current layer.
        allTakes (bool): If True, copy animation on all takes. Otherwise, only copy animation on the current take.
    """
    propertyMatches = []

    for propertyName in properties:
        sourceProperty = source.PropertyList.Find(propertyName)
        sourceAnimationNode = getattr(sourceProperty, "GetAnimationNode", lambda *args: None)()
        if not sourceProperty or (not sourceAnimationNode and not clear):
            continue

        for destination in destinations:

            destinationProperty = destination.PropertyList.Find(propertyName)

            if type(sourceProperty) != type(destinationProperty) or (
                destination is not None and not destinationProperty.IsAnimatable()
            ):
                continue

            destinationProperty.SetAnimated(True)

            destinationAnimationNode = destinationProperty.GetAnimationNode()

            if destinationProperty is None or (destinationAnimationNode is None and not clear):
                continue

            propertyMatches.append(Property.PropertyMatch(sourceProperty, destinationProperty))

    CopyAnimationPropertyToProperty(
        propertyMatches, allTakes=allTakes, allLayers=allLayers, clear=clear, transfer=transfer
    )


def CopyAnimationPropertyToProperty(propertyMatches, allTakes=True, allLayers=True, clear=False, transfer=False):
    """
    Copies animation curves from source properties to target properties.

    Args:
        propertyMatches (list of Property.PropertyMatch): The list of source to target property mapping to copy between.
        allTakes (bool): If True, copy animation on all takes. Otherwise, only copy animation on the current take.
        allLayers (bool): copy keys all the layers and not just the current layer.
        clear (bool): clear the keys on the destination property before copying the keys. If the source has no keys
                         then keys on the destination will be cleared.
        transfer (bool): if the animation from the source component should be deleted after copying it to the
                          destination objects
    """
    if not propertyMatches:
        return

    originalTake = Globals.System.CurrentTake
    if allTakes:
        targetTakes = [take for take in Globals.Takes]
    else:
        targetTakes = [originalTake]

    uniqueSourceProperties = set(propMatch.SourceProperty for propMatch in propertyMatches)

    for currentTake in targetTakes:
        Globals.System.CurrentTake = currentTake

        originalLayerIndex = currentTake.GetCurrentLayer()

        if allLayers:
            layerIndexes = xrange(currentTake.GetLayerCount())
        else:
            layerIndexes = [originalLayerIndex]

        for layerIndex in layerIndexes:
            currentTake.SetCurrentLayer(layerIndex)

            for propertyMatch in propertyMatches:
                if clear:
                    ClearAnimationFromProperty(propertyMatch.TargetProperty)

                propertyMatch.CopyAnimationData()

            if transfer:
                # Delete curves from the source properties if we are transferring animation
                for uniqueSourceProperty in uniqueSourceProperties:
                    ClearAnimationFromProperty(uniqueSourceProperty)

        # Restore the original layer as the current one
        currentTake.SetCurrentLayer(originalLayerIndex)

    # Restore the initial take as the current take
    Globals.System.CurrentTake = originalTake


def ClearAnimation(component, properties=None, allProperties=False):
    """
    Clear the animation from the provided properties of a component

    Arguments:
        component (mobu.FBComponent): component to clear animation from
        properties (list[mobu.FBProperty, etc.]): list of FBProperties to clear
        allProperties (bool): clear all the properties of the component
    """
    properties = properties or []
    if allProperties:
        properties = component.PropertyList

    for property in properties:
        ClearAnimationFromProperty(property)


def ClearAnimationFromProperty(property):
    """
    Clears the animation from a property

    Arguments:
        property (mobu.FBProperty): property with animation to clear
    """
    if property.IsAnimatable() and property.IsAnimated() and property.GetAnimationNode():

        animationNodes = property.GetAnimationNode().Nodes
        if not animationNodes:
            animationNodes = [property.GetAnimationNode()]
        data = property.Data
        for eachNode in animationNodes:
            eachNode.FCurve.EditClear()
        property.Data = data


# --- Get/Query Methods ---


def GetCurrentCharacter(warning=False):
    """
    Gets the current / last selected character

    Arguments:
        warning = (bool):  When true it launches a dialog window when the command fails

    Return:
       mobu.FBCharacter() instance
    """
    lApp = mobu.FBApplication()
    lCurrentCharacter = lApp.CurrentCharacter

    if not lCurrentCharacter and warning:
        QtGui.QMessageBox.warning(None, "Anim2FBX Error", "No Character Selected")

    return lCurrentCharacter


def GetNamespace(character=None, warning=False):
    """
    Gets the Namespace of the provided character

    Arguments:
        character (mobu.FBComponent): The component whose namespace you want, defaults to the selected
                                                 FBCharacter
        warning (bool): When true it launches a dialog window when the command fails

    Return:
        string
    """
    if not character:
        character = GetCurrentCharacter(warning)

    namespace = Namespace.GetNamespace(character)

    if not namespace and warning:
        QtGui.QMessageBox.warning(
            None, "Anim2FBX Error", "You did not use the reference system to load your character."
        )

    return namespace


# --- FBTake Methods ---


def CopyTake(path, use_take=False, warning=True):
    """
    Copies the current take

    Arguments:
        path (str): path to an anim file. The name of the file is used as the name of the new take
        use_take (bool): use the current take instead of creating a copy, this clears animation on the current take
        warning (bool): if a warning should pop up that you are going to use the current take
    """
    # use current take instead of a copy if checkbox is checked
    lTake = mobu.FBSystem().CurrentTake

    if use_take and warning:
        # Throw a Messagebox warning to make sure this is what is desired
        use_take = (
            QtGui.QMessageBox.warning(
                None,
                "WARNING WARNING WARNING!!!!!!!!!!!!!!!!!!",
                "This will overwrite an existing anim on the take." " Are you sure you want to do this?",
                QtGui.QMessageBox.Yes,
                QtGui.QMessageBox.No,
            )
            == QtGui.QMessageBox.Yes
        )

    baseName = str(Path.GetBaseNameNoExtension(path))

    if not use_take:
        lTake = lTake.CopyTake(baseName)

        # CopyTake copies the animation data too
        # We need to get rid of them.
        lTake.ClearAllProperties(False)
    return lTake


# --- Set Properties Methods ---


def EnableRotationDOF(boneName, setting=True, namespace=None):
    """
    Enable Degrees of Freedom

    Arguments:
        boneName (str): name of the FBModelBone to enable degrees of freedom on
        setting (bool): should rotation be active
        namespace (str): namespace of the bone to enable degrees of freedom on
    """
    if not namespace:
        namespace = GetNamespace()
    lBone = mobu.FBFindModelByLabelName("{}:{}".format(namespace, boneName))
    lBone.PropertyList.Find("RotationActive").Data = setting


def SetPlayerControl(isSixty=False):
    """Sets the player control to 30 FPS or 60 FPS

    Args:
        isSixty (bool): If true, set the player control to 60 FPS instead of 30.
    """
    lPlayerControl = mobu.FBPlayerControl()
    lPlayerControl.SnapMode = mobu.FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames
    if isSixty:
        lPlayerControl.SetTransportFps(mobu.FBTimeMode.kFBTimeMode60Frames)
    else:
        lPlayerControl.SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)


def Plot(
    character,
    plot_type,
    CreateControlRig=False,
    AllTakes=False,
    OnFrame=True,
    Period=(0, 0, 0, 1),
    RotationFilter=mobu.FBRotationFilter.kFBRotationFilterGimbleKiller,
    UseConstantKeyReducer=True,
    ConstantKeyReducerKeepOnKey=True,
    TranslationOnRootOnly=True,
    ForceInputActive=False,
    PlotLockedProperties=False,
    FrameRate=None,
):
    """
    Plots animation on a character. It will plot from the current input to the skeleton, current input to the rig, or
    if the current input is the rig, skeleton to the rig.

    Args:
        character (mobu.FBCharacter): character to plot
        plot_type (string): to plot to skeleton or rig
        CreateControlRig (boolean): create a control rig for the character
        AllTakes (boolean): plot the current animation to all the takes
        OnFrame (boolean): ?
        Period (list[int, int, int, int]): the number of frames between keyframes.
        RotationFilter (pyfbsdk.FBRotationFilter): Rotation filter to apply to the animation
        UseConstantKeyReducer (boolean): Removes keys whose values do not change over for consecutive frames
        ConstantKeyReducerKeepOnKey (boolean): keeps one frame keyed instead of removing all keys when the
                                               ConstantKeyReducer parameter is used
        TranslationOnRootOnly (boolean): only key translation values on the root node of the character
        ForceInputActive (boolean): activates the characters input if it isn't activated
        PlotLockedProperties (bool): Should we plot locked properties
        FrameRate (mobu.FBTimeMode): Frame rate to override if needed, otherwise use preferred

    Return:
        boolean
    """
    plot_location = PLOT_TYPE.get(plot_type, PLOT_TYPE["skeleton"])
    # Create a control rig using Forward and Inverse Kinematics,
    # as specified by the "True" parameter.
    active = character.ActiveInput
    previousType = character.InputType

    # Create Control Rig and make sure that it isn't set to active
    if CreateControlRig:
        character.CreateControlRig(True)
        character.ActiveInput = False

    # If the character was being driven by another character make sure to set it back to being driven by that
    # character

    if previousType is InputTypes.Character:
        character.InputType = previousType
        character.ActiveInput = active

    # Turn on input if
    if ForceInputActive:
        # Set the control rig to active.
        character.ActiveInput = True

    plotOptions = PlotOptions(
        frameStep=Period[-1],
        allTakes=AllTakes,
        onFrame=OnFrame,
        rotationFilter=RotationFilter,
        useConstantKeyReducer=UseConstantKeyReducer,
        constantKeyReducerKeepOnKey=ConstantKeyReducerKeepOnKey,
        translationOnRootOnly=TranslationOnRootOnly,
        plotLockedProperties=PlotLockedProperties,
        frameRate=FrameRate,
    )

    character.PlotAnimation(plot_location, plotOptions)

    return True


def PlotOptions(
    frameStep=1,
    allTakes=False,
    onFrame=True,
    rotationFilter=mobu.FBRotationFilter.kFBRotationFilterGimbleKiller,
    useConstantKeyReducer=True,
    constantKeyReducerKeepOnKey=True,
    translationOnRootOnly=False,
    plotLockedProperties=False,
    frameRate=None,
):
    """
    Get the FBPlotOptions object to pass into a plot method and set the value for it.

    Args:
        frameStep (int): frame step number, only accepts integers higher than one
        children (boolean): key properties of all children components as well
        frameStep (int): step to key by
        allTakes (boolean): plot all takes
        onFrame (boolean): plot on frame and not between frames
        rotationFilter (pyfbsdk.FBRotationFilter): rotation filter to use
        useConstantKeyReducer (boolean): use the constant key reducer
        constantKeyReducerKeepOnKey(boolean): keep at least one key when reducing keyframes
        translationRootOnly (boolean): only plot translation animation on the the root
        plotLockedProperties (bool): Should we plot locked properties
        frameRate (mobu.FBTimeMode): Frame rate to override if needed, otherwise use preferred

    Return:
        mobu.FBPlotOptions
    """
    plotOptions = mobu.FBPlotOptions()
    if frameRate is not None:
        plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, frameStep, 0, frameRate)

    plotOptions.UseConstantKeyReducer = useConstantKeyReducer
    plotOptions.ConstantKeyReducerKeepOneKey = constantKeyReducerKeepOnKey
    plotOptions.PlotAllTakes = allTakes
    plotOptions.PlotOnFrame = onFrame
    plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, frameStep)
    plotOptions.PlotTranslationOnRootOnly = translationOnRootOnly
    plotOptions.RotationFilterToApply = rotationFilter
    plotOptions.PlotLockedProperties = plotLockedProperties

    return plotOptions


def PlotObjects(
    components,
    children=True,
    frameRange=(None, None),
    frameStep=1,
    allTakes=False,
    onFrame=True,
    rotationFilter=mobu.FBRotationFilter.kFBRotationFilterGimbleKiller,
    useConstantKeyReducer=True,
    constantKeyReducerKeepOnKey=True,
    translationOnRootOnly=False,
    onlyAnimatedObjects=False,
    plotToAllTakes=True,
    plotToBaseLayer=False,
    plotLockedProperties=False,
):
    """
    Plots the given objects in the scene. This is done by selecting them, plot using the PlotToSelected function and
    returning the selected objects to their original selected state.

    Arguments:
        components (list): list of animatable components
        children (boolean): plot the children of the components as well
        frameRange (list): start and end frame to plot animation in. If the contents of the list are None, then the
                            current start/end frames are used instead
        frameStep (int): set keys every x number of frames
        allTakes (bool): plot all takes
        onFrame (bool): set keys on whole number frames
        rotationFilter (pyfbsdk.FBRotationFilter.Enum): which rotation filter to use
        useConstantKeyReducer (boolean): removes keys that follow each other and are of the same value
        constantKeyReducerKeepOnKey (boolean): keeps at least one key
        translationOnRootOnly (boolean): only plot translation on the root object
        onlyAnimatedObjects (boolean): filters the list of given components to only plot those that have keys
        plotToAllTakes (boolean): plots the current animation to all the takes
        plotLockedProperties (bool): Should we plot locked properties

    """
    # Set focus to properties we want to plot
    if onlyAnimatedObjects:
        plotObjects = GetAnimatableComponents(
            components, setFocus=False, allProperties=True, children=children, selectComponents=True
        )
    else:
        plotObjects = {}
        for component in components:
            plotObjects[component] = component.Selected
            component.Selected = True
    # Change the frameRange to the values of the time slider if frameRange values aren't None
    if frameRange is not (None, None):
        SetTimeRange(*frameRange)

    # Plot Options

    currentTake = Globals.System.CurrentTake
    if allTakes and not plotToAllTakes:
        currentTake.PlotAllTakesOnSelected(mobu.FBTime(0, 0, 0, frameStep))
    else:
        plotOptions = PlotOptions(
            frameStep,
            allTakes and plotToAllTakes,
            onFrame,
            rotationFilter,
            useConstantKeyReducer,
            constantKeyReducerKeepOnKey,
            translationOnRootOnly,
            plotLockedProperties=plotLockedProperties,
        )
        currentTake.PlotTakeOnSelected(plotOptions)

    if plotToBaseLayer:
        FlattenLayers(Globals.System.CurrentTake, deleteLayers=False, selection=True)
    for component, selected in plotObjects.iteritems():
        if component.Selected != selected:
            component.Selected = selected


def PlotProperties(
    components,
    properties=("Lcl Translation", "Lcl Rotation"),
    children=True,
    frameRange=(None, None),
    frameStep=1,
    allTakes=False,
    onFrame=True,
    rotationFilter=mobu.FBRotationFilter.kFBRotationFilterGimbleKiller,
    useConstantKeyReducer=True,
    constantKeyReducerKeepOneKey=True,
    translationOnRootOnly=False,
    maxDepth=None,
    depth=0,
    allProperties=False,
    onlyAnimatedProperties=False,
    plotToAllTakes=True,
):
    """
    Plots animation for the properties of the components passed based on the time range provided using the
    PlotTakeOnSelectedProperties method provided by motion builder.

    Arguments:
            components (list[mobu.FBModel, etc]): list of FBModels to plot animation to
            properties (list[string, etc.]): any property that you want to have keyed.
            frameRange (list[int, int]): frame range to set keys on, defaults to the range of the time slider if no
                                         values are provided.
            frameStep (int): frame step number, only accepts integers higher than one
            children (bool): key properties of all children components as well
            frameStep (int): step to key by
            allTakes (bool): plot all takes
            onFrame (bool): plot on frame and not between frames
            rotationFilter (mobu.FBRotationFilter): rotation filter to use
            useConstantKeyReducer (bool): use the constant key reducer
            constantKeyReducerKeepOneKey(bool): keep at least one key when reducing keyframes
            translationRootOnly (bool): only plot translation animation on the the root
            maxDepth (int): max child depth to plot too
            depth (int): recursion depth
            allProperties (bool): Ignores the properties parameter and plots to all properties of a component
            onlyAnimatedProperties (bool): Only key properties with animation keys
            plotToAllTakes (bool): plots the animation from the current take to all takes, the allTakes parameter
                                      needs to be set to True for this parameter to take effect.

    Return:
        list(mobu.FBProperty, etc.)
    """
    # Set focus to properties we want to plot

    plotProperties = GetAnimatableComponents(
        components,
        properties,
        setFocus=True,
        allProperties=allProperties,
        onlyAnimatedProperties=onlyAnimatedProperties,
        children=children,
        maxDepth=maxDepth,
        selectComponents=False,
    )

    # Change the frameRange to the values of the time slider if frameRange values aren't None
    if frameRange is not (None, None):
        SetTimeRange(*frameRange)
    # Plot Options

    # Set frame range for the take
    currentTake = Globals.System.CurrentTake
    if allTakes and not plotToAllTakes:
        currentTake.PlotAllTakesOnSelectedProperties(mobu.FBTime(0, 0, 0, frameStep))
    else:
        plotOptions = PlotOptions(
            frameStep,
            allTakes and plotToAllTakes,
            onFrame,
            rotationFilter,
            useConstantKeyReducer,
            constantKeyReducerKeepOneKey,
            translationOnRootOnly,
        )
        currentTake.PlotTakeOnSelectedProperties(plotOptions)
    [property.SetFocus(value) for property, value in plotProperties.iteritems() if property.IsFocused() != value]

    return properties


def PlotCharacters(
    characters,
    plotLocations=("rig",),
    muteStory=True,
    takes=None,
    progressBar=None,
    mergeLayers=False,
    deleteLayers=True,
):
    """
    Plots the animation from characters to the skeleton or control rig. This method contains extra logic for
    removing animation layers, muting StoryTracks, and plotting to multiple takes.

    Arguments:
        characters  (list[FBCharacters, etc]): list of characters to plot
        location (str): where the animation should be plotted to, ei. 'rig' for control rig or 'skeleton' for
                            skeleton.
        muteStory (bool): mute Story
        mergeLayers (bool): merge all the animation layers before plotting animation on the characters
        deleteLayers (bool): delete the animation layers where animation was merged from. This argument is only respected
                            when mergeLayers is set to True.
    """
    system = mobu.FBSystem()

    currentTake = system.CurrentTake
    if not takes:
        takes = [currentTake]

    allCharacters = system.Scene.Characters
    storyTrackDictionary = {
        allCharacters[track.CharacterIndex - 1]: track
        for track in mobu.FBStory().RootFolder.Tracks
        if track.CharacterIndex
    }
    for take in takes:
        system.CurrentTake = take
        if mergeLayers:
            baseLayer = take.GetLayerByName("BaseAnimation")
            take.SetCurrentLayer(baseLayer.GetLayerIndex())
            FlattenLayers(take)
        for character in characters:

            # If a story track is unmuted, we force the control rig to be active
            # this is so the animation gets plotted correctly to the base skeleton

            ForceRigActive = (
                storyTrackDictionary.get(character, False)
                and not storyTrackDictionary[character].Mute
                and character.InputType is InputTypes.Rig
            )

            # store the active input state, since it will be changed after the plot (to either Rig/Skeleton)
            isInputActive = character.ActiveInput

            for location in plotLocations:
                # if there is no active input, no point on trying to plot to skeleton.
                if location == "skeleton" and not character.ActiveInput:
                    continue
                # We check if the control rig exists again because it will be created during the loop if it was missing
                CreateControlRig = not HasControlRig(character) and location == "rig"
                # Plot animation
                Plot(character, location, CreateControlRig=CreateControlRig, ForceInputActive=ForceRigActive)

                # Mute Story
                if (muteStory or character.InputType is InputTypes.Rig) and storyTrackDictionary.get(character, None):
                    storyTrackDictionary[character].Mute = True
            # reset the active input, since it might be set to a different value from what it used to be after the plot.
            character.ActiveInput = isInputActive
    for character in characters:
        if plotLocations[-1] == "skeleton":
            character.ActiveInput = False
        elif plotLocations[-1] == "rig":
            character.ActiveInput = True
    system.CurrentTake = currentTake


def PlotCustomComponents(
    plotFrom,
    plotTo,
    swapConstraints=None,
    children=True,
    frameRange=(None, None),
    frameStep=1,
    allTakes=False,
    onFrame=True,
    rotationFilter=mobu.FBRotationFilter.kFBRotationFilterGimbleKiller,
    useConstantKeyReducer=True,
    constantKeyReducerKeepOneKey=True,
    translationOnRootOnly=False,
    onlyAnimatedObjects=False,
    plotToAllTakes=True,
    plotToBaseLayer=False,
    plotLockedProperties=False,
    evaluateDeformation=False,
    plotAuxEffectors=True,
):
    """
    Plot animation to/from a skeleton to the given a list of bones and markers
    Can swap parent/child relationships on constraints to make sure animation is plotted correctly

    swapConstraints is for plotting from skeletons to controllers, since PlotObjects only works
    when plotting from parent to child, SwapParentChildRelationship is called at the beginning and end of the method
    to make the bones parents of the controllers, then restore them as children

    TODO: Figure out why PlotCharacters isn't working for this

    Arguments:
        plotFrom (list of mobu.FBModel objects): List of markers or bones to plot
        plotTo (list of mobu.FBModel objects): List of target markers or bones to plot to
        swapConstraints (list of mobu.FBConstraint objects): List of constraints on which to swap parent/child relations
        children (bool): plot the children of the components as well
        frameRange (list): start and end frame to plot animation in. If the contents of the list are None, then the
                            current start/end frames are used instead
        frameStep (int): set keys every x number of frames
        allTakes (bool): plot all takes
        onFrame (bool): set keys on whole number frames
        rotationFilter (mobu.FBRotationFilter.Enum): which rotation filter to use
        useConstantKeyReducer (bool): removes keys that follow each other and are of the same value
        constantKeyReducerKeepOneKey (bool): keeps at least one key
        translationOnRootOnly (bool): only plot translation on the root object
        onlyAnimatedObjects (bool): filters the list of given components to only plot those that have keys
        plotToAllTakes (bool): plots the current animation to all the takes
        plotLockedProperties (bool): Should we plot locked properties
        evaluateDeformation (bool): Should we evaluate deformation
        plotAuxEffectors (bool): Should we plot aux effectors
    """

    if swapConstraints:
        SwapParentChildInConstraints(swapConstraints)

    # Deselect objects in scene
    for select in Scene.GetSelection(mobu.FBModel):
        select.Selected = False

    # Select target objects
    for tgtObject in plotTo:
        tgtObject.Selected = True

    # Plot to selected
    PlotObjects(
        plotFrom,
        plotExtensions=True,
        children=children,
        frameRange=frameRange,
        frameStep=frameStep,
        allTakes=allTakes,
        onFrame=onFrame,
        rotationFilter=rotationFilter,
        useConstantKeyReducer=useConstantKeyReducer,
        constantKeyReducerKeepOneKey=constantKeyReducerKeepOneKey,
        translationOnRootOnly=translationOnRootOnly,
        onlyAnimatedObjects=onlyAnimatedObjects,
        plotToAllTakes=plotToAllTakes,
        plotToBaseLayer=plotToBaseLayer,
        plotLockedProperties=plotLockedProperties,
        evaluateDeformation=evaluateDeformation,
        plotAuxEffectors=plotAuxEffectors,
    )

    if swapConstraints:
        SwapParentChildInConstraints(swapConstraints)


def SwapParentChildInConstraints(constraints):
    """
    Swap the parent/child relationship in a list of constraints

    Arguments:
        constraints (list of mobu.FBConstraint objects): Constraints that shall have parent/child relations swapped
    """
    for con in constraints:
        child = con.ReferenceGet(0)
        parent = con.ReferenceGet(1)
        Constraint.SwapTargetModel(con, child, parent)
        Constraint.SwapSourceModel(con, parent, child)


def PlotOnFrame(component, properties=("Translation", "Rotation"), frame=-100, children=True, inverse=False):
    """
    Sets a key with values from the current frame on the properties provided to the function.
    If recursive is set true, keys are set on all the children components as well.

    Arguments:
        component (mobu.FBModel): Any object that is considered a model by MB
                            (mobu.FBModel(), mobu.FBModelNull, mobu.FBLight, etc.)
        properties (list[string, etc.]): any property that you want to have keyed.
        frame (int): frame number to create keys on
        children (bool): key properties of all children components as well
        inverse (bool): inverts the values of the properties
    Return:
        list(mobu.FBComponents, etc.)
    """
    if isinstance(component, basestring):
        component = mobu.FBFindModelByLabelName(component)

    if not component:
        return

    inverse = -1 if inverse else 1
    for eachProperty in properties:
        property = getattr(component, eachProperty, component.PropertyList.Find(eachProperty))

        # Check to make sure we get the FBProperty instance of the property
        # Some properties return just the value, not the FBProperty instance
        # PropertyList.Find returns the FBProperty instance

        if not isinstance(property, mobu.FBProperty):
            property = component.PropertyList.Find(eachProperty)

        if property is None or not property.IsAnimatable():
            continue

        value = property.Data
        property.SetAnimated(True)

        animationNode = property.GetAnimationNode()

        keyableNodes = animationNode.Nodes

        # Some animation nodes have sub nodes that you can key
        # If the animation node does not have keyable subnodes
        # key the animation node we already have.

        if not len(keyableNodes):
            keyableNodes = [animationNode]
            value = [value]

        # We just need the index values of xyz

        for index, keyableNode in enumerate(keyableNodes):
            keyableNode.KeyAdd(mobu.FBTime(0, 0, 0, frame), float(value[index]) * inverse)

    keyedComponents = [component]
    if children:
        for eachChild in component.Children:
            keyedComponents.extend(PlotOnFrame(eachChild, properties, frame, children))

    return keyedComponents


def PlotTimeRange(
    components, properties=("Translation", "Rotation"), frameRange=(None, None), frameStep=1, children=True
):
    """
    Plots animation for the properties of the components passed based on the time range provided using
    RS.Core.Animation.Lib's PlotOnFrame method.

    Arguments:
            components (list[mobu.FBModel, etc]): list of FBModels to plot animation to
            properties (list[string, etc.]): any property that you want to have keyed.
            startFrame (list[int, int]): frame range to set keys on, defaults to the range of the time slider if no
                                         values are provided.
            frameStep (int): frame step number, only accepts integers higher than one
            children (bool): key properties of all children components as well
    """

    frameRange = list(frameRange)

    # Change the frameRange to the values of the time slider if frameRange values are None
    LocalTime = Globals.System.CurrentTake.LocalTimeSpan
    # LocalTime = mobu.FBSystem().LocalTime
    for index, frame in enumerate(frameRange):
        # Zero is considered false but it is a valid frame value, so we check if the value is None instead
        if frame is None:
            frameRange[index] = getattr(LocalTime, ["GetStart", "GetStop"][index])().GetFrame()

    startFrame, endFrame = frameRange
    currentFrame = Globals.System.LocalTime.GetFrame()

    if frameStep < 1:
        frameStep = 1

    # Set keys in each time frame
    PlayerControl = Globals.Player
    for frame in xrange(startFrame - 1, endFrame, frameStep):
        frame += 1
        PlayerControl.Goto(mobu.FBTime(0, 0, 0, frame))
        skipComponents = []
        for component in components:
            if component not in skipComponents:
                skipComponents.extend(PlotOnFrame(component, properties=properties, frame=frame, children=children))
    PlayerControl.Goto(mobu.FBTime(0, 0, 0, currentFrame))


def SetTimeRange(start=None, end=None):
    """
    Sets the time range of the time slider/ player

    Arguments:
        start (int): start frame
        end (int): end frame
    """
    currentTake = Globals.System.CurrentTake
    localTime = currentTake.LocalTimeSpan
    ranges = []
    for index, frame in enumerate((start, end)):
        # Zero is considered false but it is a valid frame value, so we check if the value is None instead
        if frame is None:
            frame = getattr(localTime, ("GetStart", "GetStop")[index])()
        else:
            frame = mobu.FBTime(0, 0, 0, frame)
        ranges.append(frame)
    localTime.Set(*ranges)


def GetTimeRange(start=None, end=None):
    """
    Sets the time range of the time slider/ player

    Arguments:
        start (int): start frame
        end (int): end frame
    """
    currentTake = Globals.System.CurrentTake
    localTime = currentTake.LocalTimeSpan
    ranges = []
    for index, frame in enumerate((start, end)):
        # Zero is considered false but it is a valid frame value, so we check if the value is None instead
        if frame is None:
            frame = getattr(localTime, ("GetStart", "GetStop")[index])()
        else:
            frame = mobu.FBTime(0, 0, 0, frame)
        ranges.append(frame)
    localTime.Set(*ranges)


def GetAnimatableComponents(
    components,
    properties=("Lcl Translation",),
    setFocus=False,
    allProperties=False,
    onlyAnimatedProperties=True,
    children=False,
    depth=0,
    maxDepth=None,
    selectComponents=True,
):
    """
      Filters the give list of components to only include animatable components and selects them as well.

      Arguments:
          components (list): list of FBComponents
          properties (list): list of property names to check to see if they are animated
          setFocus (bool): instead of selecting animatable components, it selects the animatable properties of the components
          allProperties (bool): ignore the list of property names provided and check all the properties available on
                                   component instead
          onlyAnimatedProperties (bool): only selects components and properties that already have animation on them
          children (bool): check child components
          depth (int): current recursion depth
          maxDepth (int): max depth to recurse

      Return:
    list
    """
    plotObjects = {}
    for component in components:

        if not isinstance(component, mobu.FBModel):
            continue

        fbproperties = component.PropertyList
        if not allProperties:
            fbproperties = (component.PropertyList.Find(property) for property in properties)

        for fbproperty in fbproperties:
            if fbproperty is not None and fbproperty.IsAnimatable():
                if not fbproperty.IsAnimated() and onlyAnimatedProperties:
                    continue
                fbproperty.SetAnimated(True)
                animationNode = fbproperty.GetAnimationNode()
                keys = [animationNode.KeyCount > 1]

                if hasattr(animationNode, "Nodes"):
                    keys = [node.KeyCount > 1 for node in animationNode.Nodes]

                if onlyAnimatedProperties and not any(keys):
                    continue

                if selectComponents and component not in plotObjects:
                    plotObjects[component] = component.Selected
                    component.Selected = True

                if setFocus:
                    plotObjects[fbproperty] = fbproperty.IsFocused()
                    fbproperty.SetFocus(True)
                else:
                    break

        if children and depth != maxDepth:
            plotObjects.update(
                GetAnimatableComponents(
                    component.Children,
                    properties,
                    setFocus,
                    allProperties,
                    onlyAnimatedProperties,
                    children,
                    depth + 1,
                    maxDepth,
                )
            )
    return plotObjects


def FramesWithKeys(component, properties=None):
    """
    The frames where this component has keys

    Arguments:
        component (mobu.FBModel): component with keys
        properties (list): list of the names of the properties to check for keys

    Return:
        list
    """
    frames = []

    if not properties:
        properties = component.PropertyList
    else:
        properties = (component.PropertyList.Find(name) for name in properties)

    for eachProperty in properties:

        if eachProperty is None or not eachProperty.IsAnimatable() or not eachProperty.IsAnimated():
            continue

        animationNode = eachProperty.GetAnimationNode()
        nodes = [animationNode]
        if animationNode.Nodes and not animationNode.FCurve:
            nodes = animationNode.Nodes

        for node in nodes:
            for key in node.FCurve.Keys:
                frame = key.Time.GetFrame()
                if frame not in frames:
                    frames.append(frame)
    frames.sort()
    return frames


def MatchPosition(source, destinations):
    """
    Matches the position and orientation of the destination components to match that of the source component

    Arguments:
        source (mobu.FBComponent): component to get the position and orientation from
        destinations (list[mobu.FBComponent, etc.]): components whose positions and orientations should be set

    """
    Globals.Scene.Evaluate()
    matrix = mobu.FBMatrix()
    source.GetMatrix(matrix)
    for destination in destinations:
        destination.SetMatrix(matrix)
        Globals.Scene.Evaluate()


def TransferMotion(child, parent, optimize=True):
    """
    Transfer translation and rotation from the child to the parent.

    Arguments:
        parent (mobu.FBComponent): parent component
        child (mobu.FBComponent): child component
    """
    # child is always first
    deleteConstraint = Constraint.Constraint(
        Constraint.PARENT_CHILD, "DELETE_ME", child, parent, Active=True, Lock=True
    )
    # Scene Evaluate is needed here to make sure that the attributes that have been set actually work
    Globals.Scene.Evaluate()

    if optimize:
        PlotProperties([child], children=False)
    else:
        PlotTimeRange([child], children=False)
    deleteConstraint.FBDelete()


# --- Rig Utility Methods ---


def GetBonesFromCharacter(character):
    """
    skeleton components associated with the FBCharacter

    Arguments:
        character (mobu.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    return filter(
        None,
        [
            character.GetModel(getattr(mobu.FBBodyNodeId, bodyNodeIdName, None))
            for bodyNodeIdName in dir(mobu.FBBodyNodeId)
            if "kFB" in bodyNodeIdName
        ],
    )


def GetControlsFromCharacter(character):
    """
    controls for the Control Rig associated with the FBCharacter

    Arguments:
        character (pyfbsdk.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    return GetFKControlsFromCharacter(character) + GetIKControlsFromCharacter(character)


def GetExtensionControlsFromCharacter(character):
    """
    Get character extension controls for the given FBCharacter

    Arguments:
        character (mobu.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    extensions = character.CharacterExtensions
    allMarkers = []
    if extensions:
        for ext in extensions:
            allMarkers.extend([marker for marker in ext.Components])
    return allMarkers


def GetFKControlsFromCharacter(character):
    """
    fk controls for the Control Rig associated with theFBCharacter

    Arguments:
        character (mobu.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    ActivateControlRig(character, character.Active)
    ControlSet = character.GetCurrentControlSet()
    toReturn = []

    # Some nodes have been causing motionbuilder to crash immediately as soon as we look for them. Avoid these.
    toExclude = ["kFBRightElbowRollNode5Id"]
    nameFilter = re.compile("Translation|Invalid|Last")

    for bodyNodeIdName in dir(mobu.FBBodyNodeId):
        if "kFB" in bodyNodeIdName and not nameFilter.search(bodyNodeIdName) and bodyNodeIdName not in toExclude:
            a = getattr(mobu.FBBodyNodeId, bodyNodeIdName, None)
            b = ControlSet.GetFKModel(a)
            if b:
                toReturn.append(b)
    return toReturn


def GetIKControlsFromCharacter(character):
    """
    ik controls for the Control Rig associated with the FBCharacter

    Arguments:
        character (mobu.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    ActivateControlRig(character, character.Active)
    ControlSet = character.GetCurrentControlSet()
    nameFilter = re.compile("Translation|Invalid|Last")
    return filter(
        None,
        [
            ControlSet.GetIKEffectorModel(getattr(mobu.FBEffectorId, effectorNodeIdName, None), 0)
            for effectorNodeIdName in dir(mobu.FBEffectorId)
            if "kFB" in effectorNodeIdName and not nameFilter.search(effectorNodeIdName)
        ],
    )


def GetAuxEffectorsFromCharacter(character):
    """
    Aux effectors Control Rig associated with the FBCharacter

    Args:
        character (mobu.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    effectors = []

    for setId in mobu.FBEffectorSetID.values.values():
        if setId == mobu.FBEffectorSetID.FBEffectorSetDefault:
            continue

        for effId in mobu.FBEffectorId.values.values():
            auxEffector = character.GetEffectorModel(effId, setId)
            if auxEffector:
                effectors.append(auxEffector)
    return effectors


def GetControlRigReferenceNull(character):
    """
    Gets the Reference Null that the control rig is parented under.

    Arguments:
        character (mobu.FBCharacter): character whose control rig reference null you want to get

    Return:
       FBModelNull
    """
    if not HasControlRig(character):
        ActivateControlRig(character)
    controlSet = character.GetCurrentControlSet()
    if controlSet is not None:
        return controlSet.GetIKEffectorModel(mobu.FBEffectorId.kFBHipsEffectorId, 0).Parent


def GetCharacterFromControlRigReferenceNull(modelnull):
    """
    Gets the FBCharacter that the reference null belongs to

    Arguments:
         modelnull (mobu.FBModelNull): control rig reference null whose fbcharacter you want to get

    Return:
       mobu.FBCharacter
    """
    for character in Globals.Characters:
        controlSet = character.GetCurrentControlSet()
        if controlSet and controlSet.GetIKEffectorModel(mobu.FBEffectorId.kFBHipsEffectorId, 0).Parent == modelnull:
            return character


def HasKeyframes(component, properties=None):
    """Checks for any keyframes on component

    Args:
        component (mobu.FBModel): component with keys
        properties (list, optional): list of the names of the properties to check for keys

    Return:
        bool: True or False if the component has keyframes
    """
    if not properties:
        properties = component.PropertyList
    else:
        properties = (component.PropertyList.Find(name) for name in properties)

    for eachProperty in properties:

        if not eachProperty:
            continue

        if eachProperty.IsAnimatable() and eachProperty.IsAnimated():
            animNode = eachProperty.GetAnimationNode()
            if animNode and animNode.KeyCount:
                return True

    return False


def HasControlRig(character):
    """
    Does a character have a control rig

    Arguments:
        character (mobu.FBCharacter): Character to check if it has a control rig

    Return:
        Boolean
    """
    hasControlRig = character.GetCurrentControlSet() is not None

    # GetCurrentControlSet will only work if the input is a control rig
    # So if the input type is something else, like Stance, we need to at attempt to switch it to control rig to get the
    # right value. If the character has not had a control rig generated, then switching to the control rig will just
    # fail
    if character.InputType is not InputTypes.Rig:

        previousInput = character.InputType
        isActive = character.ActiveInput

        character.InputType = InputTypes.Rig
        character.ActiveInput = True

        hasControlRig = character.GetCurrentControlSet() is not None

        character.InputType = previousInput
        character.ActiveInput = isActive

    return hasControlRig


def ActivateControlRig(character, active=True, includeNamespace=False):
    """
    Activates or deactivates the Control Rig for selected characters in the scene
    Argument:
        character (mobu.FBCharacter): the character who needs their control rig activated
        active (bool): set control rig to active
        includeNamespace (bool): adds a namespace to the control rig if it doesn't have one
    """

    if not HasControlRig(character):
        # CreateControlRig accepts 0 and 1 as values, represent Ik and FK/IK respectively
        character.CreateControlRig(1)

    character.InputType = InputTypes.Rig
    character.Active = active
    controlSet = character.GetCurrentControlSet()

    if includeNamespace and controlSet is not None:
        effector = controlSet.GetIKEffectorModel(mobu.FBEffectorId.kFBHipsEffectorId, 0)
        if effector is not None and effector.Parent is not None:

            namespaceSections = effector.Parent.LongName.split(":")
            namespaceSections[-1] = controlSet.Name
            namespace = ":".join(namespaceSections)

            if controlSet.LongName != namespace:
                controlSet.LongName = namespace


def SetControlRigVisibility(character, visible=True, active=True):
    """
    Activates or deactivates the Control Rig for selected characters in the scene
    Argument:
        character (mobu.FBCharacter): the character who needs their control rig visibility set
        visible (bool): set control rig visibility
    """
    if not HasControlRig(character):
        ActivateControlRig(character, active=active)

    [
        setattr(control, property, visible)
        for control in GetControlsFromCharacter(character)
        for property in ("Show", "Visibility")
    ]
    [
        setattr(bone, property, bool(visible - 1))
        for bone in GetBonesFromCharacter(character)
        for property in ("Show", "Visibility")
        if "SKEL_ROOT" not in bone.Name
    ]


# Old code that could possibly be removed
def ControlRigToSkeletonPlot(character):
    """
    Plots animation on the control rig to the skeleton

    Arguments:
        character (mobu.FBCharacter): character to plot
    """
    EnableRotationDOF(character, "SKEL_L_UpperArm")
    EnableRotationDOF(character, "SKEL_R_UpperArm")
    PlotCharacters(character, plotLocations=["skeleton"])
    DeleteControlRig(character)


def DeleteControlRig(character):
    """
    Delete Control Rig

    Arguments:
        character (mobu.FBCharacter): character with a control rig to delete
    """

    if HasControlRig(character):
        character.InputType = InputTypes.Rig
        character.GetCurrentControlSet().FBDelete()


def AddPoseToPoseControl(poseName, pCharacter=None):
    """Adds the pose to the Pose Manager

    Args:
        poseName (str): name for the pose
        pCharacter (mobu.FBCharacter): character whose pose we want to store

    Returns:
        FBPose
    """
    if not pCharacter:
        pCharacter = GetCurrentCharacter()
    # Create Character Pose
    pose = mobu.FBCharacterPose(poseName)
    pose.CopyPose(pCharacter)
    return pose


def AddPoseObjectToPoseControl(poseName, components=None):
    """Adds the pose to the Pose Manager

    Args:
        poseName (str): name for the pose
        components (mobu.FBComponent): component whose pose we want to store

    Return:
        mobu.FBPose: Pose
    """
    if components is None:
        sceneList = mobu.FBModelList()
        mobu.FBGetSelectedModels(sceneList, None, True, True)

        components = [sceneList.GetModel(idx) for idx in xrange(sceneList.count())]
        if not components:
            return

    # Create object Pose
    pose = mobu.FBObjectPose(poseName)
    for component in components:
        pose.CopyObjectPose(component.Name, component)

    return pose


def ImportPoses(path):
    """
    Imports the poses from the given fbx

    Arguments:
        path (str): path to fbx
    """
    # Merge options
    # You need to pass in the path to the file you want to load so you can disable the loading of takes
    options = mobu.FBFbxOptions(True, path)

    # Using SetAll doesn't set all the properties to False & Discard, causing the file merge to change the
    # presentation of the viewport
    for attr in dir(options):
        if attr.startswith("_"):
            continue
        value = getattr(options, attr)
        if isinstance(value, mobu.FBElementAction):
            setattr(options, attr, mobu.FBElementAction.kFBElementActionDiscard)
        elif isinstance(value, bool):
            setattr(options, attr, False)

    options.Poses = mobu.FBElementAction.kFBElementActionMerge
    options.ViewingMode = mobu.FBVideoRenderViewingMode().FBViewingModeXRay
    options.TakeSpan = mobu.FBTakeSpanOnLoad.kFBLeaveAsIs

    # deselect takes so they aren't merged into the file
    for index in xrange(options.GetTakeCount()):
        options.SetTakeSelect(index, False)

    Globals.Application.FileMerge(path, False, options)

    # Add logic to iterate and add poses


def ExportAllCharacterAnimations(path, AllTakes=True, Character=False, ControlSet=True, CharacterExtensions=False):
    """
    Export Characters Animations on takes as FBX files

    Arguments:
        path (str): directory to export tracks to
        AllTakes (bool): export all takes in the scene
        Character (bool): Save characters out to the FBX File
        ControlSet (bool): Save control sets to the fbx file
        CharacterExtension (bool): Save character extensions to the FBX file

    """
    application = mobu.FBApplication()

    options = mobu.FBFbxOptions()
    options.SaveCharacter = Character
    options.SaveControlSet = ControlSet
    options.SaveCharacterExtensions = CharacterExtensions

    # Get All takes + the current take
    takes = mobu.FBSystem().CurrentTake
    if AllTakes:
        takes = mobu.FBSystem().Scene.Takes

    filename = Scene.GetSceneName()

    if not path.endswith(filename):
        path = os.path.join(path, filename)

    if not os.path.exists(path):
        os.mkdir(path)

    for take in takes:
        takedirectory = os.path.join(path, take.Name)

        if not os.path.exists(takedirectory):
            os.mkdir(takedirectory)

        for character in mobu.FBSystem().Scene.Characters:
            application.SaveCharacterRigAndAnimation(
                os.path.join(takedirectory, "{}.fbx".format(character.Name)), character, options
            )


# --- Anim2Fbx Utility Method ---


def PrepareMover(namespace, properties=("RotationActive", "TranslationActive", "Enable Translation DOF")):
    """
    Prepares Mover, this method can be deprecated\
    Arguments:
        namespace (str): namespace for mover
        properties [list(string, etc.)]: the name of attributes you want to set to False
    """
    lMover = mobu.FBFindModelByLabelName("{}:{}".format(namespace, "mover"))

    if lMover:
        [setattr(lMover.PropertyList.Find(each_property), "Data", False) for each_property in properties]


def GetControlRigXMLPath(character, sync=False):
    """
    Searches for, syncs and returns the controlrig.xml file related to the character passed as the
    arg.

    Args:
        FBCharacter

    Returns:
        String: file path to the character's controlrig.xml
    """
    directory = GetControlRigXMLDirectory(character, sync)
    if directory and os.path.exists(os.path.join(directory, "controlrig.xml")):
        return os.path.join(directory, "controlrig.xml")
    return ""


def GetControlRigXMLDirectory(character, sync=False):
    """
    Searches for, syncs and returns the controlrig.xml file related to the character passed as the
    arg.

    Args:
        FBCharacter

    Returns:
        String: file path to the character's controlrig.xml
    """
    rootDirectory = os.path.join(Config.Tool.Path.Root, "etc", "config", "characters", "skeleton")

    if sync:
        Perforce.Sync(["-f", os.path.join(rootDirectory, "...")])

    controlRigXmlDictionary = {
        os.path.basename(folder): os.path.join(localpath, folder)
        for localpath, folders, files in os.walk(rootDirectory)
        for folder in folders
    }

    filename = os.path.splitext(os.path.basename(Globals.Application.FBXFileName))[0]
    for name in (character.Name, filename):
        directory = controlRigXmlDictionary.get(name, "")
        if directory and os.path.exists(directory):
            return directory
    return ""


def FlattenLayers(take, deleteLayers=True, selection=False):
    """
    Merges all animations to the base animation layer and deletes all other layers

    Arguments:
        take (FBTake): take to flatten layers on
        deleteLayers(bool): delete layers that are being merged.
                               The default base animation layer will not be deleted.
    """
    whatToPlot = mobu.FBAnimationLayerMergeOptions.kFBAnimLayerMerge_AllLayers_CompleteScene
    if selection:
        whatToPlot = mobu.FBAnimationLayerMergeOptions.kFBAnimLayerMerge_AllLayers_SelectedProperties
    take.MergeLayers(whatToPlot, deleteLayers, mobu.FBMergeLayerMode.kFBMergeLayerModeAutomatic, True)


def RemoveAnimationLayersFromTakes(takes=[]):
    """
    Removes all the animation layers except the base layer from the given takes

    Arguments:
        takes (list[FBTake]): list of takes to clear animation layers from

    """
    for take in takes:
        # Start deleting layers starting with the highest index number to take into account that the index of a
        # layer dynamically changes
        for index in reversed(xrange(take.GetLayerCount())):
            if not index:
                continue
            layer = take.GetLayer(index)
            layer.FBDelete()


def SaveCharacterAnimationToFbx(path, character):
    """
    Saves out a character's animation to the given fbx file

    Arguments:
        path (str): fbx file to save animations to
        character (mobu.FBCharacter): character whose animation should be saved
    """

    options = mobu.FBFbxOptions(False)
    options.SaveCharacter = True
    options.SaveControlSet = False
    options.SaveCharacterExtensions = False

    Globals.Application.SaveCharacterRigAndAnimation(path, character, options)


def LoadCharacterAnimationFromFbx(path, character):
    """
    Loads out a character's animation from the given fbx file

    Arguments:
        path (str): fbx file to load animation from
        character (mobu.FBCharacter): character to apply animation to
    """
    options = mobu.FBFbxOptions(False)
    options.ReplaceControlSet = False
    options.CopyCharacterExtensions = False
    options.ProcessAnimationOnExtension = True
    options.IgnoreConflicts = True
    options.TransferMethod = mobu.FBCharacterLoadAnimationMethod.kFBCharacterLoadPlot
    options.ResetDOF = False
    options.ResetHierarchy = False
    options.RetargetOnBaseLayer = True
    options.RemoveConstraintReference = False

    plotOptions = mobu.FBPlotOptions()
    plotOptions.PlotAllTakes = False
    plotOptions.PlotOnFrame = True
    plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
    plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterGimbleKiller
    plotOptions.UseConstantKeyReducer = True
    plotOptions.ConstantKeyReducerKeepOneKey = True
    plotOptions.PlotTranslationOnRootOnly = True

    ActivateControlRig(character, True)
    Globals.Application.LoadAnimationOnCharacter(path, character, options, plotOptions)


def GetExportMetaFile(name):
    """
    Gets the export metafile based on the name of the rig

    Looks at meta ped files located in //{project}/assets/export/characters/metapeds/component_models

    Arguments:
        name (str): name of the rig

    Return:
        string or None
    """
    # Remove the letter A from rigs using the RigName_##A naming convention
    name = name.rstrip("a")

    exportMetaFileDirectory = os.path.join(
        Config.Project.Path.Assets, "export", "characters", "metapeds", "component_models", name[:2].lower()
    )
    Perforce.Sync(os.path.join(exportMetaFileDirectory, "..."))

    if not os.path.exists(exportMetaFileDirectory):
        return

    for directory in os.listdir(exportMetaFileDirectory):
        if not directory.startswith(name):
            continue
        for meshname in ("hand", "body", "uppr", "lowr", "head"):
            metaFilePath = os.path.join(exportMetaFileDirectory, directory, "{}.meta".format(directory))
            if meshname in directory and os.path.exists(metaFilePath):
                return metaFilePath


def GetExportMetaFileFromFbx(fbxPath):
    """
    Gets the export metafile from the given fbx

    Arguments:
        fbxPath (str): path to the fbx to check for a meta file

    Return:
        string or None
    """
    filename, _ = os.path.splitext(os.path.basename(fbxPath).lower())
    return GetExportMetaFile(filename)


def GetSkeletonPathsFromSkelset(skeletonSetPath):
    """
    Gets the bonelist path and skeleton paths from a skeleton set file

    skeleton sets are found in //{project}/assets/metadata/characters/skeleton/skeletonsets

    Arguments:
        skeletonSetPath (str): path to skeleton set

    Return:
        list(string, list)
    """
    tree = xml.parse(skeletonSetPath)
    root = tree.getroot()

    mainSkeletonElement = root.find("MainSkeleton")
    skeletonsElement = root.find("Skeletons")
    skeletonPaths = (
        [mainSkeletonElement.text.replace("$(assets)", Config.Project.Path.Assets)]
        if mainSkeletonElement is not None
        else []
    )
    if skeletonPaths:
        skeletonPaths += [element.text.replace("$(assets)", Config.Project.Path.Assets) for element in skeletonsElement]

    bonelistPath = (
        root.find("MasterBoneList").text.replace("$(assets)", Config.Project.Path.Assets)
        if root.find("MasterBoneList") is not None
        else []
    )

    return bonelistPath, skeletonPaths


def GetSkeletonPathsFromMetaFile(metaFilePath):
    """
    Gets the bonelist path and paths to the skeleton files from the given meta file

    Arguments:
        metaFilePath (str): path to the metafile

    Returns:
        string, string, list[string, etc.]
    """
    tree = xml.parse(metaFilePath)
    root = tree.getroot()

    bonelistPath = (
        root.find("MasterBoneList").text.replace("$(assets)", Config.Project.Path.Assets)
        if root.find("MasterBoneList") is not None
        else None
    )

    skeletonSetPath = (
        "{}.skelset".format(root.find("SkeletonSet").text.replace("$(assets)", Config.Project.Path.Assets))
        if root.find("SkeletonSet") is not None
        else None
    )
    skeletonPaths = []
    if skeletonSetPath:
        skeletonPaths, bonelistPath_ = GetSkeletonPathsFromSkelset(skeletonSetPath)
        bonelistPath = bonelistPath or bonelistPath_

    return bonelistPath, skeletonSetPath, skeletonPaths


def ConvertMetapedToSkeleton(skeletonPaths, boneListPath):
    """
    Converts the metaped files into a skel file

    Arguments:
        skeletonPaths (list): list of skeletons paths
        boneListPath (str): path to the bonelist file

    Return:
        string or None

    """
    # Get the metaped file
    if not isinstance(skeletonPaths, (tuple, list)):
        skeletonPaths = [skeletonPaths]

    skeletonPaths = [path for path in skeletonPaths if path]
    if not skeletonPaths:
        return None

    skeletonPath = skeletonPaths[0]
    executable = os.path.join(Config.Tool.Path.Bin, "anim", "skeletonmodularise.exe")
    temporaryDirectory = tempfile.mkdtemp()
    outputPath = os.path.join(temporaryDirectory, "skeleton.skel")

    commandList = [
        executable,
        "-inputFile",
        '"{}"'.format(skeletonPath),
        "-bonelist",
        '"{}"'.format(boneListPath),
        "-outputdir",
        '"{}"'.format(temporaryDirectory),
    ]

    if skeletonPaths[1:]:
        commandList += ["-additionalFiles", ",".join(['"{}"'.format(path) for path in skeletonPaths[1:]])]

    subprocess.call(commandList)

    fileExists = os.path.exists(outputPath)
    destinationPath = os.path.join(ProjectData.data.GetSkelFilesDirectory(), "skeleton.skel")
    if fileExists:
        shutil.move(outputPath, destinationPath)

    os.removedirs(temporaryDirectory)
    return destinationPath if fileExists else None


def GenerateSkeletonFromFbx(path=None):
    """
    Finds the latest meta file or zip file based on the path and name of the fbx file and generates/extract a skeleton
    file from the found meta/zip files.

    Arguments:
        path (str): path to the fbx file to generate a skel file from

    Return:
        string
    """
    filename, _ = os.path.splitext(os.path.basename(path or ""))
    GenerateSkeletonFromName(filename, path)


def GenerateSkeletonFromName(name=None, path=None):
    """
    Finds the latest meta file or zip file based on the path and name of the fbx file and generates/extract a skeleton
    file from the found meta/zip files.

    Arguments:
        name (str): name of the
        path (str): path to the fbx file to generate a skel file from

    Return:
        string
    """
    metafile = GetExportMetaFile(name or "")
    path = path or ""

    skeletonDirectory = ProjectData.data.GetSkelFilesDirectory()

    directoryName = "vehicles"

    # Animals and most biped characters use the new MetaPed system, this system can be used to figure out the correct
    # skeleton that was used at export time

    skeletonPath = None
    zipPath = None
    updated = False

    if metafile:
        bonelistPath, skeletonSetPath, metaSkelPaths = GetSkeletonPathsFromMetaFile(metafile)
        if "bipeds" not in metaSkelPaths[0]:
            skeletonPath, updated = GenerateSkelFromSkeletonSet(metafile, skeletonDirectory, skeletonSetPath)

        elif "female" in metaSkelPaths[0]:
            skeletonPath = os.path.join(skeletonDirectory, "human", "female.skel")

    else:
        # Biped characters have three prefixes if they are a ped.
        # Not all peds have matching meta files to go
        # Props, Weapons, & Vehicles skeletons are exported out to zip file and must be extracted from them
        if "cutscene" in path or "ingame" in path or re.match("[a-z]_[a-z]_[a-z]", name, re.I):
            skeletonPath = os.path.join(skeletonDirectory, "human", "biped.skel")
            if re.match("[a-z]_f_[a-z]", name, re.I):
                skeletonPath = os.path.join(skeletonDirectory, "human", "female.skel")

        elif "weapon" in path or name.startswith("w_"):
            directoryName = "weapons"
            zipPath = os.path.join(Config.Project.Path.Assets, "export", "models", "cdimages", "weapons.zip")

        elif "props" in path or re.match("p_|pg_|rdr_|s_|int_|v_|prop_", name, re.I):
            directoryName = "props"
            path = os.path.join(Config.Project.Path.Assets, "export", "levels", "rdr3", "props")
            # create a generator to get all available zip files within the props folder
            zipGenerator = (
                os.path.join(root, _file)
                for root, directories, files in os.walk(path)
                for _file in files
                if _file.endswith(".zip")
            )
            for zipPath in zipGenerator:
                hasSkel, _ = HasSkel(zipPath, name)
                if hasSkel:
                    break
                zipPath = None

        elif "vehicle" in path or name.endswith("x"):
            directoryName = "vehicles"
            zipPath = os.path.join(
                Config.Project.Path.Assets, "export", "levels", "rdr3", "vehicles_packed", "{}.zip".format(name)
            )

        if zipPath and os.path.isfile(zipPath):
            skeletonPath = os.path.join(ProjectData.data.GetSkelFilesDirectory(), directoryName, "{}.skel".format(name))
            skeletonFileState = Perforce.GetFileState(skeletonPath)
            if skeletonFileState:
                zipFileState = Perforce.GetFileState(zipPath)
                updated = zipFileState.HeadChangelistTime <= skeletonFileState.HeadChangelistTime
            else:
                updated = True

            if updated:
                Perforce.Edit(skeletonPath)
                ExtractSkelFromZip(zipPath, name, directoryName)
    return skeletonPath, updated


def HasSkel(zipPath, filename):
    """
    Does the zip file has the given skel

    Arguments:
        zipPath (str): path to the zipfile
        filename (str): name of the child zip file to extract the skeleton.skel from
    """

    with ZipUtils.ZipFile(zipPath, "r") as zipFile:
        for name in zipFile.namelist():
            if re.search(r"{}.(ift|idr).zip".format(filename), name, re.I):
                childZip = zipFile.getChildZip(name)
                if "skeleton.skel" in childZip.namelist():
                    return True, name
    return False, ""


def ExtractSkelFromZip(zipPath, filename, folder, childZip=None):
    """
    Extracts the skeleton file from the given zip and puts in the given folder

    Arguments:
        zipPath (str): path to the zipfile
        filename (str): name of the child zip file to extract the skeleton.skel from
        folder (str): the name of the folder to put the skeleton.skel under after being extracted
        childZip (str):
    """
    if childZip is None:
        _, childZip = HasSkel(zipPath, filename)

    if not childZip:
        return

    with ZipUtils.ZipFile(zipPath, "r") as zipFile:

        temporaryDirectory = tempfile.mkdtemp()
        skeletonPath = os.path.join(ProjectData.data.GetSkelFilesDirectory(), folder, "{}.skel".format(filename))
        temporaryPath = zipFile.getChildZip(childZip).extract("skeleton.skel", temporaryDirectory)
        if os.path.exists(temporaryPath):
            if not os.path.exists(skeletonPath) or not filecmp.cmp(skeletonPath, temporaryPath):
                shutil.move(temporaryPath, skeletonPath)
            shutil.rmtree(temporaryDirectory)
            return skeletonPath


def GetModelsFromIASFile(path):
    """
    Gets the debug model names from a ias file

    Argument:
        path (str): path to the ias file

    Return:
        generator
    """
    tree = xml.parse(path)
    root = tree.getroot()
    found = []
    for element in root.findall("entities/Item/debugModelName"):
        text = element.text.strip().lower()
        if text is not None and text not in found:
            found.append(text)
            yield text


def GetFragNameFromModelNames(names):
    """
    Get frag names from the model info files

    Arguments:
        names (list): list of models name that have info model info

    Return:
        generator
    """
    metaDirectory = os.path.join(Config.Project.Path.Assets, "export", "characters", "metapeds", "modelinfo")
    for filename in os.listdir(metaDirectory):
        name = filename.split(".")[0]
        if name.lower() in names:
            path = os.path.join(metaDirectory, filename)
            tree = xml.parse(path)
            root = tree.getroot()
            element = root.find("InitDatas/Item/FragName")
            if element.text:
                yield element.text


def GetSkeletonSetFromFragName(fragname):
    """
    Gets the skeleton set from meta files found in //{project}/assets/metadata/characters/description

    Arguments:
        fragname (str): frag name that corresponds to a file in the description directory

    """
    descriptionDirectory = os.path.join(Config.Project.Path.Assets, "metadata", "characters", "description")
    for name in os.listdir(descriptionDirectory):
        if name.lower().startswith(fragname.lower()):
            path = os.path.join(descriptionDirectory, name)
            tree = xml.parse(path)
            root = tree.getroot()
            return "{}.skelset".format(root.find("SkeletonSet").text.replace("$(assets)", Config.Project.Path.Assets))


def GenerateSkelFromSkeletonSet(metafile, skeletonDirectory, skeletonSetPath):
    """
    Creates a skel file based on the metafiles and skeletonset file provided at the directory provided

    Arguments:
        metafile (str): path to metafile
        skeletonDirectory (str): path to the directory to save skel file
        skeletonSetPath (str): path to the skelset to use for creating the skel file
    """
    bonelistPath, metaSkelPaths = GetSkeletonPathsFromSkelset(skeletonSetPath)
    filename = Strings.common(os.path.basename(metafile), os.path.basename(skeletonSetPath))[0].strip("_")

    if ".skel" not in filename:
        filename = "{}.skel".format(filename)

    # Check if any of the files that make up the metaped skeleton have been updated

    directoryName = "human"
    if "props" in metaSkelPaths[0]:
        directoryName = "props"
    elif "bipeds" not in metaSkelPaths[0]:
        directoryName = "animals"
    else:
        return os.path.join(skeletonDirectory, directoryName, "biped.skel"), False

    skeletonPath = os.path.join(skeletonDirectory, directoryName, filename)
    skeletonFileState = Perforce.GetFileState(skeletonPath)
    metaFilesUpdated = False
    if skeletonFileState:
        for path in [
            os.path.join(skeletonDirectory, directoryName, filename),
            bonelistPath,
            skeletonSetPath,
        ] + metaSkelPaths:
            # Store data in the database
            filestate = Perforce.GetFileState(path)

            # Check if the files have been updated since the last the skel file was updated
            if filestate and filestate.HeadChangelistTime <= skeletonFileState.HeadChangelistTime:
                continue

            metaFilesUpdated = True
            break
    else:
        metaFilesUpdated = True

    # generate new skeleton
    if metaFilesUpdated and metaSkelPaths:
        _skeletonPath = ConvertMetapedToSkeleton(metaSkelPaths, bonelistPath)
        if _skeletonPath is not None:
            if os.path.exists(skeletonPath):
                os.remove(skeletonPath)
            os.rename(_skeletonPath, skeletonPath)
        # Add perforce diff here once we move to our system so we can detect if a change has happened.
    return skeletonPath, metaFilesUpdated


def GenerateSkeletonsFromIAS(path):
    """
    Gets a list of skeletons from the given IAS file

    Arguments:
        path (str): path to the ias file

    Return:
        generator
    """
    skeletonDirectory = ProjectData.data.GetSkelFilesDirectory()
    for modelName in GetModelsFromIASFile(path):
        metafile = GetExportMetaFile(modelName)
        if metafile:
            for fragName in GetFragNameFromModelNames([modelName]):
                skeletonSetPath = GetSkeletonSetFromFragName(fragName)
                if skeletonSetPath and os.path.exists(skeletonSetPath):
                    yield modelName, GenerateSkelFromSkeletonSet(metafile, skeletonDirectory, skeletonSetPath)
        else:
            yield modelName, GenerateSkeletonFromName(modelName)


def autoTangent(selectedModels):
    """
    Change auto tangent to fcurve in selected model
    """
    for objectName in selectedModels:
        for Property in objectName.PropertyList:
            propAnimationNodeList = Scene.Property.GetAnimationNodeList(Property)
            for node in propAnimationNodeList:
                for fcurve in Scene.AnimationNode.GetSubAnimationNodeList(node):
                    for key in fcurve.FCurve.Keys:
                        key.Interpolation = mobu.FBInterpolation.kFBInterpolationCubic
                        key.TangentMode = mobu.FBTangentMode.kFBTangentModeClampProgressive


class Key(object):
    """
    Clone key value into a instance attribute.

    Args:
        curveKey (FBFCurveKey).

    """

    # Inspired from http://awforsythe.com/tutorials/pyfbsdk-7
    def __init__(self, curveKey=mobu.FBFCurveKey()):
        # Core values
        # In order to scale this value I use secondDouble type
        self.time = curveKey.Time.GetSecondDouble()
        self.value = curveKey.Value

        # Tangent values
        self.interpolation = curveKey.Interpolation
        self.tangentMode = curveKey.TangentMode
        self.constantMode = curveKey.TangentConstantMode
        self.leftDerivative = curveKey.LeftDerivative
        self.rightDerivative = curveKey.RightDerivative
        self.leftWeight = curveKey.LeftTangentWeight
        self.rightWeight = curveKey.RightTangentWeight
        self.tangentBreak = curveKey.TangentBreak
        self.tangentClampMode = curveKey.TangentClampMode

        # TCB values
        self.tension = curveKey.Tension
        self.continuity = curveKey.Continuity
        self.bias = curveKey.Bias


class AnimationCurve(object):
    """
    Store keys included in a timeRange for an animationNode from motion builder.
    """

    def __init__(self):
        self.model = ""
        self.attribute = ""
        self.animationNode = ""
        self.curveNode = ""
        self.timeRange = [0, 1]
        self.keyInTimeRange = []
        self.keys = []


class AnimationBoundingBox(object):
    """
    Store global bounding box data as simple struct/enum.
    """

    def __init__(self):
        self.boundingBox = {}
        self.curveTriangleStrips = []
        self.keyPositions = []


def tangentIsDefaultWeight(tangentWeight):
    """Checks if tangent is equal to default value of 1/3

    Args:
        tangentWeight (float): value to check

    Returns:
        bool: True or False if value is default
    """
    return tangentWeight > 0.3333 and tangentWeight < 0.3334


def getCurveData(fcurve):
    """Gets data from fcurve

    Returns:
        dict: fcurve data
    """
    keyDataList = []

    for key in fcurve.Keys:

        keyData = {
            "time": key.Time.Get(),
            "value": key.Value,
            "interpolation": int(key.Interpolation),
            "tangent-mode": int(key.TangentMode),
            "constant-mode": int(key.TangentConstantMode),
            "left-derivative": key.LeftDerivative,
            "right-derivative": key.RightDerivative,
            "left-weight": key.LeftTangentWeight,
            "right-weight": key.RightTangentWeight,
        }

        keyDataList.append(keyData)

    return keyDataList


def setCurveData(fcurve, keyDataList, frameOffset=0):
    """Sets curve data

    Args:
        fcurve (mobu.FBFCurve): fcurve to set data on
        keyDataList (list of dict): Key data ordered in time as list
        frameOffset (int): frame offset value
    """
    # clear curve first
    fcurve.EditClear()
    fcurve.EditBegin()

    # grab offset in frames
    fbOffset = mobu.FBTime(0, 0, 0, frameOffset, 0)

    # set keys
    fcurve.EditBegin()
    for keyData in keyDataList:

        # add key
        keyIndex = fcurve.KeyAdd(mobu.FBTime(keyData["time"]) + fbOffset, keyData["value"])
        key = fcurve.Keys[keyIndex]

        # set interp
        key.Interpolation = mobu.FBInterpolation.values[keyData["interpolation"]]

        # set tangent
        key.TangentMode = mobu.FBTangentMode.values[keyData["tangent-mode"]]

        # not using TCB mode just set to break
        if key.TangentMode == mobu.FBTangentMode.kFBTangentModeTCB:
            key.TangentMode = mobu.FBTangentMode.kFBTangentModeBreak

        # set tangent constant
        key.TangentConstantMode = mobu.FBTangentConstantMode.values[keyData["constant-mode"]]

    # set tangents
    for idx, keyData in enumerate(keyDataList):
        key = fcurve.Keys[idx]

        key.LeftDerivative = keyData["left-derivative"]
        key.RightDerivative = keyData["right-derivative"]

        # set tangent if needed to keep tangents from being unlocked
        if not tangentIsDefaultWeight(keyData["left-weight"]):
            key.LeftTangentWeight = keyData["left-weight"]

        if not tangentIsDefaultWeight(keyData["right-weight"]):
            key.RightTangentWeight = keyData["right-weight"]

    fcurve.EditEnd()


def getAnimData(
    component, properties=["Translation (Lcl)", "Rotation (Lcl)"], static=True, allProperties=False, allLayers=False
):
    """Gets all data on component based on properties

    Args:
        component (mobu.FBComponent): Component to get data on
        properties (list of str): Properties to get data on
        static (bool): Add static data in anim (non-keyed)
        allProperties (bool): Whether to gather all property datg or not
        allLayers (bool): Whether to get all layer info or just the current

    Returns:
        dict: Animation data
    """
    if allProperties:
        properties = component.PropertyList
    else:
        properties = list(filter(None, [component.PropertyList.Find(srcProperty) for srcProperty in properties]))

    # use anim layers
    currentTake = Globals.System.CurrentTake
    currentLayerIndex = currentTake.GetCurrentLayer()
    layerIndexes = range(currentTake.GetLayerCount()) if allLayers else [currentLayerIndex]

    animData = {}
    for layerIdx in layerIndexes:
        currentTake.SetCurrentLayer(layerIdx)

        layer = currentTake.GetLayer(layerIdx)
        layerData = collections.defaultdict(list)
        layerData["layerName"] = layer.Name
        for srcProperty in properties:

            isLocked = srcProperty.IsLocked()
            if srcProperty.IsUserProperty():
                propertyType = srcProperty.GetPropertyType()
                propertyDict = {
                    "name": srcProperty.Name,
                    "type": propertyType,
                    "dataType": srcProperty.GetPropertyTypeName(),
                    "animatable": srcProperty.IsAnimatable(),
                    "min": srcProperty.GetMin(),
                    "max": srcProperty.GetMax(),
                }

                if propertyType == mobu.FBPropertyType.kFBPT_enum:
                    propertyDict["enumStringList"] = srcProperty.GetEnumStringList()

                elif propertyType == mobu.FBPropertyType.kFBPT_object:
                    propertyDict["object"] = None
                    sourceObject = srcProperty.GetSrc(0)
                    if sourceObject:
                        propertyDict["object"] = sourceObject.Name

                elif propertyType == mobu.FBPropertyType.kFBPT_Reference:
                    refProperty = srcProperty.GetSrc(0)
                    propertyDict["references"] = {
                        "owner": refProperty.GetOwner().Name,
                        "propertyName": refProperty.Name,
                    }

                layerData["userProperties"].append(propertyDict)

            if (
                not hasattr(srcProperty, "GetAnimationNode")
                or not srcProperty.GetAnimationNode()
            ):
                try:
                    layerData[srcProperty.Name].append({"value": srcProperty.Data, "static": True, "locked": isLocked})
                except NotImplementedError:
                    continue
                continue

            animNode = srcProperty.GetAnimationNode()
            if not len(animNode.Nodes):
                layerData[srcProperty.Name].append(getCurveData(animNode.FCurve))
            else:
                for subAnimNode in animNode.Nodes:
                    if subAnimNode.FCurve and len(subAnimNode.FCurve.Keys):
                        layerData[srcProperty.Name].append(getCurveData(subAnimNode.FCurve))

        animData[layerIdx] = dict(layerData)

    currentTake.SetCurrentLayer(currentLayerIndex)
    return animData


def setAnimData(component, animData):
    """Sets anim data onto component

    Args:
        component (mobu.FBComponent): Component to set data on
        animData (dict): Animamtion dictionary data
    """
    currentTake = Globals.System.CurrentTake
    currentLayerIndex = currentTake.GetCurrentLayer()

    for layerIndex, layerData in animData.iteritems():
        layerName = layerData["layerName"]
        layer = currentTake.GetLayerByName(layerName)
        if not layer:
            currentTake.CreateNewLayer()
            layer = currentTake.GetLayer(currentTake.GetLayerCount() - 1)
            layer.Name = layerName

        currentIndex = layer.GetLayerIndex()
        currentTake.SetCurrentLayer(currentIndex)

        # move layer around
        if currentIndex != layerIndex:
            if currentIndex > layerIndex:
                for idx in xrange(currentIndex - layerIndex):
                    currentTake.MoveCurrentLayerDown()
            else:
                for idx in xrange(layerIndex - currentIndex):
                    currentTake.MoveCurrentLayerUp()

        currentTake.SetCurrentLayer(layerIndex)
        for userPropertyData in layerData.get("userProperties", ()):
            userPropertyName = userPropertyData["name"]
            if component.PropertyList.Find(userPropertyName):
                continue

            propertyType = userPropertyData["type"]
            refProperty = None
            if propertyType == mobu.FBPropertyType.kFBPT_Reference:
                referenceObject = mobu.FBFindModelByLabelName(userPropertyData["references"]["owner"])
                if referenceObject:
                    refProperty = referenceObject.PropertyList.Find(userPropertyData["references"]["propertyName"])

            userProperty = component.PropertyCreate(
                userPropertyName,
                propertyType,
                userPropertyData["dataType"],
                userPropertyData["animatable"],
                True,  # is user attribute
                refProperty,
            )
            userProperty.SetMin(userPropertyData["min"])
            userProperty.SetMax(userPropertyData["max"])

            if propertyType == mobu.FBPropertyType.kFBPT_enum:
                userEnumList = userProperty.GetEnumStringList(True)
                for enumText in userPropertyData["enumStringList"]:
                    userEnumList.Add(enumText)

                userProperty.NotifyEnumStringListChanged()

            elif propertyType == mobu.FBPropertyType.kFBPT_object:
                if not userPropertyData["object"]:
                    continue
                objectComponent = mobu.FBFindModelByLabelName(userPropertyData["object"])
                if objectComponent:
                    userProperty.ConnectSrc(objectComponent)

        for propertyName, propertyData in layerData.iteritems():
            destProperty = component.PropertyList.Find(propertyName)
            if not destProperty or not len(propertyData):
                continue

            isLocked = destProperty.IsLocked()
            if isLocked:
                destProperty.SetLocked(False)

            # set static data
            if len(propertyData) == 1 and isinstance(propertyData[0], dict) and propertyData[0].get("static", False):
                destProperty.Data = propertyData[0]["value"]
                destProperty.SetLocked(propertyData[0]["locked"])
            else:
                destProperty.SetAnimated(True)
                animNode = destProperty.GetAnimationNode()

                if animNode.FCurve:
                    setCurveData(animNode.FCurve, propertyData[0])

                for idx, node in enumerate(animNode.Nodes):
                    setCurveData(node.FCurve, propertyData[idx])

                destProperty.SetLocked(isLocked)

    currentTake.SetCurrentLayer(currentLayerIndex)
    Scene.EvaluateTimeline()


def createNewTake(name=None):
    """Creates a new take with a given name.

    Args:
        name (str): Name for the new take

    Returns:
        mobu.FBTake
    """
    # If no name is given just call the take "New Take"
    name = name or "New Take"
    # Copy the current take and give it the new name
    newTake = Globals.System.CurrentTake.CopyTake(name)
    # Clear animation on all properties (True for selected objects, False for all objects)
    newTake.ClearAllProperties(False)
    # Set the new take to the current take
    Globals.System.CurrentTake = newTake

    return newTake


def deleteKeysByRange(component, keyRange, allLayers=False):
    """Delete keyframes outside of keyRange and insert keys as needed to maintain curve

    Args:
        component (FBComponent): Component to delete all keys on in range
        keyRange (list of 2 int): Keys range to retain
        allLayers (bool): delete keys all the layers and not just the current layer.
    """
    currentTake = Globals.System.CurrentTake
    currentLayerIndex = currentTake.GetCurrentLayer()
    layerIndexes = range(currentTake.GetLayerCount()) if allLayers else [currentLayerIndex]

    startTime = mobu.FBTime(0, 0, 0, keyRange[0])
    endTime = mobu.FBTime(0, 0, 0, keyRange[-1])

    for layerIdx in layerIndexes:
        currentTake.SetCurrentLayer(layerIdx)

        for srcProperty in component.PropertyList:

            if not hasattr(srcProperty, "GetAnimationNode"):
                continue

            animNode = srcProperty.GetAnimationNode()
            if not animNode:
                continue

            isLocked = srcProperty.IsLocked()
            if isLocked:
                srcProperty.SetLocked(False)

            # collect all the fCurves
            fCurves = []
            if not len(animNode.Nodes):
                if animNode.FCurve and len(animNode.FCurve.Keys):
                    fCurves.append(animNode.FCurve)
            else:
                for subAnimNode in animNode.Nodes:
                    if subAnimNode.FCurve and len(subAnimNode.FCurve.Keys):
                        fCurves.append(subAnimNode.FCurve)

            for fCurve in fCurves:
                # check if first frame is greater than start range and make sure a key is inserted
                if fCurve.Keys[0].Time.GetFrame() < startTime.GetFrame():
                    fCurve.KeyInsert(startTime)

                # check if last frame is less than end frame and make sure a key is inserted
                if fCurve.Keys[-1].Time.GetFrame() > endTime.GetFrame():
                    fCurve.KeyInsert(endTime)

                # delete keys outside of range
                fCurve.KeyDeleteByTimeRange(mobu.FBTime.MinusInfinity, startTime, False)  # start, end, inclusive
                fCurve.KeyDeleteByTimeRange(endTime, mobu.FBTime.Infinity, False)

            srcProperty.SetLocked(isLocked)

    currentTake.SetCurrentLayer(currentLayerIndex)
