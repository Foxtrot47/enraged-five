"""
Description:
    Core module for creating renders of ingame animations.
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import base64
import pyfbsdk as mobu
import RS.Core.DatabaseConnection as DatabaseConnection

DB_DRIVER = '{SQL Server}'
DB_SERVER = 'RSGNYCSQL1\\RSGNYCSQL1'
DB_DATABASE = 'AnimationStats_RDR3'
DB_USER = 'tools'
DB_PASS = base64.b64decode('cm9ja3N0YXIxQQ==')


def RenderVideoFromPath(outputPath, customOptions=False):
    """
    Forces render options and creates a render video from a path.
    Arguments:
        outputPath (string): local path to render the video to
    """
    # Set Options
    lOptions = mobu.FBVideoGrabber().GetOptions()
    VideoManager = mobu.FBVideoCodecManager()
    VideoManager.VideoCodecMode = mobu.FBVideoCodecMode.FBVideoCodecStored
    if customOptions:
        outputFormat = os.path.splitext(os.path.basename(lOptions.OutputFileName))[1]
        outputPath = "".join([outputPath[:-4], outputFormat])
    else:
        lOptions.CameraResolution = lOptions.CameraResolution.kFBResolutionD1NTSC
        lOptions.RenderAudio = True
        lOptions.AudioRenderFormat = 2113538  # Code for 48K
        lOptions.ShowTimeCode = True
        lOptions.ShowSafeArea = False
        lOptions.ShowCameraLabel = False
        lOptions.AntiAliasing = False
        lOptions.TimeSpan = mobu.FBTimeSpan(mobu.FBTime(0, 0, 0, 0), mobu.FBTime(0, 0, 0, 0))
        lOptions.TimeSteps = mobu.FBTime(0, 0, 0, 1)

    # Render Video
    lOptions.OutputFileName = outputPath
    mobu.FBApplication().FileRender(lOptions)


def DeleteAudioHudItems():
    """
    Deletes all IgRender Hud and Audio elements from the current camera.
    """
    deleteHuds = []
    deleteElements = []
    currentCam = mobu.FBSystem().Scene.Renderer.CurrentCamera
    for camHud in currentCam.HUDs:
        if camHud.Name.startswith("IgRenderHud"):
            deleteHuds.append(camHud)
        for hudElement in camHud.Elements:
            if hudElement.Name.startswith("IgRenderAudio"):
                deleteElements.append(hudElement)
    deleteList = deleteHuds + deleteElements
    for deleteItem in deleteList:
        deleteItem.FBDelete()


def AddAudioHudItems(audioNames):
    """
    Adds audio file names to the current camera Hud during renders.
    Arguments:
        audioNames (list of strings): containing audio file names
    """
    # Delete Any Existing Audio Path Elements
    DeleteAudioHudItems()

    # Get Current HUD or Create New One
    currentCam = mobu.FBSystem().Scene.Renderer.CurrentCamera
    camHuds = list(currentCam.HUDs)
    if not camHuds:
        currentHud = mobu.FBHUD("IgRenderHud")
        mobu.FBSystem().Scene.ConnectSrc(currentHud)
    else:
        currentHud = camHuds[0]

    # Add Each Audio Name to the HUD
    for index, audioName in enumerate(audioNames):
        verticalOffset = index * 5
        HUDTextElement = mobu.FBHUDTextElement("IgRenderAudio")
        HUDTextElement.Content = audioName
        HUDTextElement.X = 0
        HUDTextElement.Y = 48 - verticalOffset
        HUDTextElement.Height = 5
        HUDTextElement.Justification = mobu.FBHUDElementHAlignment.kFBHUDCenter
        HUDTextElement.HorizontalDock = mobu.FBHUDElementHAlignment.kFBHUDCenter
        HUDTextElement.VerticalDock = mobu.FBHUDElementVAlignment.kFBHUDVCenter
        currentHud.ConnectSrc(HUDTextElement)
        currentCam.ConnectSrc(currentHud)


def CleanTakeName(take):
    """
    Removes invalid characters (for file names) from take names.
    Arguments:
        take (mobu.FBTake): the take use for this render
    Returns:
        cleanName (string): take name with invalid characters removed
    """
    invalidCharacters = ['\\', '/', ':', '*', '?', '"', '<', '>', '|']
    cleanedName = "".join([char for char in take.Name if char not in invalidCharacters])
    return cleanedName


def RenderIgVideo(take, outputPath, audioTracks=None, customOptions=False):
    """
    The main module for creating ingame renders.
    Arguments:
        take (mobu.FBTake): the take use for this render
        outputPath (string): local path to render the video to
    Kewyword Arguments:
        audioTracks (mobu.FBStoryTrack): audio tracks enable during render
    """
    # Set Story Mute, Current Take, and Get Story Tracks
    mobu.FBSystem().CurrentTake = take
    storyTracks = [track for track in mobu.FBStory().RootFolder.Tracks
                   if track.Type == mobu.FBStoryTrackType.kFBStoryTrackAudio]

    # Set Audio Tracks Options - if provided
    if audioTracks:
        for track in storyTracks:
            if track in audioTracks:
                track.Mute = False
            else:
                track.Mute = True

    # Get Audio Names for HUD Text
    audioNames = []
    for track in storyTracks:
        if not track.Mute:
            for clip in track.Clips:
                clipName = "{}.WAV".format(clip.Name.upper().split(".WAV")[0])
                if clipName not in audioNames:
                    audioNames.append(clipName)

    # Add Audio Names to HUD
    if audioNames:
        AddAudioHudItems(audioNames)

    # Render Out Video
    RenderVideoFromPath(outputPath, customOptions)

    # Cleanup HUD and Elements
    if audioNames:
        DeleteAudioHudItems()


def UpdateRenderDatabase(takeAudioDict):
    """
    Updates the database with takeName and audioPath info from a takeAudioDict.
    Arguments:
        takeAudioDict (dict): Mobu takes for keys and lists of Mobu story audio tracks for values.
    """
    # Connect to Database (only if tracks detected)
    detectedTracks = [track for track in takeAudioDict.itervalues() if track]
    if detectedTracks:
        DB_CONN = DatabaseConnection.DatabaseConnection(
            driver=DB_DRIVER, server=DB_SERVER, database=DB_DATABASE,
            username=DB_USER, password=DB_PASS)
        if DB_CONN.is_connected:

            # Loop Through Takes and Tracks Setting Parameters
            fbxPath = mobu.FBApplication().FBXFileName
            fbxName = os.path.splitext(os.path.basename(fbxPath))[0]
            for take, tracks in takeAudioDict.iteritems():
                takeName = take.Name.replace("'", "''")
                for track in tracks:
                    audioPath = track.Clips[0].ClipAudioPath

                    # Create Command Text and Run
                    cmdTxt = "exec FBX_Add_Take_Audio '{0}', '{1}', '{2}', '{3}'"
                    cmdTxt = cmdTxt.format(fbxName, fbxPath, takeName, audioPath)
                    DB_CONN.sql_command(cmdTxt)
