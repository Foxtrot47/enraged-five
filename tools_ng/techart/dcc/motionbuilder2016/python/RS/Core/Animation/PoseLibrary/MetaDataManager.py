'''
Pose importer tester

'''

import RS
import RS.Core.Metadata as Metautils

# 
#_poseFile1 = "X:/wildwest/etc/poseLibrary/global_Body.meta"
#_poseFile2 = "X:/wildwest/etc/poseLibrary/test.meta"

'''
Loading a MEtaFIle
'''
def load(poseFile = None):
    if poseFile is not None:
        poseManager = RS.Core.Metadata.ParseMetaFile( poseFile )
        return poseManager
    return False

'''
SAving an Meta File
'''
def save():
    metaFile = RS.Core.Metadata.CreateMetaFile( "cPose_ManagerXml" )
    metaFile.SetFilePath(_poseFile2)
    
    # Add Entry
    f = metaFile.Poses.Create()
    # Define our Category
    f.Category = "Generic"
    # Add sub enties to poses
    entry = f.PoseList.Create()
    entry.Name = "Generic Male Idle"
    entry.Anim = "//depot/gta5/art/anim/export_mb/MOVE_/M@/GENERIC/Idle.anim"
    entry.Frame = 34
    entry.Date = "10/2/23:32"
    entry.User = "John.bob@rockstargames.com"
    # Create anohter entry
    entry = f.PoseList.Create()
    '''
    We can then add differnt objects
    '''
    
    
    f = metaFile.Poses.Create()
    f.Category = "Ingame"
    
    
    metaFile.Save()
    
#metaObject = load()
## get first pose Category
#print metaObject.Root.Poses[0].Category
## print each pose in the list under category
#for _pose in metaObject.Root.Poses[0].PoseList:
    #print _pose.Name
    #print _pose.Anim
    #print _pose.Frame

'''
Uncomment for example of how to save/update an meta file
'''
#save()




