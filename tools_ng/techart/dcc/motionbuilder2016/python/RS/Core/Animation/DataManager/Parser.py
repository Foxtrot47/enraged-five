"""
Description:
    Contains the base class for parsing data from a file and storing it into a class instance.

    This class is meant to be inherited by other classes to add support for reading and interacting
    with files. This class provides a simple framework for implementing different algorithms when
    extracting data from different type of files.

Authors:
    David Vega <david.vega@rockstargames.com>

"""
import os
import re
from collections import OrderedDict
from functools import wraps


def StorePath(func):
    """
    A decorator for class methods utilizing self.

    If a path is passed, store it internally and run the function

    Arguments:
        func (function): function being decorated
    """

    @wraps(func)
    def wrapper(self, path, *args, **kwargs):
        """
        wrapper method

        Arguments:
            path (string): path to store
            *args (list): positional arguments the decorated method may accept
            **kwargs (dictionary): keyword arguments the decorated method may accept

        Return:
            result from the decorated method
        """
        # Fix path to use correct amount of backslashes to escape special characters correctly
        # os.abspath() does not return the correct results after calling it once
        pathParts = re.split("\\|/", path)
        path = "".join(["{}\\".format(each) for each in pathParts if each])[:-1]

        # Remove any extra spaces in the path
        path = path.strip()
        self.Path = ""
        self.Extension = ""
        if os.path.isfile(path) and path != getattr(self, "Path", ""):
            # Add path to the object instance
            self.Path = path
            self.Extension = os.path.splitext(path)[-1][1:]

        # run method
        return func(self, self.Path, *args, **kwargs)
    return wrapper


class AbstractObject(object):

    @StorePath
    def __init__(self, path, **keywordArguments):
        """
        Parses a file and stores it's information through attributes to make accessing the
        information easier

        Arguments:
            path = (string): path to a file to the file to parse
            **keywordArguments (dictionary): adds the values of the dictionary keys as attributes to the instance
                                             of this object
        """
        self.Path = path
        self.Extension = getattr(self, "Extension", "")
        self.Data = []           # The raw data from the anim file stored as a list

        if self.Path:
            self.Initialize(self.Path, **keywordArguments)

    def Initialize(self, path, **keywordArguments):
        """
        Converts the data from the anim/text file into attributes and objects accessible by this object

        Arguments:
            path = (string): path to a file to the file to parse
            **keywordArguments (dictionary): adds the values of the dictionary keys as attributes to the instance
                                             of this object
        """
        # Convert keyword argument values into variables of this class instance
        [setattr(self, each_key, value) for each_key, value in keywordArguments.iteritems()]

        # Parse the File and retrieve the data
        self.ParseFile(path)
        self.ParseData()

    @StorePath
    def ParseFile(self, path, clean=True):
        """
        Parses a file and converts the content into a list

        Arguments:
            path (string): path to a file
            clean (boolean): if empty strings and None statements should be removed from the results
                             generated from the file.
        """
        # Dynamically retrieve the correct function for parsing the file
        # We use a python cheat to get the private methods dynamically

        parse_method = getattr(self, "_{}__Parse{}File".format(self.__class__.__name__,
                                                               self.Extension.capitalize()),
                               self.__ParseTextFile)

        # Read file
        self.Data = parse_method()

        if clean:
            # Get output as a list
            output = self.Data.splitlines()
            # Clean the output of any empty strings/None values
            self.Data = filter(None, output)

        return self.Data

    def __ParseTextFile(self):
        """ Gets the content of a text file """
        text_file = open(self.Path, "r")
        output = text_file.read()
        text_file.close()
        return output

    def ParseData(self):
        """
        Converts the parsed data into attributes and classes for ease of access

        This method should be reimplemented in classes that subclass AbstractObject
        """
        pass


class AbstractDictionary(OrderedDict):
    """
    This is a custom dictionary that allows you to access keys as variables
    """

    def __str__(self):
        """
        Overrides built-in method

        Displays the value of the attribute value when this class is printed on the console
        """
        return str(self.__dict__.get("value", self.__class__.__name__))

    def __setitem__(self, item, value):
        """
        Overrides built-in method

        When a key value pair is set, add the key as an attribute with the value

        Arguments:
            item (string): name of the key
            value (anything): value being stored

        """
        if "_OrderedDict" not in item:
            self.__dict__[item] = value
        return OrderedDict.__setitem__(self, item, value)

    def __setattr__(self, item, value):
        """
        Overrides built-in method

        When an attribute is set, add the attribute namd and the value a a key value pair to the dictionary

        Arguments:
            item (string): name of the attribute
            value (anything): value being stored

        """

        if "_OrderedDict" not in item:
            self[item] = value
        return OrderedDict.__setattr__(self, item, value)