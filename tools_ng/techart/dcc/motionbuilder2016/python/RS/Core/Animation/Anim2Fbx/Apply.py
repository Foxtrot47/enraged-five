"""
Description:
    Anim to mobu.FBX Parser Classes that support Motion Builder interaction

    Exposes animation data from anim files and their txt counter parts
    through an object interface to ease access of data.

Authors:
    Mark Harrison-Ball <mark.harrison-ball@rockstargames.com>
    David Vega <david.vega@rockstargames.com>

Example:
    #Reference in a Character First through the Reference Editor
    
    import RS.Core.Animation.Anim2Fbx as AnimToFbx
    reload(AnimToFbx)
    anim_data = AnimToFbx.AnimObject(AnimToFbx.DEBUG_BODY_FILE)
    anim_data.ApplyAnimation()

"""

import os.path

import pyfbsdk as mobu

from RS import Globals, Config
from RS.Core.Animation import Lib
from RS.Utils import Math, MathGameUtils, Scene

# Static variables

FACIAL_CONTROL_ID = {control_id: control_name for control_name, control_id
                     in Globals.gAmbientControllerIds.items()}
XYZ= {"x": 0,
      "y": 1,
      "z": 2}

# Anim Files for testing purposes
DEBUG_BODY_FILE = os.path.join(Config.Project.Path.Art,'anim\export_mb\COMBAT@\BULLET_REACTIONS\BEHIND_1.anim')
DEBUG_FACE_FILE = os.path.join(Config.Project.Path.Art,'anim\export_mb\FACIALS@\GEN_FEMALE@\BASE\BlowKiss_1.anim')
DEBUG_SKELETON_FILE = os.path.join(Config.Tool.Path.Root, 'etc\\config\\anim\\skeletons\\player.skel')

# Methods


def ApplyTrack(animationNode, animTrack, start_frame=0, absolute=False, animatable_axis="xyz",
               rotation_order=mobu.FBRotationOrder.kFBXYZ, rotation_offset=None):
    """
    Applies the animTrack to the bone/control/model that is provided in MotionBuilder.

    Arguments:

        animationNode (pyfbsdk.FBAnimationNode) The animation node object that you want to apply the animation to
        animTrack (AnimTrack): The Animation Data that you want to apply to the model
        start_frame (int): The start frame for the animation, by default it is 0
        absolute (boolean): If you want the start frame to be the exact frame an animation is applied.
                            Default behavior , when False, is to offset animation from the frame embedded in the data
                            by the start frame.
        animatable_axis (string): The axis that on which you want the animation to be applied, accepts "xyz"
        rotation_order (FBRotationOrder): rotation order of the object
    """
    # isOffsetQuaternion = len(rotation_offset or []) == 4
    for each_value in animTrack.values:

        each_value_length = len(each_value.value)
        value = each_value.value

        frame = each_value.frame + start_frame

        if absolute:
            frame = start_frame

        # Convert values into mobu.FBVectors
        if each_value_length == 4:
            # TODO: Finish looking up a solution that is more stable than adding two vectors together
            # if isOffsetQuaternion:
            #     lVector4d = mobu.FBVector4d()
            #     offsetQuaternion = mobu.FBVector4d(*rotation_offset)
            #     mobu.FBQMult(lVector4d, mobu.FBVector4d(*value), offsetQuaternion)
            # else:
                # Convert Quaternion Values into Euler
            lVector4d = mobu.FBVector4d(*value)
            value = mobu.FBVector3d()
            if rotation_order in [mobu.FBRotationOrder.kFBXYZ, mobu.FBRotationOrder.kFBZYX]:

                mobu.FBQuaternionToRotation(value, lVector4d, rotation_order)
                value = [float(each_vector_value) for each_vector_value in value]

            else:
                value = Math.FBQuaternionToEuler(rotationOrder=str(rotation_order)[-3:], *each_value.value)

            # if not isOffsetQuaternion and rotation_offset:
            if rotation_offset:
                value = [_value + offset for _value, offset in zip(value, rotation_offset)]
        # Apply Keyframes
        for each_axis in animatable_axis:

            xyz_index = XYZ.get(each_axis, 0)
            value_index = xyz_index

            # If value index is larger than the actual size of the value, change it to -1
            if value_index >= len(value):
                value_index = -1

            if not len(animationNode.Nodes):
                animationNode.KeyAdd(
                    mobu.FBTime(0, 0, 0, frame), value[value_index])

            elif len(animationNode.Nodes) < xyz_index:
                continue

            else:
                animationNode.Nodes[xyz_index].KeyAdd(
                    mobu.FBTime(0, 0, 0, frame), value[value_index])


def MapToSkeleton(namespace, TrackList, start_frame=0, absolute=False, degreesOfFreedomOn=True):
    """
    Applies the animation parsed from the anim file to the selected rig.

    Arguments:
        namespace (string): namespace of the bones that the animation is being applied to
        TrackList (list[AnimTrack, AnimTrack, ect.]): list of tracks that you want to apply to the selected
                        character.
        start_frame (int): frame to start the animation at
        absolute (boolean): If you want the start frame to be the exact frame an animation is applied.
                            Default behavior , when False, is to offset animation from the frame embedded in the data
                            by the start frame.

    """

    # Turn off characters and constraints
    [setattr(each_character, "Active", False) for each_character in Globals.gCharacters if
     each_character.LongName.startswith("{}:".format(namespace))]
    [setattr(each_constraint, "Active", False) for each_constraint in Globals.gConstraints
        if each_constraint.Name == "mover_toybox_Control"]

    Lib.PrepareMover(namespace)

    for each_track in TrackList:

        modelName = "{}:{}".format(namespace, each_track.name).strip()
        lModel = mobu.FBFindModelByLabelName(modelName)

        # If we are working with props or facial rigs
        # Then we need to select different models from the names provided

        if each_track.track_id in [22, 25, 26]:
            controlName = FACIAL_CONTROL_ID.get(each_track.id, None)
            lModel = mobu.FBFindModelByLabelName("{}:{}".format(namespace, controlName))

        # This is for props BUT it doesn't work as expected
        # The mover gets tagged with these ids and the character may not end up facing the right way
        # elif each_track.track_id in [5, 6]:
        #    lModel = mobu.FBFindModelByLabelName("geo_Control")

        # The Facial FX rig is only animated in Y, so we only need to plot that axis
        axis = "xyz"
        if each_track.track_id == 22:
            axis = "y"

        if lModel:

            # Get the Rotate Order of the model
            rotationOrder = mobu.FBRotationOrder.kFBXYZ

            # The QuaternionToRotation method doesn't behave as expected in Motion Builder
            # The method only returns values for the XYZ and ZYX rotation orders
            # We use our own math methods for getting  the rotation for the other rotation orders

            # Get the offset rotation
            rotationOffset = None
            if each_track.type == "Rotation" and degreesOfFreedomOn:
                lModel.PropertyList.Find("Enable Rotation DOF").Data = True
                rotationOffset = lModel.PropertyList.Find("Pre Rotation").Data * -1
                #quaternionOffset = mobu.FBVector4d()
                #mobu.FBRotationToQuaternion(quaternionOffset, rotationOffset)
                #rotationOffset = [quaternionOffset[index] for index in xrange(4)]
                rotationOffset = [rotationOffset[index] for index in xrange(3)]


            elif each_track.type == "Rotation" and not degreesOfFreedomOn:
                lModel.PropertyList.Find("Enable Rotation DOF").Data = False

            if lModel.PropertyList.Find("RotationOrder"):
                rotationOrder_string = lModel.RotationOrder.name[-3:]
                rotationOrder = getattr(mobu.FBRotationOrder, "kFB{}".format(rotationOrder_string))

            # Get the correct property of the model
            # ei. Translation, Rotation, Scale, etc.
            lModelTranslations = getattr(lModel, each_track.type, None)
            if lModelTranslations is None:
                continue

            lModelTranslations.SetAnimated(True)

            lModelAnimationNode = lModelTranslations.GetAnimationNode()

            ApplyTrack(lModelAnimationNode, each_track, start_frame, absolute, axis, rotationOrder, rotationOffset)

    UpdateDirectionComponents(namespace)
    return True


def MapToCamera(TrackList, start_frame=0, absolute=False, name=None):
    """
    Imports a camera with the animation from the anim file
    Arguments:
        TrackList  (list[AnimTrack.AnimTrack, ect.]): list of AnimTracks with the values for the cameras
        start_frame (int): frame to start the animation on
        absolute (boolean): force the start frame to be the frame where all animation is plotted to.
    """
    # Create Camera to apply animation too
    if name is None:
        lCamera = mobu.FBCamera('ImportedCamera')
    else:
        lCamera = mobu.FBFindModelByLabelName(name)

    lCamera.Show = True

    for each_track in TrackList:

        cameraProperty = getattr(lCamera, each_track.type)
        cameraProperty.SetAnimated(True)

        axis = "xyz"

        if each_track.type == "Translation":
            # Correct Translation Values
            [setattr(each_value, "value", [each_value.value[0] * 100,
                                           each_value.value[2] * 100,
                                           each_value.value[1] * -100])
                for each_value in each_track.values]

        elif each_track.type == "Rotation":
            for eachValue in each_track.values:

                matrix = mobu.FBMatrix()
                # Convert Quaternion into a Matrix
                Math.FBQuaternionToFBMatrix(mobu.FBVector4d(*eachValue.value), matrix)

                # Rotate Matrix to match the world up axis of Mobu
                matrix = MathGameUtils.convertGameToMobu(matrix)

                # We rotate the camera 90 degrees on Z so it faces forward
                # It seems that by default it faces the ground unless this extra rotation is added
                matrix = MathGameUtils._rotMatrix(matrix, 0, 0, 90, False)

                lCamera.SetMatrix(matrix)
                mobu.FBSystem().Scene.Evaluate()
                eachValue.value = lCamera.Rotation.Data[0], lCamera.Rotation.Data[1], lCamera.Rotation.Data[2]
        else:
            axis = "y"

        lCameraTranslations = getattr(lCamera, each_track.type)
        lCameraTranslations.SetAnimated(True)
        lCameraAnimationNode = lCameraTranslations.GetAnimationNode()

        ApplyTrack(lCameraAnimationNode, each_track, start_frame, absolute, axis)

# Patch Code


def UpdateDirectionComponents(namespace):
    """
    Copies the animation from the OH direction bones to their controls

    Arguments:
        namespace (string): namespace of the bones

    """
    time = mobu.FBTime(0, 0, 0, 0, 0)
    mobu.FBPlayerControl().Goto(time)

    allConstraints = []

    # TODO: Move bone control list to be data driven

    for bone, control in zip(("OH_FacingDir", "OH_UpperFixup", "OH_IndependentMover", "OH_TorsoDir"),
                             ("OH_FacingDirection", "OH_UpperFixupDirection", "OH_IndependentMoverDirection",
                              "OH_TorsoDirection")):
        bone = mobu.FBFindModelByLabelName("{}:{}".format(namespace, bone))
        control = mobu.FBFindModelByLabelName("{}:{}".format(namespace, control))

        if None not in (bone, control):

            constraints = Scene.GetIncomingConnections(control, filterByType=[mobu.FBConstraint])

            if constraints:
                constraints[0].Active = False

            Lib.CopyAnimation(bone, [control])
            mobu.FBSystem().Scene.Evaluate()

            # To force the constraints to recognize that their offset values have been keyed
            # We have to move the time slider one frame forward and evaluate the scene
            # We also evaluate the scene when we change values to make sure that motion builder
            # picks up the changes

            currentTime = mobu.FBSystem().LocalTime
            currentFrame = int(currentTime.GetFrame())

            nextTime = mobu.FBTime(0, 0, 0, currentFrame + 1)
            mobu.FBPlayerControl().Goto(nextTime)
            mobu.FBSystem().Scene.Evaluate()

            if constraints and None not in (constraints[0].PropertyList.Find("Rotation"), bone.PropertyList.Find("Lcl Rotation")):
                mobu.FBSystem().Scene.Evaluate()
                constraints[0].PropertyList.Find("Rotation").Data = bone.PropertyList.Find("Lcl Rotation").Data * -1
                mobu.FBSystem().Scene.Evaluate()

            allConstraints.extend(constraints)
            mobu.FBPlayerControl().Goto(currentTime)
            mobu.FBSystem().Scene.Evaluate()

    for constraint in allConstraints:
        constraint.Active = True

