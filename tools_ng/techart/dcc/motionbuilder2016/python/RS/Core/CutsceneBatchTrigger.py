"""
Simple wrapper around the batch Exporter
"""
import os


import RS.Core.Automation.Server
import RS.Core.Automation


jobModes = [ RS.Core.Automation.JOB_MODE_UPDATE_CAMERAS, RS.Core.Automation.JOB_MODE_UPDATE_EST_TRACKS, RS.Core.Automation.JOB_MODE_UPDATE_REFERENCES ]

debug = 0

def submitJob(fbxfilename):
    
    global jobModes
    if debug:
        print "Simulating job send"
        print fbxfilename
        print jobModes
    else:
        RS.Core.Automation.Server.addJob( fbxfilename, jobModes )