"""
Runner script for the reparent roll bones script so it can be run from the menu
"""

from PySide import QtGui

from RS.Core.Mocap.Giant import PrepCharacter


def Run(show=True):
    PrepCharacter.ReparentRollBones()
    
    if show:
        QtGui.QMessageBox.information(None, "Prep Character", "RollBones have been re-parented.")