'''

 Script Path: RS/Core/Mocap/Previz/CreateExportCamera.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 24 October 2013
 
 Description: Launcher for creating an export camera, using HB's CameraCore functions

'''

from pyfbsdk import *

from RS.Core.Camera import Lib as CamLib

#calling the CreateExportCamera module from CameraCore.py
CamLib.MobuCamera.CreateExportCamera()
