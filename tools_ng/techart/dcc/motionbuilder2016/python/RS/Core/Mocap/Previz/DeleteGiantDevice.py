'''

 Script Path: RS/Core/Mocap/Previz/DeleteGiantDevice.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 29 October 2013
 
 Description: Launcher for DeleteGiantDevice module from MocapCommands.py
              Turns off and deletes the giant device from the scene

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the DeleteGiantDevice module from MocapCommands.py
mocapCommand.DeleteGiantDevice()