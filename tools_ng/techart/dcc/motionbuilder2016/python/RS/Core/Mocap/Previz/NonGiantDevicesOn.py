'''

 Script Path: RS/Core/Mocap/Previz/NonGiantDevicesOn.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 08 October 2013
 
 Description: Launcher for NonGiantDevicesOn module from MocapCommands.py
              Sets 'Online', 'Recording' and 'Live' options to ON for all
              devices, apart from the 'giant studios device'

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the NonGiantDevicesOn module from MocapCommands.py
mocapCommand.NonGiantDevicesOn()