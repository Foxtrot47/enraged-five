'''

 Script Path: RS/Core/Mocap/Previz/AddFacewareLiveDevice.py
 
 Written And Maintained By: Kristine Middlemiss
 
 Created: 07 January 2014
 
 Description: Launcher for AddFacewareLiveDevice module from MocapCommands.py
              Adds the giant device to the scene

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the AddFacewareLiveDevice module from MocapCommands.py
mocapCommand.AddFacewareLiveDevice()