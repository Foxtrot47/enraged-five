###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: PreserveProperty
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Perserve Giant Custom Property when plotting gs char to mb char
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Globals as glo

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Get Giant Character List
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def Run():
    lCharacterList = []
    
    for iChar in glo.gCharacters:
        lCharacterList.append(iChar)
    
    for iChar in lCharacterList:
        
        lGiantChar = iChar.InputCharacter
        
        lSkelRoot = iChar.GetModel(FBBodyNodeId. kFBHipsNodeId)
        
        if lGiantChar != None:
            
            lGiantSkelRoot = lGiantChar.GetModel(FBBodyNodeId. kFBHipsNodeId)
                
            lGiantProperty = lGiantSkelRoot.PropertyList.Find("GiantAnim")
            lProperty = lSkelRoot.PropertyList.Find("GiantAnim")
            
            if lProperty:
                #Adjust if they wish to keep previous anim properties/add multiple takes.
                lSkelRoot.PropertyRemove(lProperty)
                
            if lGiantProperty:
                
                lGiantData = lGiantProperty.Data
                lProperty = lSkelRoot.PropertyCreate('GiantAnim', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lProperty.Data = lGiantData
                print "rs_Giant_PreserveProperty script found a GiantAnim property on " +  lGiantChar.Name + ". Transferring across to " + iChar.Name 
            
            else:
                print "Unable to find a Custom Property 'GiantAnim' on " + lGiantSkelRoot.LongName  

    
    
