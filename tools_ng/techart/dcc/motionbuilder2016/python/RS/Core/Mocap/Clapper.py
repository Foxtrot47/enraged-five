"""Core Library of methods around the clapper to help duplicate, get and work with it"""
import math
import copy
import operator

import pyfbsdk as mobu
from RS import Globals
from RS.Utils.Scene import AnimationNode, Groups
from RS.Utils import Scene, Vector


def _AnalyzeClapperData(clapperData, offset=0):
    """Function to create a highpass filter to run over the clapper data and to retrieve the peaks
    of the filter data, and follow that to where the clapper is closed again.

    Args:
        clapperData (dict): The clapper data, where the key is the frame number and the value
            is the rotation data clapper for x, y, or z.
        offset (int, optional): A heuristic that allows us to shift which frame the slate clap is on. This
            allows us to make adjustments to sync up with other slate extraction methods.

    Returns:
        list of int: All the frame numbers where the clapper hits down after the third clap

    Notes:
        This is identical to rockstar.mocapBoss.tasks.slateClapperExtraction.TimeCodeActionExtraction._analyzeClapperData
    """
    clapRange = max(clapperData.values()) - min(clapperData.values())
    clapDowns = []
    downThreshold = clapRange / 10
    clapperUpFrames = [frame for frame, value in clapperData.items() if value > downThreshold]
    if len(clapperUpFrames) == 0:
        return []

    clapsGroups = []
    currentClapGroup = []
    for idx in range(1, len(clapperUpFrames)):
        frame = clapperUpFrames[idx]
        if frame - clapperUpFrames[idx - 1] > 50:
            clapsGroups.append(currentClapGroup)
            currentClapGroup = []
        else:
            currentClapGroup.append(frame)
    clapsGroups.append(currentClapGroup)

    # Go through each clap group and get the touch down frame
    for clapperGroup in clapsGroups:
        if len(clapperGroup) > 1000 or len(clapperGroup) < 1:
            break  # If the group is too large, it's probably because every frame was added to the group. Skip.
        lastFrame = clapperGroup[-1]

        for idx in range(50):
            delta = clapperData[lastFrame + idx - 1] - clapperData[lastFrame + idx]
            if delta <= 0.005:
                clapDowns.append(lastFrame + idx + offset)
                break
    return clapDowns


def GetClaps(maxClumpSeperation=120):
    """Get the last frame in a clump of claps where the clapper board hits down, if a clapper board exists in the scene

    Args:
        maxClumpSeperation (int): The number of frames to clump groups of clumps together

    Returns:
        dict of Key:object value:list of int - object is the clapper board bone and list is each
                                               3rd clap frame found
    """
    clapsDict = {}
    for component in Globals.Components:
        # Check Type
        if not isinstance(component, mobu.FBModel):
            continue

        # Check name
        if component.Name not in ['SlateTop_bone', 'SlateClapperTop_bone']:
            continue

        rot = component.PropertyList.Find("Lcl Rotation")
        rotAnim = rot.GetAnimationNode()

        # Check it has an animation node (not 100% sure why it wouldn't)
        if rotAnim is None:
            continue

        for channel in rotAnim.Nodes:
            if str(channel.Name).lower() != "z":
                continue
            # Check to make sure it has at least 1 key
            if len(channel.FCurve.Keys) == 0:
                continue
            clapperDict = {}

            # Populate the clapperDict. Key: frame (int). Value: The rotation value of the channel per frame.
            for idx in xrange(channel.FCurve.Keys[0].Time.GetFrame(), channel.FCurve.Keys[-1].Time.GetFrame()):
                clapperDict[idx] = channel.FCurve.Evaluate(mobu.FBTime(0, 0, 0, idx))
            sortedClapperList = _AnalyzeClapperData(clapperDict)

            # Dynamically sort the claps into "clumps"
            clapParts = []
            currentClapPart = []
            for part in sortedClapperList:
                if len(currentClapPart) == 0:
                    currentClapPart.append(part)
                    continue
                if (part - currentClapPart[0]) <= maxClumpSeperation:
                    currentClapPart.append(part)
                else:
                    clapParts.append(currentClapPart)
                    currentClapPart = [part]
            clapParts.append(currentClapPart)

            # Now get the last one from each "clump"
            claps = [clapPart[-1] for clapPart in clapParts if len(clapPart) > 0]

            # Check we have claps to add
            if len(claps) > 0:
                # final dict
                clapsDict[component] = claps

    return clapsDict


def GetTakeSlateGroup():
    """Get the Take Slate Group

    Returns:
        FBGroup: The take slate MotionBuilder group
    """
    for group in Globals.gGroups:
        if group.Name == "PREVIZ_SLATE":
            return group


def DuplicateClapper(newName=None, newNamespace=None, createGroup=False):
    """Duplicate the scenes clapper board, giving the option to set a new name and or namespace
    as well as creating a new group for the new clapper. All animation is copied.

    Args:
        newName (str): The new name for the clapper board
        newNamespace (str): The new namespace to give each part of the clapper board
        createGroup (bool): If the new clapper board should be added to a new group

    Returns:
        list of FBModels: All the parts which make up the duplicated clapper board

    Notes:
        The clapper board constraint is not copied at this time
    """
    slateGroup = GetTakeSlateGroup()
    if slateGroup is None:
        raise ValueError("No Slate in scene")
    slateItems = [item for item in slateGroup.Items if isinstance(item, mobu.FBModel)]

    newGroup = None
    if createGroup:
        newGroup = mobu.FBGroup("{0}:PREVIZ_SLATE".format(newNamespace or "CLONE"))

    dupDict = {}
    for item in slateItems:
        itemName = newName or str(item.LongName)
        if newNamespace is None:
            newLongName = itemName
        else:
            newLongName = "{0}:{1}".format(newNamespace, itemName)

        newItem = copy.copy(item)
        newItem.LongName = newLongName
        dupDict[item] = newItem

        if newGroup is not None:
            newGroup.ConnectSrc(newItem)

    # Re-parent newBones to newBone equivalent of orgBones
    for item in slateItems:
        parItem = item.Parent
        if parItem is None:
            continue

        newItem = dupDict[item]
        newItemBone = dupDict.get(parItem, parItem)
        newItem.Parent = newItemBone

    for item in slateItems:
        newItem = dupDict[item]

        AnimationNode.CopyData(item.AnimationNode, newItem.AnimationNode)

        newItem.Translation = Vector.CloneVector(item.Translation)
        newItem.Rotation = Vector.CloneVector(item.Rotation)

    return dupDict.values()
