'''
 Script Path: RS/Core/Mocap/Previz/GrabWatchstarSetCoordinates.py
 Written And Maintained By: Kristine Middlemiss
 Created: 18 April 2014
 Description: Get the Watchstar Set Co-ordinates.
'''

from pyfbsdk import *
from pyfbsdk_additions import *
import RS.Tools.UI
import RS.Utils.UserPreferences as userPref
import RS.Core.Mocap.MocapCommands as mocapCommand
reload(mocapCommand)
reload(userPref)

class GrabSetCoordinatesFromWatchstar( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Grab Set Co-ordinates From Watchstar', size = [ 345, 172 ])
        self.UserDataObj = None
    
    def Create( self, pMain ):
 
        
def Run( show = True ):
    tool = GrabSetCoordinatesFromWatchstar()
    
    if show:
        tool.Show()
        
    return tool

Run()