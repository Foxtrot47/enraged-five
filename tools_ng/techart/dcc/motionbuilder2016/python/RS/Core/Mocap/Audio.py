from PySide import QtGui

from RS.Core.ReferenceSystem import Manager
from RS.Core.AnimData import Context
from RS.Tools.UI import Application


class NoWatchstarTrialForCurrentTake(Exception):
    pass


class NoAudioOnMotionScene(Exception):
    pass


class NoAudioOnTrialAndMotionScene(Exception):
    pass


class LoadAudio(object):
    @classmethod
    def loadAudioFromCurrentTrial(cls):
        trialID = Manager.Manager().TrialID
        if trialID in [-1, 0, None]:
            raise NoWatchstarTrialForCurrentTake("No Watchstar Trial for the current Motion Builder take found")
        
        trialContext = Context.animData.getTrialByID(trialID)
        cls.loadAudioFromTrial(trialContext)
    
    @staticmethod
    def _getAudioFromTrial(trialContext):
        audioType = Context.settingsManager.consts.FilePathTypes.FBX_AUDIO
        trialAudios = trialContext.getAllFiles(audioType)
        if trialAudios == []:
            motionScenes = []
            for select in trialContext.getDataDict().get("selects", []):
                motionSceneID = select.get("motionSceneID", None)
                if motionSceneID is not None:
                    motionScenes.append(trialContext.project().getCompositeByID(motionSceneID))
            
            if motionScenes != []:
                motionSceneAudio = []
                for scene in motionScenes:
                    motionSceneAudio.extend(scene.getAllFiles(audioType))
                if motionSceneAudio  != []:
                    return motionSceneAudio[0].filePath()
                else:
                    raise NoAudioOnMotionScene("No FBX Audio found on any motion scenes for '{}'".format(trialContext.name))
            else:
                raise NoAudioOnTrialAndMotionScene("No FBX Audio found on trial, no motion scene found for '{}'".format(trialContext.name))
        else:
            return trialAudios[0].filePath()
    @classmethod
    def loadAudioFromTrial(cls, trialContext):
        manager = Manager.Manager()
        manager.CreateReferences(cls._getAudioFromTrial(trialContext))


def Run(show=True):
    if show is False:
        LoadAudio.loadAudioFromCurrentTrial()
    else:
        messageboxName = "Load Audio From Trial"
        try:
            LoadAudio.loadAudioFromCurrentTrial()
            QtGui.QMessageBox.critical(Application.GetMainWindow(), messageboxName, "Audio Loaded!")
        except NoWatchstarTrialForCurrentTake, error:
            QtGui.QMessageBox.critical(Application.GetMainWindow(), messageboxName, error.message)
        except NoAudioOnMotionScene, error:
            QtGui.QMessageBox.critical(Application.GetMainWindow(), messageboxName, error.message)
        except NoAudioOnTrialAndMotionScene, error:
            QtGui.QMessageBox.critical(Application.GetMainWindow(), messageboxName, error.message)
