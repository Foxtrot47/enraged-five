'''

 Script Path: RS/Core/Mocap/Previz/CameraCleanupPartOne.py
 
 Written And Maintained By: Kristine Middlemiss
 
 Created: 3 January 2015
 
 Description: 

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand
reload(mocapCommand)

mocapCommand.CameraCleanupPartOne()