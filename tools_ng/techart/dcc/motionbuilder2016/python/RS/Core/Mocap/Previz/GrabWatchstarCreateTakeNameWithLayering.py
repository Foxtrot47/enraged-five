"""
Create take with layering
"""
from PySide import QtGui

from RS.Core.Camera import CamUtils
from RS.Core.Mocap import realtimeConnection
from RS.Tools.UI import Application


def GrabTakeWithLayeringHotKey():
    """
    Attempt to get the trial name from CaptureBoss and create a new take in MotionBuilder.

    Notes:
        Any changes to this function should be mirrored in the _handleCreateTakeWithLayerClicked method in:
            RS\Tools\VirtualProduction\widgets\duringShootWidget.py
    """
    toolTitle = "Virtual Production Toolbox"
    try:
        connection = realtimeConnection.RealtimeConnection()
        if not connection.isConnected():
            QtGui.QMessageBox.warning(
                Application.GetMainWindow(),
                toolTitle,
                "Connection to CaptureBoss has not been established - cannot create a new take!",
            )
            return
        newTakeName = connection.grabTakeName(True)
        if newTakeName is None:
            QtGui.QMessageBox.warning(
                Application.GetMainWindow(),
                toolTitle,
                "No active take for stage: {0}".format(realtimeConnection.RealtimeConnection().stage)
            )
            return
        CamUtils.HideOldCameraPlanes()
    except ValueError:
        QtGui.QMessageBox.warning(
                Application.GetMainWindow(),
                toolTitle,
                "Current stage is invalid; please set the mocap stage in the Virtual Production Tool!"
            )
    except realtimeConnection.TakeAlreadExistsException:
        QtGui.QMessageBox.warning(
                Application.GetMainWindow(),
                toolTitle,
                "Take already exists with the current trial name. You can't create a new take with the same name!"
            )


GrabTakeWithLayeringHotKey()
