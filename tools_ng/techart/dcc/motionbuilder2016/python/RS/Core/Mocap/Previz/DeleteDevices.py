'''

 Script Path: RS/Core/Mocap/Previz/DeleteDevices.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 08 October 2013
 
 Description: Launcher for DeleteDevices module from MocapCommands.py
              Turns off and deletes all devices from the scene

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the DeleteDevices module from MocapCommands.py
mocapCommand.DeleteDevices()