'''

 Script Path: RS/Core/Mocap/Previz/RTCRangeSave.py
 
 Maintained By: Kathryn Bodey
 
 Created: 28 November 2013
 
 Description: Launcher for RTCRangeSave module from MocapCommands.py
              Outputs current hard and soft ranges to a .txt file 

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the CamRockBake module from MocapCommands.py
mocapCommand.RTCRangeSave()