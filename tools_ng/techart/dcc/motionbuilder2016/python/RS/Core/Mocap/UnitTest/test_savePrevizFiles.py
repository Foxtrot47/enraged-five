import unittest

from RS.Core.Mocap import previzIO
from RS.Core.AnimData import Context
from RS.Core.AnimData._internal import contexts


def Run():
    """Run unittests from this module"""
    unittest.TextTestRunner(verbosity=0).run(suite())


def suite():
    """A suite to hold all the tests from this module

    Returns:
        unittest.TestSuite
    """
    testLoader = unittest.TestLoader()
    suite = unittest.TestSuite()
    testMethods = [
        "test_getCutSceneTrialNameTokens",
        "test_getCutSceneTrialOutputDir",
        "test_getSceneNameToken",
    ]
    testName = "RS.Core.Mocap.UnitTest.test_savePrevizFiles.TestGetTrialNameTokens.{}"
    for testMethod in testMethods:
        test = testLoader.loadTestsFromName(name=testName.format(testMethod))
        suite.addTests(test)
    return suite


class TestGetTrialNameTokens(unittest.TestCase):
    """Test parsing tokens for the CS trials."""

    mappings = {
        149382: {
            "missionAbbrev": "FIX_AGY",
            "featureName": "FIX_AGY_INT1",
            "shotName": "FIX_AGY_INT1_P1",
            "shotNameWithTakeNumber": "FIX_AGY_INT1_P1_T06",
            "outputDir": r"X:\gta5_dlc\mpPacks\mpSecurity\art\ng\animation\cutscene\!!scenes\FIX_AGY\FIX_AGY_INT1\FIX_AGY_INT1_P1\FIX_AGY_INT1_P1_T06",
        },  # GTA_5_DLC, Trial: 002812_06_BE_FIX_AGY_INT1_P1 ID#145450
        149524: {
            "missionAbbrev": "FIX_TRIP1",
            "featureName": "FIX_TRIP1_MCS2",
            "shotName": "FIX_TRIP1_MCS2_P1",
            "shotNameWithTakeNumber": "FIX_TRIP1_MCS2_P1_T01",
            "outputDir": r"X:\gta5_dlc\mpPacks\mpSecurity\art\ng\animation\cutscene\!!scenes\FIX_TRIP1\FIX_TRIP1_MCS2\FIX_TRIP1_MCS2_P1\FIX_TRIP1_MCS2_P1_T01",
        },  # GTA_5_DLC, Trial: 002843_01_BE_FIX_TRIP1_MCS2_P1 ID#145540
        # TODO: Get working mappings for Redemption2 DLC
        # 149382: {
        #     "missionAbbrev": "LAW_INT",
        #     "featureName": "LAW_INT",
        #     "shotName": "LAW_INT_P1",
        #     "shotNameWithTakeNumber": "LAW_INT_P1_T09",
        # },  # Redemption2_DLC, Trial: 000864_09_BE_LAW_INT_P1 ID#149382
    }

    def test_getCutSceneTrialNameTokens(self):
        """Test getting the name tokens from previzIO.GetCutSceneTrialNameTokens()."""
        for input, expectedResults in self.mappings.items():
            trial = Context.animData.getTrialByID(input)
            missionAbbrev, featureName, shotName, shotNameWithTakeNumber = previzIO.GetCutSceneTrialNameTokens(
                trial,
            )
            self.assertEqual(
                missionAbbrev,
                expectedResults.get("missionAbbrev"),
                "missionAbbrev tokens do not match for trial {}!\nActual: {}\n Expected: {}".format(
                    input, missionAbbrev, expectedResults.get("missionAbbrev")
                ),
            )
            self.assertEqual(
                featureName,
                expectedResults.get("featureName"),
                "featureName tokens do not match for trial {}!\nActual: {}\n Expected: {}".format(
                    input, featureName, expectedResults.get("featureName")
                ),
            )
            self.assertEqual(
                shotName,
                expectedResults.get("shotName"),
                "shotName tokens do not match for trial {}!\nActual: {}\n Expected: {}".format(
                    input, shotName, expectedResults.get("shotName")
                ),
            )
            self.assertEqual(
                shotNameWithTakeNumber,
                expectedResults.get("shotNameWithTakeNumber"),
                "shotNameWithTakeNumber tokens do not match for trial {}!\nActual: {}\n Expected: {}".format(
                    input, shotNameWithTakeNumber, expectedResults.get("shotNameWithTakeNumber")
                ),
            )

    def test_getCutSceneTrialOutputDir(self):
        """Test getting the outputDir returned from previzIO.GetFolderPathFromTake()."""
        for input, expectedResults in self.mappings.items():
            trial = Context.animData.getTrialByID(input)
            outputDir = previzIO.GetFolderPathFromTake(trial)
            self.assertEqual(
                outputDir,
                expectedResults.get("outputDir"),
                "outputDir does not match for trial {}!\nActual: {}\n Expected: {}".format(
                    input, outputDir, expectedResults.get("outputDir")
                ),
            )

    def test_getSceneNameToken(self):
        """Testing parsing the scene info from the Watchstar feature name with previzIO.GetSceneNameToken()."""
        expectedResults = {
            "INT_AIR_IG1_ARRIVE_AT_SLAUGHTERHOUSE": {
                "scene_name": "INT_AIR",
                "splitter_token": "IG1",
                "remaining": "ARRIVE_AT_SLAUGHTERHOUSE",
            },  # FeatureID#13009
            "SWS_MGBAG_IG1_EXAMINE": {
                "scene_name": "SWS_MGBAG",
                "splitter_token": "IG1",
                "remaining": "EXAMINE",
            },  # FeatureID#11398
            "UP_BCHBM_IG1_PREACHER": {
                "scene_name": "UP_BCHBM",
                "splitter_token": "IG1",
                "remaining": "PREACHER",
            },  # FeatureID#11374
            "INT_CHASE_IG3_CAMP_SING": {
                "scene_name": "INT_CHASE",
                "splitter_token": "IG3",
                "remaining": "CAMP_SING",
            },  # FeatureID#11369
            "CSC_DRT_JBOFF_IG1_INVITE": {
                "scene_name": "CSC_DRT_JBOFF",
                "splitter_token": "IG1",
                "remaining": "INVITE",
            },  # FeatureID#13013
        }
        for input, expectedResult in expectedResults.items():
            sceneName, splitterToken, remaining = previzIO.GetSceneNameToken(input)
            self.assertEqual(
                sceneName,
                expectedResult.get("scene_name"),
                "SceneName tokens do not match for feature {}!\nActual: {}\n Expected: {}".format(
                    input, sceneName, expectedResult.get("scene_name")
                ),
            )
            self.assertEqual(
                splitterToken,
                expectedResult.get("splitter_token"),
                "SceneName tokens do not match for feature {}!\nActual: {}\n Expected: {}".format(
                    input, splitterToken, expectedResult.get("splitter_token")
                ),
            )
            self.assertEqual(
                remaining,
                expectedResult.get("remaining"),
                "SceneName tokens do not match for feature {}!\nActual: {}\n Expected: {}".format(
                    input, remaining, expectedResult.get("remaining")
                ),
            )


if __name__ == "__builtin__":
    # Run this in MotionBuilder.
    from RS.Core.Mocap.UnitTest import test_savePrevizFiles
    reload(test_savePrevizFiles)
    test_savePrevizFiles.Run()
