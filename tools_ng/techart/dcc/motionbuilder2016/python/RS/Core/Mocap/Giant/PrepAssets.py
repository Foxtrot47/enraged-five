'''

 Script Path: RS/Core/Mocap/Giant/PrepAssets.py

 Written And Maintained By: Kathryn Bodey

 Updated: 22 October 2013

 Description: Functions for prepping assets for Giant-use, for UI

 Functions: RemoveMatTexVid, KeyFramesGiantPrep
            PrepGiantAnimals, PrepGiantCharacters, PrepGiantSets,
            PrepGiantProps, PrepGiantVehicles

'''

from pyfbsdk import *

import RS.Utils.Path
import RS.Utils.Scene
import RS.Core.AssetSetup.Lib
import RS.Core.AssetSetup.Characters
import RS.Core.AssetSetup.Vehicles
import RS.Globals

from RS.Utils.Scene import Cleanup, Component


######################################################
# Cleanup Definitions
######################################################
#delete materials, textures & video
def RemoveMatTexVid():

    deleteList = []

    for material in RS.Globals.gMaterials:
        deleteList.append(material)

    for texture in RS.Globals.gTextures:
        deleteList.append(texture)

    for shader in RS.Globals.gShaders:
        deleteList.append(shader)

    for video in RS.Globals.gVideoClips:
        deleteList.append(video)

    for item in deleteList:
        item.FBDelete()

    # remove unused components
    deleteList = Cleanup.GetUnusedComponentsList()
    Component.Delete(deleteList)

#delete excess nulls, not part of main hierarchy
def RemoveExcessNulls( parentDummy ):

    hierarchyList= []

    #get main hierarchy
    RS.Utils.Scene.GetChildren(parentDummy, hierarchyList)

    #select full hierarchy
    RS.Utils.Scene.DeSelectAll()
    for item in hierarchyList:
        item.Selected = True

    #get list of unselected
    unSelectedList = FBModelList()
    FBGetSelectedModels (unSelectedList, None, False)

    #delete items in unselected list
    for item in unSelectedList:
        item.FBDelete()

    RS.Utils.Scene.DeSelectAll()


######################################################
# Key Add
######################################################
#add keys to bones (frames 0, 1 & 2)
def KeyFramesGiantPrep(parentNull):

    nullList = []

    RS.Utils.Scene.GetChildren(parentNull, nullList)

    for null in nullList:

        null.Translation.SetAnimated(True)
        null.Rotation.SetAnimated(True)

        trnsX = null.Translation.Data[0]
        trnsY = null.Translation.Data[1]
        trnsZ = null.Translation.Data[2]
        rotX = null.Rotation.Data[0]
        rotY = null.Rotation.Data[1]
        rotZ = null.Rotation.Data[2]

        trnsAnimNode = null.Translation.GetAnimationNode()
        rotAnimNode = null.Rotation.GetAnimationNode()

        FBPlayerControl().Goto(FBTime(0,0,0, 0))

        trnsAnimNode.Nodes[0].KeyAdd(trnsX)
        trnsAnimNode.Nodes[1].KeyAdd(trnsY)
        trnsAnimNode.Nodes[2].KeyAdd(trnsZ)
        rotAnimNode.Nodes[0].KeyAdd(rotX)
        rotAnimNode.Nodes[1].KeyAdd(rotY)
        rotAnimNode.Nodes[2].KeyAdd(rotZ)

        FBPlayerControl().Goto(FBTime(0,0,0, 1))

        trnsAnimNode.Nodes[0].KeyAdd(trnsX)
        trnsAnimNode.Nodes[1].KeyAdd(trnsY)
        trnsAnimNode.Nodes[2].KeyAdd(trnsZ)
        rotAnimNode.Nodes[0].KeyAdd(rotX)
        rotAnimNode.Nodes[1].KeyAdd(rotY)
        rotAnimNode.Nodes[2].KeyAdd(rotZ)

        FBPlayerControl().Goto(FBTime(0,0,0, 2))

        trnsAnimNode.Nodes[0].KeyAdd(trnsX)
        trnsAnimNode.Nodes[1].KeyAdd(trnsY)
        trnsAnimNode.Nodes[2].KeyAdd(trnsZ)
        rotAnimNode.Nodes[0].KeyAdd(rotX)
        rotAnimNode.Nodes[1].KeyAdd(rotY)
        rotAnimNode.Nodes[2].KeyAdd(rotZ)

        FBSystem().Scene.Evaluate()


######################################################
# Animal Prep
######################################################
def PrepGiantAnimals(control , event):

    fileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    FBSystem().CurrentTake.ClearAllProperties(False)

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    RS.Core.AssetSetup.Lib.rs_AssetSetupDeleteItems()

    if RS.Utils.Scene.FindModelByName('Dummy01'):
        dummy = RS.Utils.Scene.FindModelByName('Dummy01')
    else:
        dummy = RS.Utils.Scene.FindModelByName('CTRLRIG_Pelvis_AUX')

    geo = RS.Utils.Scene.FindModelByName(fileName)

    KeyFramesGiantPrep(dummy)

    geo.Parent = dummy

    namespaceList = []

    RS.Utils.Scene.GetChildren(dummy, namespaceList)

    for null in namespaceList:
        null.Name = "gs:" + fileName + ":" + null.Name

    RemoveMatTexVid()

    RemoveExcessNulls( dummy )


######################################################
# Character Prep
######################################################
def PrepGiantCharacters(control, event):

    fileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    FBSystem().CurrentTake.ClearAllProperties(False)

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    dummy = RS.Utils.Scene.FindModelByName("Dummy01")
    skelRoot = RS.Utils.Scene.FindModelByName("SKEL_ROOT")
    mover = RS.Utils.Scene.FindModelByName("mover")

    for component in RS.Globals.gComponents:
        if fileName.lower() == component.Name.lower():
            geo = component
        elif 'geometry' == component.Name.lower():
            geo = component

    FBDisconnect(mover, dummy)
    dummy.Rotation = FBVector3d(-90,0,0)
    FBConnect(mover, dummy)
    mover.Rotation = FBVector3d(0,0,0)

    geo.Parent = dummy

    mover.Selected = True
    mover.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    mover.Selected = False
    mover.PropertyList.Find("Visibility").Data = False

    dummy.PropertyList.Find("Pre Rotation").Data = FBVector3d(0,0,0)

    deleteList = []

    for component in RS.Globals.gComponents:
        if component.ClassName() == 'FBModel':
            if "nub" in component.Name.lower():
                deleteList.append(component)
            if "ph" in component.Name.lower() and "hand" in component.Name.lower():
                deleteList.append(component)
            if "ik" in component.Name.lower() and "hand" in component.Name.lower():
                deleteList.append(component)

    for item in deleteList:
        item.FBDelete()

    skelList = []

    RS.Utils.Scene.GetChildren(skelRoot, skelList)

    for skel in skelList:
        RS.Core.AssetSetup.Characters.rs_PrepJointsCharacterSetup(skel, True)
        FBSystem().Scene.Evaluate()

    character = FBCharacter(fileName)

    RS.Utils.Scene.AssignJoints(character)

    KeyFramesGiantPrep(skelRoot)

    RemoveMatTexVid()

    namespaceList = []

    RS.Utils.Scene.GetChildren(dummy, namespaceList)

    for item in namespaceList:
        item.Name = "gs:" + fileName + ":" + item.Name

    RemoveExcessNulls( dummy )


######################################################
# Environment Prep
######################################################
def PrepGiantSets(control, event):

    fileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    FBSystem().CurrentTake.ClearAllProperties(False)

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    dummy = RS.Utils.Scene.FindModelByName('SetRoot')

    KeyFramesGiantPrep(dummy)

    namespaceList = []

    RS.Utils.Scene.GetChildren(dummy, namespaceList)

    for null in namespaceList:
        null.Name = "gs:" + fileName + ":" + null.Name

    RemoveExcessNulls( dummy )


######################################################
# Prop Prep
######################################################
def PrepGiantProps(control, event):

    fileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    FBSystem().CurrentTake.ClearAllProperties(False)

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    sceneList = FBModelList()
    FBGetSelectedModels (sceneList, None, False)
    FBGetSelectedModels (sceneList, None, True)

    for component in sceneList:
        if "mover" in component.Name.lower():
            mover = component
            mover.Name = "mover"
        elif "dummy" in component.Name.lower():
            dummy = component
            dummy.Name = "Dummy01"
        elif fileName.lower() == component.Name.lower():
            geo = component

    mover.Selected = True
    mover.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    mover.Selected = False

    if geo.Parent == None:
        geo.Parent = dummy

    KeyFramesGiantPrep(dummy)

    namespaceList = []
    RS.Utils.Scene.GetChildren(dummy, namespaceList)

    for null in namespaceList:
        null.Name = "gs:" + fileName + ":" + null.Name

    RemoveMatTexVid()

    RS.Core.AssetSetup.Lib.rs_AssetSetupDeleteItems()

    RemoveExcessNulls( dummy )


######################################################
# Vehicle Prep
######################################################
def PrepGiantVehicles(pControl, pEvent):

    fileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    FBSystem().CurrentTake.ClearAllProperties(False)

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    sceneList = FBModelList()
    FBGetSelectedModels (sceneList, None, False)
    FBGetSelectedModels (sceneList, None, True)

    for component in sceneList:
        if "_skel" in component.Name:
            mover = component
            mover.Name = "mover"
        elif "mover" in component.Name:
            mover = component
        elif "_skin" in component.Name:
            geo = component
        elif "dummy" in component.Name.lower():
            if "chassis" in component.Name.lower():
                None
            else:
                dummy = component

    geo.Parent = dummy

    FBDisconnect(mover, dummy)
    dummy.Rotation = FBVector3d(-90,0,0)
    FBConnect(mover, dummy)
    mover.Rotation = FBVector3d(0,0,0)

    namespaceList = []
    RS.Utils.Scene.GetChildren(dummy, namespaceList)


    for null in namespaceList:
        null.Name = "gs:" + fileName + ":" + null.Name

    RS.Core.AssetSetup.Vehicles.rs_DeleteItemsVehicleSetup()

    RS.Core.AssetSetup.Vehicles.VehicleSetupDuplicateWheel()

    KeyFramesGiantPrep(dummy)

    trnsShader = FBShaderLighted('Window_Lighted')
    trnsShader.Transparency = FBAlphaSource.kFBAlphaSourceAccurateAlpha
    geo.Shaders.append(trnsShader)
    geo.DisplayMode = FBModelShadingMode.kFBModelShadingAll

    RemoveMatTexVid()

    RemoveExcessNulls( dummy )
