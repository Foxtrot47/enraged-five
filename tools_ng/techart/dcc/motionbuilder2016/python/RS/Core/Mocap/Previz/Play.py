'''

 Script Path: RS/Core/Mocap/Previz/Play.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 07 October 2013
 
 Description: Launcher for Play module from MocapCommands.py
              Plays the timeline via player controls

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the Play module from MocapCommands.py
mocapCommand.Play()