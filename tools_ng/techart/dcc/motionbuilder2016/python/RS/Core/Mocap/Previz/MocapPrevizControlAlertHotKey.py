'''
Message Box for Previz computer to assing to a hotkey.
Used to inform connected computers that previz requires control.
'''

from PySide import QtGui
import RS.Config.Script.Path.ToolImages


class DialogWithIcon(QtGui.QMessageBox):
    def __init__(self, parent= None):
        super(DialogWithIcon, self).__init__()
        self.dialog()

    def dialog(self):
        font = QtGui.QFont()
        font.setPointSize(12)
        self.setText("\n\nMocap Previz Requires Control of Motionbuilder.")
        self.setWindowTitle("Ah Ah Ah")
        self.setIconPixmap(QtGui.QPixmap("{0}\\Mocap\\ahahah.png".format( RS.Config.Script.Path.ToolImages )))
        self.setFont(font)
        self.setStandardButtons(QtGui.QMessageBox.Ok)

form = DialogWithIcon()
form.show()
