"""
Library for setting up a character for export to Giant Land conversion. Has all the steps needed
as well as a method called PrepCharacter which will process the current scene in the correct order.

This is a linux friendly script, but does require the RelationshipConstraintManager module, which is
pure python as well
"""
import os
import logging
import re

from PySide import QtGui

import pyfbsdk as mobu

from RS.Utils.Scene import RelationshipConstraintManager as rcm
from RS import ProjectData


class BoneSettings(object):
    """
    Struct class to Store bone local transformation before constraint are disabled.
    """
    def __init__(self, matrix, bone):
        self.matrix = matrix
        self.bone = bone


class _RollBoneContainer(object):
    """
    Struct class to allow for easier moving of rollbone, driver and constraint data around.
    """
    def __init__(self, rollbone, driverbone, constraint):
        self.rollBone = rollbone
        self.driverBone = driverbone
        self.constraint = constraint


def RestoreJointSettings(jointSettings):
    mobu.FBSystem().Scene.Evaluate()

    for jointSetting in jointSettings:
        if jointSetting.bone is None:
            continue

        jointSetting.bone.SetMatrix(jointSetting.matrix)

    mobu.FBSystem().Scene.Evaluate()


def SaveJointSettings():
    mobu.FBSystem().Scene.Evaluate()

    skelRoot = mobu.FBFindModelByLabelName('SKEL_ROOT')
    characterBones = GetAllChildren(skelRoot)

    jointSettings = []
    for joint in characterBones:
        currentMatrix = mobu.FBMatrix()
        joint.GetMatrix(currentMatrix)

        jointSettings.append(BoneSettings(currentMatrix, joint))

    return jointSettings


def FixForeArmHelperJoints():
    jointArray = [[['MH_{side}_Wrist', 'RB_{side}_Wrist01'], 'SKEL_{side}_Hand']]#,
                  #[['MH_{side}_Elbow'], 'SKEL_{side}_Forearm']]

    for sideParameter in ['L', 'R']:
        for jointData in jointArray:
            targetParent = FindModelByName(jointData[1].format(side=sideParameter))

            if targetParent is None:
                continue

            for skinJointName in jointData[0]:
                skinJoint = FindModelByName(skinJointName.format(side=sideParameter))

                if skinJoint is None:
                    continue

                skinJoint.Parent = targetParent


def FixLegHelperJoints():
    dummyRoot = FindModelByName('Dummy01')
    if dummyRoot is None:
        return

    characterBones = GetAllChildren(dummyRoot)

    rollBones = [bone for bone in characterBones 
                 if ('Roll' in bone.Name and not bone.Name.endswith('_NUB'))
                 or ( bone.Name.startswith('RB') and not bone.Name.endswith('_Nub'))
                 or ('Roll' in bone.Name and not bone.Name.endswith('_NUB'))
                 or ( bone.Name.startswith('RB') and not bone.Name.endswith('_Nub'))]

    legJoints = [bone for bone in rollBones if bone.Parent.Name == 'SKEL_Pelvis' ]

    if len(legJoints) == 0:
        return

    leftLeg = FindModelByName('SKEL_L_Thigh')
    rightLeg = FindModelByName('SKEL_R_Thigh')

    if leftLeg is None or rightLeg is None:
        return

    mobu.FBSystem().Scene.Evaluate()

    jointPositions = []
    for rollBone in legJoints:
        jointPosition = mobu.FBVector3d()
        rollBone.GetVector(jointPosition)
        jointPositions.append(jointPosition)

    leftLegPosition = mobu.FBVector3d()
    rightLegPosition = mobu.FBVector3d()

    leftLeg.GetVector(leftLegPosition)
    rightLeg.GetVector(rightLegPosition)

    for jointIndex, rollBone in enumerate(legJoints):
        leftJointDistance = (jointPositions[jointIndex] - leftLegPosition).Length()
        if leftJointDistance < 0.001:
            rollBone.Parent = leftLeg

        rightJointDistance = (jointPositions[jointIndex] - rightLegPosition).Length()
        if rightJointDistance < 0.001:
            rollBone.Parent = rightLeg


def RemoveScaleJoints():
    mobu.FBSystem().Scene.Evaluate()

    dummyRoot = FindModelByName('Dummy01')
    if dummyRoot is None:
        return

    characterBones = GetAllChildren(dummyRoot)
    scaleBones = [bone for bone in characterBones if 'Scale' in bone.Name or 'SCALE' in bone.Name]

    for joint in scaleBones:
        jointChildren = GetAllChildren(joint)

        for bone in jointChildren:
            bone.Parent = joint.Parent

    mobu.FBSystem().Scene.Evaluate()
    for joint in scaleBones:
        joint.Selected = True


def GetAllRollBonesConstraints():
    """
    Return a list of Roll bone constraints as managed relation ship class
    """
    allCons = rcm.GetAllRelationshipConstraints()
    return [con for con in allCons if con.Name.startswith("RB_")]


def GetAllRollBoneDrives():
    """
    Method to get which bone is driving which roll bone so it can be re-parented and the constraint
    that drives it

    returns:
        a dict of rollbones (key) and a RollBoneContainer, which has the rollbone, driver bone
        and the constraint which binds them.
    """
    allRolls = GetAllRollBonesConstraints()
    returnDict = {}
    for constraint in allRolls:
        # The outputBox should be a rollbone item
        for outputBox in constraint.GetRecievingOnlyBoxes():
            # Resolve driver bone, we ideally only want one bone out of this
            driverBones = list(set([node.GetMobuObject() for node in outputBox.GetInputDrivers()
                                 if node.GetMobuObject() != outputBox.GetMobuObject()]))
            if len(driverBones) > 1:
                logging.warning("More than one driving bone for %s" % outputBox.Name)
                continue
            if len(driverBones) == 0:
                logging.warning("No Driver bones for %s" % outputBox.Name)
                continue

            mobuObject = outputBox.GetMobuObject()
            driverObject = driverBones[0]

            # Create a container for all the data
            rollSolution = _RollBoneContainer(mobuObject, driverObject, constraint)

            # Check for multiple drivers of the same bone
            currentDictValue = returnDict.get(mobuObject)
            if currentDictValue is None:
                returnDict[mobuObject] = rollSolution
            elif currentDictValue.driverBone != driverObject :
                logging.warning("Multiple driver bones for %s" % outputBox.Name)
                continue
    return returnDict


def ReparentRollBones():
    """
    Method get all the rollbones, and re-parent them to the bone which drives them
    """
    # Regex to get the arm bone name from the rollbone name
    rollBoneRegex = "\ARB_([a-zA-Z_0-9*]*)Arm[Rr]oll\Z"
    boneDict = GetAllRollBoneDrives()
    for rollBoneContainer in boneDict.itervalues():

        rollBone = rollBoneContainer.rollBone
        driverBone = rollBoneContainer.driverBone

        # Skip if arm bone
        if len(re.compile(rollBoneRegex, 0).findall(rollBone.Name)) != 0:
            logging.info("Arm Detected for bone {0}, Skipping".format(rollBone.Name))
            continue

        # Skip if already parented
        if rollBone.Parent == driverBone:
            logging.info('{0} is already Parented under Driver ({0})'.format(
                                                                             rollBone.Name,
                                                                             driverBone.Name)
                         )
            continue

        logging.debug("parenting '{0}' under '{1}'".format(rollBone.Name, driverBone.Name))
        rollBone.Parent = driverBone

    # Turn off all roll bone constraints
    for con in GetAllRollBonesConstraints():
        con.GetConstraint().Active = False

    #Bug 3092691 allow leg helper joint to follow thigh bones
    FixLegHelperJoints()

    #Bug 3092691 make sure helper joint on the forearm are place at the correct place
    FixForeArmHelperJoints()


def GetAllChildren(element, currentList=None):
    """
    Recursive method to get all the child items from an object

    args:
        element (FBModel): Object to get the children from

    kwargs:
        currentList (list of FBModels): The internal list of all found models.

    returns:
        list of FBModels: All the models found as children of the top element
    """
    if currentList is None:
        currentList = []
    currentList.append(element)
    for child in element.Children:
        currentList = GetAllChildren(child, currentList)
    return currentList


def FindModelByName(name, includeNamespace=False):
    """
    Mimics the deprecated behavior of mobu.FBFindModelByName.

    args:
        name: The model name to search.

    returns:
        The found model, None otherwise.
    """
    componentList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(name, componentList, includeNamespace, True)

    if len(componentList) > 0:
        return componentList[0]

    else:
        return None


def Align(pModel, pAlignTo, pAlignTransX=True, pAlignTransY=True, pAlignTransZ=True, pAlignRotX=True, pAlignRotY=True,
          pAlignRotZ=True):
    """
    Aligns 2 objects together, aligns the trns & rot for XYZ axis

    Arguments:
        pModel (FBModel): component to align
        pAlignTo (FBModel): component to align to
        pAlignTransX (boolean): align the x translation
        pAlignTransY (boolean): align the x translation
        pAlignTransZ (boolean): align the x translation
        pAlignRotX (boolean): align the x rotation
        pAlignRotY (boolean): align the x rotation
        pAlignRotZ (boolean): align the x rotation
    """

    lAlignTransPos = mobu.FBVector3d()
    lModelTransPos = mobu.FBVector3d()
    lAlignRotPos = mobu.FBVector3d()
    lModelRotPos = mobu.FBVector3d()

    pAlignTo.GetVector(lAlignTransPos)
    pModel.GetVector(lModelTransPos)

    pAlignTo.GetVector(lAlignRotPos, mobu.FBModelTransformationType.kModelRotation)
    pModel.GetVector(lModelRotPos, mobu.FBModelTransformationType.kModelRotation)

    if pAlignTransX:
        lModelTransPos[0] = lAlignTransPos[0]
    if pAlignTransY:
        lModelTransPos[1] = lAlignTransPos[1]
    if pAlignTransZ:
        lModelTransPos[2] = lAlignTransPos[2]
    if pAlignRotX:
        lModelRotPos[0] = lAlignRotPos[0]
    if pAlignRotY:
        lModelRotPos[1] = lAlignRotPos[1]
    if pAlignRotZ:
        lModelRotPos[2] = lAlignRotPos[2]

    pModel.SetVector(lModelTransPos)
    pModel.SetVector(lModelRotPos, mobu.FBModelTransformationType.kModelRotation)


def _getDummy():
    """
    Internal Method

    Get the Dummy object

    returns:
        FBModel of the Dummy01
    """
    return FindModelByName("Dummy01")


def _getSkelRoot():
    """
    Internal Method

    Get the Skeleton Root object

    returns:
        FBModel of the SKEL_ROOT
    """
    return FindModelByName("SKEL_ROOT")


def _getMover():
    """
    Internal Method

    Get the Mover object

    returns:
        FBModel of the Mover
    """
    return FindModelByName("mover")


def _getBaseGeo():
    """
    Internal Method

    Get the The Base Geo object

    returns:
        FBModel of the Mover
    """
    return FindModelByName(_getFilename())


def _getFacialRoot():
    """
    Internal Method

    Get the facial Root object

    returns:
        FBModel of the facial Root
    """
    facialRoot = FindModelByName("FACIAL_facialRoot")
    if facialRoot is None:
        facialRoot = FindModelByName("FACIAL_C_FacialRoot")
    return facialRoot


def _getFilename():
    """
    Internal Method

    Get the base name of the file name

    returns:
        string of the file name
    """
    return os.path.splitext(os.path.basename(mobu.FBApplication().FBXFileName))[0]


def StripRedudantElements():
    """
    Strip out all redudant elemetns from the scene such as Materials, Textures, Shaders and
    Video clips
    """
    scene = mobu.FBSystem().Scene
    for itemGrp in [
                    [obj for obj in scene.Materials],
                    [obj for obj in scene.Textures],
                    [obj for obj in scene.Shaders],
                    [obj for obj in scene.VideoClips]
                    ]:
        for item in itemGrp:
            item.FBDelete()


def RemoveFaceControls():
    """
    Remove all face controls from the scene, and all their children
    """
    topLevelFacialControls = ["faceControls_OFF", "FacialAttrGUI", "head"]
    for controlName in topLevelFacialControls:
        control = FindModelByName(controlName)
        if control is None:
            continue
        for item in GetAllChildren(control):
            item.FBDelete()


def DeleteContacts():
    """
    Delete all IK and PH contact in the hands and any nub bones
    """
    scene = mobu.FBSystem().Scene
    for component in scene.Components:
        if isinstance(component, mobu.FBModel):
            if "nub" in component.Name.lower():
                component.FBDelete()
                continue
            if "ph" in component.Name.lower() and "hand" in component.Name.lower():
                component.FBDelete()
                continue
            if "ik" in component.Name.lower() and "hand" in component.Name.lower():
                component.FBDelete()
                continue


def ReparentRootNode():
    """
    Pre-Rotate the Root Node and mover seprarely so its facing the correct way
    """
    dummy = _getDummy()
    mover = _getMover()
    filename = _getFilename()
    geo = None

    scene = mobu.FBSystem().Scene
    for component in scene.Components:
        if filename.lower() == component.Name.lower():
            geo = component
        elif 'geometry' == component.Name.lower():
            geo = component

    mobu.FBDisconnect(mover, dummy)
    # Only needed on GTA - Add ProjectData code when released
    # dummy.Rotation = mobu.FBVector3d(-90, 0, 0)
    mobu.FBConnect(mover, dummy)
    # mover.Rotation = mobu.FBVector3d(0, 0, 0)

    if geo is not None:
        geo.Parent = dummy

    mover.Selected = True
    mover.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
    mover.Selected = False
    mover.PropertyList.Find("Visibility").Data = False
    dummy.PropertyList.Find("Pre Rotation").Data = mobu.FBVector3d(0, 0, 0)


def RotateThumbs():
    """
    Rotate the thumbs in the scene so their facing up rather than outwards
    """
    # rotate Left Thumb by -90 on X
    leftThumb = FindModelByName("SKEL_L_Finger00")
    value = leftThumb.PropertyList.Find("Rotation (Lcl)").Data
    leftThumb.PropertyList.Find("Rotation (Lcl)").Data = mobu.FBVector3d(-90, value[1], value[2])

    # rotate Right Thumb by 90 on X
    rightThumb = FindModelByName("SKEL_R_Finger00")
    value = rightThumb.PropertyList.Find("Rotation (Lcl)").Data
    rightThumb.PropertyList.Find("Rotation (Lcl)").Data = mobu.FBVector3d(90, value[1], value[2])


def FilenameWithoutCostumeName():
    """
    Get the filename without the costume part of the name
    """
    # Remove costume name part
    regex = "(-[a-zA-Z0-9_]+[^_vs$])"
    return re.sub(regex, "", _getFilename())


def StripCostumeName():
    """
    Remove the Costume Name from other parts of the file, including geo
    """
    baseGeo = _getBaseGeo()
    if baseGeo is not None:
        baseGeo.Name ="{0}_gs".format(FilenameWithoutCostumeName())


def AddPostfixName():
    """
    Update the scene objects by adding the character _gs name as a namespace
    """
    filename = FilenameWithoutCostumeName()

    scene = mobu.FBSystem().Scene
    for character in scene.Characters:
        character.Name = "{0}_gs".format(filename)

    for item in GetAllChildren(_getDummy()):
        item.LongName = ":".join(["gs:", "{0}_gs".format(filename), item.LongName])


def RelinkFaceNode():
    """
    Create a new Face Root node, taking the children and parent of the orginal one, as well as
    the transforms.
    This is to remove any custom attributes found on the root face bone added by faceware.
    """
    transformSets = ["Translation (Lcl)", "Rotation (Lcl)", "Scaling (Lcl)"]

    # Get the parts we need
    skelHead = FindModelByName("SKEL_Head")
    facialRoot = _getFacialRoot()

    facialRoot.Name = "ORG_FACIAL_facialRoot"
    # Add and reparent the new part
    newFacialRoot = mobu.FBModelSkeleton("FACIAL_facialRoot")


    Align(newFacialRoot, facialRoot)
    newFacialRoot.Parent = skelHead

    # Reparent its child to the new one
    for child in [node for node in facialRoot.Children]:
        child.Parent = newFacialRoot

    facialRoot.FBDelete()


def CreateNewTake():
    """
    Create a new ExportTake to use to export out the file, and remove all other takes in the file
    """
    system = mobu.FBSystem()
    system.CurrentTake.ClearAllProperties(False)

    newTake = mobu.FBTake("ExportTake")
    system.Scene.Takes.append(newTake)
    system.CurrentTake = newTake
    for take in [tk for tk in system.Scene.Takes]:
        if take != system.CurrentTake:
            take.FBDelete()


def AddKeysForExport():
    """
    Set the frame range to only 2 frames, and add keys for each objected in the scene for those 2
    frames
    """
    system = mobu.FBSystem()
    newTimeSpan = mobu.FBTimeSpan(mobu.FBTime(0, 0, 0, 0), mobu.FBTime(0, 0, 0, 2))
    system.CurrentTake.LocalTimeSpan = newTimeSpan

    for item in GetAllChildren(_getSkelRoot()):
        item.Translation.SetAnimated(True)
        item.Rotation.SetAnimated(True)

        trnsData = [data for data in item.Translation.Data]
        rotData = [data for data in item.Rotation.Data]

        trnsAnimNode = item.Translation.GetAnimationNode()
        rotAnimNode = item.Rotation.GetAnimationNode()

        for frIdx in xrange(3):
            mobu.FBPlayerControl().Goto(mobu.FBTime(0, 0, 0, frIdx))

            for idx in xrange(3):
                trnsAnimNode.Nodes[idx].KeyAdd(trnsData[idx])
                rotAnimNode.Nodes[idx].KeyAdd(rotData[idx])
            mobu.FBSystem().Scene.Evaluate()


def RemoveAllDevices():
    """
    Remove all devices in the scene.

    Issues where Faceware plugins will cause saving the fbx scene to crash
    """
    scene = mobu.FBSystem().Scene
    for device in [device for device in scene.Devices]:
        device.FBDelete()


def TurnOffConstraints():
    """
    Turn off all constraints in the scene
    """
    scene = mobu.FBSystem().Scene
    for con in scene.Constraints:
        con.Active = False


def SaveCharacter():
    """
    Save the fbx in the project data path for GS Asset Prep Skeletons.

    THIS METHOD IS NOT LINUX SAFE AND SHOULD BE REMOVED
    """
    filePath = _getOutputFile()
    if not os.path.exists(os.path.dirname(filePath)):
        os.makedirs(os.path.dirname(filePath))
    mobu.FBApplication().FileSave(filePath)


def _getOutputFile():
    """
    Get the output folder for the GS Asset Prep Skeletons.

    THIS METHOD IS NOT LINUX SAFE AND SHOULD BE REMOVED
    """
    outputFolder = ProjectData.data.GetGsAssetPrepSkeletonPath()
    return os.path.join(outputFolder, "{0}.fbx".format(FilenameWithoutCostumeName()))


def GotAllPieces():
    """
    Checks all the peices are in the file
    
    Return:
        a list of pieces which are missing
    """
    errors = []
    if _getDummy() is None: errors.append("Missing Dummy")
    if _getSkelRoot() is None: errors.append("Missing Skel Root")
    if _getMover() is None: errors.append("Missing Mover")
    return errors


def PrepCharacter():
    """
    Main method to prep the character for Giant land, where we run through in ORDER all the steps
    needed for a file we can convert.
    """
    CreateNewTake()
    StripRedudantElements()
    RemoveFaceControls()
    DeleteContacts()

    #Bug 3092691 fix shoulder volume loss when solver 
    #and constraint are disabled
    jointSettings = SaveJointSettings()

    TurnOffConstraints()

    #Bug 3092691 fix shoulder volume loss 
    RestoreJointSettings(jointSettings)

    ReparentRollBones()

    #Bug 3092691 Remove scale bones
    RemoveScaleJoints()

    ReparentRootNode()
    RotateThumbs()
    RemoveAllDevices()
    if _getFacialRoot() is not None:
        RelinkFaceNode()
    AddKeysForExport()
    AddPostfixName()
    StripCostumeName()
    SaveCharacter()


def Run(show=True):
    errors = GotAllPieces()
    if len(errors) > 0:
        if show:
            QtGui.QMessageBox.critical(None, "Prep Character", "Unable to prep chacter.\nPlease re-Resource Character!\nErrors:{0}".format("\n - ".join(errors)))
            return
        raise ValueError("Unable to prep chacter. Errors:{0}".format(", ".join(errors)))
    PrepCharacter()

    if show:
        QtGui.QMessageBox.information(None, "Prep Character", "Character has been prepped for Gaint and output to '{0}'".format(_getOutputFile()))
