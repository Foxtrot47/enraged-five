'''

 Script Path: RS/Core/Mocap/Previz/RecordOverwritePlay.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 07 October 2013
 
 Description: Launcher for Play module from MocapCommands.py
              Records, overwrites take and plays the timeline via player controls

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the RecordOverwritePlay module from MocapCommands.py
mocapCommand.RecordOverwritePlay()