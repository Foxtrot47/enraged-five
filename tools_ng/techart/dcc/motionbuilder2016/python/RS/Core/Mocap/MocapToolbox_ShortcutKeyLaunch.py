'''

 Script Path: RS\Core\Mocap\MocapToolbox_ShortcutKeyLaunch.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 10 December 2014
 
 Description: Launcher for the Mocap Toolbox, for Kelly to assign as a hotkey

'''

from pyfbsdk import *

from RS.Tools.VirtualProduction import run

#launch for mocap toolbox
run.Run()