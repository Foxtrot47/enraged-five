"""
Set the audio path based off the current watchstar take name
"""
from PySide import QtGui

from RS.Core.Mocap import AudioRecording, realtimeConnection


def SetAudioPath():
    realtime = realtimeConnection.RealtimeConnection()
    if realtime.stage is None:
        QtGui.QMessageBox.information(
                                      None,
                                      "Virtual Production Toolbox",
                                      "No Valid set, Please set the current Mocap Shoothing stage in the Virtual Production Tool"
                                      )
        return
    try:
        AudioRecording.SetAudioRecordingFilePath()
    except ValueError, expMsg:
        QtGui.QMessageBox.information(
                                      None,
                                      "Virtual Production Toolbox",
                                      str(expMsg)
                                      )
        return
    except realtimeConnection.TakeAlreadExistsException:
        QtGui.QMessageBox.information(
                                      None,
                                      "Virtual Production Toolbox",
                                      "Take Already exists with the same name, you cant create a new take with the same name"
                                      )
        return

SetAudioPath()
