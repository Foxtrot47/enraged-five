'''

 Script Path: RS/Core/Mocap/Previz/PlotMocapAssets.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 15 October 2013
 
 Description: Launcher for PlotMocapAssets module from MocapCommands.py
              Plots all assets part of the Ref System Null

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommands

reload( mocapCommands )

#calling the PlotMocapAssets module from MocapCommands.py
mocapCommands.PlotMocapAssets()