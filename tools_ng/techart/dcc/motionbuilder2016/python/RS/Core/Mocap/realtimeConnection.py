import os
import traceback

import pyfbsdk as mobu

from PySide import QtCore, QtGui

from RS import Globals
from RS.Core.AnimData import Context
from RS.Tools.CameraToolBox.PyCore import singleton
from RS.Core.Yoke import client
from RS.Utils.Logging import Universal
from RS.Core.ReferenceSystem import Manager


class TakeAlreadExistsException(Exception):
    pass


class MessageIndex(object):
    ComputerName = 0
    TrialName = 1
    RunCommand = 2
    RunFile = 3

    TrialId = 4
    ManualTrialName = 5

    RunRawPythonWithReply = 98
    RunRawPython = 99
    
    RunRawPythonReplyValue = 101


class CommandIndex(object):
    RecordAndPlay = 0
    StopAndRewind = 1
    Play = 2
    Stop = 3
    NextTake = 4
    
    RecordStart = 20
    RecordStop = 21
    RecordState = 22
    
    AddGiantDevice = 30
    RemoveGiantDevice = 31
    TurnOnGiantDevice = 32
    TurnOffGiantDevice = 33
    GiantDeviceOnlineStatus = 34


class PySideSingleton(type(QtCore.QObject), singleton.Singleton):
    """
    Qt Aware Singleton Class Object
    """
    pass


class RealtimeConnection(QtCore.QObject):
    """
    Class to connect to Realtime from Motionbuilder
    """
    __metaclass__ = PySideSingleton

    realTimeConnected = QtCore.Signal()
    realTimeDisconnected = QtCore.Signal()
    messageRecived = QtCore.Signal()
    takeNameUpdated = QtCore.Signal(object) #Deprecated
    autoTakeUpdated = QtCore.Signal(object)
    manualTakeUpdated = QtCore.Signal(object, object, object)
    trialDoesNotExist = QtCore.Signal(int)

    def __init__(self, stage=None):
        """
        Constructor
        """
        super(RealtimeConnection, self).__init__()
        self._trialName = None
        self._currentTrial = None
        self._log = Universal.UniversalLog("{0}_Mobu_RealtimeConnection".format(os.getpid()), True, False) 
        self._boxName = "Motionbuilder (Previs) ({0})".format(os.environ['COMPUTERNAME'])

        self._settings = QtCore.QSettings("RockstarSettings", "MocapRealtimeConnection")
        lastStageId = self._settings.value("stageId")
        lastIPAddress = self._settings.value("realtimeIpAddress")

        lastStage = None
        if lastStageId is not None:
            lastStage = Context.animData.getStageFromID(lastStageId)
        self._stage = stage or lastStage or self._getLocalStage()
        stageIpAddress = ""

        if self._stage is not None:
            stageIpAddress = self._stage.captureWorkstationIp()

        self._currentIpAddress = lastIPAddress or stageIpAddress

        self._client = client.YokeClient('0.0.0.0', port=8988)
        self._client.serverConnected.connect(self._handleServerConnected)
        self._client.serverDisconnected.connect(self._handleServerDisconnected)
        self._client.messageRecived.connect(self._handleMessageRecived)

    def currentIpAddress(self):
        return self._currentIpAddress

    def disconnectFromHost(self):
        self._client.disconnect()

    def connectToHost(self, ipAddress):
        self._currentIpAddress = ipAddress
        self._client.setAddress(ipAddress)
        self._settings.setValue("realtimeIpAddress", ipAddress)

    def _getLocalStage(self):
        currentLoc = Context.animData.getCurrentLocation()
        if currentLoc is None:
            return None
        stages = currentLoc.getStages()
        if len(stages) == 0:
            return None
        return stages[0]

    def isConnected(self):
        return self._client.isConnected()

    @property
    def stage(self):
        return self._stage

    @stage.setter
    def stage(self, stage):
        self._stage = stage
        stageId = None
        if stage is not None:
            stageId = stage.stageId
        self._settings.setValue("stageId", stageId)

    def _addToClipBoard(self, text):
        clipboard = QtGui.QApplication.clipboard()
        clipboard.setText(text)

    def grabTake(self):
        """
        Grab the current Take from the set stage
        """
        if self._stage is None:
            return None

        stageTrial = Context.animData.getCurrentTrialByStage(self._stage)
        if stageTrial is None:
            return None
        
        if self._trialName is not None:
            trials = stageTrial.project().getTrialsByRegex(self._trialName)
            if len(trials) == 1:
                return trials[0]
        return stageTrial

    def grabRawTakeName(self):
        """
        Get the 'raw' trial/take name.

        Returns:
             str: The trial/take name as listed in Watchstar
        """
        if self._trialName is not None:
            return self._trialName
        take = self.grabTake()
        if take is None:
            return None
        return str(take.name)

    def grabTakeName(self, layer=False, rawName=False):
        """
        Grab the current Take name from the set stage
        """
        if self._stage is None:
            return None

        trialName = self.grabRawTakeName()
        if trialName is None:
            return None

        if rawName:
            self._addToClipBoard(trialName)
            return trialName
        
        for take in Globals.System.Scene.Takes:
            if take.Name == trialName:
                raise TakeAlreadExistsException("Unable to add take, Take Name Already Exists!")
        
        if layer:
            Globals.System.CurrentTake.CopyTake(trialName)
        else:
            newTake = mobu.FBTake(trialName)
            Globals.System.Scene.Takes.append(newTake)
            Globals.System.CurrentTake = newTake

        # Copy into the Windows Clip Board so people can copy to a bug and Rag
        self._addToClipBoard(trialName)
        if self._currentTrial is not None:
            Manager.Manager().TrialID = self._currentTrial.captureId 
        return trialName

    def _updateTrial(self, newTrialName):
        self._trialName = str(newTrialName)
        self.takeNameUpdated.emit(self._trialName)

    def _runCommand(self, commandIdx):
        if commandIdx == CommandIndex.RecordAndPlay:
            mobu.FBPlayerControl().Record(True, False)
            mobu.FBPlayerControl().Play()
            pass
        elif commandIdx == CommandIndex.StopAndRewind:
            mobu.FBPlayerControl().Stop()
            mobu.FBPlayerControl().GotoStart()

        elif commandIdx == CommandIndex.Play:
            mobu.FBPlayerControl().Play()

        elif commandIdx == CommandIndex.Stop:
            mobu.FBPlayerControl().Stop()

        elif commandIdx == CommandIndex.NextTake:
            takeIdx = [tk for tk in mobu.FBSystem().Scene.Takes].index(mobu.FBSystem().CurrentTake)
            takeIdx += 1
            if takeIdx > len(mobu.FBSystem().Scene.Takes) - 1:
                takeIdx = 0
            mobu.FBSystem().CurrentTake = mobu.FBSystem().Scene.Takes[takeIdx]
        else:
            print "An error occurred!"
            print "Unknown command: '{0}'".format(commandIdx)

    def _executeCode(self, code):
        """
        Internal Method

        Args:
            code (str): The code to execute
        """
        try:
            exec(str(code))
            return None
        except Exception, ex:
            print "An Error Occured"
            print traceback.format_exc()
            self._log.LogWarning("An error occurred running code:\n\n{0}\n\nwith the following traceback:\n\n{1}".format(str(code), traceback.format_exc()))
            return "ERROR: {0}".format(traceback.format_exc()) 

    def currentTrial(self):
        """
        Get the current trial, is a watchstar trial is being used
        """
        return self._currentTrial
    
    def _setCurrentAutoTrial(self, trial):
        self._currentTrial = trial
        self._updateTrial(trial.name)
        self.autoTakeUpdated.emit(trial)

    def _setCurrentManualTrial(self, project, session, trialName):
        self._currentTrial = None
        self._updateTrial(trialName)
        self.manualTakeUpdated.emit(project, session, trialName)

    def _handleServerConnected(self):
        """
        Internal Method

        Handle Server connection
        """
        self.realTimeConnected.emit()
        self._client.sendMessage(MessageIndex.TrialId, "")
        self._client.sendMessage(MessageIndex.ManualTrialName, "")

    def _handleServerDisconnected(self):
        """
        Internal Method

        Handle Server disconnection
        """
        self.realTimeDisconnected.emit()

    def _handleMessageRecived(self, msg):
        """
        Internal Method

        Handle Receiving a message from the server
        """
        if msg.messageType == MessageIndex.ComputerName:
            self._client.sendMessage(MessageIndex.ComputerName, self._boxName)
        elif msg.messageType == MessageIndex.TrialId:
            data = msg.messageContents
            if data is not None:
                trialId = int(data)
                try:
                    trial = Context.animData.getTrialByID(trialId)
                except Exception as error:
                    print "[DEBUG] An error occurred when searching for trial with ID: {}".format(trialId)
                    trial = None
                if trial is not None:
                    self._setCurrentAutoTrial(trial)
                else:
                    self.trialDoesNotExist.emit(trialId)
        elif msg.messageType == MessageIndex.ManualTrialName:
            data = msg.messageContents
            project, session, trialName = data
            if trialName is not None:
                self._setCurrentManualTrial(str(project), str(session), str(trialName))
        elif msg.messageType == MessageIndex.RunCommand:
            self._runCommand(msg.messageContents)
        elif msg.messageType in [MessageIndex.RunRawPython, MessageIndex.RunRawPythonWithReply]:
            returnValue = self._executeCode(str(msg.messageContents))
            if msg.messageType == MessageIndex.RunRawPythonWithReply:
                self._client.sendMessage(MessageIndex.RunRawPythonReplyValue, returnValue)
