"""Methods for getting and saving FBX filepaths for trials in the Virtual Production Toolbox.
This is primarily used when saving FBX files in the Shoot: During Shoot tab.

IMPORTANT: This file is different per project! Be careful when submitting so that you don't
overwrite changes with other projects' code!

The FBX file structure is different per project and is always subject to change per project
depending on the requests of Production.

See RS.Core.Mocap.Unittest.test_savePrevizFiles for examples of paths and usages.
"""
import os
import re
import glob

import pyfbsdk as mobu

from RS.Core import DatabaseConnection
from RS.Core.AnimData import Context
from RS.Core.AnimData._internal import contexts
from RS.Core.Mocap import MocapCommands
from RS import Globals, Perforce


def GetSceneNameToken(featureName):
    """Parse out the scene name from the Watchstar feature name.

    This method is currently unused, but here in case we every need it.

    Args:
        featureName (str): The Watchstar feature name.

    Returns:
        str

    Notes:
        * CS (INT, EXT, MSC) and IG (IG#) compatible.
    """
    pattern = "(?P<scene_name>[a-zA-Z0-9_]+)_(?P<splitter_token>IG[0-9]|INT|EXT|MCS)_(?P<remaining>[a-zA-Z0-9_]+)"
    results = re.search(pattern, featureName)

    if results is None:
        raise ValueError("Unable to parse name: {}".format(featureName))

    sceneName = results.group("scene_name")
    splitterToken = results.group("splitter_token")
    remaining = results.group("remaining")

    return (sceneName, splitterToken, remaining)


def GetCutSceneTrialNameTokens(trial):
    """Tokenize the trial name, returning only the take number and the shot name.

    Args:
        trial (contexts.CutSceneTrial): The trial the needs its name parsed.

    Returns:
        tuple of str: (mission abbreviation, feature name, shot name, shot name with take number)

    Raises:
        ValueError: If the trial name is not able to be parsed.

    Warnings:
        * This code matches the Americas project but is different from other projects. The code differences are to
            account for variation in the file structure.
    """
    if not isinstance(trial, contexts.CutSceneTrial):
        raise ValueError("Unsupported type '{}': Unable to parse name for trial: {}".format(type(trial), trial.name))

    parts = trial.getNameTokens(trial.name)
    shotName = parts.get("shot_name")
    takeNumber = parts.get("take_number")
    featureName = trial.shot().featureName()
    partName = shotName.replace(featureName, "").strip("_")
    takeNumberName = "T{}".format(takeNumber)
    shotNameWithTakeNumber = "{}_{}".format(shotName, takeNumberName)

    # NOTE - How to get mission abbreviation
    # * The mission abbreviation added to Watchstar almost never matches the desire abbreviation in the filepath.
    #   Instead, we have stored the desired abbreviations in the additional data to be accessed based on cutscene name.
    # * If the cutscene doesn't have an abbreviation set in the additional data, just grab whatever value is stored
    #   in the Watchstar missionAbbrev field for the trial.
    projectAdditionalData = trial.project().additionalData()
    missionAbbreviationData = projectAdditionalData.get("missionAbbrevOverride", {})
    missionAbbreviation = missionAbbreviationData.get(featureName, trial.shot().missionAbbrev())

    sceneName = featureName.replace(missionAbbreviation, "").strip("_")

    return (missionAbbreviation, sceneName, partName, takeNumberName, shotNameWithTakeNumber)


def GetFolderPathFromTake(trialContext):
    """Get the folder path for a given AnimData.MotionTrialBase trial

    Args:
        trialContext (MotionTrialBase): The trial to get the folder for

    Returns:
        str: path for its folder on disk

    Raises:
        ValueError: If unable to generate the folder path from the take.
    """
    # Dont allow ROM's, Scans or Facial trials go any further
    if not isinstance(trialContext, contexts.MotionTrialBase):
        raise ValueError("Unable to generate folder path from trial '{}'. Bad trial type".format(trialContext.name))

    # Two different scenarios requiring separate logic: Cutscene and InGame
    # * CutScene: Watchstar calculates the folder path.
    # * InGame: Folder is based on shoot name.
    # TODO bugstar://7977058 - [VirtualProductionToolbox] Update IG save path logic to use the watchstar logic as CS currently does
    projectConfig = trialContext.projectConfig()

    # Cutscene (layered)
    if isinstance(trialContext, contexts.CutSceneTrial):
        outputPath = trialContext.getFormulaBasedCompositePath()
        try:
            outputPath = os.path.dirname(Perforce.GetLocalPaths([outputPath])[0])
        except Exception as error:
            outputPath = ""
    # InGame
    else:
        sceneRootDepotPath = projectConfig.getIGFbxSceneRootPath()
        if not sceneRootDepotPath:
            project = trialContext.project()
            msg = "IG scene root path is not set in the '{}' project config '{}'. " \
                  "Unable to generate folder path from trial '{}'.".format(project.name, projectConfig.name,
                                                                           trialContext.name)
            raise ValueError(msg)
        sceneRootLocalPaths = Perforce.GetLocalPaths([sceneRootDepotPath])
        if not sceneRootLocalPaths:
            raise ValueError(
                "Could not get any local paths for: {} (Please check your p4v mappings!) " \
                "Unable to generate folder path from trial '{}'.".format(sceneRootDepotPath, trialContext.name)
            )
        sceneRootLocalPath = sceneRootLocalPaths[0]
        # This output path differs from newer project's logic due to requests from animation.
        outputPath = os.path.join(sceneRootLocalPath, "DLC{}".format(trialContext.shoot().name.replace(" ", "")))

    return outputPath


def SaveTake(take, p4ClNumber, overwriteFbx=False):
    """Save a separate file for the provided take, with folder naming based on
    metadata from the Watchstar trial and add p4 path to Watchstar.

    Args:
        take (pyfbsdk.FBTake): Take that is to be saved.
        p4ClNumber (str): The number of the Perforce changelist to add file to.
        overwriteFbx (bool): If True, will overwrite existing FBX file for take.

    Returns:
        str: File system path to saved .fbx file.

    Raises:
        IOError: If the file already exists.

    Warnings:
        * This method has to set the scene's current take a minimum of two times,
        potentially triggering unwanted callbacks.

    Notes:
        * The directory path is based on the take's trial context.
        * If the trial is both a cutscene trial and a "layered take", it must be
        saved into the directory of the trial it was shot into, AKA: its parent
        trial. To specify the parent trial, we present a dialog for the user to
        make that selection.
        * FBX SCENE paths aren't stored on the trial in Watchstar; creating a new
        trial select creates the motion scene + adds the path.
    """
    # Store current take before making changes.
    usersCurrentTake = Globals.System.CurrentTake

    # Set take as the scene's current take.
    Globals.System.CurrentTake = take

    # Get the trial context object in order to access data from Watchstar.
    trialContext = MocapCommands.GetTrialFromTake(take)

    # Refresh trial data in case the layer fields have been updated.
    trialContext = trialContext.project().getTrialByID(trialContext.captureId)

    # Get target file path for .fbx from context data.
    dirPath = GetFolderPathFromTake(trialContext)
    basename = take.Name
    if isinstance(trialContext, contexts.CutSceneTrial):
        _, _, _, _, shotNameWithTakeNumber = GetCutSceneTrialNameTokens(trialContext)
        basename = shotNameWithTakeNumber
    fileName = "{}.fbx".format(basename)
    filePath = str(os.path.join(dirPath, fileName))
    if os.path.exists(filePath) and not overwriteFbx:
        raise IOError("File already exists: {}".format(filePath))

    # Set the FBX options before saving.
    fbxOpts = mobu.FBFbxOptions(False)  # Default save (all elements, anim).
    fbxOpts.EmbedMedia = False  # Important not to ever embed media.
    for tkIdx, take in enumerate(Globals.Scene.Takes):
        selectTake = False
        if take == Globals.System.CurrentTake:
            selectTake = True
        fbxOpts.SetTakeSelect(tkIdx, selectTake)

    # Store scene file name before saving.
    sceneFileName = Globals.Application.FBXFileName

    # Write to disk.
    if not os.path.exists(dirPath):
        os.makedirs(dirPath)
    Globals.Application.FileSave(filePath, fbxOpts)

    # Reset the scene's current take.
    Globals.System.CurrentTake = usersCurrentTake

    # Restore scene file name to prevent accidental overwriting via ctrl+s, etc.
    Globals.Application.FBXFileName = sceneFileName

    # Add fbx and audio to p4 changelist.
    newP4ClPaths = glob.glob(filePath)
    # The audio file naming doesn't match the fbx on cutscene trials.
    newP4ClPaths.extend(glob.glob(os.path.join(dirPath, "{}.wav".format(take.Name))))
    if newP4ClPaths:
        Perforce.AddOrEditFiles(newP4ClPaths, changelistNum=p4ClNumber, force=True)

    # Add depot path to the Watchstar trial.
    fileState = Perforce.GetFileState(filePath)
    if isinstance(fileState, list):
        raise ValueError("fileState: '{}'".format(fileState))
    # Invert the result of Perforce.FixFilePath to prevent errors in Watchstar.
    depotFilePath = fileState.DepotFilename
    fbxScenePath = Perforce.FixFilePath(depotFilePath, invert=True)
    trialContext.addNewFile(Context.settingsManager.consts.FilePathTypes.FBX_SCENE, fbxScenePath)
    try:
        notes = "Auto-Generated by the Virtual Production Toolbox"
        if isinstance(trialContext, contexts.CutSceneTrial):
            trialContext.addFormulaBasedComposite(notes)
        else:
            trialContext.addComposite(fbxScenePath, notes)
    except DatabaseConnection.ConnectionError as error:
        if not "MOTION SCENE WITH THIS NAME ALREADY EXISTS" in str(error):  # This is expected when re-saving the take.
            raise  # Reraise everything else.
    return filePath
