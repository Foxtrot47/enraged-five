'''

 Script Path: RS/Core/Mocap/Previz/RTEBaseSetup.py
 
 Written And Maintained By: Kristine Middlemiss
 
 Created: 3 January 2015
 
 Description: 

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand
reload(mocapCommand)

'''
Prompts the user to enter a how many cameras they want to create. Returns the number
if the user confirms the create operation, or None if the user
cancels.
'''    
(button, cameraNumber) = FBMessageBoxGetUserValue('Creating Cameras','How many Cameras would you like to create?', 6, FBPopupInputType.kFBPopupInt,'Create','Cancel')

mocapCommand.RTEBaseSetup(cameraNumber if button == 1 else None)