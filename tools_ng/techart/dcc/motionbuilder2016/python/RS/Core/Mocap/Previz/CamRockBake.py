"""
Plots down RTC cameras to new RS cameras.
I simplified this to use the newer code in CamUtils.
-BlakeBuck 3/4/19
"""

from RS.Core.Camera import CamUtils
CamUtils.DupeRtcsToRsCams()
