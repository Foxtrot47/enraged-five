'''

 Script Path: RS/Core/Mocap/Previz/gsSkelMatchMerge.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 18 November 2013
 
 Description: Launcher for gsSkelMatchMerge module from MocapCommands.py
              Matches characters with existing gs skeletons in mocap P4, 
              merges them in and activates inputs

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand

reload( mocapCommand )


#calling the gsSkelMatchMerge module from MocapCommands.py
mocapCommand.gsSkelMatchMerge()