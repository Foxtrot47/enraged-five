import os

import pyfbsdk as mobu

from RS.Core.Mocap import realtimeConnection, previzIO


def SetAudioRecordingFilePath(stage=None):
    """
    Set the audio recording file path based off what is in watchstar
    """
    connection = realtimeConnection.RealtimeConnection()
    if stage is not None:
        connection.stage = stage

    audioInputs = mobu.FBSystem().AudioInputs
    # Check we have a single audio input device to use
    if len(audioInputs) == 0:
        raise ValueError("Unable to detect any audio input devices")

    audioInput = audioInputs[0]

    # Get the take
    take = connection.grabTake()
    if take is None:
        raise ValueError("Unable to find source current Take from Stage '{0}'".format(str(connection.stage)))

    # Get the folder name from the take
    folderPath = previzIO.GetFolderPathFromTake(take)

    # Create the final output file path with the .wav file ext
    outputPath = str(os.path.join(folderPath, "{}.wav".format(take.name)))

    print("Setting path to '{0}' for '{1}'".format(outputPath, take))

    dirName = os.path.dirname(outputPath)
    if not os.path.exists(dirName):
        os.makedirs(os.path.dirname(outputPath))

    # Set the path to record to
    audioInput.SetOnline(True)
    audioInput.PrepareToRecord(outputPath)

    return outputPath
