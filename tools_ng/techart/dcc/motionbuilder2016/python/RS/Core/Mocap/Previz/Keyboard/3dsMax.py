'''

 Script Path: RS/Core/Mocap/Previz/Keybaord/3ds Max.py
 
 Written And Maintained By: Kristine Middlemiss
 
 Created: 31 December 2013
 
 Description: Launcher for ChangeKeyboardConfiguration function from MocapCommands.py
              Switched the Keyboard preferences to for 3ds Max

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand

mocapCommand.ChangeKeyboardConfiguration("3ds Max")