'''

 Script Path: RS/Core/Mocap/Previz/RTEBaseSetup.py
 
 Written And Maintained By: Kristine Middlemiss
 
 Created: 26 June 2014
 
 Description: 

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand
reload(mocapCommand)

'''
Prompts the user to enter a how many cameras they want to create. Returns the number
if the user confirms the create operation, or None if the user
cancels.
'''    
print mocapCommand.PreVizSetup()
