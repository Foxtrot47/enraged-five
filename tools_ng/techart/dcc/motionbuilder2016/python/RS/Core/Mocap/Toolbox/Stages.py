import pyfbsdk as mobu

import os

from RS import Config, Globals, Perforce
from RS.Utils.Scene import Component
from RS.Utils import Scene
from RS.Tools.VirtualProduction import const


def rs_MocapStages(userInputName, userInputColor):
    '''
    Creates a Stage Object and sets properties

    Args:
        userInputName (string): name of the stage
        userInputColor (vector): color vector to apply to stage and handle

    Returns:
        Stage object (FBModel)
    '''
    stagePlane = None

    # get stage texture path
    texturePath = os.path.join(Config.VirtualProduction.Path.Previz,
                               const.PrevizFolder().GetStudioFolderName(),
                               'standardTextures',
                               'stage01_texture.tif')
    Perforce.Sync(texturePath)

    # get stage path
    stagePath = os.path.join(Config.VirtualProduction.Path.Previz,
                             const.PrevizFolder().GetStudioFolderName(),
                             'stages',
                             'stage01.fbx')
    Perforce.Sync(stagePath)

    # get city name
    cityString = const.PrevizFolder().GetCityLocation()

    # merge options
    options = mobu.FBFbxOptions(True)
    options.NamespaceList = "PlaceholderNamespace"
    options.CamerasAnimation = False
    options.BaseCameras = False
    options.CurrentCameraSettings = False
    options.CameraSwitcherSettings = False

    # merge stage into file
    if os.path.isfile(stagePath):
        mobu.FBApplication().FileMerge(stagePath, False, options)
        # get list of stage items
        stageItemList = Component.GetComponents("PlaceholderNamespace")
        for component in stageItemList:
            # remove namespace
            component.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBRemoveAllNamespace, '', '')
            # add user defined name
            if isinstance(component, mobu.FBModel):
                component.Name = userInputName
                # define a variable for the stage model - stage plane is the only FBModel merged in
                stagePlane = component
            elif isinstance(component, mobu.FBMaterial):
                component.Name = userInputName
                component.Diffuse = userInputColor

    if stagePlane is None:
        return

    # create materials and textures and set to stage
    stagePlaneMat = mobu.FBMaterial(userInputName)
    stagePlaneMat.Diffuse = userInputColor
    stagePlane.Materials.append(stagePlaneMat)
    mobu.FBFolder(userInputName, stagePlaneMat)
    stagePlaneTex = mobu.FBTexture(userInputName)
    stagePlaneTex.Video = mobu.FBVideoClip(texturePath)
    stagePlaneMat.SetTexture(stagePlaneTex, mobu.FBMaterialTextureType.kFBMaterialTextureDiffuse)
    mobu.FBFolder(userInputName, stagePlaneTex)
    # set groups
    group = mobu.FBGroup(userInputName)
    group.ConnectSrc(stagePlane)

    # set handle
    handle = mobu.FBHandle(userInputName)
    handle.Follow.append(stagePlane)
    handle.Manipulate.append(stagePlane)
    handle.PropertyList.Find("Label").Data = userInputName
    handle.PropertyList.Find("2D Display Color").Data = userInputColor
    handle.PropertyList.Find("Auto-Size to Viewer").Data = True 
    handle.PropertyList.Find("Translation Offset").Data = mobu.FBVector3d(0,500,0)

    # Stage Indication
    stageProperty = stagePlane.PropertyCreate('Stages', mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)
    stageProperty.Data = cityString

    # Active Stage Indication
    stageProperty = stagePlane.PropertyCreate('Active Stage', mobu.FBPropertyType.kFBPT_bool, "Bool", False, True, None)
    stageProperty.Data = False

    # create up-vector for decks connection
    upVectorMarker = mobu.FBModelMarker(userInputName + '_upVector')
    upVectorMarker.Size = 200
    upVectorMarker.Look = mobu.FBMarkerLook.kFBMarkerLookCube
    Scene.Align(upVectorMarker, stagePlane)
    mobu.FBSystem().Scene.Evaluate()
    upVectorMarker.Translation = mobu.FBVector3d(upVectorMarker.Translation.Data[0],
                                                 upVectorMarker.Translation.Data[1] + 5,
                                                 upVectorMarker.Translation.Data[2])
    upVectorMarker.Parent = stagePlane

    # locking some transform axis, enable rot and set rot order to YZX url:bugstar:2225950
    stagePlane.Rotation.SetMemberLocked(0 , True)
    stagePlane.Rotation.SetMemberLocked(1 , False)
    stagePlane.Rotation.SetMemberLocked(2 , True)
    stagePlane.Scaling.SetMemberLocked(0 , True)
    stagePlane.Scaling.SetMemberLocked(1 , True)
    stagePlane.Scaling.SetMemberLocked(2 , True)
    stagePlane.PropertyList.Find("Enable Rotation DOF").Data = True
    stagePlane.PropertyList.Find("Rotation Order").Data = 2

    return stagePlane