'''

 Script Path: RS/Core/Mocap/Previz/AddBasePrevizProperty.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 10 October 2013
 
Description: Launcher for AddBasePrevizProperty module from MocapCommands.py
             Applies 'BasePreviz' custom property to all objects in file

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the AddBasePrevizProperty module from MocapCommands.py
mocapCommand.AddBasePrevizProperty()