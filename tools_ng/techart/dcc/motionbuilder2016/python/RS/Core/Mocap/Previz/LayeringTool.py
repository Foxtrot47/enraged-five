"""
Libaray for layering motion capture Animations for use on set
"""
import os
import copy

import pyfbsdk as mobu
from PySide import QtGui

from RS import Config, Globals, Perforce
from RS.Utils.Scene import AnimationNode, Component, Character, Groups
from RS.Core.ReferenceSystem.Types.MocapSlate import MocapSlate
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.AssetSetup.Resource import GroupCleanup
from RS.Core.Mocap import Clapper, MocapCommands
from RS.Utils import Namespace, Scene, Vector


class CharacterAnimationLayerContainer(object):
    """
    Container class for the Character Animation layer
    """
    def __init__(self, characterReference, gender, colour):
        '''
        Constructor

        args:
            characterReference (Reference Object): The Reference System Character
            gender (str): The gender as a string. e.g. 'male', 'female'
            colour (list of floats): The RGB colour (scale from 0.0 to 1.0)
        '''
        super(CharacterAnimationLayerContainer, self).__init__()
        self._characterReference = characterReference
        self._gender = gender
        self._colour = colour
        
    def colour(self):
        return self._colour
    
    def gender(self):
        return self._gender
    
    def characterReference(self):
        return self._characterReference


class AnimationLayering(object):
    """
    Logic and methods to layer animation in Motion Builder, as well as class methods to get the
    clap frames and duplicate characters
    """
    @classmethod
    def GetTopGroup(cls, layerName):
        '''
        Searches for the main clone group, if it doesnt exist it will create one and return the
        FBGroup component. If it does exist, it will return the existing FBGroup component.

        Args:
        layerName (string): The layer name to use to namespace the characters layer

        Returns:
        FBGroup: Cloned ref top group
        '''
        topGroup = None
        topGroupList = [group for group in Globals.Groups if group.LongName == "{0}:LayerGroup".format(layerName)]
        if len(topGroupList) > 0:
            return topGroupList[0]
        if topGroup is None:
            topGroup = mobu.FBGroup("LayerGroup")
            topGroup.LongName = "{0}:{1}".format(layerName, topGroup.LongName)
        return topGroup

    @classmethod
    def cloneReferenceAndCopyAnimation(cls, reference, layerName, groupCreate=True, referenceModel=False, gender=None, colour=None):
        '''
        Creates a new reference with the same path as the original passes reference. Updates the
        name to have an additional namespace prefix with the current take name.
        Copies animation from originalr eference to new reference.
        Adds cloned references to a new group, to keep them organised together.

        Args:
            reference: Mananger Reference variable
            layerName(string): The layer name to use to namespace the characters layer
        
        Kwargs:
            groupCreate (bool): If a group should be created
            referenceModel ():
            gender ():
            colour ():
        '''
        manager = Manager()
        if referenceModel is True:
            # create a new reference with a ref pose
            if gender == 'Female':
                # create female ref pose reference
                femaleRefPosePath = MocapCommands.GetFemaleRefPosePath()
                cloneReference = manager.CreateReferences(femaleRefPosePath)
            else:
                # create male ref pose reference
                maleRefPosePath = MocapCommands.GetMaleRefPosePath()
                cloneReference = manager.CreateReferences(maleRefPosePath)

            if cloneReference is None or gender is None:
                return

            # rename new reference
            newNamespace = manager.ChangeNamespace(cloneReference[0], "{0}:{1}".format(layerName, reference.Namespace), 
                                                   includeIteration=True)

            # get characters to do an animation transfer
            # original character component
            originalCharacter = None
            namespace = Globals.Scene.NamespaceGet(reference.Namespace)
            originalCharacterList = Namespace.GetContentsByType(namespace, mobu.FBCharacter)
            if len(originalCharacterList) > 0:
                originalCharacter = originalCharacterList[0]

            # ref character component
            refCharacter = None
            namespace = Globals.Scene.NamespaceGet(newNamespace)
            characterTargetList = Namespace.GetContentsByType(namespace, mobu.FBCharacter)
            if len(characterTargetList) > 0:
                refCharacter = characterTargetList[0]

            if all([originalCharacter, refCharacter]):
                # transfer animation across
                refCharacter.InputCharacter = originalCharacter
                refCharacter.InputType = (mobu.FBCharacterInputType.kFBCharacterInputCharacter)
                refCharacter.ActiveInput = True
                refCharacter.PropertyList.Find('Match Source').Data = True

                plotOptions = mobu.FBPlotOptions()
                plotOptions.PlotAllTakes = False
                plotOptions.PlotOnFrame = True
                plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll
                plotOptions.UseConstantKeyReducer = False
                plotOptions.ConstantKeyReducerKeepOneKey = False
                plotOptions.PlotTranslationOnRootOnly = True

                refCharacter.PlotAnimation(mobu.FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, plotOptions)

                refCharacter.ActiveInput = False

        else:
            # create a new reference with a model character pose
            cloneReference = manager.CreateReferences(reference.Path)

            if cloneReference is None:
                return

            # rename new reference
            newNamespace = manager.ChangeNamespace(cloneReference[0], "{0}:{1}".format(layerName, reference.Namespace), 
                                                   includeIteration=True)

            # get componenet match list
            # Only copy on FBBox derived objects to avoid dialog boxes appearing [2630472]
            sourceComponents = [component for component in Globals.Components
                                if isinstance(component, mobu.FBBox)
                                and Component.GetNamespace(component) == reference.Namespace]

            targetComponents = [component for component in Globals.Components
                                if isinstance(component, mobu.FBBox)
                                and Component.GetNamespace(component) == newNamespace]

            componentMatchList = Component.GetComponentMatchList(sourceComponents, targetComponents)
            # clone the animation to new asset
            Component.CopyAnimationByNamespace(componentMatchList)

        if colour is not None:
            red = colour[0]
            green = colour[1]
            blue = colour[2]
            namespace = Globals.Scene.NamespaceGet(newNamespace)
            for refComp in Namespace.GetContents(namespace):
                if isinstance(refComp, mobu.FBNamespace):
                    continue

                if refComp.ClassName() == 'FBMaterial':
                    refComp.Diffuse = mobu.FBColor(red, green, blue)
                    refComp.Ambient = mobu.FBColor(red, green, blue)
                    refComp.Specular = mobu.FBColor(0, 0, 0)

        # add to clone group
        if groupCreate is True:
            topGroup = cls.GetTopGroup(layerName)
            for group in Groups.GetGroupsFromNamespace(newNamespace):
                topGroup.ConnectSrc(group)

        return cloneReference


    @classmethod
    def layerAnimation(cls, charactersToLayer=None, offsetFrame=None, layerName=None, props=None, referenceModel=False):
        """
        Class Method

        logic for creating the animation layering given the specific characters, offset frame
        and layername

        Kwargs:
            charactersToLayer (list of CharacterAnimationLayerContainer): The characters to layer
            offsetFrame (int): The amount of frames to zero out the animation
            layerName (string): The layer name to use to namespace the characters layer
            props (list of Reference System Objects): Any Refernce system items that make up props to be duplicated
        """
        manager = Manager()

        # Duplicate the slate
        slateRefList = manager.GetReferenceListByType(MocapSlate)
        if len(slateRefList) > 0:
            cloneRef = cls.cloneReferenceAndCopyAnimation(slateRefList[0], layerName, groupCreate=False)
            if cloneRef is None:
                return
        else:
            slateParts = Clapper.DuplicateClapper(newNamespace=layerName, createGroup=True)
            topGroup = cls.GetTopGroup(layerName)
            for group in Groups.GetGroupsFromObject(slateParts[0]):
                topGroup.ConnectSrc(group)

        # Zero out the scene to the offset frame
        if offsetFrame is not None or offsetFrame != 0:
            cls.ZeroOutScene(offsetFrame)

        if charactersToLayer is None:
        # user only requires the slate, so we can return out of the script now
            return

        # Duplicate the characters
        for char in charactersToLayer:
            reference = char.characterReference()
            gender = char.gender()
            colour = char.colour()
            cloneRef = cls.cloneReferenceAndCopyAnimation(reference, layerName, referenceModel=referenceModel, gender=gender, colour=colour)
            if cloneRef is None:
                continue
            mobu.FBSystem().Scene.Evaluate()

        if props is None:
            # no props required to duplicate, we can return out of the script
            return

        # Duplicate the props
        for prop in props:
            cloneRef = cls.cloneReferenceAndCopyAnimation(prop, layerName)
            if cloneRef is None:
                continue
            mobu.FBSystem().Scene.Evaluate()

    @classmethod
    def ZeroOutScene(cls, frame):
        """
        Class Method

        Method to zero out all animation in a scene bringing the given frame number to be at frame
        zero.

        Args:
            frame (int): The frame number to bring to zero
        """
        if frame == 0 or frame == None:
            return False
        # get filter
        filterManager = mobu.FBFilterManager()
        timeShiftScaleFilter = filterManager.CreateFilter('Time Shift And Scale')
        # set shift property
        propShift = timeShiftScaleFilter.PropertyList.Find('Shift')
        # needs to be negative
        propShift.Data = mobu.FBTime(0, 0, 0, -(int(frame)))
        midiInstrument = [item for item in mobu.FBSystem().Scene.Components if item.ClassName() != 'FBMidiInstrument']
        for item in midiInstrument:
            for prop in item.PropertyList:
                if prop.IsAnimatable() and prop.IsAnimated() and prop.GetAnimationNode():
                    timeShiftScaleFilter.Apply(prop.GetAnimationNode(), True)

        timeShiftScaleFilter.FBDelete()
        mobu.FBSystem().Scene.Evaluate()
        return True
