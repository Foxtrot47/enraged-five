from pyfbsdk import *
import RS.Globals as glo

def Run():
    lExprList = ["SPR_", "SM_", "MH_", "RB_", "BagPivot"]
    
    for iComponent in glo.gComponents:
        for iName in lExprList:
            if iComponent.Name.startswith(iName) and iComponent.ClassName() == 'FBModelNull':
                iExpression = iComponent
                lExpRot = iExpression.Rotation
                iExpression.PropertyList.Find("Enable Rotation DOF").Data = True
                iExpression.PropertyList.Find("Pre Rotation").Data = FBVector3d(lExpRot)
                FBSystem().Scene.Evaluate()            
                iExpression.Rotation = FBVector3d(0,0,0)
                FBSystem().Scene.Evaluate()