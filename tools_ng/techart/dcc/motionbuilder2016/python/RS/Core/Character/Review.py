'''
RS.Core.Character.Review

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
Description: Library for character review process.
'''

import os
import shutil
import getpass

from pyfbsdk import *

import RS.Tools.UI
import RS.Config
import RS.Perforce

import clr
clr.AddReference("RSG.SourceControl.Perforce")

from RSG.SourceControl.Perforce import FileState
from RSG.SourceControl.Perforce import FileAction
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character

from PySide import QtCore, QtGui
from pyfbsdk import *


## Constants ##

MESSAGEBOX_TITLE = "Character Approval"

# TODO: Make the permission list data-driven.
PERMISSION_LIST = ['jhayes', 'sletts']

CUTSCENE_REVIEW_DIR = "{0}\\animation\\resources\\characters\\models\\cutscene\\review".format(RS.Config.Project.Path.Art).lower()
CUTSCENE_RESOURCE_DIR = "{0}\\animation\\resources\\characters\\models\\cutscene".format(RS.Config.Project.Path.Art).lower()

INGAME_REVIEW_DIR = "{0}\\animation\\resources\\characters\\models\\ingame\\review".format(RS.Config.Project.Path.Art).lower()
INGAME_RESOURCE_DIR = "{0}\\animation\\resources\\characters\\models\\ingame\\ig".format(RS.Config.Project.Path.Art).lower()


## Functions ##

def ApproveCharacter(reviewFilename, changelistDescription='', quiet=False):
    '''
    Copies the .fbx file from the review folder into the target (cutscene or ingame) directory and submits.
    '''
    
    # Does user have authority to approve the character.
    if (getpass.getuser() not in PERMISSION_LIST):
        msg = "You do not have permission to approve characters!"
        
        if (not quiet):
            FBMessageBox(MESSAGEBOX_TITLE, msg, "OK")
            
        print msg
        return False
    
    # Valid filename.
    if (reviewFilename == ''):
        msg = "No file to review!"
        
        if (not quiet):
            FBMessageBox(MESSAGEBOX_TITLE, msg, "OK")
            
        print msg
        return False
    
    # Ensure user has the latest revision of the review file.
    reviewRecord = RS.Perforce.GetFileState(reviewFilename)
    
    if (reviewRecord):
        if (reviewRecord.HaveRevision != reviewRecord.HeadRevision):
            msg = "Cannot approve character because you do not have the latest revision of the review file! You have revision ({0}) of ({1}).".format(reviewRecord.HaveRevision, reviewRecord.HeadRevision)
            
            if (not quiet):
                FBMessageBox(MESSAGEBOX_TITLE, msg, "OK")
                
            print msg
            return False
    
    else:
        msg = "The review file is not in Perforce! Aborting approval.\n\n{0}".format(reviewFilename)
        
        if (not quiet):
            FBMessageBox(MESSAGEBOX_TITLE, msg, "OK")        

        print msg
        return False
    
    targetDir = None
    
    # Determine target directory, if it's cutscene or ingame based on the review filename.
    if (reviewFilename.lower().startswith(CUTSCENE_REVIEW_DIR)):
        targetDir = CUTSCENE_RESOURCE_DIR
        
    if (reviewFilename.lower().startswith(INGAME_REVIEW_DIR)):
        targetDir = INGAME_RESOURCE_DIR
        
    if (targetDir != None):
        targetFilename = "{0}\\{1}".format(targetDir, os.path.basename(reviewFilename))
        
        record = RS.Perforce.GetFileState(targetFilename)
        
        # Copy file over and submit.
        if (record != None):
            if (record.HaveRevision != record.HeadRevision):
                RS.Perforce.Sync(record.DepotFilename)
            
            # Checked out by someone else, abort.
            if (record.OtherOpen):
                msg = "Cannot approve character because the following file cannot be checked out!\n\n{0}".format(record.DepotFilename)
                
                if (not quiet):
                    FBMessageBox(MESSAGEBOX_TITLE, msg, "OK")
                    
                print msg
                return False
            
            changelist = RS.Perforce.CreateChangelist("Approved Character\n\n{0}".format(changelistDescription))
            RS.Perforce.Edit(record.DepotFilename, changelistNum = changelist.Number)
            RS.Perforce.Run('reopen', args = ['-c', str(changelist.Number), record.ClientFilename])            
            shutil.copyfile(reviewFilename, record.ClientFilename)
            RS.Perforce.Submit(changelist.Number)
            
            msg = "Successfully approved!  The review file was published to the following location:\n\n{0}".format(record.DepotFilename)
            
            if (not quiet):
                FBMessageBox(MESSAGEBOX_TITLE, msg, "OK")
                
            print msg
            return True
            
    else:
        msg = "The file does not live under a review folder! {0}".format(reviewFilename)
        
        if (not quiet):
            FBMessageBox(MESSAGEBOX_TITLE, msg, "OK")   

        print msg
        return False
    
def ApproveCurrentFile():
    '''
    Approves the current file.
    '''
    tool = CharacterApprovalDialog()
    tool.show()    

    
class CharacterApprovalDialog( RS.Tools.UI.QtMainWindowBase ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, title = 'Character Approval', size = [ 400, 300 ] )
        
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)

        self.__centralWidget = QtGui.QWidget()
        self.__centralWidget.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
        mainLayout = QtGui.QVBoxLayout()
        self.__centralWidget.setLayout( mainLayout )
        
        self.__description = QtGui.QPlainTextEdit()
        
        self.__ok = QtGui.QPushButton("Approve")
        self.__ok.clicked.connect(self.Approve_Clicked)
        
        self.__cancel = QtGui.QPushButton("Cancel")
        self.__cancel.clicked.connect(self.Cancel_Clicked)
        
        mainLayout.addWidget(self.__description)
        mainLayout.addWidget(self.__ok)
        mainLayout.addWidget(self.__cancel)
        
        self.setCentralWidget( self.__centralWidget )
        
    def Approve_Clicked(self):
        success = False
        
        manager = Manager()
        
        references = manager.GetReferenceListByType( Character )
        
        if (references):
            for reference in references:
                success = ApproveCharacter(reference.Path, changelistDescription=str(self.__description.toPlainText()))
            
        else:
            success = ApproveCharacter(FBApplication().FBXFileName, changelistDescription=str(self.__description.toPlainText()))
            
        if (success):
            self.close()
    
    def Cancel_Clicked(self):
        self.close()