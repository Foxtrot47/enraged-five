###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_AmbientShading_0_CurrentCharacter.py
## Written And Maintained By: Kathryn Bodey
## Contributors: -
## Description: Sets Ambient Shading nodes to 0 for the currently selected character.
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Globals as glo
import RS.Utils.Scene

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: rs_AmbientShading_CurrentCharacter
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  

def rs_AmbientShading_0_CurrentCharacter():

    lCurrentCharacter = FBApplication().CurrentCharacter
    
    lCharHips = lCurrentCharacter.GetModel(FBBodyNodeId.kFBHipsNodeId)
    
    lRootRefNull = RS.Utils.Scene.GetParent(lCharHips)
    
    lCurrentCharacterModelList = []
    RS.Utils.Scene.GetChildren(lRootRefNull, lCurrentCharacterModelList)
    #lCurrentCharacterModelList.extend(eva.gChildList)
    #del eva.gChildList[:]
    
    for iModel in lCurrentCharacterModelList:
        for iComp in iModel.Components:
            if iComp.ClassName() == "FBMaterial":
                if "_Contact" in iComp.Name:
                    None
                else:
                    iComp.Ambient = FBColor(0,0,0)
