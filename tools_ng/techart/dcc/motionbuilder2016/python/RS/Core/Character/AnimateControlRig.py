###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_AnimateControlRig_Characters.py
## Written And Maintained By: David Bailey
## Contributors: Kathryn Bodey
## Description: Animates Character Control Rig
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
from pyfbsdk import *
import RS.Globals as glo
import RS.Utils.Creation as cre

import RS.Utils.Scene
import os

from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_ControlRigList
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ControlRigList():

    lCurrentCharacter = FBApplication().CurrentCharacter
    if lCurrentCharacter == None:
        # 1. open the character control only opens if not open so my test show.
        FBPopNormalTool ("ControlSet")
        lCurrentCharacter = FBApplication().CurrentCharacter    
    
    if lCurrentCharacter:        
        lCurrentCharacter.CreateControlRig(True)
        lCurrentCharacter.ActiveInput = True   
        
        for iComp in glo.gComponents:
            if iComp.ClassName() == 'FBControlSet':
               for iProp in iComp.PropertyList:
                    if iProp.Name.endswith('TPin') or iProp.Name.endswith('RPin'):
                        iProp.Data = False
        
        lCtrlRigHips = lCurrentCharacter.GetCtrlRigModel(FBBodyNodeId.kFBHipsNodeId)
        lRootRefNull = RS.Utils.Scene.GetParent(lCtrlRigHips)
        lControlRigList = []
        RS.Utils.Scene.GetChildren(lRootRefNull, lControlRigList)
        
        return lControlRigList
        
    return None
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_AnimateRotation
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AnimateRotation(pModel, pNode, pOffset, pLength):
   
    lCurrentCharacter = FBApplication().CurrentCharacter
   
    lAttr = pModel.PropertyList.Find("Lcl Rotation")
    lAttr.SetAnimated(True)
    
    #updated FindModelByName to FindModelByLabelName for MB14
    lLeftEffector = FBFindModelByLabelName(lCurrentCharacter.Name + "_Ctrl:LeftAnkleEffector")
    IKReachRProperty = lLeftEffector.PropertyList.Find('IK Reach Rotation')
    IKReachRProperty.Data = 0
    IKReachTProperty = lLeftEffector.PropertyList.Find('IK Reach Translation')
    IKReachTProperty.Data = 0

    lRightEffector = FBFindModelByLabelName(lCurrentCharacter.Name + "_Ctrl:RightAnkleEffector")
    IKReachRProperty = lRightEffector.PropertyList.Find('IK Reach Rotation')
    IKReachRProperty.Data = 0
    IKReachTProperty = lRightEffector.PropertyList.Find('IK Reach Translation')
    IKReachTProperty.Data = 0
   
    lNode = pModel.Rotation.GetAnimationNode().Nodes[pNode]
   
    lValue = lNode.FCurve.Evaluate(FBTime(0,0,0,0))
    
    lNode.KeyAdd(FBTime(0,0,0,(0 + pOffset)), lValue)
    lNode.KeyAdd(FBTime(0,0,0,((pLength / 2) + pOffset)), lValue + 90)
    lNode.KeyAdd(FBTime(0,0,0,(pLength + pOffset)), lValue)
    FBSystem().Scene.Evaluate()
    
###############################################################################################################   
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_AnimateTranslation
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AnimateTranslation(pModel, pNode, pOffset, pLength):
   
    lAttr = pModel.PropertyList.Find("Lcl Translation")
    lAttr.SetAnimated(True)
    
    lNode = pModel.Translation.GetAnimationNode().Nodes[pNode]
    
    lValue = lNode.FCurve.Evaluate(FBTime(0,0,0,0))
    
    lNode.KeyAdd(FBTime(0,0,0,(0 + pOffset)), lValue)
    lNode.KeyAdd(FBTime(0,0,0,((pLength / 2) + pOffset)), lValue + 25)
    lNode.KeyAdd(FBTime(0,0,0,( pLength + pOffset)), lValue)
    FBSystem().Scene.Evaluate()
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_GenerateROM
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_GenerateROM(pArray, pLength, pCharacter):

    if pArray:
        lPlayerControl = FBPlayerControl()
        lLengthOfAnimation = (len(pArray) * pLength) * 3 + (pLength * 3)
    
        lPlayerControl.LoopStop = FBTime(0,0,0,lLengthOfAnimation)
        
        lOffset = 0
        
        for iControl in pArray:
              
            if iControl.Name == 'Reference':      
                for i in range(3):
                    
                    rs_AnimateTranslation( iControl, i, lOffset, pLength )
                    
                    lOffset = lOffset + pLength
                    
            for i in range(3):
                
                rs_AnimateRotation( iControl, i, lOffset, pLength )
                
                lOffset = lOffset + pLength
            
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_GenerateROM
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################           

def rs_RunROMTest(pControl, pEvent):
    
    #MessageBox
    result = FBMessageBox('ROM Test','A new file will be opened for testing', 'Ok', 'Cancel')

    if result == 1:
        #Check to see if the current file is a resource file
        filePath = RS.Utils.Path.GetFileFolder ( FBApplication().FBXFileName )
        
        fileName = RS.Utils.Path.GetBaseNameNoExtension( FBApplication().FBXFileName )
        
        #Resource location path
        resorcePath = '{0}\\art\\animation\\resources\\characters\\models'.format( RS.Config.Project.Path.Root )
        if str(resorcePath).lower() in str(filePath).lower() :
            #Grab the reference location
            refPath = filePath
        else:
            #Go to the default location for the Characters
            refPath = resorcePath      
            fileName = None

        #If yes Create new file
        RS.Globals.App.FileNew()
        
        manager = Manager() 

        #Browse to character that you want to test
        browse = FBFilePopup()
        browse.Caption = "Select a file a Character to test"
        browse.Style = FBFilePopupStyle.kFBFilePopupOpen
        
        browse.Filter = "*.FBX"
        
        browse.FileName = fileName
        
        browse.Path = refPath
        
        resultBrowse = browse.Execute()        
        
        #if we picked a character
        if resultBrowse:
            
            #If valid character create reference
            if str(resorcePath).lower() in str(browse.Path).lower() :
                #Full reference path
                fullRefPath = browse.FullFilename
                
                #Create the reference            
                reference = manager.CreateReferences( fullRefPath )     
                
                #If character has metadata
                if reference[0]._getMetaFile():
                    #If we have metadata type information
                    if reference[0]._getMetaFile().Type != None:
                        #We run the new the stype of Rom testing
                        rs_StoryROMTest( reference[0] )  
                    else:
                        #if we don't we continue with the previous style of testing
                        lControlRigList = rs_ControlRigList()
                        rs_GenerateROM(lControlRigList, 20, True)                    
      
    '''
    #####
    #oldcode
    
    #Check to see whether or not the characters in the scene have metadata files 
    characterReference= manager.GetReferenceListByType( Character )
    
    if pControl.Name != "Validate":
        FBMessageBox('ROM Test','Please remember: DO NOT SAVE this file with the ROM in it.','Ok')
        
    if characterReference[0]._getMetaFile():
        if characterReference[0]._getMetaFile().Type != None:
            #We run the new the stype of Rom testing
            rs_StoryROMTest( characterReference[0] )
    
    #if we don't we continue with the previous style of testing
    lControlRigList = rs_ControlRigList()
    rs_GenerateROM(lControlRigList, 20, True)
    '''
        
def rs_StoryROMTest( characterReference ):    
    
    #Create a track for our character:
    track = FBStoryTrack(FBStoryTrackType.kFBStoryTrackCharacter, glo.Story.RootFolder)
    
    #Hardcoding the path for now
    path = r"X:\\rdr3\\art\\animation\\resources\\characters\\Models\\cutscene\\Review\\CharRoms\\"
    
    #Get gender/age information from the meta file
    characterType = characterReference._getMetaFile().Type
    
    romPath = {
      'Human Male':   lambda path: '{0}ROM_MaleClip.fbx'.format( path ),
      'Human Female': lambda path: '{0}ROM_FemaleClip.fbx'.format( path ),
      'Human Teen':   lambda path: '{0}ROM_TeenJackClip.fbx'.format( path ),
      'Human Child':  lambda path: '{0}ROM_JackClip.fbx'.format( path )
    }[ characterType ]( path )    

    #Creates clip
    clip = FBStoryClip( os.path.normpath( str(romPath) ), track, FBTime(0,0,0,0) )
    
    #Check to see if we have a rig, if no create one
    controlRig = characterReference._getCharacterComponent().GetCurrentControlSet()
    if not controlRig:    
	characterReference._getCharacterComponent().CreateControlRig(True)
    
	# Set the control rig to active.
	characterReference._getCharacterComponent().ActiveInput = True          
	
    #Assign our CurrentCharacter to the track
    track.Details.append( characterReference._getCharacterComponent() )
    
    #Turn on story mode
    glo.Story.Mute = False
    
    #Find out end of clip and expand it
    FBPlayerControl().LoopStop = FBTime(0 ,0 ,0 , clip.Stop.GetFrame()- clip.Start.GetFrame()  ,0 )
    
    #Open up the Outfit Tool
    from RS.Tools.UI import OutfitSelection
    
    OutfitSelection.Run()       
    
    #Start loop
    FBPlayerControl().Play()    
        
