###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_GeoSetup_PlayerTwo
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Player Two's Geo Group Setup
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Utils.Path
import RS.Globals as glo

def PlayerTwo_GeoSetup():

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: rs_ConstraintsFolder
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################   
    
    def rs_ConstraintsFolder():
        
        lFolder = None
        
        for iFolder in glo.gFolders:
            if iFolder.Name == 'Constraints 1':
                lFolder = iFolder
        
        if lFolder == None:
            lPlaceholder = FBConstraintRelation('Remove_Me')
            lFolder = FBFolder("Constraints 1", lPlaceholder)   
            lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Folders"
            
            FBSystem().Scene.Evaluate()
            
            lPlaceholder.FBDelete()  
                
        return lFolder
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Group Definition
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    def rs_GroupSetup(pGroupName, pGeoArray, pMainGroup):
    
        lGroup = FBGroup(pGroupName)
        
        for iGeo in pGeoArray:
            lGroup.ConnectSrc(iGeo)
        
        pMainGroup.ConnectSrc(lGroup) 
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Geo Arrays
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    lMainDict = {}
    
    lDefaultArray = []
    lDefaultGeoDict = {'accs':'accs_000', 
                       'feet':'feet_000', 
                       'lowr':'lowr_000', 
                       'uppr':'uppr_000', 
                       'hand':'hand_000', 
                       'hair':'hair_000', 
                       'teef':'teef_000', 
                       'head':'head_000'}
    
    lMainDict[ 'default' ] = ( lDefaultGeoDict, lDefaultArray )
                      
    
    lBallisticArray = []                    
    lBallisticDict =  {'accs':'accs_002', 
                       'lowr':'lowr_002', 
                       'uppr':'uppr_002',
                       'hand':'hand_000',      
                       'hair':'hair_000', 
                       'teef':'teef_000', 
                       'p_head':'p_head_024',
                       'head':'head_000'}
    
    lMainDict[ 'ballistic' ] = ( lBallisticDict, lBallisticArray )
    
    
    lDockArray = []                    
    lDockDict =  {'accs':'accs_004', 
                  'feet':'feet_005',
                  'lowr':'lowr_005', 
                  'uppr':'uppr_005',  
                  'hand':'hand_002',
                  'p_eyes':'p_eyes_002',
                  'p_head':'p_head_007',                  
                  'hair':'hair_000', 
                  'teef':'teef_000', 
                  'head':'head_000'}
    
    
    lMainDict[ 'dock' ] = ( lDockDict, lDockArray )
            
    
    lHighwayArray = []                    
    lHighwayDict = {'accs':'accs_000', 
                    'feet':'feet_005',
                    'lowr':'lowr_007', 
                    'uppr':'uppr_007',  
                    'hand':'hand_000',
                    'decl':'decl_003',
                    'p_head':'p_head_005', 
                    'task':'task_002',               
                    'hair':'hair_000', 
                    'teef':'teef_000', 
                    'head':'head_000'}
    
    
    lMainDict[ 'highway' ] = ( lHighwayDict, lHighwayArray )

    lBankSuitArray = []                    
    lBankSuitDict = {'lowr':'lowr_020', 
                     'uppr':'uppr_027',
                     'feet':'feet_009',
                     'hand':'hand_000',              
                     'hair':'hair_000', 
                     'teef':'teef_000', 
                     'head':'head_000'}
    
    lMainDict[ 'banksuit' ] = ( lBankSuitDict, lBankSuitArray )     
    
    lPrologueArray = []                    
    lPrologueDict = {'feet':'feet_012',
                     'lowr':'lowr_009', 
                     'uppr':'uppr_009',
                     'berd':'berd_001',  
                     'hand':'hand_000',              
                     'hair':'hair_007', 
                     'teef':'teef_000', 
                     'head':'head_000'}
    
    lMainDict[ 'prologue' ] = ( lPrologueDict, lPrologueArray )        
    
                                        
    lSecurityArray = []                    
    lSecurityDict = {'accs':'accs_007',
                     'lowr':'lowr_008', 
                     'uppr':'uppr_008',
                     'p_head':'p_head_006',  
                     'hand':'hand_000',              
                     'hair':'hair_000', 
                     'teef':'teef_000', 
                     'head':'head_000'}          
    
    lMainDict[ 'security' ] = ( lSecurityDict, lSecurityArray )   
    
    
    lSweatsArray = []                    
    lSweatsDict = {'accs':'accs_000',
                   'feet':'feet_000',
                   'lowr':'lowr_023', 
                   'uppr':'uppr_000',
                   'hand':'hand_000',              
                   'hair':'hair_000', 
                   'teef':'teef_000', 
                   'head':'head_000'}
    
    lMainDict[ 'sweat' ] = ( lSweatsDict, lSweatsArray )   
    
    
    lTennisArray = []                    
    lTennisDict = {'accs':'accs_000',
                   'lowr':'lowr_015', 
                   'uppr':'uppr_015',
                   'p_head':'p_head_010',
                   'hand':'hand_000',              
                   'hair':'hair_000', 
                   'teef':'teef_000', 
                   'head':'head_000'}
    
    lMainDict[ 'tennis' ] = ( lTennisDict, lTennisArray )   
          
    
    lAccArray = []                    
    lAccDict = {'accs0':'accs_000',
                'accs3':'accs_003', 
                'accs6':'accs_006',
                'task':'task_001'}             
    
    lMainDict[ 'acc' ] = ( lAccDict, lAccArray )   
    
    
    lPropArray = []                    
    lPropDict = {}
    
    for key, value in lMainDict.iteritems():
        for name, geo in value[0].iteritems():
            if "p_" in geo:
               lPropDict[ geo ] = geo
               
    lMainDict[ 'props' ] = ( lPropDict, lPropArray )   
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Group Create
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    lPropsList = []
    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    
    lMainGroup = FBGroup("GeoVariations")
    
    for key, value in lMainDict.iteritems():
        for name, geo in value[0].iteritems():
            for iComponent in glo.gComponents:
                if iComponent.Name.lower().startswith(geo) and iComponent.ClassName() == 'FBModel':
                    if iComponent.Parent != None:
                        if iComponent.Parent.Name.lower() == lFileName.lower() or iComponent.Parent.Name.lower() == "geometry":
                            value[1].append(iComponent)
                            lMainDict[ key ] = ( value[0], value[1] ) 
                            if iComponent.Name.lower().startswith('p_'):
                                lPropsList.append(iComponent)
    
    for key, value in lMainDict.iteritems():
        rs_GroupSetup(key, value[1], lMainGroup)
    
    lMainGroup.Pickable = False
    lMainGroup.Transformable = False
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ## Description: Add Prop Geo Objects ('p_') to the SKEL_Head via constraint
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    lHead = None
    lHead = FBFindModelByLabelName("SKEL_Head")
    
    lPropsList = list(set(lPropsList))
    
    lGeoPropDict = {
                    'p_he':'SKEL_Head',
                    'p_ey':'SKEL_Head',
                    'p_ea':'SKEL_Head',
                    'p_lw':'SKEL_L_Forearm',
                    'p_rw':'SKEL_R_Forearm',
                    'p_lh':'SKEL_Pelvis',
                    'p_rh':'SKEL_Pelvis',
                    'p_lf':'SKEL_L_Foot',
                    'p_rf':'SKEL_R_Foot'
                    }                    
    
    if len(lPropsList) >= 1:
        if lHead != None:
            lPlaceholder = FBConstraintRelation('Remove_Me')
            lFolder = FBFolder("Geo_Prop_Constraints", lPlaceholder)   
            lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Folders"       
            lPlaceholder.FBDelete()  
                  
            for i in range(len(lPropsList)):
                for lProp, lJoint in lGeoPropDict.iteritems():
                    if lProp in lPropsList[i].Name.lower():
                        lParentChildConstraint = FBConstraintManager().TypeCreateConstraint(3)
                        lParentChildConstraint.Name = lPropsList[i].Name + "_Attach_PC"
                        FBSystem().Scene.Constraints.append(lParentChildConstraint)
                        lParentChildConstraint.ReferenceAdd (0,lPropsList[i])
                        lParentChildConstraint.ReferenceAdd (1,FBFindModelByLabelName(lJoint))
                        lParentChildConstraint.Active = True
                        
                        FBSystem().Scene.Evaluate()
                        
                        lFolder.Items.append(lParentChildConstraint)
                             
                        lTag = lParentChildConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                        lTag.Data = "rs_Constraints"
                
            lConFolder = rs_ConstraintsFolder()
            lConFolder.Items.append(lFolder)
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Add to Character Group if it exists
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    lCharacterGroup = None
    
    for iGroup in glo.gGroups:
        if iGroup.Name == lFileName:
            lCharacterGroup = iGroup
    
    if lCharacterGroup != None:
        lCharacterGroup.ConnectSrc(lMainGroup)
    
    FBMessageBox('GeoSetup',"Player_Two Geo Groups are now setup.\n\nMake sure to do a cleanup of any unused geo!",'Ok')
