###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_GeoSetup_PlayerZero
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Player Zero's Geo Group Setup
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Globals as glo
import RS.Utils.Scene
import RS.Core.AssetSetup.Lib as ass

def PlayerZero_GeoSetup( projName = None ):
        
    if "gta5" in projName.lower():
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: rs_ConstraintsFolder
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################   
        
        def rs_ConstraintsFolder():
            
            lFolder = None
            
            for iFolder in glo.gFolders:
                if iFolder.Name == 'Constraints 1':
                    lFolder = iFolder
            
            if lFolder == None:
                lPlaceholder = FBConstraintRelation('Remove_Me')
                lFolder = FBFolder("Constraints 1", lPlaceholder)   
                lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = "rs_Folders"
                
                FBSystem().Scene.Evaluate()
                
                lPlaceholder.FBDelete()  
                    
            return lFolder
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Group Definition
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        def rs_GroupSetup(pGroupName, pGeoArray, pMainGroup):
        
            lGroup = FBGroup(pGroupName)
            
            for iGeo in pGeoArray:
                lGroup.ConnectSrc(iGeo)
            
            pMainGroup.ConnectSrc(lGroup) 
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Geo Arrays
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        lMainDict = {}
        
        lDefaultArray = []
        lDefaultGeoDict = {'feet':'feet_000', 
                           'lowr':'lowr_000', 
                           'uppr':'uppr_000', 
                           'hand':'hand_000', 
                           'hair':'hair_000', 
                           'teef':'teef_000', 
                           'head':'head_000'}
        
        lMainDict[ 'default' ] = ( lDefaultGeoDict, lDefaultArray )
        
        
        lLeatherJackArray = []
        lLeatherJackDict = {'feet':'feet_000', 
                           'lowr':'lowr_000', 
                           'uppr':'uppr_022', 
                           'hand':'hand_000', 
                           'hair':'hair_000', 
                           'teef':'teef_000', 
                           'head':'head_000'}
        
        lMainDict[ 'leatherjacket' ] = ( lLeatherJackDict, lLeatherJackArray )
        
    
        lAccArray = []
        lAccDict = {'task1':'task_001', 
                    'task2':'task_002', 
                    'task5':'task_005',   
                    'task9':'task_009',
                    'task10':'task_010',
                    'accs7':'accs_007', 
                    'accs9':'accs_009', 
                    'accs15':'accs_015', 
                    'accs16':'accs_016',
                    'p_eyes5':'p_eyes_005',
                    'p_head11':'p_head_011'}
        
        lMainDict[ 'acc' ] = ( lAccDict, lAccArray )
        
        
        lBallisticArray = []
        lBallisticDict = {'decl':'decl_000', 
                          'lowr':'lowr_005', 
                          'uppr':'uppr_005', 
                          'hand':'hand_001', 
                          'hair':'hair_000', 
                          'teef':'teef_000', 
                          'head':'head_000',
                          'p_head':'p_head_026',
                          'accs':'accs_005'}
        
        lMainDict[ 'ballistic' ] = ( lBallisticDict, lBallisticArray )
        
        
        lBeachArray = []
        lBeachDict = {'decl':'decl_006', 
                      'feet':'feet_001', 
                      'lowr':'lowr_016', 
                      'uppr':'uppr_017', 
                      'hand':'hand_000', 
                      'hair':'hair_000', 
                      'teef':'teef_000', 
                      'head':'head_000'}
        
        lMainDict[ 'beach' ] = ( lBeachDict, lBeachArray )
        
        
        lBeachBareArray = []
        lBeachBareDict = {'decl':'decl_006', 
                          'feet':'feet_001', 
                          'lowr':'lowr_016', 
                          'uppr':'uppr_017', 
                          'hand':'hand_000', 
                          'hair':'hair_000', 
                          'teef':'teef_000', 
                          'head':'head_000'}              
        
        lMainDict[ 'beachbare' ] = ( lBeachBareDict, lBeachBareArray )                    
        
        
        lCoderArray = []
        lCoderDict = {'accs':'accs_010', 
                      'lowr':'lowr_013', 
                      'uppr':'uppr_014', 
                      'hand':'hand_000', 
                      'hair':'hair_000', 
                      'teef':'teef_000', 
                      'head':'head_000',
                      'p_eyes':'p_eyes_004'}
        
        lMainDict[ 'coder' ] = ( lCoderDict, lCoderArray )  
        
        lEpsilonArray = []
        lEpsilonDict = {'accs':'accs_013', 
                        'lowr':'lowr_019', 
                        'uppr':'uppr_027', 
                        'hand':'hand_000', 
                        'hair':'hair_000', 
                        'teef':'teef_000', 
                        'head':'head_000'}
        
        lMainDict[ 'epsilon' ] = ( lEpsilonDict, lEpsilonArray )  
        
        lFireArray = []
        lFireDict =  {'accs':'accs_001', 
                      'lowr':'lowr_001', 
                      'uppr':'uppr_001', 
                      'hand':'hand_000', 
                      'hair':'hair_000', 
                      'teef':'teef_000', 
                      'head':'head_000',
                      'p_head':'p_head_000',
                      'decl':'decl_001'}
        
        lMainDict[ 'fire' ] = ( lFireDict, lFireArray )  
        
        
        lHighwayArray = []
        lHighwayDict =  {'accs':'accs_008', 
                         'lowr':'lowr_009', 
                         'uppr':'uppr_010', 
                         'hand':'hand_000', 
                         'hair':'hair_000', 
                         'teef':'teef_000', 
                         'head':'head_000',
                         'feet':'feet_001',
                         'decl':'decl_003'}
        
        lMainDict[ 'highway' ] = ( lHighwayDict, lHighwayArray )                               
        
        
        
        lJanitorArray = []
        lJanitorDict =  {'accs':'accs_002', 
                         'lowr':'lowr_002', 
                         'uppr':'uppr_002', 
                         'hand':'hand_000', 
                         'hair':'hair_000', 
                         'teef':'teef_000', 
                         'head':'head_000',
                         'feet':'feet_002',
                         'decl':'decl_000',
                         'p_head':'p_head_001',
                         'p_eyes':'p_eyes_000'}
        
        lMainDict[ 'janitor' ] = ( lJanitorDict, lJanitorArray )  
        
        
        lPestArray = []
        lPestDict =  {'accs':'accs_009', 
                      'lowr':'lowr_011', 
                      'uppr':'uppr_012', 
                      'hand':'hand_000', 
                      'hair':'hair_000', 
                      'teef':'teef_000', 
                      'head':'head_000',
                      'feet':'feet_001',
                      'decl':'decl_004',
                      'p_head':'p_head_006',
                      'p_eyes':'p_eyes_000'}
        
        lMainDict[ 'pest' ] = ( lPestDict, lPestArray )  
        
        lLoungeArray = []
        lLoungeDict =    {'lowr':'lowr_028', 
                          'uppr':'uppr_008', 
                          'hand':'hand_000', 
                          'hair':'hair_000', 
                          'teef':'teef_000', 
                          'head':'head_000',
                          'jbib':'jbib_002',
                          'p_eyes':'p_eyes_005'}
        
        lMainDict[ 'loungewear' ] = ( lLoungeDict, lLoungeArray )      
        
        lUnderwearArray = []
        lUnderwearDict = {'lowr':'lowr_018', 
                          'uppr':'uppr_019', 
                          'hand':'hand_000', 
                          'hair':'hair_000', 
                          'teef':'teef_000', 
                          'head':'head_000'}
        
        lMainDict[ 'underwear' ] = ( lUnderwearDict, lUnderwearArray )  
        
                    
        lPrologueArray = []
        lPrologueDict = {'accs':'accs_007', 
                         'lowr':'lowr_014', 
                         'uppr':'uppr_015', 
                         'hand':'hand_005', 
                         'hair':'hair_000', 
                         'teef':'teef_000', 
                         'head':'head_000',
                         'feet':'feet_001',
                         'task':'task_000'}
        
        lMainDict[ 'prologue' ] = ( lPrologueDict, lPrologueArray )  
        
        
        lPrologue2Array = []
        lPrologue2Dict = {'accs':'accs_007', 
                          'lowr':'lowr_026', 
                          'uppr':'uppr_031', 
                          'hand':'hand_005', 
                          'hair':'hair_000', 
                          'teef':'teef_000', 
                          'head':'head_000',
                          'feet':'feet_014'}
        
        lMainDict[ 'prologue2' ] = ( lPrologue2Dict, lPrologue2Array )
                          
        
        lSecurityArray = []
        lSecurityDict = {'accs':'accs_008', 
                         'lowr':'lowr_010', 
                         'uppr':'uppr_011', 
                         'hand':'hand_000', 
                         'hair':'hair_000', 
                         'teef':'teef_000', 
                         'head':'head_000',
                         'p_head':'p_head_005'}
        
        lMainDict[ 'security' ] = ( lSecurityDict, lSecurityArray )
        
        
        lSpecOpsArray = []
        lSpecOpsDict =  {'task':'task_003', 
                         'lowr':'lowr_004', 
                         'uppr':'uppr_004', 
                         'hand':'hand_006', 
                         'hair':'hair_000', 
                         'teef':'teef_000', 
                         'head':'head_000',
                         'feet':'feet_004',                 
                         'p_eyes':'p_eyes_002'}
        
        lMainDict[ 'specops' ] = ( lSpecOpsDict, lSpecOpsArray )
        
        
        lSpecOps2Array = []
        lSpecOps2Dict =  {'task':'task_000', 
                         'accs':'accs_004', 
                         'lowr':'lowr_004', 
                         'uppr':'uppr_004', 
                         'hand':'hand_006', 
                         'hair':'hair_000', 
                         'teef':'teef_000', 
                         'head':'head_000',
                         'feet':'feet_004',                 
                         'p_eyes':'p_eyes_002'}
        
        lMainDict[ 'specops harness OFF' ] = ( lSpecOps2Dict, lSpecOps2Array )
        
        
        lScubaArray = []
        lScubaDict = {'task':'task_007', 
                      'accs':'accs_003', 
                      'lowr':'lowr_003', 
                      'uppr':'uppr_003', 
                      'hand':'hand_006', 
                      'hair':'hair_000', 
                      'teef':'teef_000', 
                      'head':'head_000',
                      'feet3':'feet_003',
                      'feet12':'feet_012',                               
                      'p_eyes':'p_eyes_001'}
        
        lMainDict[ 'scuba' ] = ( lScubaDict, lScubaArray )
        
        
        lTennisArray = []
        lTennisDict = {'lowr':'lowr_023',
                       'uppr':'uppr_028', 
                       'hand':'hand_006', 
                       'hair':'hair_000', 
                       'teef':'teef_000', 
                       'head':'head_000'}
        
        lMainDict[ 'tennis' ] = ( lTennisDict, lTennisArray )
        
        
        lTuxArray = []
        lTuxDict = {'lowr':'lowr_000',
                    'uppr':'uppr_029', 
                    'hand':'hand_006', 
                    'hair':'hair_000', 
                    'teef':'teef_000', 
                    'head':'head_000',
                    'feet':'feet_000'}
        
        lMainDict[ 'tux' ] = ( lTuxDict, lTuxArray )                 
        
        
        lPropArray = []                    
        lPropDict = {}
        
        for key, value in lMainDict.iteritems():
            for name, geo in value[0].iteritems():
                if "p_" in geo:
                   lPropDict[ geo ] = geo
                   
        lMainDict[ 'props' ] = ( lPropDict, lPropArray )   
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Group Create
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        lPropsList = []
        lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        
        lMainGroup = FBGroup("GeoVariations")
        
        for key, value in lMainDict.iteritems():
            for name, geo in value[0].iteritems():
                for iComponent in glo.gComponents:
                    if iComponent.Name.lower().startswith(geo) and iComponent.ClassName() == 'FBModel':
                        if iComponent.Parent != None:
                            if iComponent.Parent.Name.lower() == lFileName.lower() or iComponent.Parent.Name.lower() == "geometry":
                                value[1].append(iComponent)
                                lMainDict[ key ] = ( value[0], value[1] ) 
                                if iComponent.Name.lower().startswith('p_'):
                                    lPropsList.append(iComponent)
        
        for key, value in lMainDict.iteritems():
            rs_GroupSetup(key, value[1], lMainGroup)
        
        lMainGroup.Pickable = False
        lMainGroup.Transformable = False
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ## Description: Add Prop Geo Objects ('p_') to the SKEL_Head via constraint
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        lHead = None
        lHead = FBFindModelByLabelName("SKEL_Head")
        
        lPropsList = list(set(lPropsList))
        
        lGeoPropDict = {
                        'p_he':'SKEL_Head',
                        'p_ey':'SKEL_Head',
                        'p_ea':'SKEL_Head',
                        'p_lw':'SKEL_L_Forearm',
                        'p_rw':'SKEL_R_Forearm',
                        'p_lh':'SKEL_Pelvis',
                        'p_rh':'SKEL_Pelvis',
                        'p_lf':'SKEL_L_Foot',
                        'p_rf':'SKEL_R_Foot'
                        }                    
        
        if len(lPropsList) >= 1:
            if lHead != None:
                lPlaceholder = FBConstraintRelation('Remove_Me')
                lFolder = FBFolder("Geo_Prop_Constraints", lPlaceholder)   
                lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = "rs_Folders"       
                lPlaceholder.FBDelete()  
                      
                for i in range(len(lPropsList)):
                    for lProp, lJoint in lGeoPropDict.iteritems():
                        if lProp in lPropsList[i].Name.lower():
                            lParentChildConstraint = FBConstraintManager().TypeCreateConstraint(3)
                            lParentChildConstraint.Name = lPropsList[i].Name + "_Attach_PC"
                            #FBSystem().Scene.Constraints.append(lParentChildConstraint)
                            lParentChildConstraint.ReferenceAdd (0,lPropsList[i])
                            lParentChildConstraint.ReferenceAdd (1,FBFindModelByLabelName(lJoint))
                            lParentChildConstraint.Active = True
                            
                            FBSystem().Scene.Evaluate()
                            
                            lFolder.Items.append(lParentChildConstraint)
                                 
                            lTag = lParentChildConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                            lTag.Data = "rs_Constraints"
                    
                lConFolder = rs_ConstraintsFolder()
                lConFolder.Items.append(lFolder)
            
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Add to Character Group if it exists
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        
        lCharacterGroup = None
        
        for iGroup in glo.gGroups:
            if iGroup.Name == lFileName:
                lCharacterGroup = iGroup
        
        if lCharacterGroup != None:
            lCharacterGroup.ConnectSrc(lMainGroup)
        
        FBMessageBox('GeoSetup',"Player_Zero Geo Groups are now setup.\n\nMake sure to do a cleanup of any unused geo!",'Ok')
    

    elif projName == "rdr3" :
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: rdr3 setup.
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        def rs_ConstraintsFolder():
            
            lFolder = None
            
            for iFolder in glo.gFolders:
                if iFolder.Name == 'Constraints 1':
                    lFolder = iFolder
            
            if lFolder == None:
                lPlaceholder = FBConstraintRelation('Remove_Me')
                lFolder = FBFolder("Constraints 1", lPlaceholder)   
                lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = "rs_Folders"
                
                FBSystem().Scene.Evaluate()
                
                lPlaceholder.FBDelete()  
                    
            return lFolder
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Group Definition
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        def rs_GroupSetup(pGroupName, pGeoArray, pMainGroup):
        
            lGroup = FBGroup(pGroupName)
            
            for subKey, iGeoGroup in pGeoArray.iteritems():
                if subKey != 'None' :
                    sGroup = FBGroup(subKey)
                else:
                    sGroup = None
                
                for iGeo in iGeoGroup:
                    if sGroup != None:
                        sGroup.ConnectSrc(iGeo)
                    else:
                        lGroup.ConnectSrc(iGeo)
            
                if sGroup != None :
                    lGroup.ConnectSrc(sGroup)
                    if "default" in sGroup.Name.lower():
                        sGroup.Show = True
            
            pMainGroup.ConnectSrc(lGroup)
            
            if "default" in lGroup.Name.lower() :
                lGroup.Show = True
            else:
                lGroup.Show = False
                
                
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Geo Arrays
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        lMainDict = {}
        
        lDefaultHeadArray = {}
        lDefaultHeadDict = {'None':{'head_000',
                                    'teef_000',
                                    'eyes_000',
                                    'hair_000'}
                           }
        
        lMainDict[ 'Default Head' ] = ( lDefaultHeadDict, lDefaultHeadArray )
        
        lDefaultArray = {}
        lDefaultGeoDict = {'None':{'uppr_000',
                                    'lowr_000',
                                    'hand_000'}
                           }
        
        lMainDict[ 'Default Body' ] = ( lDefaultGeoDict, lDefaultArray )
        
        lv00DictArray = {}
        lv00Dict = {'None':{'uppr_000',
                            'lowr_000',
                            'jbib_000',
                            'decl_000',
                            'hand_000'}
                           }
        
        lMainDict[ 'v00' ] = ( lv00Dict, lv00DictArray )
        
        
        lv01DictArray = {}
        lv01Dict = {'None':{#'uppr_001',
                            'lowr_001',
                            #'jbib_001',
                            'decl_001',
                            'hand_001'}
                    }
        
        lMainDict[ 'v01' ] = ( lv01Dict, lv01DictArray )
        
        
        lv02DictArray = {}
        lv02Dict = {'None':['uppr_002',
                            'lowr_002',
                            'hand_002',
                            'jbib_002',
                            'decl_002',
                            'feet_002']
                    }
        
        lMainDict[ 'v02' ] = ( lv02Dict, lv02DictArray )
        
        
        lv03DictArray = {}
        lv03Dict = {'None':['uppr_003',
                            'lowr_003',
                            'hand_003',
                            'jbib_003',
                            'decl_003',
                            'feet_003']
                    }
        
        lMainDict[ 'v03' ] = ( lv03Dict, lv03DictArray )
        
        
        lv04DictArray = {}
        lv04Dict = {'None':['uppr_004',
                            'lowr_004',
                            'hand_004',
                            'jbib_004',
                            'decl_004',
                            'feet_004',
                            'accs_001']
                    }
        
        lMainDict[ 'v04' ] = ( lv04Dict, lv04DictArray )
        
        
        lv05DictArray = {}
        lv05Dict = {'None':['uppr_005',
                            'lowr_002',
                            'hand_002',
                            'jbib_002',
                            'decl_002',
                            'feet_002']
                    }
        
        lMainDict[ 'v05' ] = ( lv05Dict, lv05DictArray )
        
        
        lv06DictArray = {}
        lv06Dict = {'None':['uppr_006',
                            'lowr_006',
                            'hand_006',
                            'jbib_006',
                            'decl_006',
                            'feet_006']
                    }
        
        lMainDict[ 'v06' ] = ( lv06Dict, lv06DictArray )
        
        
        lAccArray = {}
        lAccDict = {'Pistol & Holster':{'misc_000_u pistol',
                              'accs_000'},
                    'Rifle & Bandolier':{'misc_000_u rifle',
                             'acc2_000'},
                    'Beard':{'berd_000'},
                    'Hat1':{'p_head_000'},
                    'Hat2':{'p_head_001'},
                    'Bandana':{'task_001'}
                    }
        
        lMainDict[ 'accs' ] = ( lAccDict, lAccArray )
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Group Create
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        lPropsList = []
        lFoundList = []
        
        ###################################
        
        lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        
        lMainGroup = FBGroup("GeoVariations")
        
        for key, value in lMainDict.iteritems():
            for name, geoList in value[0].iteritems():
                
                if name not in value[1]:
                    value[1][name] = []
                for geo in geoList:
                    for iComponent in glo.gComponents:
                        if iComponent.Name.lower().startswith(geo) and iComponent.ClassName() == 'FBModel':
                            if iComponent.Parent != None:
                                if iComponent.Parent.Name.lower() == lFileName.lower() or iComponent.Parent.Name.lower() == "geometry":
                                    if iComponent not in value[1][name]:
                                        value[1][name].append(iComponent)
                                    lMainDict[ key ] = ( value[0], value[1] ) 
                                    if iComponent.Name.lower().startswith('p_'):
                                        lPropsList.append(iComponent)
                                    if iComponent not in lFoundList:
                                        lFoundList.append( iComponent )
        
        for key, value in lMainDict.iteritems():
            rs_GroupSetup(key, value[1], lMainGroup)
        
        ##
        ## Sets up the "unsorted" group        
        ##
        
        lMasterList = []
        lGeo = ass.rs_AssetSetupVariables()[2]
        lMasterList = lGeo.Children
        lUnsortedGroup = FBGroup( "Unsorted" )
        for iObj in lMasterList:
            if iObj not in lFoundList:
                lUnsortedGroup.ConnectSrc( iObj )
        lMainGroup.ConnectSrc( lUnsortedGroup )
        lUnsortedGroup.Show = False
        
        ##
        ##
        ##
        
        lMainGroup.Pickable = False
        lMainGroup.Transformable = False
        
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ## Description: Add Prop Geo Objects ('p_') to the SKEL_Head via constraint
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        lHead = None
        lHead = FBFindModelByLabelName("SKEL_Head")
        
        lPropsList = list(set(lPropsList))
        
        lGeoPropDict = {
                        'p_he':'SKEL_Head',
                        'p_ey':'SKEL_Head',
                        'p_ea':'SKEL_Head',
                        'p_lw':'SKEL_L_Forearm',
                        'p_rw':'SKEL_R_Forearm',
                        'p_lh':'SKEL_Pelvis',
                        'p_rh':'SKEL_Pelvis',
                        'p_lf':'SKEL_L_Foot',
                        'p_rf':'SKEL_R_Foot'
                        }                    
        
        if len(lPropsList) >= 1:
            if lHead != None:
                lPlaceholder = FBConstraintRelation('Remove_Me')
                lFolder = FBFolder("Geo_Prop_Constraints", lPlaceholder)   
                lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = "rs_Folders"       
                lPlaceholder.FBDelete()  
                      
                for i in range(len(lPropsList)):
                    for lProp, lJoint in lGeoPropDict.iteritems():
                        if lProp in lPropsList[i].Name.lower():
                            lParentChildConstraint = FBConstraintManager().TypeCreateConstraint(3)
                            lParentChildConstraint.Name = lPropsList[i].Name + "_Attach_PC"
                            #FBSystem().Scene.Constraints.append(lParentChildConstraint)
                            lParentChildConstraint.ReferenceAdd (0,lPropsList[i])
                            lParentChildConstraint.ReferenceAdd (1,FBFindModelByLabelName(lJoint))
                            lParentChildConstraint.Active = True
                            
                            FBSystem().Scene.Evaluate()
                            
                            lFolder.Items.append(lParentChildConstraint)
                                 
                            lTag = lParentChildConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                            lTag.Data = "rs_Constraints"
                    
                lConFolder = rs_ConstraintsFolder()
                lConFolder.Items.append(lFolder)
            
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Add to Character Group if it exists
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###############################################################################################################
        
        lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        
        lCharacterGroup = None
        
        for iGroup in glo.gGroups:
            if iGroup.Name == lFileName:
                lCharacterGroup = iGroup
        
        if lCharacterGroup != None:
            lCharacterGroup.ConnectSrc(lMainGroup)
        
        FBMessageBox('GeoSetup',"Player_Zero Geo Groups are now setup.\n\nMake sure to do a cleanup of any unused geo!",'Ok')
    
    
    else:
        print "Could not determine project name."