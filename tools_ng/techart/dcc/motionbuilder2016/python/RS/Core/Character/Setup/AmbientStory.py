###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Script Name: rs_CharactersGeoGroups
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Basic Setup of Generic Character Geo Groups
##
## Rules: Definitions: Prefixed with "rs_"
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

from pyfbsdk import *
import os
import csv

from RS.Utils import Path
import RS.Utils.Scene as Scene
from RS import Globals, Perforce
from PySide import QtGui


def dict_add_or_append(dictionary, key, value):
    # Checks whether a key is in a dictionary, adds the value to that key if so. If not, adds a new key with the passed value
    if key not in dictionary.keys():
        dictionary[key] = [value]
    if value not in dictionary[key]:
        dictionary[key].append(value)


def getGroupHierarchy(group, groupHierarchy):
    # Gets the hierachy of a passed group
    if len(group.Parents)>1:
        groupHierarchy.append(group.Parents[1].Name)
        getGroupHierarchy(group.Parents[1], groupHierarchy)
    return groupHierarchy


def saveCharGeoGroups(characterName, charOutputPath):
    # Saves the character's geometry groups out to a .xml file
    
    # Define empty variatbles
    geoVarGroup = None
    geoGroups = {}
    
    # Find our geo variation group
    for group in Globals.Groups:
        if group.Name.lower() == "geovariations":
            geoVarGroup = group
    
    if geoVarGroup == None:
        QtGui.QMessageBox.information(None, "Character Groups", characterName + " couldn't find any geo variation groups. Is the character set up correctly?")
        return
    
    # Add the geo variation groups to the geoGroups dictionary
    for group in geoVarGroup.Items:
        groupHierarchy = []
        getGroupHierarchy(group, groupHierarchy)
        groupHierarchy.reverse()
        groupHierarchy.append(group.Name)
        for item in group.Items:
            if " " in item.Name:
                geoName = item.Name.split(" ")[0]
            else:
                geoName = item.Name
            groupString = ""
            for thing in groupHierarchy:
                groupString += ">>" + thing
            groupName = groupString.split(">>")[-1]
            dict_add_or_append(geoGroups, groupString, geoName)
    
    # Add to Perforce
    if Perforce.DoesFileExist(charOutputPath) == True:
        Perforce.Edit(charOutputPath)
    else:
        Perforce.Add(charOutputPath)
    
    # Write the CSV
    with open(charOutputPath, "w") as outputFile:
        writer = csv.DictWriter(outputFile, fieldnames = ["Group Name", "Group Structure", "Object Names"])
        writer.writeheader()
        sortedNames=sorted(geoGroups.keys(), key=lambda x:x.lower())
        for object in sortedNames:
            if not object == "":
                groupName = object.split(">>")[-1]
                geoNames = geoGroups[object]
                writer.writerows({'Group Name':groupName, 'Group Structure':object, 'Object Names':geoName} for geoName in geoGroups[object])
    
    # Alert the user if we didn't write the file
    if not os.path.exists(charOutputPath):
        QtGui.QMessageBox.information(None, "Character Groups", characterName + " could not be saved. Is your character named correctly?")
        return
    
    QtGui.QMessageBox.information(None, "Character Groups", characterName + ".xml has been saved and added to your default Perforce Changelist. Please submit this along with the FBX.")
    
    
def restoreCharGeoGroups(characterName, inputPath):
    # Restore character geo groups from an external .xml file
    # Define empty variatbles
    geoVarGroup = None
    deleteList = []
    
    for group in Globals.Groups:
        if group.Name.lower() == "geovariations":
            geoVarGroup = group
            
    if geoVarGroup is not None:
        for item in geoVarGroup.Items:
            if not item in deleteList:
                deleteList.append(item)
        if not geoVarGroup in deleteList:
            deleteList.append(geoVarGroup)
        for item in deleteList:
            item.FBDelete()
        Globals.Scene.Evaluate()
    
    sceneGroups = []
    for group in Globals.Groups:
        sceneGroups.append(group.Name)
    
    # Check the existing .csv file
    if not os.path.exists(inputPath):
        return False
    
    with open(inputPath, "r") as inputFile:
        reader = csv.reader(inputFile)
        next(reader, None)  # skip the headers
        for row in reader:
            objectGroupName = row[0]
            csvGroups = list(row[1].split(">>"))
            matchedObjects = []
            objectName = row[2]
            if not objectName == "":
                [matchedObjects.append(component) for component in Globals.Components if objectName in component.Name and isinstance(component, FBModel)]
            for index in range(len(csvGroups)):
                csvGroup = csvGroups[index]
                parentCsvGroup = csvGroups[index-1]
                if not csvGroup == "" and not parentCsvGroup == "":
                    if not csvGroup in sceneGroups:
                        newGroup = FBGroup(csvGroup)
                        sceneGroups.append(csvGroup)
                    if not parentCsvGroup in sceneGroups:
                        parentGroup = FBGroup(parentCsvGroup)
                        sceneGroups.append(parentCsvGroup)
                    matchedGroup = None
                    matchedParent = None
                    for sceneGroup in Globals.Groups:
                        if sceneGroup.Name == objectGroupName:
                            matchedGroup = sceneGroup
                            for matchedObject in matchedObjects:
                                sceneGroup.ConnectSrc(matchedObject)
                        elif sceneGroup.Name == csvGroup:
                            matchedGroup = sceneGroup
                        if sceneGroup.Name == parentCsvGroup:
                            matchedParent = sceneGroup
                    if not matchedParent == None and not matchedGroup == None:
                        matchedParent.ConnectSrc(matchedGroup)
    
    return True



def AmbientStory_GeoSetup():

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: rs_ConstraintsFolder
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    def rs_ConstraintsFolder():

        lFolder = None

        for iFolder in Globals.gFolders:
            if iFolder.Name == 'Constraints 1':
                lFolder = iFolder

        if lFolder == None:
            lPlaceholder = FBConstraintRelation('Remove_Me')
            lFolder = FBFolder("Constraints 1", lPlaceholder)
            lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Folders"

            FBSystem().Scene.Evaluate()

            lPlaceholder.FBDelete()

        return lFolder

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ## Description: Get a list of Geo nulls in scene
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    geoList = []
    accList = []
    propList = []
    oldList = []
    unknownList = []
    
    geoTypes = ["accs_",
                "berd_",
                "decl_",
                "feet_",
                "hair_",
                "hand_",
                "head_",
                "jbib_",
                "lowr_",
                "task_",
                "teef_",
                "uppr_",
                "p_lwrist_",
                "p_rwrist_",
                "p_head_",
                "p_eyes_",
                "p_ears_",
                ]
    
    geoDict = {}

    lMasterArray = []
    fileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    
    lHead = None
    lHead = FBFindModelByLabelName("SKEL_Head")

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    lGeoDummy = None
    for iScene in lSceneList:
        if iScene.Name.lower() == fileName.lower():
            lGeoDummy = iScene
        elif iScene.Name == "Geometry":
            lGeoDummy = iScene

    if lGeoDummy:
        Scene.GetChildren(lGeoDummy, geoList, "", False)

    if len(geoList) > 1:
        for geo in geoList:
            geoName = geo.Name
            if geoName[:4].lower() == "old_":
                oldList.append(geo)
                continue
            
            # Determine the geotype by taking the first 5, 7 or 9 characters and comparing them to the geotype list
            geoType = None
            
            geoPrefix = geoName[:5].lower()
            if not geoPrefix in geoTypes:
                geoPrefix = geoName[:7].lower()
                if not geoPrefix in geoTypes:
                    geoPrefix = geoName[:9].lower()
            
            if geoPrefix in geoTypes:
                geoType = geoPrefix
            
            if geoType:
                # variation number should be the three numbers after the geo type - usually 000, 001, etc
                geoVarNo = geoName.lower().split(geoType)[1][:3]
                varNo = None
    
                # Add accessories, props to their own groups
                if geoType == "accs_":
                    accList.append(geo)
                elif geoType == "berd_":
                    accList.append(geo)
                elif "p_" in geoType:
                    propList.append(geo)
                
                # Determine the variation number by removing preceiding "0" characters
                elif geoVarNo[:2] == "00":
                    varNo = geoVarNo[2]
                elif geoVarNo[:1] == "0":
                    varNo = geoVarNo[1:]
                else:
                    varNo = geoVarNo
                
                if varNo != None:
                    # Add to the geo dictionary
                    if varNo not in geoDict.keys():
                        geoDict[varNo] = {}
                        geoDict[varNo][geoType] = geo
                    else:
                        geoDict[varNo][geoType] = geo
            else:
                unknownList.append(geo)
    
    # Set up our main groups, present on every character
    lMasterGroup = FBGroup("GeoVariations")
    lAccGroup = FBGroup("accessories")
    lPropsGroup = FBGroup("props")
    lDefaultHeadGroup  = FBGroup("default_head")
    lDefaultUpprGroup  = FBGroup("default_uppr")
    lDefaultHandGroup  = FBGroup("default_hand")
    lDefaultLowrGroup  = FBGroup("default_lowr")
    
    # Connect the groups up to the main character geo group
    lMasterGroup.ConnectSrc(lAccGroup)
    lMasterGroup.ConnectSrc(lPropsGroup)
    lMasterGroup.ConnectSrc(lDefaultHeadGroup)
    lMasterGroup.ConnectSrc(lDefaultUpprGroup)
    lMasterGroup.ConnectSrc(lDefaultHandGroup)
    lMasterGroup.ConnectSrc(lDefaultLowrGroup)
    
    # Add accessories, props to their own groups
    for geo in accList:
        lAccGroup.ConnectSrc(geo)
    for geo in propList:
        lPropsGroup.ConnectSrc(geo)
    
    if len(geoDict) == 0:
        return
    
    # Loop through the variations to add the geo objects to the appropriate variation groups
    for i in range(len(geoDict)):
        variation = str(i)
        if variation in geoDict.keys():
            # Create the main variation group
            varGroup = FBGroup("var_" + variation)
            lMasterGroup.ConnectSrc(varGroup)
            
            # Add each object into the variation group
            for geoType in geoDict[variation].keys():
                geoObject = geoDict[variation][geoType]
            
                # Add everything from the 000 variation to the default variation groups
                if variation == "0":
                    if geoType == "head_":
                        lDefaultHeadGroup.ConnectSrc(geoObject)
                    elif geoType == "teef_":
                        lDefaultHeadGroup.ConnectSrc(geoObject)
                    elif geoType == 'eyes_':
                        lDefaultHeadGroup.ConnectSrc(geoObject)
                    elif geoType == "hand_":
                        lDefaultHandGroup.ConnectSrc(geoObject)
                    elif geoType == "uppr_":
                        lDefaultUpprGroup.ConnectSrc(geoObject)
                    elif geoType == "jbib_":
                        lDefaultUpprGroup.ConnectSrc(geoObject)
                    elif geoType == "lowr_":
                        lDefaultLowrGroup.ConnectSrc(geoObject)
                
                varGroup.ConnectSrc(geoObject)
    
    # Add unknown objects to their own group
    if len(oldList) > 0:
        oldGroup = FBGroup("var_old")
        for geo in oldList:
            oldGroup.ConnectSrc(geo)    
        # Add the unknown group to the
        lMasterGroup.ConnectSrc(oldGroup)
    
    # Add unknown objects to their own group
    if len(unknownList) > 0:
        unknownGroup = FBGroup("var_unknown")
        for geo in unknownList:
            unknownGroup.ConnectSrc(geo)    
        # Add the unknown group to the
        lMasterGroup.ConnectSrc(unknownGroup)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ## Description: Add 'Acc' and 'P_Head' Objects to the SKEL_Head via constraint
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    lGeoPropDict = {
                    'p_he':'SKEL_Head',
                    'p_ey':'SKEL_Head',
                    'p_ea':'SKEL_Head',
                    'p_lwr':'RB_L_ForeArmRoll',
                    'p_rwr':'RB_R_ForeArmRoll',
                    'p_lh':'SKEL_Pelvis',
                    'p_rh':'SKEL_Pelvis',
                    'p_lf':'SKEL_L_Foot',
                    'p_rf':'SKEL_R_Foot'
                    }

    if len(propList) >= 1:
        if lHead != None:

            lPlaceholder = FBConstraintRelation('Remove_Me')
            lFolder = FBFolder("Geo_Prop_Constraints", lPlaceholder)
            lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Folders"
            lPlaceholder.FBDelete()

            for i in range(len(propList)):
                for lProp, lJoint in lGeoPropDict.iteritems():
                    if lProp in propList[i].Name.lower():
                        lParentChildConstraint = FBConstraintManager().TypeCreateConstraint(3)
                        lParentChildConstraint.Name = propList[i].Name + "_Attach_PC"
                        #FBSystem().Scene.Constraints.append(lParentChildConstraint)
                        lParentChildConstraint.ReferenceAdd (0,propList[i])
                        lParentChildConstraint.ReferenceAdd (1,FBFindModelByLabelName(lJoint))
                        lParentChildConstraint.Active = True

                        FBSystem().Scene.Evaluate()

                        lFolder.Items.append(lParentChildConstraint)

                        lTag = lParentChildConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                        lTag.Data = "rs_Constraints"

            lConFolder = rs_ConstraintsFolder()
            lConFolder.Items.append(lFolder)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ## Description: Add to Character Group if it exists
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    lCharacterGroup = None
    lVar0Group = None
    for iGroup in Globals.gGroups:
        if iGroup.Name == fileName:
            lCharacterGroup = iGroup
        if iGroup.Name == 'var_0':
            lVar0Group = iGroup

    if lCharacterGroup != None:
        lCharacterGroup.ConnectSrc(lMasterGroup)
    if lVar0Group != None:
        lMasterGroup.Show = False
        lDefaultHeadGroup.Show = True
        lDefaultUpprGroup.Show = True
        lDefaultHandGroup.Show = True
        lDefaultLowrGroup.Show = True
        lVar0Group.Show = False
        lVar0Group.Show = True

    Globals.gScene.Evaluate()

    lMasterGroup.Pickable = False
    lMasterGroup.Transformable = False
