import os

import glob

import xml.dom.minidom as minidom

from PySide import QtGui

from RS import Config, Perforce


def GetPedsMetaPath():
    '''
    peds meta path and sync
    '''
    pedsMeta = r'X:\rdr3\assets\export\data\peds.pso.meta'
    Perforce.Sync(pedsMeta)
    return pedsMeta

def GetExprSetPath():
    '''
    
    '''
    # expressions set path and sync
    exprSetPath = r'X:\rdr3\build\dev\common\data\anim\expression_sets\expression_sets.xml'
    Perforce.Sync(exprSetPath)
    return exprSetPath

def GetExprDirectoryFolder():
    # expressions directory and sync
    exprDir = os.path.join(Config.Project.Path.Root,
                            "assets",
                            "anim",
                            "expressions")
    #Perforce.Sync(["-f", os.path.join(exprDir, "...")])
    return exprDir

def getChildrenByTag(parent, childName):
    # def for getting child nodes
    return [node for node in parent.childNodes if not isinstance(node, minidom.Text) and node.tagName == childName]

def GetExpressionPathList(characterNamespace):
    exprList = []
    pedsMeta = GetPedsMetaPath()
    exprSetPath = GetExprSetPath()
    exprDir = GetExprDirectoryFolder()
    if os.path.exists(pedsMeta):
        # parse xml
        xmldoc = minidom.parse(pedsMeta)  
        # iterate through xml starting at the top
        exprSetName = 'null'
        exprDictName = 'null'
        exprName = 'null'
        roots = xmldoc.getElementsByTagName("CPedModelInfo__InitDataList")
        # if expression set is 'null', get expressions dictionary name from 'ExpressionDictionaryName' 
        # tag and the xml from ExpressionName tag 
        for root in roots:
            datas = getChildrenByTag(root, 'InitDatas')
            for data in datas:
                items = getChildrenByTag(data, 'Item')
                for item in items:
                    name = str(getChildrenByTag(item, "Name")[0].childNodes[0].nodeValue)
                    if name == characterNamespace:
                        exprSetName = str(getChildrenByTag(item, "ExpressionSetName")[0].childNodes[0].nodeValue)
                        exprDictName = str(getChildrenByTag(item, "ExpressionDictionaryName")[0].childNodes[0].nodeValue)
                        exprName =  str(getChildrenByTag(item, "ExpressionName")[0].childNodes[0].nodeValue)
                        break

        if exprSetName != 'null':
            dictName = 'null'
            # proceed with expr Setup, there will be an array of expr files to gather - we need to find which ones in the expression_set.xml
            if os.path.exists(exprSetPath):
                # parse xml
                xmldoc = minidom.parse(exprSetPath)
                # iterate through xml starting at the top
                roots = xmldoc.getElementsByTagName("fwExpressionSetManager")
                for root in roots:
                    exprSets = getChildrenByTag(root, 'expressionSets')
                    for exprSet in exprSets:
                        items = getChildrenByTag(exprSet, 'Item')
                        for item in items:
                            if item.getAttribute('key') == exprSetName:
                                dictName = str(getChildrenByTag(item, "dictionaryName")[0].childNodes[0].nodeValue)
                                expressions = getChildrenByTag(item, "expressions")
                                for expression in expressions:
                                    exprItems = getChildrenByTag(expression, "Item")
                                    exprNameList = []
                                    for exprItem in exprItems:
                                        exprNameList.append(str(exprItem.childNodes[0].nodeValue))
                                    break

            for exprName in exprNameList:
                # if the expression name is the same as the dict name, in the expressions sets xml, we want all expressions in that folder.
                if exprName == dictName:
                    exprFilePath = os.path.join(exprDir,
                                                dictName,
                                                'Components') 
                    Perforce.Sync(["-f", os.path.join(exprFilePath, "...")])
                    expressionFiles = glob.glob('{0}\*.xml'.format(exprFilePath))
                    for expressionFile in expressionFiles: 
                        exprList.append(expressionFile)
                else:
                    exprFilePath = os.path.join(exprDir,
                                                dictName,
                                                '{0}.xml'.format(exprName)) 
                    Perforce.Sync(exprFilePath) 
                    if os.path.exists(exprFilePath):
                        exprList.append(exprFilePath)
            
        elif exprDictName != 'null' and exprName!= 'null':
            # proceed with expr setup, will only need one expr file to gather in this scenario
            exprFilePath = os.path.join(exprDir,
                                        exprDictName,
                                        '{0}.xml'.format(exprName))
            Perforce.Sync(exprFilePath)
            if os.path.exists(exprFilePath):
                exprList.append(exprFilePath)

        else:
            # unable to find any expressions for this character
            result = QtGui.QMessageBox.warning(None,
                                               "Warning",
                                               "Unable to find Expressions for this character.",
                                               QtGui.QMessageBox.Ok)

    return exprList