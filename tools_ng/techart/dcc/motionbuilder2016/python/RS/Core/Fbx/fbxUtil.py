import fbx
import os
import re

from RS import Globals, Perforce
from RS.Core.Fbx import fbxProcessor

class TickConverter(object):
    """
    Utility class to handle converting tick values read via the FBX SDK into ones compatible with Motionbuilder's API
    and vice-versa.
    """
    FBXSDK_TICKS_PER_MILLISECOND = 46186158

    if Globals.System.Version < 19000:
        PYFBSDK_TICKS_PER_MILLISECOND = 46186158
    else:
        # Tick rate was changed from the value above to this in Motionbuilder 2019 according to Autodesk
        PYFBSDK_TICKS_PER_MILLISECOND = 141120

    FBXSDK_TO_PYFBSDK_TICKS = float(PYFBSDK_TICKS_PER_MILLISECOND) / FBXSDK_TICKS_PER_MILLISECOND
    PYFBSDK_TO_FBXSDK_TICKS = float(FBXSDK_TICKS_PER_MILLISECOND) / PYFBSDK_TICKS_PER_MILLISECOND

    @classmethod
    def fbxsdkTicksToPyfbsdkTicks(cls, fbxSdkTicks):
        """
        Convert from FBX SDK ticks to Motionbuilder API ticks.
        
        Args:
            fbxSdkTicks (long): Ticks read from the FBX SDK
            
        Returns:
            long: ticks equivalent to the input but converted for use in Motionbuilder's pyfbsdk.
        """
        return long(fbxSdkTicks * cls.FBXSDK_TO_PYFBSDK_TICKS)

    @classmethod
    def pyfbsdkTicksToFbxsdkTicks(cls, pyfbsdkTicks):
        """
        Convert from Motionbuilder API ticks to FBX SDK ticks.

        Args:
            fbxSdkTicks (long): Ticks read from the Motionbuilder API.

        Returns:
            long: ticks equivalent to the input but converted for use in the FBX SDK.
        """
        return long(pyfbsdkTicks * cls.PYFBSDK_TO_FBXSDK_TICKS)


def syncFbxTextures(filePath):
    """
    Convenience method to extract all textures path in a .FBX file, and sync them with perforce if they do not exists.
    Args:
        filePath (str): filepath to a FBX file

    """
    regex = re.compile(r"\.fbx$", re.IGNORECASE)
    if not regex.search(filePath):
        return
    with fbxProcessor.BaseFbxProcessorContext() as processor:

        processor.loadFbx(filePath)

        if not processor.fbxScene:
            return
        # get textures int he ref scene
        textureArray = fbx.FbxTextureArray()
        # fill array with textures
        processor.fbxScene.FillTextureArray(textureArray)

        # loop through textures and file paths
        toSync = []
        for i in range(0, textureArray.GetCount()):
            texture = textureArray.GetAt(i)
            if texture.ClassId == fbx.FbxFileTexture.ClassId:
                texturePath = texture.GetFileName()
                if not os.path.exists(texturePath) and texturePath:
                    # sync texture paths
                    toSync.append(texturePath)
        if toSync:
            Perforce.Sync(toSync)
