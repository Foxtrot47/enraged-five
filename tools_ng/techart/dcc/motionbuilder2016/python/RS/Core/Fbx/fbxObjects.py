#Fbx modules
import FbxCommon
from FbxCommon import FbxManager, FbxScene, FbxIOSettings, IOSROOT, FbxImporter, FbxConstraint, FbxNode, FbxDouble3, FbxExporter
import fbx
import imp

#RS modules
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize

#Python modules
import collections
import sys

#RS modules
if 'X:\\rdr3\\tools\\release\\techart\\script\\python\\packages' in sys.path:
  sys.path.remove('X:\\rdr3\\tools\\release\\techart\\script\\python\\packages')
if 'X:\\rdr3\\tools\\release\\bin\\python\\Python27\\x64\\assemblies' in sys.path:
  sys.path.remove('X:\\rdr3\\tools\\release\\bin\\python\\Python27\\x64\\assemblies')

sys.path.append('X:\\rdr3\\tools\\release\\dcc\\current\\motionbuilder2016\\assemblies\\x64')
sys.path.append('X:\\wildwest\\dcc\\motionbuilder\\python\\')
sys.path.append('X:\\wildwest\\dcc\\motionbuilder\\python\\external')


@memorize.memoized
def getFbxInitialization():
    '''
    Gets the initialization of the fbx top module objects.

    :return: FbxManager, FbxScene
    '''

    #Need to initialize the fbxmanager and fbxscene
    return FbxCommon.InitializeSdkObjects()


def getHandle(filepath):
    '''
    Gets the fbx scene handle.

    :param filepath: <str> The filepath that will be initialized by the FbxImporter
    :return: scene <FbxScene>
    '''

    #Create an fbx manager and IO settings
    fbxManager = getFbxInitialization()[0].Create()
    ios = FbxIOSettings.Create(fbxManager, IOSROOT)

    #Create the importer and initialize it
    importer = FbxImporter.Create(fbxManager, "")
    importer.Initialize(filepath, -1, ios)

    #Create an FbxScene and import the scene
    scene = getFbxInitialization()[1].Create(fbxManager, "myScene")
    importer.Import(scene)
    importer.Destroy()

    return scene


def getAnimStack(handle):
	return handle.GetCurrentAnimationStack()


def getAnimLayerCount(animStack):
	return animStack.GetMemberCount()


def getAnimLayers(animStack):
	animLayers = []

	for idx in xrange(getAnimLayerCount(animStack)):
		animLayers.append(animStack.GetMember(idx))

	return animLayers


def getAnimCurve(node, animLayer, transform='', axis=''):
	if not transform:
		return None

	if transform == 'translation' or transform.lower() == 'translation':
		if axis == 'x':
			return node.LclTranslation.GetCurve(animLayer, True)
		elif axis == 'y':
			return node.LclTranslation.GetCurve(animLayer, True)
		elif axis == 'z':
			return node.LclTranslation.GetCurve(animLayer, True)

	elif transform == 'rotation' or transform.lower() == 'rotation':
		if axis == 'x':
			return node.LclRotation.GetCurve(animLayer, True)
		elif axis == 'y':
			return node.LclRotation.GetCurve(animLayer, True)
		elif axis == 'z':
			return node.LclRotation.GetCurve(animLayer, True)

	else:
		if axis == 'x':
			return node.LclScaling.GetCurve(animLayer, True)
		elif axis == 'y':
			return node.LclScaling.GetCurve(animLayer, True)
		elif axis == 'z':
			return node.LclScaling.GetCurve(animLayer, True)


def getCurveKey(curve, frame):
	return curve.KeyGet(frame)


def getKeyValue(curve, index):
	return curve.KeyGetValue(index)


def getKeyInterpolation(curveKey, index):
	return curveKey.GetInterpolation(index)


def getKeyTangentMode(curveKey, index):
	return curveKey.KeyGetTangentMode(index)


def getNodeAttrs(node):
	return node.GetNodeAttribute()


def getCameras(handle):
	cameras = [handle.GetMember(idx) for idx in xrange(handle.GetMemberCount()) if isinstance(handle.GetMember(idx), FbxCommon.FbxCamera)]

	return cameras


def addNewAnimStack(handle, stackName):
	stack = fbx.FbxAnimStack.Create(handle, stackName)
	return stack


def addTakes(handle, animStack, takeName):
	newLayer = fbx.FbxAnimLayer.Create(handle, takeName)
	return newLayer


def getTakes(filepath):
	'''
	Gets all takes within an fbxfile.

	:param filepath: <str> The filepath to check
	:return: stackList <list> The names of the takes contained in the fbx file
	'''

	(manager, scene) = FbxCommon.InitializeSdkObjects()
	scenePath = filepath
	FbxCommon.LoadScene(manager, scene, scenePath)

	stackClassId = fbx.FbxAnimStack.ClassId
	stackCount = scene.GetSrcObjectCount(stackClassId)
	stackList = [scene.GetSrcObject(stackClassId, idx).GetName() for idx in xrange(stackCount)]

	if not stackList:
		with open(filepath) as fbxFile:
			stackList.extend(fbxFile.readlines())

	return stackList


def getConstraints(handle):
    '''
    Gets all constraints in an fbx file.

    :param handle: <fbxScene> The FbxScene handle
    :return: constraints <list> List of constraints
    '''

    #Troll through the handle fbxScene, and compare the instance is an 'FbxConstraint'
    constraints = [handle.GetMember(idx) for idx in xrange(handle.GetMemberCount()) if isinstance(handle.GetMember(idx), FbxCommon.FbxConstraint)]

    return constraints


def commonConstraints(files):
    '''
    Returns all constraints that match from given files.

    :param files: <list> List of fbx files to check
    :return: <list> All constraints that match from the different fbx files
    '''

    overallConstraints = []
    commConstraints = []

    for file in files:
        handle = getHandle(file)
        constraints = [handle.GetMember(idx) for idx in xrange(handle.GetMemberCount()) if
                       isinstance(handle.GetMember(idx), FbxCommon.FbxConstraint)]

        overallConstraints += constraints

    return [item for item, count in collections.Counter(overallConstraints).items() if count > 1]


def getConstraintName(constraint):
    '''
    Gets the name of the constraint within the fbx file.

    :param constraint: <FbxConstraint> The constraint object
    :return: constraint.GetName() <str> Name of constraint
    '''

    return str(constraint.GetName())


def getConstraintType(constraint):
    '''
    Returns the constraint type

    :param constraint: <FbxConstraint> The constraint to query
    :return: <int> The index of the constraint type
    '''

    return constraint.GetConstraintType()


def getNodes(handle):
    '''
    Gets all nodes in a scene, which DOES NOT include things like constraints, textures, videos, and so on.

    :param handle: <fbxScene> The handle fbx scene
    :return: nodes <list> The physical nodes within an fbx file.
    '''

    nodes = [handle.GetNode(idx) for idx in xrange(handle.GetNodeCount())]

    return nodes


def getCharacters(handle):
    '''
    Gets all characters within an fbx file.

    :param handle: <fbxScene> The handle fbx scene
    :return: chars <list> The list of characters within an fbx file
    '''

    chars = [handle.GetCharacter(idx) for idx in xrange(handle.GetCharacterCount())]

    return chars


def getMaterials(handle):
    '''
    Gets the materials within an fbx file.

    :param handle: <fbxScene> The handle fbx scene
    :return: materials <list> The list of materials within an fbx file
    '''

    materials = [handle.GetMaterial(idx) for idx in xrange(handle.GetMaterialCount())]

    return materials


def getGeometry(handle):
    '''
    Gets the geometry (i.e. FBMesh) within an fbx file.

    :param handle: <fbxScene> The handle fbx scene
    :return: geometry <list> The list of geometry within an fbx file
    '''

    geometry = [handle.GetGeometry(idx) for idx in xrange(handle.GetGeometryCount())]

    return geometry


def getCharPoses(handle):
    '''
    Gets all characters poses within an fbx file

    :param handle: <fbxScene> The handle fbx scene
    :return: poses <list> The list of poses within an fbx file
    '''

    poses = [handle.GetCharacterPose(idx) for idx in xrange(handle.GetCharacterPoseCount())]

    return poses


def getTextures(handle):
    '''
    Gets all textures within an fbx file

    :param handle: <fbxScene> The handle fbx scene
    :return: textures <list> The list of textures within an fbx file
    '''

    textures = [handle.GetTexture(idx) for idx in xrange(handle.GetTextureCount())]

    return textures


def getVideos(handle):
    '''
    Gets all video objects within an fbx file

    :param handle: <fbxScene> The handle fbx scene
    :return: videos <list> The list of videos within an fbx file
    '''

    videos = [handle.GetVideo(idx) for idx in xrange(handle.GetVideoCount())]

    return videos


def getPropertyValue(node, propertyName, propertyType):
	'''
	Get the value of the passed in property name, from the passed in node.

	:param node: <FbxNode> The fbx node that contains the property
	:param propertyName: <str> The name of the property you are looking for
	:param propertyType: <fbx.FbxPropertyType> (ex. fbx.FbxPropertyDouble)
	:return: The value of the property. Type differs on the type you established it in the "propertyType"
	'''

	prop = node.FindProperty(propertyName)
	return propertyType(prop).Get()


def setKeyInterpolation(curveKey, index, interpolationType=''):
	pass


def setKeyTangents(curveKey, index, tangentMode=''):
	pass


def addNode(scene, nodeName, **kwargs):
    '''
    Allows addition of nodes into fbx file within need for Motionbuilder.

    :param scene: <fbxScene> The scene (file) that will be affected
    :param nodeName: <str> The name of the node being added
    :param kwargs: <kwargs> Any inherited kwargs
    :return: newNode <object> The new object added to the fbx file
    '''

    # Obtain a reference to the scene's root node We only need scale and translation.
    scaling = kwargs["scaling"]
    location = kwargs["location"]
    newNode = FbxNode.Create(scene, nodeName)
    newNode.LclScaling.Set(FbxDouble3(scaling[0], scaling[1], scaling[2]))
    newNode.LclTranslation.Set(FbxDouble3(location[0], location[1], location[2]))

    # Create a new node in the scene.
    return newNode


def getASCIIFormatIndex(manager):
    '''
    Obtain the index of the ASCII export format.

    :param manager: <FbxManager> The manager to manipulate the fbx
    :return: formatIndex <int> The index of the ascii format
    '''

    # Count the number of formats we can write to.
    numFormats = manager.GetIOPluginRegistry().GetWriterFormatCount()

    # Set the default format to the native binary format.
    formatIndex = manager.GetIOPluginRegistry().GetNativeWriterFormat()

    # Get the FBX format index whose corresponding description contains "ascii".
    for i in xrange(0, numFormats):

        # First check if the writer is an FBX writer.
        if manager.GetIOPluginRegistry().WriterIsFBX(i):

            # Obtain the description of the FBX writer.
            description = manager.GetIOPluginRegistry().GetWriterFormatDescription(i)

            # Check if the description contains 'ascii'.
            if 'ascii' in description:
                formatIndex = i
                break

    # Return the file format.
    return formatIndex


def saveScene(filename, fbxManager, fbxScene, asASCII=False):
    '''
    Save the scene using the Python FBX API.

    :param filename: <str> The filename
    :param fbxManager: <FbxManager> The manager to manipulate the fbx
    :param fbxScene: <FbxScene> The fbx scene of the file
    :param asASCII: <bool> Is it ascii?
    :return: None
    '''

    exporter = FbxExporter.Create(fbxManager, '')

    if asASCII:
        #DEBUG: Initialize the FbxExporter object to export in ASCII.
        asciiFormatIndex = getASCIIFormatIndex(fbxManager)
        isInitialized = exporter.Initialize(filename, asciiFormatIndex)
    else:
        isInitialized = exporter.Initialize(filename)

    #If it isn't initialized, raise an exception
    if isInitialized == False:
        raise Exception('Exporter failed to initialize. Error returned: ' + str(exporter.GetLastErrorString()))

    #Export the fbx scene, then destroy to exporter
    exporter.Export(fbxScene)
    exporter.Destroy()
