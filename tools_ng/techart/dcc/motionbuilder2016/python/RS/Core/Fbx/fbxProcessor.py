import os
import fbx


class FbxProcessorError(Exception):
    """Exception type raised if errors occur while processing an FBX file."""
    pass


class BaseFbxProcessorContext(object):
    """
    Base class for implementing FBX reading/writing related functionality.
    Handles the common functionality like loading, saving and logging and implements a context to
    ensure FBXSDK objects are cleaned up.
    Multiple fbx files can be processed within one processor context.
    """
    LOG_CONTEXT = "FbxProcessor"
    EXCEPTION_TYPE = FbxProcessorError

    def __init__(self, ulog=None, verbose=False):
        """
        Initialise the fbx processor instance.

        Args:
            ulog (UniversalLog): Optional universal log instance to record messages to.
            verbose (bool): If True more verbose messages will be logged during processing.
        """
        self.fbxFilePath = None
        self.fbxScene = None
        self.ulog = ulog
        self._verbose = verbose

        self.sdkManager = fbx.FbxManager.Create()
        if not self.sdkManager:
            msg = "Failed to initialise FBXSDK manager"
            self._logError(msg, throw=True)

        ioSettings = fbx.FbxIOSettings.Create(self.sdkManager, fbx.IOSROOT)
        self.sdkManager.SetIOSettings(ioSettings)

        self._setDefaultIoSettings()

    def _setDefaultIoSettings(self):
        ioSettings = self.getIoSettings()
        ioSettings.SetBoolProp(fbx.EXP_FBX_EMBEDDED, False)

    def getIoSettings(self):
        """Returns a reference to the active FbxIOSettings"""
        return self.sdkManager.GetIOSettings()

    def _logMessage(self, message):
        if not self._verbose:
            return

        if self.ulog:
            self.ulog.LogMessage(message, self.LOG_CONTEXT)

    def _logWarning(self, message):
        if self.ulog:
            self.ulog.LogWarning(message, self.LOG_CONTEXT)

    def _logError(self, message, throw=False):
        if self.ulog:
            self.ulog.LogError(message, self.LOG_CONTEXT)
        if throw:
            raise self.EXCEPTION_TYPE(message)

    def _logDebug(self, message):
        if not self._verbose:
            return

        if self.ulog:
            self.ulog.LogDebug(message, self.LOG_CONTEXT)

    def loadFbx(self, fbxFilePath=None):
        """
        Loads the given FBX file path using the fbxsdk

        Args:
            fbxFilePath: File path to the FBX file to load
        """
        self.fbxFilePath = fbxFilePath

        if not self.fbxFilePath:
            msg = "Failed to load FBX file. No file path given."
            self._logError(msg, throw=True)

        if not os.path.isfile(self.fbxFilePath):
            msg = "Failed to load FBX file. File doesn't exist: \"{}\"".format(self.fbxFilePath)
            self._logError(msg, throw=True)

        if isinstance(self.fbxScene, fbx.FbxScene):
            # Destroy the current scene fbxsdk objects if one is already loaded
            self._logMessage("Clearing existing scene")
            self.fbxScene.Destroy()
            self.fbxScene = None

        self._logMessage("Loading scene with fbxsdk: {}".format(self.fbxFilePath))

        self.fbxScene = fbx.FbxScene.Create(self.sdkManager, "")

        importer = fbx.FbxImporter.Create(self.sdkManager, "")
        try:
            result = importer.Initialize(self.fbxFilePath, -1, self.sdkManager.GetIOSettings())
            if result:
                result = importer.Import(self.fbxScene)
        except Exception as ex:
            self._logError("Error while loading FBX: {}".format(ex))
            result = False
        finally:
            importer.Destroy()

        if not result:
            self.fbxScene.Destroy()
            self.fbxScene = None
            self._logError("Failed to load fbx: \"{}\"".format(self.fbxFilePath), throw=True)

        self._logMessage("Loading scene complete")

    def saveFbx(self, outputPath):
        """
        Writes the current fbxsdk scene data to a binary fbx file on disk.

        Args:
            outputPath (str): The file path to write the fbx scene to.
        """
        if not self.fbxScene:
            msg = "No scene data to save."
            self._logError(msg, throw=True)

        self._logMessage("Saving scene with fbxsdk: {}".format(outputPath))

        fileFormat = self.sdkManager.GetIOPluginRegistry().GetNativeWriterFormat()
        exporter = fbx.FbxExporter.Create(self.sdkManager, "")
        try:
            result = exporter.Initialize(outputPath, fileFormat, self.sdkManager.GetIOSettings())
            if result:
                result = exporter.Export(self.fbxScene)
        except Exception as ex:
            self._logError("Error while saving FBX: {}".format(ex))
            result = False
        finally:
            exporter.Destroy()

        if not result:
            self._logError("Failed to save FBX: \"{}\"".format(outputPath), throw=True)
        self._logMessage("Saving scene complete")
    
    def cleanup(self):
        """Releases all fbxsdk objects from memory."""
        self._logMessage("Release fbxsdk objects")

        if self.fbxScene:
            self.fbxScene.Destroy()
            self.fbxScene = None

        if self.sdkManager:
            self.sdkManager.Destroy()
            self.sdkManager = None

    def __enter__(self):
        """Called when entering the context when used as a context manager."""
        return self

    def __exit__(self, exceptionType, value, traceback):
        """Called when leaving the context when used as a context manager."""
        self.cleanup()
