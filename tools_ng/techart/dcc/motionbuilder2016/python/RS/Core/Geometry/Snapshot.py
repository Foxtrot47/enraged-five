"""
Interface for the snapshotting the current geo state of an object.

Examples:
from RS.Core.Geometry import Snapshot

# Snapshot a mesh
from RS.Utils import Scene
item1 = Scene.FindModelByName ("P_C_Horse_01_head_000 Head")
item2 = Scene.FindModelByName("P_C_Horse_01_uppr_002 Leather Bridal")
Snapshot.Snapshot.shapshotModel(item1) # Single Mesh
Snapshot.Snapshot.shapshotModels([item1, item2]) # Multiple Meshes

# Snapshot a Character
from RS import Globals
charDict = {char.Name: char for char in Globals.Characters}
Snapshot.Snapshot.shapshotCharacter(charDict["CS_AbigailRoberts-META_OUTFIT_ENDLESS_SUMMER_ALT"])
"""
from PySide import QtCore, QtGui

import pyfbsdk as mobu
import mobuExtender

from RS.Utils.Scene import Character


class Snapshot(object):
    """
    Class to deal with Snap shotting items
    """
    @staticmethod
    def shapshotModel(model, copyName=True, copyNamespace=True, copyVisabilty=True, copyMaterials=True, evalScene=True):
        """
        Static Method
        
        SnapShot a single motion builder Model and its current state on the current frame
        
        args:
            model (FBModel): The fbModel to snapshot
        
        kwargs:
            copyName (bool): if the name should be copied across, with the prefix of Clone
            copyVisabilty (bool): if the source visability should be copied across
            copyMaterials (bool): if the source material should be copied across
            copyNameSpace (bool): if the new model should be added to the orginal models namespace
            evalScene (bool): If scene needs to be evaluated before the model is snapshotted or not
            
        returns:
            an FBModel of the cloned geo
        """
        if not isinstance(model.Geometry, mobu.FBMesh):
            return None 
        
        newModel = mobuExtender.SnapshotDeformedModel(model, evalScene)
        if copyName is True:
            newModel.Name = "Snapshot_{}".format(model.Name)
        
        if copyVisabilty is True:
            newModel.Visibility = model.Visibility

        if copyVisabilty is True:
            for item in model.Textures:
                newModel.Textures.append(item)
            for item in model.Shaders:
                newModel.Shaders.append(item)
            for item in model.Materials:
                newModel.Materials.append(item)
            
        if copyNamespace is True and ":" in model.LongName:
            nameSpace, _ = model.LongName.rsplit(":", 1)
            newModel.LongName = "{}:{}".format(nameSpace ,newModel.Name)
        return newModel

    @classmethod
    def shapshotModels(cls, models, copyName=True, copyNamespace=True):
        """
        Class Method
        
        SnapShot a list of motion builder Models and their current states on the current frame
        
        args:
            list of models (List of FBModel): List of FBModel to snapshot
        
        kwargs:
            copyName (bool): if the name should be copied across, with the prefix of Clone
            copyNameSpace (bool): if the new models should be added to the orginal models namespace
            
        returns:
            list of FBModel of the cloned geo
        """
        returnItems = []
        for item in models:
            newModel = cls.shapshotModel(item, copyName=copyName, copyNamespace=copyNamespace, evalScene=False)
            if newModel is not None:
                returnItems.append(newModel)
        return returnItems
    
    @classmethod
    def shapshotCharacter(cls, char):
        """
        Class Method
        
        Snapshot a single Character and all the meshes that are driven by it at the current frame.
        
        args:
            char (FBCharacter): The character to snapshot
        
        returns:
            list of FBModel of the cloned geo
        """
        modelList = mobu.FBModelList()
        for obj in Character.GetComponentsFromCharacter(char):
            if isinstance(obj, mobu.FBModelSkeleton):
                obj.GetSkinModelList(modelList)
        return cls.shapshotModels([item for item in modelList])
