"""
Description:
    Wrapper functions for interfacing with HB's DatabaseConnection module.
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import uuid
from datetime import datetime

try:
    # Mobu Import
    from RS import Databases

except ImportError:
    # Setup the Mobu directory when used outside mobu
    import os
    import sys

    PYTHON_ROOT = __file__.split("\\RS\\Core")[0] or r"x:\wildwest\dcc\motionbuilder\python"
    EXTERNAL_ROOT = os.path.join(PYTHON_ROOT, "external")

    for path in (PYTHON_ROOT, EXTERNAL_ROOT):
        if path not in sys.path:
            sys.path.append(path)

    # Remove standalone directory from the system path
    sys.path = [path for path in sys.path if "script\\python\\packages" not in path]

    from RS import Databases

# Set Globals
DB_CONN = Databases.NYC.CaptureRenders()


def CreateDbJob(fbxName, userName, fbxPath):
    """
    Creates a new job on the database with standard options.
    Stored procedure params:
       @username nvarchar(50),
       @scene nvarchar(50),
       @status int = 0,
       @jobid nvarchar(64),
       @jobpriority int = 0,
       @fbxpath nvarchar(max) = '',
       @progress nvarchar(50) = '',
       @worker nvarchar(50) = ''

    Arguments:
        fbxName: A string of the job's FBX name.
        userName: A string of the job's username.
    Keyword Arguments:
        jobPriority: An int of the job's priority.
    Returns:
        An int (technically a long) of the jobID, or None if connection fails.
    """

    jobId = uuid.uuid1().int
    cmdTxt = "exec CaptureMonitor_AddJob '{0}', '{1}', {2}, '{3}', '{4}', '{5}'"
    cmdTxt = cmdTxt.format(userName, fbxName, 0, jobId, 0, fbxPath)
    DB_CONN.ExecuteQuery(cmdTxt)
    return jobId


def SubmitJobUpdate(jobId, statusInt, isCanceled, machineName=None):
    """
    Sends job update command to the database.
    Arguments:
        jobId: A long of the jobId to update.
        statusInt: An int of the job's status.
        isCanceled: An bool - True if job canceled, False otherwise.
    Returns:
        True if the command was sent, none otherwise.
    """

# get info for triggerdate, completeddate and worker (machine name)
    triggeredDate = None
    completedDate = None
    # if status id is 1 ("Exporting") we want to add a "Triggered Date"
    if statusInt == 1 and isCanceled == False:
        triggeredDate = datetime.now()
    # if status id is 5 ("Complete") we want to add a "Completed Date"
    if statusInt == 5 and isCanceled == False:
        completedDate = datetime.now()

    # if the following params are none, we want to use whatever is currently in the database
    if triggeredDate == None:
        triggeredDate = CheckJobTag(jobId, "Triggered")
        if triggeredDate == None:
            triggeredDate = ''
    if completedDate == None:
        completedDate = CheckJobTag(jobId, "Completed")
        if completedDate == None:
            completedDate = ''
    if machineName == None:
        machineName = CheckJobTag(jobId, "Worker")
        if machineName == None:
            machineName = ''

    cmdTxt = "exec CaptureMonitor_UpdateJob2 '{0}', {1}, {2}, '{3}', '{4}', '{5}'".format(jobId, statusInt, isCanceled, triggeredDate, completedDate, machineName)
    DB_CONN.sql_command(cmdTxt)
    return True


def UpdateDbJob(jobId, statusInt, machineName=None):
    """
    Updates a job's status on the database without altering isCanceled.
    Arguments:
        jobId: A long of the jobId to update.
        statusInt: An int of the job's status.
        machineName: string for name of machine used. if this is None, we ignore adding this param
    """
    if jobId:
        isCanceled = CheckJobTag(jobId, "IsCanceled")
        if isCanceled is not True:
            isCanceled = False

        SubmitJobUpdate(jobId, statusInt, isCanceled, machineName)


def CancelDbJob(jobId):
    """
    Sets a job's isCanceled value to True without altering status.
    Arguments:
        jobId: A long of the jobId to update.
    """
    if jobId:
        statusInt = CheckJobTag(jobId, "Status")
        if statusInt is None:
            statusInt = 0

        SubmitJobUpdate(jobId, statusInt, True)


def ReadDbJobs():
    """
    Reads all jobs on the database and returns the results.
    Returns:
        A list of row objects for the jobs on the database.
    """
    cmdTxt = "exec CaptureMonitor_ReadJobs"
    tableResults = DB_CONN.sql_command(cmdTxt)
    return tableResults


def CheckJobTag(jobId, jobTag):
    """
    Looks up a specific value for a specific jobId on the database.
    Arguments:
        jobId: A long of the jobId to update.
        jobTag: A string of the job value name to lookup.
    Returns:
        The found jobTag's value - can be any type.
    """
    dbJobs = ReadDbJobs()
    for eachJob in dbJobs:
        if long(eachJob.JobID) == jobId:
            return getattr(eachJob, jobTag)

def DebugFlushJobs():
    """
    Debug command to set all jobs to complete and cancel to False.
    Useful for flushing out test data and old jobs.
    """
    dbJobs = ReadDbJobs()
    for eachJob in dbJobs:
        jobId = int(eachJob.JobID)

        SubmitJobUpdate(jobId, 5, False)