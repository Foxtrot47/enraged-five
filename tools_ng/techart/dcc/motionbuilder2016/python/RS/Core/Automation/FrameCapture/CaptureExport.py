"""
Description:
    Utility functions for common export processes in MotionBuilder.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import subprocess
import pyfbsdk as mobu

import RS.Utils.Namespace as rsNamespace
from RS.Core.Export.Export import RexExport
from RS.Core.Automation.Layout import LayoutTools
from RS.Core.Automation.FrameCapture import CapturePaths, CutTools, FileTools
from RS.Core.Camera import CamUtils, RangeTools


def RunTerminalCommand(cmdTxt):
    capPaths = CapturePaths.FastPath()
    cmdProc = subprocess.Popen(cmdTxt, shell=True, cwd=capPaths.projectRoot,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    cmdOut, cmdErr = cmdProc.communicate()
    return cmdOut, cmdErr


def GetAvailableName(newName, nameList):
    """
    Searches a list of existing names for an available dupe name (if dupes exist).
    Arguments:
         newName (string): the base name (non dupe version) to search for dupes with
         nameList (list): a list of strings to search through for an available name
    Returns:
        newName (string): an available name factoring in existing dupes
    """
    newName = newName.split("^")[0]
    dupeNames = [name for name in nameList if name.lower().startswith(newName.lower())]
    if dupeNames:
        if len(dupeNames) == 1:
            newName = "{}^1".format(newName)
        else:
            newName = "{0}^{1}".format(newName, len(dupeNames))
    return newName


def SwapProxyCharacters():
    """
    Swaps character namespace and exporter object any matched items in the proxyCharacterConfig.xml
    This is the new mobu version of the old CutTools module, which currently doesn't support animScenes.
    """
    # Create Proxy Dict and Export Object Names List
    fbxPath = mobu.FBApplication().FBXFileName
    proxyDict = CutTools.GetProxyCharacterDict(fbxPath)
    objectCount = RexExport.GetNumberCutsceneObjects()
    exportObjectNames = [RexExport.GetCutsceneObjectName(index) for index in xrange(objectCount)]

    # Check Each Export Object for Valid Swap
    for index, objectName in enumerate(exportObjectNames):
        proxyMatch = [proxyDict[name] for name in proxyDict.iterkeys() if name.lower() == objectName.lower()]
        modelMatch = mobu.FBFindModelByLabelName("{}:SKEL_ROOT".format(objectName))

        # Valid Swap Found - remove original from exporter and get new character name
        if proxyMatch and modelMatch:
            originalName = modelMatch.OwnerNamespace.Name
            RexExport.RemoveCutsceneObject("{}:SKEL_ROOT".format(originalName))
            newName = GetAvailableName(proxyMatch[0], exportObjectNames)

            # Check Conflicting Namespace - if one already exists we must rename it first
            existingNameSpaces = [nameSpace.Name for nameSpace in mobu.FBSystem().Scene.Namespaces]
            tempName = GetAvailableName(newName, existingNameSpaces)
            if tempName != newName:
                rsNamespace.Rename(newName, tempName)

            # Rename Character and Add to Exporter - and update our names list
            rsNamespace.Rename(originalName, newName)
            RexExport.AddCutsceneCharacter("{}:SKEL_ROOT".format(newName))
            exportObjectNames[index] = newName


def DeleteExtraZips(fbxPath):
    capPaths = CapturePaths.FastPath(fbxPath)
    zipFolder, capZipName = os.path.split(capPaths.zipPath)
    if os.path.exists(zipFolder):
        extraZips = [os.path.join(zipFolder, zipName) for zipName in os.listdir(zipFolder) if zipName.lower() != capZipName.lower()]
        [FileTools.DeletePath(zipPath) for zipPath in extraZips if os.path.getsize(zipPath) / 1024 < 5]


def ExportFbxForCapture(capObj):
    """
    Force our default shot options for capture exports.
    Setup for all capture types before type specific steps.
    Arguments:
        capObj: An instance of the CaptureModel object.
    Returns:
        A bool True after export success, or a string of an error message.
    """
    # Update Status and exportStartTime
    capObj.UpdateTag("mobuStatus", "PREP EXPORTER")
    capObj.UpdateTime("exportStartTime")

    # Run Export - via cinematic or cutscene exporter
    capPaths = CapturePaths.FastPath(capObj.fbxPath)
    if capPaths.isCinematicFbx:
        if capObj.captureType == "FAST":
            exportResults = RunCinematicFast(capObj)
        else:
            exportResults = RunCinematicFull(capObj)
    elif capObj.captureType == "FAST":
        exportResults = RunFastCap(capObj)
    else:
        exportResults = RunFullCap(capObj)

    # Return Export Results
    return exportResults


def RunFullCap(capObj):
    """
    Run full capture specific steps. Used for full and simple captures.
    Arguments:
        cabObj: An instance of the CaptureModel object.
    Returns:
        A results string - "EXPORT SUCCESS", or a string of an error message.
    """
    # Open Export Window
    mobu.ShowToolByName("Rex Rage Cut-Scene Export")

    # Reload or Populate Cut Files
    capPaths = CapturePaths.FastPath(capObj.fbxPath)
    cutsPath = capPaths.cutsPath
    cutXmlPath = os.path.join(cutsPath, "data.cutxml")
    cutPartPath = os.path.join(cutsPath, "data.cutpart")
    if os.path.exists(cutXmlPath) and os.path.exists(cutPartPath):
        RexExport.ImportCutFile(cutXmlPath)
        RexExport.ImportCutPart(cutPartPath)
    else:
        # Autopopulate - or use old version on GTA5
        if capPaths.project == "GTA5":
            LayoutTools.AddItemsToExporter()
        else:
            RexExport.AutoPopulateCutscene()

    # Setup Cut Files via Auto Layout
    LayoutTools.FixMissingHandles()
    LayoutTools.SetExporterShotData()
    LayoutTools.SetSafeLocation()

    # Force Frame Options - rarely old cutfiles with different ranges cause problems
    RexExport.SetCutsceneShotStartRange(0, 0)
    RexExport.SetCutsceneShotEndRange(0, int(capObj.endFrame))
    
    # GTA5 DOF Options
    if capPaths.project == "GTA5":
        dofPassDetected = CamUtils.DetectDofPassComplete()
        RexExport.SetCutsceneShotEnableDOFFirstCut(0, dofPassDetected)

    # Make Cutfiles Writeable
    FileTools.DeletePath(cutsPath)
    FileTools.DeletePath(capPaths.previewPath)

    # Manual Export - exit early before export commands
    if capObj.exportMode == "manual":
        return "MANUAL EXPORT"
    
    # Export Anims
    capObj.UpdateTag("mobuStatus", "EXPORT ANIMS")
    try:
        animsBuilt = RexExport.ExportActiveCutscene(False, False)  # no kwargs as spelling differs between projects
    except:
        animsBuilt = False
    if not animsBuilt:
        return "EXPORT ERROR: Issue during Export Anims"

    # Build Zip
    zipPath = capPaths.zipPath
    zipBuilt = CutTools.BuildZipFromCutsPath(cutsPath, zipPath)
    if not zipBuilt:
        return "EXPORT ERROR: Issue during Rebuild Zips (from tempCuts files)"

    # Delete Extra Zips
    DeleteExtraZips(capObj.fbxPath)

    # Build Preview
    capObj.UpdateTag("mobuStatus", "BUILD PREVIEW")
    try:
        previewBuilt = RexExport.BuildActiveCutscene(False, True)  # kwarg spelling again
    except:
        previewBuilt = False
    if not previewBuilt:
        return "EXPORT ERROR: Issue during Build Preview"

    # Scene Built Successfully
    return "EXPORT SUCCESS"


def RunFastCap(capObj):
    """
    Run fast capture specific steps. Used for fast captures only.
    Arguments:
        cabObj: An instance of the CaptureModel object.
    Returns:
        A bool True after export success, or a string of an error message.
    """
    # Open Export Window
    mobu.ShowToolByName("Rex Rage Cut-Scene Export")

    # Setup Exporter Shot Data
    capPaths = CapturePaths.FastPath(capObj.fbxPath)
    cutsPath = capPaths.cutsPath
    cutPartPath = os.path.join(cutsPath, "data.cutpart")
    RexExport.ImportCutPart(cutPartPath)
    LayoutTools.SetExporterShotData()

    # Remove Extra Cutfiles and Add Export Camera
    RexExport.RemoveAllCutsceneObjects()
    RexExport.AddCutsceneCamera("ExportCamera", addFade=False)

    # Make Cutfiles Writeable
    FileTools.DeletePath(cutsPath)
    FileTools.DeletePath(capPaths.previewPath)

    # Export Camera Anim
    capObj.UpdateTag("mobuStatus", "EXPORT CAM ANIMS")
    camAnimsBuilt = RexExport.ExportCameraAnim(False, False)  # kwarg spelling again
    if not camAnimsBuilt:
        return "EXPORT ERROR: Issue during Export Cam Anims"

    # Update Status and Paths
    capObj.UpdateTag("mobuStatus", "REBUILD ZIP")
    capDataRoot = capPaths.capDataRoot

    # Set Temp Cuts Paths
    tempCutsRoot = os.path.join(os.path.split(cutsPath)[0], "tempCuts")
    tempCutFolder = os.path.join(tempCutsRoot, capObj.fbxName)
    expCamAnimPath = os.path.join(cutsPath, "ExportCamera.anim")
    expCamClipPath = os.path.join(cutsPath, "ExportCamera.clip")

    # Delete Previous Data and Copy Network Cuts Files
    FileTools.DeletePath(tempCutsRoot)
    FileTools.CreateParentFolders(tempCutsRoot)
    dataCutsFolder = os.path.join(capDataRoot, "CutFiles")
    FileTools.CopyFolderContents(dataCutsFolder, tempCutFolder)

    # Copy Export Cam Anim/Clip Files to TempCuts
    FileTools.CopyPath(expCamAnimPath, tempCutFolder)
    FileTools.CopyPath(expCamClipPath, tempCutFolder)

    # TODO - commented out ias copy below - why were we doing this?
    # Copy Previous IAS - if available
    # localIasFolder = CapturePaths.iasRoot
    # dataIasPath = os.path.join(capDataRoot, "IasFiles", "@{}.ias".format(capObj.fbxName))
    # if os.path.exists(dataIasPath):
    #     FileTools.CopyPath(dataIasPath, localIasFolder)

    # Build Zip
    zipPath = capPaths.zipPath
    zipBuilt = CutTools.BuildZipFromCutsPath(tempCutFolder, zipPath)
    if not zipBuilt:
        return "EXPORT ERROR: Issue during Rebuild Zips (from tempCuts files)"

    # Delete Extra Zips
    DeleteExtraZips(capObj.fbxPath)

    # Build Preview
    capObj.UpdateTag("mobuStatus", "BUILD PREVIEW")
    previewBuilt = RexExport.BuildActiveCutscene(False, True)  # kwarg spelling again
    if not previewBuilt:
        return "EXPORT ERROR: Issue during Build Preview"

    # Scene Built Successfully
    return "EXPORT SUCCESS"


def ForceAuthoredScale(xmlPath):
    """ A hacky 'temporary' fix to manually override a flag in .rexcin files that is breaking animation. """
    # todo: potentially need to handle missing or invalid rexcin files?
    from xml.etree import cElementTree as ET
    tree = ET.parse(xmlPath)
    root = tree.getroot()

    # Get authoredScale node
    authoredScaleNode = root.find(".//useAuthoredScale")
    if authoredScaleNode is None:
        # No authoredScale - add new node
        authoredScaleNode = ET.Element("useAuthoredScale")
        previousNode = root.find(".//useParentScale")
        if previousNode is None:
            # No parentScale node - just use 2nd to last node (last tail is different)
            previousNode = [child for child in root.getchildren()][-2]
        # Copy tail and insert
        authoredScaleNode.tail = previousNode.tail
        insertIndex = [index for index, childNode in enumerate(root.getchildren()) if childNode == previousNode]
        root.insert(insertIndex[0] + 1, authoredScaleNode)

    authoredScaleNode.attrib["value"] = "false"
    tree.write(xmlPath)


def ReconcileLiveFile(fbxPath=None):
    """ After updating ranges or adding models to the exporter, a reconcile of the IAS file is now required.
        So after exporting anims, but before building, we need to run this reconcile via a cmd line call.  """
    # Read FbxPath - if none provided
    if not fbxPath:
        fbxPath = mobu.FBApplication().FBXFileName
    capPaths = CapturePaths.FastPath(fbxPath)

    # Reconcile Live File
    smashPath = "X:\\americas\\tools\\release\\bin\\anim\\animscene\\asmash.exe"
    definitionsPath = "x:\\americas\\assets\\dev\\metadata\\definitions\\"
    logPath = "X:\\americas\\tools\\release\\logs\\AnimSceneEditor\\ReconcileLiveFile\\"
    configPath = "x:\\americas\\assets\\dev\\anim\\config\\cinematic_settings.xml"
    inputTxt = "-reconcileAnimSceneFile -liveAnimSceneFile {0} -inputDir {1}".format(capPaths.iasPath, capPaths.cutsRoot)
    argsTxt = "-definitionsDir {0} -logDirectory {1} -cinematicConfigFile {2} -assetsDir {3}".format(definitionsPath, logPath, configPath, capPaths.assetsRoot)
    cmdTxt = "{0} {1} {2}".format(smashPath, inputTxt, argsTxt)
    cmdOut, cmdErr = RunTerminalCommand(cmdTxt)  # ???
    # print "cmdOut:", cmdOut
    # print "cmdErr:", cmdErr
    # TODO: test that this error check actually works
    if cmdErr:
        return "EXPORT ERROR: Issue during Reconcile Live File"
    return True


def FixMissingHandles(modelDict):
    """ Takes a dict of model types and names to add to the cinematic handle file, which are required to export.
        Valid Model Type Names: "Character", "Prop", "Vehicle"
        Example Dict: {"Prop": ["W_SMG_001", "W_SMG_002"]} would add 2 guns to the Prop category in the file. """
    from xml.etree import cElementTree as ET
    fbxPath = mobu.FBApplication().FBXFileName
    capPaths = CapturePaths.FastPath(fbxPath)
    xmlPath = "X:\\americas\\assets\\dev\\anim\\cinematic_handles\\{0}.xml".format(capPaths.missionName)
    if os.path.exists(xmlPath) is False:
        # TODO: potentially need to cover xml not found, maybe create/copy?
        return
    FileTools.MakePathWriteable(xmlPath)
    rootNode = ET.parse(xmlPath)
    handleTypesNode = rootNode.find('.//HandleTypes')
    for modelType, modelNames in modelDict.iteritems():
        # itemNode = handleTypesNode.find('Item/TypeName[text()="{0}"]'.format(modelType))
        handlesDataNode = handleTypesNode.find('Item/[TypeName="{0}"]/HandlesData'.format(modelType))
        for modelName in modelNames:
            itemNode = ET.SubElement(handlesDataNode, "Item")
            for nodeName in ["ModelName", "HandleName"]:
                newNode = ET.SubElement(itemNode, nodeName)
                newNode.text = modelName
    rootNode.write(xmlPath)


def RunCinematicFull(capObj):
    """ Exports all animations in an FBX, then builds them into game data. """
    # Get CapPaths
    capPaths = CapturePaths.FastPath(capObj.fbxPath)
    iasPath = capPaths.iasPath
    rexPath = "{0}\\{1}.rexcin".format(capPaths.cutsPath, capPaths.fbxName.upper())

    # Sync or Create IAS
    checkStatus = RangeTools.CheckPathCmdLine(iasPath)
    if checkStatus is True:
        RangeTools.SyncPathCmdLine(iasPath, force=True)
    else:
        pass
        # todo: write tool to create new cinematic ias locally

    # Make Cuts/Ias Writeable
    # FileTools.DeletePath(cutsPath)  # todo: skipping delete, don't remember why we deleted previously?
    FileTools.MakePathWriteable(capPaths.cutsPath)
    FileTools.MakePathWriteable(capPaths.iasPath)

    # Update Frame Xml
    exportCamera = mobu.FBFindModelByLabelName("ExportCamera")
    if not exportCamera:
        return "EXPORT ERROR: Export camera could not be found."
    endFrame = int(capObj.endFrame) * 2
    RangeTools.CaptureRangeXml(capObj.fbxPath, exportCamera.Name, endFrame)

    # Force AuthoredScale
    ForceAuthoredScale(rexPath)

    # todo: detect if sceneLocation is 0,0,0 and if so move it to   -38.34500,8.03500,1.87500

    # Initialize Exporter - janky imports since it's americas only
    from PluginLoader import PluginManager
    from PluginCommon import CinematicExporter
    plugins = PluginManager.Instance
    exporter = CinematicExporter.ICinematicExporter(plugins.GetPlugin[CinematicExporter.ICinematicExporter]())
    exporter.LoadMarkup(rexPath)
    markup = CinematicExporter.ICinematicMarkup(exporter.Markup)

    # todo: detect missing cutfiles - and use autopopulate instead?

    # Check Existing Models
    # 0 character, 1 face, 2 camera, 3 prop, 4 weapon, 5 vehicle, 6 audio
    handleModelTypes = [0, 3, 4, 5]
    oldCameraModels = []
    missingHandleModels = []
    for model in markup.Models:
        model = CinematicExporter.ICinematicModel(model)
        if model.ModelType == 2:
            oldCameraModels.append(model)
        if model.ModelType in handleModelTypes and model.Handle == "":
            missingHandleModels.append(model)

    # Remove Old Cameras
    for oldCameraModel in oldCameraModels:
        exporter.RemoveModel(oldCameraModel)

    # Add Export Camera to Markup
    exporter.AddModel("Camera", exportCamera.LongName)

    # Fix Missing Handles
    # todo: wrote this handle code, but was waiting for a test case to come up before submitting
    # if missingHandleModels:
    #     pass

    # Set Exporter Ranges
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode60Frames)
    markup.StartFrame = 0
    markup.Endframe = endFrame  # todo: need to double until our ranges are 60fps compatible

    # Delete Preview Files
    FileTools.DeletePath(capPaths.previewPath)

    # Manual Export - exit early before export commands
    if capObj.exportMode == "manual":
        return "MANUAL EXPORT"

    # Export Anims
    capObj.UpdateTag("mobuStatus", "EXPORT ANIMS")
    try:
        animsBuilt = exporter.Export(False, True)  # enablePerforce, showToolkit
    except:
        animsBuilt = False
    if not animsBuilt:
        return "EXPORT ERROR: Issue during Export Anims"
    # todo: errors seem to get missed here - returns true even when errors encountered

    # Reconcile Live File
    capObj.UpdateTag("mobuStatus", "RECONCILE LIVE FILE")
    reconcileStatus = ReconcileLiveFile(fbxPath=capObj.fbxPath)
    if reconcileStatus is not True:
        return reconcileStatus

    # Build Preview
    capObj.UpdateTag("mobuStatus", "BUILD PREVIEW")
    convertPath = "X:\\americas\\tools\\release\\ironlib\\lib\\RSG.Pipeline.Convert.exe"
    argsTxt = "--no-default-resolver --sync-dependencies --preview --no-content-cache " \
              "--branch dev --content-tree-grouptag cinematic_animation"
    cmdTxt = "{0} {1} {2}\\".format(convertPath, argsTxt, capPaths.cutsPath)
    cmdOut, cmdErr = RunTerminalCommand(cmdTxt)  # cmdOut is str (log lines), cmdErr is str (empty if no error)
    # TODO: test that this error check actually works
    if cmdErr:
        return "EXPORT ERROR: Issue during Build Preview"

    # Scene Built Successfully
    return "EXPORT SUCCESS"


def RunCinematicFast(capObj):
    """ Exports camera animation only in a blank FBX, then combines the exported camera anims
        with previous export data, then builds.  This is much faster and less prone to errors. """
    # Make Cuts/Ias Writeable
    capPaths = CapturePaths.FastPath(capObj.fbxPath)
    FileTools.MakePathWriteable(capPaths.cutsPath)
    FileTools.MakePathWriteable(capPaths.iasPath)

    # Get Export Cam and Endframe
    exportCamera = mobu.FBFindModelByLabelName("ExportCamera")
    if not exportCamera:
        return "EXPORT ERROR: Export camera could not be found."
    endFrame = int(capObj.endFrame) * 2

    # Initialize Exporter - janky imports since it's americas only
    from PluginLoader import PluginManager
    from PluginCommon import CinematicExporter
    plugins = PluginManager.Instance
    exporter = CinematicExporter.ICinematicExporter(plugins.GetPlugin[CinematicExporter.ICinematicExporter]())
    markup = CinematicExporter.ICinematicMarkup(exporter.Markup)

    # Add Export Camera to Markup
    exporter.AddModel("Camera", exportCamera.LongName)

    # Set Exporter Ranges
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode60Frames)
    markup.StartFrame = 0
    markup.Endframe = endFrame

    # Delete Preview Files
    FileTools.DeletePath(capPaths.previewPath)

    # Export Anims
    capObj.UpdateTag("mobuStatus", "EXPORT ANIMS")
    try:
        animsBuilt = exporter.Export(False, True)  # enablePerforce, showToolkit
    except:
        animsBuilt = False
    if not animsBuilt:
        return "EXPORT ERROR: Issue during Export Anims"

    # Update Status and Paths
    capObj.UpdateTag("mobuStatus", "REBUILD ZIP")
    capDataRoot = capPaths.capDataRoot

    # Set Temp Cuts Paths
    tempCutsFolder = os.path.join(capPaths.cutsRoot, "tempCuts")
    expCamAnimPath = os.path.join(capPaths.cutsPath, "ExportCamera.anim")
    expCamClipPath = os.path.join(capPaths.cutsPath, "ExportCamera.clip")

    # Copy ExportCamera to Temp Cuts
    FileTools.DeletePath(tempCutsFolder)
    FileTools.CopyPath(expCamAnimPath, tempCutsFolder)
    FileTools.CopyPath(expCamClipPath, tempCutsFolder)

    # Copy Network Cuts to Local Cuts
    FileTools.DeletePath(capPaths.cutsPath)
    dataCutsFolder = os.path.join(capDataRoot, "CutFiles")
    FileTools.CopyFolderContents(dataCutsFolder, capPaths.cutsPath)

    # Copy Network Ias to Local Ias
    iasPath = capPaths.iasPath
    dataIasFolder = os.path.join(capPaths.capDataRoot, "IasFiles")
    dataIasPath = os.path.join(dataIasFolder, os.path.split(iasPath)[1])
    if os.path.exists(dataIasPath) is False:
        return "EXPORT ERROR: Ias file not found on network during fast cap."
    FileTools.CopyPath(dataIasPath, iasPath)

    # Copy ExportCamera back in from tempCuts
    FileTools.CopyFolderContents(tempCutsFolder, capPaths.cutsPath)

    # Build Preview
    capObj.UpdateTag("mobuStatus", "BUILD PREVIEW")
    convertPath = "X:\\americas\\tools\\release\\ironlib\\lib\\RSG.Pipeline.Convert.exe"
    argsTxt = "--no-default-resolver --sync-dependencies --preview --no-content-cache " \
              "--branch dev --content-tree-grouptag cinematic_animation"
    cmdTxt = "{0} {1} {2}\\".format(convertPath, argsTxt, capPaths.cutsPath)
    cmdOut, cmdErr = RunTerminalCommand(cmdTxt)  # cmdOut is str (log lines), cmdErr is str (empty if no error)
    # TODO: test that this error check actually works
    if cmdErr:
        return "EXPORT ERROR: Issue during Build Preview"

    # Scene Built Successfully
    return "EXPORT SUCCESS"


def SendLocalExportData():
    fbxPath = mobu.FBApplication().FBXFileName
    if fbxPath:
        fbxName = os.path.splitext(os.path.basename(fbxPath))[0]
        fbxEndFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
        exportEndFrame = RexExport.GetCutsceneShotEndRange(0)
        if fbxEndFrame != exportEndFrame:
            msgTxt = "Error: The FBX endframe {0} does not match the exported endframe {1}".format(fbxEndFrame, exportEndFrame)
            mobu.FBMessageBox("Export Transfer Tool", msgTxt, "Damn")
        else:
            msgTxt = "Send local export of {} to capture machines?".format(fbxName)
            userChoice = mobu.FBMessageBox("Export Transfer Tool", msgTxt, "Send Data", "Cancel")
            if userChoice == 1:
                msgTxt = CutTools.SendExportToNetwork(fbxPath)
                mobu.FBMessageBox("Export Transfer Tool", msgTxt, "Okay")
