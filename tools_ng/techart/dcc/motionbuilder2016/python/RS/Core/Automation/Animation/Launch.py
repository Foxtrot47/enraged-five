"""
Launcher for automated rendering of Anim files in Motion Builder

"X:\rdr3\tools\bin\python\Python27\x64\bin\python.exe" X:\wildwest\dcc\motionbuilder2014\python\RS\Core\Automation\Animation\Launch.py "execute"

import subprocess
subprocess.check_output(r'"X:\rdr3\tools\bin\setx.exe"  MOTIONBUILDER_SCRIPT_FILE ""')
"X:\rdr3\tools\bin\setx.exe" MOTIONBUILDER_SCRIPT_FILE "X:\wildwest\dcc\motionbuilder2014\python\RS\Core\Automation\Animation\Launch.py"

"""
import os
import re
import sys
import time
import subprocess


def GetEnvironmentVariable(environment_var):
    """
    Gets the value associated with the environment variable passed

    Arguments:
         environment_var (string): the environment variable that you want to query

    Return:
        string
    """

    try:
        osEnvironments = subprocess.check_output('setenv', shell=True)

    except WindowsError:
        results = os.popen('setenv')
        osEnvironments = results.read()
        results.close()

    # Get the python tools executable
    regularExpression = r"(?<={environment_var}:)[a-z0-9._:\t\b\\ ]+".format(environment_var=environment_var)
    searchResults = re.search(regularExpression, osEnvironments, re.M | re.I)

    if searchResults:
        tools_executable = searchResults.group().strip()
    else:
        raise Exception, "No system environment matching {environment_var} is setup".format(environment_var=environment_var)

    return tools_executable


if "execute" in sys.argv:

    print "*" * 79
    print "Starting Animation Capture Process"
    print "*" * 79

    print "Opening Motion Builder"

    # Starting Motion Builder Launcher

    toolsPath = GetEnvironmentVariable("RS_TOOLSROOT")
    mobuLauncherPath = os.path.join(toolsPath, "techart", "bin", "MotionbuilderLauncher", "MotionbuilderLauncher.exe")

    print "Setting MOTIONBUILDER_SCRIPT_FILE Environment Variable"

    print subprocess.check_output(r'"{}" MOTIONBUILDER_SCRIPT_FILE "{}"'.format(
        os.path.join(toolsPath, "bin", "setx.exe"), __file__))

    launchCommand = '{0} -major_build "2016" -minor_build "CUT2" -profile "Dev - Automation" -script_file {1}'.format(
        mobuLauncherPath, __file__)
    print launchCommand
    subprocess.Popen(launchCommand)

    # Wait for Motion Builder to Load

    mobuLoadTime = time.time()
    counter = 0
    while not os.path.exists("x:\\ping.py"):
        print "Checking for ping", time.time() - mobuLoadTime
        time.sleep(5)
        counter += 1
        if counter > 30:
            print "Motion Builder took too long to open, killing process"
            sys.exit(False)

    # We are sleeping for an extra 5 seconds to ensure mobu is really done opening
    time.sleep(5)
    print "Motion Builder is Open"

    # Send command to Mobu to execute
    print "Sending Command to start the capture process in Motion Builder"

    with open(r"x:\mobuRunRemoteScript.txt", 'w') as mobuscript:
        mobuscript.write("-script -ignoreWing -silent;X:\\ping.py")

    # We sleep for an extra 10 seconds to give motion builder time to detect that a command has been sent to it

    time.sleep(10)

    # check that we are done processing anims

    finished = False
    counter = 0
    while not finished:
        time.sleep(30)
        print "Checking if process has finished"
        try:
            with open("x:\\ping.py", "r") as _file:
                finished = "DONE = True" in _file.read()
        except Exception, error:
            print error
            counter += 1

        if counter > 5:
            print "Ending Process Early"
            sys.exit(True)

    time.sleep(5)
    print "Motion Builder Closed"
    # sleep and wait for mobu to process the command and close
    time.sleep(5)
    os.remove("x:\\ping.py")
    print "Done"
    sys.exit(True)

else:

    with open("x:\\ping.py", "w") as _file:
        _file.write("DONE = False\n"
                    "from RS.Core.Automation.Animation import FBX\n"
                    "FBX.StoreRecentAnimData(close=True)\n")
