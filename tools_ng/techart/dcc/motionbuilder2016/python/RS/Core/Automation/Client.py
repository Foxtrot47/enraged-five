'''
Description:
    Client module that looks at a database to determine if a job can be claimed.
    
Author:
    Jason Hayes <jason.hayes@rockstarsandiego.com>

'''

import os
import sys
import glob
import shutil
import socket
import time
import datetime
import subprocess
import win32gui
import win32api
import win32process
import win32con
import threading
import ctypes

from optparse import OptionParser

import RS.Core.Automation
import RS.Core.Automation.Server
import RS.Utils.Email
import RS.Perforce
import RS.Config
import RS.Utils.Logging.Universal
import RS.Utils.Process


## Constants ##

# If False, then the files will be shelved.
SUBMIT_FILE = True

# Poll for a job every n seconds
POLL_INCREMENT      = 1

# Watch status
STATUS_IDLE         = 1
STATUS_PROCESSING   = 2
STATUS_STOP         = 3

# Status strings
STATUS = { STATUS_IDLE          : 'Idle',
           STATUS_PROCESSING    : 'Processing' }

CURRENT_STATUS      = STATUS_IDLE
CURRENT_JOB         = None

MOBU_CRASH_WINDOW_TITLES = ( 'MotionBuilder 2012 SP2 Error Report',
                             'Rockstar - Tools Error',
                             'Microsoft Visual C++ Runtime Library' )
MOBU_CRASHED = False

EMAIL_TO = [ 'jason.hayes@rockstarsandiego.com',
             'mark.harrison-ball@rockstarlondon.com' ]

# Once MotionBuilder has been set to close down, this is the time that can pass before we
# forcibly kill the process.  Hitting lots of cases where Mobu doesn't crash, but the exe
# hangs around preventing the job from completing.
#
# In minutes.  Time delta object uses result in seconds (at our resolution), so multiply to
# get total number of seconds to check against.
TIME_TO_KILL_MOBU_EXE = 5 * 60.0
MOBU_IS_CLOSING = False
MOBU_IS_CLOSING_START = None
   
   
## Functions ##

def enumWindowHandler( hwnd, ctx ):
    global MOBU_CRASHED
    
    if win32gui.IsWindowVisible( hwnd ):
        windowTitle = win32gui.GetWindowText( hwnd )
        
        for title in MOBU_CRASH_WINDOW_TITLES:
            if title in windowTitle:
                MOBU_CRASHED = True
                
        if MOBU_CRASHED:
            
            # Kill MotionBuilder, and feel good about it.
            os.system( 'taskkill /f /im motionbuilder.exe' )                        
            
            # Kill any error report windows that pop up.
            t, pid = win32process.GetWindowThreadProcessId( hwnd )
            handle = ctypes.windll.kernel32.OpenProcess( 1, False, pid )
            
            if handle:
                ctypes.windll.kernel32.TerminateProcess( handle, -1 )
                ctypes.windll.kernel32.CloseHandle( handle )
                
            RS.Core.Automation.Server.updateJobStatus( CURRENT_JOB.id, 'MotionBuilder Crashed', level = RS.Core.Automation.STATUS_LEVEL_FAILED )
        
def threadWatchForMotionBuilderCrash():
    global MOBU_CRASHED
    global CURRENT_JOB
    global MOBU_IS_CLOSING
    global MOBU_IS_CLOSING_START
    global TIME_TO_KILL_MOBU_EXE
    
    while True:
        win32gui.EnumWindows( enumWindowHandler, None )
        
        # Check to see if MotionBuilder is closing.
        '''
        if CURRENT_JOB:
            job = rs_Server.getJobById( CURRENT_JOB.id )
            
            # Start tracking how long Mobu has been closing.
            if job.status == 'Shutting down MotionBuilder.' and not MOBU_IS_CLOSING and process.isRunning( 'motionbuilder.exe' ):
                print 'Started tracking how long MotionBuilder is trying to close...'
                
                MOBU_IS_CLOSING = True
                MOBU_IS_CLOSING_START = datetime.datetime.now()
            
        # Test to see if MotionBuilder is taking too long to close.
        if MOBU_IS_CLOSING:
            elapsed = datetime.datetime.now() - MOBU_IS_CLOSING_START
            
            if elapsed.seconds >= TIME_TO_KILL_MOBU_EXE:
                if process.kill( 'motionbuilder.exe' ):
                    print 'Forcibly killed MotionBuilder.  Fuck that felt good.'
                    
                    MOBU_IS_CLOSING = False
                    
        # Catch all.
        if not process.isRunning( 'motionbuilder.exe' ):
            MOBU_IS_CLOSING = False
        '''
                
        time.sleep( 5 )

def copyJobLogToNetwork( job, ulogFile ):
    if not os.path.isdir( RS.Core.Automation.LOGS_DIR ):
        os.makedirs( RS.Core.Automation.LOGS_DIR )
        
    if os.path.isfile( ulogFile ):
        shutil.copyfile( ulogFile, '{0}CutsceneUpdate_Job{1}.ulog'.format( RS.Core.Automation.LOGS_DIR, job.id ) )
        
def completeJob( job, fileInfo, changelist ):
    global EMAIL_TO
    global SUBMIT_FILE
    
    ulogFile = '{0}\\logs\\MotionBuilder.ulog'.format( RS.Config.Tool.Path.Root )
    
    body = 'Job ID: {0}\n'.format( job.id )
    body += 'Submitted By: {0}\n'.format( job.submittedBy )
    body += 'Filename: {0}\n'.format( job.filename )
    body += 'Modes: {0}\n'.format( job.modes )
    body += 'Machine: {0}\n'.format( job.machine )
    body += 'Perforce Revision: ({0}) of ({1})\n'.format( fileInfo.HaveRevision, fileInfo.HeadRevision )
    
    if not SUBMIT_FILE:
        body += 'Shelved Changelist: {0}\n'.format( changelist )
    
    to = [ emailAddress for emailAddress in EMAIL_TO ]
    
    if job.submittedBy not in to:
        to.append( job.submittedBy )

    submitted = False
    
    if SUBMIT_FILE:
        RS.Core.Automation.Server.updateJobStatus( job.id, 'Submitting file.', level = RS.Core.Automation.STATUS_LEVEL_COMPLETE )
        
        submitted = RS.Perforce.Submit( changelist )        
        
    else:
        shelvedFile = False
        RS.Core.Automation.Server.updateJobStatus( job.id, 'Shelving file.', level = RS.Core.Automation.STATUS_LEVEL_COMPLETE )
        
        if RS.Perforce.Shelve( job.filename, changelist ) != False:
            shelvedFile = True
            
            RS.Core.Automation.Server.updateJobStatus( job.id, 'Reverting file.', level = RS.Core.Automation.STATUS_LEVEL_COMPLETE )
            
            RS.Perforce.Revert( job.filename )
            
        else:
            RS.Core.Automation.Server.updateJobStatus( job.id, 'Unable to shelve the file!', level = RS.Core.Automation.STATUS_LEVEL_COMPLETE )

    if RS.Core.Automation.Server.markJobAsComplete( job.id ):
        
        if SUBMIT_FILE:
            fileInfo = RS.Perforce.GetFileState( job.filename )
            
            if submitted and fileInfo:
                subject = 'Cutscene Update - Job Finished: {0}'.format( job.name )
                RS.Core.Automation.Server.updateJobStatus( job.id, 'Finished.  Submitted as revision #{0}.'.format( fileInfo.HeadRevision ), level = RS.Core.Automation.STATUS_LEVEL_COMPLETE ) 
                
            else:
                subject = 'Cutscene Update - Job Finished with Errors: {0}'.format( job.name )
                RS.Core.Automation.Server.updateJobStatus( job.id, 'Finished, but could not submit the file!  Please contact Tech Art.', level = RS.Core.Automation.STATUS_LEVEL_FAILED )                 
                
        else:
            if shelvedFile:
                subject = 'Cutscene Update - Job Finished: {0}  Shelved Changelist: {1}'.format( job.name, changelist )
                RS.Core.Automation.Server.updateJobStatus( job.id, 'Finished.  Unshelve changelist #{0}'.format( changelist ), level = RS.Core.Automation.STATUS_LEVEL_COMPLETE )
                
            else:
                subject = 'Cutscene Update - Job Finished with Errors: {0}'.format( job.name )
                RS.Core.Automation.Server.updateJobStatus( job.id, 'Finished.  But the shelve command failed!  Please contact Tech Art.', level = RS.Core.Automation.STATUS_LEVEL_COMPLETE )
            
        RS.Utils.Email.Send( 'svcrsgsanvmbuilder@rockstarsandiego.com', to, subject, body, attachments = [ ulogFile ] )
        
    copyJobLogToNetwork( job, ulogFile )

def failJob( job, errorMsg, changelist ):
    global EMAIL_TO
    
    ulogFile = '{0}\\logs\\MotionBuilder.ulog'.format( RS.Config.Tool.Path.Root )
    
    body = 'Job ID: {0}\n'.format( job.id )
    body += 'Submitted By: {0}\n'.format( job.submittedBy )
    body += 'Filename: {0}\n'.format( job.filename )
    body += 'Modes: {0}\n'.format( job.modes )
    body += 'Machine: {0}\n'.format( job.machine )
    body += 'Error Message: {0}\n'.format( errorMsg )
    
    ulogObj = RS.Utils.Logging.Universal.UniversalLog( 'MotionBuilder', appendToFile = True )
    
    if ulogObj.HasErrors:
        errors = ulogObj.GetErrors()
        
        body += 'Universal Log Errors:\n\n'
        
        for error in errors:
            body += '{0}\n\n'.format( error )
    
    to = [ emailAddress for emailAddress in EMAIL_TO ]
    
    if job.submittedBy not in to:
        to.append( job.submittedBy )            

    subject = 'Cutscene Update - Job Failed: {0}'.format( job.name )
    RS.Core.Automation.Server.updateJobStatus( job.id, '{0}'.format( errorMsg ), level = RS.Core.Automation.STATUS_LEVEL_FAILED )
    RS.Utils.Email.Send( 'svcrsgsanvmbuilder@rockstarsandiego.com', to, subject, body, attachments = [ ulogFile ] )
    
    copyJobLogToNetwork( job, ulogFile )
    
    RS.Perforce.Revert( job.filename )
    RS.Perforce.DeleteChangelist( changelist )
    
def startJob( job, changelist, fileInfo ):
    
    # Send email that the job has started.
    subject = 'Cutscene Update - Job ({0}) has started.'.format( job.name )
    
    body = 'Job ID: {0}\n'.format( job.id )
    body += 'Filename: {0}\n'.format( job.filename )
    body += 'Perforce Revision: ({0}) of ({1})\n'.format( fileInfo.HaveRevision, fileInfo.HeadRevision )
    body += 'View the web monitor: {0}\n'.format( RS.Core.Automation.WEB_MONITOR_URL )
    
    to = [ emailAddress for emailAddress in EMAIL_TO ]
                
    if job.submittedBy not in to:
        to.append( job.submittedBy )            

    RS.Utils.Email.Send( 'svcrsgsanvmbuilder@rockstarsandiego.com', to, subject, body )
    
    # Sync files.
    RS.Perforce.Sync( '{0}/art/animation/resources/characters/Expressions/Binary/...'.format( RS.Config.Project.Path.Root ) )
    RS.Perforce.Sync( '{0}/art/animation/resources/PreRotation/...'.format( RS.Config.Project.Path.Root ) )
    RS.Perforce.Sync( '{0}/art/VFX/rmptfx/...'.format( RS.Config.Project.Path.Root ) )
    RS.Perforce.Sync( '{0}/assets/anim/expressions/...'.format( RS.Config.Project.Path.Root ) )
    RS.Perforce.Sync( '{0}/...'.format( RS.Config.Script.Path.Root ) )
    RS.Perforce.Sync( '{0}/etc/config/cutscene/Cutscene_Model_Handles.csv'.format( RS.Config.Tool.Path.Root ) )        
    RS.Perforce.Sync( '{0}/dcc/current/motionbuilder2014/plugins/x64/...'.format( RS.Config.Tool.Path.Root ) )

    # TODO: NEED TO FIX THIS!!!! 
    toolsIni = r'{0}\wildwest\script\MotionBuilderPython\ini.py'.format( RS.Config.Tool.Path.Root )

    # Setup job to be processed, passed by temp environment variables to MotionBuilder.
    os.environ[ 'RS_AUTOMATION' ] = 'True'
    os.environ[ 'RS_JOB_ID' ] = str( job.id )
    
    RS.Core.Automation.Server.updateJobStatus( job.id, 'Opening MotionBuilder' )  
    
    # Launch MotionBuilder.
    proc = subprocess.Popen( '{0} -suspendMessages {1}'.format( MOBU_EXE, toolsIni ) )
    proc.wait()
    
    print 'Processed Job:', job.name  
    
    # Get the job object again, because it's status has likely been updated from MotionBuilder.
    job = RS.Core.Automation.Server.getJobById( job.id )
    
    if job:
        
        # Job Completed
        if job.isComplete():
            completeJob( job, fileInfo, changelist )
                          
        # MotionBuilder Crashed 
        elif MOBU_CRASHED:
            
            # Don't fail the job if MotionBuilder crashed while shutting down.
            if 'Shutting down MotionBuilder' not in job.status:
                failJob( job, 'MotionBuilder Crashed', changelist )
        
        # Unknown Error
        else:
            failJob( job, 'Error occurred, please check the log.', changelist )
            
    # Job ID no longer exists in the database.
    else:
        failJob( job, 'Job using ID {0} no longer exists!'.format( job.id ), changelist )    
   
def processJob():
    global CURRENT_JOB
    global MOBU_CRASHED
    global EMAIL_TO
    
    if CURRENT_JOB:
        MOBU_CRASHED = False        
        
        # Record timestamp of when we started the job.
        RS.Core.Automation.Server.timestampStartJob( CURRENT_JOB.id )
        
        # Sync files first.
        RS.Core.Automation.Server.updateJobStatus( CURRENT_JOB.id, 'Syncing files' )
        
        # Sync/edit the job file.
        RS.Perforce.Sync( CURRENT_JOB.filename, force = True )
        
        description = '[Automated] Cutscene Update - Job ID: {0}\n\n'.format( CURRENT_JOB.id )
        
        for mode in CURRENT_JOB.modes:
            description += '- {0}\n'.format( mode )
        
        changelist = RS.Perforce.CreateChangelist( description ).Number
        fileInfo = RS.Perforce.Edit( CURRENT_JOB.filename, changelist )
        
        if fileInfo:
            
            # File isn't the latest.
            if not RS.Perforce.IsLatest( fileInfo ):
                failJob( CURRENT_JOB, 'File is not the latest version!  The sync likely failed.', changelist )
            
            # File checked out, so start it.
            elif RS.Perforce.IsOpenForEdit( fileInfo ):
                startJob( CURRENT_JOB, changelist, fileInfo )
                
            # File has been deleted from Perforce.
            elif fileInfo.isDeleted():
                failJob( CURRENT_JOB, 'File has been deleted from Perforce!', changelist )
                
            # Could not check out the file.  
            else:
                failJob( CURRENT_JOB, 'Could not check out the file! Checked out by ({0}).'.format( fileInfo.otherOpen ), changelist )
                
            # Remove the file locally from the client.
            if CURRENT_JOB:
                RS.Perforce.Sync( '{0}#0'.format( CURRENT_JOB.filename ), force = True )
            
        # File doesn't exist in Perforce
        else:
            failJob( CURRENT_JOB, 'File does not exist in Perforce!', changelist )
        
        # Record total job time.
        RS.Core.Automation.Server.timestampEndJob( CURRENT_JOB.id )        
        
        CURRENT_JOB = None
        MOBU_CRASHED = False       
        
def watch():
    global CURRENT_STATUS
    global CURRENT_JOB
    
    clientVersion = 0    
    
    fileInfo = RS.Perforce.GetFileState( __file__ )
        
    if fileInfo:
        clientVersion = fileInfo.HaveRevision
    
    # Launch our watcher thread to see if MotionBuilder crashed.
    watcher = threading.Thread( target = threadWatchForMotionBuilderCrash )
    watcher.start()
    
    # Start watching for a job.
    while CURRENT_STATUS != STATUS_STOP:
        
        # Idling, so look for a job to process.
        if CURRENT_STATUS == STATUS_IDLE:
            job = RS.Core.Automation.Server.claimJob()
            
            if job:
                print 'Version ({2}): Processing Job - ID:{0} Name:{1}'.format( job.id, job.name, clientVersion )
                
                CURRENT_JOB = job
            
                # Update our status.
                CURRENT_STATUS = STATUS_PROCESSING
                
                # Process job.
                processJob()
                
                # Set us back to idle to look for another job.
                CURRENT_STATUS = STATUS_IDLE
        
        sys.stdout.write( 'Version ({0}): Looking for a job...\ \r'.format( clientVersion ) )
        time.sleep( POLL_INCREMENT )
        sys.stdout.write( 'Version ({0}): Looking for a job...| \r'.format( clientVersion ) )
        time.sleep( POLL_INCREMENT )
        sys.stdout.write( 'Version ({0}): Looking for a job.../ \r'.format( clientVersion ) )
        time.sleep( POLL_INCREMENT )
        sys.stdout.write( 'Version ({0}): Looking for a job...- \r'.format( clientVersion ) )
        time.sleep( POLL_INCREMENT )        
        
def startClient():
    timeToStart = 5
    
    while timeToStart != 0:
        sys.stdout.write( 'Starting client in {0} seconds...\r'.format( timeToStart ) )
        
        time.sleep( 1 )
        timeToStart -= 1

    sys.stdout.write( '\n' )
    watch()
        
if __name__ == '__main__':
    global MOBU_EXE
    MOBU_EXE = r'C:\Program Files\Autodesk\MotionBuilder 2014\bin\x64\motionbuilder.exe'
    
    parser = OptionParser()
    parser.add_option( '-m', '--mobu', dest = 'mobu', help = 'The install path for MotionBuilder.' )
        
    ( options, args ) = parser.parse_args()    

    if options.mobu:
        MOBU_EXE = options.mobu

    startClient()