import socket

WEB_MONITOR_URL = 'http://sanb-vm1/reports/CutsceneUpdate/CutsceneUpdateMonitor.html'

FAIL_JOB_AFTER_NUM_ATTEMPTS = 1

# Update the jobs report every n seconds.
JOBS_REPORT_REFRESH = 5

# Directories
JOBS_DIR            = '\\\\sanb-vm1\\reports\\CutsceneUpdate\\'
LOGS_DIR            = '{0}logs\\'.format( JOBS_DIR )

# Job Modes
JOB_MODE_UPDATE_REFERENCES      = 'Update References'
JOB_MODE_UPDATE_CAMERAS         = 'Update Cameras'
JOB_MODE_UPDATE_EST_TRACKS      = 'Update EST Tracks'
JOB_MODE_FIX_AMBIENT_FACE_RIGS  = 'Fix Ambient Face Rigs'

JOB_MODES = ( JOB_MODE_UPDATE_REFERENCES,
              JOB_MODE_UPDATE_CAMERAS,
              JOB_MODE_UPDATE_EST_TRACKS,
              JOB_MODE_FIX_AMBIENT_FACE_RIGS )

# Status level
STATUS_LEVEL_MESSAGE    = 1
STATUS_LEVEL_WARNING    = 2
STATUS_LEVEL_ERROR      = 3
STATUS_LEVEL_COMPLETE   = 4
STATUS_LEVEL_FAILED     = 5

STATUS_LEVEL = { STATUS_LEVEL_MESSAGE   : '[Message]',
                 STATUS_LEVEL_WARNING   : '[Warning]',
                 STATUS_LEVEL_ERROR     : '[Error]',
                 STATUS_LEVEL_COMPLETE  : '[Complete]',
                 STATUS_LEVEL_FAILED    : '[Failed]' }