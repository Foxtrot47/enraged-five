import os
import pyodbc
import datetime
import socket
import types

import RS.Config
import RS.Perforce
import RS.Utils.Email
import RS.Core.Automation as constants


# Database connection objects
DB_CONN = None
DB_CURSOR = None

# Database table
DB_TABLE_NAME = 'CutsceneProcessing'

# Database column names
DB_COLUMN_ID            = 'ID'
DB_COLUMN_JOB_NAME      = 'JobName'
DB_COLUMN_FILENAME      = 'FileName'
DB_COLUMN_MODE          = 'Mode'
DB_COLUMN_STATUS        = 'Status'
DB_COLUMN_START_TIME    = 'StartTime'
DB_COLUMN_RESULT        = 'Result'
DB_COLUMN_MACHINE_NAME  = 'MachineName'

DB_COLUMN_TIME          = 'Extra0'
DB_COLUMN_TOTAL_TIME    = 'Extra1'
DB_COLUMN_EXTRA_2       = 'Extra2'
DB_COLUMN_EXTRA_3       = 'Extra3'
DB_COLUMN_EXTRA_4       = 'Extra4'
DB_COLUMN_EXTRA_5       = 'Extra5'
DB_COLUMN_EXTRA_6       = 'Extra6'
DB_COLUMN_EXTRA_7       = 'Extra7'
DB_COLUMN_EXTRA_8       = 'Extra8'
DB_COLUMN_EXTRA_9       = 'Extra9'


class RsJob( object ):
    '''
    Description:
        Wrapper object for a job from the database.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''
    
    def __init__( self ):
        self.id = None
        self.name = None
        self.submittedBy = None
        self.filename = None
        self.modes = []
        self.status = None
        self.startTime = None  # This one isn't used now.  Use .time as the start time.
        self.time = None
        self.totalTime = None
        self.result = None
        self.machine = None
        self.numErrors = 0
        
    def isFailed( self ):
        return constants.STATUS_LEVEL[ constants.STATUS_LEVEL_FAILED ] in self.status
    
    def isComplete( self ):
        return constants.STATUS_LEVEL[ constants.STATUS_LEVEL_COMPLETE ] in self.status or self.result == True


def connectToDatabase():
    global DB_CONN
    global DB_CURSOR
    
    DB_CONN = pyodbc.connect( 'Driver={SQL Server};Server=RSGSANSQL1\PRODUCTION;Database=RockstarTools;Initial Catalog=RockstarTools;Integrated Security=True' )
    
    if DB_CONN:
        DB_CURSOR = DB_CONN.cursor()
    
        return True
    
    return False

def getAllJobs():
    global DB_CONN
    global DB_CURSOR
    
    jobs = []
    
    if not DB_CONN:
        connectToDatabase()
    
    if DB_CONN:
        statement = 'SELECT * FROM {0}'.format( DB_TABLE_NAME )
        
        rows = DB_CURSOR.execute( statement ).fetchall()
        
        for row in rows:
            jobId, jobNameData, jobFilename, jobModes, jobStatus, jobStart, jobResult, jobMachine, extra0, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, extra9 = row
            
            if jobResult == 0:
                jobResult = False
                
            else:
                jobResult = True
            
            metadata = str( jobNameData ).split( ',' )
            
            if ',' in jobModes:
                jobModes = str( jobModes ).split( ',' )
                
            else:
                jobModes = [ jobModes ]
            
            job = RsJob()
            job.id = jobId
            job.name = metadata[ 0 ]
            job.submittedBy = metadata[ 1 ]
            job.filename = jobFilename
            job.modes = jobModes
            job.status = jobStatus
            job.result = jobResult
            job.machine = jobMachine
            job.startTime = jobStart
            job.totalTime = extra1
            
            if extra0 != None:
                try:
                    job.time = datetime.datetime.strptime( extra0, '%Y-%m-%d %H:%M:%S.%f' )
                    
                except:
                    job.time = datetime.datetime.strptime( extra0, '%Y-%m-%d %H:%M:%S' )
            
            # Legacy jobs don't have this metadata.
            if len( metadata ) > 2:
                job.numErrors = int( metadata[ 2 ] )
            
            jobs.append( job )
            
    return jobs

def timestampStartJob( jobId ):
    job = getJobById( jobId )
    
    if job:
        statement = "UPDATE {0} SET {1} = '{2}' WHERE ID = {3};".format( DB_TABLE_NAME, DB_COLUMN_TIME, str( datetime.datetime.now() ), job.id )
        DB_CURSOR.execute( statement )
        DB_CONN.commit()
            
def timestampGetJobElapsedTime( jobId ):
    job = getJobById( jobId )
    
    if job:
        if job.time:
            elapsedTime = str( datetime.datetime.now() - job.time )
            
            if '.' in elapsedTime:
                return elapsedTime.split( '.' )[ 0 ]
            
            else:
                return elapsedTime
    
def timestampEndJob( jobId ):
    job = getJobById( jobId )
    
    if job:
        elapsedTime = timestampGetJobElapsedTime( jobId )
        
        statement = "UPDATE {0} SET {1} = '{2}' WHERE ID = {3};".format( DB_TABLE_NAME, DB_COLUMN_TOTAL_TIME, elapsedTime, job.id )
        DB_CURSOR.execute( statement )
        DB_CONN.commit()        

def getJobsInQueue():
    jobs = getAllJobs()
    
    return [ job for job in jobs if job.status == 'Waiting' and job.result == False ]

def getJobsInProgress():
    jobs = getAllJobs()
    
    return [ job for job in jobs if job.status != 'Waiting' and job.result == False and not job.isFailed() ]

def getCompletedJobs():
    jobs = getAllJobs()
        
    return [ job for job in jobs if job.result == True ]

def getFailedJobs():
    jobs = getAllJobs()
    
    return [ job for job in jobs if job.isFailed() ]
    
def removeJob( jobId ):
    global DB_CONN
    
    if not DB_CONN:
        connectToDatabase()
        
    if DB_CONN:
        statement = 'DELETE FROM {0} WHERE {1} = {2}'.format( DB_TABLE_NAME, DB_COLUMN_ID, jobId )
        
        DB_CURSOR.execute( statement )
        DB_CONN.commit()
        
def resubmitJob( jobId, newId = False ):
    global DB_CONN
            
    if not DB_CONN:
        connectToDatabase()
        
    if DB_CONN:
        jobs = getAllJobs()      
        
        for job in jobs:
            if job.id == jobId:
                
                # Revert file.
                RS.Perforce.revert( job.filename )
                
                if job.isFailed():
                    newId = True

                # Creates a new job id, essentially putting it at the bottom of the queue.
                if newId:
                    removeJob( job.id )
                    addJob( job.filename, job.modes, submittedBy = job.submittedBy )                
                    
                # Puts it back into the queue using the same ID, possibly being picked up by a different machine.
                else:
                    statement = "UPDATE {0} SET {1} = '{2}' WHERE ID = {3};".format( DB_TABLE_NAME, DB_COLUMN_STATUS, 'Waiting', job.id )
                    DB_CURSOR.execute( statement )
                    DB_CONN.commit()  
                    
                    statement = "UPDATE {0} SET {1} = '{2}' WHERE ID = {3};".format( DB_TABLE_NAME, DB_COLUMN_MACHINE_NAME, 'Unassigned', job.id )
                    DB_CURSOR.execute( statement )
                    DB_CONN.commit()
                
def clearAllJobs():
    global DB_CONN
        
    if not DB_CONN:
        connectToDatabase()
        
    if DB_CONN:
        jobs = getAllJobs()
        
        for job in jobs:
            removeJob( job.id )
   
def getJobById( jobId ):
    global DB_CONN
        
    if not DB_CONN:
        connectToDatabase()
    
    if DB_CONN:
        jobs = getAllJobs()
        
        for job in jobs:
            if job.id == jobId:
                return job
            
    return None

def updateJobStatus( jobId, status, level = constants.STATUS_LEVEL_MESSAGE ):
    global DB_CONN
            
    if not DB_CONN:
        connectToDatabase()
    
    if DB_CONN:
        jobs = getAllJobs()
        
        for job in jobs:
            if job.id == jobId:
                
                # If the status level is marked as an error, then increase the number of errors for this job.
                if job.isFailed():
                    statement = "UPDATE {0} SET {1} = 'Unassigned' WHERE ID = {2};".format( DB_TABLE_NAME, DB_COLUMN_MACHINE_NAME, job.id )
                                        
                    DB_CURSOR.execute( statement )
                    DB_CONN.commit()                    
                    
                statement = "UPDATE {0} SET {1} = '{2} {3}' WHERE ID = {4};".format( DB_TABLE_NAME, DB_COLUMN_STATUS, constants.STATUS_LEVEL[ level ], status, job.id )
                                                                    
                DB_CURSOR.execute( statement )
                DB_CONN.commit()                    

def markJobAsComplete( jobId ):
    global DB_CONN
                
    if not DB_CONN:
        connectToDatabase()
    
    if DB_CONN:
        jobs = getAllJobs()
        
        for job in jobs:
            if job.id == jobId:
                statement = "UPDATE {0} SET {1} = 1 WHERE ID = {2};".format( DB_TABLE_NAME, DB_COLUMN_RESULT, job.id )
                DB_CURSOR.execute( statement )
                DB_CONN.commit()           

                return True
            
    return False

def claimJob():
    global DB_CONN
    
    if not DB_CONN:
        connectToDatabase()
    
    if DB_CONN:
        jobs = getAllJobs()
        
        # See if the machine that called this already has a job to process.
        for job in jobs:
            if job.machine == socket.gethostname() and not job.result:
                return job
            
        # No previous job to process, so claim the next one in the queue.
        for job in jobs:
            if job.status == 'Waiting' and not job.isFailed():
                statement = "UPDATE {0} SET {1} = '{2}' WHERE ID = {3};".format( DB_TABLE_NAME, DB_COLUMN_MACHINE_NAME, socket.gethostname(), job.id )
                
                DB_CURSOR.execute( statement )
                DB_CONN.commit()
                
                statement = "UPDATE {0} SET {1} = '{2}' WHERE ID = {3};".format( DB_TABLE_NAME, DB_COLUMN_STATUS, '{0} Assigned'.format( constants.STATUS_LEVEL[ constants.STATUS_LEVEL_MESSAGE ] ), job.id )
                                
                DB_CURSOR.execute( statement )
                DB_CONN.commit()                
                
                # Update this job object.
                job.machine = socket.gethostname()
                job.status = '{0} Assigned'.format( constants.STATUS_LEVEL[ constants.STATUS_LEVEL_MESSAGE ] )
                
                return job
                
def addJob( fbxFilename, modes, submittedBy = None ):
    global DB_CONN
    
    if not DB_CONN:
        connectToDatabase()
    
    if DB_CONN:
        emailAddress = RS.Config.User.Email

        if submittedBy:
            emailAddress = submittedBy
        
        # Use the job name field to store extra meta-data.  Hacky I know, I feel shame...
        # 0: Job name
        # 1: Submitted by
        # 2: Number of errors
        metadata = '{0},{1},0'.format( os.path.basename( fbxFilename ).split( '.' )[ 0 ].upper(), emailAddress )
        
        # Modes
        if type( modes ) != types.ListType and type( modes ) != types.TupleType:
            modes = [ modes ]
        
        modesStr = ''
        
        for mode in modes:
            modesStr += '{0},'.format( mode )
            
        modesStr = modesStr.rstrip( ',' )
        
        statement = "INSERT INTO {0} ( {1}, {2}, {3}, {4}, {5}, {6}, {7} ) VALUES ( '{8}', '{9}', '{10}', '{11}', {12}, {13}, '{14}' )".format( DB_TABLE_NAME,
                                                                                                                                   
                                                                                                                                    # Columns
                                                                                                                                    DB_COLUMN_JOB_NAME,
                                                                                                                                    DB_COLUMN_FILENAME,
                                                                                                                                    DB_COLUMN_MODE,
                                                                                                                                    DB_COLUMN_STATUS,
                                                                                                                                    DB_COLUMN_RESULT,
                                                                                                                                    DB_COLUMN_START_TIME,
                                                                                                                                    DB_COLUMN_MACHINE_NAME,
                                                                                                                                    
                                                                                                                                    # Values
                                                                                                                                    metadata,
                                                                                                                                    fbxFilename,
                                                                                                                                    modesStr,
                                                                                                                                    'Waiting',
                                                                                                                                    0,
                                                                                                                                    datetime.date.today(),
                                                                                                                                    'Unassigned' )
              
        DB_CURSOR.execute( statement )
        DB_CONN.commit()
        
        metadata = metadata.split( ',' )        
        
        # Send an email confirming the job submitted.
        subject = 'Cutscene Update - Job Submitted: {0}'.format( metadata[ 0 ] )
        body = 'Submitted By: {0}\n'.format( emailAddress )
        body += 'Filename: {0}\n\n'.format( fbxFilename )
        body += 'View the web monitor: {0}\n'.format( constants.WEB_MONITOR_URL )

        RS.Utils.Email.Send( 'svcrsgsanvmbuilder@rockstarsandiego.com', emailAddress, subject, body )
    
def addJobsFromFile( filename, modes = constants.JOB_MODES ):
    if str( filename ).endswith( '.batchxml' ):
        import rs_CutsceneBatchNetworkExport
        
        mgr = rs_CutsceneBatchNetworkExport.RsCutsceneBatchNetworkFileManager()
        mgr.loadFromBatchXmlFile( filename )
        
        for cutsceneName, entry in mgr.currentFile.entries.iteritems():
            addJob( entry.fbxFilename, modes )
            
    else:
        print 'The supplied filename is not supported!', filename