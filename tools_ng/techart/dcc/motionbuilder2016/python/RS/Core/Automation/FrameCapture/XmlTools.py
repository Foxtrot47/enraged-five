import os
from xml.dom import minidom


def ReadTagValueFromXmlPath(xmlPath, tagName):
    if os.path.exists(xmlPath):
        xmlFile = minidom.parse(xmlPath)
        matchedTags = xmlFile.getElementsByTagName(tagName)
        if matchedTags:
            validNode = [node.firstChild for node in matchedTags][0]
            if validNode:
                return str(validNode.nodeValue)


def WriteOutCleanedXml(xmlDoc, xmlPath):
    pretty_print = lambda data: '\n'.join([line for line in minidom.parseString(data).toprettyxml(indent=' '*2).split('\n') if line.strip()])
    docPretty = pretty_print(xmlDoc.toxml())

    docPretty = docPretty.replace("\n  </", "</")
    docPretty = docPretty.replace("\n    ", "")

    outputFile = open(xmlPath, "w")
    outputFile.write(docPretty)
    outputFile.close()


def AddElementsToXml(xmlDoc, xmlNode, elementName, elementValue):
    newElementName = xmlDoc.createElement(elementName)
    newElementValue = xmlDoc.createTextNode(elementValue)
    newElementName.appendChild(newElementValue)
    xmlNode.appendChild(newElementName)


def AddUpdateXmlFile(xmlPath, tagName, tagValue):
    xmlFile = minidom.parse(xmlPath)
    doc = minidom.Document()
    top_element = xmlFile.documentElement
    elementCheck = top_element.getElementsByTagName(tagName)

    if elementCheck == []:
        AddElementsToXml(doc, top_element, tagName, tagValue)
    else:
        elementCheck[0].firstChild.replaceWholeText(tagValue)

    doc.appendChild(top_element)
    WriteOutCleanedXml(doc, xmlPath)


def RemoveXmlTags(xmlPath, tagNameList):
    xmlFile = minidom.parse(xmlPath)
    doc = minidom.Document()
    top_element = xmlFile.documentElement
    xmlUpdated = False

    for eachTagName in tagNameList:
        foundElement = top_element.getElementsByTagName(eachTagName)
        if foundElement:
            top_element.removeChild(foundElement[0])
            xmlUpdated = True

    if xmlUpdated:
        doc.appendChild(top_element)
        WriteOutCleanedXml(doc, xmlPath)


def CreateTagValueDict(xmlPath):
    tagValueDict = {}
    xmlFile = minidom.parse(xmlPath)
    nodeList = [node for node in xmlFile.documentElement.childNodes if node.nodeType == 1]
    for eachNode in nodeList:
        tagName = str(eachNode.nodeName)
        tagValue = str(eachNode.firstChild.nodeValue)
        tagValueDict[tagName] = tagValue
    return tagValueDict