"""
Description:
    Functions for manipulating frames, syncing audio, and encoding them into video.
    Full documentation coming soon, but need to get this in now for Avid renders.
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import re
import subprocess
import CapturePaths
import FileTools


def SyncVideoTools():
    capPaths = CapturePaths.FastPath()
    videoToolPaths = [capPaths.ffmpegExePath, capPaths.ffmpegFontPath]
    for toolPath in videoToolPaths:
        cmdTxt = "p4 sync -q {0}".format(toolPath)
        subprocess.call(cmdTxt, shell=True, cwd=os.environ.get("RS_PROJROOT"))


def SearchPerforceForAudio(fbxPath):
    projectRoot = CapturePaths.FastPath().projectRoot  # detect project root based on tools, not FBX path
    capPaths = CapturePaths.FastPath(fbxPath)
    audioRoot = capPaths.audioRoot
    audioName = "{}.C.wav".format(capPaths.fbxName.upper())
    cmdTxt = "p4 files -e {0}...{1}".format(audioRoot, audioName)
    matchedAudio = subprocess.check_output(cmdTxt, shell=True, cwd=projectRoot).splitlines()
    if matchedAudio:
        p4AudioPath = os.path.join(matchedAudio[0].split(audioName)[0], audioName)
        cmdTxt = "p4 sync -q {0}".format(p4AudioPath)
        subprocess.call(cmdTxt, shell=True, cwd=projectRoot)
        cmdTxt = "p4 where {0}".format(p4AudioPath)
        mapString = subprocess.check_output(cmdTxt, shell=True, cwd=projectRoot)
        audioPath = mapString.split(".wav ")[-1].strip()
        return audioPath


def ExtendAudioFile(srcPath, destPath, duration):
    capPaths = CapturePaths.FastPath()
    inputTxt = '{0} -i {1} -f lavfi -i anullsrc=r=48000:cl=mono'.format(capPaths.ffmpegExePath, srcPath)
    filterTxt = '-filter_complex "[0:a][1:a]concat=n=2:v=0:a=1[a]" -map "[a]"'
    outputTxt = '-t {0} -loglevel error -y'.format(duration)
    cmdTxt = '{0} {1} {2} {3}'.format(inputTxt, filterTxt, outputTxt, destPath)
    subprocess.call(cmdTxt, shell=True)


def AddZeroesToNumber(number, zeroCount):
    numberString = str(number)
    while len(numberString) < zeroCount:
        numberString = "".join(["0", numberString])
    return numberString


def ConvertFrameIntToDropFrameInt(frameInt):
    offset = (frameInt + 500) / 1000
    return frameInt - offset


def GetFrameNameParts(inputFrame):
    frameText = ""
    frameDigit = ""
    inputFolder, frameName = os.path.split(inputFrame)
    fileName = os.path.splitext(frameName)[0]
    frameSplit = re.match("^(\d+)(.*)", fileName[::-1])
    if frameSplit:
        frameText = frameSplit.group(2)[::-1]
        frameDigit = frameSplit.group(1)[::-1]
    return frameText, frameDigit


def GetFrameListFromSequence(inputFrame):
    # Get Input Folder, Search Text, and Frame Index
    inputFolder, frameName = os.path.split(inputFrame)
    fileName, fileExt = os.path.splitext(frameName)
    frameText, frameDigit = GetFrameNameParts(inputFrame)
    searchText = "{0}(\d+){1}".format(re.escape(frameText), fileExt)
    frameIndex = int(frameDigit)

    # Create Frame Dict - so we only have one path for each frame number
    frameDict = {}
    for frame in os.listdir(inputFolder):
        nameMatch = re.match(searchText, frame, re.I)
        if nameMatch:
            frameString = nameMatch.group(1)
            frameNumber = int(frameString)
            if frameNumber not in frameDict.iterkeys():
                frameDict[frameNumber] = frame
            elif len(frame) > len(frameDict[frameNumber]):
                frameDict[frameNumber] = frame

    # Create Frame List - so we skip invalid frames
    frameList = []
    for frameNumber in sorted(frameDict.iterkeys()):
        if frameNumber == frameIndex:
            framePath = os.path.join(inputFolder, frameDict[frameNumber])
            frameList.append(os.path.join(inputFolder, framePath))
            frameIndex += 1
    return frameList


def FixFrameNames(inputFrame, useDropFrame=True):
    droppedFrames = 0
    inputFolder, frameName = os.path.split(inputFrame)
    fileName, fileExt = os.path.splitext(frameName)
    frameList = GetFrameListFromSequence(inputFrame)
    for frameIndex, framePath in enumerate(frameList):
        frameText, frameDigit = GetFrameNameParts(framePath)
        if useDropFrame and str(frameIndex).endswith("499"):
            FileTools.DeletePath(framePath)
            droppedFrames += 1
        else:
            fixedDigit = AddZeroesToNumber(int(frameDigit) - droppedFrames, 5)
            fixedName = "".join([frameText, fixedDigit, fileExt])
            fixedPath = os.path.join(inputFolder, fixedName)
            if framePath != fixedPath:
                FileTools.DeletePath(fixedPath)
                os.rename(framePath, fixedPath)


def FixMissingStartFrames(inputFrame, missingFrames):
    inputFolder = os.path.split(inputFrame)[0]
    fileExt = os.path.splitext(inputFrame)[1]
    frameText, frameDigit = GetFrameNameParts(inputFrame)
    frameList = GetFrameListFromSequence(inputFrame)
    frameNumber = len(frameList) + missingFrames
    lastPath = ""
    while frameNumber:
        frameNumber -= 1
        fixedNumber = AddZeroesToNumber(frameNumber, len(frameDigit))
        fixedName = "".join([frameText, fixedNumber, fileExt])
        fixedPath = os.path.join(inputFolder, fixedName)
        if frameNumber >= missingFrames:
            framePath = frameList[frameNumber - missingFrames]
            os.rename(framePath, fixedPath)
        else:
            FileTools.CopyPath(lastPath, fixedPath)
        lastPath = fixedPath


def FormatInputPathForFfmpeg(firstFramePath):
    inputFolder, frameName = os.path.split(firstFramePath)
    frameDigit = GetFrameNameParts(frameName)[1]
    fixedName = frameName.replace(frameDigit, "%0{0}d".format(len(frameDigit)))
    fixedInputPath = os.path.join(inputFolder, fixedName)
    return fixedInputPath


def CreateRdrAvidVideo(fbxPath, inputPath, outputPath, noteText="", jobLog=None):
    if jobLog:
        jobLog.LogMessage("Setting audio and ffmpeg options.")
    # Sync Video Tools
    SyncVideoTools()

    # Get Audio Path
    capPaths = CapturePaths.FastPath(fbxPath)
    audioTemp = "x:/videoToolsTemp.wav"
    audioPath = SearchPerforceForAudio(fbxPath)
    if not audioPath:
        audioName = "{}.C.wav".format(capPaths.fbxName.upper())
        audioPath = FileTools.SearchFolderForFileName(capPaths.audioRoot, audioName)

    # Fix Frame Names - not needed, will replace soon
    print " Video Details: 1920x1080 30fps"
    FixFrameNames(inputPath, useDropFrame=False)

    # Get Frame Count and Frame Digit
    frameList = GetFrameListFromSequence(inputPath)
    frameCount = len(frameList)
    frameName = os.path.split(frameList[0])[1]
    frameDigit = GetFrameNameParts(frameName)[1]

    # Get Fixed Input and Font Paths
    fixedInputPath = FormatInputPathForFfmpeg(frameList[0])
    fixedFontPath = os.path.splitdrive(capPaths.ffmpegFontPath)[1].replace("\\", "/")

    # Set Default FFMPEG Options
    inputTxt = '{0} -r 30 -start_number {1} -i "{2}"'.format(capPaths.ffmpegExePath, frameDigit, fixedInputPath)
    if capPaths.project is not "AMERICAS":
        filterTxt = ('-vf [in]drawtext="fontfile={0}: '
                     'text=\'{1}\': '
                     'fontcolor=white: '
                     'fontsize=48: '
                     'box=1: boxcolor=black@0.5: '
                     'x=80: y=1010:"[out]').format(fixedFontPath, noteText)
    else:
        filterTxt = ('-vf [in]drawtext="fontfile={0}: '
                     'text=\'{1}\': '
                     'fontcolor=white: '
                     'fontsize=48: '
                     'box=1: boxcolor=black@0.5: '
                     'x=80: y=1010:",'
                     'drawtext="fontfile={0}: '
                     'fontcolor=white: '
                     'fontsize=48: '
                     'box=1: boxcolor=red@0.5: '
                     'text=\'{2}\': '
                     'x=1800: y=1010[out]"').format(fixedFontPath, noteText, "%{eif\:n*2\:d}")

    outputTxt = '-r 30 -f mov -codec dnxhd -pix_fmt yuv422p -b:v 45M -shortest -loglevel error -y'

    # Verify FFMPEG Output Folder
    FileTools.CreateParentFolders(outputPath)

    # Audio Found - extend audio and update options
    if audioPath and os.path.exists(audioPath):
        ExtendAudioFile(audioPath, audioTemp, frameCount / 30 + 5)
        inputTxt += ' -i {0}'.format(audioTemp)
        outputTxt += ' -acodec copy'
    elif jobLog:
        jobLog.LogWarning("No audio found for this scene. Rendering video only.")

    # Create Video - via ridiculously long commandline call
    cmdTxt = '{0} {1} {2} "{3}"'.format(inputTxt, filterTxt, outputTxt, outputPath)
    if jobLog:
        jobLog.LogMessage("Rendering output video.")
    subprocess.call(cmdTxt, cwd=os.environ.get("RS_PROJROOT"), shell=True)

    # Delete Audio Temp
    FileTools.DeletePath(audioTemp)


def CreatePreviewVideo(sceneName, firstFramePath, capRev):
    # Get Input and Output Paths
    capPaths = CapturePaths.FastPath(sceneName)
    inputPath = FormatInputPathForFfmpeg(firstFramePath)
    videoName = "{0}-v{1}.mp4".format(sceneName.lower(), capRev)
    previewFolder = os.path.join(capPaths.captureRoot, "PreviewVideos")
    outputPath = os.path.join(previewFolder, videoName)

    # Cleanup Previous Preview Videos
    if not os.path.exists(previewFolder):
        os.makedirs(previewFolder)
    for oldName in os.listdir(previewFolder):
        if re.match("{0}(-v\d+)*.mp4".format(sceneName), oldName, re.I):
            oldVideoPath = os.path.join(previewFolder, oldName)
            try:
                FileTools.DeletePath(oldVideoPath)
            except:
                print "Warning: Couldn't not delete old preview", oldName
    if os.path.exists(outputPath):
        print "Warning: Preview video could not be updated, trying dupe name."
        outputPath = "{0}-1.mp4".format(os.path.splitext(outputPath)[0])

    # Call Ffmpeg via Cmd Line
    ffmpegPath = capPaths.ffmpegExePath
    fontPath = os.path.splitdrive(capPaths.ffmpegFontPath)[1].replace("\\", "/")
    drawTxt = ':fontcolor=red:fontsize=42:x=40:y=25'
    filterTxt = '-vf "drawtext=fontfile={0}:text=\'preview video v{1}\'{2}"'.format(fontPath, capRev, drawTxt)
    cmdTxt = "{0} -i {1} {2} -loglevel error -y {3}".format(ffmpegPath, inputPath, filterTxt, outputPath)
    subprocess.call(cmdTxt, cwd=os.environ.get("RS_PROJROOT"), shell=True)
    return os.path.basename(outputPath)
