from pyfbsdk import *
import os
import shutil
import time

""" NOTE: SIMPLIFIED SCRIPT
This script was originally written to be run with a variety of other automation
scripts outside of MotionBuilder (used for opening files, batching a list of
files, error detection, and creating video from image sequences).

I've simplified it quite a bit just for this test."""

def rs_GetFileName(pFile):
    """ Originally in another script. """
    lPath, lFilename = os.path.split(pFile)
    if lFilename:
        return os.path.splitext(lFilename)[0]
    else:
        return "unknown"
    del(lPath, lFilename, pFile)


#BASIC SETUP
lApp = FBApplication()
lOptions = FBVideoGrabber().GetOptions()
lFbxName = rs_GetFileName(FBApplication().FBXFileName)
ltempDir = "X:/renderTemp/"
ldestDir = ltempDir + "/" + lFbxName + "/"
lrenderCamName = "ExportCamera"
limgFormat = "320x240"


#CREATE NEW DIRECTORIES / REMOVE OLD RENDER
if os.path.exists(ltempDir) == False:
	os.makedirs(ltempDir)
if os.path.exists(ldestDir) == True:
	print 'Old render detected - removing and overwriting.'
	shutil.rmtree(ldestDir)
	startTime = time.clock()
	elapsedTime = 0
	while elapsedTime < 3:
		elapsedTime = (time.clock() - startTime)
os.makedirs(ldestDir)


#SET RENDER OPTIONS
if limgFormat == "D1NTSC":
    lOptions.CameraResolution = lOptions.CameraResolution.kFBResolutionD1NTSC
elif limgFormat == "NTSC":
    lOptions.CameraResolution = lOptions.CameraResolution.kFBResolutionNTSC
elif limgFormat == "320x240":
    lOptions.CameraResolution = lOptions.CameraResolution.kFBResolution320x240
else:
	lOptions.CameraResolution = lOptions.CameraResolution.kFBResolution320x240
	print 'WARNING: ' + limgFormat + ' IS NOT A RECOGNIZED FORMAT'
	print '   Currently supported formats: D1NTSC    NTSC    320x240'
	print '   Defaulting to 320x240\n\n'
VideoManager = FBVideoCodecManager()
VideoManager.VideoCodecMode = FBVideoCodecMode.FBVideoCodecUncompressed
lOptions.RenderAudio = False
lOptions.ShowTimeCode = True
lOptions.TimeSteps = FBTime(0,0,0,1)


#FIND AND SELECT REQUESTED CAM (OR USE SWITCHER IF NOT FOUND)
lsys = FBSystem()
lscene = lsys.Scene
lrenderCam = None
for camera in lscene.Cameras:
    if camera.Name == lrenderCamName:
        lrenderCam = camera
lsys.Scene.Renderer.UseCameraSwitcher = True
lcameraSwitcher = FBCameraSwitcher()
if lrenderCam != None:
    lcameraSwitcher.CurrentCamera = lrenderCam


#GET SHOT INFO FROM AUTOCLIPS
Story = FBStory()
Story.LockedShot = False
lTrackContainer = Story.RootEditFolder.Tracks
lrawframedata = []
for iTrack in lTrackContainer:
    if iTrack.Name.endswith("EST") and iTrack.Name.startswith("Shot"):
        gTrack = iTrack
        lShotCount = 0
        for lClip in gTrack.Clips:
                lTrackNum = gTrack.Name[5:-4]
                if len(lTrackNum) < 2:
                    lTrackNum = '0' + lTrackNum
                
                lShotCount = lShotCount + 1
                lShotString = str(lShotCount)
                if len(str(lShotString)) < 2:
                    lShotString = '0' + lShotString
                
               
                lBeginFrame = str(lClip.MarkIn.GetFrame())
                if len(lBeginFrame) < 2:
                    lBeginFrame = '0' + lBeginFrame
                    
                if len(lBeginFrame) < 3:
                    lBeginFrame = '0' + lBeginFrame
                
                if len(lBeginFrame) < 4:
                    lBeginFrame = '0' + lBeginFrame
                
                lEndFrame = str((lClip.MarkOut.GetFrame()) - 1)
                if len(lEndFrame) < 3:
                    lEndFrame = '0' + lEndFrame
                    
                if len(lEndFrame) < 4:
                    lEndFrame = '0' + lEndFrame
                    
                lcamname = filter(lambda x: x!=' ', lClip.ShotCamera.Name)
                
                lrawframedata.append(
                    'T' + lTrackNum + 'S' + str(lShotString) + '-' + lBeginFrame + '/' +
                    lEndFrame + '-' + lcamname)
lfinalframedata = sorted(lrawframedata)


#RENDER OUT EACH SHOT SEQUENTIALLY
for eachshot in lfinalframedata:
    lShotTrak = eachshot[:6]
    lStart = int(eachshot[7:11])
    lStop = int(eachshot[12:16])
    lCamName = eachshot[17:]
    lOptions.OutputFileName = (ldestDir + lFbxName + '-' +
        lShotTrak + '-' + lCamName + '-' + '.jpg')
    lOptions.TimeSpan = FBTimeSpan(FBTime(0,0,0,lStart),FBTime(0,0,0,lStop))
    lApp.FileRender(lOptions)