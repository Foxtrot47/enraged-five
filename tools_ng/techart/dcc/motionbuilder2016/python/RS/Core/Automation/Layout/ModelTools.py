"""
Description:
    A module specifically for working with models during Layout.  I split this off from the main
    LayoutTools module as it was getting pretty big, and these functions make more sense together here.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import pyfbsdk as mobu
from xml.etree import ElementTree as ET
from xml.dom import minidom

import RS.Globals
import RS.Perforce as P4
import RS.Core.ReferenceSystem.Manager as RefManager
import RS.Utils.Scene.Constraint as ConstMod
from RS.Utils import Math
from RS.Core.Automation.Layout import WatchstarTools

refManager = RefManager.Manager()
weaponXmlPath = "X:\\rdr3\\tools\\release\\techart\etc\\automation\\layoutModelDefaults.xml"


# def GetPerforceModelNames(modelsFolder):
#     """
#     Searches for model FBX files within a modelsFolder and returns a list of their names.
#     Arguments:
#         modelsFolder (string): perforce path to search within.
#         searchDepth (int): the number of subfolders to search within.
#     Returns:
#         (string): a list of model names found within the modelsFolder
#     """
#     perforceModels = []
#     argsList = ["-e", modelsFolder + "...fbx"]
#     p4Files = P4.Run("files", args=argsList)
#     for record in p4Files.Records:
#         recordText = str(record['depotFile'])
#         modelName = os.path.splitext(os.path.basename(recordText))[0].lower()
#         perforceModels.append(modelName)
#         # rootIndex = recordText.lower().index(modelsFolder.lower()) + len(modelsFolder) + 1
#         # pathText = recordText[rootIndex:]
#         # if pathText.count("/") <= searchDepth and os.path.splitext(pathText)[1].lower() == ".fbx":
#         #     modelName = os.path.splitext(pathText.split("/")[-1])[0]
#         #     perforceModels.append(modelName)
#     return perforceModels


def GetPerforceModelPaths(modelsFolder):
    """
    Searches for model FBX files within a modelsFolder and returns a list of their names.
    Arguments:
        modelsFolder (string): perforce path to search within.
        searchDepth (int): the number of subfolders to search within.
    Returns:
        (string): a list of model names found within the modelsFolder
    """
    perforceModels = []
    argsList = ["-e", modelsFolder + "...fbx"]
    p4Files = P4.Run("files", args=argsList)
    for record in p4Files.Records:
        recordText = str(record['depotFile'])
        if "face" not in recordText.lower() and "facial" not in recordText.lower():
            perforceModels.append(recordText)
    return perforceModels


def GetMocapNameLists():
    # mocapNames = []
    mocapRootPath = "//virtualproduction/previz/"
    mocapModelPaths = [path.lower() for path in GetPerforceModelPaths(mocapRootPath)]
    if mocapModelPaths:
        # pathFilterList = ["nyc/props/animals/", "nyc/props/weapons/", "nyc/proptoys/", "globalassets/propclones/"]
        # for filterName in pathFilterList:
        #     filterPath = "".join([mocapRootPath, filterName])
        #     print "FILTER PATH:", filterPath
        #     pathList = [path for path in mocapModelPaths if path.lower().startswith(filterPath)]
        #     mocapNames.append([os.path.splitext(os.path.basename(path))[0].lower() for path in pathList])
        # return mocapNames

        animalFolder = "{0}{1}".format(mocapRootPath, "nyc/props/animals/")
        weaponFolder = "{0}{1}".format(mocapRootPath, "nyc/props/weapons/")
        propToyFolder = "{0}{1}".format(mocapRootPath, "nyc/proptoys/")
        propCloneFolder = "{0}{1}".format(mocapRootPath, "globalassets/propclones/")

        mocapAnimalNames = [path for path in mocapModelPaths if path.startswith(animalFolder)]
        mocapWeaponNames = [path for path in mocapModelPaths if path.startswith(weaponFolder)]
        mocapPropToyNames = [path for path in mocapModelPaths if path.startswith(propToyFolder)]
        mocapPropToyNames.extend([path for path in mocapModelPaths if path.startswith(propCloneFolder)])

        nameLists = [[os.path.splitext(os.path.basename(path))[0] for path in pathList]
                     for pathList in [mocapAnimalNames, mocapWeaponNames, mocapPropToyNames]]
        return nameLists

    else:
        # TODO - Read Data from Backup Xml or Database
        print "We should read this data out of a backup xml or from the databse."


def DetectSignificantAnimation(mobuModel, tolerance=1.0, checkedProps=["Translation (Lcl)", "Rotation (Lcl)"]):
    """
    Detects if the range of animation on a model is above a certain threshold.
    Useful for telling when a model needs to be plotted or simply positioned (like horses).
    Arguments:
        mobuModel (FBModel): Mobu model to search for animation on
        tolerance (float): the tolerated range
        checkedProps (list): strings of the properties to check
    Returns:
        (Bool): True if significant animation detected, false otherwise
    """
    for propName in checkedProps:
        animNode = mobuModel.PropertyList.Find(propName).GetAnimationNode()
        for axisIndex in xrange(3):
            keyValues = [key.Value for key in animNode.Nodes[axisIndex].FCurve.Keys]
            valueRange = max(keyValues) - min(keyValues)
            if valueRange > tolerance:
                return True
    return False


def DetectDistanceBetweenModels(firstModel, secondModel):
    """
    Takes two mobu models a returns the distance between them.
    Arguments:
        firstModel (FBModel): First model to compare between
        secondModel (FBModel): Second model to compare between
    Returns:
        (Float): The distance between the two models.
    """
    # Get First Matrix
    firstMatrix = mobu.FBMatrix()
    firstModel.GetMatrix(firstMatrix)

    # Get Second Matrix
    secondMatrix = mobu.FBMatrix()
    secondModel.GetMatrix(secondMatrix)

    # Measure Distance - and return as a float
    distanceVector = [secondMatrix[index+12] - firstMatrix[index+12] for index in xrange(3)]
    distanceBetweenModels = Math.Length(*distanceVector)
    return distanceBetweenModels


def DetectMocapModels(searchText):
    mocapModels = []
    matchedModels = [model for model in mobu.FBSystem().Scene.RootModel.Children
                     if model.LongName.lower().startswith(searchText.lower())]
    for model in matchedModels:
        validChild = [child for child in model.Children
                      if (child.Translation.GetAnimationNode() and child.Translation.GetAnimationNode().KeyCount)
                      or (child.Rotation.GetAnimationNode() and child.Rotation.GetAnimationNode().KeyCount)]
        if validChild:
            mocapModels.append(validChild[0])
    return mocapModels


def DetectAllMocapModels():
    mocapModels = []
    for model in mobu.FBSystem().Scene.RootModel.Children:
        if not model.Name.lower().startswith("gizmo") and type(model) == mobu.FBModelNull:
            for child in model.Children:
                translationKeys = child.Translation.GetAnimationNode() and child.Translation.GetAnimationNode().KeyCount
                rotationKeys = child.Rotation.GetAnimationNode() and child.Rotation.GetAnimationNode().KeyCount
                if translationKeys or rotationKeys:
                    mocapModels.append(child)
                    break
    return mocapModels


def DetectExportModels():
    """
    Detects which characters, props, weapons, and vehicles within a FBX should be exported.
    The code is a bit crap, with extra bits tacked on over time, but it's had a ton of testing and works well.
    Returns:
        (list): A list of four string lists - [validCharacters, validProps, validWeapons, validVehicles]
    """
    # TODO: Rework this to rely less on string detection, and split FixBadDupes off into a separate function.

    # Create Valid Lists
    validNodeNames = ("Lcl Translation", "Lcl Rotation")
    validCharacters = []
    validProps = []
    validWeapons = []
    validVehicles = []

    # Detect Valid Characters - by detecting keys the skelroot
    characters = [model for model in mobu.FBSystem().Scene.Characters if not model.LongName.lower().endswith("_gs")]
    for character in characters:
        keyedNodes = []
        namespace = character.LongName.split(":")[0]
        skelRoot = mobu.FBFindModelByLabelName("{}:SKEL_ROOT".format(namespace))
        if skelRoot is None:
            continue
        # Lets check the skel root has some data
        keyedNodes = [node.Name for node in skelRoot.Translation.GetAnimationNode().Nodes if node.KeyCount > 0]
        if not keyedNodes == []:
            validCharacters.append(skelRoot.LongName)

    # Detect Prop Horses - controlled by a prop bone instead of a character rig
    mocapHorses = DetectMocapModels("pc_horse")
    gameHorses = [model for model in mobu.FBSystem().Scene.Characters if model.LongName.lower().startswith("p_c_horse")]
    horseNames = [gameHorses[i].LongName.split(":")[0] for i in xrange(len(mocapHorses)) if i < len(gameHorses)]
    for name in horseNames:
        validCharacters.append("{}:SKEL_ROOT".format(name))

    # Detect Valid Props, Weapons, and Vehicles - by detecting keys on constrained objects
    for model in mobu.FBSystem().Scene.RootModel.Children:
        childMover = [child for child in model.Children if child.Name.lower() == "mover"]
        childController = [child for child in model.Children if child.Name.lower().endswith("_control")]
        # Catch rigged vehicles
        if not childController:
            childController = [child for child in model.Children if child.Name.lower() == "rig"]
        # Catch new weapon hierarchy
        if not childController:
            childController = [child for child in model.Children if child.Name.lower().startswith("control_")]
        if childMover and childController:
            rootModel = [child for child in childMover[0].Children]
            if rootModel:
                itemName = rootModel[0].Name
                rootName = rootModel[0].LongName
                if itemName.lower() == "gun_root":
                    validWeapons.append(rootName)
                elif itemName.lower() == "wpn_root":
                    validWeapons.append(rootName)
                elif itemName.lower() == "chassis":
                    validVehicles.append(rootName)
                else:
                    validProps.append(rootName)

    # Create Export Model List
    exportModelList = [validCharacters, validProps, validWeapons, validVehicles]

    # TODO: Disabled Dupe Fix Code Below - this doesn't belong here and should be split off if needed later.
    # # Fix Bad Dupes - if we've got a dupe model but no original, we swap their names before export
    # for modelList in exportModelList:
    #     modelNames = [model.split(":")[0] for model in modelList]
    #     badDupes = [name for name in modelNames if "^" in name and name.split("^")[0] not in modelNames]
    #     for badName in badDupes:
    #         print "BAD DUPE:", badName
    #         goodName = badName.split("^")[0]
    #         tempName = "AutoLayoutTemp"
    #         mobu.FBSystem().Scene.NamespaceRename(goodName, tempName)
    #         mobu.FBSystem().Scene.NamespaceRename(badName, goodName)
    #         mobu.FBSystem().Scene.NamespaceRename(tempName, badName)
    #         nameIndex = modelNames.index(badName)
    #         nameEnd = modelList[nameIndex].split(":")[1]
    #         fullName = "{}:{}".format(goodName, nameEnd)
    #         modelList[nameIndex] = fullName

    return exportModelList


def SetupCharacters():
    """
    Detects mocap actors and game models from Watchstar, imports any
    missing references, and maps game models to their matched mocap actors.
    """
    # Create refManager and modelDict from Watchstar
    refManager = RefManager.Manager()
    fbxPath = mobu.FBApplication().FBXFileName
    takeName = mobu.FBSystem().CurrentTake.Name
    modelDict = WatchstarTools.GetCharacterDict(fbxPath, takeName)

    # Main Character Loop
    for actorText, pedPath in modelDict.iteritems():

        # Find Actor Namespace - starting with full name then shortening
        actorName = actorText.split("GC_ROM_")[-1].split("_")[0]
        for charIndex in xrange(len(actorName)):
            searchText = actorName[:-charIndex] if charIndex else actorName
            matchedName = [namespace.Name for namespace in mobu.FBSystem().Scene.Namespaces
                           if namespace.Name.lower() == searchText.lower()]
            if matchedName:
                actorName = matchedName[0]
                break

        # Search for Actor's Mocap Model
        actorString = "{}:".format(actorName.upper())
        mocapModel = [model for model in mobu.FBSystem().Scene.RootModel.Children
                      if model.LongName.upper().startswith(actorString) and len(model.Children) > 0]
        if mocapModel:

            # Check Model is Animated and has Mocap Character
            mocapAnimated = DetectSignificantAnimation(mocapModel[0].Children[0])
            mocapCharacter = [character for character in mobu.FBSystem().Scene.Characters
                              if character.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId) == mocapModel[0].Children[0]]
            if mocapAnimated and mocapCharacter:

                # Search for Valid Ped Character - has correct name with input set the mocap character
                pedName = pedPath.split("/")[-1][:-4].split("^")[0]
                pedMatches = [character for character in mobu.FBSystem().Scene.Characters
                              if character.LongName.lower().startswith(pedName.lower())]
                validPedChar = [character for character in pedMatches if character.InputCharacter == mocapCharacter[0]]
                if not validPedChar:

                    # No Valid Ped Character - we try searching the FBX for unused ped characters
                    pedCharacter = [character for character in pedMatches if character.InputCharacter is None]
                    if not pedCharacter:

                        # No Unused Ped Characters Found - so we import a new reference
                        p4RefPath = "{0}/{1}.fbx".format(os.path.split(pedPath)[0], pedName)
                        localRefPath = "x:{}".format(p4RefPath[1:])
                        P4.Sync(p4RefPath)
                        refManager.CreateReferences(localRefPath)
                        pedCharacter = [character for character in mobu.FBSystem().Scene.Characters][::-1]

                    # Hookup Unused Ped Character to Mocap Character
                    pedCharacter[0].InputCharacter = mocapCharacter[0]
                    pedCharacter[0].InputType = mobu.FBCharacterInputType.kFBCharacterInputCharacter
                    pedCharacter[0].Active = True


def SetupHorses():
    """
    Searches the scene for keyed mocap prop horses, and move/plots any game horses to their position.
    Useful during layout when game horse positions often don't match the mocap horse.
    """
    # Get Ref Manager and Mocap / Game Horse Lists
    refManager = RefManager.Manager()
    mocapHorses = DetectMocapModels("pc_horse")
    gameHorses = [model for model in mobu.FBSystem().Scene.Characters if model.LongName.lower().startswith("p_c_horse")]

    # Set Base Layer and Disable Snapping - in case we need to add keys
    mobu.FBSystem().CurrentTake.SetCurrentLayer(0)
    RS.Globals.Player.SnapMode = mobu.FBTransportSnapMode.kFBTransportSnapModeNoSnap

    # Main Horse Loop
    for horseIndex, mocapHorse in enumerate(mocapHorses):

        # Get Game Horse - using an available horse or by referencing a new one
        if horseIndex < len(gameHorses):
            gameHorse = gameHorses[horseIndex]
        else:
            p4RefPath = "//rdr3/art/animation/resources/characters/Models/Animals/P_C_Horse_01.FBX"
            localRefPath = "x:{}".format(p4RefPath[1:])
            P4.Sync(p4RefPath)
            refManager.CreateReferences(localRefPath)
            newHorses = [model for model in mobu.FBSystem().Scene.Characters if "horse" in model.LongName.lower() and model not in gameHorses]
            gameHorse = newHorses[0]
        
        moverName = "{}:mover".format(gameHorse.LongName.split(":")[0])
        gameMover = mobu.FBFindModelByLabelName(moverName)
        keyTimes = [0.0]

        # Detect Horse Animation - and get key times if animated
        horseAnimation = DetectSignificantAnimation(mocapHorse)
        if horseAnimation:
            transKeys = mocapHorse.PropertyList.Find("Translation (Lcl)").GetAnimationNode().Nodes[0].FCurve.Keys
            keyTimes = [eachKey.Time.GetSecondDouble() for eachKey in transKeys]

        # Remove Existing Constraints
        [const.FBDelete() for const in mobu.FBSystem().Scene.Constraints if const.TypeInfo == 125 and const.Components[0] == mocapHorse]
        mobu.FBSystem().Scene.Evaluate()

        # Clear Existing Keys
        [node.FCurve.EditClear() for node in gameMover.Translation.GetAnimationNode().Nodes]
        [node.FCurve.EditClear() for node in gameMover.Rotation.GetAnimationNode().Nodes]

        # Force Visible in Groups
        [setattr(group, "Show", True) for group in mobu.FBSystem().Scene.Groups if group.LongName == gameHorse.LongName]

        # Key Loop - moves game horse to mocap Horse position at each keyTime
        for keyFloat in keyTimes:
            keyTime = mobu.FBTime()
            keyTime.SetSecondDouble(keyFloat)
            RS.Globals.Player.Goto(keyTime)

            # Get Mocap Horse Global Matrix
            mocapMatrix = mobu.FBMatrix()
            mocapHorse.GetMatrix(mocapMatrix)

            # Get Game Horse Parent's Global Inverse and Scale Matrix
            inverseMatrix = mobu.FBMatrix()
            gameMover.Parent.GetMatrix(inverseMatrix, mobu.FBModelTransformationType.kModelInverse_Transformation, True)
            scaleMatrix = mobu.FBMatrix()
            gameMover.Parent.GetMatrix(scaleMatrix, mobu.FBModelTransformationType.kModelScaling, True)

            # Calculate Final Position Matrix and Set to Game Horse
            finalMatrix = inverseMatrix * mocapMatrix * scaleMatrix
            gameMover.SetMatrix(finalMatrix, mobu.FBModelTransformationType.kModelTransformation, False)

            # Add Key - only if horse is actually animated
            if horseAnimation:
                gameMover.Translation.Key()
                gameMover.Rotation.Key()

    # Restore Default Snapping
    RS.Globals.Player.SnapMode = mobu.FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames


# TODO - Below code is still being tested, possibly rewritten for new mocap setup workflow


def CreateGameModelDicts():
    gameCharacterPaths = []
    gameCharacterFolders = ["//rdr3/art/animation/resources/characters/models/animals/",
                            "//rdr3/art/animation/resources/characters/models/cutscene/",
                            "//rdr3/art/animation/resources/characters/models/ingame/ig/"]
    for folderPath in gameCharacterFolders:
        gameCharacterPaths.extend([path for path in GetPerforceModelPaths(folderPath)
                                   if os.path.split(path)[0].lower() == folderPath[:-1]])
    gameCharacterDict = {os.path.splitext(os.path.basename(path))[0].lower(): path for path in gameCharacterPaths}

    gamePropPaths = []
    gamePropFolders = ["//rdr3/art/animation/resources/props/cutscene/",
                       "//rdr3/art/animation/resources/props/ingame/",
                       "//rdr3/art/animation/resources/vehicles/"]
    for folderPath in gamePropFolders:
        gamePropPaths.extend([path for path in GetPerforceModelPaths(folderPath)])
    gamePropDict = {os.path.splitext(os.path.basename(path))[0].lower(): path for path in gamePropPaths}

    return gameCharacterDict, gamePropDict


def SetupGameModels():

    mocapModels = DetectAllMocapModels()
    characterRoots = [char.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId) for char in mobu.FBSystem().Scene.Characters]
    mocapAnimalNames, mocapWeaponNames, mocapPropToyNames = GetMocapNameLists()
    gameCharacterDict, gamePropDict = CreateGameModelDicts()

    fbxPath = mobu.FBApplication().FBXFileName
    takeName = mobu.FBSystem().CurrentTake.Name
    wsCharacterDict = WatchstarTools.GetCharacterDict(fbxPath, takeName)
    wsPropDict = WatchstarTools.GetPropDict(fbxPath, takeName)

    for mocapModel in mocapModels:
        print ""
        defaultName = None
        modelName = mocapModel.LongName
        if mocapModel in characterRoots:
            print "CHARACTER:", modelName
            defaultName = "CS_GenStoryMale"
            gameModelPath = GetGameModelPath(0, modelName, wsCharacterDict, gameCharacterDict, defaultName)
            print "  GAME MODEL PATH:", gameModelPath
            if gameModelPath:
                # UpdateBackupXml(mocapModel.LongName, gameModelPath)
                SetupCharacter(mocapModel, gameModelPath)

        elif [name for name in mocapAnimalNames if modelName.lower().startswith(name)]:
            print "ANIMAL:", modelName
            if "PC_HORSE" in modelName:
                defaultName = "P_C_Horse_01"
            gameModelPath = GetGameModelPath(1, modelName, wsPropDict, gameCharacterDict, defaultName)
            print "  GAME MODEL PATH:", gameModelPath
            if gameModelPath:
                # UpdateBackupXml(mocapModel.LongName, gameModelPath)
                SetupProp(mocapModel, gameModelPath)


        elif [name for name in mocapWeaponNames if modelName.lower().startswith(name)]:
            print "WEAPON:", modelName
            if "repeater_rifle" in modelName.lower():
                defaultName = "w_rifle_boltAction01"
            elif "shotgun" in modelName.lower():
                defaultName = "w_shotgun_doubleBarrel01"

            gameModelPath = GetGameModelPath(2, modelName, wsPropDict, gamePropDict, defaultName)
            print "  GAME MODEL PATH:", gameModelPath
            if gameModelPath:
                # UpdateBackupXml(mocapModel.LongName, gameModelPath)
                SetupProp(mocapModel, gameModelPath)


        elif [name for name in mocapPropToyNames if modelName.lower().startswith(name)]:
            print "PROP:", modelName
            gameModelPath = GetGameModelPath(3, modelName, wsPropDict, gamePropDict)
            print "  GAME MODEL PATH:", gameModelPath
            if gameModelPath:
                SetupProp(mocapModel, gameModelPath)


        else:
            print "UNKNOWN MODEL:", modelName


def DetectMatchingName(modelText, mocapText):
    if modelText in mocapText or mocapText in modelText:
        return True
    for searchText, mainText in ((modelText, mocapText), (mocapText, modelText)):
        while len(searchText) > 3:
            if searchText in mainText:
                return True
            searchText = searchText[:-1]


def GetModelNameFromWatchstar(modelType, modelName, modelDict):
    modelText = modelName.split(":")[0].split("^")[0].lower().split("_bone")[0]
    for mocapText, modelPath in modelDict.iteritems():
        if mocapText != "None" and modelPath != "None":
            if modelType == 0:
                modelText = "".join([char for char in modelText if char != " " and not char.isdigit()])
                mocapText = mocapText.split("GC_ROM_")[-1].split("_")[0].lower()
                matchedName = DetectMatchingName(modelText, mocapText)
                if matchedName:
                    return os.path.splitext(os.path.basename(modelPath))[0].split("^")[0]

            elif modelType in [1, 2]:
                mocapText = os.path.splitext(os.path.basename(mocapText))[0].split("^")[0].lower().replace(" ", "_")
                if modelName.lower().startswith(mocapText):
                    return os.path.splitext(os.path.basename(modelPath))[0].split("^")[0]

            elif modelType == 3:
                toyNames = ["proptoy", "propclone"]
                matchedToy = [name for name in toyNames if mocapText.startswith(name) and modelText.startswith(name)]
                if matchedToy:
                    modelNumber = [char for char in modelText.split(matchedToy[0])[-1] if char.isdigit()]
                    mocapNumber = [char for char in mocapText.split(matchedToy[0])[-1] if char.isdigit()]
                    if modelNumber and mocapNumber:
                        if int("".join(modelNumber)) == int("".join(modelNumber)):
                            return os.path.splitext(os.path.basename(modelPath))[0].split("^")[0]


def GetBackupXmlRoot():
    rootNode = None
    try:
        rootNode = ET.parse(weaponXmlPath).getroot()
    except:
        print "Error: Weapon Xml could not be parsed."
    return rootNode


def GetModelNameFromBackupXml(modelName):
    print " SEARCHING BACKUP XML"
    searchText = modelName.split(":")[0].split("^")[0].lower().split("_bone")[0]
    # P4.Sync(weaponXmlPath)
    rootNode = GetBackupXmlRoot()
    if rootNode is not None:
        matchedModel = [node.find("gameName").text for node in rootNode.getchildren()
                        if node.find("mocapName").text.lower() == searchText.lower()]
        if matchedModel:
            return matchedModel[0].lower()


def UpdateBackupXml(modelName, gameModelPath):
    mocapName = modelName.split(":")[0].split("^")[0].split("_bone")[0]
    gameName = os.path.splitext(os.path.basename(gameModelPath))[0].split("^")[0]
    # P4.Edit(weaponXmlPath)
    rootNode = GetBackupXmlRoot()
    if rootNode is not None:
        matchedModel = [node for node in rootNode.getchildren()
                        if node.find("mocapName").text.lower() == mocapName.lower()]
        if matchedModel:
            gameNameNode = matchedModel[0].find("gameName")
            if gameNameNode.text != gameName:
                print "updating old node"
                gameNameNode.text = gameName
        else:
            print "adding new node"
            modelNode = ET.SubElement(rootNode, "model")
            mocapNameNode = ET.SubElement(modelNode, "mocapName")
            mocapNameNode.text = mocapName
            gameNameNode = ET.SubElement(modelNode, "gameName")
            gameNameNode.text = gameName

        xmlString = ET.tostring(rootNode, 'utf-8')
        xmlString = minidom.parseString(xmlString).toprettyxml(indent="\t")
        xmlString = "\n".join([line for line in xmlString.split("\n") if "<" in line])

        # Write Out xmlString
        xmlFile = open(weaponXmlPath, "w")
        xmlFile.write(xmlString)
        xmlFile.close()


def GetGameModelPath(modelType, modelName, watchstarDict, gameModelDict, defaultName=None):
    nameList = [GetModelNameFromWatchstar(modelType, modelName, watchstarDict)]
    if modelType != 3:
        nameList.append(GetModelNameFromBackupXml(modelName))
    if defaultName:
        nameList.append(defaultName)
    for gameName in nameList:
        if gameName and gameName.lower() in gameModelDict.iterkeys():
            return gameModelDict[gameName.lower()]


def ForceModelVisibility(modelName):
    dupeName = modelName.split(":")[0].lower()
    groupName = "{0}:{1}".format(dupeName, dupeName.split("^")[0])
    matchedGroup = [group for group in mobu.FBSystem().Scene.Groups if group.LongName.lower() == groupName]
    if matchedGroup and matchedGroup[0].Show is False:
        matchedGroup[0].Show = True
        moverName = "{}:mover".format(dupeName)
        moverGroup = [group for group in mobu.FBSystem().Scene.Groups if group.LongName.lower() == moverName]
        if moverGroup:
            moverGroup[0].Show = False


def SetupCharacter(mocapModel, gameModelPath):
    mocapCharacter = [char for char in mobu.FBSystem().Scene.Characters
                      if char.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId) == mocapModel]
    gameModelName = os.path.splitext(os.path.basename(gameModelPath))[0].split("^")[0].lower()
    matchedCharacters = [character for character in mobu.FBSystem().Scene.Characters
                         if character.LongName.lower().startswith(gameModelName)]
    gameCharacter = [character for character in matchedCharacters if character.InputCharacter == mocapCharacter[0]]
    if not gameCharacter:

        # No Valid Ped Character - we try searching the FBX for unused ped characters
        gameCharacter = [character for character in matchedCharacters if character.InputCharacter is None]
        if not gameCharacter:

            # No Unused Ped Characters Found - so we import a new reference
            P4.Sync(gameModelPath)
            localRefPath = "x:{}".format(gameModelPath[1:])
            refManager.CreateReferences(localRefPath)
            gameCharacter = [character for character in mobu.FBSystem().Scene.Characters][::-1]

        # Hookup Unused Ped Character to Mocap Character
        gameCharacter[0].InputCharacter = mocapCharacter[0]
        gameCharacter[0].InputType = mobu.FBCharacterInputType.kFBCharacterInputCharacter
        gameCharacter[0].Active = True

    ForceModelVisibility(gameCharacter[0].LongName)


def GetParentedModelNames():
    parentedModelNames = []
    validParentTypes = [mobu.FBModelMarker, mobu.FBModel]
    for const in [const for const in mobu.FBSystem().Scene.Constraints if const.TypeInfo == 125]:
        for parent in [parent for parent in const.Parents if type(parent) in validParentTypes]:
            parentedModelNames.append(parent.LongName.split(":")[0].lower())
    return parentedModelNames


def SetupProp(mocapModel, gameModelPath):
    mocapConstraints = [const for const in mobu.FBSystem().Scene.Constraints
                        if const.TypeInfo == 125 and mocapModel in const.Components]
    gameModelName = os.path.splitext(os.path.basename(gameModelPath))[0].split("^")[0].lower()
    gameProp = [parent for const in mocapConstraints for parent in const.Parents
                if parent.LongName.lower().startswith(gameModelName)]
    print gameProp
    if not gameProp:
        print "NEW CONSTRAINT NEEDED"
        parentedModelNames = GetParentedModelNames()
        print "PARENTED MODEL NAMES:", parentedModelNames
        gameProp = [model for model in mobu.FBSystem().Scene.RootModel.Children if
                    model.LongName.lower().startswith(gameModelName) and
                    model.LongName.lower().split(":")[0] not in parentedModelNames]
        print "GAME MODEL:", gameProp
        if not gameProp:
            print "    CREATING REFERENCE"
            P4.Sync(gameModelPath)
            localRefPath = "x:{}".format(gameModelPath[1:])
            refManager.CreateReferences(localRefPath)
            gameProp = [model for model in mobu.FBSystem().Scene.RootModel.Children[::-1]]

        gameCtrl = [child for child in gameProp[0].Children if child.LongName.lower().endswith("control")]
        if not gameCtrl:
            gameCtrl = [mobu.FBFindModelByLabelName("{}:mover".format(gameProp[0].LongName.split(":")[0]))]
        if gameCtrl:
            print "    CONSTRAINING:", gameCtrl[0].LongName
            ConstMod.Constraint(ConstMod.PARENT_CHILD, "MocapToGame", gameCtrl[0], mocapModel, Active=True, Lock=True)

    ForceModelVisibility(gameProp[0].LongName)
