"""
Description:
    One main function for preparing an FBX for a capture export.

    Most prep steps happen within this function, but a few are handled by
    CamUtils (for simple MB stuff used frequently) and camKeyTasks (for the
    saving and loading of camera key data). Also and a few utility functions
    like ToyBox, ContentUpdate, and the Automation.FrameCapture's FileTools.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import pyfbsdk as mobu

from RS.Core.Camera.KeyTools import camKeyTasks
from RS.Core.Automation.FrameCapture import CapturePaths, FileTools
from RS.Core.Automation.Layout import LayoutTools
from RS.Core.Camera import CameraLocker, CamUtils, ClipTools, Lib as CamLib


def PrepFbxForCapture(capObj):
    """
    Mid level function to set up a capture for export based on the capture type.
    Arguments:
        cabObj: An instance of the CaptureModel object.
    """
    # Update Status
    capObj.UpdateTag("mobuStatus", "PREP FBX")

    # Set Basic Paths
    capPaths = CapturePaths.FastPath(capObj.fbxPath)
    cutsPath = capPaths.cutsPath
    capDataRoot = capPaths.capDataRoot
    dataCutsFolder = os.path.join(capDataRoot, "CutFiles")

    camName = getattr(capObj, "camName", None)

    if capObj.captureType == "SIMPLE":
        # Open Full FBX
        mobu.FBApplication().FileOpen(capObj.fbxPath, True)
        capObj.UpdateTime("fbxOpenTime")

        # Detect Empty Full Fbx
        if mobu.FBApplication().FBXFileName == "":
            return "EXPORT ERROR: Full Fbx Is Empty - it needs to be resubmitted before full capture possible."

        # Unlock Cmas and Run Capture Layout
        camLockObj= CameraLocker.LockManager()
        camLockObj.setAllCamLocks(False)
        LayoutTools.CaptureLayout()

        # TODO: NEW CAM NAME CODE
        if camName:
            CamUtils.DupeRtcsToRsCams()
            camList = [cam for cam in mobu.FBSystem().Scene.Cameras if not cam.SystemCamera]
            foundCam = [cam for cam in camList if camName.lower() in cam.Name.lower()]
            if foundCam:
                CamLib.Manager.DeleteSwitcherKeys()
                switcher = CamLib.Manager.GetCameraSwitcher()
                switcherNode = switcher.PropertyList.Find('Camera Index').GetAnimationNode()
                camIndex = camList.index(foundCam[0]) + 1
                switcherNode.KeyAdd(mobu.FBTime(0, 0, 0, 0), float(camIndex))

        # Set Ranges
        endFrame = getattr(capObj, "endFrame", None)
        if endFrame is None:
            endFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
            capObj.UpdateTag("endFrame", str(endFrame))
        ClipTools.SetHardRanges(int(endFrame))
        
        # Save Cam Xml - if none found
        camXmlFound = camKeyTasks.CheckCamXmlExists(capObj.fbxPath)
        if not camXmlFound:
            print "Capture Prep: No cam xml found, saving cam data."
            camKeyTasks.SaveAndSubmitFbxCams()

        # Plot Export Cam
        CamUtils.SetupExportCam()

    if capObj.captureType == "FULL":
        # Set Ranges in Empty FBX
        ClipTools.SetHardRanges(capObj.endFrame)

        # Load Cams in Empty FBX - via camKeyTasks
        camsLoaded = camKeyTasks.LoadKeyXmlIntoEmptyFbx(capObj.fbxPath, camName=camName)
        if not camsLoaded:
            return "EXPORT ERROR: Error loading cam data into empty Fbx during prep."

        # Create Transfer Cam Data - via camKeyTasks
        transCamData = camKeyTasks.CreateTransferCam()

        # Open Full FBX
        mobu.FBApplication().FileOpen(capObj.fbxPath, True)
        capObj.UpdateTime("fbxOpenTime")

        # Detect Empty Full Fbx
        if mobu.FBApplication().FBXFileName == "":
            return "EXPORT ERROR: Full Fbx Is Empty - it needs to be resubmitted before full capture possible."

        # Run Capture Layout Steps
        LayoutTools.CaptureLayout()

        # Set Ranges in Full FBX
        ClipTools.SetHardRanges(capObj.endFrame)

        # Load Transfer Cam Data - via camKeyTasks
        camKeyTasks.LoadTransferCam(transCamData)

        # Plot Planes - GTA5 only
        if capPaths.project == "GTA5":
            CamUtils.PlotGta5Planes()

    if capObj.captureType == "FAST":
        # Update fbxOpenTime
        capObj.UpdateTime("fbxOpenTime")
        
        # Save & Reload - hacky workaround to refresh exporter dlc pack
        FileTools.MakePathWriteable(capObj.fbxPath)
        mobu.FBApplication().FileSave(capObj.fbxPath)
        mobu.FBApplication().FileOpen(capObj.fbxPath, True)

        # Set Ranges
        ClipTools.SetHardRanges(capObj.endFrame)

        # Load Cams in Empty FBX - via camKeyTasks
        camsLoaded = camKeyTasks.LoadKeyXmlIntoEmptyFbx(capObj.fbxPath, camName=camName)
        if not camsLoaded:
            return "EXPORT ERROR: Error loading cam data into empty Fbx during fast prep."

        # Setup Export Cam
        CamUtils.SetupExportCam()

        # Create Toybox
        LayoutTools.SetupToybox(showMessages=False)

        # Copy Cut Folder from Network - GTA5 and RDR3
        # todo: this is janky, all cutfile stuff should probably be in CaptureExport instead?
        if capPaths.project != "AMERICAS":
            FileTools.CopyFolderContents(dataCutsFolder, cutsPath)

    # Cleanup Bad Components
    # CamUtils.BadCompCleanup()

    # Set Thumbnail Frame
    thumbnailFrame = CamUtils.GetThumbnailFrame()
    if capPaths.isCinematicFbx:
        thumbnailFrame /= 2
    capObj.UpdateTag("thumbnailFrame", str(thumbnailFrame))
    CamUtils.GoToFrameNumber(thumbnailFrame)

    # Prep Success
    return "PREP SUCCESS"
