import os


class FastPath(object):

    projectInstalled = os.environ["RS_PROJECT"].upper()

    def __init__(self, fbxPath=None):
        fbxPath = fbxPath or ""
        fbxPath = "x:" + fbxPath[1:] if fbxPath.startswith("//") else fbxPath
        fbxPath = fbxPath.replace("/", "\\")
        pathParts = [part.lower() for part in fbxPath.split(os.sep)]
        self.fbxName = os.path.splitext(pathParts[-1])[0]

        # Get Project Paths
        self.project = self.projectInstalled
        if len(pathParts) > 2:
            if "rdr3" == pathParts[1]:
                self.project = "RDR3"
            elif "gta5" in pathParts[1]:
                self.project = "GTA5"
            elif "americas" == pathParts[1]:
                self.project = "AMERICAS"
        self.projectRoot = os.path.join("x:" + os.sep, self.project.lower())

        # Get Pack Root
        self.packRoot = self.projectRoot
        if "art" in pathParts:
            artIndex = pathParts.index("art")
            self.packRoot = os.sep.join(pathParts[:artIndex])
        elif self.project == "RDR3":
            self.packRoot = os.path.join(self.projectRoot, "dlc", "tuPacks", "dlc_009", "mp009")
        elif self.project == "GTA5":
            self.packRoot = "x:\\gta5_dlc\\mpPacks\\mpSecurity"

        # Get Art Roots
        self.artRoot = os.path.join(self.packRoot, "art")
        if self.project == "RDR3":
            self.animationRoot = os.path.join(self.artRoot, "animation")
        elif self.project == "GTA5":
            self.animationRoot = os.path.join(self.artRoot, "ng", "animation")
        elif self.project == "AMERICAS":
            self.animationRoot = os.path.join(self.artRoot, "dev", "animation")
        self.cutsceneRoot = os.path.join(self.animationRoot, "cutscene", "!!scenes")
        if self.project == "AMERICAS":
            self.cutsceneRoot = os.path.join(self.animationRoot, "cinematic", "!!scenes")
        self.audioRoot = os.path.join(self.animationRoot, "resources", "audio")
        if self.project == "GTA5":
            self.audioRoot = "x:\\gta5_dlc\\art\\animation\\resources\\Audio"

        # Get Assets Root
        self.assetsRoot = os.path.join(self.packRoot, "assets")
        if self.project == "AMERICAS" or self.project == "RDR3" and "dlc" in pathParts:
            self.assetsRoot = os.path.join(self.assetsRoot, "dev")
        if self.project == "GTA5":
            self.assetsRoot += "_ng"

        # Detect Cinematic Fbx and Mission Name
        self.isCinematicFbx = False
        if self.project == "AMERICAS" and "animation\\cinematic\\" in fbxPath.lower():
            self.isCinematicFbx = True
        self.missionName = self.fbxName
        if "!!scenes" in pathParts and pathParts.index("!!scenes") + 1 < len(pathParts):
            missionIndex = pathParts.index("!!scenes") + 1
            self.missionName = pathParts[missionIndex]
        elif len(self.fbxName.split("_")) > 1 and len(self.fbxName.split("_")) > 2:
            self.missionName = self.fbxName.split("_")[0]

        # Get Export Roots
        self.cutsRoot = os.path.join(self.assetsRoot, "cuts")
        self.iasRoot = os.path.join(self.assetsRoot, "export", "anim", "anim_scenes", "cutscene")
        self.zipRoot = os.path.join(self.assetsRoot, "export", "anim", "cutscene")
        if self.isCinematicFbx:
            self.cutsRoot = os.path.join(self.assetsRoot, "anim", "cinematic")
            self.iasRoot = os.path.join(self.assetsRoot, "export", "anim", "cinematic", "anim_scene")
            self.zipRoot = os.path.join(self.assetsRoot, "export", "anim", "cinematic", "anim")

        # Get Export Paths
        self.cutsPath = os.path.join(self.cutsRoot, self.fbxName)
        self.cutListRoot = os.path.join(self.cutsRoot, "!!Cutlists")
        self.mergeListRoot = os.path.join(self.cutsRoot, "!!Mergelists")
        self.zipFolder = os.path.join(self.zipRoot, self.missionName)
        self.zipPath = os.path.join(self.zipFolder, "{}.icd.zip".format(self.fbxName))
        self.iasPath = os.path.join(self.iasRoot, "@{}.ias".format(self.fbxName))
        if self.isCinematicFbx:
            self.iasPath = os.path.join(self.iasRoot, self.missionName, "{}.ias".format(self.fbxName))

        # Get Tools Paths
        self.toolsRoot = os.path.join(self.projectRoot, "tools", "release")
        if self.project == "GTA5":
            self.toolsRoot = os.path.join(self.projectRoot, "tools_ng")
        self.frameDumpExePath = os.path.join(self.toolsRoot, "techart", "standalone", "FrameDumper", "FrameDumper.exe")

        # FFMPEG Paths - hardcoded for now, only supported in RDR
        self.ffmpegExePath = "x:\\rdr3\\tools\\release\\bin\\video\\ffmpeg\\3_0_1\\ffmpeg.exe"
        self.ffmpegFontPath = "x:\\rdr3\\tools\\release\\techart\\resources\\fonts\\rdr2lucid.ttf"

        # Get Mobu Exporter Paths
        self.mobuExePath = os.path.join(self.toolsRoot, "bin", "MotionbuilderLauncher", "MotionBuilderLauncher.exe")
        wildWestCaptureRoot = os.path.join("x:" + os.sep, "wildwest", "script", "python", "standalone", "CaptureRender")
        self.mobuScriptPath = os.path.join(wildWestCaptureRoot, "crapMBScript.py")
        self.mobuCapPath = os.path.join("x:" + os.sep, "mobuCap.txt")
        animPreProcessorName = "RSG.Pipeline.Processor.Animation.Cutscene.PreProcess"
        processorExt = ".xml" if self.project == "GTA5" else ".meta"
        self.animPreProcessorPath = os.path.join(self.toolsRoot, "etc", "processors", animPreProcessorName + processorExt)

        # Get Preview Path
        self.previewPath = os.path.join(self.projectRoot, "build", "dev", "preview")
        if self.project == "GTA5":
            self.previewPath = "x:\\gta5\\build\\dev_ng\\preview"
        elif self.project == "RDR3":
            self.previewPath = os.path.join(self.packRoot, "build", "dev", "preview")

        # Get Capture Paths
        self.captureRoot = os.path.join("N:" + os.sep, "RSGNYC", "Production", "CaptureRenders")
        self.capDataRoot = os.path.join(self.captureRoot, "CaptureData", self.project, self.fbxName)
        self.expJobQueue = os.path.join(self.captureRoot, "ExportQueue", self.project)
        self.capJobQueue = os.path.join(self.captureRoot, "CaptureQueue", self.project)
        self.expJobBackup = os.path.join(self.expJobQueue, "backups")
        self.capJobBackup = os.path.join(self.capJobQueue, "backups")

        # Get Range Paths
        self.rangePathXml = os.path.join(self.toolsRoot, "techart", "etc", "config", "camera", "rangePaths.xml")
