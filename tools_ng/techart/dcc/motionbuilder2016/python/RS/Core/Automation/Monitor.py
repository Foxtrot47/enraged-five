import os
import sys
import time
import datetime
import socket

import RS.Core.Automation
import RS.Core.Automation.Server


def createHtmlTable( sectionName, jobs ):
    html = '<tr>\n' 
    html += '<td><font size="3" face="Arial"><b>{0}</b></font></td>\n'.format( sectionName )
    html += '</tr>\n' 
    
    html += '<tr>\n' 
    html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Job ID</b></font></td>\n'
    html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Job Name</b></font></td>\n'
    html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Submitted By</b></font></td>\n'
    html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Mode</b></font></td>\n'
    html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Status</b></font></td>\n'

    if sectionName == 'Completed Today' or sectionName == 'Failed Today':
        html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Total Time</b></font></td>\n'
        
    else:
        html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Elapsed Time</b></font></td>\n'    
    
    html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Filename</b></font></td>\n'
    html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Log</b></font></td>\n'
    html += '<td bgcolor="#C0C0C0"><font size="1.5" face="Arial"><b>Machine</b></font></td>\n'
    html += '</tr>\n'      
    
    for job in jobs:
        bgcolor = '#C0C0C0'
        fgcolor = '#000000'
        
        if RS.Core.Automation.STATUS_LEVEL[ RS.Core.Automation.STATUS_LEVEL_MESSAGE ] in job.status:
            bgcolor = '#FFFFFF'
            
        elif RS.Core.Automation.STATUS_LEVEL[ RS.Core.Automation.STATUS_LEVEL_WARNING ] in job.status:
            bgcolor = '#FFFF00'
            
        elif RS.Core.Automation.STATUS_LEVEL[ RS.Core.Automation.STATUS_LEVEL_ERROR ] in job.status or job.isFailed():
            bgcolor = '#FF0000'
            fgcolor = '#C0C0C0'
            
        elif RS.Core.Automation.STATUS_LEVEL[ RS.Core.Automation.STATUS_LEVEL_COMPLETE ] in job.status:
            bgcolor = '#33FF00'
            
        html += '<tr>\n' 
        html += '<td><font size="1" face="Arial">{0}</font></td>\n'.format( job.id )
        html += '<td><font size="1" face="Arial">{0}</font></td>\n'.format( job.name )
        
        submittedBy = str( job.submittedBy ).split( '@' )[ 0 ]        
        
        html += '<td><font size="1" face="Arial">{0}</font></td>\n'.format( submittedBy )
        
        modesStr = ''
        
        for mode in job.modes:
            modesStr += '{0},'.format( mode )
            
        modesStr = modesStr.rstrip( ',' )
        
        html += '<td><font size="1" face="Arial">{0}</font></td>\n'.format( modesStr )
        html += '<td bgcolor="{1}" fgcolor="{2}"><font size="1" face="Arial">{0}</font></td>\n'.format( job.status, bgcolor, fgcolor )
        
        if sectionName == 'In Progress':
            elapsedTime = RS.Core.Automation.Server.timestampGetJobElapsedTime( job.id )
            
        elif sectionName == 'Completed Today' or sectionName == 'Failed Today':
            if job.totalTime != None:
                elapsedTime = job.totalTime
                
            else:
                elapsedTime = ''
            
        else:
            elapsedTime = ''
        
        html += '<td><font size="1" face="Arial">{0}</font></td>\n'.format( elapsedTime )
        
        html += '<td><a href="{1}"><font size="1" face="Arial">{0}</font></a></td>\n'.format( job.filename, os.path.dirname( job.filename ) )
        
        # View log
        ulogFile = '{0}CutsceneUpdate_Job{1}.ulog'.format( RS.Core.Automation.LOGS_DIR, job.id )
        
        if os.path.isfile( ulogFile ):
            html += '<td><a href="file:////{0}"><font size="1" face="Arial">View Log</font></a></td>\n'.format( ulogFile )
            
        else:
            html += '<td><font size="1" face="Arial"></font></td>\n'
        
        html += '<td><font size="1" face="Arial">{0}</font></td>\n'.format( job.machine )
        html += '</tr>\n'            
    
    return html

def updateJobStatusReport():
    jobsInQueue = RS.Core.Automation.Server.getJobsInQueue()
    jobsInProgress = RS.Core.Automation.Server.getJobsInProgress()
    completedJobs = RS.Core.Automation.Server.getCompletedJobs()
    failedJobs = RS.Core.Automation.Server.getFailedJobs()
    
    # Only show jobs from today.
    todaysCompletedJobs = []
    today = datetime.datetime.now()
    
    for job in completedJobs:
        if job.time.month == today.month and job.time.day == today.day:
            todaysCompletedJobs.append( job )
            
    completedJobs = todaysCompletedJobs
    
    # Today's failed jobs.
    todaysFailedJobs = []
    today = datetime.datetime.now()
    
    for job in failedJobs:
        if job.time.month == today.month and job.time.day == today.day:
            todaysFailedJobs.append( job )
            
    failedJobs = todaysFailedJobs        
    
    
    currentIssues = [ '- We are experiencing intermittent issues with Perforce commands failing, especially on submits.  Please contact us if your file fails to submit.' ]
    
    html = '<html>\n'
    
    html += '<title>Cutscene Update Monitor</title>\n'
    html += '<meta http-equiv="refresh" content="{0}" >\n'.format( RS.Core.Automation.JOBS_REPORT_REFRESH )
    
    html += '<body>\n'
    
    html += '<font size="5" face="Arial">Cutscene Update Monitor</font><br>\n'
    html += '<font size="1" face="Arial">Last Updated: {0}</font><br>\n'.format( datetime.datetime.now() )
    html += '<font size="1" face="Arial">Monitoring from machine: {0}</font><br><br>\n'.format( socket.gethostname() )
    
    html += '<font size="2" face="Arial">Current Issues</font><br>\n'
    
    for currentIssue in currentIssues:
        html += '<font color="#FF0000" size="1.5" face="Arial">{0}</font><br>\n'.format( currentIssue )
    
    # Tables
    html += '<br><table border="1" cellspacing="0" width="100%">\n'  
    
    html += createHtmlTable( 'In Progress', jobsInProgress )
    html += createHtmlTable( 'In Queue', jobsInQueue )
    html += createHtmlTable( 'Failed Today', failedJobs )
    html += createHtmlTable( 'Completed Today', reversed( completedJobs ) )
    
    html += '</table><br>\n'
    
    html += '</body>\n'
    html += '</html>\n'
    
    try:
        report = open( '{0}CutsceneUpdateMonitor.html'.format( RS.Core.Automation.JOBS_DIR ), 'w' )
        report.write( html )
        
    except:
        print 'Could not create monitor!'
        
    finally:
        report.close()

def watch():
    while True:
        updateJobStatusReport()
        
        sys.stdout.write( 'Watching jobs...\ \r' )
        time.sleep( 1 )
        sys.stdout.write( 'Watching jobs...| \r' )
        time.sleep( 1 )
        sys.stdout.write( 'Watching jobs.../ \r' )
        time.sleep( 1 )
        sys.stdout.write( 'Watching jobs...- \r' )
        time.sleep( 1 )
    
def startServer():
    watch()
    
    
if __name__ == '__main__':
    startServer()
    