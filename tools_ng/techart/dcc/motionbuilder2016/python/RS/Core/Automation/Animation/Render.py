"""
Module that contains code for rendering anim files in motion builder
"""
import os
import re
import time
import shutil
import subprocess

import pyfbsdk as mobu

from RS import ProjectData, Config, Perforce
from RS.Core import DatabaseConnection
from RS.Utils.Scene import Constraint
from RS.Utils.Logging import Universal
from RS.Core.Animation.Anim2Fbx import Load
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Tools.CameraToolBox.PyCore.Decorators.memorize import memoized


APPLICATION = mobu.FBApplication()
SYSTEM = mobu.FBSystem()
LOG = Universal.UniversalLog("Motionbuilder_EST", False, True)

FFMPEG_PATH = os.path.join(Config.Tool.Path.Bin, "video", "ffmpeg.exe")
DEFAULT_RIG = os.path.join(Config.Tool.Path.TechArt, "etc", "Reference", "MALE_REF_ANIM_01.fbx")
INVALID_REFERENCES = ["REFERENCE", "RS_Null", "MALE_REF_POSE_01", "FEMALE_REF_POSE_01"]
ANIMATION_DATABASE = DatabaseConnection.DatabaseConnection('{SQL Server}', 'NYCW-MHB-SQL', 'AnimationStats_RDR3', LOG)


def ValidateDirectory(path):
    """
    Validates that the directory exists and creates missing directories if possible

    Arguments:
        path (string): directory to validate

    Return:
        boolean
    """
    if not os.path.exists(path):
        ValidateDirectory(os.path.dirname(path))
        try:
            os.mkdir(path)

        except Exception, error:
            print error
            return False
    return bool(path)


@memoized
def GetFbxFromSkeletonPath(path):
    """
    Gets an FBX File that a skeleton file works on

    Arguments:
        path (string): path to tothe skeleton file

    Returns:
        string, path to the fbx file
    """
    results = ANIMATION_DATABASE.ExecuteQuery("SELECT FbxPath FROM Skeletons WHERE Path = '{}'".format(path))
    if results:
        return results[0][0]


def OpenCleanScene(skeletonPath=None, isFemale=False):
    """  Opens a clean scene with the appropriate character based on the skeleton file provided """

    path = GetFbxFromSkeletonPath(skeletonPath)
    manager = Manager()
    manager.SetSilent(True)

    reference = True
    referencePaths = [reference.Path for reference in manager.GetReferenceListAll()]

    if (not path or os.path.basename(skeletonPath) in ("biped.skel", "player.skel")) and isFemale:
        path = os.path.join(Config.Tool.Path.TechArt, "etc", "Reference", "FEM_REF_ANIM_01.fbx")
        reference = False

    if not reference and path.lower() != APPLICATION.FBXFileName.lower():
        APPLICATION.FileOpen(path)

    elif reference and path not in referencePaths:
        # New Scene
        APPLICATION.FileNew()
        Perforce.Sync(path, force=True)
        basename = os.path.basename(os.path.splitext(path)[0])
        if not os.path.exists(path) and basename.endswith("01A"):
            path = path.replace(basename, basename[:-1])
            Perforce.Sync(path, force=True)

        # Reference Character
        manager.CreateReferences(path)

        # Make sure only models are visible

        # Turn off the visibility of all the bones
        [setattr(component, "Show", False) for component in mobu.FBSystem().Scene.Components
         if isinstance(component,  mobu.FBModelSkeleton)]

        mobu.FBSystem().Scene.Evaluate()
    else:
        # Motion Builder doesn't like looping through components in its scene and deleting its contents
        # it causes it to exit the loop early. So we convert it to a normal python list to work around this bug.
        [take.FBDelete() for take in list(mobu.FBSystem().Scene.Takes)]

    return True


def PositionRenderCamera(namespace):
    """
    Creates/Positions a to render with

    Arguments:
        namespace (string): namespace of the character to position the camera for

    """
    matrix = mobu.FBMatrix()

    mover = mobu.FBFindModelByLabelName("{}:mover".format(namespace))
    skelRoot = mobu.FBFindModelByLabelName("{}:SKEL_ROOT".format(namespace)) or mover
    skelRoot.GetMatrix(matrix)

    rotationMatrix = [
        -100.000000, -0.000000, 0.000000, 0.000000,
        0.000000, -0.000000, 100.000000, 0.000000,
        -0.000000, 100.000000, 0.000000, 0.000000]

    finalMatrix = mobu.FBMatrix(rotationMatrix + [matrix[index + 12] for index in xrange(4)])

    # find/create null to rotate camera with
    CameraBufferNull = mobu.FBFindModelByLabelName("CameraBuffer")
    CameraPivotNull = mobu.FBFindModelByLabelName("CameraPivot")

    mover = mobu.FBFindModelByLabelName("{}:mover".format(namespace))

    if not CameraBufferNull:
        CameraBufferNull = mobu.FBModelNull("CameraBuffer")

        constraint = Constraint.CreateConstraint(Constraint.POSITION)
        constraint.ReferenceAdd(0, CameraBufferNull)
        constraint.ReferenceAdd(1, mover)
        constraint.Active = True

    if not CameraPivotNull:
        CameraPivotNull = mobu.FBModelNull("CameraPivot")
        CameraPivotNull.Parent = CameraBufferNull
        CameraPivotNull.PropertyList.Find("Lcl Translation").Data = mobu.FBVector3d(0, 0, 0)

    # position the null at the root of the character
    CameraBufferNull.SetMatrix(finalMatrix)
    mobu.FBSystem().Scene.Evaluate()
    CameraPivotNull.PropertyList.Find("Lcl Rotation").Data = mover.PropertyList.Find("Lcl Rotation").Data + mobu.FBVector3d(0, 0, 180)
    # find/create camera
    RenderCamera = None
    for camera in mobu.FBSystem().Scene.Cameras:
        if camera.Name == "RenderCamera":
            RenderCamera = camera
            break

    if RenderCamera is None:
        RenderCamera = mobu.FBCamera("RenderCamera")

    # position camera
    RenderCamera.Parent = CameraPivotNull
    RenderCamera.PropertyList.Find("ShowGrid").Data = False
    RenderCamera.PropertyList.Find("Lcl Translation").Data = mobu.FBVector3d(2.61, 2.61, 0.97)
    RenderCamera.PropertyList.Find("Lcl Rotation").Data = mobu.FBVector3d(90, 15.80, -135)

    mobu.FBSystem().Scene.Renderer.CurrentCamera = RenderCamera
    mobu.FBCameraSwitcher.CurrentCamera = RenderCamera

    mobu.FBSystem().Scene.Evaluate()


# Move this method to a more generic place
def Render(path, timespan=None,
           viewingMode=mobu.FBVideoRenderViewingMode.FBViewingModeModelsOnly,
           cameraResolution=mobu.FBCameraResolutionMode.kFBResolution640x480,
           fieldMode=mobu.FBVideoRenderFieldMode.FBFieldModeNoField,
           stillImageCompression=100,
           showCameraLabel=False, antiAliasing=True, renderAudio=False, showSafeArea=False,
           ):
    """
    Render time range

    Arguments:
        path (string): path to save file out too
        timespan (pyfbsdk.FBTimeSpan): frame range to capture
        viewingMode (pyfbsdk.FBVideoRenderViewingMode):
        cameraResolution (pyfbsdk.FBCameraResolutionMode): camera resolution
        fieldMode (pyfbsdk.FBVideoRenderFieldMode): the field of view for the camera
        stillImageCompression (int): image compression for the video, 100 is no compression and 0 is full compression
        showCameraLabel (boolean): should the camera label be shown
        antiAliasing (boolean): turn on antiAliasing
        renderAudio (boolean): include audio in the render
        showSafeArea (boolean): show the camera safe area in the video
    """

    codec = mobu.FBVideoCodecManager()
    codec.VideoCodecMode = mobu.FBVideoCodecMode.FBVideoCodecStored

    options = mobu.FBVideoGrabber().GetOptions()
    options.OutputFileName = str(path)
    options.TimeSpan = timespan or mobu.FBSystem().CurrentTake.LocalTimeSpan
    options.TimeSteps = mobu.FBTime(0, 0, 0, 1)
    options.ViewingMode = viewingMode
    options.CameraResolution = cameraResolution
    options.ShowCameraLabel = showCameraLabel
    options.AntiAliasing = antiAliasing
    options.RenderAudio = renderAudio
    options.ShowSafeArea = showSafeArea
    options.FieldMode = fieldMode
    options.StillImageCompression = stillImageCompression

    APPLICATION.FileRender(options)

    existCount = 0
    renderCount = 0
    path = path.replace("####", "0001")

    while renderCount <= 3:
        if existCount > 3:
            break

        if not os.path.exists(path):
            time.sleep(1)
            existCount += 1

        if not os.path.exists(path) or os.stat(path).st_size < 1:
            APPLICATION.FileRender(options)

            renderCount += 1
            time.sleep(1)
            continue

        return True
    return False


def ConvertImageSequenceToVideo(imagePath, videoPath):
    """
    Converts an image sequence to a video

    Arguments:
        imagePath (string): path to the image sequence
        videoPath (string): path to save video to

    """
    ffmpegCommand = '"{ffmpeg_path}" -r 30 -f image2 -s 640x480 -i {imagePath} -vcodec libx264' \
                    ' -crf 25 -loglevel quiet -y "{videoPath}"'.format(
        ffmpeg_path=FFMPEG_PATH, imagePath=imagePath, videoPath=videoPath)

    convert = 3
    while convert:
        try:
            try:
                subprocess.check_call(ffmpegCommand)
            except WindowsError:
                results = subprocess.check_call(ffmpegCommand, shell=True, stdin=subprocess.PIPE,
                                                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                results.stdin.close()
                results.stderr.close()
            break

        except subprocess.CalledProcessError:
                convert -= 1
                time.sleep(2)


def CaptureAnimation(path, animPaths, rootPath="", skeletonPath=""):
    """
    Loads the given animations into the available REF_POSE Rig in the scene and renders a video for each
    animation.

    Arguments:
        path (string): location to save renders at
        animPaths (list[string, etc.]): list of paths to anim files that should be rendered out
        rootPath (string): common root path for the anim paths
        skeletonPath (string): skeleton to use for the anim files

    """
    if not skeletonPath:
        skeletonDirectory = ProjectData.data.GetSkelFilesDirectory()
        skeletonPath = os.path.join(skeletonDirectory, "human", "biped.skel")
        if not os.path.exists(skeletonPath):
            skeletonPath = os.path.join(skeletonDirectory,  "human", "player.skel")

    # Open Scene
    if isinstance(animPaths, basestring):
        animPaths = [animPaths]

    # Load Animation
    animFolders, animFiles = Load.GetValidAnimPaths(animPaths)

    renderPaths = []
    for animPath in animFolders + animFiles:
        animPath = animPath.strip()
        isAnim = animPath.lower().endswith(".anim")
        folder = os.path.dirname(animPath)

        splitPath = folder[len(rootPath):]
        renderDirectory = os.path.join(path, splitPath[1:])

        if isAnim:
            folder = os.path.dirname(animPath)

        if not ValidateDirectory(renderDirectory):
            continue

        # Sometimes files may be passed that don't exist, so we skip them

        if not os.path.exists(animPath):
            basename = os.path.splitext(os.path.basename(animPath))[0]
            renderPath = os.path.join(renderDirectory, "{}.mov".format(basename))
            renderPaths.append((renderPath, False))
            continue

        # Clear Takes / Open appropriate rig to plot

        isFemale = "female" in animPath.lower()
        OpenCleanScene(skeletonPath, isFemale=isFemale)
        namespace = [namespace.Name for namespace in SYSTEM.Scene.Namespaces
                     if namespace.Name not in INVALID_REFERENCES and not namespace.Name.endswith("_Ctrl")][:1]

        # We are exploiting a bug where python doesn't destroy pointers from loops, so the last value of the the
        # character variable still exists

        if not isAnim:
            Load.LoadFolder(folder, namespaces=namespace, skeletonPath=skeletonPath)
        else:
            Load.LoadFile(animPath, namespaces=namespace, skeletonPath=skeletonPath)

        PositionRenderCamera(namespace[0])

        # Render
        for take in SYSTEM.Scene.Takes:
            renderPath = os.path.join(renderDirectory, "{}.mov".format(take.Name))
            results = RenderTake(take, renderPath, extendedPath=splitPath[1:])
            if results:
                renderPaths.append(results)

    return renderPaths


def RenderTake(take, path, extendedPath=None, includeThumbnail=True):
    """
    Renders the take into a video

    Arguments:
        take (pyfbsdk.FBTake): take to render out into a video
        path (string): path to render out video to
        extendedPath (string): path extension so that the images are rendered within subfolder of the temp folder
                                generated by this command.
        includeThumbnail (boolean): should a thumbnail of the middle most frame be saved out

    Return: tuple, path where the video was rendered out to and if the render was successful
    """
    if re.search("Take [0-9]+", take.Name, re.I):
        return []

    temporaryDirectory = r"x:\_temp"
    if extendedPath:
        temporaryDirectory = os.path.join(r"x:\_temp", extendedPath)

    SYSTEM.CurrentTake = take

    # We go forward in time by one frame to force the components in the scene to evaluate
    currentTime = mobu.FBSystem().LocalTime
    currentFrame = int(currentTime.GetFrame())

    startFrame = take.LocalTimeSpan.GetStart().GetFrame()
    endFrame = take.LocalTimeSpan.GetStop().GetFrame()
    midFrame = (endFrame - startFrame)/2
    midFrame = midFrame if midFrame < endFrame else endFrame - 1

    nextTime = mobu.FBTime(0, 0, 0, currentFrame + 1)
    mobu.FBPlayerControl().Goto(nextTime)
    mobu.FBSystem().Scene.Evaluate()


    # Create directory to render images too

    ValidateDirectory(temporaryDirectory)

    imagesPath = os.path.join(temporaryDirectory, "{}_####.jpg".format(take.Name))
    result = Render(imagesPath)

    imagesPath = imagesPath.replace("####", "%04d")

    ConvertImageSequenceToVideo(imagesPath, path)

    if includeThumbnail and os.path.exists(imagesPath % midFrame):
        thumbnailPath = path.replace(".mov", ".jpg")
        shutil.copyfile(imagesPath % midFrame, thumbnailPath)

        ffmpegCommand = '"{ffmpeg_path}" -i {imagePath} -vf scale=100:-1 "{previewPath}"'.format(
            ffmpeg_path=FFMPEG_PATH, imagePath=thumbnailPath, previewPath=thumbnailPath.replace(".jpg", "_preview.jpg"))

        try:
            subprocess.check_call(ffmpegCommand)
        except WindowsError:
            results = subprocess.check_call(ffmpegCommand, shell=True, stdin=subprocess.PIPE,
                                            stderr=subprocess.PIPE, stdout=subprocess.PIPE)
            results.stdin.close()
            results.stderr.close()

    shutil.rmtree(r"x:\_temp")

    return path, result
