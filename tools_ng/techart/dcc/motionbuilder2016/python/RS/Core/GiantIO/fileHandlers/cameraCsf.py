"""
Class to handle the reading and writing of camera CSF file formats
"""

from RS.Core.GiantIO.fileHandlers import giantBase


class CameraData(object):
    """
    Class to represent a camera in giant, its position, rotation, and other details
    """
    def __init__(self, stubb):
        """
        Constructor

        args:
            stubb (GiantStubb): The stubb to wrap
        """
        super(CameraData, self).__init__()
        self._stubb = stubb

    def getId(self):
        """
        Get the ID of the camera

        return:
            int of the camera's ID
        """
        return self._stubb.getByStubbName("ID").value

    def getPosition(self):
        """
        Get the Position of the camera

        return:
            tuple of floats, X axis, Y axis, Z axis
        """
        _, xCoords, yCoords, zCoords = self._stubb.getByStubbName("POSITION").value.split(" ", 3)
        return (float(xCoords), float(yCoords), float(zCoords))

    def getRotation(self):
        """
        Get the Rotation of the camera

        return:
            tuple of floats, X axis, Y axis, Z axis
        """
        _, xRots, yRots, zRots, _ = self._stubb.getByStubbName("ROTATION").value.split(" ", 4)
        return (float(xRots), float(yRots), float(zRots))

    def getAspectRatio(self):
        """
        Get the aspect ratio of the camera

        return:
            float of the aspect ratio
        """
        return self._stubb.getByStubbName("ASPECT_RATIO").value

    def getFovX(self):
        """
        Get the X axis of the of the camera's FOV

        return:
            float FOV X axis
        """
        return self._stubb.getByStubbName("FOV_X").value

    def getFovY(self):
        """
        Get the Y axis of the of the camera's FOV

        return:
            float FOV Y axis
        """
        return self._stubb.getByStubbName("FOV_Y").value

    def getRotationOrder(self):
        """
        Get the rotation axis order of the camera. e.g. "xyz", "xzy"

        return:
            string of the rotation axis order
        """
        return self._stubb.getByStubbName("ROTATION_ORDER").value

    def getRotationMatrix(self):
        """
        Get the rotation matrix for the camera.

        return:
            List of tuples with 3 floats, to make a matrix
        """
        colParts = [col.split(" ") for col in self._stubb.getByStubbName("ROTATION_MATRIX").value]
        wholeMat = []
        for col in colParts:
            wholeMat.append([float(val) for val in col])
        return wholeMat


class CameraCsf(giantBase.GiantFileBase):
    """
    Class to load in a Giant CSF file so its easy to read
    """
    def __init__(self, filePath=None):
        """
        Constructor

        kwargs:
            filePath (str): File path to the file to read in
        """
        super(CameraCsf, self).__init__(filePath=filePath)

    def getCameras(self):
        """
        Get all the cameras in the file.

        return:
            list of the cameras as CameraData
        """
        return [CameraData(cam) for cam in self.getAllByStubbName("CAMERA")]


