"""Exceptions for the giantIO subpackage."""


class GiantEnvironmentError(ValueError):
    """Raised when there is an error with the Giant environment variables.

    Inherits from ValueError to maintain API compatibility.
    """


class GiantEnvironmentNotSetError(GiantEnvironmentError):
    """Raised when the required Giant environment variables are not set."""


class MissingStubbs(Exception):
    """Raised when trying to access a file stubb that does not exist.

    Args:
        missingStubbs (list of str): A list of the missing stubbs.
        fileExtension (str): The file extension, e.g. "prj", "rtparams", "cal"

        filePath (str, optional): The file with the missing stubb.
    """

    def __init__(self, missingStubbs, fileExtension, filePath=None):
        self.missingStubbs = missingStubbs
        self.fileExtension = fileExtension
        self.filePath = filePath
        if self.filePath:
            message = "Missing stubb(s) in {} file: {} for {}".format(
                fileExtension, ", ".join(missingStubbs), self.filePath)
        else:
            message = "Missing stubb(s) in {} file: {}".format(fileExtension, ", ".join(missingStubbs))
        super(MissingStubbs, self).__init__(message)


class MissingGiantFileError(IOError):
    """Raised when a Giant based file is missing.

    Args:
        filePath (str): The filepath.
    """

    def __init__(self, filePath):
        self.filePath = filePath
        message = "Giant file not found: {}".format(filePath)
        super(MissingGiantFileError, self).__init__(message)
