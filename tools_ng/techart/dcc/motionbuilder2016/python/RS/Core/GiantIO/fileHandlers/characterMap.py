"""
Class to handle the reading and writing of camera CM file formats

This is still WIP
"""

from RS.Core.GiantIO.fileHandlers import giantBase


class VariableTypes(object):
    Scalar = "SCALAR"


class VariableStubb(object):
    def __init__(self, stubb):
        super(VariableStubb, self).__init__()

        self._name = ""
        self._value = 0.0
        self._stubb = stubb

        self._loadData()

    def getFileName(self):
        return self._fileName

    def setFileName(self, value):
        self._fileName = value

    def getBodyIdOffset(self):
        return self._bodyIdOffset

    def setBodyIdOffset(self, value):
        self._bodyIdOffset = value

    def getBodyNameSuffix(self):
        return self._bodyNameSuffix

    def setBodyNameSuffix(self, value):
        self._bodyNameSuffix = value

    def _loadData(self):
        self._fileName = self._stubb.getByStubbName("FILE_NAME").value
        self._bodyIdOffset = self._stubb.getByStubbName("BODY_ID_OFFSET").value
        self._bodyNameSuffix = self._stubb.getByStubbName("BODY_NAME_SUFFIX").value

    def writeToStubb(self):
        self._stubb.getByStubbName("FILE_NAME").value = self._fileName
        self._stubb.getByStubbName("BODY_ID_OFFSET").value = str(self._bodyIdOffset)
        self._stubb.getByStubbName("BODY_NAME_SUFFIX").value = self._bodyNameSuffix

    def getStubb(self):
        return self._stubb



class VariableGroupStubb(object):
    def __init__(self, stubb):
        super(VariableGroupStubb, self).__init__()

        self._description = ""
        self._vars = []
        self._childGroups = []

        self._stubb = stubb

        self._loadData()

    def getDescription(self):
        return self._description

    def setDescription(self, value):
        self._description = value

    def _loadData(self):
        self._description = str(self._stubb.getByStubbName("DESCRIPTION").value)

    def writeToStubb(self):
        self._stubb.getByStubbName("DESCRIPTION").value = "\"%s\"" % (self._description)

    def getStubb(self):
        return self._stubb


class CharacterMap(giantBase.GiantFileBase):
    """
    Class to load and write a Giant CM (character map) file so its easy to read
    """
    def __init__(self, filePath=None):
        super(CharacterMap, self).__init__(filePath=filePath)
        self._fileType = "Bio Character Map File"
        self._fileTypeVersion = "v1.00"
        if filePath is None:
            self._createStruct()

        self._includedModesl = [IncludedModelStubb(modle) for modle in self.getAllByStubbName("INCLUDE")]

    def _createStruct(self):
        newStubb = self._addStubb("UNITS")

        newStubb.addNewChild("LENGTH", value="1 INCHES")
        newStubb.addNewChild("ANGLE", value="1 RADIANS")

    def getIncludedBodies(self):
        return self._includedModesl

    def addIncludedBody(self):
        newStubb = self._addStubb("INCLUDE")

        newStubb.addNewChild("FILE_NAME", value="")
        newStubb.addNewChild("BODY_ID_OFFSET", value="")
        newStubb.addNewChild("BODY_NAME_SUFFIX", value="")

        newBody = IncludedModelStubb(newStubb)
        self._includedModesl.append(newBody)
        return newBody

    def writeFile(self, filePath=None):
        for model in self.getIncludedBodies():
            model.writeToStubb()
        super(ModelMdl, self).writeFile(filePath=filePath)
