"""
Contains constants used in the library.
"""
import os


class Resources(object):
    """
    Constants for the various resource directories.
    """
    ROOT = os.path.dirname(__file__)
    TESTS = os.path.join(ROOT, "resources", "tests")


class MetaDataKeys(object):
    """
    Constants for the various custom metadata keys in our Giant files.

    This could be a stub name or the key in a metadata line (.cal files).
    """
    CONTEXT = "_CONTEXT"
