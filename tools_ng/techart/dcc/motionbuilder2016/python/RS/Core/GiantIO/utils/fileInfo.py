"""
Convenience functions for extracting and syncing internal file paths.

Tests are in giantIO/tests.
"""
import os
import re

from RS.Core.GiantIO.fileHandlers import project, animationAnm, giantBase
from RS.Core.GiantIO.utils import giantPaths
from RS import Perforce


class MissingFile(Exception):
    pass


class _SyncManager(object):
    _cachedData = {}

    @classmethod
    def clearCache(cls):
        cls._cachedData = {}

    @classmethod
    def syncFile(cls, filePath, force=False, onDiskCheck=True):
        Perforce.Sync([filePath])
        fileState = Perforce.GetFileState([filePath])
        if not fileState.exists:
            raise MissingFile("File '{0}' does not exist on Perforce".format(filePath))
        if onDiskCheck is True:
            if os.path.isfile(fileState.clientFilename) is False:
                Perforce.Sync([filePath], force=True)
        return fileState

    @classmethod
    def syncFolder(cls, folderPath):
        Perforce.Sync([folderPath])


def projToDict(prjFilePath, substituteDict=None):
    substituteDict = substituteDict or {}
    if not os.path.isfile(prjFilePath):
        raise ValueError("File '{0}' does not exist on the Local disk or Perforce".format(prjFilePath))
    prj = project.GiantPrj(prjFilePath)
    returnDict = {}
    for key in prj.getKeys():
        value = prj.getValue(key)
        if isinstance(value, str):
            for oldSub, newSub in substituteDict.iteritems():
                value = value.replace(oldSub, newSub)
        returnDict[key] = value

    return returnDict


def fileReadLogic(inputPath, depotRootBasename):
    """
    Read file logic to work out what method to use to read a file, or not

    Args:
        inputPath (str): The input depot path.
        depotRootBasename (str): The basename of the project's mocap depot root.

    Returns:
        set of str: The file's dependency list.
    """
    returnList = set()
    if inputPath.lower().endswith(".anm"):
        returnList.update(getFileListFromAnm(inputPath, depotRootBasename))
    elif inputPath.lower().endswith(".pdf"):
        returnList.update(getFileListFromPdf(inputPath, depotRootBasename))
    elif inputPath.lower().endswith(".mdl"):
        returnList.update(getFileListFromMdl(inputPath, depotRootBasename))
    elif inputPath.lower().endswith(".dof"):
        returnList.update(getFileListFromDof(inputPath, depotRootBasename))
    elif inputPath.lower().endswith(".lmt"):
        returnList.update(getFileListFromLmt(inputPath, depotRootBasename))
    elif inputPath.lower().endswith(".scl"):
        returnList.update(getFileListFromScl(inputPath, depotRootBasename))
    elif inputPath.lower().endswith(".bdf"):
        returnList.update(getFileListFromBdf(inputPath, depotRootBasename))
    elif inputPath.lower().endswith(".psf"):
        returnList.update(getFileListFromPsf(inputPath, depotRootBasename))
    elif inputPath.lower().endswith(".prj"):
        returnList.update(getFileListFromPrj(inputPath, depotRootBasename))
    else:
        returnList.add(inputPath)
    return returnList


def getFileListFromPdf(pdfFilePath, depotRootBasename):
    """
    Get all the files from a nested pdf

    args:
        pdfFilePath (str): The local path to the pdf file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the pdf
    """
    # Make depot path local if needed
    if pdfFilePath.startswith("//depot/projects/{0}".format(depotRootBasename)) or not os.path.isfile(pdfFilePath):
        fileState = _SyncManager.syncFile(pdfFilePath)
        pdfFilePath = fileState.clientFilename
        if not os.path.isfile(pdfFilePath):
            raise MissingFile("File '{0}' does not exist on the Local disk or Perforce".format(pdfFilePath))

    pdf = giantBase.GiantFileBase(pdfFilePath)
    returnList = {pdfFilePath}
    substituteDict = giantPaths.generateSubstituteDict(depotRootBasename)
    for stubb in pdf.getStubbs():
        name = stubb.stubbName

        if name.lower() == "include":
            value = stubb.getByStubbName("FILE_NAME").value
            if value is None:
                continue
            for oldSub, newSub in substituteDict.iteritems():
                value = value.replace(oldSub, newSub)
            returnList.update(fileReadLogic(value, depotRootBasename))
    return list(returnList)


def getFileListFromPrj(prjFilePath, depotRootBasename):
    """
    Get all the files from a nested prj

    args:
        prjFilePath (str): The local path to the prj file to parse
        depotRootBasename (str): The basename of the project's mocap depot root, e.g. "development".

    return:
        string list of depot paths that are in the prj
    """
    # Make depot path local if needed
    if prjFilePath.startswith("//depot/projects/{0}".format(depotRootBasename)) or not os.path.isfile(prjFilePath):
        fileState = _SyncManager.syncFile(prjFilePath)
        prjFilePath = fileState.clientFilename
        if not os.path.isfile(prjFilePath):
            raise MissingFile("File '{0}' does not exist on the Local disk or Perforce".format(prjFilePath))

    # Sync the whole prj folder
    baseName, _ = os.path.splitext(prjFilePath)
    prjFolderName = "{0}/...".format(baseName)
    _SyncManager.syncFolder(prjFolderName)

    prj = project.GiantPrj(prjFilePath)
    returnList = set()
    substituteDict = giantPaths.generateSubstituteDict(depotRootBasename)

    stubbs = prj.getStubbs()
    namedStubbs = {stubb.stubbName: stubb for stubb in stubbs}
    for stubb in stubbs:
        if stubb.stubbName in ["PROBLEM_SPEC", "PROBLEM_DEF", "CALIBRATION",
                               "SCALE_DEF", "UNSCALED_BODY_DEF", "RAW_DATA",
                               "GLOB_IDS", "BODY_DEF", "LIMITS", "SEARCH_PARAMS",
                               "GFX_MODEL", "GFX_SCALING", "MOTION", "PATTERN"]:
            value = stubb.value
            if value is None:
                continue
            for oldSub, newSub in substituteDict.iteritems():
                value = value.replace(oldSub, newSub)
            returnList.add(value)
            returnList.update(fileReadLogic(value, depotRootBasename))

    # Workaround for some ROM .prj files missing their PATTERN stub.
    # SCALE .prj files will not have a PATTERN stub (.ppf); the common practice of shooting a ROM right after a SCALE
    # means that there is usually subject folder that matches the naming for both the SCALE and ROM. It also results in
    # the .ppf from the ROM being added to that subject folder alongside the .bdf.
    if namedStubbs.get("PATTERN") is None and namedStubbs.get("BODY_DEF") is not None:
        rawBdfPath = namedStubbs.get("BODY_DEF").value
        for oldSub, newSub in substituteDict.iteritems():
            rawBdfPath = rawBdfPath.replace(oldSub, newSub)
        ppfPath = re.sub(".bdf$", ".ppf", rawBdfPath)
        fileState = Perforce.GetFileState(ppfPath)
        if fileState.exists:
            returnList.add(ppfPath)

    return list(returnList)


def getFileListFromPsf(psfFilePath, depotRootBasename):
    """
    Get all the files from a nested psf

    args:
        psfFilePath (str): The local path to the psf file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the psf
    """
    # Make depot path local if needed
    if psfFilePath.startswith("//depot/projects/{0}".format(depotRootBasename)) or not os.path.isfile(psfFilePath):
        fileState = _SyncManager.syncFile(psfFilePath)
        psfFilePath = fileState.clientFilename
        if not os.path.isfile(psfFilePath):
            raise MissingFile("File '{0}' does not exist on the Local disk or Perforce".format(psfFilePath))

    psf = giantBase.GiantFileBase(psfFilePath)
    returnList = set()
    substituteDict = giantPaths.generateSubstituteDict(depotRootBasename)
    for stubb in psf.getStubbs():
        name = stubb.stubbName

        if name.lower() == "subject":

            if stubb.getByStubbName("ENABLED").value == 0:
                continue

            for stubbName in ["PROJECT", "CHARMAP", "TARGET_ANIM", "PATTERN"]:
                subStubb = stubb.getByStubbName(stubbName)
                if subStubb is None:
                    continue
                value = subStubb.value
                if value is None:
                    continue
                for oldSub, newSub in substituteDict.iteritems():
                    value = value.replace(oldSub, newSub)
                returnList.update(fileReadLogic(value, depotRootBasename))
    return list(returnList)


def getFileListFromBdf(bdfFilePath, depotRootBasename):
    """
    Get all the files from a nested bdf

    args:
        bdfFilePath (str): The local path to the bdf file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the bdf
    """
    # Make depot path local if needed
    if bdfFilePath.startswith("//depot/projects/{0}".format(depotRootBasename)) or not os.path.isfile(bdfFilePath):
        fileState = _SyncManager.syncFile(bdfFilePath)
        try:
            bdfFilePath = fileState.clientFilename
        except AttributeError:
            pass
        if not os.path.isfile(bdfFilePath):
            raise MissingFile("File '{0}' does not exist on the Local disk or Perforce".format(bdfFilePath))

    bdf = giantBase.GiantFileBase(bdfFilePath)
    returnList = {bdfFilePath}
    substituteDict = giantPaths.generateSubstituteDict(depotRootBasename)
    for stubb in bdf.getStubbs():
        name = stubb.stubbName

        if name.lower() == "include":
            value = stubb.getByStubbName("FILE_NAME").value
            if value is None:
                continue
            for oldSub, newSub in substituteDict.iteritems():
                value = value.replace(oldSub, newSub)
            returnList.update(getSupportingFilesForSubject(value, depotRootBasename))
            returnList.update(fileReadLogic(value, depotRootBasename))
    return list(returnList)


def getFileListFromScl(sclFilePath, depotRootBasename):
    """
    Get all the files from a nested scl

    args:
        sclFilePath (str): The local path to the scl file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the scl
    """
    # Make depot path local if needed
    if sclFilePath.startswith("//depot/projects/{0}".format(depotRootBasename)) or not os.path.isfile(sclFilePath):
        fileState = _SyncManager.syncFile(sclFilePath)
        sclFilePath = fileState.clientFilename
        if not os.path.isfile(sclFilePath):
            raise MissingFile("File '{0}' does not exist on the Local disk or Perforce".format(sclFilePath))

    scl = giantBase.GiantFileBase(sclFilePath)
    returnList = {sclFilePath}
    substituteDict = giantPaths.generateSubstituteDict(depotRootBasename)
    for stubb in scl.getStubbs():
        name = stubb.stubbName

        if name.lower() == "include":
            value = stubb.getByStubbName("FILE_NAME").value
            if value is None:
                continue
            for oldSub, newSub in substituteDict.iteritems():
                value = value.replace(oldSub, newSub)
            returnList.update(fileReadLogic(value, depotRootBasename))
    return list(returnList)


def getSupportingFilesForSubject(filePath, depotRootBasename):
    """
    Get all the supporting files (ppf, bdf) for the subject

    args:
        filePath (str): The path to the file to get the subject files for
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are supporting it
    """
    depotPath = giantPaths.generateSubstituteDict(depotRootBasename)["$BIO_DATA_DIR"]
    returnList = set()
    # Get the PPF and BDF files!
    subjectName = os.path.basename(filePath).split(".", 1)[0]
    subDir = "/".join([depotPath, "subject", subjectName])
    for ext in [".bdf", ".ppf"]:
        subPath = "{}/{}{}".format(subDir, subjectName, ext)
        Perforce.Sync([subPath])
        fileState = Perforce.GetFileState([subPath])
        if fileState.exists:
            localPath = fileState.clientFilename
            returnList.add(localPath)
            if not os.path.exists(localPath):
                Perforce.Sync([subPath], force=True)
    return list(returnList)


def getFileListFromLmt(lmtFilePath, depotRootBasename):
    """
    Get all the files from a nested lmt

    args:
        lmtFilePath (str): The local path to the lmt file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the lmt
    """
    # Make depot path local if needed
    if lmtFilePath.startswith("//depot/projects/{0}".format(depotRootBasename)) or not os.path.isfile(lmtFilePath):
        fileState = _SyncManager.syncFile(lmtFilePath)
        lmtFilePath = fileState.clientFilename
        if not os.path.isfile(lmtFilePath):
            raise MissingFile("File '{0}' does not exist on the Local disk or Perforce".format(lmtFilePath))

    lmt = giantBase.GiantFileBase(lmtFilePath)
    returnList = {lmtFilePath}
    substituteDict = giantPaths.generateSubstituteDict(depotRootBasename)
    for stubb in lmt.getStubbs():
        name = stubb.stubbName

        if name.lower() == "include":
            value = stubb.getByStubbName("FILE_NAME").value
            if value is None:
                continue
            for oldSub, newSub in substituteDict.iteritems():
                value = value.replace(oldSub, newSub)
            returnList.update(fileReadLogic(value, depotRootBasename))
    return list(returnList)


def getFileListFromDof(dofFilePath, depotRootBasename):
    """
    Get all the files from a nested dof

    args:
        dofFilePath (str): The local path to the dof file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the dof
    """
    # Make depot path local if needed
    if dofFilePath.startswith("//depot/projects/{0}".format(depotRootBasename)) or not os.path.isfile(dofFilePath):
        fileState = _SyncManager.syncFile(dofFilePath)
        dofFilePath = fileState.clientFilename
        if not os.path.isfile(dofFilePath):
            raise MissingFile("File '{0}' does not exist on the Local disk or Perforce".format(dofFilePath))

    dof = giantBase.GiantFileBase(dofFilePath)
    returnList = {dofFilePath}
    substituteDict = giantPaths.generateSubstituteDict(depotRootBasename)

    for stubb in dof.getStubbs():
        name = stubb.stubbName

        if name.lower() == "include":
            value = stubb.getByStubbName("FILE_NAME").value
            if value is None:
                continue
            for oldSub, newSub in substituteDict.iteritems():
                value = value.replace(oldSub, newSub)
            returnList.update(fileReadLogic(value, depotRootBasename))
    return list(returnList)


def getFileListFromMdl(mdlFilePath, depotRootBasename):
    """
    Get all the files from a nested mdl

    args:
        mdlFilePath (str): The local path to the mdl file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the mdl
    """
    # Make depot path local if needed
    if mdlFilePath.startswith("//depot/projects/{0}".format(depotRootBasename)) or not os.path.isfile(mdlFilePath):
        fileState = _SyncManager.syncFile(mdlFilePath)
        mdlFilePath = fileState.clientFilename
        if not os.path.isfile(mdlFilePath):
            raise MissingFile("File '{0}' does not exist on the Local disk or Perforce".format(mdlFilePath))

    mdl = giantBase.GiantFileBase(mdlFilePath)
    returnList = {mdlFilePath}
    substituteDict = giantPaths.generateSubstituteDict(depotRootBasename)

    for stubb in mdl.getStubbs():
        name = stubb.stubbName

        if name.lower() == "include":
            value = stubb.getByStubbName("FILE_NAME").value
            if value is None:
                continue
            for oldSub, newSub in substituteDict.iteritems():
                value = value.replace(oldSub, newSub)
            returnList.update(fileReadLogic(value, depotRootBasename))
    return list(returnList)


def getFileListFromAnm(anmFilePath, depotRootBasename):
    """
    Get all the files from a nested anm

    args:
        anmFilePath (str): The local path to the anm file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the anm
    """
    # Make depot path local if needed
    if anmFilePath.startswith("//depot/projects/{0}".format(depotRootBasename)) or not os.path.isfile(anmFilePath):
        fileState = _SyncManager.syncFile(anmFilePath)
        anmFilePath = fileState.clientFilename
        if not os.path.isfile(anmFilePath):
            raise MissingFile("File '{0}' does not exist on the Local disk or Perforce".format(anmFilePath))

    anm = animationAnm.AnimationAnm(anmFilePath)

    returnList = {anmFilePath}
    substituteDict = giantPaths.generateSubstituteDict(depotRootBasename)

    for stubb in anm.getStubbs():
        name = stubb.stubbName

        if re.search("source_fbx|fbx_take_name|name|translation|hidden|scale", name, re.I):
            continue

        value = stubb.value
        if value is None or not isinstance(value, basestring):
            continue
        for oldSub, newSub in substituteDict.iteritems():
            value = value.replace(oldSub, newSub)
        returnList.update(fileReadLogic(value, depotRootBasename))
    return list(returnList)


def syncAllFilesForPrj(prjFilePath, depotRootBasename):
    """
    Given a prj file path and a project name, sync all the files nested and linked in the prj file

    args:
        prjFilePath (str): The local path to the prj file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the prj
    """
    _SyncManager.clearCache()
    allFiles = getFileListFromPrj(prjFilePath, depotRootBasename)
    if allFiles == []:
        return []
    Perforce.Sync(allFiles)
    return list(allFiles)


def syncAllFilesForAnm(anmFilePath, depotRootBasename):
    """
    Given a anm file path and a project name, sync all the files nested and linked in the anm file

    args:
        anmFilePath (str): The local path to the anm file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the anm
    """
    _SyncManager.clearCache()
    allFiles = getFileListFromAnm(anmFilePath, depotRootBasename)
    if allFiles == []:
        return []
    Perforce.Sync(allFiles)
    return list(allFiles)


def syncAllFilesForPsf(psfFilePath, depotRootBasename):
    """
    Given a psf file path and a project name, sync all the files nested and linked in the psf file

    args:
        psfFilePath (str): The local path to the psf file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    return:
        string list of depot paths that are in the psf
    """
    _SyncManager.clearCache()
    allFiles = fileReadLogic(psfFilePath, depotRootBasename)
    if allFiles == set():
        return []
    Perforce.Sync(allFiles)
    return list(allFiles)


def syncAllFilesForScl(sclFilePath, depotRootBasename):
    """
    Given a scl file path and a project name, sync all the files nested and linked in the scl file

    Args:
        sclFilePath (str): The local path to the scl file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    Returns:
        list[str]: depot paths that are in the scl
    """
    _SyncManager.clearCache()
    allFiles = getFileListFromScl(sclFilePath, depotRootBasename)
    if allFiles == []:
        return []
    Perforce.Sync(allFiles)
    return list(allFiles)


def syncAllFilesForDof(dofFilePath, depotRootBasename):
    """
    Given a dof file path and a project name, sync all the files nested and linked in the dof file

    Args:
        dofFilePath (str): The local path to the dof file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    Returns:
        list[str]: depot paths that are in the dof
    """
    _SyncManager.clearCache()
    allFiles = getFileListFromDof(dofFilePath, depotRootBasename)
    if allFiles == []:
        return []
    Perforce.Sync(allFiles)
    return list(allFiles)


def syncAllFilesForMdl(mdlFilePath, depotRootBasename):
    """
    Given a mdl file path and a project name, sync all the files nested and linked in the mdl file

    Args:
        mdlFilePath (str): The local path to the mdl file to parse
        depotRootBasename (str): The basename of the project's mocap depot root.

    Returns:
        list[str]: depot paths that are in the mdl
    """
    _SyncManager.clearCache()
    allFiles = getFileListFromMdl(mdlFilePath, depotRootBasename)
    if allFiles == []:
        return []
    Perforce.Sync(allFiles)
    return list(allFiles)


if __name__ == "__main__":
    import pprint

    # TODO Move these to unittests...
    # psf = "//depot/projects/bob/capture/takes/2018_0528_Session2122/015705_01_TO_WORLD_PLAYER_CAMP_FIRE_SQUAT_REACTS/015705_01_TO_WORLD_PLAYER_CAMP_FIRE_SQUAT_REACTS.psf"
    # psf = "/media/giant_capture_data/bob/capture/takes/2018_0528_Session2122/015705_01_TO_WORLD_PLAYER_CAMP_FIRE_SQUAT_REACTS/015705_01_TO_WORLD_PLAYER_CAMP_FIRE_SQUAT_REACTS.psf"
    # pprint.pprint(getFileListFromPsf(psf, "bob"))
    pprint.pprint(syncAllFilesForScl("/media/giant_capture_data/bob_dlc/capture/takes/2019_1217_Session0227/000779_04_ED_TEAM_INTRO_HOLDING_LONGARM/000779_04_ED_TEAM_INTRO_HOLDING_LONGARM.scl", "bob_dlc"))
    pprint.pprint(syncAllFilesForDof("/media/giant_capture_data/bob_dlc/capture/takes/2019_1217_Session0227/000779_04_ED_TEAM_INTRO_HOLDING_LONGARM/000779_04_ED_TEAM_INTRO_HOLDING_LONGARM.dof", "bob_dlc"))
    pprint.pprint(syncAllFilesForMdl("/media/giant_capture_data/bob_dlc/capture/takes/2019_1217_Session0227/000779_04_ED_TEAM_INTRO_HOLDING_LONGARM/000779_04_ED_TEAM_INTRO_HOLDING_LONGARM.mdl", "bob_dlc"))
