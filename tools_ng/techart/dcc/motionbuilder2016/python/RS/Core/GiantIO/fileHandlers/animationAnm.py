"""
Class to handle the reading and writing of Animation anm file formats
"""
from RS.Core.GiantIO.fileHandlers import giantBase


class AnimationStubb(object):
    """
    Class to represent an animation
    """

    def __init__(self, stubb):
        """
        Constructor

        args:
            stubb (GiantStubb): The stubb to wrap
        """
        super(AnimationStubb, self).__init__()

        self._model = None
        self._mapping = None
        self._motion = None
        self._name = None

        self._scale = None
        self._translate = None
        self._rotate = None

        self._stubb = stubb

        self._loadData()

    def getModelPath(self):
        """
        Get the model path

        return:
            model path string to use
        """
        return self._model

    def setModelPath(self, value):
        """
        set the model path

        args:
            value(str): mode path string
        """
        self._model = value

    def getMappingPath(self):
        """
        Get the mapping Path

        return:
            mapping path string
        """
        return self._mapping

    def setMappingPath(self, value):
        """
        set the mapping Path

        args:
            value(str): mapping path string
        """
        self._mapping = value

    def getMotionPath(self):
        """
        get the motion path

        return:
            string of the motion path
        """
        return self._motion

    def setMotionPath(self, value):
        """
        set the motion path string

        args:
            value(string): the motion path
        """
        self._motion = value

    def getName(self):
        """
        Get the animation name

        return:
            string name
        """
        return self._name

    def setName(self, value):
        """
        set the animation name

        args:
            value(str): animation name
        """
        self._name = value

    def setTranslation(self, xValue, yValue, zValue):
        """
        set the translation for the animation

        args:
            xValue(float): the xValue to set
            yValue(float): the yValue to set
            zValue(float): the zValue to set
        """
        self._translate = [xValue, yValue, zValue]

    def setRotation(self, xValue, yValue, zValue):
        """
        set the rotation for the animation

        args:
            xValue(float): the xValue to set
            yValue(float): the yValue to set
            zValue(float): the zValue to set
        """
        self._rotate = [xValue, yValue, zValue]

    def setScale(self, scaleFactor):
        """
        set the scale factor for the animation

        args:
            scaleFactor(float): the scale factor
        """
        self._scale = scaleFactor

    def _loadData(self):
        """
        Internal Method

        Load the data from the stubb into the class
        """
        self._model = self._stubb.getOrCreateByStubbName("MODEL").value
        self._mapping = self._stubb.getOrCreateByStubbName("MAPPING").value
        self._motion = self._stubb.getOrCreateByStubbName("MOTION").value
        self._name = self._stubb.getOrCreateByStubbName("NAME").value

        if self._motion is None and self._stubb.value != "":
            self._motion = self._stubb.value

    def writeToStubb(self):
        """
        Write the data from the class onto the underlying stubb so it can be written to a file.
        This does not write to disk.
        """
        self._stubb.getByStubbName("MODEL").value = self._model
        self._stubb.getByStubbName("MAPPING").value = self._mapping
        self._stubb.getByStubbName("MOTION").value = self._motion
        self._stubb.getByStubbName("NAME").value = self._name

        trans = self._translate
        if trans is not None:
            trans = " ".join([str(val) for val in trans])

        rot = self._rotate
        if rot is not None:
            rot = " ".join([str(val) for val in rot])

        scl = self._scale
        if scl is not None:
            scl = ("%s %s %s" % (scl, scl, scl))

        self._stubb.getByStubbName("TRANSLATION").value = trans
        self._stubb.getByStubbName("ROTATION").value = rot
        self._stubb.getByStubbName("SCALE").value = scl

    def getStubb(self):
        """
        Get the stubb which is being wrapped

        return:
            GiantStubb that is wrapped
        """
        return self._stubb


class AnimationAnm(giantBase.GiantFileBase):
    """
    Class to load in a Giant ANM file so its easy to read
    """

    def __init__(self, filePath=None):
        """
        Constructor

        kwargs:
            filePath (str): File path to the file to read in
        """
        super(AnimationAnm, self).__init__(filePath=filePath)
        self._fileType = "Bio Animation File"
        self._fileTypeVersion = "v1.00"

        self._animations = [AnimationStubb(anm) for anm in self.getAllByStubbName("ANIMATION")]

    def getAnimations(self):
        """
        Get all the animations in the file

        return:
            list of all the included models as AnimationStubb
        """
        return self._animations

    def addAnimation(self):
        """
        Add a new animation to the file

        return:
            the newly created model as a AnimationStubb
        """
        newStubb = self._addStubb("ANIMATION")

        newStubb.addNewChild("MODEL", value="")
        newStubb.addNewChild("MAPPING", value=None)
        newStubb.addNewChild("MOTION", value=None)
        newStubb.addNewChild("NAME", value=None)
        newStubb.addNewChild("TRANSLATION", value=None)
        newStubb.addNewChild("ROTATION", value=None)
        newStubb.addNewChild("SCALE", value=None)

        newBody = AnimationStubb(newStubb)
        self._animations.append(newBody)
        return newBody

    def addModel(self, giantFilePath=None):
        """
        Add a new model line to the file

        keyword args:
            giantFilePath (str): Giant formatted file path for the .mdl; default: None

        return:
            giantBase.GiantStubb: The newly created stubb for the model
        """
        newStubb = self._addStubb("MODEL")
        newStubb.value = giantFilePath
        return newStubb

    def addName(self, name):
        """
        Add a new name line to the file

        keyword args:
            name (str): name of the animation

        return:
            giantBase.GiantStubb: The newly created stubb for the model
        """
        newStubb = self._addStubb("NAME")
        newStubb.value = name
        return newStubb

    def addMapping(self, giantFilePath=None):
        """
        Add a new mapping line to the file

        keyword args:
            giantFilePath (str): Giant formatted file path for the .dof; default: None

        return:
            giantBase.GiantStubb: The newly created stubb for the mapping
        """
        newStubb = self._addStubb("MAPPING")
        newStubb.value = giantFilePath
        return newStubb

    def addMotion(self, giantFilePath=None):
        """
        Add a new motion line to the file

        keyword args:
            giantFilePath (str): Giant formatted file path for the .bmo; default: None

        return:
            giantBase.GiantStubb: The newly created stubb for the motion
        """
        newStubb = self._addStubb("MOTION")
        newStubb.value = giantFilePath
        return newStubb

    def addScaling(self, giantFilePath=None):
        """
        Add a new scaling line to the file

        keyword args:
            giantFilePath (str): Giant formatted file path for the .scl; default: None

        return:
            giantBase.GiantStubb: The newly created stubb for the scaling
        """
        newStubb = self._addStubb("SCALING")
        newStubb.value = giantFilePath
        return newStubb

    def writeFile(self, filePath=None):
        """
        ReImplemeted

        Write to a new file or update the currently loaded in file

        kwargs:
            filePath (str): The file path to write to
        """
        for anm in self.getAnimations():
            anm.writeToStubb()
        super(AnimationAnm, self).writeFile(filePath=filePath)
