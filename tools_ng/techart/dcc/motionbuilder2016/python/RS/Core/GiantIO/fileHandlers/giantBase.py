"""
Giant Base file, for reading the Giant "tag" file format
"""
import os


class GiantStubb(object):
    """
    Class to represent a Giant Stubb in a Giant file.

    A Stubb must have a name, and can either have values or sub stubbs
    """
    def __init__(self, stubbName, value=None):
        """
        Constructor

        args:
            stubbName (str): The name of the stubb

        kwargs:
            value (object): A value to assign to the stubb
        """
        self._stubbName = stubbName
        self._value = value
        self._children = []

    def __str__(self):
        """
        Nicely printable representation of object
        """
        strValue = str(self._value or None)
        if len(strValue) > 20:
            strValue = "{}...".format(strValue[:10])
        return "{}({!r}, value={!r})".format(self.__class__.__name__, self._stubbName, strValue)

    def __repr__(self):
        """
        Printable representation of object
        """
        return "<{} at {}>".format(self.__str__(), hex(id(self)))

    @property
    def stubbName(self):
        return self._stubbName

    @stubbName.setter
    def stubbName(self, value):
        self._stubbName = value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value

    def children(self):
        """
        Get all the child stubbs of this stubb

        return:
            list of GiantStubbs
        """
        return self._children

    def addChild(self, newChild):
        """
        Add a GiantStubb as a child of this stubb

        args:
            newChild (newChild): the new stubb to add as a child
        """
        self._children.append(newChild)

    def addNewChild(self, newChildName, value=None):
        """
        Add a new child stubb using its name and value

        args:
            newChildName (str): the name of the new stubb

        kwargs:
            value (object): The value the new stubb has

        returns:
            the new GiantStubb that was added as a child
        """
        newStubb = type(self)(newChildName, value=value)
        self.addChild(newStubb)
        return newStubb

    def getAllByStubbName(self, stubbName):
        """
        Get all the child stubbs in the stubb with the given stubb name

        args:
            stubbName (str): The stubb name to find

        returns:
            list of GiantStubbs with the given name
        """
        return [node for node in self._children if node.stubbName == stubbName]

    def getByStubbName(self, stubbName):
        """
        Get the first child stubb with the given name

        args:
            stubbName (str): The stubb name to find

        returns:
            the GiantStubbs with the given name, or None if none are found
        """
        for node in self._children:
            if node.stubbName == stubbName:
                return node
        return None

    def getOrCreateByStubbName(self, stubbName):
        """
        Get the first child stubb with the given name, or creates one if none exist

        args:
            stubbName (str): The stubb name to find

        returns:
            the GiantStubbs with the given name, or None if none are found
        """
        stubb = self.getByStubbName(stubbName)
        if stubb is not None:
            return stubb
        return self.addNewChild(stubbName)


class GiantFileBase(object):
    """
    Giant File Base for reading and writing Giant ASCII files.

    This should be used as a base for specific file types for a better interface into the files
    """
    def __init__(self, filePath=None, environment=None):
        """
        Args:
            filePath (str, optional): The path to the .psf file.
            environment (str, optional): The path to the current project.
        """
        super(GiantFileBase, self).__init__()
        self._filePath = None
        self._fileType = None
        self._fileTypeVersion = None
        self._lastLinePosition = None
        self._stubbs = []
        self._environment = environment

        if filePath is not None:
            self.readFile(filePath)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{}(filePath={!r})".format(self.__class__.__name__, self._filePath)

    def __repr__(self):
        """
        Printable representation of object
        """
        return "<{} at {}>".format(self.__str__(), hex(id(self)))

    def generateGiantStubb(self, stubbName, value=None, parent=None):
        """
        Virtual method

        Reimplement to allow for subclasses to use specific version of the GiantStubb class for added functionality
        """
        return GiantStubb(stubbName, value=value)

    def readFile(self, filePath):
        """
        Read a file in given file path

        args:
            filePath (str): File path of file to read in
        """
        if not os.path.isfile(filePath):
            raise ValueError("Unable to read file. '{0}' is not a file".format(filePath))
        self._filePath = filePath
        handler = open(filePath)
        self._fileType = self._readLine(handler)
        self._fileTypeVersion = self._readLine(handler)

        while True:
            try:
                newStubb = self._readStubb(handler)
                if newStubb is not None:
                    self._stubbs.append(newStubb)
            except ValueError:
                break
        handler.close()

    def _readStubb(self, handler, parent=None):
        """
        Internal Method

        Read a stubb of the giant file.

        args:
            handler (file handler): the handler for the file

        kwargs:
            parent (GiantStubb): The parent stubb if nested
        """
        line = self._readLine(handler)

        if line == "":
            raise ValueError()

        if "}" in line:
            return True

        if ":" in line:
            name, value = line.split(":", 1)
            name = name.strip()
            newStubb = self.generateGiantStubb(name, self._convertValue(value), parent=parent)
            if parent is not None:
                parent.addChild(newStubb)
            else:
                self._stubbs.append(newStubb)
            return

        if "{" in line:
            name, _ = line.split("{", 1)
            name = name.strip()
            newStubb = self.generateGiantStubb(name, parent=parent)
            newStubb.value = []
            if parent is not None:
                parent.addChild(newStubb)
            else:
                self._stubbs.append(newStubb)
            while(True):
                if self._readStubb(handler, parent=newStubb) == True:
                    return
            return

        if parent is None:
            return
        value = self._convertValue(line)

        if parent.value is None:
            parent.value = value
        elif not isinstance(parent.value, list):
            parent.value = [parent.value, value]
        else:
            parent.value.append(value)

    def _addStubb(self, stubbName):
        newStubb = self.generateGiantStubb(stubbName)
        self._stubbs.append(newStubb)
        return newStubb

    def _convertValue(self, strValue):
        """
        Internal Method

        Convert a string to a float or int if possible, otherwise return back the string

        args:
            strValue (str): the input string value

        returns:
            an int, float or string depending if it can be converted
        """
        try:
            newVal = int(strValue)
            return newVal
        except:
            pass
        try:
            newVal = float(strValue)
            return newVal
        except:
            pass
        return strValue.strip()

    def _readLine(self, handler):
        """
        Internal Method

        Read a single line off a file and strip it so it can be read just for values
        """
        self._lastLinePosition = handler.tell()  # Store last position to allow for re-reading of line.
        newLine = handler.readline()
        newLine = newLine.strip()
        return newLine

    def getAllByStubbName(self, stubbName):
        """
        Get all the stubbs in the file with the given stubb name

        args:
            stubbName (str): The stubb name to find

        returns:
            list of GiantStubbs with the given name
        """
        return [node for node in self._stubbs if node.stubbName == stubbName]

    def getStubbs(self):
        """
        Get all the stubbs in the file

        return:
            list of GiantStubbs with the given name
        """
        return [node for node in self._stubbs]

    def getByStubbName(self, stubbName):
        """
        Get the first stubb with the given name

        args:
            stubbName (str): The stubb name to find

        returns:
            the GiantStubbs with the given name, or None if none are found
        """
        for node in self._stubbs:
            if node.stubbName == stubbName:
                return node
        return None

    def removeStubb(self, stubb):
        try:
            self._stubbs.remove(stubb)
        except ValueError:
            pass

    def removeStubbs(self, stubbs):
        for stubb in stubbs:
            self.removeStubb(stubb)

    def removeStubbByName(self, stubbName):
        self.removeStubb(self.getByStubbName(stubbName))

    def writeFile(self, filePath=None):
        """Write to a new file or update the currently loaded in file

        Args:
            filePath (str, optional): The file path to write to
        """
        with open(filePath or self._filePath, "w") as handler:
            handler.writelines("%s\n" % (self._fileType))
            handler.writelines("%s\n" % (self._fileTypeVersion))

            for stubb in self._stubbs:
                self._writeStubb(handler, stubb)

    def _writeStubb(self, handler, stubb, indentation=0, whitespace=1):
        """Write a stubb of the struct to a file handler

        Args:
            handler (file handler): The handler for the file
            stubb (GiantStubb): The stubb to write
            indentation (int, optional): How much to indent the line
            whitespace (int, optional): How much whitespace to include after the :
        """
        if len(stubb.children()) > 0:
            handler.writelines("%s%s {\n" % (" " * indentation, stubb.stubbName))
            for child in stubb.children():
                self._writeStubb(handler, child, indentation + 2, whitespace=whitespace)
            handler.writelines("%s}\n" % (" " * indentation))

        elif isinstance(stubb.value, list):
            handler.writelines("%s%s {\n" % (" " * indentation, stubb.stubbName))
            for value in stubb.value:
                handler.writelines("%s %s\n" % (" " * (indentation + 1), value))
            handler.writelines("%s}\n" % (" " * indentation))
        else:
            if stubb.value is not None:
                handler.writelines(
                    "{}{}:{}{}\n".format(
                        " " * indentation,
                        stubb.stubbName,
                        " " * whitespace,
                        self._getSafeValue(stubb),
                    )
                )

    def _getSafeValue(self, stubb):
        """Add extra padding to the value string to ensure it works with giants specs"""
        val = stubb.value
        if isinstance(val, str):
            if val.startswith(" "):
                return val
        return "%s" % val

    def filePath(self):
        """
        The file that was read.

        Returns:
            str
        """
        return self._filePath
