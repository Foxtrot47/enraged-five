"""Unittests for utils.giantPaths"""
import unittest
import os
import platform

from RS.Core.GiantIO.utils import giantPaths
from RS.Core.GiantIO import exceptions


class TestGiantPaths(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.projectCodeName = "bob"
        baseDir = "/media/giant_capture_data"
        if platform.system() == "Windows":
            baseDir = r"x:\projects"
        self.rootDir = os.path.join(baseDir, self.projectCodeName)
        self.envVars = {
            "BIO_DATA_DIR": self.rootDir,
            "GIANT_DATA_DIR": self.rootDir,
            "NU_DATA_DIR": self.rootDir,
        }
        for envVar, value in self.envVars.iteritems():
            os.environ[envVar] = value
        self.giantPath = "$BIO_DATA_DIR/dof/male_8.2.dof"
        self.unixPath = "/media/giant_capture_data/bob/dof/male_8.2.dof"
        self.windowsPath = r"x:\projects\bob\dof\male_8.2.dof"
        self.pathMappings = {
            "Windows": self.windowsPath,
            "Linux": self.unixPath,
        }
        self.fileSystemPath = self.pathMappings[platform.system()]

    def test_getRootDirFromEnv(self):
        """Test the output values for giantPaths.getRootDirFromEnv."""
        for envVar in self.envVars.keys():
            self.assertEqual(self.rootDir, giantPaths.getRootDirFromEnv(giantEnvVar=envVar))

    def test_getProjectCodeNameFromEnv(self):
        """Test the output values for giantPaths.getProjectCodeNameFromEnv."""
        # Defaults
        self.assertEqual(self.projectCodeName, giantPaths.getProjectCodeNameFromEnv())
        # Valid root dir
        self.assertEqual(self.projectCodeName, giantPaths.getProjectCodeNameFromEnv(rootPath=self.rootDir))

    @unittest.skipIf(platform.system() == "Windows", "Test is Linux-only.")
    def test_toUnixPath(self):
        """Test the output values for giantPaths.toUnixPath.

        Warnings:
            * This function is deprecated.
        """
        self.assertEqual(
            self.unixPath,
            giantPaths.toUnixPath(self.giantPath, rootDir=None),
            "Path does not match expected output!",
        )
        self.assertEqual(
            self.unixPath,
            giantPaths.toUnixPath(self.giantPath, rootDir=self.rootDir),
            "Path does not match expected output!",
        )

    def test_toFileSystemPath(self):
        """Test the output values for giantPaths.toFileSystemPath."""
        self.assertEqual(
            self.fileSystemPath,
            giantPaths.toFileSystemPath(self.giantPath, self.rootDir),
            "Path does not match expected output!",
        )

    def test_toGiantPath(self):
        """Test the output values for giantPaths.toGiantPath."""
        self.assertEqual(
            self.giantPath,
            giantPaths.toGiantPath(self.fileSystemPath),
            "Path does not match expected output!",
        )
        self.assertEqual(
            self.giantPath,
            giantPaths.toGiantPath(self.fileSystemPath, giantVar="BIO_DATA_DIR"),
            "Path does not match expected output!",
        )
        self.assertEqual(
            self.giantPath,
            giantPaths.toGiantPath(self.fileSystemPath, giantVar="$BIO_DATA_DIR"),
            "Path does not match expected output!",
        )


class TestGiantPathsNoEnv(TestGiantPaths):
    @classmethod
    def setUpClass(cls):
        cls.projectCodeName = "bob"
        baseDir = "/media/giant_capture_data"
        if platform.system() == "Windows":
            baseDir = r"x:\projects"
        cls.rootDir = os.path.join(baseDir, cls.projectCodeName)
        cls.envVars = {
            "BIO_DATA_DIR": cls.rootDir,
            "GIANT_DATA_DIR": cls.rootDir,
            "NU_DATA_DIR": cls.rootDir,
        }
        for envVar, value in cls.envVars.iteritems():
            if os.getenv(envVar):
                os.environ.pop(envVar)
        cls.giantPath = "$BIO_DATA_DIR/dof/male_8.2.dof"
        cls.unixPath = "/media/giant_capture_data/bob/dof/male_8.2.dof"
        cls.windowsPath = r"x:\projects\bob\dof\male_8.2.dof"
        cls.pathMappings = {
            "Windows": cls.windowsPath,
            "Linux": cls.unixPath,
        }
        cls.fileSystemPath = cls.pathMappings[platform.system()]

    def test_getRootDirFromEnv(self):
        """Test the raised exceptions for giantPaths.getRootDirFromEnv."""
        for envVar in self.envVars.keys():
            self.assertRaises(exceptions.GiantEnvironmentNotSetError, giantPaths.getRootDirFromEnv, giantEnvVar=envVar)

    def test_getProjectCodeNameFromEnv(self):
        """Test the raised exceptions for giantPaths.getProjectCodeNameFromEnv."""
        # Defaults
        self.assertRaises(exceptions.GiantEnvironmentNotSetError, giantPaths.getProjectCodeNameFromEnv)
        # Valid root dir
        self.assertEqual(giantPaths.getProjectCodeNameFromEnv(rootPath=self.rootDir), self.projectCodeName)

    @unittest.skipIf(platform.system() == "Windows", "Test is Linux-only.")
    def test_toUnixPath(self):
        """Test the output values for giantPaths.toUnixPath.

        Warnings:
            * This function is deprecated.
        """
        self.assertRaises(exceptions.GiantEnvironmentNotSetError, giantPaths.toUnixPath, self.giantPath, rootDir=None)

    def test_toGiantPath(self):
        """Test the raised exceptions for giantPaths.toGiantPath."""
        self.assertRaises(exceptions.GiantEnvironmentNotSetError, giantPaths.toGiantPath, self.fileSystemPath)
        self.assertRaises(ValueError, giantPaths.toGiantPath, self.fileSystemPath, rootDir="/something/else")
