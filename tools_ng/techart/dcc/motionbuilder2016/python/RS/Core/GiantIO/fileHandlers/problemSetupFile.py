"""
Contains the file handler class for a problem setup file (.psf), the class to represent a Realtime subject,
and related functions.
"""
import os

from RS.Core.GiantIO.fileHandlers import giantBase
from RS.Core.GiantIO.utils import giantPaths, giantEnv


class SubjectCreationError(Exception):
    """
    Raise to indicate an error when creating a new subject.
    """


class SubjectPathValidationError(Exception):
    """
    Raise to indicate an error when validating a subject's path (PROJECT, CHARMAP, TARGET_ANIM, PATTERN) stubb values.
    """


class SubjectIdValidationError(Exception):
    """
    Raise to indicate an error when validating a subject's SUBJECT_ID stubb values.
    """


class Subject(object):
    """
    Class to represent a Realtime subject to be used as a data store or added to the ProblemSetup class.

    Default GiantStubb datatypes are not overridden or modified - getters/setters are responsible for type conversion.
    File paths are internally stored as $BIO_DATA_DIR/path/to/file.ext - getters/setters handle this subsitution.
    For reference, here are the stubb dataypes with notes on which stubbs are required to be in the .psf file:
        PROJECT - str (Required)
        ENABLED - int (Required)
        STEADY_CAMERA - str (Required)
        REFERENCE_OFFSET - str (Required)
        CHARMAP - str (Optional)
        TARGET_ANIM - str (Optional)
        PATTERN - str (Required)
        ATTACHED_OBJECT - str (Optional)
        SUBJECT_ID - str (Optional)

    Note that RT will fail to continue parsing the subject if the first value in the STEADY_CAMERA str is not an int.

    By design, casting this class to a str will return the same result you would see in a .psf file.

    Examples:
        >>> subject = Subject.fromGiantStubb(giantStubb)
        >>> subject = Subject.createSubject()
    """
    # Stubb names.
    SUBJECT_STUBB_NAME = "SUBJECT"
    PROJECT_STUBB_NAME = "PROJECT"
    ENABLED_STUBB_NAME = "ENABLED"
    STEADY_CAMERA_STUBB_NAME = "STEADY_CAMERA"
    REFERENCE_OFFSET_STUBB_NAME = "REFERENCE_OFFSET"
    CHARMAP_STUBB_NAME = "CHARMAP"
    TARGET_ANIM_STUBB_NAME = "TARGET_ANIM"
    PATTERN_STUBB_NAME = "PATTERN"
    ATTACHED_OBJECT_STUBB_NAME = "ATTACHED_OBJECT"
    SUBJECT_ID_STUBB_NAME = "SUBJECT_ID"

    # Defaults.
    REFERENCE_OFFSET_DEFAULT = (0, 0, 0)
    STEADY_CAMERA_DEFAULT = (0, 1, 1)
    ILLEGAL_SUBJECT_ID_CHARS = ("_", " ")

    def __init__(self, stubb, giantPathEnvVarName=None):
        self._stubb = stubb
        self._giantPathEnvVarName = giantPathEnvVarName or giantEnv.GiantVarNames.BIO_DATA_DIR

        # Set on the instance to accurately reflect the environment.
        self._bioDataDir = giantPathEnvVarName or giantPaths.getRootDirFromEnv()

    def __str__(self):
        return "{0}('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')".format(
            self.__class__.__name__,
            self._encodePathForPsf(self.getPrj()),
            str(int(self.getEnabled())),
            " ".join([str(i) for i in self.getReferenceOffset()]),
            " ".join([str(i) for i in self.getSteadyCamera()]),
            self._encodePathForPsf(self.getCharmap()) if self.getCharmap() else None,
            self._encodePathForPsf(self.getTargetAnim()) if self.getTargetAnim() else None,
            self._encodePathForPsf(self.getPattern()) if self.getPattern() else None,
            self.getAttachedObject(),
            self.getSubjectId()
        )

    def __repr__(self):
        return "<{0}('{1}') at {2:#x}>".format(self.__class__.__name__, self.getPrj(), id(self))

    def inGiantPsfFormat(self):
        """
        Get a string back that you cant print to terminal in a format that allows copy/paste into a .psf file.

        This order exactly matches what comes out of Giant Realtime.

        Returns:
            str
        """
        lines = [
            "SUBJECT {",
            "  {0}: {1}".format(self.PROJECT_STUBB_NAME, self._encodePathForPsf(self.getPrj())),
            "  {0}: {1}".format(self.ENABLED_STUBB_NAME, int(self.getEnabled())),
            "  {0}: {1}".format(self.REFERENCE_OFFSET_STUBB_NAME, " ".join([str(i) for i in self.getReferenceOffset()])),
            "  {0}: {1}".format(self.STEADY_CAMERA_STUBB_NAME, " ".join([str(i) for i in self.getSteadyCamera()])),
        ]
        charmap = self.getCharmap()
        if charmap:
            lines.append("  {0}: {1}".format(self.CHARMAP_STUBB_NAME, self._encodePathForPsf(charmap)))
        targetAnim = self.getTargetAnim()
        if targetAnim:
            lines.append("  {0}: {1}".format(self.TARGET_ANIM_STUBB_NAME, self._encodePathForPsf(targetAnim)))
        lines.append("  {0}: {1}".format(self.PATTERN_STUBB_NAME, self._encodePathForPsf(self.getPattern())))
        attachedObject = self.getAttachedObject()
        if attachedObject:
            lines.append("  {0}: {1}".format(self.ATTACHED_OBJECT_STUBB_NAME, attachedObject))
        subjectId = self.getSubjectId()
        if subjectId:
            lines.append("  {0}: {1}".format(self.SUBJECT_ID_STUBB_NAME, subjectId))
        lines.append("}")
        return "\n".join(lines)

    def _resolvePathFromPsf(self, path, environment=None):
        """
        Internal Method

        Replace the env var in the path, so it can be used in standard filesystem calls.

        Args:
            environment (str):

        Returns:
            str: The filepath.
        """
        if environment:
            return giantPaths.toFileSystemPath(path, environment)
        else:
            return giantPaths.toUnixPath(path)

    def _encodePathForPsf(self, path):
        """
        Internal Method

        Replace the value of $BIO_DATA_DIR in the path with "$BIO_DATA_DIR", so it can be used in a Giant .psf file.
        """
        return giantPaths.toGiantPath(path, self._bioDataDir, giantVar=self._giantPathEnvVarName)

    def validate(self):
        """
        Internal Method

        Test for known errors:
            * Confirm that the paths are the expected file extensions.
            * Subject ID has invalid characters.

        Raises:
            SubjectValidationError: When one of known errors is detected.
        """
        # Giant file paths.
        nameExtMappings = {
            self.PROJECT_STUBB_NAME: ".prj",
            self.CHARMAP_STUBB_NAME: ".cm",
            self.TARGET_ANIM_STUBB_NAME: ".anm",
            self.PATTERN_STUBB_NAME: ".ppf",
        }
        for stubbName, ext in nameExtMappings.iteritems():
            stubb = self._stubb.getByStubbName(stubbName)
            if stubb:
                path = stubb.value
                if path is not None:
                    if not path.endswith(ext):
                        raise SubjectPathValidationError("{} stubb ({}) has incorrect extension: {}".format(stubbName, ext, path))

        # Subject ID.
        stubb = self._stubb.getByStubbName(self.SUBJECT_ID_STUBB_NAME)
        if stubb:
            subjectId = stubb.value
            if subjectId is not None:
                self.validateSubjectId(subjectId)

    def getStubb(self):
        """
        Get the internal GiantStubb object representing the Giant Subject.

        Returns:
            giantBase.GiantStubb
        """
        return self._stubb

    def getPrj(self, environment=None):
        """
        Get the resolved path for the subject's .prj file.

        Args:
            environment (str, optional): The path to the project. Ex. /media/giant_capture_data/development

        Returns:
            str: Resolved path to the .prj file.
        """
        return self._resolvePathFromPsf(self._stubb.getByStubbName(self.PROJECT_STUBB_NAME).value, environment=environment)

    def setPrj(self, value):
        self._stubb.getByStubbName(self.PROJECT_STUBB_NAME).value = value

    def getEnabled(self):
        """
        Get the value for the subject's ENABLED stubb as True/False.

        Returns:
            bool: True if enabled, False if disabled
        """
        return self._stubb.getByStubbName(self.ENABLED_STUBB_NAME).value == 1

    def setEnabled(self, value):
        """
        Get the value for the subject's ENABLED stubb.

        Args:
            value (bool): True if enabled, False if disabled
        """
        self._stubb.getByStubbName(self.ENABLED_STUBB_NAME).value = int(value)

    def getSteadyCamera(self):
        """
        Get the values for the subject's STEADY_CAMERA stubb as int/float objects (instead of a single string).

        Returns:
            tuple of int, float, float: i.e. steadyCamToggle, rotationFilter, translationFilter
        """
        stubb = self._stubb.getByStubbName(self.STEADY_CAMERA_STUBB_NAME)
        if stubb:
            toggle, rotationFilter, translationFilter = stubb.value.split(" ")
            return [int(toggle), float(rotationFilter), float(translationFilter)]
        else:
            return self.STEADY_CAMERA_DEFAULT

    def setSteadyCamera(self, value):
        """
        Set the value for the subject's STEADY_CAMERA stubb.

        Args:
            value (tuple of int, float, float): (steadyCamToggle, rotationFilter, translationFilter)
        """
        toggle, rotationFilter, translationFilter = value
        value = " ".join([int(toggle), float(rotationFilter), float(translationFilter)])
        self._stubb.getByStubbName(self.STEADY_CAMERA_STUBB_NAME).value = value

    def getReferenceOffset(self):
        """
        Get the values for the subject's REFERENCE_OFFSET stubb as int/float objects (instead of a single string).

        Returns:
            tuple of int: (x, y, z)
        """
        return [int(val) for val in self._stubb.getByStubbName(self.REFERENCE_OFFSET_STUBB_NAME).value.split(" ")]

    def setReferenceOffset(self, value):
        """
        Set the value for the subject's REFERENCE_OFFSET stubb.

        Args:
            value (tuple of int): i.e. (x, y, z)
        """
        value = " ".join([str(val) for val in value])
        self._stubb.getByStubbName(self.REFERENCE_OFFSET_STUBB_NAME).value = value

    def getCharmap(self):
        """
        Get the value from the subject's CHARMAP stubb as a resolved file path.

        Returns:
            str: Resolved path to the .cm file.
        """
        stubb = self._stubb.getByStubbName(self.CHARMAP_STUBB_NAME)
        if stubb:
            return self._resolvePathFromPsf(stubb.value)
        return None

    def setCharmap(self, path):
        """
        Set the value for the subject's CHARMAP stubb (creating a stubb if none exists).

        Will replace the value of BIO_DATA_DIR in the path with "$BIO_DATA_DIR" if not already done.

        Args:
            path (str): Resolved path to the .cm file.
        """
        stubb = self._stubb.getOrCreateByStubbName(self.CHARMAP_STUBB_NAME)
        stubb.value = self._encodePathForPsf(path)

    def getTargetAnim(self):
        """
        Get the value from the subject's TARGET_ANIM stubb as a resolved file path.

        Returns:
            str: Resolved path to the .anm file.
        """
        stubb = self._stubb.getByStubbName(self.TARGET_ANIM_STUBB_NAME)
        if stubb:
            return self._resolvePathFromPsf(stubb.value)
        return None

    def setTargetAnim(self, path):
        """
        Set the value for the subject's TARGET_ANIM stubb (creating a stubb if none exists).

        Will replace the value of BIO_DATA_DIR in the path with "$BIO_DATA_DIR" if not already done.

        Args:
            path (str): Resolved path to the .anm file.
        """
        stubb = self._stubb.getOrCreateByStubbName(self.TARGET_ANIM_STUBB_NAME)
        stubb.value = self._encodePathForPsf(path)

    def getPattern(self):
        """
        Get the value from the subject's PATTERN stubb as a resolved file path.

        Returns:
            str: Resolved path to the .ppf file.
        """
        stubb = self._stubb.getByStubbName(self.PATTERN_STUBB_NAME)
        if stubb:
            return self._resolvePathFromPsf(stubb.value)
        return None

    def setPattern(self, path):
        """
        Set the value for the subject's PATTERN stubb.

        Will replace the value of BIO_DATA_DIR in the path with "$BIO_DATA_DIR" if not already done.

        Args:
            path (str): Resolved path to the .ppf file.
        """
        stubb = self._stubb.getOrCreateByStubbName(self.PATTERN_STUBB_NAME)
        stubb.value = self._encodePathForPsf(path)

    def getAttachedObject(self):
        stubb = self._stubb.getByStubbName(self.ATTACHED_OBJECT_STUBB_NAME)
        if stubb:
            return stubb.value
        return None

    def setAttachedObject(self, value):
        """
        Set the subject ID value for the subject's ATTACHED_OBJECT stubb (creating a stubb if none exists).

        Args:
            value (str):
        """
        self._stubb.getOrCreateByStubbName(self.ATTACHED_OBJECT_STUBB_NAME).value = value

    def getSubjectId(self):
        """
        Get the raw value for the subject's SUBJECT_ID stubb.

        Returns:
            str: Subject ID value.
        """
        stubb = self._stubb.getByStubbName(self.SUBJECT_ID_STUBB_NAME)
        if stubb:
            return stubb.value
        return None

    def setSubjectId(self, value):
        """
        Set the subject ID value for the subject's SUBJECT_ID stubb (creating a stubb if none exists).

        Args:
            value (str): Subject ID value.
        """
        self.validateSubjectId(value)
        self._stubb.getOrCreateByStubbName(self.SUBJECT_ID_STUBB_NAME).value = value

    @classmethod
    def validateSubjectId(cls, value):
        """
        Runs validation logic extracted from probsetup.tcl's problem_setup_dialog_verify_subject_id method.

        Args:
            value (str): The subject id string to test.
        """
        for char in cls.ILLEGAL_SUBJECT_ID_CHARS:
            if char in value:
                raise SubjectIdValidationError("Subject ID cannot contain spaces or underscores!")

    @classmethod
    def createSubject(cls, prj, enabled, referenceOffset, pattern=None, charmap=None, targetAnim=None,
                      steadyCamera=None, attachedObject=None, subjectId=None, rootDir=None):
        """Factory method.

        Create and return a new GiantStubb and uses that to create a Subject object.

        Will replace the value of BIO_DATA_DIR/GIANT_DATA_DIR in the path with "$BIO_DATA_DIR" if not already done.

        Args:
            prj (str): Path to the .prj file.
            enabled (bool): 1 if enabled, 0 if disabled.
            referenceOffset (list of int): Reference offset values, e.g. [x, y, z].
            pattern (str, optional): Path to the .ppf file.
            charmap (str, optional): Path to the .cm file.
            targetAnim (str, optional): Path to the .anm file.
            steadyCamera (tuple of int, float, float, optional): Steady cam values, e.g. (enabled, rotationFilter, translationFilter).
                Default is disabled with 1 for both filters, e.g. (0, 1, 1). Enabled would be noted with a 1, e.g. (1, 1, 1).
            attachedObject (str, optional): Attached object string used by Giant plugins, i.e. the MotionBuilder plugin.
            subjectId (str, optional): The subject ID.
            rootDir (str, optional): The directory to the project root.

        Returns:
            Subject
        """
        if rootDir is None:
            rootDir = giantPaths.getRootDirFromEnv()
            if rootDir is None:
                raise SubjectCreationError("BIO_DATA_DIR and GIANT_DATA_DIR are not in the environment!")

        if prj is None:
            raise SubjectCreationError("The project file must be defined!")

        prj = giantPaths.toGiantPath(prj, rootDir=rootDir)
        if charmap:
            charmap = giantPaths.toGiantPath(charmap, rootDir=rootDir)
        if targetAnim:
            targetAnim = giantPaths.toGiantPath(targetAnim, rootDir=rootDir)
        if pattern:
            pattern = giantPaths.toGiantPath(pattern, rootDir=rootDir)

        # Defaults.
        if steadyCamera is None:
            steadyCamera = cls.STEADY_CAMERA_DEFAULT

        # Validation.
        if (charmap is not None and targetAnim is None) or (charmap is None and targetAnim is not None):
            raise SubjectCreationError("Both charmap and target anim must be passed in or left as None!")

        failedPaths = [path for path in [prj, charmap, targetAnim, pattern] if path and not path.startswith("$BIO_DATA_DIR")]
        if failedPaths:
            raise SubjectCreationError("Paths could not be formatted with $BIO_DATA_DIR as root:\n{}".format("\n".join(failedPaths)))

        try:
            refOffX, refOffY, refOffZ = referenceOffset
        except:
            raise SubjectCreationError("ReferenceOffset must be a tuple of int, e.g. (0, 0, 0)")
        try:
            scEnabled, scRotFilter, scTrnFilter = steadyCamera
        except:
            raise SubjectCreationError("SteadyCamera must be a tuple of int, float, float, e.g. (0, 1, 1)")
        if subjectId:
            cls.validateSubjectId(subjectId)

        # Add child stubbs.
        newStubb = giantBase.GiantStubb(cls.SUBJECT_STUBB_NAME)
        newStubb.addNewChild(cls.PROJECT_STUBB_NAME, value=prj)
        newStubb.addNewChild(cls.ENABLED_STUBB_NAME, value=int(enabled))
        newStubb.addNewChild(cls.REFERENCE_OFFSET_STUBB_NAME, value=" ".join(str(val) for val in [refOffX, refOffY, refOffZ]))
        newStubb.addNewChild(cls.STEADY_CAMERA_STUBB_NAME, value=" ".join(str(val) for val in (int(scEnabled), scRotFilter, scTrnFilter)))
        if charmap:
            newStubb.addNewChild(cls.CHARMAP_STUBB_NAME, value=charmap)
        if targetAnim:
            newStubb.addNewChild(cls.TARGET_ANIM_STUBB_NAME, value=targetAnim)
        newStubb.addNewChild(cls.PATTERN_STUBB_NAME, value=pattern)
        if attachedObject:
            newStubb.addNewChild(cls.ATTACHED_OBJECT_STUBB_NAME, value=attachedObject)
        if subjectId:
            newStubb.addNewChild(cls.SUBJECT_ID_STUBB_NAME, value=subjectId)

        # Create/add subject object.
        newSubject = Subject(newStubb)

        return newSubject


class ProblemSetup(giantBase.GiantFileBase):
    """
    Class to load and write a Giant PSF (problem setup file).

    Designed to read an existing file or create a new file, not to be used as a data store.

    Examples:
        >>> psf = ProblemSetup(filePath='/tmp/probsetupfile_module_write.psf')
        >>> psf
        <ProblemSetup(filePath='/tmp/probsetupfile_module_write.psf') at 0x7fc457e8a450>
        >>> print psf
        ProblemSetup(filePath='/tmp/probsetupfile_module_write.psf')
        >>> for subject in psf:print subject
    """
    # Stubb names.
    PROBLEM_STUBB_NAME = "PROBLEM_NAME"

    # Defaults.
    DEFAULT_NAME = "rockstar_default_psf"

    def __init__(self, filePath=None, environment=None):
        """
        Args:
            filePath (str, optional): The path to the .psf file.
            environment (str, optional): The path to the current project.
        """
        super(ProblemSetup, self).__init__(filePath=filePath, environment=environment)
        self._filePath = filePath
        self._environment = environment

        self._fileType = "Bio Problem Setup File"
        self._fileTypeVersion = "v1.00"
        self._subjects = []

        if filePath:
            self._readPsf(self._environment)
        else:
            self._addDefaultStubbs()

    def __str__(self):
        return "{0}(filePath='{1}')".format(self.__class__.__name__, self._filePath)

    def __repr__(self):
        return "<{0}(filePath='{1}') at {2:#x}>".format(self.__class__.__name__, self._filePath, id(self))

    def __len__(self):
        return len(self._subjects)

    def __getitem__(self, idx):
        return self._subjects[idx]

    def _addDefaultStubbs(self):
        """
        Internal Method

        Adds the problem name stubb.
        """
        newStubb = giantBase.GiantStubb(self.PROBLEM_STUBB_NAME, value=self.DEFAULT_NAME)
        self._stubbs.append(newStubb)

    def _readPsf(self, environment=None):
        """
        Internal Method

        Checks the stubbs for all Giant subjects and the problem name.

        Args:
            environment (str): The path to the project.

        Warning:
            Will allow invalid subject IDs to be read in from a file in order to allow a UI to present the corrupt data.
        """
        self._subjects = [Subject(stubb, environment) for stubb in self.getAllByStubbName(Subject.SUBJECT_STUBB_NAME)]

    def readFile(self, filePath):
        """
        Reimplemented from giantBase.GiantFileBase.

        Args:
            filePath (str): File path of file to read in
        """
        super(ProblemSetup, self).readFile(filePath)
        self._readPsf(self._environment)

    def _getProblemNameStubb(self):
        """
        Internal Method
        """
        for stubb in self._stubbs:
            if stubb.stubbName == "PROBLEM_NAME":
                return stubb

    def getProblemName(self):
        """
        Returns the value of the PROBLEM_NAME stubb.

        Returns:
            str
        """
        return self._getProblemNameStubb().value

    def setProblemName(self, value):
        """
        Sets the value of the PROBLEM_NAME stubb.

        Args:
            value (str): The name for the problem.
        """
        self._getProblemNameStubb().value = value

    def getSubjects(self, enabled=None):
        """
        Get a list of Giant subjects for the psf, optionally filtered by whether they are enabled or not.

        Args:
            enabled (None, bool): If None, all subjects are returned; if True, only enabled are returned; if False, all
            disabled subjects returned. Default: None.

        Returns:
            list of Subject: A filtered list of subject objects
        """
        if enabled is None:
            return self._subjects
        elif enabled is True:
            return [subject for subject in self._subjects if subject.getEnabled() is True]
        elif enabled is False:
            return [subject for subject in self._subjects if subject.getEnabled() is False]

    def addSubject(self, subject):
        """
        Add a Subject object to the psf's subjects list.

        Args:
            subject (problemSetupFile.Subject): The subject to add to the subject's list.
        """
        self._stubbs.append(subject.getStubb())
        self._subjects.append(subject)

    def validateSubjects(self):
        """
        Runs validation on all subjects.
        """
        for subject in self._subjects:
            if subject.getSubjectId():
                subject.validateSubjectId(subject.getSubjectId())
