"""
Contains convenience classes for working with environment variables required by the LEI/Giant software packages.

The goals of these classes and their properties is to reduce the required boilerplate code and encapsulate any
non-obvious usage details.

Examples:

    # Set all the data related env vars when testing a widget in a main clause of a module:
    >>> giantEnv.DataDirs.setAll("/media/giant_capture_data/development")
    >>> giantEnv.DataDirs.all()
    {'GIANT_DATA_DIR': '/media/giant_capture_data/development',
    'BIO_DATA_DIR': '/media/giant_capture_data/development',
    'NU_DATA_DIR': '/media/giant_capture_data/development'}

    # Get/set specific env var values.
    >>> giantEnv.DataDirs.bio()
    '/media/giant_capture_data/development'
    >>> giantEnv.DataDirs.setBio("/media/giant_capture_data/other")
    >>> giantEnv.DataDirs.bio()
    '/media/giant_capture_data/other'
    >>> giantEnv.DataDirs.setBio(None)  # Can also set it to ""
    >>> type(giantEnv.DataDirs.bio)
    NoneType
    >>> type(os.getenv(giantEnv.GiantVarNames.BIO_DATA_DIR))
    NoneType

    # Get problem setup related values while maintaining compatibility with Giant tools, .i.e. if not set, return "1".
    >>> giantEnv.ProblemSetup.multiCombine()
    '1'
    >>> type(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_MULTI_COMBINE))
    NoneType
    # Typical usage:
    >>> if giantEnv.ProblemSetup.multiCombine():
    ...     print("Do things...")

    # Get lists of var names:
    >>> giantEnv.GiantVarNames.DATA_DIRS
    ['GIANT_DATA_DIR', 'BIO_DATA_DIR', 'NU_DATA_DIR']
    >>> giantEnv.GiantVarNames.RTTRACK_DIRS
    ['BIO_RTTRACK_DIR', 'GIANT_RTTRACK_DIR']
"""
import os
import ast

from RS.Core.GiantIO import exceptions


class GiantVarNames(object):
    """
    Enum of Giant environment variables.

    RTTRACK_DIRS and DATA_DIRS are handy for setting the environment for automated tests.
    """
    BIO_RTTRACK_DIR = "BIO_RTTRACK_DIR"
    GIANT_RTTRACK_DIR = "GIANT_RTTRACK_DIR"
    RTTRACK_DIRS = [BIO_RTTRACK_DIR, GIANT_RTTRACK_DIR]

    BIO_DATA_DIR = "BIO_DATA_DIR"
    GIANT_DATA_DIR = "GIANT_DATA_DIR"
    NU_DATA_DIR = "NU_DATA_DIR"
    DATA_DIRS = [GIANT_DATA_DIR, BIO_DATA_DIR, NU_DATA_DIR]

    GIANT_USE_COMBINED_DIR = "GIANT_USE_COMBINED_DIR"
    GIANT_PROBLEM_SETUP_USE_IMU = "GIANT_PROBLEM_SETUP_USE_IMU"
    GIANT_PROBLEM_SETUP_LIVE_CHANGES = "GIANT_PROBLEM_SETUP_LIVE_CHANGES"
    GIANT_PROBLEM_SETUP_MULTI_COMBINE = "GIANT_PROBLEM_SETUP_MULTI_COMBINE"

    ALL = [
        BIO_RTTRACK_DIR,
        GIANT_RTTRACK_DIR,
        BIO_DATA_DIR,
        GIANT_DATA_DIR,
        NU_DATA_DIR,
        GIANT_USE_COMBINED_DIR,
        GIANT_PROBLEM_SETUP_USE_IMU,
        GIANT_PROBLEM_SETUP_LIVE_CHANGES,
        GIANT_PROBLEM_SETUP_MULTI_COMBINE,
    ]


class _BaseEnvVarManager(object):
    """
    Base class for environment variable managers.
    """

    @classmethod
    def _setValue(cls, key, value):
        if not isinstance(value, basestring) and value is not None:
            raise TypeError("Accepted types: str, None. Got: {}".format(type(value)))
        if value in [None, ""]:
            if key in os.environ:
                os.environ.pop(key)
        else:
            os.environ[key] = str(value)


class DataDirs(_BaseEnvVarManager):
    """
    Manager for the *_DATA_DIR environment variables.
    """

    @staticmethod
    def tokenize(value):
        """
        Parse the data dir path into useful tokens.

        Args:
            value (str): A data dir path.

        Returns:
            tuple: projectRootDir, projectCode
        """
        if not value or not isinstance(value, basestring):
            raise exceptions.GiantEnvironmentError("Could not parse: {}".format(value))
        projectRootDir, projectCode = value.rsplit(os.path.sep, 1)
        return projectRootDir, projectCode

    @classmethod
    def bio(cls):
        return os.getenv(GiantVarNames.BIO_DATA_DIR, None)

    @classmethod
    def setBio(cls, value):
        cls._setValue(GiantVarNames.BIO_DATA_DIR, value)

    @classmethod
    def giant(cls):
        return os.getenv(GiantVarNames.GIANT_DATA_DIR, None)

    @classmethod
    def setGiant(cls, value):
        cls._setValue(GiantVarNames.GIANT_DATA_DIR, value)

    @classmethod
    def nu(cls):
        return os.getenv(GiantVarNames.NU_DATA_DIR, None)

    @classmethod
    def setNu(cls, value):
        cls._setValue(GiantVarNames.NU_DATA_DIR, value)

    @classmethod
    def all(cls):
        """
        Get *_DATA_DIR environment variable values.

        Returns:
            dict of *_DATA_DIR environment variables and their values. A None value indicates the var is not set.
        """
        return {key: os.getenv(key, None) for key in GiantVarNames.DATA_DIRS}

    @classmethod
    def setAll(cls, value):
        """
        Convenience method for setting all the *_DATA_DIR environment variables.

        Args:
            value (str): Directory path with no trailing slash.
        """
        for key in GiantVarNames.DATA_DIRS:
            cls._setValue(key, value)


class RtTrackDirs(_BaseEnvVarManager):
    """
    Manager for the *_RTTRACK_DIR environment variables.
    """

    @classmethod
    def bio(cls):
        return os.getenv(GiantVarNames.BIO_RTTRACK_DIR, None)

    @classmethod
    def setBio(cls, value):
        cls._setValue(GiantVarNames.BIO_RTTRACK_DIR, value)

    @classmethod
    def giant(cls):
        return os.getenv(GiantVarNames.GIANT_RTTRACK_DIR, None)

    @classmethod
    def setGiant(cls, value):
        cls._setValue(GiantVarNames.GIANT_RTTRACK_DIR, value)

    @classmethod
    def all(cls):
        """
        Get *_RTTRACK_DIR environment variable values.

        Returns:
            dict of *_RTTRACK_DIR environment variables and their values. A None value indicates the var is not set.
        """
        return {key: os.getenv(key, None) for key in GiantVarNames.RTTRACK_DIRS}

    @classmethod
    def setAll(cls, value):
        """
        Convenience method for setting all the *_DATA_DIR environment variables.

        Args:
            value (str): Directory path with no trailing slash.
        """
        for key in GiantVarNames.RTTRACK_DIRS:
            cls._setValue(key, value)


class ProblemSetup(_BaseEnvVarManager):
    """
    Manager for the GIANT_PROBLEM_SETUP_* and related environment variables.

    The logic is based on probsetup.tcl with notes regarding anything special listed in the docstrings.

    All methods store the values as integers to maintain compatibility with the Giant software.
    """

    @classmethod
    def _setValue(cls, key, value):
        """
        Reimplemented

        Required to support the Giant string values.
        """
        if not isinstance(value, bool) and value is not None:
            raise TypeError("Accepted types: bool, None. Got: {}".format(type(value)))
        if value in [None, ""]:
            if key in os.environ:
                os.environ.pop(key)
        else:
            os.environ[key] = str(int(value))

    @classmethod
    def _getValue(cls, key):
        """
        Internal method

        Handles env var str to bool conversions.
        """
        value = os.getenv(key, None)
        if value:
            value = bool(ast.literal_eval(value))
        return value

    @classmethod
    def useCombinedDir(cls):
        return cls._getValue(GiantVarNames.GIANT_USE_COMBINED_DIR)

    @classmethod
    def setUseCombinedDir(cls, value):
        cls._setValue(GiantVarNames.GIANT_USE_COMBINED_DIR, value)

    @classmethod
    def useImu(cls):
        return cls._getValue(GiantVarNames.GIANT_PROBLEM_SETUP_USE_IMU)

    @classmethod
    def setUseImu(cls, value):
        """
        Show/hide the IMU widgets in Realtime.

        Args:
            value (bool): True to show, False to hide.
        """
        cls._setValue(GiantVarNames.GIANT_PROBLEM_SETUP_USE_IMU, value)

    @classmethod
    def liveChanges(cls):
        """
        Has special logic due to some assumptions in the Giant codebase - returns True if the env var is not set.

        Returns:
            bool
        """
        value = cls._getValue(GiantVarNames.GIANT_PROBLEM_SETUP_LIVE_CHANGES)
        if value is None:
            value = True
        return value

    @classmethod
    def setLiveChanges(cls, value):
        cls._setValue(GiantVarNames.GIANT_PROBLEM_SETUP_LIVE_CHANGES, value)

    @classmethod
    def multiCombine(cls):
        """
        Has special logic due to some assumptions in the Giant codebase - returns True if the env var is not set.

        Returns:
            bool
        """
        value = cls._getValue(GiantVarNames.GIANT_PROBLEM_SETUP_MULTI_COMBINE)
        if value is None:
            value = True
        return value

    @classmethod
    def setMultiCombine(cls, value):
        cls._setValue(GiantVarNames.GIANT_PROBLEM_SETUP_MULTI_COMBINE, value)


if __name__ == '__main__':
    rootDir = "/media/giant_capture_data/development"
    for envVar in GiantVarNames.RTTRACK_DIRS+GiantVarNames.DATA_DIRS:
        os.environ[envVar] = rootDir

    print DataDirs.all()
    DataDirs.setAll("/media/giant_capture_data/other")
    print DataDirs.all()

    print ProblemSetup.useImu()
    print ProblemSetup.liveChanges()
    print ProblemSetup.multiCombine()
