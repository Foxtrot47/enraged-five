"""
Class to handle the reading and writing of BMO file formats
"""
import sys
import os
import re
import mmap
import csv


class BmoFrame(object):
    """
    A Bmo frame of data
    """

    def __init__(self, frame, numberOfVars, tc=None, vars=None):
        """
        Constructor

        args:
            frame (int): Frame number for the frame of data

        kwargs:
            tc (str): the time code as a string for the frame
        """
        self._frame = frame
        self._tc = tc or ""
        self._vars = vars or [None for _ in xrange(numberOfVars + 1)]

    def __str__(self):
        """
        Nicely printable representation of object
        """
        args_ = ", ".join([str(self._frame), "tc='{}'".format(self._tc)])
        return "{}({})".format(self.__class__.__name__, args_)

    def __repr__(self):
        """
        Printable representation of object
        """
        return "<{} at {}>".format(self.__str__(), hex(id(self)))

    def setFrame(self, frame):
        """
        Set the frame index for the frame

        args:
            frame (int): the frame number
        """
        self._frame = frame

    def getFrame(self):
        """
        Get the frame index for the frame

        return:
            the frame number as an int
        """
        return self._frame

    def setTC(self, tc):
        """
        Set the TimeCode for the frame

        args:
            tc(string): the timecode as a string "14:45:50:20"
        """
        self._tc = tc

    def getTC(self):
        """
        Get the TimeCode for the frame

        return:
            the timecode as a string "14:45:50:20"
        """
        return self._tc

    def setVar(self, idx, value):
        """
        Set the variable for the given index

        args:
            idx (int): the index to set the value
            value (object): the value to set
        """
        # BMO indexs start at 1.
        self._vars[idx - 1] = value

    def getVar(self, idx):
        """
        Get the value for the given index

        args:
            idx (int): the index to get

        return:
            the variable at the given index as an object
        """
        # BMO indexs start at 1.
        return self._vars[idx - 1]

    def getVars(self):
        """
        Get all the variables as a dict

        return:
            a dict of all the variables where the key is the index and value is the value
        """
        return {idx + 1: value for idx, value in enumerate(self._vars)}

    def getRawVars(self):
        return self._vars


class SystemVarsBmo(object):
    """
    Giant File Base for reading and writing Giant BMO files.
    """
    REGEX_FRAME_HEADER = r"(?:Results for field |Results for frame )(?P<frameIndex>[\d]+)(?: +)?(?P<timecode>[\d:;]+)?"

    def __init__(self, filePath=None):
        """
        Constructor

        kargs:
            filePath (str): File path to read in if given
        """
        super(SystemVarsBmo, self).__init__()
        self._filePath = None
        self._fileType = "Bio System Variable File"
        self._fileTypeVersion = "v1.00"

        self._varCount = 0  # Populated in the readFile method.
        self._frameData = []

        if filePath is not None:
            self.readFile(filePath)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{}({!r})".format(self.__class__.__name__, self._filePath)

    def __repr__(self):
        """
        Printable representation of object
        """
        return "<{} at {}>".format(self.__str__(), hex(id(self)))

    def getVarCount(self):
        """
        Return the variable count as read from the input file.

        Returns:
            int: The number of variables per frame.
        """
        return self._varCount

    def getFrames(self):
        """
        Get the frames in the bmo

        return:
            a list of all the frames as BmoFrame
        """
        return self._frameData

    def getFrame(self, idx):
        """
        Get the frame for the given index

        args:
            idx (int): the frame to get

        return:
            the indexed frame as a BmoFrame or None
        """
        try:
            # BMO indexs start at 1.
            return self._frameData[idx - 1]
        except IndexError:
            return None

    def getOrCreateFrame(self, frameIdx):
        """
        Get or create a frame with the given index

        args:
            idx (int): the frame to get

        return:
            the indexed frame as a BmoFrame
        """
        frame = self.getFrame(frameIdx)
        if frame is None:
            frame = self.addFrame(frameIdx)
        return frame

    def addFrame(self, frameIdx):
        """
        add new a frame with the given index

        args:
            idx (int): the frame to get

        return:
            the newly created frame as a BmoFrame
        """
        newFrame = BmoFrame(int(frameIdx, 10), self._varCount)
        self._frameData.append(newFrame)
        return newFrame

    def readFile(self, filePath):
        """
        Read a file in given file path

        args:
            filePath (str): File path of file to read in
        """
        self._frameData = []
        if not os.path.isfile(filePath):
            raise ValueError("Unable to read file. '{0}' is not a file".format(filePath))
        self._filePath = filePath

        # Read file to a memory map, much faster to access
        with open(filePath, mode="r") as fileHandle:
            handler = mmap.mmap(fileHandle.fileno(), length=0, access=mmap.ACCESS_READ)

        self._fileType = self._readLine(handler)
        self._fileTypeVersion = self._readLine(handler)

        frameCount = int(self._readLine(handler), 10)  # number of frames in data
        self._varCount = int(self._readLine(handler), 10)  # number of vars per frame

        # Read the frames - Start of highly optimized code
        oldInterval = sys.getcheckinterval()
        sys.setcheckinterval(10000)
        framesAppend = self._frameData.append  # Speed tip to avoid dots

        for _ in xrange(frameCount):
            line = self._readLine(handler)

            frameIdx, tc = re.search(self.REGEX_FRAME_HEADER, line).groups()
            self._readLine(handler)  # Skip "Comment" line

            rawValues = []
            rawAppend = rawValues.append  # Speed tip to avoid dots
            for _ in xrange(self._varCount):
                rawAppend(self._readLine(handler))

            frameVars = []
            frameAppend = frameVars.append  # Speed tip to avoid dots
            for data in  csv.reader(rawValues, delimiter=' ', skipinitialspace=True, quoting=csv.QUOTE_NONNUMERIC):
                frameAppend(data[0])

            frame = BmoFrame(int(frameIdx, 10), self._varCount, tc, frameVars)
            framesAppend(frame)
        sys.setcheckinterval(oldInterval)

        handler.close()

    def _readLine(self, handler):
        """
        Internal Method

        Read a single line off a file and strip it so it can be read just for values
        """
        return handler.readline()

    def writeFile(self, filePath=None):
        """
        Write to a new file or update the currently loaded in file

        kwargs:
            filePath (str): The file path to write to
        """
        with open(filePath or self._filePath, 'w') as handler:

            maxVarCount = self._varCount

            handler.writelines("%s\n" % (self._fileType))
            handler.writelines("%s\n" % (self._fileTypeVersion))

            handler.writelines("%s\n" % (len(self._frameData)))
            handler.writelines("%s\n" % (maxVarCount))

            frames = {frame.getFrame(): frame for frame in self._frameData}
            keyFrames = frames.keys()
            keyFrames.sort()
            for key in keyFrames:
                self._writeFrame(handler, frames[key], maxVarCount)

    def _writeFrame(self, handler, frame, maxVarCount):
        """
        Internal Method

        Write a frame of the struct to a file handler

        args:
            handler (file handler): the handler for the file
            frame (BmoFrame): the frame to write
            maxVarCount (int): the maximum variables to wirte
        """
        handler.writelines(" Results for frame %s %s\n" % (frame.getFrame(), frame.getTC()))
        handler.writelines("    Value      #   Description\n")
        for idx in xrange(1, maxVarCount + 1):
            handler.writelines("    %s %s \n" % (frame.getVar(idx) or 0.0, idx))
