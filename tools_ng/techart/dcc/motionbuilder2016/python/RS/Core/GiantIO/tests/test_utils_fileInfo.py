"""Unittests for utils.fileInfo"""
import unittest
import platform


@unittest.skipIf(platform.system() == "Windows", "Linux-only")
class TestFileInfo(unittest.TestCase):
    projectName = "development"

    # TODO: find a pdf with files in it.
    def test_getFileListFromPdf(self):
        from RS.Core.GiantIO.utils import fileInfo
        expectedResult = [
            "/media/giant_capture_data/development/probspec/props/ActionSlate_BeEast/ActionSlate_BeEast.pdf"
        ]
        pdfFilePath = "/media/giant_capture_data/development/probspec/props/ActionSlate_BeEast/ActionSlate_BeEast.pdf"
        result = fileInfo.getFileListFromPdf(pdfFilePath, self.projectName)
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")

    def test_getFileListFromPrj(self):
        from RS.Core.GiantIO.utils import fileInfo

        # Test a prj with the PATTERN stub in it.
        expectedResult = [
            "/media/giant_capture_data/development/calib/cal_gruE_opti_62cam_nov22_2016pm.cal",
            "/media/giant_capture_data/development/capture/props/ActionSlate_BeEast_02.rom/ActionSlate_BeEast_02.rom.bmo",
            "/media/giant_capture_data/development/capture/props/ActionSlate_BeEast_02.rom/ActionSlate_BeEast_02.rom.gid",
            "/media/giant_capture_data/development/capture/props/ActionSlate_BeEast_02.rom/ActionSlate_BeEast_02.rom.raw",
            "/media/giant_capture_data/development/model/ActionSlate_BeEast/ActionSlate_BeEast.mdl",
            "/media/giant_capture_data/development/probspec/body.sch",
            "/media/giant_capture_data/development/probspec/props/ActionSlate_BeEast.psp",
            "/media/giant_capture_data/development/probspec/props/ActionSlate_BeEast/ActionSlate_BeEast.lmt",
            "/media/giant_capture_data/development/probspec/props/ActionSlate_BeEast/ActionSlate_BeEast.pdf",
            "/media/giant_capture_data/development/probspec/props/ActionSlate_BeEast/ActionSlate_BeEast.sdf",
            "/media/giant_capture_data/development/scaling/ActionSlate_BeEast_02.scl",
            "/media/giant_capture_data/development/subject/ActionSlate_BeEast_02/ActionSlate_BeEast_02.bdf",
            "/media/giant_capture_data/development/subject/ActionSlate_BeEast_02/ActionSlate_BeEast_02.ppf",
        ]
        prjFilePath = "/media/giant_capture_data/development/capture/props/ActionSlate_BeEast_02.rom.prj"
        result = sorted(fileInfo.getFileListFromPrj(prjFilePath, self.projectName))
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")

        # Test a prj without the PATTERN stub in it.
        expectedResult = [
            "/media/giant_capture_data/development/calib/cal_north_opti_28cam_aug14_2017pm.cal",
            "/media/giant_capture_data/development/capture/takes/2017_0814_Session0081/000077_01_ED_North_Prime41_Person_Test/000077_01_ED_North_Prime41_Person_Test.bmo",
            "/media/giant_capture_data/development/capture/takes/2017_0814_Session0081/000077_01_ED_North_Prime41_Person_Test/000077_01_ED_North_Prime41_Person_Test.gid",
            "/media/giant_capture_data/development/capture/takes/2017_0814_Session0081/000077_01_ED_North_Prime41_Person_Test/000077_01_ED_North_Prime41_Person_Test.raw",
            "/media/giant_capture_data/development/model/N_AK47A/N_AK47A.mdl",
            "/media/giant_capture_data/development/model/combined/N_AK47A_rsM_01.mdl",
            "/media/giant_capture_data/development/model/rsM_01.mdl",
            "/media/giant_capture_data/development/probspec/body.sch",
            "/media/giant_capture_data/development/probspec/characters/rsM_03.2/rsM_03.2.lmt",
            "/media/giant_capture_data/development/probspec/characters/rsM_03.2/rsM_03.2.pdf",
            "/media/giant_capture_data/development/probspec/props/N_AK47A/N_AK47A.lmt",
            "/media/giant_capture_data/development/probspec/props/N_AK47A/N_AK47A.pdf",
            "/media/giant_capture_data/development/probspec/props/combined/N_AK47A_rsM_03.2/N_AK47A_rsM_03.2.lmt",
            "/media/giant_capture_data/development/probspec/props/combined/N_AK47A_rsM_03.2/N_AK47A_rsM_03.2.pdf",
            "/media/giant_capture_data/development/scaling/N_AK47.scl",
            "/media/giant_capture_data/development/scaling/combined/N_AK47_rlego_170814pm.scl",
            "/media/giant_capture_data/development/scaling/rlego_170814pm.scl",
            "/media/giant_capture_data/development/subject/N_AK47/N_AK47.bdf",
            "/media/giant_capture_data/development/subject/N_AK47/N_AK47.ppf",
            "/media/giant_capture_data/development/subject/combined/N_AK47_rlego_170814pm/N_AK47_rlego_170814pm.bdf",
            "/media/giant_capture_data/development/subject/rlego_170814pm/rlego_170814pm.bdf",
            "/media/giant_capture_data/development/subject/rlego_170814pm/rlego_170814pm.ppf",
        ]
        prjFilePath = "/media/giant_capture_data/development/capture/takes/2017_0814_Session0081/000077_01_ED_North_Prime41_Person_Test.prj"
        result = sorted(fileInfo.getFileListFromPrj(prjFilePath, self.projectName))
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")

    def test_getFileListFromPsf(self):
        from RS.Core.GiantIO.utils import fileInfo
        expectedResult = [
            "/media/giant_capture_data/development/calib/cal_grumman_aux_8cam_aug29_2016.cal",
            "/media/giant_capture_data/development/capture/props/ActionSlate_03.rom/ActionSlate_03.rom.bmo",
            "/media/giant_capture_data/development/capture/props/ActionSlate_03.rom/ActionSlate_03.rom.gid",
            "/media/giant_capture_data/development/capture/props/ActionSlate_03.rom/ActionSlate_03.rom.raw",
            "/media/giant_capture_data/development/model/ActionSlate_03/ActionSlate_03.mdl",
            "/media/giant_capture_data/development/probspec/body.sch",
            "/media/giant_capture_data/development/probspec/props/ActionSlate_03.psp",
            "/media/giant_capture_data/development/probspec/props/ActionSlate_03/ActionSlate_03.lmt",
            "/media/giant_capture_data/development/probspec/props/ActionSlate_03/ActionSlate_03.pdf",
            "/media/giant_capture_data/development/probspec/props/ActionSlate_03/ActionSlate_03.sdf",
            "/media/giant_capture_data/development/scaling/ActionSlate_03.scl",
            "/media/giant_capture_data/development/subject/ActionSlate_03/ActionSlate_03.bdf",
            "/media/giant_capture_data/development/subject/ActionSlate_03/ActionSlate_03.ppf",
        ]
        psfFilePath = "/media/giant_capture_data/development/capture/props/ActionSlate_BeEast_02.rom/ActionSlate_BeEast_02.rom.psf"
        result = sorted(fileInfo.getFileListFromPsf(psfFilePath, self.projectName))
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")

    def test_getFileListFromBdf(self):
        from RS.Core.GiantIO.utils import fileInfo
        expectedResult = [
            "/media/giant_capture_data/development/subject/PropToy2_001/PropToy2_001.bdf",
            "/media/giant_capture_data/development/subject/PropToy2_001/PropToy2_001.ppf",
            "/media/giant_capture_data/development/subject/combined/ddumm_181214am_PropToy2_001/ddumm_181214am_PropToy2_001.bdf",
            "/media/giant_capture_data/development/subject/ddumm_181214am/ddumm_181214am.bdf",
            "/media/giant_capture_data/development/subject/ddumm_181214am/ddumm_181214am.ppf",
        ]
        bdfFilePath = "/media/giant_capture_data/development/subject/combined/ddumm_181214am_PropToy2_001/ddumm_181214am_PropToy2_001.bdf"
        result = sorted(fileInfo.getFileListFromBdf(bdfFilePath, self.projectName))
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")

    def test_getFileListFromScl(self):
        from RS.Core.GiantIO.utils import fileInfo
        sclFilePath = "/media/giant_capture_data/bob_dlc/scaling/combined/cmacl_181113am_cmill_190130am.scl"
        self.assertRaises(fileInfo.MissingFile, fileInfo.getFileListFromScl, sclFilePath, self.projectName)

        expectedResult = [
            "/media/giant_capture_data/bob_dlc/scaling/combined/slateActionClockWest_slateWest.scl",
            "/media/giant_capture_data/development/scaling/slateActionClockWest.scl",
            "/media/giant_capture_data/development/scaling/slateWest.scl",
        ]
        sclFilePath = "/media/giant_capture_data/bob_dlc/scaling/combined/slateActionClockWest_slateWest.scl"
        result = sorted(fileInfo.getFileListFromScl(sclFilePath, self.projectName))
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")

    # TODO: find a lmt with files in it.
    def test_getFileListFromLmt(self):
        from RS.Core.GiantIO.utils import fileInfo
        expectedResult = [
            "/media/giant_capture_data/development/probspec/props/ActionSlate_BeEast/ActionSlate_BeEast.lmt"
        ]
        lmtFilePath = "/media/giant_capture_data/development/probspec/props/ActionSlate_BeEast/ActionSlate_BeEast.lmt"
        result = fileInfo.getFileListFromLmt(lmtFilePath, self.projectName)
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")

    # TODO: find a dof with files in it.
    def test_getFileListFromDof(self):
        from RS.Core.GiantIO.utils import fileInfo
        expectedResult = [
            "/media/giant_capture_data/development/dof/ActionSlate_BeEast.dof"
        ]
        dofFilePath = "/media/giant_capture_data/development/dof/ActionSlate_BeEast.dof"
        result = fileInfo.getFileListFromDof(dofFilePath, self.projectName)
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")

    # TODO: find a mdl with files in it.
    def test_getFileListFromMdl(self):
        from RS.Core.GiantIO.utils import fileInfo
        expectedResult = [
            "/media/giant_capture_data/development/model/ActionSlate_BeEast/ActionSlate_BeEast.mdl"
        ]
        mdlFilePath = "/media/giant_capture_data/development/model/ActionSlate_BeEast/ActionSlate_BeEast.mdl"
        result = fileInfo.getFileListFromMdl(mdlFilePath, self.projectName)
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")

    def test_getFileListFromAnm(self):
        from RS.Core.GiantIO.utils import fileInfo
        expectedResult = [
            "/media/giant_capture_data/development/anim/refined/takes/2017_0623_Session0048/000046_01_BE_4Person_live_hANDS.rsrefine.anm",
            "/media/giant_capture_data/development/capture/takes/2017_0623_Session0048/000046_01_BE_4Person_live_hANDS/000046_01_BE_4Person_live_hANDS.rsrefine.bmo",
            "/media/giant_capture_data/development/dof/ActionSlate_BeEast.dof",
            "/media/giant_capture_data/development/dof/combined/ActionSlate_BeEast_BRAINFL.M01_rsM_03.2_ROBRA.M02_rsM_03.2_LUKASWO.M03_rsM_03.2_TOMMYWI.MO4_rsM_03.2.dof",
            "/media/giant_capture_data/development/dof/rsM_03.2.dof",
            "/media/giant_capture_data/development/model/ActionSlate_BeEast/ActionSlate_BeEast.mdl",
            "/media/giant_capture_data/development/model/combined/ActionSlate_BeEast_BRAINFL.M01_rsM_01_ROBRA.M02_rsM_01_LUKASWO.M03_rsM_01_TOMMYWI.MO4_rsM_01.mdl",
            "/media/giant_capture_data/development/model/rsM_01.mdl",
            "/media/giant_capture_data/development/scaling/ActionSlate_BeEast_02.scl",
            "/media/giant_capture_data/development/scaling/bfloo_170623pm.scl",
            "/media/giant_capture_data/development/scaling/combined/ActionSlate_BeEast_02_BRAINFL.M01_bfloo_170623pm_ROBRA.M02_rraim_170623pm_LUKASWO.M03_lwood_170623pm_TOMMYWI.MO4_twill_170623pm.scl",
            "/media/giant_capture_data/development/scaling/lwood_170623pm.scl",
            "/media/giant_capture_data/development/scaling/rraim_170623pm.scl",
            "/media/giant_capture_data/development/scaling/twill_170623pm.scl",
        ]
        anmFilePath = "/media/giant_capture_data/development/anim/refined/takes/2017_0623_Session0048/000046_01_BE_4Person_live_hANDS.rsrefine.anm"
        result = sorted(fileInfo.getFileListFromAnm(anmFilePath, self.projectName))
        self.assertEqual(expectedResult, result, "Did not get expected list of files!")
