"""Class to handle the reading and writing of Project (.prj) file format."""
import os

from RS.PyCore.abstracts import abstractConsts
from RS.Core.GiantIO.fileHandlers import giantBase
from RS.Core.GiantIO import exceptions


class FileStubNames(abstractConsts.AbstractConsts):
    PROBLEM_SPEC = "PROBLEM_SPEC"
    PROBLEM_DEF = "PROBLEM_DEF"
    CALIBRATION = "CALIBRATION"
    SCALE_DEF = "SCALE_DEF"
    RAW_DATA = "RAW_DATA"
    GLOB_IDS = "GLOB_IDS"
    BODY_DEF = "BODY_DEF"
    LIMITS = "LIMITS"
    SEARCH_PARAMS = "SEARCH_PARAMS"
    GFX_MODEL = "GFX_MODEL"
    GFX_SCALING = "GFX_SCALING"
    PATTERN = "PATTERN"
    MOTION = "MOTION"
    RANGES = "RANGES"
    # Single bone only
    UNSCALED_BODY_DEF = "UNSCALED_BODY_DEF"
    # Multi bone only
    CHARMAP_PROJECT = "CHARMAP_PROJECT"
    GLOBS = "GLOBS"
    IDS = "IDS"
    PROBLEM_SETUP = "PROBLEM_SETUP"
    RTTRACK_PARAMS = "RTTRACK_PARAMS"
    SECONDARY_ANIM = "SECONDARY_ANIM"
    STREAM = "STREAM"


class DataStubNames(abstractConsts.AbstractConsts):
    # Single bone only
    ACQUISITION_HOST = "ACQUISITION_HOST"
    THRESHOLDS = "THRESHOLDS"
    NUM_CAMERAS = "NUM_CAMERAS"
    FRAMES_PER_SEC = "FRAMES_PER_SEC"
    # Multi bone only
    FRAME_RATE = "FRAME_RATE"
    TIMECODE_RATE = "TIMECODE_RATE"


class GiantPrj(giantBase.GiantFileBase):
    """Class to load in a Giant Prj file so its easy to read"""

    def __init__(self, filePath=None):
        super(GiantPrj, self).__init__(filePath=filePath)
        self._fileType = "Bio Project File"
        self._fileTypeVersion = "v3.0"

    def readFile(self, filePath):
        """ReImplemented.

        Read a file in given file path.

        Args:
            filePath (str): File path of file to read in
        """
        if not os.path.isfile(filePath):
            raise ValueError("Unable to read file. '{0}' is not a file".format(filePath))
        self._filePath = filePath
        handler = open(filePath)
        self._fileType = self._readLine(handler)
        self._fileTypeVersion = self._readLine(handler)

        # Giant sometimes pads out its header and body data
        for _ in xrange(20):
            currentPos = handler.tell()
            line = self._readLine(handler)
            if line != "":
                handler.seek(currentPos)
                break

        while True:
            try:
                newStubb = self._readStubb(handler)
                if newStubb is not None:
                    self._stubbs.append(newStubb)
            except:
                break
        handler.close()

    def writeFile(self, filePath=None):
        """ReImplemented.

        Read a file in given file path.

        Args:
            filePath (str): File path of file to read in
        """
        with open(filePath or self._filePath, "w") as handler:
            handler.writelines("%s\n" % (self._fileType))
            handler.writelines("%s\n" % (self._fileTypeVersion))
            for stubb in self._stubbs:
                self._writeStubb(handler, stubb, whitespace=10)

    def getKeys(self):
        return [stubb.stubbName for stubb in self._stubbs]

    def getValues(self):
        return [stubb.value for stubb in self._stubbs]

    def getValue(self, key):
        try:
            return self.getByStubbName(key).value
        except AttributeError:
            raise exceptions.MissingStubbs([key], "prj", filePath=self.filePath())

    def setValue(self, key, value):
        stubb = self.getByStubbName(key)
        if stubb is None:
            stubb = self._addStubb(key)

        self.getByStubbName(key).value = value

    def isSingleBone(self):
        """Does the .prj have only a single bone?

        Returns:
            bool
        """
        return FileStubNames.CHARMAP_PROJECT not in self.getKeys()


if __name__ == "__main__":
    import sys
    from PyQt4 import QtGui

    app = QtGui.QApplication(sys.argv)

    prj = GiantPrj(
        "/media/giant_capture_data/meridian/capture/takes/2020_0205_Session0095/000488_01_TO_SUPPRESSIVE_AIM_TRANS_HANDSWAP_LF_RH_TO_LF_LH_HANDGUN.prj"
    )
    print(prj.getValue("RTTRACK_PARAMS")) #  Will throw error if there's no RTTRACK_PARAMS
    print(prj.getValue("FAKE_VALUE"))  # Will throw error
