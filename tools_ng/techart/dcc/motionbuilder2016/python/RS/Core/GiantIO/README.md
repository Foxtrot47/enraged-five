# GiantIO


# *** WARNING ***

This module was ported ad hoc from rockstarGiant.giantIO in the TA depot specifically for use in the Virtual Production Toolbox.
Therefore, it is possible that some features will not work in this codebase!

This will be needed until giantIO becomes a pip installable dependency for use in MotionBuilder.

This package was also ported over with RS.Core.PinPoint, so the same issues may apply there.

Additionally, some portions of the code differ from the TA depot versions in order to reduce import issues. We
removed some runners in the code because imports from the TA depot are not allowed here.

# *** END WARNING ***
