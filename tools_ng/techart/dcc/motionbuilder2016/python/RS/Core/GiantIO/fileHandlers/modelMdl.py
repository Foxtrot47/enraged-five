"""
Class to handle the reading and writing of Model MDL file formats
"""
from RS.Core.GiantIO.fileHandlers import giantBase


class IncludedModelStubb(object):
    """
    Class to represent an included model
    """
    def __init__(self, stubb):
        """
        Constructor

        args:
            stubb (GiantStubb): The stubb to wrap
        """
        super(IncludedModelStubb, self).__init__()

        self._fileName = ""
        self._bodyIdOffset = 0
        self._bodyNameSuffix = ""
        self._stubb = stubb

        self._loadData()

    def getFileName(self):
        """
        Get the file name to include

        return:
            filepath string to use
        """
        return self._fileName

    def setFileName(self, value):
        """
        set the file name to include

        args:
            value(str): file path string to include
        """
        self._fileName = value

    def getBodyIdOffset(self):
        """
        Get the body ID Offset value

        return:
            int of the body ID offset
        """
        return self._bodyIdOffset

    def setBodyIdOffset(self, value):
        """
        set the body ID Offset value

        args:
            value(int): the body ID offset
        """
        self._bodyIdOffset = value

    def getBodyNameSuffix(self):
        """
        get the body name suffix value

        return:
            string of the body name suffix
        """
        return self._bodyNameSuffix

    def setBodyNameSuffix(self, value):
        """
        set the body name suffix

        args:
            value(string): the body name suffix
        """
        self._bodyNameSuffix = value

    def _loadData(self):
        """
        Internal Method

        Load the data from the stubb into the class
        """
        self._fileName = self._stubb.getByStubbName("FILE_NAME").value
        self._bodyIdOffset = self._stubb.getByStubbName("BODY_ID_OFFSET").value
        self._bodyNameSuffix = self._stubb.getByStubbName("BODY_NAME_SUFFIX").value

    def writeToStubb(self):
        """
        Write the data from the class onto the underlying stubb so it can be written to a file.
        This does not write to disk.
        """
        self._stubb.getByStubbName("FILE_NAME").value = self._fileName
        self._stubb.getByStubbName("BODY_ID_OFFSET").value = str(self._bodyIdOffset)
        self._stubb.getByStubbName("BODY_NAME_SUFFIX").value = self._bodyNameSuffix

    def getStubb(self):
        """
        Get the stubb which is being wrapped

        return:
            GiantStubb that is wrapped
        """
        return self._stubb


class BodyStubb(giantBase.GiantStubb):
    """
    Class to represent a top level body object
    """
    def __init__(self, stubbName, value=None):
        super(BodyStubb, self).__init__(stubbName, value=value)

    def name(self):
        """
        Get the name of the body

        return:
            string name of the body
        """
        return self.getByStubbName("NAME").value

    def setName(self, newName):
        """
        set the name of the body

        args:
            newName (str): The new name of the body
        """
        self.getByStubbName("NAME").value = newName

    def ID(self):
        """
        Get the ID of the body

        return:
            ID of the body as an int
        """
        return self.getByStubbName("ID").value

    def setID(self, newID):
        """
        Set the ID of the body

        args:
            newID (int): The new ID of the body
        """
        self.getByStubbName("ID").value = newID

    def parentName(self):
        """
        Get the name of the parent body

        return:
            string name of the parent body
        """
        return self.getByStubbName("PARENT_NAME").value

    def setParentName(self, newParentName):
        """
        Set the name of the parent body

        args:
            newParentName (str): The name of the new parent body
        """
        self.getByStubbName("PARENT_NAME").value = newParentName

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{}('{}' ID:{})".format(self.__class__.__name__, self.name(), self.ID())


class VertStubb(giantBase.GiantStubb):
    """
    Class to represent a top level vert object
    """
    def __init__(self, stubbName, value=None):
        super(VertStubb, self).__init__(stubbName, value=value)

    def name(self):
        """
        Get the name of the vert

        return:
            string name of the vert
        """
        return self.getByStubbName("NAME").value

    def setName(self, newName):
        """
        Get the name of the vert

        args:
            newName (str): The new name of the vert
        """
        self.getByStubbName("NAME").value = newName

    def coordinates(self):
        """
        Get the coordinates of all the verts

        return:
            list of string of the coordinates of the verts
        """
        return self.getByStubbName("COORDINATES").value

    def clearCoordinates(self):
        """
        Clear all the vert coordinates
        """
        self.getByStubbName("COORDINATES").value = []

    def addCoordinate(self, coordinate):
        """
        Add a single new coordinate to the list of coordinates

        args:
            coordinate (str): The coordinates to add
        """
        self.getByStubbName("COORDINATES").value.append(coordinate)

    def addCoordinates(self, coordinates):
        """
        Add new coordinates to the list of coordinates

        args:
            coordinates (list of str): The coordinates to add
        """
        self.getByStubbName("COORDINATES").value.extend(coordinates)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{}('{}')".format(self.__class__.__name__, self.name())


class GeoStubb(giantBase.GiantStubb):
    """
    Class to represent a top level geometry object
    """
    def __init__(self, stubbName, value=None):
        super(GeoStubb, self).__init__(stubbName, value=value)

    def body(self):
        """
        Get the body the geo is for

        return:
            string name of the body
        """
        return self.getByStubbName("BODY").value

    def setBody(self, newBody):
        """
        Set the body the geo is for

        args:
            newBody (str): The name of the body to use
        """
        self.getByStubbName("BODY").value = newBody

    def vertices(self):
        """
        Get the vert name that geo is made up of

        return:
            string name of the vert
        """
        vertStubb = self.getByStubbName("VERTICES")
        if vertStubb is None:
            return None
        return vertStubb.value

    def setVertices(self, newVert):
        """
        Set the vert name that geo is made up of

        args:
            newVert (str): The name of the vert to use
        """
        self.getOrCreateByStubbName("VERTICES").value = newVert

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{}('{}')".format(self.__class__.__name__, self.body())


class ModelMdl(giantBase.GiantFileBase):
    """
    Class to load in a Giant MDL file so its easy to read
    """
    def __init__(self, filePath=None):
        """
        Constructor

        kwargs:
            filePath (str): File path to the file to read in
        """
        super(ModelMdl, self).__init__(filePath=filePath)
        self._fileType = "Bio Graphic Model File"
        self._fileTypeVersion = "v2.00"
        if filePath is None:
            self._createStruct()

        self._includedModels = [IncludedModelStubb(model) for model in self.getAllByStubbName("INCLUDE")]

    def generateGiantStubb(self, stubbName, value=None, parent=None):
        """
        Virtual method

        Reimplement to allow for subclasses to use specific version of the GiantStubb class for added
        functionality
        """
        # Only deal with 'top level' assets
        if parent is not None:
            return super(ModelMdl, self).generateGiantStubb(stubbName, value=value, parent=parent)

        stubbType = giantBase.GiantStubb
        if stubbName == "BODY":
            stubbType = BodyStubb
        elif stubbName == "VERTICES":
            stubbType = VertStubb
        elif stubbName == "GEOMETRY":
            stubbType = GeoStubb
        return stubbType(stubbName, value=value)

    def _createStruct(self):
        """
        Internal Method

        Create a struct needed in the file for new files
        """
        newStubb = self._addStubb("UNITS")

        newStubb.addNewChild("LENGTH", value="1 INCHES")
        newStubb.addNewChild("ANGLE", value="1 RADIANS")

    def getIncludedModels(self):
        """
        Get all the included models in the file

        return:
            list of all the included models as IncludedModelStubb
        """
        return self._includedModels

    def addIncludedModel(self):
        """
        Add a new included model to the file

        return:
            the newly created model as a IncludedModelStubb
        """
        newStubb = self._addStubb("INCLUDE")

        newStubb.addNewChild("FILE_NAME", value="")
        newStubb.addNewChild("BODY_ID_OFFSET", value="")
        newStubb.addNewChild("BODY_NAME_SUFFIX", value="")

        newBody = IncludedModelStubb(newStubb)
        self._includedModels.append(newBody)
        return newBody

    def getBodies(self):
        """
        Get all the bodies in the MDL

        return:
            list of all the bodies as BodyStubb instances
        """
        return [stubb for stubb in self.getStubbs() if isinstance(stubb, BodyStubb)]

    def getGeometry(self):
        """
        Get all the Geometry in the MDL

        return:
            list of all the Geometry as GeoStubb instances
        """
        return [stubb for stubb in self.getStubbs() if isinstance(stubb, GeoStubb)]

    def getVertices(self):
        """
        Get all the vertices in the MDL

        return:
            list of all the vertices as VertStubb instances
        """
        return [stubb for stubb in self.getStubbs() if isinstance(stubb, VertStubb)]

    def getVerticeByName(self, name):
        """
        Get a vertice by its name

        return:
            single vert with the same name as a VertStubb instances
        """
        for vert in self.getVertices():
            if vert.name() == name:
                return vert
        return None

    def getGeometryByBodyName(self, bodyName):
        """
        Get a Geometry by its body name

        args:
            bodyName (str): The body name to use to get the geo

        return:
            single geo with the same body name as a GeoStubb instances
        """
        for geo in self.getGeometry():
            if geo.body() == bodyName:
                return geo
        return None

    def getGeometryByBody(self, body):
        """
        Get geometry by a body

        args:
            body (BodyStubb): The body to get the geo for

        return:
            The geomery as a GeoStubb instances
        """
        for geo in self.getGeometry():
            if geo.body() == body.name():
                return geo
        return None

    def getBodyByName(self, name):
        """
        Get a Body by its body name

        args:
            name (str): The name of the body to get

        return:
            single body as a BodyStubb instances
        """
        for body in self.getBodies():
            if body.name() == name:
                return body
        return None

    def writeFile(self, filePath=None):
        """
        ReImplemeted

        Write to a new file or update the currently loaded in file

        kwargs:
            filePath (str): The file path to write to
        """
        for model in self.getIncludedModels():
            model.writeToStubb()
        super(ModelMdl, self).writeFile(filePath=filePath)
