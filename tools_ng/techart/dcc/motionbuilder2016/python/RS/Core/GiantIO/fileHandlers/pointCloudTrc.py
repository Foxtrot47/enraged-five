import os


class Vector3(object):
    def __init__(self, newX=0.0, newY=0.0, newZ=0.0):
        self._x = newX
        self._y = newY
        self._z = newZ

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def z(self):
        return self._z

    @x.setter
    def x(self, value):
        self._x = value

    @y.setter
    def y(self, value):
        self._y = value

    @z.setter
    def z(self, value):
        self._z = value


class MarkerData(object):
    def __init__(self, name):
        self._name = name
        self._data = {}

    def getName(self):
        return self._name

    def setName(self, value):
        self._name = value

    def setData(self, frame, x, y, z):
        self._data[frame] = Vector3(x, y, z)

    def getData(self, frame):
        return self._data.get(frame)


class PointCloudTrc(object):
    def __init__(self, filePath=None):
        super(PointCloudTrc, self).__init__()
        self._filePath = None

        self._header = {}
        self._markerData = []
        self._lastFrame = 0

        if filePath is not None:
            self.readFile(filePath)

    def getLastFrame(self):
        return self._lastFrame

    def getMarkers(self):
        return self._markerData

    def readFile(self, filePath):
        """
        Read a file in given file path

        args:
            filePath (str): File path of file to read in
        """
        self._frameData = []
        if not os.path.isfile(filePath):
            raise ValueError("Unable to read file. '{0}' is not a file".format(filePath))
        self._filePath = filePath
        handler = open(filePath)
        self._readLine(handler) # Ingore this
        fileHeaderNames = self._readLine(handler).split("\t") # header names
        fileHeaderValues = self._readLine(handler).split("\t") # header values

        for headerName, headerValue in zip(fileHeaderNames, fileHeaderValues):
            self._header[headerName] = self._convertValue(headerValue)

        sectionNames = [sec for sec in self._readLine(handler).split("\t") if sec not in ("", "Frame#", "Time")]

        for part in sectionNames:
            newMrk = MarkerData(part)
            self._markerData.append(newMrk)

        self._readLine(handler) # Ingore this

        while(True):
            line = self._readLine(handler)
            if line == "":
                break
            values = [self._convertValue(val) for val in line.split("\t")]
            frameNumber = values.pop(0)
            _ = values.pop(0) # this is the "time" (seems to be dataRate/frame)
            for idx in xrange(len(self._markerData) - 1):
                mrk = self._markerData[idx]
                mrk.setData(frameNumber, values[(idx * 3)], values[(idx * 3) + 1], values[(idx * 3) + 2])

            self._lastFrame = frameNumber
        handler.close()

    def _convertValue(self, strValue):
        """
        Internal Method

        Convert a string to a float or int if possible, otherwise return back the string

        args:
            strValue (str): the input string value

        returns:
            an int, float or string depending if it can be converted
        """
        try:
            newVal = int(strValue)
            return newVal
        except:
            pass
        try:
            newVal = float(strValue)
            return newVal
        except:
            pass
        return strValue


    def _readLine(self, handler):
        """
        Internal Method

        Read a single line off a file and strip it so it can be read just for values
        """
        newLine = handler.readline()
        newLine = newLine.strip()
        return newLine

