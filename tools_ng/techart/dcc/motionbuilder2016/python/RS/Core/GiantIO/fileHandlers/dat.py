import os

from RS.Core.GiantIO.fileHandlers import giantBase


class GiantDat(giantBase.GiantFileBase):
    """
    Class to load in a Giant dat file so its easy to read
    """
    def __init__(self, filePath=None):
        super(GiantDat, self).__init__(filePath=filePath)

    def readFile(self, filePath):
        """
        ReImplemented

        Read a file in given file path

        args:
            filePath (str): File path of file to read in
        """
        if not os.path.isfile(filePath):
            raise ValueError("Unable to read file. '{0}' is not a file".format(filePath))
        self._filePath = filePath
        handler = open(filePath)

        # Giant sometimes pads out its header and body data
        for _ in xrange(20):
            currentPos = handler.tell()
            line = self._readLine(handler)
            if line != "":
                handler.seek(currentPos)
                break

        while(True):
            try:
                newStubb = self._readStubb(handler)
                if newStubb is not None:
                    self._stubbs.append(newStubb)
            except:
                break
        handler.close()

    def writeFile(self, filePath=None):
        """
        ReImplemented

        Read a file in given file path

        args:
            filePath (str): File path of file to read in
        """
        with open(filePath or self._filePath, 'w') as handler:
            for stubb in self._stubbs:
                self._writeStubb(handler, stubb, whitespace=10)

    def getKeys(self):
        return [stubb.stubbName for stubb in self._stubbs]

    def getValues(self):
        return [stubb.value for stubb in self._stubbs]

    def getValue(self, key):
        return self.getByStubbName(key).value

    def setValue(self, key, value):
        stubb = self.getByStubbName(key)
        if stubb is None:
            stubb = self._addStubb(key)

        stubb.value = value
