"""
Class to handle the reading and writing of the calibration (.cal) file format.
"""
import os

from RS.Core.GiantIO import const
from RS.Core.GiantIO.fileHandlers import giantBase


class Calibration(giantBase.GiantFileBase):
    """
    Class to load in a Giant CAL file so its easy to read

    Notes:
        Realtime doesn't allow us to store custom stubbs. To workaround this, we wedge in comments with a prefix.

    Class Attrs:
        METADATA_TAG: The prefix for comments added below the file header intended to be metadata key: value pairs.
    """
    METADATA_TAG = "#RSMETA"  # Might move this to a new base class if other formats require it later on.

    def __init__(self, filePath=None):
        self._metadata = {}
        super(Calibration, self).__init__(filePath=filePath)

    def contextId(self):
        """
        Get the animdata context's ID from the file's Rocsktar metadata.

        Returns:
            int or None
        """
        value = self._metadata.get(const.MetaDataKeys.CONTEXT, None)
        return int(value) if value is not None else None

    def setContextId(self, contextId):
        """
        Set the animdata context ID.

        Args:
            contextId (int): The animdata context's ID (as listed in the database).
        """
        if contextId is None and const.MetaDataKeys.CONTEXT in self._metadata.keys():
            self._metadata.pop(const.MetaDataKeys.CONTEXT)
            return
        self._metadata[const.MetaDataKeys.CONTEXT] = contextId

    def _readStubb(self, handler, parent=None):
        """
        Reimplemented from giantBase.GiantFileBase to allow for reading of metadata.

        Read a stubb of the giant file.

        Args:
            handler (file handler): the handler for the file

        Keyword Args:
            parent (GiantStubb): The parent stubb if nested
        """
        line = self._readLine(handler)

        if line == "":
            raise ValueError()

        if line.startswith(self.METADATA_TAG):
            _, key, value = line.split(" ")
            self._metadata[key] = value

        if "}" in line:
            return True

        if ":" in line:
            name, value = line.split(":", 1)
            name = name.strip()
            newStubb = self.generateGiantStubb(name, self._convertValue(value), parent=parent)
            if parent is not None:
                parent.addChild(newStubb)
            else:
                self._stubbs.append(newStubb)
            return

        if "{" in line:
            name, _ = line.split("{", 1)
            name = name.strip()
            newStubb = self.generateGiantStubb(name, parent=parent)
            newStubb.value = []
            if parent is not None:
                parent.addChild(newStubb)
            else:
                self._stubbs.append(newStubb)
            while (True):
                if self._readStubb(handler, parent=newStubb) == True:
                    return
            return

        if parent is None:
            return
        value = self._convertValue(line)

        if parent.value is None:
            parent.value = value
        elif not isinstance(parent.value, list):
            parent.value = [parent.value, value]
        else:
            parent.value.append(value)

    def writeFile(self, filePath=None):
        """
        Reimplemented from giantBase.GiantFileBase to allow for writing of metadata.

        Write to a new file or update the currently loaded in file

        Keyword Args:
            filePath (str): The file path to write to
        """
        with open(filePath or self._filePath, 'w') as handler:
            handler.writelines("%s\n" % (self._fileType))
            handler.writelines("%s\n" % (self._fileTypeVersion))

            for key in sorted(self._metadata.keys()):
                handler.writelines("%s %s %s\n" % (self.METADATA_TAG, key, self._metadata[key]))

            for stubb in self._stubbs:
                self._writeStubb(handler, stubb)


if __name__ == '__main__':
    import pprint

    path = os.path.join(const.Resources.TESTS, "SD_StudioA.32cam.2019-11-20_16-19.calibration.cal")
    cal = Calibration(str(path))
    print(cal)
    print(repr(cal))
    print(cal.contextId())
    pprint.pprint(cal.getStubbs())
