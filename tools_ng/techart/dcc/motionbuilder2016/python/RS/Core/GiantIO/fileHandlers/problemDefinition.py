"""
Class to handle the reading and writing of Problem Definition (.pdf) file formats
"""
import os
import re

from RS.Core.GiantIO.fileHandlers import giantBase


class DataReadError(Exception):
    pass


class ProblemVariable(object):
    """
    Class to represent a variable
    """
    def __init__(self, index, description):
        super(ProblemVariable, self).__init__()
        """
        Args:
            index (int): The variable index
            description (str): The description of the variable
        """
        self._index = index
        self._description = description

    def description(self):
        """
        Get the description of the variable

        return:
            String description of the variable
        """
        return self._description

    def setDescription(self, value):
        """
        set the file name to include

        args:
            value(str): the description of the variable
        """
        self._description = value

    def index(self):
        """
        Get the variable index

        return:
            int of the variable
        """
        return self._index

    def setIndex(self, value):
        """
        set the variable index

        args:
            value(int): the index of the variable
        """
        self._index = value


class Body(object):
    """
    Represent a Body object
    """
    def __init__(self, bodyName, parentName, graphicsType, bodyType):
        """
        Constructor

        args:
            bodyName(str): the name of the body
            parentName(str): the name of the parent body
            graphicsType(int): the graphics type
            bodyType(int): the type of body

        """
        super(Body, self).__init__()
        self._bodyName = bodyName
        self._parentName = parentName
        self._graphicsType = graphicsType
        self._bodyType = bodyType
        self._transformOrder = []
        self._transformVars = {}

    def setBodyName(self, value):
        """
        The name of the body

        args:
            value(str): The name of the body
        """
        self._bodyName = value

    def bodyName(self):
        """
        Get the name of the body

        return:
            str of the body's name
        """
        return self._bodyName

    def bodyType(self):
        """
        Get the type of the body

        return:
            int of the body's type
        """
        return self._bodyType

    def setBodyType(self, value):
        """
        Set the type of the body

        args:
            value(int): The body's type
        """
        self._bodyType = value

    def graphicsType(self):
        """
        Get the graphics type of the body

        return:
            int of the body's graphics type
        """
        return self._graphicsType

    def setGraphicsType(self, value):
        """
        Set the graphics type of the body

        args:
            value(int): The body's graphics type
        """
        self._graphicsType = value

    def setParentName(self, value):
        """
        The name of the body's parent

        args:
            value(str): The name of the bodies parent
        """
        self._parentName = value

    def parentName(self):
        """
        Get the name of the parent body

        return:
            str of the body's parent name
        """
        return self._parentName

    def setTransformOrder(self, value):
        """
        set the transform order of the body

        args:
            value(list): A list of TransformOrder values to make up the transform order
        """
        self._transformOrder = value

    def getTransformOrder(self):
        """
        get the transform order of the body

        return:
            list of TransformOrder values
        """
        return self._transformOrder

    def getVariableCount(self):
        """
        Get the amount of transform values in the body

        return:
            int the number of transform values
        """
        return len([num for num in self._transformVars.values() if num != 0])

    def getTransformVar(self, coords):
        """
        Get the transform variable number for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to get the variable for

        return:
            int of the variable number, or 0 if none is set
        """
        return self._transformVars.get(coords, 0)

    def setTransformVar(self, coords, value):
        """
        set the transform variable number for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to set the variable for
            value(int): the variable number for the body coordinate
        """
        self._transformVars[coords] = value


class IncludedBodyStubb(object):
    """
    Class to represent an included body
    """
    def __init__(self, stubb):
        super(IncludedBodyStubb, self).__init__()
        """
        Constructor

        args:
            stubb (GiantStubb): The stubb to wrap
        """
        self._fileName = ""
        self._bodyNameSuffix = ""
        self._bodyIdOffset = 0
        self._stubb = stubb

        self._loadData()

    def getFileName(self):
        """Get FILE_NAME from the pdf file.

        Returns:
            str: file name set of the stubb
        """
        return self._fileName

    def setFileName(self, value):
        """Set FILE_NAME in the pdf file.

        Args:
            value(str): file path string to include
        """
        self._fileName = value

    def getBodyIdOffset(self):
        """Get BODY_ID_OFFSET from the pdf file.
        This value is based on the order that the bodies appear under the refine in the Nuance Scene Browser.
        E.g. an offset of 8 means it is the ninth body in the list, which starts at 0.

        Returns:
            int
        """
        return self._bodyIdOffset

    def setBodyIdOffset(self, value):
        """Set BODY_ID_OFFSET in the pdf file.

        Args:
            value (int): the body ID offset
        """
        self._bodyIdOffset = value

    def getBodyNameSuffix(self):
        """Get BODY_NAME_SUFFIX in the pdf file.
        This is the suffix which appears after the name of the bone.

        Returns:
            str: the suffix, such as _a in RCam_Slate_bone_a
        """
        return self._bodyNameSuffix

    def _loadData(self):
        """Load the data from the stubb into the class"""
        self._fileName = self._stubb.getByStubbName("FILE_NAME").value
        self._bodyNameSuffix = self._stubb.getByStubbName("BODY_NAME_SUFFIX").value
        self._bodyIdOffset = int(self._stubb.getByStubbName("BODY_ID_OFFSET").value)


class ProblemDefinition(giantBase.GiantFileBase):
    """
    Giant File Base for reading and writing Giant PDF files.
    """
    def __init__(self, filePath=None):
        """
        Args:
            filePath (str): File path to read in if given
        """
        super(ProblemDefinition, self).__init__()
        self._filePath = None
        self._fileType = "Bio Problem Definition File"
        self._fileTypeVersion = "v1.00"

        self._vars = []
        self._bodies = []
        self._realPoints = []
        self._fixPoints = []
        self._doublePoints = []
        self._artificialPoints = []

        self._includedBodies = []

        if filePath is not None:
            self.readFile(filePath)

    def readFile(self, filePath):
        """
        Read a file in given file path

        args:
            filePath (str): File path of file to read in
        """
        self._frameData = []
        if not os.path.isfile(filePath):
            raise ValueError("Unable to read file. '{0}' is not a file".format(filePath))
        self._filePath = filePath
        handler = open(filePath)
        self._fileType = self._readLine(handler)
        self._fileTypeVersion = self._readLine(handler)

        varCount = int(self._readLine(handler))  # number of vars in each body
        self._readVars(handler, varCount)

        bodyCount = int(self._readLine(handler))  # number of bodies in the file
        self._readBodies(handler, bodyCount)

        realPointCount = int(self._readLine(handler))  # number of realPoints in the file
        self._readRealPoints(handler, realPointCount)

        fixPointCount = int(self._readLine(handler))  # number of fix points in the file
        if fixPointCount != 0:
            raise ValueError("Unable to read file with fixed points")

        doublePointCount = int(self._readLine(handler))  # number of double in the file
        if doublePointCount != 0:
            raise ValueError("Unable to read file with double points")

        artificalPointCount = int(self._readLine(handler))  # number of artificial pairs the file
        if artificalPointCount != 0:
            raise ValueError("Unable to read file with artificial pairs")

        while(True):
            try:
                self._readStubb(handler)
            except ValueError:
                break

        for stubb in self.getAllByStubbName("INCLUDE"):
            self._includedBodies.append(IncludedBodyStubb(stubb))

        handler.close()

    def includedBodies(self):
        """The INCLUDE stubs provided in the pdf file.
        Pdf files in the probspec folder do not have this stubb.
        """
        return self._includedBodies

    def vars(self):
        """Get each variable defined in the problem definition.

        Variables are the body names + the translation or rotation name.

        Ex. var description: r_toes.ry

        Returns:
            list: dataStructures.problem.ProblemVariable
        """
        return self._vars

    def bodies(self):
        """Get each body part defined in the problem definition.

        Bodies are selectable portions of the character or prop that each
        have their own translation/rotation values.

        Ex. name of a body: r_low_fing_5

        Returns:
            list: dataStructures.problem.Body
        """
        return self._bodies

    def realPoints(self):
        """Returns the RealPoints in the pdf, which contain markerIDs, marker weights, and bodies they are driving.

        Returns:
            list: dataStructures.problem.RealPoint
        """
        return self._realPoints

    def _readVars(self, handler, numberOfVars):
        """
        Internal Method

        Read all the vars from the file
        """
        for _ in xrange(numberOfVars):
            line = self._readLine(handler)
            varIdx, limitType, description = line.strip().split(" ", 2)
            varIdx = int(varIdx)
            description = description.strip()
            self._vars.append(ProblemVariable(varIdx, description))

    def _readBodies(self, handler, numberOfBodies):
        """
        Internal Method

        Read all the bodies from the file
        """
        regexStr = "^([ ]|)(?P<BoneName>([a-zA-Z0-9_\.]{1,20})|(\"[a-zA-Z0-9_ \.]+\"))([ ]+|)(?P<ParentName>([a-zA-Z0-9_\.]{1,20})|(\"[a-zA-Z0-9_ \.]+\"))[ ]+(?P<GraphicsType>[\d]+)[ ]+(?P<BodyType>[\d]+)$"
        for _ in xrange(numberOfBodies):
            line = self._readLine(handler)

            results = re.match(regexStr, line)
            if results is None:
                raise DataReadError("Unable to parse BODY line '{}'".format(line))

            dataDict = results.groupdict()
            newBody = Body(dataDict["BoneName"].replace('"', ""),
                 dataDict["ParentName"].replace('"', ""),
                 int(dataDict["GraphicsType"]),
                 int(dataDict["BodyType"])
            )

            # trx
            trxLine = self._readLine(handler)
            transformOrder = [int(num) for num in trxLine.split(" ") if num != ""]
            newBody.setTransformOrder(transformOrder)

            for idx in xrange(len(transformOrder)):
                line = self._readLine(handler)
                if line.startswith("-"):
                    for _ in xrange(abs(int(line))):
                        self._readLine(handler)
                    var = 0
                else:
                    var = int(line)
                newBody.setTransformVar(idx + 1, var)

            # Body Type of 1 has an extra line of data. not 100% what it does yet
            if newBody.bodyType() == 1:
                line = self._readLine(handler)

            self._bodies.append(newBody)

    def _readRealPoints(self, handler, numberOfRealPoints):
        """
        Internal Method

        Read all the bodies from the file
        """
        regexStr = "^([ ]|)(?P<PointIdx>[\d]+)[ ]+(?P<PredType>[\d]+)[ ]+(?P<Weight>[\d.]+)[ ]+(?P<BodyName>([a-zA-Z0-9_ \.]{1,20})|(\"[a-zA-Z0-9_ \.]+\"))$"
        for _ in xrange(numberOfRealPoints):
            line = self._readLine(handler)

            results = re.match(regexStr, line)
            if results is None:
                raise DataReadError("Unable to parse REAL POINT line '{}'".format(line))

            dataDict = results.groupdict()

            bodyName = dataDict["BodyName"].replace('"', "")
            pointIdx = int(dataDict["PointIdx"])
            predType = int(dataDict["PredType"])
            weight = float(dataDict["Weight"])

            # Make this into a class when we start using it more
            self._realPoints.append([pointIdx, predType, weight, bodyName])

    def _readLine(self, handler, splitCommentLine=True):
        """
        Internal Method

        Read a single line off a file and strip it so it can be read just for values
        """
        newLine = handler.readline()
        if splitCommentLine is True:
            newLine = newLine.split("#")[0]
        newLine = newLine.strip()
        return newLine

