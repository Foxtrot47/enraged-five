"""Convenience methods for working with Giant's file path formatting.

For performance reasons, the variable names are hardcoded.

Tests are in giantIO/tests.
"""
import os
import platform
import re

# In rockstar.corePy.decorators, file name is called memoize and the method name is memoize.
from RS.PyCore.Decorators import memorize as memoize
from RS import Perforce
from RS.Core.GiantIO.utils import giantEnv
from RS.Core.GiantIO import exceptions


_TO_FILESYSTEM_PATH_PATTERN = re.compile("\$GIANT_DATA_DIR|\$BIO_DATA_DIR|\$NU_DATA_DIR")


@memoize.memoized
def getRootDirFromDepotRootBasename(depotRootBasename):
    """Get the local filesystem root directory from Mocap depot root directory's basename.

    Args:
        depotRootBasename (str): The basename of the project's mocap depot root.

    Returns:
        str
    """
    if platform.system() == "Linux":
        # TODO move the base dir for this to a const
        rootPath = os.path.join("/media", "giant_capture_data", depotRootBasename)
    elif platform.system() == "Windows":
        p4ProjectPath = "//depot/projects/{}".format(depotRootBasename)
        data = Perforce.Where([p4ProjectPath])
        try:
            rootPath = data[0]["path"]
        except Exception:
            raise ValueError("Unable to find local P4 path for {}".format(p4ProjectPath))
    return rootPath


@memoize.memoized
def generateSubstituteDict(depotRootBasename):
    """Create a dictionary with all the Giant env vars and the directory path to substitute them with.

    Args:
        depotRootBasename (str): The basename of the project's mocap depot root.

    Returns:
        dict
    """
    rootPath = getRootDirFromDepotRootBasename(depotRootBasename)
    return {"$BIO_DATA_DIR": rootPath, "$GIANT_DATA_DIR": rootPath, "$NU_DATA_DIR": rootPath}


def getRootDirFromEnv(giantEnvVar=None):
    """Get the local filesystem root directory from one of the Giant environment variables.

    Args:
        giantEnvVar (str, optional): The Giant environment variable to search for.

    Returns:
        str: The Unix formatted path to the root directory.
    """
    if giantEnvVar is None:
        giantEnvVar = "BIO_DATA_DIR"

    rootDir = os.getenv(giantEnvVar, None) or os.getenv("GIANT_DATA_DIR", None)
    if rootDir is None:
        msg = "Could not find environment variable: {}. Check if the environment was set correctly!".format(giantEnvVar)
        raise exceptions.GiantEnvironmentNotSetError(msg)

    return rootDir


def getProjectCodeNameFromEnv(rootPath=None):
    """Get the project code name (AKA: "depot root basename") from one of the Giant environment variables.

    Notes:
        * Returns the formatting required for a file system path not the database, e.g. "code_name" vs "Code Name".
        * Ideal for getting the project code token/pattern for migration tasks.

    Args:
        rootPath (str, optional): The path to the root directory.

    Returns:
        str: The project's code name, e.g. "code_name".
    """
    if rootPath is None:
        rootPath = getRootDirFromEnv()
    if rootPath:
        return giantEnv.DataDirs.tokenize(rootPath)[1]


def toUnixPath(giantPath, rootDir=None):
    """Reformat a Giant formatted file path to a Unix formatted file path.

    Args:
        giantPath (str): The Giant formatted path that needs reformatted.
        rootDir (str, optional): The sub-string to replace the Giant sub-string with.

    Returns:
        str: The Unix formatted path.
    """
    if rootDir is None:
        rootDir = getRootDirFromEnv(giantEnvVar=None)

    substituteDict = generateSubstituteDict(os.path.basename(rootDir))
    for oldSub, newSub in substituteDict.iteritems():
        giantPath = giantPath.replace(oldSub, newSub)
    return giantPath


@memoize.memoized
def toFileSystemPath(giantPath, rootDir):
    """Reformat a Giant formatted file path to a file path for the current OS.

    Args:
        giantPath (str): The Giant formatted path.
        rootDir (str): The Unix/Windows formatted path that needs reformatted.

    Returns:
        str
    """
    fsPath = _TO_FILESYSTEM_PATH_PATTERN.sub(rootDir.encode("string-escape"), giantPath)
    return os.path.normpath(fsPath)


def toGiantPath(fileSystemPath, rootDir=None, giantVar=None):
    """Reformat a file system path to a Giant formatted file path.

    Args:
        fileSystemPath(str): The filesystem formatted path that needs reformatted.
        rootDir (str, optional): The filesystem directory sub-string to replace. If None, the env is searched to find
            the dir path.
        giantVar (str, optional): The Giant environment variable to replace the rootDir sub-string with.

    Raises:
        ValueError: If the filesystem path doesn't start with the provided root directory.

    Returns:
        str: The Giant formatted path.
    """
    if giantVar is None:
        giantVar = "$BIO_DATA_DIR"
    elif not giantVar.startswith("$"):
        giantVar = "${}".format(giantVar)

    if rootDir is None:
        rootDir = getRootDirFromEnv(giantEnvVar=None)

    if not fileSystemPath.startswith(rootDir):
        raise ValueError("The filesystem path '{}' must start with the root dir '{}'".format(fileSystemPath, rootDir))

    return fileSystemPath.replace(rootDir, giantVar).replace("\\", "/")  # Always convert to unix style slashes.


if __name__ == "__main__":
    # Setup env for demo.
    baseDir = "/media/giant_capture_data"
    if platform.system() == "Windows":
        baseDir = r"x:\projects"
    rootDir = os.path.join(baseDir, "bob")
    os.environ["BIO_DATA_DIR"] = rootDir

    # Demo functions. Examples copied from unittests.
    giantPath = "$BIO_DATA_DIR/dof/male_8.2.dof"
    windowsPath = r"x:\projects\bob\dof\male_8.2.dof"
    unixPath = "/media/giant_capture_data/bob/dof/male_8.2.dof"
    pathMappings = {"Windows": windowsPath, "Linux": unixPath}
    fsPath = pathMappings[platform.system()]
    print(toUnixPath(giantPath, rootDir))
    print(toFileSystemPath(giantPath, rootDir))
    print(toGiantPath(fsPath, rootDir=rootDir))
