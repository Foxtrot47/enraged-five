"""
Class to handle the reading and writing of DOF file formats
"""
import os

from RS.Core.GiantIO.fileHandlers import giantBase


class TransformOrder(object):
    """
    Enum class to represent the transforms
    """
    TX = 1
    TY = 2
    TZ = 3
    RX = 4
    RY = 5
    RZ = 6


class DofBody(object):
    """
    Represent a DoF Body object
    """
    def __init__(self, bodyId):
        """
        Constructor

        args:
            bodyId(int): the bodies ID number
        """
        self._bodyId = bodyId
        self._transformOrder = []
        self._transformVars = {}
        self._transformEqs = {}

    def setBodyId(self, value):
        """
        set the ID of the body

        args:
            newId(int): The ID of the body
        """
        self._bodyId = value

    def getBodyId(self):
        """
        Get the ID of the body

        return:
            int of the body's ID
        """
        return self._bodyId

    def setTransformOrder(self, value):
        """
        set the transform order of the body

        args:
            value(list): A list of TransformOrder values to make up the transform order
        """
        self._transformOrder = value

    def getTransformOrder(self):
        """
        get the transform order of the body

        return:
            list of TransformOrder values
        """
        return self._transformOrder

    def getVariableCount(self):
        """
        Get the amount of transform values in the body

        return:
            int the number of transform values
        """
        return len([num for num in self._transformVars.values() if num != -1])

    def getTransformVar(self, coords):
        """
        Get the transform variable number for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to get the variable for

        return:
            int of the variable number, or -1 if none is set
        """
        return self._transformVars.get(coords, -1)

    def setTransformVar(self, coords, value):
        """
        set the transform variable number for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to set the variable for
            value(int): the variable number for the body coordinate
        """
        self._transformVars[coords] = value

    def getTransformEquation(self, coords):
        """
        get the transform equation for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to set the variable for

        returns:
            LinearCombinationEquation for the body coordinate
        """
        return self._transformEqs.get(coords, None)

    def setTransformEquation(self, coords, equation):
        """
        set the transform equation for a given transform coordinate of the body

        args:
            coords(TransformOrder): the coords to set the variable for
            equation(LinearCombinationEquation): the equation for the body coordinate
        """
        self._transformEqs[coords] = equation
        self.setTransformVar(coords, -1)


class Equation(object):
    """
    Represents an equation
    """
    def __init__(self, coefficientValue, systemVariable):
        """
        args:
             coefficientValue (float): The  coefficient Value
             systemVariable (int): The system variable for the equation
        """
        super(Equation, self).__init__()
        self.coefficient = coefficientValue
        self.variable = systemVariable


class LinearCombinationEquation(object):
    """
    Represents and solves a limear combination equation
    """

    def __init__(self, coefficientConst, coefficients):
        """
        Constructor

        args:
            coefficientConst (float): The coefficient constant value
            coefficients (list of Equation): List of Equation that make up the equation
        """
        super(LinearCombinationEquation, self).__init__()
        self._coefficients = coefficients
        self._coefficientConst = coefficientConst

    def getVars(self):
        """
        Get all the system vars needed to solve this equation

        returns:
            list of ints
        """
        return [co.variable for co in self._coefficients]

    def coefficientConst(self):
        """
        Get the coefficient Constant

        returns:
            float coefficient Const
        """
        return self._coefficientConst

    def coefficients(self):
        """
        Get the coefficients

        returns:
            list of Equation
        """
        return self._coefficients

    def solve(self, varDict):
        """
        solve the equation

        args:
            varDict (dict of int:floats): dict where the keys are the vars needed and values are the float values

        returns:
            float value
        """
        return sum([co.coefficient * varDict[co.variable] for co in self._coefficients]) + self._coefficientConst


class IncludedBodyStubb(object):
    """
    Class to represent an included body
    """
    def __init__(self, stubb):
        """
        Constructor

        args:
            stubb (giantBase.GiantStubb): The stubb to wrap
        """
        super(IncludedBodyStubb, self).__init__()
        self._fileName = ""
        self._bodyIdOffset = 0
        self._systemVariableIdOffset = 0
        self._stubb = stubb

        self._loadData()

    def getFileName(self):
        """
        Get the file name to include

        return:
            filepath string to use
        """
        return self._fileName

    def setFileName(self, value):
        """
        set the file name to include

        args:
            value(str): file path string to include
        """
        self._fileName = value

    def getBodyIdOffset(self):
        """
        Get the body ID Offset value

        return:
            int of the body ID offset
        """
        return self._bodyIdOffset

    def setBodyIdOffset(self, value):
        """
        set the body ID Offset value

        args:
            value(int): the body ID offset
        """
        self._bodyIdOffset = value

    def getSystemVariableIdOffset(self):
        """
        get the System Variable ID Offset value

        return:
            int of the System Variable ID Offset
        """
        return self._systemVariableIdOffset

    def setSystemVariableIdOffset(self, value):
        """
        set the System Variable ID Offset value

        args:
            value(int): the System Variable ID Offset
        """
        self._systemVariableIdOffset = value

    def _loadData(self):
        """
        Internal Method

        Load the data from the stubb into the class
        """
        self._fileName = self._stubb.getByStubbName("FILE_NAME").value
        self._bodyIdOffset = self._stubb.getByStubbName("BODY_ID_OFFSET").value
        self._systemVariableIdOffset = self._stubb.getByStubbName("SYSTEM_VARIABLE_ID_OFFSET").value

    def writeToStubb(self):
        """
        Write the data from the class onto the underlying stubb so it can be written to a file.
        This does not write to disk.
        """
        self._stubb.getByStubbName("FILE_NAME").value = self._fileName
        self._stubb.getByStubbName("BODY_ID_OFFSET").value = str(self._bodyIdOffset)
        self._stubb.getByStubbName("SYSTEM_VARIABLE_ID_OFFSET").value = str(self._systemVariableIdOffset)

    def getStubb(self):
        """
        Get the stubb which is being wrapped

        return:
            GiantStubb that is wrapped
        """
        return self._stubb


class GraphicalMappingDof(giantBase.GiantFileBase):
    """
    Giant File Base for reading and writing Giant DOF files.
    """
    def __init__(self, filePath=None):
        """
        Constructor

        kargs:
            filePath (str): File path to read in if given
        """
        super(GraphicalMappingDof, self).__init__()
        self._filePath = None
        self._fileType = "Bio Graphic Mapping File"
        self._fileTypeVersion = "v1.00"

        self._bodies = []
        self._includedBodies = []

        if filePath is not None:
            self.readFile(filePath)

    def addBody(self, bodyId):
        """
        Add a new body with the given ID number

        return:
            DofBody of the newly created body
        """
        newBody = DofBody(bodyId)
        self._bodies.append(newBody)
        return newBody

    def readFile(self, filePath):
        """
        Read a file in given file path

        args:
            filePath (str): File path of file to read in
        """
        self._frameData = []
        if not os.path.isfile(filePath):
            raise ValueError("Unable to read file. '{0}' is not a file".format(filePath))
        self._filePath = filePath
        handler = open(filePath)
        self._fileType = self._readLine(handler)
        self._fileTypeVersion = self._readLine(handler)

        varCount = int(self._readLine(handler)) # number of vars in each body
        bodyCount = int(self._readLine(handler)) # number of bodies in the file

        for _ in xrange(bodyCount):
            self._bodies.append(self._readBody(handler, varCount))

        while True:
            try:
                self._readStubb(handler)
            except ValueError:
                break

        for stubb in self.getAllByStubbName("INCLUDE"):
            self._includedBodies.append(IncludedBodyStubb(stubb))

        handler.close()

    def _readBody(self, handler, varCount):
        """
        Internal Method

        Read a body of the giant file.

        args:
            handler (file handler): the handler for the file
        """
        newBody = DofBody(int(self._readLine(handler)))
        transformOrder = [int(var) for var in self._readLine(handler).split(" ", varCount)]

        newBody.setTransformOrder(transformOrder)
        for idx in xrange(len(transformOrder)):
            line = self._readLine(handler)
            if line.startswith("-"):
                # Read the linear equation
                parts = line.split(" ")
                parts.reverse()
                parts.pop()
                eqConst = float(parts.pop())
                allEqs = []
                while len(parts) > 0:
                    systemVariable = int(parts.pop())
                    coefficientValue = float(parts.pop())
                    allEqs.append(Equation(coefficientValue, systemVariable))
                newBody.setTransformEquation(idx + 1, LinearCombinationEquation(eqConst, allEqs))
            else:
                newBody.setTransformVar(idx + 1, int(line))

        return newBody

    def _readLine(self, handler):
        """
        Internal Method

        Read a single line off a file and strip it so it can be read just for values
        """
        newLine = handler.readline()
        newLine = newLine.strip()
        return newLine

    def getIncludedBodies(self):
        """
        get all the included bodies in the file

        return:
            list of includes bodies as IncludedBodyStubb
        """
        return self._includedBodies

    def getBodies(self):
        """
        get all the bodies in the file

        return:
            list of bodies as DofBody
        """
        return self._bodies

    def addIncludedBody(self):
        """
        Add a new included body

        returns:
            the newly created body as a IncludedBodyStubb
        """
        newStubb = self._addStubb("INCLUDE")

        newStubb.addNewChild("FILE_NAME", value="")
        newStubb.addNewChild("BODY_ID_OFFSET", value="")
        newStubb.addNewChild("SYSTEM_VARIABLE_ID_OFFSET", value="")

        newBody = IncludedBodyStubb(newStubb)
        self._includedBodies.append(newBody)
        return newBody

    def writeFile(self, filePath=None):
        """
        Reimplemented

        Write to a new file or update the currently loaded in file

        kwargs:
            filePath (str): The file path to write to
        """
        with open(filePath or self._filePath, 'w') as handler:
            maxVarCount = sum([body.getVariableCount() for body in self._bodies] or [0])

            handler.writelines("%s\n" % (self._fileType))
            handler.writelines("%s\n" % (self._fileTypeVersion))

            handler.writelines("%s\n" % (maxVarCount))
            handler.writelines("%s\n" % (len(self._bodies)))

            for body in self._bodies:
                self._writeBody(handler, body)

            for body in self.getIncludedBodies():
                body.writeToStubb()
                self._writeStubb(handler, body.getStubb())

    def _writeBody(self, handler, body):
        """
        Internal Method

        Write out the body (not inlcuded bodies)

        args:
            handler (file handler): the handler for the file
            body (DofBody): the body to write
        """
        transformOrder = body.getTransformOrder()

        handler.writelines("%s\n" % (body.getBodyId()))  # Write ID
        handler.writelines("%s\n" % (" ".join([str(trx) for trx in transformOrder])))

        for trx in transformOrder:
            handler.writelines("%s\n" % (body.getTransformVar(trx)))
