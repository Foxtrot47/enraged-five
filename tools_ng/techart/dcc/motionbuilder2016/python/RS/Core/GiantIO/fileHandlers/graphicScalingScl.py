"""
Contains classes to handle the reading and writing of the "Bio Graphic Scaling File" (.scl) format.
"""
from RS.Core.GiantIO.fileHandlers import giantBase


class IncludedScalingStubb(object):
    """
    Class to represent an included scaling in a .scl file.
    """

    def __init__(self, stubb):
        """
        Constructor

        args:
            stubb (giantBase.GiantStubb): The stubb to wrap.
        """
        super(IncludedScalingStubb, self).__init__()

        self._fileName = ""
        self._bodyIdOffset = 0
        self._stubb = stubb

        self._loadData()

    def getFileName(self):
        """
        Get the file name to include

        return:
            str: The filepath listed in the stubb.
        """
        return self._fileName

    def setFileName(self, value):
        """
        set the file name to include

        args:
            value(str): file path string to include
        """
        self._fileName = value

    def getBodyIdOffset(self):
        """
        Get the body ID Offset value.

        return:
            int: The body ID offset.
        """
        return self._bodyIdOffset

    def setBodyIdOffset(self, value):
        """
        Set the body ID Offset value.

        args:
            value(int): The body ID offset.
        """
        self._bodyIdOffset = value

    def _loadData(self):
        """
        Internal Method

        Load the data from the stubb into the class.
        """
        self._fileName = self._stubb.getByStubbName("FILE_NAME").value
        self._bodyIdOffset = self._stubb.getByStubbName("BODY_ID_OFFSET").value

    def writeToStubb(self):
        """
        Write the data from the class onto the underlying stubb so it can be written to a file.
        This does not write to disk.
        """
        self._stubb.getByStubbName("FILE_NAME").value = self._fileName
        self._stubb.getByStubbName("BODY_ID_OFFSET").value = str(self._bodyIdOffset)

    def getStubb(self):
        """
        Get the stubb which is being wrapped.

        return:
            giantBase.GiantStubb: The internally stored stubb for the class.
        """
        return self._stubb


class ScalingScl(giantBase.GiantFileBase):
    """
    Class to load in a Giant .scl file so its easy to read.
    """

    def __init__(self, filePath=None):
        """
        Constructor

        kwargs:
            filePath (str): File path to the file to read in.
        """
        super(ScalingScl, self).__init__(filePath=filePath)
        self._fileType = "Bio Graphic Scaling File"
        self._fileTypeVersion = "v1.00"

        self._includedScalings = [IncludedScalingStubb(scaling) for scaling in self.getAllByStubbName("INCLUDE")]

    def getIncludedScalings(self):
        """
        Get all the included models in the file.

        return:
            list of IncludedScalingStubb: All the included scalings.
        """
        return self._includedScalings

    def addIncludedScaling(self):
        """
        Add a new included scaling to the file.

        return:
            IncludedScalingStubb: The newly created included scaling.
        """
        newStubb = self._addStubb("INCLUDE")

        newStubb.addNewChild("FILE_NAME", value="")
        newStubb.addNewChild("BODY_ID_OFFSET", value="")

        newBody = IncludedScalingStubb(newStubb)
        self._includedScalings.append(newBody)
        return newBody

    def writeFile(self, filePath=None):
        """
        ReImplemeted

        Write to a new file or update the currently loaded in file.

        kwargs:
            filePath (str): The file path to write to.
        """
        for scaling in self.getIncludedScalings():
            scaling.writeToStubb()
        super(ScalingScl, self).writeFile(filePath=filePath)
