"""
Class to handle the reading and writing of Nuance Maker Paths MPF file formats
"""

from RS.Core.GiantIO.fileHandlers import giantBase


class MarkerData(object):
    """
    Class to represent a single marker path
    """
    def __init__(self, stubb):
        """
        Constructor

        args:
            stubb (GiantStubb): The stubb to wrap
        """
        super(MarkerData, self).__init__()

        self._positionData = {}
        self._id = ""
        self._name = ""
        self._stubb = stubb

        self._loadData()

    def getId(self):
        """
        Get the ID of the Path

        return:
            int of the path's ID
        """
        return self._id

    def setId(self, newId):
        """
        set the ID of the Path

        args:
            newId(int): The ID of the path
        """
        self._id = newId

    def getName(self):
        """
        Get the name of the Path

        return:
            str of the paths name
        """
        return self._name

    def setName(self, newName):
        """
        set the name of the Path

        args:
            newName(str): The name of the path
        """
        self._name = newName

    def getPositions(self):
        """
        Get all the position data for the marker

        returns:
            a dict of all the data where the frame is the key and the value is (x, y, z) as floats
        """
        return self._positionData

    def setPositions(self, newDict):
        """
        Set the marker positions using a dict where the frame is the key and the value is
        (x, y, z) as floats

        args:
            newDict(dict): the new frame data
        """
        self._positionData.update(newDict)

    def getPosition(self, frame):
        """
        Get the position data for a given frame number

        args:
            frame(int): the frame to get the data for

        returns:
            a tuple of floats, making the X, Y and Z axis
        """
        return self._positionData[frame]

    def setPosition(self, frame, xCoords, yCoords, zCoords):
        """
        Set the position data for a given frame number

        args:
            frame(int): the frame to set the data for
            xCoords(float): the x axis data
            yCoords(float): the y axis data
            zCoords(float): the z axis data
        """
        self._positionData[frame] = (xCoords, yCoords, zCoords)

    def _loadData(self):
        """
        Internal Method

        Load the data from the stubb into the class
        """
        self._positionData = {}
        self._id = self._stubb.getByStubbName("ID").value
        self._name = self._stubb.getByStubbName("NAME").value

        for frameData in self._stubb.getByStubbName("POSITIONS").value:
            frame, xCoord, yCoord, zCoord = frameData.split(" ", 4)
            self._positionData[int(frame)] = (float(xCoord), float(yCoord), float(zCoord))

    def writeToStubb(self):
        """
        Write the data from the class onto the underlying stubb so it can be written to a file.
        This does not write to disk.
        """
        self._stubb.getByStubbName("ID").value = "\"%s\"" % (self._id)
        self._stubb.getByStubbName("NAME").value = "\"%s\"" % (self._name)
        self._stubb.getByStubbName("POSITIONS").value = ["%s %s" % (key, "%s %s %s" % (value)) for key, value in self._positionData.iteritems()]
        self._stubb.getByStubbName("DISPLAY").getByStubbName("RANGE").value = "%s %s" % (min(self._positionData.keys()), max(self._positionData.keys()))


class GiantMarkerPaths(giantBase.GiantFileBase):
    """
    Class to load in a Giant CSF file so its easy to read
    """
    def __init__(self, filePath=None):
        """
        Constructor

        kwargs:
            filePath (str): File path to the file to read in
        """
        super(GiantMarkerPaths, self).__init__(filePath=filePath)
        self._fileType = "Nuance Marker File"
        self._fileTypeVersion = "v2.0"

        self._markers = [MarkerData(mkr) for mkr in self.getAllByStubbName("UNBOUND_PATH")]

    def addMarker(self):
        """
        Add a marker path.

        returns:
            the MarkerData of the newly created marker
        """
        newStubb = self._addStubb("UNBOUND_PATH")

        newStubb.addNewChild("ID", value="")
        newStubb.addNewChild("NAME", value="")
        newStubb.addNewChild("POSITIONS", value={})
        displayStubb = newStubb.addNewChild("DISPLAY")

        displayStubb.addNewChild("RANGE", value="1 1")
        displayStubb.addNewChild("MARKERS", value="false")
        displayStubb.addNewChild("LINES", value="true")
        displayStubb.addNewChild("LINE_COLOR", value="0x0")

        newMrk = MarkerData(newStubb)
        self._markers.append(newMrk)
        return newMrk

    def getMarkers(self):
        """
        Get all the marker paths in the file

        returns:
            a list of all the markers as MarkerData
        """
        return self._markers

    def writeFile(self, filePath=None):
        """
        ReImplemeted

        Write to a new file or update the currently loaded in file

        kwargs:
            filePath (str): The file path to write to
        """
        for marker in self.getMarkers():
            marker.writeToStubb()
        super(GiantMarkerPaths, self).writeFile(filePath=filePath)
