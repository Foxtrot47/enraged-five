"""
Processors for Giant anims.
"""
import os

from PySide import QtGui, QtCore

from RS.Core.GiantIO.utils import giantPaths
from RS.Core.GiantIO.fileHandlers import (project, modelMdl, graphicalMappingDof, giantBase, systemVarsBmo,
                                                graphicScalingScl, animationAnm)


class AddBodyToRefineProcessor(object):
    """
    Methods for adding a new body

    Contains no perforce calls.
    """

    def __init__(self):
        self._anim = None
        self._dofPath = None
        self._mdlPath = None
        self._sclPath = None
        self._sysVarId = None
        self._bodyNameSuffix = None

        self._animPath = None
        self._animMdlPath = None
        self._animDofPath = None
        self._animBmoPath = None
        self._animSclPath = None

        self._newMdlPath = None
        self._newDofPath = None
        self._newSclPath = None
        self._newBmoPath = None
        self._newAnimPath = None

    @classmethod
    def getInputsForProp(cls, prjUnixPath, rootDir=None):
        """
        Wrapper for getting the Giant mapping, model, scaling file paths and sys var id from a Giant Project file path.

        args:
            prjUnixPath (str): The filesystem path to the Giant Project file.

        keyword args:
            rootDir (str|None): The root project directory to build dir/file paths from. If None, gets dir from env.

        Returns:
            tuple of str, str, str, int: dof giant path, system var id
        """
        if rootDir is None:
            rootDir = giantPaths.getRootDirFromEnv()

        # Parse the prj to get its mdl and scl paths.
        prj = project.GiantPrj(filePath=prjUnixPath)
        gfxModels = prj.getAllByStubbName("GFX_MODEL")
        mdlPath = gfxModels[0].value
        gfxScales = prj.getAllByStubbName("GFX_SCALING")
        sclPath = gfxScales[0].value

        # Generate the dof path from the prj path. This is hacky, but replicates TCL and we don't have a better option
        # at present. It won't work on some props, due to poor naming practices.
        dofBasename = os.path.splitext(os.path.basename(prjUnixPath))[0]
        dofPath = os.path.join("$BIO_DATA_DIR", "dof", "{}.dof".format(dofBasename))

        # If the path doesn't exist, get the dof file from the user.
        dofUnixPath = giantPaths.toUnixPath(dofPath)
        if not os.path.exists(dofUnixPath):
            defaultDofDir = os.path.join(rootDir, "dof")
            title = "Could not find matching .dof for: {}. Please select one:".format(os.path.basename(prjUnixPath))
            dofUnixPath, _ = QtGui.QFileDialog.getOpenFileName(None, title, defaultDofDir,
                                                               filter="Giant Graphic Mapping (*.dof)")
            if not dofUnixPath:
                # Hitting cancel in the file open dialog should cancel the whole user action.
                return (None, None, None, None)
            dofPath = giantPaths.toGiantPath(dofUnixPath)

        # Parse the dof to get the sys var ID.
        sysVarId = None
        sysVarIdLineIdx = 2
        with open(dofUnixPath, "r") as dofFo:
            for idx, line in enumerate(dofFo):
                if idx == sysVarIdLineIdx:
                    sysVarValue = line.strip(" ").replace("\n", "")
                    sysVarId = int(sysVarValue)
                    break

        return (dofPath, mdlPath, sclPath, sysVarId)

    def setInputs(self, anim, dofPath, mdlPath, sclPath, sysVarId, bodyNameSuffix):
        """
        Set the inputs for the rest of the class to access.

        args:
            anim (atlas anim):
            dofPath (str): The input mapping path.
            mdlPath (str): The input model path.
            sclPath (str): The input scaling path.
            sysVarId (str): The sys var id (as extracted from a dof).
            bodyNameSuffix (str): The new model's "body name suffix".
        """
        # Validation.
        for path in [dofPath, mdlPath, sclPath]:
            try:
                assert os.path.exists(giantPaths.toUnixPath(path)), "Required file does not exist! Check: {}".format(path)
            except AssertionError as error:
                QtGui.QMessageBox.critical(self, self.WINDOW_TITLE, error.message)
                return

        # Set inputs.
        self._anim = anim
        self._dofPath = dofPath
        self._mdlPath = mdlPath
        self._sclPath = sclPath
        self._sysVarId = sysVarId
        self._bodyNameSuffix = bodyNameSuffix

        self._animPath = anim.fileName()
        animData = anim.getAllMotionData()
        self._animMdlPath = animData.get("mdl")
        self._animDofPath = animData.get("dof")
        self._animBmoPath = animData.get("bmo")
        self._animSclPath = animData.get("scl")

        # Generate output paths.
        animBaseFilename = os.path.splitext(self._animPath)[0]
        self._newAnimPath = "{}_new.anm".format(animBaseFilename)

        baseFilename = os.path.splitext(self._animBmoPath)[0]
        self._newMdlPath = "{}_new.mdl".format(baseFilename)
        self._newDofPath = "{}_new.dof".format(baseFilename)
        self._newSclPath = "{}_new.scl".format(baseFilename)
        self._newBmoPath = "{}_new.bmo".format(baseFilename)

    def getOutputPaths(self):
        """
        Get the output paths for the new Giant anim, model, mapping, motion, and scaling. Use these for pre-file
        generation validation, etc.

        returns:
            dict: Dictionary of output file paths.
        """
        paths = {
            "anim": self._newAnimPath,
            "mdl": self._newMdlPath,
            "dof": self._newDofPath,
            "bmo": self._newBmoPath,
            "scl": self._newSclPath
        }
        return paths

    def generateNewAnim(self):
        """
        Creates new dof, mdl, scl, and anm files based on the params and body name suffix provided by user prompt.

        yields:
            str: Status message to pass to logger, status widgets, etc.
        """
        # Open .bmo and read.
        yield "Reading motion file..."
        animBmo = systemVarsBmo.SystemVarsBmo(self._animBmoPath)

        yield "Processing the model, mapping, and scaling files..."
        # Get the bmo id offset for use in the new dof body and new bmo offset id for use in creating a new bmo.
        bmoIdOffset = animBmo.getVarCount()
        newBmoIdOffset = bmoIdOffset + self._sysVarId

        # Open .mdl and read.
        animMdl = modelMdl.ModelMdl(self._animMdlPath)

        # Find and Open last .mdl in file, use it to get body ID offset.
        lastIncludedMdl = animMdl.getIncludedModels()[-1]
        lastIncludedMdlPath = giantPaths.toUnixPath(lastIncludedMdl.getFileName())
        lastIncludedMdlWithAllStubbs = giantBase.GiantFileBase(lastIncludedMdlPath)
        bodyIdStubbs = lastIncludedMdlWithAllStubbs.getAllByStubbName("BODY")
        lastBodyId = bodyIdStubbs[-1].getByStubbName("ID").value
        lastMdlId = lastIncludedMdl.getStubb().getByStubbName("BODY_ID_OFFSET").value

        newBodyIdOffset = int(lastBodyId) + int(lastMdlId)

        # Add a new include section for the new body's mdl.
        newIncMdl = animMdl.addIncludedModel()
        newIncMdl.setFileName(self._mdlPath)
        newIncMdl.setBodyIdOffset(newBodyIdOffset)
        newIncMdl.setBodyNameSuffix(self._bodyNameSuffix)

        # Create new .mdl file.
        animMdl.writeFile(self._newMdlPath)

        # Open .dof and read.
        animDof = graphicalMappingDof.GraphicalMappingDof(self._animDofPath)

        # Add a new include section for the new body's dof.
        newIncBody = animDof.addIncludedBody()
        newIncBody.setFileName(self._dofPath)
        newIncBody.setBodyIdOffset(newBodyIdOffset)
        newIncBody.setSystemVariableIdOffset(bmoIdOffset)

        # Create new .dof file.
        animDof.writeFile(self._newDofPath)

        # Open .scl and read.
        animScl = graphicScalingScl.ScalingScl(self._animSclPath)

        # Add a new include section for the new body's scl.
        newIncScl = animScl.addIncludedScaling()
        newIncScl.setFileName(self._sclPath)
        newIncScl.setBodyIdOffset(newBodyIdOffset)

        # Create new .scl file.
        animScl.writeFile(self._newSclPath)

        # Create new .bmo file (add new vars with a value of 0 to all frames).
        yield "Adding frame data for new body..."
        newVarCount = newBmoIdOffset - bmoIdOffset
        for frame in animBmo.getFrames():
            for _ in xrange(newVarCount):
                frame.addVar(0)

        # Create new .bmo file.
        yield "Writing new motion file..."
        animBmo.writeFile(self._newBmoPath)

        # Create new .anm file.
        yield "Writing refine anim file..."
        newAnim = animationAnm.AnimationAnm()
        newAnim.addModel(giantPaths.toGiantPath(self._newMdlPath))
        newAnim.addMapping(giantPaths.toGiantPath(self._newDofPath))
        newAnim.addMotion(giantPaths.toGiantPath(self._newBmoPath))
        newAnim.addScaling(giantPaths.toGiantPath(self._newSclPath))

        newAnim.writeFile(self._newAnimPath)
