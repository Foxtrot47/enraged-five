import os

from RS.CorePy.abstracts import abstractConsts
from RS.Core.GiantIO.fileHandlers import giantBase
from RS.Core.GiantIO import exceptions


class FileStubNames(abstractConsts.AbstractConsts):
    PROBLEM_NAME = "PROBLEM_NAME"
    CAMERA_CALIBRATION = "CAMERA_CALIBRATION"
    CALIBRATION_ERROR = "CALIBRATION_ERROR"
    PATTERN_TOLERANCE = "PATTERN_TOLERANCE"
    GRAPHICMODEL = "GRAPHICMODEL"
    REFERENCE_OFFSET = "REFERENCE_OFFSET"
    ACQUISITION_RATE = "ACQUISITION_RATE"
    FILTER_LEVEL = "FILTER_LEVEL"
    MIN_VISIBILITY = "MIN_VISIBILITY"
    AUTO_SCALE = "AUTO_SCALE"
    TIMECODE_24FPS = "TIMECODE_24FPS"
    RECORD_AUDIO = "RECORD_AUDIO"
    DRIFT_COMPENSATION = "DRIFT_COMPENSATION"


class RtParams(giantBase.GiantFileBase):
    """Class to load in a Giant rtparams file so its easy to read"""

    def __init__(self, filePath=None):
        super(RtParams, self).__init__(filePath=filePath)

    def readFile(self, filePath):
        """ReImplemented.

        Read a file in given file path.

        Args:
            filePath (str): File path of file to read in.
        """
        if not os.path.isfile(filePath):
            raise ValueError("Unable to read file. '{0}' is not a file".format(filePath))
        self._filePath = filePath
        handler = open(filePath)
        while True:
            try:
                newStubb = self._readStubb(handler)
                if newStubb is not None:
                    self._stubbs.append(newStubb)
            except Exception:
                break
        handler.close()

    def writeFile(self, filePath=None):
        """Reimplemented

        Write a file to given file path.

        Args:
            filePath (str, optional): File path of file to write to
        """
        with open(filePath or self._filePath, "w") as handler:
            for stubb in self._stubbs:
                self._writeStubb(handler, stubb, whitespace=1)

    def getKeys(self):
        return [stubb.stubbName for stubb in self._stubbs]

    def getValues(self):
        return [stubb.value for stubb in self._stubbs]

    def getValue(self, key):
        try:
            return self.getByStubbName(key).value
        except AttributeError:
            raise exceptions.MissingStubbs([key], "rtparams")

    def setValue(self, key, value):
        stubb = self.getByStubbName(key)
        if stubb is None:
            stubb = self._addStubb(key)

        self.getByStubbName(key).value = value
