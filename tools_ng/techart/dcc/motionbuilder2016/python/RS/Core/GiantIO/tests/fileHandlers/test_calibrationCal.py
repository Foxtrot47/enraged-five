"""Unittests for RS.Core.GiantIO.fileHandlers.calibrationCal.Calibration class."""
import unittest
import platform
import os

import pathlib

from RS.Core.GiantIO import const
from RS.Core.GiantIO.fileHandlers import calibrationCal


@unittest.skipIf(platform.system() == "Windows", "Linux-only")
class TestCalibration(unittest.TestCase):
    TEST_DATA = pathlib.Path(const.Resources.TESTS)
    INPUTS = {
        "old": TEST_DATA.joinpath("cal_tor_opti_28cam_aug02pm_2019.cal"),
        "new": TEST_DATA.joinpath("SD_StudioA.32cam.2019-11-20_16-19.calibration.cal"),
    }

    def test_init(self):
        for name, path in self.INPUTS.items():
            cal = calibrationCal.Calibration(filePath=str(path))
            self.assertEqual("Calibration(filePath='{}')".format(str(path)), str(cal))
            self.assertEqual(str(path), cal.filePath())
            self.assertTrue(isinstance(cal.getStubbs(), list))

    def test_contextId(self):
        """Make sure setting works and the data is stored correctly."""
        contextId = 777

        cal = calibrationCal.Calibration(filePath=str(self.INPUTS["new"]))
        self.assertEqual(8888, cal.contextId())
        cal.setContextId(contextId)
        self.assertEqual(contextId, cal.contextId())
        cal.setContextId(None)
        self.assertIsNone(cal.contextId())
        self.assertTrue(const.MetaDataKeys.CONTEXT not in cal._metadata.keys())

        cal = calibrationCal.Calibration(filePath=str(self.INPUTS["old"]))
        self.assertIsNone(cal.contextId())
        cal.setContextId(contextId)
        self.assertEqual(contextId, cal.contextId())
        cal.setContextId(None)
        self.assertIsNone(cal.contextId())
        self.assertTrue(const.MetaDataKeys.CONTEXT not in cal._metadata.keys())

    def test_write(self):
        # Make sure the new file wrote to disk and contains the new data.
        cal = calibrationCal.Calibration(filePath=str(self.INPUTS["new"]))
        contextId = 777
        cal.setContextId(contextId)
        newPath = os.path.join("/tmp", os.path.basename(cal.filePath()))
        cal.writeFile(newPath)

        self.assertTrue(os.path.exists(newPath))
        newCal = calibrationCal.Calibration(filePath=newPath)
        self.assertEqual("Calibration(filePath='{}')".format(newPath), str(newCal))
        self.assertEqual(contextId, newCal.contextId())
