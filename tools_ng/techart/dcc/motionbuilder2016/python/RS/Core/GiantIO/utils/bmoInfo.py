import os
import linecache
import mmap
import re

from RS.Core.GiantIO.fileHandlers import systemVarsBmo


def frameCountFromBmo(bmoFile):
    """
    Get the frame frame from a bmo File
    
    args:
        bmoFile (str): Input file path
    
    return:
        frame count as a stringfrae
    """
    if not os.path.exists(bmoFile):
        raise IOError("Input BMO File '{0}' does not exist").format(bmoFile)
    return linecache.getline(bmoFile, 3).rstrip("\n")


def startTcFromBmo(bmoFile):
    """
    Get the start TC from a bmo File
    
    args:
        bmoFile (str): Input file path
    
    return:
        start TC from the BMO as a string or None if none is found
    """
    if not os.path.exists(bmoFile):
        raise IOError("Input BMO File '{0}' does not exist").format(bmoFile)
    splitline = linecache.getline(bmoFile, 5).split(" "*2)
    if len(splitline) == 2:
        return splitline[-1].strip()
    return None


def endTcFromBmo(bmoFile):
    """
    Get the end TC from a bmo File
    
    args:
        bmoFile (str): Input file path
    
    return:
        end TC from the BMO as a string or None if none is found
    """
    if not os.path.exists(bmoFile):
        raise IOError("Input BMO File '{0}' does not exist".format(bmoFile))
    frame = frameCountFromBmo(bmoFile)
    if frame is None:
        return None
    with open(bmoFile) as fp:
        for idx, line in enumerate(fp):
            if " Results for field {0}".format(frame) in line:
                splitline = line.split(" {0} ".format(frame))
                if len(splitline) == 2:
                    return splitline[-1].strip()


def findFrameFromTimecode(tcToFind, bmoPath):
    """
    Get the frame for the given timecode in the given bmo, if the timecode is in the bmo

    args:
        tcToFind (string): Timecode to find
        bmoPath (string): Path to the BMO

    returns:
        int of the frame number, or None if the timecode isnt present
    """
    with open(bmoPath) as fileHandler:
        handler = mmap.mmap(fileHandler.fileno(), 0, access=mmap.ACCESS_READ)
        pos = handler.find(tcToFind)
        if pos == -1:
            return None
        line = handler[pos - 50:pos + 30]
        frameIdx, tc = re.search(systemVarsBmo.SystemVarsBmo.REGEX_FRAME_HEADER, line).groups()
    return int(frameIdx)
