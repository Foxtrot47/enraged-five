"""Unittests for utils.giantEnv"""
import unittest
import platform
import os

from RS.Core.GiantIO.utils import giantEnv
from RS.Core.GiantIO import exceptions


@unittest.skipIf(platform.system() == "Windows", "Linux-only")
class TestVarNames(unittest.TestCase):

    def test_bioDataDir(self):
        self.assertEqual("BIO_DATA_DIR", giantEnv.GiantVarNames.BIO_DATA_DIR)

    def test_giantDataDir(self):
        self.assertEqual("GIANT_DATA_DIR", giantEnv.GiantVarNames.GIANT_DATA_DIR)

    def test_nuDataDir(self):
        self.assertEqual("NU_DATA_DIR", giantEnv.GiantVarNames.NU_DATA_DIR)

    def test_dataDirs(self):
        self.assertEqual(["GIANT_DATA_DIR", "BIO_DATA_DIR", "NU_DATA_DIR"], giantEnv.GiantVarNames.DATA_DIRS)

    def test_bioRttrackDir(self):
        self.assertEqual("BIO_RTTRACK_DIR", giantEnv.GiantVarNames.BIO_RTTRACK_DIR)

    def test_giantRttrackDir(self):
        self.assertEqual("GIANT_RTTRACK_DIR", giantEnv.GiantVarNames.GIANT_RTTRACK_DIR)

    def test_rttrackDirs(self):
        self.assertEqual(["BIO_RTTRACK_DIR", "GIANT_RTTRACK_DIR"], giantEnv.GiantVarNames.RTTRACK_DIRS)

    def test_giantUseCombinedDir(self):
        self.assertEqual("GIANT_USE_COMBINED_DIR", giantEnv.GiantVarNames.GIANT_USE_COMBINED_DIR)

    def test_giantProblemSetupUseImu(self):
        self.assertEqual("GIANT_PROBLEM_SETUP_USE_IMU", giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_USE_IMU)

    def test_giantProblemSetupLiveChanges(self):
        self.assertEqual("GIANT_PROBLEM_SETUP_LIVE_CHANGES", giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_LIVE_CHANGES)

    def test_giantProblemSetupMultiCombine(self):
        self.assertEqual("GIANT_PROBLEM_SETUP_MULTI_COMBINE", giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_MULTI_COMBINE)

    def test_all(self):
        vars = [
            "BIO_RTTRACK_DIR",
            "GIANT_RTTRACK_DIR",
            "BIO_DATA_DIR",
            "GIANT_DATA_DIR",
            "NU_DATA_DIR",
            "GIANT_USE_COMBINED_DIR",
            "GIANT_PROBLEM_SETUP_USE_IMU",
            "GIANT_PROBLEM_SETUP_LIVE_CHANGES",
            "GIANT_PROBLEM_SETUP_MULTI_COMBINE"
        ]
        self.assertEqual(vars, giantEnv.GiantVarNames.ALL)


@unittest.skipIf(platform.system() == "Windows", "Linux-only")
class TestDataDirs(unittest.TestCase):

    def setUp(self):
        self.rootDir1 = "/media/giant_capture_data/development"
        self.rootDir2 = "/media/giant_capture_data/other"
        self.envVars = {
            "BIO_DATA_DIR": self.rootDir1,
            "GIANT_DATA_DIR": self.rootDir1,
            "NU_DATA_DIR": self.rootDir1,
        }
        for envVar, value in self.envVars.iteritems():
            os.environ[envVar] = value

    def test_tokenize(self):
        self.assertRaises(ValueError, giantEnv.DataDirs.tokenize, None)
        self.assertRaises(ValueError, giantEnv.DataDirs.tokenize, "")
        self.assertRaises(exceptions.GiantEnvironmentError, giantEnv.DataDirs.tokenize, None)
        self.assertRaises(exceptions.GiantEnvironmentError, giantEnv.DataDirs.tokenize, "")
        self.assertEqual(("/media/giant_capture_data", "development"), giantEnv.DataDirs.tokenize(self.rootDir1))
        self.assertEqual(("/media/giant_capture_data", "other"), giantEnv.DataDirs.tokenize(self.rootDir2))

    def test_bio(self):
        self.assertEqual(self.rootDir1, giantEnv.DataDirs.bio())
        self.assertEqual(self.rootDir1, os.getenv(giantEnv.GiantVarNames.BIO_DATA_DIR))

        giantEnv.DataDirs.setBio(None)
        self.assertEqual(None, giantEnv.DataDirs.bio())
        self.assertEqual(None, os.getenv(giantEnv.GiantVarNames.BIO_DATA_DIR))

        giantEnv.DataDirs.setBio(self.rootDir2)
        self.assertEqual(self.rootDir2, giantEnv.DataDirs.bio())
        self.assertEqual(self.rootDir2, os.getenv(giantEnv.GiantVarNames.BIO_DATA_DIR))

        giantEnv.DataDirs.setBio("")
        self.assertEqual(None, giantEnv.DataDirs.bio())
        self.assertEqual(None, os.getenv(giantEnv.GiantVarNames.BIO_DATA_DIR))

    def test_giant(self):
        self.assertEqual(self.rootDir1, giantEnv.DataDirs.giant())
        self.assertEqual(self.rootDir1, os.getenv(giantEnv.GiantVarNames.GIANT_DATA_DIR))

        giantEnv.DataDirs.setGiant(None)
        self.assertEqual(None, giantEnv.DataDirs.giant())
        self.assertEqual(None, os.getenv(giantEnv.GiantVarNames.GIANT_DATA_DIR))

        giantEnv.DataDirs.setGiant(self.rootDir2)
        self.assertEqual(self.rootDir2, giantEnv.DataDirs.giant())
        self.assertEqual(self.rootDir2, os.getenv(giantEnv.GiantVarNames.GIANT_DATA_DIR))

        giantEnv.DataDirs.setGiant("")
        self.assertEqual(None, giantEnv.DataDirs.giant())
        self.assertEqual(None, os.getenv(giantEnv.GiantVarNames.GIANT_DATA_DIR))

    def test_nu(self):
        self.assertEqual(self.rootDir1, giantEnv.DataDirs.nu())
        self.assertEqual(self.rootDir1, os.getenv(giantEnv.GiantVarNames.NU_DATA_DIR))

        giantEnv.DataDirs.setNu(None)
        self.assertEqual(None, giantEnv.DataDirs.nu())
        self.assertEqual(None, os.getenv(giantEnv.GiantVarNames.NU_DATA_DIR))

        giantEnv.DataDirs.setNu(self.rootDir2)
        self.assertEqual(self.rootDir2, giantEnv.DataDirs.nu())
        self.assertEqual(self.rootDir2, os.getenv(giantEnv.GiantVarNames.NU_DATA_DIR))

        giantEnv.DataDirs.setNu("")
        self.assertEqual(None, giantEnv.DataDirs.nu())
        self.assertEqual(None, os.getenv(giantEnv.GiantVarNames.NU_DATA_DIR))

    def test_all(self):
        expectedResult = {
            giantEnv.GiantVarNames.BIO_DATA_DIR: self.rootDir1,
            giantEnv.GiantVarNames.GIANT_DATA_DIR: self.rootDir1,
            giantEnv.GiantVarNames.NU_DATA_DIR: self.rootDir1
        }
        self.assertEqual(giantEnv.DataDirs.all(), expectedResult)
        self.assertEqual(giantEnv.DataDirs.bio(), self.rootDir1)
        self.assertEqual(giantEnv.DataDirs.giant(), self.rootDir1)
        self.assertEqual(giantEnv.DataDirs.nu(), self.rootDir1)
        for key in giantEnv.GiantVarNames.DATA_DIRS:
            self.assertEqual(os.getenv(key), self.rootDir1)

        giantEnv.DataDirs.setAll(None)
        expectedResult = {
            giantEnv.GiantVarNames.BIO_DATA_DIR: None,
            giantEnv.GiantVarNames.GIANT_DATA_DIR: None,
            giantEnv.GiantVarNames.NU_DATA_DIR: None
        }
        self.assertEqual(giantEnv.DataDirs.all(), expectedResult)
        self.assertEqual(giantEnv.DataDirs.bio(), None)
        self.assertEqual(giantEnv.DataDirs.giant(), None)
        self.assertEqual(giantEnv.DataDirs.nu(), None)
        for key in giantEnv.GiantVarNames.DATA_DIRS:
            self.assertEqual(os.getenv(key), None)

        giantEnv.DataDirs.setAll(self.rootDir2)
        expectedResult = {
            giantEnv.GiantVarNames.BIO_DATA_DIR: self.rootDir2,
            giantEnv.GiantVarNames.GIANT_DATA_DIR: self.rootDir2,
            giantEnv.GiantVarNames.NU_DATA_DIR: self.rootDir2
        }
        self.assertEqual(giantEnv.DataDirs.all(), expectedResult)
        self.assertEqual(giantEnv.DataDirs.bio(), self.rootDir2)
        self.assertEqual(giantEnv.DataDirs.giant(), self.rootDir2)
        self.assertEqual(giantEnv.DataDirs.nu(), self.rootDir2)
        for key in giantEnv.GiantVarNames.DATA_DIRS:
            self.assertEqual(os.getenv(key), self.rootDir2)

        giantEnv.DataDirs.setAll("")
        expectedResult = {
            giantEnv.GiantVarNames.BIO_DATA_DIR: None,
            giantEnv.GiantVarNames.GIANT_DATA_DIR: None,
            giantEnv.GiantVarNames.NU_DATA_DIR: None
        }
        self.assertEqual(giantEnv.DataDirs.all(), expectedResult)
        self.assertEqual(giantEnv.DataDirs.bio(), None)
        self.assertEqual(giantEnv.DataDirs.giant(), None)
        self.assertEqual(giantEnv.DataDirs.nu(), None)
        for key in giantEnv.GiantVarNames.DATA_DIRS:
            self.assertEqual(os.getenv(key), None)

        # This covers all the vars.
        for value in [True, False, 0, [], {}]:
            self.assertRaises(TypeError, giantEnv.DataDirs.setAll, value)


@unittest.skipIf(platform.system() == "Windows", "Linux-only")
class TestRttrackDirs(unittest.TestCase):

    def setUp(self):
        self.rootDir1 = "/media/giant_capture_data/development"
        self.rootDir2 = "/media/giant_capture_data/other"
        self.envVars = {
            "BIO_RTTRACK_DIR": self.rootDir1,
            "GIANT_RTTRACK_DIR": self.rootDir1,
        }
        for envVar, value in self.envVars.iteritems():
            os.environ[envVar] = value

    def test_bio(self):
        self.assertEqual(giantEnv.RtTrackDirs.bio(), self.rootDir1)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.BIO_RTTRACK_DIR), self.rootDir1)

        giantEnv.RtTrackDirs.setAll(None)
        self.assertEqual(giantEnv.RtTrackDirs.bio(), None)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.BIO_RTTRACK_DIR), None)

        giantEnv.RtTrackDirs.setBio(self.rootDir2)
        self.assertEqual(giantEnv.RtTrackDirs.bio(), self.rootDir2)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.BIO_RTTRACK_DIR), self.rootDir2)

        giantEnv.RtTrackDirs.setAll("")
        self.assertEqual(giantEnv.RtTrackDirs.bio(), None)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.BIO_RTTRACK_DIR), None)

    def test_giant(self):
        self.assertEqual(giantEnv.RtTrackDirs.giant(), self.rootDir1)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_RTTRACK_DIR), self.rootDir1)

        giantEnv.RtTrackDirs.setGiant(None)
        self.assertEqual(giantEnv.RtTrackDirs.giant(), None)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_RTTRACK_DIR), None)

        giantEnv.RtTrackDirs.setGiant(self.rootDir2)
        self.assertEqual(giantEnv.RtTrackDirs.giant(), self.rootDir2)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_RTTRACK_DIR), self.rootDir2)

        giantEnv.RtTrackDirs.setGiant("")
        self.assertEqual(giantEnv.RtTrackDirs.giant(), None)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_RTTRACK_DIR), None)

    def test_all(self):
        expectedResult = {
            giantEnv.GiantVarNames.BIO_RTTRACK_DIR: self.rootDir1,
            giantEnv.GiantVarNames.GIANT_RTTRACK_DIR: self.rootDir1,
        }
        self.assertEqual(giantEnv.RtTrackDirs.all(), expectedResult)
        self.assertEqual(giantEnv.RtTrackDirs.bio(), self.rootDir1)
        self.assertEqual(giantEnv.RtTrackDirs.giant(), self.rootDir1)
        for key in giantEnv.GiantVarNames.RTTRACK_DIRS:
            self.assertEqual(os.getenv(key), self.rootDir1)

        giantEnv.RtTrackDirs.setAll(None)
        expectedResult = {
            giantEnv.GiantVarNames.BIO_RTTRACK_DIR: None,
            giantEnv.GiantVarNames.GIANT_RTTRACK_DIR: None,
        }
        self.assertEqual(giantEnv.RtTrackDirs.all(), expectedResult)
        self.assertEqual(giantEnv.RtTrackDirs.bio(), None)
        self.assertEqual(giantEnv.RtTrackDirs.giant(), None)
        for key in giantEnv.GiantVarNames.RTTRACK_DIRS:
            self.assertEqual(os.getenv(key), None)

        giantEnv.RtTrackDirs.setAll(self.rootDir2)
        expectedResult = {
            giantEnv.GiantVarNames.BIO_RTTRACK_DIR: self.rootDir2,
            giantEnv.GiantVarNames.GIANT_RTTRACK_DIR: self.rootDir2,
        }
        self.assertEqual(giantEnv.RtTrackDirs.all(), expectedResult)
        self.assertEqual(giantEnv.RtTrackDirs.bio(), self.rootDir2)
        self.assertEqual(giantEnv.RtTrackDirs.giant(), self.rootDir2)
        for key in giantEnv.GiantVarNames.RTTRACK_DIRS:
            self.assertEqual(os.getenv(key), self.rootDir2)

        giantEnv.RtTrackDirs.setAll("")
        expectedResult = {
            giantEnv.GiantVarNames.BIO_RTTRACK_DIR: None,
            giantEnv.GiantVarNames.GIANT_RTTRACK_DIR: None,
        }
        self.assertEqual(giantEnv.RtTrackDirs.all(), expectedResult)
        self.assertEqual(giantEnv.RtTrackDirs.bio(), None)
        self.assertEqual(giantEnv.RtTrackDirs.giant(), None)
        for key in giantEnv.GiantVarNames.RTTRACK_DIRS:
            self.assertEqual(os.getenv(key), None)

        # This covers all the vars.
        for value in [True, False, 0, [], {}]:
            self.assertRaises(TypeError, giantEnv.RtTrackDirs.setAll, value)


@unittest.skipIf(platform.system() == "Windows", "Linux-only")
class TestProblemSetup(unittest.TestCase):

    def test_useImu(self):
        self.assertEqual(giantEnv.ProblemSetup.useImu(), None)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_USE_IMU), None)

        giantEnv.ProblemSetup.setUseImu(False)
        self.assertEqual(giantEnv.ProblemSetup.useImu(), False)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_USE_IMU), "0")

        giantEnv.ProblemSetup.setUseImu(None)
        self.assertEqual(giantEnv.ProblemSetup.useImu(), None)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_USE_IMU), None)

        giantEnv.ProblemSetup.setUseImu(True)
        self.assertEqual(giantEnv.ProblemSetup.useImu(), True)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_USE_IMU), "1")

        for value in [0, [], {}]:
            self.assertRaises(TypeError, giantEnv.ProblemSetup.setUseImu, value)

    def test_liveChanges(self):
        self.assertEqual(giantEnv.ProblemSetup.liveChanges(), True)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_LIVE_CHANGES), None)

        giantEnv.ProblemSetup.setLiveChanges(False)
        self.assertEqual(giantEnv.ProblemSetup.liveChanges(), False)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_LIVE_CHANGES), "0")

        giantEnv.ProblemSetup.setLiveChanges(None)
        self.assertEqual(giantEnv.ProblemSetup.liveChanges(), True)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_LIVE_CHANGES), None)

        giantEnv.ProblemSetup.setLiveChanges(True)
        self.assertEqual(giantEnv.ProblemSetup.liveChanges(), True)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_LIVE_CHANGES), "1")

        for value in ["True", "", 0, [], {}]:
            self.assertRaises(TypeError, giantEnv.ProblemSetup.setMultiCombine, value)

    def test_multiCombine(self):
        self.assertEqual(giantEnv.ProblemSetup.multiCombine(), True)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_MULTI_COMBINE), None)

        giantEnv.ProblemSetup.setMultiCombine(False)
        self.assertEqual(giantEnv.ProblemSetup.multiCombine(), False)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_MULTI_COMBINE), "0")

        giantEnv.ProblemSetup.setMultiCombine(None)
        self.assertEqual(giantEnv.ProblemSetup.multiCombine(), True)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_MULTI_COMBINE), None)

        giantEnv.ProblemSetup.setMultiCombine(True)
        self.assertEqual(giantEnv.ProblemSetup.multiCombine(), True)
        self.assertEqual(os.getenv(giantEnv.GiantVarNames.GIANT_PROBLEM_SETUP_MULTI_COMBINE), "1")

        for value in ["True", "", 0, [], {}]:
            self.assertRaises(TypeError, giantEnv.ProblemSetup.setMultiCombine, value)
