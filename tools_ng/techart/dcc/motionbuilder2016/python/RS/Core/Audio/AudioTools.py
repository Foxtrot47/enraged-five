"""
Description:
    Various functions for syncing and setting up audio in Mobu.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import pyfbsdk as mobu

from RS import Globals, Config
from RS.Utils import Path
import RS.Perforce as P4
import RS.Utils.UserPreferences as userPref
from RS.Core.Automation.FrameCapture import CapturePaths
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Audio import Audio


def GetCurrentAudioFolders():
    """
    Detects audio folders based on the current FBX (including random event audio if needed).
    Returns:
        (List): of local paths to search for audio within
    """
    fbxPath = Globals.Application.FBXFileName.lower()
    capPaths = CapturePaths.FastPath(fbxPath)
    audioFolders = [capPaths.audioRoot]
    randomEventsFolder = os.path.join("ingame", "source", "script_re")
    if randomEventsFolder in fbxPath:
        reAudioRoot = os.path.join(capPaths.projectRoot, "art", "animation", "ingame", "renders", "script_re")
        audioFolders.append(reAudioRoot)
    return audioFolders


def FindPerforceAudio(filter="wav"):
    """
    Searches perforce for all WAVs in the audioFolder, and returns them in a list.
    Arguments:
        filter (string): optional filter text to include when searching perforce files
    Returns:
        (List): of all .wav files found within the audioFolders (quite large - 4701 files on 1/13/16)
    """
    audioPaths = []
    audioFolders = GetCurrentAudioFolders()
    for audioFolder in audioFolders:
        argsList = ["-e", "{0}...{1}".format(audioFolder, filter)]
        p4Files = P4.Run("files", args=argsList)
        for record in p4Files.Records:
            try:
                recordText = str(record["depotFile"])
            except:
                recordText = ""
            audioPaths.append(recordText)
    return audioPaths


def DeleteAudioTracksAndClips():
    """
    Deletes all audio tracks and clips within an FBX.
    """
    audioTracks = [track for track in Globals.Story.RootFolder.Tracks if track.Type == mobu.FBStoryTrackType.kFBStoryTrackAudio]
    for eachTrack in audioTracks:
        eachTrack.FBDelete()
    audioClips = list(Globals.Scene.AudioClips)
    for eachClip in audioClips:
        eachClip.FBDelete()


def SetupCutsceneAudio():
    """
    Detects, syncs, and updates audio clips in a cutscene FBX.
    """
    # Delete Old Tracks and Clips
    DeleteAudioTracksAndClips()

    # Detect/Sync Audio Path
    fbxName = Path.GetBaseNameNoExtension(Globals.Application.FBXFileName)
    if fbxName is "":
        return
    
    audioName = "{}.C.wav".format(fbxName)
    matchedPaths = FindPerforceAudio(audioName)
    if matchedPaths:

        # Sync Matched Path and Get Local Audio Path
        P4.Sync(matchedPaths[0])
        audioPath = str(P4.GetFileState(matchedPaths[0]).ClientFilename)

        # Verify Path Exists - skip for rare case where sync succeeds but file is missing
        if os.path.exists(audioPath):
            
            # Delete Old Audio References
            audioRefs = Manager().GetReferenceListByType(Audio)
            if len(audioRefs) > 0:
                # delete via ref editor
                Manager().DeleteReferences(audioRefs)
            
            # Create new Reference
            Manager().CreateReferences(audioPath)
            
            audioTracks = Globals.AudioClips
            
            if len(audioTracks) > 0:
                for track in audioTracks:
                    # Setup Audio Track
                    audioTrack = mobu.FBStoryTrack(mobu.FBStoryTrackType.kFBStoryTrackAudio)
                    audioTrack.AudioOutIndex = 0
                    audioTrack.Name = track.LongName
                    
                    # Setup Story Audio Clip
                    storyClip = track
                    mobu.FBStoryClip(track, audioTrack, mobu.FBTime(0, 0, 0, 0))
                    storyClip.LongName = track.LongName
                    storyDest = storyClip.PropertyList.Find("Destination")
                    storyDest.Data = 0
            return True
    return False

def SetupIngameAudio():
    """
    Detects, syncs, and updates audio in an ingame FBX.
    Differs from cutscene, as we detect and update existing clips.
    """
    success = False
    # Perforce Lists - so slower perforce calls only happen once when needed
    validAudioPaths = []
    latestAudioPaths = []

    # Detect Story Clips Missing Audio
    audioTrackType = mobu.FBStoryTrackType.kFBStoryTrackAudio
    audioTracks = [track for track in Globals.Story.RootFolder.Tracks if track.Type == audioTrackType]
    for audioTrack in audioTracks:
        emptyClips = []
        for storyClip in list(audioTrack.Clips):
            if storyClip.AudioClip is None:

                # Check Missing Clip Paths - skip if valid paths and create latestAudioPaths list (once)
                audioPath = storyClip.PropertyList.Find("ClipAudioPath").Data
                if audioPath.lower() not in validAudioPaths:
                    if not latestAudioPaths:
                        latestAudioPaths = FindPerforceAudio()

                    # Search For Latest Perforce Audio - using file name
                    audioName = os.path.basename(audioPath)
                    matchedPaths = [path for path in latestAudioPaths if path.lower().endswith(audioName.lower())]
                    if matchedPaths:

                        # Sync and Update Paths - via fast string formatting, slow method below
                        P4.Sync(matchedPaths[0])
                        audioPath = "X:{}".format(matchedPaths[0].replace("/", "\\")[1:])
                        # audioPath = str(P4.GetFileState(matchedPaths[0]).ClientFilename)
                        validAudioPaths.append(audioPath.lower())

                # Valid Audio Found - store data in a dict and delete old clip
                if os.path.exists(audioPath):
                    clipDict = {"ClipAudioPath": audioPath, "Start": storyClip.Start.Get(),
                                "Stop": storyClip.Stop.Get(), "MarkIn": storyClip.MarkIn.Get()}
                    emptyClips.append(clipDict)
                    storyClip.FBDelete()
                    success = True

        # Loop Through Empty Clips - and find or create matched audio clips
        for emptyClip in emptyClips:
            audioPath = emptyClip["ClipAudioPath"]
            matchedAudio = [clip for clip in Globals.System.Scene.AudioClips if clip.Path.lower() == audioPath.lower()]
            audioClip = matchedAudio[0] if matchedAudio else mobu.FBAudioClip(audioPath)

            # Create New Clip
            newTime = mobu.FBTime()
            newClip = mobu.FBStoryClip(audioClip, audioTrack, mobu.FBTime(0, 0, 0, -1))

            # Set Stop Time from Duration
            clipDuration = emptyClip["Stop"] - emptyClip["Start"]
            newTime.Set(newClip.Start.Get() + clipDuration)
            newClip.Stop = newTime
            # setattr(newClip, "Stop", newTime)

            # Set MarkIn and Start Times
            newTime.Set(emptyClip["MarkIn"])
            # setattr(newClip, "MarkIn", newTime)
            newClip.MarkIn = newTime
            newTime.Set(emptyClip["Start"])
            # setattr(newClip, "Start", newTime)
            newClip.Start = newTime
    return success

def AutoLoadAudio():
    """
    Detects a cutscene or ingame FBX and sets up audio accordingly.
    """
    # Check Cutscene or InGame FBX
    fbxPath = mobu.FBApplication().FBXFileName.lower()
    cutsceneFolderString = os.path.join("animation", "cutscene", "!!scenes")
    ingameFolderString = os.path.join("animation", "ingame", "source")
    cinematicsFolderString = os.path.join("animation", "cinematic", "!!scenes")

    # Run Setup Steps
    if cutsceneFolderString in fbxPath:
        return SetupCutsceneAudio()
    elif ingameFolderString in fbxPath:
        return SetupIngameAudio()
    elif cinematicsFolderString in fbxPath:
        return SetupCutsceneAudio()

    return False

def AudioFlipper(control, event):
    """
    Mutes main audio whenever story audio is playing, and vice versa.
    Used in conjuction with the RegisterAutoAudio callback below.

    TODO: By luck it seems, this callback only disables a clip set to the current take.
    Which has been working fine for cutscene and now ingame scenes, but it should
    definitely get some more work if we decide to roll it out to all users.
    """
    # Audio Flipper - Mutes main audio whenever story audio is playing, and vice versa.
    if Globals.Player.IsPlaying:
        userDest = userPref.__Readini__("audioTools", "audioDest", notFoundValue=None)
        if userDest and Globals.Scene.AudioClips:
            audioClip = Globals.Scene.AudioClips[0]
            destProp = audioClip.PropertyList.Find("Destination")
            if Globals.Story.Mute is True:
                if destProp.Data == 0:
                    # print "MAIN AUDIO ON"
                    destProp.Data = int(userDest)
            elif audioClip.Destination is not None:
                # print "MAIN AUDIO OFF"
                userPref.__Saveini__("audioTools", "audioDest", destProp.Data)
                destProp.Data = 0


def RegisterAutoAudio(enabled=True):
    """
    Callback using Mobu's OnUIIdle to run the AudioFlipper method above.
    WARNING: This callback runs constantly, and should only be used for special cases.
    """
    try:
        Globals.Callbacks.OnUIIdle.Remove(AudioFlipper)
    except Exception, e:
        print "AutoAudio Error: {}".format(e)
    if enabled:
        Globals.Callbacks.OnUIIdle.Add(AudioFlipper)
