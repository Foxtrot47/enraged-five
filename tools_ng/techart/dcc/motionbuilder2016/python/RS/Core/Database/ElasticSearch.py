import json
import os
import logging
from ssl import create_default_context
from elasticsearch import Elasticsearch, helpers, ElasticsearchException

# from Core import Config
# from Utils.Logging.Logging import Logger

# # Suppress SSL warning if needed
# warnings.filterwarnings("ignore")

# Set connection timeout to 5 seconds
TIMEOUT = 5
LOG = logging.getLogger(__name__)

# TechArt Server
class Server(object):
    DEV_SERVER = "https://8fdbfc86c772493ab58b723c15ee7331.lb.dev.elastic.rockstargames.com:9243"
    PROD_SERVER = "https://f3b67f29bcde40149f493a6c6e3fb1db.lb.prod.elastic.rockstargames.com:9243"


class SearchIndex(object):
    """This is kind of representing the database"""
    UNIT_TEST = "telemetry_unit_test"
    MOBU_TOOLS = "telemetry_Motionbuilder_Tools_Tracking"
    # MOBU_LAUNCHER =


class DocType(object):
    """This is representing like a collection/table in the database."""
    UNIT_TEST = "unit_test"
    TOOLS_TRACKING = "Motionbuilder_TA_tool"


class ElasticSearch(object):
    # USER = "admin"
    # PASSWORD = "82zFvkS8CZdqNLjZ"

    certFile = os.path.join(os.environ.get("RS_TOOLSROOT", "X:/americas/tools/release"), "techart", "etc", "config", "database", "T2RootCert.cer")
    CONTEXT = create_default_context(cafile=certFile)

    def __init__(self, searchIndex, docType, mapping=None, server=Server.PROD_SERVER):
        self._searchIndex = searchIndex.lower()
        self._docType = docType.lower()
        self._mapping = mapping
        self._server = server
        
        LOG.info("Connecting to: {}".format(self._searchIndex))
        user = "admin"
        if self._server == Server.DEV_SERVER:
            password = "82zFvkS8CZdqNLjZ"
        else:
            password = "5fSM6jFuLwx83Q3m"
        self._db = Elasticsearch([self._server],
                                ssl_context=self.CONTEXT,
                                http_auth=(user, password),
                                timeout=TIMEOUT)
        # Check if passed index exists and if not create one.
        self.createIndex()

    @property
    def server(self):
        return self._server
        
    @property
    def db(self):
        return self._db

    def createIndex(self):
        if not self._db.indices.exists(self._searchIndex):
            LOG.info("Creating new Index: {}\n\n\n\n".format(self._searchIndex))
            # Mapping tells elasticsearch what kind of data each filed contains
            if not self._mapping:
                self._mapping = "{\"mappings\":{\"" + self._docType + "\":{}}}"

            LOG.info("Using Mappings:\n")
            LOG.info(self._mapping)
            LOG.info("\n")

            self._db.indices.create(index=self._searchIndex, ignore=400, body=self._mapping, )

    def addData(self, data):
        try:
            LOG.info("Sending to Elasticsearch {0}".format(data))
            result = self._db.index(index=self._searchIndex, doc_type=self._docType, body=data)
            LOG.info('Completed succesfully!')
        except ElasticsearchException as error:
            LOG.error("[Warning] ElasticSearch DB problem.")
            LOG.error(error)
        except Exception as error:
            LOG.error("Error during elasticsearch upload")
            LOG.error(error)

        return result

    def DeleteIndex(self, indexName):
        """Delete the entire database/index.

        Args:
            indexName (str):

        Returns:
        """
        if self._db.indices.exists(indexName):
            self._db.indices.delete(index=indexName)




