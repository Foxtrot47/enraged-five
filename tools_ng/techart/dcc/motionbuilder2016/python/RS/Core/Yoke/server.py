"""
Server side for Yoke, a signal based client/server messageing system
"""
import socket
import functools
import subprocess

from PySide import QtCore, QtGui, QtNetwork

from RS.Core.Yoke import cipher


class YokeServer(QtCore.QObject):
    """
    Yoke Server class, to allow Yoke Clients to connect and emit signals as messages are sent and recived

    Signals:
        clientConnected(QTcpSocket) : New client has been connected to
        clientDisconnected(QTcpSocket) : client has been disconnected
        messageRecived(Message) : Message has been received from a client, along with the message as a Message object
        messageSent(Message) : Message has been sent to the clients, along with the message as a Message object
        clientError(QTcpSocket, QAbstractSocket.SocketError) : The socket and its error
    """
    clientConnected = QtCore.Signal(object)
    clientDisconnected = QtCore.Signal(object)
    messageRecived = QtCore.Signal(object, object)
    messageSent = QtCore.Signal(object)
    clientError = QtCore.Signal(object, object)

    def __init__(self, address=None, port=None):
        """
        Constructor:

        args:
            address (str): IP address to connect to the server on
            port (int): Port to use to connect to the server
        """
        super(YokeServer, self).__init__()
        self._connectedClients = []

        self._tcpServer = None
        self.setupServer(address=address, port=port)

    def setupServer(self, address=None, port=8888):
        if self._tcpServer is not None:
            self.closeAllConnections()
            self._tcpServer.newConnection.disconnect(self._handleNewConnection)
            self._connectedClients = []

        self._connectedIpAddress = address or self._getLocalIpAddress()
        self._connectedPort = port or 8888

        self._tcpServer = QtNetwork.QTcpServer()
        self._tcpServer.newConnection.connect(self._handleNewConnection)
        self._tcpServer.listen(QtNetwork.QHostAddress(self._connectedIpAddress), self._connectedPort)

    def getCurrentIpAddress(self):
        return self._connectedIpAddress

    def getLocalIpAddresses(self):
        ips = subprocess.check_output(['hostname', '--all-ip-addresses'])
        return [ip.strip() for ip in ips.split(" ") if ip.strip() != ""]

    def _getLocalIpAddress(self):
        """
        Internal Method

        Get the Ip address of the host computer

        returns:
            string of the IP address of this computer
        """
        allIps = self.getLocalIpAddresses()
        if len(allIps) > 0:
            return allIps[0]
        return '0.0.0.0'

    def sendMessage(self, messageType, message, ignoreConnection=None, connections=None):
        """
        Send Message to all clients

        args:
            messageType (int): The message Type to send
            message (object): The message contents to sent

        kwargs:
            ignoreConnection (QTcpSocket): any connections not to send to
            connections (List of QTcpSocket): List of connections to send the message to
        """
        msg = cipher.Message(messageType, message)
        encodedMessage = msg.encodeMessage()

        for client in connections or self._connectedClients:
            if client == ignoreConnection:
                continue
            client.write(encodedMessage)
        self.messageSent.emit(msg)

    def closeAllConnections(self):
        """
        Disconnect all currently connected clients
        """
        for connectedClient in self._connectedClients:
            connectedClient.disconnectFromHost()
        self._tcpServer.close()

    def listenForConnections(self):
        """
        Start listening for new connections
        """
        self._tcpServer.listen()

    def getConnectedClients(self):
        """
        Get a list of all the currently connected clients

        return:
            list of QTcpSocket objects which are connected
        """
        return self._connectedClients

    def _handleRecieveMessage(self, connection):
        """
        Internal method

        Handle a receiving a new message a the client
        """
        while connection.canReadLine() is True:
            msg = cipher.Message.decodeMessage(str(connection.readLine(9999)))
            self.messageRecived.emit(connection, msg)

    def _handleNewConnection(self):
        """
        Internal method

        Handle a new client connection
        """
        tcpServerConnection = self._tcpServer.nextPendingConnection()
        tcpServerConnection.readyRead.connect(functools.partial(self._handleRecieveMessage, tcpServerConnection))
        tcpServerConnection.error.connect(functools.partial(self._handleConnectionError, tcpServerConnection))
        self._connectedClients.append(tcpServerConnection)

        self.clientConnected.emit(tcpServerConnection)

    def _handleConnectionError(self, connection, socketError):
        """
        Internal method

        Handle a connection error
        """
        if socketError == QtNetwork.QTcpSocket.RemoteHostClosedError:
            self._connectedClients.remove(connection)
            self.clientDisconnected.emit(connection)
            return
        self.clientError(connection, socketError)
