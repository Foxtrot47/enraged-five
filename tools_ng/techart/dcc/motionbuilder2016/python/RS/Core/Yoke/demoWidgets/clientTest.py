"""
CLIENT TEST FILE, FOR DEMO PURPOSES ONLY!!
"""

import socket
import functools

from PySide import QtCore, QtGui, QtNetwork
from RS.Core.Yoke import client, cipher


class MessageTypes:
    ClientData = 0
    TakeData = 1
    DebugMessage = 99


class ClientData:
    Name = 0
    ReadyToShot = 1
    InSync = 2


class TakeData:
    Name = 0
    Description = 1
    CallSheet = 2


class ClientDialog(QtGui.QDialog):
    def __init__(self, address='127.0.0.1', port=8888, parent=None):
        super(ClientDialog, self).__init__(parent)
        self.setObjectName(self.__class__.__name__)
        self._yokeClient = client.YokeClient(address=address, port=port)
        self._yokeClient.messageRecived.connect(self._recieveMessage)
        self.setupUi()
        self._yokeClient.serverConnected.connect(self._sendWholeClientUpdate)

        self._cachedDescription = None
        self._cachedCallsheet = None
        self._diffDescription = None
        self._diffCallsheet = None

    def _sendWholeClientUpdate(self):
        msg = {
                ClientData.ReadyToShot: self._readyToShoot.isChecked(),
                ClientData.InSync: self._inSync.isChecked(),
                ClientData.Name: self._hostName.text()
                }
        print "NEW CONNECTION"
        self._yokeClient.sendMessage(MessageTypes.ClientData, msg)

    def setupUi(self):
        mainLayout = QtGui.QGridLayout()
        self._takeName = QtGui.QLineEdit()
        self._callSheet = QtGui.QTextEdit() # QtGui.QWidget()  # TEMP
        self._description = QtGui.QTextEdit()
        self._hostName = QtGui.QLineEdit()
        self._readyToShoot = QtGui.QCheckBox("Ready to Shoot")
        self._inSync = QtGui.QCheckBox("In Sync")
        self._saveChanges = QtGui.QPushButton("Save Changes")

        self._inSync.setChecked(True)
        self._readyToShoot.setChecked(True)
        self._inSync.setEnabled(False)

        self._takeName.setReadOnly(True)
        self._hostName.setText("TestBox")

        mainLayout.addWidget(self._takeName, 0, 1, 1, 2)
        mainLayout.addWidget(self._inSync, 0, 3)
        mainLayout.addWidget(self._description, 1, 0, 1, 2)
        mainLayout.addWidget(self._callSheet, 1, 2, 1, 2)
        mainLayout.addWidget(self._hostName, 2, 0, 1, 2)
        mainLayout.addWidget(self._saveChanges, 2, 2)
        mainLayout.addWidget(self._readyToShoot, 2, 3)

        self.setLayout(mainLayout)

        self._hostName.textChanged.connect(self._handleHostNameChange)
        self._readyToShoot.stateChanged.connect(self._handleReadyToShootChange)
        self._inSync.stateChanged.connect(self._handleInSyncChange)
        self._saveChanges.pressed.connect(self._handleSaveChanges)
        self._description.textChanged.connect(self._handleDescriptionChange)

    def _handleDescriptionChange(self):
        self._editedState(True)

    def _editedState(self, state):
        if state is True:
            self.setStyleSheet("#%s {background-color: rgb(170, 255, 127);}" % (self.__class__.__name__))
            self._saveChanges.setEnabled(True)
        else:
            self.setStyleSheet("#%s {}" % (self.__class__.__name__))
            self._saveChanges.setEnabled(False)

    def _handleReadyToShootChange(self, newState):
        msg = {ClientData.ReadyToShot:self._readyToShoot.isChecked()}
        self._yokeClient.sendMessage(MessageTypes.ClientData, msg)

    def _handleInSyncChange(self, newState):
        msg = {ClientData.InSync:self._inSync.isChecked()}
        self._yokeClient.sendMessage(MessageTypes.ClientData, msg)

    def _handleHostNameChange(self, newText):
        self._updateHostName(newText)

    def _updateHostName(self, newHostName):
        msg = {ClientData.Name:newHostName}
        self._yokeClient.sendMessage(MessageTypes.ClientData, msg)
        
    def setHostName(self, newHostName):
        self._hostName.setText(newHostName)
        self.setWindowTitle(newHostName)

    def _handleSaveChanges(self):
        self._submitChanges()
        self._editedState(False)

    def _submitChanges(self):
        msg = {TakeData.Description: self._description.toPlainText()}
        self._yokeClient.sendMessage(MessageTypes.TakeData, msg)

    def _recieveMessage(self, msg):
        if msg.messageType == MessageTypes.TakeData:
            self._handleTakeDataUpdate(msg.messageContents)

    def _handleTakeDataUpdate(self, dataDict):
        for key, value in dataDict.iteritems():
            if key == TakeData.Name:
                self._takeName.setText(value)
#                 self._updateClientName(connection, value)
            elif key == TakeData.Description:
                self._description.setPlainText(value)
#                 self._updateClientReadyToShoot(connection, value)
            elif key == TakeData.CallSheet:
                pass
#                 self._updateClientInSync(connection, value)
        self._editedState(False)


if __name__ == '__main__':

    import sys

    app = QtGui.QApplication(sys.argv)
    dialog = ClientDialog()
    dialog.show()
    sys.exit(dialog.exec_())
