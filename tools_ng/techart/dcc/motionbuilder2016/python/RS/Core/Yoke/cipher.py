"""
Module to encode and decode, across the wire, data messages for Yoke
"""
import pickle
import codecs
import binascii


class Message(object):
    """
    Message class to encode/decode a message and its type
    """
    _SPLITTER = "::"
    _LINE_BREAK_REPLACEMENT = "\^n"

    def __init__(self, messageType, messageContents):
        """
        Args:
            messageType (int): The type of message this is
            messageContents (object): the information, can be a dict, string, tuple, list
        """
        self._messageType = messageType
        self._messageContents = messageContents

    @property
    def messageType(self):
        return self._messageType

    @messageType.setter
    def messageType(self, value):
        """
        Property

        Set the message Type as an int
        """
        self._messageType = value

    @property
    def messageContents(self):
        return self._messageContents

    @messageContents.setter
    def messageContents(self, value):
        self._messageContents = value

    @classmethod
    def _encodeMessage(cls, messageType, messageData):
        # step 1: serialize to bytes/string
        # NOTE: In Python3 using protocol 2 yields errors when trying to serialize anything with empty strings
        #       as converting that to a bytes object is invalid it seems, keeping it at 1 we can maintain communication
        #       between 2.7 versions of yoke and 3.7 versions
        messageDataStr = pickle.dumps(messageData, protocol=2)
        # step 2: encode to base 64 (this is to ensure compatibility with py3 version of yoke)
        messageDataStr = codecs.encode(messageDataStr, "base64")
        # step 3: replace any line break instances with a temporary replacement
        messageDataStr = messageDataStr.replace("\n", cls._LINE_BREAK_REPLACEMENT)
        # step 4: add a line break at the end
        encoded = "{0}{1}{2}\n".format(messageType, cls._SPLITTER, messageDataStr)
        return encoded

    @classmethod
    def _decodeMessage(cls, encodedMessage):
        # extract the type and msg data
        messageType, messageData = encodedMessage.split(cls._SPLITTER, 1)
        messageType = int(messageType)
        # remove the added line break
        messageData = messageData.replace("\r\n", "")
        # replace temporary line break replacements
        messageData = messageData.replace(cls._LINE_BREAK_REPLACEMENT, "\n")

        # leaving this in here in order to be able to quickly debug if this triggers an exception in the near future
        # we were getting errors with encoding / decoding empty string messages, but the problem seemed to go away
        # when restarting the machines...
        # try:
        if True:
            # decode from base64
            messageData = codecs.decode(messageData, "base64")
            # unpickle the data
            messageData = pickle.loads(messageData)
        # except binascii.Error:
        #     messageData = ""

        return messageType, messageData

    def encodeMessage(self):
        return self._encodeMessage(self._messageType, self._messageContents)

    @classmethod
    def decodeMessage(cls, encodedMessage):
        """
        return:
            the message as a Message instance
        """
        msgType, msgTxt = cls._decodeMessage(encodedMessage)
        return Message(msgType, msgTxt)
