"""
Client side for Yoke, a signal based client/server messageing system
"""
import socket
import functools
import time

from PySide import QtCore, QtGui, QtNetwork

from RS.Core.Yoke import cipher


class PingWorker(QtCore.QThread):
    """
    Thread worker to allow for retrying a connection if lost
    """
    ping = QtCore.Signal()

    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent=parent)
        self._stop = False
        self._isRunning = False

    def stopPing(self):
        """
        Stop the ping
        """
        self._stop = True
        self._isRunning = False
        
    def resetPing(self):
        """
        Reset the ping
        """
        self._stop = False
    
    def run(self):
        """
        Main logic to emit the ping or stop if the thread is done
        """
        self._isRunning = True
        while(True):
            time.sleep(5)
            if self._stop is True:
                break
            self.ping.emit()
        self._isRunning = False
    
    def isCurrentlyRunning(self):
        return self._isRunning


class YokeClient(QtCore.QObject):
    """
    Yoke Client class, to connect to a Yoke Server and emit signals as messages are sent and recived
    
    Signals:
        serverConnected : New Server has been connected to
        ServerDiconnected : Server has been disconnected
        messageRecived(Message) : Message has been received from the server, along with the message as a Message object
        messageSent(Message) : Message has been sent to the server, along with the message as a Message object
    """
    serverConnected = QtCore.Signal()
    serverDisconnected = QtCore.Signal()
    messageRecived = QtCore.Signal(object)
    messageSent = QtCore.Signal(object)
    
    def __init__(self, address='127.0.0.1', port=8888):
        """
        Constructor:
        
        kargs:
            address (str): IP address to connect to the server on
            port (int): Port to use to connect to the server
        """
        super(YokeClient, self).__init__()
        self._address = address
        self._port = port
        
        self._pingThread = QtCore.QThread()
        self._pingWorker = PingWorker()
        self._pingWorker.moveToThread(self._pingThread)
        self._pingWorker.ping.connect(self._connectToServer)
        self._pingWorker.finished.connect(self._pingThread.quit)
        self._pingWorker.finished.connect(self._pingWorker.deleteLater)
        self._pingWorker.finished.connect(self._pingThread.deleteLater)
        
        self._tcpClient = QtNetwork.QTcpSocket()
        self._tcpClient.readyRead.connect(self._handleRecieveMessage)
        self._tcpClient.connected.connect(self._handleConnectionConnected)
        self._tcpClient.disconnected.connect(self._handleDisconnect)
        
        self._connectToServer()

    def setAddress(self, address=None, port=None):
        """
        Set the address or port to connect to
        
        kwargs:
            address (str): IP address to connect to the server on
            port (int): Port to use to connect to the server
        """
        self._pingWorker.stopPing()
        if address is not None:
            self._address = address
        if port is not None:
            self._port = port
        
        if self.isConnected():
            self.disconnect()
        self._connectToServer()

    def getAddress(self):
        """
        Get the address and port that the client is trying to connect to
        
        return:
            str, int for the ipAddress and port
        """
        return (self._address, self._port)

    def sendMessage(self, messageType, message):
        """
        Send Message to the server.
        
        args:
            messageType (int): The messageType
            message (object): The object to send to the server
        """
        msg = cipher.Message(messageType, message)
        self._tcpClient.write(msg.encodeMessage())
        self.messageSent.emit(msg)

    def disconnect(self):
        """
        Disconnect from the current server
        """
        self._tcpClient.disconnected.disconnect(self._handleDisconnect)
        self._tcpClient.disconnectFromHost()
        self._tcpClient.disconnected.connect(self._handleDisconnect)
        self.serverDisconnected.emit()

    def isConnected(self):
        """
        Check if the client is connected to the server
        
        returns:
            bool if connected
        """
        return self._tcpClient.state() == QtNetwork.QTcpSocket.ConnectedState

    def _connectToServer(self):
        """
        Internal Method
        
        Connect to the server, retrying the connection if not able to connect
        """
        self._pingWorker.resetPing()
        if self._tcpClient.state() not in [QtNetwork.QTcpSocket.ConnectingState, QtNetwork.QTcpSocket.ConnectedState]:
            self._tcpClient.connectToHost(QtNetwork.QHostAddress(self._address), self._port)
        
        if self._tcpClient.state() == QtNetwork.QTcpSocket.ConnectedState:
            self._pingWorker.stopPing()
            return
        
        if not self._pingWorker.isRunning():
            self._pingWorker.start()
                
    def _handleConnectionConnected(self):
        """
        Internal method
        
        Handle a new connection being made
        """
        self.serverConnected.emit()
        self._pingWorker.stopPing()

    def _handleDisconnect(self):
        """
        Internal method
        
        Handle a connection being disconnected
        """
        self._connectToServer()
        self.serverDisconnected.emit()

    def _handleRecieveMessage(self):
        """
        Internal method
        
        Handle a receiving a new message from the server
        """
        while self._tcpClient.canReadLine() is True:
            msg = cipher.Message.decodeMessage(str(self._tcpClient.readLine(9999)))
            self.messageRecived.emit(msg)