"""
SERVER TEST FILE, FOR DEMO PURPOSES ONLY!!
"""

import socket
import functools

from PySide import QtCore, QtGui, QtNetwork
from RS.Core.Yoke import server, cipher


class MessageTypes:
    ClientData = 0
    TakeData = 1
    DebugMessage = 99


class ClientData:
    Name = 0
    ReadyToShot = 1
    InSync = 2


class TakeData:
    Name = 0
    Description = 1
    CallSheet = 2


class ConnectedClientsWidget(QtGui.QTreeWidget):
    def __init__(self, parent=None):
        super(ConnectedClientsWidget, self).__init__(parent=parent)
        self.setHeaderLabels(["Name", "Ready?", "In Sync"])

class ServerDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(ServerDialog, self).__init__(parent)
        self._yokeServer = server.YokeServer()
        self.setupUi()

    def setupUi(self):
        mainLayout = QtGui.QGridLayout()
        self._takeName = QtGui.QLineEdit()
        self._callSheet = QtGui.QTextEdit()  # QtGui.QWidget()  # TEMP
        self._description = QtGui.QTextEdit()
        self._clients = ConnectedClientsWidget()
        self._saveChanges = QtGui.QPushButton("Save Changes")
        self._captureButton = QtGui.QPushButton("Shoot!")

        mainLayout.addWidget(self._takeName, 0, 0, 1, 2)
        mainLayout.addWidget(self._callSheet, 1, 0, 1, 2)
        mainLayout.addWidget(self._description, 2, 0, 1, 2)
        mainLayout.addWidget(self._saveChanges, 3, 0, 1, 1)

        mainLayout.addWidget(self._clients, 0, 2, 3, 2)
        mainLayout.addWidget(self._captureButton, 3, 2, 1, 2)

        self._yokeServer.clientConnected.connect(self._addNewConnectionToList)
        self._yokeServer.clientDisconnected.connect(self._removeConnectionFromList)
        self._yokeServer.messageRecived.connect(self._recieveMessage)
        self._saveChanges.pressed.connect(self._handleSaveChanges)

        self.setLayout(mainLayout)

    def _addNewConnectionToList(self, newConnection):
        ipAddress = newConnection.peerAddress().toString()
        hostName, _, _ = socket.gethostbyaddr(ipAddress)
        newItem = QtGui.QTreeWidgetItem(0, "{0} ({1})".format(hostName, ipAddress))
        newItem.setData(0, QtCore.Qt.UserRole, newConnection)
        newItem.setCheckState(1, QtCore.Qt.CheckState.Unchecked)
        newItem.setCheckState(2, QtCore.Qt.CheckState.Unchecked)
        self._clients.addTopLevelItem(newItem)

    def _removeConnectionFromList(self, connection):
        for idx in xrange(self._clients.topLevelItemCount()):
            item = self._clients.topLevelItem(idx)
            if item.data(0, QtCore.Qt.UserRole) == connection:
                self._clients.takeTopLevelItem(idx)
                return

    def _recieveMessage(self, connection, msg):
        if msg.messageType == MessageTypes.ClientData:
            self._updateClientLogic(connection, msg.messageContents)
        elif msg.messageType == MessageTypes.TakeData:
            self._updateClientData(connection, msg.messageContents)
        elif msg.messageType == MessageTypes.DebugMessage:
            print msg.messageContents

    def _updateClientData(self, connection, dataDict):
        needsUpdate = False
        for key, value in dataDict.iteritems():
            if key == TakeData.Name:
                pass
#                 self._updateClientName(connection, value)
            elif key == TakeData.Description:
                self._description.setText(value)
                needsUpdate = True
#                 self._updateClientReadyToShoot(connection, value)
            elif key == TakeData.CallSheet:
                pass
#                 self._updateClientInSync(connection, value)

        if needsUpdate is True:
            self._submitChanges(connection)

    def _handleSaveChanges(self):
        self._submitChanges()

    def _submitChanges(self, ignoreConnection=None):
        msg = {
            TakeData.Name: self._takeName.text(),
            TakeData.Description: self._description.toPlainText(),
                }
        self._yokeServer.sendMessage(MessageTypes.TakeData, msg, ignoreConnection=ignoreConnection)

    def _updateClientLogic(self, connection, dataDict):
        for key, value in dataDict.iteritems():
            if key == ClientData.Name:
                self._updateClientName(connection, value)
            elif key == ClientData.ReadyToShot:
                self._updateClientReadyToShoot(connection, value)
            elif key == ClientData.InSync:
                self._updateClientInSync(connection, value)

    def _getClientFromConnection(self, connection):
        for idx in xrange(self._clients. topLevelItemCount()):
            item = self._clients.topLevelItem(idx)
            if item.data(0, QtCore.Qt.UserRole) == connection:
                return item

    def _boolToCheckState(self, val):
        if val is True:
            return QtCore.Qt.CheckState.Checked
        return QtCore.Qt.CheckState.Unchecked

    def _updateClientInSync(self, connection, newState):
        self._getClientFromConnection(connection).setCheckState(2, self._boolToCheckState(newState))

    def _updateClientReadyToShoot(self, connection, newState):
        self._getClientFromConnection(connection).setCheckState(1, self._boolToCheckState(newState))

    def _updateClientName(self, connection, newName):
        self._getClientFromConnection(connection).setText(0, newName)

    def displayError(self, connection, socketError):
        if socketError == QtNetwork.QTcpSocket.RemoteHostClosedError:
            ipAddress = connection.peerAddress().toString()
            hostName, _, _ = socket.gethostbyaddr(ipAddress)
            self._removeConnectionFromList(connection)
            self._addClientMessage("Client {0} ({1}) disconnected".format(hostName, ipAddress))
            return

        QtGui.QMessageBox.information(self, "Network error",
                "The following error occured: %s." % self.tcpClient.errorString())
#

if __name__ == '__main__':

    import sys

    app = QtGui.QApplication(sys.argv)
    dialog = ServerDialog()
    dialog.show()
    sys.exit(dialog.exec_())
