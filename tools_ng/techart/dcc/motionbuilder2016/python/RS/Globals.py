import gc
import types
import logging
import traceback

import pyfbsdk as mobu

from RS import ProjectData

from RS.Utils.Logging import Universal
from RS.Tools.CameraToolBox.PyCore.Decorators import Singleton

logging.getLogger(__name__)

Ulog = Universal.UniversalLog('MotionBuilder')

WingRemoteWatchFile = r'x:\mobuRunRemoteScript.txt'

Quiet = False

Application = mobu.FBApplication()
App = Application
System = mobu.FBSystem()
Scene = System.Scene
ActorFaces = Scene.ActorFaces
Actors = Scene.Actors
AudioClips = Scene.AudioClips
Cameras = Scene.Cameras
CharacterExtensions = Scene.CharacterExtensions
CharacterFaces = Scene.CharacterFaces
CharacterPoses = Scene.CharacterPoses
Characters = Scene.Characters
ConstraintSlovers = Scene.ConstraintSolvers
Constraints = Scene.Constraints
ControlSets = Scene.ControlSets
Devices = Scene.Devices
Folders = Scene.Folders
Groups = Scene.Groups
Handles = Scene.Handles
Huds = Scene.HUDs
Lights = Scene.Lights
MarkerSets = Scene.MarkerSets
Materials = Scene.Materials
Namespaces = Scene.Namespaces
Notes = Scene.Notes
ObjectPoses = Scene.ObjectPoses
PhysicalProperties = Scene.PhysicalProperties
Poses = Scene.Poses
RootModel = Scene.RootModel
Sets = Scene.Sets
Shaders = Scene.Shaders
Story = mobu.FBStory()
Takes = Scene.Takes
Textures = Scene.Textures
UserObjects = Scene.UserObjects
VideoClips = Scene.VideoClips


class _Components(object):
    """
    Wrapper for the Scene.Components object
    The Dynamxyz Bridge plugins crashes when this is initialized at startup
    """
    def __iter__(self):
        """
        overrides built in method

        Allows the instance to be iterate over all the objects in the scene

        Returns:
            generator
        """
        for model in Scene.Components:
            yield model

    def __len__(self):
        """ overrides built in method """
        return len(Scene.Components)

    def __getitem__(self, start):
        if not isinstance(start, slice):
            return Scene.Components[start]

        start, stop, step = start.start, start.stop, start.step
        start = start or 0
        step = step or 1
        models = Scene.Components
        count = len(models)

        if start < 0:
            start += count

        if stop < 0:
            stop += count
        else:
            stop -= 1

        modelSlice = []
        if start < stop:
            counter = start
            while counter >= 0 and counter <= stop:
                modelSlice.append(models[counter])
                counter += step
        return modelSlice


class SceneModels(object):
    """ Exposes methods that return lists of models in the current scene """

    @property
    def All(self):
        """ Gets all the FBModels in the scene """
        models = mobu.FBModelList()
        mobu.FBGetSelectedModels(models, None, True, True)
        mobu.FBGetSelectedModels(models, None, False, True)
        return models

    @property
    def Selected(self):
        """ Gets all the selected models in the scene """
        models = mobu.FBModelList()
        mobu.FBGetSelectedModels(models, None, True, True)
        return models

    @property
    def UnSelected(self):
        """ Gets all the unselected models in the scene """
        models = mobu.FBModelList()
        mobu.FBGetSelectedModels(models, None, False, True)
        return models

    def __iter__(self):
        """
        overrides built in method

        Allows the instance to be iterate over all the objects in the scene

        Returns:
            generator
        """
        for model in self.All:
            yield model

    def __len__(self):
        """ overrides built in method """
        return len(self.All)

    def __getitem__(self, start):
        if not isinstance(start, slice):
            return self.All[start]

        start, stop, step = start.start, start.stop, start.step
        start = start or 0
        step = step or 1
        models = self.All
        count = len(models)

        if start < 0:
            start += count

        if stop < 0:
            stop += count
        else:
            stop -= 1

        modelSlice = []
        if start < stop:
            counter = start
            while counter >= 0 and counter <= stop:
                modelSlice.append(models[counter])
                counter += step
        return modelSlice


# This is NOT a list
Models = SceneModels()
Components = _Components()

ComponentLists = {
                     mobu.FBActor: Actors,
                     mobu.FBActorFace: ActorFaces,
                     mobu.FBCamera: Cameras,
                     mobu.FBCharacter: Characters,
                     mobu.FBCharacterExtension: CharacterExtensions,
                     mobu.FBCharacterPose: CharacterPoses,
                     mobu.FBComponent: Components,
                     mobu.FBConstraint: Constraints,
                     mobu.FBConstraintSolver: ConstraintSlovers,
                     mobu.FBControlSet: ControlSets,
                     mobu.FBDevice: Devices,
                     mobu.FBFolder: Folders,
                     mobu.FBLight: Lights,
                     mobu.FBMarkerSet: MarkerSets,
                     mobu.FBMaterial: Materials,
                     mobu.FBMotionClip: Scene.MotionClips,
                     mobu.FBNote: Notes,
                     mobu.FBObjectPose: ObjectPoses,
                     mobu.FBSet: Sets,
                     mobu.FBShader: Shaders,
                     mobu.FBTake: Takes,
                     mobu.FBTexture: Textures,
                     mobu.FBVideoClip: VideoClips,
                     mobu.FBGroup: Groups
                    }

Player = mobu.FBPlayerControl()
CameraSwitcher = mobu.FBCameraSwitcher()
FileMonitor = mobu.FBFileMonitoringManager()


# Adding in an instance of FBEvaluateManager so only 1 exists in the scene
EvaluateManager = mobu.FBEvaluateManager()

# The .FullName of any component that is 'default' or 'builtin'. Trying to delete these things will
# cause crashes in some situations. Best avoid trying to delete them all together. We do this by name
# rather than storing the actual references because at different times in code excution (i.e. during startup)
# some of these items won't exist 'yet'. This list is created by loading Mobu - allowing it to pass
# startup then printing all components at that point - before opening a scene or anything else.
DefaultComponentNames = ["Model::Scene",
                         "Core",
                         "Thread Manager",
                         "Evaluation Manager",
                         "Timing Manager",
                         "Command Manager",
                         "KTimeWarpManager",
                         "Profiler",
                         "Evaluation",
                         "Transport",
                         "Timer",
                         "Audio",
                         "Video",
                         "Material::DefaultMaterial",
                         "Model::Producer Perspective",
                         "Model::Producer Front",
                         "Model::Producer Back",
                         "Model::Producer Right",
                         "Model::Producer Left",
                         "Model::Producer Top",
                         "Model::Producer Bottom",
                         "Model::Camera Switcher",
                         "Video::USB Video Device",
                         "Video::Video Output 1",
                         "KVideoRenderer",
                         "KSerialManager",
                         "KCharacterHelper",
                         "Network Evaluation",
                         "KNLEManager",
                         "BaseObjectGroup::Path Creation",
                         "Folder::Constraints",
                         "Audio::Speakers (High Definition Audio Device)",
                         "Audio::Microphone (3- USB Audio Device)",
                         "KAudioManager",
                         "KMotionTriggerManager",
                         "Timeline::Story root",
                         "Timeline::Edit root",
                         "TimelineTrack::Shot Track",
                         "KTimelineXManager",
                         "Settings::Ghosts",
                         "Scene",
                         "Actors",
                         "Marker Set",
                         "Actor Faces",
                         "Audio",
                         "Cameras",
                         "Characters",
                         "Character Faces",
                         "Character Plugins",
                         "Control Rigs",
                         "Character Extensions",
                         "Constraints",
                         "Decks",
                         "Devices",
                         "Handles",
                         "Image Operators",
                         "Groups",
                         "Sets",
                         "Namespaces",
                         "Heads Up Display",
                         "Heads Up Display Elements",
                         "Lights",
                         "Materials",
                         "Keying Groups",
                         "Notes",
                         "Opticals",
                         "Poses",
                         "Shaders",
                         "Takes",
                         "Textures",
                         "3D Curve",
                         "Motions",
                         "Videos",
                         "Physical Properties",
                         "Solvers",
                         "Deformer",
                         "Scripts",
                         "UserObjects",
                         "System",
                         "BaseObjectGroup::T",
                         "BaseObjectGroup::R",
                         "BaseObjectGroup::S",
                         "BaseObjectGroup::TR",
                         "BaseObjectGroup::TRS",
                         "PointCacheManager",
                         "Folder::Poses",
                         "Folder::Takes",
                         "Global Light",
                         "Shader::Default Shader",
                         "Renderer::Renderer",
                         "FBX Export",
                         "MainViewer",
                         "ModelView0",
                         "ModelView1",
                         "ModelView2",
                         "ModelView3",
                         "Geometry::__SYSTEM_DEFAULT_GEOMETRY_AZIMUT_",
                         "HierarchyView",
                         "BaseObjectGroup::Current Camera",
                         "Transport",
                         "FCurve",
                         "Application"]

class CallbackError(Exception):
    """ Raises all the exceptions encountered in a callback in a single exception """
    def __init__(self, message, exceptions=None):
        """
        Constructor

        Arguments:
            message (string): message to include in exception
            exceptions (list): list of exceptions to raise together
        """
        super(CallbackError, self).__init__(message)
        self._message = message or ""
        self._exceptions = exceptions or []
        for index, exception in enumerate(self._exceptions):
            self._message = "{}\n\n## Exception {}{}\n\n".format(self._message, index+1, exception)

    def __str__(self):
        return self._message.strip()


class CallbackWrapper(object):
    """
    Wraps a callback so that the logic for running the methods is done by python rather than through C++

    It also keeps the number of callbacks on the actual callback to a minimum
    """
    __cache__ = {}
    __externalEnabled = True

    def __new__(cls, callback):
        """
        Overrides built-in method

        If caches the callback so the same instance of the wrapper is returned

        Arguments:
            callback (pyfbsdk.FBCallback): callback to cache
        """
        instance = cls.__cache__.get(callback, None)
        if instance is None:
            instance = object.__new__(cls)
        return instance

    def __init__(self, callback):
        """
        Constructor

        Arguments:
            callback (pyfbsdk.FBCallback): callback to cache
        """

        instance = self.__class__.__cache__.get(callback, None)
        if instance is not None:
            return

        self.__class__.__cache__[callback] = self
        self._enabled = True
        self._functions = []
        self._callback = callback
        self._callback.Add(self._execute)

    def _execute(self, control, event):
        """
        Method attached to the callback that runs the stored functions

        Arguments:
            control (pyfbsdk.FBControl): UI or component that initiated the callback
            event (pyfbsdk.FBEvent): the event that triggered the callback
        """
        if not self.__externalEnabled or not self._enabled:
            return

        remove = []
        exceptions = []
        for function in self._functions:
            if isinstance(function, mobu.FBCallback):
                remove.append(function)
                continue
            try:
                function(control, event)
            except Exception as exception:
                exceptions.append(traceback.format_exc([exception.__class__, exception.message, exception]))

        # Remove invalid function
        for function in remove:
            self.Remove(function)

        if exceptions:
            raise CallbackError("The following {} errors were raised while running the {} callback".format(len(exceptions), self.name),
                                exceptions)

    def Add(self, command):
        """
        Adds function to be called by the callback

        Arguments:
            command (function): function to be called by the callback

        Return:
            boolean
        """
        if not isinstance(command, (types.NoneType, mobu.FBCallback)) and command not in self._functions:
            self._functions.append(command)
            return True
        return False

    def Remove(self, command):
        """
        Removes the function from the callback if it belongs to the callback.

        Arguments:
            command (function): function to remove

        Return:
            boolean
        """
        if command in self._functions:
            self._functions.remove(command)
            return True
        return False

    def RemoveAll(self):
        """ Removes all the functions attached to the callback  """
        self._functions = []

    @property
    def name(self):
        """ The name of the callback """
        return CallbackManager.GetCallbackNameByEventType(self._callback.eventtype)

    @property
    def eventtype(self):
        """ The callback event type """
        return self._callback.eventtype

    @property
    def callbacks(self):
        """ List of functions attached to this callback """
        return self._functions

    @property
    def enabled(self):
        """ Is this callback enabled """
        return self.__externalEnabled and self._enabled

    @enabled.setter
    def enabled(self, value):
        """ Sets if the callback enabled """
        self._enabled = value

    @classmethod
    def allEnabled(cls):
        """
        The enable state for all callbacks. This value takes precedence over the local enable state of individual
        callbacks

        Arguments:
            value (bool): state to set for all callbacks
        """
        return cls.__externalEnabled

    @classmethod
    def setAllEnabled(cls, value):
        """
        Sets the enable state for all callbacks. This value takes precedence over the local enable state of individual
        callbacks
        """
        cls.__externalEnabled = value


@Singleton.Singleton
class CallbackManager(object):
        """
        Manages callbacks in Motion Builder so they are accessible in one location
        """
        # NOTE: This does not contain all the available callbacks in Motion Builder as they are scattered through
        #       out the SDK. Add missing callbacks as needed.

        # ---------------------------
        # ----- Scene callbacks -----
        # ---------------------------

        _CallbackTypes = {Scene.OnChange.eventtype: "OnChange",
                          Scene.OnTakeChange.eventtype: "OnTakeChange",
                          Player.OnChange.eventtype: "PlayerOnChange",
                          Application.OnFileOpen.eventtype: "OnFileOpen",
                          Application.OnFileOpenCompleted.eventtype: "OnFileOpenCompleted",
                          Application.OnFileNew.eventtype: "OnFileNew",
                          Application.OnFileNewCompleted.eventtype: "OnFileNewCompleted",
                          Application.OnFileSave.eventtype: "OnFileSave",
                          Application.OnFileSaveCompleted.eventtype: "OnFileSaveCompleted",
                          Application.OnFileMerge.eventtype: "OnFileMerge",
                          Application.OnFileExit.eventtype: "OnFileExit",
                          System.OnUIIdle.eventtype: "OnUIIdle",
                          System.OnConnectionNotify.eventtype: "OnConnectionNotify",
                          System.OnConnectionKeyingNotify.eventtype: "OnConnectionKeyingNotify",
                          System.OnConnectionDataNotify.eventtype: "OnConnectionDataNotify",
                          System.OnConnectionStateNotify.eventtype: "OnConnectionStateNotify",
                          System.OnVideoFrameRendering.eventtype: "OnVideoFrameRendering",
                          FileMonitor.OnFileChangeMainScene.eventtype: "OnFileChangeMainScene",
                          FileMonitor.OnFileChangeAnimationClip.eventtype: "OnFileChangeAnimationClip",
                          FileMonitor.OnFileChangeFileReference.eventtype: "OnFileChangeFileReference",
                          EvaluateManager.OnSynchronizationEvent.eventtype: "OnSynchronizationEvent",
                          EvaluateManager.OnRenderingPipelineEvent.eventtype: "OnRenderingPipelineEvent"
                          }

        @property
        def enabled(self):
            """ Are callbacks enabled """
            return CallbackWrapper.allEnabled()

        @enabled.setter
        def enabled(self, value):
            """
            Enable or disable all callbacks managed by the manager

            Arguments:
                value (boolean): enable or disable callbacks
            """
            CallbackWrapper.setAllEnabled(value)

        @property
        def OnChange(self):
            """
            Callback triggered when a component has been selected, unselected, added, destroyed, renamed, etc.

            Return:
                FBEvent
            """
            return CallbackWrapper(Scene.OnChange)

        @property
        def PlayerOnChange(self):
            """
            Callback triggered when a component has been selected, unselected, added, destroyed, renamed, etc.

            Return:
                FBEvent
            """
            return CallbackWrapper(Player.OnChange)

        @property
        def OnTakeChange(self):
            """
            Callback triggered when a take has been selected, unselected, added, destroyed, renamed, etc.

            Return:
                FBEvent
            """
            return CallbackWrapper(Scene.OnTakeChange)

        # ---------------------------
        # -- Application callbacks --
        # ---------------------------

        @property
        def OnFileOpen(self):
            """
            A File Open has been requested, nothing has been loaded yet.

            Return:
                pyfbsdk.FBEvent
            """
            return CallbackWrapper(Application.OnFileOpen)

        @property
        def OnFileOpenCompleted(self):
            """
            A File Open has been completed.

            Return:
                pyfbsdk.FBEvent
            """
            return CallbackWrapper(Application.OnFileOpenCompleted)

        @property
        def OnFileNew(self):
            """
            A File New has been requested, nothing has been destroyed yet.

            Return:
                pyfbsdk.FBEvent
            """
            return CallbackWrapper(Application.OnFileNew)

        @property
        def OnFileNewCompleted(self):
            """
            A File New has been completed.

            Return:
                pyfbsdk.FBEvent
            """
            return CallbackWrapper(Application.OnFileNewCompleted)

        @property
        def OnFileSave(self):
            """
            A File Save has been requested, nothing has been saved yet.

            Return:
                pyfbsdk.FBEvent
            """
            return CallbackWrapper(Application.OnFileSave)

        @property
        def OnFileSaveCompleted(self):
            """
            A File Save has been completed.

            Return:
                pyfbsdk.FBEvent
            """
            return CallbackWrapper(Application.OnFileSaveCompleted)

        @property
        def OnFileMerge(self):
            """
            A File Merge has been requested, nothing has been loaded yet.
            When a file merge is completed, the FileOpenCompleted callback is called.

            Return:
                pyfbsdk.FBEvent
            """
            return CallbackWrapper(Application.OnFileMerge)

        @property
        def OnFileExit(self):
            """
            A File Exit as been requested, nothing has been destroyed yet.
            This is called when Motion Builder has been asked to close.

            Return:
                pyfbsdk.FBEvent
            """
            return CallbackWrapper(Application.OnFileExit)

        # --------------------------
        # ---- System callbacks ----
        # --------------------------

        @property
        def OnUIIdle(self):
            """
            Useful callback for less frequent GUI refresh and etc. lightweight tasks (occur once per several frames)

            Return:
                FBPropertyEventUIIdle
            """
            return CallbackWrapper(System.OnUIIdle)

        @property
        def OnConnectionNotify(self):
            """
            A connection event occurred between objects in the system.

            Return:
                FBPropertyEventConnectionNotify
            """
            return CallbackWrapper(System.OnConnectionNotify)

        @property
        def OnConnectionDataNotify(self):
            """
            A data event occured between objects in the system

            Return:
                pyfbsdk.FBEvent
            """
            return CallbackWrapper(System.OnConnectionDataNotify)

        @property
        def OnConnectionStateNotify(self):
            """
            A state change event occurred between objects in the system.

            Return:
                FBPropertyEventConnectionNotify
            """
            return CallbackWrapper(System.OnConnectionStateNotify)

        @property
        def OnConnectionKeyingNotify(self):
            """
            A connection event occured between a property and keyframe

            Return:
                FBEventConnectionKeyingNotify
            """
            return CallbackWrapper(System.OnConnectionKeyingNotify)

        @property
        def OnConnectionVideoFramesRendering(self):
            """
            A video frame rendering event occurred when the scene is being off-line rendered into video files.

            Return:
                FBPropertyEventVideoFrameRendering
            """
            return CallbackWrapper(System.OnVideoFrameRendering)

        # --------------------------------
        # ---- File Monitor callbacks ----
        # --------------------------------

        @property
        def OnFileChangeMainScene(self):
            """
            A file that is being monitored has changed since the last event in the current scene was triggered

            Return:
                FBPropertyEvent
            """
            return CallbackWrapper(FileMonitor.OnFileChangeMainScene)

        @property
        def OnFileChangeFileReference(self):
            """
            A file that is being monitored has changed since the last namespace related event in the current scene was
            triggered

            Return:
                FBPropertyEvent
            """
            return CallbackWrapper(FileMonitor.OnFileChangeFileReference)

        @property
        def OnFileChangeAnimationClip(self):
            """
            A file that is being monitored has changed since the last animation clip related event in the current scene
            was triggered

            Return:
                FBPropertyEvent
            """
            return CallbackWrapper(FileMonitor.OnFileChangeAnimationClip)

        # --------------------------------
        # -- Evaluate Manager callbacks --
        # --------------------------------

        @property
        def OnSynchronizationEvent(self):
            """
            For callback events at synchronization point, such as when there is a frame change in the time slider

            Return:
                FBPropertyEventCallbackSynPoint
            """
            return CallbackWrapper(EvaluateManager.OnSynchronizationEvent)

        @property
        def OnRenderingPipelineEvent(self):
            """
            For callback events at synchronization point, such as when there is a frame change in the time slider

            Return:
                FBPropertyEventCallbackSynPoint
            """
            return CallbackWrapper(EvaluateManager.OnRenderingPipelineEvent)

        @staticmethod
        def Remove(func, events=None):
            """
            Removes a function if it is hooked to the given events. When no events are given, it searches all
            available events that it might belong to and removes the function / method from them.

            Arguments:
                func (function or method): function / method to remove from the
                events (list[FBEvents, etc.]): events to remove methods from

            Return:
                boolean; whether the function/method was removed from the events
            """
            search = True

            # Get a list of events that the function hooked if no events are passed
            if events is None:
                events = [event
                          for event in CallbackManager.GetAllEvents()
                          for callback in list(event.callbacks)
                          if callback.Callback == func]
                search = False
            # convert events into a list if it isn't already a list
            elif not isinstance(events, (list, tuple)):
                events = [events]

            # remove callbacks
            removed = False
            for event in events:
                if search:
                    removed = [event.Remove(func) for callback in list(event.callbacks) if callback.Callback == func]
                    removed = len(removed) > 0

                else:
                    event.Remove(func)
                    removed = True

            return removed

        @classmethod
        def GetCallbackNameByEventType(cls, eventtype):
            """
            Get the name of the callback by its eventtype value

            Arguments:
                eventtype (pyfbsdk.FBEventType): the type of event, some callbacks return an FBEvent others an interger

            Return:
                string when the value is found, returns back the eventtype otherwise
            """
            return cls._CallbackTypes.get(eventtype, eventtype)

        @classmethod
        def RemoveAll(cls, skipEvents=()):
            """
            Attempts to remove all callbacks

            Arguments:
                skipEvents (list[pyfbsdk.FBEventName]): list of event types to avoid removing
            """
            for event in cls.GetAllEvents():

                if event is None or event.eventtype in skipEvents:
                    continue

                if event.callbacks:
                    logging.info("Close - \t Clearing {instance}.{event}".format(
                        event=cls.GetCallbackNameByEventType(event.eventtype), instance=event.control))

                for callback in event.callbacks:

                    while isinstance(callback, mobu.FBCallback):
                        callback = callback.Callback
                    logging.info("Close -  \t\t Removing {module}.{function}".format(
                                 function=getattr(callback, "__name__", "None"),
                                 module=getattr(callback, "__module__", "None")))

                event.RemoveAll()

        @classmethod
        def GetAllEvents(cls):
            """
            Gets all the available events that this class manages and attempts to retrieve events that have been
            picked up by python's garbage collection.

            Return:
                generator
            """
            # Garbage Collection may not pick up these callbacks, so we explicitly return them to be safe
            yield Scene.OnChange
            yield Scene.OnTakeChange

            yield Application.OnFileOpen
            yield Application.OnFileOpenCompleted
            yield Application.OnFileNew
            yield Application.OnFileNewCompleted
            yield Application.OnFileSave
            yield Application.OnFileSaveCompleted
            yield Application.OnFileMerge

            yield System.OnUIIdle
            yield System.OnConnectionNotify
            yield System.OnConnectionDataNotify
            yield System.OnConnectionStateNotify
            yield System.OnConnectionKeyingNotify
            yield System.OnVideoFrameRendering

            yield FileMonitor.OnFileChangeFileReference
            yield FileMonitor.OnFileChangeAnimationClip
            yield FileMonitor.OnFileChangeMainScene

            yield EvaluateManager.OnSynchronizationEvent
            yield EvaluateManager.OnRenderingPipelineEvent

            for gc_object in gc.get_objects():

                # get all Scene callbacks
                if isinstance(gc_object, mobu.FBScene):
                    for eventName in ("OnChange", "OnTakeChange"):
                        yield getattr(gc_object, eventName)

                # get all Application callbacks
                elif isinstance(gc_object, mobu.FBApplication):
                    for eventName in ("OnFileOpen", "OnFileOpenCompleted",
                                      "OnFileNew",  "OnFileNewCompleted",
                                      "OnFileSave", "OnFileSaveCompleted",
                                      "OnFileMerge", "OnFileExit"):
                        yield getattr(gc_object, eventName)

                elif isinstance(gc_object, mobu.FBSystem):
                    for eventName in ("OnUIIdle", "OnConnectionNotify",
                                      "OnConnectionDataNotify", "OnConnectionStateNotify",
                                      "OnVideoFrameRendering"):
                        yield getattr(gc_object, eventName)

Callbacks = CallbackManager()

#
#
# Mirror Partners
#
#


# Mirror_Label:[ Left, Right ]
mirrorPairs = {
        "IK_Hand":[ "IK_R_Hand", "IK_L_Hand" ],
        "IK_Foot":[ "IK_R_Foot", "IK_L_Foot" ],
        "PH_Hand":[ "PH_R_Hand", "PH_L_Hand" ],
        "PH_Foot":[ "PH_R_Foot", "PH_L_Foot" ],
        "PH_Boot":[ "PH_R_Boot", "PH_L_Boot" ],
        "PH_Hip":[ "PH_R_Hip", "PH_L_Hip" ],
        "PH_Breast":[ "PH_R_Breast", "PH_L_Breast" ],
        "PH_Shoulder":[ "PH_R_Shoulder", "PH_L_Shoulder" ],
        "RB_ThighRoll":[ "RB_R_ThighRoll", "RB_L_ThighRoll" ]
    }

# 0 = None, 1 = Self
mirrorNoPairs = {
        "mover":1,
        "HeelHeight":0,
        "SKEL_Tail":1,
        "SKEL_ROOT":0,
        "SKEL_SADDLE":0,
        "OH_FacingDir":1,
        "OH_LookDir":0,
        "PH_Belt_Front":0,
        "PH_Belt_Melee":0,
        "PH_Belt_Rear":0,
        "PH_Belt_Thrower":0,
        "PH_Bow":0,
        "PH_Rifle":0
    }


# ---------------------------------------------------------------------------------------------------------------------
# --------------------------------- Anything with 'g' to be depreciated. Don't use.# ----------------------------------
# ---------------------------------------------------------------------------------------------------------------------
gApp = Application
gScene = Scene
gActorFaces = gScene.ActorFaces
gActors = gScene.Actors
gAudioClips = gScene.AudioClips
gCameras = gScene.Cameras
gCharacterExtensions = gScene.CharacterExtensions
gCharacterFaces = gScene.CharacterFaces
gCharacterPoses = gScene.CharacterPoses
gCharacters = gScene.Characters
gConstraintSlovers = gScene.ConstraintSolvers
gComponents = Components
gConstraints = gScene.Constraints
gControlSets = gScene.ControlSets
gDevices = gScene.Devices
gFolders = gScene.Folders
gGroups = gScene.Groups
gHandles = gScene.Handles
gLights = gScene.Lights
gMarkerSets = gScene.MarkerSets
gMaterials = gScene.Materials
gNotes = gScene.Notes
gObjectPoses = gScene.ObjectPoses
gPhysicalProperties = gScene.PhysicalProperties
gPoses = gScene.Poses
gRoot = gScene.RootModel
gSets = gScene.Sets
gShaders = gScene.Shaders
gTakes = gScene.Takes
gTextures = gScene.Textures
gUserObjects = gScene.UserObjects
gVideoClips = gScene.VideoClips

gUlog = Ulog


def rs_NonSystemCameraList():
    return [camera for camera in Cameras if not camera.SystemCamera]


# # Colors ##

gRed = mobu.FBColor(1, 0, 0)
gOrange = mobu.FBColor(0.9, 0.4, 0)
gPink = mobu.FBColor(1, 0, 1)
gGreen = mobu.FBColor(0.4, 0.7, 0)
gYellow = mobu.FBColor(1, 1, 0)
gDarkBlue = mobu.FBColor(0, 0.2, 0.65)
gCyan = mobu.FBColor(0, 1, 1)


# # Character skeleton array ## - Get from ProjectData
gSkelArray = ProjectData.data.GetSkeltonArray()


# # Floor contacts array ##

gContactsArray = ['L_Foot_Contact',
              'R_Foot_Contact',
              'L_Hand_Contact',
              'R_Hand_Contact', ]



# # Animal skeleton arrays ##

gAnimalSkelHeadNeckArray = ['_Neck_1',
                            '_Neck_2',
                            '_Head',
                            '_Head_NUB',
                            'SPR_R_Ear_ROOT',
                            'SPR_R_Ear',
                            'SPR_L_Ear_ROOT',
                            'SPR_L_Ear',
                            'AntlerRoot',
                            'AntlerRoot_NUB']

gAnimalSkelTorsoArray = ['_ROOT',
                        '_Spine_Root',
                        '_Spine0',
                        '_Spine1',
                        '_Spine2',
                        '_Spine2_NUB',
                        '_Spine3',
                        '_PelvisRoot',
                        '_Pelvis',
                        '_Pelvis1']

gAnimalSkelLeftArmArray = ['_L_Clavicle',
                            '_L_Clavicle_NUB',
                            '_L_UpperArm',
                            '_L_Forearm',
                            '_L_Hand',
                            '_L_Finger00',
                            '_L_Finger00_NUB',
                            '_L_Finger01',
                            '_L_Finger01_NUB',
                            'MH_L_ShoulderBladeRoot',
                            'MH_L_ShoulderBlade']

gAnimalSkelRightArmArray = ['_R_Clavicle',
                            '_R_Clavicle_NUB',
                            '_R_UpperArm',
                            '_R_Forearm',
                            '_R_Hand',
                            '_R_Finger00',
                            '_R_Finger00_NUB',
                            '_R_Finger01',
                            '_R_Finger01_NUB',
                            'MH_R_ShoulderBladeRoot',
                            'MH_R_ShoulderBlade']

gAnimalSkelLeftLegArray = ['_L_Thigh',
                           '_L_Thigh_NUB',
                           '_L_Calf',
                           '_L_Foot',
                           '_L_Toe0',
                           '_L_Toe0_NUB',
                           '_L_Toe1',
                           '_L_Toe1_NUB']

gAnimalSkelRightLegArray = ['_R_Thigh',
                            '_R_Thigh_NUB',
                            '_R_Calf',
                            '_R_Foot',
                            '_R_Toe0',
                            '_R_Toe0NUB',
                            '_R_Toe1',
                            '_R_Toe1_NUB']

gAnimalSkelTailArray = ['_Tail_01',
                        '_Tail_02',
                        '_Tail_03',
                        '_Tail_04',
                        '_Tail_05',
                        '_Tail_05_NUB',
                        '_Tail_05_NUB-END']

gAnimalSkelArray = [gAnimalSkelHeadNeckArray[0],
                    gAnimalSkelHeadNeckArray[1],
                    gAnimalSkelHeadNeckArray[2],
                    gAnimalSkelHeadNeckArray[3],
                    gAnimalSkelHeadNeckArray[4],
                    gAnimalSkelHeadNeckArray[5],
                    gAnimalSkelHeadNeckArray[6],
                    gAnimalSkelHeadNeckArray[7],
                    gAnimalSkelHeadNeckArray[8],
                    gAnimalSkelHeadNeckArray[9],

                    gAnimalSkelTorsoArray[0],
                    gAnimalSkelTorsoArray[1],
                    gAnimalSkelTorsoArray[2],
                    gAnimalSkelTorsoArray[3],
                    gAnimalSkelTorsoArray[4],
                    gAnimalSkelTorsoArray[5],
                    gAnimalSkelTorsoArray[6],
                    gAnimalSkelTorsoArray[7],
                    gAnimalSkelTorsoArray[8],
                    gAnimalSkelTorsoArray[9],

                    gAnimalSkelLeftArmArray[0],
                    gAnimalSkelLeftArmArray[1],
                    gAnimalSkelLeftArmArray[2],
                    gAnimalSkelLeftArmArray[3],
                    gAnimalSkelLeftArmArray[4],
                    gAnimalSkelLeftArmArray[5],
                    gAnimalSkelLeftArmArray[6],
                    gAnimalSkelLeftArmArray[7],
                    gAnimalSkelLeftArmArray[8],
                    gAnimalSkelLeftArmArray[9],
                    gAnimalSkelLeftArmArray[10],

                    gAnimalSkelRightArmArray[0],
                    gAnimalSkelRightArmArray[1],
                    gAnimalSkelRightArmArray[2],
                    gAnimalSkelRightArmArray[3],
                    gAnimalSkelRightArmArray[4],
                    gAnimalSkelRightArmArray[5],
                    gAnimalSkelRightArmArray[6],
                    gAnimalSkelRightArmArray[7],
                    gAnimalSkelRightArmArray[8],
                    gAnimalSkelRightArmArray[9],
                    gAnimalSkelRightArmArray[10],

                    gAnimalSkelLeftLegArray[0],
                    gAnimalSkelLeftLegArray[1],
                    gAnimalSkelLeftLegArray[2],
                    gAnimalSkelLeftLegArray[3],
                    gAnimalSkelLeftLegArray[4],
                    gAnimalSkelLeftLegArray[5],
                    gAnimalSkelLeftLegArray[6],
                    gAnimalSkelLeftLegArray[7],

                    gAnimalSkelRightLegArray[0],
                    gAnimalSkelRightLegArray[1],
                    gAnimalSkelRightLegArray[2],
                    gAnimalSkelRightLegArray[3],
                    gAnimalSkelRightLegArray[4],
                    gAnimalSkelRightLegArray[5],
                    gAnimalSkelRightLegArray[6],
                    gAnimalSkelRightLegArray[7],

                    gAnimalSkelTailArray[0],
                    gAnimalSkelTailArray[1],
                    gAnimalSkelTailArray[2],
                    gAnimalSkelTailArray[3],
                    gAnimalSkelTailArray[4],
                    gAnimalSkelTailArray[5],
                    gAnimalSkelTailArray[6]]

gAnimalPrefixSkel = 'SKEL'
gAnimalPrefixCtrlRig = 'CTRLRIG'
gAnimalPrefixGS = 'gs:'

gNewAnimalSkelArray = ["_Pelvis",
                        "_L_Thigh",
                        "_L_Calf",
                        "_L_Foot",
                        "_R_Thigh",
                        "_R_Calf",
                        "_R_Foot",
                        "_Spine0",
                        "_L_UpperArm",
                        "_L_Forearm",
                        "_L_Hand",
                        "_R_UpperArm",
                        "_R_Forearm",
                        "_R_Hand",
                        "_Head",
                        "_L_Toe0",
                        "_R_Toe0",
                        "_L_Clavicle",
                        "_R_Clavicle",
                        "_Neck0",
                        "_L_Finger00",
                        "_R_Finger00",
                        "_Spine1",
                        "_Spine2",
                        "_Spine3",
                        "_Neck1",
                        "_Neck2",
                        "_Neck3",
                        "_Neck4",
                        "_Neck5",
                        "_R_Toe1",
                        "_L_Toe1",
                        "_R_Finger01",
                        "_L_Finger01"]

# # GIANT skeleton array ##

gGiantSkelArray = ['root',
                   'waist_dum',
                   'torso_1',
                   'torso_2',
                   'torso_3',
                   'torso_4',
                   'torso_5',
                   'torso_6',
                   'torso_7',
                   'neck_1',
                   'neck_2',
                   'neck_3',
                   'head',
                   'l_shoulder_dum',
                   'l_shoulder',
                   'l_up_arm',
                   'l_up_arm_twist',
                   'l_low_arm',
                   'l_low_arm_twist',
                   'l_hand',
                   'l_hand_1_dum',
                   'l_hand_1',
                   'l_up_fing_1',
                   'l_mid_fing_1',
                   'l_low_fing_1',
                   'l_hand_2_dum',
                   'l_hand_2',
                   'l_up_fing_2',
                   'l_mid_fing_2',
                   'l_low_fing_2',
                   'l_hand_3_dum',
                   'l_hand_3',
                   'l_up_fing_3',
                   'l_mid_fing_3',
                   'l_low_fing_3',
                   'l_hand_4_dum',
                   'l_hand_4',
                   'l_up_fing_4',
                   'l_mid_fing_4',
                   'l_low_fing_4',
                   'l_hand_5_dum',
                   'l_hand_5',
                   'l_up_fing_5',
                   'l_mid_fing_5',
                   'l_low_fing_5',
                   'r_shoulder_dum',
                   'r_shoulder',
                   'r_up_arm',
                   'r_up_arm_twist',
                   'r_low_arm',
                   'r_low_arm_twist',
                   'r_hand',
                   'r_hand_1_dum',
                   'r_hand_1',
                   'r_up_fing_1',
                   'r_mid_fing_1',
                   'r_low_fing_1',
                   'r_hand_2_dum',
                   'r_hand_2',
                   'r_up_fing_2',
                   'r_mid_fing_2',
                   'r_low_fing_2',
                   'r_hand_3_dum',
                   'r_hand_3',
                   'r_up_fing_3',
                   'r_mid_fing_3',
                   'r_low_fing_3',
                   'r_hand_4_dum',
                   'r_hand_4',
                   'r_up_fing_4',
                   'r_mid_fing_4',
                   'r_low_fing_4',
                   'r_hand_5_dum',
                   'r_hand_5',
                   'r_up_fing_5',
                   'r_mid_fing_5',
                   'r_low_fing_5',
                   'l_up_leg_dum',
                   'l_up_leg',
                   'l_low_leg',
                   'l_foot',
                   'l_toes',
                   'r_up_leg_dum',
                   'r_up_leg',
                   'r_low_leg',
                   'r_foot',
                   'r_toes',
                   'mesh']


gHoovedQuadrupeds = ["A_C_Horse",
                     "A_C_Deer",
                     "A_C_Cow",
                     "A_C_Boar",
                     "A_C_Pig",
                     "P_C_Horse_01",
                     "P_C_Horse_02"]

gNonHoovedQuadrupeds = ["A_C_Rottweiler",
                        "A_C_Chop",
                        "A_C_Retriever",
                        "A_C_Coyote",
                        "A_C_Coyote_01",
                        "A_C_MtLion"]


# # 3Lateral face geometries ##

gCAFaceAttrGui = ["FacialAttrGUI", "Tongue", "RECT_press", "TEXT_press", "RECT_rollIn", "TEXT_rollIn",
                  "RECT_narrowWide", "TEXT_narrowWide", "JAW", "RECT_clench", "TEXT_clench", "RECT_backFwd",
                  "TEXT_backFwd", "NOSE", "RECT_nasolabialFurrowL", "TEXT_nasolabialFurrowL", "RECT_nasolabialFurrowR",
                  "TEXT_nasolabialFurrowR", "MOUTH", "RECT_smileR", "RECT_smileL", "TEXT_smileL", "TEXT_smileR", "RECT_openSmileR",
                  "TEXT_openSmileR", "RECT_openSmileL", "TEXT_openSmileL", "RECT_frownR", "TEXT_frownR", "RECT_frownL",
                  "TEXT_frownL", "RECT_scream", "TEXT_scream", "RECT_lipsNarrowWideR", "TEXT_lipsNarrowWideR", "RECT_lipsNarrowWideL",
                  "TEXT_lipsNarrowWideL", "RECT_lipsStretchOpenR", "TEXT_lipsStretchOpenR", "RECT_lipsStretchOpenL",
                  "TEXT_lipsStretchOpenL", "RECT_chinWrinkle", "TEXT_chinWrinkle", "RECT_chinRaiseUpper", "TEXT_chinRaiseUpper",
                  "RECT_chinRaiseLower", "TEXT_chinRaiseLower", "RECT_closeOuterR", "TEXT_closeOuterR", "RECT_closeOuterL",
                  "TEXT_closeOuterL", "RECT_puckerR", "TEXT_puckerR", "RECT_puckerL", "TEXT_puckerL", "RECT_oh", "TEXT_oh",
                  "RECT_funnelUR", "TEXT_funnelUR", "RECT_funnelDR", "TEXT_funnelDR", "RECT_mouthSuckUR", "TEXT_mouthSuckUR",
                  "RECT_mouthSuckDR", "TEXT_mouthSuckDR", "RECT_pressR", "TEXT_pressR", "RECT_pressL", "TEXT_pressL", "RECT_dimpleR",
                  "TEXT_dimpleR", "RECT_dimpleL", "TEXT_dimpleL", "RECT_suckPuffR", "TEXT_suckPuffR", "RECT_suckPuffL", "TEXT_suckPuffL",
                  "RECT_lipBite", "TEXT_lipBite", "RECT_funnelUL", "TEXT_funnelUL", "RECT_funnelDL", "TEXT_funnelDL", "RECT_funnelDL001",
                  "TEXT_mouthSuckUL", "RECT_mouthSuckDL", "TEXT_mouthSuckDL", "Eyes", "RECT_blinkL", "TEXT_blinkL", "RECT_squeezeR",
                  "TEXT_squeezeR", "RECT_squeezeL", "TEXT_squeezeL", "RECT_lipsNarrowWideR001", "TEXT_blinkR", "RECT_openCloseUR001",
                  "TEXT_openCloseDR", "RECT_squintInnerUR", "TEXT_squintInnerUR", "RECT_squintInnerDR", "TEXT_squintInnerDR",
                  "RECT_openCloseUR", "TEXT_openCloseUR", "RECT_openCloseDL", "TEXT_openCloseDL", "RECT_squintInnerUL", "TEXT_squintInnerUL",
                  "RECT_squintInnerDL", "TEXT_squintInnerDL", "RECT_openCloseUL", "TEXT_openCloseUL", "LIPS", "RECT_thinThick_C",
                  "RECT_thinThick_D", "RECT_stickyLips_E", "RECT_thinThick_B", "RECT_thinThick_F", "RECT_thinThick_H", "RECT_thinThick_G",
                  "RECT_stickyLips_A",
                  "faceControls_FRAME", "mouth_FRAME", "mouth_TEXT", "jaw_FRAME", "jaw_TEXT", "tongue_TEXT", "tongueMove_FRAME",
                  "tongueRoll_FRAME", "tongueInOut_FRAME", "outerBrow_R_FRAME", "innerBrow_L_FRAME", "outerBrow_L_FRAME", "brows_TEXT",
                  "innerBrow_R_FRAME", "eye_R_FRAME", "eye_L_FRAME", "eyes_TEXT", "eye_C_FRAME", "nose_L_FRAME", "nose_R_FRAME",
                  "nose_TEXT", "cheek_TEXT", "cheek_L_FRAME", "cheek_R_FRAME"]

gCAOneDSliders = ["CIRC_press", "CIRC_rollIn", "CIRC_nasolabialFurrowL", "CIRC_nasolabialFurrowR", "CIRC_smileR", "CIRC_smileL",
                  "CIRC_openSmileR", "CIRC_openSmileL", "CIRC_frownR", "CIRC_frownL", "CIRC_scream", "CIRC_lipsStretchOpenR",
                  "CIRC_lipsStretchOpenL", "CIRC_chinWrinkle", "CIRC_puckerR", "CIRC_puckerL", "CIRC_oh", "CIRC_funnelUR", "CIRC_funnelDR",
                  "CIRC_mouthSuckUR", "CIRC_mouthSuckDR", "CIRC_pressR", "CIRC_pressL", "CIRC_dimpleR", "CIRC_dimpleL", "CIRC_lipBite",
                  "CIRC_funnelUL", "CIRC_funnelDL", "CIRC_mouthSuckUL", "CIRC_mouthSuckDL", "CIRC_squeezeR", "CIRC_squeezeL",
                  "CIRC_squintInnerUR", "CIRC_squintInnerDR", "CIRC_squintInnerUL", "CIRC_squintInnerDL", "CIRC_stickyLips_E", "CIRC_stickyLips_A"]

gCATwoDSliders = ["CIRC_narrowWide", "CIRC_clench", "CIRC_backFwd", "CIRC_lipsNarrowWideR", "CIRC_lipsNarrowWideL", "CIRC_chinRaiseUpper",
                  "CIRC_chinRaiseLower", "CIRC_closeOuterR", "CIRC_closeOuterL", "CIRC_suckPuffR", "CIRC_suckPuffL", "CIRC_blinkL", "CIRC_blinkR",
                  "CIRC_openCloseDR", "CIRC_openCloseUR", "CIRC_openCloseDL", "CIRC_openCloseUL", "CIRC_thinThick_C", "CIRC_thinThick_D",
                  "CIRC_thinThick_B", "CIRC_thinThick_F", "CIRC_thinThick_H", "CIRC_thinThick_G"]

qSquareSliders = ["mouth_CTRL", "tongueMove_CTRL", "tongueRoll_CTRL", "eye_C_CTRL", "eye_L_CTRL", "eye_R_CTRL", "nose_L_CTRL", "nose_R_CTRL"]

qPosNegSliders = ["tongueInOut_CTRL", "outerBrow_R_CTRL", "outerBrow_L_CTRL"]

qPosSliders = ["cheek_L_CTRL", "cheek_R_CTRL"]

g3WaySliders = ["jaw_CTRL", "innerBrow_L_CTRL", "innerBrow_R_CTRL"]

gOnFaceControls = ["lowerLip_L_CTRL", "lowerLip_C_CTRL", "lowerLip_R_CTRL", "lipCorner_R_CTRL", "upperLip_C_CTRL",
                   "upperLip_L_CTRL", "lipCorner_L_CTRL", "upperLip_R_CTRL", "eyelidUpperOuter_L_CTRL", "eyelidUpperInner_L_CTRL",
                   "eyelidLowerOuter_L_CTRL", "eyelidLowerInner_L_CTRL", "eyelidLowerOuter_R_CTRL", "eyelidLowerInner_R_CTRL",
                   "eyelidUpperOuter_R_CTRL", "eyelidUpperInner_R_CTRL"]



gBrowCtrls = ["innerBrow_R_CTRL", "outerBrow_L_CTRL", "innerBrow_L_CTRL", "outerBrow_R_CTRL"]

gEyeCtrls = ["CIRC_openCloseUL", "CIRC_squintInnerDL", "CIRC_squintInnerUL", "CIRC_openCloseDL", "CIRC_openCloseUR", "CIRC_squintInnerDR",
            "CIRC_squintInnerUR", "CIRC_openCloseDR", "CIRC_blinkR", "CIRC_squeezeL", "CIRC_squeezeR", "CIRC_blinkL", "eyelidUpperInner_R_CTRL",
            "eyelidUpperOuter_R_CTRL", "eyelidLowerInner_R_CTRL", "eyelidLowerOuter_R_CTRL", "eyelidLowerInner_L_CTRL", "eyelidLowerOuter_L_CTRL",
            "eyelidUpperInner_L_CTRL", "eyelidUpperOuter_L_CTRL", "eye_C_CTRL", "eye_R_CTRL", "eye_L_CTRL"]

gMouthCtrls = ["CIRC_stickyLips_A", "CIRC_thinThick_G", "CIRC_thinThick_H", "CIRC_thinThick_F", "CIRC_thinThick_B", "CIRC_stickyLips_E",
                "CIRC_thinThick_D", "CIRC_thinThick_C", "CIRC_mouthSuckDL", "CIRC_mouthSuckUL", "CIRC_funnelDL", "CIRC_funnelUL", "CIRC_lipBite",
                "CIRC_suckPuffL", "CIRC_suckPuffR", "CIRC_dimpleL", "CIRC_dimpleR", "CIRC_pressL", "CIRC_pressR", "CIRC_mouthSuckDR",
                "CIRC_mouthSuckUR", "CIRC_funnelDR", "CIRC_funnelUR", "CIRC_oh", "CIRC_puckerL", "CIRC_puckerR", "CIRC_closeOuterL",
                "CIRC_closeOuterR", "CIRC_chinRaiseLower", "CIRC_chinRaiseUpper", "CIRC_chinWrinkle", "CIRC_lipsStretchOpenL", "CIRC_lipsStretchOpenR",
                "CIRC_lipsNarrowWideL", "CIRC_lipsNarrowWideR", "CIRC_scream", "CIRC_frownL", "CIRC_frownR", "CIRC_openSmileL", "CIRC_openSmileR",
                "CIRC_smileL", "CIRC_smileR", "CIRC_nasolabialFurrowR", "CIRC_nasolabialFurrowL", "CIRC_backFwd", "CIRC_clench", "CIRC_narrowWide",
                "CIRC_rollIn", "CIRC_press", "upperLip_R_CTRL", "lipCorner_L_CTRL", "upperLip_L_CTRL", "upperLip_C_CTRL", "lipCorner_R_CTRL",
                "lowerLip_R_CTRL", "lowerLip_C_CTRL", "lowerLip_L_CTRL", "cheek_R_CTRL", "cheek_L_CTRL", "nose_R_CTRL", "nose_L_CTRL",
                "tongueInOut_CTRL", "tongueRoll_CTRL", "tongueMove_CTRL", "jaw_CTRL", "mouth_CTRL"]

gBlushCtrl = [ "cutsceneBlush_OFF" ]

gFacialRoot = [ "FACIAL_facialRoot" ]

additionalAnimalControls = ["CTRL_R_Ear", "CTRL_L_Ear", "CTRL_R_Nose"]

# # Ambient face controller id's ##

gAmbientControllerIds = {   "CTRL_R_Brow":7689,
                            "CTRL_C_Brow":31528,
                            "CTRL_L_Brow":19497,
                            "CTRL_R_Blink":25778,
                            "CTRL_L_Blink":19205,
                            "CTRL_R_Eye":64876,
                            "CTRL_L_Eye":447,
                            "CTRL_R_Cheek":40256,
                            "CTRL_L_Cheek":33683,
                            "CTRL_UpperLip":28113,
                            "CTRL_UpperLip_Curl":17867,
                            "CTRL_R_Mouth":10760,
                            "CTRL_L_Mouth":4187,
                            "CTRL_Mouth":4626,
                            "CTRL_Tongue_In_Out":10274,
                            "CTRL_Tongue":51288,
                            "CTRL_LowerLip":64382,
                            "CTRL_LowerLip_Curl":15406,
                            "open":19779,
                            "W":455,
                            "ShCh":34077,
                            "PBM":21981,
                            "FV":1574,
                            "wide":50730,
                            "tBack":58819,
                            "tTeeth":47000,
                            "tRoof":62959,
                            "AU2_Outer_Brow_Raiser_Right":59610,
                            "AU2_Outer_Brow_Raiser_Left":20502,
                            "AU4_Brow_Lowerer":41424,
                            "AU10_Upper_Lip_Raiser":63645,
                            "AU15_Lip_Corner_Depressor_Right":22207,
                            "AU15_Lip_Corner_Depressor_Left":27754,
                            "AU16_Lower_Lip_Depressor":20640,
                            "AU20_Lip_Stretcher":33019,
                            "AU23_Lip_Tightener":53070,
                            "AU26_Jaw_Drop":54342,
                            "open_pose":840,
                            "W_pose":51201,
                            "wide_pose":52693,
                            "tBack_pose":34146,
                            "tTeeth_pose":23631,
                            "tRoof_pose":22140,
                            "Brow_Up_L":58471,
                            "Brow_Up_R":58445,
                            "Brows_Down":18040,
                            "Squint":41298,
                            "Look_Up_Lids":35793,
                            "Look_Down_Lids":62006,
                            "IH":1608}


# # Ambient facial geometries ##

gAmbientBorders = ["RECT_R_Blink",
                   "TEXT_R_Blink",
                   "RECT_L_Blink",
                   "TEXT_L_Blink",
                   "RECT_L_Cheek",
                   "TEXT_L_Cheek",
                   "RECT_R_Cheek",
                   "TEXT_R_Cheek",
                   "RECT_UpperLip_Curl",
                   "TEXT_UpperLip_Curl",
                   "RECT_LowerLip_Curl",
                   "TEXT_LowerLip_Curl",
                   "RECT_Tongue_In_Out",
                   "TEXT_Tongue_In_Out",
                   "RECT_C_Brow",
                   "TEXT_C_Brow",
                   "RECT_R_Brow",
                   "TEXT_R_Brow",
                   "RECT_L_Brow",
                   "TEXT_L_Brow",
                   "RECT_R_Eye",
                   "TEXT_R_Eye",
                   "RECT_L_Eye",
                   "TEXT_L_Eye",
                   "RECT_R_Mouth",
                   "TEXT_R_Mouth",
                   "RECT_Mouth",
                   "TEXT_Mouth",
                   "RECT_L_Mouth",
                   "TEXT_L_Mouth",
                   "RECT_UpperLip",
                   "TEXT_UpperLip",
                   "RECT_LowerLip",
                   "TEXT_LowerLip",
                   "RECT_Jaw",
                   "TEXT_Jaw",
                   "RECT_Tongue",
                   "TEXT_Tongue",
                   "RECT_MouthPinch",
                   "TEXT_MouthPinch",
                   "RECT_open",
                   "TEXT_open",
                   "RECT_W",
                   "TEXT_W",
                   "RECT_ShCh",
                   "TEXT_ShCh",
                   "RECT_PBM",
                   "TEXT_PBM",
                   "RECT_FV",
                   "TEXT_FV",
                   "RECT_wide",
                   "TEXT_wide",
                   "RECT_tBack",
                   "TEXT_tBack",
                   "RECT_tTeeth",
                   "TEXT_tTeeth",
                   "RECT_tRoof",
                   "TEXT_tRoof",
                   "RECT_AU2_Outer_Brow_Raiser_Left",
                   "TEXT_AU2_Outer_Brow_Raiser_Left",
                   "RECT_AU2_Outer_Brow_Raiser_Right",
                   "TEXT_AU2_Outer_Brow_Raiser_Right",
                   "RECT_AU4_Brow_Lowerer",
                   "TEXT_AU4_Brow_Lowerer",
                   "RECT_AU10_Upper_Lip_Raiser",
                   "TEXT_AU10_Upper_Lip_Raiser",
                   "RECT_AU15_Lip_Corner_Depressor_Left",
                   "TEXT_AU15_Lip_Corner_Depressor_Left",
                   "RECT_AU15_Lip_Corner_Depressor_Right",
                   "TEXT_AU15_Lip_Corner_Depressor_Right",
                   "RECT_AU16_Lower_Lip_Depressor",
                   "TEXT_AU16_Lower_Lip_Depressor",
                   "RECT_AU20_Lip_Stretcher",
                   "TEXT_AU20_Lip_Stretcher",
                   "RECT_AU23_Lip_Tightener",
                   "TEXT_AU23_Lip_Tightener",
                   "RECT_AU26_Jaw_Drop",
                   "TEXT_AU26_Jaw_Drop",
                   "RECT_open_pose",
                   "TEXT_open_pose",
                   "RECT_W_pose",
                   "TEXT_W_pose",
                   "RECT_ShCh",
                   "TEXT_ShCh",
                   "RECT_PBM",
                   "TEXT_PBM",
                   "RECT_FV",
                   "TEXT_FV",
                   "RECT_wide_pose",
                   "TEXT_wide_pose",
                   "RECT_tBack_pose",
                   "TEXT_tBack_pose",
                   "RECT_tTeeth_pose",
                   "TEXT_tTeeth_pose",
                   "RECT_tRoof_pose",
                   "TEXT_tRoof_pose",
                   "RECT_Brow_Up_L",
                   "TEXT_Brow_Up_L",
                   "RECT_Brow_Up_R",
                   "TEXT_Brow_Up_R",
                   "RECT_Brows_Down",
                   "TEXT_Brows_Down",
                   "RECT_Squint",
                   "TEXT_Squint",
                   "RECT_Look_Up_Lids",
                   "TEXT_Look_Up_Lids",
                   "RECT_Look_Down_Lids",
                   "TEXT_Look_Down_Lids",
                   "RECT_IH",
                   "TEXT_IH"]

gAmbient2DSliders = ["CTRL_C_Brow",
                     "CTRL_R_Brow",
                     "CTRL_L_Brow",
                     "CTRL_R_Eye",
                     "CTRL_L_Eye",
                     "CTRL_R_Mouth",
                     "CTRL_Mouth",
                     "CTRL_L_Mouth",
                     "CTRL_UpperLip",
                     "CTRL_LowerLip",
                     "CTRL_Jaw",
                     "CTRL_Tongue"]

gAmbient1DSliders = ["CTRL_R_Blink",
                     "CTRL_L_Blink",
                     "CTRL_L_Cheek",
                     "CTRL_R_Cheek",
                     "CTRL_UpperLip_Curl",
                     "CTRL_LowerLip_Curl",
                     "CTRL_Tongue_In_Out",
                     "CTRL_MouthPinch",
                     "open",
                     "W",
                     "ShCh",
                     "PBM",
                     "FV",
                     "wide",
                     "tBack",
                     "tTeeth",
                     "tRoof",
                     "AU2_Outer_Brow_Raiser_Left",
                     "AU2_Outer_Brow_Raiser_Right",
                     "AU4_Brow_Lowerer",
                     "AU10_Upper_Lip_Raiser",
                     "AU15_Lip_Corner_Depressor_Left",
                     "AU15_Lip_Corner_Depressor_Right",
                     "AU16_Lower_Lip_Depressor",
                     "AU20_Lip_Stretcher",
                     "AU23_Lip_Tightener",
                     "AU26_Jaw_Drop",
                     "open_pose",
                     "W_pose",
                     "wide_pose",
                     "tBack_pose",
                     "tTeeth_pose",
                     "tRoof_pose",
                     "Brow_Up_L",
                     "Brow_Up_R",
                     "Brows_Down",
                     "Squint",
                     "Look_Up_Lids",
                     "Look_Down_Lids",
                     "IH"]







# ##
# ##
# ##
# ##
# ##
# ##
# ##
# ## UFC Facial Rig Arrays
# ##
# ##
# ##
# ##
# ##
# ##
# ##
# ##

gUFCWolf_Ctrls = [
	"CTRL_analog_R_ear3",
	"CTRL_analog_L_ear3",
	"CTRL_analog_R_ear2",
	"CTRL_analog_L_ear2",
	"CTRL_analog_R_ear1",
	"CTRL_analog_L_ear1",
	"CTRL_analog_C_tongue4",
	"CTRL_analog_C_tongue3",
	"CTRL_analog_C_tongue2",
	"CTRL_analog_C_tongue1",
	"CTRL_lookAt_WorldParentSwitch",
	"CTRL_rig_LogicSwitch",
	"CTRL_R_nose",
	"CTRL_L_nose",
	"CTRL_C_tongue_inOut",
	"CTRL_C_tongue_stickToSnout",
	"CTRL_C_tongue_moveUpDown",
	"CTRL_C_tongue_roll",
	"CTRL_R_eye_blink",
	"CTRL_L_eye_blink",
	"CTRL_R_eye_cheekRaise",
	"CTRL_L_eye_cheekRaise",
	"CTRL_R_eye_squintInner",
	"CTRL_L_eye_squintInner",
	"CTRL_L_eye",
	"CTRL_R_eye",
	"CTRL_C_eye",
	"CTRL_L_brow_lateral",
	"CTRL_R_brow_lateral",
	"CTRL_L_brow_down",
	"CTRL_R_brow_down",
	"CTRL_L_brow_raiseOut",
	"CTRL_L_brow_raiseIn",
	"CTRL_R_brow_raiseIn",
	"CTRL_R_brow_raiseOut",
	"CTRL_R_mouth_lipsRollD",
	"CTRL_L_mouth_lipsRollD",
	"CTRL_R_mouth_lipsRollU",
	"CTRL_L_mouth_lipsRollU",
	"CTRL_L_mouth_howlD",
	"CTRL_L_mouth_howlU",
	"CTRL_R_mouth_howlU",
	"CTRL_R_mouth_howlD",
	"CTRL_R_mouth_upperLip",
	"CTRL_L_mouth_upperLip",
	"CTRL_R_mouth_lowerLipDepress",
	"CTRL_R_mouth_stretch",
	"CTRL_R_mouth_cornerPull",
	"CTRL_R_mouth_sharpCornerPull",
	"CTRL_R_mouth_upperLipRaise",
	"CTRL_L_mouth_lowerLipDepress",
	"CTRL_L_mouth_stretch",
	"CTRL_L_mouth_cornerPull",
	"CTRL_L_mouth_sharpCornerPull",
	"CTRL_L_mouth_upperLipRaise",
	"CTRL_C_jaw",
	"CTRL_R_ear",
	"CTRL_L_ear",
	"CTRL_R_lookAtOffset",
	"CTRL_L_lookAtOffset",
	"CTRL_C_lookAt",
	"CTRL_convergenceSwitch"
]

gUFCHorse_Ctrls = [
        "CTRL_lookAt_WorldParentSwitch",
        "CTRL_rig_LogicSwitch",
        "CTRL_convergenceSwitch",
        "CTRL_C_neck_swallow",
        "CTRL_R_nose",
        "CTRL_L_nose",
        "CTRL_C_lowerSnout",
        "CTRL_C_upperSnout",
        "CTRL_C_tongue_inOut",
        "CTRL_C_tongue_stickToSnout",
        "CTRL_C_tongue_moveUpDown",
        "CTRL_C_tongue_roll",
        "CTRL_L_eye_iris",
        "CTRL_R_eye_blink",
        "CTRL_L_eye_blink",
        "CTRL_R_eye_pupil",
        "CTRL_R_eye_cheekRaise",
        "CTRL_L_eye_cheekRaise",
        "CTRL_R_eye_squintInner",
        "CTRL_L_eye_squintInner",
        "CTRL_L_eye",
        "CTRL_R_eye",
        "CTRL_C_eye",
        "CTRL_L_brow_lateral",
        "CTRL_R_brow_lateral",
        "CTRL_L_brow_down",
        "CTRL_R_brow_down",
        "CTRL_L_brow_raiseOut",
        "CTRL_L_brow_raiseIn",
        "CTRL_R_brow_raiseIn",
        "CTRL_R_brow_raiseOut",
        "CTRL_R_mouth_lipsRollD",
        "CTRL_L_mouth_lipsRollD",
        "CTRL_R_mouth_lipsRollU",
        "CTRL_L_mouth_lipsRollU",
        "CTRL_R_mouth_upperLip",
        "CTRL_L_mouth_upperLip",
        "CTRL_R_mouth_lowerLipDepress",
        "CTRL_L_mouth_lowerLipDepress",
        "CTRL_R_mouth_upperLipRaise",
        "CTRL_L_mouth_upperLipRaise",
        "CTRL_C_jaw",
        "CTRL_R_ear",
        "CTRL_L_ear",
        "CTRL_R_LookAt",
        "CTRL_L_LookAt",
        "CTRL_C_lookAt",
        "CTRL_analog_R_ear3",
        "CTRL_analog_L_ear3",
        "CTRL_analog_R_ear2",
        "CTRL_analog_L_ear2",
        "CTRL_analog_C_tongue5",
        "CTRL_analog_C_tongue4",
        "CTRL_analog_C_tongue3",
        "CTRL_analog_C_tongue2",
        "CTRL_analog_C_tongue1"
]

gUFCFacial_Ctrls_Mouth = [
    "CTRL_R_mouth_lipUpDownD",
    "CTRL_L_mouth_lipUpDownD",
    "CTRL_R_mouth_lipUpDownU",
    "CTRL_L_mouth_lipUpDownU",
    "CTRL_R_nose_nasolabialDeepen",
    "CTRL_L_nose_nasolabialDeepen",
    "CTRL_R_mouth_corner",
    "CTRL_L_mouth_corner",
    "CTRL_L_mouth_thicknessD",
    "CTRL_R_mouth_thicknessD",
    "CTRL_R_mouth_thicknessU",
    "CTRL_L_mouth_thicknessU",
    "CTRL_L_mouth_lipsRollD",
    "CTRL_R_mouth_lipsRollD",
    "CTRL_R_mouth_lipsRollU",
    "CTRL_L_mouth_lipsRollU",
    "CTRL_R_mouth_cornerShapnessU",
    "CTRL_L_mouth_cornerShapnessU",
    "CTRL_L_mouth_cornerShapnessD",
    "CTRL_R_mouth_cornerShapnessD",
    "CTRL_L_mouth_pushPullD",
    "CTRL_R_mouth_pushPullU",
    "CTRL_L_mouth_pushPullU",
    "CTRL_R_mouth_pushPullD",
    "CTRL_R_nose",
    "CTRL_L_nose",
    "CTRL_Nose",
    "CTRL_R_ear_up",
    "CTRL_L_ear_up",
    "CTRL_R_mouth_suckBlow",
    "CTRL_L_mouth_suckBlow",
    "CTRL_R_mouth_openSmile",
    "CTRL_L_mouth_openSmile",
    "CTRL_L_mouth_lowerLipDepress",
    "CTRL_R_mouth_lowerLipDepress",
    "CTRL_R_mouth_stretch",
    "CTRL_L_mouth_stretch",
    "CTRL_R_mouth_cornerDepress",
    "CTRL_L_mouth_cornerDepress",
    "CTRL_R_mouth_dimple",
    "CTRL_L_mouth_dimple",
    "CTRL_R_mouth_cornerPull",
    "CTRL_L_mouth_cornerPull",
    "CTRL_R_mouth_sharpCornerPull",
    "CTRL_L_mouth_sharpCornerPull",
    "CTRL_R_mouth_upperLipRaise",
    "CTRL_L_mouth_upperLipRaise",
    "CTRL_C_mouth",
    "CTRL_L_mouth_lipBiteD",
    "CTRL_L_mouth_lipBiteU",
    "CTRL_R_mouth_lipBiteU",
    "CTRL_R_mouth_lipBiteD",
    "CTRL_L_mouth_lipsPressU",
    "CTRL_R_mouth_lipsPressU",
    "CTRL_L_mouth_tightenD",
    "CTRL_L_mouth_tightenU",
    "CTRL_R_mouth_tightenU",
    "CTRL_R_mouth_tightenD",
    "CTRL_L_mouth_lipsBlow",
    "CTRL_R_mouth_lipsBlow",
    "CTRL_L_mouth_pressD",
    "CTRL_L_mouth_pressU",
    "CTRL_R_mouth_pressU",
    "CTRL_R_mouth_pressD",
    "CTRL_L_mouth_lipsTogetherD",
    "CTRL_L_mouth_lipsTogetherU",
    "CTRL_R_mouth_lipsTogetherU",
    "CTRL_R_mouth_lipsTogetherD",
    "CTRL_L_mouth_puckerD",
    "CTRL_L_mouth_puckerU",
    "CTRL_R_mouth_puckerU",
    "CTRL_R_mouth_puckerD",
    "CTRL_L_mouth_funnelD",
    "CTRL_L_mouth_funnelU",
    "CTRL_R_mouth_funnelU",
    "CTRL_R_mouth_funnelD",
    "CTRL_R_jaw_chinCompress",
    "CTRL_L_jaw_chinCompress",
    "CTRL_L_jaw_ChinLiftU",
    "CTRL_L_jaw_ChinLiftD",
    "CTRL_R_jaw_ChinLiftU",
    "CTRL_R_jaw_ChinLiftD",
    "CTRL_C_jaw_fwdBack",
    "CTRL_C_jaw_clench",
    "CTRL_C_jaw"
    ]

gUFCFacial_Ctrls_Neck = [
    "CTRL_C_neck_stretch",
    "CTRL_L_neck_stretch",
    "CTRL_R_neck_stretch",
    "CTRL_C_neck_swallow"
    ]

gUFCFacial_Ctrls_Tongue = [
    "CTRL_C_tongue_narrowWide",
    "CTRL_C_tongue_inOut",
    "CTRL_C_tongue_press",
    "CTRL_C_tongue_roll",
    "CTRL_C_tongue"
    ]

gUFCFacial_Ctrls_Eyes = [
    "CTRL_R_eye_relax",
    "CTRL_R_eye_squeeze",
    "CTRL_L_eye_relax",
    "CTRL_L_eye_squeeze",
    "CTRL_L_eye_pupil",
    "CTRL_R_eye_pupil",
    "CTRL_R_eye_blink",
    "CTRL_L_eye_blink",
    "CTRL_R_eye_cheekRaise",
    "CTRL_L_eye_cheekRaise",
    "CTRL_R_eye_squintInner",
    "CTRL_L_eye_squintInner",
    "CTRL_L_eye",
    "CTRL_R_eye",
    "CTRL_C_eye"
    ]

gUFCFacial_Ctrls_GlobalEyes = [
    "CTRL_C_lookAt",
    "CTRL_L_LookAt",
    "CTRL_R_LookAt"
    ]

gUFCFacial_Ctrls_Brows = [
    "CTRL_L_brow_lateral",
    "CTRL_R_brow_lateral",
    "CTRL_L_brow_down",
    "CTRL_R_brow_down",
    "CTRL_L_brow_raiseOut",
    "CTRL_L_brow_raiseIn",
    "CTRL_R_brow_raiseIn",
    "CTRL_R_brow_raiseOut"
    ]

gUFCFacial_Ctrls_Switches = [
    "CTRL_rig_LogicSwitch",
    "CTRL_lookAt_WorldParentSwitch",
    "CTRL_convergenceSwitch"
    ]

gUFCFacial_Ctrls = list(set(# converting to a set then back to a list removes dupes.
    gUFCFacial_Ctrls_Mouth +
    gUFCFacial_Ctrls_Neck +
    gUFCFacial_Ctrls_Tongue +
    gUFCFacial_Ctrls_Eyes +
    gUFCFacial_Ctrls_GlobalEyes +
    gUFCFacial_Ctrls_Brows +
    gUFCWolf_Ctrls +
    gUFCHorse_Ctrls
    ))

gUFCFacial_Frames = [
    "FRM_faceGUI",
    "FRM_switches",
    # "FRM_lookAtFollowGUISwitch",
    # "FRM_lookAtSwitch",
    "FRM_rig_LogicSwitch",
    "FRM_lipsUpDown",
    "FRM_R_mouth_lipsUpDownD",
    "FRM_L_mouth_lipsUpDownD",
    "FRM_R_mouth_lipsUpDownU",
    "FRM_L_mouth_lipsUpDownU",
    "FRM_R_eye_squeeze",
    "FRM_R_eye_border",
    "FRM_R_eye_relax",
    # "FRM_R_eye_squeeze 1",
    "FRM_L_eye_squeeze",
    "FRM_L_eye_border",
    "FRM_L_eye_relax",
    # "FRM_L_eye_squeeze 1",
    "FRM_R_nose_nasolabialDeepen",
    "FRM_L_nose_nasolabialDeepen",
    "FRM_R_mouth_corner",
    "FRM_L_mouth_corner",
    "FRM_thickness",
    "FRM_L_mouth_thicknessD",
    "FRM_R_mouth_thicknessD",
    "FRM_R_mouth_thicknessU",
    "FRM_L_mouth_thicknessU",
    "FRM_lipsRoll",
    "FRM_L_mouth_lipsRollD",
    "FRM_R_mouth_lipsRollD",
    "FRM_R_mouth_lipsRollU",
    "FRM_L_mouth_lipsRollU",
    "FRM_cornerSharpness",
    "FRM_R_mouth_cornerShapnessU",
    "FRM_L_mouth_cornerShapnessU",
    "FRM_L_mouth_cornerShapnessD",
    "FRM_R_mouth_cornerShapnessD",
    "FRM_lipsPushPull",
    "FRM_L_mouth_pushPullD",
    "FRM_R_mouth_pushPullU",
    "FRM_L_mouth_pushPullU",
    "FRM_R_mouth_pushPullD",
    "FRM_L_brow_lateral",
    "FRM_R_brow_lateral",
    "FRM_L_brow_down",
    "FRM_R_brow_down",
    "FRM_L_brow_raiseOut",
    "FRM_L_brow_raiseIn",
    "FRM_R_brow_raiseIn",
    "FRM_R_brow_raiseOut",
    "FRM_L_eye_pupil",
    "FRM_R_eye_pupil",
    "FRM_R_eye_blink",
    "FRM_L_eye_blink",
    "FRM_R_eye_cheekRaise",
    "FRM_L_eye_cheekRaise",
    "FRM_R_eye_squintInner",
    "FRM_L_eye_squintInner",
    "FRM_L_eye",
    "FRM_R_eye",
    "FRM_C_eye",
    "FRM_R_nose",
    "FRM_L_nose",
    "FRM_R_ear_up",
    "FRM_L_ear_up",
    "FRM_R_mouth_suckBlow",
    "FRM_L_mouth_suckBlow",
    "FRM_R_mouth_openSmile",
    "FRM_L_mouth_openSmile",
    "FRM_L_mouth_lowerLipDepress",
    "FRM_R_mouth_lowerLipDepress",
    "FRM_R_mouth_stretch",
    "FRM_L_mouth_stretch",
    "FRM_R_mouth_cornerDepress",
    "FRM_L_mouth_cornerDepress",
    "FRM_R_mouth_dimple",
    "FRM_L_mouth_dimple",
    "FRM_R_mouth_cornerPull",
    "FRM_L_mouth_cornerPull",
    "FRM_R_mouth_sharpCornerPull",
    "FRM_L_mouth_sharpCornerPull",
    "FRM_R_mouth_upperLipRaise",
    "FRM_L_mouth_upperLipRaise",
    "FRM_c_mouth",
    "FRM_C_tongue_narrowWide",
    "FRM_C_tongue_inOut",
    "FRM_C_tongue_press",
    "FRM_C_tongue_roll",
    "FRM_C_tongue",
    "FRM_lipBite",
    "FRM_L_mouth_lipBiteD",
    "FRM_L_mouth_lipBiteU",
    "FRM_R_mouth_lipBiteU",
    "FRM_R_mouth_lipBiteD",
    "FRM_lipsPress",
    "FRM_L_mouth_lipsPressD",
    "FRM_L_mouth_lipsPressU",
    "FRM_R_mouth_lipsPressU",
    "FRM_R_mouth_lipsPressD",
    "FRM_mouthTighten",
    "FRM_L_mouth_tightenD",
    "FRM_L_mouth_tightenU",
    "FRM_R_mouth_tightenU",
    "FRM_R_mouth_tightenD",
    "FRM_lipsBlow",
    "FRM_L_mouth_lipsBlow",
    "FRM_R_mouth_lipsBlow",
    "FRM_mouthSuck",
    "FRM_mouth_press",
    "FRM_L_mouth_pressD",
    "FRM_L_mouth_pressU",
    "FRM_R_mouth_pressU",
    "FRM_R_mouth_pressD",
    "FRM_lipsTogether",
    "FRM_L_mouth_lipsTogetherD",
    "FRM_L_mouth_lipsTogetherU",
    "FRM_R_mouth_lipsTogetherU",
    "FRM_R_mouth_lipsTogetherD",
    "FRM_oh",
    "FRM_pucker",
    "FRM_L_mouth_puckerD",
    "FRM_L_mouth_puckerU",
    "FRM_R_mouth_puckerU",
    "FRM_R_mouth_puckerD",
    "FRM_funnel",
    "FRM_L_mouth_funnelD",
    "FRM_L_mouth_funnelU",
    "FRM_R_mouth_funnelU",
    "FRM_R_mouth_funnelD",
    "FRM_R_jaw_chinCompress",
    "FRM_L_jaw_chinCompress",
    "FRM_chinLift",
    "FRM_L_jaw_ChinLiftU",
    "FRM_L_jaw_ChinLiftD",
    "FRM_R_jaw_ChinLiftU",
    "FRM_R_jaw_ChinLiftD",
    "FRM_C_jaw_fwdBack",
    "FRM_C_jaw_clench",
    "FRM_C_jaw",
    "FRM_neckStretch",
    "FRM_C_neck_stretch",
    "FRM_L_neck_stretch",
    "FRM_R_neck_stretch",
    "FRM_C_neck_swallow",
    "FRM_convergenceSwitch",
    "FRM_convergenceGUI"
    ]

gUFCFacial_Text = [
    "TEXT_switches",
    "TEXT_lipsUpDown",
    "TEXT_tweakers",
    "TEXT_thickness",
    "TEXT_lipsRoll",
    "TEXT_cornerSharpness",
    "TEXT_lipsPushPull",
    "TEXT_lipBite",
    "TEXT_lipsPress",
    "TEXT_mouthTighten",
    "TEXT_lipsBlow",
    "TEXT_mouthPress",
    "TEXT_lipsTogether",
    "TEXT_mouthSuck",
    "TEXT_OH",
    "TEXT_pucker",
    "TEXT_funnel",
    "TEXT_neckStretch",
    "TEXT_convergence"
    ]

gUFCFacial_Unused = [
    "CTRL_L_mouth_lipsPressD",
    "CTRL_R_mouth_lipsPressD",
    "CTRL_R_neck_stretch_loc",
    "CTRL_L_neck_stretch_loc"
    ]

gUFCFacial_Other = [
    "CTRL_faceGUI",
    "CTRL_rig_Logic"
    ]

gUFCFacial_NonCtrls = (
    gUFCFacial_Frames +
    gUFCFacial_Text +
    gUFCFacial_Unused +
    gUFCFacial_Other
    )

gUFCFacial_AnalogCtrls_TR = [
    "CTRL_C_lookAt",
    "CTRL_L_LookAt",
    "CTRL_R_LookAt",
    "CTRL_analog_R_ear3",
    "CTRL_analog_L_ear3",
    "CTRL_analog_R_ear2",
    "CTRL_analog_L_ear2",
    "CTRL_analog_R_ear1",
    "CTRL_analog_L_ear1",
    "CTRL_analog_C_tongue5",
    "CTRL_analog_C_tongue4",
    "CTRL_analog_C_tongue3",
    "CTRL_analog_C_tongue2",
    "CTRL_analog_C_tongue1"
    ]

gUFCFacial_AnalogCtrls_T = [
    "CTRL_C_bit"
    ]

gUFCFacial_AnalogCtrls_Ears_TR = [
    "CTRL_analog_R_ear3",
    "CTRL_analog_L_ear3",
    "CTRL_analog_R_ear2",
    "CTRL_analog_L_ear2",
    "CTRL_analog_R_ear1",
    "CTRL_analog_L_ear1"
]

gUFCFacial_CustomCtrls = [
    "CTRL_C_chin_backFwd",
    "CTRL_C_chin_downUp",
    "CTRL_C_chin_narrowWide",
    "CTRL_C_jaw_backFwd",
    "CTRL_C_jaw_lowHigh",
    "CTRL_C_jaw_narrowWide",
    "CTRL_C_lowerLip_narrowWide",
    "CTRL_C_lowerLip_backFwd",
    "CTRL_C_lowerLip_thinThick",
    "CTRL_C_upperLip_narrowWide",
    "CTRL_C_upperLip_backFwd",
    "CTRL_C_upperLip_thinThick",
    "CTRL_R_mouth_narrowWide",
    "CTRL_L_mouth_narrowWide",
    "CTRL_C_mouth_narrowWide",
    "CTRL_R_mouth_backFwd",
    "CTRL_L_mouth_backFwd",
    "CTRL_C_mouth_backFwd",
    "CTRL_R_mouth_downUp",
    "CTRL_L_mouth_downUp",
    "CTRL_C_mouth_downUp",
    "CTRL_R_mouth_rightLeft",
    "CTRL_L_mouth_rightLeft",
    "CTRL_C_mouth_rightLeft",
    "CTRL_R_cheek_backFwd",
    "CTRL_L_cheek_backFwd",
    "CTRL_C_cheek_backFwd",
    "CTRL_R_cheek_downUp",
    "CTRL_L_cheek_downUp",
    "CTRL_C_cheek_downUp",
    "CTRL_R_cheek_narrowWide",
    "CTRL_L_cheek_narrowWide",
    "CTRL_C_cheek_narrowWide",
    "CTRL_R_ears_earlobe",
    "CTRL_L_ears_earlobe",
    "CTRL_C_ears_earlobe",
    "CTRL_R_ears_smallBig",
    "CTRL_L_ears_smallBig",
    "CTRL_C_ears_smallBig",
    "CTRL_R_ears_shortLong",
    "CTRL_L_ears_shortLong",
    "CTRL_C_ears_shortLong",
    "CTRL_R_ears_narrowWide",
    "CTRL_L_ears_narrowWide",
    "CTRL_C_ears_narrowWide",
    "CTRL_R_nose_nostril",
    "CTRL_L_nose_nostril",
    "CTRL_C_nose_nostril",
    "CTRL_C_nose_tiltRightLeft",
    "CTRL_C_nose_tiltDownUp",
    "CTRL_C_nose_smallBig",
    "CTRL_C_nose_shortLong",
    "CTRL_C_nose_narrowWide",
    "CTRL_C_eyes_angle",
    "CTRL_L_eyes_angle",
    "CTRL_R_eyes_angle",
    "CTRL_R_eyes_width",
    "CTRL_L_eyes_width",
    "CTRL_C_eyes_width",
    "CTRL_R_eyes_size",
    "CTRL_L_eyes_size",
    "CTRL_C_eyes_size",
    "CTRL_R_eyes_backFwd",
    "CTRL_L_eyes_backFwd",
    "CTRL_C_eyes_backFwd",
    "CTRL_R_eyes_downUp",
    "CTRL_L_eyes_downUp",
    "CTRL_C_eyes_downUp",
    "CTRL_R_eyes_narrowWide",
    "CTRL_L_eyes_narrowWide",
    "CTRL_C_eyes_narrowWide",
    "CTRL_R_brows_backFwd",
    "CTRL_L_brows_backFwd",
    "CTRL_C_brows_backFwd",
    "CTRL_R_brows_downUp",
    "CTRL_L_brows_downUp",
    "CTRL_C_brows_downUp",
    "CTRL_R_brows_narrowWide",
    "CTRL_L_brows_narrowWide",
    "CTRL_C_brows_narrowWide",
    "CTRL_C_weight",
    "CTRL_C_faceWidth",
    "CTRL_C_cranium_flatPointy",
    "CTRL_C_cranium_downUp",
    "CTRL_C_cranium_narrowWide"
]

gUFCFacialRoot = [ "FACIAL_C_FacialRoot" ]






# ##
# ##
# ##
# ##
# ##
# ##
# ##
# ## END - UFC Facial Rig Arrays
# ##
# ##
# ##
# ##
# ##
# ##
# ##
# ##




gAllFacialRoot = (gFacialRoot, gUFCFacialRoot)
