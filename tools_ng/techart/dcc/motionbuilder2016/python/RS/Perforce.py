# TODO refactor all code in ./_Perforce/windows.py into this module and remove ./_Perforce/windows.py
"""
Description:
    Helper functions for interfacing with Perforce

    This module used to also allow for importing from a module with Linux related commands, but that
    functionality has been removed.
"""
from RS._Perforce.windows import *

