"""
Provied an easy API to access project specific data, and tools options.
"""
import os
import platform

from RS._ProjectsData import * # Used to automatically get the project configs
from RS._ProjectsData import default


LINUX_CONST = "Linux"
WINDOWS_CONST = "Windows"


class ProjectDataManager(object):
    """
    Project Data Manager Class. Used to load in the correct project and resolve any issues
    between the project data and the wildwest data.

    Access by calling config methods on this class.
    """
    def __init__(self, project=None):

        if project is None:
            project = "other"
            if platform.system() == WINDOWS_CONST:
                project = os.getenv("RS_CORE_PROJECT")
                
                if project is None:
                    # Fall back to the project name if the core project name wasn't passed in
                    # e.g. old launcher
                    project = os.getenv("RS_PROJECT")
                
            elif platform.system() == LINUX_CONST:
                project = os.getenv("GIANT_DATA_DIR", "").split("/")[-1]

        projData = default.DefaultData.GetProject(project)
        if projData is None:
            # Try looking for a codename
            projData = default.DefaultData.GetProjectFromCodeName(project)
                # raise ValueError("Unable to find config for '{0}'".format(project))
        self._data = (projData or default.DefaultData)()

        self._wildWest = None
        if platform.system() == WINDOWS_CONST:
            from RS import Config
            if "wildwest" in Config.Tool.Path.TechArt:
                self._wildWest = default.DefaultData.GetProject("wildwest")()
        elif platform.system() == LINUX_CONST:
            self._wildWest = default.DefaultData.GetProject("wildwest")()

    def Project(self):
        """
        Get the current Project name, post-peneded with WildWest if in WildWest

        returns:
            String : Name of the project
        """
        if self._wildWest is None:
            return self._data.KEY
        return "{0}(WildWest)".format(self._data.KEY)

    def GetConfig(self, name, defaultValue=None):
        """
        Get a config setting from the project data, or use the default value if a value is not
        in the config.
        This will take WildWest into account.

        args:
            name (string): The name of the config setting

        kwarg:
            defaultValue (object): The value to return if no config is set

        returns:
            object: The value in the config, or default value, if supplied, and not in config
        """
        if self._wildWest is not None:
            config = self._wildWest.GetConfigDict(superDict=self._data.GetConfigDict())
            return config.get(name, defaultValue)

        return self._data.GetConfig(name, defaultValue=defaultValue)

    def InWildWest(self):
        """ Is the current environment in the wildwest depot """
        return self._wildWest is not None

    def __getattr__(self, name):
        """
        Python magic method, gets called when no attribute on the class of the name given is called.

        Hook for access the method from the config via the ProjectDataManager class.
        Any WildWest Specific methods will be ignored.

        args:
            name (string): Name of the method to call

        returns:
            function pointer: Pointer to the method from the config class
        """
        return getattr(self._data, name)


data = ProjectDataManager()
