from RS._ProjectsData import default


class WildWestData(default.DefaultData):
    """
    WildWest Config, that will be placed above the project config if using wild west
    """

    KEY = "wildwest"

    def GetConfigDict(self, superDict=None):
        orgDict = superDict or super(WildWestData, self).GetConfigDict()

        orgDict.update({
                # wildwest is the Rockstar Logo Orange/Yellow background color with black text
                "MenuColour": ("#ffa500", "black"),
                })
        return orgDict
