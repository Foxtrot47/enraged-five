import os
import re

from RS._ProjectsData import baseProjectData


class DefaultData(baseProjectData.BaseProjectData):
    """
    Default Config, this is the base class all configs should be derived from, allowing for a
    common base class.
    """

    KEY = "other"

    # Hand config used for the hand pose tool. If none is found, the tool will dynamically query rig components in the hand.
    HAND_CONFIG = None

    @classmethod
    def GetProject(cls, key):
        return cls.KEY_BINDINGS.get(key)

    @classmethod
    def GetProjectFromCodeName(cls, codeName):
        for value in cls.KEY_BINDINGS.itervalues():
            if codeName in value.GetAnimDataProjectCodeNames():
                return value
        return None

    @classmethod
    def GetAllProjectNames(cls):
        return cls.KEY_BINDINGS.keys()

    @classmethod
    def GetAnimDataProjectName(cls):
        return ""

    @classmethod
    def GetAnimDataProjectCodeNames(cls):
        return []

    @classmethod
    def GetSDRockstarToolsProjectId(cls):
        return 0

    def GetConfigDict(self):
        return {
            "MenuColour": ("#ffa500", "black"),
            "FPSTool": False,
            "CharSetupGestureSafeArea": False,
            "CharSetupFacingDirection": True,
            "CharSetupPrepSpine": True,
            "CharSetupNeckZeroTrns": True,
            "CharSetupFaceGUI": False,
            "CharSetupFaceTempParent": True,
            "CharSetupPelvisSetup": True,
            "CharSetupGTAFaceFix": False,
            "CharSetupMasterDummy": True,
            "CharSetupUFCSetup": True,
            "CharSetupArmRotation": True,
            "CharSetupPlot": True,
            "CharSetupUFCPreSetup": True,
            "LookAtTargetSetup": True,
            "LookAtTargetSetup": True,
            "SetupSpawnPointFromMetaData": True,
        }

    def GetConfig(self, name, defaultValue=None):
        return self.GetConfigDict().get(name)

    def GetGsAssetPrepSkeletonPath(self):
        raise NotImplementedError("Not Set In Config")

    def GetFpsChBones(self):
        raise NotImplementedError("Not Set In Config")

    def GetGsSkeletonPath(self):
        raise NotImplementedError("Not Set In Config")

    def GetAllMocapRoots(self):
        raise NotImplementedError("Not Set In Config")

    def GetNullName(self, null):
        return null.Parent.Name.lower()

    def GetSkeltonArray(self):
        return [
            "SKEL_L_Finger0_NUB",
            "SKEL_L_Finger1_NUB",
            "SKEL_L_Finger2_NUB",
            "SKEL_L_Finger3_NUB",
            "SKEL_L_Finger4_NUB",
            "SKEL_R_Finger0_NUB",
            "SKEL_R_Finger1_NUB",
            "SKEL_R_Finger2_NUB",
            "SKEL_R_Finger3_NUB",
            "SKEL_R_Finger4_NUB",
            "SKEL_L_Clavicle",
            "SKEL_L_UpperArm",
            "SKEL_L_Forearm",
            "SKEL_L_Finger00",
            "SKEL_L_Finger01",
            "SKEL_L_Finger02",
            "SKEL_L_Finger10",
            "SKEL_L_Finger11",
            "SKEL_L_Finger12",
            "SKEL_L_Finger20",
            "SKEL_L_Finger21",
            "SKEL_L_Finger22",
            "SKEL_L_Finger30",
            "SKEL_L_Finger31",
            "SKEL_L_Finger32",
            "SKEL_L_Finger40",
            "SKEL_L_Finger41",
            "SKEL_L_Finger42",
            "SKEL_R_Clavicle",
            "SKEL_R_UpperArm",
            "SKEL_R_Forearm",
            "SKEL_R_Finger00",
            "SKEL_R_Finger01",
            "SKEL_R_Finger02",
            "SKEL_R_Finger10",
            "SKEL_R_Finger11",
            "SKEL_R_Finger12",
            "SKEL_R_Finger20",
            "SKEL_R_Finger21",
            "SKEL_R_Finger22",
            "SKEL_R_Finger30",
            "SKEL_R_Finger31",
            "SKEL_R_Finger32",
            "SKEL_R_Finger40",
            "SKEL_R_Finger41",
            "SKEL_R_Finger42",
            "SKEL_L_Thigh",
            "SKEL_R_Thigh",
            "SKEL_ROOT",
            "SKEL_L_Calf",
            "SKEL_L_Foot",
            "SKEL_L_Toe0Nub",
            "SKEL_L_Toe0",
            "SKEL_R_Calf",
            "SKEL_R_Foot",
            "SKEL_R_Toe0Nub",
            "SKEL_R_Toe0",
            "SKEL_R_Hand",
            "SKEL_L_Hand",
            "SKEL_Spine0",
            "SKEL_Spine1",
            "SKEL_Spine2",
            "SKEL_Spine3",
            "SKEL_Neck_1",
            "SKEL_Head",
            "SKEL_Pelvis",
            "SKEL_Spine_Root",
            "RB_R_BumRoll",
            "RB_L_BumRoll",
            "RB_Neck_1",
            "SKEL_L_Clavicle",
            "SKEL_R_Clavicle",
            "RB_L_ArmRoll",
            "RB_R_ArmRoll",
            "RB_L_ForeArmRoll",
            "RB_R_ForeArmRoll",
            "RB_R_ForeArmRoll_1",
            "RB_R_ForeArmRoll_1",
            "RB_L_ThighRoll",
            "RB_R_ThighRoll",
            "RB_L_CalfRoll",
            "RB_R_CalfRoll",
            "PH_L_Hand",
            "PH_R_Hand",
            "IK_L_Hand",
            "IK_R_Hand",
            "PH_L_Foot",
            "PH_R_Foot",
            "IK_L_Foot",
            "IK_R_Foot",
            "IK_Head",
            "IK_Root",
            "RB_Neck",
        ]

    def GetContactsArray(self):
        return [
            "L_Foot_Contact",
            "R_Foot_Contact",
            "L_Hand_Contact",
            "R_Hand_Contact",
        ]

    def GetDummyName(self):
        """
        Get the default name of the dummy node

        Returns:
            str:
        """
        return "Dummy01"

    def GetMoverName(self):
        """
        Get the default name of the mover node

        Returns:
            str:
        """
        return "mover"

    def GetGeometryName(self):
        """
        Get the default name of the Geometry node, which should contain all the mesh of an Asset.

        Returns:
            str:
        """
        return "Geometry"

    def GetJointAssignments(self):
        """
        Get joint assignments for characters.

        Returns:
            dict {str: str}
        """
        contacts = self.GetContactsArray()
        skelArray = self.GetSkeltonArray()

        return {
            "LeftHandThumb4": skelArray[0],
            "LeftHandIndex4": skelArray[1],
            "LeftHandMiddle4": skelArray[2],
            "LeftHandRing4": skelArray[3],
            "LeftHandPinky4": skelArray[4],
            "RightHandThumb4": skelArray[5],
            "RightHandIndex4": skelArray[6],
            "RightHandMiddle4": skelArray[7],
            "RightHandRing4": skelArray[8],
            "RightHandPinky4": skelArray[9],
            "LeftShoulder": skelArray[10],
            "LeftArm": skelArray[11],
            "LeftForeArm": skelArray[12],
            "LeftHandThumb1": skelArray[13],
            "LeftHandThumb2": skelArray[14],
            "LeftHandThumb3": skelArray[15],
            "LeftHandIndex1": skelArray[16],
            "LeftHandIndex2": skelArray[17],
            "LeftHandIndex3": skelArray[18],
            "LeftHandMiddle1": skelArray[19],
            "LeftHandMiddle2": skelArray[20],
            "LeftHandMiddle3": skelArray[21],
            "LeftHandRing1": skelArray[22],
            "LeftHandRing2": skelArray[23],
            "LeftHandRing3": skelArray[24],
            "LeftHandPinky1": skelArray[25],
            "LeftHandPinky2": skelArray[26],
            "LeftHandPinky3": skelArray[27],
            "RightShoulder": skelArray[28],
            "RightArm": skelArray[29],
            "RightForeArm": skelArray[30],
            "RightHandThumb1": skelArray[31],
            "RightHandThumb2": skelArray[32],
            "RightHandThumb3": skelArray[33],
            "RightHandIndex1": skelArray[34],
            "RightHandIndex2": skelArray[35],
            "RightHandIndex3": skelArray[36],
            "RightHandMiddle1": skelArray[37],
            "RightHandMiddle2": skelArray[38],
            "RightHandMiddle3": skelArray[39],
            "RightHandRing1": skelArray[40],
            "RightHandRing2": skelArray[41],
            "RightHandRing3": skelArray[42],
            "RightHandPinky1": skelArray[43],
            "RightHandPinky2": skelArray[44],
            "RightHandPinky3": skelArray[45],
            "LeftUpLeg": skelArray[46],
            "RightUpLeg": skelArray[47],
            "Hips": skelArray[48],
            "LeftLeg": skelArray[49],
            "LeftFoot": skelArray[50],
            "LeftFootExtraFinger1": skelArray[51],
            "LeftToeBase": skelArray[52],
            "RightLeg": skelArray[53],
            "RightFoot": skelArray[54],
            "RightFootExtraFinger1": skelArray[55],
            "RightToeBase": skelArray[56],
            "RightHand": skelArray[57],
            "LeftHand": skelArray[58],
            "Spine": skelArray[59],
            "Spine1": skelArray[60],
            "Spine2": skelArray[61],
            "Spine3": skelArray[62],
            "Neck": skelArray[63],
            "Head": skelArray[64],
            "LeftFootFloor": contacts[0],
            "RightFootFloor": contacts[1],
            "LeftHandFloor": contacts[2],
            "RightHandFloor": contacts[3],
        }

    def GetGiantCharacterizationDict(self):
        """
        Gets the giant characterization naming convertion dict for the project, with the keys
        as the mobu character part name and the value as a string list of possible bone names

        Returns:
            Dict:       key:String
                        value:List of String
        """
        return {
            # Standard
            "Hips": [
                "SKEL_ROOT",
            ],
            "LeftUpLeg": [
                "SKEL_L_Thigh",
            ],
            "LeftLeg": [
                "SKEL_L_Calf",
            ],
            "LeftFoot": [
                "SKEL_L_Foot",
                "EO_L_Foot",
            ],
            "RightUpLeg": [
                "SKEL_R_Thigh",
            ],
            "RightLeg": [
                "SKEL_R_Calf",
            ],
            "RightFoot": [
                "SKEL_R_Foot",
                "EO_R_Foot",
            ],
            "Spine": [
                "SKEL_Spine0",
            ],
            "LeftArm": [
                "SKEL_L_UpperArm",
            ],
            "LeftForeArm": [
                "SKEL_L_Forearm",
            ],
            "LeftHand": [
                "SKEL_L_Hand",
            ],
            "RightArm": [
                "SKEL_R_UpperArm",
            ],
            "RightForeArm": [
                "SKEL_R_Forearm",
            ],
            "RightHand": [
                "SKEL_R_Hand",
            ],
            "Head": [
                "SKEL_Head",
            ],
            # Auxiliary
            "LeftToeBase": [
                "SKEL_L_Toe0",
                "EO_L_Toe",
            ],
            "RightToeBase": [
                "SKEL_R_Toe0",
                "EO_R_Toe",
            ],
            "LeftShoulder": [
                "SKEL_L_Clavicle",
            ],
            "RightShoulder": [
                "SKEL_R_Clavicle",
            ],
            "Neck": [
                "SKEL_Neck0",
            ],
            # Spine
            "Spine": [
                "SKEL_Spine0",
            ],
            "Spine1": [
                "SKEL_Spine1",
            ],
            "Spine2": [
                "SKEL_Spine2",
            ],
            "Spine3": [
                "SKEL_Spine3",
            ],
            "Spine4": [
                "SKEL_Spine4",
            ],
            "Spine5": [
                "SKEL_Spine5",
            ],
            "Spine6": [
                "SKEL_Spine6",
            ],
            # Left Hand
            "LeftHandThumb1": [
                "SKEL_L_Finger00",
            ],
            "LeftHandThumb2": [
                "SKEL_L_Finger01",
            ],
            "LeftHandThumb3": [
                "SKEL_L_Finger02",
            ],
            "LeftHandThumb4": [
                "SKEL_L_Finger0_NUB",
            ],
            "LeftHandIndex1": [
                "SKEL_L_Finger11",
            ],
            "LeftHandIndex2": [
                "SKEL_L_Finger12",
            ],
            "LeftHandIndex3": [
                "SKEL_L_Finger13",
            ],
            "LeftHandIndex4": [
                "SKEL_L_Finger1_NUB",
            ],
            "LeftHandMiddle1": [
                "SKEL_L_Finger21",
            ],
            "LeftHandMiddle2": [
                "SKEL_L_Finger22",
            ],
            "LeftHandMiddle3": [
                "SKEL_L_Finger23",
            ],
            "LeftHandMiddle4": [
                "SKEL_L_Finger2_NUB",
            ],
            "LeftHandRing1": [
                "SKEL_L_Finger31",
            ],
            "LeftHandRing2": [
                "SKEL_L_Finger32",
            ],
            "LeftHandRing3": [
                "SKEL_L_Finger33",
            ],
            "LeftHandRing4": [
                "SKEL_L_Finger3_NUB",
            ],
            "LeftHandPinky1": [
                "SKEL_L_Finger41",
            ],
            "LeftHandPinky2": [
                "SKEL_L_Finger42",
            ],
            "LeftHandPinky3": [
                "SKEL_L_Finger43",
            ],
            "LeftHandPinky4": [
                "SKEL_L_Finger4_NUB",
            ],
            # Right Hand
            "RightHandThumb1": [
                "SKEL_R_Finger00",
            ],
            "RightHandThumb2": [
                "SKEL_R_Finger01",
            ],
            "RightHandThumb3": [
                "SKEL_R_Finger02",
            ],
            "RightHandThumb4": [
                "SKEL_R_Finger0_NUB",
            ],
            "RightHandIndex1": [
                "SKEL_R_Finger11",
            ],
            "RightHandIndex2": [
                "SKEL_R_Finger12",
            ],
            "RightHandIndex3": [
                "SKEL_R_Finger13",
            ],
            "RightHandIndex4": [
                "SKEL_R_Finger1_NUB",
            ],
            "RightHandMiddle1": [
                "SKEL_R_Finger21",
            ],
            "RightHandMiddle2": [
                "SKEL_R_Finger22",
            ],
            "RightHandMiddle3": [
                "SKEL_R_Finger23",
            ],
            "RightHandMiddle4": [
                "SKEL_R_Finger2_NUB",
            ],
            "RightHandRing1": [
                "SKEL_R_Finger31",
            ],
            "RightHandRing2": [
                "SKEL_R_Finger32",
            ],
            "RightHandRing3": [
                "SKEL_R_Finger33",
            ],
            "RightHandRing4": [
                "SKEL_R_Finger3_NUB",
            ],
            "RightHandPinky1": [
                "SKEL_R_Finger41",
            ],
            "RightHandPinky2": [
                "SKEL_R_Finger42",
            ],
            "RightHandPinky3": [
                "SKEL_R_Finger43",
            ],
            "RightHandPinky4": [
                "SKEL_R_Finger4_NUB",
            ],
            # Left In-Hand
            "LeftInHandIndex": [
                "SKEL_L_Finger10",
            ],
            "LeftInHandMiddle": [
                "SKEL_L_Finger20",
            ],
            "LeftInHandRing": [
                "SKEL_L_Finger30",
            ],
            "LeftInHandPinky": [
                "SKEL_L_Finger40",
            ],
            # Right In-Hand
            "RightInHandIndex": [
                "SKEL_R_Finger10",
            ],
            "RightInHandMiddle": [
                "SKEL_R_Finger20",
            ],
            "RightInHandRing": [
                "SKEL_R_Finger30",
            ],
            "RightInHandPinky": [
                "SKEL_R_Finger40",
            ],
            # Neck
            "Neck": [
                "SKEL_Neck0",
            ],
            "Neck1": [
                "SKEL_Neck1",
            ],
            "Neck2": [
                "SKEL_Neck2",
            ],
        }

    def GetFacingDirectionArrowDict(self):
        """
        Used for character setup - Facing Arrows for Ingame Setup.

        Returns:
            Dict:   key:String
                    value[0]:String
                    value[1]:Int
        """
        return {"IK_Root": ("IK_FacingDirection", 0)}

    def GetHelperPrefix(self):
        """
        Used for character setup - Facing Arrows for Ingame Setup.
        Returns:
            String
        """
        return "OH", "OH_LookDir", "LookAt_Target"

    def UseOHDirections(self):
        """
        If the project should use the new OH_Directions arrows, merged in from an FBX file.
        Returns:
            bool
        """
        return False

    def GetLegacyHelpers(self):
        """
        Returrs a list of Legacy OH Helpers that needs to be removed/ignored/rewired.
        Returns:
            list
        """
        return []

    def UFCAssetSetupButtonState(self):
        """
        Used for character face setup - UFC Setup.  Will determine if the buttons, for ufc setup
        are in an active state or not.

        Returns:
            Bool
        """
        return False

    def GetGSSkelFolder(self):
        raise NotImplementedError("Not Set In Config")

    def GetDefaultGSSkelName(self):
        return "CS_Male_Proxy_gs"

    def GetDefaultHandPoseLibraryName(self):
        return "GEN_MALE_HAND_POSES"

    def LaunchVehicleControls(self):
        from RS.Tools.UI import VehicleControls

        tool = VehicleControls.VehicleControlsToolbox()
        tool.Show()
        return tool

    def fixupExpressions(self):
        """
        launches fixupExpressions script if applies to project.
        """
        return

    def GetAnimationDirectories(self):
        """
        Used for animation - Anim2Fbx. Returns the animation directories to search for animations

        Returns:
            list
        """
        return (("", ""),)

    def GetSkelFilesDirectory(self):
        """
        Returns the directory where the skel files used by the Anim2Fbx tool lives

        Return:
            string
        """
        return ""

    def GetFbxDirectories(self):
        """ Returns the directories where the cutscene and ingame fbx live for the project """

        return "", "", "", ""

    def PrepJointsCharacterSetup(self, model, femaleSetup, fileName, csSetup):
        raise NotImplementedError("Not Set")

    def SafeBoneList(self):
        raise NotImplementedError("Not Set")

    def CharacterExtensionBoneStringList(self):
        raise NotImplementedError("Not Set")

    def CharacterIKPHBoneList(self):
        raise NotImplementedError("Not Set")

    def CharacterReferenceBaseType(self):
        """Returns the reference system base type used for characters."""
        from RS.Core.ReferenceSystem.Types import CharacterComplex

        return CharacterComplex.CharacterComplex

    def CharacterAnimationControlPatterns(self, characterName):
        """
        Regular expression patterns for finding all controls relevant to animators in a character's hierarchy.
        Primarily for use with RS.Util.Scene.Model.HierarchyRegexSearcher
        """
        rootControlName = "Dummy01"
        stopRecursionList = [
            r"^{}$".format(re.escape(characterName)),
            r"^Geometry$",
            r"^FACIAL_",
            r"^Beard_",
            r"_ShoulderTwist$",
            r"_ThighRoll$",
        ]
        stopRecursionPattern = "|".join(stopRecursionList)

        excludeList = [
            stopRecursionPattern,
            r"^SCALE_",
            r"_Nub$",
            r"^RECT_",
            r"^TEXT_",
            r"^SPR_",
            r"^RB_",
            r"^MH_",
            r"^Hlp_",
            r"^CP_",
            r"^EO_",
            r"^TGT_",
            r"^SH_",
            r"^CTRL_",
            r"^HairScale_",
            r"^ES_",
            r"^Hat_",
            r"^RH_",
            r"^RZ_",
            r"^UV_",
            r"^CH_",
            r"^XH_",
            r"^OH_",
            r"^SH_",
            r"^AA_",
            r"^SC_",
            r"^SM_",
        ]
        excludePattern = "|".join(excludeList)

        forceIncludePattern = r"CTRL_PH_HAT_LOC"
        disableFilterPattern = None

        return rootControlName, stopRecursionPattern, excludePattern, forceIncludePattern, disableFilterPattern

    def PropAnimationControlPatterns(self):
        """
        Regular expression patterns for finding all controls relevant to animators in a prop's hierarchy.
        Primarily for use with RS.Util.Scene.Model.HierarchyRegexSearcher
        """
        rootControlName = "Dummy01"
        # For Props: Everything with the "_Control" suffix
        excludePattern = r"."
        forceIncludePattern = "|".join([rootControlName, r"_Control$"])
        stopRecursionPattern = None
        disableFilterPattern = None

        return rootControlName, stopRecursionPattern, excludePattern, forceIncludePattern, disableFilterPattern

    def VehicleAnimationControlPatterns(self):
        """
        Regular expression patterns for finding all controls relevant to animators in a vehicle's hierarchy.
        Primarily for use with RS.Util.Scene.Model.HierarchyRegexSearcher
        """
        rootControlName = ["Dummy01", "Chassis_Control"]
        # For Vehicles: Everything under the chassis control
        excludePattern = r"."
        forceIncludePattern = "|".join(rootControlName)
        disableFilterPattern = r"chassis_Control"
        stopRecursionPattern = None

        return rootControlName, stopRecursionPattern, excludePattern, forceIncludePattern, disableFilterPattern

    def OldLookAtRigDict(self):
        return {}

    def UseUFC(self):
        """ Setups the background processes for running the facial animation in Motion Builder """
        return False

    def FinalFBXFolderPath(self):
        """
        Default to RDR path as thats the most used project at the moment
        Returns: file path to rdr final fbx files
        """
        return os.path.join("x:\\", "projects", "bob", "fbx_final")

    def GetAnimExporterApiVersion(self):
        return 1

    def GetWeaponBonePrefix(self):
        """Gets the prefix used for weapon bones

        Returns:
            str: Weapon bone prefix.
        """
        return "Gun_"

    def GetPropControlNameFromBoneName(self, propBoneName, propIsWeapon=False):
        """Gets the prop's control name string corresponding to the given bone name"""
        return "{}_Control".format(propBoneName)

    def GetAllMocapReviewTrialPaths(self):
        """Get all folders where the .rsrefine.fbx files for trials are stored."""
        rsrefineFbxPaths = []
        for mocapRoot in self.GetAllMocapRoots():  # required for the main project and its DLC project(s).
            rsrefineFbxPaths.append(os.path.join(mocapRoot, "capture", "takes"))
        return rsrefineFbxPaths

    def GetVirtualCameraHudPath(self):
        """
        Get the p4 path to the Virtual Camera HUD Path
        """
        raise NotImplementedError("Not Set In Config")

    def GetAnimalBoneNames(self, animalType, targetSide):
       """returns empty tuple if we're not on a recognised project"""
       return ()
