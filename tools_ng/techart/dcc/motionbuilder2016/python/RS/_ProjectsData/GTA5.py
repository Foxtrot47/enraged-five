import os

import pyfbsdk as mobu

from RS._ProjectsData import default
from RS import Config


class HandConfig(object):
    """
    For the Hand Pose tool. The hand components found in the skeleton/rig for GTA V.
    """
    effectorId = mobu.FBEffectorId
    nodeId = mobu.FBBodyNodeId

    LeftFingerEffectorArray = (
        effectorId.kFBLeftHandThumbEffectorId,
        effectorId.kFBLeftHandIndexEffectorId,
        effectorId.kFBLeftHandMiddleEffectorId,
        effectorId.kFBLeftHandRingEffectorId,
        effectorId.kFBLeftHandPinkyEffectorId,
        effectorId.kFBLeftWristEffectorId,
    )

    RightFingerEffectorArray = (
        effectorId.kFBRightHandThumbEffectorId,
        effectorId.kFBRightHandIndexEffectorId,
        effectorId.kFBRightHandMiddleEffectorId,
        effectorId.kFBRightHandRingEffectorId,
        effectorId.kFBRightHandPinkyEffectorId,
        effectorId.kFBRightWristEffectorId,
    )

    HandMapping = (
        "Thumb", "Thumb", "Thumb",
        "Index", "Index", "Index",
        "Middle", "Middle", "Middle",
        "Ring", "Ring", "Ring",
        "Pinky", "Pinky", "Pinky",
    )

    IdRange = (
        (3, 0),
        (3, 3),
        (3, 6),
        (3, 9),
        (3, 12),
        (1, 15),
    )

    LeftControlNames = (
        "LeftHandThumb1", "LeftHandThumb2", "LeftHandThumb3",
        "LeftHandIndex1", "LeftHandIndex2", "LeftHandIndex3",
        "LeftHandMiddle1", "LeftHandMiddle2", "LeftHandMiddle3",
        "LeftHandRing1", "LeftHandRing2", "LeftHandRing3",
        "LeftHandPinky1", "LeftHandPinky2", "LeftHandPinky3",
    )

    RightControlNames = (
        "RightHandThumb1", "RightHandThumb2", "RightHandThumb3",
        "RightHandIndex1", "RightHandIndex2", "RightHandIndex3",
        "RightHandMiddle1", "RightHandMiddle2", "RightHandMiddle3",
        "RightHandRing1", "RightHandRing2", "RightHandRing3",
        "RightHandPinky1", "RightHandPinky2", "RightHandPinky3",
    )

    LeftIdArray = (
        nodeId.kFBLeftThumbANodeId, nodeId.kFBLeftThumbBNodeId, nodeId.kFBLeftThumbCNodeId,
        nodeId.kFBLeftIndexANodeId, nodeId.kFBLeftIndexBNodeId, nodeId.kFBLeftIndexCNodeId,
        nodeId.kFBLeftMiddleANodeId, nodeId.kFBLeftMiddleBNodeId, nodeId.kFBLeftMiddleCNodeId,
        nodeId.kFBLeftRingANodeId, nodeId.kFBLeftRingBNodeId, nodeId.kFBLeftRingCNodeId,
        nodeId.kFBLeftPinkyANodeId, nodeId.kFBLeftPinkyBNodeId, nodeId.kFBLeftPinkyCNodeId,
    )

    RightIdArray = (
        nodeId.kFBRightThumbANodeId, nodeId.kFBRightThumbBNodeId, nodeId.kFBRightThumbCNodeId,
        nodeId.kFBRightIndexANodeId, nodeId.kFBRightIndexBNodeId, nodeId.kFBRightIndexCNodeId,
        nodeId.kFBRightMiddleANodeId, nodeId.kFBRightMiddleBNodeId, nodeId.kFBRightMiddleCNodeId,
        nodeId.kFBRightRingANodeId, nodeId.kFBRightRingBNodeId, nodeId.kFBRightRingCNodeId,
        nodeId.kFBRightPinkyANodeId, nodeId.kFBRightPinkyBNodeId, nodeId.kFBRightPinkyCNodeId,
    )


class Gta5Data(default.DefaultData):
    """
    GTA5 Config
    """
    KEY = "gta5"
    HAND_CONFIG = HandConfig()

    @classmethod
    def GetAnimDataProjectName(cls):
        return "GTA 5"

    @classmethod
    def GetAnimDataProjectCodeNames(cls):
        return ["paradise", "paradise_dlc"]

    @classmethod
    def GetSDRockstarToolsProjectId(cls):
        return 1

    def GetMocapRoot(self):
        return os.path.join("x:\\", "projects", "paradise_dlc")

    def GetAllMocapRoots(self):
        return [os.path.join("x:\\", "projects", "paradise"),
                os.path.join("x:\\", "projects", "paradise_dlc")]
                
    def GetGsSkeletonPath(self):
        return os.path.join(self.GetMocapRoot(), "assets", "fbx", "gs_characters")

    def GetAllGsSkeletonPaths(self):
        gsSkelPaths = []
        for mocapRoot in self.GetAllMocapRoots():
            gsSkelPaths.append(os.path.join(mocapRoot, "assets", "fbx", "gs_characters"))
        return gsSkelPaths

    def GetGsAssetPrepSkeletonPath(self):
        return os.path.join(self.GetMocapRoot(), "assetPrep", "fbxPrep", "characters")
        
    def GetNullName(self, null):
        return null.Name.lower()

    def GetConfigDict(self):
        orgDict = super(Gta5Data, self).GetConfigDict()

        orgDict.update({
                # GTA is the Rockstar North Logo Blue background color with white text
                "MenuColour": ("#004684", "white"),
                # the bugstar user ids for gta5 teams can be found at
                # https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest/Projects/1546/Defaults
                "TechArtBugstarId": 546382,
                "FPSTool": True,
                "CharSetupGestureSafeArea":True,
                "CharSetupFacingDirection":False,
                "CharSetupPrepSpine":False,
                "CharSetupNeckZeroTrns":False,
                "CharSetupFaceGUI":True,
                "CharSetupFaceTempParent":True,
                "CharSetupPelvisSetup":False,
                "CharSetupGTAFaceFix":True,
                "CharSetupMasterDummy":False,
                "CharSetupUFCSetup":False,
                "CharSetupArmRotation":False,
                "CharSetupPlot":False,
                "CharSetupUFCPreSetup":False,
                "LookAtTargetSetup":False,
                "SetupSpawnPointFromMetaData":False,
                })
        return orgDict

    def GetGSSkelFolder(self):
        """
        Get folder path for where the gs skeletons live

        Returns:
            String (file path)
        """
        filePath = os.path.join(Config.Project.Path.MocapRoot,
                                'fbx',
                                'gs')
        return filePath

    def GetDefaultGSSkelName(self):
        return "CS_Male_Proxy_gs"

    def GetDefaultHandPoseLibraryName(self):
        return "GTAV_HandPoses"

    def fixupExpressions(self):
        """ launches fixupExpressions script if applies to project. """
        return

    def GetAnimationDirectories(self):
        """
        Used for animation - Anim2Fbx. Returns the animation directories to search for animations

        Returns:
            list
        """
        directories = [("GTA5", "x:\\gta5\\art\\ng\\anim\\export_mb")]
        directories += [(dlc.Name, os.path.join(dlc.Path.Art, "anim", "export_mb"))
                        for dlc in Config.DLCProjects.itervalues()]
        return tuple(directories)

    def GetGiantCharacterizationDict(self):
        """
        Gets the giant characterization naming convertion dict for the project, with the keys
        as the mobu character part name and the value as a string list of possible bone names

        Returns:
            Dict:       key:String
                        value:List of String
        """
        return {
            # Standard
            "Hips": ["SKEL_ROOT",],
            "LeftUpLeg": ["SKEL_L_Thigh",],
            "LeftLeg": ["SKEL_L_Calf",],
            "LeftFoot": ["SKEL_L_Foot","EO_L_Foot",],
            "RightUpLeg": ["SKEL_R_Thigh",],
            "RightLeg": ["SKEL_R_Calf",],
            "RightFoot": ["SKEL_R_Foot","EO_R_Foot",],
            "Spine": ["SKEL_Spine0",],
            "LeftArm": ["SKEL_L_UpperArm",],
            "LeftForeArm": ["SKEL_L_Forearm",],
            "LeftHand": ["SKEL_L_Hand",],
            "RightArm": ["SKEL_R_UpperArm",],
            "RightForeArm": ["SKEL_R_Forearm",],
            "RightHand": ["SKEL_R_Hand",],
            "Head": ["SKEL_Head",],

            # Auxiliary
            "LeftToeBase": ["SKEL_L_Toe0","EO_L_Toe",],
            "RightToeBase": ["SKEL_R_Toe0","EO_R_Toe",],
            "LeftShoulder": ["SKEL_L_Clavicle",],
            "RightShoulder": ["SKEL_R_Clavicle",],
            "Neck": ["SKEL_Neck0",],

            # Spine
            "Spine": ["SKEL_Spine0",],
            "Spine1": ["SKEL_Spine1",],
            "Spine2": ["SKEL_Spine2",],
            "Spine3": ["SKEL_Spine3",],
            "Spine4": ["SKEL_Spine4",],
            "Spine5": ["SKEL_Spine5",],
            "Spine6": ["SKEL_Spine6",],

            # Left Hand
            "LeftHandThumb1": ["SKEL_L_Finger00",],
            "LeftHandThumb2": ["SKEL_L_Finger01",],
            "LeftHandThumb3": ["SKEL_L_Finger02",],
            "LeftHandThumb4": ["SKEL_L_Finger0_NUB",],
            "LeftHandIndex1": ["SKEL_L_Finger10",],
            "LeftHandIndex2": ["SKEL_L_Finger11",],
            "LeftHandIndex3": ["SKEL_L_Finger12",],
            "LeftHandIndex4": ["SKEL_L_Finger1_NUB",],
            "LeftHandMiddle1": ["SKEL_L_Finger20",],
            "LeftHandMiddle2": ["SKEL_L_Finger21",],
            "LeftHandMiddle3": ["SKEL_L_Finger22",],
            "LeftHandMiddle4": ["SKEL_L_Finger2_NUB",],
            "LeftHandRing1": ["SKEL_L_Finger30",],
            "LeftHandRing2": ["SKEL_L_Finger31",],
            "LeftHandRing3": ["SKEL_L_Finger32",],
            "LeftHandRing4": ["SKEL_L_Finger3_NUB",],
            "LeftHandPinky1": ["SKEL_L_Finger40",],
            "LeftHandPinky2": ["SKEL_L_Finger41",],
            "LeftHandPinky3": ["SKEL_L_Finger42",],
            "LeftHandPinky4": ["SKEL_L_Finger4_NUB",],

            # Right Hand
            "RightHandThumb1": ["SKEL_R_Finger00",],
            "RightHandThumb2": ["SKEL_R_Finger01",],
            "RightHandThumb3": ["SKEL_R_Finger02",],
            "RightHandThumb4": ["SKEL_R_Finger0_NUB",],
            "RightHandIndex1": ["SKEL_R_Finger10",],
            "RightHandIndex2": ["SKEL_R_Finger11",],
            "RightHandIndex3": ["SKEL_R_Finger12",],
            "RightHandIndex4": ["SKEL_R_Finger1_NUB",],
            "RightHandMiddle1": ["SKEL_R_Finger20",],
            "RightHandMiddle2": ["SKEL_R_Finger21",],
            "RightHandMiddle3": ["SKEL_R_Finger22",],
            "RightHandMiddle4": ["SKEL_R_Finger2_NUB",],
            "RightHandRing1": ["SKEL_R_Finger30",],
            "RightHandRing2": ["SKEL_R_Finger31",],
            "RightHandRing3": ["SKEL_R_Finger32",],
            "RightHandRing4": ["SKEL_R_Finger3_NUB",],
            "RightHandPinky1": ["SKEL_R_Finger40",],
            "RightHandPinky2": ["SKEL_R_Finger41",],
            "RightHandPinky3": ["SKEL_R_Finger42",],
            "RightHandPinky4": ["SKEL_R_Finger4_NUB",],

            # Neck
            "Neck": ["SKEL_Neck_1",],
            }

    def VehicleAnimationControlPatterns(self):
        """Regular expression patterns for finding all controls relevant to animators in a vehicle's hierarchy.
        Primarily for use with RS.Util.Scene.Model.HierarchyRegexSearcher

        Returns:
            tuple: Each control pattern rootControlName, stopRecursionPattern, excludePattern, forceIncludePattern,
                and disableFilterPattern
        """
        rootControlName = ("Dummy01", "Chassis_Control", "chassis", "mover", "rig")
        # For Vehicles: Everything under the chassis control
        excludePattern = r"."
        forceIncludePattern = "|".join(rootControlName)
        disableFilterPattern = r"chassis_Control"
        stopRecursionPattern = None

        return rootControlName, stopRecursionPattern, excludePattern, forceIncludePattern, disableFilterPattern

    def GetSkelFilesDirectory(self):
        """
        Returns the directory where the skel files used by the Anim2Fbx tool lives

        Return:
            string
        """
        return os.path.join(Config.Tool.Path.Root, "etc", "config", "anim", "skeletons", "sp")

    def GetFbxDirectories(self):
        """ Returns the directories where the cutscene and ingame fbx live for the project """
        ingamePath = os.path.join(Config.Project.Path.Art, "anim", "source_fbx")
        dlcIngamePaths = [os.path.join(dlc.Path.Art, "anim", "source_fbx")
                          for dlc in Config.DLCProjects.itervalues()]

        cutscenePath = os.path.join(Config.Project.Path.Art, "animation", "cutscene", "!!scenes")
        dlcCutscenePaths = [os.path.join(dlc.Path.Art, "animation", "cutscene", "!!scenes")
                            for dlc in Config.DLCProjects.itervalues()]

        return cutscenePath, dlcCutscenePaths, ingamePath, dlcIngamePaths

    def PrepJointsCharacterSetup(self, model, femaleSetup, fileName, csSetup):
        '''
        For Character setup script.
        Args:
            model - FBModel
            femaleSetup - boolean (whether the character is female or not)
            filename - string (name of the file)
            csSetup - boolean (whether this is run for cutscene setup or not)
        Returns:
            vector - for translation
            vector - for color
            boolean - for global or local
            float - for size
            integer - key for prep type
        '''
        # bone prep type
        jointPrep = 0
        spinePrep = 1
        colorPrep = 2

        # color key
        red = (1, 0, 0)
        orange = (0.9, 0.4, 0)
        pink = (1, 0, 1)
        green = (0.4, 0.7, 0)
        yellow = (1, 1, 0)
        darkBlue = (0, 0.2, 0.65)
        cyan = (0, 1, 1)

        # skel bone list
        skelArray = self.GetSkeltonArray()

        # prep the bones
        if model.Name == skelArray[48]:
            return (0, 0,-180, red, False, None, jointPrep)
        elif model.Name == skelArray[65]:
            return (0,90,0, darkBlue, False, None, jointPrep)
        elif model.Name == skelArray[66]:
            return (0,-90,0, darkBlue, False, None, jointPrep)
        if "Male" in fileName or "Female" in fileName:
            if model.Name in [skelArray[59], skelArray[60], skelArray[61], skelArray[62], skelArray[63],
                              skelArray[64]]:
                return (0,0,0, darkBlue, False, None, jointPrep)
            elif model.Name == skelArray[69]:
                return (0,0,0, cyan, False, None, jointPrep)
        else:
            if model.Name in [skelArray[59], skelArray[60], skelArray[61], skelArray[62], skelArray[63],
                              skelArray[64]]:
                return (None, None, None, darkBlue, None, None, colorPrep)
            elif model.Name == skelArray[69]:
                return (None, None, None, cyan, None, None, colorPrep)
        if model.Name == skelArray[10]:
            return (90,-71.880,-90, green, False, None, jointPrep)
        elif model.Name == skelArray[11]:
            return (0,0,18.12, green, False, None, jointPrep)
        elif model.Name == skelArray[12]:
            return (0,0,0, green, False, None, jointPrep)
        elif model.Name in [skelArray[72], skelArray[74], skelArray[76]]:
            return (0,0,0, yellow, False, None, jointPrep)
        elif model.Name == skelArray[58]:
            return (0,0,0, green, False, 0.5, jointPrep)
        if csSetup == 1:
            if model.Name in [skelArray[13], skelArray[14], skelArray[15]]:
                return (-90,0,0, green, False, 0.5, jointPrep)
        else:
            if model.Name in [skelArray[13], skelArray[14], skelArray[15]]:
                return (None, None, None, green, None, 0.5, colorPrep)

        if model.Name in [skelArray[16],skelArray[19], skelArray[22], skelArray[25]]:
            return (-90,0,0, green, False, 0.5, jointPrep)
        elif model.Name in [skelArray[17], skelArray[18], skelArray[20], skelArray[21], skelArray[23],
                          skelArray[24], skelArray[26], skelArray[27]]:
            return (0,0,0, green, False, 0.5, jointPrep)
        elif model.Name == skelArray[18]:
            return (0,0,0, green, False, 0.5, jointPrep)

        elif model.Name == skelArray[28]:
            return (-90,71.880,-90, orange, False, None, jointPrep)
        elif model.Name == skelArray[29]:
            return (0,0,18.12, orange, False, None, jointPrep)

        elif model.Name in [skelArray[73], skelArray[75], skelArray[77]]:
            return (0,0,0, pink, False, None, jointPrep)
        elif model.Name == skelArray[30]:
            return (0,0,0, orange, False, None, jointPrep)
        elif model.Name == skelArray[57]:
            return (0,0,0, orange, False, 0.5, jointPrep)

        if csSetup == 1:
            if model.Name in [skelArray[31], skelArray[32], skelArray[33]]:
                return (90,0,0, orange, False, 0.5, jointPrep)
        else:
            if model.Name in [skelArray[31], skelArray[32], skelArray[33]]:
                return (None, None, None, orange, None, 0.5, colorPrep)
        if model.Name in [skelArray[34], skelArray[37], skelArray[40],skelArray[43]]:
            return (-90,0,0, orange, False, 0.5, jointPrep)
        elif model.Name in [skelArray[35],skelArray[36], skelArray[38], skelArray[39], skelArray[41],
                            skelArray[42], skelArray[44], skelArray[45]]:
            return (0,0,0, orange, False, 0.5, jointPrep)
        elif model.Name in [skelArray[46], skelArray[49]]:
            return (0,0,0, green, False, None, jointPrep)
        elif model.Name in [skelArray[78], skelArray[80]]:
            return (0,0,0, yellow, False, None, jointPrep)
        elif model.Name in [skelArray[47], skelArray[53]]:
            return (0,0,0, orange, False, None, jointPrep)
        elif model.Name in [skelArray[79], skelArray[81]]:
            return (0,0,0, pink, False, None, jointPrep)

        return (None, None, None, None, None, None, None)

    def SafeBoneList(self):
        '''
        list of bones
        Returns:
            list of strings
        '''
        # skel bone list
        skelArray = self.GetSkeltonArray()

        # safe bone list
        safeBoneList = [skelArray[10],
                        skelArray[11],
                        skelArray[12],
                        skelArray[13],
                        skelArray[14],
                        skelArray[15],
                        skelArray[16],
                        skelArray[17],
                        skelArray[18],
                        skelArray[19],
                        skelArray[20],
                        skelArray[21],
                        skelArray[22],
                        skelArray[23],
                        skelArray[24],
                        skelArray[25],
                        skelArray[26],
                        skelArray[27],
                        skelArray[28],
                        skelArray[29],
                        skelArray[30],
                        skelArray[31],
                        skelArray[32],
                        skelArray[33],
                        skelArray[34],
                        skelArray[35],
                        skelArray[36],
                        skelArray[37],
                        skelArray[38],
                        skelArray[39],
                        skelArray[40],
                        skelArray[41],
                        skelArray[42],
                        skelArray[43],
                        skelArray[44],
                        skelArray[45],
                        skelArray[46],
                        skelArray[47],
                        skelArray[48],
                        skelArray[49],
                        skelArray[50],
                        skelArray[52],
                        skelArray[53],
                        skelArray[54],
                        skelArray[56],
                        skelArray[57],
                        skelArray[58],
                        skelArray[59],
                        skelArray[60],
                        skelArray[61],
                        skelArray[62],
                        skelArray[63],
                        skelArray[64],
                        skelArray[65],
                        skelArray[66],
                        skelArray[82],
                        skelArray[83],
                        skelArray[84],
                        skelArray[85],
                        skelArray[86],
                        skelArray[87],
                        skelArray[88],
                        skelArray[89],]
        return safeBoneList

    def CharacterExtensionBoneStringList(self):
        '''
        list of bone names for populating character extensions
        Returns:
            list of strings
        '''
        # skel bone list
        skelArray = self.GetSkeltonArray()

        boneList = ["mover",
                    skelArray[82],
                    skelArray[83],
                    skelArray[86],
                    skelArray[87],
                    skelArray[84],
                    skelArray[85],
                    skelArray[88],
                    skelArray[89],
                    skelArray[90],
                    skelArray[91],
                    "IK_FacingDirection",
                    "HeelHeight",
                    "BagBody",
                    "BagBone_R",
                    "BagBone_L",]
        return boneList

    def CharacterIKPHBoneList(self):
        '''
        list of IK/PH bone names
        Returns:
            list of strings
        '''
        # skel bone list
        skelArray = self.GetSkeltonArray()

        boneList = ['mover',
                    skelArray[82],
                    skelArray[83],
                    skelArray[86],
                    skelArray[87],
                    skelArray[84],
                    skelArray[85],
                    skelArray[88],
                    skelArray[89],
                    skelArray[90],
                    skelArray[91],]
        return boneList

    def OldExpressionBoneNameRegex(self):
        """
        Returns a list of all the names of old expression Constraints
        """
        nameRegex = ("abs(x)|acos(x)|atan(x)|ceil(x)|comp(v,i)|cos(x)|"
                     "degToRad(x)|e|exp(x)|floor(x)|length(v)|ln(x)|log(x)|max(x,y)|"
                     "min(x,y)|pi|mod(x,y)|radToDeg(x)|sin(x)|sqrt(x)|tan(x)|unit(v)|"
                     "vif(c,v1,v2)|RB_L_ThighRoll-Y_rotation|RB_L_ThighRoll-Z_rotation|RB_R_ThighRoll-Y_rotation|"
                     "RB_R_ThighRoll-Z_rotation|RB_L_ForeArmRoll-Y_rotation|RB_L_ForeArmRoll-Z_rotation|"
                     "RB_L_ArmRoll-Y_rotation|RB_L_ArmRoll-Z_rotation|RB_R_ForeArmRoll-Y_rotation|"
                     "RB_R_ForeArmRoll-Z_rotation|RB_R_ArmRoll-Y_rotation|RB_R_ArmRoll-Z_rotation|"
                     "EO_L_Foot-Z_rotation|EO_L_Toe-Z_rotation|PH_L_Heel-X_Position|PH_L_Heel-Y_Position|"
                     "RB_L_KneeFront-X_rotation|RB_L_KneeFront-Y_rotation|RB_L_KneeFront-Z_rotation|"
                     "MH_L_KneeBack-X_Position|EO_R_Foot-Z_rotation|EO_R_Toe-Z_rotation|PH_R_Heel-X_Position|"
                     "PH_R_Heel-Y_Position|RB_R_KneeFront-X_rotation|RB_R_KneeFront-Y_rotation|"
                     "RB_R_KneeFront-Z_rotation|MH_R_KneeBack-X_Position|RB_L_ThighRoll-X_rotation|"
                     "RB_R_ThighRoll-X_rotation|RB_L_BumRoll-X_Position|RB_L_BumRoll-Y_Position|"
                     "RB_L_BumRoll-Z_Position|RB_L_BumRoll-Y_rotation|"
                     "RB_L_BumRoll-Z_rotation|RB_L_BumRoll-X_rotation|RB_R_BumRoll-X_Position|"
                     "RB_R_BumRoll-Y_Position|RB_R_BumRoll-Z_Position|RB_R_BumRoll-Y_rotation|"
                     "RB_R_BumRoll-Z_rotation|RB_R_BumRoll-X_rotation|RB_Satchel-X_rotation|"
                     "RB_Satchel-Y_rotation|MH_Hair_Scale-X_scale|MH_Hair_Scale-Y_scale|"
                     "MH_Hair_Scale-Z_scale|MH_Hair_Crown-X_Position|MH_Hair_Crown-X_scale|"
                     "MH_Hair_Crown-Y_scale|MH_Hair_Crown-Z_scale|MH_L_Finger41-X_Position|"
                     "MH_L_HandSide-X_Position|MH_L_HandSide-Z_Position|MH_L_Finger31-X_Position|"
                     "MH_L_Finger21-X_Position|MH_L_Finger11-X_Position|MH_L_Finger01-X_Position|"
                     "MH_L_Finger01Bulge-X_Position|MH_L_Finger01Top-X_Position|MH_L_Finger01Top-Y_Position|"
                     "MH_L_Finger01Top-Z_Position|RB_L_ForeArmRoll-X_rotation|MH_L_Wrist-X_rotation|"
                     "RB_L_ArmRoll-X_rotation|MH_L_Elbow-X_Position|MH_L_Elbow-Y_Position|"
                     "MH_R_Finger01-X_Position|MH_R_Finger01Bulge-X_Position|MH_R_Finger11-X_Position|"
                     "MH_R_Finger21-X_Position|MH_R_Finger31-X_Position|MH_R_Finger41-X_Position|"
                     "MH_R_HandSide-X_Position|MH_R_HandSide-Z_Position|MH_R_Finger01Top-X_Position|"
                     "MH_R_Finger01Top-Y_Position|MH_R_Finger01Top-Z_Position|RB_R_ForeArmRoll-X_rotation|"
                     "MH_R_Wrist-X_rotation|RB_R_ArmRoll-X_rotation|MH_R_Elbow-X_Position|MH_R_Elbow-Y_Position|"
                     "MH_L_Jacket2-Y_Position|MH_L_Jacket2-Z_Position|MH_R_Jacket2-Y_Position|"
                     "MH_R_Jacket2-Z_Position|MH_L_Jacket1-Y_Position|MH_L_Jacket1-Z_Position|"
                     "MH_R_Jacket1-Y_Position|MH_R_Jacket1-Z_Position|RB_L_OpenJacket-Y_rotation|"
                     "RB_L_OpenJacket-Z_rotation|RB_R_OpenJacket-Y_rotation|RB_R_OpenJacket-Z_rotation|"
                     "LOC_R_eyeDriver_orientConstraint1|LOC_L_eyeDriver_orientConstraint1|"
                     "GRP_convergenceGUI_parentConstraint1|GRP_C_lookAt_parentConstraint1|"
                     "GRP_L_lookAtOffset_parentConstraint1|GRP_R_lookAtOffset_parentConstraint1|"
                     "LOC_L_LookAtDriver_aimConstraint1|LOC_R_LookAtDriver_aimConstraint1|"
                     "LOC_L_LookAtDriverOffset_aimConstraint1|LOC_R_LookAtDriverOffset_aimConstraint1|"
                     "setup_relationConstraint|3Lateral UFC|")
        return nameRegex

    def CharacterReferenceBaseType(self):
        """Returns the reference system base type used for characters."""
        from RS.Core.ReferenceSystem.Types import CharacterComplex
        return CharacterComplex.CharacterComplex

    def GetHelperPrefix(self):
        '''
        Used for character setup - Facing Arrows for Ingame Setup.

        Returns:
            String, String
        '''
        return "IK", "IK_Head", "IK_Head_Lookat"

    def OldLookAtRigDict(self):
        """
        Dict of 'old' LookAt rig components and their FB Type
        url:bugstar:2575815
        To be used in the ref editor to remove components

        Returns:
                Dict[string] = list(FBType)
        """
        return {'OH_LookDir_Lookat': [mobu.FBModel, mobu.FBMaterial, mobu.FBGroup],
                'IK_Control_Rig': [mobu.FBModel],
                'TargetParent': [mobu.FBModel],
                'MoverSpaceTarget': [mobu.FBModel],
                'HeadSpaceTarget': [mobu.FBModel],
                'EyeOrigin': [mobu.FBModel],
                'Look_At_Direction': [mobu.FBModel],
                'MoverTracer': [mobu.FBConstraint],
                'HeadTracer': [mobu.FBConstraint],
                'EyeOriginRotation': [mobu.FBConstraint],
                'Head_Delta': [mobu.FBConstraint],
                'Lookat_Aim': [mobu.FBConstraint],
                'MoverSpaceConstraint': [mobu.FBConstraint],
                'HeadSpaceConstraint': [mobu.FBConstraint],
                'FollowHead_Bool': [mobu.FBConstraint],
                'IK_Head_Eye_Control': [mobu.FBConstraint],
                'Eye_Track_Bool': [mobu.FBConstraint],
                'OH_LookDir_LookAtNull': [mobu.FBGroup]}

    def FinalFBXFolderPath(self):
        return os.path.join("x:\\", "projects", "paradise_dlc", "fbx_final")

    def GetVirtualCameraHudPath(self):
        """
        Get the p4 path to the Virtual Camera HUD Path
        """
        return "//depot/gta5/techart/dcc/motionbuilder2016/images/Camera/masks/TPVC_Cam_HUD.fbx"
