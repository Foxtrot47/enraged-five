"""
Description:
    Builds the Python Documentation for the RS Lib
"""
import os
import subprocess
import datetime


def CreateRSTAndCacheFiles(progressBar=None):
    """
    Builds the RST files for Sphinx and teh cache files used by the Motion Builder Tools

    Arguments:
        progressBar (RS.Tools.UI.ExternalProgressBar.Launcher.ProgressBarController): progress bar to show which files
            are being cached.

    """
    import RS.Perforce
    from RS.Docs import RSTConverter

    mobuDirectory = os.path.dirname(RS.Config.Script.Path.Root)
    rootDirectory = os.path.join(RS.Config.Script.Path.Root, "RS")
    # Check Out Cache and Help Files so they can be edited

    checkOutFiles = (os.path.join(rootDirectory, "Docs", "_cache", "..."),
                     os.path.join(rootDirectory, "Docs", "RST", "..."),
                     os.path.join(mobuDirectory, "help", "..."))


    if progressBar:
        progressBar.Update(value=0, maximum=len(RSTConverter.GetAllPythonFiles(rootDirectory)) + progressBar.Maximum/10)
        progressBar.Update(value=1, text="Checking out help and cache files from Perforce")

    RS.Perforce.Edit(checkOutFiles)
    if progressBar:
        progressBar.Update(value=2, text="Creating Index RST File")

    RSTConverter.CreateIndexFile(os.path.join(rootDirectory))
    RSTConverter.CreateRstFiles(rootDirectory, progressbar=progressBar)

    if progressBar:
        # The +10 is to have a bigger visual bugger
        progressBar.Update(text="Generating Sphinx Documentation", value=progressBar.Value + 1)


def CreateDocumentation(rebuild=True):
    """
    Builds documentation using Sphinx.

    Arguments:
        rebuild (boolean): rebuilds all the rst files and generates a new index page
        warnOnFinish (boolean): pops up a message box when the documentation finishes building

    """
    import os
    import time
    import sphinx.application

    import RS.Config
    import RS.Perforce
    from RS.Tools.UI.ExternalProgressBar import Launcher

    start = time.time()

    # Create a _static directory.  Not sure why Sphinx wants this here, but it does.
    staticDir = '{0}\\RS\\Docs\\_static'.format(RS.Config.Script.Path.Root)
    if not os.path.isdir(staticDir):
        os.makedirs(staticDir)

    # try:
    #     progressbar = Launcher.ProgressBarController(text="Generating Documentation")
    # except:
    progressbar = None


    if rebuild:
        CreateRSTAndCacheFiles(progressBar=progressbar)

    docBuilder = sphinx.application.Sphinx('{0}\\RS\\Docs\\RST\\'.format(RS.Config.Script.Path.Root),
                                           '{0}\\RS\\Docs\\'.format(RS.Config.Script.Path.Root),
                                           '{0}\\..\\help\\'.format(RS.Config.Script.Path.Root),
                                           '{0}\\..\\help\\'.format(RS.Config.Script.Path.Root),
                                           None)
    docBuilder.build(True, [])

    # This is to clear the global CACHE DIRECTORY variable after build is ran incase this method is ran
    # multiple times within the same python session
    from RS.Docs import conf
    reload(conf)
    if progressbar:
        progressbar.Update(text="Documentation Built. Total Time: {}".format(
            datetime.timedelta(0, int(time.time() - start))),
                           value=progressbar.Maximum - 1)
        time.sleep(5)
        progressbar.Update(value=progressbar.Maximum)


def MotionBuilderBatch():
    """
    Launches a new session of Motion Builder to create the documentation of the RS Python Library
    """
    # Create temporary file to call build
    directoryName = os.path.dirname(__file__)
    for _ in xrange(2):
        directoryName = os.path.dirname(directoryName)

    filepath = os.path.join(os.path.dirname(directoryName), "BatchBuild.py")
    with open(filepath, "w+") as batchFile:
        batchFile.write("import os\n"
                        "from RS.Tools.UI import Application\n"
                        "from RS.Docs import Build\n"
                        "from RS.Utils import Scene\n"
                        #"Application.GetMainWindow().hide()\n"
                        "Build.CreateDocumentation(rebuild=True)\n"
                        "if os.path.exists(''.join((r'{filepath}', 'c'))): os.remove(''.join((r'{filepath}', 'c')))\n"
                        #"Application.GetMainWindow().show()\n"
                        "Scene.Close()"
                        "".format(filepath=filepath))

    mobuExePath = r'C:\Program Files\Autodesk\MotionBuilder 2014\bin\x64\motionbuilder.exe'
    mbAppFolder = "C:\\Program Files\\Autodesk\\MotionBuilder 2014\\"
    subprocess.Popen('{0} -suspendMessages {1}'.format(mobuExePath, filepath), cwd=mbAppFolder)