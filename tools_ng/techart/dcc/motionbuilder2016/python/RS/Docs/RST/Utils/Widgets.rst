Widgets --- :mod:`RS.Utils.Widgets`
***********************************

Consists of useful GUI widgets.

.. automodule:: RS.Utils.Widgets
	:members: RsChoiceDialog, RsVerticalListDialog
