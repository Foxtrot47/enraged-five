Rockstar Menu
=============

This page covers how to work with the Rockstar menu system inside of MotionBuilder.


Menu Builder
------------
The module that builds the menu is :doc:`Utils/MenuBuilder`.


Adding a New Menu Item
----------------------
To add a new menu item, you will need to edit the **menu.config** file.  Either under the root
or a submenu, add an xml line similar to the following: ::

	<menu displayName="Reference Editor" modulePath="RS.UI.ReferenceEditor" />

.. attribute:: displayName
The name that will show up in the menu.

.. attribute:: modulePath
This attribute should contain the import path to the module that will run when the user clicks on the menu item.

.. attribute:: command
This attribute would point to a function defined in the :doc:`UI/Commands` module.

.. attribute:: separator
If set to "True", will create a separator after the menu item.


Creating a Sub Menu
-------------------
You can nest as many menus as you'd like.  To do this, create an xml block as follows: ::

	<submenu displayName="Referencing System">
		...
	</submenu>

.. attribute:: displayName
The display name of the sub-menu item.

.. attribute:: separator
If set to "True", will create a separator after the menu item.