Perforce --- :mod:`RS.Perforce`
*******************************
This module mostly operates through the tools framework RSG.SourceControl.Perforce .NET assembly.

Example usage: ::

  import RS.Perforce
  
  # Query the status of one or more files.  This is running the Perforce 'fstat' command.
  fileStates = RS.Perforce.GetFileState( [ 'c:\\myFile1.txt', 'c:\\myFile2.txt' ] )
  
  # The function GetFileState() returns either a single or list of RSG.SourceControl.Perforce.FileState objects.
  # From that object, you can query information about a file.  For example:
  fileStates[ 0 ].HeadRevision
  fileStates[ 0 ].HaveRevision
  fileStates[ 0 ].DepotFilename
  fileStates[ 0 ].ClientFilename
  
  # See if you have the latest version.
  if RS.Perforce.IsLatest( fileStates[ 0 ] ):
	...
  
  # Check if one of the files is opened for edit.
  if RS.Perforce.IsOpenForEdit( fileStates[ 0 ] ):
	...
	
  # Check if the file can be opened for edit.
  if RS.Perforce.CanOpenForEdit( fileStates[ 0 ] ):
	...
	
  # Check if a file exists in Perforce.
  if RS.Perforce.DoesFileExist( 'c:\\myFile1.txt' ):
	...
	
  # Work with changelists.
  change = RS.Perforce.CreateChangelist( 'My changelist description' )
  
  # Shelve a changelist.
  RS.Perforce.ShelveChangelist( theChangelistNumber )

.. automodule:: RS.Perforce
	:members:
	:undoc-members:
