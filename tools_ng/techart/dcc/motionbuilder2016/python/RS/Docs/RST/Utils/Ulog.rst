Universal Log --- :mod:`RS.Utils.Logging.Universal`
***************************************************
Provides Universal Log capability .

.. note::
	
	This module mostly operates through the .NET assembly **RSG.Base.Logging**.

Example usage: ::

	import RS.Utils.Logging.Universal
	
	ulog = RS.Utils.Logging.Universal.UniversalLog( 'My Log' )
	
	# Log messages.
	ulog.LogMessage( 'Keanu Reeves is the greatest actor in the world.', context = 'He says to himself.' )
	ulog.LogError( 'Somefin' went wrong.' )
	ulog.LogWarning( 'Keanu Reeves is in that film you are about to watch.' )
	
	# See if the log has errors or warnings.
	ulog.HasErrors
	ulog.HasWarnings
	
	# Show the log.
	ulog.Show()
	RS.Utils.Logging.Universal.Show( ulogFilename )

Classes
=======
.. autoclass:: RS.Utils.Logging.Universal.UniversalLog
	:members:
	:undoc-members:
	
Functions
=========
.. autofunction:: RS.Utils.Logging.Universal.Show
