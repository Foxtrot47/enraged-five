Configuration --- :mod:`RS.Config`
**********************************

This module should be used for accessing project paths, user information and environment settings.

.. note::

	This module interfaces with the .NET assembly **RSG.Base.Configuration**.

.. warning::

	Only ever use this module to get configuration settings!  For example, never use os.environ[ 'RS_PROJROOT' ]
	to query the project root.  This module ensures branch safety and future proofing paths.

Project
=======
Accessing project data is done by using: ::

	import RS.Config
	RS.Config.Project

Attributes
----------
================ =================================== ===================================
Type             Access                              Example Output
================ =================================== ===================================
Name             RS.Config.Project.Name              GTA5
================ =================================== ===================================


Paths
-----
================ =================================== ===================================
Type             Access                              Example Output
================ =================================== ===================================
Root             RS.Config.Project.Path.Root         "x:\\gta5"
Art              RS.Config.Project.Path.Art          "x:\\gta5\\art"
Assets           RS.Config.Project.Path.Assets       "x:\\gta5\\assets"
Export           RS.Config.Project.Path.Export       "x:\\gta5\\assets\\export"
Anim             RS.Config.Project.Path.Anim         "x:\\gta5\\art\\anim"
Metadata         RS.Config.Project.Path.Metadata     "x:\\gta5\\assets\\metadata"
================ =================================== ===================================

User
====
Accessing user data is done by using: ::

	import RS.Config
	RS.Config.User

Attributes
----------
================ =================================== ===================================
Type             Access                              Example Output
================ =================================== ===================================
Name             RS.Config.User.Name                 "jhayes"
Studio           RS.Config.User.Studio               "R* San Diego"
Email            RS.Config.User.Email                "jason.hayes@rockstarsandiego.com"
Domain           RS.Config.User.Domain               "rockstar.t2.corp"
Type             RS.Config.User.Type                 "Animator"
Exchange Server  RS.Config.User.ExchangeServer       "smtp-na.rockstar.t2.corp"
Is Outsourcer    RS.Config.User.IsExternal           False
================ =================================== ===================================

Tool
====
Accessing tool data is done by using: ::

	import RS.Config
	RS.Config.Tool

Attributes
----------
================ =================================== ===================================
Type             Access                              Example Output
================ =================================== ===================================
Version          RS.Config.Tool.Version              "[GTA5]_TOOLS_VERSION_14"
================ =================================== ===================================

Paths
-----
================ =================================== ===================================
Type             Access                              Example Output
================ =================================== ===================================
Root             RS.Config.Tool.Path.Root            "x:\\gta5\\tools"
Bin              RS.Config.Tool.Path.Bin             "x:\\gta5\\tools\\bin"
Logs             RS.Config.Tool.Path.Logs            "x:\\gta5\\tools\\logs"
Library          RS.Config.Tool.Path.Library         "x:\\gta5\\tools\\lib"
Drive            RS.Config.Tool.Path.Drive           "x:\\"
================ =================================== ===================================

Script
======
Access various Python script configuration data. ::

	import RS.Config
	RS.Config.Script

Attributes
----------
================ ====================================== ==================================== ================================================================================
Type             Access                                 Example Output                       Description
================ ====================================== ==================================== ================================================================================
Development Mode RS.Config.Script.DevelopmentMode       False                                Flag that lets you know if you are working out of the global depot or project.
Build Version    RS.Config.Script.TargetBuildVersion    06262013-218012 (changelist #218012) Build version of MotionBuilder.
Build Title      RS.Config.Script.TargetBuildTitle      Motionbuilder 2014 - Hotfix 4        Build title of MotionBuilder.
================ ====================================== ==================================== ================================================================================

Paths
-----
=========================== =================================== ==========================================================================
Type                        Access                              Example Output
=========================== =================================== ==========================================================================
Root                        RS.Config.Script.Path.Root          "x:\\gta5\\tools\\dcc\\current\\motionbuilder2014\\python"
Package Root                RS.Config.Script.Path.RockstarRoot  "x:\\gta5\\tools\\dcc\\current\\motionbuilder2014\\python\\RS"
External Package Root       RS.Config.Script.Path.ExternalRoot  "x:\\gta5\\tools\\dcc\\current\\motionbuilder2014\\python\\external"
Setup Config File           RS.Config.Script.Path.SetupConfig   "x:\\gta5\\tools\\dcc\\current\\motionbuilder2014\\python\\setup.config"
Menu Config File            RS.Config.Script.Path.MenuConfig    "x:\\gta5\\tools\\dcc\\current\\motionbuilder2014\\python\\menu.config"
Tool Images Root            RS.Config.Script.Path.ToolImages    "x:\\gta5\\tools\\dcc\\current\\motionbuilder2014\\images"
=========================== =================================== ==========================================================================