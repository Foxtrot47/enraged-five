Custom Exception Handler --- :mod:`RS.Utils.Exception`
******************************************************

When MotionBuilder starts up, we register a custom unhandled exception handler to catch when tools crash.  When they crash,
an email is automatically sent out to the list of people defined in the setup.config file.

If you don't want to have the exceptions emailed, you can temporarily disable the system by doing the following: ::

 	import RS.Utils.Exception
 	RS.Utils.Exception.SEND_EXCEPTION = False

In addition to the email, a window will pop up with useful information about the crash.  This is for cases when an email
couldn't be sent out.


.. automodule:: RS.Utils.Exception
	:members: UnhandledExceptionHandler, ShowExceptionDialog
	:undoc-members: