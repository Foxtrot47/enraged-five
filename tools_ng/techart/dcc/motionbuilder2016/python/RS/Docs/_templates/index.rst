Rockstar - MotionBuilder 2014 Python Tools documentation
========================================================

This documentation is aimed at Technical Artists and Tools Programmers who are developing Python tools
for MotionBuilder.


Introduction
============

.. toctree::
	:maxdepth: 1
	
	Framework
	menu
	
	
.. seealso::

	* `Python Coding Standards <https://devstar.rockstargames.com/wiki/index.php/Python_Coding_Standards>`_
	* `Debugging Python in MotionBuilder <https://devstar.rockstargames.com/wiki/index.php/Debugging_Python_in_MotionBuilder>`_
	* `Remotely Execute Scripts in MotionBuilder  <https://devstar.rockstargames.com/wiki/index.php/Remotely_Execute_Scripts_in_MotionBuilder>`_

	
{TableOfContent}

Tutorials
=========
* Creating a unit test
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

