from PySide import QtCore


class _WorkerSignals(QtCore.QObject):
    finishedSignal = QtCore.Signal()


class Worker(QtCore.QRunnable):
    """Worker Class"""
    _PENDING = 0
    _PROCESSING = 1
    _FINISHED = 2
    _ERRORED = 3
    
    _THREAD_POOL = QtCore.QThreadPool.globalInstance()

    def __init__(self, func, *args, **kwargs):
        """Constructor
         
        args:
            func (method): The method to run
            *args + **kwargs: Arguments to be passed into the function to be run 
        """
        super(Worker, self).__init__()
        self.setAutoDelete(True)
        self._signals = _WorkerSignals()
        self._state = self._PENDING
        self._func = func
        self._args = args
        self._kwargs = kwargs
        self._error = None
        self._return = None

    @classmethod
    def submit(cls, function, *args, **kwargs):
        """Static Method
         
        Submit the function to be run on the global threading pool
         
        Args:
            function (method): The method to run
            *args + **kwargs: Arguments to be passed into the function to be run 
        """
        newWorker = Worker(function, *args, **kwargs)
        cls._THREAD_POOL.start(newWorker)
        return newWorker
 
    def run(self):
        """Re-Implemented Method

        The run logic of the QRunnable, call the function and store the output
        """
        self._state = self._PROCESSING
        
        try:
            self._return = self._func(*self._args, **self._kwargs)
            self._state = self._FINISHED
        except Exception as error:
            self._error = error
            self._state = self._ERRORED
        
        self._signals.finishedSignal.emit()
        
        self._signals.deleteLater()
        self._signals = None
     
    def result(self):
        """Get the return value of the function, waiting if it hasnt finished yet"""
        if self._state in [self._PENDING, self._PROCESSING]:
            self._wait()
        if self._state == self._ERRORED:
            self._handleRaise()
        return self._return
    
    def _handleRaise(self):
        # Maybe Wrap this so its clear to what caused it and what the locals/gloabs were at the time
        raise self._error
        
    def _wait(self):
        """Hold the main thread until the process has finished"""
        if self._state == self._FINISHED:
            return
        loop = QtCore.QEventLoop()
        self._signals.finishedSignal.connect(loop.quit)
        loop.exec_()
