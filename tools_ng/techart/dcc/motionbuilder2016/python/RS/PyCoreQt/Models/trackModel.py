import re

from PySide import QtCore

from RS.PyCoreQt import const
from RS.PyCoreQt.Models import dragDropModel, baseModelItem


class TrackModelItem(baseModelItem.BaseModelItem):
    """Base TrackModel Item

    This is a very simple model item that has two columns for a name and a timeline range.
    """

    def __init__(self, name, timelineRange, children=None, parent=None):
        """Initializer

        Args:
            name (str): Name of item
            timelineRange (list of 2 int): Timeline range
            children (list of str): Child items to add
            parent (QtWidgets.QWidget): Parent object
        """
        super(TrackModelItem, self).__init__(parent)
        self.timelineRange = timelineRange
        self.name = name
        self.children = children if children else []

    def columnCount(self, parent=None):
        """Reimplemented from QAbstractItemModel interface

        Args:
            parent (QtCore.QModelIndex): Model index used in method

        Returns:
            int: Number of columns
        """
        return 2

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface

        Args:
            index (QtCore.QModelIndex): Index to get data from
            value (QtCore.QVariant): Value to set
            role (QtCore.Qt.Role): Role to get data from
        """
        return True

    def data(self, modelIndex, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface

        Args:
            index (QtCore.QModelIndex): Index to get data from
            role (QtCore.Qt.Role): Role to get data from

        Returns:
            QtCore.QVariant: Value of data based on index
        """
        if not modelIndex.isValid():
            return None

        if role == QtCore.Qt.DisplayRole:
            if modelIndex.column() == const.TimelineColumns.NAME_COLUMN:
                return self.name

            elif modelIndex.column() == const.TimelineColumns.FRAME_RANGE_COLUMN:
                return self.timelineRange

    def setRange(self, timelineRange):
        """Sets timeline range of item

        Args:
            timelineRange (list of 2 int): Start and end frame range
        """
        self.timelineRange = timelineRange

    def getRange(self):
        """Gets timeline range

        Returns:
            list of 2 int: Start and end frame range
        """
        return self.timelineRange

    def getName(self):
        """Gets name of item

        Returns:
            str: Name of item
        """
        return self.name

    def getIndex(self):
        """Gets index by number in name

        Returns:
            int: Index of item
        """
        return int(re.findall(r"\d+", self.name)[-1])

    def setChildren(self, children):
        """Sets children

        Args:
            children (list of str): Child names to add
        """
        self.children = children

    def getChildren(self):
        """Gets children

        Returns:
            list of str: Names of children
        """
        return self.children


class TrackModel(dragDropModel.DragDropModel):
    """Track model to store track model items

    This is a default model to use when building the timeline widget. It stores name and timeline ranges for tracks.
    Generally this should be inherited and edited as needed if using custom data. The getModelIndexByRow method
    is required by the timeline widget so any custom model will need that method.

    Signals:
        modelRefresh: Emitted when model is updated/refreshed

    Attributes:
        timelineRange (list of 2 int): Frame range
    """

    modelRefreshed = QtCore.Signal()
    HEADERS = ["Name", "Range"]

    def __init__(self, timelineRange, parent=None):
        """Initializer

        Args:
            timelineRange (list of 2 int): Start and end frame range
            parent (QtWidgets.QWidget): Parent object
        """
        super(TrackModel, self).__init__(parent)
        self.timelineRange = timelineRange

    def setupModelData(self, rootItem):
        """Sets up the model data"""
        self.refreshModel()

    def addTrack(self):
        """Adds a track"""
        rowCount = self.rootItem.rowCount()

        index = 0
        if rowCount > 0:
            allIndices = set([self.rootItem.childItems[idx].getIndex() for idx in xrange(0, rowCount)])
            allPossibleIndices = set(range(0, max(allIndices) + 1))
            missingIndexes = allPossibleIndices.difference(allIndices)
            if missingIndexes:
                index = sorted(list(missingIndexes))[0]
            else:
                index = sorted(list(allIndices))[-1] + 1

        trackItem = TrackModelItem(
            "_".join(["Track", str(index)]),
            self.timelineRange,
            children=["_".join(["Child", str(index)])]
        )

        # insert item
        self.beginInsertRows(QtCore.QModelIndex(), rowCount, rowCount + 1)
        self.rootItem.appendChild(trackItem)
        self.endInsertRows()

        self.refreshModel()

    def removeTrack(self, modelIndexList):
        """Removes tracks

        Args:
            modelIndexList (list of QtCore.QModelIndex): Model indexes to remove
        """
        # Items are row-based so filter out additional column indexes to avoid attempting to delete the same item twice
        modelIndexList = [idx for idx in modelIndexList if idx.isValid() and idx.column() == 0]
        # Delete in reverse row order to avoid an access violation crash where the view reads from a deleted item
        modelIndexList.sort(key=lambda modelIdx: modelIdx.row(), reverse=True)

        for modelIndex in modelIndexList:
            rowIndex = modelIndex.row()
            item = modelIndex.internalPointer()

            self.beginRemoveRows(QtCore.QModelIndex(), rowIndex, rowIndex)
            self.rootItem.removeChild(item)
            self.endRemoveRows()

        self.refreshModel()

    def splitTrack(self, modelIndexList, frame):
        """Split track by frame

        Args:
            modelIndexList (list of QtCore.QModelIndex): Model indexes to remove
            frame (int): Frame number to split by
        """
        # Items are row-based so filter out additional column indexes to avoid attempting to delete the same item twice
        modelIndexList = [idx for idx in modelIndexList if idx.isValid() and idx.column() == 0]
        # Delete in reverse row order to avoid an access violation crash where the view reads from a deleted item
        modelIndexList.sort(key=lambda modelIdx: modelIdx.row(), reverse=True)

        for modelIndex in modelIndexList:
            item = modelIndex.internalPointer()

            trackItem = TrackModelItem(item.getName(), self.timelineRange, children=item.getChildren())
            insertRow = modelIndex.row() + 1

            self._splitRanges(trackItem, frame=frame)

            self.beginInsertRows(QtCore.QModelIndex(), insertRow, insertRow)
            self.rootItem.appendChild(trackItem)
            self.endInsertRows()

        self.refreshModel()

    def _splitRanges(self, item, frame=0):
        """Splits all time frame ranges based on current frame

        Args:
            item (TrackModelItem): The item to split
            frame (int): frame to split at
        """
        name = item.getName()

        # get scene frame range
        startSceneFrame, endSceneFrame = self.timelineRange
        startRange, endRange = startSceneFrame, endSceneFrame

        allItems = sorted(self.rootItem.childItems)

        # init loop variables
        lastFrameRange = allItems[0].getRange()

        firstIndex = lastIndex = None
        for index, splitPart in enumerate(allItems):
            if splitPart.getName() == name:
                if not firstIndex:
                    firstIndex = index
                lastIndex = index

        foundSplitRange = False
        for index, splitPart in enumerate(allItems):

            # if not same name skip
            if not splitPart.getName() == name:
                continue

            # get frame range of current split
            startFrame, endFrame = splitPart.getRange()

            # check if in range
            if endFrame >= frame >= startFrame:
                if startFrame == frame:
                    startRange, endRange = startFrame, frame
                    splitPart.setRange([startFrame + 1, endFrame])
                elif endFrame == frame:
                    startRange, endRange = endFrame, frame
                    splitPart.setRange([startFrame, frame - 1])
                else:
                    startRange, endRange = frame, endFrame
                    splitPart.setRange([startFrame, frame - 1])
                foundSplitRange = True

            # check if in-between range
            elif lastFrameRange[1] < frame < startFrame:
                startRange, endRange = lastFrameRange[1] + 1, startFrame - 1
                foundSplitRange = True

            # check if after range
            elif index == lastIndex and frame > endFrame:
                startRange, endRange = endFrame + 1, endSceneFrame
                foundSplitRange = True

            # check if before range
            elif index == firstIndex and frame < startFrame:
                startRange, endRange = startSceneFrame, startFrame - 1
                foundSplitRange = True

            if foundSplitRange:
                break

            lastFrameRange = [startFrame, endFrame]

        # set range to new split values
        item.setRange([startRange, endRange])

    def mergeTracks(self, modelIndexList, verticalMerge=False):
        """Merges tracks together

        Args:
            modelIndexList (list of QtCore.QModelIndex): Model indexes to merge together
            verticalMerge (bool): If True will only merge vertical splits
        """
        # Items are row-based so filter out additional column indexes to avoid attempting to delete the same item twice
        modelIndexList = [idx for idx in modelIndexList if idx.isValid() and idx.column() == 0]

        if verticalMerge:
            # filter to vertical splits
            modelIndexList = [idx for idx in modelIndexList if self.isVerticalSplit(idx)]

        if len(modelIndexList) < 2:
            return

        # grab parent to merge into
        parentModelIndex = modelIndexList.pop()
        parentItem = parentModelIndex.internalPointer()

        if not verticalMerge:
            # add all vertical splits contained in a horizontal into index
            modelIndexList = self.getAllSplits(modelIndexList, excludeIndexes=[parentModelIndex])

        # get model items and get range to merge
        childItems = [modelIndex.internalPointer() for modelIndex in modelIndexList]
        allRanges = [childItem.getRange() for childItem in childItems]
        allRanges.append(parentItem.getRange())

        # set parent range
        startFrames, endFrames = zip(*allRanges)
        frameRange = [min(startFrames), max(endFrames)]
        parentItem.setRange(frameRange)

        # get all the children, use set to remove duplicates
        allChildren = set()
        for childItem in childItems:
            for child in childItem.getChildren():
                allChildren.add(child)
        for parentChild in parentItem.getChildren():
            allChildren.add(parentChild)

        # set children merge
        parentItem.setChildren(list(allChildren))

        # remove this items make sure it's reverse
        self.removeTrack(modelIndexList)

    def refreshModel(self):
        """Emit modelRefreshed signal"""
        self.modelRefreshed.emit()

    def getAllSplits(self, modelIndexList, excludeIndexes=None):
        """Get all vertical splits related to any model index in list

        Args:
            modelIndexList (list of QtCore.QModelIndex): ModelIndexes to search
            excludeIndexes (list of QtCore.QModelIndex): Items to exclude

        Returns:
            list of QtCore.QModelIndex: Extended ModelIndex list with any splits found
        """
        excludeIndexes = excludeIndexes if excludeIndexes is not None else []

        for modelIndex in modelIndexList:
            splitItem = modelIndex.internalPointer()
            for idx, item in enumerate(self.rootItem.childItems):
                if item.getName() != splitItem.getName() or splitItem == item:
                    continue

                foundModelIndex = self.getModelIndexByRow(idx)
                if foundModelIndex not in modelIndexList and not any(idx == index.row() for index in excludeIndexes):
                    modelIndexList.append(foundModelIndex)

        return modelIndexList

    def isVerticalSplit(self, modelIndex):
        """Checks if modelIndex is a vertical split item

        Args:
            modelIndex (QtCore.QModelIndex): ModelIndex to check

        Returns:
            bool: True or False if is vertical split
        """
        splitItem = modelIndex.internalPointer()
        splitName = splitItem.getName()

        for item in self.rootItem.childItems:
            if item is not splitItem and item.getName() == splitName:
                return True

        return False

    def getModelIndexByRow(self, rowIndex, columnIndex=0, childIndex=None):
        """Get first column modelIndex by row

        Args:
            rowIndex (int): Row to get
            columnIndex (int): Column to get
            childIndex (int): Child of row to get model index of
        """
        childItem = self.rootItem.childItems[rowIndex]
        modelIndex = self.createIndex(rowIndex, columnIndex, childItem)

        if childIndex is not None:
            return self.index(childIndex, columnIndex, modelIndex)

        return modelIndex

    def headerData(self, section, orientation=QtCore.Qt.Vertical, role=QtCore.Qt.DisplayRole):
        """Reimplemented from QAbstractItemModel interface

        Args:
            section (int): Section row
            orientation (QtCore.Qt.Alignment):
            role (QtCore.Qt.Role): Role get header
        """
        if role == QtCore.Qt.DisplayRole and section < len(self.HEADERS):
            return self.HEADERS[section]

    def columnCount(self, parent=None):
        """Reimplemented from QAbstractItemModel interface.

        Args:
            parent (QtWidgets.QWidget): Parent item
        """
        return 2

    def setData(self, modelIndex, value, role=QtCore.Qt.EditRole):
        """Sets data

        Args:
            modelIndex (QtCore.QModelIndex): Model index to set
            value (QtCore.QVariant): Value to set
            role (QtCore.Qt.Role): Role to set value

        Returns:
            bool: True or False if succesfully set data
        """
        if not modelIndex.isValid():
            return False

        if role == QtCore.Qt.EditRole:
            if modelIndex.column() == const.TimelineColumns.FRAME_RANGE_COLUMN:
                item = modelIndex.internalPointer()
                item.setRange(value)
                return True

        return super(TrackModel, self).setData(modelIndex, value, role)

    def moveItem(self, index, row, parentIndex, mimeType):
        """Move item method needed to be overridden in abstract model

        Args:
            index (QtGui.QModelIndex): Index to the item that is being dropped
            row (int): Row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): Index of the new parent for the index being dropped
        """
        pass

    def dropItem(self, index, row, parentIndex, mimeType):
        """Drop item method needed to be overridden

        Args:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped
        """
        pass

    def copyItem(self, index, row, parentIndex, mimeType):
        """Copy item method needed to be overridden

        Args:
            index (QtGui.QModelIndex): index to the item that is being dropped
            row (int): row on that the item is being dropped on
            parentIndex (QtGui.QModelIndex): index of the new parent for the index being dropped
        """
        pass
