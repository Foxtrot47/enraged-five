from PySide import QtCore, QtGui

from RS.Core.ComponentSelection import common


class ProxySelectionModel(QtGui.QItemSelectionModel):
    '''
        This class provides a proxy selection model to be shared between different views using the same sourceModel.
        To use it you need a main sourceModel and sharedSelectionModel mapped to it.
        Then instantiate a ProxySelectionModel for each proxyModel you create for each view, using it as a model input
        and the sharedSelectionModel as sharedModel.
        Finally set the proxySelectionModel as the view selectionModel.
    '''
    def __init__(self, model, sharedModel):
        '''
        Args:
            model (QAbstractProxyModel): the base model.
            sharedModel (QItemSelectionModel): the shared selection model.
        '''
        if not isinstance(model, QtGui.QAbstractProxyModel):
            raise TypeError("Proxy Selection Model parent model must be an instance of QAbstractProxyModel.")
        super(ProxySelectionModel, self).__init__(model)
        self.sharedModel = sharedModel
        self.sharedModel.selectionChanged.connect(self._proxySelectionChangedSignal)

    def select(self, selection, command):
        '''
            Maps selection from the proxySelectionModel on the sourceModel, then performs a selection on the
            sharedSelectionModel.
            Checks if the selection is QModelIndex[] or QItemSelection and uses correct function.

            Args:
                selection (QModelIndex or QItemSelection): the selection received from sender.
                command (QItemSelectionModel::SelectionFlags): the selection command.

            Returns:
                function: SelectionModel.select
        '''
        if isinstance(selection, QtCore.QModelIndex):
            mappedIndex = self.model().mapToSource(selection)
            modelName = mappedIndex.data(QtCore.Qt.ToolTipRole)
            common.selectModel(modelName)
            return self.sharedModel.select(mappedIndex, command)
        elif isinstance(selection, QtGui.QItemSelection):
            mappedSelection = self.model().mapSelectionToSource(selection)
            selectionIndexes = mappedSelection.indexes()
            for index in selectionIndexes:
                modelName = index.data(QtCore.Qt.ToolTipRole)
                common.selectModel(modelName)
            return self.sharedModel.select(mappedSelection, command)

    def _proxySelectionChangedSignal(self, selection):
        '''
            Remaps the selection back to the proxy one so we can update the view and actually show what's selected.
        '''
        proxyMappedSelection = self.model().mapSelectionFromSource(selection)
        proxyMappedSelectionIndexes = proxyMappedSelection.indexes()
        super(ProxySelectionModel, self).select(proxyMappedSelection, self.ClearAndSelect)

        for index in proxyMappedSelectionIndexes:
            modelName = index.data(QtCore.Qt.ToolTipRole)
            common.selectModel(modelName)
