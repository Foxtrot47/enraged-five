import random
import operator
import itertools

from PySide import QtCore


class BaseTrackItem(QtCore.QAbstractItemModel):
    """Base Track item"""
    def __init__(self, parent, startFrame, endFrame):
        super(BaseTrackItem, self).__init__(parent)

        self._startFrame = startFrame
        self._endFrame = endFrame

    @property
    def fullTimelineRange(self):
        return self.parent().fullTimelineRange()

    @property
    def fullStartFrame(self):
        """Grab parent fullEndFrame"""
        return self.parent().fullStartFrame

    @property
    def fullEndFrame(self):
        """Grab parent fullEndFrame"""
        return self.parent().fullEndFrame

    @property
    def fullDuration(self):
        return self.parent().fullDuration

    @property
    def duration(self):
        return self._endFrame - self._startFrame

    @property
    def startFrame(self):
        return self._startFrame

    @startFrame.setter
    def startFrame(self, startFrame):
        self._startFrame = int(max(min(startFrame, self._endFrame - 1), self.fullStartFrame))
        self.parent().startFrame = self._startFrame
        self.parent().updateGapTracks()

    @property
    def endFrame(self):
        return self._endFrame

    @endFrame.setter
    def endFrame(self, endFrame):
        self._endFrame = int(min(max(endFrame, self._startFrame + 1), self.fullEndFrame))
        self.parent().endFrame = self._endFrame
        self.parent().updateGapTracks()

    @property
    def color(self):
        return self.parent().color


class ClipTrackItem(BaseTrackItem):
    """Clip item"""


class GapTrackItem(BaseTrackItem):
    """Gap item"""


class TrackDataItem(QtCore.QAbstractItemModel):

    def __init__(self, timelineRange, fullTimelineRange, trackTitle=None, trackTitleChildren=None, modelIndex=None):
        """Track class that holds data on a track

        Args:
            timelineRange (list of 2 int): Start and end frame track
            fullTimelineRange (list of 2 int): Start and end frame of full track
            trackTitle (str): Title of track
            trackTitleChildren (list of str): Subtitle items that collapse under main title of track
            model (PySide2.QAbstractItemModel): Data model to update
        """
        super(TrackDataItem, self).__init__()

        self._startFrame, self._endFrame = timelineRange
        self._fullStartFrame, self._fullEndFrame = fullTimelineRange
        self._clipTracks = ClipTrackItem(self, self._startFrame, self._endFrame)
        self._gapTracks = []
        self._trackTitle = trackTitle
        self._trackTitleChildren = trackTitleChildren if trackTitleChildren else []
        self._modelIndex = modelIndex

        self._addGapTracks()

    @property
    def color(self):
        """Get a unique color based on title

        Returns:
            tuple of 4: RGBA colors
        """
        # set seed based on name
        random.seed(self._trackTitle)
        return random.randint(100, 255), random.randint(100, 255), random.randint(100, 255), 255

    @property
    def duration(self):
        """Duration of clip"""
        return self._endFrame - self._startFrame

    @property
    def fullDuration(self):
        """Duration of timeline"""
        return self._fullEndFrame - self._fullStartFrame

    @property
    def startFrame(self):
        return self._startFrame

    @startFrame.setter
    def startFrame(self, startFrame):
        self._startFrame = int(max(self.fullStartFrame, min(startFrame, self._endFrame - 1)))
        self._modelIndex.model().setData(self._modelIndex, [self._startFrame, self._endFrame])
        self.updateGapTracks()

    @property
    def endFrame(self):
        return self._endFrame

    @endFrame.setter
    def endFrame(self, endFrame):
        self._endFrame = int(min(self.fullEndFrame, max(endFrame, self._startFrame + 1)))
        self._modelIndex.model().setData(self._modelIndex, [self._startFrame, self._endFrame])
        self.updateGapTracks()

    @property
    def title(self):
        """Title of track"""
        return self._trackTitle

    @title.setter
    def title(self, title):
        self._trackTitle = title

    @property
    def titleChildren(self):
        """Title of track"""
        return self._trackTitleChildren

    @titleChildren.setter
    def titleChildren(self, titleChildren):
        self._trackTitleChildren = list(set(titleChildren))

    def addChildTitle(self, titleChild):
        """Add a child title

        Args:
            titleChild (str): Text to add
        """
        if titleChild not in self._trackTitleChildren:
            model = self._modelIndex.model()
            if hasattr(model, "addSplitResource"):
                modelIndex = model.getModelIndexByRow(self._modelIndex.row())
                model.addSplitResource(modelIndex, titleChild)
            else:
                self._trackTitleChildren.append(titleChild)

    @property
    def fullStartFrame(self):
        return self._fullStartFrame

    @fullStartFrame.setter
    def fullStartFrame(self, fullStartFrame):
        self._fullStartFrame = fullStartFrame

    @property
    def fullEndFrame(self):
        return self._fullEndFrame

    @fullEndFrame.setter
    def fullEndFrame(self, fullEndFrame):
        self._fullEndFrame = fullEndFrame

    def timelineRange(self):
        """Full timeline range of track"""
        return self._startFrame, self._endFrame

    def fullTimelineRange(self):
        """Full timeline range of track"""
        return self._fullStartFrame, self._fullEndFrame

    @property
    def tracks(self):
        """Tracks property"""
        return [self._clipTracks] + self._gapTracks

    @property
    def clipTracks(self):
        """Clip tracks property"""
        return [self._clipTracks]

    @property
    def gapTracks(self):
        """Gap tracks property"""
        return self._gapTracks

    @property
    def gapNumber(self):
        """Gap tracks property"""
        return len(self._gapTracks)

    def getGapFrames(self):
        """Get gap frames from current frames set"""
        gapRanges = []
        sceneFrames = set(xrange(self._fullStartFrame, self._fullEndFrame + 1))
        splitFrameRange = sorted(set(xrange(self._startFrame, self._endFrame + 1)))

        gapFrames = sceneFrames.difference(splitFrameRange)

        # group missing sequential ranges
        for key, group in itertools.groupby(enumerate(gapFrames), lambda frameRange: frameRange[0] - frameRange[1]):
            ranges = (list(map(operator.itemgetter(1), group)))
            gapRanges.append([max(ranges[0] - 1, self.fullStartFrame), ranges[-1] + 1])

        return gapRanges

    def updateGapTracks(self):
        """Updates the gap ranges"""
        gapRanges = self.getGapFrames()

        if len(gapRanges) != len(self._gapTracks):
            self._gapTracks = []
            self._addGapTracks()
            return

        for idx, gapRange in enumerate(self.getGapFrames()):
            self._gapTracks[idx]._startFrame = gapRange[0]
            self._gapTracks[idx]._endFrame = gapRange[1]

    def _addGapTracks(self):
        """Adds gap items to track"""

        for gapRange in self.getGapFrames():
            self._gapTracks.append(GapTrackItem(self, gapRange[0], gapRange[1]))


class TimelineDataItem(QtCore.QAbstractItemModel):
    """Timeline object with data for tracks"""

    def __init__(self, parent=None, tracks=None):
        """Timeline class that holds data on a tracks in a timeline

        Args:
            parent (QtCore.QObject): Parent object
            tracks (list of TrackData): Tracks to store data
        """
        super(TimelineDataItem, self).__init__(parent)
        self._tracks = [track for track in tracks if isinstance(track, TrackDataItem)] if tracks else []

    @property
    def tracks(self):
        """Tracks property"""
        return self._tracks

    @tracks.setter
    def tracks(self, tracks):
        self._tracks = [track for track in tracks if isinstance(track, TrackDataItem)]
