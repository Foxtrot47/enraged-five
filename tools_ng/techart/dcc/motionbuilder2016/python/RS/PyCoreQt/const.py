
# Timeline zoom level
CURRENT_ZOOM_LEVEL = 1.0
MIMEDATA_DELIMITER = ";"


class TimelineValues(object):
    """Values for timeline widget"""
    MARKER_SIZE = 10
    TIME_SLIDER_HEIGHT = 20
    TRACK_HEIGHT = 35
    TRACK_NAME_WIDGET_WIDTH = 250.0
    TIME_MULTIPLIER = 25
    HIGHLIGHT_WIDTH = 2
    LABEL_MARGIN = 15
    TOP_MARGIN = 60
    RULER_SIZE = 20
    FONT = "MS Shell Dlg 2"
    MAX_NAME_LENGTH = 40

class TimelineColumns(object):
    NAME_COLUMN = 0
    FRAME_RANGE_COLUMN = 1
