import re

from PySide import QtCore

from RS import Globals


class QLogObject(QtCore.QObject):

    def __init__(self, log=None):
        super(QLogObject, self).__init__()
        self._log = log or Globals.Ulog

    def disconnect(self, *args, **kwargs):
        """
        Disconnect callback
        """
        value = super(QLogObject, self).disconnect(*args, **kwargs)

        signalArg = args[1]
        if isinstance(signalArg, str):
            signalName = re.search("\d+(?P<signalArg>[a-zA-Z0-9]+)\(", signalArg).group(1)
        else:
            signalName = "Signal not found. May be stored in a different arg!"

        if value is False:
            self._log.LogError("Unable to disconnect signal '{}'".format(signalName))

        return True
