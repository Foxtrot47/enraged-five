import math

from PySide import QtGui, QtCore

from RS.Utils.Scene import Time
from RS.Tools.UI import Application
from RS.PyCoreQt import const
from RS.PyCoreQt.Widgets import trackWidgets


class TimeBox(QtGui.QGraphicsRectItem):
    """Timeline box rectangle"""

    def __init__(self, parent=None):

        super(TimeBox, self).__init__(parent)
        self.setBrush(QtGui.QBrush(QtGui.QColor(71, 71, 71, 255)))
        self.setPen(QtCore.Qt.NoPen)

    def paint(self, painter, option, widget):
        """Overridden method to draw lines

        Args:
            painter (QtGui.QPainter): Painter object
            option (QtGui.QStyleOptionGraphicsItem): Parameters used to draw item
            widget (QWidgets.QWidget): Widget to update
        """
        # draw rect first
        super(TimeBox, self).paint(painter, option, widget)

        topLeft = option.rect.topLeft()
        topRight = option.rect.topRight()
        bottomLeft = option.rect.bottomLeft()
        bottomRight = option.rect.bottomRight()
        center = option.rect.center()

        # draw emboss look
        pen = QtGui.QPen(QtGui.QColor(193, 193, 193, 255))
        pen.setWidth(2)
        painter.setPen(pen)
        painter.drawLine(topLeft.x(), topLeft.y(), topRight.x(), topRight.y())
        painter.drawLine(topLeft.x(), topLeft.y(), bottomLeft.x(), bottomLeft.y())
        pen.setColor(QtGui.QColor(43, 43, 43, 255))
        painter.setPen(pen)
        painter.drawLine(bottomLeft.x(), bottomLeft.y(), bottomRight.x(), bottomRight.y())
        painter.drawLine(bottomRight.x(), bottomRight.y(), topRight.x(), topRight.y())

        # draw green line over
        pen.setColor(QtGui.QColor(73, 210, 100, 255))
        pen.setWidth(3)
        painter.setPen(pen)
        painter.drawLine(center.x(), topLeft.y() + 2, center.x(), bottomLeft.y() - 3)


class Ruler(QtGui.QGraphicsPolygonItem):
    """Ruler Graphics Item that represents a rectangle and a green line as the ruler

    This ruler item is to be used together with the trackWidgets.TimeSlider. The setParentItem method of this class
    has been overridden to add the ruler to the TimeSlider for update/callback purposes. The green ruler line will be
    drawn to fit the height passed and const.TimelineValues.TOP_MARGIN is used for the starting height position.
    The timeBox height is set to const.TimelineValues.RULER_SIZE. This ruler requires a QGraphicsScene and QGraphicsView
    in order to be used.

    An example of usage of this can be found in the main section at the bottom of this script.

    Attributes:
        timeBox (TimeBox): A QGraphicsRectItem that moves along the timeslider
    """

    def __init__(self, height, view, *args, **kwargs):
        """Initializer

        Args:
            height (float): Height of ruler
            view (QtGraphicsView): View to update
        """
        self._view = view

        # draw a green line for ruler, this is a polygon incase we change the shape down the road
        poly = QtGui.QPolygonF()
        poly.append(QtCore.QPointF(0, -0.5 * const.TimelineValues.RULER_SIZE + const.TimelineValues.TOP_MARGIN))
        poly.append(QtCore.QPointF(0, height - (0.5 * const.TimelineValues.RULER_SIZE)))

        super(Ruler, self).__init__(poly, *args, **kwargs)

        # set green line for ruler
        pen = QtGui.QPen(QtGui.QColor(73, 210, 100, 255))
        pen.setWidth(3)
        self.setPen(pen)

        # add timebox
        self.timeBox = TimeBox(self)
        self.snapBottom(height)

        self.setY(const.TimelineValues.TIME_SLIDER_HEIGHT - const.TimelineValues.MARKER_SIZE)
        self.setAcceptHoverEvents(True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsFocusable, True)

    def snapBottom(self, height):
        """Set height of ruler

        Args:
            height (float): Height value
        """
        self.timeBox.setRect(
            -const.TimelineValues.RULER_SIZE,
            height - (0.5 * const.TimelineValues.RULER_SIZE) - const.TimelineValues.RULER_SIZE,
            const.TimelineValues.RULER_SIZE * 2,
            const.TimelineValues.RULER_SIZE,
        )
        poly = self.polygon()
        poly[1] = QtCore.QPointF(0, height - (0.5 * const.TimelineValues.RULER_SIZE))
        self.setPolygon(poly)

    def paint(self, painter, option, widget):
        """Overridden method to remove outline

         Args:
             painter (QtGui.QPainter): Painter object
             option (QtGui.QStyleOptionGraphicsItem): Parameters used to draw item
             widget (QWidgets.QWidget): Widget to update
         """
        # remove outline when selected
        option.state = QtGui.QStyle.State_None
        super(Ruler, self).paint(painter, option, widget)

    def mouseMoveEvent(self, mouseEvent):
        """Mouse event

        Args:
            moveEvent (QtCore.QEvent): Mouse press event
        """
        pos = self.mapToScene(mouseEvent.pos())

        scene = self._view.scene()

        if hasattr(scene, "_timeSlider"):

            boundedX = min(
                scene._timeSlider.rect().right(),
                max(pos.x() - const.TimelineValues.TRACK_NAME_WIDGET_WIDTH * const.CURRENT_ZOOM_LEVEL, 0),
            )
            xPosition = int(
                math.ceil(boundedX / const.TimelineValues.TIME_MULTIPLIER) * const.TimelineValues.TIME_MULTIPLIER
            )

            self.setPos(
                QtCore.QPointF(xPosition, const.TimelineValues.TIME_SLIDER_HEIGHT - const.TimelineValues.MARKER_SIZE)
            )
            self.updateFrame(xPosition / const.TimelineValues.TIME_MULTIPLIER)

    def mouseReleaseEvent(self, mouseEvent):
        """Mouse release event

        Args:
            moveEvent (QtCore.QEvent): Mouse press event
        """
        self.setSelected(False)
        super(Ruler, self).mouseReleaseEvent(mouseEvent)

    def hoverEnterEvent(self, hoverEvent):
        """Mouse hover enter event

        Args:
            hoverEvent (QtCore.QEvent): Mouse press event
        """
        self.setSelected(True)
        super(Ruler, self).hoverEnterEvent(hoverEvent)

    def hoverLeaveEvent(self, hoverEvent):
        """Mouse hover leave event

        Args:
            hoverEvent (QtCore.QEvent): Mouse press event
        """
        self.setSelected(False)
        super(Ruler, self).hoverLeaveEvent(hoverEvent)

    def counteractZoom(self, zoomLevel=1.0):
        """Set zoom level

        Args:
            zoomLevel (float): Zoom level to set
        """
        # inverse scale
        self.setTransform(QtGui.QTransform.fromScale(zoomLevel, 1.0))

    def updateFrame(self, frame):
        """Update frame

        Args:
            frame (int): Frame to set
        """
        if hasattr(self._view, "frameChanged"):
            self._view.frameChanged.emit(frame)
            self._view.currentFrame = frame

    def setParentItem(self, timeSlider):
        """Set parent item so ruler is added

        Args:
            timeSlider (trackWidgets.TimeSlider): TimeSlider item
        """
        if isinstance(timeSlider, trackWidgets.TimeSlider):
            timeSlider.addRuler(self)

        super(Ruler, self).setParentItem(timeSlider)


# Example of usage
if __name__ == "__builtin__":

    # Get rid of offset area we don't need but is used in full timeline setup,
    const.TimelineValues.TRACK_NAME_WIDGET_WIDTH = 0
    const.TimelineValues.TOP_MARGIN = 0

    # get frame range to set
    timelineRange = [0, 100]
    duration = timelineRange[1] - timelineRange[0]
    # scene width is calculated by frame range and TIME_MULTIPLIER constant
    sceneWidth = (duration * const.TimelineValues.TIME_MULTIPLIER)

    # make a dialog
    windowWidth, windowHeight = 800.0, 100.0
    dialog = QtGui.QDialog(parent=Application.GetMainWindow())
    dialog.resize(windowWidth, windowHeight)
    dialog.setContentsMargins(0, 0, 0, 0)
    dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
    dialog.setWindowTitle("Test Ruler")

    # make a graphics/scene view
    view = QtGui.QGraphicsView()
    view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    scene = QtGui.QGraphicsScene()
    # set a scene size
    scene.setSceneRect(0, 0, sceneWidth, windowHeight)
    view.setScene(scene)

    # make a layout
    layout = QtGui.QVBoxLayout()
    layout.setContentsMargins(0, 0, 0, 0)
    layout.addWidget(view)
    dialog.setLayout(layout)

    # add timeslider
    sceneRect = scene.sceneRect()
    height = scene.height()
    # make scene rectangle heigh of the time slider and move it to the bottom of the view
    sceneRect.setHeight(const.TimelineValues.TIME_SLIDER_HEIGHT)
    sceneRect.moveTop(height - const.TimelineValues.TIME_SLIDER_HEIGHT)
    timeSlider = trackWidgets.TimeSlider(sceneRect, timelineRange)
    scene.addItem(timeSlider)

    # add ruler, set parent to timeslider
    ruler = Ruler(height, view)
    ruler.setParentItem(timeSlider)

    # How we calculate the zoom for the view. We only want this horizontally which is why we don't use fitInView
    zoomAmount = windowWidth / sceneWidth
    view.scale(zoomAmount, 1)
    zoomLevel = 1.0 / view.matrix().m11()
    const.CURRENT_ZOOM_LEVEL = zoomLevel

    # counteract zoom
    timeSlider.counteractZoom(zoomLevel)
    ruler.counteractZoom(zoomLevel)

    # show dialog
    dialog.show()
