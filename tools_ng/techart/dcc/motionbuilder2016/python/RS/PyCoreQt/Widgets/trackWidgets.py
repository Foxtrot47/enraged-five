import math

from PySide import QtCore, QtGui

from RS.Utils.Scene import Time
from RS.PyCoreQt import const
from RS.PyCoreQt.Models import trackModelItem, trackModel
from RS.Tools.SceneSplitEditor import const as uiConst
from RS.Tools.UI import Application


class FrameNumber(QtGui.QGraphicsRectItem):
    """Frame Number QGraphicsRectItem that represents a rectangle with a simple text item

    Attributes:
        frameNumber (QtGui.QGraphicsSimpleTextItem): Text graphics item
    """

    def __init__(self, text, *args, **kwargs):
        """Initializer

        Args:
            text (str): Text to display
        """
        super(FrameNumber, self).__init__(*args, **kwargs)

        # create frame
        self.frameNumber = QtGui.QGraphicsSimpleTextItem(self)

        # set font
        self.font = QtGui.QFont(const.TimelineValues.FONT)
        self.font.setPointSize(10)
        self.frameNumber.setFont(self.font)
        self.frameNumber.setText(text)
        fontColor = QtGui.QBrush(QtGui.QColor(45, 45, 45, 255))
        self.setBrush(fontColor)
        self.frameNumber.setBrush(fontColor)

        # no highlight
        self.setPen(QtCore.Qt.NoPen)

    def counteractZoom(self, zoomLevel=1.0):
        """Counteracts zoom based on zoomLevel

        Args:
            zoomLevel (float): Value to set zoom
        """
        # this just inverts scale so the frame will be the same width
        self.setTransform(QtGui.QTransform.fromScale(zoomLevel, 1.0))


class BaseText(QtGui.QGraphicsSimpleTextItem):

    def setParentItem(self, parent):
        """Store parent

        Args:
            parent (QtWidgets.QWidget): Parent to set
        """
        self._parent = parent
        return super(BaseText, self).setParentItem(parent)


class BaseItem(QtGui.QGraphicsRectItem):
    """Base Graphics Item for track items

    This is a base time that represents a timeline range based on the passed track item. It has an in label, out label,
    and track label to show the bounds of the track in the rectangle.

    The counteractZoom method is used when framing and zooming the SceneView. It will be overridden/added to graphic
    items that require specific zooming such as the TrackNameItem that needs a specific constant width in view. It
    should be run when the item is initialized or when the scene view is scrolled/zoomed.

    Attributes:
        item (trackModel.trackModelItem): Track item holding data
        sourceInLabel (QtGui.QGraphicsSimpleTextItem): Track inframe text label
        sourceOutLabel (QtGui.QGraphicsSimpleTextItem): Track outframe text label
        sourceNameLabel (QtGui.QGraphicsSimpleTextItem): Track name text label
        xValue (float): Start position
        currentXOffset (float): Offset x value of rectangle in view
    """

    def __init__(self, item, *args, **kwargs):
        """Initializer

        Args:
            item (Models.trackModelItem.TrackData): Track item
        """
        super(BaseItem, self).__init__(*args, **kwargs)
        self.item = item

        # set selectable
        self.setFlags(QtGui.QGraphicsItem.ItemIsSelectable)

        # set brush
        self.setBrush(QtGui.QBrush(QtGui.QColor(57, 57, 57, 255)))

        # set pen cosmetic so it scales nicely
        pen = QtGui.QPen()
        pen.setWidth(0)
        pen.setCosmetic(True)
        self.setPen(pen)

        # add labels for in and out of clips
        self.sourceInLabel = BaseText(self)
        self.sourceOutLabel = BaseText(self)
        self.sourceNameLabel = BaseText(self)

        self.font = QtGui.QFont(const.TimelineValues.FONT)
        self.font.setPointSize(8)
        fontColor = QtGui.QBrush(QtGui.QColor(228, 228, 228, 255))

        for label in (
            self.sourceNameLabel,
            self.sourceInLabel,
            self.sourceOutLabel,
        ):
            label.setFont(self.font)
            label.setBrush(fontColor)

        self._inMarker = None
        self._outMarker = None

        self._addMarkers()
        self._setLabels()
        self._setTooltip()

        self.xValue = 0.0
        self.currentXOffset = const.TimelineValues.TRACK_NAME_WIDGET_WIDTH

    def paint(self, *args, **kwargs):
        """Overridden paint method"""
        # remove parent to remove style
        newArgs = [args[0], QtGui.QStyleOptionGraphicsItem()] + list(args[2:])
        super(BaseItem, self).paint(*newArgs, **kwargs)

    def setParentItem(self, parent):
        """Store parent

        Args:
            parent (QtGui.QWidget): Parent to set
        """
        self._parent = parent
        return super(BaseItem, self).setParentItem(parent)

    def _positionLabels(self):
        """Position labels to margin"""
        self.sourceInLabel.setY(-const.TimelineValues.LABEL_MARGIN)
        self.sourceOutLabel.setY(-const.TimelineValues.LABEL_MARGIN)
        self.sourceNameLabel.setY(
            (const.TimelineValues.TRACK_HEIGHT - self.sourceNameLabel.boundingRect().height()) / 2.0
        )

    def itemChange(self, change, value):
        """Item change callback for selection highlighting

        Args:
            change (QtGui.GraphicsItemChange): Item change
            value (QtCore.QVariant): Value change
        """
        if change == QtGui.QGraphicsItem.ItemSelectedHasChanged:
            pen = self.pen()
            pen.setColor(QtGui.QColor(0, 255, 0, 255) if self.isSelected() else QtGui.QColor(0, 0, 0, 255))
            pen.setWidth(const.TimelineValues.HIGHLIGHT_WIDTH if self.isSelected() else 0)
            self.setPen(pen)

        return super(BaseItem, self).itemChange(change, value)

    def _addMarkers(self):
        """Adds markers. Depending on the graphics item this will be overridden to add markers"""

    def _setLabels(self):
        """Set label positions/data in view"""
        self._positionLabels()

    def _setTooltip(self):
        """Sets tools tips on items"""

    def counteractZoom(self, zoomLevel=1.0):
        """This counteracts the scale and offsets from the text so it's readable

        Args:
            zoomLevel (float): Zoom value
        """
        # set labels and offset
        self.setX(self.xValue + self.currentXOffset * zoomLevel)
        for label in (
            self.sourceNameLabel,
            self.sourceInLabel,
            self.sourceOutLabel,
        ):
            # invert labels so they don't squish
            label.setTransform(QtGui.QTransform.fromScale(zoomLevel, 1.0))

        # set labels visible if they're not too big for view
        rect = self.boundingRect()
        nameWidth = self.sourceNameLabel.boundingRect().width() * zoomLevel
        inWidth = self.sourceInLabel.boundingRect().width() / 2 * zoomLevel
        outWidth = self.sourceOutLabel.boundingRect().width() / 2 * zoomLevel

        framesSpace = inWidth + outWidth + 3 * zoomLevel

        if framesSpace > rect.width():
            self.sourceInLabel.setVisible(False)
            self.sourceOutLabel.setVisible(False)
        else:
            self.sourceInLabel.setVisible(True)
            self.sourceOutLabel.setVisible(True)

            self.sourceInLabel.setX(zoomLevel - inWidth)
            self.sourceOutLabel.setX(rect.width() - zoomLevel - outWidth)

        totalWidth = nameWidth + framesSpace
        if totalWidth > rect.width():
            self.sourceNameLabel.setVisible(False)
        else:
            self.sourceNameLabel.setVisible(True)
            self.sourceNameLabel.setX(0.5 * (rect.width() - nameWidth))

        # set in label so we can see it if overlaying start frame
        if self.item.fullStartFrame == self.item.startFrame:
            self.sourceInLabel.setX(inWidth)

        # set out label so we can see it if culled by timelien
        if self.item.fullEndFrame == self.item.endFrame:
            value = self.rect().right() - zoomLevel - outWidth * 2
            self.sourceOutLabel.setX(value)


class FrameBox(QtGui.QWidget):
    """FrameBox is editable frame widget that has a label and QSpinBox

    Signals:
        frameChange (int): Frame changed value
    """

    frameChanged = QtCore.Signal(int)

    def __init__(self, timelineRange, parent=None):
        """Initializer

        Args:
            timelineRange (list of 2 int): Start and end frame range
            parent (QtGui.QWidget):
        """
        super(FrameBox, self).__init__(parent)
        self.timelineRange = timelineRange
        self.setupUi()

    def setupUi(self):
        """Sets up ui"""
        self.setStyleSheet("background-color: rgb(82, 82, 82);")

        self.font = QtGui.QFont(const.TimelineValues.FONT)
        self.font.setPointSize(8)

        self.layout = QtGui.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.frameText = QtGui.QLabel("frame: ")
        self.setFont(self.font)
        self.frameText.setStyleSheet("color: rgb(228, 228, 228);")

        self.frameBox = QtGui.QSpinBox()
        self.frameBox.setRange(*self.timelineRange)
        self.frameBox.setMinimumWidth(60)
        self.frameBox.setStyleSheet("background-color: rgb(52, 52, 52);")

        # set alignment
        self.frameBox.setAlignment(QtCore.Qt.AlignRight)
        # remove buttons
        self.frameBox.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)

        self.layout.addWidget(self.frameText)
        self.layout.addWidget(self.frameBox)

        self.frameBox.valueChanged.connect(self.frameUpdate)

    def frameUpdate(self, frame):
        """Emits frameChanged signal

        Args:
            frame (int): Frame value
        """
        self.frameChanged.emit(frame)


class EmptyNameItem(QtGui.QGraphicsRectItem):
    """Just an empty rectangle that has a color"""

    def __init__(self, rect):
        """Initializer

        Args:
            rect (QtCore.QRect): Rectangle to init size
        """
        super(EmptyNameItem, self).__init__(rect)
        self.setBrush(QtGui.QBrush(QtGui.QColor(57, 57, 57, 255)))


class TrackButtonWidget(QtGui.QWidget):
    """Add/remove/merge track buttons widget

    A widget with buttons and signals for use on callback

    Signals:
        trackAdded: Emitted when add track button pressed
        trackRemoved: Emitted when remove track pressed
        horizontalSplit: Emitted when horizontal split track button pressed
        verticalSplit: Emitted when vertical split button pressed
        horizontalMerge: Emitted when horizontal merge button pressed
        verticalMerge: Emitted when vertical merge button pressed
    """

    trackAdded = QtCore.Signal()
    trackRemoved = QtCore.Signal()
    horizontalSplit = QtCore.Signal()
    verticalSplit = QtCore.Signal()
    horizontalMerge = QtCore.Signal()
    verticalMerge = QtCore.Signal()

    def __init__(self, parent=None):
        """Initializer

        Args:
            parent (QtGui.QWidget): Parent widget
        """
        super(TrackButtonWidget, self).__init__(parent)

        self.layout = QtGui.QHBoxLayout()
        self.layout.setSpacing(20)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.setStyleSheet("background-color: rgb(82, 82, 82);")

        self.addButton = QtGui.QPushButton()
        self.addButton.setIcon(uiConst.Icons.ADD)
        self.addButton.setToolTip("Add track")

        self.removeButton = QtGui.QPushButton()
        self.removeButton.setIcon(uiConst.Icons.TRASH)
        self.removeButton.setToolTip("Remove track")

        self.addRemoveLayout = QtGui.QHBoxLayout()
        self.addRemoveLayout.setSpacing(5)
        self.addRemoveLayout.addWidget(self.addButton)
        self.addRemoveLayout.addWidget(self.removeButton)

        self.addHSplitButton = QtGui.QPushButton()
        self.addHSplitButton.setIcon(uiConst.Icons.SPLIT_HORIZONTAL)
        self.addHSplitButton.setToolTip("Horizontally split selected track")

        self.addVSplitButton = QtGui.QPushButton()
        self.addVSplitButton.setIcon(uiConst.Icons.SPLIT_VERTICAL)
        self.addVSplitButton.setToolTip("Vertically split selected track")

        self.splitLayout = QtGui.QHBoxLayout()
        self.splitLayout.setSpacing(5)
        self.splitLayout.addWidget(self.addHSplitButton)
        self.splitLayout.addWidget(self.addVSplitButton)

        self.addHJoinButton = QtGui.QPushButton()
        self.addHJoinButton.setIcon(uiConst.Icons.JOIN_HORIZONTAL)
        self.addHJoinButton.setToolTip("Horizontally merge selected track")

        self.addVJoinButton = QtGui.QPushButton()
        self.addVJoinButton.setIcon(uiConst.Icons.JOIN_VERTICAL)
        self.addVJoinButton.setToolTip("Vertically merge selected track")

        actionBtnSize = 26
        actionIconSize = QtCore.QSize(24, 24)
        for button in (
            self.addHSplitButton,
            self.addVSplitButton,
            self.addButton,
            self.removeButton,
            self.addHJoinButton,
            self.addVJoinButton,
        ):
            button.setFixedSize(actionBtnSize, actionBtnSize)
            button.setIconSize(actionIconSize)

        self.addHSplitButton.clicked.connect(self.splitHorizontal)
        self.addVSplitButton.clicked.connect(self.splitVertical)
        self.addButton.clicked.connect(self.addTrack)
        self.removeButton.clicked.connect(self.removeTrack)
        self.addHJoinButton.clicked.connect(self.mergeHorizontal)
        self.addVJoinButton.clicked.connect(self.mergeVertical)

        # commented out until merge is implemented
        self.joinLayout = QtGui.QHBoxLayout()
        self.joinLayout.setSpacing(5)
        self.joinLayout.addWidget(self.addHJoinButton)
        self.joinLayout.addWidget(self.addVJoinButton)

        self.layout.addLayout(self.addRemoveLayout)
        self.layout.addLayout(self.splitLayout)
        self.layout.addLayout(self.joinLayout)

    def addTrack(self):
        """Emit add track"""
        self.trackAdded.emit()

    def removeTrack(self):
        """Emit remove track"""
        self.trackRemoved.emit()

    def splitHorizontal(self):
        """Emit add horizontal split track"""
        self.horizontalSplit.emit()

    def splitVertical(self):
        """Emit add vertical split track"""
        self.verticalSplit.emit()

    def mergeHorizontal(self):
        """Emit merge horizontal split track"""
        self.horizontalMerge.emit()

    def mergeVertical(self):
        """Emit merge vertical split track"""
        self.verticalMerge.emit()


class TopBar(QtGui.QGraphicsRectItem):
    """Top bar area with buttons and framebox

    This is the top most part of the timeline known as the top bar margin area. The height of it is based on the
    const.TimelineValues.TOP_MARGIN value.
    """

    def __init__(self, sceneRect, timelineRange, parent=None):
        """Initializer

        Args:
            sceneRect (QtCore.QRect): Rectangle size
            timelineRange (list of 2 int): Start and end frame range
            parent (QtGui.QWidget):
        """
        super(TopBar, self).__init__(sceneRect)
        self._parent = parent
        self.timelineRange = timelineRange
        self.setupUi()

    def setupUi(self):
        """Sets up ui"""

        # empty item
        trackNameRect = QtCore.QRectF(
            0, 0, const.TimelineValues.TRACK_NAME_WIDGET_WIDTH, const.TimelineValues.TOP_MARGIN
        )
        self.emptyItem = EmptyNameItem(trackNameRect)
        self.emptyItem.setParentItem(self)
        self.emptyItem.setX(0)
        self.emptyItem.setZValue(1)

        # button widget with add track etc
        self._buttonItem = QtGui.QGraphicsProxyWidget(self)
        self.buttonWidget = TrackButtonWidget()
        self._buttonItem.setWidget(self.buttonWidget)
        self._buttonItem.setY(10)

        # connect button callbacks
        self.buttonWidget.trackAdded.connect(self._parent._view.addTrack)
        self.buttonWidget.trackRemoved.connect(self._parent._view.removeTrack)
        self.buttonWidget.horizontalSplit.connect(self._parent._view.horizontalSplitTrack)
        self.buttonWidget.verticalSplit.connect(self._parent._view.verticalSplitTrack)
        self.buttonWidget.horizontalMerge.connect(self._parent._view.horizontalMergeTrack)
        self.buttonWidget.verticalMerge.connect(self._parent._view.verticalMergeTrack)

        # frame box
        self._spinItem = QtGui.QGraphicsProxyWidget(self)
        self.spinBox = FrameBox(self.timelineRange)
        self.spinBox.frameChanged.connect(self._parent.updateRulerFrame)
        self._spinItem.setWidget(self.spinBox)
        self._spinItem.setX(
            (self._parent._view.size().width() - self._spinItem.boundingRect().width()) * const.CURRENT_ZOOM_LEVEL
        )
        self._spinItem.setY(10)

    def updateFrame(self, frame):
        """Set spinbox frame

        Args:
            frame (int): Frame value
        """
        self.spinBox.frameBox.setValue(frame)

    def counteractZoom(self, zoomLevel=1.0):
        """Counteracts zoom based on zoomLevel

        Args:
            zoomLevel (float): Zoom value
        """
        # inverse scale
        self.setTransform(QtGui.QTransform.fromScale(zoomLevel, 1.0))

        # set these to proper spots in view
        self._spinItem.setX((self._parent._view.size().width() - self._spinItem.boundingRect().width() - 10))
        self._buttonItem.setX(const.TimelineValues.TRACK_NAME_WIDGET_WIDTH + 10)


class TimeSliderOverlay(QtGui.QGraphicsRectItem):
    """Overlay lines for timeline

    This just represents a rectangle with lines at frame positions based on the a timeline range. The paint method is
    overridden to draw the the lines.
    """

    def __init__(self, sceneRect, timelineRange):
        """Initializer

        Args:
            sceneRect (QtCore.QRect): Rectangle size
            timelineRange (list of 2 int): Start and end frame range
        """
        super(TimeSliderOverlay, self).__init__(sceneRect)

        # get frames and step values
        self.startFrame, self.endFrame = timelineRange
        self.step = Time.getStep(self.startFrame, self.endFrame)
        self.subStep = Time.getStep(self.startFrame, self.step, scaleFactor=6.0)

    def paint(self, painter, option, widget):
        """Overridden method to draw overlay

        Args:
            painter (QtGui.QPainter): Painter object
            option (QtGui.QStyleOptionGraphicsItem): Parameters used to draw item
            widget (QWidgets.QWidget): Widget to update
        """
        super(TimeSliderOverlay, self).paint(painter, option, widget)

        # set pen for big bars
        pen = QtGui.QPen((QtGui.QColor(50, 50, 50, 255)))
        pen.setCosmetic(True)
        painter.setPen(pen)

        # ensure the last frame is draw even if not found in step
        stepFrames = range(self.startFrame, self.endFrame, self.step)
        if self.endFrame not in stepFrames:
            stepFrames.append(self.endFrame)

        height = self.rect().height()
        top = self.rect().top()

        # draw frames
        for frame in stepFrames:
            # set to larger width for larger lines
            pen.setWidth(3)
            painter.setPen(pen)

            xValue = frame * const.TimelineValues.TIME_MULTIPLIER
            painter.drawLine(xValue, top + 1.5, xValue, height - 10 + top)

            nextFrame = frame + (self.step - self.subStep)
            if nextFrame > self.endFrame:
                continue

            # set to smaller width for sub lines
            pen.setWidth(1)
            painter.setPen(pen)

            for subFrame in xrange(frame + self.subStep, frame + self.step, self.subStep):
                xValue = subFrame * const.TimelineValues.TIME_MULTIPLIER
                painter.drawLine(xValue, top + 0.5, xValue, height - 15 + top)

    def counteractZoom(self, zoomLevel=1.0):
        """Set zoom level and includes width of name area

        Args:
            zoomLevel (float): Zoom level to set
        """
        self.setX(zoomLevel * const.TimelineValues.TRACK_NAME_WIDGET_WIDTH)


class TimeSliderBaseItem(QtGui.QGraphicsRectItem):
    """Base rect item with emboss paint method

    The TimeSliderBaseItem is just a GraphicsRectItem with an emboss look overidding the paint method.

    Attributes:
        embossLightColor (QtGui.QColor): Light color of emboss
        embossDarkColor (QtGui.QColor): Dark color of emboss
    """

    def __init__(self, sceneRect):
        """Initializer

        Args:
            sceneRect (QtCore.QRect): Rectangle size
        """
        super(TimeSliderBaseItem, self).__init__(sceneRect)
        self.embossLightColor = QtGui.QColor(100, 100, 100, 255)
        self.embossDarkColor = QtGui.QColor(50, 50, 50, 255)

    def paint(self, painter, option, widget):
        """Overridden method to draw emboss look

        Args:
            painter (QtGui.QPainter): Painter object
            option (QtGui.QStyleOptionGraphicsItem): Parameters used to draw item
            widget (QWidgets.QWidget): Widget to update
        """
        option.state = QtGui.QStyle.State_None

        # draw rect first
        super(TimeSliderBaseItem, self).paint(painter, option, widget)

        topLeft = option.rect.topLeft()
        topRight = option.rect.topRight()
        bottomLeft = option.rect.bottomLeft()
        bottomRight = option.rect.bottomRight()

        # draw emboss look
        pen = QtGui.QPen(self.embossLightColor)
        pen.setWidth(2)
        painter.setPen(pen)
        painter.drawLine(topLeft.x(), topLeft.y(), topRight.x(), topRight.y())
        pen.setColor(self.embossDarkColor)
        pen.setWidth(4)
        painter.setPen(pen)
        painter.drawLine(bottomLeft.x(), bottomLeft.y(), bottomRight.x(), bottomRight.y())


class TimeSliderGapItem(TimeSliderBaseItem):
    """TimeSlider gap item for name widget area bottom and acts as a spacer in the timeline"""

    def __init__(self, sceneRect):
        """Initializer

        Args:
            sceneRect (QtCore.QRect): Rectangle size
        """
        super(TimeSliderGapItem, self).__init__(sceneRect)
        self.embossLightColor = QtGui.QColor(70, 70, 70, 255)
        self.embossDarkColor = QtGui.QColor(49, 49, 49, 255)
        self.setBrush(QtGui.QBrush(QtGui.QColor(57, 57, 57, 255)))

        self.setX(0)
        self.counteractZoom()

    def counteractZoom(self, zoomLevel=1.0):
        """Counteract zoom level

        Args:
            zoomLevel (float): Zoom level to set
        """
        # inverse zoom
        self.setTransform(QtGui.QTransform.fromScale(zoomLevel, 1.0))


class TimeSlider(TimeSliderBaseItem):
    """TimeSlider graphics item

    This represents the bottom timeslider rectangle with frame numbers. Any mouse clicks or move events trigger an
    update to the ruler widget to trigger frame updates if set.
    """

    def __init__(self, sceneRect, timelineRange=None):
        """Initializer

        Args:
            sceneRect (QtCore.QRect): Scene rectangle
            timelineRange (list of 2 int): Start and end frame range
        """
        super(TimeSlider, self).__init__(sceneRect)

        self.startFrame, self.endFrame = timelineRange if timelineRange else Time.getSceneFrameRange()
        self.step = Time.getStep(self.startFrame, self.endFrame)

        # set brush bg color
        self.setBrush(QtGui.QBrush(QtGui.QColor(77, 77, 77, 255)))

        # set it so we can drag on timeslider to update time
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)

        self._ruler = None
        self._frames = []
        self._addFrames()

    def _addFrames(self):
        """Add frame numbers to slider"""
        stepFrames = range(self.startFrame, self.endFrame, self.step)
        # make sure the end frame is added always regardless of step
        if self.endFrame not in stepFrames:
            stepFrames.append(self.endFrame)

        for frame in stepFrames:
            frameNumber = FrameNumber(str(frame))
            frameNumber.setX((frame * const.TimelineValues.TIME_MULTIPLIER) + 10)
            frameNumber.setY(self.rect().y())
            frameNumber.setParentItem(self)
            self._frames.append(frameNumber)

    def updateFrames(self):
        """Update frames y position"""
        for frameNumber in self._frames:
            frameNumber.setY(self.rect().y())

    def mousePressEvent(self, mouseEvent):
        """Mouse event

        Args:
            mouseEvent (QtCore.QEvent): Mouse press event
        """
        pos = self.mapToScene(mouseEvent.pos())

        # set ruler position in increments/bounds and update the framebox
        if self._ruler:
            boundedX = min(
                self.rect().right(),
                max(pos.x() - const.TimelineValues.TRACK_NAME_WIDGET_WIDTH * const.CURRENT_ZOOM_LEVEL, 0),
            )
            xPosition = int(
                math.ceil(boundedX / const.TimelineValues.TIME_MULTIPLIER) * const.TimelineValues.TIME_MULTIPLIER
            )

            self._ruler.setPos(
                QtCore.QPointF(xPosition, const.TimelineValues.TIME_SLIDER_HEIGHT - const.TimelineValues.MARKER_SIZE)
            )
            self._ruler.updateFrame(xPosition / const.TimelineValues.TIME_MULTIPLIER)

        return super(TimeSlider, self).mousePressEvent(mouseEvent)

    def mouseMoveEvent(self, moveEvent):
        """Mouse event

        Args:
            moveEvent (QtCore.QEvent): Mouse press event
        """
        pos = self.mapToScene(moveEvent.pos())

        # set ruler position in increments/bounds and update the framebox
        if self._ruler:
            boundedX = min(
                self.rect().right(),
                max(pos.x() - const.TimelineValues.TRACK_NAME_WIDGET_WIDTH * const.CURRENT_ZOOM_LEVEL, 0),
            )
            xPosition = int(
                math.ceil(boundedX / const.TimelineValues.TIME_MULTIPLIER) * const.TimelineValues.TIME_MULTIPLIER
            )

            # make sure in bounds of time slider
            self._ruler.setPos(
                QtCore.QPointF(
                    xPosition,
                    const.TimelineValues.TIME_SLIDER_HEIGHT - const.TimelineValues.MARKER_SIZE,
                )
            )
            self._ruler.updateFrame(int(xPosition / const.TimelineValues.TIME_MULTIPLIER))

    def addRuler(self, ruler):
        """Add ruler to timeline so it will update on press events"""
        self._ruler = ruler

    def counteractZoom(self, zoomLevel=1.0):
        """Set zoom level and includes width of name area

        Args:
            zoomLevel (float): Zoom level to set
        """
        # inverse scale
        self.setX(zoomLevel * const.TimelineValues.TRACK_NAME_WIDGET_WIDTH)

        for frame in self._frames:
            frame.counteractZoom(zoomLevel=zoomLevel)


class TrackChildText(QtGui.QGraphicsSimpleTextItem):
    """Track child text item

    This is a selectable text item with a colored box next to it. Represents the track child text.
    """

    def __init__(self, *args, **kwargs):
        super(TrackChildText, self).__init__(*args, **kwargs)

        # make a selection rectangle
        self.selectionRect = QtGui.QGraphicsRectItem(self)
        self.selectionRect.setRect(0, 0, const.TimelineValues.TRACK_NAME_WIDGET_WIDTH - 45, 13)
        self.selectionRect.setBrush(QtGui.QColor(53, 104, 255, 100))
        self.selectionRect.setZValue(-1)
        self.selectionRect.setVisible(False)
        self.selectionRect.setPen(QtCore.Qt.NoPen)

        # make little box next to element
        self.box = QtGui.QGraphicsRectItem(self)
        self.box.setRect(0, 0, 10, 10)
        self.box.setPos(-15, 3)

        # make selectable
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsFocusable, True)

        self._parent = None

    def paint(self, *args, **kwargs):
        """Overridden paint method"""
        # remove parent so it doesn't inherit selection options etc
        new_args = [args[0], QtGui.QStyleOptionGraphicsItem()] + list(args[2:])
        super(TrackChildText, self).paint(*new_args, **kwargs)

    def setParentItem(self, parent):
        """Sets parent item

        Args:
            parent (QtGui.QWidget): Parent item
        """
        self._parent = parent
        return super(TrackChildText, self).setParentItem(parent)

    def itemChange(self, change, value):
        """Item change callback for selection highlighting

        Args:
            change (QtGui.GraphicsItemChange): Item change
            value (QtCore.QVariant): Value change
        """
        if change == QtGui.QGraphicsItem.ItemSelectedHasChanged:
            self.selectionRect.setVisible(self.isSelected())

        return super(TrackChildText, self).itemChange(change, value)

    def setColor(self, color):
        """Sets the color of box"""
        self.box.setBrush(QtGui.QColor(*color))

    def mouseMoveEvent(self, mouseEvent):
        """Mouse press event when pressed

        Args:
            mouseEvent (QtCore.QMouseEvent): Mouse event
        """
        self._parent._dragEvent(mouseEvent)


class SelectionRect(QtGui.QGraphicsRectItem):
    """Simple Graphics Item to be able to filter with events"""
    def __init__(self, *args, **kwargs):
        super(SelectionRect, self).__init__(*args, **kwargs)
        self.setBrush(QtGui.QColor(53, 104, 255))
        self.setZValue(-1)
        self.setVisible(False)


class TrackNameItem(BaseItem):
    """Name item to left side of track

    This is a collapsible track item with a double clickable label to open or close children of the track
    """

    def __init__(self, track, rect, *args, **kwargs):
        """Initializer

        Args:
            track (models.trackModelItem.TrackData): Track item
            rect (QtCore.QRect): Rectangle of item
        """
        super(TrackNameItem, self).__init__(track, rect, *args, **kwargs)
        self.trackTitle = track.title

        # make metrics item and check width
        self.fontMetrics = QtGui.QFontMetrics(self.font)
        self.trackTitleWidth = self.fontMetrics.width(self.trackTitle)

        # icons
        self.addIcon = uiConst.Icons.ADD.pixmap(QtCore.QSize(10, 10))
        self.removeIcon = uiConst.Icons.REMOVE.pixmap(QtCore.QSize(10, 10))

        # set title and y position
        self.sourceNameLabel.setText(self.trackTitle)
        self.sourceNameLabel.setY(
            (const.TimelineValues.TRACK_HEIGHT - self.sourceNameLabel.boundingRect().height())
            - const.TimelineValues.TRACK_HEIGHT * 0.35
        )
        self.sourceNameLabel.setParentItem(self)

        if self.trackTitleWidth > const.TimelineValues.TRACK_NAME_WIDGET_WIDTH - 30:
            self.sourceNameLabel.setText(self.trackTitle[: const.TimelineValues.MAX_NAME_LENGTH] + "...")

        # make clickable label
        self._checkItem = QtGui.QGraphicsProxyWidget(self)
        self._checkLabel = QtGui.QLabel()
        self._checkLabel.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self._checkLabel.setPixmap(uiConst.Icons.REMOVE.pixmap(QtCore.QSize(10, 10)))
        self._checkItem.setWidget(self._checkLabel)
        self._checkItem.setY(
            self.sourceNameLabel.y()
            + self.sourceNameLabel.boundingRect().height()
            - self._checkItem.boundingRect().height()
        )

        # add selection rectangle
        rect.setHeight(self.sourceNameLabel.boundingRect().height() + const.TimelineValues.TRACK_HEIGHT * 0.35 + 10)
        self.selectionRect = SelectionRect(self)
        self.selectionRect.setRect(rect)

        # checked by default
        self._checked = True
        self._children = []

        self._addChildren()

    def _addChildren(self):
        """Add child titles"""

        # remove all first
        if self._children:
            for item in self._children:
                self._parent._scene.removeItem(item)
            self._children = []

        # add children
        yPos = self.sourceNameLabel.y() + 10
        for title in self.item.titleChildren:
            childTitle = TrackChildText(self)
            childTitle.setParentItem(self)
            font = self.font
            font.setPointSize(8)

            # make sure it doesn't go out of bounds
            metric = QtGui.QFontMetrics(font)
            titleWidth = metric.width(title)
            if titleWidth > const.TimelineValues.TRACK_NAME_WIDGET_WIDTH - 35:
                childTitle.setText(title[: const.TimelineValues.MAX_NAME_LENGTH - 2] + "...")
            else:
                childTitle.setText(str(title))

            childTitle.setFont(font)
            childTitle.setBrush(self.sourceNameLabel.brush())
            childTitle.setColor(self.item.color)

            yPos += childTitle.boundingRect().height() + 5
            childTitle.setY(yPos)
            self._children.append(childTitle)

        # update rect
        self.setRect(
            0, 0, const.TimelineValues.TRACK_NAME_WIDGET_WIDTH, yPos + 20 + (const.TimelineValues.TRACK_HEIGHT * 0.35)
        )

    def itemChange(self, change, value):
        """Item change callback for selection highlighting

        Args:
            change (QtGui.GraphicsItemChange): Item change
            value (QtCore.QVariant): Value change
        """
        if change == QtGui.QGraphicsItem.ItemSelectedHasChanged:
            self.selectionRect.setVisible(self.isSelected())

        return super(BaseItem, self).itemChange(change, value)

    def getChildrenHeight(self):
        """Children full height

        Returns:
            float : Height of children
        """
        yPos = self.sourceNameLabel.y() + 10
        for childTitle in self._children:
            yPos += childTitle.boundingRect().height() + 5

        return yPos + 20 + const.TimelineValues.TRACK_HEIGHT * 0.35

    def mousePressEvent(self, mouseEvent):
        """Mouse press event

        Args:
            mouseEvent (QtCore.QMouseEvent): The mouse event
        """
        # if double click collapse or open widget names
        if mouseEvent.type() == QtCore.QEvent.GraphicsSceneMouseDoubleClick:
            # set to add icon and make invisible
            if self._checked:
                self._checkLabel.setPixmap(self.addIcon)
                self._checked = False

                for childTitle in self._children:
                    childTitle.setVisible(False)

                self.setRect(0, 0, const.TimelineValues.TRACK_NAME_WIDGET_WIDTH, const.TimelineValues.TRACK_HEIGHT)
            # set to remove icon and make visible
            else:
                self._checkLabel.setPixmap(self.removeIcon)
                self._checked = True

                for childTitle in self._children:
                    childTitle.setVisible(True)

                self.setRect(0, 0, const.TimelineValues.TRACK_NAME_WIDGET_WIDTH, self.getChildrenHeight())

            # update view track heights
            self._parent.updateTrackHeight()

    def _dragEvent(self, mouseEvent):
        """Drag event to drop items outside of graphics scene

        Args:
            mouseEvent (QtCore.QEvent): Mouse press event
        """
        indices = [child.text() for child in self._children if child.isSelected()]
        mimeText = const.MIMEDATA_DELIMITER.join(indices) + ":" + self.trackTitle
        mimeData = QtCore.QMimeData()
        mimeData.setData("text/plain", mimeText)
        mimeData.setText(mimeText)

        # make a drag and set mimeData
        drag = QtGui.QDrag(mouseEvent.widget())
        drag.setMimeData(mimeData)
        drag.exec_(QtCore.Qt.LinkAction | QtCore.Qt.MoveAction)

    def counteractZoom(self, zoomLevel=1.0):
        """Counteracts zoom level

        Args:
            zoomLevel (float): Zoom level to set
        """
        self.sourceNameLabel.setX(self.boundingRect().left() + 30)
        self._checkItem.setX(self.boundingRect().left() + 10)

        for item in self._children:
            item.setX(self.boundingRect().left() + 35)

        # counteract zoom on width
        self.setTransform(QtGui.QTransform.fromScale(zoomLevel, 1.0))


class Marker(QtGui.QGraphicsPolygonItem):
    """A selectable marker to set ranges of track

    This marker is used to visualize, edit, and set bounds of a track. Double clicking on the marker will popup a
    spinbox you can edit directly to set the frame. It will inherit a brush color when setParentItem is executed with
    a graphics parent.
    """

    def __init__(self, item, baseItem, mirror=False, *args, **kwargs):
        """
        Args:
            item (models.trackModelItem.BaseItem): Track item
            baseItem (QtGui.QGraphicsItem): Parent item to update when marker changes
            mirror (bool): Left or right position of marker
        """
        self.item = item
        self.mirror = mirror
        self.baseItem = baseItem

        if mirror:
            poly = QtGui.QPolygonF()
            poly.append(QtCore.QPointF(0, const.TimelineValues.TRACK_HEIGHT))
            poly.append(QtCore.QPointF(-const.TimelineValues.MARKER_SIZE * 0.3333, const.TimelineValues.TRACK_HEIGHT))
            poly.append(
                QtCore.QPointF(-const.TimelineValues.MARKER_SIZE * 0.3333, const.TimelineValues.TRACK_HEIGHT * 0.5)
            )
            poly.append(QtCore.QPointF(-const.TimelineValues.MARKER_SIZE, 0))
            poly.append(QtCore.QPointF(0, 0))

        else:
            poly = QtGui.QPolygonF()
            poly.append(QtCore.QPointF(0, const.TimelineValues.TRACK_HEIGHT))
            poly.append(QtCore.QPointF(0, 0))
            poly.append(QtCore.QPointF(const.TimelineValues.MARKER_SIZE, 0))
            poly.append(
                QtCore.QPointF(const.TimelineValues.MARKER_SIZE * 0.3333, const.TimelineValues.TRACK_HEIGHT * 0.5)
            )
            poly.append(QtCore.QPointF(const.TimelineValues.MARKER_SIZE * 0.3333, const.TimelineValues.TRACK_HEIGHT))

        super(Marker, self).__init__(poly, *args, **kwargs)

        # make movable, focusable, and selectable
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsFocusable, True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)

        # no outline
        pen = QtGui.QPen(QtGui.QColor(0, 0, 0, 255), 1)
        pen.setWidth(0)
        pen.setStyle(QtCore.Qt.NoPen)
        self.setPen(pen)

        # add framebox that when double clicked allows you to set marker frame
        self._frameItem = QtGui.QGraphicsProxyWidget(self)
        self.frameBox = QtGui.QSpinBox()
        self.frameBox.setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
        self.frameBox.setRange(*self.item.fullTimelineRange)
        self.frameBox.setMinimumWidth(20)
        self.frameBox.setAlignment(QtCore.Qt.AlignCenter)
        self.frameBox.setStyleSheet("background-color: rgba(52, 52, 52, 100);")
        self.frameBox.setValue(self.item.endFrame if mirror else self.item.startFrame)
        self.frameBox.editingFinished.connect(self.updateFrame)
        self.frameBox.hide()

        # set position
        self._frameItem.setWidget(self.frameBox)
        self._frameItem.setX(-self.frameBox.width() / 2)
        self._frameItem.setY(const.TimelineValues.TRACK_HEIGHT)

    def paint(self, *args, **kwargs):
        """Overridden paint method"""
        # remove parent so it doesn't inherit selection options etc
        new_args = [args[0], QtGui.QStyleOptionGraphicsItem()] + list(args[2:])
        super(Marker, self).paint(*new_args, **kwargs)

    def updateFrame(self, frame=None):
        """Update frame

        Args:
            frame (int): Frame to set
        """
        if not frame:
            frame = self.frameBox.value()

        if self.mirror:
            self.item.endFrame = frame
        else:
            self.item.startFrame = frame

        self.baseItem._adjustToRange()
        self.frameBox.hide()

    def mouseMoveEvent(self, mouseEvent):
        """Mouse event

        Args:
            mouseEvent (QtCore.QEvent): Mouse press event
        """
        pos = self.mapToScene(mouseEvent.pos())

        # get frame
        boundedX = min(
            self.item.fullDuration * const.TimelineValues.TIME_MULTIPLIER,
            max(pos.x() - const.TimelineValues.TRACK_NAME_WIDGET_WIDTH * const.CURRENT_ZOOM_LEVEL, 0),
        )
        xPosition = int(
            math.ceil(boundedX / const.TimelineValues.TIME_MULTIPLIER) * const.TimelineValues.TIME_MULTIPLIER
        )
        frame = int(xPosition / const.TimelineValues.TIME_MULTIPLIER)

        # update frames
        self.frameBox.setValue(frame)
        self.updateFrame(frame)

        return super(Marker, self).mousePressEvent(mouseEvent)

    def mousePressEvent(self, mouseEvent):
        """Mouse press event for selection and frame popup

        Args:
            mouseEvent (QtCore.QEvent): Mouse press event
        """
        eventType = mouseEvent.type()
        # show framebox
        if eventType == QtCore.QEvent.GraphicsSceneMouseDoubleClick:
            self.frameBox.show()
            self.frameBox.setFocus()
            self.frameBox.selectAll()

        elif eventType == QtCore.QEvent.GraphicsSceneMousePress:
            self.setSelected(True)

        return super(Marker, self).mousePressEvent(mouseEvent)

    def setParentItem(self, parent):
        """Store parent and grab brush from it

        Args:
            parent (QtGui.QWidget): Parent to set
        """
        self.setBrush(parent.brushItem)
        return super(Marker, self).setParentItem(parent)

    def itemChange(self, change, value):
        """Item change callback for selection highlighting

        Args:
            change (QtGui.GraphicsItemChange): Item change
            value (QtCore.QVariant): Value change
        """
        if change == QtGui.QGraphicsItem.ItemSelectedHasChanged:
            pen = self.pen()
            pen.setColor(QtGui.QColor(0, 255, 0, 255) if self.isSelected() else QtGui.QColor(0, 0, 0, 255))
            pen.setWidth(const.TimelineValues.HIGHLIGHT_WIDTH if self.isSelected() else 0)
            pen.setStyle(QtCore.Qt.SolidLine if self.isSelected() else QtCore.Qt.NoPen)
            self.setPen(pen)
            if not self.isSelected():
                self.frameBox.hide()

        return super(Marker, self).itemChange(change, value)

    def counteractZoom(self, zoomLevel=1.0):
        """Counteracts zoom

        Args:
            zoomLevel (float): Zoom level to set
        """
        self.setTransform(QtGui.QTransform.fromScale(zoomLevel, 1.0))


class GapItem(BaseItem):
    """Gap item rectangle that is drawn in areas where the clipItem is not

    This gap item only shows markers if the start or end frame is at the bounds of the timeline.
    """

    def __init__(self, *args, **kwargs):
        # set brush item for baseItem
        gapColor = QtGui.QColor(32, 32, 32, 255)
        self.brushItem = QtGui.QBrush(gapColor)

        super(GapItem, self).__init__(*args, **kwargs)

        # draw gradient over view to make it prettier
        dark = QtGui.QColor(0, 0, 0, 255)
        gradient = QtGui.QLinearGradient(
            QtCore.QPointF(0, self.boundingRect().top()),
            QtCore.QPointF(0, self.boundingRect().bottom()),
        )
        gradient.setColorAt(0.3, gapColor)
        gradient.setColorAt(1.0, dark)
        self.setBrush(QtGui.QBrush(gradient))
        # draw this under other objects
        self.setZValue(-1)
        self._parent = None

    def _addMarkers(self):
        """Add markers for gap ends"""
        self._inMarker = Marker(self.item, self)
        self._inMarker.setParentItem(self)
        self._inMarker.setX(0)
        self._inMarker.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)
        self._inMarker.setFlag(QtGui.QGraphicsItem.ItemIsMovable, False)

        self._outMarker = Marker(self.item, self, mirror=True)
        self._outMarker.setParentItem(self)
        self._outMarker.setX((self.item.duration - 1) * const.TimelineValues.TIME_MULTIPLIER)
        self._outMarker.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)
        self._outMarker.setFlag(QtGui.QGraphicsItem.ItemIsMovable, False)

        self.sourceInLabel.setText(str(self.item.startFrame))
        self.sourceInLabel.setBrush(QtGui.QColor(52, 52, 52, 255))
        self.sourceOutLabel.setText(str(self.item.endFrame - 1))
        self.sourceOutLabel.setBrush(QtGui.QColor(52, 52, 52, 255))

        if self.item.startFrame == self.item.fullStartFrame:
            self._inMarker.setVisible(True)
            self.sourceInLabel.setVisible(True)
        else:
            self._inMarker.setVisible(False)
            self.sourceInLabel.setVisible(False)

        if self.item.endFrame - 1 == self.item.fullEndFrame:
            self._outMarker.setVisible(True)
            self.sourceOutLabel.setVisible(True)
        else:
            self._outMarker.setVisible(False)
            self.sourceOutLabel.setVisible(False)

    def counteractZoom(self, zoomLevel=1.0):
        """Counteract zoom

        Args:
            zoomLevel (float): Zoom level to set
        """
        super(GapItem, self).counteractZoom(zoomLevel)

        # Set marker visible if at beginning or end of track
        if self.item.startFrame == self.item.fullStartFrame:
            self.sourceInLabel.setVisible(True)
            self._inMarker.setVisible(True)
        else:
            self._inMarker.setVisible(False)
            self.sourceInLabel.setVisible(False)

        if self.item.endFrame - 1 == self.item.fullEndFrame:
            self.sourceOutLabel.setVisible(True)
            outWidth = self.sourceOutLabel.boundingRect().width() / 2 * zoomLevel
            value = self.boundingRect().right() - zoomLevel - outWidth * 2
            self.sourceOutLabel.setX(value)

            self._outMarker.setVisible(True)
        else:
            self._outMarker.setVisible(False)
            self.sourceOutLabel.setVisible(False)

        self._inMarker.counteractZoom(const.CURRENT_ZOOM_LEVEL)
        self._outMarker.counteractZoom(const.CURRENT_ZOOM_LEVEL)


class ClipItem(BaseItem):
    """Clip item to visualize track bounds

    This clip has selectable/movable markers to set the bounds of the track
    """

    def __init__(self, item, *args, **kwargs):
        """Initializer

        Args:
            item (models.trackModelItem.ClipDataItem): Clip item to use data from
        """
        clipColor = QtGui.QColor(*item.color)
        self.brushItem = QtGui.QBrush(clipColor)

        super(ClipItem, self).__init__(item, *args, **kwargs)

        # draw gradient
        dark = QtGui.QColor(0, 0, 0, 255)
        gradient = QtGui.QLinearGradient(
            QtCore.QPointF(0, self.boundingRect().top()),
            QtCore.QPointF(0, self.boundingRect().bottom()),
        )
        gradient.setColorAt(0.3, clipColor)
        gradient.setColorAt(1.0, dark)
        self.setBrush(QtGui.QBrush(gradient))

        self._parent = None

    def _addMarkers(self):
        """Add selectable markers for in and out of track"""

        self._inMarker = Marker(self.item, self)
        self._inMarker.setX(0)
        self._inMarker.setParentItem(self)

        self._outMarker = Marker(self.item, self, mirror=True)
        self._outMarker.setX(self.item.duration * const.TimelineValues.TIME_MULTIPLIER)
        self._outMarker.setParentItem(self)

        self.sourceInLabel.setText(str(self.item.startFrame))
        self.sourceOutLabel.setText(str(self.item.endFrame))

    def _adjustToRange(self):
        """Adjust rect to item range"""

        rect = self.rect()
        rect.setWidth(self.item.duration * const.TimelineValues.TIME_MULTIPLIER)
        self.setRect(rect)

        self.xValue = self.item.startFrame * const.TimelineValues.TIME_MULTIPLIER
        self.setX(self.xValue)
        self.counteractZoom(zoomLevel=const.CURRENT_ZOOM_LEVEL)

        self._inMarker.setX(0)
        self._outMarker.setX(self.item.duration * const.TimelineValues.TIME_MULTIPLIER)

        # update labels
        self.sourceInLabel.setText(str(self.item.startFrame))
        self.sourceOutLabel.setText(str(self.item.endFrame))

        # update gaps
        self._parent.updateGapTracks()


class Track(QtGui.QGraphicsRectItem):
    """Main track item

    This represents a full track item including the name area as well as well as all track and gap items.
    It will accept drops for the text area to add child items if the item dropped has text data.
    The height of this is based on const.TimelineValues.TRACK_HEIGHT.

    Example usage is at the bottom of this script.
    """

    def __init__(self, track, scene, rect):
        """Initializer

        Args:
            track (models.trackModelItem.TrackDataItem): Track item to read/set data
            scene (QtGui.QGraphicsScene): Graphics scene
            rect (QtCore.QRect): Rectangle for track size
        """
        super(Track, self).__init__(rect)
        self.track = track
        self._scene = scene
        self.clipItems = []
        self.gapItems = []

        self.setupUi()

        # accept drops for child titles
        self.setAcceptDrops(True)

    def _dropEvent(self, dropEvent):
        """Drop event for adding items

        Args:
            dropEvent (QtCore.QDropEvent): Drop event

        Returns:
            bool: If children added or not
        """
        mimeData = dropEvent.mimeData()

        addChildren = False
        if hasattr(mimeData, "indicies"):
            for modelIndex in mimeData.indicies():
                # grab data
                childText = str(modelIndex.data())
                # set data
                self.trackNameItem.item.addChildTitle(childText)

            self.trackNameItem._addChildren()
        else:
            mimeData = mimeData.data("text/plain")
            namespaces = mimeData.split(const.MIMEDATA_DELIMITER)
            namespaces[-1], trackTitle = namespaces[-1].split(":")
            for namespace in namespaces:
                self.trackNameItem.item.addChildTitle(str(namespace))
                addChildren = True

            self.trackNameItem._addChildren()

        # update height and counteract zoom
        self.updateTrackHeight()
        self.trackNameItem.counteractZoom(const.CURRENT_ZOOM_LEVEL)
        return addChildren

    def setupUi(self):
        """Setup ui"""
        rect = self.rect()
        self.fullTrack = QtGui.QGraphicsRectItem(self)
        self.fullTrack.setRect(0, 0, rect.width(), const.TimelineValues.TRACK_HEIGHT)
        self.fullTrack.setBrush(QtGui.QBrush(QtGui.QColor(52, 52, 52, 255)))
        self.fullTrack.setZValue(-2)

        trackNameRect = QtCore.QRectF(
            0, 0, const.TimelineValues.TRACK_NAME_WIDGET_WIDTH, const.TimelineValues.TRACK_HEIGHT
        )
        self.trackNameItem = TrackNameItem(self.track, trackNameRect)
        self.trackNameItem.setParentItem(self)
        self.trackNameItem.setX(0)
        self.updateTrackHeight()
        self.trackNameItem.counteractZoom()

        trackHeightOffset = const.TimelineValues.TRACK_HEIGHT * 0.35
        for track in self.track.clipTracks:
            rect = QtCore.QRectF(
                0,
                trackHeightOffset,
                min(track.duration, (self.track.fullEndFrame - track.startFrame))
                * const.TimelineValues.TIME_MULTIPLIER,
                const.TimelineValues.TRACK_HEIGHT - trackHeightOffset,
            )

            newItem = ClipItem(track, rect)
            newItem.setParentItem(self)
            newItem.xValue = track.startFrame * const.TimelineValues.TIME_MULTIPLIER
            newItem.setX(newItem.xValue)
            newItem.counteractZoom()
            self.clipItems.append(newItem)

        # add gap tracks
        self.updateGapTracks()

    def updateTrackHeight(self):
        """Updates rect height based on children visible"""
        rect = self.rect()
        if self.trackNameItem._checked:
            self.setRect(0, 0, rect.width(), self.trackNameItem.getChildrenHeight())
        else:
            self.setRect(0, 0, rect.width(), const.TimelineValues.TRACK_HEIGHT)

        if hasattr(self._scene, "updateTrackHeights"):
            self._scene.updateTrackHeights()

    def updateGapTracks(self):
        """Update gap items"""
        for item in self.gapItems:
            self._scene.removeItem(item)

        trackHeightOffset = const.TimelineValues.TRACK_HEIGHT * 0.35

        for gapTrack in self.track.gapTracks:
            rect = QtCore.QRectF(
                0,
                trackHeightOffset,
                min(gapTrack.duration, (self.track.fullEndFrame - gapTrack.startFrame))
                * const.TimelineValues.TIME_MULTIPLIER,
                const.TimelineValues.TRACK_HEIGHT - trackHeightOffset,
            )

            newItem = GapItem(gapTrack, rect)
            newItem.setParentItem(self)
            newItem.xValue = gapTrack.startFrame * const.TimelineValues.TIME_MULTIPLIER
            newItem.setX(newItem.xValue)
            newItem.counteractZoom(const.CURRENT_ZOOM_LEVEL)
            self.gapItems.append(newItem)

    def counteractZoom(self, zoomLevel=1.0):
        """Counteracts zoom on items"""

        # counteract zoom
        self.trackNameItem.counteractZoom(zoomLevel)

        for gapTrack in newTrack.gapItems:
            gapTrack.counteractZoom(zoomLevel)
            gapTrack._inMarker.counteractZoom(zoomLevel)
            gapTrack._outMarker.counteractZoom(zoomLevel)

        for clipTrack in newTrack.clipItems:
            clipTrack.counteractZoom(zoomLevel)
            clipTrack._inMarker.counteractZoom(zoomLevel)
            clipTrack._outMarker.counteractZoom(zoomLevel)


# Example of usage
if __name__ == "__builtin__":

    # Get rid of offset area we don't need but is used in full timeline setup,
    const.TimelineValues.TRACK_NAME_WIDGET_WIDTH = 250.0
    const.TimelineValues.TOP_MARGIN = 0

    # get frame range to set
    timelineRange = [0, 100]
    duration = timelineRange[1] - timelineRange[0]
    # scene width is calculated by frame range and TIME_MULTIPLIER constant
    sceneWidth = (duration * const.TimelineValues.TIME_MULTIPLIER)

    # make a dialog
    windowWidth, windowHeight = 1400.0, 100.0
    dialog = QtGui.QDialog(parent=Application.GetMainWindow())
    dialog.resize(windowWidth, windowHeight)
    dialog.setContentsMargins(0, 0, 0, 0)
    dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
    dialog.setWindowTitle("Test Track")

    # make a graphics/scene view
    view = QtGui.QGraphicsView()
    view.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
    scene = QtGui.QGraphicsScene()
    # set a scene size
    scene.setSceneRect(0, 0, sceneWidth, windowHeight)
    view.setScene(scene)

    # make a layout
    layout = QtGui.QVBoxLayout()
    layout.setContentsMargins(0, 0, 0, 0)
    layout.addWidget(view)
    dialog.setLayout(layout)

    # make track item
    sceneRect = scene.sceneRect()
    sceneRect.setWidth(sceneRect.width() * 10)
    sceneRect.setHeight(const.TimelineValues.TRACK_HEIGHT)

    # add a track model, grab updating modelIndex
    model = trackModel.TrackModel(timelineRange=timelineRange)
    model.addTrack()
    modelIndex = model.getModelIndexByRow(0, columnIndex=1)

    # make a track item and make a track
    trackItem = trackModelItem.TrackDataItem([0, 50], timelineRange, trackTitle="Track", modelIndex=modelIndex)
    newTrack = Track(trackItem, scene, sceneRect)
    scene.addItem(newTrack)

    # How we calculate the zoom for the view. We only want this horizontally which is why we don't use fitInView
    zoomAmount = windowWidth / sceneWidth
    view.scale(zoomAmount, 1)
    zoomLevel = 1.0 / view.matrix().m11()
    const.CURRENT_ZOOM_LEVEL = zoomLevel

    # counteract zoom
    newTrack.trackNameItem.counteractZoom(zoomLevel)
    for track in newTrack.gapItems:
        track.counteractZoom(zoomLevel)
    for track in newTrack.clipItems:
        track.counteractZoom(zoomLevel)

    # show dialog
    dialog.show()
