import functools

from PySide import QtCore, QtGui

from RS.Utils.Scene import Time
from RS.PyCoreQt import const
from RS.PyCoreQt.Models import trackModelItem, trackModel
from RS.PyCoreQt.Widgets import trackWidgets, rulerWidget
from RS.Tools.UI import Application


class SceneWidget(QtGui.QGraphicsScene):
    """Graphics Scene for view

    This graphics scene builds all graphics items into the timeline. This sets the scene size based on the timeline
    range provided. It will also add a top bar with buttons, overlays, a timeslider, a ruler, and adds tracks.

    Attributes:
        startFrame (int): Start frame value
        endFrame (int): End frame value
        duration (int): Length of frame range
    """

    def __init__(self, timelineRange, view, *args, **kwargs):
        """Initializer

        Args:
            timelineRange (list of int): Start and end frame range
            view (QtGui.QGraphicsView): View to use
        """
        super(SceneWidget, self).__init__(*args, **kwargs)

        self._view = view

        self._tracks = []
        self._trackItems = []

        # get frame range in scene
        self.timelineRange = timelineRange
        self.startFrame, self.endFrame = self.timelineRange
        self.duration = self.endFrame - self.startFrame

        # set default bg color
        self.setBackgroundBrush(QtGui.QColor(82, 82, 82, 255))

        # adjust scene size to tracks
        self._adjustSceneSize()
        # add top bar with buttons/frame sett value
        self._addTopBar()
        # add overlay to draw increments
        self._addOverlay()
        # add timeslider
        self._addTimeSlider()
        # add the tracks
        self._addTracks()
        # add a ruler
        self._ruler = self._addRuler()

    @property
    def tracks(self):
        """Tracks property that holds all trackDataItems"""
        return self._tracks

    @tracks.setter
    def tracks(self, tracks):
        """Setter for tracks property"""

        # remove tracks before setting new ones
        for track in self._trackItems:
            self.removeItem(track)
        self._trackItems = []

        # set tracks and update
        self._tracks = tracks
        self._addTracks()

    def _adjustSceneSize(self):
        """Adjust scene size to fit to tracks"""
        trackHeight = (
            const.TimelineValues.TOP_MARGIN
            + sum(item.trackNameItem.rect().height() for item in self._trackItems)
            + const.TimelineValues.TIME_SLIDER_HEIGHT
        )

        self.setSceneRect(
            0,
            0,
            (self.duration * const.TimelineValues.TIME_MULTIPLIER),
            trackHeight,
        )

    def _addTracks(self):
        """Adds a tracks"""
        for track in self._tracks:
            self._trackItems.append(self._addTrack(track))

        # update y positions of tracks based on height of text
        self.updateTrackHeights()
        self._adjustSceneSize()

    def _addTrack(self, track):
        """Add a track

        Args:
            track (trackWidgets.Track): Track item

        Returns:
            trackWidgets.Track: Track item
        """
        # set width to much wider than view
        sceneRect = self.sceneRect()
        sceneRect.setWidth(sceneRect.width() * 10)
        sceneRect.setHeight(const.TimelineValues.TRACK_HEIGHT)
        newTrack = trackWidgets.Track(track, self, sceneRect)
        self.addItem(newTrack)
        newTrack.setX(sceneRect.x())
        return newTrack

    def updateTrackHeights(self):
        """Updates height of tracks based on dropdown of assets"""

        # set y positions based on previous track's position
        lastTrack = None
        for track in self._trackItems:
            yPos = const.TimelineValues.TOP_MARGIN

            if lastTrack:
                yPos = lastTrack.y()
                if lastTrack.trackNameItem._checked:
                    yPos += lastTrack.rect().height()
                else:
                    yPos += const.TimelineValues.TRACK_HEIGHT

            track.setY(yPos)

            lastTrack = track

    def snapTimeSlider(self):
        """Snap the timeslider to the bottom of the view"""

        # move each timeslider item to bottom
        for item in (self._timeSlider, self._timeSliderGap):
            rect = item.rect()
            rect.moveTop(self._view.size().height() - const.TimelineValues.TIME_SLIDER_HEIGHT + self._view.scrollOffset)
            item.setRect(rect)

        # snap ruler to bottom
        self._ruler.snapBottom(self._view.size().height() + self._view.scrollOffset)

        # set overlay to scale
        sceneRect = self.sceneRect()
        sceneRect.setHeight(self._view.size().height() - const.TimelineValues.TOP_MARGIN)
        sceneRect.moveTop(const.TimelineValues.TOP_MARGIN)
        sceneRect.setWidth(sceneRect.width() * 10)
        self._overlay.setRect(sceneRect)

        # fix frames to follow
        self._timeSlider.updateFrames()

        # fix top bar
        self._topBar.counteractZoom(const.CURRENT_ZOOM_LEVEL)

    def _addTimeSlider(self):
        """Add time slider"""
        sceneRect = self.sceneRect()

        # make a gap item inbetween time span and track name area
        sceneRect.setWidth(const.TimelineValues.TRACK_NAME_WIDGET_WIDTH)
        sceneRect.setHeight(const.TimelineValues.TIME_SLIDER_HEIGHT)
        sceneRect.moveTop(self.parent().height() - const.TimelineValues.TIME_SLIDER_HEIGHT)

        # make gap
        self._timeSliderGap = trackWidgets.TimeSliderGapItem(sceneRect)
        self._timeSliderGap.setZValue(float("inf"))
        self.addItem(self._timeSliderGap)

        # make timeslider and set width to duration
        sceneRect.setWidth(self.duration * const.TimelineValues.TIME_MULTIPLIER)
        self._timeSlider = trackWidgets.TimeSlider(sceneRect)
        self._timeSlider.counteractZoom()
        self._timeSlider.setZValue(float("inf"))
        self.addItem(self._timeSlider)

    def _addRuler(self):
        """Add ruler

        Returns:
            rulerWidget.Ruler: Ruler item
        """
        sceneRect = self.sceneRect()
        ruler = rulerWidget.Ruler(sceneRect.height(), view=self._view)
        ruler.setParentItem(self._timeSlider)
        ruler.setX(0)
        ruler.setY(const.TimelineValues.TIME_SLIDER_HEIGHT - const.TimelineValues.MARKER_SIZE)
        ruler.counteractZoom()
        ruler.setZValue(float("inf"))
        return ruler

    def _addOverlay(self):
        """Draws line overlays over view """
        sceneRect = self.sceneRect()
        sceneRect.setHeight(sceneRect.height() - const.TimelineValues.TOP_MARGIN)
        sceneRect.setWidth(sceneRect.width() * 10)
        sceneRect.moveTop(const.TimelineValues.TOP_MARGIN)
        self._overlay = trackWidgets.TimeSliderOverlay(sceneRect, self.timelineRange)
        self._overlay.counteractZoom()
        self.addItem(self._overlay)

    def _addTopBar(self):
        """Add top menu bar"""
        sceneRect = self.sceneRect()
        sceneRect.setHeight(const.TimelineValues.TOP_MARGIN)
        self._topBar = trackWidgets.TopBar(sceneRect, self.timelineRange, parent=self)
        self._topBar.counteractZoom()
        self.addItem(self._topBar)

    def updateFrame(self, frame):
        """Updates top bar frame

        Args:
            frame (int): Frame to set
        """
        self._topBar.updateFrame(frame)
        self._view.currentFrame = frame
        self._view._parent.frameChanged.emit(frame)

        if self._ruler:
            self._ruler.setX(frame * const.TimelineValues.TIME_MULTIPLIER)

    def updateRulerFrame(self, frame):
        """Updates ruler frame

        Args:
            frame (int): Frame to set
        """
        if self._ruler:
            self._ruler.setX(frame * const.TimelineValues.TIME_MULTIPLIER)
            self._view.currentFrame = frame
            self._view._parent.frameChanged.emit(frame)


class SceneView(QtGui.QGraphicsView):
    """Graphics view for timeline

    This graphics view builds all of the interactions such as zooming/framing and key press events. The view represents
    the viewport of the all the graphics in the timeline scene.

    Attributes:
        scrollOffset (float): Offset of scroll bar
        currentFrame (int): Current frame value of view

    Signals:
        selectionChanged (object): Graphics item when item selection changed
        frameChanged (int): Frame changed value
    """

    selectionChanged = QtCore.Signal(object)
    frameChanged = QtCore.Signal(int)

    def __init__(self, timelineRange, *args, **kwargs):
        """Initializer

        Args:
            timelineRange (list of int): Start and end frame range
            view (QtGui.QGraphicsView): View to use
        """
        # this parent is the timeline widget
        self._parent = kwargs.get("parent", None)

        super(SceneView, self).__init__(*args, **kwargs)

        # set resize under mouse for zooming
        self.setResizeAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.setTransformationAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        scene = SceneWidget(timelineRange, self, parent=self)
        self.setScene(scene)
        self.setAlignment((QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop))
        self.setStyleSheet("QGraphicsView:focus" "{" "    border: none;" "    outline: none;" "}")

        # connect signals of scroll bar, frame change, and selection change
        self.verticalScrollBar().valueChanged.connect(self.verticalScrollBarChanged)
        scene.selectionChanged.connect(self.parseSelectionChange)
        self.frameChanged.connect(scene.updateFrame)

        # set default values
        self.scrollOffset = 0
        self.currentFrame = 0
        self.sortedItems = []
        self.setAcceptDrops(True)

    def verticalScrollBarChanged(self, value):
        """Signal when vertical scroll bar is moved

        Args:
            value (int): Value slider has changed
        """
        self.scrollOffset = value
        self.scene().snapTimeSlider()

    def resizeEvent(self, resizeEvent):
        """Resize event when the view is resized

        Args:
             resizeEvent (QtCore.QResizeEvent): Resize event
        """
        # adjust scene scale
        scene = self.scene()
        scene._adjustSceneSize()
        # mover ruler to bottom
        scene.snapTimeSlider()

        super(SceneView, self).resizeEvent(resizeEvent)

    def parseSelectionChange(self):
        """Callback to parse selection"""

        if self.scene().selectedItems():
            # Remove any item that was de-selected
            for idx in xrange(len(self.sortedItems) - 1, -1, -1):
                if not self.sortedItems[idx].isSelected():
                    del self.sortedItems[idx]

            # Add new items
            for item in self.scene().selectedItems():
                if item not in self.sortedItems and item.isSelected():
                    self.sortedItems.append(item)
        else:
            self.sortedItems = []
            self.scene().clearSelection()

        # Exclude ruler from selection
        for item in self.sortedItems:
            if isinstance(item, rulerWidget.Ruler):
                continue
            self.selectionChanged.emit(item)

    def keyPressEvent(self, keyEvent):
        """Key press event when clicked

        Args:
            keyEvent (QtCore.QKeyEvent): Key event
        """
        super(SceneView, self).keyPressEvent(keyEvent)
        self.setInteractive(True)
        # check if frame all pressed
        self._keyPressFrameAll(keyEvent)

    def mousePressEvent(self, mouseEvent):
        """Mouse press event when pressed

        Args:
            mouseEvent (QtGui.QMouseEvent): Mouse event
        """
        modifiers = QtGui.QApplication.keyboardModifiers()
        self.setDragMode(
            QtGui.QGraphicsView.ScrollHandDrag if modifiers == QtCore.Qt.AltModifier else QtGui.QGraphicsView.NoDrag
        )
        self.setInteractive(not modifiers == QtCore.Qt.AltModifier)
        super(SceneView, self).mousePressEvent(mouseEvent)

    def mouseReleaseEvent(self, mouseEvent):
        """Mouse release event when released

        Args:
            mouseEvent (QtGui.QMouseEvent): Mouse event
        """
        super(SceneView, self).mouseReleaseEvent(mouseEvent)
        self.setDragMode(QtGui.QGraphicsView.NoDrag)

    def dragEnterEvent(self, dragEvent):
        """Drag event for adding items

        Args:
            dropEvent (QtGui.QDropEvent): Drop event
        """
        dragEvent.acceptProposedAction()

    def dragMoveEvent(self, dragEvent):
        """Drag event for adding items

        Args:
            dropEvent (QtGui.QDropEvent): Drop event
        """
        dragEvent.acceptProposedAction()

    def dropEvent(self, dropEvent):
        """Drop event for adding items

        Args:
            dropEvent (QtGui.QDropEvent): Drop event
        """
        itemAtDrop = self.itemAt(dropEvent.pos())

        if isinstance(itemAtDrop, trackWidgets.Track):
            itemAtDrop._dropEvent(dropEvent)
        elif isinstance(itemAtDrop, trackWidgets.TrackNameItem):
            if isinstance(dropEvent.source(), QtGui.QWidget) and not isinstance(dropEvent.source(), QtGui.QTreeView):
                namespaces = str(dropEvent.mimeData().data("text/plain")).split(const.MIMEDATA_DELIMITER)
                namespaces[-1], trackTitle = namespaces[-1].split(":")
                self.removeTrack(trackTitle=str(trackTitle), namespaces=list(namespaces))

            itemAtDrop._parent._dropEvent(dropEvent)
        elif isinstance(itemAtDrop, (trackWidgets.BaseText, trackWidgets.TrackChildText)):
            if isinstance(dropEvent.source(), QtGui.QWidget) and not isinstance(dropEvent.source(), QtGui.QTreeView):
                namespaces = str(dropEvent.mimeData().data("text/plain")).split(const.MIMEDATA_DELIMITER)
                namespaces[-1], trackTitle = namespaces[-1].split(":")
                self.removeTrack(trackTitle=str(trackTitle), namespaces=list(namespaces))

            itemAtDrop._parent._parent._dropEvent(dropEvent)
        else:
            mimeData = dropEvent.mimeData()
            if hasattr(mimeData, "indicies"):
                namespaces = [str(modelIndex.data()) for modelIndex in mimeData.indicies()]
                self.addTrack(namespaces)
                dropEvent.acceptProposedAction()
                return
            else:
                data = str(mimeData.data("text/plain"))
                namespaces = data.split(const.MIMEDATA_DELIMITER)
                namespaces[-1], trackTitle = namespaces[-1].split(":")
                self.addTrack(namespaces)

    def wheelEvent(self, scrollEvent):
        """Scroll wheel event to control zooming

        Args:
            scrollEvent(QtCore.QEvent): Scroll wheel event
        """
        itemAtScroll = self.itemAt(scrollEvent.pos())
        if isinstance(
            itemAtScroll,
            (
                trackWidgets.TrackChildText,
                trackWidgets.BaseText,
                trackWidgets.TrackNameItem,
                trackWidgets.SelectionRect,
                QtGui.QGraphicsProxyWidget,
            ),
        ):
            return super(SceneView, self).wheelEvent(scrollEvent)

        scaleBy = 1.0 + (float(scrollEvent.delta()) / 1000)
        self.scale(scaleBy, 1)
        zoomLevel = 1.0 / self.matrix().m11()
        const.CURRENT_ZOOM_LEVEL = zoomLevel
        self.zoomItems(zoomLevel)

    def zoomItems(self, zoomLevel=1.0):
        """Counteracts zoom on items based on zoomLevel

        Args:
            zoomLevel (float): Value to zoom
        """
        # some items we do want to keep the same visual size. So we need to
        # inverse the effect of the zoom
        zoomTypes = (
            rulerWidget.Ruler,
            trackWidgets.BaseItem,
            trackWidgets.Marker,
            trackWidgets.TimeSlider,
            trackWidgets.TimeSliderGapItem,
            trackWidgets.FrameNumber,
            trackWidgets.GapItem,
            trackWidgets.TimeSliderOverlay,
            trackWidgets.TopBar,
        )

        for item in self.scene().items():
            if isinstance(item, zoomTypes):
                item.counteractZoom(zoomLevel)

    def _keyPressFrameAll(self, keyEvent):
        """Checks event for frame all (ctrl + f) and performs frame all if so

        Args:
            keyEvent (QtCore.QKeyEvent): Key event
        """
        if keyEvent.key() == QtCore.Qt.Key_F and (keyEvent.modifiers() & QtCore.Qt.ControlModifier):
            self.frameAll()

    def frameAll(self):
        """Frame all the graphics items in view"""
        # get horizontal scale which is m11
        zoomLevel = 1.0 / self.matrix().m11()

        # get width of window and size of scene scalar
        scaleFactor = self.size().width() / self.sceneRect().width()

        # get track width after scale applied
        trackWidthScaled = (1.0 / scaleFactor) * const.TimelineValues.TRACK_NAME_WIDGET_WIDTH

        # calculate negated width for scale
        widthScale = (self.sceneRect().width() - trackWidthScaled) / self.sceneRect().width()
        self.scale(scaleFactor * zoomLevel * widthScale, 1)

        zoomLevel = 1.0 / self.matrix().m11()
        const.CURRENT_ZOOM_LEVEL = zoomLevel

        self.zoomItems(zoomLevel=zoomLevel)

    def addTrack(self, namespaces=None):
        """Method ran when track is added"""
        self._parent.trackAdded.emit(namespaces)

    def removeTrack(self, trackTitle=None, namespaces=None):
        """Method ran when track is removed"""
        self._parent.trackRemoved.emit(trackTitle, namespaces)

    def horizontalSplitTrack(self):
        """Method ran when track is horizontally split"""
        self._parent.horizontalSplit.emit()

    def verticalSplitTrack(self):
        """Method ran when track is vertically split"""
        self._parent.verticalSplit.emit()

    def horizontalMergeTrack(self):
        """Method ran when track is horizontally merged"""
        self._parent.horizontalMerge.emit()

    def verticalMergeTrack(self):
        """Method ran when track is vertically merged"""
        self._parent.verticalMerge.emit()


class Timeline(QtGui.QWidget):
    """Main Timeline widget

    This is the main containing widget for the timeline. The timeline represents a timeslider, track items with markers,
    and editing buttons based on a framerange.

    The timeline is driven by a QAbstractModelItem and in order to visualize any data on this timeline it's required to
    use the setTimeline method with trackModel.TimelineDataItem. It will use the trackModel.TrackModel by default if no
    custom model is passed. Any model passed needs the two columns name and framerange of track. Examples of usage are
    at the bottom of this script.

    This widget also has a frameAll method that frames all the elements in view. Generally this should
    be added to the showEvent of your parented widget or whenever the data is refreshed.

    Signals:
        trackAdded: Emitted when add track button pressed
        trackRemoved: Emitted when remove track pressed
        horizontalSplit: Emitted when horizontal split track button pressed
        verticalSplit: Emitted when vertical split button pressed
        horizontalMerge: Emitted when horizontal merge button pressed
        verticalMerge: Emitted when vertical merge button pressed
    """

    trackAdded = QtCore.Signal(list)
    trackRemoved = QtCore.Signal(str, list)
    horizontalSplit = QtCore.Signal()
    verticalSplit = QtCore.Signal()
    horizontalMerge = QtCore.Signal()
    verticalMerge = QtCore.Signal()
    frameChanged = QtCore.Signal(int)

    def __init__(self, timelineRange, parent=None, defaultSetup=False):
        """Initializer

        Args:
            timelineRange (list of int): Start and end frame as a list
            parent (QtGui.QWidget): Parent of widget
            defaultSetup (bool): Whether or not to emit default callbacks on signals
        """
        super(Timeline, self).__init__(parent)
        self.timelineRange = timelineRange

        self.setupUi()

        if defaultSetup:
            self.trackAdded.connect(self.addTrack)
            self.trackRemoved.connect(self.removeTrack)
            self.horizontalSplit.connect(self.addTrack)
            self.verticalSplit.connect(self.splitTrack)
            self.horizontalMerge.connect(functools.partial(self.mergeTrack, False))
            self.verticalMerge.connect(functools.partial(self.mergeTrack, True))

        self._model = None

    def setupUi(self):
        """Setup ui"""
        self._view = SceneView(self.timelineRange, parent=self)
        self._scene = self._view.scene()

        self.layout = QtGui.QVBoxLayout()
        self.layout.addWidget(self._view)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def setLastSelected(self, item):
        """Stores last selected"""
        self._lastSelected = item

    def frameAll(self):
        """Frame all items in view"""
        self._view.frameAll()

    def currentFrame(self):
        """Grab current frame from scene view

        Returns:
            int: Frame currently in timeline
        """
        return self._view.currentFrame

    def updateFrame(self, frame):
        """Updates the frame"""
        self._scene.updateFrame(frame)

    def getItemsByNamespaces(self, title, namespaces):
        """Get indexes of items by namespaces and title

        Args:
            title (str): Title of item
            namespaces (list): namespaces to remove

        Returns:
            (list of QtCore.ModelIndex): modelIndexes in the model
        """
        trackItems = []
        for item in self._view.items():
            if isinstance(item, trackWidgets.TrackNameItem) and item.trackTitle == title:
                trackItems.append(item)

        modelIndexes = []
        for track in trackItems:
            for childIndex, childTitle in enumerate(track._children):
                if childTitle.text() in namespaces:
                    modelIndex = self._model.getModelIndexByRow(track.item._modelIndex.row(), childIndex=childIndex)
                    modelIndexes.append(modelIndex)

        return modelIndexes

    def getSelection(self):
        """Gets selected model indexes

        Returns:
            list of QModelIndex: Selected modelIndexes
        """
        selectedTracks = []
        for item in self._view.sortedItems:

            if isinstance(item, trackWidgets.TrackNameItem):
                modelIndex = self._model.getModelIndexByRow(item.item._modelIndex.row())
                selectedTracks.append(modelIndex)

            elif isinstance(item, trackWidgets.TrackChildText):
                childIndex = item._parent._children.index(item)
                modelIndex = self._model.getModelIndexByRow(item._parent.item._modelIndex.row(), childIndex=childIndex)
                selectedTracks.append(modelIndex)

        return selectedTracks

    def addTrack(self, namespaces=None):
        """Adds a track"""
        namespaces = [] if namespaces is None else namespaces
        self._model.addTrack(namespaces=namespaces)

    def removeTrack(self, trackTitle=None, namespaces=None):
        """Removes a track"""
        selection = self.getSelection()
        self._model.removeTrack(selection)

    def splitTrack(self):
        """Splits a track"""
        splitFrame = self.currentFrame()
        selection = self.getSelection()
        self._model.splitTrack(selection, splitFrame)

    def mergeTrack(self, verticalMerge=False):
        """Merges tracks

        Args:
            verticalMerge (bool): If True will only merge vertical splits
        """
        selection = self.getSelection()
        self._model.mergeTracks(selection, verticalMerge=verticalMerge)

    def refreshTimeline(self):
        """Refreshes timeline if using trackModel.TrackModel"""
        if isinstance(self._model, trackModel.TrackModel):
            tracks = []
            for row, track in enumerate(self._model.rootItem.childItems):
                tracks.append(
                    trackModelItem.TrackDataItem(
                        track.getRange(),
                        self.timelineRange,
                        trackTitle=track.getName(),
                        trackTitleChildren=track.getChildren(),
                        modelIndex=self._model.getModelIndexByRow(row, columnIndex=1),
                    )
                )

            tracks.sort(key=lambda trackItem: (trackItem.title, trackItem.timelineRange()))
            self.setTimeline(trackModelItem.TimelineDataItem(tracks=tracks), model=self._model)

    def setTimeline(self, timeline, model=None):
        """Adds tracks to the timeline

        Args:
            timeline (trackModelItem.TimelineDataItem): Timeline to set
            model (QtCore.QAbstractItemModel): Model to update if model not set it will use trackModel.TrackModel
        """
        self._scene.tracks = timeline.tracks
        self._view.zoomItems(const.CURRENT_ZOOM_LEVEL)

        if model:
            self._model = model
        else:
            self._model = trackModel.TrackModel(timelineRange=self.timelineRange)
            self._model.modelRefreshed.connect(self.refreshTimeline)


class MoveAssetMessageBox(QtGui.QMessageBox):
    """Message box when moving assets between splits"""

    def __init__(self, namespaces, parent=None):
        parent = Application.GetMainWindow() if not parent else parent
        super(MoveAssetMessageBox, self).__init__(parent)

        self.setWindowTitle("KEEP ASSET IN BOTH TRACKS?")
        self.setText("Would you like to keep these assets in both tracks?\n{0}".format("\n".join(namespaces)))
        copyButton = self.addButton("Yes", QtGui.QMessageBox.YesRole)
        moveButton = self.addButton("No", QtGui.QMessageBox.NoRole)
        self.setIcon(QtGui.QMessageBox.Warning)


# Example of usage
if __name__ == "__builtin__":

    # Set offsets incase it was unset
    const.TimelineValues.TRACK_NAME_WIDGET_WIDTH = 250.0
    const.TimelineValues.TOP_MARGIN = 60

    # make a dialog
    dialog = QtGui.QDialog(parent=Application.GetMainWindow())
    dialog.resize(800, 400)
    dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
    dialog.setWindowTitle("Test Timeline")

    # make a layout, remove margins
    layout = QtGui.QVBoxLayout()
    layout.setContentsMargins(0, 0, 0, 0)
    dialog.setLayout(layout)

    # make timeline
    timelineData = trackModelItem.TimelineDataItem()
    timeline = Timeline(Time.getSceneFrameRange(), parent=dialog, defaultSetup=True)
    timeline.setTimeline(timelineData)

    layout.addWidget(timeline)

    # show dialog and frame it to view
    dialog.show()
    timeline.frameAll()
