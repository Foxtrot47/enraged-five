'''
RS.Utils.Gallery

Author:

    Jason Hayes <jason.hayes@rockstarsandiego.com>
    
Description:

    Provides a gallery widget for viewing thumbnails of images.  This only works with interfaces built
    using PySide.
    
Example Usage:

    import RS.Utils.Qt.Gallery

    class Main( QtGui.QWidget ):
        def __init__( self, *args, **kwargs ):
            QtGui.QWidget.__init__( self, *args, **kwargs )
            
            # Setup style flags.  This first one will display the controls to change thumbnail sizes.
            styleFlags = RS.Utils.Qt.Gallery.Style.ThumbnailSizes
            
            # Render item labels.
            styleFlags |= RS.Utils.Qt.Gallery.Style.ItemLabels
            
            # Render each gallery item with rounded corners instead of square.
            styleFlags |= RS.Utils.Qt.Gallery.Style.RoundedItems
            
            # Create the gallery widget.
            self.__gallery = RS.Utils.Qt.Gallery.Gallery( style = styleFlags )
            
            # Change background color to red.
            self.__gallery.SetBackgroundColor( QtGui.QColor( 255, 0, 0 ) )
            
            # Add items to the gallery.
            imageFilenames = os.listdir( 'path/to/images' )
            
            for imageFilename in imageFilenames:
            
                # The created gallery item will be returned.
                item = self.__gallery.AddItem( imageFilename )
'''

import os

from PySide import QtCore, QtGui

import RS.Config


class Style( object ):
    '''
    Style flags for the gallery widget.
    '''
    
    # Whether the thumbnail items will be rounded or square.
    RoundedItems    = 1
    
    # Display controls to change between large, medium and small thumbnail sizes.
    ThumbnailSizes  = 2
    
    # Display item labels.
    ItemLabels      = 4
    
class GalleryItem( QtGui.QGraphicsItem ):
    '''
    Represents an individual item in the gallery.
    
    Arguments:
    
        imageFilename: Path to an image to display.
        size: The size of the item.  Must supply a QtCore.QSize object.
        customData: Any custom Python object to attach to the item.
        parent: The item's parent widget.
        
    Keyword Arguments:
    
        style = Style flags for how the item renders and behaves.  For example:
        
            # Render the item label, and make the thumbnail have rounded corners.
            style = Gallery.Style.ItemLabels | Gallery.Style.RoundedItems
    '''
    def __init__( self, imageFilename, size, customData, parent, style = Style.ItemLabels | Style.RoundedItems ):
        QtGui.QGraphicsItem.__init__( self )
        
        assert ( isinstance( size, QtCore.QSize ) ), 'Must supply a QtCore.QSize() object for the size!'
        
        self.setFlag( QtGui.QGraphicsItem.ItemIsSelectable, True )
        self.setAcceptHoverEvents( True )
        
        self.__parent = parent
        self.__customData = customData
        self.__styleFlags = style
        
        self.__imageFilename = imageFilename
        self.__image = None
        
        self.__name = os.path.basename( imageFilename ).split( '.' )[ 0 ]
        
        self.__selectedPen = QtGui.QPen( QtGui.QColor( 255, 255, 255 ) )
        self.__selectedPen.setWidth( 1 )
        
        self.__nonSelectedPen = QtGui.QPen( QtGui.QColor( 50, 50, 50, 0 ) )
        self.__nonSelectedPen.setWidth( 1 )
        
        self.__thumbBuffer = 6
        
        self.__font = QtGui.QFont( 'Calibri', 8 )
        self.__fontColor = QtGui.QColor( 220, 220, 220 )
        
        self.Resize( size )
    
      
    ## Properties ##
    
    @property
    def Name( self ):
        return self.__name
    
    @property
    def ImageFilename( self ):
        return self.__imageFilename
    
    @property
    def Image( self ):
        return self.__image
    
    @property
    def Data( self ):
        return self.__customData
        
    
    ## Methods ##
    
    def SetFont( self, font ):
        assert ( isinstance( newSize, QtGui.QFont ) ), 'Must supply a QtGui.QFont() object for the font!'
        
        self.__font = font
    
    def Resize( self, newSize ):
        assert ( isinstance( newSize, QtCore.QSize ) ), 'Must supply a QtCore.QSize() object for the size!'
        
        self.__size = newSize
        self.__boundingRect = QtCore.QRect( 0, 0, newSize.width(), newSize.height() )

    def SetThumbnail( self, imageFilename ):
        self.__imageFilename = imageFilename
        self.__image = QtGui.QPixmap( imageFilename )
        
        self.update()
    
    def SetData( self, customData ):
        self.__customData = customData
        
    def SetFontColor( self, color ):
        assert ( isinstance( newSize, QtGui.QColor ) ), 'Must supply a QtGui.QColor() object for the color!'
        
        self.__fontColor = color
        
    
    ## Overrides ##
        
    def boundingRect( self ):
        return self.__boundingRect
        
    def paint( self, painter, option, widget ):
        
        # Setup pen for item selection.
        if self.isSelected():
            painter.setPen( self.__selectedPen )
            
        else:
            painter.setPen( self.__nonSelectedPen )
            
        # Setup item background gradient.
        bgGradient = QtGui.QLinearGradient( QtCore.QPointF( 0, 0 ), QtCore.QPointF( 0, self.__boundingRect.height() ) )
        bgGradient.setColorAt( 0, QtGui.QColor( 70, 70, 70 ) )
        bgGradient.setColorAt( 1, QtGui.QColor( 60, 60, 60 ) )
        painter.setBrush( bgGradient ) 
        
        # Draw item background.
        if self.__styleFlags & Style.RoundedItems:
            painter.drawRoundedRect( self.__boundingRect, 5, 5 )
            
        else:
            painter.drawRect( self.__boundingRect )
            
        # Draw theumbnail image.
        painter.drawPixmap( self.__thumbBuffer, self.__thumbBuffer, self.__size.width() - ( self.__thumbBuffer * 2 ), self.__size.height() - ( self.__thumbBuffer * 2 ), self.__image )
        
        # Draw item label.
        if self.__styleFlags & Style.ItemLabels:
            painter.setFont( self.__font )
            
            fontMetrics = QtGui.QFontMetrics( self.__font )
            labelWidth = fontMetrics.width( self.__name )
            
            painter.setBrush( QtGui.QBrush( QtGui.QColor( 0, 0, 0 ) ) )
            painter.setPen( QtGui.QPen( self.__fontColor ) )
            painter.drawText( self.__boundingRect.center().x() - ( labelWidth / 2.0 ), self.__boundingRect.height(), labelWidth, 24, QtCore.Qt.AlignCenter, self.__name )
        
class Gallery( QtGui.QWidget ):
    '''
    Gallery widget for displaying multiple images as thumbnails.
    
    Keyword Arguments:
    
        style = Flags for how the gallery widget renders and behaves.  Example:
        
            # The gallery should display the thumbnail size controls, render items with rounded corners and labels.
            style = Gallery.Style.RoundedItems | Gallery.Style.ThumbnailSizes | Gallery.Style.ItemLabels
    '''
    def __init__( self, style = Style.RoundedItems | Style.ThumbnailSizes | Style.ItemLabels ):
        QtGui.QWidget.__init__( self )
        
        mainLayout = QtGui.QVBoxLayout()
        self.setLayout( mainLayout )
        
        self.__styleFlags = style
        self.__bgColor = QtGui.QColor( 100, 100, 100 )
        
        self.__scene = QtGui.QGraphicsScene()
        
        self.__view = QtGui.QGraphicsView( self.__scene, self )
        self.__view.setRenderHint( QtGui.QPainter.Antialiasing, True )
        self.__view.setViewportUpdateMode( QtGui.QGraphicsView.FullViewportUpdate )
        self.__view.setHorizontalScrollBarPolicy( QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff )
        self.__view.setDragMode( QtGui.QGraphicsView.RubberBandDrag )
        self.__view.setBackgroundBrush( QtGui.QBrush( self.__bgColor ) )
        
        # Gallery items.
        self.__items = []
        
        # Thumbnail sizes.
        self.__smallThumbnailSize = QtCore.QSize( 96, 96 )
        self.__mediumThumbnailSize = QtCore.QSize( 160, 160 )
        self.__largeThumbnailSize = QtCore.QSize( 256, 256 )
        
        # Current thumbnail size.
        self.__thumbnailSize = self.__mediumThumbnailSize

        # Change gallery thumbnail size.
        thumbnailCtrlLayout = QtGui.QHBoxLayout()
                              
        largeThumbnailIcon = QtGui.QIcon()
        largeThumbnailIcon.addPixmap( QtGui.QPixmap( "{0}/largeThumbnail.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.On )                              
        
        mediumThumbnailIcon = QtGui.QIcon()
        mediumThumbnailIcon.addPixmap( QtGui.QPixmap( "{0}/mediumThumbnail.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.On )                              

        smallThumbnailIcon = QtGui.QIcon()
        smallThumbnailIcon.addPixmap( QtGui.QPixmap( "{0}/smallThumbnail.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.On )                              
                                
        self.__btnLargeThumbnail = QtGui.QPushButton( largeThumbnailIcon, '' )
        self.__btnLargeThumbnail.setIconSize( QtCore.QSize( 16, 16 ) )
        
        self.__btnMediumThumbnail = QtGui.QPushButton( mediumThumbnailIcon, '' )
        self.__btnMediumThumbnail.setIconSize( QtCore.QSize( 16, 16 ) )
        
        self.__btnSmallThumbnail = QtGui.QPushButton( smallThumbnailIcon, '' )
        self.__btnSmallThumbnail.setIconSize( QtCore.QSize( 16, 16 ) )
        
        # Add controls to layout.        
        thumbnailCtrlLayout.addStretch()
        thumbnailCtrlLayout.addWidget( self.__btnSmallThumbnail )
        thumbnailCtrlLayout.addWidget( self.__btnMediumThumbnail )
        thumbnailCtrlLayout.addWidget( self.__btnLargeThumbnail )
        
        # Bind events.
        self.__btnLargeThumbnail.pressed.connect( self.OnChangeLargeThumbnailSize )
        self.__btnMediumThumbnail.pressed.connect( self.OnChangeMediumThumbnailSize )
        self.__btnSmallThumbnail.pressed.connect( self.OnChangeSmallThumbnailSize )          
        
        mainLayout.addLayout( thumbnailCtrlLayout )
        mainLayout.addWidget( self.__view )
        
        if not style & Style.ThumbnailSizes:
            self.__btnSmallThumbnail.hide()
            self.__btnMediumThumbnail.hide()
            self.__btnLargeThumbnail.hide()
        
    
    ## Properties ##
    
    @property
    def Items( self ):
        return self.__items
    
    @property
    def View( self ):
        return self.__view
    
    @property
    def Scene( self ):
        return self.__scene
    
        
    ## Methods ##
    
    def SetBackgroundColor( self, color ):
        assert ( isinstance( color, QtGui.QColor ) ), 'Must supply a QtGui.QColor() object for the size!'
        
        self.__bgColor = color
        self.__view.setBackgroundBrush( QtGui.QBrush( self.__bgColor ) )
    
    def GetSelectedItems( self ):
        items = []
        
        for item in self.__items:
            if item.isSelected():
                items.append( item )
                
        return items
    
    def SetSmallThumbnailSize( self, size ):
        assert ( isinstance( size, QtCore.QSize ) ), 'Must supply a QtCore.QSize() object for the size!'
        
        self.__smallThumbnailSize = size
        self.OnChangeSmallThumbnailSize()       
        
    def SetMediumThumbnailSize( self, size ):
        assert ( isinstance( size, QtCore.QSize ) ), 'Must supply a QtCore.QSize() object for the size!'
        
        self.__mediumThumbnailSize = size
        self.OnChangeMediumThumbnailSize()       
        
    def SetLargeThumbnailSize( self, size ):
        assert ( isinstance( size, QtCore.QSize ) ), 'Must supply a QtCore.QSize() object for the size!'
        
        self.__largeThumbnailSize = size
        self.OnChangeLargeThumbnailSize()       
    
    def ResizeThumbnails( self, newSize ):
        assert ( isinstance( newSize, QtCore.QSize ) ), 'Must supply a QtCore.QSize() object for the size!'
        
        self.__thumbnailSize = newSize
        
        for item in self.__items:
            self.__scene.removeItem( item )
            
        for item in self.__items:
            item.Resize( newSize )
            self.__scene.addItem( item )
    
    def ResizeGallery( self ):
        thumbBuffer = 24
        
        thumbWidth = self.__thumbnailSize.width() + thumbBuffer
        thumbHeight = self.__thumbnailSize.height() + thumbBuffer
        
        x = 10
        y = 10
        
        for item in self.__items:
            if item.isVisible():
                futureX = x + thumbWidth
                
                if futureX >= self.width():
                    x = 10
                    y += thumbHeight
                
                item.setPos( x, y )
                x += thumbWidth
             
        if y < self.height():
            y = self.height()
   
        self.__view.setSceneRect( QtCore.QRect( 0, 0, self.width(), y + thumbHeight ) )
        
    def AddItem( self, imageFilename, customData = None ):
        thumbnailItem = GalleryItem( imageFilename, self.__thumbnailSize, customData, self, style = self.__styleFlags )
        thumbnailItem.SetThumbnail( imageFilename )
        
        self.__items.append( thumbnailItem )
        self.__scene.addItem( thumbnailItem )
        
        self.ResizeGallery()
        
        return thumbnailItem
        
    def RemoveItem( self, imageFilename ):
        itemToRemove = None
        
        for item in self.__items:
            if item.ImageFilename == imageFilename:
                itemToRemove = item
                break
               
        if itemToRemove: 
            self.__scene.removeItem( item )
            self.__items.remove( item )  
            
            self.ResizeGallery()
    
    
    ## Events ##
    
    def OnChangeLargeThumbnailSize( self ):
        self.__thumbnailSize = self.__largeThumbnailSize
        self.ResizeThumbnails( self.__thumbnailSize )
        self.ResizeGallery()
            
    def OnChangeMediumThumbnailSize( self ):
        self.__thumbnailSize = self.__mediumThumbnailSize
        self.ResizeThumbnails( self.__thumbnailSize )
        self.ResizeGallery()  

    def OnChangeSmallThumbnailSize( self ):
        self.__thumbnailSize = self.__smallThumbnailSize
        self.ResizeThumbnails( self.__thumbnailSize )
        self.ResizeGallery()      
    
    
    ## Overrides ##
    
    def resizeEvent( self, event ):
        self.ResizeGallery()
        
        QtGui.QWidget.resizeEvent( self, event )