import functools
import pyfbsdk as mobu

from PySide import QtCore, QtGui

from RS import Globals
from RS.Utils import Scene


class ModelProcessingActive(object):
    """
        Important version which can prevent callback triggered by selection change
    """
    def __init__(self):
        """
        Constructor
        """
        pass

    def __enter__(self):
        """
        method called by the with statement
        """
        mobu.FBBeginChangeAllModels()

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        mobu.FBEndChangeAllModels()


class ModelTransactionActive(object):
    """
        Inspired from from RS.Utils.Scene.UndoBlock()
    """
    def __init__(self):
        """
        Constructor
        """
        pass

    def __enter__(self):
        """
        method called by the with statement
        """
        mobu.FBModelTransactionBegin()

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        mobu.FBModelTransactionEnd()


class AbstractOnOffContext(object):
    """
    Abstract context providing functionality for:
    - Once within a context, the successive context/decorations will be ignored until the end of the first context.
        You can check if the context is still running by calling AbstractOnOffContext.isOn()
    - The effect of the context can be forcefully deactivated with another context, by calling it like this
            with AbstractOnOffContext(value=False):
    - Support for running the context as a decorator
    - Deactivating all contexts deriving from this context class, by calling AbstractOnOffContext.setEnabled(False).
        When a context is not enabled, any calls to it will just do nothing.

    """
    _ACTIVE = False
    _ENABLED = True

    def __init__(self, value=True):
        """

        Args:
            value (bool): if True, the context will be turned on on __enter__ and turned off on __exit__
                          If False, the context will be turned off on __enter__ and turned back on on __exit__.
                          False only works if the context was previously activated.
        """
        self._value = value
        # the executed variable holds if this context was ever able to execute or not.
        self._executed = False

    @classmethod
    def enabled(cls):
        """
        Returns:
            bool: if the context is enabled or not. While the context is disabled, it will never turn on.

        """
        return cls._ENABLED

    @classmethod
    def setEnabled(cls, value):
        """
        Enable or Disable the context.
        This is an override that can be used to make sure none of the instances of this context will do anything.
        While the context is disabled, it will never turn on.
        Args:
            value (bool):
        """
        cls._ENABLED = value

    @classmethod
    def isOn(cls):
        """
        By default, the internal ACTIVE variable is set to True or False when turnOn or turnOff are called.
        This is then used to determine if the context is running or not.
        This can be overridden, if for example you want to specify an external method that defines if the context
        is running or not.
        Returns:
            bool: if the operation of this context has been run or not.

        """
        return cls._ACTIVE

    def turnOn(self):
        """
        Public method to turn on the context.
        Subclasses will override this to implement what the context should do when is activated (turned on).
        Returns:
            bool: if the context was successfully turned on.

        """
        raise NotImplementedError

    def turnOff(self):
        """
        Public method to turn off the context.
        Subclasses will override this to implement what the context should do on shutdown.
        Returns:
            bool: if the context was successfully ended.

        """
        raise NotImplementedError

    def __call__(self, func):
        """
        Overrides built-in method
        Implementation for Decorator behaviour.

        Arguments:
            func (function): the decorated function

        Return:
            func
        """

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            with self:
                results = func(*args, **kwargs)
            return results

        return wrapper

    def _turnOn(self):
        """
        Internal method to turn on the context.
        This is called internally during __enter__ or __exit__, and makes sure the context is not turned on if it
        was already running.
        Returns:
            bool: if the operation was successful (if so, isOn() should return True)
        """
        if not self.isOn():
            if self.turnOn():
                self.__class__._ACTIVE = True
        return self.isOn()

    def _turnOff(self):
        """
        Internal method to turn off the context.
        This is called internally during __enter__ or __exit__, and makes sure the context is not turned off unless it
        is already active.
        Returns:
            bool: if the operation was successful (if so, isOn() should return False)
        """
        if self.isOn():
            if self.turnOff():
                self.__class__._ACTIVE = False
        return not self.isOn()

    def __enter__(self):
        """
        method called by the with statement
        """
        if self.enabled():
            if not self.isOn() and self._value:
                self._executed = self._turnOn()
            elif self.isOn() and not self._value:
                self._executed = self._turnOff()
        return self

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        if self.enabled():
            if self._executed:
                if self._value:
                    self._turnOff()
                else:
                    self._turnOn()
                self._executed = False


class AbstractTransaction(AbstractOnOffContext):
    """
    Abstract Transaction class for disabling all context managers that inherit this class
    """


class PreventUIUpdateActive(AbstractTransaction):
    """
        This is used for actions that do not require merges, such as looping through components to delete.

        For operations that require a merge, use the MergeTransaction context manager.

        This context manager will do nothing if it is used under MergeTransaction Context Manager.
        Can only be used for CC4 +. Script will return if the function doesnt exist for the user
        (i.e. they're on an older CC)
    """

    def turnOn(self):
        if not MergeTransactionActive.isOn():
            mobu.FBPreventUIUpdateBegin()
            return True
        return False

    def turnOff(self):
        mobu.FBPreventUIUpdateEnd()
        return True


class MergeTransactionActive(AbstractTransaction):
    """
        Stops the UI from updating while performing operations that adds, removes or updates components in the scene.
        Only meant to be used during merge actions.

        Use PreventUIUpdateActive, for other actions (such as looping through components to delete) if a merge action
        is not involved.
    """

    @classmethod
    def isOn(cls):
        return mobu.FBMergeTransactionIsOn()

    def turnOn(self):
        mobu.FBMergeTransactionBegin()
        return True

    def turnOff(self):
        mobu.FBMergeTransactionEnd()
        return True


class SceneConstraintsDisabled(AbstractOnOffContext):
    """
        Disables all constraints in the scene for the duration of a context
        Re-enables them afterwards
    """

    Constraints = {}

    def __init__(self, constraints=None, value=True, characters=True):
        """

        Args:
            constraints (list of mobu.FBConstraint or None): if specified, only operate on the given namespace.
            value (bool): if True, the context will be turned on on __enter__ and turned off on __exit__
              If False, the context will be turned off on __enter__ and turned back on on __exit__.
              False only works if the context was previously activated.
        """
        super(SceneConstraintsDisabled, self).__init__(value=value)
        self._constraints = constraints
        self._characters = characters

    def turnOn(self):
        cls = self.__class__
        constraints = self._constraints or Globals.Constraints
        for constraint in constraints:
            if not self._characters and isinstance(constraint, mobu.FBCharacter):
                continue
            cls.Constraints[constraint] = constraint.Active
            constraint.Active = False
        return True

    def turnOff(self):
        cls = self.__class__
        with SuspendMessages():
            for constraint, value in cls.Constraints.items():
                try:
                    constraint.Active = value
                except:
                    pass
        cls.Constraints = {}
        return True


class SceneExpressionsDisabled(SceneConstraintsDisabled):
    """
        Disables all RS Expression and devices in the scene for the duration of a context
        Re-enables them afterwards
    """
    Constraints = {}
    Devices = {}

    def turnOn(self):
        cls = self.__class__
        constraints = self._constraints or Globals.Constraints
        for constraint in constraints:
            if constraint not in cls.Constraints and constraint.ClassName() == "RSExprSolverConstraint":
                cls.Constraints[constraint] = constraint.Active
                constraint.Active = False

        for device in Globals.Devices:
            if device not in cls.Devices and device.ClassName() == "RSExprSolverDevice":
                cls.Devices[device] = device.Online
                device.Online = False
        return True

    def turnOff(self):
        cls = self.__class__
        with SuspendMessages():
            super(SceneExpressionsDisabled, self).turnOff()
            for device, value in cls.Devices.items():
                try:
                    device.Online = value
                except:
                    pass
        cls.Devices = {}
        return True


class SceneAnimScriptsDisabled(SceneConstraintsDisabled):
    """
        Disables all RS AnimScript constraints in the scene for the duration of a context
        Re-enables them afterwards
    """
    Constraints = {}

    def turnOn(self):
        cls = self.__class__
        constraints = self._constraints or Globals.Constraints
        for constraint in constraints:
            if constraint not in cls.Constraints:
                if constraint.ClassName() == "AnimScriptSolverConstraint":
                    cls.Constraints[constraint] = constraint.Active
                    constraint.Active = False
        return True


class RestorePickingMode(AbstractOnOffContext):
    """
        Makes sure the PickingMode of the Viewport is maintained during the context.
        Notes:
            This internally calls "FBScene.Evaluate". Make sure to not call this within contexts that might crash
            if the scene is forcefully evaluated when they run (MergeTransaction for example).
    """
    STATE = None

    def turnOn(self):
        cls = self.__class__
        renderer = Globals.Scene.Renderer
        selectedIndex = renderer.GetSelectedPaneIndex()
        renderer.SetSelectedPaneIndex(0)
        cls.STATE = renderer.GetViewingOptions().PickingMode
        renderer.SetSelectedPaneIndex(selectedIndex)
        return True

    def turnOff(self):
        cls = self.__class__
        if cls.STATE:
            Scene.SetPickingMode(cls.STATE)

        cls.STATE = None
        return True


class RestoreSelection(AbstractOnOffContext):
    """
        Makes sure the selection is restored at the end of the context.
    """
    STATE = {}

    def turnOn(self):
        cls = self.__class__
        modelList = mobu.FBModelList()
        mobu.FBGetSelectedModels(modelList)
        for component in modelList:
            cls.STATE[component.FullName] = True

        return True

    def turnOff(self):
        cls = self.__class__
        modelList = mobu.FBModelList()
        mobu.FBGetSelectedModels(modelList)
        for component in modelList:
            if component.FullName not in cls.STATE.keys():
                component.Selected = False
        for fullName, value in cls.STATE.items():
            component = mobu.FBFindObjectByFullName(fullName)
            if component is not None:
                component.Selected = value

        cls.STATE = {}
        return True


class WaitCursor(AbstractOnOffContext):
    """
    Setup a WaitCursor for the duration of the context;

    Notes:
        This context calls ProcessEvents, so make sure to use it with caution to prevent crashes.
    """

    def turnOn(self):
        QtGui.QApplication.instance().setOverrideCursor(QtCore.Qt.WaitCursor)
        QtGui.QApplication.instance().processEvents()
        return True

    def turnOff(self):
        QtGui.QApplication.instance().restoreOverrideCursor()
        return True


class Undo(object):
    """
        Inspired from from RS.Utils.Scene.UndoBlock()
        Add require options to fully support undo.
        Captures motion builder code into one undo call.

        Support TRS restore.
        Values restore.

        Args:
            undoStackName (str): internal undo name
            modelList (list of FBModel): models which can be used to undo TRS tweak.
            attributeArray (list of FBProperty): property which can be restored.
    """
    def __init__(self,
                 undoStackName='',
                 modelList=None,
                 attributeArray=None,
                 destructionArray=None):
        """
        Constructor

        Args:
            name (string): name of the undo transaction
        """
        self.__name = undoStackName
        self.__modelList = modelList
        self.__attributeArray = attributeArray
        self.__destructionArray = destructionArray

    def __enter__(self):
        """
        method called by the with statement

        Returns:
            pyfbsdk.FBUndoManager
        """
        self.undoManager = mobu.FBUndoManager()
        self.undoManager.TransactionBegin(self.__name)

        if self.__destructionArray is not None:
            for model in self.__destructionArray:
                if model != None:
                    self.undoManager.TransactionAddObjectDestroy(model)

        if self.__modelList is not None:
            for model in self.__modelList:
                if model != None:
                    self.undoManager.TransactionAddModelTRS(model)

        if self.__attributeArray is not None:
            for property in self.__attributeArray:
                if property != None:
                    if isinstance(property, mobu.FBProperty) is True:
                        self.undoManager.TransactionAddProperty(property)

        return self.undoManager

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        self.undoManager.TransactionEnd()


class SuspendMessages(object):
    """
        Inspired from from RS.Utils.Scene.UndoBlock()
    """

    def __enter__(self):
        """
        method called by the with statement
        """
        self._value = Globals.System.SuspendMessageBoxes
        Globals.System.SuspendMessageBoxes = True

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        Globals.System.SuspendMessageBoxes = self._value


class ClearModelSelection(object):
    """
    Clears the selection in Motion Builder for the duration of the code block
    """

    def __init__(self, ignoreList=()):
        """
        Constructor

        Arguments:
            ignoreList (list): list of FBModels to avoid reseting their selection
        """
        self._ignoreList = ignoreList
        self._selection = {}

    def __enter__(self):
        """
        method called by the with statement
        """
        modelList = mobu.FBModelList()
        mobu.FBGetSelectedModels(modelList)
        for component in modelList:
            if component not in self._ignoreList:
                self._selection[component] = True
                component.Selected = False

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block
        Restores the selection to how it was at the start of the block

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        for component, selected in self._selection.iteritems():
            component.Selected = selected


class SuspendCallbacks(object):
    """ Disables all callbacks managed by the callback manager """
    Count = 0

    def __enter__(self):
        """
        method called by the with statement
        """
        if not SuspendCallbacks.Count:
            self._value = Globals.Callbacks.enabled
            Globals.Callbacks.enabled = False
        SuspendCallbacks.Count += 1

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        SuspendCallbacks.Count -= 1
        if not SuspendCallbacks.Count:
            Globals.Callbacks.enabled = self._value


class SuspendSceneParallelism(object):
    """
    Disables the Parallel options of the Scene along with GPU Skinning if they are enabled
    """
    Count = 0

    @classmethod
    def isOn(cls):
        return cls.Count != 0

    def __enter__(self):
        """
        method called by the with statement
        """
        if not SuspendSceneParallelism.isOn():
            self._pipeline = Globals.EvaluateManager.ParallelPipeline
            self._evaluation = Globals.EvaluateManager.ParallelEvaluation
            self._deformation = Globals.EvaluateManager.ParallelDeformation
            self._gpuDeformation = Globals.EvaluateManager.UseGPUDeformation

            Globals.EvaluateManager.ParallelPipeline = False
            Globals.EvaluateManager.ParallelEvaluation = False
            Globals.EvaluateManager.ParallelDeformation = False
            Globals.EvaluateManager.UseGPUDeformation = False

            Globals.Scene.Evaluate()

        SuspendSceneParallelism.Count += 1

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block

        Arguments:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        SuspendSceneParallelism.Count -= 1
        if not SuspendSceneParallelism.isOn():
            Globals.EvaluateManager.ParallelPipeline = self._pipeline
            Globals.EvaluateManager.ParallelEvaluation = self._evaluation
            Globals.EvaluateManager.ParallelDeformation = self._deformation
            Globals.EvaluateManager.UseGPUDeformation = self._gpuDeformation
            Globals.Scene.Evaluate()


class UnlockLayers(object):
    """Unlocks animation layers and returns them back"""

    def __init__(self, ignoreList=None):
        """Constructor

        Args:
            ignoreList (list): list of layer names to ignore
        """
        self._ignoreList = ignoreList if ignoreList else ()
        self._layers = {}

    def __enter__(self):
        """Enter method stores current takes layers lock states and turns them off"""
        take = Globals.System.CurrentTake

        for idx in range(take.GetLayerCount()):
            layer = take.GetLayer(idx)
            layerName = layer.Name

            if layerName not in self._ignoreList:
                self._layers[layerName] = layer.Lock
                layer.Lock = False

    def __exit__(self, exceptionType, value, traceback):
        """Exit method reverts layer lock state

        Args:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        take = Globals.System.CurrentTake

        for layerName, lockState in self._layers.iteritems():
            layer = take.GetLayerByName(layerName)
            layer.Lock = lockState
