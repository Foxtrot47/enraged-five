import os
import sys
import imp
import modulefinder
import inspect
import webbrowser

import RS.Config
import RS.Utils.Path
import RS.Utils.Html
import RS.Perforce

reload( RS.Utils.Html )


## Enums ##

class ItemType:
    CLASS           = 1
    CLASS_METHOD    = 2
    CLASS_PROPERTY  = 3
    FUNCTION        = 4
    IMPORT          = 5

class ErrorType:
    
    # Item does not match Pascal Case.
    PASCAL_CASE         = 1
    
    # Item is a core module that's importing from an invalid location.
    CORE_LIB_IMPORT     = 2
    
    # Item has no doc string.
    NO_DOC_STRING       = 3
    
    # Item is using os.environ to query a project setting.
    USING_OS_ENVIRON    = 4
    
    
## Constants ##

ITEM_TYPE_COLORS = { ItemType.CLASS             : RS.Utils.Html.Color.Thistle,
                     ItemType.CLASS_METHOD      : RS.Utils.Html.Color.AntiqueWhite,
                     ItemType.CLASS_PROPERTY    : RS.Utils.Html.Color.PaleGreen,
                     ItemType.FUNCTION          : RS.Utils.Html.Color.Azure,
                     ItemType.IMPORT            : RS.Utils.Html.Color.MistyRose }
    

## Classes ##

class ReportItem( object ):
    def __init__( self, name, moduleName, itemType, errorType, lineNumber = None ):
        self.__name = name
        self.__moduleName = moduleName
        self.__itemType = itemType
        self.__errorType = errorType
        self.__lineNumber = lineNumber
        
    @property
    def Name( self ):
        return self.__name
    
    @property
    def ModuleName( self ):
        return self.__moduleName
    
    @property
    def ItemType( self ):
        return self.__itemType
    
    @property
    def LineNumber( self ):
        return self.__lineNumber
    
    @property
    def ItemTypeAsString( self ):
        if self.__itemType == ItemType.CLASS:
            return 'Class'
        
        if self.__itemType == ItemType.CLASS_METHOD:
            return 'Class Method'
        
        if self.__itemType == ItemType.CLASS_PROPERTY:
            return 'Class Property'
        
        if self.__itemType == ItemType.FUNCTION:
            return 'Function'
        
        if self.__itemType == ItemType.IMPORT:
            return 'Import'
    
    @property
    def ErrorType( self ):
        return self.__errorType

class CodingStandardsReport( object ):
    def __init__( self ):
        
        # Dictionary of report items.  Data is stored as follows:
        #
        #   { 'Module.Name' : { ErrorType : [ ReportItem objects ] } }
        #
        # For each module, there is an associated dictionary where the key is the error type, accompanied by a 
        # list of the offending items.
        #
        self.__errors = {}
        
        # Running totals of items.
        self.__totalCoreImportErrors = 0
        self.__totalPascalCaseErrors = 0
        self.__totalNoDocStringErrors = 0
        
    def Clear( self ):
        self.__errors = {}
                
        # Running totals of items.
        self.__totalCoreImportErrors = 0
        self.__totalPascalCaseErrors = 0
        self.__totalNoDocStringErrors = 0        
    
    @property
    def TotalCoreImportErrors( self ):
        return self.__totalCoreImportErrors
    
    @property
    def TotalPascalCaseErrors( self ):
        return self.__totalPascalCaseErrors
    
    @property
    def TotalNoDocStringErrors( self ):
        return self.__totalNoDocStringErrors

    
    ## Methods ##
    
    def GetItemsByErrorType( self, moduleName, errorType ):
        if moduleName in self.__errors:
            if errorType in self.__errors[ moduleName ]:
                return self.__errors[ moduleName ][ errorType ]
            
        return []
    
    def GetAllItemsByErrorType( self, errorType ):
        items = {}
        
        for modName, data in self.__errors.iteritems():
            errors = self.GetItemsByErrorType( modName, errorType )
            
            if errors:
                for error in errors:
                    if error.ModuleName not in items:
                        items[ error.ModuleName ] = error
            
        return items
               
    def AddItem( self, reportItem ):
        
        # Increment internal totals.
        if reportItem.ErrorType == ErrorType.CORE_LIB_IMPORT:
            self.__totalCoreImportErrors += 1
            
        if reportItem.ErrorType == ErrorType.PASCAL_CASE:
            self.__totalPascalCaseErrors += 1
            
        if reportItem.ErrorType == ErrorType.NO_DOC_STRING:
            self.__totalNoDocStringErrors += 1
        
        # Store the report item.
        if reportItem.ModuleName not in self.__errors:
            self.__errors[ reportItem.ModuleName ] = { reportItem.ErrorType : [ reportItem ] }
            
        else:
            if reportItem.ErrorType not in self.__errors[ reportItem.ModuleName ]:
                self.__errors[ reportItem.ModuleName ][ reportItem.ErrorType ] = [ reportItem ]
                
            else:
                self.__errors[ reportItem.ModuleName ][ reportItem.ErrorType ].append( reportItem )
       
    def GetItemTypeTableCellColor( self, itemType ):
        global ITEM_TYPE_COLORS
        
        if itemType in ITEM_TYPE_COLORS:
            return ITEM_TYPE_COLORS[ itemType ]
        
        else:
            return RS.Utils.Html.Color.White
       
    def _WriteModuleReports( self, outputDir ):
        if not os.path.isdir( outputDir ):
            os.makedirs( outputDir )
            
        for moduleName, data in self.__errors.iteritems():
            doc = RS.Utils.Html.Document( '{0}\\{1}.html'.format( outputDir, moduleName ), 'Python Coding Standards Report - {0}'.format( moduleName ) )
            
            doc.AddLine( 'Back to Index', RS.Utils.Html.FontStyle( fontSize = 2, hyperLink = '{0}\\..\\index.html'.format( outputDir ) ) )
            doc.AddHeader( 'Python Coding Standards Report - {0}'.format( moduleName ), RS.Utils.Html.FontStyle( fontSize = 5 ), RS.Utils.Html.HeaderStyle.H1 )
            
            tableHeaderFontStyle = RS.Utils.Html.FontStyle( foregroundColor = 'white' )
            tableHeaderCellStyle = RS.Utils.Html.TableCellStyle( tableHeaderFontStyle, backgroundColor = 'gray' )
            
            fontStyle = RS.Utils.Html.FontStyle()
            whiteFontStyle = RS.Utils.Html.FontStyle( foregroundColor = RS.Utils.Html.Color.White )
            
            tableCellStyle = RS.Utils.Html.TableCellStyle( fontStyle )
            passTableCellStyle = RS.Utils.Html.TableCellStyle( fontStyle, backgroundColor = RS.Utils.Html.Color.GreenYellow )
            errorTableCellStyle = RS.Utils.Html.TableCellStyle( whiteFontStyle, backgroundColor = '#CC3333' )
            
            # Module Statistics
            numPascalCaseErrors = len( self.GetItemsByErrorType( moduleName, ErrorType.PASCAL_CASE ) )
            numCoreLibImportErrors = len( self.GetItemsByErrorType( moduleName, ErrorType.CORE_LIB_IMPORT ) )
            numNoDocErrors = len( self.GetItemsByErrorType( moduleName, ErrorType.NO_DOC_STRING ) )
            
            statisticsTable = RS.Utils.Html.Table( numColumns = 2, width = 75, widthStyle = RS.Utils.Html.WidthStyle.Percentage )
            statisticsTable.AddItem( 'Type', tableHeaderCellStyle )
            statisticsTable.AddItem( 'Total Offending Items', tableHeaderCellStyle )
            
            statisticsTable.AddItem( 'Pascal Case', tableCellStyle )
            statisticsTable.AddItem( numPascalCaseErrors, passTableCellStyle if numPascalCaseErrors == 0 else errorTableCellStyle )
                        
            statisticsTable.AddItem( 'Core Library Import', tableCellStyle )
            statisticsTable.AddItem( numCoreLibImportErrors, passTableCellStyle if numCoreLibImportErrors == 0 else errorTableCellStyle )
            
            statisticsTable.AddItem( 'No Doc String', tableCellStyle )
            statisticsTable.AddItem( numNoDocErrors, passTableCellStyle if numNoDocErrors == 0 else errorTableCellStyle )
            
            doc.AddLine( 'Module Statistics', RS.Utils.Html.FontStyle( fontSize = 4, textStyleFlags = RS.Utils.Html.TextStyle.Bold ) )
            doc.AddTable( statisticsTable )
            doc.AddLineBreak()
            
            # Core lib imports
            if ErrorType.CORE_LIB_IMPORT in data:
                coreLibImportItems = data[ ErrorType.CORE_LIB_IMPORT ]
                
                if coreLibImportItems:
                    table = RS.Utils.Html.Table( numColumns = 2, width = 75, widthStyle = RS.Utils.Html.WidthStyle.Percentage )
                    
                    # Table header.
                    table.AddItem( 'Module Being Imported', tableHeaderCellStyle )
                    table.AddItem( 'Line Number', tableHeaderCellStyle )
                    
                    for item in coreLibImportItems:
                        tableCellStyle = RS.Utils.Html.TableCellStyle( RS.Utils.Html.FontStyle() )
                        tableCellStyle.BackgroundColor = self.GetItemTypeTableCellColor( item.ItemType )
                        
                        table.AddItem( item.Name, tableCellStyle )
                        table.AddItem( item.LineNumber, tableCellStyle )
                        
                    doc.AddHorizontalLine()
                    doc.AddLine( 'Core Lib Imports', RS.Utils.Html.FontStyle( fontSize = 4, textStyleFlags = RS.Utils.Html.TextStyle.Bold ) )
                    doc.AddTable( table )
                    doc.AddLineBreak()
                    doc.AddDetails( 'Description:', 'Core library modules should never import from RS.UI', RS.Utils.Html.FontStyle() )
                
            # Pascal Case
            if ErrorType.PASCAL_CASE in data:
                pascalCaseItems = data[ ErrorType.PASCAL_CASE ]
                
                if pascalCaseItems:
                    table = RS.Utils.Html.Table( numColumns = 3, width = 75, widthStyle = RS.Utils.Html.WidthStyle.Percentage )
    
                    # Table header.
                    table.AddItem( 'Item', tableHeaderCellStyle )
                    table.AddItem( 'Object Type', tableHeaderCellStyle )
                    table.AddItem( 'Line Number', tableHeaderCellStyle )
                    
                    for item in pascalCaseItems:
                        tableCellStyle = RS.Utils.Html.TableCellStyle( RS.Utils.Html.FontStyle() )
                        tableCellStyle.BackgroundColor = self.GetItemTypeTableCellColor( item.ItemType )            
                        
                        table.AddItem( item.Name, tableCellStyle )
                        table.AddItem( item.ItemTypeAsString, tableCellStyle )
                        table.AddItem( item.LineNumber, tableCellStyle )
                        
                    doc.AddHorizontalLine()
                    doc.AddLine( 'Pascal Case', RS.Utils.Html.FontStyle( fontSize = 4, textStyleFlags = RS.Utils.Html.TextStyle.Bold ) )
                    doc.AddTable( table )
                    doc.AddLineBreak()
                    doc.AddDetails( 'Description:', 'These items are not using the PascalCase naming convention.', RS.Utils.Html.FontStyle() )
            
            # No doc string
            if ErrorType.NO_DOC_STRING in data:
                noDocStringItems = data[ ErrorType.NO_DOC_STRING ]
                
                if noDocStringItems:
                    table = RS.Utils.Html.Table( numColumns = 3, width = 75, widthStyle = RS.Utils.Html.WidthStyle.Percentage )

                    # Table header.
                    table.AddItem( 'Item', tableHeaderCellStyle )
                    table.AddItem( 'Object Type', tableHeaderCellStyle )
                    table.AddItem( 'Line Number', tableHeaderCellStyle )
                    
                    for item in noDocStringItems:
                        tableCellStyle = RS.Utils.Html.TableCellStyle( RS.Utils.Html.FontStyle() )
                        tableCellStyle.BackgroundColor = self.GetItemTypeTableCellColor( item.ItemType )
                        
                        table.AddItem( item.Name, tableCellStyle )
                        table.AddItem( item.ItemTypeAsString, tableCellStyle )
                        table.AddItem( item.LineNumber, tableCellStyle )
                        
                    doc.AddHorizontalLine()
                    doc.AddLine( 'No Doc', RS.Utils.Html.FontStyle( fontSize = 4, textStyleFlags = RS.Utils.Html.TextStyle.Bold ) )
                    doc.AddTable( table )
                    doc.AddLineBreak()
                    doc.AddDetails( 'Description:', 'These items have no doc string.', RS.Utils.Html.FontStyle() )
                
            doc.Write()
                
                
    def Write( self, moduleNames = None, outputDir = 'x:\\temp\\PythonCodingStandardsReport' ):
        if not os.path.isdir( outputDir ):
            os.makedirs( outputDir )
            
        # Generate all of the module reports with errors.
        self._WriteModuleReports( '{0}\\Modules'.format( outputDir ) )
        
        # Create index.html for our report.
        doc = RS.Utils.Html.Document( '{0}\\index.html'.format( outputDir ), 'Python Coding Standards Report' )
        
        doc.AddHeader( 'Python Coding Standards Report', RS.Utils.Html.FontStyle( fontSize = 5 ) )
        
        doc.AddLine( 'View Python Coding Standards', RS.Utils.Html.FontStyle( hyperLink = 'https://devstar.rockstargames.com/wiki/index.php/Python_Coding_Standards' ) )
        
        # Setup html render styles.
        fontStyle = RS.Utils.Html.FontStyle()
        whiteFontStyle = RS.Utils.Html.FontStyle( foregroundColor = RS.Utils.Html.Color.White )
        
        tableHeaderFontStyle = RS.Utils.Html.FontStyle( foregroundColor = 'white', textStyleFlags = RS.Utils.Html.TextStyle.Bold )
        tableHeaderCellStyle = RS.Utils.Html.TableCellStyle( tableHeaderFontStyle, backgroundColor = 'gray' )
        tableCellStyle = RS.Utils.Html.TableCellStyle( fontStyle )
        badTableCellStyle = RS.Utils.Html.TableCellStyle( tableHeaderFontStyle, backgroundColor = '#CC3333' )
        
        # Create overall statistics table.
        statsTable = RS.Utils.Html.Table( numColumns = 2, width = 50, cellSpacing = 0, widthStyle = RS.Utils.Html.WidthStyle.Percentage )
        
        # Table header.
        statsTable.AddItem( 'Type', tableHeaderCellStyle )
        statsTable.AddItem( 'Total Offending Items', tableHeaderCellStyle )
        
        # Table stats.
        statsTable.AddItem( 'Pascal Case', tableCellStyle )
        statsTable.AddItem( self.TotalPascalCaseErrors, tableCellStyle if self.TotalPascalCaseErrors == 0 else badTableCellStyle )
        
        statsTable.AddItem( 'Core Library Import', tableCellStyle )
        statsTable.AddItem( self.TotalCoreImportErrors, tableCellStyle if self.TotalCoreImportErrors == 0 else badTableCellStyle )   
        
        statsTable.AddItem( 'No Doc String', tableCellStyle )
        statsTable.AddItem( self.TotalNoDocStringErrors, tableCellStyle if self.TotalNoDocStringErrors == 0 else badTableCellStyle )        
        
        doc.AddHeader( 'Overall Report Statistics', RS.Utils.Html.FontStyle( fontSize = 4 ), headerStyle = RS.Utils.Html.HeaderStyle.H2 )
        doc.AddTable( statsTable )
        doc.AddLineBreak()
        doc.AddHorizontalLine()
        
        # List modules with errors.
        modulesWithErrorsTable = RS.Utils.Html.Table( numColumns = 4, width = 100, cellPadding = 0, widthStyle = RS.Utils.Html.WidthStyle.Percentage )
        
        if self.__errors:
            modNames = self.__errors.keys()
            modNames.sort()
            
            # Header
            modulesWithErrorsTable.AddItem( 'Module', tableHeaderCellStyle )
            modulesWithErrorsTable.AddItem( 'Pascal Case', tableHeaderCellStyle )
            modulesWithErrorsTable.AddItem( 'Core Library Import', tableHeaderCellStyle )
            modulesWithErrorsTable.AddItem( 'No Doc String', tableHeaderCellStyle )
            
            for modName in modNames:
                urlFontStyle = RS.Utils.Html.FontStyle()  
                urlFontStyle.Hyperlink = '{0}\\modules\\{1}.html'.format( outputDir, modName )
                modulesWithErrorsTable.AddItem( modName, RS.Utils.Html.TableCellStyle( urlFontStyle ) )
                
                numPascalCaseErrors = len( self.GetItemsByErrorType( modName, ErrorType.PASCAL_CASE ) )
                numCoreImportErrors = len( self.GetItemsByErrorType( modName, ErrorType.CORE_LIB_IMPORT ) )
                numNoDocErrors = len( self.GetItemsByErrorType( modName, ErrorType.NO_DOC_STRING ) )
                
                modulesWithErrorsTable.AddItem( numPascalCaseErrors if numPascalCaseErrors > 0 else '-', RS.Utils.Html.TableCellStyle( fontStyle if numPascalCaseErrors > 0 else whiteFontStyle ) )
                modulesWithErrorsTable.AddItem( numCoreImportErrors if numCoreImportErrors > 0 else '-', RS.Utils.Html.TableCellStyle( fontStyle if numCoreImportErrors > 0 else whiteFontStyle ) )
                modulesWithErrorsTable.AddItem( numNoDocErrors if numNoDocErrors > 0 else '-', RS.Utils.Html.TableCellStyle( fontStyle if numNoDocErrors > 0 else whiteFontStyle ) )
        
        doc.AddLine( 'Modules with Errors', RS.Utils.Html.FontStyle( fontSize = 3, textStyleFlags = RS.Utils.Html.TextStyle.Bold ) )
        doc.AddTable( modulesWithErrorsTable )
        
        doc.Write()

def CheckCoreLibraryImports():
    '''
    Check if any module under RS.Core is importing from RS.UI.
    '''
    global report
    
    for directory, dirNames, filenames in os.walk( '{0}\\Core'.format( RS.Config.Script.Path.RockstarRoot ) ):
        for filename in filenames:
            if filename.endswith( '.py' ):
                pyModule = os.path.join( directory, filename )
                
                try:
                    f = open( pyModule, 'r' )
                    lines = f.readlines()
                    
                except IOError, er:
                    print er
                    
                finally:
                    f.close()
                
                importPath = RS.Utils.Path.GetImportPathFromFilename( pyModule )
                
                lineNo = 1
                
                for line in lines:
                    if 'import' in line:
                        if 'RS.UI' in line:
                            offendingImport = str( line.lstrip().rstrip() ).split( ' ' )[ -1 ]
                            
                            report.AddItem( ReportItem( offendingImport, importPath, ItemType.IMPORT, ErrorType.CORE_LIB_IMPORT, lineNumber = lineNo ) )
                            
                    lineNo += 1

def CheckDocString( obj ):
    
    # Get arguments.
    
    # Get keyword arguments.
    
    # Does the function return.
    
    # Not necessary for built-ins.
    if inspect.isbuiltin( obj ):
        return True
    
    return inspect.getdoc( obj )
    
def IsBuiltIn( name ):
    return name.startswith( '__' ) and name.endswith( '__' )

def CheckPascalCase( name ):
    '''
    Check to see if the incoming name satisfies the PascalCase convention.
    '''
    
    # Exit early as this is likely a MotionBuilder SDK name.
    if name.startswith( 'FB' ):
        return True
    
    # Ignore class builtins and privates  Beavis: Ahe he hehe, privates.
    if IsBuiltIn( name ):
        return True
    
    # Strip any prefixed underscores.
    name = name.lstrip( '_' )
    
    # If only one character long, it should be uppercase.
    if len( name ) == 1:
        return name[ 0 ].isupper()
    
    # Test if the first letter is uppercase, and the second is lowercase.  That is the minimum requirement for
    # Pascal Case.
    return name[ 0 ].isupper() and name[ 1 ].islower()

def GetLineNumber( obj ):
    '''
    Get the line number of the code object.
    '''
    try:
        return inspect.getsourcelines( obj )[ -1 ]
    
    except IndexError:
        return None
    
    except TypeError:
        return None

def CheckNamingConventions( module ):
    global report
    
    # Functions to only return module level items.  Otherwise, inspect.getmembers() returns
    # all of the imported module definitions, and we definitely don't want that.
    def GetModuleClasses( obj ):
        return inspect.isclass( obj ) and module.__name__ == obj.__module__
    
    def GetModuleFunctions( obj ):
        return inspect.isfunction( obj ) and module.__name__ == obj.__module__
    
    def GetClassMethods( obj ):
        return inspect.ismethod( obj )
    
    def GetClassPublicProperties( obj ):
        return not inspect.ismethod( obj )

    classes = inspect.getmembers( module, GetModuleClasses )
    functions = inspect.getmembers( module, GetModuleFunctions )
    
    # Check module level classes.
    for cls in classes:
        clsName, clsObj = cls

        hasDocString = CheckDocString( clsObj )
        isPascalCase = CheckPascalCase( clsName )
                      
        if not hasDocString:
            report.AddItem( ReportItem( clsName, module.__name__, ItemType.CLASS, ErrorType.NO_DOC_STRING, GetLineNumber( clsObj ) ) )
        
        if not isPascalCase:
            report.AddItem( ReportItem( clsName, module.__name__, ItemType.CLASS, ErrorType.PASCAL_CASE, GetLineNumber( clsObj ) ) )
            
        # Class properties
        if module.__name__ == 'RS.Core.Reference.Manager':
            publicProperties = []
            items = inspect.getmembers( clsObj )
            
            for item in items:
                itemName, itemObj = item
                
                if isinstance( itemObj, property ):
                    if not IsBuiltIn( itemName ):
                        hasDocString = CheckDocString( itemObj )
                        isPascalCase = CheckPascalCase( itemName )
                                      
                        if not hasDocString:
                            report.AddItem( ReportItem( '<i>{0}</i>.<b>{1}</b>'.format( clsName, itemName ), module.__name__, ItemType.CLASS_PROPERTY, ErrorType.NO_DOC_STRING, GetLineNumber( itemObj ) ) )
                        
                        if not isPascalCase:
                            report.AddItem( ReportItem( '<i>{0}</i>.<b>{1}</b>'.format( clsName, itemName ), module.__name__, ItemType.CLASS_PROPERTY, ErrorType.PASCAL_CASE, GetLineNumber( itemObj ) ) )
                
        # Check class members.
        clsMethods = inspect.getmembers( clsObj, GetClassMethods )
        
        for clsMethod in clsMethods:
            methodName, methodObj = clsMethod
            
            if not IsBuiltIn( methodName ):
                hasDocString = CheckDocString( methodObj )
                isPascalCase = CheckPascalCase( methodName )
                              
                if not hasDocString:
                    report.AddItem( ReportItem( '<i>{0}</i>.<b>{1}</b>'.format( clsName, methodName ), module.__name__, ItemType.CLASS_METHOD, ErrorType.NO_DOC_STRING, GetLineNumber( methodObj ) ) )
                
                if not isPascalCase:
                    report.AddItem( ReportItem( '<i>{0}</i>.<b>{1}</b>'.format( clsName, methodName ), module.__name__, ItemType.CLASS_METHOD, ErrorType.PASCAL_CASE, GetLineNumber( methodObj ) ) )
        
    # Check module level functions.
    for fn in functions:
        fnName, fnObj = fn
        
        if not IsBuiltIn( fnName ):
            hasDocString = CheckDocString( fnObj )
            isPascalCase = CheckPascalCase( fnName )
                          
            if not hasDocString:
                report.AddItem( ReportItem( fnName, module.__name__, ItemType.FUNCTION, ErrorType.NO_DOC_STRING, GetLineNumber( fnObj ) ) )
            
            if not isPascalCase:
                report.AddItem( ReportItem( fnName, module.__name__, ItemType.FUNCTION, ErrorType.PASCAL_CASE, GetLineNumber( fnObj ) ) )

def CheckAllNamingConventions():
    global report
    
    for directory, dirNames, filenames in os.walk( '{0}'.format( RS.Config.Script.Path.RockstarRoot ) ):
        for filename in filenames:
            if filename.endswith( '.py' ):
                pyModule = os.path.join( directory, filename )
                
                # Construct import path from filename.
                moduleName = RS.Utils.Path.GetImportPathFromFilename( pyModule )
                
                if moduleName not in sys.modules:
                    try:
                        #module = imp.load_source( moduleName, pyModule )
                        __import__( moduleName, globals(), locals(), [], -1 )
                        module = sys.modules[ moduleName ]
                        CheckNamingConventions( module )
                        
                    except ImportError:
                        print '{0} could not be imported because of errors.'.format( moduleName )                    
                    
                else:
                    CheckNamingConventions( sys.modules[ moduleName ] )
                    pass

def GenerateReport( filenames = None ):
    global report
    
    report = CodingStandardsReport()
    report.Clear()
    
    CheckCoreLibraryImports()
    CheckAllNamingConventions()
    
    reportDir = 'x:\\temp\\PythonCodingStandardsReport'
    
    report.Write( outputDir = reportDir )
    
    webbrowser.get( 'windows-default' ).open( '{0}\\index.html'.format( reportDir ) )
    