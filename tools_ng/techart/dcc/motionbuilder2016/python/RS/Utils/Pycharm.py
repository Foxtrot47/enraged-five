"""
Module for interacting with the PyCharm debugger.

Notes:
    * We currently add PyCharm's pydev egg to the MotionBuilder codebase's external packages folder.
    * C:\Program Files\JetBrains\<PyCharm folder for TA standard version>\debug-eggs\pydevd-pycharm.egg
"""
import socket

# Defer pydev imports to avoid issues for other IDEs.


class RemoteDebugger(object):
    """
    Class for interacting with the debugger
    """
    HOST = "localhost"
    PORT = 21100  # The port we use for the pycharm debugger

    @classmethod
    def isPortOpen(cls):
        """
        Check if the port is open

        Return:
            bool
        """
        # Close the socket object we just used for determining if the socket is open
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind((cls.HOST, cls.PORT))
            return False  # The socket is available to be bound, thus not in use by the remote debugger
        except:
            return True  # The socket is unavailable to be bound, thus it is already in use by the remote debugger
        finally:
            sock.close()

    @classmethod
    def connect(cls):
        """
        Connect to pycharm's remote debugger

        Returns:
            bool
        """
        if cls.isPortOpen():
            cls._connect()
            return True
        return False

    @classmethod
    def _connect(cls):
        """
        The pydevd code to connect to the debugger
        """
        import pydevd
        import pydevd_pycharm

        pydevd.stoptrace()  # Stop any existing pydev trace that may prevent a reconnect to the python server
        pydevd_pycharm.settrace(cls.HOST, port=cls.PORT, stdoutToServer=True, stderrToServer=True)
