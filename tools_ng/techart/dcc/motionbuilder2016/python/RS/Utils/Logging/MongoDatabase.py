import threading
import platform
import os
import datetime
import inspect
import re
import getpass
import logging

from RS.Utils.Logging import utils
from RS import Databases

LOG = logging.getLogger(__name__)

@utils.Enabled
def LogUserInfo():
    """Logs the User and Machine information to the Mongo Database."""

    def _Log():

        # Get our System Info
        sysInfo = utils.getSystemInfo()

        # Get application Version
        mobuVersion = utils.MotionbuilderVersion()

        # Get the MB build, i.e 2014or 2015
        mobuBuild = utils.MotionbuilderBuild()

        timeStamp = datetime.datetime.now()

        utcTimestamp = datetime.datetime.utcnow()

        bsonData = { "$set":{"TimeStamp":timeStamp,
                             "TimeStampUTC":utcTimestamp,
                            "User":str(os.environ['USERNAME']).lower(),
                            "Machine": platform.node(),
                            "Studio": utils.getStudioName(),
                            "ToolsVersion": -1,
                            "MobuInfo": {"MajorVersion": mobuBuild,
                                        "MinorVersion": mobuVersion
                                        },
                            "SysInfo": {"CPU":sysInfo.get.cpu,
                                        "RAM":sysInfo.get.totalRam,
                                        "GPU":sysInfo.get.gpu,
                                        "OS":sysInfo.get.os,
                                        "HD":sysInfo.get.hdTotal}}}

        collection = Databases.MONGO_NYC.ClientsTracking()

        try:
            collection.update_many(filter={"Machine": platform.node()},
                                   update=bsonData,
                                   upsert=True )
            LOG.info("- Logging Client Info to Mongo Database:")
        except Exception as e:
            LOG.info("- Unable to update Mongo Database: {}".format(e.message))

    thread = threading.Thread(target=_Log)
    thread.start()

@utils.Enabled
def LogToolInfo(moduleName,
                context,
                toolName,
                modulePath="",
                thread=True):
    """Logs Information about the Tool that is opened in the R* menu to the Mongo Database.

    Args:
        moduleName (str): name of the module
        context (enum):
        toolName (str): name of the tool
        modulePath (str): full path to the module
        thread (bool): if the logging process should be done in the thread so it doesn't block the main software.

    :return:
        None
    """
    def _log(moduleName=moduleName,
            context=context,
            toolName=toolName,
            moduleFilePath=modulePath
            ):

        # Get Module file name is not passed in
        if not modulePath:
            moduleFilePath = inspect.getfile(inspect.currentframe().f_back)
        moduleFilePath = os.path.abspath(moduleFilePath)
        moduleFilePath = re.sub('"|\'', '', moduleFilePath)

        # Get Universal Timestamp
        utcTimestamp = datetime.datetime.utcnow()

        bsonData = { "UTCTimeStamp":utcTimestamp,
                     "Tool": {"ModuleName": moduleName,
                              "FriendlyName": toolName,
                              "ModulePath": moduleFilePath},
                     "Username": getpass.getuser(),
                     "Scene": utils.getSceneFileName(),
                     "Studio": utils.getStudioName(),
                     "Project:": utils.getProjectID()
                     }

        collection = Databases.MONGO_NYC.ToolsTracking()

        try:
            collection.insert_one(bsonData)
        except Exception as e:
            LOG.info("_CORE - ERROR  - Unable to update Mongo database:".format(e.message))

    if thread:
        thread = threading.Thread(target=_log)
        thread.start()
    else:
        _log()




