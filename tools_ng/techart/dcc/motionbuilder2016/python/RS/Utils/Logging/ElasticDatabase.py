import datetime
import getpass
import inspect
import logging
import os
import re
import threading

from RS import Databases
from RS.Utils.Logging import utils

LOG = logging.getLogger(__name__)

SEARCH_INDEX = "telemetry_Motionbuilder_Tools_Tracking"
SEARCH_DOCTYPE = "Motionbuilder_TA_tool"


@utils.ElasticEnabled
def LogToolInfo(moduleName, toolName, modulePath=""):
    """Logs information about the tools usage to the elastic search database.

    Args:
        moduleName (str): name of the module where the tool is
        toolName (str): friendly name of the tool
        modulePath (str, optional): full path to the module.
    """
    # Get Module file name is not passed in
    if modulePath == "":
        modulePath = inspect.getfile(inspect.currentframe().f_back)
    modulePath = os.path.abspath(modulePath)
    modulePath = modulePath.replace("\"|'", "")

    # Get Universal Timestamp
    utcTimestamp = datetime.datetime.utcnow()

    bsonData = {
        "UTCTimeStamp": utcTimestamp,
        "Tool": {"ModuleName": moduleName, "FriendlyName": toolName, "ModulePath": modulePath},
        "Username": getpass.getuser(),
        "Scene": utils.getSceneFileName(),
        "Studio": utils.getStudioName(),
        "Project:": utils.getProjectID(),
    }

    LOG.info("Connecting to Elastic Search server")
    LOG.info("Logging Tool usage to Elastic")

    def _log():
        try:
            db = Databases.Elastic.ToolsTracker()
            db.addData(bsonData)
        except Exception as error:
            LOG.error(error)

    thread = threading.Thread(target=_log)
    thread.start()
