class MessageTextColours(object):
    Default = 'White'
    Error = 'Red'
    Warning = 'Orange'
    Minor_Update = 'SkyBlue'
    Major_Update = 'Yellow'
    Delete = 'DarkOrchid'