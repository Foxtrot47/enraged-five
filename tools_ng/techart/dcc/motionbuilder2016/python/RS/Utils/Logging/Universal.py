'''
RS.Utils.Logging.Universal

Module for interfacing with the Universal Log.
'''
import os
import subprocess
from functools import wraps

import clr

from pyfbsdk import *

from PySide import QtCore, QtGui

clr.AddReference('RSG.Base')

from System.IO import FileMode

from RSG.Base.Logging import FlushMode
from RSG.Base.Logging import LogFactory
from RSG.Base.Logging.Universal import UniversalLogFile

from RS import Config
from RS.Tools.ReferenceEditor.widget import progressWindowWidget


def Show(ulogFilename, modal =True):
    '''
    Show a Universal Log file.

    Arguments:

        ulogFilename: The Universal Log filename to show.

    Keyword Arguments:

        modal: Whether or not the window is modal.
    '''
    ulogViewerExe = '{0}\\bin\\UniversalLogViewer\\UniversalLogViewer.exe'.format(Config.Tool.Path.Root)

    if os.path.isfile(ulogViewerExe):
        process = subprocess.Popen('{0} {1}'.format(ulogViewerExe, ulogFilename), shell = False)

        # Force ULog to be a blocking call, making it modal.
        if modal:
            process.communicate()

    else:
        print 'Cannot open the Universal Log Viewer, since it could not be found!  Was expecting it here ({0}).'.format( ulogViewerExe )


def OpenLog(logFilePath):
    '''
    Opens/creates a log at the passed directory.
    '''
    logPath = os.path.dirname(logFilePath)
    logName, logExtension = os.path.splitext( os.path.basename( logFilePath ) )

    if logExtension.lower() != '.ulog':
        raise Exception("Passed a non uLog file path.")

    return UniversalLog(logName,
                        appendToFile=True,
                        consoleOutput=False,
                        logPath=logPath)


def CheckProgressWindow(func):
    """
    Tries to run progress window command and if it has been deleted, it creates a new progress window

    Arguments:
        func (function): decorated function

    """
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except:
            self._progressWindow = progressWindowWidget.ProgressWindowDialog()
            return func(self, *args, **kwargs)
    return wrapper


class UniversalLog(object):
    '''
    Wrapper that represents a Universal Log object.

    Arguments:

        name: The name of the Universal Log to create.

    Keyword Arguments:

        appendToFile: Whether or not to append to an existing Universal Log.
        consoleOutput: Output logging messages to the console as they happen.
        logPath: Explict path to save log file. Defaults to the tools log directory.
    '''
    def __init__(self, name, appendToFile=False, consoleOutput=True, logPath=None):
        self._name = name
        self._progressWindow = progressWindowWidget.ProgressWindowDialog()
        if logPath == None:
            self._filename = '{0}\\{1}.ulog'.format(Config.Tool.Path.Logs, name)
        else:
            self._filename = '{0}\\{1}.ulog'.format(logPath, name)

        self._logObj = LogFactory.CreateUniversalLog(self._filename, True)

        self._bufferSize = 100
        self._fileMode = FileMode.CreateNew

        self._active = True

        self._contextStack = []

        self._progressWindowOutput = False

        if appendToFile:
            self._fileMode = FileMode.Append

        self._logFile = UniversalLogFile(self._filename, self._fileMode, FlushMode.Always, self._logObj, self._bufferSize)

        # Whether or not to output logging to the console as it happens.
        self._consoleOutput = consoleOutput

    @property
    def Name(self):
        return self._name

    @property
    def Filename(self):
        return self._logFile.Filename

    @property
    def HasErrors(self):
        return self._logObj.HasErrors

    @property
    def HasWarnings(self):
        return self._logObj.HasWarnings

    @property
    def ConsoleOutput(self):
        return self._consoleOutput

    @ConsoleOutput.setter
    def ConsoleOutput(self, state):
        self._consoleOutput = state

    @property
    def ProgressWindowOutput(self):
        return self._progressWindowOutput

    @ProgressWindowOutput.setter
    def ProgressWindowOutput(self, state):
        self._progressWindowOutput = state

        if state is True:
            self.ProgressWindowStart()
        else:
            self.ProgressWindowStop()

    @property
    def Active(self):
        return self._active

    @Active.setter
    def Active(self, state):
        if(type(state) == bool):
            self._active = state

    def GetErrors(self):
        '''
        TODO (Hayes 7/18/2013): Need to finish this implementation.  The cutscene update system is calling this.
        '''
        pass

    def Show(self, modal=True):
        Show(self.Filename, modal)

    def Flush(self):
        self._logFile.Flush()

    def PushContext(self, context):
        '''
        Pushes a new context to the log so all future entries get added to this context.
        '''
        self._contextStack.append(context)

    def PopContext(self):
        '''
        Pops last pushed context off stack.
        '''
        self._contextStack.pop()
        if len(self._contextStack) == 0:
            self.ProgressWindowStop()

    def GetContext(self):
        '''
        Gets context off the top of stack if one exists.
        '''
        if len(self._contextStack) > 0:
            return self._contextStack[-1]
        else:
            return ''

    def LogMessage(self, message, context=None):
        if not self._active: return()
        if context == None:
            context = self.GetContext()

        self._logObj.MessageCtx(context, message, [])

        messageToPrint = 'MESSAGE [{0}]: {1}'.format(context, message)

        if self._progressWindowOutput:
            QtCore.QCoreApplication.processEvents()
            self.ProgressWindowAddText(messageToPrint)

    def LogError(self, message, context = None):
        if not self._active: return()
        if context == None:
            context = self.GetContext()

        self._logObj.ErrorCtx(context, message, [])

        messageToPrint= 'ERROR [{0}]: {1}'.format(context, message)

        if self._consoleOutput:
            print(messageToPrint)

        if self.ProgressWindowOutput:
            self.ProgressWindowAddText(messageToPrint)

    def LogWarning(self, message, context = None):
        if not self._active: return()
        if context == None:
            context = self.GetContext()

        self._logObj.WarningCtx( context, message, [] )

        messageToPrint= ('WARNING [{0}]: {1}'.format(context, message))

        if self._consoleOutput:
            print(messageToPrint)

        if self.ProgressWindowOutput:
            self.ProgressWindowAddText(messageToPrint)

    def LogDebug(self, message, context = None):
        if not self._active: return()
        if context == None:
            context = self.GetContext()

        self._logObj.DebugCtx(context, message, [])

        messageToPrint= ('DEBUG [{0}]: {1}'.format(context, message))

        if self._consoleOutput:
            print(messageToPrint)

        if self.ProgressWindowOutput:
            self.ProgressWindowAddText(messageToPrint)

    @CheckProgressWindow
    def ProgressWindowAddText(self, text):
        '''
        Adding text to the window
        '''
        self._progressWindow.AddText(text)

    @CheckProgressWindow
    def ProgressWindowStart(self):
        '''
        Starting the Progress window
        '''
        self._progressWindow.show()

    @CheckProgressWindow
    def ProgressWindowStop(self):
        '''
        Closing the Progress window

        TODO : close needs to not be available until the current process finishes(piggy back on the
               process)
        '''
        self._progressWindow.hide()


class Logger(object):
    '''
    A decorator that will act as log wrapper around the RS.Logging system
    args:
    log - RS.Utils.Logging.Universal.UniversalLog

    kwargs:
    context - string: message title
    state - bool: launch the UI
    textColour - str: CSS colour name for text shown in the UI within this context
    '''
    def __init__(self, log, context=None, state=True):
        self._context = context
        self._state = state
        self._log = log

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            # set the progresswindowoutput property and run the log UI
            self._log.ProgressWindowOutput = self._state

            # add log string to a list
            if self._context:
                self._log.PushContext(self._context)

            try:
                return func(*args, **kwargs)

            except Exception as error:
                if not hasattr(error, '_handled'):
                    self._log.LogError(error.message)
                    error._handled = True
                raise

            finally:
                if self._context:
                    self._log.PopContext()

        return wrapper