import os
import functools
import xml.etree.cElementTree as xml

import pyfbsdk as mobu
from RS import Perforce
from RS import Config, ProjectData
from RS.Utils import SystemInfo as sysInfo


PACKAGE_PATH = os.path.dirname(__file__)


def getStudioName():
    """Returns Studio User is part of.

    Returns:
        string
    """
    return Config.User.Studio


def MotionbuilderVersion():
    """Returns the version of  motion builder.

    Returns:
        string
    """
    return Config.Script.TargetCut


def MotionbuilderBuild():
    """Returns the name of the current Motion Builder Build.

    Returns:
        string
    """
    return Config.Script.TargetBuild


def getSystemInfo():
    """Return the machines system information.

    Returns:
        system Object
    """
    return sysInfo


def getProjectID():
    """Returns our Project ID.

    Returns:
        Integer
    """
    currentProjectID = ProjectData.data.GetSDRockstarToolsProjectId()
    if ProjectData.data.InWildWest():
        currentProjectID = 3
    return currentProjectID


def getSceneFileName():
    """Returns the Scene file name if any.

    Returns:
        String
    """
    # Get scene file name
    sceneFilename = mobu.FBApplication().FBXFileName

    if not sceneFilename:
        sceneFilename = sceneFilename.replace("\\", "/").lower()

    if sceneFilename:
        sceneFilename = os.path.abspath(sceneFilename)

    return sceneFilename


def Enabled(func):
    """Runs the decorated function if logging is enabled.

    Args:
        func (function): function to decorate

    Returns:
        function
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if IsLoggingEnabled():
            return func(*args, **kwargs)

    return wrapper


def ElasticEnabled(func):
    """Decorator to check if the elastic search logging is enabled.

    Args:
         func (function): name of the function we are wrapping around

    Returns:
        function
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        xml_path = os.path.join(PACKAGE_PATH, "ElasticEnabled.xml")
        # Sync file
        if not os.path.exists(xml_path) or not os.path.getsize(xml_path):
            Perforce.Sync(xml_path, force=True)

        # Read the information
        xml_tree = xml.parse(xml_path)
        xml_element = xml_tree.getroot()
        enabled = xml_element.text
        if "True" in enabled:
            return func(*args, **kwargs)

    return wrapper


def checkXml(func):
    """Decorator for synching the latest version of the enabled.xml if it is empty or missing.

    Args:
        func (function): the decorated function
    """

    def wrapper(*args, **kwargs):
        # Using XML as that seems to be what we are all using
        xml_path = os.path.join(PACKAGE_PATH, "Enabled.xml")
        if not os.path.exists(xml_path) or not os.path.getsize(xml_path):
            Perforce.Sync(xml_path, force=True)
        return func(*args, **kwargs)

    return wrapper


@checkXml
def IsLoggingEnabled():
    """Is logging enabled.

    Returns:
        boolean
    """
    # Using XML as that seems to be what we are all using
    xml_path = os.path.join(PACKAGE_PATH, "Enabled.xml")

    xml_tree = xml.parse(xml_path)
    xml_element = xml_tree.getroot()
    enabled = xml_element.text
    return "True" in enabled
