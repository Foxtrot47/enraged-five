"""
Module for commmon string operations
"""
import re
import operator

import RS.Config
import RSG.Base
import mobuExtender


_OPERATORS_MAP = {
    '==': operator.eq,
    '!=': operator.ne,
    '<=': operator.le,
    '>=': operator.ge,
}


def commonStart(string1, string2):
    """
    returns the longest common substring from the beginning of sa and sb
    stole from http://stackoverflow.com/questions/18715688/find-common-substring-between-two-strings

    Arguments:
        string1 (string): string to compare
        string2 (string): string to compare
    """
    def _iter():
        for character1, character2 in zip(string1, string2):
            if character1 == character2:
                yield character1
            else:
                return

    return ''.join(_iter())


def common(string1, string2, lengthMinimum=1):
    """
    Compares two strings and returns the common characters in both strings

    Arguments:
        string1 (string): string to compare
        string2 (string): string to compare
        lengthMinimum (int): minimum length of the common strings to return

    Return:
        list(string, etc.)
    """
    strings = {}
    for index, character in enumerate(string1):
        startIndex = string2.find(character)
        while string2.find(character, startIndex) != -1:
            startIndex = string2.find(character, startIndex)
            commonString = commonStart(string1[index:], string2[startIndex:])
            commonStringLen = len(commonString)
            if commonString not in strings.setdefault(commonStringLen, []) and commonStringLen >= lengthMinimum:
                strings[commonStringLen].append(commonString)
            startIndex += 1
    return [word for key in reversed(sorted(strings.keys())) for word in strings[key]]


def toBackSlashes(theString):
    return theString.replace("/","\\")


def toForwardSlashes(theString):
    return theString.replace("\\","/")


def Get16BitHash(theString):
    return int(mobuExtender.atHash16U(theString))


def Get32BitHash(theString):
    return int(RSG.Base.Security.Cryptography.OneAtATime.ComputeHash(theString))


def GetProjectDependentHash(theString):
    projectsNeeding32BitHashes = ["americas"]
    if RS.Config.Project.Name.lower() in projectsNeeding32BitHashes:
        return Get32BitHash(theString)
    return Get16BitHash(theString)


def resolveStringExpression(expression, matchValue):
    """Resolves a string expression, fetching the input value and operator from it, finally matching it to matchValue
    argument.

    The input value is by default considered an integer value.

    Args:
        expression (str): the string expression.
        matchValue (int): the value to match the int value contained in the expression.

    Returns:
        bool: result of the expression.
    """
    regex = re.compile("==|>=|<=|!=")
    expressionResult = True
    try:
        operatorStr = regex.findall(expression)[0]
        expressionOperationFn = _OPERATORS_MAP.get(operatorStr)
        expressionInputValue = int(re.findall('\d+', expression)[0])
        expressionResult = expressionOperationFn(matchValue, expressionInputValue)

    except Exception as error:
        pass

    finally:
        return expressionResult
