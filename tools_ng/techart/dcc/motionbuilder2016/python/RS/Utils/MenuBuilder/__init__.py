"""
RS.Utils.MenuBuilder

Author: Jason Hayes (jason.hayes@rockstarsandiego.com)
Description: Builds a Motionbuilder menu system based on definitions in the menu.config file.

Notes:

- I've found that if you wrap the code that generates and binds the event handler within a function,
  then Motionbuilder will crash any time the user attempts to click on one of the created menu items.
  It's really strange.

"""

__DoNotReload__ = True  # Module is NOT safe to be dynamically reloaded

import os
import sys
import traceback
from inspect import isroutine
from xml.etree import cElementTree

import pyfbsdk as mobu

from RS import Config
import RS.Tools.UI.Commands
import RS.Utils.Exception as ex
from RS.Utils.Logging import Database
from RS.Utils.Logging import MongoDatabase

# # Defines # #

# The MoBu app.

# The menu name for Rockstar.
RS_MAIN_MENU_NAME = 'Rockstar'  # + " - {0}".format(RS.Config.Project.Name)

# Dictionary of ToolMenuItem objects that get created when menu.config is parsed.
RegisteredTools = {}

MenuTree = None


# # Classes # #

class ToolMenuTree(object):
    """
    Holds information about the menu
    """
    def __init__(self):
        self.DisplayName    = None
        self.Separator      = False
        self.Sort           = False
        self.UnitTest       = True
        self.SubMenuItems   = []
        self.MenuItems      = []


class ToolMenuItem(object):
    """
    Holds information about each menu item parsed out of menu.config
    """
    def __init__(self):
        self.DisplayName    = None
        self.ModulePath     = None
        self.Command        = None
        self.Separator      = False
        self.UnitTest       = True

# # Functions # #

def RecurseParseConfig(xmlRoot, tree):
    """
    Parses the menu.config xml

    Arguments:
        xmlRoot (elementTree.Element): root element of the menu.config file
        tree (elementTree.Tree): element tree that holds the elements that make up the menu.config xml
    """
    global RegisteredTools

    xmlSubMenuNodes = xmlRoot.findall('submenu')
    xmlMenuNodes = xmlRoot.findall('menu')

    if xmlMenuNodes:
        for xmlNode in xmlMenuNodes:
            toolMenuItem = ToolMenuItem()
            toolMenuItem.DisplayName = xmlNode.get('displayName')

            try:
                toolMenuItem.ModulePath = xmlNode.get('modulePath')

            except:
                pass

            try:
                toolMenuItem.Command = xmlNode.get('command')

            except:
                pass

            try:
                separator = xmlNode.get('separator')

                if str(separator).lower() == 'true':
                    toolMenuItem.Separator = True

            except:
                pass

            try:
                toolMenuItem.UnitTest = xmlNode.get('unitTest')

            except:
                pass


            tree.MenuItems.append(toolMenuItem)

            RegisteredTools[ toolMenuItem.DisplayName ] = toolMenuItem

        if tree.Sort:
            tree.MenuItems.sort(key=lambda x: x.DisplayName)

    if xmlSubMenuNodes:
        for xmlNode in xmlSubMenuNodes:
            leaf = ToolMenuTree()
            leaf.DisplayName = xmlNode.get('displayName')

            try:
                separator = xmlNode.get('separator')

                if str(separator).lower() == 'true':
                    leaf.Separator = True

            except:
                pass

            try:
                sort = xmlNode.get('sort')

                if str(sort).lower() == 'true':
                    leaf.Sort = True

            except:
                pass

            tree.SubMenuItems.append(leaf)

            RecurseParseConfig(xmlNode, leaf)

        if tree.Sort:
            tree.SubMenuItems.sort(key=lambda x: x.DisplayName)


def GetRegisteredTools():
    """ Parse menu.config and build a tree of menu items. """
    global RegisteredTools
    global MenuTree

    configFile = Config.Script.Path.MenuConfig
    doc = cElementTree.parse(configFile)
    root = doc.getroot()
    print "*" * 100
    print configFile
    print "*" * 100
    if root:
        MenuTree = ToolMenuTree()
        MenuTree.DisplayName = RS_MAIN_MENU_NAME
        RecurseParseConfig(root, MenuTree)

    return RegisteredTools


def RecurseBuildMenu(currentLeaf, parentMenu):
    """
    Recursively build the menu.

    Arguments:
        currentLeaf (pyfbsdk.FBGenericMenu): the current menu
        parentMenu (pyfbsdk.FBGenericMenu): the parent menu
    """
    global menuId

    for menuItem in currentLeaf.MenuItems:
        parentMenu.InsertLast(menuItem.DisplayName, menuId)

        if menuItem.Separator:
            menuId += 1
            parentMenu.InsertLast('', menuId)

        menuId += 1

    menuId += 1

    for subMenuItem in currentLeaf.SubMenuItems:
        subMenu = mobu.FBGenericMenu()
        parentMenu.InsertLast(subMenuItem.DisplayName, menuId, subMenu)

        if subMenuItem.Separator:
            menuId += 1
            parentMenu.InsertLast('', menuId)

        subMenu.OnMenuActivate.Add(OnMenuItem_Clicked)

        RecurseBuildMenu(subMenuItem, subMenu)


def runModule(module, *args):
    """
    Runs the module or method passed in.

    Arguments:
        module: module or method; module or method to run
        isfunction: boolean; DEPRECATED, is the passed in module a function. The code now resolves this by itself.
    """
    # isroutine checks if the object passed in is a function/method
    if isroutine(module):
        module()
    else:
        module.Run()

# # Event Handlers # #


def OnMenuItem_Clicked(control, event):
    """
    Event handler for a menu item.
    """

    global RegisteredTools

    if event.Name not in RegisteredTools:
        return

    toolItem = RegisteredTools[event.Name]

    try:
        modulePath = toolItem.ModulePath or RS.Tools.UI.Commands.__file__

        if toolItem.ModulePath:
            if toolItem.ModulePath not in sys.modules:
                __import__(toolItem.ModulePath, globals(), locals(), [], -1)

            module = sys.modules[toolItem.ModulePath]
            reload(module)
            modulePath = module.__file__
            moduleName = module.__name__

        # Execute a function from the commands module.
        elif toolItem.Command and getattr(RS.Tools.UI.Commands, toolItem.Command, None):
            module = getattr(RS.Tools.UI.Commands, toolItem.Command)
            modulePath = RS.Tools.UI.Commands.__file__
            moduleName = module.__name__

        elif toolItem.Command and not getattr(RS.Tools.UI.Commands, toolItem.Command, None):
            mobu.FBMessageBox('Rockstar', 'Could not find the command named "{0}"!'.format(toolItem.Command), 'OK')
            return

        else:
            mobu.FBMessageBox('Rockstar', 'There is no command or module associated with this menu item!', 'OK')
            return

        runModule(module)

        # Log Tool the the database
        MongoDatabase.LogToolInfo(moduleName, Database.Context.Opened, event.Name, modulePath=modulePath)



    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()

        # Convert exc_type to a string and only extract the name of the error type

        exception = traceback.format_exception_only(exc_type, exc_value)[0]
        exception = exception.strip().split(":")[0]

        # This is to make sure that we get the correct path to the tool
        if not os.path.exists(modulePath):
            moduleName = modulePath
            modulePath = os.path.join(os.path.dirname(RS.Config.Script.Path.RockstarRoot), *modulePath.split("."))

            if os.path.isfile("{}.py".format(modulePath)):
                modulePath = "{}.py".format(modulePath)

            elif os.path.isfile(os.path.join(modulePath, "__init__.py")):
                modulePath = os.path.join(modulePath, "__init__.py")
        else:
            # Remove __init__ and .py from the base name and the directories after RS
            moduleName = os.path.splitext(modulePath)[0].replace(".__init__", "")
            moduleName = moduleName.replace(os.path.dirname(RS.Config.Script.Path.RockstarRoot), "")[1:].replace("\\",
                                                                                                                 ".")

        Database.ConnectToDatabase()
        Database.Log(moduleName, Database.Context.Closed, recordType=Database.Type.Error,
                     filename=moduleName, applicationId=Database.Application.MotionBuilder,
                     exception=exception, traceback=traceback.format_exc())
        ex.ExceptionHook.handleError(exc_type, exc_value, exc_traceback)

# # Entry Point # #


def Build():
    """ Builds the Rockstar Menu """
    global menuId

    GetRegisteredTools()

    # Starting menu id.
    menuId = 1

    menuMgr = mobu.FBMenuManager()

    # See if the menu already exists.
    rsMenu = menuMgr.GetMenu(RS_MAIN_MENU_NAME)

    if rsMenu:
        rsMenu.FBDelete()
        rsMenu = None

    # Menu doesn't exist, so create it.
    if not rsMenu:

        # Create highest-level Rockstar menu.
        menuMgr.InsertBefore(None, "&Help", RS_MAIN_MENU_NAME)
        menuMgr.InsertBefore(None, "&Help", 'Python Tools')
        # Create menu items for each registered tool.
        rsMenu = menuMgr.GetMenu(RS_MAIN_MENU_NAME)

    # Build the menu.
    RecurseBuildMenu(MenuTree, rsMenu)

    # Bind the event handler.
    rsMenu.OnMenuActivate.Add(OnMenuItem_Clicked)