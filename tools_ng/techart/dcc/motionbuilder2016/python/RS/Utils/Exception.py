"""
Tracks exceptions raised in Motion Builder and sends email reports about them
"""

import os
import re
import sys
import socket
import datetime
import traceback
import webbrowser
from PySide import QtGui, QtCore

from RS import Config, Perforce, ProjectData
from RS.Tools.UI import Application
from RS.Utils import Email
from RS.Utils.Logging import Database as Logging

try:
    from RS.Utils import Bugstar

    BUGSTAR = Bugstar.Bugstar()
except:
    BUGSTAR = None

PATHS = (Config.Script.Path.Root, r"x:\applications")


class SilentError(Exception):
    """
    Raises if not really an error, but aborting silently for whatever reason
    """

    pass


class PopupError(SilentError):
    """
    Doesn't send an error, but will generate a popup notifying the user of the issue.
    """

    def __init__(self, errorTitle, errorMessage):
        self.errorMessage = errorMessage
        self.errorTitle = errorTitle
        self.popup()

    def popup(self):
        return QtGui.QMessageBox.warning(
            Application.GetMainWindow(),
            str(self.errorTitle), str(self.errorMessage))


class PropertyError(Exception):
    """
    Error that has to do with interacting with FBProperties
    """

    pass


class ExceptionMode(object):
    """
    Class for holding values of the exception modes of the exception hook
    """
    Default = 0
    MotionBuilder = 1


class ExceptionHook(object):
    """
    Class that contains the logic for handling exceptions and sending error reports about them
    """

    def __init__(self, mode=ExceptionMode.Default, silent=False, sendExceptions=True):
        """
        Constructor

        Arguments:
            mode (ExceptionMode): The mode to set for handling errors
            silent (boolean): should the error dialog be suppressed when errors occur
            sendExceptions (boolean): should the hook send out email reports
        """
        self.mode = mode
        self.silent = silent
        self.suppressDialog = False
        self.sendExceptions = sendExceptions
        self.debug = False

        self._errors = []
        self._parameterHtmlValues = {
                                    # Default Value:    ("#e6e5c9", "normal", "Black", True)
                                    "Project":          ("#f2f2f2", "bold", "Black", True),
                                    "Module":           ("#f2f2f2", "normal", "Blue", True),
                                    "Line Number":      ("#f2f2f2", "normal", "Blue", True),
                                    "Function":         ("#f2f2f2", "normal", "Blue", True),
                                    "Exception":        ("#f2f2f2", "normal", "Blue", True),
                                    "Traceback":        ("#f2f2f2", "normal", "Red", True),
                                    "SMTP":             ("#f2f2f2", "normal", "Red", True),
                                    "Comments":         ("#f2f2f2", "normal", "Black", True),
                                    "Bugstar Number":   ("#f2f2f2", "bold", "Black", False)
                                    }

        self._htmlRowTemplate = (
                                '<tr>\n'
                                    '\t<td style="text-align: right; color: rgb(253, 253, 253); background-color: #2f323c;'
                                        'font-weight: bold; padding-left:10px; padding-right:4px; width:110px;" >\n'
                                        '{title}:</td>'
                                    '\t<td style="vertical-align: middle; padding-left: 10px; padding-left: 10px; '
                                        'text-align: justify; color: {fontColor}; background-color:{backgroundColor}; '
                                        'font-weight:{fontWeight};">'
                                        '{value}'
                                    '</td>\n'
                                '</tr>'
                                )
        self._ignore = ("<MotionBuilder>",)

    @property
    def defaultWarning(self):
        """
        The default warning to use for the exception dialog

        Return:
            string
        """

        text = "The following information will be emailed to support."
        if not Config.User.ExchangeServer:
            text = "Please email the following information to support."
        return ("An unhandled exception was caught by Rockstar!\n"
                "{}").format(text)

    def handler(self, exceptionType, exceptionValue, _traceback):
        """
        Method to hook into pythons exception hook

        Arguments:
            exceptionType (type): the type of exception
            exceptionValue (Exception): results of the exception
            _traceback (traceback): the traceback of the exception
        """
        if isinstance(exceptionValue, SilentError):
            return

        self.handleError(exceptionType, exceptionValue, _traceback)

    def handleError(self, exceptionType, exceptionValue, _traceback):
        """
        Core logic for handling exceptions

        Arguments:
            exceptionType (type): the type of exception
            exceptionValue (Exception): results of the exception
            _traceback (traceback): the traceback of the exception
        """
        print 'Unhandled exception caught by Rockstar!'

        # Print the exception to the Python console.
        traceback.print_exception(exceptionType, exceptionValue, _traceback)

        if not self.sendExceptions:
            print "Exception not emailed as user has turned off email notifications"
            return

        filename = getattr(exceptionValue, "filename", "<No File>")
        lineNum = getattr(exceptionValue, "lineno", -1)
        text = getattr(exceptionValue, "text", "")

        # Extrapolate the exception info. New logic that handles a traceback that is empty.
        filename, lineNum, funcName, text = extractInformationFromTraceback(_traceback,
                                                                            filename=filename,
                                                                            lineNum=lineNum,
                                                                            text=text)
        # Get the module name.

        # Escape \ so regex can understand them
        inValidDirectory = False
        for path in tracebackPaths(_traceback):
            absoluteFilename = os.path.abspath(path)
            moduleName = str(os.path.basename(absoluteFilename)).split('.')[0]

            inValidDirectory = re.match(r"|".join(PATHS).replace("\\", "\\\\"), path, re.I)
            # Did the exception happen from the sandbox folder, a location tech art does not track
            inSandbox = re.search("motionbuildersandbox|sandboxmotionbuilder", absoluteFilename, re.I)
            # Did the exception occur from a module that should be ignored.
            ignoreModule = moduleName in self._ignore

            if inSandbox or ignoreModule:
                inValidDirectory = False
                break

        user = Config.User.Email
        userEmail = Config.User.Email if Config.User.Email != "unknown" else Config.QA_OWNER_EMAIL.get(Config.User.Studio,
                                                                                    "svcrsgnycvmx@rockstargames.com")
        ignoreUser = Config.User.Email in Config.Script.Exception.IgnorableUsers
        emailList = Config.Script.Exception.EmailList
        emailList = emailList if not self.debug else [userEmail]

        # Only send exception if we aren't in the exception email list, i.e. Technical Artists.
        # and the script lives in one of the tech art repositories i.e. X:\wildwest

        if not self.debug and (ignoreUser or not inValidDirectory):
            print("Exception not emailed as user {} is {}".format(
                user, ("working outside the tech-art directories", "in the exception list")[ignoreUser]
            ))
            return
        # Get data for reporting the error and build the html
        subject, rows, tags = self.getModeData(exceptionType, exceptionValue, _traceback)
        html, width = self.buildHtml(rows)

        # Check if the exception has already been raised and reported.
        msgTup = (exceptionValue.__class__, exceptionValue.message, filename, lineNum)
        if msgTup in self._errors:
            print("Error has already been handled")
            return
        else:
            # Show the exception dialog.
            sendToBugsar, comment = self.show(html, width, buttonLabel="Send To Bugstar")
            if comment:
                rows.append(("Comments", comment))
                html, _ = self.buildHtml(rows)
            self._errors.append(msgTup)

        # Create frame data attachment
        frameData, success = createFrameDataFileAttachment(_traceback)
        attachments = [frameData] if success else []

        if sendToBugsar and BUGSTAR is not None:
            # Attempts to create a bug in bugstar
            bugId = sendBug(filename, self.displayText(rows), userEmail if self.debug else None, tags,
                            attachments=attachments)

            rows[0:0] = [("Bugstar Number", '<a href="bugstar:{bugId}">{bugId}</a>'.format(bugId=bugId))]
            html, _ = self.buildHtml(rows)

        # Attempt to email the exception.
        sendEmail(_traceback, emailList, subject, html, attachments)

        # add a bug number pop-up to inform user
        if sendToBugsar and BUGSTAR is not None:
            msgBox = QtGui.QMessageBox(Application.GetMainWindow())
            msgBox.setTextFormat(QtCore.Qt.RichText)
            msgBox.setWindowTitle("Bug Submitted!")
            msgBox.setText("<a href='http://bs/@bug/{0}'>Bugstar {0}</a>".format(bugId))
            msgBox.setStandardButtons(QtGui.QMessageBox.Ok)
            msgBox.setStyleSheet("QLabel{min-width: 120px;}")
            msgBox.exec_()
            print("Bug Submitted: http://bs/@bug/{0}".format(bugId))

    @staticmethod
    def emailTemplate():
        """ Path to email template """
        return os.path.join(Config.Tool.Path.TechArt, "etc", "config", "notificationTemplates","ToolsLoggingTemplate.html")

    def buildHtml(self, rows):
        """
        Builds the html to be shown to the user and emailed out to tech art support

        Arguments:
            rows (list): list of tuples, where each tuple contains a parameter name and value pair
        """

        rowHtmls = []
        titleColumnWidth = 0
        valueColumnWidth = 0
        for title, value in rows:
            _value = value
            isString = isinstance(value, basestring)
            backgroundColor, fontWeight, fontColor, disableHtml = self._parameterHtmlValues.get(
                                                                            title, ("#e6e5c9", "normal", "Black", True))

            if isString and disableHtml:
                value.replace("<", "&lt;").replace(">", "&gt;").replace(" ", "&nbsp;")
                value = re.sub("&lt;br&gt;|\n", "<br>", value)

            rowHtmls.append(self._htmlRowTemplate.format(title=title, backgroundColor=backgroundColor,
                                                         fontWeight=fontWeight, fontColor=fontColor, value=value))

            # resolve the width of the html that will be displayed

            _width = len(title)
            if _width > titleColumnWidth:
                titleColumnWidth = _width

            for string in str(_value).splitlines():
                _width = len(string)
                if _width > valueColumnWidth:
                    valueColumnWidth = _width

        with open(self.emailTemplate(), "r") as _file:
            html = _file.read()
            title = "Standalone"
            if self.mode == ExceptionMode.MotionBuilder:
                title = "Motion Builder"
            html = html.replace("#TITLE#", title)
            html = html.replace("#CONTENT#", "\n".join(rowHtmls))

        # The 4 is just some extra buffer width
        width = titleColumnWidth + valueColumnWidth

        return html, width

    def getModeData(self, exceptionType, exceptionValue,  _traceback):
        """
        Gets the data to build the html code for the email and bug based on the Exception Mode set.

        Arguments:
            exceptionType (type): the type of exception
            exceptionValue (Exception): results of the exception
            _traceback (traceback): the traceback of the exception

        Returns:
            tuple(string, list, list)
        """
        filename = getattr(exceptionValue, "filename", "<No File>")
        lineNum = getattr(exceptionValue, "lineno", -1)
        text = getattr(exceptionValue, "text", "")

        filename, lineNumber, funcName, text = extractInformationFromTraceback(_traceback,
            filename=filename, lineNum=lineNum, text=text)
        tracebackString = "".join(traceback.format_exception(exceptionType, exceptionValue, _traceback))
        moduleName = str(os.path.basename(filename)).split('.')[0]
        fileInfo = Perforce.GetFileState(filename.encode('string_escape').replace('\\\\', '\\'))

        # Default Values
        subject = "Python {} - Python Tools Exception: {}".format(re.search("[0-9.]*", sys.version).group(), moduleName)
        rows = [("Project", Config.Project.Name),
                ("Username", Config.User.Name),
                ("Host", socket.gethostname()),
                ("Timestamp", datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
                ("Python Filename", filename),
                ("Perforce Revision", "{} of {}".format(getattr(fileInfo, "HaveRevision", 0),
                                                 getattr(fileInfo, "HeadRevision", 0))),
                ("Module", moduleName),
                ("Line Number", lineNumber),
                ("Function", funcName),
                ("Exception", exceptionType.__name__),
                ("Traceback", tracebackString)]
        tags = []

        # Motion Builder Values

        if self.mode == ExceptionMode.MotionBuilder:
            from RS import Globals
            subject = "Motion Builder {} - Python Tools Exception: {}".format(Config.Script.TargetBuild, moduleName)
            rows[3:3] = [("MB Installed HF", os.path.basename(os.path.dirname(os.path.dirname(Globals.System.ApplicationPath))))]
            rows[6:6] = [("FBX Filename", Globals.Application.FBXFileName or "Empty FBX File")]
            tags = ['ta_motionbuilder_exception', 'Dont Track']

        return subject, rows, tags

    def displayText(self, rows):
        """
        converts the list of rows into text

        Arguments:
            rows (list): list of tuples, where each tuple contains a parameter name and value pair
        Return:
            string
        """
        return "\n".join(["{}: {}".format(name, value) for name, value in rows])

    def show(self, text, width, description=None, buttonLabel="", suppressComment=False):
        """
        Shows the error dialog and returns the if the user wants to send the error to bugstar and any comment
        they may have written

        Arguments:
            text (string): text or html to show to the user
            width (int): for the text or html to display
        Return:
            tuple (boolean, string)
        """
        description = description or self.defaultWarning
        if self.suppressDialog or self.silent:
            return False, ""
        return self._dialogClass.exception(None, description=description, text=text, width=width,
                                           buttonText=buttonLabel, suppressComment=suppressComment)

    def dialog(self):
        """ Class for the error dialog to use """
        return self._dialogClass

    def setDialog(self, dialogClass):
        """
        Sets which error dialog to use

        Arguments:
            dialogClass (QDialog): dialog to use for showing errors. The class being passed must be support a
                                   exception method that accepts a string and returns tuple containing a
                                   boolean if the dialog was accepted or not and a string.
        """
        self._dialogClass = dialogClass

    @staticmethod
    def register(mode=ExceptionMode.Default):
        """
        Registers the hook to the python interpreter.

        Arguments:
            mode (ExceptionMode): exception mode that determines what information to retrieve
        """
        ExceptionHook.mode = mode
        ExceptionHook.suppressDialog = True
        sys.excepthook = ExceptionHook.handler

    @staticmethod
    def deregister():
        """Deregisters the hook to the python interpreter."""
        sys.excepthook = sys.__excepthook__


def extractInformationFromTraceback(tb, filename="<No File>", lineNum=-1, funcName="None", text=""):
    """
    Extracts information from traceback if there is any to extract

    Arguments:
        tb (traceback): traceback to extract error details from
        filename (string): file where the error was encountered
        lineNum (int): line number for the line that errored out
        funcName (string): name of the function that errored out
        text (string): description of the error

    Return:
        string(filename), int(line number), string(method name), string(error string)
    """

    information = [filename, lineNum, funcName, text]
    for extract in traceback.extract_tb(tb):
        for index, data in enumerate(extract):
            information[index] = data

    return information


def tracebackPaths(_traceback):
    """
    Gets a list of the file paths for the modules in the tracback stack

    Arguments:
        _traceback (traceback): the traceback to get paths from

    Return:
        Generator
    """
    paths = []
    while _traceback:
        if _traceback.tb_frame.f_code.co_filename in paths:
            _traceback = _traceback.tb_next
            continue
        yield _traceback.tb_frame.f_code.co_filename
        paths.append(_traceback.tb_frame.f_code.co_filename)
        _traceback = _traceback.tb_next


def getFrameData(tb):
    """
    Gets all the locals for each frame of the exception level.

    Example:

        Frame DeleteSelectionSet in x:\wildwest\dcc\motionbuilder2014\python\RS\Tools\Animation\AnimToFBX\ToolBox.py at line 124
        self = <class 'RS.Tools.Animation.AnimToFBX.ToolBox.ToolBox'> <RS.Tools.Animation.AnimToFBX.ToolBox.ToolBox object at 0x0000000053F77A88>

        Frame DeleteSelectionSet in x:\wildwest\dcc\motionbuilder2014\python\RS\Tools\Animation\AnimToFBX\Controllers\ToolBox.py at line 207
        setNode = <type 'NoneType'> None
        mainWidget = <class 'RS.Tools.Animation.AnimToFBX.ToolBox.ToolBox'> <RS.Tools.Animation.AnimToFBX.ToolBox.ToolBox object at 0x0000000053F77A88>

    Arguments:
        tb(traceback): traceback for the error

    Returns:
        List of Strings, each line being a line of output from the frame in plain text
    """
    returnList = []
    while tb.tb_next:
        tb = tb.tb_next
    stack = []
    frame = tb.tb_frame
    while frame:
        stack.append(frame)
        frame = frame.f_back
    stack.reverse()
    traceback.print_exc()
    for frame in stack:
        returnList.append("")
        returnList.append("Frame {0} in {1} at line {2}".format(
                                                                frame.f_code.co_name,
                                                                frame.f_code.co_filename,
                                                                frame.f_lineno)
                          )
        for key, value in frame.f_locals.items():
            # The Try is to make sure we can convert the object to a string without error
            try:
                strValue = str(value)
            except:
                strValue = "<ERROR WHILE PRINTING VALUE>"

            if strValue != "":
                returnList.append("    {0} = {1} {2}".format(key, str(type(value)), strValue))
    return returnList


def createFrameDataFileAttachment(tb):
    """
    Creates a log file with the frame data from the error

    Arguments:
        tb (traceback): the traceback of the error to log

    Return:
        tuple(string, boolean)
    """
    exLogFile = os.path.join(Config.Tool.Path.Logs, "MotionBuilderToolsTraceback.txt")
    success = False
    try:
        frameData = getFrameData(tb)
        with open(exLogFile, 'w') as fstream:
            for line in frameData:
                fstream.write("{0}\n".format(line))
            print 'Unhandled exception has been logged to ({0}).'.format(exLogFile)
        success = True
    except:
        print 'An unknown error occurred trying to write out the exception log to ({0}).'.format(exLogFile)

    return exLogFile, success


def sendBug(filename, tracebackString, owner=None, tags=(), attachments=()):
    """
    Creates a bug on bugstar based on the traceback error provided

    Arguments:
        filename (string): the path of the file that caused the error
        tracebackString (string): traceback of the error
        owner (string): email of the owner of the bug, by default the bug is created to the tech art group
        tags (list): list of tags to add to the bug
    """
    # TODO: Thread Bug creation once latest custom cut of mobu with threading support is rolled out
    owner = owner or ProjectData.data.GetConfig("TechArtBugstarId", "Mark.Harrison-Ball@rockstargames.com")

    if not isinstance(traceback, basestring):
        tracebackString = str(tracebackString)

    # Names are now coming out with (Rockstar <studio name>) and the studio name should be stripped out
    application = "Python {}".format(re.search("[0-9.]*", sys.version).group())
    if ExceptionHook.mode == ExceptionMode.MotionBuilder:
        from RS import Globals
        application = "Motion Builder {}".format(Config.Script.TargetBuild)

    user = BUGSTAR.get_user_by_email(Config.User.Email or "").Name.split("(")[0].strip()
    userEmail = Config.User.Email if Config.User.Email !=  "unknown" else Config.QA_OWNER_EMAIL.get(
                                                                          Config.User.Studio,
                                                                          "Mark.Harrison-Ball@rockstargames.com")

    bug = BUGSTAR.create_bug("{application} - Caught Exception from {python_file} by {user}".format(
                             application=application, python_file=filename, user=user),
                             tracebackString, owner=owner, qaOwner=userEmail
                             , tags=tags, category="C", attachments=attachments)
    print("Bug sent")
    return bug.Id


def sendEmail(_traceback, emailTo, emailSubject, emailBody, attachments):
    """
    Sends an email of the error to the techart team

    Arguments:
        _traceback(traceback): traceback of the error
        emailTo (string): email adress(es) to send error report to
        emailSubject (string): the title of the email
        emailBody (string): the message/contents of the email.
        attachments (list): paths to files to attach to the email
    """
    if not Config.User.ExchangeServer:
        return
    email = Config.User.Email if not Config.User.Email.startswith("unknown") else Config.QA_OWNER_EMAIL.get(Config.User.Studio,
                                                                                    "svcrsgnycvmx@rockstargames.com")
    Email.Send(emailFrom=email, emailTo=emailTo, emailSubject=emailSubject, emailBody=emailBody,
                      attachments=attachments, asHtml=True)


def sendToDatabase(dbRawEx, filename, funcName, moduleName, text, p4revision, applicationId=Logging.Application.Default,
                   fbxFilename=""):
    """
    Logs the error to the database

    Arguments:
        dbRawEx (string): traceback of the error
        filename (string): name of the file
        funcName (string): name of the method
        moduleName (string): name of the module
        text (string): the error report
        p4revision (int): the perforce revision of the module that errored out
    """
    # TODO: Expand Database to separate data based on the application (Mobu, standalone python, etc.) and operating system
    try:
        Logging.Log(moduleName,
                    funcName,
                    applicationId=applicationId,
                    sceneFilename=fbxFilename,
                    recordType=Logging.Type.Error,
                    filename=filename,
                    perforceRevision=p4revision,
                    exception=text,
                    traceback=dbRawEx)
    except:
        pass

ExceptionHook = ExceptionHook()
