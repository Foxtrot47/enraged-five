'''
Vector math class.

Author: Jason Hayes (jason.hayes@rockstarsandiego.com)
'''

import math
import types

import pyfbsdk as mobu


class RsVector3( object ):
    __slots__ = ( 'x', 'y', 'z' )

    def __init__( self, x = 0.0, y = 0.0, z = 0.0 ):
        self.x = x
        self.y = y
        self.z = z


    ## Methods ##
    
    def convertFromFBVector3d( self, fbVector ):
        self.x = fbVector.GetList()[ 0 ]
        self.y = fbVector.GetList()[ 1 ]
        self.z = fbVector.GetList()[ 2 ]
         
    def getFBVector3d( self ):
        return mobu.FBVector3d( self.x, self.y, self.z )

    def angle( self, vec ):
        return math.acos( self.dot( vec ) )

    def cross( self, vec ):
        """
        Get the cross product of this vector with another vector.
        """
        x = ( ( self.y * vec.z ) - ( self.z * vec.y ) )
        y = ( ( self.z * vec.x ) - ( self.x * vec.z ) )
        z = ( ( self.x * vec.y ) - ( self.y * vec.x ) )

        return RsVector3( x, y, z )

    def dot( self, vec ):
        return float( self.x * vec.x + self.y * vec.y + self.z * vec.z )

    def zero( self ):
        """
        Zero's out the vector.
        """
        self.x = self.y = self.z = 0.0

    def identity( self ):
        """
        Zero's out the vector.
        """
        self.zero( )

    def clear( self, value ):
        """
        Clears the vector to the supplied value.
        """
        self.x = self.y = self.z = value

    def normalize( self ):
        """
        Normalizes this vector.
        """
        ln = self.length( )

        if ln == 0:
            return

        self.x /= ln
        self.y /= ln
        self.z /= ln

    def length( self ):
        """
        Returns the vector length.
        """
        return math.sqrt( ( self.x * self.x ) + ( self.y * self.y ) + ( self.z * self.z ) )

    def lengthSquared( self ):
        """
        Returns the vector length squared.  This is faster than the .length() method.
        """
        return ( ( self.x * self.x ) + ( self.y * self.y ) + ( self.z * self.z ) )

    def asTuple( self ):
        """
        Returns the vector as a tuple.
        """
        return ( self.x, self.y, self.z )

    def asList( self ):
        """
        Returns the vector as a list.
        """
        return [ self.x, self.y, self.z ]

    def asDictionary( self ):
        """
        Returns the vector as a dictionary where the keys are 'x', 'y', and 'z'.
        """
        return { 'x': self.x, 'y': self.y, 'z': self.z }

    ## Overloads ##

    def __str__( self ):
        return 'RsVector3( %.3f, %.3f, %.3f )' % ( self.x, self.y, self.z )

    def __repr__( self ):
        return str( self )

    def __eq__( self, v ):
        if type( v ) == types.IntType or type( v ) == types.FloatType:
            return self.x == v and self.y == v and self.z == v
            
        else:
            return self.x == v.x and self.y == v.y and self.z == v.z

    def __cmp__( self, v ):
        return self.__eq__( v )

    def __ne__( self, v ):
        return not self.__eq__( v )

    def __lt__( self, v ):
        if type( v ) == types.IntType or type( v ) == types.FloatType:
            if self.x < v:
                return True
    
            if self.y < v:
                return True
    
            if self.z < v:
                return True                        
            
        else:
            if self.x < v.x:
                return True
    
            if self.y < v.y:
                return True
    
            if self.z < v.z:
                return True

    def __le__( self, v ):
        if type( v ) == types.IntType or type( v ) == types.FloatType:
            if self.x <= v:
                return True
    
            if self.y <= v:
                return True
    
            if self.z <= v:
                return True
                        
        else:
            if self.x <= v.x:
                return True
    
            if self.y <= v.y:
                return True
    
            if self.z <= v.z:
                return True

    def __gt__( self, v ):
        if type( v ) == types.IntType or type( v ) == types.FloatType:
            if self.x > v:
                return True
    
            if self.y > v:
                return True
    
            if self.z > v:
                return True    
                        
        else:
            if self.x > v.x:
                return True
    
            if self.y > v.y:
                return True
    
            if self.z > v.z:
                return True

    def __ge__( self, v ):
        if type( v ) == types.IntType or type( v ) == types.FloatType:
            if self.x >= v:
                return True
    
            if self.y >= v:
                return True
    
            if self.z >= v:
                return True
            
        else:
            if self.x >= v.x:
                return True
    
            if self.y >= v.y:
                return True
    
            if self.z >= v.z:
                return True

    def __add__( self, v ):
        if isinstance( v, RsVector3 ):
            return RsVector3( self.x + v.x, self.y + v.y, self.z + v.z )

        else:
            return RsVector3( self.x + v, self.y + v, self.z + v )

    def __sub__( self, v ):
        if isinstance( v, RsVector3 ):
            return RsVector3( self.x - v.x, self.y - v.y, self.z - v.z )

        else:
            return RsVector3( self.x - v, self.y - v, self.z - v )

    def __div__( self, v ):
        if isinstance( v, RsVector3 ):
            return RsVector3( self.x / v.x, self.y / v.y, self.z / v.z )

        else:
            return RsVector3( self.x / v, self.y / v, self.z / v )

    def __mul__( self, v ):
        if isinstance( v, RsVector3 ):
            return RsVector3( self.x * v.x, self.y * v.y, self.z * v.z )

        else:
            return RsVector3( self.x * v, self.y * v, self.z * v )

    def __neg__( self ):
        return RsVector3( -self.x, -self.y, -self.z )



def CloneVector(vectorToClone):
    """
    Clone a FBVector3d object
    
    Args:
         vectorToClone (FBVector3d): The vector3d to be cloned 
     
    Return:
         FBVector3d: A clone of the original vector 
    """
    newVector = mobu.FBVector3d()
    for channel in xrange(3):
        newVector[channel] = vectorToClone[channel]
    return newVector
