'''
Description:
    Provides methods for reading the machines system info

Example:
    import RS.Utils.SystemInfo as sysinfo
    sysinfo.get.gpu
    sysinfo.get.cpu
    sysinfo.get.os
    sysinfo.get.totalRam
    sysinfo.get.hdTotal
'''
__author__ = 'mharrison-ball'


import ctypes
import math
import os
import platform
import re
import subprocess
import getpass
import sys

try:
    import _winreg
except:
    pass

LINUX_CONST = "Linux"
WINDOWS_CONST = "Windows"


class MemoryStatusStruct(ctypes.Structure):
    _fields_ = [
    ("dwLength", ctypes.c_ulong),
    ("dwMemoryLoad", ctypes.c_ulong),
    ("ullTotalPhys", ctypes.c_ulonglong),
    ("ullAvailPhys", ctypes.c_ulonglong),
    ("ullTotalPageFile", ctypes.c_ulonglong),
    ("ullAvailPageFile", ctypes.c_ulonglong),
    ("ullTotalVirtual", ctypes.c_ulonglong),
    ("ullAvailVirtual", ctypes.c_ulonglong),
    ("sullAvailExtendedVirtual", ctypes.c_ulonglong),
    ]

    def __init__(self):
        '''
         have to initialize this to the size of MEMORYSTATUSEX

         :return:
        '''
        self.dwLength = ctypes.sizeof(self)
        super(MemoryStatusStruct, self).__init__()


def getRegistryValue(key, subkey, value):
    '''

    :param key:
    :param subkey:
    :param value:
    :return:
    '''
    try:
        key = getattr(_winreg, key)    
        handle = _winreg.OpenKey(key, subkey)    
        (value, type) = _winreg.QueryValueEx(handle, value)
    except:
        return ""
    return value


class SystemInformation(object):
    '''
        Get System Info
    '''
    def __init__(self):
        '''

        :return:
        '''
        super(SystemInformation, self).__init__()
        self._os = self._osVersion().strip()
        self._cpu = self._cpu().strip()
        self._gpu = self._gpu().strip()
        self._browsers = self._browsers()
        self._ram = self._ram()
        self._totalRam = math.ceil((self._ram.ullTotalPhys / 1024 /1024 /1000 ))
        self._hdTotal = math.ceil(self._disk_c() / (1024*1024*1024))
        self._user = getpass.getuser()


    @property
    def user(self):
        return self._user

    @property
    def os(self):
        return self._os

    @property
    def cpu(self):
        return self._cpu
    
    @property
    def gpu(self):
        return self._gpu
    
    @property
    def browsers(self):
        return self._browsers
    
    @property
    def ram(self):
        return self._ram
    
    @property
    def totalRam(self):
        return self._totalRam

    @property
    def hdTotal(self):
        return self._hdTotal

    def _get(key, value):
        return getRegistryValue(key, value)

    def _osVersion(self):
        '''
            Get OS version

        :return:
        '''
        # Linux Op
        if platform.system() == LINUX_CONST:
            osName = platform.platform() 
            build = platform.version()
            return "{0} ({1})".format(osName, build)
        
        # Windows Op
        elif platform.system() == WINDOWS_CONST:
            key = "HKEY_LOCAL_MACHINE"
            subkey = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"
            
            osName = getRegistryValue(key, subkey,"ProductName")
            sp = getRegistryValue(key, subkey,"CSDVersion")
            build = getRegistryValue(key, subkey,"CurrentBuildNumber")
            return "{0} {1} (build {2})".format(osName, sp, build)
        return platform.system()
    
    def _cpu(self):
        '''
            Get CPU spec
        :return:
        '''
        # Linux Op
        if platform.system() == LINUX_CONST:
            command = "cat /proc/cpuinfo"
            allInfo = subprocess.check_output(command, shell=True).strip()
            for line in allInfo.split("\n"):
                if "model name" in line:
                    return re.sub(".*model name.*:", "", line, 1)

        # Winows Op
        elif platform.system() == WINDOWS_CONST:
            return getRegistryValue(
                "HKEY_LOCAL_MACHINE", 
                "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0",
                "ProcessorNameString")
        return None
    
    def _gpu(self):
        '''

            Get GPU
        :return:
        '''
        # Linux Op
        if platform.system() == LINUX_CONST:
            command = "lspci"
            deviceInfo = subprocess.check_output(command, shell=True).strip()
            for line in deviceInfo.split("\n"):
                if "VGA compatible controller" in line:
                    return re.sub(".*VGA compatible controller.*:", "", line, 1).strip()

        # Winows Op
        elif platform.system() == WINDOWS_CONST:
            return getRegistryValue(
                "HKEY_LOCAL_MACHINE",
                "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winsat",
                "PrimaryAdapterString")
        return ""

    def _firefoxVersion(self):
        '''
            Get Firefox Version

        :return:
        '''
        # Linux Op
        if platform.system() == LINUX_CONST:
            return os.popen("firefox --version").read().strip()

        # Windows Op
        elif platform.system() == WINDOWS_CONST:
            try:
                version = getRegistryValue(
                    "HKEY_LOCAL_MACHINE", 
                    "SOFTWARE\\Mozilla\\Mozilla Firefox",
                    "CurrentVersion")
                return (u"Mozilla Firefox", version)
            except WindowsError:
                pass
        return None

    def _iexploreVersion(self):
        '''
             Get Internet Explorer Version

        :return:
        '''
        # Windows Op
        if platform.system() == WINDOWS_CONST:
            try:
                version = getRegistryValue(
                    "HKEY_LOCAL_MACHINE", 
                    "SOFTWARE\\Microsoft\\Internet Explorer",
                    "Version")
                return (u"Internet Explorer", version)
            except WindowsError:
                pass
        return None

    def _browsers(self):
        '''
            Get Firefox and Explorer version

        :return:
        '''
        browsers = []
        firefox = self._firefoxVersion()
        if firefox:
            browsers.append(firefox)
        iexplore = self._iexploreVersion()
        if iexplore:
            browsers.append(iexplore)
            
        return browsers

    def _ram(self):
        '''
            Get Ram info

        :return:
        '''
        self.stat = MemoryStatusStruct()
       
        # Linux Op
        if platform.system() == LINUX_CONST:
            meminfo = open('/proc/meminfo').read()
            memList = [mem for mem in meminfo.split("\n") if mem != ""]
            memDict = {}
            for memLine in memList:
                parts = [mem for mem in memLine.strip().split(" ") if mem != ""]
                try:
                    memDict[parts[0].split(":")[0]] = int(parts[1])
                except:
                    pass
            self.stat.dwMemoryLoad = int(memDict['MemFree']/float(memDict['MemTotal']) * 100)
            self.stat.ullTotalPhys = memDict['MemTotal'] * 1000
            self.stat.ullAvailPhys = memDict['MemFree']
            self.stat.ullTotalPageFile = memDict['PageTables']
            self.stat.ullAvailPageFile = 0 # Page files dont have an upper limit in linux
            self.stat.ullTotalVirtual = memDict['VmallocTotal']
            self.stat.ullAvailVirtual = memDict['VmallocTotal'] - memDict['VmallocUsed']
            self.stat.sullAvailExtendedVirtual = 0 # docs say its always 0

        # Windows Op
        elif platform.system() == WINDOWS_CONST:
            ctypes.windll.kernel32.GlobalMemoryStatusEx(ctypes.byref(self.stat))
        return self.stat

    def _disk_c(self):
        '''
            Get HD primary HD space
        '''
        # Linux Op
        if platform.system() == LINUX_CONST:
            statvfs = os.statvfs("/home")
            return statvfs.f_frsize * statvfs.f_blocks
        
        # Windows Op
        elif platform.system() == WINDOWS_CONST:
            drive = unicode(os.getenv("SystemDrive"))
            freeuser = ctypes.c_int64()
            total = ctypes.c_int64()
            free = ctypes.c_int64()
            ctypes.windll.kernel32.GetDiskFreeSpaceExW(drive, 
                                            ctypes.byref(freeuser), 
                                            ctypes.byref(total), 
                                            ctypes.byref(free))
            return total.value
        return None

get = SystemInformation()
