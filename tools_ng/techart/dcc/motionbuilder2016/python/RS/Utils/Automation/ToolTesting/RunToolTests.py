"""
Get list of tools in our menu config
Run each tool and test to see if it runs or not, log if fails

Database: http://animtracking.rockstargames.com/Pages/Mobu/MobuToolsStatus.aspx
"""
import os
import sys
import csv
import webbrowser
import xml.etree.cElementTree

from inspect import isroutine

import pyfbsdk as mobu

from PySide import QtGui


from RS import Config, Databases
from RS.Tools.UI import Commands
from RS.Utils.Logging import Universal


USE_DB = True
DB_CONN = None
DB_CURSOR = None


class FakeFBMessageBox(object):
    """
    Fake Mobu Messagebox class so it does not popup
    """
    def __init__(self):
        self._hasOpened = False

    def FBMessageBox(self, pBoxTitle, pMessage, pButton1Str="Ok", pButton2Str=None, pButton3Str=None, pDefaultButton=0, pScrolledMessage=0):
        self._hasOpened = True
        return pDefaultButton


class FakeQWidget(object):
    """
    Fake QWidget class so it does not popup
    """
    def __init__(self):
        self._hasOpened = False

    def show(self):
        self._hasOpened = True


class FakeFBFilePopup(object):
    """
    Fake Mobu File open class so it does not popup
    """
    def __init__(self):
        self._hasOpened = False

    def Execute(self):
        self._hasOpened = True
        return False


class FakeFBMessageBoxWithCheck(object):
    """
    Fake Mobu messagebox with check class so it does not popup
    """
    def __init__(self):
        self._hasOpened = False

    def FBMessageBoxWithCheck(self, pBoxTitle, pMessage, pButton1Str="Ok", pButton2Str=None, pButton3Str=None, pCheckBoxValuep=False, DefaultButton=0, pScrolledMessage=0):
        self._hasOpened = True
        return pDefaultButton


class FakeFBMessageBoxGetUserValue(object):
    """
    Fake Mobu messagebox with user input class so it does not popup
    """
    def __init__(self):
        self._hasOpened = False

    def FBMessageBoxGetUserValue(self, pBoxTitle, pMessage, pValue, pValueType, pButton1Str="Ok", pButton2Str=None, pButton3Str=None, pDefaultButton=0, pLastButtonCancel=True):
        self._hasOpened = True
        return pDefaultButton


class FakeShowTool(object):
    """
    Fake Mobu Show tool class so it does not popup
    """
    def __init__(self):
        self._hasOpened = False

    def ShowTool(self, tool):
        self._hasOpened = True


class FakeStartFile(object):
    """
    Fake system start file class so it does not open
    """
    def __init__(self):
        self._hasOpened = False

    def startfile(self, str):
        self._hasOpened = True


class FakeWebBrowser(object):
    """
    Fake web browser class so it does not open any webpages
    """
    def __init__(self):
        self._hasOpened = False

    def open(self, str):
        self._hasOpened = True


class FakeULogger(object):
    """
    Fake ULogger class so it does not open
    """
    def __init__(self):
        self._hasOpened = False

    def Show(self, ulogFilename, modal=True):
        self._hasOpened = True


class ToolInfo(object):
    """
    Hold the tool information
    """
    def __init__(self, path, menuName):
        self.menuName = menuName
        self.Path = path
        self.IsCommand = False
        self.UnitTest = True


def ConnectToDatabase():
    """
    Connect to the  logging table in the global tools database.

    Return:
        boolean
    """
    global DB_CONN
    global DB_CURSOR

    DB_CONN = Databases.NYC.RockstarTools()
    DB_CONN.Connect()
    if DB_CONN.IsConnected:
        DB_CURSOR = DB_CONN.Cursor

        return True

    return False


def writeDBtoolStatus(name, module, path, status, testing = "Complete"):
    '''
    :param name:
    :param module:
    :param path:
    :param status:
    :return:
    '''
    sql_cmd = ("exec updateMBUnitTest"
                 """ "{0}", '{1}', '{2}', {3}, {4}, '{5}'""").format(
                                                    name,
                                                    module,
                                                    path,
                                                    status,
                                                    int(Config.Script.TargetBuild),
                                                    testing
                                                     )

    if DB_CONN.IsConnected:
        DB_CONN.ExecuteQuery(sql_cmd)


def writeCVStoolStatus(csvWriter, index, name, module, path, status):
    '''
    Wrapper around CSV Writer with database logging support
    :param csvWriter:
    :param index:
    :param name:
    :param module:
    :param path:
    :param status:
    :return:
    '''
    csvWriter.writerow([index, name, module, path, status])
    if USE_DB:
        writeDBtoolStatus(name, module, path, status)


def getMenuTools():
    '''
    get a list of menu items, pass them to RunTools method
    '''
    configFile = Config.Script.Path.MenuConfig
    doc = xml.etree.cElementTree.parse(configFile)
    root = doc.getroot()

    moduleList = []

    if root:
        RecurseParseConfig(root, moduleList)

    RunTools(moduleList)


def RecurseParseConfig(xmlRoot, moduleList):
    '''
    :param xmlRoot:
    :param tree:
    :return:
    '''
    xmlSubMenuNodes = xmlRoot.findall('submenu')
    xmlMenuNodes = xmlRoot.findall('menu')

    if not xmlMenuNodes:
        return
    for xmlNode in xmlMenuNodes:
        menuName = xmlNode.get('displayName')

        try:
            modulePath = xmlNode.get('modulePath')
            if modulePath:
                _toolInfo = ToolInfo(modulePath, menuName)
                moduleList.append(_toolInfo)
        except:
            pass

        try:
            commandPath = xmlNode.get('command')
            if commandPath:
                _toolInfo = ToolInfo(commandPath, menuName)
                _toolInfo.IsCommand = True
                moduleList.append(_toolInfo)
        except:
            pass

        try:
            unitTest = xmlNode.get('unitTest')
            if unitTest:
                _toolInfo.UnitTest = False
        except:
            pass

    if xmlSubMenuNodes:
        for xmlNode in xmlSubMenuNodes:
            RecurseParseConfig(xmlNode, moduleList)


def RunTools(moduleList):
    ConnectToDatabase()

    reportFile = open(os.path.join(Config.Project.Path.Root, 'ToolReport.csv'), 'wb')
    csvWriter = csv.writer(reportFile)

    # Store Original methods for Monkey Patches
    monkeyPatchDict = {}
    monkeyPatchDict[(mobu, "FBMessageBox")] = mobu.FBMessageBox
    monkeyPatchDict[(mobu, "FBMessageBoxWithCheck")] = mobu.FBMessageBoxWithCheck
    monkeyPatchDict[(mobu, "FBMessageBoxGetUserValue")] = mobu.FBMessageBoxGetUserValue
    monkeyPatchDict[(mobu, "ShowTool")] = mobu.ShowTool
    monkeyPatchDict[(mobu.FBFilePopup, "Execute")] = mobu.FBFilePopup.Execute
    monkeyPatchDict[(os, "startfile")] = os.startfile
    monkeyPatchDict[(webbrowser, "open")] = webbrowser.open
    monkeyPatchDict[(QtGui.QWidget, "show")] = QtGui.QWidget.show
    monkeyPatchDict[(Universal, "Show")] = Universal.Show

    fakeFBMessageBox = FakeFBMessageBox()
    fakeQWidget = FakeQWidget()
    fakeFBMessageBoxWithCheck = FakeFBMessageBoxWithCheck()
    fakeFBMessageBoxGetUserValue = FakeFBMessageBoxGetUserValue()
    fakeFBFilePopup = FakeFBFilePopup()
    fakeShowTool = FakeShowTool()
    fakeWebBrowser = FakeWebBrowser()
    fakeStartFile = FakeStartFile()
    fakeULogger = FakeULogger()

    # Apply Monkey Patches
    mobu.FBMessageBox = fakeFBMessageBox.FBMessageBox
    mobu.FBMessageBoxWithCheck = fakeFBMessageBoxWithCheck.FBMessageBoxWithCheck
    mobu.FBMessageBoxGetUserValue = fakeFBMessageBoxGetUserValue.FBMessageBoxGetUserValue
    mobu.ShowTool = fakeShowTool.ShowTool
    webbrowser.open = fakeWebBrowser.open
    os.startfile = fakeStartFile.startfile
    mobu.FBFilePopup.Execute = fakeFBFilePopup.Execute
    QtGui.QWidget.show = fakeQWidget.show
    Universal.Show = fakeULogger.Show

    # carry out methods for running the tests and outputting test info to DB & csv
    try:
        idx = 0
        for module in  moduleList:
            if module.UnitTest is True:
                # initialise module & run
                result = initModule(module, idx)

                # write out test result to DB
                writeDBtoolStatus(module.menuName, module.Path, "", result, "")

                # write out test result to csv
                if module.IsCommand:
                    moduleAttr = getattr(Commands, module.Path)
                else:
                    moduleAttr = sys.modules[module.Path]

                if isroutine(moduleAttr):
                    writeCVStoolStatus(csvWriter, idx, module.menuName, moduleAttr.func_name, moduleAttr.func_code.co_filename, result)
                else:
                    writeCVStoolStatus(csvWriter, idx, module.menuName, moduleAttr.__name__, moduleAttr.__file__, result)
            else:
                #  write out skipped modules to DB
                writeDBtoolStatus(module.menuName, module.Path, "", False, "Skipped")
            idx += 1

    except Exception as inst:
        print inst

        pass
    finally:
        # Un-Monkey Patch
        for key, value in monkeyPatchDict.iteritems():
            module, method = key
            setattr(module, method, value)
    reportFile.close()


def initModule(ModuleInfo, index):
    try:

        if not ModuleInfo.IsCommand:
            if ModuleInfo.Path not in sys.modules:
                try:
                    __import__(ModuleInfo.Path, globals(), locals(), [], -1)
                except:
                    pass

            try:
                module = sys.modules[ModuleInfo.Path]
                reload(module)
            except:
                return False


        elif ModuleInfo.IsCommand:
            module = getattr(Commands, ModuleInfo.Path)

        try:
            return runModule(module)
        except:
            return False

    except():
        return False


def runModule(module):

    Succesfull = False

    if isroutine(module):
        try:
            module()
            return True
        except:
            return False

    else:
        try:
            module.Run()
            return True
        except:
            return False


def Run(show=False):
    getMenuTools()
