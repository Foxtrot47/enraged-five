"""
Description:
    These are methods for reading, writing and removing data from the Motion Builder mbtools.ini file.
    The mbtools.ini file is used to set the settings for our custom tools.

Example:
    import RS.Utils.UserPreferences as userPref

    # Enable the cutscene menu when opening motion builder
    userPref.__Saveini__("menu","Enable Cutscene Menu", source.State)

    # Disable the cutscene menu when opening motion builder
    userPref.__Readini__("menu","Enable Cutscene Menu", True)

"""

# Config Parser
import os
import tempfile
import ConfigParser


DEFAULT_SETTINGS_PATH = tempfile.gettempdir() + "\\mbtools.ini"


def __Saveini__(section, attribute, value):
    """
    Basic method to write a single attribute and value to the INI file settings

    Arguments:
        section (string): ini section
        attribute (string): the attribute you want to assign the value to
        value (string): value to store
    """
    Config = ConfigParser.ConfigParser()
    Config.read(DEFAULT_SETTINGS_PATH)

    if not Config.has_section(section):
        Config.add_section(section)
    Config.set(section, attribute, value)

    with open(DEFAULT_SETTINGS_PATH, 'w') as cfgfile:
        Config.write(cfgfile)


def __Readini__(section, attribute, default_value=False, returnValue=False, notFoundValue=False):
    """
    Reads an attribute from INI

    Arguments:
        section (string): ini section
        attribute (string): the attribute you want value to
        default value (boolean): if value not found, will return false
        returnValue (boolean): forces the value
        notFoundValue (boolean): if it can't find the value return the value of notFoundValue

    Return:
        string or boolean
    """
    value = default_value
    Config = ConfigParser.ConfigParser()
    if os.path.exists(DEFAULT_SETTINGS_PATH):
        Config.read(DEFAULT_SETTINGS_PATH)
        if Config.sections():
            try:
                if attribute in ["OperatorUserName", "FocusMarkerScale", "LensSetting",
                                 "VehicleTypeSelected", "RenderFolder", "Set Date",
                                 "avidrender", "igrender"] or returnValue:
                    value = Config.get(section, attribute)

                elif attribute in ["VehicleTypeSelectedIndex", "VehicleSelectedIndex", "RenderExtensionIndex",
                                   "PictureFormatIndex", "audioDest"]:
                    value = Config.getint(section, attribute)

                else:
                    value = Config.getboolean(section, attribute)

            except:
                return notFoundValue

    return value


def __RemoveSection__(section):
    """
    Removes the section from the INIT file

    Arguments:
        section (string): section
    """
    Config = ConfigParser.ConfigParser()
    Config.read(DEFAULT_SETTINGS_PATH)

    if Config.has_section(section):
        Config.remove_section(section)

    with open(DEFAULT_SETTINGS_PATH, 'w') as cfgfile:
        Config.write(cfgfile)
