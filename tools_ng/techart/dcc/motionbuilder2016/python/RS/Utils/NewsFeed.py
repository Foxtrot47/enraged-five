"""
Shows the latest updates for Motion Builder that have been done by the Tech Art Team

Image:
    $TechArt/Tools/NewsFeed.png
"""
__author__ = 'mharrison-ball'
import os

import clr

import RS
import RS.Perforce as p4

clr.AddReference("RSG.TechArt.NewsFeed")
from RSG.TechArt.Newsfeed import RsNewsFeed

TAG = "#tag:MOBU"

#if RS.Config.Script.DevelopmentMode:
#    TAG = "#tag:MOBO_DEV"


def Run(forceshow=False):
    """
    Shows the News Feed WPF window

    Image:
        $TechArt/Tools/NewsFeed.png

    Arguments:
        forceshow (boolean): show the widget
    """
    args = [
            os.path.join(RS.Config.Tool.Path.Root,  'wildwest\\etc\\config\\general\\newsFeeds.xml'),
            os.path.join(RS.Config.Project.Path.Root,  'docs\\TechArt\\NewsFeeds\\...')
            ]
    p4.Run("sync", args)

    RsNewsFeed.Show( TAG, forceshow)