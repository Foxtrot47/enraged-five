"""
RS.Utils.Bugstar

Module for interfacing with Bugstar.

"""

import re
import clr
import webbrowser
from functools import wraps

from RS import ProjectData
import System

clr.AddReference("RSG.Base")    
from RSG.Base import Logging

clr.AddReference("RSG.Configuration")
from RSG import Configuration

clr.AddReference("RSG.Interop.Bugstar")
from RSG.Interop import Bugstar as IBugstar
from RSG.Interop.Bugstar.Organisation import Project
from RSG.Interop.Bugstar import AttachmentBuilder
from RSG.Interop.Bugstar import BugCategory
from RSG.Interop.Bugstar import BugBuilder
from RSG.Interop.Bugstar import BugPriority

# Decorator


def cache(func):
    """
    caches the value of the decorated function as a variable of the same name but that has a leading underscore.
    This is done to avoid calling bugstar multiple times to retrieve the same data. Calls to bugstar are slow, so
    we want to minimize the number of times that we try to access it as much as possible.

    Arguments:
        func (method): the method which you are decorating, automatically gets passed when using the decorator
        expression

    Returns:
        func
    """
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        # Create the variable name from the method name
        func_name = func.func_name
        cached_attribute_name = "_{}".format(func_name)

        # Get the value of the method from the object passed in
        attribute_value = getattr(self, cached_attribute_name, None)

        # Set the value of the variable by running the method
        # If it hasn't been cached
        if not attribute_value:
            setattr(self, cached_attribute_name, func(self,*args, **kwargs))

        return getattr(self, cached_attribute_name, None)

    return wrapper

# Class


class Bugstar(object):

    category = {    "A": BugCategory.A,
                    "B": BugCategory.B,
                    "C": BugCategory.C,
                    "D": BugCategory.D,
                    "Task": BugCategory.Task,
                    "Todo": BugCategory.Todo,
                    "Track": BugCategory.Track}

    priority = {    "High": BugPriority.High,
                    "Low" : BugPriority.Low,
                    1: BugPriority.High,
                    2: BugPriority.P2,
                    3: BugPriority.P3,
                    4: BugPriority.P4,
                    5: BugPriority.Low}

    def __init__(self, username="gameasserts", password="gamea$$ert5", domain=""):
        """
        logins into the server based on the credentials passed in. If no credentials are
        passed in then the generic login info from North will be used.

        The domain for rockstar is rockstar.t2.corp

        Arguments:
            username (string): your login name ei. dvega
            password (string): your password
            domain (string): the domain that you are trying to access ei. rockstar.t2.corp
            connection (Bugstar.BugstarConnection): the connection instance to bugstar

        """
        self._config = None
        self.loggedin = False
        self.login(username, password, domain)

    def _AssertProject(self, project):
        """
        check that the project is a RS.Interop.Bugstar.Project

        Arguments:
            project (RS.Interop.Bugstar.Project): object to check if it is a RS.Interop.Bugstar.Project

        """
        assert isinstance(project, Project)

    def login(self, username="gameasserts", password="gamea$$ert5", domain=""):
        """
        logins into the server based on the credentials passed in. If no credentials are
        passed in then the generic login info from North will be used.

        The domain for rockstar is rockstar.t2.corp

        Arguments:
            username (string): your login name ei. dvega
            password (string): your password
            domain (string): the domain that you are trying to access ei. rockstar.t2.corp
        """

        try:
            self.connection.Login(username, password, domain)
            self.clear_cache()
            self.loggedin = True
        except:
            self.loggedin = False

    def connected(self):
        """
        Is this class connected to bugstar

        Return:
            boolean
        """
        return self.loggedin

    @property
    @cache
    def connection(self):
        """
        gets the bugstar connection instance

        Return:
            BugstarConnection instance
        """
        return IBugstar.BugstarConnection(self.config.RESTService, self.config.AttachmentService)

    @property
    @cache
    def config(self):
        """
        Generates a Bugstar Config instance to use

        Return:
            BugstarConfig instance
        """
        if self._config is None:
            config = Configuration.ConfigFactory.CreateConfig(Logging.LogFactory.ApplicationLog)
            self._config = Configuration.ConfigFactory.CreateBugstarConfig(Logging.LogFactory.ApplicationLog, config.Project)
        return self._config

    @property
    @cache
    def project(self):
        """
        Query the assigned Bugstar project for the current project.

        Returns:

            RSG.Model.Bugstar.Organisation.Project object.
        """
        self._project_id = ProjectData.data.GetConfigDict().get("ProjectBugstarId", self.config.ProjectId)
        return Project.GetProjectById(self.connection, self._project_id)

    @property
    @cache
    def projects(self):
        """
        Collect all of the users projects as a dictionary.

        Returns:
            Dictionary of RSG.Model.Bugstar.Organisation.Project objects.
            The key is the name of the project.
        """
        projects = Project.GetProjects(self.connection)
        return {project.Name: project for project in projects}

    def get_project_by_name(self, project_name ):
        """
        Get a Bugstar project.

        Arguments:
            projectName (string): The name of the project to get.

        Returns:
            RSG.Model.Bugstar.Organisation.Project object.
        """

        return self.projects.get(project_name, None)

    @property
    @cache
    def user(self):
        """
        Returns the name of the current user

        Return: string;
            The name of the current user
        """
        return self.project.CurrentUser

    @property
    @cache
    def users(self):
        """
        Gets a list of all the users on the project
        """

        # This regular expression is so we get names without the studio they work at
        # ei. David Vega (R*NYC) to david vega

        user_name = re.compile("[a-z\- ]+", re.I)
        user_name_match = user_name.match
        lower = str.lower
        users = {lower(user_name_match(user.Name.encode('utf-8')).group()[:-1]): user for user in self.project.Users}
        return users

    def get_user_by_name( self, name):
        """
        Get a Bugstar user.

        Arguments:
            name (string): The friendly name of the user to look up.  Ei. 'Jason Hayes'

        Returns:
            If the user exists in the project, will return a RSG.Model.Bugstar.Organisation.User object.
            None if the user doesn't exist in the project.
        """

        self._AssertProject(self.project)
        return self.users.get(name.lower(), None)

    def get_user_by_email(self, email):
        """
        Get a Bugstar user via their email address

        Arguments:
            email (string): the email address for the user that you want. ei. david.vega@rockstargames.com
            project (RSG.Interop.Bugstar.Organisation.Project): The project instance generated from
                    RSG.Interop.Bugstar.Organisation.Project()

        Return:
            RSG.Interop.Bugstar.Organisation.User or None
        """
        lower = unicode.lower
        email_dictionary = {lower(user.Email): user for user in self.project.Users if user.Email}
        return email_dictionary.get(email.lower(), None)

    def get_user_id(self, user=None):
        """
        Retrieves the user id of the provided user

        Arguments:
            user (string or RSG.Interop.Bugstar.Organisation.User): the name, email or User instance of the person
                    who's user Id you want to get
            project (RSG.Interop.Bugstar.Organisation.Project): the current project

        Return:
            int 
        """

        # Owner
        if not user:
            return self.user.Id

        if isinstance(user, basestring):
            if "@" in user:
                user = self.get_user_by_email(user)
            else:
                user = self.get_user_by_name(user)

        elif isinstance(user, int):
            return user

        return user.Id

    @property
    @cache
    def teams(self):
        """ List of teams on bugstar """
        lower = str.lower
        return {lower(user.Name.encode('utf-8')): user for user in self.project.GetTeamsList()}

    def get_team_by_name(self, name):
        return self.team.get(name, None)

    @property
    @cache
    def members(self):
        """	List of all users and teams on bugstar """
        members = dict(self.users)
        members.update(self.teams)
        return members

    def get_member_by_name(self, name):
        """
        Gets the bugstar object for a user or team based on their name

        Arguments:
            name (string): name of the user or team

        Return:
            int or None
        """
        return self.members.get(name.lower(), None)

    def get_member_id(self, member):
        """
        Gets the bugstar id for a user or team based on their name or email address

        Arguments:
            member (string): name or email of the user or team

        Return:
            int or None
        """
        if not member:
            return self.user.Id

        if isinstance(member, basestring):
            if "@" in member:
                member = self.get_user_by_email(member)
            else:
                member = self.get_member_by_name(member)

        elif isinstance(member, int):
            return member
        if member:
            return member.Id

    def open_bug(self, bugId ):
        """
        Opens the supplied bug.

        Arguments:
            bugId (int): The bug id to open.
        """
        webbrowser.open('url:bugstar:{0}'.format(bugId))

    def create_bug(self,
                   summary,
                   description,
                   owner       = None,
                   qaOwner     = None,
                   reviewer    = None,
                   ccList      = (),
                   category    = "Task",
                   priority    = 3,
                   tags        = (),
                   attachments = ()):
        """
        Creates a bug.

        Arguments:

            summary (string): The summary for the bug.
            description (string): The description for the bug.
            owner (string): name, email or bugstar id for the user who will own the bug
            qaOwner (string): name, email or bugstar id for the user who will be qa for the bug
            reviewer (string): name, email or bugstar id for the user who will be the reviewer for a bug.
                               currently unused as the list of permitted reviewers is low
            ccList (list): list of names, emails, and/or bugstar ids for the users who should be cc'd on the buck
            category (string): category for the bug
            priority (int): priority for the bug
            tags (list): list of tags (strings) to add to the bug
            attachments (list): list of paths to files to attach to the bug

        Returns:
            RSG.Interop.Bugstar.Bug
        """

        self._AssertProject(self.project)

        bugBuilder = BugBuilder(self.project)
        bugBuilder.Summary = summary
        bugBuilder.Description = description
        bugBuilder.Category = self.category.get(category, BugCategory.Task)
        bugBuilder.Priority = self.priority.get(priority, BugPriority.P3)

        if owner and qaOwner:
            # Owner
            bugBuilder.DeveloperId = self.get_member_id(owner)
            # QA Owner
            bugBuilder.TesterId = self.get_member_id(qaOwner)

            # Reviewer shouldn't be set- the reviewer list for RDR2 is limited, if set and the reviewer is not on the
            # list it will throw a 505 Internal Error

            # CC List
            cc_list = [self.get_member_id(each) for each in ccList]
            bugBuilder.CCList = System.Array[System.UInt32](cc_list)

            # Tags
            for tag in tags:
                bugBuilder.Tags.Add(tag)

            attachmentBuilder = AttachmentBuilder(self.project)
            attachments = [attachmentBuilder.ToAttachment(self.connection, attachment).Id
                           for attachment in attachments]
            attachments = System.Array[System.UInt32](attachments)
            bugBuilder.Attachments = attachments

            # Create the bug.
            return bugBuilder.ToBug(self.connection)

    def get_bug_by_id( self, bugId ):
        """
        Get a bug by the supplied id.

        Arguments:

            bugId: The bug id to query.

        Returns:

            RSG.Model.Bugstar.Bug object, None if the bug could not be found.
        """
        # Storing project variable to avoid calling bugstar multiple times

        bug = IBugstar.Bug( self.project )
        found = bug.GetBugById( self.project, bugId )

        if found:
            return found

        else:
            current_projects = dict(self.projects)
            current_projects.pop(self.project.Name)
            for project_name, project in current_projects.iteritems():
                bug = IBugstar.Bug( project )
                found = bug.GetBugById( project, bugId )

                if found:
                    return found

        return None

    def clear_cache(self):
        """ Sets the value of the cached attributes to None so the cached attributes can be set again """
        [setattr(self, each, None) for each in dir(self)
         if re.match("_(?=[a-z]+)", each) and each not in ("_config", "_connection", "_AssertProject")]
