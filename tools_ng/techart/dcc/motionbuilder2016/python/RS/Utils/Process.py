'''
Description:
    Module to deal with Windows processes.
    
Author:
    Jason Hayes <jason.hayes@rockstarsandiego.com>
'''

import os
import subprocess
import ctypes

from System.Diagnostics import Process
from System.Diagnostics import ProcessWindowStyle

def IsRunning( processName ):
    '''
    Check to see if a process is running.
    '''
    matchingProcesses = Process.GetProcessesByName( processName );
    if len(matchingProcesses) > 0:
        return True
    else:
        return False

def GetCount( processName ):
    '''
    Get the number of times a process is running.
    '''
    matchingProcesses = Process.GetProcessesByName( processName );
    return len(matchingProcesses)
    

def GetId( processName ):
    '''
    Get the process id.
    '''
    s = subprocess.Popen( 'wmic PROCESS get Caption,ProcessId', stdout = subprocess.PIPE )
    processName = processName.lower()        
        
    for line in s.stdout:
        result = line.split()
        
        if result:
            proc = result[ 0 ].lower()
        
            if proc == processName:
                return int( result[ 1 ] )
                  
    return None

def Start( processFilePath, silent = True, waitForExit = False , *args ):
    
    process = Process()
    process.StartInfo.FileName = processFilePath
    
    for arg in args:
        process.StartInfo.Arguments += str(arg) + " "
        
    process.StartInfo
    if silent:
        process.StartInfo.UseShellExecute = False
        process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
        process.StartInfo.CreateNoWindow = True
        
    process.Start()
    
    if waitForExit:
        process.WaitForExit() 
        
def KillAll(processName):
    matchingProcesses = Process.GetProcessesByName( processName );
    for matchingProcess in matchingProcesses:
        matchingProcesses.Kill()
        
def Kill( processName ):
    '''
    Kill a process by name.
    '''
    pid = GetId( processName )
    
    if pid:
        handle = ctypes.windll.kernel32.OpenProcess( 1, False, pid )
        
        if handle:
            ctypes.windll.kernel32.TerminateProcess( handle, -1 )
            ctypes.windll.kernel32.CloseHandle( handle )
        
            return True
        
    return False
    