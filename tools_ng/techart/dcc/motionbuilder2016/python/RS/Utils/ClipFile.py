import clr

clr.AddReference("RSG.ManagedRage")

from RSG.ManagedRage.ClipAnimation import MClip

FBX_SOURCE_PROPERTY = "FBXFile_DO_NOT_RESOURCE"

# dn = DotNet
class ClipFile (object):
    def __init__(self, ClipFilePath):
        MClip.Init()
        self._dnClip = MClip( MClip.ClipType.Normal )
        self._dnClip.Load( ClipFilePath )
        
    def GetProperty(self, PropertyName ):
        for dnClipProperty in self._dnClip.GetProperties():
            if dnClipProperty.GetName().lower() == PropertyName.lower():
                return ClipProperty( dnClipProperty )
                
class ClipProperty (object):
    def __init__(self, dnClipProperty):
        self._dnClipProperty = dnClipProperty
        
    def GetAttribute(self, AttributeName ):
        for dnClipAttribute in self._dnClipProperty.GetPropertyAttributes():
            if dnClipAttribute.GetName().lower() == AttributeName.lower():
                return ClipAttribute(dnClipAttribute)


STRING_ATTRIBUTE_TYPE = "MPropertyAttributeString"
BOOL_ATTRIBUTE_TYPE = "MPropertyAttributeBool"
FLOAT_ATTRIBUTE_TYPE = "MPropertyAttributeFloat"
INT_ATTRIBUTE_TYPE = "MPropertyAttributeInt"
HASHSTRING_ATTRIBUTE_TYPE = "MPropertyAttributeHashString"

class ClipAttribute (object):
    def __init__(self, dnClipAttribute):
        self._dnClipAttribute = dnClipAttribute
        
    def GetValue( self ):
        attributeType = self._dnClipAttribute.__class__.__name__
        
        if attributeType == STRING_ATTRIBUTE_TYPE:
            return str(self._dnClipAttribute.GetString())
        if attributeType == BOOL_ATTRIBUTE_TYPE:
            return self._dnClipAttribute.GetBool()
        if attributeType == FLOAT_ATTRIBUTE_TYPE:
            return self._dnClipAttribute.GetFloat()
        if attributeType == INT_ATTRIBUTE_TYPE:
            return self._dnClipAttribute.GetInt()
        if attributeType == HASHSTRING_ATTRIBUTE_TYPE:
            return str(self._dnClipAttribute.GetString())
            
        raise Exception("Unsupported clip attribute type: {0}".format( attributeType ) )
                
##test = ClipFile(r"X:\gta5\assets\cuts\AH_1_INT_p2\CS_LesterCrest.clip")
##testProperty = test.GetProperty('FBXFile_DO_NOT_RESOURCE')
##testAttr = testProperty.GetAttribute('FBXFile_DO_NOT_RESOURCE')
##print (testAttr.GetValue() )
##testProperty = test.GetProperty('ExporterType')
##testAttr = testProperty.GetAttribute('ExporterType')
##print (testAttr.GetValue() )

