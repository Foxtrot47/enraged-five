
import os
import pyfbsdk
from pyfbsdk import *

def RemoveCallback_Facial():
    FBSystem().OnUIIdle.Remove( AutoRunProcess_Facial )

def AutoRunProcess_Facial(pCaller, pEvent):
    if 'PROCESSFACIAL' in os.environ.keys():
        RemoveCallback_Facial()
        
        import RS.Core.Automation.Cutscene.AutoImplement
        
        RS.Core.Automation.Cutscene.AutoImplement.AutoProcessFile(os.environ['PROCESSFACIAL'])
        
        
        
    else:
        print "ERROR: no environment var 'PROCESSFACIAL'"
        print "ENV: {0}".format(os.environ.keys())