'''
This scripot is called by the Rage Animation Exporter
There is a checkbox called Render which will trigger this script before continuing to export

The script is data driven and is location is defined in x:\[Project]]\tools_*\etc\export.xml

Author: Mark Harrison-Ball <mark.harrison-Ball@rockstargames.com>
'''
#import RS.Tools.UI.Animation.IgaRenderUI as IGRender
#IGRender.Run()