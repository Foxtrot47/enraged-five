import pyfbsdk
from pyfbsdk import *
from fbx import *

import os
import math
import time
import random
import shutil
import hashlib
import colorsys
import uuid
import subprocess
import re
import pstats
import cProfile

import RS.Core.Metadata
import RS.Config
import RS.Globals
import RS.Perforce
import RS.Utils.Scene
import RS.Utils.Scene.Model
import RS.Utils.Scene.Skin
import RS.Utils.Logging.Universal
import RS.Utils.Color
import RS.Utils.Instrument
import RS.Utils.Math

from System import DateTime
from System.Xml import XmlTextReader
from System.Xml import XmlDocument

#We don't use techart path here because we want the project techart path not WW
GRADING_CONFIG_FILE_PATH = os.path.join( RS.Config.Tool.Path.Root, r"techart\etc\config\grading\config.xml" )
GRADING_IMAGE_PROCESSOR = os.path.join( RS.Config.Tool.Path.Root, r"techart\bin\GradingImageProcessor\GradingImageProcessor.exe" )

class MissingExportCameraError (Exception):
    def __init__(self):
        self.message = "Missing export camera."
    
class Job:
    def __init__( self, ID ):
        
        self._ID = ID
        self._configFile = RS.Core.Metadata.ParseMetaFile( GRADING_CONFIG_FILE_PATH )

        self._jobDirectory = str( os.path.join( self._configFile.JobRootPath, RS.Config.Project.Name , ID ) )
        
        self._logFile = RS.Utils.Logging.Universal.UniversalLog( "job.process", True, False, self._jobDirectory )
        self._profileFilePath = os.path.join( self._jobDirectory, "job.profile" )
        self._profileRawFilePath = os.path.join( self._jobDirectory, "job.profile.raw" )
        
        self._logMessage("Motionbuilder process picked up job [{0}].".format(ID))
        
        try:
            self._logMessage("Parsing job.info file.")
            self._jobFilePath = os.path.join( self._jobDirectory, "job.info" )
            self._jobFile = RS.Core.Metadata.ParseMetaFile( self._jobFilePath )
        except Exception as e:
            self._logError("Failed to parse job.info file.")
            raise e
        
        try:
            self._logMessage("Parsing source.cut file.")
            self._cutFilePath = os.path.join( self._jobDirectory, "source.cut" )
            
            #now use xml parser rather than the metadata system because metadata system 
            #doesn't play nice with the cutxmls
            cutXmlReader = XmlTextReader( self._cutFilePath )
            self._cutFile = XmlDocument()        
            self._cutFile.Load( cutXmlReader )
            
        except Exception as e:
            self._logError("Failed to parse source.cut file.")
            self._logError(e.message)
            raise e        
        
        self._fbxFilePath = os.path.join( self._jobDirectory, "source.fbx")
        
        #Instrument pyfbsdk so we get accurate profiling
        self._logMessage("Setting up for profiling")
        RS.Utils.Instrument.InstrumentModule( pyfbsdk )        
    
    def _createMaterial( self, MaterialName, MaterialColor ):
        material = FBMaterial( MaterialName )
        material.Emissive = MaterialColor
        material.EmissiveFactor = 1.0
        material.DiffuseFactor = 0.0
        material.SpecularFactor = 0.0
        material.AmbientFactor = 0.0
        return material    
    
    def _logMessage(self, Message):
        self._logFile.LogMessage( Message )

    def _logWarning(self, Message):
        self._logFile.LogWarning( Message ) 
    
    def _logError(self, Message):
        self._logFile.LogError( Message )

    def _preProcessComponent( self, Component ):
        
        if issubclass( type( Component ), FBModel ):

            #Check if we are dealing with head accessories or hair
            if re.search( "HEAD.*HAT|HEAD.*HELMET|HEAD.*CAP|HEAD.*BERET|HEAD.*BEANIE|HAIR", Component.FullName, re.IGNORECASE ):
                Component.Show = False              
        
    def _getEntityType( self, cutObjectElement ):
            
            cutObjectType = cutObjectElement.GetAttribute("type")
            
            #See if this matches any of our entity types
            for entityType in self._configFile.EntityTypes:
                if re.search( entityType.CutType, cutObjectType, re.IGNORECASE ):
                    
                    #If it does we'll grab the ctrl file and see if this is on the list for this object type
                    cutObjectCtrlFile = cutObjectElement.SelectSingleNode("cAnimExportCtrlSpecFile").InnerText
                    for ctrlFile in entityType.CtrlFiles:
                        if re.search( ctrlFile, cutObjectCtrlFile, re.IGNORECASE ):
                            #If we have a matching ctrl file then return this object type
                            return entityType
                        
            #We don't have any entity types that match the passed cutObjectElement so return None
            return None                
    
    def _getEntityStreamingName( self, cutObjectElement ):
        
        #Get the streaming name directly
        streamingName = cutObjectElement.SelectSingleNode("cName").InnerText.split(':')[0]
        streamingName = streamingName.split('^')[0]
        return streamingName
    
    def _preProcessFbx( self ):
            
        self._logMessage( "Initializing FBXSDK." )
        sdkManager = FbxManager.Create()
        if not sdkManager:
            raise Exception( "Failed to initialize FBXSDK." )
        ios = FbxIOSettings.Create(sdkManager, IOSROOT)
        sdkManager.SetIOSettings(ios)
        scene = FbxScene.Create(sdkManager, "")
        
        self._logMessage( "Mounting source.fbx file." )
        importer = FbxImporter.Create( sdkManager, "" )    
        result = importer.Initialize( self._fbxFilePath, -1, sdkManager.GetIOSettings())
        if not result:
            raise Exception( "Failed to mount source.fbx with FBXSDK." )
        importer.Import(scene)
        importer.Destroy()       

        self._logMessage( "Processsing geometry." )
        
        for nodeIndex in range( scene.GetNodeCount() ):
            node = scene.GetNode(nodeIndex)

            geometry = node.GetGeometry()
            if isinstance( geometry, FbxMesh ):
                layer = geometry.GetLayer(0)
                material = FbxLayerElementMaterial.Create( geometry, "material" )
                material.SetMappingMode( FbxLayerElement.eByPolygon )
                material.SetReferenceMode( FbxLayerElement.eDirect )
                material.GetIndexArray().SetCount( geometry.GetPolygonCount() )      
                for i in range( geometry.GetPolygonCount() ):
                    material.GetIndexArray().SetAt(i, 0)                  
                layer.SetMaterials( material )
    
        self._logMessage( "Saving out processed source.fbx." )
        exporter = FbxExporter.Create(sdkManager, "")
        fileFormat = -1
        if fileFormat < 0 or fileFormat >= sdkManager.GetIOPluginRegistry().GetWriterFormatCount():
            fileFormat = sdkManager.GetIOPluginRegistry().GetNativeWriterFormat()
            formatCount = sdkManager.GetIOPluginRegistry().GetWriterFormatCount()
            for formatIndex in range(formatCount):
                if sdkManager.GetIOPluginRegistry().WriterIsFBX(formatIndex):
                    writerFormatDescription = sdkManager.GetIOPluginRegistry().GetWriterFormatDescription(formatIndex)
                    if "FBX binary" in writerFormatDescription:
                        fileFormat = formatIndex
    
        ios = FbxIOSettings.Create(sdkManager, IOSROOT)
        ios.SetBoolProp(EXP_FBX_COMPRESS_ARRAYS, True)
        ios.SetIntProp(EXP_FBX_COMPRESS_MINSIZE, 1024) #1 Megabyte
        ios.SetIntProp(EXP_FBX_COMPRESS_LEVEL, 1)
    
        if exporter.Initialize( self._fbxFilePath, fileFormat, ios ):
            exporter.Export(scene)
            exporter.Destroy()
        else:
            raise Exception( "Failed to save out source.fbx with FBXSDK." )
        
        
    def Start(self):
        
        try:
            #Start profiling
            profile = cProfile.Profile()
            profile.enable()                
    
            #Check that the source cut has some shot information
            concatDataListElement = self._cutFile.DocumentElement.SelectSingleNode("concatDataList")
            if concatDataListElement == None:
                raise Exception("No <concatDataList> element in the cut xml.")
            if concatDataListElement.ChildNodes.Count < 1:
                raise Exception("No shot information in the cut file.")
    
            #Preprocess the fbx scene
            self._logMessage("Preprocessing source.fbx file." )
            self._preProcessFbx()
            
            #Open the fbx scene
            self._logMessage("Opening source.fbx file." )
            FBApplication().FileOpen( self._fbxFilePath )
            
            #Fix faces (jaw error)
            import RS.Core.Face.Lib
            RS.Core.Face.Lib.FixFaceInCutscene(True)
            
            #Preprocess all the components
            self._logMessage("Preprocessing components" )
            for component in RS.Globals.Scene.Components:
                self._preProcessComponent( component )
    
            #Start our tracking dictionary
            trackedEntityManager = TrackedEntityManager()
            
            #Get our xml element - should only have one of them
            cutsceneObjectsElement = self._cutFile.DocumentElement.SelectSingleNode( "pCutsceneObjects" )
            if cutsceneObjectsElement == None:
                raise Exception("No <pCutsceneObjects> element in the cut xml.")
                
            #For each object in the cutscene
            self._logMessage( r"Processing cut rage objects" )
            for cutObjectElement in cutsceneObjectsElement.ChildNodes:
                
                cutObjectID = int( cutObjectElement.SelectSingleNode( "iObjectId" ).GetAttribute("value") )
                self._logMessage( r"Analyzing cut rage object. ID: {0} Type: {1}".format( cutObjectID, cutObjectElement.GetAttribute("type") ) )
        
                #Get entity type
                entityType = self._getEntityType( cutObjectElement )
                if entityType == None:
                    self._logMessage( r"Ignoring cut rage object - unknown/supported type" )
                    continue
                else:
                    self._logMessage( r"Processing entity as: {0}".format( entityType.Name ) )
                
                #Get entity name
                entityStreamingName = self._getEntityStreamingName( cutObjectElement )
                self._logMessage( r"Entity name: {0}".format( entityStreamingName ) )
                
                #Get the root model. If we failed to find the root model then continue to next object. This is bad.
                entityRootModelName = str( cutObjectElement.SelectSingleNode("cName").InnerText )
                entityRootModel = FBFindModelByLabelName( entityRootModelName ) 
                if entityRootModel == None: 
                    self._logWarning( r"Failed to find root model: {0}".format( entityRootModelName ) )
                    continue
                else:
                    self._logMessage( r"Obtained root model: {0}".format( entityRootModelName ) )
                
                #Setup our tracked entity
                trackedEntity = trackedEntityManager.Create( entityStreamingName, entityType, entityRootModel )
                
                #Get all child elements of the root model (likely be the bone models)
                boneModels = list()
                RS.Utils.Scene.GetChildren( entityRootModel, boneModels )
                
                #Go through each boneModel exclude them from the list if they match the BoneExclusionPattern
                if trackedEntity.GetType().BoneExclusionPattern != None:
                    for boneModel in boneModels:
                        if re.search( trackedEntity.GetType().BoneExclusionPattern, boneModel.Name, re.IGNORECASE ) != None:
                            self._logMessage( r"Excluding bone [{0}] with pattern: {1}.".format( boneModel.Name, trackedEntity.GetType().BoneExclusionPattern ) )
                            boneModel.Scaling.Data = FBVector3d(0.0001,0.0001,0.0001)
                    
                #Get the skin models that these models act as bones for (if any)
                skinModels = RS.Utils.Scene.Model.GetSkinModels( boneModels )
                
                #If we have some skinned models then we are dealing with a skinned object (majority of objects)
                if len( skinModels ) > 0:
                    
                    self._logMessage( r"Processing as skinned entity." )
                    
                    #For each skinned model
                    for skinModel in skinModels:
                        self._logMessage( r"Processing skinned model: {0}.".format( skinModel.Name ) )                    
                        
                        #Check the model to see if it should be excluded by pattern against name
                        if trackedEntity.GetType().ModelExclusionPattern != None:
                            if re.search( trackedEntity.GetType().ModelExclusionPattern, skinModel.Name, re.IGNORECASE ) != None:
                                self._logMessage( r"Excluding model with pattern: {0}.".format( trackedEntity.GetType().ModelExclusionPattern ) )
                                skinModel.Show = False
                                continue
                
                        #If model is visible at this point then we should process it
                        if RS.Utils.Scene.Model.IsModelVisible( skinModel ):
                            trackedEntity.AddModel( skinModel )
                        else:
                            self._logMessage( r"Model is not visible so ignoring." )
                
                #Otherwise we are dealing with a simple 'non-skinned' entity
                else:
                    self._logMessage( r"Processing as simple (flat) entity" )
                    trackedEntity.AddModel( entityRootModel )
                    
                #Check we actually have some models (this can happen if all of an object's models are hidden)
                if( trackedEntity.GetModelCount() == 0 ):
                    trackedEntityManager.Remove( trackedEntity )
                    self._logWarning( r"Entity has no visible\valid models so ignoring" )
                else:
                    self._logMessage( r"Finished processing entity. Entity is composed of [{0}] model(s) and has [{1}] controller(s)".format( trackedEntity.GetModelCount(), trackedEntity.GetControllerCount() ) )                        
            
            
            self._logMessage( r"Setting up scene for render" )
            
            #Now we have our objects collected we will gather all the models that relate to them so we can hide 
            #everything else
            modelList = trackedEntityManager.GetModels()            
    
            #Need to also gather and process the set models
            setRootModels = list( component for component in RS.Globals.Components if component.Name == "SetRoot" and type(component) == FBModelNull )             
            for setRootModel in setRootModels:
                setRootModelChildren = list()
                RS.Utils.Scene.GetChildren( setRootModel, setRootModelChildren ) 
                for child in setRootModelChildren:
                    if type(child) == FBModel:
                        model = child
                        if re.search( "DOOR|CYLINDER", model.Name, re.IGNORECASE ):
                            model.Show = False
                        else:
                            model.Show = True
                            colorValue = random.uniform(0.75, 0.95)
                            color = FBColor( colorValue, colorValue, colorValue )                
                            newMaterial = self._createMaterial( model.FullName, color )
                            model.Materials.removeAll()
                            model.Materials.append( newMaterial )
                            modelList.append(model)                        
            
            #Hide everything that is not on this list (not the static set or exporting objects.)
            for component in RS.Globals.Components:
                if type( component ) == FBModel:
                    component.Shaders.removeAll()
                    component.ShadingMode = FBModelShadingMode.kFBModelShadingHard
                    if component not in modelList:
                        component.Show = False
                        
            #Now we start outputting the data. First get the export camera and validate it.
            exportCamera = FBFindModelByLabelName( 'ExportCamera' )
            if( type( exportCamera ) != FBCamera ):
                self._logWarning("Missing export camera so falling back to camera switcher.")
                RS.Globals.System.Renderer.UseCameraSwitcher = True
                cameraProcessList = RS.Globals.Scene.Cameras #Process all cameras
            else:
                cameraProcessList = [exportCamera]
                #Make sure we are not on camera switcher (important as this overrides 'CurrentCamera')
                RS.Globals.System.Renderer.UseCameraSwitcher = False
                RS.Globals.System.Renderer.CurrentCamera = exportCamera
                
            for camera in cameraProcessList:             
                #Setup resolution and render options
                #Unlock all properties
                for prop in camera.PropertyList:
                    if prop.AllowsLocking ():
                        prop.SetLocked(False)
                        
                camera.CameraResolution = FBCameraResolutionMode.kFBResolutionCustom
                camera.ResolutionWidth = self._configFile.RenderWidth
                camera.ResolutionHeight = self._configFile.RenderHeight
                camera.ViewShowAxis = False
                camera.ViewShowGrid = False
                camera.BackGroundColor = FBColor(0.5,0.5,0.5)
                camera.FrameColor = FBColor(0.1,0.1,0.1)
                camera.UseFrameColor = True            

            #This helps with the poping in about out of objects (mobu optimization)
            RS.Globals.System.Renderer.FrustumCulling = False
            
            #Set up our render options
            grabber = FBVideoGrabber()
            renderOptions = grabber.GetOptions()
            renderOptions.CameraResolution = FBCameraResolutionMode.kFBResolutionCustom
            renderOptions.ViewingMode = FBVideoRenderViewingMode.FBViewingModeModelsOnly
            renderOptions.ShowSafeArea = False
            renderOptions.ShowTimeCode = False
            renderOptions.ShowCameraLabel = False
            renderOptions.AntiAliasing = False
            renderOptions.RenderAudio = False
            
            #Do a throw grab - this kick starts the grabber otherwise first frame is empty
            grabber.RenderSnapshot()
            
            #Output all our shot data for the part
            for concatDataElement in concatDataListElement.ChildNodes:
                                
                shotName = concatDataElement.SelectSingleNode("cSceneName").InnerText             
                shotStartFrame = int( concatDataElement.SelectSingleNode("iRangeStart").GetAttribute( "value" ) )
                shotEndFrame = int( concatDataElement.SelectSingleNode("iRangeEnd").GetAttribute( "value" ) )
                shotDataDirectory = os.path.join( self._jobDirectory, shotName )
                
                self._logMessage( "Outputting data for shot: [{0}]".format(shotName) )                
                self._logMessage( "Frames: [{0} - {1}].".format( shotStartFrame, shotEndFrame ) )
                
                renderOptions.OutputFileName = str( os.path.join( shotDataDirectory, "#####.tif" ) )
                    
                shotInfo = trackedEntityManager.GetShotInfo()
                shotInfo.DataJobID = self._ID
                shotInfo.DataTimeStamp = str( DateTime.Now.ToString() )
                shotInfo.SourceCutFileRevision = self._jobFile.SourceCutFileRevision
                shotInfo.SourceCutFilePath = self._jobFile.SourceCutFilePath
                shotInfo.SourceFbxFileRevision = self._jobFile.SourceFbxFileRevision
                shotInfo.SourceFbxFilePath = self._jobFile.SourceFbxFilePath       
                shotInfo.FrameCount = shotEndFrame - shotStartFrame
                shotInfoFilePath = os.path.join( shotDataDirectory, "shot.data" )        
                shotInfo.SetFilePath( shotInfoFilePath )        
                shotInfo.Save()        
                
                frameTime = FBTime()
                frameIndex = 0
                for sourceFrameIndex in range( shotStartFrame, shotEndFrame ):
                    
                    #Output a message every ten frames which we use to register if MoBu has hung
                    if( frameIndex % 10 == 0 ):
                        self._logMessage("Processing frame: {0} ({1}).".format( frameIndex, sourceFrameIndex ))
            
                    #Move to correct frame
                    frameTime.SetFrame( sourceFrameIndex )
                    RS.Globals.Player.Goto( frameTime )	
                    
                    #Process our objects and output or frame info data
                    frameInfo = trackedEntityManager.GetFrameInfo()
                    frameInfo.FrameIndex = frameIndex
                    frameInfo.SourceFrameIndex = sourceFrameIndex
                    frameInfoFileName = "{0:0>5d}.frame.data".format( sourceFrameIndex )
                    frameInfoFilePath = os.path.join( shotDataDirectory, frameInfoFileName )
                    frameInfo.SetFilePath( frameInfoFilePath ) 
                    frameInfo.Save()
                    
                    #Get the name and path
                    frameImageFileName = "{0:0>5d}.tif".format( sourceFrameIndex )
                    frameImageFilePath = os.path.join( shotDataDirectory, frameImageFileName )                    

                    #Output our image data
                    #renderOptions.TimeSpan = FBTimeSpan( frameTime, frameTime )
                    #RS.Globals.App.FileRender( renderOptions )
                    
                    #Using snapshot instead
                    frameImage = grabber.RenderSnapshot()
                    frameImage.WriteToTif( str(frameImageFilePath), "", True )
                    
                    #Send image off for post processing
                    startupinfo = subprocess.STARTUPINFO()
                    startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW            
                    subprocess.Popen( [GRADING_IMAGE_PROCESSOR, frameImageFilePath, frameInfoFilePath, shotInfoFilePath, GRADING_CONFIG_FILE_PATH ], startupinfo=startupinfo )
                    
                    frameIndex += 1
            
            self._logMessage("Finished outputting data")
            
            #Stop profiling and dump the raw data
            profile.disable()
            profile.dump_stats( self._profileRawFilePath )               
            
            #Process data to human readable information
            profileFileStream = open( self._profileFilePath, 'w' )
            stats = pstats.Stats( self._profileRawFilePath, stream = profileFileStream )
            stats.strip_dirs()
            stats.sort_stats( 'time' )
            stats.print_stats()
            profileFileStream.close()
            
            #Remove the raw file now as it's useless
            os.remove(self._profileRawFilePath)
            
        except Exception as e :
           if(e.message == None or e.message == ""):
               self._logError( str(e) )
           else:
               self._logError(e.message)

            
class TrackedEntityManager:
    
    def __init__( self ):
        self._entityList = []
        self._entityControllerDict = {}

        colorHueStep = 1.0 / 360
        self._availableColors = list()
        for i in range( 0, 360 ):
            colorRGB = colorsys.hsv_to_rgb( colorHueStep * i, 1.0, 1.0 )
            self._availableColors.append( FBColor( colorRGB[0], colorRGB[1], colorRGB[2]  ) )
    
    def GetNextColor( self ):
        chosenColor = random.choice( self._availableColors )
        self._availableColors.remove( chosenColor )
        return chosenColor              

    def Create( self, EntityName, EntityType, EntityRootModel  ):
        newTrackedEntity = TrackedEntity( self, EntityName, EntityType, EntityRootModel )
        self._entityList.append( newTrackedEntity )
        return newTrackedEntity
    
    def Remove( self, TrackedEntityToRemove ):
        self._entityList.remove( TrackedEntityToRemove )
        
    def _getController( self, Model ):
        if self._entityControllerDict.has_key( Model ):
            return self._entityControllerDict[ Model ]
        else:
            newController = TrackedEntityController( Model )
            self._entityControllerDict[ Model ] = newController
            return newController
    
    def GetModels( self ):
        modelList = []
        for trackedEntity in self._entityList:
            modelList.extend( trackedEntity.GetModels() )
        return modelList
    
    def GetFrameInfo( self ):
        
        #Evaluate on this frame
        self.Evaluate()
        
        #Output our frame info
        frameInfo = RS.Core.Metadata.CreateMetaFile( "GradingFrameInfo" )        

        #Get our camera
        camera = RS.Globals.System.Renderer.CurrentCamera
        cameraMatrix = FBMatrix()
        camera.GetMatrix( cameraMatrix )
        
        cameraPosition = FBVector4d()
        FBMatrixToTranslation( cameraPosition, cameraMatrix )        
        frameInfo.CameraPosition.X = cameraPosition[0]
        frameInfo.CameraPosition.Y = cameraPosition[1]
        frameInfo.CameraPosition.Z = cameraPosition[2]
        
        cameraRotation = FBVector3d()
        FBMatrixToRotation( cameraRotation, cameraMatrix, FBRotationOrder.kFBXYZ )  
        frameInfo.CameraRotation.X = cameraRotation[0]
        frameInfo.CameraRotation.Y = cameraRotation[1]
        frameInfo.CameraRotation.Z = cameraRotation[2]        
        frameInfo.CameraFOV = camera.FieldOfView.Data
        
        for trackedEntity in self._entityList:

            frameInfoEntity = frameInfo.Entities.Create()
            frameInfoEntity.ID = str( trackedEntity.GetID() )
            
            trackedEntityRootTranslation = trackedEntity.GetRootTranslation()
            frameInfoEntity.RootPosition.X = trackedEntityRootTranslation[0]          
            frameInfoEntity.RootPosition.Y = trackedEntityRootTranslation[1]
            frameInfoEntity.RootPosition.Z = trackedEntityRootTranslation[2]
            
            trackedEntityRootRotation = trackedEntity.GetRootRotation()
            frameInfoEntity.RootRotation.X = trackedEntityRootRotation[0]          
            frameInfoEntity.RootRotation.Y = trackedEntityRootRotation[1]
            frameInfoEntity.RootRotation.Z = trackedEntityRootRotation[2]            
            
            trackedEntityAverageTranslation = trackedEntity.GetAverageGlobalTranslation()
            frameInfoEntity.Position.X = trackedEntityAverageTranslation[0]          
            frameInfoEntity.Position.Y = trackedEntityAverageTranslation[1]
            frameInfoEntity.Position.Z = trackedEntityAverageTranslation[2]            
                        
            for part in trackedEntity.GetParts():
                if part.HasControllers():
                    partInfo = frameInfoEntity.Parts.Create()
                    partInfo.Name = part.GetName()
                    
                    partAverageGlobalTranslation = part.GetAverageGlobalTranslation()
                    partInfo.Position.X = partAverageGlobalTranslation[0]          
                    partInfo.Position.Y = partAverageGlobalTranslation[1]
                    partInfo.Position.Z = partAverageGlobalTranslation[2]
                         
                    partInfo.ControllerRotationMovement = part.GetRotationMovementValues()
                    partInfo.ControllerPositionMovement = part.GetPositionMovementValues()
            
        return frameInfo
    
    def GetShotInfo( self ):
        
        #Reset any previous evaluation
        self.Reset()        
        
        returnShotInfo = RS.Core.Metadata.CreateMetaFile( "GradingShotInfo" )
        for trackedEntity in self._entityList:
            shotInfoEntity = returnShotInfo.Entities.Create()
            shotInfoEntity.ID = str( trackedEntity.GetID() )
            shotInfoEntity.Name = str( trackedEntity.GetName() )
            shotInfoEntity.Type = str( trackedEntity.GetType().Name )
            shotInfoEntity.ModelCount = trackedEntity.GetModelCount()
            shotInfoEntity.ControllerCount = trackedEntity.GetControllerCount()            
            for model in trackedEntity.GetModels():
                modelName = shotInfoEntity.Models.Create()
                modelName.Set( self._processModelName(model.Name) )
            for part in trackedEntity.GetParts():
                partInfo = shotInfoEntity.Parts.Create()
                partInfo.Name = part.GetName()
                partInfo.Color = part.GetColorHex()
                partInfo.ColorDebug = str( part.GetColor() )
                for controller in part.GetControllers():
                    controllerName = partInfo.Controllers.Create()
                    controllerName.Set( self._processModelName( controller.GetModel().Name ) )
        return returnShotInfo
    
    def _processModelName( self, modelName ):
        '''
        Takes a string (model name) and processes it to remove certain characters and replace them with a '_'
        '''
        returnModelName = ""
        for char in modelName:
            if char.isalpha() or char.isdigit() or char in ["_", " "]:
                returnModelName += char
            else:
                returnModelName += "_"
        return returnModelName
    
    def Evaluate( self ):
        for controller in self._entityControllerDict.values():
            controller.Evaluate()
    
    def Reset(self):
        for controller in self._entityControllerDict.values():
            controller.Reset()        
            
class TrackedEntity:
    def __init__( self, Manager, Name, Type, RootModel ):
        
        self._manager = Manager
        
        self._id = uuid.uuid1()
        self._name = Name
        self._type = Type
        
        self._controllerSet = TrackedEntityControllerSet()
        self._modelSet = set()      
        self._rootController = self._resolveController( RootModel )
        
        self._partDict = dict()
        for partType in self._type.Parts:
            self._partDict[ partType.Name ] = TrackedEntityPart( self, partType, self._manager.GetNextColor() )

    def GetID( self ):
        return self._id
    
    def GetName( self ):
        return self._name

    def GetType( self ):
        return self._type
    
    def GetRootTranslation( self ): 
        return self._rootController.GetGlobalTranslation()
    
    def GetRootRotation( self ): 
        return self._rootController.GetGlobalRotation()
    
    def GetDefaultPart( self ):
        for part in self._partDict.values():
            if part.GetPattern() == '*':
                return part        
    
    def _processBoneModel( self, BoneModel ):
        
        matchingPart = None
        for part in self._partDict.values():
            if part.GetPattern() == "*":
                matchingPart = part
            elif re.search( part.GetPattern(), BoneModel.Name, re.IGNORECASE ) != None:
                matchingPart = part
                break
            
        boneController = self._resolveController( BoneModel ) 
        matchingPart.AddController( boneController )
        return matchingPart
    
    def _resolveController( self, Model ):
        controller = self._manager._getController( Model )
        self._controllerSet.add( controller )
        return controller
    
    def AddModel( self, Model ):
        
        #Add the model to our model set (mainly for debug purposes)        
        self._modelSet.add( Model ) 
        
        if Model.SkeletonDeformable:
            
            #Create vertex weight table
            vertexWeightTable = RS.Utils.Scene.Skin.VertexWeightTable( Model )
    
            #Get the model's mesh and start editing
            mesh = Model.Geometry
            mesh.GeometryBegin()
            
            #Get material index array (so we can start changing the IDs to match parts)            
            materialIndexArray = mesh.GetMaterialIndexArray()
            
            for polygonIndex in range( 0, mesh.PolygonCount() ):
                
                #Gather up the verts that form this polygon
                vertexIndexList = []
                for polygonVertexIndex in range( 0, mesh.PolygonVertexCount( polygonIndex ) ):
                    vertexIndexList.append( mesh.PolygonVertexIndex( polygonIndex, polygonVertexIndex ) )
                    
                #Gather up weights used by polygon
                vertexWeightList = []
                for vertexIndex in vertexIndexList:
                    vertexWeightList.append( vertexWeightTable.GetVertexWeight(vertexIndex) )
                
                #Gather up culminitive weight bone pairings
                valueDict = dict()
                for vertexWeight in vertexWeightList:
                    for entry in vertexWeight.EntryList:
                        part = self._processBoneModel( entry.Model ) 
                        if not part in valueDict:
                            valueDict[part] = 0.0
                        valueDict[part] += entry.Value
                
                #Get strongest part across the polygon
                strongestPart = None
                strongestPartValue = 0.0
                for part in valueDict:
                    
                    #Check if this part is set to coerce (override any other parts)
                    if part.GetCoerce():
                        strongestPart = part
                        strongestPartValue = valueDict[part]
                        break
                    
                    #See if this part is stronger than others    
                    if strongestPart == None:
                        strongestPart = part
                        strongestPartValue = valueDict[part]
                    else:
                        if valueDict[part] > strongestPartValue:
                            strongestPart = part
                            strongestPartValue = valueDict[part]
                            
                #If we failed to find a part (because the vertex is not weighted to anything) then use the default part
                if strongestPart == None: 
                    strongestPart = self.GetDefaultPart()
                    
                #Get part material
                strongestPartMaterial = strongestPart.GetMaterial()
                if strongestPartMaterial not in Model.Materials:
                    Model.Materials.append( strongestPartMaterial )
                
                #Get the index of the material
                for index in range( len( Model.Materials ) ):
                    material = Model.Materials[index]
                    if material == strongestPartMaterial:
                        strongestPartMaterialIndex = index
                        break
                
                #Set the poly material id to the matching part material id
                materialIndexArray[polygonIndex] = strongestPartMaterialIndex
            
            #Set the material index array and stop editing the mesh    
            mesh.SetMaterialIndexArray(materialIndexArray)    
            mesh.GeometryEnd()
        else:
            part = self._processBoneModel( Model )
            Model.Materials.removeAll()
            Model.Materials.append( part.GetMaterial() )
            
    def GetAverageGlobalTranslation( self ): 
        return self._controllerSet.GetTransformAverage( GLOBAL_TRANSLATION ) 
    
    def GetPartCount( self ): 
        return len( self._partDict )
    
    def GetModelCount( self ): 
        return len( self._modelSet )
    
    def GetControllerCount( self ): 
        return len( self._controllerSet )
    
    def GetParts( self ): 
        return self._partDict.values()
    
    def GetModels( self ):
        return self._modelSet        
    
    def GetControllers( self ):
        return self._controllerSet    
    
#Represents a part of an object 
class TrackedEntityPart:
    def __init__( self, Parent, Type, Color ):

        self._parent = Parent
        self._partType = Type
        self._name = Type.Name
        self._fullName = "{0}_{1}".format( self._parent.GetName(), self._name)
        self._pattern = Type.Pattern
        self._color = Color
        self._controllerSet = TrackedEntityControllerSet()
        self._coerce = Type.Coerce        
        
        self._material = FBMaterial( self._fullName )
        self._material.Emissive = self._color
        self._material.EmissiveFactor = 1.0
        self._material.DiffuseFactor = 0.0
        self._material.SpecularFactor = 0.0
        self._material.AmbientFactor = 0.0
        
    def AddController( self, Controller ): 
        self._controllerSet.add( Controller )
    
    def HasControllers( self ):
        return len(self._controllerSet) > 0
    
    def GetControllers( self ):
        return self._controllerSet 
    
    def GetAverageGlobalTranslation( self ): 
        return self._controllerSet.GetTransformAverage( GLOBAL_TRANSLATION ) 
    
    def GetRotationMovementValues( self ):
        return self._controllerSet.GetRotationMovementValues() 

    def GetPositionMovementValues( self ):
        return self._controllerSet.GetPositionMovementValues() 

    def GetMaterial( self ): 
        return self._material
    
    def GetPattern( self ):
        return self._pattern
    
    def GetName(self):
        return self._name
    
    def GetCoerce(self):
        return self._coerce
    
    def GetColor(self):
        return self._color

    def GetColorHex(self):
        return RS.Utils.Color.GetHexCode( self._color )
    
#Essentially  a bone            
class TrackedEntityController:
    def __init__( self, Model ):
        
        self._model = Model

        self._previousGlobalTranslationVector = None
        self._currentGlobalTranslationVector = None
        
        self._previousLocalRotationMatrix = None
        self._currentLocalRotationMatrix = None        
    
    def GetGlobalTranslation( self ): 
        currentGlobalTranslationVector = FBVector3d()
        self._model.GetVector( currentGlobalTranslationVector, FBModelTransformationType.kModelTranslation )
        return currentGlobalTranslationVector
    
    def GetGlobalRotation( self ): 
        currentGlobalRotationVector = FBVector3d()
        self._model.GetVector( currentGlobalRotationVector, FBModelTransformationType.kModelRotation )
        return currentGlobalRotationVector
    
    def GetPositionMovement( self ):
        if self._previousGlobalTranslationVector != None:
            return math.sqrt( pow((self._previousGlobalTranslationVector[0]-self._currentGlobalTranslationVector[0]),2) + 
                              pow((self._previousGlobalTranslationVector[1]-self._currentGlobalTranslationVector[1]),2) + 
                              pow((self._previousGlobalTranslationVector[2]-self._currentGlobalTranslationVector[2]),2))        
        else:
            return 0.0
        
    def GetRotationMovement( self ):
        if self._previousLocalRotationMatrix != None:
                
                currentXAxis = FBVector3d(  self._currentLocalRotationMatrix[0], 
                                            self._currentLocalRotationMatrix[1], 
                                            self._currentLocalRotationMatrix[2])
                currentYAxis = FBVector3d(  self._currentLocalRotationMatrix[4], 
                                            self._currentLocalRotationMatrix[5], 
                                            self._currentLocalRotationMatrix[6])
                currentZAxis = FBVector3d(  self._currentLocalRotationMatrix[8], 
                                            self._currentLocalRotationMatrix[9], 
                                            self._currentLocalRotationMatrix[10])

                previousXAxis = FBVector3d( self._previousLocalRotationMatrix[0], 
                                            self._previousLocalRotationMatrix[1], 
                                            self._previousLocalRotationMatrix[2])
                previousYAxis = FBVector3d( self._previousLocalRotationMatrix[4], 
                                            self._previousLocalRotationMatrix[5], 
                                            self._previousLocalRotationMatrix[6])
                previousZAxis = FBVector3d( self._previousLocalRotationMatrix[8], 
                                            self._previousLocalRotationMatrix[9], 
                                            self._previousLocalRotationMatrix[10])
                
                xAxisChannge =  RS.Utils.Math.AngleBetweenVectors( currentXAxis, previousXAxis )
                yAxisChannge =  RS.Utils.Math.AngleBetweenVectors( currentYAxis, previousYAxis )
                zAxisChannge =  RS.Utils.Math.AngleBetweenVectors( currentZAxis, previousZAxis )
                
                return max([xAxisChannge, yAxisChannge, zAxisChannge])
        else:
            return 0.0
            
    def GetModel(self):
        return self._model
    
    def Reset(self):
        self._previousGlobalTranslationVector = None
        self._currentGlobalTranslationVector = None
        
        self._previousLocalRotationMatrix = None
        self._currentLocalRotationMatrix = None          

    def Evaluate(self):
        
        self._previousGlobalTranslationVector = self._currentGlobalTranslationVector
        self._currentGlobalTranslationVector = FBVector3d()
        self._model.GetVector( self._currentGlobalTranslationVector, FBModelTransformationType.kModelTranslation )
        
        self._previousLocalRotationMatrix = self._currentLocalRotationMatrix
        self._currentLocalRotationMatrix = FBMatrix()
        self._model.GetMatrix( self._currentLocalRotationMatrix, FBModelTransformationType.kModelRotation, False )
        
            
GLOBAL_TRANSLATION = "GetGlobalTranslation"
GLOBAL_ROTATION = "GetGlobalRotation"
LOCAL_TRANSLATION = "GetLocalTranslation"
LOCAL_ROTATION = "GetLocalRotation"

class TrackedEntityControllerSet (set):
    
    def GetTransformAverage( self, TargetTransformElement ):
        
        X = Y = Z = 0

        for controller in self:
            controllerFunc = getattr( controller, TargetTransformElement )
            controllerVector = controllerFunc()
            X += controllerVector[0]
            Y += controllerVector[1]
            Z += controllerVector[2]
         
        return FBVector3d( X / len(self),
                           Y / len(self),
                           Z / len(self))
 
    def GetPositionMovementValues( self ):
        '''
        #Returns a formatted string with all the position (global translation) based movement values.
        '''        
        movementString = ""

        for controller in self:
            controllerMovement = controller.GetPositionMovement()
            movementString += "{0:.2f}, ".format( controllerMovement )
        
        #return string with the space and comma removed from the end
        return movementString[:-2]

    def GetRotationMovementValues( self ):
        '''
        #Returns a formatted string with all the rotation based movement values.
        '''
        movementString = ""

        for controller in self:
            controllerMovement = controller.GetRotationMovement()
            movementString += "{0:.2f}, ".format( controllerMovement )
        
        #return string with the space and comma removed from the end
        return movementString[:-2]