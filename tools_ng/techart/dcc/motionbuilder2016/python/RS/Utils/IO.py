import os

import pyfbsdk as mobu

from RS import Globals
from RS.Utils import Namespace


def getNextNamespace(baseNameSpace, maxNumberOfNamespacesToCheck=50):
    """
    Get the next free namespace, adding ^n until a free namespace is found

    args:
        baseNameSpace (str): The base namespace to use

    kwargs:
        maxNumberOfNamespacesToCheck (int): The max number of namespaces to check

    returns:
        string of the next free namespace to use
    """
    if Namespace.GetFBNamespace(baseNameSpace) is None:
        return baseNameSpace

    for idx in xrange(1, maxNumberOfNamespacesToCheck):
        namespace = "{0}^{1}".format(baseNameSpace, idx)
        if Namespace.GetFBNamespace(namespace) is None:
            return namespace
    raise ValueError("Unable to find free namespace for '{}'".format(baseNameSpace))


def mergeInFile(filePath, namespace=None, autoResolveConflictingNamespace=True):
    """
    Merge in a file into the scene, applying the namespace as needed and returing the loaded object
    namespace

    args:
        filePath (str): The file path to load in

    kwargs:
        namespace (str): The namespace to use, otherwise the filename is used
        autoResolveConflictingNamespace (bool): If a new namespace is always used, ensuring no conflict

    returns:
        FBNamespace of the newly loaded object
    """
    namespace = namespace or os.path.splitext(os.path.basename(filePath))[0]
    if autoResolveConflictingNamespace is True:
        namespace = getNextNamespace(namespace)
    mergeOptions = mobu.FBFbxOptions(True, filePath)
    mergeOptions.UpdateRecentFiles = False  # This will stop the merged file getting added to the recent file list
    mergeOptions.NamespaceList = namespace
    Globals.Application.FileMerge(filePath, False, mergeOptions)
    return Namespace.GetFBNamespace(namespace)
