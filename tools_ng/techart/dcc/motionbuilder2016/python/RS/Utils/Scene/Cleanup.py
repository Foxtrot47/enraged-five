"""
Module for getting components in the scene that are from out-dated/deprecated setups or that are unused, thus cluttering
the scene.
"""
import pyfbsdk as mobu

from PySide import QtGui

import unbind

from RS import Globals, Config
from RS.Utils import Scene
from RS.Utils.Scene import Component


def GetMultiCameraViewerDataList():
    """
    Clears out the user properties 'MultiCameraViewerData'
    url:bugstar:3669663

    Return:
        list
    """
    return [userObject for userObject in Globals.UserObjects if 'MultiCameraViewerData' in userObject.Name]


def GetUnusedBindPoseList(namespace):
    """
    Finds Namespaces that only contain a BindPose. These bindposes and namespaces will not
    be in use anymore. This method will clear out the bindposes.
    Arg:
        FBNamespace
    Return:
        list(FBPose)
    """
    # check if the content is only bindposes - if so then this namespace is not in use, this is
    # old left behind data
    poseList = mobu.FBComponentList()
    Globals.Scene.NamespaceGetContentList(poseList, namespace.LongName, mobu.FBPlugModificationFlag.kFBPlugAllContent,
                                          True, mobu.FBPose.TypeInfo)
    for content in poseList:
        if "BindPose" not in content.Name:
            return []

    # Get a list of all the models in the namespace
    modelList = mobu.FBComponentList()
    Globals.Scene.NamespaceGetContentList(modelList, namespace.LongName, mobu.FBPlugModificationFlag.kFBPlugAllContent,
                                          True, mobu.FBModel.TypeInfo)
    # If a namespace has no models, then it is empty for the intent and purpose of this function
    if len(modelList):
        return []
    # this content list only contains bind poses, so we want to clear it out as they wont be used
    return poseList


def ClearOutUnusedNamespaces():
    """
    Remove namespaces no longer in use.

    Most namespaces left over in a scene will be because they still have a 'BindPose' attached.
    This will no longer be needed if that is all the namespace has as content. We delete these
    bind poses then clear out the namespaces which no longer have content.
    """
    contentList = []
    # remove unused bindposes
    for namespace in Globals.Namespaces:
        contentList.extend(GetUnusedBindPoseList(namespace))

    # this content list only contains bind poses, so we want to clear it out as they wont be used
    for content in contentList:
        print content.LongName
        content.FBDelete()

    # removed namespaces with no cotnent (includes namespaces with namespace children which also do
    # not have any content)
    Globals.Scene.NamespaceCleanup()


def GetUnusedComponentsList():
    """
    Deletes components that are not connected to anything
    """

    componentTypes = (
                      mobu.FBControlSet,
                      mobu.FBVideoClip,
                      mobu.FBConstraint,
                      mobu.FBTexture,
                      mobu.FBMaterial,
                      mobu.FBShader,
                      mobu.FBSet,
                      mobu.FBGroup,
                      mobu.FBFolder,
                      mobu.FBConstraintSolver,
                      mobu.FBDevice,
                      mobu.FBPose,
                      mobu.FBModelNull)

    componentDeleteList = []

    try:

        # Delete empty Sets, Groups and Folders
        for component in Globals.Components:
            # skip components that have been deleted or not the type of objects we are looking to delete

            if Component.IsDestroyed(component) or not isinstance(component, componentTypes):
                continue

            if isinstance(component, (mobu.FBPose)) and component in Globals.CharacterPoses:
                continue

            # skip nulls that aren't empty nested reference nulls from bad merges from unclean fbx files
            # there should only be one reference null in the scene at any time.
            if isinstance(component, mobu.FBModelNull):
                if component.LongName != "Scene":
                    continue
                if component.Name.count(":") <= 1:
                    continue
                if component.Children:
                    continue

            # Delete any old device expression solvers (all should be replaced with constraints now)
            if isinstance(component, mobu.FBDevice) and component.ClassName() not in ("UFCDevice", "RSExprSolverDevice"):
                continue

            # Find and skip control sets that are connected to a character
            elif isinstance(component, mobu.FBControlSet) and component.GetDstCount():
                continue

            # Find and skip components that are not attached to any object
            elif isinstance(component, (mobu.FBVideoClip, mobu.FBConstraint, mobu.FBTexture, mobu.FBMaterial, mobu.FBShader)):
                if component.GetDstCount() or component.Name in ['Default Shader', 'DefaultMaterial']:
                    continue

            # Find and skip any set, group and folder that has objects
            elif isinstance(component, (mobu.FBSet, mobu.FBGroup, mobu.FBFolder)):
                if component.Items or component.Name == 'CHARACTERS':
                    continue

            # Delete any HIK solvers
            elif isinstance(component, mobu.FBConstraintSolver) and component.UniqueName and 'HIK' not in component.UniqueName:
                    continue

            # Delete components that we couldn't skip
            componentDeleteList.append(component)

        return componentDeleteList

    except unbind.UnboundWrapperError:
        QtGui.QMessageBox.warning(None,
                                  "Cleanup Error",
                                  "Component object(s) have become unbound, which means it has\nalready been removed from the scene.\n\nUnable to proceed with cleanup.")
        return componentDeleteList


def GetOldFacialRigs():
        """ Get old facial rigs components from the scene """
        oldRoots = mobu.FBComponentList()
        mobu.FBFindObjectsByName("FACIAL_FacialRoot", oldRoots, False, True)
        mobu.FBFindObjectsByName("FACIAL_facialRoot", oldRoots, False, True)

        delete = []
        for oldRoot in oldRoots:
            for sibling in oldRoot.Parent.Children:
                if sibling.Name.lower() == "facial_c_facialroot":
                    for child in Scene.GetAllChildren(oldRoot):
                        if child.ClassName() in ("FBModelSkeleton", "FBModelNull", "FBModel"):
                            delete.append(child)
        return delete
