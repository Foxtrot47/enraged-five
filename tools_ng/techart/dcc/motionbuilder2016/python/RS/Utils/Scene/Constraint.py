"""
Usage:
    This module should only contain functionality related to working with FBConstraints (or their sub types).
Author:
    Ross George
"""
import pyfbsdk as mobu

import RS.Utils.Collections
from RS.Utils.Scene import Component


class ConstraintType:
    """
    Represents a constraint type that can be passed around for testing an FBConstraint's type
    (not straight forward) and also creating FBconstraints of designated type
    """
    def __init__(self, Name, Description):
        self.Name = Name
        self.Description = Description


AIM = ConstraintType("Aim", "Aim")
EXPRESSION = ConstraintType("Expression", "Expressions")
MULTI_REFERENTIAL = ConstraintType("Multi Referential", "Multi-Referential")
PARENT_CHILD = ConstraintType("Parent/Child", "Parent-Child")
PATH = ConstraintType("Path", "Path Constraint")
POSITION = ConstraintType("Position", "Position From Positions")
RANGE = ConstraintType("Range", "Range")
RELATION = ConstraintType("Relation", "Relations")
RIGID_BODY = ConstraintType("Rigid Body", "Rigid Body")
THREE_POINT = ConstraintType("3 Points", "Rotation From 3 Positions")
ROTATION = ConstraintType("Rotation", "Rotation From Rotations")
SCALE = ConstraintType("Scale", "Scale From Scales")
MAPPING = ConstraintType("Mapping", "Aim")
CHAIN_IK = ConstraintType("Chain IK", "Single Chain IK")
REACH_CONE = ConstraintType("ReachConeConstraint", "Reach Cone Constraint")
SPLINE_IK = ConstraintType("SplineIk", "Spline Ik")
CONSTRAINT_TYPES = (POSITION, ROTATION, SCALE, PARENT_CHILD, AIM, PATH, RANGE, RELATION, RIGID_BODY,
                    THREE_POINT, MAPPING, CHAIN_IK, MULTI_REFERENTIAL, EXPRESSION, REACH_CONE)


def IsConstraintType(TargetConstraint, ConstraintTypeList):
    """
    Test to see if the passed FBConstaint is of the passed ConstraintType
    """
    if not isinstance(TargetConstraint, mobu.FBConstraint):
        raise Exception("Not a FBConstraint or derivitive.")

    # If we have passed a tuple type then convert it to a list
    if isinstance(ConstraintTypeList, tuple):
        ConstraintTypeList = list(ConstraintTypeList)

    # If we have been passed a single type then convert it to list
    if not isinstance(ConstraintTypeList, list):
        ConstraintTypeList = [ConstraintTypeList]

    for constraintType in ConstraintTypeList:
        if TargetConstraint.Description == constraintType.Description:
            return True

    return False


def CreateConstraint(ConstraintType):
    """
    Creates and returns FBConstaint from the passed ConstraintType
    """
    return mobu.FBConstraintManager().TypeCreateConstraint(ConstraintType.Name)


def GetConstraintType(Constraint):
    """
    Get the type of constraint that is passed in as a ConstraintType
    """
    if not isinstance(Constraint, mobu.FBConstraint):
        raise Exception("Not a FBConstraint or derivitive.")

    for con in [AIM, EXPRESSION, MULTI_REFERENTIAL, PARENT_CHILD, PATH, POSITION, RANGE, RELATION,
                RIGID_BODY, THREE_POINT, ROTATION, SCALE, MAPPING, CHAIN_IK]:
        if Constraint.Description == con.Description:
            return con
    return None


def Constraint(ConstraintType, Name="", *References, **Properties):
    """
    Convenience method for creating a constraint, adding components the constraint manages and setting properties

    Arguments:
        ConstraintType (RS.Utils.Scene.Constraint.ConstraintType): Type of constraint to create
        Name (string): name of the constraint
        *References (tuple/list): components to attach to the constraint, order matters as it determines where component
                                  will be attached to in the constraint. Ei. For a parent/child constraint the child
                                  component is always first and parents always follow
        **Properties (dictionary): keys are the name of the properties on the constraint and the values are the values
                                   that should be set for those properties.

    Example:
        # Create a parent constraint that is Active and Locked
        CreateConstraint(PARENT_CHILD, "MyConstraint", child, parent01, parent02, Active=True, Lock=True)

    Return:
        pyfbsdk.FBConstraint
    """

    constraint = CreateConstraint(ConstraintType)
    if Name:
        constraint.LongName = Name

    [constraint.ReferenceAdd(index, component) for index, component in enumerate(References) if component is not None]
    Component.SetProperties(constraint, **Properties)

    return constraint


def SwapTargetModel(TargetConstraint, TargetModel, NewModel):
    """
    Swaps the TargetModel on a PARENT_CHILD, ROTATION, POSITION, SCALE constraint for the passed NewModel
    """
    # Get target model
    if IsConstraintType(TargetConstraint, [AIM]):
        # there is a separate check for aim constraints
        return

    targetModel = None
    if IsConstraintType(TargetConstraint, PARENT_CHILD):
        targetModel = TargetConstraint.PropertyList.Find("Constrained Object (Child)")

    if IsConstraintType(TargetConstraint, [ROTATION, POSITION, SCALE]):
        targetModel = TargetConstraint.PropertyList.Find("Constrained Object")

    # Check we have a target model (if not we are dealing with incompatible constraints
    if targetModel==None:
        raise Exception("Not a constraint that has a target  that can be switched (Parent/Child, Rotation, Position, Scale).")

    # Check that the original model the target
    if not targetModel[0] == TargetModel:
        raise Exception("TargetModel is not the correct child")
    if targetModel[0] == NewModel:
        raise Exception("NewModel already the child")
    else:
        targetModel.remove( TargetModel )
        targetModel.append( NewModel )


def SwapSourceModel(TargetConstraint, SourceModel, NewModel):
    """
    Swaps the SourceModel on a PARENT_CHILD, ROTATION, POSITION, SCALE constraint for the passed NewModel
    """
    # Get source model list
    if IsConstraintType(TargetConstraint, [AIM, RELATION]):
        # there is a separate check for aim constraints
        return

    if IsConstraintType(TargetConstraint, PARENT_CHILD):
        sourceModelList = TargetConstraint.PropertyList.Find("Source (Parent)")

    if IsConstraintType(TargetConstraint, [ROTATION, POSITION, SCALE]):
        sourceModelList = TargetConstraint.PropertyList.Find("Source")

    # Check we have a source model list (if not we are dealing with incompatible constraint)
    if sourceModelList == None:
        raise Exception("Not a constraint that has a source that can be switched (Parent/Child, Rotation, Position, Scale).")

    # Check that the original model is definately on the list
    sourceModelInSourceList = False
    newModelInSourceList = False
    for model in sourceModelList:
        if model == SourceModel:
            sourceModelInSourceList = True
        if model == NewModel:
            newModelInSourceList = True

    if not sourceModelInSourceList:
        raise Exception("SourceModel is not on the source model list")

    if newModelInSourceList:
        raise Exception("NewModel already in source model list")

    # Add the NewModel to the list
    sourceModelList.append(NewModel)

    # Copy property information
    for propExtension in ["Offset T", "Offset R", "Offset S", "Weight"]:
        sourceProp = TargetConstraint.PropertyList.Find("{0}.{1}".format(SourceModel.LongName, propExtension))
        newProp = TargetConstraint.PropertyList.Find("{0}.{1}".format(NewModel.LongName, propExtension))
        if sourceProp != None and newProp != None:
            RS.Utils.Scene.Property.CopyData(sourceProp, newProp)
            RS.Utils.Scene.Property.CopyAnimationData(sourceProp, newProp)

    # Remove the original model
    sourceModelList.remove(SourceModel)

def SwapAimConstraintModels(targetConstraint, sourceModel, newModel):
    '''
    swap old models with the new ones
    '''
    # double check the constraint is definitely an aim constraint
    if GetConstraintType(targetConstraint) != AIM:
        return

    # constrainted Object
    for idx in range(targetConstraint.ReferenceGetCount(0)):
        if sourceModel.LongName == targetConstraint.ReferenceGet(0,idx).LongName:
            targetConstraint.ReferenceRemove(0, targetConstraint.ReferenceGet(0,idx))
            targetConstraint.ReferenceAdd(0, newModel)

    # aim objects
    for idx in range(targetConstraint.ReferenceGetCount(1)):
        if sourceModel.LongName == targetConstraint.ReferenceGet(1,idx).LongName:
            targetConstraint.ReferenceRemove(1, targetConstraint.ReferenceGet(1,idx))
            targetConstraint.ReferenceAdd(1, newModel)

    # world up object
    for idx in range(targetConstraint.ReferenceGetCount(2)):
        if sourceModel.LongName == targetConstraint.ReferenceGet(2,idx).LongName:
            targetConstraint.ReferenceRemove(2, targetConstraint.ReferenceGet(2,idx))
            targetConstraint.ReferenceAdd(2, newModel)


def SwapRelationConstraintModels(targetConstraint, sourceModel, newModel):
    """Swap relation constraint senders and receivers

    Args:
        targetConstraint (mobu.FBRelationConstraint): target constraint
        sourceModel (mobu.FBModel): source model
        newModel (mobu.FBModel): target model
    """
    # double check the constraint is definitely a relation constraint
    if GetConstraintType(targetConstraint) != RELATION:
        return

    # get senders and receivers
    senderList = []
    receiverList = []
    for box in targetConstraint.Boxes:

        # sender or receivers are FBModelPlaceHolders
        if not isinstance(box, mobu.FBModelPlaceHolder):
            continue

        # check if all source counts have a value, if so it's a sender, else a receiver
        animInNode = box.AnimationNodeInGet()
        if all(node.GetSrcCount() for node in animInNode.Nodes):
            senderList.append(box)
        else:
            receiverList.append(box)

    for sender in senderList:

        if sender.Model is not sourceModel:
            continue

        # add new box from new model
        newSender = targetConstraint.SetAsSource(newModel)
        newSenderAnimNode = newSender.AnimationNodeOutGet()

        # match position
        result, xPos, yPos = targetConstraint.GetBoxPosition(sender)
        targetConstraint.SetBoxPosition(newSender, xPos, yPos)

        # get sender out plugs
        senderAnimNode = sender.AnimationNodeOutGet()
        for index, srcPlug in enumerate(senderAnimNode.Nodes):
            # get destination plugs
            for dstPlugIndex in xrange(srcPlug.GetDstCount()):
                dstPlug = srcPlug.GetDst(dstPlugIndex)

                # connect new model to plug
                mobu.FBConnect(newSenderAnimNode.Nodes[index], dstPlug)
                mobu.FBDisconnect(srcPlug, dstPlug)

    for receiver in receiverList:

        if receiver.Model is not sourceModel:
            continue

        # add new box from new model
        newReceiverNode = targetConstraint.ConstrainObject(newModel)

        # match position
        result, xPos, yPos = targetConstraint.GetBoxPosition(receiver)
        targetConstraint.SetBoxPosition(newReceiverNode, xPos, yPos)

        newReceiverAnimNode = newReceiverNode.AnimationNodeInGet()

        # get receiver in plugs
        receiverAnimNode = receiver.AnimationNodeInGet()
        for index, dstPlug in enumerate(receiverAnimNode.Nodes):
            # get source plugs
            for srcPlugIndex in xrange(dstPlug.GetSrcCount()):
                srcPlug = dstPlug.GetSrc(srcPlugIndex)

                # connect new model to plug
                mobu.FBConnect(srcPlug, newReceiverAnimNode.Nodes[index])
                mobu.FBDisconnect(srcPlug, dstPlug)


def GetRelationBoxesWithInAnim(relation):
    """
    returns a list of senders, receivers and function boxes in a realtion constraint,
    which have In Animation node connections
    """
    if relation:
        senderList = []
        receiverList = []
        functionList = []
        for lBox in relation.Boxes:
            lOut = 0
            lIn = 0
            lAnimationNodesOUT = lBox.AnimationNodeOutGet().Nodes
            for lAnimationNode in lAnimationNodesOUT:
                for i in range(lAnimationNode.GetDstCount()):
                    if lAnimationNode.GetDst(i) != None:
                        # Workaround for why it's printing in nodes that aren't connected
                        if not lAnimationNode.GetDst(i).GetOwner().Name.startswith("Relation"):
                            lOut = 1
            # IN  Animation Nodes
            lAnimationNodesIN = lBox.AnimationNodeInGet().Nodes
            for lAnimationNode in lAnimationNodesIN:
                for i in range(lAnimationNode.GetSrcCount()):
                    if lAnimationNode.GetSrc(i) != None:
                        lIn = 1
            if lOut == 0 and lIn == 1:
                receiverList.append(lBox)
            elif lOut == 1 and lIn == 0:
                senderList.append(lBox)
            else:
                functionList.append(lBox)

        return (senderList, receiverList, functionList)
