'''
Usage:
    This module should only contain functionality related to working with FBPlug (or their sub types - which is pretty much everything).    
Author:
    Ross George
'''

from pyfbsdk import *

'''
Get all the destination plugs from the TargetPlug. Can optionally define the type of plugs to return i.e. FBModel.
'''
def GetDstPlugList( TargetPlug, TypeList = None ):
    
    #If we have been passed a single type then turn it into a list
    if TypeList != None and type(TypeList) != list:
        TypeList = [TypeList]
    
    dstPlugList = []
    for dstPlugIndex in range( TargetPlug.GetDstCount() ):
        dstPlug = TargetPlug.GetDst(dstPlugIndex)
        if TypeList != None:
            if type(dstPlug) in TypeList:
                dstPlugList.append( dstPlug ) 
        else:
            dstPlugList.append( dstPlug ) 
    return dstPlugList

'''
Get all the source plugs from the TargetPlug. Can optionally define the type of plugs to return i.e. FBModel.
'''    
def GetSrcPlugList( TargetPlug, TypeList = None ):
    
    #If we have been passed a single type then turn it into a list
    if TypeList != None and type(TypeList) != list:
        TypeList = [TypeList]
        
    srcPlugList = []
    for srcPlugIndex in range( TargetPlug.GetSrcCount() ):
        srcPlug = TargetPlug.GetSrc(srcPlugIndex)
        if TypeList != None:
            if type(srcPlug) in TypeList:
                srcPlugList.append( srcPlug ) 
        else:
            srcPlugList.append( srcPlug ) 
    return srcPlugList