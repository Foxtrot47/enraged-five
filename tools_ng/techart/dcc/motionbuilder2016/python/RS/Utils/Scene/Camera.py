# Generic Camera Functions
import pyfbsdk as mobu

from RS import Globals
from RS.Utils.Scene import Property


def GetCameraList():
    '''
    Gets list of Cameras in the Scene.
    Excludes System Cameras.
    '''
    camera_list = []
    for camera in Globals.Cameras:
        if not camera.SystemCamera:
            camera_list.append(camera)
    return camera_list


def GetCurrentCamera():
    '''
    Get current camera in switcher for current frame.
    '''
    camera_switcher = mobu.FBCameraSwitcher()
    current_camera = camera_switcher.CurrentCamera

    return current_camera


class SwitcherMonitor():
    '''
    Callback: Select current camera in scene based on current frame
    '''
    def __init__(self):
        self.previous_frame = 0
        self.camera_list = []
        self.previous_camera = None
        self.current_camera = None

        self.camera_list = GetCameraList()

    def TimelineSync(self, control, event):
        current_frame = mobu.FBSystem().LocalTime.GetFrame()
        if current_frame != self.previous_frame:
            self.previous_frame = current_frame
        self.SelectCurrentCamera()

    def Register(self):
        self.Unregister()
        Globals.EvaluateManager.OnSynchronizationEvent.Add(self.TimelineSync)

    def Unregister(self):
        # url:bugstar:2511202 Check in case the OnSynchronizationEvent is None
        if Globals.EvaluateManager.OnSynchronizationEvent is None:
            return
        Globals.EvaluateManager.OnSynchronizationEvent.Remove(self.TimelineSync)

    def SelectCurrentCamera(self):
        self.previous_camera = self.current_camera
        self.current_camera = GetCurrentCamera()

        if self.previous_camera != self.current_camera:

            for camera in self.camera_list:
                camera.Selected = False

            for camera in self.camera_list:
                if camera.Name == self.current_camera.Name:
                    self.current_camera.Selected = True
