"""
Library of common character functions such as Duplicating, getting if a character is hidden or not
"""
import copy

import os

from PySide import QtGui

from numbers import Number

import pyfbsdk as mobu

from RS import Globals

from RS.Utils.Scene import AnimationNode
from RS.Utils import Scene, Vector
from RS.Core.Expression import expressionSolver
from RS.Core.Character import expressionPaths


def GetControlRigElements(inputCharacter):
    """
        Extract from the provided inputCharacter ik/Fk/Character Extensions control rig FBModel.
        (for Character Extensions, we will assume people will use _Control  prefix or metadata)

        Args:
            inputCharacter (FBCharacter):  source Character

        returns  (dict of FBModelList) .
    """
    #Source code from MotionBuilder 2014\bin\config\PythonStartup\CharacterSelectAndKey.py
    fkControls = mobu.FBModelList()
    for controlId in mobu.FBBodyNodeId.values.itervalues():
        if controlId == mobu.FBBodyNodeId.kFBReferenceNodeId:
            continue
        rigPart = inputCharacter.GetCtrlRigModel(controlId)
        if rigPart:
            fkControls.append(rigPart)

    ikControls = mobu.FBModelList()
    for controlId in mobu.FBEffectorId.values.itervalues():
        rigPart = inputCharacter.GetEffectorModel(controlId)
        if rigPart:
            ikControls.append(rigPart)

    #Now parse character extensions
    characterComponents = []
    characterElementCount = inputCharacter.GetSrcCount ()

    for index in range(characterElementCount):
        characterComponents.append(inputCharacter.GetSrc(index))

    customControls = mobu.FBModelList()
    for characterComponent in characterComponents:
        if not isinstance(characterComponent,mobu.FBCharacterExtension):
            continue
        controlObjectListCount = characterComponent.GetSubObjectCount()

        for index in range(controlObjectListCount):
            controlObject = characterComponent.GetSubObject(index)
            if not isinstance(controlObject,mobu.FBModel):
                continue
            customControls.append(controlObject)

    return {'fkControls':fkControls,
            'ikControls':ikControls,
            'customControls':customControls}


def DuplicateCharacter(character, newName=None, newNamespace=None, createGroup=False):
    """
    Duplicate a given character, including all skinning, animation and materials. The new
    duplicated character can have a new name, new namespace and all have the duplicated
    elements added to a new group

    Args:
        character (FBCharacter): The character to duplicate

    Kwargs:
        newName (str): The new name for the character
        newNamespace (str): The new namespace to give each part of the duplicated character
        createGroup (bool): If the duplicated character should be added to a new group

    Return:
        FBCharacter: The duplicated character
    """
    if not isinstance(character, mobu.FBCharacter):
        raise ValueError("'{0}' is not of type FBCharacter".format(character))
    newName = newName or str(character.LongName)
    # Clone the character
    cloneChar = copy.copy(character)
    if newNamespace is None:
        newLongName = newName
    else:
        newLongName = "{0}:{1}".format(newNamespace, newName)

    cloneChar.LongName = newLongName

    newGroup = None
    if createGroup:
        newGroup = mobu.FBGroup(newLongName)
        newGroup.ConnectSrc(cloneChar)

    # Get the top level offset
    hips = character.PropertyList.Find("HipsLink")
    if len(hips) == 0:
        raise ValueError("'{0}' is not a valid character. missing the Hips bone".format(character.Name))
    hip = hips[0]
    topLevel = GetTopLevelItem(hip)
    allBones = GetAllChildren(topLevel)

    # duplicate all child nodes and make dict (orgBone:newBone)
    dupDict = {}
    for orgBone in allBones:
        boneName = str(orgBone.LongName).replace(character.LongName, newLongName)
        newBone = copy.copy(orgBone)
        newBone.LongName = boneName
        dupDict[orgBone] = newBone

        if newGroup is not None:
            newGroup.ConnectSrc(newBone)

    # Re-parent newBones to newBone equlivants of orgBones
    for orgBone in allBones:
        parBone = orgBone.Parent
        if parBone is None:
            continue

        newBone = dupDict[orgBone]
        newParBone = dupDict[parBone]
        newBone.Parent = newParBone

    # Replace org bones with new bones on cloned characters
    for item in cloneChar.PropertyList:
        if not item.Name.endswith("Link"):
            continue
        if len(item) == 0:
            continue
        newBone = dupDict[item[0]]
        item.removeAll()
        item.append(newBone)

    for orgBone in allBones:
        newBone = dupDict[orgBone]

        AnimationNode.CopyData(orgBone.AnimationNode, newBone.AnimationNode)

        newBone.Translation = Vector.CloneVector(orgBone.Translation)
        newBone.Rotation = Vector.CloneVector(orgBone.Rotation)

    materialDict = {}
    # Copy over Skin Weights on Models, only do this on FBModel types, no other class is valid
    for model in [item for item in allBones if type(item) == mobu.FBModel]:

        # Duplicate the materials
        materialsToApply = []
        for material in dupDict[model].Materials:
            if not materialDict.has_key(material):

                matName = str(material.LongName)
                if newNamespace is None:
                    matLongName = matName
                else:
                    matLongName = "{0}:{1}".format(newNamespace, matName)
                newMat = copy.copy(material)
                newMat.LongName = matLongName
                materialDict[material] = newMat

            materialsToApply.append(materialDict[material])

        dupDict[model].Materials.removeAll()
        for mat in materialsToApply:
            dupDict[model].Materials.append(mat)

        # Skin Weights
        sourceCluster = mobu.FBCluster(model)
        clonedCluster = mobu.FBCluster(dupDict[model])
        for linkIdx in xrange(sourceCluster.LinkGetCount()):
            sourceCluster.ClusterBegin(linkIdx)
            clonedCluster.ClusterBegin()

            clonedCluster.LinkSetName(sourceCluster.LinkGetName(linkIdx), linkIdx)
            clonedCluster.LinkSetModel(dupDict[sourceCluster.LinkGetModel(linkIdx)])

            sourcePosition = mobu.FBVector3d()
            sourceRotation = mobu.FBVector3d()
            sourceScale = mobu.FBVector3d()

            sourceCluster.VertexGetTransform( sourcePosition, sourceRotation, sourceScale )
            clonedCluster.VertexSetTransform( sourcePosition, sourceRotation, sourceScale )

            for linkVertexIdx in xrange(sourceCluster.VertexGetCount()):
                vertexIdx = sourceCluster.VertexGetNumber(linkVertexIdx)
                weightValue = sourceCluster.VertexGetWeight(linkVertexIdx)
                clonedCluster.VertexAdd(vertexIdx, weightValue)

            clonedCluster.ClusterEnd()
            sourceCluster.ClusterEnd()

    cloneChar.LongName = newLongName
    return cloneChar


def GetComponentsFromCharacter(character):
    """
    Get all the FBModel parts which make up an FBCharacter.

    Args:
        character (FBCharacter): The character to get all the parts of

    Return:
        list of FBModels: All the parts which make up the character
    """
    charList = character.PropertyList
    return [item[0] for item in charList if item.Name.endswith("Link") and len(item) > 0]


def CharacterHidden(character):
    """
    Method to calculate if a character is hidden in motion builder or not. An FBCharacter does
    not have a visible state, that is stored on the individual parts which make up the character.

    Args:
        character (FBCharacter): The character to find if it is hidden or not

    Return:
        bool: If the character is hidden or not
    """
    # Get all the show values for all the parts that make up the character
    shown = all([item.Show for item in GetComponentsFromCharacter(character)])
    # Flip the shown value to get if the skeleton is hidden
    return not shown


def GetAllChildren(element, currentList=None):
    """
    Recursive method to get all the child items from an object

    Args:
        element (FBModel): Object to get the children from

    Kwargs:
        currentList (list of FBModels): The internal list of all found models.

    Return:
        list of FBModels: All the models found as children of the top element
    """
    if currentList is None:
        currentList = []
    currentList.append(element)
    for child in element.Children:
        currentList = GetAllChildren(child, currentList)
    return currentList


def GetTopLevelItem(element):
    """
    Recursive method to get the top node, which does not have a parent

    Args:
        element (FBModel): Object to get the top level parent of

    Return:
        FBModel: The top, parent-less parent of the original element
    """
    parent = element.Parent
    if parent is None:
        return element
    return GetTopLevelItem(parent)


def ReadMaxProperties(mobuObject):
    """
    Read the max propreties from the given object and return it as a dict

    args:
        mobuObject (FBModel): The object to get the properties off

    Returns:
        Dict[propertyString] = propertyDataString
    """
    if mobuObject is None:
        return

    dataDict = {}
    maxProperty = mobuObject.PropertyList.Find('UDP3DSMAX')
    if maxProperty is not None:
        dataList = maxProperty.Data.split('\n')
        for data in dataList:
            data = data.replace('\r', '',)
            data = data.replace(' ', '',)
            dataList = data.split('=')
            if len(dataList) > 1:
                dataDict[dataList[0]] = dataList[1]
    return dataDict


def GetMaxProperties(character):
    characterNodeList = []
    characterHips = character.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
    characterDummy = Scene.GetParent(characterHips)
    Scene.GetChildren(characterDummy, characterNodeList, "", True)
    return {node:ReadMaxProperties(node) for node in characterNodeList}


def GetMaxDummyProperties(character):
    '''
    go through hierarchy and find the max property 'GEODUMMYNODE', this will give us the geometry
    dummy node and we can scan this for the remainder of the max properties

    Returns:
        Dict[propertyString] = propertyDataString
    '''
    # check if character is characerized, if it is not we exit the script as the character will not
    # have a linked hip bone
    if character.GetCharacterize() is False:
        return {}

    characterHips = character.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
    return ReadMaxProperties(Scene.GetParent(characterHips))


def IsPedFemale(character):
    isPedFemale = False
    maxDict = GetMaxDummyProperties(character)
    if maxDict is not None:
        for key, value in maxDict.iteritems():
            if key == 'PEDSEX':
                if str(value).lower() == 'female':
                    isPedFemale = True
                    break
    return isPedFemale


def GetMaxUDPScale(character):
    '''
    Max UDP 'IGTOCSSCALEVALUE' found on the Character's Geo Dummy node

    IG to CS Scale value for the RS Expression Solver

    Returns:
        Float
    '''
    scale = None
    maxPropertyDict = GetMaxDummyProperties(character)
    for key, value in maxPropertyDict.iteritems():
        if key == 'IGTOCSSCALEVALUE':
            scale = float(value)
            break

    if scale is not None and not isinstance(scale, Number):
        QtGui.QMessageBox.warning(None,
                                "Warning: Scale",
                                "Scale passed is not valid, it must be a Number.\n\nEmail/bug Tech Art.\n\nAborting Expression Solver Setup.",
                                QtGui.QMessageBox.Ok)
        return

    return scale


def GetMaxUdpIsExprSet(character):
    '''
    Max UDP 'ISEXPRSET' found on the Character's Geo Dummy node

    Tells us whether the Character uses an Expression Set or not, for the RS Expression Solver

    Returns:
        Boolean
    '''
    exprSetBoolean = None
    # get list of max UDPs
    maxPropertyDict = GetMaxDummyProperties(character)
    # iterate through to find the property we need
    for key, value in maxPropertyDict.iteritems():
        if key == 'ISEXPRSET':
            if str(value).lower() == 'false':
                exprSetBoolean = False
            else:
                exprSetBoolean = True
                break
    return exprSetBoolean


def GetMaxUdpExprFileList(character):
    '''
    Max UDP 'EXPRESSIONFILELIST' found on the Character's Geo Dummy node

    Grabs the list of expression paths that max has set in the property

    Returns:
        List of Strings (file paths)
    '''
    exprFileList = []
    exprFile = None
    # get list of max UDPs
    maxPropertyDict = GetMaxDummyProperties(character)
    # iterate through to find the property we need
    for key, value in maxPropertyDict.iteritems():
        if key == 'EXPRESSIONFILELIST':
            if "#(" in str(value):
                maxStringList = str(value)
                # value is displayed as '#()' we need to remove these symbols to get the list of paths
                # inside the brackets. first get everything fromt he right side of the open bracket
                maxStringListSplit1 = maxStringList.split('(')
                # get result of the first split, everything on the left side of the close bracket
                maxStringListSplit2 = maxStringListSplit1[1].split(')')
                # get result of the second split and create a python list
                maxStringListSplit3 = maxStringListSplit2[0].split(',')
                for path in maxStringListSplit3:
                    if os.path.exists(str(path[1:-1])):
                        exprFileList.append(str(path[1:-1]))
    return exprFileList


def GetMaxUdpSkelSet(character):
    '''
    Max UDP 'SKELSET' found on the Character's Geo Dummy node

    Grabs the list of skelset paths that max has set in the property

    Returns:
        List of Strings (file paths)
    '''
    skelSetList = []
    # get list of max UDPs
    maxPropertyDict = GetMaxDummyProperties(character)
    # iterate through to find the property we need
    for key, value in maxPropertyDict.iteritems():
        if key == 'SKELSET':
            if "#(" in str(value):
                maxStringList = str(value)
                # value is displayed as '#()' we need to remove these symbols to get the list of paths
                # inside the brackets. first get everything fromt he right side of the open bracket
                maxStringListSplit1 = maxStringList.split('(')
                # get result of the first split, everything on the left side of the close bracket
                maxStringListSplit2 = maxStringListSplit1[1].split(')')
                # get result of the second split and create a python list
                maxStringListSplit3 = maxStringListSplit2[0].split(',')
                for path in maxStringListSplit3:
                    if os.path.exists(str(path[1:-1])):
                        skelSetList.append(str(path[1:-1]))
                break
    return skelSetList


def SetupExpressionSolver(character):
    '''
    sets up expression device and its properties
    '''
    # get max properties
    scale = GetMaxUDPScale(character)
    skelFilePathList = GetMaxUdpSkelSet(character)
    exprSetBoolean = GetMaxUdpIsExprSet(character)
    exprFileList = GetMaxUdpExprFileList(character)

    # check character is an FBCharacter
    if not isinstance(character, mobu.FBCharacter):
        QtGui.QMessageBox.warning(None,
                                "Warning: Character",
                                "Character passed is not valid, it must be an FBCharacter.\n\nEmail/bug Tech Art.\n\nAborting Expression Solver Setup.",
                                QtGui.QMessageBox.Ok)
        return

    # declare solver variable
    solver = expressionSolver.ExpressionSolver()

    # create a dictionary with the skelPaths and bonePaths, create 1 device per unique skelset
    if len(skelFilePathList) != 0:
        for skelSet in skelFilePathList:
            # get basename of skelset path - to name the device with
            skelSetBaseNameExt = os.path.basename(skelSet)
            skelSetBaseName = skelSetBaseNameExt.split('.')
            skelSetBaseName = skelSetBaseName[0]

            # add solver device to scene
            deviceOn = True
            device = mobu.FBCreateObject("Browsing/Templates/Devices", "RS Expression Solver", "RS Expression Solver")
            device.Name = '{0}_RSExprSolver'.format(skelSetBaseName)
            mobu.FBSystem().Scene.Devices.append(device)

            # set scale to device properties
            if scale is not None:
                solver.SetScale(scale, device.LongName)
            else:
                QtGui.QMessageBox.warning(None,
                                          "RS Expression Solver",
                                          "Scale has come up as 'None'. It will be set to a default\nvalue in the Device.\n\nThis issue could be due to a meta file or script error.\nPlease add a bug for Default Tech Art.",
                                          QtGui.QMessageBox.Ok)

            # skel file path list in device properties
            solver.SetSkelPath(os.path.normpath(skelSet), device.LongName)

            # In Max, the list of expressions are found for us. We just need to pass them to the
            # device.
            if len(exprFileList) != 0:
                for expr in exprFileList:
                    solver.AddExprPath(os.path.normpath(expr), device.LongName)

            # switch the device On
            if deviceOn is True:
                device.Online = True
    else:
        QtGui.QMessageBox.warning(None,
                                  "RS Expression Solver",
                                  "skel list is empty.\n\n Unable to set up the RS Expressions Device, please\ncontact Default Tech Art.",
                                  QtGui.QMessageBox.Ok)