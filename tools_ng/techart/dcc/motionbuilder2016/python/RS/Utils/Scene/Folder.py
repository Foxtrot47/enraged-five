import pyfbsdk as mobu


def CollectAllFolderItems(currentFolder, itemArray):
    """
    pass a folder to return the items from
    also includes child folders
    args:
        currentFolder (FBFolder)
        itemArray (list)
    """
    for item in currentFolder.Items:
        if not isinstance(item, mobu.FBFolder):
            itemArray.append(item)
        else:
            CollectAllFolderItems(item, itemArray)
