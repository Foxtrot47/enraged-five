'''

 Script Path: RS\Utils\Scene\Plot.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 15 October 2013
 
 Description: variety of plotting defs

'''

from pyfbsdk import *

import RS.Utils.Scene
import RS.Globals


##############################################################
#
#PlotCurrentTakeonSelected: plots all selected models in scene
#
##############################################################
def PlotCurrentTakeonSelected( keyReduceBool = False, allTakesBool = False, trnsRootBool = False ):
    
    plotOptions = FBPlotOptions()
    plotOptions.PlotOnFrame = True
    plotOptions.UseConstantKeyReducer = keyReduceBool
    plotOptions.PlotAllTakes = allTakesBool
    plotOptions.PlotTranslationOnRootOnly = trnsRootBool
    plotOptions.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
    
    FBSystem().CurrentTake.PlotTakeOnSelected( plotOptions )
    
    printString = ""
    for component in RS.Globals.gComponents:
        if component.Selected == True:
            printString = printString + component.LongName + ', '
            
    print 'Plotted selected scene objects:\n' + printString
    print


############################################################################
#
#PlotOnCharacterSkeleton: plots character to skeleton, only if Active = True
#
############################################################################
def PlotOnCharacterSkeleton( character, keyReduceBool = False, allTakesBool = False, trnsRootBool = False, plotPeriod=FBTime( 0, 0, 0, 1 ) ):
    
    plotOptions = FBPlotOptions()
    plotOptions.PlotOnFrame = True
    plotOptions.UseConstantKeyReducer = keyReduceBool
    plotOptions.PlotAllTakes = allTakesBool
    plotOptions.PlotTranslationOnRootOnly = trnsRootBool
    plotOptions.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
    plotOptions.PlotPeriod = plotPeriod
    
    if character.ActiveInput == True:

        character.PlotAnimation( FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, plotOptions )
        character.ActiveInput == False
        
        print 'Active Character Plotted: ' + character.LongName
        print
        
    else:
        print 'Character not on an Active Input: ' + character.LongName
        print


#########################################################
#
#PlotOnCharacterCtrlRig: plots character to a controlrig
#
#########################################################
def PlotOnCharacterCtrlRig( character, keyReduceBool = False, allTakesBool = False, trnsRootBool = False ):

        
    plotOptions = FBPlotOptions()
    plotOptions.UseConstantKeyReducer = keyReduceBool
    plotOptions.PlotAllTakes = allTakesBool
    plotOptions.PlotTranslationOnRootOnly = trnsRootBool
    plotOptions.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
    
    character.PlotAnimation( FBCharacterPlotWhere.kFBCharacterPlotOnControlRig, plotOptions )
    character.InputType = FBCharacterInputType.kFBCharacterInputMarkerSet
    character.ActiveInput = True
    
    print 'Plotted Char to Ctrl Rig: ' + character.Name
    print
    
    
############################################################################################
#
#PlotStoryTrackCharacter: plots character present in a story track, but not on a control rig
#
############################################################################################
def PlotStoryTrackCharacter( character, keyReduceBool = False, allTakesBool = False, trnsRootBool = False ):

    charFound = False
    rigFound = False

    #check if the character is hooked into any story tracks
    storyFolder = FBStory().RootFolder    
    for track in storyFolder.Tracks:
        if track.Character and track.Mute == False:
            if track.Character.LongName == character.LongName:
                charTrack = track
                charFound = True
                
    #find if control rig exists
    for rig in RS.Globals.gControlSets:
        for dst in range( rig.GetDstCount() ):
            if rig.GetDst( dst ).ClassName() == 'FBCharacter':
                if rig.GetDst( dst ).LongName == character.LongName:
                    charRig = rig
                    rigFound = True
    

    #character track and rig found
    if charFound == True and rigFound == True:
        #ensure controlrig is the input type and turn it on
        character.InputType = FBCharacterInputType.kFBCharacterInputMarkerSet
        character.ActiveInput = True
        character.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingFullBody
        #plot track to control rig
        PlotOnCharacterCtrlRig( character )
        #plot ctrlrig to skel
        PlotOnCharacterSkeleton( character )
        character.ActiveInput = False

        print 'StoryTrack Plotted: ' + character.LongName
        print
    
    #character track found, rig not present
    if charFound == True and rigFound == False:
        #create new control rig
        character.CreateControlRig( True )
        #ensure controlrig is the input type and turn it on
        character.InputType = FBCharacterInputType.kFBCharacterInputMarkerSet
        character.ActiveInput = True
        character.KeyingMode = FBCharacterKeyingMode.kFBCharacterKeyingFullBody
        #plot track to control rig
        PlotOnCharacterCtrlRig( character )      
        #plot ctrlrig to skel
        PlotOnCharacterSkeleton( character )
        character.ActiveInput = False
        
        print 'StoryTrack Plotted: ' + character.LongName
        print

