"""
Usage:
    This module should only contain functionality related to working with FBTime. 
    Current this module is a little muddle with a custom Range type that should probably use the FBTimeSpan
Author:
    Ross George
"""
import math

import pyfbsdk as mobu

from RS import Globals


class Range(object):
    """Represents a range of time. Should favour FBTimeSpan"""

    def __init__(self, pStart=mobu.FBTime.Infinity, pEnd=mobu.FBTime.MinusInfinity):
        """
        Args:
            pStart (FBTime, optional): start time
            pEnd (FBTime, optional): end time
        """
        self.Start = pStart
        self.End = pEnd

    def __str__(self):
        """string representation of the current frame range"""
        return "[{0}][{1}]".format(self.Start, self.End)

    def Merge(self, pInputFrameRange):
        """Merges the passed time range with this time range.

        Args:
            pInputFrameRange (FBTime): FBTime with data of the new time range
        """
        self.Start = GetLowerTime(self.Start, pInputFrameRange.Start)
        self.End = GetHigherTime(self.End, pInputFrameRange.End)

    def Valid(self):
        """Checks if the time range contains valid values (i.e. non infinity values where start is less than end)"""
        return self.Start.Get() < self.End.Get()

    def GetStartFrame(self):
        """Returns the start frame based on default FPS"""
        return self.Start.GetFrame()

    def GetEndFrame(self):
        """Return the end frame based on default FPS"""
        return self.End.GetFrame()


def GetLowerTime(pInputTimeA, pInputTimeB):
    """Gets lower time between two inputs

    Args:
        pInputTimeA (FBTime): FBTime to compare to pInputTimeB
        pInputTimeB (FBTime): FBTime to compare to pInputTimeA
    Returns:
        FBTime: Lower of the two times
    """
    if pInputTimeA.Get() <= pInputTimeB.Get():
        return pInputTimeA
    else:
        return pInputTimeB


def GetHigherTime(pInputTimeA, pInputTimeB):
    """Returns the greater of the two FBTime objects

    Args:
        pInputTimeA (FBTime): FBTime to compare to pInputTimeB
        pInputTimeB (FBTime): FBTime to compare to pInputTimeA
    Returns:
        FBTime: Great of the two FBTimes
    """
    if pInputTimeA.Get() >= pInputTimeB.Get():
        return pInputTimeA
    else:
        return pInputTimeB


def IsNegative(pInputTime):
    """Checks if the FBTime is below zero

    Args:
        pInputTimeA (FBTime): FBTime to be checked if it is below zero
    Return:
        bool: True or False if below zero
    """
    return pInputTime.Get() < 0 


def IsPositive(pInputTime):
    """Checks if the FBTime is above zero

    Args:
        pInputTimeA = FBTime; FBTime to be checked if it is below zero
    Returns:
        bool: True or False if below zero
    """
    return pInputTime.Get() > 0


class TimelineMonitor(object):
    """Select current camera in scene based on current frame"""
    def __init__(self):
        self.previousFrame = 0
        self.currentFrame = 0

    def TimelineSync(self, control, event):
        """Method that monitors changes in the timeline

        Args:
            control (object): object calling this method
            event (FBEvent): the event that caused this method to be called
        """
        self.currentFrame = mobu.FBSystem().LocalTime.GetFrame()
        if self.currentFrame != self.previousFrame:
            self.TimelineMethod(self.currentFrame, self.previousFrame)
            self.previousFrame = self.currentFrame

    def Register(self):
        """Register TimelineMonitor as a callback in Motion Builder"""
        self.Unregister()
        Globals.EvaluateManager.OnSynchronizationEvent.Add(self.TimelineSync)

    def Unregister(self):
        """ Unregister TimelineMonitor as a callback in Motion Builder """
        Globals.EvaluateManager.OnSynchronizationEvent.Remove(self.TimelineSync)

    def TimelineMethod(self, currentFrame, previousFrame):
        """Method that is called when the frame changes. This method is meant to be overridden.

        Args:
            currentFrame(int): the current frame
            previousFrame(int): the previous frame
        """
        print(currentFrame, previousFrame)


def getSceneFps():
    """"Gets the scene take's frame rate

    Returns:
        float: Fps value
    """
    return mobu.FBPlayerControl().GetTransportFpsValue()


def deleteByTimeRange(component, startTime, endTime):
    """Delete keyframes in range

    Args:
        component (FBComponent): Component to delete all keys on in range
        startTime (FBTime): Start range to delete
        endTime (FBTime): End time to delete
    """
    for destProperty in component.PropertyList:
        if not hasattr(destProperty, "GetAnimationNode"):
            continue

        destAnimNode = destProperty.GetAnimationNode()
        if not destAnimNode:
            continue

        for animNode in destAnimNode.Nodes:
            destAnimCurve = animNode.FCurve
            if not destAnimCurve:
                continue

            destAnimCurve.KeyDeleteByTimeRange(startTime, endTime)


def getSceneFrameRange():
    """"Gets the scene take's first and last frame

    Returns:
        list: the frame range of the scene
    """
    startFrame = Globals.System.CurrentTake.LocalTimeSpan.GetStart().GetFrame()
    endFrame = Globals.System.CurrentTake.LocalTimeSpan.GetStop().GetFrame()

    return startFrame, endFrame


def getStep(startFrame, endFrame, scaleFactor=5.0):
    """Get steps between two frame ranges based on count

    Args:
        startFrame (int): Start frame value
        endFrame (int): End frame value
        scaleFactor (float): Chunk scale factor

    Returns:
        int: step value
    """
    duration = endFrame - startFrame

    step = duration / scaleFactor
    # move the decimal place
    basePrecision = step // 10

    # check if remainder is greater than 2 (more than 100 frames)
    if step % 10 > 2 or basePrecision > 0:

        if basePrecision >= 1:
            multValue = 10 * basePrecision
        elif duration > 200:
            multValue = 10.0
        else:
            multValue = 5.0

        # take a value and round up by multValue such as round by 5 or 10 depending on precision
        step = int(math.ceil(step / multValue) * multValue)

    else:
        # else just grab the ceil of the step value
        step = int(math.ceil(step))

    return step


def toggleFPS():
    """Toggle fps of scene

    This is used as a workaround fix for url:bugstar:6624766
    """
    currentFPS = mobu.FBPlayerControl().GetTransportFps()

    # grab first mode that isn't current fps
    fpsMode = mobu.FBTimeMode.kFBTimeModeDefault
    for mode in dir(mobu.FBTimeMode):
        if not mode.startswith("kFBTimeMode"):
            continue

        fpsMode = getattr(mobu.FBTimeMode, mode)
        if not fpsMode == currentFPS:
            break

    # toggle it
    mobu.FBPlayerControl().SetTransportFps(fpsMode)
    mobu.FBPlayerControl().SetTransportFps(currentFPS)
