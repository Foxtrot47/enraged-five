"""
Collection of methods for dealing with motion builder groups and related to motion builder groups
"""

import pyfbsdk as mobu


from RS import Globals
from RS.Utils.Scene import Component


def GetAllItemsFromGroup(group, currentList=None):
    """
    Recursive method to get all the items from a group and its sub groups

    Args:
        group (FBGroup): Group the get all items from

    Kwargs:
        currentList (list of FBModels): The internal list of all found models.

    Return:
        list of FBModels: All the models found within the group and its subgroups
    """
    currentList = currentList or []
    currentList.append(group)
    for item in group.Items:
        if isinstance(item, mobu.FBGroup):
            currentList = GetAllItemsFromGroup(item, currentList=currentList)
            continue
        currentList.append(item)
    return currentList


def GetGroupsFromObject(mobuObject):
    """
    Get all groups which an motion builder, FBObject, is in

    Args:
        mobuObject (FBObject): The object to get all the groups from

    Return:
        list of FBGroups: All the groups which the motion builder object is part of
    """
    groups = []
    for idx in xrange(mobuObject.GetDstCount()):
        obj = mobuObject.GetDst(idx)
        if not isinstance(obj, mobu.FBGroup):
            continue
        groups.append(obj)
    return groups


def GetGroupsFromNamespace(namespace):
    """
    Get all groups which contain the passed namespace

    Args:
        namespace (string): The namespace to get all the groups from

    Return:
        list of FBGroups: All the groups which contain the namespace
    """
    return [group for group in Globals.Groups if Component.GetNamespace(group) == namespace]