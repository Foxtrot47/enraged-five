"""
Description:
    Utility functions to make working with objects in a MotionBuilder scene a bit easier.

"""
import re
import sys
import os

import pyfbsdk as mobu

import fbx
import FbxCommon

import RS.Config
from RS import Globals

from RS.Utils import UserPreferences
from RS.Tools.UI import Application
from RS import ProjectData
from RS.Utils.Scene import Constraint


class UndoBlock(object):
    """
    Captures motion builder code into one undo call
    """
    def __init__(self, name):
        """
        Constructor

        Arguments:
            name (string): name of the undo transaction
        """
        self.__name = name

    def __enter__(self):
        """
        method called by the with statement

        Returns:
            pyfbsdk.FBUndoManager
        """
        self.undoManager = mobu.FBUndoManager()
        self.undoManager.TransactionBegin(self.__name)
        return self.undoManager

    def __exit__(self, exceptionType, value, traceback):
        """
        Method called when the with statement ends.
        It also catches error values within the with block

        Arguments:
            exceptionType (Exception): type of exception encountered within the with statement
            value (string): error string
            traceback (traceback): the full traceback of the error
        """
        self.undoManager.TransactionEnd()

# # Functions # #

def ClearAnimationData( PropertyList, AllTakes= True, AllLayers= True ):
    '''
    Removes animation from the properties in the list provided, on the list of layers provided- default all layers
    '''
    currentTake         = RS.Globals.System.CurrentTake

    #If AllTakes is true we delete all animation on all takes
    if AllTakes:
        for take in RS.Globals.System.Scene.Takes:
            RS.Globals.System.CurrentTake   = take
            #If the allLayers is true go through each layer and delete the animation
            if AllLayers:
                for layerNumber in xrange( RS.Globals.System.CurrentTake.GetLayerCount()):
                    RS.Globals.System.CurrentTake.GetLayer( layerNumber ).SelectLayer( True, True )
                    RS.Utils.Scene.Property.ClearAnimationData( PropertyList )
            else :
                #If allLayers is false, delete animation from the current layer only
                RS.Utils.Scene.Property.ClearAnimationData( PropertyList )
    else:
        if AllLayers:
            for layerNumber in xrange( RS.Globals.System.CurrentTake.GetLayerCount()):
                RS.Globals.System.CurrentTake.GetLayer( layerNumber ).SelectLayer( True, True )
                RS.Utils.Scene.Property.ClearAnimationData( PropertyList )
        else :
            #If allLayers is false, delete animation from the current layer only
            RS.Utils.Scene.Property.ClearAnimationData( PropertyList )

    RS.Globals.System.CurrentTake = currentTake

def ForceFullSceneRefresh():
    '''
    Merges in an empty scene.
    This has the effect of refreshing the scene: emptying the undo stack and resetting the object flags
    '''
    pathName = os.path.normpath( RS.Config.Tool.Path.Temp + '\\blank.fbx')

    if not os.path.exists( pathName ):

        sdkManager, scene = FbxCommon.InitializeSdkObjects()

        exporter = fbx.FbxExporter.Create(sdkManager, "")

        if not sdkManager.GetIOSettings():
            ios = fbx.FbxIOSettings.Create(sdkManager, fbx.IOSROOT)
            sdkManager.SetIOSettings(ios)

        fileFormat = -1
        if fileFormat < 0 or fileFormat >= sdkManager.GetIOPluginRegistry().GetWriterFormatCount():
            fileFormat = sdkManager.GetIOPluginRegistry().GetNativeWriterFormat()
            formatCount = sdkManager.GetIOPluginRegistry().GetWriterFormatCount()
            for formatIndex in range(formatCount):
                if sdkManager.GetIOPluginRegistry().WriterIsFBX(formatIndex):
                    writerFormatDescription = sdkManager.GetIOPluginRegistry().GetWriterFormatDescription(formatIndex)
                    if "FBX binary" in writerFormatDescription:
                        fileFormat = formatIndex

        sdkManager.GetIOSettings().SetBoolProp(fbx.EXP_FBX_MATERIAL, False)
        sdkManager.GetIOSettings().SetBoolProp(fbx.EXP_FBX_TEXTURE, False)
        sdkManager.GetIOSettings().SetBoolProp(fbx.EXP_FBX_SHAPE, False)
        sdkManager.GetIOSettings().SetBoolProp(fbx.EXP_FBX_GOBO, False)
        sdkManager.GetIOSettings().SetBoolProp(fbx.EXP_FBX_ANIMATION, False)
        sdkManager.GetIOSettings().SetBoolProp(fbx.EXP_FBX_GLOBAL_SETTINGS, False)

        if exporter.Initialize(pathName, fileFormat , sdkManager.GetIOSettings()):
            exporter.Export(scene)
            exporter.Destroy()
            sdkManager.Destroy()

    mergeOptions = mobu.FBFbxOptions( True, pathName)
    mergeOptions.SetAll( mobu.FBElementAction.kFBElementActionMerge, False )
    mergeOptions.UpdateRecentFiles = False

    mobu.FBApplication().FileMerge(pathName, False, mergeOptions)

def EditEmbedMediaOptions(embedMedia=False):
    """
    Set embed media options.

    Keyword Arguments:

        embedMedia (boolean): Whether or not to embed media.

    """
    embedMediaStr = 'No'

    if embedMedia:
        embedMediaStr = 'Yes'

    configFile = mobu.FBConfigFile('@Application.txt')
    configFile.Set('SaveLoad', 'EmbedMedias', embedMediaStr)


def FindModelByName(name, includeNamespace=False):
    """
    Mimics the deprecated behavior of mobu.FBFindModelByName.

    Arguments:
        name (string): The model name to search.
        includeNamespace (boolean): to include the namespace of the object if its found

    Returns:
        The found model, None otherwise.

    """
    componentList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(name, componentList, includeNamespace, True)

    if len(componentList) > 0:
        return componentList[0]

    else:
        return None


def FindObjectsByNamespace(namePattern, modelsOnly=False, namespaceOnly=False):
    """
    mobu.FBFindObjectsByNamespace was deprecated in 2013.  This function is meant to replace it.

    Arguments:

        namePattern (string): The name pattern to search.  If no wildcard is supplied, one will be appended to the end
                     automatically.

    Keyword Arguments:
        modelsOnly(boolean): Whether we search everything in the scene, or just the models.  By default, we search
                            everything.
        includeNamespace (boolean): to include the namespace of the object if its found

    Returns:

        A list of the found components.

    """
    if not namePattern.endswith(':') and not namespaceOnly:
        namePattern += ':'

    # Need to ensure there is a wildcard in the name pattern.  Otherwise,
    # mobu.FBFindObjectsByName doesn't work.
    if '*' not in namePattern:
        namePattern += '*'

    componentList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(namePattern, componentList, True, modelsOnly)

    # So, it appears that mobu.FBFindObjectsByName includes a mobu.FBNamespace object type in the component list.
    # I have never seen this before, perhaps it's new in 2014.
    return [item for item in componentList if not isinstance(item, mobu.FBNamespace)]


def GetComponentByName(name, excludeByTypes=()):
    """
    Finds components with the given name

    Arguments:
        name (string): name of the component that you want to get
        excludeByType (list[Class]): list of pyfbsdk classes that you want to exclude from the selection

    """
    components = filter(lambda component: component.LongName == name and component.__class__ not in excludeByTypes,
                        mobu.FBSystem().Scene.Components)
    if components:
        return components[0]


def GetComponentByNameAndType(Type, Name):
    """
    Gets the first component that matches the name and type provided

    Arguments:
        Type (pyfbsdk.FBComponent class): the type of class that the object belongs to
        Name (string): name of the object

    Return:
        FBComponent(or an object that subclasses it) or None

    """
    componentList = GetComponentsByNamePatternAndType(Type, Name)
    mobu.FBFindObjectsByName(Name, componentList, True, False)

    for component in componentList:
        if Name.lower() in (component.LongName.lower(), component.Name.lower()):
            return component

    return None


def GetComponentsByNamePatternAndType(Type, NamePattern):
    """
    Gets the first component that matches the name and type provided

    Arguments:

        Type (pyfbsdk.FBComponent class): the type of class that the object belongs to
        NamePattern (string): name of the object

    Return:
        List[FBComponents, etc.]

    """
    componentList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(NamePattern, componentList, True, False)

    processedComponentList = mobu.FBComponentList()
    processedComponentListAppend = processedComponentList.append
    [processedComponentListAppend(component) for component in componentList if component.__class__ is Type]

    return processedComponentList


def GetSafeNamespaceAndName(longName):
    """
    Safely parses the supplied long name to return the namespace and name part.  Deals with
    namespaces who have multiple : characters in the name.

    Arguments:
        longName (string): A string representing the long name.

    Returns:
        A list containing the namespace as the first item, and the name as the second.

    """
    namespace = ""

    names = str(longName).split(':')
    name = names[-1]

    if names[:-1]:
        namespace = "".join(["{0}:".format(eachNamespace) for eachNamespace in names[:-1] if eachNamespace.strip()])

    return namespace[:-1], name


def GetNamespacesFromSelection(filterByType=mobu.FBComponent):
    """
    Gets the namespaces from the selected objects in the scene

    Arguments:
        filterByType (FBComponent Class): type to filter objects by

    Return:
        list[FBComponent, etc.]

    """
    return list(set([GetSafeNamespaceAndName(selection.LongName)[0]
                     for selection in GetSelection(filterByType=filterByType)]))


def GetSceneName():
    """ Returns the name of the scene"""
    return RS.Utils.Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName)


def SwitchViewerCamera(pCamera):
    """
    Switches the viewer camera to the provided camera

    Arguments:
        pCamera (FBCamera): camera to set the viewer to use

    """
    if pCamera:
        mobu.FBApplication().SwitchViewerCamera(pCamera)

    else:
        mobu.FBMessageBox("Camera Viewer", "Sorry, specified Camera does not exist.", "Ok")


def DeSelectAll():
    """ Deselect everything that is currently selected. """
    [setattr(iComponent, "Selected", False) for iComponent in Globals.gComponents if iComponent.Selected]


# We should Deprecate these and use the global Time script!!!!!!
# ------------------------------------------------------------
def GetStartFrame():
    """ Gets the current start frame from the player control/timeslider """
    lPlayerControl = mobu.FBPlayerControl()
    lPlayerControl.GotoStart()
    mobu.FBSystem().Scene.Evaluate()
    lStartFrame = mobu.FBSystem().LocalTime

    return lStartFrame


def GetEndFrame():
    """ Gets the current last frame from the player control/timeslider """
    lPlayerControl = mobu.FBPlayerControl()
    lPlayerControl.GotoEnd()
    mobu.FBSystem().Scene.Evaluate()
    lEndFrame = mobu.FBSystem().LocalTime

    return lEndFrame


# ------------------Deprecate------------------------------------------


def GetSetVector(gGetModel, pSetModel, pGlobal=True):
    """
    Copies the translation values from one component to another
    Arguments:
        pGetModel (FBModel): object to copy translations from
        pSetModel (FBModel): object to paste translations to
        pGlobal (boolean): the values should be global

    """
    lVector = mobu.FBVector3d()
    gGetModel.GetVector(lVector, mobu.FBModelTransformationType.kModelTranslation, pGlobal)
    pSetModel.SetVector(lVector, mobu.FBModelTransformationType.kModelTranslation, pGlobal)

    mobu.FBSystem().Scene.Evaluate()


def GetParent(node):
    """
    Recursively searches for the top node of a hierarchy.

    Arguments:

        node (FBModel or string): The node to search.

    Returns:
        The top parent node in the hierarchy

    """
    if isinstance(node, basestring):
        node = FindModelByName(node)

    if node:
        if node.Parent:
            return GetParent(node.Parent)

        else:
            return node


def GetParentList(node, parentList):
    """
    Recursively searches for each Parent node, adds to a list.

    Arguments:

        node(FBModel or string): The node to search.

    Returns:
        The list of parent nodes (not including the original child)
        doesnt remove dupes

    """
    if isinstance(node, basestring):
        node = FindModelByName(node)

    if node:
        if node.Parent:
            parentList.append(node.Parent)
            return GetParentList(node.Parent, parentList)

        else:
            return node


def GetChildren(root, childList, pattern="", addRoot=True):
    """
    Returns all children under a node as a list.

    Arguments:
        root (FBModel or string): The current or starting node.
        childList (list): An array to populate.

    Keyword Arguments:
        pattern (string): Add the child node if it starts with this pattern.
        addRoot (boolean): Whether or not the root node is added to the list.

    Returns:
        The list of children.

    """
    if root:
        if isinstance(root, basestring):
            root = mobu.FBFindModelByLabelName(root)

        if root.LongName is not None:
            if addRoot == 1 and root.LongName.startswith(pattern):
                childList.append(root)

        for iChild in root.Children:

            if pattern == "" or (iChild.LongName is not None and iChild.LongName.startswith(pattern)):
                childList.append(iChild)

                if iChild.Children is None:
                    return childList

                else:
                    GetChildren(iChild, childList, pattern, False)


def GetChildrenRegex(model, pattern="", regexFlags=0, add=False, level=0):
    """
    Returns all children under a node as a list.

    Arguments:

        model (FBModel): The current or starting model component.

    Keyword Arguments:

        pattern (string) Add the child node that follows this regular expression
        regexFlags (re.Flags) accepts flags from the regular expression module ei. re.I, re.M, re.V, etc.
        add: (boolean): Whether or not this node should be added to the list.
        level (int): depth of recursion, only there for

    Returns:
        The list of children.
    """

    if not model:
        return []

    childList = []
    search = re.compile(pattern, regexFlags).search

    if add and search(model.Name):
        childList.append(model)

    for eachChild in model.Children:
        childList += GetChildrenRegex(eachChild, pattern, regexFlags, True, level + 1)

    return childList


def GetAllChildren(component, ignoreComponents=[], onlyTopChildren=False):
    """
    Gets all children of a component in the scene.
    It uses the GetSrc method instead of the Children method to get all the Children of a component even if that
    component isn't a model component.

    Arguments:
        component (mobu.FBComponent): An mobu.FBComponent object or an object that inherits mobu.FBComponent
        ignoreComponents (list[mobu.FBComponent(), etc.]): list of objects not to return if they are children.

    Return:
        mobu.FBComponent

    """
    # There some MB Components that can't have children but can be children
    # so we are going to skip them if they asked if they have children
    ignoreTypes = ["FBConstraint", "FBShader", "FBMaterial", "FBPose"]
    children = []

    # This is to work around a python bug where if you manipulate change a list that is the default value of an
    # argument, those changes stick around for the next time you use the method.
    # Here we create another list instance with the same value to avoid editing the instanced stored in the default
    # arguments

    ignoreComponents = list(ignoreComponents)

    if component not in mobu.FBSystem().Scene.Components or component.__class__.__name__ in ignoreTypes:
        return children

    if component not in ignoreComponents:
        ignoreComponents += [component]

    for index in xrange(component.GetSrcCount()):
        child = component.GetSrc(index)

        if isinstance(child, mobu.FBComponent) and child not in ignoreComponents:
            children.append(child)

            if not onlyTopChildren:
                children.extend(GetAllChildren(child, ignoreComponents=ignoreComponents + [child]))

    return children


def GetConnections(component, filterByType=(mobu.FBComponent,), incoming=True, outgoing=True):
    """
    Get other components that are connected to passed component

    Arguments:
        component: (mobu.FBComponent): component to search for connections
        filterByType (list[pyfbsdk classes, etc.]) classes to filter connections out by
        incoming (boolean): list incoming connections
        outgoing (boolean): list outgoing connections

    Return:
        list

    """
    connections = []
    if incoming:
        connections = [component.GetSrc(index) for index in xrange(component.GetSrcCount())
                       if component.GetSrc(index).__class__ in filterByType]
    if outgoing:
        connections.extend([component.GetDst(index) for index in xrange(component.GetDstCount())
                            if component.GetDst(index).__class__ in filterByType])

    return filter(None, connections)


def GetIncomingConnections(component, filterByType=(mobu.FBComponent,)):
    """
    Get other components where the passed component is the end connection

    Arguments:
        component (mobu.FBComponent): component to search for connections
        filterByType (list[pyfbsdk classes]): classes to filter connections out by

    Return:
        list

    """
    return GetConnections(component, filterByType=filterByType, incoming=True, outgoing=False)


def GetOutgoingConnections(component, filterByType=(mobu.FBComponent,)):
    """
    Get other components where the passed component is the source connection

    Arguments:
        component (mobu.FBComponent): component to search for connections
        filterByType (list[pyfbsdk classes]): classes to filter connections out by

    Return:
        list

    """
    return GetConnections(component, filterByType=filterByType, incoming=False, outgoing=True)


def GetSelection(filterByType=mobu.FBComponent):
    """
    Gets a list of the current selection
    Arguments:
        filterByType (FBComponent Class): type to filter objects by
    Return:
        list[FBComponent, etc.]
    """
    return filter(lambda component: component.Selected and isinstance(component, filterByType),
                  mobu.FBSystem().Scene.Components)


def Delete(*components):
    """
    Deletes a component and all its children

    Arguments:
        components (list[mobu.FBComponent, etc.]): a list of mobu.FBComponent instances to delete

    """
    for component in components:

        if component not in mobu.FBSystem().Scene.Components:
            continue

        for child in GetAllChildren(component):
            if child in mobu.FBSystem().Scene.Components:
                child.FBDelete()

        if component in mobu.FBSystem().Scene.Components:
            component.FBDelete()


def FindAnimationNode(parent, name):
    """
    Find an animation node.

    Arguments:
        parent(FBAnimationNode): Animation Node with "children" animation nodes
        name (string): name of the child animation node to return

    """
    result = None

    for iNode in parent.Nodes:
        if iNode.Name == name:
            result = iNode
            break

    return result


def ConnectBox(nodeOut, nodeOutPos, nodeIn, nodeInPos):
    """
    Connects boxes

    Arguments:
        nodeOut (FBComponent): Component that has FBAnimationNodes you want to use as the output
        nodeOutPos (string): name of the animation node to get from nodeOut
        nodeIn (FBComponent): Component that has FBAnimationNodes you want to use as the input
        nodeInPos (string): name of the animation node to get from nodeIn

    """
    myNodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos)
    myNodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos)

    if myNodeOut and myNodeIn:
        mobu.FBConnect(myNodeOut, myNodeIn)


def FindBoxesToConnect(pRelationCon, pOutBox, pOutParam, pInBox, pInParam):
    """
    Finds boxes to connect

    Arguments:
        pRelationCon (FBRelationConstraint): relationship constraint to look for boxes in
        pOutBox (string):  name of the box to get from the relationship constraint to use as the output
        pOutParam (string): name of the animation node to get from pOutBox
        pInBox (string): name of the box to get from the relationship constraint to use as the input
        pInParam (string): name of the animation node to get from pInBox

    """
    lBoxA = None
    lBoxB = None

    for iBox in pRelationCon.Boxes:
        if iBox.Name == pOutBox:
            lBoxA = iBox
            continue

    for iBox in pRelationCon.Boxes:
        if iBox.Name == pInBox:
            lBoxB = iBox

    if lBoxA is not None and lBoxB is not None:
        ConnectBox(lBoxA, pOutParam, lBoxB, pInParam)


def AlignTranslation(pModel, pAlignTo, pAlignX, pAlignY, pAlignZ):
    """
    Aligns the translation of one component to another

    Arguments:
        pModel (FBModel): component to align
        pAlignTo (FBModel): component to align to
        pAlignX (boolean): align the x translation
        pAlignY (boolean): align the x translation
        pAlignZ (boolean): align the x translation

    """
    lAlignPos = mobu.FBVector3d()
    lModelPos = mobu.FBVector3d()
    pAlignTo.GetVector(lAlignPos)
    pModel.GetVector(lModelPos)

    if pAlignX:
        lModelPos[0] = lAlignPos[0]

    if pAlignY:
        lModelPos[1] = lAlignPos[1]

    if pAlignZ:
        lModelPos[2] = lAlignPos[2]

    pModel.SetVector(lModelPos)


def AssignJoints(character):
    """
    Assigns joints to the character provided

    TODO (Hayes 7/17/2013): This looks more like a setup function, and should probably live some place else.
    """
    # # Character skeleton array -- RDR ##

    if "RDR3" in RS.Config.Project.Name:

        lJointAssignment = {'LeftHandThumb4': Globals.gSkelArray[0],
                            'LeftHandIndex4': Globals.gSkelArray[1],
                            'LeftHandMiddle4': Globals.gSkelArray[2],
                            'LeftHandRing4': Globals.gSkelArray[3],
                            'LeftHandPinky4': Globals.gSkelArray[4],
                            'RightHandThumb4': Globals.gSkelArray[5],
                            'RightHandIndex4': Globals.gSkelArray[6],
                            'RightHandMiddle4': Globals.gSkelArray[7],
                            'RightHandRing4': Globals.gSkelArray[8],
                            'RightHandPinky4': Globals.gSkelArray[9],
                            'LeftShoulder': Globals.gSkelArray[10],
                            'LeftArm': Globals.gSkelArray[11],
                            'LeftForeArm': Globals.gSkelArray[12],
                            'LeftHandThumb1': Globals.gSkelArray[13],
                            'LeftHandThumb2': Globals.gSkelArray[14],
                            'LeftHandThumb3': Globals.gSkelArray[15],
                            'LeftHandIndex1': Globals.gSkelArray[16],
                            'LeftHandIndex2': Globals.gSkelArray[17],
                            'LeftHandIndex3': Globals.gSkelArray[18],
                            'LeftHandMiddle1': Globals.gSkelArray[19],
                            'LeftHandMiddle2': Globals.gSkelArray[20],
                            'LeftHandMiddle3': Globals.gSkelArray[21],
                            'LeftHandRing1': Globals.gSkelArray[22],
                            'LeftHandRing2': Globals.gSkelArray[23],
                            'LeftHandRing3': Globals.gSkelArray[24],
                            'LeftHandPinky1': Globals.gSkelArray[25],
                            'LeftHandPinky2': Globals.gSkelArray[26],
                            'LeftHandPinky3': Globals.gSkelArray[27],
                            'RightShoulder': Globals.gSkelArray[28],
                            'RightArm': Globals.gSkelArray[29],
                            'RightForeArm': Globals.gSkelArray[30],
                            'RightHandThumb1': Globals.gSkelArray[31],
                            'RightHandThumb2': Globals.gSkelArray[32],
                            'RightHandThumb3': Globals.gSkelArray[33],
                            'RightHandIndex1': Globals.gSkelArray[34],
                            'RightHandIndex2': Globals.gSkelArray[35],
                            'RightHandIndex3': Globals.gSkelArray[36],
                            'RightHandMiddle1': Globals.gSkelArray[37],
                            'RightHandMiddle2': Globals.gSkelArray[38],
                            'RightHandMiddle3': Globals.gSkelArray[39],
                            'RightHandRing1': Globals.gSkelArray[40],
                            'RightHandRing2': Globals.gSkelArray[41],
                            'RightHandRing3': Globals.gSkelArray[42],
                            'RightHandPinky1': Globals.gSkelArray[43],
                            'RightHandPinky2': Globals.gSkelArray[44],
                            'RightHandPinky3': Globals.gSkelArray[45],
                            'LeftUpLeg': Globals.gSkelArray[46],
                            'RightUpLeg': Globals.gSkelArray[47],
                            'Hips': Globals.gSkelArray[48],
                            'LeftLeg': Globals.gSkelArray[49],
                            'LeftFoot': Globals.gSkelArray[50],
                            'LeftFootExtraFinger1': Globals.gSkelArray[51],
                            'LeftToeBase': Globals.gSkelArray[52],
                            'RightLeg': Globals.gSkelArray[53],
                            'RightFoot': Globals.gSkelArray[54],
                            'RightFootExtraFinger1': Globals.gSkelArray[55],
                            'RightToeBase': Globals.gSkelArray[56],
                            'RightHand': Globals.gSkelArray[57],
                            'LeftHand': Globals.gSkelArray[58],
                            'Spine': Globals.gSkelArray[59],
                            'Spine1': Globals.gSkelArray[60],
                            'Spine2': Globals.gSkelArray[61],
                            'Spine3': Globals.gSkelArray[62],
                            'Spine4': Globals.gSkelArray[63],
                            'Spine5': Globals.gSkelArray[64],
                            'Spine6': Globals.gSkelArray[65],
                            'Neck': Globals.gSkelArray[66],
                            'Neck1': Globals.gSkelArray[67],
                            'Neck2': Globals.gSkelArray[68],
                            'Head': Globals.gSkelArray[69],
                            'LeftFootFloor': Globals.gContactsArray[0],
                            'RightFootFloor': Globals.gContactsArray[1],
                            'LeftHandFloor': Globals.gContactsArray[2],
                            'RightHandFloor': Globals.gContactsArray[3],
                            'LeftInHandIndex': Globals.gSkelArray[98],
                            'LeftInHandMiddle': Globals.gSkelArray[99],
                            'LeftInHandRing': Globals.gSkelArray[100],
                            'LeftInHandPinky': Globals.gSkelArray[101],
                            'RightInHandIndex': Globals.gSkelArray[102],
                            'RightInHandMiddle': Globals.gSkelArray[103],
                            'RightInHandRing': Globals.gSkelArray[104],
                            'RightInHandPinky': Globals.gSkelArray[105]}

    # # Character skeleton array -- GTA, or other ##

    else:
        lJointAssignment = {'LeftHandThumb4': Globals.gSkelArray[0],
                            'LeftHandIndex4': Globals.gSkelArray[1],
                            'LeftHandMiddle4': Globals.gSkelArray[2],
                            'LeftHandRing4': Globals.gSkelArray[3],
                            'LeftHandPinky4': Globals.gSkelArray[4],
                            'RightHandThumb4': Globals.gSkelArray[5],
                            'RightHandIndex4': Globals.gSkelArray[6],
                            'RightHandMiddle4': Globals.gSkelArray[7],
                            'RightHandRing4': Globals.gSkelArray[8],
                            'RightHandPinky4': Globals.gSkelArray[9],
                            'LeftShoulder': Globals.gSkelArray[10],
                            'LeftArm': Globals.gSkelArray[11],
                            'LeftForeArm': Globals.gSkelArray[12],
                            'LeftHandThumb1': Globals.gSkelArray[13],
                            'LeftHandThumb2': Globals.gSkelArray[14],
                            'LeftHandThumb3': Globals.gSkelArray[15],
                            'LeftHandIndex1': Globals.gSkelArray[16],
                            'LeftHandIndex2': Globals.gSkelArray[17],
                            'LeftHandIndex3': Globals.gSkelArray[18],
                            'LeftHandMiddle1': Globals.gSkelArray[19],
                            'LeftHandMiddle2': Globals.gSkelArray[20],
                            'LeftHandMiddle3': Globals.gSkelArray[21],
                            'LeftHandRing1': Globals.gSkelArray[22],
                            'LeftHandRing2': Globals.gSkelArray[23],
                            'LeftHandRing3': Globals.gSkelArray[24],
                            'LeftHandPinky1': Globals.gSkelArray[25],
                            'LeftHandPinky2': Globals.gSkelArray[26],
                            'LeftHandPinky3': Globals.gSkelArray[27],
                            'RightShoulder': Globals.gSkelArray[28],
                            'RightArm': Globals.gSkelArray[29],
                            'RightForeArm': Globals.gSkelArray[30],
                            'RightHandThumb1': Globals.gSkelArray[31],
                            'RightHandThumb2': Globals.gSkelArray[32],
                            'RightHandThumb3': Globals.gSkelArray[33],
                            'RightHandIndex1': Globals.gSkelArray[34],
                            'RightHandIndex2': Globals.gSkelArray[35],
                            'RightHandIndex3': Globals.gSkelArray[36],
                            'RightHandMiddle1': Globals.gSkelArray[37],
                            'RightHandMiddle2': Globals.gSkelArray[38],
                            'RightHandMiddle3': Globals.gSkelArray[39],
                            'RightHandRing1': Globals.gSkelArray[40],
                            'RightHandRing2': Globals.gSkelArray[41],
                            'RightHandRing3': Globals.gSkelArray[42],
                            'RightHandPinky1': Globals.gSkelArray[43],
                            'RightHandPinky2': Globals.gSkelArray[44],
                            'RightHandPinky3': Globals.gSkelArray[45],
                            'LeftUpLeg': Globals.gSkelArray[46],
                            'RightUpLeg': Globals.gSkelArray[47],
                            'Hips': Globals.gSkelArray[48],
                            'LeftLeg': Globals.gSkelArray[49],
                            'LeftFoot': Globals.gSkelArray[50],
                            'LeftFootExtraFinger1': Globals.gSkelArray[51],
                            'LeftToeBase': Globals.gSkelArray[52],
                            'RightLeg': Globals.gSkelArray[53],
                            'RightFoot': Globals.gSkelArray[54],
                            'RightFootExtraFinger1': Globals.gSkelArray[55],
                            'RightToeBase': Globals.gSkelArray[56],
                            'RightHand': Globals.gSkelArray[57],
                            'LeftHand': Globals.gSkelArray[58],
                            'Spine': Globals.gSkelArray[59],
                            'Spine1': Globals.gSkelArray[60],
                            'Spine2': Globals.gSkelArray[61],
                            'Spine3': Globals.gSkelArray[62],
                            'Neck': Globals.gSkelArray[63],
                            'Head': Globals.gSkelArray[64],
                            'LeftFootFloor': Globals.gContactsArray[0],
                            'RightFootFloor': Globals.gContactsArray[1],
                            'LeftHandFloor': Globals.gContactsArray[2],
                            'RightHandFloor': Globals.gContactsArray[3]}

    for iSlot, iJoint in lJointAssignment.iteritems():
            lJointLink = (iSlot + "Link")
            lProp = character.PropertyList.Find(lJointLink)
            lReferencedJoint = mobu.FBFindModelByLabelName(iJoint)
            lProp.append(lReferencedJoint)


def CreateParentConstraint(pSrcModel, pDstModel, pMoveToSource=True, pActive=True, pLock=True, pWeight=100.0, pTX=True,
                           pTY=True, pTZ=True, pRX=True, pRY=True, pRZ=True):
    """
    Creates of a parent constraint between source and destination model.

    Arguments:
        pSrcModel (mobu.FBModel) : source model
        pDstModel (mobu.FBModel) : destination model
        pActive (bool): set the constraint active
        pLock (bool): set the lock state
        pMoveToSource (bool): if true copies translation and rotation values from source to destination, equivalent to
                                not snapping the constraint in the UI
        pTX (bool): constrain the X translation
        pTY (bool): constrain the Y translation
        pTZ (bool): constrain the Z translation
        pRX (bool): constrain the X rotation
        pRY (bool): constrain the Y rotation
        pRZ (bool): constrain the Z rotation
    """

    if pSrcModel is None or pDstModel is None:
        return

    lProp = None
    lRot = mobu.FBVector3d()
    lPos = mobu.FBVector3d()

    # 4 is index in the constraint manager list for parent constraint
    c = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
    if c:

        c.Name = "%s->%s_Parent" % (pSrcModel.Name.replace(':', '_'), pDstModel.Name.replace(':', '_'))
        c.ReferenceAdd(0, pDstModel)
        mobu.FBSystem().Scene.Evaluate()
        c.ReferenceAdd(1, pSrcModel)
        mobu.FBSystem().Scene.Evaluate()

        # copy the transform from source to destination if specified
        if not pMoveToSource:
            pDstModel.GetVector(lRot, mobu.FBModelTransformationType.kModelRotation, True)
            mobu.FBSystem().Scene.Evaluate()
            pDstModel.GetVector(lPos, mobu.FBModelTransformationType.kModelTranslation, True)
            mobu.FBSystem().Scene.Evaluate()

        # set the active state
        c.Active = pActive

        if not pMoveToSource:
            pDstModel.SetVector(lRot, mobu.FBModelTransformationType.kModelRotation, True)
            mobu.FBSystem().Scene.Evaluate()
            pDstModel.SetVector(lPos, mobu.FBModelTransformationType.kModelTranslation, True)
            mobu.FBSystem().Scene.Evaluate()

        # set lock attribute
        lProp = c.PropertyList.Find('Lock')
        if lProp:
            lProp.Data = pLock
            mobu.FBSystem().Scene.Evaluate()

        # set weight attribute
        lProp = c.PropertyList.Find('Weight')
        if lProp:
            mobu.FBSystem().Scene.Evaluate()
            lProp.Data = pWeight
            mobu.FBSystem().Scene.Evaluate()

        # set the affects properties
        if not pTX:
            lProp = c.PropertyList.Find('AffectTranslationX')
            if lProp:
                lProp.Data = 0
                mobu.FBSystem().Scene.Evaluate()

        if not pTY:
            lProp = c.PropertyList.Find('AffectTranslationY')
            if lProp:
                lProp.Data = 0
                mobu.FBSystem().Scene.Evaluate()

        if not pTZ:
            lProp = c.PropertyList.Find('AffectTranslationZ')
            if lProp:
                lProp.Data = 0
                mobu.FBSystem().Scene.Evaluate()

        if not pRX:
            lProp = c.PropertyList.Find('AffectRotationX')
            if lProp:
                lProp.Data = 0
                mobu.FBSystem().Scene.Evaluate()

        if not pRY:
            lProp = c.PropertyList.Find('AffectRotationY')
            if lProp:
                lProp.Data = 0
                mobu.FBSystem().Scene.Evaluate()

        if not pRZ:
            lProp = c.PropertyList.Find('AffectRotationZ')
            if lProp:
                lProp.Data = 0
                mobu.FBSystem().Scene.Evaluate()

        mobu.FBSystem().Scene.Evaluate()

    del (lProp, lRot, lPos)

    return c


def Align(pModel, pAlignTo, pAlignTransX=True, pAlignTransY=True, pAlignTransZ=True, pAlignRotX=True, pAlignRotY=True,
          pAlignRotZ=True):
    """
    Aligns 2 objects together, aligns the trns & rot for XYZ axis

    Arguments:
        pModel (FBModel): component to align
        pAlignTo (FBModel): component to align to
        pAlignTransX (boolean): align the x translation
        pAlignTransY (boolean): align the x translation
        pAlignTransZ (boolean): align the x translation
        pAlignRotX (boolean): align the x rotation
        pAlignRotY (boolean): align the x rotation
        pAlignRotZ (boolean): align the x rotation
    """

    lAlignTransPos = mobu.FBVector3d()
    lModelTransPos = mobu.FBVector3d()
    lAlignRotPos = mobu.FBVector3d()
    lModelRotPos = mobu.FBVector3d()

    pAlignTo.GetVector(lAlignTransPos)
    pModel.GetVector(lModelTransPos)

    pAlignTo.GetVector(lAlignRotPos, mobu.FBModelTransformationType.kModelRotation)
    pModel.GetVector(lModelRotPos, mobu.FBModelTransformationType.kModelRotation)

    if pAlignTransX:
        lModelTransPos[0] = lAlignTransPos[0]
    if pAlignTransY:
        lModelTransPos[1] = lAlignTransPos[1]
    if pAlignTransZ:
        lModelTransPos[2] = lAlignTransPos[2]
    if pAlignRotX:
        lModelRotPos[0] = lAlignRotPos[0]
    if pAlignRotY:
        lModelRotPos[1] = lAlignRotPos[1]
    if pAlignRotZ:
        lModelRotPos[2] = lAlignRotPos[2]

    pModel.SetVector(lModelTransPos)
    pModel.SetVector(lModelRotPos, mobu.FBModelTransformationType.kModelRotation)


def Close():
    """ Closes Motion Builder """
    mainWindow = Application.GetMainWindow()
    mobu.FBSystem().Scene.Evaluate()
    mainWindow.close()


def SetWindowTitle(filename="", project="", numberOfSpaces=10):
    """
    Sets the title for the Motion Builder Main Window

    Arguments:
        filename (string): name of the file, defaults to the current name of the file or untitled if the name has not
                            been saved before
        project (string): the project that is currently being worked on
        space (int): the number of spaces between text
    """
    mainwindow = Application.GetMainWindow()
    currentTitle = mainwindow.windowTitle()

    version = currentTitle[:18]

    filename = filename or mobu.FBApplication().FBXFileName.strip() or "Untitled"
    project = project or RS.Config.Project.Name
    is64bit = sys.maxsize > 2 ** 32
    space = " " * numberOfSpaces

    # not the best way to get the hotfix version but it works
    hotfix = os.path.basename(os.path.dirname(os.path.dirname(Globals.System.ApplicationPath)))
    if "MotionBuilder" in hotfix:
        hotfix = ""
    else:
        "{} ".format(hotfix)

    title = ("{version} {hotfix}- {bit} bit{space}{filename}{space}"
             "Project : {project}{space}Tools : {toolPath}").format(
        version=version, hotfix=hotfix, filename=filename, project=RS.Config.Project.Name,
        toolPath=RS.Config.Tool.Path.Root, space=space, bit=32 * 2 ** is64bit)

    if "wildwest" in RS.Config.Tool.Path.TechArt:
        title = "{}{}{}".format(title, space, "--WORKING IN WILDWEST--")
        project = "wildwest"


    mainwindow.setWindowTitle(title)

    styleSheetString = ""
    if UserPreferences.__Readini__("menu", "Enable Project Menu Color", default_value=True, notFoundValue=True):
        backgroundColor, textColor = ProjectData.data.GetConfig("MenuColour", defaultValue=("#ffdc19", "black"))
        styleSheetString = ("QMenuBar{"
                            "background-color: %s;"
                            "color: %s;"
                            "}") % (backgroundColor, textColor)
        # format breaks with the string below, so we use the old way of combining strings %s
    mainwindow.setStyleSheet(styleSheetString)

def EvaluateTimeline():
    Globals.Player.StepForward()
    Globals.Player.StepBackward()
    Globals.Scene.Evaluate()
