import os
import stat
import tempfile

import pyfbsdk as mobu
from PySide import QtGui

from RS import Globals


def getProducerPerspective():
    """Get the Producer Perspective camera

    Returns:
        FBCamera
    """
    for camera in Globals.Cameras:
        if camera.Name == "Producer Perspective":
            return camera
    raise ValueError("Unable to find Producer Perspective Camera")


def getCurrentCamera(pane=0):
    """Get the camera the user is currently viewing through.

    Args:
        pane (int): The pane used, staring from 0

    Returns:
        FBCamera
    """
    return Globals.System.Renderer.GetCameraInPane(pane)


def setCamerasBackground(color=None):
    """Change the background color of all cameras to the provided FBColor.
    This can be useful because mobu's default camera background might not contrast enough with characters in the scene.

    Args:
        color (mobu.FBColor): color to change the background to, in RGB 0 to 1.

    """
    color = color or mobu.FBColor(0.6, 0.6, 0.6)

    for camera in Globals.Cameras:
        camera.BackGroundColor = color


def renderToFile(camera, filePath, width=512, height=512):
    """Render the given camera viewport to a image file.

    Args:
        camera (mobu.FBCamera): camera to render
        filePath (str): destination file the render should go to
        width (int): horizontal resolution of the final image
        height (int): vertical resolution of the final image
    """
    origCamera = Globals.System.Renderer.GetCameraInPane(0)
    tempRender = os.path.join(tempfile.tempdir, "raw_render.tif")
    try:
        if not os.path.exists(os.path.dirname(filePath)):
            os.makedirs(os.path.dirname(filePath))

        Globals.System.Renderer.SetCameraInPane(camera, 0)
        if os.path.exists(filePath):
            os.chmod(filePath, stat.S_IWRITE)

        # Render Image
        image = mobu.FBVideoGrabber().RenderSnapshot(width, height)
        image.ConvertFormat(mobu.FBImageFormat.kFBImageFormatRGB24)
        image.WriteToTif(tempRender, "", False)

        # convert automatically the raw render to the format of the provided file
        pixmap = QtGui.QPixmap(tempRender)
        # save will find the right format automatically from the file suffix
        pixmap.save(filePath)

    finally:
        os.remove(tempRender)
        Globals.System.Renderer.SetCameraInPane(origCamera, 0)
