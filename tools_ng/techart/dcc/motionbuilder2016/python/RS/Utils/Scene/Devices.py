import pyfbsdk as mobu

from RS import Globals


def GetAllDeviceByType(deviceType):
    return [dev for dev in Globals.Devices if dev.ClassName() == deviceType]
