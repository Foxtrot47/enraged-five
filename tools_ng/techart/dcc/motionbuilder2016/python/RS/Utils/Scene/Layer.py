'''
Usage:
    This module should only contain functionality for working with FBAnimationLayer objects
Author:
    Ross George
'''


import pyfbsdk as mobu

import RS
import RS.Core


def GetLayerList(pInputTake=None, pInputLayerNameList=None, pInputIndexRangeLower=None, pInputIndexRangeUpper=None):
    '''
    Fetches all the layers for a given take based on the parameter passed
    '''
    # Get our target take - if none is supplied we'll use the current take
    lTargetTake = pInputTake
    if(lTargetTake == None):
        lTargetTake = RS.Core.System.CurrentTake

    # Get lower range - if none is supplied we'll use the first layer index
    lIndexRangeLower = pInputIndexRangeLower
    if(lIndexRangeLower == None):
        lIndexRangeLower = 0

    # Get upper range - if none is supplied we'll use the last layer index
    lIndexRangeUpper = pInputIndexRangeUpper
    if(lIndexRangeUpper == None):
        lIndexRangeUpper = lTargetTake.GetLayerCount()

    # Define our return layer list
    lReturnLayerList = []

    # Go through each of the indexes
    for iLayerIndex in range(lIndexRangeLower , lIndexRangeUpper):

        # Get the layer
        lLayer = lTargetTake.GetLayer(iLayerIndex)

        # If we were passed a list of layer names
        if(pInputLayerNameList != None):
            # Check the layer is on the list
            if lLayer.Name in pInputLayerNameList:
                lReturnLayerList.append(lLayer)
        else:
            # Otherwise just add it
            lReturnLayerList.append(lLayer)

    # Return the list of takes
    return lReturnLayerList


def GetBaseAnimationLayer(take=None):
    """
    Get the Base Animation Layer that must be present in the scene

    kwargs:
        take (FBTake): The take to get the current layer for

    Returns:
        FBAnimationLayer for the BaseAnimation layer
    """
    baseAnimationName = "BaseAnimation"
    take = take or RS.Core.System.CurrentTake
    for idx in xrange(take.GetLayerCount()):
        layer = take.GetLayer(idx)
        if layer.Name == baseAnimationName:
            return layer
    return None


def GetActiveLayer(take=None):
    """
    Get the current active layer for the take, or the current take

    kwargs:
        take (FBTake): The take to get the current layer for

    Returns:
        FBAnimationLayer for the current active layer
    """
    take = take or RS.Core.System.CurrentTake
    return take.GetLayer(take.GetCurrentLayer())


def ResolveLayer(pInputLayerName, pInputTake=None):
    '''
    Returns the layer which matches the passed name or creates a new layer with passd name and returns it
    '''
    # Get our target take - if none is supplied we'll use the current take
    lTargetTake = pInputTake
    if(lTargetTake == None):
        lTargetTake = RS.Core.System.CurrentTake

    # Get our layer
    lLayerList = GetLayerList(lTargetTake, [ pInputLayerName ])

    if len(lLayerList) != 0 :
        return lLayerList[0]
    else:
        lReturnLayer = lTargetTake.CreateNewLayer ()
        lReturnLayer.Name = pInputLayerName
        return lReturnLayer


def SetLayerActive(pInputTake, pInputLayer):
    '''
    Sets the active layer by take and layer index
    '''
    # First check we are on the correct take
    if(RS.Core.System.CurrentTake != pInputTake):
        RS.Core.System.CurrentTake = pInputTake

    # Next set the layer
    for iLayerIndex in range(0 , pInputTake.GetLayerCount()):
        lCurrentLayer = pInputTake.GetLayer(iLayerIndex)
        if(lCurrentLayer == pInputLayer):
            pInputTake.SetCurrentLayer(iLayerIndex)
            return


def SetLayerSelected(pInputTake, pInputLayer, pExclusive=True):
    '''
    Sets the active and selected layer by take and layer index
    '''
    # First check we are on the correct take
    if(RS.Core.System.CurrentTake != pInputTake):
        RS.Core.System.CurrentTake = pInputTake

    pInputLayer.SelectLayer (True, pExclusive)


def DeleteLayersWithNoAnimation():
    '''
    Go through all the takes and all the layers and checks to see if components have any animation on a layer,
    if it doesn't it removes it.
    '''

    currentTake = RS.Globals.System.CurrentTake
    currentLayerNumber = RS.Globals.System.CurrentTake.GetCurrentLayer()

    # Iterate over takes
    for take in RS.Globals.System.Scene.Takes:

        RS.Globals.System.CurrentTake = take
        deleteList = []
        # Iterate over layers
        for layerNumber in xrange(RS.Globals.System.CurrentTake.GetLayerCount() - 1):

            # Select consecutively each layer
            RS.Globals.System.CurrentTake.GetLayer(RS.Globals.System.CurrentTake.GetLayerCount() - layerNumber - 1).SelectLayer(True, True)

            currentLayer = RS.Globals.System.CurrentTake.GetLayer(RS.Globals.System.CurrentTake.GetLayerCount() - layerNumber - 1)


            # For all components
            componentList = RS.Utils.Scene.Component.GetComponents(Namespace="",
                                                    TypeList=[ mobu.FBComponent ],
                                                    DerivedTypes=True)

            layerHasAnimation = False
            # Find if any compoents have animation on this layer
            for component in componentList:

                # Check to see if it has animatable properties
                for node in  RS.Utils.Scene.Component.GetAnimationNodeList(component):

                    if not node.FCurve:
                        continue
                    else:
                        layerHasAnimation = True
                        break

            # If it doesn't, delete it
            if not layerHasAnimation:
                deleteList.append(currentLayer)

        for item in deleteList:
            print item.Name
            item.FBDelete ()

    # Go back to the previous setting
    RS.Globals.System.CurrentTake = currentTake

    if currentLayerNumber:
        # Go back to initial layer
        RS.Globals.System.CurrentTake.SetCurrentLayer(currentLayerNumber)
