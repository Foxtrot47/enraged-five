def assignHudToCamera(camera, hud):
    """
    Assign a HUD to a camera

    args:
        camera (FBCamera): The camera
        hud (FBHUD): The Hud
    """
    camHuds = list(camera.HUDs)
    # Remove any old HUDs
    for  oldHud in camHuds:
        camera.DisconnectSrc(oldHud)
    camera.ConnectSrc(hud)
