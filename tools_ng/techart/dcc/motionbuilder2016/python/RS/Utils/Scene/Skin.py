'''
Usage:
    This module should only contain functionality related to working with skin information of an FBmodel
Author:
    Ross George
'''

from pyfbsdk import *

'''
Represents a skinning vertex weight table for a passed FBModel
'''         
class VertexWeightTable:
    def __init__( self, model ):
        self._vertexWeightDict = dict()
        
        if not model.SkeletonDeformable:
            raise Exception("Attempted to get weight table of a model that is not skeleton deformable.")
        
        mesh = model.Geometry
        for vertexIndex in range( 0, mesh.VertexCount() ):
            self._vertexWeightDict[vertexIndex] = VertexWeight( vertexIndex )
        
        skinCluster = FBCluster(model)
        if skinCluster and skinCluster.LinkGetCount():
            for linkIndex in range ( skinCluster.LinkGetCount() ): 
                skinCluster.ClusterBegin( linkIndex ) # Set the current cluster index
                linkModel = skinCluster.LinkGetModel( linkIndex )

                #Check if the model is returning None - this indicates the cluster maps to nothing
                if( linkModel == None ): 
                    continue

                for v in range ( skinCluster.VertexGetCount() ):
                    vertexIndex = skinCluster.VertexGetNumber(v) # Using the current cluster index
                    weightValue = skinCluster.VertexGetWeight(v)                

                    self._vertexWeightDict[vertexIndex]._addEntry( linkModel, weightValue  )
                    
                skinCluster.ClusterEnd() 
                
    def GetVertexWeight( self, VertexIndex ):
        return self._vertexWeightDict[VertexIndex]
    
    def GetVertexWeightListByBoneModel( self, Model ):
        vertexWeightList = []
        for key in self._vertexWeightDict:
            vertexWeight = self._vertexWeightDict[key]
            if vertexWeight.UsesModel( Model ):
                vertexWeightList.append( vertexWeight )
        
        return vertexWeightList
    
    def GetBoneModelList( self ):
        boneModelList = set()
        for key in self._vertexWeightDict:
            vertexWeight = self._vertexWeightDict[key]
            for entry in vertexWeight.EntryList:
                boneModelList.add(entry.Model)
        
        return boneModelList

'''
An entry in a vertex weight table
'''     
class VertexWeight:
    def __init__( self, VertexIndex ):
        self.VertexIndex = VertexIndex
        self.EntryList = []
        
    def _addEntry( self, WeightModel, WeightValue ):
        self.EntryList.append( VertexWeightEntry( WeightModel, WeightValue )  )
        
    def GetModelNameList( self ):
        weightModelNames = []
        for vertexWeight in self.EntryList:
            weightModelNames.append( vertexWeight.Model.Name )
        return weightModelNames
    
    def GetModelNameList( self ):
        weightModels = []
        for vertexWeight in self.EntryList:
            weightModels.append( vertexWeight.Model )
        return weightModels

    def GetStrongestEntry( self ):
        strongestEntry = None
        for entry in self.EntryList:
            if strongestEntry == None or entry.Value > strongestEntry.Value:
                strongestEntry = entry
        return strongestEntry
    
    def UsesModel( self, Model ):
        for entry in self.EntryList:
            if entry.Model == Model:
                return True
            
    def GetValueByModel( self, Model ):
        for entry in self.EntryList:
            if entry.Model == Model:
                return entry.Value        
    
        return False

'''
An entry of vertex weight
''' 
class VertexWeightEntry:
    def __init__( self, Model, Value ):
        self.Model = Model
        self.Value = Value    
