'''
Usage:
    This module should only contain functionality related to working with FBTakes.
Author:
    Ross George
'''

from pyfbsdk import *

import RS.Core
import RS.Utils.Scene.Layer
import RS.Utils.Scene.Time
import RS.Utils.Scene.Component
import RS.Globals

'''
Attempts to get a list of takes by the parameters provided (certain ranges). If no parameters are passed it returns all takes
'''
def GetTakeList( pInputTakeNameList = None, pInputIndexRangeLower = None, pInputIndexRangeUpper = None ):
    
    #Get lower range - if none is supplied we'll use the first take index
    lIndexRangeLower = pInputIndexRangeLower
    if( lIndexRangeLower == None ):
        lIndexRangeLower = 0 
    
    #Get upper range - if none is supplied we'll use the last take index
    lIndexRangeUpper = pInputIndexRangeUpper
    if( lIndexRangeUpper == None ):
        lIndexRangeUpper = len( RS.Core.System.Scene.Takes )

    #Define our return take list
    lReturnTakeList = list()
        
    #Go through each of the indexes
    for iTakeIndex in range( lIndexRangeLower , lIndexRangeUpper ):
        
        #Get the take
        lTake = RS.Core.System.Scene.Takes[ iTakeIndex ]
                
        #If we were passed a list of take names
        if( pInputTakeNameList != None ):
            #Check the take is on the list
            if lTake.Name in pInputTakeNameList: 
                lReturnTakeList.append( lTake )
        else:
            #Otherwise just add it
            lReturnTakeList.append( lTake )
    
    #Return the list of takes      
    return lReturnTakeList


def GetTakeDictionary():
    return {eachTake.Name: eachTake for eachTake in FBSystem().Scene.Takes}


'''
Attempts to get a take by name
'''
def GetTakeByName( pInputTakeName ):            
    for iTake in RS.Core.System.Scene.Takes:
        if( iTake.Name == pInputTakeName ):
            return iTake
        
    return None

'''
Attempts to set the current take by the passed name
'''
def SetCurrentTakeByName( pInputTakeName ):
    for iTake in RS.Core.System.Scene.Takes:
        if( iTake.Name == pInputTakeName ):
            RS.Core.System.CurrentTake = iTake
            return True
    
    return False

'''
Gets the time range of the animation on all objects on the passed take
'''
def GetTimeRange( pInputTake ):
    return RS.Utils.Scene.Time.Range( pInputTake.ReferenceTimeSpan.GetStart(), pInputTake.ReferenceTimeSpan.GetStop() )

'''
Gets the local time range of the animation on all objects on the passed take
'''    
def GetLocalTimeRange( pInputTake ):
    return RS.Utils.Scene.Time.Range( pInputTake.LocalTimeSpan.GetStart(), pInputTake.LocalTimeSpan.GetStop() )

'''
Copies animation on the passed Components from the passed SourceTake to 
each of the passed target takes in TargetTakeList. Currently this function 
only copies and pastes on the base layer. Could be expanded to use other layers.
'''
def CopyAnimationData( TargetComponentList, SourceTake, TargetTakeList ):
    
    #If we have been passed a single take we will turn it into a list
    if type( TargetTakeList ) != list:
        TargetTakeList = [TargetTakeList]
        
    #Create our data store object for copying and pasting
    dataStore = RS.Utils.Scene.Component.DataStore()
    for component in TargetComponentList:
        dataStore.AddComponent( component )        
        
    #Record the current take and layer so we can return state           
    initialTake = RS.Globals.System.CurrentTake
    initialLayerIndex = initialTake.GetCurrentLayer()

    #Copy data
    RS.Globals.System.CurrentTake = SourceTake
    SourceTake.SetCurrentLayer(0)
    dataStore.Copy()
    
    #Go through each of the target takes and paste data
    for targetTake in TargetTakeList:
        RS.Globals.System.CurrentTake = targetTake
        targetTake.SetCurrentLayer(0)
        dataStore.Paste()
    
    #Go back to previous take
    RS.Globals.System.CurrentTake = initialTake    
    initialTake.SetCurrentLayer( initialLayerIndex )