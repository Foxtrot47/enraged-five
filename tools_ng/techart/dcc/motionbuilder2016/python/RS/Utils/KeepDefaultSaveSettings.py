from pyfbsdk import *

def SetSaveSettingToDefaultOptions():
    
    #After every Save, Set back to the factory default.
    lconfig = FBConfigFile("@History.txt")
    if lconfig:        
        lconfig.Set( 'FbxSdkSaveOptions', 'Actor Faces', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Actor Faces Animation', 'ConsiderAnimation')        
        lconfig.Set( 'FbxSdkSaveOptions', 'Actors', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Audio', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Base Cameras', 'ConsiderSetting')
        lconfig.Set( 'FbxSdkSaveOptions', 'Bones', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Bones Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Camera Switcher', 'ConsiderSetting')
        lconfig.Set( 'FbxSdkSaveOptions', 'Cameras', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Cameras Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Character Extensions', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Character Faces', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Character Faces Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Characters', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Characters Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Consider Mute And Solo', 'Yes')
        lconfig.Set( 'FbxSdkSaveOptions', 'Constraints', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Constraints Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Copy Character Extensions', 'Yes')
        lconfig.Set( 'FbxSdkSaveOptions', 'Current Camera', 'ConsiderSetting')
        lconfig.Set( 'FbxSdkSaveOptions', 'Devices', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Devices Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'File Reference Edits', 'Yes')
        lconfig.Set( 'FbxSdkSaveOptions', 'File References', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Global Lighting', 'ConsiderSetting')
        lconfig.Set( 'FbxSdkSaveOptions', 'Groups', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'HUDs', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Ignore Conflicts', 'Yes')
        lconfig.Set( 'FbxSdkSaveOptions', 'Keying Groups', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Lights', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Lights Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Materials', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Materials Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Models', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Models Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Notes', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Notes Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Optical Data', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Physical Properties', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Physical Properties Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Poses', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Process Animation On Extensions', 'Yes')
        lconfig.Set( 'FbxSdkSaveOptions', 'Remove Constraint Reference', 'Yes')
        lconfig.Set( 'FbxSdkSaveOptions', 'RemoveEmptyLayer', 'Yes')
        lconfig.Set( 'FbxSdkSaveOptions', 'Replace Control Set', 'Yes')
        lconfig.Set( 'FbxSdkSaveOptions', "Reset the Control Rig's Hierarchy", "Yes")
        lconfig.Set( 'FbxSdkSaveOptions', "Reset the Control Rig's Rotation DOF", "Yes")
        lconfig.Set( 'FbxSdkSaveOptions', 'Retarget On Base Layer', 'Yes')
        lconfig.Set( 'FbxSdkSaveOptions', 'Scripts', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Sets', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Shaders', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Shaders Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Solvers', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Solvers Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Story', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Story Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Textures', 'SaveElement')
        lconfig.Set( 'FbxSdkSaveOptions', 'Textures Animation', 'ConsiderAnimation')
        lconfig.Set( 'FbxSdkSaveOptions', 'Transfer Method', '0')
        lconfig.Set( 'FbxSdkSaveOptions', 'Transport', 'ConsiderSetting')
        lconfig.Set( 'FbxSdkSaveOptions', 'Video', 'SaveElement')