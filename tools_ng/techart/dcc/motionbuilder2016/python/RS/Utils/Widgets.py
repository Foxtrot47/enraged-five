from pyfbsdk import *
from pyfbsdk_additions import *

import os
import webbrowser

import RS.Config

class Banner( object ):
    def __init__( self, parentLyt, lToolName, lEmailTo, lToolPath, lCc = None, lWikiLink = None ):

        self.parentLyt = parentLyt
        
        self.gToolName = lToolName
        self.gEmailTo = lEmailTo
        self.gToolPath = lToolPath
        self.gCc = lCc        
        self.gWikiLink = lWikiLink
        
        self.__height = 30
        
    @property
    def Height( self ):
        return self.__height
          
    def Create( self ):
        
        # Rockstar Logo
        titlex = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, "" ) 
        titley = FBAddRegionParam( 0, FBAttachType.kFBAttachTop, "" )
        titlew = FBAddRegionParam( 30, FBAttachType.kFBAttachNone, None )
        titleh = FBAddRegionParam( self.__height, FBAttachType.kFBAttachNone, None )
        self.parentLyt.AddRegion( "Title", "Title", titlex, titley, titlew, titleh )
        
        titleImg = FBImageContainer()
        titleImg.Filename = "{0}\\YellowRockstarLogo_30x30.jpg".format( RS.Config.Script.Path.ToolImages )
        self.parentLyt.SetControl( "Title", titleImg )
        
        # Spacer
        spacerx = FBAddRegionParam( 30, FBAttachType.kFBAttachLeft, "" ) 
        spacery = FBAddRegionParam( 0, FBAttachType.kFBAttachTop, "" )
        spacerw = FBAddRegionParam( -60, FBAttachType.kFBAttachRight, "" )
        spacerh = FBAddRegionParam( self.__height, FBAttachType.kFBAttachNone, None )
        self.parentLyt.AddRegion("Spacer","Spacer", spacerx, spacery, spacerw, spacerh )
        
        spacerImg = FBImageContainer()
        spacerImg.Filename = "{0}\\YellowSpacerBanner.jpg".format( RS.Config.Script.Path.ToolImages )
        self.parentLyt.SetControl( "Spacer", spacerImg )   
        
        # Send an email about the tool logo
        lEmailX = FBAddRegionParam( -60, FBAttachType.kFBAttachRight, "" )
        lEmailY = FBAddRegionParam( 0, FBAttachType.kFBAttachTop, "")
        lEmailW = FBAddRegionParam( 30, FBAttachType.kFBAttachNone, None )
        lEmailH = FBAddRegionParam( self.__height, FBAttachType.kFBAttachNone, None )
        self.parentLyt.AddRegion( "lEmail","lEmail", lEmailX, lEmailY, lEmailW, lEmailH )
    
        lEmailContainer = FBVisualContainer()
        self.parentLyt.SetControl( "lEmail", lEmailContainer )
        lEmailContainer.OnChange.Add( self.OnEmail_Clicked )   
        
        lEmailImage = "{0}\\email_icon_yellow.tif".format( RS.Config.Script.Path.ToolImages )
        lEmailContainer.Items.append("")
        lEmailContainer.ItemIconSet( 0, lEmailImage )
        lEmailContainer.ItemWidth = 27
        lEmailContainer.ItemHeight = 44
        lEmailContainer.ItemWrap = True      
        
        # Find more information on the wiki about the tool logo
        lInfoX = FBAddRegionParam( -30, FBAttachType.kFBAttachRight, "" )
        lInfoY = FBAddRegionParam( 0, FBAttachType.kFBAttachTop, "" )
        lInfoW = FBAddRegionParam( 30, FBAttachType.kFBAttachNone, None )
        lInfoH = FBAddRegionParam( self.__height, FBAttachType.kFBAttachNone, None )
        self.parentLyt.AddRegion( "lInfo","lInfo", lInfoX, lInfoY, lInfoW, lInfoH )
        
        lInfoContainer = FBVisualContainer()
        self.parentLyt.SetControl( "lInfo", lInfoContainer )
        lInfoContainer.OnChange.Add( self.OnInfo_Clicked )   
    
        lInfoImage = "{0}\\information_icon_yellow.tif".format( RS.Config.Script.Path.ToolImages )
        lInfoContainer.Items.append("")
        lInfoContainer.ItemIconSet(0, lInfoImage)
        lInfoContainer.ItemWidth = 27
        lInfoContainer.ItemHeight = 44
        lInfoContainer.ItemWrap = True
        
        return self.parentLyt
    
    ## Event Handlers ##
    
    def OnEmail_Clicked( self, pControl, pEvent ): 
        if self.gCc == None:
            os.startfile( "mailto:" + self.gEmailTo + "&Subject= " + self.gToolName + "&Body=" + self.gToolPath )
            
        else:
            os.startfile( "mailto:" + self.gEmailTo + "?CC=" + self.gCc + "&Subject= " + self.gToolName + "&Body=" + self.gToolPath )
                    
    def OnInfo_Clicked( self, pControl, pEvent ):
        if self.gWikiLink == None:
            FBMessageBox( "Information", "Sorry, there is no Wiki page for this UI...YET :)", "Ok" )
            
        else:
            lNew = 2
            webbrowser.open( self.gWikiLink )    


class RsChoiceDialog( FBPopup ):
    '''
    Description:
        Pops up a modal dialog of choices for the user to pick from.
    
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
        
    Example: ::
        
        # None is returned if the user clicked Cancel.
        result = RsChoiceDialog( 'Title', [ 1, 2, 3 ] )
        
        if result.value:
            # Do something.
    '''
    
    def __init__( self, title, choices = [], size = [ 180, 86 ], showCancelButton = True ):
        FBPopup.__init__( self )
        self.Caption = title
        self.Width = size[ 0 ]
        self.Height = size[ 1 ]
        
        # Position dialog in the middle of the screen.
        screenWidth, screenHeight, huh = FBSystem().DesktopSize.GetList()
        
        self.Top = abs( int( ( screenHeight / 2.0 ) - ( self.Height / 2.0 ) ) )
        self.Left = abs( int( ( screenWidth / 2.0 ) - ( self.Width / 2.0 ) ) )
        
        # Setup layout
        mainLyt = FBVBoxLayout()
        
        x = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( 10, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( -10, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( -10, FBAttachType.kFBAttachBottom, '' )
        
        self.AddRegion( 'main', 'main', x, y, w, h )
        self.SetControl( 'main', mainLyt )
        
        # Ok / Cancel Buttons
        okCancelLyt = FBGridLayout()
        
        okButton = FBButton()
        okButton.Caption = 'OK'
        okButton.OnClick.Add( self._onOk_Clicked )
        
        cancelButton = FBButton()
        cancelButton.Caption = 'Cancel'
        cancelButton.OnClick.Add( self._onCancel_Clicked )
        
        okCancelLyt.Add( okButton, 0, 0, width = 72 )
        if showCancelButton:
            okCancelLyt.Add( cancelButton, 0, 1, width = 72 )
        
        self.options = FBList()
        self.options.Style = FBListStyle.kFBDropDownList
        
        for item in choices:
            self.options.Items.append( str( item ) )
        
        mainLyt.Add( self.options, 24 )
        mainLyt.Add( okCancelLyt, 32 )
        
        self.value = None
        
        self.Show()
        
    def _onCancel_Clicked( self, control, event ):
        self.value = None
        self.Close( True )
        
    def _onOk_Clicked( self, control, event ):
        self.value = self.options.Items[ self.options.ItemIndex ]
        self.Close( True )
        
class RsVerticalListDialog( FBPopup ):
    '''
    Description:
        Pops up a modal vertical list dialog.
    
    Author:
        Kristine Middlemiss <kristine.middlemiss@rockstartoronto.com>
        
    Example: ::
        
        # None is returned if the user clicked Cancel.
        result = RsVerticalListDialog( 'Title', [ 1, 2, 3 ] )
        
        if result.value:
            # Do something.
    '''
    
    def __init__( self, title, choices = [], size = [ 200, 125 ], multiChoice = False ):
        FBPopup.__init__( self )
        
        self.Caption = title
        self.Width = size[ 0 ]
        self.Height = size[ 1 ]
        
        # Position dialog in the middle of the screen.
        screenWidth, screenHeight, huh = FBSystem().DesktopSize.GetList()
        self.Top = int( ( screenHeight / 2.0 ) - ( self.Height / 2.0 ) )
        self.Left = int( ( screenWidth / 2.0 ) - ( self.Width / 2.0 ) )
        
        # Setup layout
        mainLyt = FBVBoxLayout()
        
        x = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( 10, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( -10, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( -10, FBAttachType.kFBAttachBottom, '' )
        
        self.AddRegion( 'main', 'main', x, y, w, h )
        self.SetControl( 'main', mainLyt )

        # List
        self.options = FBList()
        self.options.Style = FBListStyle.kFBVerticalList
        self.options.MultiSelect = multiChoice
        
        for item in choices:
            self.options.Items.append( str( item ) )
            
        
        # OK / Cancel Buttons
        okCancelLyt = FBHBoxLayout( FBAttachType.kFBAttachRight )
        
        okButton = FBButton()
        okButton.Caption = 'OK'
        okButton.OnClick.Add( self._onOk_Clicked )
        
        cancelButton = FBButton()
        cancelButton.Caption = 'Cancel'       
        cancelButton.OnClick.Add( self._onCancel_Clicked )
        
        okCancelLyt.Add( cancelButton, 50 )        
        okCancelLyt.Add( okButton, 50 )
        
        mainLyt.AddRelative( self.options )
        mainLyt.Add( okCancelLyt, 24 )
        
        self.value = None        
        self.Show()
        
    def _onCancel_Clicked( self, control, event ):
        self.value = None
        self.Close( True )
        
    def _onOk_Clicked( self, control, event ):
        values = []
        
        itemIndex = 0
        
        for item in self.options.Items:
            if self.options.IsSelected( itemIndex ):
                values.append( self.options.Items[ itemIndex ] )
            
            itemIndex += 1
            
        if len( values ) == 1:
            self.value = values[ 0 ]
            
        elif len( values ) > 1:
            self.value = values
            
        else:
            self.value = None
            
        self.Close( True )
