'''
Writes the active document to a specific file that MotionBuilder is watching.  Based on the document type being active,
it will cause the script to be executed in MotionBuilder, or the module to be reloaded.  Currently only supported with
MotionBuilder 2014 and higher.

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
'''
import wingapi
import os


def ExecuteActiveScript( app = wingapi.kArgApplication ):
  '''
  Bind this function to a keyboard shortcut, using the function name as the command.
  '''
  editor = app.GetActiveEditor()
  filename = editor.GetDocument().GetFilename()
  
  mobuRunRemoteScriptFile = 'x:\\mobuRunRemoteScript.txt'
  
  # Determine if the script we are executing is a package level module, or script.
  scriptType = '-script'
  
  if 'motionbuilder2014\\python\\RS\\' in filename:
    scriptType = '-reload'
    
    # Construct module import path.
    rawImport = filename[ filename.index( 'RS\\' ): ]
    rawImport = rawImport.rstrip( '.py' )
    filename = rawImport.replace( '\\', '.' )
  
  f = open( mobuRunRemoteScriptFile, 'w' )
  f.write( '{0};{1}'.format( scriptType, filename ) )
  f.close()