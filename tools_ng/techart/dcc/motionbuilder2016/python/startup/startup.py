"""
Module for setting up a motionbuilder session.
"""

import os
import imp
import inspect
import logging
import traceback
from logging import config

import pyfbsdk as mobu

import pyfbsdk_init


# Setup Logging Config
STARTUP_DIRECTORY = os.path.dirname(inspect.getfile(inspect.currentframe()).replace("\\", "/"))
DEFAULT_CONFIG = os.path.join(STARTUP_DIRECTORY, 'logging.conf')
CONFIG = os.path.join(STARTUP_DIRECTORY, 'startup.conf')

with open(DEFAULT_CONFIG, "r") as configHandle:
    configContents = configHandle.read()
with open(CONFIG, "w") as configHandle:
    configHandle.write(configContents.format(logpath=os.path.join(STARTUP_DIRECTORY, "startup.log")))

config.fileConfig(CONFIG)

logging.getLogger(__name__)


class Startup(object):
    """
    Because of the order that motion builder calls scripts we must use a
    decorator (this class) to override the default order. This way 
    we guarantee that the Pyfbsdk libraries will be initialized first 
    then the R* tools. Which in the case of the 'clr' module is important.
    """

    def __init__(self, targetStartPyfbsdk):
        """
        Constructor

        Arguments:
            targetStartPyfbsdk: motion builder session

        """
        self._targetStartPyfbsdk = targetStartPyfbsdk

    def __call__(self, *args):
        """
        This will get called via pyfbsdk_init.StartPyfbsdk within 
        the Motion builder normal startup logic
        """

        # Check if we have already ran this so we don't run it twice...
        if not hasattr(self, '_initialized'):
            try:
                logging.info("------------------------------------")
                logging.info("Initializing Motionbuilder Python SDK")
                logging.info("------------------------------------")

                self._targetStartPyfbsdk(*args)
                
                logging.info("Initializing Rockstar Tools")
                logging.info("------------------------------------")
                self._runStartupScripts()
                self._cleanup = False

                import RS
                RS.MOBU_IS_STARTING = False   
                
            except Exception:
                mobu.FBMessageBox("Error",
                                  ("A problem occured during startup and the tools have not been setup"
                                   " fully/correctly.\n\nError: {0} \n\nContact a member of the TA department"
                                   " for assistance.").format(traceback.format_exc()), "OK")
                logging.error(traceback.format_exc())

            # Mark that we have completed the initialization process
            self._initialized = True                         

    def _runStartupScripts(self):
        """ Runs scripts found in the <Project>\techart\dcc\motionbuilder20XX\python\startup\scripts folder """

        # We sort the list of directories returned so _Standalone is found before _UI
        for name in sorted(os.listdir(STARTUP_DIRECTORY)):
            if not name.startswith("_"):
                continue

            module = imp.load_source(name, os.path.join(STARTUP_DIRECTORY, name, "Setup.py"))
            module.Execute()


# Override the current StartPyfbsdk with the Startup decorator.
pyfbsdk_init.StartPyfbsdk = Startup(pyfbsdk_init.StartPyfbsdk)

# Call StartPyfbsdk immediately since motion builder won't do so until the python editor is opened otherwise
pUserConfigPath = pyfbsdk_init.MB_USER_CONFIG_PATH
pExePath = mobu.FBSystem().CurrentDirectory() + "\\bin\\x64\\"
pyfbsdk_init.StartPyfbsdk(pUserConfigPath, pExePath)