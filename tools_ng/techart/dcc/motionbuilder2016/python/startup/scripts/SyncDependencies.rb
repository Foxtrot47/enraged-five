#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 15 September 2013 (AP3)
# Purpose:
#			~ Sync and Log MB dependencies

require 'RSG.Base'
require 'RSG.Base.Configuration'
require 'RSG.SourceControl.Perforce'
require 'RSG.Editor.Controls'
require 'System'
require 'PresentationFramework'



# Core
include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal
include RSG::Editor::Controls

include System::Diagnostics
include System::Windows
include System::Diagnostics


#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------

SILENT = false


# Grab the MB version for the source script name using REG ex
MB_VERSION =  File.dirname(__FILE__).split(/motionbuilder([0-9+]+)/)[1]




# Will check if user has latest plugins and expression, if not it will close mb, sync and restart
def checkPlugins()

  pluginsPath = [@g_Config.project.config.tools_root + "/dcc/current/motionbuilder#{MB_VERSION}/plugins/x64/3rdParty/...", 
                @g_Config.project.config.tools_root + "/dcc/current/motionbuilder#{MB_VERSION}/plugins/x64/expressions/..."]
                
  # Using the -n parameter we can see what files would need to be sync'd to.
  # If no records are found then we know we have latest  
  recordset = @p4.Run('sync', true, System::Array[System::String].new( ["-n"].concat(pluginsPath)))
  
  updatedFiles = []
  
  # Only check for added or updated files
  recordset.records.each do | record |
    if record['action'] == 'updated' or record['action'] == 'added'
      puts record['depotFile']
      updatedFiles << record
    end
  end
  if updatedFiles.count > 0 then  
      # Lets get the CM main window handle, its difficult to get MB as need to get child spashwindow.
    myHandle = nil
     System::Diagnostics::Process.GetProcessesByName("cmd").each do | pr |
        # Tricky one to determine whihc command is run but fo rnow will use one that is not 0
        #puts pr.MainWindowHandle
        myHandle = pr.MainWindowHandle if pr.MainWindowHandle != 0 
      end
    result = RsMessageBox.Show(myHandle, "Warning: !!!! Detected #{updatedFiles.count} Facial motionbuilder plug-in(s) that are out of date!!!!\n\nPress OK to sync latest plug-ins and restart motionbuilder\n\n(This will close ALL instances of motionbuilder)", "Motionbuilder #{MB_VERSION}", 1, 48) if SILENT == false
    
  
     # 64 = Informnation
     # 48 = Warning
     # 16 = Stop

    if result == System::Windows::MessageBoxResult.OK or SILENT == true  
      # DEFAULT MB Path
      MB_EXE = "C:/Program Files/Autodesk/MotionBuilder #{MB_VERSION}/bin/x64/motionbuilder.exe"
    
      # go through each MB process we find, get the path name and then kill it
      System::Diagnostics::Process.GetProcessesByName("motionbuilder").each do | pr |
        MB_EXE = pr.MainModule.FileName
        pr.kill()
      end

      @p4.Run('sync', true, System::Array[System::String].new( ["-f"].concat(pluginsPath)))
      
      @taskkillProcess.StartInfo.FileName = MB_EXE
      @taskkillProcess.StartInfo.Arguments = ""
      @taskkillProcess.Start()
      exit
    end
  end

end

    
if ( __FILE__ == $0 ) then
    
    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
        g_Log = LogFactory.ApplicationLog

	#Killing the UFC exes
	@taskkillProcess = System::Diagnostics::Process.new
	@taskkillProcess.StartInfo.FileName = "TASKKILL"
	
	g_Log.message('Killing UFCWorker...')
	@taskkillProcess.StartInfo.Arguments = "/IM UFCWorker.exe /F"
  @taskkillProcess.Start()
	@taskkillProcess.WaitForExit()
	
	g_Log.message('Killing UFCServer...')
	@taskkillProcess.StartInfo.Arguments = "/IM UFCServer.exe /F"
  @taskkillProcess.Start()
	@taskkillProcess.WaitForExit()

    @g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( ) 
	 
	if @g_Config.project.name == 'gta5' then
		@p4 = @g_Config.Project.SCMConnect()
	else
		@p4 = @g_Config.Project.SCMConnect(g_Log)
	end
    
  
    checkPlugins()
  
	
    g_Log.message('Syncing critical dependencies...')
    criticalSyncPaths = [ "-f", @g_Config.project.DefaultBranch.Assets + "/metadata/definitions/techart/motionbuilder...",
								@g_Config.project.DefaultBranch.Assets + "/metadata/definitions/rage/suite/tools/cli/cutftools/...",
                @g_Config.project.DefaultBranch.Assets + "/metadata/definitions/rage/suite/src/cutscenemeta/...",
								@g_Config.project.config.tools_root + "/techart/bin/ManagedSync/...",                
                @g_Config.project.config.tools_root + "bin/anim/UFC/..."]
    @p4.Run( 'sync', true, System::Array[System::String].new(criticalSyncPaths) )
	
	g_Log.message('Syncing asset dependencies...')
	syncProcess = System::Diagnostics::Process.new
	syncProcess.StartInfo.FileName = @g_Config.ToolsRoot + "\\techart\\bin\\ManagedSync\\ManagedSync.exe"
	syncProcess.StartInfo.Arguments = @g_Config.project.config.tools_root + "/techart/dcc/motionbuilder#{MB_VERSION}/... "
	syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/dcc/current/motionbuilder#{MB_VERSION}/assemblies/x64/... "
	syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/techart/sandboxmotionbuilder/... "
  syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/techart/etc/EST/... "
	syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/techart/etc/config/camera/... "
	syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/techart/etc/config/motionbuilder/... "
	syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/techart/etc/config/database/... "
	syncProcess.StartInfo.Arguments += @g_Config.project.Root + "/assets/anim/expressions/... " #Hack lines which we will eventually want to remove
	syncProcess.StartInfo.Arguments += @g_Config.project.Root + "/assets_ng/anim/expressions/... " #Should use g_Config.project.DefaultBranch.Assets instead.
  syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/etc/config/anim/... "
  syncProcess.StartInfo.Arguments += @g_Config.project.Root + "/assets/anim/skeletons/... "
  syncProcess.StartInfo.Arguments += @g_Config.project.DefaultBranch.Assets + "/metadata/characters/skeleton/... "
	syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/etc/config/cutscene/... "
  syncProcess.StartInfo.Arguments += @g_Config.ToolsRoot + "/techart/etc/config/notificationTemplates/... "
  syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/bin/anim/UFC/... "
	syncProcess.StartInfo.Arguments += @g_Config.ToolsRoot + "/techart/bin/... "
	syncProcess.StartInfo.Arguments += "//wildwest/bin/... "
	syncProcess.StartInfo.Arguments += "//wildwest/dcc/motionbuilder#{MB_VERSION}/... "
	syncProcess.StartInfo.Arguments += "//wildwest/etc/config/motionbuilder/... "
	syncProcess.StartInfo.Arguments += @g_Config.project.config.tools_root + "/techart/etc/config/facestar/..."
	syncProcess.Start()
	syncProcess.WaitForExit()
	g_Log.message('Finished syncing...')
  sleep(0.7)

end