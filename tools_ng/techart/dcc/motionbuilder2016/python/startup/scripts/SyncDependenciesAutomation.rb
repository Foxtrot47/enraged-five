#
#
# Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
# Date:: 15 September 2013 (AP3)
# Purpose:
#			~ Sync dependencies for automation task

require 'RSG.Base'
require 'RSG.Base.Configuration'
require 'RSG.SourceControl.Perforce'

include RSG::Base::Configuration
include RSG::Base::Logging
include RSG::Base::Logging::Universal

# Grab the MB version for the source script name using REG ex
MB_VERSION =  File.dirname(__FILE__).split(/motionbuilder([0-9+]+)/)[1]

if ( __FILE__ == $0 ) then
    LogFactory.Initialize()
    LogFactory.CreateApplicationConsoleLogTarget( )
        g_Log = LogFactory.ApplicationLog
        
    @g_Config = RSG::Base::Configuration::ConfigFactory::CreateConfig( )  
    @p4 = @g_Config.Project.SCMConnect()
    
    # Kill any UFC tools so we can sync 
    @taskkillProcess = System::Diagnostics::Process.new
    @taskkillProcess.StartInfo.FileName = "TASKKILL"
    
    g_Log.message('Killing UFCWorker...')
    @taskkillProcess.StartInfo.Arguments = "/IM UFCWorker.exe /F"
    @taskkillProcess.Start()
    @taskkillProcess.WaitForExit()
    
    g_Log.message('Killing UFCServer...')
    @taskkillProcess.StartInfo.Arguments = "/IM UFCServer.exe /F"
    @taskkillProcess.Start()
    @taskkillProcess.WaitForExit()
    
    
    pluginsPath = [ @g_Config.project.config.tools_root + "/bin/anim/UFC/...",
                    @g_Config.project.config.tools_root + "/dcc/current/motionbuilder#{MB_VERSION}/plugins/x64/3rdParty/...", 
                    @g_Config.project.config.tools_root + "/dcc/current/motionbuilder#{MB_VERSION}/plugins/x64/expressions/..."]
                
    @p4.Run('sync', true, System::Array[System::String].new( pluginsPath))             
    
    
end


