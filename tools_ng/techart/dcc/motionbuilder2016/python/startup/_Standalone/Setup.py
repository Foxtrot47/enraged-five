"""
This module contains logic that should be ran at startup but is independent of the Motion Builder UI
"""
import re
import os
import sys
import imp
import site
import stat
import time
import inspect
import subprocess
from functools import wraps
import pyfbsdk as mobu


import logging
log = logging.getLogger(__name__)

CHARACTER_SOLVER = "MB Character Solver"
DEBUG = False


def ExecuteTime(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        if DEBUG:
            log.info("{} : Execution Time for {} ***\n".format(time.time()-start, func.__name__))
        return result
    return wrapper


@ExecuteTime
def Execute():
    """ Executes all the methods that should be ran at Startup """
    _removeLocalPythonPackages()
    #_checkForPowerShell()
    _setupCLR()
    _setupPaths()
    _initializeConfig()
    _removeLocalCompiledFiles()
    _setDefaultSettings()
    _setupWing()
    _startUFC()
    _removeDeveloperPaths()
    _addCallbacks()


@ExecuteTime
def _addCallbacks():
    """ Adds Callbacks """
    log.info('Adding callbacks')
    callbackPath = os.path.join(os.path.dirname(__file__), "Callbacks.py")
    if not os.path.exists(callbackPath):
        from RS import Perforce
        Perforce.Sync(callbackPath, force=True)
    module = imp.load_source("Callbacks", callbackPath)
    module.Add()


@ExecuteTime
def _removeLocalPythonPackages():
    """Ensure that we don't have any local C:\Python26 paths in sys.path."""
    log.info('Removing local and global Python packages.')

    # If it's not a motionbuilder sourced path then remove it
    for path in list(sys.path):
        if 'motionbuilder' not in path.lower():
            sys.path.remove(path)
            log.info( '\t{0}'.format(path))


@ExecuteTime
def _checkForPowerShell():
    """ Check that PowerShell is installed on the machine as it is required for our tools """
    log.info('Checking that PowerShell is installed on the computer.')
    error = None
    if subprocess.call(r'REG QUERY HKLM\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine',
                       stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                       shell=True):
        log.warning('PowerShell 4.0 was not found on this computer.')
        error = "PowerShell 4.0 was not found on this machine"

    else:
        versionString = subprocess.check_output(
            r'REG QUERY HKLM\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine /v PowerShellVersion',
            stdin=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).strip()
        version = re.split('[ ]+', versionString)[-1]
        if version != "4.0":
            log.warning('PowerShell 4.0 is not installed on this computer.')
            error = "The incorrect version of PowerShell is installed on this machine"
    if error:
        mobu.FBMessageBox("Tools Setup Error",
                          "{}\n"
                          "Contact IT to install the latest version of PowerShell 4.0 and reinstall  Tools".format(error),
                          "Ok")


@ExecuteTime
def _setupCLR():
    """Adds the assembly path to the sys.path and then imports CLR. Checks that this happened correctly."""
    log.info('Setting up CLR and assemblies')

    assemblyPath = None
    if 'MOTIONBUILDER_ASSEMBLY_PATH' in os.environ.keys():
        assemblyPath = os.environ['MOTIONBUILDER_ASSEMBLY_PATH']
    else:
        # Add in the path where our CLR assemblies and module can be found.
        applicationConfigFile = mobu.FBConfigFile("@Application.txt")
        if applicationConfigFile is not None:
            assemblyPath = applicationConfigFile.Get("Rockstar", "AssemblyPath")
    
    if assemblyPath is not None and os.path.exists(assemblyPath):
        sys.path.append(assemblyPath)
        try:
            import clr
        except:
            raise Exception("Failed to load CLR module. This is a critical component. "
                            "Try to reboot MoBu to resolve this issue. If it persists contact "
                            "a Technical Artist/Animator.")              
        
        # Loop through the assemblies and load them
        for dllFilePath in os.listdir(assemblyPath):
            if not dllFilePath.endswith(".dll"):
                continue
            try:
                log.info("\t{0}".format(dllFilePath))
                clr.AddReference( os.path.splitext(dllFilePath)[0] )
            except:
                raise Exception("Failed to load the .dll: {0}".format(dllFilePath))
                
    
    else:
        raise Exception("Failed to resolve the assembly path. "
                        "Try and reinstall/resync tools to resolve this.")


@ExecuteTime
def _initializeConfig():
    """ Attempts to initialize the log factory - needed on newer assemblies. This can be phased out eventually. """
    log.info('Initializing config.')
    import RS.Config


@ExecuteTime
def _setupPaths():
    """ Sets up our Python site packages, and then adds any additional Python paths."""
    log.info('Setting up core paths')
    # Get the current filename.
    currentFilename = inspect.getfile(inspect.currentframe())
    currentDirectory = os.path.dirname(currentFilename)

    # Add the plugin directories as path to source modules from as well - we will now grab the
    # pythoninjectioninteractionmode.pyd from here.
    sys.path += mobu.FBSystem().GetPluginPath()
    
    pythonPackageDir = os.path.abspath('{0}/../../../python'.format(currentDirectory))
    pythonExternalPackageDir = os.path.abspath('{0}/../../../python/external'.format(currentDirectory))

    # Setup our root Python package directory.
    if not os.path.isdir(pythonPackageDir):
        raise Exception('You are missing the following directory: {0}.'.format(pythonPackageDir))
    else:
        site.addsitedir(pythonPackageDir)

    # Setup our external Python packages.
    if not os.path.isdir(pythonExternalPackageDir):
        raise Exception('You are missing the following directoy: {0}.'.format(pythonExternalPackageDir))
    else:
        site.addsitedir(pythonExternalPackageDir)


@ExecuteTime
def _removeLocalCompiledFiles():
    """Removes any old compiled python files"""
    import sys
    sys.dont_write_bytecode = True

    import RS.Config
    import RS.Utils.Path

    compiledFileList = RS.Utils.Path.Walk(RS.Config.Script.Path.RockstarRoot, pRecurse=True, pPattern='*.pyc',
                                          pReturn_folders=False)

    if compiledFileList:
        log.info('Removing previously compiled python files.')

        for compiledFile in compiledFileList:
            try:
                    os.chmod(compiledFile, stat.S_IWRITE)
                    os.remove(compiledFile)

                    log.info(' \t {0}'.format(compiledFile))

            except OSError:
                log.warning('Could not remove redundant compiled file ({0})!'.format(compiledFile))


@ExecuteTime
def _setDefaultSettings():
    """ Sets the default settings for Motion Builder """
    log.info('Applying default start settings.')

    # url:bugstar:2601384 - Embed Medias Turned Off on Start-Up
    from RS import Globals
    from RS.Utils import UserPreferences

    applicationConfigFile = mobu.FBConfigFile("@Application.txt")

    if applicationConfigFile:
        embedMediaValue = UserPreferences.__Readini__("menu", "Embed Media Overload", True, notFoundValue=True)
        embedMediaString = 'No' if embedMediaValue else 'Yes'
        applicationConfigFile.Set("SaveLoad", "EmbedMedias", embedMediaString)
        applicationConfigFile.Set("SaveLoad", "EmbedMediasSimpleSave", embedMediaString)

        # Set Character Solver to be MB by default in application.txt url:bugstar:1860765

        applicationConfigFile.Set("Character", "DefaultCharacterSolver", CHARACTER_SOLVER)

        # Set the undo if it's equal to 10, as this is way too low, if it's anything other then 10,
        # it will leave it the same.

        lUndoValue = applicationConfigFile.Get("Undo", "UndoLevel ")
        if lUndoValue == "10":
            applicationConfigFile.Set("Undo", "UndoLevel", "200")

    else:
        log.warning( 'Cannot find config file: "@Application.txt"')

    # The Story Mode is on by default in MotionBuilder, Animators would like it off by default.
    Globals.Story.Mute = True

    # set 'parallel evaluation' to be On - a fix to allow the relation constraint values to always draw
    # manual fix window > profiling center > turn on parallel evaluation
    # We actually now force this off because it fucks up auto-rotation on car wheels
    # (oh Motion builder how we love you so)
    Globals.EvaluateManager.ParallelEvaluation = False


@ExecuteTime
def _setupWing():
    """ Setups WingIde debug integration with Mobu """
    log.info('Setting up Wing IDE preferences.')

    from RS import Globals
    from RS.Tools.UI import Application
    from RS.Utils import Wing

    # Setup Wing IDE.
    _, errorMessage = Wing.SetupWingIDE()
    # if errorMessage:
    if errorMessage:
        Application.BringMainWindowToFront(force=True)
        mobu.FBMessageBox("Rockstar", errorMessage, "Ok")

    # Create/wipe our wing file to watch.
    with open(Globals.WingRemoteWatchFile, 'w') as watchFile:
        watchFile.write('')


@ExecuteTime
def _startUFC():
    """  Setups the background processes for running the facial animation in Motion Builder """
    log.info('Starting UFC')

    from RS.Tools.UI import Commands
    Commands.StartUFC()


@ExecuteTime
def _removeDeveloperPaths():
    """
    This is here simply to remove any global wildwest depot paths from sys.path if we are running from the project.
    For example, this deals with the situation where I (Hayes) have got a .pth file in my
    C:\Python27\lib\site-packages folder that points to the global WW depot Python tools
    (x:\wildwest\dcc\motionbuilder2014\python), so that it see's our RS package so that WingIDE can pick up on
    the intellisense.  I'm so selfish.
    """

    currentFilename = os.path.abspath(inspect.getfile(inspect.currentframe())).lower()
    developerMode = 'x:\\wildwest' in currentFilename

    if not developerMode:
        pathsToRemove = []

        if pathsToRemove:
            log.info('Removing development paths.')

        for path in list(sys.path):
            if path.startswith('x:\\wildwest'):
                sys.path.remove(path)
                log.info('\t{0}'.format(path))
