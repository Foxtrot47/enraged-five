"""
Callbacks to add to Motion Builder at startup that do not affect the UI
"""
import logging
from functools import partial

from PySide import QtGui
import pyfbsdk as mobu

from RS import Globals
from RS.Utils import Wing, Process, UserPreferences
from RS.Core.Face import Lib as FaceLib
from RS.Core.Audio import AudioTools
from RS.Core.Server import Client

logging.getLogger(__name__)


def Add():
    """Adds callbacks to Motion Builder """
    # Add file to monitor
    Globals.FileMonitor.AddFileToMonitor(Globals.WingRemoteWatchFile,
                                     mobu.FBFileMonitoringType.kFBFileMonitoring_MAINSCENE)

    # Setup callbacks
    Globals.Callbacks.OnFileOpenCompleted.Add(OnFileOpenCompleted)
    Globals.Callbacks.OnFileNewCompleted.Add(OnFileNewCompleted)
    Globals.Callbacks.OnFileExit.Add(OnFileExit)
    Globals.Callbacks.OnFileChangeMainScene.Add(OnFileChangeMainScene)

    # Miscellaneous Code Here
    # Audio Flipper Callback - if option enabled in tools preferences
    audioSettings = UserPreferences.__Readini__("audioTools", "audioDest", notFoundValue=None)
    AudioTools.RegisterAutoAudio(audioSettings)


def OnFileOpenCompleted(control, event):
    """
    Custom method attached to the Mobu onFileCompleted Callback

    Arguments:
        control: Motion Builder object that is calling this method
        event (pyfbsdk.FBEvent)= Motion Builder event; unused
    """
    # The 2D Magnifier option on cameras is accidentally getting enabled by users, and it is causing a lot of havoc,
    # this will check all camera's on open and make sure it's off.
    # MB Should always default turn off 2D magnifier on all cameras when file is opened

    [setattr(camera, "Use2DMagnifier", False) for camera in Globals.Cameras if camera.Use2DMagnifier]

    # The Story Mode is on by default in MotionBuilder, Animators would like it off by default.
    Globals.Story.Mute = True

    # Load Cutscene Audio - if option enabled in tools preferences
    if UserPreferences.__Readini__("audioTools", "audioDest", notFoundValue=0):
        AudioTools.AutoLoadAudio()

    # Facial Jaw Break Fix
    FaceLib.FixFaceInCutscene(True)
    # If I don't call this evaluation, the first file open after a MotionBuilder restart, won't have the jaws fixed.
    Globals.Scene.Evaluate()


def OnFileNewCompleted(control, event):
    """
    When a new scene is finished loading, this method is called.

    Arguments:
        source (object): Motion Builder object calling the event
        event (FBEvent): the event being called
    """
    Globals.Story.Mute = True


def OnFileChangeMainScene(control, event):
    """
    When a file that is being is updated, this method is called.

    Arguments:
        source (object): Motion Builder object calling the event
        event (FBEvent): the event being called
    """
    Wing.OnWatchRunRemoteScriptFileChanged()


def OnFileExit(control, source):
    """
    When Motion Builder is closed, this method is called

    Arguments:
        source (object): Motion Builder object calling the event
        event (FBEvent): the event being called
    """

    # Kills any UFC*.exe processes currently running when MB closes.
    logging.info("Close - Killing UFC Server")
    Process.KillAll('UFCServer.exe')
    logging.info("Close - Killing UFC Worker")
    Process.KillAll('UFCWorker.exe')

    # Kills any External Python process running as a server when MB Closes
    logging.info("Close - Closing External Python Executables")
    Client.CloseAll()

    # remove exception hook on exit http://bs/@bug/7133260
    try:
        from RS.Utils import Exception
        Exception.ExceptionHook.deregister()
        logging.info("Close - Exception hook")
    except BaseException as ex:
        logging.warning("Failed to deregister exception hook: {}".format(ex))

    RemoveAllCallbacks()


def RemoveAllCallbacks(remove=False):
    """
    Removes all non-OnFileExit callbacks attached to Motion Builder when Motion Builder is closed

    Arguments:
        remove (boolean): remove all callbacks, when false it adds this method to the OnFileExit callback to make
                          it activate last
    """

    if remove is True:
            # Total horseshit

            # Remove all callbacks
            # Even though Motion Builder returns Singletons of the FBScene and FBApplication class, they may not all
            # share the same list of registered callbacks, so we are going to go through the instances caught by the
            # garbage collection to clear as many as we can
            logging.info("Close - Removing All Callbacks")
            Globals.Callbacks.RemoveAll(skipEvents=[Globals.Callbacks.OnFileExit.eventtype])
            logging.info("Close - Callbacks Removed")

    else:
        logging.info("Close - Add RemoveAllCallback to QApplication.lastWindowClosed event so it runs last")
        # This method is added to the FileOnExit callback so it gets executed last
        QtGui.QApplication.instance().lastWindowClosed.connect(partial(RemoveAllCallbacks, remove=True))
