"""
Callbacks to add to Motion Builder at startup that do affect the UI
"""
import os
import logging

from PySide import QtGui, QtCore

from RS import Config
from RS import Globals
from RS.Utils import Scene
from RS.Utils import UserPreferences
from RS.Utils import KeepDefaultSaveSettings
from RS.Core.Camera import CameraLocker
from RS.Core.Camera import CamValidationTools
from RS.Utils.Logging import Database
from RS.Utils import AutoRunProcess
from RS.Tools.CameraToolBox.PyCoreQt.Dialogs import popupWidget

logging.getLogger(__name__)


def Add():
    """ Adds callbacks to Motion Builder """
    # Add Callbacks
    Globals.Callbacks.OnFileOpenCompleted.Add(OnFileOpenCompleted)
    Globals.Callbacks.OnFileNewCompleted.Add(OnFileNewCompleted)
    Globals.Callbacks.OnFileSave.Add(OnFileSave)
    Globals.Callbacks.OnFileSaveCompleted.Add(OnFileSaveCompleted)
    Globals.Callbacks.OnFileExit.Add(OnFileExit)

    # Miscellaneous Code Here
    # Extra Save Setting - to catch the first time setup, as this doesn't take affect until after the first save.
    if UserPreferences.__Readini__("menu", "KeepDefaultSaveSettings"):
        KeepDefaultSaveSettings.SetSaveSettingToDefaultOptions()

    if "PROCESSFACIAL" in os.environ.keys():
        Globals.Callbacks.OnUIIdle(AutoRunProcess.AutoRunProcess_Facial)


def OnFileOpenCompleted(control, event):
    """
    Custom method attached to the Mobu onFileCompleted Callback

    Arguments:
        control: Motion Builder object that is calling this method
        event (pyfbsdk.FBEvent)= Motion Builder event; unused
    """

    projectRoot = os.path.normpath(Config.CoreProject.Path.Root.lower())
    fbxPath = os.path.normpath(Globals.Application.FBXFileName.lower())
    if not fbxPath.startswith(projectRoot):
        message = "This file is not within the current project:   {}".format(projectRoot)
        popupWidget.TimedPopupWidget(
            text=message, prefixType=popupWidget.PrefixType.WARNING, timer=10000, safeToProcessEvents=False
        )

    # Unlock RS Cams - for cam team users only
    if CamValidationTools.CheckUserEmail():
        CameraLocker.LockManager().setAllCamLocks(False)
    # Unlock Other Cams - to unlock any animator cameras
    else:
        CameraLocker.LockManager().unlockOtherCams()

    # Log how long last scene was opened for
    Database.LogSceneTimeOpen()
    Database.LogSceneTimeLoaded(Globals.Application.FBXFileName)

    # Set the window title
    Scene.SetWindowTitle()


def OnFileNewCompleted(source, event):
    """
    When a new scene is finished loading, this method is called.

    Arguments:
        source (object): Motion Builder object calling the event
        event (FBEvent): the event being called
    """
    Database.LogSceneTimeLoaded(Globals.Application.FBXFileName)
    Database.initSceneTimeLoad()

    Scene.SetWindowTitle()


def OnFileSave(source, event):
    """
    When a file is saved, this method is called

    Arguments:
        source (object): Motion Builder object calling the event
        event (FBEvent): the event being called
    """
    if CamValidationTools.CheckUserEmail():
        CameraLocker.LockManager().setAllCamLocks(True)
        for cam in Globals.Scene.Cameras:
            if not cam.SystemCamera:
                for camChild in cam.Children:
                    camChild.PropertyList.Find("Visibility").Data = False
                    camChild.Show = False


def OnFileSaveCompleted(control, event):
    """
    Custom method attached to the Mobu onFileSaveCompleted Callback

    Arguments:
        control: Motion Builder object that is calling this method
        event (pyfbsdk.FBEvent)= Motion Builder event
    """
    # Use Default Save Settings
    if UserPreferences.__Readini__("menu", "KeepDefaultSaveSettings"):
        KeepDefaultSaveSettings.SetSaveSettingToDefaultOptions()

    # Unlock Cams - for cam team users only
    if CamValidationTools.CheckUserEmail():
        CameraLocker.LockManager().setAllCamLocks(False)

    Scene.SetWindowTitle()


def OnFileExit(control, event):
    """
    This method is called when Motion Builder is closed

    Arguments:
        control: Motion Builder object that is calling this method
        event (pyfbsdk.FBEvent)= Motion Builder event
    """

    # Remove Qt Plugins
    QtPluginsPackageDir = os.path.join(os.path.dirname(Config.Script.Path.RockstarRoot), "external", "qt", "plugins")
    for path in QtCore.QCoreApplication.libraryPaths():
        if QtPluginsPackageDir.lower() == path.replace(os.altsep, os.sep).lower():
            logging.info("Close - Removing Qt Plugin Paths".format(QtPluginsPackageDir))
            QtGui.QApplication.removeLibraryPath(path)
            break

    logging.info("Close - Closing Active Custom Qt Widgets")
    # Close all Qt windows to trigger cleanup code they might have
    widgetsToDelete = set()
    for widget in QtGui.QApplication.topLevelWidgets():
        # Skip standard QWidgets and QMenus as they are part of the main Motion Builder UI
        # Our Qt tools use our QtMainWindowBase, QtDockBase or QtDialogBase classes
        if hasattr(widget, "windowTitle") and type(widget) not in (
            QtGui.QWidget,
            QtGui.QMenu,
            QtGui.QLabel,
        ):
            widgetsToDelete.add(widget)

    # Close all Qt windows to trigger cleanup code they might have
    for widget in widgetsToDelete:
        logging.info("Close - \t Closing '{}' of type {}".format(widget.windowTitle(), widget.__class__.__name__))
        widget.close()
        del widget
