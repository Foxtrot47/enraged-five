"""
Tracks exceptions raised in python and sends email reports about them
"""
from __future__ import absolute_import

import os
import re
import sys
import datetime
import traceback
import webbrowser
from xml.etree import cElementTree as xml

import pyperclip

from rsg_core.configs import main as config
from rsg_core import emails
from rsg_core_py.exceptions import types
from rsg_core_py.exceptions import tracebacks as traceback_
from rsg_core_py.metaclasses import singleton
from rsg_clients.perforce import servers



class Handler(object):
    """
    Class that contains the logic for handling exceptions and sending error reports about them
    """
    _exceptionEmails = []
    _ignorableUsers = []

    ErrorPaths = [
            project.pythonTechart
            for project in (
                config.system.environment.toolsProject,
                config.system.environment.techartProject,
            )
            if project is not None
        ]
    QAOwnerEmail = {"Technicolor": "raviteja.varma@technicolor.com"}

    __metaclass__ = singleton.Singleton

    def __init__(self, silent=False, sendExceptions=True):
        """
        Initializer

        Arguments:
            silent (boolean): should the error dialog be suppressed when errors occur
            sendExceptions (boolean): should the hook send out email reports
        """
        self.title = "Python {}".format(re.search("[0-9.]*", sys.version).group())
        self.silent = silent
        self.suppressDialog = False # Suppresses the dialog if one has been set
        self.sendExceptions = sendExceptions  # send error emails
        self.emailIgnoredUsers = False  # email the users that the error emails are sent to
        self.debug = False # Only send error emails to the current user
        self.emailTemplate = os.path.join(os.path.dirname(__file__), "template.html")
        self.attachments = []

        self._errors = []
        self._parameterHtmlValues = {
                                    # Default Value:    ("#e6e5c9", "normal", "Black", True)
                                    "Project":          ("#f2f2f2", "bold", "Black", True),
                                    "Module":           ("#f2f2f2", "normal", "Blue", True),
                                    "Line Number":      ("#f2f2f2", "normal", "Blue", True),
                                    "Function":         ("#f2f2f2", "normal", "Blue", True),
                                    "Exception":        ("#f2f2f2", "normal", "Blue", True),
                                    "Traceback":        ("#f2f2f2", "normal", "Red", True),
                                    "SMTP":             ("#f2f2f2", "normal", "Red", True),
                                    "Comments":         ("#f2f2f2", "normal", "Black", True),
                                    "Bugstar Number":   ("#f2f2f2", "bold", "Black", False)
                                    }

        self._htmlRowTemplate = (
                                '<tr>\n'
                                    '\t<td style="text-align: right; color: rgb(253, 253, 253); background-color: #2f323c;'
                                        'font-weight: bold; padding-left:10px; padding-right:4px; width:110px;" >\n'
                                        '{title}:</td>'
                                    '\t<td style="vertical-align: middle; padding-left: 10px; padding-left: 10px; '
                                        'text-align: justify; color: {fontColor}; background-color:{backgroundColor}; '
                                        'font-weight:{fontWeight};">'
                                        '{value}'
                                    '</td>\n'
                                '</tr>'
                                )
        self._ignore = ("<MotionBuilder>",)
        self._dialogClass = None

    def __call__(self, exceptionType, exceptionValue, _traceback):
        """
        Overrides built-in method
        Allows the class instance to be hooked into python's exception hook

        Arguments:
            exceptionType (Exception): the type of exception
            exceptionValue (string): results of the exception
            _traceback (traceback): the traceback of the exception
        """
        if isinstance(exceptionValue, types.SilentError):
            return

        self.handleError(exceptionType, exceptionValue, _traceback)

    @property
    def exceptionEmails(self):
        if not self._exceptionEmails:
            self._getExceptionEmails()
        return self._exceptionEmails

    @exceptionEmails.setter
    def exceptionEmails(self, value):
        if isinstance(value, list):
            self._exceptionEmails = tuple([email.strip().lower() for email in value])
            if not self._ignorableUsers:
            	self._ignorableUsers = self._exceptionEmails

    @property
    def ignorableUsers(self):
        if not self._ignorableUsers:
            self._getExceptionEmails()
        return self._ignorableUsers

    @classmethod
    def _getExceptionEmails(cls):
        tree = xml.parse(os.path.join(os.path.dirname(__file__), "exception.xml"))
        root = tree.getroot()
        exceptions, ignore = [], []
        for element in root:
            exceptions.append(element.text)
            if element.attrib.get("ignore", "True") == "True":
                ignore.append(element.text.strip().lower())
        cls._exceptionEmails = tuple(exceptions)
        cls._ignorableUsers = tuple(ignore)

    @property
    def defaultWarning(self):
        """
        The default warning to use for the exception dialog

        Return:
            string
        """

        text = "The following information will be emailed to support."
        if not config.system.studio.exchangeServer:
            text = "Please email the following information to support."
        return ("An unhandled exception was caught by Rockstar!\n"
                "{}").format(text)

    @property
    def logPath(self):
        return os.path.join(config.system.environment.toolsProject.logs, "TechartTraceback.txt")

    def validate(self, exceptionType, exceptionValue, _traceback):
        """
        Method that is meant to be overridden in a subclass.

        Checks that the error being passed is a valid error.

        Arguments:
            exceptionType (Exception): the type of exception
            exceptionValue (string): results of the exception
            _traceback (traceback): the traceback of the exception

        Returns:
            bool
        """
        return True

    def handleError(self, exceptionType, exceptionValue, _traceback):
        """
        Core logic for handling exceptions

        Arguments:
            exceptionType (Exception): the type of exception
            exceptionValue (string): results of the exception
            _traceback (traceback): the traceback of the exception
        """
        print 'Unhandled exception caught by Rockstar!'

        # Print the exception to the Python console.
        traceback.print_exception(exceptionType, exceptionValue, _traceback)

        if not self.sendExceptions:
            print "Exception not emailed as user has turned off email notifications"
            return

        elif not self.validate(exceptionType, exceptionValue, _traceback):
            print "Exception is not a valid error."
            return

        filename = getattr(exceptionValue, "filename", "<No File>")
        lineNum = getattr(exceptionValue, "lineno", -1)
        text = getattr(exceptionValue, "text", "")

        # Extrapolate the exception info. New logic that handles a traceback that is empty.
        filename, lineNum, funcName, text = traceback_.extract(_traceback, filename=filename, lineNum=lineNum,
                                                               text=text)

        # Check if the exception has already been raised and reported.
        msgTup = (exceptionValue.__class__, exceptionValue.message, filename, lineNum)
        if msgTup in self._errors:
            print "Error has already been handled"
            return
        else:
            self._errors.append(msgTup)

        # Get the module name.

        # Escape \ so regex can understand them
        inValidDirectory = False
        for path in traceback_.paths(_traceback):
            absoluteFilename = os.path.abspath(path)
            moduleName = str(os.path.basename(absoluteFilename)).split('.')[0]

            inValidDirectory = re.match(r"|".join(self.ErrorPaths).replace("\\", "\\\\"), path, re.I)
            # Did the exception happen from the sandbox folder, a location tech art does not track
            inSandbox = re.search("motionbuildersandbox|sandboxmotionbuilder", absoluteFilename, re.I)
            # Did the exception occur from a module that should be ignored.
            ignoreModule = moduleName in self._ignore

            if inSandbox or ignoreModule:
                inValidDirectory = False
                break

        user = config.system.user.email
        userEmail = config.system.user.email if config.system.user.email != "unknown" else self.QAOwnerEmail.get(config.system.studio.name,
                                                                                    "svcrsgnycvmx@rockstargames.com")
        ignoreUser = config.system.user.email in self.ignorableUsers if not self.emailIgnoredUsers else False
        emailList = self.exceptionEmails if not self.debug else [userEmail]

        # Only send exception if we aren't in the exception email list, i.e. Technical Artists.
        # and the script lives in one of the tech art repositories i.e. X:\wildwest

        if not self.debug and (ignoreUser or not inValidDirectory):
            print('Exception not emailed as user {} is {}'.format(user ,
                            ('working outside the tech-art directories', 'in the exception list')[ignoreUser]))
            return
        # Get data for reporting the error and build the html
        subject, rows, tags = self._getData(exceptionType, exceptionValue, _traceback)
        html, width = self.buildHtml(rows)

        # Show the exception dialog.
        sendToBugsar, comment = self.show(html, width, buttonLabel="Send To Bugstar")

        if comment:
            rows.append(("Comments", comment))
            html, _ = self.buildHtml(rows)

        # Create frame data attachment
        frameData, success = traceback_.createFrameDataFileAttachment(_traceback, self.logPath)
        attachments = [frameData] if success else []
        attachments.extend(self.attachments)
        # Attempt to email the exception.
        success, debug = self.sendEmail(_traceback, emailList, subject, html, attachments)

        if not success:
            # from PySide import QtGui, QtCore

            rows.append(("SMTP", debug))
            html, _ = self.buildHtml(rows)
            self.copyToClipboard(html)
            if self.show(html, width,
                        "Email could not be sent !\n"

                        "Manually send the email error to the tech art department to get your error looked at \n"
                        "The error has already been copied your clipboard so you can paste it into the email.",
                        buttonLabel="Open Email", suppressComment=True)[0]:
                webbrowser.open("mailto:{}&Subject={}&Body=Paste Error Here".format(",".join(emailList), subject))

    def copyToClipboard(self, text):
        """
        Copy text to the clipboard

        Arguments:
            text (string): text to copy to clipboard
        """
        pyperclip.copy(text)

    def buildHtml(self, rows):
        """
        Builds the html to be shown to the user and emailed out to tech art support

        Arguments:
            rows (list): list of tuples, where each tuple contains a parameter name and value pair
        """

        rowHtmls = []
        titleColumnWidth = 0
        valueColumnWidth = 0
        for title, value in rows:
            _value = value
            isString = isinstance(value, basestring)
            backgroundColor, fontWeight, fontColor, disableHtml = self._parameterHtmlValues.get(
                                                                            title, ("#e6e5c9", "normal", "Black", True))

            if isString and disableHtml:
                value.replace("<", "&lt;").replace(">", "&gt;").replace(" ", "&nbsp;")
                value = re.sub("&lt;br&gt;|\n", "<br>", value)

            rowHtmls.append(self._htmlRowTemplate.format(title=title, backgroundColor=backgroundColor,
                                                         fontWeight=fontWeight, fontColor=fontColor, value=value))

            # resolve the width of the html that will be displayed

            _width = len(title)
            if _width > titleColumnWidth:
                titleColumnWidth = _width

            for string in str(_value).splitlines():
                _width = len(string)
                if _width > valueColumnWidth:
                    valueColumnWidth = _width

        with open(self.emailTemplate, "r") as _file:
            html = _file.read()
            title = "Standalone"
            html = html.replace("#TITLE#", title)
            html = html.replace("#CONTENT#", "\n".join(rowHtmls))

        # The 4 is just some extra buffer width
        width = titleColumnWidth + valueColumnWidth

        return html, width

    def _getData(self, exceptionType, exceptionValue, _traceback):
        """
        Gets the data to build the html code for the email and bug.

        Arguments:
            exceptionType (Exception): the type of exception
            exceptionValue (string): results of the exception
            _traceback (traceback): the traceback of the exception

        Returns:
            tuple(string, list, list)
        """
        filename = getattr(exceptionValue, "filename", "<No File>")
        lineNum = getattr(exceptionValue, "lineno", -1)
        text = getattr(exceptionValue, "text", "")

        filename, lineNumber, funcName, text = traceback_.extract(_traceback, filename=filename, lineNum=lineNum,
                                                                  text=text)
        tracebackString = "".join(traceback.format_exception(exceptionType, exceptionValue, _traceback))
        moduleName = str(os.path.basename(filename)).split('.')[0]
        fileInfo = servers.gameDepot.fileState(filename.encode('string_escape').replace('\\\\', '\\'))

        # Default Values

        subject = "{} - Python Tools Exception: {}".format(self.title, moduleName)
        rows = [("Project", config.system.environment.toolsProject.name),
                ("Username", config.system.user.name),
                ("Host", config.system.host),
                ("Timestamp", datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
                ("Python Filename", filename),
                ("Perforce Revision", "{} of {}".format(getattr(fileInfo, "haveRevision", 0),
                                                 getattr(fileInfo, "headRevision", 0))),
                ("Module", moduleName),
                ("Line Number", lineNumber),
                ("Function", funcName),
                ("Exception", exceptionType.__name__),
                ("Traceback", tracebackString)]
        tags = []

        return subject, rows, tags

    def displayText(self, rows):
        """
        converts the list of rows into text

        Arguments:
            rows (list): list of tuples, where each tuple contains a parameter name and value pair
        Return:
            string
        """
        return "\n".join(["{}: {}".format(name, value) for name, value in rows])

    def dialog(self):
        """ Class for the error dialog to use """
        return self._dialogClass

    def setDialog(self, dialogClass):
        """
        Sets which error dialog to use

        Arguments:
            dialogClass (QDialog): dialog to use for showing errors. The class being passed must be support a
                                   exception method that accepts a string and returns tuple containing a
                                   boolean if the dialog was accepted or not and a string.
        """
        self._dialogClass = dialogClass

    def show(self, text, width, description=None, buttonLabel="", suppressComment=False):
        """
        Shows the error dialog and returns the if the user wants to send the error to bugstar and any comment
        they may have written

        Arguments:
            text (string): text or html to show to the user
            width (int): for the text or html to display
        Return:
            tuple (boolean, string)
        """
        description = description or self.defaultWarning
        if self._dialogClass is None or self.suppressDialog or self.silent:
            return False, ""
        return self._dialogClass.exception(None, description=description, text=text, width=width,
                                           buttonText=buttonLabel, suppressComment=suppressComment)

    @staticmethod
    def sendEmail(_traceback, emailTo, emailSubject, emailBody, attachments):
        """
        Sends an email of the error to the techart team

        Arguments:
            _traceback(traceback): traceback of the error
            emailTo (string): email adress(es) to send error report to
            emailSubject (string): the title of the email
            emailBody (string): the message/contents of the email.
            attachments (list): paths to files to attach to the email
        """
        if not config.system.studio.exchangeServer:
            return False, None
        emailAddress = config.system.user.email if not config.system.user.email.startswith("unknown") else Handler.QA_OWNER_EMAIL.get(config.system.user.studio.name,
                                                                                        "svcrsgnycvmx@rockstargames.com")
        return emails.send(emailFrom=emailAddress, emailTo=emailTo, emailSubject=emailSubject, emailBody=emailBody,
                           attachments=attachments, asHtml=True)

    @classmethod
    def register(cls, title=None, exceptionEmails=None, dialog=None):
        """
        Registers the hook to the python interpreter.

        Arguments:
            exceptionEmails(string): list of email addresses to send exception email too
            dialog
        """
        handler_ = cls()

        if title:
            handler_.title = title
        handler_.suppressDialog = False
        if dialog:
            handler_.setDialog(dialog)
            handler_.suppressDialog = True

        handler_.exceptionEmails = exceptionEmails
        if sys.excepthook != handler_:
            sys.excepthook = handler_

        return handler_


def register(title=None, exceptionEmails=None, dialog=None, hook=None):
    """
    Registers the hook to the python interpreter.

    Arguments:
        title (string): title for the emails sent with the hooks
        exceptionEmails(string): list of email addresses to send exception email too
        dialog (QtGui.QDialog): dialog to bring up when an error is encountered
        hook (rsg_core.exceptions.hook.Handler): custom handler to register rather then the default one
    """
    hook = hook or handler
    if title:
        hook.title = title
    hook.suppressDialog = False
    if dialog:
        hook.setDialog(dialog)
        hook.suppressDialog = True

    hook.exceptionEmails = exceptionEmails
    if sys.excepthook != handler:
        sys.excepthook = handler
    return hook

handler = Handler()
