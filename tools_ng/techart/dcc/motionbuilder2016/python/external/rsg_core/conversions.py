"""Modules for general conversion of data from one type to another."""
import re
import ast
import datetime
import base64

import pytz
from dateutil import parser, tz


def isFloat(value):
    """
    Checks if the given string can be a float

    Arguments:
        value (str): can this string be a float

    Returns:
        bool
    """
    try:
        float(value)
        return True
    except:
        return False


def isDateTime(value, datetimeFormat=None):
    """
    Checks if this string can be converted to datetime

    Args:
        value (str): String that represents a datetime format

    Keyword Args:
        datetimeFormat (str): The date time format to use.
                              If none is provided then the value criteria will not be strict.

    Returns:
        bool
    """
    try:
        if datetimeFormat is not None:
            datetime.datetime.strptime(value, datetimeFormat)
        else:
            parser.parse(value)
        return True

    except:
        return False


def convertValue(value, datetimeFormat=None):
    """
    Converts a string value into an appropriate python data type.
    This method also accept iterables (list, tuples and dictionaries).

    Arguments:
        value (object): string or an iterable with strings that should be converted to another
                        standard python data type

    Keyword Args:
        datetimeFormat (bool): datetime format to convert the string to

    Returns:
        object
    """
    if value is None:
        return None
    elif isinstance(value, dict):
        value = {key: convertValue(value, datetimeFormat=datetimeFormat) for key, value in value.iteritems()}
    elif isinstance(value, (tuple, list)):
        value = [convertValue(subvalue, datetimeFormat=datetimeFormat) for subvalue in value]
    elif isDateTime(datetimeFormat):
        value = toDateTime(value)
    elif isinstance(value, basestring):
        try:
            # If the intended value of the string is to be a string but has not quotes, then this will error out.
            value = ast.literal_eval(value)
        except (ValueError, SyntaxError):
            pass

    return value


def encode(string):
    """Convert a string into bytes.

    Args:
        string (str): string to convert into bytes

    Notes:
        We simply do a try-catch instead of an if statement as it is faster to compute.
        This method maybe used widely and we don't want to slow things down further.

    Returns:
        bytes
    """
    try:
        return string.encode()
    except AttributeError:
        return string


def decode(bytestring):
    """Convert a bytes into string.

    Args:
        bytestring (str): bytes to convert into string

    Notes:
        We simply do a try-catch instead of an if statement as it is faster to compute.
        This method maybe used widely and we don't want to slow things down further.

    Returns:
        str
    """
    try:
        return bytestring.decode()
    except AttributeError:
        return bytestring


def base64Encode(string):
    """
    Encodes the provided byte string into base64 str

    Args:
        string (str): A byte string to be encoded.

    Returns: str
    """
    string = encode(string)
    return base64.b64encode(string).decode()


def base64Decode(bytestring):
    """
    Decodes a base64 encoded byte string into a normal unencoded string

    Args:
        bytestring (bytes): The encoded string

    Returns:
        str
    """
    return base64.b64decode(encode(bytestring)).decode("utf-8")


def convertToAscii(value, xml=False):
    """
    Removes invalid ascii characters from unicode and strings

    Args:
        value (basestring): string or unicode with invalid ascii characters

    Keyword Args:
        xml (bool): make the string xml friendly
    Returns:
        str
    """
    try:
        result = value.encode("utf-8", "ignore").decode("ascii", "ignore")
    except UnicodeDecodeError:
        # Skip the utf-8 conversion as it doesn't always work
        result = value.decode("ascii", "ignore")

    if xml:
        result = re.sub("[\x00-\x08\x0B-\x0C\x0E-\x1F\x7F]", "", result)
    return result


def escapeString(string):
    """Escapes special string characters such as

    The string returned will be encoded in utf-8

    Notes:
        string-escape was removed in python 3 and this is the work around.

    Returns:
        str
    """
    # We convert the string to bytes and then decode again
    return encode(string).decode("unicode_escape")


def toDateTime(value):
    """
    Converts a string to datetime

    Arguments:
        value (str): a date time string value to convert into datetime

    Returns:
        datetime.datetime
    """
    return parser.parse(value)


def fromDateTime(value, format=None):
    """
    Converts a datetime object to a string

    Arguments:
        value (datetime.datetime): a datetime value to convert into str

        format (str, optional): format to switch datetime to. Defaults to ISO 8601 format (YYYY-MM-DDTHH:MM:SS)

    Returns:
        str
    """
    return value.strftime(format) if format else value.replace(microsecond=0).isoformat()


class Datetime(object):
    """
    Container for various common datetime object conversions.

    See Also:
        https://medium.com/@eleroy/10-things-you-need-to-know-about-date-and-time-in-python-with-datetime-pytz-dateutil-timedelta-309bfbafb3f7
    """

    @classmethod
    def toUtc(cls, localDatetime):
        """
        Convert a timezone naive datetime with a local timezone value to a new timezone naive datetime with a UTC value.

        Args:
            localDatetime (datetime.datetime): A timezone naive datetime object with a local timezone value.

        Returns:
            datetime.datetime
        """
        localTz = tz.tzlocal()
        localDatetime = localDatetime.replace(tzinfo=localTz)
        utcTz = tz.tzutc()
        utcDatetime = localDatetime.astimezone(utcTz)
        return utcDatetime.replace(tzinfo=None)

    @classmethod
    def toLocal(cls, utcDatetime):
        """
        Convert a timezone naive datetime with a UTC value to a new timezone naive datetime with a local timezone value.

        Args:
            utcDatetime (datetime.datetime): A timezone naive datetime object with a UTC value.

        Returns:
            datetime.datetime
        """
        utcTz = tz.tzutc()
        utcDatetime = utcDatetime.replace(tzinfo=utcTz)
        localTz = tz.tzlocal()
        localDt = utcDatetime.astimezone(localTz)
        return localDt.replace(tzinfo=None)

    @classmethod
    def setTimeZoneToUtc(cls, naiveDatetime):
        """Sets the timezone to UTC for a naive datetime object with no timezone value."""
        if naiveDatetime.tzinfo is None:
            return pytz.UTC.localize(naiveDatetime)
        return naiveDatetime


def convertBytes(size, sizeOut="KB", base=1000.0):
    """convert bytes to KB, MB, GB or TB

    Args:
        size (float): size in bytes
        sizeOut (str): unit size to convert bytes.
        base (float): unit base size (1000.0 or 1024.0)

    Returns:
        float: converted bytes
    """
    sizeOut = sizeOut.upper()

    b = float(size)
    kb = float(base)
    mb = float(kb ** 2)
    gb = float(kb ** 3)
    tb = float(kb ** 4)

    formatSize = {"KB": b / kb, "MB": b / mb, "GB": b / gb, "TB": b / tb}

    return formatSize.get(sizeOut, b)
