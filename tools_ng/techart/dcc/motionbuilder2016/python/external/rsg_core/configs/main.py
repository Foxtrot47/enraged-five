"""Config information required to run our tools.

The structure is system/machine first so the information flows as follows

-appConfig
-system
--system.environment
---system.environment.toolsProject
--system.studio
--system.user
"""
from rsg_core.configs import system


class AppConfig:
    """Additional Config Version settings"""

    FFMPEG = "4_0_0"


# General system information
system = system.System()
appConfig = AppConfig()
