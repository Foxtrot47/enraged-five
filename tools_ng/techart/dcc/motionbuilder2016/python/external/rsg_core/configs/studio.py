"""Metadata about the different R* Studios.

The data is retrieved from the techart microservices when possible but will fallback on the studios.meta if it can't
connect to the network.

File dependency when the techart service is unavailable:
    Windows:
        <project>/tools/<toolsbranch>/etc/globals/studio.meta
        or
        X:/techart/<project>/python/rockstar/core/resources/configs/studio.meta
    Linux:
        /etc/rockstar/studio.meta
"""
from xml.etree import cElementTree as xml

import ipaddress
import requests
import platform

from rsg_core_py.decorators import memoize
from rsg_core_resources import resources


class Studio(object):
    """Studio metadata used by our code."""

    _STUDIO_META_PATH = None

    def __init__(self, data=None):
        self.name = None
        self.domain = None
        self.exchangeServer = None
        self.p4ServerDefault = None
        self.p4ServerMocap = None
        self.p4ServerSwarm = None
        self.networkDrive = None
        self.subnetMasks = []
        self.ldapServer = None

        if data:
            self.setupData(data)

    @classmethod
    def getByIpAddress(cls, ipaddress_, path):
        """Gets the studio based on its friendly name

        Args:
            ipaddress_ (str): the ipaddress whose studio belongs to it
            path (str): the path to the studios.meta file to use for comparing ipaddresses against

        Returns:
            Studio
        """
        studios = cls.all(path)
        for studio in studios:
            for subnetMask in studio.subnetMasks:
                if ipaddress_ in ipaddress.ip_network(unicode(subnetMask)):
                    return studio

    @memoize.memoize
    @classmethod
    def all(cls, path=None):
        """Gets all the studios.

        Returns:
            list
        """
        data = None
        try:
            data = cls._getDataFromService()
        except Exception:
            if path:
                data = cls._getDataFromFile(path)
        if not data:
            raise LookupError("Could not access the studio.meta service or meta file.")
        return [cls(studioData) for studioData in data]

    def setupData(self, data):
        """Sets the values to the class from the retrieved data.

        Arguments:
            data (dict): dictionary with the data from the studio.meta
        """
        self.name = data.get("FriendlyName", None) or data.get("friendlyname", None)
        self.shortname = data.get("name", None)
        self.domain = data.get("Domain", None)
        self.exchangeServer = data.get("ExchangeServer", None) or data.get("exchangeServer", None)
        self.p4ServerDefault = data.get("PerforceServer", None)
        self.p4ServerMocap = data.get("PerforceMocapServer", None)
        self.p4ServerSwarm = data.get("PerforceSwarmServer", None)
        self.networkDrive = data.get("NetworkDrive", None)
        self.subnetMasks = data.get("SubnetMasks", None) or data.get("ips", None)
        self.ldapServer = "ldaps://ldap-rsg.core.t010.net" if self.name != "R* India" else "rsginddmc1"

    @memoize.memoize
    @staticmethod
    def _getDataFromService():
        """Gets the studio meta data from the techart service.

        Returns:
            list
        """
        with requests.session() as session:
            session.verify = resources.getWindowsCert() if platform.system() == "Windows" else resources.getLinuxCert()
            # TODO: Update url so it doesn't connect directly to the service machine
            response = session.get("https://webapi-dev1.techart.rockstargames.com/api/v1/Studios/GetAllStudioCfg")
            return response.json()

    @memoize.memoize
    @staticmethod
    def _getDataFromFile(path):
        """Reads the studio meta and returns a list with all the metadata for each studio.

        Args:
            path (str): path to the studio.meta file

        Returns:
            list
        """
        studioConfigFileTree = xml.parse(path)
        studioConfigFileRoot = studioConfigFileTree.getroot()

        studios = []
        for element in studioConfigFileRoot.findall("Studios/Item[@type='Studio']"):
            subnetMaskElements = element.find("SubnetMasks")
            if not subnetMaskElements:
                subnetMaskElements = [element.find("SubnetMask")]
            subnetMasks = [subnetMaskElement.text for subnetMaskElement in subnetMaskElements]
            data = {subElement.tag.strip(): subElement.text for subElement in element}
            data["SubnetMasks"] = subnetMasks
            studios.append(data)
        return studios
