"""
Information about the user and their computer.

Required files:
    Windows:
        <project>/tools/<toolsbranch>/local.xml
        <project>/tools/<toolsbranch>/config.xml
"""
import json
import base64
import getpass
import os
import random
import string
from xml.etree import cElementTree as xml

# TODO: Create new library for managing storage of settings and passwords

from rsg_core import conversions
from rsg_core_py.exceptions import types
from rsg_core_py.metaclasses import singleton


class User(object):
    """
    Information about the current user.

    Properties:
        name (str): User name as listed in Active Directory.
        email (str): The user's email as listed in Active Directory (normalized to lowercase).
        type (str): The "type" name as listed in the tools config.
        puppetClass (str|None): Puppet class of the user's current Linux host (on Windows this is always None).

    Note:
        While this class can be used directly and still function properly, by convention it should
        only be accessed via config.system.user.
    """
    __metaclass__ = singleton.Singleton

    def __init__(self, system):
        """Initializer

        Args:
            system (rsg_core.configs.system): Data about the system
        """
        self.__system = system
        self.name = getpass.getuser()
        self.type = "Unknown"

        if system.isWindows:
            self.type = self._getType()

        # expose cached data of the user

        self.__asciiCharacters = string.ascii_letters + string.digits
        self.__email = None

    @property
    def email(self):
        """Gets the email for the current user.

        Notes:
            accessing AD can be expensive at startup, so we only query the email when asked.
        Returns:
            str
        """
        if self.__email is None:
            # TODO: This is a cyclical import that needs to be addressed.
            from rsg_core import activeDirectory
            self.__email = activeDirectory.DirectoryServices().getEmailAddress()
        return self.__email

    # TODO: Can probably remove this method
    @classmethod
    def _getType(cls):
        """Gets the data for the user when on Windows."""
        toolsRoot = os.environ.get("RS_TOOLSROOT", "")

        # Read the local.xml to get user data
        localConfigFilePath = os.path.join(toolsRoot, "local.xml")

        if not os.path.exists(localConfigFilePath):
            return "Unknown"

        localConfigFileTree = xml.parse(localConfigFilePath)
        localConfigFileRoot = localConfigFileTree.getroot()

        userElement = localConfigFileRoot.find("User")
        userType = userElement.find("UserType")
        if userType:
            userType = int(userType.text.strip())

        # Read config xml to get usertypes
        toolsConfigFilePath = os.path.join(toolsRoot, "config.xml")
        toolsConfigFileTree = xml.parse(toolsConfigFilePath)
        toolsConfigFileRoot = toolsConfigFileTree.getroot()

        userTypeName = "Unknown"
        # Determine user type.
        for element in toolsConfigFileRoot.find("usertypes"):
            # convert hexadecimal to decimal to compare values
            if int(element.attrib.get("flags", "0x0000"), 16) == userType:
                userTypeName = element.attrib.get("uiname", "Unknown")
                break
        return userTypeName

    # TODO: Move away from storing files locally to mongodb instead
    @property
    def __settingsPath(self):
        """Path to the secrets file we use to store our settings."""
        return os.path.join(self.__system.appDataDir, ".settings.txt")

    def __loadCache(self):
        """Loads the cache from disk.

        Returns:
            dict
        """
        if not os.path.exists(self.__settingsPath):
            return {}
        with open(self.__settingsPath, "rb") as handle:
            return json.loads(base64.b64decode(handle.read()) or b"{}")

    def __saveCache(self, cache):
        """ Updates the QSettings user cache for the current user.

        Args:
            cache (dict): the dictionary with the cached data
        """
        if not os.path.exists(self.__system.appDataDir):
            os.mkdir(self.__system.appDataDir)
        with open(self.__settingsPath, "wb") as handle:
            handle.write(conversions.encode(conversions.base64Encode(json.dumps(cache))))

    def authenticate(self, instance, **kwargs):
        """
        Passes the password to the object that needs to authenticate as the user.

        The object being passed in for authentication should have method called authenticate that accepts a parameter
        called password.

        The authenticate method of the instance being passed should return true or false

        Args:
            instance (object): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        # Note: This is not a good way to access passwords at all but is a necessary evil at this time
        hash = self.getCache("hash", None)
        if not hasattr(instance, "authenticate"):
            raise types.AuthenticationError("{} does not have a login method".format(type(instance)))

        elif hash is None:
            raise types.AuthenticationError("No password has been set")

        # remove extra characters from hash
        # TODO: Add decoding endpoint for decoding the password.
        return instance.authenticate(password="".join(hash[::2]), **kwargs)

    def setPassword(self, password, isEncoded=True):
        """
        Sets the password for the user

        Arguments:
            password (str): password to store
            isEncoded (bool): is the password already encoded in base64
        """
        if not isEncoded:
            password = conversions.base64Encode(password)

        # TODO: Add encoding endpoint to encrypt the password
        # Add random characters to the hash so it is less obvious what it contains
        cache = self.__loadCache()
        cache["hash"] = "".join(["{}{}".format(each, random.choice(self.__asciiCharacters)) for each in password])
        self.__saveCache(cache)

    def getCache(self, key, default=None):
        """
        Gets a value from the user cache

        Arguments:
            key (str): key for retrieving data
            default (str): default value to return when the key isn't in the cache

        Returns:
            str or None
        """
        cache = self.__loadCache()
        return cache.get(key, default)

    def setCache(self, key, value):
        """
        Sets a value to the user cache.
        Please note that the values will be converted to a string if they aren't already one.

        Arguments:
            key (str): key for retrieving data
            value (object): the value to store
        """
        cache = self.__loadCache()
        cache[key] = value
        self.__saveCache(cache)

    def removeCache(self, key):
        """
        Removes the value from the user cache

        Arguments:
            key (str): key to remove
        """
        cache = self.__loadCache()
        cache.pop(key, None)
        self.__saveCache(cache)
