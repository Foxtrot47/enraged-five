import os
import platform
import socket
import sys
import unittest
import getpass

from rsg_core.configs import main, environment, studio, project, user
from rsg_core_resources import resources

# TODO: Add Unit Test specifically for when this test is ran on an automation machine
# This will allow us to test that the config against an environment that we know how it is setup
# vs adding dummy data that only really tests path resolution


class TestConfig(unittest.TestCase):
    def setUp(self):
        """Sets up mock environment variables to test our config code with.

        * RS_PROJECT: The project set by the tools installer on windows or setws on linux
        * RS_TECHART_PROJECT_BRANCH: The project that the techart code should be pointing to
        * RS_PROJROOT: The root folder on disk for the project, this is set by the tools installer on windows
        * RS_TOOLSROOT: The location of the tools department code, this is set by the tools installer on windows
        * RS_DEV: Are we running on dev, this is set by hopper on windows and setws on linux
        * RS_TECHART: The location of the Tech Art directory, this is set by hopper on windows and linux.
              This usually the on disk location of //techart/<project>
        * RS_PYTHON_VER: The versions of python to use, this is set by hopper on linux and windows
        * GIANT_DATA_DIR (Linux only): The location of the giant/1777 direcotry on disk, this is set by setws
        * DOCKER_ENV: Is this running from a docker environment
        """
        self._currentEnvironment = dict(os.environ)
        # Set a mock environment to test data with
        # Techart Environment Variables
        os.environ["RS_DEV"] = "true"
        os.environ["RS_PYTHON_VER"] = "3.7.3"
        os.environ["DOCKER_ENV"] = "true"

        # Fake tools and techart os environment
        os.environ["RS_PROJECT"] = "dummy"
        os.environ["RS_TA_PROJECT_P4_NAME"] = "development"
        if platform.system() == "Windows":
            os.environ["RS_PROJROOT"] = r"x:\dummy"
            os.environ["RS_TOOLSROOT"] = r"x:\dummy\tools\release"
            os.environ["RS_TA_PROJECT_LOCAL_ROOT"] = r"x:\techart\development"

        elif platform.system() == "Linux":
            os.environ["GIANT_DATA_DIR"] = "/media/giant_capture_data/development"
            os.environ["RS_TA_PROJECT_LOCAL_ROOT"] = "/media/giant_capture_data/north/techart/development"

    def tearDown(self):
        """Restore environment to how it was before we changed the values."""
        os.environ.update(self._currentEnvironment)

    def test_System(self):
        """Test that the values for the system are correct."""
        self.assertEqual(main.system.kernel, platform.system())
        self.assertEqual(main.system.host, socket.gethostname())
        self.assertEqual(main.system.pythonVersion, platform.python_version())

        self.assertEqual(main.system.is64bit, sys.maxsize > 2 ** 32)
        self.assertEqual(main.system.isWindows, platform.system() == "Windows")
        self.assertEqual(main.system.isLinux, platform.system() == "Linux")
        self.assertEqual(main.system.isOsx, platform.system() == "OSX")

        if main.system.isWindows:
            self.assertEqual(main.system.appDataDir, "C:\\Users\\{user}\\AppData\\Local\\Rockstar Games".format(user=getpass.getuser()))
        elif main.system.isLinux:
            self.assertEqual(main.system.appDataDir, "/home/{user}/.rockstar".format(user=getpass.getuser()))

    def test_Environment(self):
        """Test that environment variables are being resolved properly."""
        environment_ = environment.Environment(main.system)
        self.assertEqual(environment_.isDevelopment, True)
        self.assertEqual(environment_.inDocker, True)

        # Tech Art related variables
        self.assertEqual(environment_.pythonTechartVersion, "3.7.3")
        self.assertEqual(environment_.techartServiceUrl, "https://webapi-dev1.techart.rockstargames.com/")

        self.assertIsInstance(environment_.techartProject, project.Project)
        self.assertEqual(environment_.techartProject.displayName, "Mocap Development - Development")
        self.assertEqual(environment_.techartProject.gameP4Name, "development")
        self.assertEqual(environment_.techartProject.mocapP4Name, "development")
        self.assertEqual(environment_.techartProject.bugstarId, "5638974")
        self.assertEqual(environment_.techartProject.color, "#bdbdbd")

        # Tools related variables
        self.assertIsInstance(environment_.toolsProject, project.Project)
        self.assertEqual(environment_.toolsProject.name, "dummy")
        # fmt: off
        if platform.system() == "Windows":
            self.assertEqual(environment_.workspaceRoot, "x:\\")
            self.assertEqual(environment_.techartProjectRoot, r"x:\techart\development")
            self.assertEqual(environment_.techartRoot, r"x:\techart")
            self.assertEqual(environment_.pythonTechartExternal, None)  # change once we move to our distributed model
            self.assertEqual(environment_.studioMeta, r"x:\dummy\tools\release\etc\globals\studios.meta")
            self.assertEqual(environment_.virtualProduction, r"x:\virtualproduction")
            self.assertEqual(environment_.previz, r"x:\virtualproduction\previz")
            self.assertEqual(environment_.sslCertificate, resources.getWindowsCert())

            # Tools Project related variables
            self.assertEqual(environment_.toolsProject.branch, "dev")
            self.assertEqual(environment_.toolsProject.art, r"x:\dummy\art")
            self.assertEqual(environment_.toolsProject.anim, r"x:\dummy\art\animation")
            self.assertEqual(environment_.toolsProject.assets, r"x:\dummy\asset")
            self.assertEqual(environment_.toolsProject.export, r"x:\dummy\asset\export")
            self.assertEqual(environment_.toolsProject.text, r"x:\dummy\asset\export\data\lang")
            self.assertEqual(environment_.toolsProject.metadata, r"x:\dummy\asset\metadata")
            self.assertEqual(environment_.toolsProject.definitions, r"x:\dummy\asset\metadata\definitions")
            self.assertEqual(environment_.toolsProject.processed, r"x:\dummy\asset\processed")
            self.assertEqual(environment_.toolsProject.tune, r"x:\dummy\asset\fragments")

            self.assertEqual(environment_.toolsProject.audio, r"x:\dummy\audio\dev")
            self.assertEqual(environment_.toolsProject.script, r"x:\dummy\script\dev")
            self.assertEqual(environment_.toolsProject.ui, r"x:\dummy\ui\dev")

            self.assertEqual(environment_.toolsProject.toolsRoot, r"x:\dummy\tools\release")
            self.assertEqual(environment_.toolsProject.build, r"x:\dummy\tools\release\build\dev")
            self.assertEqual(environment_.toolsProject.common, r"x:\dummy\tools\release\build\dev\common")
            self.assertEqual(environment_.toolsProject.shaders, r"x:\dummy\tools\release\build\dev\common\shaders")
            self.assertEqual(environment_.toolsProject.preview, r"x:\dummy\tools\release\build\dev\preview")

            self.assertEqual(environment_.toolsProject.bin, r"x:\dummy\tools\release\bin")
            self.assertEqual(environment_.toolsProject.code, r"x:\dummy\tools\release\src\dev")
            self.assertEqual(environment_.toolsProject.ragecode, r"x:\dummy\tools\release\src\dev\rage")
            self.assertEqual(environment_.toolsProject.config, r"x:\dummy\tools\release\etc")
            self.assertEqual(environment_.toolsProject.temp, r"x:\dummy\tools\release\temp")

            self.assertEqual(environment_.toolsProject.motionbuilderTechArt, r"x:\dummy\tools\release\techart\dcc\motionbuilder\python")

        elif platform.system() == "Linux":
            self.assertEqual(environment_.workspaceRoot, r"/media/giant_capture_data/north")
            self.assertEqual(environment_.techartProjectRoot, "/media/giant_capture_data/north/techart/development")
            self.assertEqual(environment_.techartRoot, "/media/giant_capture_data/north/techart")
            self.assertEqual(environment_.pythonTechartExternal, None)  # change once we move to our distributed model
            self.assertEqual(environment_.studioMeta, "/etc/rockstar/studios.meta")
            self.assertEqual(environment_.virtualProduction, r"/media/giant_capture_data/north/virtualproduction")
            self.assertEqual(environment_.previz, r"/media/giant_capture_data/north/virtualproduction/previz")
            self.assertEqual(environment_.sslCertificate, resources.getLinuxCert())

            # Tools Project related variables
            self.assertEqual(environment_.toolsProject.branch, "dev")
            self.assertEqual(environment_.toolsProject.art, r"/media/giant_capture_data/north/dummy/art")
            self.assertEqual(environment_.toolsProject.anim, r"/media/giant_capture_data/north/dummy/art/animation")
            self.assertEqual(environment_.toolsProject.assets, r"/media/giant_capture_data/north/dummy/asset")
            self.assertEqual(environment_.toolsProject.export, r"/media/giant_capture_data/north/dummy/asset/export")
            self.assertEqual(environment_.toolsProject.text, r"/media/giant_capture_data/north/dummy/asset/export/data/lang")
            self.assertEqual(environment_.toolsProject.metadata, r"/media/giant_capture_data/north/dummy/asset/metadata")
            self.assertEqual(environment_.toolsProject.definitions, r"/media/giant_capture_data/north/dummy/asset/metadata/definitions")
            self.assertEqual(environment_.toolsProject.processed, r"/media/giant_capture_data/north/dummy/asset/processed")
            self.assertEqual(environment_.toolsProject.tune, r"/media/giant_capture_data/north/dummy/asset/fragments")

            self.assertEqual(environment_.toolsProject.audio, r"/media/giant_capture_data/north/dummy/audio/dev")
            self.assertEqual(environment_.toolsProject.script, r"/media/giant_capture_data/north/dummy/script/dev")
            self.assertEqual(environment_.toolsProject.ui, r"/media/giant_capture_data/north/dummy/ui/dev")

            self.assertEqual(environment_.toolsProject.toolsRoot, r"/media/giant_capture_data/north/dummy/tools/release")
            self.assertEqual(environment_.toolsProject.build, r"/media/giant_capture_data/north/dummy/tools/release/build/dev")
            self.assertEqual(environment_.toolsProject.common, r"/media/giant_capture_data/north/dummy/tools/release/build/dev/common")
            self.assertEqual(environment_.toolsProject.shaders, r"/media/giant_capture_data/north/dummy/tools/release/build/dev/common/shaders")
            self.assertEqual(environment_.toolsProject.preview, r"/media/giant_capture_data/north/dummy/tools/release/build/dev/preview")

            self.assertEqual(environment_.toolsProject.bin, r"/media/giant_capture_data/north/dummy/tools/release/bin")
            self.assertEqual(environment_.toolsProject.code, r"/media/giant_capture_data/north/dummy/tools/release/src/dev")
            self.assertEqual(environment_.toolsProject.ragecode, r"/media/giant_capture_data/north/dummy/tools/release/src/dev/rage")
            self.assertEqual(environment_.toolsProject.config, r"/media/giant_capture_data/north/dummy/tools/release/etc")
            self.assertEqual(environment_.toolsProject.temp, r"/media/giant_capture_data/north/dummy/tools/release/temp")

            self.assertEqual(environment_.toolsProject.motionbuilderTechArt, r"/media/giant_capture_data/wildwest/dcc/motionbuilder/python")
        # fmt: on

    def test_Project(self):
        """Test that we can grab the project data properly."""
        environment_ = environment.Environment(main.system)

        # Confirm that getting a project on mongo works
        self.assertIsInstance(environment_.techartProject, project.Project)
        self.assertEqual(environment_.techartProject.name, "Mocap Development - Development")

        # Confirm that getting all projects on mongo work
        self.assertGreater(len(environment_.allProjects), 1)

        # Confirm that we  can get a project by its codename
        mocapProjects = environment_.getProjectByMocapP4Name("development")
        # We have two projects that use development as their mocap P4 name
        self.assertGreater(len(mocapProjects), 1)
        # TODO: Figure out how to resolve this within the project code elegantly
        mocapProject = [mocapProject for mocapProject in mocapProjects if mocapProject.techartP4Name == "development"][0]
        self.assertIsInstance(mocapProject, project.Project)
        self.assertEqual("Mocap Development - Development", mocapProject.name)
        self.assertEqual("development", mocapProject.mocapP4Name)

    def test_Studio(self):
        """Test that the data is being returned properly for the studios."""
        testStudioData = None
        allData = studio.Studio._getDataFromService()
        for eachStudioData in allData:
            if eachStudioData.get("name", None) == "newyork":
                testStudioData = eachStudioData
                break

        self.assertIsNotNone(testStudioData)

        testStudio = studio.Studio(testStudioData)
        self.assertEqual(testStudio.name, "R* New York")
        self.assertEqual(testStudio.shortname, "newyork")
        self.assertEqual(testStudio.subnetMasks, ["10.1.0.0/16", "10.127.3.0/24"])
        self.assertEqual(testStudio.exchangeServer, "smtp-na.rockstar.t2.corp")

    def test_User(self):
        """Test that data for the user is being returned properly."""
        testUser = user.User(main.system)
        self.assertEqual(testUser.name, getpass.getuser())

        # Tests caching user specific data
        testUser.removeCache("test")
        self.assertEqual(testUser.getCache("test", None), None)
        testUser.setCache("test", True)
        self.assertEqual(testUser.getCache("test", False), True)
        testUser.removeCache("test")
        self.assertEqual(testUser.getCache("test", None), None)
