"""Information about the current operating system."""
import os
import platform
import re
import socket
import subprocess
import sys
import tempfile
import ipaddress as ipLib

import pathlib2 as pathlib
import psutil

try:
    import win32file
except ImportError:
    # This import is only needed in Windows for resolving a path name when it gets truncated by the os for being too long.
    pass

from rsg_core_py.decorators import memoize
from rsg_core.configs import studio, user, environment


class System(object):
    """Data about the operating system running the current python process."""

    # Consts
    WINDOWS = "Windows"
    LINUX = "Linux"
    OSX = "OSX"

    def __init__(self):
        """Initializer."""
        self.kernel = platform.system()
        self.host = socket.gethostname()
        self.pythonVersion = platform.python_version()

        self.is64bit = sys.maxsize > 2 ** 32
        self.isWindows = self.kernel == self.WINDOWS
        self.isLinux = self.kernel == self.LINUX
        self.isOsx = self.kernel == self.OSX
        self.isPython2 = sys.version_info[0] == 2
        self.isPython3 = sys.version_info[0] == 3

        self._ipAddress = None
        self._ipAddresses = None

        # Changes to the application and applicationRegex values should happen at the startup of an application
        self.application = "Standalone Python {}".format(self.pythonVersion)
        self.applicationRegex = ""

        self._studio = None
        self._user = None
        self._environment = None

    def _getIpAddresses(self, family=socket.AF_INET):
        """Get all the existing IP addresses on the machine.

        Args:
            family (socket constant, optional): Socket address/protocol family constant to filter on;
                Options: socket.AF_INET|socket.AF_INET6; Default: socket.AF_INET (ipv4)

        Returns:
            tuple of ipaddress.IPv4Address
        """
        snics = [snic for val in list(psutil.net_if_addrs().values()) for snic in val]
        ipAddresses = [snic.address for snic in snics if snic.family == family]

        localIpAddress = "127.0.0.1"
        ipAddresses.remove(localIpAddress)

        return tuple(ipLib.ip_address(unicode(ipAddress)) for ipAddress in ipAddresses)

    @memoize.memoize
    def _getPrimaryIpAddressAndStudio(self):
        """Gets the primary ip address and studio for this machine"""
        for ipaddress in self.ipAddresses:
            studio_ = studio.Studio.getByIpAddress(ipaddress, self.environment.studioMeta)
            if studio_:
                return studio_, ipaddress
        return None, None

    def _getPrimaryIpAddressOnLinux(self):
        """Gets the primary IP address on the machine (linux only).

        First finds the default interface by what the default route is.
        Then grabs the IP of that interface with 'ip addr show <interface name>'

        Puppet uses a similar process under the hood to determine the IP address to use as its primary.

        Returns:
            ipAddress (ipaddress.IPv4Address): The primary IP address.
        """
        route = re.findall(r"([\w.][\w.]*'?\w?)", subprocess.check_output(["ip", "route"]))
        defaultInterface = route[4]
        ipv4 = re.search(
            re.compile(r"(?<=inet )(.*)(?=\/)", re.M), os.popen("ip addr show {}".format(defaultInterface)).read()
        ).groups()[0]
        return ipLib.ip_address(bytes(ipv4).decode())

    @property
    def studio(self):
        """The studio that this machine is located in.

        Returns:
            studio.Studio
        """
        self._studio, _ = self._getPrimaryIpAddressAndStudio() if not self._studio else (self._studio, None)
        return self._studio

    @property
    def environment(self):
        """The environment variables and related resolved paths.

        Returns:
            environment.Environment
        """
        self._environment = environment.Environment(self) if not self._environment else self._environment
        return self._environment

    @property
    def user(self):
        """The current user logged in to the system.

        Returns:
            user.User
        """
        self._user = user.User(self) if not self._user else self._user
        return self._user

    @property
    def ipAddresses(self):
        """The available IP addresses on this machine.

        Returns:
            tuple
        """
        if self._ipAddresses is None:
            self._ipAddresses = self._getIpAddresses()

            # If multiple IPs, shift the primary IP address to the front of the list (Linux-only).
            # This will make sure it is selected when system.ipaddress is called.
            if self.isLinux and len(self._ipAddresses) > 1:
                preferredIp = self._getPrimaryIpAddressOnLinux()
                self._ipAddresses = list(self._ipAddresses)
                self._ipAddresses.remove(preferredIp)
                self._ipAddresses.insert(0, preferredIp)
        return tuple(self._ipAddresses)

    @property
    def ipAddress(self):
        """The primary ip address of this machine.

        Returns:
            str
        """
        if self._ipAddress is None:
            if self.isLinux:
                primaryIpAddress = self._getPrimaryIpAddressOnLinux()
            else:
                _, primaryIpAddress = self._getPrimaryIpAddressAndStudio()
            self._ipAddress = primaryIpAddress
        return self._ipAddress

    @property
    def puppetClass(self):
        """Gets the puppet class for the current system on linux.

        Note:
            Puppet classes are assigned using the host name as the key.

        Returns:
            str: The name of the puppet class.
        """
        # The puppet directory is not accessible without root (sudo) access
        # We are wrapping it in a try statement incase the file doesn't exist (as os.exists won't work)
        puppetDir = os.path.join("/", "opt", "puppetlabs", "puppet", "cache", "state")
        lastRunReportPath = os.path.join(puppetDir, "last_run_report.yaml")
        try:
            content = subprocess.check_output(["sudo", "cat", lastRunReportPath])
        except:
            content = ""

        for line in content.splitlines():
            if line.startswith(b"environment:"):
                _, puppetClass = line.split(b":")
                return puppetClass.strip()

    @property
    def tempDir(self):
        """Get a temp dir with the user directory portion of the path fully expanded.

        Notes:
            tempfile.gettempdir() will not be fully expanded on Windows if the user name is long enough.

        Returns:
            str
        """
        if self.isWindows:
            return str(win32file.GetLongPathName(tempfile.gettempdir()))
        return tempfile.gettempdir()

    @property
    def documentsDir(self):
        """Get the path to the user's Documents directory.

        Returns:
            str
        """
        return str(pathlib.Path.home().joinpath("Documents"))

    @property
    def desktopDir(self):
        """Get the path to the user's Desktop directory.

        Returns:
            str
        """
        return str(pathlib.Path.home().joinpath("Desktop"))

    @property
    def appDataDir(self):
        """Gets the app data directory """
        if self.isWindows:
            return str(pathlib.Path.home().joinpath(os.path.join("AppData", "Local", "Rockstar Games")))
        return str(pathlib.Path.home().joinpath(".rockstar"))

    @property
    def downloadsDir(self):
        """Get the path to the user's Downloads directory.

        Returns:
            str
        """
        return str(pathlib.Path.home().joinpath("Downloads"))

    def setPreferredIpAddress(self, ipAddress):
        """Set the preferred address.

        Required for resolving correct ip address on workstations with multiple ethernet adapters.

        Args:
            ipAddress (ipaddress.IPv4Address): The preferred ip address.
        """
        self._ipAddress = ipAddress
        if self._ipAddresses:
            ipAddresses = list(self._ipAddresses)
            ipAddresses.remove(ipAddress)
            ipAddresses.insert(0, ipAddress)
            self._ipAddresses = tuple(ipAddresses)
