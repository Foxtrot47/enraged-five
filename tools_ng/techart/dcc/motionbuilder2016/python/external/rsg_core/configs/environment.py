import os
import re
import logging

from rsg_core.configs import project

try:
    from rsg_core_resources import resources
except ImportError:
    resources = None

logger = logging.getLogger(__name__)


class Environment(object):
    """Paths and data resolved from the current environment variables set for the current process.

    The environment variables this expects to find are as follows:
    * RS_DEV: Are we running on development mode for testing purposes, this is set by hopper on windows and .rsglobalrc on linux.
    * RS_PROJECT: The project set by the tools installer on windows or .rsglobalrc on linux.
    * RS_PROJROOT: The root folder on disk for the project, this is set by the tools installer on windows
    * RS_TOOLSROOT: The location of the tools department code, this is set by the tools installer on windows
    * RS_TA_PROJECT_P4_NAME: The project that the techart code should be pointing to, this is set by our setPyVenv bat and bash files.
    * RS_TA_PROJECT_LOCAL_ROOT: The location of the Tech Art directory, this is set by our setPyVenv bat and bash files.
                  This usually the on disk location of //techart/<project>
    * RS_PYTHON_VER: The versions of python to use, this is set by .rsglobalrc on linux and hopper on windows
    * GIANT_DATA_DIR (Linux only): The location of the giant/1777 direcotry on disk, this is set by setws
    * DOCKER_ENV: Is this running from a docker environment
    """

    def __init__(self, system):
        """Initializer.

        Args:
            system (rsg_core.configs.system.System): information about the current machine and python process
        """
        self.__system = system
        self.__projectBuilder = project.ProjectBuilder(system=system, environment=self)
        self.__toolsProject = None
        self.__techartProject = None
        self.__defaultWindowsRoot = "x:\\"
        self.__defaultLinuxRoot = "/media/giant_capture_data/north"

        # Are we running our tools in developer mode/out of the development branch
        self.isDevelopment = os.environ.get("RS_DEV", "false") == "true"

        # Docker Environment Variables
        self.inDocker = re.search("true", os.environ.get("DOCKER_ENV", "false")) is not None
        self.techartSecret = os.environ.get("TA_SECRET", None)
        self.shotgunSecret = os.environ.get("TA_SHOTGUN_SECRET", None)

        # Tools related environment variables

        # These variables shouldn't be used outside of the configs library itself
        # It is preferred that project data be accessed by the ToolsProject variable instead.
        # RS_PROJECT is the project p4 name set by the tools installer
        # RS_PROJROOT is the root directory of the project
        # RS_TOOLSROOT is the root directory for the tools directory currently in use

        # Techart related environment variables

        # RS_TA_PROJECT_P4_NAME is the techart project being set
        # RS_TA_PROJECT_LOCAL_ROOT is the root directory of the techart branch being used

        # TODO: This should probably be its own environment variable
        # The root directory of the p4 workspace from the rsgperforce:1666 server that syncs the project branches
        # On most occasions on windows this should be the X:\ drive and /media/giant_capture_data/north on linux
        self._workspaceRoot = None
        self._techartRoot = None

        # Tech Art Environment Variables
        # The path to the techart directory where we are importing our code from
        self._pythonTechart = None
        self._pythonTechartExternal = None

        # Virtual production depot location on disk
        self._virtualProduction = None
        self._previz = None

        # Techart helper paths
        # Where we store non-python resource files for our core libraries

        self._resources = None
        self._certificates = None
        self._sslCertificate = None
        self._studioMeta = None

    def _getEnvironmentVariable(self, environmentVariable, raiseError=True, defaultValue=None):
        """Gets the given environment variable and raises an error if it is missing.

        Args:
            environmentVariable (str): The environment variable to query the system
            raiseError (bool): raise an error if the environment variable is not set. Defaults to True
            defaultValue (str): default value for the environment variable if it isn't set.
                                The raiseError argument must be false for this arguement to be respected
        Returns:
            str
        """
        value = os.environ.get(environmentVariable, None)
        if not value:
            logger.warning(
                "Environment variable {} is not set".format(environmentVariable)
            )
        if not value and raiseError:
            raise LookupError("Environment variable {} is not set".format(environmentVariable))
        elif not value and defaultValue is not None:
            logger.warning(
                "Defaulting '{}' to {} ".format(environmentVariable, defaultValue)
            )
            value = defaultValue
        return value

    @property
    def techartProjectName(self):
        """The techart project name as it is on P4.

        Returns:
            str
        """
        return self._getEnvironmentVariable(
            "RS_TA_PROJECT_P4_NAME",
            defaultValue="development" if self.isDevelopment else "production",
            raiseError=False,
        )

    @property
    def techartProjectRoot(self):
        """The techart project root as it is on P4.

        This will usually be where the //techart/<project> is mapped to locally.

        Returns:
            str
        """
        return self._getEnvironmentVariable(
            "RS_TA_PROJECT_LOCAL_ROOT",
            defaultValue=os.path.join(self.techartRoot, self.techartProjectName),
            raiseError=False,
        )

    @property
    def toolsProjectName(self):
        """The tools project name as it is on P4.

        Notes:
            This value is usually set by the tools installer

        Returns:
            str
        """
        return self._getEnvironmentVariable("RS_PROJECT")

    @property
    def toolsProjectRoot(self):
        """The root directory of the project set by the tools installer as it is mapped locally from P4.

        This will usually be where the //<project> is mapped to on disk.

        Notes:
            This value is usually set by the tools installer

        Returns:
            str
        """
        return self._getEnvironmentVariable("RS_PROJROOT", raiseError=False)

    # TODO: Revist and evaluate if it is better fit as the tools branch root
    @property
    def toolsRoot(self):
        """The root directory for the tools directory currently in use as it is mapped on locally from P4.

        This will usually be where the //<project>/tools/release is mapped to on disk.

        Notes:
            This value is usually set by the tools installer

        Returns:
            str
        """
        return self._getEnvironmentVariable("RS_TOOLSROOT", raiseError=False)

    @property
    def pythonTechartVersion(self):
        """The python version of the code that we are running, this is separate from the interpreter version

        Returns:
            str
        """
        return self._getEnvironmentVariable("RS_PYTHON_VER", raiseError=False, defaultValue="2.7.15")

    @property
    def giantDataRoot(self):
        """The root project directory for Giant applications on Linux, where all capture and post data is stored

        Notes:
            This environment variable is only set in Linux.
            The contents from this directory are usually synced from rsgperforce:1777
            Project specific TCL scripts from rsgperforce:1777 are also synced here
            
        Returns:
            str
        """
        return self._getEnvironmentVariable("GIANT_DATA_DIR")

    @property
    def pythonTechartExternal(self):
        """The path were python external libraries that the techart code depends on are being sourced from.

        Returns:
            str
        """
        if not self._pythonTechartExternal:
            # TODO: We may want this to be driven by an environment variable
            if resources:
                self._resources = os.path.dirname(resources.__file__)
        return self._pythonTechartExternal

    @property
    def techartRoot(self):
        """The location of the techart depot.

        This is the location of //techart on disk.

        Returns:
            str
        """
        if not self._techartRoot:
            self._techartRoot = os.path.join(self.workspaceRoot, "techart")
        return self._techartRoot

    @property
    def workspaceRoot(self):
        """The root on disk location where the perforce server rsgperforce:1666 syncs depots to.

        Notes:
            On windows it is usually X:\
            On linux it is usually /media/giant_capture_data/north.

        Returns:
            str
        """
        if not self._workspaceRoot:
            if self.toolsProjectRoot:
                # On windows, the tools installer should be setting the tools project root to the environment
                self._workspaceRoot = os.path.dirname(self.toolsProjectRoot)
            else:
                # TODO: Add environment variable for the P4 root, RS_P4_ROOT ?
                # TODO: Add OSX default value if that ever becomes a thing
                self._workspaceRoot = self.__defaultWindowsRoot if self.__system.isWindows else self.__defaultLinuxRoot
                logger.warning("Resolving the workspace root to the default value of {}".format(self._workspaceRoot))
        return self._workspaceRoot

    @property
    def virtualProduction(self):
        """Location of the virtual production depot on disk.

        Returns:
            str
        """
        if not self._virtualProduction:
            self._virtualProduction = os.path.join(self.workspaceRoot, "virtualproduction")
        return self._virtualProduction

    @property
    def previz(self):
        """Location of the previz brach of the virtual production depot on disk.

        Returns:
            str
        """
        if not self._previz:
            self._previz = os.path.join(self.virtualProduction, "previz")
        return self._previz

    @property
    def studioMeta(self):
        """The location of the studios.meta file on disk

        Returns:
            str
        """
        if not self._studioMeta:
            # Resolve where the studio meta file lives on disk if available
            if self.__system.isWindows and self.toolsProject:
                studioMeta = os.path.join(self.toolsProject.toolsRoot, "etc", "globals", "studios.meta")

            elif self.__system.isLinux:
                studioMeta = os.path.join("/etc", "rockstar", "studios.meta")
            else:
                studioMeta = None

            self._studioMeta = studioMeta
        return self._studioMeta

    @property
    def resources(self):
        """The location of the directory for our non-python resource files.

        Returns:
            str
        """
        if not self._resources:
            if resources:
                self._resources = resources.getResourcesPath()
        return self._resources

    @property
    def certificates(self):
        """Location for certificates our tools may depend on.

        Returns:
            str
        """
        if not self._certificates:
            if resources:
                self._resources = resources.getCertificatePaths()
            else:
                self._resources = os.path.join(self.resources, "certificates")
        return self._certificates

    @property
    def sslCertificate(self):
        """The ssl certificate to use for this system.

        Returns:
            str
        """
        if not self._sslCertificate:
            if self.__system.isWindows and resources:
                self._sslCertificate = resources.getWindowsCert()
            elif self.__system.isLinux and resources:
                self._sslCertificate = resources.getLinuxCert()
        return self._sslCertificate

    @property
    def techartServiceUrl(self):
        """The techart microservice to target for retrieving project and studio related data.

        Returns:
            str
        """
        if self.isDevelopment:
            return "https://webapi-dev1.techart.rockstargames.com/"
        return "https://webapi.techart.rockstargames.com/"

    @property
    def allProjects(self):
        """Returns all the available projects.

        Returns:
            list
        """
        return self.__projectBuilder.getAll()

    @property
    def toolsProject(self):
        """Gets the current project set through the RS_PROJECT Environment variable.

        This variable is primarily set by the tools installer on window and by setws on linux

        Returns:
            project.Project
        """
        if self.__toolsProject is None:
            # Check to make sure the required environment variable is set by calling the decorated property that uses it
            self.toolsProjectName
            # Resolve the project
            for each in self.allProjects:
                if each.isToolsProject:
                    self.__toolsProject = each

            if not self.__toolsProject:
                logger.warning("Existing data for the tools project could not be found")

            if not self.__toolsProject and self.toolsProjectName:
                logger.warning("Dynamically building the tools project data for {}; this data maybe inaccurate".format(self.toolsProjectName))
                self.__toolsProject = self.__projectBuilder._buildProject(
                    {
                        "displayName": self.toolsProjectName,
                        "gameP4Name": self.toolsProjectName,
                        "mocapP4Name": self.toolsProjectName,
                        "taP4Name": self.techartProjectName
                    }
                )
        return self.__toolsProject

    @property
    def techartProject(self):
        """Gets the current project set through the RS_TA_PROJECT_P4_NAME Environment variable.

        This variable is primarily set by techart on both windows and linux.

        Returns:
            project.Project
        """
        # Resolve the project
        if self.__techartProject is None:
            for each in self.allProjects:
                if each.isTechArtProject:
                    self.__techartProject = each
        return self.__techartProject

    def getProjectByMocapP4Name(self, name):
        """Gets a project by its Mocap P4 Name.

        Notes:
            multiple projects may share the same mocap p4 name so a list is returned

        Args:
            name (str): The Mocap P4 name of the project, the name is usually the codename used for that project.
        Returns:
            list
        """
        return self.__projectBuilder.getByMocapP4Name(name)
