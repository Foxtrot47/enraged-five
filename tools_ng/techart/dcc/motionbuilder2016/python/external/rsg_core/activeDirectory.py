"""
Contains class for interacting with Rockstar's Active Directory Directory Services.

Example:
    >>> from rsg_core import activeDirectory
    >>> activeDirectory.DirectoryServices().getEmailAddress()
    nikola.tesla@rockstar.com

Notes:
    * Basic unit tests are located in RS/Core/Database/UnitTest/test_ActiveDirectory.py
"""
import sys
from distutils import version
import base64
import getpass
import re
import os
import tempfile

import ldap3


class ActiveDirectoryError(Exception):
    """
    Covers all errors directly related to Active Directory queries, etc.
    """


class MissingAttributeError(ActiveDirectoryError):
    """
    Covers the case where an attribute was queried, but data wasn't found for the specified user.
    """


class Connection(object):
    """
    Wrapper for handling all connection logic.

    Warning:
        In older versions of Python 2.7 on Windows, sys.stdin.encoding returned "cp0". This
        encoding is not supported in ldap3-2.5 (the version at time of writing). The workaround
        is to patch the ldap3 config module to use the value that the later patched versions of
        Python would have returned: "utf-8".
    """
    # Patch for older versions of Python 2.7.
    if version.LooseVersion(sys.version.split(" ")[0]) < version.LooseVersion("2.7.11"):
        ldap3.utils.config._DEFAULT_CLIENT_ENCODING = "utf-8"

    def __init__(self, user, password, ldapHost):
        self._user = user
        self._password = password
        self._ldapHost = ldapHost
        self._connection = None

    def _getConnection(self):
        server = ldap3.Server(self._ldapHost, get_info=ldap3.ALL, use_ssl=True)
        return ldap3.Connection(
            server,
            user=self._user,
            password=self._password,
            authentication=ldap3.NTLM,
            auto_bind=True
        )

    def connect(self):
        """
        Connect to AD via LDAP and return the connection class.

        The LDAP connection class can be used as a context manager so this class' close method
        doesn't need to be called. See this page:
        https://ldap3.readthedocs.io/tutorial_intro.html?highlight=Context%20manager#connection-context-manager

        Returns:
            ldap3.core.connection.Connection
        """
        try:
            if self._connection is not None and self._connection.bound:
                self._connection.unbind()
            self._connection = self._getConnection()
        except Exception as error:
            self._connection = self._getConnection()
        return self._connection

    @property
    def connection(self):
        """
        Get the current connection. Could be None, if a connection has not yet been made.

        Returns:
            ldap3.core.connection.Connection
        """
        return self._connection

    @property
    def isConnected(self):
        if self._connection is None:
            return False
        return self._connection.bound

    def close(self):
        if self.isConnected:
            self._connection.unbind()


class ConnectionManager(object):
    """
    Convenience methods for configuring the Connection class.
    """
    # TODO: Remove the logic in this class and move it to appropriate modules
    # Login info should go to accounts
    # ldap data should go to the studio
    DEFAULT_HOST = "ldaps://ldap-rsg.core.t010.net"
    INDIA_HOST = "rsginddmc1"
    _USER = "ROCKSTAR\\svcrsgldapbind"
    _PASSWORD = base64.b64decode("ajBYRjZ6RzZoRTE3M2Jy")

    @classmethod
    def getLdapHost(cls, studioName=None):
        """
        Get the LDAP host machine name for the specified studio.

        Keyword Args:
            studioName (str|None): The studio name to get from the studio: host mapping.
                Default: The current studio name as listed in the tools config.

        Returns:
            str
        """
        # Active Directory can't import configs
        from rsg_core.configs import main as config

        studioName = studioName or config.system.studio.name
        mappings = {"R* India": cls.INDIA_HOST, "Technicolor": cls.INDIA_HOST}
        return mappings.get(studioName, cls.DEFAULT_HOST)

    @classmethod
    def getConnection(cls):
        """
        Get a configured connection class.

        Returns:
            ldap3.core.connection.Connection
        """
        return Connection(cls._USER, cls._PASSWORD, cls.getLdapHost()).connect()


class DirectoryServices(object):
    """
    Methods for interacting with R*'s instance of Active Directory's Directory Services.

    Contains methods exposing specific Active Directory searches.

    Notes:
        * Be sure to run related unit tests when making changes!
            * Located in: rockstar/core/tests/test_activeDirectory.py
    """
    DEFAULT_SEARCH_BASE = "dc=rockstar,dc=t2,dc=corp"

    @classmethod
    def _getConnection(cls):
        """
        Get a connection (can be used as a context manager).
        """
        return ConnectionManager.getConnection()

    @staticmethod
    def getSearchFilterFromUser(user=None):
        """
        Get the appropriate LDAP search filter for the "user string".

        Keyword Args:
            user (str): E.g. "nikola.tesla", "nikola.tesla@rockstar.com", "Nikola Tesla"
                Default: The current user name.

        Returns:
            str
        """
        if user is not None:
            searchFilter = "(samAccountName={})".format(user)
            if '@' in user:
                # get user based off their email
                searchFilter = "(mail={})".format(user)
            elif " " in user:
                # we are user a first and surname to filter
                name = user.strip().split(" ")
                searchFilter = "(&(givenName={})(sn={}))".format(name[0], name[-1])
            elif user.isdigit():
                searchFilter = "(employeeId={})".format(user)
        else:
            searchFilter = "(samAccountName={})".format(getpass.getuser())
        return searchFilter

    def getUserAttributes(self, user=None, attributes=None):
        """
        Wrapper method for fetching a specific user's attributes.

        Args:
            user (str): E.g. "nikola.tesla", "nikola.tesla@rockstar.com", "Nikola Tesla"
                Default: The current user name.

        Keyword Args:
            attributes (list of str): List of the attributes to get back from Active Directory.

        Returns:
            dict: The entry attributes
        """
        searchFilter = self.getSearchFilterFromUser(user)
        with self._getConnection() as conn:
            conn.search(
                search_base=self.DEFAULT_SEARCH_BASE,
                search_filter=searchFilter,
                search_scope=ldap3.SUBTREE,
                attributes=attributes or [ldap3.core.connection.ALL_ATTRIBUTES]
            )
            if not conn.entries:
                attrs = ",".join(attributes) if attributes else ldap3.core.connection.ALL_ATTRIBUTES
                msg = "Failed to get any entries back for requested attributes: '{}'".format(attrs)
                raise ActiveDirectoryError(msg)
            return conn.entries[0].entry_attributes_as_dict

    def getEmailAddress(self, user=None):
        """
        Get the email address of the user (including service accounts).

        Notes:
            * The "mail" attr does not exist on service accounts.
            * "userPrincipalName" is in both account types.

        Keyword Args:
            user (str): E.g. "nikola.tesla", "nikola.tesla@rockstar.com", "Nikola Tesla"
                Default: The current user name.

        Raises:
            MissingAttributeError: If a requested attribute's data was not found on the user.

        Returns:
             unicode: The email address normalized to lowercase, e.g. "address@rockstargames.com"
        """
        attrPrimary = "mail"
        attrSecondary = "userPrincipalName"
        attributes = [attrPrimary, attrSecondary]
        attrs = self.getUserAttributes(user=user, attributes=attributes)
        value = attrs.get(attrPrimary) or attrs.get(attrSecondary)
        if not value:
            user = user or getpass.getuser()
            attrs = ",".join(attributes)
            msg = "Could not find data for attributes '{}' on user '{}'".format(attrs, user)
            raise MissingAttributeError(msg)
        return value[0].lower()

    def getFullName(self, user=None):
        """
        Get the full name of the user from the username.

        Keyword Args:
            user (str): E.g. "nikola.tesla", "nikola.tesla@rockstar.com"
                Default: The current user name.

        Raises:
            MissingAttributeError: If a requested attribute's data was not found on the user.

        Returns:
             unicode: The full name of the user, e.g. "Nikola Tesla".
        """
        attrPrimary = "name"
        attributes = [attrPrimary]
        attrs = self.getUserAttributes(user=user, attributes=attributes)
        value = attrs.get(attrPrimary)
        if not value:
            user = user or getpass.getuser()
            attrs = ",".join(attributes)
            msg = "Could not find data for attributes '{}' on user '{}'".format(attrs, user)
            raise MissingAttributeError(msg)
        return value[0]

    def getUserPhoto(self, user=None):
        """
        Get the user's photo as a local .jpg image.

        Will always return None for service accounts, as they don't have a "thumbnailPhoto" attr.

        Keyword Args:
            user (str): E.g. "nikola.tesla", "nikola.tesla@rockstar.com", "Nikola Tesla"
                Default: The current user name.

        Returns:
            unicode: The tmp file path for the user's photo | None: no thumbnail data was found
        """
        user = user or getpass.getuser()
        attribute = "thumbnailPhoto"
        attrs = self.getUserAttributes(user=user, attributes=[attribute])
        value = attrs.get(attribute)
        if value:
            filestream = value[0]
            fd, path = tempfile.mkstemp(suffix='.jpg', prefix=user + "_")
            with os.fdopen(fd, "wb") as tmp:
                tmp.write(filestream)
            return path
        return None

    def getUserGroups(self, user=None):
        """
        Get the Active Directory groups for the user.

        Keyword Args:
            user (str): The user name to use in Active Directory search, e.g. "nikola.tesla"

        Returns:
             list of str: List of permission group names.
        """
        user = user or getpass.getuser()
        attribute = "memberOf"
        with self._getConnection() as conn:
            conn.search(
                search_base=self.DEFAULT_SEARCH_BASE,
                search_filter="(&(objectClass=user)(samAccountName={0}))".format(user),
                search_scope=ldap3.SUBTREE,
                attributes=[attribute]
            )
            rawGroupStrings = conn.entries[0].entry_attributes_as_dict.get(attribute)
        regex = re.compile("(^CN=)([0-9a-zA-Z_ \-]+)")
        return [str(regex.match(pattern).groups()[1]) for pattern in rawGroupStrings]

    def isUserInGroups(self, groupNames):
        """
        Check for user's membership in any of the specified Active Directory groups.

        Args:
            groupNames (list of str): The list of Active Directory group names to test.

        Returns:
            bool: True if user is in one of the specified groups, else False.
        """
        userGroups = self.getUserGroups()
        for groupName in groupNames:
            if groupName in userGroups:
                return True
        return False


if __name__ == '__main__':
    ds = DirectoryServices()
    print(ds.getEmailAddress())
