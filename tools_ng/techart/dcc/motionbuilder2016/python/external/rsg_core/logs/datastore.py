"""
Classes for uploading log files to a database.
"""
import os
import getpass
import socket
import platform
import datetime

import psutil
from bson import binary

from rsg_core.configs import main as config
from rsg_core.database import databases
from rsg_core.logs import const


class AppLogData(object):
    """
    Data class for an application's log related data.
    """

    __slots__ = "pid", "name", "logPath", "procCmdline", "procCreateTime"


class Datastore(object):
    """
    Contains methods for submitting the data related to a log file.

    Can be further expanded to cover tool check-ins.

    Designed for inserting a single document and then disconnecting, not for continuously leaving a connection open.
    """

    applications = {}

    @classmethod
    def _getConnection(cls):
        return databases.NycMongoProd.logs()

    @classmethod
    def _getLogDocumentData(cls, path, appName):
        # The log's text data must first be converted to binary data before the insert.
        with open(path, "r") as fileObject:
            fileData = fileObject.read()
        bsonEncodedFileData = binary.Binary(fileData)

        # Assemble doc.
        docData = {
            # depot data
            const.PrimaryFields.PROJECT: config.system.environment.toolsProjectName,
            # system data
            const.PrimaryFields.OS: platform.system(),  # E.g. 'Linux', 'Windows'
            const.PrimaryFields.USER: getpass.getuser(),
            const.PrimaryFields.HOST: socket.gethostname().lower(),  # Windows has uppercase host names.
            const.PrimaryFields.STUDIO: config.system.studio.name,
            const.PrimaryFields.CREATION_TIME: datetime.datetime.utcnow(),
            # log data
            const.PrimaryFields.LOG_DATA: bsonEncodedFileData,
            const.PrimaryFields.LOG_FILENAME: os.path.basename(path),
            const.PrimaryFields.APP_NAME: appName,
        }
        return docData

    @classmethod
    def _insertLogDocument(cls, data):
        docId = None
        connection = cls._getConnection()
        try:
            collection = connection.getCollection(databases.NycMongoProd.LOGS_COLLECTION)
            docId = collection.insert_one(data).inserted_id
        except Exception as error:
            print(str(error))
        finally:
            connection.close()
        return docId

    @classmethod
    def registerApplication(cls, pid, name, logPath):
        """
        Register the application so its log can be uploaded once the application closes.

        Args:
            pid (int): The process ID of the application.
            name (str): The name of the application.
            logPath (str): The file path to the log.
        """
        process = psutil.Process(pid)

        appData = AppLogData()
        appData.pid = pid
        appData.name = name
        appData.logPath = logPath
        appData.procCmdline = process.cmdline()[1]
        appData.procCreateTime = process.create_time()

        cls.applications[pid] = appData

    @classmethod
    def deregisterApplication(cls, pid):
        """
        Deregister the application from the service.

        Args:
            pid (int): The process ID of the application.

        Returns:
            AppLogData
        """
        return cls.applications.pop(pid)

    @classmethod
    def submitLog(cls, path, appName):
        """
        Submit a log file to the database.

        Warning:
            * MongoDB has a maximum document size of 16 MB.
            * More here: https://docs.mongodb.com/manual/reference/limits/#limit-bson-document-size

        Args:
            path (str): The file path to the log.
            appName (str): The name of the application.
        """
        data = cls._getLogDocumentData(path, appName)
        cls._insertLogDocument(data)


if __name__ == "__main__":
    import pathlib2 as pathlib

    path = pathlib.Path(config.system.tempDir).joinpath("test_logfileservice.plog")
    if not path.exists():
        path.touch()
        # TODO populate file with 1MB of data
        # TODO populate file with 16.2MB of data

    # monkey patch the connection to point at a local instance of mongo and test submitting to it.
    def getTestConnection():
        from rsg_core.database import mongo

        return mongo.Connection(host="sanw-mkapfhamm").connect(database="log_files", timeout=2000)

    ds = Datastore()
    ds._getConnection = getTestConnection
    ds.submitLog(path, "TestBoss")
