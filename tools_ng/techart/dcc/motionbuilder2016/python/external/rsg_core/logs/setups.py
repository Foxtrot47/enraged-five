"""
Contains convenience for configuring the various logging parts based on common scenarios.

The methods are modelled somewhat after logging.basicConfig, but with a more defined method signature.
"""
import logging
import os
import datetime

import pathlib2 as pathlib

from rsg_core.configs import main as config
from rsg_core.logs import formatters, plog
from rsg_core_py.logs import colorizedStreamHandler


def setLogLevelByName(loggers, level=logging.CRITICAL):
    """
    Set the logging level for a specified group of loggers. Helpful to quiet 'noisy' loggers.

    Args:
        loggers (list[str]): The names of all loggers that need updated.
        level (int, optional): The new logging level.
    """
    rootLogger = logging.getLogger()
    for name in loggers:
        log = rootLogger.manager.loggerDict.get(name, None)
        try:
            log.setLevel(level)
        except Exception:
            pass  # it was a logging.PlaceHolder instance or None


def setStreamHandlerLevel(level):
    """
    Sets stream handler level.

    Args:
        level (int): level for the stream handler (check logging built-in module for levels.)
    """
    rootLogger = logging.getLogger()
    for handler in rootLogger.handlers:
        if isinstance(handler, logging.StreamHandler):
            handler.setLevel(level)
            break


def setFileHandlerLevel(level):
    """
    Sets file handler level.

    Args:
        level (int): level for the file handler (check logging built-in module for levels.)
    """
    rootLogger = logging.getLogger()
    for handler in rootLogger.handlers:
        if isinstance(handler, logging.FileHandler):
            handler.setLevel(level)
            break


def console(level=logging.INFO):
    """
    Setup a logger to use the colorized stream handler and the aligned messages formatter.

    This logging configuration is designed for general development needs in the Python console or developer facing
    command line tools where file logging isn't required.

    Args:
        level (int, optional): The logging level to apply to the logger. Defaults to logging.INFO.

    Returns:
        logging.Logger: The root logger.
    """
    if not plog.LoggingModuleSetup.isReady():
        plog.LoggingModuleSetup.run()

    logger = logging.getLogger()  # Root logger.

    logger.setLevel(level)

    formatter = formatters.AlignedMessagesFormatter()

    streamHandler = colorizedStreamHandler.ColorizedStreamHandler()
    streamHandler.setFormatter(formatter)
    streamHandler.setLevel(level)
    logger.root.addHandler(streamHandler)

    return logger


def consoleDebugging(streamLevel=logging.DEBUG, fileLevel=logging.DEBUG, logPath=None):
    """
    Setup a logger to use the colorized stream handler, the debugging formatter, and the plog file handler.

    This logging configuration is designed for as needed debugging in the Python console or command line tools.

    Args:
        streamLevel (int, optional): The logging level to apply to the stream handler. Defaults to logging.INFO.
        fileLevel (int, optional): The logging level to apply to the file handler. Defaults to logging.DEBUG.
        logPath (logging.Logger, optional): The logger to setup.

    Returns:
        logging.Logger: The root logger.
    """
    if logPath is None:
        timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        logPath = os.path.join(config.system.tempDir, "consoleDebugging_{}.plog".format(timestamp))

    if not plog.LoggingModuleSetup.isReady():
        plog.LoggingModuleSetup.run()

    logger = logging.getLogger()  # Root logger.

    logger.setLevel(min((streamLevel, fileLevel)))  # Set to the lowest level, so it doesn't interfere with handlers.

    formatter = formatters.DebuggingFormatter()

    streamHandler = colorizedStreamHandler.ColorizedStreamHandler()
    streamHandler.setFormatter(formatter)
    streamHandler.setLevel(streamLevel)
    logger.addHandler(streamHandler)

    logDir = os.path.dirname(logPath)
    if not os.path.exists(logDir):
        os.makedirs(logDir)
    fileHandler = plog.PlogFileHandler(logPath)
    fileHandler.setLevel(fileLevel)
    logger.addHandler(fileHandler)

    logger.info("Log file: '{}'".format(logPath))

    return logger


def perTool(toolName, logPath, streamLevel=logging.INFO, fileLevel=logging.DEBUG):
    """
    Setup the logging for a single logger object in a way that allows each logger to be setup with its own handlers.

    Warnings:
        * If you use this, configure a logger and handlers for each tool - DO NOT add handlers to the root logger as you
        will likely end up with each logging call being handled more than once.

    Args:
        toolName (str): The name of the tool to be used as the logger name.
        logPath (str): The file path for the log file.
        streamLevel (int, optional): The logging level for the stream/console handler. Defaults to logging.INFO.
        fileLevel (int, optional): The logging level for the file handler. Defaults to logging.DEBUG.

    Returns:
        logging.Logger: Logger created for the tool.
    """
    if not plog.LoggingModuleSetup.isReady():
        plog.LoggingModuleSetup.run()

    logger = logging.getLogger(toolName)

    logger.setLevel(min((streamLevel, fileLevel)))  # Set to the lowest level, so it doesn't interfere with handlers.
    logger.propagate = False

    formatter = formatters.AlignedMessagesFormatter()
    streamHandler = colorizedStreamHandler.ColorizedStreamHandler()
    streamHandler.setFormatter(formatter)
    streamHandler.setLevel(streamLevel)
    logger.addHandler(streamHandler)

    logDir = os.path.dirname(logPath)
    if not os.path.exists(logDir):
        os.makedirs(logDir)
    fileHandler = plog.PlogFileHandler(logPath)
    fileHandler.setLevel(fileLevel)
    logger.addHandler(fileHandler)

    logger.info("Log file: '{}'".format(logPath))

    return logger


def commandLineApplication(appName, streamLevel=logging.INFO, fileLevel=logging.DEBUG, logPath=None):
    """
    Setup a logger to use the colorized stream handler, the aligned messages formatter, and the plog file handler.

    This logging configuration is designed for more production oriented command line tools.

    Args:
        appName (str): The application name.
        streamLevel (int, optional): The logging level to apply to the stream handler. Defaults to logging.INFO.
        fileLevel (int, optional): The logging level to apply to the file handler. Defaults to logging.DEBUG.
        logPath (logging.Logger, optional): The logger to setup. If None, the path will be set to use:
            "$HOME/Rockstar/$AppName/$AppName_YYMMDD-HHMMSS.plog". Defaults to None.

    Returns:
        logging.Logger: The root logger.
    """
    if logPath is None:
        timestamp = datetime.datetime.now().strftime("%y%m%d-%H%M%S")
        logDir = pathlib.Path.home().joinpath("Rockstar", appName, "Logs")
        logPath = os.path.join(str(logDir), "{}_{}.plog".format(appName, timestamp))

    if not plog.LoggingModuleSetup.isReady():
        plog.LoggingModuleSetup.run()

    logger = logging.getLogger()  # Root logger.

    logger.setLevel(min((streamLevel, fileLevel)))  # Set to the lowest level, so it doesn't interfere with handlers.

    formatter = formatters.AlignedMessagesFormatter()

    streamHandler = colorizedStreamHandler.ColorizedStreamHandler()
    streamHandler.setFormatter(formatter)
    streamHandler.setLevel(streamLevel)
    logger.addHandler(streamHandler)

    logDir = os.path.dirname(logPath)
    if not os.path.exists(logDir):
        os.makedirs(logDir)
    fileHandler = plog.PlogFileHandler(logPath)
    fileHandler.setLevel(fileLevel)
    logger.addHandler(fileHandler)

    logger.info("Log file: '{}'".format(logPath))

    return logger


if __name__ == "__main__":
    from rockstar.animData import context

    # # Demo: Console setup
    loggerRoot = console(level=logging.INFO)
    loggerApp = logging.getLogger("application")
    context.animData.getAllProjects()
    loggerRoot.profile("main")
    loggerRoot.info("main")
    loggerApp.profile("app")
    loggerApp.info("app")

    # Demo: Console debugging setup
    # loggerRoot = consoleDebugging()
    # loggerApp = logging.getLogger("application")
    # context.animData.getAllProjects()
    # loggerRoot.debug("main")
    # loggerRoot.profile("main")
    # loggerRoot.info("main")
    # loggerApp.debug("app")
    # loggerApp.profile("app")
    # loggerApp.info("app")

    # # Demo: Per tool setup
    # toolName1 = "TestTool1"
    # logger1 = perTool(toolName1, os.path.join(config.system.tempDir, "perTool_{}.plog".format(toolName1)), streamLevel=logging.INFO)
    # logger1.debug("TestTool1")
    # logger1.info("TestTool1")
    # logger1.error("TestTool1")
    # logger1.info("TestTool1")
    # toolName2 = "TestTool2"
    # logger2 = perTool(toolName2, os.path.join(config.system.tempDir, "perTool_{}.plog".format(toolName2)), streamLevel=logging.INFO)
    # logger2.debug("TestTool2")
    # logger2.info("TestTool2")
    # logger2.error("TestTool2")
    # logger2.info("TestTool2")
