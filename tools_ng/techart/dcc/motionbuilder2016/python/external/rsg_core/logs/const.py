"""
Constants for the log file service.
"""


class PrimaryFields(object):
    """
    Enum like class for the primary MongoDB document field names.
    """
    PROJECT = "project"
    OS = "os"
    USER = "user"
    HOST = "host"
    STUDIO = "studio"
    CREATION_TIME = "creation_time"
    LOG_DATA = "log_data"
    LOG_FILENAME = "log_filename"
    APP_NAME = "app_name"
