"""
A module for plog related utilities.
"""
from __future__ import division  # This allows the division operation to return a float instead of an int.
import logging
import datetime
import re

from dateutil import tz
import enum


class LogLevels(enum.Enum):
    """
    Enum for plog logging levels.

    Python standard logging levels:
        0: 'NOTSET'
        10: 'DEBUG'
        20: 'INFO'
        30: 'WARNING'
        40: 'ERROR'
        50: 'CRITICAL'

    See Also:
        https://hub.gametools.dev/display/RSGTOOLS/Log+Message+Levels
    """
    DEBUG = logging.DEBUG  # 10
    TRACE = 11
    PROFILE = 12
    INFO = logging.INFO  # 20
    WARNING = logging.WARNING  # 30
    ERROR = logging.ERROR  # 40
    TOOL_ERROR = 41
    TOOL_EXCEPTION = 42


class Timestamp(object):
    """
    Class that holds simple functions to convert between datetime objects and ticks.

    Examples:
        # Get plog compatible timestamp from a datetime.
        >>> plog.Timestamp.toTicks(datetime.datetime.utcnow())
        636858884267130112L
        # Get a datetime from a plog timestamp.
        >>> plog.Timestamp.toDateTime(636858499284020480)
        datetime.datetime(2019, 2, 15, 17, 52, 8, 402048)
        >>> plog.Timestamp.toDateTime(636858499284020480, tzNaive=False)
        datetime.datetime(2019, 2, 15, 17, 52, 8, 402048, tzinfo=tzutc())

    See Also:
        https://stackoverflow.com/questions/29366914/what-is-python-equivalent-of-cs-system-datetime-ticks
    """

    @staticmethod
    def toTicks(datetime_):
        """
        Convert a datetime object to ticks.

        Args:
            datetime_ (datetime.datetime): Timezone naive datetime in UTC.

        Returns:
            long
        """
        return long((datetime_ - datetime.datetime(1, 1, 1)).total_seconds() * 10000000)

    @staticmethod
    def toDateTime(ticks, local=False, tzNaive=True):
        """
        Convert the ticks to a datetime.datetime object.

        Args:
            ticks (long|int): The timestamp as ticks.

        Keyword Args:
            local (bool): If True, the datetime will be in the local time, else UTC.
            tzNaive (bool): If True, the datetime will be timezone unaware.

        Returns:
            datetime.datetime: A datetime representing the input timestamp.
        """
        utcDt = datetime.datetime(1, 1, 1) + datetime.timedelta(microseconds=ticks/10)
        if local is False and tzNaive is True:
            return utcDt

        utcTz = tz.tzutc()
        utcDt = utcDt.replace(tzinfo=utcTz)
        if local is False and tzNaive is False:
            return utcDt

        localTz = tz.tzlocal()
        localDt = utcDt.astimezone(localTz)
        if tzNaive is True:
            localDt = localDt.replace(tzinfo=None)
        return localDt


class LoggingModuleSetup(object):
    """
    Convenience methods for appending the log levels and logging calls required for the .plog format.
    """
    PLOG_LEVELS_TO_ADD = {
        LogLevels.PROFILE,  # This is currently the only extra level our codebase requires.
        LogLevels.TRACE,  # Add later if requested.
        LogLevels.TOOL_ERROR,  # Add later if requested.
        LogLevels.TOOL_EXCEPTION  # Add later if requested.
    }

    @classmethod
    def isReady(cls):
        try:
            for level in cls.PLOG_LEVELS_TO_ADD:
                getattr(logging, level.name)
            return True
        except AttributeError:
            return False

    @staticmethod
    def getMethodName(levelName):
        # Method naming is based on the camelcase naming convention.
        methodName = levelName.lower()
        if "_" in methodName:
            methodName = "".join([methodName.split("_")[0], methodName.split("_")[1].capitalize()])
        return methodName

    @classmethod
    def _addLoggingLevel(cls, levelName, levelValue, methodName=None):
        """
        Adds a new logging level to the `logging` module and the currently configured logging class.

        Examples:
            >>> _addLoggingLevel("PROFILE", 11)
            >>> logging.profile("profile")
            >>> logging.PROFILE
            11

        Args:
            levelName (str): The name for the new attribute on the `logging` module.
            levelValue (int): The value for the new attribute.

        Keyword Args:
             methodName (str): The name for the new convenience method to be added to both the `logging` module and the
                class returned by `logging.getLoggerClass()` (usually just `logging.Logger`).

        Raises:
            AttributeError: If either the new attribute on the `logging` module or if the method name already exist.

        See Also:
            * http://stackoverflow.com/q/2183233/2988730
            * http://stackoverflow.com/a/13638084/2988730
        """
        if methodName is None:
            methodName = cls.getMethodName(levelName)

        # Validation.
        if hasattr(logging, levelName):
            raise AttributeError("{} attribute is already defined on the logging module!".format(levelName))
        if hasattr(logging, methodName):
            raise AttributeError("{} method is already defined on the logging module!".format(methodName))
        if hasattr(logging.getLoggerClass(), methodName):
            raise AttributeError("{} method is already defined on the logger class!".format(methodName))

        # Generate new funcs for classes.
        def logForLevel(self, message, *args, **kwargs):
            if self.isEnabledFor(levelValue):
                self._log(levelValue, message, args, **kwargs)  # Note that the api doesn't use *args.

        def logToRoot(message, *args, **kwargs):
            logging.log(levelValue, message, *args, **kwargs)

        # Add new attr and method.
        logging.addLevelName(levelValue, levelName)
        setattr(logging, levelName, levelValue)
        setattr(logging.getLoggerClass(), methodName, logForLevel)
        setattr(logging, methodName, logToRoot)

    @classmethod
    def run(cls):
        """
        Configures the Python logging module for use with ulog/plog levels.

        See Also:
            https://hub.gametools.dev/display/RSGTOOLS/Log+Message+Levels
        """
        for level in cls.PLOG_LEVELS_TO_ADD:
            try:
                cls._addLoggingLevel(level.name, level.value)
            except AttributeError:
                pass


class PlogFormatter(logging.Formatter):
    """
    Formatter for use with Python standard logging handlers that formats a message in the plog format.
    """
    LEVEL_VALUE_TO_PLOG_ENUM_MAPPINGS = {
        logging.DEBUG: LogLevels.DEBUG,
        LogLevels.TRACE.value: LogLevels.TRACE,
        LogLevels.PROFILE.value: LogLevels.PROFILE,
        logging.INFO: LogLevels.INFO,
        logging.WARNING: LogLevels.WARNING,
        logging.ERROR: LogLevels.ERROR,
        LogLevels.TOOL_ERROR.value: LogLevels.TOOL_ERROR,
        logging.CRITICAL: LogLevels.TOOL_EXCEPTION,  # There's no equivalent to CRITICAL by default.
        LogLevels.TOOL_EXCEPTION.value: LogLevels.TOOL_EXCEPTION,
    }

    def __init__(self, fmt=None, datefmt=None):
        super(PlogFormatter, self).__init__(fmt=fmt, datefmt=datefmt)
        self.levelValueToPlogFormattedNameMappings = {
            logging.DEBUG: self.formatLevelNameForFile(LogLevels.DEBUG),
            LogLevels.TRACE.value: self.formatLevelNameForFile(LogLevels.TRACE),
            LogLevels.PROFILE.value: self.formatLevelNameForFile(LogLevels.PROFILE),
            logging.INFO: self.formatLevelNameForFile(LogLevels.INFO),
            logging.WARNING: self.formatLevelNameForFile(LogLevels.WARNING),
            logging.ERROR: self.formatLevelNameForFile(LogLevels.ERROR),
            LogLevels.TOOL_ERROR.value: self.formatLevelNameForFile(LogLevels.TOOL_ERROR),
            logging.CRITICAL: self.formatLevelNameForFile(LogLevels.TOOL_EXCEPTION),
            LogLevels.TOOL_EXCEPTION.value: self.formatLevelNameForFile(LogLevels.TOOL_EXCEPTION),
        }

    @staticmethod
    def formatLevelNameForFile(logLevel):
        return "".join([token.capitalize() for token in logLevel.name.split("_")])

    def format(self, record):
        """
        Reimplemented from logging.Formatter.

        Notes:
            The plog level names are Pascal case, i.e. ToolException.

        Args:
            record (logging.LogRecord): The log record.

        Returns:
            str
        """
        localDt = datetime.datetime.fromtimestamp(record.created, tz=tz.tzlocal())  # "created" is a time.time() result.
        utcDt = localDt.astimezone(tz.tzutc())  # Convert to utc.
        utcDtTzNaive = utcDt.replace(tzinfo=None)
        currentTimeAsTicks = Timestamp.toTicks(utcDtTzNaive)
        plogLevelName = self.levelValueToPlogFormattedNameMappings.get(record.levelno)
        return "[{0}] {1}: {2}".format(currentTimeAsTicks, plogLevelName, record.msg)


class PlogFileHandler(logging.FileHandler):
    """
    A handler class which writes formatted logging records to disk files.
    """

    def __init__(self, filename, mode="a", encoding=None, delay=False, level=logging.DEBUG):
        """
        Reimplemented from logging.FileHandler.

        Args:
            filename (str): The specified file is opened and used as the stream for logging. By default,
                the file grows indefinitely.

        Keyword Args:
            mode (str): The mode to pass into the file open call; Default: "a".
            encoding (str): Encoding used to open the file with; Default: None
            delay (bool): If delay is True, then file opening is deferred until the first call to emit().
            level (int): Logging level to use (e.g. logging.DEBUG etc).
        """
        super(PlogFileHandler, self).__init__(filename, mode=mode, encoding=encoding, delay=delay)
        self.setFormatter(PlogFormatter())
        self.setLevel(level)


class PlogParsingError(Exception):
    """
    Raise when there is an error parsing a line in a .plog file.
    """


class PlogFileReader(object):
    """
    File reader for .plog files.
    """
    DEFAULT_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

    def __init__(self, path):
        self.path = path

    @staticmethod
    def parseLine(line):
        result = re.match("\[(\d*)\] ([a-zA-Z]*): (.*)", line)
        if result is None:
            raise PlogParsingError("Could not parse line: '{}'".format(line))
        ticks, plogLevelName, message = result.groups()
        return ticks, plogLevelName, message

    @classmethod
    def toHumanReadableDatetimes(cls, path, fmt=None):
        fmt = fmt or cls.DEFAULT_DATETIME_FORMAT
        with open(path, "r") as log:
            plogLines = log.readlines()
        lines = []
        for line in plogLines:
            ticks, plogLevelName, message = cls.parseLine(line)
            datetime_ = Timestamp.toDateTime(int(ticks), local=True)
            lines.append(line.replace(ticks, datetime_.strftime(fmt)).replace("\n", ""))
        return lines

    def printLog(self):
        """
        Utility for printing a plog formatted file in a console via python/ipython for quick debugging needs.

        Examples:
            >>> plog.PlogFileReader("/tmp/SomeApp.plog").printLog()
            [2019-02-01 22:54:12] Debug: TestMessage
            [2019-02-01 22:54:12] Info: TestMessage
            [2019-02-01 22:54:12] Warning: TestMessage
        """
        print(self.path)
        lines = self.toHumanReadableDatetimes(self.path)
        for line in lines:
            print(line)


if __name__ == '__main__':
    from rsg_core.logs import formatters
    from rsg_core_py.logs import colorizedStreamHandler
    LoggingModuleSetup.run()

    path = '/tmp/{}.plog'.format(datetime.datetime.now().strftime("%Y%m%d%H%M%S%f"))

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    sh = colorizedStreamHandler.ColorizedStreamHandler()
    sh.setFormatter(formatters.AlignedMessagesFormatter())
    sh.setLevel(logging.DEBUG)

    logger.addHandler(sh)

    fh = PlogFileHandler(path)
    logger.addHandler(fh)

    logger.debug("debug {}".format(datetime.datetime.now()))
    logger.trace("trace {}".format(datetime.datetime.now()))
    logger.profile("profile {}".format(datetime.datetime.now()))
    logger.info("info {}".format(datetime.datetime.now()))
    logger.warning("warning {}".format(datetime.datetime.now()))
    logger.error("error {}".format(datetime.datetime.now()))
    logger.toolError("toolsError {}".format(datetime.datetime.now()))
    logger.critical("critical {}".format(datetime.datetime.now()))
    logger.toolException("toolException {}".format(datetime.datetime.now()))

    import time;time.sleep(.1)  # Give the logging module enough time to flush.
    PlogFileReader(path).printLog()
