"""
A collection of formatters for use with Python standard logging handlers.
"""
import logging
import itertools


class DateFormats(object):
    # 2020-05-25 18:27:04,724; Logging module default; Sorts better, easier to read.
    LOGGING_DEFAULT = "%Y-%m-%d %H:%M:%S"
    # 05/25/2020 18:27:04,724; US user friendly.
    US_STANDARD = "%m/%d/%Y %H:%M:%S"


class AlignedMessagesFormatter(logging.Formatter):
    """
    Formatter that returns a message where the start of all messages are aligned.
    """

    def __init__(self, datefmt=None):
        """
        Initialize the formatter with specified format strings.

        Keyword Args:
            datefmt (str): The same formatting as you would pass to time.strftime; Default: ISO8601 format.
        """
        super(AlignedMessagesFormatter, self).__init__(datefmt=datefmt)
        # TODO must be better way to safely get names...
        # Note that the following line trims the long plog names, but leaves stdlib logging names alone.
        logLevelNames = [key[:8] for key in logging._levelNames.keys() if isinstance(key, str)]
        self.maxLevelNameLength = max(itertools.imap(len, logLevelNames))

    def format(self, record):
        """
        Reimplemented from logging.Formatter.

        Args:
            record (logging.LogRecord): The log record.

        Returns:
            str
        """
        messageToken = record.getMessage()
        timeToken = self.formatTime(record, self.datefmt)
        # Note that the following line trims the long plog names, but leaves stdlib logging names alone.
        return "{} {}: {}".format(timeToken, record.levelname[:8].ljust(self.maxLevelNameLength), messageToken)


class DebuggingFormatter(AlignedMessagesFormatter):
    """
    Formatter that returns a message where extra contextual info is included.

    Format:
        "$DATE_FORMAT $LOG_LEVEL_NAME $LOGGER_NAME,$THREAD_NAME,$MODULE_NAME,$FUNCTION_NAME,L$LINE_NUM: $MESSAGE"
    Sample output:
        "2020-05-26 13:50:20,575 DEBUG    MainApp, Dummy-22,dev_submitLinuxPythonTask,run,L118: Started"
    """

    def format(self, record):
        """
        Reimplemented from logging.Formatter.

        Notes:
            The stdlib logging module doesn't doesn't check for/include a function's class name; as a result, we must
            include the line number to cover use cases where we have multiple methods of the same name on different
            classes in the same module, i.e. "__init__".

        Args:
            record (logging.LogRecord): The log record.

        Returns:
            str
        """
        messageToken = record.getMessage()
        timeToken = self.formatTime(record, self.datefmt)
        return "{} {} {},{},{},{},L{}: {}".format(
            timeToken,
            record.levelname[:8].ljust(self.maxLevelNameLength),
            record.name,
            record.threadName,
            record.module,
            record.funcName,
            record.lineno,
            messageToken,
        )


if __name__ == "__main__":
    import time

    from rsg_core.logs import plog
    from rsg_core_py.logs import colorizedStreamHandler
    from rsg_core_py.contextManagers import profiler

    # Demo use with plog and colorizedStreamHandler.
    with profiler.Time("setup"):
        plog.LoggingModuleSetup.run()

    with profiler.Time("handlers"):
        handler = colorizedStreamHandler.ColorizedStreamHandler()
    with profiler.Time("formatter"):
        formatter = DebuggingFormatter(datefmt=DateFormats.US_STANDARD)
        handler.setFormatter(formatter)
    with profiler.Time("logger"):
        logger = logging.getLogger("LoggerName")  # Use __main__ in a library module.
        logger.addHandler(handler)
        logger.setLevel(logging.DEBUG)

    time.sleep(0.1)  # Give the profiler messages enough time to print.
    logger.debug("message")
    logger.trace("message")
    logger.profile("message")
    logger.info("message")
    logger.warning("message")
    logger.error("message")
    logger.toolError("message")
    logger.critical("message")
    logger.toolException("message")
