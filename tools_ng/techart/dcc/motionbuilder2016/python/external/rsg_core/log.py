"""
Module containing logging logic that uses the logging python library and custom solutions
"""
import os
import sys
import time
import uuid
from logging import config as logConfig
from itertools import imap

import colorama

from rsg_core.configs import main as config


class LogWriter(object):

    def __init__(self, logPath):
        """
        Constructor

        Arguments:
            logPath (string): place to save log file to
        """
        self._initLog(logPath)

    def _createLogFile(self):
        """ Creates the log file"""
        if not os.path.exists(os.path.dirname(self._logFile)):
            os.makedirs(os.path.dirname(self._logFile))

    def _initLog(self, logPath):
        """
        creates the log file at the specified location
        Arguments:
            logPath (string): place to save log file to
        """
        self._logFile = logPath
        self._createLogFile()

    def writeLog(self, logText):
        """
        writes out the log

        Arguments:
            logText (string): text to save out to log
        """
        with open(self._logFile, "a") as filestream:
            filestream.write(logText)


class Logger(object):
    def __init__(self, logPath):
        """
        constructor

        Arguments:
            logPath (string): path to save log out to
        """
        print((colorama.Style.BRIGHT))
        self.StatusTypes = {"Error": colorama.Fore.RED, "Info": colorama.Fore.GREEN, "Warn": colorama.Fore.YELLOW, "Debug": colorama.Fore.BLUE }
        self.enableDebugPrint = True

        self.buffer = max(imap(len, self.StatusTypes.keys()))
        self._timePadding = 18
        if config.system.isLinux:
            self._timePadding = 25

        self.logPath = logPath

        self.clearLogsBySize()

        self.logWriter = LogWriter(self.logPath)

    def enableDebugPrint(self, showDebug=True):
        """
        Set this if we want to output debug print commands if called
        Arguments:
            showDebug (bool):
        """
        self.enableDebugPrint = showDebug
        print((Back.LIGHTBLACK_EX))

    def constant(self, text):
        """
        Adds error text to the log

        Arguments:
            text (string): text to include in log
        """
        self._log(text, "Info", "")

    def error(self, text):
        """
        Adds error text to the log

        Arguments:
            text (string): text to include in log
        """
        self._log(text, "Error")

    def warning(self, text):
        """
        Adds warning text to the log

        Arguments:
            text (string): text to include in log
        """
        self._log(text, "Warn")

    def info(self, text):
        """
        Adds info text to the log

        Arguments:
            text (string): text to include in log
        """
        self._log(text)

    def debug(self, text):
        """
        Adds debug text to the log

        Arguments:
            text (string): text to include in log
        """
        if self.enableDebugPrint:
            self._log(text, "Debug")

    def _log(self, text,  statusTxt="Info", line="\n"):
        """
        Adds text to the log

        Arguments:
            text (string): text to log
            statusTxt (string): status of the text
            line(string)
        """
        padding = (self.buffer-len(statusTxt)+self._timePadding)
        status = "".join([colorama.Style.BRIGHT , self.StatusTypes[statusTxt], statusTxt, colorama.Style.NORMAL, colorama.Fore.WHITE])
        timestamp = "{}[{}]".format(time.strftime("%c"), status).ljust(padding)

        logTxt = ("{0}{1}{2}\r{3} {4}\r".format(colorama.Style.NORMAL, colorama.Fore.WHITE, line, timestamp, str(text)))
        sys.stdout.write(logTxt)
        if line != "":
            self.logWriter.writeLog("\n{0}[]".format(time.strftime("%c")).ljust(padding+1) +"[" +  statusTxt + "] " + str(text) )

    def clearLogsBySize(self, target_maxsize=2048):
        """
        Will remove log files under the target max size

        Arguments:
            target_maxsize (int): minimum size a file must be to not be removed
        """
        logBasePath = os.path.dirname(self.logPath)
        if os.path.exists(logBasePath):
            for file in os.listdir(logBasePath):
                path = os.path.join(logBasePath, file)
                if os.stat(path).st_size < target_maxsize:
                    os.remove(path)


def configure(name, toolRootDirectory):
    """
    Configures the settings for the standard python logging library

    Arguments:
        name (string): basename for the file to write output to.
                       The time the config was created will be added to the filename.
        toolRootDirectory (string): path to the root directory for the tool being logged

    Returns:
        (str, str): (config path, log path)
    """
    localTime = time.localtime()
    defaultConfigPath = os.path.join(os.path.dirname(__file__), "resources", "configs", "logging.conf")
    timestamp = "{year}{month}{day}{hour}{minutes}{seconds}".format(year=localTime.tm_year, month=localTime.tm_mon,
                                                                day=localTime.tm_mday, hour=localTime.tm_hour,
                                                                minutes=localTime.tm_min, seconds=localTime.tm_sec)
    toolRootDirectoryName = os.path.basename(toolRootDirectory)
    logDirectory = os.path.join(config.system.environment.techartProject.logs, toolRootDirectoryName)
    logPath = os.path.join(config.system.environment.techartProject.logs, toolRootDirectoryName, "{name}.{timestamp}.log".format(name=name, timestamp=timestamp))
    if not os.path.exists(logDirectory):
        os.makedirs(logDirectory)

    configPath = os.path.join(toolRootDirectory, "{}.conf".format(toolRootDirectoryName))
    with open(defaultConfigPath, "r") as configHandle:
        configContents = configHandle.read()
    with open(configPath, "w") as configHandle:
        configHandle.write(configContents.format(logpath=logPath))

    logConfig.fileConfig(configPath)

    return (configPath, logPath)

