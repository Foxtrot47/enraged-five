import unittest
import datetime

from pytz import reference

from rsg_core import conversions


class TestDatetime(unittest.TestCase):
    IST_DATETIME = datetime.datetime(2019, 11, 11, 23, 30, 0)
    UTC_DATETIME = datetime.datetime(2019, 11, 11, 18, 0, 0)
    EST_DATETIME = datetime.datetime(2019, 11, 11, 13, 0, 0)
    GMT_DATETIME = datetime.datetime(2019, 11, 11, 18, 0, 0)
    PST_DATETIME = datetime.datetime(2019, 11, 11, 10, 0, 0)

    def _getLocalTzDatetime(self):
        localTzName = reference.LocalTimezone().tzname(datetime.datetime.now())
        if localTzName in ["PDT", "PST", "Pacific Standard Time", "Pacific Daylight Time"]:
            return self.PST_DATETIME
        elif localTzName in ["EDT", "EST", "Eastern Standard Time", "Eastern Daylight Time"]:
            return self.EST_DATETIME
        elif localTzName in ["GMT", "GMT", "GMT Standard Time", "GMT Daylight Time"]:
            return self.GMT_DATETIME
        elif localTzName in ["BST", "BST", "British Summer Time"]:
            return self.GMT_DATETIME
        else:
            raise ValueError("Add the missing info for your timezone ({})!".format(localTzName))

    def test_toUtc(self):
        localDatetime = self._getLocalTzDatetime()
        expectedResult = self.UTC_DATETIME
        self.assertEqual(expectedResult, conversions.Datetime.toUtc(localDatetime))

    def test_toLocal(self):
        expectedResult = self._getLocalTzDatetime()
        self.assertEqual(expectedResult, conversions.Datetime.toLocal(self.UTC_DATETIME))
