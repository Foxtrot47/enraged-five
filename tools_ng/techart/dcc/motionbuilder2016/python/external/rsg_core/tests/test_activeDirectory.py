"""
These tests do no require MotionBuilder and should be able to be run from your
IDE (PyCharm, Eclipse, etc.) using the default tooling.
"""
import unittest

from rsg_core import activeDirectory


class TestDirectoryServices(unittest.TestCase):
    SERVICE_ACCOUNTS = {
        "svcrsgsanshoot": "svcrsgsanshoot@rockstarsandiego.com",
        "svcrsgtorshoot": "svcrsgtorshoot@rockstartoronto.com",
        "svcrsgedishoot": "svcrsgedigiant@rockstarnorth.com",
        "svcrsglicshoot": "svcrsglicshoot@rockstargames.com",
        "svcrsgnycvmx": "svcrsgnycvmx@rockstargames.com",
    }

    def test_getSearchFilterFromUser(self):
        ads = activeDirectory.DirectoryServices()
        for user in self.SERVICE_ACCOUNTS:
            self.assertIsInstance(ads.getSearchFilterFromUser(user=user), str)

    def test_getEmailAddress(self):
        ads = activeDirectory.DirectoryServices()

        # Default arg.
        self.assertIsInstance(ads.getEmailAddress(), unicode)

        # Check known, critical users.
        for user, email in self.SERVICE_ACCOUNTS.items():
            self.assertEqual(email, ads.getEmailAddress(user=user))

    def test_getUserPhoto(self):
        ads = activeDirectory.DirectoryServices()

        # Human users should return photo data.
        self.assertIsInstance(ads.getEmailAddress(), unicode)

        # Service accounts have no photo data.
        for user in self.SERVICE_ACCOUNTS:
            self.assertIsNone(ads.getUserPhoto(user=user))

    def test_getUserGroups(self):
        ads = activeDirectory.DirectoryServices()

        # Default arg.
        defaultArgResult = ads.getUserGroups()
        self.assertIsInstance(defaultArgResult, list)
        self.assertNotEqual([], defaultArgResult)

        # Service accounts should all return lists.
        for user in self.SERVICE_ACCOUNTS:
            result = ads.getUserGroups(user=user)
            self.assertIsInstance(result, list)
            self.assertNotEqual([], result)

    def test_getFullName(self):
        ads = activeDirectory.DirectoryServices()

        # Default arg.
        self.assertIsInstance(ads.getFullName(), unicode)

        # Check known, critical users.
        for user, email in self.SERVICE_ACCOUNTS.items():
            self.assertEqual(user, ads.getFullName(user=user))
