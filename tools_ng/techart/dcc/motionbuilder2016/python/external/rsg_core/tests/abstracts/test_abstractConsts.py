import unittest
from rsg_core_py.abstracts import abstractConsts


class TestAbstractConsts(unittest.TestCase):

    def test_auto(self):
        # Doesnt guarantee order, so you cant predict and test the value
        class ExampleConst(abstractConsts.AbstractConsts):
            DOG = abstractConsts.Auto()
            BIRD = abstractConsts.Auto()
            CAT = abstractConsts.Auto()
            TURTLE = abstractConsts.Auto()
            _CATDOG = "catDog"
            def exampleMethod(self):
                return 1
        self.assertIsInstance(ExampleConst.DOG, int)
        self.assertIsInstance(ExampleConst.BIRD, int)
        self.assertIsInstance(ExampleConst.CAT, int)
        self.assertIsInstance(ExampleConst.TURTLE, int)

        self.assertEqual((0, 1, 2, 3), tuple(ExampleConst))

    def test_strValues(self):
        dog = "dog"
        bird = "bird"
        cat = "cat"
        turtle = "turtle"
        _CATDOG = "catDog"

        class ExampleConst(abstractConsts.AbstractConsts):
            DOG = dog
            BIRD = bird
            CAT = cat
            TURTLE = turtle
            def exampleMethod(self):
                return 1

        self.assertEqual(dog, ExampleConst.DOG)
        self.assertEqual(bird, ExampleConst.BIRD)
        self.assertEqual(cat, ExampleConst.CAT)
        self.assertEqual(turtle, ExampleConst.TURTLE)

        # Values are sorted
        self.assertEqual((bird, cat, dog, turtle), tuple(ExampleConst))

    def test_iter(self):
        class ExampleConst(abstractConsts.AbstractConsts):
            DOG = abstractConsts.Auto()
            BIRD = abstractConsts.Auto()
            CAT = abstractConsts.Auto()
            TURTLE = abstractConsts.Auto()
            _CATDOG = "catDog"
            def exampleMethod(self):
                return 1

        exConstIter = iter(ExampleConst)
        self.assertEqual(0, next(exConstIter))
        self.assertEqual(1, next(exConstIter))
        self.assertEqual(2, next(exConstIter))
        self.assertEqual(3, next(exConstIter))
        self.assertRaises(StopIteration, next, exConstIter)

    def test_attrNameByValueInt(self):
        class ExampleConst(abstractConsts.AbstractConsts):
            DOG = 0
            BIRD = 1
            CAT = 2
            TURTLE = 3
            _CATDOG = "catDog"
            def exampleMethod(self):
                return 1

        exConstIter = iter(ExampleConst)
        self.assertEqual("DOG", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("BIRD", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("CAT", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("TURTLE", ExampleConst.getAttrNameByValue(next(exConstIter)))

        self.assertRaises(StopIteration, next, exConstIter)

    def test_attrNameByValueStr(self):
        class ExampleConst(abstractConsts.AbstractConsts):
            DOG = "0"
            BIRD = "1"
            CAT = "2"
            TURTLE = "3"
            _CATDOG = "catDog"
            def exampleMethod(self):
                return 1

        exConstIter = iter(ExampleConst)
        self.assertEqual("DOG", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("BIRD", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("CAT", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("TURTLE", ExampleConst.getAttrNameByValue(next(exConstIter)))

        self.assertRaises(StopIteration, next, exConstIter)

    def test_attrNameByValueList(self):
        class ExampleConst(abstractConsts.AbstractConsts):
            DOG = [0, 1]
            BIRD = [2, 3]
            CAT = [4, 5]
            TURTLE = [6, 7]
            _CATDOG = "catDog"
            def exampleMethod(self):
                return 1

        exConstIter = iter(ExampleConst)
        self.assertEqual("DOG", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("BIRD", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("CAT", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("TURTLE", ExampleConst.getAttrNameByValue(next(exConstIter)))

        self.assertRaises(StopIteration, next, exConstIter)

    def test_attrNameByValueDict(self):
        class ExampleConst(abstractConsts.AbstractConsts):
            DOG = {0: 0}
            BIRD = {1: 1}
            CAT = {2: 2}
            TURTLE = {3: {3}}
            _CATDOG = "catDog"
            def exampleMethod(self):
                return 1

        exConstIter = iter(ExampleConst)
        self.assertEqual("DOG", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("BIRD", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("CAT", ExampleConst.getAttrNameByValue(next(exConstIter)))
        self.assertEqual("TURTLE", ExampleConst.getAttrNameByValue(next(exConstIter)))

        self.assertRaises(StopIteration, next, exConstIter)
