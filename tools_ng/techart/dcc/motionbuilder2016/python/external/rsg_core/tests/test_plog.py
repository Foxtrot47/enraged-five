import unittest
import datetime
import logging
import re

from pytz import reference
from dateutil import tz

from rsg_core.logs import plog


class TestTimestamp(unittest.TestCase):
    EX1_TICKS = 636858499284020480

    EX1_INPUT_DT = datetime.datetime(2019, 2, 15, 17, 52, 8, 402048)
    EX1_OUTPUT_DT_UTC_TZ = datetime.datetime(2019, 2, 15, 17, 52, 8, 402048, tzinfo=tz.tzutc())
    EX1_OUTPUT_DT_UTC_NAIVE = datetime.datetime(2019, 2, 15, 17, 52, 8, 402048)

    # We can delete most of this code, once we have access to backports.zoneinfo + tzdata + tzlocal-3.x in Python-3.7.3.
    LOCAL_TZNAME_RAW = reference.LocalTimezone().tzname(datetime.datetime.now())
    TZNAME_MAPPINGS = {  # Map all to standard time, as that's all we care about in the test datetime.
        # Windows
        "GMT Standard Time": "GMT",
        "GMT Daylight Time": "GMT",
        "British Summer Time": "GMT",
        "Eastern Daylight Time": "EST",
        "Eastern Standard Time": "EST",
        "Pacific Daylight Time": "PST",
        "Pacific Standard Time": "PST",
        # Ubuntu
        "BST": "GMT",
        "EST": "EST",  # Pass value through.
        "EDT": "EST",
        "PST": "PST",  # Pass value through.
        "PDT": "PST",
    }
    LOCAL_TZNAME_RESOLVED = TZNAME_MAPPINGS[LOCAL_TZNAME_RAW]  # Will raise error if new support is required.
    TZNAME_TO_LOCALTZ_MAPPINGS = {
        "GMT": datetime.datetime(2019, 2, 15, 17, 52, 8, 402048, tzinfo=tz.tzlocal()),
        "EST": datetime.datetime(2019, 2, 15, 12, 52, 8, 402048, tzinfo=tz.tzlocal()),
        "PST": datetime.datetime(2019, 2, 15, 9, 52, 8, 402048, tzinfo=tz.tzlocal()),
    }
    EX1_OUTPUT_DT_LOCAL_TZ = TZNAME_TO_LOCALTZ_MAPPINGS.get(LOCAL_TZNAME_RESOLVED)
    TZNAME_TO_LOCALNAIVE_MAPPINGS = {
        "GMT": datetime.datetime(2019, 2, 15, 17, 52, 8, 402048),
        "EST": datetime.datetime(2019, 2, 15, 12, 52, 8, 402048),
        "PST": datetime.datetime(2019, 2, 15, 9, 52, 8, 402048),
    }
    EX1_OUTPUT_DT_LOCAL_NAIVE = TZNAME_TO_LOCALNAIVE_MAPPINGS.get(LOCAL_TZNAME_RESOLVED)

    def test_toTicks(self):
        self.assertEqual(self.EX1_TICKS, plog.Timestamp.toTicks(self.EX1_INPUT_DT))

    def test_toDateTime(self):
        self.assertEqual(self.EX1_OUTPUT_DT_UTC_NAIVE, plog.Timestamp.toDateTime(self.EX1_TICKS))

        self.assertEqual(self.EX1_OUTPUT_DT_UTC_NAIVE, plog.Timestamp.toDateTime(self.EX1_TICKS, local=False))
        self.assertEqual(self.EX1_OUTPUT_DT_UTC_TZ, plog.Timestamp.toDateTime(self.EX1_TICKS, local=False, tzNaive=False))

        self.assertEqual(self.EX1_OUTPUT_DT_LOCAL_NAIVE, plog.Timestamp.toDateTime(self.EX1_TICKS, local=True))
        self.assertEqual(self.EX1_OUTPUT_DT_LOCAL_TZ, plog.Timestamp.toDateTime(self.EX1_TICKS, local=True, tzNaive=False))


class TestAddLoggingLevels(unittest.TestCase):

    def test_configuring(self):
        """
        This test should not be run with others, as it modifies the Python standard logger.
        """
        self.assertRaises(AttributeError, lambda: logging.PROFILE)

        plog.LoggingModuleSetup.run()
        for level in plog.LogLevels:
            self.assertTrue(logging._levelNames.get(level.name) == level.value)


class TestPlogFormatter(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if not plog.LoggingModuleSetup.isReady():
            plog.LoggingModuleSetup.run()

    def test_format(self):
        messageText = "Message text - 34234!"
        record = logging.LogRecord("test", logging.DEBUG, "/tmp/srcFile.py", 5, messageText, [], None)
        formattedMessage = plog.PlogFormatter().format(record)

        result = re.match("\[(\d*)\] ([a-zA-Z]*): (.*)", formattedMessage)
        self.assertIsNotNone(result, "Message is formatted incorrectly!")

        ticks, logLevel, message = result.groups()
        self.assertEqual(messageText, message)
        self.assertEqual("Debug", logLevel)
        self.assertTrue(ticks.isdigit())

        # TODO check the conversion from ticks back to record.created.
