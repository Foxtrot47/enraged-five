"""
Module for interacting with PyCharm Remote Debugger
"""
import socket

import pydevd
import pydevd_pycharm


class RemoteDebugger(object):
    """
    Class for interacting with the debugger
    """
    HOST = "localhost"
    PORT = 21100  # The port we use for the pycharm debugger

    @classmethod
    def isPortOpen(cls):
        """
        Check if the port is open

        Return:
            bool
        """
        # Close the socket object we just used for determining if the socket is open
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.bind((cls.HOST, cls.PORT))
            return False  # The socket is available to be bound, thus not in use by the remote debugger
        except:
            return True  # The socket is unavailable to be bound, thus it is already in use by the remote debugger
        finally:
            sock.close()

    @classmethod
    def connect(cls):
        """
        Connect to pycharm's remote debugger

        Returns:
            bool
        """
        if cls.isPortOpen():
            cls.breakpoint(silent=True)
            return True
        return False

    @classmethod
    def breakpoint(cls, silent=True):
        """
        The pydevd code to set a breakpoint in the debugger

        Arguments:
            silent (bool): Should this breakpoint actually be hit; intended for instances where only establishing a
                           connection to the debugger is required.
        """
        pydevd.stoptrace()  # Stop any existing pydev trace that may prevent a reconnect to the python server
        pydevd_pycharm.settrace(cls.HOST, port=cls.PORT, stdoutToServer=True, stderrToServer=True, suspend=silent is False)
