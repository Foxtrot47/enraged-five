"""
Module for sending emails
"""
import os
import sys
import collections
import smtplib
import traceback

from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

from rsg_core.configs import main as config


SUPPORTED_TEXT_ATTACHMENTS = ["xml", "ulog", "plog", "txt", "ms", "py", "ini", ]
SUPPORTED_IMAGE_ATTACHMENTS = ["png", "tga", "jpg", "bmp", ]
SUPPORTED_ATTACHMENTS = SUPPORTED_TEXT_ATTACHMENTS + SUPPORTED_IMAGE_ATTACHMENTS


class StderrDebug(object):
    """
    Tracks smtp behavior
    """
    def __init__(self):
        self._debug = []

    def write(self, message):
        self._debug.append(message.strip())

    def debug(self):
        return "\n".join(self._debug)


def send(emailFrom, emailTo, emailSubject, emailBody, asHtml=False, attachments=(), exchangeServer=None):
    """
    Sends an email using the exchange server found in the project module.

    Arguments:
        emailFrom (str): email address of the sender
        emailTo (list): list of email addresses to send email to
        emailSubject (str): the subject/title of the email
        emailBody (str): the content of the email
        asHtml (bool): add the content of the email as html
        attachments (list): list of paths to files to attach
        exchangeServer (str): server to use for sending the email
    """
    if not isinstance(emailTo, collections.Iterable) or isinstance(emailTo, basestring):
        emailTo = [emailTo]

    message = MIMEMultipart("alternative")
    message["Subject"] = "{0}".format(emailSubject)
    message["To"] = ";".join(emailTo)
    message["From"] = emailFrom

    body = MIMEText(emailBody, ("plain", "html")[asHtml])

    message.attach(body)

    for attachment in attachments:
        if not os.path.isfile(attachment):
            print("Could not attach file ({0}) because it does not exist!".format(attachment))
            continue

        fileType = os.path.splitext(attachment)[-1][1:].lower()
        if fileType in SUPPORTED_ATTACHMENTS:

            isImage = fileType in SUPPORTED_IMAGE_ATTACHMENTS

            part = MIMEBase("application", "octet-stream")
            with open(attachment, "rb"[:isImage + 1]) as attachmentFile:
                part.set_payload(attachmentFile.read())
            encoders.encode_base64(part)
            part.add_header("Content-Disposition", "attachment; filename={0}".format(os.path.basename(attachment)))
            message.attach(part)

            continue

        print(("The attachment file type ({0}) is not supported! "
               " You will need to add the file type as a supported type to this module.".format(fileType)))

    # Replace the stderr with ours to get the debug traceback
    success = False
    _stderr = sys.stderr# Route the error output as smtp library is printing out the output in the error channel.

    server = None
    try:
        debugStderr = StderrDebug()
        sys.stderr = debugStderr

        if not exchangeServer:
            exchangeServer = config.system.studio.exchangeServer

        server = smtplib.SMTP(exchangeServer)

        server.set_debuglevel(1)
        server.sendmail(emailFrom, emailTo, message.as_string())
        success = True
        server.quit()

    except smtplib.SMTPServerDisconnected:
        traceback.print_exception(*sys.exc_info())

    except Exception as _:
        if server is not None:
            server.quit()
        traceback.print_exception(*sys.exc_info())

    finally:
        sys.stderr = _stderr
        if not success:
            print(debugStderr.debug())
    return success, debugStderr.debug()
