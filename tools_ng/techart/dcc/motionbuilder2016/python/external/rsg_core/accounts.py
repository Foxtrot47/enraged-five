"""
Classes for retrieving information required for authenticating against the various services we use
"""
import getpass
import base64

from rsg_core import passwordState
from rsg_core.configs import main as config
from rsg_core_py.metaclasses import singleton
from rsg_core_py.exceptions import types as exceptions


class AbstractAccount(object):
    """ Abstract class for the account logic"""

    _promptForPassword = False

    name = None
    requiresDomain = True
    isApiKey = False
    promptDialog = None
    promptParent = None

    def __init__(self):
        """
        Constructor
        """
        self.raiseError = False

    def _setPassword(self, password):
        """
        Stores the password for future use.
        This should be reimplemented in the inherited classes if it is required.

        Args:
            password (str): encoded password to set
        """
        raise NotImplementedError

    def authenticate(self, instance, **kwargs):
        """
        This method needs to be reimplemented in inherited classes

        Passes the password to the object that needs to authenticate as the user.

        The object being passed in for authentication should have method called authenticate that accepts a parameter
        called password.

        The authenticate method of the instance being passed should return true or false

        Args:
            instance (object): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        raise NotImplementedError("This method needs to be reimplemented in inherited classes")

    def passwordPrompt(self, title=None):
        """
        Prompts the user for their password and returns if a new password was set.

        Args:
            title (str): title for the prompt to use

        Returns:
            bool
        """

        title = title or "Account Authentication"
        prompt = "Enter password for user {}".format(self.name)

        if AbstractAccount.promptDialog is not None:
            dialog = AbstractAccount.promptDialog(AbstractAccount.promptParent)
            dialog.setWindowTitle(title)
            dialog.setText(prompt)
            dialog.exec_()
            password, successful = dialog.result()
        else:
            raise exceptions.PerforceError(
                "Login to Perforce 1666 depot and 1777 depot if applicable. "
                "User {} is not logged in to Perforce.".format(self.name)
            )

        if successful:
            self._setPassword(base64.b64encode(password))
        return successful

    @property
    def promptForPassword(self):
        """Prompt the user for a password.

        Returns:
            bool
        """
        return AbstractAccount._promptForPassword

    @promptForPassword.setter
    def promptForPassword(self, value):
        """Set if the user should be prompted for a password.

        Returns:
            bool
        """
        AbstractAccount._promptForPassword = value

    @property
    def idapAddress(self):
        """
        Address required for authentication for services that authenticate against IDAP.
        Service accounts do not require this.

        Returns:
            str
        """
        if self.requiresDomain:
            return "{}@rockstar.t2.corp".format(self.name)
        return self.name


class DockerAccount(AbstractAccount):
    """
    Class that supports authentication using a secret provided by portainer, when the code is executed in a docker
    environment.
    """
    __metaclass__ = singleton.Singleton
    _secretPath = None

    def _passwordFromSecret(self):
        # We are running inside a docker image, so we expect the password to be in a specific location encrypted.
        secretPath = self._secretPath
        if not secretPath:
            raise Exception("Secret path wasn't set properly!")
        return open(secretPath).read()


class CurrentUser(AbstractAccount):
    """ Authenticate as the current user """
    __metaclass__ = singleton.Singleton
    name = config.system.user.name

    def authenticate(self, instance, **kwargs):
        """
        Passes the password to the object that needs to authenticate as the user.

        The object being passed in for authentication should have method called authenticate that accepts a parameter
        called password.

        The authenticate method of the instance being passed should return true or false

        Args:
            instance (object): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        # Note: This is not a good way to access passwords at all but is a necessary evil at this time
        return config.system.user.authenticate(instance, **kwargs)

    def _setPassword(self, password):
        """
        Re-implements inherited method.

        Stores the password for future use.

        Args:
            password (str): encoded password to set
        """
        config.system.user.setPassword(password)


class OtherUser(AbstractAccount):
    """
    Authenticate as a user who isn't the current user.
    Intended for debugging purposes and authenticating as non-human users (ei. script keys).
    """

    _instances = {}

    def __new__(cls, username, *args, **kwargs):
        """
        Overrides built-in method

        Stores & returns the instance of the class if an argument is provided, otherwise creates a new instance.

        Arguments:
            username (str): name of the user

        Returns:
            object
        """
        instance = cls._instances.get(username, None)
        if instance is None:
            instance = object.__new__(cls)
        return instance

    def __init__(self, username, password, isEncoded=False):
        """
        Constructor

        Args:
            username (str): username to authenticate as
            password (str): password for the user
            isEncoded (bool): is the password already encoded
        """
        if username is not None and username in self.__class__._instances:
            return
        elif username is not None:
            self.__class__._instances[username] = self

        super(OtherUser, self).__init__()
        self.name = username
        if not isEncoded:
            password = base64.b64encode(password)
        self.__password = password

    def authenticate(self, instance, **kwargs):
        """
        Passes the password to the object that needs to authenticate as the user.

        The object being passed in for authentication should have method called authenticate that accepts a parameter
        called password.

        The authenticate method of the instance being passed should return true or false

        Args:
            instance (object): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        return instance.authenticate(password=self.__password, **kwargs)

    def _setPassword(self, password):
        """
        Re-implements inherited method.

        Stores the password for future use.

        Args:
            password (str): encoded password to set
        """
        self.__password = password


class TechArt(DockerAccount):
    """
    Authenticate as the general techart service account.
    Intended for accessing accessing services that do not or have limited support for standard user accounts.
    """
    __metaclass__ = singleton.Singleton
    name = "svcrsgnyctechart"
    requiresDomain = True
    promptForPassword = False
    _secretPath = config.system.environment.techartSecret

    def authenticate(self, instance, **kwargs):
        """
        Reimplemented from the inherited class.

        Args:
            instance (object): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        # TODO: bugstar://7335399 Remove inDocker environment variable
        if config.system.environment.inDocker:
            return instance.authenticate(password=self._passwordFromSecret(), **kwargs)
        return instance.authenticate(password="NTUzcnYxYzNTUw==", **kwargs)


class TechArtAutomation(DockerAccount):
    """
    Authenticate as the TechArt Automation service account.
    The intended use for this account is for running automated jobs.
    """

    __metaclass__ = singleton.Singleton
    name = "svcrsgtectaa"
    requiresDomain = False
    promptForPassword = False

    def authenticate(self, instance, **kwargs):
        """
        Reimplemented from the inherited class.

        Accesses the password from password state. This will error out and fail if the current machine AND user does not
        have permissions to retrieve the password from password state.

        Args:
            instance (object): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.

        Raises:
            OSError: This method is only supported for windows at this time
            ValueError: No values are returned from password state
            ConnectionError: Error returned by the REST call

        Returns:
            bool
        """
        return instance.authenticate(password=passwordState.password(29232), **kwargs)


class Shotgun(DockerAccount):
    """
    Authenticate as the shotgun service account.
    The purpose of this account is for bridging changes between bugstar and shotgun that require user delegation.
    """
    __metaclass__ = singleton.Singleton
    name = "svcrsgtecshotbug"
    _secretPath = config.system.environment.shotgunSecret
    promptForPassword = False

    def authenticate(self, instance, **kwargs):
        """
        Reimplemented from the inherited class.

        Accesses the password from password state. This will error out and fail if the current machine AND user does not
        have permissions to retrieve the password from password state.
        Args:
            instance (object): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.

        Raises:
            OSError: This method is only supported for windows at this time
            ValueError: No values are returned from password state
            ConnectionError: Error returned by the REST call

        Returns:
            bool
        """
        # TODO: bugstar://7335399 Remove inDocker environment variable
        if config.system.environment.inDocker:
            return instance.authenticate(password=self._passwordFromSecret(), **kwargs)

        return instance.authenticate(password=passwordState.password(29221), **kwargs)


def setPromptForPassword(value):
    """Convenience method for setting if the user should be prompted to insert a password for accounts that use the CurrentUser and OtherUser class.

    Args:
        value (bool): should CurrentUser and OtherUser accounts raise a prompt asking for password when a password fails.
    """
    AbstractAccount._promptForPassword = value


def setPasswordPrompt(dialog, parent=None):
    """Convenience method for setting the Qt Dialog to use as the password prompt.

    Args:
        dialog (PySide2.QWidgets.QDialog): The class for the dialog to use for asking for the users password
        parent (PySide2.QWidgets.QWidget): The parent widget for the password prompt
    """
    AbstractAccount.promptDialog = dialog
    AbstractAccount.promptParent = parent


def setPasswordPromptParent(parent=None):
    """Convenience method for setting the parent of the the password prompt.

    Args:
        parent (PySide2.QWidgets.QWidget): The parent widget for the password prompt
    """
    AbstractAccount.promptParent = parent
