"""
Module for accessing passwords stored in Password State
"""
import base64

import requests

from rsg_core.configs import main as config
from rsg_core_py.metaclasses import singleton
from rsg_core_py.exceptions import types


class PasswordStateError(types.RockstarError):
    def __init__(self, message, errorCode):
        """Initializer.

        Args:
            message (str): error message to display
            errorCode (int): the status code returned by the password state command
        """
        self.message = "{} Status Code Returned\n\t{}".format(errorCode, message)
        types.RockstarError.__init__(self, self.message)


class PasswordState(object):

    __metaclass__ = singleton.Singleton
    URL = 'https://passwordstate/winapi' # base url for the api

    def __init__(self):
        """
        Initializer
        """
        super(PasswordState, self).__init__()
        self._session = None

    @staticmethod
    def _getAuth():
        """
        Gets the appropriate authentication instance based on the operating system

        Raises:
            OSError: SSO Access is currently not supported for the current OS

        Returns:
            object
        """
        # TODO: Move imports to top of script once we confirm requests_kerberos works
        if config.system.isWindows:
            import requests_negotiate_sspi
            return requests_negotiate_sspi.HttpNegotiateAuth()

        elif config.system.isLinux:
            import requests_kerberos
            return requests_kerberos.HTTPKerberosAuth()

        else:
            raise OSError("SSO Access is currently not supported for this OS")

    @property
    def session(self):
        """
        Session that sends credentials to password state

        Returns:
            requests.Session
        """
        if not self._session:
            self._session = requests.Session()
            self._session.verify = config.system.environment.techartProject.sslCertificate
            self._session.auth = self._getAuth()
        return self._session

    def get(self, url):
        """
        Performs a get request on Password State

        Args:
            url (str): url to perform get request on

        Raises:
            ValueError: No values provided from password state
            rsg_core_py.exceptions.types.ConnectionError: There was an error connecting to Password State

        Returns:
            dict
        """
        with self.session as session:
            result = session.get(url)
        self._session = None  # Get rid of the session after it is closed

        try:
            dictionary = result.json()[0]
        except Exception:
            dictionary = {}

        if result.status_code != 200:
            messageDict, phraseDict = dictionary["errors"]
            message = "{}\n{}".format(messageDict['message'].strip(), phraseDict['phrase'].strip())
            raise PasswordStateError(message, result.status_code)

        return dictionary

    def password(self, passwordId):
        """
        Gets an encoded password from Password State

        Args:
            passwordId (int): id of the password

        Returns:
            str
        """
        dictionary = self.get("{}/passwords/{}".format(self.URL, str(passwordId)))  # password is the endpoint for individual passwords
        return base64.b64encode(dictionary["Password"])


def password(passwordId):
    """
    Convenience method for getting an individual password

    Returns:
        str
    """
    return PasswordState().password(passwordId=passwordId)

