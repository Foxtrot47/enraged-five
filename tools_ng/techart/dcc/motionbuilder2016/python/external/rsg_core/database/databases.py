"""
Classes with convenience methods for getting connections to commonly accessed databases.
"""
import base64

from rsg_core.configs import main as config
from rsg_core.database import sql, mongo, elasticSearch
from rsg_core_py.decorators import memoize
from rsg_core import conversions


DEFAULT_USERNAME = "tools"
DEFAULT_PASSWORD = base64.b64decode('cm9ja3N0YXIxQQ==')


class BaseConnection(object):
    NAME = None


class NYC(BaseConnection):

    NAME = "animtracking-sql"

    @classmethod
    def AnimationStats(cls):
        return sql.Connection(
                             driver='{SQL Server}',
                             server=cls.NAME,
                             database="AnimationStats_{}".format(config.system.environment.toolsProjectName),
                             username=DEFAULT_USERNAME,
                             password=DEFAULT_PASSWORD
                             )

    @classmethod
    def CutsceneStats(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="CutsceneStats_{}".format(config.system.environment.toolsProjectName),
                             username=DEFAULT_USERNAME,
                             password=DEFAULT_PASSWORD
                             )

    @classmethod
    def LevelStats(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="LevelStats_{}".format(config.system.environment.name),
                             )

    @classmethod
    def GradingStats(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="gradingJobs",
                             )

    @classmethod
    def CutsceneLogging(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="CutsceneDatabase_Logging",
                             )

    @classmethod
    def AutomationJobs(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="AutomationJobs",
                             )

    @classmethod
    def CaptureRenders(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="CaptureRenders",
                             username=DEFAULT_USERNAME,
                             password=DEFAULT_PASSWORD
                             )

    @classmethod
    def FaceAudioMocap(cls):
        return sql.Connection(driver="{SQL Server}", server=cls.NAME, database="FaceAudioMocap")

    @classmethod
    def RockstarTools(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="RockstarTools",
                             )

    @classmethod
    def Mocap(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="Mocap",
                             username=DEFAULT_USERNAME,
                             password=DEFAULT_PASSWORD
                             )


class NYCDEV(object):
    """
    Convenience methods for getting connections to databases stored on the SQL server instance used for development.
    """
    NAME = "RSGNYCSQL2\RSGNYCSQL2"

    @classmethod
    def FaceAudioMocap(cls):
        return sql.Connection(driver="{SQL Server}", server=cls.NAME, database="FaceAudioMocap")

    @classmethod
    def Mocap(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="Mocap",
                             username=DEFAULT_USERNAME,
                             password=DEFAULT_PASSWORD
                             )


class SD(BaseConnection):
    NAME = "RSGSANSQL1\PRODUCTION"

    @classmethod
    def RockstarTools(cls):
        return sql.Connection(
                             driver="{SQL Server}",
                             server=cls.NAME,
                             database="RockstarTools",
                             username=DEFAULT_USERNAME,
                             password=DEFAULT_PASSWORD
                             )


class NycMongoDev(BaseConnection):
    """
    Convenience methods for getting connections to databases stored on the Mongo instance used for
    development and testing.
    """
    NAME = "rsgadcmgo2"

    _USER = "siteUserAdmin"
    _PASSWORD = base64.b64decode("cnN0YXRvb2xz")
    _HOST = NAME

    METRICS_DATABASE = "metrics"
    APP_CHECKINS_COLLECTION = "app_checkins"
    NUC_BS_DATABASE = "nuc_backend_services"
    MANIFEST_COLLECTION = "manifest"
    SHOTGUN_BACKEND = "shotgun_backend"
    YAAS_DATABASE = "yaas"
    YAAS_JOB_COLLECTION = "jobs"
    YAAS_TASKS_COLLECTION = "tasks"
    LOGS_DATABASE = "logs"
    LOGS_COLLECTION = "log_files"
    MOCAP = "mocap"
    MOCAP_STAGE_STATS_COLLECTION = "StageStats"
    TECHART_CONFIGS_DATABASE = "techart_configs"
    PROJECT_CONFIGS = "projects"

    @classmethod
    def metrics(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.METRICS_DATABASE)

    @memoize.memoize
    @classmethod
    def nucBackendServices(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.NUC_BS_DATABASE)

    @classmethod
    def shotgunBackend(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.SHOTGUN_BACKEND)

    @classmethod
    def mocap(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.MOCAP)

    @classmethod
    def yaas(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.YAAS_DATABASE)

    @classmethod
    def logs(cls):
        server = mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST)
        return server.connect(database=cls.LOGS_DATABASE)

    @classmethod
    def configs(cls):
        server = mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST)
        return server.connect(database=cls.TECHART_CONFIGS_DATABASE)


class NycADCMongo(BaseConnection):
    """
    Convenience methods for getting connections to databases stored on the Mongo instance used for
    rsgadcmgo8.
    """

    NAME = "rsgadcmgo8"

    _USER = "siteUserAdmin"
    _PASSWORD = conversions.base64Decode("cnN0YXRvb2xz")
    _HOST = NAME

    PIPELINE_CACHE_COLLECTION = "pipeline-cache-1-0-0"

    @classmethod
    def pipelineCache(cls):
        server = mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST)
        return server.connect(database=cls.PIPELINE_CACHE_COLLECTION)


class NycTranscodingMongoDev(BaseConnection):
    """
    Convenience methods for getting connections to databases stored on the Mongo instance used for
    rsgadcmgo5.
    """

    NAME = "rsgadcmgo5"

    _USER = "siteUserAdmin"
    _PASSWORD = conversions.base64Decode("cnN0YXRvb2xz")
    _HOST = NAME

    KAFKA_DATABASE = "kafka"
    TRANSCODING_DATABASE = "transcoding"
    UNITTEST_DATABASE = "unittest"
    TEST_DATABASE = "test"

    TRANSCODING_APP_CONFIG_COLLECTION = "application_configurations"
    TRANSCODING_CONFIGS_COLLECTION = "configurations"
    SHARED_FFMPEG_COMMANDS_COLLECTION = "shared_ffmpeg_commands"
    TRANSCODING_JOBS_COLLECTION = "transcoding_jobs"
    PYTHON_UNITTEST_COLLECTION = "python"
    CHANGE_STREAMS_TEST_COLLECTION = "change_streams_test"

    @classmethod
    def kafka(cls):
        server = mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST)
        return server.connect(database=cls.KAFKA_DATABASE)

    @classmethod
    def transcoding(cls):
        server = mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST)
        return server.connect(database=cls.TRANSCODING_DATABASE)

    @classmethod
    def unittest(cls):
        server = mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST)
        return server.connect(database=cls.UNITTEST_DATABASE)

    @classmethod
    def test(cls):
        server = mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST)
        return server.connect(database=cls.TEST_DATABASE)


class NycMongoProd(BaseConnection):
    """
    Convenience methods for getting connections to databases stored on the Mongo instance used for
    production.
    """
    NAME = "rsgadcmgo1"

    _USER = "siteUserAdmin"
    _PASSWORD = base64.b64decode("cnN0YXRvb2xz")
    _HOST = NAME

    METRICS_DATABASE = "metrics"
    APP_CHECKINS_COLLECTION = "app_checkins"
    NUC_BS_DATABASE = "nuc_backend_services"
    MANIFEST_COLLECTION = "manifest"
    YAAS_DATABASE = "yaas"
    YAAS_JOB_COLLECTION = "jobs"
    YAAS_TASKS_COLLECTION = "tasks"
    LOGS_DATABASE = "logs"
    LOGS_COLLECTION = "log_files"
    MOCAP = "mocap"
    MOCAP_STAGE_STATS_COLLECTION = "StageStats"
    SHOTGUN_BACKEND = "shotgun_backend"

    @classmethod
    def metrics(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.METRICS_DATABASE)

    @memoize.memoize
    @classmethod
    def nucBackendServices(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.NUC_BS_DATABASE)

    @classmethod
    def mocap(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.MOCAP)

    @classmethod
    def yaas(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.YAAS_DATABASE)

    @classmethod
    def logs(cls):
        server = mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST)
        return server.connect(database=cls.LOGS_DATABASE)

    @classmethod
    def shotgunBackend(cls):
        return mongo.Connection(user=cls._USER, password=cls._PASSWORD, host=cls._HOST).connect(database=cls.SHOTGUN_BACKEND)


class ElasticSearchDev(BaseConnection):
    """Convenience methods for getting connections to indices used for development."""
    NAME = elasticSearch.ElasticServer.DEV_SERVER

    TELEMETRY_TECHART_SHOTGUN_USERS_INDEX = "telemetry_techart_shotgun_users"
    TELEMETRY_TECHART_SHOTGUN_USERS_DOCTYPE = "shotgun_user"
    TELEMETRY_TECHART_APPS_PYTHON_INDEX = "telemetry_techart_apps_python"
    TELEMETRY_TECHART_APPS_PYTHON_DOCTYPE = "generic_app"

    @classmethod
    def telemetryTechartShotgunUsers(cls):
        return elasticSearch.Connection(
            cls.NAME,
            cls.TELEMETRY_TECHART_SHOTGUN_USERS_INDEX,
            cls.TELEMETRY_TECHART_SHOTGUN_USERS_DOCTYPE,
        )

    @classmethod
    def telemetryTechartAppsPython(cls):
        return elasticSearch.Connection(
            cls.NAME,
            cls.TELEMETRY_TECHART_APPS_PYTHON_INDEX,
            cls.TELEMETRY_TECHART_APPS_PYTHON_DOCTYPE,
        )


class ElasticSearchProd(ElasticSearchDev):
    """Convenience methods for getting connections to indices used for production."""
    NAME = elasticSearch.ElasticServer.PROD_SERVER


def getByName(name):
    """
    Gets the database based on the name

    Args:
        name (str): name of the database to get

    Returns:
        rsg_core.databases.BaseConnection
    """
    for cls in BaseConnection.__subclasses__():
        if cls.NAME == name:
            return cls()
