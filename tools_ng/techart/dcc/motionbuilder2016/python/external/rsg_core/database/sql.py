"""
Description:
   Class for interacting with an database sql connection

Example:

from rsg_core import database

# example
connection = database.connection('{SQL Server}','NYCW-MHB-SQL','CaptureRenders', None)

# normal query
connection.executeQuery("SELECT * FROM AvidEdits")

# stored procedure
results = connection.executeQuery("exec GetCaptureStatus_RDR 2,0")

"""
import re
import platform
import traceback

import pyodbc

from rsg_core.configs import main as config
from rsg_core_py.exceptions import types
from rsg_core_py.decorators import databases

WARNING = 'There is no connection to the database.'


class Connection(object):
    """
    Methods for connecting to a database
    """

    driver = ''
    server = ''
    database = ''
    log = None
    timeout = 60

    warning = 'There is no connection to the database'

    __connection_format = ('Driver={driver};Server={server};Database={database};'
                           'Initial Catalog={database};Integrated Security={security};Connection Timeout={timeout}')
    __connection_format_Linux = ('Driver={driver};Servername={server};Database={database};'
                                 'Initial Catalog={database};Integrated Security={security};'
                                 'Connection Timeout={timeout}')
    __cache = {}

    def __new__(cls, driver, server, database, log=None, force=False, security=True, username=None,
                 password=None, timeout=timeout, raiseErrors=False, encoding=None, decoding=None, forceNew=False):
        """
        Overrides built-in method

        Caches the connection string so the same connection instance is returned if that connection string is
        passed in again.

        Arguments:
            driver (string): name of the driver
            server (string): name of the server
            database (string): name of the database
            log (ULog): Universal Log
            force (boolean): force the connection
            security (bool): use integrated security
            username (string): the username to use to connect to the database
            password (string): password for the username being used
            timeout (int): time to wait before a connection should time out
            encoding (string): the data type that string and unicode need to be encoded in when sent to the server
            decoding (string): the data type that CHAR and WCHAR data from the server needs to be decoded from when
                               getting that from the server.
        Return:
            DatabaseConnection
        """
        if config.system.isLinux:
            parts = server.split("\\")
            if len(parts) == 2:
                server = parts[1]

        connectionString = cls._connectionString(driver=driver, server=server, database=database, security=security,
                                          username=username, timeout=timeout, password=password)
        instance = None
        if forceNew is False:
            instance = cls.__cache.get(connectionString, None)
        if instance is None:
            instance = object.__new__(cls)
        return instance

    def __init__(self, driver, server, database, log=None, force=False, security=True, username=None,
                 password=None, timeout=timeout, raiseErrors=False, encoding=None, decoding=None, forceNew=False):
        """
        Attempts to connect to the database based on the parameters provided

        Arguments:
            driver (string): name of the driver
            server (string): name of the server
            database (string): name of the database
            log (ULog): Universal Log
            force (boolean): force the connection
            security (bool): use integrated security
            username (string): the username to use to connect to the database
            password (string): password for the username being used
            timeout (int): time to wait before a connection should time out
            encoding (string): the data type that string and unicode need to be encoded in when sent to the server
            decoding (string): the data type that CHAR and WCHAR data from the server needs to be decoded from when
                               getting that from the server.
        """
        # Account for the DB Instance
        if config.system.isLinux:
                parts = server.split("\\")
                if len(parts) == 2:
                        server = parts[1]
        connectionString = self._connectionString(driver=driver, server=server, database=database, security=security,
                                                  username=username, timeout=timeout, password=password)

        if self.__class__.__cache.get(connectionString, None) and forceNew is False:
            return

        # Set values if they are passed as keyword arguments
        self.__connection = None
        self.raiseErrors = raiseErrors
        self.driver = driver
        self.server = server
        self.database = database
        self.log = log
        self.security = security
        self.username = username
        self.password = password
        self.encoding = encoding or "utf-8"
        self.decoding = decoding or "utf-8"
        if timeout:
            self.timeout = timeout

        self.connect(force=force)

        self.__class__.__cache.setdefault(connectionString, self)

    def __str__(self):
        return "{driver}:{server}:{database}".format(
            driver=self.driver,
            server=self.server,
            database=self.database)

    # ----------- Connection Methods ----------
    @classmethod
    def _connectionString(cls, driver, server, database, security, timeout, username, password):
        """
        String that returns the driver name, server and database the current DatabaseConnection instance is
        connected to.

        Return:
            String
        """

        connectionFormat = cls.__connection_format
        if config.system.isLinux:
            connectionFormat = cls.__connection_format_Linux
            username = "ROCKSTAR\\svcrsgnycvmx"
            password = "B0bthecapture!"

        connectionString = connectionFormat.format(
            driver=driver,
            server=server,
            database=database,
            security=security,
            timeout=timeout
        )
        if username and password:
            connectionString = "{};UID={};PWD={}".format(connectionString, username, password)
        return connectionString

    @property
    def connectionString(self):
        """
        String that returns the driver name, server and database the current DatabaseConnection instance is
        connected to.

        Return:
            String
        """
        return self._connectionString(driver=self.driver, server=self.server, database=self.database,
                                      security=self.security, timeout=self.timeout, username=self.username,
                                      password=self.password)

    @property
    def isConnected(self):
        """ returns true or false if we are connected to the database """
        try:
            cursor = self.__connection.cursor()
            cursor.close()
            # Read somewhere that explicitly deleting cursor should be done
            del cursor
            return True
        except Exception as e:
            return False

    @property
    def connection(self):
        """ The pydobc connection instance """
        return self.__connection

    def connect(self, force=True):
        """
        connects to the database

        Arguments: 
            force (boolean): tries to connect to the database even if there is a connection
        
        Returns: 
            cursor
        """
        if not self.isConnected or force:

            self.__connection = None
            log_type = 'LogMessage'
            context = "Message"
            error = None
            # Attempt to connect to database
            try:
                self.__connection = pyodbc.connect(self.connectionString)
                self.__connection.timeout = self.timeout
                # For pyodbc 4.0.+, you have declare how data gets encoded and decode

                # Set all sql characters of fixed & undetermined length to be decoded
                # Using the data type currently used on the server being connected to
                self.__connection.setdecoding(pyodbc.SQL_CHAR, encoding=self.decoding)
                self.__connection.setdecoding(pyodbc.SQL_WCHAR, encoding=self.decoding)

                # Set data types of string and unicode to be encoded
                # using the data type currently used on the server being connected to
                self.__connection.setencoding(str, encoding=self.encoding)
                self.__connection.setencoding(unicode, encoding=self.encoding)

                log_message = "Connected to {0}:{1} succesfully".format(self.server, self.database)

            except pyodbc.ProgrammingError as e:
                log_type, log_message, context = 'LogError', e.args[1], "Database Programming Error"
                error = e
            except pyodbc.Error as e:
                log_type, log_message, context = 'LogError', e.args[1], "Database Connection Error"
                self.__class__.timeout = 1
                error = e
            finally:
                # TODO: Replace with a techart log system
                # log results from connection if log is available
                # if self.log:
                #     getattr(self.log, log_type)(log_message, context)

                if self.raiseErrors and error:
                    raise types.ConnectionError(log_message)

        return self.__connection

    # ----------- SQL Methods ----------

    def executeQuery(self, query, fetchall=True):
        """
        performs the sql query provided on the current database

        Arguments:
            query (string): an sql query to perform on the database
        
        Return: 
            string or None
        """
        return self.executeQueries([query], fetchall=fetchall)

    @databases.timeout(timeout, pyodbc.OperationalError)
    @databases.connectionCheck
    def executeQueries(self, queries, fetchall=True):
        """
        performs multiple sql queries within the same transaction.

        Arguments:
            query (string): an sql query to perform on the database

        Return:
            string or None
        """
        result = []
        error = None

        cursor = self.cursor()

        for index, query in enumerate(queries):
            try:
                if config.system.isLinux:
                    query = re.compile(re.escape("'null'"), re.IGNORECASE).sub("NULL", query)
                cursor.execute(query)

            except pyodbc.Error as e:
                # TODO: replace with a techart log
                # if self.log:
                #     self.log.LogError(e.args[1], "Database Connection Error")
                # else:
                print(traceback.format_exc())
                error = e
            except Exception as e:
                # TODO: replace with a techart log
                # if self.log:
                #     self.log.LogError(e.args[1], "Python Error")
                # else:
                print(traceback.format_exc())
                error = e

            # Close the connection when an error happens to avoid open transactions
            if error:
                cursor.close()
                self.connection.close()
                if self.raiseErrors:
                    raise types.SQLError(str(error))
                return

            # Added support for running stored procedure and also made case insensitive
            try:
                result.extend([row for row in cursor])
            except Exception as error:
                pass
            cursor.commit()

        return result

    @property
    @databases.connectionCheck
    def cursor(self):
        """ Returns a cursor object and starts a new transaction """
        return self.__connection.cursor

    @databases.connectionCheck
    def close(self):
        """ Closes the current connection """
        return self.__connection.close()

    @property
    @databases.connectionCheck
    def commit(self):
        """ Commits current transaction """
        return self.__connection.commit

    @databases.connectionCheck
    def rollback(self):
        """ NOT YET IMPLEMENTED
        Rolls back current transaction """
        pass
