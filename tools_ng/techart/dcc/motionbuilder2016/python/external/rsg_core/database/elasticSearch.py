"""Contains the ElasticsearchController class and supporting objects.

Examples:

    from rsg_core.database import elasticSearch

    # Server Name
    SERVER = elasticSearch.ElasticServer.DEV_SERVER
    # Database Name
    ELASTIC_SEARCH_INDEX = "name of my database"
    # Document Type
    ELASTIC_SEARCH_DOCTYPE = "name of the document type I am storing"
    # Mappings on how to store/describe our data
    mappings = {"mappings": {
        ELASTIC_SEARCH_INDEX: {
            "_timestamp": {
                "enabled": "true"
            },
            'properties': {
                "timestamp" : {"type": "date",
                               "format":"dd/MM/yyy HH:mm:ss"}

            }
    }}}

    dataDict = {
        mydata: "",
        moreData: {
            dataEntry: "someData",
            anotherEntry: "evenMOreData"
        }
    }

    esConnection = elasticSearch.Connection(SERVER, ELASTIC_SEARCH_INDEX, ELASTIC_SEARCH_DOCTYPE)
    esConnections.createDocument(dataDict)
"""
import logging
import warnings
import base64
import os

import elasticsearch

from rsg_core.configs import main as config


logger = logging.getLogger(__name__)


# Suppress SSL warning on older versions of Python (<2.7.9), etc.
warnings.filterwarnings("ignore")


class ElasticServer(object):
    """Describes the location of the TechArt production and development servers."""

    DEV_SERVER = "https://8fdbfc86c772493ab58b723c15ee7331.lb.dev.elastic.rockstargames.com:9243"
    PROD_SERVER = "https://f3b67f29bcde40149f493a6c6e3fb1db.lb.prod.elastic.rockstargames.com:9243"

    DEV_PSW = "ODJ6RnZrUzhDWmRxTkxqWg=="
    PROD_PSW = "NWZTTTZqRnVMd3g4M1EzbQ=="


class Connection(object):
    """Wrapper class for connecting to an ElasticSearch index with authentication.

    Loosely based on the Connection class in database.sql.
    """

    DEFAULT_TIMEOUT = 5  # Time in seconds.

    def __init__(self, server, index, docType, timeout=None):
        """Connect and create an Elastic Search Index.

        Args:
            server (str): Server Name.
            index (str): Search index
            docType (str): Doc type.
            timeout (str, optional): The connection's timeout in seconds. Default: DEFAULT_TIMEOUT.

        Warnings:
            * The index and docType values need to be lower case!

        References:
            * https://github.com/elastic/elasticsearch-py/
            * https://www.elastic.co/guide/en/elasticsearch/reference/current/removal-of-types.html
        """
        self._server = server
        self._index = index.lower()  # Must be lowercase.
        self._docType = docType.lower()  # Must be lowercase.

        certificate = config.system.environment.sslCertificate

        psw = ElasticServer.PROD_PSW if server == ElasticServer.PROD_SERVER else ElasticServer.DEV_PSW

        self._client = elasticsearch.Elasticsearch(
            [self._server],
            use_ssl=True,
            verify_certs=True,
            ca_certs=certificate,
            http_auth=("admin", base64.b64decode(psw).decode()),
            timeout=timeout or self.DEFAULT_TIMEOUT,
        )

    @property
    def server(self):
        return self._server

    @property
    def index(self):
        return self._index

    @property
    def docType(self):
        return self._docType

    def indexExists(self):
        return self._client.indices.exists(self._index)

    def createIndex(self, mapping=None):
        """Create the instance's specified index.

        This should only need to be run once, when setting up a new index.

        Args:
            mapping (dict, optional): mapping

        References:
            * https://www.elastic.co/blog/what-is-an-elasticsearch-index
            * https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html
        """
        if self._client.indices.exists(self.index):
            logger.warning("Index already exists! Aborting.")
            return

        logger.info("Creating new Index: {}\n\n\n\n".format(self.index))
        # Mapping tells elasticsearch what kind of data each field contains.
        if not mapping:
            mapping = '{"mappings":{"' + self.docType + '":{}}}'

        logger.info("Using Mappings:\n")
        logger.info(mapping)
        self._client.indices.create(
            index=self.index,
            ignore=400,
            body=mapping,
        )

    def createMapping(self, index, docType, mapping):
        """Create existing data mappings.

        Args:
            index (str): Elastic Search Index Name
            docType (str): Elastic Search doc type Name
            mapping (dict): json mapping schema

        References:
            * https://www.elastic.co/guide/en/elasticsearch/reference/current/removal-of-types.html
        """
        self._client.indices.put_mapping(index=index, doc_type=docType, body=mapping)

    def getMapping(self, index):
        """Get the Elastic Search mappings for the specific index.

        Args:
            index (str): Index name

        Returns:
            dict
        """
        return self._client.indices.get_mapping(index=index)

    def createDocument(self, data):
        """Add a new document to the current index.

        Args:
            data (dict): The new document data.

        Returns:
            object: The deserialized data (usually as dict)

        References:
            * https://www.elastic.co/guide/en/elasticsearch/reference/current/nested.html
        """
        return self._client.index(index=self._index, doc_type=self._docType, body=data)

    def search(self, body=None, params=None):
        return self._client.search(index=self._index, doc_type=self._docType, body=body, params=params or {})

    def deleteIndex(self, index):
        """Delete the entire Database (index).

        Args:
            index (str): name of Search Index
        """
        if self._client.indices.exists(index):
            self._client.indices.delete(index=index, ignore=[400, 404])
            logger.info("Deleted index: {}".format(index))

    def close(self):
        """Close the client's connections."""
        # TODO change to self._client.close() after updating to >=elasticsearch-py v7.8.0.
        self._client.transport.close()


if __name__ == "__main__":
    import pprint
    from rsg_core.logs import setups

    setups.consoleDebugging()

    # Demo
    ELASTIC_SEARCH_INDEX = "telemetry_unit_test2"
    ELASTIC_SEARCH_DOCTYPE = "unit_test"
    connection = Connection(ElasticServer.DEV_SERVER, ELASTIC_SEARCH_INDEX, ELASTIC_SEARCH_DOCTYPE)
    connection.createDocument({})

    print(("Index exists: {}".format(connection.indexExists())))

    print("Mappings:")
    pprint.pprint(connection.getMapping(ELASTIC_SEARCH_INDEX))

    results = connection.search()
    if results:
        print(("Total Docs for Index: {}".format(results.get("hits", {}).get("total"))))
        print("Documents (first page of results):")
        docs = results.get("hits", {}).get("hits", [])  # First 10 results.
        for doc in docs:
            pprint.pprint(doc.get("_source"))
