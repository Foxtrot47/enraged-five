"""
Class for interacting with a MongoDB connection.

Example:
    >>> from rsg_core.database import mongo
    # Create a new connection to a local instance of MongoDB.
    >>> db = mongo.Connection().connect(databaseName="foo")
    # Create a new connection to a server based instance of MongoDB.
    >>> db = mongo.Connection(user="admin", password="12345", host="nycu-example").connect(databaseName="foo")
    # Get a Connection object in order add data.
    >>> collection = db.getCollection("bar")
    # Submit a query.
    >>> TODO
    # Close the connection.
    >>> db.close()

Using Datetime Objects with MongoDB:
    * Because MongoDB assumes that dates and times are in UTC, care should be taken to ensure that dates and times
    written to the database reflect UTC.
        * >>> currentTime = datetime.datetime.utcnow()
        * >>> datetime.utcfromtimestamp(psutil.Process(os.getpid()).create_time())
        * More here: http://api.mongodb.com/python/current/examples/datetimes.html
    * While we can extract the datetime the "_id" field's ObjectID object, we should not rely on it.
        * Instead, it is advised to add a "creation_time" field with a value that is generated as closely as possible to
        when you wish to submit the record to MongoDB.

To start local MongoDB instance on Windows via command prompt:
    # https://docs.mongodb.com/tutorials/install-mongodb-on-windows/
    "C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe"

MongoDB CLI Querying:
    # Range
    db.getCollection("log_files").find({_id: {$lt: ObjectId("5b240e53db21b203c640633a"), $gt: ObjectId("5b2405dcdb21b203c6406332")}})

Python Querying:
    gen_time = datetime.datetime(2010, 1, 1, 22,33,0)
    dummy_id_start = pymongo.ObjectId.from_datetime(datetime.datetime(2010, 1, 1, 22, 00, 0))
    dummy_id_end = pymongo.ObjectId.from_datetime(datetime.datetime(2010, 1, 1, 22, 30, 0))
    result = db.getCollection("bar").collection.find({"_id": {"$lt": dummy_id_end, "$gt": dummy_id_start}})
"""
import pymongo


class Connection(object):
    """
    Wrapper class for connecting to a MongoDB instance with or without authentication.

    Loosely based on the Connection class in database.sql.
    """
    DEFAULT_HOST = "localhost"
    DEFAULT_PORT = 27017
    DEFAULT_TIMEOUT = 60000  # Listed in milliseconds.

    def __init__(self, user=None, password=None, host=None, port=None):
        """
        To connect to a local instance of MongoDB, leave all params at their defaults.

        Args:
            user (str): The user name; defaults to None.
            password (str): The password; defaults to None.
            host (str): The host name; defaults to the value in DEFAULT_HOST.
            port (int): The port number; defaults to the value in DEFAULT_PORT.
        """
        if (user is not None) and (password is not None) and (host is not None):
            host = self.createMongodbUri(user, password, host)
        self._host = host or self.DEFAULT_HOST
        self._port = port or self.DEFAULT_PORT
        self._client = None
        self._database = None

    @staticmethod
    def createMongodbUri(user, password, host):
        """
        Generate the MongoDB URI to pass to the MongoClient's host param.

        Args:
            user (str): The username to authenticate with.
            password (str): The password to authenticate with.
            host (str): The host to authenticate from.

        Returns:
            str: The MongoDB formatted URI.
        """
        return "mongodb://{}:{}@{}".format(user, password, host)

    def connect(self, database, timeout=None):
        """
        Establish a connection to the server and specified database.

        Args:
            database (str): The name of the desired database.
            timeout (int): Timeout after n milliseconds. Defaults to the value in DEFAULT_TIMEOUT.

        Returns:
            Connection: The instance of the Connection class.
        """
        timeout = timeout or self.DEFAULT_TIMEOUT
        self._client = pymongo.MongoClient(self._host, self._port, serverSelectionTimeoutMS=timeout, maxPoolSize=200)
        self._database = self._client[database]
        return self

    def getCollection(self, name):
        """
        Get a Collection with the given name using PyMongo's default options.

        Args:
            name (str): The name of the collection.

        Returns:
            pymongo.collection.Collection
        """
        return self._database[name]

    def databaseNames(self):
        """
        Get a list of the names of all databases on the connected server using PyMongo's default options.

        Note:
            databases with no collections or with empty collections will not show up in the return list

        Returns:
            list
        """
        return self._client.database_names()

    def collectionNames(self):
        """
        Get a list of all the collection names in this database using PyMongo's default options.

        Returns:
            list
        """
        return self._database.collection_names()

    def callCommand(self,command):
        """
        Execute database command

        Args:
            command (str): command name to execute

        Returns:
            list
        """
        return self._database.command(command)

    def close(self):
        """
        Cleanup client resources and disconnect from MongoDB.
        """
        self._client.close()


if __name__ == '__main__':
    import socket

    # Test via local MongoDB instance.
    hostWithLocalMongoInstanceRunning = socket.gethostname()
    db = Connection(host=hostWithLocalMongoInstanceRunning).connect(database="test_db", timeout=2000)
    print(db.collectionNames())

    db.close()
