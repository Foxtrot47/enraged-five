import unittest


class CoreTestingClass(unittest.TestCase):

    __monkeyPatch_originalClass = "originalClass"
    __monkeyPatch_originalMethod = "originalMethod"
    __monkeyPatch_method = "method"

    __monkeyPatches = []

    def setUp(self):
        super(CoreTestingClass, self).setUp()

    def tearDown(self):
        super(CoreTestingClass, self).tearDown()

        for patch in self.__monkeyPatches:
            setattr(
                    patch[self.__monkeyPatch_originalClass],
                    patch[self.__monkeyPatch_originalMethod],
                    patch[self.__monkeyPatch_method]
                    )
        self.__monkeyPatches = []

    def monkeyPatch(self, patchClass, patchMethod, replacementMethod):
        """
        Enables monkey patching with the given class and method.

        Examples:
            # The following will override QtGui.QMessageBox.critical() to run fakeShow() instead.

            >>> def fakeShow():
            ...     pass
            >>> monkeyPatch(QtGui.QMessageBox, "critical", fakeShow)

        Args:
            patchClass (object): The class to monkey patch.
            patchMethod (str): The method name to monkey patch.
            replacementMethod (instancemethod): The method that will be called instead.
        """
        mpDict = {}
        mpDict[self.__monkeyPatch_originalClass] = patchClass
        mpDict[self.__monkeyPatch_originalMethod] = patchMethod
        mpDict[self.__monkeyPatch_method] = getattr(patchClass, patchMethod)
        self.__monkeyPatches.append(mpDict)
        setattr(patchClass, patchMethod, replacementMethod)