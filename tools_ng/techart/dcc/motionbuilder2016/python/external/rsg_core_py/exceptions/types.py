"""
Custom Exception Errors
"""


class RockstarError(Exception):
    """ Base error class to help filter only for our custom errors """


class SilentError(RockstarError):
    """ Raises an error that passed silently by the Exception Hook """


class ConnectionError(RockstarError):
    """Raised if we can not connect to DB"""


class SQLError(RockstarError):
    """Raised if we run into an sql error"""


class PerforceError(RockstarError):
    """ Raised if we run into a Perforce error """


class AuthenticationError(RockstarError):
    """ Raised if we run into an error authenticating against a program or server """
