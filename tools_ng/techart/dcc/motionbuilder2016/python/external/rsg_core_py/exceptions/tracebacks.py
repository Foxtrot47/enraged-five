""" Module containing utility functions for retrieving data from tracebacks"""

import os
import shutil
import traceback


def stack(tb):
    """
    Gets the frame stack for the traceback
    Arguments:
        tb (traceback): the traceback to get frame stack from
    Return:
        Generator
    """
    while tb:
        yield tb.tb_frame
        tb = tb.tb_next


def paths(tb):
    """
    Gets a list of the file paths for the modules in the tracback stack

    Arguments:
        tb (tb): the traceback to get paths from

    Return:
        Generator
    """
    paths = []
    for frame in stack(tb):
        if frame.f_code.co_filename not in paths:
            yield tb.tb_frame.f_code.co_filename
            paths.append(frame.f_code.co_filename)


def extract(tb, filename=None, lineNum=-1, funcName=None, text=None):
    """
    Extracts information from traceback if there is any to extract

    Arguments:
        tb (traceback): traceback to extract error details from
        filename (string): file where the error was encountered
        lineNum (int): line number for the line that errored out
        funcName (string): name of the function that errored out
        text (string): description of the error

    Return:
        string(filename), int(line number), string(method name), string(error string)
    """

    information = [filename or "<No File>", lineNum, funcName or "None", text or ""]
    for extract in traceback.extract_tb(tb):
        for index, data in enumerate(extract):
            information[index] = data

    return information


def getFrameData(tb):
    """
    Gets all the locals for each frame of the exception level.

    Example:

        Frame DeleteSelectionSet in x:\wildwest\dcc\motionbuilder2014\python\RS\Tools\Animation\AnimToFBX\ToolBox.py at line 124
        self = <class 'RS.Tools.Animation.AnimToFBX.ToolBox.ToolBox'> <RS.Tools.Animation.AnimToFBX.ToolBox.ToolBox object at 0x0000000053F77A88>

        Frame DeleteSelectionSet in x:\wildwest\dcc\motionbuilder2014\python\RS\Tools\Animation\AnimToFBX\Controllers\ToolBox.py at line 207
        setNode = <type 'NoneType'> None
        mainWidget = <class 'RS.Tools.Animation.AnimToFBX.ToolBox.ToolBox'> <RS.Tools.Animation.AnimToFBX.ToolBox.ToolBox object at 0x0000000053F77A88>

    Arguments:
        tb(traceback): traceback for the error

    Returns:
        List of Strings, each line being a line of output from the frame in plain text
    """
    returnList = []
    # traceback.print_exc()

    for frame in stack(tb):
        returnList.append("")
        returnList.append("Frame {0} in {1} at line {2}".format(
                                                                frame.f_code.co_name,
                                                                frame.f_code.co_filename,
                                                                frame.f_lineno)
                          )
        for key, value in frame.f_locals.iteritems():
            # The Try is to make sure we can convert the object to a string without error
            try:
                strValue = str(value)
            except:
                strValue = "<ERROR WHILE PRINTING VALUE>"

            if strValue != "":
                returnList.append("    {0} = {1} {2}".format(key, str(type(value)), strValue))
    return returnList


def createFrameDataFileAttachment(tb, path):
    """
    Creates a log file with the frame data from the error

    Arguments:
        tb (traceback): the traceback of the error to log
        path (string): path to save attachment out to

    Return:
        tuple(string, boolean)
    """
    success = False
    directory = os.path.dirname(path)
    try:
        if not os.path.exists(directory):
            os.mkdir(directory)

        frameData = getFrameData(tb)
        with open(path, 'w') as fstream:
            fstream.write("\n".join(frameData))
            print 'Unhandled exception has been logged to ({0}).'.format(path)
        success = True
    except Exception as exception:
        print 'An unknown error occurred trying to write out the exception log to ({0}).\n{1}'.format(path, str(exception))

    return path, success