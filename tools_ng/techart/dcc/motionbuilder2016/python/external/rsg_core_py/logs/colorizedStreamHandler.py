"""
Contains a Python standard logging StreamHandler to colorize output.
"""
import os
import logging
import platform

import colorama


class ColorizedStreamHandler(logging.StreamHandler):
    """
    StreamHandler with colorized output.

    Notes:
        * Supports all log levels listed in the standard lib.

    Examples:
        handler = ColorizedStreamHandler()
        formatter = logging.Formatter("%(asctime)s [%(levelname)s]: %(message)s")
        handler.setFormatter(formatter)

        logger = logging.getLogger(__name__)
        logger.addHandler(handler)
        logger.setLevel(logging.DEBUG)

        logger.debug("message")
        logger.info("message")
        logger.warning("message")
        logger.error("message")
        logger.critical("message")
    """

    COLORS = {
        "DEBUG": colorama.Fore.CYAN,
        "TRACE": colorama.Fore.WHITE,
        "PROFILE": colorama.Fore.WHITE,
        "INFO": colorama.Fore.GREEN,
        "WARN": colorama.Fore.YELLOW,
        "WARNING": colorama.Fore.YELLOW,
        "ERROR": colorama.Fore.RED,
        "CRIT": colorama.Fore.RED,
        "CRITICAL": colorama.Fore.RED,
        "TOOL_ERROR": colorama.Fore.RED,
        "TOOL_EXCEPTION": colorama.Fore.RED,
    }
    if os.getenv("PYDEV_CONSOLE_ENCODING"):
        # By default, the Eclipse Console view doesn't handle ANSI escape colors properly, resulting in the characters
        # showing up in log messages and no colorization. The following lines won't colorize, but simply prevent errors.
        if platform.system() == "Windows":
            colorama.init()
        else:
            colorama.init(strip=True)
    elif os.getenv("PYCHARM_HOSTED") and platform.system() == "Windows":
        # The wrap param must be set to False to support Pycharm on Windows - see colorama's docs for more info.
        colorama.init(wrap=False)
    else:
        # This works for Windows 10, Ubuntu 14, and Ubuntu 18.
        colorama.init()

    def emit(self, record):
        try:
            message = self.format(record)
            self.stream.write(self.COLORS[record.levelname] + message + colorama.Style.RESET_ALL)
            self.stream.write(getattr(self, "terminator", "\n"))
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception:
            self.handleError(record)


if __name__ == "__main__":
    handler = ColorizedStreamHandler()
    formatter = logging.Formatter("%(asctime)s [%(levelname)s]: %(message)s")
    handler.setFormatter(formatter)

    logger = logging.getLogger(__name__)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    logger.debug("message")
    logger.info("message")
    logger.warning("message")
    logger.error("message")
    logger.critical("message")
