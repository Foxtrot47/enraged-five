"""
Constants used across Python codebase
"""


class WindowsHresultValues(object):
    """Enum for windows HRESULT values.
        These are returned when calling `ctypes.windll` functions, ex: ctypes.windll.shcore.GetProcessDpiAwareness()
        Error codes are outlined at: https://docs.microsoft.com/en-us/windows/win32/seccrypto/common-hresult-values
    """
    S_OK = 0x00000000
    E_ABORT = 0x80004004
    E_ACCESSDENIED = 0x80070005
    E_FAIL = 0x80004005
    E_HANDLE = 0x80070006
    E_INVALIDARG = 0x80070057
    E_NOINTERFACE = 0x80004002
    E_NOTIMPL = 0x80004001
    E_OUTOFMEMORY = 0x8007000E
    E_POINTER = 0x80004003
    E_UNEXPECTED = 0x8000FFFF
