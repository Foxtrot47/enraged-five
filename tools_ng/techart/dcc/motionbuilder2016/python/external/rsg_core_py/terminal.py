"""Module for holding generic code that has to do with interacting with a terminal of a given os."""
import os
import subprocess


def buildWindowsCommand(title, cmd):
    """Builds the command to be ran as an independent subprocess in its own separate terminal.

    Args:
        title (str): title for the terminal running the process
        cmd (str): command to run
    """
    return ["start", title, "cmd.exe", "/C", cmd]


def executeWindowsCommand(command, devNull=None):
    """Execute a subprocess in windows as a separate process from the one that executes it.

    Args:
        command (str): command to run
        devNull (File): file object to redirect subprocess output to.
    """
    # Stealing code from the services/Geoff
    devNull = devNull or open(os.devnull, "wb")

    # This is a windows magic number!! Yey Windows!!
    detachedProcess = 0x00000008
    createNewProcessGroup = 0x00000200

    subprocess.Popen(
        command,
        shell=True,
        stdin=devNull,
        stdout=devNull,
        stderr=devNull,
        creationflags=detachedProcess | createNewProcessGroup,
    )
