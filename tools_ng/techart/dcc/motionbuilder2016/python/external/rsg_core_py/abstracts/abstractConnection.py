"""Abstract class for setting up a connection to a server."""
import uuid
import urllib

from rsg_core_py.exceptions import types


class AbstractConnection(object):
    """Abstract class for a server connection."""

    def __init__(self):
        """Initializer."""
        raise NotImplementedError

    def _setup(self, server):
        """Setups the base properties used by this object.

        This method should be invoked in the reimplemented __init__ method.

        Arguments:
            server (str): path of the server to connect to
        """
        self._server = server
        self._type = None
        self._account = None
        self._delegate = None
        self._session = None
        self._sslCertificate = None
        self._authenticated = False
        self._uuid = uuid.uuid4()  # This is for caching purposes so our caching doesn't break in the services
        self._builders = {}

    @property
    def instance(self):
        """The internal connection to the server if it is represented by another object."""
        raise NotImplementedError

    @property
    def account(self):
        """The account to authenticate as.

        Return:
            rsg_core.accounts.AbstractAccount
        """
        raise NotImplementedError

    @account.setter
    def account(self, value):
        """Sets the account to authenticate as.

        Arguments:
            value (rsg_core.accounts.AbstractAccount): account to authenticate as
        """
        raise NotImplementedError

    @property
    def uuid(self):
        """The uuid for this connection instance.

        Used for mapping the cache.
        """
        return self._uuid

    @property
    def client(self):
        """The client that this connection connects to.

        Returns:
            str
        """
        raise NotImplementedError

    @property
    def serverType(self):
        """Gets the type of the server we are connecting to.

        Returns:
            string
        """
        return self._type

    @property
    def delegate(self):
        """Username of the user to delegate behavior as.

        Return:
            str
        """
        return self._delegate

    @delegate.setter
    def delegate(self, value):
        """Sets the user to delegate behavior as.

        Arguments:
            value (str): username of the user to delegate actions as
        """
        self._delegate = value

    def _getBuilder(self, classType):
        """Gets the builder for the given entity.

        Args:
            classType (str): the type of the class

        Returns:
            rsg_core_py.abstracts.abstractBuilder.AbstractBuilder
        """
        raise NotImplementedError

    def _setScriptKey(self, username, password, isEncoded):
        """Set the script key to authenticate as.

        This will set an account with the credentials of the provided script key

        Args:
            username (string): name of the script key
            password (string): the key/password of the script key
            isEncoded (bool): is the password encoded
        """
        raise NotImplementedError

    def buildUrl(self, sections, fields=None, additionalArgs={}):
        """Builds an url to request/send information through the rest api this connection supports.

        Args:
            sections (list): parts of the url

            fields (dict, optional): fields to include in the url
            additionalArgs (object, optional): to be determined as some queries require non-standardized information

        Returns:
            str
        """
        url = "/".join([self.restUrl] + [str(section) for section in sections])
        if fields:
            fieldStringList = ["{}={}".format(key, urllib.quote(str(value), safe="")) for key, value in fields.iteritems()]
            fieldsString = "&".join(fieldStringList)
            url = "{}?{}".format(url, fieldsString)

        return url

    def login(self, username=None, counter=10, raiseErrors=False, **kwargs):
        """Login to the server.

        Args:
            username (str, optional): username to use instead of the name of the account that has been set for this connection.
            counter (int, optional): amount of times to attempt to login to the server, defaults to 10.
            raiseErrors (bool, optional): raise an error if the login method can't connect to the server.
            **kwargs (dict): additional arguments accepted by the reimplemented authenticate method of the class

        Returns:
            bool
        """
        continue_ = True
        # attempt to connect to the server
        while continue_ and counter:
            try:
                self.account.authenticate(self, username=username, **kwargs)
                self._authenticated = True
                return True

            except types.AuthenticationError:
                counter -= 1
                # Do not prompt the user after the first error to attempt to use the stored password first
                if counter > 0:
                    continue_ = False
                    if self.account.promptForPassword:
                        continue_ = self.account.passwordPrompt()
                if (self.account.raiseError or raiseErrors) and (not continue_ or not counter):
                    raise
        return False

    def logout(self):
        """Logs out the server."""
        raise NotImplementedError

    def authenticate(self, username, password):
        """Authenticates the set account against the server for access.

        Args:
            username (str): name of the user to access the server as
            password (str): password of the user
        """
        raise NotImplementedError

    def ping(self):
        """Pings the server to confirm if it is accessible."""
        raise NotImplementedError

    def cleanLastAccessedCache(self):
        """Cleans up the last accessed cache to free up memory and resources."""
        raise NotImplementedError

    def clearCache(self):
        """Empties out the local cache of instances created by this connection."""
        raise NotImplementedError
