"""
Abstract class for building payloads
"""
import types

class AbstractPayloadBuilder(object):
    """
    Intermediate class for building payloads
    """

    def __call__(self, index=None, data=None):
        """
        Builds a payload.

        If no index is provided then an empty instance is returned.
        If an index is provided then this instance will be populated with data from the server.
        If an instance of an existing object of the same class is passed, then it is returned.
        If an invalid index is passed then an error will be raised.

        Keyword Args:
            index (int): id of the payload on the server
            data (dict): data to fill the instance with

        Returns:
            rsg_core_py.abstracts.abstractPayload.AbstractPayload
        """
        class_ = self.cls()
        # Nested if statement to keep this heavily used method quick.
        # Having an if-elif with an extra isinstance call doubles the amount of time to do this call.
        if not isinstance(index, (int, basestring, types.NoneType)):
            if isinstance(index, class_):
                return index
            raise ValueError("{} is not a valid value for this builder".format(index))
        return class_(self._server, index, data)

    def __init__(self, server):
        """
        Initializer

        Args:
            server (rsg_core_py.abstracts.abstractServer): the server to use
        """
        self._server = server

    def _clearCache(self):
        """
        This clears out the memory caches of the entities that have been queried from Shotgun.
        This class is strictly for debugging purposes
        """
        self.cls()._clearCache()

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        raise NotImplementedError

    def all(self, filters):
        """
        Gets the all the payloads for the class this builder manages

        Arguments:
            filters(list): additional filters to include

        Returns:
            list
        """
        raise NotImplementedError

    def getByName(self, name, filters=None):
        """
        Gets a payload based on its name

        Arguments:
            name (str): name of the

        Keyword Args:
            filters(list): additional filters to include

        Returns:
            rsg_core_py.abstracts.abstractPayload.AbstractPayload
        """
        raise NotImplementedError

    @classmethod
    def isInstance(cls, object):
        """
        Is the passed object an instance of the type of class this builder generates.

        Args:
            object (object):  object to check

        Returns:
            bool
        """
        return isinstance(object, cls.cls())