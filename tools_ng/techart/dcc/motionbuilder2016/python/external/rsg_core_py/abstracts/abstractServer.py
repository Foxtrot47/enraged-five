"""Abstract class for managing a connection to a server."""


class AbstractServer(object):
    """Abstract class for managing a connection to a server."""

    def __init__(self):
        """Initializer."""
        raise NotImplementedError

    @property
    def instance(self):
        """The internal connection to the server if it is represented by another object."""
        raise NotImplementedError

    @property
    def account(self):
        """The account to use for authenticating against the server.

        Return:
            rsg_core.accounts.AbstractAccount
        """
        return self.instance.account

    @account.setter
    def account(self, value):
        """Sets the account to use for authenticating against the server.

        Arguments:
            value (rsg_core.accounts.AbstractAccount): account to authenticate as
        """
        self.instance.account = value

    @property
    def delegate(self):
        """User to delegate behavior as."""
        return self.instance.delegate

    @delegate.setter
    def delegate(self, value):
        """Sets the user to delegate behavior as.

        Args:
            value (str): username of the user to delegate actions as
        """
        self.instance.delegate = value

    @property
    def scriptKey(self):
        """The script key information being used to access the server."""
        return self.instance.scriptKey

    @scriptKey.setter
    def scriptKey(self, scriptKey):
        """Set the script key to authenticate as.

        This will set an account with the credentials of the provided script key

        Args:
            scriptKey (collections.namedtuple): namedtuple with the name and password for the script key
        """
        self.instance.scriptKey = scriptKey

    def clearCache(self):
        """Empties out the local cache of instances created by the connection to this server."""
        self.instance.clearCache()
