"""Abstract Payload to use as basis for other classes that represent some sort of payload that is provided or received from some server."""

import sys
import types
import datetime


class Differences(object):
    """Object for storing differences between mutable data types, particularly lists and dictionaries."""

    def __init__(self, type_):
        """Constructor.

        Args:
            type_(class): the type of iterable being compared, accepts list or dict.
        """
        self.type = type_
        self.append = None
        self.remove = None


class AbstractPayload(object):
    """Abstract payload class for any type of data that interacts with a server that exposes a CRUD paradigm that allows you to send the same data back.

    Attributes:
        TYPE (str): The type of this class
        IS_READONLY (int): Is this object type read only.
                           This will disable the ability to modify contents of the payload on the server.
        DATA_EXPIRY (int): the amount of time in seconds to wait until retrieving data from the server again
        CACHE_EXPIRY (int): the amount of time in seconds to wait until clearing stale data from the local cache
        CACHE_LIMIT (int): the threshold for the amount of objects allowed to be stored in memory.
                           Once the threshold is exceeded, we start culling the list to keep.
        DATETIME_FORMAT (str): the default date time format to use
        _INSTANCES (dict): internal cache of the different instances of this class
        _INDEX_FIELD (str): the field to use for getting the index of the payload
        _INVALID_SERVER_ERROR (Exception): error to invoke when no server is set
        _IS_READONLY_ERROR (Exception): exception to raise when a read only payload is edited
        _LAST_CACHE_CLEAR (datetime.datetime): the last time the data was updated
    """

    TYPE = None
    IS_READONLY = False  # Is this payload read only and essentially non-editable
    DATA_EXPIRY = 60 * 15  # 15 minutes in seconds
    CACHE_EXPIRY = 60 * 30  # half an hour in seconds
    CACHE_LIMIT = 50  # the amount of items to keep in memory at a time.
    DATETIME_FORMAT = None

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []
    _INDEX_FIELD = None
    _INDEX_TYPE = int
    _INVALID_SERVER_ERROR = None
    _IS_READONLY_ERROR = None
    _LAST_CACHE_CLEAR = datetime.datetime.utcnow()

    def __init__(self, index, data, server):
        """Initializer.

        Arguments:
            index (int): id of the object from the server
            data (dictionary): data that represents this object
            server (rsg_core_py.abstracts.abstractServer.AbstractServer): the local server to set for this object
        """
        raise NotImplementedError

    @classmethod
    def _getInstance(cls, server, index):
        """Gets the instance for this class that is cached based on the server and id.

        Args:
            server (rsg_core_py.abstracts.abstractConnection.AbstractConnection): server the payload was created against
            index (int): the id of the payload

        Returns:
            rsg_core_py.abstracts.abstractPayload.AbstractPayload
        """
        instance = cls._INSTANCES.get((server.uuid, index), None)
        if instance is None:
            instance = object.__new__(cls)
        return instance

    @classmethod
    def _setLastAccessedInstance(cls, instance):
        """Sets this instance to the front of the last accessed cache and cleans up the least accessed instances.

        Args:
            instance (rsg_core_py.abstracts.abstractPayload.AbstractPayload): payload instance to set as recently accessed.
        """
        cls._lastAccessed = datetime.datetime.utcnow()
        if instance in cls._LAST_ACCESSED_INSTANCES:
            cls._LAST_ACCESSED_INSTANCES.remove(instance)
        cls._LAST_ACCESSED_INSTANCES.insert(0, instance)

    @classmethod
    def _removeInstance(cls, server, index):
        """Removes this instance from the cache.

        Args:
            server (rsg_core_py.abstracts.abstractConnection.AbstractConnection): server the payload was created against
            index (int): the id of the payload

        Return:
            bool
        """
        return cls._INSTANCES.pop((server.uuid, index), None) is not None

    @classmethod
    def _isCached(cls, server, index, instance):
        """Checks that an instance for this class is cached based on the server and id.

        Args:
            server (rsg_core_py.abstracts.abstractConnection.AbstractConnection): server the payload was created against
            index (int): the id of the payload
            instance (AbstractPayload): the payload to check if it is cached

        Returns:
            bool
        """
        key = (server.uuid, index)
        return key in cls._INSTANCES and cls._INSTANCES.get(key, None) == instance

    def _isRemovable(self, minimumReferenceCount=3):
        """Is this object ready to be collected by garbage collection.

        Note:
            The default value for the minimumReferenceCount is 2 as the sys.getrefcount method creates a temporary
            reference for use in the function and there should be a reference for this item in the cache for this class.

        Args:
            minimumReferenceCount (int, optional): minimum amount of references allowed an object can be removed from our the
                                         internal cache.

        Returns:
            bool
        """
        return sys.getrefcount(self) <= minimumReferenceCount

    @classmethod
    def _cleanLeastAccessedCache(cls, cacheLimit=None, isCacheStale=False):
        """Go through items in the cache to verify if they should be kept in memory.

        Args:
            cacheLimit (int, optional): the maximum amount of objects allowed in the cache
            isCacheStale (bool, optional): only remove objects that fall out of the limit and that haven't been accessed recently
        """

        leastAccessedLength = len(cls._LAST_ACCESSED_INSTANCES)
        cacheLimit = cls.CACHE_LIMIT if cacheLimit is None else cacheLimit

        # TODO: url:bugstar:6911806 Re-evaluate how to make this more performance friendly
        # Start from the end/oldest to avoid errors from editing the list in place.
        if leastAccessedLength > cacheLimit:
            # Doing this if statement outside the loop for performance
            # Code looks ugly, I know
            if isCacheStale:
                for index in xrange(1, leastAccessedLength-cacheLimit):
                    instance = cls._LAST_ACCESSED_INSTANCES[index * -1]
                    if instance.isCacheStale:
                        instance._clearDataCache()  # Added precaution to make sure all data is removed
                        instance._removeFromCache()
                        continue
            else:
                for index in xrange(1, leastAccessedLength - cacheLimit):
                    instance = cls._LAST_ACCESSED_INSTANCES[index * -1]
                    instance._clearDataCache()  # Added precaution to make sure all data is removed
                    instance._removeFromCache()

    @classmethod
    def _cleanDataCache(cls, start=None, isDataStale=False):
        """Go through items in the cache and clear their internal data out.

        Args:
            start (int, optional): item to start at in the list to clear.
            isDataStale (bool, optional): checks if the data is stale before clearing it
        """
        start = start or 0

        # Doing this if statement outside the loop for performance
        # Code looks ugly, I know

        if isDataStale:
            for index in xrange(start, len(cls._LAST_ACCESSED_INSTANCES) - start):
                instance = cls._LAST_ACCESSED_INSTANCES[index]
                if instance.isDataStale:
                    instance._clearDataCache()
        else:
            for index in xrange(start, len(cls._LAST_ACCESSED_INSTANCES) - start):
                instance = cls._LAST_ACCESSED_INSTANCES[index]
                instance._clearDataCache()

    def isCached(self):
        """Is this instance cached.

        Returns:
            bool
        """
        return self._isCached(self.server, self._id, self)

    def isValid(self):
        """Is this instance a valid.

        Note:
            This performs a query against the server to test the validity of the data

        Returns:
            bool
        """
        try:
            self.refresh()
            return False
        except:
            return True

    def _setup(self, index=None, data=None, server=None):
        """Setups the base properties used by this object. This method should be invoked in the reimplemented __init__ method.

        Keyword Args:
            index (int): id of the object from the server
            data (dictionary): data that represents this object
            server (rsg_core_py.abstracts.abstractConnection.AbstractConnection): the local server to set for this object
        """
        self._id = index if index is None else self._INDEX_TYPE(index)

        # properties to store
        self._fields = data or {self._INDEX_FIELD: self._id}  # The data retrieved from the server
        self._edits = {}  # The data that has been edited locally
        self._mutables = {} # cache for mutable objects such as lists so we can compare against the data from the server
        self._isMutableCast = {}  # check to see if  field that needs to have its values recast has done so
        self._mutableItemType = {}  # The type of that the data inside the mutable needs to be cast as
        self._server = server  # local cache of the server this is instance is connected to

        self._lastDataUpdate = None
        self._lastAccessed = None
        self._isUpdated = False
        self._isCachable = True  # Should this instance be cached

        if data is not None:
            self._lastDataUpdate = datetime.datetime.utcnow()
            self._lastAccessed = datetime.datetime.utcnow()
            self._isUpdated = True

    def _getattr(self, item, refresh=True, itemType=None):
        """Gets the non-mutable value for the given attribute.

        Arguments:
            item (str): name of the property being accessed

            refresh (bool, optional): populate the rest of the object with data from the server
            itemType (class, optional): The class that the item should be.

        Returns:
            object
        """
        self._setLastAccessedInstance(self)
        value = self._fields.get(item, None)
        if itemType and not isinstance(value, (itemType, types.NoneType)):
            value = itemType(value)
        # Gets the "dirty" data; this is data that has been edited locally
        if item in self._edits:
            if self._edits[item] != value:
                return self._edits[item]
            self._edits.pop(item)

        if not refresh or self.id is None or (self._isUpdated and not self.isDataStale):
            return value

        # grab the latest data from the server to populate the object with
        self.refresh()
        value = self._fields.get(item, None)
        if itemType and not isinstance(value, (itemType, types.NoneType)):
            return itemType(value)
        return value

    def _getmutable(self, item, mutableType, refresh=True, itemType=None):
        """Gets the mutable value for the given attribute based.

        Arguments:
            item (str): name of the property being accessed
            mutableType (type): the mutable data type for the attribute being requested

            refresh (bool, optional): populate the rest of the object with data from the server
            itemType (object, optional): a callable object to cast the data in a mutable as another type.
                                    This is intented for times when a mutable, particularly a list,
                                    has data that is expensive to discern ahead of time.

        Returns:
            object
        """
        self._setLastAccessedInstance(self)
        if mutableType is None:
            raise ValueError("No mutable type was given")

        value = self._fields.get(item, None)

        if value is None and self.id and not self._isUpdated:
            self.refresh()  # We have to do this refresh early to ensure fields has the correct data if it hasn't been called yet.
            value = self._fields.get(item, None) # Grab the data again

        if value is None:
            value = mutableType()
            self._fields[item] = value

        # Logic for casting
        if itemType is not None and not self._isMutableCast.get(item, False):
            value = self._castField(item, itemType)

        mutableValue = value
        # Get the mutable value if there is one
        if self.id is not None:
            inMutables = item in self._mutables
            if not inMutables and itemType is None:  # nested if statement to avoid calling the mutableType too much
                mutableValue = self._mutables.setdefault(
                    item, mutableType(mutableValue) if value is not None else mutableType()
                )
            elif not inMutables:  # We now that the content is a mutable and does not need to be recast
                mutableValue = self._mutables.setdefault(item, mutableValue if value is not None else mutableType())
            else:
                mutableValue = self._mutables[item]

        if not refresh or self.id is None or (self._isUpdated and not self.isDataStale):
            return mutableValue

        # grab the latest data from the server to populate the object with
        self.refresh()
        if self.id:
            # If the data is mutable get the appropriate data
            return self._mutables[item]
        return self._fields.get(item, None)

    def _castMutable(self, item, value, itemType):
        """Casts the items in the mutable object as the given types and sets them as the default values from.

        Args:
            item (str): the name of the field/property
            value (list): list of items that should be cast
            itemTypes (class): class or builder to cast the items as

        Returns:
            list
        """
        value = [itemType(content) for content in value]
        self._isMutableCast[item] = True
        self._mutableItemType[item] = itemType
        return value

    def _castField(self, item, itemType):
        """Convenience method for recasting the data retrieved from shotgun into its correct type.

        Args:
            item (str): the name of the field/property
            itemTypes (class): class or builder to cast the items as

        Returns:
            list
        """
        value = self._castMutable(item, self._fields[item] or [], itemType)
        self._fields[item] = value
        return value

    def _setattr(self, key, value, isMutable=False, refresh=True):
        """Sets the value of the property.

        If this object has gotten data from the server, the value is stored in a "dirty" dictionary to keep track if the data on the instance of the object is different from that on the server.

        Args:
            key (str): name of the property being accessed
            value (object): data being set

            isMutable(bool, optional): is the object mutable
            refresh (bool, optional): get the latest data from the server if it hasn't been retrieved yet or data is stale.
        """
        dictionary = self._fields
        if self.id is not None:
            dictionary = self._edits
            if isMutable:
                dictionary = self._mutables
            if refresh and (not self._isUpdated or self.isDataStale):
                self.refresh()

        dictionary[key] = value
        if not isMutable and key in self._edits and key in self._fields and self._edits[key] == self._fields[key]:
            self._edits.pop(key)

    def _getMutableDifferences(self):
        """Gets the difference between the source values and the mutable versions of the data stored by this class.

        This is so we can add back local changes after the data is repopulated from the server.

        Returns:
            dictionary
        """
        differences = {}
        for key, localValues in self._mutables.iteritems():
            source = self._fields.get(key, None)
            if isinstance(localValues, list):
                source = source or []
                differences[key] = Differences(type_=list)
                differences[key].append = [value for value in localValues if value not in source]
                differences[key].remove = [value for value in source if value not in localValues]

            elif isinstance(localValues, dict):
                source = source or {}
                differences[key] = Differences(type_=dict)
                differences[key].append = {
                    key_: value for key_, value in localValues.iteritems() if key_ in source and source[key_] != value
                }
                differences[key].remove = [key_ for key_ in source.iterkeys() if key_ not in localValues]
        return differences

    def _refreshMutables(self, differences):
        """Updates mutable data stored by the object that have local edits ,such as lists, so that new changes are included on top of local changes.

        Arguments:
             differences (dict): dictionary with the difference objects for updating mutable objects
        """
        # Iterate over items that will retain local edits made by the user
        for attrName, values in self._mutables.iteritems():
            # If a mutable piece of data is empty on a server, it may not be returned.
            # Thus we must set the default empty value here
            difference = differences.get(attrName, None)

            if isinstance(values, (list, tuple)):
                source = self._fields.get(attrName, None)

                if self._isMutableCast.get(attrName, False):
                    source = self._castField(attrName, self._mutableItemType[attrName])

                if source is None:
                    source = []
                    self._fields[attrName] = source

                if difference is None:
                    continue

                updatedValues = list(source)

                del values[:]  # sneaky python way to clear a list; this is the clear function in python 3.x
                values[:] = updatedValues  # populate the editable list with the contents of the new source

                for value in difference.remove:
                    if value in values:
                        values.remove(value)  # add items that were removed locally
                values.extend(difference.append)  # add items that were added locally

            elif isinstance(values, dict):
                source = self._fields.get(attrName, None)
                if source is None:
                    source = {}
                    self._fields[attrName] = source

                if difference is None:
                    continue

                values.update(source)  # populate the editable dictionary with the contents of the new source

                for item in difference.remove:
                    values.pop(item, None)  # remove items that were removed locally
                values.update(difference.append)  # add items that were added locally

    def _matchMutables(self):
        """Match mutables to their current values in fields.

        This method should only be used when refreshing after data was completely cleared.
        """
        for key in self._mutables.iterkeys():
            value = self._fields.get(key, None)
            if isinstance(value, dict):
                self._mutables[key].update(value)
            elif isinstance(value, (list, tuple)):
                self._mutables[key][:] = list(value)

    def _isFieldDirty(self, field):
        """Does the given field have any local changes.

        Arguments:
            field (str): name of the field to check

        Returns:
            bool
        """
        if field in self._edits and self._edits[field] != self._fields.get(field, None):
            return True
        elif field in self._mutables and self._mutables[field] != self._fields.get(field, None):
            return True
        return False

    def _cache(self):
        """Caches the object so it returned in the future when the current id is used."""
        if self._isCachable and self.id is not None and self.server is not None and (self.server, self.id) not in self._INSTANCES:
            self.__class__._INSTANCES[(self.server.uuid, self.id)] = self
            self._setLastAccessedInstance(self)

    def _removeFromCache(self, key=None, force=False, minimumReferenceCount=3):
        """Removes this object from the cache if it is safe to be picked up by the garbage collection.

        Args:
            key (tuple, optional): key used in the cache dictionary to
            force (bool, optional): force the removal of the payload from the cache.
            minimumReferenceCount(int, optional): The minimum amount of in memory references allowed before it is purged from memory.
        """
        remove = force or (self.isDataStale and self._isRemovable(minimumReferenceCount))
        if remove:
            self._isCachable = False
            self._clearDataCache()
            self.__class__._LAST_ACCESSED_INSTANCES.remove(self)
            self.__class__._INSTANCES.pop(key or (self.server.uuid, self.id), None)
            return True
        return False

    @classmethod
    def _clearCache(cls):
        """This clears out the memory caches of the objects that have been queried from the server.

        This class is for debugging purposes.
        """
        cls._cleanLeastAccessedCache(cacheLimit=0)
        cls._INSTANCES = {}
        cls._LAST_CACHE_CLEAR = datetime.datetime.utcnow()

    def _clearDataCache(self):
        """Clear out the internal data cache from the object and resets the isUpdated field."""
        self._fields = {self._INDEX_FIELD: self._fields.get(self._INDEX_FIELD, None)}
        self._isUpdated = False
        self._lastDataUpdate = None
        self.removeEdits()

    @classmethod
    def _removeStalePayloads(cls, force=False, instance=None):
        """Removes stale payloads ready for garbage collection from the cache.

        Note:
            This method is meant to be called within the refresh method, and must be implemented there as the last
            function called. The refresh method gets called rather frequently internally and thus this method has an
            internal timer to avoid iterating through the list of cached data constantly which overall will slow down
            applications that use this.

        Args:
            force (bool, optional): force the the removal of stale data
            instance (AbstractPayload, optional): payload to avoid removing when invoking this function

        Return:
            bool
        """
        if force or (datetime.datetime.utcnow() - cls._LAST_CACHE_CLEAR).seconds > cls.CACHE_EXPIRY:
            for key, value in cls._INSTANCES.items():
                if value != instance:
                    value._clearDataCache()
                    value._removeFromCache(key)
            cls._LAST_CACHE_CLEAR = datetime.datetime.utcnow()
            return True
        return False

    @classmethod
    def key(cls):
        """Key to use to resolve to the class for data returned from the server.

        This method is used by the Mapping metaclass.

        Returns:
            str
        """
        raise NotImplementedError

    @classmethod
    def className(cls):
        """Convenience method for getting the name of the class.

        Returns:
            str
        """
        return cls.__name__

    @property
    def client(self):
        """The client that the connection that this payload uses connects to.

        Returns:
            str
        """
        return self.server.client

    @property
    def isDataStale(self):
        """Has the local data become stale.

        This is determined by checking if the expiry time has been exceeded by the amount of time since the data was pulled.

        Returns:
            bool
        """
        elapsedTime = 0
        if self._lastDataUpdate is not None:
            elapsedTime = (datetime.datetime.utcnow() - self._lastDataUpdate).seconds

        return elapsedTime > self.DATA_EXPIRY

    @property
    def isCacheStale(self):
        """Has the object become stale from not being used.

        This is determined by checking if the cache expiry time has been exceeded by the amount of time since the last time it was used.

        Returns:
            bool
        """
        elapsedTime = 0
        if self._lastAccessed is not None:
            elapsedTime = (datetime.datetime.utcnow() - self._lastAccessed).seconds

        return elapsedTime > self.CACHE_EXPIRY

    @property
    def hasEdits(self):
        """Does this payload have any local changes.

        Returns:
            bool
        """
        edits = [value != self._fields.get(key, None) for key, value in self._edits]
        mutables = [value != self._fields.get(key, None) for key, value in self._mutables]
        edits.extend(mutables)
        if edits:
            return any(edits)
        return False

    @property
    def server(self):
        """The server that is used by this payload, returns the default server if no server has been set.

        Returns:
            rsg_core_py.abstracts.abstractConnection.AbstractConnection
        """
        return self._server

    @server.setter
    def server(self, value):
        """Sets the server that is used by this payload.

        Args:
            value (rsg_core_py.abstracts.abstractConnection.AbstractConnection): server to use for this payload
        """
        self._server = value

    def copy(self):
        """Makes a copy in memory of this entity.

        The user must invoke create on the newly copied entity to add it to the server.

        Returns:
            rsg_core_py.abstracts.abstracPayload.AbstractPayload
        """
        if self.IS_READONLY:
            raise self._IS_READONLY_ERROR(
                "Copy operation is not permmitted as {} are read only".format(self.__class__.__name__)
            )

        if not self._isUpdated and self.id:
            self.refresh()
        copy = self.__class__(self._server)
        values = self._fields.copy()
        values.update(self._edits)
        copy._fields = values
        return copy

    def removeEdits(self, fields=None):
        """Remove local edits to the object.

        Arguments:
            fields (list): only remove local edits to these fields
        """
        mutables = []
        if not fields:
            self._edits = {}
            mutables = self._mutables.iteritems()

        else:
            for field in fields:
                self._edits.pop(field, None)
                value = self._mutables.get(field, None)
                if value:
                    mutables.append((field, value))

        # Clear out the mutable data
        for key, value in mutables:
            if isinstance(value, list):
                del value[:]  # clear list
                value[:] = self._fields.get(key, None) or []
            elif isinstance(value, dict):
                value.clear()
                value.update(self._fields.get(key, None) or {})

    def create(self):
        """Performs the query to the server required to create the data in this object.

        This method should be reimplemented in inherited classes
        """
        raise NotImplementedError

    def refresh(self):
        """Performs the query to the server required to get the data to fill this object with.

        This method should be reimplemented in inherited classes.
        This method should call _removeStalePayloads right before exiting this code block.
        """
        raise NotImplementedError

    def parse(self, data):
        """Parses the data that is being used by the class converts it to the correct data types or classes as needed.

        This method should be reimplemented in inherited classes.

        Args:
            data (dict): the data to go through and convert as necessary to the appropriate data types expected by this
                         class
        """
        raise NotImplementedError

    def commit(self):
        """Performs the query to the server required to update the server with data in this object.

        This method should be reimplemented in inherited classes
        """
        raise NotImplementedError

    def commitFields(self, fields):
        """Updates specific fields on the server.

        Args:
            fields (list): list of fields to update
        """
        raise NotImplementedError

    def delete(self):
        """Performs the query to the server to delete the data from this object.

        This method should be reimplemented in inherited classes
        """
        raise NotImplementedError
