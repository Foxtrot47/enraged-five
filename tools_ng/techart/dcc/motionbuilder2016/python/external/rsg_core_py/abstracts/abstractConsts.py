"""
Abstract class for constants.
"""
import inspect

from rsg_core_py.decorators import memoize


class _AbstractConstsMeta(type):
    """
    Internal metaclass that enables iteration and auto-resolving of values without requiring an instance of the class.
    """
    def __iter__(self):
        return self._classIter()

    def __new__(self, name, bases, attrs):
        val = super(_AbstractConstsMeta, self).__new__(self, name, bases, attrs)
        val._resolveAutos()
        return val


class Auto(object):
    """
    Placeholder object for use with AbstractConsts.
    """
    pass


class AbstractConsts(object):
    """
    Abstract class for const groups.

    Features:
        * Similar iteration features as Enum34, but with very little overhead.
            * Enum34's Python2&3 compatible metaclass has a lot more complexity.
        * Drop in replacement for the base class for our existing const classes.
        * Member order is guaranteed and based on alphanum sort of the class attr values (same as Enum34).

    Examples:
        >>> class ExampleConst(AbstractConsts):
        ...     DOG = "dog"
        ...     BIRD = "bird"
        ...     CAT = "cat"

        # Normal access.
        >>> ExampleConst.DOG
        'dog'

        # Get all values in a tuple.
        >>> tuple(ExampleConst)
        ('bird', 'cat', 'dog')

        # Iterate over all member in order.
        >>> for member in ExampleConst:print member
        bird
        cat
        dog

        # Get an indexed mapping to populate a QCombobox.
        >>> dict(enumerate(ExampleConst))
        {0: 'bird', 1: 'cat', 2: 'dog'}

        # Get inverse mapping.
        >>> {v: k for k, v in enumerate(ExampleConst)}
        {'dog': 2, 'bird': 0, 'cat': 1}

    Warnings:
        * Be cautious of using Auto() to populate values when you need those values to be consistent across multiple
        instances of Python (especially on different operating systems).
    """
    __metaclass__ = _AbstractConstsMeta

    @memoize.memoize
    @classmethod
    def getAttrNameByValue(cls, val):
        """
        Returns the attribute string matching its value.

        Args:
            val (object): the value used to match the attributes.

        Returns:
            str: the string representation of the attribute.
        """
        try:
            return [key for key, value in cls.__dict__.iteritems() if value == val and not key.startswith("_")][0]
        except:
            return None

    @memoize.memoize
    @classmethod
    def __getOrderedValues(cls):
        """
        Get an ordered list of the class' const members' values.

        Returns:
            list
        """
        return sorted([value for key, value in cls.__dict__.iteritems()
                       if not key.startswith("_") and not inspect.isfunction(value)])

    @classmethod
    def _resolveAutos(cls):
        """
        Resolve the values for all const members.
        """
        currentIdx = 0
        for key, value in cls.__dict__.iteritems():
            if isinstance(value, Auto) is True:
                setattr(cls, key, currentIdx)
                currentIdx += 1

    @classmethod
    def _classIter(cls):
        """
        Yield the values of attrs that are considered const members.

        Yields:
            object
        """
        for value in cls.__getOrderedValues():
            yield value


if __name__ == '__main__':
    # Demo the examples in the class docstring.
    class ExampleConst(AbstractConsts):
        DOG = "dog"
        BIRD = "bird"
        CAT = "cat"
        _CATDOG = "catDog"
        def exampleMethod(self):
            return 1

    print ExampleConst.DOG, ExampleConst.BIRD, ExampleConst.CAT

    for member in ExampleConst:
        print member

    print tuple(ExampleConst)
    print dict(enumerate(ExampleConst))
    print {v: k for k, v in enumerate(ExampleConst)}
