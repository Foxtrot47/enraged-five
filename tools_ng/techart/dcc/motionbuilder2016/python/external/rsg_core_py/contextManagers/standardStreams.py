"""
Context managers that work with the standard streams: stdin, stdout, stderr.
"""
import sys
import cStringIO


class CaptureStdout(list):
    """
    Context manager for capturing a stream during a block of code and returning it as a list.

    Useful for consuming output from code that writes to stdout instead of raising errors or returning results.

    Examples:
        >>> with CaptureStdout() as output:
        ...     print "Line 1"
        ...     print "Line 2"
        ...     print "Line 3"
        >>> output
        ['Line 1', 'Line 2', 'Line 3']
    """
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = cStringIO.StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio  # free up some memory
        sys.stdout = self._stdout


if __name__ == '__main__':
    with CaptureStdout() as output:
        print "Line 1"
        print "Line 2"
        print "Line 3"
    print output
