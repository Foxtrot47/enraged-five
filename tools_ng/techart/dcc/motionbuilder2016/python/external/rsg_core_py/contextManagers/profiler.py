"""
Profiling related utilities: Timer, Profile
"""
from __future__ import print_function
from timeit import default_timer
import os
import pstats
import cProfile
import tempfile
import functools


class Time(object):
    """
    Context manager for timing specific blocks of code via wall clock time (not CPU time).

    Supports passing in custom print/logging function - helpful for including timing info in existing logging systems.

    Examples:
        # Basic usage.
        >>> from rsg_core_py.contextManagers import profiler
        >>> with profiler.Time():
        >>>    time.sleep(.5)
        total time elapsed: 1.50144195557 seconds

        # Advanced usage - message prefix, multiple measurements within a context, and custom logging function.
        >>> import sys
        >>> from rsg_core_py.contextManagers import profiler
        >>> def timedFunc():
        >>>     print("doing some untimed things")
        >>>     with Time(msgPrefix=sys._getframe().f_code.co_name, logFunc=LOG.debug) as timer:
        >>>         time.sleep(1.5)
        >>>         timer.log()
        >>>         time.sleep(1.5)
        >>>         timer.log()
        >>>     print("doing more untimed things")
        >>> timedFunc()
        doing some untimed things
        DEBUG:Test:timedFunc time elapsed: 1.50154995918 seconds
        DEBUG:Test:timedFunc time elapsed: 3.00331497192 seconds
        DEBUG:Test:timedFunc total time elapsed: 3.00353884697 seconds
        doing more untimed things

    Note:
        In testing, this approach adds about <.05 milliseconds to the total elapsed time value when compared with the
        result of just using time.time() to get the difference between start and end. This is negligible for most tasks.

    Keyword Arguments:
        msgPrefix (str): If specified, will prepend to verbose message.
        logFunc (func): Allows for custom logging function to be called.
            Default: print function.
        verbose (bool): If True, will call the logger function with the message.
    """

    def __init__(self, msgPrefix=None, logFunc=None, verbose=True):
        self._timer = default_timer
        self.msgPrefix = msgPrefix or ""
        self._baseMsg = "time elapsed: {} seconds"
        self.verbose = verbose
        self.logFunc = logFunc
        if logFunc is None:
            self.logFunc = print
        self.start = None
        self.end = None

    def __call__(self):
        return self._timer()

    def __enter__(self):
        self.start = self()
        return self

    def __exit__(self, excType, excValue, excTraceback):
        self.end = self()

        if self.verbose:
            self.msgPrefix = " ".join([token for token in [self.msgPrefix, "total"] if token])
            self.log()

    def __str__(self):
        return "Time elapsed: {} seconds".format(self.formatTime(self.elapsed))

    def formatTime(self, time):
        return "{:.10f}".format(time)

    def log(self):
        msgTemplate = self._baseMsg
        if self.msgPrefix:
            msgTemplate = "{} {}".format(self.msgPrefix, self._baseMsg)
        self.logFunc(msgTemplate.format(self.formatTime(self.elapsed)))

    @property
    def elapsed(self):
        """
        The current elapsed time.

        Allows for getting the elapsed time at any point from within the scope or the total time outside of scope.
        """
        if self.end is None:
            # Inside the context manager scope.
            return (self() - self.start)
        else:
            # Outside the context manager scope.
            return (self.end - self.start)


class Profile(object):
    """
    Context manager that profiles the execution of a code block.

    Examples:
        >>> from rsg_core_py.contextManagers import profiler
        >>> with profiler.Profile():
        >>>    time.sleep(.5)
        Thu Sep 13 18:43:11 2018    /tmp/profileDump.prof

                 3 function calls in 0.100 seconds

           Ordered by: cumulative time

           ncalls  tottime  percall  cumtime  percall filename:lineno(function)
                1    0.100    0.100    0.100    0.100 {time.sleep}
                1    0.000    0.000    0.000    0.000 profiler.py:66(__exit__)
                1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    """
    name = "name"  # sort by the name of the module
    file = "file"  # sort by the name of the file
    time = "time"  # sort by the time it took to run one call to this function
    calls = "calls"  # sort by the number of times the function was called
    primitiveCalls = "pcalls"  # sort by primitive call count
    cumulativeTime = "cumtime"  # sort by total time it took to run all calls to this function
    line = "line"  # sort by line number
    nameFileLine = "nfl"  # sort by name , filename and line number
    standard = "stdname"  # sort by standard name

    def __init__(self, path=None, sortBy='cumulative', print_=True):
        """
        Constructor

        Arguments:
            path (string): directory to save output too, default is None
            sortBy (string): order to sort profile stats by
            print_ (boolean): print the results to the console, default is False
        """
        self._path = path or tempfile.gettempdir()
        self._textPath = os.path.join(self._path, "profileDump.txt")
        self._profPath = os.path.join(self._path, "profileDump.prof")
        self._sortBy = sortBy
        self._print = print_

    def __call__(self, func):
        """
         Overrides built-in method
         Allows the context manager to be used as a decorator

         Arguments:
             func (function): decorated function
        """
        @functools.wraps(func)
        def inner(*args, **kwargs):
            with self:
                result = func(*args, **kwargs)
            return result
        return inner

    def __enter__(self):
        """
        Enable
        """
        self.prof = cProfile.Profile()
        self.prof.enable()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.prof.disable()
        # create file
        self.prof.dump_stats(self._profPath)
        with open(self._textPath, "w+") as handle:
            # redirect output from the console to the file
            stats = pstats.Stats(self._profPath, stream=handle)
            stats.strip_dirs()
            stats.sort_stats(self._sortBy)
            stats.print_stats()

        if self._print:
            with open(self._textPath, "r") as handle:
                content = handle.read()
                print(content)


if __name__ == '__main__':
    import sys
    import time
    import logging
    LOG = logging.getLogger("Test")
    logging.basicConfig(level=logging.DEBUG)


    # TIMER
    # Test average use case.
    with Time():
        time.sleep(.5)

    # Test with verbose on and custom logging function.
    with Time(logFunc=LOG.debug):
        time.sleep(.5)

    # Test with verbose off.
    with Time(verbose=False) as timer:
        time.sleep(.5)
        print(timer.elapsed)

    # Test with msgPrefix, multiple measurements within a context, and custom logging function.
    def timedFunc():
        print("doing some untimed things")
        with Time(msgPrefix=sys._getframe().f_code.co_name, logFunc=LOG.debug) as timer:
            time.sleep(.5)
            timer.log()
            time.sleep(.5)
            timer.log()
        print("doing more untimed things")
    timedFunc()


    # PROFILE
    # Test average use case.
    with Profile(path=os.path.dirname(__file__)):
        print("doing some things")  # <-- should get excluded from the output by default.
        time.sleep(.5)
        os.path.exists(tempfile.gettempdir())
        time.sleep(.5)
