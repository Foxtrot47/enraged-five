"""
Contains the memoize and memoizeWithExpiry decorators.
"""
import os
import atexit
import cPickle
import functools
import datetime


class memoize(object):
    """
    Decorator. Caches a function's return value each time it is called.
    If called later with the same arguments, the cached value is returned
    (not reevaluated).

    Example:

        @memoize
        def addNumbers(a, b):
            print 'Calculating {0} + {1}'.format(a,b)
            return a+b

        # Initial Calculation
        print addNumbers(1, 2)
        # Re-use the data
        print addNumbers(1, 2)
        # New Calculation (Different order of args)
        print addNumbers(2, 1)
        # another new calculation
        print addNumbers(4, 5)
        # Reuse the cache again
        print addNumbers(2, 1)

        #Also works for class methods!
        class MyMathClass(object):
            @memoize
            @classmethod
            def addNumbers(cls, a, b):
                print 'Calculating {0} + {1}'.format(a,b)
                return a+b

        # Initial Calculation
        print MyMathClass.addNumbers(1, 2)
        # Re-use the data
        print MyMathClass.addNumbers(1, 2)
        # New Calculation (Different order of args)
        print MyMathClass.addNumbers(2, 1)
        # another new calculation
        print MyMathClass.addNumbers(4, 5)
        # Reuse the cache again
        print MyMathClass.addNumbers(2, 1)

        #Also works for static methods!
        class MyMathClass(object):
            @memoize
            @staticmethod
            def addNumbers(a, b):
                print 'Calculating {0} + {1}'.format(a,b)
                return a+b

        # Initial Calculation
        print MyMathClass.addNumbers(1, 2)
        # Re-use the data
        print MyMathClass.addNumbers(1, 2)
        # New Calculation (Different order of args)
        print MyMathClass.addNumbers(2, 1)
        # another new calculation
        print MyMathClass.addNumbers(4, 5)
        # Reuse the cache again
        print MyMathClass.addNumbers(2, 1)

    Example which shows the issues of it:

        import datetime
        @memoize
        def getTime():
            return datetime.datetime.now()

        for i in xrange(10):
            print getTime()

    """
    def __init__(self, func):
        """
        Constructor

        args:
            func (Function): The function to decorate
        """
        self.isStaticmethod = False
        self.isClassmethod = False
        if isinstance(func, classmethod):
            self.isClassmethod = True
            func = func.__func__

        if isinstance(func, staticmethod):
            self.isStaticmethod = True
            func = func.__func__

        self.func = func
        self.cache = {}

    def __call__(self, *args, **kwargs):
        """
        Decorator is called with the args and kwargs for the decorated function
        """
        if self.isStaticmethod is True:
            args = args[1:]
        if self.isClassmethod is True:
            args = tuple([self._classObject] + list(args[1:]))
        try:
            masterArgs = tuple([value for value in args] + [(key, value) for key, value in kwargs.iteritems()])
            hash(masterArgs)
        except TypeError:
            try:
                masterArgs = cPickle.dumps(masterArgs)
            except:
                # uncacheable. a list, for instance.
                # better to not cache than blow up.
                return self.func(*args, **kwargs)
        if masterArgs in self.cache:
            return self.cache[masterArgs]
        else:
            value = self.func(*args, **kwargs)
            self.cache[masterArgs] = value
            return value

    def __repr__(self):
        """
        Return the function's name.
        """
        return self.func.__name__

    def __get__(self, obj, objtype):
        """
        Support instance methods.
        """
        self._classObject = objtype
        return functools.partial(self.__call__, obj)


class memoizeWithExpiry(object):
    """
    Decorator. Caches a function's return value each time it is called.
    If called later with the same arguments, the cached value is returned
    (not reevaluated). Cached values will be removed after the specified time.

    Example:

        # Cached
        @memoizeWithExpiry(expiry=120)
        def addNumbers(a, b):
            print 'Calculating {0} + {1}'.format(a,b)
            return a+b

        # Inital Calculation
        print addNumbers(1, 2)
        # Re-use the data
        print addNumbers(1, 2)
        # New Calculation (Different order of args)
        print addNumbers(2, 1)
        # another new calculation
        print addNumbers(4, 5)
        # Reuse the cache again
        print addNumbers(2, 1)

    Example which shows the issues of it:

        import datetime
        @memoizeWithExpiry(expiry=120)
        def getTime():
            return datetime.datetime.now()

        for i in xrange(10):
            print getTime()
    """

    def __init__(self, expiry):
        """
        Constructor

        Args:
            func (Function): The function to decorate.
            expiry (int): The time in seconds for which cached data will expire.
        """
        self.cache = {}
        self._timeouts = {}
        self.expiry = expiry

    def __call__(self, func):
        """
        Decorator is called with the args and kwargs for the decorated function.
        """
        def wrapper(*args, **kwargs):
            try:
                masterArgs = tuple([value for value in args] + [(key, value) for key, value in kwargs.iteritems()])
                hash(masterArgs)
            except TypeError:
                try:
                    masterArgs = cPickle.dumps(masterArgs)
                except:
                    # uncacheable. a list, for instance.
                    # better to not cache than blow up.
                    return func(*args, **kwargs)

            if masterArgs in self.cache:
                # Check for and remove expired data.
                value, timestamp = self.cache[masterArgs]
                age = datetime.datetime.now() - timestamp
                if age.seconds < self.expiry:
                    return value
                else:
                    self.cache.pop(masterArgs)
            # Value not in cache or has expired.
            value = func(*args, **kwargs)
            timestamp = datetime.datetime.now()
            self.cache[masterArgs] = (value, timestamp)
            return value

        return wrapper

    def __repr__(self):
        """
        Return the function's docstring.
        """
        return self.func.__doc__

    def __get__(self, obj, objtype):
        """
        Support instance methods.
        """
        return functools.partial(self.__call__, obj)


class memoizeWithLastModifiedTime(object):
    """
    Decorator. Caches a function's return value each time it is called.
    If called later and file being monitored hasn't changed, then the cached value is returned
    (not reevaluated). Cached values will be removed if the file being monitored is updated.

    Example:

        # Cached
        @memoizeWithFileMonitor(path='path/to/some/file')
        def getContents():
            with open('path/to/some/file') as handle:
                return handle.readlines()

        # Inital Calculation
        print getContents()
        # Re-use the data
        print getContents()

        with open('path/to/some/file') as handle:
            handle.write("New Content")

        # New Calculation (File recently changed)
        print getContents()
        # Reuse the cache again
        print getContents()
    """

    def __init__(self, path):
        """
        Initializer

        Args:
            func (Function): The function to decorate.
            path (str): Path to monitor for local changes
        """
        self.cache = {}
        self.path = path

    def __call__(self, func):
        """
        Decorator is called with the args and kwargs for the decorated function.
        """
        def wrapper(*args, **kwargs):
            try:
                masterArgs = tuple([value for value in args] + [(key, value) for key, value in kwargs.iteritems()])
                hash(masterArgs)
            except TypeError:
                try:
                    masterArgs = cPickle.dumps(masterArgs)
                except:
                    # uncacheable. a list, for instance.
                    # better to not cache than blow up.
                    return func(*args, **kwargs)

            if masterArgs in self.cache:
                # Check for and remove expired data.
                value, timestamp = self.cache[masterArgs]

                lastModifiedTime = timestamp
                if os.path.exists(self.path):
                    lastModifiedTime = os.path.getmtime(self.path)

                if lastModifiedTime == timestamp:
                    return value
                else:
                    self.cache.pop(masterArgs)

            # Value not in cache or has expired.
            value = func(*args, **kwargs)
            if os.path.exists(self.path):
                lastModifiedTime = os.path.getmtime(self.path)
            self.cache[masterArgs] = (value, lastModifiedTime)
            return value

        return wrapper

    def __repr__(self):
        """
        Return the function's docstring.
        """
        return self.func.__doc__

    def __get__(self, obj, objtype):
        """
        Support instance methods.
        """
        return functools.partial(self.__call__, obj)


class stubCache(object):
    """
    Decorator. Caches a function's return value each time it is called.
    Created mostly to remove the first argument upon call which is self and
    to generate the stub database for unit testing.
    """

    CREATE_DB_STUB_ENV = "TA_SAVE_DB_STUB"

    def __init__(self, filePath):
        """Initializer

        Args:
            filePath (str): Path to where to save the stub data file.
        """
        self.cache = {}
        self.filePath = filePath
        atexit.register(self._saveCache)

    def __call__(self, func):
        if os.getenv(self.CREATE_DB_STUB_ENV) and os.getenv(self.CREATE_DB_STUB_ENV).lower() == "true":
            print("Adding to stub cache...")
            isStaticmethod = False
            isClassmethod = False
            if isinstance(func, classmethod):
                isClassmethod = True
                func = func.__func__

            if isinstance(func, staticmethod):
                isStaticmethod = True
                func = func.__func__
            def wrapper(*args, **kwargs):
                """ Decorator is called with the args and kwargs for the decorated function."""
                if isStaticmethod is True:
                    args = args[1:]
                if isClassmethod is True:
                    args = tuple([self._classObject] + list(args[1:]))
                try:
                    masterArgs = tuple([value for value in args] + [(key, value) for key, value in kwargs.items()])
                    hash(masterArgs)
                except TypeError:
                    try:
                        masterArgs = cPickle.dumps(masterArgs[1:])
                    except Exception as error:
                        # uncacheable. a list, for instance.
                        # better to not cache than blow up.
                        return func(*args, **kwargs)
                if masterArgs in self.cache:
                    return self.cache[masterArgs]
                else:
                    value = func(*args, **kwargs)
                    self.cache[masterArgs] = value
                    return value

            return wrapper

        # Run as normal.
        return func

    def __repr__(self):
        """ Return the function's name."""
        return self.func.__name__

    def __get__(self, obj, objtype):
        """ Support instance methods."""
        self._classObject = objtype
        return functools.partial(self.__call__, obj)

    def _saveCache(self):
        """ Saves the pickled data to the specified file if the environment variable is set to do so. """
        if os.getenv(self.CREATE_DB_STUB_ENV) and os.getenv(self.CREATE_DB_STUB_ENV).lower() == "true":
            # Clear existing file before saving to make sure the data is all current.
            if os.path.exists(self.filePath) and os.path.isfile(self.filePath):
                os.remove(self.filePath)

            with open(self.filePath, "wb") as fh:
                cPickle.dump(self.cache, fh)
            print("Saved a mock stub database data in a file: {}".format(self.filePath))


if __name__ == "__main__":
    import time

    def constructorMethod():
        print "constructing!"

    @memoize
    def testMethod(myArgs, myKwarg=None):
        constructorMethod()

    @memoizeWithExpiry(expiry=2)
    def testTimedMethod(myArgs, myKwarg=None):
        constructorMethod()

    print "Normal Methods"
    testMethod(1)
    testMethod(2)
    testMethod(1, myKwarg=1)
    testMethod(1, myKwarg=[1, 2, 3, 4])
    testMethod(1, myKwarg=2)
    testMethod(1, myKwarg=[1, 2, 3, 4])
    testMethod(1, myKwarg=2)

    print "Timed Methods"
    testTimedMethod(1)
    testTimedMethod(1)
    time.sleep(2)
    testTimedMethod(1)
    testTimedMethod(1)

    print "Sample code - Classmethods"
    #Also works for class methods!
    class MyMathClass(object):
        @memoize
        @classmethod
        def addNumbers(cls, a, b):
            print 'Calculating {0} + {1}'.format(a,b)
            return a+b

    # Initial Calculation
    print MyMathClass.addNumbers(1, 2)
    # Re-use the data
    print MyMathClass.addNumbers(1, 2)
    # New Calculation (Different order of args)
    print MyMathClass.addNumbers(2, 1)
    # another new calculation
    print MyMathClass.addNumbers(4, 5)
    # Reuse the cache again
    print MyMathClass.addNumbers(2, 1)

    print "Sample code - Staticmethods"
    #Also works for static methods!
    class MyMathClass(object):
        @memoize
        @staticmethod
        def addNumbers(a, b):
            print 'Calculating {0} + {1}'.format(a,b)
            return a+b

    # Initial Calculation
    print MyMathClass.addNumbers(1, 2)
    # Re-use the data
    print MyMathClass.addNumbers(1, 2)
    # New Calculation (Different order of args)
    print MyMathClass.addNumbers(2, 1)
    # another new calculation
    print MyMathClass.addNumbers(4, 5)
    # Reuse the cache again
    print MyMathClass.addNumbers(2, 1)

    #Class attribute test!
    class Tableau20RGB(object):
        BLUE = (31, 119, 180)

        @memoize
        @classmethod
        def getColoursMemoized(cls):
            """
            Get all colours as a list.

            Returns:
                list of tuple of int
            """
            print "Getting!"
            colours = (
                cls.BLUE,
            )
            return colours

    print Tableau20RGB.getColoursMemoized()
    print Tableau20RGB.getColoursMemoized()
    print Tableau20RGB.getColoursMemoized()
