"""Decorator for communicating deprecation warnings to other developers."""
import logging


logger = logging.getLogger(__name__)


def deprecated(msg):
    """Decorator for logging a deprecation warning each time the method is called.

    Examples:

        @deprecated("Use module.newMethod instead.")
        def oldMethod(a):
            return a + 1

        def newMethod(a):
            return a + 2

    Notes:
        * The default logging level is higher than debug; using this without setting up handlers will not trigger
          a warning/error, but it also means that no messages will be displayed.
            * This is still preferable to print, which can cause artists to think that there are errors in the tools.
    """

    def deprecatedDec(func):
        def funcWrapper(*args, **kwargs):
            logger.debug("Function '{}' in '{}' has been deprecated. {}".format(func.__name__, func.__module__, msg))
            return func(*args, **kwargs)

        return funcWrapper

    return deprecatedDec
