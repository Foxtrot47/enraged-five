"""
Decorator for converting any decorated class into a singleton

Example:

from rsg_core_py.decorators import singleton

@singleton.Singleton
class MyClass(object):
    pass

a = MyClass()
a.test = "Harry"

b = MyClass()
# print what we stored before
print a.test
"""


class singleton(object):
    """
    Converts a decorated class into a singleton.
    It does this by making sure only the same instance is returned when the class is called.
    """
    def __init__(self, _class):
        """
        Constructor

        Arguments:
            _class (class): class to convert into a singleton
        """
        self._class = _class
        self._instance = None

    def __call__(self, *args, **kwargs):
        """
        Overrides built-in method. When the class is called, if an instance was already created, return that instance.
        """
        if self._instance is None:
            self._instance = self._class(*args, **kwargs)

        return self._instance

    def __getattr__(self, item):
        """
        Overrides built-in method; Gets information from the decorated class rather then the Singleton decorator class

        Arguments:
            item (string): name of the variable to query from the class

        Return:
            object
        """
        return getattr(self._class, item)
