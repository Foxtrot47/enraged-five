"""
Decorators specifically for the database module. These are not generic enough to live in rsg_core_py.decorators
"""
import functools
import multiprocessing
from multiprocessing import pool


def connectionCheck(func, *parameters):
    """
    decorator for the database class, to checks if there is a connection to the database

    Arguments:
        func (method): method that is being decorated, it is automatically passed into this method.
        warning (string): The warning message that you want to print out when the connection fails.
                          default warning is : "There is no connection to the database."
        *parameters: anything
            For catching extra arguments passed into the decorator

    Return:
        results from func
    """

    # First wrapper is to grab arguments passed into the decorator
    @functools.wraps(func)
    def wrapper(instance=None, *args, **kwargs):
        # Call function if there is a connection to the database
        if not instance.isConnected:
            instance.connect(force=True)

        return func(instance, *args, **kwargs)

    return wrapper


def timeout(timeout, exception=Exception):
    """
    Time out database queries when they take too long

    Arguments:
        timeout (int): the number of seconds to wait before stopping a query operation
        exception (Exception): the type of exception to raise when a function times out
    """
    def wrap(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            threadPool = pool.ThreadPool(processes=1)
            try:
                asyncResult = threadPool.apply_async(func, args, kwargs)
                return asyncResult.get(timeout)
            except multiprocessing.TimeoutError:
                raise exception("Timeout hit while trying to connect to database. "
                                "function:{}, args:{}, kwargs:{}".format(str(func), str(args), str(kwargs)))
            finally:
                threadPool.close()
        return wrapper
    return wrap