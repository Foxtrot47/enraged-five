class classProperty(object):
    """
    Decorator to make a function a class level property
    """
    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        return self.fget(owner_cls)
