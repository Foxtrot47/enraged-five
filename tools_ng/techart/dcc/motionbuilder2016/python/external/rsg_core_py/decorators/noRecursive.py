"""
noRecursive Decorator
"""
import functools


class noRecursive(object):
    """
    Decorator. Does not allow the method be run again while it is still running that method
    Will not allow recursion


    Example:

        @noRecursive
        def testDerp1(depth=0):
            if depth == 5:
                    return
            print depth
            testDerp1(depth+1)

        def testDerp2(depth=0):
            if depth == 5:
                    return
            print depth
            testDerp2(depth+1)

        print 'Normal'
        testDerp2()
        print 'Non-Recursive'
        testDerp1()

    """

    def __init__(self, func):
        """
        Constructor

        args:
            func (Function): The function to decorate
        """
        self.func = func
        self.running = False

    def __call__(self, *args, **kwargs):
        """
        Decoractor is called with the args and kwargs for the decorated function
        """
        if not self.running:
            self.running = True
            value = self.func(*args, **kwargs)
            self.running = False
            return value

    def __repr__(self):
        """
        Return the function's docstring.
        """
        return self.func.__doc__

    def __get__(self, obj, objtype):
        """
        Support instance methods.
        """
        return functools.partial(self.__call__, obj)
