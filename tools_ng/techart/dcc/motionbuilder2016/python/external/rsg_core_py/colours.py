"""A module with utilities for working with colours."""
from matplotlib import colors
from PIL import ImageColor

from rsg_core_py.decorators import memoize


def htmlColorNameToHex(name):
    """Convert a html color name (CSS4 color name) to its corresponding hex value.

    Args:
        name (str): The color name.

    Returns:
        str: The hex value, e.g. "#000000".
    """
    return str(colors.CSS4_COLORS.get(name))  # technically unicode. can stop casting to str when we move to py3.


def hexToRgb(hex):
    """Convert a hex value to its corresponding html color name (CSS4 color name).

    Args:
        hex (str): The hex value, e.g. "#000000".

    Returns:
        tuple[int, int, int]: The rgb values.
    """
    return ImageColor.getrgb(hex)


def rgbToHex(red, green, blue):
    """Convert the rgb value to a hex value, e.g. (0,0,0) -> '#000000' for use in css, etc.

    Args:
        red (int): The red value (0-255).
        green (int): The green value (0-255).
        blue (int): The blue value (0-255).

    Returns:
        str
    """
    return "#{0:02x}{1:02x}{2:02x}".format(red, green, blue)


def rgbNormalized(red, green, blue):
    """Get the normalized rgb value (format for matplotlib).

    Args:
        red (int): The red value (0-255).
        green (int): The green value (0-255).
        blue (int): The blue value (0-255).

    Returns:
        tuple[float, float, float]
    """
    return tuple(val / 255.0 for val in (red, green, blue))


class Tableau20RGB(object):
    """A class for working with the 'Tableau 20' colours.

    Examples:
        # Assign RGB values to a list of items.
        >>> characters = ["char1", "char2", "char3"]
        >>> charColorMappings = {chr: Tableau20RGB.getColours()[idx] for idx, chr in enumerate(characters)}
        >>> import pprint;pprint.pformat(charColorMappings)
        "{'char1': (31, 119, 180), 'char2': (174, 199, 232), 'char3': (255, 127, 14)}"

        # Use a class attr directly.
        >>> rc, gc, bc = Tableau20RGB.BLUE
        >>> rc, gc, bc
        (31, 119, 180)

        # Get colors pre-formatted for use in matplotlib.
        >>> Tableau20RGB.getNormalizedColours()[0]
        (0.12156862745098039, 0.4666666666666667, 0.7058823529411765)

    Related:
        https://public.tableau.com/profile/chris.gerrard#!/vizhome/TableauColors/ColorPaletteswithRGBValues
        https://www.tableau.com/about/blog/2016/7/colors-upgrade-tableau-10-56782
    """

    BLUE = (31, 119, 180)
    LIGHT_BLUE = (174, 199, 232)
    ORANGE = (255, 127, 14)
    LIGHT_ORANGE = (255, 187, 120)
    GREEN = (44, 160, 44)
    LIGHT_GREEN = (152, 223, 138)
    RED = (214, 39, 40)
    LIGHT_RED = (255, 152, 150)
    PURPLE = (148, 103, 189)
    LIGHT_PURPLE = (197, 176, 213)
    BROWN = (140, 86, 75)
    LIGHT_BROWN = (196, 156, 148)
    PINK = (227, 119, 194)
    LIGHT_PINK = (247, 182, 210)
    GRAY = (127, 127, 127)
    LIGHT_GRAY = (199, 199, 199)
    YELLOW = (188, 189, 34)  # This is actually "desaturated yellow" - decided to keep name short.
    LIGHT_YELLOW = (219, 219, 141)
    CYAN = (23, 190, 207)
    LIGHT_CYAN = (158, 218, 229)

    @memoize.memoize
    @classmethod
    def getColours(cls):
        """Get all colours as a list.

        Returns:
            list[tuple[int, int, int]]

        """
        colours = (
            cls.BLUE,
            cls.LIGHT_BLUE,
            cls.ORANGE,
            cls.LIGHT_ORANGE,
            cls.GREEN,
            cls.LIGHT_GREEN,
            cls.RED,
            cls.LIGHT_RED,
            cls.PURPLE,
            cls.LIGHT_PURPLE,
            cls.BROWN,
            cls.LIGHT_BROWN,
            cls.PINK,
            cls.LIGHT_PINK,
            cls.GRAY,
            cls.LIGHT_GRAY,
            cls.YELLOW,
            cls.LIGHT_YELLOW,
            cls.CYAN,
            cls.LIGHT_CYAN,
        )
        return colours

    @memoize.memoize
    @classmethod
    def getColourMappings(cls):
        """Get the colour name: RGB value mappings.

        Returns:
            dict: The name: color tuple mappings

        """
        mappings = {
            "blue": cls.BLUE,
            "light_blue": cls.LIGHT_BLUE,
            "orange": cls.ORANGE,
            "light_orange": cls.LIGHT_ORANGE,
            "green": cls.GREEN,
            "light_green": cls.LIGHT_GREEN,
            "red": cls.RED,
            "light_red": cls.LIGHT_RED,
            "purple": cls.PURPLE,
            "light_purple": cls.LIGHT_PURPLE,
            "brown": cls.BROWN,
            "light_brown": cls.LIGHT_BROWN,
            "pink": cls.PINK,
            "light_pink": cls.LIGHT_PINK,
            "gray": cls.GRAY,
            "light_gray": cls.LIGHT_GRAY,
            "yellow": cls.YELLOW,
            "light_yellow": cls.LIGHT_YELLOW,
            "cyan": cls.CYAN,
            "light_cyan": cls.LIGHT_CYAN,
        }
        return mappings

    @memoize.memoize
    @classmethod
    def getNormalizedColours(cls):
        """Scale the values to the 0.0-1.0 range (format for matplotlib).

        Returns:
            list[tuple[float, float, float]]

        """
        # Format for matplotlib by scaling the RGB values to the [0, 1] range.
        return tuple(rgbNormalized(*rgb) for rgb in cls.getColours())

    @memoize.memoize
    @classmethod
    def getNormalizedColourMappings(cls):
        """Get the colour name: normalized RGB value mappings.

        Returns:
            dict: The name: color tuple mappings

        """
        names = cls.getColourMappings().keys()
        colours = cls.getNormalizedColours()
        return {name: colours[idx] for idx, name in enumerate(names)}


if __name__ == "__main__":
    from pprint import pprint

    pprint(Tableau20RGB.getColours())
    print(Tableau20RGB.getColours()[0])
    pprint(Tableau20RGB.getNormalizedColours())
    print(Tableau20RGB.getNormalizedColours()[0])

    pprint(Tableau20RGB.getColourMappings())
    print(Tableau20RGB.getColourMappings()["yellow"])
    pprint(Tableau20RGB.getNormalizedColourMappings())
    print(Tableau20RGB.getNormalizedColourMappings()["yellow"])

    # Make sure the examples work.
    characters = ["char1", "char2", "char3"]
    charColorMappings = {char: Tableau20RGB.getColours()[idx] for idx, char in enumerate(characters)}
    pprint(charColorMappings)
