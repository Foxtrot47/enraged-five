from rsg_core_py.decorators import memoize


class Singleton(type):
    """
    Singleton Class Object
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class SingletonMemoize(type):
    """
    Singleton Memoize Class Object
    """

    @memoize.memoize
    def __call__(cls, *args, **kwargs):
        return super(SingletonMemoize, cls).__call__(*args, **kwargs)
