"""
Metaclass for mapping subclasses to a dictionary so they can be retrieved based on a key
"""


class Mapping(type):
    """
    Metaclass for retrieving the class based on the mapping

    Note:
        The base class that inherits this metaclass should implement a MAPPINGS dictionary attribute and a key
        classmethod for retrieving the key that should be used.
    """

    def __init__(cls, name, bases, dict):
        if not hasattr(cls, 'MAPPINGS'):
            cls.MAPPINGS = {}
        else:
            if cls.key() is None:
                return
            cls.MAPPINGS[cls.key()] = cls