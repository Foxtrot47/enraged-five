import json


class AttributesLazyLoader(type):
    """
    Metaclass to use in combination with @classproperty decorator.
    It allows to define constants in a static class that get data from a json file,
    and to load json data only when the property is accessed, not on class creation.
    """
    _data = {}
    _dataJsonPath = None
    _dataLoaded = False

    def getData(cls, dataKey, defaultValue=None):
        """
        Returns data from json loaded data.
        Json data is loaded when this method is called for the first time.

        Args:
            dataKey (str): the key of the data to fetch.

        Returns:
            object: the value matching dataKey in cls._data dictionary.
        """
        if not cls._dataJsonPath:
            raise Exception("Define a path for json data to load.")
        if not cls._dataLoaded:
            cls._loadData()
            cls._dataLoaded = True
        return cls._data.get(dataKey, defaultValue)

    def _loadData(cls):
        """
        Loads data from a json file in cls._data, using subclass defined _dataJsonPath attribute.
        """
        try:
            with open(cls._dataJsonPath, "r") as jsonFile:
                cls._data = json.load(jsonFile)
        except IOError:
            cls._data = {}
