class AppInfo(object):
    NAME = "rsg_core_resources"
    VERSION = "0.1.0"
    TITLE = "{0} (Version {1})".format(NAME, VERSION)
