import os


def getResourcesPath():
    """Returns path to root resources folder.

    Returns:
        str: path to the resources root folder.
    """
    return os.path.join(os.path.dirname(__file__), "resources")


def getCertificatesPath():
    """Returns path to certificates folder.

    Returns:
        str: list of available certificates file paths.
    """
    return os.path.join(os.path.dirname(__file__), "resources", "certificates")


def getWindowsCert():
    """
    Returns T2 Windows certificate.

    Returns:
        str: path to Windows certificate.
    """
    return os.path.join(os.path.dirname(__file__), "resources", "certificates", "T2RootCert.cer")


def getLinuxCert():
    """
    Returns T2 Linux certificate.

    Returns:
        str: path to Linux certificate.
    """
    return os.path.join(os.path.dirname(__file__), "resources", "certificates", "T2RootCertLinux.cer")


def getConfigsPath():
    """Returns path to the config folder.

    Returns:
        str: path to the config folder.
    """
    return os.path.join(os.path.dirname(__file__), "resources", "configs")
