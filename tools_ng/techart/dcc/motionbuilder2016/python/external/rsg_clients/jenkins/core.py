"""
A wrapper on top of jenkinsapi python module that logs in and deals with some basic jobs functionalities...
- getting the list of all the jobs(or jobs per view)
- getting the parameters on a specific job (gives you the list of parameters and their info)
- building a job, with or without parameters
- copy the existing job
- checking if the job is enabled
- get/update the configuration files of the jobs
- creates a simple jenkins jobs (freestyle and pipeline) that by default doesn't do anything but their configurations can be modified later on...

"""
import re
import base64

from jenkinsapi import jenkins

from rsg_clients.internals import baseConnection
from rsg_core import activeDirectory
from rsg_clients import const as clientsConst
from rsg_clients.jenkins import const, accounts, exceptions

# TODO: implement the job as abstractPayload instead of jenkinsapi job
# from rsg_clients.jenkins.internal import job


class Jenkins(baseConnection.BaseConnection):
    """
    Wrapper for Jenkins to be able to run API queries.
    """

    def __init__(self, server):
        """
        Constructor

        Args:
            server (str): server to connect to
        """
        super(Jenkins, self).__init__(server)
        self._setup(server=server)
        self._instance = None
        self._data = {}
        self._account = accounts.TechArtAutomation()
        self._sslCertificate = const.Resources.SSL_CERTIFICATE

    @property
    def instance(self):
        if self._instance is None:
            self.login()
        return self._instance

    @property
    def server(self):
        """Name of the server we are accessing."""
        return self._server

    @property
    def restUrl(self):
        """The rest url for the server"""
        return const.Servers.getBaseUrl(self._server)

    def client(self):
        """Name of this client."""
        return clientsConst.Clients.JENKINS

    def authenticate(self, password, username=None):
        """Authenticates the user against the server.

        Notes:
            This should replace the main authenticate function once we phase out the jenkins python module

        Args:
            password (str): password encoded in base64 for the account being accessed.

            username (str, optional): the name of the user to authenticate as. Defaults to the current account set.
        """
        # Get user email because that is used in order to log in to Jenkins not the actual username.
        try:
            ad = activeDirectory.DirectoryServices()
            userAttr = ad.getUserAttributes(self.account.name)
        except:
            raise ValueError("User has no email assigned to them.")
            return False

        self._instance = jenkins.Jenkins(
            self.restUrl,
            username=userAttr["mail"][0],
            password=base64.b64decode(password),
            ssl_verify=self._sslCertificate,
        )
        self._authenticate(password, userAttr["mail"][0])
        return True

    def _authenticate(self, password, username=None):
        """Authenticates the user against the server.

        Notes:
            This should replace the main authenticate function once we phase out the jenkins python module

        Args:
            password (str): password encoded in base64 for the account being accessed.

            username (str, optional): the name of the user to authenticate as. Defaults to the current account set.
        """
        username = username or self.account.name
        if not self._authenticated:
            token = base64.b64encode("{}:{}".format(username, base64.b64decode(password)))
            self.session.headers["authorization"] = "Basic {}".format(token)

        # Test the authentication
        result = self.session.get(self.restUrl, verify=self._sslCertificate)

        if result.status_code >= 300:
            self._authenticated = False
            raise exceptions.NotAuthenticated(
                "Could not authenticate user {} to access the {} server\n"
                "Verify your credentials and try again.\n"
                "If the problem persists, contact techart and the {} team for "
                "further assistance.".format(username, self.server, self.client),
                url=self.restUrl,
                errorCode=result.status_code,
            )
        self._authenticated = True

    def logout(self):
        """Logs out of Jenkins."""
        if self._instance:
            # There doesn't appear to be any logout function for the Jenkins api.
            self._instance = None
        super(Jenkins, self).logout()

    # TODO: Clean up these methods so they match the structure used by the other clients

    def getAllJobs(self, view="techArt"):
        """
        Gets the list of all the existing jobs on Jenkins server

        Keyword Args:
            view (str): name of the view requested from the server

        Returns (list):
            {jobName: jenkinsapi.job.Job}
        """
        if view:
            viewObj = self.getViewByName(view)
            serverJobs = viewObj.items()
        else:
            serverJobs = self.instance.get_jobs()

        if serverJobs:
            return {jobName: jobObject for jobName, jobObject in serverJobs}

    def getViewByName(self, viewName):
        """
        Find the specified view

        Args:
            viewName (str): name of the view to find

        Returns:
            jenkinsapi.view.View
        """
        allViews = self.instance.views
        if viewName in allViews.keys():
            return allViews[viewName]
        else:
            raise exceptions.InvalidView

    # TODO: Clean up code so these methods are handled by the objects themselves
    def getJobByName(self, jobName):
        """
        Get the specified job

        Args:
            jobName (str): Name of the job requested from the server

        Returns:
            jenkinsapi.job.Job
        """
        if self.instance.has_job(jobName):
            return self.instance.get_job(jobName)
        else:
            raise exceptions.InvalidJob

    def copyJob(self, sourceJobName, destinationJobName):
        """
        Copy existing job

        Args:
            sourceJobName (str): name of the job to be copied
            destinationJobName (str): name of the new created copy

        Returns:
            jenkinsapi.job.Job
        """
        if self.instance.has_job(sourceJobName):
            destinationJobName = self._versionJobName(destinationJobName)
            job = self.instance.copy_job(sourceJobName, destinationJobName)
            job.enable()
            return job

        # TODO: Add when the job is being copied to move it to the right view.
        else:
            raise exceptions.InvalidJob(message="Source job doesn't exist")

    def deleteJob(self, jobName):
        """
        Deletes the job from the server if it exists.

        Args:
            jobName (str): name of the job to be removed.

        Returns:

        """
        if self.instance.has_job(jobName):
            return self.instance.delete_job(jobName)
        else:
            raise exceptions.InvalidJob

    def _versionJobName(self, jobName):
        """
        If the job with the name already exists, add the version numbers at the end of the name.

        Args:
            jobName (str): name of the job

        Returns (str):
            name for the new creating job.
        """
        while self.instance.has_job(jobName):
            # Make sure that we create a job with the numbering at the end
            # in case the job with specified destination name already exists.
            if re.search("_(\d)+$", jobName):
                splitName = jobName.split("_")
                counter = int(splitName[-1]) + 1
                baseName = "_".join(splitName[:-1])
            else:
                counter = 0
                baseName = jobName
            jobName = "{0}_{1:04d}".format(baseName, counter)

        return jobName

    def getJobBuild(self, jobName, metadata=None, id=None):
        """
        return the Build(Task) object with the information of that particular run.
        Args:
            jobName (str): name of the job
            id (int): number of the build
            metadata (dict): unique build data. Ex. {"user": username, "id": uuid.uuid4}

        Returns:
            rockstar.jenkinsapi.build.Build
        """
        if self.instance.has_job(jobName):
            job = self.instance.get_job(jobName)
            if id:
                return job.get_build(id)
            else:
                ids = job.get_build_ids()
                for id in ids:
                    build = job.get_build_metadata(id)
                    parameters = build.get_params()
                    if const.JobParameters.METADATA in parameters and parameters[const.JobParameters.METADATA] == str(
                        metadata
                    ):
                        return build

    def enableJob(self, jobName):
        """ Enable the job to be ran as scheduled. """
        jobObject = self.getJobByName(jobName)
        if not jobObject.is_enabled():
            jobObject.enable()

    def disableJob(self, jobName):
        """ Disable the job to be ran as scheduled. """
        jobObject = self.getJobByName(jobName)
        if jobObject.is_enabled():
            jobObject.disable()

    def runJob(self, jobName, params=None):
        """
        SExecute (build) the specified job

        Args:
            jobName (str): name of the job to be ran

        Keyword Args:
            params (dict): parameters to be passed to the job.
        """
        if self.instance.has_job(jobName) and self.getJobByName(jobName).is_enabled():
            self.instance.build_job(jobName, params=params)
        else:
            raise exceptions.InvalidJob(message="Job with the name {} doesn't exist.".format(jobName))

    def isRunning(self, jobName):
        """
        Checks if the specified job is running

        Args:
            jobName (str): name of the job to check.

        Returns (bool):
            True if job is running and False if it's not.
        """
        jobObject = self.getJobByName(jobName)
        return jobObject.is_running()

    def raiseAuthenticationError(self, url, response):
        """Raises an authentication error if the response returns an html page with said error.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server

        Raises:
            NotAuthenticated
        """
        if "<!doctype html>" in response.text and response.status_code == 401:
            raise exceptions.NotAuthenticated(
                "Basic Authentication Failure - Reason : AUTHENTICATED_FAILED", url=url, errorCode=response.status_code
            )

    def raiseException(self, url, response, data):
        """Raises an exception if the response returns a non-200 status code.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server
            data (dict): data extrapolated from the response as a dict

        Raises:
            JenkinsException
        """
        exceptionClass = exceptions.JenkinsException.getByErrorCode(response.status_code)
        if 300 <= response.status_code and not exceptionClass:
            exceptionClass = exceptions.JenkinsException

        if exceptionClass is not None:
            # Get the correct error message, based on the response it may require different keys
            errorDict = data.get("ResponseHeader", data).get("Errors", {})
            errorMessage = errorDict.get("messages", errorDict.get("#text", [""]))[0]

            if response.status_code == exceptions.JenkinsServerError.ERROR_CODE:  # This the generic 500 error code
                if exceptions.ServerUnavailable.REGEX.search(errorMessage):
                    raise exceptions.ServerUnavailable(server=self.type, message=errorMessage, url=url)

            raise exceptionClass(errorMessage, url=url, errorCode=response.status_code)
