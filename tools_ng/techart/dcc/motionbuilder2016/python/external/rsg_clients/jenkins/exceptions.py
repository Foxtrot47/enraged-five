"""
Exception classes for Jenkins related errors, mostly for error codes returned by the REST API
"""
import re

from rsg_core_py.exceptions import types
from rsg_clients.jenkins import const
from rsg_clients import exceptions
from rsg_core_py.decorators import memoize


class JenkinsException(exceptions.RestException):
    """Base jenkins Exception."""

    @memoize.memoize
    @classmethod
    def getByErrorCode(cls, errorCode):
        """Get an exception by its error code.

        Args:
            errorCode (int): error code

        Returns:
            rsg_clients.jenkins.jenkinsException
        """
        if errorCode is None:
            return

        for subclass in cls.__subclasses__():
            if subclass.ERROR_CODE == errorCode:
                return subclass


class NotAuthenticated(JenkinsException, types.AuthenticationError):
    """ User cannot be authenticated on Jenkins server """

    DEFAULT_MESSAGE = "The current user is not authenticated to access the current server"


class InvalidView(JenkinsException):
    """ Invalid view name passed to the Jenkins """

    def __init__(self, message=None):
        self.message = message or "A view with the specified name doesn't exist on the server."
        types.RockstarError.__init__(self, self.message)


class InvalidJob(JenkinsException):
    """ Invalid job name passed to the Jenkins """

    def __init__(self, message=None):
        self.message = message or "A job with the specified name doesn't exist on the server."
        types.RockstarError.__init__(self, self.message)


class JenkinsServerError(JenkinsException, exceptions.ServerError):
    """Error from status code 500."""

    ERROR_CODE = 500
    DEFAULT_MESSAGE = "Unhandled Error by the jenkins Team"


class ServerUnavailable(JenkinsException, exceptions.ServerUnavailable):
    """Server is unavailable and can't be reached."""

    REGEX = re.compile(
        "(The CacheManager has been shut down)|(Session/EntityManager is closed)|"
        "(Can not read response from server)|(BeanCreationNotAllowedException)"
    )

    def __init__(self, server, message=None, url=None, includeErrorCode=None):
        """Overrides inherited method.

        Args:
            server (rsg_clients.jenkins.servers.BaseServer): server that raised

            message (str, optional): error message to set for the error.
            url (str, optional): The url that raised the error
            includeErrorCode (bool, optional): Include the error code in the error message
        """
        exceptions.ServerUnavailable.__init__(
            self, client=const.AppInfo.NAME, server=server, message=message, url=url, includeErrorCode=includeErrorCode
        )
