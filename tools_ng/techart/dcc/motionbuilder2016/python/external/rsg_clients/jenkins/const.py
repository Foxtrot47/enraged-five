import os

from rsg_core.configs import main as config


class AppInfo(object):
    NAME = "Jenkins"
    VERSION = "0.4.2"
    TITLE = "{0} (Version {1})".format(NAME, VERSION)


class System(object):
    # Used to specify on which system do we want to run the job.
    LINUX = "linux"
    WINDOWS = "windows"


class Objects(object):
    VIEW = "view"
    JOB = "job"
    BUILD = "build"


class Resources(object):
    """
    Constants for the various resource directories.
    """

    ROOT = os.path.dirname(__file__)
    SSL_CERTIFICATE = config.system.environment.sslCertificate or config.system.environment.techartProject.sslCertificate


# TODO: Rethink this, project sensitive information shouldn't be in hardcoded
class Projects(object):
    BOB = "bob"
    BOB_DLC = "bob_dlc"
    DEVELOPMENT = "development"
    MERIDIAN = "meridian"
    PARADISE = "paradise"
    PARADISE_DLC = "paradise_dlc"


class JobParameters(object):
    SCRIPT = "script"
    SCRIPT_ARGS = "args"
    SCRIPT_KWARGS = "kwargs"
    PRODUCTION_EMAIL = "emails"
    UNSHELVE_CL = "unshelveCL"
    PROJECT = "project"
    METADATA = "METADATA"


class JobMetadata(object):
    ID = "id"
    USER = "user"


class Servers(object):
    """ Paths to the Jenkins Servers"""

    _BASE_URL = ".rockstargames.com"
    _DEV_URL = ""

    PRODUCTION = "jenkins"
    DEVELOPMENT = "jenkins-dev"

    @classmethod
    def getBaseUrl(cls, service):
        """
        Builds the base url for the given service

        Arguments:
            service (string): name of the service to build the url for

        Returns:
            string
        """

        return "https://{}{}".format(service, cls._BASE_URL)


class Endpoints(object):
    """Endpoints supported by Jenkins"""

    API = "api"
    JSON = "json"
    XML = "xml"
    PYTHON = "python"
