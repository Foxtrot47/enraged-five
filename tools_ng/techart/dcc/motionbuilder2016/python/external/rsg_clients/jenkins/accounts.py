"""Accounts to access Jenkins with that use tokens/keys vendored by it."""
from rsg_core import accounts


class TechArtAutomation(accounts.TechArtAutomation):
    """Access Jenkins as the TechArtAutomation account using the token vendored by jenkins."""

    isApiKey = True

    def authenticate(self, instance, **kwargs):
        """Reimplemented from the inherited class.

        Passes the password for this account to the authenticate method of the shotgun instance.

        Args:
            instance (rsg_clients.shotgun.core.Shotgun): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        return instance.authenticate(password="MTFmNDc1N2ZjNzQzM2U0ZTNmODk0NmQ1MjY3ODFlYmQwMg==", **kwargs)
