import getpass
import uuid

from rsg_clients.jenkins import core, const
from rsg_core_py.abstracts import abstractServer
from rsg_core_py.metaclasses import singleton


class JenkinsServer(abstractServer.AbstractServer):

    NAME = None

    def __init__(self):
        """
        Constructor
        """
        self._instance = core.Jenkins(self.NAME)

    @property
    def instance(self):
        """ Internal pointer to the server instance."""
        return self._instance

    @property
    def jenkins(self):
        """Friendly name for the internal pointer to the server."""
        return self.instance

    @property
    def jobs(self):
        return self._instance.getAllJobs()

    def getJobByName(self, jobName):
        """
        Query the job object with the specified name
        Args:
            jobName (str): name of the job whose object we want to get.

        Returns:
            jenkinsapi.job.Job
        """
        return self._instance.getJobByName(jobName)

    def copyJob(self, jobName, jobSystem=const.System.LINUX):
        """
        Copy template created for the specified system to the new job with specified name.

        Args:
            jobName (str): name of the new job we want to create
            jobSystem (str): system on which we will run the job. const.System.LINUX or const.System.WINDOWS

        Returns:
            jenkinsapi.job.Job
        """
        if jobSystem == const.System.LINUX:
            return self._instance.copyJob("linuxTemplate", jobName)
        else:
            return self._instance.copyJob("winTemplate", jobName)

    def getJobBuild(self, jobName, metadata=None):
        """
        Get build instance

        Args:
            jobName (str): name of the job that has run the build.
            metadata (dict): metadata which to search for.

        Returns:
            jenkinsapi.build.Build
        """
        return self._instance.getJobBuild(jobName, metadata)

    def runJob(self, jobName, params=None):
        """
        Fixes params if they were passed in a wrong type and executes the build of the passed job.

        Args:
            jobName (str): name of the job to be executed
            params (dict): dictionary with the parameters to be passed to the job.

        Returns:
            dict - metadata by which this job can be queried, searched for.
        """
        metadata = {const.JobMetadata.USER: getpass.getuser(), const.JobMetadata.ID: str(uuid.uuid4())}
        if params:
            params[const.JobParameters.METADATA] = str(metadata)
        else:
            params = {const.JobParameters.METADATA: str(metadata)}

        # Make sure that the arguments and keyword arguments are converted to string if there are any.
        if params.get(const.JobParameters.SCRIPT_ARGS, None) and not isinstance(
            params[const.JobParameters.SCRIPT_ARGS], str
        ):
            params[const.JobParameters.SCRIPT_ARGS] = " ".join(list(params[const.JobParameters.SCRIPT_ARGS]))
        if params.get(const.JobParameters.SCRIPT_KWARGS, None) and isinstance(
            params[const.JobParameters.SCRIPT_KWARGS], dict
        ):
            kwargs = ""
            for key, value in params[const.JobParameters.SCRIPT_KWARGS].items():
                kwargs += " --{} {}".format(key, value)

            params[const.JobParameters.SCRIPT_KWARGS] = kwargs
        if params.get(const.JobParameters.PRODUCTION_EMAIL, None) and not isinstance(
            params[const.JobParameters.PRODUCTION_EMAIL], str
        ):
            params[const.JobParameters.PRODUCTION_EMAIL] = " ".join(list(params[const.JobParameters.PRODUCTION_EMAIL]))

        # TODO: Add metadata info to the database
        self._instance.runJob(jobName, params=params)
        return metadata


class Production(JenkinsServer):
    __metaclass__ = singleton.Singleton
    NAME = const.Servers.PRODUCTION


class Development(JenkinsServer):
    __metaclass__ = singleton.Singleton
    NAME = const.Servers.DEVELOPMENT


def getByName(name):
    """
    Gets the server based on the name

    Args:
        name (str): name of the server to get

    Returns:
        rsg_clients.jenkins.servers.JenkinsServer or None
    """
    for cls in JenkinsServer.__subclasses__():
        if cls.NAME == name:
            return cls()
    return None


production = Production()
development = Development()
