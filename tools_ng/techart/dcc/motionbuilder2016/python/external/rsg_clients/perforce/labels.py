"""
This module contains code for creating, shelving and unshelving an automatic label
"""
# This module is used at Linux Startup, so we don't use the services here
from rsg_clients.perforce import servers, exceptions


class AutomaticLabel(object):
    """
    Class for using an automatic label, which for our use case is simply an alias for a changelist number
    """
    def __init__(self, name):
        """
        Constructor

        Arguments:
            name (string): name of the label
        """
        self._name = name
        self._cache = {}
        self._message = None

    def exists(self):
        """ Does this label exist """
        for p4Dict in servers.gameDepot.P4.run("labels", ["-e", self._name]):
            if not self._cache:
                self._cache = p4Dict
            self._message = "Label {} Exists".format(self._name)
            return True

        if self._cache:
            self._cache = {}
        self._message = "Label {} Does Not Exist".format(self._name)
        return False

    def create(self, changelist, description=None):
        """
        Create the label.
        If the name of the label conflicts with the name of an existing client then it will error out.

        Arguments:
            changelist (string): changelist number to associate with the label
            description (string): description for the label. If the changelist passed in is valid then it will attempt
                                  set the description of the label to be the same. Otherwise the description is set as
                                  to the same as the name of the label.

        Returns:
            bool
        """
        if not changelist:
            self._message = "Invalid changelist provided"
            return False

        if not description:
            p4Results = servers.gameDepot.P4.run("describe", [changelist])
            if not p4Results:
                description = self._name
            else:
                description = p4Results[0].get("desc")

        if not isinstance(changelist, basestring) or not changelist.startswith('#'):
            changelist = "#{}".format(changelist)

        # The double quotes is so the hashtag isn't treated as a comment when passing it into P4
        changelist = '"{}"'.format(changelist)
        if self.exists():
            self.delete()
        try:
            # View should always be an empty list so this automatic label is only treated as an alias for the changelist
            self._cache = servers.gameDepot.P4.run("label", [self._name], silent=True, input={"Description": description, "View": [],
                                                                                     "Revision": changelist})
            self._message = "Label {} created".format(self._name)
            return True
        except exceptions.P4Warning as exception:
            self._message = exception.message
            return False

    def delete(self):
        """ Deletes the label """
        try:
            servers.gameDepot.P4.run("label", ["-d", self._name])
            self._message = "Label {} deleted".format(self._name)
            return True
        except exceptions.P4Warning as exception:
            self._message = exception.message
            # This command can error out if the label doesn't exist, which is good
            return False
        finally:
            self._cache = {}

    def changelist(self):
        """ The changelist associated with this label """
        revisionString = self._cache.get("Revision", None)
        if isinstance(revisionString, basestring):
            # We skip the first part of the string as it should be always be a hashtag
            return revisionString.strip()[1:]

    def description(self):
        """ Description for this label """
        return self._cache.get("Description", "")

    def localChangelist(self):
        """ The local changelist for unshelved files for this label """
        # TODO: This code copied from CrossShelver, this should be moved to a generic location
        changelists = servers.gameDepot.P4.run("changes", ["-l", "-c", servers.gameDepot.P4.client(), "-s", "pending"])
        for changelist in changelists:
            # Descriptions may end with newline characters, so they need to be stripped
            if changelist["desc"].strip() == self._name:
                return changelist.get("change")

    def unshelve(self, force=False):
        """ Unshelves files from the changelist number associated with the label """
        changelist = self.changelist()
        if not changelist:
            return False
        localChangelist = self.localChangelist()
        if localChangelist is None:
            localChangelist = servers.gameDepot.P4.changelist(self._name)
        try:
            args = ["-s", changelist, "-c", localChangelist]
            if force:
                args[0:0] = ["-f"]
            servers.gameDepot.P4.run("unshelve", args)
            self._message = "Label {} unshelved".format(self._name)
            return True
        except exceptions.P4Warning as exception:
            self._message = exception.message
            return False

    def cleanup(self):
        """ Reverts unshelved files and deletes local changelists generated from this label """
        changelist = self.localChangelist()
        if not changelist:
            self._message = "No local changelist to remove found"
            return False
        paths = servers.gameDepot.filesInChangelist(changelist)
        if paths:
            servers.gameDepot.P4.run("revert", ["-c", changelist, paths])
        servers.gameDepot.P4.run("change", ["-d", changelist])
        self._message = "Changelist {} removed".format(changelist)
        return True

    def message(self):
        """
        Errors

        Returns:
            string or None
        """
        return self._message