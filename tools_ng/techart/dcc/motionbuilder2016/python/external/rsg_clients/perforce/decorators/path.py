import functools
from rsg_clients.perforce import utils


def cleanPaths(func):
    """
    Wrapper to clean p4 paths before p4 operations. Mainly removes all invalid chars replacing them with ASCII version.

    The first argument of the decorated function must be a the perforce instance to use for accessing perforce.
    The second argument of the decorated function must be a single , list or dict of strings AND the name of the
    argument must be either path or paths.

    Args:
        func (function): function to wrap.

    Examples:
        from rsg_clients.perforce import servers
        from rsg_clients.perforce.decorators import path

        @path.cleanPath
        def example(perforce, paths, other=False):
            print perforce, paths, other

        example(servers.gameDepot, ["//techart/@production"], True)
        # prints <server object>, "//techart/%40production", True
    """
    @functools.wraps(func)
    def wrapper(instance, *args, **kwargs):
        args = list(args)
        paths = kwargs.pop("path", kwargs.pop("paths", None))
        if not paths and args:
            paths = args[0]
            args.pop(0)

        if isinstance(paths, (list, tuple)):
            cleanedPaths = [utils.removeSpecialCharacters(path) for path in paths]

        elif isinstance(paths, dict):
            cleanedPaths = {utils.removeSpecialCharacters(oldPath): utils.removeSpecialCharacters(newPath)
                            for oldPath, newPath in paths.iteritems()}
        elif isinstance(paths, basestring):
            # Assume that the paths variable is a string
            cleanedPaths = utils.removeSpecialCharacters(paths)
        else:
            raise ValueError("{} is not a string, list or dictionary".format(paths))

        args[0:0] = [instance, cleanedPaths]
        return func(*args, **kwargs)

    return wrapper
