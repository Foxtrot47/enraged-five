"""
Automated tests for clients.perforce.servers module.

These tests are at a very early stage and meant to be updated incrementally over time.

Warnings:
    * You'll need access to the game and mocap depots.
"""
import unittest
import platform

from rsg_clients.perforce import servers


class TestMocapServer(unittest.TestCase):
    """Unittests for the Mocap class."""

    def test_dirExists(self):
        """Test that the given directories exist in P4."""
        # TODO: Replace this test with a test on the development server
        expectedResults = {
            "//depot/projects/development/capture/takes/2017_0331_Session0007": True,
            "//depot/projects/development/capture/takes/2017_0331_Session000": False,
        }
        if platform.system() == "Linux":
            expectedResults.update({
                "/media/giant_capture_data/development/capture/takes/2017_0331_Session0007": True,
                "/media/giant_capture_data/development/capture/takes/2017_0331_Session00": False,
            })
        elif platform.system() == "Windows":
            expectedResults.update({
                "X:\\projects\\development\\capture\\takes\\2017_0331_Session0007": True,
                "X:\\projects\\development\\capture\\takes\\2017_0331_Session00": False,
            })
        for path, expectedResult in expectedResults.iteritems():
            self.assertEqual(expectedResult, servers.mocapDepot.dirExists(path))
