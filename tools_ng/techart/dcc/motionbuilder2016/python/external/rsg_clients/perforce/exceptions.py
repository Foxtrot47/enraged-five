from rsg_core_py.exceptions import types


class PerforceError(types.PerforceError):
    """Standard P4 Error."""
    DEFAULT_MESSAGE = None

    def __init__(self, message=None, connection=None):
        message = message or self.DEFAULT_MESSAGE
        self.message = ""
        self.rawMessage = message

        if connection is not None:
            self.message = "{}\n\tServer: {}\n\tWorkspace: {}".format(self.message, connection.server(), connection.client())
        if message is not None:
            self.message = "{}\n\t{}".format(self.message, message)

        types.PerforceError.__init__(self, self.message)


class InvalidStream(PerforceError):
    """The given stream does not exist."""


class AuthenticationError(types.AuthenticationError, PerforceError):
    """Authentication failed."""


class SessionExpired(AuthenticationError):
    """Authentication failed because the P4 Ticket has timed out."""


class AccessDenied(PerforceError):
    """The user was denied access to the server."""


class PerforceDisabled(PerforceError):
    """Perforce is disabled."""


class InvalidClient(PerforceError):
    """The given workspace doesn't exist."""


class PathNotInPerforce(PerforceError):
    """Raise when the path does not exist in Perforce."""

    def __init__(self, path, connection):
        """Constructor.

        Args:
            path (str): path to the file that is not in perforce
            connection (rsg_clients.perforce.core.RSP4): The connection to the server
        """
        super(PathNotInPerforce, self).__init__("Path is not on '{}': '{}'".format(connection.server(), path), connection)
