import pathlib

from rsg_clients.perforce import servers, core


class FileStateCache(object):
    """Use this class to get fileState data from p4, while also caching the result within the class.
    This allows to get the fileState only once, and then retrieve that same cached fileState without having to contact
    the server every single time for the data.
    The cache can be used by various classes/tools, so that various areas of the code can all share the same cached
    data, and can read from it.
    """

    def __init__(self, server=None):
        """
        Args:
            server (servers.BaseServer): server to use for the p4 operations
        """
        self._server = server or servers.production
        self._fileStateCache = {}

    def getServerName(self):
        """
        Returns:
            str: name of the p4 server this cache refers to.
        """
        return self._server.NAME

    def clearCache(self):
        """Convenience method to clear the cache for the instance"""
        self._fileStateCache = {}

    def getFileState(self, fileName, force=False):
        """Return the fileState for the provided FileName.
        If the fileState was already cached, return the cached one instead.

        Notes:
            Wildcards fileNames, like X:/folder/..., that might return more FileStates, are not supported.

        Args:
            fileName (str): local file path
            force (bool): if true, ignore the cached fileState and grab a new one from the p4 server

        Returns:
            rsg_clients.perforce.core.FileState: The requested fileState
        """
        path = pathlib.PurePath(fileName)
        if force or not self.isCached(path):
            self._fileStateCache[path] = self._server.P4.fileState(str(path))

        return self._fileStateCache[path]

    def getFileStates(self, fileNames, force=False):
        """Get the FileStates for all the given fileNames, and for each fileName cache the result.
        Finally, return all the fileStates.

        This is the go-to command if you want to quickly update the cache for a list of multiple files.

        Args:
            fileNames (list of str): list of local file paths
            force (bool): if true, ignore the cached fileStates and grab a new ones from the p4 server

        Returns:
            list of rsg_clients.perforce.core.FileState: list of fileStates grabbed and cached by the command.
        """
        filePaths = [pathlib.PurePath(fileName) for fileName in fileNames]
        # get a list of files we actually need to cache
        toCache = [filePath for filePath in filePaths if not self.isCached(filePath) or force]
        toCache = list(set(toCache))
        if not toCache:
            # if nothing needs to be cached, just return the cached fileStates
            return [self._fileStateCache[filePath] for filePath in filePaths]
        # get the new fileStates from the server
        fileStates = self._server.P4.fileStates([str(f) for f in toCache])

        filesInDepot = []
        for fileState in fileStates:
            # update the cache with the retrieved fileStates
            self._fileStateCache[pathlib.PurePath(fileState.clientFilename)] = fileState
            filesInDepot.append(pathlib.PurePath(fileState.clientFilename))

        returnFileStates = []
        # p4 fstat only returns data for files existing in the p4 depot.
        # We need to account for client only files, to avoid returning less fileStates than the files we were provided.
        for filePath in filePaths:
            # if we had to cache this file, but we didn't get back a fileState, this file is only on the client side
            if filePath in toCache and filePath not in filesInDepot:
                # so cache it as a default fileState
                self._fileStateCache[filePath] = core.FileState(dict())
            returnFileStates.append(self._fileStateCache[filePath])
        return returnFileStates

    def setFileState(self, fileName, fileState=None):
        """Set the given fileState as the cached result of the provided fileName.
        If no fileState is specified, the cache will retrieve the data from the server.

        This is useful when you already have a FileState object from a different operation, and might want to update
        the one in the cache.

        Args:
            fileName (str): local filename
            fileState (rsg_clients.perforce.core.FileState or None): The fileState to set in the cache.

        Returns:
            rsg_clients.perforce.core.FileState: the given fileState, or the one retrieved from p4.
        """
        filePath = pathlib.PurePath(fileName)
        if fileState is None:
            fileState = self._server.P4.fileState(str(filePath))
        self._fileStateCache[filePath] = fileState
        return fileState

    def isCached(self, filePath):
        """
        Args:
            filePath (str):
        Returns:
            bool: check if the current path is cached
        """
        if not isinstance(filePath, pathlib.PurePath):
            filePath = pathlib.PurePath(filePath)
        return self._fileStateCache.get(filePath) is not None

