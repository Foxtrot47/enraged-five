import os
import sys
import time
import argparse
import platform


def parseArgs():
    """
    Parses the arguments in sys.argsv

    Return:
        argparse.Namespace
    """
    parser = argparse.ArgumentParser()

    # describe the flags this module expects and what they do
    parser.add_argument("name", help="Name of the label", default="")
    parser.add_argument("--get", help="Unshelves the label if it exists", action="store_true")
    parser.add_argument("--force", help="Force the files to unshelve even if there are unopened files with changes on disk", action="store_true")
    parser.add_argument("--create", help="Creates an automatic label using the provided changelist number", default="")
    parser.add_argument("--delete", help="Deletes the automatic label", action="store_true")
    parser.add_argument("--cleanup", help="Removes local unshelved changelists whose description matches the name of the label."
                                          "When used with the --get flag, it performs a cleanup if it fails to create a changelist.", action="store_true")
    parser.add_argument("-v", "--verbose", help="Display the results of the command", action="store_true")
    return parser.parse_args()


args = parseArgs()

if __name__ == "__main__":
    status = False
    start = time.time()

    try:
        if args.name == "":
            # No name was passed in, so we exit early
            # Using 2 to differentiate it from a bad p4 call
            sys.exit(2)

        path = os.path.join(__file__.split("python", 1)[0], "python")
        if path not in sys.path:
            # We map the root python directory and external libs to the front of sys.path so that this runner can
            # be ran even without out having the techart depot mapped to the machine.
            # This important to when running it against hopper as it may be run before the environment has been setup
            os_ = platform.system()
            is64bit = sys.maxsize > 2**32
            pythonVersion = "{}.{}".format(*sys.version_info[:2])
            externalPath = os.path.join(path, "externalLibs", os_, "x64" if is64bit else "x32", pythonVersion)
            if not os.path.exists(externalPath):
                externalPath = os.path.join(path, "externalLibs", os_.lower(), "x64" if is64bit else "x32", pythonVersion)

            for sysPath in (externalPath, path):
                if sysPath not in sys.path:
                    sys.path[0:0] = [sysPath]

        from rsg_clients.perforce import labels

        currentLabel = labels.AutomaticLabel(args.name)
        labelExists = currentLabel.exists()

        if args.create:
            status = currentLabel.create(args.create)

        elif args.delete:
            status = currentLabel.delete()

        elif args.get:
            unshelvedSuccessfully = currentLabel.unshelve(force=args.force)
            if not unshelvedSuccessfully and args.cleanup:
                currentLabel.cleanup()
            status = unshelvedSuccessfully

        elif args.cleanup:
            status = currentLabel.cleanup()

    except:
        status = False

    finally:
        if args.verbose:
            print currentLabel.message()
        print time.time() - start
        sys.exit(0 if status else 1)