import unittest
import platform

from rsg_clients.perforce import const, utils


class TestUtils(unittest.TestCase):

    def test_removeSpecialCharactersFromPath(self):
        testStrTemplate = "art/animation/ingame/source/face_human/{}dialogue/{}videos/"

        for specialChar, asciiChar in zip(const.SpecialCharacters.SPECIAL_CHARS, const.SpecialCharacters.ASCII_CHARS):
            testPath = testStrTemplate.format(specialChar, specialChar)
            expectedResult = testStrTemplate.format(asciiChar, asciiChar)
            self.assertEqual(expectedResult, utils.removeSpecialCharacters(testPath))

    def test_restoreSpecialCharactersOnPath(self):
        testStrTemplate = "art/animation/ingame/source/face_human/{}dialogue/videos/"

        for specialChar, asciiChar in zip(const.SpecialCharacters.SPECIAL_CHARS, const.SpecialCharacters.ASCII_CHARS):
            testPath = testStrTemplate.format(asciiChar)
            expectedResult = testStrTemplate.format(specialChar)
            self.assertEqual(expectedResult, utils.restoreSpecialCharacters(testPath))

    def test_convertToDirectory(self):
        """
        Tests that the output of the convertToDirectory method is correctnycu-renderpythin+
        """
        # Defaults
        examplePaths = {
            # Depot
            "//techart/development/*": [
                "//techart/development",
                "//techart/development/",
                "//techart/development/*",
            ],
        }
        if platform.system() == "Windows":
            examplePaths["x:\\techart\\development\\*"] = [
                "x:\\techart\\development",
                "x:\\techart\\development\\",
                "x:\\techart\\development\\*",
            ]
        elif platform.system() == "Linux":
            examplePaths["/media/giant/data/*"] = [
                "/media/giant/data",
                "/media/giant/data/",
                "/media/giant/data/*",
            ]

        for expectedResult, inputPaths in examplePaths.items():
            for inputPath in inputPaths:
                self.assertEqual(utils.convertToDirectory(inputPath), expectedResult)

        # Iterative
        examplePaths = {
            # Depot
            "//techart/development/.../*": [
                "//techart/development",
                "//techart/development/",
                "//techart/development/*",
            ],
        }
        if platform.system() == "Windows":
            examplePaths["x:\\techart\\development\\...\\*"] = [
                "x:\\techart\\development",
                "x:\\techart\\development\\",
                "x:\\techart\\development\\*",
            ]
        elif platform.system() == "Linux":
            examplePaths["/media/giant/data/.../*"] = [
                "/media/giant/data",
                "/media/giant/data/",
                "/media/giant/data/*",
            ]
        for expectedResult, inputPaths in examplePaths.items():
            for inputPath in inputPaths:
                actualResult = utils.convertToDirectory(inputPath, iterative=True)
                self.assertEqual(
                    actualResult,
                    expectedResult,
                    "input '{}': actual result '{}' vs expected '{}'".format(inputPath, actualResult, expectedResult)
                )
