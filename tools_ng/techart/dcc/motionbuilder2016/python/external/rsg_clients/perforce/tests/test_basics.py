"""
Test module for debugging the basic interface for interacting with perforce

Notes:
    The development server for perforce does has a max storage of 10 gigs and any test files being uploaded should be
    empty.
"""
import os
import unittest

import pathlib

from rsg_core.configs import main as config
from rsg_core import accounts
from rsg_clients.perforce import servers, const, core, exceptions


class TestConnection(unittest.TestCase):
    """Tests basic connection functionality."""

    SERVER = None  # Server to use for this test

    @classmethod
    def setUpClass(cls):
        """Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.developmentDepot
        cls.SERVER.instance.account = accounts.TechArt()

    def test_Connection(self):
        """Test if you can connect to the servers."""
        for server in (servers.gameDepot, servers.mocapDepot, servers.developmentDepot):
            # Logout the server to remove the current P4 Ticket
            currentAccount = server.instance.account
            server.instance.logout()

            # Login with a fake account to verify login fails for bad users
            server.instance.account = accounts.OtherUser("notreal", "badpassword")
            with self.assertRaises(exceptions.AuthenticationError) as context:
                server.instance.login()
            self.assertTrue(context.exception)

            # Login with an account that has access to the server
            server.instance.account = accounts.TechArt()
            self.assertTrue(server.instance.login())

            # Log back in the original account that we signed off
            server.instance.account = currentAccount
            server.instance.login()


class Test_A01_Connecting(unittest.TestCase):

    WORKSPACE = "{}_UnitTest".format(config.system.host)
    ALTERNATE_WORKSPACE = "{}_DO_NOT_USE".format(config.system.host)

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.developmentDepot

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.SERVER.P4.logout()

    def test_A01_setWorkspace(self):
        """ unload and reload the workspace before setting it """
        # Determine if the client exists and if it is unloaded
        clients = [client['client'] for client in self.SERVER.instance.clients()]
        unloadedClients = [client['client'] for client in self.SERVER.instance.clients(unloaded=True)]

        for workspace in [self.WORKSPACE, self.ALTERNATE_WORKSPACE]:
            # Create the workspace if it currently doesn't exist
            if workspace not in clients and workspace not in unloadedClients:
                self.SERVER.instance.run("client", [workspace], silent=True)
                clients = [client['client'] for client in self.SERVER.instance.clients()]
                self.assertTrue(workspace in clients)

            # Test that reloading and unloading works
            if workspace in clients:
                self.SERVER.P4.run("unload", ["-c", workspace])
                unloadedClients = [client['client'] for client in self.SERVER.instance.clients(unloaded=True)]
                self.assertTrue(workspace in unloadedClients)

                self.SERVER.P4.reloadClient(workspace)
                clients = [client['client'] for client in self.SERVER.instance.clients()]
                self.assertTrue(workspace in clients)

            else:
                self.SERVER.instance.reloadClient(workspace)
                clients = [client['client'] for client in self.SERVER.instance.clients()]
                self.assertTrue(workspace in clients)

                self.SERVER.instance.run("unload", ["-c", workspace])
                unloadedClients = [client['client'] for client in self.SERVER.instance.clients(unloaded=True)]
                self.assertTrue(workspace in unloadedClients)

                self.SERVER.instance.reloadClient(workspace)

        # Assign workspace to our connection
        self.SERVER.instance.setClient(self.WORKSPACE)

    def test_switchWorkspaces(self):
        """ test that you can switch workspaces properly """
        self.SERVER.P4.setClient(self.ALTERNATE_WORKSPACE)
        self.assertEqual(self.SERVER.P4.client(), self.ALTERNATE_WORKSPACE)
        self.SERVER.P4.setClient(self.WORKSPACE)
        self.assertEqual(self.SERVER.P4.client(), self.WORKSPACE)


class Test_A02_FileOperations(unittest.TestCase):
    """
    Tests for common file operations.
    The first test should always be the add test and the last test should always be the delete test
    """

    WORKSPACE = "{}_UnitTest".format(config.system.host)
    DEPOT_PATH = "//development/unittest/empty.txt"
    MOVE_PATH = "//development/unittest/move/empty.txt"

    # Perforce does not like non-string file paths
    SUBPATH = pathlib.Path("development/unittest/empty.txt")
    MOVE_SUBPATH = pathlib.Path("development/unittest/move/empty.txt")
    WINDOWS_ROOT = pathlib.Path("X:/")
    LINUX_ROOT = pathlib.Path("/media/giant_capture_data/north")

    LOCAL_WINDOWS_PATH = None
    LOCAL_LINUX_PATH = None
    LOCAL_PATH = None
    LOCAL_MOVE_PATH = None
    LOCAL_DIRECTORY = None

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.developmentDepot
        root = cls.WINDOWS_ROOT
        if config.system.isLinux:
            root = cls.LINUX_ROOT

        cls.LOCAL_DIRECTORY = str(root.joinpath("development"))
        cls.LOCAL_PATH = str(root.joinpath(cls.SUBPATH))
        cls.LOCAL_MOVE_PATH = str(root.joinpath(cls.MOVE_SUBPATH))

        try:
            cls.SERVER.P4.revert([cls.LOCAL_PATH])
            cls.SERVER.P4.sync([cls.LOCAL_PATH], force=True)
        except:
            pass

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.SERVER.P4.logout()

    def setUp(self):
        """
        Make sure that the file is synced up to the latest version on the server
        Returns:

        """
        self.SERVER.P4.revert(self.LOCAL_PATH)
        self.SERVER.P4.sync(self.LOCAL_PATH, force=True)

    def test_A01_addFile(self):
        """ Add file to perforce """
        # Create File
        if not os.path.exists(os.path.dirname(self.LOCAL_PATH)):
            os.makedirs(os.path.dirname(self.LOCAL_PATH))

        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        if os.path.exists(self.LOCAL_PATH):
            if filestate.headActionString == const.ActionTypes.UNKNOWN:
                # Remove local file that should not be on disk
                os.remove(self.LOCAL_PATH)
            elif filestate.headActionString not in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE]:
                # Make sure the file is marked as deleted from the server and removed locally
                changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Delete File")
                self.SERVER.P4.delete(self.LOCAL_PATH, changelist=changelist)
                filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
                self.assertEqual(filestate.openActionString, const.ActionTypes.DELETE)
                self.SERVER.P4.submit(changelist)
            elif filestate.headActionString in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE]:
                # Make sure the file is removed locally to match the server
                self.SERVER.P4.revert([self.LOCAL_PATH])
                self.SERVER.P4.sync([self.LOCAL_PATH], force=True)

        elif filestate.headActionString != const.ActionTypes.UNKNOWN:
            # Make sure the file is deleted on the server
            if filestate.headActionString not in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE]:
                self.SERVER.P4.revert([self.LOCAL_PATH])
                self.SERVER.P4.sync([self.LOCAL_PATH], force=True)
                changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Delete File")
                self.SERVER.P4.delete(self.LOCAL_PATH)
                filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
                self.assertEqual(filestate.openActionString, const.ActionTypes.DELETE)
                self.SERVER.P4.submit(changelist)

        with open(self.LOCAL_PATH, "w+"):
            pass

        changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Adding File")
        self.SERVER.P4.add([self.LOCAL_PATH], changelist=changelist)
        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        self.assertEqual(filestate.openActionString, const.ActionTypes.ADD)
        self.SERVER.P4.submit(changelist)

    def test_B01_editFile(self):
        """ Check out and modify the file """
        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        if not os.path.exists(self.LOCAL_PATH):
            if filestate.headActionString in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE, const.ActionTypes.UNKNOWN]:
                # Make sure the file is on the server
                with open(self.LOCAL_PATH, "w+"):
                    pass
                changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Add File")
                self.SERVER.P4.add(self.LOCAL_PATH)
                filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
                self.assertEqual(filestate.openActionString, const.ActionTypes.ADD)
                self.SERVER.P4.submit(changelist)

            else:
                self.SERVER.P4.revert([self.LOCAL_PATH])
                self.SERVER.P4.sync([self.LOCAL_PATH], force=True)

        elif filestate.headActionString != const.ActionTypes.UNKNOWN:
            # Make sure the file is deleted on the server
            if filestate.headActionString in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE]:
                self.SERVER.P4.revert([self.LOCAL_PATH])
                self.SERVER.P4.sync([self.LOCAL_PATH], force=True)
                changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Add File")
                self.SERVER.P4.add(self.LOCAL_PATH)
                filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
                self.assertEqual(filestate.openActionString, const.ActionTypes.DELETE)
                self.SERVER.P4.submit(changelist)

        changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Editing File")
        self.SERVER.P4.edit([self.LOCAL_PATH], changelist=changelist)
        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        self.assertEqual(filestate.openActionString, const.ActionTypes.EDIT)
        self.SERVER.P4.submit(changelist)

    def test_B02_shelveFile(self):
        """ Shelve and unshelve file """
        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        if not os.path.exists(self.LOCAL_PATH):
            if filestate.headActionString in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE,
                                              const.ActionTypes.UNKNOWN]:
                # Make sure the file is on the server
                with open(self.LOCAL_PATH, "w+"):
                    pass
                changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Add File")
                self.SERVER.P4.add(self.LOCAL_PATH, changelist=changelist)
                filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
                self.assertEqual(filestate.openActionString, const.ActionTypes.ADD)
                self.SERVER.P4.submit(changelist)
            else:
                self.SERVER.P4.revert([self.LOCAL_PATH])
                self.SERVER.P4.sync([self.LOCAL_PATH], force=True)

        elif filestate.headActionString != const.ActionTypes.UNKNOWN:
            # Make sure the file is deleted on the server
            if filestate.headActionString in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE]:
                self.SERVER.P4.revert([self.LOCAL_PATH])
                self.SERVER.P4.sync([self.LOCAL_PATH], force=True)
                changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Add File")
                self.SERVER.P4.add(self.LOCAL_PATH, changelist=changelist)
                filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
                self.assertEqual(filestate.openActionString, const.ActionTypes.DELETE)
                self.SERVER.P4.submit(changelist)

        # Get the instances
        changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Shelve Files")
        self.SERVER.P4.edit([self.LOCAL_PATH], changelist=changelist)
        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        self.assertEqual(filestate.openActionString, const.ActionTypes.EDIT)

        # Shelve files
        self.SERVER.P4.shelve([self.LOCAL_PATH], pathsToRemove=None, changelist=changelist)
        # Remove files from local disk to test that files are unshelved properly
        self.SERVER.removeFromWorkspace([self.LOCAL_PATH])
        self.assertFalse(os.path.exists(self.LOCAL_PATH))
        # Get a list of shelved files
        shelvedFiles = self.SERVER.shelvedFiles(changelist)  # returns depot paths
        self.assertTrue(self.DEPOT_PATH in shelvedFiles)

        unshelveChangelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Unshelve Files")
        self.SERVER.P4.unshelve(changelist, unshelveChangelist, force=True)
        unshelvedFiles = self.SERVER.filesInChangelist(unshelveChangelist)  # returns depot paths
        self.assertTrue(self.DEPOT_PATH in unshelvedFiles)
        self.assertTrue(os.path.exists(self.LOCAL_PATH))

        # Clear out changelists
        self.SERVER.P4.revert([self.LOCAL_PATH])
        self.SERVER.P4.deleteShelve(changelist)
        self.SERVER.P4.deleteChangelist(changelist)
        self.SERVER.P4.deleteChangelist(unshelveChangelist)

    def test_B03_moveFiles(self):
        """
        Move files around in perforce
        """
        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        if not os.path.exists(self.LOCAL_PATH):
            if filestate.headActionString in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE,
                                              const.ActionTypes.UNKNOWN]:
                # Make sure the file is on the server
                with open(self.LOCAL_PATH, "w+"):
                    pass
                changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Add File")
                self.SERVER.P4.add(self.LOCAL_PATH, changelist=changelist)
                filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
                self.assertEqual(filestate.openActionString, const.ActionTypes.ADD)
                self.SERVER.P4.submit(changelist)
            else:
                self.SERVER.P4.revert([self.LOCAL_PATH])
                self.SERVER.P4.sync([self.LOCAL_PATH], force=True)

        changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Move Files")
        self.SERVER.P4.edit([self.LOCAL_PATH])
        self.SERVER.P4.move({self.LOCAL_PATH: self.LOCAL_MOVE_PATH}, changelist=changelist)
        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        self.assertEqual(filestate.openActionString, const.ActionTypes.MOVE_DELETE)
        filestate = self.SERVER.P4.fileState(self.LOCAL_MOVE_PATH)
        self.assertEqual(filestate.openActionString, const.ActionTypes.MOVE_ADD)

        self.SERVER.P4.revert([self.LOCAL_MOVE_PATH])
        self.SERVER.P4.revert([self.LOCAL_PATH])
        self.SERVER.P4.deleteChangelist(changelist)

    def test_C01_deleteFile(self):
        """ Delete the file from perforce"""
        # Create File
        if not os.path.exists(os.path.dirname(self.LOCAL_PATH)):
            os.makedirs(os.path.dirname(self.LOCAL_PATH))

        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        if not os.path.exists(self.LOCAL_PATH):
            if filestate.headActionString in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE, const.ActionTypes.UNKNOWN]:
                # Make sure the file is on the server
                with open(self.LOCAL_PATH, "w+"):
                    pass
                changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Add File")
                self.SERVER.P4.add(self.LOCAL_PATH)
                filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
                self.assertEqual(filestate.openActionString, const.ActionTypes.ADD)
                self.SERVER.P4.submit(changelist)
            else:
                # Make sure the file is added on disk
                self.SERVER.P4.revert([self.LOCAL_PATH])
                self.SERVER.P4.sync([self.LOCAL_PATH], force=True)

        elif filestate.headActionString in [const.ActionTypes.DELETE, const.ActionTypes.MOVE_DELETE]:
                self.SERVER.P4.revert([self.LOCAL_PATH])
                self.SERVER.P4.sync([self.LOCAL_PATH], force=True)
                with open(self.LOCAL_PATH, "w+"):
                    pass
                changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Add File")
                self.SERVER.P4.add(self.LOCAL_PATH, changelist=changelist)
                filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
                self.assertEqual(filestate.openActionString, const.ActionTypes.ADD)
                self.SERVER.P4.submit(changelist)

        changelist = self.SERVER.P4.changelist("Techart P4 Unit Test: Delete File")
        self.SERVER.P4.delete([self.LOCAL_PATH], changelist=changelist)
        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        self.assertEqual(filestate.openActionString, const.ActionTypes.DELETE)
        self.SERVER.P4.submit(changelist)


    def test_D01_fileState(self):
        """
        Gets the filestates
        """
        # Test that we can get information from one file
        filestate = self.SERVER.P4.fileState(self.LOCAL_PATH)
        self.assertIsInstance(filestate, core.FileState)

        # Use the local directory for getting results
        fileStates = self.SERVER.P4.folderFileStates(self.LOCAL_DIRECTORY)
        self.assertGreater(len(fileStates), 0)

