import os
import platform
import re

from rsg_clients.perforce import const


def removeSpecialCharacters(filename):
    """
    Removes special characters used by P4 so file paths resolve properly

    Args:
        filename (str): filename to remove special characters from
    """
    filename_ = [filename.replace(character, escape)
                 for character, escape, regex in const.SpecialCharacters.SPECIAL_CHARS_MATRIX
                 if not re.search(regex, filename) and character in filename]
    filename = filename_ or [filename]
    return str(r'{}'.format(filename[0]))


def restoreSpecialCharacters(filename):
    """
    Readds special characters after being cleaned from p4 path.

    Args:
         filename (str): filename to re-add special characters to.
    """
    filename_ = [filename.replace(character, escape)
                 for escape, character, regex in const.SpecialCharacters.SPECIAL_CHARS_MATRIX
                 if re.search(regex, filename) and character in filename]
    filename = filename_ or [filename]
    return str(r'{}'.format(filename[0]))


def convertPath(path):
    """
    Converts depot path to a local path & vice versa

    Args:
        path (string): path to convert
    """
    if platform.system().lower() == "windows":

        if re.match("[a-zA-Z]", path):
            # Convert from local to depot path
            prefix, path, oldSeperator, newSeperator = r"/", path[3:], os.sep, os.altsep
        else:
            prefix, path, oldSeperator, newSeperator = r"x:", path[2:], os.altsep, os.sep

        sections = [prefix] + path.split(oldSeperator)
        return newSeperator.join(sections)
    return path


def convertToDirectory(path, iterative=False):
    """
    Converts the given path to the correct format expected by P4 for querying data of the files in a directory.

    Args:
        path (str): path to covert

    Keyword Args:
        iterative (bool): determines if the fstat command should look for all the subfolders too.

    Returns:
        str
    """
    if not path.endswith("*"):
        # Do a simple check to see if this is a local windows path or a linux/depot path
        seperator = "/" if path.startswith("/") else "\\"

        if path.endswith(seperator):
            # remove the seperator so the join works properly
            path = path[:-1]

        parts = [path, "*"]
        if iterative:
            parts.insert(1, "...")

        path = seperator.join(parts)
    return path


def isDepotPath(path):
    """
    Checks if the input path is a depot path or not.

    Args:
        path (str): the path to check.

    Returns:
        bool: check result.
    """
    return path.startswith("//")
