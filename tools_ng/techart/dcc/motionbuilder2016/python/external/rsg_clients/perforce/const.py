"""Constant values used by the P4 libs."""
import os
import re

from rsg_core_py.abstracts import abstractConsts


class Environment(object):
    """Environment variables that change the behavior of the code."""
    RS_P4_USE_P4CONFIG = re.search("true", os.environ.get("RS_P4_USE_P4CONFIG", "false")) is not None


class AppInfo(object):
    """Application Info."""

    NAME = "Perforce"
    VERSION = "3.5.12"
    TITLE = "{0} (Version {1})".format(NAME, VERSION)


class Servers(object):
    """P4 Server Addresses."""

    GAME = "rsgperforce:1666"
    MOCAP = "rsgperforce:1777"
    DEVELOPMENT = "rsgperforce:3111"


class ChangelistTypes(object):
    """The different states of a changelist."""

    PENDING = "pending"
    SHELVED = "shelved"
    SUBMITTED = "submitted"


class ActionTypes(object):
    """Perform Actions are a note of the last performed action on a file at given revision or changelist."""

    UNKNOWN = "unknown"
    ADD = "add"
    EDIT = "edit"
    DELETE = "delete"
    BRANCH = "branch"
    MOVE_ADD = "move/add"
    MOVE_DELETE = "move/delete"
    INTEGRATE = "integrate"
    IMPORT = "import"
    PURGE = "purge"
    ARCHIVE = "archive"

    # List of all the actions, do not change order as this is the order determined by Helix P4
    ACTIONS = (UNKNOWN, ADD, EDIT, DELETE, BRANCH, MOVE_ADD, MOVE_DELETE, INTEGRATE, IMPORT, PURGE, ARCHIVE)

    _DESCRIPTIONS = {
        "opened for edit": EDIT,
        "also opened by": EDIT,
        "opened for add": ADD,
    }

    @classmethod
    def getByDescription(cls, description):
        """This method converts common messages about files that P4 returns to their appropriate action type.

        Args:
            description (str): description from P4.

        Returns:
            str
        """
        for section, action in cls._DESCRIPTIONS.items():
            if section in description:
                return action

class ExceptionLevels(object):
    """Perforce exception levels that determine whether errors get raised or not.

    Attributes:
        NO_ERRORS: disables all exception handling and makes the interface completely procedural;
            you are responsible for checking the p4.errors and p4.warnings arrays.
        ONLY_ERRORS: causes exceptions to be raised only when errors are encountered.
        ERRORS_AND_WARNINGS: causes exceptions to be raised for both errors and warnings.
    """

    NO_ERRORS = 0
    ONLY_ERRORS = 1
    ERRORS_AND_WARNINGS = 2


class SpecialCharacters(object):
    """Stores special characters that can't be used in p4 commands."""

    SPECIAL_CHARS = ("%", "#", "@")
    ASCII_CHARS = ("%25", "%23", "%40")
    REGEX = ("%25|%23|%40", "%23", "%40")
    SPECIAL_CHARS_MATRIX = [(sChar, aChar, reg) for sChar, aChar, reg in zip(SPECIAL_CHARS, ASCII_CHARS, REGEX)]


class Commands(abstractConsts.AbstractConsts):
    """Consts for all the terminal commands that perforce has."""

    ADD = "add"
    ADMIN = "admin"
    ANNOTATE = "annotate"
    ARCHIVE = "archive"
    ATTRIBUTE = "attribute"
    BRANCH = "branch"
    BRANCHES = "branches"
    CACHEPURGE = "cachepurge"
    CHANGE = "change"
    CHANGELIST = "changelist"
    CHANGELISTS = "changelists"
    CHANGES = "changes"
    CLEAN = "clean"
    CLIENT = "client"
    CLIENTS = "clients"
    CLONE = "clone"
    CONFIGURE = "configure"
    COPY = "copy"
    COUNTER = "counter"
    COUNTERS = "counters"
    CSTAT = "cstat"
    DBSCHEMA = "dbschema"
    DBSTAT = "dbstat"
    DBVERIFY = "dbverify"
    DELETE = "delete"
    DEPOT = "depot"
    DEPOTS = "depots"
    DESCRIBE = "describe"
    DIFF = "diff"
    DIFF2 = "diff2"
    DIRS = "dirs"
    DISKSPACE = "diskspace"
    EDIT = "edit"
    EXPORT = "export"
    FETCH = "fetch"
    FILELOG = "filelog"
    FILES = "files"
    FIX = "fix"
    FIXES = "fixes"
    FLUSH = "flush"
    FSTAT = "fstat"
    GREP = "grep"
    GROUP = "group"
    GROUPS = "groups"
    HAVE = "have"
    HELP = "help"
    IGNORES = "ignores"
    INFO = "info"
    INIT = "init"
    INTEGRATE = "integrate"
    INTEGRATED = "integrated"
    INTERCHANGES = "interchanges"
    ISTAT = "istat"
    JOB = "job"
    JOBS = "jobs"
    JOBSPEC = "jobspec"
    JOURNALCOPY = "journalcopy"
    JOURNALDBCHECKSUMS = "journaldbchecksums"
    JOURNALS = "journals"
    KEY = "key"
    KEYS = "keys"
    LABEL = "label"
    LABELS = "labels"
    LABELSYNC = "labelsync"
    LDAP = "ldap"
    LDAPS = "ldaps"
    LDAPSYNC = "ldapsync"
    LICENSE = "license"
    LIST = "list"
    LOCK = "lock"
    LOCKSTAT = "lockstat"
    LOGAPPEND = "logappend"
    LOGGER = "logger"
    LOGIN = "login"
    LOGOUT = "logout"
    LOGPARSE = "logparse"
    LOGROTATE = "logrotate"
    LOGSCHEMA = "logschema"
    LOGSTAT = "logstat"
    LOGTAIL = "logtail"
    MERGE = "merge"
    MONITOR = "monitor"
    MOVE = "move"
    OBLITERATE = "obliterate"
    OPENED = "opened"
    PASSWD = "passwd"
    PING = "ping"
    POPULATE = "populate"
    PRINT = "print"
    PROPERTY = "property"
    PROTECT = "protect"
    PROTECTS = "protects"
    PROXY = "proxy"
    PRUNE = "prune"
    PULL = "pull"
    PUSH = "push"
    RECONCILE = "reconcile"
    RELOAD = "reload"
    REMOTE = "remote"
    REMOTES = "remotes"
    RENAME = "rename"
    RENAMEUSER = "renameuser"
    REOPEN = "reopen"
    REPLICATE = "replicate"
    RESOLVE = "resolve"
    RESOLVED = "resolved"
    RESTORE = "restore"
    RESUBMIT = "resubmit"
    REVERT = "revert"
    REVIEW = "review"
    REVIEWS = "reviews"
    SERVER = "server"
    SERVERID = "serverid"
    SERVERS = "servers"
    SET = "set"
    SHELVE = "shelve"
    SIZES = "sizes"
    STATUS = "status"
    STREAM = "stream"
    STREAMS = "streams"
    SUBMIT = "submit"
    SWITCH = "switch"
    SYNC = "sync"
    TAG = "tag"
    TICKETS = "tickets"
    TRIGGERS = "triggers"
    TRUST = "trust"
    TYPEMAP = "typemap"
    UNLOAD = "unload"
    UNLOCK = "unlock"
    UNSHELVE = "unshelve"
    UNSUBMIT = "unsubmit"
    UNZIP = "unzip"
    UPDATE = "update"
    USER = "user"
    USERS = "users"
    VERIFY = "verify"
    WHERE = "where"
    WORKSPACE = "workspace"
    WORKSPACES = "workspaces"
    ZIP = "zip"
