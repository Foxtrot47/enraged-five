"""Unit tests for the bridges package."""
import unittest
from rsg_core import conversions
from rsg_core import accounts
from rsg_clients import managers
from rsg_clients.shotgun import accounts as shotgunAccounts


class BaseTest(object):
    def assertBugMatchesTask(self, bug, task):

        # Clear all data to only test data received from the server
        #self.MANAGER.shotgun.clearCache()
        #self.MANAGER.bugstar.clearCache()
        bug.removeEdits()
        task.removeEdits()

        self.assertBugstarIds(bug, task)
        self.assertBugstarIds(bug.project, task.project)
        self.assertEqual(bug.summary, task.bugSummary)
        self.assertEqual(bug.description, task.description)
        self.assertEqual(bug.workflow, self.MANAGER.bridges.shotgunToBugstar.convertWorkflow(task))
        self.assertEqual(bug.state, self.MANAGER.bridges.shotgunToBugstar.convertStatus(task))

        # Confirm owner, qa owner and reviewers are the same
        self.assertEqual(1, len(task.owners))
        self.assertUsers(bug.developer, task.owners[0])
        self.assertUsers(bug.tester, task.qaOwner)
        numberOfValues = len(
            [_f for _f in [bug.reviewer, task.reviewer or None] if _f]
        )  # Check that both reviewer fields have or don't have values
        self.assertTrue(numberOfValues in (0, 2))
        if numberOfValues == 2:
            self.assertUsers(bug.reviewer, task.reviewer[0])

        # Assert other one to one values:
        self.assertBugstarIds(bug.component, task.bugstarComponent)
        self.assertBugstarIds(bug.grid, task.bugstarGrid)
        self.assertBugstarIds(bug.mission, task.mission)

        cinematicAnimator = task.cinematicAnimator.name if task.cinematicAnimator else None
        if cinematicAnimator == "Tech Art Support":
            print("{} does not exist in Shotgun".format(bug.cinematicAnimator))
        else:
            self.assertEqual(bug.cinematicAnimator, cinematicAnimator)  # cinematic animator is a string in bugstar

        # Validate times
#        self.assertDateTimes(bug.createdOn, task.bugstarCreatedOn)
        self.assertEqual(bug.timeSpent, task.bugstarActualTime)
        self.assertEqual(bug.estimatedTime, task.bugstarEstimatedTime)

        # Shotgun only deals with due dates as Year-Month-Day strings
        bugDueDate = conversions.fromDateTime(bug.dueDate, "%Y-%m-%d") if bug.dueDate is not None else None
        self.assertEqual(bugDueDate, task.dueDate)
        numberOfValues = len(
            [_f for _f in [bug.deadlineList, task.deadline or None] if _f]
        )  # Check that both fields have or don't have values

        self.assertTrue(numberOfValues in (0, 2))
        if numberOfValues == 2:
            self.assertEqual(bug.deadlineList[0].name, task.deadline)

        # Validate Lists
        # Comments
        self.assertEqual(len(bug.comments), len(task.bugComments))
        bugstarComments = [comment.text for comment in bug.comments]
        shotgunComments = [comment.body for comment in task.bugComments]
        for index in reversed(bugstarComments):
            if index in shotgunComments:
                bugstarComments.remove(index)
                shotgunComments.remove(index)
        self.assertEqual(len(bugstarComments), len(shotgunComments))

        # Tags
        self.assertEqual(len(bug.tagList), len(task.tags))
        bugstarTags = [tag for tag in bug.tagList]
        shotgunTags = [tag.name for tag in task.tags]
        for index in reversed(bugstarTags):
            if index in shotgunTags:
                bugstarTags.remove(index)
                shotgunTags.remove(index)
        self.assertEqual(len(bugstarTags), len(shotgunTags))

        # Changelists
        self.assertEqual(len(bug.p4ChangeLists), len(task.bugstarChangelists))
        bugstarChangelists = [changelist for changelist in bug.p4ChangeLists]
        shotgunChangelists = [int(changelist.name) for changelist in task.bugstarChangelists]
        for index in reversed(bugstarChangelists):
            if index in shotgunChangelists:
                bugstarChangelists.remove(index)
                shotgunChangelists.remove(index)
        self.assertEqual(len(bugstarChangelists), len(shotgunChangelists))

        # Attachments
        self.assertEqual(len(bug.attachmentList), len(task.versions))
        bugstarAttachmentListIds = [attachment.id for attachment in bug.attachmentList]
        shotgunAttachmentListIds = [attachment.bugstarId for attachment in task.versions]
        for index in reversed(bugstarAttachmentListIds):
            if index in shotgunAttachmentListIds:
                bugstarAttachmentListIds.remove(index)
                shotgunAttachmentListIds.remove(index)
        self.assertEqual(len(bugstarAttachmentListIds), len(shotgunAttachmentListIds))

        # Goals
        self.assertEqual(len(bug.goal), len(task.goals))
        bugstarGoals = [goal.id for goal in bug.goal]
        shotgunGoals = [goal.bugstarId for goal in task.goals]
        for index in reversed(bugstarGoals):
            if index in shotgunGoals:
                bugstarGoals.remove(index)
                shotgunGoals.remove(index)
        self.assertEqual(len(bugstarGoals), len(shotgunGoals))

        # This part of the test will be the one most prone to failure due to people missing from shotgun.
        # CC List
        self.assertEqual(len(bug.ccList), len(task.cc))
        bugstarCcListIds = [cc.id for cc in bug.ccList]
        shotgunCcListIds = [cc.bugstarId for cc in task.cc]
        for index in reversed(bugstarCcListIds):
            if index in shotgunCcListIds:
                bugstarCcListIds.remove(index)
                shotgunCcListIds.remove(index)
        self.assertEqual(len(bugstarCcListIds), len(shotgunCcListIds))

    def assertBugstarIds(self, bugPayload, shotgunPayload):
        numberOfValues = len([_f for _f in [bugPayload, shotgunPayload] if _f])
        self.assertTrue(numberOfValues in (0, 2))
        if numberOfValues == 2:
            self.assertEqual(bugPayload.id, shotgunPayload.bugstarId)

    def assertDateTimes(self, bugDateTime, shotgunDateTime):
        numberOfValues = len([_f for _f in [bugDateTime, shotgunDateTime] if _f])
        self.assertTrue(numberOfValues in (0, 2))
        if numberOfValues == 2:
            self.assertEqual(conversions.fromDateTime(bugDateTime), conversions.fromDateTime(shotgunDateTime))

    def assertUsers(self, bugstarUser, shotgunUser, useNames=False):
        if shotgunUser and shotgunUser.name == "Tech Art Support":
            testUser = self.MANAGER.bridges.bugstarToShotgun.get(bugstarUser)
            if testUser is None:
                # While it sucks that it doesn't match, it is okay for these values not to match under this scenerio
                print("{} does not exist in Shotgun".format(bugstarUser.name))
                return
        if useNames:
            self.assertEqual(bugstarUser.name, shotgunUser.name)
        else:
            self.assertBugstarIds(bugstarUser, shotgunUser)


class TestShotgunToBugstar(unittest.TestCase, BaseTest):
    """Tests for the Shotgun to Bugstar bridge."""

    MANAGER = None

    @classmethod
    def setUpClass(cls):
        """Overrides inherited method.

        Sets up the server connection when the class is invoked the first time.
        """
        cls.MANAGER = managers.Servers()
        cls.MANAGER.setShotgun(cls.MANAGER.DEVELOPMENT, shotgunAccounts.UnitTests())
        cls.MANAGER.setBugstar(cls.MANAGER.DEVELOPMENT, accounts.TechArt())

    @classmethod
    def tearDownClass(cls):
        """Overrides inherited method.

        Closes the server connection when the class is invoked the first time.
        """
        cls.MANAGER.shotgun.logout()
        cls.MANAGER.bugstar.logout()

    def tearDown(self):
        """Method to run every time a test method finishes running."""
        self.MANAGER.shotgun.clearCache()
        self.MANAGER.bugstar.clearCache()

    def test_TaskToBug(self):
        """Test task to bug convertion."""
        task = self.MANAGER.shotgun.task.getByBugstarId(5381704)
        bug = self.MANAGER.bridges.shotgunToBugstar.get(task)
        self.assertBugMatchesTask(bug, task)

    def test_A01_ConvertTaskWithAttachment(self):
        task = self.MANAGER.shotgun.task.getByBugstarId(5381704)  # Python test Task.
        bug = self.MANAGER.bridges.shotgunToBugstar.convertTask(task)
        task.refresh()  # Pick up latest changes from server to get bugstar ID changes on attachments
        self.assertBugMatchesTask(bug, task)


class TestBugstarToShotgun(unittest.TestCase, BaseTest):
    """Tests for the Bugstar to Shotgun bridge."""

    MANAGER = None

    @classmethod
    def setUpClass(cls):
        """Overrides inherited method.

        Sets up the server connection when the class is invoked the first time.
        """
        cls.MANAGER = managers.Servers()
        cls.MANAGER.setShotgun(cls.MANAGER.DEVELOPMENT, shotgunAccounts.UnitTests())
        cls.MANAGER.setBugstar(cls.MANAGER.DEVELOPMENT, accounts.TechArt())

    @classmethod
    def tearDownClass(cls):
        """Overrides inherited method.

        Closes the server connection when the class is invoked the first time.
        """
        cls.MANAGER.shotgun.logout()
        cls.MANAGER.bugstar.logout()

    def tearDown(self):
        """Method to run every time a test method finishes running."""
        self.MANAGER.shotgun.clearCache()
        self.MANAGER.bugstar.clearCache()

    def test_TaskToBug(self):
        """Test bug to task convertion."""
        bug = self.MANAGER.bugstar.bug(5381704)
        task = self.MANAGER.bridges.bugstarToShotgun.get(bug)
        self.assertBugMatchesTask(bug, task)

    def test_SearchToFilter(self):
        search = self.MANAGER.bugstar.bug.search(7200818)
        sgFilter = self.MANAGER.bridges.bugstarToShotgun.convertSearch(search)
        self.assertIsNotNone(sgFilter.search())

    def test_A01_ConvertBug(self):
        bug = self.MANAGER.bugstar.bug(2502525)  # 5399725) #
        task = self.MANAGER.bridges.bugstarToShotgun.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls())
        self.assertBugMatchesTask(bug, task)

    def test_A01_ConvertBugWithGoal(self):
        bug = self.MANAGER.bugstar.bug(5828374)
        task = self.MANAGER.bridges.bugstarToShotgun.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls())
        self.assertBugMatchesTask(bug, task)

    def test_A01_ConvertBugWithAttachment(self):
        bug = self.MANAGER.bugstar.bug(5381704)  # Python test bug.
        task = self.MANAGER.bridges.bugstarToShotgun.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls())
        self.assertBugMatchesTask(bug, task)

    def test_ConvertBugWithMicrosoftWordAttachment(self):
        bug = self.MANAGER.bugstar.bug(5408668)  # 2502525) # 5408668
        task = self.MANAGER.bridges.bugstarToShotgun.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls())
        self.assertBugMatchesTask(bug, task)

    def test_SetDescription(self):
        bug = self.MANAGER.bugstar.bug(6067474)
        task = self.MANAGER.bridges.bugstarToShotgun.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls())
        self.assertBugMatchesTask(bug, task)

    def test_SetName(self):
        bug = self.MANAGER.bugstar.bug(6502967)
        task = self.MANAGER.bridges.bugstarToShotgun.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls())
        self.assertBugMatchesTask(bug, task)

    def test_BadId(self):
        bug = self.MANAGER.bugstar.bug(6615176)
        task = self.MANAGER.bridges.bugstarToShotgun.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls())
        self.assertBugMatchesTask(bug, task)

    def test_ConvertDeadline(self):
        deadline = self.MANAGER.bugstar.deadline(6611370)
        sgProject = self.MANAGER.bridges.bugstarToShotgun.get(deadline.project)
        sgDeadline = self.MANAGER.bridges.bugstarToShotgun.convertDeadline(deadline, sgProject)
        self.assertIsInstance(sgDeadline, self.MANAGER.shotgun.deadline.cls())

    def test_ConvertGrid(self):
        bug = self.MANAGER.bugstar.bug(2747803)
        sgProject = self.MANAGER.bridges.bugstarToShotgun.get(bug.project)
        sgGrid = self.MANAGER.bridges.bugstarToShotgun.convertGrid(bug, sgProject)
        self.assertIsInstance(sgGrid, self.MANAGER.shotgun.location.cls())
