"""Bridges for converting generic bugstar to formats digestible by other platforms."""
import os
import re

from rsg_clients import const as clientsConst
from rsg_clients.bridges import const, exceptions
from rsg_clients.bridges.integrations import abstractBridge
from rsg_clients.bugstar import const as bugstarConst
from rsg_clients.bugstar import exceptions as bugstarExceptions
from rsg_clients.shotgun import const as shotgunConst
from rsg_core import conversions
from rsg_core_py.decorators import memoize


class ShotgunBridge(abstractBridge.AbstractBridge):
    """Bridge for converting bugstar data to the matching shotgun data."""

    KEY = clientsConst.Clients.SHOTGUN  # Key associated with this class for mapping purposes
    REVISION_REGEX = re.compile(r"(?<=\(Rev)[0-9/ +]+(?=\))")
    COMMENT_REGEX = re.compile("Comment ((from Shotgun)|by)")
    MAPPING_PATH = os.path.join(const.Resources.MAPPINGS_DIRECTORY, "bugstar.json")

    def __init__(self, bugstar, shotgun):
        """Initializer.

        Args:
            bugstar (rsg_clients.bugstar.servers.BaseServer): bugstar server that data is coming from.
            shotgun (rsg_clients.shotgun.servers..BaseServer): shotgun server that data is going to.
        """
        super(ShotgunBridge, self).__init__(source=bugstar, target=shotgun)
        self._defaultUser = self.shotgun.humanUser.getByName("Tech Art Support")

    @property
    def bugstar(self):
        """The bugstar server being accessed."""
        return self._source

    @property
    def shotgun(self):
        """The shotgun server being accessed."""
        return self._target

    def get(self, payload):
        """Gets the matching shotgun payload for this payload.

        This will not attempt to match the any data between two data types if a match is found.

        Args:
            payload (rsg_clients.bugstar.payloads.basePayload): bugstar payload to convert

        Returns:
            rsg_clients.shotgun.entities.baseEntity
        """
        if payload is None:
            return

        elif self.bugstar.user.isInstance(payload):
            return self.shotgun.humanUser.getByUsername(payload.username)

        elif self.bugstar.team.isInstance(payload):
            return self.shotgun.group.getByBugstarId(payload.id)

        elif isinstance(payload, (self.bugstar.department.cls(), self.bugstar.globalDepartment.cls())):
            return self.shotgun.department.getByName(payload.name)

        elif self.bugstar.project.isInstance(payload):
            return self.shotgun.project.getByBugstarId(payload.id) or self.shotgun.project.getByName(payload.name)

        elif self.bugstar.bug.isInstance(payload):
            return self.shotgun.task.getByBugstarId(payload.id)

        elif self.bugstar.mission.isInstance(payload):
            if payload.subtype == self.shotgun.strand.subtypes.MISSION and payload.bugstarId:
                return self.shotgun.strand.getByBugstarId(payload.bugstarId)

        elif self.bugstar.goal.isInstance(payload):
            return self.shotgun.goal.getByBugstarId(payload.id)

        elif self.bugstar.tag.isInstance(payload):
            return self.shotgun.tag.getByName(payload.name)

        elif self.bugstar.search.isInstance(payload):
            return self.convertSearch(payload)

    def getTask(self, bug):
        """Gets the bug associated with the given task.

        Args:
            bug (rsg_clients.bugstar.payloads.bug.Bug): bug whose task we are retrieving

        Returns:
            rsg_clients.shotgun.entities.task.Task
        """
        project = self.get(bug.project)
        task = self.shotgun.task.getByBugstarId(bug.id) or self.shotgun.task.getByBugNumber(bug.id)

        if task is None:
            task = self.shotgun.task()
            task.project = project
            task.bugstarId = bug.id

        elif task.bugNumber and task.bugstarId is None:
            task.bugstarId = task.bugNumber
        return task

    def getRevision(self, bug):
        """Gets the revision number for the bug based on the bug summary.

        Args:
            bug (rsg_clients.bugstar.contents.Bug): bug to get revision number from

        Returns:
            int
        """
        regex = self.REVISION_REGEX.search(bug.summary)
        if regex:
            return int(regex.group())
        return 1

    def convert(self, payload):
        """Gets the matching shotgun entity for this payload.

        This attempt to match the data between two data types.
        The data returned is not submitted to the server, that must be done explicitly.

        Args:
            payload (rsg_clients.bugstar.payloads.abstractPayload.AbstractPayload): payload to convert to a bugstar object.

        Returns:
            rsg_clients.shotgun.entities.baseEntity.BaseEntity
        """
        if self.bugstar.bug.isInstance(payload):
            return self.convertBug(payload)
        elif self.bugstar.mission.isInstance(payload):
            return self.convertMission(payload)
        elif self.bugstar.user.isInstance(payload) or self.bugstar.team.isInstance(payload):
            return self.convertUser(payload, createGroup=True)
        elif self.bugstar.search.isInstance(payload):
            return self.convertSearch(payload)

    def convertBug(self, bug):
        """Converts a bug to a task.

        Notes:
            If a bug already has a task associated with it, it will update that task.
            Otherwise this method will error out if the create flag is not set.

            In order to add comments and tags, we need to the task to be available on the server at this time.
            As such if we are converting a bug to a task, then it gets created in this method.

        Args:
            bug (rsg_clients.bugstar.payloads.Task): task to convert into a bug

        Keyword Args:
            create (bool): creates a bug if the task does not already have one

        Returns:
            rsg_clients.bugstar.payload.bug.Bug
        """
        task = self.getTask(bug)

        # Required Fields
        project = self.get(bug.project)
        if not project:
            raise exceptions.ResolveError("Could not access Shotgun Project for {}".format(bug.project.name))

        task.name = bug.summary
        task.bugstarId = bug.id
        # Not required fields for creation
        task.owners = [self.convert(bug.developer)]
        task.qaOwner = self.convert(bug.tester)
        task.reviewer = list(filter(None, [self.get(bug.reviewer)] if bug.reviewer else []))
        task.bugSummary = bug.summary
        task.description = bug.description
        task.workflow = self.convertWorkflow(bug)
        task.status = self.convertStatus(bug.state, bug.isWorking)
        task.cc = list(filter(
            lambda user: user is not None and user != self._defaultUser, [self.convertUser(cc) for cc in bug.ccList]
        ))

        task.mission = self.convertMission(bug.mission, project) if bug.mission else None
        task.goals = self.convertGoals(bug, project)
        task.subtype = self.shotgun.task.subtypes.BUG
        task.bugstarActualTime = bug.timeSpent
        task.bugstarEstimatedTime = float(bug.estimatedTime) if bug.estimatedTime is not None else None
        task.bugstarCreatedOn = bug.createdOn
        task.bugstarGrid = self.convertGrid(bug, project)
        task.bugstarComponent = self.convertComponent(bug, project)

        # bugstar-copied versions need to be appended to the existing list to avoid unlinking human-added versions
        versions = task.versions
        versionsToAppend = self.convertAttachments(task, bug) if bug.attachmentList else []
        for version in versionsToAppend:
            if not version in versions:
                versions.append(version)
        task.versions = versions

        deadline = None
        dueDate = bug.dueDate

        if bug.deadlineList:
            deadline = self.convertDeadline(bug.deadlineList[0], project)

        # TODO: Add support for the Time Log entity for added time against a task.
        # TODO: Make the deadline an entity on Shotgun
        dueDate = conversions.fromDateTime(dueDate, shotgunConst.AppInfo.DATE_TIME_FORMAT) if dueDate else None
        task.deadline = deadline
        task.dueDate = dueDate

        if task.exists:
            task.update()
            self.debug("Updated task {} with data from bug {}".format(task.id, bug.id))
        else:
            # If 2 searches are picking up the same bug at the same time, regular create will cause duplicate creations.
            task.create(validateField=shotgunConst.Fields.BUG_NUMBER)

        self.convertComments(bug, task)
        self.convertTags(bug.tagList, task)
        self.convertTags(bug.p4ChangeLists, task, shotgunConst.Fields.BUGSTAR_CHANGELISTS, inBugstar=False)
        # TODO: Investigate the best way to compare attachments
        # TODO: Add missing properties to Shotgun
        return task

    def convertUser(self, user, createGroup=False):
        """Converts the given user or Team payload into a humanUser or group entity on Shotgun.

        If the team does not exist on Shotgun, it will be created.

        Args:
            user (rsg_clients.bugstar.contents.AbstractPayload): a user or team to find on Shotgun
            createGroup (bool): create a group if the user is a Team on bugstar and it currently does not exist on Shotgun

        Returns:
            rockstar.shotgun.entities.baseEntity.BaseEntity
        """
        shotgunUser = None
        if self.bugstar.user.isInstance(user):
            shotgunUser = self.shotgun.humanUser.getByUsername(user.username)
        elif self.bugstar.team.isInstance(user):
            shotgunUser = self.shotgun.group.getByBugstarId(user.id)
            if not shotgunUser and createGroup:
                shotgunUser = self.shotgun.group()
                shotgunUser.name = user.name
                shotgunUser.bugstarId = user.id
                shotgunUser.create()
                self.debug("Adding Team/Group {} to Shotgun".format(user.name))

        if not shotgunUser:
            shotgunUser = self._defaultUser

        return shotgunUser

    def convertMission(self, mission, project):
        """Converts the bugstar mission payload into a strand entity on Shotgun.

        If the entity doesn't exist, it will add it to shotgun.

        Args:
            bug (roockstar.clients.bugstar.payloads.mission): mission to convert
            project (rsg_clients.shotgun.entities.project.Project): shotgun project representing

        Returns:
            rsg_clients.shotgun.entities.mission.Mission
        """
        filters = [
            [shotgunConst.Fields.PROJECT, shotgunConst.Operators.IS, project],
            [shotgunConst.Fields.ENTITY_SUBTYPE, shotgunConst.Operators.IS, self.shotgun.strand.subtypes.MISSION],
        ]
        shotgunMission = self.shotgun.strand.getByBugstarId(mission.id, filters=filters)
        create = False
        if not shotgunMission:
            shotgunMission = self.shotgun.strand()
            shotgunMission.bugstarId = mission.id
            shotgunMission.subtype = self.shotgun.strand.subtypes.MISSION
            create = True

        if create is True or mission.version != shotgunMission.version:
            cinematicAnimator = None
            if mission.cinematicAnimator:
                cinematicAnimator = self.get(mission.cinematicAnimator) or self._defaultUser

            levelDesigner = None
            if mission.levelDesigner:
                levelDesigner = self.get(mission.levelDesigner) or self._defaultUser

            audioDesigner = None
            if mission.audioDesigner:
                audioDesigner = self.get(mission.audioDesigner) or self._defaultUser
            
            shotgunMission.name = mission.name
            shotgunMission.missionId = mission.missionId
            shotgunMission.description = mission.description
            shotgunMission.storyDescription = mission.storyDescription
            shotgunMission.cinematicAnimator = cinematicAnimator
            shotgunMission.levelDesigner = levelDesigner
            shotgunMission.audioDesigner = audioDesigner
            shotgunMission.version = mission.version
            shotgunMission.project = project
            shotgunMission.active = mission.active == "true"
            shotgunMission.singlePlayer = mission.singlePlayer or False

            if create:
                shotgunMission.create()
                self.debug("Adding Mission {} to Shotgun".format(mission.name))
            else:
                shotgunMission.update()
                self.debug("Updating Mission {} on Shotgun".format(mission.name))
        return shotgunMission

    def convertDeadline(self, deadline, project):
        """Converts a bugstar deadline into a shotgun deadline.

        Returns:
            rsg_clients.shotgun.entities.deadline.Deadline
        """
        sgDeadline = self.shotgun.deadline.getByBugstarId(deadline.id)
        if not sgDeadline:
            sgDeadline = self.shotgun.deadline()
            sgDeadline.name = deadline.name
            sgDeadline.bugstarId = deadline.id
            sgDeadline.project = project
            sgDeadline.description = deadline.reason
            sgDeadline.startDate = deadline.startDate
            sgDeadline.dueDate = deadline.due
            sgDeadline.submissionDate = deadline.submissionDate
            sgDeadline.create()
        return sgDeadline

    def convertGoals(self, bug, project):
        """Converts a bugstar goal into a shotgun goal.

        Notes:
            Once a goal is disabled on bugstar it becomes undiscoverable by non-super admin accounts

        Args:
            bug (rsg_clients.bugstar.payload.bug.Bug): bug to get goal from
            project (rsg_clients.shotgun.entities.project.Project): The project that the goal belongs to

        Returns:
            rsg_clients.shotgun.entities.goal.Goal
        """
        goals = []
        for goal in bug.goal:
            shotgunGoal = self.shotgun.goal.getByBugstarId(goal.id)

            if goal and not shotgunGoal:
                shotgunGoal = self.shotgun.goal()
                shotgunGoal.name = goal.name
                shotgunGoal.description = goal.description
                shotgunGoal.bugstarId = goal.id
                shotgunGoal.project = project
                shotgunGoal.create()
                # TODO: Add support for goal status

            if shotgunGoal:
                goals.append(shotgunGoal)

        return goals

    def convertComponent(self, bug, project):
        """Converts a bugstar component into a shotgun component.

        Notes:
            Once a component is disabled on bugstar it becomes undiscoverable by non-super admin accounts

        Args:
            bug (rsg_clients.bugstar.payload.bug.Bug): bug to get component from
            project (rsg_clients.shotgun.entities.project.Project): The project that the component belongs to

        Returns:
            rsg_clients.shotgun.entities.component.Component
        """
        component = bug.component
        if not component:
            return None

        shotgunComponent = self.shotgun.component.getByBugstarId(component.id)

        if component and not shotgunComponent:
            shotgunComponent = self.shotgun.component()
            shotgunComponent.name = component.hierarchyName
            shotgunComponent.bugstarId = component.id
            shotgunComponent.project = project
            shotgunComponent.create()

        return shotgunComponent

    def convertGrid(self, bug, project):
        """Converts a bugstar grid into a shotgun grid.

        Notes:
            Once a grid is disabled on bugstar it becomes undiscoverable by non-super admin accounts

        Args:
            bug (rsg_clients.bugstar.payload.bug.Bug): bug to get grid from
            project (rsg_clients.shotgun.entities.project.Project): The project that the grid belongs to

        Returns:
            rsg_clients.shotgun.entities.grid.Grid
        """
        grid = bug.grid
        if not grid:
            return None

        shotgunGrid = self.shotgun.location.getByBugstarId(grid.id, filters=[
            [shotgunConst.Fields.ENTITY_SUBTYPE, shotgunConst.Operators.IS, self.shotgun.location.subtypes.GRID]]
                                                           )

        if not shotgunGrid:
            shotgunGrid = self.shotgun.location()
            shotgunGrid.name = grid.name
            shotgunGrid.bugstarId = grid.id
            shotgunGrid.project = project
            shotgunGrid.subtype = self.shotgun.location.subtypes.GRID
            shotgunGrid.create()

        elif shotgunGrid and not shotgunGrid.bugstarId:
            shotgunGrid.bugstarId = grid.id
            shotgunGrid.updateFields([shotgunConst.Fields.BUGSTAR_ID])

        return shotgunGrid

    def convertWorkflow(self, bug):
        """Converts the Bugstar workflow into a Shotgun status.

        Args:
            bug (rsg_clients.bugstar.payloads.bug.Bug): bug whose workflow should be converted.

        Returns:
            str
        """
        return bugstarConst.Workflows.getWorkflowName(bug.workflow)

    def convertStatus(self, state, isWorking):
        """Converts the Bugstar status into a Shotgun status.

        Args:
            status (str): the bugstar status to convert into a shotgun status.
            isWorking (bool): is the bug marked as isWorking on bugstar.

        Returns:
            str
        """
        if state == bugstarConst.States.DEV_FIXING and not isWorking:
            return shotgunConst.Statuses.READY_TO_START

        shotgunConst.Statuses.getByName(bugstarConst.States.getFullNameFromState(state))
        return shotgunConst.Statuses.getByName(bugstarConst.States.getFullNameFromState(state))

    def convertComments(self, bug, task):
        """Converts the bugstar comments into shotgun replies.

        As you need to have a note in shotgun to add replies, we create/update the note and replies in this method.

        Args:
            bug (rsg_clients.bugstar.payloads.bug.Bug): bug to get comments from.
            task (rsg_clients.shotgun.entities.task.Task): task whose comments we are converting

        Returns:
            list
        """
        self.debug("Matching comments from bug {} to notes on shotgun task {}".format(bug.id, task.id))

        noteName = "Bug {} Comments".format(bug.id)
        bugstarNote = None
        for note in task.notes:
            if noteName == note.subject:
                bugstarNote = note
                break

        # Add the note to hold the bugstar comments if it already doesn't exist
        if bugstarNote is None:
            bugstarNote = task.addNote(
                "Comments from Bugstar Bug {}".format(bug.id),
                self.shotgun.humanUser.getByName("Tech Art Support"),
                "Bug {} Comments".format(bug.id),
            )

        # Make sure that the note has the appropriate fields set to denote it as the holder of bugstar comments
        updateFields = []
        if bugstarNote.status != shotgunConst.Statuses.READY:
            bugstarNote.status = shotgunConst.Statuses.READY
            updateFields.append(shotgunConst.Fields.STATUS)

        if bugstarNote.subtype != self.shotgun.note.subtypes.BUG:
            bugstarNote.subtype = self.shotgun.note.subtypes.BUG
            updateFields.append(shotgunConst.Fields.ENTITY_SUBTYPE)

        if updateFields:
            bugstarNote.updateFields(updateFields)

        taskComments = {}
        for reply in bugstarNote.replies:
            taskComments.setdefault(reply.position, []).append(reply)

        bugComments = list(bug.comments)
        bugComments.reverse()  # bugstar lists the newest comments first

        create, update, delete = [], [], taskComments.get(None, [])

        # Add the new comments from Bugstar to Shotgun as replies
        for index, comment in enumerate(bugComments):
            replies = taskComments.get(index, [])

            # Remove duplicate comments
            delete.extend(replies[1:])

            author = self.get(comment.creator)

            # Add comment on Shotgun
            if not replies:
                text = comment.text
                if author is None:
                    text = "Comment by {}:\n{}".format(comment.creator.name, text)

                reply = self.shotgun.reply()
                reply.author = author or self._defaultUser
                reply.body = text
                reply.bugstarCreatedOn = comment.createdOn
                reply.position = index
                reply.link = bugstarNote

                create.append(reply)
                continue

            # Update existing comments if need be
            reply = replies[0]
            updateAuthor = author and reply.author != author
            updateDate = reply.bugstarCreatedOn is None
            updateReply = any([updateAuthor, updateDate])

            if updateAuthor:
                reply.author = author
                reply.body = comment.text

            if updateDate:
                reply.bugstarCreatedOn = comment.createdOn

            if updateReply:
                update.append(reply)
        # Remove comments that aren't in Bugstar from Shotgun
        bugstarCommentCount = len(bugComments)
        taskCommentCount = len(taskComments)
        if taskCommentCount > bugstarCommentCount:
            for index in xrange(bugstarCommentCount, taskCommentCount):
                replies = taskComments.get(index, [])
                delete.extend(replies[0:1])

        self.shotgun.batch(create=create, update=update, delete=delete)

    def convertTags(self, tags, task, shotgunField=None, inBugstar=True):
        """Adds Tags to the given task.

        Args:
            tags (list of str): list of tags to convert into shotgun tags
            task (rsg_clients.shotgun.entities.task.Task): task to add/remove the tags to/from.

            shotgunField (str, optional): the shotgun field to add/remove the tags to
            inBugstar (bool): Are these tags in bugstar
        """
        dataType = "tag"
        shotgunField = shotgunField or shotgunConst.Fields.TAGS
        shotgunTags = task._getmutable(shotgunField, list)
        shotgunTagsDict = {tag.name: tag for tag in shotgunTags}

        # Add missing tags
        newTags = []
        update = False
        for tagString in tags:
            isChangelist = isinstance(tagString, (int, long))
            if isChangelist:
                dataType = "changelist"
                tagString = str(tagString)
            if tagString not in shotgunTagsDict:
                tag = self.shotgun.tag.getByName(tagString)
                if not tag:
                    tag = self.shotgun.tag()
                    tag.name = str(tagString)
                    tag.inBugstar = inBugstar
                    tag.isChangelist = isChangelist
                    newTags.append(tag)
                self.debug("Adding missing {} {} on {}".format(dataType, tagString, task.name))
                shotgunTags.append(tag)
                update = True
            else:
                shotgunTagsDict.pop(tagString, None)

        # Remove tags that are no longer valid
        for tag in shotgunTagsDict.itervalues():
            shotgunTags.remove(tag)
            update = True

        if newTags:
            self.shotgun.batch(create=newTags)

        if update:
            task.updateFields(fields=shotgunField)

    def convertList(self, shotgunList, bugstarList, additive=True, convert=None):
        """Converts a list of bugstar items into their correct shotgun values.

        Args:
            shotgunList (list): shotgun values to convert
            bugstarList (list): the current bugstar values

            additive (bool, optional): doesn't remove missing differences, only appends.
            convert (func, optional): function for converting the value. Defaults to the main convert method

        Returns:
            list
        """
        convert = convert or self.convert
        if not additive:
            return [convert(item) for item in shotgunList]

        newList = list(bugstarList)
        for item in shotgunList:
            bugstarItem = convert(item)
            if bugstarItem is not None and bugstarItem not in newList:
                newList.append(bugstarItem)

        return newList

    def convertAttachments(self, task, bug):
        """Converts a list of bugstar attachments to versions attached to the task in shotgun.

        Args:
            task (rsg_clients.shotgun.entities.task.Task): task we are updating.
            bug (rsg_clients.bugstar.payloads.bug.Bug): Bug object from Bugstar

        Returns:
            list (rsg_clients.shotgun.entities.version.Version) - list of version entities
        """
        # TODO: Optimize with batch commands
        self.debug("Matching attachment from bug {} to attachments on shotgun task {}".format(bug.id, task.id))
        versions = task.versions or []

        taskAttachments = {version.bugstarId: version for version in versions}
        taskAttachmentIds = set(taskAttachments.keys())

        bugAttachments = {attachment.id: attachment for attachment in bug.attachmentList}
        bugAttachmentIds = set(bugAttachments.keys())

        # Compare existing task versions and bug attachments
        removeAttachments = taskAttachmentIds - bugAttachmentIds
        addAttachments = sorted(list(bugAttachmentIds - taskAttachmentIds))

        imageIds = sorted(
            list(filter(
                lambda attachment: attachment.contentType.startswith("image") and attachment.id in addAttachments,
                bug.attachmentList,
            )), key=lambda attachment: attachment.id
        )

        thumbnailId = imageIds[-1].id if imageIds else None

        # Add missing attachments to the task
        # Check if attachment exists on the task, if not check if it exists at all on the shotgun server,
        # if not then create one.
        for attachmentId in addAttachments:
            attachment = bugAttachments[attachmentId]
            if attachment.id not in taskAttachments:

                try:
                    downloadedAttachment = (
                        attachment.download(
                            bugstarConst.Paths.ATTACHMENTS_DIRECTORY) if not attachment.isArchived else None
                    )
                except bugstarExceptions.FileNotFoundOnServer as exception:
                    self.warning("Could not download attachment:\n{}".format(str(exception)))
                    downloadedAttachment = None

                version = self.shotgun.version.getByBugstarId(attachment.id)
                if not version:
                    version = self.shotgun.version()
                    version.name = os.path.splitext(attachment.name)[0]
                    version.project = task.project
                    version.bugstarId = attachment.id
                    version.task = task
                    version.entity = task.entity
                    version.archived = attachment.isArchived == "true"
                    version.create()
                    # Check that file has some data in it, as some bugs have logs attached as an attachment,
                    # that are usually empty and shotgun errors out when we try to upload an empty file.
                    if downloadedAttachment and attachment.fileSize != 0:
                        # Must provide to which field do you want to put the link to be clickable to access the attachment.
                        version.upload(
                            downloadedAttachment,
                            field=shotgunConst.Fields.UPLOADED_MOVIE,
                            name=os.path.basename(downloadedAttachment),
                        )

                elif task != version.task:
                    version.task = task
                    version.entity = task.entity

                    version.updateFields([shotgunConst.Fields.TASK, shotgunConst.Fields.ENTITY])

                # Remove junk from the file system
                if downloadedAttachment and os.path.exists(downloadedAttachment):
                    os.remove(downloadedAttachment)

                versions.append(version)

        # Disconnect attachment versions that doesn't exist on the bug in Bugstar any more.
        if removeAttachments:
            for attachmentId in removeAttachments:
                version = taskAttachments[attachmentId]
                version.task = None
                version.entity = None

                version.updateFields([shotgunConst.Fields.TASK, shotgunConst.Fields.ENTITY])

                versions.remove(version)

        # Not needed to update shotgun server, but needed to update the cached task object
        return versions

    def convertSearch(self, search):
        """Converts a search in to a complex filter that is consumable by Shotgun.

        Returns:
            dict
        """
        bugstarRoot = search.root()
        collection = self.convertCollection(bugstarRoot)
        # In Shotgun we dictate if a task is bug by its type being bug and the bugstar id having value.
        # In turn we are going to add to the search an additional filter to only look for tags that are bugs
        bugFilter = {"logical_operator": "and",
                     "conditions": [[shotgunConst.Fields.BUGSTAR_ID, shotgunConst.Operators.IS_NOT, None],
                                    [shotgunConst.Fields.ENTITY_SUBTYPE, shotgunConst.Operators.IS,
                                     shotgunConst.SubTypes.BUG]]}
        collection['conditions'].append(bugFilter)
        return self.shotgun.task.search(collection)

    def convertCollection(self, collection):
        """Converts a bugstar collection object into a complex filter that is consumable by shotgun.

        Args:
            collection (rsg_clients.bugstar.paylaods.search.Collection): bugstar collection to convert into a simple filter

        Returns:
            dict
        """
        targetField, _, targetValue, __ = self.mapValues(self._targetClient, "collection", "op", collection.operation)
        dictionary = {targetField: targetValue}
        children = []
        for child in collection.children:
            if child.disabled:
                continue
            elif self.bugstar.search.isCollection(child):
                children.append(self.convertCollection(child))
            else:
                children.append(self.convertComparison(child))
        dictionary["conditions"] = children
        return dictionary

    def convertComparison(self, comparison):
        """Converts a bugstar comparison object into a basic filter that is consumable by shotgun.

        Args:
            comparison (rsg_clients.bugstar.paylaods.search.Comparison): bugstar comparison to convert into a simple filter

        Returns:
            list of str, str, and object
        """
        targetField, targetDataType, targetValue, _ = self.mapValues(self._targetClient, "comparison", comparison.field,
                                                                  comparison._values)
        targetFieldFormat, alternateDataType, operatorValue, useAlternateDataType = self.mapValues(self._targetClient, "comparison", "op",
                                                              comparison.operation)

        if "{source}" in targetFieldFormat:
            targetField = targetFieldFormat.format(source=targetField)
        targetValue = self.convertValue(targetDataType if not useAlternateDataType else alternateDataType, targetValue)
        return [targetField, operatorValue, targetValue]

    def convertValue(self, dataType, value):
        """Convert a bugstar value object into the a data type that the shotgun filters can can consume.

        Args:
            dataType (str): the type of data that the value contains
            value (object):  the data to convert into a relevant data type

        Returns:
            object
        """
        if dataType is None:
            return value

        elif dataType == "datetime":
            value = conversions.fromDateTime(value[0].value)

        elif dataType == "days":
            value = [int(value[0].value), "DAY"] # The first value should be an int

        elif dataType == "status":
            statuses = []
            for _value in value:
                status = _value.value.split("/")[-1]
                if status == bugstarConst.SearchValues.NOT_CLOSED_STATES:
                    statuses.extend(
                        [self.convertStatus(_status, isWorking=False) for _status in bugstarConst.States.NOT_CLOSED])
                    continue
                statuses.append(self.convertStatus(status, isWorking=False))

            if shotgunConst.Statuses.DEV_FIXING in statuses:
                statuses.append(shotgunConst.Statuses.READY_TO_START)
            value = statuses

        elif isinstance(value, (list, tuple)):
            builder = self.bugstar.instance._getBuilder(dataType)
            values = []
            for _value in value:
                payload = builder(int(_value.value))
                entity = self.get(payload)
                if not entity:
                    raise ValueError("{} {} is not available in Shotgun".format(payload.key()[:-1], payload.name))
                values.append({"id": entity.id, "type": entity.type})
            value = values
        else:
            builder = self.bugstar.instance._getBuilder(dataType)
            entity = self.get(builder(int(value.value)))
            value = ({"id": entity.id, "type": entity.type})

        return value


@memoize.memoize
def getByType(key):
    """Gets the bridge based on the client it needs to go.

    Args:
        key (str): the type of bridge to go to.

    Returns:
        rsg_clients.bridges.adapters.abstractBridge.AbstractBridge
    """
    for subclass in abstractBridge.AbstractBridge.__subclasses__():
        if subclass.__module__ == __name__ and subclass.KEY == key:
            return subclass
