from rsg_core_py.exceptions import types


class DataBridgeError(types.RockstarError):
    """Base error for this package."""


class InvalidEntitySubtype(DataBridgeError):
    """The entity is not of a valid subtype to perform the request operation."""


class ResolveError(DataBridgeError):
    """The data could not be converted to a matching format on the other server."""


class InvalidProject(DataBridgeError):
    """The provided project does not exist in the given server."""
