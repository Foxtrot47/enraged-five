"""Static values for bridges packages."""
import os


class AppInfo(object):
    """Information about the package."""

    NAME = "Bridges"
    VERSION = "0.5.3"
    TITLE = "{0} (Version {1})".format(NAME, VERSION)


class Resources(object):
    ROOT_PATH = os.path.dirname(__file__)
    RESOURCES_DIRECTORY = os.path.join(ROOT_PATH, "resources")
    MAPPINGS_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "mappings")
