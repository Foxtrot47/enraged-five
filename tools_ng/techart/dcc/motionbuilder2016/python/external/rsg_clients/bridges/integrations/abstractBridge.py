"""Abstract class for making bridges."""
import json
import logging


class AbstractBridge(object):
    """Bridge for converting bugstar data to the matching shotgun data."""

    LOGGER = None
    KEY = None
    MAPPING_PATH = None

    def __init__(self, source, target):
        """Initializer.

        Args:
            source (rsg_core_py.abstracts.abstractServer.AbstractServer): server that data is coming from
            target (rsg_core_py.abstracts.abstractServer.AbstractServer): server that data is going to
        """
        self._source = source
        self._target = target
        self._targetClient = target.instance.client
        self._logger = logging.getLogger(self.__module__)
        self._mapping = None

    def get(self, value):
        """Virtual method.

        Catch all method for getting data from the target server that matches the data from the source server.

        Args:
            value (object): data from the source server to match

        Returns:
            object
        """
        raise NotImplementedError

    def convert(self, value):
        """Virtual method.

        Catch all method for converting data from the target server that matches the data from the source server.

        Args:
            value (object): data from the source server convert into data in the target server.

        Returns:
            object
        """
        raise NotImplementedError

    @property
    def mapping(self):
        if not self._mapping:
            with open(self.MAPPING_PATH, "r") as handler:
                self._mapping = json.load(handler)
        return self._mapping

    @property
    def source(self):
        """The source server to map from.

        Returns:
            rsg_core_py.abstracts.abstractServer.AbstractServer
        """
        return self._source

    @property
    def target(self):
        """The target server to map to.

        Returns:
            rsg_core_py.abstracts.abstractServer.AbstractServer
        """
        return self._target

    def mapValues(self, targetClient, sourcePayloadType, sourceFieldName, sourceValue):
        """Maps search values to the target client.

        Args:
            targetClient (str): the client we are trying to convert the data to
            sourcePayloadType (str): the type of the payload we are converting
            sourceFieldName (str): the field name we are converting
            sourceValue (object): the data we are converting

        Returns:
            list of str, str, object, bool
        """
        commonMappings = self.mapping.get("__commonFields__", {})
        payloadMapping = self.mapping.get(sourcePayloadType, {})

        for mappings in (commonMappings, payloadMapping):
            # Check that the mappings even support the source field
            if not mappings or sourceFieldName not in mappings:
                continue

            # Check that there are mappings for the target client for the field we are converting
            targetFieldMapping = mappings.get(sourceFieldName, {})
            if not targetClient in targetFieldMapping:
                continue

            # See if the value can be converted over to another based on the contents of the mapping
            targetValue = sourceValue
            if isinstance(sourceValue, list):
                targetValue = tuple(sourceValue)

            targetClientMapping = targetFieldMapping.get(targetClient, {})
            targetValue = targetClientMapping.get(targetValue, None)
            targetDataType = None
            alternateDataType = False
            if isinstance(targetValue, dict):
                targetValueMapping = targetValue
                targetFieldName = targetValueMapping.get("__fieldname__", "{source}")
                targetValue = targetValueMapping.get("__value__", None)
                targetDataType = targetValueMapping.get("__datatypes__", None)
                alternateDataType = targetDataType is not None
            else:
                targetFieldName = targetClientMapping.get("__fieldname__", "{source}")
            targetValue = targetValue or sourceValue
            targetDataType = targetClientMapping.get("__datatypes__", targetDataType or type(targetValue))
            return targetFieldName, targetDataType, targetValue, alternateDataType

    def logger(self):
        """The logger being used by this bridge.

        Returns:
            logging.logger
        """
        return self._logger or self.LOGGER

    def setLogger(self, value):
        """Sets the logger being used by this bridge.

        Args:
            value (logging.logger): logger to use
        """
        self._logger = value

    def debug(self, message):
        """Logs debug to the log, if one is available.

        Args:
            message (str): message to add to the log
        """
        if self.logger():
            self._logger.debug(message)
        else:
            print(message)

    def info(self, message):
        """Logs information to the log, if one is available.

        Args:
            message (str): message to add to the log
        """
        if self.logger():
            self._logger.info(message)
        else:
            print(message)

    def warning(self, message):
        """Logs warning to the log, if one is available.

        Args:
            message (str): message to add to the log
        """
        if self.logger():
            self._logger.warning(message)
        else:
            print(message)

    def error(self, message, error=None):
        """Logs error to the log, if one is available.

        Args:
            message (str): message to add to the log
            error (Exception): exception to attach to the log
        """
        if self.logger():
            self._logger.error(message, exc_info=error)
        else:
            print(message)
