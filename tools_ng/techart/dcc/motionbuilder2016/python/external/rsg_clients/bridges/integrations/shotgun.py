"""Bridges for converting generic shotgun data to formats digestible by other platforms."""
import os

from rsg_core_py.decorators import memoize
from rsg_clients import const
from rsg_clients.bridges import exceptions
from rsg_clients.bridges.integrations import abstractBridge
from rsg_clients.shotgun import const as shotgunConst
from rsg_clients.bugstar import const as bugstarConst


class BugstarBridge(abstractBridge.AbstractBridge):
    """Bridge for converting shotgun data to the matching bugstar data."""

    KEY = const.Clients.BUGSTAR  # Key associated with this class for mapping purposes

    def __init__(self, shotgun, bugstar):
        """Initializer.

        Args:
            shotgun (rsg_clients.shotgun.servers.BaseServer): shotgun server that data is coming from
            bugstar (rsg_clients.bugstar.servers.BaseServer): bugstar server that data is going to
        """
        super(BugstarBridge, self).__init__(source=shotgun, target=bugstar)

    @property
    def shotgun(self):
        """The shotgun server being accessed."""
        return self._source

    @property
    def bugstar(self):
        """The bugstar server being accessed."""
        return self._target

    def get(self, entity):
        """Gets the matching bugstar payload for this entity.

        This will not attempt to match the any data between two data types if a match is found.

        Args:
            entity (rsg_core.shotgun.entities.baseEntity): shotgun entity to convert

        Returns:
            rsg_clients.bugstar.payloads.abstractPayload
        """
        if entity is None:
            return

        elif self.shotgun.humanUser.isInstance(entity):
            return self.bugstar.user.getByName(entity.username)

        elif self.shotgun.group.isInstance(entity):
            team = self.bugstar.team.getByName(entity.name)
            return team[0] if team else None

        elif self.shotgun.project.isInstance(entity):
            if entity.bugstarId:
                return self.bugstar.project(entity.bugstarId)

        elif self.shotgun.task.isInstance(entity):
            if entity.bugNumber:
                return self.bugstar.bug(entity.bugNumber)

        elif self.shotgun.strand.isInstance(entity):
            if entity.subtype == self.shotgun.strand.subtypes.MISSION and entity.bugstarId:
                return self.bugstar.mission(entity.bugstarId)

        elif self.shotgun.goal.isInstance(entity):
            if entity.bugstarId:
                return self.bugstar.goal(entity.bugstarId)

        elif self.shotgun.location.isInstance(entity):
            if entity.subtype == self.shotgun.location.subtypes.GRID and entity.bugstarId:
                return self.bugstar.grid(entity.bugstarId)

        elif self.shotgun.component.isInstance(entity):
            if entity.bugstarId:
                return self.bugstar.component(entity.bugstarId)

    def convert(self, entity, create=False):
        """Gets the matching bugstar payload for this entity.

        This attempt to match the data between two data types.
        The data returned is not submitted to the server, that must be done explicitly.

        Args:
            entity (rsg_core.shotgun.entity.baseEntity.BaseEntity): entity to convert into a matching bugstar object

            create (bool, optional): create the object in bugstar if it doesn't already exist.

        Returns:
            rsg_clients.bugstar.payloads.abstractPayload
        """
        if self.shotgun.task.isInstance(entity):
            return self.convertTask(entity, create=create)

    def convertTask(self, task, create=False):
        """Converts a task to a bug.

        If a task already has a bug associated with it, it will update that bug.
        Otherwise this method will error out if the create flag is not set.

        Args:
            task (rsg_core.shotgun.entity.Task): task to convert into a bug

            create (bool, optional): creates a bug if the task does not already have one

        Returns:
            rsg_clients.bugstar.payload.bug.Bug
        """
        bug = self.getBug(task, force=create)

        # Required Fields
        bug.project = self.get(task.project)
        if not bug.project:
            raise exceptions.InvalidProject("{} does not have a matching Bugstar project".format(task.project.name))
        if task.owners:
            bug.developer = self.get(task.owners[0])
        bug.tester = self.get(task.qaOwner)
        if task.reviewer:
            bug.reviewer = self.get(task.reviewer[0])
        bug.tagList = [tag.name.replace("bugstar-", "") for tag in task.tags]
        bug.p4ChangeLists = [int(changelist.name) for changelist in task.bugstarChangelists]
        bug.summary = task.bugSummary or task.name

        # Not required fields for creation
        bug.description = task.description
        bug.workflow = self.convertWorkflow(task)
        bug.state = self.convertStatus(task)
        bug.isWorking = shotgunConst.Statuses.isWorkingStatus(task.status)
        bug.comments.extend(self.convertComments(task, bug))
        bug.ccList = self.convertCCList(task, bug)  # If task.cc has a user that doesn't exist in Bugstar,
        # it returns None and we don't want None in the list.
        bug.estimatedTime = task.bugstarEstimatedTime
        bug.dueDate = task.dueDate
        bug.mission = self.get(task.mission)
        bug.component = self.get(task.bugstarComponent)
        bug.grid = self.get(task.bugstarGrid)

        # TODO: Add support for connecting goals, this is currently unsupported by the rest api
        if task.deadline:
            bug.deadlineList = self.convertList(
                [task.deadline], bug.deadlineList, convert=self.bugstar.deadline.getByName
            )
        bug.attachmentList = self.convertAttachments(task, bug)
        return bug

    def getBug(self, task, force=True):
        """Gets the bug associated with the given task.

        Arguments:
            task (rsg_core.shotgun.entities.task.Task): task whose bug we are trying to get

        Returns:
            rsg_clients.bugstar.payloads.bug.Bug
        """
        if not task.subtype == self.shotgun.task.subtypes.BUG:
            raise Exception("This Task does not represent a bug, it represents {} instead.".format(task.subtype))

        if not task.bugstarId:
            if not force:
                project = task.project
                projectName = "Global"
                if project is not None:
                    projectName = task.project.name

                raise Exception(
                    "Task {} {} from Project {} does not have an associated bug".format(task.id, task.name, projectName)
                )
            return self.bugstar.bug()
        return self.bugstar.bug(task.bugNumber)

    def _getShotgunUser(self, user):
        """Gets the shotgun equivalent of a bugstar user or team.

        This is required as Shotgun does not have every bugstar user and we must only edit the contents of a bug
        with users that are valid on Shotgun and leave users not in Shotgun untouched.

        Args:
            user (rsg_clients.bugstar.payload.user.User or
                  or rsg_clients.bugstar.payload.team.Team): the user or group get from Shotgun

        Returns:
            rockstar.shotgun.clients.shotgun.humanUser.HumanUser or rockstar.shotgun.clients.shotgun.group.Group
        """
        if self.bugstar.user.isInstance(user):
            return self.shotgun.humanUser.getByUsername(user.username)
        elif self.bugstar.team.isInstance(user):
            return self.shotgun.group.getByName(user.name)

    def convertWorkflow(self, task):
        """Converts the shotgun workflow value into one that bugstar can digest.

        Args:
            task (rsg_core.shotgun.entities.task.Task): task whose workflow needs converting

        Returns:
            str
        """
        if task.workflow:
            return bugstarConst.Workflows.getWorkflow(task.workflow)
        return bugstarConst.Workflows.SIMPLE

    def convertStatus(self, task):
        """Converts the shotgun status value into one that bugstar can digest.

        Args:
            task (rsg_core.shotgun.entities.task.Task): task whose status needs converting

        Returns:
            str
        """
        status = task.status
        if status == shotgunConst.Statuses.READY_TO_START:
            status = shotgunConst.Statuses.DEV_FIXING  # Ready to start is a SG variant of the dev fixing status in B*
        statusFullName = shotgunConst.Statuses.getName(status)
        return bugstarConst.States.getStateFromFullName(statusFullName)

    def convertComments(self, task, bug):
        """Converts the shotgun replies into bugstar comments.

        As bugstar comments are uneditable, we can only add comments and not remove.
        Thus only NEW comments are returned by this method.

        Args:
            task (rsg_core.shotgun.entities.task.Task): task whose comments we are converting
            bug (rsg_clients.bugstar.payloads.bug.Bug): list of comments the bug already has

        Returns:
            list
        """
        count = len(bug.comments)
        updatedReplies = []
        newComments = []
        if task.bugNote:
            for reply in task.bugNote.replies:
                if not reply.position and not reply.bugstarCreatedOn:
                    count += 1
                    reply.position = count
                    updatedReplies.append(reply)
                    author = self.get(reply.author)
                    newComments.append(self.bugstar.bug.comment(reply.body, author))
        # update the positioning of the replies on Shotgun
        if updatedReplies:
            self.shotgun.batch(update=updatedReplies)
        return newComments

    def convertCCList(self, task, bug):
        """Converts the ccList from task to one that the bug can accept.

        It makes sure that users whom are missing from Shotgun are not modified

        Args:
            task (rsg_clients.shotgun.entities.task.Task): task whose ccList we are copying to bugstar
            bug (rsg_clients.bugstar.payloads.bug.Bug): bug to copy the ccList to.
        """
        # Get a list of all the bugstar users not on Shotgun
        ccList = [cc for cc in bug.ccList if self._getShotgunUser(cc) is None]
        # Add back the users that are on Shotgun
        for cc in task.cc:
            cc = self.get(cc)
            if cc not in ccList:
                ccList.append(cc)
        return ccList

    def convertList(self, shotgunList, bugstarList, additive=True, convert=None):
        """Converts a list of bugstar items into their correct shotgun values.

        Args:
            shotgunList (list): shotgun values to convert
            bugstarList (list): the current bugstar values

            additive (bool, optional): doesn't remove missing differences, only appends.
            convert (func, optional): function for converting the value. Defaults to the main convert method

        Returns:
            list
        """
        convert = convert or self.convert
        if not additive:
            return [convert(item) for item in shotgunList]

        newList = list(bugstarList)
        for item in shotgunList:
            bugstarItem = convert(item)
            if bugstarItem is not None and bugstarItem not in newList:
                newList.append(bugstarItem)

        return newList

    def convertAttachments(self, task, bug):
        """Convert shotgun versions to bugstar attachments.

        Args:
            task (rsg_clients.shotgun.entities.task.Task): Shotgun task entity
            bug (rsg_clients.bugstar.payloads.bug.Bug): Bugstar bug we are updating
        """
        self.debug("Matching attachment from shotgun task {} to attachments in bugstar bug {}".format(task.id, bug.id))
        # get attachFile from task
        taskAttachments = {version.bugstarId: version for version in task.versions}
        taskAttachmentIds = set(taskAttachments.keys())

        bugAttachments = {attachment.id: attachment for attachment in bug.attachmentList}
        bugAttachmentIds = set(bugAttachments.keys())
        # Compare existing bug attachments with task attachments
        removeAttachments = bugAttachmentIds - taskAttachmentIds
        addAttachments = taskAttachmentIds - bugAttachmentIds

        attachments = list(bug.attachmentList)
        for attachmentId in addAttachments:
            # making it idiot proof, in case someone makes a version that doesn't have a file uploaded, there is nothing to upload to B*.
            attachment = taskAttachments[attachmentId]
            attachmentPath = (
                attachment.downloadAttachment(bugstarConst.Paths.ATTACHMENTS_DIRECTORY)
                if attachment.attachment
                else None
            )
            if attachmentPath and os.path.exists(attachmentPath):
                # add the attachment to the bug
                attachment = self.bugstar.attachment()
                attachment.setFile(attachmentPath)

                attachment.create()
                attachments.append(attachment)

                # update bugstarId field in shotgun as now it is uploaded to bugstar.
                taskAttachments[attachmentId].bugstarId = attachment.id
                taskAttachments[attachmentId].updateFields(shotgunConst.Fields.BUGSTAR_ID)

                # Clear the junk from the system
                os.remove(attachmentPath)

        for attachmentId in removeAttachments:
            bugstarAttachment = self.bugstar.attachment(attachmentId)
            attachments.remove(bugstarAttachment)

        return attachments


@memoize.memoize
def getByType(key):
    """Gets the bridge based on the client it needs to go.

    Args:
        key (str): the type of bridge to go to

    Returns:
        rsg_clients.bridges.adapters.abstractBridge.AbstractBridge
    """
    for subclass in abstractBridge.AbstractBridge.__subclasses__():
        if subclass.__module__ == __name__ and subclass.KEY == key:
            return subclass
