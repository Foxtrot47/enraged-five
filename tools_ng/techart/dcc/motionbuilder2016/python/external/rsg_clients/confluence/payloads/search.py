from rsg_clients.confluence import const
from rsg_clients.confluence.payloads import basePayload


class Search(basePayload.BasePayload):

    _COMMAND = const.Endpoints.SEARCH

    _INSTANCES = {}

    def _setup(self, index=None, data=None, server=None):
        """Setups the base properties used by this object.

        Args:
            index (int): id of the object from the server
            data (dict): data that represents this object
            server (rsg_core_py.abstracts.abstractConnection.AbstractConnection): the local server to set for this object
        """
        super(Search, self)._setup(index, data, server)
        self._exists = None

    def _parse(self, data):
        """Parses Search style dictionaries.

        Args:
            data (dict): dictionary generated from the confluence rest api
        """
        values = {}
        for key, value in data.items():
            values[key] = value
        self._fields.update(values)

    def refresh(self, command=None, data=None):
        """Performs the confluence query required to get the data to fill this object with.

        Args:
            command (str): command to run or the id of the payload type we want to grab from confluence
            data (dict): data from confluence to refresh the instance with
        """
        if data:
            dictionary = data
        else:
            dictionary = self._getData(self.server, command or self.id)

        self.parse(dictionary.get(self.className(), dictionary))
        self._removeStalePayloads(instance=self)

    @classmethod
    def builder(cls):
        """The class for the builder that is used to build instances of this class.

        Returns:
            rsg_clients.confluence.basePayload.BaseBuilder
        """
        return Builder

    def toDict(self):
        """The representation of this payload as a dictionary that can be used to print out as string or post as payload.

        Returns:
            dict
        """
        values = {}
        # Get mutable changes
        edits = dict(self._edits)
        edits.update(self._mutables)
        # Get edits on the local entity
        fields = dict(self._fields)
        fields.update(edits)

        return values


class Builder(basePayload.BaseBuilder):
    @classmethod
    def cls(cls):
        """Class for the object the builder creates.

        Returns:
            rsg_clients.confluence.payloads.search.Search
        """
        return Search

    def getContentByTitle(self, title, parentId=None, space=None, contentType="page", isExact=True):
        """Gets the Search  based on the title provided.

        Args:
            title (str): title which to look for
            parentId (int): Id of the parent page
            space (str): space where the page belongs to
            contentType (str): What kind of content are we searching for.
                                Ex: space, user, page, blogpost, comment, attachment. Default page
            isExact (bool): Did we type in the exact title

        Returns:
            rsg_clients.confluence.payloads.search.Search
        """
        sections = [const.Endpoints.SEARCH]

        if isExact:
            cqlExpression = '{}="{}"'.format(const.Fields.TITLE, title)
        else:
            cqlExpression = '{}~"{}"'.format(const.Fields.TITLE, title)

        if space:
            cqlExpression += " and space={}".format(space)

        if parentId:
            cqlExpression += " and parent={}".format(parentId)

        cqlExpression += " and type={}".format(contentType)

        url = self._server.buildUrl(sections, fields={const.Command.CQL: cqlExpression})

        returnData = self._server.get(url)
        returnData = returnData["results"]
        if returnData:
            contents = []
            for result in returnData:
                # IF it has content that can be attachment or the page
                if const.Fields.CONTENT in result:
                    if result[const.Fields.CONTENT][const.Fields.TYPE] == const.Endpoints.ATTACHMENT:
                        content = self._server.attachment(result[const.Fields.CONTENT][const.Fields.ID])
                    else:
                        content = self._server.page(int(result[const.Fields.CONTENT][const.Fields.ID]))
                if const.Fields.SPACE in result:
                    content = self._server.space.getByKey(result[const.Fields.SPACE][const.Fields.KEY])

                contents.append(content)

            return contents
