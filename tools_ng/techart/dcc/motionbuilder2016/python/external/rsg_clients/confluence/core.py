"""Core connection logic for getting data from confluence."""
import json
import base64
import logging
import requests

from rsg_core import accounts, conversions
from rsg_clients.confluence import const, exceptions, factory
from rsg_clients import const as clientConst
from rsg_clients.internals import baseConnection
from rsg_core_py.exceptions import types


LOGGER = logging.getLogger(__name__)


class Confluence(baseConnection.BaseConnection):
    def __init__(self, server):
        """Initializer.

        Args:
            server (str): name of the server we are connecting to.
        """
        super(Confluence, self).__init__(server)
        self._account = accounts.CurrentUser()  # Set the current user as the default account

    def authenticate(self, password, username=None):
        """Authenticates the user against the server.

        Args:
            password (str): password encoded in base64 for the account being accessed.

            username (str, optional): the name of the user to authenticate as. Defaults to the current account set.
        """
        username = username or self.account.name
        if not self._authenticated:
            token = base64.b64encode("{}:{}".format(username, base64.b64decode(password)))
            self.session.headers["authorization"] = "Basic {}".format(token)
            self.session.headers[
                "X-Atlassian-Token"
            ] = "no-check"  # Without this uploading of the attachments won't work.
            # We are adding a / at the end to get a nice error
        result = self.session.get(self.restUrl, headers={"Content-type": clientConst.ContentType.JSON}, verify=False)
        # We are expecting a 404 error as this is a badly built url (but still works for authenticating)
        # 401 Error code means you don't have permissions to access the endpoint.
        # Due to the anonymous access supported by confluence, this is the generic error returned with bad credentials
        if result.status_code != 404:
            self._authenticated = False
            raise types.AuthenticationError()
        self._authenticated = True

    @property
    def session(self):
        """The session that the current user is logged in for.

        Returns:
            requests.Session
        """
        if not self._session:
            self._session = requests.Session()
            self._session.verify = False
            # We add a bad credentials on purpose to trigger the login logic as the confluence api gives anonymous read
            # access and we want to be able to confirm that the credentials being used are correct.
            self._session.headers["authorization"] = "Basic None"
        return self._session

    @property
    def restUrl(self):
        """Convenience method for getting the server url with the rest endpoint."""
        return const.Servers.getRestUrl(self._server)

    def raiseAuthenticationError(self, url, response):
        """Raises an authentication error if the response returns an html page with said error.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server

        Raises:
            NotAuthenticated
        """
        if "<!doctype html>" in response.text and response.status_code == 401:
            raise exceptions.NotAuthenticated(
                "Basic Authentication Failure - Reason : AUTHENTICATED_FAILED", url=url, errorCode=response.status_code
            )

    def raiseException(self, url, response, data):
        """Raises an exception if the response returns a non-200 status code.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server
            data (dict): data extrapolated from the response as a dict

        Raises:
            ConfluenceException
        """
        exceptionClass = exceptions.ConfluenceException.getByErrorCode(response.status_code)
        if 300 <= response.status_code and not exceptionClass:
            exceptionClass = exceptions.ConfluenceException

        if exceptionClass is not None:
            # Get the correct error message, based on the response it may require different keys
            # errorDict = data.get("ResponseHeader", data).get("Errors", {})
            # errorMessage = errorDict.get("messages", errorDict.get("#text", [""]))[0]
            errorMessage = data.get("message", [])
            if response.status_code == exceptions.ConfluenceServerError.ERROR_CODE:  # This the generic 500 error code
                if exceptions.ServerUnavailable.REGEX.search(errorMessage):
                    raise exceptions.ServerUnavailable(server=self.type, message=errorMessage, url=url)

            raise exceptionClass(errorMessage, url=url, errorCode=response.status_code)

    @property
    def client(self):
        """ The name of the server."""
        return clientConst.Clients.CONFLUENCE

    def _getBuilder(self, payloadType):
        """Gets the builder for the given payload.

        Args:
            payloadType (str): the type of the object

        Returns:
            rsg_clients.confluence.payload.basePayload.BaseBuilder
        """
        builder = self._builders.get(payloadType, None)
        if not builder:
            cls = factory.resolvePayloadClass(payloadType)
            builderCls = cls.builder()
            builder = builderCls(self)
            self._builders[payloadType] = builder
        return builder

    @property
    def page(self):
        """Page builder."""
        return self._getBuilder(const.Endpoints.CONTENT)

    @property
    def space(self):
        """Space builder."""
        return self._getBuilder(const.Endpoints.SPACE)

    @property
    def search(self):
        """Search builder."""
        return self._getBuilder(const.Endpoints.SEARCH)

    @property
    def label(self):
        """Label builder."""
        return self._getBuilder(const.Endpoints.LABEL)

    @property
    def attachment(self):
        """Attachment builder."""
        return self._getBuilder(const.Endpoints.ATTACHMENT)
