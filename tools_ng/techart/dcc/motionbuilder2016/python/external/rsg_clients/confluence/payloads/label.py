from rsg_clients.confluence import const
from rsg_clients.confluence.payloads import basePayload

class Label(basePayload.BasePayload):
    _COMMAND = const.Endpoints.LABEL
    _INSTANCES = {}

    def _setup(self, index=None, data=None, server=None):
        """Setups the base properties used by this object.

        Args:
            index (int): id of the object from the server
            data (dict): data that represents this object
            server (rsg_core_py.abstracts.abstractConnection.AbstractConnection): the local server to set for this object
        """
        super(Label, self)._setup(index, data, server)
        self._exists = None

    def _parse(self, data):
        """Parses Attachment style dictionary.

        Args:
            data (dict): dictionary generated from the confluence rest api
        """
        values = {}
        for key, value in data.items():
            values[key] = value
        self._fields.update(values)

    def toDict(self):
        """The representation of this payload as a dictionary that can be used to print out as string or post as payload.

        Returns:
            dict
        """
        fields = {}
        for key, value in self._fields.items():
            if key == const.Fields.ID and value is None:
                continue
            fields[key] = value
        return fields

    @classmethod
    def builder(cls):
        return Builder

    @property
    def id(self):
        """"""
        return self._id

    @property
    def name(self):
        """Name is a readable Label property."""
        return self._getattr(const.Fields.NAME)

    @name.setter
    def name(self, value):
        """Name is writeable Label property.

        Args:
            value (str):text of the label
        """
        self._setattr(const.Fields.NAME, value)

    @property
    def prefix(self):
        """Prefix is readable Label property."""
        return self._getattr(const.Fields.PREFIX)

    @prefix.setter
    def prefix(self, value):
        """Prefix is writable label property."""
        self._setattr(const.Fields.PREFIX, value)


class Builder(basePayload.BaseBuilder):
    @classmethod
    def cls(cls):
        return Label

    def getByPageId(self, pageId):
        """Gets the page based on the title provided and the space it belongs to.

        Args:
            pageId (int): id of the page whose label we are looking for..

        Returns:
            list of rsg_clients.confluence.payloads.label.Label
        """
        sections = [
            const.Endpoints.CONTENT,
            pageId,
            const.Endpoints.LABEL
        ]

        url = self._server.buildUrl(sections)
        returnData = self._server.get(url)
        values = []
        for item in returnData["results"]:
            values.append(Label(self._server, index=item[const.Fields.ID], data=item))
        return values
