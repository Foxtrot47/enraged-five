from rsg_clients.confluence.payloads import *  # The init has been modified to import all modules in its directory when import * is used
from rsg_clients.confluence.payloads import basePayload


def resolvePayloadClass(payloadCommand):
    """
    Resolves which class to get based on the given payload name

    Arguments:
        payloadCommand (str): the type of entity whose custom class we want to get

    Returns:
        class
    """
    subclass = basePayload.BasePayload.MAPPINGS.get(payloadCommand, None)
    if subclass is not None:
        return subclass
    raise ValueError("No class is available for {} command".format(payloadCommand))
