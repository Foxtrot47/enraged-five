import os
import json
import mimetypes

from rsg_clients import const as clientsConst
from rsg_clients.confluence import const
from rsg_clients.confluence.payloads import basePayload


class Attachment(basePayload.BasePayload):
    """ Class that presents confluence space object."""

    _COMMAND = const.Endpoints.ATTACHMENT
    _INSTANCES = {}

    _METADATA = [const.Fields.COMMENT, const.Fields.MEDIA_TYPE, const.Fields.LABELS]

    _EXPANDABLES = [const.Fields.VERSION, const.Fields.CONTAINER]

    def _setup(self, index=None, data=None, server=None):
        """Setups the base properties used by this object.

        Args:
            index (int): id of the object from the server
            data (dict): data that represents this object
            server (rsg_core_py.abstracts.abstractConnection.AbstractConnection): the local server to set for this object
        """
        super(Attachment, self)._setup(index, data, server)
        self._exists = None

    def _parse(self, data):
        """Parses Attachment style dictionary.

        Args:
            data (dict): dictionary generated from the confluence rest api
        """
        values = {}
        for key, value in data.items():
            if key in const.Fields.METADATA:
                metaValue = value
                for metaKey in metaValue:
                    if metaKey == const.Fields.LABELS:
                        labels = []
                        for labelItem in metaValue[metaKey][const.Fields.RESULTS]:
                            labels.append(self.server.label(data=labelItem))
                        values[metaKey] = labels
                    else:
                        values[metaKey] = metaValue[metaKey]
            else:
                values[key] = value
        self._fields.update(values)

    def refresh(self, command=None, data=None):
        """Performs the confluence query required to get the data to fill this object with.

        Args:
            command (str): command to run or the id of the payload type we want to grab from confluence
            data (dict): data from confluence to refresh the instance with
        """
        if data:
            dictionary = data
        else:
            dictionary = self._getData(self.server, self.id, command=const.Endpoints.CONTENT)

        self.parse(dictionary.get(self.className(), dictionary))
        self._removeStalePayloads(instance=self)

    def update(self):
        """Updates the attachement labels.

        Returns:
            dict: result of the put command.
        """
        sections = [
            const.Endpoints.CONTENT,
            self.id,
        ]

        putData = self.toDict()

        putPayload = json.dumps(putData)
        putUrl = self._server.buildUrl(sections)
        pageUpdateResults = self._server.put(putUrl, payload=putPayload, contentType=clientsConst.ContentType.JSON)

        return pageUpdateResults

    def _getLabelData(self, fields=None, returnRaw=False):
        """Queries the labels information from the confluence server.
        This is a separate function as labels have their own endpoint.

        Args:
            fields (dictionary): the field name and its value to include in the url for the get request
            returnRaw (bool): return the data from the request as is (usually a string)

        Returns:
            dict payload from the server
        """
        sections = [
            const.Endpoints.CONTENT,
            self.id,
            const.Endpoints.LABEL,
        ]

        fields = fields or {}
        url = self.server.buildUrl(sections, fields=fields)
        return self.server.get(url, returnRaw=returnRaw)

    def toDict(self):
        """The representation of this payload as a dictionary that can be used to print out as string or post as payload.

        Returns:
            dict
        """
        values = {}
        values[const.Fields.METADATA] = {}
        # Get mutable changes
        edits = dict(self._edits)
        edits.update(self._mutables)
        # Get edits on the local entity
        fields = dict(self._fields)
        fields.update(edits)

        for key, value in fields.items():
            if key in self._METADATA:
                if key == const.Fields.LABELS:
                    values[const.Fields.METADATA][key] = {}
                    labels = [label.toDict() for label in fields[key]]
                    values[const.Fields.METADATA][key][const.Fields.RESULTS] = labels
                else:
                    values[const.Fields.METADATA][key] = value
            # We are skipping the content as that is used only for the upload of the files from the perforce without syncing them.
            elif key == const.Fields.CONTENT:
                continue
            else:
                values[key] = value
        return values

    @classmethod
    def builder(cls):
        """The class for the builder that is used to build instances of this class.

        Returns:
            rsg_clients.confluence.basePayload.BaseBuilder
        """
        return Builder

    @property
    def id(self):
        """Id is readable attachment property."""
        return self._id

    @property
    def type(self):
        """Type is readable attachment property."""
        return self._getattr(const.Fields.TYPE)

    @property
    def status(self):
        """Status is readable attachment property."""
        return self._getattr(const.Fields.STATUS)

    @property
    def title(self):
        """Title is readable attachment property."""
        return self._getattr(const.Fields.TITLE)

    @property
    def labels(self):
        """Labels is readable attachment property."""
        if self._id and const.Fields.LABELS not in self._fields:
            data = self._getLabelData()
            values = []
            if data:
                for labelItem in data["results"]:
                    values.append(self.server.label(index=labelItem[const.Fields.ID], data=labelItem))
            self._fields[const.Fields.LABELS] = values
        return self._getmutable(const.Fields.LABELS, mutableType=list, itemType=self.server.label)

    @property
    def comment(self):
        """Comment is readable attachment property."""
        return self._getattr(const.Fields.COMMENT)

    @comment.setter
    def comment(self, value):
        """Title is writeable attachment property.

        Args:
            value (str): comment to add on the attachment
        """
        self._setattr(const.Fields.COMMENT, value)

    @property
    def filename(self):
        """Filename is readable attachment property."""
        return self._getattr(const.Fields.FILENAME)

    @filename.setter
    def filename(self, value):
        """Filename is writeable attachment property.

        Args:
            value (str): Path to the file that we want to upload.
        """
        self._setattr(const.Fields.FILENAME, value)

    @property
    def content(self):
        """Content is readable attachment property."""
        return self._getattr(const.Fields.CONTENT)

    @content.setter
    def content(self, value):
        """Content is a writable attachment property.

        Args:
            value (bytes): Bytes presentation of the file
        """
        self._setattr(const.Fields.CONTENT, value)


class Builder(basePayload.BaseBuilder):
    @classmethod
    def cls(cls):
        """Class for the object the builder creates.

        Returns:
            rsg_clients.confluence.payloads.attachment.Attachment
        """
        return Attachment

    def label(self, labelName):
        """Creates a Label object.

        Args:
           labelName (str): text to show on the label

        Returns:
           rsg_clients.confluence.payloads.label.Label
        """
        label = self._server.label()
        label.name = labelName
        label.prefix = "global"
        return label

    def getByPageId(self, pageId):
        """Gets all the attachments based on the page id.

        Args:
            pageId (int): id of the page which attachments we want to query

        Returns:
            list of rsg_clients.confluence.payloads.attachment.Attachments
        """
        sections = [const.Endpoints.CONTENT, pageId, const.Endpoints.CHILD, const.Endpoints.ATTACHMENT]

        url = self._server.buildUrl(sections, fields={const.Command.EXPAND: ",".join(Attachment._EXPANDABLES)})
        returnData = self._server.get(url)
        values = []
        for item in returnData["results"]:
            values.append(Attachment(self._server, index=item[const.Fields.ID], data=item))
        return values
