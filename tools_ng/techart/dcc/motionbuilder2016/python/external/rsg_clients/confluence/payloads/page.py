import mimetypes
import json
import os

from rsg_clients import const as clientsConst
from rsg_clients.confluence import const, exceptions
from rsg_clients.confluence.payloads import basePayload


class Body(basePayload.BasePayload):
    """Class that presents the body structure of the page."""

    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)

    def __init__(self, confluence, html, representation):
        if not isinstance(html, str):
            html = str(html)

        self.server = confluence
        self._value = html
        self._representation = representation

    @property
    def bodyValue(self):
        """BodyValue is a reading property of the body.
        It is an HTML presentation of the page."""
        return self._value

    @bodyValue.setter
    def bodyValue(self, value):
        """Setter for the page look aka HTML."""
        if isinstance(value, str) and value.startswith("<"):
            self._value = value
        else:
            raise Exception("Body of the page has to be of type string and start with the tag.")

    @property
    def representation(self):
        """Representation is a reading property of the body."""
        return self._representation

    def toDict(self):
        """Converts the object into dictionary.

        Returns:
            dict
        """
        values = {}
        values[const.Fields.VALUE] = self.bodyValue
        values[const.Fields.REPRESENTATION] = self.representation
        return values


class Page(basePayload.BasePayload):
    """ Class that presents confluence page object."""

    _COMMAND = const.Endpoints.CONTENT
    _INSTANCES = {}

    # Expandables are fields that require to be specified with expand field in ReST request.
    # Here is the list of the once we care about for now. Upor GET request they are combined in a expand string.
    _EXPANDABLES = [
        "{}.{}".format(const.Fields.BODY, const.Fields.STORAGE),
        const.Fields.CHILDREN,
        const.Fields.VERSION,
        const.Fields.SPACE,
        const.Fields.ANCESTORS,
    ]

    REQUIRED_CREATE_PROPERTIES = [
        const.Fields.BODY,
        const.Fields.TITLE,
        const.Fields.TYPE,
        const.Fields.SPACE,
    ]

    def _setup(self, index=None, data=None, server=None):
        """Setups the base properties used by this object.

        Args:
            index (int): id of the object from the server
            data (dict): data that represents this object
            server (rsg_core_py.abstracts.abstractConnection.AbstractConnection): the local server to set for this object
        """
        super(Page, self)._setup(index, data, server)
        self._exists = None

    def _parse(self, data):
        """Parses Page style dictionaries.

        Args:
            data (dict): dictionary generated from the confluence rest api
        """
        values = {}
        for key, value in data.items():
            if key == const.Fields.SPACE:
                value = self.server.space.getByKey(value[const.Fields.KEY])

            elif key == const.Fields.BODY:
                value = Body(
                    self.server,
                    value[const.Fields.STORAGE][const.Fields.VALUE],
                    value[const.Fields.STORAGE][const.Fields.REPRESENTATION],
                )

            elif key == const.Fields.ANCESTORS:
                ancestors = [Page(self.server, index=ancestor[const.Fields.ID], data=ancestor) for ancestor in value]
                value = ancestors

            values[key] = value
        self._fields.update(values)

    def refresh(self, command=None, data=None):
        """Performs the confluence query required to get the data to fill this object with.

        Args:
            command (str): command to run or the id of the payload type we want to grab from confluence
            data (dict): data from confluence to refresh the instance with
        """
        if data is None:
            data = self._getData(
                self.server, command or self.id, fields={const.Command.EXPAND: ",".join(Page._EXPANDABLES)}
            )

        self.parse(data.get(self.className(), data))
        self._removeStalePayloads(instance=self)

    def toDict(self):
        """The representation of this payload as a dictionary that can be used to print out as string or post as payload.

        Returns:
            dict
        """
        # Get mutable changes
        edits = dict(self._edits)
        edits.update(self._mutables)
        # Get edits on the local entity
        fields = dict(self._fields)
        fields.update(edits)

        values = {}
        for key in sorted(fields.keys()):
            uneditedValue = self._fields.get(key, None)
            value = edits.get(key, uneditedValue)

            if key == const.Fields.BODY:
                value = {const.Fields.STORAGE: value.toDict()}

            elif key in [const.Fields.SPACE]:
                value = value.toDict()

            # Custom fields from different endpoints
            elif key in [const.Fields.ATTACHMENTS, const.Fields.LABELS]:
                valueList = []
                for valueItem in value:
                    valueList.append(valueItem.toDict())
                value = valueList

            elif key == const.Fields.ANCESTORS:
                valueList = []
                # Need to reverse the list so it will not change the parent of the page once the update is called
                value.reverse()
                for valueItem in value:
                    valueList.append(valueItem.toDict())
                value = valueList

            values[key] = value

        return values

    def _getLabelData(self, fields=None, returnRaw=False):
        """Queries the labels information from the confluence server.
        This is a separate function as labels have their own endpoint.

        Args:
            fields (dictionary): the field name and its value to include in the url for the get request
            returnRaw (bool): return the data from the request as is (usually a string)

        Returns:
            dict payload from the server
        """
        sections = [
            const.Endpoints.CONTENT,
            self.id,
            const.Endpoints.LABEL,
        ]

        fields = fields or {}
        url = self.server.buildUrl(sections, fields=fields)
        return self.server.get(url, returnRaw=returnRaw)

    def _getAttachmentData(self, fields=None, returnRaw=False):
        """Queries the attachments information from the confluence server.
        This is a separate function as attachments have their own endpoint.

        Args:
            fields (dictionary): the field name and its value to include in the url for the get request
            returnRaw (bool): return the data from the request as is (usually a string)

        Returns:
            dict payload from the server
        """
        sections = [
            const.Endpoints.CONTENT,
            self.id,
            const.Endpoints.CHILD,
            const.Endpoints.ATTACHMENT,
        ]

        fields = fields or {}
        url = self.server.buildUrl(sections, fields=fields)
        return self.server.get(url, returnRaw=returnRaw)

    def create(self):
        """Creates a new page on the confluence server based on the information in the object.
        It also updates labels and attachments to match this object.

        Returns:
            dict - respond from the server from creation of the page.
        """
        self._validate(self.REQUIRED_CREATE_PROPERTIES)

        postUrl = self._server.buildUrl([const.Endpoints.CONTENT])
        dictionary = self._server.post(postUrl, payload=self, contentType=clientsConst.ContentType.JSON)

        self.parse(dictionary)
        self._id = self._fields.get(const.Fields.ID, None)

        # Update labels
        self._updateLabels(add=self._getmutable(const.Fields.LABELS, mutableType=list))
        # Update attachments
        self._updateAttachments(add=self._getmutable(const.Fields.ATTACHMENTS, mutableType=list))

        self._cache()

        return dictionary

    def update(self, minorEdit=False):
        """Updates the page with the information from the object.
        After updating the page it updates the labels and attachments.

        Args:
            minorEdit (bool): specify if the change on the page is minor or not.

        Returns:
            dict - respond from the server from updating of the page.
        """
        sections = [
            const.Endpoints.CONTENT,
            self.id,
        ]

        putData = self.toDict()

        # These 2 need to be updated from the current payload
        putData[const.Fields.VERSION][const.Fields.NUMBER] = int(putData[const.Fields.VERSION][const.Fields.NUMBER]) + 1
        putData[const.Fields.VERSION][const.Fields.MINOR_EDIT] = str(minorEdit).lower()

        putPayload = json.dumps(putData)
        putUrl = self._server.buildUrl(sections)
        pageUpdateResults = self._server.put(putUrl, payload=putPayload, contentType=clientsConst.ContentType.JSON)

        differences = self._getMutableDifferences()
        # Update labels
        if const.Fields.LABELS in differences:
            self._updateLabels(
                add=differences[const.Fields.LABELS].append, remove=differences[const.Fields.LABELS].remove
            )
        # Update attachments
        if const.Fields.ATTACHMENTS in differences:
            self._updateAttachments(
                add=differences[const.Fields.ATTACHMENTS].append, remove=differences[const.Fields.ATTACHMENTS].remove
            )

        return pageUpdateResults

    def _uploadAttachment(self, url, attachment, title, data=None):
        """Upload the attachment to the web.

        Args:
            url (str):url to call for creation
            attachment (rsg_clients.confluence.payload.attachment.Attachment):
            title (str): name of the file with the extension
            data (dict): payload to be passed

        Returns:
            dict: response from the server.
        """
        contentType, encoding = mimetypes.guess_type(attachment.filename)
        if contentType is None:
            contentType = "multipart/form-data"

        if attachment.filename.startswith("//"):
            # If the file is a perforce path we need to get the file content
            imgContent = attachment.content
        else:
            imgContent = open(attachment.filename, "rb")

        # 1st is the name of the file, second is the file and content type at the end.
        fileData = {"file": (title, imgContent, contentType)}
        # We are calling the session straight because the post function is requesting the contentType,
        # and in order to update the different kinds of files, we need to specify contentType in the files data.
        result = self.server.session.post(url, data=data, files=fileData)
        return result

    def _updateLabels(self, add=None, remove=None):
        """Adds and removes the labels from the page.

        Args:
            add (list of rsg_clients.confluence.payloads.label.Label): list of labels that should be added to the page on the server
            remove (list of rsg_clients.confluence.payloads.label.Label): list of labels that should be removed from the page on the server.
        """
        # Add missing labels
        sections = [const.Endpoints.CONTENT, self.id, const.Endpoints.LABEL]
        postUrl = self.server.buildUrl(sections)

        if add:
            lbl2Add = [labelItem.toDict() for labelItem in add]
            self.server.post(postUrl, payload=json.dumps(lbl2Add), contentType=clientsConst.ContentType.JSON)

        if remove:
            for labelItem in remove:
                deleteUrl = self.server.buildUrl(sections, fields={const.Fields.NAME: labelItem.name})
                self.server.delete(deleteUrl, payload=None, contentType=clientsConst.ContentType.JSON)

    def _updateAttachments(self, add=None, remove=None):
        """Adds and removes the labels from the page.

        Args:
            add (list of rsg_clients.confluence.payloads.label.Label): list of labels that should be added to the page on the server
            remove (list of rsg_clients.confluence.payloads.label.Label): list of labels that should be removed from the page on the server.
        """
        sections = [
            const.Endpoints.CONTENT,
        ]
        # Removing the attachments
        if remove:
            for attach in remove:
                sections.append(attach.id)
                deleteUrl = self.server.buildUrl(sections)
                self.server.session.delete(deleteUrl)

        sections.extend([self.id, const.Endpoints.CHILD, const.Endpoints.ATTACHMENT])

        # Adding the attachments
        if add:
            postUrl = self.server.buildUrl(sections)
            for attach in add:
                self._uploadAttachment(postUrl, attach, os.path.basename(attach.filename), data=attach.toDict())

    def uploadAttachVersion(self, attachmentPath):
        """Removes the previous version of the attachment and uploads a new version.

        Args:
            attachmentPath (str): path to the file we want to upload
        """
        removeAttachments = []
        for attachment in self.attachments:
            if attachment.title == os.path.basename(attachmentPath):
                removeAttachments.append(attachment)

        attachment = self.server.attachment()
        attachment.filename = attachmentPath

        self._updateAttachments(add=[attachment], remove=removeAttachments)

    def delete(self):
        """Deletes the page from the server.

        Returns:
            dict - respond from the server from deleting the page.
        """
        sections = [const.Endpoints.CONTENT, self.id]
        deleteUrl = self.server.buildUrl(sections)
        dictionary = self.server.delete(deleteUrl, payload=self, contentType=clientsConst.ContentType.JSON)
        return dictionary

    @classmethod
    def builder(cls):
        """The class for the builder that is used to build instances of this class.

        Returns:
            rsg_clients.confluence.basePayload.BaseBuilder
        """
        return Builder

    @property
    def id(self):
        """Id is readable page property."""
        return self._id

    @property
    def pageType(self):
        """PageType is readable page property."""
        return self._getattr(const.Fields.TYPE)

    @pageType.setter
    def pageType(self, value):
        """PageType is writeable page property.

        Args:
            value (str): type of the page.
        """
        self._setattr(const.Fields.TYPE, value)

    @property
    def status(self):
        """ Status is readable Page property."""
        return self._getattr(const.Fields.STATUS)

    @property
    def title(self):
        """Title is readable Page property."""
        return self._getattr(const.Fields.TITLE)

    @title.setter
    def title(self, value):
        """Title is writeable Page property.

        Args:
            value (str):title of the page
        """
        self._setattr(const.Fields.TITLE, value)

    @property
    def space(self):
        """Space is readable Page property."""
        return self._getattr(const.Fields.SPACE)

    @space.setter
    def space(self, value):
        """Space is writeable Page property.

        Args:
            value (str or rsg_clients.confluence.payloads.space.Space): Space to which we want to assign the page to.
        """
        if isinstance(value, str):
            value = self.server.space.getByKey(value)
        self._setattr(const.Fields.SPACE, value)

    @property
    def body(self):
        """Body is readable Page property."""
        return self._getattr(const.Fields.BODY, itemType=Body)

    @body.setter
    def body(self, value):
        """Body is writeable Page property.

        Args:
            value (rsg_clients.confluence.payloads.page.Body): Body object that holds the information about the page body.
        """
        if not isinstance(value, Body):
            value = Body(self.server, value, const.Representation.STORAGE)
        self._setattr(const.Fields.BODY, value)

    @property
    def parent(self):
        """Parent is readable Page property."""
        return self._getmutable(const.Fields.ANCESTORS, list, itemType=self.server.page)[-1]

    @parent.setter
    def parent(self, value):
        """Set the parent page of the curent page.

        Args:
            value (int or rsg_clients.confluence.payloads.page.Page): ID of the page or a page itself.
        """
        if isinstance(value, int):
            value = self.server.page(value)
        self._setattr(const.Fields.ANCESTORS, [value], True)

    @property
    def labels(self):
        """Labels is readable Page property."""
        # Query for labels only if the page already exists on the server.
        if self._id and const.Fields.LABELS not in self._fields:
            data = self._getLabelData()
            values = []
            if data:
                for labelItem in data["results"]:
                    values.append(self.server.label(index=labelItem[const.Fields.ID], data=labelItem))
            self._fields[const.Fields.LABELS] = values
        return self._getmutable(const.Fields.LABELS, list, itemType=self.server.label)

    @property
    def attachments(self):
        """Attachments is readable Page property."""
        if self._id and const.Fields.ATTACHMENTS not in self._fields:
            data = self._getAttachmentData()
            if data:
                values = []
                for attachItem in data["results"]:
                    values.append(self.server.attachment(index=attachItem[const.Fields.ID], data=attachItem))
                self._fields[const.Fields.ATTACHMENTS] = values
        return self._getmutable(const.Fields.ATTACHMENTS, list, itemType=self.server.attachment)

    @property
    def children(self):
        """Queries the children pages from the confluence server."""
        if self._id and const.Fields.CHILDREN not in self._fields:
            data = self._getChildrenPages()
            if data:
                values = []
                for child in data:
                    values.append(self.server.page(index=child[const.Fields.ID], data=child))
                self._fields[const.Fields.CHILDREN] = values

        return self._getmutable(const.Fields.CHILDREN, list, itemType=self.server.page)

    def _getChildrenPages(self, fields=None, returnRaw=False, childType=None):
        """Queries the page children information from the confluence server.
        This is a separate function as children have their own endpoint.

        Args:
            fields (dictionary): the field name and its value to include in the url for the get request
            returnRaw (bool): return the data from the request as is (usually a string)
            childType (str): what kind of children do we want to get back, attachments, pages

        Returns:
            dict payload from the server
        """
        sections = [const.Endpoints.CONTENT, self.id, const.Endpoints.CHILD, const.Endpoints.PAGE]

        fields = fields or {}
        url = self.server.buildUrl(sections, fields=fields)
        return self.server.get(url, returnRaw=returnRaw)["results"]


class Builder(basePayload.BaseBuilder):
    @classmethod
    def cls(cls):
        """Class for the object the builder creates.

        Returns:
            rsg_clients.confluence.payloads.page.Page
        """
        return Page

    def body(self, body=None, representation=None):
        """Creates a Body payload.

        Args:
            body (str): html presentation of the body of the page
            representation (str): how do we want the presentation of the page, "page", "wiki" or "markup"

        Returns:
            rsg_clients.confluence.payloads.page.Body
        """
        return Body(self._server, body, representation)

    def search(self, index=None, data=None):
        """Creates a Search payload.

        Args:
            index (str): string which to search for.
            data (dict): payload information.

        Returns:
            rsg_clients.confluence.payloads.search.Search
        """
        return self._server.search(index, data)

    def label(self, labelName):
        """Creates a Label object.

        Args:
            labelName (str): text to show on the label

        Returns:
            rsg_clients.confluence.payloads.label.Label
        """
        label = self._server.label()
        label.name = labelName
        label.prefix = "global"
        return label

    def getByTitle(self, title):
        """Gets the page based on the title provided.

        Args:
            title (str): title which to look for.

        Returns:
            rsg_clients.confluence.payloads.page.Page
        """
        sections = [const.Endpoints.CONTENT]
        url = self._server.buildUrl(
            sections, fields={const.Fields.TITLE: title, const.Command.EXPAND: ",".join(Page._EXPANDABLES)}
        )
        returnData = self._server.get(url)
        if returnData[const.Fields.RESULTS]:
            returnData = returnData[const.Fields.RESULTS][0]
            return Page(self._server, index=int(returnData[const.Fields.ID]), data=returnData)

    def getByTitleAndSpace(self, title, spaceName):
        """Gets the page based on the title provided and the space it belongs to.

        Args:
            title (str): title which to look for.
            spaceName (str): Key name of the space

        Returns:
            rsg_clients.confluence.payloads.page.Page
        """
        sections = [const.Endpoints.CONTENT]
        url = self._server.buildUrl(
            sections,
            fields={
                const.Fields.TITLE: title,
                const.Fields.SPACE: spaceName,
                const.Command.EXPAND: ",".join(Page._EXPANDABLES),
            },
        )

        returnData = self._server.get(url)
        if returnData:
            returnData = returnData[const.Fields.RESULTS][0]
            return Page(self._server, index=int(returnData[const.Fields.ID]), data=returnData)
