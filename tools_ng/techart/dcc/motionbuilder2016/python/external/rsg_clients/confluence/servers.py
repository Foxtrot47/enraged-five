"""The confluence servers that we connect to."""
from rsg_core_py.abstracts import abstractServer
from rsg_core_py.metaclasses import singleton
from rsg_clients.confluence import const, core


class BaseServer(abstractServer.AbstractServer):
    """Base server class for interacting with a confluence server."""

    NAME = None

    def __init__(self):
        """Constructor."""
        self._confluence = core.Confluence(self.NAME)

    def logout(self):
        """Logs out the server."""
        self._confluence.logout()

    @property
    def instance(self):
        """The internal connection to the server if it is represented by another object.

        Returns:
            rsg_clients.confluence.core.Confluence
        """
        return self._confluence

    @property
    def confluence(self):
        """The confluence instance to perform all the commands through.

        Returns:
            rsg_clients.confluence.core.Confluence
        """
        return self._confluence

    @property
    def restUrl(self):
        """Rest url for this server.

        Returns:
            str
        """
        return self._confluence.restUrl

    def buildUrl(self, sections, fields=None, additionalArgs=None):
        """Builds the url to request/send information through the rest api.

        Args:
            sections (list): parts of the url

            fields (dict): fields to include in the url
            additionalArgs (object): to be determined as some queries require non-standardized information

        Returns:
            str
        """
        return self.confluence.buildUrl(sections=sections, fields=fields, additionalArgs=additionalArgs)

    def get(self, url, contentType=None, returnRaw=False):
        """Does a get request on the given url and attempts to connect to the server if the token timed out.

        Args:
            url (str): url to query information from

            contentType (str): The content type that the get command should expect. Defaults to json.
            returnRaw (bool): returns the results of the query as a string.

        Returns:
            dict
        """
        return self._confluence.get(url, contentType=contentType, returnRaw=returnRaw)

    def post(self, url, payload=None, contentType=None, files=None):
        """Does a post request on the given url.

        Args:
            url (str): url to query information from

            payload (rsg_clients.confluence.contents.AbstractPayload): Data to provide to the post request.
            contentType (str): The content type that this post command should expect. Defaults to xml.

        Returns:
            dict
        """
        return self._confluence.post(url, payload=payload, contentType=contentType, files=None)

    def put(self, url, payload=None, contentType=None):
        """Does a put request on the given url.

        Args:
            url (str): url to query information from

            payload (rsg_clients.confluence.contents.AbstractPayload): Data to provide to the post request.
            contentType (str): The content type that this post command should expect. Defaults to xml.

        Returns:
            dict
        """
        return self._confluence.put(url, payload=payload, contentType=contentType)

    def delete(self, url, payload=None, contentType=None):
        """Does a delete request on the given url.

        Args:
            url (str): url to query information from

            payload (rsg_clients.confluence.contents.AbstractPayload): Data to provide to the post request.
            contentType (str): The content type that this post command should expect. Defaults to xml.

        Returns:
            dict
        """
        return self._confluence.delete(url, payload=payload, contentType=contentType)

    @property
    def page(self):
        """Page builder.

        Returns:
            rsg_clients.confluence.payloads.page.Builder
        """
        return self._confluence.page

    @property
    def space(self):
        """Page builder.

        Returns:
            rsg_clients.confluence.payloads.space.Builder
        """
        return self._confluence.space

    @property
    def attachment(self):
        """Page builder.

        Returns:
            rsg_clients.confluence.payloads.attachment.Builder
        """
        return self._confluence.attachment

    @property
    def search(self):
        """Search builder.

        Returns:
            rsg_clients.confluence.payloads.search.Builder
        """
        return self._confluence.search


class Production(BaseServer):
    """Connection to the Confluence Production server."""

    __metaclass__ = singleton.Singleton
    NAME = const.Servers.PRODUCTION
    TYPE = const.Servers.getType(NAME)


class Development(BaseServer):
    """Connection to the Confluence Development server.

    Notes:
        Access to the dev server will need to be requested from IT.
    """

    __metaclass__ = singleton.Singleton
    NAME = const.Servers.DEVELOPMENT
    TYPE = const.Servers.getType(NAME)


def getByName(name):
    """Gets the server based on the name.

    Args:
        name (str): name of the server to get

    Returns:
        rockstar.shotgun.servers.BaseServer
    """
    for cls in BaseServer.__subclasses__():
        if cls.NAME == name:
            return cls()
    return None


def getByType(serverType):
    """Gets the server based on the type.

    Args:
        serverType (str): type of the server to get

    Returns:
        rockstar.shotgun.servers.BaseServer
    """
    for cls in BaseServer.__subclasses__():
        if cls.TYPE == serverType:
            return cls()
    return None


production = Production()
development = Development()
