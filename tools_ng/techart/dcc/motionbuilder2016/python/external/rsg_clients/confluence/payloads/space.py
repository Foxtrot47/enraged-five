import os

from rsg_clients.confluence import const
from rsg_clients.confluence.payloads import basePayload


class Space(basePayload.BasePayload):
    """ Class that presents confluence space object."""

    _COMMAND = const.Endpoints.SPACE

    _INSTANCES = {}

    _EXPANDABLES = [const.Fields.METADATA, const.Fields.ICON, const.Fields.DESCRIPTION, const.Fields.HOMEPAGE]

    def _setup(self, index=None, data=None, server=None):
        super(Space, self)._setup(index, data, server)
        self._exists = None

    def refresh(self, command=None, data=None):
        """Performs the confluence query required to get the data to fill this object with.

        Args:
            command (str): command to run or the id of the payload type we want to grab from confluence
            data (dict): data from confluence to refresh the instance with
        """
        if data is None:
            data = self._getData(
                self.server, command or self.id, fields={const.Command.EXPAND: ",".join(Space._EXPANDABLES)}
            )

        self.parse(data.get(self.className(), data))
        self._removeStalePayloads(instance=self)

    def _parse(self, data):
        """Parses Space style dictionary.

        Args:
            data (dict): dictionary generated from the confluence rest api
        """
        values = {}
        for key, value in data.items():
            if key == const.Fields.HOMEPAGE:
                value = self.server.page(index=value[const.Fields.ID], data=value)

            values[key] = value

        self._fields.update(values)

    @classmethod
    def builder(cls):
        """The class for the builder that is used to build instances of this class.

        Returns:
            rsg_clients.confluence.basePayload.BaseBuilder
        """
        return Builder

    @property
    def id(self):
        """Id is readable page property."""
        return self._id

    @property
    def spaceKey(self):
        """SpaceKey is readable page property."""
        return self._getattr(const.Fields.KEY)

    @property
    def name(self):
        """Name is readable page property."""
        return self._getattr(const.Fields.NAME)

    @property
    def spaceType(self):
        """SpaceType is readable page property."""
        return self._getattr(const.Fields.TYPE)

    @property
    def homepage(self):
        """Homepage of the space."""
        return self._getattr(const.Fields.HOMEPAGE)

    @property
    def homepageID(self):
        """Homepage id of the space."""
        return self.homepage.id

    def toDict(self):
        """The representation of this payload as a dictionary that can be used to print out as string or post as payload.

        Returns:
            dict
        """
        values = {}
        # Get mutable changes
        edits = dict(self._edits)
        edits.update(self._mutables)
        # Get edits on the local entity
        fields = dict(self._fields)
        fields.update(edits)

        for key, value in fields.items():
            if key == const.Fields.HOMEPAGE:
                value = value.toDict()
            values[key] = value

        return values


class Builder(basePayload.BaseBuilder):
    @classmethod
    def cls(cls):
        raise LookupError("Space can be looked up only by it's name. Please use getByKey instead.")

    def search(self, index=None, data=None):
        """Creates a Search payload.

        Args:
            index (str): string which to search for.
            data (dict): payload information.

        Returns:
            rsg_clients.confluence.payloads.search.Search
        """
        return self._server.search(index, data)

    def getByKey(self, keyName):
        """Gets the space info based on the space key name provided.

        Args:
            keyName (str): key by which the space is saved on the server.

        Returns:
            rsg_clients.confluence.payloads.space.Space
        """
        sections = [const.Endpoints.SPACE, keyName]
        # url = self._server.buildUrl(sections)
        url = self._server.buildUrl(sections, fields={const.Command.EXPAND: ",".join(Space._EXPANDABLES)})
        returnData = self._server.get(url)

        return Space(self._server, index=returnData[const.Fields.ID], data=returnData)
