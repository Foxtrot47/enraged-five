import json
import datetime

from rsg_core import conversions
from rsg_core_py.metaclasses import mapping
from rsg_core_py.abstracts import abstractBuilder, abstractPayload
from rsg_clients.confluence import const, exceptions


class BasePayload(abstractPayload.AbstractPayload):

    __metaclass__ = mapping.Mapping
    _COMMAND = None

    _INDEX_FIELD = const.Fields.ID

    _EXPANDABLE = []  # List of fields that are expandable to add in the expand to get the info on.

    _isCachable = True

    def __new__(cls, confluence, index=None, *args, **kwargs):
        """Overrides built-in method.

        Stores & returns the instance of the class if an argument is provided, otherwise creates a new instance

        Args:
            confluence (rsg_clients.confluence.core.Connection): the local server to set for this object
            index (int): the id of the item from confluence
            *args (list): additional parameters accepted by the inherited class
            **kwargs (dict): additional keyword arguments accepted by the inherited class

        Returns:
            object
        """
        # There are cases where we have may get non-int values for the index and this breaks the caching
        # if not isinstance(index, (int, type(None))):
        #     raise ValueError("Expected int or None, received {} instead".format(type(index).__name__))
        return cls._getInstance(confluence, index)

    def __init__(self, confluence, index=None, data=None):
        """Initializer.

        Args:
            confluence (rsg_clients.confluence.server.BaseServer): the local server to set for this object
            index (int): id of the object from the server
            data (dictionary): data that represents this object
        """
        self._setup(index, data, confluence)
        if self._isCached(confluence, index, self):
            if data is not None:
                # Helps refresh the data when the data is required
                self.refresh(data=data)
            return

        self._isValid = None  # This will get set a value once data is pulled from the server
        # properties to store
        if isinstance(data, dict):
            # The class name is usually the top level key and it is capitalized, so it should not conflict wth field names
            # field names are always lowercase
            # data = data.get(self.className(), data)
            self.parse(data)

        self._cache()

    def refresh(self, command=None, data=None):
        """Performs the confluence query required to get the data to fill this object with.

        Args:
            command (str): command to run or the id of the payload type we want to grab from confluence
            data (dict): data from confluence to refresh the instance with
        """
        if data:
            dictionary = data
        else:
            dictionary = self._getData(self.server, command or self.id)

        self.parse(dictionary.get(self.className(), dictionary))
        self._removeStalePayloads(instance=self)

    @classmethod
    def _getData(cls, server, index, returnRaw=False, fields=None, command=None, acceptType=None):
        """Performs a get request against the server to retrieve information on data on a single object.
        It is based on the data type that the class represents.

        Args:
            server (rsg_clients.confluence.server.BaseServer): server to connect to
            index (str): the index of the object or command to run
            returnRaw (bool): return the data from the request as is (usually a string)
            fields (dict): the field name and its value to include in the url for the get request
            command (str): the command to run in the url, defaults to the command for the class if nothign is passed in.
            acceptType (str):  the format that the data pulled from the rest query should be returned as.

        Returns:
            list
        """
        sections = [command or cls.command()]
        if index:
            sections.append(index)
        fields = fields or {}
        url = server.buildUrl(sections, fields=fields)

        return server.get(url, returnRaw=returnRaw, acceptType=acceptType)

    def parse(self, dictionary):
        """Parses a dictionary generated from confluence rest api and adds its contents to this instance.

        Args:
            dictionary (dict): dictionary to parse
        """
        try:
            # remove invalid values
            dictionary = conversions.convertValue(dictionary)
            value = self._parse(dictionary)

            self._isUpdated = True
            self._isValid = True
            self._lastDataUpdate = datetime.datetime.utcnow()
            return value

        except Exception as exception:
            self._isUpdated = False
            raise

    def _validate(self, requiredProperties):
        for requiredProperty in requiredProperties:
            if self._fields.get(requiredProperty, None) is None:
                raise exceptions.MissingValue("A value needs to be set for the {} property".format(requiredProperty))

    @classmethod
    def command(cls):
        """Reimplements inherited method.
        Returns the value to use as a key for retrieving this class

        Returns:
            str
        """
        return cls._COMMAND

    @classmethod
    def key(cls):
        """Name to use as key for dictionaries returned from confluence to find information about this type of data.

        Returns:
            str
        """
        return cls.command()

    @classmethod
    def builder(cls):
        raise NotImplementedError

    def toDict(self):
        raise NotImplementedError

    def toString(self):
        """Json representation of the payload that can be used in the post commands."""
        return json.dumps(self.toDict())


class BaseBuilder(abstractBuilder.AbstractPayloadBuilder):
    """Base Builder for building bugstar payloads."""

    def _clearCache(self, all=False):
        """Clears out the memory caches of the entities that have been queried from Shotgun.
        This class is strictly for debugging purposes.

        Args:
            all (bool): clear the cache for all classes that subclass the BaseEntity class.
        """
        self.cls()._INSTANCES = {}
        if all:
            for subclass in BasePayload.__subclasses__():
                subclass._INSTANCES = {}
