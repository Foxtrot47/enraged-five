"""Test module for debugging the basic interface for interacting with bugs on Confluence."""
import os
import unittest

from rsg_core import accounts
from rsg_clients.confluence import servers, const


class TestConnections(unittest.TestCase):
    """Tests basic connection functionality."""

    SERVER = None  # Server to use for this test

    def test_000_connection(self):
        """Tests if you can connect to the server."""
        for server in (servers.development, servers.production):
            server.account = accounts.OtherUser("notreal", "badpassword")
            self.assertFalse(server.confluence.login())
            server.account = accounts.CurrentUser()
            self.assertTrue(server.confluence.login())


class TestBasics(unittest.TestCase):
    """Tests basic connection functionality."""

    SERVER = None  # Server to use for this test

    PAGE_ID = None
    PAGE_TITLE = "Unit test page creation"

    SPACE_NAME = "RSGTECHART"

    # Attachment file can be path to the local file or the full path to the file on perforce.
    ATTACHMENT_IMAGE = os.path.join(const.Paths.TESTS_DIRECTORY, "image.jpg")

    @classmethod
    def setUpClass(cls):
        """Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.TechArtAutomation()
        # cls.SERVER.account = accounts.CurrentUser()

    @classmethod
    def tearDownClass(cls):
        cls.SERVER.confluence.logout()

    def test_000_connection(self):
        """Tests if you can connect to the server."""
        for server in (servers.development, servers.production):
            server.account = accounts.OtherUser("notreal", "badpassword")
            self.assertFalse(server.confluence.login())
            server.account = accounts.CurrentUser()
            self.assertTrue(server.confluence.login())

    def test_001_getSpace(self):
        """Tests getting the space."""
        space = self.SERVER.space.getByKey(self.SPACE_NAME)
        self.assertIsNotNone(space)
        self.assertIsNotNone(space.id)

    def test_002_createPage(self):
        """Tests creation of the page."""
        page = self.SERVER.page()
        page.title = self.PAGE_TITLE
        page.space = self.SERVER.space.getByKey(self.SPACE_NAME)
        page.body = "Here goes the body of the page..."
        page.pageType = const.PageTypes.PAGE
        page.create()
        self.assertIsNotNone(page)
        self.assertIsNotNone(page.id)

    def test_003_searchPage(self):
        """Test searching for the page via title and update the pages parent page."""
        parentPages = self.SERVER.search.getContentByTitle("Tech Art", space=self.SPACE_NAME)
        self.assertIsNotNone(parentPages)
        self.assertIsNotNone(parentPages[0].id)

        page = self.SERVER.page.getByTitle(self.PAGE_TITLE)
        page.parent = parentPages[0]
        page.update()

        # Make sure to get the latest changes from the server on the page before checking
        page = self.SERVER.page(page.id)
        self.assertEqual(page.parent.id, parentPages[0].id)

    def test_004_pageLabels(self):
        """Tests adding and removing the labels to the page."""
        labelTxt = "unit_test"
        label = self.SERVER.page.label(labelTxt)
        page = self.SERVER.page.getByTitleAndSpace(self.PAGE_TITLE, self.SPACE_NAME)
        # Add a label to the page.
        page.labels.append(label)
        page.update()

        page = self.SERVER.page(page.id)
        self.assertEqual(len(page.labels), 1)
        self.assertEqual(page.labels[0].name, labelTxt)
        # Removing the labels
        page.labels.pop(0)
        page.update()
        self.assertEqual(len(page.labels), 0)

    def test_005_pageAttachments(self):
        """Tests adding and removing the attachments to the page."""
        attachment = self.SERVER.attachment()
        attachment.filename = self.ATTACHMENT_IMAGE

        page = self.SERVER.page.getByTitle(self.PAGE_TITLE)
        page.attachments.append(attachment)
        page.update()

        # Query the page from the server again to refresh the data.
        page = self.SERVER.page(page.id)
        self.assertEqual(len(page.attachments), 1)
        self.assertEqual(page.attachments[0].title, os.path.basename(self.ATTACHMENT_IMAGE))

        # Just calling the function to make sure it doesn't error out
        page.uploadAttachVersion(self.ATTACHMENT_IMAGE)

        # Removing the attachment from the page.
        page.attachments.pop(0)
        page.update()
        self.assertEqual(len(page.attachments), 0)

    def test_006_updatePageContext(self):
        """Tests updating the Page body content."""
        page = self.SERVER.page.getByTitle(self.PAGE_TITLE)
        bodyContent = "Updating the page content"
        page.body = bodyContent
        page.update()

        page = self.SERVER.page(page.id)
        self.assertEqual(page.body.bodyValue, bodyContent)

    def test_007_deletePage(self):
        """Tests that there is no error while calling to delete the page tree."""
        page = self.SERVER.page.getByTitle(self.PAGE_TITLE)
        page.delete()

        page = self.SERVER.page.getByTitle(self.PAGE_TITLE)
        self.assertIsNone(page)
