""" Module for storing persistent values for the Confluence package"""
import os

from rsg_clients import const


class AppInfo(object):
    """Basic information about the application"""

    NAME = "Confluence"
    VERSION = "0.3.1"
    TITLE = "{0} (Version {1})".format(NAME, VERSION)


class Servers(object):
    """Confluence servers we use.

    Notes:
        T2 does not have individual urls for the different instances of Confluence.
        These are some legacy ip addresses to the test site that should still work.
        Keeping them here for incase we may need to access them.
        tk2edccnf2n1, 10.80.20.94
        tk2edccnf2n2, 10.80.20.95
    """

    PRODUCTION = "hub.gametools.dev"
    DEVELOPMENT = "10.80.2.71"  # The ip address for the test site, there is no url for it

    MAPPINGS = {const.Servers.PRODUCTION: PRODUCTION, const.Servers.DEVELOPMENT: DEVELOPMENT}

    @classmethod
    def getRestUrl(cls, server):
        """Builds the base url for running rest commands on the given server.

        Arguments:
            server (string): name of the server to build the url for

        Returns:
            string
        """
        url = "https://{}/rest/api".format(server)
        return url

    @classmethod
    def getType(cls, server):
        """The type of server of that is being hit (production or development).

        Args:
            server (str): the name of the confluence server

        Returns:
            str
        """
        for key, value in cls.MAPPINGS.iteritems():
            if server == value:
                return key


class Paths(object):
    """Paths for directories and files used by this library."""

    ROOT_PATH = os.path.dirname(__file__)
    RESOURCES_DIRECTORY = os.path.join(ROOT_PATH, "resources")
    PAYLOAD_DIRECTORY = os.path.join(ROOT_PATH, "payloads")
    TESTS_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "tests")


# TODO: Figure out what this is for
class Representation(object):
    """Representations of data in Confluence."""

    STORAGE = "storage"
    MARKDOWN = "markdown"
    WIKI = "wiki"


class Endpoints(object):
    """Commonly accessed enpoints for the Confluence Rest Api.

    For full list of supported endpoints, check out:
        https://docs.atlassian.com/ConfluenceServer/rest/7.11.0/
    """
    SPACE = "space"
    CONTENT = "content"
    SEARCH = "search"
    LABEL = "label"
    CHILD = "child"
    COMMENT = "comment"
    ATTACHMENT = "attachment"
    PAGE = "page"
    DATA = "data"


class Fields(object):
    # Common fields
    ID = "id"
    TYPE = "type"
    VERSION = "version"
    # Common _expandable fields
    METADATA = "metadata"
    RESULTS = "results"
    # Common _links fields
    WEBUI = "webui"
    COLLECTION = "collection"
    BASE = "base"
    CONTEXT = "context"
    SELF = "self"

    # Page fields
    STATUS = "status"
    TITLE = "title"
    SPACE = "space"
    # Page Expandable
    BODY = "body"
    ANCESTORS = "ancestors"
    CHILDREN = "children"
    CONTAINER = "container"
    DESCENDANTS = "descendants"
    OPERATIONS = "operations"
    RESTRICTIONS = "restrictions"
    # Page links fields
    EDIT = "edit"

    # Fields created to store information from different Endpoints
    LABELS = "labels"
    ATTACHMENTS = "attachments"

    # Page body fields
    STORAGE = "storage"
    VALUE = "value"
    REPRESENTATION = "representation"

    # Page label fields
    PREFIX = "prefix"

    # Page attachment fields
    COMMENT = "comment"
    MEDIA_TYPE = "mediaType"
    FILENAME = "filename"

    # Space fields
    KEY = "key"
    NAME = "name"

    # Space expandable
    ICON = "icon"
    DESCRIPTION = "description"
    HOMEPAGE = "homepage"

    # Search fields
    CONTENT = "content"

    # Version fields
    NUMBER = "number"
    MINOR_EDIT = "minorEdit"


class Command(object):
    EXPAND = "expand"
    CQL = "cql"


class PageTypes(object):
    PAGE = "page"



# TODO: Remove once the module is refactored and the wrapper file removed
# Credentials for confluence are username:password encoded in base64
# CREDENTIAL = "Basic c3ZjcnNnY29uZmx1ZW5jZXdlYjpqdW5rLU5RZWhUUHE="
