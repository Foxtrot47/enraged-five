# Copied over from the tools/bin/ConfluenceTools. original author is Kristopher Williams
# TODO: Refactor completely so it matches the standards of the clients lib
# TODO: Decouple logic into appropriate modules
# TODO: Create a pages, labels, and attachment payloads

import ssl
import warnings
import json

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.poolmanager import PoolManager
from requests.packages.urllib3.exceptions import InsecureRequestWarning, InsecurePlatformWarning

from rsg_clients.confluence import const


class MyAdapter(HTTPAdapter):
    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = PoolManager(
            num_pools=connections, maxsize=maxsize, block=block, ssl_version=ssl.PROTOCOL_TLSv1_2
        )


class ConfluencePage:
    def __init__(self):
        self.pageId = None
        self.body = None
        self.version = None
        self.title = None
        self.status = None


class ConfluenceAttachmentData:
    def __init__(self):
        self.attId = None
        self.link = None
        self.version = None
        self.labels = None
        self.name = None
        self.container = None


class ConfluenceUploadController:
    def __init__(self):
        self.adapterSetup()

    def IsLong(self, inputString):
        """Function to check whether a variable is a long type

        Args:
            inputString (str): The input string to test.

        Returns:
            bool: True if input value can be cast to a long, False if not.
        """
        try:
            long(inputString)
            return True
        except ValueError:
            return False

    def FixSpecialCharacters(self, input, isExact=False):
        output = input
        if isExact:
            output = input.replace("[", "%5B")
            output = output.replace("]", "%5D")
        else:
            output = input.replace("[", "'%5B'")
            output = output.replace("]", "'%5D'")
        output = output.replace("&", "%26")
        return output

    def postAttachmentFile(
        self, name, content, pageID, label="", attachType="application/octet-stream", deleteFirst=False
    ):
        """Function to take the content of a file and upload them to a page as an attachment

        Args:
            name ([type]): [description]
            content ([type]): [description]
            pageID ([type]): [description]
            label (str, optional): [description]. Defaults to "".
            attachType (str, optional): [description]. Defaults to "application/octet-stream".
            deleteFirst (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        if len(content) > 0:
            HUB_UPLOAD = const.Servers.getRestUrl(const.Servers.PRODUCTION) + pageID + "/child/attachment"

            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
                warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

                r = self.adapterInstance.request(
                    method="GET",
                    url=HUB_UPLOAD + "?expand=version",
                    params={"filename": name},
                    headers=({"Authorization": const.CREDENTIAL}),
                    verify=False,
                )

                if r.status_code == 401 and r.reason == "Unauthorized":
                    return -1
                newAttachment = ConfluenceAttachmentData()
                rJson = r.json()
                # if it exists, update it (if it differs)
                if len(rJson["results"]) != 0:
                    results = rJson["results"][0]
                    attId = results["id"]

                    if deleteFirst == False:
                        version = results["version"]["number"]

                        r = self.adapterInstance.request(
                            method="GET",
                            url=const.Servers.PRODUCTION + results["_links"]["download"][1:],
                            headers=({"Authorization": const.CREDENTIAL}),
                            verify=False,
                        )

                        oldJson = r.content

                        if content != oldJson:
                            print "Changes needed for {}. Updating...".format(name)
                            r = self.adapterInstance.request(
                                method="POST",
                                url=HUB_UPLOAD + "/{attId}/data".format(attId=attId),
                                files={"file": (name, content, attachType), "minorEdit": "true"},
                                headers=({"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL}),
                                verify=False,
                            )

                            if r.ok:
                                rJson = r.json()
                                if label != "":
                                    self.addLabelToContent(attId, label)
                                newAttachment.attId = rJson["id"]
                                newAttachment.link = rJson["_links"]["download"]
                                newAttachment.version = rJson["version"]["number"]
                                labelsList = rJson["metadata"]["labels"]["results"]
                                newAttachment.container = rJson["container"]["id"]
                                newAttachment.labels = []
                                for label in labelsList:
                                    newAttachment.labels.append(label["name"])
                                newAttachment.name = rJson["title"]
                                print "Complete."
                                return newAttachment
                            else:
                                print "Wiki error: Status Code: {}: {}: {}".format(
                                    r.status_code, r.reason, r.json()["message"]
                                )
                                return newAttachment
                        else:
                            print "No changes in file, no upload took place"
                    else:
                        print ("Removing existing attachment")
                        self.removeAttachmentFileViaId(attId)
                        print "Readding file...".format(name)
                        r = self.adapterInstance.request(
                            method="POST",
                            url=HUB_UPLOAD,
                            params={"filename": name},
                            files={"file": (name, content, attachType), "minorEdit": "true"},
                            headers=({"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL}),
                            verify=False,
                        )
                        if r.ok:
                            rJson = r.json()
                            jsonToParse = rJson["results"][0]
                            contentId = jsonToParse["id"]
                            if label != "":
                                self.addLabelToContent(contentId, label)
                            newAttachment.attId = jsonToParse["id"]
                            newAttachment.link = jsonToParse["_links"]["download"]
                            newAttachment.version = jsonToParse["version"]["number"]
                            newAttachment.container = jsonToParse["container"]["id"]
                            newAttachment.labels = []
                            newAttachment.name = jsonToParse["title"]
                            print "Complete."
                            return newAttachment
                        else:
                            print "Wiki error: Status Code: {}: {}: {}".format(
                                r.status_code, r.reason, r.json()["message"]
                            )
                            return newAttachment
                else:  # doesn't exist, so add it!
                    print "Changes needed for {}. Adding new file...".format(name)
                    r = self.adapterInstance.request(
                        method="POST",
                        url=HUB_UPLOAD,
                        params={"filename": name},
                        files={"file": (name, content, attachType), "minorEdit": "true"},
                        headers=({"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL}),
                        verify=False,
                    )
                    if r.ok:
                        rJson = r.json()
                        jsonToParse = rJson["results"][0]
                        contentId = jsonToParse["id"]
                        if label != "":
                            self.addLabelToContent(contentId, label)
                        newAttachment.attId = jsonToParse["id"]
                        newAttachment.link = jsonToParse["_links"]["download"]
                        newAttachment.version = jsonToParse["version"]["number"]
                        newAttachment.container = jsonToParse["container"]["id"]
                        newAttachment.labels = []
                        newAttachment.name = jsonToParse["title"]
                        print "Complete."
                        return newAttachment
                    else:
                        print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
                        return newAttachment
        else:
            print "Error during attachment upload. Attachment {0} has no content".format(attachmentData.name)
            return -2
        return 0

    def updateAttachmentFile(self, attachmentData, content, attachType="application/octet-stream"):
        """Function to update a known attachment

        Args:
            attachmentData ([type]): [description]
            content ([type]): [description]
            attachType (str, optional): [description]. Defaults to "application/octet-stream".

        Returns:
            [type]: [description]
        """
        if len(content) > 0:
            HUB_UPLOAD = const.Servers.getRestUrl(const.Servers.PRODUCTION) + attachmentData.container + "/child/attachment"
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
                warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

                r = self.adapterInstance.request(
                    method="GET",
                    url=const.Servers.PRODUCTION + attachmentData.link[1:],
                    headers=({"Authorization": const.CREDENTIAL}),
                    verify=False,
                )

                oldJson = r.content
                newAttachment = ConfluenceAttachmentData()
                if content != oldJson:
                    print "Changes needed for {}. Updating...".format(attachmentData.name)
                    r = self.adapterInstance.request(
                        method="POST",
                        url=HUB_UPLOAD + "/{attId}/data".format(attId=attachmentData.attId),
                        files={"file": (attachmentData.name, content, attachType), "minorEdit": "true"},
                        headers=({"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL}),
                        verify=False,
                    )

                    if r.ok:
                        rJson = r.json()
                        print "Complete."
                        newAttachment.attId = rJson["id"]
                        newAttachment.link = rJson["_links"]["download"]
                        newAttachment.version = rJson["version"]["number"]
                        labelsList = rJson["metadata"]["labels"]["results"]
                        newAttachment.container = rJson["container"]["id"]
                        newAttachment.labels = []
                        for label in labelsList:
                            newAttachment.labels.append(label["name"])
                        newAttachment.name = rJson["title"]
                        return newAttachment
                    else:
                        print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
                        return -2
                else:
                    print "No changes in file, no upload took place"
        else:
            print "Error during attachment upload. Attachment {0} has no content".format(attachmentData.name)
            return -2

        return 0

    def updateAttachmentMimeType(self, attachmentData, attachType):
        """Function to update a known attachments mimetype

        Args:
            attachmentData ([type]): [description]
            attachType ([type]): [description]
        """
        HUB_UPLOAD = const.Servers.getRestUrl(const.Servers.PRODUCTION) + attachmentData.container + "/child/attachment"

        self.setAttachmentMediaType(
            HUB_UPLOAD + "/" + attachmentData.attId, attachmentData.attId, attachmentData.version + 1, attachType
        )

    def addLabelToContent(self, contentId, label):
        """Function to add a label to a piece of content

        Args:
            contentId ([type]): [description]
            label ([type]): [description]

        Returns:
            [type]: [description]
        """
        requestBody = [{"name": label}]

        labelUrl = const.Servers.getRestUrl(const.Servers.PRODUCTION) + contentId + "/label"
        r = self.adapterInstance.request(
            method="POST",
            url=labelUrl,
            data=json.dumps(requestBody),
            headers=(
                {"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL, "Content-Type": "application/json"}
            ),
            verify=False,
        )

        if r.ok:
            print "Complete."
            return 1
        else:
            print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
            return -2

    def setAttachmentMediaType(self, ATTACHMENTURL, attId, version, mediaType):
        """Function to add a label to a piece of content

        Args:
            ATTACHMENTURL ([type]): [description]
            attId ([type]): [description]
            version ([type]): [description]
            mediaType ([type]): [description]

        Returns:
            [type]: [description]
        """
        requestBody = {"id": attId, "version": {"number": version}, "metadata": {"mediaType": mediaType}}

        r = self.adapterInstance.request(
            method="PUT",
            url=ATTACHMENTURL,
            data=json.dumps(requestBody),
            headers=(
                {"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL, "Content-Type": "application/json"}
            ),
            verify=False,
        )

        if r.ok:
            print "Complete."
            return 1
        else:
            print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
            return -2
        # -d '{"id":"att917507", "version":{"number":1}, "metadata": {"mediaType": "image/svg+xml" }}' http://localhost:1990/confluence/rest/api/content/98310/child/attachment/att917507

    def removeLabelFromContent(self, contentId, label):
        """Function remove a label from a piece of content

        Args:
            contentId ([type]): [description]
            label ([type]): [description]

        Returns:
            [type]: [description]
        """
        labelUrl = const.Servers.getRestUrl(const.Servers.PRODUCTION) + contentId + "/label?name=" + label
        r = self.adapterInstance.request(
            method="DELETE",
            url=labelUrl,
            headers=(
                {"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL, "Content-Type": "application/json"}
            ),
            verify=False,
        )

        if r.ok:
            print "Complete."
            return 1
        else:
            print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
            return -2

    def removeAttachmentFile(self, attachmentData):
        """Function remove an attachment

        Args:
            attachmentData ([type]): [description]

        Returns:
            [type]: [description]
        """
        HUB_UPLOAD = const.Servers.getRestUrl(const.Servers.PRODUCTION) + attachmentData.attId
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
            warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

            r = self.adapterInstance.request(
                method="DELETE",
                url=HUB_UPLOAD,
                headers=({"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL}),
                verify=False,
            )

            if r.ok:
                print "Complete."
                return 1
            else:
                print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
                return -2
        return 0

    def removeAttachmentFileViaId(self, attachmentId):
        """Function remove an attachment using its ID

        Args:
            attachmentId ([type]): [description]

        Returns:
            [type]: [description]
        """
        HUB_UPLOAD = const.Servers.getRestUrl(const.Servers.PRODUCTION) + attachmentId
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
            warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

            r = self.adapterInstance.request(
                method="DELETE",
                url=HUB_UPLOAD,
                headers=({"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL}),
                verify=False,
            )

            if r.ok:
                print "Complete."
                return 1
            else:
                print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
                return -2
        return 0

    def doesPageExist(self, pageName, nameSpace="", isExact=False):
        """Function to check whether a given page can be found on confluence from a supplied text string

        Args:
            pageName ([type]): [description]
            nameSpace (str, optional): [description]. Defaults to "".
            isExact (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        SEARCH_URL = const.Servers.getRestUrl(const.Servers.PRODUCTION)
        if self.IsLong(pageName):
            SEARCH_URL = SEARCH_URL + "search?cql=id"
            isExact = True
        else:
            SEARCH_URL = SEARCH_URL + "search?cql=title"

        if isExact == False:
            SEARCH_URL = SEARCH_URL + '~"{0}" and type=page'.format(pageName)
        else:
            SEARCH_URL = SEARCH_URL + '="{0}" and type=page'.format(pageName)

        if nameSpace != "":
            SEARCH_URL = SEARCH_URL + " and space={0}".format(nameSpace)

        SEARCH_URL = self.FixSpecialCharacters(SEARCH_URL, isExact)

        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
            warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

            r = self.adapterInstance.request(
                method="GET", url=SEARCH_URL, headers=({"Authorization": const.CREDENTIAL}), verify=False
            )

            rJson = r.json()

            if len(rJson["results"]) != 0:
                return rJson["results"][0]["id"]
            else:
                return None

    def getPageViaSearch(self, pageName, nameSpace="", isExact=False):
        """Function to check whether a given page can be found on confluence from a supplied text string and return the id and contents

        Args:
            pageName ([type]): [description]
            nameSpace (str, optional): [description]. Defaults to "".
            isExact (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        SEARCH_URL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + "search?"
        if self.IsLong(pageName):
            SEARCH_URL = SEARCH_URL + "cql=id"
            isExact = True
        else:
            SEARCH_URL = SEARCH_URL + "cql=title"

        if isExact == False:
            SEARCH_URL = SEARCH_URL + '~"{0}" and type=page'.format(pageName)
        else:
            SEARCH_URL = SEARCH_URL + '="{0}" and type=page'.format(pageName)

        if nameSpace != "":
            SEARCH_URL = SEARCH_URL + " and space={0}".format(nameSpace)

        SEARCH_URL = self.FixSpecialCharacters(SEARCH_URL, isExact)

        SEARCH_URL = SEARCH_URL + "&expand=body.storage,version"

        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
            warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

            r = self.adapterInstance.request(
                method="GET", url=SEARCH_URL, headers=({"Authorization": const.CREDENTIAL}), verify=False
            )

            rJson = r.json()

            if len(rJson["results"]) != 0:
                foundPage = ConfluencePage()
                try:
                    foundPage.pageId = rJson["results"][0]["id"]
                    foundPage.body = rJson["results"][0]["body"]["storage"]["value"]
                    foundPage.version = int(rJson["results"][0]["version"]["number"])
                    foundPage.status = rJson["results"][0]["status"]
                    return foundPage
                except:
                    print ("Confluence Error: Error during retrieval of page search {0}".format(pageName))
                    return ConfluencePage()
            else:
                return ConfluencePage()


    def getChildPageViaSearch(self, pageName, parentId, nameSpace="", isExact=False):
        """Function to check whether a given page can be found on confluence from a supplied text string under a parent page and return the id and contents

        Args:
            pageName ([type]): [description]
            parentId ([type]): [description]
            nameSpace (str, optional): [description]. Defaults to "".
            isExact (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        SEARCH_URL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + "search?"
        if self.IsLong(pageName):
            SEARCH_URL = SEARCH_URL + "cql=id"
            isExact = True
        else:
            SEARCH_URL = SEARCH_URL + "cql=title"

        if isExact == False:
            SEARCH_URL = SEARCH_URL + '~"{0}" and type=page'.format(pageName)
        else:
            SEARCH_URL = SEARCH_URL + '="{0}" and type=page'.format(pageName)

        if nameSpace != "":
            SEARCH_URL = SEARCH_URL + " and space={0}".format(nameSpace)

        if parentId != "":
            SEARCH_URL = SEARCH_URL + " and parent={0}".format(parentId)

        SEARCH_URL = self.FixSpecialCharacters(SEARCH_URL, isExact)

        SEARCH_URL = SEARCH_URL + "&expand=body.storage,version"

        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
            warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

            r = self.adapterInstance.request(
                method="GET", url=SEARCH_URL, headers=({"Authorization": const.CREDENTIAL}), verify=False
            )

            rJson = r.json()

            if len(rJson["results"]) != 0:
                foundPage = ConfluencePage()
                try:
                    foundPage.pageId = rJson["results"][0]["id"]
                    foundPage.body = rJson["results"][0]["body"]["storage"]["value"]
                    foundPage.version = int(rJson["results"][0]["version"]["number"])
                    foundPage.status = rJson["results"][0]["status"]
                    return foundPage
                except:
                    print ("Confluence Error: Error during retrieval of page search {0}".format(pageName))
                    return ConfluencePage()
            else:
                return ConfluencePage()

    def getPageViaId(self, id):
        """Function to retrieve the contents of a known page id

        Args:
            id ([type]): [description]

        Returns:
            [type]: [description]
        """
        PAGEURL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + id + "?expand=body.storage,version"

        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
            warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

            r = self.adapterInstance.request(
                method="GET", url=PAGEURL, headers=({"Authorization": const.CREDENTIAL}), verify=False
            )

            rJson = r.json()
            foundPage = ConfluencePage()
            try:
                foundPage.pageId = rJson["id"]
                foundPage.body = rJson["body"]["storage"]["value"]
                foundPage.version = int(rJson["version"]["number"])
                foundPage.title = rJson["title"]
                foundPage.status = rJson["status"]
                return foundPage
            except:
                print ("Confluence Error: Error during retrieval of page id {0}".format(id))
                return ConfluencePage()

    def getChildrenListOfPage(self, id, findString=None):
        """Function to retrieve a list of the child page names from a specified page

        Args:
            id ([type]): [description]
            findString ([type], optional): [description]. Defaults to None.

        Returns:
            [type]: [description]
        """
        BATCHSIZE = 25
        STARTVAL = 0
        ChildrenList = []
        while True:
            PAGEURL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + id + "/child/page?limit=25&maxsize={0}&start={1}".format(BATCHSIZE, STARTVAL)
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
                warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

                r = self.adapterInstance.request(
                    method="GET", url=PAGEURL, headers=({"Authorization": const.CREDENTIAL}), verify=False
                )

                rJson = r.json()
                STARTVAL = STARTVAL + BATCHSIZE
                try:
                    childrenResults = rJson["results"]
                    if len(childrenResults) == 0:
                        break
                    for cResult in childrenResults:
                        AddPage = True
                        if findString is not None:
                            if findString.lower() not in cResult["title"].lower():
                                AddPage = False
                        if AddPage == True:
                            ChildrenList.append(cResult["title"])
                except:
                    print ("Confluence Error: Error during retrieval of page id {0}".format(id))
                    break
        return ChildrenList

    def getChildrenPagesOfPage(self, id, findString=None):
        """Function to retrieve a list of the child page data from a specified page

        Args:
            id ([type]): [description]
            findString ([type], optional): [description]. Defaults to None.

        Returns:
            [type]: [description]
        """
        BATCHSIZE = 25
        STARTVAL = 0
        ChildrenList = []
        while True:
            PAGEURL = (
                const.Servers.getRestUrl(const.Servers.PRODUCTION)
                + id
                + "/child/page?expand=body.storage,version&limit=25&maxsize={0}&start={1}".format(BATCHSIZE, STARTVAL)
            )
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
                warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

                r = self.adapterInstance.request(
                    method="GET", url=PAGEURL, headers=({"Authorization": const.CREDENTIAL}), verify=False
                )

                rJson = r.json()

                STARTVAL = STARTVAL + BATCHSIZE

                try:
                    childrenResults = rJson["results"]
                    if len(childrenResults) == 0:
                        break
                    for cResult in childrenResults:
                        AddPage = True
                        if findString is not None:
                            if findString.lower() not in cResult["title"].lower():
                                AddPage = False
                        if AddPage == True:
                            ChildPage = ConfluencePage()
                            ChildPage.title = cResult["title"]
                            ChildPage.pageId = cResult["id"]
                            ChildPage.body = cResult["body"]["storage"]["value"]
                            ChildPage.status = cResult["status"]
                            ChildPage.version = int(cResult["version"]["number"])
                            ChildrenList.append(ChildPage)
                except:
                    print ("Confluence Error: Error during retrieval of page id {0}".format(id))
                    break
        return ChildrenList

    def getRecursiveChildrenPagesOfPage(self, id, findString=None):
        """Function to recursively retrieve a list of the child page data from a specified page and its children

        Args:
            id ([type]): [description]
            findString ([type], optional): [description]. Defaults to None.

        Returns:
            [type]: [description]
        """
        TotalChildList = self.getChildrenPagesOfPage(id)
        listToProcess = TotalChildList
        while True:
            print ("Processing child list and recursively retrieving. Current Count {0}".format(len(TotalChildList)))
            currentChildList = listToProcess
            listToProcess = []
            for curChild in currentChildList:
                listToProcess = listToProcess + self.getChildrenPagesOfPage(curChild.pageId)
            TotalChildList = TotalChildList + listToProcess
            if len(listToProcess) == 0:
                break
        if findString is not None:
            purgedList = []
            for child in TotalChildList:
                if findString.lower() in child.title.lower():
                    purgedList.append(child)
            TotalChildList = purgedList
        print ("All children processed. Final page count {0}".format(len(TotalChildList)))
        return TotalChildList

    def CreatePage(self, pageName, nameSpace, contentOfPage, parentPage=None, representation=const.Representation.STORAGE):
        """Function to create a page with the given name in a given namespace

        Optional arg to give a parent page to create the page the child of

        Args:
            pageName ([type]): [description]
            nameSpace ([type]): [description]
            contentOfPage ([type]): [description]
            parentPage ([type], optional): [description]. Defaults to None.
            representation ([type], optional): [description]. Defaults to const.Representation.STORAGE.

        Returns:
            [type]: [description]
        """
        if representation == const.Representation.MARKDOWN:
            contentOfPage = self.ConvertMarkdownToWiki(contentOfPage)
            representation = const.Representation.WIKI

        requestBody = {
            "type": "page",
            "title": pageName,
            "space": {"key": nameSpace},
            "body": {"storage": {"value": contentOfPage, "representation": representation}},
        }
        if parentPage is not None:
            requestBody["ancestors"] = [{"id": parentPage}]

        r = self.adapterInstance.request(
            method="POST",
            url=const.Servers.getRestUrl(const.Servers.PRODUCTION),
            data=json.dumps(requestBody),
            headers=(
                {"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL, "Content-Type": "application/json"}
            ),
            verify=False,
        )

        if r.ok:
            rJson = r.json()
            print "Complete."
            try:
                return rJson["id"]
            except:
                print "Wiki error: Can not find newly created page id"
                return None
        else:
            print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
            return None

    def UpdatePage(
        self, pageId, version, contentOfPage, pageName, minorEdit=False, representation=const.Representation.STORAGE
    ):
        """Function to update a pages content

        Args:
            pageId ([type]): [description]
            version ([type]): [description]
            contentOfPage ([type]): [description]
            pageName ([type]): [description]
            minorEdit (bool, optional): [description]. Defaults to False.
            representation ([type], optional): [description]. Defaults to const.Representation.STORAGE.

        Returns:
            [type]: [description]
        """

        if representation == const.Representation.MARKDOWN:
            contentOfPage = self.ConvertMarkdownToWiki(contentOfPage)
            representation = const.Representation.WIKI

        UPDATEPAGEURL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + pageId
        requestBody = {
            "body": {"storage": {"value": contentOfPage, "representation": representation}},
            "version": {"number": version, "minorEdit": "true" if minorEdit == True else "false"},
            "title": pageName,
            "type": "page",
        }

        r = self.adapterInstance.request(
            method="PUT",
            url=UPDATEPAGEURL,
            data=json.dumps(requestBody),
            headers=(
                {"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL, "Content-Type": "application/json"}
            ),
            verify=False,
        )

        if r.ok:
            rJson = r.json()
            print "Complete."
            try:
                return rJson["id"]
            except:
                print "Wiki error: Can not find newly created page id"
                return None
        else:
            print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
            return None

    def DeletePage(self, pageId):
        """Function to delete a page

        Args:
            pageId ([type]): [description]

        Returns:
            [type]: [description]
        """
        UPDATEPAGEURL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + pageId

        r = self.adapterInstance.request(
            method="DELETE",
            url=UPDATEPAGEURL,
            headers=({"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL}),
            verify=False,
        )

        if r.ok:
            print "Complete."
        else:
            print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
            return None

    def GetLabelsForPage(self, pageId):
        """Function to get list of labels on a page

        Args:
            pageId ([type]): [description]

        Returns:
            [type]: [description]
        """
        LABELPAGEURL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + pageId + "/label"

        r = self.adapterInstance.request(
            method="GET",
            url=LABELPAGEURL,
            headers=(
                {"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL, "Content-Type": "application/json"}
            ),
            verify=False,
        )
        labels = []
        if r.ok:
            rJson = r.json()
            print "Complete."
            try:
                for result in rJson["results"]:
                    labels.append(result["name"])
                return labels
            except:
                print "Wiki error: Can not find newly created page id"
                return labels
        else:
            print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
            return labels

    def SetLabelsForPage(self, pageId, labels):
        """Function to set the labels for a page

        Notes:
            * Batches in 20 because confluence

        Args:
            pageId ([type]): [description]
            labels ([type]): [description]

        Returns:
            [type]: [description]
        """
        LabelBatchSize = 20
        LABELPAGEURL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + pageId + "/label"
        batches = []
        if len(labels) <= LabelBatchSize:
            batches.append(labels)
        else:
            print ("Batching labels to sizes of {0}".format(LabelBatchSize))
            for currentPos in range(0, len(labels), LabelBatchSize):
                batches.append(labels[currentPos : currentPos + LabelBatchSize])
        for batch in batches:
            print ("Processing label batch")
            requestBody = []

            for label in batch:
                requestBody.append({"prefix": "global", "name": label})

            r = self.adapterInstance.request(
                method="POST",
                url=LABELPAGEURL,
                data=json.dumps(requestBody),
                headers=(
                    {"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL, "Content-Type": "application/json"}
                ),
                verify=False,
            )
            if r.ok:
                rJson = r.json()
                print "Complete."
            else:
                print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
        return labels

    def GetAttachmentsList(self, pageId):
        """Function to return the attachment list of attachments on page

        Args:
            pageId ([type]): [description]

        Returns:
            [type]: [description]
        """
        attachmentList = []
        BATCHSIZE = 200
        STARTVAL = 0
        while True:
            ATTACHMENTPAGEURL = (
                const.Servers.getRestUrl(const.Servers.PRODUCTION)
                + pageId
                + "/child/attachment?expand=version&limit=2000&maxsize={0}&start={1}".format(BATCHSIZE, STARTVAL)
            )
            r = self.adapterInstance.request(
                method="GET",
                url=ATTACHMENTPAGEURL,
                headers=(
                    {"X-Atlassian-Token": "no-check", "Authorization": const.CREDENTIAL, "Content-Type": "application/json"}
                ),
                verify=False,
            )

            STARTVAL = STARTVAL + BATCHSIZE
            if r.ok:
                rJson = r.json()
                results = rJson["results"]

                if len(results) == 0:
                    break
                for result in results:
                    newAttachment = ConfluenceAttachmentData()
                    newAttachment.attId = result["id"]
                    newAttachment.link = result["_links"]["download"]
                    newAttachment.version = result["version"]["number"]
                    labelsList = result["metadata"]["labels"]["results"]
                    newAttachment.container = result["_expandable"]["container"].split("/")[-1]
                    newAttachment.labels = []
                    for label in labelsList:
                        newAttachment.labels.append(label["name"])
                    newAttachment.name = result["title"]
                    attachmentList.append(newAttachment)
            else:
                print "Wiki error: Status Code: {}: {}: {}".format(r.status_code, r.reason, r.json()["message"])
        return attachmentList

    def getAttachmentViaSearch(self, attachmentName, nameSpace="", isExact=False):
        """Function to retrieve an attachmetn from a search

        Args:
            attachmentName ([type]): [description]
            nameSpace (str, optional): [description]. Defaults to "".
            isExact (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """
        SEARCH_URL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + "search?"
        if self.IsLong(attachmentName):
            SEARCH_URL = SEARCH_URL + "cql=id"
            isExact = True
        else:
            SEARCH_URL = SEARCH_URL + "cql=title"

        if isExact == False:
            SEARCH_URL = SEARCH_URL + '~"{0}" and type=attachment'.format(attachmentName)
        else:
            SEARCH_URL = SEARCH_URL + '="{0}" and type=attachment'.format(attachmentName)

        if nameSpace != "":
            SEARCH_URL = SEARCH_URL + " and space={0}".format(nameSpace)

        SEARCH_URL = self.FixSpecialCharacters(SEARCH_URL, isExact)

        SEARCH_URL = SEARCH_URL + "&expand=version"

        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
            warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

            r = self.adapterInstance.request(
                method="GET", url=SEARCH_URL, headers=({"Authorization": const.CREDENTIAL}), verify=False
            )

            rJson = r.json()

            if len(rJson["results"]) != 0:
                foundAttachment = ConfluenceAttachmentData()
                try:
                    topResult = rJson["results"][0]
                    foundAttachment.attId = topResult["id"]
                    foundAttachment.link = topResult["_links"]["download"]
                    foundAttachment.version = topResult["version"]["number"]
                    foundAttachment.container = topResult["_expandable"]["container"].split("/")[-1]
                    foundAttachment.labels = []
                    foundAttachment.name = topResult["title"]
                    return foundAttachment
                except:
                    print ("Confluence Error: Error during retrieval of attachment search {0}".format(attachmentName))
                    return ConfluenceAttachmentData()
            else:
                return ConfluenceAttachmentData()

    def getParentId(self, id):
        """Function to get the ancestors of a page and return the closest ancestor

        Args:
            id ([type]): [description]

        Returns:
            [type]: [description]
        """
        PAGEURL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + id + "?expand=ancestors"

        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
            warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

            r = self.adapterInstance.request(
                method="GET", url=PAGEURL, headers=({"Authorization": const.CREDENTIAL}), verify=False
            )

            rJson = r.json()

            try:
                parent = rJson["ancestors"][-1]
                return parent["id"]
            except:
                return None

    def getAncestorIds(self, id):
        """Function to get the ancestors of a page

        Args:
            id ([type]): [description]

        Returns:
            [type]: [description]
        """
        PAGEURL = const.Servers.getRestUrl(const.Servers.PRODUCTION) + id + "?expand=ancestors"
        ancestors = []
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=InsecureRequestWarning)  # Disable known warnings
            warnings.simplefilter("ignore", category=InsecurePlatformWarning)  # Disable known warnings

            r = self.adapterInstance.request(
                method="GET", url=PAGEURL, headers=({"Authorization": const.CREDENTIAL}), verify=False
            )

            rJson = r.json()

            try:
                for ancestor in rJson["ancestors"]:
                    ancestors.append(ancestor["id"])
                return ancestors
            except:
                return ancestors

    #   get most direct parent from Ancestor list
    def GetDirectParentfromAncestor(self, AncestorIdList, PageID):
        ParentId = None
        PageObj = self.getPageViaId(PageID)
        # check the last Ancesotr first < most common position
        ChildList = self.getChildrenListOfPage(AncestorIdList[-1], findString=None)
        if PageObj.title in ChildList:
            ParentId = AncestorIdList[-1]
        # then the first  < second most common position
        if ParentId == None:
            ChildList = self.getChildrenListOfPage(AncestorIdList[0], findString=None)
            if PageObj.title in ChildList:
                ParentId = AncestorIdList[0]
        # then everything in between  < hub might decide to randomize the order completly
        if ParentId == None:
            for i in xrange(1, (len(AncestorIdList)) - 1):
                ChildList = self.getChildrenListOfPage(AncestorIdList[i], findString=None)
                if PageObj.title in ChildList:
                    ParentId = AncestorIdList[i]
                    break
        return ParentId

    def ConvertMarkdownToWiki(self, markdown):
        """Quick function to take an input markdown string and return it formatted for confluence wiki markup

        Args:
            markdown ([type]): [description]

        Returns:
            [type]: [description]
        """
        markDownAppended = "{markdown}" + markdown + "{markdown}"
        return markDownAppended

    # 	Function to create an adapter for the other functions to use
    def adapterSetup(self):
        if self.adapterInstance is None:
            self.adapterInstance = requests.Session()
            self.adapterInstance.mount("https://", MyAdapter())

    adapterInstance = None
