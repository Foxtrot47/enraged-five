"""Exception classes for Confluence related errors, mostly for error codes returned by the REST API."""
import re

from rsg_clients.confluence import const
from rsg_clients import exceptions
from rsg_core_py.decorators import memoize
from rsg_core_py.exceptions import types


class ConfluenceException(exceptions.RestException):
    """Base Confluence Exception."""

    @memoize.memoize
    @classmethod
    def getByErrorCode(cls, errorCode):
        """Get an exception by its error code.

        Args:
            errorCode (int): error code

        Returns:
            rsg_clients.confluence.ConfluenceException
        """
        if errorCode is None:
            return

        for subclass in cls.__subclasses__():
            if subclass.ERROR_CODE == errorCode:
                return subclass


class ConfluenceServerError(ConfluenceException, exceptions.ServerError):
    """Error from status code 500."""

    ERROR_CODE = 500
    DEFAULT_MESSAGE = "Unhandled Error by the Confluence Team"


class NotAuthenticated(ConfluenceException, types.AuthenticationError):
    """Could not authenticate against the server."""

    DEFAULT_MESSAGE = "The current user is not authenticated to access the current server."


class ServerUnavailable(ConfluenceException, exceptions.ServerUnavailable):
    """Server is unavailable and can't be reached."""

    REGEX = re.compile(
        "(The CacheManager has been shut down)|(Session/EntityManager is closed)|"
        "(Can not read response from server)|(BeanCreationNotAllowedException)"
    )

    def __init__(self, server, message=None, url=None, includeErrorCode=None):
        """Overrides inherited method.

        Args:
            server (rsg_clients.confluence.servers.BaseServer): server that raised

            message (str, optional): error message to set for the error.
            url (str, optional): The url that raised the error
            includeErrorCode (bool, optional): Include the error code in the error message
        """
        exceptions.ServerUnavailable.__init__(
            self, client=const.AppInfo.NAME, server=server, message=message, url=url, includeErrorCode=includeErrorCode
        )

class MissingValue(ConfluenceException):
    """Required data to include in a payload is missing."""

    def __init__(self, message=None):
        """Overrides inherited method.

        Args:
            message (str, optional): error message to set for the error.
        """
        self.message = message or "Payload is missing required information"
        types.RockstarError.__init__(self, self.message)
