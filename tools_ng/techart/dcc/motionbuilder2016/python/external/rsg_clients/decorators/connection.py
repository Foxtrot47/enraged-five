"""Decorators for dealing with connections to a server."""
import json
import datetime
import functools
import logging
import time

from concurrent import futures
import inspect

import xmltodict
from requests import adapters

from rsg_clients import const, exceptions
from rsg_core_py.exceptions import types as exceptionTypes

LOGGER = logging.getLogger(__name__)


def authenticate(func):
    """Authenticates/login to the server when an authentication error occurs.

    Args:
        func (function): function being wrapped

    Returns:
        func
    """

    @functools.wraps(func)
    def wrapper(instance, *args, **kwargs):
        """Wrapped function.

        Args:
            instance (rsg_core_py.abstracts.abstractConnection.AbstractConnection)
        """
        try:
            return func(instance, *args, **kwargs)
        except exceptions.ConnectionAborted:
            instance.reconnect()
            return func(instance, *args, **kwargs)
        except exceptionTypes.AuthenticationError:
            instance.login()
            return func(instance, *args, **kwargs)

    return wrapper


# class is lowercase to keep up with decorator naming conventions
class resolve:
    """Wraps the get and post methods, makes sure results from the confluence instance get converted into a dictionary and
    raises the appropriate errors based on the status codes returned.
    """
    __name__ = "resolve"
    # Non-Json content types that we know we shouldn't attempt to convert to json
    NON_JSON_CONTENT_TYPES = (
        const.ContentType.XML,
        const.ContentType.VIDEO,
        const.ContentType.IMAGE,
        const.ContentType.OCTET_STREAM,
        const.ContentType.ZIP,
        const.ContentType.MSWORD,
        const.ContentType.TEXT,
        const.ContentType.RTF,
        const.ContentType.PDF,
        const.ContentType.TEXT_XML,
        const.ContentType.HTML,
        const.ContentType.TEXT,
        const.ContentType.WAV,
        const.ContentType.DOS_EXE,
        const.ContentType.HDF
    )

    def __init__(self, func):
        """Initializer.

        Args:
            func (function): the function being decorated
        """
        self.func = func

    def __call__(self, *args, **kwargs):
        """Overwrites inherited method.

        Absorbs the arguments being sent to the decorated function.

        Args:
            *args (tuple): the positional arguments passed into the decorated function
            **kargs (dict): the keyword arguments passed into the decorated function
        """
        return self.process(self.func, *args, **kwargs)

    @classmethod
    def process(cls, func, instance, url, *args, **kwargs):
        """
        Processes the results of the decorated and runs additional logic if it isn't a future object.

        Args:
            func (function): the decorated function.
            instance (rsg_clients.internals.baseConnection.BaseConnection): the instance of object that this method is being invoked from.
            url (str): string that is being passed into the get or post method:
            returnRaw (bool, optional): return the results of this query without any extra processing.
                                        This argument is not explictly set but obtained from the kwarg
            *args (tuple): list of arguments passed into the function
            *kwargs (dict): list of keyword arguments and their values passed into the function

        Returns:
            dict
        """

        if not instance.account:
            raise exceptions.NotAuthenticated("No account to authenticate with has been set yet", url=url)
        returnRaw = kwargs.pop("returnRaw", False)
        try:
            response = func(instance, url, *args, **kwargs)
        except adapters.ConnectionError as connectionError:
            message = str(connectionError)
            if "Connection aborted." in message:
                # The connection has been closed by the remote host, so we are going to attempt to reconnect
                raise exceptions.ConnectionAborted(url=url)
            raise exceptions.ServerUnavailable(
                client=instance.client, server=instance.type, message=message, url=url, connection=instance
            )
        if not isinstance(response, futures.Future):
            return cls.response(response, instance, url, returnRaw)
        # If we get a futures object back then we just return it as the data needs to be resolved after the rest call
        # has finished processing in the background.
        return response

    @classmethod
    def response(cls, response, instance=None, url=None, returnRaw=False):
        """Resolves the response obtained from the server.

        Args:
            response (requests.Response): the response from the server
            instance (rsg_core_py.abstracts.abstractConnection.AbstractConnection): The connection that was used for the request
            url (str): the url used for the request
            returnRaw (bool): returns the request object instead of the resolved json dict.

        Returns:
            dict
        """
        responseText = response.text

        if response.status_code == 204:
            return {}

        else:
            # Raise an authentication error if we catch it in the response text
            instance.raiseAuthenticationError(url, response)

        jsonData = {}
        if responseText:
            contentType = response.headers.get("Content-Type", "")

            if const.ContentType.JSON in contentType:
                jsonData = response.json()

            elif const.ContentType.XML in contentType and not returnRaw:
                jsonData = json.loads(json.dumps(xmltodict.parse(response.text)))

            elif any(contentPrefix in contentType for contentPrefix in cls.NON_JSON_CONTENT_TYPES):
                # Check for non-json/xml data being returned by the response
                if not returnRaw:
                    raise exceptions.JsonDecodeError(
                        instance.client,
                        instance.serverType,
                        url=url,
                        contentType=contentType,
                        connection=instance,
                    )
            else:
                # All default behaviors failed so we will try a custom response
                try:
                    jsonData = instance.convertResponse(response)
                except json.decoder.JSONDecodeError as exception:
                    raise exceptions.JsonDecodeError(
                        instance.client,
                        instance.serverType,
                        url=url,
                        contentType=contentType,
                        message=str(exception),
                        connection=instance,
                    )
                except Exception:
                    raise

        # dynamically resolve any errors thrown back by the response
        # Errors and responses in general should be unique to the underlying rest api that invoked them.
        instance.raiseException(url, response, jsonData)
        if returnRaw:
            return response
        return jsonData

    def __repr__(self):
        """Return the function's name."""
        return self.func.__name__

    def __get__(self, obj, objtype):
        """Support instance methods."""
        self._classObject = objtype
        return functools.partial(self.__call__, obj)


def reconnectToServer(unavailableServers, connection):
    """Attempts to reconnect to the server by login in again.

    Args:
        unavailableServers (dict): dictionary with the servers that are currently inaccessible.
        connection (rsg_core_py.abstracs.AbstractConnection): The connection to reconnect to.
    """
    counter = 0

    while True:
        try:
            availableTime = currentTimeStr()
            connection.reconnect(raiseErrors=True)
            break
        except exceptions.ServerUnavailable as exception:
            unavailableMessage(unavailableServers, exception)
            # We divide by 60 since the code sleeps for 1 minute per iteration
            counter += 1
            if not counter % 60:
                logging.warning("Attempt {} to connect failed. {}".format(counter, exception.message))
    availableMessage(unavailableServers, availableTime)


# TODO: Simplify code in Python 3 as you can have a yield and return in the same function.
class ServerUnavailable(object):
    """Class for what to do when a server connection is unavailable."""

    RAISE_ERROR = False  # This is for debugging purposes

    @classmethod
    def wait(cls, func, waitTime=None, amountOfRetries=None, *args, **kwargs):
        """Attempts to reconnect to the server until it is available again.

        Args:
            func (function): function to run that has some connection to a server.
            waitTime (float, optional): Number of seconds to wait between retries.
            amountOfRetries (int, optional): Maximum number of retries.
            args (tuple): list of arguments accepted by the decorated function
            kwargs (dict): dictionary of the keyword arguments

        Returns:
           object; the results from the decorated function
        """
        unavailableServers = {}
        counter = 0
        while True:
            try:
                return func(*args, **kwargs)
            except exceptions.ServerUnavailable as exception:
                counter += 1
                raiseError = cls.RAISE_ERROR or amountOfRetries == counter
                unavailableMessage(unavailableServers, exception, wait=raiseError is False, waitTime=waitTime)
                if raiseError:
                    raise
                reconnectToServer(unavailableServers, exception.connection)

    @classmethod
    def yieldWait(cls, func, *args, **kwargs):
        """Attempts to reconnect to the server until it is available again.

        Notes:
            This is intended to be used for functions that return generators.

        Args:
            func (function): function to run that has some connection to a server.
            args (tuple): list of arguments accepted by the decorated function
            kwargs (dict): dictionary of the keyword arguments
        """
        unavailableServers = {}
        while True:
            try:
                for each in func(*args, **kwargs):
                    yield each
                break
            except exceptions.ServerUnavailable as exception:
                unavailableMessage(unavailableServers, exception, wait=cls.RAISE_ERROR is False)
                if cls.RAISE_ERROR:
                    raise
                reconnectToServer(unavailableServers, exception.connection)


def waitForServer(waitTime=None, amountOfRetries=None):
    """Decorator for waiting for a server to come back online.

    This includes two wrapper functions to allow for accepting additional arguments. This decorator with arguments
    will return a function (outerWrapper) that will take another function (innerWrapper).

    Args:
        waitTime (float or func, optional): Number of seconds to wait between retries or the function we are decorating
        amountOfRetries (int, optional): Maximum number of retries.
    """
    if inspect.ismethod(waitTime) or inspect.isfunction(waitTime):
        # This is for cases where we have decorated a function without calling the decorator first
        func = waitTime
        waitTime = None  # Set the default value of waitTime to None since no arguments were passed in.

        @functools.wraps(func)
        def decoratorWithoutArgs(*args, **kwargs):
            """Inner wrapper function for the decorator.

            Args:
                args (tuple): list of arguments accepted by the decorated function
                kwargs (dict): dictionary of the keyword argument
            """
            return ServerUnavailable.wait(func, waitTime, amountOfRetries, *args, **kwargs)

        return decoratorWithoutArgs

    # This for when we actually pass in arguments to the decorator
    def decoratorWithArgs(func):
        """Outer wrapper function for the decorator.

        Args:
            func (function): the function to decorate
        """

        @functools.wraps(func)
        def innerWrapper(*args, **kwargs):
            """Inner wrapper function for the decorator.

            Args:
                args (tuple): list of arguments accepted by the decorated function
                kwargs (dict): dictionary of the keyword argument
            """
            return ServerUnavailable.wait(func, waitTime, amountOfRetries, *args, **kwargs)

        return innerWrapper

    return decoratorWithArgs


# TODO: In Python 3 you can use return and yield in the same code block.
def yieldWaitForServer(func):
    """Decorator for waiting for a server for to comeback online for methods that return generators.

    Args:
        func (function): the function to decorate
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        """Wrapper function for the decorator.

        Args:
            args (tuple): list of arguments accepted by the decorated function
            kwargs (dict): dictionary of the keyword arguments
        """
        for each in ServerUnavailable.yieldWait(func, *args, **kwargs):
            yield each

    return wrapper


def unavailableMessage(unavailableServers, exception, wait=True, waitTime=None):
    """Utility method for logging that server is currently unavailable.

    Args:
        unavailableServers (dict): dictionary with the servers that are currently inaccessible.
        exception (Exception): The exception being invoked.
        wait (bool, optional): Pauses the system for 60 seconds.
        waitTime (float, optional): Number of seconds to wait between retries.
    """
    if exception.name not in unavailableServers:
        unavailableServers[exception.name] = exception

        # TODO: Update email logic to use the same template for all emails
        # Send email that the server is back up
        message = ("As of {} the {} server is offline:\n" "\t{}").format(
            exception.time.strftime("%b %d %Y %H:%M:%S"), exception.name, exception.message
        )
        # LOGGER should handle sending emails
        LOGGER.warning(message)
    if wait:
        time.sleep(waitTime or 60)


def availableMessage(availableServers, availableTime):
    """Utility method for logging that server is available.

    Args:
        availableServers (dict): dictionary with the servers that are currently accessible.
        availableTime (str): the time the servers became available
    """
    if availableServers:
        message = ("As of {} the following servers are back online:\n" "\t{}").format(
            availableTime or currentTimeStr(),
            "\n\t".join(name for name in sorted(availableServers, key=str.lower)),
        )
        LOGGER.info(message)


def currentTimeStr():
    """Get the current time as a string.

    Returns:
        str
    """
    return datetime.datetime.utcnow().strftime("%b %d %Y %H:%M:%S")
