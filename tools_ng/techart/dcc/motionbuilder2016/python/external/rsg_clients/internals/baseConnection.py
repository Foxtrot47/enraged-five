import json
import time

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from requests_futures import sessions

from rsg_core.configs import main as config
from rsg_core import accounts
from rsg_core_py.abstracts import abstractConnection, abstractPayload
from rsg_clients.decorators import connection
from rsg_clients import const

# Mute no ssl certificate set warnings
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class BaseConnection(abstractConnection.AbstractConnection):
    """Adds support for basic REST procedures."""

    DEFAULT_GET_CONTENT_TYPE = const.ContentType.JSON_UTF8
    DEFAULT_GET_ACCEPT_TYPE = None
    DEFAULT_POST_CONTENT_TYPE = const.ContentType.JSON_UTF8

    def __init__(self, server):
        """Initializer.

        Args:
            server (str): name of the server we are connecting to.
        """
        self._setup(server)
        self._account = accounts.CurrentUser()  # Default account to authenticate as
        self._sslCertificate = config.system.environment.sslCertificate or config.system.environment.techartProject.sslCertificate # Default SSL Certificate to use
        self._token = None  # some rest apis require a token
        self._token_expires = None  # some tokens have expiry
        self._lastAccessedTime = None  # Some connections time out and tracking when we last logged in is important
        self._futures = None  # The future session object to use for accessing threadable objects
        self._isFutures = False

    @property
    def account(self):
        """Account to authenticate as.

        Returns:
            rsg_core.accounts.AbstractAccount
        """
        return self._account

    @account.setter
    def account(self, account):
        """Sets the account to authenticate as.

        Args:
            account (rsg_core.accounts.AbstractAccount): account to authenticate as.
        """
        if not isinstance(account, accounts.AbstractAccount):
            raise ValueError("{} is not a valid account".format(account))

        self._account = account
        self.logout()

    @property
    def session(self):
        """The session that the current user is logged in for.

        Returns:
            requests.Session
        """
        if not self._session:
            self._session = requests.Session()
            self._session.verify = self._sslCertificate
        return self._session

    @property
    def futures(self):
        """The futures session to use for sending concurrent commands.

        Returns:
            sessions.FuturesSession
        """
        if not self._futures:
            self._futures = sessions.FuturesSession(session=self.session)
        return self._futures

    @property
    def request(self):
        """Gets the proper session to run requests with."""
        if self.isFutures:
            return self.futures
        return self.session

    @property
    def isFutures(self):
        """Is this a connection currently using futures to run commands concurrently."""
        return self._isFutures

    @isFutures.setter
    def isFutures(self, value):
        """Sets if this connection should be using futures to run commands concurrently.

        Args:
            value (bool): Is this connection running processes concurrently
        """
        if value is False and self._futures is not None:
            self._futures.close()
            self._futures = None
        self._isFutures = value

    @property
    def server(self):
        """Name of the shotgun server this instance connects to."""
        return self._server

    @property
    def type(self):
        """The type of the server this instance connects to."""
        return self._type

    @connection.authenticate
    def get(self, url, contentType=None, returnRaw=False, acceptType=None, futures=False):
        """Does a get request on the given url and attempts to connect to the server if the connection timed out/disconnected.

        Notes:
            a dictionary is returned due to the connect decorator unless returnRaw is specified

        Args:
            url (str): url to query information from

            contentType (str, optional): The content type that the get command should expect. Defaults to json.
            returnRaw (bool, optional): returns the results of the query as a string.
            contentType (str, optional): The content type that the get command should expect. Defaults to data type the endpoint sends the data as.
            futures (bool, optional): perform the rest request concurrently and return the futures object to track the completion of the request

        Returns:
            dict or str or concurrent.Futures
        """
        return self._get(self, url, contentType=contentType, returnRaw=returnRaw, acceptType=acceptType, futures=futures)

    @connection.resolve
    def _get(self, url, contentType=None, returnRaw=False, acceptType=None, futures=False):
        """Does a get request on the given url and resolves any html errors into python errors.

        Args:
            url (str): url to query information from

            contentType (str, optional): The content type that the get command should expect. Defaults to json.
            returnRaw (bool, optional): returns the results of the query as a string.
                              This argument is consumed by the decorator.
            acceptType (str, optional): The content type that the get command should return. Defaults to json.
            futures (bool, optional): perform the rest request concurrently and return the futures object to track the completion of the request

        Returns:
            dict or str or concurrent.Futures
        """
        headers = {
            "Content-type": contentType or self.DEFAULT_GET_CONTENT_TYPE,
            "Accept": acceptType or self.DEFAULT_GET_ACCEPT_TYPE,
        }
        try:
            if futures:
                return self.futures.get(url, headers=headers)
            return self.session.get(url, headers=headers)
        finally:
            self._lastAccessedTime = time.time()

    @connection.authenticate
    @connection.resolve
    def post(self, url, payload=None, contentType=None, futures=False):
        """Does a post request on the given url.

        Notes:
            a dictionary is returned due to the connect decorator

        Args:
            url (str): url to query information from

            payload (str, optional): Data to provide to the post requests.
                           It must be in xml format as that is what bugstar expects.
            contentType (str, optional): The content type that this post command should expect. Defaults to json.
            futures (bool, optional): perform the rest request concurrently and return the futures object to track the completion of the request

        Returns:
            dict or str or concurrent.Futures
        """
        if isinstance(payload, abstractPayload.AbstractPayload):
            payload = payload.toString()
        headers = {"Content-type": contentType or self.DEFAULT_POST_CONTENT_TYPE}
        try:
            if futures:
                return self.futures.post(url, headers=headers, data=payload)
            return self.session.post(url, headers=headers, data=payload)

        finally:
            self._lastAccessedTime = time.time()

    @connection.authenticate
    @connection.resolve
    def put(self, url, payload=None, contentType=None, futures=False):
        """Does a put request on the given url.

        Notes:
            a dictionary is returned due to the connect decorator

        Args:
            url (str): url to query information from

            payload (str, optional): Data to provide to the post requests.
                           It must be in xml format as that is what bugstar expects.
            contentType (str, optional): The content type that this post command should expect. Defaults to json.
            futures (bool, optional): perform the rest request concurrently and return the futures object to track the completion of the request

        Returns:
            dict or str or concurrent.Futures
        """
        if isinstance(payload, abstractPayload.AbstractPayload):
            payload = payload.toString()
        headers = {"Content-type": contentType or self.DEFAULT_POST_CONTENT_TYPE}
        try:
            if futures:
                return self.futures.put(url, headers=headers, data=payload)
            return self.session.put(url, headers=headers, data=payload)
        finally:
            self._lastAccessedTime = time.time()

    @connection.authenticate
    @connection.resolve
    def delete(self, url, payload=None, contentType=None, futures=False):
        """Does a delete request on the given url.

        Notes:
            a dictionary is returned due to the connect decorator

        Arguments:
            url (str): url to query information from.

            payload (str, optional): Data to provide to the post requests.
                           It must be in xml format as that is what bugstar expects.
            contentType (str, optional): The content type that this post command should expect. Defaults to xml.
            futures (bool, optional): perform the rest request concurrently and return the futures object to track the completion of the request

        Returns:
            dict or str or concurrent.Futures
        """
        if isinstance(payload, abstractPayload.AbstractPayload):
            payload = payload.toString()
        headers = {"Content-type": contentType or const.ContentType.XML_UTF8}
        try:
            if futures:
                return self.futures.delete(url, headers=headers, data=payload)
            return self.session.delete(url, headers=headers, data=payload)
        finally:
            self._lastAccessedTime = time.time()

    def logout(self):
        """Clears the session and authentication cookies from memory."""
        if self._futures is not None:
            self._futures.close()
            self._futures = None

        if self._session is not None:
            self._session.close()
            self._session = None

        self._authenticated = False
        self._token = None
        self._token_expires = None
        self._lastAccessedTime = None

    def reconnect(self, raiseErrors=True):
        """Reconnects to the server by loging out and loging in again.

        Args:
            raiseErrors (bool): raise an error if you can't login.

        Returns:
            bool
        """
        self.logout()  # Make sure we are clearing up any existing authentication
        return self.login(raiseErrors=raiseErrors)

    def convertResponse(self, response):
        """Converts the response from the rest call to json.

        Args:
            response (requests.Response): response object returned by the requests module

        Returns:
            dict
        """
        return json.loads(response.text)

    def buildLink(self):
        """Builds a link to the given data where it can be accessed on the web or a service."""
        raise NotImplementedError

    def raiseAuthenticationError(self, url, response):
        """Raises an authentication error if the response returns an html page with said error.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server
        """
        raise NotImplemented

    def raiseException(self, url, response, data=None):
        """Raises an exception if the response returns a non-200 status code.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server
            data (dict): data extrapolated from the response as a dict
        """
        raise NotImplementedError
