""" Module for storing persistent values for the Watchstar package"""
from rsg_clients import const


class AppInfo(object):
    """Info about the clients.watchstar module."""

    NAME = "Watchstar"
    VERSION = "0.2.2"
    TITLE = "{0} (Version {1})".format(NAME, VERSION)
    DATE_TIME_FORMAT = "%Y-%m-%d"


class Servers(object):
    """Watchstar server we use."""

    PRODUCTION = "-"
    PREPRODUCTION = "-test-"
    DEVELOPMENT = "-dev-"
    MAPPINGS = {
        const.Servers.PRODUCTION: PRODUCTION,
        const.Servers.PREPRODUCTION: PREPRODUCTION,
        const.Servers.DEVELOPMENT: DEVELOPMENT,
    }

    @staticmethod
    def getServerUrl(server):
        """Gets the url path to the given server.

        Args:
            server (str): server name

        Returns:
            str
        """
        return "https://watchstar{}api".format(server)

    @classmethod
    def getRestUrl(cls, server):
        """Builds the base url for running rest commands on the given server.

        Args:
            server (str): name of the server to build the url for.

        Returns:
            str
        """
        url = "{}/v1".format(cls.getServerUrl(server))
        return url

    @classmethod
    def getType(cls, server):
        """The type of server of that is being hit (production or development).

        Args:
            server (str): the name of the watchstar server

        Returns:
            str
        """
        for key, value in cls.MAPPINGS.items():
            if server == value:
                return key


class Endpoints(object):
    """The endpoints supported by the watchstar rest api.

    Notes:
        For additional information on the available endpoints please visit:
        https://hub.gametools.dev/display/RWS/Watchstar+API
    """

    AUTH = "auth"
    TOKEN = "token"
    REFRESH_TOKEN = "refreshtoken"
    ASSET_CLASSES = "assetclasses"
