"""The watchstar servers that we connect to."""
from rsg_clients.watchstar import const, core
from rsg_core_py.abstracts import abstractServer
from rsg_core_py.metaclasses import singleton


class BaseServer(abstractServer.AbstractServer):
    """Base server class for interacting with a watchstar server."""

    NAME = None

    def __init__(self):
        """Constructor."""
        self._watchstar = core.Watchstar(self.NAME)

    def logout(self):
        """Logs out the server."""
        self._watchstar.logout()

    @property
    def instance(self):
        """The internal connection to the server if it is represented by another object.

        Returns:
            rsg_clients.watchstar.core.Watchstar
        """
        return self._watchstar

    @property
    def watchstar(self):
        """The watchstar instance to perform all the commands through.

        Returns:
            rsg_clients.watchstar.core.Watchstar
        """
        return self._watchstar

    @property
    def restUrl(self):
        """Rest url for this server.

        Returns:
            str
        """
        return self._watchstar.restUrl

    def buildUrl(self, sections, fields = None, additionalArgs = None):
        """Builds the url to request/send information through the rest api.

        Args:
            sections (list): parts of the url

            fields (dict): fields to include in the url
            additionalArgs (object): to be determined as some queries require non-standardized information

        Returns:
            str
        """
        return self.watchstar.buildUrl(sections=sections, fields=fields, additionalArgs=additionalArgs)


class Production(BaseServer):
    __metaclass__ = singleton.Singleton
    """Connection to the Watchstar Production server."""

    NAME = const.Servers.PRODUCTION
    TYPE = const.Servers.getType(NAME)


class PreProduction(BaseServer):
    __metaclass__ = singleton.Singleton
    """Connection to the Watchstar PreProduction server.

    Notes:
        Access to the dev server will need to be requested from IT.
    """

    NAME = const.Servers.PREPRODUCTION
    TYPE = const.Servers.getType(NAME)


class Development(BaseServer):
    __metaclass__ = singleton.Singleton
    """Connection to the Watchstar Development server.

    Notes:
        Access to the dev server will need to be requested from IT.
    """

    NAME = const.Servers.DEVELOPMENT
    TYPE = const.Servers.getType(NAME)


def getByName(name):
    """Gets the server based on the name.

    Args:
        name (str): name of the server to get

    Returns:
        rockstar.watchstar.servers.BaseServer
    """
    for cls in BaseServer.__subclasses__():
        if cls.NAME == name:
            return cls()
    return None


def getByType(serverType):
    """Gets the server based on the type.

    Args:
        serverType (str): type of the server to get

    Returns:
        rockstar.watchstar.servers.BaseServer
    """
    for cls in BaseServer.__subclasses__():
        if cls.TYPE == serverType:
            return cls()
    return None


production = Production()
preproduction = PreProduction()
development = Development()
