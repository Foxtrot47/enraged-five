"""Exception classes for Watchstar related errors, mostly for error codes returned by the REST API."""
import re

from rsg_clients import exceptions
from rsg_clients.watchstar import const
from rsg_core_py.decorators import memoize
from rsg_core_py.exceptions import types


class WatchstarException(exceptions.RestException):
    """Base Watchstar Exception."""

    @memoize.memoize
    @classmethod
    def getByErrorCode(cls, errorCode):
        """Get an exception by its error code.

        Args:
            errorCode (int): error code

        Returns:
            rsg_clients.watchstar.WatchstarException
        """
        if errorCode is None:
            return

        for subclass in cls.__subclasses__():
            if subclass.ERROR_CODE == errorCode:
                return subclass


class WatchstarServerError(WatchstarException, exceptions.ServerError):
    """Error from status code 500."""

    ERROR_CODE = 500
    DEFAULT_MESSAGE = "Unhandled Error by the Watchstar Team"


class NotAuthenticated(WatchstarException, types.AuthenticationError):
    """Could not authenticate against the server."""

    DEFAULT_MESSAGE = "The current user is not authenticated to access the current server."


class ServerUnavailable(WatchstarException, exceptions.ServerUnavailable):
    """Server is unavailable and can't be reached."""

    REGEX = re.compile(
        "(The CacheManager has been shut down)|(Session/EntityManager is closed)|"
        "(Can not read response from server)|(BeanCreationNotAllowedException)"
    )

    def __init__(self, server, message = None, url = None, includeErrorCode = None):
        """Overrides inherited method.

        Args:
            server (rsg_clients.watchstar.servers.BaseServer): server that raised

            message (str, optional): error message to set for the error.
            url (str, optional): The url that raised the error
            includeErrorCode (bool, optional): Include the error code in the error message
        """
        exceptions.ServerUnavailable.__init__(
            self, client=const.AppInfo.NAME, server=server, message=message, url=url, includeErrorCode=includeErrorCode
        )
