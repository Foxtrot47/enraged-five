"""Core connection logic for getting data from watchstar."""
import base64
import json
import logging

import requests

from rsg_core.configs import main as config
from rsg_clients import const as clientConst
from rsg_clients.internals import baseConnection
from rsg_clients.watchstar import const, exceptions
from rsg_core import accounts, activeDirectory
from rsg_core_py.exceptions import types

LOGGER = logging.getLogger(__name__)


class Watchstar(baseConnection.BaseConnection):
    def __init__(self, server):
        """Initializer.

        Args:
            server (str): name of the server we are connecting to.
        """
        super(Watchstar, self).__init__(server)
        self._account = accounts.CurrentUser()  # Set the current user as the default account

    def authenticate(self, password, username = None):
        """Authenticates the user against the server.

        Args:
            password (str): password encoded in base64 for the account being accessed.

            username (str, optional): the name of the user to authenticate as. Defaults to the current account set.
        """
        username = username or self.account.name
        authEndpoint = const.Endpoints.REFRESH_TOKEN
        payload = None

        if not self._authenticated:
            # Get the front portion of the email of the user since watchstar uses that for authentication
            email = activeDirectory.DirectoryServices().getEmailAddress(username)
            if email and not email.startswith(username):
                username = email.split("@", 1)[0]
            # Try to get email:
            payload = json.dumps({"userName": username, "password": base64.b64decode(password).decode()})
            authEndpoint = const.Endpoints.TOKEN

        # We are adding a / at the end to get a nice error
        result = self.session.get(
            "{}/".format(self.restUrl), headers={"Content-type": clientConst.ContentType.JSON}, verify=False
        )
        url = self.buildUrl([const.Endpoints.AUTH, authEndpoint])
        result = self.session.post(
            url,
            data=payload,
            headers={"Content-type": clientConst.ContentType.JSON_UTF8},
            verify=config.system.environment.sslCertificate or config.system.environment.techartProject.sslCertificate,
        )

        if result.status_code >= 300:
            self._authenticated = False
            raise types.AuthenticationError()

        accessToken = result.json()["accessToken"]
        self.session.headers["authorization"] = "Bearer {}".format(accessToken)
        self._authenticated = True

    @property
    def restUrl(self):
        """Convenience method for getting the server url with the rest endpoint."""
        return const.Servers.getRestUrl(self._server)

    def raiseAuthenticationError(self, url, response):
        """Raises an authentication error if the response returns an html page with said error.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server

        Raises:
            NotAuthenticated
        """
        if "<!doctype html>" in response.text and response.status_code == 401:
            raise exceptions.NotAuthenticated(
                "Basic Authentication Failure - Reason : AUTHENTICATED_FAILED", url=url, errorCode=response.status_code
            )

    def raiseException(self, url, response, data):
        """Raises an exception if the response returns a non-200 status code.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server
            data (dict): data extrapolated from the response as a dict

        Raises:
            WatchstarException
        """
        exceptionClass = exceptions.WatchstarException.getByErrorCode(response.status_code)
        if 300 <= response.status_code and not exceptionClass:
            exceptionClass = exceptions.WatchstarException

        if exceptionClass is not None:
            # Get the correct error message, based on the response it may require different keys
            errorDict = data.get("ResponseHeader", data).get("Errors", {})
            errorMessage = errorDict.get("messages", errorDict.get("#text", [""]))[0]

            if response.status_code == exceptions.WatchstarServerError.ERROR_CODE:  # This the generic 500 error code
                if exceptions.ServerUnavailable.REGEX.search(errorMessage):
                    raise exceptions.ServerUnavailable(server=self.type, message=errorMessage, url=url)

            raise exceptionClass(errorMessage, url=url, errorCode=response.status_code)
