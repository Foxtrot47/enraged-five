"""Test module for debugging the basic interface for interacting with data on Watchstar."""
import unittest

from rsg_clients.watchstar import servers
from rsg_core import accounts


class TestConnection(unittest.TestCase):
    """Tests basic connection functionality."""

    SERVER = None  # Server to use for this test

    @classmethod
    def setUpClass(cls):
        """Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.TechArt()

    def test_Connection(self):
        """Test if you can connect to the server."""
        self.assertTrue(self.SERVER.watchstar.login())
