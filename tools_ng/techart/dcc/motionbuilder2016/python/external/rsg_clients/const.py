class AppInfo(object):
    NAME = "Clients"
    VERSION = "0.15.16"
    TITLE = "{0} (Version {1})".format(NAME, VERSION)


class Servers(object):
    """The server classification of servers that we most commonly use."""

    PRODUCTION = "production"
    PREPRODUCTION = "preproduction"
    DEVELOPMENT = "development"


class Clients(object):
    """The clients supported by this library."""

    SHOTGUN = "shotgun"
    BUGSTAR = "bugstar"
    PERFORCE = "perforce"
    MONGO = "mongo"
    JENKINS = "jenkins"
    CONFLUENCE = "confluence"
    WATCHSTAR = "watchstar"


class ContentType(object):
    """The type of of content that a REST command should expect or return.

    They are included in the header of the rest call.
    """

    # Prefix Sections
    APPLICATION = "application"
    VIDEO = "video"
    IMAGE = "image"

    # Valid Content Types
    XML = "application/xml"
    JSON = "application/json"
    URL_ENCODED = "application/x-www-form-urlencoded"
    OCTET_STREAM = "application/octet-stream"
    ZIP = "application/zip"
    MSWORD = "application/msword"
    DOS_EXE = "application/x-dosexec"
    TEXT = "text/plain"
    TEXT_XML = "text/xml"
    HTML = "text/html"
    RTF = "text/rtf"
    PDF = "application/pdf"
    WAV = "audio/x-wav"
    HDF = "application/x-hdf"

    # Content Types with extended formatting requirements
    XML_UTF8 = "application/xml; charset=utf-8"
    JSON_UTF8 = "application/json; charset=utf-8"
