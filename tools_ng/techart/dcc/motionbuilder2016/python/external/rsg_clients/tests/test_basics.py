"""
Test module for debugging the basic interface for interacting with bugs on Bugstar
"""
import unittest

from rsg_core import accounts
from rsg_clients import managers
from rsg_clients.shotgun import const as shotgunConst
from rsg_clients.shotgun import accounts as shotgunAccounts
from rsg_clients.bridges.integrations import shotgun, bugstar


class TestTasks(unittest.TestCase):
    SHOTGUN_BRIDGE = None
    MANAGER = None

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection when the class is invoked the first time
        """
        cls.MANAGER = managers.Servers()
        cls.MANAGER.setShotgun(cls.MANAGER.DEVELOPMENT, shotgunAccounts.UnitTests())
        cls.MANAGER.setBugstar(cls.MANAGER.DEVELOPMENT, accounts.TechArt())
        cls.SHOTGUN_BRIDGE = shotgun.BugstarBridge(shotgun=cls.MANAGER.shotgun, bugstar=cls.MANAGER.bugstar)

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.MANAGER.shotgun.logout()
        cls.MANAGER.bugstar.logout()
        cls.BRIDGE = None

    def tearDown(self):
        self.MANAGER.shotgun.task._clearCache(True)
        self.MANAGER.bugstar.bug._clearCache(True)

    def test_A01_ConvertTaskWithAttachment(self):
        task = self.MANAGER.shotgun.task.getByBugNumber(5381704)  # Python test Task.
        bug = self.SHOTGUN_BRIDGE.convertTask(task)

        task.refresh()  # Pick up latest changes from server to get bugstar ID changes on attachments
        taskAttachments = [attachment.bugstarId for attachment in task.versions]

        self.assertIsInstance(bug, self.MANAGER.bugstar.bug.cls(), "Bug created is not of an instance bug.")
        self.assertIsNotNone(bug.attachmentList, "There are no attachments attached to the converted bug on Bugstar. ")

        for attachment in bug.attachmentList:
            self.assertIsInstance(
                attachment,
                self.MANAGER.bugstar.attachment.cls(),
                "Attachments on the bug are not of the instance Attachment.",
            )
            self.assertIn(
                attachment.id,
                taskAttachments,
                "Bug contains attachment that doesn't exist in the shotgun task. Attachment ID:{}".format(
                    attachment.id
                ),
            )


class TestBugs(unittest.TestCase):
    SHOTGUN_BRIDGE = None
    MANAGER = None

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection when the class is invoked the first time
        """
        cls.MANAGER = managers.Servers()
        cls.MANAGER.setShotgun(cls.MANAGER.DEVELOPMENT, shotgunAccounts.UnitTests())
        cls.MANAGER.setBugstar(cls.MANAGER.DEVELOPMENT, accounts.TechArt())
        cls._BRIDGE = bugstar.ShotgunBridge(shotgun=cls.MANAGER.shotgun, bugstar=cls.MANAGER.bugstar)

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.MANAGER.shotgun.logout()
        cls.MANAGER.bugstar.logout()
        cls.BRIDGE = None

    def tearDown(self):
        self.MANAGER.shotgun.task._clearCache(True)
        self.MANAGER.bugstar.bug._clearCache(True)

    def test_A01_ConvertBug(self):
        bug = self.MANAGER.bugstar.bug(5399725)
        task = self._BRIDGE.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls())

    def test_A01_ConvertBugWithGoal(self):
        bug = self.MANAGER.bugstar.bug(5828374)
        task = self._BRIDGE.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls())

    def test_A01_ConvertBugWithAttachment(self):
        bug = self.MANAGER.bugstar.bug(5381704)  # Python test bug.

        bugAttachments = [attachment.id for attachment in bug.attachmentList]

        task = self._BRIDGE.convertBug(bug)
        self.assertIsInstance(task, self.MANAGER.shotgun.task.cls(), "Task created is not of an instance task.")
        self.assertIsNotNone(task.versions, "There are no attachments on the task created in shotgun. ")
        # Check that there are the same numbers of attachments.
        self.assertEqual(
            len(bugAttachments),
            len(task.versions),
            "Number of the attachments on the bug doesn't match the number of task versions in Shotgun",
        )
        for version in task.versions:
            self.assertIsInstance(
                version, self.MANAGER.shotgun.version.cls(), "Versions on the task are not of the instance Version."
            )
            self.assertIn(
                version.bugstarId,
                bugAttachments,
                "Task contains attachment that doesn't exist in the bugstar bug. Attachment ID: {}".format(
                    version.bugstarId
                ),
            )
