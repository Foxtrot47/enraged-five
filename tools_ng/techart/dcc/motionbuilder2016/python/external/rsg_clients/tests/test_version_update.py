import unittest
import os
import sys
import logging
import tempfile
import filecmp

from rsg_clients.shotgun import servers, accounts, exceptions, const

# set up logging (only print to stdout)
root = logging.getLogger()
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler.setFormatter(formatter)
root.addHandler(handler)

class TestVersions(unittest.TestCase):

    entitiesToDelete = []

    fpath0 = os.path.join(const.Paths.RESOURCES_DIRECTORY, "tests", "testImage.jpg")
    fpath1 = os.path.join(const.Paths.RESOURCES_DIRECTORY, "tests", "testImage2.jpg")

    @classmethod
    def setUpClass(cls):
        for fpath in [cls.fpath0, cls.fpath1]:
            if not os.path.isfile(fpath):
                print("No file at {0}. Exiting testing.".format(fpath))
                raise Exception("Unable to continue with testing. No file exists at {0}".format(fpath))

        cls.server = servers.development
        cls.server.account = accounts.UnitTests()
        cls.defaultProject = cls.server.project.getByName("Sandbox")

    def setUp(self):
        self.entitiesToDelete = []

        self.testEntity = self.server.photogrammetry()
        self.testEntity.project = self.defaultProject
        self.testEntity.name = "unit test entity"
        self.testEntity.create()
        self.entitiesToDelete.append(self.testEntity)

    def tearDown(self):
        print("deleting {0} entities".format(len(self.entitiesToDelete)))
        for entity in self.entitiesToDelete:
            # this is in a try/catch so we don't create entities, not delete them, and then not inform the user.
            try:
                entity.delete()
            except Exception as e:
                print("Unable to delete entity {0}. {1}: {2}".format(entity, type(e), e))

    def test_upload_version(self):
        """Test that a version is uploaded correctly."""

        # attach a version to it
        newVersion = self.testEntity.uploadVersion(self.fpath0)
        self.entitiesToDelete.append(newVersion)

        # make sure it got there
        self.assertEqual(len(self.testEntity.versions), 1)
        self.assertIsNotNone(newVersion.attachment)

    def test_upload_thumbnail(self):
        """Test that a thumbnail is uploaded correctly, based on the absence, and then presence, of a thumb url."""

        # attach a thumbnail'd version to it
        thumbVersion = self.testEntity.uploadThumbnail(self.fpath0)
        self.entitiesToDelete.append(thumbVersion)

        # make sure it got there, and it's handled right
        self.assertEqual(len(self.testEntity.versions), 1)
        self.assertIsNotNone(thumbVersion.attachment)
        self.assertTrue(thumbVersion.displayAsThumbnail)

    def test_download_version(self):
        """Test that a version attachment is downloaded correctly."""

        # upload a version
        newVersion = self.testEntity.uploadVersion(self.fpath0)
        self.entitiesToDelete.append(newVersion)

        # download it, and check for equality
        tempDir = tempfile.TemporaryDirectory().name
        newFPath = newVersion.downloadAttachment(tempDir)

        # make sure the files are equal
        self.assertTrue(filecmp.cmp(newFPath, self.fpath0))

    def test_thumbnail_url(self):
        """Test that a version-based thumbnail generates a thumbnail url."""
        # make sure the url starts out empty
        with self.assertRaises(exceptions.ThumbnailDoesNotExist):
            dummyVariable = self.testEntity.thumbnailUrl

        # now upload a thumbnail
        thumbVersion = self.testEntity.uploadThumbnail(self.fpath0)
        self.entitiesToDelete.append(thumbVersion)

        self.assertIsNotNone(self.testEntity.thumbnailUrl)

    def test_overwrite_thumbnail(self):
        """Test that overwriting the existing thumbnail works."""
        # upload one thumbnail to the entity
        origThumb = self.testEntity.uploadThumbnail(self.fpath0)
        self.entitiesToDelete.append(origThumb)

        # upload another, overwrite set to True
        self.testEntity.uploadThumbnail(self.fpath1, overwrite=True)

        # make sure there's still only one version on that entity
        self.assertEqual(len(self.testEntity.versions), 1)

    def test_no_overwrite_thumbnail(self):
        """Test that not overwriting the existing thumbnail works.

        It should set the existing thumbnail version's displayAsThumbnail to False, create a new version with the
        given path, and then set that version's displayAsThumbnail to True.
        """
        # upload one thumb to the entity
        origThumb = self.testEntity.uploadThumbnail(self.fpath0)
        self.entitiesToDelete.append(origThumb)

        # upload another, overwrite set to false
        otherThumb = self.testEntity.uploadThumbnail(self.fpath1, overwrite=False)
        self.entitiesToDelete.append(otherThumb)

        # make sure there are now TWO version on that entity
        self.assertEqual(len(self.testEntity.versions), 2)

if __name__ == "__main__":
    unittest.main()