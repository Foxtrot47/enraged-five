"""Context manager for managing that the Futures Session object is open """


class Futures:
    CONNECTION_COUNT = {}

    def __init__(self, connection):
        """Initializer.

        Args:
            connection (rsg_clients.internals.baseConnection.BaseConnection): The connection to enable futures on.
        """
        self.connection = connection
        self.CONNECTION_COUNT.setdefault(connection.uuid, 0)

    @classmethod
    def isOn(cls, connection):
        """Is this context manager currently being used for this connection.

        Args:
            connection (rsg_clients.internals.baseConnection.BaseConnection): The connection to enable futures on.

        Returns:
            bool
        """
        return cls.CONNECTION_COUNT.setdefault(connection.uuid, 0) < 0

    def __enter__(self):
        """Overrides inherited method.

        Logic to perform when the context manager is started
        """
        if not Futures.isOn(self.connection):
            self.connection.isFutures = True
        Futures.CONNECTION_COUNT[self.connection.uuid] += 1
        return self.connection

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Overrides inherited method.

        Logic to perform when the context manager is exited.
        """
        Futures.CONNECTION_COUNT[self.connection.uuid] -= 1
        if not self.isOn(self.connection):
            self.connection.isFutures = False
