"""Reusable managers for using specific objects."""
import sys
import importlib

from rsg_clients import const

from rsg_core_py.metaclasses import singleton
from rsg_clients.bugstar import servers as bugstarServers
from rsg_clients.shotgun import servers as shotgunServers
from rsg_clients.confluence import servers as confluenceServers
from rsg_clients.perforce import const as perforceConst, servers as perforceServers
from rsg_core.database import databases


# TODO: Add perforce support
class Servers(object):
    """Manager for holding different servers a process should use.

    This allows for support for connecting to different types of servers for the lifetime of the application/process.

    Examples:
        from rsg_core import accounts, managers
        from rsg_core.shotgun import const as shotgunConst

        # Setup connection manager
        # This is cached based on the name incase you need to access different connections for different
        # tools running within the same process
        connections = managers.Servers("Title of the tool that uses this")

        # Setup the Bugstar Connection
        # First parameter is the server 'type' to connect to
        # Second Argument is the account to authenticate with.

        connections.setBugstar(connections.DEVELOPMENT, accounts.TechArt())

        # Setup A Shotgun Connection
        # Second Argument for the Shotgun connection has to be a script key

        connections.setShotgun(connections.DEVELOPMENT, shotgunConst.Scripts.CORE)

        # Set as a global or pass into process that will use this
        # the bugstar connection is accessed by using connections.bugstar
        # the shotgun connection is accessed by using connections.shotgun
        # Support for P4 and other connections type to be implemented at a later date
    """

    __metaclass__ = singleton.Singleton

    # Common Connection/Server Types
    PRODUCTION = const.Servers.PRODUCTION
    PREPRODUCTION = const.Servers.PREPRODUCTION
    DEVELOPMENT = const.Servers.DEVELOPMENT

    # Frequently used connections
    SHOTGUN = const.Clients.SHOTGUN
    BUGSTAR = const.Clients.BUGSTAR
    PERFORCE = const.Clients.PERFORCE
    MONGO = const.Clients.MONGO
    JENKINS = const.Clients.JENKINS
    CONFLUENCE = const.Clients.CONFLUENCE

    # TODO: Update the databases class to use a format
    _MONGO_MAPPINGS = {PRODUCTION: databases.NycMongoProd.NAME, DEVELOPMENT: databases.NycMongoDev.NAME}

    # TODO: Move the P4 code into clients
    _PERFORCE_MAPPINGS = {
        PRODUCTION: perforceConst.Servers.GAME,
        DEVELOPMENT: perforceConst.Servers.DEVELOPMENT,
        perforceServers.Mocap.NAME: perforceConst.Servers.MOCAP,
    }

    def __init__(self):
        """Sets up the basic connections required by modules used by the event daemon plugins.

        These connections should only be setup when the daemon is started.

        Args:
            name (str): name of the process
        """
        self._connections = {}
        self._bridges = Bridges(self)
        # TODO: See if it is worth to add watchstar and p4 server access here

    @property
    def bridges(self):
        """The manager for the different bridges accessible with these connections.

        Returns:
            Bridges
        """
        return self._bridges

    @classmethod
    def getConnectionType(cls, connection):
        """Gets the type of connection this server is.

        Args:
            connection (rsg_core.abstractServer.AbstractServer): the connection whose type to get

        Returns:
            str
        """
        mappings = {}
        if isinstance(connection, (shotgunServers.BaseServer, bugstarServers.BaseServer)):
            return connection.TYPE

        elif isinstance(connection, databases.BaseConnection):
            mappings = cls._MONGO_MAPPINGS

        for key, value in mappings.iteritems():
            if value == connection.NAME:
                return key

    def activeServers(self):
        """Gets all the currently used servers.

        Returns:
            list of rsg_core_py.abstracts.abstractConnection.AbstractConnection
        """
        return [connection for connection in self._connections.values()]

    def clearCaches(self):
        """Clears caches from the servers currently set for use."""
        # Clear out the cache between scans to ensure the latest data is getting grabbed.
        for server in self.activeServers():
            if hasattr(server, "clearCache"):
                try:
                    server.clearCache()
                except NotImplementedError:
                    continue

    def getConnection(self, name):
        """Gets a connection based on its name.

        Args:
            name (str): name of the connection

        Returns:
            rsg_core_py.abstracts.abstractServer
        """
        return self._connections.get(name, None)

    def setConnection(self, name, connection):
        """Stores a connection internally to reference later.

        Args:
            name (str): name for the connection
            connection (rsg_core_py.abstracts.abstractServer): connection to store
        """
        self._connections[name] = connection

    @property
    def shotgun(self):
        """Convenience method for getting the standard shotgun connection.

        If the connection hasn't been explicitly set, it defaults to the production server and connects using the
        Core shotgun script key

        Returns:
            rsg_clients.shotgun.servers.BaseServer
        """
        connection = self.getConnection(self.SHOTGUN)
        if not connection:
            connection = self.setShotgun(self.PRODUCTION)
        return connection

    @property
    def bugstar(self):
        """Convenience method for getting the standard bugstar connection.

        If the connection isn't explicitly set, it defaults to the production server and connects using the credentials
        of the user currently logged in to the machine.

        Returns:
            rsg_clients.bugstar.servers.BaseServer
        """
        connection = self.getConnection(self.BUGSTAR)
        if not connection:
            connection = self.setBugstar(self.PRODUCTION)
        return connection

    @property
    def confluence(self):
        """Convenience method for getting the standard confluence connection.

        If the connection isn't explicitly set, it defaults to the production server and connects using the credentials
        of the user currently logged in to the machine.

        Returns:
            rsg_clients.confluence.servers.BaseServer
        """
        connection = self.getConnection(self.CONFLUENCE)
        if not connection:
            connection = self.setConfluence(self.PRODUCTION)
        return connection

    @property
    def mongo(self):
        """Convenience method for getting the standard mongo connection.

        If the connection isn't explicitly set, it defaults to the production server and connects using the credentials
        of the user currently logged in to the machine.
        """
        connection = self.getConnection(self.MONGO)
        if not connection:
            connection = self.setMongo(self.PRODUCTION)
        return connection

    @property
    def perforce(self):
        """Convenience method for getting the standard perforce connection.

        If the connection isn't explicitly set, it defaults to the production server and connects using the credentials
        of the user currently logged in to the machine.
        """
        connection = self.getConnection(self.PERFORCE)
        if not connection:
            connection = self.setPerforce(self.PRODUCTION)
        return connection

    def setShotgun(self, name=None, apiAccount=None):
        """Builds and sets the standard shotgun connection to use.

        Defaults to the production server using the core api account

        Keyword Args:
            name (str): name of the shotgun server
            apiAccount (rsg_clients.shotgun.accounts.AbstractApiKey, optional): The api name and key to access Shotgun with.
        """
        shotgun = shotgunServers.getByType(name)
        if shotgun is None:
            raise ValueError("{} is not a valid shotgun server to use".format(name))
        shotgun.account = apiAccount or shotgun.account
        self.setConnection(self.SHOTGUN, shotgun)
        return shotgun

    def setBugstar(self, name=None, account=None):
        """Builds and sets the standard bugstar connection to use.

        Defaults to the production server using the current user for authentication

        Keyword Args:
            name (str): type of the bugstar server
            account (rsg_core.accounts.AbstractAccount, optional): The account to access Bugstar with. Defaults to the current user.
        """
        bugstar = bugstarServers.getByType(name)
        if bugstar is None:
            raise ValueError("{} is not a valid bugstar server to use".format(name))
        bugstar.account = account or bugstar.account
        self.setConnection(self.BUGSTAR, bugstar)
        return bugstar

    def setConfluence(self, name=None, account=None):
        """Builds and sets the standard confluence connection to use.

        Defaults to the production server using the current user for authentication

        Keyword Args:
            name (str): type of the confluence server
            account (rsg_core.accounts.AbstractAccount, optional): The account to access Confluence with. Defaults to the current user.
        """
        confluence = confluenceServers.getByType(name)
        if confluence is None:
            raise ValueError("{} is not a valid confluence server to use".format(name))
        confluence.account = account or confluence.account
        self.setConnection(self.CONFLUENCE, confluence)
        return confluence

    def setMongo(self, name=None, account=None):
        """Builds and sets the standard mongo connection to use.

        Defaults to the production server using the current user for authentication

        Keyword Args:
            name (str): name of the mongo server
            account (rsg_core.accounts.AbstractAccount, optional): The account to access Mongo with.
                                                              This is currently unused for Mongo connections.
        """
        serverName = self._MONGO_MAPPINGS.get(name, None)
        if serverName is None:
            raise ValueError("{} is not a valid mongo server to use".format(name))
        mongo = databases.getByName(serverName)
        self.setConnection(self.MONGO, mongo)
        return mongo

    def setPerforce(self, name=None, account=None):
        """Builds and sets the standard perforce connection to use.

        Defaults to the production server using the current user for authentication

        Keyword Args:
            name (str): name of the perforce server
            account (rsg_core.accounts.AbstractAccount, optional): The account to access Perforce with.
                                                              This is currently unused for Perforce connections.
        """
        serverName = self._PERFORCE_MAPPINGS.get(name, None)
        if serverName is None:
            raise ValueError("{} is not a valid perforce server to use".format(name))
        perforce = perforceServers.getByName(serverName)
        self.setConnection(self.PERFORCE, perforce)
        return perforce


class Bridges(object):
    """Manager for the bridges that can be accessed by the connections manager."""

    def __init__(self, connections):
        """Constructor.

        Args:
            connections (Servers): connections to build bridges with
        """
        self._connections = connections
        self._mappings = {}
        self._integrations = ["rsg_clients.bridges.integrations"]

    def addIntegration(self, packageName):
        """Name of the package with the integrations to add.

        Args:
            packageName: name of the the package with the integrations to use
        """
        if packageName not in self._integrations:
            self._integrations.append(packageName)

    def removeIntegration(self, packageName):
        """Name of the package with the integrations to remove.

        Args:
            packageName: name of the the package with the integrations to use
        """
        if packageName in self._integrations:
            self._integrations.remove(packageName)

    def getBridge(self, source, target):
        """Gets a bridge for the given source and target pairing.

        Args:
            source (str): the service to get data from
            target (str): the service to convert data to

        Returns:
            rsg_clients.bridges.integrations.abstractBridge.AbstractBridge
        """
        key = (source, target)
        bridge = self._mappings.get(key, None)
        if not bridge:
            if not self._integrations:
                # TODO: Add custom error
                raise ValueError("No integrations set")

            for packageName in self._integrations:
                moduleName = "{}.{}".format(packageName, source)
                module = sys.modules.get(moduleName, None)
                if not module:
                    try:
                        module = importlib.import_module(moduleName)
                        break
                    except ImportError as exception:
                        pass

            if not module:
                raise exception

            cls = module.getByType(target)
            bridge = cls(self._connections.getConnection(source), self._connections.getConnection(target))
            self._mappings[key] = bridge
        return bridge

    @property
    def shotgunToBugstar(self):
        """Bridge for converting shotgun data to bugstar data.

        Returns:
             rsg_clients.bridges.integrations.shotgun.BugstarBridge
        """
        return self.getBridge(self._connections.SHOTGUN, self._connections.BUGSTAR)

    @property
    def bugstarToShotgun(self):
        """Bridge for converting bugstar data to shotgun data.

        Returns:
             rsg_clients.bridges.integrations.shotgun.BugstarBridge
        """
        return self.getBridge(self._connections.BUGSTAR, self._connections.SHOTGUN)
