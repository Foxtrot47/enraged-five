"""
Decorators that deal with rest connection and queries
"""
import functools
from rsg_clients.bugstar import exceptions, const


def project(func):
    """
    Wraps around the get and post methods to return project specific urls.
    This is mostly required for V1 requests.

    Args:
        func (function): function being wrapped

    Returns:
        func
    """

    @functools.wraps(func)
    def wrapper(project, commands, fields=None, *args, **kwargs):
        if project.id is None:
            raise exceptions.InvalidProject()
        fields = fields or {}
        sections = [project.server.restUrl, const.Endpoints.V1, const.Endpoints.PROJECTS, project.id]

        if project.server.delegate is not None:
            fields[const.QueryFields.AS_USER] = project.server.delegateUser.id

        if kwargs.get("isV2", None):
            sections[1] = const.Endpoints.V2
            sections[2:2] = commands or []  # project information goes at the end
        else:
            sections.extend(commands or [])
        return func(project, sections, fields, *args, **kwargs)

    return wrapper


def resolveProject(func):
    """
    Resolve the correct project when it has changed on the server and currently cached project is incorrect.

    Arguments:
        isGet (bool): is the operation being decorated a get or post command

    Returns:
        func
    """

    @functools.wraps(func)
    def wrapper(bug, *args, **kwargs):
        try:
            return func(bug, *args, **kwargs)
        except exceptions.ProjectChanged as exception:
            bug._setattr(const.Fields.PROJECT_ID, exception.currentId, refresh=False)
            return func(bug, *args, **kwargs)

    return wrapper
