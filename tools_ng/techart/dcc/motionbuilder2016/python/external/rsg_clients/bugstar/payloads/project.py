from xml.etree import cElementTree as xml

from rsg_clients.bugstar import const
from rsg_clients.bugstar.decorators import connection
from rsg_clients.bugstar.payloads import basePayload


class Project(basePayload.BasePayload):
    """
    class for getting project informations from bugstar

    const.Fields.ID
    const.Fields.ACCOUNTING_TYPE
    const.Fields.ACTIVE
    const.Fields.ACTIVE_FOR_SEARCH
    const.Fields.AUTOFILL_BUG_OWNER
    const.Fields.COMPLETED
    const.Fields.CREATED_AT
    const.Fields.CREATED_BY
    const.Fields.DESCRIPTION
    const.Fields.HOST_STUDIO
    const.Fields.IS_ACTIVE
    const.Fields.IS_VERSIONED
    const.Fields.MODIFIED_AT
    const.Fields.MODIFIED_BY
    const.Fields.NAME
    const.Fields.PROJECT_VERSION
    const.Fields.START_DATE
    const.Fields.TYPE
    const.Fields.USES_IMPORTANCE_SYSTEM
    const.Fields.VERSION
    """

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element("Project")
        for key, value in self._fields.iteritems():
            if value is None:
                continue
            valueElement = xml.Element(key)

            if isinstance(value, (self.server.user._CLASS, self.server.team._CLASS)):
                valueElement.text = str(value.id)
            else:
                valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        self._fields.update(dictionary)

    def isDefaultProject(self):
        """ Is this project the default project """
        return self == self.server.defaultProject()

    @property
    def name(self):
        """ Name of the project """
        return self._getattr(const.Fields.NAME)

    @connection.project
    def get(self, commands, fields=None, contentType=None, isV2=None):
        """
        Convenience method for doing a get requests with the current project id included in the uri

        Arguments:
            commands (list): list of commands to pass into the url
            fields (dict): fields to include in the url
            contentType (str): The content type that this post command should expect.
                                  Defaults to json for get commands and xml for a post commands.
            isV2 (bool): Is this a bugstar rest api version 2 style query

        Returns:
            list of basePayload.BasePayload
        """
        url = self.server.buildUrl(commands, fields=fields)
        return self.server.get(url, contentType=contentType)

    @connection.project
    def post(self, commands, fields=None, payload=None, contentType=None, isV2=None):
        """
        Convenience method for doing a post requests with the current project id included in the uri

        Args:
            commands (list): list of commands to pass into the url

        Keyword Args:
            fields (dict): query fields to add to the command
            payload (AbstractPayload): data to provide to the post requests.
                             It must be in xml format as that is what bugstar expects.
            contentType (str): The content type that this post command should expect.
                                  Defaults to json for get commands and xml for a post commands.
            isV2 (bool): Is this a bugstar rest api version 2 style query

        Returns:
            list of basepayload.BasePayload
        """
        url = self.server.buildUrl(commands, fields=fields)
        return self.server.post(url, payload, contentType=contentType)

    def bug(self, developer, tester, summary, description=None, workflow=const.Workflows.REQUIRES_VERIFICATION):
        """
        Creates a bug on bugstar.

        Arguments:
            developer (str): The username of the user to set as the owner of the bug.
                             Also accepts the bugstar id of the user or the User object that represents them.
            tester (str): The username of the user to set as the qaowner of the bug.
                          Also accepts the bugstar id of the user or the User object that represents them.
            summary (str): title of the bug
            description (str): description for the bug

        Keyword Args:
            workflow (int): workflow for the bug
        """

        # TODO: Add more arguments to support additional features such as CC and tags
        if isinstance(developer, basestring):
            developer = self.server.user.getByName(developer)

        if isinstance(tester, basestring):
            tester = self.server.user.getByName(tester)

        bug = self.server.bug()
        bug.project = self
        bug.developer = developer
        bug.tester = tester
        bug.summary = summary
        bug.description = description or ""
        bug.workflow = workflow
        bug.create()
        return bug

    def toString(self, isV2=True):
        """
        Overrides inherited method

        Converts the xml element that represents this data into a string

        Arguments:
            isV2 (bool): is the data from a V2 query

        Returns:
            string
        """
        return super(Project, self).toString(isV2=isV2)


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the Project class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Project

    def all(self):
        """
        Returns all the currently accessible projects

        Yields:
            rsg_clients.bugstar.contents.Project
        """
        cls = self.cls()
        projects = cls._getData(self._server, const.Endpoints.MY, returnRaw=False)
        for data in projects.get("items", []):
            project = cls(self._server, int(data[const.Fields.ID]))
            project.parse(data, isV2=True)
            yield project

    def getByName(self, name):
        """
        Gets the project by its name.
        The project endpoint requires super admin access to use the SearchBy endpoint.

        Args:
            name (str): name of the project
        """
        for project in self.all():
            if project.name == name:
                return project
