from xml.etree import cElementTree as xml
from rsg_core_py.decorators import memoize
from rsg_clients.bugstar import const, exceptions
from rsg_clients.bugstar.payloads import basePayload


class Tag(basePayload.BasePayload):
    """
    Bugstar Payload for tags

    Properties:
        id(int): id of the attachment
        name(str): name of the file without the extension

    """

    IS_READONLY = False
    REQUIRED_CREATE_PROPERTIES = const.Fields.NAME
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element("Tag")

        for key, value in self._fields.iteritems():
            if value is None:
                continue

            valueElement = xml.Element(key)
            valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Args:
            dictionary (dict): data to parse
        Returns:
            xml.etree.cElementTree.Element
        """

        self._fields.update(dictionary)

    def toString(self, isV2=True):
        """
        Overrides inherited method

        Converts the xml element that represents this data to the string

        Args:
            isV2 (bool): data from a V2 query

        Returns:
            string
        """
        return super(Tag, self).toString(isV2=isV2)

    def create(self):
        """
        Creates a tag on the bugstar server

        Notes:
            By design the bugstar rest api does not let you create tags without assigning them to bugs.
            Once a tag has been removed from all bugs on the server it is completely removed from the server.
            To work around this design, we create tags by adding them to a place holder bug.
        """
        if self.id:
            raise exceptions.PermissionDenied(
                "Tag {} already exists on the {} server".format(self.id, self.server.NAME)
            )
        elif self.name is None:
            raise exceptions.MissingValue("Tag requires a name so that it can be created")

        # In bugstar you can't create a tag without assign it to a bug
        # We will be adding the tag to a placeholder bug so it gets added to the bugstar
        # The account attempting to create the bug will need to have access to the testing ground project
        # TODO: Remove this if statement next time that the data from prod is migrated over to dev
        bugId = 6616744

        # Get the bug to add the tag to
        bug = self.server.bug(bugId)
        if self.name in bug.tagList:
            raise exceptions.PermissionDenied(
                "Tag {} already exists on the {} server".format(self.id, self.server.NAME)
            )

        bug.tagList.append(self.name)
        # Create tag
        try:
            bug.update()
        finally:
            bug.removeEdits()

        dictionary = self._getData(self._server, "{}/{}".format(const.Endpoints.NAME, self.name))
        self.parse(dictionary.get("Tag", {}), isV2=True)
        self._cache()

    @property
    def name(self):
        """ name is readable tag property"""
        return self._getattr(const.Fields.NAME)

    @name.setter
    def name(self, value):
        """ name is writeable tag property"""
        return self._setattr(const.Fields.NAME, value)


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the User class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Tag

    @memoize.memoize
    def getByName(self, name):
        """
        Get tag data from bugstar based on the tag passed.

        Args:
            name (str): tag to look for

        Returns:
            User
        """
        dictionary = Tag._getData(self._server, "{}/{}".format(const.Endpoints.NAME, name))
        if not dictionary:
            return None

        tag = Tag(self._server, int(dictionary[const.Fields.ID]))
        tag.parse(dictionary, isV2=True)
        return tag
