from xml.etree import cElementTree as xml

from rsg_clients.bugstar import const
from rsg_clients.bugstar.payloads import basePayload


class Goal(basePayload.BasePayload):
    """
    Class that represents a goal in bugstar
    """

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element(const.Fields.GOAL)

        for key, value in self._fields.iteritems():

            if value is None:
                continue
            valueElement = xml.Element(key)

            if isinstance(value, (self.server.user.cls(), self.server.team.cls())):
                valueElement.text = str(value.id)
            else:
                valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Args:
            dictionary (dict): data to parse
        Returns:
            xml.etree.cElementTree.Element
        """
        if dictionary.get(const.Fields.ID, None):
            dictionary[const.Fields.ID] = int(dictionary[const.Fields.ID])
        self._fields.update(dictionary)

    @property
    def status(self):
        """
        status is readable bugstar property

        Returns:
            rsg_clients.bugstar.payloads.goal.Status
        """
        value = self._getattr(const.Fields.STATUS)
        if isinstance(value, dict):
            # wrap data around the status object
            value = Status(value.get(const.Fields.NAME, ""), value)
            value.__setattr__(const.Fields.STATUS, value)
        return value

    @property
    def active(self):
        """
        is the goal still active

        Returns:
            bool
        """
        return self._getattr(const.Fields.ACTIVE)

    @property
    def createdBy(self):
        """
        createdBy is readable bugstar property

        Returns:
            rsg_clients.bugstar.payloads.user.User
        """
        value = self._getattr(const.Fields.CREATED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.user(value)
        return value

    @property
    def createdAt(self):
        """
        createdAt is readable bugstar property

        Returns:
            datetime.datetime
        """
        return self._getattr(const.Fields.CREATED_AT)

    @property
    def modifiedBy(self):
        """
        modifiedBy is readable bugstar property

        Returns:
            rsg_clients.bugstar.payloads.user.User
        """
        value = self._getattr(const.Fields.MODIFIED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.user(value)
        return value

    @property
    def modifiedAt(self):
        """
        modifiedAt is readable bugstar property

        Returns:
            datetime.datetime
        """
        return self._getattr(const.Fields.MODIFIED_AT)

    @property
    def name(self):
        """
        name of the goal

        Returns:
            str
        """
        return self._getattr(const.Fields.NAME)

    @property
    def name(self):
        """
        name of the goal

        Returns:
            str
        """
        return self._getattr(const.Fields.NAME)

    @property
    def category(self):
        """
        category of the goal

        Returns:
            int
        """
        return self._getattr(const.Fields.CATEGORY)

    @property
    def description(self):
        """
        description of the goal

        Returns:
            str
        """
        return self._getattr(const.Fields.DESCRIPTION)

    @property
    def priority(self):
        """
        priority of the goal

        Returns:
            int
        """
        return self._getattr(const.Fields.PRIORITY)

    @property
    def revision(self):
        """
        revision of the goal

        Returns:
            int
        """
        return self._getattr(const.Fields.REVISION)

    @property
    def version(self):
        """
        version of the goal

        Returns:

        """
        return self._getattr(const.Fields.VERSION)


# TODO: Move to its own module, this payload is actually an enumerations payload but doesn't seem to be used widely at
# the moment and they don't have an id. At this time this solution is enough for the current use case.
class Status(basePayload.BasePayload):
    """
    Class for representing a status from a goal on bugstar
    """

    IS_REQUESTABLE = False
    IS_READONLY = True

    def __new__(cls, index, *args, **kwargs):
        """
        Overrides built-in method

        Stores & returns the instance of the class if an argument is provided, otherwise creates a new instance

        Args:
            *args (list): additional parameters accepted by the inherited class
            **kwargs (dict): additional keyword arguments accepted by the inherited class

        Keyword Args:
            index (str): the unique identifier of the status, usually its name

        Returns:
            object
        """
        return cls._getInstance(None, index)

    def __init__(self, index, data):
        """
        Initializer

        Args:
            index (str): text of the comment

        Keyword Args:
            creator (rsg_clients.bugstar.contents.User): user that created this comment
            createdAt (str): time this comment was created at
        """
        if self._isCached(None, index, self):
            return

        data = data or {}
        self._fields = data
        self.name = data.setdefault(const.Fields.NAME, index)
        self.description = data.setdefault(const.Fields.DESCRIPTION, None) or ""
        self.type = data.setdefault(const.Fields.TYPE, "")
        self.value = data.setdefault(const.Fields.VALUE, 0)

    def __eq__(self, other):
        """
        Overrides built-in method

        Adds support for matching against strings

        Args:
            other (object): object being compared to

        Returns:
            bool
        """
        if isinstance(other, Status):
            return other.name == self.name
        elif isinstance(other, basestring):
            return other == self.name
        return False

    def __ne__(self, other):
        """
        Overrides built-in method

        Adds support for not matching against strings

        Args:
            other (object): object being compared to

        Returns:
            bool
        """
        if isinstance(other, Status):
            return other.name != self.name
        elif isinstance(other, basestring):
            return other != self.name
        return True

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element(const.Fields.STATUS)

        for key, value in self._fields.iteritems():
            valueElement = xml.Element(key)
            valueElement.text = str(value)
            element.append(valueElement)
        return element


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the Goal class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Goal
