"""
Servers to access bugstar
"""
from rsg_core.configs import main as config
from rsg_core_py.abstracts import abstractServer
from rsg_core_py.decorators import memoize
from rsg_core_py.metaclasses import singleton
from rsg_clients.bugstar import const, core


class BaseServer(abstractServer.AbstractServer):
    """
    Base server class for interacting with a bugstar server
    """

    NAME = None

    def __init__(self):
        """
        Constructor
        """
        self._bugstar = core.Bugstar(self.NAME)
        self._delegate = None

    def logout(self):
        """ Logs out the server """
        self._bugstar.logout()

    @memoize.memoize
    def defaultProject(self):
        """
        Gets the bugstar project if one is available for the current project the user is under

        Returns:
            rsg_clients.bugstar.contents.Project
        """
        # If no tools project is available, exit early
        if not config.system.environment.toolsProject:
            return None

        for dlc in config.system.environment.toolsProject.dlc:
            if dlc.bugstarId:
                return self.bugstar.project(dlc.bugstarId)

        if config.system.environment.toolsProject.bugstarId:
            return self.bugstar.project(config.system.environment.toolsProject.bugstarId)

    @property
    def instance(self):
        """
        The internal connection to the server if it is represented by another object
        """
        return self._bugstar

    @property
    def bugstar(self):
        """
        The bugstar instance to perform all the commands through

        Returns:
            rsg_clients.bugstar.core.Bugstar
        """
        return self._bugstar

    @property
    def currentUser(self):
        """
        Gets the bugstar user object for the currently authenticated account

        Returns:
            rsg_clients.bugstar.contents.User
        """
        if self.account:
            return self.bugstar.user.getByName(self.account.name)

    @property
    def delegateUser(self):
        """
        The bugstar user account for the delegate that was set

        Note:
            The account used to login to Bugstar must have the required privelages to use this feature.

        Returns:
            rsg_clients.bugstar.contents.User
        """
        return self.bugstar.delegateUser

    @property
    def restUrl(self):
        """
        Rest url for this server

        Returns:
            str
        """
        return self._bugstar.restUrl

    def buildUrl(self, sections, fields=None, additionalArgs=None):
        """
        Builds the url to request/send information through the rest api

        Args:
            sections (list): parts of the url
            fields (dict): fields to include in the url
            additionalArgs (object): to be determined as some queries require non-standardized information

        Returns:
            str
        """
        return core.Bugstar.buildUrl(sections=sections, fields=fields, additionalArgs=additionalArgs)

    def get(self, url, contentType=None, returnRaw=False, acceptType=None):
        """
        Does a get request on the given url and attempts to connect to the server if the cookie timed out.

        Arguments:
            url (str): url to query information from
            contentType (str): The content type that the get command should expect. Defaults to json.
            returnRaw (bool): returns the results of the query as a string.

        Returns:
            dictionary (due to the connect decorator) or str (when returnRaw is set to True)
        """
        return self._bugstar.get(url, contentType=contentType, returnRaw=returnRaw, acceptType=acceptType)

    def post(self, url, payload=None, contentType=None):
        """
        Does a post request on the given url

        Arguments:
            url (str): url to query information from
            payload (rsg_clients.bugstar.contents.AbstractPayload): Data to provide to the post request.
            contentType (str): The content type that this post command should expect. Defaults to xml.

        Returns:
            dictionary (due to the connect decorator)
        """
        return self._bugstar.post(url, payload=payload.toString(), contentType=contentType)

    def delete(self, url, payload=None, contentType=None):
        """
        Does a delete request on the given url

        Arguments:
            url (str): url to query information from
            payload (rsg_clients.bugstar.contents.AbstractPayload): Data to provide to the post request.
            contentType (str): The content type that this post command should expect. Defaults to xml.

        Returns:
            dictionary (due to the connect decorator)
        """
        return self._bugstar.post(url, payload=payload.toString(), contentType=contentType)

    @property
    def attachment(self):
        """
        Attachment Builder

        Return:
            rsg_clients.bugstar.payloads.attachment.Builder
        """
        return self._bugstar.attachment

    @property
    def build(self):
        """
        Build Builder

        Return:
            rsg_clients.bugstar.payloads.bug.Builder
        """
        return self._bugstar.build

    @property
    def bug(self):
        """
        Bug Builder

        Return:
            rsg_clients.bugstar.payloads.bug.Builder
        """
        return self._bugstar.bug

    @property
    def component(self):
        """Component Builder."""
        return self._bugstar.component
    
    @property
    def deadline(self):
        """
        Deadline Builder

        Return:
            rsg_clients.bugstar.payloads.deadline.Builder
        """
        return self._bugstar.deadline

    @property
    def department(self):
        """
        Department Builder

        Return:
            rsg_clients.bugstar.payloads.department.Builder
        """
        return self._bugstar.department

    @property
    def gameMap(self):
        """Game Map Builder.

        Return:
            rsg_clients.bugstar.payloads.gameMap.Builder
        """
        return self._bugstar.gameMap

    @property
    def goal(self):
        """
        Goal Builder

        Return:
            rsg_clients.bugstar.payloads.goal.Builder
        """
        return self._bugstar.goal

    @property
    def globalDepartment(self):
        """
        Global Department Builder

        Return:
            rsg_clients.bugstar.payloads.globalDepartment.Builder
        """
        return self._bugstar.globalDepartment

    @property
    def grid(self):
        """Grid Builder."""
        return self._bugstar.grid

    @property
    def mission(self):
        """
        Mission Builder

        Return:
            rsg_clients.bugstar.payloads.mission.Builder
        """
        return self._bugstar.mission

    @property
    def project(self):
        """
        Project Builder

        Return:
            rsg_clients.bugstar.project.goal.Builder
        """
        return self._bugstar.project

    @property
    def search(self):
        """
        Search Builder

        Return:
            rsg_clients.bugstar.payloads.search.Builder
        """
        return self._bugstar.search

    @property
    def studio(self):
        """
        Studio Builder

        Return:
            rsg_clients.bugstar.payloads.studio.Builder
        """
        return self._bugstar.studio

    @property
    def team(self):
        """
        Team Builder

        Return:
            rsg_clients.bugstar.payloads.team.Builder
        """
        return self._bugstar.team

    @property
    def user(self):
        """
        User Builder

        Return:
            rsg_clients.bugstar.payloads.user.Builder
        """
        return self._bugstar.user

    @property
    def tag(self):
        """
        Tag Builder

        Return:
            rsg_clients.bugstar.payloads.tag.Builder
        """
        return self._bugstar.tag


class Production(BaseServer):
    __metaclass__ = singleton.Singleton
    NAME = const.Servers.REST
    TYPE = const.Servers.getType(NAME)


class PreProduction(BaseServer):
    __metaclass__ = singleton.Singleton
    NAME = const.Servers.REST_PREPROD
    TYPE = const.Servers.getType(NAME)


class Development(BaseServer):
    __metaclass__ = singleton.Singleton
    NAME = const.Servers.REST_DEV
    TYPE = const.Servers.getType(NAME)


def getByName(name):
    """
    Gets the server based on the name

    Args:
        name (str): name of the server to get

    Returns:
        rockstar.shotgun.servers.BaseServer
    """
    for cls in BaseServer.__subclasses__():
        if cls.NAME == name:
            return cls()


def getByType(serverType):
    """
    Gets the server based on the type

    Args:
        serverType (str): type of the server to get

    Returns:
        rockstar.shotgun.servers.BaseServer
    """
    for cls in BaseServer.__subclasses__():
        if cls.TYPE == serverType:
            return cls()


production = Production()
preProduction = PreProduction()
development = Development()
