from xml.etree import cElementTree as xml

from rsg_clients.bugstar import const
from rsg_clients.bugstar.payloads import basePayload, actor


class GlobalDepartment(actor.Actor):
    """
    const.Fields.ID
    const.Fields.ACTIVE
    const.Fields.CREATED_AT
    const.Fields.CREATED_BY
    const.Fields.MODIFIED_BY
    const.Fields.MODIFIED_AT
    const.Fields.NAME
    const.Fields.PARENT
    const.Fields.VERSION
    """

    _INSTANCES = {}
    _COMMAND = const.Endpoints.GLOBAL_DEPARTMENTS
    TYPE = const.Endpoints.GLOBAL_DEPARTMENTS

    def __new__(cls, bugstar, index=None, *args, **kwargs):
        """
        Overrides built-in method

        Stores & returns the instance of the class if an argument is provided, otherwise creates a new instance

        Args:
            bugstar (rsg_clients.bugstar.server.BaseServer): the local server to set for this object
            *args (list): additional parameters accepted by the inherited class
            **kwargs (dict): additional keyword arguments accepted by the inherited class

        Keyword Args:
            index (int): the id of the bug on bugstar

        Returns:
            object
        """
        return cls._getInstance(bugstar, index)

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def isInternalClass(self, value):
        """
        Checks if the object is an instance of a class used internally

        Args:
            value (object): the object to check

        Returns:
            bool
        """
        return isinstance(
            value,
            (
                self.server.user.cls(),
                self.server.team.cls(),
                self.server.studio.cls(),
                self.server.department.cls(),
                self.server.globalDepartment.cls(),
            ),
        )

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element(const.Fields.MISSION)

        for key, value in self._fields.iteritems():

            if value is None:
                continue
            valueElement = xml.Element(key)

            if self.isInternalClass(value):
                valueElement.text = str(value.id)
            else:
                valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        self._fields.update(dictionary)

    @property
    def active(self):
        """ is the department still active """
        return self._getattr(const.Fields.ACTIVE)

    @property
    def name(self):
        """ name of the department """
        return self._getattr(const.Fields.NAME)

    @property
    def parent(self):
        """ parent of this department """
        value = self._getattr(const.Fields.PARENT)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.department(value)
        return value

    @property
    def studio(self):
        """ global department equivalent of this department """
        value = self._getattr(const.Fields.STUDIO)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.studio(value)
        return value

    @property
    def members(self):
        """Gets a list of members of this department.

        Notes:
            This method is still wip, it will be changed
        """
        command = "/".join([const.Endpoints.GLOBAL_DEPARTMENTS, str(self.id), const.Endpoints.MEMBERS])
        results = self._getData(
            self.server, None, command=command
        )  # incase the given user does not belong to any teams

        return results


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the GlobalDepartment class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return GlobalDepartment
