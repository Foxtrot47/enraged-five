from xml.etree import cElementTree as xml

from rsg_clients.bugstar import const
from rsg_clients.bugstar.payloads import basePayload


class Studio(basePayload.BasePayload):
    """
    Class that represents a studio in bugstar

    Notes:
        self._fields[const.Fields.ID] = departmentId
        self._fields[const.Fields.ACTIVE] = None
        self._fields[const.Fields.ADDRESS] = None
        self._fields[const.Fields.CREATED_AT] = None
        self._fields[const.Fields.CREATED_BY] = None
        self._fields[const.Fields.MODIFIED_BY] = None
        self._fields[const.Fields.MODIFIED_AT] = None
        self._fields[const.Fields.DOMAIN] = None
        self._fields[const.Fields.NAME] = None
        self._fields[const.Fields.TAG] = None
        self._fields[const.Fields.TIMEZONE] = None
        self._fields[const.Fields.VERSION] = None
    """

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element(const.Fields.STUDIO)

        for key, value in self._fields.iteritems():

            if value is None:
                continue
            valueElement = xml.Element(key)

            if isinstance(value, (self.server.user._CLASS, self.server.team._CLASS)):
                valueElement.text = str(value.id)
            else:
                valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Args:
            dictionary (dict): data to parse
        Returns:
            xml.etree.cElementTree.Element
        """
        self._fields.update(dictionary)

    @property
    def active(self):
        """ is the studio still active """
        return self._getattr(const.Fields.ACTIVE)

    @property
    def name(self):
        """ name of the studio """
        return self._getattr(const.Fields.NAME)

    @property
    def address(self):
        """ address of the studio """
        return self._getattr(const.Fields.ADDRESS)

    @property
    def domain(self):
        """ domain of the studio """
        return self._getattr(const.Fields.DOMAIN)

    @property
    def tag(self):
        """ tag of the studio """
        return self._getattr(const.Fields.TAG)

    @property
    def timezone(self):
        """ timezone of the studio """
        return self._getattr(const.Fields.TIMEZONE)


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the Studio class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Studio
