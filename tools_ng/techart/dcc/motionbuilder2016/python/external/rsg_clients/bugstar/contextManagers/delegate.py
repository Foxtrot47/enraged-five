class AsUser(object):
    """
    Delegate the tasks being performed on Bugstar as another user.

    This requires that the account accessing Bugstar have superadmin privilages
    """

    def __init__(self, server, username):
        """
        Initializer

        Arguments:
            server (rsg_clients.bugstar.servers.BaseServer): server to set delegate on.
            username (str): username of the user to perform actions as
        """
        self._server = server
        self._username = username
        self._logger = None

    def __enter__(self):
        self._previousDelegate = self._server.delegate
        self._server.delegate = self._username

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._server.delegate = self._previousDelegate
