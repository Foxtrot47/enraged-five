import os
import tempfile
import base64
from xml.etree import cElementTree as xml

from rsg_core import conversions
from rsg_clients.bugstar import const, exceptions
from rsg_clients.bugstar.payloads import basePayload


class Attachment(basePayload.BasePayload):
    """
    Bugstar Payload for attachments

    Properties:
        id(int): id of the attachment
        filename(str): the name of the filewith the extension. This property is required when creating a new attachment.
        content(str): the contents of the file. This property is required when creating a new attachment.
        createdBy(rsg_clients.bugstar.payloads.user.User): user who added the attachment to the server
        createdAt(datetime.datetime): the time the attachment was added to the server
        name(str): name of the file without the extension
        fileSize(int): size of the file
        contentType(str): the type of content
        isArchived(bool): has this attachment been archived on the server.

    Notes:
        This payload looks for the following fields from Bugstar.

        # Id of the payload
        const.Fields.ID

        # Required fields for creation
        const.Fields.FILENAME
        const.Fields.CONTENT

        # Metadata fields from the bug it was attached to
        const.Fields.CREATED_BY
        const.Fields.CREATED_AT
        const.Fields.NAME
        const.Fields.FILE_SIZE
        const.Fields.CONTENT_TYPE
        const.Fields.IS_ARCHIVED
    """

    REQUIRED_CREATE_PROPERTIES = (const.Fields.FILENAME, const.Fields.CONTENT)

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []
    IS_READONLY = False

    def __new__(cls, bugstar, index=None, bugId=None, data=None):
        """
        Overrides built-in method

        Stores & returns the instance of the class if an argument is provided, otherwise creates a new instance

        Args:
            bugstar (rsg_clients.bugstar.server.BaseServer): the local server to set for this object

        Keyword Args:
            index (int): the id for the attachment on bugstar
            bugId (int): bug id of bug where this attachment belong to.
            data (dictionary): data that represents this object

        Returns:
            object
        """
        return cls._getInstance(bugstar, index)

    def __init__(self, bugstar, index=None, bugId=None, data=None):
        """
        Initializer

        Args:
            bugstar (rsg_clients.bugstar.server.BaseServer): the local server to set for this object

        Keyword Args:
            index (int): the id for the attachment on bugstar
            bugId (int): bug id of bug where this attachment belong to.
            data (dictionary): data that represents this object
        """
        if self._isCached(bugstar, index, self):
            if data:
                # Helps refresh the data when the data is required
                self.refresh(data=data, bugId=bugId)
            return

        # properties to store
        self._setup(index, data, bugstar)
        # Internal values
        self._bugId = bugId or None
        self._isRegistered = None not in (bugId, data) or None
        self._localFilePath = None
        self._cache()

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    @classmethod
    def _getData(cls, bug, index, returnRaw=False, fields=None):
        """
        Reimplementated method because attachments don't have a V2 Api.
        Performs a get request to the server to retrieve the information about the attachment data.

        Args:
            bug (rsg_clients.bugstar.payloads.bug): the bug this attachment is associated with
            index (str): index of the object

        Keyword Args:
            returnRaw (bool):return the data from the request as it
            fields (dictionary): the field name and its value to include in the url for the get request

        Returns:
            list
        """
        data = bug._getAttachmentData()

        attachmentList = []
        if data:
            # Return only the data about the attachment with the ID that we care about.
            attachmentList = data[const.Fields.ATTACHMENTS][const.Fields.ATTACHMENT]

        if not isinstance(attachmentList, list):
            attachmentList = [attachmentList]

        # Add data for all attachments associated with this bug to avoid wasting the excess data and making extra calls in the future
        for item in attachmentList:
            cls(bug.server, int(item[const.Fields.ID]), bugId=bug.id, data=item)
        return attachmentList

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.payloads.basePayload.BaseBuilder
        """
        return Builder

    def _getattr(self, item, refresh=True):
        """
        Get the value for the given attachment attribute

        Args:
            item (str): name of the property to access

        Keyword Args:
            refresh (bool): Populate the rest of the fields.
        """
        value = self._fields.get(item, None)

        if value or not refresh or self.id is None:
            return value

        self.refresh()

        value = self._fields.get(item, None)

        return value

    def _xmlElementV1(self):
        """
        XML representation of this payload

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element(const.Fields.ATTACHMENT)
        for key in [const.Fields.FILENAME, const.Fields.CONTENT]:
            valueElement = xml.Element(key)
            if isinstance(self._fields.get(key, None), bool):
                valueElement.text = str(self._fields.get(key, "").lower())
            else:
                valueElement.text = self._fields.get(key, None)

            element.append(valueElement)

        return element

    def _parseV1(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V1 query since V2 query doesn't exist for
        attachments yet.

        Args:
            dictionary (dict): dictionary with the data info about this instance

        Returns:
            xml.etree.cElementTree.Element
        """
        dictionary[const.Fields.NAME] = conversions.convertToAscii(dictionary.get(const.Fields.NAME, ""))
        self._fields = {
            key: (value == "true" if isinstance(value, basestring) and value in ["true", "false"] else value)
            for key, value in list(dictionary.items())
        }

    @property
    def bug(self):
        """
        The bug this attachment belongs to

        This property is not returned by the attachment rest query but is part of it
        """
        if self._bugId:
            return self.server.bug(self._bugId)

    @bug.setter
    def bug(self, value):
        """
        bug is readable attachment property

        This is used once we are out of scope of AbstractPayload class and still want to access bug that
        this attachment belongs to.
        """
        if not isinstance(value, self.server.bug.cls()):
            ValueError("{} is not a bug".format(value))
        self._bugId = value.id

    @property
    def filename(self):
        """Base name of the file that this attachment represents.

        Gets set upon creation of the attachment but doesn't exist in the attachment metadata.
        """
        return self._getattr(const.Fields.FILENAME)

    @property
    def name(self):
        """ name is readable attachment property """
        return self._getattr(const.Fields.NAME, refresh=self.isRegistered)

    @property
    def createdBy(self):
        """ createdBy is readable attachment property """
        value = self._getattr(const.Fields.CREATED_BY, refresh=self.isRegistered)
        if isinstance(value, int):
            value = self.server.actor(value)
        return value

    @property
    def createdAt(self):
        """ createdAt is readable attachment property """
        value = self._getattr(const.Fields.CREATED_AT, refresh=self.isRegistered)
        if isinstance(value, basestring):
            value = conversions.toDateTime(value)
        return value

    @property
    def contentType(self):
        """ contentType is readable attachment property """
        return self._getattr(const.Fields.CONTENT_TYPE, refresh=self.isRegistered)

    @property
    def fileSize(self):
        """ fileSize is readable attachment property """
        return self._getattr(const.Fields.FILE_SIZE, refresh=self.isRegistered)

    @property
    def isArchived(self):
        """ isArchived is readable attachment property """
        return self._getattr(const.Fields.IS_ARCHIVED, refresh=self.isRegistered)

    @property
    def isRegistered(self):
        """
        Is this attachment registered to a bug

        Returns:
            bool
        """
        if self._isRegistered is None:
            if self._bugId:
                self.refresh(bugId=self._bugId)  # Obtuse way to set the isRegistered flag if it grabs data from bugstar
            else:
                self._isRegistered = False
        return self._isRegistered

    @staticmethod
    def encodeFile(filePath):
        """
        Encode the image to byte64 codec

        Args:
            filePath (str): File path to be encoded

        Returns:
            base64 encoded image data
        """
        with open(filePath, "rb") as fileHandler:
            fileData = fileHandler.read()
            encodedImage = base64.b64encode(fileData).decode()
        return encodedImage

    def create(self):
        """ Creates a new Attachment on the server """
        if self.id is not None:
            raise exceptions.InvalidId(
                "Attachment with id {} already exists on bugstar".format(self.id), includeErrorCode=True
            )
        self._validate(self.REQUIRED_CREATE_PROPERTIES)

        # Run the rest command to create and attachment
        sections = [self.server.restUrl, const.Endpoints.V1, const.Endpoints.ATTACHMENTS, const.Endpoints.CREATE_ATTACHMENT]
        url = self.server.buildUrl(sections)
        dictionary = self.server.post(url, payload=self)
        # This url only returns the id of the newly added attachment
        # The field name is different from the standard id field used/returned by the other rest queries
        self._id = int(dictionary.get(const.Fields.ATTACHMENT).get(const.Fields.ATTACHMENT_ID))
        self._fields[const.Fields.ID] = self._id

        # Append this instance to the cache.
        self._cache()

    def copy(self):
        """
        Copies this instance

        Returns:
            rsg_clients.bugstar.payloads.attachment.Attachment
        """
        copy = super(Attachment, self).copy()
        content = copy._fields.get(const.Fields.CONTENT, None)
        filename = copy._fields.get(const.Fields.FILENAME, None)

        copy._fields = {const.Fields.CONTENT: content, const.Fields.FILENAME: filename}

        temporaryDirectory = tempfile.gettempdir()
        if not content:
            path = self.download(temporaryDirectory)
            copy.setFile(path)
            os.remove(path)
        return copy

    def refresh(self, data=None, bugId=None):
        """
        Populates the object with information from bugstar

        Keyword Args:
            data(dict): data from bugstar
            bugId (int): the bug id that the data was retrieved from
        """
        if None in (data, bugId):
            if self._isRegistered is False:  # Let None slip by so it can query the server
                raise exceptions.InvalidBug("This attachment needs to be attached to a bug to get the data")

            self._getData(self.bug, self.id)

        else:
            dictionary = data
            self._isRegistered = True
            if bugId != self._bugId:
                self._bugId = bugId
            self.parse(dictionary, isV2=False)
            self._id = self._fields.get(const.Fields.ID, None)
        self._removeStalePayloads(instance=self)

    def download(self, path):
        """
        Downloads the attachment to the specified file location with it's own name. If the file already exists,
        adds the number at the end.

        Args:
            path (str): Directory where to save the attachment

        Return:
            str
        """
        if not self.id:
            raise exceptions.InvalidId("This attachement has not been addd to the server.")

        sections = [
            self.server.restUrl,
            const.Endpoints.V2,
            const.Endpoints.ATTACHMENTS,
            const.Endpoints.DOWNLOAD,
            self.id,
        ]

        fields = None
        if self.server.delegate is not None:
            fields = {const.QueryFields.AS_USER: self.server.delegateUser.id}
        downloadUrl = self.server.buildUrl(sections, fields=fields)
        result = self.server.get(downloadUrl, contentType=self.contentType, returnRaw=True)

        downloadLocation = os.path.join(path, self.name)
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except Exception as error:
                raise exceptions.FolderCreationError(path, message=str(error))

        # To avoid overwriting the already existing files, add the number at the end if the filename already exists
        counter = 1
        while os.path.exists(downloadLocation):
            name, extension = os.path.splitext(self.name)
            downloadLocation = os.path.join(path, "{}({}){}".format(str(name), str(counter), str(extension)))
            counter += 1

        # Write the file
        with open(downloadLocation, "wb") as fileHandler:
            for chunk in result.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    fileHandler.write(chunk)

        return downloadLocation

    def connectToBug(self, bugId=None):
        """
        Connecting the current attachment to the bug associated with this

        Args:
            bugId (int): id of the bug to connect to.
                         Only valid if there isn't a bug already connected to this instance.
        """
        if not self._bugId and isinstance(bugId, int):
            self._bugId = bugId
        if not self.bug:
            exceptions.MissingValue("No valid bug has be been set to add this attachment to")
        if self._isRegistered:
            exceptions.PermissionDenied("An attachment can only be attached to a single bug at this timed")

        sections = [self.server.restUrl, const.Endpoints.V1, const.Endpoints.ATTACHMENTS, const.Endpoints.REGISTER_ATTACHMENT]
        fields = {const.Fields.BUG_ID: self.bug.id, const.Fields.ATTACHMENT_ID: self.id}
        if self.server.delegate is not None:
            fields[const.QueryFields.AS_USER] = self.server.delegateUser.id
        connectURL = self.server.buildUrl(sections, fields=fields)
        self.server.get(connectURL)
        self._isRegistered = True
        self.IS_READONLY = True

    def disconnectFromBug(self):
        """
        Disconnecting the attachment from the bug currently associated with
        """

        if not self.bug:
            exceptions.MissingValue("There is no bug associated with this attachment")
        # TODO: Check if there is a better way of generating the V@ rest link...
        sections = [
            self.server.restUrl,
            const.Endpoints.V2,
            const.Endpoints.ATTACHMENT_ASSOCIATIONS,
            const.Endpoints.ATTACHMENT,
            self.id,
            const.Endpoints.CONTAINER,
            self.bug.id,
        ]
        fields = {}
        if self.server.delegate is not None:
            fields[const.QueryFields.AS_USER] = self.server.delegateUser.id
        disconnectURL = self.server.buildUrl(sections, fields=fields)
        self.server.delete(disconnectURL)
        self._isRegistered = False
        self.IS_READONLY = True

    def setFile(self, path):
        """
        Encodes the file in memory and sets the Attachment property filename that holds the name of the file.

        Args:
            path (str): Full path to the file to add
        """
        if self.id:
            raise exceptions.PermissionDenied("Cannot modify attachment that has already been added to the server")
        # Encode the image to byte64 before actually setting the value
        encodedImage = self.encodeFile(path)
        self._setattr(const.Fields.CONTENT, encodedImage)
        self._setattr(const.Fields.FILENAME, os.path.basename(path))

    def toString(self, isV2=False):
        """Overrides inherited method.

        Set the time record to use only the v1 endpoint.

        Returns:
            str
        """
        return super(Attachment, self).toString(isV2=isV2)


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the Attachment class
    """

    def __call__(self, index=None, bugId=None, data=None):
        """
        Builds a payload.

        If no index is provided then an empty instance is returned.
        If an index is provided then this instance will be populated with data from the server.

        Args:
            index (int): id of the entity on the shotgun server.
            bugId (int): bug id of bug where this attachment belong to.
            data (dict): data to fill the instance with

        Returns:
            rsg_core_py.abstracts.abstractPayload.AbstractPayload
        """
        class_ = self.cls()
        return class_(self._server, index, bugId, data)

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Attachment

    def getByName(self, name, all=False):
        """
        Gets the bugstar object by its name.

        Note:
            This code uses an endpoint that currently isn't mass supported, this method may need to be reimplemented
            in inherited classes in the meantime

        Args:
            name (str): name to search for, if other entites start with the same name then those will be returned as well.
            all (bool): retun all the entites matched, otherwise just return the first match.

        Raises:
            ValueError : This method expected a string and didn't receive one or the string was empty
        """
        raise NotImplementedError("This method is unsupported by Attachments")
