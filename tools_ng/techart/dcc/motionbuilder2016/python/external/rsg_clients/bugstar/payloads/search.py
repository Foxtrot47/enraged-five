import os
import json
import types
import datetime
from xml.etree import cElementTree as xml

import xmltodict

from rsg_core import conversions
from rsg_clients import const as clientConst
from rsg_clients.bugstar import const, exceptions
from rsg_clients.bugstar.payloads import basePayload


class Search(basePayload.BasePayload):
    """
    An payload class for programmatically creating an xml for performing a search in bugstar

    Note:

        # required fields for storing data from bugstar
        self._fields[const.Fields.ID] = index
        self._fields[const.Fields.NAME] = None
        self._fields[const.Fields.DESCRIPTION] = None
        self._fields[const.Fields.MODIFIED_BY] = None
        self._fields[const.Fields.MODIFIED_AT] = None
        self._fields[const.Fields.CREATED_BY] = None
        self._fields[const.Fields.CREATED_AT] = None
        self._fields[const.Fields.VERSION] = None

    Examples:
        from rsg_clients.bugstar import core, const, contents

        # Perform a Search
        core.Service.setServer(const.Servers.REST_DEV)

        # simple search for getting all bugs with the word test in their summary
        collection = contents.Collection(const.Operations.AND)
        comparision = contents.Comparison(const.Fields.SUMMARY, const.Operations.CONTAINS)

        collection.add(comparision)
        comparision.add("test")

        # Add the new collection to the actual search
        search = contents.Search()
        search.add(collection)

        # Time search
        start = time.time()
        command = [core.Service.restUrl(), const.Commands.V2,
                   const.Commands.BUGS, const.Commands.SEARCHES, const.Commands.INVOKE, const.Commands.PROJECT, 69367]
        url = core.Service.buildUrl(command)

        print "Number of Bugs Found"
        value = core.Service.post(url, payload=search.toString())
        print len(value.get("Results", {}).get("Result"))
    """

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []
    _COMMAND = const.Endpoints.SEARCHES
    IS_READONLY = False

    def __init__(self, bugstar, index=None, root=None, projects=None, endpoint=None):
        """
        Overrides inherited method

        Arguments:
            bugstar (rsg_clients.bugstar.server.BaseServer): the local server to set for this object
            index (int): id of the search if it already exists on watchstar
            root (Collection): the root collection for this search. Creates a default collection that looks in the
                               current project by default.
            projects (str): which projects should the scope of the search cover. Only valid if the root is not passed in.
                            This value is a string, that can be obtained from rsg_clients.bugstar.const.SearchValues.
            endpoint (str): the default endpoint to perform the search no. Bugstar only supports users and bugs at this time.
        """
        if self._isCached(bugstar, index, self):
            return

        # properties to store
        self._setup(index, bugstar, root, projects, endpoint)
        if index is not None:
            self.refresh()  # Pulls the search parameters from bugstar
        self._cache()

    def _setup(self, index=None, server=None, root=None, projects=None, endpoint=None):
        """
        Overrides inherited method

        Arguments:
            index (int): id of the search if it already exists on watchstar
            server (rsg_clients.bugstar.server.BaseServer): the local server to set for this object
            root (Collection): the root collection for this search. Creates a default collection that looks in the
                               current project by default.
            projects (str): which projects should the scope of the search cover. Only valid if the root is not passed in.
                            This value is a string, that can be obtained from rsg_clients.bugstar.const.SearchValues.
            endpoint (str): the default endpoint to perform the search no. Bugstar only supports users and bugs at this time.

        """
        super(Search, self)._setup(index, None, server)
        self._root = root
        self._projects = projects
        self._endpoint = endpoint
        self._searchFields = [const.SearchFields.ID, const.SearchFields.PROJECT_ID]
        self._statistics = Statistics(self)
        self.path = None

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    @classmethod
    def _getData(cls, server, index, endpoint, returnRaw=False):
        """
        Performs a get request against the server to retrieve information on data on a single object based on the
        data type that the class represents

        Arguments:
            server (rsg_clients.bugstar.servers.BaseServer): server to connect to
            index (str): the index of the object, may also accept REST
            endpoint (str): the default endpoint to perform the search no. Bugstar only supports users and bugs at this time.
            returnRaw (bool): return the data from the request as is (usually a string)
        Returns:
            list
        """
        restUrl = server.restUrl
        # TODO: Add support for user searches
        sections = [restUrl, const.Endpoints.V2, endpoint, cls.command(), index]
        fields = None
        if server.delegate is not None:
            fields = {const.QueryFields.AS_USER: server.delegateUser.id}
        url = server.buildUrl(sections, fields=fields)
        response = server.get(url, returnRaw=returnRaw, acceptType=clientConst.ContentType.XML_UTF8)
        return response.text

    @property
    def fields(self):
        """
        Fields to return when the search is performed
        To avoid users from accidentally removing required fields, we return the list as a tuple

        Return:
            tuple
        """
        return tuple(self._searchFields)

    @property
    def filename(self):
        """ The base name of the file that was used to generate this search """
        if self.path is not None:
            return os.path.basename(self.path)

    @property
    def name(self):
        """ isOnHold is readable bugstar property """
        return self._getattr(const.Fields.NAME)

    @name.setter
    def name(self, value):
        """ isOnHold is readable bugstar property """
        if not self._id:
            return self._setattr(const.Fields.NAME, value)
        raise exceptions.ReadOnlyError()

    @property
    def version(self):
        """ isOnHold is readable bugstar property """
        return self._getattr(const.Fields.VERSION)

    @property
    def description(self):
        """ isOnHold is readable bugstar property """
        return self._getattr(const.Fields.DESCRIPTION)

    @property
    def createdBy(self):
        """ developer is readable bugstar property """
        value = self._getattr(const.Fields.CREATED_BY)
        if not isinstance(value, (self.server.user._CLASS, self.server.test._CLASS, types.NoneType)):
            self.createdBy = value
            value = self._getattr(const.Fields.CREATED_BY)
        return value

    @createdBy.setter
    def createdBy(self, value):
        """ developer is writeable bugstar property """
        if isinstance(value, int):
            owner = self.server.user(value)
            if not owner.isValid:
                owner = self.server.team(value)
            value = owner
        self._createdBy = value

    @property
    def createdOn(self):
        """ Due date is readable bugstar property """
        value = self._fields.get(const.Fields.CREATED_ON, None)
        if isinstance(value, basestring):
            value = self.convertToDateTime(value)
            self._fields[const.Fields.CREATED_ON] = value
        return value

    @property
    def modifiedBy(self):
        """ developer is readable bugstar property """
        value = self._getattr(const.Fields.MODIFIED_BY)
        if not isinstance(value, (self.server.user._CLASS, self.server.test._CLASS, types.NoneType)):
            self.modifiedBy = value
            value = self._getattr(const.Fields.MODIFIED_BY)
        return value

    @modifiedBy.setter
    def modifiedBy(self, value):
        """ developer is writeable bugstar property """
        if isinstance(value, int):
            owner = self.server.user(value)
            if not owner.isValid:
                owner = self.server.team(value)
            value = owner
        self._modifiedBy = value

    @property
    def modifiedAt(self):
        """ Due date is readable bugstar property """
        value = self._fields.get(const.Fields.MODIFIED_AT, None)
        if isinstance(value, basestring):
            value = self.convertToDateTime(value)
            self._fields[const.Fields.MODIFIED_AT] = value
        return value

    @property
    def statistics(self):
        return self._statistics

    def root(self):
        """
        This is the root collection of the search.

        Returns:
            Collection
        """
        if self._root is None:
            root = Collection(const.Operations.AND)
            comparison = Comparison(const.SearchFields.PROJECT, const.Operations.IN)
            comparison.add(Value(self._projects or const.SearchValues.ALL_PROJECTS, "parameter"))
            root.add(comparison)
            self._root = root
        return self._root

    @property
    def totalCount(self):
        """The total number of results from this search."""
        return self.statistics.count

    def setRoot(self, collection):
        """
        Sets the root collection of this search.

        Usually the root dictates which projects to perform a search in.

        Arguments:
            collection (rsg_clients.bugstar.contents.Collection): collection to set as the root of the search
        """
        if isinstance(collection, Collection):
            self._root = collection

    def add(self, value):
        """
        Convenience method for adding a collection or comparision as a child to the root collection

        Arguments:
            value (Collection or Comparison): a collection or comparison to add as a child
        """
        self.root().add(value)

    def remove(self, value):
        """
        Convenience method for removing a collection or comparision as a child from the root collection

        Arguments:
            value (Collection or Comparison): a collection or comparison to remove as a child
        """
        self.root().remove(value)

    def search(self, endpoint=None, size=None, page=None, iterative=True, generator=False):
        """
        Performs the search

        Args:
            endpoint (str, optional): The endpoint to use for determining the data returned from the search.
                                Currently only supports the the Bugs and Users command, and it defaults to the type that
                                the search was obtained from.
            size (int, optional): The amount of bugs to be returned by a page in a single search. Default is 10,000
            page (int, optional): The page of the search to return. This can be used to break searches with large amount of bugs
                        into smaller chunks. Default is 1.
            generator (bool, optional): return a generator that yields chunks of the search results at a time rather then a single
                              complete list.
            iterative (bool, optional): iterate through all the pages returned by a search. By default it is True.

        Returns:
            list or generator
        """
        results = self.server.search_(
            endpoint=endpoint or self._endpoint,
            payload=self.toString(),
            size=size,
            page=page,
            iterative=iterative,
            delegate=self.server.delegateUser,
        )

        # Grab the first result as it will always be statistics data
        self.statistics.refresh(results.next())
        if not generator:
            allResults = []
            for result in results:
                allResults.extend(result)
            return allResults
        return results

    def bugs(self, size=None, page=None, onlyData=False, iterative=True):
        """
        Gets all the bugs returned from the search

        Keyword Args:
            size (int): The amount of bugs to be returned by a page in a single search. Default is 10,000
            page (int): The page of the search to return. This can be used to break searches with large amount of bugs
                        into smaller chunks. Default is 1.
            onlyData (bool): return the raw dictionary data rather than bug instances
            iterative (bool, optional): iterate through all the pages returned by a search. By default it is True.

        Returns:
            list
        """
        results = self.search(size=size, page=page, endpoint=const.Endpoints.BUGS, iterative=iterative)
        if onlyData:
            return results
        bugs = []
        for bug in results:
            bugs.append(self.server.bug(int(bug[const.SearchFields.ID]), int(bug[const.SearchFields.PROJECT_ID])))
        return bugs
        # return [
        #     self.server.bug(int(bug[const.SearchFields.ID]), int(bug[const.SearchFields.PROJECT_ID])) for bug in results
        # ]

    def yieldBugs(self, size=None, page=None, onlyData=False, iterative=True):
        """
        Gets all the bugs returned from the search as a generator

        Keyword Args:
            size (int): The amount of bugs to be returned by a page in a single search. Default is 10,000
            page (int): The page of the search to return. This can be used to break searches with large amount of bugs
                        into smaller chunks. Default is 1.
            onlyData (bool): return the raw dictionary data rather than bug instances
            iterative (bool, optional): iterate through all the pages returned by a search. By default it is True.

        Yields:
            rsg_clients.bugstar.contents.Bug or dict
        """
        for results in self.search(
            size=size, page=page, endpoint=const.Endpoints.BUGS, generator=True, iterative=iterative
        ):
            for bug in results:
                if onlyData:
                    yield bug
                else:
                    yield self.server.bug(int(bug[const.SearchFields.ID]), int(bug[const.SearchFields.PROJECT_ID]))

    def users(self, size=None, page=None, onlyData=False, iterative=True):
        """
        Gets all the users returned from the search

        Args:
            size (int): The amount of bugs to be returned by a page in a single search. Default is 10,000
            page (int): The page of the search to return. This can be used to break searches with large amount of bugs
                        into smaller chunks. Default is 1.
            onlyData (bool): return the raw dictionary data rather than bug instances
            iterative (bool, optional): iterate through all the pages returned by a search. By default it is True.

        Returns:
            list
        """
        results = self.search(size=size, page=page, command=const.Endpoints.USERS, iterative=iterative)
        if onlyData:
            return results
        return [self.server.user(int(user[const.SearchFields.ID])) for user in results]

    def yieldUsers(self, size=None, page=None, onlyData=False, iterative=True):
        """
        Gets all the users returned from the search as a generator

        Args:
            size (int, optional): The amount of bugs to be returned by a page in a single search. Default is 10,000
            page (int, optional): The page of the search to return. This can be used to break searches with large amount of bugs
                        into smaller chunks. Default is 1.
            onlyData (bool, optional): return the raw dictionary data rather than bug instances
            iterative (bool, optional): iterate through all the pages returned by a search. By default it is True.

        Yield:
            rsg_clients.bugstar.contents.User or dict
        """
        for results in self.search(
            size=size, page=page, command=const.Endpoints.USERS, generator=True, iterative=iterative
        ):
            for user in results:
                if onlyData:
                    yield user
                else:
                    yield self.server.user(int(user[const.SearchFields.ID]))

    def results(self):
        if self._endpoint == const.Endpoints.BUGS:
            return

    def addField(self, value):
        """
        Add fields from the list of fields the search is expected to return

        Arguments:
            value (str): adds the field to the list of results that the search is expected to return
        """
        if value not in self._searchFields:
            self._searchFields.append(value)

    def removeField(self, value):
        """
        Remove results from the list of results the search is expected to return.

        Arguments:
            value (str): removes the field from the

        Raises:
            BugstarException when someone attempts to remove the id or project id fields as those are required by
            the Bug class for it to work properly.
        """
        if value in (const.SearchFields.ID, const.SearchFields.PROJECT_ID):
            raise exceptions.BugstarException(
                "{} is a required result when performing searches and can not be removed".format(value),
                includeErrorCode=False,
            )
        if value in self._searchFields:
            self._searchFields.remove(value)

    def refresh(self, xmlString=None):
        """Overrides inherited method.

        Args:
            xmlString (string): the xml data as a string
        """
        # TODO: Update to support json
        if not xmlString:
            xmlString = self._getData(self.server, self.id, self._endpoint, returnRaw=True)

        dictionary = json.loads(json.dumps(xmltodict.parse(xmlString)))
        dictionary = dictionary.get("Search")
        self.parse(dictionary, isV2=True)
        self._search = self.parseXmlString(xmlString, self)

    def xmlElement(self, isV2=True):
        """
        Overrides inherited method

        Only calls the V2 xml version

        Arguments:
            isV2 (bool): is this element supposed to be v2 compatible

        Returns:
            xml.etree.cElementTree
        """
        return self._xmlElementV2()

    def _xmlElementV2(self):
        """
        An xml element that represents this bug search

        Returns:
            xml.etree.cElement.Element
        """
        element = xml.Element("Search")
        element.append(self._headerElement())
        element.append(self._root.xmlElement())  # we are putting the collection after the header
        element.append(self._resultsElement())  # adds the fields that the search will return in its results
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Sets the values from bugstar to the instance

        Args:
            dictionary (dict): the dictionary generated from the bugstar data

        Returns:
            None
        """
        self._fields.update(dictionary)

    def toString(self):
        """
        The xml representation of this search as a string that can be used as a payload for the search post command.

        Returns:
            string
        """
        return super(Search, self).toString(isV2=True)

    @staticmethod
    def _headerElement():
        """
        Creates the header for the xml payload

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element("target")
        for tag, text in [
            (const.Fields.TYPE, "ENTITY_TYPE"),
            (const.Fields.VALUE, "401"),
            (const.Fields.NAME, "ENTITY_Bug"),
            (const.Fields.DESCRIPTION, "com.rsg.entities.bugstar.Bug"),
        ]:
            subelement = xml.Element(tag)
            subelement.text = text
            element.append(subelement)
        return element

    def _resultsElement(self):
        """
        Creates the result element for the xml payload

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element("resultColumns")

        for field in self._searchFields:
            subelement = xml.Element("ResultColumn")
            tagElement = xml.Element(const.Fields.TAG)
            tagElement.text = field
            subelement.append(tagElement)
            element.append(subelement)
        return element

    @classmethod
    def parseXml(cls, path):
        """
        Builds a search from an existing xml file

        Args:
            path (str): path to the file to parse

        Returns:
            Search
        """
        tree = xml.parse(path)
        root = tree.getroot()
        try:
            search = cls._parseRoot(root)
        except exceptions.BugstarException:
            raise exceptions.BugstarException(
                "{} is not a valid bugstar search xml".format(path), includeErrorCode=False
            )
        search.path = path
        return search

    @classmethod
    def parseXmlString(cls, xmlString, instance=None):
        """
        Builds a search from an xml represented as a string

        Args:
            xmlString (str): xml as a string

        Returns:
            Search
        """
        root = xml.fromstring(xmlString)
        search = cls._parseRoot(root, instance=instance)
        search.path = None
        return search

    @classmethod
    def _parseRoot(cls, root, instance=None):
        """
        Parses an xml element and returns a Search object representing that search

        Arguments:
            root (xml.etree.ElementTree.element): root of the xml

        Returns:
            Search
        """
        element = root.find("Collection")
        if not element:
            raise exceptions.BugstarException("This is not a valid bugstar search xml", includeErrorCode=False)

        collection = Collection.convertElement(element)
        if instance is None:
            search = Search(root=collection)
        else:
            instance.setRoot(collection)
            search = instance

        fieldsElement = root.find("resultColumns")
        if fieldsElement:
            for fieldElement in fieldsElement:
                tagElement = fieldElement.find(const.Fields.TAG)
                if tagElement:
                    search.addField(tagElement.text.strip())
        return search

    def isModified(self, refresh=False):
        """
        Checks if the search has been modified recently

        Args:
            refresh (bool): update the search parameters locally if it has been modified

        Returns:
            bool
        """
        xmlString = self._getData(self.server, self.id, endpoint=self._endpoint, returnRaw=True)
        element = xml.fromstring(xmlString)
        modifiedElement = element.find("Search|modifiedAt")
        if not modifiedElement or not modifiedElement.text:
            return False
        lastModifiedTime = self.convertToDateTime(modifiedElement.text.strip())
        if self.modifiedAt == lastModifiedTime:
            if refresh:
                self.populate(xmlString)
            return False
        return True


class Collection(basePayload.BasePayload):
    """
    A Collection is a group of comparisons and other collections that build up a search
    """

    TAG_NAME = "Collection"  # Name of the element from the xml generated by bugstar
    IS_REQUESTABLE = False

    def __new__(cls, *args, **kwargs):
        """
        Overrides inherited method

        Restores original behavior as objects from this class don't need to be cached.
        """
        return object.__new__(cls)

    def __init__(self, operation=None, disabled=False):
        """
        Overrides inherited method

        Arguments:
            operation (str): operation being performed by the collection
            disabled (bool): should the search ignore this collection
        """
        self._operation = operation or const.Operations.AND
        self._disabled = disabled
        self._parent = None
        self._children = []

    def add(self, value):
        """
        Add a collection or comparision as a child to this collection

        Arguments:
            value (Collection or Comparison): a collection or comparison to add as a child
        """
        if not isinstance(value, (Comparison, Collection, FullText)):
            raise ValueError("{} is not a Comparison or FullText or Collection".format(value))

        if value not in self._children:
            self._children.append(value)
            value._parent = self

    def remove(self, value):
        """
        Removes the value from the the collection

        Arguments:
            value (rsg_clients.bugstar.contents.AbstractPayload): the comparison or collection to remove

        Returns:

        """
        if not isinstance(value, (Comparison, Collection, FullText)):
            raise ValueError("{} is not a Comparison or Collection".format(value))

        if value in self._children:
            self._children.remove(value)
            value._parent = None

    @property
    def parent(self):
        """ Parent collection for this collection """
        return self._parent

    @property
    def children(self):
        """ The children comparisons and collections of this collection """
        return tuple(self._children)

    @property
    def operation(self):
        """The operation this collection performs."""
        return self._operation

    @property
    def disabled(self):
        """Is this collection disabled"""
        return self._disabled

    def xmlElement(self, isV2=False):
        """
        An xml element that represents this collection

        Returns:
            xml.etree.cElement.Element
        """
        element = xml.Element(self.__class__.__name__)
        element.attrib["op"] = self._operation
        element.attrib["disabled"] = str(self._disabled).lower()

        for child in self._children:
            element.append(child.xmlElement())
        return element

    @classmethod
    def _validateAttributes(cls, attrib):
        """
        Returns the attrib dictionary passed in with correct key names that are accepted by the __init__ of this class.

        Arguments:
            attrib (dict): attribute dictionary from an xml.etree.elementTree.Element instance

        Returns:
            dict
        """
        dictionary = conversions.convertValue(attrib)
        dictionary["operation"] = dictionary.pop("op")
        dictionary["disabled"] = dictionary.pop("disabled", "false") == "true"
        return dictionary


class Comparison(basePayload.BasePayload):
    """
    A comparison is the condition being used to filter out information from bugstar when performing a search
    """

    TAG_NAME = "Comparison"  # Name of the element from the xml generated by bugstar
    IS_REQUESTABLE = False

    def __new__(cls, *args, **kwargs):
        """
        Overrides inherited method

        Restores original behavior as objects from this class don't need to be cached.
        """
        return object.__new__(cls)

    def __init__(self, field, operation, disabled=False):
        """
        Overrides inherited method

        Arguments:
            field (str): the field to compare data from
            operation (str): how the data found from the field should be compared against
            disabled (bool): should the search ignore this collection
        """
        self._field = field
        self._operation = operation
        self._disabled = disabled
        self._values = []
        self._parent = None

    @property
    def field(self):
        """ Name of the field whose data will be compared """
        return self._field

    @property
    def operation(self):
        """ Name of the operation being used to compare data in field to the one stored in this comparison """
        return self._operation

    @property
    def parent(self):
        """ Parent collection for this comparison """
        return self._parent

    @property
    def values(self):
        """ The values held by this comparison"""
        return self._values

    @property
    def disabled(self):
        """Is this comparison disabled"""
        return self._disabled

    def add(self, value, datatype=None):
        """
        Adds values to compare against the data on Bugstar

        Arguments:
            value (object): data used to compare against existing data on bugstar. If the object provided isn't a Value
                            object it will be converted to one.
            datatype (object): if Value object isn't provided, this argument is used to determine the type of data.
                               When None is provided, the type is assumed to be that of the current class of that object.

        Returns:
            rsg_clients.bugstar.payloads.search.Value
        """
        if not isinstance(value, Value):
            value = Value(value, datatype=datatype)

        if value not in self._values:
            self._values.append(value)
        return value

    def remove(self, value, datatype=None):
        """
        Removes values to compare against the data on Bugstar.

        Arguments:
            value (object): data used to compare against existing data on bugstar. If the object provided isn't a Value
                            object it will be converted to one.
            datatype (object): if Value object isn't provided, this argument is used to determine the type of data.
                               When None is provided, the type is assumed to be that of the current class of that object.

        Returns:
            rsg_clients.bugstar.payloads.search.Value
        """
        if not isinstance(value, Value):
            value = Value(value, datatype=datatype)

        if value in self._values:
            self._values.remove(value)
            return value

    def clear(self):
        """ Clears out the values for the comparision """
        self._values = []

    def xmlElement(self, isV2=False):
        """
        An xml element that represents this Comparison

        Returns:
            xml.etree.cElement.Element
        """
        element = xml.Element(self.__class__.__name__)
        element.attrib["field"] = self._field
        element.attrib["op"] = self._operation
        element.attrib["disabled"] = str(self._disabled).lower()

        for value in self._values:
            element.append(value.xmlElement())
        return element

    @classmethod
    def _validateAttributes(cls, attrib):
        """
        Returns the attrib dictionary passed in with correct key names that are accepted by the __init__ of this class.

        Arguments:
            attrib (dict): attribute dictionary from an xml.etree.elementTree.Element instance

        Returns:
            dict
        """
        dictionary = conversions.convertValue(attrib)
        dictionary["operation"] = dictionary.pop("op")
        return dictionary


class FullText(basePayload.BasePayload):
    """
    A comparison is the condition being used to filter out information from bugstar when performing a search
    """

    TAG_NAME = "Fulltext"  # Name of the element from the xml generated by bugstar
    IS_REQUESTABLE = False

    def __new__(cls, *args, **kwargs):
        """
        Overrides inherited method

        Restores original behavior as objects from this class don't need to be cached.
        """
        return object.__new__(cls, *args, **kwargs)

    def __init__(self, field, operation, disabled=False, escapeSpecialChars=False):
        """
        Overrides inherited method

        Arguments:
            field (str): the field to compare data from
            operation (str): how the data found from the field should be compared against
            disabled (bool): should the search ignore this collection
        """
        self._field = field
        self._operation = operation
        self._disabled = disabled
        self._escapeSpecialChars = escapeSpecialChars
        self._values = []

    def xmlElement(self, isV2=False):
        """
        An xml element that represents this Comparison

        Returns:
            xml.etree.cElement.Element
        """
        element = xml.Element(self.TAG_NAME)
        element.attrib["escapeSpecialChars"] = self._escapeSpecialChars
        element.attrib["field"] = self._field
        element.attrib["op"] = self._operation
        element.attrib["disabled"] = str(self._disabled).lower()

        values = []
        for value in self._values:
            if isinstance(value, SearchQuery):
                value = Value(value.value)

            if value.value in values:
                continue
            values.append(value.value)
            element.append(value.xmlElement())

        return element

    def add(self, value, datatype=None):
        """
        Adds values to compare against the data on Bugstar

        Arguments:
            value (object): data used to compare against existing data on bugstar. If the object provided isn't a Value
                            object it will be converted to one.
            datatype (object): if Value object isn't provided, this argument is used to determine the type of data.
                               When None is provided, the type is assumed to be that of the current class of that object.

        Returns:
            Value
        """
        if not isinstance(value, (Value, SearchQuery)):
            value = Value(value, datatype=datatype)

        if value not in self._values:
            self._values.append(value)
        return value

    def remove(self, value, datatype=None):
        """
        Adds values to compare against the data on Bugstar

        Arguments:
            value (object): data used to compare against existing data on bugstar. If the object provided isn't a Value
                            object it will be converted to one.
            datatype (object): if Value object isn't provided, this argument is used to determine the type of data.
                               When None is provided, the type is assumed to be that of the current class of that object.

        Returns:
            Value
        """
        if not isinstance(value, (Value, SearchQuery)):
            value = Value(value, datatype=datatype)

        if value in self._values:
            self._values.remove(value)
            return value

    def clear(self):
        """ Clears out the values for the comparision """
        self._values = []

    @classmethod
    def _validateAttributes(cls, attrib):
        """
        Returns the attrib dictionary passed in with correct key names that are accepted by the __init__ of this class.

        Arguments:
            attrib (dict): attribute dictionary from an xml.etree.elementTree.Element instance

        Returns:
            dict
        """
        dictionary = conversions.convertValue(attrib)
        dictionary["operation"] = dictionary.pop("op")
        return dictionary


class Value(basePayload.BasePayload):
    """ A value is the data used by the comparison to determine what to filter by when performing a search """

    TAG_NAME = const.Fields.VALUE  # Name of the element from the xml generated by bugstar
    DATETIME_FORMAT = const.DateTimeFormats.SEARCH_VALUES
    IS_REQUESTABLE = False

    def __new__(cls, *args, **kwargs):
        """
        Overrides inherited method

        Restores original behavior as objects from this class don't need to be cached.
        """
        return object.__new__(cls)

    def __init__(self, value, datatype=None):
        """
        Initializer

        Arguments:
            value (object): data used to compare against existing data on bugstar. If the object provided isn't a Value
                            object it will be converted to one.
            datatype (object): If Value object isn't provided, this argument is used to determine the type of data.
                               When None is provided, the type is assumed to be that of the current class of that object.
        """
        if datatype is None and isinstance(value, basestring):
            datatype = "string"
        elif datatype is None and isinstance(value, datetime.datetime):
            datatype = "date"
        elif datatype == "date" and isinstance(value, basestring):
            value = datetime.datetime.strptime(value, self.DATETIME_FORMAT)
        elif isinstance(value, basePayload.BasePayload):
            value = value.id
            datatype = "int"

        self.value = value
        self.datatype = datatype or type(value).__name__

    def xmlElement(self):
        """
        An xml element that represents this Value

        Returns:
            xml.etree.cElement.Element
        """
        element = xml.Element(self.__class__.__name__.lower())
        element.attrib[const.Fields.TYPE] = self.datatype
        if isinstance(self.value, datetime.datetime):
            element.attrib[const.Fields.VALUE] = self.value.strftime(self.DATETIME_FORMAT)
        else:
            element.attrib[const.Fields.VALUE] = str(self.value)

        return element

    @classmethod
    def _validateAttributes(cls, attrib):
        """
        Returns the attrib dictionary passed in with correct key names that are accepted by the __init__ of this class.

        Arguments:
            attrib (dict): attribute dictionary from an xml.etree.elementTree.Element instance

        Returns:
            dict
        """
        dictionary = conversions.convertValue(attrib)
        dictionary["datatype"] = dictionary.pop(const.Fields.TYPE)
        return dictionary


class SearchQuery(basePayload.BasePayload):
    """ A value used by the Full Text payload element to express search parameters exposed as text"""

    TAG_NAME = "searchQuery"  # Name of the element from the xml generated by bugstar
    DATETIME_FORMAT = const.DateTimeFormats.SEARCH_VALUES
    IS_REQUESTABLE = False

    def __new__(cls, *args, **kwargs):
        """
        Overrides inherited method

        Restores original behavior as objects from this class don't need to be cached.
        """
        return object.__new__(cls, *args, **kwargs)

    def __init__(self, value):
        """
        Initializer

        Arguments:
            value (object): data used to compare against existing data on bugstar. If the object provided isn't a Value
                            object it will be converted to one.
            datatype (object): If Value object isn't provided, this argument is used to determine the type of data.
                               When None is provided, the type is assumed to be that of the current class of that object.
        """
        self.value = conversions.convertToAscii(value)

    def xmlElement(self):
        """
        An xml element that represents this Value

        Returns:
            xml.etree.cElement.Element
        """
        element = xml.Element(self.TAG_NAME)
        element.text = self.value
        return element

    @classmethod
    def _validateText(cls, element):
        """
        Returns the text data from the data in a way that is consumable by other methods.

        Arguments:
            element (xml.etree.cElementTree): an xml.etree.elementTree.Element instance to grab text data from

        Returns:
            list
        """
        return [element.text.strip()]

    @classmethod
    def _validateAttributes(cls, attrib):
        """
        Returns the attrib dictionary passed in with correct key names that are accepted by the __init__ of this class.

        Arguments:
            attrib (dict): attribute dictionary from an xml.etree.elementTree.Element instance

        Returns:
            dict
        """
        return attrib


class Statistics(basePayload.BasePayload):
    """
    Stats about the bugs contained within this search. This data is strictly returned from the server and uneditable.
    For fresh data the search must be performed against the server again.
    """

    TAG_NAME = "Statistics"  # Name of the element from the xml generated by bugstar
    IS_REQUESTABLE = False
    IS_READONLY = True

    def __new__(cls, *args, **kwargs):
        """
        Overriding inherited method, we are removing the caching for this class

        Args:
            *args (tuple or list): arguments being passed into the function
            **kwargs (dict): keyword arguments being passed into the function
        """
        return object.__new__(cls)

    def __init__(self, search, data=None):
        """
        Initializer

        Arguments:
            search (Search): The search that this statistics belongs to.

        Keyword Args:
            data (dict): data used to compare against existing data on bugstar. If the object provided isn't a Value
                            object it will be converted to one.
        """
        self._search = search
        self._setup(None, data, None)

    def _getattr(self, item, type, default=None):
        """
        Gets the non-mutable value for the given attribute.

        Arguments:
            item (str): name of the property being accessed

        Keyword Args:
            refresh (bool): populate the rest of the object with data from the server
            mutableType (bool): the mutable data type for the attribute being requested

        Returns:
            object
        """
        value = self._fields.get(item, default)

        if not isinstance(value, type):
            value = type(value)
            self._fields[item] = value

        if self._isUpdated and not self.isDataStale:
            return value

        # grab the latest data from the server to populate the object with
        self.refresh()
        return self._fields.get(item, default)

    def refresh(self, data=None):
        """
        Overrides inherited function

        Simplified logic for getting the latest data from the server

        Keyword Args:
            data (dict): statistics data returned after performing a search
        """
        if data is None:
            # Perform the search to populate the stats
            self._search.search(size=1, page=1, generator=True)
        else:
            self.parse(data, isV2=True)

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Sets the data on the object

        Args:
            dictionary (dict): data to set on the object
        """
        self._fields = dictionary

    @property
    def count(self):
        """
        Number of bugs in this search

        Returns:
            int
        """
        return self._getattr(const.Fields.COUNT, int, 0)

    @property
    def totalDistinctOwners(self):
        """
        Number of unique bug owners in this search

        Returns:
            int
        """
        return self._getattr(const.Fields.TOTAL_DISTINCT_OWNERS, int, 0)

    @property
    def totalProgress(self):
        """
        The progress on all the bugs, which is calculated by comparing the estimated time vs logged time

        Returns:
            int
        """
        return self._getattr(const.Fields.TOTAL_PROGRESS, int, 0)

    @property
    def totalEstimated(self):
        """
        The total amount of estimated time of all the bugs from this search

        Returns:
            int
        """
        return self._getattr(const.Fields.TOTAL_ESTIMATED, float, 0.0)

    @property
    def totalTimeRemaining(self):
        """
        The total amount of time of remaining for the bugs from this search

        Returns:
            float
        """
        return self._getattr(const.Fields.TOTAL_TIME_REMAINING, float, 0.0)

    @property
    def totalTimeSpent(self):
        """
        The total amount of time of work done for the bugs from this search

        Returns:
            float
        """
        return self._getattr(const.Fields.TOTAL_TIME_SPENT, float, 0.0)

    def xmlElement(self):
        """
        An xml element that represents this Value

        Returns:
            xml.etree.cElement.Element
        """
        element = xml.Element(self.__class__.__name__.lower())
        for key, value in self._fields:
            element.attrib[key] = str(value)
        return element


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the Search class and classes used for creating searches.
    """

    def __call__(self, index=None, root=None, projects=None, endpoint=None):
        """
        Builds a payload.

        If no index is provided then an empty instance is returned.
        If an index is provided then this instance will be populated with data from the server.

        Args:
            index (int, optional): id of the entity on the shotgun server.
            root (rsg_clients.bugstar.payloads.search.Collection, optional): The root collection for the search
            projects (str, optional): the search comparison value that determines which projects the search will cover
            endpoint (str, optional): the default endpoint to perform the search on. Bugstar only supports users and bugs at this time.

        Returns:
            rsg_core_py.abstracts.abstractPayload.AbstractPayload
        """
        class_ = self.cls()
        return class_(self._server, index=index, root=root, projects=projects, endpoint=endpoint)

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Search

    @classmethod
    def isCollection(cls, value):
        return isinstance(value, Collection)

    @classmethod
    def isComparison(cls, value):
        return isinstance(value, Comparison)

    def collection(self, operation=None, disabled=False):
        """
        Builds a collection payload

        Keyword Args:
            operation (str): operation being performed by the collection
            disabled (bool): should the search ignore this collection

        Returns:
            rsg_clients.bugstar.payloads.search.Collection
        """
        return Collection(operation, disabled)

    def comparison(self, field, operation, disabled=False):
        """
        Builds a comparison payload

        Arguments:
            field (str): the field to compare data from
            operation (str): how the data found from the field should be compared against

        Keyword Args:
            disabled (bool): should the search ignore this collection


        Returns:
            rsg_clients.bugstar.payloads.search.Comparison
        """
        return Comparison(field, operation, disabled)

    def fullText(self, field, operation, disabled=False, escapeSpecialChars=False):
        """
        Builds a full text payload

        Arguments:
            field (str): the field to compare data from
            operation (str): how the data found from the field should be compared against

        Keyword Args:
            disabled (bool): should the search ignore this collection
            escapeSpecialChars (bool): escapes special characters


        Returns:
            rsg_clients.bugstar.payloads.search.FullText
        """
        return FullText(field, operation, disabled=disabled, escapeSpecialChars=escapeSpecialChars)

    def searchQuery(self, value):
        """
        Builds a search query payload

        Arguments:
            value (str): the data to include in as the search query

        Returns:
            rsg_clients.bugstar.payloads.search.SearchQuery
        """
        return SearchQuery(value)

    def value(self, value, datatype=None):
        """
        Builds a value payload

        Arguments:
            value (object): data used to compare against existing data on bugstar. If the object provided isn't a Value
                            object it will be converted to one.
        Keyword Args:
            datatype (object): If Value object isn't provided, this argument is used to determine the type of data.
                               When None is provided, the type is assumed to be that of the current class of that object.

        Returns:
            rsg_clients.bugstar.payloads.search.Value
        """
        return Value(value, datatype=datatype)
