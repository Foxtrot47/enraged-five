"""Constants for the bugstar client."""
import os
import collections

from rsg_clients import const
from rsg_core_py.decorators.classProperty import classProperty


class AppInfo(object):
    """General app information."""

    NAME = "Bugstar"
    VERSION = "0.35.6"
    TITLE = "{0} (Version {1})".format(NAME, VERSION)


class Paths(object):
    """Paths for directories and files used by this library."""

    ROOT_PATH = os.path.dirname(__file__)
    RESOURCES_DIRECTORY = os.path.join(ROOT_PATH, "resources")
    PAYLOAD_DIRECTORY = os.path.join(ROOT_PATH, "payloads")
    TEMPLATES_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "templates")
    TESTS_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "tests")
    ATTACHMENTS_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "attachments")


class Services(object):
    """The versions available of the rest api services."""

    V1 = 1
    V2 = 2


class Servers(object):
    """Paths to the bugstar REST Service that hit particular servers."""

    _BASE_URL = "bugstar.rockstargames.com"

    REST = "gateway"  # Production Server
    REST_DEV = "gateway-dev"  # Development Server
    REST_PREPROD = "gateway-preprod"  # Pre-Production/Release Candidate Server

    MAPPINGS = {
        const.Servers.PRODUCTION: REST,
        const.Servers.PREPRODUCTION: REST_PREPROD,
        const.Servers.DEVELOPMENT: REST_DEV,
    }

    @classmethod
    def getBaseUrl(cls, server):
        """Builds the base url for the given server.

        Arguments:
            server (string): name of the server to build the url for.

        Returns:
            string
        """
        return "https://{}.{}".format(server, cls._BASE_URL)

    @classmethod
    def getRestUrl(cls, server):
        """Builds the base url for running rest commands on the given server.

        Args:
            server (str): name of the server to build the url for.

        Returns:
            str
        """
        url = "{}/api".format(cls.getBaseUrl(server))
        return url

    @classmethod
    def getType(cls, server):
        """The type of server of that is being hit (production, pre-production or development).

        Args:
            server (str): the name of the rest server

        Returns:
            str
        """
        for key, value in cls.MAPPINGS.iteritems():
            if server == value:
                return key


class Fields(object):
    """Fields returned by the get and post requests from Bugstar.

    They are usually represented as xml elements when the payload is an xml.
    """

    # Generic/Shared Fields
    ID = "id"
    NAME = "name"
    CREATED_AT = "createdAt"
    CREATED_BY = "createdBy"
    CREATED_ON = "createdOn"
    MODIFIED_AT = "modifiedAt"
    MODIFIED_BY = "modifiedBy"
    DESCRIPTION = "description"
    VERSION = "version"
    ACTIVE = "active"
    ACTOR_TYPE = "actorType"
    DEVELOPER = "developer"
    TESTER = "tester"
    REVIEWER = "reviewer"
    CATEGORY = "category"
    START_DATE = "startDate"
    TYPE = "type"
    DUE_DATE = "dueDate"
    MISSION = "mission"
    CINEMATIC_ANIMATOR = "cinematicAnimator"
    TAG = "tag"
    VALUE = "value"
    PROJECT = "project"

    # Project Fields
    ACCOUNTING_TYPE = "accountingType"
    ACTIVE_FOR_SEARCH = "activeForSearch"
    AUTOFILL_BUG_OWNER = "autofillBugOwner"
    COMPLETED = "completed"

    HOST_STUDIO = "hostStudio"
    IS_ACTIVE = "isActive"
    IS_VERSIONED = "isVersioned"
    PROJECT_VERSION = "projectVersion"
    USES_IMPORTANCE_SYSTEM = "usesImportanceSystem"

    # Bug Fields
    IS_WORKING = "isWorking"
    IS_ON_HOLD = "isOnHold"
    FIXED_ON_IMPORTANCE = "fixedOnImportance"
    HIGH_PRIORITY_FIX = "highPriorityFix"
    LAST_MODIFIED_ON = "lastModifiedOn"

    # required fields for creating bugs
    PROJECT_ID = "projectId"
    SUMMARY = "summary"
    WORKFLOW = "workflow"
    TAG_LIST = "tagList"
    TEAM_TYPE = "teamType"

    #  "developer", "tester" and "reviewer" are also required fields for creating bugs

    # writable bug fields
    PRIORITY = "priority"
    STATE = "state"
    X = "x"
    Y = "y"
    Z = "z"
    XF = "xf"
    YF = "yf"
    ZF = "zf"
    TODHRS = "todhrs"
    TODMINS = "todmins"
    MAP = "map"
    GRID = "grid"
    COMPONENT = "component"
    ATTACHMENT_LIST = "attachmentList"
    PLATFORM_LIST = "platformList"
    ENVIRONMENT_LIST = "environmentList"
    DEADLINE_LIST = "deadlineList"
    FOUND_IN = "foundIn"
    FIXED_IN = "fixedIn"
    FIXED_ON = "fixedOn"
    ATTEMPTS = "attempts"
    VERIFICATIONS = "verifications"
    SEEN = "seen"
    DIFFICULTY = "difficulty"
    TIME_SPENT = "timeSpent"
    QA_TIME_SPENT = "qaTimeSpent"
    ESTIMATED_TIME = "estimatedTime"
    DO_NOT_MAIL = "doNotMail"
    DO_NOT_EMAIL = "doNotEmail"
    AWAITING_USER = "awaitingUser"
    PROGRESS = "progress"
    ACTIVE_FRAMES = "activeFrames"
    P4_CHANGELISTS = "p4ChangeLists"
    CC_LIST = "ccList"
    VERIFIED_BY = "verifiedBy"
    COMMENTS = "comments"
    UPDATE_FLAG = "updateFlag"
    DEPENDENTS_LIST = "dependentsList"
    BLOCKERS_LIST = "blockersList"
    CHILDREN_LIST = "childrenList"
    PARENT_ID = "parentId"

    # Bug TIme record fields
    BUG_ID = "bugId"
    TIME_TYPE = "timeType"
    DEPARTMENT_TIME_TYPE = "departmentTimeType"
    TIME = "time"
    ATTRIBUTE_TIME_TO_ID = "attributeTimeToId"
    COMMENT_TEXT = "commentText"

    # Comment Fields
    CREATOR = "creator"
    TEXT = "text"

    # Attachment Fields
    ATTACHMENT = "attachment"
    ATTACHMENTS = "attachments"
    ATTACHMENT_ID = "attachmentId"
    FILE_SIZE = "fileSize"
    CONTENT_TYPE = "contentType"
    IS_ARCHIVED = "isArchived"
    ENTITY_TYPE = "entityType"
    CONTENT = "content"
    FILENAME = "filename"

    # User Fields
    FORENAME = "forename"
    SURNAME = "surname"
    LEAD = "lead"
    DEPARTMENT = "department"
    EMAIL = "email"
    USERNAME = "username"
    STUDIO = "studio"
    JOB_TITLE = "jobTitle"
    START_DATE = "startDate"
    IMAGE = "image"
    IS_CONTRACTOR = "isContractor"
    IS_EXTERNAL = "isExternal"
    IS_TEAM_LEAD = "isTeamLead"
    IS_RESTRICTED = "isRestricted"
    IS_TECHNICAL = "isTechnical"
    IS_READ_ONLY = "isReadOnly"
    IS_REPORTED_ON = "isReportedOn"
    ROLES = "roles"

    # Team Fields
    TEAMTYPE = "teamType"
    OWNER = "owner"
    GLOBAL_DEPARTMENT = "globalDepartment"

    # Studio Fields
    ADDRESS = "address"
    DOMAIN = "domain"
    TIMEZONE = "timezone"

    # Global Department
    PARENT = "parent"

    # Mission V1-Only Endpoint return values
    ADMIN_TO_PLAY = "adminToPlay"
    PROJECTED_ATTEMPTS = "projectedAttempts"
    PROJECTED_ATTEMPTS_MIN = "projectedAttemptsMin"
    PROJECTED_ATTEMPTS_MAX = "projectedAttemptsMax"
    CHECKPOINTS = "checkpoints"

    # Mission V2 Fields
    AUDIO_DESIGNER = "audioDesigner"
    CATEGORY = "category"
    CHAPTER = "chapter"
    DESIGNER_INITIALS = "designerInitials"
    ISSUES = "issues"
    INACTIVE = "inactive"
    LEVEL_DESIGNER = "levelDesigner"
    MISSION_ID = "missionId"
    PERCENTAGE_COMPLETE = "percentageComplete"
    PLAYED = "played"
    SCRIPT_NAME = "scriptName"
    SINGLE_PLAYER = "singlePlayer"
    STORY_DESCRIPTION = "storyDescription"
    VARIANT_NUMBER = "variantNumber"

    # Deadline Fields
    DUE = "due"
    DEADLINE_TAG = "deadline_tag"
    REASON = "reason"
    SUBMISSION_DATE = "submissionDate"

    # Search Stats Fields
    TOTAL_DISTINCT_OWNERS = "totalDistinctOwners"
    TOTAL_ESTIMATED = "totalEstimated"
    TOTAL_PROGRESS = "totalProgress"
    TOTAL_TIME_REMAINING = "totalTimeRemaining"
    TOTAL_TIME_SPENT = "totalTimeSpent"
    COUNT = "count"  # Techincally a search field but we add it to the statistics in our code

    # Goal Fields
    GOAL = "goal"
    STATUS = "status"
    ESTIMATED_START = "estimatedStart"
    ESTIMATED_END = "estimatedEnd"

    # Component Fields
    HIERARCHY_NAME = "hierarchyName"


class QueryFields(object):
    """List of fields that can be included in an url to alter the results of returned payload.

    For more info check out https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/swagger-ui/index.html#/Goals/getAll_47
    """

    # Universal V1/V2 Supported Field
    AS_USER = "asUser"  # Usage of this field requires super admin permissions

    # Universal V2 Supported Fields
    PAGE = "page"  # Requested page number
    SIZE = "size"  # Number of items per page
    SORT = "sort"  # One or more sort fields and directions
    FORMAT = "format"  # Returns the result in the desired data format (json/xml) (only supported by V2 endpoints)
    DEBUG = "debug"  # Returns the java stack trace of the error. Useful for logging bugs (true/false)
    LINKS = "links"  # 	If any other fields can be requested over REST, provides a partial URL for those fields in each object
    FIELDS = "fields"  #  Allows the user to return results with the requested fields only

    # V2 Field with limited support
    SEARCH = "search"  # when used in conjuction with endpoints that support it, you can get a result based on a name

    # V1 Bugstar fields
    ID = "id"
    NAME = "name"
    COMMENT = "comment"
    SEND_MAIL = "sendmail"


class SearchFields(object):
    """Fields used by the Search payload.

    The names used to represent the same data are different then the standard output from the other endpoints.
    """

    # Bugstar Search fields
    ID = "id"
    PROJECT = "Task_Project"
    PROJECT_ID = "Task_Project_ID"
    SUMMARY = "Task_Summary"
    DESCRIPTION = "Task_Description"
    BUG_DESCRIPTION = "Bug_Description"
    STATUS = "Task_Status"
    PRIORITY = "Task_Priority"
    DEVELOPER = "Bug_Developer"
    TESTER = "Bug_Tester"
    REVIEWER = "Bug_Reviewer"
    TIME_SPENT = "Task_TimeSpent"
    ESTIMATED_TIME = "Task_EstimatedTime"
    REMAINING_TIME = "Task_Remaining_Time"
    QA_TIME = "Task_QATimeSpent"
    DUE_DATE = "Task_DueDate_Date"
    IMPORTANCE = "Bug_Importance"
    COLOUR = "Bug_Colour"
    CURRENT_OWNER = "Task_CurrentOwner"
    LOCATION = "Bug_Location"
    BLOCKERS = "Task_NoOfBlockers"
    CHILDREN = "Task_NoOfChildren"
    DEPENDENTS = "Task_NoOfDependents"
    ON_HOLD = "Task_OnHold"
    PARENT_ID = "Task_ParentId"
    PROJECTED_OVERDUE = "Task_Projected_Overdue"
    SCREENSHOT_REQUIRED = "Bug_ScreenshotRequired"
    WORKFLOW_TAG = "Task_Workflow_Tag"
    WORKING = "Task_Working"
    MISSION = "Bug_Mission"
    TAG = "Bug_Tag"
    BUG_TAGS = "Bug_Tags"  # When adding this field to search results, field is called Bug_Tags
    # while in the search query field is called Bug_Tag.
    CATEGORY = "Bug_Category"
    LAST_MODIFIED_ON = "BaseEntity_ModifiedAt"
    CREATED_ON = "BaseEntity_CreatedAt"
    MISSION = "Bug_Mission_Name"
    CINEMATIC_ANIMATOR = "Bug_Mission_Cinematic_Animator"
    GOALS = "Bug_Goals"


class Operations(object):
    """Operations accepted by the search payload."""

    IS = "="
    AND = "AND"
    OR = "OR"
    IN = "IN"
    NOT_IN = "NOT IN"
    CONTAINS = "STR CONTAINS"
    EQUALS = "="
    DATE_BETWEEN = "DATE BETWEEN"
    GREATER_THAN = ">="


class SearchValues(object):
    """Commonly used values for comparisons in search operations."""

    CURRENT_PROJECT = "thisProject"
    ALL_PROJECTS = "allUserProjectsIncludingInactive"
    NOT_CLOSED_STATES = "notClosedStates"


class ValueTypes(object):
    """Types that the values of the comparisons can be when passed to the payload."""

    STR = "string"
    INT = "int"
    DBE = "dbe"
    PARAMETER = "parameter"


class DateTimeFormats(object):
    """Commonly used formats by the bugstar api."""

    SEARCH_VALUES = "%Y%m%d %H:%M"


class Endpoints(object):
    """Endpoints accepted by the Bugstar REST API."""

    ACTORS = "Actors"  # Gets data for users, teams, departments, and global departments
    ACTIVE = "Active"  # Gets currently active (valid) components from Bugstar.
    ACTION = "Action"  # Sets a bug state.
    NEXT_ACTIONS = "NextActions"  # Gets viable states the current bug can be switched to
    ATTACHMENT = "Attachment"  # Retrieve attachment information from V2
    ATTACHMENTS = "Attachments"  # Retrieves attachment information
    ATTACHMENT_ASSOCIATIONS = "AttachmentAssociations"  # Deals with attachment association to other containers/bugs.
    BUILDS = "Builds"  # Gets build information
    BUGS = "Bugs"  # Gets bug information
    CATEGORIES = "Categories"  # Gets category information.
    CONTAINER = "Container"  # DIfferent containers
    COMMENTS = "Comments"  # Adds comments to a bug
    COMPONENTS = "Components"  # Gets information about components
    CREATE_ATTACHMENT = "CreateAttachment"  # Creates an attachment on the server
    CURRENT_USER = "CurrentUser"  # Gets information about the current user logged into bugstar
    DATA = "Data"  # Get Graph data
    DATA_POINTS = "DataPoints"  # Get graph data points
    DEADLINES = "Deadlines"  # Get deadline information .
    DEPARTMENTS = "Departments"  # Get department information
    DOWNLOAD = "Download"  # Download the attachment
    GAME_MAPS = "GameMaps"  # Map of the games
    GLOBAL_DEPARTMENTS = "GlobalDepartments"  # Gets Global Department information.
    GRAPHS = "Graphs"  # Gets graph information
    GRIDS = "Grids"  # Gets Map Grid information
    GROUPS = "Groups"  # Gets group endpoint information
    GOALS = "Goals"  # Gets group endpoint information
    INACTIVE = "Inactive"  # Gets currently active (valid) components from Bugstar.
    INVOKE = "Invoke"  # Endpoint for executing an endpoint, currently only supported by the search endpoint
    MAPS = "Maps"  # Gets map information.
    MISSIONS = "Missions"  # Gets  mission information.
    MISSION_CHECKPOINT = "Checkpoint"  # Gets mission checkpoint information
    MEMBERS = "Members"  # Gets the members of a department
    NAME = "Name"  # Gets the component by its name.
    PARAMETERS = "Parameters"  # Gets search parameters
    PRIORITIES = "Priorities"  # Gets the list of bug priorities
    PLATFORMS = "Platforms"  # Gets the platforms supported by our games
    PROJECT = "Project"  # Gets information about a single project
    PROJECTS = "Projects"  # Gets information about the available projects
    PROJECT_MEMBERSHIPS = "ProjectMemberships"  # Gets information about which projects a user has access to.
    PROJECT_ROLES = "ProjectRoles"
    REGISTER_ATTACHMENT = "RegisterAttachment"  # Registers an attachment to the attachment service
    REPORTS = "Reports"  # The relative endpoint for retrieving report information.
    STUDIOS = "Studios"  # The relative endpoint for retrieving studio information
    SEARCH = "Search"  # The relative endpoint for retrieving search information for Users
    SEARCHES = "Searches"  # The relative endpoint for retrieving search information.
    SEARCH_MODE_OPEN_BY_LOCATION = "OpenByLocation"  # The relative endpoint for searching for bugs by location.
    SEARCH_MODE_FULL_TEXT = "FulltextSearch"  # The relative endpoint for searching for bugs via full text.
    SEARCH_IDS = "Ids"  # The relative endpoint for searching for bugs via full text.
    BUG_TIME_RECORD = "BugTimeRecord"  # The relative endpoint for retrieving bug time record information.
    BUG_TIME_RECORDS = "BugTimeRecords"  # The relative endpoint for retrieving bug time record information.
    STUDIOS = "Studios"  # The relative endpoint for retrieving studio information.
    STYLESHEET = "Stylesheet"  # The relative endpoint for retrieving a graphs stylesheet.
    TAGS = "Tags"  # The relative endpoint for retrieving the tags
    TEAMS = "Teams"  # The relative endpoint for retrieving team information.
    TIME_RECORD = "BugTimeRecord"  # The relative endpoint for posting a bugstar time record.
    TEAMS_V2 = "TeamsV2"  # The relative endpoint for retrieving team information.
    TYPE = "Type"  # The relative endpoint for getting a type for a team
    DEFAULTS = "Defaults"  # The relative endpoint for retrieving default information.
    USERS = "Users"  # The relative endpoint for retrieving user information.
    BUILDS = "Builds"  # The relative endpoint for retrieving a default build configuration.
    BUILD_CONFIGURATION = "buildConfiguration"  # The relative endpoint for posting a new build configuration.
    GAME_ASSERTS = "GameAsserts"  # The relative endpoint for retrieving game asserts
    GAME_ASSERT = "GameAssert"  # The relative endpoint for posting game asserts
    DEVELOPERS = "Developers"  # The relative endpoint for retrieving developer ids (bug owners).
    TESTERS = "Testers"  # The relative endpoint for retrieving testers (qaowners).
    V1 = "v1"  # The relative endpoint for retrieving information for version 1 of the bugstar API.
    V2 = "v2"  # The relative endpoint for retrieving information for version 2 of the bugstar API.
    MY = "My"  # The relative endpoint for retrieving information about yourself in version 2 of the bugstar API when used in conjuction with the Project endpoint.
    SEARCH_BY = "SearchBy"  # The relative endpoint for retrieving information as another user
    TOKEN = "token"  # Bugstar uses tokens now for authenticating
    BASIC = "basic"  # Endpoint for requesting a basic token


class Categories(object):
    """The different categories a bug can be set to.

    A is the highest and Wish is the lowest.
    """

    # Categories
    A = "A"
    B = "B"
    C = "C"
    TODO = "TODO"
    TASK = "TASK"
    WISH = "WISH"


class TimeTypes(object):
    """The different ways that time can be marked as when setting it on a bug."""

    # Valid Time Types when adding time to a bug
    WORK = "Work"
    REWORK = "Rework"
    EXPORTING = "Exporting"
    TOOLS_ISSUE = "Tools Issue"
    BUILD_ISSUE = "Build Issue"
    SUPPORT = "Support"

    # The Department to attribute a time to
    DEVELOPMENT = "Development"
    QA = "QA"


class Workflows(object):
    """The different workflows a bug can be set to.

    Depending on the workflow set, the process for closing a bug is different.
    """

    # Workflow Values
    REQUIRES_VERIFICATION = 1
    SIMPLE = 2
    REQUIRES_BUILD = 3

    _WORKFLOW_NAMES = collections.OrderedDict(
        (("Requires Verification", REQUIRES_VERIFICATION), ("Simple", SIMPLE), ("Requires Build", REQUIRES_BUILD))
    )

    @classmethod
    def getWorkflowName(cls, workflow):
        """Get the workflow name based on its value.

        Args:
            workflow (int): the workflow value used by bugstar

        Returns:
            str
        """
        # The bugstar id for the workflow names start at 1 so we subtract one to make sure it matches its position in the list.
        return list(cls._WORKFLOW_NAMES.keys())[workflow - 1]

    @classmethod
    def getWorkflow(cls, name):
        """Get the workflow value based on its name.

        Args:
            name (str): the workflow value used by bugstar

        Returns:
            int
        """
        workflowId = cls._WORKFLOW_NAMES.get(name, None)
        if workflowId is None:
            raise ValueError("{} is not a valid workflow name".format(name))
        return workflowId


class Actions(object):
    """Constant for the different actions that can be performed to change the status of a bug."""

    # The relative endpoints for determining a bug state.
    RESOLVE = "ACTION_RESOLVE"
    VERSION = "ACTION_VERSION"
    WAIVE = "ACTION_WAIVE"
    MARK_AS_DUPLICATE = "ACTION_MARK_AS_DUPLICATE"
    MARK_AS_NOT_SEEN = "ACTION_MARK_AS_NOT_SEEN"
    REQUEST_MORE_INFO = "ACTION_REQUEST_MORE_INFO"
    MARK_AS_NOT_IN_BUILD = "ACTION_MARK_AS_NOT_IN_BUILD"
    CLOSE = "ACTION_CLOSE"
    REOPEN = "ACTION_REOPEN"
    INITIATE = "ACTION_INITIATE"
    POSTPONE = "ACTION_POSTPONE"
    ADD_HOURS_OR_RESOLVE = "ACTION_ADD_HOURS_OR_RESOLVE"  # This action is not supported by the REST Api
    ON_HOLD = "ACTION_ON_HOLD"
    OFF_HOLD = "ACTION_OFF_HOLD"
    ACTMAX = "ACTION_MAX"

    _NAMES = {
        "resolve": RESOLVE,
        "requestMoreInfo": REQUEST_MORE_INFO,
        "markAsNotSeen": MARK_AS_NOT_SEEN,
        "waive": WAIVE,
        "offHold": OFF_HOLD,
        "markAsDuplicate": MARK_AS_DUPLICATE,
        "version": VERSION,
        "notInBuild": MARK_AS_NOT_IN_BUILD,
        "onHold": ON_HOLD,
        "close": CLOSE,
        "postpone": POSTPONE,
        "initiate": INITIATE,
        "reopen": REOPEN,
        "addHoursOrResolve": RESOLVE,
    }

    @classmethod
    def getActionFromName(cls, name):
        """Get and action based on its user friendly name.

        Args:
            name (str): The user friendly name that appears on the bugstar UI.

        Returns:
            str
        """
        return cls._NAMES.get(name, None)


class States(object):
    """Constant for the different states a bug can be in."""

    # States (Currently not all states are included)
    DEV_FIXING = "DEV"
    DEV_REOPEN = "DEV_FAIL"
    DEV_FIXED_WAITING_BUILD = "BUILD_RESOLVED"
    DEV_PLANNED = "PLANNED"
    TEST_VERIFYING = "TEST_TO_VERIFY"
    TEST_NEED_MORE_INFO = "TEST_MORE_INFO"
    CLOSED_FIXED = "CLOSED_FIXED"
    CLOSED_WAIVED = "CLOSED_WAIVED"
    CLOSED_DUPLICATE = "CLOSED_DUPLICATE"
    BLOCKED_PLANNED = "BLOCKED_PLANNED"
    BLOCKED = "BLOCKED"
    ON_HOLD = "ON_HOLD"
    REVIEW_FIXED = "REVIEW_FIXED"
    REVIEW_WAIVED = "REVIEW_WAIVED"
    REVIEW_DUPLICATE = "REVIEW_DUPLICATE"
    REVIEW_NOT_SEEN = "REVIEW_NOT_SEEN"

    # Groupings of States
    CLOSED = (CLOSED_FIXED, CLOSED_DUPLICATE, CLOSED_WAIVED)
    FIXED = (CLOSED_FIXED, REVIEW_FIXED)
    FIXING = (DEV_FIXING, DEV_REOPEN)
    NOT_CLOSED = (DEV_FIXING, DEV_REOPEN, DEV_PLANNED, BLOCKED, BLOCKED_PLANNED, TEST_VERIFYING, TEST_NEED_MORE_INFO)

    # not all states are currently covered by an action
    # To get to a particular state, a bug must be in a state that can transition to that state.
    # Depending on the workflow required for a bug, a different action maybe required as well
    _DEFAULT_STATE_ACTIONS = {
        ON_HOLD: {"default": Actions.ON_HOLD},
        BLOCKED_PLANNED: {"default": Actions.POSTPONE},
        DEV_REOPEN: {"default": Actions.REOPEN},
        DEV_FIXED_WAITING_BUILD: {"default": Actions.MARK_AS_NOT_IN_BUILD},
        DEV_FIXING: {
            "default": Actions.INITIATE,
            ON_HOLD: Actions.OFF_HOLD,
            CLOSED_WAIVED: Actions.REOPEN,
            CLOSED_DUPLICATE: Actions.REOPEN,
            CLOSED_FIXED: Actions.REOPEN,
        },
        CLOSED_DUPLICATE: {"default": Actions.MARK_AS_DUPLICATE},
        CLOSED_FIXED: {"default": Actions.CLOSE},
        CLOSED_WAIVED: {"default": Actions.WAIVE},
        TEST_VERIFYING: {"default": Actions.RESOLVE},
        TEST_NEED_MORE_INFO: {"default": Actions.REQUEST_MORE_INFO},
        DEV_PLANNED: {"default": Actions.POSTPONE},
        BLOCKED: {"default": Actions.ON_HOLD, BLOCKED_PLANNED: Actions.INITIATE},
    }

    _REQUIRES_BUILD_ACTIONS = {CLOSED_FIXED: {"default": Actions.RESOLVE}}
    _REQUIRES_VERIFICATION_ACTIONS = {CLOSED_FIXED: {"default": Actions.RESOLVE, TEST_VERIFYING: Actions.CLOSE}}
    _STATE_ACTIONS = {}

    _STATE_FULLNAMES = {
        DEV_FIXING: "dev (fixing)",
        DEV_REOPEN: "dev (reopen)",
        DEV_FIXED_WAITING_BUILD: "dev (fixed, waiting build)",
        DEV_PLANNED: "dev (planned)",
        TEST_VERIFYING: "test (verifying)",
        TEST_NEED_MORE_INFO: "test (need more info)",
        CLOSED_FIXED: "closed (fixed)",
        CLOSED_WAIVED: "closed (waived)",
        CLOSED_DUPLICATE: "closed (duplicate)",
        BLOCKED_PLANNED: "blocked (planned)",
        BLOCKED: "blocked",
        ON_HOLD: "on hold",
    }

    @classProperty
    def stateActions(cls):
        """Dictionary that maps which states are triggered by which actions.

        These actions change depending on the workflow set on the bug.

        Returns:
            dict
        """
        if not cls._STATE_ACTIONS:
            for workflow, changes in {
                Workflows.SIMPLE: {},
                Workflows.REQUIRES_BUILD: cls._REQUIRES_BUILD_ACTIONS,
                Workflows.REQUIRES_VERIFICATION: cls._REQUIRES_VERIFICATION_ACTIONS,
            }.iteritems():
                actions = dict(cls._DEFAULT_STATE_ACTIONS)
                actions.update(changes)
                cls._STATE_ACTIONS[workflow] = actions
        return cls._STATE_ACTIONS

    @classmethod
    def getActionFromState(cls, workflow, state, previousState=None):
        """Gets the appropriate action for the corresponding state.

        Args:
            workflow (int): the workflow of the bug as it determines what actions are viable for a bug.
            state (str): bug state whose corresponding action needs to be retrieved
            previousState (str, optional): bug state that is being switched from. Depending on the current state of a bug,
                                 a different action than the standard one may be required to get it to the desired state
        Returns:
            str or None
        """
        workflowActions = cls.stateActions.get(workflow, cls._DEFAULT_STATE_ACTIONS)
        actions = workflowActions.get(state, {})
        action = actions.get(previousState, actions.get("default", None))
        return action

    @classmethod
    def getFullNameFromState(cls, state):
        """Gets the appropriate description for the corresponding state.

        Args:
            state (str): bug state whose corresponding description needs to be retrieved

        Returns:
            str or None
        """
        return cls._STATE_FULLNAMES.get(state, None)

    @classmethod
    def getStateFromAction(cls, action, workflow=None):
        """Gets the appropriate state for the corresponding action.

        Args:
            action (str): action whose corresponding state needs to be retrieved

            workflow (str, optional): the workflow to use for getting the correct action that corresponds to a status.
                            defaults to simple workflow
        Returns:
            str or None
        """
        actions = cls.stateActions.get(workflow, cls._DEFAULT_STATE_ACTIONS)
        for key, value in actions:
            if value == action:
                return key

    @classmethod
    def getStateFromFullName(cls, name):
        """Gets the appropriate state for the corresponding description.

        Arguments:
            name (str): description whose corresponding bug state needs to be retrieved

        Returns:
            str or None
        """
        for key, value in cls._STATE_FULLNAMES.iteritems():
            if value == name:
                return key

    @classmethod
    def getStateSearchName(cls, name):
        """Gets the appropriate state name when we search for bugs with a certain state.

        The search states are of type dbe and have "TASK_STATE/" in front of their actual name.

        Returns:
            str
        """
        return "TASK_STATE/{}".format(name)


class ProjectRoles(object):
    """The type of roles that a User or Team may be assigned."""

    BUG_IMPORTANCE_MANAGER = "BugImportanceManager"
    PROJECT_BUILD_ADMIN = "ProjectBuildAdmin"
    DEADLINE_MANAGER = "DeadlineManager"
    GOAL_MANAGER = "GoalManager"
    GOAL_UPDATER = "GoalUpdater"
    OUTSOURCE_ADMIN = "OutsourceAdmin"
    REPORT_ADMIN = "ReportAdmin"
    PROJECT_USER_ADMIN = "ProjectUserAdmin"
    COMPONENT_MANAGER = "ComponentManager"
    DEADLINE_ASSIGNER = "DeadlineAssigner"
    MISSIONG_MANAGER = "MissionManager"
    SEARCH_MANAGER = "SearchManager"
    ADMIN = "Admin"
    OWNER = "Owner"
    QA_OWNER = "QAOwner"
    REVIEWER = "Reviewer"
