import datetime

from xml.etree import cElementTree as xml

from rsg_core import conversions
from rsg_clients.bugstar import const
from rsg_clients.bugstar.payloads import basePayload


class Deadline(basePayload.BasePayload):
    """
    Represents a deadline from bugstar

    Properties:
        pass

    Notes:
        Fields used by the deadline payload.

        const.Fields.ID
        const.Fields.ACTIVE
        const.Fields.CREATED_BY
        const.Fields.CREATED_AT
        const.Fields.DUE
        const.Fields.MODIFIED_AT
        const.Fields.MODIFIED_BY
        const.Fields.NAME
    """

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element(const.Endpoints.DEADLINES)

        for key, value in self._fields.iteritems():

            if value is None:
                continue
            valueElement = xml.Element(key)

            if isinstance(value, (self.server.user.cls(), self.server.team.cls())):
                valueElement.text = str(value.id)
            else:
                valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Args:
            dictionary(dict): data from bugstar to parse
        """
        self._fields.update(dictionary)

    @property
    def active(self):
        """ active is readable bugstar property """
        return self._getattr(const.Fields.ACTIVE)

    @property
    def project(self):
        """ active is readable bugstar property """
        projectId = self._getattr(const.Fields.PROJECT)
        if projectId:
            return self.server.project(projectId)

    @property
    def name(self):
        """ name is readable bugstar property """
        return self._getattr(const.Fields.NAME)

    @property
    def deadlineTag(self):
        """ deadlineTag is readable bugstar property """
        return self._getattr(const.Fields.DEADLINE_TAG)

    @property
    def createdBy(self):
        """ createdBy is readable bugstar property """
        value = self._getattr(const.Fields.CREATED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.user(value)
        return value

    @property
    def createdAt(self):
        """ createdAt is readable bugstar property """
        return self._getattr(const.Fields.CREATED_AT)

    @property
    def startDate(self):
        """ Start date is readable bugstar property """
        value = self._getattr(const.Fields.START_DATE)
        if isinstance(value, int):
            value = datetime.datetime.utcfromtimestamp(value / 1000)
            self._fields[const.Fields.START_DATE] = value
        return value

    @property
    def submissionDate(self):
        """ Submission date is readable bugstar property """
        value = self._getattr(const.Fields.SUBMISSION_DATE)
        if isinstance(value, int):
            value = datetime.datetime.utcfromtimestamp(value / 1000)
            self._fields[const.Fields.SUBMISSION_DATE] = value
        return value

    @property
    def due(self):
        """ Due is readable bugstar property """
        value = self._getattr(const.Fields.DUE_DATE)
        if isinstance(value, int):
            value = datetime.datetime.utcfromtimestamp(value / 10000)
            self._fields[const.Fields.DUE_DATE] = value
        return value

    @property
    def reason(self):
        """ Reasons is readable bugstar property """
        return self._getattr(const.Fields.REASON)

    @property
    def modifiedBy(self):
        """ modifiedBy is readable bugstar property """
        value = self._getattr(const.Fields.MODIFIED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.user(value)
        return value

    @property
    def modifiedAt(self):
        """ modifiedAt is readable bugstar property """
        return self._getattr(const.Fields.MODIFIED_AT)


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the Deadline class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Deadline
