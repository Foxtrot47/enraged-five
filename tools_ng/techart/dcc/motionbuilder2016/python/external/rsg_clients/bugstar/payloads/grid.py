from xml.etree import cElementTree as xml
from rsg_clients import const as clientsConst
from rsg_clients.bugstar import const
from rsg_clients.bugstar.payloads import basePayload


class Grid(basePayload.BasePayload):
    """
    Bugstar Payload for grids

    Properties:
        id(int): id of the attachment
        name(str): name of the file without the extension

    """

    # The Grid endpoint errors out if you return the data as xml
    DEFAULT_GET_ACCEPT_TYPE = clientsConst.ContentType.JSON_UTF8
    IS_READONLY = False
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element("Grid")

        for key, value in self._fields.iteritems():
            if value is None:
                continue

            valueElement = xml.Element(key)
            valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Args:
            dictionary (dict): data to parse
        Returns:
            xml.etree.cElementTree.Element
        """

        self._fields.update(dictionary)

    @classmethod
    def _getData(cls, server, index, returnRaw=False, fields=None, isV2=True, command=None, acceptType=None):
        return super(Grid, cls)._getData(
            server,
            index,
            returnRaw=returnRaw,
            fields=fields,
            isV2=isV2,
            command=command,
            acceptType=acceptType or clientsConst.ContentType.JSON_UTF8,
        )

    def toString(self, isV2=True):
        """
        Overrides inherited method

        Converts the xml element that represents this data to the string

        Args:
            isV2 (bool): data from a V2 query

        Returns:
            string
        """
        return super(Grid, self).toString(isV2=isV2)

    @property
    def name(self):
        """ name is readable grid property"""
        return self._getattr(const.Fields.NAME)

    @name.setter
    def name(self, value):
        """ name is writeable grid property"""
        return self._setattr(const.Fields.NAME, value)

    @property
    def project(self):
        """ active is readable bugstar property """
        projectId = self._getattr(const.Fields.PROJECT)
        if projectId:
            return self.server.project(projectId)


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the User class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Grid
