from xml.etree import cElementTree as xml

from rsg_core import conversions
from rsg_core_py.decorators import memoize
from rsg_clients.bugstar import const
from rsg_clients.bugstar.payloads import basePayload, actor


class User(actor.Actor):
    """
    Class for interacting with user information from Bugstar

    Notes:
        self._fields[const.Fields.ID] = index
        self._fields[const.Fields.NAME] = None
        self._fields[const.Fields.FORENAME] = None
        self._fields[const.Fields.SURNAME] = None
        self._fields[const.Fields.VERSION] = None
        self._fields[const.Fields.LEAD] = None
        self._fields[const.Fields.DEPARTMENT] = None
        self._fields[const.Fields.VERSION] = None
        self._fields[const.Fields.CREATED_BY] = None
        self._fields[const.Fields.CREATED_AT] = None
        self._fields[const.Fields.MODIFIED_BY] = None
        self._fields[const.Fields.MODIFIED_AT] = None
        self._fields[const.Fields.ACTIVE] = None
        self._fields[const.Fields.EMAIL] = None
        self._fields[const.Fields.ACTOR_TYPE] = None
        self._fields[const.Fields.USERNAME] = None
        self._fields[const.Fields.STUDIO] = None
        self._fields[const.Fields.JOB_TITLE] = None
        self._fields[const.Fields.START_DATE] = None
        self._fields[const.Fields.IMAGE] = None
        self._fields[const.Fields.IS_CONTRACTOR] = None
        self._fields[const.Fields.IS_EXTERNAL] = None
        self._fields[const.Fields.IS_TEAM_LEAD] = None
        self._fields[const.Fields.IS_RESTRICTED] = None
        self._fields[const.Fields.IS_TECHNICAL] = None
        self._fields[const.Fields.IS_READ_ONLY] = None
        self._fields[const.Fields.IS_REPORTED_ON] = None

    Examples:

        from rsg_clients.bugstar import servers

        user = servers.production.user.getByName("gameasserts")

        # Get information on the user and other users associated with them
        print user
        print user.lead
        print user.createdBy
    """

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []
    _COMMAND = const.Endpoints.USERS
    TYPE = const.Endpoints.USERS

    def __new__(cls, bugstar, index=None, *args, **kwargs):
        """
        Overrides built-in method

        Stores & returns the instance of the class if an argument is provided, otherwise creates a new instance

        Args:
            bugstar (rsg_clients.bugstar.server.BaseServer): the local server to set for this object
            *args (list): additional parameters accepted by the inherited class
            **kwargs (dict): additional keyword arguments accepted by the inherited class

        Keyword Args:
            index (int): the id of the bug on bugstar

        Returns:
            object
        """
        return cls._getInstance(bugstar, index)

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element("User")
        for key, value in self._fields.iteritems():
            if value is None:
                continue
            valueElement = xml.Element(key)

            if isinstance(
                value,
                (
                    self.server.user.cls(),
                    self.server.team.cls(),
                    self.server.studio.cls(),
                    self.server.department.cls(),
                    self.server.globalDepartment.cls(),
                ),
            ):
                valueElement.text = str(value.id)
            else:
                valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Args:
            dictionary(dict): data to parse
        """
        # People's name may have invalid ascii characters
        dictionary[const.Fields.NAME] = conversions.convertToAscii(dictionary.get(const.Fields.NAME, ""))
        self._fields.update(dictionary)

    def toString(self, isV2=True):
        """
        Overrides inherited method

        Converts the xml element that represents this data into a string

        Arguments:
            isV2 (bool): is the data from a V2 query

        Returns:
            string
        """
        return super(User, self).toString(isV2=isV2)

    @property
    def name(self):
        """ name is readable bugstar property """
        return self._getattr(const.Fields.NAME)

    @property
    def forename(self):
        """ forename is readable bugstar property """
        return self._getattr(const.Fields.FORENAME)

    @property
    def surname(self):
        """ surname is readable bugstar property """
        return self._getattr(const.Fields.SURNAME)

    @property
    def version(self):
        """ version is readable bugstar property """
        return self._getattr(const.Fields.VERSION)

    @property
    def lead(self):
        """ lead is readable bugstar property """
        value = self._getattr(const.Fields.LEAD)
        if isinstance(value, int):
            # Get data from Bugstar
            value = User(self.server, value)
        return value

    @property
    def department(self):
        """ department is readable bugstar property """
        # TODO: Switch to use department class
        return self._getattr(const.Fields.DEPARTMENT)

    @property
    def version(self):
        """ version is readable bugstar property """
        return self._getattr(const.Fields.VERSION)

    @property
    def createdBy(self):
        """ createdBy is readable bugstar property """
        value = self._getattr(const.Fields.CREATED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = User(self.server, value)
        return value

    @property
    def createdAt(self):
        """ createdAt is readable bugstar property """
        return self._getattr(const.Fields.CREATED_AT)

    @property
    def modifiedBy(self):
        """ modifiedBy is readable bugstar property """
        value = self._getattr(const.Fields.MODIFIED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = User(self.server, value)
        return value

    @property
    def modifiedAt(self):
        """ modifiedAt is readable bugstar property """
        return self._getattr(const.Fields.MODIFIED_AT)

    @property
    def active(self):
        """ active is readable bugstar property """
        return self._getattr(const.Fields.ACTIVE)

    @property
    def email(self):
        """ email is readable bugstar property """
        return self._getattr(const.Fields.EMAIL)

    @property
    def actorType(self):
        """ actorType is readable bugstar property """
        return self._getattr(const.Fields.ACTOR_TYPE)

    @property
    def username(self):
        """ username is readable bugstar property """
        return self._getattr(const.Fields.USERNAME)

    @property
    def studio(self):
        """ studio is readable bugstar property """
        value = self._getattr(const.Fields.STUDIO)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.studio(value)
        return value

    @property
    def jobTitle(self):
        """ jobTitle is readable bugstar property """
        return self._getattr(const.Fields.JOB_TITLE)

    @property
    def startDate(self):
        """ startDate is readable bugstar property """
        return self._getattr(const.Fields.START_DATE)

    @property
    def image(self):
        """ image is readable bugstar property """
        return self._getattr(const.Fields.IMAGE)

    @property
    def isContractor(self):
        """ isContractor is readable bugstar property """
        return self._getattr(const.Fields.IS_CONTRACTOR)

    @property
    def isExternal(self):
        """ isExternal is readable bugstar property """
        return self._getattr(const.Fields.IS_EXTERNAL)

    @property
    def isTeamLead(self):
        """ isTeamLead is readable bugstar property """
        return self._getattr(const.Fields.IS_TEAM_LEAD)

    @property
    def isRestricted(self):
        """ isRestricted is readable bugstar property """
        return self._getattr(const.Fields.IS_RESTRICTED)

    @property
    def isTechnical(self):
        """ isTechnical is readable bugstar property """
        return self._getattr(const.Fields.IS_TECHNICAL)

    @property
    def isReadOnly(self):
        """ isReadOnly is readable bugstar property """
        return self._getattr(const.Fields.IS_READ_ONLY)

    @property
    def isReportedOn(self):
        """ isReportedOn is readable bugstar property """
        return self._getattr(const.Fields.IS_REPORTED_ON)

    @property
    def isCurrentUser(self):
        """
        Is this user the current user authenticated against the server

        Returns:
            bool
        """
        return self.server.account.name == self.username

    @property
    @memoize.memoize
    def projects(self):
        """
        Gets the projects that this user has access to.

        This command will error out if the user is not the current user or the account used to access bugstar
        does not have privileges to perform actions on behalf of another user

        Returns:
            list
        """
        fields = None
        sections = [self.server.restUrl, const.Endpoints.V2, const.Endpoints.PROJECTS, const.Endpoints.MY]
        if not self.isCurrentUser:
            fields = {const.QueryFields.AS_USER: self.id}

        url = self.server.buildUrl(sections, fields=fields)

        projects = []
        results = self.server.get(url)
        if results:
            projectsData = results.get("items", {})

            if isinstance(projectsData, dict):
                # The parsed xml will return a dictionary if the user only has access to one project
                projectsData = [projectsData]

            for data in projectsData:
                projectId = int(data.get(const.Fields.ID))
                projects.append(self.server.project(projectId))
                projects[-1].parse(dict(data), isV2=True)
        return projects

    def teams(self, project):
        """
        List of teams that this user belongs to within the given project

        Returns:
            list
        """
        command = "/".join(
            [
                const.Endpoints.TEAMS,
                const.Endpoints.PROJECTS,
                str(project.id),
                const.Endpoints.ACTORS,
                str(self.id),
                const.Endpoints.TYPE,
            ]
        )
        results = (
            self._getData(self.server, "Default", command=command) or {}
        )  # incase the given user does not belong to any teams

        teams = results.get("items", [])
        if not isinstance(teams, list):
            # If only one team is returned, then the team key will have a single dict rather then a list
            teams = [teams]

        return [self.server.team(index=int(data[const.Fields.ID]), data=data) for data in teams]

    @property
    def roles(self):
        """List of global roles that this user can have and is assigned.

        Notes:
            A user can have the following roles enabled.

            * superAdmin
            * featureTester
            * timestarUser
            * timestarAdmin
            * timesheetValidator
            * timesheetEditor
            * globalUserAdmin

        Returns:
            dict
        """
        return self._getattr(const.Fields.ROLES)

    @property
    def isSuperAdmin(self):
        """Does this user have super admin privilages."""
        roles = self.roles
        if roles is not None:
            return roles.get("superAdmin", None) is True
        return False


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the User class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return User

    @memoize.memoize
    def getByName(self, name):
        """
        Get user data from bugstar based on the username

        Args:
            name (str): username to look for

        Returns:
            User
        """
        dictionary = User._getData(self._server, name)
        if not dictionary:
            return None

        user = User(self._server, int(dictionary[const.Fields.ID]))
        user.parse(dictionary, isV2=True)
        return user
