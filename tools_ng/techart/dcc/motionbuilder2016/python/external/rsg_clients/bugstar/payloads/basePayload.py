import json
import types
import datetime
import xmltodict
from xml.dom import minidom
from xml.etree import cElementTree as xml

from rsg_core import conversions
from rsg_core_py.decorators import memoize
from rsg_core_py.abstracts import abstractBuilder, abstractPayload
from rsg_core_py.metaclasses import mapping
from rsg_clients.bugstar import const, exceptions


class BaseBuilder(abstractBuilder.AbstractPayloadBuilder):
    """
    Base Builder for building bugstar payloads
    """

    def _clearCache(self, all=False):
        """
        This clears out the memory caches of the entities that have been queried from Shotgun.
        This class is strictly for debugging purposes

        Keyword Args:
            all (bool): clear the cache for all classes that subclass the BaseEntity class.
        """
        self.cls()._INSTANCES = {}
        if all:
            for subclass in BasePayload.__subclasses__():
                subclass._INSTANCES = {}

    def getByName(self, name, all=False, project=None, command=const.Endpoints.ACTIVE):
        """
        Gets the bugstar object by its name.

        Note: This code uses an endpoint that currently isn't mass supported, this method may need to be reimplemented
              in inherited classes in the meantime

        Args:
            name (str): name to search for, if other entites start with the same name then those will be returned as well.

        Keyword Args:
            all (bool): retun all the entites matched, otherwise just return the first match.
            project (rsg_clients.bugstar.payloads.project.Project): project to search for data in. Some endpoints have
                        can only perform this search when the project is included.
            command (str): additional command/endpoint to include before the SearchBy endpoint. Defaults to Active.

        Raises:
            ValueError : This method expected a string and didn't receive one or the string was empty
        Returns:
            list or None
        """
        if not isinstance(name, basestring):
            raise ValueError("Expected string or unicode, received {} instead".format(type(name)))
        elif not name:
            raise ValueError("Received an empty string")

        cls = self.cls()
        commands = [const.Endpoints.SEARCH_BY]
        if project is not None:
            commands[0:0] = [const.Endpoints.PROJECTS, str(project.id)]

        if command:
            commands.insert(0, command)

        endpoint = "/".join(commands)

        data = cls._getData(self._server, endpoint, fields={const.QueryFields.SEARCH: name})
        results = data.get("items", None)  # The key for the individual data should be the name of the class

        if results is not None:
            if not isinstance(results, list):
                results = [results]
            if not all:
                results = results[:1]
            return [cls(self._server, int(result[const.Fields.ID])) for result in results] or None


class BasePayload(abstractPayload.AbstractPayload):
    """ Base class for bugstar payloads """

    __metaclass__ = mapping.Mapping
    SERVICE_VERSION = const.Services.V2  # version of the rest api
    DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
    IS_READONLY = True  # Is this class readonly
    IS_REQUESTABLE = True  # Does this class have an endpoint to request more data
    XML_EXCLUDE_FIELDS = None  # Fields that should not be included in the payload for creating/updating queries

    # The default format that endpoints should return data as
    _INDEX_FIELD = const.Fields.ID  # The field to get the index from
    _INVALID_SERVER_ERROR = exceptions.InvalidServer  # The error to use when an invalid server is used
    _IS_READONLY_ERROR = exceptions.ReadOnlyError  # The error to invoke when a non-editable payload is edited

    _COMMAND = None  # The rest command used to query information about data the class represents
    _LAST_ACCESSED_INSTANCES = []

    def __new__(cls, bugstar, index=None, *args, **kwargs):
        """
        Overrides built-in method

        Stores & returns the instance of the class if an argument is provided, otherwise creates a new instance

        Args:
            bugstar (rsg_clients.bugstar.core.Connection): the local server to set for this object
            *args (list): additional parameters accepted by the inherited class
            **kwargs (dict): additional keyword arguments accepted by the inherited class

        Keyword Args:
            index (int): the id of the bug on bugstar

        Returns:
            object
        """
        # There are cases where we have may get non-int values for the index and this breaks the caching
        if not isinstance(index, (int, types.NoneType)):
            raise ValueError("Expected int or None, received {} instead".format(type(index).__name__))
        return cls._getInstance(bugstar, index)

    def __init__(self, bugstar, index=None, data=None):
        """
        Initializer

        Arguments:
            server (rsg_clients.bugstar.server.BaseServer): the local server to set for this object

        Keyword Args:
            index (int): id of the object from the server
            data (dictionary): data that represents this object
        """
        if self._isCached(bugstar, index, self) and self._isCachable:
            if data is not None:
                # Helps refresh the data when the data is required
                self.refresh(data=data)
            return

        if index is None and self.IS_READONLY:
            raise self._IS_READONLY_ERROR(
                "{} is a read only class.\n"
                "An id is required to create an instance of this class.".format(self.__class__.__name__)
            )
        self._isValid = None  # This will get set a value once data is pulled from the server
        # properties to store
        if isinstance(data, dict):
            # The class name is usually the top level key and it is capitalized, so it should not conflict wth field names
            # field names are always lowercase
            data = data.get(self.className(), data) # This is now old and should be phased out. The new results no longer include the data type as the header.
        self._setup(index, data, bugstar)
        self._cache()

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.payloads.basePayload.BaseBuilder
        """
        raise NotImplementedError

    @classmethod
    def command(cls):
        """
        Reimplements inherited method
        Returns the value to use as a key for retrieving this class

        Returns:
            str
        """
        if cls.IS_REQUESTABLE:
            if cls._COMMAND is None:
                cls._COMMAND = "{}s".format(cls.__name__)
            return cls._COMMAND

    def _xmlElementV1(self):
        """
        Builds the xml element from dictionary data obtained from bugstar using a get query to version 1 of the bugstar rest api.

        This method should be reimplemented in inherited classes and return an xml.etree.cElementTree.Element object
        """
        raise NotImplementedError

    def _setup(self, index=None, data=None, server=None):
        super(BasePayload, self)._setup(index, data, server)
        self._isValid = None  # TODO: Confirm that this doesn't overlap with the exists flag in behavior.

    def _xmlElementV2(self):
        """
        Builds the xml element from dictionary data obtained from bugstar using a query to version 2 of the bugstar rest api.

        This method should be reimplemented in inherited classes and return an xml.etree.cElementTree.Element object
        """
        raise NotImplementedError

    def _parseV1(self, dictionary):
        """
        Reads and sets the dictionary data obtained from bugstar using a post query to version 1 of the bugstar rest api.
        """
        raise NotImplementedError

    def _parseV2(self, dictionary):
        """
        Reads and sets the dictionary data obtained from bugstar using a post query to version 2 of the bugstar rest api.
        """
        raise NotImplementedError

    def refresh(self, command=None, data=None):
        """
        Performs the bugstar query required to get the data to fill this object with.

        Args:
            command (str): command to run or the id of the payload type we want to grab from bugstar
            data (dict): data from bugstar to refresh the instance with
        """
        dictionary = data
        isV2 = self.SERVICE_VERSION == const.Services.V2
        if not isinstance(data, dict):
            dictionary = self._getData(self.server, command or self.id, isV2=isV2)
        # Using the class name should now be obsolte et
        self.parse(dictionary.get(self.className(), dictionary), isV2=isV2)
        self._removeStalePayloads(instance=self)

    @classmethod
    def _validateText(cls, element):
        """
        Virtual Method; should be reimplemented in the inherited class

        Returns the attrib dictionary passed in with correct key names that are accepted by the __init__ of this class.

        Arguments:
            element (xml.etree.cElementTree): an xml.etree.elementTree.Element instance to grab text data from

        Returns:
            list
        """
        return []

    @classmethod
    def _validateAttributes(cls, attrib):
        """
        Virtual Method; should be reimplemented in the inherited class

        Returns the attrib dictionary passed in with correct key names that are accepted by the __init__ of this class.

        Arguments:
            attrib (dictionary): attribute dictionary from an xml.etree.elementTree.Element instance

        Returns:
            dictionary
        """
        raise NotImplementedError

    def _validate(self, requiredProperties):
        """
        Validates that the bug has the data required to perform a create or update api command

        Arguments:
            requiredProperties (list): list of properties that need to have values before performing a REST command
        """
        for requiredProperty in requiredProperties:
            if self._fields.get(requiredProperty, None) is None:
                raise exceptions.MissingValue("A value needs to be set for the {} property".format(requiredProperty))

    @classmethod
    def _getData(cls, server, index, returnRaw=False, fields=None, isV2=True, command=None, acceptType=None):
        """
        Performs a get request against the server to retrieve information on data on a single object based on the
        data type that the class represents

        Arguments:
            server (rsg_clients.bugstar.server.BaseServer): server to connect to
            index (str): the index of the object or command to run

        Keyword Args:
            returnRaw (bool): return the data from the request as is (usually a string)
            fields (dictionary): the field name and its value to include in the url for the get request
            isV2 (bool): is this a v2 query; default is True
            command (str): the command to run in the url, defaults to the command for the class if nothign is passed in.
            acceptType (str):  the format that the data pulled from the rest query should be returned as.

        Returns:
            list
        """
        restUrl = server.restUrl
        sections = [restUrl, const.Endpoints.V2 if isV2 else const.Endpoints.V1, command or cls.command()]
        if index:
            sections.append(index)
        fields = fields or {}
        if server.delegate is not None:
            fields[const.QueryFields.AS_USER] = server.delegateUser.id
        url = server.buildUrl(sections, fields=fields)
        return server.get(url, returnRaw=returnRaw, acceptType=acceptType)

    def _getattr(self, item):
        """
        Gets data from bugstar if it has not been queried recently.

        Arguments:
            item (str): name of the property to get data from

        Returns:
            object
        """
        self._setLastAccessedInstance(self)
        # With the exception of bugs and attachments, most bugstar data is not meant to be edited
        if self._id and not self._isUpdated and item != const.Fields.ID:
            self.refresh()
        return self._fields.get(item, None)

    @staticmethod
    @memoize.memoize
    def _getSubclassByTagName(tag):
        """
        Dynamically resolves which subclass the given tag is associated with

        Arguments:
            tag (str): tag name from an xml element

        Returns:
            AbstractPayload Subclass or None
        """
        for subclass in BasePayload.__subclasses__():
            if tag.strip() == getattr(subclass, "TAG_NAME", None):
                return subclass

    @classmethod
    def _getValidPayload(cls, index, builders):
        """
        Gets the correct payload for the index based on the provided payloads

        Args:
            index (index): index for the payload
            builders (list): list of builders to attempt to build an entity for

        Returns:
            rsg_clients.bugstar.payload.basePayload.BasePayload
        """
        for builder in builders:
            payload = builder(index)
            if not payload.isValid:
                continue
            return payload
        raise exceptions.InvalidType(
            "The following payloads are not valid types for id {}:\n{}".format(
                index, "\n".join(builder.cls().command() for builder in builders)
            )
        )

    def _getValidUserPayload(self, index):
        """
        Convenience method for getting a valid user/team payload

        Args:
            index (index): index for the payload

        Returns:
            rsg_clients.bugstar.payload.user.User or rsg_clients.bugstar.payload.team.Team
        """
        return self._getValidPayload(index, [self.server.user, self.server.team])

    @classmethod
    def key(cls):
        """
        Name to use as key for dictionaries returned from bugstar to find information about this type of data

        Returns:
            str
        """
        return cls.command()

    @classmethod
    def convertElement(cls, element):
        """
        Converts an xml element from a search xml generated from bugstar into the search object hierarchy

        Arguments:
            element (xml.etree.cElementTree): element to convert to a search element object

        Raises:
            BugstarException when an invalid xml element is provided

        Returns:
            AbstractPayload
        """
        if element.tag.strip() != cls.TAG_NAME:
            raise exceptions.BugstarException(
                "{} is not a valid search xml element".format(element.tag), includeErrorCode=False
            )
        args = cls._validateText(element)
        kwargs = cls._validateAttributes(element.attrib)

        instance = cls(*args, **kwargs)
        for child in element:
            # dynamically get the class from the element tag
            objectClass = cls._getSubclassByTagName(child.tag.strip())
            if objectClass is None:
                continue
            childInstance = objectClass.convertElement(child)
            instance.add(childInstance)
        return instance

    @property
    def id(self):
        """ id is readable bugstar property """
        return self._id

    def xmlElement(self, isV2=False):
        """
        An xml element that represents this payload

        Returns:
            xml.etree.cElement.Element
        """
        if isV2:
            return self._xmlElementV2()
        return self._xmlElementV1()

    def parse(self, dictionary, isV2=False):
        """
        Parses a dictionary generated from bugstar rest api and adds its contents to this instance.

        Args:
            dictionary (dict): dictionary to parse
        Keyword Args:
            isV2 (bool): is the dictionary generated using version 2 of the REST Api
        """
        try:
            # remove invalid values
            dictionary = conversions.convertValue(dictionary)
            if isV2:
                value = self._parseV2(dictionary)
            else:
                value = self._parseV1(dictionary)

            self._isUpdated = True
            self._isValid = True
            self._lastDataUpdate = datetime.datetime.utcnow()
            return value

        except Exception as exception:
            self._isUpdated = False
            if isinstance(exception, exceptions.InvalidType):
                self._isValid = False
            raise

    def toString(self, isV2=True):
        """
        The xml representation of this payload as a string that can be used with the post command.

        Keyword Args:
            isV2 (bool): is the dictionary generated using version 2 of the REST Api

        Returns:
            string
        """
        xmlString = xml.tostring(self.xmlElement(isV2=isV2), encoding="utf8", method="xml")
        minidomXml = minidom.parseString(xmlString)
        return minidomXml.toprettyxml()

    def toDict(self, isV2=True):
        """
        The representation of this payload as a dictionary that can be used to post it to mongo database.

        Keyword Args:
            isV2 (bool): is the dictionary generated using version 2 of the REST Api

        Returns:
            dict
        """
        xmlString = xml.tostring(self.xmlElement(isV2=isV2), encoding="utf8", method="xml")
        orderedDict = xmltodict.parse(xmlString)
        # Convert ordered dict to regular dict
        dictionary = json.loads(json.dumps(orderedDict))
        return dictionary

    @property
    def isValid(self):
        """ Does this id represent the type of object represents """
        if self._isValid is None:
            try:
                # To determine if an id is of a certain type, we have to query bugstar
                self.refresh()
            except (exceptions.InvalidId, exceptions.InvalidType):
                # TODO: Look up what is the main difference between these two errors
                pass
        return self._isValid
