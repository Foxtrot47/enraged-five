import types

from rsg_clients.bugstar import exceptions, const
from rsg_clients.bugstar.payloads import basePayload


class Actor(basePayload.BasePayload):
    """
    This class shouldn't be used, it is simply here to keep with the paradigm used for this package
    """

    TYPE = const.Endpoints.ACTORS

    def __new__(cls, bugstar, index=None, *args, **kwargs):
        raise exceptions.PermissionDenied(
            "This class should not be constructed as it represents an endpoint that returns"
            " data of other types (User, Team, Department, Global Department)"
        )

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def hasProjectRole(self, project, roles=None):
        """
        Does this actor have the given roles within the given project.
        This method can be used to check if an account has access to a given project.

        Args:
            project (rsg_clients.bugstar.payloads.project.Project): the project to check role against

        Keyword Args:
            roles (list): the roles to check for. Defaults to Owner and QA owner.

        Returns:
            bool
        """
        roles = roles or (const.ProjectRoles.OWNER, const.ProjectRoles.QA_OWNER)
        if not isinstance(roles, (tuple, list)):
            raise ValueError("Expected a list or tuple, received a {} ({}) instead".format(type(roles), roles))
        command = ",".join(roles)
        command = "/".join(
            [
                const.Endpoints.ACTORS,
                const.Endpoints.ACTIVE,
                const.Endpoints.PROJECTS,
                str(project.id),
                const.Endpoints.PROJECT_ROLES,
                command,
            ]
        )
        results = self._getData(self.server, self.id, command=command)
        # This command returns users who have the given roles.
        # If no one is returned then that user does not have that role
        return len(results.get('items', [])) > 0


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the User class
    """

    def __init__(self, server):
        """
        Overrides inherited method

        Adds builder cache to remember the correct builder for the ids to avoid calling the server again in the
        future.

        Args:
            server (rsg_clients.bugstar.server.BaseServer): server that this builder should use to create assets
        """
        super(Builder, self).__init__(server)
        self._builderCache = {}  # caches which builder an actor id goes with

    def __call__(self, index):
        """
        Builds the object that bests represents the data from index.
        The type of data is grabbed from a rest.

        Args:
            index (int): the index on bugstar for the actor subtype

        Returns:
            rsg_clients.bugstar.payloads.basePayload.BasePayload
        """

        if not isinstance(index, (int, basestring, types.NoneType)):
            if isinstance(index, Actor):
                return index
            raise ValueError("{} is not a valid value for this builder".format(index))
        builder = self._builderCache.get(index, None)
        data = None
        if not builder:
            server = self._server
            url = [server.restUrl, const.Endpoints.V2, const.Endpoints.ACTORS, index]
            fields = {}
            if server.delegateUser is not None:
                fields[const.QueryFields.AS_USER] = server.delegateUser.id
            actorUrl = server.buildUrl(url, fields=fields)
            data = server.get(actorUrl)  # returns a dictionary
            key = data["actorType"]  # Get the actor type from the returned payload
            builderKey = "{}s".format(key)
            builder = server._getBuilder(builderKey)
            self._builderCache[index] = builder
        return builder(index, data)

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Actor
