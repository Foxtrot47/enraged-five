"""
Test module for debugging the basic interface for interacting with bugs on Bugstar
"""
import os
import time
import unittest
import tempfile
import datetime

from rsg_core import accounts
from rsg_clients.bugstar import servers, const, exceptions
from rsg_clients.bugstar.contextManagers import delegate
from rsg_clients.bugstar.payloads import basePayload


class TestConnection(unittest.TestCase):
    """Tests basic connection functionality."""

    SERVER = None  # Server to use for this test

    def test_Connection(self):
        """Test if you can connect to the servers."""
        for server in (servers.development, servers.production, servers.preProduction):
            server.account = accounts.OtherUser("notreal", "badpassword")
            self.assertFalse(server.bugstar.login())
            server.account = accounts.TechArt()
            self.assertTrue(server.bugstar.login())

    def test_Token(self):
        """Test that the token is getting resolved properly if it is invalid"""
        server = servers.development
        server.account = accounts.TechArt()
        # Connect to the server
        self.assertTrue(server.bugstar.login())
        # Mess up the token so that it is essentially incorrect
        server.bugstar.session.headers["authorization"] = "Bearer BADTOKEN"
        # Perform an action that invokes a rest command
        user = server.user(1991291)
        self.assertIsNotNone(user.name)
        self.assertEqual(server.bugstar._authenticated, True)


class TestSimpleBugs(unittest.TestCase):
    """
    Unit test for making interactions with simple bugs work as expected.

    Unit tests that need to be ran after the bug has been created should include B in the function name and functions
    that run after include a D.
    """

    SERVER = None  # Server to use for this test
    SIMPLE_BUG_ID = None
    SIMPLE_ATTACHMENT_BUG_ID = None  # Used to test out second bug and attaching the file on a different bug.
    REQUIRES_BUILD_BUG_ID = None
    REQUIRES_VERIFICATION_BUG_ID = None
    ATTACHMENT_ID_1 = None
    ATTACHMENT_ID_2 = None
    BLOCKER_BUG_ID = None
    ATTACHMENT_1 = os.path.join(const.Paths.TESTS_DIRECTORY, "testText.txt")
    ATTACHMENT_2 = os.path.join(const.Paths.TESTS_DIRECTORY, "testImage.jpg")

    @classmethod
    def setUpClass(cls):
        """Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.TechArt()

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.SIMPLE_BUG_ID = None
        cls.REQUIRES_BUILD_BUG_ID = None
        cls.REQUIRES_VERIFICATION_BUG_ID = None
        cls.BLOCKER_BUG_ID = None

        cls.SERVER.bugstar.logout()
        cls.SERVER.delegate = None

    def setUp(self):
        """
        Overrides inherited method.

        Clear the internal bug cache so all data that comes from bugstar is fresh
        """
        self.SERVER.clearCache()

    def test_A01_createSimpleBug(self):
        """
        Create a bug in bugstar
        """
        user = self.SERVER.user.getByName(self.SERVER.account.name)

        bug = self.SERVER.bug()
        bug.developer = user.id
        bug.tester = user.id
        bug.summary = "Python Unit Test: Simple Bug"
        bug.workflow = const.Workflows.SIMPLE
        bug.project = self.SERVER.project.getByName("Testing Ground")
        bug.doNotMail = True
        bug.create()
        self.assertIsNotNone(bug.id)

        self.__class__.SIMPLE_BUG_ID = bug.id  # Set the bug id that will be used for the rest of this test

    def test_A02_createSimpleAttachmentBug(self):
        """
        Create a second bug in bugstar to test out the proper attachment connections
        """
        user = self.SERVER.user.getByName(self.SERVER.account.name)

        bug = self.SERVER.bug()
        bug.developer = user
        bug.tester = user
        bug.summary = "Python Unit Test: Simple Bug for attachment test"
        bug.workflow = const.Workflows.SIMPLE
        bug.project = self.SERVER.project.getByName("Testing Ground")
        bug.create()
        self.assertIsNotNone(bug.id)

        self.__class__.SIMPLE_ATTACHMENT_BUG_ID = bug.id  # Set the bug id that will be used for the rest of this test

    def test_B01_addComment(self):
        """
        Add a comment to the unit test bug
        """
        self.assertIsNotNone(self.SIMPLE_BUG_ID)

        user = self.SERVER.user.getByName(self.SERVER.account.name)
        comment = self.SERVER.bug.comment("Python Unit Test: Adding Comment", user)
        bug = self.SERVER.bug(self.SIMPLE_BUG_ID)
        bug.comments.append(comment)
        bug.update()

        self.assertGreater(len(bug.comments), 0)
        newComment = bug.comments[-1]
        self.assertEqual(newComment.text, "Python Unit Test: Adding Comment")
        self.assertIsNotNone(newComment.creator)
        self.assertIsNotNone(newComment.createdOn)

    def test_B02_getTeamByName(self):
        """
        Get Team by name
        """
        # Retrieve specific non-internal team
        team = self.SERVER.team.getByName("*Tech Art*")
        self.assertIsNotNone(team)

        # Retrieve multiple results based on an incomplete name
        team = self.SERVER.team.getByName("*Tech Art", all=True)
        self.assertIsInstance(team, list)

        # Retrieve specific internal team
        team = self.SERVER.team.getByName("~Tech Art (Shotgun)")
        self.assertIsNotNone(team)

        # Retrieve multiple results based on an incomplete name
        team = self.SERVER.team.getByName("~Tech Art", all=True)
        self.assertIsInstance(team, list)

        # Fail to retrieve team that doesn't exist
        team = self.SERVER.team.getByName("Not Real")
        self.assertIsNone(team)

    def test_B03_mutableData(self):
        """
        Test that mutable data is being set/edited properly
        """
        bug = self.SERVER.bug(self.SIMPLE_BUG_ID)
        self.assertIsInstance(bug, self.SERVER.bug.cls())

        user = self.SERVER.user.getByName(self.SERVER.account.name)

        if bug.ccList:
            # Test that we can edit the value by setting it
            bug.ccList = []
            bug.update()
            bug.removeEdits()  # verify that no edits are in place and only RAW data
            self.assertEqual(bug.ccList, [])

            # Test that we can add data by editing it in place
            bug.ccList.append(user)
            bug.update()
            bug.removeEdits()  # verify that no edits are in place and only RAW data
            self.assertEqual(bug.ccList, [user])

            # Leave the cc empty for future tests
            bug.ccList = []
            bug.update()

        else:
            # Test that we can edit the value by setting it
            bug.ccList = [user]
            bug.update()
            bug.removeEdits()  # verify that no edits are in place and only RAW data
            self.assertEqual(bug.ccList, [user])

            # Test that we can add data by editing it in place
            bug.ccList.remove(user)
            bug.update()
            bug.removeEdits()  # verify that no edits are in place and only RAW data
            self.assertEqual(bug.ccList, [])

    def test_C01_addAttachment(self):
        """
        Adding 2 attachments to the unit test bug
        """
        self.assertIsNotNone(self.SIMPLE_BUG_ID)
        bug = self.SERVER.bug(self.SIMPLE_BUG_ID)
        # Add both attachments to 1st bug to make sure it downloads only one in the tests that are comming up.
        for attachFile in [self.ATTACHMENT_1, self.ATTACHMENT_2]:
            attachment = self.SERVER.attachment()
            attachment.setFile(attachFile)
            bug.attachmentList.append(attachment)
            bug.update()

            testId = attachment.id
            # Save the Id on the class to test out the download of 1 attachment later on.
            if attachFile == self.ATTACHMENT_1:
                self.__class__.ATTACHMENT_ID_1 = attachment.id

            self.assertGreater(len(bug.attachmentList), 0)
            # Test that it added the attachment to the end of the list
            self.assertEqual(
                bug.attachmentList[-1].name, os.path.basename(attachFile), "Attachment file base name is not the same."
            )
            self.assertEqual(
                bug.attachmentList[-1].id,
                testId,
                "The attachment with the ID {} is not added at the end of the bug attachment list".format(testId),
            )

    def test_C02_addAttachmentToSecondBug(self):
        """
        Adding attachment to the second bug to test out if the attachment gets attached to the right bug.
        """
        bug = self.SERVER.bug(self.SIMPLE_ATTACHMENT_BUG_ID)
        attachment = self.SERVER.attachment()
        attachment.setFile(self.ATTACHMENT_2)
        bug.attachmentList.append(attachment)
        bug.update()

        self.__class__.ATTACHMENT_ID_2 = bug.attachmentList[-1].id

        self.assertGreater(len(bug.attachmentList), 0, "Attachment is not added to the bug. Bug has no attachments.")
        self.assertEqual(
            bug.attachmentList[-1].name,
            os.path.basename(self.ATTACHMENT_2),
            "Attachment file base name is not the same.",
        )
        self.assertEqual(
            bug.attachmentList[-1].id,
            self.ATTACHMENT_ID_2,
            "The attachment with the ID {} is not added at the end of the bug attachment list".format(
                self.ATTACHMENT_ID_2
            ),
        )

    def test_C03_downloadAttachments(self):
        """
        Downloads one attachment per bug.
        Tests that it downloads the right attachment from the right bug and that it downloads only one and not all
        the attachments on the bug.
        """
        self.assertIsNotNone(self.SIMPLE_BUG_ID)
        self.assertIsNotNone(self.SIMPLE_ATTACHMENT_BUG_ID)
        self.assertIsNotNone(self.ATTACHMENT_ID_1)
        self.assertIsNotNone(self.ATTACHMENT_ID_2)

        # Test the first bug
        bug = self.SERVER.bug(self.SIMPLE_BUG_ID)
        path = os.path.join(tempfile.gettempdir(), "BugstarAttachDownload", str(bug.id), "attachments")
        if not os.path.exists(path):
            os.makedirs(path)

        paths = bug.downloadAttachment(path=path, attachmentId=self.ATTACHMENT_ID_1)
        downloadLocation = paths[0]  # We are only asking for one attachment, so the list should only contain one value

        # Check if the file has been downloaded
        fileDownloaded = os.path.exists(paths[-1])
        self.assertTrue(fileDownloaded, "Download doesn't work properly. File doesn't exist in the download location.")
        # Check if the files are the same.

        with open(downloadLocation, "r") as downFileHandler:
            downloadedFileContent1 = downFileHandler.readlines()

        with open(self.ATTACHMENT_1, "r") as upFileHandler:
            uploadedFileContent1 = upFileHandler.readlines()

        sameFiles1 = uploadedFileContent1 == downloadedFileContent1
        self.assertTrue(
            sameFiles1,
            "Uploaded and downloaded files are not the same. Downloaded location: {}".format(downloadLocation),
        )
        # Check that only one attachment was downloaded to the location.
        self.assertEqual(
            len(os.listdir(os.path.dirname(downloadLocation))), 1, "Downloaded more then specified (one) attachments."
        )

        # Test the second bug
        # # Download the second bug to make sure that it didn't connect the attachment to the wrong bug.
        bug = self.SERVER.bug(self.SIMPLE_ATTACHMENT_BUG_ID)
        path = os.path.join(tempfile.gettempdir(), "BugstarAttachDownload", str(bug.id), "attachments")
        paths = bug.downloadAttachment(path=path, attachmentId=self.ATTACHMENT_ID_2)
        downloadLocation = paths[0]  # We are only asking for one attachment, so the list should only contain one value

        # Check if the file has been downloaded
        fileDownloaded = os.path.exists(downloadLocation)
        self.assertTrue(fileDownloaded, "Download doesn't work properly. File doesn't exist in the download location.")
        # Check if the files are the same.
        with open(downloadLocation, "rb") as downFileHandler:
            downloadedFileContent2 = downFileHandler.readlines()

        with open(self.ATTACHMENT_2, "rb") as upFileHandler:
            uploadedFileContent2 = upFileHandler.readlines()
        sameFiles = uploadedFileContent2 == downloadedFileContent2
        self.assertTrue(
            sameFiles,
            "Uploaded and downloaded files are not the same. Downloaded location: {}".format(downloadLocation),
        )

    def test_D01_closeSimpleBug(self):
        """
        Close the unit test bug
        """
        self.assertIsNotNone(self.SIMPLE_BUG_ID)

        bug = self.SERVER.bug(self.SIMPLE_BUG_ID)
        bug.state = const.States.CLOSED_FIXED
        bug.update()

        self.assertEqual(bug.state, const.States.CLOSED_FIXED)

    def test_C02_copySimpleBug(self):
        """
        Close the unit test bug
        """
        self.assertIsNotNone(self.SIMPLE_BUG_ID)

        bug = self.SERVER.bug(self.SIMPLE_BUG_ID)
        copy = bug.copy(includeAttachments=True)
        copy.summary = "{} Copy".format(bug.summary)
        copy.create()

        self.assertIsNotNone(copy.id)
        self.assertNotEqual(bug.id, copy.id)

    # TODO: Add tests for switching bugs to blocked, on-hold, and on-hold waiting to start.


class TestRequiresBuildBugs(unittest.TestCase):
    """
    Unit test for making interactions with requires build bugs work as expected.

    Unit tests that need to be ran after the bug has been created should include B in the function name and functions
    that run after include a D.
    """

    SERVER = None  # Server to use for this test
    REQUIRES_BUILD_BUG_ID = None

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.TechArt()

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.REQUIRES_BUILD_BUG_ID = None
        cls.SERVER.bugstar.logout()
        cls.SERVER.delegate = None

    def setUp(self):
        """
        Overrides inherited method.

        Clear the internal bug cache so all data that comes from bugstar is fresh
        """
        self.SERVER.clearCache()

    def test_A01_createRequiresBuildBug(self):
        """
        Create a bug in bugstar
        """
        user = self.SERVER.user.getByName(self.SERVER.account.name)

        bug = self.SERVER.bug()
        bug.developer = user.id
        bug.tester = user.id
        bug.summary = "Python Unit Test: Requires Build Bug"
        bug.workflow = const.Workflows.REQUIRES_BUILD
        bug.project = self.SERVER.project.getByName("Testing Ground")
        bug.create()
        self.assertIsNotNone(bug.id)
        self.assertEqual(bug.workflow, const.Workflows.REQUIRES_BUILD)

        self.__class__.REQUIRES_BUILD_BUG_ID = bug.id  # Set the bug id that will be used for the rest of this test

    def test_D01_closeRequiresBuildBug(self):
        """
        Close the unit test bug
        """
        self.assertIsNotNone(self.REQUIRES_BUILD_BUG_ID)

        bug = self.SERVER.bug(self.REQUIRES_BUILD_BUG_ID)
        bug.state = const.States.CLOSED_FIXED
        bug.update()

        self.assertEqual(bug.state, const.States.DEV_FIXED_WAITING_BUILD)

        # TODO: Contact Bugstar team about how to close bugs waiting build
        # bug.state = const.States.CLOSED_FIXED
        # bug.update()
        # self.assertEquals(bug.state, const.States.CLOSED_FIXED)


class TestRequiresVerificationBugs(unittest.TestCase):
    """
    Unit test for making interactions with require verification bugs work as expected.

    Unit tests that need to be ran after the bug has been created should include B in the function name and functions
    that run after include a D.
    """

    SERVER = None  # Server to use for this test
    REQUIRES_VERIFICATION_BUG_ID = None

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.TechArt()

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.REQUIRES_VERIFICATION_BUG_ID = None
        cls.SERVER.bugstar.logout()
        cls.SERVER.delegate = None

    def setUp(self):
        """
        Overrides inherited method.

        Clear the internal bug cache so all data that comes from bugstar is fresh
        """
        self.SERVER.clearCache()

    def test_A01_createRequiresVerificationBug(self):
        """
        Create a bug in bugstar
        """
        user = self.SERVER.user.getByName(self.SERVER.account.name)

        bug = self.SERVER.bug()
        bug.developer = user.id
        bug.tester = user.id
        bug.summary = "Python Unit Test: Requires Verification Bug"
        bug.workflow = const.Workflows.REQUIRES_VERIFICATION
        bug.project = self.SERVER.project.getByName("Testing Ground")
        bug.create()
        self.assertIsNotNone(bug.id)
        self.assertEqual(bug.workflow, const.Workflows.REQUIRES_VERIFICATION)

        self.__class__.REQUIRES_VERIFICATION_BUG_ID = (
            bug.id
        )  # Set the bug id that will be used for the rest of this test

    def test_D01_closeRequiresBuildBug(self):
        """
        Close the unit test bug
        """
        self.assertIsNotNone(self.REQUIRES_VERIFICATION_BUG_ID)

        bug = self.SERVER.bug(self.REQUIRES_VERIFICATION_BUG_ID)
        bug.state = const.States.CLOSED_FIXED
        bug.update()

        self.assertEqual(bug.state, const.States.TEST_VERIFYING)

        bug.state = const.States.CLOSED_FIXED
        bug.update()

        self.assertEqual(bug.state, const.States.CLOSED_FIXED)


class TestSearch(unittest.TestCase):
    SERVER = None  # Server to use for this test

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.TechArt()

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.SERVER.bugstar.logout()
        cls.SERVER.delegate = None
        cls.SERVER.clearCache()

    def setUp(self):
        """
        Overrides inherited method.

        Clear the internal bug cache so all data that comes from bugstar is fresh
        """
        self.SERVER.clearCache()

    def testExistingSearch(self):
        """Run an existing search from bugstar."""
        # Add the new collection to the actual search
        search = self.SERVER.bug.search(
            5980722
        )  # This search is the same as the dynamically generated search but saved in bugstar

        self.assertGreater(search.statistics.totalDistinctOwners, 0)

        # Test that we only grab 3 bugs from page 2
        count = len(
            search.bugs(size=3, page=2, iterative=False)
        )  # This saved search performs the same search as the dynamic search
        self.assertEqual(count, 3)

        # Time search
        count = len(search.bugs())  # This saved search performs the same search as the dynamic search
        self.assertGreater(count, 0)

    def testDynamicSearch(self):
        """Run a dynamic search from bugstar."""
        # Create the root of the search, limiting it to only the current project
        root = self.SERVER.search.collection(const.Operations.AND)
        comparison = self.SERVER.search.comparison(const.SearchFields.PROJECT, const.Operations.IN)
        comparison.add(self.SERVER.project.getByName("Testing Ground"))
        root.add(comparison)

        # simple search for getting all bugs with the word test in their summary
        collection = self.SERVER.search.collection(const.Operations.AND)
        comparision = self.SERVER.search.comparison(const.SearchFields.SUMMARY, const.Operations.CONTAINS)

        collection.add(comparision)
        comparision.add("Python Unit Test: Simple Bug")

        # Add the new collection to the actual search
        search = self.SERVER.bug.search(root=root)
        search.add(collection)

        self.assertGreater(search.statistics.totalDistinctOwners, 0)

        # Time search
        count = len(search.bugs())
        self.assertGreater(count, 0)

    def testConcurrentSearch(self):
        """Runs a saved search on bugstar to test that it can process different search commands concurrently"""
        search = self.SERVER.bug.search(
            5980722
        )  # This search is the same as the dynamically generated search but saved in bugstar

        self.assertGreater(search.statistics.totalDistinctOwners, 0)

        # Time search
        count = len(search.bugs(size=1))  # This saved search performs the same search as the dynamic search
        self.assertGreater(count, 0)


class TestSimpleBugsDelegation(TestSimpleBugs):

    SERVER = None  # Server to use for this test

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.Shotgun()
        cls.SERVER.delegate = accounts.TechArt().name  # Test user delegation as the Tech Art Account

    def setUp(self):
        """
        Overrides inherited method.

        Clear the internal bug cache so all data that comes from bugstar is fresh
        """
        self.SERVER.clearCache()

    def test_A01_createSimpleBug(self):
        """
        Create a bug in bugstar
        """
        user = self.SERVER.user.getByName(self.SERVER.delegateUser.username)

        bug = self.SERVER.bug()
        bug.developer = user
        bug.tester = user
        bug.summary = "Python Unit Test: Simple Bug"
        bug.workflow = const.Workflows.SIMPLE
        bug.project = self.SERVER.project.getByName("Testing Ground")
        bug.create()
        self.assertIsNotNone(bug.id)

        self.__class__.SIMPLE_BUG_ID = bug.id  # Set the bug id that will be used for the rest of this test

    def test_A02_createSimpleAttachmentBug(self):
        """
        Create a bug in bugstar
        """
        user = self.SERVER.user.getByName(self.SERVER.delegateUser.username)

        bug = self.SERVER.bug()
        bug.developer = user
        bug.tester = user
        bug.summary = "Python Unit Test: Simple Bug for attachment test"
        bug.workflow = const.Workflows.SIMPLE
        bug.project = self.SERVER.project.getByName("Testing Ground")
        bug.create()
        self.assertIsNotNone(bug.id)

        self.__class__.SIMPLE_ATTACHMENT_BUG_ID = bug.id  # Set the bug id that will be used for the rest of this test

    def test_B01_addComment(self):
        """
        Add a comment to the unit test bug
        """
        user = self.SERVER.user.getByName(self.SERVER.delegateUser.username)

        bug = self.SERVER.bug(self.SIMPLE_BUG_ID)
        comment = self.SERVER.bug.comment("Python Unit Test: Adding Comment", user)
        bug.comments.append(comment)
        bug.update()

        self.assertGreater(len(bug.comments), 0)


class TestSearchDelegation(TestSearch):
    """Test that the search endpoints work with user delegation"""

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection when the class is invoked the first time
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.Shotgun()
        cls.SERVER.delegate = accounts.TechArt().name  # Test user delegation as the Tech Art Account

    def setUp(self):
        """
        Overrides inherited method.

        Clear the internal bug cache so all data that comes from bugstar is fresh
        """
        self.SERVER.delegate = accounts.TechArt().name  # Test user delegation as the Tech Art Account
        self.SERVER.bug._clearCache()


class TestBugs(unittest.TestCase):
    SERVER = None  # Server to use for this test

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.TechArt()

    def setUp(self):
        """Overrides inherited method.

        Sets the delegate account
        """
        self.SERVER.account = accounts.TechArt()

    def test_AsciiEncoding(self):
        """Sometimes invalid ascii characters are included in bugs.

        Test that we are properly removing invalid ascii characters.
        """
        bug = self.SERVER.bugstar.bug(6128776)
        print(
            str(bug.summary)
        )  # This the assert essentially, if there are invalid ascii characters the print statement will error out

    def test_InvalidProject(self):
        """Sometimes bugs are moved on the server after they have been cached resulting in the URL using the wrong

        Project in the URI. This tests that we can recreate said error and that the fix works.
        """

        # Using the bug from the previous ascii test as it is not in the Testing Ground project
        bug = self.SERVER.bugstar.bug(6128776)
        projectId = bug.project.id
        badProject = self.SERVER.project.getByName("Testing Ground")
        # Break the bug so it looks at the wrong project
        bug._fields[const.Fields.PROJECT_ID] = badProject.id
        # Verify that the error still occurs
        self.assertRaises(exceptions.ProjectChanged, bug.project.get, [const.Endpoints.BUGS, bug.id])
        # Verify that the wrong project is set
        self.assertNotEqual(bug.projectId, projectId)
        bug.refresh()
        # Confirm that the correct project was set
        self.assertEqual(bug.projectId, projectId)

    def test_LazyCasting(self):
        """For lists, we can now cast the contents of the lists at the time they are asked for rather then at the loading of
        the bug.
        """
        bug = self.SERVER.bugstar.bug(6128776)
        bug.ccList
        bug.removeEdits()
        bug.ccList

    def test_PermissionDenied(self):
        """Test getting data from a bug whose project is not accessible to the current user."""
        bug = self.SERVER.bug(6252603)
        try:
            print(bug.summary)
            self.assertTrue(
                "This is a valid bug. Change bug in the unit test to one that is not accessible to the service account"
            )
        except Exception as exception:
            self.assertIsInstance(exception, exceptions.PermissionDenied)

    def test_PermissionDenied(self):
        """Test getting data from a bug whose project is not accessible to the current user."""
        bug = self.SERVER.bug(6252603)
        try:
            print(bug.summary)
            self.assertTrue(
                "This is a valid bug. Change bug in the unit test to one that is not accessible to the service account"
            )
        except Exception as exception:
            self.assertIsInstance(exception, exceptions.PermissionDenied)

    def test_DelegationError(self):
        """Test that this errors out when a user doesn't have super admin privilages."""
        try:
            with delegate.AsUser(self.SERVER, "svcrsgnyctechart"):
                tags = self.SERVER.bug(3045404).tagList
        except exceptions.PermissionDenied as exception:
            self.assertIsInstance(exception, exceptions.PermissionDenied)

        # Switch to our shotgun super admin account
        self.SERVER.account = accounts.Shotgun()
        with delegate.AsUser(self.SERVER, "svcrsgnyctechart"):
            tags = self.SERVER.bug(3045404).tagList

    def test_ActorRoles(self):
        """Test getting if a  that an actor has access to."""
        project = self.SERVER.project.getByName("Testing Ground")
        user = self.SERVER.user.getByName(self.SERVER.account.name)

        # The TA service account has access to this project as an owner & qaowner
        self.assertTrue(user.hasProjectRole(project))

        # The TA service account is not an admin
        self.assertFalse(user.hasProjectRole(project, [const.ProjectRoles.ADMIN]))

        # The Shotgun service account doesn't have access to any projects
        user = self.SERVER.user.getByName(accounts.Shotgun.name)
        self.assertFalse(user.hasProjectRole(project))

    def test_TeamMembership(self):
        """Test that an actor has access to a project."""
        project = self.SERVER.project.getByName("Testing Ground")
        user = self.SERVER.user.getByName(accounts.TechArt().name)

        # The TA service account has access to this project as an owner & qaowner
        teams = user.teams(project)
        self.assertGreater(len(teams), 0)

    def test_BadOwner(self):
        """
        Test getting data from a bug whose project is not accessible to the current owner.
        """
        # At the time of writting, this bug doesn't exist on the dev server yet
        bug = self.SERVER.bug(6621198)
        try:
            bug.update()
            self.assertTrue(
                "This is a valid bug."
                "Change bug in the unit test to one whose owner does not have access to the project the bug is assigned to"
            )
        except Exception as exception:
            self.assertIsInstance(exception, exceptions.BugstarServerError)
            print(str(exception))

    def test_ClearData(self):
        """Test that we can clear and remove data from the cache properly while still being able to query it."""
        bug = self.SERVER.bug(6621198)
        self.assertIsInstance(bug.developer.name, basestring)
        bug._clearDataCache()
        self.assertIsInstance(bug.developer.name, basestring)
        bug._removeFromCache()
        self.assertIsInstance(bug.developer.name, basestring)
        bug._removeFromCache(force=True)
        self.assertIsInstance(bug.developer.name, basestring)
        basePayload.BasePayload.CACHE_LIMIT = 0
        basePayload.BasePayload.CACHE_EXPIRY = 1
        basePayload.BasePayload.DATA_EXPIRY = 1
        time.sleep(3)
        self.SERVER.instance.cleanLeastAccessedCache()
        self.assertIsInstance(bug.developer.name, basestring)

    def test_InvalidXMLCharacters(self):
        """
        The comments have invalid xml characters
        """
        bug = self.SERVER.bug(6028293)
        # Test that the xml is built properly
        bug.toString()
        # Test that the xml is a valid payload
        bug.update()

    def test_HasDueDate(self):
        """
        The comments have invalid xml characters
        """
        bug = self.SERVER.bug(5342561)
        # Test that the xml is built properly
        print(bug.toString())
        self.assertIsNotNone(bug.dueDate)

    def test_HasGrid(self):
        """Test that grid data is obtainable."""
        bug = self.SERVER.bug(6022118)
        # Test that the xml is built properly
        self.assertIsNotNone(bug.grid.name)

    def test_BugYielding(self):
        """Test that the search generator logic works and futures doesn't lock up the ability to send rest requests"""
        search = self.SERVER.bug.search(5120942)  # ADR search that is quick
        index = 0
        for bug in search.yieldBugs(size=1, iterative=True):
            index += 1
            if index > 3:
                break
            testTag = "test"
            if testTag not in bug.tagList:
                bug.tagList.append(testTag)
            else:
                bug.tagList.remove(testTag)
            bug.update()

    def test_DownloadAttachments(self):
        """Test downloading attachments that can't be made to json."""
        tempDirectory = os.path.join(tempfile.gettempdir(), "unittests")
        # The first id is the attachment id and the second is the bug it is attached to
        # this is a requirement of the api so we can grab info about the attachment
        attachments = [
            self.SERVER.attachment(27657085, 6681574),  # random html file
            self.SERVER.attachment(27335067, 7005132),  # xml file
        ]
        for attachment in attachments:
            path = attachment.download(tempDirectory)
            if os.path.exists(path):
                os.remove(path)

    def test_GetBugsByDescription(self):
        """Tests that the get bug description method works properly.

        Notes:
            This may not work if the bug description has invalid ascii characters python my not support out of the box.
        """
        bug = self.SERVER.bug(5342561)
        bugs = self.SERVER.bug.getByDescription([bug.project], bug.description)
        self.assertTrue(len(list(bugs)) > 1)


class TestDataValidation(unittest.TestCase):
    """Test that each payload is returning the expected data types for each property they support."""
    NONE_TYPE = type(None) # The ony way to get the type of None in Python 3.9 and lower
    SERVER = None  # Server to use for this test

    @classmethod
    def setUpClass(cls):
        """Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.TechArt()

    def assertList(self, payloadList, types):
        self.assertIsInstance(payloadList, list)
        for each in payloadList:
            self.assertIsInstance(each, types)

    def test_Attachment(self):
        """Test that all the values are being returned in their expected formats."""
        bug = self.SERVER.bug(3224070) # Random Bug with an attachment
        self.assertIsInstance(bug.attachmentList, list)
        self.assertGreater(len(bug.attachmentList), 0)
        attachment = bug.attachmentList[0]
        self.assertIsInstance(attachment,  self.SERVER.attachment.cls())
        self.assertEqual(attachment.bug, bug)
        self.assertIsInstance(attachment.contentType, basestring)
        self.assertIsInstance(attachment.createdAt, datetime.datetime)
        self.assertIsInstance(attachment.createdBy, self.SERVER.user.cls())
        self.assertIsNone(attachment.filename)  # This value is only a str when creating a file
        self.assertIsInstance(attachment.fileSize, int)
        self.assertIsInstance(attachment.isArchived, bool)
        self.assertIsInstance(attachment.isRegistered, bool)
        self.assertIsInstance(attachment.name, basestring)

    def test_Bug(self):
        """Test that all the values are being returned in their expected formats."""
        bug = self.SERVER.bug(3224070) # Random Bug with an attachment
        self.assertIsInstance(bug.developer, self.SERVER.instance.actor.cls())
        self.assertIsInstance(bug.tester, self.SERVER.instance.actor.cls())
        self.assertIsInstance(bug.activeFrames, int)
        self.assertIsInstance(bug.attempts, int)
        self.assertIsInstance(bug.awaitingUser, (self.SERVER.instance.actor.cls(), self.NONE_TYPE))
        self.assertIsInstance(bug.category, basestring)
        self.assertIsInstance(bug.cinematicAnimator, (self.SERVER.instance.actor.cls(), self.NONE_TYPE)) #This bug doesn't have a cinematic animator
        self.assertIsInstance(bug.description, basestring)
        self.assertIsInstance(bug.difficulty, (basestring, self.NONE_TYPE))
        self.assertIsInstance(bug.doNotMail, bool)
        self.assertIsInstance(bug.dueDate, datetime.datetime)
        self.assertIsInstance(bug.estimatedTime, int)
        self.assertIsInstance(bug.exists, bool)
        self.assertIsInstance(bug.fixedIn, (basestring, self.NONE_TYPE))
        self.assertIsInstance(bug.fixedOnImportance, int)
        self.assertIsInstance(bug.foundIn, (int, self.NONE_TYPE))
        self.assertIsInstance(bug.grid, (self.SERVER.grid.cls(), self.NONE_TYPE))
        self.assertIsInstance(bug.highPriorityFix, bool)
        self.assertIsInstance(bug.isOnHold, bool)
        self.assertIsInstance(bug.isWorking, bool)
        self.assertIsInstance(bug.lastModifiedOn, datetime.datetime)
        self.assertIsInstance(bug.map, self.SERVER.gameMap.cls())
        self.assertIsInstance(bug.mission, (self.SERVER.mission.cls(), self.NONE_TYPE))
        self.assertIsInstance(bug.parent, (self.SERVER.bug.cls(), self.NONE_TYPE))
        self.assertIsInstance(bug.priority, int)
        self.assertIsInstance(bug.progress, int)
        self.assertIsInstance(bug.project, self.SERVER.project.cls())
        self.assertIsInstance(bug.projectId, int)
        self.assertIsInstance(bug.qaTimeSpent, (float, self.NONE_TYPE))
        self.assertIsInstance(bug.reviewer, (self.SERVER.instance.actor.cls(), self.NONE_TYPE))
        self.assertIsInstance(bug.seen, int)
        self.assertIsInstance(bug.state, basestring)
        self.assertIsInstance(bug.summary, basestring)
        self.assertIsInstance(bug.timeSpent, float)
        self.assertIsInstance(bug.timeType, basestring)
        self.assertIsInstance(bug.updateFlag, bool)
        self.assertIsInstance(bug.verifications, int)
        self.assertIsInstance(bug.verifiedBy, (self.SERVER.instance.actor.cls(), self.NONE_TYPE))
        self.assertIsInstance(bug.workflow, int)
        self.assertIsInstance(bug.x, float)
        self.assertIsInstance(bug.xf, float)
        self.assertIsInstance(bug.y, float)
        self.assertIsInstance(bug.yf, float)
        self.assertIsInstance(bug.z, float)
        self.assertIsInstance(bug.zf, float)

        # Lists
        self.assertList(bug.attachmentList, self.SERVER.attachment.cls())
        self.assertList(bug.blockersList, self.SERVER.bug.cls())
        self.assertList(bug.ccList, self.SERVER.instance.actor.cls())
        self.assertList(bug.deadlineList, self.SERVER.deadline.cls())
        self.assertList(bug.goal, self.SERVER.goal.cls())
        self.assertList(bug.environmentList, self.SERVER.grid.cls())
        self.assertList(bug.p4ChangeLists, int)
        self.assertList(bug.platformList, int)
        self.assertList(bug.tagList, basestring)

        self.assertIsInstance(bug.comments, list)
        for each in bug.comments:
            self.assertTrue(self.SERVER.bug.isComment(each))

