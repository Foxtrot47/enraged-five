from xml.etree import cElementTree as xml

from rsg_core import conversions
from rsg_clients.bugstar import const
from rsg_clients.bugstar.payloads import basePayload


class Mission(basePayload.BasePayload):
    """
    #V2 Fields
    const.Fields.ID
    const.Fields.ACTIVE
    const.Fields.AUDIO_DESIGNER
    const.Fields.CATEGORY
    const.Fields.CHAPTER
    const.Fields.CINEMATIC_ANIMATOR
    const.Fields.CREATED_AT
    const.Fields.CREATED_BY
    const.Fields.DESCRIPTION
    const.Fields.DESIGNER_INITIALS
    const.Fields.ISSUES
    const.Fields.INACTIVE
    const.Fields.LEVEL_DESIGNER
    const.Fields.MISSION_ID
    const.Fields.MODIFIED_BY
    const.Fields.MODIFIED_AT
    const.Fields.NAME
    const.Fields.PERCENTAGE_COMPLETE
    const.Fields.PLAYED
    const.Fields.SCRIPT_NAME
    const.Fields.SINGLE_PLAYER
    const.Fields.STORY_DESCRIPTION
    const.Fields.VARIANT_NUMBER
    const.Fields.VERSION

    # V1 Fields
    const.Fields.ADMIN_TO_PLAY
    const.Fields.PROJECTED_ATTEMPTS
    const.Fields.PROJECTED_ATTEMPTS_MIN
    const.Fields.PROJECTED_ATTEMPTS_MAX
    const.Fields.CHECKPOINTS
    """

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    XML_EXCLUDE_FIELDS = (
        const.Fields.ADMIN_TO_PLAY,
        const.Fields.PROJECTED_ATTEMPTS,
        const.Fields.PROJECTED_ATTEMPTS_MIN,
        const.Fields.PROJECTED_ATTEMPTS_MAX,
        const.Fields.CHECKPOINTS,
    )

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element(const.Fields.MISSION)

        for key, value in self._fields.iteritems():

            if value is None or key in self._xmlExcludeField:
                continue
            valueElement = xml.Element(key)

            if isinstance(value, (self.server.user._CLASS, self.server.team._CLASS)):
                valueElement.text = str(value.id)
            else:
                valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        name = dictionary.get(const.Fields.NAME, None)
        if name is not None:
            # Mission names may contain invalid ascii characters
            dictionary[const.Fields.NAME] = conversions.convertToAscii(name)
        self._fields.update(dictionary)

    @property
    def audioDesigner(self):
        """ levelDesigner is readable bugstar property """
        value = self._getattr(const.Fields.AUDIO_DESIGNER)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self._getValidUserPayload(value)
        return value

    @property
    def active(self):
        """ active is readable bugstar property """
        return self._getattr(const.Fields.ACTIVE)

    @property
    def category(self):
        """ category is readable bugstar property """
        return self._getattr(const.Fields.CATEGORY)

    @property
    def chapter(self):
        """ category is readable bugstar property """
        return self._getattr(const.Fields.CHAPTER)

    @property
    def cinematicAnimator(self):
        """ levelDesigner is readable bugstar property """
        value = self._getattr(const.Fields.CINEMATIC_ANIMATOR)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self._getValidUserPayload(value)
        return value

    @property
    def createdBy(self):
        """ createdBy is readable bugstar property """
        value = self._getattr(const.Fields.CREATED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.user(value)
        return value

    @property
    def createdAt(self):
        """ createdAt is readable bugstar property """
        return self._getattr(const.Fields.CREATED_AT)

    @property
    def description(self):
        """ description is readable bugstar property """
        return self._getattr(const.Fields.DESCRIPTION)

    @property
    def designerInitials(self):
        """ Designer initials is readable bugstar property """
        return self._getattr(const.Fields.DESIGNER_INITIALS)

    @property
    def issues(self):
        """ issues is readable bugstar property """
        return self._getattr(const.Fields.ISSUES)

    @property
    def inactive(self):
        """ inactive is readable bugstar property """
        return self._getattr(const.Fields.INACTIVE)

    @property
    def levelDesigner(self):
        """ levelDesigner is readable bugstar property """
        value = self._getattr(const.Fields.LEVEL_DESIGNER)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self._getValidUserPayload(value)
        return value

    @property
    def index(self):
        """ index is readable bugstar property """
        return self._getattr(const.Fields.MISSION_ID)

    @property
    def modifiedBy(self):
        """ modifiedBy is readable bugstar property """
        value = self._getattr(const.Fields.MODIFIED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.user(value)
        return value

    @property
    def modifiedAt(self):
        """ modifiedAt is readable bugstar property """
        return self._getattr(const.Fields.MODIFIED_AT)

    @property
    def name(self):
        """ name is readable bugstar property """
        return self._getattr(const.Fields.NAME)

    @property
    def percentageComplete(self):
        """ percentageComplete is readable bugstar property """
        return self._getattr(const.Fields.PERCENTAGE_COMPLETE)

    @property
    def played(self):
        """ played is readable bugstar property """
        return self._getattr(const.Fields.PLAYED)

    @property
    def scriptName(self):
        """ scriptName is readable bugstar property """
        return self._getattr(const.Fields.SCRIPT_NAME)

    @property
    def singlePlayer(self):
        """ singlePlayer is readable bugstar property """
        return self._getattr(const.Fields.SINGLE_PLAYER)

    @property
    def storyDescription(self):
        """ storyDescription is readable bugstar property """
        return self._getattr(const.Fields.STORY_DESCRIPTION)

    @property
    def variantNumber(self):
        """ variantNumber is readable bugstar property """
        return self._getattr(const.Fields.VARIANT_NUMBER)

    @property
    def version(self):
        """ version is readable bugstar property """
        return self._getattr(const.Fields.VERSION)

    @property
    def adminToPlay(self):
        """ adminToPlay is readable bugstar property """
        return self._getattr(const.Fields.ADMIN_TO_PLAY)

    @property
    def projectedAttempts(self):
        """ projectedAttempts is readable bugstar property """
        return self._getattr(const.Fields.PROJECTED_ATTEMPTS)

    @property
    def projectedAttemptsMin(self):
        """ projectedAttemptsMin is readable bugstar property """
        return self._getattr(const.Fields.PROJECTED_ATTEMPTS_MIN)

    @property
    def projectedAttemptsMax(self):
        """ projectedAttemptsMax is readable bugstar property """
        return self._getattr(const.Fields.PROJECTED_ATTEMPTS_MAX)

    @property
    def checkpoints(self):
        """ checkpoints is readable bugstar property """
        return self._getattr(const.Fields.CHECKPOINTS)

    @property
    def missionId(self):
        """ missionId is readable bugstar property """
        return self._getattr(const.Fields.MISSION_ID)


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the Mission class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Mission
