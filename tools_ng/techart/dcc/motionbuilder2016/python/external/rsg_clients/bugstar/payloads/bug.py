import os
import types
import datetime
from xml.etree import cElementTree as xml

import pathlib

from rsg_core import conversions
from rsg_core_py.decorators import memoize
from rsg_clients.bugstar import const, exceptions
from rsg_clients.bugstar.payloads import basePayload
from rsg_clients.bugstar.decorators import connection


class Bug(basePayload.BasePayload):
    """
    A Bug from Bugstar.

    Can also be used as a payload for relevant operations (create, update, etc.)
    It uses a lazy loading mechanism to only query the database when a property is accessed to avoid making
    unnecessary amount of calls to the bugstar server. Local changes are also cached locally so the user can determine
    if they have made any changes to the bug instance since the last time the bug got information from the server.

    This is based of V1 bugstar interface and supports the following properties.

    Properties:
        id (int): id of the bug aka the bug number in bugstar.
                  This value is None when using this interface for creating a bug.
                  This property is required for updating a bug.
        projectId (int): id of the project this bug is associated with.
        summary (str): Title for the bug.
                       This property is required for creating or updating a bug.
        description (str): a more in-depth description about the bug
        workflow (int): The workflow associated with the bug, Requires Build, Simple or Verification.
                        These values can be accessed from the Bugs constant class.
                        This property is required for creating or updating a bug.
        tagList (list): list of strings that can be added as tags for the bug.
                        This property is required for updating a bug.
        developer (User): The user that is the owner of the bug.
                          This property is required for creating or updating a bug.
        tester (User): The user that is the QA owner of the bug.
                       This property is required for creating or updating a bug.
        reviewer (User): The user that is the reviewer of the bug.
                         This property is required for creating or updating a bug.
        category (string): Category of the bug.
                           These values can be accessed from the Bugs constant class.
        priority (int): The priority of the bug. Accepts the values from 1 to 5.
        state (string): The current state of the bug.
                        These values can be accessed from the Bugs constant class.
        x (float): x position in the game where the bug occured
        y (float): y position in the game where the bug occured
        z (float): z position in the game where the bug occured
        xf (float): x  position in the game where the bug occured
        yf(float): y position in the game where the bug occured
        zf (float): z position in the game where the bug occured
        todhrs (string): the hours it will take to finish the bug
        todhrs (string): the minutes it will take to finish the bug
        map (int): the map associated with this bug
        grid (int?): grid associated with this bug
        mission (int): the mission associated with this bug
        attachmentList (list): list of attachments on this bug
        platformList (list): list of platforms this bug targets
        environmentList (list): list of environments this bug affects.
        deadlineList (list) list of deadlines for this bug
        foundIn (int): the build the bug was found in
        fixedIn (int): the build the bug was fixed in
        attempts (int): number of attempts to ?
        verifications (int): number of verifactions
        seen (int): number of times the bug has been seen ingame
        timeSpent (float): time spent on the bug
        qaTimeSpent (float): qa time spent on the bug
        estimatedTime (float): how long the bug is estimated to take to complete
        doNotMail (bool): should an email be sent to the users associated with this bug
        awaitingUser (int): ????
        progress (int): the progress of the bug
        activeFrames (int): ????
        p4Changelists (list): list of P4 changelists associated with this bug
        ccList (list): list of users that should be notified of changes to this bug
        verifiedBy (User): user that verified this bug
        childrenList(list): list of children for the bug
        depedentsList(list): list of bugs that depend on this bug

    Notes:
        List of the properties used for the bug class

        # required fields for creating bugs
        const.Fields.PROJECT_ID

        const.Fields.SUMMARY
        const.Fields.DESCRIPTION
        const.Fields.WORKFLOW
        const.Fields.TAG_LIST

        const.Fields.DEVELOPER
        const.Fields.TESTER
        const.Fields.REVIEWER
        const.Fields.CATEGORY

        # writable fields
        const.Fields.PRIORITY
        const.Fields.STATE
        const.Fields.X
        const.Fields.Y
        const.Fields.Z
        const.Fields.XF
        const.Fields.YF
        const.Fields.ZF
        const.Fields.TODHRS
        const.Fields.TODMINS
        const.Fields.MAP
        const.Fields.GRID
        const.Fields.MISSION
        const.Fields.COMPONENT
        const.Fields.ATTACHMENT_LIST
        const.Fields.PLATFORM_LIST
        const.Fields.ENVIRONMENT_LIST
        const.Fields.DEADLINE_LIST
        const.Fields.FOUND_IN
        const.Fields.FIXED_IN
        const.Fields.ATTEMPTS
        const.Fields.VERIFICATIONS
        const.Fields.SEEN
        const.Fields.DIFFICULTY
        const.Fields.TIME_SPENT
        const.Fields.QA_TIME_SPENT
        const.Fields.ESTIMATED_TIME
        const.Fields.DO_NOT_MAIL
        const.Fields.AWAITING_USER
        const.Fields.PROGRESS
        const.Fields.ACTIVE_FRAMES
        const.Fields.P4_CHANGELISTS
        const.Fields.CC_LIST
        const.Fields.VERIFIED_BY
        const.Fields.COMMENTS
        const.Fields.UPDATE_FLAG

        # read only fields
        const.Fields.ID
        const.Fields.IS_WORKING,
        const.Fields.IS_ON_HOLD,
        const.Fields.FIXED_ON_IMPORTANCE,
        const.Fields.HIGH_PRIORITY_FIX,
        const.Fields.DUE_DATE,
        const.Fields.CREATED_ON,
        const.Fields.LAST_MODIFIED_ON,
        const.Fields.CINEMATIC_ANIMATOR
        const.Fields.GOALS

    Examples:

        # Create Bug
        from rsg_clients.bugstar import const, server

        # Note: This requires a project to be set

        bug = server.production.bug()
        bug.developer = server.production.user.getByName("gameasserts")
        bug.tester = server.production.user.getByName("dvega")
        bug.summary = "Bug for testing Python API"
        bug.workflow = const.Workflows.REQUIRES_BUILD
        bug.project = server.production.project.getByName("Testing Ground")
        bug.create()

    """

    _listElementNames = {
        const.Fields.P4_CHANGELISTS: "p4ChangeList",
        const.Fields.COMMENTS: "comment",
        const.Fields.DEPENDENTS_LIST: const.Fields.ID,
        const.Fields.BLOCKERS_LIST: const.Fields.ID,
        const.Fields.GOAL: const.Fields.GOAL,
    }

    _INSTANCES = {}

    SERVICE_VERSION = const.Services.V1

    IS_READONLY = False

    COMMAND = const.Endpoints.BUGS

    DATETIME_FORMAT = "%Y-%m-%d"
    LONG_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"  # Some datetime bug properties expect the ISO 8601 standard in UTC
    LONG_DATETIME_PROPERTIES = (
        const.Fields.DUE_DATE,
        const.Fields.FIXED_ON,
        const.Fields.LAST_MODIFIED_ON,
        const.Fields.CREATED_ON,
    )

    REQUIRED_CREATE_PROPERTIES = (
        const.Fields.DEVELOPER,
        const.Fields.TESTER,
        const.Fields.SUMMARY,
        const.Fields.WORKFLOW,
    )
    REQUIRED_UPDATE_PROPERTIES = (
        const.Fields.DEVELOPER,
        const.Fields.TESTER,
        const.Fields.SUMMARY,
        const.Fields.WORKFLOW,
        const.Fields.ID,
    )  # taglist is required but it does not need to have a value

    # Maps search field values to the fields of the bug
    SEARCH_FIELD_MAPPINGS = {
        # const.SearchFields.DUE_DATE: const.Fields.DUE_DATE,
        const.SearchFields.CREATED_ON: const.Fields.CREATED_ON,
        const.SearchFields.LAST_MODIFIED_ON: const.Fields.LAST_MODIFIED_ON,
        const.SearchFields.PROJECT_ID: const.Fields.PROJECT_ID,
        const.SearchFields.CINEMATIC_ANIMATOR: const.Fields.CINEMATIC_ANIMATOR,
        const.SearchFields.GOALS: const.Fields.GOAL,
    }

    # Fields that should not be included in the payload for creating/updating a bug
    XML_EXCLUDE_FIELDS = (
        const.Fields.IS_WORKING,
        const.Fields.IS_ON_HOLD,
        const.Fields.FIXED_ON_IMPORTANCE,
        const.Fields.HIGH_PRIORITY_FIX,
        const.Fields.CREATED_ON,
        const.Fields.LAST_MODIFIED_ON,
        const.Fields.CINEMATIC_ANIMATOR,
        const.Fields.TIME_TYPE,
        const.Fields.DEPARTMENT_TIME_TYPE,
    )

    def __init__(self, bugstar, index=None, projectId=None, data=None):
        """
        Initializer

        Arguments:
            bugstar (rsg_clients.bugstar.server.BaseServer): the local server to set for this object
            index (int): the id of the bug on bugstar
            projectId (int): id of the project this bug belongs to. This is to ensure that the V1 api can retrieve &
                             update information about the bug by switching to the project it belongs to.
                             This argument is only respected when no bug id is passed in.
            data (dict): data that represents this object
        """
        # all the values that are expected in a bug payload
        if not index and not data:
            data = {const.Fields.PROJECT_ID: projectId}

        return super(Bug, self).__init__(bugstar, index, data)

    def _setup(self, index=None, data=None, server=None):
        """
        Overrides inherited method

        Setups the base properties used by this object. This method should be invoked in the reimplemented __init__ method.

        Arguments:
            index (int): id of the object from the server
            data (dictionary): data that represents this object
            server (rsg_core_py.abstracts.abstractServer.AbstractServer): the local server to set for this object
        """
        super(Bug, self)._setup(index, data, server)
        self._exists = None
        self._exception = None

    def __str__(self):
        """
        Overrides built-in method.

        Replaces what gets displayed when this object is printed or added to a string

        Return:
            str
        """
        return self.toString()

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _getattr(self, item, refresh=True, itemType=None):
        return super(basePayload.BasePayload, self)._getattr(item, refresh, itemType)

    def _getAttachmentData(self, fields=None, returnRaw=False):
        """
        Gets data on the attachments associated with the bug

        Keyword Args:
            fields(dict):
        """
        # Attachment data query requires bug and project ids.
        sections = [
            self.server.restUrl,
            const.Endpoints.V1,
            const.Endpoints.PROJECTS,
            self.projectId,
            const.Endpoints.BUGS,
            self.id,
            const.Endpoints.ATTACHMENTS,
        ]

        fields = fields or {}
        if self.server.delegate is not None:
            fields[const.QueryFields.AS_USER] = self.server.delegateUser.id
        url = self.server.buildUrl(sections, fields=fields)
        # Returns the data of all the attachments.
        return self.server.get(url, returnRaw=returnRaw)

    @connection.resolveProject
    def _projectGet(self, commands, fields=None, contentType=None, isV2=None):
        """
        Convenience method for doing a get requests with the current project id included in the uri.
        This method is decorated around the resolve project

        Arguments:
            commands (list): list of commands to pass into the url
            fields (dict): fields to include in the url
            contentType (str): The content type that this post command should expect.
                               Defaults to json for get commands and xml for a post commands.
            isV2 (bool): Is this a bugstar rest api version 2 style query

        Returns:
            list of basePayload.BasePayload
        """
        return self.project.get(commands=commands, fields=fields, contentType=contentType, isV2=isV2)

    @connection.resolveProject
    def _projectPost(self, commands, fields=None, payload=None, contentType=None, isV2=None):
        """
        Convenience method for doing a post requests with the current project id included in the uri.
        This method is decorated around the resolve project

        Arguments:
            commands (list): list of commands to pass into the url
            fields (dict): fields to include in the url
            contentType (str): The content type that this post command should expect.
                                  Defaults to json for get commands and xml for a post commands.
            isV2 (bool): Is this a bugstar rest api version 2 style query

        Returns:
            list of basePayload.BasePayload
        """
        return self.project.post(commands=commands, payload=payload, fields=fields, contentType=contentType, isV2=isV2)

    @memoize.memoize
    def _internalClasses(self):
        """
        List of class types that are used by Bug class

        Returns:
            tuple
        """
        return (
            self.server.project.cls(),
            self.server.user.cls(),
            self.server.team.cls(),
            self.server.mission.cls(),
            self.server.deadline.cls(),
            self.server.attachment.cls(),
            Bug,
        )

    def _isValidUserFieldValue(self, value):
        return isinstance(value, (self.server.user.cls(), self.server.team.cls(), type(None)))

    def toDict(self, isV2=False):
        return super(Bug, self).toDict(isV2=isV2)

    def toString(self, isV2=False):
        return super(Bug, self).toString(isV2=isV2)

    def create(self):
        """
        Creates a bug on bugstar on the current project based on the information contained in this object.

        The values for developer, tester, summary and workflow are required for creating a bug.
        """
        if self.id is not None:
            raise exceptions.InvalidId("Bug {} already exists on bugstar".format(self.id), includeErrorCode=False)

        self._validate(self.REQUIRED_CREATE_PROPERTIES)

        # Create Attachments that don't exist on the server set
        for attachment in self.attachmentList:
            if not attachment.id:
                attachment.create()

        dictionary = self.project.post([const.Endpoints.BUGS], payload=self)
        self.parse(dictionary.get("bug", {}))
        self._id = self._fields.get(const.Fields.ID, None)
        # Connect the attachments to this new bug
        for attachment in self.attachmentList:
            attachment.connectToBug(self.id)
        self._cache()

    def copy(self, includeAttachments=False):
        """
        Makes a copy in memory of this entity minus the id, state, comments, and doNotMail fields.
        The user must invoke create on the newly copied entity to add it to Bugstar.

        Args:
            includeAttachments(bool): copy attachments as well. This may add extra time to this function as it may be
                                      required to download the files to disk to be able to copy the data.
        Returns:
            rsg_clients.bugstar.payloads.Bug
        """
        copy = super(Bug, self).copy()
        copy._fields.pop(const.Fields.ID, None)
        copy._fields.pop(const.Fields.STATE, None)
        copy._fields.pop(const.Fields.COMMENTS, None)
        # This breaks the create method for whatever reason, so removing it for now
        copy._fields.pop(const.Fields.DO_NOT_MAIL, None)
        attachments = copy._fields.pop(const.Fields.ATTACHMENT_LIST, None) or []
        if includeAttachments:
            copy._fields[const.Fields.ATTACHMENT_LIST] = [attachment.copy() for attachment in attachments]
        return copy

    def update(self, clear=False):
        """
        Updates a bug on bugstar on the current project based on the information contained in this object.

        The values for developer, tester, summary and workflow are required for updating a bug.

        Arguments:
            clear (bool): clears the local edits of this bug after performing the update
        """
        if self.id is None or self._exists == False:
            raise exceptions.InvalidId("This bug has not been added to bugstar yet", includeErrorCode=False)

        if not self._isUpdated:
            self.refresh()

        if self.projectId is None:
            raise exceptions.InvalidProject("This bug is not associated with a project yet")

        # Make sure the update operation is performed on the project the bug belongs to
        project = self.project
        if project.id != self.projectId:
            project = self.server.project.getById(self.projectId)

        self._validate(self.REQUIRED_UPDATE_PROPERTIES)
        # Perform non-update operations first so the changes done by the previous operations are visible
        # in the email sent from bugstar to the owner, qaowner, and cc'd individuals

        # Change Status
        # Note: Statuses can only changed by actions BUT a bug may only accept certain actions based on the current
        # state of the bug and as such a state may not actually be able to changed.
        try:
            clearComments = False
            if self._isFieldDirty(const.Fields.STATE):
                # Is this a valid state that the action can enter
                # If the state is not valid raise error
                self._setState(self.state, project)
                # An action may change the status to a similar but different status than the one requested.
                # So we clear the internal value so the correct one can be set from the update call to bugstar.
                # Example: reopening a bug may switch it to dev(fixing) or dev(reopened) depending on how it was closed
                self._state = None
                self._edits.pop(const.Fields.STATE, None)

            if self._isFieldDirty(const.Fields.ATTACHMENT_LIST):
                self._updateAttachments()

            if self._isFieldDirty(const.Fields.COMMENTS):
                self._addComments()
                clearComments = True

        except exceptions.BugstarException:
            raise

        finally:
            dictionary = project.post([const.Endpoints.BUGS, self.id], payload=self)
            self.parse(dictionary.get("bug", {}))
            if clear:
                self.removeEdits()
            elif not clear and clearComments:
                self.removeEdits([const.Fields.COMMENTS])

    @property
    def project(self):
        """
        Gets the project associated with the bug.

        Returns:
            rsg_clients.bugstar.projects.Project
        """
        project = None
        if self.projectId is not None:
            project = self.server.project(self.projectId)
        return project

    @project.setter
    def project(self, project):
        """
        Sets the project associated with this bug

        Arguments:
            project (rsg_clients.bugstar.payloads.Project): project to set for this bug
        """
        if not isinstance(project, self.server.project.cls()):
            raise exceptions.InvalidProject("{} is not a valid project".format(project))
        self.projectId = project.id

    def refresh(self):
        """
        Re-implements virtual method

        Logic for interpreting data from bugstar and making extra calls for additional fields that the v1 xml
        does not support.
        """
        # Get data to populate the bug with
        dictionary = {}
        if self.id is not None and self._exists is None:
            try:
                dictionary = self._getData(self.server, str(self.id), isV2=False)
                self._exists = True
            except Exception as exception:
                self._exists = False
                self._exception = exception
                raise exception
        elif self.id and self._exists:
            dictionary = self._getData(self.server, str(self.id), isV2=False)
        elif not self._exists:
            # Raise the previously caught exception to avoid pinging the server again
            exception = exceptions.InvalidId("Bug {} does not exist on bugstar".format(self.id), includeErrorCode=False)
            if self._exception:
                exception = self._exception
            raise exception

        # Fill the search dictionary with data that is not included by v1 Get Bug endpoint
        searchDictionary = self._search()

        # Set the project that this bug belongs to as it is a requirement for the v1 Get Bug endpoint
        if self._fields.get(const.Fields.PROJECT_ID, None) is None:
            self._fields[const.Fields.PROJECT_ID] = searchDictionary.pop(const.Fields.PROJECT_ID, None)

        # Get the current state of the mutable data
        if self._isUpdated:
            mutableDifferences = self._getMutableDifferences()

        # Store the previous state of the data being up-to-date or not
        wasUpdated = self._isUpdated
        # Get data to populate the bug with
        dictionary = dictionary.get("bug", {})
        dictionary.update(searchDictionary)
        self.parse(dictionary)

        # Take the mutable data and update it with added/removed content from bugstar
        # Examples include people being removed/added to a cc list, new comments, attachments, etc.
        if wasUpdated:
            self._refreshMutables(mutableDifferences)
        else:
            # In the case that we are grabbing the data after a first update but mutable data was being used.
            # We go ahead and match the previously mutable data with the latest from the server.
            self._matchMutables()

    def _search(self):
        """
        Perform a custom search to retrieve information about the bug that is not returned by the V1 Bug endpoint.
        The dictionary is then returned in a form digestible by the V1 parsing logic.
        """
        search = self.server.search(projects=const.SearchValues.ALL_PROJECTS)
        collection = self.server.search.collection(const.Operations.AND)
        comparision = self.server.search.comparison(const.SearchFields.ID, const.Operations.EQUALS)

        comparision.add(self.id)
        collection.add(comparision)
        search.add(collection)
        for key in self.SEARCH_FIELD_MAPPINGS:
            search.addField(key)

        results = {}
        searchResults = search.bugs(onlyData=True)
        if searchResults:
            results = {
                self.SEARCH_FIELD_MAPPINGS[key]: value
                for key, value in searchResults[0].iteritems()
                if key in self.SEARCH_FIELD_MAPPINGS
            }
        return results

    def _parseV1(self, dictionary):
        """
        Parses Bugstar V1 style dictionaries

        Args:
            dictionary (dict): dictionary generated from bugstar rest api v1
        """
        values = {}
        for key, value in dictionary.iteritems():
            if key in self._listElementNames or key.endswith("List"):
                if isinstance(value, dict):
                    for _, subvalue in value.iteritems():
                        if not isinstance(subvalue, (tuple, list)):
                            subvalue = [subvalue]

                        # TODO: Expand this to include the other list data types
                        if key == const.Fields.COMMENTS:
                            subvalue = [
                                Comment(
                                    self._server, comment["text"], comment["creator"], comment[const.Fields.CREATED_AT]
                                )
                                for comment in subvalue
                            ]

                        elif self._isMutableCast.get(key, False):
                            subvalue = self._castMutable(key, subvalue, self._mutableItemType[key])

                        elif key == const.Fields.DEADLINE_LIST:
                            subvalue = [self.server.deadline(deadlineId) for deadlineId in subvalue]

                        elif key == const.Fields.ATTACHMENT_LIST:
                            subvalue = [self.attachment(attachmentId) for attachmentId in subvalue]

                        elif key == const.Fields.TAG_LIST:
                            tags = []
                            for tag in subvalue:
                                try:
                                    tags.append(conversions.convertToAscii(tag))
                                except AttributeError:
                                    # Our generic parsing logic is converting tags that are numbers into ints
                                    # We don't want that here so we are simply going to convert those values back to strings
                                    tags.append(str(tag))
                            subvalue = tags
                        elif key == const.Fields.PLATFORM_LIST:
                            subvalue = [self.server.platform(platformId) for platformId in subvalue]

                        elif key in (
                            const.Fields.DEPENDENTS_LIST,
                            const.Fields.BLOCKERS_LIST,
                            const.Fields.CHILDREN_LIST,
                        ):
                            subvalue = [self.server.bug(bugId) for bugId in subvalue]

                        value = subvalue
                        break

                elif isinstance(value, unicode):
                    value = conversions.convertToAscii(value)
                    if key == const.Fields.GOAL:
                        names = value.split("|")
                        value = []
                        for name in names:
                            goal = self.server.goal.getByName(name.strip(), project=self.project)
                            if goal:
                                value.append(goal[0])

            elif value in ("true", "false"):
                value = value == "true"

            elif isinstance(value, unicode):
                value = conversions.convertToAscii(value)  # This will need to be changed for python 3

            values[key] = value
        self._fields.update(values)

    def _xmlElementV1(self):
        """
        xml element that represents this payload

        Returns:
            xml.etree.cElementTree.Element
        """

        element = xml.Element("bug")

        edits = dict(self._edits)
        edits.update(self._mutables)

        fields = dict(self._fields)
        fields.update(edits)

        for key in sorted(fields.iterkeys()):
            uneditedValue = self._fields.get(key, None)
            value = edits.get(key, uneditedValue)
            if key in self.XML_EXCLUDE_FIELDS:
                continue
            elif key == const.Fields.TAG_LIST and not value:
                value = ""
            elif key == const.Fields.GOAL and isinstance(value, (list, tuple)):
                value = " | ".join([goal.name for goal in value])
            elif key == const.Fields.DUE_DATE and fields.get(const.Fields.DEADLINE_LIST, None):
                # The v1 endpoint doesn't support setting a due date if there is deadline or dependent with a deadline
                continue
            elif value is None:
                continue
            elif isinstance(value, (list, tuple)) and len(value) == 0 and value == uneditedValue:
                continue
            elif isinstance(value, self._internalClasses()):
                value = value.id
            elif isinstance(value, datetime.datetime):
                datetimeFormat = self.DATETIME_FORMAT
                if key in self.LONG_DATETIME_PROPERTIES:
                    datetimeFormat = self.LONG_DATETIME_FORMAT
                value = value.strftime(datetimeFormat)
            elif isinstance(value, bool):
                value = str(value).lower()

            valueElement = xml.Element(key)

            if isinstance(value, list):
                subvalueName = self._listElementNames.get(key, key.replace("List", ""))
                for subvalue in value:
                    subvalueElement = xml.Element(subvalueName)
                    if isinstance(subvalue, Comment):
                        for name in ("text", const.Fields.CREATED_ON, "creator"):
                            commentElement = xml.Element(name)
                            commentValue = subvalue.__getattribute__(name)
                            if isinstance(commentValue, self.server.user.cls()):
                                commentValue = commentValue.id
                            commentElement.text = str(commentValue)
                            subvalueElement.append(commentElement)
                    elif isinstance(subvalue, self._internalClasses()):
                        subvalueElement.text = str(subvalue.id)
                    else:
                        subvalueElement.text = str(subvalue)
                    valueElement.append(subvalueElement)
            else:
                valueElement.text = str(value)

            element.append(valueElement)
        return element

    def attachment(self, index=None, data=None):
        """
        Creates an attachment for this bug

        Keyword Args:
            index(int): bugstar id of the attachment
            data(dict): payload data

        Returns:
            rsg_clients.bugstar.payloads.attachment.Attachment
        """
        return self.server.attachment(index, self.id, data)

    @property
    def exists(self):
        """ Does this bug exist in bugstar """
        if self._exists is None and self._id:
            try:
                self._getData(self.server, str(self.id), isV2=False)
                self._exists = True
            except (exceptions.BadRequest, exceptions.PermissionDenied) as exception:
                self._exception = exception
                self._exists = False
        return self._exists or False

    @property
    def id(self):
        """ id is readable bugstar property """
        return self._id

    @property
    def projectId(self):
        """ projectId is readable bugstar property """
        # making populate flag to be based on "projectId" existance in fields to avoid the endless loop while
        # trying to query data from the server since it is requiring the projectId for the query.
        return self._getattr(const.Fields.PROJECT_ID, refresh=const.Fields.PROJECT_ID not in self._fields, itemType=int)

    @projectId.setter
    def projectId(self, value):
        """
        projectId is writeable bugstar property

        Args:
            value(int): id of the project
        """
        self._setattr(const.Fields.PROJECT_ID, value)

    @property
    def summary(self):
        """ summary is readable bugstar property """
        return self._getattr(const.Fields.SUMMARY)

    @summary.setter
    def summary(self, value):
        """
        summary is writeable bugstar property

        Args:
            value(str): the summary/title of the bug
        """
        self._setattr(const.Fields.SUMMARY, value)

    @property
    def description(self):
        """ description is readable bugstar property """
        return self._getattr(const.Fields.DESCRIPTION, itemType=str)

    @description.setter
    def description(self, value):
        """
        description is writeable bugstar property

        Args:
            value (str): description of the bug
        """
        self._setattr(const.Fields.DESCRIPTION, value)

    @property
    def workflow(self):
        """ workflow is readable bugstar property """
        return self._getattr(const.Fields.WORKFLOW)

    @workflow.setter
    def workflow(self, value):
        """
        workflow is writeable bugstar property

        Args:
            value(int): the workflow for the bug
        """
        self._setattr(const.Fields.WORKFLOW, value)

    @property
    def tagList(self):
        """ tagList is readable bugstar property """
        return self._getmutable(const.Fields.TAG_LIST, mutableType=list)

    @tagList.setter
    def tagList(self, value):
        """
        tagList is writeable bugstar property

        Args:
            value (list): list of tags for the bug
        """
        self._setattr(const.Fields.TAG_LIST, value, isMutable=True)

    @property
    def developer(self):
        """ developer is readable bugstar property """
        value = self._getattr(const.Fields.DEVELOPER)
        if not self._isValidUserFieldValue(value):
            self.developer = value
            value = self._getattr(const.Fields.DEVELOPER)
        return value

    @developer.setter
    def developer(self, value):
        """
        developer is writeable bugstar property

        Args:
            value (int or rsg_clients.bugstar.payloads.user.User): The user to set
        """
        if isinstance(value, int):
            value = self._getValidUserPayload(value)
        self._setattr(const.Fields.DEVELOPER, value)

    @property
    def tester(self):
        """ tester is readable bugstar property """
        value = self._getattr(const.Fields.TESTER)
        if not self._isValidUserFieldValue(value):
            self.tester = value
            value = self._getattr(const.Fields.TESTER)
        return value

    @tester.setter
    def tester(self, value):
        """
        tester is writeable bugstar property

        Args:
            value (int|rsg_clients.bugstar.payloads.user.User): The user to set
        """
        if isinstance(value, int):
            value = self._getValidUserPayload(value)
        self._setattr(const.Fields.TESTER, value)

    @property
    def reviewer(self):
        """ reviewer is readable bugstar property """
        value = self._getattr(const.Fields.REVIEWER)
        if not self._isValidUserFieldValue(value):
            self.reviewer = value
            value = self._getattr(const.Fields.REVIEWER)
        return value

    @reviewer.setter
    def reviewer(self, value):
        """
        reviewer is writeable bugstar property

        Args:
            value (int|rsg_clients.bugstar.payloads.user.User): The user to set
        """
        if isinstance(value, int):
            value = self._getValidUserPayload(value)
        self._setattr(const.Fields.REVIEWER, value)

    @property
    def cinematicAnimator(self):
        """ Cinematic Animator is readable bugstar property """
        value = self._getattr(const.Fields.CINEMATIC_ANIMATOR)
        if not self._isValidUserFieldValue(value):
            self.cinematicAnimator = value
            value = self._getattr(const.Fields.CINEMATIC_ANIMATOR)
        return value

    @cinematicAnimator.setter
    def cinematicAnimator(self, value):
        """
        Cinematic Animator is writeable bugstar property

        Args:
            value (int|rsg_clients.bugstar.payloads.user.User): The user to set
        """
        if isinstance(value, int):
            value = self._getValidUserPayload(value)
        self._setattr(const.Fields.CINEMATIC_ANIMATOR, value)

    @property
    def category(self):
        """ category is readable bugstar property """
        return self._getattr(const.Fields.CATEGORY)

    @category.setter
    def category(self, value):
        """
        category is writeable bugstar property

        Args:
            value (int): bug category
        """
        self._setattr(const.Fields.CATEGORY, value)

    @property
    def priority(self):
        """ priority is readable bugstar property """
        return self._getattr(const.Fields.PRIORITY)

    @priority.setter
    def priority(self, value):
        """
        priority is writeable bugstar property

        Args:
            value (int): priority of the bug
        """
        self._setattr(const.Fields.PRIORITY, value)

    @property
    def state(self):
        """ state is readable bugstar property """
        return self._getattr(const.Fields.STATE)

    # TODO: Update setter to handle which new states are valid states this bug can transition to
    @state.setter
    def state(self, value):
        """
        state is writeable bugstar property

        Args:
            value (str): codename of the status
        """
        self._setattr(const.Fields.STATE, value)

    @property
    def x(self):
        """ x is readable bugstar property """
        return self._getattr(const.Fields.X)

    @x.setter
    def x(self, value):
        """ x is writeable bugstar property """
        self._setattr(const.Fields.X, value)

    @property
    def y(self):
        """ y is readable bugstar property """
        return self._getattr(const.Fields.Y)

    @y.setter
    def y(self, value):
        """ y is writeable bugstar property """
        self._setattr(const.Fields.Y, value)

    @property
    def z(self):
        """ z is readable bugstar property """
        return self._getattr(const.Fields.Z)

    @z.setter
    def z(self, value):
        """ z is writeable bugstar property """
        self._setattr(const.Fields.Z, value)

    @property
    def xf(self):
        """ xf is readable bugstar property """
        return self._getattr(const.Fields.XF)

    @xf.setter
    def xf(self, value):
        """ xf is writeable bugstar property """
        self._setattr(const.Fields.XF, value)

    @property
    def yf(self):
        """ yf is readable bugstar property """
        return self._getattr(const.Fields.YF)

    @yf.setter
    def yf(self, value):
        """ yf is writeable bugstar property """
        self._setattr(const.Fields.YF, value)

    @property
    def zf(self):
        """ zf is readable bugstar property """
        return self._getattr(const.Fields.ZF)

    @zf.setter
    def zf(self, value):
        """ zf is writeable bugstar property """
        self._setattr(const.Fields.ZF, value)

    @property
    def todhrs(self):
        """ todhrs is readable bugstar property """
        return self._getattr(const.Fields.TODHRS)

    @todhrs.setter
    def todhrs(self, value):
        """ todhrs is writeable bugstar property """
        self._setattr(const.Fields.TODHRS, value)

    @property
    def todmins(self):
        """ todmins is readable bugstar property """
        return self._getattr(const.Fields.TODMINS)

    @todmins.setter
    def todmins(self, value):
        """ todmins is writeable bugstar property """
        self._setattr(const.Fields.TODMINS, value)

    @property
    def map(self):
        """ map is readable bugstar property """
        value = self._getattr(const.Fields.MAP)
        if isinstance(value, int):
            self.map = value
            value = self._getattr(const.Fields.MAP)
        return value

    @map.setter
    def map(self, value):
        """ map is writeable bugstar property """
        if isinstance(value, int):
            value = self.server.gameMap(value)
        self._setattr(const.Fields.MAP, value)

    @property
    def grid(self):
        """ grid is readable bugstar property """
        value = self._getattr(const.Fields.GRID)
        if not isinstance(value, (self.server.grid.cls(), types.NoneType)):
            self.grid = value
            value = self._getattr(const.Fields.GRID)
        return value

    @grid.setter
    def grid(self, value):
        """ grid is writeable bugstar property """
        if isinstance(value, int):
            value = self.server.grid(value)
        self._setattr(const.Fields.GRID, value)

    @property
    def mission(self):
        """ mission is readable bugstar property """
        value = self._getattr(const.Fields.MISSION)
        if not isinstance(value, (self.server.mission.cls(), types.NoneType)):
            self.mission = value
            value = self._getattr(const.Fields.MISSION)
        return value

    @mission.setter
    def mission(self, value):
        """ mission is writeable bugstar property """
        if isinstance(value, int):
            value = self.server.mission(value)
        self._setattr(const.Fields.MISSION, value)

    @property
    def component(self):
        """ component is readable bugstar property """
        value = self._getattr(const.Fields.COMPONENT)
        if not isinstance(value, (self.server.component.cls(), types.NoneType)):
            self.component = value
            value = self._getattr(const.Fields.COMPONENT)
        return value

    @component.setter
    def component(self, value):
        """ component is writeable bugstar property """
        if isinstance(value, int):
            value = self.server.component(value)
        self._setattr(const.Fields.COMPONENT, value)

    @property
    def attachmentList(self):
        """ attachmentList is readable bugstar property """
        return self._getmutable(const.Fields.ATTACHMENT_LIST, mutableType=list)

    @attachmentList.setter
    def attachmentList(self, value):
        """ attachmentList is writeable bugstar property """
        self._setattr(const.Fields.ATTACHMENT_LIST, value, isMutable=True)

    @property
    def platformList(self):
        """ platformList is readable bugstar property """
        return self._getmutable(const.Fields.PLATFORM_LIST, list)

    @platformList.setter
    def platformList(self, value):
        """ platformList is writeable bugstar property """
        self._setattr(const.Fields.PLATFORM_LIST, value, isMutable=True)

    @property
    def environmentList(self):
        """ environmentList is readable bugstar property """
        return self._getmutable(const.Fields.ENVIRONMENT_LIST, mutableType=list)

    @environmentList.setter
    def environmentList(self, value):
        """ environmentList is writeable bugstar property """
        self._setattr(const.Fields.ENVIRONMENT_LIST, value, isMutable=True)

    @property
    def deadlineList(self):
        """ deadlineList is readable bugstar property """
        return self._getmutable(const.Fields.DEADLINE_LIST, mutableType=list)

    @deadlineList.setter
    def deadlineList(self, value):
        """ deadlineList is writeable bugstar property """
        self._setattr(const.Fields.DEADLINE_LIST, value, isMutable=True)

    @property
    def dependentsList(self):
        """ dependentsList is readable bugstar property """
        return self._getmutable(const.Fields.DEPENDENTS_LIST, mutableType=list)

    @dependentsList.setter
    def dependentsList(self, value):
        """ dependentsList is writeable bugstar property """
        self._setattr(const.Fields.DEPENDENTS_LIST, value, isMutable=True)

    @property
    def foundIn(self):
        """ foundIn is readable bugstar property """
        return self._getattr(const.Fields.FOUND_IN)

    @foundIn.setter
    def foundIn(self, value):
        """ foundIn is writeable bugstar property """
        self._setattr(const.Fields.FOUND_IN, value)

    @property
    def fixedIn(self):
        """ fixedIn is readable bugstar property """
        value = self._getattr(const.Fields.FIXED_IN)
        if isinstance(value, int):
            self.fixedIn = value
            value = self._getattr(const.Fields.FIXED_IN)
        return value

    @fixedIn.setter
    def fixedIn(self, value):
        """ fixedIn is writeable bugstar property """
        if isinstance(value, int):
            value = self.server.build(value)
        self._setattr(const.Fields.FIXED_IN, value)

    @property
    def attempts(self):
        """ attempts is readable bugstar property """
        return self._getattr(const.Fields.ATTEMPTS)

    @attempts.setter
    def attempts(self, value):
        """ attempts is writeable bugstar property """
        self._setattr(const.Fields.ATTEMPTS, value)

    @property
    def verifications(self):
        """ verifications is readable bugstar property """
        return self._getattr(const.Fields.VERIFICATIONS)

    @verifications.setter
    def verifications(self, value):
        """ verifications is writeable bugstar property """
        self._setattr(const.Fields.VERIFICATIONS, value)

    @property
    def seen(self):
        """ seen is readable bugstar property """
        return self._getattr(const.Fields.SEEN)

    @seen.setter
    def seen(self, value):
        """ seen is writeable bugstar property """
        self._setattr(const.Fields.SEEN, value)

    @property
    def difficulty(self):
        """ difficulty is readable bugstar property """
        return self._getattr(const.Fields.DIFFICULTY)

    @difficulty.setter
    def difficulty(self, value):
        """ difficulty is writeable bugstar property """
        self._setattr(const.Fields.DIFFICULTY, value)

    @property
    def timeSpent(self):
        """ timeSpent is readable bugstar property """
        value = self._getattr(const.Fields.TIME_SPENT)
        return value if value < -1 else 0.0

    @timeSpent.setter
    def timeSpent(self, value):
        """ timeSpent is writeable bugstar property """
        self._setattr(const.Fields.TIME_SPENT, value)

    @property
    def qaTimeSpent(self):
        """ qaTimeSpent is readable bugstar property """
        return self._getattr(const.Fields.QA_TIME_SPENT)

    @qaTimeSpent.setter
    def qaTimeSpent(self, value):
        """ qaTimeSpent is writeable bugstar property """
        self._setattr(const.Fields.QA_TIME_SPENT, value)

    @property
    def estimatedTime(self):
        """ estimatedTime is readable bugstar property """
        return self._getattr(const.Fields.ESTIMATED_TIME)

    @estimatedTime.setter
    def estimatedTime(self, value):
        """ estimatedTime is writeable bugstar property """
        self._setattr(const.Fields.ESTIMATED_TIME, value)

    @property
    def doNotMail(self):
        """ doNotMail is readable bugstar property """
        return self._getattr(const.Fields.DO_NOT_MAIL)

    @doNotMail.setter
    def doNotMail(self, value):
        """ doNotMail is writeable bugstar property """
        self._setattr(const.Fields.DO_NOT_MAIL, value)

    @property
    def awaitingUser(self):
        """ awaitingUser is readable bugstar property """
        value = self._getattr(const.Fields.AWAITING_USER)
        if not self._isValidUserFieldValue(value):
            self.awaitingUser = value
            value = self._getattr(const.Fields.AWAITING_USER)
        return value

    @awaitingUser.setter
    def awaitingUser(self, value):
        """ awaitingUser is writeable bugstar property """
        if isinstance(value, int) and value > 0:
            value = self._getValidUserPayload(value)
        elif value < 0:
            value = None
        self._setattr(const.Fields.AWAITING_USER, value)

    @property
    def progress(self):
        """ progress is readable bugstar property """
        return self._getattr(const.Fields.PROGRESS)

    @progress.setter
    def progress(self, value):
        """ progress is writeable bugstar property """
        self._setattr(const.Fields.PROGRESS, value)

    @property
    def goal(self):
        """
        goal is readable bugstar property

        Note:
            Due to how the getByName method works, this property will fail to retrieve information for inactive goals
            until the bug endpoint is updated to include the id of the goal.
            The Inactive endpoint required to get disabled goals is only available to super users

        Return:
            list of rsg_clients.bugstar.payload.goal.Goal
        """

        return self._getmutable(const.Fields.GOAL, list)

    @property
    def activeFrames(self):
        """ activeFrames is readable bugstar property """
        return self._getattr(const.Fields.ACTIVE_FRAMES)

    @activeFrames.setter
    def activeFrames(self, value):
        """ activeFrames is writeable bugstar property """
        self._setattr(const.Fields.ACTIVE_FRAMES, value)

    @property
    def p4ChangeLists(self):
        """ p4Changelists is readable bugstar property """
        return self._getmutable(const.Fields.P4_CHANGELISTS, mutableType=list)

    @property
    def parent(self):
        """ parent is readable bugstar property """
        value = self._getattr(const.Fields.PARENT_ID)
        if isinstance(value, int):
            self.parent = value
            value = self._getattr(const.Fields.PARENT_ID)
        return value

    @parent.setter
    def parent(self, value):
        """
        parent is writeable bugstar property

        Args:
            value (int|rsg_clients.bugstar.payloads.user.User): The user to set
        """
        if isinstance(value, int):
            value = self.server.bug(value)
        self._setattr(const.Fields.PARENT_ID, value)

    @p4ChangeLists.setter
    def p4ChangeLists(self, value):
        """ p4Changelists is writeable bugstar property """
        self._setattr(const.Fields.P4_CHANGELISTS, value, isMutable=True)

    @property
    def ccList(self):
        """ ccList is readable bugstar property """
        return self._getmutable(const.Fields.CC_LIST, mutableType=list, itemType=self._server.actor)

    @ccList.setter
    def ccList(self, value):
        """ ccList is writeable bugstar property """
        self._setattr(const.Fields.CC_LIST, value, isMutable=True)

    @property
    def childrenList(self):
        """ childrenList is readable bugstar property """
        # TODO: Edit logic to be better for handling the different data types
        return self._getmutable(const.Fields.CHILDREN_LIST, mutableType=list)

    @childrenList.setter
    def childrenList(self, value):
        """ childrenList is writeable bugstar property """
        self._setattr(const.Fields.CHILDREN_LIST, value, isMutable=True)

    @property
    def blockersList(self):
        """ blockersList is readable bugstar property """
        # TODO: Edit logic to be better for handling the different data types
        return self._getmutable(const.Fields.BLOCKERS_LIST, mutableType=list)

    @blockersList.setter
    def blockersList(self, value):
        """ blockersList is writeable bugstar property """
        self._setattr(const.Fields.BLOCKERS_LIST, value, isMutable=True)

    @property
    def verifiedBy(self):
        """ verifiedBy is readable bugstar property """
        value = self._getattr(const.Fields.VERIFIED_BY)
        if not self._isValidUserFieldValue(value):
            self.verifiedBy = value
            value = self._getattr(const.Fields.VERIFIED_BY)

        return value

    @verifiedBy.setter
    def verifiedBy(self, value):
        """ verifiedBy is writeable bugstar property """
        if isinstance(value, int) and value > 0:
            value = self._getValidUserPayload(value)
        elif value < 0:
            value = None
        self._setattr(const.Fields.VERIFIED_BY, value)

    @property
    def comments(self):
        """ comments is readable bugstar property """
        return self._getmutable(const.Fields.COMMENTS, mutableType=list)

    @comments.setter
    def comments(self, value):
        """ comments is writeable bugstar property """
        self._setattr(const.Fields.COMMENTS, value, isMutable=True)

    @property
    def updateFlag(self):
        """ updateFlag is readable bugstar property """
        return self._getattr(const.Fields.UPDATE_FLAG)

    @updateFlag.setter
    def updateFlag(self, value):
        """ updateFlag is writeable bugstar property """
        self._setattr(const.Fields.UPDATE_FLAG, value)

    @property
    def isWorking(self):
        """ isWorking is readable bugstar property """
        return self._getattr(const.Fields.IS_WORKING)

    @isWorking.setter
    def isWorking(self, value):
        """ updateFlag is writeable bugstar property """
        self._setattr(const.Fields.IS_WORKING, value)

    @property
    def isOnHold(self):
        """ isOnHold is readable bugstar property """
        return self._getattr(const.Fields.IS_ON_HOLD)

    @property
    def fixedOnImportance(self):
        """ fixedOnImportance is readable bugstar property """
        return self._getattr(const.Fields.FIXED_ON_IMPORTANCE)

    @property
    def highPriorityFix(self):
        """ isOnHold is readable bugstar property """
        return self._getattr(const.Fields.HIGH_PRIORITY_FIX)

    @property
    def dueDate(self):
        """ Due date is readable bugstar property """
        value = self._getattr(const.Fields.DUE_DATE)
        if isinstance(value, basestring):
            self.dueDate = value
            value = self._getattr(const.Fields.DUE_DATE)
        return value

    @dueDate.setter
    def dueDate(self, value):
        """ Due date is a writeable bugstar property """
        if isinstance(value, basestring):
            value = conversions.toDateTime(value)
        self._setattr(const.Fields.DUE_DATE, value)

    @property
    def createdOn(self):
        """ Due date is readable bugstar property """
        value = self._getattr(const.Fields.CREATED_ON)
        if isinstance(value, basestring):
            value = conversions.toDateTime(value)
            self._fields[const.Fields.CREATED_ON] = value
        return value

    @property
    def lastModifiedOn(self):
        """ last modified on is readable bugstar property """
        value = self._getattr(const.Fields.LAST_MODIFIED_ON)
        if isinstance(value, basestring):
            value = conversions.toDateTime(value)
            self._fields[const.Fields.LAST_MODIFIED_ON] = value
        return value

    @property
    def timeType(self):
        """
        The type of time to assign when adding time to the bug
        """
        return self._fields.get(const.Fields.TIME_TYPE, const.TimeTypes.WORK)

    @timeType.setter
    def timeType(self, value):
        """
        The type of time to assign when adding time to the bug

        Args:
            value (str): time type to set when updating the time spent on a bug
        """
        self._fields[const.Fields.TIME_TYPE] = value

    @property
    def departmentType(self):
        """
        The type of department to assign when adding time to the bug
        """
        return self._fields.get(const.Fields.DEPARTMENT_TIME_TYPE, const.TimeTypes.DEVELOPMENT)

    @departmentType.setter
    def departmentType(self, value):
        """
        The type of department to assign when adding time to the bug

        Args:
            value (str): department type to set when updating the time spent on a bug
        """
        self._fields[const.Fields.DEPARTMENT_TIME_TYPE] = value

    def availableActions(self):
        """
        List of actions to the change the status fo the bug that can be performed based on the current state and
        workflow set.
        """
        return self._availableActions(self._fields[const.Fields.STATE], self.workflow)

    @memoize.memoize
    def _availableActions(self, state, workflow):
        """
        Gets the available actions for this bug. This is determined by the current action and the bug type.
        The arguments are used by the memoized decorator to cache the results of the rest query.

        Args:
            state (str): the current action of the bug
            workflow (str): the workflow type of the current bug.

        Returns:
            list
        """
        actions = self._projectGet([const.Endpoints.BUGS, self.id, const.Endpoints.NEXT_ACTIONS]).get("nextActions", {})
        availableActions = [
            const.Actions.getActionFromName(action) for action, valid in actions.iteritems() if valid != "false"
        ]
        return availableActions

    def _setState(self, state=None, project=None):
        """
        Sets the status of the bug on bugstar

        Keyword Args:
            state (str): codename of the status
            project (rsg_clients.bugstar.payloads.project.Project): project the bug belongs to

        Returns:
            bool
        """
        # Get the current state
        currentState = self._fields.get(const.Fields.STATE, None)
        if state is None:
            state = self.state

        # Exit early if the current state and desired state are the same
        if currentState == state:
            return False

        # Get the project if one isn't specified
        if project is None:
            project = self.project
            if project.id != self.projectId:
                project = self.server.project(self.projectId)

        # Get the action required to switch to a given state
        action = const.States.getActionFromState(self._fields[const.Fields.WORKFLOW], state, currentState)
        availableActions = self.availableActions()
        if action not in availableActions:
            raise exceptions.InvalidStatus(
                "Bug {id} currently can't perform action {action} with the {workflow} workflow to switch status to {newState} from {oldState}\n"
                "Performable actions on a bug whose current state is {oldState} and using the {workflow} workflow are:\n"
                "{actions}".format(
                    id=self.id,
                    action=action,
                    workflow=const.Workflows.getWorkflowName(self.workflow),
                    newState=state,
                    oldState=currentState,
                    actions=", ".join(availableActions),
                )
            )

        if action == const.Actions.RESOLVE and self._fields[const.Fields.WORKFLOW] != const.Workflows.SIMPLE:
            self._addTime(self.developer)
        project.get(
            [const.Endpoints.BUGS, self.id, const.Endpoints.ACTION, action],
            fields={const.QueryFields.COMMENT: "", const.QueryFields.SEND_MAIL: self.doNotMail is False},
        )
        return True

    def _addTime(self, user_):
        """
        Adds time to a bug

        Args:
            user_(rsg_clients.bugstar.payloads.User): the user to allocate the time to
        """
        timeSpent = self.timeSpent  # local value
        previousTimeSpent = self._fields.get(const.Fields.TIME_SPENT, 0)  # last value from bugstar
        timeDifference = timeSpent - previousTimeSpent

        timeRecord = TimeRecord()
        timeRecord.bug = self
        timeRecord.timeType = self.timeType
        timeRecord.departmentType = self.departmentType
        timeRecord.time = int(timeDifference)
        timeRecord.attributeTimeTo = user_ or self.developer

        self._projectPost([const.Endpoints.BUGS, self.id, const.Endpoints.BUG_TIME_RECORD], payload=timeRecord)

    def _addComments(self):
        """Adds new comments for this bug in bugstar."""
        # TODO: Look into caching comments based on bugstar id and position on the bug
        comments = list(self._fields.get(const.Fields.COMMENTS, []))
        for comment in self.comments:
            if comment in comments:
                # This is to ensure that you can add multiple comments that have the same text
                comments.remove(comment)
                continue
            comment.add(self)

    def _updateAttachments(self):
        """
        Updates bug attachments on the server.
        """
        mutableAttachments = self._getMutableDifferences()[const.Fields.ATTACHMENT_LIST]
        # Connect/create attachments that don't exist
        for attachment in mutableAttachments.append:
            if not attachment.id:
                attachment.create()
            if not attachment.bug:
                attachment.bug = self
            attachment.connectToBug()

        # Disconnect attachments
        for attachment in mutableAttachments.remove:
            attachment.disconnectFromBug()

    def downloadAttachment(self, path=None, attachmentId=None):
        """
        Downloads the attachment to the folder Downloads/Bug_attachments/{index}

        Keyword Args:
            path (str): where to save the attachment
            attachmentId (int): ID number of the attachment to download
                        If no ID is passed, then it will download all the attachments.
        """
        dlDir = path or str(pathlib.Path.home().joinpath("Downloads", "BugstarAttachments", str(self.id)))

        # Create the folder in case it doesn't exist.
        if not os.path.exists(dlDir):
            os.makedirs(dlDir)
        # If the id is passed, download only that attachment
        attachmentList = self.attachmentList
        if attachmentId:
            attachmentList = [self.attachment(attachmentId)]
        # If there is no ID passed, download all the attachments on current bug
        return [attachment.download(dlDir) for attachment in attachmentList]

    def toString(self, isV2=False):
        """Overrides inherited method.

        Set the bug to use only the v1 endpoint.

        Returns:
            str
        """
        return super(Bug, self).toString(isV2=isV2)

class Comment(basePayload.BasePayload):
    """
    Class for representing a comment from a bug on bugstar
    """

    IS_REQUESTABLE = False

    def __new__(cls, *args, **kwargs):
        """
        Overrides inherited method

        Restores original behavior as objects from this class don't need to be cached.
        """
        return object.__new__(cls)

    def __init__(self, bugstar, text, creator=None, createdAt=None):
        """
        Initializer

        Args:
            bugstar (rsg_clients.bugstar.server.BaseServer): the local server to set for this object
            text (str): text of the comment

        Keyword Args:
            creator (rsg_clients.bugstar.payloads.User): user that created this comment
            createdAt (str): time this comment was created at
        """
        if not isinstance(text, basestring):
            # The convert data method will convert comments that are just pure numbers to int/floats
            # It is simpler and easier to fix here than in the convertData method
            text = str(text)
        self.server = bugstar
        # Odd characters from India and extra bytecode from Microsoft Office applications cause errors, so we need to decode the string again
        self.text = conversions.convertToAscii(text, xml=True)
        self.createdOn = createdAt
        self._creator = creator

    @property
    def creator(self):
        if not isinstance(self._creator, (self.server.user.cls(), types.NoneType)):
            # Get data from Bugstar
            self._creator = self.server.user(self._creator)
        return self._creator

    def add(self, bug):
        """Adds the comment to the bug on bugstar and updates the internal data for the comment.

        Args:
            bug (Bug): bug to add the comment to on the server.
        """
        results = bug._projectPost([const.Endpoints.BUGS, bug.id, const.Endpoints.COMMENTS], payload=self)
        newData = results.get("comment", {})
        creatorId = newData.get("creator", self._creator)
        if isinstance(creatorId, str):
            creatorId = int(creatorId)
        self._creator = creatorId
        self.createdOn = newData.get("createdAt", self.createdOn)

    def _xmlElementV1(self):
        """
        Overrides inherited method.

        Converts the contents of this class into an xml format that is consumable by bugstar

        Returns:
            xml.etree.cElement.Element
        """
        element = xml.Element("comment")
        xml.SubElement(element, "text").text = self.text
        return element

    def __eq__(self, other):
        """
        Overrides built-in method

        Compares the text of this object to the passed object

        Arguments:
            other (object): object to compare against

        Returns:
            bool
        """
        return other == self.text

    def __ne__(self, other):
        """
        Overrides built-in method

        Compares the text of this object to the passed object

        Arguments:
            other (object): object to compare against

        Returns:
            bool
        """
        return other != self.text

    def toString(self, isV2=False):
        """Overrides inherited method.

        Set the comment to use only the v1 endpoint.

        Returns:
            str
        """
        return super(Comment, self).toString(isV2=isV2)

class TimeRecord(basePayload.BasePayload):
    """
    Time record object for bugstar
    """

    def __new__(cls, *args, **kwargs):
        """
        Overrides inherited method

        Restores original behavior as objects from this class don't need to be cached.
        """
        return object.__new__(cls, *args, **kwargs)

    def __init__(self):
        self.bug = None
        self.time = None  # in minutes
        self.attributeTimeTo = None
        self.timeType = const.TimeTypes.WORK
        self.departmentType = const.TimeTypes.DEVELOPMENT

    def _xmlElementV1(self):
        """
        Overrides the inherited method

        Returns:
            xml.cElementTree.etree.Element
        """
        element = xml.Element("BugTimeRecord")
        xml.SubElement(element, const.Fields.BUG_ID).text = str(self.bug.id)
        xml.SubElement(element, const.Fields.TIME_TYPE).text = self.timeType
        xml.SubElement(element, const.Fields.DEPARTMENT_TIME_TYPE).text = self.departmentType
        xml.SubElement(element, const.Fields.TIME).text = str(self.time)
        xml.SubElement(element, const.Fields.ATTRIBUTE_TIME_TO_ID).text = (
            str(self.attributeTimeTo.id) if self.attributeTimeTo else None
        )
        xml.SubElement(element, const.Fields.COMMENT_TEXT).text = "Adding Time for {}".format(self.attributeTimeTo.name)
        xml.SubElement(element, const.Fields.ATTACHMENT_ID)
        return element

    def toString(self, isV2=False):
        """Overrides inherited method.

        Set the time record to use only the v1 endpoint.

        Returns:
            str
        """
        return super(TimeRecord, self).toString(isV2=isV2)


class Builder(basePayload.BaseBuilder):
    """Intermediate class for building instances of the Bug class."""

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Bug

    def comment(self, text, creator=None, createdAt=None):
        """
        Creates a comment payload

        Args:
            text (str): text of the comment

        Keyword Args:
            creator (rsg_clients.bugstar.payloads.User): user that created this comment
            createdAt (str): time this comment was created at
        """
        return Comment(self._server, text, creator, createdAt)

    def timeRecord(self):
        """
        Creates a time record payload
        """
        return TimeRecord()

    def search(self, index=None, root=None):
        """Creates a search payload that returns bugs.

        Args:
            index (int, optional): The id of the search on bugstar.
            root (rsg_clients.bugstar.search.Collection): The root collection of the search

        Returns:
            rsg_clients.bugstar.search.Search
        """
        return self._server.search(index=index, root=root, endpoint=const.Endpoints.BUGS)

    def isComment(self, item):
        """Is this object a bugstar comment."""
        return isinstance(item, Comment)

    def isTimeRecord(self, item):
        """Is this object a bugstar time record."""
        return isinstance(item, TimeRecord)

    def getByDescription(self, projects, description, collection=None, returnSearch=False, closed=False):
        """Gets bugs by their description.

        This method is intended to be used for finding duplicated bugs or to check if a bug with the same
        description already exists.

        Args:
            projects (list): list of projects to look for bugs in.
            description (str): The description to look for in the summary.
            collection (rsg_clients.bugstar.entities.search.Collection): Additional search parameters to include in the search.
            returnSearch (bool): return the search object instead of the results of the search.
            closed (bool): Check for closed bugs if True else filter closed bugs
        """
        # Set which projects we are looking for the bugs in.
        rootCollection = self._server.search.collection(const.Operations.AND)
        projectComparison = self._server.search.comparison(const.SearchFields.PROJECT, const.Operations.IN)
        for project in projects:
            projectComparison.add(project)
        rootCollection.add(projectComparison)

        # create core collection and operation
        coreCollection = self._server.search.collection(const.Operations.AND)

        # add any user defined collection
        if collection:
            coreCollection.add(collection)

        # remove closed state bugs if not needed
        if not closed:
            statusComparison = self._server.search.comparison(const.SearchFields.STATUS, const.Operations.IN)
            statusComparison.add(const.SearchValues.NOT_CLOSED_STATES, datatype=const.ValueTypes.PARAMETER)
            coreCollection.add(statusComparison)

        # simple search for getting all bugs with the given description in their description.
        comparision = self._server.search.comparison(const.SearchFields.BUG_DESCRIPTION, const.Operations.CONTAINS)
        comparision.add(description)

        coreCollection.add(comparision)
        rootCollection.add(coreCollection)
        # Add the new collection to an actual search
        search = self._server.bug.search(root=rootCollection)
        if returnSearch:
            return search
        return search.yieldBugs()

    def getBySummary(self, projects, summary, collection=None, returnSearch=False, closed=False):
        """Gets bugs by their summary.

        This method is intended to be used for finding duplicated bugs or to check if a bug with the same
        summary already exists.

        Args:
            projects (list): list of projects to look for bugs in.
            description (str): The description to look for in the summary.
            collection (rsg_clients.bugstar.entities.search.Collection): Additional search parameters to include in the search.
            returnSearch (bool): return the search object instead of the results of the search.
            closed (bool): Check for closed bugs if True else filter closed bugs
        """
        # Set which projects we are looking for the bugs in.
        rootCollection = self._server.search.collection(const.Operations.AND)
        projectComparison = self._server.search.comparison(const.SearchFields.PROJECT, const.Operations.IN)
        for project in projects:
            projectComparison.add(project)
        rootCollection.add(projectComparison)

        # create core collection and operation
        coreCollection = self._server.search.collection(const.Operations.AND)

        # add any user defined collection
        if collection:
            coreCollection.add(collection)

        # remove closed state bugs if not needed
        if not closed:
            statusComparison = self._server.search.comparison(const.SearchFields.STATUS, const.Operations.IN)
            statusComparison.add(const.SearchValues.NOT_CLOSED_STATES, datatype=const.ValueTypes.PARAMETER)
            coreCollection.add(statusComparison)

        # simple search for getting all bugs with the given summary.
        summaryComparision = self._server.search.comparison(const.SearchFields.SUMMARY, const.Operations.CONTAINS)
        summaryComparision.add(summary)

        coreCollection.add(summaryComparision)
        rootCollection.add(coreCollection)
        # Add the new collection to an actual search
        search = self._server.bug.search(root=rootCollection)
        if returnSearch:
            return search
        return search.yieldBugs()

    def buildFromTemplate(self, path):
        """Builds a bug object based on a given bug template.

        Args:
            path (str): path to a bug template

        Returns:
            Bug
        """
        raise NotImplementedError
