from xml.etree import cElementTree as xml
from rsg_clients import const as clientsConst
from rsg_clients.bugstar import const
from rsg_clients.bugstar.payloads import basePayload


class GameMap(basePayload.BasePayload):
    """Bugstar Payload for gameMaps of the game."""

    # The GameMap endpoint errors out if you return the data as xml
    DEFAULT_GET_ACCEPT_TYPE = clientsConst.ContentType.JSON_UTF8
    IS_READONLY = False
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to gameMap instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        GameMaps the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element("GameMap")

        for key, value in self._fields.items():
            if value is None:
                continue

            valueElement = xml.Element(key)
            valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Args:
            dictionary (dict): data to parse

        Returns:
            xml.etree.cElementTree.Element
        """

        self._fields.update(dictionary)

    @classmethod
    def _getData(cls, server, index, returnRaw=False, fields=None, isV2=True, command=None, acceptType=None):
        return super(GameMap, cls)._getData(
            server,
            index,
            returnRaw=returnRaw,
            fields=fields,
            isV2=isV2,
            command=command,
            acceptType=acceptType or clientsConst.ContentType.JSON_UTF8,
        )

    def toString(self, isV2=True):
        """
        Overrides inherited method

        Converts the xml element that represents this data to the string

        Args:
            isV2 (bool): data from a V2 query

        Returns:
            string
        """
        return super(GameMap, self).toString(isV2=isV2)

    @property
    def name(self):
        """ Name is readable gameMap property"""
        return self._getattr(const.Fields.NAME)


class Builder(basePayload.BaseBuilder):
    """Intermediate class for gameMaping instances of the GameMap class"""

    @classmethod
    def cls(cls):
        """Class for the object the builder creates.

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return GameMap
