"""Exception classes for Bugstar related errors, mostly for error codes returned by the REST API."""
import re
from rsg_clients.bugstar import const
from rsg_clients import exceptions
from rsg_core_py.decorators import memoize
from rsg_core_py.exceptions import types


class BugstarException(exceptions.RestException):
    """Base Bugstar Exception."""

    @memoize.memoize
    @classmethod
    def getByErrorCode(cls, errorCode):
        """Get an exception by its error code.

        Args:
            errorCode (int): error code

        Returns:
            rsg_clients.bugstar.BugstarException
        """
        if errorCode is None:
            return

        for subclass in cls.__subclasses__():
            if subclass.ERROR_CODE == errorCode:
                return subclass


class InvalidServer(BugstarException):
    """Invalid Project Id provided to Bugstar."""

    def __init__(self, message=None):
        """Overrides inherited method.

        Args:
            message (str, optional): error message to set for the error.
        """
        self.message = message or "A server has not been set for this connection"
        types.RockstarError.__init__(self, self.message)


class InvalidProject(BugstarException):
    """Invalid Project Id provided to Bugstar."""

    def __init__(self, message=None):
        self.message = message or "A Project has not been set for this connection"
        types.RockstarError.__init__(self, self.message)


class InvalidBug(BugstarException):
    """Invalid Bug Id provided to Bugstar."""

    def __init__(self, message=None):
        """Overrides inherited method.

        Args:
            message (str, optional): error message to set for the error.
        """
        self.message = message or "A Bug has not been set for this connection"
        types.RockstarError.__init__(self, self.message)


class MissingValue(BugstarException):
    """Required data to include in a payload is missing."""

    def __init__(self, message=None):
        """Overrides inherited method.

        Args:
            message (str, optional): error message to set for the error.
        """
        self.message = message or "Payload is missing required information"
        types.RockstarError.__init__(self, self.message)


class BadRequest(BugstarException, exceptions.ServerError):
    """Error from status code 400."""

    ERROR_CODE = 400
    DEFAULT_MESSAGE = "The request sent to the service is unreadable"


class InvalidType(BadRequest):
    """Sub Error from status code 400 that represents if data from a specific type could not be found."""

    DEFAULT_MESSAGE = "The supplied id does not represent an object of the requested type, or could not be found."


class PermissionDenied(BugstarException, exceptions.ServerError):
    """Error from status code 401 and a sub error of 403 & 500,."""

    ERROR_CODE = 401
    DEFAULT_MESSAGE = "The current user lacks the permission role to access this command"
    REGEX = re.compile("Permission (denied|restricted)")


class NotAuthenticated(PermissionDenied, types.AuthenticationError):
    """Invalid Project Id provided to Bugstar."""

    REGEX = re.compile(
        "Full authentication is required to access this resource|Access Token Expired|Invalid access token", re.I
    )
    DEFAULT_MESSAGE = "The current user is not authenticated to access the current server"


class InvalidId(BugstarException, exceptions.ServerError):
    """Error from status code 403."""

    ERROR_CODE = 403


class InvalidUriFormat(BugstarException, exceptions.ServerError):
    """Error from status code 404."""

    ERROR_CODE = 404
    DEFAULT_MESSAGE = "URL uses an invalid URI format"


class FileNotFoundOnServer(BugstarException, exceptions.ServerError):
    """Error from status code 410."""

    ERROR_CODE = 410
    DEFAULT_MESSAGE = "File not found on the server"


class ProjectChanged(InvalidUriFormat):
    """Sub error from status code 404 when the wrong project is used to query bug information.

    This is usually caused by a cached bug trying to query the server after its has been moved to a new project.
    """

    GROUP = "projectId"
    REGEX = re.compile("Invalid project ID specified for bug [0-9]+. Current project is (?P<projectId>[0-9]+)")
    URL_REGEX = re.compile("Projects/(?P<projectId>[0-9]+)")

    def __init__(self, message=None, url=None, includeErrorCode=True):
        """Initializer.

        Args:
            message (str, optional): the error message
            url (str, optional): the url that raised the error
            includeErrorCode (bool, optional): include the error in the error message
        """
        super(ProjectChanged, self).__init__(message, url, includeErrorCode)
        regex = self.REGEX.search(message)
        urlRegex = self.URL_REGEX.search(url)
        self.currentId = int(regex.group(self.GROUP))
        self.previousId = int(urlRegex.group(self.GROUP))


class UnsupportedHttpMethod(BugstarException, exceptions.ServerError):
    """Error from status code 405."""

    ERROR_CODE = 405
    DEFAULT_MESSAGE = "The specified HTTP method is not allowed for the requested resource"


class UnsupportedMedia(BugstarException, exceptions.ServerError):
    """Error from status code 415."""

    ERROR_CODE = 415
    DEFAULT_MESSAGE = "Post command contains unsupported media"


class TooManyRequests(BugstarException, exceptions.ServerError):
    """Error from status code 429."""

    ERROR_CODE = 429
    DEFAULT_MESSAGE = "Too many requests made against the server within small period of time."


class BugstarServerError(BugstarException, exceptions.ServerError):
    """Error from status code 500."""

    ERROR_CODE = 500
    DEFAULT_MESSAGE = "Unhandled Error by the Bugstar Team"


class ReadOnlyError(BugstarException, exceptions.ClientError):
    """Attempting to modify information on a Read Only class."""


class InvalidStatus(BugstarException, exceptions.ClientError):
    """Error when trying to switch to a status a bug can't switch to."""


class InvalidSearchSize(BadRequest):
    """Attempting to access more search pages then are available."""

    DEFAULT_MESSAGE = "Invalid page number for chosen size"


class ServerUnavailable(BugstarException, exceptions.ServerUnavailable):
    """Server is unavailable and can't be reached."""

    REGEX = re.compile(
        "(The CacheManager has been shut down)|(Session/EntityManager is closed)|"
        "(Can not read response from server)|(BeanCreationNotAllowedException)"
    )

    def __init__(self, server, message=None, url=None, includeErrorCode=None, connection=None):
        """Overrides inherited method.

        Args:
            server (rsg_clients.bugstar.servers.BaseServer): server that raised
            message (str, optional): error message to set for the error.
        """
        exceptions.ServerUnavailable.__init__(
            self,
            client=const.AppInfo.NAME,
            server=server,
            message=message,
            url=url,
            includeErrorCode=includeErrorCode,
            connection=connection,
        )


class FolderCreationError(BugstarException, exceptions.FolderCreationError):
    """ System errored out during the creation of the folder """

    def __init__(self, path, message=None):
        exceptions.FolderCreationError.__init__(self, path, message=message)
        BugstarException.__init__(self, self.message)


class RollbackOnlyError(BugstarServerError):
    """ The transaction has been marked as rollback only by the bugstar team."""

    REGEX = re.compile("Transaction silently rolled back because it has been marked as rollback-only")
