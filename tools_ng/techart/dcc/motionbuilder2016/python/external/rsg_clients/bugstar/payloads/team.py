from xml.etree import cElementTree as xml

from rsg_core import conversions
from rsg_clients.bugstar import const
from rsg_clients.bugstar.payloads import basePayload, actor


class Team(actor.Actor):
    """
    Class that represents a team on Bugstar

    Note:
        self._fields[const.Fields.ID] = index
        self._fields[const.Fields.NAME] = None
        self._fields[const.Fields.VERSION] = None
        self._fields[const.Fields.ACTIVE] = None
        self._fields[const.Fields.ACTOR_TYPE] = None
        self._fields[const.Fields.TEAM_TYPE] = None
        self._fields[const.Fields.DESCRIPTION] = None
        self._fields[const.Fields.CREATED_BY] = None
        self._fields[const.Fields.CREATED_AT] = None
        self._fields[const.Fields.MODIFIED_BY] = None
        self._fields[const.Fields.MODIFIED_AT] = None
        self._fields[const.Fields.OWNER] = None
        self._fields[const.Fields.GLOBAL_DEPARTMENT] = None
        self._fields[const.Fields.DEVELOPER] = None
        self._fields[const.Fields.REVIEWER] = None
        self._fields[const.Fields.TESTER] = None
    """

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []
    _COMMAND = const.Endpoints.TEAMS
    TYPE = const.Endpoints.TEAMS

    def __new__(cls, bugstar, index=None, *args, **kwargs):
        """
        Overrides built-in method

        Stores & returns the instance of the class if an argument is provided, otherwise creates a new instance

        Args:
            bugstar (rsg_clients.bugstar.server.BaseServer): the local server to set for this object
            *args (list): additional parameters accepted by the inherited class
            **kwargs (dict): additional keyword arguments accepted by the inherited class

        Keyword Args:
            index (int): the id of the bug on bugstar

        Returns:
            object
        """
        return cls._getInstance(bugstar, index)

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.bugstar.basePayload.BaseBuilder
        """
        return Builder

    def _xmlElementV2(self):
        """
        Overrides inherited method

        Builds the xml element when the dictionary data obtained from bugstar came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        element = xml.Element("Team")
        for key, value in self._fields.iteritems():
            if value is None:
                continue
            valueElement = xml.Element(key)

            if isinstance(value, (self.server.user._CLASS, Team)):
                valueElement.text = str(value.id)
            else:
                valueElement.text = str(value)

            element.append(valueElement)
        return element

    def _parseV2(self, dictionary):
        """
        Overrides inherited method

        Parses the dictionary data obtained from bugstar that came from a V2 query

        Returns:
            xml.etree.cElementTree.Element
        """
        dictionary[const.Fields.NAME] = conversions.convertToAscii(dictionary.get(const.Fields.NAME, ""))
        self._fields.update(dictionary)

    @property
    def name(self):
        """ name is readable bugstar property """
        return self._getattr(const.Fields.NAME)

    @property
    def developer(self):
        """ developer is readable bugstar property """
        return self._getattr(const.Fields.DEVELOPER)

    @property
    def reviewer(self):
        """ reviewer is readable bugstar property """
        return self._getattr(const.Fields.REVIEWER)

    @property
    def modifiedAt(self):
        """ modifiedAt is readable bugstar property """
        return self._getattr(const.Fields.MODIFIED_AT)

    @property
    def actorType(self):
        """ actorType is readable bugstar property """
        return self._getattr(const.Fields.ACTOR_TYPE)

    @property
    def description(self):
        """ description is readable bugstar property """
        return self._getattr(const.Fields.DESCRIPTION)

    @property
    def owner(self):
        """ owner is readable bugstar property """
        value = self._getattr(const.Fields.OWNER)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.user(value)
        return value

    @property
    def createdBy(self):
        """ createdBy is readable bugstar property """
        value = self._getattr(const.Fields.CREATED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.user(value)
        return value

    @property
    def modifiedBy(self):
        """ modifiedBy is readable bugstar property """
        value = self._getattr(const.Fields.MODIFIED_BY)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.user(value)
        return value

    @property
    def active(self):
        """ active is readable bugstar property """
        return self._getattr(const.Fields.ACTIVE)

    @property
    def globalDepartment(self):
        """ globalDepartment is readable bugstar property """
        value = self._getattr(const.Fields.GLOBAL_DEPARTMENT)
        if isinstance(value, int):
            # Get data from Bugstar
            value = self.server.globalDepartment(value)
        return value

    @property
    def version(self):
        """ version is readable bugstar property """
        return self._getattr(const.Fields.VERSION)

    @property
    def teamType(self):
        """ teamType is readable bugstar property """
        return self._getattr(const.Fields.TEAM_TYPE)

    @property
    def createdAt(self):
        """ createdAt is readable bugstar property """
        return self._getattr(const.Fields.CREATED_AT)


class Builder(basePayload.BaseBuilder):
    """
    Intermediate class for building instances of the Team class
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Team
