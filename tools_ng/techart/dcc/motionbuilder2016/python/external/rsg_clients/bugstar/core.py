"""Bugstar wrapper around the rest interface.

Reference:
https://hub.rockstargames.com/display/RSGBGSDOCS/Bugstar+REST+API+Documentation
https://rest-dev.bugstar.rockstargames.com:8443/BugstarRestService-1.0/swagger-ui/index.html

Example Rest Queries:

#TODO: Provide v2 examples
"""
from concurrent import futures
import urllib
import json
import xmltodict

import requests

from rsg_core import accounts, conversions
from rsg_clients.internals import baseConnection
from rsg_clients import const as clientsConst
from rsg_clients.decorators import connection
from rsg_clients.contextManagers import futures as clientFutures
from rsg_clients.bugstar import const, exceptions, factory
from rsg_clients.bugstar.payloads import basePayload

# TODO: Add logging back at a later date


class Bugstar(baseConnection.BaseConnection):
    """Light wrapper for the bugstar rest service interface."""

    # TODO: Abstract further to have a generic rest client api wrapper
    DEFAULT_POST_CONTENT_TYPE = clientsConst.ContentType.XML_UTF8

    def __init__(self, server):
        """Initializer.

        Args:
            server (str): name of the server we are connecting to.
        """
        super(Bugstar, self).__init__(server)
        self._account = accounts.CurrentUser()  # Set the current user as the default account
        self._delegateUser = None
        self._sslCertificate = None  # Bugstar doesn't use the Internal Take2 Cert
        self._type = const.Servers.getType(server)

    def _search(self, url, payload, futures=False):
        """Performs search and catches an internal error.

        Args:
            url (str): url to run post command on
            payload (dict): payload to provide to the rest query
            futures (bool, optional): perform the rest request concurrently and return the futures object to track the completion of the request

        Returns:
            dict or None or concurrent.Futures
        """
        try:
            return self.post(url, payload=payload, futures=futures)
        except exceptions.InvalidSearchSize:
            # This appears to be caused by a change in the content of the search, however replicating the issue
            # has proven difficult
            return

    def _getBuilder(self, payloadType):
        """Gets the builder for the given payload.

        Args:
            payloadType (str): the type of the entity

        Returns:
            rockstar.shotgun.entities.BaseEntity.BaseBuilder
        """
        builder = self._builders.get(payloadType, None)
        if not builder:
            cls = factory.resolvePayloadClass(payloadType)
            builderCls = cls.builder()
            builder = builderCls(self)
            self._builders[payloadType] = builder
        return builder

    @property
    def client(self):
        """The name of the server."""
        return clientsConst.Clients.BUGSTAR

    @property
    def delegate(self):
        """Username of the user to delegate behavior as.

        Return:
            str
        """
        return self._delegate

    @delegate.setter
    def delegate(self, value):
        """Sets the user to delegate behavior as.

        Args:
            value (str or rsg_clients.bugstar.payloads.user.User): username or user object of the user to delegate actions as.
        """
        if not self.account:
            raise exceptions.NotAuthenticated("An account must be set first before setting a delegate")

        if value is not None:
            # Verify that this account can accept delegate users
            accountUser = self.user.getByName(self.account.name)
            if not accountUser:
                raise LookupError("Could not find bugstar user account for {}".format(self.account.name))
            elif not accountUser.isSuperAdmin:
                raise exceptions.PermissionDenied(
                    "{} does not have permissions to perform user delegation.".format(self.account.name), includeErrorCode=False
                )

        previousDelegate = self._delegate
        self._delegate = None  # Set it to None to avoid any recursion errors
        if isinstance(value, basestring):
            self._delegateUser = self.user.getByName(value)
            self._delegate = value
        elif isinstance(value, self.user.cls()):
            self._delegateUser = value
            self._delegate = value.name
        elif value is None:
            self._delegateUser = None
            self._delegate = None
        else:
            self._delegate = previousDelegate
            raise ValueError("{} is not a valid bugstar user".format(value))

    @property
    def delegateUser(self):
        """The bugstar user account for the delegate that was set.

        Note:
            The account used to login to Bugstar must have the required privileges to use this feature.

        Returns:
            rsg_clients.bugstar.contents.User
        """
        return self._delegateUser

    @property
    def url(self):
        """Convenience method for getting the server url."""
        return const.Servers.getBaseUrl(self._server)

    @property
    def restUrl(self):
        """Convenience method for getting the server url with the rest endpoint."""
        return const.Servers.getRestUrl(self._server)

    def buildUrl(self, sections, fields=None, additionalArgs={}):
        """Builds the url to request/send information through the rest api.

        Args:
            sections (list): parts of the url

            fields (dict, optional): fields to include in the url
            additionalArgs (object, optional): to be determined as some queries require non-standardized information

        Returns:
            str
        """
        url = "/".join([str(section) for section in sections])
        if fields:
            fieldStringList = [
                "{}={}".format(key, urllib.quote(str(value), safe="")) for key, value in fields.iteritems()
            ]
            fieldsString = "&".join(fieldStringList)
            url = "{}?{}".format(url, fieldsString)

        return url

    def buildLink(self, index):
        """Overwrites the inherited method.

        Args:
            index (int): id of the bug to build link to

        Notes:
            This method only supports adding links for bugs
        """
        return "http://bs/@bug/{}".format(index)

    def authenticate(self, username, password):
        """Authenticates the set account against Bugstar for access.

        Args:
            username (str): name of the user to access bugstar as
            password (str): password of the user

        Returns:
            bool
        """
        username = username or self.account.idapAddress
        try:
            if not self._authenticated:
                with self.session as session:
                    token = conversions.base64Encode("{}:{}".format(username, conversions.base64Decode(password)))
                    session.headers["authorization"] = "Basic {}".format(token)
                    # TODO MIGRATE: Weird Bug in Py2 where the decorators appear to be eating up the self instance variable
                    response = self._get(self, "{}/{}/{}".format(self.url,const.Endpoints.TOKEN,const.Endpoints.BASIC), returnRaw=True)
                    session.headers["authorization"] = "Bearer {}".format(response.text)

            # We are attempting to query an invalid url so we get consistent errors based on the current authentication
            # This is also faster then querying actual data
            # TODO MIGRATE: Weird Bug in Py2 where the decorators appear to be eating up the self instance variable
            self._get(self, "{}/{}/BadUrl".format(self.restUrl, const.Endpoints.V2), contentType=clientsConst.ContentType.JSON_UTF8)

        except exceptions.ServerUnavailable:
            raise

        except exceptions.BugstarException as exception:
            # We are expecting a 404 error as this is a badly built url (but still works for authenticating)
            # Getting any other error means you didn't login, including a 200 status code
            if exception.ERROR_CODE != 404:
                self._authenticated = False
                raise exceptions.NotAuthenticated(
                    "Could not authenticate user {} to access the {} server\n"
                    "Verify your credentials and try again.\n"
                    "If the problem persists, contact techart and the {} team for "
                    "further assistance.".format(username, self.server, self.client)
                )
        self._authenticated = True
        return True

    def login(self, raiseErrors=False):
        """Login to bugstar.

        Returns:
            bool
        """
        return super(Bugstar, self).login(username=self.account.idapAddress, raiseErrors=raiseErrors)

    def logout(self):
        """Clears the session from memory."""
        super(Bugstar, self).logout()
        self.delegate = None

    def search_(self, endpoint, payload, fields=None, size=None, page=None, iterative=True, delegate=None):
        """Performs a search on bugstar based on the xml information provided to the payload.

        This returns the results as a dictionary.

        Args:
            endpoint (str): The endpoint to use for determining the data returned from the search.
                                Currently only supports the the Bugs and Users command, and it defaults to Bugs.
            payload (str): xml information to base the search on

            fields (dictionary, optional): optional fields that can be added to the rest url.
            size (int, optional): The amount of results to return. Default is 100 and max value is 500.
            page (int, optional): The page of results to return.
            iterative (bool, optional): iterate through all the pages returned by a search. By default it is True.
            delegate (rsg_clients.bugstar.contents.User, optional): User to perform the search as

        Yields:
            generator(dict)
        """
        yieldStats = True
        fields = fields or {}
        size = fields.setdefault("size", size or 100)
        fields.setdefault("page", page or 1)
        if size > 500:
            fields["size"] = 500

        url = [
            self.restUrl,
            const.Endpoints.V2,
            endpoint,
            const.Endpoints.SEARCHES,
            const.Endpoints.INVOKE,
        ]
        if delegate is not None:
            fields[const.QueryFields.AS_USER] = delegate.id

        # The first search must be performed to grab a list of pages that the search returns
        searchUrl = self.buildUrl(url, fields=fields)
        dictionary = self._search(searchUrl, payload)
        if dictionary:
            # Get stats from the search
            results = dictionary.get("Results", {})
            pageInfo = results.get("_page")
            totalCount = int(pageInfo.get("totalCount", 0))
            totalPages = int(pageInfo.get("totalPages"))
            currentPage = int(pageInfo.get("page"))
            currentResults = results.get("Result", None) or []
            if not isinstance(currentResults, list):
                currentResults = [currentResults]

            # Yield the stats of the search as the first results
            if yieldStats:
                stats = {"count": totalCount}
                stats.update(results.get("Statistics", {}))
                yield stats

            # yield the first result
            yield currentResults
            if iterative:
                with clientFutures.Futures(connection=self):
                    # TODO: Reevaluate this implementation and make a more generic solution for this
                    # We are going to leave one free worker/thread so we don't lock up the requests session from being
                    # able to process non-concurrent requests
                    queues = []
                    futuresRequests = []
                    maxCount = self.futures.executor._max_workers - 1
                    maxRange = totalPages if totalPages < maxCount else maxCount
                    for page in range(currentPage, maxRange):
                        page += 1  # Bump the page number by one so it reflects the correct page we need
                        if page > totalPages:
                            break # Safety check to make sure we don't create more searches then we need.
                        fields["page"] = page
                        searchUrl = self.buildUrl(url, fields=fields)
                        futureRequest = self._search(searchUrl, payload, futures=True)
                        futureRequest.page = page  # This is for tracking which request goes with which page
                        futuresRequests.append(futureRequest)
                    queues.append(futuresRequests)

                    currentPage += 1
                    completedPages = {}
                    # process the future events in chunks as editing the list in place while using the as_completed
                    # generator causes weirdness
                    for currentQueue in queues:
                        nextQueue = []
                        for future in futures.as_completed(currentQueue):
                            # TODO: Debug why hooks aren't working
                            completedPages[future.page] = connection.resolve.response(future.result(), self, url)
                            while currentPage in completedPages:
                                currentResults = completedPages[currentPage].get("Results", {}).get("Result", [])
                                if not isinstance(currentResults, list):
                                    currentResults = [currentResults]
                                yield currentResults

                                # Add requests for pages that still need to be processed
                                offsetPage = currentPage + len(futuresRequests)
                                if offsetPage < totalPages:
                                    fields["page"] = offsetPage
                                    searchUrl = self.buildUrl(url, fields=fields)
                                    futureRequest = self._search(searchUrl, payload, futures=True)
                                    futureRequest.page = offsetPage  # This is for tracking which request goes with which page
                                    nextQueue.append(futureRequest)
                                currentPage += 1
                        if nextQueue:
                            queues.append(nextQueue)

    def clearCache(self):
        """Clears out the local cache of bugstar related objects that were spawned from this connection."""
        for subclass in basePayload.BasePayload.__subclasses__():
            subclass._clearCache()

    def cleanLeastAccessedCache(self, isDataStale=False, isCacheStale=True, cacheLimit=None):
        """Cleans out the least accessed cache and the current contents of cached objects.

        Args:
            isDataStale (bool, optional): only clear data that is stale.
            isCacheStale (bool, optional): only removes objects from the cache only if the haven't been recently accessed.
            cacheLimit (int, optional): the amount of items the cache is allowed to hold at a given time.
        """
        for subclass in basePayload.BasePayload.__subclasses__():
            subclass._cleanDataCache(isDataStale=isDataStale)
            subclass._cleanLeastAccessedCache(isCacheStale=isCacheStale, cacheLimit=cacheLimit)

    def convertResponse(self, response):
        """Converts the response from the rest call to json.

        Args:
            response (requests.Response): response object returned by the requests module

        Returns:
            dict
        """
        try:
            return response.json()
        except json.decoder.JSONDecodeError:
            raise
        except ValueError:
            return json.loads(json.dumps(xmltodict.parse(response.text)))

    def raiseAuthenticationError(self, url, response):
        """Raises an authentication error if the response returns an html page with said error.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server

        Raises:
            rsg_clients.bugstar.exceptions.NotAuthenticated
        """
        if ("<!DOCTYPE html>" in response.text and "Login" in response.text) or exceptions.NotAuthenticated.REGEX.search(response.text):
            username = self.account.name
            self._authenticated = False
            raise exceptions.NotAuthenticated(
                "{} has not authenticated to access the {} server".format(username, self._server), url=url
            )

    def raiseException(self, url, response, data=None):
        """Raises an exception if the response returns a non-200 status code.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server
            data (dict): data extrapolated from the response as a dict

        Raises:
            rsg_clients.bugstar.exceptions.BugstarException
        """
        exceptionClass = exceptions.BugstarException.getByErrorCode(response.status_code)
        if exceptionClass is not None:
            # Get the correct error message, based on the response it may require different keys
            errorDict = data.get("ResponseHeader", data).get("Error", data)
            errorMessage = errorDict.get("details", errorDict.get("#text", errorDict.get("error_description", errorDict.get("error", response.text))))
            if isinstance(errorMessage, list):
                errorMessage = errorMessage[0]
            # Handle getting error from this nested xml that is returned for some 500 errors
            if not errorMessage and "linked-hash-map" in data:
                errorList = data.get("linked-hash-map", {}).get("entry")
                for errorDict in errorList:
                    if errorDict.get("string", None) and errorDict["string"][0] == "message":
                        errorMessage = errorDict["string"][-1]
                        break

            if response.status_code == exceptions.BadRequest.ERROR_CODE:  # This the generic 400 error code
                if exceptions.InvalidType.DEFAULT_MESSAGE in errorMessage:
                    raise exceptions.InvalidType(errorMessage, url=url, errorCode=response.status_code)

                elif exceptions.InvalidSearchSize.DEFAULT_MESSAGE in errorMessage:
                    raise exceptions.InvalidSearchSize(errorMessage, url=url)

            elif response.status_code == exceptions.PermissionDenied.ERROR_CODE:
                if exceptions.NotAuthenticated.REGEX.search(errorMessage):
                    raise exceptions.NotAuthenticated(errorMessage, url=url, errorCode=response.status_code)
                raise exceptions.PermissionDenied(errorMessage, url=url, errorCode=response.status_code)

            if response.status_code == exceptions.InvalidId.ERROR_CODE:  # This is the generic 403 error code
                if exceptions.PermissionDenied.REGEX.search(errorMessage):
                    raise exceptions.PermissionDenied(errorMessage, url=url, errorCode=response.status_code)

            elif response.status_code == exceptions.InvalidUriFormat.ERROR_CODE:  # This the generic 404 error code
                if exceptions.ProjectChanged.REGEX.search(errorMessage):
                    raise exceptions.ProjectChanged(errorMessage, url=url)

            elif response.status_code == exceptions.TooManyRequests.ERROR_CODE:
                raise exceptions.TooManyRequests(errorMessage, url=url)

            elif response.status_code == exceptions.BugstarServerError.ERROR_CODE:  # This the generic 500 error code
                if exceptions.ServerUnavailable.REGEX.search(errorMessage):
                    raise exceptions.ServerUnavailable(server=self.type, message=errorMessage, url=url, connection=self)
                elif exceptions.PermissionDenied.REGEX.search(errorMessage):
                    raise exceptions.PermissionDenied(errorMessage, url=url, errorCode=response.status_code)
                elif exceptions.RollbackOnlyError.REGEX.search(errorMessage):
                    raise exceptions.RollbackOnlyError(errorMessage, url=url, errorCode=response.status_code)

            raise exceptionClass(errorMessage, url=url)

    @property
    def actor(self):
        """Base bugstar type for users, teams, departments, and global departments.

        Returns:
            rsg_clients.bugstar.payloads.basePayload.BasePayload
        """
        return self._getBuilder(const.Endpoints.ACTORS)

    @property
    def attachment(self):
        """Attachment Builder."""
        return self._getBuilder(const.Endpoints.ATTACHMENTS)

    @property
    def build(self):
        """Build Builder."""
        return self._getBuilder(const.Endpoints.BUILDS)

    @property
    def bug(self):
        """Bug Builder."""
        return self._getBuilder(const.Endpoints.BUGS)

    @property
    def component(self):
        """Component Builder."""
        return self._getBuilder(const.Endpoints.COMPONENTS)

    @property
    def deadline(self):
        """Deadline Builder."""
        return self._getBuilder(const.Endpoints.DEADLINES)

    @property
    def department(self):
        """Department Builder."""
        return self._getBuilder(const.Endpoints.DEPARTMENTS)

    @property
    def gameMap(self):
        """Game Map Builder."""
        return self._getBuilder(const.Endpoints.GAME_MAPS)

    @property
    def grid(self):
        """Grid Builder."""
        return self._getBuilder(const.Endpoints.GRIDS)

    @property
    def goal(self):
        """Goal Builder."""
        return self._getBuilder(const.Endpoints.GOALS)

    @property
    def globalDepartment(self):
        """Global Department Builder."""
        return self._getBuilder(const.Endpoints.GLOBAL_DEPARTMENTS)

    @property
    def mission(self):
        """Mission Builder."""
        return self._getBuilder(const.Endpoints.MISSIONS)

    @property
    def platform(self):
        """Platform Builder."""
        return self._getBuilder(const.Endpoints.PLATFORMS)

    @property
    def project(self):
        """Project Builder."""
        return self._getBuilder(const.Endpoints.PROJECTS)

    @property
    def search(self):
        """Search Builder."""
        return self._getBuilder(const.Endpoints.SEARCHES)

    @property
    def studio(self):
        """Studio Builder."""
        return self._getBuilder(const.Endpoints.STUDIOS)

    @property
    def team(self):
        """Team Builder."""
        return self._getBuilder(const.Endpoints.TEAMS)

    @property
    def user(self):
        """User Builder."""
        return self._getBuilder(const.Endpoints.USERS)

    @property
    def tag(self):
        """Tag Builder."""
        return self._getBuilder(const.Endpoints.TAGS)
