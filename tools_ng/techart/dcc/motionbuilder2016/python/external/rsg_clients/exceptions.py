import re
import datetime

from rsg_core_py.exceptions import types
from rsg_core_py.decorators import memoize


class ClientError(types.RockstarError):
    """
    Refers to an error encountered locally in the client.
    This intended to be used for errors validating business logic on data available in the local instance.
    """


class ServerError(types.RockstarError):
    """
    Refers to an error encountered on the server or while connecting to a server
    """


class FolderCreationError(ClientError):
    """ System errored out during the creation of the folder """

    def __init__(self, path, message=None):
        self.message = "Error during the creation of the folder {}".format(path)
        if message:
            self.message = "{}\n\t{}".format(self.message, message)
        super(FolderCreationError, self).__init__(self.message)


class RestException(types.RockstarError):
    """Base Confluence Exception."""

    ERROR_CODE = None
    DEFAULT_MESSAGE = None

    def __init__(self, message=None, url=None, includeErrorCode=True, errorCode=None):
        """Initializer.

        Args:
            message (str, optional): error message to display
            url (str, optional): url that caused this error
            includeErrorCode (bool, optional): include the error code in the error message
            errorCode (int, optional): alternate error code to the default error code.
                            The same errors can returned under different error codes from the ones we have assigned as
                            their default.

        """
        self.message = ""
        message = message or self.DEFAULT_MESSAGE
        errorCode = errorCode or self.ERROR_CODE

        if errorCode and includeErrorCode:
            self.message = "{} Status Code Returned".format(errorCode)
        if url is not None:
            self.message = "{}\n\tUrl: {}".format(self.message, url)
        if message is not None:
            self.message = "{}\n\t{}".format(self.message, message)

        types.RockstarError.__init__(self, self.message)

    @memoize.memoize
    @classmethod
    def getByErrorCode(cls, errorCode):
        """Get an exception by its error code.

        Args:
            errorCode (int): error code

        Returns:
            rsg_clients.confluence.ConfluenceException
        """
        if errorCode is None:
            return

        for subclass in cls.__subclasses__():
            if subclass.ERROR_CODE == errorCode:
                return subclass


class NotAuthenticated(RestException, types.AuthenticationError):
    """Could not authenticate against the server."""

    ERROR_CODE = 401
    HTML_REGEX = re.compile(r"<!doctype html>")
    DEFAULT_MESSAGE = "The current account is not authenticated to access the current server."


class ConnectionAborted(NotAuthenticated):
    """The remote host has disconnected it this account from the server."""

    ERROR_CODE = None
    HTML_REGEX = None
    DEFAULT_MESSAGE = "Connection aborted by remote host."


class ServerUnavailable(RestException, ServerError):
    """ Server is unavailable and can't be reached """

    def __init__(self, client, server, message=None, url=None, includeErrorCode=True, errorCode=None, connection=None):
        """Initializer

        Args:
            client (str): name of the client (bugstar, shotgun, jenkins, etc.)
            server (str): name of the server (production, pre-production, development,etc.)

            url (str, optional): url that caused this error
            includeErrorCode (bool, optional): include the error code in the error message
            errorCode (int, optional): alternate error code to the default error code.
                            The same errors can returned under different error codes from the ones we have assigned as
                            their default.
            connection (rsg_core_py.abstracts.AsbtractConnection, optional): the connection that raised this error.
        """
        self.client = client
        self.server = server
        self.time = datetime.datetime.utcnow()
        self.name = "{} {}".format(client, server)
        self.message = (
            "Attempt to connect to the {} server on {} UTC was unsuccesful.\n"
            "The server is unavailable and can't be reached"
        ).format(self.name, self.time.strftime("%b %d %Y %H:%M:%S"))
        self.connection = connection
        if message:
            self.message = "{}\nOriginal Error:\n\t{}".format(self.message, message)
        ServerError.__init__(self, self.message)
        RestException.__init__(
            self, message=self.message, url=url, includeErrorCode=includeErrorCode, errorCode=errorCode
        )


class JsonDecodeError(RestException, ClientError):
    """A more verbose Json Decode error"""

    def __init__(self, client, server, contentType, message=None, url=None, connection=None):
        self.client = client
        self.server = server
        self.time = datetime.datetime.utcnow()
        self.name = "{} {}".format(client, server)
        self.message = (
            "Could not convert response of {} format to json".format(contentType)
        )
        if message:
            self.message = "{}\nOriginal Error:\n\t{}".format(self.message, message)
        self.connection = connection
        ServerError.__init__(self, self.message)
        RestException.__init__(
            self, message=self.message, url=url, includeErrorCode=False
        )
