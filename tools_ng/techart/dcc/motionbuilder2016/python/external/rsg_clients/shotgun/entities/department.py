from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Department(baseEntity.BaseEntity):
    """
    Class that represents a Department entity
    """

    ENTITY_TYPE = const.Entities.DEPARTMENT
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.NAME,
        const.Fields.CODE,
        const.Fields.USERS,
        const.Fields.NOTES,
    )
    NAME_FIELD = const.Fields.NAME

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def code(self):
        return self._getattr(const.Fields.CODE)

    @code.setter
    def code(self, value):
        self._setattr(const.Fields.CODE, value)

    @property
    def users(self):
        return self._getmutable(const.Fields.USERS, mutableType=list)

    @users.setter
    def users(self, value):
        self._setattr(const.Fields.USERS, value, isMutable=True)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Department
