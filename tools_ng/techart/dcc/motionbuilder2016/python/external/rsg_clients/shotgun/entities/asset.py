from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class SubTypes(baseEntity.BaseEntitySubtypes):
    ANIMAL = const.SubTypes.ANIMAL
    CHARACTER = const.SubTypes.CHARACTER
    VEHICLE = const.SubTypes.VEHICLE
    POSE = const.SubTypes.POSE
    PROP = const.SubTypes.PROP
    DIALOGUE = const.SubTypes.DIALOGUE
    VFX = const.SubTypes.VFX


class CharacterSubTypes(baseEntity.BaseEntitySubtypes):
    GARMENT = const.SubTypes.GARMENT
    BOARDCONCEPT = const.SubTypes.BOARDCONCEPT
    BOARDPLAYER = const.SubTypes.BOARDPLAYER
    BOARDSHOPSTORE = const.SubTypes.BOARDSHOPSTORE
    BOARDSTORY = const.SubTypes.BOARDSTORY
    BOARDWORLD = const.SubTypes.BOARDWORLD
    PED = const.SubTypes.PED
    PEDUNIQUE = const.SubTypes.PEDUNIQUE
    PEDWORLD = const.SubTypes.PEDWORLD
    ROLEOUTFIT = const.SubTypes.ROLEOUTFIT
    SHOP = const.SubTypes.SHOP
    ITEM = const.SubTypes.ITEM


class GarmentTypes(baseEntity.BaseEntitySubtypes):
    ACCESSORIES = const.SubTypes.ACCESSORIES
    BAGS = const.SubTypes.BAGS
    BOTTOMS = const.SubTypes.BOTTOMS
    EYEWEAR = const.SubTypes.EYEWEAR
    FOOTWEAR = const.SubTypes.FOOTWEAR
    HAIRSTYLES = const.SubTypes.HAIRSTYLES
    HEAD = const.SubTypes.HEAD
    HEADWEAR = const.SubTypes.HEADWEAR
    NUDE = const.SubTypes.NUDE
    ONEPIECESUIT = const.SubTypes.ONEPIECESUIT
    PROXYPEDS = const.SubTypes.PROXYPEDS
    TOPS = const.SubTypes.TOPS


class GarmentFamily(baseEntity.BaseEntitySubtypes):
    DERIVATIVE = const.SubTypes.DERIVATIVE
    ARCHETYPE = const.SubTypes.ARCHETYPE


class GarmentSubTypes(baseEntity.BaseEntitySubtypes):
    ARMORBODY = const.SubTypes.ARMORBODY
    ARMORLEGS = const.SubTypes.ARMORLEGS
    BELTS = const.SubTypes.BELTS
    BELTSBUCKLES = const.SubTypes.BELTSBUCKLES
    BRACELETS = const.SubTypes.BRACELETS
    EARRINGS = const.SubTypes.EARRINGS
    GLOVES = const.SubTypes.GLOVES
    HEADPHONES = const.SubTypes.HEADPHONES
    NECKLACES = const.SubTypes.NECKLACES
    NECKWEAR = const.SubTypes.NECKWEAR
    RINGS = const.SubTypes.RINGS
    GILETS = const.SubTypes.GILETS
    WATCHES = const.SubTypes.WATCHES
    PANTS = const.SubTypes.PANTS
    SHORTS = const.SubTypes.SHORTS
    SKIRTS = const.SubTypes.SKIRTS
    UNDERWEAR = const.SubTypes.UNDERWEAR
    GOGGLES = const.SubTypes.GOGGLES
    SHADES = const.SubTypes.SHADES
    SPECS = const.SubTypes.SPECS
    BOOTS = const.SubTypes.BOOTS
    SHOES = const.SubTypes.SHOES
    SOCKS = const.SubTypes.SOCKS
    HAIR = const.SubTypes.HAIR
    BEARD = const.SubTypes.BEARD
    HATS = const.SubTypes.HATS
    HELMETS = const.SubTypes.HELMETS
    MASKS = const.SubTypes.MASKS
    WRAPS = const.SubTypes.WRAPS
    COSTUME = const.SubTypes.COSTUME
    FULLBODY = const.SubTypes.FULLBODY
    SWIMSUIT = const.SubTypes.SWIMSUIT
    DRESS = const.SubTypes.DRESS
    BRAS = const.SubTypes.BRAS
    COATS = const.SubTypes.COATS
    HOODIES = const.SubTypes.HOODIES
    JACKETS = const.SubTypes.JACKETS
    SHIRTS = const.SubTypes.SHIRTS
    SWEATSHIRTS = const.SubTypes.SWEATSHIRTS
    TANKTOPS = const.SubTypes.TANKTOPS
    TSHIRTS = const.SubTypes.TSHIRTS
    VESTS = const.SubTypes.VESTS
    BACKPACKS = const.SubTypes.BACKPACKS
    BAGSTRAPS = const.SubTypes.BAGSTRAPS
    DUFFLEBAGS = const.SubTypes.DUFFLEBAGS
    HANDBAGS = const.SubTypes.HANDBAGS
    PARACHUTES = const.SubTypes.PARACHUTES


class Gender(baseEntity.BaseEntitySubtypes):
    MALE = const.SubTypes.MALE
    FEMALE = const.SubTypes.FEMALE
    BOTH = const.SubTypes.BOTH


class Asset(baseEntity.BaseEntity):
    """
    Class that represents a Asset entity
    """

    ENTITY_TYPE = const.Entities.ASSET
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.NOTES,
        const.Fields.TAGS,
        const.Fields.BUG_NUMBER,
        const.Fields.BUG_URL,
        const.Fields.TASKS,
        const.Fields.DIALOGUE_ID,
        const.Fields.ASSET_TYPE,
        const.Fields.DESCRIPTION,
        const.Fields.CHARACTER_TYPE,
        const.Fields.GARMENT_TYPE,
        const.Fields.GARMENT_SUBTYPE,
        const.Fields.GARMENT_FAMILY,
        const.Fields.ENTITY_SUBTYPE,
        const.Fields.MAX_PATH,
        const.Fields.HUB_LINK,
        const.Fields.GENDER,
        const.Fields.THUMBNAIL,
        const.Fields.VERSIONS,
        const.Fields.PROJECT,
        const.Fields.FACIAL_RIG_TYPE,
        const.Fields.ANIMAL_TYPE,
        const.Fields.FRAME_RATE,
        const.Fields.PROP_TYPE,
        const.Fields.VAULT_ID,
        const.Fields.VAULT_PATH,
        const.Fields.PARENTS,
        const.Fields.DUE_DATE,
    )

    NAME_FIELD = const.Fields.CODE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def hubLink(self):
        """
        The hubLink of the entity

        Return:
            str
        """
        return self._getattr(const.Fields.HUB_LINK)

    @hubLink.setter
    def hubLink(self, value):
        """
        Sets the hubLink of the entity

        Arguments:
            value (str): hubLink for the entity
        """
        self._setattr(const.Fields.HUB_LINK, value)

    @property
    def maxPath(self):
        """
        The maxPath of the entity

        Return:
            str
        """
        return self._getattr(const.Fields.MAX_PATH)

    @maxPath.setter
    def maxPath(self, value):
        """
        Sets the maxPath of the entity

        Arguments:
            value (str): maxPath for the entity
        """
        self._setattr(const.Fields.MAX_PATH, value)

    @property
    def parents(self):
        """
        The parents of the entity

        Return:
            str
        """
        return self._getmutable(const.Fields.PARENTS, mutableType=list)

    @parents.setter
    def parents(self, value):
        """
        Sets the parents of the entity

        Arguments:
            value (str): parents for the entity
        """
        self._setattr(const.Fields.PARENTS, value, isMutable=True)

    @property
    def gender(self):
        """
        The gender of the entity

        Return:
            str
        """
        return self._getattr(const.Fields.GENDER)

    @gender.setter
    def gender(self, value):
        """
        Sets the gender of the entity

        Arguments:
            value (str): gender for the entity
        """
        self._setattr(const.Fields.GENDER, value)

    @property
    def characterType(self):
        """
        The characterType of the entity

        Return:
            str
        """
        return self._getattr(const.Fields.CHARACTER_TYPE)

    @characterType.setter
    def characterType(self, value):
        """
        Sets the characterType of the entity

        Arguments:
            value (str): characterType for the entity
        """
        self._setattr(const.Fields.CHARACTER_TYPE, value)

    @property
    def garmentType(self):
        """
        The garmentType of the entity

        Return:
            str
        """
        return self._getattr(const.Fields.GARMENT_TYPE)

    @garmentType.setter
    def garmentType(self, value):
        """
        Sets the garmentType of the entity

        Arguments:
            value (str): garmentType for the entity
        """
        self._setattr(const.Fields.GARMENT_TYPE, value)

    @property
    def garmentSubType(self):
        """
        The garmentSubType of the entity

        Return:
            str
        """
        return self._getattr(const.Fields.GARMENT_SUBTYPE)

    @garmentSubType.setter
    def garmentSubType(self, value):
        """
        Sets the garmentSubType of the entity

        Arguments:
            value (str): garmentSubType for the entity
        """
        self._setattr(const.Fields.GARMENT_SUBTYPE, value)

    @property
    def garmentFamily(self):
        """
        The garmentFamily of the entity

        Return:
            str
        """
        return self._getattr(const.Fields.GARMENT_FAMILY)

    @garmentFamily.setter
    def garmentFamily(self, value):
        """
        Sets the garmentFamily of the entity

        Arguments:
            value (str): garmentFamily for the entity
        """
        self._setattr(const.Fields.GARMENT_FAMILY, value)

    @property
    def name(self):
        """
        The name of the entity

        Return:
            str
        """
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        """
        Sets the name of the entity

        Arguments:
            value (str): name for the entity
        """
        self._setattr(const.Fields.CODE, value)

    @property
    def tags(self):
        """
        The tags of the entity

        Returns:
            list
        """
        return self._getmutable(const.Fields.TAGS, mutableType=list)

    @tags.setter
    def tags(self, value):
        """
        Sets the tags for the entity

        Arguments:
            value (list): list of tags
        """
        self._setattr(const.Fields.TAGS, value, isMutable=True)

    @property
    def tasks(self):
        """
        Tasks associated with this entity

        Returns:
            list
        """
        return self._getattr(const.Fields.TASKS)

    @tasks.setter
    def tasks(self, value):
        """
        Sets the tasks associated with this entity

        Arguments:
            value (list): list of tasks

        Returns:
            list
        """
        self._setattr(const.Fields.TASKS, value)

    @property
    def subtype(self):
        """
        The entity subtype for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.ENTITY_SUBTYPE)

    @subtype.setter
    def subtype(self, value):
        """
        Sets the entity subtype for this entity

        Arguments:
            value (str): entity subtype
        """
        self._setattr(const.Fields.ENTITY_SUBTYPE, value)

    @property
    def dialogueId(self):
        """
        The dialogue id from Dialogue Star associated with this entity.
        Only Asset Character entities should have have this value.

        Returns:
            int
        """
        return self._getattr(const.Fields.DIALOGUE_ID)

    @dialogueId.setter
    def dialogueId(self, value):
        """
        Sets the Dialogue Star Id for this entity

        Returns:
            int
        """
        self._setattr(const.Fields.DIALOGUE_ID, value)

    @property
    def status(self):
        """
        The status code for current status this entity is set to

        Returns:
            str
        """
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        """
        Sets the status code to use for this entity.
        Also sets the status name based on the code being set.

        Arguments:
            value (str): status code to set
        """
        self._setattr(const.Fields.STATUS, value)
        self._setattr(const.Fields.STATUS_NAME, const.Statuses.getName(value))

    @property
    def statusName(self):
        """
        The full name of the status this entity is set to

        Returns:
            str
        """
        return const.Statuses.getName(self.status)

    @property
    def perforcePath(self):
        """
        The perforce path for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.PERFORCE_PATH)

    @perforcePath.setter
    def perforcePath(self, value):
        """
        Sets the perforce path of this entity

        Arguments:
            value (str): the path to this entity in the perforce
        """
        self._setattr(const.Fields.PERFORCE_PATH, value)

    @property
    def description(self):
        """
        The description for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.DESCRIPTION)

    @description.setter
    def description(self, value):
        """
        Sets the description of this entity

        Arguments:
            value (str): the description for this entity
        """
        self._setattr(const.Fields.DESCRIPTION, value)

    @property
    def facialRigType(self):
        """
        The facial rig type for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.FACIAL_RIG_TYPE)

    @facialRigType.setter
    def facialRigType(self, value):
        """
        Sets the facial rig type of this entity

        Arguments:
            value (str): the facial rig type for this entity
        """
        self._setattr(const.Fields.FACIAL_RIG_TYPE, value)

    @property
    def animalType(self):
        """
        The animal type for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.ANIMAL_TYPE)

    @animalType.setter
    def animalType(self, value):
        """
        Sets the animal type of this entity

        Arguments:
            value (str): the animal type for this entity
        """
        self._setattr(const.Fields.ANIMAL_TYPE, value)
    
    @property    
    def uploadedMovie(self):
        """
        The sg_uploaded_movie for this entity

        Returns:
            str (path)
        """
        return self._getattr(const.Fields.UPLOADED_MOVIE)
    
    @uploadedMovie.setter
    def uploadedMovie(self, value):
        """
        Sets the sg_uploaded_movie of this entity

        Arguments:
            value (str): the sg_uploaded_movie path for this entity
        """    
        self._setattr(const.Fields.UPLOADED_MOVIE, value)
        
    @property    
    def frameRate(self):
        """
        The frame rate for this entity

        Returns:
            float
        """
        return self._getattr(const.Fields.FRAME_RATE)
    
    @frameRate.setter
    def frameRate(self, value):
        """
        Sets the frame rate of this entity

        Arguments:
            value (float): the frame rate for this entity
        """
        self._setattr(const.Fields.FRAME_RATE, value)

    @property
    def asmId(self):
        """The *asm id for assets being tracked by one of the asm tools maintained by the techart team.
        Only Asset Character entities should have have this value.

        Returns:
            str
        """
        return self._getattr(const.Fields.DIALOGUE_ID)

    @asmId.setter
    def asmId(self, value):
        """Sets the *asm id for assets being tracked by one of the asm tools maintained by the techart team on this entity.
        
        Args: 
            value (str): the *asm id for this asset 
        """
        self._setattr(const.Fields.DIALOGUE_ID, value)

    @property
    def vaultId(self):
        """The vault id for assets being tracked on the vault database maintained by the tools team.

        Returns:
            str
        """
        return self._getattr(const.Fields.VAULT_ID)

    @vaultId.setter
    def vaultId(self, value):
        """Sets the vault id for assets being tracked on the vault database maintained by the tools team.

        Args: 
            value (str): the vault id for this asset
        """
        self._setattr(const.Fields.VAULT_ID, value)

    @property
    def propType(self):
        """
        The prop type for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.PROP_TYPE)

    @propType.setter
    def propType(self, value):
        """
        Sets the prop type of this entity

        Arguments:
            value (str): the prop type for this entity
        """
        self._setattr(const.Fields.PROP_TYPE, value)

    @property
    def vaultPath(self):
        """
        The vault path for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.VAULT_PATH)

    @vaultPath.setter
    def vaultPath(self, value):
        """
        Sets the vault path of this entity

        Arguments:
            value (str): the vault path for this entity
        """
        self._setattr(const.Fields.VAULT_PATH, value)

    @property
    def dueDate(self):
        """
        The due date for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.DUE_DATE)

    @dueDate.setter
    def dueDate(self, value):
        """
        Sets the due date of this entity

        Arguments:
            value (str): the vault path for this entity
        """
        self._setattr(const.Fields.DUE_DATE, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Asset

    @property
    def subtypes(self):
        return SubTypes

    def getByBugNumber(self, bugNumber, filters=None):
        """
        Gets the entity based on the bug number associated with it

        Arguments:
            bugNumber (int): bug id to use for getting the task associated to the bug
            filters(list): list of filters (more lists) to use for narrowing down the search

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        filters = filters or []
        filters.append([const.Fields.BUG_NUMBER, const.Operators.IS, bugNumber])
        entity = self.find(filters)
        if entity:
            return entity[0]

    def getByDialogueStarName(self, dialogueStarName, filters=None):
        """Gets the asset based on its d* name.

        Args:
            dialogueStarName (str): D* name of the asset.
            filters (list): List of extra filters to apply.

        Returns:
            rsg_core.shotgun.entities.asset.Asset: Asset object with the given d* name, else None if none is found.
        """
        filters = filters or []
        filters.append([const.Fields.DIALOGUE_STAR_NAME, const.Operators.IS, dialogueStarName])
        return self.findOne(filters)

    def getByDialogueId(self, dialogueId, filters=None, shotgun=None):
        """
        Gets the entity based on the bug number associated with it

        Arguments:
            name (str): name of the entity
            shotgun (rsg_core.shotgun.core.Shotgun): shotgun instance to perform operations with

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        filters = filters or []
        filters.append([const.Fields.DIALOGUE_ID, const.Operators.IS, dialogueId])
        entity = self.find(filters)
        if entity:
            return entity[0]

    def getByPerforcePath(self, perforcePath, filters=[]):
        """Gets the Asset based on the perforce Path that it represents.

        Args:
            perforcePath (str): file path like it is in perforce.
            filters (list): List of extra filters to apply.

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        filters = filters or []
        filters.append([const.Fields.PERFORCE_PATH, const.Operators.IS, perforcePath])
        entity = self.find(filters)
        if entity:
            return entity[0]
