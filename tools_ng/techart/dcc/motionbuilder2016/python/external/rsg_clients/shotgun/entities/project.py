from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Project(baseEntity.BaseEntity):
    """
    Class that represents a Project entity
    """

    ENTITY_TYPE = const.Entities.PROJECT
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.NAME,
        const.Fields.SHORTNAME,
        const.Fields.BUGSTAR_ID,
        const.Fields.WATCHSTAR_ID,
    )
    NAME_FIELD = const.Fields.NAME
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        return self._getattr(const.Fields.NAME)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.NAME, value)

    @property
    def shortname(self):
        return self._getattr(const.Fields.SHORTNAME)

    @shortname.setter
    def shortname(self, value):
        self._setattr(const.Fields.SHORTNAME, value)

    @property
    def bugstarId(self):
        return self._getattr(const.Fields.BUGSTAR_ID)

    @bugstarId.setter
    def bugstarId(self, value):
        self._setattr(const.Fields.BUGSTAR_ID, value)

    @property
    def watchstarId(self):
        return self._getattr(const.Fields.WATCHSTAR_ID)

    @watchstarId.setter
    def watchstarId(self, value):
        self._setattr(const.Fields.WATCHSTAR_ID, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Project

    def getByBugstarId(self, bugstarId):
        """
        Get a project from shotgun based on the bugstar id

        Args:
            bugstarId (int): the bugstar id of the project

        Returns:
            rockstare.shotgun.entities.project.Project
        """
        projects = self.find([[const.Fields.BUGSTAR_ID, const.Operators.IS, bugstarId]])
        if projects:
            return projects[0]
        return None

    def getByWatchstarId(self, watchstarId):
        """
        Get a project from shotgun based on the watchstar id

        Args:
            watchstarId (int): the watchstar id of the project

        Returns:
            rockstare.shotgun.entities.project.Project
        """
        projects = self.find([[const.Fields.WATCHSTAR_ID, const.Operators.IS, watchstarId]])
        if projects:
            return projects[0]
        return None
